﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.Win32;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000175 RID: 373
	[Serializable]
	public sealed class RuntimeFramework
	{
		// Token: 0x060009D4 RID: 2516 RVA: 0x00021030 File Offset: 0x0001F230
		public RuntimeFramework(RuntimeType runtime, Version version)
		{
			this.Runtime = runtime;
			if (version.Build < 0)
			{
				this.InitFromFrameworkVersion(version);
			}
			else
			{
				this.InitFromClrVersion(version);
			}
			this.DisplayName = RuntimeFramework.GetDefaultDisplayName(runtime, version);
		}

		// Token: 0x060009D5 RID: 2517 RVA: 0x00021080 File Offset: 0x0001F280
		private void InitFromFrameworkVersion(Version version)
		{
			this.ClrVersion = version;
			this.FrameworkVersion = version;
			if (version.Major > 0)
			{
				switch (this.Runtime)
				{
				case RuntimeType.Any:
				case RuntimeType.Net:
				case RuntimeType.Mono:
					switch (version.Major)
					{
					case 1:
						switch (version.Minor)
						{
						case 0:
							this.ClrVersion = ((this.Runtime == RuntimeType.Mono) ? new Version(1, 1, 4322) : new Version(1, 0, 3705));
							break;
						case 1:
							if (this.Runtime == RuntimeType.Mono)
							{
								this.FrameworkVersion = new Version(1, 0);
							}
							this.ClrVersion = new Version(1, 1, 4322);
							break;
						default:
							RuntimeFramework.ThrowInvalidFrameworkVersion(version);
							break;
						}
						break;
					case 2:
					case 3:
						this.ClrVersion = new Version(2, 0, 50727);
						break;
					case 4:
						this.ClrVersion = new Version(4, 0, 30319);
						break;
					default:
						RuntimeFramework.ThrowInvalidFrameworkVersion(version);
						break;
					}
					break;
				case RuntimeType.NetCF:
				{
					int num = version.Major;
					if (num == 3)
					{
						num = version.Minor;
						if (num == 5)
						{
							this.ClrVersion = new Version(3, 5, 7283);
						}
					}
					break;
				}
				case RuntimeType.Silverlight:
					this.ClrVersion = ((version.Major >= 4) ? new Version(4, 0, 60310) : new Version(2, 0, 50727));
					break;
				}
			}
		}

		// Token: 0x060009D6 RID: 2518 RVA: 0x00021216 File Offset: 0x0001F416
		private static void ThrowInvalidFrameworkVersion(Version version)
		{
			throw new ArgumentException("Unknown framework version " + version, "version");
		}

		// Token: 0x060009D7 RID: 2519 RVA: 0x00021230 File Offset: 0x0001F430
		private void InitFromClrVersion(Version version)
		{
			this.FrameworkVersion = new Version(version.Major, version.Minor);
			this.ClrVersion = version;
			if (this.Runtime == RuntimeType.Mono && version.Major == 1)
			{
				this.FrameworkVersion = new Version(1, 0);
			}
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x060009D8 RID: 2520 RVA: 0x00021288 File Offset: 0x0001F488
		public static RuntimeFramework CurrentFramework
		{
			get
			{
				return RuntimeFramework.currentFramework.Value;
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x060009D9 RID: 2521 RVA: 0x000212A4 File Offset: 0x0001F4A4
		// (set) Token: 0x060009DA RID: 2522 RVA: 0x000212BB File Offset: 0x0001F4BB
		public RuntimeType Runtime
		{
			[CompilerGenerated]
			get
			{
				return this.<Runtime>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Runtime>k__BackingField = value;
			}
		}

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x060009DB RID: 2523 RVA: 0x000212C4 File Offset: 0x0001F4C4
		// (set) Token: 0x060009DC RID: 2524 RVA: 0x000212DB File Offset: 0x0001F4DB
		public Version FrameworkVersion
		{
			[CompilerGenerated]
			get
			{
				return this.<FrameworkVersion>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<FrameworkVersion>k__BackingField = value;
			}
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x060009DD RID: 2525 RVA: 0x000212E4 File Offset: 0x0001F4E4
		// (set) Token: 0x060009DE RID: 2526 RVA: 0x000212FB File Offset: 0x0001F4FB
		public Version ClrVersion
		{
			[CompilerGenerated]
			get
			{
				return this.<ClrVersion>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ClrVersion>k__BackingField = value;
			}
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x060009DF RID: 2527 RVA: 0x00021304 File Offset: 0x0001F504
		public bool AllowAnyVersion
		{
			get
			{
				return this.ClrVersion == RuntimeFramework.DefaultVersion;
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x060009E0 RID: 2528 RVA: 0x00021328 File Offset: 0x0001F528
		// (set) Token: 0x060009E1 RID: 2529 RVA: 0x0002133F File Offset: 0x0001F53F
		public string DisplayName
		{
			[CompilerGenerated]
			get
			{
				return this.<DisplayName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<DisplayName>k__BackingField = value;
			}
		}

		// Token: 0x060009E2 RID: 2530 RVA: 0x00021348 File Offset: 0x0001F548
		public static RuntimeFramework Parse(string s)
		{
			RuntimeType runtime = RuntimeType.Any;
			Version version = RuntimeFramework.DefaultVersion;
			string[] array = s.Split(new char[]
			{
				'-'
			});
			if (array.Length == 2)
			{
				runtime = (RuntimeType)Enum.Parse(typeof(RuntimeType), array[0], true);
				string text = array[1];
				if (text != "")
				{
					version = new Version(text);
				}
			}
			else if (char.ToLower(s[0]) == 'v')
			{
				version = new Version(s.Substring(1));
			}
			else if (RuntimeFramework.IsRuntimeTypeName(s))
			{
				runtime = (RuntimeType)Enum.Parse(typeof(RuntimeType), s, true);
			}
			else
			{
				version = new Version(s);
			}
			return new RuntimeFramework(runtime, version);
		}

		// Token: 0x060009E3 RID: 2531 RVA: 0x0002142C File Offset: 0x0001F62C
		public override string ToString()
		{
			string result;
			if (this.AllowAnyVersion)
			{
				result = this.Runtime.ToString().ToLower();
			}
			else
			{
				string text = this.FrameworkVersion.ToString();
				if (this.Runtime == RuntimeType.Any)
				{
					result = "v" + text;
				}
				else
				{
					result = this.Runtime.ToString().ToLower() + "-" + text;
				}
			}
			return result;
		}

		// Token: 0x060009E4 RID: 2532 RVA: 0x000214B0 File Offset: 0x0001F6B0
		public bool Supports(RuntimeFramework target)
		{
			return (this.Runtime == RuntimeType.Any || target.Runtime == RuntimeType.Any || this.Runtime == target.Runtime) && (this.AllowAnyVersion || target.AllowAnyVersion || (RuntimeFramework.VersionsMatch(this.ClrVersion, target.ClrVersion) && ((this.Runtime == RuntimeType.Silverlight) ? (this.FrameworkVersion.Major == target.FrameworkVersion.Major && this.FrameworkVersion.Minor == target.FrameworkVersion.Minor) : (this.FrameworkVersion.Major >= target.FrameworkVersion.Major && this.FrameworkVersion.Minor >= target.FrameworkVersion.Minor))));
		}

		// Token: 0x060009E5 RID: 2533 RVA: 0x000215CC File Offset: 0x0001F7CC
		private static bool IsRuntimeTypeName(string name)
		{
			return TypeHelper.GetEnumNames(typeof(RuntimeType)).Any((string item) => item.ToLower() == name.ToLower());
		}

		// Token: 0x060009E6 RID: 2534 RVA: 0x0002160C File Offset: 0x0001F80C
		private static string GetDefaultDisplayName(RuntimeType runtime, Version version)
		{
			string result;
			if (version == RuntimeFramework.DefaultVersion)
			{
				result = runtime.ToString();
			}
			else if (runtime == RuntimeType.Any)
			{
				result = "v" + version;
			}
			else
			{
				result = runtime + " " + version;
			}
			return result;
		}

		// Token: 0x060009E7 RID: 2535 RVA: 0x00021668 File Offset: 0x0001F868
		private static bool VersionsMatch(Version v1, Version v2)
		{
			return v1.Major == v2.Major && v1.Minor == v2.Minor && (v1.Build < 0 || v2.Build < 0 || v1.Build == v2.Build) && (v1.Revision < 0 || v2.Revision < 0 || v1.Revision == v2.Revision);
		}

		// Token: 0x060009E8 RID: 2536 RVA: 0x000218D0 File Offset: 0x0001FAD0
		// Note: this type is marked as 'beforefieldinit'.
		static RuntimeFramework()
		{
		}

		// Token: 0x060009E9 RID: 2537 RVA: 0x000216DC File Offset: 0x0001F8DC
		[CompilerGenerated]
		private static RuntimeFramework <.cctor>b__4()
		{
			Type type = Type.GetType("Mono.Runtime", false);
			Type type2 = Type.GetType("MonoTouch.UIKit.UIApplicationDelegate,monotouch");
			bool flag = type2 != null;
			bool flag2 = type != null;
			RuntimeType runtime = flag ? RuntimeType.MonoTouch : (flag2 ? RuntimeType.Mono : ((Environment.OSVersion.Platform == PlatformID.WinCE) ? RuntimeType.NetCF : RuntimeType.Net));
			int num = Environment.Version.Major;
			int minor = Environment.Version.Minor;
			if (flag2)
			{
				switch (num)
				{
				case 1:
					minor = 0;
					break;
				case 2:
					num = 3;
					minor = 5;
					break;
				}
			}
			else if (num == 2)
			{
				using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\.NETFramework"))
				{
					if (registryKey != null)
					{
						string text = registryKey.GetValue("InstallRoot") as string;
						if (text != null)
						{
							if (Directory.Exists(Path.Combine(text, "v3.5")))
							{
								num = 3;
								minor = 5;
							}
							else if (Directory.Exists(Path.Combine(text, "v3.0")))
							{
								num = 3;
								minor = 0;
							}
						}
					}
				}
			}
			else if (num == 4 && Type.GetType("System.Reflection.AssemblyMetadataAttribute") != null)
			{
				minor = 5;
			}
			RuntimeFramework runtimeFramework = new RuntimeFramework(runtime, new Version(num, minor))
			{
				ClrVersion = Environment.Version
			};
			if (flag2)
			{
				MethodInfo method = type.GetMethod("GetDisplayName", BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.ExactBinding);
				if (method != null)
				{
					runtimeFramework.DisplayName = (string)method.Invoke(null, new object[0]);
				}
			}
			return runtimeFramework;
		}

		// Token: 0x04000240 RID: 576
		public static readonly Version DefaultVersion = new Version(0, 0);

		// Token: 0x04000241 RID: 577
		private static readonly System.Lazy<RuntimeFramework> currentFramework = new System.Lazy<RuntimeFramework>(delegate()
		{
			Type type = Type.GetType("Mono.Runtime", false);
			Type type2 = Type.GetType("MonoTouch.UIKit.UIApplicationDelegate,monotouch");
			bool flag = type2 != null;
			bool flag2 = type != null;
			RuntimeType runtime = flag ? RuntimeType.MonoTouch : (flag2 ? RuntimeType.Mono : ((Environment.OSVersion.Platform == PlatformID.WinCE) ? RuntimeType.NetCF : RuntimeType.Net));
			int num = Environment.Version.Major;
			int minor = Environment.Version.Minor;
			if (flag2)
			{
				switch (num)
				{
				case 1:
					minor = 0;
					break;
				case 2:
					num = 3;
					minor = 5;
					break;
				}
			}
			else if (num == 2)
			{
				using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\.NETFramework"))
				{
					if (registryKey != null)
					{
						string text = registryKey.GetValue("InstallRoot") as string;
						if (text != null)
						{
							if (Directory.Exists(Path.Combine(text, "v3.5")))
							{
								num = 3;
								minor = 5;
							}
							else if (Directory.Exists(Path.Combine(text, "v3.0")))
							{
								num = 3;
								minor = 0;
							}
						}
					}
				}
			}
			else if (num == 4 && Type.GetType("System.Reflection.AssemblyMetadataAttribute") != null)
			{
				minor = 5;
			}
			RuntimeFramework runtimeFramework = new RuntimeFramework(runtime, new Version(num, minor))
			{
				ClrVersion = Environment.Version
			};
			if (flag2)
			{
				MethodInfo method = type.GetMethod("GetDisplayName", BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.ExactBinding);
				if (method != null)
				{
					runtimeFramework.DisplayName = (string)method.Invoke(null, new object[0]);
				}
			}
			return runtimeFramework;
		});

		// Token: 0x04000242 RID: 578
		[CompilerGenerated]
		private RuntimeType <Runtime>k__BackingField;

		// Token: 0x04000243 RID: 579
		[CompilerGenerated]
		private Version <FrameworkVersion>k__BackingField;

		// Token: 0x04000244 RID: 580
		[CompilerGenerated]
		private Version <ClrVersion>k__BackingField;

		// Token: 0x04000245 RID: 581
		[CompilerGenerated]
		private string <DisplayName>k__BackingField;

		// Token: 0x04000246 RID: 582
		[CompilerGenerated]
		private static Func<RuntimeFramework> CS$<>9__CachedAnonymousMethodDelegate5;

		// Token: 0x020001C4 RID: 452
		[CompilerGenerated]
		private sealed class <>c__DisplayClass1
		{
			// Token: 0x06000B66 RID: 2918 RVA: 0x0002159A File Offset: 0x0001F79A
			public <>c__DisplayClass1()
			{
			}

			// Token: 0x06000B67 RID: 2919 RVA: 0x000215A4 File Offset: 0x0001F7A4
			public bool <IsRuntimeTypeName>b__0(string item)
			{
				return item.ToLower() == this.name.ToLower();
			}

			// Token: 0x040002F3 RID: 755
			public string name;
		}
	}
}
