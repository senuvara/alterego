﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x0200006D RID: 109
	public static class Reflect
	{
		// Token: 0x0600040D RID: 1037 RVA: 0x0000CF7C File Offset: 0x0000B17C
		public static MethodInfo[] GetMethodsWithAttribute(Type fixtureType, Type attributeType, bool inherit)
		{
			List<MethodInfo> list = new List<MethodInfo>();
			BindingFlags bindingAttr = Reflect.AllMembers | (inherit ? BindingFlags.FlattenHierarchy : BindingFlags.DeclaredOnly);
			foreach (MethodInfo methodInfo in fixtureType.GetMethods(bindingAttr))
			{
				if (methodInfo.IsDefined(attributeType, inherit))
				{
					list.Add(methodInfo);
				}
			}
			list.Sort(new Reflect.BaseTypesFirstComparer());
			return list.ToArray();
		}

		// Token: 0x0600040E RID: 1038 RVA: 0x0000CFFC File Offset: 0x0000B1FC
		public static bool HasMethodWithAttribute(Type fixtureType, Type attributeType)
		{
			foreach (MethodInfo methodInfo in fixtureType.GetMethods(Reflect.AllMembers | BindingFlags.FlattenHierarchy))
			{
				if (methodInfo.IsDefined(attributeType, false))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x0000D050 File Offset: 0x0000B250
		public static object Construct(Type type)
		{
			object result;
			if (Reflect.ConstructorCallWrapper != null)
			{
				result = Reflect.ConstructorCallWrapper(type, null);
			}
			else
			{
				ConstructorInfo constructor = type.GetConstructor(Reflect.EmptyTypes);
				if (constructor == null)
				{
					throw new InvalidTestFixtureException(type.FullName + " does not have a default constructor");
				}
				result = constructor.Invoke(null);
			}
			return result;
		}

		// Token: 0x06000410 RID: 1040 RVA: 0x0000D0B4 File Offset: 0x0000B2B4
		public static object Construct(Type type, object[] arguments)
		{
			object result;
			if (Reflect.ConstructorCallWrapper != null)
			{
				result = Reflect.ConstructorCallWrapper(type, arguments);
			}
			else if (arguments == null)
			{
				result = Reflect.Construct(type);
			}
			else
			{
				Type[] typeArray = Reflect.GetTypeArray(arguments);
				ITypeInfo typeInfo = new TypeWrapper(type);
				ConstructorInfo constructor = typeInfo.GetConstructor(typeArray);
				if (constructor == null)
				{
					throw new InvalidTestFixtureException(type.FullName + " does not have a suitable constructor");
				}
				result = constructor.Invoke(arguments);
			}
			return result;
		}

		// Token: 0x06000411 RID: 1041 RVA: 0x0000D13C File Offset: 0x0000B33C
		internal static Type[] GetTypeArray(object[] objects)
		{
			Type[] array = new Type[objects.Length];
			int num = 0;
			foreach (object obj in objects)
			{
				array[num++] = ((obj == null) ? typeof(NUnitNullType) : obj.GetType());
			}
			return array;
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x0000D19C File Offset: 0x0000B39C
		public static object InvokeMethod(MethodInfo method, object fixture)
		{
			return Reflect.InvokeMethod(method, fixture, null);
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x0000D1E8 File Offset: 0x0000B3E8
		public static object InvokeMethod(MethodInfo method, object fixture, params object[] args)
		{
			if (method != null)
			{
				try
				{
					if (Reflect.MethodCallWrapper != null)
					{
						return Reflect.MethodCallWrapper(() => method.Invoke(fixture, args));
					}
					return method.Invoke(fixture, args);
				}
				catch (ThreadAbortException)
				{
					return null;
				}
				catch (TargetInvocationException ex)
				{
					throw new NUnitException("Rethrown", ex.InnerException);
				}
				catch (Exception inner)
				{
					throw new NUnitException("Rethrown", inner);
				}
			}
			return null;
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000414 RID: 1044 RVA: 0x0000D2C8 File Offset: 0x0000B4C8
		// (set) Token: 0x06000415 RID: 1045 RVA: 0x0000D2DE File Offset: 0x0000B4DE
		public static Func<Func<object>, object> MethodCallWrapper
		{
			[CompilerGenerated]
			get
			{
				return Reflect.<MethodCallWrapper>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				Reflect.<MethodCallWrapper>k__BackingField = value;
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000416 RID: 1046 RVA: 0x0000D2E8 File Offset: 0x0000B4E8
		// (set) Token: 0x06000417 RID: 1047 RVA: 0x0000D2FE File Offset: 0x0000B4FE
		public static Func<Type, object[], object> ConstructorCallWrapper
		{
			[CompilerGenerated]
			get
			{
				return Reflect.<ConstructorCallWrapper>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				Reflect.<ConstructorCallWrapper>k__BackingField = value;
			}
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x0000D306 File Offset: 0x0000B506
		// Note: this type is marked as 'beforefieldinit'.
		static Reflect()
		{
		}

		// Token: 0x040000AE RID: 174
		private static readonly BindingFlags AllMembers = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

		// Token: 0x040000AF RID: 175
		private static readonly Type[] EmptyTypes = new Type[0];

		// Token: 0x040000B0 RID: 176
		[CompilerGenerated]
		private static Func<Func<object>, object> <MethodCallWrapper>k__BackingField;

		// Token: 0x040000B1 RID: 177
		[CompilerGenerated]
		private static Func<Type, object[], object> <ConstructorCallWrapper>k__BackingField;

		// Token: 0x0200006E RID: 110
		private class BaseTypesFirstComparer : IComparer<MethodInfo>
		{
			// Token: 0x06000419 RID: 1049 RVA: 0x0000D31C File Offset: 0x0000B51C
			public int Compare(MethodInfo m1, MethodInfo m2)
			{
				int result;
				if (m1 == null || m2 == null)
				{
					result = 0;
				}
				else
				{
					Type declaringType = m1.DeclaringType;
					Type declaringType2 = m2.DeclaringType;
					if (declaringType == declaringType2)
					{
						result = 0;
					}
					else if (declaringType.IsAssignableFrom(declaringType2))
					{
						result = -1;
					}
					else
					{
						result = 1;
					}
				}
				return result;
			}

			// Token: 0x0600041A RID: 1050 RVA: 0x0000D373 File Offset: 0x0000B573
			public BaseTypesFirstComparer()
			{
			}
		}

		// Token: 0x020001B0 RID: 432
		[CompilerGenerated]
		private sealed class <>c__DisplayClass2
		{
			// Token: 0x06000B28 RID: 2856 RVA: 0x0000D1B6 File Offset: 0x0000B3B6
			public <>c__DisplayClass2()
			{
			}

			// Token: 0x06000B29 RID: 2857 RVA: 0x0000D1C0 File Offset: 0x0000B3C0
			public object <InvokeMethod>b__0()
			{
				return this.method.Invoke(this.fixture, this.args);
			}

			// Token: 0x040002C2 RID: 706
			public MethodInfo method;

			// Token: 0x040002C3 RID: 707
			public object fixture;

			// Token: 0x040002C4 RID: 708
			public object[] args;
		}
	}
}
