﻿using System;
using System.Threading;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000E7 RID: 231
	public static class ThreadUtility
	{
		// Token: 0x060006AF RID: 1711 RVA: 0x00018401 File Offset: 0x00016601
		public static void Kill(Thread thread)
		{
			ThreadUtility.Kill(thread, null);
		}

		// Token: 0x060006B0 RID: 1712 RVA: 0x0001840C File Offset: 0x0001660C
		public static void Kill(Thread thread, object stateInfo)
		{
			try
			{
				if (stateInfo == null)
				{
					thread.Abort();
				}
				else
				{
					thread.Abort(stateInfo);
				}
			}
			catch (ThreadStateException)
			{
				thread.Resume();
			}
			if ((thread.ThreadState & ThreadState.WaitSleepJoin) != ThreadState.Running)
			{
				thread.Interrupt();
			}
		}
	}
}
