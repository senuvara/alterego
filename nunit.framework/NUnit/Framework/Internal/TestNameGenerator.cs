﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000A4 RID: 164
	public class TestNameGenerator
	{
		// Token: 0x060004EB RID: 1259 RVA: 0x0001091D File Offset: 0x0000EB1D
		public TestNameGenerator()
		{
			this._pattern = TestNameGenerator.DefaultTestNamePattern;
		}

		// Token: 0x060004EC RID: 1260 RVA: 0x00010933 File Offset: 0x0000EB33
		public TestNameGenerator(string pattern)
		{
			this._pattern = pattern;
		}

		// Token: 0x060004ED RID: 1261 RVA: 0x00010948 File Offset: 0x0000EB48
		public string GetDisplayName(TestMethod testMethod)
		{
			return this.GetDisplayName(testMethod, null);
		}

		// Token: 0x060004EE RID: 1262 RVA: 0x00010964 File Offset: 0x0000EB64
		public string GetDisplayName(TestMethod testMethod, object[] args)
		{
			if (this._fragments == null)
			{
				this._fragments = TestNameGenerator.BuildFragmentList(this._pattern);
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (TestNameGenerator.NameFragment nameFragment in this._fragments)
			{
				stringBuilder.Append(nameFragment.GetText(testMethod, args));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060004EF RID: 1263 RVA: 0x000109F8 File Offset: 0x0000EBF8
		private static List<TestNameGenerator.NameFragment> BuildFragmentList(string pattern)
		{
			List<TestNameGenerator.NameFragment> list = new List<TestNameGenerator.NameFragment>();
			int i = 0;
			while (i < pattern.Length)
			{
				int num = pattern.IndexOf('{', i);
				if (num < 0)
				{
					break;
				}
				int num2 = pattern.IndexOf('}', num);
				if (num2 < 0)
				{
					break;
				}
				if (num > i)
				{
					list.Add(new TestNameGenerator.FixedTextFragment(pattern.Substring(i, num - i)));
				}
				string text = pattern.Substring(num, num2 - num + 1);
				string text2 = text;
				if (text2 == null)
				{
					goto IL_270;
				}
				if (<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x60004dd-1 == null)
				{
					<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x60004dd-1 = new Dictionary<string, int>(17)
					{
						{
							"{m}",
							0
						},
						{
							"{i}",
							1
						},
						{
							"{n}",
							2
						},
						{
							"{c}",
							3
						},
						{
							"{C}",
							4
						},
						{
							"{M}",
							5
						},
						{
							"{a}",
							6
						},
						{
							"{0}",
							7
						},
						{
							"{1}",
							8
						},
						{
							"{2}",
							9
						},
						{
							"{3}",
							10
						},
						{
							"{4}",
							11
						},
						{
							"{5}",
							12
						},
						{
							"{6}",
							13
						},
						{
							"{7}",
							14
						},
						{
							"{8}",
							15
						},
						{
							"{9}",
							16
						}
					};
				}
				int num3;
				if (!<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x60004dd-1.TryGetValue(text2, out num3))
				{
					goto IL_270;
				}
				switch (num3)
				{
				case 0:
					list.Add(new TestNameGenerator.MethodNameFragment());
					break;
				case 1:
					list.Add(new TestNameGenerator.TestIDFragment());
					break;
				case 2:
					list.Add(new TestNameGenerator.NamespaceFragment());
					break;
				case 3:
					list.Add(new TestNameGenerator.ClassNameFragment());
					break;
				case 4:
					list.Add(new TestNameGenerator.ClassFullNameFragment());
					break;
				case 5:
					list.Add(new TestNameGenerator.MethodFullNameFragment());
					break;
				case 6:
					list.Add(new TestNameGenerator.ArgListFragment(0));
					break;
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 16:
				{
					int index = (int)(text[1] - '0');
					list.Add(new TestNameGenerator.ArgumentFragment(index, 40));
					break;
				}
				default:
					goto IL_270;
				}
				IL_326:
				i = num2 + 1;
				continue;
				IL_270:
				char c = text[1];
				if (text.Length >= 5 && text[2] == ':' && (c == 'a' || char.IsDigit(c)))
				{
					int num4;
					try
					{
						num4 = int.Parse(text.Substring(3, text.Length - 4));
					}
					catch
					{
						num4 = -1;
					}
					if (num4 > 0)
					{
						if (c == 'a')
						{
							list.Add(new TestNameGenerator.ArgListFragment(num4));
						}
						else
						{
							list.Add(new TestNameGenerator.ArgumentFragment((int)(c - '0'), num4));
						}
						goto IL_326;
					}
				}
				list.Add(new TestNameGenerator.FixedTextFragment(text));
				goto IL_326;
			}
			if (i < pattern.Length)
			{
				list.Add(new TestNameGenerator.FixedTextFragment(pattern.Substring(i)));
			}
			return list;
		}

		// Token: 0x060004F0 RID: 1264 RVA: 0x00010D80 File Offset: 0x0000EF80
		// Note: this type is marked as 'beforefieldinit'.
		static TestNameGenerator()
		{
		}

		// Token: 0x040000F4 RID: 244
		public static string DefaultTestNamePattern = "{m}{a}";

		// Token: 0x040000F5 RID: 245
		private string _pattern;

		// Token: 0x040000F6 RID: 246
		private List<TestNameGenerator.NameFragment> _fragments;

		// Token: 0x020000A5 RID: 165
		private abstract class NameFragment
		{
			// Token: 0x060004F1 RID: 1265 RVA: 0x00010D8C File Offset: 0x0000EF8C
			public virtual string GetText(TestMethod testMethod, object[] args)
			{
				return this.GetText(testMethod.Method.MethodInfo, args);
			}

			// Token: 0x060004F2 RID: 1266
			public abstract string GetText(MethodInfo method, object[] args);

			// Token: 0x060004F3 RID: 1267 RVA: 0x00010DB0 File Offset: 0x0000EFB0
			protected static void AppendGenericTypeNames(StringBuilder sb, MethodInfo method)
			{
				sb.Append("<");
				int num = 0;
				foreach (Type type in method.GetGenericArguments())
				{
					if (num++ > 0)
					{
						sb.Append(",");
					}
					sb.Append(type.Name);
				}
				sb.Append(">");
			}

			// Token: 0x060004F4 RID: 1268 RVA: 0x00010E24 File Offset: 0x0000F024
			protected static string GetDisplayString(object arg, int stringMax)
			{
				string text = (arg == null) ? "null" : Convert.ToString(arg, CultureInfo.InvariantCulture);
				if (arg is double)
				{
					double num = (double)arg;
					if (double.IsNaN(num))
					{
						text = "double.NaN";
					}
					else if (double.IsPositiveInfinity(num))
					{
						text = "double.PositiveInfinity";
					}
					else if (double.IsNegativeInfinity(num))
					{
						text = "double.NegativeInfinity";
					}
					else if (num == 1.7976931348623157E+308)
					{
						text = "double.MaxValue";
					}
					else if (num == -1.7976931348623157E+308)
					{
						text = "double.MinValue";
					}
					else
					{
						if (text.IndexOf('.') == -1)
						{
							text += ".0";
						}
						text += "d";
					}
				}
				else if (arg is float)
				{
					float num2 = (float)arg;
					if (float.IsNaN(num2))
					{
						text = "float.NaN";
					}
					else if (float.IsPositiveInfinity(num2))
					{
						text = "float.PositiveInfinity";
					}
					else if (float.IsNegativeInfinity(num2))
					{
						text = "float.NegativeInfinity";
					}
					else if (num2 == 3.4028235E+38f)
					{
						text = "float.MaxValue";
					}
					else if (num2 == -3.4028235E+38f)
					{
						text = "float.MinValue";
					}
					else
					{
						if (text.IndexOf('.') == -1)
						{
							text += ".0";
						}
						text += "f";
					}
				}
				else if (arg is decimal)
				{
					decimal d = (decimal)arg;
					if (d == -79228162514264337593543950335m)
					{
						text = "decimal.MinValue";
					}
					else if (d == 79228162514264337593543950335m)
					{
						text = "decimal.MaxValue";
					}
					else
					{
						text += "m";
					}
				}
				else if (arg is long)
				{
					if (arg.Equals(-9223372036854775808L))
					{
						text = "long.MinValue";
					}
					else if (arg.Equals(9223372036854775807L))
					{
						text = "long.MaxValue";
					}
					else
					{
						text += "L";
					}
				}
				else if (arg is ulong)
				{
					ulong num3 = (ulong)arg;
					if (num3 == 0UL)
					{
						text = "ulong.MinValue";
					}
					else if (num3 == 18446744073709551615UL)
					{
						text = "ulong.MaxValue";
					}
					else
					{
						text += "UL";
					}
				}
				else if (arg is string)
				{
					string text2 = (string)arg;
					bool flag = stringMax > 0 && text2.Length > stringMax;
					int num4 = flag ? (stringMax - "...".Length) : 0;
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append("\"");
					foreach (char c in text2)
					{
						stringBuilder.Append(TestNameGenerator.NameFragment.EscapeCharInString(c));
						if (flag && stringBuilder.Length > num4)
						{
							stringBuilder.Append("...");
							break;
						}
					}
					stringBuilder.Append("\"");
					text = stringBuilder.ToString();
				}
				else if (arg is char)
				{
					text = "'" + TestNameGenerator.NameFragment.EscapeSingleChar((char)arg) + "'";
				}
				else if (arg is int)
				{
					if (arg.Equals(2147483647))
					{
						text = "int.MaxValue";
					}
					else if (arg.Equals(-2147483648))
					{
						text = "int.MinValue";
					}
				}
				else if (arg is uint)
				{
					if (arg.Equals(4294967295U))
					{
						text = "uint.MaxValue";
					}
					else if (arg.Equals(0U))
					{
						text = "uint.MinValue";
					}
				}
				else if (arg is short)
				{
					if (arg.Equals(32767))
					{
						text = "short.MaxValue";
					}
					else if (arg.Equals(-32768))
					{
						text = "short.MinValue";
					}
				}
				else if (arg is ushort)
				{
					if (arg.Equals(65535))
					{
						text = "ushort.MaxValue";
					}
					else if (arg.Equals(0))
					{
						text = "ushort.MinValue";
					}
				}
				else if (arg is byte)
				{
					if (arg.Equals(255))
					{
						text = "byte.MaxValue";
					}
					else if (arg.Equals(0))
					{
						text = "byte.MinValue";
					}
				}
				else if (arg is sbyte)
				{
					if (arg.Equals(127))
					{
						text = "sbyte.MaxValue";
					}
					else if (arg.Equals(-128))
					{
						text = "sbyte.MinValue";
					}
				}
				return text;
			}

			// Token: 0x060004F5 RID: 1269 RVA: 0x00011434 File Offset: 0x0000F634
			private static string EscapeSingleChar(char c)
			{
				string result;
				if (c == '\'')
				{
					result = "\\'";
				}
				else
				{
					result = TestNameGenerator.NameFragment.EscapeControlChar(c);
				}
				return result;
			}

			// Token: 0x060004F6 RID: 1270 RVA: 0x00011460 File Offset: 0x0000F660
			private static string EscapeCharInString(char c)
			{
				string result;
				if (c == '"')
				{
					result = "\\\"";
				}
				else
				{
					result = TestNameGenerator.NameFragment.EscapeControlChar(c);
				}
				return result;
			}

			// Token: 0x060004F7 RID: 1271 RVA: 0x0001148C File Offset: 0x0000F68C
			private static string EscapeControlChar(char c)
			{
				char c2 = c;
				if (c2 > '\\')
				{
					if (c2 != '\u0085')
					{
						switch (c2)
						{
						case '\u2028':
						case '\u2029':
							break;
						default:
							goto IL_C6;
						}
					}
					return string.Format("\\x{0:X4}", (int)c);
				}
				switch (c2)
				{
				case '\0':
					return "\\0";
				case '\u0001':
				case '\u0002':
				case '\u0003':
				case '\u0004':
				case '\u0005':
				case '\u0006':
					break;
				case '\a':
					return "\\a";
				case '\b':
					return "\\b";
				case '\t':
					return "\\t";
				case '\n':
					return "\\n";
				case '\v':
					return "\\v";
				case '\f':
					return "\\f";
				case '\r':
					return "\\r";
				default:
					if (c2 == '\\')
					{
						return "\\\\";
					}
					break;
				}
				IL_C6:
				return c.ToString();
			}

			// Token: 0x060004F8 RID: 1272 RVA: 0x0001156A File Offset: 0x0000F76A
			protected NameFragment()
			{
			}

			// Token: 0x040000F7 RID: 247
			private const string THREE_DOTS = "...";
		}

		// Token: 0x020000A6 RID: 166
		private class TestIDFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x060004F9 RID: 1273 RVA: 0x00011574 File Offset: 0x0000F774
			public override string GetText(MethodInfo method, object[] args)
			{
				return "{i}";
			}

			// Token: 0x060004FA RID: 1274 RVA: 0x0001158C File Offset: 0x0000F78C
			public override string GetText(TestMethod testMethod, object[] args)
			{
				return testMethod.Id;
			}

			// Token: 0x060004FB RID: 1275 RVA: 0x000115A4 File Offset: 0x0000F7A4
			public TestIDFragment()
			{
			}
		}

		// Token: 0x020000A7 RID: 167
		private class FixedTextFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x060004FC RID: 1276 RVA: 0x000115AC File Offset: 0x0000F7AC
			public FixedTextFragment(string text)
			{
				this._text = text;
			}

			// Token: 0x060004FD RID: 1277 RVA: 0x000115C0 File Offset: 0x0000F7C0
			public override string GetText(MethodInfo method, object[] args)
			{
				return this._text;
			}

			// Token: 0x040000F8 RID: 248
			private string _text;
		}

		// Token: 0x020000A8 RID: 168
		private class MethodNameFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x060004FE RID: 1278 RVA: 0x000115D8 File Offset: 0x0000F7D8
			public override string GetText(MethodInfo method, object[] args)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(method.Name);
				if (method.IsGenericMethod)
				{
					TestNameGenerator.NameFragment.AppendGenericTypeNames(stringBuilder, method);
				}
				return stringBuilder.ToString();
			}

			// Token: 0x060004FF RID: 1279 RVA: 0x00011618 File Offset: 0x0000F818
			public MethodNameFragment()
			{
			}
		}

		// Token: 0x020000A9 RID: 169
		private class NamespaceFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x06000500 RID: 1280 RVA: 0x00011620 File Offset: 0x0000F820
			public override string GetText(MethodInfo method, object[] args)
			{
				return method.DeclaringType.Namespace;
			}

			// Token: 0x06000501 RID: 1281 RVA: 0x0001163D File Offset: 0x0000F83D
			public NamespaceFragment()
			{
			}
		}

		// Token: 0x020000AA RID: 170
		private class MethodFullNameFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x06000502 RID: 1282 RVA: 0x00011648 File Offset: 0x0000F848
			public override string GetText(MethodInfo method, object[] args)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(method.DeclaringType.FullName);
				stringBuilder.Append('.');
				stringBuilder.Append(method.Name);
				if (method.IsGenericMethod)
				{
					TestNameGenerator.NameFragment.AppendGenericTypeNames(stringBuilder, method);
				}
				return stringBuilder.ToString();
			}

			// Token: 0x06000503 RID: 1283 RVA: 0x000116A3 File Offset: 0x0000F8A3
			public MethodFullNameFragment()
			{
			}
		}

		// Token: 0x020000AB RID: 171
		private class ClassNameFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x06000504 RID: 1284 RVA: 0x000116AC File Offset: 0x0000F8AC
			public override string GetText(MethodInfo method, object[] args)
			{
				return method.DeclaringType.Name;
			}

			// Token: 0x06000505 RID: 1285 RVA: 0x000116C9 File Offset: 0x0000F8C9
			public ClassNameFragment()
			{
			}
		}

		// Token: 0x020000AC RID: 172
		private class ClassFullNameFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x06000506 RID: 1286 RVA: 0x000116D4 File Offset: 0x0000F8D4
			public override string GetText(MethodInfo method, object[] args)
			{
				return method.DeclaringType.FullName;
			}

			// Token: 0x06000507 RID: 1287 RVA: 0x000116F1 File Offset: 0x0000F8F1
			public ClassFullNameFragment()
			{
			}
		}

		// Token: 0x020000AD RID: 173
		private class ArgListFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x06000508 RID: 1288 RVA: 0x000116F9 File Offset: 0x0000F8F9
			public ArgListFragment(int maxStringLength)
			{
				this._maxStringLength = maxStringLength;
			}

			// Token: 0x06000509 RID: 1289 RVA: 0x0001170C File Offset: 0x0000F90C
			public override string GetText(MethodInfo method, object[] arglist)
			{
				StringBuilder stringBuilder = new StringBuilder();
				if (arglist != null)
				{
					stringBuilder.Append('(');
					for (int i = 0; i < arglist.Length; i++)
					{
						if (i > 0)
						{
							stringBuilder.Append(",");
						}
						stringBuilder.Append(TestNameGenerator.NameFragment.GetDisplayString(arglist[i], this._maxStringLength));
					}
					stringBuilder.Append(')');
				}
				return stringBuilder.ToString();
			}

			// Token: 0x040000F9 RID: 249
			private int _maxStringLength;
		}

		// Token: 0x020000AE RID: 174
		private class ArgumentFragment : TestNameGenerator.NameFragment
		{
			// Token: 0x0600050A RID: 1290 RVA: 0x00011786 File Offset: 0x0000F986
			public ArgumentFragment(int index, int maxStringLength)
			{
				this._index = index;
				this._maxStringLength = maxStringLength;
			}

			// Token: 0x0600050B RID: 1291 RVA: 0x000117A0 File Offset: 0x0000F9A0
			public override string GetText(MethodInfo method, object[] args)
			{
				return (this._index < args.Length) ? TestNameGenerator.NameFragment.GetDisplayString(args[this._index], this._maxStringLength) : string.Empty;
			}

			// Token: 0x040000FA RID: 250
			private int _index;

			// Token: 0x040000FB RID: 251
			private int _maxStringLength;
		}
	}
}
