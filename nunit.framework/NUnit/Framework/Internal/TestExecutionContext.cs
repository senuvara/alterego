﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;
using System.Security;
using System.Security.Principal;
using System.Threading;
using NUnit.Compatibility;
using NUnit.Framework.Constraints;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Execution;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000B8 RID: 184
	public class TestExecutionContext : LongLivedMarshalByRefObject, ITestExecutionContext
	{
		// Token: 0x06000566 RID: 1382 RVA: 0x00012AE4 File Offset: 0x00010CE4
		public TestExecutionContext()
		{
			this._priorContext = null;
			this.TestCaseTimeout = 0;
			this.UpstreamActions = new List<ITestAction>();
			this._currentCulture = CultureInfo.CurrentCulture;
			this._currentUICulture = CultureInfo.CurrentUICulture;
			this._currentPrincipal = Thread.CurrentPrincipal;
			this.CurrentValueFormatter = ((object val) => MsgUtils.DefaultValueFormatter(val));
			this.IsSingleThreaded = false;
		}

		// Token: 0x06000567 RID: 1383 RVA: 0x00012B70 File Offset: 0x00010D70
		public TestExecutionContext(TestExecutionContext other)
		{
			this._priorContext = other;
			this.CurrentTest = other.CurrentTest;
			this.CurrentResult = other.CurrentResult;
			this.TestObject = other.TestObject;
			this.WorkDirectory = other.WorkDirectory;
			this._listener = other._listener;
			this.StopOnError = other.StopOnError;
			this.TestCaseTimeout = other.TestCaseTimeout;
			this.UpstreamActions = new List<ITestAction>(other.UpstreamActions);
			this._currentCulture = other.CurrentCulture;
			this._currentUICulture = other.CurrentUICulture;
			this._currentPrincipal = other.CurrentPrincipal;
			this.CurrentValueFormatter = other.CurrentValueFormatter;
			this.Dispatcher = other.Dispatcher;
			this.ParallelScope = other.ParallelScope;
			this.IsSingleThreaded = other.IsSingleThreaded;
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x06000568 RID: 1384 RVA: 0x00012C5C File Offset: 0x00010E5C
		// (set) Token: 0x06000569 RID: 1385 RVA: 0x00012C94 File Offset: 0x00010E94
		public static ITestExecutionContext CurrentContext
		{
			[SecuritySafeCritical]
			get
			{
				TestExecutionContext testExecutionContext = TestExecutionContext.GetTestExecutionContext();
				if (testExecutionContext == null)
				{
					testExecutionContext = new TestExecutionContext();
					CallContext.SetData(TestExecutionContext.CONTEXT_KEY, testExecutionContext);
				}
				return testExecutionContext;
			}
			[SecuritySafeCritical]
			private set
			{
				if (value == null)
				{
					CallContext.FreeNamedDataSlot(TestExecutionContext.CONTEXT_KEY);
				}
				else
				{
					CallContext.SetData(TestExecutionContext.CONTEXT_KEY, value);
				}
			}
		}

		// Token: 0x0600056A RID: 1386 RVA: 0x00012CC8 File Offset: 0x00010EC8
		[SecuritySafeCritical]
		public static TestExecutionContext GetTestExecutionContext()
		{
			return CallContext.GetData(TestExecutionContext.CONTEXT_KEY) as TestExecutionContext;
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x00012CE9 File Offset: 0x00010EE9
		public static void ClearCurrentContext()
		{
			TestExecutionContext.CurrentContext = null;
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x0600056C RID: 1388 RVA: 0x00012CF4 File Offset: 0x00010EF4
		// (set) Token: 0x0600056D RID: 1389 RVA: 0x00012D0B File Offset: 0x00010F0B
		public Test CurrentTest
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentTest>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CurrentTest>k__BackingField = value;
			}
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x0600056E RID: 1390 RVA: 0x00012D14 File Offset: 0x00010F14
		// (set) Token: 0x0600056F RID: 1391 RVA: 0x00012D2B File Offset: 0x00010F2B
		public DateTime StartTime
		{
			[CompilerGenerated]
			get
			{
				return this.<StartTime>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StartTime>k__BackingField = value;
			}
		}

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000570 RID: 1392 RVA: 0x00012D34 File Offset: 0x00010F34
		// (set) Token: 0x06000571 RID: 1393 RVA: 0x00012D4B File Offset: 0x00010F4B
		public long StartTicks
		{
			[CompilerGenerated]
			get
			{
				return this.<StartTicks>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StartTicks>k__BackingField = value;
			}
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000572 RID: 1394 RVA: 0x00012D54 File Offset: 0x00010F54
		// (set) Token: 0x06000573 RID: 1395 RVA: 0x00012D6C File Offset: 0x00010F6C
		public TestResult CurrentResult
		{
			get
			{
				return this._currentResult;
			}
			set
			{
				this._currentResult = value;
				if (value != null)
				{
					this.OutWriter = value.OutWriter;
				}
			}
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06000574 RID: 1396 RVA: 0x00012D98 File Offset: 0x00010F98
		// (set) Token: 0x06000575 RID: 1397 RVA: 0x00012DAF File Offset: 0x00010FAF
		public TextWriter OutWriter
		{
			[CompilerGenerated]
			get
			{
				return this.<OutWriter>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<OutWriter>k__BackingField = value;
			}
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x06000576 RID: 1398 RVA: 0x00012DB8 File Offset: 0x00010FB8
		// (set) Token: 0x06000577 RID: 1399 RVA: 0x00012DCF File Offset: 0x00010FCF
		public object TestObject
		{
			[CompilerGenerated]
			get
			{
				return this.<TestObject>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TestObject>k__BackingField = value;
			}
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x06000578 RID: 1400 RVA: 0x00012DD8 File Offset: 0x00010FD8
		// (set) Token: 0x06000579 RID: 1401 RVA: 0x00012DEF File Offset: 0x00010FEF
		public string WorkDirectory
		{
			[CompilerGenerated]
			get
			{
				return this.<WorkDirectory>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<WorkDirectory>k__BackingField = value;
			}
		}

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x0600057A RID: 1402 RVA: 0x00012DF8 File Offset: 0x00010FF8
		// (set) Token: 0x0600057B RID: 1403 RVA: 0x00012E0F File Offset: 0x0001100F
		public bool StopOnError
		{
			[CompilerGenerated]
			get
			{
				return this.<StopOnError>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StopOnError>k__BackingField = value;
			}
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x0600057C RID: 1404 RVA: 0x00012E18 File Offset: 0x00011018
		// (set) Token: 0x0600057D RID: 1405 RVA: 0x00012E5C File Offset: 0x0001105C
		public TestExecutionStatus ExecutionStatus
		{
			get
			{
				if (this._executionStatus == TestExecutionStatus.Running && this._priorContext != null)
				{
					this._executionStatus = this._priorContext.ExecutionStatus;
				}
				return this._executionStatus;
			}
			set
			{
				this._executionStatus = value;
				if (this._priorContext != null)
				{
					this._priorContext.ExecutionStatus = value;
				}
			}
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x0600057E RID: 1406 RVA: 0x00012E8C File Offset: 0x0001108C
		// (set) Token: 0x0600057F RID: 1407 RVA: 0x00012EA4 File Offset: 0x000110A4
		internal ITestListener Listener
		{
			get
			{
				return this._listener;
			}
			set
			{
				this._listener = value;
			}
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x06000580 RID: 1408 RVA: 0x00012EB0 File Offset: 0x000110B0
		// (set) Token: 0x06000581 RID: 1409 RVA: 0x00012EC7 File Offset: 0x000110C7
		public IWorkItemDispatcher Dispatcher
		{
			[CompilerGenerated]
			get
			{
				return this.<Dispatcher>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Dispatcher>k__BackingField = value;
			}
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000582 RID: 1410 RVA: 0x00012ED0 File Offset: 0x000110D0
		// (set) Token: 0x06000583 RID: 1411 RVA: 0x00012EE7 File Offset: 0x000110E7
		public ParallelScope ParallelScope
		{
			[CompilerGenerated]
			get
			{
				return this.<ParallelScope>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ParallelScope>k__BackingField = value;
			}
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x06000584 RID: 1412 RVA: 0x00012EF0 File Offset: 0x000110F0
		// (set) Token: 0x06000585 RID: 1413 RVA: 0x00012F07 File Offset: 0x00011107
		public string WorkerId
		{
			[CompilerGenerated]
			get
			{
				return this.<WorkerId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<WorkerId>k__BackingField = value;
			}
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06000586 RID: 1414 RVA: 0x00012F10 File Offset: 0x00011110
		public Randomizer RandomGenerator
		{
			get
			{
				if (this._randomGenerator == null)
				{
					this._randomGenerator = new Randomizer(this.CurrentTest.Seed);
				}
				return this._randomGenerator;
			}
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x06000587 RID: 1415 RVA: 0x00012F50 File Offset: 0x00011150
		internal int AssertCount
		{
			get
			{
				return this._assertCount;
			}
		}

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x06000588 RID: 1416 RVA: 0x00012F68 File Offset: 0x00011168
		// (set) Token: 0x06000589 RID: 1417 RVA: 0x00012F7F File Offset: 0x0001117F
		public int TestCaseTimeout
		{
			[CompilerGenerated]
			get
			{
				return this.<TestCaseTimeout>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TestCaseTimeout>k__BackingField = value;
			}
		}

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x0600058A RID: 1418 RVA: 0x00012F88 File Offset: 0x00011188
		// (set) Token: 0x0600058B RID: 1419 RVA: 0x00012F9F File Offset: 0x0001119F
		public List<ITestAction> UpstreamActions
		{
			[CompilerGenerated]
			get
			{
				return this.<UpstreamActions>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<UpstreamActions>k__BackingField = value;
			}
		}

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x0600058C RID: 1420 RVA: 0x00012FA8 File Offset: 0x000111A8
		// (set) Token: 0x0600058D RID: 1421 RVA: 0x00012FC0 File Offset: 0x000111C0
		public CultureInfo CurrentCulture
		{
			get
			{
				return this._currentCulture;
			}
			set
			{
				this._currentCulture = value;
				Thread.CurrentThread.CurrentCulture = this._currentCulture;
			}
		}

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x0600058E RID: 1422 RVA: 0x00012FDC File Offset: 0x000111DC
		// (set) Token: 0x0600058F RID: 1423 RVA: 0x00012FF4 File Offset: 0x000111F4
		public CultureInfo CurrentUICulture
		{
			get
			{
				return this._currentUICulture;
			}
			set
			{
				this._currentUICulture = value;
				Thread.CurrentThread.CurrentUICulture = this._currentUICulture;
			}
		}

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06000590 RID: 1424 RVA: 0x00013010 File Offset: 0x00011210
		// (set) Token: 0x06000591 RID: 1425 RVA: 0x00013028 File Offset: 0x00011228
		public IPrincipal CurrentPrincipal
		{
			get
			{
				return this._currentPrincipal;
			}
			set
			{
				this._currentPrincipal = value;
				Thread.CurrentPrincipal = this._currentPrincipal;
			}
		}

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06000592 RID: 1426 RVA: 0x00013040 File Offset: 0x00011240
		// (set) Token: 0x06000593 RID: 1427 RVA: 0x00013057 File Offset: 0x00011257
		public ValueFormatter CurrentValueFormatter
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentValueFormatter>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<CurrentValueFormatter>k__BackingField = value;
			}
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06000594 RID: 1428 RVA: 0x00013060 File Offset: 0x00011260
		// (set) Token: 0x06000595 RID: 1429 RVA: 0x00013077 File Offset: 0x00011277
		public bool IsSingleThreaded
		{
			[CompilerGenerated]
			get
			{
				return this.<IsSingleThreaded>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsSingleThreaded>k__BackingField = value;
			}
		}

		// Token: 0x06000596 RID: 1430 RVA: 0x00013080 File Offset: 0x00011280
		public void UpdateContextFromEnvironment()
		{
			this._currentCulture = CultureInfo.CurrentCulture;
			this._currentUICulture = CultureInfo.CurrentUICulture;
			this._currentPrincipal = Thread.CurrentPrincipal;
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x000130A4 File Offset: 0x000112A4
		public void EstablishExecutionEnvironment()
		{
			Thread.CurrentThread.CurrentCulture = this._currentCulture;
			Thread.CurrentThread.CurrentUICulture = this._currentUICulture;
			Thread.CurrentPrincipal = this._currentPrincipal;
			TestExecutionContext.CurrentContext = this;
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x000130DC File Offset: 0x000112DC
		public void IncrementAssertCount()
		{
			Interlocked.Increment(ref this._assertCount);
		}

		// Token: 0x06000599 RID: 1433 RVA: 0x000130EC File Offset: 0x000112EC
		public void IncrementAssertCount(int count)
		{
			while (count-- > 0)
			{
				Interlocked.Increment(ref this._assertCount);
			}
		}

		// Token: 0x0600059A RID: 1434 RVA: 0x00013115 File Offset: 0x00011315
		public void AddFormatter(ValueFormatterFactory formatterFactory)
		{
			this.CurrentValueFormatter = formatterFactory(this.CurrentValueFormatter);
		}

		// Token: 0x0600059B RID: 1435 RVA: 0x0001312C File Offset: 0x0001132C
		[SecurityCritical]
		public override object InitializeLifetimeService()
		{
			return null;
		}

		// Token: 0x0600059C RID: 1436 RVA: 0x00012AC8 File Offset: 0x00010CC8
		[CompilerGenerated]
		private static string <.ctor>b__0(object val)
		{
			return MsgUtils.DefaultValueFormatter(val);
		}

		// Token: 0x0600059D RID: 1437 RVA: 0x0001313F File Offset: 0x0001133F
		// Note: this type is marked as 'beforefieldinit'.
		static TestExecutionContext()
		{
		}

		// Token: 0x04000111 RID: 273
		private TestExecutionContext _priorContext;

		// Token: 0x04000112 RID: 274
		private TestExecutionStatus _executionStatus;

		// Token: 0x04000113 RID: 275
		private ITestListener _listener = TestListener.NULL;

		// Token: 0x04000114 RID: 276
		private int _assertCount;

		// Token: 0x04000115 RID: 277
		private Randomizer _randomGenerator;

		// Token: 0x04000116 RID: 278
		private CultureInfo _currentCulture;

		// Token: 0x04000117 RID: 279
		private CultureInfo _currentUICulture;

		// Token: 0x04000118 RID: 280
		private TestResult _currentResult;

		// Token: 0x04000119 RID: 281
		private IPrincipal _currentPrincipal;

		// Token: 0x0400011A RID: 282
		private static readonly string CONTEXT_KEY = "NUnit.Framework.TestContext";

		// Token: 0x0400011B RID: 283
		[CompilerGenerated]
		private Test <CurrentTest>k__BackingField;

		// Token: 0x0400011C RID: 284
		[CompilerGenerated]
		private DateTime <StartTime>k__BackingField;

		// Token: 0x0400011D RID: 285
		[CompilerGenerated]
		private long <StartTicks>k__BackingField;

		// Token: 0x0400011E RID: 286
		[CompilerGenerated]
		private TextWriter <OutWriter>k__BackingField;

		// Token: 0x0400011F RID: 287
		[CompilerGenerated]
		private object <TestObject>k__BackingField;

		// Token: 0x04000120 RID: 288
		[CompilerGenerated]
		private string <WorkDirectory>k__BackingField;

		// Token: 0x04000121 RID: 289
		[CompilerGenerated]
		private bool <StopOnError>k__BackingField;

		// Token: 0x04000122 RID: 290
		[CompilerGenerated]
		private IWorkItemDispatcher <Dispatcher>k__BackingField;

		// Token: 0x04000123 RID: 291
		[CompilerGenerated]
		private ParallelScope <ParallelScope>k__BackingField;

		// Token: 0x04000124 RID: 292
		[CompilerGenerated]
		private string <WorkerId>k__BackingField;

		// Token: 0x04000125 RID: 293
		[CompilerGenerated]
		private int <TestCaseTimeout>k__BackingField;

		// Token: 0x04000126 RID: 294
		[CompilerGenerated]
		private List<ITestAction> <UpstreamActions>k__BackingField;

		// Token: 0x04000127 RID: 295
		[CompilerGenerated]
		private ValueFormatter <CurrentValueFormatter>k__BackingField;

		// Token: 0x04000128 RID: 296
		[CompilerGenerated]
		private bool <IsSingleThreaded>k__BackingField;

		// Token: 0x04000129 RID: 297
		[CompilerGenerated]
		private static ValueFormatter CS$<>9__CachedAnonymousMethodDelegate1;
	}
}
