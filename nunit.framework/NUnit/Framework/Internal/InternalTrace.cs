﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000041 RID: 65
	public static class InternalTrace
	{
		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060001E4 RID: 484 RVA: 0x000073D0 File Offset: 0x000055D0
		// (set) Token: 0x060001E5 RID: 485 RVA: 0x000073E6 File Offset: 0x000055E6
		public static bool Initialized
		{
			[CompilerGenerated]
			get
			{
				return InternalTrace.<Initialized>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				InternalTrace.<Initialized>k__BackingField = value;
			}
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x000073F0 File Offset: 0x000055F0
		public static void Initialize(string logName, InternalTraceLevel level)
		{
			if (!InternalTrace.Initialized)
			{
				InternalTrace.traceLevel = level;
				if (InternalTrace.traceWriter == null && InternalTrace.traceLevel > InternalTraceLevel.Off)
				{
					InternalTrace.traceWriter = new InternalTraceWriter(logName);
					InternalTrace.traceWriter.WriteLine("InternalTrace: Initializing at level {0}", InternalTrace.traceLevel);
				}
				InternalTrace.Initialized = true;
			}
			else
			{
				InternalTrace.traceWriter.WriteLine("InternalTrace: Ignoring attempted re-initialization at level {0}", level);
			}
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x00007470 File Offset: 0x00005670
		public static void Initialize(TextWriter writer, InternalTraceLevel level)
		{
			if (!InternalTrace.Initialized)
			{
				InternalTrace.traceLevel = level;
				if (InternalTrace.traceWriter == null && InternalTrace.traceLevel > InternalTraceLevel.Off)
				{
					InternalTrace.traceWriter = new InternalTraceWriter(writer);
					InternalTrace.traceWriter.WriteLine("InternalTrace: Initializing at level " + InternalTrace.traceLevel.ToString());
				}
				InternalTrace.Initialized = true;
			}
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x000074E0 File Offset: 0x000056E0
		public static Logger GetLogger(string name)
		{
			return new Logger(name, InternalTrace.traceLevel, InternalTrace.traceWriter);
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x00007504 File Offset: 0x00005704
		public static Logger GetLogger(Type type)
		{
			return InternalTrace.GetLogger(type.FullName);
		}

		// Token: 0x0400006A RID: 106
		private static InternalTraceLevel traceLevel;

		// Token: 0x0400006B RID: 107
		private static InternalTraceWriter traceWriter;

		// Token: 0x0400006C RID: 108
		[CompilerGenerated]
		private static bool <Initialized>k__BackingField;
	}
}
