﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000CD RID: 205
	public class PlatformHelper
	{
		// Token: 0x06000635 RID: 1589 RVA: 0x000158FF File Offset: 0x00013AFF
		public PlatformHelper()
		{
			this._os = OSPlatform.CurrentPlatform;
			this._rt = RuntimeFramework.CurrentFramework;
		}

		// Token: 0x06000636 RID: 1590 RVA: 0x0001592B File Offset: 0x00013B2B
		public PlatformHelper(OSPlatform os, RuntimeFramework rt)
		{
			this._os = os;
			this._rt = rt;
		}

		// Token: 0x06000637 RID: 1591 RVA: 0x00015950 File Offset: 0x00013B50
		public bool IsPlatformSupported(string[] platforms)
		{
			return platforms.Any(new Func<string, bool>(this.IsPlatformSupported));
		}

		// Token: 0x06000638 RID: 1592 RVA: 0x00015974 File Offset: 0x00013B74
		public bool IsPlatformSupported(PlatformAttribute platformAttribute)
		{
			string include = platformAttribute.Include;
			string exclude = platformAttribute.Exclude;
			return this.IsPlatformSupported(include, exclude);
		}

		// Token: 0x06000639 RID: 1593 RVA: 0x0001599C File Offset: 0x00013B9C
		public bool IsPlatformSupported(TestCaseAttribute testCaseAttribute)
		{
			string includePlatform = testCaseAttribute.IncludePlatform;
			string excludePlatform = testCaseAttribute.ExcludePlatform;
			return this.IsPlatformSupported(includePlatform, excludePlatform);
		}

		// Token: 0x0600063A RID: 1594 RVA: 0x000159C4 File Offset: 0x00013BC4
		private bool IsPlatformSupported(string include, string exclude)
		{
			try
			{
				if (include != null && !this.IsPlatformSupported(include))
				{
					this._reason = string.Format("Only supported on {0}", include);
					return false;
				}
				if (exclude != null && this.IsPlatformSupported(exclude))
				{
					this._reason = string.Format("Not supported on {0}", exclude);
					return false;
				}
			}
			catch (Exception ex)
			{
				this._reason = ex.Message;
				return false;
			}
			return true;
		}

		// Token: 0x0600063B RID: 1595 RVA: 0x00015A50 File Offset: 0x00013C50
		public bool IsPlatformSupported(string platform)
		{
			bool result;
			if (platform.IndexOf(',') >= 0)
			{
				result = this.IsPlatformSupported(platform.Split(new char[]
				{
					','
				}));
			}
			else
			{
				string text = platform.Trim();
				string text2 = text.ToUpper();
				bool flag;
				switch (text2)
				{
				case "WIN":
				case "WIN32":
					flag = this._os.IsWindows;
					goto IL_4F7;
				case "WIN32S":
					flag = this._os.IsWin32S;
					goto IL_4F7;
				case "WIN32WINDOWS":
					flag = this._os.IsWin32Windows;
					goto IL_4F7;
				case "WIN32NT":
					flag = this._os.IsWin32NT;
					goto IL_4F7;
				case "WINCE":
					flag = this._os.IsWinCE;
					goto IL_4F7;
				case "WIN95":
					flag = this._os.IsWin95;
					goto IL_4F7;
				case "WIN98":
					flag = this._os.IsWin98;
					goto IL_4F7;
				case "WINME":
					flag = this._os.IsWinME;
					goto IL_4F7;
				case "NT3":
					flag = this._os.IsNT3;
					goto IL_4F7;
				case "NT4":
					flag = this._os.IsNT4;
					goto IL_4F7;
				case "NT5":
					flag = this._os.IsNT5;
					goto IL_4F7;
				case "WIN2K":
					flag = this._os.IsWin2K;
					goto IL_4F7;
				case "WINXP":
					flag = this._os.IsWinXP;
					goto IL_4F7;
				case "WIN2003SERVER":
					flag = this._os.IsWin2003Server;
					goto IL_4F7;
				case "NT6":
					flag = this._os.IsNT6;
					goto IL_4F7;
				case "VISTA":
					flag = this._os.IsVista;
					goto IL_4F7;
				case "WIN2008SERVER":
					flag = this._os.IsWin2008Server;
					goto IL_4F7;
				case "WIN2008SERVERR2":
					flag = this._os.IsWin2008ServerR2;
					goto IL_4F7;
				case "WIN2012SERVER":
					flag = (this._os.IsWin2012ServerR1 || this._os.IsWin2012ServerR2);
					goto IL_4F7;
				case "WIN2012SERVERR2":
					flag = this._os.IsWin2012ServerR2;
					goto IL_4F7;
				case "WIN7":
				case "WINDOWS7":
					flag = this._os.IsWindows7;
					goto IL_4F7;
				case "WINDOWS8":
				case "WIN8":
					flag = this._os.IsWindows8;
					goto IL_4F7;
				case "WINDOWS8.1":
				case "WIN8.1":
					flag = this._os.IsWindows81;
					goto IL_4F7;
				case "WINDOWS10":
				case "WIN10":
					flag = this._os.IsWindows10;
					goto IL_4F7;
				case "WINDOWSSERVER10":
					flag = this._os.IsWindowsServer10;
					goto IL_4F7;
				case "UNIX":
				case "LINUX":
					flag = this._os.IsUnix;
					goto IL_4F7;
				case "XBOX":
					flag = this._os.IsXbox;
					goto IL_4F7;
				case "MACOSX":
					flag = this._os.IsMacOSX;
					goto IL_4F7;
				case "64-BIT":
				case "64-BIT-PROCESS":
					flag = (IntPtr.Size == 8);
					goto IL_4F7;
				case "32-BIT":
				case "32-BIT-PROCESS":
					flag = (IntPtr.Size == 4);
					goto IL_4F7;
				}
				flag = this.IsRuntimeSupported(text);
				IL_4F7:
				if (!flag)
				{
					this._reason = "Only supported on " + platform;
				}
				result = flag;
			}
			return result;
		}

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x0600063C RID: 1596 RVA: 0x00015F70 File Offset: 0x00014170
		public string Reason
		{
			get
			{
				return this._reason;
			}
		}

		// Token: 0x0600063D RID: 1597 RVA: 0x00015F88 File Offset: 0x00014188
		private bool IsRuntimeSupported(string platformName)
		{
			string versionSpecification = null;
			string[] array = platformName.Split(new char[]
			{
				'-'
			});
			if (array.Length == 2)
			{
				platformName = array[0];
				versionSpecification = array[1];
			}
			string text = platformName.ToUpper();
			if (text != null)
			{
				if (<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x6000626-1 == null)
				{
					<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x6000626-1 = new Dictionary<string, int>(8)
					{
						{
							"NET",
							0
						},
						{
							"NETCF",
							1
						},
						{
							"SSCLI",
							2
						},
						{
							"ROTOR",
							3
						},
						{
							"MONO",
							4
						},
						{
							"SL",
							5
						},
						{
							"SILVERLIGHT",
							6
						},
						{
							"MONOTOUCH",
							7
						}
					};
				}
				int num;
				if (<PrivateImplementationDetails>{59819BE1-42C3-4115-ADE6-88F6962021F1}.$$method0x6000626-1.TryGetValue(text, out num))
				{
					bool result;
					switch (num)
					{
					case 0:
						result = this.IsRuntimeSupported(RuntimeType.Net, versionSpecification);
						break;
					case 1:
						result = this.IsRuntimeSupported(RuntimeType.NetCF, versionSpecification);
						break;
					case 2:
					case 3:
						result = this.IsRuntimeSupported(RuntimeType.SSCLI, versionSpecification);
						break;
					case 4:
						result = this.IsRuntimeSupported(RuntimeType.Mono, versionSpecification);
						break;
					case 5:
					case 6:
						result = this.IsRuntimeSupported(RuntimeType.Silverlight, versionSpecification);
						break;
					case 7:
						result = this.IsRuntimeSupported(RuntimeType.MonoTouch, versionSpecification);
						break;
					default:
						goto IL_133;
					}
					return result;
				}
			}
			IL_133:
			throw new ArgumentException("Invalid platform name", platformName);
		}

		// Token: 0x0600063E RID: 1598 RVA: 0x000160D8 File Offset: 0x000142D8
		private bool IsRuntimeSupported(RuntimeType runtime, string versionSpecification)
		{
			Version version = (versionSpecification == null) ? RuntimeFramework.DefaultVersion : new Version(versionSpecification);
			RuntimeFramework target = new RuntimeFramework(runtime, version);
			return this._rt.Supports(target);
		}

		// Token: 0x0600063F RID: 1599 RVA: 0x0001610F File Offset: 0x0001430F
		// Note: this type is marked as 'beforefieldinit'.
		static PlatformHelper()
		{
		}

		// Token: 0x0400014E RID: 334
		private const string CommonOSPlatforms = "Win,Win32,Win32S,Win32NT,Win32Windows,WinCE,Win95,Win98,WinMe,NT3,NT4,NT5,NT6,Win2008Server,Win2008ServerR2,Win2012Server,Win2012ServerR2,Win2K,WinXP,Win2003Server,Vista,Win7,Windows7,Win8,Windows8,Win8.1,Windows8.1,Win10,Windows10,WindowsServer10,Unix,Linux";

		// Token: 0x0400014F RID: 335
		public const string OSPlatforms = "Win,Win32,Win32S,Win32NT,Win32Windows,WinCE,Win95,Win98,WinMe,NT3,NT4,NT5,NT6,Win2008Server,Win2008ServerR2,Win2012Server,Win2012ServerR2,Win2K,WinXP,Win2003Server,Vista,Win7,Windows7,Win8,Windows8,Win8.1,Windows8.1,Win10,Windows10,WindowsServer10,Unix,Linux,Xbox,MacOSX";

		// Token: 0x04000150 RID: 336
		private readonly OSPlatform _os;

		// Token: 0x04000151 RID: 337
		private readonly RuntimeFramework _rt;

		// Token: 0x04000152 RID: 338
		private string _reason = string.Empty;

		// Token: 0x04000153 RID: 339
		public static readonly string RuntimePlatforms = "Net,NetCF,SSCLI,Rotor,Mono,MonoTouch";
	}
}
