﻿using System;
using System.IO;
using System.Text;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000DE RID: 222
	public class InternalTraceWriter : TextWriter
	{
		// Token: 0x0600068A RID: 1674 RVA: 0x000173B8 File Offset: 0x000155B8
		public InternalTraceWriter(string logPath)
		{
			this.writer = new StreamWriter(new FileStream(logPath, FileMode.Append, FileAccess.Write, FileShare.Write))
			{
				AutoFlush = true
			};
		}

		// Token: 0x0600068B RID: 1675 RVA: 0x000173F7 File Offset: 0x000155F7
		public InternalTraceWriter(TextWriter writer)
		{
			this.writer = writer;
		}

		// Token: 0x17000177 RID: 375
		// (get) Token: 0x0600068C RID: 1676 RVA: 0x00017414 File Offset: 0x00015614
		public override Encoding Encoding
		{
			get
			{
				return this.writer.Encoding;
			}
		}

		// Token: 0x0600068D RID: 1677 RVA: 0x00017434 File Offset: 0x00015634
		public override void Write(char value)
		{
			lock (this.myLock)
			{
				this.writer.Write(value);
			}
		}

		// Token: 0x0600068E RID: 1678 RVA: 0x0001747C File Offset: 0x0001567C
		public override void Write(string value)
		{
			lock (this.myLock)
			{
				base.Write(value);
			}
		}

		// Token: 0x0600068F RID: 1679 RVA: 0x000174C0 File Offset: 0x000156C0
		public override void WriteLine(string value)
		{
			this.writer.WriteLine(value);
		}

		// Token: 0x06000690 RID: 1680 RVA: 0x000174D0 File Offset: 0x000156D0
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.writer != null)
			{
				this.writer.Flush();
				this.writer.Dispose();
				this.writer = null;
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000691 RID: 1681 RVA: 0x0001751C File Offset: 0x0001571C
		public override void Flush()
		{
			if (this.writer != null)
			{
				this.writer.Flush();
			}
		}

		// Token: 0x0400015D RID: 349
		private TextWriter writer;

		// Token: 0x0400015E RID: 350
		private object myLock = new object();
	}
}
