﻿using System;
using System.Runtime.Serialization;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000024 RID: 36
	[Serializable]
	public class NUnitException : Exception
	{
		// Token: 0x0600015C RID: 348 RVA: 0x000053C0 File Offset: 0x000035C0
		public NUnitException()
		{
		}

		// Token: 0x0600015D RID: 349 RVA: 0x000053CB File Offset: 0x000035CB
		public NUnitException(string message) : base(message)
		{
		}

		// Token: 0x0600015E RID: 350 RVA: 0x000053D7 File Offset: 0x000035D7
		public NUnitException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x0600015F RID: 351 RVA: 0x000053E4 File Offset: 0x000035E4
		protected NUnitException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
