﻿using System;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000F1 RID: 241
	public class TestFixtureParameters : TestParameters, ITestFixtureData, ITestData
	{
		// Token: 0x060006E0 RID: 1760 RVA: 0x00019634 File Offset: 0x00017834
		public TestFixtureParameters()
		{
		}

		// Token: 0x060006E1 RID: 1761 RVA: 0x0001963F File Offset: 0x0001783F
		public TestFixtureParameters(Exception exception) : base(exception)
		{
		}

		// Token: 0x060006E2 RID: 1762 RVA: 0x0001964B File Offset: 0x0001784B
		public TestFixtureParameters(params object[] args) : base(args)
		{
		}

		// Token: 0x060006E3 RID: 1763 RVA: 0x00019657 File Offset: 0x00017857
		public TestFixtureParameters(ITestFixtureData data) : base(data)
		{
			this.TypeArgs = data.TypeArgs;
		}

		// Token: 0x1700017F RID: 383
		// (get) Token: 0x060006E4 RID: 1764 RVA: 0x00019670 File Offset: 0x00017870
		// (set) Token: 0x060006E5 RID: 1765 RVA: 0x00019687 File Offset: 0x00017887
		public Type[] TypeArgs
		{
			[CompilerGenerated]
			get
			{
				return this.<TypeArgs>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<TypeArgs>k__BackingField = value;
			}
		}

		// Token: 0x04000188 RID: 392
		[CompilerGenerated]
		private Type[] <TypeArgs>k__BackingField;
	}
}
