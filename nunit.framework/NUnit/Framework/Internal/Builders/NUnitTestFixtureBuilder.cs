﻿using System;
using System.Reflection;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x0200002A RID: 42
	public class NUnitTestFixtureBuilder
	{
		// Token: 0x06000175 RID: 373 RVA: 0x000056A8 File Offset: 0x000038A8
		public TestSuite BuildFrom(ITypeInfo typeInfo)
		{
			TestFixture testFixture = new TestFixture(typeInfo);
			if (testFixture.RunState != RunState.NotRunnable)
			{
				NUnitTestFixtureBuilder.CheckTestFixtureIsValid(testFixture);
			}
			testFixture.ApplyAttributesToTest(typeInfo.Type.GetTypeInfo());
			this.AddTestCasesToFixture(testFixture);
			return testFixture;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x000056F0 File Offset: 0x000038F0
		public TestSuite BuildFrom(ITypeInfo typeInfo, ITestFixtureData testFixtureData)
		{
			Guard.ArgumentNotNull(testFixtureData, "testFixtureData");
			object[] array = testFixtureData.Arguments;
			if (typeInfo.ContainsGenericParameters)
			{
				Type[] array2 = testFixtureData.TypeArgs;
				if (array2.Length == 0)
				{
					int num = 0;
					foreach (object obj in array)
					{
						if (!(obj is Type))
						{
							break;
						}
						num++;
					}
					array2 = new Type[num];
					for (int j = 0; j < num; j++)
					{
						array2[j] = (Type)array[j];
					}
					if (num > 0)
					{
						object[] array4 = new object[array.Length - num];
						for (int j = 0; j < array4.Length; j++)
						{
							array4[j] = array[num + j];
						}
						array = array4;
					}
				}
				if (array2.Length > 0 || TypeHelper.CanDeduceTypeArgsFromArgs(typeInfo.Type, array, ref array2))
				{
					typeInfo = typeInfo.MakeGenericType(array2);
				}
			}
			TestFixture testFixture = new TestFixture(typeInfo);
			if (array != null && array.Length > 0)
			{
				string text = testFixture.Name = typeInfo.GetDisplayName(array);
				string @namespace = typeInfo.Namespace;
				testFixture.FullName = ((@namespace != null && @namespace != "") ? (@namespace + "." + text) : text);
				testFixture.Arguments = array;
			}
			if (testFixture.RunState != RunState.NotRunnable)
			{
				testFixture.RunState = testFixtureData.RunState;
			}
			foreach (string key in testFixtureData.Properties.Keys)
			{
				foreach (object value in testFixtureData.Properties[key])
				{
					testFixture.Properties.Add(key, value);
				}
			}
			if (testFixture.RunState != RunState.NotRunnable)
			{
				NUnitTestFixtureBuilder.CheckTestFixtureIsValid(testFixture);
			}
			testFixture.ApplyAttributesToTest(typeInfo.Type.GetTypeInfo());
			this.AddTestCasesToFixture(testFixture);
			return testFixture;
		}

		// Token: 0x06000177 RID: 375 RVA: 0x000059A0 File Offset: 0x00003BA0
		private void AddTestCasesToFixture(TestFixture fixture)
		{
			if (fixture.TypeInfo.ContainsGenericParameters)
			{
				fixture.RunState = RunState.NotRunnable;
				fixture.Properties.Set("_SKIPREASON", NUnitTestFixtureBuilder.NO_TYPE_ARGS_MSG);
			}
			else
			{
				IMethodInfo[] methods = fixture.TypeInfo.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
				foreach (IMethodInfo method in methods)
				{
					Test test = this.BuildTestCase(method, fixture);
					if (test != null)
					{
						fixture.Add(test);
					}
				}
			}
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00005A30 File Offset: 0x00003C30
		private Test BuildTestCase(IMethodInfo method, TestSuite suite)
		{
			return this._testBuilder.CanBuildFrom(method, suite) ? this._testBuilder.BuildFrom(method, suite) : null;
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00005A64 File Offset: 0x00003C64
		private static void CheckTestFixtureIsValid(TestFixture fixture)
		{
			if (fixture.TypeInfo.ContainsGenericParameters)
			{
				fixture.RunState = RunState.NotRunnable;
				fixture.Properties.Set("_SKIPREASON", NUnitTestFixtureBuilder.NO_TYPE_ARGS_MSG);
			}
			else if (!fixture.TypeInfo.IsStaticClass)
			{
				Type[] typeArray = Reflect.GetTypeArray(fixture.Arguments);
				if (!fixture.TypeInfo.HasConstructor(typeArray))
				{
					fixture.RunState = RunState.NotRunnable;
					fixture.Properties.Set("_SKIPREASON", "No suitable constructor was found");
				}
			}
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00005AF4 File Offset: 0x00003CF4
		private static bool IsStaticClass(Type type)
		{
			return type.GetTypeInfo().IsAbstract && type.GetTypeInfo().IsSealed;
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00005B2D File Offset: 0x00003D2D
		public NUnitTestFixtureBuilder()
		{
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00005B21 File Offset: 0x00003D21
		// Note: this type is marked as 'beforefieldinit'.
		static NUnitTestFixtureBuilder()
		{
		}

		// Token: 0x04000043 RID: 67
		private static readonly string NO_TYPE_ARGS_MSG = "Fixture type contains generic parameters. You must either provide Type arguments or specify constructor arguments that allow NUnit to deduce the Type arguments.";

		// Token: 0x04000044 RID: 68
		private ITestCaseBuilder _testBuilder = new DefaultTestCaseBuilder();
	}
}
