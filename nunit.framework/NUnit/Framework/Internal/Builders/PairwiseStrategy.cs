﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x0200004A RID: 74
	public class PairwiseStrategy : ICombiningStrategy
	{
		// Token: 0x06000251 RID: 593 RVA: 0x00007FEC File Offset: 0x000061EC
		public IEnumerable<ITestCaseData> GetTestCases(IEnumerable[] sources)
		{
			List<ITestCaseData> list = new List<ITestCaseData>();
			List<object>[] array = this.CreateValueSet(sources);
			int[] dimensions = this.CreateDimensions(array);
			IEnumerable testCases = new PairwiseStrategy.PairwiseTestCaseGenerator().GetTestCases(dimensions);
			foreach (object obj in testCases)
			{
				PairwiseStrategy.TestCaseInfo testCaseInfo = (PairwiseStrategy.TestCaseInfo)obj;
				object[] array2 = new object[testCaseInfo.Features.Length];
				for (int i = 0; i < testCaseInfo.Features.Length; i++)
				{
					array2[i] = array[i][testCaseInfo.Features[i]];
				}
				TestCaseParameters item = new TestCaseParameters(array2);
				list.Add(item);
			}
			return list;
		}

		// Token: 0x06000252 RID: 594 RVA: 0x000080D8 File Offset: 0x000062D8
		private List<object>[] CreateValueSet(IEnumerable[] sources)
		{
			List<object>[] array = new List<object>[sources.Length];
			for (int i = 0; i < array.Length; i++)
			{
				List<object> list = new List<object>();
				foreach (object item in sources[i])
				{
					list.Add(item);
				}
				array[i] = list;
			}
			return array;
		}

		// Token: 0x06000253 RID: 595 RVA: 0x00008174 File Offset: 0x00006374
		private int[] CreateDimensions(List<object>[] valueSet)
		{
			int[] array = new int[valueSet.Length];
			for (int i = 0; i < valueSet.Length; i++)
			{
				array[i] = valueSet[i].Count;
			}
			return array;
		}

		// Token: 0x06000254 RID: 596 RVA: 0x000081AF File Offset: 0x000063AF
		public PairwiseStrategy()
		{
		}

		// Token: 0x0200004B RID: 75
		internal class FleaRand
		{
			// Token: 0x06000255 RID: 597 RVA: 0x000081B8 File Offset: 0x000063B8
			public FleaRand(uint seed)
			{
				this._b = seed;
				this._c = seed;
				this._d = seed;
				this._z = seed;
				this._m = new uint[256];
				this._r = new uint[256];
				for (int i = 0; i < this._m.Length; i++)
				{
					this._m[i] = seed;
				}
				for (int i = 0; i < 10; i++)
				{
					this.Batch();
				}
				this._q = 0U;
			}

			// Token: 0x06000256 RID: 598 RVA: 0x00008250 File Offset: 0x00006450
			public uint Next()
			{
				if (this._q == 0U)
				{
					this.Batch();
					this._q = (uint)(this._r.Length - 1);
				}
				else
				{
					this._q -= 1U;
				}
				return this._r[(int)((UIntPtr)this._q)];
			}

			// Token: 0x06000257 RID: 599 RVA: 0x000082AC File Offset: 0x000064AC
			private void Batch()
			{
				uint num = this._b;
				uint num2 = this._c + (this._z += 1U);
				uint num3 = this._d;
				for (int i = 0; i < this._r.Length; i++)
				{
					uint num4;
					checked
					{
						num4 = this._m[(int)((IntPtr)(unchecked((ulong)num % (ulong)((long)this._m.Length))))];
						this._m[(int)((IntPtr)(unchecked((ulong)num % (ulong)((long)this._m.Length))))] = num3;
					}
					num3 = (num2 << 19) + (num2 >> 13) + num;
					num2 = (num ^ this._m[i]);
					num = num4 + num3;
					this._r[i] = num2;
				}
				this._b = num;
				this._c = num2;
				this._d = num3;
			}

			// Token: 0x0400007A RID: 122
			private uint _b;

			// Token: 0x0400007B RID: 123
			private uint _c;

			// Token: 0x0400007C RID: 124
			private uint _d;

			// Token: 0x0400007D RID: 125
			private uint _z;

			// Token: 0x0400007E RID: 126
			private uint[] _m;

			// Token: 0x0400007F RID: 127
			private uint[] _r;

			// Token: 0x04000080 RID: 128
			private uint _q;
		}

		// Token: 0x0200004C RID: 76
		internal class FeatureInfo
		{
			// Token: 0x06000258 RID: 600 RVA: 0x00008367 File Offset: 0x00006567
			public FeatureInfo(int dimension, int feature)
			{
				this.Dimension = dimension;
				this.Feature = feature;
			}

			// Token: 0x04000081 RID: 129
			public readonly int Dimension;

			// Token: 0x04000082 RID: 130
			public readonly int Feature;
		}

		// Token: 0x0200004D RID: 77
		internal class FeatureTuple
		{
			// Token: 0x06000259 RID: 601 RVA: 0x00008380 File Offset: 0x00006580
			public FeatureTuple(PairwiseStrategy.FeatureInfo feature1)
			{
				this._features = new PairwiseStrategy.FeatureInfo[]
				{
					feature1
				};
			}

			// Token: 0x0600025A RID: 602 RVA: 0x000083A8 File Offset: 0x000065A8
			public FeatureTuple(PairwiseStrategy.FeatureInfo feature1, PairwiseStrategy.FeatureInfo feature2)
			{
				this._features = new PairwiseStrategy.FeatureInfo[]
				{
					feature1,
					feature2
				};
			}

			// Token: 0x170000AA RID: 170
			// (get) Token: 0x0600025B RID: 603 RVA: 0x000083D4 File Offset: 0x000065D4
			public int Length
			{
				get
				{
					return this._features.Length;
				}
			}

			// Token: 0x170000AB RID: 171
			public PairwiseStrategy.FeatureInfo this[int index]
			{
				get
				{
					return this._features[index];
				}
			}

			// Token: 0x04000083 RID: 131
			private readonly PairwiseStrategy.FeatureInfo[] _features;
		}

		// Token: 0x0200004E RID: 78
		internal class TestCaseInfo
		{
			// Token: 0x0600025D RID: 605 RVA: 0x0000840A File Offset: 0x0000660A
			public TestCaseInfo(int length)
			{
				this.Features = new int[length];
			}

			// Token: 0x0600025E RID: 606 RVA: 0x00008424 File Offset: 0x00006624
			public bool IsTupleCovered(PairwiseStrategy.FeatureTuple tuple)
			{
				for (int i = 0; i < tuple.Length; i++)
				{
					if (this.Features[tuple[i].Dimension] != tuple[i].Feature)
					{
						return false;
					}
				}
				return true;
			}

			// Token: 0x04000084 RID: 132
			public readonly int[] Features;
		}

		// Token: 0x0200004F RID: 79
		internal class PairwiseTestCaseGenerator
		{
			// Token: 0x0600025F RID: 607 RVA: 0x00008478 File Offset: 0x00006678
			public IEnumerable GetTestCases(int[] dimensions)
			{
				this._prng = new PairwiseStrategy.FleaRand(15485863U);
				this._dimensions = dimensions;
				this.CreateAllTuples();
				List<PairwiseStrategy.TestCaseInfo> list = new List<PairwiseStrategy.TestCaseInfo>();
				for (;;)
				{
					PairwiseStrategy.FeatureTuple nextTuple = this.GetNextTuple();
					if (nextTuple == null)
					{
						break;
					}
					PairwiseStrategy.TestCaseInfo testCaseInfo = this.CreateTestCase(nextTuple);
					this.RemoveTuplesCoveredByTest(testCaseInfo);
					list.Add(testCaseInfo);
				}
				this.SelfTest(list);
				return list;
			}

			// Token: 0x06000260 RID: 608 RVA: 0x000084F0 File Offset: 0x000066F0
			private int GetNextRandomNumber()
			{
				return (int)(this._prng.Next() >> 1);
			}

			// Token: 0x06000261 RID: 609 RVA: 0x00008510 File Offset: 0x00006710
			private void CreateAllTuples()
			{
				this._uncoveredTuples = new List<PairwiseStrategy.FeatureTuple>[this._dimensions.Length][];
				for (int i = 0; i < this._dimensions.Length; i++)
				{
					this._uncoveredTuples[i] = new List<PairwiseStrategy.FeatureTuple>[this._dimensions[i]];
					for (int j = 0; j < this._dimensions[i]; j++)
					{
						this._uncoveredTuples[i][j] = this.CreateTuples(i, j);
					}
				}
			}

			// Token: 0x06000262 RID: 610 RVA: 0x0000858C File Offset: 0x0000678C
			private List<PairwiseStrategy.FeatureTuple> CreateTuples(int dimension, int feature)
			{
				List<PairwiseStrategy.FeatureTuple> list = new List<PairwiseStrategy.FeatureTuple>();
				list.Add(new PairwiseStrategy.FeatureTuple(new PairwiseStrategy.FeatureInfo(dimension, feature)));
				for (int i = 0; i < this._dimensions.Length; i++)
				{
					if (i != dimension)
					{
						for (int j = 0; j < this._dimensions[i]; j++)
						{
							list.Add(new PairwiseStrategy.FeatureTuple(new PairwiseStrategy.FeatureInfo(dimension, feature), new PairwiseStrategy.FeatureInfo(i, j)));
						}
					}
				}
				return list;
			}

			// Token: 0x06000263 RID: 611 RVA: 0x00008614 File Offset: 0x00006814
			private PairwiseStrategy.FeatureTuple GetNextTuple()
			{
				for (int i = 0; i < this._uncoveredTuples.Length; i++)
				{
					for (int j = 0; j < this._uncoveredTuples[i].Length; j++)
					{
						List<PairwiseStrategy.FeatureTuple> list = this._uncoveredTuples[i][j];
						if (list.Count > 0)
						{
							PairwiseStrategy.FeatureTuple result = list[0];
							list.RemoveAt(0);
							return result;
						}
					}
				}
				return null;
			}

			// Token: 0x06000264 RID: 612 RVA: 0x00008694 File Offset: 0x00006894
			private PairwiseStrategy.TestCaseInfo CreateTestCase(PairwiseStrategy.FeatureTuple tuple)
			{
				PairwiseStrategy.TestCaseInfo result = null;
				int num = -1;
				for (int i = 0; i < 7; i++)
				{
					PairwiseStrategy.TestCaseInfo testCaseInfo = this.CreateRandomTestCase(tuple);
					int num2 = this.MaximizeCoverage(testCaseInfo, tuple);
					if (num2 > num)
					{
						result = testCaseInfo;
						num = num2;
					}
				}
				return result;
			}

			// Token: 0x06000265 RID: 613 RVA: 0x000086E8 File Offset: 0x000068E8
			private PairwiseStrategy.TestCaseInfo CreateRandomTestCase(PairwiseStrategy.FeatureTuple tuple)
			{
				PairwiseStrategy.TestCaseInfo testCaseInfo = new PairwiseStrategy.TestCaseInfo(this._dimensions.Length);
				for (int i = 0; i < this._dimensions.Length; i++)
				{
					testCaseInfo.Features[i] = this.GetNextRandomNumber() % this._dimensions[i];
				}
				for (int j = 0; j < tuple.Length; j++)
				{
					testCaseInfo.Features[tuple[j].Dimension] = tuple[j].Feature;
				}
				return testCaseInfo;
			}

			// Token: 0x06000266 RID: 614 RVA: 0x00008774 File Offset: 0x00006974
			private int MaximizeCoverage(PairwiseStrategy.TestCaseInfo testCase, PairwiseStrategy.FeatureTuple tuple)
			{
				int num = 1;
				int[] mutableDimensions = this.GetMutableDimensions(tuple);
				bool flag;
				do
				{
					flag = false;
					this.ScrambleDimensions(mutableDimensions);
					foreach (int num2 in mutableDimensions)
					{
						int num3 = this.CountTuplesCoveredByTest(testCase, num2, testCase.Features[num2]);
						int num4 = this.MaximizeCoverageForDimension(testCase, num2, num3);
						num += num4;
						if (num4 > num3)
						{
							flag = true;
						}
					}
				}
				while (flag);
				return num;
			}

			// Token: 0x06000267 RID: 615 RVA: 0x00008804 File Offset: 0x00006A04
			private int[] GetMutableDimensions(PairwiseStrategy.FeatureTuple tuple)
			{
				List<int> list = new List<int>();
				bool[] array = new bool[this._dimensions.Length];
				for (int i = 0; i < tuple.Length; i++)
				{
					array[tuple[i].Dimension] = true;
				}
				for (int j = 0; j < this._dimensions.Length; j++)
				{
					if (!array[j])
					{
						list.Add(j);
					}
				}
				return list.ToArray();
			}

			// Token: 0x06000268 RID: 616 RVA: 0x00008888 File Offset: 0x00006A88
			private void ScrambleDimensions(int[] dimensions)
			{
				for (int i = 0; i < dimensions.Length; i++)
				{
					int num = this.GetNextRandomNumber() % dimensions.Length;
					int num2 = dimensions[i];
					dimensions[i] = dimensions[num];
					dimensions[num] = num2;
				}
			}

			// Token: 0x06000269 RID: 617 RVA: 0x000088C4 File Offset: 0x00006AC4
			private int MaximizeCoverageForDimension(PairwiseStrategy.TestCaseInfo testCase, int dimension, int bestCoverage)
			{
				List<int> list = new List<int>(this._dimensions[dimension]);
				for (int i = 0; i < this._dimensions[dimension]; i++)
				{
					testCase.Features[dimension] = i;
					int num = this.CountTuplesCoveredByTest(testCase, dimension, i);
					if (num >= bestCoverage)
					{
						if (num > bestCoverage)
						{
							bestCoverage = num;
							list.Clear();
						}
						list.Add(i);
					}
				}
				testCase.Features[dimension] = list[this.GetNextRandomNumber() % list.Count];
				return bestCoverage;
			}

			// Token: 0x0600026A RID: 618 RVA: 0x0000895C File Offset: 0x00006B5C
			private int CountTuplesCoveredByTest(PairwiseStrategy.TestCaseInfo testCase, int dimension, int feature)
			{
				int num = 0;
				List<PairwiseStrategy.FeatureTuple> list = this._uncoveredTuples[dimension][feature];
				for (int i = 0; i < list.Count; i++)
				{
					if (testCase.IsTupleCovered(list[i]))
					{
						num++;
					}
				}
				return num;
			}

			// Token: 0x0600026B RID: 619 RVA: 0x000089B4 File Offset: 0x00006BB4
			private void RemoveTuplesCoveredByTest(PairwiseStrategy.TestCaseInfo testCase)
			{
				for (int i = 0; i < this._uncoveredTuples.Length; i++)
				{
					for (int j = 0; j < this._uncoveredTuples[i].Length; j++)
					{
						List<PairwiseStrategy.FeatureTuple> list = this._uncoveredTuples[i][j];
						for (int k = list.Count - 1; k >= 0; k--)
						{
							if (testCase.IsTupleCovered(list[k]))
							{
								list.RemoveAt(k);
							}
						}
					}
				}
			}

			// Token: 0x0600026C RID: 620 RVA: 0x00008A44 File Offset: 0x00006C44
			private void SelfTest(List<PairwiseStrategy.TestCaseInfo> testCases)
			{
				for (int i = 0; i < this._dimensions.Length - 1; i++)
				{
					for (int j = i + 1; j < this._dimensions.Length; j++)
					{
						for (int k = 0; k < this._dimensions[i]; k++)
						{
							for (int l = 0; l < this._dimensions[j]; l++)
							{
								PairwiseStrategy.FeatureTuple featureTuple = new PairwiseStrategy.FeatureTuple(new PairwiseStrategy.FeatureInfo(i, k), new PairwiseStrategy.FeatureInfo(j, l));
								if (!this.IsTupleCovered(testCases, featureTuple))
								{
									throw new InvalidOperationException(string.Format("PairwiseStrategy : Not all pairs are covered : {0}", featureTuple.ToString()));
								}
							}
						}
					}
				}
			}

			// Token: 0x0600026D RID: 621 RVA: 0x00008B08 File Offset: 0x00006D08
			private bool IsTupleCovered(List<PairwiseStrategy.TestCaseInfo> testCases, PairwiseStrategy.FeatureTuple tuple)
			{
				foreach (PairwiseStrategy.TestCaseInfo testCaseInfo in testCases)
				{
					if (testCaseInfo.IsTupleCovered(tuple))
					{
						return true;
					}
				}
				return false;
			}

			// Token: 0x0600026E RID: 622 RVA: 0x00008B70 File Offset: 0x00006D70
			public PairwiseTestCaseGenerator()
			{
			}

			// Token: 0x04000085 RID: 133
			private PairwiseStrategy.FleaRand _prng;

			// Token: 0x04000086 RID: 134
			private int[] _dimensions;

			// Token: 0x04000087 RID: 135
			private List<PairwiseStrategy.FeatureTuple>[][] _uncoveredTuples;
		}
	}
}
