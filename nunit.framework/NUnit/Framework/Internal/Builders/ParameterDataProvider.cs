﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x02000128 RID: 296
	public class ParameterDataProvider : IParameterDataProvider
	{
		// Token: 0x060007EB RID: 2027 RVA: 0x0001BDC7 File Offset: 0x00019FC7
		public ParameterDataProvider(params IParameterDataProvider[] providers)
		{
			this._providers.AddRange(providers);
		}

		// Token: 0x060007EC RID: 2028 RVA: 0x0001BDEC File Offset: 0x00019FEC
		public bool HasDataFor(IParameterInfo parameter)
		{
			foreach (IParameterDataProvider parameterDataProvider in this._providers)
			{
				if (parameterDataProvider.HasDataFor(parameter))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060007ED RID: 2029 RVA: 0x0001C114 File Offset: 0x0001A314
		public IEnumerable GetDataFor(IParameterInfo parameter)
		{
			foreach (IParameterDataProvider provider in this._providers)
			{
				foreach (object data in provider.GetDataFor(parameter))
				{
					yield return data;
				}
			}
			yield break;
		}

		// Token: 0x040001BC RID: 444
		private List<IParameterDataProvider> _providers = new List<IParameterDataProvider>();

		// Token: 0x020001C1 RID: 449
		[CompilerGenerated]
		private sealed class <GetDataFor>d__0 : IEnumerable<object>, IEnumerable, IEnumerator<object>, IEnumerator, IDisposable
		{
			// Token: 0x06000B51 RID: 2897 RVA: 0x0001BE58 File Offset: 0x0001A058
			[DebuggerHidden]
			IEnumerator<object> IEnumerable<object>.GetEnumerator()
			{
				ParameterDataProvider.<GetDataFor>d__0 <GetDataFor>d__;
				if (Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId && this.<>1__state == -2)
				{
					this.<>1__state = 0;
					<GetDataFor>d__ = this;
				}
				else
				{
					<GetDataFor>d__ = new ParameterDataProvider.<GetDataFor>d__0(0);
					<GetDataFor>d__.<>4__this = this;
				}
				<GetDataFor>d__.parameter = parameter;
				return <GetDataFor>d__;
			}

			// Token: 0x06000B52 RID: 2898 RVA: 0x0001BEBC File Offset: 0x0001A0BC
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator();
			}

			// Token: 0x06000B53 RID: 2899 RVA: 0x0001BED4 File Offset: 0x0001A0D4
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num == 0)
					{
						this.<>1__state = -1;
						enumerator = this._providers.GetEnumerator();
						this.<>1__state = 1;
						goto IL_C1;
					}
					if (num != 3)
					{
						goto IL_DB;
					}
					this.<>1__state = 2;
					IL_AB:
					if (enumerator2.MoveNext())
					{
						data = enumerator2.Current;
						this.<>2__current = data;
						this.<>1__state = 3;
						return true;
					}
					this.<>m__Finally7();
					IL_C1:
					if (enumerator.MoveNext())
					{
						provider = enumerator.Current;
						enumerator2 = provider.GetDataFor(parameter).GetEnumerator();
						this.<>1__state = 2;
						goto IL_AB;
					}
					this.<>m__Finally4();
					IL_DB:
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x17000286 RID: 646
			// (get) Token: 0x06000B54 RID: 2900 RVA: 0x0001BFDC File Offset: 0x0001A1DC
			object IEnumerator<object>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B55 RID: 2901 RVA: 0x0001BFF3 File Offset: 0x0001A1F3
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000B56 RID: 2902 RVA: 0x0001BFFC File Offset: 0x0001A1FC
			void IDisposable.Dispose()
			{
				switch (this.<>1__state)
				{
				case 1:
					break;
				case 2:
					break;
				case 3:
					break;
				default:
					return;
				}
				try
				{
					switch (this.<>1__state)
					{
					case 2:
						break;
					case 3:
						break;
					default:
						goto IL_4F;
					}
					try
					{
					}
					finally
					{
						this.<>m__Finally7();
					}
					IL_4F:;
				}
				finally
				{
					this.<>m__Finally4();
				}
			}

			// Token: 0x17000287 RID: 647
			// (get) Token: 0x06000B57 RID: 2903 RVA: 0x0001C080 File Offset: 0x0001A280
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B58 RID: 2904 RVA: 0x0001C097 File Offset: 0x0001A297
			[DebuggerHidden]
			public <GetDataFor>d__0(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
			}

			// Token: 0x06000B59 RID: 2905 RVA: 0x0001C0B6 File Offset: 0x0001A2B6
			private void <>m__Finally4()
			{
				this.<>1__state = -1;
				((IDisposable)enumerator).Dispose();
			}

			// Token: 0x06000B5A RID: 2906 RVA: 0x0001C0D4 File Offset: 0x0001A2D4
			private void <>m__Finally7()
			{
				this.<>1__state = 1;
				disposable = (enumerator2 as IDisposable);
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			// Token: 0x040002DD RID: 733
			private object <>2__current;

			// Token: 0x040002DE RID: 734
			private int <>1__state;

			// Token: 0x040002DF RID: 735
			private int <>l__initialThreadId;

			// Token: 0x040002E0 RID: 736
			public ParameterDataProvider <>4__this;

			// Token: 0x040002E1 RID: 737
			public IParameterInfo parameter;

			// Token: 0x040002E2 RID: 738
			public IParameterInfo <>3__parameter;

			// Token: 0x040002E3 RID: 739
			public IParameterDataProvider <provider>5__1;

			// Token: 0x040002E4 RID: 740
			public object <data>5__2;

			// Token: 0x040002E5 RID: 741
			public List<IParameterDataProvider>.Enumerator <>7__wrap3;

			// Token: 0x040002E6 RID: 742
			public IEnumerator <>7__wrap5;

			// Token: 0x040002E7 RID: 743
			public IDisposable <>7__wrap6;
		}
	}
}
