﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x02000029 RID: 41
	public class SequentialStrategy : ICombiningStrategy
	{
		// Token: 0x06000173 RID: 371 RVA: 0x000055F0 File Offset: 0x000037F0
		public IEnumerable<ITestCaseData> GetTestCases(IEnumerable[] sources)
		{
			List<ITestCaseData> list = new List<ITestCaseData>();
			IEnumerator[] array = new IEnumerator[sources.Length];
			for (int i = 0; i < sources.Length; i++)
			{
				array[i] = sources[i].GetEnumerator();
			}
			for (;;)
			{
				bool flag = false;
				object[] array2 = new object[sources.Length];
				for (int i = 0; i < sources.Length; i++)
				{
					if (array[i].MoveNext())
					{
						array2[i] = array[i].Current;
						flag = true;
					}
					else
					{
						array2[i] = null;
					}
				}
				if (!flag)
				{
					break;
				}
				TestCaseParameters item = new TestCaseParameters(array2);
				list.Add(item);
			}
			return list;
		}

		// Token: 0x06000174 RID: 372 RVA: 0x0000569F File Offset: 0x0000389F
		public SequentialStrategy()
		{
		}
	}
}
