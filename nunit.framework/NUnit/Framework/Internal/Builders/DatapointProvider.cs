﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x02000051 RID: 81
	public class DatapointProvider : IParameterDataProvider
	{
		// Token: 0x06000271 RID: 625 RVA: 0x00008B78 File Offset: 0x00006D78
		public bool HasDataFor(IParameterInfo parameter)
		{
			IMethodInfo method = parameter.Method;
			bool result;
			if (!method.IsDefined<TheoryAttribute>(true))
			{
				result = false;
			}
			else
			{
				Type parameterType = parameter.ParameterType;
				if (parameterType == typeof(bool) || parameterType.GetTypeInfo().IsEnum)
				{
					result = true;
				}
				else
				{
					Type type = method.TypeInfo.Type;
					foreach (MemberInfo memberInfo in type.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy))
					{
						if (memberInfo.IsDefined(typeof(DatapointAttribute), true) && this.GetTypeFromMemberInfo(memberInfo) == parameterType)
						{
							return true;
						}
						if (memberInfo.IsDefined(typeof(DatapointSourceAttribute), true) && this.GetElementTypeFromMemberInfo(memberInfo) == parameterType)
						{
							return true;
						}
					}
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06000272 RID: 626 RVA: 0x00008C74 File Offset: 0x00006E74
		public IEnumerable GetDataFor(IParameterInfo parameter)
		{
			List<object> list = new List<object>();
			Type parameterType = parameter.ParameterType;
			Type type = parameter.Method.TypeInfo.Type;
			foreach (MemberInfo memberInfo in type.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy))
			{
				if (memberInfo.IsDefined(typeof(DatapointAttribute), true))
				{
					FieldInfo fieldInfo = memberInfo as FieldInfo;
					if (this.GetTypeFromMemberInfo(memberInfo) == parameterType && fieldInfo != null)
					{
						if (fieldInfo.IsStatic)
						{
							list.Add(fieldInfo.GetValue(null));
						}
						else
						{
							list.Add(fieldInfo.GetValue(ProviderCache.GetInstanceOf(type)));
						}
					}
				}
				else if (memberInfo.IsDefined(typeof(DatapointSourceAttribute), true))
				{
					if (this.GetElementTypeFromMemberInfo(memberInfo) == parameterType)
					{
						FieldInfo fieldInfo = memberInfo as FieldInfo;
						PropertyInfo propertyInfo = memberInfo as PropertyInfo;
						MethodInfo methodInfo = memberInfo as MethodInfo;
						if (fieldInfo != null)
						{
							object obj = fieldInfo.IsStatic ? null : ProviderCache.GetInstanceOf(type);
							foreach (object item in ((IEnumerable)fieldInfo.GetValue(obj)))
							{
								list.Add(item);
							}
						}
						else if (propertyInfo != null)
						{
							MethodInfo getMethod = propertyInfo.GetGetMethod(true);
							object obj = getMethod.IsStatic ? null : ProviderCache.GetInstanceOf(type);
							foreach (object item in ((IEnumerable)propertyInfo.GetValue(obj, null)))
							{
								list.Add(item);
							}
						}
						else if (methodInfo != null)
						{
							object obj = methodInfo.IsStatic ? null : ProviderCache.GetInstanceOf(type);
							foreach (object item in ((IEnumerable)methodInfo.Invoke(obj, new Type[0])))
							{
								list.Add(item);
							}
						}
					}
				}
			}
			if (list.Count == 0)
			{
				if (parameterType == typeof(bool))
				{
					list.Add(true);
					list.Add(false);
				}
				else if (parameterType.GetTypeInfo().IsEnum)
				{
					foreach (object item2 in TypeHelper.GetEnumValues(parameterType))
					{
						list.Add(item2);
					}
				}
			}
			return list;
		}

		// Token: 0x06000273 RID: 627 RVA: 0x00009004 File Offset: 0x00007204
		private Type GetTypeFromMemberInfo(MemberInfo member)
		{
			FieldInfo fieldInfo = member as FieldInfo;
			Type result;
			if (fieldInfo != null)
			{
				result = fieldInfo.FieldType;
			}
			else
			{
				PropertyInfo propertyInfo = member as PropertyInfo;
				if (propertyInfo != null)
				{
					result = propertyInfo.PropertyType;
				}
				else
				{
					MethodInfo methodInfo = member as MethodInfo;
					if (methodInfo != null)
					{
						result = methodInfo.ReturnType;
					}
					else
					{
						result = null;
					}
				}
			}
			return result;
		}

		// Token: 0x06000274 RID: 628 RVA: 0x00009068 File Offset: 0x00007268
		private Type GetElementTypeFromMemberInfo(MemberInfo member)
		{
			Type typeFromMemberInfo = this.GetTypeFromMemberInfo(member);
			Type result;
			if (typeFromMemberInfo == null)
			{
				result = null;
			}
			else if (typeFromMemberInfo.IsArray)
			{
				result = typeFromMemberInfo.GetElementType();
			}
			else if (typeFromMemberInfo.GetTypeInfo().IsGenericType && typeFromMemberInfo.Name == "IEnumerable`1")
			{
				result = typeFromMemberInfo.GetGenericArguments()[0];
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000275 RID: 629 RVA: 0x000090DA File Offset: 0x000072DA
		public DatapointProvider()
		{
		}
	}
}
