﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x020000FD RID: 253
	public class ParameterDataSourceProvider : IParameterDataProvider
	{
		// Token: 0x0600071E RID: 1822 RVA: 0x00019D54 File Offset: 0x00017F54
		public bool HasDataFor(IParameterInfo parameter)
		{
			return parameter.IsDefined<IParameterDataSource>(false);
		}

		// Token: 0x0600071F RID: 1823 RVA: 0x00019D70 File Offset: 0x00017F70
		public IEnumerable GetDataFor(IParameterInfo parameter)
		{
			List<object> list = new List<object>();
			foreach (IParameterDataSource parameterDataSource in parameter.GetCustomAttributes<IParameterDataSource>(false))
			{
				foreach (object item in parameterDataSource.GetData(parameter))
				{
					list.Add(item);
				}
			}
			return list;
		}

		// Token: 0x06000720 RID: 1824 RVA: 0x00019E14 File Offset: 0x00018014
		public ParameterDataSourceProvider()
		{
		}
	}
}
