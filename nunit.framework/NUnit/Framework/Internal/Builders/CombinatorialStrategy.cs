﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x02000052 RID: 82
	public class CombinatorialStrategy : ICombiningStrategy
	{
		// Token: 0x06000276 RID: 630 RVA: 0x000090E4 File Offset: 0x000072E4
		public IEnumerable<ITestCaseData> GetTestCases(IEnumerable[] sources)
		{
			List<ITestCaseData> list = new List<ITestCaseData>();
			IEnumerator[] array = new IEnumerator[sources.Length];
			int num = -1;
			for (;;)
			{
				while (++num < sources.Length)
				{
					array[num] = sources[num].GetEnumerator();
					if (!array[num].MoveNext())
					{
						goto Block_1;
					}
				}
				object[] array2 = new object[sources.Length];
				for (int i = 0; i < sources.Length; i++)
				{
					array2[i] = array[i].Current;
				}
				TestCaseParameters item = new TestCaseParameters(array2);
				list.Add(item);
				num = sources.Length;
				while (--num >= 0 && !array[num].MoveNext())
				{
				}
				if (num < 0)
				{
					goto Block_6;
				}
			}
			Block_1:
			return list;
			Block_6:
			return list;
		}

		// Token: 0x06000277 RID: 631 RVA: 0x000091BE File Offset: 0x000073BE
		public CombinatorialStrategy()
		{
		}
	}
}
