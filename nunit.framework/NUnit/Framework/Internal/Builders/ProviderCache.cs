﻿using System;
using System.Collections.Generic;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x02000070 RID: 112
	internal class ProviderCache
	{
		// Token: 0x0600041F RID: 1055 RVA: 0x0000D3AC File Offset: 0x0000B5AC
		public static object GetInstanceOf(Type providerType)
		{
			return ProviderCache.GetInstanceOf(providerType, null);
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x0000D3C8 File Offset: 0x0000B5C8
		public static object GetInstanceOf(Type providerType, object[] providerArgs)
		{
			ProviderCache.CacheEntry key = new ProviderCache.CacheEntry(providerType, providerArgs);
			object obj = ProviderCache.instances.ContainsKey(key) ? ProviderCache.instances[key] : null;
			if (obj == null)
			{
				obj = (ProviderCache.instances[key] = Reflect.Construct(providerType, providerArgs));
			}
			return obj;
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x0000D420 File Offset: 0x0000B620
		public static void Clear()
		{
			foreach (ProviderCache.CacheEntry key in ProviderCache.instances.Keys)
			{
				IDisposable disposable = ProviderCache.instances[key] as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			ProviderCache.instances.Clear();
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x0000D4B0 File Offset: 0x0000B6B0
		public ProviderCache()
		{
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x0000D4A4 File Offset: 0x0000B6A4
		// Note: this type is marked as 'beforefieldinit'.
		static ProviderCache()
		{
		}

		// Token: 0x040000B2 RID: 178
		private static Dictionary<ProviderCache.CacheEntry, object> instances = new Dictionary<ProviderCache.CacheEntry, object>();

		// Token: 0x02000071 RID: 113
		private class CacheEntry
		{
			// Token: 0x06000424 RID: 1060 RVA: 0x0000D4B8 File Offset: 0x0000B6B8
			public CacheEntry(Type providerType, object[] providerArgs)
			{
				this.providerType = providerType;
			}

			// Token: 0x06000425 RID: 1061 RVA: 0x0000D4CC File Offset: 0x0000B6CC
			public override bool Equals(object obj)
			{
				ProviderCache.CacheEntry cacheEntry = obj as ProviderCache.CacheEntry;
				return cacheEntry != null && this.providerType == cacheEntry.providerType;
			}

			// Token: 0x06000426 RID: 1062 RVA: 0x0000D504 File Offset: 0x0000B704
			public override int GetHashCode()
			{
				return this.providerType.GetHashCode();
			}

			// Token: 0x040000B3 RID: 179
			private Type providerType;
		}
	}
}
