﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x0200010E RID: 270
	public class NamespaceTreeBuilder
	{
		// Token: 0x06000762 RID: 1890 RVA: 0x0001A94C File Offset: 0x00018B4C
		public NamespaceTreeBuilder(TestSuite rootSuite)
		{
			this.rootSuite = rootSuite;
		}

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x06000763 RID: 1891 RVA: 0x0001A96C File Offset: 0x00018B6C
		public TestSuite RootSuite
		{
			get
			{
				return this.rootSuite;
			}
		}

		// Token: 0x06000764 RID: 1892 RVA: 0x0001A984 File Offset: 0x00018B84
		public void Add(IList<Test> fixtures)
		{
			foreach (Test test in fixtures)
			{
				TestSuite fixture = (TestSuite)test;
				this.Add(fixture);
			}
		}

		// Token: 0x06000765 RID: 1893 RVA: 0x0001A9E0 File Offset: 0x00018BE0
		public void Add(TestSuite fixture)
		{
			string namespaceForFixture = NamespaceTreeBuilder.GetNamespaceForFixture(fixture);
			TestSuite testSuite = this.BuildFromNameSpace(namespaceForFixture);
			if (fixture is SetUpFixture)
			{
				this.AddSetUpFixture(fixture, testSuite, namespaceForFixture);
			}
			else
			{
				testSuite.Add(fixture);
			}
		}

		// Token: 0x06000766 RID: 1894 RVA: 0x0001AA24 File Offset: 0x00018C24
		private static string GetNamespaceForFixture(TestSuite fixture)
		{
			string text = fixture.FullName;
			int num = text.IndexOfAny(new char[]
			{
				'[',
				'('
			});
			if (num >= 0)
			{
				text = text.Substring(0, num);
			}
			num = text.LastIndexOf('.');
			return (num > 0) ? text.Substring(0, num) : string.Empty;
		}

		// Token: 0x06000767 RID: 1895 RVA: 0x0001AA88 File Offset: 0x00018C88
		private TestSuite BuildFromNameSpace(string ns)
		{
			TestSuite result;
			if (ns == null || ns == "")
			{
				result = this.rootSuite;
			}
			else
			{
				TestSuite testSuite = this.namespaceSuites.ContainsKey(ns) ? this.namespaceSuites[ns] : null;
				if (testSuite != null)
				{
					result = testSuite;
				}
				else
				{
					int num = ns.LastIndexOf(".");
					if (num == -1)
					{
						testSuite = new TestSuite(ns);
						if (this.rootSuite == null)
						{
							this.rootSuite = testSuite;
						}
						else
						{
							this.rootSuite.Add(testSuite);
						}
					}
					else
					{
						string text = ns.Substring(0, num);
						TestSuite testSuite2 = this.BuildFromNameSpace(text);
						string name = ns.Substring(num + 1);
						testSuite = new TestSuite(text, name);
						testSuite2.Add(testSuite);
					}
					this.namespaceSuites[ns] = testSuite;
					result = testSuite;
				}
			}
			return result;
		}

		// Token: 0x06000768 RID: 1896 RVA: 0x0001AB80 File Offset: 0x00018D80
		private void AddSetUpFixture(TestSuite newSetupFixture, TestSuite containingSuite, string ns)
		{
			foreach (ITest test in containingSuite.Tests)
			{
				TestSuite test2 = (TestSuite)test;
				newSetupFixture.Add(test2);
			}
			if (containingSuite is SetUpFixture)
			{
				containingSuite.Tests.Clear();
				containingSuite.Add(newSetupFixture);
			}
			else
			{
				TestSuite testSuite = (TestSuite)containingSuite.Parent;
				if (testSuite == null)
				{
					newSetupFixture.Name = this.rootSuite.Name;
					this.rootSuite = newSetupFixture;
				}
				else
				{
					testSuite.Tests.Remove(containingSuite);
					testSuite.Add(newSetupFixture);
				}
			}
			this.namespaceSuites[ns] = newSetupFixture;
		}

		// Token: 0x040001A8 RID: 424
		private Dictionary<string, TestSuite> namespaceSuites = new Dictionary<string, TestSuite>();

		// Token: 0x040001A9 RID: 425
		private TestSuite rootSuite;
	}
}
