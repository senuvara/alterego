﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x02000073 RID: 115
	public class DefaultTestCaseBuilder : ITestCaseBuilder
	{
		// Token: 0x06000429 RID: 1065 RVA: 0x0000D524 File Offset: 0x0000B724
		public bool CanBuildFrom(IMethodInfo method)
		{
			return method.IsDefined<ITestBuilder>(false) || method.IsDefined<ISimpleTestBuilder>(false);
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x0000D54C File Offset: 0x0000B74C
		public Test BuildFrom(IMethodInfo method)
		{
			return this.BuildFrom(method, null);
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x0000D568 File Offset: 0x0000B768
		public bool CanBuildFrom(IMethodInfo method, Test parentSuite)
		{
			return this.CanBuildFrom(method);
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x0000D584 File Offset: 0x0000B784
		public Test BuildFrom(IMethodInfo method, Test parentSuite)
		{
			List<TestMethod> list = new List<TestMethod>();
			List<ITestBuilder> list2 = new List<ITestBuilder>(method.GetCustomAttributes<ITestBuilder>(false));
			bool flag = true;
			foreach (ITestBuilder testBuilder in list2)
			{
				if (testBuilder is CombiningStrategyAttribute)
				{
					flag = false;
				}
			}
			if (flag)
			{
				list2.Add(new CombinatorialAttribute());
			}
			foreach (ITestBuilder testBuilder in list2)
			{
				foreach (TestMethod item in testBuilder.BuildFrom(method, parentSuite))
				{
					list.Add(item);
				}
			}
			return (list.Count > 0) ? this.BuildParameterizedMethodSuite(method, list) : this.BuildSingleTestMethod(method, parentSuite);
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x0000D6CC File Offset: 0x0000B8CC
		private Test BuildParameterizedMethodSuite(IMethodInfo method, IEnumerable<TestMethod> tests)
		{
			ParameterizedMethodSuite parameterizedMethodSuite = new ParameterizedMethodSuite(method);
			parameterizedMethodSuite.ApplyAttributesToTest(method.MethodInfo);
			foreach (TestMethod test in tests)
			{
				parameterizedMethodSuite.Add(test);
			}
			return parameterizedMethodSuite;
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x0000D740 File Offset: 0x0000B940
		private Test BuildSingleTestMethod(IMethodInfo method, Test suite)
		{
			ISimpleTestBuilder[] customAttributes = method.GetCustomAttributes<ISimpleTestBuilder>(false);
			return (customAttributes.Length > 0) ? customAttributes[0].BuildFrom(method, suite) : this._nunitTestCaseBuilder.BuildTestMethod(method, suite, null);
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x0000D77A File Offset: 0x0000B97A
		public DefaultTestCaseBuilder()
		{
		}

		// Token: 0x040000B4 RID: 180
		private NUnitTestCaseBuilder _nunitTestCaseBuilder = new NUnitTestCaseBuilder();
	}
}
