﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x02000110 RID: 272
	public class DefaultSuiteBuilder : ISuiteBuilder
	{
		// Token: 0x0600076B RID: 1899 RVA: 0x0001AC64 File Offset: 0x00018E64
		public bool CanBuildFrom(ITypeInfo typeInfo)
		{
			return (!typeInfo.IsAbstract || typeInfo.IsSealed) && (typeInfo.IsDefined<IFixtureBuilder>(true) || (!typeInfo.IsGenericTypeDefinition && typeInfo.HasMethodWithAttribute(typeof(IImplyFixture))));
		}

		// Token: 0x0600076C RID: 1900 RVA: 0x0001ACC4 File Offset: 0x00018EC4
		public TestSuite BuildFrom(ITypeInfo typeInfo)
		{
			List<TestSuite> list = new List<TestSuite>();
			TestSuite result;
			try
			{
				IFixtureBuilder[] fixtureBuilderAttributes = this.GetFixtureBuilderAttributes(typeInfo);
				foreach (IFixtureBuilder fixtureBuilder in fixtureBuilderAttributes)
				{
					foreach (TestSuite item in fixtureBuilder.BuildFrom(typeInfo))
					{
						list.Add(item);
					}
				}
				if (typeInfo.IsGenericType)
				{
					result = this.BuildMultipleFixtures(typeInfo, list);
				}
				else
				{
					switch (list.Count)
					{
					case 0:
						result = this._defaultBuilder.BuildFrom(typeInfo);
						break;
					case 1:
						result = list[0];
						break;
					default:
						result = this.BuildMultipleFixtures(typeInfo, list);
						break;
					}
				}
			}
			catch (Exception innerException)
			{
				TestFixture testFixture = new TestFixture(typeInfo);
				testFixture.RunState = RunState.NotRunnable;
				if (innerException is TargetInvocationException)
				{
					innerException = innerException.InnerException;
				}
				string value = "An exception was thrown while loading the test." + Env.NewLine + innerException.ToString();
				testFixture.Properties.Add("_SKIPREASON", value);
				result = testFixture;
			}
			return result;
		}

		// Token: 0x0600076D RID: 1901 RVA: 0x0001AE28 File Offset: 0x00019028
		private TestSuite BuildMultipleFixtures(ITypeInfo typeInfo, IEnumerable<TestSuite> fixtures)
		{
			TestSuite testSuite = new ParameterizedFixtureSuite(typeInfo);
			foreach (TestSuite test in fixtures)
			{
				testSuite.Add(test);
			}
			return testSuite;
		}

		// Token: 0x0600076E RID: 1902 RVA: 0x0001AE8C File Offset: 0x0001908C
		private IFixtureBuilder[] GetFixtureBuilderAttributes(ITypeInfo typeInfo)
		{
			IFixtureBuilder[] array = new IFixtureBuilder[0];
			while (typeInfo != null && !typeInfo.IsType(typeof(object)))
			{
				array = typeInfo.GetCustomAttributes<IFixtureBuilder>(false);
				if (array.Length > 0)
				{
					IFixtureBuilder[] result;
					if (array.Length == 1)
					{
						result = array;
					}
					else
					{
						int num = 0;
						foreach (IFixtureBuilder fixtureBuilder in array)
						{
							if (this.HasArguments(fixtureBuilder))
							{
								num++;
							}
						}
						if (num == array.Length)
						{
							result = array;
						}
						else if (num == 0)
						{
							result = new IFixtureBuilder[]
							{
								array[0]
							};
						}
						else
						{
							IFixtureBuilder[] array3 = new IFixtureBuilder[num];
							int num2 = 0;
							foreach (IFixtureBuilder fixtureBuilder in array)
							{
								if (this.HasArguments(fixtureBuilder))
								{
									array3[num2++] = fixtureBuilder;
								}
							}
							result = array3;
						}
					}
					return result;
				}
				typeInfo = typeInfo.BaseType;
			}
			return array;
		}

		// Token: 0x0600076F RID: 1903 RVA: 0x0001AFCC File Offset: 0x000191CC
		private bool HasArguments(IFixtureBuilder attr)
		{
			TestFixtureAttribute testFixtureAttribute = attr as TestFixtureAttribute;
			return testFixtureAttribute == null || testFixtureAttribute.Arguments.Length > 0 || testFixtureAttribute.TypeArgs.Length > 0;
		}

		// Token: 0x06000770 RID: 1904 RVA: 0x0001B001 File Offset: 0x00019201
		public DefaultSuiteBuilder()
		{
		}

		// Token: 0x040001AA RID: 426
		private NUnitTestFixtureBuilder _defaultBuilder = new NUnitTestFixtureBuilder();
	}
}
