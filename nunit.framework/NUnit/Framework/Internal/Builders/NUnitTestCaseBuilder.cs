﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Builders
{
	// Token: 0x020000A0 RID: 160
	public class NUnitTestCaseBuilder
	{
		// Token: 0x060004DC RID: 1244 RVA: 0x00010174 File Offset: 0x0000E374
		public NUnitTestCaseBuilder()
		{
			this._nameGenerator = new TestNameGenerator();
		}

		// Token: 0x060004DD RID: 1245 RVA: 0x00010198 File Offset: 0x0000E398
		public TestMethod BuildTestMethod(IMethodInfo method, Test parentSuite, TestCaseParameters parms)
		{
			TestMethod testMethod = new TestMethod(method, parentSuite)
			{
				Seed = this._randomizer.Next()
			};
			NUnitTestCaseBuilder.CheckTestMethodSignature(testMethod, parms);
			if (parms == null || parms.Arguments == null)
			{
				testMethod.ApplyAttributesToTest(method.MethodInfo);
			}
			string fullName = testMethod.Method.TypeInfo.FullName;
			if (parentSuite != null)
			{
				fullName = parentSuite.FullName;
			}
			if (parms != null)
			{
				parms.ApplyToTest(testMethod);
				if (parms.TestName != null)
				{
					testMethod.Name = (parms.TestName.Contains("{") ? new TestNameGenerator(parms.TestName).GetDisplayName(testMethod, parms.OriginalArguments) : parms.TestName);
				}
				else
				{
					testMethod.Name = this._nameGenerator.GetDisplayName(testMethod, parms.OriginalArguments);
				}
			}
			else
			{
				testMethod.Name = this._nameGenerator.GetDisplayName(testMethod, null);
			}
			testMethod.FullName = fullName + "." + testMethod.Name;
			return testMethod;
		}

		// Token: 0x060004DE RID: 1246 RVA: 0x000102C0 File Offset: 0x0000E4C0
		private static bool CheckTestMethodSignature(TestMethod testMethod, TestCaseParameters parms)
		{
			bool result;
			if (testMethod.Method.IsAbstract)
			{
				result = NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, "Method is abstract");
			}
			else if (!testMethod.Method.IsPublic)
			{
				result = NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, "Method is not public");
			}
			else
			{
				IParameterInfo[] parameters = testMethod.Method.GetParameters();
				int num = 0;
				foreach (IParameterInfo parameterInfo in parameters)
				{
					if (!parameterInfo.IsOptional)
					{
						num++;
					}
				}
				int num2 = parameters.Length;
				object[] array2 = null;
				int num3 = 0;
				if (parms != null)
				{
					testMethod.parms = parms;
					testMethod.RunState = parms.RunState;
					array2 = parms.Arguments;
					if (array2 != null)
					{
						num3 = array2.Length;
					}
					if (testMethod.RunState != RunState.Runnable)
					{
						return false;
					}
				}
				ITypeInfo returnType = testMethod.Method.ReturnType;
				if (returnType.IsType(typeof(void)))
				{
					if (parms != null && parms.HasExpectedResult)
					{
						return NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, "Method returning void cannot have an expected result");
					}
				}
				else if (parms == null || !parms.HasExpectedResult)
				{
					return NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, "Method has non-void return value, but no result is expected");
				}
				if (num3 > 0 && num2 == 0)
				{
					result = NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, "Arguments provided for method with no parameters");
				}
				else if (num3 == 0 && num > 0)
				{
					result = NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, "No arguments were provided");
				}
				else if (num3 < num)
				{
					result = NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, string.Format("Not enough arguments provided, provide at least {0} arguments.", num));
				}
				else if (num3 > num2)
				{
					result = NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, string.Format("Too many arguments provided, provide at most {0} arguments.", num2));
				}
				else
				{
					if (testMethod.Method.IsGenericMethodDefinition && array2 != null)
					{
						Type[] typeArguments = new GenericMethodHelper(testMethod.Method.MethodInfo).GetTypeArguments(array2);
						foreach (Type type in typeArguments)
						{
							if (type == null || type == TypeHelper.NonmatchingType)
							{
								return NUnitTestCaseBuilder.MarkAsNotRunnable(testMethod, "Unable to determine type arguments for method");
							}
						}
						testMethod.Method = testMethod.Method.MakeGenericMethod(typeArguments);
						parameters = testMethod.Method.GetParameters();
					}
					if (array2 != null && parameters != null)
					{
						TypeHelper.ConvertArgumentList(array2, parameters);
					}
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060004DF RID: 1247 RVA: 0x00010598 File Offset: 0x0000E798
		private static bool MarkAsNotRunnable(TestMethod testMethod, string reason)
		{
			testMethod.RunState = RunState.NotRunnable;
			testMethod.Properties.Set("_SKIPREASON", reason);
			return false;
		}

		// Token: 0x040000EA RID: 234
		private readonly Randomizer _randomizer = Randomizer.CreateRandomizer();

		// Token: 0x040000EB RID: 235
		private readonly TestNameGenerator _nameGenerator;
	}
}
