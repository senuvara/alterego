﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Compatibility;

namespace NUnit.Framework.Internal
{
	// Token: 0x0200005F RID: 95
	public class GenericMethodHelper
	{
		// Token: 0x060002E5 RID: 741 RVA: 0x0000AEEC File Offset: 0x000090EC
		public GenericMethodHelper(MethodInfo method)
		{
			Guard.ArgumentValid(method.IsGenericMethod, "Specified method must be generic", "method");
			this.Method = method;
			this.TypeParms = this.Method.GetGenericArguments();
			this.TypeArgs = new Type[this.TypeParms.Length];
			ParameterInfo[] parameters = this.Method.GetParameters();
			this.ParmTypes = new Type[parameters.Length];
			for (int i = 0; i < parameters.Length; i++)
			{
				this.ParmTypes[i] = parameters[i].ParameterType;
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x060002E6 RID: 742 RVA: 0x0000AF84 File Offset: 0x00009184
		// (set) Token: 0x060002E7 RID: 743 RVA: 0x0000AF9B File Offset: 0x0000919B
		private MethodInfo Method
		{
			[CompilerGenerated]
			get
			{
				return this.<Method>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Method>k__BackingField = value;
			}
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x060002E8 RID: 744 RVA: 0x0000AFA4 File Offset: 0x000091A4
		// (set) Token: 0x060002E9 RID: 745 RVA: 0x0000AFBB File Offset: 0x000091BB
		private Type[] TypeParms
		{
			[CompilerGenerated]
			get
			{
				return this.<TypeParms>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TypeParms>k__BackingField = value;
			}
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x060002EA RID: 746 RVA: 0x0000AFC4 File Offset: 0x000091C4
		// (set) Token: 0x060002EB RID: 747 RVA: 0x0000AFDB File Offset: 0x000091DB
		private Type[] TypeArgs
		{
			[CompilerGenerated]
			get
			{
				return this.<TypeArgs>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TypeArgs>k__BackingField = value;
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x060002EC RID: 748 RVA: 0x0000AFE4 File Offset: 0x000091E4
		// (set) Token: 0x060002ED RID: 749 RVA: 0x0000AFFB File Offset: 0x000091FB
		private Type[] ParmTypes
		{
			[CompilerGenerated]
			get
			{
				return this.<ParmTypes>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ParmTypes>k__BackingField = value;
			}
		}

		// Token: 0x060002EE RID: 750 RVA: 0x0000B004 File Offset: 0x00009204
		public Type[] GetTypeArguments(object[] argList)
		{
			Guard.ArgumentValid(argList.Length == this.ParmTypes.Length, "Supplied arguments do not match required method parameters", "argList");
			for (int i = 0; i < this.ParmTypes.Length; i++)
			{
				object obj = argList[i];
				if (obj != null)
				{
					Type type = obj.GetType();
					this.TryApplyArgType(this.ParmTypes[i], type);
				}
			}
			return this.TypeArgs;
		}

		// Token: 0x060002EF RID: 751 RVA: 0x0000B07C File Offset: 0x0000927C
		private void TryApplyArgType(Type parmType, Type argType)
		{
			if (parmType.IsGenericParameter)
			{
				this.ApplyArgType(parmType, argType);
			}
			else if (parmType.GetTypeInfo().ContainsGenericParameters)
			{
				Type[] genericArguments = parmType.GetGenericArguments();
				if (argType.HasElementType)
				{
					this.ApplyArgType(genericArguments[0], argType.GetElementType());
				}
				else if (argType.GetTypeInfo().IsGenericType && this.IsAssignableToGenericType(argType, parmType))
				{
					Type[] genericArguments2 = argType.GetGenericArguments();
					if (genericArguments2.Length == genericArguments.Length)
					{
						for (int i = 0; i < genericArguments.Length; i++)
						{
							this.TryApplyArgType(genericArguments[i], genericArguments2[i]);
						}
					}
				}
			}
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x0000B13C File Offset: 0x0000933C
		private void ApplyArgType(Type parmType, Type argType)
		{
			int genericParameterPosition = parmType.GenericParameterPosition;
			this.TypeArgs[genericParameterPosition] = TypeHelper.BestCommonType(this.TypeArgs[genericParameterPosition], argType);
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x0000B168 File Offset: 0x00009368
		private bool IsAssignableToGenericType(Type givenType, Type genericType)
		{
			Type[] interfaces = givenType.GetInterfaces();
			foreach (Type type in interfaces)
			{
				if (type.GetTypeInfo().IsGenericType)
				{
					Type genericTypeDefinition = type.GetGenericTypeDefinition();
					if (genericTypeDefinition.Name == genericType.Name && genericTypeDefinition.Namespace == genericType.Namespace)
					{
						return true;
					}
				}
			}
			if (givenType.GetTypeInfo().IsGenericType)
			{
				Type genericTypeDefinition = givenType.GetGenericTypeDefinition();
				if (genericTypeDefinition.Name == genericType.Name && genericTypeDefinition.Namespace == genericType.Namespace)
				{
					return true;
				}
			}
			Type baseType = givenType.GetTypeInfo().BaseType;
			return baseType != null && this.IsAssignableToGenericType(baseType, genericType);
		}

		// Token: 0x0400009D RID: 157
		[CompilerGenerated]
		private MethodInfo <Method>k__BackingField;

		// Token: 0x0400009E RID: 158
		[CompilerGenerated]
		private Type[] <TypeParms>k__BackingField;

		// Token: 0x0400009F RID: 159
		[CompilerGenerated]
		private Type[] <TypeArgs>k__BackingField;

		// Token: 0x040000A0 RID: 160
		[CompilerGenerated]
		private Type[] <ParmTypes>k__BackingField;
	}
}
