﻿using System;
using System.Runtime.Serialization;

namespace NUnit.Framework.Internal
{
	// Token: 0x0200006F RID: 111
	[Serializable]
	public class InvalidTestFixtureException : Exception
	{
		// Token: 0x0600041B RID: 1051 RVA: 0x0000D37B File Offset: 0x0000B57B
		public InvalidTestFixtureException()
		{
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x0000D386 File Offset: 0x0000B586
		public InvalidTestFixtureException(string message) : base(message)
		{
		}

		// Token: 0x0600041D RID: 1053 RVA: 0x0000D392 File Offset: 0x0000B592
		public InvalidTestFixtureException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x0000D39F File Offset: 0x0000B59F
		protected InvalidTestFixtureException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
