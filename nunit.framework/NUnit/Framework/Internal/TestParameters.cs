﻿using System;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000B4 RID: 180
	public abstract class TestParameters : ITestData, IApplyToTest
	{
		// Token: 0x0600051D RID: 1309 RVA: 0x0001251A File Offset: 0x0001071A
		public TestParameters()
		{
			this.RunState = RunState.Runnable;
			this.Properties = new PropertyBag();
		}

		// Token: 0x0600051E RID: 1310 RVA: 0x00012539 File Offset: 0x00010739
		public TestParameters(object[] args)
		{
			this.RunState = RunState.Runnable;
			this.InitializeAguments(args);
			this.Properties = new PropertyBag();
		}

		// Token: 0x0600051F RID: 1311 RVA: 0x00012560 File Offset: 0x00010760
		public TestParameters(Exception exception)
		{
			this.RunState = RunState.NotRunnable;
			this.Properties = new PropertyBag();
			this.Properties.Set("_SKIPREASON", ExceptionHelper.BuildMessage(exception));
			this.Properties.Set("_PROVIDERSTACKTRACE", ExceptionHelper.BuildStackTrace(exception));
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x000125B8 File Offset: 0x000107B8
		public TestParameters(ITestData data)
		{
			this.RunState = data.RunState;
			this.Properties = new PropertyBag();
			this.TestName = data.TestName;
			this.InitializeAguments(data.Arguments);
			foreach (string key in data.Properties.Keys)
			{
				this.Properties[key] = data.Properties[key];
			}
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x00012664 File Offset: 0x00010864
		private void InitializeAguments(object[] args)
		{
			this.OriginalArguments = args;
			int num = args.Length;
			this.Arguments = new object[num];
			Array.Copy(args, this.Arguments, num);
		}

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x06000522 RID: 1314 RVA: 0x0001269C File Offset: 0x0001089C
		// (set) Token: 0x06000523 RID: 1315 RVA: 0x000126B3 File Offset: 0x000108B3
		public RunState RunState
		{
			[CompilerGenerated]
			get
			{
				return this.<RunState>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RunState>k__BackingField = value;
			}
		}

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06000524 RID: 1316 RVA: 0x000126BC File Offset: 0x000108BC
		// (set) Token: 0x06000525 RID: 1317 RVA: 0x000126D3 File Offset: 0x000108D3
		public object[] Arguments
		{
			[CompilerGenerated]
			get
			{
				return this.<Arguments>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Arguments>k__BackingField = value;
			}
		}

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x06000526 RID: 1318 RVA: 0x000126DC File Offset: 0x000108DC
		// (set) Token: 0x06000527 RID: 1319 RVA: 0x000126F3 File Offset: 0x000108F3
		public string TestName
		{
			[CompilerGenerated]
			get
			{
				return this.<TestName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TestName>k__BackingField = value;
			}
		}

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x06000528 RID: 1320 RVA: 0x000126FC File Offset: 0x000108FC
		// (set) Token: 0x06000529 RID: 1321 RVA: 0x00012713 File Offset: 0x00010913
		public IPropertyBag Properties
		{
			[CompilerGenerated]
			get
			{
				return this.<Properties>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Properties>k__BackingField = value;
			}
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x0001271C File Offset: 0x0001091C
		public void ApplyToTest(Test test)
		{
			if (this.RunState != RunState.Runnable)
			{
				test.RunState = this.RunState;
			}
			foreach (string key in this.Properties.Keys)
			{
				foreach (object value in this.Properties[key])
				{
					test.Properties.Add(key, value);
				}
			}
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x0600052B RID: 1323 RVA: 0x000127F0 File Offset: 0x000109F0
		// (set) Token: 0x0600052C RID: 1324 RVA: 0x00012807 File Offset: 0x00010A07
		public object[] OriginalArguments
		{
			[CompilerGenerated]
			get
			{
				return this.<OriginalArguments>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<OriginalArguments>k__BackingField = value;
			}
		}

		// Token: 0x0400010A RID: 266
		[CompilerGenerated]
		private RunState <RunState>k__BackingField;

		// Token: 0x0400010B RID: 267
		[CompilerGenerated]
		private object[] <Arguments>k__BackingField;

		// Token: 0x0400010C RID: 268
		[CompilerGenerated]
		private string <TestName>k__BackingField;

		// Token: 0x0400010D RID: 269
		[CompilerGenerated]
		private IPropertyBag <Properties>k__BackingField;

		// Token: 0x0400010E RID: 270
		[CompilerGenerated]
		private object[] <OriginalArguments>k__BackingField;
	}
}
