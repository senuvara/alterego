﻿using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000FB RID: 251
	public class TestCaseResult : TestResult
	{
		// Token: 0x0600070C RID: 1804 RVA: 0x00019AFF File Offset: 0x00017CFF
		public TestCaseResult(TestMethod test) : base(test)
		{
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x0600070D RID: 1805 RVA: 0x00019B0C File Offset: 0x00017D0C
		public override int FailCount
		{
			get
			{
				return (base.ResultState.Status == TestStatus.Failed) ? 1 : 0;
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x0600070E RID: 1806 RVA: 0x00019B30 File Offset: 0x00017D30
		public override int PassCount
		{
			get
			{
				return (base.ResultState.Status == TestStatus.Passed) ? 1 : 0;
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x0600070F RID: 1807 RVA: 0x00019B54 File Offset: 0x00017D54
		public override int SkipCount
		{
			get
			{
				return (base.ResultState.Status == TestStatus.Skipped) ? 1 : 0;
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000710 RID: 1808 RVA: 0x00019B78 File Offset: 0x00017D78
		public override int InconclusiveCount
		{
			get
			{
				return (base.ResultState.Status == TestStatus.Inconclusive) ? 1 : 0;
			}
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000711 RID: 1809 RVA: 0x00019B9C File Offset: 0x00017D9C
		public override bool HasChildren
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000712 RID: 1810 RVA: 0x00019BB0 File Offset: 0x00017DB0
		public override IEnumerable<ITestResult> Children
		{
			get
			{
				return new ITestResult[0];
			}
		}
	}
}
