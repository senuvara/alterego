﻿using System;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000F0 RID: 240
	public enum TestExecutionStatus
	{
		// Token: 0x04000185 RID: 389
		Running,
		// Token: 0x04000186 RID: 390
		StopRequested,
		// Token: 0x04000187 RID: 391
		AbortRequested
	}
}
