﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000A2 RID: 162
	public class ActionsHelper
	{
		// Token: 0x060004E0 RID: 1248 RVA: 0x000105C5 File Offset: 0x0000E7C5
		public static void ExecuteBeforeActions(IEnumerable<ITestAction> actions, ITest test)
		{
			ActionsHelper.ExecuteActions(ActionsHelper.ActionPhase.Before, actions, test);
		}

		// Token: 0x060004E1 RID: 1249 RVA: 0x000105D1 File Offset: 0x0000E7D1
		public static void ExecuteAfterActions(IEnumerable<ITestAction> actions, ITest test)
		{
			ActionsHelper.ExecuteActions(ActionsHelper.ActionPhase.After, actions, test);
		}

		// Token: 0x060004E2 RID: 1250 RVA: 0x000105E0 File Offset: 0x0000E7E0
		private static void ExecuteActions(ActionsHelper.ActionPhase phase, IEnumerable<ITestAction> actions, ITest test)
		{
			if (actions != null)
			{
				foreach (ITestAction testAction in ActionsHelper.GetFilteredAndSortedActions(actions, phase))
				{
					if (phase == ActionsHelper.ActionPhase.Before)
					{
						testAction.BeforeTest(test);
					}
					else
					{
						testAction.AfterTest(test);
					}
				}
			}
		}

		// Token: 0x060004E3 RID: 1251 RVA: 0x0001063C File Offset: 0x0000E83C
		public static ITestAction[] GetActionsFromTestAssembly(TestAssembly testAssembly)
		{
			return ActionsHelper.GetActionsFromAttributeProvider(testAssembly.Assembly);
		}

		// Token: 0x060004E4 RID: 1252 RVA: 0x0001065C File Offset: 0x0000E85C
		public static ITestAction[] GetActionsFromTestMethodInfo(IMethodInfo testAssembly)
		{
			return ActionsHelper.GetActionsFromAttributeProvider(testAssembly.MethodInfo);
		}

		// Token: 0x060004E5 RID: 1253 RVA: 0x0001067C File Offset: 0x0000E87C
		public static ITestAction[] GetActionsFromAttributeProvider(ICustomAttributeProvider attributeProvider)
		{
			ITestAction[] result;
			if (attributeProvider == null)
			{
				result = new ITestAction[0];
			}
			else
			{
				List<ITestAction> list = new List<ITestAction>((ITestAction[])attributeProvider.GetCustomAttributes(typeof(ITestAction), false));
				list.Sort(new Comparison<ITestAction>(ActionsHelper.SortByTargetDescending));
				result = list.ToArray();
			}
			return result;
		}

		// Token: 0x060004E6 RID: 1254 RVA: 0x000106D8 File Offset: 0x0000E8D8
		public static ITestAction[] GetActionsFromTypesAttributes(Type type)
		{
			ITestAction[] result;
			if (type == null)
			{
				result = new ITestAction[0];
			}
			else if (type == typeof(object))
			{
				result = new ITestAction[0];
			}
			else
			{
				List<ITestAction> list = new List<ITestAction>();
				list.AddRange(ActionsHelper.GetActionsFromTypesAttributes(type.GetTypeInfo().BaseType));
				Type[] declaredInterfaces = ActionsHelper.GetDeclaredInterfaces(type);
				foreach (Type type2 in declaredInterfaces)
				{
					list.AddRange(ActionsHelper.GetActionsFromAttributeProvider(type2.GetTypeInfo()));
				}
				list.AddRange(ActionsHelper.GetActionsFromAttributeProvider(type.GetTypeInfo()));
				result = list.ToArray();
			}
			return result;
		}

		// Token: 0x060004E7 RID: 1255 RVA: 0x00010794 File Offset: 0x0000E994
		private static Type[] GetDeclaredInterfaces(Type type)
		{
			List<Type> list = new List<Type>(type.GetInterfaces());
			Type[] result;
			if (type.GetTypeInfo().BaseType == typeof(object))
			{
				result = list.ToArray();
			}
			else
			{
				List<Type> list2 = new List<Type>(type.GetTypeInfo().BaseType.GetInterfaces());
				List<Type> list3 = new List<Type>();
				foreach (Type item in list)
				{
					if (!list2.Contains(item))
					{
						list3.Add(item);
					}
				}
				result = list3.ToArray();
			}
			return result;
		}

		// Token: 0x060004E8 RID: 1256 RVA: 0x0001085C File Offset: 0x0000EA5C
		private static ITestAction[] GetFilteredAndSortedActions(IEnumerable<ITestAction> actions, ActionsHelper.ActionPhase phase)
		{
			List<ITestAction> list = new List<ITestAction>();
			foreach (ITestAction item in actions)
			{
				if (!list.Contains(item))
				{
					list.Add(item);
				}
			}
			if (phase == ActionsHelper.ActionPhase.After)
			{
				list.Reverse();
			}
			return list.ToArray();
		}

		// Token: 0x060004E9 RID: 1257 RVA: 0x000108E8 File Offset: 0x0000EAE8
		private static int SortByTargetDescending(ITestAction x, ITestAction y)
		{
			return y.Targets.CompareTo(x.Targets);
		}

		// Token: 0x060004EA RID: 1258 RVA: 0x00010915 File Offset: 0x0000EB15
		public ActionsHelper()
		{
		}

		// Token: 0x020000A3 RID: 163
		private enum ActionPhase
		{
			// Token: 0x040000F2 RID: 242
			Before,
			// Token: 0x040000F3 RID: 243
			After
		}
	}
}
