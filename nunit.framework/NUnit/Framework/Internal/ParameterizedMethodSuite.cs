﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000160 RID: 352
	public class ParameterizedMethodSuite : TestSuite
	{
		// Token: 0x0600092E RID: 2350 RVA: 0x0001EF6A File Offset: 0x0001D16A
		public ParameterizedMethodSuite(IMethodInfo method) : base(method.TypeInfo.FullName, method.Name)
		{
			base.Method = method;
			this._isTheory = method.IsDefined<TheoryAttribute>(true);
			base.MaintainTestOrder = true;
		}

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x0600092F RID: 2351 RVA: 0x0001EFA4 File Offset: 0x0001D1A4
		public override string TestType
		{
			get
			{
				string result;
				if (this._isTheory)
				{
					result = "Theory";
				}
				else if (base.Method.ContainsGenericParameters)
				{
					result = "GenericMethod";
				}
				else
				{
					result = "ParameterizedMethod";
				}
				return result;
			}
		}

		// Token: 0x04000209 RID: 521
		private bool _isTheory;
	}
}
