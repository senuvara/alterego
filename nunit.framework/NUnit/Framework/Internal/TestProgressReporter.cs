﻿using System;
using System.Web.UI;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000095 RID: 149
	public class TestProgressReporter : ITestListener
	{
		// Token: 0x060004BA RID: 1210 RVA: 0x0000F9E9 File Offset: 0x0000DBE9
		public TestProgressReporter(ICallbackEventHandler handler)
		{
			this.handler = handler;
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x0000F9FC File Offset: 0x0000DBFC
		public void TestStarted(ITest test)
		{
			string text = (test is TestSuite) ? "start-suite" : "start-test";
			ITest parent = TestProgressReporter.GetParent(test);
			try
			{
				string report = string.Format("<{0} id=\"{1}\" parentId=\"{2}\" name=\"{3}\" fullname=\"{4}\"/>", new object[]
				{
					text,
					test.Id,
					(parent != null) ? parent.Id : string.Empty,
					TestProgressReporter.FormatAttributeValue(test.Name),
					TestProgressReporter.FormatAttributeValue(test.FullName)
				});
				this.handler.RaiseCallbackEvent(report);
			}
			catch (Exception ex)
			{
				TestProgressReporter.log.Error("Exception processing " + test.FullName + Env.NewLine + ex.ToString());
			}
		}

		// Token: 0x060004BC RID: 1212 RVA: 0x0000FACC File Offset: 0x0000DCCC
		public void TestFinished(ITestResult result)
		{
			try
			{
				TNode tnode = result.ToXml(false);
				ITest parent = TestProgressReporter.GetParent(result.Test);
				tnode.Attributes.Add("parentId", (parent != null) ? parent.Id : string.Empty);
				this.handler.RaiseCallbackEvent(tnode.OuterXml);
			}
			catch (Exception ex)
			{
				TestProgressReporter.log.Error("Exception processing " + result.FullName + Env.NewLine + ex.ToString());
			}
		}

		// Token: 0x060004BD RID: 1213 RVA: 0x0000FB64 File Offset: 0x0000DD64
		public void TestOutput(TestOutput output)
		{
			try
			{
				this.handler.RaiseCallbackEvent(output.ToXml());
			}
			catch (Exception ex)
			{
				TestProgressReporter.log.Error("Exception processing TestOutput event" + Env.NewLine + ex.ToString());
			}
		}

		// Token: 0x060004BE RID: 1214 RVA: 0x0000FBC0 File Offset: 0x0000DDC0
		private static ITest GetParent(ITest test)
		{
			ITest result;
			if (test == null || test.Parent == null)
			{
				result = null;
			}
			else
			{
				result = (test.Parent.IsSuite ? test.Parent : TestProgressReporter.GetParent(test.Parent));
			}
			return result;
		}

		// Token: 0x060004BF RID: 1215 RVA: 0x0000FC10 File Offset: 0x0000DE10
		private static string FormatAttributeValue(string original)
		{
			return original.Replace("&", "&amp;").Replace("\"", "&quot;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;");
		}

		// Token: 0x060004C0 RID: 1216 RVA: 0x0000FC6E File Offset: 0x0000DE6E
		// Note: this type is marked as 'beforefieldinit'.
		static TestProgressReporter()
		{
		}

		// Token: 0x040000E0 RID: 224
		private static Logger log = InternalTrace.GetLogger("TestProgressReporter");

		// Token: 0x040000E1 RID: 225
		private ICallbackEventHandler handler;
	}
}
