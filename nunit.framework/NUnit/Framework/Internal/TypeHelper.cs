﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Text;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000E5 RID: 229
	public class TypeHelper
	{
		// Token: 0x060006A4 RID: 1700 RVA: 0x000179C0 File Offset: 0x00015BC0
		public static string GetDisplayName(Type type)
		{
			string result;
			if (type.IsGenericParameter)
			{
				result = type.Name;
			}
			else if (type.GetTypeInfo().IsGenericType)
			{
				string text = type.FullName;
				int num = text.IndexOf('[');
				if (num >= 0)
				{
					text = text.Substring(0, num);
				}
				num = text.LastIndexOf('.');
				if (num >= 0)
				{
					text = text.Substring(num + 1);
				}
				Type[] genericArguments = type.GetGenericArguments();
				int num2 = 0;
				StringBuilder stringBuilder = new StringBuilder();
				bool flag = false;
				foreach (string text2 in text.Split(new char[]
				{
					'+'
				}))
				{
					if (flag)
					{
						stringBuilder.Append("+");
					}
					flag = true;
					num = text2.IndexOf('`');
					if (num >= 0)
					{
						string value = text2.Substring(0, num);
						stringBuilder.Append(value);
						stringBuilder.Append("<");
						int num3 = int.Parse(text2.Substring(num + 1));
						for (int j = 0; j < num3; j++)
						{
							if (j > 0)
							{
								stringBuilder.Append(",");
							}
							stringBuilder.Append(TypeHelper.GetDisplayName(genericArguments[num2++]));
						}
						stringBuilder.Append(">");
					}
					else
					{
						stringBuilder.Append(text2);
					}
				}
				result = stringBuilder.ToString();
			}
			else
			{
				int num4 = type.FullName.LastIndexOf('.');
				result = ((num4 >= 0) ? type.FullName.Substring(num4 + 1) : type.FullName);
			}
			return result;
		}

		// Token: 0x060006A5 RID: 1701 RVA: 0x00017B9C File Offset: 0x00015D9C
		public static string GetDisplayName(Type type, object[] arglist)
		{
			string displayName = TypeHelper.GetDisplayName(type);
			string result;
			if (arglist == null || arglist.Length == 0)
			{
				result = displayName;
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder(displayName);
				stringBuilder.Append("(");
				for (int i = 0; i < arglist.Length; i++)
				{
					if (i > 0)
					{
						stringBuilder.Append(",");
					}
					object obj = arglist[i];
					string text = (obj == null) ? "null" : obj.ToString();
					if (obj is double || obj is float)
					{
						if (text.IndexOf('.') == -1)
						{
							text += ".0";
						}
						text += ((obj is double) ? "d" : "f");
					}
					else if (obj is decimal)
					{
						text += "m";
					}
					else if (obj is long)
					{
						text += "L";
					}
					else if (obj is ulong)
					{
						text += "UL";
					}
					else if (obj is string)
					{
						if (text.Length > 40)
						{
							text = text.Substring(0, 37) + "...";
						}
						text = "\"" + text + "\"";
					}
					stringBuilder.Append(text);
				}
				stringBuilder.Append(")");
				result = stringBuilder.ToString();
			}
			return result;
		}

		// Token: 0x060006A6 RID: 1702 RVA: 0x00017D6C File Offset: 0x00015F6C
		public static Type BestCommonType(Type type1, Type type2)
		{
			Type result;
			if (type1 == TypeHelper.NonmatchingType)
			{
				result = TypeHelper.NonmatchingType;
			}
			else if (type2 == TypeHelper.NonmatchingType)
			{
				result = TypeHelper.NonmatchingType;
			}
			else if (type1 == type2)
			{
				result = type1;
			}
			else if (type1 == null)
			{
				result = type2;
			}
			else if (type2 == null)
			{
				result = type1;
			}
			else
			{
				if (TypeHelper.IsNumeric(type1) && TypeHelper.IsNumeric(type2))
				{
					if (type1 == typeof(double))
					{
						return type1;
					}
					if (type2 == typeof(double))
					{
						return type2;
					}
					if (type1 == typeof(float))
					{
						return type1;
					}
					if (type2 == typeof(float))
					{
						return type2;
					}
					if (type1 == typeof(decimal))
					{
						return type1;
					}
					if (type2 == typeof(decimal))
					{
						return type2;
					}
					if (type1 == typeof(ulong))
					{
						return type1;
					}
					if (type2 == typeof(ulong))
					{
						return type2;
					}
					if (type1 == typeof(long))
					{
						return type1;
					}
					if (type2 == typeof(long))
					{
						return type2;
					}
					if (type1 == typeof(uint))
					{
						return type1;
					}
					if (type2 == typeof(uint))
					{
						return type2;
					}
					if (type1 == typeof(int))
					{
						return type1;
					}
					if (type2 == typeof(int))
					{
						return type2;
					}
					if (type1 == typeof(ushort))
					{
						return type1;
					}
					if (type2 == typeof(ushort))
					{
						return type2;
					}
					if (type1 == typeof(short))
					{
						return type1;
					}
					if (type2 == typeof(short))
					{
						return type2;
					}
					if (type1 == typeof(byte))
					{
						return type1;
					}
					if (type2 == typeof(byte))
					{
						return type2;
					}
					if (type1 == typeof(sbyte))
					{
						return type1;
					}
					if (type2 == typeof(sbyte))
					{
						return type2;
					}
				}
				if (type1.IsAssignableFrom(type2))
				{
					result = type1;
				}
				else if (type2.IsAssignableFrom(type1))
				{
					result = type2;
				}
				else
				{
					result = TypeHelper.NonmatchingType;
				}
			}
			return result;
		}

		// Token: 0x060006A7 RID: 1703 RVA: 0x00018074 File Offset: 0x00016274
		public static bool IsNumeric(Type type)
		{
			return type == typeof(double) || type == typeof(float) || type == typeof(decimal) || type == typeof(long) || type == typeof(int) || type == typeof(short) || type == typeof(ulong) || type == typeof(uint) || type == typeof(ushort) || type == typeof(byte) || type == typeof(sbyte);
		}

		// Token: 0x060006A8 RID: 1704 RVA: 0x0001811C File Offset: 0x0001631C
		public static void ConvertArgumentList(object[] arglist, IParameterInfo[] parameters)
		{
			Debug.Assert(arglist.Length <= parameters.Length);
			for (int i = 0; i < arglist.Length; i++)
			{
				object obj = arglist[i];
				if (obj != null && obj is IConvertible)
				{
					Type type = obj.GetType();
					Type parameterType = parameters[i].ParameterType;
					bool flag = false;
					if (type != parameterType && !type.IsAssignableFrom(parameterType))
					{
						if (TypeHelper.IsNumeric(type) && TypeHelper.IsNumeric(parameterType))
						{
							if (parameterType == typeof(double) || parameterType == typeof(float))
							{
								flag = (obj is int || obj is long || obj is short || obj is byte || obj is sbyte);
							}
							else if (parameterType == typeof(long))
							{
								flag = (obj is int || obj is short || obj is byte || obj is sbyte);
							}
							else if (parameterType == typeof(short))
							{
								flag = (obj is byte || obj is sbyte);
							}
						}
					}
					if (flag)
					{
						arglist[i] = Convert.ChangeType(obj, parameterType, CultureInfo.InvariantCulture);
					}
				}
			}
		}

		// Token: 0x060006A9 RID: 1705 RVA: 0x000182A0 File Offset: 0x000164A0
		public static bool CanDeduceTypeArgsFromArgs(Type type, object[] arglist, ref Type[] typeArgsOut)
		{
			Type[] genericArguments = type.GetGenericArguments();
			foreach (ConstructorInfo constructorInfo in type.GetConstructors())
			{
				ParameterInfo[] parameters = constructorInfo.GetParameters();
				if (parameters.Length == arglist.Length)
				{
					Type[] array = new Type[genericArguments.Length];
					for (int j = 0; j < array.Length; j++)
					{
						for (int k = 0; k < arglist.Length; k++)
						{
							if (genericArguments[j].IsGenericParameter || parameters[k].ParameterType.Equals(genericArguments[j]))
							{
								array[j] = TypeHelper.BestCommonType(array[j], arglist[k].GetType());
							}
						}
						if (array[j] == null)
						{
							array = null;
							break;
						}
					}
					if (array != null)
					{
						typeArgsOut = array;
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x060006AA RID: 1706 RVA: 0x000183B0 File Offset: 0x000165B0
		public static Array GetEnumValues(Type enumType)
		{
			return Enum.GetValues(enumType);
		}

		// Token: 0x060006AB RID: 1707 RVA: 0x000183C8 File Offset: 0x000165C8
		public static string[] GetEnumNames(Type enumType)
		{
			return Enum.GetNames(enumType);
		}

		// Token: 0x060006AC RID: 1708 RVA: 0x000183F1 File Offset: 0x000165F1
		public TypeHelper()
		{
		}

		// Token: 0x060006AD RID: 1709 RVA: 0x000183E0 File Offset: 0x000165E0
		// Note: this type is marked as 'beforefieldinit'.
		static TypeHelper()
		{
		}

		// Token: 0x04000169 RID: 361
		private const int STRING_MAX = 40;

		// Token: 0x0400016A RID: 362
		private const int STRING_LIMIT = 37;

		// Token: 0x0400016B RID: 363
		private const string THREE_DOTS = "...";

		// Token: 0x0400016C RID: 364
		public static readonly Type NonmatchingType = typeof(TypeHelper.NonmatchingTypeClass);

		// Token: 0x020000E6 RID: 230
		internal sealed class NonmatchingTypeClass
		{
			// Token: 0x060006AE RID: 1710 RVA: 0x000183F9 File Offset: 0x000165F9
			public NonmatchingTypeClass()
			{
			}
		}
	}
}
