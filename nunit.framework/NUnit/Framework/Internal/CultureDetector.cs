﻿using System;
using System.Globalization;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000049 RID: 73
	public class CultureDetector
	{
		// Token: 0x0600024B RID: 587 RVA: 0x00007E3F File Offset: 0x0000603F
		public CultureDetector()
		{
			this.currentCulture = CultureInfo.CurrentCulture;
		}

		// Token: 0x0600024C RID: 588 RVA: 0x00007E60 File Offset: 0x00006060
		public CultureDetector(string culture)
		{
			this.currentCulture = new CultureInfo(culture);
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00007E84 File Offset: 0x00006084
		public bool IsCultureSupported(string[] cultures)
		{
			foreach (string culture in cultures)
			{
				if (this.IsCultureSupported(culture))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600024E RID: 590 RVA: 0x00007EC8 File Offset: 0x000060C8
		public bool IsCultureSupported(CultureAttribute cultureAttribute)
		{
			string include = cultureAttribute.Include;
			string exclude = cultureAttribute.Exclude;
			bool result;
			if (include != null && !this.IsCultureSupported(include))
			{
				this.reason = string.Format("Only supported under culture {0}", include);
				result = false;
			}
			else if (exclude != null && this.IsCultureSupported(exclude))
			{
				this.reason = string.Format("Not supported under culture {0}", exclude);
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x0600024F RID: 591 RVA: 0x00007F3C File Offset: 0x0000613C
		public bool IsCultureSupported(string culture)
		{
			culture = culture.Trim();
			if (culture.IndexOf(',') >= 0)
			{
				if (this.IsCultureSupported(culture.Split(new char[]
				{
					','
				})))
				{
					return true;
				}
			}
			else if (this.currentCulture.Name == culture || this.currentCulture.TwoLetterISOLanguageName == culture)
			{
				return true;
			}
			this.reason = "Only supported under culture " + culture;
			return false;
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000250 RID: 592 RVA: 0x00007FD4 File Offset: 0x000061D4
		public string Reason
		{
			get
			{
				return this.reason;
			}
		}

		// Token: 0x04000078 RID: 120
		private CultureInfo currentCulture;

		// Token: 0x04000079 RID: 121
		private string reason = string.Empty;
	}
}
