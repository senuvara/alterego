﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x0200001D RID: 29
	public abstract class TestResult : ITestResult, IXmlNodeBuilder
	{
		// Token: 0x060000CE RID: 206 RVA: 0x00003D08 File Offset: 0x00001F08
		public TestResult(ITest test)
		{
			this.Test = test;
			this.ResultState = ResultState.Inconclusive;
			this.OutWriter = TextWriter.Synchronized(new StringWriter(this._output));
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000CF RID: 207 RVA: 0x00003D54 File Offset: 0x00001F54
		// (set) Token: 0x060000D0 RID: 208 RVA: 0x00003D6B File Offset: 0x00001F6B
		public ITest Test
		{
			[CompilerGenerated]
			get
			{
				return this.<Test>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Test>k__BackingField = value;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000D1 RID: 209 RVA: 0x00003D74 File Offset: 0x00001F74
		// (set) Token: 0x060000D2 RID: 210 RVA: 0x00003DA4 File Offset: 0x00001FA4
		public ResultState ResultState
		{
			get
			{
				ResultState resultState;
				try
				{
					resultState = this._resultState;
				}
				finally
				{
				}
				return resultState;
			}
			private set
			{
				this._resultState = value;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000D3 RID: 211 RVA: 0x00003DB0 File Offset: 0x00001FB0
		public virtual string Name
		{
			get
			{
				return this.Test.Name;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000D4 RID: 212 RVA: 0x00003DD0 File Offset: 0x00001FD0
		public virtual string FullName
		{
			get
			{
				return this.Test.FullName;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x00003DF0 File Offset: 0x00001FF0
		// (set) Token: 0x060000D6 RID: 214 RVA: 0x00003E08 File Offset: 0x00002008
		public double Duration
		{
			get
			{
				return this._duration;
			}
			set
			{
				this._duration = ((value >= 1E-06) ? value : 1E-06);
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060000D7 RID: 215 RVA: 0x00003E2C File Offset: 0x0000202C
		// (set) Token: 0x060000D8 RID: 216 RVA: 0x00003E43 File Offset: 0x00002043
		public DateTime StartTime
		{
			[CompilerGenerated]
			get
			{
				return this.<StartTime>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<StartTime>k__BackingField = value;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060000D9 RID: 217 RVA: 0x00003E4C File Offset: 0x0000204C
		// (set) Token: 0x060000DA RID: 218 RVA: 0x00003E63 File Offset: 0x00002063
		public DateTime EndTime
		{
			[CompilerGenerated]
			get
			{
				return this.<EndTime>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<EndTime>k__BackingField = value;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00003E6C File Offset: 0x0000206C
		// (set) Token: 0x060000DC RID: 220 RVA: 0x00003E9C File Offset: 0x0000209C
		public string Message
		{
			get
			{
				string message;
				try
				{
					message = this._message;
				}
				finally
				{
				}
				return message;
			}
			private set
			{
				this._message = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060000DD RID: 221 RVA: 0x00003EA8 File Offset: 0x000020A8
		// (set) Token: 0x060000DE RID: 222 RVA: 0x00003ED8 File Offset: 0x000020D8
		public virtual string StackTrace
		{
			get
			{
				string stackTrace;
				try
				{
					stackTrace = this._stackTrace;
				}
				finally
				{
				}
				return stackTrace;
			}
			private set
			{
				this._stackTrace = value;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000DF RID: 223 RVA: 0x00003EE4 File Offset: 0x000020E4
		// (set) Token: 0x060000E0 RID: 224 RVA: 0x00003F14 File Offset: 0x00002114
		public int AssertCount
		{
			get
			{
				int internalAssertCount;
				try
				{
					internalAssertCount = this.InternalAssertCount;
				}
				finally
				{
				}
				return internalAssertCount;
			}
			internal set
			{
				this.InternalAssertCount = value;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000E1 RID: 225
		public abstract int FailCount { get; }

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000E2 RID: 226
		public abstract int PassCount { get; }

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000E3 RID: 227
		public abstract int SkipCount { get; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000E4 RID: 228
		public abstract int InconclusiveCount { get; }

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000E5 RID: 229
		public abstract bool HasChildren { get; }

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060000E6 RID: 230
		public abstract IEnumerable<ITestResult> Children { get; }

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000E7 RID: 231 RVA: 0x00003F20 File Offset: 0x00002120
		// (set) Token: 0x060000E8 RID: 232 RVA: 0x00003F37 File Offset: 0x00002137
		public TextWriter OutWriter
		{
			[CompilerGenerated]
			get
			{
				return this.<OutWriter>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<OutWriter>k__BackingField = value;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000E9 RID: 233 RVA: 0x00003F40 File Offset: 0x00002140
		public string Output
		{
			get
			{
				return this._output.ToString();
			}
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00003F60 File Offset: 0x00002160
		public TNode ToXml(bool recursive)
		{
			return this.AddToXml(new TNode("dummy"), recursive);
		}

		// Token: 0x060000EB RID: 235 RVA: 0x00003F84 File Offset: 0x00002184
		public virtual TNode AddToXml(TNode parentNode, bool recursive)
		{
			TNode tnode = this.Test.AddToXml(parentNode, false);
			tnode.AddAttribute("result", this.ResultState.Status.ToString());
			if (this.ResultState.Label != string.Empty)
			{
				tnode.AddAttribute("label", this.ResultState.Label);
			}
			if (this.ResultState.Site != FailureSite.Test)
			{
				tnode.AddAttribute("site", this.ResultState.Site.ToString());
			}
			tnode.AddAttribute("start-time", this.StartTime.ToString("u"));
			tnode.AddAttribute("end-time", this.EndTime.ToString("u"));
			tnode.AddAttribute("duration", this.Duration.ToString("0.000000", NumberFormatInfo.InvariantInfo));
			if (this.Test is TestSuite)
			{
				tnode.AddAttribute("total", (this.PassCount + this.FailCount + this.SkipCount + this.InconclusiveCount).ToString());
				tnode.AddAttribute("passed", this.PassCount.ToString());
				tnode.AddAttribute("failed", this.FailCount.ToString());
				tnode.AddAttribute("inconclusive", this.InconclusiveCount.ToString());
				tnode.AddAttribute("skipped", this.SkipCount.ToString());
			}
			tnode.AddAttribute("asserts", this.AssertCount.ToString());
			switch (this.ResultState.Status)
			{
			case TestStatus.Inconclusive:
			case TestStatus.Skipped:
			case TestStatus.Passed:
				if (this.Message != null)
				{
					this.AddReasonElement(tnode);
				}
				break;
			case TestStatus.Failed:
				this.AddFailureElement(tnode);
				break;
			}
			if (this.Output.Length > 0)
			{
				this.AddOutputElement(tnode);
			}
			if (recursive && this.HasChildren)
			{
				foreach (ITestResult testResult in this.Children)
				{
					TestResult testResult2 = (TestResult)testResult;
					testResult2.AddToXml(tnode, recursive);
				}
			}
			return tnode;
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00004230 File Offset: 0x00002430
		public void SetResult(ResultState resultState)
		{
			this.SetResult(resultState, null, null);
		}

		// Token: 0x060000ED RID: 237 RVA: 0x0000423D File Offset: 0x0000243D
		public void SetResult(ResultState resultState, string message)
		{
			this.SetResult(resultState, message, null);
		}

		// Token: 0x060000EE RID: 238 RVA: 0x0000424C File Offset: 0x0000244C
		public void SetResult(ResultState resultState, string message, string stackTrace)
		{
			try
			{
				this.ResultState = resultState;
				this.Message = message;
				this.StackTrace = stackTrace;
			}
			finally
			{
			}
		}

		// Token: 0x060000EF RID: 239 RVA: 0x0000428C File Offset: 0x0000248C
		public void RecordException(Exception ex)
		{
			if (ex is NUnitException)
			{
				ex = ex.InnerException;
			}
			if (ex is ResultStateException)
			{
				this.SetResult(((ResultStateException)ex).ResultState, ex.Message, StackFilter.Filter(ex.StackTrace));
			}
			else if (ex is ThreadAbortException)
			{
				this.SetResult(ResultState.Cancelled, "Test cancelled by user", ex.StackTrace);
			}
			else
			{
				this.SetResult(ResultState.Error, ExceptionHelper.BuildMessage(ex), ExceptionHelper.BuildStackTrace(ex));
			}
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00004328 File Offset: 0x00002528
		public void RecordException(Exception ex, FailureSite site)
		{
			if (ex is NUnitException)
			{
				ex = ex.InnerException;
			}
			if (ex is ResultStateException)
			{
				this.SetResult(((ResultStateException)ex).ResultState.WithSite(site), ex.Message, StackFilter.Filter(ex.StackTrace));
			}
			else if (ex is ThreadAbortException)
			{
				this.SetResult(ResultState.Cancelled.WithSite(site), "Test cancelled by user", ex.StackTrace);
			}
			else
			{
				this.SetResult(ResultState.Error.WithSite(site), ExceptionHelper.BuildMessage(ex), ExceptionHelper.BuildStackTrace(ex));
			}
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x000043D8 File Offset: 0x000025D8
		public void RecordTearDownException(Exception ex)
		{
			if (ex is NUnitException)
			{
				ex = ex.InnerException;
			}
			ResultState resultState = (this.ResultState == ResultState.Cancelled) ? ResultState.Cancelled : ResultState.Error;
			if (this.Test.IsSuite)
			{
				resultState = resultState.WithSite(FailureSite.TearDown);
			}
			string text = "TearDown : " + ExceptionHelper.BuildMessage(ex);
			if (this.Message != null)
			{
				text = this.Message + Env.NewLine + text;
			}
			string text2 = "--TearDown" + Env.NewLine + ExceptionHelper.BuildStackTrace(ex);
			if (this.StackTrace != null)
			{
				text2 = this.StackTrace + Env.NewLine + text2;
			}
			this.SetResult(resultState, text, text2);
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x000044A4 File Offset: 0x000026A4
		private TNode AddReasonElement(TNode targetNode)
		{
			TNode tnode = targetNode.AddElement("reason");
			return tnode.AddElementWithCDATA("message", this.Message);
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x000044D4 File Offset: 0x000026D4
		private TNode AddFailureElement(TNode targetNode)
		{
			TNode tnode = targetNode.AddElement("failure");
			if (this.Message != null)
			{
				tnode.AddElementWithCDATA("message", this.Message);
			}
			if (this.StackTrace != null)
			{
				tnode.AddElementWithCDATA("stack-trace", this.StackTrace);
			}
			return tnode;
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x00004534 File Offset: 0x00002734
		private TNode AddOutputElement(TNode targetNode)
		{
			return targetNode.AddElementWithCDATA("output", this.Output);
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x00004557 File Offset: 0x00002757
		// Note: this type is marked as 'beforefieldinit'.
		static TestResult()
		{
		}

		// Token: 0x0400001A RID: 26
		internal const double MIN_DURATION = 1E-06;

		// Token: 0x0400001B RID: 27
		internal static readonly string CHILD_ERRORS_MESSAGE = "One or more child tests had errors";

		// Token: 0x0400001C RID: 28
		internal static readonly string CHILD_IGNORE_MESSAGE = "One or more child tests were ignored";

		// Token: 0x0400001D RID: 29
		private StringBuilder _output = new StringBuilder();

		// Token: 0x0400001E RID: 30
		private double _duration;

		// Token: 0x0400001F RID: 31
		protected int InternalAssertCount;

		// Token: 0x04000020 RID: 32
		private ResultState _resultState;

		// Token: 0x04000021 RID: 33
		private string _message;

		// Token: 0x04000022 RID: 34
		private string _stackTrace;

		// Token: 0x04000023 RID: 35
		[CompilerGenerated]
		private ITest <Test>k__BackingField;

		// Token: 0x04000024 RID: 36
		[CompilerGenerated]
		private DateTime <StartTime>k__BackingField;

		// Token: 0x04000025 RID: 37
		[CompilerGenerated]
		private DateTime <EndTime>k__BackingField;

		// Token: 0x04000026 RID: 38
		[CompilerGenerated]
		private TextWriter <OutWriter>k__BackingField;
	}
}
