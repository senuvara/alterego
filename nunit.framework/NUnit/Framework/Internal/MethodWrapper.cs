﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000181 RID: 385
	public class MethodWrapper : IMethodInfo, IReflectionInfo
	{
		// Token: 0x06000A13 RID: 2579 RVA: 0x00021ED1 File Offset: 0x000200D1
		public MethodWrapper(Type type, MethodInfo method)
		{
			this.TypeInfo = new TypeWrapper(type);
			this.MethodInfo = method;
		}

		// Token: 0x06000A14 RID: 2580 RVA: 0x00021EF1 File Offset: 0x000200F1
		public MethodWrapper(Type type, string methodName)
		{
			this.TypeInfo = new TypeWrapper(type);
			this.MethodInfo = type.GetMethod(methodName);
		}

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x06000A15 RID: 2581 RVA: 0x00021F18 File Offset: 0x00020118
		// (set) Token: 0x06000A16 RID: 2582 RVA: 0x00021F2F File Offset: 0x0002012F
		public ITypeInfo TypeInfo
		{
			[CompilerGenerated]
			get
			{
				return this.<TypeInfo>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<TypeInfo>k__BackingField = value;
			}
		}

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x06000A17 RID: 2583 RVA: 0x00021F38 File Offset: 0x00020138
		// (set) Token: 0x06000A18 RID: 2584 RVA: 0x00021F4F File Offset: 0x0002014F
		public MethodInfo MethodInfo
		{
			[CompilerGenerated]
			get
			{
				return this.<MethodInfo>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<MethodInfo>k__BackingField = value;
			}
		}

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x06000A19 RID: 2585 RVA: 0x00021F58 File Offset: 0x00020158
		public string Name
		{
			get
			{
				return this.MethodInfo.Name;
			}
		}

		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06000A1A RID: 2586 RVA: 0x00021F78 File Offset: 0x00020178
		public bool IsAbstract
		{
			get
			{
				return this.MethodInfo.IsAbstract;
			}
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x06000A1B RID: 2587 RVA: 0x00021F98 File Offset: 0x00020198
		public bool IsPublic
		{
			get
			{
				return this.MethodInfo.IsPublic;
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x06000A1C RID: 2588 RVA: 0x00021FB8 File Offset: 0x000201B8
		public bool ContainsGenericParameters
		{
			get
			{
				return this.MethodInfo.ContainsGenericParameters;
			}
		}

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x06000A1D RID: 2589 RVA: 0x00021FD8 File Offset: 0x000201D8
		public bool IsGenericMethod
		{
			get
			{
				return this.MethodInfo.IsGenericMethod;
			}
		}

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x06000A1E RID: 2590 RVA: 0x00021FF8 File Offset: 0x000201F8
		public bool IsGenericMethodDefinition
		{
			get
			{
				return this.MethodInfo.IsGenericMethodDefinition;
			}
		}

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x06000A1F RID: 2591 RVA: 0x00022018 File Offset: 0x00020218
		public ITypeInfo ReturnType
		{
			get
			{
				return new TypeWrapper(this.MethodInfo.ReturnType);
			}
		}

		// Token: 0x06000A20 RID: 2592 RVA: 0x0002203C File Offset: 0x0002023C
		public IParameterInfo[] GetParameters()
		{
			ParameterInfo[] parameters = this.MethodInfo.GetParameters();
			IParameterInfo[] array = new IParameterInfo[parameters.Length];
			for (int i = 0; i < parameters.Length; i++)
			{
				array[i] = new ParameterWrapper(this, parameters[i]);
			}
			return array;
		}

		// Token: 0x06000A21 RID: 2593 RVA: 0x00022084 File Offset: 0x00020284
		public Type[] GetGenericArguments()
		{
			return this.MethodInfo.GetGenericArguments();
		}

		// Token: 0x06000A22 RID: 2594 RVA: 0x000220A4 File Offset: 0x000202A4
		public IMethodInfo MakeGenericMethod(params Type[] typeArguments)
		{
			return new MethodWrapper(this.TypeInfo.Type, this.MethodInfo.MakeGenericMethod(typeArguments));
		}

		// Token: 0x06000A23 RID: 2595 RVA: 0x000220D4 File Offset: 0x000202D4
		public T[] GetCustomAttributes<T>(bool inherit) where T : class
		{
			return (T[])this.MethodInfo.GetCustomAttributes(typeof(T), inherit);
		}

		// Token: 0x06000A24 RID: 2596 RVA: 0x00022104 File Offset: 0x00020304
		public bool IsDefined<T>(bool inherit)
		{
			return this.MethodInfo.IsDefined(typeof(T), inherit);
		}

		// Token: 0x06000A25 RID: 2597 RVA: 0x0002212C File Offset: 0x0002032C
		public object Invoke(object fixture, params object[] args)
		{
			return Reflect.InvokeMethod(this.MethodInfo, fixture, args);
		}

		// Token: 0x06000A26 RID: 2598 RVA: 0x0002214C File Offset: 0x0002034C
		public override string ToString()
		{
			return this.MethodInfo.Name;
		}

		// Token: 0x04000262 RID: 610
		[CompilerGenerated]
		private ITypeInfo <TypeInfo>k__BackingField;

		// Token: 0x04000263 RID: 611
		[CompilerGenerated]
		private MethodInfo <MethodInfo>k__BackingField;
	}
}
