﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000021 RID: 33
	public class TestSuite : Test
	{
		// Token: 0x0600013A RID: 314 RVA: 0x00004DA1 File Offset: 0x00002FA1
		public TestSuite(string name) : base(name)
		{
			this.Arguments = new object[0];
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00004DC5 File Offset: 0x00002FC5
		public TestSuite(string parentSuiteName, string name) : base(parentSuiteName, name)
		{
			this.Arguments = new object[0];
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00004DEA File Offset: 0x00002FEA
		public TestSuite(ITypeInfo fixtureType) : base(fixtureType)
		{
			this.Arguments = new object[0];
		}

		// Token: 0x0600013D RID: 317 RVA: 0x00004E0E File Offset: 0x0000300E
		public TestSuite(Type fixtureType) : base(new TypeWrapper(fixtureType))
		{
			this.Arguments = new object[0];
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00004E38 File Offset: 0x00003038
		public void Sort()
		{
			if (!this.MaintainTestOrder)
			{
				this.tests.Sort();
				foreach (ITest test in this.Tests)
				{
					Test test2 = (Test)test;
					TestSuite testSuite = test2 as TestSuite;
					if (testSuite != null)
					{
						testSuite.Sort();
					}
				}
			}
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00004EC0 File Offset: 0x000030C0
		public void Add(Test test)
		{
			test.Parent = this;
			this.tests.Add(test);
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000140 RID: 320 RVA: 0x00004ED8 File Offset: 0x000030D8
		public override IList<ITest> Tests
		{
			get
			{
				return this.tests;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000141 RID: 321 RVA: 0x00004EF0 File Offset: 0x000030F0
		public override int TestCaseCount
		{
			get
			{
				int num = 0;
				foreach (ITest test in this.Tests)
				{
					Test test2 = (Test)test;
					num += test2.TestCaseCount;
				}
				return num;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000142 RID: 322 RVA: 0x00004F5C File Offset: 0x0000315C
		// (set) Token: 0x06000143 RID: 323 RVA: 0x00004F73 File Offset: 0x00003173
		public object[] Arguments
		{
			[CompilerGenerated]
			get
			{
				return this.<Arguments>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Arguments>k__BackingField = value;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000144 RID: 324 RVA: 0x00004F7C File Offset: 0x0000317C
		// (set) Token: 0x06000145 RID: 325 RVA: 0x00004F93 File Offset: 0x00003193
		protected bool MaintainTestOrder
		{
			[CompilerGenerated]
			get
			{
				return this.<MaintainTestOrder>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<MaintainTestOrder>k__BackingField = value;
			}
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00004F9C File Offset: 0x0000319C
		public override TestResult MakeTestResult()
		{
			return new TestSuiteResult(this);
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000147 RID: 327 RVA: 0x00004FB4 File Offset: 0x000031B4
		public override bool HasChildren
		{
			get
			{
				return this.tests.Count > 0;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000148 RID: 328 RVA: 0x00004FD4 File Offset: 0x000031D4
		public override string XmlElementName
		{
			get
			{
				return "test-suite";
			}
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00004FEC File Offset: 0x000031EC
		public override TNode AddToXml(TNode parentNode, bool recursive)
		{
			TNode tnode = parentNode.AddElement("test-suite");
			tnode.AddAttribute("type", this.TestType);
			base.PopulateTestNode(tnode, recursive);
			tnode.AddAttribute("testcasecount", this.TestCaseCount.ToString());
			if (recursive)
			{
				foreach (ITest test in this.Tests)
				{
					Test test2 = (Test)test;
					test2.AddToXml(tnode, recursive);
				}
			}
			return tnode;
		}

		// Token: 0x0600014A RID: 330 RVA: 0x000050A4 File Offset: 0x000032A4
		protected void CheckSetUpTearDownMethods(Type attrType)
		{
			foreach (MethodInfo methodInfo in Reflect.GetMethodsWithAttribute(base.TypeInfo.Type, attrType, true))
			{
				if (methodInfo.IsAbstract || (!methodInfo.IsPublic && !methodInfo.IsFamily) || methodInfo.GetParameters().Length > 0 || methodInfo.ReturnType != typeof(void))
				{
					base.Properties.Set("_SKIPREASON", string.Format("Invalid signature for SetUp or TearDown method: {0}", methodInfo.Name));
					base.RunState = RunState.NotRunnable;
					break;
				}
			}
		}

		// Token: 0x0400003C RID: 60
		private List<ITest> tests = new List<ITest>();

		// Token: 0x0400003D RID: 61
		[CompilerGenerated]
		private object[] <Arguments>k__BackingField;

		// Token: 0x0400003E RID: 62
		[CompilerGenerated]
		private bool <MaintainTestOrder>k__BackingField;
	}
}
