﻿using System;
using System.Collections;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000064 RID: 100
	[Serializable]
	public class PropertyFilter : ValueMatchFilter
	{
		// Token: 0x0600030C RID: 780 RVA: 0x0000B6DF File Offset: 0x000098DF
		public PropertyFilter(string propertyName, string expectedValue) : base(expectedValue)
		{
			this._propertyName = propertyName;
		}

		// Token: 0x0600030D RID: 781 RVA: 0x0000B6F4 File Offset: 0x000098F4
		public override bool Match(ITest test)
		{
			IList list = test.Properties[this._propertyName];
			if (list != null)
			{
				foreach (object obj in list)
				{
					string input = (string)obj;
					if (base.Match(input))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x0600030E RID: 782 RVA: 0x0000B788 File Offset: 0x00009988
		public override TNode AddToXml(TNode parentNode, bool recursive)
		{
			TNode tnode = base.AddToXml(parentNode, recursive);
			tnode.AddAttribute("name", this._propertyName);
			return tnode;
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x0600030F RID: 783 RVA: 0x0000B7B8 File Offset: 0x000099B8
		protected override string ElementName
		{
			get
			{
				return "prop";
			}
		}

		// Token: 0x040000A8 RID: 168
		private string _propertyName;
	}
}
