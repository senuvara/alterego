﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000007 RID: 7
	public abstract class CompositeFilter : TestFilter
	{
		// Token: 0x0600002F RID: 47 RVA: 0x000027DD File Offset: 0x000009DD
		public CompositeFilter()
		{
			this.Filters = new List<ITestFilter>();
		}

		// Token: 0x06000030 RID: 48 RVA: 0x000027F4 File Offset: 0x000009F4
		public CompositeFilter(params ITestFilter[] filters)
		{
			this.Filters = new List<ITestFilter>(filters);
		}

		// Token: 0x06000031 RID: 49 RVA: 0x0000280C File Offset: 0x00000A0C
		public void Add(ITestFilter filter)
		{
			this.Filters.Add(filter);
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000032 RID: 50 RVA: 0x0000281C File Offset: 0x00000A1C
		// (set) Token: 0x06000033 RID: 51 RVA: 0x00002833 File Offset: 0x00000A33
		public IList<ITestFilter> Filters
		{
			[CompilerGenerated]
			get
			{
				return this.<Filters>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Filters>k__BackingField = value;
			}
		}

		// Token: 0x06000034 RID: 52
		public abstract override bool Pass(ITest test);

		// Token: 0x06000035 RID: 53
		public abstract override bool Match(ITest test);

		// Token: 0x06000036 RID: 54
		public abstract override bool IsExplicitMatch(ITest test);

		// Token: 0x06000037 RID: 55 RVA: 0x0000283C File Offset: 0x00000A3C
		public override TNode AddToXml(TNode parentNode, bool recursive)
		{
			TNode tnode = parentNode.AddElement(this.ElementName);
			if (recursive)
			{
				foreach (ITestFilter testFilter in this.Filters)
				{
					testFilter.AddToXml(tnode, true);
				}
			}
			return tnode;
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000038 RID: 56
		protected abstract string ElementName { get; }

		// Token: 0x04000003 RID: 3
		[CompilerGenerated]
		private IList<ITestFilter> <Filters>k__BackingField;
	}
}
