﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000026 RID: 38
	[Serializable]
	public class FullNameFilter : ValueMatchFilter
	{
		// Token: 0x06000168 RID: 360 RVA: 0x000054D4 File Offset: 0x000036D4
		public FullNameFilter(string expectedValue) : base(expectedValue)
		{
		}

		// Token: 0x06000169 RID: 361 RVA: 0x000054E0 File Offset: 0x000036E0
		public override bool Match(ITest test)
		{
			return base.Match(test.FullName);
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x0600016A RID: 362 RVA: 0x00005500 File Offset: 0x00003700
		protected override string ElementName
		{
			get
			{
				return "test";
			}
		}
	}
}
