﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x020000DF RID: 223
	[Serializable]
	public class MethodNameFilter : ValueMatchFilter
	{
		// Token: 0x06000692 RID: 1682 RVA: 0x00017543 File Offset: 0x00015743
		public MethodNameFilter(string expectedValue) : base(expectedValue)
		{
		}

		// Token: 0x06000693 RID: 1683 RVA: 0x00017550 File Offset: 0x00015750
		public override bool Match(ITest test)
		{
			return base.Match(test.MethodName);
		}

		// Token: 0x17000178 RID: 376
		// (get) Token: 0x06000694 RID: 1684 RVA: 0x00017570 File Offset: 0x00015770
		protected override string ElementName
		{
			get
			{
				return "method";
			}
		}
	}
}
