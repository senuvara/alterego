﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x0200014C RID: 332
	[Serializable]
	public class IdFilter : ValueMatchFilter
	{
		// Token: 0x060008E5 RID: 2277 RVA: 0x0001E438 File Offset: 0x0001C638
		public IdFilter(string id) : base(id)
		{
		}

		// Token: 0x060008E6 RID: 2278 RVA: 0x0001E444 File Offset: 0x0001C644
		public override bool Match(ITest test)
		{
			return test.Id == base.ExpectedValue;
		}

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x060008E7 RID: 2279 RVA: 0x0001E468 File Offset: 0x0001C668
		protected override string ElementName
		{
			get
			{
				return "id";
			}
		}
	}
}
