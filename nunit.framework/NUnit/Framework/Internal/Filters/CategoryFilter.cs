﻿using System;
using System.Collections;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000176 RID: 374
	[Serializable]
	public class CategoryFilter : ValueMatchFilter
	{
		// Token: 0x060009EA RID: 2538 RVA: 0x00021907 File Offset: 0x0001FB07
		public CategoryFilter(string name) : base(name)
		{
		}

		// Token: 0x060009EB RID: 2539 RVA: 0x00021914 File Offset: 0x0001FB14
		public override bool Match(ITest test)
		{
			IList list = test.Properties["Category"];
			if (list != null)
			{
				foreach (object obj in list)
				{
					string input = (string)obj;
					if (base.Match(input))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x060009EC RID: 2540 RVA: 0x000219A4 File Offset: 0x0001FBA4
		protected override string ElementName
		{
			get
			{
				return "cat";
			}
		}
	}
}
