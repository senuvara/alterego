﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000164 RID: 356
	[Serializable]
	public class OrFilter : CompositeFilter
	{
		// Token: 0x0600095E RID: 2398 RVA: 0x0001F91E File Offset: 0x0001DB1E
		public OrFilter()
		{
		}

		// Token: 0x0600095F RID: 2399 RVA: 0x0001F929 File Offset: 0x0001DB29
		public OrFilter(params ITestFilter[] filters) : base(filters)
		{
		}

		// Token: 0x06000960 RID: 2400 RVA: 0x0001F938 File Offset: 0x0001DB38
		public override bool Pass(ITest test)
		{
			foreach (ITestFilter testFilter in base.Filters)
			{
				if (testFilter.Pass(test))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000961 RID: 2401 RVA: 0x0001F9A4 File Offset: 0x0001DBA4
		public override bool Match(ITest test)
		{
			foreach (ITestFilter testFilter in base.Filters)
			{
				TestFilter testFilter2 = (TestFilter)testFilter;
				if (testFilter2.Match(test))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000962 RID: 2402 RVA: 0x0001FA14 File Offset: 0x0001DC14
		public override bool IsExplicitMatch(ITest test)
		{
			foreach (ITestFilter testFilter in base.Filters)
			{
				TestFilter testFilter2 = (TestFilter)testFilter;
				if (testFilter2.IsExplicitMatch(test))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x06000963 RID: 2403 RVA: 0x0001FA84 File Offset: 0x0001DC84
		protected override string ElementName
		{
			get
			{
				return "or";
			}
		}
	}
}
