﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000025 RID: 37
	[Serializable]
	public abstract class ValueMatchFilter : TestFilter
	{
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000160 RID: 352 RVA: 0x000053F4 File Offset: 0x000035F4
		// (set) Token: 0x06000161 RID: 353 RVA: 0x0000540B File Offset: 0x0000360B
		public string ExpectedValue
		{
			[CompilerGenerated]
			get
			{
				return this.<ExpectedValue>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ExpectedValue>k__BackingField = value;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000162 RID: 354 RVA: 0x00005414 File Offset: 0x00003614
		// (set) Token: 0x06000163 RID: 355 RVA: 0x0000542B File Offset: 0x0000362B
		public bool IsRegex
		{
			[CompilerGenerated]
			get
			{
				return this.<IsRegex>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsRegex>k__BackingField = value;
			}
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00005434 File Offset: 0x00003634
		public ValueMatchFilter(string expectedValue)
		{
			this.ExpectedValue = expectedValue;
		}

		// Token: 0x06000165 RID: 357 RVA: 0x00005448 File Offset: 0x00003648
		protected bool Match(string input)
		{
			bool result;
			if (this.IsRegex)
			{
				result = (input != null && new Regex(this.ExpectedValue).IsMatch(input));
			}
			else
			{
				result = (this.ExpectedValue == input);
			}
			return result;
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00005490 File Offset: 0x00003690
		public override TNode AddToXml(TNode parentNode, bool recursive)
		{
			TNode tnode = parentNode.AddElement(this.ElementName, this.ExpectedValue);
			if (this.IsRegex)
			{
				tnode.AddAttribute("re", "1");
			}
			return tnode;
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000167 RID: 359
		protected abstract string ElementName { get; }

		// Token: 0x04000040 RID: 64
		[CompilerGenerated]
		private string <ExpectedValue>k__BackingField;

		// Token: 0x04000041 RID: 65
		[CompilerGenerated]
		private bool <IsRegex>k__BackingField;
	}
}
