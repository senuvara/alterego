﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000127 RID: 295
	[Serializable]
	public class TestNameFilter : ValueMatchFilter
	{
		// Token: 0x060007E8 RID: 2024 RVA: 0x0001BD84 File Offset: 0x00019F84
		public TestNameFilter(string expectedValue) : base(expectedValue)
		{
		}

		// Token: 0x060007E9 RID: 2025 RVA: 0x0001BD90 File Offset: 0x00019F90
		public override bool Match(ITest test)
		{
			return base.Match(test.Name);
		}

		// Token: 0x170001BA RID: 442
		// (get) Token: 0x060007EA RID: 2026 RVA: 0x0001BDB0 File Offset: 0x00019FB0
		protected override string ElementName
		{
			get
			{
				return "name";
			}
		}
	}
}
