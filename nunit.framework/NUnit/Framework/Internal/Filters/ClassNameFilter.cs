﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000063 RID: 99
	[Serializable]
	public class ClassNameFilter : ValueMatchFilter
	{
		// Token: 0x06000309 RID: 777 RVA: 0x0000B672 File Offset: 0x00009872
		public ClassNameFilter(string expectedValue) : base(expectedValue)
		{
		}

		// Token: 0x0600030A RID: 778 RVA: 0x0000B680 File Offset: 0x00009880
		public override bool Match(ITest test)
		{
			return test.IsSuite && !(test is ParameterizedMethodSuite) && test.ClassName != null && base.Match(test.ClassName);
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x0600030B RID: 779 RVA: 0x0000B6C8 File Offset: 0x000098C8
		protected override string ElementName
		{
			get
			{
				return "class";
			}
		}
	}
}
