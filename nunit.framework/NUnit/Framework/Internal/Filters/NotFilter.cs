﻿using System;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000027 RID: 39
	[Serializable]
	public class NotFilter : TestFilter
	{
		// Token: 0x0600016B RID: 363 RVA: 0x00005517 File Offset: 0x00003717
		public NotFilter(TestFilter baseFilter)
		{
			this.BaseFilter = baseFilter;
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600016C RID: 364 RVA: 0x0000552C File Offset: 0x0000372C
		// (set) Token: 0x0600016D RID: 365 RVA: 0x00005543 File Offset: 0x00003743
		public TestFilter BaseFilter
		{
			[CompilerGenerated]
			get
			{
				return this.<BaseFilter>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<BaseFilter>k__BackingField = value;
			}
		}

		// Token: 0x0600016E RID: 366 RVA: 0x0000554C File Offset: 0x0000374C
		public override bool Pass(ITest test)
		{
			return !this.BaseFilter.Match(test) && !this.BaseFilter.MatchParent(test);
		}

		// Token: 0x0600016F RID: 367 RVA: 0x00005580 File Offset: 0x00003780
		public override bool Match(ITest test)
		{
			return !this.BaseFilter.Match(test);
		}

		// Token: 0x06000170 RID: 368 RVA: 0x000055A4 File Offset: 0x000037A4
		public override bool IsExplicitMatch(ITest test)
		{
			return false;
		}

		// Token: 0x06000171 RID: 369 RVA: 0x000055B8 File Offset: 0x000037B8
		public override TNode AddToXml(TNode parentNode, bool recursive)
		{
			TNode tnode = parentNode.AddElement("not");
			if (recursive)
			{
				this.BaseFilter.AddToXml(tnode, true);
			}
			return tnode;
		}

		// Token: 0x04000042 RID: 66
		[CompilerGenerated]
		private TestFilter <BaseFilter>k__BackingField;
	}
}
