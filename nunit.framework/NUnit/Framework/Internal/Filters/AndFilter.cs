﻿using System;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal.Filters
{
	// Token: 0x02000008 RID: 8
	[Serializable]
	public class AndFilter : CompositeFilter
	{
		// Token: 0x06000039 RID: 57 RVA: 0x000028B8 File Offset: 0x00000AB8
		public AndFilter()
		{
		}

		// Token: 0x0600003A RID: 58 RVA: 0x000028C3 File Offset: 0x00000AC3
		public AndFilter(params ITestFilter[] filters) : base(filters)
		{
		}

		// Token: 0x0600003B RID: 59 RVA: 0x000028D0 File Offset: 0x00000AD0
		public override bool Pass(ITest test)
		{
			foreach (ITestFilter testFilter in base.Filters)
			{
				if (!testFilter.Pass(test))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002938 File Offset: 0x00000B38
		public override bool Match(ITest test)
		{
			foreach (ITestFilter testFilter in base.Filters)
			{
				TestFilter testFilter2 = (TestFilter)testFilter;
				if (!testFilter2.Match(test))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600003D RID: 61 RVA: 0x000029A4 File Offset: 0x00000BA4
		public override bool IsExplicitMatch(ITest test)
		{
			foreach (ITestFilter testFilter in base.Filters)
			{
				TestFilter testFilter2 = (TestFilter)testFilter;
				if (!testFilter2.IsExplicitMatch(test))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600003E RID: 62 RVA: 0x00002A10 File Offset: 0x00000C10
		protected override string ElementName
		{
			get
			{
				return "and";
			}
		}
	}
}
