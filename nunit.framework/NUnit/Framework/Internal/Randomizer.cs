﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000017 RID: 23
	public class Randomizer : Random
	{
		// Token: 0x06000080 RID: 128 RVA: 0x0000334A File Offset: 0x0000154A
		static Randomizer()
		{
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000081 RID: 129 RVA: 0x00003368 File Offset: 0x00001568
		// (set) Token: 0x06000082 RID: 130 RVA: 0x0000337F File Offset: 0x0000157F
		public static int InitialSeed
		{
			get
			{
				return Randomizer._initialSeed;
			}
			set
			{
				Randomizer._initialSeed = value;
				Randomizer._seedGenerator = new Random(Randomizer._initialSeed);
			}
		} = new Random().Next();

		// Token: 0x06000083 RID: 131 RVA: 0x00003398 File Offset: 0x00001598
		public static Randomizer GetRandomizer(MemberInfo member)
		{
			Randomizer result;
			if (Randomizer.Randomizers.ContainsKey(member))
			{
				result = Randomizer.Randomizers[member];
			}
			else
			{
				Randomizer randomizer = Randomizer.CreateRandomizer();
				Randomizer.Randomizers[member] = randomizer;
				result = randomizer;
			}
			return result;
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000033E0 File Offset: 0x000015E0
		public static Randomizer GetRandomizer(ParameterInfo parameter)
		{
			return Randomizer.GetRandomizer(parameter.Member);
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00003400 File Offset: 0x00001600
		public static Randomizer CreateRandomizer()
		{
			return new Randomizer(Randomizer._seedGenerator.Next());
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00003421 File Offset: 0x00001621
		public Randomizer()
		{
		}

		// Token: 0x06000087 RID: 135 RVA: 0x0000342C File Offset: 0x0000162C
		public Randomizer(int seed) : base(seed)
		{
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00003438 File Offset: 0x00001638
		[CLSCompliant(false)]
		public uint NextUInt()
		{
			return this.NextUInt(0U, uint.MaxValue);
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00003454 File Offset: 0x00001654
		[CLSCompliant(false)]
		public uint NextUInt(uint max)
		{
			return this.NextUInt(0U, max);
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00003470 File Offset: 0x00001670
		[CLSCompliant(false)]
		public uint NextUInt(uint min, uint max)
		{
			Guard.ArgumentInRange(max >= min, "Maximum value must be greater than or equal to minimum.", "max");
			uint result;
			if (min == max)
			{
				result = min;
			}
			else
			{
				uint num = max - min;
				uint num2 = uint.MaxValue - uint.MaxValue % num;
				uint num3;
				do
				{
					num3 = this.RawUInt();
				}
				while (num3 > num2);
				result = num3 % num + min;
			}
			return result;
		}

		// Token: 0x0600008B RID: 139 RVA: 0x000034CC File Offset: 0x000016CC
		public short NextShort()
		{
			return this.NextShort(0, short.MaxValue);
		}

		// Token: 0x0600008C RID: 140 RVA: 0x000034EC File Offset: 0x000016EC
		public short NextShort(short max)
		{
			return this.NextShort(0, max);
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00003508 File Offset: 0x00001708
		public short NextShort(short min, short max)
		{
			return (short)this.Next((int)min, (int)max);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00003524 File Offset: 0x00001724
		[CLSCompliant(false)]
		public ushort NextUShort()
		{
			return this.NextUShort(0, ushort.MaxValue);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00003544 File Offset: 0x00001744
		[CLSCompliant(false)]
		public ushort NextUShort(ushort max)
		{
			return this.NextUShort(0, max);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00003560 File Offset: 0x00001760
		[CLSCompliant(false)]
		public ushort NextUShort(ushort min, ushort max)
		{
			return (ushort)this.Next((int)min, (int)max);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x0000357C File Offset: 0x0000177C
		public long NextLong()
		{
			return this.NextLong(0L, long.MaxValue);
		}

		// Token: 0x06000092 RID: 146 RVA: 0x000035A0 File Offset: 0x000017A0
		public long NextLong(long max)
		{
			return this.NextLong(0L, max);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x000035BC File Offset: 0x000017BC
		public long NextLong(long min, long max)
		{
			Guard.ArgumentInRange(max >= min, "Maximum value must be greater than or equal to minimum.", "max");
			long result;
			if (min == max)
			{
				result = min;
			}
			else
			{
				ulong num = (ulong)(max - min);
				ulong num2 = ulong.MaxValue - ulong.MaxValue % num;
				ulong num3;
				do
				{
					num3 = this.RawULong();
				}
				while (num3 > num2);
				result = (long)(num3 % num + (ulong)min);
			}
			return result;
		}

		// Token: 0x06000094 RID: 148 RVA: 0x0000361C File Offset: 0x0000181C
		[CLSCompliant(false)]
		public ulong NextULong()
		{
			return this.NextULong(0UL, ulong.MaxValue);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00003638 File Offset: 0x00001838
		[CLSCompliant(false)]
		public ulong NextULong(ulong max)
		{
			return this.NextULong(0UL, max);
		}

		// Token: 0x06000096 RID: 150 RVA: 0x00003654 File Offset: 0x00001854
		[CLSCompliant(false)]
		public ulong NextULong(ulong min, ulong max)
		{
			Guard.ArgumentInRange(max >= min, "Maximum value must be greater than or equal to minimum.", "max");
			ulong num = max - min;
			ulong result;
			if (num == 0UL)
			{
				result = min;
			}
			else
			{
				ulong num2 = ulong.MaxValue - ulong.MaxValue % num;
				ulong num3;
				do
				{
					num3 = this.RawULong();
				}
				while (num3 > num2);
				result = num3 % num + min;
			}
			return result;
		}

		// Token: 0x06000097 RID: 151 RVA: 0x000036B4 File Offset: 0x000018B4
		public byte NextByte()
		{
			return this.NextByte(0, byte.MaxValue);
		}

		// Token: 0x06000098 RID: 152 RVA: 0x000036D4 File Offset: 0x000018D4
		public byte NextByte(byte max)
		{
			return this.NextByte(0, max);
		}

		// Token: 0x06000099 RID: 153 RVA: 0x000036F0 File Offset: 0x000018F0
		public byte NextByte(byte min, byte max)
		{
			return (byte)this.Next((int)min, (int)max);
		}

		// Token: 0x0600009A RID: 154 RVA: 0x0000370C File Offset: 0x0000190C
		[CLSCompliant(false)]
		public sbyte NextSByte()
		{
			return this.NextSByte(0, sbyte.MaxValue);
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00003728 File Offset: 0x00001928
		[CLSCompliant(false)]
		public sbyte NextSByte(sbyte max)
		{
			return this.NextSByte(0, max);
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00003744 File Offset: 0x00001944
		[CLSCompliant(false)]
		public sbyte NextSByte(sbyte min, sbyte max)
		{
			return (sbyte)this.Next((int)min, (int)max);
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00003760 File Offset: 0x00001960
		public bool NextBool()
		{
			return this.NextDouble() < 0.5;
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00003784 File Offset: 0x00001984
		public bool NextBool(double probability)
		{
			Guard.ArgumentInRange(probability >= 0.0 && probability <= 1.0, "Probability must be from 0.0 to 1.0", "probability");
			return this.NextDouble() < probability;
		}

		// Token: 0x0600009F RID: 159 RVA: 0x000037D0 File Offset: 0x000019D0
		public double NextDouble(double max)
		{
			return this.NextDouble() * max;
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x000037EC File Offset: 0x000019EC
		public double NextDouble(double min, double max)
		{
			Guard.ArgumentInRange(max >= min, "Maximum value must be greater than or equal to minimum.", "max");
			double result;
			if (max == min)
			{
				result = min;
			}
			else
			{
				double num = max - min;
				result = this.NextDouble() * num + min;
			}
			return result;
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00003834 File Offset: 0x00001A34
		public float NextFloat()
		{
			return (float)this.NextDouble();
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00003850 File Offset: 0x00001A50
		public float NextFloat(float max)
		{
			return (float)this.NextDouble((double)max);
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x0000386C File Offset: 0x00001A6C
		public float NextFloat(float min, float max)
		{
			return (float)this.NextDouble((double)min, (double)max);
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x0000388C File Offset: 0x00001A8C
		public object NextEnum(Type type)
		{
			Array enumValues = TypeHelper.GetEnumValues(type);
			return enumValues.GetValue(this.Next(0, enumValues.Length));
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x000038B8 File Offset: 0x00001AB8
		public T NextEnum<T>()
		{
			return (T)((object)this.NextEnum(typeof(T)));
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x000038E0 File Offset: 0x00001AE0
		public string GetString(int outputLength, string allowedChars)
		{
			StringBuilder stringBuilder = new StringBuilder(outputLength);
			for (int i = 0; i < outputLength; i++)
			{
				stringBuilder.Append(allowedChars[this.Next(0, allowedChars.Length)]);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x0000392C File Offset: 0x00001B2C
		public string GetString(int outputLength)
		{
			return this.GetString(outputLength, "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789_");
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x0000394C File Offset: 0x00001B4C
		public string GetString()
		{
			return this.GetString(25, "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789_");
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x0000396C File Offset: 0x00001B6C
		public decimal NextDecimal()
		{
			int lo = this.Next(0, int.MaxValue);
			int mid = this.Next(0, int.MaxValue);
			int hi = this.Next(0, int.MaxValue);
			return new decimal(lo, mid, hi, false, 0);
		}

		// Token: 0x060000AA RID: 170 RVA: 0x000039B0 File Offset: 0x00001BB0
		public decimal NextDecimal(decimal max)
		{
			return this.NextDecimal() % max;
		}

		// Token: 0x060000AB RID: 171 RVA: 0x000039D0 File Offset: 0x00001BD0
		public decimal NextDecimal(decimal min, decimal max)
		{
			Guard.ArgumentInRange(max >= min, "Maximum value must be greater than or equal to minimum.", "max");
			Guard.ArgumentValid(max < 0m == min < 0m || min + decimal.MaxValue >= max, "Range too great for decimal data, use double range", "max");
			decimal result;
			if (min == max)
			{
				result = min;
			}
			else
			{
				decimal d = max - min;
				decimal d2 = decimal.MaxValue - decimal.MaxValue % d;
				decimal d3;
				do
				{
					d3 = this.NextDecimal();
				}
				while (d3 > d2);
				result = d3 % d + min;
			}
			return result;
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00003A9C File Offset: 0x00001C9C
		private uint RawUInt()
		{
			byte[] array = new byte[4];
			this.NextBytes(array);
			return BitConverter.ToUInt32(array, 0);
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00003AC4 File Offset: 0x00001CC4
		private uint RawUShort()
		{
			byte[] array = new byte[4];
			this.NextBytes(array);
			return BitConverter.ToUInt32(array, 0);
		}

		// Token: 0x060000AE RID: 174 RVA: 0x00003AEC File Offset: 0x00001CEC
		private ulong RawULong()
		{
			byte[] array = new byte[8];
			this.NextBytes(array);
			return BitConverter.ToUInt64(array, 0);
		}

		// Token: 0x060000AF RID: 175 RVA: 0x00003B14 File Offset: 0x00001D14
		private long RawLong()
		{
			byte[] array = new byte[8];
			this.NextBytes(array);
			return BitConverter.ToInt64(array, 0);
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00003B3C File Offset: 0x00001D3C
		private decimal RawDecimal()
		{
			int lo = this.Next(0, int.MaxValue);
			int mid = this.Next(0, int.MaxValue);
			int hi = this.Next(0, int.MaxValue);
			bool isNegative = this.NextBool();
			byte scale = this.NextByte(29);
			return new decimal(lo, mid, hi, isNegative, scale);
		}

		// Token: 0x04000011 RID: 17
		public const string DefaultStringChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789_";

		// Token: 0x04000012 RID: 18
		private const int DefaultStringLength = 25;

		// Token: 0x04000013 RID: 19
		private static Random _seedGenerator;

		// Token: 0x04000014 RID: 20
		private static int _initialSeed;

		// Token: 0x04000015 RID: 21
		private static Dictionary<MemberInfo, Randomizer> Randomizers = new Dictionary<MemberInfo, Randomizer>();
	}
}
