﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000023 RID: 35
	public class PropertyBag : IPropertyBag, IXmlNodeBuilder
	{
		// Token: 0x06000152 RID: 338 RVA: 0x00005144 File Offset: 0x00003344
		public void Add(string key, object value)
		{
			IList list;
			if (!this.inner.TryGetValue(key, out list))
			{
				list = new List<object>();
				this.inner.Add(key, list);
			}
			list.Add(value);
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00005184 File Offset: 0x00003384
		public void Set(string key, object value)
		{
			Guard.ArgumentNotNull(key, "key");
			Guard.ArgumentNotNull(value, "value");
			IList list = new List<object>();
			list.Add(value);
			this.inner[key] = list;
		}

		// Token: 0x06000154 RID: 340 RVA: 0x000051C8 File Offset: 0x000033C8
		public object Get(string key)
		{
			IList list;
			return (this.inner.TryGetValue(key, out list) && list.Count > 0) ? list[0] : null;
		}

		// Token: 0x06000155 RID: 341 RVA: 0x00005200 File Offset: 0x00003400
		public bool ContainsKey(string key)
		{
			return this.inner.ContainsKey(key);
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000156 RID: 342 RVA: 0x00005220 File Offset: 0x00003420
		public ICollection<string> Keys
		{
			get
			{
				return this.inner.Keys;
			}
		}

		// Token: 0x1700006F RID: 111
		public IList this[string key]
		{
			get
			{
				IList list;
				if (!this.inner.TryGetValue(key, out list))
				{
					list = new List<object>();
					this.inner.Add(key, list);
				}
				return list;
			}
			set
			{
				this.inner[key] = value;
			}
		}

		// Token: 0x06000159 RID: 345 RVA: 0x0000528C File Offset: 0x0000348C
		public TNode ToXml(bool recursive)
		{
			return this.AddToXml(new TNode("dummy"), recursive);
		}

		// Token: 0x0600015A RID: 346 RVA: 0x000052B0 File Offset: 0x000034B0
		public TNode AddToXml(TNode parentNode, bool recursive)
		{
			TNode tnode = parentNode.AddElement("properties");
			foreach (string text in this.Keys)
			{
				foreach (object obj in this[text])
				{
					TNode tnode2 = tnode.AddElement("property");
					tnode2.AddAttribute("name", text.ToString());
					tnode2.AddAttribute("value", obj.ToString());
				}
			}
			return tnode;
		}

		// Token: 0x0600015B RID: 347 RVA: 0x000053AC File Offset: 0x000035AC
		public PropertyBag()
		{
		}

		// Token: 0x0400003F RID: 63
		private Dictionary<string, IList> inner = new Dictionary<string, IList>();
	}
}
