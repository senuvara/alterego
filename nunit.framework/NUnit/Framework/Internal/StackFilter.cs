﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace NUnit.Framework.Internal
{
	// Token: 0x020000DA RID: 218
	public static class StackFilter
	{
		// Token: 0x06000674 RID: 1652 RVA: 0x00016F08 File Offset: 0x00015108
		public static string Filter(string rawTrace)
		{
			string result;
			if (rawTrace == null)
			{
				result = null;
			}
			else
			{
				StringReader stringReader = new StringReader(rawTrace);
				StringWriter stringWriter = new StringWriter();
				try
				{
					string text;
					while ((text = stringReader.ReadLine()) != null && StackFilter.assertOrAssumeRegex.IsMatch(text))
					{
					}
					while (text != null && text.IndexOf(" System.Reflection.") < 0)
					{
						stringWriter.WriteLine(text.Trim());
						text = stringReader.ReadLine();
					}
				}
				catch (Exception)
				{
					return rawTrace;
				}
				result = stringWriter.ToString();
			}
			return result;
		}

		// Token: 0x06000675 RID: 1653 RVA: 0x00016FAC File Offset: 0x000151AC
		// Note: this type is marked as 'beforefieldinit'.
		static StackFilter()
		{
		}

		// Token: 0x0400015A RID: 346
		private static readonly Regex assertOrAssumeRegex = new Regex(" NUnit\\.Framework\\.Ass(ert|ume)\\.");
	}
}
