﻿using System;
using System.IO;
using System.Reflection;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000086 RID: 134
	public static class AssemblyHelper
	{
		// Token: 0x0600047D RID: 1149 RVA: 0x0000EE0C File Offset: 0x0000D00C
		public static string GetAssemblyPath(Assembly assembly)
		{
			string codeBase = assembly.CodeBase;
			string result;
			if (AssemblyHelper.IsFileUri(codeBase))
			{
				result = AssemblyHelper.GetAssemblyPathFromCodeBase(codeBase);
			}
			else
			{
				result = assembly.Location;
			}
			return result;
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x0000EE44 File Offset: 0x0000D044
		public static string GetDirectoryName(Assembly assembly)
		{
			return Path.GetDirectoryName(AssemblyHelper.GetAssemblyPath(assembly));
		}

		// Token: 0x0600047F RID: 1151 RVA: 0x0000EE64 File Offset: 0x0000D064
		public static AssemblyName GetAssemblyName(Assembly assembly)
		{
			return assembly.GetName();
		}

		// Token: 0x06000480 RID: 1152 RVA: 0x0000EE7C File Offset: 0x0000D07C
		public static Assembly Load(string nameOrPath)
		{
			string a = Path.GetExtension(nameOrPath).ToLower();
			Assembly result;
			if (a == ".dll" || a == ".exe")
			{
				result = Assembly.Load(AssemblyName.GetAssemblyName(nameOrPath));
			}
			else
			{
				result = Assembly.Load(nameOrPath);
			}
			return result;
		}

		// Token: 0x06000481 RID: 1153 RVA: 0x0000EED4 File Offset: 0x0000D0D4
		private static bool IsFileUri(string uri)
		{
			return uri.ToLower().StartsWith(Uri.UriSchemeFile);
		}

		// Token: 0x06000482 RID: 1154 RVA: 0x0000EEF8 File Offset: 0x0000D0F8
		public static string GetAssemblyPathFromCodeBase(string codeBase)
		{
			int num = Uri.UriSchemeFile.Length + Uri.SchemeDelimiter.Length;
			if (codeBase[num] == '/')
			{
				if (codeBase[num + 2] == ':')
				{
					num++;
				}
			}
			else if (codeBase[num + 1] != ':')
			{
				num -= 2;
			}
			return codeBase.Substring(num);
		}
	}
}
