﻿using System;

namespace NUnit.Framework.Internal
{
	// Token: 0x02000180 RID: 384
	public class PropertyNames
	{
		// Token: 0x06000A12 RID: 2578 RVA: 0x00021EC9 File Offset: 0x000200C9
		public PropertyNames()
		{
		}

		// Token: 0x0400024E RID: 590
		public const string AppDomain = "_APPDOMAIN";

		// Token: 0x0400024F RID: 591
		public const string JoinType = "_JOINTYPE";

		// Token: 0x04000250 RID: 592
		public const string ProcessID = "_PID";

		// Token: 0x04000251 RID: 593
		public const string ProviderStackTrace = "_PROVIDERSTACKTRACE";

		// Token: 0x04000252 RID: 594
		public const string SkipReason = "_SKIPREASON";

		// Token: 0x04000253 RID: 595
		public const string Author = "Author";

		// Token: 0x04000254 RID: 596
		public const string ApartmentState = "ApartmentState";

		// Token: 0x04000255 RID: 597
		public const string Category = "Category";

		// Token: 0x04000256 RID: 598
		public const string Description = "Description";

		// Token: 0x04000257 RID: 599
		public const string LevelOfParallelism = "LevelOfParallelism";

		// Token: 0x04000258 RID: 600
		public const string MaxTime = "MaxTime";

		// Token: 0x04000259 RID: 601
		public const string ParallelScope = "ParallelScope";

		// Token: 0x0400025A RID: 602
		public const string RepeatCount = "Repeat";

		// Token: 0x0400025B RID: 603
		public const string RequiresThread = "RequiresThread";

		// Token: 0x0400025C RID: 604
		public const string SetCulture = "SetCulture";

		// Token: 0x0400025D RID: 605
		public const string SetUICulture = "SetUICulture";

		// Token: 0x0400025E RID: 606
		public const string TestOf = "TestOf";

		// Token: 0x0400025F RID: 607
		public const string Timeout = "Timeout";

		// Token: 0x04000260 RID: 608
		public const string IgnoreUntilDate = "IgnoreUntilDate";

		// Token: 0x04000261 RID: 609
		public const string Order = "Order";
	}
}
