﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000150 RID: 336
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class SingleThreadedAttribute : NUnitAttribute, IApplyToContext
	{
		// Token: 0x060008EE RID: 2286 RVA: 0x0001E51A File Offset: 0x0001C71A
		public void ApplyToContext(ITestExecutionContext context)
		{
			context.IsSingleThreaded = true;
		}

		// Token: 0x060008EF RID: 2287 RVA: 0x0001E525 File Offset: 0x0001C725
		public SingleThreadedAttribute()
		{
		}
	}
}
