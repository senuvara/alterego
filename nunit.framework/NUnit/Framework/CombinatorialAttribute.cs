﻿using System;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x0200017F RID: 383
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class CombinatorialAttribute : CombiningStrategyAttribute
	{
		// Token: 0x06000A11 RID: 2577 RVA: 0x00021EB4 File Offset: 0x000200B4
		public CombinatorialAttribute() : base(new CombinatorialStrategy(), new ParameterDataSourceProvider())
		{
		}
	}
}
