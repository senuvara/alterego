﻿using System;
using System.Globalization;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x0200013C RID: 316
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class SetUICultureAttribute : PropertyAttribute, IApplyToContext
	{
		// Token: 0x06000871 RID: 2161 RVA: 0x0001D2F8 File Offset: 0x0001B4F8
		public SetUICultureAttribute(string culture) : base("SetUICulture", culture)
		{
			this._culture = culture;
		}

		// Token: 0x06000872 RID: 2162 RVA: 0x0001D310 File Offset: 0x0001B510
		void IApplyToContext.ApplyToContext(ITestExecutionContext context)
		{
			context.CurrentUICulture = new CultureInfo(this._culture);
		}

		// Token: 0x040001D3 RID: 467
		private string _culture;
	}
}
