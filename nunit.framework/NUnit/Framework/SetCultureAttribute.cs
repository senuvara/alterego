﻿using System;
using System.Globalization;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000189 RID: 393
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class SetCultureAttribute : PropertyAttribute, IApplyToContext
	{
		// Token: 0x06000A4C RID: 2636 RVA: 0x0002278D File Offset: 0x0002098D
		public SetCultureAttribute(string culture) : base("SetCulture", culture)
		{
			this._culture = culture;
		}

		// Token: 0x06000A4D RID: 2637 RVA: 0x000227A5 File Offset: 0x000209A5
		void IApplyToContext.ApplyToContext(ITestExecutionContext context)
		{
			context.CurrentCulture = new CultureInfo(this._culture);
		}

		// Token: 0x0400026C RID: 620
		private string _culture;
	}
}
