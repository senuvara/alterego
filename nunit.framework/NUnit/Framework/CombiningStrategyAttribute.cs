﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x02000035 RID: 53
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public abstract class CombiningStrategyAttribute : NUnitAttribute, ITestBuilder, IApplyToTest
	{
		// Token: 0x060001A7 RID: 423 RVA: 0x000065CF File Offset: 0x000047CF
		protected CombiningStrategyAttribute(ICombiningStrategy strategy, IParameterDataProvider provider)
		{
			this._strategy = strategy;
			this._dataProvider = provider;
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x000065F3 File Offset: 0x000047F3
		protected CombiningStrategyAttribute(object strategy, object provider) : this((ICombiningStrategy)strategy, (IParameterDataProvider)provider)
		{
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x0000660C File Offset: 0x0000480C
		public IEnumerable<TestMethod> BuildFrom(IMethodInfo method, Test suite)
		{
			List<TestMethod> list = new List<TestMethod>();
			IParameterInfo[] parameters = method.GetParameters();
			if (parameters.Length > 0)
			{
				IEnumerable[] array = new IEnumerable[parameters.Length];
				try
				{
					for (int i = 0; i < parameters.Length; i++)
					{
						array[i] = this._dataProvider.GetDataFor(parameters[i]);
					}
				}
				catch (InvalidDataSourceException ex)
				{
					TestCaseParameters testCaseParameters = new TestCaseParameters();
					testCaseParameters.RunState = RunState.NotRunnable;
					testCaseParameters.Properties.Set("_SKIPREASON", ex.Message);
					list.Add(this._builder.BuildTestMethod(method, suite, testCaseParameters));
					return list;
				}
				foreach (ITestCaseData testCaseData in this._strategy.GetTestCases(array))
				{
					list.Add(this._builder.BuildTestMethod(method, suite, (TestCaseParameters)testCaseData));
				}
			}
			return list;
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00006738 File Offset: 0x00004938
		public void ApplyToTest(Test test)
		{
			string text = this._strategy.GetType().Name;
			if (text.EndsWith("Strategy"))
			{
				text = text.Substring(0, text.Length - 8);
			}
			test.Properties.Set("_JOINTYPE", text);
		}

		// Token: 0x0400004A RID: 74
		private NUnitTestCaseBuilder _builder = new NUnitTestCaseBuilder();

		// Token: 0x0400004B RID: 75
		private ICombiningStrategy _strategy;

		// Token: 0x0400004C RID: 76
		private IParameterDataProvider _dataProvider;
	}
}
