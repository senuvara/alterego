﻿using System;
using System.Collections;

namespace NUnit.Framework
{
	// Token: 0x02000143 RID: 323
	public class List
	{
		// Token: 0x0600087A RID: 2170 RVA: 0x0001D464 File Offset: 0x0001B664
		public static ListMapper Map(ICollection actual)
		{
			return new ListMapper(actual);
		}

		// Token: 0x0600087B RID: 2171 RVA: 0x0001D47C File Offset: 0x0001B67C
		public List()
		{
		}
	}
}
