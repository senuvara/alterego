﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000188 RID: 392
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = true, Inherited = false)]
	public class ValueSourceAttribute : DataAttribute, IParameterDataSource
	{
		// Token: 0x06000A42 RID: 2626 RVA: 0x00022598 File Offset: 0x00020798
		public ValueSourceAttribute(string sourceName)
		{
			this.SourceName = sourceName;
		}

		// Token: 0x06000A43 RID: 2627 RVA: 0x000225AB File Offset: 0x000207AB
		public ValueSourceAttribute(Type sourceType, string sourceName)
		{
			this.SourceType = sourceType;
			this.SourceName = sourceName;
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06000A44 RID: 2628 RVA: 0x000225C8 File Offset: 0x000207C8
		// (set) Token: 0x06000A45 RID: 2629 RVA: 0x000225DF File Offset: 0x000207DF
		public string SourceName
		{
			[CompilerGenerated]
			get
			{
				return this.<SourceName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<SourceName>k__BackingField = value;
			}
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06000A46 RID: 2630 RVA: 0x000225E8 File Offset: 0x000207E8
		// (set) Token: 0x06000A47 RID: 2631 RVA: 0x000225FF File Offset: 0x000207FF
		public Type SourceType
		{
			[CompilerGenerated]
			get
			{
				return this.<SourceType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<SourceType>k__BackingField = value;
			}
		}

		// Token: 0x06000A48 RID: 2632 RVA: 0x00022608 File Offset: 0x00020808
		public IEnumerable GetData(IParameterInfo parameter)
		{
			return this.GetDataSource(parameter);
		}

		// Token: 0x06000A49 RID: 2633 RVA: 0x00022624 File Offset: 0x00020824
		private IEnumerable GetDataSource(IParameterInfo parameter)
		{
			Type type = this.SourceType ?? parameter.Method.TypeInfo.Type;
			IEnumerable result;
			if (this.SourceName == null)
			{
				result = (Reflect.Construct(type) as IEnumerable);
			}
			else
			{
				MemberInfo[] member = type.GetMember(this.SourceName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
				IEnumerable dataSourceValue = ValueSourceAttribute.GetDataSourceValue(member);
				if (dataSourceValue == null)
				{
					ValueSourceAttribute.ThrowInvalidDataSourceException();
				}
				result = dataSourceValue;
			}
			return result;
		}

		// Token: 0x06000A4A RID: 2634 RVA: 0x000226A0 File Offset: 0x000208A0
		private static IEnumerable GetDataSourceValue(MemberInfo[] members)
		{
			IEnumerable result;
			if (members.Length != 1)
			{
				result = null;
			}
			else
			{
				MemberInfo memberInfo = members[0];
				FieldInfo fieldInfo = memberInfo as FieldInfo;
				if (fieldInfo != null)
				{
					if (fieldInfo.IsStatic)
					{
						return (IEnumerable)fieldInfo.GetValue(null);
					}
					ValueSourceAttribute.ThrowInvalidDataSourceException();
				}
				PropertyInfo propertyInfo = memberInfo as PropertyInfo;
				if (propertyInfo != null)
				{
					if (propertyInfo.GetGetMethod(true).IsStatic)
					{
						return (IEnumerable)propertyInfo.GetValue(null, null);
					}
					ValueSourceAttribute.ThrowInvalidDataSourceException();
				}
				MethodInfo methodInfo = memberInfo as MethodInfo;
				if (methodInfo != null)
				{
					if (methodInfo.IsStatic)
					{
						return (IEnumerable)methodInfo.Invoke(null, null);
					}
					ValueSourceAttribute.ThrowInvalidDataSourceException();
				}
				result = null;
			}
			return result;
		}

		// Token: 0x06000A4B RID: 2635 RVA: 0x00022780 File Offset: 0x00020980
		private static void ThrowInvalidDataSourceException()
		{
			throw new InvalidDataSourceException("The sourceName specified on a ValueSourceAttribute must refer to a non null static field, property or method.");
		}

		// Token: 0x0400026A RID: 618
		[CompilerGenerated]
		private string <SourceName>k__BackingField;

		// Token: 0x0400026B RID: 619
		[CompilerGenerated]
		private Type <SourceType>k__BackingField;
	}
}
