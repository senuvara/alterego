﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x02000013 RID: 19
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
	public class PropertyAttribute : NUnitAttribute, IApplyToTest
	{
		// Token: 0x06000076 RID: 118 RVA: 0x00003152 File Offset: 0x00001352
		public PropertyAttribute(string propertyName, string propertyValue)
		{
			this.properties.Add(propertyName, propertyValue);
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00003176 File Offset: 0x00001376
		public PropertyAttribute(string propertyName, int propertyValue)
		{
			this.properties.Add(propertyName, propertyValue);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x0000319F File Offset: 0x0000139F
		public PropertyAttribute(string propertyName, double propertyValue)
		{
			this.properties.Add(propertyName, propertyValue);
		}

		// Token: 0x06000079 RID: 121 RVA: 0x000031C8 File Offset: 0x000013C8
		protected PropertyAttribute()
		{
		}

		// Token: 0x0600007A RID: 122 RVA: 0x000031E0 File Offset: 0x000013E0
		protected PropertyAttribute(object propertyValue)
		{
			string text = base.GetType().Name;
			if (text.EndsWith("Attribute"))
			{
				text = text.Substring(0, text.Length - 9);
			}
			this.properties.Add(text, propertyValue);
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600007B RID: 123 RVA: 0x00003240 File Offset: 0x00001440
		public IPropertyBag Properties
		{
			get
			{
				return this.properties;
			}
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003258 File Offset: 0x00001458
		public virtual void ApplyToTest(Test test)
		{
			foreach (string key in this.Properties.Keys)
			{
				foreach (object value in this.Properties[key])
				{
					test.Properties.Add(key, value);
				}
			}
		}

		// Token: 0x0400000F RID: 15
		private PropertyBag properties = new PropertyBag();
	}
}
