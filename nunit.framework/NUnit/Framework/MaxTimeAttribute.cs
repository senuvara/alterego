﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Commands;

namespace NUnit.Framework
{
	// Token: 0x02000016 RID: 22
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public sealed class MaxTimeAttribute : PropertyAttribute, IWrapSetUpTearDown, ICommandWrapper
	{
		// Token: 0x0600007E RID: 126 RVA: 0x00003314 File Offset: 0x00001514
		public MaxTimeAttribute(int milliseconds) : base(milliseconds)
		{
			this._milliseconds = milliseconds;
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0000332C File Offset: 0x0000152C
		TestCommand ICommandWrapper.Wrap(TestCommand command)
		{
			return new MaxTimeCommand(command, this._milliseconds);
		}

		// Token: 0x04000010 RID: 16
		private int _milliseconds;
	}
}
