﻿using System;
using System.Threading;

namespace NUnit.Framework
{
	// Token: 0x020001A4 RID: 420
	[Obsolete("Use ApartmentAttribute and pass in ApartmentState.MTA instead")]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class RequiresMTAAttribute : PropertyAttribute
	{
		// Token: 0x06000AD1 RID: 2769 RVA: 0x000243B2 File Offset: 0x000225B2
		public RequiresMTAAttribute()
		{
			base.Properties.Add("ApartmentState", ApartmentState.MTA);
		}
	}
}
