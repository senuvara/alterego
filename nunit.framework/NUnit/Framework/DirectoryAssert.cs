﻿using System;
using System.ComponentModel;
using System.IO;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x02000126 RID: 294
	public static class DirectoryAssert
	{
		// Token: 0x060007DA RID: 2010 RVA: 0x0001BC8C File Offset: 0x00019E8C
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new static bool Equals(object a, object b)
		{
			throw new InvalidOperationException("DirectoryAssert.Equals should not be used for Assertions");
		}

		// Token: 0x060007DB RID: 2011 RVA: 0x0001BC99 File Offset: 0x00019E99
		public new static void ReferenceEquals(object a, object b)
		{
			throw new InvalidOperationException("DirectoryAssert.ReferenceEquals should not be used for Assertions");
		}

		// Token: 0x060007DC RID: 2012 RVA: 0x0001BCA6 File Offset: 0x00019EA6
		public static void AreEqual(DirectoryInfo expected, DirectoryInfo actual, string message, params object[] args)
		{
			Assert.AreEqual(expected, actual, message, args);
		}

		// Token: 0x060007DD RID: 2013 RVA: 0x0001BCB3 File Offset: 0x00019EB3
		public static void AreEqual(DirectoryInfo expected, DirectoryInfo actual)
		{
			DirectoryAssert.AreEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x060007DE RID: 2014 RVA: 0x0001BCC4 File Offset: 0x00019EC4
		public static void AreNotEqual(DirectoryInfo expected, DirectoryInfo actual, string message, params object[] args)
		{
			Assert.AreNotEqual(expected, actual, message, args);
		}

		// Token: 0x060007DF RID: 2015 RVA: 0x0001BCD1 File Offset: 0x00019ED1
		public static void AreNotEqual(DirectoryInfo expected, DirectoryInfo actual)
		{
			DirectoryAssert.AreNotEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x060007E0 RID: 2016 RVA: 0x0001BCE2 File Offset: 0x00019EE2
		public static void Exists(DirectoryInfo actual, string message, params object[] args)
		{
			Assert.That<DirectoryInfo>(actual, new FileOrDirectoryExistsConstraint().IgnoreFiles, message, args);
		}

		// Token: 0x060007E1 RID: 2017 RVA: 0x0001BCF8 File Offset: 0x00019EF8
		public static void Exists(DirectoryInfo actual)
		{
			DirectoryAssert.Exists(actual, string.Empty, null);
		}

		// Token: 0x060007E2 RID: 2018 RVA: 0x0001BD08 File Offset: 0x00019F08
		public static void Exists(string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, new FileOrDirectoryExistsConstraint().IgnoreFiles, message, args);
		}

		// Token: 0x060007E3 RID: 2019 RVA: 0x0001BD1E File Offset: 0x00019F1E
		public static void Exists(string actual)
		{
			DirectoryAssert.Exists(actual, string.Empty, null);
		}

		// Token: 0x060007E4 RID: 2020 RVA: 0x0001BD2E File Offset: 0x00019F2E
		public static void DoesNotExist(DirectoryInfo actual, string message, params object[] args)
		{
			Assert.That<DirectoryInfo>(actual, new NotConstraint(new FileOrDirectoryExistsConstraint().IgnoreFiles), message, args);
		}

		// Token: 0x060007E5 RID: 2021 RVA: 0x0001BD49 File Offset: 0x00019F49
		public static void DoesNotExist(DirectoryInfo actual)
		{
			DirectoryAssert.DoesNotExist(actual, string.Empty, null);
		}

		// Token: 0x060007E6 RID: 2022 RVA: 0x0001BD59 File Offset: 0x00019F59
		public static void DoesNotExist(string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, new NotConstraint(new FileOrDirectoryExistsConstraint().IgnoreFiles), message, args);
		}

		// Token: 0x060007E7 RID: 2023 RVA: 0x0001BD74 File Offset: 0x00019F74
		public static void DoesNotExist(string actual)
		{
			DirectoryAssert.DoesNotExist(actual, string.Empty, null);
		}
	}
}
