﻿using System;
using System.ComponentModel;
using System.IO;
using NUnit.Framework.Constraints;

namespace NUnit.Framework
{
	// Token: 0x02000178 RID: 376
	public static class FileAssert
	{
		// Token: 0x060009EE RID: 2542 RVA: 0x000219CB File Offset: 0x0001FBCB
		[EditorBrowsable(EditorBrowsableState.Never)]
		public new static bool Equals(object a, object b)
		{
			throw new InvalidOperationException("FileAssert.Equals should not be used for Assertions");
		}

		// Token: 0x060009EF RID: 2543 RVA: 0x000219D8 File Offset: 0x0001FBD8
		public new static void ReferenceEquals(object a, object b)
		{
			throw new InvalidOperationException("FileAssert.ReferenceEquals should not be used for Assertions");
		}

		// Token: 0x060009F0 RID: 2544 RVA: 0x000219E5 File Offset: 0x0001FBE5
		public static void AreEqual(Stream expected, Stream actual, string message, params object[] args)
		{
			Assert.That<Stream>(actual, Is.EqualTo(expected), message, args);
		}

		// Token: 0x060009F1 RID: 2545 RVA: 0x000219F7 File Offset: 0x0001FBF7
		public static void AreEqual(Stream expected, Stream actual)
		{
			FileAssert.AreEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x060009F2 RID: 2546 RVA: 0x00021A08 File Offset: 0x0001FC08
		public static void AreEqual(FileInfo expected, FileInfo actual, string message, params object[] args)
		{
			using (FileStream fileStream = expected.OpenRead())
			{
				using (FileStream fileStream2 = actual.OpenRead())
				{
					FileAssert.AreEqual(fileStream, fileStream2, message, args);
				}
			}
		}

		// Token: 0x060009F3 RID: 2547 RVA: 0x00021A74 File Offset: 0x0001FC74
		public static void AreEqual(FileInfo expected, FileInfo actual)
		{
			FileAssert.AreEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x060009F4 RID: 2548 RVA: 0x00021A88 File Offset: 0x0001FC88
		public static void AreEqual(string expected, string actual, string message, params object[] args)
		{
			using (FileStream fileStream = File.OpenRead(expected))
			{
				using (FileStream fileStream2 = File.OpenRead(actual))
				{
					FileAssert.AreEqual(fileStream, fileStream2, message, args);
				}
			}
		}

		// Token: 0x060009F5 RID: 2549 RVA: 0x00021AF4 File Offset: 0x0001FCF4
		public static void AreEqual(string expected, string actual)
		{
			FileAssert.AreEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x060009F6 RID: 2550 RVA: 0x00021B05 File Offset: 0x0001FD05
		public static void AreNotEqual(Stream expected, Stream actual, string message, params object[] args)
		{
			Assert.That<Stream>(actual, Is.Not.EqualTo(expected), message, args);
		}

		// Token: 0x060009F7 RID: 2551 RVA: 0x00021B1C File Offset: 0x0001FD1C
		public static void AreNotEqual(Stream expected, Stream actual)
		{
			FileAssert.AreNotEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x060009F8 RID: 2552 RVA: 0x00021B30 File Offset: 0x0001FD30
		public static void AreNotEqual(FileInfo expected, FileInfo actual, string message, params object[] args)
		{
			using (FileStream fileStream = expected.OpenRead())
			{
				using (FileStream fileStream2 = actual.OpenRead())
				{
					FileAssert.AreNotEqual(fileStream, fileStream2, message, args);
				}
			}
		}

		// Token: 0x060009F9 RID: 2553 RVA: 0x00021B9C File Offset: 0x0001FD9C
		public static void AreNotEqual(FileInfo expected, FileInfo actual)
		{
			FileAssert.AreNotEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x060009FA RID: 2554 RVA: 0x00021BB0 File Offset: 0x0001FDB0
		public static void AreNotEqual(string expected, string actual, string message, params object[] args)
		{
			using (FileStream fileStream = File.OpenRead(expected))
			{
				using (FileStream fileStream2 = File.OpenRead(actual))
				{
					FileAssert.AreNotEqual(fileStream, fileStream2, message, args);
				}
			}
		}

		// Token: 0x060009FB RID: 2555 RVA: 0x00021C1C File Offset: 0x0001FE1C
		public static void AreNotEqual(string expected, string actual)
		{
			FileAssert.AreNotEqual(expected, actual, string.Empty, null);
		}

		// Token: 0x060009FC RID: 2556 RVA: 0x00021C2D File Offset: 0x0001FE2D
		public static void Exists(FileInfo actual, string message, params object[] args)
		{
			Assert.That<FileInfo>(actual, new FileOrDirectoryExistsConstraint().IgnoreDirectories, message, args);
		}

		// Token: 0x060009FD RID: 2557 RVA: 0x00021C43 File Offset: 0x0001FE43
		public static void Exists(FileInfo actual)
		{
			FileAssert.Exists(actual, string.Empty, null);
		}

		// Token: 0x060009FE RID: 2558 RVA: 0x00021C53 File Offset: 0x0001FE53
		public static void Exists(string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, new FileOrDirectoryExistsConstraint().IgnoreDirectories, message, args);
		}

		// Token: 0x060009FF RID: 2559 RVA: 0x00021C69 File Offset: 0x0001FE69
		public static void Exists(string actual)
		{
			FileAssert.Exists(actual, string.Empty, null);
		}

		// Token: 0x06000A00 RID: 2560 RVA: 0x00021C79 File Offset: 0x0001FE79
		public static void DoesNotExist(FileInfo actual, string message, params object[] args)
		{
			Assert.That<FileInfo>(actual, new NotConstraint(new FileOrDirectoryExistsConstraint().IgnoreDirectories), message, args);
		}

		// Token: 0x06000A01 RID: 2561 RVA: 0x00021C94 File Offset: 0x0001FE94
		public static void DoesNotExist(FileInfo actual)
		{
			FileAssert.DoesNotExist(actual, string.Empty, null);
		}

		// Token: 0x06000A02 RID: 2562 RVA: 0x00021CA4 File Offset: 0x0001FEA4
		public static void DoesNotExist(string actual, string message, params object[] args)
		{
			Assert.That<string>(actual, new NotConstraint(new FileOrDirectoryExistsConstraint().IgnoreDirectories), message, args);
		}

		// Token: 0x06000A03 RID: 2563 RVA: 0x00021CBF File Offset: 0x0001FEBF
		public static void DoesNotExist(string actual)
		{
			FileAssert.DoesNotExist(actual, string.Empty, null);
		}
	}
}
