﻿using System;
using System.Runtime.Serialization;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework
{
	// Token: 0x02000179 RID: 377
	[Serializable]
	public class IgnoreException : ResultStateException
	{
		// Token: 0x06000A04 RID: 2564 RVA: 0x00021CCF File Offset: 0x0001FECF
		public IgnoreException(string message) : base(message)
		{
		}

		// Token: 0x06000A05 RID: 2565 RVA: 0x00021CDB File Offset: 0x0001FEDB
		public IgnoreException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06000A06 RID: 2566 RVA: 0x00021CE8 File Offset: 0x0001FEE8
		protected IgnoreException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x06000A07 RID: 2567 RVA: 0x00021CF8 File Offset: 0x0001FEF8
		public override ResultState ResultState
		{
			get
			{
				return ResultState.Ignored;
			}
		}
	}
}
