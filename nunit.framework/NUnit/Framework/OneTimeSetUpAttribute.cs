﻿using System;

namespace NUnit.Framework
{
	// Token: 0x020000C9 RID: 201
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class OneTimeSetUpAttribute : NUnitAttribute
	{
		// Token: 0x0600062D RID: 1581 RVA: 0x0001589E File Offset: 0x00013A9E
		public OneTimeSetUpAttribute()
		{
		}
	}
}
