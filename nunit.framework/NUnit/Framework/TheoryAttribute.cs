﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x020000EF RID: 239
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public class TheoryAttribute : CombiningStrategyAttribute, ITestBuilder, IImplyFixture
	{
		// Token: 0x060006DF RID: 1759 RVA: 0x000195FC File Offset: 0x000177FC
		public TheoryAttribute() : base(new CombinatorialStrategy(), new ParameterDataProvider(new IParameterDataProvider[]
		{
			new DatapointProvider(),
			new ParameterDataSourceProvider()
		}))
		{
		}
	}
}
