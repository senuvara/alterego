﻿using System;

namespace NUnit.Framework
{
	// Token: 0x020000D8 RID: 216
	public class RangeAttribute : ValuesAttribute
	{
		// Token: 0x06000669 RID: 1641 RVA: 0x00016B70 File Offset: 0x00014D70
		public RangeAttribute(int from, int to) : this(from, to, (from > to) ? -1 : 1)
		{
		}

		// Token: 0x0600066A RID: 1642 RVA: 0x00016B88 File Offset: 0x00014D88
		public RangeAttribute(int from, int to, int step)
		{
			Guard.ArgumentValid((step > 0 && to >= from) || (step < 0 && to <= from), "Step must be positive with to >= from or negative with to <= from", "step");
			int num = (to - from) / step + 1;
			this.data = new object[num];
			int i = 0;
			int num2 = from;
			while (i < num)
			{
				this.data[i++] = num2;
				num2 += step;
			}
		}

		// Token: 0x0600066B RID: 1643 RVA: 0x00016BFF File Offset: 0x00014DFF
		[CLSCompliant(false)]
		public RangeAttribute(uint from, uint to) : this(from, to, 1U)
		{
		}

		// Token: 0x0600066C RID: 1644 RVA: 0x00016C10 File Offset: 0x00014E10
		[CLSCompliant(false)]
		public RangeAttribute(uint from, uint to, uint step)
		{
			Guard.ArgumentValid(step > 0U, "Step must be greater than zero", "step");
			Guard.ArgumentValid(to >= from, "Value of to must be greater than or equal to from", "to");
			uint num = (to - from) / step + 1U;
			this.data = new object[num];
			uint num2 = 0U;
			uint num3 = from;
			while (num2 < num)
			{
				this.data[(int)((UIntPtr)(num2++))] = num3;
				num3 += step;
			}
		}

		// Token: 0x0600066D RID: 1645 RVA: 0x00016C8B File Offset: 0x00014E8B
		public RangeAttribute(long from, long to) : this(from, to, (from > to) ? -1L : 1L)
		{
		}

		// Token: 0x0600066E RID: 1646 RVA: 0x00016CA4 File Offset: 0x00014EA4
		public RangeAttribute(long from, long to, long step)
		{
			Guard.ArgumentValid((step > 0L && to >= from) || (step < 0L && to <= from), "Step must be positive with to >= from or negative with to <= from", "step");
			long num = (to - from) / step + 1L;
			this.data = new object[num];
			int num2 = 0;
			long num3 = from;
			while ((long)num2 < num)
			{
				this.data[num2++] = num3;
				num3 += step;
			}
		}

		// Token: 0x0600066F RID: 1647 RVA: 0x00016D20 File Offset: 0x00014F20
		[CLSCompliant(false)]
		public RangeAttribute(ulong from, ulong to) : this(from, to, 1UL)
		{
		}

		// Token: 0x06000670 RID: 1648 RVA: 0x00016D30 File Offset: 0x00014F30
		[CLSCompliant(false)]
		public RangeAttribute(ulong from, ulong to, ulong step)
		{
			Guard.ArgumentValid(step > 0UL, "Step must be greater than zero", "step");
			Guard.ArgumentValid(to >= from, "Value of to must be greater than or equal to from", "to");
			ulong num = (to - from) / step + 1UL;
			this.data = new object[num];
			ulong num2 = 0UL;
			ulong num3 = from;
			while (num2 < num)
			{
				object[] data = this.data;
				ulong num4 = num2;
				num2 = num4 + 1UL;
				data[(int)(checked((IntPtr)num4))] = num3;
				num3 += step;
			}
		}

		// Token: 0x06000671 RID: 1649 RVA: 0x00016DB0 File Offset: 0x00014FB0
		public RangeAttribute(double from, double to, double step)
		{
			Guard.ArgumentValid((step > 0.0 && to >= from) || (step < 0.0 && to <= from), "Step must be positive with to >= from or negative with to <= from", "step");
			double num = Math.Abs(step);
			double num2 = num / 1000.0;
			int num3 = (int)(Math.Abs(to - from) / num + num2 + 1.0);
			this.data = new object[num3];
			int i = 0;
			double num4 = from;
			while (i < num3)
			{
				this.data[i++] = num4;
				num4 += step;
			}
		}

		// Token: 0x06000672 RID: 1650 RVA: 0x00016E60 File Offset: 0x00015060
		public RangeAttribute(float from, float to, float step)
		{
			Guard.ArgumentValid((step > 0f && to >= from) || (step < 0f && to <= from), "Step must be positive with to >= from or negative with to <= from", "step");
			float num = Math.Abs(step);
			float num2 = num / 1000f;
			int num3 = (int)(Math.Abs(to - from) / num + num2 + 1f);
			this.data = new object[num3];
			int i = 0;
			float num4 = from;
			while (i < num3)
			{
				this.data[i++] = num4;
				num4 += step;
			}
		}
	}
}
