﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x0200013A RID: 314
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class TimeoutAttribute : PropertyAttribute, IApplyToContext
	{
		// Token: 0x06000862 RID: 2146 RVA: 0x0001D109 File Offset: 0x0001B309
		public TimeoutAttribute(int timeout) : base(timeout)
		{
			this._timeout = timeout;
		}

		// Token: 0x06000863 RID: 2147 RVA: 0x0001D121 File Offset: 0x0001B321
		void IApplyToContext.ApplyToContext(ITestExecutionContext context)
		{
			context.TestCaseTimeout = this._timeout;
		}

		// Token: 0x040001CC RID: 460
		private int _timeout;
	}
}
