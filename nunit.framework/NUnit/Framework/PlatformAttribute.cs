﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x0200007F RID: 127
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	public class PlatformAttribute : IncludeExcludeAttribute, IApplyToTest
	{
		// Token: 0x06000467 RID: 1127 RVA: 0x0000EA28 File Offset: 0x0000CC28
		public PlatformAttribute()
		{
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x0000EA3E File Offset: 0x0000CC3E
		public PlatformAttribute(string platforms) : base(platforms)
		{
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x0000EA58 File Offset: 0x0000CC58
		public void ApplyToTest(Test test)
		{
			if (test.RunState != RunState.NotRunnable && test.RunState != RunState.Ignored && !this.platformHelper.IsPlatformSupported(this))
			{
				test.RunState = RunState.Skipped;
				test.Properties.Add("_SKIPREASON", this.platformHelper.Reason);
			}
		}

		// Token: 0x040000C9 RID: 201
		private PlatformHelper platformHelper = new PlatformHelper();
	}
}
