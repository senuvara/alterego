﻿using System;

namespace NUnit.Framework
{
	// Token: 0x02000037 RID: 55
	public abstract class IncludeExcludeAttribute : NUnitAttribute
	{
		// Token: 0x060001AC RID: 428 RVA: 0x000067A0 File Offset: 0x000049A0
		public IncludeExcludeAttribute()
		{
		}

		// Token: 0x060001AD RID: 429 RVA: 0x000067AB File Offset: 0x000049AB
		public IncludeExcludeAttribute(string include)
		{
			this.include = include;
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060001AE RID: 430 RVA: 0x000067C0 File Offset: 0x000049C0
		// (set) Token: 0x060001AF RID: 431 RVA: 0x000067D8 File Offset: 0x000049D8
		public string Include
		{
			get
			{
				return this.include;
			}
			set
			{
				this.include = value;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x000067E4 File Offset: 0x000049E4
		// (set) Token: 0x060001B1 RID: 433 RVA: 0x000067FC File Offset: 0x000049FC
		public string Exclude
		{
			get
			{
				return this.exclude;
			}
			set
			{
				this.exclude = value;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x00006808 File Offset: 0x00004A08
		// (set) Token: 0x060001B3 RID: 435 RVA: 0x00006820 File Offset: 0x00004A20
		public string Reason
		{
			get
			{
				return this.reason;
			}
			set
			{
				this.reason = value;
			}
		}

		// Token: 0x0400004D RID: 77
		private string include;

		// Token: 0x0400004E RID: 78
		private string exclude;

		// Token: 0x0400004F RID: 79
		private string reason;
	}
}
