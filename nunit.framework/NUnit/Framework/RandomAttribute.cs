﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Compatibility;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x0200018A RID: 394
	public class RandomAttribute : DataAttribute, IParameterDataSource
	{
		// Token: 0x06000A4E RID: 2638 RVA: 0x000227BA File Offset: 0x000209BA
		public RandomAttribute(int count)
		{
			this._count = count;
		}

		// Token: 0x06000A4F RID: 2639 RVA: 0x000227CC File Offset: 0x000209CC
		public RandomAttribute(int min, int max, int count)
		{
			this._source = new RandomAttribute.IntDataSource(min, max, count);
		}

		// Token: 0x06000A50 RID: 2640 RVA: 0x000227E5 File Offset: 0x000209E5
		[CLSCompliant(false)]
		public RandomAttribute(uint min, uint max, int count)
		{
			this._source = new RandomAttribute.UIntDataSource(min, max, count);
		}

		// Token: 0x06000A51 RID: 2641 RVA: 0x000227FE File Offset: 0x000209FE
		public RandomAttribute(long min, long max, int count)
		{
			this._source = new RandomAttribute.LongDataSource(min, max, count);
		}

		// Token: 0x06000A52 RID: 2642 RVA: 0x00022817 File Offset: 0x00020A17
		[CLSCompliant(false)]
		public RandomAttribute(ulong min, ulong max, int count)
		{
			this._source = new RandomAttribute.ULongDataSource(min, max, count);
		}

		// Token: 0x06000A53 RID: 2643 RVA: 0x00022830 File Offset: 0x00020A30
		public RandomAttribute(short min, short max, int count)
		{
			this._source = new RandomAttribute.ShortDataSource(min, max, count);
		}

		// Token: 0x06000A54 RID: 2644 RVA: 0x00022849 File Offset: 0x00020A49
		[CLSCompliant(false)]
		public RandomAttribute(ushort min, ushort max, int count)
		{
			this._source = new RandomAttribute.UShortDataSource(min, max, count);
		}

		// Token: 0x06000A55 RID: 2645 RVA: 0x00022862 File Offset: 0x00020A62
		public RandomAttribute(double min, double max, int count)
		{
			this._source = new RandomAttribute.DoubleDataSource(min, max, count);
		}

		// Token: 0x06000A56 RID: 2646 RVA: 0x0002287B File Offset: 0x00020A7B
		public RandomAttribute(float min, float max, int count)
		{
			this._source = new RandomAttribute.FloatDataSource(min, max, count);
		}

		// Token: 0x06000A57 RID: 2647 RVA: 0x00022894 File Offset: 0x00020A94
		public RandomAttribute(byte min, byte max, int count)
		{
			this._source = new RandomAttribute.ByteDataSource(min, max, count);
		}

		// Token: 0x06000A58 RID: 2648 RVA: 0x000228AD File Offset: 0x00020AAD
		[CLSCompliant(false)]
		public RandomAttribute(sbyte min, sbyte max, int count)
		{
			this._source = new RandomAttribute.SByteDataSource(min, max, count);
		}

		// Token: 0x06000A59 RID: 2649 RVA: 0x000228C8 File Offset: 0x00020AC8
		public IEnumerable GetData(IParameterInfo parameter)
		{
			Type parameterType = parameter.ParameterType;
			if (this._source == null)
			{
				if (parameterType == typeof(int))
				{
					this._source = new RandomAttribute.IntDataSource(this._count);
				}
				else if (parameterType == typeof(uint))
				{
					this._source = new RandomAttribute.UIntDataSource(this._count);
				}
				else if (parameterType == typeof(long))
				{
					this._source = new RandomAttribute.LongDataSource(this._count);
				}
				else if (parameterType == typeof(ulong))
				{
					this._source = new RandomAttribute.ULongDataSource(this._count);
				}
				else if (parameterType == typeof(short))
				{
					this._source = new RandomAttribute.ShortDataSource(this._count);
				}
				else if (parameterType == typeof(ushort))
				{
					this._source = new RandomAttribute.UShortDataSource(this._count);
				}
				else if (parameterType == typeof(double))
				{
					this._source = new RandomAttribute.DoubleDataSource(this._count);
				}
				else if (parameterType == typeof(float))
				{
					this._source = new RandomAttribute.FloatDataSource(this._count);
				}
				else if (parameterType == typeof(byte))
				{
					this._source = new RandomAttribute.ByteDataSource(this._count);
				}
				else if (parameterType == typeof(sbyte))
				{
					this._source = new RandomAttribute.SByteDataSource(this._count);
				}
				else if (parameterType == typeof(decimal))
				{
					this._source = new RandomAttribute.DecimalDataSource(this._count);
				}
				else if (parameterType.GetTypeInfo().IsEnum)
				{
					this._source = new RandomAttribute.EnumDataSource(this._count);
				}
				else
				{
					this._source = new RandomAttribute.IntDataSource(this._count);
				}
			}
			else if (this._source.DataType != parameterType && this.WeConvert(this._source.DataType, parameterType))
			{
				this._source = new RandomAttribute.RandomDataConverter(this._source);
			}
			return this._source.GetData(parameter);
		}

		// Token: 0x06000A5A RID: 2650 RVA: 0x00022B40 File Offset: 0x00020D40
		private bool WeConvert(Type sourceType, Type targetType)
		{
			bool result;
			if (targetType == typeof(short) || targetType == typeof(ushort) || targetType == typeof(byte) || targetType == typeof(sbyte))
			{
				result = (sourceType == typeof(int));
			}
			else
			{
				result = (targetType == typeof(decimal) && (sourceType == typeof(int) || sourceType == typeof(double)));
			}
			return result;
		}

		// Token: 0x0400026D RID: 621
		private RandomAttribute.RandomDataSource _source;

		// Token: 0x0400026E RID: 622
		private int _count;

		// Token: 0x0200018B RID: 395
		private abstract class RandomDataSource : IParameterDataSource
		{
			// Token: 0x17000261 RID: 609
			// (get) Token: 0x06000A5B RID: 2651 RVA: 0x00022BD8 File Offset: 0x00020DD8
			// (set) Token: 0x06000A5C RID: 2652 RVA: 0x00022BEF File Offset: 0x00020DEF
			public Type DataType
			{
				[CompilerGenerated]
				get
				{
					return this.<DataType>k__BackingField;
				}
				[CompilerGenerated]
				protected set
				{
					this.<DataType>k__BackingField = value;
				}
			}

			// Token: 0x06000A5D RID: 2653
			public abstract IEnumerable GetData(IParameterInfo parameter);

			// Token: 0x06000A5E RID: 2654 RVA: 0x00022BF8 File Offset: 0x00020DF8
			protected RandomDataSource()
			{
			}

			// Token: 0x0400026F RID: 623
			[CompilerGenerated]
			private Type <DataType>k__BackingField;
		}

		// Token: 0x0200018C RID: 396
		private abstract class RandomDataSource<T> : RandomAttribute.RandomDataSource
		{
			// Token: 0x06000A5F RID: 2655 RVA: 0x00022C00 File Offset: 0x00020E00
			protected RandomDataSource(int count)
			{
				this._count = count;
				this._inRange = false;
				base.DataType = typeof(T);
			}

			// Token: 0x06000A60 RID: 2656 RVA: 0x00022C2A File Offset: 0x00020E2A
			protected RandomDataSource(T min, T max, int count)
			{
				this._min = min;
				this._max = max;
				this._count = count;
				this._inRange = true;
				base.DataType = typeof(T);
			}

			// Token: 0x06000A61 RID: 2657 RVA: 0x00022E1C File Offset: 0x0002101C
			public override IEnumerable GetData(IParameterInfo parameter)
			{
				this._randomizer = Randomizer.GetRandomizer(parameter.ParameterInfo);
				for (int i = 0; i < this._count; i++)
				{
					yield return this._inRange ? this.GetNext(this._min, this._max) : this.GetNext();
				}
				yield break;
			}

			// Token: 0x06000A62 RID: 2658
			protected abstract T GetNext();

			// Token: 0x06000A63 RID: 2659
			protected abstract T GetNext(T min, T max);

			// Token: 0x04000270 RID: 624
			private T _min;

			// Token: 0x04000271 RID: 625
			private T _max;

			// Token: 0x04000272 RID: 626
			private int _count;

			// Token: 0x04000273 RID: 627
			private bool _inRange;

			// Token: 0x04000274 RID: 628
			protected Randomizer _randomizer;

			// Token: 0x020001C5 RID: 453
			[CompilerGenerated]
			private sealed class <GetData>d__0 : IEnumerable<object>, IEnumerable, IEnumerator<object>, IEnumerator, IDisposable
			{
				// Token: 0x06000B68 RID: 2920 RVA: 0x00022C64 File Offset: 0x00020E64
				[DebuggerHidden]
				IEnumerator<object> IEnumerable<object>.GetEnumerator()
				{
					RandomAttribute.RandomDataSource<T>.<GetData>d__0 <GetData>d__;
					if (Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId && this.<>1__state == -2)
					{
						this.<>1__state = 0;
						<GetData>d__ = this;
					}
					else
					{
						<GetData>d__ = new RandomAttribute.RandomDataSource<T>.<GetData>d__0(0);
						<GetData>d__.<>4__this = this;
					}
					<GetData>d__.parameter = parameter;
					return <GetData>d__;
				}

				// Token: 0x06000B69 RID: 2921 RVA: 0x00022CC8 File Offset: 0x00020EC8
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator();
				}

				// Token: 0x06000B6A RID: 2922 RVA: 0x00022CE0 File Offset: 0x00020EE0
				bool IEnumerator.MoveNext()
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						this._randomizer = Randomizer.GetRandomizer(parameter.ParameterInfo);
						i = 0;
						break;
					case 1:
						this.<>1__state = -1;
						i++;
						break;
					default:
						goto IL_CD;
					}
					if (i < this._count)
					{
						this.<>2__current = (this._inRange ? this.GetNext(this._min, this._max) : this.GetNext());
						this.<>1__state = 1;
						return true;
					}
					IL_CD:
					return false;
				}

				// Token: 0x1700028A RID: 650
				// (get) Token: 0x06000B6B RID: 2923 RVA: 0x00022DC0 File Offset: 0x00020FC0
				object IEnumerator<object>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06000B6C RID: 2924 RVA: 0x00022DD7 File Offset: 0x00020FD7
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000B6D RID: 2925 RVA: 0x00022DDE File Offset: 0x00020FDE
				void IDisposable.Dispose()
				{
				}

				// Token: 0x1700028B RID: 651
				// (get) Token: 0x06000B6E RID: 2926 RVA: 0x00022DE4 File Offset: 0x00020FE4
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06000B6F RID: 2927 RVA: 0x00022DFB File Offset: 0x00020FFB
				[DebuggerHidden]
				public <GetData>d__0(int <>1__state)
				{
					this.<>1__state = <>1__state;
					this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
				}

				// Token: 0x040002F4 RID: 756
				private object <>2__current;

				// Token: 0x040002F5 RID: 757
				private int <>1__state;

				// Token: 0x040002F6 RID: 758
				private int <>l__initialThreadId;

				// Token: 0x040002F7 RID: 759
				public RandomAttribute.RandomDataSource<T> <>4__this;

				// Token: 0x040002F8 RID: 760
				public IParameterInfo parameter;

				// Token: 0x040002F9 RID: 761
				public IParameterInfo <>3__parameter;

				// Token: 0x040002FA RID: 762
				public int <i>5__1;
			}
		}

		// Token: 0x0200018D RID: 397
		private class RandomDataConverter : RandomAttribute.RandomDataSource
		{
			// Token: 0x06000A64 RID: 2660 RVA: 0x00022E44 File Offset: 0x00021044
			public RandomDataConverter(IParameterDataSource source)
			{
				this._source = source;
			}

			// Token: 0x06000A65 RID: 2661 RVA: 0x000232C0 File Offset: 0x000214C0
			public override IEnumerable GetData(IParameterInfo parameter)
			{
				Type parmType = parameter.ParameterType;
				foreach (object obj in this._source.GetData(parameter))
				{
					if (obj is int)
					{
						int ival = (int)obj;
						if (parmType == typeof(short))
						{
							yield return (short)ival;
						}
						else if (parmType == typeof(ushort))
						{
							yield return (ushort)ival;
						}
						else if (parmType == typeof(byte))
						{
							yield return (byte)ival;
						}
						else if (parmType == typeof(sbyte))
						{
							yield return (sbyte)ival;
						}
						else if (parmType == typeof(decimal))
						{
							yield return ival;
						}
					}
					else if (obj is double)
					{
						double d = (double)obj;
						if (parmType == typeof(decimal))
						{
							yield return (decimal)d;
						}
					}
				}
				yield break;
			}

			// Token: 0x04000275 RID: 629
			private IParameterDataSource _source;

			// Token: 0x020001C6 RID: 454
			[CompilerGenerated]
			private sealed class <GetData>d__4 : IEnumerable<object>, IEnumerable, IEnumerator<object>, IEnumerator, IDisposable
			{
				// Token: 0x06000B70 RID: 2928 RVA: 0x00022E58 File Offset: 0x00021058
				[DebuggerHidden]
				IEnumerator<object> IEnumerable<object>.GetEnumerator()
				{
					RandomAttribute.RandomDataConverter.<GetData>d__4 <GetData>d__;
					if (Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId && this.<>1__state == -2)
					{
						this.<>1__state = 0;
						<GetData>d__ = this;
					}
					else
					{
						<GetData>d__ = new RandomAttribute.RandomDataConverter.<GetData>d__4(0);
						<GetData>d__.<>4__this = this;
					}
					<GetData>d__.parameter = parameter;
					return <GetData>d__;
				}

				// Token: 0x06000B71 RID: 2929 RVA: 0x00022EBC File Offset: 0x000210BC
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator();
				}

				// Token: 0x06000B72 RID: 2930 RVA: 0x00022ED4 File Offset: 0x000210D4
				bool IEnumerator.MoveNext()
				{
					bool result;
					try
					{
						switch (this.<>1__state)
						{
						case 0:
							this.<>1__state = -1;
							parmType = parameter.ParameterType;
							enumerator = this._source.GetData(parameter).GetEnumerator();
							this.<>1__state = 1;
							goto IL_29A;
						case 2:
							this.<>1__state = 1;
							goto IL_22C;
						case 3:
							this.<>1__state = 1;
							goto IL_22C;
						case 4:
							this.<>1__state = 1;
							goto IL_22C;
						case 5:
							this.<>1__state = 1;
							goto IL_22C;
						case 6:
							this.<>1__state = 1;
							goto IL_22C;
						case 7:
							this.<>1__state = 1;
							goto IL_298;
						}
						goto IL_2B4;
						IL_22C:
						IL_298:
						IL_299:
						IL_29A:
						if (!enumerator.MoveNext())
						{
							this.<>m__Finallyb();
						}
						else
						{
							obj = enumerator.Current;
							if (obj is int)
							{
								ival = (int)obj;
								if (parmType == typeof(short))
								{
									this.<>2__current = (short)ival;
									this.<>1__state = 2;
									return true;
								}
								if (parmType == typeof(ushort))
								{
									this.<>2__current = (ushort)ival;
									this.<>1__state = 3;
									return true;
								}
								if (parmType == typeof(byte))
								{
									this.<>2__current = (byte)ival;
									this.<>1__state = 4;
									return true;
								}
								if (parmType == typeof(sbyte))
								{
									this.<>2__current = (sbyte)ival;
									this.<>1__state = 5;
									return true;
								}
								if (parmType == typeof(decimal))
								{
									this.<>2__current = ival;
									this.<>1__state = 6;
									return true;
								}
								goto IL_22C;
							}
							else
							{
								if (!(obj is double))
								{
									goto IL_299;
								}
								d = (double)obj;
								if (parmType == typeof(decimal))
								{
									this.<>2__current = (decimal)d;
									this.<>1__state = 7;
									return true;
								}
								goto IL_298;
							}
						}
						IL_2B4:
						result = false;
					}
					catch
					{
						this.System.IDisposable.Dispose();
						throw;
					}
					return result;
				}

				// Token: 0x1700028C RID: 652
				// (get) Token: 0x06000B73 RID: 2931 RVA: 0x000231C0 File Offset: 0x000213C0
				object IEnumerator<object>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06000B74 RID: 2932 RVA: 0x000231D7 File Offset: 0x000213D7
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000B75 RID: 2933 RVA: 0x000231E0 File Offset: 0x000213E0
				void IDisposable.Dispose()
				{
					switch (this.<>1__state)
					{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					case 4:
						break;
					case 5:
						break;
					case 6:
						break;
					case 7:
						break;
					default:
						return;
					}
					try
					{
					}
					finally
					{
						this.<>m__Finallyb();
					}
				}

				// Token: 0x1700028D RID: 653
				// (get) Token: 0x06000B76 RID: 2934 RVA: 0x00023248 File Offset: 0x00021448
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06000B77 RID: 2935 RVA: 0x0002325F File Offset: 0x0002145F
				[DebuggerHidden]
				public <GetData>d__4(int <>1__state)
				{
					this.<>1__state = <>1__state;
					this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
				}

				// Token: 0x06000B78 RID: 2936 RVA: 0x00023280 File Offset: 0x00021480
				private void <>m__Finallyb()
				{
					this.<>1__state = -1;
					disposable = (enumerator as IDisposable);
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}

				// Token: 0x040002FB RID: 763
				private object <>2__current;

				// Token: 0x040002FC RID: 764
				private int <>1__state;

				// Token: 0x040002FD RID: 765
				private int <>l__initialThreadId;

				// Token: 0x040002FE RID: 766
				public RandomAttribute.RandomDataConverter <>4__this;

				// Token: 0x040002FF RID: 767
				public IParameterInfo parameter;

				// Token: 0x04000300 RID: 768
				public IParameterInfo <>3__parameter;

				// Token: 0x04000301 RID: 769
				public Type <parmType>5__5;

				// Token: 0x04000302 RID: 770
				public object <obj>5__6;

				// Token: 0x04000303 RID: 771
				public int <ival>5__7;

				// Token: 0x04000304 RID: 772
				public double <d>5__8;

				// Token: 0x04000305 RID: 773
				public IEnumerator <>7__wrap9;

				// Token: 0x04000306 RID: 774
				public IDisposable <>7__wrapa;
			}
		}

		// Token: 0x0200018E RID: 398
		private class IntDataSource : RandomAttribute.RandomDataSource<int>
		{
			// Token: 0x06000A66 RID: 2662 RVA: 0x000232E8 File Offset: 0x000214E8
			public IntDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A67 RID: 2663 RVA: 0x000232F4 File Offset: 0x000214F4
			public IntDataSource(int min, int max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A68 RID: 2664 RVA: 0x00023304 File Offset: 0x00021504
			protected override int GetNext()
			{
				return this._randomizer.Next();
			}

			// Token: 0x06000A69 RID: 2665 RVA: 0x00023324 File Offset: 0x00021524
			protected override int GetNext(int min, int max)
			{
				return this._randomizer.Next(min, max);
			}
		}

		// Token: 0x0200018F RID: 399
		private class UIntDataSource : RandomAttribute.RandomDataSource<uint>
		{
			// Token: 0x06000A6A RID: 2666 RVA: 0x00023343 File Offset: 0x00021543
			public UIntDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A6B RID: 2667 RVA: 0x0002334F File Offset: 0x0002154F
			public UIntDataSource(uint min, uint max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A6C RID: 2668 RVA: 0x00023360 File Offset: 0x00021560
			protected override uint GetNext()
			{
				return this._randomizer.NextUInt();
			}

			// Token: 0x06000A6D RID: 2669 RVA: 0x00023380 File Offset: 0x00021580
			protected override uint GetNext(uint min, uint max)
			{
				return this._randomizer.NextUInt(min, max);
			}
		}

		// Token: 0x02000190 RID: 400
		private class LongDataSource : RandomAttribute.RandomDataSource<long>
		{
			// Token: 0x06000A6E RID: 2670 RVA: 0x0002339F File Offset: 0x0002159F
			public LongDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A6F RID: 2671 RVA: 0x000233AB File Offset: 0x000215AB
			public LongDataSource(long min, long max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A70 RID: 2672 RVA: 0x000233BC File Offset: 0x000215BC
			protected override long GetNext()
			{
				return this._randomizer.NextLong();
			}

			// Token: 0x06000A71 RID: 2673 RVA: 0x000233DC File Offset: 0x000215DC
			protected override long GetNext(long min, long max)
			{
				return this._randomizer.NextLong(min, max);
			}
		}

		// Token: 0x02000191 RID: 401
		private class ULongDataSource : RandomAttribute.RandomDataSource<ulong>
		{
			// Token: 0x06000A72 RID: 2674 RVA: 0x000233FB File Offset: 0x000215FB
			public ULongDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A73 RID: 2675 RVA: 0x00023407 File Offset: 0x00021607
			public ULongDataSource(ulong min, ulong max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A74 RID: 2676 RVA: 0x00023418 File Offset: 0x00021618
			protected override ulong GetNext()
			{
				return this._randomizer.NextULong();
			}

			// Token: 0x06000A75 RID: 2677 RVA: 0x00023438 File Offset: 0x00021638
			protected override ulong GetNext(ulong min, ulong max)
			{
				return this._randomizer.NextULong(min, max);
			}
		}

		// Token: 0x02000192 RID: 402
		private class ShortDataSource : RandomAttribute.RandomDataSource<short>
		{
			// Token: 0x06000A76 RID: 2678 RVA: 0x00023457 File Offset: 0x00021657
			public ShortDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A77 RID: 2679 RVA: 0x00023463 File Offset: 0x00021663
			public ShortDataSource(short min, short max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A78 RID: 2680 RVA: 0x00023474 File Offset: 0x00021674
			protected override short GetNext()
			{
				return this._randomizer.NextShort();
			}

			// Token: 0x06000A79 RID: 2681 RVA: 0x00023494 File Offset: 0x00021694
			protected override short GetNext(short min, short max)
			{
				return this._randomizer.NextShort(min, max);
			}
		}

		// Token: 0x02000193 RID: 403
		private class UShortDataSource : RandomAttribute.RandomDataSource<ushort>
		{
			// Token: 0x06000A7A RID: 2682 RVA: 0x000234B3 File Offset: 0x000216B3
			public UShortDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A7B RID: 2683 RVA: 0x000234BF File Offset: 0x000216BF
			public UShortDataSource(ushort min, ushort max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A7C RID: 2684 RVA: 0x000234D0 File Offset: 0x000216D0
			protected override ushort GetNext()
			{
				return this._randomizer.NextUShort();
			}

			// Token: 0x06000A7D RID: 2685 RVA: 0x000234F0 File Offset: 0x000216F0
			protected override ushort GetNext(ushort min, ushort max)
			{
				return this._randomizer.NextUShort(min, max);
			}
		}

		// Token: 0x02000194 RID: 404
		private class DoubleDataSource : RandomAttribute.RandomDataSource<double>
		{
			// Token: 0x06000A7E RID: 2686 RVA: 0x0002350F File Offset: 0x0002170F
			public DoubleDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A7F RID: 2687 RVA: 0x0002351B File Offset: 0x0002171B
			public DoubleDataSource(double min, double max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A80 RID: 2688 RVA: 0x0002352C File Offset: 0x0002172C
			protected override double GetNext()
			{
				return this._randomizer.NextDouble();
			}

			// Token: 0x06000A81 RID: 2689 RVA: 0x0002354C File Offset: 0x0002174C
			protected override double GetNext(double min, double max)
			{
				return this._randomizer.NextDouble(min, max);
			}
		}

		// Token: 0x02000195 RID: 405
		private class FloatDataSource : RandomAttribute.RandomDataSource<float>
		{
			// Token: 0x06000A82 RID: 2690 RVA: 0x0002356B File Offset: 0x0002176B
			public FloatDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A83 RID: 2691 RVA: 0x00023577 File Offset: 0x00021777
			public FloatDataSource(float min, float max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A84 RID: 2692 RVA: 0x00023588 File Offset: 0x00021788
			protected override float GetNext()
			{
				return this._randomizer.NextFloat();
			}

			// Token: 0x06000A85 RID: 2693 RVA: 0x000235A8 File Offset: 0x000217A8
			protected override float GetNext(float min, float max)
			{
				return this._randomizer.NextFloat(min, max);
			}
		}

		// Token: 0x02000196 RID: 406
		private class ByteDataSource : RandomAttribute.RandomDataSource<byte>
		{
			// Token: 0x06000A86 RID: 2694 RVA: 0x000235C7 File Offset: 0x000217C7
			public ByteDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A87 RID: 2695 RVA: 0x000235D3 File Offset: 0x000217D3
			public ByteDataSource(byte min, byte max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A88 RID: 2696 RVA: 0x000235E4 File Offset: 0x000217E4
			protected override byte GetNext()
			{
				return this._randomizer.NextByte();
			}

			// Token: 0x06000A89 RID: 2697 RVA: 0x00023604 File Offset: 0x00021804
			protected override byte GetNext(byte min, byte max)
			{
				return this._randomizer.NextByte(min, max);
			}
		}

		// Token: 0x02000197 RID: 407
		private class SByteDataSource : RandomAttribute.RandomDataSource<sbyte>
		{
			// Token: 0x06000A8A RID: 2698 RVA: 0x00023623 File Offset: 0x00021823
			public SByteDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A8B RID: 2699 RVA: 0x0002362F File Offset: 0x0002182F
			public SByteDataSource(sbyte min, sbyte max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A8C RID: 2700 RVA: 0x00023640 File Offset: 0x00021840
			protected override sbyte GetNext()
			{
				return this._randomizer.NextSByte();
			}

			// Token: 0x06000A8D RID: 2701 RVA: 0x00023660 File Offset: 0x00021860
			protected override sbyte GetNext(sbyte min, sbyte max)
			{
				return this._randomizer.NextSByte(min, max);
			}
		}

		// Token: 0x02000198 RID: 408
		private class EnumDataSource : RandomAttribute.RandomDataSource
		{
			// Token: 0x06000A8E RID: 2702 RVA: 0x0002367F File Offset: 0x0002187F
			public EnumDataSource(int count)
			{
				this._count = count;
				base.DataType = typeof(Enum);
			}

			// Token: 0x06000A8F RID: 2703 RVA: 0x00023868 File Offset: 0x00021A68
			public override IEnumerable GetData(IParameterInfo parameter)
			{
				Guard.ArgumentValid(parameter.ParameterType.GetTypeInfo().IsEnum, "EnumDataSource requires an enum parameter", "parameter");
				Randomizer randomizer = Randomizer.GetRandomizer(parameter.ParameterInfo);
				base.DataType = parameter.ParameterType;
				for (int i = 0; i < this._count; i++)
				{
					yield return randomizer.NextEnum(parameter.ParameterType);
				}
				yield break;
			}

			// Token: 0x04000276 RID: 630
			private int _count;

			// Token: 0x020001C7 RID: 455
			[CompilerGenerated]
			private sealed class <GetData>d__e : IEnumerable<object>, IEnumerable, IEnumerator<object>, IEnumerator, IDisposable
			{
				// Token: 0x06000B79 RID: 2937 RVA: 0x000236A4 File Offset: 0x000218A4
				[DebuggerHidden]
				IEnumerator<object> IEnumerable<object>.GetEnumerator()
				{
					RandomAttribute.EnumDataSource.<GetData>d__e <GetData>d__e;
					if (Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId && this.<>1__state == -2)
					{
						this.<>1__state = 0;
						<GetData>d__e = this;
					}
					else
					{
						<GetData>d__e = new RandomAttribute.EnumDataSource.<GetData>d__e(0);
						<GetData>d__e.<>4__this = this;
					}
					<GetData>d__e.parameter = parameter;
					return <GetData>d__e;
				}

				// Token: 0x06000B7A RID: 2938 RVA: 0x00023708 File Offset: 0x00021908
				[DebuggerHidden]
				IEnumerator IEnumerable.GetEnumerator()
				{
					return this.System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator();
				}

				// Token: 0x06000B7B RID: 2939 RVA: 0x00023720 File Offset: 0x00021920
				bool IEnumerator.MoveNext()
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						Guard.ArgumentValid(parameter.ParameterType.GetTypeInfo().IsEnum, "EnumDataSource requires an enum parameter", "parameter");
						randomizer = Randomizer.GetRandomizer(parameter.ParameterInfo);
						base.DataType = parameter.ParameterType;
						i = 0;
						break;
					case 1:
						this.<>1__state = -1;
						i++;
						break;
					default:
						goto IL_DA;
					}
					if (i < this._count)
					{
						this.<>2__current = randomizer.NextEnum(parameter.ParameterType);
						this.<>1__state = 1;
						return true;
					}
					IL_DA:
					return false;
				}

				// Token: 0x1700028E RID: 654
				// (get) Token: 0x06000B7C RID: 2940 RVA: 0x0002380C File Offset: 0x00021A0C
				object IEnumerator<object>.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06000B7D RID: 2941 RVA: 0x00023823 File Offset: 0x00021A23
				[DebuggerHidden]
				void IEnumerator.Reset()
				{
					throw new NotSupportedException();
				}

				// Token: 0x06000B7E RID: 2942 RVA: 0x0002382A File Offset: 0x00021A2A
				void IDisposable.Dispose()
				{
				}

				// Token: 0x1700028F RID: 655
				// (get) Token: 0x06000B7F RID: 2943 RVA: 0x00023830 File Offset: 0x00021A30
				object IEnumerator.Current
				{
					[DebuggerHidden]
					get
					{
						return this.<>2__current;
					}
				}

				// Token: 0x06000B80 RID: 2944 RVA: 0x00023847 File Offset: 0x00021A47
				[DebuggerHidden]
				public <GetData>d__e(int <>1__state)
				{
					this.<>1__state = <>1__state;
					this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
				}

				// Token: 0x04000307 RID: 775
				private object <>2__current;

				// Token: 0x04000308 RID: 776
				private int <>1__state;

				// Token: 0x04000309 RID: 777
				private int <>l__initialThreadId;

				// Token: 0x0400030A RID: 778
				public RandomAttribute.EnumDataSource <>4__this;

				// Token: 0x0400030B RID: 779
				public IParameterInfo parameter;

				// Token: 0x0400030C RID: 780
				public IParameterInfo <>3__parameter;

				// Token: 0x0400030D RID: 781
				public Randomizer <randomizer>5__f;

				// Token: 0x0400030E RID: 782
				public int <i>5__10;
			}
		}

		// Token: 0x02000199 RID: 409
		private class DecimalDataSource : RandomAttribute.RandomDataSource<decimal>
		{
			// Token: 0x06000A90 RID: 2704 RVA: 0x00023890 File Offset: 0x00021A90
			public DecimalDataSource(int count) : base(count)
			{
			}

			// Token: 0x06000A91 RID: 2705 RVA: 0x0002389C File Offset: 0x00021A9C
			public DecimalDataSource(decimal min, decimal max, int count) : base(min, max, count)
			{
			}

			// Token: 0x06000A92 RID: 2706 RVA: 0x000238AC File Offset: 0x00021AAC
			protected override decimal GetNext()
			{
				return this._randomizer.NextDecimal();
			}

			// Token: 0x06000A93 RID: 2707 RVA: 0x000238CC File Offset: 0x00021ACC
			protected override decimal GetNext(decimal min, decimal max)
			{
				return this._randomizer.NextDecimal(min, max);
			}
		}
	}
}
