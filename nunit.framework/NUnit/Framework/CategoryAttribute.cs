﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace NUnit.Framework
{
	// Token: 0x0200014B RID: 331
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
	public class CategoryAttribute : NUnitAttribute, IApplyToTest
	{
		// Token: 0x060008E1 RID: 2273 RVA: 0x0001E339 File Offset: 0x0001C539
		public CategoryAttribute(string name)
		{
			this.categoryName = name.Trim();
		}

		// Token: 0x060008E2 RID: 2274 RVA: 0x0001E350 File Offset: 0x0001C550
		protected CategoryAttribute()
		{
			this.categoryName = base.GetType().Name;
			if (this.categoryName.EndsWith("Attribute"))
			{
				this.categoryName = this.categoryName.Substring(0, this.categoryName.Length - 9);
			}
		}

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x060008E3 RID: 2275 RVA: 0x0001E3B0 File Offset: 0x0001C5B0
		public string Name
		{
			get
			{
				return this.categoryName;
			}
		}

		// Token: 0x060008E4 RID: 2276 RVA: 0x0001E3D0 File Offset: 0x0001C5D0
		public void ApplyToTest(Test test)
		{
			test.Properties.Add("Category", this.Name);
			if (this.Name.IndexOfAny(new char[]
			{
				',',
				'!',
				'+',
				'-'
			}) >= 0)
			{
				test.RunState = RunState.NotRunnable;
				test.Properties.Set("_SKIPREASON", "Category name must not contain ',', '!', '+' or '-'");
			}
		}

		// Token: 0x040001EB RID: 491
		protected string categoryName;
	}
}
