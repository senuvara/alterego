﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;

namespace NUnit.Framework
{
	// Token: 0x020000C8 RID: 200
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
	public class TestFixtureSourceAttribute : NUnitAttribute, IFixtureBuilder
	{
		// Token: 0x06000620 RID: 1568 RVA: 0x000152A4 File Offset: 0x000134A4
		public TestFixtureSourceAttribute(string sourceName)
		{
			this.SourceName = sourceName;
		}

		// Token: 0x06000621 RID: 1569 RVA: 0x000152C2 File Offset: 0x000134C2
		public TestFixtureSourceAttribute(Type sourceType, string sourceName)
		{
			this.SourceType = sourceType;
			this.SourceName = sourceName;
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x000152E8 File Offset: 0x000134E8
		public TestFixtureSourceAttribute(Type sourceType)
		{
			this.SourceType = sourceType;
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x06000623 RID: 1571 RVA: 0x00015308 File Offset: 0x00013508
		// (set) Token: 0x06000624 RID: 1572 RVA: 0x0001531F File Offset: 0x0001351F
		public string SourceName
		{
			[CompilerGenerated]
			get
			{
				return this.<SourceName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<SourceName>k__BackingField = value;
			}
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x06000625 RID: 1573 RVA: 0x00015328 File Offset: 0x00013528
		// (set) Token: 0x06000626 RID: 1574 RVA: 0x0001533F File Offset: 0x0001353F
		public Type SourceType
		{
			[CompilerGenerated]
			get
			{
				return this.<SourceType>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<SourceType>k__BackingField = value;
			}
		}

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x06000627 RID: 1575 RVA: 0x00015348 File Offset: 0x00013548
		// (set) Token: 0x06000628 RID: 1576 RVA: 0x0001535F File Offset: 0x0001355F
		public string Category
		{
			[CompilerGenerated]
			get
			{
				return this.<Category>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Category>k__BackingField = value;
			}
		}

		// Token: 0x06000629 RID: 1577 RVA: 0x000155B8 File Offset: 0x000137B8
		public IEnumerable<TestSuite> BuildFrom(ITypeInfo typeInfo)
		{
			Type sourceType = this.SourceType ?? typeInfo.Type;
			foreach (ITestFixtureData testFixtureData in this.GetParametersFor(sourceType))
			{
				TestFixtureParameters parms = (TestFixtureParameters)testFixtureData;
				yield return this._builder.BuildFrom(typeInfo, parms);
			}
			yield break;
		}

		// Token: 0x0600062A RID: 1578 RVA: 0x000155E0 File Offset: 0x000137E0
		public IEnumerable<ITestFixtureData> GetParametersFor(Type sourceType)
		{
			List<ITestFixtureData> list = new List<ITestFixtureData>();
			try
			{
				IEnumerable testFixtureSource = this.GetTestFixtureSource(sourceType);
				if (testFixtureSource != null)
				{
					foreach (object obj in testFixtureSource)
					{
						ITestFixtureData testFixtureData = obj as ITestFixtureData;
						if (testFixtureData == null)
						{
							object[] array = obj as object[];
							if (array == null)
							{
								array = new object[]
								{
									obj
								};
							}
							testFixtureData = new TestFixtureParameters(array);
						}
						if (this.Category != null)
						{
							foreach (string value in this.Category.Split(new char[]
							{
								','
							}))
							{
								testFixtureData.Properties.Add("Category", value);
							}
						}
						list.Add(testFixtureData);
					}
				}
			}
			catch (Exception exception)
			{
				list.Clear();
				list.Add(new TestFixtureParameters(exception));
			}
			return list;
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x00015760 File Offset: 0x00013960
		private IEnumerable GetTestFixtureSource(Type sourceType)
		{
			IEnumerable result;
			if (this.SourceName == null)
			{
				result = (Reflect.Construct(sourceType) as IEnumerable);
			}
			else
			{
				MemberInfo[] member = sourceType.GetMember(this.SourceName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
				if (member.Length == 1)
				{
					MemberInfo memberInfo = member[0];
					FieldInfo fieldInfo = memberInfo as FieldInfo;
					if (fieldInfo != null)
					{
						return fieldInfo.IsStatic ? ((IEnumerable)fieldInfo.GetValue(null)) : TestFixtureSourceAttribute.SourceMustBeStaticError();
					}
					PropertyInfo propertyInfo = memberInfo as PropertyInfo;
					if (propertyInfo != null)
					{
						return propertyInfo.GetGetMethod(true).IsStatic ? ((IEnumerable)propertyInfo.GetValue(null, null)) : TestFixtureSourceAttribute.SourceMustBeStaticError();
					}
					MethodInfo methodInfo = memberInfo as MethodInfo;
					if (methodInfo != null)
					{
						return methodInfo.IsStatic ? ((IEnumerable)methodInfo.Invoke(null, null)) : TestFixtureSourceAttribute.SourceMustBeStaticError();
					}
				}
				result = null;
			}
			return result;
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x0001585C File Offset: 0x00013A5C
		private static IEnumerable SourceMustBeStaticError()
		{
			TestFixtureParameters testFixtureParameters = new TestFixtureParameters();
			testFixtureParameters.RunState = RunState.NotRunnable;
			testFixtureParameters.Properties.Set("_SKIPREASON", "The sourceName specified on a TestCaseSourceAttribute must refer to a static field, property or method.");
			return new TestFixtureParameters[]
			{
				testFixtureParameters
			};
		}

		// Token: 0x04000145 RID: 325
		public const string MUST_BE_STATIC = "The sourceName specified on a TestCaseSourceAttribute must refer to a static field, property or method.";

		// Token: 0x04000146 RID: 326
		private readonly NUnitTestFixtureBuilder _builder = new NUnitTestFixtureBuilder();

		// Token: 0x04000147 RID: 327
		[CompilerGenerated]
		private string <SourceName>k__BackingField;

		// Token: 0x04000148 RID: 328
		[CompilerGenerated]
		private Type <SourceType>k__BackingField;

		// Token: 0x04000149 RID: 329
		[CompilerGenerated]
		private string <Category>k__BackingField;

		// Token: 0x020001B4 RID: 436
		[CompilerGenerated]
		private sealed class <BuildFrom>d__0 : IEnumerable<TestSuite>, IEnumerable, IEnumerator<TestSuite>, IEnumerator, IDisposable
		{
			// Token: 0x06000B30 RID: 2864 RVA: 0x00015368 File Offset: 0x00013568
			[DebuggerHidden]
			IEnumerator<TestSuite> IEnumerable<TestSuite>.GetEnumerator()
			{
				TestFixtureSourceAttribute.<BuildFrom>d__0 <BuildFrom>d__;
				if (Thread.CurrentThread.ManagedThreadId == this.<>l__initialThreadId && this.<>1__state == -2)
				{
					this.<>1__state = 0;
					<BuildFrom>d__ = this;
				}
				else
				{
					<BuildFrom>d__ = new TestFixtureSourceAttribute.<BuildFrom>d__0(0);
					<BuildFrom>d__.<>4__this = this;
				}
				<BuildFrom>d__.typeInfo = typeInfo;
				return <BuildFrom>d__;
			}

			// Token: 0x06000B31 RID: 2865 RVA: 0x000153CC File Offset: 0x000135CC
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<NUnit.Framework.Internal.TestSuite>.GetEnumerator();
			}

			// Token: 0x06000B32 RID: 2866 RVA: 0x000153E4 File Offset: 0x000135E4
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					switch (this.<>1__state)
					{
					case 0:
						this.<>1__state = -1;
						sourceType = (this.SourceType ?? typeInfo.Type);
						enumerator = this.GetParametersFor(sourceType).GetEnumerator();
						this.<>1__state = 1;
						goto IL_BF;
					case 2:
						this.<>1__state = 1;
						goto IL_BF;
					}
					goto IL_D6;
					IL_BF:
					if (enumerator.MoveNext())
					{
						parms = (TestFixtureParameters)enumerator.Current;
						this.<>2__current = this._builder.BuildFrom(typeInfo, parms);
						this.<>1__state = 2;
						return true;
					}
					this.<>m__Finally4();
					IL_D6:
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x17000284 RID: 644
			// (get) Token: 0x06000B33 RID: 2867 RVA: 0x000154E8 File Offset: 0x000136E8
			TestSuite IEnumerator<TestSuite>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B34 RID: 2868 RVA: 0x000154FF File Offset: 0x000136FF
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06000B35 RID: 2869 RVA: 0x00015508 File Offset: 0x00013708
			void IDisposable.Dispose()
			{
				switch (this.<>1__state)
				{
				case 1:
					break;
				case 2:
					break;
				default:
					return;
				}
				try
				{
				}
				finally
				{
					this.<>m__Finally4();
				}
			}

			// Token: 0x17000285 RID: 645
			// (get) Token: 0x06000B36 RID: 2870 RVA: 0x00015550 File Offset: 0x00013750
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06000B37 RID: 2871 RVA: 0x00015567 File Offset: 0x00013767
			[DebuggerHidden]
			public <BuildFrom>d__0(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Thread.CurrentThread.ManagedThreadId;
			}

			// Token: 0x06000B38 RID: 2872 RVA: 0x00015588 File Offset: 0x00013788
			private void <>m__Finally4()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x040002C8 RID: 712
			private TestSuite <>2__current;

			// Token: 0x040002C9 RID: 713
			private int <>1__state;

			// Token: 0x040002CA RID: 714
			private int <>l__initialThreadId;

			// Token: 0x040002CB RID: 715
			public TestFixtureSourceAttribute <>4__this;

			// Token: 0x040002CC RID: 716
			public ITypeInfo typeInfo;

			// Token: 0x040002CD RID: 717
			public ITypeInfo <>3__typeInfo;

			// Token: 0x040002CE RID: 718
			public Type <sourceType>5__1;

			// Token: 0x040002CF RID: 719
			public TestFixtureParameters <parms>5__2;

			// Token: 0x040002D0 RID: 720
			public IEnumerator<ITestFixtureData> <>7__wrap3;
		}
	}
}
