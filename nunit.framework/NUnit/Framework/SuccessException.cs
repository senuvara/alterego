﻿using System;
using System.Runtime.Serialization;
using NUnit.Framework.Interfaces;

namespace NUnit.Framework
{
	// Token: 0x020000FE RID: 254
	[Serializable]
	public class SuccessException : ResultStateException
	{
		// Token: 0x06000721 RID: 1825 RVA: 0x00019E1C File Offset: 0x0001801C
		public SuccessException(string message) : base(message)
		{
		}

		// Token: 0x06000722 RID: 1826 RVA: 0x00019E28 File Offset: 0x00018028
		public SuccessException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06000723 RID: 1827 RVA: 0x00019E35 File Offset: 0x00018035
		protected SuccessException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06000724 RID: 1828 RVA: 0x00019E44 File Offset: 0x00018044
		public override ResultState ResultState
		{
			get
			{
				return ResultState.Success;
			}
		}
	}
}
