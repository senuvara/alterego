﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace NUnit.Framework
{
	// Token: 0x020000FC RID: 252
	public class TestParameters
	{
		// Token: 0x17000193 RID: 403
		// (get) Token: 0x06000713 RID: 1811 RVA: 0x00019BC8 File Offset: 0x00017DC8
		public int Count
		{
			get
			{
				return this._parameters.Count;
			}
		}

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000714 RID: 1812 RVA: 0x00019BE8 File Offset: 0x00017DE8
		public ICollection<string> Names
		{
			get
			{
				return this._parameters.Keys;
			}
		}

		// Token: 0x06000715 RID: 1813 RVA: 0x00019C08 File Offset: 0x00017E08
		public bool Exists(string name)
		{
			return this._parameters.ContainsKey(name);
		}

		// Token: 0x17000195 RID: 405
		public string this[string name]
		{
			get
			{
				return this.Get(name);
			}
		}

		// Token: 0x06000717 RID: 1815 RVA: 0x00019C44 File Offset: 0x00017E44
		public string Get(string name)
		{
			return this.Exists(name) ? this._parameters[name] : null;
		}

		// Token: 0x06000718 RID: 1816 RVA: 0x00019C70 File Offset: 0x00017E70
		public string Get(string name, string defaultValue)
		{
			return this.Get(name) ?? defaultValue;
		}

		// Token: 0x06000719 RID: 1817 RVA: 0x00019C90 File Offset: 0x00017E90
		public T Get<T>(string name, T defaultValue)
		{
			string text = this.Get(name);
			return (text != null) ? ((T)((object)Convert.ChangeType(text, typeof(T), TestParameters.MODIFIED_INVARIANT_CULTURE))) : defaultValue;
		}

		// Token: 0x0600071A RID: 1818 RVA: 0x00019CCA File Offset: 0x00017ECA
		internal void Add(string name, string value)
		{
			this._parameters[name] = value;
		}

		// Token: 0x0600071B RID: 1819 RVA: 0x00019CDC File Offset: 0x00017EDC
		private static IFormatProvider CreateModifiedInvariantCulture()
		{
			CultureInfo cultureInfo = (CultureInfo)CultureInfo.InvariantCulture.Clone();
			cultureInfo.NumberFormat.CurrencyGroupSeparator = string.Empty;
			cultureInfo.NumberFormat.NumberGroupSeparator = string.Empty;
			cultureInfo.NumberFormat.PercentGroupSeparator = string.Empty;
			return cultureInfo;
		}

		// Token: 0x0600071C RID: 1820 RVA: 0x00019D3E File Offset: 0x00017F3E
		public TestParameters()
		{
		}

		// Token: 0x0600071D RID: 1821 RVA: 0x00019D32 File Offset: 0x00017F32
		// Note: this type is marked as 'beforefieldinit'.
		static TestParameters()
		{
		}

		// Token: 0x04000193 RID: 403
		private static readonly IFormatProvider MODIFIED_INVARIANT_CULTURE = TestParameters.CreateModifiedInvariantCulture();

		// Token: 0x04000194 RID: 404
		private readonly Dictionary<string, string> _parameters = new Dictionary<string, string>();
	}
}
