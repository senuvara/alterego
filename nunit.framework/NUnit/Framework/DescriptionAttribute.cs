﻿using System;

namespace NUnit.Framework
{
	// Token: 0x020000C1 RID: 193
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public sealed class DescriptionAttribute : PropertyAttribute
	{
		// Token: 0x060005E6 RID: 1510 RVA: 0x000142F4 File Offset: 0x000124F4
		public DescriptionAttribute(string description) : base("Description", description)
		{
		}
	}
}
