﻿using System;

namespace NUnit
{
	// Token: 0x02000065 RID: 101
	public class Env
	{
		// Token: 0x06000310 RID: 784 RVA: 0x0000B7F0 File Offset: 0x000099F0
		public Env()
		{
		}

		// Token: 0x06000311 RID: 785 RVA: 0x0000B7CF File Offset: 0x000099CF
		// Note: this type is marked as 'beforefieldinit'.
		static Env()
		{
		}

		// Token: 0x040000A9 RID: 169
		public static readonly string NewLine = Environment.NewLine;

		// Token: 0x040000AA RID: 170
		public static string DocumentFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

		// Token: 0x040000AB RID: 171
		public static readonly string DefaultWorkDirectory = Environment.CurrentDirectory;
	}
}
