﻿using System;

namespace NUnit
{
	// Token: 0x02000151 RID: 337
	public static class FrameworkPackageSettings
	{
		// Token: 0x040001ED RID: 493
		public const string DebugTests = "DebugTests";

		// Token: 0x040001EE RID: 494
		public const string PauseBeforeRun = "PauseBeforeRun";

		// Token: 0x040001EF RID: 495
		public const string InternalTraceLevel = "InternalTraceLevel";

		// Token: 0x040001F0 RID: 496
		public const string WorkDirectory = "WorkDirectory";

		// Token: 0x040001F1 RID: 497
		public const string DefaultTimeout = "DefaultTimeout";

		// Token: 0x040001F2 RID: 498
		public const string InternalTraceWriter = "InternalTraceWriter";

		// Token: 0x040001F3 RID: 499
		public const string LOAD = "LOAD";

		// Token: 0x040001F4 RID: 500
		public const string NumberOfTestWorkers = "NumberOfTestWorkers";

		// Token: 0x040001F5 RID: 501
		public const string RandomSeed = "RandomSeed";

		// Token: 0x040001F6 RID: 502
		public const string StopOnError = "StopOnError";

		// Token: 0x040001F7 RID: 503
		public const string SynchronousEvents = "SynchronousEvents";

		// Token: 0x040001F8 RID: 504
		public const string DefaultTestNamePattern = "DefaultTestNamePattern";

		// Token: 0x040001F9 RID: 505
		public const string TestParameters = "TestParameters";
	}
}
