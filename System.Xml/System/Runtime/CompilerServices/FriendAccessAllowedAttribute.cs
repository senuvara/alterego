﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200054B RID: 1355
	internal class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x0600342C RID: 13356 RVA: 0x0000D3B3 File Offset: 0x0000B5B3
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
