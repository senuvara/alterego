﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x02000219 RID: 537
	internal class XPathNodeList : XmlNodeList
	{
		// Token: 0x0600132F RID: 4911 RVA: 0x000717AC File Offset: 0x0006F9AC
		public XPathNodeList(XPathNodeIterator nodeIterator)
		{
			this.nodeIterator = nodeIterator;
			this.list = new List<XmlNode>();
			this.done = false;
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06001330 RID: 4912 RVA: 0x000717CD File Offset: 0x0006F9CD
		public override int Count
		{
			get
			{
				if (!this.done)
				{
					this.ReadUntil(int.MaxValue);
				}
				return this.list.Count;
			}
		}

		// Token: 0x06001331 RID: 4913 RVA: 0x000717EE File Offset: 0x0006F9EE
		private XmlNode GetNode(XPathNavigator n)
		{
			return ((IHasXmlNode)n).GetNode();
		}

		// Token: 0x06001332 RID: 4914 RVA: 0x000717FC File Offset: 0x0006F9FC
		internal int ReadUntil(int index)
		{
			int num = this.list.Count;
			while (!this.done && num <= index)
			{
				if (!this.nodeIterator.MoveNext())
				{
					this.done = true;
					break;
				}
				XmlNode node = this.GetNode(this.nodeIterator.Current);
				if (node != null)
				{
					this.list.Add(node);
					num++;
				}
			}
			return num;
		}

		// Token: 0x06001333 RID: 4915 RVA: 0x00071861 File Offset: 0x0006FA61
		public override XmlNode Item(int index)
		{
			if (this.list.Count <= index)
			{
				this.ReadUntil(index);
			}
			if (index < 0 || this.list.Count <= index)
			{
				return null;
			}
			return this.list[index];
		}

		// Token: 0x06001334 RID: 4916 RVA: 0x00071899 File Offset: 0x0006FA99
		public override IEnumerator GetEnumerator()
		{
			return new XmlNodeListEnumerator(this);
		}

		// Token: 0x06001335 RID: 4917 RVA: 0x000718A1 File Offset: 0x0006FAA1
		// Note: this type is marked as 'beforefieldinit'.
		static XPathNodeList()
		{
		}

		// Token: 0x04000D74 RID: 3444
		private List<XmlNode> list;

		// Token: 0x04000D75 RID: 3445
		private XPathNodeIterator nodeIterator;

		// Token: 0x04000D76 RID: 3446
		private bool done;

		// Token: 0x04000D77 RID: 3447
		private static readonly object[] nullparams = new object[0];
	}
}
