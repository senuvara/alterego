﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml.Schema;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x02000100 RID: 256
	internal abstract class XmlRawWriter : XmlWriter
	{
		// Token: 0x060008E6 RID: 2278 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteStartDocument()
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008E7 RID: 2279 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteStartDocument(bool standalone)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008E8 RID: 2280 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteEndDocument()
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008E9 RID: 2281 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
		}

		// Token: 0x060008EA RID: 2282 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteEndElement()
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008EB RID: 2283 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteFullEndElement()
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008EC RID: 2284 RVA: 0x000293DF File Offset: 0x000275DF
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			if (this.base64Encoder == null)
			{
				this.base64Encoder = new XmlRawWriterBase64Encoder(this);
			}
			this.base64Encoder.Encode(buffer, index, count);
		}

		// Token: 0x060008ED RID: 2285 RVA: 0x00016D08 File Offset: 0x00014F08
		public override string LookupPrefix(string ns)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x060008EE RID: 2286 RVA: 0x00016D08 File Offset: 0x00014F08
		public override WriteState WriteState
		{
			get
			{
				throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			}
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x060008EF RID: 2287 RVA: 0x00016D08 File Offset: 0x00014F08
		public override XmlSpace XmlSpace
		{
			get
			{
				throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x060008F0 RID: 2288 RVA: 0x00016D08 File Offset: 0x00014F08
		public override string XmlLang
		{
			get
			{
				throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			}
		}

		// Token: 0x060008F1 RID: 2289 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteNmToken(string name)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008F2 RID: 2290 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteName(string name)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008F3 RID: 2291 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteQualifiedName(string localName, string ns)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008F4 RID: 2292 RVA: 0x00028DF6 File Offset: 0x00026FF6
		public override void WriteCData(string text)
		{
			this.WriteString(text);
		}

		// Token: 0x060008F5 RID: 2293 RVA: 0x00029403 File Offset: 0x00027603
		public override void WriteCharEntity(char ch)
		{
			this.WriteString(new string(new char[]
			{
				ch
			}));
		}

		// Token: 0x060008F6 RID: 2294 RVA: 0x0002941A File Offset: 0x0002761A
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			this.WriteString(new string(new char[]
			{
				lowChar,
				highChar
			}));
		}

		// Token: 0x060008F7 RID: 2295 RVA: 0x00028DF6 File Offset: 0x00026FF6
		public override void WriteWhitespace(string ws)
		{
			this.WriteString(ws);
		}

		// Token: 0x060008F8 RID: 2296 RVA: 0x0001CACF File Offset: 0x0001ACCF
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.WriteString(new string(buffer, index, count));
		}

		// Token: 0x060008F9 RID: 2297 RVA: 0x0001CACF File Offset: 0x0001ACCF
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.WriteString(new string(buffer, index, count));
		}

		// Token: 0x060008FA RID: 2298 RVA: 0x00028DF6 File Offset: 0x00026FF6
		public override void WriteRaw(string data)
		{
			this.WriteString(data);
		}

		// Token: 0x060008FB RID: 2299 RVA: 0x00029435 File Offset: 0x00027635
		public override void WriteValue(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.WriteString(XmlUntypedConverter.Untyped.ToString(value, this.resolver));
		}

		// Token: 0x060008FC RID: 2300 RVA: 0x00028DF6 File Offset: 0x00026FF6
		public override void WriteValue(string value)
		{
			this.WriteString(value);
		}

		// Token: 0x060008FD RID: 2301 RVA: 0x0002945C File Offset: 0x0002765C
		public override void WriteValue(DateTimeOffset value)
		{
			this.WriteString(XmlConvert.ToString(value));
		}

		// Token: 0x060008FE RID: 2302 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteAttributes(XmlReader reader, bool defattr)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x060008FF RID: 2303 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteNode(XmlReader reader, bool defattr)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000900 RID: 2304 RVA: 0x00016D08 File Offset: 0x00014F08
		public override void WriteNode(XPathNavigator navigator, bool defattr)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000901 RID: 2305 RVA: 0x000185B8 File Offset: 0x000167B8
		// (set) Token: 0x06000902 RID: 2306 RVA: 0x0002946A File Offset: 0x0002766A
		internal virtual IXmlNamespaceResolver NamespaceResolver
		{
			get
			{
				return this.resolver;
			}
			set
			{
				this.resolver = value;
			}
		}

		// Token: 0x06000903 RID: 2307 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void WriteXmlDeclaration(XmlStandalone standalone)
		{
		}

		// Token: 0x06000904 RID: 2308 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void WriteXmlDeclaration(string xmldecl)
		{
		}

		// Token: 0x06000905 RID: 2309
		internal abstract void StartElementContent();

		// Token: 0x06000906 RID: 2310 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void OnRootElement(ConformanceLevel conformanceLevel)
		{
		}

		// Token: 0x06000907 RID: 2311
		internal abstract void WriteEndElement(string prefix, string localName, string ns);

		// Token: 0x06000908 RID: 2312 RVA: 0x00029473 File Offset: 0x00027673
		internal virtual void WriteFullEndElement(string prefix, string localName, string ns)
		{
			this.WriteEndElement(prefix, localName, ns);
		}

		// Token: 0x06000909 RID: 2313 RVA: 0x0002947E File Offset: 0x0002767E
		internal virtual void WriteQualifiedName(string prefix, string localName, string ns)
		{
			if (prefix.Length != 0)
			{
				this.WriteString(prefix);
				this.WriteString(":");
			}
			this.WriteString(localName);
		}

		// Token: 0x0600090A RID: 2314
		internal abstract void WriteNamespaceDeclaration(string prefix, string ns);

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x0600090B RID: 2315 RVA: 0x000020CD File Offset: 0x000002CD
		internal virtual bool SupportsNamespaceDeclarationInChunks
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0600090C RID: 2316 RVA: 0x00010D4A File Offset: 0x0000EF4A
		internal virtual void WriteStartNamespaceDeclaration(string prefix)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600090D RID: 2317 RVA: 0x00010D4A File Offset: 0x0000EF4A
		internal virtual void WriteEndNamespaceDeclaration()
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600090E RID: 2318 RVA: 0x000294A1 File Offset: 0x000276A1
		internal virtual void WriteEndBase64()
		{
			this.base64Encoder.Flush();
		}

		// Token: 0x0600090F RID: 2319 RVA: 0x000294AE File Offset: 0x000276AE
		internal virtual void Close(WriteState currentState)
		{
			this.Close();
		}

		// Token: 0x06000910 RID: 2320 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteStartDocumentAsync()
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000911 RID: 2321 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteStartDocumentAsync(bool standalone)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000912 RID: 2322 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteEndDocumentAsync()
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000913 RID: 2323 RVA: 0x000294B6 File Offset: 0x000276B6
		public override Task WriteDocTypeAsync(string name, string pubid, string sysid, string subset)
		{
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000914 RID: 2324 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteEndElementAsync()
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000915 RID: 2325 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteFullEndElementAsync()
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000916 RID: 2326 RVA: 0x000294BD File Offset: 0x000276BD
		public override Task WriteBase64Async(byte[] buffer, int index, int count)
		{
			if (this.base64Encoder == null)
			{
				this.base64Encoder = new XmlRawWriterBase64Encoder(this);
			}
			return this.base64Encoder.EncodeAsync(buffer, index, count);
		}

		// Token: 0x06000917 RID: 2327 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteNmTokenAsync(string name)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000918 RID: 2328 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteNameAsync(string name)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000919 RID: 2329 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteQualifiedNameAsync(string localName, string ns)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x0600091A RID: 2330 RVA: 0x000294E1 File Offset: 0x000276E1
		public override Task WriteCDataAsync(string text)
		{
			return this.WriteStringAsync(text);
		}

		// Token: 0x0600091B RID: 2331 RVA: 0x000294EA File Offset: 0x000276EA
		public override Task WriteCharEntityAsync(char ch)
		{
			return this.WriteStringAsync(new string(new char[]
			{
				ch
			}));
		}

		// Token: 0x0600091C RID: 2332 RVA: 0x00029501 File Offset: 0x00027701
		public override Task WriteSurrogateCharEntityAsync(char lowChar, char highChar)
		{
			return this.WriteStringAsync(new string(new char[]
			{
				lowChar,
				highChar
			}));
		}

		// Token: 0x0600091D RID: 2333 RVA: 0x000294E1 File Offset: 0x000276E1
		public override Task WriteWhitespaceAsync(string ws)
		{
			return this.WriteStringAsync(ws);
		}

		// Token: 0x0600091E RID: 2334 RVA: 0x0002951C File Offset: 0x0002771C
		public override Task WriteCharsAsync(char[] buffer, int index, int count)
		{
			return this.WriteStringAsync(new string(buffer, index, count));
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x0002951C File Offset: 0x0002771C
		public override Task WriteRawAsync(char[] buffer, int index, int count)
		{
			return this.WriteStringAsync(new string(buffer, index, count));
		}

		// Token: 0x06000920 RID: 2336 RVA: 0x000294E1 File Offset: 0x000276E1
		public override Task WriteRawAsync(string data)
		{
			return this.WriteStringAsync(data);
		}

		// Token: 0x06000921 RID: 2337 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteAttributesAsync(XmlReader reader, bool defattr)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000922 RID: 2338 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteNodeAsync(XmlReader reader, bool defattr)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000923 RID: 2339 RVA: 0x00016D08 File Offset: 0x00014F08
		public override Task WriteNodeAsync(XPathNavigator navigator, bool defattr)
		{
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
		}

		// Token: 0x06000924 RID: 2340 RVA: 0x000294B6 File Offset: 0x000276B6
		internal virtual Task WriteXmlDeclarationAsync(XmlStandalone standalone)
		{
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x000294B6 File Offset: 0x000276B6
		internal virtual Task WriteXmlDeclarationAsync(string xmldecl)
		{
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000926 RID: 2342 RVA: 0x0000A6CF File Offset: 0x000088CF
		internal virtual Task StartElementContentAsync()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000927 RID: 2343 RVA: 0x0000A6CF File Offset: 0x000088CF
		internal virtual Task WriteEndElementAsync(string prefix, string localName, string ns)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000928 RID: 2344 RVA: 0x0002952C File Offset: 0x0002772C
		internal virtual Task WriteFullEndElementAsync(string prefix, string localName, string ns)
		{
			return this.WriteEndElementAsync(prefix, localName, ns);
		}

		// Token: 0x06000929 RID: 2345 RVA: 0x00029538 File Offset: 0x00027738
		internal virtual async Task WriteQualifiedNameAsync(string prefix, string localName, string ns)
		{
			if (prefix.Length != 0)
			{
				await this.WriteStringAsync(prefix).ConfigureAwait(false);
				await this.WriteStringAsync(":").ConfigureAwait(false);
			}
			await this.WriteStringAsync(localName).ConfigureAwait(false);
		}

		// Token: 0x0600092A RID: 2346 RVA: 0x0000A6CF File Offset: 0x000088CF
		internal virtual Task WriteNamespaceDeclarationAsync(string prefix, string ns)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600092B RID: 2347 RVA: 0x00010D4A File Offset: 0x0000EF4A
		internal virtual Task WriteStartNamespaceDeclarationAsync(string prefix)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600092C RID: 2348 RVA: 0x00010D4A File Offset: 0x0000EF4A
		internal virtual Task WriteEndNamespaceDeclarationAsync()
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600092D RID: 2349 RVA: 0x0002958D File Offset: 0x0002778D
		internal virtual Task WriteEndBase64Async()
		{
			return this.base64Encoder.FlushAsync();
		}

		// Token: 0x0600092E RID: 2350 RVA: 0x0002959A File Offset: 0x0002779A
		protected XmlRawWriter()
		{
		}

		// Token: 0x0400055E RID: 1374
		protected XmlRawWriterBase64Encoder base64Encoder;

		// Token: 0x0400055F RID: 1375
		protected IXmlNamespaceResolver resolver;

		// Token: 0x02000101 RID: 257
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteQualifiedNameAsync>d__74 : IAsyncStateMachine
		{
			// Token: 0x0600092F RID: 2351 RVA: 0x000295A4 File Offset: 0x000277A4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlRawWriter xmlRawWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_FC;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_163;
					}
					default:
						if (prefix.Length == 0)
						{
							goto IL_103;
						}
						configuredTaskAwaiter = xmlRawWriter.WriteStringAsync(prefix).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlRawWriter.<WriteQualifiedNameAsync>d__74>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlRawWriter.WriteStringAsync(":").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlRawWriter.<WriteQualifiedNameAsync>d__74>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_FC:
					configuredTaskAwaiter.GetResult();
					IL_103:
					configuredTaskAwaiter = xmlRawWriter.WriteStringAsync(localName).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlRawWriter.<WriteQualifiedNameAsync>d__74>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_163:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000930 RID: 2352 RVA: 0x00029768 File Offset: 0x00027968
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000560 RID: 1376
			public int <>1__state;

			// Token: 0x04000561 RID: 1377
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000562 RID: 1378
			public string prefix;

			// Token: 0x04000563 RID: 1379
			public XmlRawWriter <>4__this;

			// Token: 0x04000564 RID: 1380
			public string localName;

			// Token: 0x04000565 RID: 1381
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
