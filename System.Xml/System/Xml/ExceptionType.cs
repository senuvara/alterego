﻿using System;

namespace System.Xml
{
	// Token: 0x0200028D RID: 653
	internal enum ExceptionType
	{
		// Token: 0x04000FF4 RID: 4084
		ArgumentException,
		// Token: 0x04000FF5 RID: 4085
		XmlException
	}
}
