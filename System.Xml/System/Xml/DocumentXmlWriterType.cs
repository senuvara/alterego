﻿using System;

namespace System.Xml
{
	// Token: 0x02000214 RID: 532
	internal enum DocumentXmlWriterType
	{
		// Token: 0x04000D44 RID: 3396
		InsertSiblingAfter,
		// Token: 0x04000D45 RID: 3397
		InsertSiblingBefore,
		// Token: 0x04000D46 RID: 3398
		PrependChild,
		// Token: 0x04000D47 RID: 3399
		AppendChild,
		// Token: 0x04000D48 RID: 3400
		AppendAttribute,
		// Token: 0x04000D49 RID: 3401
		ReplaceToFollowingSibling
	}
}
