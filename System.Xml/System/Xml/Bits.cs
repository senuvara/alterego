﻿using System;

namespace System.Xml
{
	// Token: 0x02000090 RID: 144
	internal static class Bits
	{
		// Token: 0x060004C2 RID: 1218 RVA: 0x00016248 File Offset: 0x00014448
		public static int Count(uint num)
		{
			num = (num & Bits.MASK_0101010101010101) + (num >> 1 & Bits.MASK_0101010101010101);
			num = (num & Bits.MASK_0011001100110011) + (num >> 2 & Bits.MASK_0011001100110011);
			num = (num & Bits.MASK_0000111100001111) + (num >> 4 & Bits.MASK_0000111100001111);
			num = (num & Bits.MASK_0000000011111111) + (num >> 8 & Bits.MASK_0000000011111111);
			num = (num & Bits.MASK_1111111111111111) + (num >> 16);
			return (int)num;
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x000162B0 File Offset: 0x000144B0
		public static bool ExactlyOne(uint num)
		{
			return num != 0U && (num & num - 1U) == 0U;
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x000162BF File Offset: 0x000144BF
		public static bool MoreThanOne(uint num)
		{
			return (num & num - 1U) > 0U;
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x000162C9 File Offset: 0x000144C9
		public static uint ClearLeast(uint num)
		{
			return num & num - 1U;
		}

		// Token: 0x060004C6 RID: 1222 RVA: 0x000162D0 File Offset: 0x000144D0
		public static int LeastPosition(uint num)
		{
			if (num == 0U)
			{
				return 0;
			}
			return Bits.Count(num ^ num - 1U);
		}

		// Token: 0x060004C7 RID: 1223 RVA: 0x000162E1 File Offset: 0x000144E1
		// Note: this type is marked as 'beforefieldinit'.
		static Bits()
		{
		}

		// Token: 0x040002F7 RID: 759
		private static readonly uint MASK_0101010101010101 = 1431655765U;

		// Token: 0x040002F8 RID: 760
		private static readonly uint MASK_0011001100110011 = 858993459U;

		// Token: 0x040002F9 RID: 761
		private static readonly uint MASK_0000111100001111 = 252645135U;

		// Token: 0x040002FA RID: 762
		private static readonly uint MASK_0000000011111111 = 16711935U;

		// Token: 0x040002FB RID: 763
		private static readonly uint MASK_1111111111111111 = 65535U;
	}
}
