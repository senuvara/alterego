﻿using System;
using System.Xml.Schema;

namespace System.Xml
{
	// Token: 0x020000CC RID: 204
	internal class XmlAsyncCheckReaderWithLineInfoNSSchema : XmlAsyncCheckReaderWithLineInfoNS, IXmlSchemaInfo
	{
		// Token: 0x060006D5 RID: 1749 RVA: 0x0001C005 File Offset: 0x0001A205
		public XmlAsyncCheckReaderWithLineInfoNSSchema(XmlReader reader) : base(reader)
		{
			this.readerAsIXmlSchemaInfo = (IXmlSchemaInfo)reader;
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060006D6 RID: 1750 RVA: 0x0001C01A File Offset: 0x0001A21A
		XmlSchemaValidity IXmlSchemaInfo.Validity
		{
			get
			{
				return this.readerAsIXmlSchemaInfo.Validity;
			}
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060006D7 RID: 1751 RVA: 0x0001C027 File Offset: 0x0001A227
		bool IXmlSchemaInfo.IsDefault
		{
			get
			{
				return this.readerAsIXmlSchemaInfo.IsDefault;
			}
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060006D8 RID: 1752 RVA: 0x0001C034 File Offset: 0x0001A234
		bool IXmlSchemaInfo.IsNil
		{
			get
			{
				return this.readerAsIXmlSchemaInfo.IsNil;
			}
		}

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x060006D9 RID: 1753 RVA: 0x0001C041 File Offset: 0x0001A241
		XmlSchemaSimpleType IXmlSchemaInfo.MemberType
		{
			get
			{
				return this.readerAsIXmlSchemaInfo.MemberType;
			}
		}

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x060006DA RID: 1754 RVA: 0x0001C04E File Offset: 0x0001A24E
		XmlSchemaType IXmlSchemaInfo.SchemaType
		{
			get
			{
				return this.readerAsIXmlSchemaInfo.SchemaType;
			}
		}

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x060006DB RID: 1755 RVA: 0x0001C05B File Offset: 0x0001A25B
		XmlSchemaElement IXmlSchemaInfo.SchemaElement
		{
			get
			{
				return this.readerAsIXmlSchemaInfo.SchemaElement;
			}
		}

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x060006DC RID: 1756 RVA: 0x0001C068 File Offset: 0x0001A268
		XmlSchemaAttribute IXmlSchemaInfo.SchemaAttribute
		{
			get
			{
				return this.readerAsIXmlSchemaInfo.SchemaAttribute;
			}
		}

		// Token: 0x040003DC RID: 988
		private readonly IXmlSchemaInfo readerAsIXmlSchemaInfo;
	}
}
