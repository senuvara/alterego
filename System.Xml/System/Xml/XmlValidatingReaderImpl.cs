﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace System.Xml
{
	// Token: 0x020001A7 RID: 423
	internal sealed class XmlValidatingReaderImpl : XmlReader, IXmlLineInfo, IXmlNamespaceResolver
	{
		// Token: 0x06000EF8 RID: 3832 RVA: 0x00058604 File Offset: 0x00056804
		internal XmlValidatingReaderImpl(XmlReader reader)
		{
			XmlAsyncCheckReader xmlAsyncCheckReader = reader as XmlAsyncCheckReader;
			if (xmlAsyncCheckReader != null)
			{
				reader = xmlAsyncCheckReader.CoreReader;
			}
			this.outerReader = this;
			this.coreReader = reader;
			this.coreReaderNSResolver = (reader as IXmlNamespaceResolver);
			this.coreReaderImpl = (reader as XmlTextReaderImpl);
			if (this.coreReaderImpl == null)
			{
				XmlTextReader xmlTextReader = reader as XmlTextReader;
				if (xmlTextReader != null)
				{
					this.coreReaderImpl = xmlTextReader.Impl;
				}
			}
			if (this.coreReaderImpl == null)
			{
				throw new ArgumentException(Res.GetString("The XmlReader passed in to construct this XmlValidatingReaderImpl must be an instance of a System.Xml.XmlTextReader."), "reader");
			}
			this.coreReaderImpl.EntityHandling = EntityHandling.ExpandEntities;
			this.coreReaderImpl.XmlValidatingReaderCompatibilityMode = true;
			this.processIdentityConstraints = true;
			this.schemaCollection = new XmlSchemaCollection(this.coreReader.NameTable);
			this.schemaCollection.XmlResolver = this.GetResolver();
			this.eventHandling = new XmlValidatingReaderImpl.ValidationEventHandling(this);
			this.coreReaderImpl.ValidationEventHandling = this.eventHandling;
			this.coreReaderImpl.OnDefaultAttributeUse = new XmlTextReaderImpl.OnDefaultAttributeUseDelegate(this.ValidateDefaultAttributeOnUse);
			this.validationType = ValidationType.Auto;
			this.SetupValidation(ValidationType.Auto);
		}

		// Token: 0x06000EF9 RID: 3833 RVA: 0x0005871C File Offset: 0x0005691C
		internal XmlValidatingReaderImpl(string xmlFragment, XmlNodeType fragType, XmlParserContext context) : this(new XmlTextReader(xmlFragment, fragType, context))
		{
			if (this.coreReader.BaseURI.Length > 0)
			{
				this.validator.BaseUri = this.GetResolver().ResolveUri(null, this.coreReader.BaseURI);
			}
			if (context != null)
			{
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.ParseDtdFromContext;
				this.parserContext = context;
			}
		}

		// Token: 0x06000EFA RID: 3834 RVA: 0x00058780 File Offset: 0x00056980
		internal XmlValidatingReaderImpl(Stream xmlFragment, XmlNodeType fragType, XmlParserContext context) : this(new XmlTextReader(xmlFragment, fragType, context))
		{
			if (this.coreReader.BaseURI.Length > 0)
			{
				this.validator.BaseUri = this.GetResolver().ResolveUri(null, this.coreReader.BaseURI);
			}
			if (context != null)
			{
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.ParseDtdFromContext;
				this.parserContext = context;
			}
		}

		// Token: 0x06000EFB RID: 3835 RVA: 0x000587E4 File Offset: 0x000569E4
		internal XmlValidatingReaderImpl(XmlReader reader, ValidationEventHandler settingsEventHandler, bool processIdentityConstraints)
		{
			XmlAsyncCheckReader xmlAsyncCheckReader = reader as XmlAsyncCheckReader;
			if (xmlAsyncCheckReader != null)
			{
				reader = xmlAsyncCheckReader.CoreReader;
			}
			this.outerReader = this;
			this.coreReader = reader;
			this.coreReaderImpl = (reader as XmlTextReaderImpl);
			if (this.coreReaderImpl == null)
			{
				XmlTextReader xmlTextReader = reader as XmlTextReader;
				if (xmlTextReader != null)
				{
					this.coreReaderImpl = xmlTextReader.Impl;
				}
			}
			if (this.coreReaderImpl == null)
			{
				throw new ArgumentException(Res.GetString("The XmlReader passed in to construct this XmlValidatingReaderImpl must be an instance of a System.Xml.XmlTextReader."), "reader");
			}
			this.coreReaderImpl.XmlValidatingReaderCompatibilityMode = true;
			this.coreReaderNSResolver = (reader as IXmlNamespaceResolver);
			this.processIdentityConstraints = processIdentityConstraints;
			this.schemaCollection = new XmlSchemaCollection(this.coreReader.NameTable);
			this.schemaCollection.XmlResolver = this.GetResolver();
			this.eventHandling = new XmlValidatingReaderImpl.ValidationEventHandling(this);
			if (settingsEventHandler != null)
			{
				this.eventHandling.AddHandler(settingsEventHandler);
			}
			this.coreReaderImpl.ValidationEventHandling = this.eventHandling;
			this.coreReaderImpl.OnDefaultAttributeUse = new XmlTextReaderImpl.OnDefaultAttributeUseDelegate(this.ValidateDefaultAttributeOnUse);
			this.validationType = ValidationType.DTD;
			this.SetupValidation(ValidationType.DTD);
		}

		// Token: 0x17000281 RID: 641
		// (get) Token: 0x06000EFC RID: 3836 RVA: 0x000588FC File Offset: 0x00056AFC
		public override XmlReaderSettings Settings
		{
			get
			{
				XmlReaderSettings xmlReaderSettings;
				if (this.coreReaderImpl.V1Compat)
				{
					xmlReaderSettings = null;
				}
				else
				{
					xmlReaderSettings = this.coreReader.Settings;
				}
				if (xmlReaderSettings != null)
				{
					xmlReaderSettings = xmlReaderSettings.Clone();
				}
				else
				{
					xmlReaderSettings = new XmlReaderSettings();
				}
				xmlReaderSettings.ValidationType = ValidationType.DTD;
				if (!this.processIdentityConstraints)
				{
					xmlReaderSettings.ValidationFlags &= ~XmlSchemaValidationFlags.ProcessIdentityConstraints;
				}
				xmlReaderSettings.ReadOnly = true;
				return xmlReaderSettings;
			}
		}

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x06000EFD RID: 3837 RVA: 0x0005895E File Offset: 0x00056B5E
		public override XmlNodeType NodeType
		{
			get
			{
				return this.coreReader.NodeType;
			}
		}

		// Token: 0x17000283 RID: 643
		// (get) Token: 0x06000EFE RID: 3838 RVA: 0x0005896B File Offset: 0x00056B6B
		public override string Name
		{
			get
			{
				return this.coreReader.Name;
			}
		}

		// Token: 0x17000284 RID: 644
		// (get) Token: 0x06000EFF RID: 3839 RVA: 0x00058978 File Offset: 0x00056B78
		public override string LocalName
		{
			get
			{
				return this.coreReader.LocalName;
			}
		}

		// Token: 0x17000285 RID: 645
		// (get) Token: 0x06000F00 RID: 3840 RVA: 0x00058985 File Offset: 0x00056B85
		public override string NamespaceURI
		{
			get
			{
				return this.coreReader.NamespaceURI;
			}
		}

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x06000F01 RID: 3841 RVA: 0x00058992 File Offset: 0x00056B92
		public override string Prefix
		{
			get
			{
				return this.coreReader.Prefix;
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x06000F02 RID: 3842 RVA: 0x0005899F File Offset: 0x00056B9F
		public override bool HasValue
		{
			get
			{
				return this.coreReader.HasValue;
			}
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x06000F03 RID: 3843 RVA: 0x000589AC File Offset: 0x00056BAC
		public override string Value
		{
			get
			{
				return this.coreReader.Value;
			}
		}

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x06000F04 RID: 3844 RVA: 0x000589B9 File Offset: 0x00056BB9
		public override int Depth
		{
			get
			{
				return this.coreReader.Depth;
			}
		}

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x06000F05 RID: 3845 RVA: 0x000589C6 File Offset: 0x00056BC6
		public override string BaseURI
		{
			get
			{
				return this.coreReader.BaseURI;
			}
		}

		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06000F06 RID: 3846 RVA: 0x000589D3 File Offset: 0x00056BD3
		public override bool IsEmptyElement
		{
			get
			{
				return this.coreReader.IsEmptyElement;
			}
		}

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x06000F07 RID: 3847 RVA: 0x000589E0 File Offset: 0x00056BE0
		public override bool IsDefault
		{
			get
			{
				return this.coreReader.IsDefault;
			}
		}

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06000F08 RID: 3848 RVA: 0x000589ED File Offset: 0x00056BED
		public override char QuoteChar
		{
			get
			{
				return this.coreReader.QuoteChar;
			}
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x06000F09 RID: 3849 RVA: 0x000589FA File Offset: 0x00056BFA
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.coreReader.XmlSpace;
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06000F0A RID: 3850 RVA: 0x00058A07 File Offset: 0x00056C07
		public override string XmlLang
		{
			get
			{
				return this.coreReader.XmlLang;
			}
		}

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000F0B RID: 3851 RVA: 0x00058A14 File Offset: 0x00056C14
		public override ReadState ReadState
		{
			get
			{
				if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.Init)
				{
					return this.coreReader.ReadState;
				}
				return ReadState.Initial;
			}
		}

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06000F0C RID: 3852 RVA: 0x00058A2C File Offset: 0x00056C2C
		public override bool EOF
		{
			get
			{
				return this.coreReader.EOF;
			}
		}

		// Token: 0x17000292 RID: 658
		// (get) Token: 0x06000F0D RID: 3853 RVA: 0x00058A39 File Offset: 0x00056C39
		public override XmlNameTable NameTable
		{
			get
			{
				return this.coreReader.NameTable;
			}
		}

		// Token: 0x17000293 RID: 659
		// (get) Token: 0x06000F0E RID: 3854 RVA: 0x00058A46 File Offset: 0x00056C46
		internal Encoding Encoding
		{
			get
			{
				return this.coreReaderImpl.Encoding;
			}
		}

		// Token: 0x17000294 RID: 660
		// (get) Token: 0x06000F0F RID: 3855 RVA: 0x00058A53 File Offset: 0x00056C53
		public override int AttributeCount
		{
			get
			{
				return this.coreReader.AttributeCount;
			}
		}

		// Token: 0x06000F10 RID: 3856 RVA: 0x00058A60 File Offset: 0x00056C60
		public override string GetAttribute(string name)
		{
			return this.coreReader.GetAttribute(name);
		}

		// Token: 0x06000F11 RID: 3857 RVA: 0x00058A6E File Offset: 0x00056C6E
		public override string GetAttribute(string localName, string namespaceURI)
		{
			return this.coreReader.GetAttribute(localName, namespaceURI);
		}

		// Token: 0x06000F12 RID: 3858 RVA: 0x00058A7D File Offset: 0x00056C7D
		public override string GetAttribute(int i)
		{
			return this.coreReader.GetAttribute(i);
		}

		// Token: 0x06000F13 RID: 3859 RVA: 0x00058A8B File Offset: 0x00056C8B
		public override bool MoveToAttribute(string name)
		{
			if (!this.coreReader.MoveToAttribute(name))
			{
				return false;
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			return true;
		}

		// Token: 0x06000F14 RID: 3860 RVA: 0x00058AA5 File Offset: 0x00056CA5
		public override bool MoveToAttribute(string localName, string namespaceURI)
		{
			if (!this.coreReader.MoveToAttribute(localName, namespaceURI))
			{
				return false;
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			return true;
		}

		// Token: 0x06000F15 RID: 3861 RVA: 0x00058AC0 File Offset: 0x00056CC0
		public override void MoveToAttribute(int i)
		{
			this.coreReader.MoveToAttribute(i);
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x00058AD5 File Offset: 0x00056CD5
		public override bool MoveToFirstAttribute()
		{
			if (!this.coreReader.MoveToFirstAttribute())
			{
				return false;
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			return true;
		}

		// Token: 0x06000F17 RID: 3863 RVA: 0x00058AEE File Offset: 0x00056CEE
		public override bool MoveToNextAttribute()
		{
			if (!this.coreReader.MoveToNextAttribute())
			{
				return false;
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			return true;
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x00058B07 File Offset: 0x00056D07
		public override bool MoveToElement()
		{
			if (!this.coreReader.MoveToElement())
			{
				return false;
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			return true;
		}

		// Token: 0x06000F19 RID: 3865 RVA: 0x00058B20 File Offset: 0x00056D20
		public override bool Read()
		{
			switch (this.parsingFunction)
			{
			case XmlValidatingReaderImpl.ParsingFunction.Read:
				break;
			case XmlValidatingReaderImpl.ParsingFunction.Init:
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				if (this.coreReader.ReadState == ReadState.Interactive)
				{
					this.ProcessCoreReaderEvent();
					return true;
				}
				break;
			case XmlValidatingReaderImpl.ParsingFunction.ParseDtdFromContext:
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				this.ParseDtdFromParserContext();
				break;
			case XmlValidatingReaderImpl.ParsingFunction.ResolveEntityInternally:
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				this.ResolveEntityInternally();
				break;
			case XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent:
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				this.readBinaryHelper.Finish();
				break;
			case XmlValidatingReaderImpl.ParsingFunction.ReaderClosed:
			case XmlValidatingReaderImpl.ParsingFunction.Error:
				return false;
			default:
				return false;
			}
			if (this.coreReader.Read())
			{
				this.ProcessCoreReaderEvent();
				return true;
			}
			this.validator.CompleteValidation();
			return false;
		}

		// Token: 0x06000F1A RID: 3866 RVA: 0x00058BCC File Offset: 0x00056DCC
		public override void Close()
		{
			this.coreReader.Close();
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.ReaderClosed;
		}

		// Token: 0x06000F1B RID: 3867 RVA: 0x00058BE0 File Offset: 0x00056DE0
		public override string LookupNamespace(string prefix)
		{
			return this.coreReaderImpl.LookupNamespace(prefix);
		}

		// Token: 0x06000F1C RID: 3868 RVA: 0x00058BEE File Offset: 0x00056DEE
		public override bool ReadAttributeValue()
		{
			if (this.parsingFunction == XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
			{
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				this.readBinaryHelper.Finish();
			}
			if (!this.coreReader.ReadAttributeValue())
			{
				return false;
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			return true;
		}

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000F1D RID: 3869 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool CanReadBinaryContent
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000F1E RID: 3870 RVA: 0x00058C24 File Offset: 0x00056E24
		public override int ReadContentAsBase64(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
			{
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this.outerReader);
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			int result = this.readBinaryHelper.ReadContentAsBase64(buffer, index, count);
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
			return result;
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x00058C78 File Offset: 0x00056E78
		public override int ReadContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
			{
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this.outerReader);
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			int result = this.readBinaryHelper.ReadContentAsBinHex(buffer, index, count);
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
			return result;
		}

		// Token: 0x06000F20 RID: 3872 RVA: 0x00058CCC File Offset: 0x00056ECC
		public override int ReadElementContentAsBase64(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
			{
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this.outerReader);
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			int result = this.readBinaryHelper.ReadElementContentAsBase64(buffer, index, count);
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
			return result;
		}

		// Token: 0x06000F21 RID: 3873 RVA: 0x00058D20 File Offset: 0x00056F20
		public override int ReadElementContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
			{
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this.outerReader);
			}
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			int result = this.readBinaryHelper.ReadElementContentAsBinHex(buffer, index, count);
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
			return result;
		}

		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000F22 RID: 3874 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool CanResolveEntity
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000F23 RID: 3875 RVA: 0x00058D74 File Offset: 0x00056F74
		public override void ResolveEntity()
		{
			if (this.parsingFunction == XmlValidatingReaderImpl.ParsingFunction.ResolveEntityInternally)
			{
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
			}
			this.coreReader.ResolveEntity();
		}

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000F24 RID: 3876 RVA: 0x00058D91 File Offset: 0x00056F91
		// (set) Token: 0x06000F25 RID: 3877 RVA: 0x00058D99 File Offset: 0x00056F99
		internal XmlReader OuterReader
		{
			get
			{
				return this.outerReader;
			}
			set
			{
				this.outerReader = value;
			}
		}

		// Token: 0x06000F26 RID: 3878 RVA: 0x00058DA2 File Offset: 0x00056FA2
		internal void MoveOffEntityReference()
		{
			if (this.outerReader.NodeType == XmlNodeType.EntityReference && this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.ResolveEntityInternally && !this.outerReader.Read())
			{
				throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			}
		}

		// Token: 0x06000F27 RID: 3879 RVA: 0x00058DD8 File Offset: 0x00056FD8
		public override string ReadString()
		{
			this.MoveOffEntityReference();
			return base.ReadString();
		}

		// Token: 0x06000F28 RID: 3880 RVA: 0x000033DE File Offset: 0x000015DE
		public bool HasLineInfo()
		{
			return true;
		}

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000F29 RID: 3881 RVA: 0x00058DE6 File Offset: 0x00056FE6
		public int LineNumber
		{
			get
			{
				return ((IXmlLineInfo)this.coreReader).LineNumber;
			}
		}

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06000F2A RID: 3882 RVA: 0x00058DF8 File Offset: 0x00056FF8
		public int LinePosition
		{
			get
			{
				return ((IXmlLineInfo)this.coreReader).LinePosition;
			}
		}

		// Token: 0x06000F2B RID: 3883 RVA: 0x00058E0A File Offset: 0x0005700A
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return this.GetNamespacesInScope(scope);
		}

		// Token: 0x06000F2C RID: 3884 RVA: 0x00034013 File Offset: 0x00032213
		string IXmlNamespaceResolver.LookupNamespace(string prefix)
		{
			return this.LookupNamespace(prefix);
		}

		// Token: 0x06000F2D RID: 3885 RVA: 0x00058E13 File Offset: 0x00057013
		string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
		{
			return this.LookupPrefix(namespaceName);
		}

		// Token: 0x06000F2E RID: 3886 RVA: 0x00058E1C File Offset: 0x0005701C
		internal IDictionary<string, string> GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return this.coreReaderNSResolver.GetNamespacesInScope(scope);
		}

		// Token: 0x06000F2F RID: 3887 RVA: 0x00058E2A File Offset: 0x0005702A
		internal string LookupPrefix(string namespaceName)
		{
			return this.coreReaderNSResolver.LookupPrefix(namespaceName);
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x06000F30 RID: 3888 RVA: 0x00058E38 File Offset: 0x00057038
		// (remove) Token: 0x06000F31 RID: 3889 RVA: 0x00058E46 File Offset: 0x00057046
		internal event ValidationEventHandler ValidationEventHandler
		{
			add
			{
				this.eventHandling.AddHandler(value);
			}
			remove
			{
				this.eventHandling.RemoveHandler(value);
			}
		}

		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06000F32 RID: 3890 RVA: 0x00058E54 File Offset: 0x00057054
		internal object SchemaType
		{
			get
			{
				if (this.validationType == ValidationType.None)
				{
					return null;
				}
				XmlSchemaType xmlSchemaType = this.coreReaderImpl.InternalSchemaType as XmlSchemaType;
				if (xmlSchemaType != null && xmlSchemaType.QualifiedName.Namespace == "http://www.w3.org/2001/XMLSchema")
				{
					return xmlSchemaType.Datatype;
				}
				return this.coreReaderImpl.InternalSchemaType;
			}
		}

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x06000F33 RID: 3891 RVA: 0x00058EA8 File Offset: 0x000570A8
		internal XmlReader Reader
		{
			get
			{
				return this.coreReader;
			}
		}

		// Token: 0x1700029C RID: 668
		// (get) Token: 0x06000F34 RID: 3892 RVA: 0x00058EB0 File Offset: 0x000570B0
		internal XmlTextReaderImpl ReaderImpl
		{
			get
			{
				return this.coreReaderImpl;
			}
		}

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x06000F35 RID: 3893 RVA: 0x00058EB8 File Offset: 0x000570B8
		// (set) Token: 0x06000F36 RID: 3894 RVA: 0x00058EC0 File Offset: 0x000570C0
		internal ValidationType ValidationType
		{
			get
			{
				return this.validationType;
			}
			set
			{
				if (this.ReadState != ReadState.Initial)
				{
					throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
				}
				this.validationType = value;
				this.SetupValidation(value);
			}
		}

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x06000F37 RID: 3895 RVA: 0x00058EE8 File Offset: 0x000570E8
		internal XmlSchemaCollection Schemas
		{
			get
			{
				return this.schemaCollection;
			}
		}

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06000F38 RID: 3896 RVA: 0x00058EF0 File Offset: 0x000570F0
		// (set) Token: 0x06000F39 RID: 3897 RVA: 0x00058EFD File Offset: 0x000570FD
		internal EntityHandling EntityHandling
		{
			get
			{
				return this.coreReaderImpl.EntityHandling;
			}
			set
			{
				this.coreReaderImpl.EntityHandling = value;
			}
		}

		// Token: 0x170002A0 RID: 672
		// (set) Token: 0x06000F3A RID: 3898 RVA: 0x00058F0B File Offset: 0x0005710B
		internal XmlResolver XmlResolver
		{
			set
			{
				this.coreReaderImpl.XmlResolver = value;
				this.validator.XmlResolver = value;
				this.schemaCollection.XmlResolver = value;
			}
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000F3B RID: 3899 RVA: 0x00058F31 File Offset: 0x00057131
		// (set) Token: 0x06000F3C RID: 3900 RVA: 0x00058F3E File Offset: 0x0005713E
		internal bool Namespaces
		{
			get
			{
				return this.coreReaderImpl.Namespaces;
			}
			set
			{
				this.coreReaderImpl.Namespaces = value;
			}
		}

		// Token: 0x06000F3D RID: 3901 RVA: 0x00058F4C File Offset: 0x0005714C
		public object ReadTypedValue()
		{
			if (this.validationType == ValidationType.None)
			{
				return null;
			}
			XmlNodeType nodeType = this.outerReader.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType == XmlNodeType.Attribute)
				{
					return this.coreReaderImpl.InternalTypedValue;
				}
				if (nodeType == XmlNodeType.EndElement)
				{
					return null;
				}
				if (this.coreReaderImpl.V1Compat)
				{
					return null;
				}
				return this.Value;
			}
			else
			{
				if (this.SchemaType == null)
				{
					return null;
				}
				if (((this.SchemaType is XmlSchemaDatatype) ? ((XmlSchemaDatatype)this.SchemaType) : ((XmlSchemaType)this.SchemaType).Datatype) != null)
				{
					if (!this.outerReader.IsEmptyElement)
					{
						while (this.outerReader.Read())
						{
							XmlNodeType nodeType2 = this.outerReader.NodeType;
							if (nodeType2 != XmlNodeType.CDATA && nodeType2 != XmlNodeType.Text && nodeType2 != XmlNodeType.Whitespace && nodeType2 != XmlNodeType.SignificantWhitespace && nodeType2 != XmlNodeType.Comment && nodeType2 != XmlNodeType.ProcessingInstruction)
							{
								if (this.outerReader.NodeType != XmlNodeType.EndElement)
								{
									throw new XmlException("'{0}' is an invalid XmlNodeType.", this.outerReader.NodeType.ToString());
								}
								goto IL_F3;
							}
						}
						throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
					}
					IL_F3:
					return this.coreReaderImpl.InternalTypedValue;
				}
				return null;
			}
		}

		// Token: 0x06000F3E RID: 3902 RVA: 0x00059074 File Offset: 0x00057274
		private void ParseDtdFromParserContext()
		{
			if (this.parserContext.DocTypeName == null || this.parserContext.DocTypeName.Length == 0)
			{
				return;
			}
			IDtdParser dtdParser = DtdParser.Create();
			XmlTextReaderImpl.DtdParserProxy adapter = new XmlTextReaderImpl.DtdParserProxy(this.coreReaderImpl);
			IDtdInfo dtdInfo = dtdParser.ParseFreeFloatingDtd(this.parserContext.BaseURI, this.parserContext.DocTypeName, this.parserContext.PublicId, this.parserContext.SystemId, this.parserContext.InternalSubset, adapter);
			this.coreReaderImpl.SetDtdInfo(dtdInfo);
			this.ValidateDtd();
		}

		// Token: 0x06000F3F RID: 3903 RVA: 0x00059104 File Offset: 0x00057304
		private void ValidateDtd()
		{
			IDtdInfo dtdInfo = this.coreReaderImpl.DtdInfo;
			if (dtdInfo != null)
			{
				switch (this.validationType)
				{
				case ValidationType.None:
				case ValidationType.DTD:
					break;
				case ValidationType.Auto:
					this.SetupValidation(ValidationType.DTD);
					break;
				default:
					return;
				}
				this.validator.DtdInfo = dtdInfo;
			}
		}

		// Token: 0x06000F40 RID: 3904 RVA: 0x00059150 File Offset: 0x00057350
		private void ResolveEntityInternally()
		{
			int depth = this.coreReader.Depth;
			this.outerReader.ResolveEntity();
			while (this.outerReader.Read() && this.coreReader.Depth > depth)
			{
			}
		}

		// Token: 0x06000F41 RID: 3905 RVA: 0x00059190 File Offset: 0x00057390
		private void SetupValidation(ValidationType valType)
		{
			this.validator = BaseValidator.CreateInstance(valType, this, this.schemaCollection, this.eventHandling, this.processIdentityConstraints);
			XmlResolver resolver = this.GetResolver();
			this.validator.XmlResolver = resolver;
			if (this.outerReader.BaseURI.Length > 0)
			{
				this.validator.BaseUri = ((resolver == null) ? new Uri(this.outerReader.BaseURI, UriKind.RelativeOrAbsolute) : resolver.ResolveUri(null, this.outerReader.BaseURI));
			}
			this.coreReaderImpl.ValidationEventHandling = ((this.validationType == ValidationType.None) ? null : this.eventHandling);
		}

		// Token: 0x06000F42 RID: 3906 RVA: 0x00059234 File Offset: 0x00057434
		private XmlResolver GetResolver()
		{
			XmlResolver resolver = this.coreReaderImpl.GetResolver();
			if (resolver == null && !this.coreReaderImpl.IsResolverSet && !XmlReaderSettings.EnableLegacyXmlSettings())
			{
				if (XmlValidatingReaderImpl.s_tempResolver == null)
				{
					XmlValidatingReaderImpl.s_tempResolver = new XmlUrlResolver();
				}
				return XmlValidatingReaderImpl.s_tempResolver;
			}
			return resolver;
		}

		// Token: 0x06000F43 RID: 3907 RVA: 0x0005927C File Offset: 0x0005747C
		private void ProcessCoreReaderEvent()
		{
			XmlNodeType nodeType = this.coreReader.NodeType;
			if (nodeType != XmlNodeType.EntityReference)
			{
				if (nodeType == XmlNodeType.DocumentType)
				{
					this.ValidateDtd();
					return;
				}
				if (nodeType == XmlNodeType.Whitespace && (this.coreReader.Depth > 0 || this.coreReaderImpl.FragmentType != XmlNodeType.Document) && this.validator.PreserveWhitespace)
				{
					this.coreReaderImpl.ChangeCurrentNodeType(XmlNodeType.SignificantWhitespace);
				}
			}
			else
			{
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.ResolveEntityInternally;
			}
			this.coreReaderImpl.InternalSchemaType = null;
			this.coreReaderImpl.InternalTypedValue = null;
			this.validator.Validate();
		}

		// Token: 0x06000F44 RID: 3908 RVA: 0x0005930D File Offset: 0x0005750D
		internal void Close(bool closeStream)
		{
			this.coreReaderImpl.Close(closeStream);
			this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.ReaderClosed;
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000F45 RID: 3909 RVA: 0x00059322 File Offset: 0x00057522
		// (set) Token: 0x06000F46 RID: 3910 RVA: 0x0005932A File Offset: 0x0005752A
		internal BaseValidator Validator
		{
			get
			{
				return this.validator;
			}
			set
			{
				this.validator = value;
			}
		}

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000F47 RID: 3911 RVA: 0x00059333 File Offset: 0x00057533
		internal override XmlNamespaceManager NamespaceManager
		{
			get
			{
				return this.coreReaderImpl.NamespaceManager;
			}
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000F48 RID: 3912 RVA: 0x00059340 File Offset: 0x00057540
		internal bool StandAlone
		{
			get
			{
				return this.coreReaderImpl.StandAlone;
			}
		}

		// Token: 0x170002A5 RID: 677
		// (set) Token: 0x06000F49 RID: 3913 RVA: 0x0005934D File Offset: 0x0005754D
		internal object SchemaTypeObject
		{
			set
			{
				this.coreReaderImpl.InternalSchemaType = value;
			}
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000F4A RID: 3914 RVA: 0x0005935B File Offset: 0x0005755B
		// (set) Token: 0x06000F4B RID: 3915 RVA: 0x00059368 File Offset: 0x00057568
		internal object TypedValueObject
		{
			get
			{
				return this.coreReaderImpl.InternalTypedValue;
			}
			set
			{
				this.coreReaderImpl.InternalTypedValue = value;
			}
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000F4C RID: 3916 RVA: 0x00059376 File Offset: 0x00057576
		internal bool Normalization
		{
			get
			{
				return this.coreReaderImpl.Normalization;
			}
		}

		// Token: 0x06000F4D RID: 3917 RVA: 0x00059383 File Offset: 0x00057583
		internal bool AddDefaultAttribute(SchemaAttDef attdef)
		{
			return this.coreReaderImpl.AddDefaultAttributeNonDtd(attdef);
		}

		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x06000F4E RID: 3918 RVA: 0x00059391 File Offset: 0x00057591
		internal override IDtdInfo DtdInfo
		{
			get
			{
				return this.coreReaderImpl.DtdInfo;
			}
		}

		// Token: 0x06000F4F RID: 3919 RVA: 0x000593A0 File Offset: 0x000575A0
		internal void ValidateDefaultAttributeOnUse(IDtdDefaultAttributeInfo defaultAttribute, XmlTextReaderImpl coreReader)
		{
			SchemaAttDef schemaAttDef = defaultAttribute as SchemaAttDef;
			if (schemaAttDef == null)
			{
				return;
			}
			if (!schemaAttDef.DefaultValueChecked)
			{
				SchemaInfo schemaInfo = coreReader.DtdInfo as SchemaInfo;
				if (schemaInfo == null)
				{
					return;
				}
				DtdValidator.CheckDefaultValue(schemaAttDef, schemaInfo, this.eventHandling, coreReader.BaseURI);
			}
		}

		// Token: 0x06000F50 RID: 3920 RVA: 0x000593E3 File Offset: 0x000575E3
		public override Task<string> GetValueAsync()
		{
			return this.coreReader.GetValueAsync();
		}

		// Token: 0x06000F51 RID: 3921 RVA: 0x000593F0 File Offset: 0x000575F0
		public override async Task<bool> ReadAsync()
		{
			switch (this.parsingFunction)
			{
			case XmlValidatingReaderImpl.ParsingFunction.Read:
				break;
			case XmlValidatingReaderImpl.ParsingFunction.Init:
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				if (this.coreReader.ReadState == ReadState.Interactive)
				{
					this.ProcessCoreReaderEvent();
					return true;
				}
				break;
			case XmlValidatingReaderImpl.ParsingFunction.ParseDtdFromContext:
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				await this.ParseDtdFromParserContextAsync().ConfigureAwait(false);
				break;
			case XmlValidatingReaderImpl.ParsingFunction.ResolveEntityInternally:
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				await this.ResolveEntityInternallyAsync().ConfigureAwait(false);
				break;
			case XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent:
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				await this.readBinaryHelper.FinishAsync().ConfigureAwait(false);
				break;
			case XmlValidatingReaderImpl.ParsingFunction.ReaderClosed:
			case XmlValidatingReaderImpl.ParsingFunction.Error:
				return false;
			default:
				return false;
			}
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			bool result;
			if (configuredTaskAwaiter.GetResult())
			{
				this.ProcessCoreReaderEvent();
				result = true;
			}
			else
			{
				this.validator.CompleteValidation();
				result = false;
			}
			return result;
		}

		// Token: 0x06000F52 RID: 3922 RVA: 0x00059438 File Offset: 0x00057638
		public override async Task<int> ReadContentAsBase64Async(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
				{
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this.outerReader);
				}
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				int num = await this.readBinaryHelper.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
				result = num;
			}
			return result;
		}

		// Token: 0x06000F53 RID: 3923 RVA: 0x00059498 File Offset: 0x00057698
		public override async Task<int> ReadContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
				{
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this.outerReader);
				}
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				int num = await this.readBinaryHelper.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
				result = num;
			}
			return result;
		}

		// Token: 0x06000F54 RID: 3924 RVA: 0x000594F8 File Offset: 0x000576F8
		public override async Task<int> ReadElementContentAsBase64Async(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
				{
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this.outerReader);
				}
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				int num = await this.readBinaryHelper.ReadElementContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
				result = num;
			}
			return result;
		}

		// Token: 0x06000F55 RID: 3925 RVA: 0x00059558 File Offset: 0x00057758
		public override async Task<int> ReadElementContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
				{
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this.outerReader);
				}
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
				int num = await this.readBinaryHelper.ReadElementContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
				this.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
				result = num;
			}
			return result;
		}

		// Token: 0x06000F56 RID: 3926 RVA: 0x000595B8 File Offset: 0x000577B8
		internal async Task MoveOffEntityReferenceAsync()
		{
			if (this.outerReader.NodeType == XmlNodeType.EntityReference && this.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.ResolveEntityInternally)
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
				}
			}
		}

		// Token: 0x06000F57 RID: 3927 RVA: 0x00059600 File Offset: 0x00057800
		public async Task<object> ReadTypedValueAsync()
		{
			object result;
			if (this.validationType == ValidationType.None)
			{
				result = null;
			}
			else
			{
				XmlNodeType nodeType = this.outerReader.NodeType;
				if (nodeType != XmlNodeType.Element)
				{
					if (nodeType != XmlNodeType.Attribute)
					{
						if (nodeType != XmlNodeType.EndElement)
						{
							if (this.coreReaderImpl.V1Compat)
							{
								result = null;
							}
							else
							{
								result = await this.GetValueAsync().ConfigureAwait(false);
							}
						}
						else
						{
							result = null;
						}
					}
					else
					{
						result = this.coreReaderImpl.InternalTypedValue;
					}
				}
				else if (this.SchemaType == null)
				{
					result = null;
				}
				else if (((this.SchemaType is XmlSchemaDatatype) ? ((XmlSchemaDatatype)this.SchemaType) : ((XmlSchemaType)this.SchemaType).Datatype) != null)
				{
					if (!this.outerReader.IsEmptyElement)
					{
						for (;;)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								await configuredTaskAwaiter;
								ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							}
							if (!configuredTaskAwaiter.GetResult())
							{
								break;
							}
							XmlNodeType nodeType2 = this.outerReader.NodeType;
							if (nodeType2 != XmlNodeType.CDATA && nodeType2 != XmlNodeType.Text && nodeType2 != XmlNodeType.Whitespace && nodeType2 != XmlNodeType.SignificantWhitespace && nodeType2 != XmlNodeType.Comment && nodeType2 != XmlNodeType.ProcessingInstruction)
							{
								goto Block_15;
							}
						}
						throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
						Block_15:
						if (this.outerReader.NodeType != XmlNodeType.EndElement)
						{
							throw new XmlException("'{0}' is an invalid XmlNodeType.", this.outerReader.NodeType.ToString());
						}
					}
					result = this.coreReaderImpl.InternalTypedValue;
				}
				else
				{
					result = null;
				}
			}
			return result;
		}

		// Token: 0x06000F58 RID: 3928 RVA: 0x00059648 File Offset: 0x00057848
		private async Task ParseDtdFromParserContextAsync()
		{
			if (this.parserContext.DocTypeName != null && this.parserContext.DocTypeName.Length != 0)
			{
				IDtdParser dtdParser = DtdParser.Create();
				XmlTextReaderImpl.DtdParserProxy adapter = new XmlTextReaderImpl.DtdParserProxy(this.coreReaderImpl);
				IDtdInfo dtdInfo = await dtdParser.ParseFreeFloatingDtdAsync(this.parserContext.BaseURI, this.parserContext.DocTypeName, this.parserContext.PublicId, this.parserContext.SystemId, this.parserContext.InternalSubset, adapter).ConfigureAwait(false);
				this.coreReaderImpl.SetDtdInfo(dtdInfo);
				this.ValidateDtd();
			}
		}

		// Token: 0x06000F59 RID: 3929 RVA: 0x00059690 File Offset: 0x00057890
		private async Task ResolveEntityInternallyAsync()
		{
			int initialDepth = this.coreReader.Depth;
			this.outerReader.ResolveEntity();
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
			do
			{
				configuredTaskAwaiter = this.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
			}
			while (configuredTaskAwaiter.GetResult() && this.coreReader.Depth > initialDepth);
		}

		// Token: 0x04000A60 RID: 2656
		private XmlReader coreReader;

		// Token: 0x04000A61 RID: 2657
		private XmlTextReaderImpl coreReaderImpl;

		// Token: 0x04000A62 RID: 2658
		private IXmlNamespaceResolver coreReaderNSResolver;

		// Token: 0x04000A63 RID: 2659
		private ValidationType validationType;

		// Token: 0x04000A64 RID: 2660
		private BaseValidator validator;

		// Token: 0x04000A65 RID: 2661
		private XmlSchemaCollection schemaCollection;

		// Token: 0x04000A66 RID: 2662
		private bool processIdentityConstraints;

		// Token: 0x04000A67 RID: 2663
		private XmlValidatingReaderImpl.ParsingFunction parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Init;

		// Token: 0x04000A68 RID: 2664
		private XmlValidatingReaderImpl.ValidationEventHandling eventHandling;

		// Token: 0x04000A69 RID: 2665
		private XmlParserContext parserContext;

		// Token: 0x04000A6A RID: 2666
		private ReadContentAsBinaryHelper readBinaryHelper;

		// Token: 0x04000A6B RID: 2667
		private XmlReader outerReader;

		// Token: 0x04000A6C RID: 2668
		private static XmlResolver s_tempResolver;

		// Token: 0x020001A8 RID: 424
		private enum ParsingFunction
		{
			// Token: 0x04000A6E RID: 2670
			Read,
			// Token: 0x04000A6F RID: 2671
			Init,
			// Token: 0x04000A70 RID: 2672
			ParseDtdFromContext,
			// Token: 0x04000A71 RID: 2673
			ResolveEntityInternally,
			// Token: 0x04000A72 RID: 2674
			InReadBinaryContent,
			// Token: 0x04000A73 RID: 2675
			ReaderClosed,
			// Token: 0x04000A74 RID: 2676
			Error,
			// Token: 0x04000A75 RID: 2677
			None
		}

		// Token: 0x020001A9 RID: 425
		internal class ValidationEventHandling : IValidationEventHandling
		{
			// Token: 0x06000F5A RID: 3930 RVA: 0x000596D5 File Offset: 0x000578D5
			internal ValidationEventHandling(XmlValidatingReaderImpl reader)
			{
				this.reader = reader;
			}

			// Token: 0x170002A9 RID: 681
			// (get) Token: 0x06000F5B RID: 3931 RVA: 0x000596E4 File Offset: 0x000578E4
			object IValidationEventHandling.EventHandler
			{
				get
				{
					return this.eventHandler;
				}
			}

			// Token: 0x06000F5C RID: 3932 RVA: 0x000596EC File Offset: 0x000578EC
			void IValidationEventHandling.SendEvent(Exception exception, XmlSeverityType severity)
			{
				if (this.eventHandler != null)
				{
					this.eventHandler(this.reader, new ValidationEventArgs((XmlSchemaException)exception, severity));
					return;
				}
				if (this.reader.ValidationType != ValidationType.None && severity == XmlSeverityType.Error)
				{
					throw exception;
				}
			}

			// Token: 0x06000F5D RID: 3933 RVA: 0x00059726 File Offset: 0x00057926
			internal void AddHandler(ValidationEventHandler handler)
			{
				this.eventHandler = (ValidationEventHandler)Delegate.Combine(this.eventHandler, handler);
			}

			// Token: 0x06000F5E RID: 3934 RVA: 0x0005973F File Offset: 0x0005793F
			internal void RemoveHandler(ValidationEventHandler handler)
			{
				this.eventHandler = (ValidationEventHandler)Delegate.Remove(this.eventHandler, handler);
			}

			// Token: 0x04000A76 RID: 2678
			private XmlValidatingReaderImpl reader;

			// Token: 0x04000A77 RID: 2679
			private ValidationEventHandler eventHandler;
		}

		// Token: 0x020001AA RID: 426
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsync>d__145 : IAsyncStateMachine
		{
			// Token: 0x06000F5F RID: 3935 RVA: 0x00059758 File Offset: 0x00057958
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_B8;
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_148;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1E8;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_25E;
					}
					default:
						switch (xmlValidatingReaderImpl.parsingFunction)
						{
						case XmlValidatingReaderImpl.ParsingFunction.Read:
							break;
						case XmlValidatingReaderImpl.ParsingFunction.Init:
							xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
							if (xmlValidatingReaderImpl.coreReader.ReadState == ReadState.Interactive)
							{
								xmlValidatingReaderImpl.ProcessCoreReaderEvent();
								result = true;
								goto IL_287;
							}
							break;
						case XmlValidatingReaderImpl.ParsingFunction.ParseDtdFromContext:
							xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
							configuredTaskAwaiter4 = xmlValidatingReaderImpl.ParseDtdFromParserContextAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadAsync>d__145>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							goto IL_148;
						case XmlValidatingReaderImpl.ParsingFunction.ResolveEntityInternally:
							xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
							configuredTaskAwaiter4 = xmlValidatingReaderImpl.ResolveEntityInternallyAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 2;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadAsync>d__145>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							goto IL_1E8;
						case XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent:
							xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
							configuredTaskAwaiter4 = xmlValidatingReaderImpl.readBinaryHelper.FinishAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 3;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadAsync>d__145>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							goto IL_25E;
						case XmlValidatingReaderImpl.ParsingFunction.ReaderClosed:
						case XmlValidatingReaderImpl.ParsingFunction.Error:
							result = false;
							goto IL_287;
						default:
							result = false;
							goto IL_287;
						}
						break;
					}
					IL_52:
					configuredTaskAwaiter3 = xmlValidatingReaderImpl.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadAsync>d__145>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_B8:
					if (configuredTaskAwaiter3.GetResult())
					{
						xmlValidatingReaderImpl.ProcessCoreReaderEvent();
						result = true;
						goto IL_287;
					}
					xmlValidatingReaderImpl.validator.CompleteValidation();
					result = false;
					goto IL_287;
					IL_148:
					configuredTaskAwaiter4.GetResult();
					goto IL_52;
					IL_1E8:
					configuredTaskAwaiter4.GetResult();
					goto IL_52;
					IL_25E:
					configuredTaskAwaiter4.GetResult();
					goto IL_52;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_287:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000F60 RID: 3936 RVA: 0x00059A1C File Offset: 0x00057C1C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A78 RID: 2680
			public int <>1__state;

			// Token: 0x04000A79 RID: 2681
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000A7A RID: 2682
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000A7B RID: 2683
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000A7C RID: 2684
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020001AB RID: 427
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBase64Async>d__146 : IAsyncStateMachine
		{
			// Token: 0x06000F61 RID: 3937 RVA: 0x00059A2C File Offset: 0x00057C2C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xmlValidatingReaderImpl.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_E7;
						}
						if (xmlValidatingReaderImpl.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
						{
							xmlValidatingReaderImpl.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xmlValidatingReaderImpl.readBinaryHelper, xmlValidatingReaderImpl.outerReader);
						}
						xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
						configuredTaskAwaiter = xmlValidatingReaderImpl.readBinaryHelper.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadContentAsBase64Async>d__146>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_E7:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000F62 RID: 3938 RVA: 0x00059B44 File Offset: 0x00057D44
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A7D RID: 2685
			public int <>1__state;

			// Token: 0x04000A7E RID: 2686
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000A7F RID: 2687
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000A80 RID: 2688
			public byte[] buffer;

			// Token: 0x04000A81 RID: 2689
			public int index;

			// Token: 0x04000A82 RID: 2690
			public int count;

			// Token: 0x04000A83 RID: 2691
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001AC RID: 428
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBinHexAsync>d__147 : IAsyncStateMachine
		{
			// Token: 0x06000F63 RID: 3939 RVA: 0x00059B54 File Offset: 0x00057D54
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xmlValidatingReaderImpl.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_E7;
						}
						if (xmlValidatingReaderImpl.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
						{
							xmlValidatingReaderImpl.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xmlValidatingReaderImpl.readBinaryHelper, xmlValidatingReaderImpl.outerReader);
						}
						xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
						configuredTaskAwaiter = xmlValidatingReaderImpl.readBinaryHelper.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadContentAsBinHexAsync>d__147>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_E7:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000F64 RID: 3940 RVA: 0x00059C6C File Offset: 0x00057E6C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A84 RID: 2692
			public int <>1__state;

			// Token: 0x04000A85 RID: 2693
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000A86 RID: 2694
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000A87 RID: 2695
			public byte[] buffer;

			// Token: 0x04000A88 RID: 2696
			public int index;

			// Token: 0x04000A89 RID: 2697
			public int count;

			// Token: 0x04000A8A RID: 2698
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001AD RID: 429
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBase64Async>d__148 : IAsyncStateMachine
		{
			// Token: 0x06000F65 RID: 3941 RVA: 0x00059C7C File Offset: 0x00057E7C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xmlValidatingReaderImpl.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_E7;
						}
						if (xmlValidatingReaderImpl.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
						{
							xmlValidatingReaderImpl.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xmlValidatingReaderImpl.readBinaryHelper, xmlValidatingReaderImpl.outerReader);
						}
						xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
						configuredTaskAwaiter = xmlValidatingReaderImpl.readBinaryHelper.ReadElementContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadElementContentAsBase64Async>d__148>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_E7:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000F66 RID: 3942 RVA: 0x00059D94 File Offset: 0x00057F94
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A8B RID: 2699
			public int <>1__state;

			// Token: 0x04000A8C RID: 2700
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000A8D RID: 2701
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000A8E RID: 2702
			public byte[] buffer;

			// Token: 0x04000A8F RID: 2703
			public int index;

			// Token: 0x04000A90 RID: 2704
			public int count;

			// Token: 0x04000A91 RID: 2705
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001AE RID: 430
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBinHexAsync>d__149 : IAsyncStateMachine
		{
			// Token: 0x06000F67 RID: 3943 RVA: 0x00059DA4 File Offset: 0x00057FA4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xmlValidatingReaderImpl.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_E7;
						}
						if (xmlValidatingReaderImpl.parsingFunction != XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent)
						{
							xmlValidatingReaderImpl.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xmlValidatingReaderImpl.readBinaryHelper, xmlValidatingReaderImpl.outerReader);
						}
						xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.Read;
						configuredTaskAwaiter = xmlValidatingReaderImpl.readBinaryHelper.ReadElementContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadElementContentAsBinHexAsync>d__149>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					xmlValidatingReaderImpl.parsingFunction = XmlValidatingReaderImpl.ParsingFunction.InReadBinaryContent;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_E7:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000F68 RID: 3944 RVA: 0x00059EBC File Offset: 0x000580BC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A92 RID: 2706
			public int <>1__state;

			// Token: 0x04000A93 RID: 2707
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000A94 RID: 2708
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000A95 RID: 2709
			public byte[] buffer;

			// Token: 0x04000A96 RID: 2710
			public int index;

			// Token: 0x04000A97 RID: 2711
			public int count;

			// Token: 0x04000A98 RID: 2712
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001AF RID: 431
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <MoveOffEntityReferenceAsync>d__150 : IAsyncStateMachine
		{
			// Token: 0x06000F69 RID: 3945 RVA: 0x00059ECC File Offset: 0x000580CC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (xmlValidatingReaderImpl.outerReader.NodeType != XmlNodeType.EntityReference || xmlValidatingReaderImpl.parsingFunction == XmlValidatingReaderImpl.ParsingFunction.ResolveEntityInternally)
						{
							goto IL_A3;
						}
						configuredTaskAwaiter3 = xmlValidatingReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<MoveOffEntityReferenceAsync>d__150>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
					}
					IL_A3:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000F6A RID: 3946 RVA: 0x00059FBC File Offset: 0x000581BC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A99 RID: 2713
			public int <>1__state;

			// Token: 0x04000A9A RID: 2714
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A9B RID: 2715
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000A9C RID: 2716
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001B0 RID: 432
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadTypedValueAsync>d__151 : IAsyncStateMachine
		{
			// Token: 0x06000F6B RID: 3947 RVA: 0x00059FCC File Offset: 0x000581CC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				object result;
				try
				{
					if (num != 0)
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 1)
						{
							if (xmlValidatingReaderImpl.validationType == ValidationType.None)
							{
								result = null;
								goto IL_24F;
							}
							XmlNodeType nodeType = xmlValidatingReaderImpl.outerReader.NodeType;
							if (nodeType != XmlNodeType.Element)
							{
								if (nodeType == XmlNodeType.Attribute)
								{
									result = xmlValidatingReaderImpl.coreReaderImpl.InternalTypedValue;
									goto IL_24F;
								}
								if (nodeType == XmlNodeType.EndElement)
								{
									result = null;
									goto IL_24F;
								}
								if (xmlValidatingReaderImpl.coreReaderImpl.V1Compat)
								{
									result = null;
									goto IL_24F;
								}
								configuredTaskAwaiter3 = xmlValidatingReaderImpl.GetValueAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 1;
									ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadTypedValueAsync>d__151>(ref configuredTaskAwaiter3, ref this);
									return;
								}
							}
							else
							{
								if (xmlValidatingReaderImpl.SchemaType == null)
								{
									result = null;
									goto IL_24F;
								}
								if (((xmlValidatingReaderImpl.SchemaType is XmlSchemaDatatype) ? ((XmlSchemaDatatype)xmlValidatingReaderImpl.SchemaType) : ((XmlSchemaType)xmlValidatingReaderImpl.SchemaType).Datatype) == null)
								{
									result = null;
									goto IL_24F;
								}
								if (!xmlValidatingReaderImpl.outerReader.IsEmptyElement)
								{
									goto IL_AA;
								}
								goto IL_19B;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						result = configuredTaskAwaiter3.GetResult();
						goto IL_24F;
					}
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					num2 = -1;
					goto IL_110;
					IL_AA:
					configuredTaskAwaiter5 = xmlValidatingReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ReadTypedValueAsync>d__151>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_110:
					if (!configuredTaskAwaiter5.GetResult())
					{
						throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
					}
					XmlNodeType nodeType2 = xmlValidatingReaderImpl.outerReader.NodeType;
					if (nodeType2 == XmlNodeType.CDATA || nodeType2 == XmlNodeType.Text || nodeType2 == XmlNodeType.Whitespace || nodeType2 == XmlNodeType.SignificantWhitespace || nodeType2 == XmlNodeType.Comment || nodeType2 == XmlNodeType.ProcessingInstruction)
					{
						goto IL_AA;
					}
					if (xmlValidatingReaderImpl.outerReader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", xmlValidatingReaderImpl.outerReader.NodeType.ToString());
					}
					IL_19B:
					result = xmlValidatingReaderImpl.coreReaderImpl.InternalTypedValue;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_24F:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000F6C RID: 3948 RVA: 0x0005A258 File Offset: 0x00058458
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A9D RID: 2717
			public int <>1__state;

			// Token: 0x04000A9E RID: 2718
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x04000A9F RID: 2719
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000AA0 RID: 2720
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000AA1 RID: 2721
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020001B1 RID: 433
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseDtdFromParserContextAsync>d__152 : IAsyncStateMachine
		{
			// Token: 0x06000F6D RID: 3949 RVA: 0x0005A268 File Offset: 0x00058468
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xmlValidatingReaderImpl.parserContext.DocTypeName == null || xmlValidatingReaderImpl.parserContext.DocTypeName.Length == 0)
						{
							goto IL_113;
						}
						IDtdParser dtdParser = DtdParser.Create();
						XmlTextReaderImpl.DtdParserProxy adapter = new XmlTextReaderImpl.DtdParserProxy(xmlValidatingReaderImpl.coreReaderImpl);
						configuredTaskAwaiter = dtdParser.ParseFreeFloatingDtdAsync(xmlValidatingReaderImpl.parserContext.BaseURI, xmlValidatingReaderImpl.parserContext.DocTypeName, xmlValidatingReaderImpl.parserContext.PublicId, xmlValidatingReaderImpl.parserContext.SystemId, xmlValidatingReaderImpl.parserContext.InternalSubset, adapter).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ParseDtdFromParserContextAsync>d__152>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IDtdInfo result = configuredTaskAwaiter.GetResult();
					xmlValidatingReaderImpl.coreReaderImpl.SetDtdInfo(result);
					xmlValidatingReaderImpl.ValidateDtd();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_113:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000F6E RID: 3950 RVA: 0x0005A3AC File Offset: 0x000585AC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000AA2 RID: 2722
			public int <>1__state;

			// Token: 0x04000AA3 RID: 2723
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000AA4 RID: 2724
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000AA5 RID: 2725
			private ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001B2 RID: 434
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ResolveEntityInternallyAsync>d__153 : IAsyncStateMachine
		{
			// Token: 0x06000F6F RID: 3951 RVA: 0x0005A3BC File Offset: 0x000585BC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlValidatingReaderImpl xmlValidatingReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_8C;
					}
					initialDepth = xmlValidatingReaderImpl.coreReader.Depth;
					xmlValidatingReaderImpl.outerReader.ResolveEntity();
					IL_2D:
					configuredTaskAwaiter3 = xmlValidatingReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlValidatingReaderImpl.<ResolveEntityInternallyAsync>d__153>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_8C:
					if (configuredTaskAwaiter3.GetResult() && xmlValidatingReaderImpl.coreReader.Depth > initialDepth)
					{
						goto IL_2D;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000F70 RID: 3952 RVA: 0x0005A4B0 File Offset: 0x000586B0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000AA6 RID: 2726
			public int <>1__state;

			// Token: 0x04000AA7 RID: 2727
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000AA8 RID: 2728
			public XmlValidatingReaderImpl <>4__this;

			// Token: 0x04000AA9 RID: 2729
			private int <initialDepth>5__1;

			// Token: 0x04000AAA RID: 2730
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
