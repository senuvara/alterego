﻿using System;

namespace System.Xml
{
	// Token: 0x020000AB RID: 171
	internal class IncrementalReadCharsDecoder : IncrementalReadDecoder
	{
		// Token: 0x06000586 RID: 1414 RVA: 0x0000DF25 File Offset: 0x0000C125
		internal IncrementalReadCharsDecoder()
		{
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x06000587 RID: 1415 RVA: 0x000183C8 File Offset: 0x000165C8
		internal override int DecodedCount
		{
			get
			{
				return this.curIndex - this.startIndex;
			}
		}

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x06000588 RID: 1416 RVA: 0x000183D7 File Offset: 0x000165D7
		internal override bool IsFull
		{
			get
			{
				return this.curIndex == this.endIndex;
			}
		}

		// Token: 0x06000589 RID: 1417 RVA: 0x000183E8 File Offset: 0x000165E8
		internal override int Decode(char[] chars, int startPos, int len)
		{
			int num = this.endIndex - this.curIndex;
			if (num > len)
			{
				num = len;
			}
			Buffer.BlockCopy(chars, startPos * 2, this.buffer, this.curIndex * 2, num * 2);
			this.curIndex += num;
			return num;
		}

		// Token: 0x0600058A RID: 1418 RVA: 0x00018434 File Offset: 0x00016634
		internal override int Decode(string str, int startPos, int len)
		{
			int num = this.endIndex - this.curIndex;
			if (num > len)
			{
				num = len;
			}
			str.CopyTo(startPos, this.buffer, this.curIndex, num);
			this.curIndex += num;
			return num;
		}

		// Token: 0x0600058B RID: 1419 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void Reset()
		{
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x00018478 File Offset: 0x00016678
		internal override void SetNextOutputBuffer(Array buffer, int index, int count)
		{
			this.buffer = (char[])buffer;
			this.startIndex = index;
			this.curIndex = index;
			this.endIndex = index + count;
		}

		// Token: 0x04000332 RID: 818
		private char[] buffer;

		// Token: 0x04000333 RID: 819
		private int startIndex;

		// Token: 0x04000334 RID: 820
		private int curIndex;

		// Token: 0x04000335 RID: 821
		private int endIndex;
	}
}
