﻿using System;

namespace System.Xml
{
	// Token: 0x02000091 RID: 145
	internal class ByteStack
	{
		// Token: 0x060004C8 RID: 1224 RVA: 0x00016315 File Offset: 0x00014515
		public ByteStack(int growthRate)
		{
			this.growthRate = growthRate;
			this.top = 0;
			this.stack = new byte[growthRate];
			this.size = growthRate;
		}

		// Token: 0x060004C9 RID: 1225 RVA: 0x00016340 File Offset: 0x00014540
		public void Push(byte data)
		{
			if (this.size == this.top)
			{
				byte[] dst = new byte[this.size + this.growthRate];
				if (this.top > 0)
				{
					Buffer.BlockCopy(this.stack, 0, dst, 0, this.top);
				}
				this.stack = dst;
				this.size += this.growthRate;
			}
			byte[] array = this.stack;
			int num = this.top;
			this.top = num + 1;
			array[num] = data;
		}

		// Token: 0x060004CA RID: 1226 RVA: 0x000163C0 File Offset: 0x000145C0
		public byte Pop()
		{
			if (this.top > 0)
			{
				byte[] array = this.stack;
				int num = this.top - 1;
				this.top = num;
				return array[num];
			}
			return 0;
		}

		// Token: 0x060004CB RID: 1227 RVA: 0x000163F0 File Offset: 0x000145F0
		public byte Peek()
		{
			if (this.top > 0)
			{
				return this.stack[this.top - 1];
			}
			return 0;
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x060004CC RID: 1228 RVA: 0x0001640C File Offset: 0x0001460C
		public int Length
		{
			get
			{
				return this.top;
			}
		}

		// Token: 0x040002FC RID: 764
		private byte[] stack;

		// Token: 0x040002FD RID: 765
		private int growthRate;

		// Token: 0x040002FE RID: 766
		private int top;

		// Token: 0x040002FF RID: 767
		private int size;
	}
}
