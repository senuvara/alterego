﻿using System;
using System.Reflection;
using System.Security;
using System.Security.Permissions;

namespace System.Xml
{
	// Token: 0x02000092 RID: 146
	internal static class BinaryCompatibility
	{
		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x060004CD RID: 1229 RVA: 0x00016414 File Offset: 0x00014614
		internal static bool TargetsAtLeast_Desktop_V4_5_2
		{
			get
			{
				return BinaryCompatibility._targetsAtLeast_Desktop_V4_5_2;
			}
		}

		// Token: 0x060004CE RID: 1230 RVA: 0x0001641C File Offset: 0x0001461C
		[SecuritySafeCritical]
		[ReflectionPermission(SecurityAction.Assert, Unrestricted = true)]
		private static bool RunningOnCheck(string propertyName)
		{
			Type type;
			try
			{
				type = typeof(object).GetTypeInfo().Assembly.GetType("System.Runtime.Versioning.BinaryCompatibility", false);
			}
			catch (TypeLoadException)
			{
				return false;
			}
			if (type == null)
			{
				return false;
			}
			PropertyInfo property = type.GetProperty(propertyName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			return !(property == null) && (bool)property.GetValue(null);
		}

		// Token: 0x060004CF RID: 1231 RVA: 0x00016490 File Offset: 0x00014690
		// Note: this type is marked as 'beforefieldinit'.
		static BinaryCompatibility()
		{
		}

		// Token: 0x04000300 RID: 768
		private static bool _targetsAtLeast_Desktop_V4_5_2 = BinaryCompatibility.RunningOnCheck("TargetsAtLeast_Desktop_V4_5_2");
	}
}
