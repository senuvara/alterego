﻿using System;

namespace System.Xml
{
	// Token: 0x02000246 RID: 582
	internal class HWStack : ICloneable
	{
		// Token: 0x06001675 RID: 5749 RVA: 0x0007BE4A File Offset: 0x0007A04A
		internal HWStack(int GrowthRate) : this(GrowthRate, int.MaxValue)
		{
		}

		// Token: 0x06001676 RID: 5750 RVA: 0x0007BE58 File Offset: 0x0007A058
		internal HWStack(int GrowthRate, int limit)
		{
			this.growthRate = GrowthRate;
			this.used = 0;
			this.stack = new object[GrowthRate];
			this.size = GrowthRate;
			this.limit = limit;
		}

		// Token: 0x06001677 RID: 5751 RVA: 0x0007BE88 File Offset: 0x0007A088
		internal object Push()
		{
			if (this.used == this.size)
			{
				if (this.limit <= this.used)
				{
					throw new XmlException("Stack overflow.", string.Empty);
				}
				object[] destinationArray = new object[this.size + this.growthRate];
				if (this.used > 0)
				{
					Array.Copy(this.stack, 0, destinationArray, 0, this.used);
				}
				this.stack = destinationArray;
				this.size += this.growthRate;
			}
			object[] array = this.stack;
			int num = this.used;
			this.used = num + 1;
			return array[num];
		}

		// Token: 0x06001678 RID: 5752 RVA: 0x0007BF23 File Offset: 0x0007A123
		internal object Pop()
		{
			if (0 < this.used)
			{
				this.used--;
				return this.stack[this.used];
			}
			return null;
		}

		// Token: 0x06001679 RID: 5753 RVA: 0x0007BF4B File Offset: 0x0007A14B
		internal object Peek()
		{
			if (this.used <= 0)
			{
				return null;
			}
			return this.stack[this.used - 1];
		}

		// Token: 0x0600167A RID: 5754 RVA: 0x0007BF67 File Offset: 0x0007A167
		internal void AddToTop(object o)
		{
			if (this.used > 0)
			{
				this.stack[this.used - 1] = o;
			}
		}

		// Token: 0x17000482 RID: 1154
		internal object this[int index]
		{
			get
			{
				if (index >= 0 && index < this.used)
				{
					return this.stack[index];
				}
				throw new IndexOutOfRangeException();
			}
			set
			{
				if (index >= 0 && index < this.used)
				{
					this.stack[index] = value;
					return;
				}
				throw new IndexOutOfRangeException();
			}
		}

		// Token: 0x17000483 RID: 1155
		// (get) Token: 0x0600167D RID: 5757 RVA: 0x0007BFBD File Offset: 0x0007A1BD
		internal int Length
		{
			get
			{
				return this.used;
			}
		}

		// Token: 0x0600167E RID: 5758 RVA: 0x0007BFC5 File Offset: 0x0007A1C5
		private HWStack(object[] stack, int growthRate, int used, int size)
		{
			this.stack = stack;
			this.growthRate = growthRate;
			this.used = used;
			this.size = size;
		}

		// Token: 0x0600167F RID: 5759 RVA: 0x0007BFEA File Offset: 0x0007A1EA
		public object Clone()
		{
			return new HWStack((object[])this.stack.Clone(), this.growthRate, this.used, this.size);
		}

		// Token: 0x04000E27 RID: 3623
		private object[] stack;

		// Token: 0x04000E28 RID: 3624
		private int growthRate;

		// Token: 0x04000E29 RID: 3625
		private int used;

		// Token: 0x04000E2A RID: 3626
		private int size;

		// Token: 0x04000E2B RID: 3627
		private int limit;
	}
}
