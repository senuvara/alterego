﻿using System;

namespace System.Xml
{
	/// <summary>Provides data for the <see cref="E:System.Xml.XmlDocument.NodeChanged" />, <see cref="E:System.Xml.XmlDocument.NodeChanging" />, <see cref="E:System.Xml.XmlDocument.NodeInserted" />, <see cref="E:System.Xml.XmlDocument.NodeInserting" />, <see cref="E:System.Xml.XmlDocument.NodeRemoved" /> and <see cref="E:System.Xml.XmlDocument.NodeRemoving" /> events.</summary>
	// Token: 0x02000239 RID: 569
	public class XmlNodeChangedEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlNodeChangedEventArgs" /> class.</summary>
		/// <param name="node">The <see cref="T:System.Xml.XmlNode" /> that generated the event.</param>
		/// <param name="oldParent">The old parent <see cref="T:System.Xml.XmlNode" /> of the <see cref="T:System.Xml.XmlNode" /> that generated the event.</param>
		/// <param name="newParent">The new parent <see cref="T:System.Xml.XmlNode" /> of the <see cref="T:System.Xml.XmlNode" /> that generated the event.</param>
		/// <param name="oldValue">The old value of the <see cref="T:System.Xml.XmlNode" /> that generated the event.</param>
		/// <param name="newValue">The new value of the <see cref="T:System.Xml.XmlNode" /> that generated the event.</param>
		/// <param name="action">The <see cref="T:System.Xml.XmlNodeChangedAction" />.</param>
		// Token: 0x0600159F RID: 5535 RVA: 0x000797C2 File Offset: 0x000779C2
		public XmlNodeChangedEventArgs(XmlNode node, XmlNode oldParent, XmlNode newParent, string oldValue, string newValue, XmlNodeChangedAction action)
		{
			this.node = node;
			this.oldParent = oldParent;
			this.newParent = newParent;
			this.action = action;
			this.oldValue = oldValue;
			this.newValue = newValue;
		}

		/// <summary>Gets a value indicating what type of node change event is occurring.</summary>
		/// <returns>An <see langword="XmlNodeChangedAction" /> value describing the node change event.XmlNodeChangedAction Value Description Insert A node has been or will be inserted. Remove A node has been or will be removed. Change A node has been or will be changed. The <see langword="Action" /> value does not differentiate between when the event occurred (before or after). You can create separate event handlers to handle both instances.</returns>
		// Token: 0x17000424 RID: 1060
		// (get) Token: 0x060015A0 RID: 5536 RVA: 0x000797F7 File Offset: 0x000779F7
		public XmlNodeChangedAction Action
		{
			get
			{
				return this.action;
			}
		}

		/// <summary>Gets the <see cref="T:System.Xml.XmlNode" /> that is being added, removed or changed.</summary>
		/// <returns>The <see langword="XmlNode" /> that is being added, removed or changed; this property never returns <see langword="null" />.</returns>
		// Token: 0x17000425 RID: 1061
		// (get) Token: 0x060015A1 RID: 5537 RVA: 0x000797FF File Offset: 0x000779FF
		public XmlNode Node
		{
			get
			{
				return this.node;
			}
		}

		/// <summary>Gets the value of the <see cref="P:System.Xml.XmlNode.ParentNode" /> before the operation began.</summary>
		/// <returns>The value of the <see langword="ParentNode" /> before the operation began. This property returns <see langword="null" /> if the node did not have a parent.For attribute nodes this property returns the <see cref="P:System.Xml.XmlAttribute.OwnerElement" />.</returns>
		// Token: 0x17000426 RID: 1062
		// (get) Token: 0x060015A2 RID: 5538 RVA: 0x00079807 File Offset: 0x00077A07
		public XmlNode OldParent
		{
			get
			{
				return this.oldParent;
			}
		}

		/// <summary>Gets the value of the <see cref="P:System.Xml.XmlNode.ParentNode" /> after the operation completes.</summary>
		/// <returns>The value of the <see langword="ParentNode" /> after the operation completes. This property returns <see langword="null" /> if the node is being removed.For attribute nodes this property returns the <see cref="P:System.Xml.XmlAttribute.OwnerElement" />.</returns>
		// Token: 0x17000427 RID: 1063
		// (get) Token: 0x060015A3 RID: 5539 RVA: 0x0007980F File Offset: 0x00077A0F
		public XmlNode NewParent
		{
			get
			{
				return this.newParent;
			}
		}

		/// <summary>Gets the original value of the node.</summary>
		/// <returns>The original value of the node. This property returns <see langword="null" /> if the node is neither an attribute nor a text node, or if the node is being inserted.If called in a <see cref="E:System.Xml.XmlDocument.NodeChanging" /> event, <see langword="OldValue" /> returns the current value of the node that will be replaced if the change is successful. If called in a <see cref="E:System.Xml.XmlDocument.NodeChanged" /> event, <see langword="OldValue" /> returns the value of node prior to the change.</returns>
		// Token: 0x17000428 RID: 1064
		// (get) Token: 0x060015A4 RID: 5540 RVA: 0x00079817 File Offset: 0x00077A17
		public string OldValue
		{
			get
			{
				return this.oldValue;
			}
		}

		/// <summary>Gets the new value of the node.</summary>
		/// <returns>The new value of the node. This property returns <see langword="null" /> if the node is neither an attribute nor a text node, or if the node is being removed.If called in a <see cref="E:System.Xml.XmlDocument.NodeChanging" /> event, <see langword="NewValue" /> returns the value of the node if the change is successful. If called in a <see cref="E:System.Xml.XmlDocument.NodeChanged" /> event, <see langword="NewValue" /> returns the current value of the node.</returns>
		// Token: 0x17000429 RID: 1065
		// (get) Token: 0x060015A5 RID: 5541 RVA: 0x0007981F File Offset: 0x00077A1F
		public string NewValue
		{
			get
			{
				return this.newValue;
			}
		}

		// Token: 0x04000DFA RID: 3578
		private XmlNodeChangedAction action;

		// Token: 0x04000DFB RID: 3579
		private XmlNode node;

		// Token: 0x04000DFC RID: 3580
		private XmlNode oldParent;

		// Token: 0x04000DFD RID: 3581
		private XmlNode newParent;

		// Token: 0x04000DFE RID: 3582
		private string oldValue;

		// Token: 0x04000DFF RID: 3583
		private string newValue;
	}
}
