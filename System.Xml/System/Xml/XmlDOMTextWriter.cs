﻿using System;
using System.IO;
using System.Text;

namespace System.Xml
{
	// Token: 0x02000226 RID: 550
	internal class XmlDOMTextWriter : XmlTextWriter
	{
		// Token: 0x06001469 RID: 5225 RVA: 0x00074DD0 File Offset: 0x00072FD0
		public XmlDOMTextWriter(Stream w, Encoding encoding) : base(w, encoding)
		{
		}

		// Token: 0x0600146A RID: 5226 RVA: 0x00074DDA File Offset: 0x00072FDA
		public XmlDOMTextWriter(string filename, Encoding encoding) : base(filename, encoding)
		{
		}

		// Token: 0x0600146B RID: 5227 RVA: 0x00074DE4 File Offset: 0x00072FE4
		public XmlDOMTextWriter(TextWriter w) : base(w)
		{
		}

		// Token: 0x0600146C RID: 5228 RVA: 0x00074DED File Offset: 0x00072FED
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			if (ns.Length == 0 && prefix.Length != 0)
			{
				prefix = "";
			}
			base.WriteStartElement(prefix, localName, ns);
		}

		// Token: 0x0600146D RID: 5229 RVA: 0x00074E0F File Offset: 0x0007300F
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			if (ns.Length == 0 && prefix.Length != 0)
			{
				prefix = "";
			}
			base.WriteStartAttribute(prefix, localName, ns);
		}
	}
}
