﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020001B3 RID: 435
	internal class XmlWellFormedWriter : XmlWriter
	{
		// Token: 0x06000F71 RID: 3953 RVA: 0x0005A4C0 File Offset: 0x000586C0
		internal XmlWellFormedWriter(XmlWriter writer, XmlWriterSettings settings)
		{
			this.writer = writer;
			this.rawWriter = (writer as XmlRawWriter);
			this.predefinedNamespaces = (writer as IXmlNamespaceResolver);
			if (this.rawWriter != null)
			{
				this.rawWriter.NamespaceResolver = new XmlWellFormedWriter.NamespaceResolverProxy(this);
			}
			this.checkCharacters = settings.CheckCharacters;
			this.omitDuplNamespaces = ((settings.NamespaceHandling & NamespaceHandling.OmitDuplicates) > NamespaceHandling.Default);
			this.writeEndDocumentOnClose = settings.WriteEndDocumentOnClose;
			this.conformanceLevel = settings.ConformanceLevel;
			this.stateTable = ((this.conformanceLevel == ConformanceLevel.Document) ? XmlWellFormedWriter.StateTableDocument : XmlWellFormedWriter.StateTableAuto);
			this.currentState = XmlWellFormedWriter.State.Start;
			this.nsStack = new XmlWellFormedWriter.Namespace[8];
			this.nsStack[0].Set("xmlns", "http://www.w3.org/2000/xmlns/", XmlWellFormedWriter.NamespaceKind.Special);
			this.nsStack[1].Set("xml", "http://www.w3.org/XML/1998/namespace", XmlWellFormedWriter.NamespaceKind.Special);
			if (this.predefinedNamespaces == null)
			{
				this.nsStack[2].Set(string.Empty, string.Empty, XmlWellFormedWriter.NamespaceKind.Implied);
			}
			else
			{
				string text = this.predefinedNamespaces.LookupNamespace(string.Empty);
				this.nsStack[2].Set(string.Empty, (text == null) ? string.Empty : text, XmlWellFormedWriter.NamespaceKind.Implied);
			}
			this.nsTop = 2;
			this.elemScopeStack = new XmlWellFormedWriter.ElementScope[8];
			this.elemScopeStack[0].Set(string.Empty, string.Empty, string.Empty, this.nsTop);
			this.elemScopeStack[0].xmlSpace = XmlSpace.None;
			this.elemScopeStack[0].xmlLang = null;
			this.elemTop = 0;
			this.attrStack = new XmlWellFormedWriter.AttrName[8];
			this.hasher = new SecureStringHasher();
		}

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x06000F72 RID: 3954 RVA: 0x0005A685 File Offset: 0x00058885
		public override WriteState WriteState
		{
			get
			{
				if (this.currentState <= XmlWellFormedWriter.State.Error)
				{
					return XmlWellFormedWriter.state2WriteState[(int)this.currentState];
				}
				return WriteState.Error;
			}
		}

		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06000F73 RID: 3955 RVA: 0x0005A6A0 File Offset: 0x000588A0
		public override XmlWriterSettings Settings
		{
			get
			{
				XmlWriterSettings settings = this.writer.Settings;
				settings.ReadOnly = false;
				settings.ConformanceLevel = this.conformanceLevel;
				if (this.omitDuplNamespaces)
				{
					settings.NamespaceHandling |= NamespaceHandling.OmitDuplicates;
				}
				settings.WriteEndDocumentOnClose = this.writeEndDocumentOnClose;
				settings.ReadOnly = true;
				return settings;
			}
		}

		// Token: 0x06000F74 RID: 3956 RVA: 0x0005A6F6 File Offset: 0x000588F6
		public override void WriteStartDocument()
		{
			this.WriteStartDocumentImpl(XmlStandalone.Omit);
		}

		// Token: 0x06000F75 RID: 3957 RVA: 0x0005A6FF File Offset: 0x000588FF
		public override void WriteStartDocument(bool standalone)
		{
			this.WriteStartDocumentImpl(standalone ? XmlStandalone.Yes : XmlStandalone.No);
		}

		// Token: 0x06000F76 RID: 3958 RVA: 0x0005A710 File Offset: 0x00058910
		public override void WriteEndDocument()
		{
			try
			{
				while (this.elemTop > 0)
				{
					this.WriteEndElement();
				}
				int num = (int)this.currentState;
				this.AdvanceState(XmlWellFormedWriter.Token.EndDocument);
				if (num != 7)
				{
					throw new ArgumentException(Res.GetString("Document does not have a root element."));
				}
				if (this.rawWriter == null)
				{
					this.writer.WriteEndDocument();
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F77 RID: 3959 RVA: 0x0005A780 File Offset: 0x00058980
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			try
			{
				if (name == null || name.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
				}
				XmlConvert.VerifyQName(name, ExceptionType.XmlException);
				if (this.conformanceLevel == ConformanceLevel.Fragment)
				{
					throw new InvalidOperationException(Res.GetString("DTD is not allowed in XML fragments."));
				}
				this.AdvanceState(XmlWellFormedWriter.Token.Dtd);
				if (this.dtdWritten)
				{
					this.currentState = XmlWellFormedWriter.State.Error;
					throw new InvalidOperationException(Res.GetString("The DTD has already been written out."));
				}
				if (this.conformanceLevel == ConformanceLevel.Auto)
				{
					this.conformanceLevel = ConformanceLevel.Document;
					this.stateTable = XmlWellFormedWriter.StateTableDocument;
				}
				if (this.checkCharacters)
				{
					int invCharIndex;
					if (pubid != null && (invCharIndex = this.xmlCharType.IsPublicId(pubid)) >= 0)
					{
						throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(pubid, invCharIndex)), "pubid");
					}
					if (sysid != null && (invCharIndex = this.xmlCharType.IsOnlyCharData(sysid)) >= 0)
					{
						throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(sysid, invCharIndex)), "sysid");
					}
					if (subset != null && (invCharIndex = this.xmlCharType.IsOnlyCharData(subset)) >= 0)
					{
						throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(subset, invCharIndex)), "subset");
					}
				}
				this.writer.WriteDocType(name, pubid, sysid, subset);
				this.dtdWritten = true;
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F78 RID: 3960 RVA: 0x0005A8E8 File Offset: 0x00058AE8
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			try
			{
				if (localName == null || localName.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid local name."));
				}
				this.CheckNCName(localName);
				this.AdvanceState(XmlWellFormedWriter.Token.StartElement);
				if (prefix == null)
				{
					if (ns != null)
					{
						prefix = this.LookupPrefix(ns);
					}
					if (prefix == null)
					{
						prefix = string.Empty;
					}
				}
				else if (prefix.Length > 0)
				{
					this.CheckNCName(prefix);
					if (ns == null)
					{
						ns = this.LookupNamespace(prefix);
					}
					if (ns == null || (ns != null && ns.Length == 0))
					{
						throw new ArgumentException(Res.GetString("Cannot use a prefix with an empty namespace."));
					}
				}
				if (ns == null)
				{
					ns = this.LookupNamespace(prefix);
					if (ns == null)
					{
						ns = string.Empty;
					}
				}
				if (this.elemTop == 0 && this.rawWriter != null)
				{
					this.rawWriter.OnRootElement(this.conformanceLevel);
				}
				this.writer.WriteStartElement(prefix, localName, ns);
				int num = this.elemTop + 1;
				this.elemTop = num;
				int num2 = num;
				if (num2 == this.elemScopeStack.Length)
				{
					XmlWellFormedWriter.ElementScope[] destinationArray = new XmlWellFormedWriter.ElementScope[num2 * 2];
					Array.Copy(this.elemScopeStack, destinationArray, num2);
					this.elemScopeStack = destinationArray;
				}
				this.elemScopeStack[num2].Set(prefix, localName, ns, this.nsTop);
				this.PushNamespaceImplicit(prefix, ns);
				if (this.attrCount >= 14)
				{
					this.attrHashTable.Clear();
				}
				this.attrCount = 0;
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F79 RID: 3961 RVA: 0x0005AA5C File Offset: 0x00058C5C
		public override void WriteEndElement()
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.EndElement);
				int num = this.elemTop;
				if (num == 0)
				{
					throw new XmlException("There was no XML start tag open.", string.Empty);
				}
				if (this.rawWriter != null)
				{
					this.elemScopeStack[num].WriteEndElement(this.rawWriter);
				}
				else
				{
					this.writer.WriteEndElement();
				}
				int prevNSTop = this.elemScopeStack[num].prevNSTop;
				if (this.useNsHashtable && prevNSTop < this.nsTop)
				{
					this.PopNamespaces(prevNSTop + 1, this.nsTop);
				}
				this.nsTop = prevNSTop;
				if ((this.elemTop = num - 1) == 0)
				{
					if (this.conformanceLevel == ConformanceLevel.Document)
					{
						this.currentState = XmlWellFormedWriter.State.AfterRootEle;
					}
					else
					{
						this.currentState = XmlWellFormedWriter.State.TopLevel;
					}
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F7A RID: 3962 RVA: 0x0005AB34 File Offset: 0x00058D34
		public override void WriteFullEndElement()
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.EndElement);
				int num = this.elemTop;
				if (num == 0)
				{
					throw new XmlException("There was no XML start tag open.", string.Empty);
				}
				if (this.rawWriter != null)
				{
					this.elemScopeStack[num].WriteFullEndElement(this.rawWriter);
				}
				else
				{
					this.writer.WriteFullEndElement();
				}
				int prevNSTop = this.elemScopeStack[num].prevNSTop;
				if (this.useNsHashtable && prevNSTop < this.nsTop)
				{
					this.PopNamespaces(prevNSTop + 1, this.nsTop);
				}
				this.nsTop = prevNSTop;
				if ((this.elemTop = num - 1) == 0)
				{
					if (this.conformanceLevel == ConformanceLevel.Document)
					{
						this.currentState = XmlWellFormedWriter.State.AfterRootEle;
					}
					else
					{
						this.currentState = XmlWellFormedWriter.State.TopLevel;
					}
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F7B RID: 3963 RVA: 0x0005AC0C File Offset: 0x00058E0C
		public override void WriteStartAttribute(string prefix, string localName, string namespaceName)
		{
			try
			{
				if (localName == null || localName.Length == 0)
				{
					if (!(prefix == "xmlns"))
					{
						throw new ArgumentException(Res.GetString("The empty string '' is not a valid local name."));
					}
					localName = "xmlns";
					prefix = string.Empty;
				}
				this.CheckNCName(localName);
				this.AdvanceState(XmlWellFormedWriter.Token.StartAttribute);
				if (prefix == null)
				{
					if (namespaceName != null && (!(localName == "xmlns") || !(namespaceName == "http://www.w3.org/2000/xmlns/")))
					{
						prefix = this.LookupPrefix(namespaceName);
					}
					if (prefix == null)
					{
						prefix = string.Empty;
					}
				}
				if (namespaceName == null)
				{
					if (prefix != null && prefix.Length > 0)
					{
						namespaceName = this.LookupNamespace(prefix);
					}
					if (namespaceName == null)
					{
						namespaceName = string.Empty;
					}
				}
				if (prefix.Length == 0)
				{
					if (localName[0] == 'x' && localName == "xmlns")
					{
						if (namespaceName.Length > 0 && namespaceName != "http://www.w3.org/2000/xmlns/")
						{
							throw new ArgumentException(Res.GetString("Prefix \"xmlns\" is reserved for use by XML."));
						}
						this.curDeclPrefix = string.Empty;
						this.SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute.DefaultXmlns);
						goto IL_224;
					}
					else if (namespaceName.Length > 0)
					{
						prefix = this.LookupPrefix(namespaceName);
						if (prefix == null || prefix.Length == 0)
						{
							prefix = this.GeneratePrefix();
						}
					}
				}
				else
				{
					if (prefix[0] == 'x')
					{
						if (prefix == "xmlns")
						{
							if (namespaceName.Length > 0 && namespaceName != "http://www.w3.org/2000/xmlns/")
							{
								throw new ArgumentException(Res.GetString("Prefix \"xmlns\" is reserved for use by XML."));
							}
							this.curDeclPrefix = localName;
							this.SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute.PrefixedXmlns);
							goto IL_224;
						}
						else if (prefix == "xml")
						{
							if (namespaceName.Length > 0 && namespaceName != "http://www.w3.org/XML/1998/namespace")
							{
								throw new ArgumentException(Res.GetString("Prefix \"xml\" is reserved for use by XML and can be mapped only to namespace name \"http://www.w3.org/XML/1998/namespace\"."));
							}
							if (localName == "space")
							{
								this.SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute.XmlSpace);
								goto IL_224;
							}
							if (localName == "lang")
							{
								this.SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute.XmlLang);
								goto IL_224;
							}
						}
					}
					this.CheckNCName(prefix);
					if (namespaceName.Length == 0)
					{
						prefix = string.Empty;
					}
					else
					{
						string text = this.LookupLocalNamespace(prefix);
						if (text != null && text != namespaceName)
						{
							prefix = this.GeneratePrefix();
						}
					}
				}
				if (prefix.Length != 0)
				{
					this.PushNamespaceImplicit(prefix, namespaceName);
				}
				IL_224:
				this.AddAttribute(prefix, localName, namespaceName);
				if (this.specAttr == XmlWellFormedWriter.SpecialAttribute.No)
				{
					this.writer.WriteStartAttribute(prefix, localName, namespaceName);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F7C RID: 3964 RVA: 0x0005AE88 File Offset: 0x00059088
		public override void WriteEndAttribute()
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.EndAttribute);
				if (this.specAttr != XmlWellFormedWriter.SpecialAttribute.No)
				{
					switch (this.specAttr)
					{
					case XmlWellFormedWriter.SpecialAttribute.DefaultXmlns:
					{
						string stringValue = this.attrValueCache.StringValue;
						if (this.PushNamespaceExplicit(string.Empty, stringValue))
						{
							if (this.rawWriter != null)
							{
								if (this.rawWriter.SupportsNamespaceDeclarationInChunks)
								{
									this.rawWriter.WriteStartNamespaceDeclaration(string.Empty);
									this.attrValueCache.Replay(this.rawWriter);
									this.rawWriter.WriteEndNamespaceDeclaration();
								}
								else
								{
									this.rawWriter.WriteNamespaceDeclaration(string.Empty, stringValue);
								}
							}
							else
							{
								this.writer.WriteStartAttribute(string.Empty, "xmlns", "http://www.w3.org/2000/xmlns/");
								this.attrValueCache.Replay(this.writer);
								this.writer.WriteEndAttribute();
							}
						}
						this.curDeclPrefix = null;
						break;
					}
					case XmlWellFormedWriter.SpecialAttribute.PrefixedXmlns:
					{
						string stringValue = this.attrValueCache.StringValue;
						if (stringValue.Length == 0)
						{
							throw new ArgumentException(Res.GetString("Cannot use a prefix with an empty namespace."));
						}
						if (stringValue == "http://www.w3.org/2000/xmlns/" || (stringValue == "http://www.w3.org/XML/1998/namespace" && this.curDeclPrefix != "xml"))
						{
							throw new ArgumentException(Res.GetString("Cannot bind to the reserved namespace."));
						}
						if (this.PushNamespaceExplicit(this.curDeclPrefix, stringValue))
						{
							if (this.rawWriter != null)
							{
								if (this.rawWriter.SupportsNamespaceDeclarationInChunks)
								{
									this.rawWriter.WriteStartNamespaceDeclaration(this.curDeclPrefix);
									this.attrValueCache.Replay(this.rawWriter);
									this.rawWriter.WriteEndNamespaceDeclaration();
								}
								else
								{
									this.rawWriter.WriteNamespaceDeclaration(this.curDeclPrefix, stringValue);
								}
							}
							else
							{
								this.writer.WriteStartAttribute("xmlns", this.curDeclPrefix, "http://www.w3.org/2000/xmlns/");
								this.attrValueCache.Replay(this.writer);
								this.writer.WriteEndAttribute();
							}
						}
						this.curDeclPrefix = null;
						break;
					}
					case XmlWellFormedWriter.SpecialAttribute.XmlSpace:
					{
						this.attrValueCache.Trim();
						string stringValue = this.attrValueCache.StringValue;
						if (stringValue == "default")
						{
							this.elemScopeStack[this.elemTop].xmlSpace = XmlSpace.Default;
						}
						else
						{
							if (!(stringValue == "preserve"))
							{
								throw new ArgumentException(Res.GetString("'{0}' is an invalid xml:space value.", new object[]
								{
									stringValue
								}));
							}
							this.elemScopeStack[this.elemTop].xmlSpace = XmlSpace.Preserve;
						}
						this.writer.WriteStartAttribute("xml", "space", "http://www.w3.org/XML/1998/namespace");
						this.attrValueCache.Replay(this.writer);
						this.writer.WriteEndAttribute();
						break;
					}
					case XmlWellFormedWriter.SpecialAttribute.XmlLang:
					{
						string stringValue = this.attrValueCache.StringValue;
						this.elemScopeStack[this.elemTop].xmlLang = stringValue;
						this.writer.WriteStartAttribute("xml", "lang", "http://www.w3.org/XML/1998/namespace");
						this.attrValueCache.Replay(this.writer);
						this.writer.WriteEndAttribute();
						break;
					}
					}
					this.specAttr = XmlWellFormedWriter.SpecialAttribute.No;
					this.attrValueCache.Clear();
				}
				else
				{
					this.writer.WriteEndAttribute();
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F7D RID: 3965 RVA: 0x0005B1E4 File Offset: 0x000593E4
		public override void WriteCData(string text)
		{
			try
			{
				if (text == null)
				{
					text = string.Empty;
				}
				this.AdvanceState(XmlWellFormedWriter.Token.CData);
				this.writer.WriteCData(text);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F7E RID: 3966 RVA: 0x0005B22C File Offset: 0x0005942C
		public override void WriteComment(string text)
		{
			try
			{
				if (text == null)
				{
					text = string.Empty;
				}
				this.AdvanceState(XmlWellFormedWriter.Token.Comment);
				this.writer.WriteComment(text);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F7F RID: 3967 RVA: 0x0005B274 File Offset: 0x00059474
		public override void WriteProcessingInstruction(string name, string text)
		{
			try
			{
				if (name == null || name.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
				}
				this.CheckNCName(name);
				if (text == null)
				{
					text = string.Empty;
				}
				if (name.Length == 3 && string.Compare(name, "xml", StringComparison.OrdinalIgnoreCase) == 0)
				{
					if (this.currentState != XmlWellFormedWriter.State.Start)
					{
						throw new ArgumentException(Res.GetString((this.conformanceLevel == ConformanceLevel.Document) ? "Cannot write XML declaration. WriteStartDocument method has already written it." : "Cannot write XML declaration. XML declaration can be only at the beginning of the document."));
					}
					this.xmlDeclFollows = true;
					this.AdvanceState(XmlWellFormedWriter.Token.PI);
					if (this.rawWriter != null)
					{
						this.rawWriter.WriteXmlDeclaration(text);
					}
					else
					{
						this.writer.WriteProcessingInstruction(name, text);
					}
				}
				else
				{
					this.AdvanceState(XmlWellFormedWriter.Token.PI);
					this.writer.WriteProcessingInstruction(name, text);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F80 RID: 3968 RVA: 0x0005B350 File Offset: 0x00059550
		public override void WriteEntityRef(string name)
		{
			try
			{
				if (name == null || name.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
				}
				this.CheckNCName(name);
				this.AdvanceState(XmlWellFormedWriter.Token.Text);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteEntityRef(name);
				}
				else
				{
					this.writer.WriteEntityRef(name);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F81 RID: 3969 RVA: 0x0005B3C8 File Offset: 0x000595C8
		public override void WriteCharEntity(char ch)
		{
			try
			{
				if (char.IsSurrogate(ch))
				{
					throw new ArgumentException(Res.GetString("The surrogate pair is invalid. Missing a low surrogate character."));
				}
				this.AdvanceState(XmlWellFormedWriter.Token.Text);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteCharEntity(ch);
				}
				else
				{
					this.writer.WriteCharEntity(ch);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F82 RID: 3970 RVA: 0x0005B434 File Offset: 0x00059634
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			try
			{
				if (!char.IsSurrogatePair(highChar, lowChar))
				{
					throw XmlConvert.CreateInvalidSurrogatePairException(lowChar, highChar);
				}
				this.AdvanceState(XmlWellFormedWriter.Token.Text);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteSurrogateCharEntity(lowChar, highChar);
				}
				else
				{
					this.writer.WriteSurrogateCharEntity(lowChar, highChar);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F83 RID: 3971 RVA: 0x0005B49C File Offset: 0x0005969C
		public override void WriteWhitespace(string ws)
		{
			try
			{
				if (ws == null)
				{
					ws = string.Empty;
				}
				if (!XmlCharType.Instance.IsOnlyWhitespace(ws))
				{
					throw new ArgumentException(Res.GetString("Only white space characters should be used."));
				}
				this.AdvanceState(XmlWellFormedWriter.Token.Whitespace);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteWhitespace(ws);
				}
				else
				{
					this.writer.WriteWhitespace(ws);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F84 RID: 3972 RVA: 0x0005B51C File Offset: 0x0005971C
		public override void WriteString(string text)
		{
			try
			{
				if (text != null)
				{
					this.AdvanceState(XmlWellFormedWriter.Token.Text);
					if (this.SaveAttrValue)
					{
						this.attrValueCache.WriteString(text);
					}
					else
					{
						this.writer.WriteString(text);
					}
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F85 RID: 3973 RVA: 0x0005B578 File Offset: 0x00059778
		public override void WriteChars(char[] buffer, int index, int count)
		{
			try
			{
				if (buffer == null)
				{
					throw new ArgumentNullException("buffer");
				}
				if (index < 0)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				if (count > buffer.Length - index)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				this.AdvanceState(XmlWellFormedWriter.Token.Text);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteChars(buffer, index, count);
				}
				else
				{
					this.writer.WriteChars(buffer, index, count);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F86 RID: 3974 RVA: 0x0005B610 File Offset: 0x00059810
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			try
			{
				if (buffer == null)
				{
					throw new ArgumentNullException("buffer");
				}
				if (index < 0)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				if (count > buffer.Length - index)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				this.AdvanceState(XmlWellFormedWriter.Token.RawData);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteRaw(buffer, index, count);
				}
				else
				{
					this.writer.WriteRaw(buffer, index, count);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F87 RID: 3975 RVA: 0x0005B6A8 File Offset: 0x000598A8
		public override void WriteRaw(string data)
		{
			try
			{
				if (data != null)
				{
					this.AdvanceState(XmlWellFormedWriter.Token.RawData);
					if (this.SaveAttrValue)
					{
						this.attrValueCache.WriteRaw(data);
					}
					else
					{
						this.writer.WriteRaw(data);
					}
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F88 RID: 3976 RVA: 0x0005B704 File Offset: 0x00059904
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			try
			{
				if (buffer == null)
				{
					throw new ArgumentNullException("buffer");
				}
				if (index < 0)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				if (count > buffer.Length - index)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				this.AdvanceState(XmlWellFormedWriter.Token.Base64);
				this.writer.WriteBase64(buffer, index, count);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F89 RID: 3977 RVA: 0x0005B784 File Offset: 0x00059984
		public override void Close()
		{
			if (this.currentState != XmlWellFormedWriter.State.Closed)
			{
				try
				{
					if (this.writeEndDocumentOnClose)
					{
						while (this.currentState != XmlWellFormedWriter.State.Error)
						{
							if (this.elemTop <= 0)
							{
								break;
							}
							this.WriteEndElement();
						}
					}
					else if (this.currentState != XmlWellFormedWriter.State.Error && this.elemTop > 0)
					{
						try
						{
							this.AdvanceState(XmlWellFormedWriter.Token.EndElement);
						}
						catch
						{
							this.currentState = XmlWellFormedWriter.State.Error;
							throw;
						}
					}
					if (this.InBase64 && this.rawWriter != null)
					{
						this.rawWriter.WriteEndBase64();
					}
					this.writer.Flush();
				}
				finally
				{
					try
					{
						if (this.rawWriter != null)
						{
							this.rawWriter.Close(this.WriteState);
						}
						else
						{
							this.writer.Close();
						}
					}
					finally
					{
						this.currentState = XmlWellFormedWriter.State.Closed;
					}
				}
			}
		}

		// Token: 0x06000F8A RID: 3978 RVA: 0x0005B870 File Offset: 0x00059A70
		public override void Flush()
		{
			try
			{
				this.writer.Flush();
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F8B RID: 3979 RVA: 0x0005B8A8 File Offset: 0x00059AA8
		public override string LookupPrefix(string ns)
		{
			string result;
			try
			{
				if (ns == null)
				{
					throw new ArgumentNullException("ns");
				}
				for (int i = this.nsTop; i >= 0; i--)
				{
					if (this.nsStack[i].namespaceUri == ns)
					{
						string prefix = this.nsStack[i].prefix;
						for (i++; i <= this.nsTop; i++)
						{
							if (this.nsStack[i].prefix == prefix)
							{
								return null;
							}
						}
						return prefix;
					}
				}
				result = ((this.predefinedNamespaces != null) ? this.predefinedNamespaces.LookupPrefix(ns) : null);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x06000F8C RID: 3980 RVA: 0x0005B96C File Offset: 0x00059B6C
		public override XmlSpace XmlSpace
		{
			get
			{
				int num = this.elemTop;
				while (num >= 0 && this.elemScopeStack[num].xmlSpace == (XmlSpace)(-1))
				{
					num--;
				}
				return this.elemScopeStack[num].xmlSpace;
			}
		}

		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000F8D RID: 3981 RVA: 0x0005B9B0 File Offset: 0x00059BB0
		public override string XmlLang
		{
			get
			{
				int num = this.elemTop;
				while (num > 0 && this.elemScopeStack[num].xmlLang == null)
				{
					num--;
				}
				return this.elemScopeStack[num].xmlLang;
			}
		}

		// Token: 0x06000F8E RID: 3982 RVA: 0x0005B9F4 File Offset: 0x00059BF4
		public override void WriteQualifiedName(string localName, string ns)
		{
			try
			{
				if (localName == null || localName.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid local name."));
				}
				this.CheckNCName(localName);
				this.AdvanceState(XmlWellFormedWriter.Token.Text);
				string text = string.Empty;
				if (ns != null && ns.Length != 0)
				{
					text = this.LookupPrefix(ns);
					if (text == null)
					{
						if (this.currentState != XmlWellFormedWriter.State.Attribute)
						{
							throw new ArgumentException(Res.GetString("The '{0}' namespace is not defined.", new object[]
							{
								ns
							}));
						}
						text = this.GeneratePrefix();
						this.PushNamespaceImplicit(text, ns);
					}
				}
				if (this.SaveAttrValue || this.rawWriter == null)
				{
					if (text.Length != 0)
					{
						this.WriteString(text);
						this.WriteString(":");
					}
					this.WriteString(localName);
				}
				else
				{
					this.rawWriter.WriteQualifiedName(text, localName, ns);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F8F RID: 3983 RVA: 0x0005BAD8 File Offset: 0x00059CD8
		public override void WriteValue(bool value)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
				this.writer.WriteValue(value);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F90 RID: 3984 RVA: 0x0005BB18 File Offset: 0x00059D18
		public override void WriteValue(DateTime value)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
				this.writer.WriteValue(value);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F91 RID: 3985 RVA: 0x0005BB58 File Offset: 0x00059D58
		public override void WriteValue(DateTimeOffset value)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
				this.writer.WriteValue(value);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F92 RID: 3986 RVA: 0x0005BB98 File Offset: 0x00059D98
		public override void WriteValue(double value)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
				this.writer.WriteValue(value);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F93 RID: 3987 RVA: 0x0005BBD8 File Offset: 0x00059DD8
		public override void WriteValue(float value)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
				this.writer.WriteValue(value);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F94 RID: 3988 RVA: 0x0005BC18 File Offset: 0x00059E18
		public override void WriteValue(decimal value)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
				this.writer.WriteValue(value);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F95 RID: 3989 RVA: 0x0005BC58 File Offset: 0x00059E58
		public override void WriteValue(int value)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
				this.writer.WriteValue(value);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F96 RID: 3990 RVA: 0x0005BC98 File Offset: 0x00059E98
		public override void WriteValue(long value)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
				this.writer.WriteValue(value);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F97 RID: 3991 RVA: 0x0005BCD8 File Offset: 0x00059ED8
		public override void WriteValue(string value)
		{
			try
			{
				if (value != null)
				{
					if (this.SaveAttrValue)
					{
						this.AdvanceState(XmlWellFormedWriter.Token.Text);
						this.attrValueCache.WriteValue(value);
					}
					else
					{
						this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
						this.writer.WriteValue(value);
					}
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F98 RID: 3992 RVA: 0x0005BD3C File Offset: 0x00059F3C
		public override void WriteValue(object value)
		{
			try
			{
				if (this.SaveAttrValue && value is string)
				{
					this.AdvanceState(XmlWellFormedWriter.Token.Text);
					this.attrValueCache.WriteValue((string)value);
				}
				else
				{
					this.AdvanceState(XmlWellFormedWriter.Token.AtomicValue);
					this.writer.WriteValue(value);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000F99 RID: 3993 RVA: 0x0005BDA8 File Offset: 0x00059FA8
		public override void WriteBinHex(byte[] buffer, int index, int count)
		{
			if (this.IsClosedOrErrorState)
			{
				throw new InvalidOperationException(Res.GetString("The Writer is closed or in error state."));
			}
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.Text);
				base.WriteBinHex(buffer, index, count);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000F9A RID: 3994 RVA: 0x0005BDFC File Offset: 0x00059FFC
		internal XmlWriter InnerWriter
		{
			get
			{
				return this.writer;
			}
		}

		// Token: 0x170002AF RID: 687
		// (get) Token: 0x06000F9B RID: 3995 RVA: 0x0005BE04 File Offset: 0x0005A004
		internal XmlRawWriter RawWriter
		{
			get
			{
				return this.rawWriter;
			}
		}

		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x06000F9C RID: 3996 RVA: 0x0005BE0C File Offset: 0x0005A00C
		private bool SaveAttrValue
		{
			get
			{
				return this.specAttr > XmlWellFormedWriter.SpecialAttribute.No;
			}
		}

		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x06000F9D RID: 3997 RVA: 0x0005BE17 File Offset: 0x0005A017
		private bool InBase64
		{
			get
			{
				return this.currentState == XmlWellFormedWriter.State.B64Content || this.currentState == XmlWellFormedWriter.State.B64Attribute || this.currentState == XmlWellFormedWriter.State.RootLevelB64Attr;
			}
		}

		// Token: 0x06000F9E RID: 3998 RVA: 0x0005BE38 File Offset: 0x0005A038
		private void SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute special)
		{
			this.specAttr = special;
			if (XmlWellFormedWriter.State.Attribute == this.currentState)
			{
				this.currentState = XmlWellFormedWriter.State.SpecialAttr;
			}
			else if (XmlWellFormedWriter.State.RootLevelAttr == this.currentState)
			{
				this.currentState = XmlWellFormedWriter.State.RootLevelSpecAttr;
			}
			if (this.attrValueCache == null)
			{
				this.attrValueCache = new XmlWellFormedWriter.AttributeValueCache();
			}
		}

		// Token: 0x06000F9F RID: 3999 RVA: 0x0005BE84 File Offset: 0x0005A084
		private void WriteStartDocumentImpl(XmlStandalone standalone)
		{
			try
			{
				this.AdvanceState(XmlWellFormedWriter.Token.StartDocument);
				if (this.conformanceLevel == ConformanceLevel.Auto)
				{
					this.conformanceLevel = ConformanceLevel.Document;
					this.stateTable = XmlWellFormedWriter.StateTableDocument;
				}
				else if (this.conformanceLevel == ConformanceLevel.Fragment)
				{
					throw new InvalidOperationException(Res.GetString("WriteStartDocument cannot be called on writers created with ConformanceLevel.Fragment."));
				}
				if (this.rawWriter != null)
				{
					if (!this.xmlDeclFollows)
					{
						this.rawWriter.WriteXmlDeclaration(standalone);
					}
				}
				else
				{
					this.writer.WriteStartDocument();
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FA0 RID: 4000 RVA: 0x0005BF14 File Offset: 0x0005A114
		private void StartFragment()
		{
			this.conformanceLevel = ConformanceLevel.Fragment;
		}

		// Token: 0x06000FA1 RID: 4001 RVA: 0x0005BF20 File Offset: 0x0005A120
		private void PushNamespaceImplicit(string prefix, string ns)
		{
			int num = this.LookupNamespaceIndex(prefix);
			XmlWellFormedWriter.NamespaceKind kind;
			if (num != -1)
			{
				if (num > this.elemScopeStack[this.elemTop].prevNSTop)
				{
					if (this.nsStack[num].namespaceUri != ns)
					{
						throw new XmlException("The prefix '{0}' cannot be redefined from '{1}' to '{2}' within the same start element tag.", new string[]
						{
							prefix,
							this.nsStack[num].namespaceUri,
							ns
						});
					}
					return;
				}
				else if (this.nsStack[num].kind == XmlWellFormedWriter.NamespaceKind.Special)
				{
					if (!(prefix == "xml"))
					{
						throw new ArgumentException(Res.GetString("Prefix \"xmlns\" is reserved for use by XML."));
					}
					if (ns != this.nsStack[num].namespaceUri)
					{
						throw new ArgumentException(Res.GetString("Prefix \"xml\" is reserved for use by XML and can be mapped only to namespace name \"http://www.w3.org/XML/1998/namespace\"."));
					}
					kind = XmlWellFormedWriter.NamespaceKind.Implied;
				}
				else
				{
					kind = ((this.nsStack[num].namespaceUri == ns) ? XmlWellFormedWriter.NamespaceKind.Implied : XmlWellFormedWriter.NamespaceKind.NeedToWrite);
				}
			}
			else
			{
				if ((ns == "http://www.w3.org/XML/1998/namespace" && prefix != "xml") || (ns == "http://www.w3.org/2000/xmlns/" && prefix != "xmlns"))
				{
					throw new ArgumentException(Res.GetString("Prefix '{0}' cannot be mapped to namespace name reserved for \"xml\" or \"xmlns\".", new object[]
					{
						prefix
					}));
				}
				if (this.predefinedNamespaces != null)
				{
					kind = ((this.predefinedNamespaces.LookupNamespace(prefix) == ns) ? XmlWellFormedWriter.NamespaceKind.Implied : XmlWellFormedWriter.NamespaceKind.NeedToWrite);
				}
				else
				{
					kind = XmlWellFormedWriter.NamespaceKind.NeedToWrite;
				}
			}
			this.AddNamespace(prefix, ns, kind);
		}

		// Token: 0x06000FA2 RID: 4002 RVA: 0x0005C09C File Offset: 0x0005A29C
		private bool PushNamespaceExplicit(string prefix, string ns)
		{
			bool result = true;
			int num = this.LookupNamespaceIndex(prefix);
			if (num != -1)
			{
				if (num > this.elemScopeStack[this.elemTop].prevNSTop)
				{
					if (this.nsStack[num].namespaceUri != ns)
					{
						throw new XmlException("The prefix '{0}' cannot be redefined from '{1}' to '{2}' within the same start element tag.", new string[]
						{
							prefix,
							this.nsStack[num].namespaceUri,
							ns
						});
					}
					XmlWellFormedWriter.NamespaceKind kind = this.nsStack[num].kind;
					if (kind == XmlWellFormedWriter.NamespaceKind.Written)
					{
						throw XmlWellFormedWriter.DupAttrException((prefix.Length == 0) ? string.Empty : "xmlns", (prefix.Length == 0) ? "xmlns" : prefix);
					}
					if (this.omitDuplNamespaces && kind != XmlWellFormedWriter.NamespaceKind.NeedToWrite)
					{
						result = false;
					}
					this.nsStack[num].kind = XmlWellFormedWriter.NamespaceKind.Written;
					return result;
				}
				else if (this.nsStack[num].namespaceUri == ns && this.omitDuplNamespaces)
				{
					result = false;
				}
			}
			else if (this.predefinedNamespaces != null && this.predefinedNamespaces.LookupNamespace(prefix) == ns && this.omitDuplNamespaces)
			{
				result = false;
			}
			if ((ns == "http://www.w3.org/XML/1998/namespace" && prefix != "xml") || (ns == "http://www.w3.org/2000/xmlns/" && prefix != "xmlns"))
			{
				throw new ArgumentException(Res.GetString("Prefix '{0}' cannot be mapped to namespace name reserved for \"xml\" or \"xmlns\".", new object[]
				{
					prefix
				}));
			}
			if (prefix.Length > 0 && prefix[0] == 'x')
			{
				if (prefix == "xml")
				{
					if (ns != "http://www.w3.org/XML/1998/namespace")
					{
						throw new ArgumentException(Res.GetString("Prefix \"xml\" is reserved for use by XML and can be mapped only to namespace name \"http://www.w3.org/XML/1998/namespace\"."));
					}
				}
				else if (prefix == "xmlns")
				{
					throw new ArgumentException(Res.GetString("Prefix \"xmlns\" is reserved for use by XML."));
				}
			}
			this.AddNamespace(prefix, ns, XmlWellFormedWriter.NamespaceKind.Written);
			return result;
		}

		// Token: 0x06000FA3 RID: 4003 RVA: 0x0005C27C File Offset: 0x0005A47C
		private void AddNamespace(string prefix, string ns, XmlWellFormedWriter.NamespaceKind kind)
		{
			int num = this.nsTop + 1;
			this.nsTop = num;
			int num2 = num;
			if (num2 == this.nsStack.Length)
			{
				XmlWellFormedWriter.Namespace[] destinationArray = new XmlWellFormedWriter.Namespace[num2 * 2];
				Array.Copy(this.nsStack, destinationArray, num2);
				this.nsStack = destinationArray;
			}
			this.nsStack[num2].Set(prefix, ns, kind);
			if (this.useNsHashtable)
			{
				this.AddToNamespaceHashtable(this.nsTop);
				return;
			}
			if (this.nsTop == 16)
			{
				this.nsHashtable = new Dictionary<string, int>(this.hasher);
				for (int i = 0; i <= this.nsTop; i++)
				{
					this.AddToNamespaceHashtable(i);
				}
				this.useNsHashtable = true;
			}
		}

		// Token: 0x06000FA4 RID: 4004 RVA: 0x0005C328 File Offset: 0x0005A528
		private void AddToNamespaceHashtable(int namespaceIndex)
		{
			string prefix = this.nsStack[namespaceIndex].prefix;
			int prevNsIndex;
			if (this.nsHashtable.TryGetValue(prefix, out prevNsIndex))
			{
				this.nsStack[namespaceIndex].prevNsIndex = prevNsIndex;
			}
			this.nsHashtable[prefix] = namespaceIndex;
		}

		// Token: 0x06000FA5 RID: 4005 RVA: 0x0005C378 File Offset: 0x0005A578
		private int LookupNamespaceIndex(string prefix)
		{
			if (this.useNsHashtable)
			{
				int result;
				if (this.nsHashtable.TryGetValue(prefix, out result))
				{
					return result;
				}
			}
			else
			{
				for (int i = this.nsTop; i >= 0; i--)
				{
					if (this.nsStack[i].prefix == prefix)
					{
						return i;
					}
				}
			}
			return -1;
		}

		// Token: 0x06000FA6 RID: 4006 RVA: 0x0005C3CC File Offset: 0x0005A5CC
		private void PopNamespaces(int indexFrom, int indexTo)
		{
			for (int i = indexTo; i >= indexFrom; i--)
			{
				if (this.nsStack[i].prevNsIndex == -1)
				{
					this.nsHashtable.Remove(this.nsStack[i].prefix);
				}
				else
				{
					this.nsHashtable[this.nsStack[i].prefix] = this.nsStack[i].prevNsIndex;
				}
			}
		}

		// Token: 0x06000FA7 RID: 4007 RVA: 0x0005C448 File Offset: 0x0005A648
		private static XmlException DupAttrException(string prefix, string localName)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (prefix.Length > 0)
			{
				stringBuilder.Append(prefix);
				stringBuilder.Append(':');
			}
			stringBuilder.Append(localName);
			return new XmlException("'{0}' is a duplicate attribute name.", stringBuilder.ToString());
		}

		// Token: 0x06000FA8 RID: 4008 RVA: 0x0005C490 File Offset: 0x0005A690
		private void AdvanceState(XmlWellFormedWriter.Token token)
		{
			if (this.currentState < XmlWellFormedWriter.State.Closed)
			{
				XmlWellFormedWriter.State state;
				for (;;)
				{
					state = this.stateTable[(int)(((int)token << 4) + (int)this.currentState)];
					if (state < XmlWellFormedWriter.State.Error)
					{
						break;
					}
					if (state != XmlWellFormedWriter.State.Error)
					{
						switch (state)
						{
						case XmlWellFormedWriter.State.StartContent:
							goto IL_E3;
						case XmlWellFormedWriter.State.StartContentEle:
							goto IL_F0;
						case XmlWellFormedWriter.State.StartContentB64:
							goto IL_FD;
						case XmlWellFormedWriter.State.StartDoc:
							goto IL_10A;
						case XmlWellFormedWriter.State.StartDocEle:
							goto IL_117;
						case XmlWellFormedWriter.State.EndAttrSEle:
							goto IL_124;
						case XmlWellFormedWriter.State.EndAttrEEle:
							goto IL_137;
						case XmlWellFormedWriter.State.EndAttrSCont:
							goto IL_14A;
						case XmlWellFormedWriter.State.EndAttrSAttr:
							goto IL_15D;
						case XmlWellFormedWriter.State.PostB64Cont:
							if (this.rawWriter != null)
							{
								this.rawWriter.WriteEndBase64();
							}
							this.currentState = XmlWellFormedWriter.State.Content;
							continue;
						case XmlWellFormedWriter.State.PostB64Attr:
							if (this.rawWriter != null)
							{
								this.rawWriter.WriteEndBase64();
							}
							this.currentState = XmlWellFormedWriter.State.Attribute;
							continue;
						case XmlWellFormedWriter.State.PostB64RootAttr:
							if (this.rawWriter != null)
							{
								this.rawWriter.WriteEndBase64();
							}
							this.currentState = XmlWellFormedWriter.State.RootLevelAttr;
							continue;
						case XmlWellFormedWriter.State.StartFragEle:
							goto IL_1C8;
						case XmlWellFormedWriter.State.StartFragCont:
							goto IL_1D2;
						case XmlWellFormedWriter.State.StartFragB64:
							goto IL_1DC;
						case XmlWellFormedWriter.State.StartRootLevelAttr:
							goto IL_1E6;
						}
						break;
					}
					goto IL_D1;
				}
				goto IL_1EF;
				IL_D1:
				this.ThrowInvalidStateTransition(token, this.currentState);
				goto IL_1EF;
				IL_E3:
				this.StartElementContent();
				state = XmlWellFormedWriter.State.Content;
				goto IL_1EF;
				IL_F0:
				this.StartElementContent();
				state = XmlWellFormedWriter.State.Element;
				goto IL_1EF;
				IL_FD:
				this.StartElementContent();
				state = XmlWellFormedWriter.State.B64Content;
				goto IL_1EF;
				IL_10A:
				this.WriteStartDocument();
				state = XmlWellFormedWriter.State.Document;
				goto IL_1EF;
				IL_117:
				this.WriteStartDocument();
				state = XmlWellFormedWriter.State.Element;
				goto IL_1EF;
				IL_124:
				this.WriteEndAttribute();
				this.StartElementContent();
				state = XmlWellFormedWriter.State.Element;
				goto IL_1EF;
				IL_137:
				this.WriteEndAttribute();
				this.StartElementContent();
				state = XmlWellFormedWriter.State.Content;
				goto IL_1EF;
				IL_14A:
				this.WriteEndAttribute();
				this.StartElementContent();
				state = XmlWellFormedWriter.State.Content;
				goto IL_1EF;
				IL_15D:
				this.WriteEndAttribute();
				state = XmlWellFormedWriter.State.Attribute;
				goto IL_1EF;
				IL_1C8:
				this.StartFragment();
				state = XmlWellFormedWriter.State.Element;
				goto IL_1EF;
				IL_1D2:
				this.StartFragment();
				state = XmlWellFormedWriter.State.Content;
				goto IL_1EF;
				IL_1DC:
				this.StartFragment();
				state = XmlWellFormedWriter.State.B64Content;
				goto IL_1EF;
				IL_1E6:
				this.WriteEndAttribute();
				state = XmlWellFormedWriter.State.RootLevelAttr;
				IL_1EF:
				this.currentState = state;
				return;
			}
			if (this.currentState == XmlWellFormedWriter.State.Closed || this.currentState == XmlWellFormedWriter.State.Error)
			{
				throw new InvalidOperationException(Res.GetString("The Writer is closed or in error state."));
			}
			throw new InvalidOperationException(Res.GetString("Token {0} in state {1} would result in an invalid XML document.", new object[]
			{
				XmlWellFormedWriter.tokenName[(int)token],
				XmlWellFormedWriter.GetStateName(this.currentState)
			}));
		}

		// Token: 0x06000FA9 RID: 4009 RVA: 0x0005C694 File Offset: 0x0005A894
		private void StartElementContent()
		{
			int prevNSTop = this.elemScopeStack[this.elemTop].prevNSTop;
			for (int i = this.nsTop; i > prevNSTop; i--)
			{
				if (this.nsStack[i].kind == XmlWellFormedWriter.NamespaceKind.NeedToWrite)
				{
					this.nsStack[i].WriteDecl(this.writer, this.rawWriter);
				}
			}
			if (this.rawWriter != null)
			{
				this.rawWriter.StartElementContent();
			}
		}

		// Token: 0x06000FAA RID: 4010 RVA: 0x0005C70D File Offset: 0x0005A90D
		private static string GetStateName(XmlWellFormedWriter.State state)
		{
			if (state >= XmlWellFormedWriter.State.Error)
			{
				return "Error";
			}
			return XmlWellFormedWriter.stateName[(int)state];
		}

		// Token: 0x06000FAB RID: 4011 RVA: 0x0005C724 File Offset: 0x0005A924
		internal string LookupNamespace(string prefix)
		{
			for (int i = this.nsTop; i >= 0; i--)
			{
				if (this.nsStack[i].prefix == prefix)
				{
					return this.nsStack[i].namespaceUri;
				}
			}
			if (this.predefinedNamespaces == null)
			{
				return null;
			}
			return this.predefinedNamespaces.LookupNamespace(prefix);
		}

		// Token: 0x06000FAC RID: 4012 RVA: 0x0005C784 File Offset: 0x0005A984
		private string LookupLocalNamespace(string prefix)
		{
			for (int i = this.nsTop; i > this.elemScopeStack[this.elemTop].prevNSTop; i--)
			{
				if (this.nsStack[i].prefix == prefix)
				{
					return this.nsStack[i].namespaceUri;
				}
			}
			return null;
		}

		// Token: 0x06000FAD RID: 4013 RVA: 0x0005C7E4 File Offset: 0x0005A9E4
		private string GeneratePrefix()
		{
			string text = "p" + (this.nsTop - 2).ToString("d", CultureInfo.InvariantCulture);
			if (this.LookupNamespace(text) == null)
			{
				return text;
			}
			int num = 0;
			string text2;
			do
			{
				text2 = text + num.ToString(CultureInfo.InvariantCulture);
				num++;
			}
			while (this.LookupNamespace(text2) != null);
			return text2;
		}

		// Token: 0x06000FAE RID: 4014 RVA: 0x0005C844 File Offset: 0x0005AA44
		private void CheckNCName(string ncname)
		{
			int length = ncname.Length;
			if ((this.xmlCharType.charProperties[(int)ncname[0]] & 4) != 0)
			{
				for (int i = 1; i < length; i++)
				{
					if ((this.xmlCharType.charProperties[(int)ncname[i]] & 8) == 0)
					{
						throw XmlWellFormedWriter.InvalidCharsException(ncname, i);
					}
				}
				return;
			}
			throw XmlWellFormedWriter.InvalidCharsException(ncname, 0);
		}

		// Token: 0x06000FAF RID: 4015 RVA: 0x0005C8A4 File Offset: 0x0005AAA4
		private static Exception InvalidCharsException(string name, int badCharIndex)
		{
			string[] array = XmlException.BuildCharExceptionArgs(name, badCharIndex);
			return new ArgumentException(Res.GetString("Invalid name character in '{0}'. The '{1}' character, hexadecimal value {2}, cannot be included in a name.", new string[]
			{
				name,
				array[0],
				array[1]
			}));
		}

		// Token: 0x06000FB0 RID: 4016 RVA: 0x0005C8E0 File Offset: 0x0005AAE0
		private void ThrowInvalidStateTransition(XmlWellFormedWriter.Token token, XmlWellFormedWriter.State currentState)
		{
			string @string = Res.GetString("Token {0} in state {1} would result in an invalid XML document.", new object[]
			{
				XmlWellFormedWriter.tokenName[(int)token],
				XmlWellFormedWriter.GetStateName(currentState)
			});
			if ((currentState == XmlWellFormedWriter.State.Start || currentState == XmlWellFormedWriter.State.AfterRootEle) && this.conformanceLevel == ConformanceLevel.Document)
			{
				throw new InvalidOperationException(@string + " " + Res.GetString("Make sure that the ConformanceLevel setting is set to ConformanceLevel.Fragment or ConformanceLevel.Auto if you want to write an XML fragment."));
			}
			throw new InvalidOperationException(@string);
		}

		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x06000FB1 RID: 4017 RVA: 0x0005C942 File Offset: 0x0005AB42
		private bool IsClosedOrErrorState
		{
			get
			{
				return this.currentState >= XmlWellFormedWriter.State.Closed;
			}
		}

		// Token: 0x06000FB2 RID: 4018 RVA: 0x0005C954 File Offset: 0x0005AB54
		private void AddAttribute(string prefix, string localName, string namespaceName)
		{
			int num = this.attrCount;
			this.attrCount = num + 1;
			int num2 = num;
			if (num2 == this.attrStack.Length)
			{
				XmlWellFormedWriter.AttrName[] destinationArray = new XmlWellFormedWriter.AttrName[num2 * 2];
				Array.Copy(this.attrStack, destinationArray, num2);
				this.attrStack = destinationArray;
			}
			this.attrStack[num2].Set(prefix, localName, namespaceName);
			if (this.attrCount < 14)
			{
				for (int i = 0; i < num2; i++)
				{
					if (this.attrStack[i].IsDuplicate(prefix, localName, namespaceName))
					{
						throw XmlWellFormedWriter.DupAttrException(prefix, localName);
					}
				}
				return;
			}
			if (this.attrCount == 14)
			{
				if (this.attrHashTable == null)
				{
					this.attrHashTable = new Dictionary<string, int>(this.hasher);
				}
				for (int j = 0; j < num2; j++)
				{
					this.AddToAttrHashTable(j);
				}
			}
			this.AddToAttrHashTable(num2);
			for (int k = this.attrStack[num2].prev; k > 0; k = this.attrStack[k].prev)
			{
				k--;
				if (this.attrStack[k].IsDuplicate(prefix, localName, namespaceName))
				{
					throw XmlWellFormedWriter.DupAttrException(prefix, localName);
				}
			}
		}

		// Token: 0x06000FB3 RID: 4019 RVA: 0x0005CA7C File Offset: 0x0005AC7C
		private void AddToAttrHashTable(int attributeIndex)
		{
			string localName = this.attrStack[attributeIndex].localName;
			int count = this.attrHashTable.Count;
			this.attrHashTable[localName] = 0;
			if (count != this.attrHashTable.Count)
			{
				return;
			}
			int num = attributeIndex - 1;
			while (num >= 0 && !(this.attrStack[num].localName == localName))
			{
				num--;
			}
			this.attrStack[attributeIndex].prev = num + 1;
		}

		// Token: 0x06000FB4 RID: 4020 RVA: 0x0005CAFC File Offset: 0x0005ACFC
		public override Task WriteStartDocumentAsync()
		{
			return this.WriteStartDocumentImplAsync(XmlStandalone.Omit);
		}

		// Token: 0x06000FB5 RID: 4021 RVA: 0x0005CB05 File Offset: 0x0005AD05
		public override Task WriteStartDocumentAsync(bool standalone)
		{
			return this.WriteStartDocumentImplAsync(standalone ? XmlStandalone.Yes : XmlStandalone.No);
		}

		// Token: 0x06000FB6 RID: 4022 RVA: 0x0005CB14 File Offset: 0x0005AD14
		public override async Task WriteEndDocumentAsync()
		{
			try
			{
				while (this.elemTop > 0)
				{
					await this.WriteEndElementAsync().ConfigureAwait(false);
				}
				XmlWellFormedWriter.State prevState = this.currentState;
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.EndDocument).ConfigureAwait(false);
				if (prevState != XmlWellFormedWriter.State.AfterRootEle)
				{
					throw new ArgumentException(Res.GetString("Document does not have a root element."));
				}
				if (this.rawWriter == null)
				{
					await this.writer.WriteEndDocumentAsync().ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FB7 RID: 4023 RVA: 0x0005CB5C File Offset: 0x0005AD5C
		public override async Task WriteDocTypeAsync(string name, string pubid, string sysid, string subset)
		{
			try
			{
				if (name == null || name.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
				}
				XmlConvert.VerifyQName(name, ExceptionType.XmlException);
				if (this.conformanceLevel == ConformanceLevel.Fragment)
				{
					throw new InvalidOperationException(Res.GetString("DTD is not allowed in XML fragments."));
				}
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Dtd).ConfigureAwait(false);
				if (this.dtdWritten)
				{
					this.currentState = XmlWellFormedWriter.State.Error;
					throw new InvalidOperationException(Res.GetString("The DTD has already been written out."));
				}
				if (this.conformanceLevel == ConformanceLevel.Auto)
				{
					this.conformanceLevel = ConformanceLevel.Document;
					this.stateTable = XmlWellFormedWriter.StateTableDocument;
				}
				if (this.checkCharacters)
				{
					int invCharIndex;
					if (pubid != null && (invCharIndex = this.xmlCharType.IsPublicId(pubid)) >= 0)
					{
						throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(pubid, invCharIndex)), "pubid");
					}
					if (sysid != null && (invCharIndex = this.xmlCharType.IsOnlyCharData(sysid)) >= 0)
					{
						throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(sysid, invCharIndex)), "sysid");
					}
					if (subset != null && (invCharIndex = this.xmlCharType.IsOnlyCharData(subset)) >= 0)
					{
						throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(subset, invCharIndex)), "subset");
					}
				}
				await this.writer.WriteDocTypeAsync(name, pubid, sysid, subset).ConfigureAwait(false);
				this.dtdWritten = true;
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FB8 RID: 4024 RVA: 0x0005CBC2 File Offset: 0x0005ADC2
		private Task TryReturnTask(Task task)
		{
			if (task.IsSuccess())
			{
				return AsyncHelper.DoneTask;
			}
			return this._TryReturnTask(task);
		}

		// Token: 0x06000FB9 RID: 4025 RVA: 0x0005CBDC File Offset: 0x0005ADDC
		private async Task _TryReturnTask(Task task)
		{
			try
			{
				await task.ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x0005CC29 File Offset: 0x0005AE29
		private Task SequenceRun(Task task, Func<Task> nextTaskFun)
		{
			if (task.IsSuccess())
			{
				return this.TryReturnTask(nextTaskFun());
			}
			return this._SequenceRun(task, nextTaskFun);
		}

		// Token: 0x06000FBB RID: 4027 RVA: 0x0005CC48 File Offset: 0x0005AE48
		private async Task _SequenceRun(Task task, Func<Task> nextTaskFun)
		{
			try
			{
				await task.ConfigureAwait(false);
				await nextTaskFun().ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FBC RID: 4028 RVA: 0x0005CCA0 File Offset: 0x0005AEA0
		public override Task WriteStartElementAsync(string prefix, string localName, string ns)
		{
			Task result;
			try
			{
				if (localName == null || localName.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid local name."));
				}
				this.CheckNCName(localName);
				Task task = this.AdvanceStateAsync(XmlWellFormedWriter.Token.StartElement);
				if (task.IsSuccess())
				{
					result = this.WriteStartElementAsync_NoAdvanceState(prefix, localName, ns);
				}
				else
				{
					result = this.WriteStartElementAsync_NoAdvanceState(task, prefix, localName, ns);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FBD RID: 4029 RVA: 0x0005CD14 File Offset: 0x0005AF14
		private Task WriteStartElementAsync_NoAdvanceState(string prefix, string localName, string ns)
		{
			Task result;
			try
			{
				if (prefix == null)
				{
					if (ns != null)
					{
						prefix = this.LookupPrefix(ns);
					}
					if (prefix == null)
					{
						prefix = string.Empty;
					}
				}
				else if (prefix.Length > 0)
				{
					this.CheckNCName(prefix);
					if (ns == null)
					{
						ns = this.LookupNamespace(prefix);
					}
					if (ns == null || (ns != null && ns.Length == 0))
					{
						throw new ArgumentException(Res.GetString("Cannot use a prefix with an empty namespace."));
					}
				}
				if (ns == null)
				{
					ns = this.LookupNamespace(prefix);
					if (ns == null)
					{
						ns = string.Empty;
					}
				}
				if (this.elemTop == 0 && this.rawWriter != null)
				{
					this.rawWriter.OnRootElement(this.conformanceLevel);
				}
				Task task = this.writer.WriteStartElementAsync(prefix, localName, ns);
				if (task.IsSuccess())
				{
					this.WriteStartElementAsync_FinishWrite(prefix, localName, ns);
					result = AsyncHelper.DoneTask;
				}
				else
				{
					result = this.WriteStartElementAsync_FinishWrite(task, prefix, localName, ns);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FBE RID: 4030 RVA: 0x0005CE00 File Offset: 0x0005B000
		private async Task WriteStartElementAsync_NoAdvanceState(Task task, string prefix, string localName, string ns)
		{
			try
			{
				await task.ConfigureAwait(false);
				await this.WriteStartElementAsync_NoAdvanceState(prefix, localName, ns).ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FBF RID: 4031 RVA: 0x0005CE68 File Offset: 0x0005B068
		private void WriteStartElementAsync_FinishWrite(string prefix, string localName, string ns)
		{
			try
			{
				int num = this.elemTop + 1;
				this.elemTop = num;
				int num2 = num;
				if (num2 == this.elemScopeStack.Length)
				{
					XmlWellFormedWriter.ElementScope[] destinationArray = new XmlWellFormedWriter.ElementScope[num2 * 2];
					Array.Copy(this.elemScopeStack, destinationArray, num2);
					this.elemScopeStack = destinationArray;
				}
				this.elemScopeStack[num2].Set(prefix, localName, ns, this.nsTop);
				this.PushNamespaceImplicit(prefix, ns);
				if (this.attrCount >= 14)
				{
					this.attrHashTable.Clear();
				}
				this.attrCount = 0;
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FC0 RID: 4032 RVA: 0x0005CF0C File Offset: 0x0005B10C
		private async Task WriteStartElementAsync_FinishWrite(Task t, string prefix, string localName, string ns)
		{
			try
			{
				await t.ConfigureAwait(false);
				this.WriteStartElementAsync_FinishWrite(prefix, localName, ns);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FC1 RID: 4033 RVA: 0x0005CF74 File Offset: 0x0005B174
		public override Task WriteEndElementAsync()
		{
			Task result;
			try
			{
				Task task = this.AdvanceStateAsync(XmlWellFormedWriter.Token.EndElement);
				result = this.SequenceRun(task, new Func<Task>(this.WriteEndElementAsync_NoAdvanceState));
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FC2 RID: 4034 RVA: 0x0005CFBC File Offset: 0x0005B1BC
		private Task WriteEndElementAsync_NoAdvanceState()
		{
			Task result;
			try
			{
				int num = this.elemTop;
				if (num == 0)
				{
					throw new XmlException("There was no XML start tag open.", string.Empty);
				}
				Task task;
				if (this.rawWriter != null)
				{
					task = this.elemScopeStack[num].WriteEndElementAsync(this.rawWriter);
				}
				else
				{
					task = this.writer.WriteEndElementAsync();
				}
				result = this.SequenceRun(task, new Func<Task>(this.WriteEndElementAsync_FinishWrite));
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FC3 RID: 4035 RVA: 0x0005D044 File Offset: 0x0005B244
		private Task WriteEndElementAsync_FinishWrite()
		{
			try
			{
				int num = this.elemTop;
				int prevNSTop = this.elemScopeStack[num].prevNSTop;
				if (this.useNsHashtable && prevNSTop < this.nsTop)
				{
					this.PopNamespaces(prevNSTop + 1, this.nsTop);
				}
				this.nsTop = prevNSTop;
				if ((this.elemTop = num - 1) == 0)
				{
					if (this.conformanceLevel == ConformanceLevel.Document)
					{
						this.currentState = XmlWellFormedWriter.State.AfterRootEle;
					}
					else
					{
						this.currentState = XmlWellFormedWriter.State.TopLevel;
					}
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000FC4 RID: 4036 RVA: 0x0005D0DC File Offset: 0x0005B2DC
		public override Task WriteFullEndElementAsync()
		{
			Task result;
			try
			{
				Task task = this.AdvanceStateAsync(XmlWellFormedWriter.Token.EndElement);
				result = this.SequenceRun(task, new Func<Task>(this.WriteFullEndElementAsync_NoAdvanceState));
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FC5 RID: 4037 RVA: 0x0005D124 File Offset: 0x0005B324
		private Task WriteFullEndElementAsync_NoAdvanceState()
		{
			Task result;
			try
			{
				int num = this.elemTop;
				if (num == 0)
				{
					throw new XmlException("There was no XML start tag open.", string.Empty);
				}
				Task task;
				if (this.rawWriter != null)
				{
					task = this.elemScopeStack[num].WriteFullEndElementAsync(this.rawWriter);
				}
				else
				{
					task = this.writer.WriteFullEndElementAsync();
				}
				result = this.SequenceRun(task, new Func<Task>(this.WriteEndElementAsync_FinishWrite));
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FC6 RID: 4038 RVA: 0x0005D1AC File Offset: 0x0005B3AC
		protected internal override Task WriteStartAttributeAsync(string prefix, string localName, string namespaceName)
		{
			Task result;
			try
			{
				if (localName == null || localName.Length == 0)
				{
					if (!(prefix == "xmlns"))
					{
						throw new ArgumentException(Res.GetString("The empty string '' is not a valid local name."));
					}
					localName = "xmlns";
					prefix = string.Empty;
				}
				this.CheckNCName(localName);
				Task task = this.AdvanceStateAsync(XmlWellFormedWriter.Token.StartAttribute);
				if (task.IsSuccess())
				{
					result = this.WriteStartAttributeAsync_NoAdvanceState(prefix, localName, namespaceName);
				}
				else
				{
					result = this.WriteStartAttributeAsync_NoAdvanceState(task, prefix, localName, namespaceName);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FC7 RID: 4039 RVA: 0x0005D240 File Offset: 0x0005B440
		private Task WriteStartAttributeAsync_NoAdvanceState(string prefix, string localName, string namespaceName)
		{
			Task result;
			try
			{
				if (prefix == null)
				{
					if (namespaceName != null && (!(localName == "xmlns") || !(namespaceName == "http://www.w3.org/2000/xmlns/")))
					{
						prefix = this.LookupPrefix(namespaceName);
					}
					if (prefix == null)
					{
						prefix = string.Empty;
					}
				}
				if (namespaceName == null)
				{
					if (prefix != null && prefix.Length > 0)
					{
						namespaceName = this.LookupNamespace(prefix);
					}
					if (namespaceName == null)
					{
						namespaceName = string.Empty;
					}
				}
				if (prefix.Length == 0)
				{
					if (localName[0] == 'x' && localName == "xmlns")
					{
						if (namespaceName.Length > 0 && namespaceName != "http://www.w3.org/2000/xmlns/")
						{
							throw new ArgumentException(Res.GetString("Prefix \"xmlns\" is reserved for use by XML."));
						}
						this.curDeclPrefix = string.Empty;
						this.SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute.DefaultXmlns);
						goto IL_1DE;
					}
					else if (namespaceName.Length > 0)
					{
						prefix = this.LookupPrefix(namespaceName);
						if (prefix == null || prefix.Length == 0)
						{
							prefix = this.GeneratePrefix();
						}
					}
				}
				else
				{
					if (prefix[0] == 'x')
					{
						if (prefix == "xmlns")
						{
							if (namespaceName.Length > 0 && namespaceName != "http://www.w3.org/2000/xmlns/")
							{
								throw new ArgumentException(Res.GetString("Prefix \"xmlns\" is reserved for use by XML."));
							}
							this.curDeclPrefix = localName;
							this.SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute.PrefixedXmlns);
							goto IL_1DE;
						}
						else if (prefix == "xml")
						{
							if (namespaceName.Length > 0 && namespaceName != "http://www.w3.org/XML/1998/namespace")
							{
								throw new ArgumentException(Res.GetString("Prefix \"xml\" is reserved for use by XML and can be mapped only to namespace name \"http://www.w3.org/XML/1998/namespace\"."));
							}
							if (localName == "space")
							{
								this.SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute.XmlSpace);
								goto IL_1DE;
							}
							if (localName == "lang")
							{
								this.SetSpecialAttribute(XmlWellFormedWriter.SpecialAttribute.XmlLang);
								goto IL_1DE;
							}
						}
					}
					this.CheckNCName(prefix);
					if (namespaceName.Length == 0)
					{
						prefix = string.Empty;
					}
					else
					{
						string text = this.LookupLocalNamespace(prefix);
						if (text != null && text != namespaceName)
						{
							prefix = this.GeneratePrefix();
						}
					}
				}
				if (prefix.Length != 0)
				{
					this.PushNamespaceImplicit(prefix, namespaceName);
				}
				IL_1DE:
				this.AddAttribute(prefix, localName, namespaceName);
				if (this.specAttr == XmlWellFormedWriter.SpecialAttribute.No)
				{
					result = this.TryReturnTask(this.writer.WriteStartAttributeAsync(prefix, localName, namespaceName));
				}
				else
				{
					result = AsyncHelper.DoneTask;
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FC8 RID: 4040 RVA: 0x0005D484 File Offset: 0x0005B684
		private async Task WriteStartAttributeAsync_NoAdvanceState(Task task, string prefix, string localName, string namespaceName)
		{
			try
			{
				await task.ConfigureAwait(false);
				await this.WriteStartAttributeAsync_NoAdvanceState(prefix, localName, namespaceName).ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FC9 RID: 4041 RVA: 0x0005D4EC File Offset: 0x0005B6EC
		protected internal override Task WriteEndAttributeAsync()
		{
			Task result;
			try
			{
				Task task = this.AdvanceStateAsync(XmlWellFormedWriter.Token.EndAttribute);
				result = this.SequenceRun(task, new Func<Task>(this.WriteEndAttributeAsync_NoAdvance));
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FCA RID: 4042 RVA: 0x0005D534 File Offset: 0x0005B734
		private Task WriteEndAttributeAsync_NoAdvance()
		{
			Task result;
			try
			{
				if (this.specAttr != XmlWellFormedWriter.SpecialAttribute.No)
				{
					result = this.WriteEndAttributeAsync_SepcialAtt();
				}
				else
				{
					result = this.TryReturnTask(this.writer.WriteEndAttributeAsync());
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FCB RID: 4043 RVA: 0x0005D584 File Offset: 0x0005B784
		private async Task WriteEndAttributeAsync_SepcialAtt()
		{
			try
			{
				string value;
				switch (this.specAttr)
				{
				case XmlWellFormedWriter.SpecialAttribute.DefaultXmlns:
					value = this.attrValueCache.StringValue;
					if (this.PushNamespaceExplicit(string.Empty, value))
					{
						if (this.rawWriter != null)
						{
							if (this.rawWriter.SupportsNamespaceDeclarationInChunks)
							{
								await this.rawWriter.WriteStartNamespaceDeclarationAsync(string.Empty).ConfigureAwait(false);
								await this.attrValueCache.ReplayAsync(this.rawWriter).ConfigureAwait(false);
								await this.rawWriter.WriteEndNamespaceDeclarationAsync().ConfigureAwait(false);
							}
							else
							{
								await this.rawWriter.WriteNamespaceDeclarationAsync(string.Empty, value).ConfigureAwait(false);
							}
						}
						else
						{
							await this.writer.WriteStartAttributeAsync(string.Empty, "xmlns", "http://www.w3.org/2000/xmlns/").ConfigureAwait(false);
							await this.attrValueCache.ReplayAsync(this.writer).ConfigureAwait(false);
							await this.writer.WriteEndAttributeAsync().ConfigureAwait(false);
						}
					}
					this.curDeclPrefix = null;
					break;
				case XmlWellFormedWriter.SpecialAttribute.PrefixedXmlns:
					value = this.attrValueCache.StringValue;
					if (value.Length == 0)
					{
						throw new ArgumentException(Res.GetString("Cannot use a prefix with an empty namespace."));
					}
					if (value == "http://www.w3.org/2000/xmlns/" || (value == "http://www.w3.org/XML/1998/namespace" && this.curDeclPrefix != "xml"))
					{
						throw new ArgumentException(Res.GetString("Cannot bind to the reserved namespace."));
					}
					if (this.PushNamespaceExplicit(this.curDeclPrefix, value))
					{
						if (this.rawWriter != null)
						{
							if (this.rawWriter.SupportsNamespaceDeclarationInChunks)
							{
								await this.rawWriter.WriteStartNamespaceDeclarationAsync(this.curDeclPrefix).ConfigureAwait(false);
								await this.attrValueCache.ReplayAsync(this.rawWriter).ConfigureAwait(false);
								await this.rawWriter.WriteEndNamespaceDeclarationAsync().ConfigureAwait(false);
							}
							else
							{
								await this.rawWriter.WriteNamespaceDeclarationAsync(this.curDeclPrefix, value).ConfigureAwait(false);
							}
						}
						else
						{
							await this.writer.WriteStartAttributeAsync("xmlns", this.curDeclPrefix, "http://www.w3.org/2000/xmlns/").ConfigureAwait(false);
							await this.attrValueCache.ReplayAsync(this.writer).ConfigureAwait(false);
							await this.writer.WriteEndAttributeAsync().ConfigureAwait(false);
						}
					}
					this.curDeclPrefix = null;
					break;
				case XmlWellFormedWriter.SpecialAttribute.XmlSpace:
					this.attrValueCache.Trim();
					value = this.attrValueCache.StringValue;
					if (value == "default")
					{
						this.elemScopeStack[this.elemTop].xmlSpace = XmlSpace.Default;
					}
					else
					{
						if (!(value == "preserve"))
						{
							throw new ArgumentException(Res.GetString("'{0}' is an invalid xml:space value.", new object[]
							{
								value
							}));
						}
						this.elemScopeStack[this.elemTop].xmlSpace = XmlSpace.Preserve;
					}
					await this.writer.WriteStartAttributeAsync("xml", "space", "http://www.w3.org/XML/1998/namespace").ConfigureAwait(false);
					await this.attrValueCache.ReplayAsync(this.writer).ConfigureAwait(false);
					await this.writer.WriteEndAttributeAsync().ConfigureAwait(false);
					break;
				case XmlWellFormedWriter.SpecialAttribute.XmlLang:
					value = this.attrValueCache.StringValue;
					this.elemScopeStack[this.elemTop].xmlLang = value;
					await this.writer.WriteStartAttributeAsync("xml", "lang", "http://www.w3.org/XML/1998/namespace").ConfigureAwait(false);
					await this.attrValueCache.ReplayAsync(this.writer).ConfigureAwait(false);
					await this.writer.WriteEndAttributeAsync().ConfigureAwait(false);
					break;
				}
				this.specAttr = XmlWellFormedWriter.SpecialAttribute.No;
				this.attrValueCache.Clear();
				value = null;
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FCC RID: 4044 RVA: 0x0005D5CC File Offset: 0x0005B7CC
		public override async Task WriteCDataAsync(string text)
		{
			try
			{
				if (text == null)
				{
					text = string.Empty;
				}
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.CData).ConfigureAwait(false);
				await this.writer.WriteCDataAsync(text).ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FCD RID: 4045 RVA: 0x0005D61C File Offset: 0x0005B81C
		public override async Task WriteCommentAsync(string text)
		{
			try
			{
				if (text == null)
				{
					text = string.Empty;
				}
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Comment).ConfigureAwait(false);
				await this.writer.WriteCommentAsync(text).ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FCE RID: 4046 RVA: 0x0005D66C File Offset: 0x0005B86C
		public override async Task WriteProcessingInstructionAsync(string name, string text)
		{
			try
			{
				if (name == null || name.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
				}
				this.CheckNCName(name);
				if (text == null)
				{
					text = string.Empty;
				}
				if (name.Length == 3 && string.Compare(name, "xml", StringComparison.OrdinalIgnoreCase) == 0)
				{
					if (this.currentState != XmlWellFormedWriter.State.Start)
					{
						throw new ArgumentException(Res.GetString((this.conformanceLevel == ConformanceLevel.Document) ? "Cannot write XML declaration. WriteStartDocument method has already written it." : "Cannot write XML declaration. XML declaration can be only at the beginning of the document."));
					}
					this.xmlDeclFollows = true;
					await this.AdvanceStateAsync(XmlWellFormedWriter.Token.PI).ConfigureAwait(false);
					if (this.rawWriter != null)
					{
						await this.rawWriter.WriteXmlDeclarationAsync(text).ConfigureAwait(false);
					}
					else
					{
						await this.writer.WriteProcessingInstructionAsync(name, text).ConfigureAwait(false);
					}
				}
				else
				{
					await this.AdvanceStateAsync(XmlWellFormedWriter.Token.PI).ConfigureAwait(false);
					await this.writer.WriteProcessingInstructionAsync(name, text).ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FCF RID: 4047 RVA: 0x0005D6C4 File Offset: 0x0005B8C4
		public override async Task WriteEntityRefAsync(string name)
		{
			try
			{
				if (name == null || name.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
				}
				this.CheckNCName(name);
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteEntityRef(name);
				}
				else
				{
					await this.writer.WriteEntityRefAsync(name).ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FD0 RID: 4048 RVA: 0x0005D714 File Offset: 0x0005B914
		public override async Task WriteCharEntityAsync(char ch)
		{
			try
			{
				if (char.IsSurrogate(ch))
				{
					throw new ArgumentException(Res.GetString("The surrogate pair is invalid. Missing a low surrogate character."));
				}
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteCharEntity(ch);
				}
				else
				{
					await this.writer.WriteCharEntityAsync(ch).ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FD1 RID: 4049 RVA: 0x0005D764 File Offset: 0x0005B964
		public override async Task WriteSurrogateCharEntityAsync(char lowChar, char highChar)
		{
			try
			{
				if (!char.IsSurrogatePair(highChar, lowChar))
				{
					throw XmlConvert.CreateInvalidSurrogatePairException(lowChar, highChar);
				}
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteSurrogateCharEntity(lowChar, highChar);
				}
				else
				{
					await this.writer.WriteSurrogateCharEntityAsync(lowChar, highChar).ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FD2 RID: 4050 RVA: 0x0005D7BC File Offset: 0x0005B9BC
		public override async Task WriteWhitespaceAsync(string ws)
		{
			try
			{
				if (ws == null)
				{
					ws = string.Empty;
				}
				if (!XmlCharType.Instance.IsOnlyWhitespace(ws))
				{
					throw new ArgumentException(Res.GetString("Only white space characters should be used."));
				}
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Whitespace).ConfigureAwait(false);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteWhitespace(ws);
				}
				else
				{
					await this.writer.WriteWhitespaceAsync(ws).ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FD3 RID: 4051 RVA: 0x0005D80C File Offset: 0x0005BA0C
		public override Task WriteStringAsync(string text)
		{
			Task result;
			try
			{
				if (text == null)
				{
					result = AsyncHelper.DoneTask;
				}
				else
				{
					Task task = this.AdvanceStateAsync(XmlWellFormedWriter.Token.Text);
					if (task.IsSuccess())
					{
						result = this.WriteStringAsync_NoAdvanceState(text);
					}
					else
					{
						result = this.WriteStringAsync_NoAdvanceState(task, text);
					}
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FD4 RID: 4052 RVA: 0x0005D868 File Offset: 0x0005BA68
		private Task WriteStringAsync_NoAdvanceState(string text)
		{
			Task result;
			try
			{
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteString(text);
					result = AsyncHelper.DoneTask;
				}
				else
				{
					result = this.TryReturnTask(this.writer.WriteStringAsync(text));
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FD5 RID: 4053 RVA: 0x0005D8C4 File Offset: 0x0005BAC4
		private async Task WriteStringAsync_NoAdvanceState(Task task, string text)
		{
			try
			{
				await task.ConfigureAwait(false);
				await this.WriteStringAsync_NoAdvanceState(text).ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FD6 RID: 4054 RVA: 0x0005D91C File Offset: 0x0005BB1C
		public override async Task WriteCharsAsync(char[] buffer, int index, int count)
		{
			try
			{
				if (buffer == null)
				{
					throw new ArgumentNullException("buffer");
				}
				if (index < 0)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				if (count > buffer.Length - index)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteChars(buffer, index, count);
				}
				else
				{
					await this.writer.WriteCharsAsync(buffer, index, count).ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FD7 RID: 4055 RVA: 0x0005D97C File Offset: 0x0005BB7C
		public override async Task WriteRawAsync(char[] buffer, int index, int count)
		{
			try
			{
				if (buffer == null)
				{
					throw new ArgumentNullException("buffer");
				}
				if (index < 0)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				if (count > buffer.Length - index)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.RawData).ConfigureAwait(false);
				if (this.SaveAttrValue)
				{
					this.attrValueCache.WriteRaw(buffer, index, count);
				}
				else
				{
					await this.writer.WriteRawAsync(buffer, index, count).ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FD8 RID: 4056 RVA: 0x0005D9DC File Offset: 0x0005BBDC
		public override async Task WriteRawAsync(string data)
		{
			try
			{
				if (data != null)
				{
					await this.AdvanceStateAsync(XmlWellFormedWriter.Token.RawData).ConfigureAwait(false);
					if (this.SaveAttrValue)
					{
						this.attrValueCache.WriteRaw(data);
					}
					else
					{
						await this.writer.WriteRawAsync(data).ConfigureAwait(false);
					}
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FD9 RID: 4057 RVA: 0x0005DA2C File Offset: 0x0005BC2C
		public override Task WriteBase64Async(byte[] buffer, int index, int count)
		{
			Task result;
			try
			{
				if (buffer == null)
				{
					throw new ArgumentNullException("buffer");
				}
				if (index < 0)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				if (count < 0)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				if (count > buffer.Length - index)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				Task task = this.AdvanceStateAsync(XmlWellFormedWriter.Token.Base64);
				if (task.IsSuccess())
				{
					result = this.TryReturnTask(this.writer.WriteBase64Async(buffer, index, count));
				}
				else
				{
					result = this.WriteBase64Async_NoAdvanceState(task, buffer, index, count);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000FDA RID: 4058 RVA: 0x0005DACC File Offset: 0x0005BCCC
		private async Task WriteBase64Async_NoAdvanceState(Task task, byte[] buffer, int index, int count)
		{
			try
			{
				await task.ConfigureAwait(false);
				await this.writer.WriteBase64Async(buffer, index, count).ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FDB RID: 4059 RVA: 0x0005DB34 File Offset: 0x0005BD34
		public override async Task FlushAsync()
		{
			try
			{
				await this.writer.FlushAsync().ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FDC RID: 4060 RVA: 0x0005DB7C File Offset: 0x0005BD7C
		public override async Task WriteQualifiedNameAsync(string localName, string ns)
		{
			try
			{
				if (localName == null || localName.Length == 0)
				{
					throw new ArgumentException(Res.GetString("The empty string '' is not a valid local name."));
				}
				this.CheckNCName(localName);
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false);
				string prefix = string.Empty;
				if (ns != null && ns.Length != 0)
				{
					prefix = this.LookupPrefix(ns);
					if (prefix == null)
					{
						if (this.currentState != XmlWellFormedWriter.State.Attribute)
						{
							throw new ArgumentException(Res.GetString("The '{0}' namespace is not defined.", new object[]
							{
								ns
							}));
						}
						prefix = this.GeneratePrefix();
						this.PushNamespaceImplicit(prefix, ns);
					}
				}
				if (this.SaveAttrValue || this.rawWriter == null)
				{
					if (prefix.Length != 0)
					{
						await this.WriteStringAsync(prefix).ConfigureAwait(false);
						await this.WriteStringAsync(":").ConfigureAwait(false);
					}
					await this.WriteStringAsync(localName).ConfigureAwait(false);
				}
				else
				{
					await this.rawWriter.WriteQualifiedNameAsync(prefix, localName, ns).ConfigureAwait(false);
				}
				prefix = null;
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FDD RID: 4061 RVA: 0x0005DBD4 File Offset: 0x0005BDD4
		public override async Task WriteBinHexAsync(byte[] buffer, int index, int count)
		{
			if (this.IsClosedOrErrorState)
			{
				throw new InvalidOperationException(Res.GetString("The Writer is closed or in error state."));
			}
			try
			{
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false);
				await base.WriteBinHexAsync(buffer, index, count).ConfigureAwait(false);
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FDE RID: 4062 RVA: 0x0005DC34 File Offset: 0x0005BE34
		private async Task WriteStartDocumentImplAsync(XmlStandalone standalone)
		{
			try
			{
				await this.AdvanceStateAsync(XmlWellFormedWriter.Token.StartDocument).ConfigureAwait(false);
				if (this.conformanceLevel == ConformanceLevel.Auto)
				{
					this.conformanceLevel = ConformanceLevel.Document;
					this.stateTable = XmlWellFormedWriter.StateTableDocument;
				}
				else if (this.conformanceLevel == ConformanceLevel.Fragment)
				{
					throw new InvalidOperationException(Res.GetString("WriteStartDocument cannot be called on writers created with ConformanceLevel.Fragment."));
				}
				if (this.rawWriter != null)
				{
					if (!this.xmlDeclFollows)
					{
						await this.rawWriter.WriteXmlDeclarationAsync(standalone).ConfigureAwait(false);
					}
				}
				else
				{
					await this.writer.WriteStartDocumentAsync().ConfigureAwait(false);
				}
			}
			catch
			{
				this.currentState = XmlWellFormedWriter.State.Error;
				throw;
			}
		}

		// Token: 0x06000FDF RID: 4063 RVA: 0x0005DC81 File Offset: 0x0005BE81
		private Task AdvanceStateAsync_ReturnWhenFinish(Task task, XmlWellFormedWriter.State newState)
		{
			if (task.IsSuccess())
			{
				this.currentState = newState;
				return AsyncHelper.DoneTask;
			}
			return this._AdvanceStateAsync_ReturnWhenFinish(task, newState);
		}

		// Token: 0x06000FE0 RID: 4064 RVA: 0x0005DCA0 File Offset: 0x0005BEA0
		private async Task _AdvanceStateAsync_ReturnWhenFinish(Task task, XmlWellFormedWriter.State newState)
		{
			await task.ConfigureAwait(false);
			this.currentState = newState;
		}

		// Token: 0x06000FE1 RID: 4065 RVA: 0x0005DCF5 File Offset: 0x0005BEF5
		private Task AdvanceStateAsync_ContinueWhenFinish(Task task, XmlWellFormedWriter.State newState, XmlWellFormedWriter.Token token)
		{
			if (task.IsSuccess())
			{
				this.currentState = newState;
				return this.AdvanceStateAsync(token);
			}
			return this._AdvanceStateAsync_ContinueWhenFinish(task, newState, token);
		}

		// Token: 0x06000FE2 RID: 4066 RVA: 0x0005DD18 File Offset: 0x0005BF18
		private async Task _AdvanceStateAsync_ContinueWhenFinish(Task task, XmlWellFormedWriter.State newState, XmlWellFormedWriter.Token token)
		{
			await task.ConfigureAwait(false);
			this.currentState = newState;
			await this.AdvanceStateAsync(token).ConfigureAwait(false);
		}

		// Token: 0x06000FE3 RID: 4067 RVA: 0x0005DD78 File Offset: 0x0005BF78
		private Task AdvanceStateAsync(XmlWellFormedWriter.Token token)
		{
			if (this.currentState < XmlWellFormedWriter.State.Closed)
			{
				XmlWellFormedWriter.State state;
				for (;;)
				{
					state = this.stateTable[(int)(((int)token << 4) + (int)this.currentState)];
					if (state < XmlWellFormedWriter.State.Error)
					{
						break;
					}
					if (state != XmlWellFormedWriter.State.Error)
					{
						switch (state)
						{
						case XmlWellFormedWriter.State.StartContent:
							goto IL_E3;
						case XmlWellFormedWriter.State.StartContentEle:
							goto IL_F1;
						case XmlWellFormedWriter.State.StartContentB64:
							goto IL_FF;
						case XmlWellFormedWriter.State.StartDoc:
							goto IL_10D;
						case XmlWellFormedWriter.State.StartDocEle:
							goto IL_11B;
						case XmlWellFormedWriter.State.EndAttrSEle:
							goto IL_129;
						case XmlWellFormedWriter.State.EndAttrEEle:
							goto IL_14B;
						case XmlWellFormedWriter.State.EndAttrSCont:
							goto IL_16D;
						case XmlWellFormedWriter.State.EndAttrSAttr:
							goto IL_18F;
						case XmlWellFormedWriter.State.PostB64Cont:
							if (this.rawWriter != null)
							{
								goto Block_6;
							}
							this.currentState = XmlWellFormedWriter.State.Content;
							continue;
						case XmlWellFormedWriter.State.PostB64Attr:
							if (this.rawWriter != null)
							{
								goto Block_7;
							}
							this.currentState = XmlWellFormedWriter.State.Attribute;
							continue;
						case XmlWellFormedWriter.State.PostB64RootAttr:
							if (this.rawWriter != null)
							{
								goto Block_8;
							}
							this.currentState = XmlWellFormedWriter.State.RootLevelAttr;
							continue;
						case XmlWellFormedWriter.State.StartFragEle:
							goto IL_217;
						case XmlWellFormedWriter.State.StartFragCont:
							goto IL_221;
						case XmlWellFormedWriter.State.StartFragB64:
							goto IL_22B;
						case XmlWellFormedWriter.State.StartRootLevelAttr:
							goto IL_235;
						}
						break;
					}
					goto IL_D1;
				}
				goto IL_244;
				IL_D1:
				this.ThrowInvalidStateTransition(token, this.currentState);
				goto IL_244;
				IL_E3:
				return this.AdvanceStateAsync_ReturnWhenFinish(this.StartElementContentAsync(), XmlWellFormedWriter.State.Content);
				IL_F1:
				return this.AdvanceStateAsync_ReturnWhenFinish(this.StartElementContentAsync(), XmlWellFormedWriter.State.Element);
				IL_FF:
				return this.AdvanceStateAsync_ReturnWhenFinish(this.StartElementContentAsync(), XmlWellFormedWriter.State.B64Content);
				IL_10D:
				return this.AdvanceStateAsync_ReturnWhenFinish(this.WriteStartDocumentAsync(), XmlWellFormedWriter.State.Document);
				IL_11B:
				return this.AdvanceStateAsync_ReturnWhenFinish(this.WriteStartDocumentAsync(), XmlWellFormedWriter.State.Element);
				IL_129:
				Task task = this.SequenceRun(this.WriteEndAttributeAsync(), new Func<Task>(this.StartElementContentAsync));
				return this.AdvanceStateAsync_ReturnWhenFinish(task, XmlWellFormedWriter.State.Element);
				IL_14B:
				task = this.SequenceRun(this.WriteEndAttributeAsync(), new Func<Task>(this.StartElementContentAsync));
				return this.AdvanceStateAsync_ReturnWhenFinish(task, XmlWellFormedWriter.State.Content);
				IL_16D:
				task = this.SequenceRun(this.WriteEndAttributeAsync(), new Func<Task>(this.StartElementContentAsync));
				return this.AdvanceStateAsync_ReturnWhenFinish(task, XmlWellFormedWriter.State.Content);
				IL_18F:
				return this.AdvanceStateAsync_ReturnWhenFinish(this.WriteEndAttributeAsync(), XmlWellFormedWriter.State.Attribute);
				Block_6:
				return this.AdvanceStateAsync_ContinueWhenFinish(this.rawWriter.WriteEndBase64Async(), XmlWellFormedWriter.State.Content, token);
				Block_7:
				return this.AdvanceStateAsync_ContinueWhenFinish(this.rawWriter.WriteEndBase64Async(), XmlWellFormedWriter.State.Attribute, token);
				Block_8:
				return this.AdvanceStateAsync_ContinueWhenFinish(this.rawWriter.WriteEndBase64Async(), XmlWellFormedWriter.State.RootLevelAttr, token);
				IL_217:
				this.StartFragment();
				state = XmlWellFormedWriter.State.Element;
				goto IL_244;
				IL_221:
				this.StartFragment();
				state = XmlWellFormedWriter.State.Content;
				goto IL_244;
				IL_22B:
				this.StartFragment();
				state = XmlWellFormedWriter.State.B64Content;
				goto IL_244;
				IL_235:
				return this.AdvanceStateAsync_ReturnWhenFinish(this.WriteEndAttributeAsync(), XmlWellFormedWriter.State.RootLevelAttr);
				IL_244:
				this.currentState = state;
				return AsyncHelper.DoneTask;
			}
			if (this.currentState == XmlWellFormedWriter.State.Closed || this.currentState == XmlWellFormedWriter.State.Error)
			{
				throw new InvalidOperationException(Res.GetString("The Writer is closed or in error state."));
			}
			throw new InvalidOperationException(Res.GetString("Token {0} in state {1} would result in an invalid XML document.", new object[]
			{
				XmlWellFormedWriter.tokenName[(int)token],
				XmlWellFormedWriter.GetStateName(this.currentState)
			}));
		}

		// Token: 0x06000FE4 RID: 4068 RVA: 0x0005DFD8 File Offset: 0x0005C1D8
		private async Task StartElementContentAsync_WithNS()
		{
			int start = this.elemScopeStack[this.elemTop].prevNSTop;
			for (int i = this.nsTop; i > start; i--)
			{
				if (this.nsStack[i].kind == XmlWellFormedWriter.NamespaceKind.NeedToWrite)
				{
					await this.nsStack[i].WriteDeclAsync(this.writer, this.rawWriter).ConfigureAwait(false);
				}
			}
			if (this.rawWriter != null)
			{
				this.rawWriter.StartElementContent();
			}
		}

		// Token: 0x06000FE5 RID: 4069 RVA: 0x0005E01D File Offset: 0x0005C21D
		private Task StartElementContentAsync()
		{
			if (this.nsTop > this.elemScopeStack[this.elemTop].prevNSTop)
			{
				return this.StartElementContentAsync_WithNS();
			}
			if (this.rawWriter != null)
			{
				this.rawWriter.StartElementContent();
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000FE6 RID: 4070 RVA: 0x0005E05C File Offset: 0x0005C25C
		// Note: this type is marked as 'beforefieldinit'.
		static XmlWellFormedWriter()
		{
		}

		// Token: 0x06000FE7 RID: 4071 RVA: 0x0005E1DA File Offset: 0x0005C3DA
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__0(byte[] buffer, int index, int count)
		{
			return base.WriteBinHexAsync(buffer, index, count);
		}

		// Token: 0x04000AAB RID: 2731
		private XmlWriter writer;

		// Token: 0x04000AAC RID: 2732
		private XmlRawWriter rawWriter;

		// Token: 0x04000AAD RID: 2733
		private IXmlNamespaceResolver predefinedNamespaces;

		// Token: 0x04000AAE RID: 2734
		private XmlWellFormedWriter.Namespace[] nsStack;

		// Token: 0x04000AAF RID: 2735
		private int nsTop;

		// Token: 0x04000AB0 RID: 2736
		private Dictionary<string, int> nsHashtable;

		// Token: 0x04000AB1 RID: 2737
		private bool useNsHashtable;

		// Token: 0x04000AB2 RID: 2738
		private XmlWellFormedWriter.ElementScope[] elemScopeStack;

		// Token: 0x04000AB3 RID: 2739
		private int elemTop;

		// Token: 0x04000AB4 RID: 2740
		private XmlWellFormedWriter.AttrName[] attrStack;

		// Token: 0x04000AB5 RID: 2741
		private int attrCount;

		// Token: 0x04000AB6 RID: 2742
		private Dictionary<string, int> attrHashTable;

		// Token: 0x04000AB7 RID: 2743
		private XmlWellFormedWriter.SpecialAttribute specAttr;

		// Token: 0x04000AB8 RID: 2744
		private XmlWellFormedWriter.AttributeValueCache attrValueCache;

		// Token: 0x04000AB9 RID: 2745
		private string curDeclPrefix;

		// Token: 0x04000ABA RID: 2746
		private XmlWellFormedWriter.State[] stateTable;

		// Token: 0x04000ABB RID: 2747
		private XmlWellFormedWriter.State currentState;

		// Token: 0x04000ABC RID: 2748
		private bool checkCharacters;

		// Token: 0x04000ABD RID: 2749
		private bool omitDuplNamespaces;

		// Token: 0x04000ABE RID: 2750
		private bool writeEndDocumentOnClose;

		// Token: 0x04000ABF RID: 2751
		private ConformanceLevel conformanceLevel;

		// Token: 0x04000AC0 RID: 2752
		private bool dtdWritten;

		// Token: 0x04000AC1 RID: 2753
		private bool xmlDeclFollows;

		// Token: 0x04000AC2 RID: 2754
		private XmlCharType xmlCharType = XmlCharType.Instance;

		// Token: 0x04000AC3 RID: 2755
		private SecureStringHasher hasher;

		// Token: 0x04000AC4 RID: 2756
		private const int ElementStackInitialSize = 8;

		// Token: 0x04000AC5 RID: 2757
		private const int NamespaceStackInitialSize = 8;

		// Token: 0x04000AC6 RID: 2758
		private const int AttributeArrayInitialSize = 8;

		// Token: 0x04000AC7 RID: 2759
		private const int MaxAttrDuplWalkCount = 14;

		// Token: 0x04000AC8 RID: 2760
		private const int MaxNamespacesWalkCount = 16;

		// Token: 0x04000AC9 RID: 2761
		internal static readonly string[] stateName = new string[]
		{
			"Start",
			"TopLevel",
			"Document",
			"Element Start Tag",
			"Element Content",
			"Element Content",
			"Attribute",
			"EndRootElement",
			"Attribute",
			"Special Attribute",
			"End Document",
			"Root Level Attribute Value",
			"Root Level Special Attribute Value",
			"Root Level Base64 Attribute Value",
			"After Root Level Attribute",
			"Closed",
			"Error"
		};

		// Token: 0x04000ACA RID: 2762
		internal static readonly string[] tokenName = new string[]
		{
			"StartDocument",
			"EndDocument",
			"PI",
			"Comment",
			"DTD",
			"StartElement",
			"EndElement",
			"StartAttribute",
			"EndAttribute",
			"Text",
			"CDATA",
			"Atomic value",
			"Base64",
			"RawData",
			"Whitespace"
		};

		// Token: 0x04000ACB RID: 2763
		private static WriteState[] state2WriteState = new WriteState[]
		{
			WriteState.Start,
			WriteState.Prolog,
			WriteState.Prolog,
			WriteState.Element,
			WriteState.Content,
			WriteState.Content,
			WriteState.Attribute,
			WriteState.Content,
			WriteState.Attribute,
			WriteState.Attribute,
			WriteState.Content,
			WriteState.Attribute,
			WriteState.Attribute,
			WriteState.Attribute,
			WriteState.Attribute,
			WriteState.Closed,
			WriteState.Error
		};

		// Token: 0x04000ACC RID: 2764
		private static readonly XmlWellFormedWriter.State[] StateTableDocument = new XmlWellFormedWriter.State[]
		{
			XmlWellFormedWriter.State.Document,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.EndDocument,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartDoc,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Document,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.AfterRootEle,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartDoc,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Document,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.AfterRootEle,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartDoc,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Document,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartDocEle,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.StartContentEle,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.EndAttrSEle,
			XmlWellFormedWriter.State.EndAttrSEle,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.EndAttrEEle,
			XmlWellFormedWriter.State.EndAttrEEle,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.EndAttrSAttr,
			XmlWellFormedWriter.State.EndAttrSAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.SpecialAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContentB64,
			XmlWellFormedWriter.State.B64Content,
			XmlWellFormedWriter.State.B64Content,
			XmlWellFormedWriter.State.B64Attribute,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.B64Attribute,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartDoc,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Document,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.AfterRootEle,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.SpecialAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartDoc,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Document,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.AfterRootEle,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.SpecialAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error
		};

		// Token: 0x04000ACD RID: 2765
		private static readonly XmlWellFormedWriter.State[] StateTableAuto = new XmlWellFormedWriter.State[]
		{
			XmlWellFormedWriter.State.Document,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.EndDocument,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.AfterRootEle,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.AfterRootEle,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartDoc,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartFragEle,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContentEle,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.EndAttrSEle,
			XmlWellFormedWriter.State.EndAttrSEle,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.EndAttrEEle,
			XmlWellFormedWriter.State.EndAttrEEle,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.RootLevelAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.EndAttrSAttr,
			XmlWellFormedWriter.State.EndAttrSAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartRootLevelAttr,
			XmlWellFormedWriter.State.StartRootLevelAttr,
			XmlWellFormedWriter.State.PostB64RootAttr,
			XmlWellFormedWriter.State.RootLevelAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.Element,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.AfterRootLevelAttr,
			XmlWellFormedWriter.State.AfterRootLevelAttr,
			XmlWellFormedWriter.State.PostB64RootAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartFragCont,
			XmlWellFormedWriter.State.StartFragCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.SpecialAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.RootLevelAttr,
			XmlWellFormedWriter.State.RootLevelSpecAttr,
			XmlWellFormedWriter.State.PostB64RootAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartFragCont,
			XmlWellFormedWriter.State.StartFragCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.EndAttrSCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartFragCont,
			XmlWellFormedWriter.State.StartFragCont,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.RootLevelAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.PostB64RootAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartFragB64,
			XmlWellFormedWriter.State.StartFragB64,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContentB64,
			XmlWellFormedWriter.State.B64Content,
			XmlWellFormedWriter.State.B64Content,
			XmlWellFormedWriter.State.B64Attribute,
			XmlWellFormedWriter.State.B64Content,
			XmlWellFormedWriter.State.B64Attribute,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.RootLevelB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.RootLevelB64Attr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartFragCont,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.SpecialAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.RootLevelAttr,
			XmlWellFormedWriter.State.RootLevelSpecAttr,
			XmlWellFormedWriter.State.PostB64RootAttr,
			XmlWellFormedWriter.State.AfterRootLevelAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.TopLevel,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.StartContent,
			XmlWellFormedWriter.State.Content,
			XmlWellFormedWriter.State.PostB64Cont,
			XmlWellFormedWriter.State.PostB64Attr,
			XmlWellFormedWriter.State.AfterRootEle,
			XmlWellFormedWriter.State.Attribute,
			XmlWellFormedWriter.State.SpecialAttr,
			XmlWellFormedWriter.State.Error,
			XmlWellFormedWriter.State.RootLevelAttr,
			XmlWellFormedWriter.State.RootLevelSpecAttr,
			XmlWellFormedWriter.State.PostB64RootAttr,
			XmlWellFormedWriter.State.AfterRootLevelAttr,
			XmlWellFormedWriter.State.Error
		};

		// Token: 0x020001B4 RID: 436
		private enum State
		{
			// Token: 0x04000ACF RID: 2767
			Start,
			// Token: 0x04000AD0 RID: 2768
			TopLevel,
			// Token: 0x04000AD1 RID: 2769
			Document,
			// Token: 0x04000AD2 RID: 2770
			Element,
			// Token: 0x04000AD3 RID: 2771
			Content,
			// Token: 0x04000AD4 RID: 2772
			B64Content,
			// Token: 0x04000AD5 RID: 2773
			B64Attribute,
			// Token: 0x04000AD6 RID: 2774
			AfterRootEle,
			// Token: 0x04000AD7 RID: 2775
			Attribute,
			// Token: 0x04000AD8 RID: 2776
			SpecialAttr,
			// Token: 0x04000AD9 RID: 2777
			EndDocument,
			// Token: 0x04000ADA RID: 2778
			RootLevelAttr,
			// Token: 0x04000ADB RID: 2779
			RootLevelSpecAttr,
			// Token: 0x04000ADC RID: 2780
			RootLevelB64Attr,
			// Token: 0x04000ADD RID: 2781
			AfterRootLevelAttr,
			// Token: 0x04000ADE RID: 2782
			Closed,
			// Token: 0x04000ADF RID: 2783
			Error,
			// Token: 0x04000AE0 RID: 2784
			StartContent = 101,
			// Token: 0x04000AE1 RID: 2785
			StartContentEle,
			// Token: 0x04000AE2 RID: 2786
			StartContentB64,
			// Token: 0x04000AE3 RID: 2787
			StartDoc,
			// Token: 0x04000AE4 RID: 2788
			StartDocEle = 106,
			// Token: 0x04000AE5 RID: 2789
			EndAttrSEle,
			// Token: 0x04000AE6 RID: 2790
			EndAttrEEle,
			// Token: 0x04000AE7 RID: 2791
			EndAttrSCont,
			// Token: 0x04000AE8 RID: 2792
			EndAttrSAttr = 111,
			// Token: 0x04000AE9 RID: 2793
			PostB64Cont,
			// Token: 0x04000AEA RID: 2794
			PostB64Attr,
			// Token: 0x04000AEB RID: 2795
			PostB64RootAttr,
			// Token: 0x04000AEC RID: 2796
			StartFragEle,
			// Token: 0x04000AED RID: 2797
			StartFragCont,
			// Token: 0x04000AEE RID: 2798
			StartFragB64,
			// Token: 0x04000AEF RID: 2799
			StartRootLevelAttr
		}

		// Token: 0x020001B5 RID: 437
		private enum Token
		{
			// Token: 0x04000AF1 RID: 2801
			StartDocument,
			// Token: 0x04000AF2 RID: 2802
			EndDocument,
			// Token: 0x04000AF3 RID: 2803
			PI,
			// Token: 0x04000AF4 RID: 2804
			Comment,
			// Token: 0x04000AF5 RID: 2805
			Dtd,
			// Token: 0x04000AF6 RID: 2806
			StartElement,
			// Token: 0x04000AF7 RID: 2807
			EndElement,
			// Token: 0x04000AF8 RID: 2808
			StartAttribute,
			// Token: 0x04000AF9 RID: 2809
			EndAttribute,
			// Token: 0x04000AFA RID: 2810
			Text,
			// Token: 0x04000AFB RID: 2811
			CData,
			// Token: 0x04000AFC RID: 2812
			AtomicValue,
			// Token: 0x04000AFD RID: 2813
			Base64,
			// Token: 0x04000AFE RID: 2814
			RawData,
			// Token: 0x04000AFF RID: 2815
			Whitespace
		}

		// Token: 0x020001B6 RID: 438
		private class NamespaceResolverProxy : IXmlNamespaceResolver
		{
			// Token: 0x06000FE8 RID: 4072 RVA: 0x0005E1E5 File Offset: 0x0005C3E5
			internal NamespaceResolverProxy(XmlWellFormedWriter wfWriter)
			{
				this.wfWriter = wfWriter;
			}

			// Token: 0x06000FE9 RID: 4073 RVA: 0x0000A6CF File Offset: 0x000088CF
			IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
			{
				throw new NotImplementedException();
			}

			// Token: 0x06000FEA RID: 4074 RVA: 0x0005E1F4 File Offset: 0x0005C3F4
			string IXmlNamespaceResolver.LookupNamespace(string prefix)
			{
				return this.wfWriter.LookupNamespace(prefix);
			}

			// Token: 0x06000FEB RID: 4075 RVA: 0x0005E202 File Offset: 0x0005C402
			string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
			{
				return this.wfWriter.LookupPrefix(namespaceName);
			}

			// Token: 0x04000B00 RID: 2816
			private XmlWellFormedWriter wfWriter;
		}

		// Token: 0x020001B7 RID: 439
		private struct ElementScope
		{
			// Token: 0x06000FEC RID: 4076 RVA: 0x0005E210 File Offset: 0x0005C410
			internal void Set(string prefix, string localName, string namespaceUri, int prevNSTop)
			{
				this.prevNSTop = prevNSTop;
				this.prefix = prefix;
				this.namespaceUri = namespaceUri;
				this.localName = localName;
				this.xmlSpace = (XmlSpace)(-1);
				this.xmlLang = null;
			}

			// Token: 0x06000FED RID: 4077 RVA: 0x0005E23D File Offset: 0x0005C43D
			internal void WriteEndElement(XmlRawWriter rawWriter)
			{
				rawWriter.WriteEndElement(this.prefix, this.localName, this.namespaceUri);
			}

			// Token: 0x06000FEE RID: 4078 RVA: 0x0005E257 File Offset: 0x0005C457
			internal void WriteFullEndElement(XmlRawWriter rawWriter)
			{
				rawWriter.WriteFullEndElement(this.prefix, this.localName, this.namespaceUri);
			}

			// Token: 0x06000FEF RID: 4079 RVA: 0x0005E271 File Offset: 0x0005C471
			internal Task WriteEndElementAsync(XmlRawWriter rawWriter)
			{
				return rawWriter.WriteEndElementAsync(this.prefix, this.localName, this.namespaceUri);
			}

			// Token: 0x06000FF0 RID: 4080 RVA: 0x0005E28B File Offset: 0x0005C48B
			internal Task WriteFullEndElementAsync(XmlRawWriter rawWriter)
			{
				return rawWriter.WriteFullEndElementAsync(this.prefix, this.localName, this.namespaceUri);
			}

			// Token: 0x04000B01 RID: 2817
			internal int prevNSTop;

			// Token: 0x04000B02 RID: 2818
			internal string prefix;

			// Token: 0x04000B03 RID: 2819
			internal string localName;

			// Token: 0x04000B04 RID: 2820
			internal string namespaceUri;

			// Token: 0x04000B05 RID: 2821
			internal XmlSpace xmlSpace;

			// Token: 0x04000B06 RID: 2822
			internal string xmlLang;
		}

		// Token: 0x020001B8 RID: 440
		private enum NamespaceKind
		{
			// Token: 0x04000B08 RID: 2824
			Written,
			// Token: 0x04000B09 RID: 2825
			NeedToWrite,
			// Token: 0x04000B0A RID: 2826
			Implied,
			// Token: 0x04000B0B RID: 2827
			Special
		}

		// Token: 0x020001B9 RID: 441
		private struct Namespace
		{
			// Token: 0x06000FF1 RID: 4081 RVA: 0x0005E2A5 File Offset: 0x0005C4A5
			internal void Set(string prefix, string namespaceUri, XmlWellFormedWriter.NamespaceKind kind)
			{
				this.prefix = prefix;
				this.namespaceUri = namespaceUri;
				this.kind = kind;
				this.prevNsIndex = -1;
			}

			// Token: 0x06000FF2 RID: 4082 RVA: 0x0005E2C4 File Offset: 0x0005C4C4
			internal void WriteDecl(XmlWriter writer, XmlRawWriter rawWriter)
			{
				if (rawWriter != null)
				{
					rawWriter.WriteNamespaceDeclaration(this.prefix, this.namespaceUri);
					return;
				}
				if (this.prefix.Length == 0)
				{
					writer.WriteStartAttribute(string.Empty, "xmlns", "http://www.w3.org/2000/xmlns/");
				}
				else
				{
					writer.WriteStartAttribute("xmlns", this.prefix, "http://www.w3.org/2000/xmlns/");
				}
				writer.WriteString(this.namespaceUri);
				writer.WriteEndAttribute();
			}

			// Token: 0x06000FF3 RID: 4083 RVA: 0x0005E334 File Offset: 0x0005C534
			internal async Task WriteDeclAsync(XmlWriter writer, XmlRawWriter rawWriter)
			{
				if (rawWriter != null)
				{
					await rawWriter.WriteNamespaceDeclarationAsync(this.prefix, this.namespaceUri).ConfigureAwait(false);
				}
				else
				{
					if (this.prefix.Length == 0)
					{
						await writer.WriteStartAttributeAsync(string.Empty, "xmlns", "http://www.w3.org/2000/xmlns/").ConfigureAwait(false);
					}
					else
					{
						await writer.WriteStartAttributeAsync("xmlns", this.prefix, "http://www.w3.org/2000/xmlns/").ConfigureAwait(false);
					}
					await writer.WriteStringAsync(this.namespaceUri).ConfigureAwait(false);
					await writer.WriteEndAttributeAsync().ConfigureAwait(false);
				}
			}

			// Token: 0x04000B0C RID: 2828
			internal string prefix;

			// Token: 0x04000B0D RID: 2829
			internal string namespaceUri;

			// Token: 0x04000B0E RID: 2830
			internal XmlWellFormedWriter.NamespaceKind kind;

			// Token: 0x04000B0F RID: 2831
			internal int prevNsIndex;

			// Token: 0x020001BA RID: 442
			[CompilerGenerated]
			[StructLayout(LayoutKind.Auto)]
			private struct <WriteDeclAsync>d__6 : IAsyncStateMachine
			{
				// Token: 0x06000FF4 RID: 4084 RVA: 0x0005E390 File Offset: 0x0005C590
				void IAsyncStateMachine.MoveNext()
				{
					int num2;
					int num = num2;
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_133;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_1B3;
						}
						case 3:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_227;
						}
						case 4:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_28D;
						}
						default:
							if (rawWriter != null)
							{
								configuredTaskAwaiter = rawWriter.WriteNamespaceDeclarationAsync(this.prefix, this.namespaceUri).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.Namespace.<WriteDeclAsync>d__6>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
							else if (this.prefix.Length == 0)
							{
								configuredTaskAwaiter = writer.WriteStartAttributeAsync(string.Empty, "xmlns", "http://www.w3.org/2000/xmlns/").ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 1;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.Namespace.<WriteDeclAsync>d__6>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_133;
							}
							else
							{
								configuredTaskAwaiter = writer.WriteStartAttributeAsync("xmlns", this.prefix, "http://www.w3.org/2000/xmlns/").ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 2;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.Namespace.<WriteDeclAsync>d__6>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_1B3;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						goto IL_294;
						IL_133:
						configuredTaskAwaiter.GetResult();
						goto IL_1BA;
						IL_1B3:
						configuredTaskAwaiter.GetResult();
						IL_1BA:
						configuredTaskAwaiter = writer.WriteStringAsync(this.namespaceUri).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.Namespace.<WriteDeclAsync>d__6>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_227:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = writer.WriteEndAttributeAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 4;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.Namespace.<WriteDeclAsync>d__6>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_28D:
						configuredTaskAwaiter.GetResult();
						IL_294:;
					}
					catch (Exception exception)
					{
						num2 = -2;
						this.<>t__builder.SetException(exception);
						return;
					}
					num2 = -2;
					this.<>t__builder.SetResult();
				}

				// Token: 0x06000FF5 RID: 4085 RVA: 0x0005E67C File Offset: 0x0005C87C
				[DebuggerHidden]
				void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
				{
					this.<>t__builder.SetStateMachine(stateMachine);
				}

				// Token: 0x04000B10 RID: 2832
				public int <>1__state;

				// Token: 0x04000B11 RID: 2833
				public AsyncTaskMethodBuilder <>t__builder;

				// Token: 0x04000B12 RID: 2834
				public XmlRawWriter rawWriter;

				// Token: 0x04000B13 RID: 2835
				public XmlWellFormedWriter.Namespace <>4__this;

				// Token: 0x04000B14 RID: 2836
				public XmlWriter writer;

				// Token: 0x04000B15 RID: 2837
				private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
			}
		}

		// Token: 0x020001BB RID: 443
		private struct AttrName
		{
			// Token: 0x06000FF6 RID: 4086 RVA: 0x0005E68A File Offset: 0x0005C88A
			internal void Set(string prefix, string localName, string namespaceUri)
			{
				this.prefix = prefix;
				this.namespaceUri = namespaceUri;
				this.localName = localName;
				this.prev = 0;
			}

			// Token: 0x06000FF7 RID: 4087 RVA: 0x0005E6A8 File Offset: 0x0005C8A8
			internal bool IsDuplicate(string prefix, string localName, string namespaceUri)
			{
				return this.localName == localName && (this.prefix == prefix || this.namespaceUri == namespaceUri);
			}

			// Token: 0x04000B16 RID: 2838
			internal string prefix;

			// Token: 0x04000B17 RID: 2839
			internal string namespaceUri;

			// Token: 0x04000B18 RID: 2840
			internal string localName;

			// Token: 0x04000B19 RID: 2841
			internal int prev;
		}

		// Token: 0x020001BC RID: 444
		private enum SpecialAttribute
		{
			// Token: 0x04000B1B RID: 2843
			No,
			// Token: 0x04000B1C RID: 2844
			DefaultXmlns,
			// Token: 0x04000B1D RID: 2845
			PrefixedXmlns,
			// Token: 0x04000B1E RID: 2846
			XmlSpace,
			// Token: 0x04000B1F RID: 2847
			XmlLang
		}

		// Token: 0x020001BD RID: 445
		private class AttributeValueCache
		{
			// Token: 0x170002B3 RID: 691
			// (get) Token: 0x06000FF8 RID: 4088 RVA: 0x0005E6D6 File Offset: 0x0005C8D6
			internal string StringValue
			{
				get
				{
					if (this.singleStringValue != null)
					{
						return this.singleStringValue;
					}
					return this.stringValue.ToString();
				}
			}

			// Token: 0x06000FF9 RID: 4089 RVA: 0x0005E6F4 File Offset: 0x0005C8F4
			internal void WriteEntityRef(string name)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				if (!(name == "lt"))
				{
					if (!(name == "gt"))
					{
						if (!(name == "quot"))
						{
							if (!(name == "apos"))
							{
								if (!(name == "amp"))
								{
									this.stringValue.Append('&');
									this.stringValue.Append(name);
									this.stringValue.Append(';');
								}
								else
								{
									this.stringValue.Append('&');
								}
							}
							else
							{
								this.stringValue.Append('\'');
							}
						}
						else
						{
							this.stringValue.Append('"');
						}
					}
					else
					{
						this.stringValue.Append('>');
					}
				}
				else
				{
					this.stringValue.Append('<');
				}
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.EntityRef, name);
			}

			// Token: 0x06000FFA RID: 4090 RVA: 0x0005E7D3 File Offset: 0x0005C9D3
			internal void WriteCharEntity(char ch)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				this.stringValue.Append(ch);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.CharEntity, ch);
			}

			// Token: 0x06000FFB RID: 4091 RVA: 0x0005E7FD File Offset: 0x0005C9FD
			internal void WriteSurrogateCharEntity(char lowChar, char highChar)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				this.stringValue.Append(highChar);
				this.stringValue.Append(lowChar);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.SurrogateCharEntity, new char[]
				{
					lowChar,
					highChar
				});
			}

			// Token: 0x06000FFC RID: 4092 RVA: 0x0005E83C File Offset: 0x0005CA3C
			internal void WriteWhitespace(string ws)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				this.stringValue.Append(ws);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.Whitespace, ws);
			}

			// Token: 0x06000FFD RID: 4093 RVA: 0x0005E861 File Offset: 0x0005CA61
			internal void WriteString(string text)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				else if (this.lastItem == -1)
				{
					this.singleStringValue = text;
					return;
				}
				this.stringValue.Append(text);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.String, text);
			}

			// Token: 0x06000FFE RID: 4094 RVA: 0x0005E899 File Offset: 0x0005CA99
			internal void WriteChars(char[] buffer, int index, int count)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				this.stringValue.Append(buffer, index, count);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.StringChars, new XmlWellFormedWriter.AttributeValueCache.BufferChunk(buffer, index, count));
			}

			// Token: 0x06000FFF RID: 4095 RVA: 0x0005E8C7 File Offset: 0x0005CAC7
			internal void WriteRaw(char[] buffer, int index, int count)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				this.stringValue.Append(buffer, index, count);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.RawChars, new XmlWellFormedWriter.AttributeValueCache.BufferChunk(buffer, index, count));
			}

			// Token: 0x06001000 RID: 4096 RVA: 0x0005E8F5 File Offset: 0x0005CAF5
			internal void WriteRaw(string data)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				this.stringValue.Append(data);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.Raw, data);
			}

			// Token: 0x06001001 RID: 4097 RVA: 0x0005E91A File Offset: 0x0005CB1A
			internal void WriteValue(string value)
			{
				if (this.singleStringValue != null)
				{
					this.StartComplexValue();
				}
				this.stringValue.Append(value);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.ValueString, value);
			}

			// Token: 0x06001002 RID: 4098 RVA: 0x0005E940 File Offset: 0x0005CB40
			internal void Replay(XmlWriter writer)
			{
				if (this.singleStringValue != null)
				{
					writer.WriteString(this.singleStringValue);
					return;
				}
				for (int i = this.firstItem; i <= this.lastItem; i++)
				{
					XmlWellFormedWriter.AttributeValueCache.Item item = this.items[i];
					switch (item.type)
					{
					case XmlWellFormedWriter.AttributeValueCache.ItemType.EntityRef:
						writer.WriteEntityRef((string)item.data);
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.CharEntity:
						writer.WriteCharEntity((char)item.data);
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.SurrogateCharEntity:
					{
						char[] array = (char[])item.data;
						writer.WriteSurrogateCharEntity(array[0], array[1]);
						break;
					}
					case XmlWellFormedWriter.AttributeValueCache.ItemType.Whitespace:
						writer.WriteWhitespace((string)item.data);
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.String:
						writer.WriteString((string)item.data);
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.StringChars:
					{
						XmlWellFormedWriter.AttributeValueCache.BufferChunk bufferChunk = (XmlWellFormedWriter.AttributeValueCache.BufferChunk)item.data;
						writer.WriteChars(bufferChunk.buffer, bufferChunk.index, bufferChunk.count);
						break;
					}
					case XmlWellFormedWriter.AttributeValueCache.ItemType.Raw:
						writer.WriteRaw((string)item.data);
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.RawChars:
					{
						XmlWellFormedWriter.AttributeValueCache.BufferChunk bufferChunk = (XmlWellFormedWriter.AttributeValueCache.BufferChunk)item.data;
						writer.WriteChars(bufferChunk.buffer, bufferChunk.index, bufferChunk.count);
						break;
					}
					case XmlWellFormedWriter.AttributeValueCache.ItemType.ValueString:
						writer.WriteValue((string)item.data);
						break;
					}
				}
			}

			// Token: 0x06001003 RID: 4099 RVA: 0x0005EAA4 File Offset: 0x0005CCA4
			internal void Trim()
			{
				if (this.singleStringValue != null)
				{
					this.singleStringValue = XmlConvert.TrimString(this.singleStringValue);
					return;
				}
				string text = this.stringValue.ToString();
				string text2 = XmlConvert.TrimString(text);
				if (text != text2)
				{
					this.stringValue = new StringBuilder(text2);
				}
				XmlCharType instance = XmlCharType.Instance;
				int num = this.firstItem;
				while (num == this.firstItem && num <= this.lastItem)
				{
					XmlWellFormedWriter.AttributeValueCache.Item item = this.items[num];
					switch (item.type)
					{
					case XmlWellFormedWriter.AttributeValueCache.ItemType.Whitespace:
						this.firstItem++;
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.String:
					case XmlWellFormedWriter.AttributeValueCache.ItemType.Raw:
					case XmlWellFormedWriter.AttributeValueCache.ItemType.ValueString:
						item.data = XmlConvert.TrimStringStart((string)item.data);
						if (((string)item.data).Length == 0)
						{
							this.firstItem++;
						}
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.StringChars:
					case XmlWellFormedWriter.AttributeValueCache.ItemType.RawChars:
					{
						XmlWellFormedWriter.AttributeValueCache.BufferChunk bufferChunk = (XmlWellFormedWriter.AttributeValueCache.BufferChunk)item.data;
						int num2 = bufferChunk.index + bufferChunk.count;
						while (bufferChunk.index < num2 && instance.IsWhiteSpace(bufferChunk.buffer[bufferChunk.index]))
						{
							bufferChunk.index++;
							bufferChunk.count--;
						}
						if (bufferChunk.index == num2)
						{
							this.firstItem++;
						}
						break;
					}
					}
					num++;
				}
				num = this.lastItem;
				while (num == this.lastItem && num >= this.firstItem)
				{
					XmlWellFormedWriter.AttributeValueCache.Item item2 = this.items[num];
					switch (item2.type)
					{
					case XmlWellFormedWriter.AttributeValueCache.ItemType.Whitespace:
						this.lastItem--;
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.String:
					case XmlWellFormedWriter.AttributeValueCache.ItemType.Raw:
					case XmlWellFormedWriter.AttributeValueCache.ItemType.ValueString:
						item2.data = XmlConvert.TrimStringEnd((string)item2.data);
						if (((string)item2.data).Length == 0)
						{
							this.lastItem--;
						}
						break;
					case XmlWellFormedWriter.AttributeValueCache.ItemType.StringChars:
					case XmlWellFormedWriter.AttributeValueCache.ItemType.RawChars:
					{
						XmlWellFormedWriter.AttributeValueCache.BufferChunk bufferChunk2 = (XmlWellFormedWriter.AttributeValueCache.BufferChunk)item2.data;
						while (bufferChunk2.count > 0 && instance.IsWhiteSpace(bufferChunk2.buffer[bufferChunk2.index + bufferChunk2.count - 1]))
						{
							bufferChunk2.count--;
						}
						if (bufferChunk2.count == 0)
						{
							this.lastItem--;
						}
						break;
					}
					}
					num--;
				}
			}

			// Token: 0x06001004 RID: 4100 RVA: 0x0005ED29 File Offset: 0x0005CF29
			internal void Clear()
			{
				this.singleStringValue = null;
				this.lastItem = -1;
				this.firstItem = 0;
				this.stringValue.Length = 0;
			}

			// Token: 0x06001005 RID: 4101 RVA: 0x0005ED4C File Offset: 0x0005CF4C
			private void StartComplexValue()
			{
				this.stringValue.Append(this.singleStringValue);
				this.AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType.String, this.singleStringValue);
				this.singleStringValue = null;
			}

			// Token: 0x06001006 RID: 4102 RVA: 0x0005ED74 File Offset: 0x0005CF74
			private void AddItem(XmlWellFormedWriter.AttributeValueCache.ItemType type, object data)
			{
				int num = this.lastItem + 1;
				if (this.items == null)
				{
					this.items = new XmlWellFormedWriter.AttributeValueCache.Item[4];
				}
				else if (this.items.Length == num)
				{
					XmlWellFormedWriter.AttributeValueCache.Item[] destinationArray = new XmlWellFormedWriter.AttributeValueCache.Item[num * 2];
					Array.Copy(this.items, destinationArray, num);
					this.items = destinationArray;
				}
				if (this.items[num] == null)
				{
					this.items[num] = new XmlWellFormedWriter.AttributeValueCache.Item();
				}
				this.items[num].Set(type, data);
				this.lastItem = num;
			}

			// Token: 0x06001007 RID: 4103 RVA: 0x0005EDF8 File Offset: 0x0005CFF8
			internal async Task ReplayAsync(XmlWriter writer)
			{
				if (this.singleStringValue != null)
				{
					await writer.WriteStringAsync(this.singleStringValue).ConfigureAwait(false);
				}
				else
				{
					for (int i = this.firstItem; i <= this.lastItem; i++)
					{
						XmlWellFormedWriter.AttributeValueCache.Item item = this.items[i];
						switch (item.type)
						{
						case XmlWellFormedWriter.AttributeValueCache.ItemType.EntityRef:
							await writer.WriteEntityRefAsync((string)item.data).ConfigureAwait(false);
							break;
						case XmlWellFormedWriter.AttributeValueCache.ItemType.CharEntity:
							await writer.WriteCharEntityAsync((char)item.data).ConfigureAwait(false);
							break;
						case XmlWellFormedWriter.AttributeValueCache.ItemType.SurrogateCharEntity:
						{
							char[] array = (char[])item.data;
							await writer.WriteSurrogateCharEntityAsync(array[0], array[1]).ConfigureAwait(false);
							break;
						}
						case XmlWellFormedWriter.AttributeValueCache.ItemType.Whitespace:
							await writer.WriteWhitespaceAsync((string)item.data).ConfigureAwait(false);
							break;
						case XmlWellFormedWriter.AttributeValueCache.ItemType.String:
							await writer.WriteStringAsync((string)item.data).ConfigureAwait(false);
							break;
						case XmlWellFormedWriter.AttributeValueCache.ItemType.StringChars:
						{
							XmlWellFormedWriter.AttributeValueCache.BufferChunk bufferChunk = (XmlWellFormedWriter.AttributeValueCache.BufferChunk)item.data;
							await writer.WriteCharsAsync(bufferChunk.buffer, bufferChunk.index, bufferChunk.count).ConfigureAwait(false);
							break;
						}
						case XmlWellFormedWriter.AttributeValueCache.ItemType.Raw:
							await writer.WriteRawAsync((string)item.data).ConfigureAwait(false);
							break;
						case XmlWellFormedWriter.AttributeValueCache.ItemType.RawChars:
						{
							XmlWellFormedWriter.AttributeValueCache.BufferChunk bufferChunk = (XmlWellFormedWriter.AttributeValueCache.BufferChunk)item.data;
							await writer.WriteCharsAsync(bufferChunk.buffer, bufferChunk.index, bufferChunk.count).ConfigureAwait(false);
							break;
						}
						case XmlWellFormedWriter.AttributeValueCache.ItemType.ValueString:
							await writer.WriteStringAsync((string)item.data).ConfigureAwait(false);
							break;
						}
						item = null;
					}
				}
			}

			// Token: 0x06001008 RID: 4104 RVA: 0x0005EE45 File Offset: 0x0005D045
			public AttributeValueCache()
			{
			}

			// Token: 0x04000B20 RID: 2848
			private StringBuilder stringValue = new StringBuilder();

			// Token: 0x04000B21 RID: 2849
			private string singleStringValue;

			// Token: 0x04000B22 RID: 2850
			private XmlWellFormedWriter.AttributeValueCache.Item[] items;

			// Token: 0x04000B23 RID: 2851
			private int firstItem;

			// Token: 0x04000B24 RID: 2852
			private int lastItem = -1;

			// Token: 0x020001BE RID: 446
			private enum ItemType
			{
				// Token: 0x04000B26 RID: 2854
				EntityRef,
				// Token: 0x04000B27 RID: 2855
				CharEntity,
				// Token: 0x04000B28 RID: 2856
				SurrogateCharEntity,
				// Token: 0x04000B29 RID: 2857
				Whitespace,
				// Token: 0x04000B2A RID: 2858
				String,
				// Token: 0x04000B2B RID: 2859
				StringChars,
				// Token: 0x04000B2C RID: 2860
				Raw,
				// Token: 0x04000B2D RID: 2861
				RawChars,
				// Token: 0x04000B2E RID: 2862
				ValueString
			}

			// Token: 0x020001BF RID: 447
			private class Item
			{
				// Token: 0x06001009 RID: 4105 RVA: 0x00002103 File Offset: 0x00000303
				internal Item()
				{
				}

				// Token: 0x0600100A RID: 4106 RVA: 0x0005EE5F File Offset: 0x0005D05F
				internal void Set(XmlWellFormedWriter.AttributeValueCache.ItemType type, object data)
				{
					this.type = type;
					this.data = data;
				}

				// Token: 0x04000B2F RID: 2863
				internal XmlWellFormedWriter.AttributeValueCache.ItemType type;

				// Token: 0x04000B30 RID: 2864
				internal object data;
			}

			// Token: 0x020001C0 RID: 448
			private class BufferChunk
			{
				// Token: 0x0600100B RID: 4107 RVA: 0x0005EE6F File Offset: 0x0005D06F
				internal BufferChunk(char[] buffer, int index, int count)
				{
					this.buffer = buffer;
					this.index = index;
					this.count = count;
				}

				// Token: 0x04000B31 RID: 2865
				internal char[] buffer;

				// Token: 0x04000B32 RID: 2866
				internal int index;

				// Token: 0x04000B33 RID: 2867
				internal int count;
			}

			// Token: 0x020001C1 RID: 449
			[CompilerGenerated]
			[StructLayout(LayoutKind.Auto)]
			private struct <ReplayAsync>d__24 : IAsyncStateMachine
			{
				// Token: 0x0600100C RID: 4108 RVA: 0x0005EE8C File Offset: 0x0005D08C
				void IAsyncStateMachine.MoveNext()
				{
					int num2;
					int num = num2;
					XmlWellFormedWriter.AttributeValueCache attributeValueCache = this;
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_18D;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_20C;
						}
						case 3:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_295;
						}
						case 4:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_314;
						}
						case 5:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_393;
						}
						case 6:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_425;
						}
						case 7:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_4A4;
						}
						case 8:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_536;
						}
						case 9:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_5B0;
						}
						default:
							if (attributeValueCache.singleStringValue == null)
							{
								i = attributeValueCache.firstItem;
								goto IL_5D0;
							}
							configuredTaskAwaiter = writer.WriteStringAsync(attributeValueCache.singleStringValue).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
								return;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						goto IL_5FC;
						IL_18D:
						configuredTaskAwaiter.GetResult();
						goto IL_5B7;
						IL_20C:
						configuredTaskAwaiter.GetResult();
						goto IL_5B7;
						IL_295:
						configuredTaskAwaiter.GetResult();
						goto IL_5B7;
						IL_314:
						configuredTaskAwaiter.GetResult();
						goto IL_5B7;
						IL_393:
						configuredTaskAwaiter.GetResult();
						goto IL_5B7;
						IL_425:
						configuredTaskAwaiter.GetResult();
						goto IL_5B7;
						IL_4A4:
						configuredTaskAwaiter.GetResult();
						goto IL_5B7;
						IL_536:
						configuredTaskAwaiter.GetResult();
						goto IL_5B7;
						IL_5B0:
						configuredTaskAwaiter.GetResult();
						IL_5B7:
						item = null;
						int num3 = i;
						i = num3 + 1;
						IL_5D0:
						if (i <= attributeValueCache.lastItem)
						{
							item = attributeValueCache.items[i];
							switch (item.type)
							{
							case XmlWellFormedWriter.AttributeValueCache.ItemType.EntityRef:
								configuredTaskAwaiter = writer.WriteEntityRefAsync((string)item.data).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 1;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_18D;
							case XmlWellFormedWriter.AttributeValueCache.ItemType.CharEntity:
								configuredTaskAwaiter = writer.WriteCharEntityAsync((char)item.data).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 2;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_20C;
							case XmlWellFormedWriter.AttributeValueCache.ItemType.SurrogateCharEntity:
							{
								char[] array = (char[])item.data;
								configuredTaskAwaiter = writer.WriteSurrogateCharEntityAsync(array[0], array[1]).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 3;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_295;
							}
							case XmlWellFormedWriter.AttributeValueCache.ItemType.Whitespace:
								configuredTaskAwaiter = writer.WriteWhitespaceAsync((string)item.data).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 4;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_314;
							case XmlWellFormedWriter.AttributeValueCache.ItemType.String:
								configuredTaskAwaiter = writer.WriteStringAsync((string)item.data).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 5;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_393;
							case XmlWellFormedWriter.AttributeValueCache.ItemType.StringChars:
							{
								XmlWellFormedWriter.AttributeValueCache.BufferChunk bufferChunk = (XmlWellFormedWriter.AttributeValueCache.BufferChunk)item.data;
								configuredTaskAwaiter = writer.WriteCharsAsync(bufferChunk.buffer, bufferChunk.index, bufferChunk.count).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 6;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_425;
							}
							case XmlWellFormedWriter.AttributeValueCache.ItemType.Raw:
								configuredTaskAwaiter = writer.WriteRawAsync((string)item.data).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 7;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_4A4;
							case XmlWellFormedWriter.AttributeValueCache.ItemType.RawChars:
							{
								XmlWellFormedWriter.AttributeValueCache.BufferChunk bufferChunk = (XmlWellFormedWriter.AttributeValueCache.BufferChunk)item.data;
								configuredTaskAwaiter = writer.WriteCharsAsync(bufferChunk.buffer, bufferChunk.index, bufferChunk.count).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 8;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_536;
							}
							case XmlWellFormedWriter.AttributeValueCache.ItemType.ValueString:
								configuredTaskAwaiter = writer.WriteStringAsync((string)item.data).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 9;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.AttributeValueCache.<ReplayAsync>d__24>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_5B0;
							default:
								goto IL_5B7;
							}
						}
					}
					catch (Exception exception)
					{
						num2 = -2;
						this.<>t__builder.SetException(exception);
						return;
					}
					IL_5FC:
					num2 = -2;
					this.<>t__builder.SetResult();
				}

				// Token: 0x0600100D RID: 4109 RVA: 0x0005F4C4 File Offset: 0x0005D6C4
				[DebuggerHidden]
				void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
				{
					this.<>t__builder.SetStateMachine(stateMachine);
				}

				// Token: 0x04000B34 RID: 2868
				public int <>1__state;

				// Token: 0x04000B35 RID: 2869
				public AsyncTaskMethodBuilder <>t__builder;

				// Token: 0x04000B36 RID: 2870
				public XmlWellFormedWriter.AttributeValueCache <>4__this;

				// Token: 0x04000B37 RID: 2871
				public XmlWriter writer;

				// Token: 0x04000B38 RID: 2872
				private XmlWellFormedWriter.AttributeValueCache.Item <item>5__1;

				// Token: 0x04000B39 RID: 2873
				private int <i>5__2;

				// Token: 0x04000B3A RID: 2874
				private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
			}
		}

		// Token: 0x020001C2 RID: 450
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteEndDocumentAsync>d__115 : IAsyncStateMachine
		{
			// Token: 0x0600100E RID: 4110 RVA: 0x0005F4D4 File Offset: 0x0005D6D4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_FE;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_185;
						}
						default:
							goto IL_8B;
						}
						IL_84:
						configuredTaskAwaiter.GetResult();
						IL_8B:
						if (xmlWellFormedWriter.elemTop <= 0)
						{
							prevState = xmlWellFormedWriter.currentState;
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.EndDocument).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndDocumentAsync>d__115>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xmlWellFormedWriter.WriteEndElementAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndDocumentAsync>d__115>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_84;
						}
						IL_FE:
						configuredTaskAwaiter.GetResult();
						if (prevState != XmlWellFormedWriter.State.AfterRootEle)
						{
							throw new ArgumentException(Res.GetString("Document does not have a root element."));
						}
						if (xmlWellFormedWriter.rawWriter != null)
						{
							goto IL_18C;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteEndDocumentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndDocumentAsync>d__115>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_185:
						configuredTaskAwaiter.GetResult();
						IL_18C:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600100F RID: 4111 RVA: 0x0005F6DC File Offset: 0x0005D8DC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B3B RID: 2875
			public int <>1__state;

			// Token: 0x04000B3C RID: 2876
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B3D RID: 2877
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B3E RID: 2878
			private XmlWellFormedWriter.State <prevState>5__1;

			// Token: 0x04000B3F RID: 2879
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001C3 RID: 451
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteDocTypeAsync>d__116 : IAsyncStateMachine
		{
			// Token: 0x06001010 RID: 4112 RVA: 0x0005F6EC File Offset: 0x0005D8EC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_24B;
							}
							if (name == null || name.Length == 0)
							{
								throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
							}
							XmlConvert.VerifyQName(name, ExceptionType.XmlException);
							if (xmlWellFormedWriter.conformanceLevel == ConformanceLevel.Fragment)
							{
								throw new InvalidOperationException(Res.GetString("DTD is not allowed in XML fragments."));
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Dtd).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteDocTypeAsync>d__116>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.dtdWritten)
						{
							xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
							throw new InvalidOperationException(Res.GetString("The DTD has already been written out."));
						}
						if (xmlWellFormedWriter.conformanceLevel == ConformanceLevel.Auto)
						{
							xmlWellFormedWriter.conformanceLevel = ConformanceLevel.Document;
							xmlWellFormedWriter.stateTable = XmlWellFormedWriter.StateTableDocument;
						}
						if (xmlWellFormedWriter.checkCharacters)
						{
							int invCharIndex;
							if (pubid != null && (invCharIndex = xmlWellFormedWriter.xmlCharType.IsPublicId(pubid)) >= 0)
							{
								throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(pubid, invCharIndex)), "pubid");
							}
							if (sysid != null && (invCharIndex = xmlWellFormedWriter.xmlCharType.IsOnlyCharData(sysid)) >= 0)
							{
								throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(sysid, invCharIndex)), "sysid");
							}
							if (subset != null && (invCharIndex = xmlWellFormedWriter.xmlCharType.IsOnlyCharData(subset)) >= 0)
							{
								throw new ArgumentException(Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(subset, invCharIndex)), "subset");
							}
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteDocTypeAsync(name, pubid, sysid, subset).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteDocTypeAsync>d__116>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_24B:
						configuredTaskAwaiter.GetResult();
						xmlWellFormedWriter.dtdWritten = true;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001011 RID: 4113 RVA: 0x0005F9C4 File Offset: 0x0005DBC4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B40 RID: 2880
			public int <>1__state;

			// Token: 0x04000B41 RID: 2881
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B42 RID: 2882
			public string name;

			// Token: 0x04000B43 RID: 2883
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B44 RID: 2884
			public string pubid;

			// Token: 0x04000B45 RID: 2885
			public string sysid;

			// Token: 0x04000B46 RID: 2886
			public string subset;

			// Token: 0x04000B47 RID: 2887
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001C4 RID: 452
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_TryReturnTask>d__118 : IAsyncStateMachine
		{
			// Token: 0x06001012 RID: 4114 RVA: 0x0005F9D4 File Offset: 0x0005DBD4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<_TryReturnTask>d__118>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001013 RID: 4115 RVA: 0x0005FAB0 File Offset: 0x0005DCB0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B48 RID: 2888
			public int <>1__state;

			// Token: 0x04000B49 RID: 2889
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B4A RID: 2890
			public Task task;

			// Token: 0x04000B4B RID: 2891
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B4C RID: 2892
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001C5 RID: 453
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_SequenceRun>d__120 : IAsyncStateMachine
		{
			// Token: 0x06001014 RID: 4116 RVA: 0x0005FAC0 File Offset: 0x0005DCC0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_E0;
							}
							configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<_SequenceRun>d__120>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = nextTaskFun().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<_SequenceRun>d__120>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_E0:
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001015 RID: 4117 RVA: 0x0005FC0C File Offset: 0x0005DE0C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B4D RID: 2893
			public int <>1__state;

			// Token: 0x04000B4E RID: 2894
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B4F RID: 2895
			public Task task;

			// Token: 0x04000B50 RID: 2896
			public Func<Task> nextTaskFun;

			// Token: 0x04000B51 RID: 2897
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B52 RID: 2898
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001C6 RID: 454
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartElementAsync_NoAdvanceState>d__123 : IAsyncStateMachine
		{
			// Token: 0x06001016 RID: 4118 RVA: 0x0005FC1C File Offset: 0x0005DE1C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_ED;
							}
							configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStartElementAsync_NoAdvanceState>d__123>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.WriteStartElementAsync_NoAdvanceState(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStartElementAsync_NoAdvanceState>d__123>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_ED:
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001017 RID: 4119 RVA: 0x0005FD74 File Offset: 0x0005DF74
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B53 RID: 2899
			public int <>1__state;

			// Token: 0x04000B54 RID: 2900
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B55 RID: 2901
			public Task task;

			// Token: 0x04000B56 RID: 2902
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B57 RID: 2903
			public string prefix;

			// Token: 0x04000B58 RID: 2904
			public string localName;

			// Token: 0x04000B59 RID: 2905
			public string ns;

			// Token: 0x04000B5A RID: 2906
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001C7 RID: 455
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartElementAsync_FinishWrite>d__125 : IAsyncStateMachine
		{
			// Token: 0x06001018 RID: 4120 RVA: 0x0005FD84 File Offset: 0x0005DF84
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							configuredTaskAwaiter = t.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStartElementAsync_FinishWrite>d__125>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						xmlWellFormedWriter.WriteStartElementAsync_FinishWrite(prefix, localName, ns);
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001019 RID: 4121 RVA: 0x0005FE78 File Offset: 0x0005E078
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B5B RID: 2907
			public int <>1__state;

			// Token: 0x04000B5C RID: 2908
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B5D RID: 2909
			public Task t;

			// Token: 0x04000B5E RID: 2910
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B5F RID: 2911
			public string prefix;

			// Token: 0x04000B60 RID: 2912
			public string localName;

			// Token: 0x04000B61 RID: 2913
			public string ns;

			// Token: 0x04000B62 RID: 2914
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001C8 RID: 456
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartAttributeAsync_NoAdvanceState>d__133 : IAsyncStateMachine
		{
			// Token: 0x0600101A RID: 4122 RVA: 0x0005FE88 File Offset: 0x0005E088
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_ED;
							}
							configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStartAttributeAsync_NoAdvanceState>d__133>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.WriteStartAttributeAsync_NoAdvanceState(prefix, localName, namespaceName).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStartAttributeAsync_NoAdvanceState>d__133>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_ED:
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600101B RID: 4123 RVA: 0x0005FFE0 File Offset: 0x0005E1E0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B63 RID: 2915
			public int <>1__state;

			// Token: 0x04000B64 RID: 2916
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B65 RID: 2917
			public Task task;

			// Token: 0x04000B66 RID: 2918
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B67 RID: 2919
			public string prefix;

			// Token: 0x04000B68 RID: 2920
			public string localName;

			// Token: 0x04000B69 RID: 2921
			public string namespaceName;

			// Token: 0x04000B6A RID: 2922
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001C9 RID: 457
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteEndAttributeAsync_SepcialAtt>d__136 : IAsyncStateMachine
		{
			// Token: 0x0600101C RID: 4124 RVA: 0x0005FFF0 File Offset: 0x0005E1F0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_1A8;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_212;
						}
						case 3:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_28C;
						}
						case 4:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_30A;
						}
						case 5:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_37A;
						}
						case 6:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_3E4;
						}
						case 7:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_506;
						}
						case 8:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_576;
						}
						case 9:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_5E1;
						}
						case 10:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_65D;
						}
						case 11:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_6DD;
						}
						case 12:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_74E;
						}
						case 13:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_7B9;
						}
						case 14:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_8D0;
						}
						case 15:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_941;
						}
						case 16:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_9AC;
						}
						case 17:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_A58;
						}
						case 18:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_AC9;
						}
						case 19:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_B31;
						}
						default:
							switch (xmlWellFormedWriter.specAttr)
							{
							case XmlWellFormedWriter.SpecialAttribute.DefaultXmlns:
								value = xmlWellFormedWriter.attrValueCache.StringValue;
								if (!xmlWellFormedWriter.PushNamespaceExplicit(string.Empty, value))
								{
									goto IL_3EB;
								}
								if (xmlWellFormedWriter.rawWriter != null)
								{
									if (xmlWellFormedWriter.rawWriter.SupportsNamespaceDeclarationInChunks)
									{
										configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteStartNamespaceDeclarationAsync(string.Empty).ConfigureAwait(false).GetAwaiter();
										if (!configuredTaskAwaiter.IsCompleted)
										{
											num2 = 0;
											ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
											this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
											return;
										}
									}
									else
									{
										configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteNamespaceDeclarationAsync(string.Empty, value).ConfigureAwait(false).GetAwaiter();
										if (!configuredTaskAwaiter.IsCompleted)
										{
											num2 = 3;
											ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
											this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
											return;
										}
										goto IL_28C;
									}
								}
								else
								{
									configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteStartAttributeAsync(string.Empty, "xmlns", "http://www.w3.org/2000/xmlns/").ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num2 = 4;
										ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
										return;
									}
									goto IL_30A;
								}
								break;
							case XmlWellFormedWriter.SpecialAttribute.PrefixedXmlns:
								value = xmlWellFormedWriter.attrValueCache.StringValue;
								if (value.Length == 0)
								{
									throw new ArgumentException(Res.GetString("Cannot use a prefix with an empty namespace."));
								}
								if (value == "http://www.w3.org/2000/xmlns/" || (value == "http://www.w3.org/XML/1998/namespace" && xmlWellFormedWriter.curDeclPrefix != "xml"))
								{
									throw new ArgumentException(Res.GetString("Cannot bind to the reserved namespace."));
								}
								if (!xmlWellFormedWriter.PushNamespaceExplicit(xmlWellFormedWriter.curDeclPrefix, value))
								{
									goto IL_7C0;
								}
								if (xmlWellFormedWriter.rawWriter != null)
								{
									if (xmlWellFormedWriter.rawWriter.SupportsNamespaceDeclarationInChunks)
									{
										configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteStartNamespaceDeclarationAsync(xmlWellFormedWriter.curDeclPrefix).ConfigureAwait(false).GetAwaiter();
										if (!configuredTaskAwaiter.IsCompleted)
										{
											num2 = 7;
											ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
											this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
											return;
										}
										goto IL_506;
									}
									else
									{
										configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteNamespaceDeclarationAsync(xmlWellFormedWriter.curDeclPrefix, value).ConfigureAwait(false).GetAwaiter();
										if (!configuredTaskAwaiter.IsCompleted)
										{
											num2 = 10;
											ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
											this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
											return;
										}
										goto IL_65D;
									}
								}
								else
								{
									configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteStartAttributeAsync("xmlns", xmlWellFormedWriter.curDeclPrefix, "http://www.w3.org/2000/xmlns/").ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num2 = 11;
										ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
										return;
									}
									goto IL_6DD;
								}
								break;
							case XmlWellFormedWriter.SpecialAttribute.XmlSpace:
								xmlWellFormedWriter.attrValueCache.Trim();
								value = xmlWellFormedWriter.attrValueCache.StringValue;
								if (value == "default")
								{
									xmlWellFormedWriter.elemScopeStack[xmlWellFormedWriter.elemTop].xmlSpace = XmlSpace.Default;
								}
								else
								{
									if (!(value == "preserve"))
									{
										throw new ArgumentException(Res.GetString("'{0}' is an invalid xml:space value.", new object[]
										{
											value
										}));
									}
									xmlWellFormedWriter.elemScopeStack[xmlWellFormedWriter.elemTop].xmlSpace = XmlSpace.Preserve;
								}
								configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteStartAttributeAsync("xml", "space", "http://www.w3.org/XML/1998/namespace").ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 14;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_8D0;
							case XmlWellFormedWriter.SpecialAttribute.XmlLang:
								value = xmlWellFormedWriter.attrValueCache.StringValue;
								xmlWellFormedWriter.elemScopeStack[xmlWellFormedWriter.elemTop].xmlLang = value;
								configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteStartAttributeAsync("xml", "lang", "http://www.w3.org/XML/1998/namespace").ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 17;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_A58;
							default:
								goto IL_B38;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.attrValueCache.ReplayAsync(xmlWellFormedWriter.rawWriter).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_1A8:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteEndNamespaceDeclarationAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_212:
						configuredTaskAwaiter.GetResult();
						goto IL_3EB;
						IL_28C:
						configuredTaskAwaiter.GetResult();
						goto IL_3EB;
						IL_30A:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.attrValueCache.ReplayAsync(xmlWellFormedWriter.writer).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 5;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_37A:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteEndAttributeAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 6;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_3E4:
						configuredTaskAwaiter.GetResult();
						IL_3EB:
						xmlWellFormedWriter.curDeclPrefix = null;
						goto IL_B38;
						IL_506:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.attrValueCache.ReplayAsync(xmlWellFormedWriter.rawWriter).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 8;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_576:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteEndNamespaceDeclarationAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 9;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_5E1:
						configuredTaskAwaiter.GetResult();
						goto IL_7C0;
						IL_65D:
						configuredTaskAwaiter.GetResult();
						goto IL_7C0;
						IL_6DD:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.attrValueCache.ReplayAsync(xmlWellFormedWriter.writer).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 12;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_74E:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteEndAttributeAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 13;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_7B9:
						configuredTaskAwaiter.GetResult();
						IL_7C0:
						xmlWellFormedWriter.curDeclPrefix = null;
						goto IL_B38;
						IL_8D0:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.attrValueCache.ReplayAsync(xmlWellFormedWriter.writer).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 15;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_941:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteEndAttributeAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 16;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_9AC:
						configuredTaskAwaiter.GetResult();
						goto IL_B38;
						IL_A58:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.attrValueCache.ReplayAsync(xmlWellFormedWriter.writer).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 18;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_AC9:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteEndAttributeAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 19;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEndAttributeAsync_SepcialAtt>d__136>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_B31:
						configuredTaskAwaiter.GetResult();
						IL_B38:
						xmlWellFormedWriter.specAttr = XmlWellFormedWriter.SpecialAttribute.No;
						xmlWellFormedWriter.attrValueCache.Clear();
						value = null;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600101D RID: 4125 RVA: 0x00060BC0 File Offset: 0x0005EDC0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B6B RID: 2923
			public int <>1__state;

			// Token: 0x04000B6C RID: 2924
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B6D RID: 2925
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B6E RID: 2926
			private string <value>5__1;

			// Token: 0x04000B6F RID: 2927
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001CA RID: 458
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCDataAsync>d__137 : IAsyncStateMachine
		{
			// Token: 0x0600101E RID: 4126 RVA: 0x00060BD0 File Offset: 0x0005EDD0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_FB;
							}
							if (text == null)
							{
								text = string.Empty;
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.CData).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteCDataAsync>d__137>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteCDataAsync(text).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteCDataAsync>d__137>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_FB:
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600101F RID: 4127 RVA: 0x00060D50 File Offset: 0x0005EF50
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B70 RID: 2928
			public int <>1__state;

			// Token: 0x04000B71 RID: 2929
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B72 RID: 2930
			public string text;

			// Token: 0x04000B73 RID: 2931
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B74 RID: 2932
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001CB RID: 459
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCommentAsync>d__138 : IAsyncStateMachine
		{
			// Token: 0x06001020 RID: 4128 RVA: 0x00060D60 File Offset: 0x0005EF60
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_FA;
							}
							if (text == null)
							{
								text = string.Empty;
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Comment).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteCommentAsync>d__138>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteCommentAsync(text).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteCommentAsync>d__138>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_FA:
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001021 RID: 4129 RVA: 0x00060EE0 File Offset: 0x0005F0E0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B75 RID: 2933
			public int <>1__state;

			// Token: 0x04000B76 RID: 2934
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B77 RID: 2935
			public string text;

			// Token: 0x04000B78 RID: 2936
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B79 RID: 2937
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001CC RID: 460
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteProcessingInstructionAsync>d__139 : IAsyncStateMachine
		{
			// Token: 0x06001022 RID: 4130 RVA: 0x00060EF0 File Offset: 0x0005F0F0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_19C;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_216;
						}
						case 3:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_280;
						}
						case 4:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_2F2;
						}
						default:
							if (name == null || name.Length == 0)
							{
								throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
							}
							xmlWellFormedWriter.CheckNCName(name);
							if (text == null)
							{
								text = string.Empty;
							}
							if (name.Length == 3 && string.Compare(name, "xml", StringComparison.OrdinalIgnoreCase) == 0)
							{
								if (xmlWellFormedWriter.currentState != XmlWellFormedWriter.State.Start)
								{
									throw new ArgumentException(Res.GetString((xmlWellFormedWriter.conformanceLevel == ConformanceLevel.Document) ? "Cannot write XML declaration. WriteStartDocument method has already written it." : "Cannot write XML declaration. XML declaration can be only at the beginning of the document."));
								}
								xmlWellFormedWriter.xmlDeclFollows = true;
								configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.PI).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteProcessingInstructionAsync>d__139>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
							else
							{
								configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.PI).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 3;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteProcessingInstructionAsync>d__139>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_280;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.rawWriter != null)
						{
							configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteXmlDeclarationAsync(text).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteProcessingInstructionAsync>d__139>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteProcessingInstructionAsync(name, text).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 2;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteProcessingInstructionAsync>d__139>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_216;
						}
						IL_19C:
						configuredTaskAwaiter.GetResult();
						goto IL_2F9;
						IL_216:
						configuredTaskAwaiter.GetResult();
						goto IL_2F9;
						IL_280:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteProcessingInstructionAsync(name, text).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 4;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteProcessingInstructionAsync>d__139>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_2F2:
						configuredTaskAwaiter.GetResult();
						IL_2F9:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001023 RID: 4131 RVA: 0x00061268 File Offset: 0x0005F468
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B7A RID: 2938
			public int <>1__state;

			// Token: 0x04000B7B RID: 2939
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B7C RID: 2940
			public string name;

			// Token: 0x04000B7D RID: 2941
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B7E RID: 2942
			public string text;

			// Token: 0x04000B7F RID: 2943
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001CD RID: 461
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteEntityRefAsync>d__140 : IAsyncStateMachine
		{
			// Token: 0x06001024 RID: 4132 RVA: 0x00061278 File Offset: 0x0005F478
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_134;
							}
							if (name == null || name.Length == 0)
							{
								throw new ArgumentException(Res.GetString("The empty string '' is not a valid name."));
							}
							xmlWellFormedWriter.CheckNCName(name);
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEntityRefAsync>d__140>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.SaveAttrValue)
						{
							xmlWellFormedWriter.attrValueCache.WriteEntityRef(name);
							goto IL_13B;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteEntityRefAsync(name).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteEntityRefAsync>d__140>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_134:
						configuredTaskAwaiter.GetResult();
						IL_13B:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001025 RID: 4133 RVA: 0x00061430 File Offset: 0x0005F630
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B80 RID: 2944
			public int <>1__state;

			// Token: 0x04000B81 RID: 2945
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B82 RID: 2946
			public string name;

			// Token: 0x04000B83 RID: 2947
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B84 RID: 2948
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001CE RID: 462
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCharEntityAsync>d__141 : IAsyncStateMachine
		{
			// Token: 0x06001026 RID: 4134 RVA: 0x00061440 File Offset: 0x0005F640
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_120;
							}
							if (char.IsSurrogate(ch))
							{
								throw new ArgumentException(Res.GetString("The surrogate pair is invalid. Missing a low surrogate character."));
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteCharEntityAsync>d__141>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.SaveAttrValue)
						{
							xmlWellFormedWriter.attrValueCache.WriteCharEntity(ch);
							goto IL_127;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteCharEntityAsync(ch).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteCharEntityAsync>d__141>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_120:
						configuredTaskAwaiter.GetResult();
						IL_127:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001027 RID: 4135 RVA: 0x000615E4 File Offset: 0x0005F7E4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B85 RID: 2949
			public int <>1__state;

			// Token: 0x04000B86 RID: 2950
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B87 RID: 2951
			public char ch;

			// Token: 0x04000B88 RID: 2952
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B89 RID: 2953
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001CF RID: 463
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteSurrogateCharEntityAsync>d__142 : IAsyncStateMachine
		{
			// Token: 0x06001028 RID: 4136 RVA: 0x000615F4 File Offset: 0x0005F7F4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_134;
							}
							if (!char.IsSurrogatePair(highChar, lowChar))
							{
								throw XmlConvert.CreateInvalidSurrogatePairException(lowChar, highChar);
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteSurrogateCharEntityAsync>d__142>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.SaveAttrValue)
						{
							xmlWellFormedWriter.attrValueCache.WriteSurrogateCharEntity(lowChar, highChar);
							goto IL_13B;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteSurrogateCharEntityAsync(lowChar, highChar).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteSurrogateCharEntityAsync>d__142>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_134:
						configuredTaskAwaiter.GetResult();
						IL_13B:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001029 RID: 4137 RVA: 0x000617AC File Offset: 0x0005F9AC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B8A RID: 2954
			public int <>1__state;

			// Token: 0x04000B8B RID: 2955
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B8C RID: 2956
			public char highChar;

			// Token: 0x04000B8D RID: 2957
			public char lowChar;

			// Token: 0x04000B8E RID: 2958
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B8F RID: 2959
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D0 RID: 464
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteWhitespaceAsync>d__143 : IAsyncStateMachine
		{
			// Token: 0x0600102A RID: 4138 RVA: 0x000617BC File Offset: 0x0005F9BC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_140;
							}
							if (ws == null)
							{
								ws = string.Empty;
							}
							if (!XmlCharType.Instance.IsOnlyWhitespace(ws))
							{
								throw new ArgumentException(Res.GetString("Only white space characters should be used."));
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Whitespace).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteWhitespaceAsync>d__143>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.SaveAttrValue)
						{
							xmlWellFormedWriter.attrValueCache.WriteWhitespace(ws);
							goto IL_147;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteWhitespaceAsync(ws).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteWhitespaceAsync>d__143>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_140:
						configuredTaskAwaiter.GetResult();
						IL_147:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600102B RID: 4139 RVA: 0x00061980 File Offset: 0x0005FB80
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B90 RID: 2960
			public int <>1__state;

			// Token: 0x04000B91 RID: 2961
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B92 RID: 2962
			public string ws;

			// Token: 0x04000B93 RID: 2963
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B94 RID: 2964
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D1 RID: 465
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStringAsync_NoAdvanceState>d__146 : IAsyncStateMachine
		{
			// Token: 0x0600102C RID: 4140 RVA: 0x00061990 File Offset: 0x0005FB90
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_E1;
							}
							configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStringAsync_NoAdvanceState>d__146>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.WriteStringAsync_NoAdvanceState(text).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStringAsync_NoAdvanceState>d__146>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_E1:
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600102D RID: 4141 RVA: 0x00061ADC File Offset: 0x0005FCDC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B95 RID: 2965
			public int <>1__state;

			// Token: 0x04000B96 RID: 2966
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B97 RID: 2967
			public Task task;

			// Token: 0x04000B98 RID: 2968
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000B99 RID: 2969
			public string text;

			// Token: 0x04000B9A RID: 2970
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D2 RID: 466
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCharsAsync>d__147 : IAsyncStateMachine
		{
			// Token: 0x0600102E RID: 4142 RVA: 0x00061AEC File Offset: 0x0005FCEC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_17B;
							}
							if (buffer == null)
							{
								throw new ArgumentNullException("buffer");
							}
							if (index < 0)
							{
								throw new ArgumentOutOfRangeException("index");
							}
							if (count < 0)
							{
								throw new ArgumentOutOfRangeException("count");
							}
							if (count > buffer.Length - index)
							{
								throw new ArgumentOutOfRangeException("count");
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteCharsAsync>d__147>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.SaveAttrValue)
						{
							xmlWellFormedWriter.attrValueCache.WriteChars(buffer, index, count);
							goto IL_182;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteCharsAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteCharsAsync>d__147>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_17B:
						configuredTaskAwaiter.GetResult();
						IL_182:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600102F RID: 4143 RVA: 0x00061CEC File Offset: 0x0005FEEC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000B9B RID: 2971
			public int <>1__state;

			// Token: 0x04000B9C RID: 2972
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000B9D RID: 2973
			public char[] buffer;

			// Token: 0x04000B9E RID: 2974
			public int index;

			// Token: 0x04000B9F RID: 2975
			public int count;

			// Token: 0x04000BA0 RID: 2976
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BA1 RID: 2977
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D3 RID: 467
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawAsync>d__148 : IAsyncStateMachine
		{
			// Token: 0x06001030 RID: 4144 RVA: 0x00061CFC File Offset: 0x0005FEFC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_17B;
							}
							if (buffer == null)
							{
								throw new ArgumentNullException("buffer");
							}
							if (index < 0)
							{
								throw new ArgumentOutOfRangeException("index");
							}
							if (count < 0)
							{
								throw new ArgumentOutOfRangeException("count");
							}
							if (count > buffer.Length - index)
							{
								throw new ArgumentOutOfRangeException("count");
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.RawData).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteRawAsync>d__148>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.SaveAttrValue)
						{
							xmlWellFormedWriter.attrValueCache.WriteRaw(buffer, index, count);
							goto IL_182;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteRawAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteRawAsync>d__148>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_17B:
						configuredTaskAwaiter.GetResult();
						IL_182:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001031 RID: 4145 RVA: 0x00061EFC File Offset: 0x000600FC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BA2 RID: 2978
			public int <>1__state;

			// Token: 0x04000BA3 RID: 2979
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BA4 RID: 2980
			public char[] buffer;

			// Token: 0x04000BA5 RID: 2981
			public int index;

			// Token: 0x04000BA6 RID: 2982
			public int count;

			// Token: 0x04000BA7 RID: 2983
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BA8 RID: 2984
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D4 RID: 468
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawAsync>d__149 : IAsyncStateMachine
		{
			// Token: 0x06001032 RID: 4146 RVA: 0x00061F0C File Offset: 0x0006010C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_110;
							}
							if (data == null)
							{
								goto IL_13F;
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.RawData).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteRawAsync>d__149>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.SaveAttrValue)
						{
							xmlWellFormedWriter.attrValueCache.WriteRaw(data);
							goto IL_117;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteRawAsync(data).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteRawAsync>d__149>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_110:
						configuredTaskAwaiter.GetResult();
						IL_117:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_13F:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001033 RID: 4147 RVA: 0x000620A0 File Offset: 0x000602A0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BA9 RID: 2985
			public int <>1__state;

			// Token: 0x04000BAA RID: 2986
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BAB RID: 2987
			public string data;

			// Token: 0x04000BAC RID: 2988
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BAD RID: 2989
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D5 RID: 469
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteBase64Async_NoAdvanceState>d__151 : IAsyncStateMachine
		{
			// Token: 0x06001034 RID: 4148 RVA: 0x000620B0 File Offset: 0x000602B0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_F2;
							}
							configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteBase64Async_NoAdvanceState>d__151>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteBase64Async_NoAdvanceState>d__151>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_F2:
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001035 RID: 4149 RVA: 0x00062210 File Offset: 0x00060410
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BAE RID: 2990
			public int <>1__state;

			// Token: 0x04000BAF RID: 2991
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BB0 RID: 2992
			public Task task;

			// Token: 0x04000BB1 RID: 2993
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BB2 RID: 2994
			public byte[] buffer;

			// Token: 0x04000BB3 RID: 2995
			public int index;

			// Token: 0x04000BB4 RID: 2996
			public int count;

			// Token: 0x04000BB5 RID: 2997
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D6 RID: 470
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FlushAsync>d__152 : IAsyncStateMachine
		{
			// Token: 0x06001036 RID: 4150 RVA: 0x00062220 File Offset: 0x00060420
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							configuredTaskAwaiter = xmlWellFormedWriter.writer.FlushAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<FlushAsync>d__152>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001037 RID: 4151 RVA: 0x00062300 File Offset: 0x00060500
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BB6 RID: 2998
			public int <>1__state;

			// Token: 0x04000BB7 RID: 2999
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BB8 RID: 3000
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BB9 RID: 3001
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D7 RID: 471
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteQualifiedNameAsync>d__153 : IAsyncStateMachine
		{
			// Token: 0x06001038 RID: 4152 RVA: 0x00062310 File Offset: 0x00060510
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_1CA;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_233;
						}
						case 3:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_29D;
						}
						case 4:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_317;
						}
						default:
							if (localName == null || localName.Length == 0)
							{
								throw new ArgumentException(Res.GetString("The empty string '' is not a valid local name."));
							}
							xmlWellFormedWriter.CheckNCName(localName);
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteQualifiedNameAsync>d__153>(ref configuredTaskAwaiter, ref this);
								return;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						prefix = string.Empty;
						if (ns != null && ns.Length != 0)
						{
							prefix = xmlWellFormedWriter.LookupPrefix(ns);
							if (prefix == null)
							{
								if (xmlWellFormedWriter.currentState != XmlWellFormedWriter.State.Attribute)
								{
									throw new ArgumentException(Res.GetString("The '{0}' namespace is not defined.", new object[]
									{
										ns
									}));
								}
								prefix = xmlWellFormedWriter.GeneratePrefix();
								xmlWellFormedWriter.PushNamespaceImplicit(prefix, ns);
							}
						}
						if (xmlWellFormedWriter.SaveAttrValue || xmlWellFormedWriter.rawWriter == null)
						{
							if (prefix.Length == 0)
							{
								goto IL_23A;
							}
							configuredTaskAwaiter = xmlWellFormedWriter.WriteStringAsync(prefix).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteQualifiedNameAsync>d__153>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteQualifiedNameAsync(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 4;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteQualifiedNameAsync>d__153>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_317;
						}
						IL_1CA:
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.WriteStringAsync(":").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteQualifiedNameAsync>d__153>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_233:
						configuredTaskAwaiter.GetResult();
						IL_23A:
						configuredTaskAwaiter = xmlWellFormedWriter.WriteStringAsync(localName).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteQualifiedNameAsync>d__153>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_29D:
						configuredTaskAwaiter.GetResult();
						goto IL_31E;
						IL_317:
						configuredTaskAwaiter.GetResult();
						IL_31E:
						prefix = null;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001039 RID: 4153 RVA: 0x000626B4 File Offset: 0x000608B4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BBA RID: 3002
			public int <>1__state;

			// Token: 0x04000BBB RID: 3003
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BBC RID: 3004
			public string localName;

			// Token: 0x04000BBD RID: 3005
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BBE RID: 3006
			public string ns;

			// Token: 0x04000BBF RID: 3007
			private string <prefix>5__1;

			// Token: 0x04000BC0 RID: 3008
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D8 RID: 472
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteBinHexAsync>d__154 : IAsyncStateMachine
		{
			// Token: 0x0600103A RID: 4154 RVA: 0x000626C4 File Offset: 0x000608C4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					if (num > 1 && xmlWellFormedWriter.IsClosedOrErrorState)
					{
						throw new InvalidOperationException(Res.GetString("The Writer is closed or in error state."));
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (num == 1)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_107;
							}
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.Text).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteBinHexAsync>d__154>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						configuredTaskAwaiter = xmlWellFormedWriter.<>n__0(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteBinHexAsync>d__154>(ref configuredTaskAwaiter, ref this);
							return;
						}
						IL_107:
						configuredTaskAwaiter.GetResult();
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600103B RID: 4155 RVA: 0x00062850 File Offset: 0x00060A50
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BC1 RID: 3009
			public int <>1__state;

			// Token: 0x04000BC2 RID: 3010
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BC3 RID: 3011
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BC4 RID: 3012
			public byte[] buffer;

			// Token: 0x04000BC5 RID: 3013
			public int index;

			// Token: 0x04000BC6 RID: 3014
			public int count;

			// Token: 0x04000BC7 RID: 3015
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001D9 RID: 473
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartDocumentImplAsync>d__155 : IAsyncStateMachine
		{
			// Token: 0x0600103C RID: 4156 RVA: 0x00062860 File Offset: 0x00060A60
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						switch (num)
						{
						case 0:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							break;
						}
						case 1:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_13A;
						}
						case 2:
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_1A2;
						}
						default:
							configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(XmlWellFormedWriter.Token.StartDocument).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStartDocumentImplAsync>d__155>(ref configuredTaskAwaiter, ref this);
								return;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						if (xmlWellFormedWriter.conformanceLevel == ConformanceLevel.Auto)
						{
							xmlWellFormedWriter.conformanceLevel = ConformanceLevel.Document;
							xmlWellFormedWriter.stateTable = XmlWellFormedWriter.StateTableDocument;
						}
						else if (xmlWellFormedWriter.conformanceLevel == ConformanceLevel.Fragment)
						{
							throw new InvalidOperationException(Res.GetString("WriteStartDocument cannot be called on writers created with ConformanceLevel.Fragment."));
						}
						if (xmlWellFormedWriter.rawWriter != null)
						{
							if (xmlWellFormedWriter.xmlDeclFollows)
							{
								goto IL_1A9;
							}
							configuredTaskAwaiter = xmlWellFormedWriter.rawWriter.WriteXmlDeclarationAsync(standalone).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStartDocumentImplAsync>d__155>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xmlWellFormedWriter.writer.WriteStartDocumentAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 2;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<WriteStartDocumentImplAsync>d__155>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_1A2;
						}
						IL_13A:
						configuredTaskAwaiter.GetResult();
						goto IL_1A9;
						IL_1A2:
						configuredTaskAwaiter.GetResult();
						IL_1A9:;
					}
					catch
					{
						xmlWellFormedWriter.currentState = XmlWellFormedWriter.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600103D RID: 4157 RVA: 0x00062A88 File Offset: 0x00060C88
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BC8 RID: 3016
			public int <>1__state;

			// Token: 0x04000BC9 RID: 3017
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BCA RID: 3018
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BCB RID: 3019
			public XmlStandalone standalone;

			// Token: 0x04000BCC RID: 3020
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001DA RID: 474
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_AdvanceStateAsync_ReturnWhenFinish>d__157 : IAsyncStateMachine
		{
			// Token: 0x0600103E RID: 4158 RVA: 0x00062A98 File Offset: 0x00060C98
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<_AdvanceStateAsync_ReturnWhenFinish>d__157>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xmlWellFormedWriter.currentState = newState;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600103F RID: 4159 RVA: 0x00062B64 File Offset: 0x00060D64
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BCD RID: 3021
			public int <>1__state;

			// Token: 0x04000BCE RID: 3022
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BCF RID: 3023
			public Task task;

			// Token: 0x04000BD0 RID: 3024
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BD1 RID: 3025
			public XmlWellFormedWriter.State newState;

			// Token: 0x04000BD2 RID: 3026
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001DB RID: 475
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_AdvanceStateAsync_ContinueWhenFinish>d__159 : IAsyncStateMachine
		{
			// Token: 0x06001040 RID: 4160 RVA: 0x00062B74 File Offset: 0x00060D74
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_E8;
						}
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<_AdvanceStateAsync_ContinueWhenFinish>d__159>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xmlWellFormedWriter.currentState = newState;
					configuredTaskAwaiter = xmlWellFormedWriter.AdvanceStateAsync(token).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<_AdvanceStateAsync_ContinueWhenFinish>d__159>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_E8:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001041 RID: 4161 RVA: 0x00062CB0 File Offset: 0x00060EB0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BD3 RID: 3027
			public int <>1__state;

			// Token: 0x04000BD4 RID: 3028
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BD5 RID: 3029
			public Task task;

			// Token: 0x04000BD6 RID: 3030
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BD7 RID: 3031
			public XmlWellFormedWriter.State newState;

			// Token: 0x04000BD8 RID: 3032
			public XmlWellFormedWriter.Token token;

			// Token: 0x04000BD9 RID: 3033
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001DC RID: 476
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <StartElementContentAsync_WithNS>d__161 : IAsyncStateMachine
		{
			// Token: 0x06001042 RID: 4162 RVA: 0x00062CC0 File Offset: 0x00060EC0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlWellFormedWriter xmlWellFormedWriter = this;
				try
				{
					if (num != 0)
					{
						start = xmlWellFormedWriter.elemScopeStack[xmlWellFormedWriter.elemTop].prevNSTop;
						i = xmlWellFormedWriter.nsTop;
						goto IL_EF;
					}
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_D6:
					configuredTaskAwaiter.GetResult();
					IL_DD:
					int num3 = i;
					i = num3 - 1;
					IL_EF:
					if (i <= start)
					{
						if (xmlWellFormedWriter.rawWriter != null)
						{
							xmlWellFormedWriter.rawWriter.StartElementContent();
						}
					}
					else
					{
						if (xmlWellFormedWriter.nsStack[i].kind != XmlWellFormedWriter.NamespaceKind.NeedToWrite)
						{
							goto IL_DD;
						}
						configuredTaskAwaiter = xmlWellFormedWriter.nsStack[i].WriteDeclAsync(xmlWellFormedWriter.writer, xmlWellFormedWriter.rawWriter).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlWellFormedWriter.<StartElementContentAsync_WithNS>d__161>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_D6;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001043 RID: 4163 RVA: 0x00062E2C File Offset: 0x0006102C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000BDA RID: 3034
			public int <>1__state;

			// Token: 0x04000BDB RID: 3035
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000BDC RID: 3036
			public XmlWellFormedWriter <>4__this;

			// Token: 0x04000BDD RID: 3037
			private int <i>5__1;

			// Token: 0x04000BDE RID: 3038
			private int <start>5__2;

			// Token: 0x04000BDF RID: 3039
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
