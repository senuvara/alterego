﻿using System;

namespace System.Xml
{
	/// <summary>Specifies the state of the reader.</summary>
	// Token: 0x020000C0 RID: 192
	public enum ReadState
	{
		/// <summary>The <see langword="Read" /> method has not been called.</summary>
		// Token: 0x040003BB RID: 955
		Initial,
		/// <summary>The <see langword="Read" /> method has been called. Additional methods may be called on the reader.</summary>
		// Token: 0x040003BC RID: 956
		Interactive,
		/// <summary>An error occurred that prevents the read operation from continuing.</summary>
		// Token: 0x040003BD RID: 957
		Error,
		/// <summary>The end of the file has been reached successfully.</summary>
		// Token: 0x040003BE RID: 958
		EndOfFile,
		/// <summary>The <see cref="M:System.Xml.XmlReader.Close" /> method has been called.</summary>
		// Token: 0x040003BF RID: 959
		Closed
	}
}
