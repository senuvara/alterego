﻿using System;

namespace System.Xml
{
	/// <summary>Specifies the current <see langword="xml:space" /> scope.</summary>
	// Token: 0x02000111 RID: 273
	public enum XmlSpace
	{
		/// <summary>No <see langword="xml:space" /> scope.</summary>
		// Token: 0x040005D1 RID: 1489
		None,
		/// <summary>The <see langword="xml:space" /> scope equals <see langword="default" />.</summary>
		// Token: 0x040005D2 RID: 1490
		Default,
		/// <summary>The <see langword="xml:space" /> scope equals <see langword="preserve" />.</summary>
		// Token: 0x040005D3 RID: 1491
		Preserve
	}
}
