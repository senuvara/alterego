﻿using System;
using System.Collections.Generic;

namespace System.Xml
{
	// Token: 0x020000D7 RID: 215
	internal class XmlCharCheckingReaderWithNS : XmlCharCheckingReader, IXmlNamespaceResolver
	{
		// Token: 0x0600078B RID: 1931 RVA: 0x0001EF2A File Offset: 0x0001D12A
		internal XmlCharCheckingReaderWithNS(XmlReader reader, IXmlNamespaceResolver readerAsNSResolver, bool checkCharacters, bool ignoreWhitespace, bool ignoreComments, bool ignorePis, DtdProcessing dtdProcessing) : base(reader, checkCharacters, ignoreWhitespace, ignoreComments, ignorePis, dtdProcessing)
		{
			this.readerAsNSResolver = readerAsNSResolver;
		}

		// Token: 0x0600078C RID: 1932 RVA: 0x0001EF43 File Offset: 0x0001D143
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return this.readerAsNSResolver.GetNamespacesInScope(scope);
		}

		// Token: 0x0600078D RID: 1933 RVA: 0x0001EF51 File Offset: 0x0001D151
		string IXmlNamespaceResolver.LookupNamespace(string prefix)
		{
			return this.readerAsNSResolver.LookupNamespace(prefix);
		}

		// Token: 0x0600078E RID: 1934 RVA: 0x0001EF5F File Offset: 0x0001D15F
		string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
		{
			return this.readerAsNSResolver.LookupPrefix(namespaceName);
		}

		// Token: 0x0400041A RID: 1050
		internal IXmlNamespaceResolver readerAsNSResolver;
	}
}
