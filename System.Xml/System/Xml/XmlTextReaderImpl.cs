﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using System.Xml.XmlConfiguration;

namespace System.Xml
{
	// Token: 0x02000123 RID: 291
	internal class XmlTextReaderImpl : XmlReader, IXmlLineInfo, IXmlNamespaceResolver
	{
		// Token: 0x06000B02 RID: 2818 RVA: 0x000320AC File Offset: 0x000302AC
		internal XmlTextReaderImpl()
		{
			this.curNode = new XmlTextReaderImpl.NodeData();
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.NoData;
		}

		// Token: 0x06000B03 RID: 2819 RVA: 0x00032134 File Offset: 0x00030334
		internal XmlTextReaderImpl(XmlNameTable nt)
		{
			this.v1Compat = true;
			this.outerReader = this;
			this.nameTable = nt;
			nt.Add(string.Empty);
			if (!XmlReaderSettings.EnableLegacyXmlSettings())
			{
				this.xmlResolver = null;
			}
			else
			{
				this.xmlResolver = new XmlUrlResolver();
			}
			this.Xml = nt.Add("xml");
			this.XmlNs = nt.Add("xmlns");
			this.nodes = new XmlTextReaderImpl.NodeData[8];
			this.nodes[0] = new XmlTextReaderImpl.NodeData();
			this.curNode = this.nodes[0];
			this.stringBuilder = new StringBuilder();
			this.xmlContext = new XmlTextReaderImpl.XmlContext();
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.SwitchToInteractiveXmlDecl;
			this.nextParsingFunction = XmlTextReaderImpl.ParsingFunction.DocumentContent;
			this.entityHandling = EntityHandling.ExpandCharEntities;
			this.whitespaceHandling = WhitespaceHandling.All;
			this.closeInput = true;
			this.maxCharactersInDocument = 0L;
			this.maxCharactersFromEntities = 10000000L;
			this.charactersInDocument = 0L;
			this.charactersFromEntities = 0L;
			this.ps.lineNo = 1;
			this.ps.lineStartPos = -1;
		}

		// Token: 0x06000B04 RID: 2820 RVA: 0x000322A4 File Offset: 0x000304A4
		private XmlTextReaderImpl(XmlResolver resolver, XmlReaderSettings settings, XmlParserContext context)
		{
			this.useAsync = settings.Async;
			this.v1Compat = false;
			this.outerReader = this;
			this.xmlContext = new XmlTextReaderImpl.XmlContext();
			XmlNameTable xmlNameTable = settings.NameTable;
			if (context == null)
			{
				if (xmlNameTable == null)
				{
					xmlNameTable = new NameTable();
				}
				else
				{
					this.nameTableFromSettings = true;
				}
				this.nameTable = xmlNameTable;
				this.namespaceManager = new XmlNamespaceManager(xmlNameTable);
			}
			else
			{
				this.SetupFromParserContext(context, settings);
				xmlNameTable = this.nameTable;
			}
			xmlNameTable.Add(string.Empty);
			this.Xml = xmlNameTable.Add("xml");
			this.XmlNs = xmlNameTable.Add("xmlns");
			this.xmlResolver = resolver;
			this.nodes = new XmlTextReaderImpl.NodeData[8];
			this.nodes[0] = new XmlTextReaderImpl.NodeData();
			this.curNode = this.nodes[0];
			this.stringBuilder = new StringBuilder();
			this.entityHandling = EntityHandling.ExpandEntities;
			this.xmlResolverIsSet = settings.IsXmlResolverSet;
			this.whitespaceHandling = (settings.IgnoreWhitespace ? WhitespaceHandling.Significant : WhitespaceHandling.All);
			this.normalize = true;
			this.ignorePIs = settings.IgnoreProcessingInstructions;
			this.ignoreComments = settings.IgnoreComments;
			this.checkCharacters = settings.CheckCharacters;
			this.lineNumberOffset = settings.LineNumberOffset;
			this.linePositionOffset = settings.LinePositionOffset;
			this.ps.lineNo = this.lineNumberOffset + 1;
			this.ps.lineStartPos = -this.linePositionOffset - 1;
			this.curNode.SetLineInfo(this.ps.LineNo - 1, this.ps.LinePos - 1);
			this.dtdProcessing = settings.DtdProcessing;
			this.maxCharactersInDocument = settings.MaxCharactersInDocument;
			this.maxCharactersFromEntities = settings.MaxCharactersFromEntities;
			this.charactersInDocument = 0L;
			this.charactersFromEntities = 0L;
			this.fragmentParserContext = context;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.SwitchToInteractiveXmlDecl;
			this.nextParsingFunction = XmlTextReaderImpl.ParsingFunction.DocumentContent;
			switch (settings.ConformanceLevel)
			{
			case ConformanceLevel.Auto:
				this.fragmentType = XmlNodeType.None;
				this.fragment = true;
				return;
			case ConformanceLevel.Fragment:
				this.fragmentType = XmlNodeType.Element;
				this.fragment = true;
				return;
			}
			this.fragmentType = XmlNodeType.Document;
		}

		// Token: 0x06000B05 RID: 2821 RVA: 0x00032521 File Offset: 0x00030721
		internal XmlTextReaderImpl(Stream input) : this(string.Empty, input, new NameTable())
		{
		}

		// Token: 0x06000B06 RID: 2822 RVA: 0x00032534 File Offset: 0x00030734
		internal XmlTextReaderImpl(Stream input, XmlNameTable nt) : this(string.Empty, input, nt)
		{
		}

		// Token: 0x06000B07 RID: 2823 RVA: 0x00032543 File Offset: 0x00030743
		internal XmlTextReaderImpl(string url, Stream input) : this(url, input, new NameTable())
		{
		}

		// Token: 0x06000B08 RID: 2824 RVA: 0x00032554 File Offset: 0x00030754
		internal XmlTextReaderImpl(string url, Stream input, XmlNameTable nt) : this(nt)
		{
			this.namespaceManager = new XmlNamespaceManager(nt);
			if (url == null || url.Length == 0)
			{
				this.InitStreamInput(input, null);
			}
			else
			{
				this.InitStreamInput(url, input, null);
			}
			this.reportedBaseUri = this.ps.baseUriStr;
			this.reportedEncoding = this.ps.encoding;
		}

		// Token: 0x06000B09 RID: 2825 RVA: 0x000325B4 File Offset: 0x000307B4
		internal XmlTextReaderImpl(TextReader input) : this(string.Empty, input, new NameTable())
		{
		}

		// Token: 0x06000B0A RID: 2826 RVA: 0x000325C7 File Offset: 0x000307C7
		internal XmlTextReaderImpl(TextReader input, XmlNameTable nt) : this(string.Empty, input, nt)
		{
		}

		// Token: 0x06000B0B RID: 2827 RVA: 0x000325D6 File Offset: 0x000307D6
		internal XmlTextReaderImpl(string url, TextReader input) : this(url, input, new NameTable())
		{
		}

		// Token: 0x06000B0C RID: 2828 RVA: 0x000325E8 File Offset: 0x000307E8
		internal XmlTextReaderImpl(string url, TextReader input, XmlNameTable nt) : this(nt)
		{
			this.namespaceManager = new XmlNamespaceManager(nt);
			this.reportedBaseUri = ((url != null) ? url : string.Empty);
			this.InitTextReaderInput(this.reportedBaseUri, input);
			this.reportedEncoding = this.ps.encoding;
		}

		// Token: 0x06000B0D RID: 2829 RVA: 0x00032638 File Offset: 0x00030838
		internal XmlTextReaderImpl(Stream xmlFragment, XmlNodeType fragType, XmlParserContext context) : this((context != null && context.NameTable != null) ? context.NameTable : new NameTable())
		{
			Encoding encoding = (context != null) ? context.Encoding : null;
			if (context == null || context.BaseURI == null || context.BaseURI.Length == 0)
			{
				this.InitStreamInput(xmlFragment, encoding);
			}
			else
			{
				this.InitStreamInput(this.GetTempResolver().ResolveUri(null, context.BaseURI), xmlFragment, encoding);
			}
			this.InitFragmentReader(fragType, context, false);
			this.reportedBaseUri = this.ps.baseUriStr;
			this.reportedEncoding = this.ps.encoding;
		}

		// Token: 0x06000B0E RID: 2830 RVA: 0x000326D8 File Offset: 0x000308D8
		internal XmlTextReaderImpl(string xmlFragment, XmlNodeType fragType, XmlParserContext context) : this((context == null || context.NameTable == null) ? new NameTable() : context.NameTable)
		{
			if (xmlFragment == null)
			{
				xmlFragment = string.Empty;
			}
			if (context == null)
			{
				this.InitStringInput(string.Empty, Encoding.Unicode, xmlFragment);
			}
			else
			{
				this.reportedBaseUri = context.BaseURI;
				this.InitStringInput(context.BaseURI, Encoding.Unicode, xmlFragment);
			}
			this.InitFragmentReader(fragType, context, false);
			this.reportedEncoding = this.ps.encoding;
		}

		// Token: 0x06000B0F RID: 2831 RVA: 0x0003275C File Offset: 0x0003095C
		internal XmlTextReaderImpl(string xmlFragment, XmlParserContext context) : this((context == null || context.NameTable == null) ? new NameTable() : context.NameTable)
		{
			this.InitStringInput((context == null) ? string.Empty : context.BaseURI, Encoding.Unicode, "<?xml " + xmlFragment + "?>");
			this.InitFragmentReader(XmlNodeType.XmlDeclaration, context, true);
		}

		// Token: 0x06000B10 RID: 2832 RVA: 0x000327BC File Offset: 0x000309BC
		public XmlTextReaderImpl(string url) : this(url, new NameTable())
		{
		}

		// Token: 0x06000B11 RID: 2833 RVA: 0x000327CC File Offset: 0x000309CC
		public XmlTextReaderImpl(string url, XmlNameTable nt) : this(nt)
		{
			if (url == null)
			{
				throw new ArgumentNullException("url");
			}
			if (url.Length == 0)
			{
				throw new ArgumentException(Res.GetString("The URL cannot be empty."), "url");
			}
			this.namespaceManager = new XmlNamespaceManager(nt);
			this.url = url;
			this.ps.baseUri = this.GetTempResolver().ResolveUri(null, url);
			this.ps.baseUriStr = this.ps.baseUri.ToString();
			this.reportedBaseUri = this.ps.baseUriStr;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.OpenUrl;
		}

		// Token: 0x06000B12 RID: 2834 RVA: 0x0003286C File Offset: 0x00030A6C
		internal XmlTextReaderImpl(string uriStr, XmlReaderSettings settings, XmlParserContext context, XmlResolver uriResolver) : this(settings.GetXmlResolver(), settings, context)
		{
			Uri uri = uriResolver.ResolveUri(null, uriStr);
			string text = uri.ToString();
			if (context != null && context.BaseURI != null && context.BaseURI.Length > 0 && !this.UriEqual(uri, text, context.BaseURI, settings.GetXmlResolver()))
			{
				if (text.Length > 0)
				{
					this.Throw("BaseUri must be specified either as an argument of XmlReader.Create or on the XmlParserContext. If it is specified on both, it must be the same base URI.");
				}
				text = context.BaseURI;
			}
			this.reportedBaseUri = text;
			this.closeInput = true;
			this.laterInitParam = new XmlTextReaderImpl.LaterInitParam();
			this.laterInitParam.inputUriStr = uriStr;
			this.laterInitParam.inputbaseUri = uri;
			this.laterInitParam.inputContext = context;
			this.laterInitParam.inputUriResolver = uriResolver;
			this.laterInitParam.initType = XmlTextReaderImpl.InitInputType.UriString;
			if (!settings.Async)
			{
				this.FinishInitUriString();
				return;
			}
			this.laterInitParam.useAsync = true;
		}

		// Token: 0x06000B13 RID: 2835 RVA: 0x00032954 File Offset: 0x00030B54
		private void FinishInitUriString()
		{
			Stream stream = null;
			if (this.laterInitParam.useAsync)
			{
				Task<object> entityAsync = this.laterInitParam.inputUriResolver.GetEntityAsync(this.laterInitParam.inputbaseUri, string.Empty, typeof(Stream));
				entityAsync.Wait();
				stream = (Stream)entityAsync.Result;
			}
			else
			{
				stream = (Stream)this.laterInitParam.inputUriResolver.GetEntity(this.laterInitParam.inputbaseUri, string.Empty, typeof(Stream));
			}
			if (stream == null)
			{
				throw new XmlException("Cannot resolve '{0}'.", this.laterInitParam.inputUriStr);
			}
			Encoding encoding = null;
			if (this.laterInitParam.inputContext != null)
			{
				encoding = this.laterInitParam.inputContext.Encoding;
			}
			try
			{
				this.InitStreamInput(this.laterInitParam.inputbaseUri, this.reportedBaseUri, stream, null, 0, encoding);
				this.reportedEncoding = this.ps.encoding;
				if (this.laterInitParam.inputContext != null && this.laterInitParam.inputContext.HasDtdInfo)
				{
					this.ProcessDtdFromParserContext(this.laterInitParam.inputContext);
				}
			}
			catch
			{
				stream.Close();
				throw;
			}
			this.laterInitParam = null;
		}

		// Token: 0x06000B14 RID: 2836 RVA: 0x00032A98 File Offset: 0x00030C98
		internal XmlTextReaderImpl(Stream stream, byte[] bytes, int byteCount, XmlReaderSettings settings, Uri baseUri, string baseUriStr, XmlParserContext context, bool closeInput) : this(settings.GetXmlResolver(), settings, context)
		{
			if (context != null && context.BaseURI != null && context.BaseURI.Length > 0 && !this.UriEqual(baseUri, baseUriStr, context.BaseURI, settings.GetXmlResolver()))
			{
				if (baseUriStr.Length > 0)
				{
					this.Throw("BaseUri must be specified either as an argument of XmlReader.Create or on the XmlParserContext. If it is specified on both, it must be the same base URI.");
				}
				baseUriStr = context.BaseURI;
			}
			this.reportedBaseUri = baseUriStr;
			this.closeInput = closeInput;
			this.laterInitParam = new XmlTextReaderImpl.LaterInitParam();
			this.laterInitParam.inputStream = stream;
			this.laterInitParam.inputBytes = bytes;
			this.laterInitParam.inputByteCount = byteCount;
			this.laterInitParam.inputbaseUri = baseUri;
			this.laterInitParam.inputContext = context;
			this.laterInitParam.initType = XmlTextReaderImpl.InitInputType.Stream;
			if (!settings.Async)
			{
				this.FinishInitStream();
				return;
			}
			this.laterInitParam.useAsync = true;
		}

		// Token: 0x06000B15 RID: 2837 RVA: 0x00032B8C File Offset: 0x00030D8C
		private void FinishInitStream()
		{
			Encoding encoding = null;
			if (this.laterInitParam.inputContext != null)
			{
				encoding = this.laterInitParam.inputContext.Encoding;
			}
			this.InitStreamInput(this.laterInitParam.inputbaseUri, this.reportedBaseUri, this.laterInitParam.inputStream, this.laterInitParam.inputBytes, this.laterInitParam.inputByteCount, encoding);
			this.reportedEncoding = this.ps.encoding;
			if (this.laterInitParam.inputContext != null && this.laterInitParam.inputContext.HasDtdInfo)
			{
				this.ProcessDtdFromParserContext(this.laterInitParam.inputContext);
			}
			this.laterInitParam = null;
		}

		// Token: 0x06000B16 RID: 2838 RVA: 0x00032C3C File Offset: 0x00030E3C
		internal XmlTextReaderImpl(TextReader input, XmlReaderSettings settings, string baseUriStr, XmlParserContext context) : this(settings.GetXmlResolver(), settings, context)
		{
			if (context != null && context.BaseURI != null)
			{
				baseUriStr = context.BaseURI;
			}
			this.reportedBaseUri = baseUriStr;
			this.closeInput = settings.CloseInput;
			this.laterInitParam = new XmlTextReaderImpl.LaterInitParam();
			this.laterInitParam.inputTextReader = input;
			this.laterInitParam.inputContext = context;
			this.laterInitParam.initType = XmlTextReaderImpl.InitInputType.TextReader;
			if (!settings.Async)
			{
				this.FinishInitTextReader();
				return;
			}
			this.laterInitParam.useAsync = true;
		}

		// Token: 0x06000B17 RID: 2839 RVA: 0x00032CCC File Offset: 0x00030ECC
		private void FinishInitTextReader()
		{
			this.InitTextReaderInput(this.reportedBaseUri, this.laterInitParam.inputTextReader);
			this.reportedEncoding = this.ps.encoding;
			if (this.laterInitParam.inputContext != null && this.laterInitParam.inputContext.HasDtdInfo)
			{
				this.ProcessDtdFromParserContext(this.laterInitParam.inputContext);
			}
			this.laterInitParam = null;
		}

		// Token: 0x06000B18 RID: 2840 RVA: 0x00032D38 File Offset: 0x00030F38
		internal XmlTextReaderImpl(string xmlFragment, XmlParserContext context, XmlReaderSettings settings) : this(null, settings, context)
		{
			this.InitStringInput(string.Empty, Encoding.Unicode, xmlFragment);
			this.reportedBaseUri = this.ps.baseUriStr;
			this.reportedEncoding = this.ps.encoding;
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x06000B19 RID: 2841 RVA: 0x00032D78 File Offset: 0x00030F78
		public override XmlReaderSettings Settings
		{
			get
			{
				XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
				if (this.nameTableFromSettings)
				{
					xmlReaderSettings.NameTable = this.nameTable;
				}
				XmlNodeType xmlNodeType = this.fragmentType;
				if (xmlNodeType != XmlNodeType.None)
				{
					if (xmlNodeType == XmlNodeType.Element)
					{
						xmlReaderSettings.ConformanceLevel = ConformanceLevel.Fragment;
						goto IL_46;
					}
					if (xmlNodeType == XmlNodeType.Document)
					{
						xmlReaderSettings.ConformanceLevel = ConformanceLevel.Document;
						goto IL_46;
					}
				}
				xmlReaderSettings.ConformanceLevel = ConformanceLevel.Auto;
				IL_46:
				xmlReaderSettings.CheckCharacters = this.checkCharacters;
				xmlReaderSettings.LineNumberOffset = this.lineNumberOffset;
				xmlReaderSettings.LinePositionOffset = this.linePositionOffset;
				xmlReaderSettings.IgnoreWhitespace = (this.whitespaceHandling == WhitespaceHandling.Significant);
				xmlReaderSettings.IgnoreProcessingInstructions = this.ignorePIs;
				xmlReaderSettings.IgnoreComments = this.ignoreComments;
				xmlReaderSettings.DtdProcessing = this.dtdProcessing;
				xmlReaderSettings.MaxCharactersInDocument = this.maxCharactersInDocument;
				xmlReaderSettings.MaxCharactersFromEntities = this.maxCharactersFromEntities;
				if (!XmlReaderSettings.EnableLegacyXmlSettings())
				{
					xmlReaderSettings.XmlResolver = this.xmlResolver;
				}
				xmlReaderSettings.ReadOnly = true;
				return xmlReaderSettings;
			}
		}

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x06000B1A RID: 2842 RVA: 0x00032E55 File Offset: 0x00031055
		public override XmlNodeType NodeType
		{
			get
			{
				return this.curNode.type;
			}
		}

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x06000B1B RID: 2843 RVA: 0x00032E62 File Offset: 0x00031062
		public override string Name
		{
			get
			{
				return this.curNode.GetNameWPrefix(this.nameTable);
			}
		}

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x06000B1C RID: 2844 RVA: 0x00032E75 File Offset: 0x00031075
		public override string LocalName
		{
			get
			{
				return this.curNode.localName;
			}
		}

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x06000B1D RID: 2845 RVA: 0x00032E82 File Offset: 0x00031082
		public override string NamespaceURI
		{
			get
			{
				return this.curNode.ns;
			}
		}

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x06000B1E RID: 2846 RVA: 0x00032E8F File Offset: 0x0003108F
		public override string Prefix
		{
			get
			{
				return this.curNode.prefix;
			}
		}

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x06000B1F RID: 2847 RVA: 0x00032E9C File Offset: 0x0003109C
		public override string Value
		{
			get
			{
				if (this.parsingFunction >= XmlTextReaderImpl.ParsingFunction.PartialTextValue)
				{
					if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.PartialTextValue)
					{
						this.FinishPartialValue();
						this.parsingFunction = this.nextParsingFunction;
					}
					else
					{
						this.FinishOtherValueIterator();
					}
				}
				return this.curNode.StringValue;
			}
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x06000B20 RID: 2848 RVA: 0x00032ED7 File Offset: 0x000310D7
		public override int Depth
		{
			get
			{
				return this.curNode.depth;
			}
		}

		// Token: 0x17000202 RID: 514
		// (get) Token: 0x06000B21 RID: 2849 RVA: 0x00032EE4 File Offset: 0x000310E4
		public override string BaseURI
		{
			get
			{
				return this.reportedBaseUri;
			}
		}

		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000B22 RID: 2850 RVA: 0x00032EEC File Offset: 0x000310EC
		public override bool IsEmptyElement
		{
			get
			{
				return this.curNode.IsEmptyElement;
			}
		}

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000B23 RID: 2851 RVA: 0x00032EF9 File Offset: 0x000310F9
		public override bool IsDefault
		{
			get
			{
				return this.curNode.IsDefaultAttribute;
			}
		}

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000B24 RID: 2852 RVA: 0x00032F06 File Offset: 0x00031106
		public override char QuoteChar
		{
			get
			{
				if (this.curNode.type != XmlNodeType.Attribute)
				{
					return '"';
				}
				return this.curNode.quoteChar;
			}
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000B25 RID: 2853 RVA: 0x00032F24 File Offset: 0x00031124
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.xmlContext.xmlSpace;
			}
		}

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06000B26 RID: 2854 RVA: 0x00032F31 File Offset: 0x00031131
		public override string XmlLang
		{
			get
			{
				return this.xmlContext.xmlLang;
			}
		}

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x06000B27 RID: 2855 RVA: 0x00032F3E File Offset: 0x0003113E
		public override ReadState ReadState
		{
			get
			{
				return this.readState;
			}
		}

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000B28 RID: 2856 RVA: 0x00032F46 File Offset: 0x00031146
		public override bool EOF
		{
			get
			{
				return this.parsingFunction == XmlTextReaderImpl.ParsingFunction.Eof;
			}
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000B29 RID: 2857 RVA: 0x00032F52 File Offset: 0x00031152
		public override XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06000B2A RID: 2858 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool CanResolveEntity
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000B2B RID: 2859 RVA: 0x00032F5A File Offset: 0x0003115A
		public override int AttributeCount
		{
			get
			{
				return this.attrCount;
			}
		}

		// Token: 0x06000B2C RID: 2860 RVA: 0x00032F64 File Offset: 0x00031164
		public override string GetAttribute(string name)
		{
			int num;
			if (name.IndexOf(':') == -1)
			{
				num = this.GetIndexOfAttributeWithoutPrefix(name);
			}
			else
			{
				num = this.GetIndexOfAttributeWithPrefix(name);
			}
			if (num < 0)
			{
				return null;
			}
			return this.nodes[num].StringValue;
		}

		// Token: 0x06000B2D RID: 2861 RVA: 0x00032FA4 File Offset: 0x000311A4
		public override string GetAttribute(string localName, string namespaceURI)
		{
			namespaceURI = ((namespaceURI == null) ? string.Empty : this.nameTable.Get(namespaceURI));
			localName = this.nameTable.Get(localName);
			for (int i = this.index + 1; i < this.index + this.attrCount + 1; i++)
			{
				if (Ref.Equal(this.nodes[i].localName, localName) && Ref.Equal(this.nodes[i].ns, namespaceURI))
				{
					return this.nodes[i].StringValue;
				}
			}
			return null;
		}

		// Token: 0x06000B2E RID: 2862 RVA: 0x00033031 File Offset: 0x00031231
		public override string GetAttribute(int i)
		{
			if (i < 0 || i >= this.attrCount)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			return this.nodes[this.index + i + 1].StringValue;
		}

		// Token: 0x06000B2F RID: 2863 RVA: 0x00033064 File Offset: 0x00031264
		public override bool MoveToAttribute(string name)
		{
			int num;
			if (name.IndexOf(':') == -1)
			{
				num = this.GetIndexOfAttributeWithoutPrefix(name);
			}
			else
			{
				num = this.GetIndexOfAttributeWithPrefix(name);
			}
			if (num >= 0)
			{
				if (this.InAttributeValueIterator)
				{
					this.FinishAttributeValueIterator();
				}
				this.curAttrIndex = num - this.index - 1;
				this.curNode = this.nodes[num];
				return true;
			}
			return false;
		}

		// Token: 0x06000B30 RID: 2864 RVA: 0x000330C4 File Offset: 0x000312C4
		public override bool MoveToAttribute(string localName, string namespaceURI)
		{
			namespaceURI = ((namespaceURI == null) ? string.Empty : this.nameTable.Get(namespaceURI));
			localName = this.nameTable.Get(localName);
			for (int i = this.index + 1; i < this.index + this.attrCount + 1; i++)
			{
				if (Ref.Equal(this.nodes[i].localName, localName) && Ref.Equal(this.nodes[i].ns, namespaceURI))
				{
					this.curAttrIndex = i - this.index - 1;
					this.curNode = this.nodes[i];
					if (this.InAttributeValueIterator)
					{
						this.FinishAttributeValueIterator();
					}
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000B31 RID: 2865 RVA: 0x00033174 File Offset: 0x00031374
		public override void MoveToAttribute(int i)
		{
			if (i < 0 || i >= this.attrCount)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			if (this.InAttributeValueIterator)
			{
				this.FinishAttributeValueIterator();
			}
			this.curAttrIndex = i;
			this.curNode = this.nodes[this.index + 1 + this.curAttrIndex];
		}

		// Token: 0x06000B32 RID: 2866 RVA: 0x000331CA File Offset: 0x000313CA
		public override bool MoveToFirstAttribute()
		{
			if (this.attrCount == 0)
			{
				return false;
			}
			if (this.InAttributeValueIterator)
			{
				this.FinishAttributeValueIterator();
			}
			this.curAttrIndex = 0;
			this.curNode = this.nodes[this.index + 1];
			return true;
		}

		// Token: 0x06000B33 RID: 2867 RVA: 0x00033204 File Offset: 0x00031404
		public override bool MoveToNextAttribute()
		{
			if (this.curAttrIndex + 1 < this.attrCount)
			{
				if (this.InAttributeValueIterator)
				{
					this.FinishAttributeValueIterator();
				}
				XmlTextReaderImpl.NodeData[] array = this.nodes;
				int num = this.index + 1;
				int num2 = this.curAttrIndex + 1;
				this.curAttrIndex = num2;
				this.curNode = array[num + num2];
				return true;
			}
			return false;
		}

		// Token: 0x06000B34 RID: 2868 RVA: 0x00033259 File Offset: 0x00031459
		public override bool MoveToElement()
		{
			if (this.InAttributeValueIterator)
			{
				this.FinishAttributeValueIterator();
			}
			else if (this.curNode.type != XmlNodeType.Attribute)
			{
				return false;
			}
			this.curAttrIndex = -1;
			this.curNode = this.nodes[this.index];
			return true;
		}

		// Token: 0x06000B35 RID: 2869 RVA: 0x00033298 File Offset: 0x00031498
		private void FinishInit()
		{
			switch (this.laterInitParam.initType)
			{
			case XmlTextReaderImpl.InitInputType.UriString:
				this.FinishInitUriString();
				return;
			case XmlTextReaderImpl.InitInputType.Stream:
				this.FinishInitStream();
				return;
			case XmlTextReaderImpl.InitInputType.TextReader:
				this.FinishInitTextReader();
				return;
			default:
				return;
			}
		}

		// Token: 0x06000B36 RID: 2870 RVA: 0x000332D8 File Offset: 0x000314D8
		public override bool Read()
		{
			if (this.laterInitParam != null)
			{
				this.FinishInit();
			}
			for (;;)
			{
				switch (this.parsingFunction)
				{
				case XmlTextReaderImpl.ParsingFunction.ElementContent:
					goto IL_85;
				case XmlTextReaderImpl.ParsingFunction.NoData:
					goto IL_2E7;
				case XmlTextReaderImpl.ParsingFunction.OpenUrl:
					this.OpenUrl();
					break;
				case XmlTextReaderImpl.ParsingFunction.SwitchToInteractive:
					this.readState = ReadState.Interactive;
					this.parsingFunction = this.nextParsingFunction;
					continue;
				case XmlTextReaderImpl.ParsingFunction.SwitchToInteractiveXmlDecl:
					break;
				case XmlTextReaderImpl.ParsingFunction.DocumentContent:
					goto IL_8C;
				case XmlTextReaderImpl.ParsingFunction.MoveToElementContent:
					this.ResetAttributes();
					this.index++;
					this.curNode = this.AddNode(this.index, this.index);
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ElementContent;
					continue;
				case XmlTextReaderImpl.ParsingFunction.PopElementContext:
					this.PopElementContext();
					this.parsingFunction = this.nextParsingFunction;
					continue;
				case XmlTextReaderImpl.ParsingFunction.PopEmptyElementContext:
					this.curNode = this.nodes[this.index];
					this.curNode.IsEmptyElement = false;
					this.ResetAttributes();
					this.PopElementContext();
					this.parsingFunction = this.nextParsingFunction;
					continue;
				case XmlTextReaderImpl.ParsingFunction.ResetAttributesRootLevel:
					this.ResetAttributes();
					this.curNode = this.nodes[this.index];
					this.parsingFunction = ((this.index == 0) ? XmlTextReaderImpl.ParsingFunction.DocumentContent : XmlTextReaderImpl.ParsingFunction.ElementContent);
					continue;
				case XmlTextReaderImpl.ParsingFunction.Error:
				case XmlTextReaderImpl.ParsingFunction.Eof:
				case XmlTextReaderImpl.ParsingFunction.ReaderClosed:
					return false;
				case XmlTextReaderImpl.ParsingFunction.EntityReference:
					goto IL_1B3;
				case XmlTextReaderImpl.ParsingFunction.InIncrementalRead:
					goto IL_2BE;
				case XmlTextReaderImpl.ParsingFunction.FragmentAttribute:
					goto IL_2C6;
				case XmlTextReaderImpl.ParsingFunction.ReportEndEntity:
					goto IL_1C7;
				case XmlTextReaderImpl.ParsingFunction.AfterResolveEntityInContent:
					this.curNode = this.AddNode(this.index, this.index);
					this.reportedEncoding = this.ps.encoding;
					this.reportedBaseUri = this.ps.baseUriStr;
					this.parsingFunction = this.nextParsingFunction;
					continue;
				case XmlTextReaderImpl.ParsingFunction.AfterResolveEmptyEntityInContent:
					goto IL_226;
				case XmlTextReaderImpl.ParsingFunction.XmlDeclarationFragment:
					goto IL_2CD;
				case XmlTextReaderImpl.ParsingFunction.GoToEof:
					goto IL_2DD;
				case XmlTextReaderImpl.ParsingFunction.PartialTextValue:
					this.SkipPartialTextValue();
					continue;
				case XmlTextReaderImpl.ParsingFunction.InReadAttributeValue:
					this.FinishAttributeValueIterator();
					this.curNode = this.nodes[this.index];
					continue;
				case XmlTextReaderImpl.ParsingFunction.InReadValueChunk:
					this.FinishReadValueChunk();
					continue;
				case XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary:
					this.FinishReadContentAsBinary();
					continue;
				case XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary:
					this.FinishReadElementContentAsBinary();
					continue;
				default:
					continue;
				}
				this.readState = ReadState.Interactive;
				this.parsingFunction = this.nextParsingFunction;
				if (this.ParseXmlDeclaration(false))
				{
					goto Block_3;
				}
				this.reportedEncoding = this.ps.encoding;
			}
			IL_85:
			return this.ParseElementContent();
			IL_8C:
			return this.ParseDocumentContent();
			Block_3:
			this.reportedEncoding = this.ps.encoding;
			return true;
			IL_1B3:
			this.parsingFunction = this.nextParsingFunction;
			this.ParseEntityReference();
			return true;
			IL_1C7:
			this.SetupEndEntityNodeInContent();
			this.parsingFunction = this.nextParsingFunction;
			return true;
			IL_226:
			this.curNode = this.AddNode(this.index, this.index);
			this.curNode.SetValueNode(XmlNodeType.Text, string.Empty);
			this.curNode.SetLineInfo(this.ps.lineNo, this.ps.LinePos);
			this.reportedEncoding = this.ps.encoding;
			this.reportedBaseUri = this.ps.baseUriStr;
			this.parsingFunction = this.nextParsingFunction;
			return true;
			IL_2BE:
			this.FinishIncrementalRead();
			return true;
			IL_2C6:
			return this.ParseFragmentAttribute();
			IL_2CD:
			this.ParseXmlDeclarationFragment();
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.GoToEof;
			return true;
			IL_2DD:
			this.OnEof();
			return false;
			IL_2E7:
			this.ThrowWithoutLineInfo("Root element is missing.");
			return false;
		}

		// Token: 0x06000B37 RID: 2871 RVA: 0x00033604 File Offset: 0x00031804
		public override void Close()
		{
			this.Close(this.closeInput);
		}

		// Token: 0x06000B38 RID: 2872 RVA: 0x00033614 File Offset: 0x00031814
		public override void Skip()
		{
			if (this.readState != ReadState.Interactive)
			{
				return;
			}
			if (this.InAttributeValueIterator)
			{
				this.FinishAttributeValueIterator();
				this.curNode = this.nodes[this.index];
			}
			else
			{
				XmlTextReaderImpl.ParsingFunction parsingFunction = this.parsingFunction;
				if (parsingFunction != XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
				{
					switch (parsingFunction)
					{
					case XmlTextReaderImpl.ParsingFunction.PartialTextValue:
						this.SkipPartialTextValue();
						break;
					case XmlTextReaderImpl.ParsingFunction.InReadValueChunk:
						this.FinishReadValueChunk();
						break;
					case XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary:
						this.FinishReadContentAsBinary();
						break;
					case XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary:
						this.FinishReadElementContentAsBinary();
						break;
					}
				}
				else
				{
					this.FinishIncrementalRead();
				}
			}
			XmlNodeType type = this.curNode.type;
			if (type != XmlNodeType.Element)
			{
				if (type != XmlNodeType.Attribute)
				{
					goto IL_DC;
				}
				this.outerReader.MoveToElement();
			}
			if (!this.curNode.IsEmptyElement)
			{
				int num = this.index;
				this.parsingMode = XmlTextReaderImpl.ParsingMode.SkipContent;
				while (this.outerReader.Read() && this.index > num)
				{
				}
				this.parsingMode = XmlTextReaderImpl.ParsingMode.Full;
			}
			IL_DC:
			this.outerReader.Read();
		}

		// Token: 0x06000B39 RID: 2873 RVA: 0x00033709 File Offset: 0x00031909
		public override string LookupNamespace(string prefix)
		{
			if (!this.supportNamespaces)
			{
				return null;
			}
			return this.namespaceManager.LookupNamespace(prefix);
		}

		// Token: 0x06000B3A RID: 2874 RVA: 0x00033724 File Offset: 0x00031924
		public override bool ReadAttributeValue()
		{
			if (this.parsingFunction != XmlTextReaderImpl.ParsingFunction.InReadAttributeValue)
			{
				if (this.curNode.type != XmlNodeType.Attribute)
				{
					return false;
				}
				if (this.readState != ReadState.Interactive || this.curAttrIndex < 0)
				{
					return false;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadValueChunk)
				{
					this.FinishReadValueChunk();
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
				{
					this.FinishReadContentAsBinary();
				}
				if (this.curNode.nextAttrValueChunk == null || this.entityHandling == EntityHandling.ExpandEntities)
				{
					XmlTextReaderImpl.NodeData nodeData = this.AddNode(this.index + this.attrCount + 1, this.curNode.depth + 1);
					nodeData.SetValueNode(XmlNodeType.Text, this.curNode.StringValue);
					nodeData.lineInfo = this.curNode.lineInfo2;
					nodeData.depth = this.curNode.depth + 1;
					this.curNode = nodeData;
					nodeData.nextAttrValueChunk = null;
				}
				else
				{
					this.curNode = this.curNode.nextAttrValueChunk;
					this.AddNode(this.index + this.attrCount + 1, this.index + 2);
					this.nodes[this.index + this.attrCount + 1] = this.curNode;
					this.fullAttrCleanup = true;
				}
				this.nextParsingFunction = this.parsingFunction;
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.InReadAttributeValue;
				this.attributeValueBaseEntityId = this.ps.entityId;
				return true;
			}
			else
			{
				if (this.ps.entityId != this.attributeValueBaseEntityId)
				{
					return this.ParseAttributeValueChunk();
				}
				if (this.curNode.nextAttrValueChunk != null)
				{
					this.curNode = this.curNode.nextAttrValueChunk;
					this.nodes[this.index + this.attrCount + 1] = this.curNode;
					return true;
				}
				return false;
			}
		}

		// Token: 0x06000B3B RID: 2875 RVA: 0x000338D4 File Offset: 0x00031AD4
		public override void ResolveEntity()
		{
			if (this.curNode.type != XmlNodeType.EntityReference)
			{
				throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadAttributeValue || this.parsingFunction == XmlTextReaderImpl.ParsingFunction.FragmentAttribute)
			{
				switch (this.HandleGeneralEntityReference(this.curNode.localName, true, true, this.curNode.LinePos))
				{
				case XmlTextReaderImpl.EntityType.Expanded:
				case XmlTextReaderImpl.EntityType.ExpandedInAttribute:
					if (this.ps.charsUsed - this.ps.charPos == 0)
					{
						this.emptyEntityInAttributeResolved = true;
						goto IL_164;
					}
					goto IL_164;
				case XmlTextReaderImpl.EntityType.FakeExpanded:
					this.emptyEntityInAttributeResolved = true;
					goto IL_164;
				}
				throw new XmlException("An internal error has occurred.", string.Empty);
			}
			switch (this.HandleGeneralEntityReference(this.curNode.localName, false, true, this.curNode.LinePos))
			{
			case XmlTextReaderImpl.EntityType.Expanded:
			case XmlTextReaderImpl.EntityType.ExpandedInAttribute:
				this.nextParsingFunction = this.parsingFunction;
				if (this.ps.charsUsed - this.ps.charPos == 0 && !this.ps.entity.IsExternal)
				{
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.AfterResolveEmptyEntityInContent;
					goto IL_164;
				}
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.AfterResolveEntityInContent;
				goto IL_164;
			case XmlTextReaderImpl.EntityType.FakeExpanded:
				this.nextParsingFunction = this.parsingFunction;
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.AfterResolveEmptyEntityInContent;
				goto IL_164;
			}
			throw new XmlException("An internal error has occurred.", string.Empty);
			IL_164:
			this.ps.entityResolvedManually = true;
			this.index++;
		}

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000B3C RID: 2876 RVA: 0x00033A5F File Offset: 0x00031C5F
		// (set) Token: 0x06000B3D RID: 2877 RVA: 0x00033A67 File Offset: 0x00031C67
		internal XmlReader OuterReader
		{
			get
			{
				return this.outerReader;
			}
			set
			{
				this.outerReader = value;
			}
		}

		// Token: 0x06000B3E RID: 2878 RVA: 0x00033A70 File Offset: 0x00031C70
		internal void MoveOffEntityReference()
		{
			if (this.outerReader.NodeType == XmlNodeType.EntityReference && this.parsingFunction == XmlTextReaderImpl.ParsingFunction.AfterResolveEntityInContent && !this.outerReader.Read())
			{
				throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			}
		}

		// Token: 0x06000B3F RID: 2879 RVA: 0x00033AA7 File Offset: 0x00031CA7
		public override string ReadString()
		{
			this.MoveOffEntityReference();
			return base.ReadString();
		}

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000B40 RID: 2880 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool CanReadBinaryContent
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000B41 RID: 2881 RVA: 0x00033AB8 File Offset: 0x00031CB8
		public override int ReadContentAsBase64(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
			{
				if (this.incReadDecoder == this.base64Decoder)
				{
					return this.ReadContentAsBinary(buffer, index, count);
				}
			}
			else
			{
				if (this.readState != ReadState.Interactive)
				{
					return 0;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
				{
					throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
				}
				if (!XmlReader.CanReadContentAs(this.curNode.type))
				{
					throw base.CreateReadContentAsException("ReadContentAsBase64");
				}
				if (!this.InitReadContentAsBinary())
				{
					return 0;
				}
			}
			this.InitBase64Decoder();
			return this.ReadContentAsBinary(buffer, index, count);
		}

		// Token: 0x06000B42 RID: 2882 RVA: 0x00033B84 File Offset: 0x00031D84
		public override int ReadContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
			{
				if (this.incReadDecoder == this.binHexDecoder)
				{
					return this.ReadContentAsBinary(buffer, index, count);
				}
			}
			else
			{
				if (this.readState != ReadState.Interactive)
				{
					return 0;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
				{
					throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
				}
				if (!XmlReader.CanReadContentAs(this.curNode.type))
				{
					throw base.CreateReadContentAsException("ReadContentAsBinHex");
				}
				if (!this.InitReadContentAsBinary())
				{
					return 0;
				}
			}
			this.InitBinHexDecoder();
			return this.ReadContentAsBinary(buffer, index, count);
		}

		// Token: 0x06000B43 RID: 2883 RVA: 0x00033C50 File Offset: 0x00031E50
		public override int ReadElementContentAsBase64(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
			{
				if (this.incReadDecoder == this.base64Decoder)
				{
					return this.ReadElementContentAsBinary(buffer, index, count);
				}
			}
			else
			{
				if (this.readState != ReadState.Interactive)
				{
					return 0;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
				{
					throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
				}
				if (this.curNode.type != XmlNodeType.Element)
				{
					throw base.CreateReadElementContentAsException("ReadElementContentAsBinHex");
				}
				if (!this.InitReadElementContentAsBinary())
				{
					return 0;
				}
			}
			this.InitBase64Decoder();
			return this.ReadElementContentAsBinary(buffer, index, count);
		}

		// Token: 0x06000B44 RID: 2884 RVA: 0x00033D18 File Offset: 0x00031F18
		public override int ReadElementContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
			{
				if (this.incReadDecoder == this.binHexDecoder)
				{
					return this.ReadElementContentAsBinary(buffer, index, count);
				}
			}
			else
			{
				if (this.readState != ReadState.Interactive)
				{
					return 0;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
				{
					throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
				}
				if (this.curNode.type != XmlNodeType.Element)
				{
					throw base.CreateReadElementContentAsException("ReadElementContentAsBinHex");
				}
				if (!this.InitReadElementContentAsBinary())
				{
					return 0;
				}
			}
			this.InitBinHexDecoder();
			return this.ReadElementContentAsBinary(buffer, index, count);
		}

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x06000B45 RID: 2885 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool CanReadValueChunk
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000B46 RID: 2886 RVA: 0x00033DE0 File Offset: 0x00031FE0
		public override int ReadValueChunk(char[] buffer, int index, int count)
		{
			if (!XmlReader.HasValueInternal(this.curNode.type))
			{
				throw new InvalidOperationException(Res.GetString("The ReadValueAsChunk method is not supported on node type {0}.", new object[]
				{
					this.curNode.type
				}));
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction != XmlTextReaderImpl.ParsingFunction.InReadValueChunk)
			{
				if (this.readState != ReadState.Interactive)
				{
					return 0;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.PartialTextValue)
				{
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue;
				}
				else
				{
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue;
					this.nextNextParsingFunction = this.nextParsingFunction;
					this.nextParsingFunction = this.parsingFunction;
				}
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.InReadValueChunk;
				this.readValueOffset = 0;
			}
			if (count == 0)
			{
				return 0;
			}
			int num = 0;
			int num2 = this.curNode.CopyTo(this.readValueOffset, buffer, index + num, count - num);
			num += num2;
			this.readValueOffset += num2;
			if (num == count)
			{
				if (XmlCharType.IsHighSurrogate((int)buffer[index + count - 1]))
				{
					num--;
					this.readValueOffset--;
					if (num == 0)
					{
						this.Throw("The buffer is not large enough to fit a surrogate pair. Please provide a buffer of size at least 2 characters.");
					}
				}
				return num;
			}
			if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue)
			{
				this.curNode.SetValue(string.Empty);
				bool flag = false;
				int num3 = 0;
				int num4 = 0;
				while (num < count && !flag)
				{
					int num5 = 0;
					flag = this.ParseText(out num3, out num4, ref num5);
					int num6 = count - num;
					if (num6 > num4 - num3)
					{
						num6 = num4 - num3;
					}
					XmlTextReaderImpl.BlockCopyChars(this.ps.chars, num3, buffer, index + num, num6);
					num += num6;
					num3 += num6;
				}
				this.incReadState = (flag ? XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue : XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue);
				if (num == count && XmlCharType.IsHighSurrogate((int)buffer[index + count - 1]))
				{
					num--;
					num3--;
					if (num == 0)
					{
						this.Throw("The buffer is not large enough to fit a surrogate pair. Please provide a buffer of size at least 2 characters.");
					}
				}
				this.readValueOffset = 0;
				this.curNode.SetValue(this.ps.chars, num3, num4 - num3);
			}
			return num;
		}

		// Token: 0x06000B47 RID: 2887 RVA: 0x000033DE File Offset: 0x000015DE
		public bool HasLineInfo()
		{
			return true;
		}

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x06000B48 RID: 2888 RVA: 0x00033FF0 File Offset: 0x000321F0
		public int LineNumber
		{
			get
			{
				return this.curNode.LineNo;
			}
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000B49 RID: 2889 RVA: 0x00033FFD File Offset: 0x000321FD
		public int LinePosition
		{
			get
			{
				return this.curNode.LinePos;
			}
		}

		// Token: 0x06000B4A RID: 2890 RVA: 0x0003400A File Offset: 0x0003220A
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return this.GetNamespacesInScope(scope);
		}

		// Token: 0x06000B4B RID: 2891 RVA: 0x00034013 File Offset: 0x00032213
		string IXmlNamespaceResolver.LookupNamespace(string prefix)
		{
			return this.LookupNamespace(prefix);
		}

		// Token: 0x06000B4C RID: 2892 RVA: 0x0003401C File Offset: 0x0003221C
		string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
		{
			return this.LookupPrefix(namespaceName);
		}

		// Token: 0x06000B4D RID: 2893 RVA: 0x00034025 File Offset: 0x00032225
		internal IDictionary<string, string> GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return this.namespaceManager.GetNamespacesInScope(scope);
		}

		// Token: 0x06000B4E RID: 2894 RVA: 0x00034033 File Offset: 0x00032233
		internal string LookupPrefix(string namespaceName)
		{
			return this.namespaceManager.LookupPrefix(namespaceName);
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000B4F RID: 2895 RVA: 0x00034041 File Offset: 0x00032241
		// (set) Token: 0x06000B50 RID: 2896 RVA: 0x0003404C File Offset: 0x0003224C
		internal bool Namespaces
		{
			get
			{
				return this.supportNamespaces;
			}
			set
			{
				if (this.readState != ReadState.Initial)
				{
					throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
				}
				this.supportNamespaces = value;
				if (value)
				{
					if (this.namespaceManager is XmlTextReaderImpl.NoNamespaceManager)
					{
						if (this.fragment && this.fragmentParserContext != null && this.fragmentParserContext.NamespaceManager != null)
						{
							this.namespaceManager = this.fragmentParserContext.NamespaceManager;
						}
						else
						{
							this.namespaceManager = new XmlNamespaceManager(this.nameTable);
						}
					}
					this.xmlContext.defaultNamespace = this.namespaceManager.LookupNamespace(string.Empty);
					return;
				}
				if (!(this.namespaceManager is XmlTextReaderImpl.NoNamespaceManager))
				{
					this.namespaceManager = new XmlTextReaderImpl.NoNamespaceManager();
				}
				this.xmlContext.defaultNamespace = string.Empty;
			}
		}

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000B51 RID: 2897 RVA: 0x0003410D File Offset: 0x0003230D
		// (set) Token: 0x06000B52 RID: 2898 RVA: 0x00034118 File Offset: 0x00032318
		internal bool Normalization
		{
			get
			{
				return this.normalize;
			}
			set
			{
				if (this.readState == ReadState.Closed)
				{
					throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
				}
				this.normalize = value;
				if (this.ps.entity == null || this.ps.entity.IsExternal)
				{
					this.ps.eolNormalized = !value;
				}
			}
		}

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000B53 RID: 2899 RVA: 0x00034173 File Offset: 0x00032373
		internal Encoding Encoding
		{
			get
			{
				if (this.readState != ReadState.Interactive)
				{
					return null;
				}
				return this.reportedEncoding;
			}
		}

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000B54 RID: 2900 RVA: 0x00034186 File Offset: 0x00032386
		// (set) Token: 0x06000B55 RID: 2901 RVA: 0x0003418E File Offset: 0x0003238E
		internal WhitespaceHandling WhitespaceHandling
		{
			get
			{
				return this.whitespaceHandling;
			}
			set
			{
				if (this.readState == ReadState.Closed)
				{
					throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
				}
				if (value > WhitespaceHandling.None)
				{
					throw new XmlException("Expected WhitespaceHandling.None, or WhitespaceHandling.All, or WhitespaceHandling.Significant.", string.Empty);
				}
				this.whitespaceHandling = value;
			}
		}

		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000B56 RID: 2902 RVA: 0x000341C4 File Offset: 0x000323C4
		// (set) Token: 0x06000B57 RID: 2903 RVA: 0x000341CC File Offset: 0x000323CC
		internal DtdProcessing DtdProcessing
		{
			get
			{
				return this.dtdProcessing;
			}
			set
			{
				if (value > DtdProcessing.Parse)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.dtdProcessing = value;
			}
		}

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000B58 RID: 2904 RVA: 0x000341E4 File Offset: 0x000323E4
		// (set) Token: 0x06000B59 RID: 2905 RVA: 0x000341EC File Offset: 0x000323EC
		internal EntityHandling EntityHandling
		{
			get
			{
				return this.entityHandling;
			}
			set
			{
				if (value != EntityHandling.ExpandEntities && value != EntityHandling.ExpandCharEntities)
				{
					throw new XmlException("Expected EntityHandling.ExpandEntities or EntityHandling.ExpandCharEntities.", string.Empty);
				}
				this.entityHandling = value;
			}
		}

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06000B5A RID: 2906 RVA: 0x0003420D File Offset: 0x0003240D
		internal bool IsResolverSet
		{
			get
			{
				return this.xmlResolverIsSet;
			}
		}

		// Token: 0x17000219 RID: 537
		// (set) Token: 0x06000B5B RID: 2907 RVA: 0x00034218 File Offset: 0x00032418
		internal XmlResolver XmlResolver
		{
			set
			{
				this.xmlResolver = value;
				this.xmlResolverIsSet = true;
				this.ps.baseUri = null;
				for (int i = 0; i <= this.parsingStatesStackTop; i++)
				{
					this.parsingStatesStack[i].baseUri = null;
				}
			}
		}

		// Token: 0x06000B5C RID: 2908 RVA: 0x00034264 File Offset: 0x00032464
		internal void ResetState()
		{
			if (this.fragment)
			{
				this.Throw(new InvalidOperationException(Res.GetString("Cannot call ResetState when parsing an XML fragment.")));
			}
			if (this.readState == ReadState.Initial)
			{
				return;
			}
			this.ResetAttributes();
			while (this.namespaceManager.PopScope())
			{
			}
			while (this.InEntity)
			{
				this.HandleEntityEnd(true);
			}
			this.readState = ReadState.Initial;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.SwitchToInteractiveXmlDecl;
			this.nextParsingFunction = XmlTextReaderImpl.ParsingFunction.DocumentContent;
			this.curNode = this.nodes[0];
			this.curNode.Clear(XmlNodeType.None);
			this.curNode.SetLineInfo(0, 0);
			this.index = 0;
			this.rootElementParsed = false;
			this.charactersInDocument = 0L;
			this.charactersFromEntities = 0L;
			this.afterResetState = true;
		}

		// Token: 0x06000B5D RID: 2909 RVA: 0x00034320 File Offset: 0x00032520
		internal TextReader GetRemainder()
		{
			XmlTextReaderImpl.ParsingFunction parsingFunction = this.parsingFunction;
			if (parsingFunction != XmlTextReaderImpl.ParsingFunction.OpenUrl)
			{
				if (parsingFunction - XmlTextReaderImpl.ParsingFunction.Eof <= 1)
				{
					return new StringReader(string.Empty);
				}
				if (parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
				{
					if (!this.InEntity)
					{
						this.stringBuilder.Append(this.ps.chars, this.incReadLeftStartPos, this.incReadLeftEndPos - this.incReadLeftStartPos);
					}
				}
			}
			else
			{
				this.OpenUrl();
			}
			while (this.InEntity)
			{
				this.HandleEntityEnd(true);
			}
			this.ps.appendMode = false;
			do
			{
				this.stringBuilder.Append(this.ps.chars, this.ps.charPos, this.ps.charsUsed - this.ps.charPos);
				this.ps.charPos = this.ps.charsUsed;
			}
			while (this.ReadData() != 0);
			this.OnEof();
			string s = this.stringBuilder.ToString();
			this.stringBuilder.Length = 0;
			return new StringReader(s);
		}

		// Token: 0x06000B5E RID: 2910 RVA: 0x00034424 File Offset: 0x00032624
		internal int ReadChars(char[] buffer, int index, int count)
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
			{
				if (this.incReadDecoder != this.readCharsDecoder)
				{
					if (this.readCharsDecoder == null)
					{
						this.readCharsDecoder = new IncrementalReadCharsDecoder();
					}
					this.readCharsDecoder.Reset();
					this.incReadDecoder = this.readCharsDecoder;
				}
				return this.IncrementalRead(buffer, index, count);
			}
			if (this.curNode.type != XmlNodeType.Element)
			{
				return 0;
			}
			if (this.curNode.IsEmptyElement)
			{
				this.outerReader.Read();
				return 0;
			}
			if (this.readCharsDecoder == null)
			{
				this.readCharsDecoder = new IncrementalReadCharsDecoder();
			}
			this.InitIncrementalRead(this.readCharsDecoder);
			return this.IncrementalRead(buffer, index, count);
		}

		// Token: 0x06000B5F RID: 2911 RVA: 0x000344D0 File Offset: 0x000326D0
		internal int ReadBase64(byte[] array, int offset, int len)
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
			{
				if (this.incReadDecoder != this.base64Decoder)
				{
					this.InitBase64Decoder();
				}
				return this.IncrementalRead(array, offset, len);
			}
			if (this.curNode.type != XmlNodeType.Element)
			{
				return 0;
			}
			if (this.curNode.IsEmptyElement)
			{
				this.outerReader.Read();
				return 0;
			}
			if (this.base64Decoder == null)
			{
				this.base64Decoder = new Base64Decoder();
			}
			this.InitIncrementalRead(this.base64Decoder);
			return this.IncrementalRead(array, offset, len);
		}

		// Token: 0x06000B60 RID: 2912 RVA: 0x00034558 File Offset: 0x00032758
		internal int ReadBinHex(byte[] array, int offset, int len)
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
			{
				if (this.incReadDecoder != this.binHexDecoder)
				{
					this.InitBinHexDecoder();
				}
				return this.IncrementalRead(array, offset, len);
			}
			if (this.curNode.type != XmlNodeType.Element)
			{
				return 0;
			}
			if (this.curNode.IsEmptyElement)
			{
				this.outerReader.Read();
				return 0;
			}
			if (this.binHexDecoder == null)
			{
				this.binHexDecoder = new BinHexDecoder();
			}
			this.InitIncrementalRead(this.binHexDecoder);
			return this.IncrementalRead(array, offset, len);
		}

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06000B61 RID: 2913 RVA: 0x00032F52 File Offset: 0x00031152
		internal XmlNameTable DtdParserProxy_NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000B62 RID: 2914 RVA: 0x000345E0 File Offset: 0x000327E0
		internal IXmlNamespaceResolver DtdParserProxy_NamespaceResolver
		{
			get
			{
				return this.namespaceManager;
			}
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x06000B63 RID: 2915 RVA: 0x000345E8 File Offset: 0x000327E8
		internal bool DtdParserProxy_DtdValidation
		{
			get
			{
				return this.DtdValidation;
			}
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x06000B64 RID: 2916 RVA: 0x0003410D File Offset: 0x0003230D
		internal bool DtdParserProxy_Normalization
		{
			get
			{
				return this.normalize;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x06000B65 RID: 2917 RVA: 0x00034041 File Offset: 0x00032241
		internal bool DtdParserProxy_Namespaces
		{
			get
			{
				return this.supportNamespaces;
			}
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x06000B66 RID: 2918 RVA: 0x000345F0 File Offset: 0x000327F0
		internal bool DtdParserProxy_V1CompatibilityMode
		{
			get
			{
				return this.v1Compat;
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x06000B67 RID: 2919 RVA: 0x000345F8 File Offset: 0x000327F8
		internal Uri DtdParserProxy_BaseUri
		{
			get
			{
				if (this.ps.baseUriStr.Length > 0 && this.ps.baseUri == null && this.xmlResolver != null)
				{
					this.ps.baseUri = this.xmlResolver.ResolveUri(null, this.ps.baseUriStr);
				}
				return this.ps.baseUri;
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000B68 RID: 2920 RVA: 0x00034660 File Offset: 0x00032860
		internal bool DtdParserProxy_IsEof
		{
			get
			{
				return this.ps.isEof;
			}
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06000B69 RID: 2921 RVA: 0x0003466D File Offset: 0x0003286D
		internal char[] DtdParserProxy_ParsingBuffer
		{
			get
			{
				return this.ps.chars;
			}
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06000B6A RID: 2922 RVA: 0x0003467A File Offset: 0x0003287A
		internal int DtdParserProxy_ParsingBufferLength
		{
			get
			{
				return this.ps.charsUsed;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06000B6B RID: 2923 RVA: 0x00034687 File Offset: 0x00032887
		// (set) Token: 0x06000B6C RID: 2924 RVA: 0x00034694 File Offset: 0x00032894
		internal int DtdParserProxy_CurrentPosition
		{
			get
			{
				return this.ps.charPos;
			}
			set
			{
				this.ps.charPos = value;
			}
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06000B6D RID: 2925 RVA: 0x000346A2 File Offset: 0x000328A2
		internal int DtdParserProxy_EntityStackLength
		{
			get
			{
				return this.parsingStatesStackTop + 1;
			}
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06000B6E RID: 2926 RVA: 0x000346AC File Offset: 0x000328AC
		internal bool DtdParserProxy_IsEntityEolNormalized
		{
			get
			{
				return this.ps.eolNormalized;
			}
		}

		// Token: 0x17000227 RID: 551
		// (get) Token: 0x06000B6F RID: 2927 RVA: 0x000346B9 File Offset: 0x000328B9
		// (set) Token: 0x06000B70 RID: 2928 RVA: 0x000346C1 File Offset: 0x000328C1
		internal IValidationEventHandling DtdParserProxy_ValidationEventHandling
		{
			get
			{
				return this.validationEventHandling;
			}
			set
			{
				this.validationEventHandling = value;
			}
		}

		// Token: 0x06000B71 RID: 2929 RVA: 0x000346CA File Offset: 0x000328CA
		internal void DtdParserProxy_OnNewLine(int pos)
		{
			this.OnNewLine(pos);
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x06000B72 RID: 2930 RVA: 0x000346D3 File Offset: 0x000328D3
		internal int DtdParserProxy_LineNo
		{
			get
			{
				return this.ps.LineNo;
			}
		}

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x06000B73 RID: 2931 RVA: 0x000346E0 File Offset: 0x000328E0
		internal int DtdParserProxy_LineStartPosition
		{
			get
			{
				return this.ps.lineStartPos;
			}
		}

		// Token: 0x06000B74 RID: 2932 RVA: 0x000346ED File Offset: 0x000328ED
		internal int DtdParserProxy_ReadData()
		{
			return this.ReadData();
		}

		// Token: 0x06000B75 RID: 2933 RVA: 0x000346F8 File Offset: 0x000328F8
		internal int DtdParserProxy_ParseNumericCharRef(StringBuilder internalSubsetBuilder)
		{
			XmlTextReaderImpl.EntityType entityType;
			return this.ParseNumericCharRef(true, internalSubsetBuilder, out entityType);
		}

		// Token: 0x06000B76 RID: 2934 RVA: 0x0003470F File Offset: 0x0003290F
		internal int DtdParserProxy_ParseNamedCharRef(bool expand, StringBuilder internalSubsetBuilder)
		{
			return this.ParseNamedCharRef(expand, internalSubsetBuilder);
		}

		// Token: 0x06000B77 RID: 2935 RVA: 0x0003471C File Offset: 0x0003291C
		internal void DtdParserProxy_ParsePI(StringBuilder sb)
		{
			if (sb == null)
			{
				XmlTextReaderImpl.ParsingMode parsingMode = this.parsingMode;
				this.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
				this.ParsePI(null);
				this.parsingMode = parsingMode;
				return;
			}
			this.ParsePI(sb);
		}

		// Token: 0x06000B78 RID: 2936 RVA: 0x00034754 File Offset: 0x00032954
		internal void DtdParserProxy_ParseComment(StringBuilder sb)
		{
			try
			{
				if (sb == null)
				{
					XmlTextReaderImpl.ParsingMode parsingMode = this.parsingMode;
					this.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
					this.ParseCDataOrComment(XmlNodeType.Comment);
					this.parsingMode = parsingMode;
				}
				else
				{
					XmlTextReaderImpl.NodeData nodeData = this.curNode;
					this.curNode = this.AddNode(this.index + this.attrCount + 1, this.index);
					this.ParseCDataOrComment(XmlNodeType.Comment);
					this.curNode.CopyTo(0, sb);
					this.curNode = nodeData;
				}
			}
			catch (XmlException ex)
			{
				if (!(ex.ResString == "Unexpected end of file while parsing {0} has occurred.") || this.ps.entity == null)
				{
					throw;
				}
				this.SendValidationEvent(XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", null, this.ps.LineNo, this.ps.LinePos);
			}
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x06000B79 RID: 2937 RVA: 0x00034820 File Offset: 0x00032A20
		private bool IsResolverNull
		{
			get
			{
				return this.xmlResolver == null || (XmlReaderSection.ProhibitDefaultUrlResolver && !this.xmlResolverIsSet);
			}
		}

		// Token: 0x06000B7A RID: 2938 RVA: 0x0003483E File Offset: 0x00032A3E
		private XmlResolver GetTempResolver()
		{
			if (this.xmlResolver != null)
			{
				return this.xmlResolver;
			}
			return new XmlUrlResolver();
		}

		// Token: 0x06000B7B RID: 2939 RVA: 0x00034854 File Offset: 0x00032A54
		internal bool DtdParserProxy_PushEntity(IDtdEntityInfo entity, out int entityId)
		{
			bool result;
			if (entity.IsExternal)
			{
				if (this.IsResolverNull)
				{
					entityId = -1;
					return false;
				}
				result = this.PushExternalEntity(entity);
			}
			else
			{
				this.PushInternalEntity(entity);
				result = true;
			}
			entityId = this.ps.entityId;
			return result;
		}

		// Token: 0x06000B7C RID: 2940 RVA: 0x00034897 File Offset: 0x00032A97
		internal bool DtdParserProxy_PopEntity(out IDtdEntityInfo oldEntity, out int newEntityId)
		{
			if (this.parsingStatesStackTop == -1)
			{
				oldEntity = null;
				newEntityId = -1;
				return false;
			}
			oldEntity = this.ps.entity;
			this.PopEntity();
			newEntityId = this.ps.entityId;
			return true;
		}

		// Token: 0x06000B7D RID: 2941 RVA: 0x000348CC File Offset: 0x00032ACC
		internal bool DtdParserProxy_PushExternalSubset(string systemId, string publicId)
		{
			if (this.IsResolverNull)
			{
				return false;
			}
			if (this.ps.baseUri == null && !string.IsNullOrEmpty(this.ps.baseUriStr))
			{
				this.ps.baseUri = this.xmlResolver.ResolveUri(null, this.ps.baseUriStr);
			}
			this.PushExternalEntityOrSubset(publicId, systemId, this.ps.baseUri, null);
			this.ps.entity = null;
			this.ps.entityId = 0;
			int charPos = this.ps.charPos;
			if (this.v1Compat)
			{
				this.EatWhitespaces(null);
			}
			if (!this.ParseXmlDeclaration(true))
			{
				this.ps.charPos = charPos;
			}
			return true;
		}

		// Token: 0x06000B7E RID: 2942 RVA: 0x00034988 File Offset: 0x00032B88
		internal void DtdParserProxy_PushInternalDtd(string baseUri, string internalDtd)
		{
			this.PushParsingState();
			this.RegisterConsumedCharacters((long)internalDtd.Length, false);
			this.InitStringInput(baseUri, Encoding.Unicode, internalDtd);
			this.ps.entity = null;
			this.ps.entityId = 0;
			this.ps.eolNormalized = false;
		}

		// Token: 0x06000B7F RID: 2943 RVA: 0x000349DA File Offset: 0x00032BDA
		internal void DtdParserProxy_Throw(Exception e)
		{
			this.Throw(e);
		}

		// Token: 0x06000B80 RID: 2944 RVA: 0x000349E3 File Offset: 0x00032BE3
		internal void DtdParserProxy_OnSystemId(string systemId, LineInfo keywordLineInfo, LineInfo systemLiteralLineInfo)
		{
			XmlTextReaderImpl.NodeData nodeData = this.AddAttributeNoChecks("SYSTEM", this.index + 1);
			nodeData.SetValue(systemId);
			nodeData.lineInfo = keywordLineInfo;
			nodeData.lineInfo2 = systemLiteralLineInfo;
		}

		// Token: 0x06000B81 RID: 2945 RVA: 0x00034A0C File Offset: 0x00032C0C
		internal void DtdParserProxy_OnPublicId(string publicId, LineInfo keywordLineInfo, LineInfo publicLiteralLineInfo)
		{
			XmlTextReaderImpl.NodeData nodeData = this.AddAttributeNoChecks("PUBLIC", this.index + 1);
			nodeData.SetValue(publicId);
			nodeData.lineInfo = keywordLineInfo;
			nodeData.lineInfo2 = publicLiteralLineInfo;
		}

		// Token: 0x06000B82 RID: 2946 RVA: 0x00034A35 File Offset: 0x00032C35
		private void Throw(int pos, string res, string arg)
		{
			this.ps.charPos = pos;
			this.Throw(res, arg);
		}

		// Token: 0x06000B83 RID: 2947 RVA: 0x00034A4B File Offset: 0x00032C4B
		private void Throw(int pos, string res, string[] args)
		{
			this.ps.charPos = pos;
			this.Throw(res, args);
		}

		// Token: 0x06000B84 RID: 2948 RVA: 0x00034A61 File Offset: 0x00032C61
		private void Throw(int pos, string res)
		{
			this.ps.charPos = pos;
			this.Throw(res, string.Empty);
		}

		// Token: 0x06000B85 RID: 2949 RVA: 0x00034A7B File Offset: 0x00032C7B
		private void Throw(string res)
		{
			this.Throw(res, string.Empty);
		}

		// Token: 0x06000B86 RID: 2950 RVA: 0x00034A89 File Offset: 0x00032C89
		private void Throw(string res, int lineNo, int linePos)
		{
			this.Throw(new XmlException(res, string.Empty, lineNo, linePos, this.ps.baseUriStr));
		}

		// Token: 0x06000B87 RID: 2951 RVA: 0x00034AA9 File Offset: 0x00032CA9
		private void Throw(string res, string arg)
		{
			this.Throw(new XmlException(res, arg, this.ps.LineNo, this.ps.LinePos, this.ps.baseUriStr));
		}

		// Token: 0x06000B88 RID: 2952 RVA: 0x00034AD9 File Offset: 0x00032CD9
		private void Throw(string res, string arg, int lineNo, int linePos)
		{
			this.Throw(new XmlException(res, arg, lineNo, linePos, this.ps.baseUriStr));
		}

		// Token: 0x06000B89 RID: 2953 RVA: 0x00034AF6 File Offset: 0x00032CF6
		private void Throw(string res, string[] args)
		{
			this.Throw(new XmlException(res, args, this.ps.LineNo, this.ps.LinePos, this.ps.baseUriStr));
		}

		// Token: 0x06000B8A RID: 2954 RVA: 0x00034B26 File Offset: 0x00032D26
		private void Throw(string res, string arg, Exception innerException)
		{
			this.Throw(res, new string[]
			{
				arg
			}, innerException);
		}

		// Token: 0x06000B8B RID: 2955 RVA: 0x00034B3A File Offset: 0x00032D3A
		private void Throw(string res, string[] args, Exception innerException)
		{
			this.Throw(new XmlException(res, args, innerException, this.ps.LineNo, this.ps.LinePos, this.ps.baseUriStr));
		}

		// Token: 0x06000B8C RID: 2956 RVA: 0x00034B6C File Offset: 0x00032D6C
		private void Throw(Exception e)
		{
			this.SetErrorState();
			XmlException ex = e as XmlException;
			if (ex != null)
			{
				this.curNode.SetLineInfo(ex.LineNumber, ex.LinePosition);
			}
			throw e;
		}

		// Token: 0x06000B8D RID: 2957 RVA: 0x00034BA1 File Offset: 0x00032DA1
		private void ReThrow(Exception e, int lineNo, int linePos)
		{
			this.Throw(new XmlException(e.Message, null, lineNo, linePos, this.ps.baseUriStr));
		}

		// Token: 0x06000B8E RID: 2958 RVA: 0x00034BC2 File Offset: 0x00032DC2
		private void ThrowWithoutLineInfo(string res)
		{
			this.Throw(new XmlException(res, string.Empty, this.ps.baseUriStr));
		}

		// Token: 0x06000B8F RID: 2959 RVA: 0x00034BE0 File Offset: 0x00032DE0
		private void ThrowWithoutLineInfo(string res, string arg)
		{
			this.Throw(new XmlException(res, arg, this.ps.baseUriStr));
		}

		// Token: 0x06000B90 RID: 2960 RVA: 0x00034BFA File Offset: 0x00032DFA
		private void ThrowWithoutLineInfo(string res, string[] args, Exception innerException)
		{
			this.Throw(new XmlException(res, args, innerException, 0, 0, this.ps.baseUriStr));
		}

		// Token: 0x06000B91 RID: 2961 RVA: 0x00034C17 File Offset: 0x00032E17
		private void ThrowInvalidChar(char[] data, int length, int invCharPos)
		{
			this.Throw(invCharPos, "'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(data, length, invCharPos));
		}

		// Token: 0x06000B92 RID: 2962 RVA: 0x00034C2D File Offset: 0x00032E2D
		private void SetErrorState()
		{
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.Error;
			this.readState = ReadState.Error;
		}

		// Token: 0x06000B93 RID: 2963 RVA: 0x00034C3E File Offset: 0x00032E3E
		private void SendValidationEvent(XmlSeverityType severity, string code, string arg, int lineNo, int linePos)
		{
			this.SendValidationEvent(severity, new XmlSchemaException(code, arg, this.ps.baseUriStr, lineNo, linePos));
		}

		// Token: 0x06000B94 RID: 2964 RVA: 0x00034C5D File Offset: 0x00032E5D
		private void SendValidationEvent(XmlSeverityType severity, XmlSchemaException exception)
		{
			if (this.validationEventHandling != null)
			{
				this.validationEventHandling.SendEvent(exception, severity);
			}
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x06000B95 RID: 2965 RVA: 0x00034C74 File Offset: 0x00032E74
		private bool InAttributeValueIterator
		{
			get
			{
				return this.attrCount > 0 && this.parsingFunction >= XmlTextReaderImpl.ParsingFunction.InReadAttributeValue;
			}
		}

		// Token: 0x06000B96 RID: 2966 RVA: 0x00034C90 File Offset: 0x00032E90
		private void FinishAttributeValueIterator()
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadValueChunk)
			{
				this.FinishReadValueChunk();
			}
			else if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
			{
				this.FinishReadContentAsBinary();
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadAttributeValue)
			{
				while (this.ps.entityId != this.attributeValueBaseEntityId)
				{
					this.HandleEntityEnd(false);
				}
				this.emptyEntityInAttributeResolved = false;
				this.parsingFunction = this.nextParsingFunction;
				this.nextParsingFunction = ((this.index > 0) ? XmlTextReaderImpl.ParsingFunction.ElementContent : XmlTextReaderImpl.ParsingFunction.DocumentContent);
			}
		}

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06000B97 RID: 2967 RVA: 0x00034D0C File Offset: 0x00032F0C
		private bool DtdValidation
		{
			get
			{
				return this.validationEventHandling != null;
			}
		}

		// Token: 0x06000B98 RID: 2968 RVA: 0x00034D17 File Offset: 0x00032F17
		private void InitStreamInput(Stream stream, Encoding encoding)
		{
			this.InitStreamInput(null, string.Empty, stream, null, 0, encoding);
		}

		// Token: 0x06000B99 RID: 2969 RVA: 0x00034D29 File Offset: 0x00032F29
		private void InitStreamInput(string baseUriStr, Stream stream, Encoding encoding)
		{
			this.InitStreamInput(null, baseUriStr, stream, null, 0, encoding);
		}

		// Token: 0x06000B9A RID: 2970 RVA: 0x00034D37 File Offset: 0x00032F37
		private void InitStreamInput(Uri baseUri, Stream stream, Encoding encoding)
		{
			this.InitStreamInput(baseUri, baseUri.ToString(), stream, null, 0, encoding);
		}

		// Token: 0x06000B9B RID: 2971 RVA: 0x00034D4A File Offset: 0x00032F4A
		private void InitStreamInput(Uri baseUri, string baseUriStr, Stream stream, Encoding encoding)
		{
			this.InitStreamInput(baseUri, baseUriStr, stream, null, 0, encoding);
		}

		// Token: 0x06000B9C RID: 2972 RVA: 0x00034D5C File Offset: 0x00032F5C
		private void InitStreamInput(Uri baseUri, string baseUriStr, Stream stream, byte[] bytes, int byteCount, Encoding encoding)
		{
			this.ps.stream = stream;
			this.ps.baseUri = baseUri;
			this.ps.baseUriStr = baseUriStr;
			int num;
			if (bytes != null)
			{
				this.ps.bytes = bytes;
				this.ps.bytesUsed = byteCount;
				num = this.ps.bytes.Length;
			}
			else
			{
				if (this.laterInitParam != null && this.laterInitParam.useAsync)
				{
					num = 65536;
				}
				else
				{
					num = XmlReader.CalcBufferSize(stream);
				}
				if (this.ps.bytes == null || this.ps.bytes.Length < num)
				{
					this.ps.bytes = new byte[num];
				}
			}
			if (this.ps.chars == null || this.ps.chars.Length < num + 1)
			{
				this.ps.chars = new char[num + 1];
			}
			this.ps.bytePos = 0;
			while (this.ps.bytesUsed < 4 && this.ps.bytes.Length - this.ps.bytesUsed > 0)
			{
				int num2 = stream.Read(this.ps.bytes, this.ps.bytesUsed, this.ps.bytes.Length - this.ps.bytesUsed);
				if (num2 == 0)
				{
					this.ps.isStreamEof = true;
					break;
				}
				this.ps.bytesUsed = this.ps.bytesUsed + num2;
			}
			if (encoding == null)
			{
				encoding = this.DetectEncoding();
			}
			this.SetupEncoding(encoding);
			byte[] preamble = this.ps.encoding.GetPreamble();
			int num3 = preamble.Length;
			int num4 = 0;
			while (num4 < num3 && num4 < this.ps.bytesUsed && this.ps.bytes[num4] == preamble[num4])
			{
				num4++;
			}
			if (num4 == num3)
			{
				this.ps.bytePos = num3;
			}
			this.documentStartBytePos = this.ps.bytePos;
			this.ps.eolNormalized = !this.normalize;
			this.ps.appendMode = true;
			this.ReadData();
		}

		// Token: 0x06000B9D RID: 2973 RVA: 0x00034F71 File Offset: 0x00033171
		private void InitTextReaderInput(string baseUriStr, TextReader input)
		{
			this.InitTextReaderInput(baseUriStr, null, input);
		}

		// Token: 0x06000B9E RID: 2974 RVA: 0x00034F7C File Offset: 0x0003317C
		private void InitTextReaderInput(string baseUriStr, Uri baseUri, TextReader input)
		{
			this.ps.textReader = input;
			this.ps.baseUriStr = baseUriStr;
			this.ps.baseUri = baseUri;
			if (this.ps.chars == null)
			{
				if (this.laterInitParam != null && this.laterInitParam.useAsync)
				{
					this.ps.chars = new char[65537];
				}
				else
				{
					this.ps.chars = new char[4097];
				}
			}
			this.ps.encoding = Encoding.Unicode;
			this.ps.eolNormalized = !this.normalize;
			this.ps.appendMode = true;
			this.ReadData();
		}

		// Token: 0x06000B9F RID: 2975 RVA: 0x00035034 File Offset: 0x00033234
		private void InitStringInput(string baseUriStr, Encoding originalEncoding, string str)
		{
			this.ps.baseUriStr = baseUriStr;
			this.ps.baseUri = null;
			int length = str.Length;
			this.ps.chars = new char[length + 1];
			str.CopyTo(0, this.ps.chars, 0, str.Length);
			this.ps.charsUsed = length;
			this.ps.chars[length] = '\0';
			this.ps.encoding = originalEncoding;
			this.ps.eolNormalized = !this.normalize;
			this.ps.isEof = true;
		}

		// Token: 0x06000BA0 RID: 2976 RVA: 0x000350D4 File Offset: 0x000332D4
		private void InitFragmentReader(XmlNodeType fragmentType, XmlParserContext parserContext, bool allowXmlDeclFragment)
		{
			this.fragmentParserContext = parserContext;
			if (parserContext != null)
			{
				if (parserContext.NamespaceManager != null)
				{
					this.namespaceManager = parserContext.NamespaceManager;
					this.xmlContext.defaultNamespace = this.namespaceManager.LookupNamespace(string.Empty);
				}
				else
				{
					this.namespaceManager = new XmlNamespaceManager(this.nameTable);
				}
				this.ps.baseUriStr = parserContext.BaseURI;
				this.ps.baseUri = null;
				this.xmlContext.xmlLang = parserContext.XmlLang;
				this.xmlContext.xmlSpace = parserContext.XmlSpace;
			}
			else
			{
				this.namespaceManager = new XmlNamespaceManager(this.nameTable);
				this.ps.baseUriStr = string.Empty;
				this.ps.baseUri = null;
			}
			this.reportedBaseUri = this.ps.baseUriStr;
			if (fragmentType <= XmlNodeType.Attribute)
			{
				if (fragmentType == XmlNodeType.Element)
				{
					this.nextParsingFunction = XmlTextReaderImpl.ParsingFunction.DocumentContent;
					goto IL_147;
				}
				if (fragmentType == XmlNodeType.Attribute)
				{
					this.ps.appendMode = false;
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.SwitchToInteractive;
					this.nextParsingFunction = XmlTextReaderImpl.ParsingFunction.FragmentAttribute;
					goto IL_147;
				}
			}
			else
			{
				if (fragmentType == XmlNodeType.Document)
				{
					goto IL_147;
				}
				if (fragmentType == XmlNodeType.XmlDeclaration)
				{
					if (allowXmlDeclFragment)
					{
						this.ps.appendMode = false;
						this.parsingFunction = XmlTextReaderImpl.ParsingFunction.SwitchToInteractive;
						this.nextParsingFunction = XmlTextReaderImpl.ParsingFunction.XmlDeclarationFragment;
						goto IL_147;
					}
				}
			}
			this.Throw("XmlNodeType {0} is not supported for partial content parsing.", fragmentType.ToString());
			return;
			IL_147:
			this.fragmentType = fragmentType;
			this.fragment = true;
		}

		// Token: 0x06000BA1 RID: 2977 RVA: 0x00035238 File Offset: 0x00033438
		private void ProcessDtdFromParserContext(XmlParserContext context)
		{
			switch (this.dtdProcessing)
			{
			case DtdProcessing.Prohibit:
				this.ThrowWithoutLineInfo("For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.");
				return;
			case DtdProcessing.Ignore:
				break;
			case DtdProcessing.Parse:
				this.ParseDtdFromParserContext();
				break;
			default:
				return;
			}
		}

		// Token: 0x06000BA2 RID: 2978 RVA: 0x00035274 File Offset: 0x00033474
		private void OpenUrl()
		{
			XmlResolver tempResolver = this.GetTempResolver();
			if (!(this.ps.baseUri != null))
			{
				this.ps.baseUri = tempResolver.ResolveUri(null, this.url);
				this.ps.baseUriStr = this.ps.baseUri.ToString();
			}
			try
			{
				this.OpenUrlDelegate(tempResolver);
			}
			catch
			{
				this.SetErrorState();
				throw;
			}
			if (this.ps.stream == null)
			{
				this.ThrowWithoutLineInfo("Cannot resolve '{0}'.", this.ps.baseUriStr);
			}
			this.InitStreamInput(this.ps.baseUri, this.ps.baseUriStr, this.ps.stream, null);
			this.reportedEncoding = this.ps.encoding;
		}

		// Token: 0x06000BA3 RID: 2979 RVA: 0x00035350 File Offset: 0x00033550
		private void OpenUrlDelegate(object xmlResolver)
		{
			this.ps.stream = (Stream)this.GetTempResolver().GetEntity(this.ps.baseUri, null, typeof(Stream));
		}

		// Token: 0x06000BA4 RID: 2980 RVA: 0x00035384 File Offset: 0x00033584
		private Encoding DetectEncoding()
		{
			if (this.ps.bytesUsed < 2)
			{
				return null;
			}
			int num = (int)this.ps.bytes[0] << 8 | (int)this.ps.bytes[1];
			int num2 = (this.ps.bytesUsed >= 4) ? ((int)this.ps.bytes[2] << 8 | (int)this.ps.bytes[3]) : 0;
			if (num <= 15360)
			{
				if (num != 0)
				{
					if (num != 60)
					{
						if (num == 15360)
						{
							if (num2 == 0)
							{
								return Ucs4Encoding.UCS4_Littleendian;
							}
							return Encoding.Unicode;
						}
					}
					else
					{
						if (num2 == 0)
						{
							return Ucs4Encoding.UCS4_3412;
						}
						return Encoding.BigEndianUnicode;
					}
				}
				else if (num2 <= 15360)
				{
					if (num2 == 60)
					{
						return Ucs4Encoding.UCS4_Bigendian;
					}
					if (num2 == 15360)
					{
						return Ucs4Encoding.UCS4_2143;
					}
				}
				else
				{
					if (num2 == 65279)
					{
						return Ucs4Encoding.UCS4_Bigendian;
					}
					if (num2 == 65534)
					{
						return Ucs4Encoding.UCS4_2143;
					}
				}
			}
			else if (num <= 61371)
			{
				if (num != 19567)
				{
					if (num == 61371)
					{
						if ((num2 & 65280) == 48896)
						{
							return new UTF8Encoding(true, true);
						}
					}
				}
				else if (num2 == 42900)
				{
					this.Throw("System does not support '{0}' encoding.", "ebcdic");
				}
			}
			else if (num != 65279)
			{
				if (num == 65534)
				{
					if (num2 == 0)
					{
						return Ucs4Encoding.UCS4_Littleendian;
					}
					return Encoding.Unicode;
				}
			}
			else
			{
				if (num2 == 0)
				{
					return Ucs4Encoding.UCS4_3412;
				}
				return Encoding.BigEndianUnicode;
			}
			return null;
		}

		// Token: 0x06000BA5 RID: 2981 RVA: 0x00035500 File Offset: 0x00033700
		private void SetupEncoding(Encoding encoding)
		{
			if (encoding == null)
			{
				this.ps.encoding = Encoding.UTF8;
				this.ps.decoder = new SafeAsciiDecoder();
				return;
			}
			this.ps.encoding = encoding;
			string webName = this.ps.encoding.WebName;
			if (webName == "utf-16")
			{
				this.ps.decoder = new UTF16Decoder(false);
				return;
			}
			if (!(webName == "utf-16BE"))
			{
				this.ps.decoder = encoding.GetDecoder();
				return;
			}
			this.ps.decoder = new UTF16Decoder(true);
		}

		// Token: 0x06000BA6 RID: 2982 RVA: 0x000355A0 File Offset: 0x000337A0
		private void SwitchEncoding(Encoding newEncoding)
		{
			if ((newEncoding.WebName != this.ps.encoding.WebName || this.ps.decoder is SafeAsciiDecoder) && !this.afterResetState)
			{
				this.UnDecodeChars();
				this.ps.appendMode = false;
				this.SetupEncoding(newEncoding);
				this.ReadData();
			}
		}

		// Token: 0x06000BA7 RID: 2983 RVA: 0x00035604 File Offset: 0x00033804
		private Encoding CheckEncoding(string newEncodingName)
		{
			if (this.ps.stream == null)
			{
				return this.ps.encoding;
			}
			if (string.Compare(newEncodingName, "ucs-2", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(newEncodingName, "utf-16", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(newEncodingName, "iso-10646-ucs-2", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(newEncodingName, "ucs-4", StringComparison.OrdinalIgnoreCase) == 0)
			{
				if (this.ps.encoding.WebName != "utf-16BE" && this.ps.encoding.WebName != "utf-16" && string.Compare(newEncodingName, "ucs-4", StringComparison.OrdinalIgnoreCase) != 0)
				{
					if (this.afterResetState)
					{
						this.Throw("'{0}' is an invalid value for the 'encoding' attribute. The encoding cannot be switched after a call to ResetState.", newEncodingName);
					}
					else
					{
						this.ThrowWithoutLineInfo("There is no Unicode byte order mark. Cannot switch to Unicode.");
					}
				}
				return this.ps.encoding;
			}
			Encoding encoding = null;
			if (string.Compare(newEncodingName, "utf-8", StringComparison.OrdinalIgnoreCase) == 0)
			{
				encoding = new UTF8Encoding(true, true);
			}
			else
			{
				try
				{
					encoding = Encoding.GetEncoding(newEncodingName);
				}
				catch (NotSupportedException innerException)
				{
					this.Throw("System does not support '{0}' encoding.", newEncodingName, innerException);
				}
				catch (ArgumentException innerException2)
				{
					this.Throw("System does not support '{0}' encoding.", newEncodingName, innerException2);
				}
			}
			if (this.afterResetState && this.ps.encoding.WebName != encoding.WebName)
			{
				this.Throw("'{0}' is an invalid value for the 'encoding' attribute. The encoding cannot be switched after a call to ResetState.", newEncodingName);
			}
			return encoding;
		}

		// Token: 0x06000BA8 RID: 2984 RVA: 0x00035768 File Offset: 0x00033968
		private void UnDecodeChars()
		{
			if (this.maxCharactersInDocument > 0L)
			{
				this.charactersInDocument -= (long)(this.ps.charsUsed - this.ps.charPos);
			}
			if (this.maxCharactersFromEntities > 0L && this.InEntity)
			{
				this.charactersFromEntities -= (long)(this.ps.charsUsed - this.ps.charPos);
			}
			this.ps.bytePos = this.documentStartBytePos;
			if (this.ps.charPos > 0)
			{
				this.ps.bytePos = this.ps.bytePos + this.ps.encoding.GetByteCount(this.ps.chars, 0, this.ps.charPos);
			}
			this.ps.charsUsed = this.ps.charPos;
			this.ps.isEof = false;
		}

		// Token: 0x06000BA9 RID: 2985 RVA: 0x00035852 File Offset: 0x00033A52
		private void SwitchEncodingToUTF8()
		{
			this.SwitchEncoding(new UTF8Encoding(true, true));
		}

		// Token: 0x06000BAA RID: 2986 RVA: 0x00035864 File Offset: 0x00033A64
		private int ReadData()
		{
			if (this.ps.isEof)
			{
				return 0;
			}
			int num;
			if (this.ps.appendMode)
			{
				if (this.ps.charsUsed == this.ps.chars.Length - 1)
				{
					for (int i = 0; i < this.attrCount; i++)
					{
						this.nodes[this.index + i + 1].OnBufferInvalidated();
					}
					char[] array = new char[this.ps.chars.Length * 2];
					XmlTextReaderImpl.BlockCopyChars(this.ps.chars, 0, array, 0, this.ps.chars.Length);
					this.ps.chars = array;
				}
				if (this.ps.stream != null && this.ps.bytesUsed - this.ps.bytePos < 6 && this.ps.bytes.Length - this.ps.bytesUsed < 6)
				{
					byte[] array2 = new byte[this.ps.bytes.Length * 2];
					XmlTextReaderImpl.BlockCopy(this.ps.bytes, 0, array2, 0, this.ps.bytesUsed);
					this.ps.bytes = array2;
				}
				num = this.ps.chars.Length - this.ps.charsUsed - 1;
				if (num > 80)
				{
					num = 80;
				}
			}
			else
			{
				int num2 = this.ps.chars.Length;
				if (num2 - this.ps.charsUsed <= num2 / 2)
				{
					for (int j = 0; j < this.attrCount; j++)
					{
						this.nodes[this.index + j + 1].OnBufferInvalidated();
					}
					int num3 = this.ps.charsUsed - this.ps.charPos;
					if (num3 < num2 - 1)
					{
						this.ps.lineStartPos = this.ps.lineStartPos - this.ps.charPos;
						if (num3 > 0)
						{
							XmlTextReaderImpl.BlockCopyChars(this.ps.chars, this.ps.charPos, this.ps.chars, 0, num3);
						}
						this.ps.charPos = 0;
						this.ps.charsUsed = num3;
					}
					else
					{
						char[] array3 = new char[this.ps.chars.Length * 2];
						XmlTextReaderImpl.BlockCopyChars(this.ps.chars, 0, array3, 0, this.ps.chars.Length);
						this.ps.chars = array3;
					}
				}
				if (this.ps.stream != null)
				{
					int num4 = this.ps.bytesUsed - this.ps.bytePos;
					if (num4 <= 128)
					{
						if (num4 == 0)
						{
							this.ps.bytesUsed = 0;
						}
						else
						{
							XmlTextReaderImpl.BlockCopy(this.ps.bytes, this.ps.bytePos, this.ps.bytes, 0, num4);
							this.ps.bytesUsed = num4;
						}
						this.ps.bytePos = 0;
					}
				}
				num = this.ps.chars.Length - this.ps.charsUsed - 1;
			}
			if (this.ps.stream != null)
			{
				if (!this.ps.isStreamEof && this.ps.bytePos == this.ps.bytesUsed && this.ps.bytes.Length - this.ps.bytesUsed > 0)
				{
					int num5 = this.ps.stream.Read(this.ps.bytes, this.ps.bytesUsed, this.ps.bytes.Length - this.ps.bytesUsed);
					if (num5 == 0)
					{
						this.ps.isStreamEof = true;
					}
					this.ps.bytesUsed = this.ps.bytesUsed + num5;
				}
				int bytePos = this.ps.bytePos;
				num = this.GetChars(num);
				if (num == 0 && this.ps.bytePos != bytePos)
				{
					return this.ReadData();
				}
			}
			else if (this.ps.textReader != null)
			{
				num = this.ps.textReader.Read(this.ps.chars, this.ps.charsUsed, this.ps.chars.Length - this.ps.charsUsed - 1);
				this.ps.charsUsed = this.ps.charsUsed + num;
			}
			else
			{
				num = 0;
			}
			this.RegisterConsumedCharacters((long)num, this.InEntity);
			if (num == 0)
			{
				this.ps.isEof = true;
			}
			this.ps.chars[this.ps.charsUsed] = '\0';
			return num;
		}

		// Token: 0x06000BAB RID: 2987 RVA: 0x00035D00 File Offset: 0x00033F00
		private int GetChars(int maxCharsCount)
		{
			int num = this.ps.bytesUsed - this.ps.bytePos;
			if (num == 0)
			{
				return 0;
			}
			int num2;
			try
			{
				bool flag;
				this.ps.decoder.Convert(this.ps.bytes, this.ps.bytePos, num, this.ps.chars, this.ps.charsUsed, maxCharsCount, false, out num, out num2, out flag);
			}
			catch (ArgumentException)
			{
				this.InvalidCharRecovery(ref num, out num2);
			}
			this.ps.bytePos = this.ps.bytePos + num;
			this.ps.charsUsed = this.ps.charsUsed + num2;
			return num2;
		}

		// Token: 0x06000BAC RID: 2988 RVA: 0x00035DB0 File Offset: 0x00033FB0
		private void InvalidCharRecovery(ref int bytesCount, out int charsCount)
		{
			int num = 0;
			int i = 0;
			try
			{
				while (i < bytesCount)
				{
					int num2;
					int num3;
					bool flag;
					this.ps.decoder.Convert(this.ps.bytes, this.ps.bytePos + i, 1, this.ps.chars, this.ps.charsUsed + num, 1, false, out num2, out num3, out flag);
					num += num3;
					i += num2;
				}
			}
			catch (ArgumentException)
			{
			}
			if (num == 0)
			{
				this.Throw(this.ps.charsUsed, "Invalid character in the given encoding.");
			}
			charsCount = num;
			bytesCount = i;
		}

		// Token: 0x06000BAD RID: 2989 RVA: 0x00035E50 File Offset: 0x00034050
		internal void Close(bool closeInput)
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.ReaderClosed)
			{
				return;
			}
			while (this.InEntity)
			{
				this.PopParsingState();
			}
			this.ps.Close(closeInput);
			this.curNode = XmlTextReaderImpl.NodeData.None;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ReaderClosed;
			this.reportedEncoding = null;
			this.reportedBaseUri = string.Empty;
			this.readState = ReadState.Closed;
			this.fullAttrCleanup = false;
			this.ResetAttributes();
			this.laterInitParam = null;
		}

		// Token: 0x06000BAE RID: 2990 RVA: 0x00035EC2 File Offset: 0x000340C2
		private void ShiftBuffer(int sourcePos, int destPos, int count)
		{
			XmlTextReaderImpl.BlockCopyChars(this.ps.chars, sourcePos, this.ps.chars, destPos, count);
		}

		// Token: 0x06000BAF RID: 2991 RVA: 0x00035EE4 File Offset: 0x000340E4
		private bool ParseXmlDeclaration(bool isTextDecl)
		{
			while (this.ps.charsUsed - this.ps.charPos < 6)
			{
				if (this.ReadData() == 0)
				{
					IL_7E0:
					if (!isTextDecl)
					{
						this.parsingFunction = this.nextParsingFunction;
					}
					if (this.afterResetState)
					{
						string webName = this.ps.encoding.WebName;
						if (webName != "utf-8" && webName != "utf-16" && webName != "utf-16BE" && !(this.ps.encoding is Ucs4Encoding))
						{
							this.Throw("'{0}' is an invalid value for the 'encoding' attribute. The encoding cannot be switched after a call to ResetState.", (this.ps.encoding.GetByteCount("A") == 1) ? "UTF-8" : "UTF-16");
						}
					}
					if (this.ps.decoder is SafeAsciiDecoder)
					{
						this.SwitchEncodingToUTF8();
					}
					this.ps.appendMode = false;
					return false;
				}
			}
			if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, 5, "<?xml") && !this.xmlCharType.IsNameSingleChar(this.ps.chars[this.ps.charPos + 5]))
			{
				if (!isTextDecl)
				{
					this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos + 2);
					this.curNode.SetNamedNode(XmlNodeType.XmlDeclaration, this.Xml);
				}
				this.ps.charPos = this.ps.charPos + 5;
				StringBuilder stringBuilder = isTextDecl ? new StringBuilder() : this.stringBuilder;
				int num = 0;
				Encoding encoding = null;
				for (;;)
				{
					int length = stringBuilder.Length;
					int num2 = this.EatWhitespaces((num == 0) ? null : stringBuilder);
					if (this.ps.chars[this.ps.charPos] == '?')
					{
						stringBuilder.Length = length;
						if (this.ps.chars[this.ps.charPos + 1] == '>')
						{
							break;
						}
						if (this.ps.charPos + 1 == this.ps.charsUsed)
						{
							goto IL_7B8;
						}
						this.ThrowUnexpectedToken("'>'");
					}
					if (num2 == 0 && num != 0)
					{
						this.ThrowUnexpectedToken("?>");
					}
					int num3 = this.ParseName();
					XmlTextReaderImpl.NodeData nodeData = null;
					char c = this.ps.chars[this.ps.charPos];
					if (c != 'e')
					{
						if (c != 's')
						{
							if (c != 'v' || !XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num3 - this.ps.charPos, "version") || num != 0)
							{
								goto IL_3B5;
							}
							if (!isTextDecl)
							{
								nodeData = this.AddAttributeNoChecks("version", 1);
							}
						}
						else
						{
							if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num3 - this.ps.charPos, "standalone") || (num != 1 && num != 2) || isTextDecl)
							{
								goto IL_3B5;
							}
							if (!isTextDecl)
							{
								nodeData = this.AddAttributeNoChecks("standalone", 1);
							}
							num = 2;
						}
					}
					else
					{
						if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num3 - this.ps.charPos, "encoding") || (num != 1 && (!isTextDecl || num != 0)))
						{
							goto IL_3B5;
						}
						if (!isTextDecl)
						{
							nodeData = this.AddAttributeNoChecks("encoding", 1);
						}
						num = 1;
					}
					IL_3CA:
					if (!isTextDecl)
					{
						nodeData.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
					}
					stringBuilder.Append(this.ps.chars, this.ps.charPos, num3 - this.ps.charPos);
					this.ps.charPos = num3;
					if (this.ps.chars[this.ps.charPos] != '=')
					{
						this.EatWhitespaces(stringBuilder);
						if (this.ps.chars[this.ps.charPos] != '=')
						{
							this.ThrowUnexpectedToken("=");
						}
					}
					stringBuilder.Append('=');
					this.ps.charPos = this.ps.charPos + 1;
					char c2 = this.ps.chars[this.ps.charPos];
					if (c2 != '"' && c2 != '\'')
					{
						this.EatWhitespaces(stringBuilder);
						c2 = this.ps.chars[this.ps.charPos];
						if (c2 != '"' && c2 != '\'')
						{
							this.ThrowUnexpectedToken("\"", "'");
						}
					}
					stringBuilder.Append(c2);
					this.ps.charPos = this.ps.charPos + 1;
					if (!isTextDecl)
					{
						nodeData.quoteChar = c2;
						nodeData.SetLineInfo2(this.ps.LineNo, this.ps.LinePos);
					}
					int num4 = this.ps.charPos;
					char[] chars;
					for (;;)
					{
						chars = this.ps.chars;
						while ((this.xmlCharType.charProperties[(int)chars[num4]] & 128) != 0)
						{
							num4++;
						}
						if (this.ps.chars[num4] == c2)
						{
							break;
						}
						if (num4 != this.ps.charsUsed)
						{
							goto IL_7A3;
						}
						if (this.ReadData() == 0)
						{
							goto Block_57;
						}
					}
					switch (num)
					{
					case 0:
						if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num4 - this.ps.charPos, "1.0"))
						{
							if (!isTextDecl)
							{
								nodeData.SetValue(this.ps.chars, this.ps.charPos, num4 - this.ps.charPos);
							}
							num = 1;
						}
						else
						{
							string arg = new string(this.ps.chars, this.ps.charPos, num4 - this.ps.charPos);
							this.Throw("Version number '{0}' is invalid.", arg);
						}
						break;
					case 1:
					{
						string text = new string(this.ps.chars, this.ps.charPos, num4 - this.ps.charPos);
						encoding = this.CheckEncoding(text);
						if (!isTextDecl)
						{
							nodeData.SetValue(text);
						}
						num = 2;
						break;
					}
					case 2:
						if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num4 - this.ps.charPos, "yes"))
						{
							this.standalone = true;
						}
						else if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num4 - this.ps.charPos, "no"))
						{
							this.standalone = false;
						}
						else
						{
							this.Throw("Syntax for an XML declaration is invalid.", this.ps.LineNo, this.ps.LinePos - 1);
						}
						if (!isTextDecl)
						{
							nodeData.SetValue(this.ps.chars, this.ps.charPos, num4 - this.ps.charPos);
						}
						num = 3;
						break;
					}
					stringBuilder.Append(chars, this.ps.charPos, num4 - this.ps.charPos);
					stringBuilder.Append(c2);
					this.ps.charPos = num4 + 1;
					continue;
					Block_57:
					this.Throw("There is an unclosed literal string.");
					goto IL_7B8;
					IL_7A3:
					this.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
					goto IL_7B8;
					IL_3B5:
					this.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
					goto IL_3CA;
					IL_7B8:
					if (this.ps.isEof || this.ReadData() == 0)
					{
						this.Throw("Unexpected end of file has occurred.");
					}
				}
				if (num == 0)
				{
					this.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
				}
				this.ps.charPos = this.ps.charPos + 2;
				if (!isTextDecl)
				{
					this.curNode.SetValue(stringBuilder.ToString());
					stringBuilder.Length = 0;
					this.nextParsingFunction = this.parsingFunction;
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ResetAttributesRootLevel;
				}
				if (encoding == null)
				{
					if (isTextDecl)
					{
						this.Throw("Invalid text declaration.");
					}
					if (this.afterResetState)
					{
						string webName2 = this.ps.encoding.WebName;
						if (webName2 != "utf-8" && webName2 != "utf-16" && webName2 != "utf-16BE" && !(this.ps.encoding is Ucs4Encoding))
						{
							this.Throw("'{0}' is an invalid value for the 'encoding' attribute. The encoding cannot be switched after a call to ResetState.", (this.ps.encoding.GetByteCount("A") == 1) ? "UTF-8" : "UTF-16");
						}
					}
					if (this.ps.decoder is SafeAsciiDecoder)
					{
						this.SwitchEncodingToUTF8();
					}
				}
				else
				{
					this.SwitchEncoding(encoding);
				}
				this.ps.appendMode = false;
				return true;
			}
			goto IL_7E0;
		}

		// Token: 0x06000BB0 RID: 2992 RVA: 0x0003678C File Offset: 0x0003498C
		private bool ParseDocumentContent()
		{
			bool flag = false;
			int num;
			for (;;)
			{
				bool flag2 = false;
				num = this.ps.charPos;
				char[] chars = this.ps.chars;
				if (chars[num] == '<')
				{
					flag2 = true;
					if (this.ps.charsUsed - num >= 4)
					{
						num++;
						char c = chars[num];
						if (c != '!')
						{
							if (c != '/')
							{
								if (c != '?')
								{
									goto IL_1D3;
								}
								this.ps.charPos = num + 1;
								if (this.ParsePI())
								{
									break;
								}
								continue;
							}
							else
							{
								this.Throw(num + 1, "Unexpected end tag.");
							}
						}
						else
						{
							num++;
							if (this.ps.charsUsed - num >= 2)
							{
								if (chars[num] == '-')
								{
									if (chars[num + 1] == '-')
									{
										this.ps.charPos = num + 2;
										if (this.ParseComment())
										{
											return true;
										}
										continue;
									}
									else
									{
										this.ThrowUnexpectedToken(num + 1, "-");
									}
								}
								else if (chars[num] == '[')
								{
									if (this.fragmentType != XmlNodeType.Document)
									{
										num++;
										if (this.ps.charsUsed - num >= 6)
										{
											if (XmlConvert.StrEqual(chars, num, 6, "CDATA["))
											{
												goto Block_14;
											}
											this.ThrowUnexpectedToken(num, "CDATA[");
										}
									}
									else
									{
										this.Throw(this.ps.charPos, "Data at the root level is invalid.");
									}
								}
								else if (this.fragmentType == XmlNodeType.Document || this.fragmentType == XmlNodeType.None)
								{
									this.fragmentType = XmlNodeType.Document;
									this.ps.charPos = num;
									if (this.ParseDoctypeDecl())
									{
										return true;
									}
									continue;
								}
								else if (this.ParseUnexpectedToken(num) == "DOCTYPE")
								{
									this.Throw("Unexpected DTD declaration.");
								}
								else
								{
									this.ThrowUnexpectedToken(num, "<!--", "<[CDATA[");
								}
							}
						}
					}
				}
				else if (chars[num] == '&')
				{
					if (this.fragmentType == XmlNodeType.Document)
					{
						this.Throw(num, "Data at the root level is invalid.");
					}
					else
					{
						if (this.fragmentType == XmlNodeType.None)
						{
							this.fragmentType = XmlNodeType.Element;
						}
						int num2;
						XmlTextReaderImpl.EntityType entityType = this.HandleEntityReference(false, XmlTextReaderImpl.EntityExpandType.OnlyGeneral, out num2);
						if (entityType > XmlTextReaderImpl.EntityType.CharacterNamed)
						{
							if (entityType == XmlTextReaderImpl.EntityType.Unexpanded)
							{
								goto Block_26;
							}
							chars = this.ps.chars;
							num = this.ps.charPos;
							continue;
						}
						else
						{
							if (this.ParseText())
							{
								return true;
							}
							continue;
						}
					}
				}
				else if (num != this.ps.charsUsed && ((!this.v1Compat && !flag) || chars[num] != '\0'))
				{
					if (this.fragmentType == XmlNodeType.Document)
					{
						if (this.ParseRootLevelWhitespace())
						{
							return true;
						}
						continue;
					}
					else
					{
						if (this.ParseText())
						{
							goto Block_33;
						}
						continue;
					}
				}
				if (this.ReadData() != 0)
				{
					num = this.ps.charPos;
					num = this.ps.charPos;
					chars = this.ps.chars;
				}
				else
				{
					if (flag2)
					{
						this.Throw("Data at the root level is invalid.");
					}
					if (!this.InEntity)
					{
						goto IL_34B;
					}
					if (this.HandleEntityEnd(true))
					{
						goto Block_39;
					}
				}
			}
			return true;
			Block_14:
			this.ps.charPos = num + 6;
			this.ParseCData();
			if (this.fragmentType == XmlNodeType.None)
			{
				this.fragmentType = XmlNodeType.Element;
			}
			return true;
			IL_1D3:
			if (this.rootElementParsed)
			{
				if (this.fragmentType == XmlNodeType.Document)
				{
					this.Throw(num, "There are multiple root elements.");
				}
				if (this.fragmentType == XmlNodeType.None)
				{
					this.fragmentType = XmlNodeType.Element;
				}
			}
			this.ps.charPos = num;
			this.rootElementParsed = true;
			this.ParseElement();
			return true;
			Block_26:
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.EntityReference)
			{
				this.parsingFunction = this.nextParsingFunction;
			}
			this.ParseEntityReference();
			return true;
			Block_33:
			if (this.fragmentType == XmlNodeType.None && this.curNode.type == XmlNodeType.Text)
			{
				this.fragmentType = XmlNodeType.Element;
			}
			return true;
			Block_39:
			this.SetupEndEntityNodeInContent();
			return true;
			IL_34B:
			if (!this.rootElementParsed && this.fragmentType == XmlNodeType.Document)
			{
				this.ThrowWithoutLineInfo("Root element is missing.");
			}
			if (this.fragmentType == XmlNodeType.None)
			{
				this.fragmentType = (this.rootElementParsed ? XmlNodeType.Document : XmlNodeType.Element);
			}
			this.OnEof();
			return false;
		}

		// Token: 0x06000BB1 RID: 2993 RVA: 0x00036B40 File Offset: 0x00034D40
		private bool ParseElementContent()
		{
			int num;
			for (;;)
			{
				num = this.ps.charPos;
				char[] chars = this.ps.chars;
				char c = chars[num];
				if (c != '&')
				{
					if (c == '<')
					{
						c = chars[num + 1];
						if (c != '!')
						{
							if (c == '/')
							{
								goto IL_13B;
							}
							if (c == '?')
							{
								this.ps.charPos = num + 2;
								if (this.ParsePI())
								{
									break;
								}
								continue;
							}
							else if (num + 1 != this.ps.charsUsed)
							{
								goto Block_14;
							}
						}
						else
						{
							num += 2;
							if (this.ps.charsUsed - num >= 2)
							{
								if (chars[num] == '-')
								{
									if (chars[num + 1] == '-')
									{
										this.ps.charPos = num + 2;
										if (this.ParseComment())
										{
											return true;
										}
										continue;
									}
									else
									{
										this.ThrowUnexpectedToken(num + 1, "-");
									}
								}
								else if (chars[num] == '[')
								{
									num++;
									if (this.ps.charsUsed - num >= 6)
									{
										if (XmlConvert.StrEqual(chars, num, 6, "CDATA["))
										{
											goto Block_12;
										}
										this.ThrowUnexpectedToken(num, "CDATA[");
									}
								}
								else if (this.ParseUnexpectedToken(num) == "DOCTYPE")
								{
									this.Throw("Unexpected DTD declaration.");
								}
								else
								{
									this.ThrowUnexpectedToken(num, "<!--", "<[CDATA[");
								}
							}
						}
					}
					else if (num != this.ps.charsUsed)
					{
						if (this.ParseText())
						{
							return true;
						}
						continue;
					}
					if (this.ReadData() == 0)
					{
						if (this.ps.charsUsed - this.ps.charPos != 0)
						{
							this.ThrowUnclosedElements();
						}
						if (!this.InEntity)
						{
							if (this.index == 0 && this.fragmentType != XmlNodeType.Document)
							{
								goto Block_22;
							}
							this.ThrowUnclosedElements();
						}
						if (this.HandleEntityEnd(true))
						{
							goto Block_23;
						}
					}
				}
				else if (this.ParseText())
				{
					return true;
				}
			}
			return true;
			Block_12:
			this.ps.charPos = num + 6;
			this.ParseCData();
			return true;
			IL_13B:
			this.ps.charPos = num + 2;
			this.ParseEndElement();
			return true;
			Block_14:
			this.ps.charPos = num + 1;
			this.ParseElement();
			return true;
			Block_22:
			this.OnEof();
			return false;
			Block_23:
			this.SetupEndEntityNodeInContent();
			return true;
		}

		// Token: 0x06000BB2 RID: 2994 RVA: 0x00036D54 File Offset: 0x00034F54
		private void ThrowUnclosedElements()
		{
			if (this.index == 0 && this.curNode.type != XmlNodeType.Element)
			{
				this.Throw(this.ps.charsUsed, "Unexpected end of file has occurred.");
				return;
			}
			int i = (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead) ? this.index : (this.index - 1);
			this.stringBuilder.Length = 0;
			while (i >= 0)
			{
				XmlTextReaderImpl.NodeData nodeData = this.nodes[i];
				if (nodeData.type == XmlNodeType.Element)
				{
					this.stringBuilder.Append(nodeData.GetNameWPrefix(this.nameTable));
					if (i > 0)
					{
						this.stringBuilder.Append(", ");
					}
					else
					{
						this.stringBuilder.Append(".");
					}
				}
				i--;
			}
			this.Throw(this.ps.charsUsed, "Unexpected end of file has occurred. The following elements are not closed: {0}", this.stringBuilder.ToString());
		}

		// Token: 0x06000BB3 RID: 2995 RVA: 0x00036E34 File Offset: 0x00035034
		private void ParseElement()
		{
			int num = this.ps.charPos;
			char[] chars = this.ps.chars;
			int num2 = -1;
			this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			while ((this.xmlCharType.charProperties[(int)chars[num]] & 4) != 0)
			{
				num++;
				for (;;)
				{
					if ((this.xmlCharType.charProperties[(int)chars[num]] & 8) != 0)
					{
						num++;
					}
					else
					{
						if (chars[num] != ':')
						{
							goto IL_A2;
						}
						if (num2 == -1)
						{
							break;
						}
						if (this.supportNamespaces)
						{
							goto Block_5;
						}
						num++;
					}
				}
				num2 = num;
				num++;
				continue;
				Block_5:
				this.Throw(num, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
				break;
				IL_A2:
				if (num + 1 >= this.ps.charsUsed)
				{
					break;
				}
				IL_C7:
				this.namespaceManager.PushScope();
				if (num2 == -1 || !this.supportNamespaces)
				{
					this.curNode.SetNamedNode(XmlNodeType.Element, this.nameTable.Add(chars, this.ps.charPos, num - this.ps.charPos));
				}
				else
				{
					int charPos = this.ps.charPos;
					int num3 = num2 - charPos;
					if (num3 == this.lastPrefix.Length && XmlConvert.StrEqual(chars, charPos, num3, this.lastPrefix))
					{
						this.curNode.SetNamedNode(XmlNodeType.Element, this.nameTable.Add(chars, num2 + 1, num - num2 - 1), this.lastPrefix, null);
					}
					else
					{
						this.curNode.SetNamedNode(XmlNodeType.Element, this.nameTable.Add(chars, num2 + 1, num - num2 - 1), this.nameTable.Add(chars, this.ps.charPos, num3), null);
						this.lastPrefix = this.curNode.prefix;
					}
				}
				char c = chars[num];
				if ((this.xmlCharType.charProperties[(int)c] & 1) > 0)
				{
					this.ps.charPos = num;
					this.ParseAttributes();
					return;
				}
				if (c == '>')
				{
					this.ps.charPos = num + 1;
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.MoveToElementContent;
				}
				else if (c == '/')
				{
					if (num + 1 == this.ps.charsUsed)
					{
						this.ps.charPos = num;
						if (this.ReadData() == 0)
						{
							this.Throw(num, "Unexpected end of file while parsing {0} has occurred.", ">");
						}
						num = this.ps.charPos;
						chars = this.ps.chars;
					}
					if (chars[num + 1] == '>')
					{
						this.curNode.IsEmptyElement = true;
						this.nextParsingFunction = this.parsingFunction;
						this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PopEmptyElementContext;
						this.ps.charPos = num + 2;
					}
					else
					{
						this.ThrowUnexpectedToken(num, ">");
					}
				}
				else
				{
					this.Throw(num, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(chars, this.ps.charsUsed, num));
				}
				if (this.addDefaultAttributesAndNormalize)
				{
					this.AddDefaultAttributesAndNormalize();
				}
				this.ElementNamespaceLookup();
				return;
			}
			num = this.ParseQName(out num2);
			chars = this.ps.chars;
			goto IL_C7;
		}

		// Token: 0x06000BB4 RID: 2996 RVA: 0x00037114 File Offset: 0x00035314
		private void AddDefaultAttributesAndNormalize()
		{
			IDtdAttributeListInfo dtdAttributeListInfo = this.dtdInfo.LookupAttributeList(this.curNode.localName, this.curNode.prefix);
			if (dtdAttributeListInfo == null)
			{
				return;
			}
			if (this.normalize && dtdAttributeListInfo.HasNonCDataAttributes)
			{
				for (int i = this.index + 1; i < this.index + 1 + this.attrCount; i++)
				{
					XmlTextReaderImpl.NodeData nodeData = this.nodes[i];
					IDtdAttributeInfo dtdAttributeInfo = dtdAttributeListInfo.LookupAttribute(nodeData.prefix, nodeData.localName);
					if (dtdAttributeInfo != null && dtdAttributeInfo.IsNonCDataType)
					{
						if (this.DtdValidation && this.standalone && dtdAttributeInfo.IsDeclaredInExternal)
						{
							string stringValue = nodeData.StringValue;
							nodeData.TrimSpacesInValue();
							if (stringValue != nodeData.StringValue)
							{
								this.SendValidationEvent(XmlSeverityType.Error, "StandAlone is 'yes' and the value of the attribute '{0}' contains a definition in an external document that changes on normalization.", nodeData.GetNameWPrefix(this.nameTable), nodeData.LineNo, nodeData.LinePos);
							}
						}
						else
						{
							nodeData.TrimSpacesInValue();
						}
					}
				}
			}
			IEnumerable<IDtdDefaultAttributeInfo> enumerable = dtdAttributeListInfo.LookupDefaultAttributes();
			if (enumerable != null)
			{
				int num = this.attrCount;
				XmlTextReaderImpl.NodeData[] array = null;
				if (this.attrCount >= 250)
				{
					array = new XmlTextReaderImpl.NodeData[this.attrCount];
					Array.Copy(this.nodes, this.index + 1, array, 0, this.attrCount);
					Array.Sort<object>(array, XmlTextReaderImpl.DtdDefaultAttributeInfoToNodeDataComparer.Instance);
				}
				foreach (IDtdDefaultAttributeInfo dtdDefaultAttributeInfo in enumerable)
				{
					if (this.AddDefaultAttributeDtd(dtdDefaultAttributeInfo, true, array) && this.DtdValidation && this.standalone && dtdDefaultAttributeInfo.IsDeclaredInExternal)
					{
						string prefix = dtdDefaultAttributeInfo.Prefix;
						string arg = (prefix.Length == 0) ? dtdDefaultAttributeInfo.LocalName : (prefix + ":" + dtdDefaultAttributeInfo.LocalName);
						this.SendValidationEvent(XmlSeverityType.Error, "Markup for unspecified default attribute '{0}' is external and standalone='yes'.", arg, this.curNode.LineNo, this.curNode.LinePos);
					}
				}
				if (num == 0 && this.attrNeedNamespaceLookup)
				{
					this.AttributeNamespaceLookup();
					this.attrNeedNamespaceLookup = false;
				}
			}
		}

		// Token: 0x06000BB5 RID: 2997 RVA: 0x0003733C File Offset: 0x0003553C
		private void ParseEndElement()
		{
			XmlTextReaderImpl.NodeData nodeData = this.nodes[this.index - 1];
			int length = nodeData.prefix.Length;
			int length2 = nodeData.localName.Length;
			while (this.ps.charsUsed - this.ps.charPos < length + length2 + 1 && this.ReadData() != 0)
			{
			}
			char[] chars = this.ps.chars;
			int num;
			if (nodeData.prefix.Length == 0)
			{
				if (!XmlConvert.StrEqual(chars, this.ps.charPos, length2, nodeData.localName))
				{
					this.ThrowTagMismatch(nodeData);
				}
				num = length2;
			}
			else
			{
				int num2 = this.ps.charPos + length;
				if (!XmlConvert.StrEqual(chars, this.ps.charPos, length, nodeData.prefix) || chars[num2] != ':' || !XmlConvert.StrEqual(chars, num2 + 1, length2, nodeData.localName))
				{
					this.ThrowTagMismatch(nodeData);
				}
				num = length2 + length + 1;
			}
			LineInfo lineInfo = new LineInfo(this.ps.lineNo, this.ps.LinePos);
			int num3;
			for (;;)
			{
				num3 = this.ps.charPos + num;
				chars = this.ps.chars;
				if (num3 != this.ps.charsUsed)
				{
					if ((this.xmlCharType.charProperties[(int)chars[num3]] & 8) != 0 || chars[num3] == ':')
					{
						this.ThrowTagMismatch(nodeData);
					}
					if (chars[num3] != '>')
					{
						char c;
						while (this.xmlCharType.IsWhiteSpace(c = chars[num3]))
						{
							num3++;
							if (c != '\n')
							{
								if (c == '\r')
								{
									if (chars[num3] == '\n')
									{
										num3++;
									}
									else if (num3 == this.ps.charsUsed && !this.ps.isEof)
									{
										continue;
									}
									this.OnNewLine(num3);
								}
							}
							else
							{
								this.OnNewLine(num3);
							}
						}
					}
					if (chars[num3] == '>')
					{
						break;
					}
					if (num3 != this.ps.charsUsed)
					{
						this.ThrowUnexpectedToken(num3, ">");
					}
				}
				if (this.ReadData() == 0)
				{
					this.ThrowUnclosedElements();
				}
			}
			this.index--;
			this.curNode = this.nodes[this.index];
			nodeData.lineInfo = lineInfo;
			nodeData.type = XmlNodeType.EndElement;
			this.ps.charPos = num3 + 1;
			this.nextParsingFunction = ((this.index > 0) ? this.parsingFunction : XmlTextReaderImpl.ParsingFunction.DocumentContent);
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PopElementContext;
		}

		// Token: 0x06000BB6 RID: 2998 RVA: 0x000375AC File Offset: 0x000357AC
		private void ThrowTagMismatch(XmlTextReaderImpl.NodeData startTag)
		{
			if (startTag.type == XmlNodeType.Element)
			{
				int num2;
				int num = this.ParseQName(out num2);
				this.Throw("The '{0}' start tag on line {1} position {2} does not match the end tag of '{3}'.", new string[]
				{
					startTag.GetNameWPrefix(this.nameTable),
					startTag.lineInfo.lineNo.ToString(CultureInfo.InvariantCulture),
					startTag.lineInfo.linePos.ToString(CultureInfo.InvariantCulture),
					new string(this.ps.chars, this.ps.charPos, num - this.ps.charPos)
				});
				return;
			}
			this.Throw("Unexpected end tag.");
		}

		// Token: 0x06000BB7 RID: 2999 RVA: 0x00037658 File Offset: 0x00035858
		private void ParseAttributes()
		{
			int num = this.ps.charPos;
			char[] chars = this.ps.chars;
			for (;;)
			{
				IL_1A:
				int num2 = 0;
				char c;
				while ((this.xmlCharType.charProperties[(int)(c = chars[num])] & 1) != 0)
				{
					if (c == '\n')
					{
						this.OnNewLine(num + 1);
						num2++;
					}
					else if (c == '\r')
					{
						if (chars[num + 1] == '\n')
						{
							this.OnNewLine(num + 2);
							num2++;
							num++;
						}
						else if (num + 1 != this.ps.charsUsed)
						{
							this.OnNewLine(num + 1);
							num2++;
						}
						else
						{
							this.ps.charPos = num;
							IL_42C:
							this.ps.lineNo = this.ps.lineNo - num2;
							if (this.ReadData() != 0)
							{
								num = this.ps.charPos;
								chars = this.ps.chars;
								goto IL_1A;
							}
							this.ThrowUnclosedElements();
							goto IL_1A;
						}
					}
					num++;
				}
				int num3 = 0;
				char c2;
				if ((this.xmlCharType.charProperties[(int)(c2 = chars[num])] & 4) != 0)
				{
					num3 = 1;
				}
				if (num3 == 0)
				{
					if (c2 == '>')
					{
						break;
					}
					if (c2 == '/')
					{
						if (num + 1 == this.ps.charsUsed)
						{
							goto IL_42C;
						}
						if (chars[num + 1] == '>')
						{
							goto Block_11;
						}
						this.ThrowUnexpectedToken(num + 1, ">");
					}
					else
					{
						if (num == this.ps.charsUsed)
						{
							goto IL_42C;
						}
						if (c2 != ':' || this.supportNamespaces)
						{
							this.Throw(num, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(chars, this.ps.charsUsed, num));
						}
					}
				}
				if (num == this.ps.charPos)
				{
					this.ThrowExpectingWhitespace(num);
				}
				this.ps.charPos = num;
				int linePos = this.ps.LinePos;
				int num4 = -1;
				num += num3;
				for (;;)
				{
					char c3;
					if ((this.xmlCharType.charProperties[(int)(c3 = chars[num])] & 8) != 0)
					{
						num++;
					}
					else
					{
						if (c3 != ':')
						{
							goto IL_23E;
						}
						if (num4 != -1)
						{
							if (this.supportNamespaces)
							{
								goto Block_18;
							}
							num++;
						}
						else
						{
							num4 = num;
							num++;
							if ((this.xmlCharType.charProperties[(int)chars[num]] & 4) == 0)
							{
								goto IL_227;
							}
							num++;
						}
					}
				}
				IL_263:
				XmlTextReaderImpl.NodeData nodeData = this.AddAttribute(num, num4);
				nodeData.SetLineInfo(this.ps.LineNo, linePos);
				if (chars[num] != '=')
				{
					this.ps.charPos = num;
					this.EatWhitespaces(null);
					num = this.ps.charPos;
					if (chars[num] != '=')
					{
						this.ThrowUnexpectedToken("=");
					}
				}
				num++;
				char c4 = chars[num];
				if (c4 != '"' && c4 != '\'')
				{
					this.ps.charPos = num;
					this.EatWhitespaces(null);
					num = this.ps.charPos;
					c4 = chars[num];
					if (c4 != '"' && c4 != '\'')
					{
						this.ThrowUnexpectedToken("\"", "'");
					}
				}
				num++;
				this.ps.charPos = num;
				nodeData.quoteChar = c4;
				nodeData.SetLineInfo2(this.ps.LineNo, this.ps.LinePos);
				char c5;
				while ((this.xmlCharType.charProperties[(int)(c5 = chars[num])] & 128) != 0)
				{
					num++;
				}
				if (c5 == c4)
				{
					nodeData.SetValue(chars, this.ps.charPos, num - this.ps.charPos);
					num++;
					this.ps.charPos = num;
				}
				else
				{
					this.ParseAttributeValueSlow(num, c4, nodeData);
					num = this.ps.charPos;
					chars = this.ps.chars;
				}
				if (nodeData.prefix.Length == 0)
				{
					if (Ref.Equal(nodeData.localName, this.XmlNs))
					{
						this.OnDefaultNamespaceDecl(nodeData);
						continue;
					}
					continue;
				}
				else
				{
					if (Ref.Equal(nodeData.prefix, this.XmlNs))
					{
						this.OnNamespaceDecl(nodeData);
						continue;
					}
					if (Ref.Equal(nodeData.prefix, this.Xml))
					{
						this.OnXmlReservedAttribute(nodeData);
						continue;
					}
					continue;
				}
				Block_18:
				this.Throw(num, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
				goto IL_263;
				IL_227:
				num = this.ParseQName(out num4);
				chars = this.ps.chars;
				goto IL_263;
				IL_23E:
				if (num + 1 >= this.ps.charsUsed)
				{
					num = this.ParseQName(out num4);
					chars = this.ps.chars;
					goto IL_263;
				}
				goto IL_263;
			}
			this.ps.charPos = num + 1;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.MoveToElementContent;
			goto IL_46C;
			Block_11:
			this.ps.charPos = num + 2;
			this.curNode.IsEmptyElement = true;
			this.nextParsingFunction = this.parsingFunction;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PopEmptyElementContext;
			IL_46C:
			if (this.addDefaultAttributesAndNormalize)
			{
				this.AddDefaultAttributesAndNormalize();
			}
			this.ElementNamespaceLookup();
			if (this.attrNeedNamespaceLookup)
			{
				this.AttributeNamespaceLookup();
				this.attrNeedNamespaceLookup = false;
			}
			if (this.attrDuplWalkCount >= 250)
			{
				this.AttributeDuplCheck();
			}
		}

		// Token: 0x06000BB8 RID: 3000 RVA: 0x00037B10 File Offset: 0x00035D10
		private void ElementNamespaceLookup()
		{
			if (this.curNode.prefix.Length == 0)
			{
				this.curNode.ns = this.xmlContext.defaultNamespace;
				return;
			}
			this.curNode.ns = this.LookupNamespace(this.curNode);
		}

		// Token: 0x06000BB9 RID: 3001 RVA: 0x00037B60 File Offset: 0x00035D60
		private void AttributeNamespaceLookup()
		{
			for (int i = this.index + 1; i < this.index + this.attrCount + 1; i++)
			{
				XmlTextReaderImpl.NodeData nodeData = this.nodes[i];
				if (nodeData.type == XmlNodeType.Attribute && nodeData.prefix.Length > 0)
				{
					nodeData.ns = this.LookupNamespace(nodeData);
				}
			}
		}

		// Token: 0x06000BBA RID: 3002 RVA: 0x00037BBC File Offset: 0x00035DBC
		private void AttributeDuplCheck()
		{
			if (this.attrCount < 250)
			{
				for (int i = this.index + 1; i < this.index + 1 + this.attrCount; i++)
				{
					XmlTextReaderImpl.NodeData nodeData = this.nodes[i];
					for (int j = i + 1; j < this.index + 1 + this.attrCount; j++)
					{
						if (Ref.Equal(nodeData.localName, this.nodes[j].localName) && Ref.Equal(nodeData.ns, this.nodes[j].ns))
						{
							this.Throw("'{0}' is a duplicate attribute name.", this.nodes[j].GetNameWPrefix(this.nameTable), this.nodes[j].LineNo, this.nodes[j].LinePos);
						}
					}
				}
				return;
			}
			if (this.attrDuplSortingArray == null || this.attrDuplSortingArray.Length < this.attrCount)
			{
				this.attrDuplSortingArray = new XmlTextReaderImpl.NodeData[this.attrCount];
			}
			Array.Copy(this.nodes, this.index + 1, this.attrDuplSortingArray, 0, this.attrCount);
			Array.Sort<XmlTextReaderImpl.NodeData>(this.attrDuplSortingArray, 0, this.attrCount);
			XmlTextReaderImpl.NodeData nodeData2 = this.attrDuplSortingArray[0];
			for (int k = 1; k < this.attrCount; k++)
			{
				XmlTextReaderImpl.NodeData nodeData3 = this.attrDuplSortingArray[k];
				if (Ref.Equal(nodeData2.localName, nodeData3.localName) && Ref.Equal(nodeData2.ns, nodeData3.ns))
				{
					this.Throw("'{0}' is a duplicate attribute name.", nodeData3.GetNameWPrefix(this.nameTable), nodeData3.LineNo, nodeData3.LinePos);
				}
				nodeData2 = nodeData3;
			}
		}

		// Token: 0x06000BBB RID: 3003 RVA: 0x00037D6C File Offset: 0x00035F6C
		private void OnDefaultNamespaceDecl(XmlTextReaderImpl.NodeData attr)
		{
			if (!this.supportNamespaces)
			{
				return;
			}
			string text = this.nameTable.Add(attr.StringValue);
			attr.ns = this.nameTable.Add("http://www.w3.org/2000/xmlns/");
			if (!this.curNode.xmlContextPushed)
			{
				this.PushXmlContext();
			}
			this.xmlContext.defaultNamespace = text;
			this.AddNamespace(string.Empty, text, attr);
		}

		// Token: 0x06000BBC RID: 3004 RVA: 0x00037DD8 File Offset: 0x00035FD8
		private void OnNamespaceDecl(XmlTextReaderImpl.NodeData attr)
		{
			if (!this.supportNamespaces)
			{
				return;
			}
			string text = this.nameTable.Add(attr.StringValue);
			if (text.Length == 0)
			{
				this.Throw("Invalid namespace declaration.", attr.lineInfo2.lineNo, attr.lineInfo2.linePos - 1);
			}
			this.AddNamespace(attr.localName, text, attr);
		}

		// Token: 0x06000BBD RID: 3005 RVA: 0x00037E3C File Offset: 0x0003603C
		private void OnXmlReservedAttribute(XmlTextReaderImpl.NodeData attr)
		{
			string a = attr.localName;
			if (!(a == "space"))
			{
				if (!(a == "lang"))
				{
					return;
				}
				if (!this.curNode.xmlContextPushed)
				{
					this.PushXmlContext();
				}
				this.xmlContext.xmlLang = attr.StringValue;
				return;
			}
			else
			{
				if (!this.curNode.xmlContextPushed)
				{
					this.PushXmlContext();
				}
				a = XmlConvert.TrimString(attr.StringValue);
				if (a == "preserve")
				{
					this.xmlContext.xmlSpace = XmlSpace.Preserve;
					return;
				}
				if (!(a == "default"))
				{
					this.Throw("'{0}' is an invalid xml:space value.", attr.StringValue, attr.lineInfo.lineNo, attr.lineInfo.linePos);
					return;
				}
				this.xmlContext.xmlSpace = XmlSpace.Default;
				return;
			}
		}

		// Token: 0x06000BBE RID: 3006 RVA: 0x00037F0C File Offset: 0x0003610C
		private void ParseAttributeValueSlow(int curPos, char quoteChar, XmlTextReaderImpl.NodeData attr)
		{
			int num = curPos;
			char[] chars = this.ps.chars;
			int entityId = this.ps.entityId;
			int num2 = 0;
			LineInfo lineInfo = new LineInfo(this.ps.lineNo, this.ps.LinePos);
			XmlTextReaderImpl.NodeData nodeData = null;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)chars[num]] & 128) == 0)
				{
					if (num - this.ps.charPos > 0)
					{
						this.stringBuilder.Append(chars, this.ps.charPos, num - this.ps.charPos);
						this.ps.charPos = num;
					}
					if (chars[num] == quoteChar && entityId == this.ps.entityId)
					{
						goto IL_63F;
					}
					char c = chars[num];
					if (c <= '&')
					{
						switch (c)
						{
						case '\t':
							num++;
							if (this.normalize)
							{
								this.stringBuilder.Append(' ');
								this.ps.charPos = this.ps.charPos + 1;
								continue;
							}
							continue;
						case '\n':
							num++;
							this.OnNewLine(num);
							if (this.normalize)
							{
								this.stringBuilder.Append(' ');
								this.ps.charPos = this.ps.charPos + 1;
								continue;
							}
							continue;
						case '\v':
						case '\f':
							goto IL_4F8;
						case '\r':
							if (chars[num + 1] == '\n')
							{
								num += 2;
								if (this.normalize)
								{
									this.stringBuilder.Append(this.ps.eolNormalized ? "  " : " ");
									this.ps.charPos = num;
								}
							}
							else
							{
								if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_54A;
								}
								num++;
								if (this.normalize)
								{
									this.stringBuilder.Append(' ');
									this.ps.charPos = num;
								}
							}
							this.OnNewLine(num);
							continue;
						default:
							if (c != '"')
							{
								if (c != '&')
								{
									goto IL_4F8;
								}
								if (num - this.ps.charPos > 0)
								{
									this.stringBuilder.Append(chars, this.ps.charPos, num - this.ps.charPos);
								}
								this.ps.charPos = num;
								int entityId2 = this.ps.entityId;
								LineInfo lineInfo2 = new LineInfo(this.ps.lineNo, this.ps.LinePos + 1);
								switch (this.HandleEntityReference(true, XmlTextReaderImpl.EntityExpandType.All, out num))
								{
								case XmlTextReaderImpl.EntityType.CharacterDec:
								case XmlTextReaderImpl.EntityType.CharacterHex:
								case XmlTextReaderImpl.EntityType.CharacterNamed:
									break;
								case XmlTextReaderImpl.EntityType.Expanded:
								case XmlTextReaderImpl.EntityType.Skipped:
								case XmlTextReaderImpl.EntityType.FakeExpanded:
									goto IL_4DB;
								case XmlTextReaderImpl.EntityType.Unexpanded:
									if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full && this.ps.entityId == entityId)
									{
										int num3 = this.stringBuilder.Length - num2;
										if (num3 > 0)
										{
											XmlTextReaderImpl.NodeData nodeData2 = new XmlTextReaderImpl.NodeData();
											nodeData2.lineInfo = lineInfo;
											nodeData2.depth = attr.depth + 1;
											nodeData2.SetValueNode(XmlNodeType.Text, this.stringBuilder.ToString(num2, num3));
											this.AddAttributeChunkToList(attr, nodeData2, ref nodeData);
										}
										this.ps.charPos = this.ps.charPos + 1;
										string text = this.ParseEntityName();
										XmlTextReaderImpl.NodeData nodeData3 = new XmlTextReaderImpl.NodeData();
										nodeData3.lineInfo = lineInfo2;
										nodeData3.depth = attr.depth + 1;
										nodeData3.SetNamedNode(XmlNodeType.EntityReference, text);
										this.AddAttributeChunkToList(attr, nodeData3, ref nodeData);
										this.stringBuilder.Append('&');
										this.stringBuilder.Append(text);
										this.stringBuilder.Append(';');
										num2 = this.stringBuilder.Length;
										lineInfo.Set(this.ps.LineNo, this.ps.LinePos);
										this.fullAttrCleanup = true;
									}
									else
									{
										this.ps.charPos = this.ps.charPos + 1;
										this.ParseEntityName();
									}
									num = this.ps.charPos;
									break;
								case XmlTextReaderImpl.EntityType.ExpandedInAttribute:
									if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full && entityId2 == entityId)
									{
										int num4 = this.stringBuilder.Length - num2;
										if (num4 > 0)
										{
											XmlTextReaderImpl.NodeData nodeData4 = new XmlTextReaderImpl.NodeData();
											nodeData4.lineInfo = lineInfo;
											nodeData4.depth = attr.depth + 1;
											nodeData4.SetValueNode(XmlNodeType.Text, this.stringBuilder.ToString(num2, num4));
											this.AddAttributeChunkToList(attr, nodeData4, ref nodeData);
										}
										XmlTextReaderImpl.NodeData nodeData5 = new XmlTextReaderImpl.NodeData();
										nodeData5.lineInfo = lineInfo2;
										nodeData5.depth = attr.depth + 1;
										nodeData5.SetNamedNode(XmlNodeType.EntityReference, this.ps.entity.Name);
										this.AddAttributeChunkToList(attr, nodeData5, ref nodeData);
										this.fullAttrCleanup = true;
									}
									num = this.ps.charPos;
									break;
								default:
									goto IL_4DB;
								}
								IL_4E7:
								chars = this.ps.chars;
								continue;
								IL_4DB:
								num = this.ps.charPos;
								goto IL_4E7;
							}
							break;
						}
					}
					else if (c != '\'')
					{
						if (c == '<')
						{
							this.Throw(num, "'{0}', hexadecimal value {1}, is an invalid attribute character.", XmlException.BuildCharExceptionArgs('<', '\0'));
							goto IL_54A;
						}
						if (c != '>')
						{
							goto IL_4F8;
						}
					}
					num++;
					continue;
					IL_4F8:
					if (num != this.ps.charsUsed)
					{
						if (XmlCharType.IsHighSurrogate((int)chars[num]))
						{
							if (num + 1 == this.ps.charsUsed)
							{
								goto IL_54A;
							}
							num++;
							if (XmlCharType.IsLowSurrogate((int)chars[num]))
							{
								num++;
								continue;
							}
						}
						this.ThrowInvalidChar(chars, this.ps.charsUsed, num);
					}
					IL_54A:
					if (this.ReadData() == 0)
					{
						if (this.ps.charsUsed - this.ps.charPos > 0)
						{
							if (this.ps.chars[this.ps.charPos] != '\r')
							{
								this.Throw("Unexpected end of file has occurred.");
							}
						}
						else
						{
							if (!this.InEntity)
							{
								if (this.fragmentType == XmlNodeType.Attribute)
								{
									break;
								}
								this.Throw("There is an unclosed literal string.");
							}
							if (this.HandleEntityEnd(true))
							{
								this.Throw("An internal error has occurred.");
							}
							if (entityId == this.ps.entityId)
							{
								num2 = this.stringBuilder.Length;
								lineInfo.Set(this.ps.LineNo, this.ps.LinePos);
							}
						}
					}
					num = this.ps.charPos;
					chars = this.ps.chars;
				}
				else
				{
					num++;
				}
			}
			if (entityId != this.ps.entityId)
			{
				this.Throw("Entity replacement text must nest properly within markup declarations.");
			}
			IL_63F:
			if (attr.nextAttrValueChunk != null)
			{
				int num5 = this.stringBuilder.Length - num2;
				if (num5 > 0)
				{
					XmlTextReaderImpl.NodeData nodeData6 = new XmlTextReaderImpl.NodeData();
					nodeData6.lineInfo = lineInfo;
					nodeData6.depth = attr.depth + 1;
					nodeData6.SetValueNode(XmlNodeType.Text, this.stringBuilder.ToString(num2, num5));
					this.AddAttributeChunkToList(attr, nodeData6, ref nodeData);
				}
			}
			this.ps.charPos = num + 1;
			attr.SetValue(this.stringBuilder.ToString());
			this.stringBuilder.Length = 0;
		}

		// Token: 0x06000BBF RID: 3007 RVA: 0x000385DF File Offset: 0x000367DF
		private void AddAttributeChunkToList(XmlTextReaderImpl.NodeData attr, XmlTextReaderImpl.NodeData chunk, ref XmlTextReaderImpl.NodeData lastChunk)
		{
			if (lastChunk == null)
			{
				lastChunk = chunk;
				attr.nextAttrValueChunk = chunk;
				return;
			}
			lastChunk.nextAttrValueChunk = chunk;
			lastChunk = chunk;
		}

		// Token: 0x06000BC0 RID: 3008 RVA: 0x000385FC File Offset: 0x000367FC
		private bool ParseText()
		{
			int num = 0;
			if (this.parsingMode != XmlTextReaderImpl.ParsingMode.Full)
			{
				int num2;
				int num3;
				while (!this.ParseText(out num2, out num3, ref num))
				{
				}
			}
			else
			{
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
				int num2;
				int num3;
				if (this.ParseText(out num2, out num3, ref num))
				{
					if (num3 - num2 != 0)
					{
						XmlNodeType textNodeType = this.GetTextNodeType(num);
						if (textNodeType != XmlNodeType.None)
						{
							this.curNode.SetValueNode(textNodeType, this.ps.chars, num2, num3 - num2);
							return true;
						}
					}
				}
				else if (this.v1Compat)
				{
					do
					{
						if (num3 - num2 > 0)
						{
							this.stringBuilder.Append(this.ps.chars, num2, num3 - num2);
						}
					}
					while (!this.ParseText(out num2, out num3, ref num));
					if (num3 - num2 > 0)
					{
						this.stringBuilder.Append(this.ps.chars, num2, num3 - num2);
					}
					XmlNodeType textNodeType2 = this.GetTextNodeType(num);
					if (textNodeType2 != XmlNodeType.None)
					{
						this.curNode.SetValueNode(textNodeType2, this.stringBuilder.ToString());
						this.stringBuilder.Length = 0;
						return true;
					}
					this.stringBuilder.Length = 0;
				}
				else
				{
					if (num > 32)
					{
						this.curNode.SetValueNode(XmlNodeType.Text, this.ps.chars, num2, num3 - num2);
						this.nextParsingFunction = this.parsingFunction;
						this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PartialTextValue;
						return true;
					}
					if (num3 - num2 > 0)
					{
						this.stringBuilder.Append(this.ps.chars, num2, num3 - num2);
					}
					bool flag;
					do
					{
						flag = this.ParseText(out num2, out num3, ref num);
						if (num3 - num2 > 0)
						{
							this.stringBuilder.Append(this.ps.chars, num2, num3 - num2);
						}
					}
					while (!flag && num <= 32 && this.stringBuilder.Length < 4096);
					XmlNodeType xmlNodeType = (this.stringBuilder.Length < 4096) ? this.GetTextNodeType(num) : XmlNodeType.Text;
					if (xmlNodeType != XmlNodeType.None)
					{
						this.curNode.SetValueNode(xmlNodeType, this.stringBuilder.ToString());
						this.stringBuilder.Length = 0;
						if (!flag)
						{
							this.nextParsingFunction = this.parsingFunction;
							this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PartialTextValue;
						}
						return true;
					}
					this.stringBuilder.Length = 0;
					if (!flag)
					{
						while (!this.ParseText(out num2, out num3, ref num))
						{
						}
					}
				}
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.ReportEndEntity)
			{
				this.SetupEndEntityNodeInContent();
				this.parsingFunction = this.nextParsingFunction;
				return true;
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.EntityReference)
			{
				this.parsingFunction = this.nextNextParsingFunction;
				this.ParseEntityReference();
				return true;
			}
			return false;
		}

		// Token: 0x06000BC1 RID: 3009 RVA: 0x00038888 File Offset: 0x00036A88
		private bool ParseText(out int startPos, out int endPos, ref int outOrChars)
		{
			char[] chars = this.ps.chars;
			int num = this.ps.charPos;
			int num2 = 0;
			int num3 = -1;
			int num4 = outOrChars;
			char c;
			int num7;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)(c = chars[num])] & 64) == 0)
				{
					if (c <= '&')
					{
						switch (c)
						{
						case '\t':
							num++;
							continue;
						case '\n':
							num++;
							this.OnNewLine(num);
							continue;
						case '\v':
						case '\f':
							break;
						case '\r':
							if (chars[num + 1] == '\n')
							{
								if (!this.ps.eolNormalized && this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
								{
									if (num - this.ps.charPos > 0)
									{
										if (num2 == 0)
										{
											num2 = 1;
											num3 = num;
										}
										else
										{
											this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
											num3 = num - num2;
											num2++;
										}
									}
									else
									{
										this.ps.charPos = this.ps.charPos + 1;
									}
								}
								num += 2;
							}
							else
							{
								if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_366;
								}
								if (!this.ps.eolNormalized)
								{
									chars[num] = '\n';
								}
								num++;
							}
							this.OnNewLine(num);
							continue;
						default:
							if (c == '&')
							{
								int num6;
								XmlTextReaderImpl.EntityType entityType;
								int num5;
								if ((num5 = this.ParseCharRefInline(num, out num6, out entityType)) > 0)
								{
									if (num2 > 0)
									{
										this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
									}
									num3 = num - num2;
									num2 += num5 - num - num6;
									num = num5;
									if (!this.xmlCharType.IsWhiteSpace(chars[num5 - num6]) || (this.v1Compat && entityType == XmlTextReaderImpl.EntityType.CharacterDec))
									{
										num4 |= 255;
										continue;
									}
									continue;
								}
								else
								{
									if (num > this.ps.charPos)
									{
										goto IL_42F;
									}
									switch (this.HandleEntityReference(false, XmlTextReaderImpl.EntityExpandType.All, out num))
									{
									case XmlTextReaderImpl.EntityType.CharacterDec:
										if (!this.v1Compat)
										{
											goto IL_221;
										}
										num4 |= 255;
										break;
									case XmlTextReaderImpl.EntityType.CharacterHex:
									case XmlTextReaderImpl.EntityType.CharacterNamed:
										goto IL_221;
									case XmlTextReaderImpl.EntityType.Expanded:
									case XmlTextReaderImpl.EntityType.Skipped:
									case XmlTextReaderImpl.EntityType.FakeExpanded:
										goto IL_249;
									case XmlTextReaderImpl.EntityType.Unexpanded:
										goto IL_1F4;
									default:
										goto IL_249;
									}
									IL_255:
									chars = this.ps.chars;
									continue;
									IL_249:
									num = this.ps.charPos;
									goto IL_255;
									IL_221:
									if (!this.xmlCharType.IsWhiteSpace(this.ps.chars[num - 1]))
									{
										num4 |= 255;
										goto IL_255;
									}
									goto IL_255;
								}
							}
							break;
						}
					}
					else
					{
						if (c == '<')
						{
							goto IL_42F;
						}
						if (c == ']')
						{
							if (this.ps.charsUsed - num >= 3 || this.ps.isEof)
							{
								if (chars[num + 1] == ']' && chars[num + 2] == '>')
								{
									this.Throw(num, "']]>' is not allowed in character data.");
								}
								num4 |= 93;
								num++;
								continue;
							}
							goto IL_366;
						}
					}
					if (num != this.ps.charsUsed)
					{
						char c2 = chars[num];
						if (XmlCharType.IsHighSurrogate((int)c2))
						{
							if (num + 1 == this.ps.charsUsed)
							{
								goto IL_366;
							}
							num++;
							if (XmlCharType.IsLowSurrogate((int)chars[num]))
							{
								num++;
								num4 |= (int)c2;
								continue;
							}
						}
						num7 = num - this.ps.charPos;
						if (this.ZeroEndingStream(num))
						{
							goto Block_29;
						}
						this.ThrowInvalidChar(this.ps.chars, this.ps.charsUsed, this.ps.charPos + num7);
					}
					IL_366:
					if (num > this.ps.charPos)
					{
						goto IL_42F;
					}
					if (this.ReadData() == 0)
					{
						if (this.ps.charsUsed - this.ps.charPos > 0)
						{
							if (this.ps.chars[this.ps.charPos] != '\r' && this.ps.chars[this.ps.charPos] != ']')
							{
								this.Throw("Unexpected end of file has occurred.");
							}
						}
						else
						{
							if (!this.InEntity)
							{
								goto IL_423;
							}
							if (this.HandleEntityEnd(true))
							{
								goto Block_36;
							}
						}
					}
					num = this.ps.charPos;
					chars = this.ps.chars;
				}
				else
				{
					num4 |= (int)c;
					num++;
				}
			}
			IL_1F4:
			this.nextParsingFunction = this.parsingFunction;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.EntityReference;
			goto IL_423;
			Block_29:
			chars = this.ps.chars;
			num = this.ps.charPos + num7;
			goto IL_42F;
			Block_36:
			this.nextParsingFunction = this.parsingFunction;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ReportEndEntity;
			IL_423:
			startPos = (endPos = num);
			return true;
			IL_42F:
			if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full && num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
			}
			startPos = this.ps.charPos;
			endPos = num - num2;
			this.ps.charPos = num;
			outOrChars = num4;
			return c == '<';
		}

		// Token: 0x06000BC2 RID: 3010 RVA: 0x00038D08 File Offset: 0x00036F08
		private void FinishPartialValue()
		{
			this.curNode.CopyTo(this.readValueOffset, this.stringBuilder);
			int num = 0;
			int num2;
			int num3;
			while (!this.ParseText(out num2, out num3, ref num))
			{
				this.stringBuilder.Append(this.ps.chars, num2, num3 - num2);
			}
			this.stringBuilder.Append(this.ps.chars, num2, num3 - num2);
			this.curNode.SetValue(this.stringBuilder.ToString());
			this.stringBuilder.Length = 0;
		}

		// Token: 0x06000BC3 RID: 3011 RVA: 0x00038D98 File Offset: 0x00036F98
		private void FinishOtherValueIterator()
		{
			switch (this.parsingFunction)
			{
			case XmlTextReaderImpl.ParsingFunction.InReadAttributeValue:
				break;
			case XmlTextReaderImpl.ParsingFunction.InReadValueChunk:
				if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue)
				{
					this.FinishPartialValue();
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue;
					return;
				}
				if (this.readValueOffset > 0)
				{
					this.curNode.SetValue(this.curNode.StringValue.Substring(this.readValueOffset));
					this.readValueOffset = 0;
					return;
				}
				break;
			case XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary:
			case XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary:
				switch (this.incReadState)
				{
				case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue:
					if (this.readValueOffset > 0)
					{
						this.curNode.SetValue(this.curNode.StringValue.Substring(this.readValueOffset));
						this.readValueOffset = 0;
						return;
					}
					break;
				case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue:
					this.FinishPartialValue();
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue;
					return;
				case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End:
					this.curNode.SetValue(string.Empty);
					break;
				default:
					return;
				}
				break;
			default:
				return;
			}
		}

		// Token: 0x06000BC4 RID: 3012 RVA: 0x00038E84 File Offset: 0x00037084
		[MethodImpl(MethodImplOptions.NoInlining)]
		private void SkipPartialTextValue()
		{
			int num = 0;
			this.parsingFunction = this.nextParsingFunction;
			int num2;
			int num3;
			while (!this.ParseText(out num2, out num3, ref num))
			{
			}
		}

		// Token: 0x06000BC5 RID: 3013 RVA: 0x00038EAD File Offset: 0x000370AD
		private void FinishReadValueChunk()
		{
			this.readValueOffset = 0;
			if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue)
			{
				this.SkipPartialTextValue();
				return;
			}
			this.parsingFunction = this.nextParsingFunction;
			this.nextParsingFunction = this.nextNextParsingFunction;
		}

		// Token: 0x06000BC6 RID: 3014 RVA: 0x00038EE0 File Offset: 0x000370E0
		private void FinishReadContentAsBinary()
		{
			this.readValueOffset = 0;
			if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue)
			{
				this.SkipPartialTextValue();
			}
			else
			{
				this.parsingFunction = this.nextParsingFunction;
				this.nextParsingFunction = this.nextNextParsingFunction;
			}
			if (this.incReadState != XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End)
			{
				while (this.MoveToNextContentNode(true))
				{
				}
			}
		}

		// Token: 0x06000BC7 RID: 3015 RVA: 0x00038F34 File Offset: 0x00037134
		private void FinishReadElementContentAsBinary()
		{
			this.FinishReadContentAsBinary();
			if (this.curNode.type != XmlNodeType.EndElement)
			{
				this.Throw("'{0}' is an invalid XmlNodeType.", this.curNode.type.ToString());
			}
			this.outerReader.Read();
		}

		// Token: 0x06000BC8 RID: 3016 RVA: 0x00038F84 File Offset: 0x00037184
		private bool ParseRootLevelWhitespace()
		{
			XmlNodeType whitespaceType = this.GetWhitespaceType();
			if (whitespaceType == XmlNodeType.None)
			{
				this.EatWhitespaces(null);
				if (this.ps.chars[this.ps.charPos] == '<' || this.ps.charsUsed - this.ps.charPos == 0 || this.ZeroEndingStream(this.ps.charPos))
				{
					return false;
				}
			}
			else
			{
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
				this.EatWhitespaces(this.stringBuilder);
				if (this.ps.chars[this.ps.charPos] == '<' || this.ps.charsUsed - this.ps.charPos == 0 || this.ZeroEndingStream(this.ps.charPos))
				{
					if (this.stringBuilder.Length > 0)
					{
						this.curNode.SetValueNode(whitespaceType, this.stringBuilder.ToString());
						this.stringBuilder.Length = 0;
						return true;
					}
					return false;
				}
			}
			if (this.xmlCharType.IsCharData(this.ps.chars[this.ps.charPos]))
			{
				this.Throw("Data at the root level is invalid.");
			}
			else
			{
				this.ThrowInvalidChar(this.ps.chars, this.ps.charsUsed, this.ps.charPos);
			}
			return false;
		}

		// Token: 0x06000BC9 RID: 3017 RVA: 0x000390F4 File Offset: 0x000372F4
		private void ParseEntityReference()
		{
			this.ps.charPos = this.ps.charPos + 1;
			this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			this.curNode.SetNamedNode(XmlNodeType.EntityReference, this.ParseEntityName());
		}

		// Token: 0x06000BCA RID: 3018 RVA: 0x00039144 File Offset: 0x00037344
		private XmlTextReaderImpl.EntityType HandleEntityReference(bool isInAttributeValue, XmlTextReaderImpl.EntityExpandType expandType, out int charRefEndPos)
		{
			if (this.ps.charPos + 1 == this.ps.charsUsed && this.ReadData() == 0)
			{
				this.Throw("Unexpected end of file has occurred.");
			}
			if (this.ps.chars[this.ps.charPos + 1] == '#')
			{
				XmlTextReaderImpl.EntityType result;
				charRefEndPos = this.ParseNumericCharRef(expandType != XmlTextReaderImpl.EntityExpandType.OnlyGeneral, null, out result);
				return result;
			}
			charRefEndPos = this.ParseNamedCharRef(expandType != XmlTextReaderImpl.EntityExpandType.OnlyGeneral, null);
			if (charRefEndPos >= 0)
			{
				return XmlTextReaderImpl.EntityType.CharacterNamed;
			}
			if (expandType == XmlTextReaderImpl.EntityExpandType.OnlyCharacter || (this.entityHandling != EntityHandling.ExpandEntities && (!isInAttributeValue || !this.validatingReaderCompatFlag)))
			{
				return XmlTextReaderImpl.EntityType.Unexpanded;
			}
			this.ps.charPos = this.ps.charPos + 1;
			int linePos = this.ps.LinePos;
			int num;
			try
			{
				num = this.ParseName();
			}
			catch (XmlException)
			{
				this.Throw("An error occurred while parsing EntityName.", this.ps.LineNo, linePos);
				return XmlTextReaderImpl.EntityType.Skipped;
			}
			if (this.ps.chars[num] != ';')
			{
				this.ThrowUnexpectedToken(num, ";");
			}
			int linePos2 = this.ps.LinePos;
			string name = this.nameTable.Add(this.ps.chars, this.ps.charPos, num - this.ps.charPos);
			this.ps.charPos = num + 1;
			charRefEndPos = -1;
			XmlTextReaderImpl.EntityType result2 = this.HandleGeneralEntityReference(name, isInAttributeValue, false, linePos2);
			this.reportedBaseUri = this.ps.baseUriStr;
			this.reportedEncoding = this.ps.encoding;
			return result2;
		}

		// Token: 0x06000BCB RID: 3019 RVA: 0x000392D0 File Offset: 0x000374D0
		private XmlTextReaderImpl.EntityType HandleGeneralEntityReference(string name, bool isInAttributeValue, bool pushFakeEntityIfNullResolver, int entityStartLinePos)
		{
			IDtdEntityInfo dtdEntityInfo = null;
			if (this.dtdInfo == null && this.fragmentParserContext != null && this.fragmentParserContext.HasDtdInfo && this.dtdProcessing == DtdProcessing.Parse)
			{
				this.ParseDtdFromParserContext();
			}
			if (this.dtdInfo == null || (dtdEntityInfo = this.dtdInfo.LookupEntity(name)) == null)
			{
				if (this.disableUndeclaredEntityCheck)
				{
					dtdEntityInfo = new SchemaEntity(new XmlQualifiedName(name), false)
					{
						Text = string.Empty
					};
				}
				else
				{
					this.Throw("Reference to undeclared entity '{0}'.", name, this.ps.LineNo, entityStartLinePos);
				}
			}
			if (dtdEntityInfo.IsUnparsedEntity)
			{
				if (this.disableUndeclaredEntityCheck)
				{
					dtdEntityInfo = new SchemaEntity(new XmlQualifiedName(name), false)
					{
						Text = string.Empty
					};
				}
				else
				{
					this.Throw("Reference to unparsed entity '{0}'.", name, this.ps.LineNo, entityStartLinePos);
				}
			}
			if (this.standalone && dtdEntityInfo.IsDeclaredInExternal)
			{
				this.Throw("Standalone document declaration must have a value of 'no' because an external entity '{0}' is referenced.", dtdEntityInfo.Name, this.ps.LineNo, entityStartLinePos);
			}
			if (dtdEntityInfo.IsExternal)
			{
				if (isInAttributeValue)
				{
					this.Throw("External entity '{0}' reference cannot appear in the attribute value.", name, this.ps.LineNo, entityStartLinePos);
					return XmlTextReaderImpl.EntityType.Skipped;
				}
				if (this.parsingMode == XmlTextReaderImpl.ParsingMode.SkipContent)
				{
					return XmlTextReaderImpl.EntityType.Skipped;
				}
				if (this.IsResolverNull)
				{
					if (pushFakeEntityIfNullResolver)
					{
						this.PushExternalEntity(dtdEntityInfo);
						this.curNode.entityId = this.ps.entityId;
						return XmlTextReaderImpl.EntityType.FakeExpanded;
					}
					return XmlTextReaderImpl.EntityType.Skipped;
				}
				else
				{
					this.PushExternalEntity(dtdEntityInfo);
					this.curNode.entityId = this.ps.entityId;
					if (!isInAttributeValue || !this.validatingReaderCompatFlag)
					{
						return XmlTextReaderImpl.EntityType.Expanded;
					}
					return XmlTextReaderImpl.EntityType.ExpandedInAttribute;
				}
			}
			else
			{
				if (this.parsingMode == XmlTextReaderImpl.ParsingMode.SkipContent)
				{
					return XmlTextReaderImpl.EntityType.Skipped;
				}
				this.PushInternalEntity(dtdEntityInfo);
				this.curNode.entityId = this.ps.entityId;
				if (!isInAttributeValue || !this.validatingReaderCompatFlag)
				{
					return XmlTextReaderImpl.EntityType.Expanded;
				}
				return XmlTextReaderImpl.EntityType.ExpandedInAttribute;
			}
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x06000BCC RID: 3020 RVA: 0x00039493 File Offset: 0x00037693
		private bool InEntity
		{
			get
			{
				return this.parsingStatesStackTop >= 0;
			}
		}

		// Token: 0x06000BCD RID: 3021 RVA: 0x000394A4 File Offset: 0x000376A4
		private bool HandleEntityEnd(bool checkEntityNesting)
		{
			if (this.parsingStatesStackTop == -1)
			{
				this.Throw("An internal error has occurred.");
			}
			if (this.ps.entityResolvedManually)
			{
				this.index--;
				if (checkEntityNesting && this.ps.entityId != this.nodes[this.index].entityId)
				{
					this.Throw("Incomplete entity contents.");
				}
				this.lastEntity = this.ps.entity;
				this.PopEntity();
				return true;
			}
			if (checkEntityNesting && this.ps.entityId != this.nodes[this.index].entityId)
			{
				this.Throw("Incomplete entity contents.");
			}
			this.PopEntity();
			this.reportedEncoding = this.ps.encoding;
			this.reportedBaseUri = this.ps.baseUriStr;
			return false;
		}

		// Token: 0x06000BCE RID: 3022 RVA: 0x0003957C File Offset: 0x0003777C
		private void SetupEndEntityNodeInContent()
		{
			this.reportedEncoding = this.ps.encoding;
			this.reportedBaseUri = this.ps.baseUriStr;
			this.curNode = this.nodes[this.index];
			this.curNode.SetNamedNode(XmlNodeType.EndEntity, this.lastEntity.Name);
			this.curNode.lineInfo.Set(this.ps.lineNo, this.ps.LinePos - 1);
			if (this.index == 0 && this.parsingFunction == XmlTextReaderImpl.ParsingFunction.ElementContent)
			{
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.DocumentContent;
			}
		}

		// Token: 0x06000BCF RID: 3023 RVA: 0x00039618 File Offset: 0x00037818
		private void SetupEndEntityNodeInAttribute()
		{
			this.curNode = this.nodes[this.index + this.attrCount + 1];
			XmlTextReaderImpl.NodeData nodeData = this.curNode;
			nodeData.lineInfo.linePos = nodeData.lineInfo.linePos + this.curNode.localName.Length;
			this.curNode.type = XmlNodeType.EndEntity;
		}

		// Token: 0x06000BD0 RID: 3024 RVA: 0x00039672 File Offset: 0x00037872
		private bool ParsePI()
		{
			return this.ParsePI(null);
		}

		// Token: 0x06000BD1 RID: 3025 RVA: 0x0003967C File Offset: 0x0003787C
		private bool ParsePI(StringBuilder piInDtdStringBuilder)
		{
			if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
			{
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			}
			int num = this.ParseName();
			string text = this.nameTable.Add(this.ps.chars, this.ps.charPos, num - this.ps.charPos);
			if (string.Compare(text, "xml", StringComparison.OrdinalIgnoreCase) == 0)
			{
				this.Throw(text.Equals("xml") ? "Unexpected XML declaration. The XML declaration must be the first node in the document, and no white space characters are allowed to appear before it." : "'{0}' is an invalid name for processing instructions.", text);
			}
			this.ps.charPos = num;
			if (piInDtdStringBuilder == null)
			{
				if (!this.ignorePIs && this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
				{
					this.curNode.SetNamedNode(XmlNodeType.ProcessingInstruction, text);
				}
			}
			else
			{
				piInDtdStringBuilder.Append(text);
			}
			char c = this.ps.chars[this.ps.charPos];
			if (this.EatWhitespaces(piInDtdStringBuilder) == 0)
			{
				if (this.ps.charsUsed - this.ps.charPos < 2)
				{
					this.ReadData();
				}
				if (c != '?' || this.ps.chars[this.ps.charPos + 1] != '>')
				{
					this.Throw("The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(this.ps.chars, this.ps.charsUsed, this.ps.charPos));
				}
			}
			int num2;
			int num3;
			if (this.ParsePIValue(out num2, out num3))
			{
				if (piInDtdStringBuilder == null)
				{
					if (this.ignorePIs)
					{
						return false;
					}
					if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
					{
						this.curNode.SetValue(this.ps.chars, num2, num3 - num2);
					}
				}
				else
				{
					piInDtdStringBuilder.Append(this.ps.chars, num2, num3 - num2);
				}
			}
			else
			{
				StringBuilder stringBuilder;
				if (piInDtdStringBuilder == null)
				{
					if (this.ignorePIs || this.parsingMode != XmlTextReaderImpl.ParsingMode.Full)
					{
						while (!this.ParsePIValue(out num2, out num3))
						{
						}
						return false;
					}
					stringBuilder = this.stringBuilder;
				}
				else
				{
					stringBuilder = piInDtdStringBuilder;
				}
				do
				{
					stringBuilder.Append(this.ps.chars, num2, num3 - num2);
				}
				while (!this.ParsePIValue(out num2, out num3));
				stringBuilder.Append(this.ps.chars, num2, num3 - num2);
				if (piInDtdStringBuilder == null)
				{
					this.curNode.SetValue(this.stringBuilder.ToString());
					this.stringBuilder.Length = 0;
				}
			}
			return true;
		}

		// Token: 0x06000BD2 RID: 3026 RVA: 0x000398D4 File Offset: 0x00037AD4
		private bool ParsePIValue(out int outStartPos, out int outEndPos)
		{
			if (this.ps.charsUsed - this.ps.charPos < 2 && this.ReadData() == 0)
			{
				this.Throw(this.ps.charsUsed, "Unexpected end of file while parsing {0} has occurred.", "PI");
			}
			int num = this.ps.charPos;
			char[] chars = this.ps.chars;
			int num2 = 0;
			int num3 = -1;
			for (;;)
			{
				char c;
				if ((this.xmlCharType.charProperties[(int)(c = chars[num])] & 64) == 0 || c == '?')
				{
					char c2 = chars[num];
					if (c2 <= '&')
					{
						switch (c2)
						{
						case '\t':
							break;
						case '\n':
							num++;
							this.OnNewLine(num);
							continue;
						case '\v':
						case '\f':
							goto IL_1F0;
						case '\r':
							if (chars[num + 1] == '\n')
							{
								if (!this.ps.eolNormalized && this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
								{
									if (num - this.ps.charPos > 0)
									{
										if (num2 == 0)
										{
											num2 = 1;
											num3 = num;
										}
										else
										{
											this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
											num3 = num - num2;
											num2++;
										}
									}
									else
									{
										this.ps.charPos = this.ps.charPos + 1;
									}
								}
								num += 2;
							}
							else
							{
								if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_247;
								}
								if (!this.ps.eolNormalized)
								{
									chars[num] = '\n';
								}
								num++;
							}
							this.OnNewLine(num);
							continue;
						default:
							if (c2 != '&')
							{
								goto IL_1F0;
							}
							break;
						}
					}
					else if (c2 != '<')
					{
						if (c2 != '?')
						{
							if (c2 != ']')
							{
								goto IL_1F0;
							}
						}
						else
						{
							if (chars[num + 1] == '>')
							{
								break;
							}
							if (num + 1 != this.ps.charsUsed)
							{
								num++;
								continue;
							}
							goto IL_247;
						}
					}
					num++;
					continue;
					IL_1F0:
					if (num == this.ps.charsUsed)
					{
						goto IL_247;
					}
					if (XmlCharType.IsHighSurrogate((int)chars[num]))
					{
						if (num + 1 == this.ps.charsUsed)
						{
							goto IL_247;
						}
						num++;
						if (XmlCharType.IsLowSurrogate((int)chars[num]))
						{
							num++;
							continue;
						}
					}
					this.ThrowInvalidChar(chars, this.ps.charsUsed, num);
				}
				else
				{
					num++;
				}
			}
			if (num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
				outEndPos = num - num2;
			}
			else
			{
				outEndPos = num;
			}
			outStartPos = this.ps.charPos;
			this.ps.charPos = num + 2;
			return true;
			IL_247:
			if (num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
				outEndPos = num - num2;
			}
			else
			{
				outEndPos = num;
			}
			outStartPos = this.ps.charPos;
			this.ps.charPos = num;
			return false;
		}

		// Token: 0x06000BD3 RID: 3027 RVA: 0x00039B60 File Offset: 0x00037D60
		private bool ParseComment()
		{
			if (this.ignoreComments)
			{
				XmlTextReaderImpl.ParsingMode parsingMode = this.parsingMode;
				this.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
				this.ParseCDataOrComment(XmlNodeType.Comment);
				this.parsingMode = parsingMode;
				return false;
			}
			this.ParseCDataOrComment(XmlNodeType.Comment);
			return true;
		}

		// Token: 0x06000BD4 RID: 3028 RVA: 0x00039B9B File Offset: 0x00037D9B
		private void ParseCData()
		{
			this.ParseCDataOrComment(XmlNodeType.CDATA);
		}

		// Token: 0x06000BD5 RID: 3029 RVA: 0x00039BA4 File Offset: 0x00037DA4
		private void ParseCDataOrComment(XmlNodeType type)
		{
			int num;
			int num2;
			if (this.parsingMode != XmlTextReaderImpl.ParsingMode.Full)
			{
				while (!this.ParseCDataOrComment(type, out num, out num2))
				{
				}
				return;
			}
			this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			if (this.ParseCDataOrComment(type, out num, out num2))
			{
				this.curNode.SetValueNode(type, this.ps.chars, num, num2 - num);
				return;
			}
			do
			{
				this.stringBuilder.Append(this.ps.chars, num, num2 - num);
			}
			while (!this.ParseCDataOrComment(type, out num, out num2));
			this.stringBuilder.Append(this.ps.chars, num, num2 - num);
			this.curNode.SetValueNode(type, this.stringBuilder.ToString());
			this.stringBuilder.Length = 0;
		}

		// Token: 0x06000BD6 RID: 3030 RVA: 0x00039C7C File Offset: 0x00037E7C
		private bool ParseCDataOrComment(XmlNodeType type, out int outStartPos, out int outEndPos)
		{
			if (this.ps.charsUsed - this.ps.charPos < 3 && this.ReadData() == 0)
			{
				this.Throw("Unexpected end of file while parsing {0} has occurred.", (type == XmlNodeType.Comment) ? "Comment" : "CDATA");
			}
			int num = this.ps.charPos;
			char[] chars = this.ps.chars;
			int num2 = 0;
			int num3 = -1;
			char c = (type == XmlNodeType.Comment) ? '-' : ']';
			for (;;)
			{
				char c2;
				if ((this.xmlCharType.charProperties[(int)(c2 = chars[num])] & 64) == 0 || c2 == c)
				{
					if (chars[num] == c)
					{
						if (chars[num + 1] == c)
						{
							if (chars[num + 2] == '>')
							{
								break;
							}
							if (num + 2 == this.ps.charsUsed)
							{
								goto IL_27D;
							}
							if (type == XmlNodeType.Comment)
							{
								this.Throw(num, "An XML comment cannot contain '--', and '-' cannot be the last character.");
							}
						}
						else if (num + 1 == this.ps.charsUsed)
						{
							goto IL_27D;
						}
						num++;
					}
					else
					{
						char c3 = chars[num];
						if (c3 <= '&')
						{
							switch (c3)
							{
							case '\t':
								break;
							case '\n':
								num++;
								this.OnNewLine(num);
								continue;
							case '\v':
							case '\f':
								goto IL_22B;
							case '\r':
								if (chars[num + 1] == '\n')
								{
									if (!this.ps.eolNormalized && this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
									{
										if (num - this.ps.charPos > 0)
										{
											if (num2 == 0)
											{
												num2 = 1;
												num3 = num;
											}
											else
											{
												this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
												num3 = num - num2;
												num2++;
											}
										}
										else
										{
											this.ps.charPos = this.ps.charPos + 1;
										}
									}
									num += 2;
								}
								else
								{
									if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
									{
										goto IL_27D;
									}
									if (!this.ps.eolNormalized)
									{
										chars[num] = '\n';
									}
									num++;
								}
								this.OnNewLine(num);
								continue;
							default:
								if (c3 != '&')
								{
									goto IL_22B;
								}
								break;
							}
						}
						else if (c3 != '<' && c3 != ']')
						{
							goto IL_22B;
						}
						num++;
						continue;
						IL_22B:
						if (num == this.ps.charsUsed)
						{
							goto IL_27D;
						}
						if (!XmlCharType.IsHighSurrogate((int)chars[num]))
						{
							goto IL_26A;
						}
						if (num + 1 == this.ps.charsUsed)
						{
							goto IL_27D;
						}
						num++;
						if (!XmlCharType.IsLowSurrogate((int)chars[num]))
						{
							goto IL_26A;
						}
						num++;
					}
				}
				else
				{
					num++;
				}
			}
			if (num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
				outEndPos = num - num2;
			}
			else
			{
				outEndPos = num;
			}
			outStartPos = this.ps.charPos;
			this.ps.charPos = num + 3;
			return true;
			IL_26A:
			this.ThrowInvalidChar(chars, this.ps.charsUsed, num);
			IL_27D:
			if (num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
				outEndPos = num - num2;
			}
			else
			{
				outEndPos = num;
			}
			outStartPos = this.ps.charPos;
			this.ps.charPos = num;
			return false;
		}

		// Token: 0x06000BD7 RID: 3031 RVA: 0x00039F40 File Offset: 0x00038140
		private bool ParseDoctypeDecl()
		{
			if (this.dtdProcessing == DtdProcessing.Prohibit)
			{
				this.ThrowWithoutLineInfo(this.v1Compat ? "DTD is prohibited in this XML document." : "For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.");
			}
			while (this.ps.charsUsed - this.ps.charPos < 8)
			{
				if (this.ReadData() == 0)
				{
					this.Throw("Unexpected end of file while parsing {0} has occurred.", "DOCTYPE");
				}
			}
			if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, 7, "DOCTYPE"))
			{
				this.ThrowUnexpectedToken((!this.rootElementParsed && this.dtdInfo == null) ? "DOCTYPE" : "<!--");
			}
			if (!this.xmlCharType.IsWhiteSpace(this.ps.chars[this.ps.charPos + 7]))
			{
				this.ThrowExpectingWhitespace(this.ps.charPos + 7);
			}
			if (this.dtdInfo != null)
			{
				this.Throw(this.ps.charPos - 2, "Cannot have multiple DTDs.");
			}
			if (this.rootElementParsed)
			{
				this.Throw(this.ps.charPos - 2, "DTD must be defined before the document root element.");
			}
			this.ps.charPos = this.ps.charPos + 8;
			this.EatWhitespaces(null);
			if (this.dtdProcessing == DtdProcessing.Parse)
			{
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
				this.ParseDtd();
				this.nextParsingFunction = this.parsingFunction;
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ResetAttributesRootLevel;
				return true;
			}
			this.SkipDtd();
			return false;
		}

		// Token: 0x06000BD8 RID: 3032 RVA: 0x0003A0C8 File Offset: 0x000382C8
		private void ParseDtd()
		{
			IDtdParser dtdParser = DtdParser.Create();
			this.dtdInfo = dtdParser.ParseInternalDtd(new XmlTextReaderImpl.DtdParserProxy(this), true);
			if ((this.validatingReaderCompatFlag || !this.v1Compat) && (this.dtdInfo.HasDefaultAttributes || this.dtdInfo.HasNonCDataAttributes))
			{
				this.addDefaultAttributesAndNormalize = true;
			}
			this.curNode.SetNamedNode(XmlNodeType.DocumentType, this.dtdInfo.Name.ToString(), string.Empty, null);
			this.curNode.SetValue(this.dtdInfo.InternalDtdSubset);
		}

		// Token: 0x06000BD9 RID: 3033 RVA: 0x0003A158 File Offset: 0x00038358
		private void SkipDtd()
		{
			int num;
			int charPos = this.ParseQName(out num);
			this.ps.charPos = charPos;
			this.EatWhitespaces(null);
			if (this.ps.chars[this.ps.charPos] == 'P')
			{
				while (this.ps.charsUsed - this.ps.charPos < 6)
				{
					if (this.ReadData() == 0)
					{
						this.Throw("Unexpected end of file has occurred.");
					}
				}
				if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, 6, "PUBLIC"))
				{
					this.ThrowUnexpectedToken("PUBLIC");
				}
				this.ps.charPos = this.ps.charPos + 6;
				if (this.EatWhitespaces(null) == 0)
				{
					this.ThrowExpectingWhitespace(this.ps.charPos);
				}
				this.SkipPublicOrSystemIdLiteral();
				if (this.EatWhitespaces(null) == 0)
				{
					this.ThrowExpectingWhitespace(this.ps.charPos);
				}
				this.SkipPublicOrSystemIdLiteral();
				this.EatWhitespaces(null);
			}
			else if (this.ps.chars[this.ps.charPos] == 'S')
			{
				while (this.ps.charsUsed - this.ps.charPos < 6)
				{
					if (this.ReadData() == 0)
					{
						this.Throw("Unexpected end of file has occurred.");
					}
				}
				if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, 6, "SYSTEM"))
				{
					this.ThrowUnexpectedToken("SYSTEM");
				}
				this.ps.charPos = this.ps.charPos + 6;
				if (this.EatWhitespaces(null) == 0)
				{
					this.ThrowExpectingWhitespace(this.ps.charPos);
				}
				this.SkipPublicOrSystemIdLiteral();
				this.EatWhitespaces(null);
			}
			else if (this.ps.chars[this.ps.charPos] != '[' && this.ps.chars[this.ps.charPos] != '>')
			{
				this.Throw("Expecting external ID, '[' or '>'.");
			}
			if (this.ps.chars[this.ps.charPos] == '[')
			{
				this.ps.charPos = this.ps.charPos + 1;
				this.SkipUntil(']', true);
				this.EatWhitespaces(null);
				if (this.ps.chars[this.ps.charPos] != '>')
				{
					this.ThrowUnexpectedToken(">");
				}
			}
			else if (this.ps.chars[this.ps.charPos] == '>')
			{
				this.curNode.SetValue(string.Empty);
			}
			else
			{
				this.Throw("Expecting an internal subset or the end of the DOCTYPE declaration.");
			}
			this.ps.charPos = this.ps.charPos + 1;
		}

		// Token: 0x06000BDA RID: 3034 RVA: 0x0003A3FC File Offset: 0x000385FC
		private void SkipPublicOrSystemIdLiteral()
		{
			char c = this.ps.chars[this.ps.charPos];
			if (c != '"' && c != '\'')
			{
				this.ThrowUnexpectedToken("\"", "'");
			}
			this.ps.charPos = this.ps.charPos + 1;
			this.SkipUntil(c, false);
		}

		// Token: 0x06000BDB RID: 3035 RVA: 0x0003A454 File Offset: 0x00038654
		private void SkipUntil(char stopChar, bool recognizeLiterals)
		{
			bool flag = false;
			bool flag2 = false;
			bool flag3 = false;
			char c = '"';
			char[] chars = this.ps.chars;
			int num = this.ps.charPos;
			for (;;)
			{
				char c2;
				if ((this.xmlCharType.charProperties[(int)(c2 = chars[num])] & 128) == 0 || chars[num] == stopChar || c2 == '-' || c2 == '?')
				{
					if (c2 == stopChar && !flag)
					{
						break;
					}
					this.ps.charPos = num;
					if (c2 <= '&')
					{
						switch (c2)
						{
						case '\t':
							break;
						case '\n':
							num++;
							this.OnNewLine(num);
							continue;
						case '\v':
						case '\f':
							goto IL_2D1;
						case '\r':
							if (chars[num + 1] == '\n')
							{
								num += 2;
							}
							else
							{
								if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_32F;
								}
								num++;
							}
							this.OnNewLine(num);
							continue;
						default:
							if (c2 == '"')
							{
								goto IL_2AC;
							}
							if (c2 != '&')
							{
								goto IL_2D1;
							}
							break;
						}
					}
					else if (c2 <= '-')
					{
						if (c2 == '\'')
						{
							goto IL_2AC;
						}
						if (c2 != '-')
						{
							goto IL_2D1;
						}
						if (flag2)
						{
							if (num + 2 >= this.ps.charsUsed && !this.ps.isEof)
							{
								goto IL_32F;
							}
							if (chars[num + 1] == '-' && chars[num + 2] == '>')
							{
								flag2 = false;
								num += 2;
								continue;
							}
						}
						num++;
						continue;
					}
					else
					{
						switch (c2)
						{
						case '<':
							if (chars[num + 1] == '?')
							{
								if (recognizeLiterals && !flag && !flag2)
								{
									flag3 = true;
									num += 2;
									continue;
								}
							}
							else if (chars[num + 1] == '!')
							{
								if (num + 3 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_32F;
								}
								if (chars[num + 2] == '-' && chars[num + 3] == '-' && recognizeLiterals && !flag && !flag3)
								{
									flag2 = true;
									num += 4;
									continue;
								}
							}
							else if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
							{
								goto IL_32F;
							}
							num++;
							continue;
						case '=':
							goto IL_2D1;
						case '>':
							break;
						case '?':
							if (flag3)
							{
								if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_32F;
								}
								if (chars[num + 1] == '>')
								{
									flag3 = false;
									num++;
									continue;
								}
							}
							num++;
							continue;
						default:
							if (c2 != ']')
							{
								goto IL_2D1;
							}
							break;
						}
					}
					num++;
					continue;
					IL_2AC:
					if (flag)
					{
						if (c == c2)
						{
							flag = false;
						}
					}
					else if (recognizeLiterals && !flag2 && !flag3)
					{
						flag = true;
						c = c2;
					}
					num++;
					continue;
					IL_2D1:
					if (num != this.ps.charsUsed)
					{
						if (XmlCharType.IsHighSurrogate((int)chars[num]))
						{
							if (num + 1 == this.ps.charsUsed)
							{
								goto IL_32F;
							}
							num++;
							if (XmlCharType.IsLowSurrogate((int)chars[num]))
							{
								num++;
								continue;
							}
						}
						this.ThrowInvalidChar(chars, this.ps.charsUsed, num);
					}
					IL_32F:
					if (this.ReadData() == 0)
					{
						if (this.ps.charsUsed - this.ps.charPos > 0)
						{
							if (this.ps.chars[this.ps.charPos] != '\r')
							{
								this.Throw("Unexpected end of file has occurred.");
							}
						}
						else
						{
							this.Throw("Unexpected end of file has occurred.");
						}
					}
					chars = this.ps.chars;
					num = this.ps.charPos;
				}
				else
				{
					num++;
				}
			}
			this.ps.charPos = num + 1;
		}

		// Token: 0x06000BDC RID: 3036 RVA: 0x0003A804 File Offset: 0x00038A04
		private int EatWhitespaces(StringBuilder sb)
		{
			int num = this.ps.charPos;
			int num2 = 0;
			char[] chars = this.ps.chars;
			for (;;)
			{
				char c = chars[num];
				switch (c)
				{
				case '\t':
					break;
				case '\n':
					num++;
					this.OnNewLine(num);
					continue;
				case '\v':
				case '\f':
					goto IL_FE;
				case '\r':
					if (chars[num + 1] == '\n')
					{
						int num3 = num - this.ps.charPos;
						if (sb != null && !this.ps.eolNormalized)
						{
							if (num3 > 0)
							{
								sb.Append(chars, this.ps.charPos, num3);
								num2 += num3;
							}
							this.ps.charPos = num + 1;
						}
						num += 2;
					}
					else
					{
						if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
						{
							goto IL_155;
						}
						if (!this.ps.eolNormalized)
						{
							chars[num] = '\n';
						}
						num++;
					}
					this.OnNewLine(num);
					continue;
				default:
					if (c != ' ')
					{
						goto IL_FE;
					}
					break;
				}
				num++;
				continue;
				IL_155:
				int num4 = num - this.ps.charPos;
				if (num4 > 0)
				{
					if (sb != null)
					{
						sb.Append(this.ps.chars, this.ps.charPos, num4);
					}
					this.ps.charPos = num;
					num2 += num4;
				}
				if (this.ReadData() == 0)
				{
					if (this.ps.charsUsed - this.ps.charPos == 0)
					{
						return num2;
					}
					if (this.ps.chars[this.ps.charPos] != '\r')
					{
						this.Throw("Unexpected end of file has occurred.");
					}
				}
				num = this.ps.charPos;
				chars = this.ps.chars;
				continue;
				IL_FE:
				if (num != this.ps.charsUsed)
				{
					break;
				}
				goto IL_155;
			}
			int num5 = num - this.ps.charPos;
			if (num5 > 0)
			{
				if (sb != null)
				{
					sb.Append(this.ps.chars, this.ps.charPos, num5);
				}
				this.ps.charPos = num;
				num2 += num5;
			}
			return num2;
		}

		// Token: 0x06000BDD RID: 3037 RVA: 0x0003AA0E File Offset: 0x00038C0E
		private int ParseCharRefInline(int startPos, out int charCount, out XmlTextReaderImpl.EntityType entityType)
		{
			if (this.ps.chars[startPos + 1] == '#')
			{
				return this.ParseNumericCharRefInline(startPos, true, null, out charCount, out entityType);
			}
			charCount = 1;
			entityType = XmlTextReaderImpl.EntityType.CharacterNamed;
			return this.ParseNamedCharRefInline(startPos, true, null);
		}

		// Token: 0x06000BDE RID: 3038 RVA: 0x0003AA40 File Offset: 0x00038C40
		private int ParseNumericCharRef(bool expand, StringBuilder internalSubsetBuilder, out XmlTextReaderImpl.EntityType entityType)
		{
			int num3;
			int num;
			for (;;)
			{
				int num2;
				num = (num2 = this.ParseNumericCharRefInline(this.ps.charPos, expand, internalSubsetBuilder, out num3, out entityType));
				if (num2 != -2)
				{
					break;
				}
				if (this.ReadData() == 0)
				{
					this.Throw("Unexpected end of file while parsing {0} has occurred.");
				}
			}
			if (expand)
			{
				this.ps.charPos = num - num3;
			}
			return num;
		}

		// Token: 0x06000BDF RID: 3039 RVA: 0x0003AA94 File Offset: 0x00038C94
		private int ParseNumericCharRefInline(int startPos, bool expand, StringBuilder internalSubsetBuilder, out int charCount, out XmlTextReaderImpl.EntityType entityType)
		{
			int num = 0;
			string res = null;
			char[] chars = this.ps.chars;
			int num2 = startPos + 2;
			charCount = 0;
			int num3 = 0;
			try
			{
				if (chars[num2] == 'x')
				{
					num2++;
					num3 = num2;
					res = "Invalid syntax for a hexadecimal numeric entity reference.";
					for (;;)
					{
						char c = chars[num2];
						checked
						{
							if (c >= '0' && c <= '9')
							{
								num = num * 16 + (int)c - 48;
							}
							else if (c >= 'a' && c <= 'f')
							{
								num = num * 16 + 10 + (int)c - 97;
							}
							else
							{
								if (c < 'A' || c > 'F')
								{
									break;
								}
								num = num * 16 + 10 + (int)c - 65;
							}
						}
						num2++;
					}
					entityType = XmlTextReaderImpl.EntityType.CharacterHex;
				}
				else
				{
					if (num2 >= this.ps.charsUsed)
					{
						entityType = XmlTextReaderImpl.EntityType.Skipped;
						return -2;
					}
					num3 = num2;
					res = "Invalid syntax for a decimal numeric entity reference.";
					while (chars[num2] >= '0' && chars[num2] <= '9')
					{
						num = checked(num * 10 + (int)chars[num2] - 48);
						num2++;
					}
					entityType = XmlTextReaderImpl.EntityType.CharacterDec;
				}
			}
			catch (OverflowException innerException)
			{
				this.ps.charPos = num2;
				entityType = XmlTextReaderImpl.EntityType.Skipped;
				this.Throw("Invalid value of a character entity reference.", null, innerException);
			}
			if (chars[num2] != ';' || num3 == num2)
			{
				if (num2 == this.ps.charsUsed)
				{
					return -2;
				}
				this.Throw(num2, res);
			}
			if (num <= 65535)
			{
				char c2 = (char)num;
				if (!this.xmlCharType.IsCharData(c2) && ((this.v1Compat && this.normalize) || (!this.v1Compat && this.checkCharacters)))
				{
					this.Throw((this.ps.chars[startPos + 2] == 'x') ? (startPos + 3) : (startPos + 2), "'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(c2, '\0'));
				}
				if (expand)
				{
					if (internalSubsetBuilder != null)
					{
						internalSubsetBuilder.Append(this.ps.chars, this.ps.charPos, num2 - this.ps.charPos + 1);
					}
					chars[num2] = c2;
				}
				charCount = 1;
				return num2 + 1;
			}
			char c3;
			char c4;
			XmlCharType.SplitSurrogateChar(num, out c3, out c4);
			if (this.normalize && (!XmlCharType.IsHighSurrogate((int)c4) || !XmlCharType.IsLowSurrogate((int)c3)))
			{
				this.Throw((this.ps.chars[startPos + 2] == 'x') ? (startPos + 3) : (startPos + 2), "'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(c4, c3));
			}
			if (expand)
			{
				if (internalSubsetBuilder != null)
				{
					internalSubsetBuilder.Append(this.ps.chars, this.ps.charPos, num2 - this.ps.charPos + 1);
				}
				chars[num2 - 1] = c4;
				chars[num2] = c3;
			}
			charCount = 2;
			return num2 + 1;
		}

		// Token: 0x06000BE0 RID: 3040 RVA: 0x0003AD1C File Offset: 0x00038F1C
		private int ParseNamedCharRef(bool expand, StringBuilder internalSubsetBuilder)
		{
			int num2;
			int num;
			for (;;)
			{
				num = (num2 = this.ParseNamedCharRefInline(this.ps.charPos, expand, internalSubsetBuilder));
				if (num2 != -2)
				{
					break;
				}
				if (this.ReadData() == 0)
				{
					return -1;
				}
			}
			if (num2 == -1)
			{
				return -1;
			}
			if (expand)
			{
				this.ps.charPos = num - 1;
			}
			return num;
		}

		// Token: 0x06000BE1 RID: 3041 RVA: 0x0003AD68 File Offset: 0x00038F68
		private int ParseNamedCharRefInline(int startPos, bool expand, StringBuilder internalSubsetBuilder)
		{
			int num = startPos + 1;
			char[] chars = this.ps.chars;
			char c = chars[num];
			if (c <= 'g')
			{
				if (c != 'a')
				{
					if (c == 'g')
					{
						if (this.ps.charsUsed - num < 3)
						{
							return -2;
						}
						if (chars[num + 1] == 't' && chars[num + 2] == ';')
						{
							num += 3;
							char c2 = '>';
							goto IL_175;
						}
						return -1;
					}
				}
				else
				{
					num++;
					if (chars[num] == 'm')
					{
						if (this.ps.charsUsed - num < 3)
						{
							return -2;
						}
						if (chars[num + 1] == 'p' && chars[num + 2] == ';')
						{
							num += 3;
							char c2 = '&';
							goto IL_175;
						}
						return -1;
					}
					else if (chars[num] == 'p')
					{
						if (this.ps.charsUsed - num < 4)
						{
							return -2;
						}
						if (chars[num + 1] == 'o' && chars[num + 2] == 's' && chars[num + 3] == ';')
						{
							num += 4;
							char c2 = '\'';
							goto IL_175;
						}
						return -1;
					}
					else
					{
						if (num < this.ps.charsUsed)
						{
							return -1;
						}
						return -2;
					}
				}
			}
			else if (c != 'l')
			{
				if (c == 'q')
				{
					if (this.ps.charsUsed - num < 5)
					{
						return -2;
					}
					if (chars[num + 1] == 'u' && chars[num + 2] == 'o' && chars[num + 3] == 't' && chars[num + 4] == ';')
					{
						num += 5;
						char c2 = '"';
						goto IL_175;
					}
					return -1;
				}
			}
			else
			{
				if (this.ps.charsUsed - num < 3)
				{
					return -2;
				}
				if (chars[num + 1] == 't' && chars[num + 2] == ';')
				{
					num += 3;
					char c2 = '<';
					goto IL_175;
				}
				return -1;
			}
			return -1;
			IL_175:
			if (expand)
			{
				if (internalSubsetBuilder != null)
				{
					internalSubsetBuilder.Append(this.ps.chars, this.ps.charPos, num - this.ps.charPos);
				}
				char c2;
				this.ps.chars[num - 1] = c2;
			}
			return num;
		}

		// Token: 0x06000BE2 RID: 3042 RVA: 0x0003AF2C File Offset: 0x0003912C
		private int ParseName()
		{
			int num;
			return this.ParseQName(false, 0, out num);
		}

		// Token: 0x06000BE3 RID: 3043 RVA: 0x0003AF43 File Offset: 0x00039143
		private int ParseQName(out int colonPos)
		{
			return this.ParseQName(true, 0, out colonPos);
		}

		// Token: 0x06000BE4 RID: 3044 RVA: 0x0003AF50 File Offset: 0x00039150
		private int ParseQName(bool isQName, int startOffset, out int colonPos)
		{
			int num = -1;
			int num2 = this.ps.charPos + startOffset;
			for (;;)
			{
				char[] chars = this.ps.chars;
				if ((this.xmlCharType.charProperties[(int)chars[num2]] & 4) != 0)
				{
					num2++;
				}
				else
				{
					if (num2 + 1 >= this.ps.charsUsed)
					{
						if (this.ReadDataInName(ref num2))
						{
							continue;
						}
						this.Throw(num2, "Unexpected end of file while parsing {0} has occurred.", "Name");
					}
					if (chars[num2] != ':' || this.supportNamespaces)
					{
						this.Throw(num2, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(chars, this.ps.charsUsed, num2));
					}
				}
				for (;;)
				{
					if ((this.xmlCharType.charProperties[(int)chars[num2]] & 8) != 0)
					{
						num2++;
					}
					else if (chars[num2] == ':')
					{
						if (this.supportNamespaces)
						{
							break;
						}
						num = num2 - this.ps.charPos;
						num2++;
					}
					else
					{
						if (num2 != this.ps.charsUsed)
						{
							goto IL_135;
						}
						if (!this.ReadDataInName(ref num2))
						{
							goto IL_124;
						}
						chars = this.ps.chars;
					}
				}
				if (num != -1 || !isQName)
				{
					this.Throw(num2, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
				}
				num = num2 - this.ps.charPos;
				num2++;
			}
			IL_124:
			this.Throw(num2, "Unexpected end of file while parsing {0} has occurred.", "Name");
			IL_135:
			colonPos = ((num == -1) ? -1 : (this.ps.charPos + num));
			return num2;
		}

		// Token: 0x06000BE5 RID: 3045 RVA: 0x0003B0AC File Offset: 0x000392AC
		private bool ReadDataInName(ref int pos)
		{
			int num = pos - this.ps.charPos;
			bool result = this.ReadData() != 0;
			pos = this.ps.charPos + num;
			return result;
		}

		// Token: 0x06000BE6 RID: 3046 RVA: 0x0003B0E0 File Offset: 0x000392E0
		private string ParseEntityName()
		{
			int num;
			try
			{
				num = this.ParseName();
			}
			catch (XmlException)
			{
				this.Throw("An error occurred while parsing EntityName.");
				return null;
			}
			if (this.ps.chars[num] != ';')
			{
				this.Throw("An error occurred while parsing EntityName.");
			}
			string result = this.nameTable.Add(this.ps.chars, this.ps.charPos, num - this.ps.charPos);
			this.ps.charPos = num + 1;
			return result;
		}

		// Token: 0x06000BE7 RID: 3047 RVA: 0x0003B170 File Offset: 0x00039370
		private XmlTextReaderImpl.NodeData AddNode(int nodeIndex, int nodeDepth)
		{
			XmlTextReaderImpl.NodeData nodeData = this.nodes[nodeIndex];
			if (nodeData != null)
			{
				nodeData.depth = nodeDepth;
				return nodeData;
			}
			return this.AllocNode(nodeIndex, nodeDepth);
		}

		// Token: 0x06000BE8 RID: 3048 RVA: 0x0003B19C File Offset: 0x0003939C
		private XmlTextReaderImpl.NodeData AllocNode(int nodeIndex, int nodeDepth)
		{
			if (nodeIndex >= this.nodes.Length - 1)
			{
				XmlTextReaderImpl.NodeData[] destinationArray = new XmlTextReaderImpl.NodeData[this.nodes.Length * 2];
				Array.Copy(this.nodes, 0, destinationArray, 0, this.nodes.Length);
				this.nodes = destinationArray;
			}
			XmlTextReaderImpl.NodeData nodeData = this.nodes[nodeIndex];
			if (nodeData == null)
			{
				nodeData = new XmlTextReaderImpl.NodeData();
				this.nodes[nodeIndex] = nodeData;
			}
			nodeData.depth = nodeDepth;
			return nodeData;
		}

		// Token: 0x06000BE9 RID: 3049 RVA: 0x0003B206 File Offset: 0x00039406
		private XmlTextReaderImpl.NodeData AddAttributeNoChecks(string name, int attrDepth)
		{
			XmlTextReaderImpl.NodeData nodeData = this.AddNode(this.index + this.attrCount + 1, attrDepth);
			nodeData.SetNamedNode(XmlNodeType.Attribute, this.nameTable.Add(name));
			this.attrCount++;
			return nodeData;
		}

		// Token: 0x06000BEA RID: 3050 RVA: 0x0003B240 File Offset: 0x00039440
		private XmlTextReaderImpl.NodeData AddAttribute(int endNamePos, int colonPos)
		{
			if (colonPos == -1 || !this.supportNamespaces)
			{
				string text = this.nameTable.Add(this.ps.chars, this.ps.charPos, endNamePos - this.ps.charPos);
				return this.AddAttribute(text, string.Empty, text);
			}
			this.attrNeedNamespaceLookup = true;
			int charPos = this.ps.charPos;
			int num = colonPos - charPos;
			if (num == this.lastPrefix.Length && XmlConvert.StrEqual(this.ps.chars, charPos, num, this.lastPrefix))
			{
				return this.AddAttribute(this.nameTable.Add(this.ps.chars, colonPos + 1, endNamePos - colonPos - 1), this.lastPrefix, null);
			}
			string prefix = this.nameTable.Add(this.ps.chars, charPos, num);
			this.lastPrefix = prefix;
			return this.AddAttribute(this.nameTable.Add(this.ps.chars, colonPos + 1, endNamePos - colonPos - 1), prefix, null);
		}

		// Token: 0x06000BEB RID: 3051 RVA: 0x0003B348 File Offset: 0x00039548
		private XmlTextReaderImpl.NodeData AddAttribute(string localName, string prefix, string nameWPrefix)
		{
			XmlTextReaderImpl.NodeData nodeData = this.AddNode(this.index + this.attrCount + 1, this.index + 1);
			nodeData.SetNamedNode(XmlNodeType.Attribute, localName, prefix, nameWPrefix);
			int num = 1 << (int)localName[0];
			if ((this.attrHashtable & num) == 0)
			{
				this.attrHashtable |= num;
			}
			else if (this.attrDuplWalkCount < 250)
			{
				this.attrDuplWalkCount++;
				for (int i = this.index + 1; i < this.index + this.attrCount + 1; i++)
				{
					if (Ref.Equal(this.nodes[i].localName, nodeData.localName))
					{
						this.attrDuplWalkCount = 250;
						break;
					}
				}
			}
			this.attrCount++;
			return nodeData;
		}

		// Token: 0x06000BEC RID: 3052 RVA: 0x0003B419 File Offset: 0x00039619
		private void PopElementContext()
		{
			this.namespaceManager.PopScope();
			if (this.curNode.xmlContextPushed)
			{
				this.PopXmlContext();
			}
		}

		// Token: 0x06000BED RID: 3053 RVA: 0x0003B43A File Offset: 0x0003963A
		private void OnNewLine(int pos)
		{
			this.ps.lineNo = this.ps.lineNo + 1;
			this.ps.lineStartPos = pos - 1;
		}

		// Token: 0x06000BEE RID: 3054 RVA: 0x0003B45C File Offset: 0x0003965C
		private void OnEof()
		{
			this.curNode = this.nodes[0];
			this.curNode.Clear(XmlNodeType.None);
			this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.Eof;
			this.readState = ReadState.EndOfFile;
			this.reportedEncoding = null;
		}

		// Token: 0x06000BEF RID: 3055 RVA: 0x0003B4BC File Offset: 0x000396BC
		private string LookupNamespace(XmlTextReaderImpl.NodeData node)
		{
			string text = this.namespaceManager.LookupNamespace(node.prefix);
			if (text != null)
			{
				return text;
			}
			this.Throw("'{0}' is an undeclared prefix.", node.prefix, node.LineNo, node.LinePos);
			return null;
		}

		// Token: 0x06000BF0 RID: 3056 RVA: 0x0003B500 File Offset: 0x00039700
		private void AddNamespace(string prefix, string uri, XmlTextReaderImpl.NodeData attr)
		{
			if (uri == "http://www.w3.org/2000/xmlns/")
			{
				if (Ref.Equal(prefix, this.XmlNs))
				{
					this.Throw("Prefix \"xmlns\" is reserved for use by XML.", attr.lineInfo2.lineNo, attr.lineInfo2.linePos);
				}
				else
				{
					this.Throw("Prefix '{0}' cannot be mapped to namespace name reserved for \"xml\" or \"xmlns\".", prefix, attr.lineInfo2.lineNo, attr.lineInfo2.linePos);
				}
			}
			else if (uri == "http://www.w3.org/XML/1998/namespace" && !Ref.Equal(prefix, this.Xml) && !this.v1Compat)
			{
				this.Throw("Prefix '{0}' cannot be mapped to namespace name reserved for \"xml\" or \"xmlns\".", prefix, attr.lineInfo2.lineNo, attr.lineInfo2.linePos);
			}
			if (uri.Length == 0 && prefix.Length > 0)
			{
				this.Throw("Invalid namespace declaration.", attr.lineInfo.lineNo, attr.lineInfo.linePos);
			}
			try
			{
				this.namespaceManager.AddNamespace(prefix, uri);
			}
			catch (ArgumentException e)
			{
				this.ReThrow(e, attr.lineInfo.lineNo, attr.lineInfo.linePos);
			}
		}

		// Token: 0x06000BF1 RID: 3057 RVA: 0x0003B628 File Offset: 0x00039828
		private void ResetAttributes()
		{
			if (this.fullAttrCleanup)
			{
				this.FullAttributeCleanup();
			}
			this.curAttrIndex = -1;
			this.attrCount = 0;
			this.attrHashtable = 0;
			this.attrDuplWalkCount = 0;
		}

		// Token: 0x06000BF2 RID: 3058 RVA: 0x0003B654 File Offset: 0x00039854
		private void FullAttributeCleanup()
		{
			for (int i = this.index + 1; i < this.index + this.attrCount + 1; i++)
			{
				XmlTextReaderImpl.NodeData nodeData = this.nodes[i];
				nodeData.nextAttrValueChunk = null;
				nodeData.IsDefaultAttribute = false;
			}
			this.fullAttrCleanup = false;
		}

		// Token: 0x06000BF3 RID: 3059 RVA: 0x0003B69E File Offset: 0x0003989E
		private void PushXmlContext()
		{
			this.xmlContext = new XmlTextReaderImpl.XmlContext(this.xmlContext);
			this.curNode.xmlContextPushed = true;
		}

		// Token: 0x06000BF4 RID: 3060 RVA: 0x0003B6BD File Offset: 0x000398BD
		private void PopXmlContext()
		{
			this.xmlContext = this.xmlContext.previousContext;
			this.curNode.xmlContextPushed = false;
		}

		// Token: 0x06000BF5 RID: 3061 RVA: 0x0003B6DC File Offset: 0x000398DC
		private XmlNodeType GetWhitespaceType()
		{
			if (this.whitespaceHandling != WhitespaceHandling.None)
			{
				if (this.xmlContext.xmlSpace == XmlSpace.Preserve)
				{
					return XmlNodeType.SignificantWhitespace;
				}
				if (this.whitespaceHandling == WhitespaceHandling.All)
				{
					return XmlNodeType.Whitespace;
				}
			}
			return XmlNodeType.None;
		}

		// Token: 0x06000BF6 RID: 3062 RVA: 0x0003B704 File Offset: 0x00039904
		private XmlNodeType GetTextNodeType(int orChars)
		{
			if (orChars > 32)
			{
				return XmlNodeType.Text;
			}
			return this.GetWhitespaceType();
		}

		// Token: 0x06000BF7 RID: 3063 RVA: 0x0003B714 File Offset: 0x00039914
		private void PushExternalEntityOrSubset(string publicId, string systemId, Uri baseUri, string entityName)
		{
			Uri uri;
			if (!string.IsNullOrEmpty(publicId))
			{
				try
				{
					uri = this.xmlResolver.ResolveUri(baseUri, publicId);
					if (this.OpenAndPush(uri))
					{
						return;
					}
				}
				catch (Exception)
				{
				}
			}
			uri = this.xmlResolver.ResolveUri(baseUri, systemId);
			try
			{
				if (this.OpenAndPush(uri))
				{
					return;
				}
			}
			catch (Exception ex)
			{
				if (this.v1Compat)
				{
					throw;
				}
				string message = ex.Message;
				this.Throw(new XmlException((entityName == null) ? "An error has occurred while opening external DTD '{0}': {1}" : "An error has occurred while opening external entity '{0}': {1}", new string[]
				{
					uri.ToString(),
					message
				}, ex, 0, 0));
			}
			if (entityName == null)
			{
				this.ThrowWithoutLineInfo("Cannot resolve external DTD subset - public ID = '{0}', system ID = '{1}'.", new string[]
				{
					(publicId != null) ? publicId : string.Empty,
					systemId
				}, null);
				return;
			}
			this.Throw((this.dtdProcessing == DtdProcessing.Ignore) ? "Cannot resolve entity reference '{0}' because the DTD has been ignored. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method." : "Cannot resolve entity reference '{0}'.", entityName);
		}

		// Token: 0x06000BF8 RID: 3064 RVA: 0x0003B810 File Offset: 0x00039A10
		private bool OpenAndPush(Uri uri)
		{
			if (this.xmlResolver.SupportsType(uri, typeof(TextReader)))
			{
				TextReader textReader = (TextReader)this.xmlResolver.GetEntity(uri, null, typeof(TextReader));
				if (textReader == null)
				{
					return false;
				}
				this.PushParsingState();
				this.InitTextReaderInput(uri.ToString(), uri, textReader);
			}
			else
			{
				Stream stream = (Stream)this.xmlResolver.GetEntity(uri, null, typeof(Stream));
				if (stream == null)
				{
					return false;
				}
				this.PushParsingState();
				this.InitStreamInput(uri, stream, null);
			}
			return true;
		}

		// Token: 0x06000BF9 RID: 3065 RVA: 0x0003B8A0 File Offset: 0x00039AA0
		private bool PushExternalEntity(IDtdEntityInfo entity)
		{
			if (!this.IsResolverNull)
			{
				Uri baseUri = null;
				if (!string.IsNullOrEmpty(entity.BaseUriString))
				{
					baseUri = this.xmlResolver.ResolveUri(null, entity.BaseUriString);
				}
				this.PushExternalEntityOrSubset(entity.PublicId, entity.SystemId, baseUri, entity.Name);
				this.RegisterEntity(entity);
				int charPos = this.ps.charPos;
				if (this.v1Compat)
				{
					this.EatWhitespaces(null);
				}
				if (!this.ParseXmlDeclaration(true))
				{
					this.ps.charPos = charPos;
				}
				return true;
			}
			Encoding encoding = this.ps.encoding;
			this.PushParsingState();
			this.InitStringInput(entity.SystemId, encoding, string.Empty);
			this.RegisterEntity(entity);
			this.RegisterConsumedCharacters(0L, true);
			return false;
		}

		// Token: 0x06000BFA RID: 3066 RVA: 0x0003B960 File Offset: 0x00039B60
		private void PushInternalEntity(IDtdEntityInfo entity)
		{
			Encoding encoding = this.ps.encoding;
			this.PushParsingState();
			this.InitStringInput((entity.DeclaredUriString != null) ? entity.DeclaredUriString : string.Empty, encoding, entity.Text ?? string.Empty);
			this.RegisterEntity(entity);
			this.ps.lineNo = entity.LineNumber;
			this.ps.lineStartPos = -entity.LinePosition - 1;
			this.ps.eolNormalized = true;
			this.RegisterConsumedCharacters((long)entity.Text.Length, true);
		}

		// Token: 0x06000BFB RID: 3067 RVA: 0x0003B9F8 File Offset: 0x00039BF8
		private void PopEntity()
		{
			if (this.ps.stream != null)
			{
				this.ps.stream.Close();
			}
			this.UnregisterEntity();
			this.PopParsingState();
			this.curNode.entityId = this.ps.entityId;
		}

		// Token: 0x06000BFC RID: 3068 RVA: 0x0003BA44 File Offset: 0x00039C44
		private void RegisterEntity(IDtdEntityInfo entity)
		{
			if (this.currentEntities != null && this.currentEntities.ContainsKey(entity))
			{
				this.Throw(entity.IsParameterEntity ? "Parameter entity '{0}' references itself." : "General entity '{0}' references itself.", entity.Name, this.parsingStatesStack[this.parsingStatesStackTop].LineNo, this.parsingStatesStack[this.parsingStatesStackTop].LinePos);
			}
			this.ps.entity = entity;
			int num = this.nextEntityId;
			this.nextEntityId = num + 1;
			this.ps.entityId = num;
			if (entity != null)
			{
				if (this.currentEntities == null)
				{
					this.currentEntities = new Dictionary<IDtdEntityInfo, IDtdEntityInfo>();
				}
				this.currentEntities.Add(entity, entity);
			}
		}

		// Token: 0x06000BFD RID: 3069 RVA: 0x0003BAFE File Offset: 0x00039CFE
		private void UnregisterEntity()
		{
			if (this.ps.entity != null)
			{
				this.currentEntities.Remove(this.ps.entity);
			}
		}

		// Token: 0x06000BFE RID: 3070 RVA: 0x0003BB24 File Offset: 0x00039D24
		private void PushParsingState()
		{
			if (this.parsingStatesStack == null)
			{
				this.parsingStatesStack = new XmlTextReaderImpl.ParsingState[2];
			}
			else if (this.parsingStatesStackTop + 1 == this.parsingStatesStack.Length)
			{
				XmlTextReaderImpl.ParsingState[] destinationArray = new XmlTextReaderImpl.ParsingState[this.parsingStatesStack.Length * 2];
				Array.Copy(this.parsingStatesStack, 0, destinationArray, 0, this.parsingStatesStack.Length);
				this.parsingStatesStack = destinationArray;
			}
			this.parsingStatesStackTop++;
			this.parsingStatesStack[this.parsingStatesStackTop] = this.ps;
			this.ps.Clear();
		}

		// Token: 0x06000BFF RID: 3071 RVA: 0x0003BBB8 File Offset: 0x00039DB8
		private void PopParsingState()
		{
			this.ps.Close(true);
			XmlTextReaderImpl.ParsingState[] array = this.parsingStatesStack;
			int num = this.parsingStatesStackTop;
			this.parsingStatesStackTop = num - 1;
			this.ps = array[num];
		}

		// Token: 0x06000C00 RID: 3072 RVA: 0x0003BBF4 File Offset: 0x00039DF4
		private void InitIncrementalRead(IncrementalReadDecoder decoder)
		{
			this.ResetAttributes();
			decoder.Reset();
			this.incReadDecoder = decoder;
			this.incReadState = XmlTextReaderImpl.IncrementalReadState.Text;
			this.incReadDepth = 1;
			this.incReadLeftStartPos = this.ps.charPos;
			this.incReadLeftEndPos = this.ps.charPos;
			this.incReadLineInfo.Set(this.ps.LineNo, this.ps.LinePos);
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.InIncrementalRead;
		}

		// Token: 0x06000C01 RID: 3073 RVA: 0x0003BC70 File Offset: 0x00039E70
		private int IncrementalRead(Array array, int index, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException((this.incReadDecoder is IncrementalReadCharsDecoder) ? "buffer" : "array");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException((this.incReadDecoder is IncrementalReadCharsDecoder) ? "count" : "len");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException((this.incReadDecoder is IncrementalReadCharsDecoder) ? "index" : "offset");
			}
			if (array.Length - index < count)
			{
				throw new ArgumentException((this.incReadDecoder is IncrementalReadCharsDecoder) ? "count" : "len");
			}
			if (count == 0)
			{
				return 0;
			}
			this.curNode.lineInfo = this.incReadLineInfo;
			this.incReadDecoder.SetNextOutputBuffer(array, index, count);
			this.IncrementalRead();
			return this.incReadDecoder.DecodedCount;
		}

		// Token: 0x06000C02 RID: 3074 RVA: 0x0003BD48 File Offset: 0x00039F48
		private int IncrementalRead()
		{
			int num = 0;
			int num3;
			int num4;
			int num5;
			int num7;
			for (;;)
			{
				int num2 = this.incReadLeftEndPos - this.incReadLeftStartPos;
				if (num2 > 0)
				{
					try
					{
						num3 = this.incReadDecoder.Decode(this.ps.chars, this.incReadLeftStartPos, num2);
					}
					catch (XmlException e)
					{
						this.ReThrow(e, this.incReadLineInfo.lineNo, this.incReadLineInfo.linePos);
						return 0;
					}
					if (num3 < num2)
					{
						break;
					}
					this.incReadLeftStartPos = 0;
					this.incReadLeftEndPos = 0;
					this.incReadLineInfo.linePos = this.incReadLineInfo.linePos + num3;
					if (this.incReadDecoder.IsFull)
					{
						return num3;
					}
				}
				num4 = 0;
				num5 = 0;
				int num10;
				for (;;)
				{
					switch (this.incReadState)
					{
					case XmlTextReaderImpl.IncrementalReadState.Text:
					case XmlTextReaderImpl.IncrementalReadState.StartTag:
					case XmlTextReaderImpl.IncrementalReadState.Attributes:
					case XmlTextReaderImpl.IncrementalReadState.AttributeValue:
						goto IL_1D7;
					case XmlTextReaderImpl.IncrementalReadState.PI:
						if (this.ParsePIValue(out num4, out num5))
						{
							this.ps.charPos = this.ps.charPos - 2;
							this.incReadState = XmlTextReaderImpl.IncrementalReadState.Text;
						}
						break;
					case XmlTextReaderImpl.IncrementalReadState.CDATA:
						if (this.ParseCDataOrComment(XmlNodeType.CDATA, out num4, out num5))
						{
							this.ps.charPos = this.ps.charPos - 3;
							this.incReadState = XmlTextReaderImpl.IncrementalReadState.Text;
						}
						break;
					case XmlTextReaderImpl.IncrementalReadState.Comment:
						if (this.ParseCDataOrComment(XmlNodeType.Comment, out num4, out num5))
						{
							this.ps.charPos = this.ps.charPos - 3;
							this.incReadState = XmlTextReaderImpl.IncrementalReadState.Text;
						}
						break;
					case XmlTextReaderImpl.IncrementalReadState.ReadData:
						if (this.ReadData() == 0)
						{
							this.ThrowUnclosedElements();
						}
						this.incReadState = XmlTextReaderImpl.IncrementalReadState.Text;
						num4 = this.ps.charPos;
						num5 = num4;
						goto IL_1D7;
					case XmlTextReaderImpl.IncrementalReadState.EndElement:
						goto IL_17A;
					case XmlTextReaderImpl.IncrementalReadState.End:
						return num;
					default:
						goto IL_1D7;
					}
					IL_6A4:
					int num6 = num5 - num4;
					if (num6 <= 0)
					{
						continue;
					}
					try
					{
						num7 = this.incReadDecoder.Decode(this.ps.chars, num4, num6);
					}
					catch (XmlException e2)
					{
						this.ReThrow(e2, this.incReadLineInfo.lineNo, this.incReadLineInfo.linePos);
						return 0;
					}
					num += num7;
					if (this.incReadDecoder.IsFull)
					{
						goto Block_54;
					}
					continue;
					IL_1D7:
					char[] chars = this.ps.chars;
					num4 = this.ps.charPos;
					num5 = num4;
					int num8;
					for (;;)
					{
						this.incReadLineInfo.Set(this.ps.LineNo, this.ps.LinePos);
						if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.Attributes)
						{
							char c;
							while ((this.xmlCharType.charProperties[(int)(c = chars[num5])] & 128) != 0)
							{
								if (c == '/')
								{
									break;
								}
								num5++;
							}
						}
						else
						{
							while ((this.xmlCharType.charProperties[(int)chars[num5]] & 128) != 0)
							{
								num5++;
							}
						}
						if (chars[num5] == '&' || chars[num5] == '\t')
						{
							num5++;
						}
						else
						{
							if (num5 - num4 > 0)
							{
								break;
							}
							char c2 = chars[num5];
							if (c2 <= '"')
							{
								if (c2 == '\n')
								{
									num5++;
									this.OnNewLine(num5);
									continue;
								}
								if (c2 == '\r')
								{
									if (chars[num5 + 1] == '\n')
									{
										num5 += 2;
									}
									else
									{
										if (num5 + 1 >= this.ps.charsUsed)
										{
											goto IL_691;
										}
										num5++;
									}
									this.OnNewLine(num5);
									continue;
								}
								if (c2 != '"')
								{
									goto IL_67A;
								}
							}
							else if (c2 <= '/')
							{
								if (c2 != '\'')
								{
									if (c2 != '/')
									{
										goto IL_67A;
									}
									if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.Attributes)
									{
										if (this.ps.charsUsed - num5 < 2)
										{
											goto IL_691;
										}
										if (chars[num5 + 1] == '>')
										{
											this.incReadState = XmlTextReaderImpl.IncrementalReadState.Text;
											this.incReadDepth--;
										}
									}
									num5++;
									continue;
								}
							}
							else if (c2 != '<')
							{
								if (c2 != '>')
								{
									goto IL_67A;
								}
								if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.Attributes)
								{
									this.incReadState = XmlTextReaderImpl.IncrementalReadState.Text;
								}
								num5++;
								continue;
							}
							else
							{
								if (this.incReadState != XmlTextReaderImpl.IncrementalReadState.Text)
								{
									num5++;
									continue;
								}
								if (this.ps.charsUsed - num5 < 2)
								{
									goto IL_691;
								}
								c2 = chars[num5 + 1];
								if (c2 != '!')
								{
									if (c2 != '/')
									{
										if (c2 == '?')
										{
											goto Block_31;
										}
										int num9;
										num8 = this.ParseQName(true, 1, out num9);
										if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos + 1, num8 - this.ps.charPos - 1, this.curNode.localName) && (this.ps.chars[num8] == '>' || this.ps.chars[num8] == '/' || this.xmlCharType.IsWhiteSpace(this.ps.chars[num8])))
										{
											goto IL_594;
										}
										num5 = num8;
										num4 = this.ps.charPos;
										chars = this.ps.chars;
										continue;
									}
									else
									{
										int num11;
										num10 = this.ParseQName(true, 2, out num11);
										if (!XmlConvert.StrEqual(chars, this.ps.charPos + 2, num10 - this.ps.charPos - 2, this.curNode.GetNameWPrefix(this.nameTable)) || (this.ps.chars[num10] != '>' && !this.xmlCharType.IsWhiteSpace(this.ps.chars[num10])))
										{
											num5 = num10;
											num4 = this.ps.charPos;
											chars = this.ps.chars;
											continue;
										}
										int num12 = this.incReadDepth - 1;
										this.incReadDepth = num12;
										if (num12 > 0)
										{
											num5 = num10 + 1;
											continue;
										}
										goto IL_47C;
									}
								}
								else
								{
									if (this.ps.charsUsed - num5 < 4)
									{
										goto IL_691;
									}
									if (chars[num5 + 2] == '-' && chars[num5 + 3] == '-')
									{
										goto Block_34;
									}
									if (this.ps.charsUsed - num5 < 9)
									{
										goto IL_691;
									}
									if (XmlConvert.StrEqual(chars, num5 + 2, 7, "[CDATA["))
									{
										goto Block_36;
									}
									continue;
								}
							}
							XmlTextReaderImpl.IncrementalReadState incrementalReadState = this.incReadState;
							if (incrementalReadState != XmlTextReaderImpl.IncrementalReadState.Attributes)
							{
								if (incrementalReadState == XmlTextReaderImpl.IncrementalReadState.AttributeValue && chars[num5] == this.curNode.quoteChar)
								{
									this.incReadState = XmlTextReaderImpl.IncrementalReadState.Attributes;
								}
							}
							else
							{
								this.curNode.quoteChar = chars[num5];
								this.incReadState = XmlTextReaderImpl.IncrementalReadState.AttributeValue;
							}
							num5++;
							continue;
							IL_67A:
							if (num5 == this.ps.charsUsed)
							{
								goto IL_691;
							}
							num5++;
						}
					}
					IL_698:
					this.ps.charPos = num5;
					goto IL_6A4;
					IL_691:
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadData;
					goto IL_698;
					IL_594:
					this.incReadDepth++;
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.Attributes;
					num5 = num8;
					goto IL_698;
					Block_36:
					num5 += 9;
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.CDATA;
					goto IL_698;
					Block_34:
					num5 += 4;
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.Comment;
					goto IL_698;
					Block_31:
					num5 += 2;
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.PI;
					goto IL_698;
				}
				IL_47C:
				this.ps.charPos = num10;
				if (this.xmlCharType.IsWhiteSpace(this.ps.chars[num10]))
				{
					this.EatWhitespaces(null);
				}
				if (this.ps.chars[this.ps.charPos] != '>')
				{
					this.ThrowUnexpectedToken(">");
				}
				this.ps.charPos = this.ps.charPos + 1;
				this.incReadState = XmlTextReaderImpl.IncrementalReadState.EndElement;
			}
			this.incReadLeftStartPos += num3;
			this.incReadLineInfo.linePos = this.incReadLineInfo.linePos + num3;
			return num3;
			IL_17A:
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PopElementContext;
			this.nextParsingFunction = ((this.index > 0 || this.fragmentType != XmlNodeType.Document) ? XmlTextReaderImpl.ParsingFunction.ElementContent : XmlTextReaderImpl.ParsingFunction.DocumentContent);
			this.outerReader.Read();
			this.incReadState = XmlTextReaderImpl.IncrementalReadState.End;
			return num;
			Block_54:
			this.incReadLeftStartPos = num4 + num7;
			this.incReadLeftEndPos = num5;
			this.incReadLineInfo.linePos = this.incReadLineInfo.linePos + num7;
			return num;
		}

		// Token: 0x06000C03 RID: 3075 RVA: 0x0003C4A0 File Offset: 0x0003A6A0
		private void FinishIncrementalRead()
		{
			this.incReadDecoder = new IncrementalReadDummyDecoder();
			this.IncrementalRead();
			this.incReadDecoder = null;
		}

		// Token: 0x06000C04 RID: 3076 RVA: 0x0003C4BC File Offset: 0x0003A6BC
		private bool ParseFragmentAttribute()
		{
			if (this.curNode.type == XmlNodeType.None)
			{
				this.curNode.type = XmlNodeType.Attribute;
				this.curAttrIndex = 0;
				this.ParseAttributeValueSlow(this.ps.charPos, ' ', this.curNode);
			}
			else
			{
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.InReadAttributeValue;
			}
			if (this.ReadAttributeValue())
			{
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.FragmentAttribute;
				return true;
			}
			this.OnEof();
			return false;
		}

		// Token: 0x06000C05 RID: 3077 RVA: 0x0003C528 File Offset: 0x0003A728
		private bool ParseAttributeValueChunk()
		{
			char[] chars = this.ps.chars;
			int num = this.ps.charPos;
			this.curNode = this.AddNode(this.index + this.attrCount + 1, this.index + 2);
			this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			if (this.emptyEntityInAttributeResolved)
			{
				this.curNode.SetValueNode(XmlNodeType.Text, string.Empty);
				this.emptyEntityInAttributeResolved = false;
				return true;
			}
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)chars[num]] & 128) == 0)
				{
					char c = chars[num];
					if (c <= '&')
					{
						switch (c)
						{
						case '\t':
						case '\n':
							if (this.normalize)
							{
								chars[num] = ' ';
							}
							num++;
							continue;
						case '\v':
						case '\f':
							goto IL_21F;
						case '\r':
							num++;
							continue;
						default:
							if (c != '"')
							{
								if (c != '&')
								{
									goto IL_21F;
								}
								if (num - this.ps.charPos > 0)
								{
									this.stringBuilder.Append(chars, this.ps.charPos, num - this.ps.charPos);
								}
								this.ps.charPos = num;
								XmlTextReaderImpl.EntityType entityType = this.HandleEntityReference(true, XmlTextReaderImpl.EntityExpandType.OnlyCharacter, out num);
								if (entityType > XmlTextReaderImpl.EntityType.CharacterNamed)
								{
									if (entityType == XmlTextReaderImpl.EntityType.Unexpanded)
									{
										goto IL_1C5;
									}
								}
								else
								{
									chars = this.ps.chars;
									if (this.normalize && this.xmlCharType.IsWhiteSpace(chars[this.ps.charPos]) && num - this.ps.charPos == 1)
									{
										chars[this.ps.charPos] = ' ';
									}
								}
								chars = this.ps.chars;
								continue;
							}
							break;
						}
					}
					else if (c != '\'')
					{
						if (c == '<')
						{
							this.Throw(num, "'{0}', hexadecimal value {1}, is an invalid attribute character.", XmlException.BuildCharExceptionArgs('<', '\0'));
							goto IL_271;
						}
						if (c != '>')
						{
							goto IL_21F;
						}
					}
					num++;
					continue;
					IL_21F:
					if (num != this.ps.charsUsed)
					{
						if (XmlCharType.IsHighSurrogate((int)chars[num]))
						{
							if (num + 1 == this.ps.charsUsed)
							{
								goto IL_271;
							}
							num++;
							if (XmlCharType.IsLowSurrogate((int)chars[num]))
							{
								num++;
								continue;
							}
						}
						this.ThrowInvalidChar(chars, this.ps.charsUsed, num);
					}
					IL_271:
					if (num - this.ps.charPos > 0)
					{
						this.stringBuilder.Append(chars, this.ps.charPos, num - this.ps.charPos);
						this.ps.charPos = num;
					}
					if (this.ReadData() == 0)
					{
						if (this.stringBuilder.Length > 0)
						{
							goto IL_2F6;
						}
						if (this.HandleEntityEnd(false))
						{
							goto Block_25;
						}
					}
					num = this.ps.charPos;
					chars = this.ps.chars;
				}
				else
				{
					num++;
				}
			}
			IL_1C5:
			if (this.stringBuilder.Length == 0)
			{
				XmlTextReaderImpl.NodeData nodeData = this.curNode;
				nodeData.lineInfo.linePos = nodeData.lineInfo.linePos + 1;
				this.ps.charPos = this.ps.charPos + 1;
				this.curNode.SetNamedNode(XmlNodeType.EntityReference, this.ParseEntityName());
				return true;
			}
			goto IL_2F6;
			Block_25:
			this.SetupEndEntityNodeInAttribute();
			return true;
			IL_2F6:
			if (num - this.ps.charPos > 0)
			{
				this.stringBuilder.Append(chars, this.ps.charPos, num - this.ps.charPos);
				this.ps.charPos = num;
			}
			this.curNode.SetValueNode(XmlNodeType.Text, this.stringBuilder.ToString());
			this.stringBuilder.Length = 0;
			return true;
		}

		// Token: 0x06000C06 RID: 3078 RVA: 0x0003C890 File Offset: 0x0003AA90
		private void ParseXmlDeclarationFragment()
		{
			try
			{
				this.ParseXmlDeclaration(false);
			}
			catch (XmlException ex)
			{
				this.ReThrow(ex, ex.LineNumber, ex.LinePosition - 6);
			}
		}

		// Token: 0x06000C07 RID: 3079 RVA: 0x0003C8D0 File Offset: 0x0003AAD0
		private void ThrowUnexpectedToken(int pos, string expectedToken)
		{
			this.ThrowUnexpectedToken(pos, expectedToken, null);
		}

		// Token: 0x06000C08 RID: 3080 RVA: 0x0003C8DB File Offset: 0x0003AADB
		private void ThrowUnexpectedToken(string expectedToken1)
		{
			this.ThrowUnexpectedToken(expectedToken1, null);
		}

		// Token: 0x06000C09 RID: 3081 RVA: 0x0003C8E5 File Offset: 0x0003AAE5
		private void ThrowUnexpectedToken(int pos, string expectedToken1, string expectedToken2)
		{
			this.ps.charPos = pos;
			this.ThrowUnexpectedToken(expectedToken1, expectedToken2);
		}

		// Token: 0x06000C0A RID: 3082 RVA: 0x0003C8FC File Offset: 0x0003AAFC
		private void ThrowUnexpectedToken(string expectedToken1, string expectedToken2)
		{
			string text = this.ParseUnexpectedToken();
			if (text == null)
			{
				this.Throw("Unexpected end of file has occurred.");
			}
			if (expectedToken2 != null)
			{
				this.Throw("'{0}' is an unexpected token. The expected token is '{1}' or '{2}'.", new string[]
				{
					text,
					expectedToken1,
					expectedToken2
				});
				return;
			}
			this.Throw("'{0}' is an unexpected token. The expected token is '{1}'.", new string[]
			{
				text,
				expectedToken1
			});
		}

		// Token: 0x06000C0B RID: 3083 RVA: 0x0003C958 File Offset: 0x0003AB58
		private string ParseUnexpectedToken(int pos)
		{
			this.ps.charPos = pos;
			return this.ParseUnexpectedToken();
		}

		// Token: 0x06000C0C RID: 3084 RVA: 0x0003C96C File Offset: 0x0003AB6C
		private string ParseUnexpectedToken()
		{
			if (this.ps.charPos == this.ps.charsUsed)
			{
				return null;
			}
			if (this.xmlCharType.IsNCNameSingleChar(this.ps.chars[this.ps.charPos]))
			{
				int num = this.ps.charPos + 1;
				while (this.xmlCharType.IsNCNameSingleChar(this.ps.chars[num]))
				{
					num++;
				}
				return new string(this.ps.chars, this.ps.charPos, num - this.ps.charPos);
			}
			return new string(this.ps.chars, this.ps.charPos, 1);
		}

		// Token: 0x06000C0D RID: 3085 RVA: 0x0003CA2C File Offset: 0x0003AC2C
		private void ThrowExpectingWhitespace(int pos)
		{
			string text = this.ParseUnexpectedToken(pos);
			if (text == null)
			{
				this.Throw(pos, "Unexpected end of file has occurred.");
				return;
			}
			this.Throw(pos, "'{0}' is an unexpected token. Expecting white space.", text);
		}

		// Token: 0x06000C0E RID: 3086 RVA: 0x0003CA60 File Offset: 0x0003AC60
		private int GetIndexOfAttributeWithoutPrefix(string name)
		{
			name = this.nameTable.Get(name);
			if (name == null)
			{
				return -1;
			}
			for (int i = this.index + 1; i < this.index + this.attrCount + 1; i++)
			{
				if (Ref.Equal(this.nodes[i].localName, name) && this.nodes[i].prefix.Length == 0)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000C0F RID: 3087 RVA: 0x0003CAD0 File Offset: 0x0003ACD0
		private int GetIndexOfAttributeWithPrefix(string name)
		{
			name = this.nameTable.Add(name);
			if (name == null)
			{
				return -1;
			}
			for (int i = this.index + 1; i < this.index + this.attrCount + 1; i++)
			{
				if (Ref.Equal(this.nodes[i].GetNameWPrefix(this.nameTable), name))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000C10 RID: 3088 RVA: 0x0003CB30 File Offset: 0x0003AD30
		private bool ZeroEndingStream(int pos)
		{
			if (this.v1Compat && pos == this.ps.charsUsed - 1 && this.ps.chars[pos] == '\0' && this.ReadData() == 0 && this.ps.isStreamEof)
			{
				this.ps.charsUsed = this.ps.charsUsed - 1;
				return true;
			}
			return false;
		}

		// Token: 0x06000C11 RID: 3089 RVA: 0x0003CB8C File Offset: 0x0003AD8C
		private void ParseDtdFromParserContext()
		{
			IDtdParser dtdParser = DtdParser.Create();
			this.dtdInfo = dtdParser.ParseFreeFloatingDtd(this.fragmentParserContext.BaseURI, this.fragmentParserContext.DocTypeName, this.fragmentParserContext.PublicId, this.fragmentParserContext.SystemId, this.fragmentParserContext.InternalSubset, new XmlTextReaderImpl.DtdParserProxy(this));
			if ((this.validatingReaderCompatFlag || !this.v1Compat) && (this.dtdInfo.HasDefaultAttributes || this.dtdInfo.HasNonCDataAttributes))
			{
				this.addDefaultAttributesAndNormalize = true;
			}
		}

		// Token: 0x06000C12 RID: 3090 RVA: 0x0003CC1C File Offset: 0x0003AE1C
		private bool InitReadContentAsBinary()
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadValueChunk)
			{
				throw new InvalidOperationException(Res.GetString("ReadValueChunk calls cannot be mixed with ReadContentAsBase64 or ReadContentAsBinHex."));
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
			{
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadChars, ReadBase64, and ReadBinHex."));
			}
			if (!XmlReader.IsTextualNode(this.curNode.type) && !this.MoveToNextContentNode(false))
			{
				return false;
			}
			this.SetupReadContentAsBinaryState(XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary);
			this.incReadLineInfo.Set(this.curNode.LineNo, this.curNode.LinePos);
			return true;
		}

		// Token: 0x06000C13 RID: 3091 RVA: 0x0003CCA4 File Offset: 0x0003AEA4
		private bool InitReadElementContentAsBinary()
		{
			bool isEmptyElement = this.curNode.IsEmptyElement;
			this.outerReader.Read();
			if (isEmptyElement)
			{
				return false;
			}
			if (!this.MoveToNextContentNode(false))
			{
				if (this.curNode.type != XmlNodeType.EndElement)
				{
					this.Throw("'{0}' is an invalid XmlNodeType.", this.curNode.type.ToString());
				}
				this.outerReader.Read();
				return false;
			}
			this.SetupReadContentAsBinaryState(XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary);
			this.incReadLineInfo.Set(this.curNode.LineNo, this.curNode.LinePos);
			return true;
		}

		// Token: 0x06000C14 RID: 3092 RVA: 0x0003CD40 File Offset: 0x0003AF40
		private bool MoveToNextContentNode(bool moveIfOnContentNode)
		{
			for (;;)
			{
				switch (this.curNode.type)
				{
				case XmlNodeType.Attribute:
					goto IL_52;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					if (!moveIfOnContentNode)
					{
						return true;
					}
					goto IL_6B;
				case XmlNodeType.EntityReference:
					this.outerReader.ResolveEntity();
					goto IL_6B;
				case XmlNodeType.ProcessingInstruction:
				case XmlNodeType.Comment:
				case XmlNodeType.EndEntity:
					goto IL_6B;
				}
				break;
				IL_6B:
				moveIfOnContentNode = false;
				if (!this.outerReader.Read())
				{
					return false;
				}
			}
			return false;
			IL_52:
			return !moveIfOnContentNode;
		}

		// Token: 0x06000C15 RID: 3093 RVA: 0x0003CDCC File Offset: 0x0003AFCC
		private void SetupReadContentAsBinaryState(XmlTextReaderImpl.ParsingFunction inReadBinaryFunction)
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.PartialTextValue)
			{
				this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue;
			}
			else
			{
				this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue;
				this.nextNextParsingFunction = this.nextParsingFunction;
				this.nextParsingFunction = this.parsingFunction;
			}
			this.readValueOffset = 0;
			this.parsingFunction = inReadBinaryFunction;
		}

		// Token: 0x06000C16 RID: 3094 RVA: 0x0003CE1C File Offset: 0x0003B01C
		private void SetupFromParserContext(XmlParserContext context, XmlReaderSettings settings)
		{
			XmlNameTable xmlNameTable = settings.NameTable;
			this.nameTableFromSettings = (xmlNameTable != null);
			if (context.NamespaceManager != null)
			{
				if (xmlNameTable != null && xmlNameTable != context.NamespaceManager.NameTable)
				{
					throw new XmlException("XmlReaderSettings.XmlNameTable must be the same name table as in XmlParserContext.NameTable or XmlParserContext.NamespaceManager.NameTable, or it must be null.");
				}
				this.namespaceManager = context.NamespaceManager;
				this.xmlContext.defaultNamespace = this.namespaceManager.LookupNamespace(string.Empty);
				xmlNameTable = this.namespaceManager.NameTable;
			}
			else if (context.NameTable != null)
			{
				if (xmlNameTable != null && xmlNameTable != context.NameTable)
				{
					throw new XmlException("XmlReaderSettings.XmlNameTable must be the same name table as in XmlParserContext.NameTable or XmlParserContext.NamespaceManager.NameTable, or it must be null.", string.Empty);
				}
				xmlNameTable = context.NameTable;
			}
			else if (xmlNameTable == null)
			{
				xmlNameTable = new NameTable();
			}
			this.nameTable = xmlNameTable;
			if (this.namespaceManager == null)
			{
				this.namespaceManager = new XmlNamespaceManager(xmlNameTable);
			}
			this.xmlContext.xmlSpace = context.XmlSpace;
			this.xmlContext.xmlLang = context.XmlLang;
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x06000C17 RID: 3095 RVA: 0x0003CF06 File Offset: 0x0003B106
		internal override IDtdInfo DtdInfo
		{
			get
			{
				return this.dtdInfo;
			}
		}

		// Token: 0x06000C18 RID: 3096 RVA: 0x0003CF10 File Offset: 0x0003B110
		internal void SetDtdInfo(IDtdInfo newDtdInfo)
		{
			this.dtdInfo = newDtdInfo;
			if (this.dtdInfo != null && (this.validatingReaderCompatFlag || !this.v1Compat) && (this.dtdInfo.HasDefaultAttributes || this.dtdInfo.HasNonCDataAttributes))
			{
				this.addDefaultAttributesAndNormalize = true;
			}
		}

		// Token: 0x1700022F RID: 559
		// (set) Token: 0x06000C19 RID: 3097 RVA: 0x000346C1 File Offset: 0x000328C1
		internal IValidationEventHandling ValidationEventHandling
		{
			set
			{
				this.validationEventHandling = value;
			}
		}

		// Token: 0x17000230 RID: 560
		// (set) Token: 0x06000C1A RID: 3098 RVA: 0x0003CF5D File Offset: 0x0003B15D
		internal XmlTextReaderImpl.OnDefaultAttributeUseDelegate OnDefaultAttributeUse
		{
			set
			{
				this.onDefaultAttributeUse = value;
			}
		}

		// Token: 0x17000231 RID: 561
		// (set) Token: 0x06000C1B RID: 3099 RVA: 0x0003CF66 File Offset: 0x0003B166
		internal bool XmlValidatingReaderCompatibilityMode
		{
			set
			{
				this.validatingReaderCompatFlag = value;
				if (value)
				{
					this.nameTable.Add("http://www.w3.org/2001/XMLSchema");
					this.nameTable.Add("http://www.w3.org/2001/XMLSchema-instance");
					this.nameTable.Add("urn:schemas-microsoft-com:datatypes");
				}
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x06000C1C RID: 3100 RVA: 0x0003CFA5 File Offset: 0x0003B1A5
		internal XmlNodeType FragmentType
		{
			get
			{
				return this.fragmentType;
			}
		}

		// Token: 0x06000C1D RID: 3101 RVA: 0x0003CFAD File Offset: 0x0003B1AD
		internal void ChangeCurrentNodeType(XmlNodeType newNodeType)
		{
			this.curNode.type = newNodeType;
		}

		// Token: 0x06000C1E RID: 3102 RVA: 0x0003CFBB File Offset: 0x0003B1BB
		internal XmlResolver GetResolver()
		{
			if (this.IsResolverNull)
			{
				return null;
			}
			return this.xmlResolver;
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06000C1F RID: 3103 RVA: 0x0003CFCD File Offset: 0x0003B1CD
		// (set) Token: 0x06000C20 RID: 3104 RVA: 0x0003CFDA File Offset: 0x0003B1DA
		internal object InternalSchemaType
		{
			get
			{
				return this.curNode.schemaType;
			}
			set
			{
				this.curNode.schemaType = value;
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06000C21 RID: 3105 RVA: 0x0003CFE8 File Offset: 0x0003B1E8
		// (set) Token: 0x06000C22 RID: 3106 RVA: 0x0003CFF5 File Offset: 0x0003B1F5
		internal object InternalTypedValue
		{
			get
			{
				return this.curNode.typedValue;
			}
			set
			{
				this.curNode.typedValue = value;
			}
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000C23 RID: 3107 RVA: 0x0003D003 File Offset: 0x0003B203
		internal bool StandAlone
		{
			get
			{
				return this.standalone;
			}
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06000C24 RID: 3108 RVA: 0x000345E0 File Offset: 0x000327E0
		internal override XmlNamespaceManager NamespaceManager
		{
			get
			{
				return this.namespaceManager;
			}
		}

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06000C25 RID: 3109 RVA: 0x000345F0 File Offset: 0x000327F0
		internal bool V1Compat
		{
			get
			{
				return this.v1Compat;
			}
		}

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06000C26 RID: 3110 RVA: 0x0003D00B File Offset: 0x0003B20B
		internal ConformanceLevel V1ComformanceLevel
		{
			get
			{
				if (this.fragmentType != XmlNodeType.Element)
				{
					return ConformanceLevel.Document;
				}
				return ConformanceLevel.Fragment;
			}
		}

		// Token: 0x06000C27 RID: 3111 RVA: 0x0003D01C File Offset: 0x0003B21C
		private bool AddDefaultAttributeDtd(IDtdDefaultAttributeInfo defAttrInfo, bool definedInDtd, XmlTextReaderImpl.NodeData[] nameSortedNodeData)
		{
			if (defAttrInfo.Prefix.Length > 0)
			{
				this.attrNeedNamespaceLookup = true;
			}
			string localName = defAttrInfo.LocalName;
			string prefix = defAttrInfo.Prefix;
			if (nameSortedNodeData != null)
			{
				if (Array.BinarySearch<object>(nameSortedNodeData, defAttrInfo, XmlTextReaderImpl.DtdDefaultAttributeInfoToNodeDataComparer.Instance) >= 0)
				{
					return false;
				}
			}
			else
			{
				for (int i = this.index + 1; i < this.index + 1 + this.attrCount; i++)
				{
					if (this.nodes[i].localName == localName && this.nodes[i].prefix == prefix)
					{
						return false;
					}
				}
			}
			XmlTextReaderImpl.NodeData nodeData = this.AddDefaultAttributeInternal(defAttrInfo.LocalName, null, defAttrInfo.Prefix, defAttrInfo.DefaultValueExpanded, defAttrInfo.LineNumber, defAttrInfo.LinePosition, defAttrInfo.ValueLineNumber, defAttrInfo.ValueLinePosition, defAttrInfo.IsXmlAttribute);
			if (this.DtdValidation)
			{
				if (this.onDefaultAttributeUse != null)
				{
					this.onDefaultAttributeUse(defAttrInfo, this);
				}
				nodeData.typedValue = defAttrInfo.DefaultValueTyped;
			}
			return nodeData != null;
		}

		// Token: 0x06000C28 RID: 3112 RVA: 0x0003D108 File Offset: 0x0003B308
		internal bool AddDefaultAttributeNonDtd(SchemaAttDef attrDef)
		{
			string text = this.nameTable.Add(attrDef.Name.Name);
			string text2 = this.nameTable.Add(attrDef.Prefix);
			string text3 = this.nameTable.Add(attrDef.Name.Namespace);
			if (text2.Length == 0 && text3.Length > 0)
			{
				text2 = this.namespaceManager.LookupPrefix(text3);
				if (text2 == null)
				{
					text2 = string.Empty;
				}
			}
			for (int i = this.index + 1; i < this.index + 1 + this.attrCount; i++)
			{
				if (this.nodes[i].localName == text && (this.nodes[i].prefix == text2 || (this.nodes[i].ns == text3 && text3 != null)))
				{
					return false;
				}
			}
			XmlTextReaderImpl.NodeData nodeData = this.AddDefaultAttributeInternal(text, text3, text2, attrDef.DefaultValueExpanded, attrDef.LineNumber, attrDef.LinePosition, attrDef.ValueLineNumber, attrDef.ValueLinePosition, attrDef.Reserved > SchemaAttDef.Reserve.None);
			nodeData.schemaType = ((attrDef.SchemaType == null) ? attrDef.Datatype : attrDef.SchemaType);
			nodeData.typedValue = attrDef.DefaultValueTyped;
			return true;
		}

		// Token: 0x06000C29 RID: 3113 RVA: 0x0003D22C File Offset: 0x0003B42C
		private XmlTextReaderImpl.NodeData AddDefaultAttributeInternal(string localName, string ns, string prefix, string value, int lineNo, int linePos, int valueLineNo, int valueLinePos, bool isXmlAttribute)
		{
			XmlTextReaderImpl.NodeData nodeData = this.AddAttribute(localName, prefix, (prefix.Length > 0) ? null : localName);
			if (ns != null)
			{
				nodeData.ns = ns;
			}
			nodeData.SetValue(value);
			nodeData.IsDefaultAttribute = true;
			nodeData.lineInfo.Set(lineNo, linePos);
			nodeData.lineInfo2.Set(valueLineNo, valueLinePos);
			if (nodeData.prefix.Length == 0)
			{
				if (Ref.Equal(nodeData.localName, this.XmlNs))
				{
					this.OnDefaultNamespaceDecl(nodeData);
					if (!this.attrNeedNamespaceLookup && this.nodes[this.index].prefix.Length == 0)
					{
						this.nodes[this.index].ns = this.xmlContext.defaultNamespace;
					}
				}
			}
			else if (Ref.Equal(nodeData.prefix, this.XmlNs))
			{
				this.OnNamespaceDecl(nodeData);
				if (!this.attrNeedNamespaceLookup)
				{
					string localName2 = nodeData.localName;
					for (int i = this.index; i < this.index + this.attrCount + 1; i++)
					{
						if (this.nodes[i].prefix.Equals(localName2))
						{
							this.nodes[i].ns = this.namespaceManager.LookupNamespace(localName2);
						}
					}
				}
			}
			else if (isXmlAttribute)
			{
				this.OnXmlReservedAttribute(nodeData);
			}
			this.fullAttrCleanup = true;
			return nodeData;
		}

		// Token: 0x17000239 RID: 569
		// (set) Token: 0x06000C2A RID: 3114 RVA: 0x0003D384 File Offset: 0x0003B584
		internal bool DisableUndeclaredEntityCheck
		{
			set
			{
				this.disableUndeclaredEntityCheck = value;
			}
		}

		// Token: 0x06000C2B RID: 3115 RVA: 0x0003D390 File Offset: 0x0003B590
		private int ReadContentAsBinary(byte[] buffer, int index, int count)
		{
			if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End)
			{
				return 0;
			}
			this.incReadDecoder.SetNextOutputBuffer(buffer, index, count);
			int num;
			int num2;
			int num3;
			XmlTextReaderImpl.ParsingFunction inReadBinaryFunction;
			for (;;)
			{
				num = 0;
				try
				{
					num = this.curNode.CopyToBinary(this.incReadDecoder, this.readValueOffset);
				}
				catch (XmlException e)
				{
					this.curNode.AdjustLineInfo(this.readValueOffset, this.ps.eolNormalized, ref this.incReadLineInfo);
					this.ReThrow(e, this.incReadLineInfo.lineNo, this.incReadLineInfo.linePos);
				}
				this.readValueOffset += num;
				if (this.incReadDecoder.IsFull)
				{
					break;
				}
				if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue)
				{
					this.curNode.SetValue(string.Empty);
					bool flag = false;
					num2 = 0;
					num3 = 0;
					while (!this.incReadDecoder.IsFull && !flag)
					{
						int num4 = 0;
						this.incReadLineInfo.Set(this.ps.LineNo, this.ps.LinePos);
						flag = this.ParseText(out num2, out num3, ref num4);
						try
						{
							num = this.incReadDecoder.Decode(this.ps.chars, num2, num3 - num2);
						}
						catch (XmlException e2)
						{
							this.ReThrow(e2, this.incReadLineInfo.lineNo, this.incReadLineInfo.linePos);
						}
						num2 += num;
					}
					this.incReadState = (flag ? XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue : XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue);
					this.readValueOffset = 0;
					if (this.incReadDecoder.IsFull)
					{
						goto Block_8;
					}
				}
				inReadBinaryFunction = this.parsingFunction;
				this.parsingFunction = this.nextParsingFunction;
				this.nextParsingFunction = this.nextNextParsingFunction;
				if (!this.MoveToNextContentNode(true))
				{
					goto Block_9;
				}
				this.SetupReadContentAsBinaryState(inReadBinaryFunction);
				this.incReadLineInfo.Set(this.curNode.LineNo, this.curNode.LinePos);
			}
			return this.incReadDecoder.DecodedCount;
			Block_8:
			this.curNode.SetValue(this.ps.chars, num2, num3 - num2);
			XmlTextReaderImpl.AdjustLineInfo(this.ps.chars, num2 - num, num2, this.ps.eolNormalized, ref this.incReadLineInfo);
			this.curNode.SetLineInfo(this.incReadLineInfo.lineNo, this.incReadLineInfo.linePos);
			return this.incReadDecoder.DecodedCount;
			Block_9:
			this.SetupReadContentAsBinaryState(inReadBinaryFunction);
			this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End;
			return this.incReadDecoder.DecodedCount;
		}

		// Token: 0x06000C2C RID: 3116 RVA: 0x0003D610 File Offset: 0x0003B810
		private int ReadElementContentAsBinary(byte[] buffer, int index, int count)
		{
			if (count == 0)
			{
				return 0;
			}
			int num = this.ReadContentAsBinary(buffer, index, count);
			if (num > 0)
			{
				return num;
			}
			if (this.curNode.type != XmlNodeType.EndElement)
			{
				throw new XmlException("'{0}' is an invalid XmlNodeType.", this.curNode.type.ToString(), this);
			}
			this.parsingFunction = this.nextParsingFunction;
			this.nextParsingFunction = this.nextNextParsingFunction;
			this.outerReader.Read();
			return 0;
		}

		// Token: 0x06000C2D RID: 3117 RVA: 0x0003D688 File Offset: 0x0003B888
		private void InitBase64Decoder()
		{
			if (this.base64Decoder == null)
			{
				this.base64Decoder = new Base64Decoder();
			}
			else
			{
				this.base64Decoder.Reset();
			}
			this.incReadDecoder = this.base64Decoder;
		}

		// Token: 0x06000C2E RID: 3118 RVA: 0x0003D6B6 File Offset: 0x0003B8B6
		private void InitBinHexDecoder()
		{
			if (this.binHexDecoder == null)
			{
				this.binHexDecoder = new BinHexDecoder();
			}
			else
			{
				this.binHexDecoder.Reset();
			}
			this.incReadDecoder = this.binHexDecoder;
		}

		// Token: 0x06000C2F RID: 3119 RVA: 0x0003D6E4 File Offset: 0x0003B8E4
		private bool UriEqual(Uri uri1, string uri1Str, string uri2Str, XmlResolver resolver)
		{
			if (resolver == null)
			{
				return uri1Str == uri2Str;
			}
			if (uri1 == null)
			{
				uri1 = resolver.ResolveUri(null, uri1Str);
			}
			Uri obj = resolver.ResolveUri(null, uri2Str);
			return uri1.Equals(obj);
		}

		// Token: 0x06000C30 RID: 3120 RVA: 0x0003D724 File Offset: 0x0003B924
		private void RegisterConsumedCharacters(long characters, bool inEntityReference)
		{
			if (this.maxCharactersInDocument > 0L)
			{
				long num = this.charactersInDocument + characters;
				if (num < this.charactersInDocument)
				{
					this.ThrowWithoutLineInfo("The input document has exceeded a limit set by {0}.", "MaxCharactersInDocument");
				}
				else
				{
					this.charactersInDocument = num;
				}
				if (this.charactersInDocument > this.maxCharactersInDocument)
				{
					this.ThrowWithoutLineInfo("The input document has exceeded a limit set by {0}.", "MaxCharactersInDocument");
				}
			}
			if (this.maxCharactersFromEntities > 0L && inEntityReference)
			{
				long num2 = this.charactersFromEntities + characters;
				if (num2 < this.charactersFromEntities)
				{
					this.ThrowWithoutLineInfo("The input document has exceeded a limit set by {0}.", "MaxCharactersFromEntities");
				}
				else
				{
					this.charactersFromEntities = num2;
				}
				if (this.charactersFromEntities > this.maxCharactersFromEntities)
				{
					this.ThrowWithoutLineInfo("The input document has exceeded a limit set by {0}.", "MaxCharactersFromEntities");
				}
			}
		}

		// Token: 0x06000C31 RID: 3121 RVA: 0x0003D7DC File Offset: 0x0003B9DC
		internal unsafe static void AdjustLineInfo(char[] chars, int startPos, int endPos, bool isNormalized, ref LineInfo lineInfo)
		{
			fixed (char* ptr = &chars[startPos])
			{
				XmlTextReaderImpl.AdjustLineInfo(ptr, endPos - startPos, isNormalized, ref lineInfo);
			}
		}

		// Token: 0x06000C32 RID: 3122 RVA: 0x0003D804 File Offset: 0x0003BA04
		internal unsafe static void AdjustLineInfo(string str, int startPos, int endPos, bool isNormalized, ref LineInfo lineInfo)
		{
			fixed (string text = str)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				XmlTextReaderImpl.AdjustLineInfo(ptr + startPos, endPos - startPos, isNormalized, ref lineInfo);
			}
		}

		// Token: 0x06000C33 RID: 3123 RVA: 0x0003D834 File Offset: 0x0003BA34
		internal unsafe static void AdjustLineInfo(char* pChars, int length, bool isNormalized, ref LineInfo lineInfo)
		{
			int num = -1;
			for (int i = 0; i < length; i++)
			{
				char c = pChars[i];
				if (c != '\n')
				{
					if (c == '\r')
					{
						if (!isNormalized)
						{
							lineInfo.lineNo++;
							num = i;
							if (i + 1 < length && pChars[i + 1] == '\n')
							{
								i++;
								num++;
							}
						}
					}
				}
				else
				{
					lineInfo.lineNo++;
					num = i;
				}
			}
			if (num >= 0)
			{
				lineInfo.linePos = length - num;
			}
		}

		// Token: 0x06000C34 RID: 3124 RVA: 0x0003D8AC File Offset: 0x0003BAAC
		internal static string StripSpaces(string value)
		{
			int length = value.Length;
			if (length <= 0)
			{
				return string.Empty;
			}
			int num = 0;
			StringBuilder stringBuilder = null;
			while (value[num] == ' ')
			{
				num++;
				if (num == length)
				{
					return " ";
				}
			}
			int i;
			for (i = num; i < length; i++)
			{
				if (value[i] == ' ')
				{
					int num2 = i + 1;
					while (num2 < length && value[num2] == ' ')
					{
						num2++;
					}
					if (num2 == length)
					{
						if (stringBuilder == null)
						{
							return value.Substring(num, i - num);
						}
						stringBuilder.Append(value, num, i - num);
						return stringBuilder.ToString();
					}
					else if (num2 > i + 1)
					{
						if (stringBuilder == null)
						{
							stringBuilder = new StringBuilder(length);
						}
						stringBuilder.Append(value, num, i - num + 1);
						num = num2;
						i = num2 - 1;
					}
				}
			}
			if (stringBuilder != null)
			{
				if (i > num)
				{
					stringBuilder.Append(value, num, i - num);
				}
				return stringBuilder.ToString();
			}
			if (num != 0)
			{
				return value.Substring(num, length - num);
			}
			return value;
		}

		// Token: 0x06000C35 RID: 3125 RVA: 0x0003D994 File Offset: 0x0003BB94
		internal static void StripSpaces(char[] value, int index, ref int len)
		{
			if (len <= 0)
			{
				return;
			}
			int num = index;
			int num2 = index + len;
			while (value[num] == ' ')
			{
				num++;
				if (num == num2)
				{
					len = 1;
					return;
				}
			}
			int num3 = num - index;
			for (int i = num; i < num2; i++)
			{
				char c;
				if ((c = value[i]) == ' ')
				{
					int num4 = i + 1;
					while (num4 < num2 && value[num4] == ' ')
					{
						num4++;
					}
					if (num4 == num2)
					{
						num3 += num4 - i;
						break;
					}
					if (num4 > i + 1)
					{
						num3 += num4 - i - 1;
						i = num4 - 1;
					}
				}
				value[i - num3] = c;
			}
			len -= num3;
		}

		// Token: 0x06000C36 RID: 3126 RVA: 0x0003DA27 File Offset: 0x0003BC27
		internal static void BlockCopyChars(char[] src, int srcOffset, char[] dst, int dstOffset, int count)
		{
			Buffer.BlockCopy(src, srcOffset * 2, dst, dstOffset * 2, count * 2);
		}

		// Token: 0x06000C37 RID: 3127 RVA: 0x0003DA3A File Offset: 0x0003BC3A
		internal static void BlockCopy(byte[] src, int srcOffset, byte[] dst, int dstOffset, int count)
		{
			Buffer.BlockCopy(src, srcOffset, dst, dstOffset, count);
		}

		// Token: 0x06000C38 RID: 3128 RVA: 0x0003DA47 File Offset: 0x0003BC47
		private void CheckAsyncCall()
		{
			if (!this.useAsync)
			{
				throw new InvalidOperationException(Res.GetString("Set XmlReaderSettings.Async to true if you want to use Async Methods."));
			}
		}

		// Token: 0x06000C39 RID: 3129 RVA: 0x0003DA61 File Offset: 0x0003BC61
		public override Task<string> GetValueAsync()
		{
			this.CheckAsyncCall();
			if (this.parsingFunction >= XmlTextReaderImpl.ParsingFunction.PartialTextValue)
			{
				return this._GetValueAsync();
			}
			return Task.FromResult<string>(this.curNode.StringValue);
		}

		// Token: 0x06000C3A RID: 3130 RVA: 0x0003DA8C File Offset: 0x0003BC8C
		private async Task<string> _GetValueAsync()
		{
			if (this.parsingFunction >= XmlTextReaderImpl.ParsingFunction.PartialTextValue)
			{
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.PartialTextValue)
				{
					await this.FinishPartialValueAsync().ConfigureAwait(false);
					this.parsingFunction = this.nextParsingFunction;
				}
				else
				{
					await this.FinishOtherValueIteratorAsync().ConfigureAwait(false);
				}
			}
			return this.curNode.StringValue;
		}

		// Token: 0x06000C3B RID: 3131 RVA: 0x0003DAD4 File Offset: 0x0003BCD4
		private Task FinishInitAsync()
		{
			switch (this.laterInitParam.initType)
			{
			case XmlTextReaderImpl.InitInputType.UriString:
				return this.FinishInitUriStringAsync();
			case XmlTextReaderImpl.InitInputType.Stream:
				return this.FinishInitStreamAsync();
			case XmlTextReaderImpl.InitInputType.TextReader:
				return this.FinishInitTextReaderAsync();
			default:
				return AsyncHelper.DoneTask;
			}
		}

		// Token: 0x06000C3C RID: 3132 RVA: 0x0003DB1C File Offset: 0x0003BD1C
		private async Task FinishInitUriStringAsync()
		{
			object obj = await this.laterInitParam.inputUriResolver.GetEntityAsync(this.laterInitParam.inputbaseUri, string.Empty, typeof(Stream)).ConfigureAwait(false);
			Stream stream = (Stream)obj;
			if (stream == null)
			{
				throw new XmlException("Cannot resolve '{0}'.", this.laterInitParam.inputUriStr);
			}
			Encoding encoding = null;
			if (this.laterInitParam.inputContext != null)
			{
				encoding = this.laterInitParam.inputContext.Encoding;
			}
			try
			{
				await this.InitStreamInputAsync(this.laterInitParam.inputbaseUri, this.reportedBaseUri, stream, null, 0, encoding).ConfigureAwait(false);
				this.reportedEncoding = this.ps.encoding;
				if (this.laterInitParam.inputContext != null && this.laterInitParam.inputContext.HasDtdInfo)
				{
					await this.ProcessDtdFromParserContextAsync(this.laterInitParam.inputContext).ConfigureAwait(false);
				}
			}
			catch
			{
				stream.Close();
				throw;
			}
			this.laterInitParam = null;
		}

		// Token: 0x06000C3D RID: 3133 RVA: 0x0003DB64 File Offset: 0x0003BD64
		private async Task FinishInitStreamAsync()
		{
			Encoding encoding = null;
			if (this.laterInitParam.inputContext != null)
			{
				encoding = this.laterInitParam.inputContext.Encoding;
			}
			await this.InitStreamInputAsync(this.laterInitParam.inputbaseUri, this.reportedBaseUri, this.laterInitParam.inputStream, this.laterInitParam.inputBytes, this.laterInitParam.inputByteCount, encoding).ConfigureAwait(false);
			this.reportedEncoding = this.ps.encoding;
			if (this.laterInitParam.inputContext != null && this.laterInitParam.inputContext.HasDtdInfo)
			{
				await this.ProcessDtdFromParserContextAsync(this.laterInitParam.inputContext).ConfigureAwait(false);
			}
			this.laterInitParam = null;
		}

		// Token: 0x06000C3E RID: 3134 RVA: 0x0003DBAC File Offset: 0x0003BDAC
		private async Task FinishInitTextReaderAsync()
		{
			await this.InitTextReaderInputAsync(this.reportedBaseUri, this.laterInitParam.inputTextReader).ConfigureAwait(false);
			this.reportedEncoding = this.ps.encoding;
			if (this.laterInitParam.inputContext != null && this.laterInitParam.inputContext.HasDtdInfo)
			{
				await this.ProcessDtdFromParserContextAsync(this.laterInitParam.inputContext).ConfigureAwait(false);
			}
			this.laterInitParam = null;
		}

		// Token: 0x06000C3F RID: 3135 RVA: 0x0003DBF4 File Offset: 0x0003BDF4
		public override Task<bool> ReadAsync()
		{
			this.CheckAsyncCall();
			if (this.laterInitParam != null)
			{
				return this.FinishInitAsync().CallBoolTaskFuncWhenFinish(new Func<Task<bool>>(this.ReadAsync));
			}
			for (;;)
			{
				switch (this.parsingFunction)
				{
				case XmlTextReaderImpl.ParsingFunction.ElementContent:
					goto IL_9E;
				case XmlTextReaderImpl.ParsingFunction.NoData:
					goto IL_2DC;
				case XmlTextReaderImpl.ParsingFunction.SwitchToInteractive:
					this.readState = ReadState.Interactive;
					this.parsingFunction = this.nextParsingFunction;
					break;
				case XmlTextReaderImpl.ParsingFunction.SwitchToInteractiveXmlDecl:
					goto IL_C4;
				case XmlTextReaderImpl.ParsingFunction.DocumentContent:
					goto IL_A5;
				case XmlTextReaderImpl.ParsingFunction.MoveToElementContent:
					this.ResetAttributes();
					this.index++;
					this.curNode = this.AddNode(this.index, this.index);
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ElementContent;
					break;
				case XmlTextReaderImpl.ParsingFunction.PopElementContext:
					this.PopElementContext();
					this.parsingFunction = this.nextParsingFunction;
					break;
				case XmlTextReaderImpl.ParsingFunction.PopEmptyElementContext:
					this.curNode = this.nodes[this.index];
					this.curNode.IsEmptyElement = false;
					this.ResetAttributes();
					this.PopElementContext();
					this.parsingFunction = this.nextParsingFunction;
					break;
				case XmlTextReaderImpl.ParsingFunction.ResetAttributesRootLevel:
					this.ResetAttributes();
					this.curNode = this.nodes[this.index];
					this.parsingFunction = ((this.index == 0) ? XmlTextReaderImpl.ParsingFunction.DocumentContent : XmlTextReaderImpl.ParsingFunction.ElementContent);
					break;
				case XmlTextReaderImpl.ParsingFunction.Error:
				case XmlTextReaderImpl.ParsingFunction.Eof:
				case XmlTextReaderImpl.ParsingFunction.ReaderClosed:
					goto IL_2D6;
				case XmlTextReaderImpl.ParsingFunction.EntityReference:
					goto IL_186;
				case XmlTextReaderImpl.ParsingFunction.InIncrementalRead:
					goto IL_29E;
				case XmlTextReaderImpl.ParsingFunction.FragmentAttribute:
					goto IL_2AA;
				case XmlTextReaderImpl.ParsingFunction.ReportEndEntity:
					goto IL_19F;
				case XmlTextReaderImpl.ParsingFunction.AfterResolveEntityInContent:
					this.curNode = this.AddNode(this.index, this.index);
					this.reportedEncoding = this.ps.encoding;
					this.reportedBaseUri = this.ps.baseUriStr;
					this.parsingFunction = this.nextParsingFunction;
					break;
				case XmlTextReaderImpl.ParsingFunction.AfterResolveEmptyEntityInContent:
					goto IL_202;
				case XmlTextReaderImpl.ParsingFunction.XmlDeclarationFragment:
					goto IL_2B6;
				case XmlTextReaderImpl.ParsingFunction.GoToEof:
					goto IL_2CA;
				case XmlTextReaderImpl.ParsingFunction.PartialTextValue:
					goto IL_2ED;
				case XmlTextReaderImpl.ParsingFunction.InReadAttributeValue:
					this.FinishAttributeValueIterator();
					this.curNode = this.nodes[this.index];
					break;
				case XmlTextReaderImpl.ParsingFunction.InReadValueChunk:
					goto IL_306;
				case XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary:
					goto IL_31F;
				case XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary:
					goto IL_338;
				}
			}
			IL_9E:
			return this.ParseElementContentAsync();
			IL_A5:
			return this.ParseDocumentContentAsync();
			IL_C4:
			return this.ReadAsync_SwitchToInteractiveXmlDecl();
			IL_186:
			this.parsingFunction = this.nextParsingFunction;
			return this.ParseEntityReferenceAsync().ReturnTaskBoolWhenFinish(true);
			IL_19F:
			this.SetupEndEntityNodeInContent();
			this.parsingFunction = this.nextParsingFunction;
			return AsyncHelper.DoneTaskTrue;
			IL_202:
			this.curNode = this.AddNode(this.index, this.index);
			this.curNode.SetValueNode(XmlNodeType.Text, string.Empty);
			this.curNode.SetLineInfo(this.ps.lineNo, this.ps.LinePos);
			this.reportedEncoding = this.ps.encoding;
			this.reportedBaseUri = this.ps.baseUriStr;
			this.parsingFunction = this.nextParsingFunction;
			return AsyncHelper.DoneTaskTrue;
			IL_29E:
			this.FinishIncrementalRead();
			return AsyncHelper.DoneTaskTrue;
			IL_2AA:
			return Task.FromResult<bool>(this.ParseFragmentAttribute());
			IL_2B6:
			this.ParseXmlDeclarationFragment();
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.GoToEof;
			return AsyncHelper.DoneTaskTrue;
			IL_2CA:
			this.OnEof();
			return AsyncHelper.DoneTaskFalse;
			IL_2D6:
			return AsyncHelper.DoneTaskFalse;
			IL_2DC:
			this.ThrowWithoutLineInfo("Root element is missing.");
			return AsyncHelper.DoneTaskFalse;
			IL_2ED:
			return this.SkipPartialTextValueAsync().CallBoolTaskFuncWhenFinish(new Func<Task<bool>>(this.ReadAsync));
			IL_306:
			return this.FinishReadValueChunkAsync().CallBoolTaskFuncWhenFinish(new Func<Task<bool>>(this.ReadAsync));
			IL_31F:
			return this.FinishReadContentAsBinaryAsync().CallBoolTaskFuncWhenFinish(new Func<Task<bool>>(this.ReadAsync));
			IL_338:
			return this.FinishReadElementContentAsBinaryAsync().CallBoolTaskFuncWhenFinish(new Func<Task<bool>>(this.ReadAsync));
		}

		// Token: 0x06000C40 RID: 3136 RVA: 0x0003DF54 File Offset: 0x0003C154
		private Task<bool> ReadAsync_SwitchToInteractiveXmlDecl()
		{
			this.readState = ReadState.Interactive;
			this.parsingFunction = this.nextParsingFunction;
			Task<bool> task = this.ParseXmlDeclarationAsync(false);
			if (task.IsSuccess())
			{
				return this.ReadAsync_SwitchToInteractiveXmlDecl_Helper(task.Result);
			}
			return this._ReadAsync_SwitchToInteractiveXmlDecl(task);
		}

		// Token: 0x06000C41 RID: 3137 RVA: 0x0003DF98 File Offset: 0x0003C198
		private async Task<bool> _ReadAsync_SwitchToInteractiveXmlDecl(Task<bool> task)
		{
			bool finish = await task.ConfigureAwait(false);
			return await this.ReadAsync_SwitchToInteractiveXmlDecl_Helper(finish).ConfigureAwait(false);
		}

		// Token: 0x06000C42 RID: 3138 RVA: 0x0003DFE5 File Offset: 0x0003C1E5
		private Task<bool> ReadAsync_SwitchToInteractiveXmlDecl_Helper(bool finish)
		{
			if (finish)
			{
				this.reportedEncoding = this.ps.encoding;
				return AsyncHelper.DoneTaskTrue;
			}
			this.reportedEncoding = this.ps.encoding;
			return this.ReadAsync();
		}

		// Token: 0x06000C43 RID: 3139 RVA: 0x0003E018 File Offset: 0x0003C218
		public override async Task SkipAsync()
		{
			this.CheckAsyncCall();
			if (this.readState == ReadState.Interactive)
			{
				if (this.InAttributeValueIterator)
				{
					this.FinishAttributeValueIterator();
					this.curNode = this.nodes[this.index];
				}
				else
				{
					XmlTextReaderImpl.ParsingFunction parsingFunction = this.parsingFunction;
					if (parsingFunction != XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
					{
						switch (parsingFunction)
						{
						case XmlTextReaderImpl.ParsingFunction.PartialTextValue:
							await this.SkipPartialTextValueAsync().ConfigureAwait(false);
							break;
						case XmlTextReaderImpl.ParsingFunction.InReadValueChunk:
							await this.FinishReadValueChunkAsync().ConfigureAwait(false);
							break;
						case XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary:
							await this.FinishReadContentAsBinaryAsync().ConfigureAwait(false);
							break;
						case XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary:
							await this.FinishReadElementContentAsBinaryAsync().ConfigureAwait(false);
							break;
						}
					}
					else
					{
						this.FinishIncrementalRead();
					}
				}
				XmlNodeType type = this.curNode.type;
				if (type != XmlNodeType.Element)
				{
					if (type != XmlNodeType.Attribute)
					{
						goto IL_318;
					}
					this.outerReader.MoveToElement();
				}
				if (!this.curNode.IsEmptyElement)
				{
					int initialDepth = this.index;
					this.parsingMode = XmlTextReaderImpl.ParsingMode.SkipContent;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					do
					{
						configuredTaskAwaiter = this.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						}
					}
					while (configuredTaskAwaiter.GetResult() && this.index > initialDepth);
					this.parsingMode = XmlTextReaderImpl.ParsingMode.Full;
				}
				IL_318:
				await this.outerReader.ReadAsync().ConfigureAwait(false);
			}
		}

		// Token: 0x06000C44 RID: 3140 RVA: 0x0003E060 File Offset: 0x0003C260
		private async Task<int> ReadContentAsBase64_AsyncHelper(Task<bool> task, byte[] buffer, int index, int count)
		{
			await task.ConfigureAwait(false);
			int result;
			if (!task.Result)
			{
				result = 0;
			}
			else
			{
				this.InitBase64Decoder();
				result = await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
			}
			return result;
		}

		// Token: 0x06000C45 RID: 3141 RVA: 0x0003E0C8 File Offset: 0x0003C2C8
		public override Task<int> ReadContentAsBase64Async(byte[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
			{
				if (this.incReadDecoder == this.base64Decoder)
				{
					return this.ReadContentAsBinaryAsync(buffer, index, count);
				}
			}
			else
			{
				if (this.readState != ReadState.Interactive)
				{
					return AsyncHelper.DoneTaskZero;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
				{
					throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
				}
				if (!XmlReader.CanReadContentAs(this.curNode.type))
				{
					throw base.CreateReadContentAsException("ReadContentAsBase64");
				}
				Task<bool> task = this.InitReadContentAsBinaryAsync();
				if (!task.IsSuccess())
				{
					return this.ReadContentAsBase64_AsyncHelper(task, buffer, index, count);
				}
				if (!task.Result)
				{
					return AsyncHelper.DoneTaskZero;
				}
			}
			this.InitBase64Decoder();
			return this.ReadContentAsBinaryAsync(buffer, index, count);
		}

		// Token: 0x06000C46 RID: 3142 RVA: 0x0003E1BC File Offset: 0x0003C3BC
		public override async Task<int> ReadContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
			{
				if (this.incReadDecoder == this.binHexDecoder)
				{
					return await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
				}
			}
			else
			{
				if (this.readState != ReadState.Interactive)
				{
					return 0;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
				{
					throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
				}
				if (!XmlReader.CanReadContentAs(this.curNode.type))
				{
					throw base.CreateReadContentAsException("ReadContentAsBinHex");
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.InitReadContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return 0;
				}
			}
			this.InitBinHexDecoder();
			return await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
		}

		// Token: 0x06000C47 RID: 3143 RVA: 0x0003E21C File Offset: 0x0003C41C
		private async Task<int> ReadElementContentAsBase64Async_Helper(Task<bool> task, byte[] buffer, int index, int count)
		{
			await task.ConfigureAwait(false);
			int result;
			if (!task.Result)
			{
				result = 0;
			}
			else
			{
				this.InitBase64Decoder();
				result = await this.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
			}
			return result;
		}

		// Token: 0x06000C48 RID: 3144 RVA: 0x0003E284 File Offset: 0x0003C484
		public override Task<int> ReadElementContentAsBase64Async(byte[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
			{
				if (this.incReadDecoder == this.base64Decoder)
				{
					return this.ReadElementContentAsBinaryAsync(buffer, index, count);
				}
			}
			else
			{
				if (this.readState != ReadState.Interactive)
				{
					return AsyncHelper.DoneTaskZero;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
				{
					throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
				}
				if (this.curNode.type != XmlNodeType.Element)
				{
					throw base.CreateReadElementContentAsException("ReadElementContentAsBinHex");
				}
				Task<bool> task = this.InitReadElementContentAsBinaryAsync();
				if (!task.IsSuccess())
				{
					return this.ReadElementContentAsBase64Async_Helper(task, buffer, index, count);
				}
				if (!task.Result)
				{
					return AsyncHelper.DoneTaskZero;
				}
			}
			this.InitBase64Decoder();
			return this.ReadElementContentAsBinaryAsync(buffer, index, count);
		}

		// Token: 0x06000C49 RID: 3145 RVA: 0x0003E374 File Offset: 0x0003C574
		public override async Task<int> ReadElementContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
			{
				if (this.incReadDecoder == this.binHexDecoder)
				{
					return await this.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
				}
			}
			else
			{
				if (this.readState != ReadState.Interactive)
				{
					return 0;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
				{
					throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
				}
				if (this.curNode.type != XmlNodeType.Element)
				{
					throw base.CreateReadElementContentAsException("ReadElementContentAsBinHex");
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.InitReadElementContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return 0;
				}
			}
			this.InitBinHexDecoder();
			return await this.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
		}

		// Token: 0x06000C4A RID: 3146 RVA: 0x0003E3D4 File Offset: 0x0003C5D4
		public override async Task<int> ReadValueChunkAsync(char[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			if (!XmlReader.HasValueInternal(this.curNode.type))
			{
				throw new InvalidOperationException(Res.GetString("The ReadValueAsChunk method is not supported on node type {0}.", new object[]
				{
					this.curNode.type
				}));
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.parsingFunction != XmlTextReaderImpl.ParsingFunction.InReadValueChunk)
			{
				if (this.readState != ReadState.Interactive)
				{
					return 0;
				}
				if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.PartialTextValue)
				{
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue;
				}
				else
				{
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue;
					this.nextNextParsingFunction = this.nextParsingFunction;
					this.nextParsingFunction = this.parsingFunction;
				}
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.InReadValueChunk;
				this.readValueOffset = 0;
			}
			int result;
			if (count == 0)
			{
				result = 0;
			}
			else
			{
				int readCount = 0;
				int num = this.curNode.CopyTo(this.readValueOffset, buffer, index + readCount, count - readCount);
				readCount += num;
				this.readValueOffset += num;
				if (readCount == count)
				{
					if (XmlCharType.IsHighSurrogate((int)buffer[index + count - 1]))
					{
						int num2 = readCount;
						readCount = num2 - 1;
						this.readValueOffset--;
						if (readCount == 0)
						{
							this.Throw("The buffer is not large enough to fit a surrogate pair. Please provide a buffer of size at least 2 characters.");
						}
					}
					result = readCount;
				}
				else
				{
					if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue)
					{
						this.curNode.SetValue(string.Empty);
						bool flag = false;
						int num3 = 0;
						int num4 = 0;
						while (readCount < count && !flag)
						{
							int outOrChars = 0;
							object obj = await this.ParseTextAsync(outOrChars).ConfigureAwait(false);
							num3 = obj.Item1;
							num4 = obj.Item2;
							outOrChars = obj.Item3;
							flag = obj.Item4;
							int num5 = count - readCount;
							if (num5 > num4 - num3)
							{
								num5 = num4 - num3;
							}
							XmlTextReaderImpl.BlockCopyChars(this.ps.chars, num3, buffer, index + readCount, num5);
							readCount += num5;
							num3 += num5;
						}
						this.incReadState = (flag ? XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue : XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue);
						if (readCount == count && XmlCharType.IsHighSurrogate((int)buffer[index + count - 1]))
						{
							int num2 = readCount;
							readCount = num2 - 1;
							num3--;
							if (readCount == 0)
							{
								this.Throw("The buffer is not large enough to fit a surrogate pair. Please provide a buffer of size at least 2 characters.");
							}
						}
						this.readValueOffset = 0;
						this.curNode.SetValue(this.ps.chars, num3, num4 - num3);
					}
					result = readCount;
				}
			}
			return result;
		}

		// Token: 0x06000C4B RID: 3147 RVA: 0x0003E431 File Offset: 0x0003C631
		internal Task<int> DtdParserProxy_ReadDataAsync()
		{
			this.CheckAsyncCall();
			return this.ReadDataAsync();
		}

		// Token: 0x06000C4C RID: 3148 RVA: 0x0003E440 File Offset: 0x0003C640
		internal async Task<int> DtdParserProxy_ParseNumericCharRefAsync(StringBuilder internalSubsetBuilder)
		{
			this.CheckAsyncCall();
			return (await this.ParseNumericCharRefAsync(true, internalSubsetBuilder).ConfigureAwait(false)).Item2;
		}

		// Token: 0x06000C4D RID: 3149 RVA: 0x0003E48D File Offset: 0x0003C68D
		internal Task<int> DtdParserProxy_ParseNamedCharRefAsync(bool expand, StringBuilder internalSubsetBuilder)
		{
			this.CheckAsyncCall();
			return this.ParseNamedCharRefAsync(expand, internalSubsetBuilder);
		}

		// Token: 0x06000C4E RID: 3150 RVA: 0x0003E4A0 File Offset: 0x0003C6A0
		internal async Task DtdParserProxy_ParsePIAsync(StringBuilder sb)
		{
			this.CheckAsyncCall();
			if (sb == null)
			{
				XmlTextReaderImpl.ParsingMode pm = this.parsingMode;
				this.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
				await this.ParsePIAsync(null).ConfigureAwait(false);
				this.parsingMode = pm;
			}
			else
			{
				await this.ParsePIAsync(sb).ConfigureAwait(false);
			}
		}

		// Token: 0x06000C4F RID: 3151 RVA: 0x0003E4F0 File Offset: 0x0003C6F0
		internal async Task DtdParserProxy_ParseCommentAsync(StringBuilder sb)
		{
			this.CheckAsyncCall();
			try
			{
				if (sb == null)
				{
					XmlTextReaderImpl.ParsingMode savedParsingMode = this.parsingMode;
					this.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
					await this.ParseCDataOrCommentAsync(XmlNodeType.Comment).ConfigureAwait(false);
					this.parsingMode = savedParsingMode;
				}
				else
				{
					XmlTextReaderImpl.NodeData originalCurNode = this.curNode;
					this.curNode = this.AddNode(this.index + this.attrCount + 1, this.index);
					await this.ParseCDataOrCommentAsync(XmlNodeType.Comment).ConfigureAwait(false);
					this.curNode.CopyTo(0, sb);
					this.curNode = originalCurNode;
					originalCurNode = null;
				}
			}
			catch (XmlException ex)
			{
				if (!(ex.ResString == "Unexpected end of file while parsing {0} has occurred.") || this.ps.entity == null)
				{
					throw;
				}
				this.SendValidationEvent(XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", null, this.ps.LineNo, this.ps.LinePos);
			}
		}

		// Token: 0x06000C50 RID: 3152 RVA: 0x0003E540 File Offset: 0x0003C740
		internal async Task<Tuple<int, bool>> DtdParserProxy_PushEntityAsync(IDtdEntityInfo entity)
		{
			this.CheckAsyncCall();
			bool item;
			if (entity.IsExternal)
			{
				if (this.IsResolverNull)
				{
					return new Tuple<int, bool>(-1, false);
				}
				item = await this.PushExternalEntityAsync(entity).ConfigureAwait(false);
			}
			else
			{
				this.PushInternalEntity(entity);
				item = true;
			}
			return new Tuple<int, bool>(this.ps.entityId, item);
		}

		// Token: 0x06000C51 RID: 3153 RVA: 0x0003E590 File Offset: 0x0003C790
		internal async Task<bool> DtdParserProxy_PushExternalSubsetAsync(string systemId, string publicId)
		{
			this.CheckAsyncCall();
			bool result;
			if (this.IsResolverNull)
			{
				result = false;
			}
			else
			{
				if (this.ps.baseUri == null && !string.IsNullOrEmpty(this.ps.baseUriStr))
				{
					this.ps.baseUri = this.xmlResolver.ResolveUri(null, this.ps.baseUriStr);
				}
				await this.PushExternalEntityOrSubsetAsync(publicId, systemId, this.ps.baseUri, null).ConfigureAwait(false);
				this.ps.entity = null;
				this.ps.entityId = 0;
				int initialPos = this.ps.charPos;
				if (this.v1Compat)
				{
					await this.EatWhitespacesAsync(null).ConfigureAwait(false);
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ParseXmlDeclarationAsync(true).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					this.ps.charPos = initialPos;
				}
				result = true;
			}
			return result;
		}

		// Token: 0x06000C52 RID: 3154 RVA: 0x0003E5E5 File Offset: 0x0003C7E5
		private Task InitStreamInputAsync(Uri baseUri, Stream stream, Encoding encoding)
		{
			return this.InitStreamInputAsync(baseUri, baseUri.ToString(), stream, null, 0, encoding);
		}

		// Token: 0x06000C53 RID: 3155 RVA: 0x0003E5F8 File Offset: 0x0003C7F8
		private Task InitStreamInputAsync(Uri baseUri, string baseUriStr, Stream stream, Encoding encoding)
		{
			return this.InitStreamInputAsync(baseUri, baseUriStr, stream, null, 0, encoding);
		}

		// Token: 0x06000C54 RID: 3156 RVA: 0x0003E608 File Offset: 0x0003C808
		private async Task InitStreamInputAsync(Uri baseUri, string baseUriStr, Stream stream, byte[] bytes, int byteCount, Encoding encoding)
		{
			this.ps.stream = stream;
			this.ps.baseUri = baseUri;
			this.ps.baseUriStr = baseUriStr;
			int num;
			if (bytes != null)
			{
				this.ps.bytes = bytes;
				this.ps.bytesUsed = byteCount;
				num = this.ps.bytes.Length;
			}
			else
			{
				if (this.laterInitParam != null && this.laterInitParam.useAsync)
				{
					num = 65536;
				}
				else
				{
					num = XmlReader.CalcBufferSize(stream);
				}
				if (this.ps.bytes == null || this.ps.bytes.Length < num)
				{
					this.ps.bytes = new byte[num];
				}
			}
			if (this.ps.chars == null || this.ps.chars.Length < num + 1)
			{
				this.ps.chars = new char[num + 1];
			}
			this.ps.bytePos = 0;
			while (this.ps.bytesUsed < 4 && this.ps.bytes.Length - this.ps.bytesUsed > 0)
			{
				int num2 = await stream.ReadAsync(this.ps.bytes, this.ps.bytesUsed, this.ps.bytes.Length - this.ps.bytesUsed).ConfigureAwait(false);
				if (num2 == 0)
				{
					this.ps.isStreamEof = true;
					break;
				}
				this.ps.bytesUsed = this.ps.bytesUsed + num2;
			}
			if (encoding == null)
			{
				encoding = this.DetectEncoding();
			}
			this.SetupEncoding(encoding);
			byte[] preamble = this.ps.encoding.GetPreamble();
			int num3 = preamble.Length;
			int num4 = 0;
			while (num4 < num3 && num4 < this.ps.bytesUsed && this.ps.bytes[num4] == preamble[num4])
			{
				num4++;
			}
			if (num4 == num3)
			{
				this.ps.bytePos = num3;
			}
			this.documentStartBytePos = this.ps.bytePos;
			this.ps.eolNormalized = !this.normalize;
			this.ps.appendMode = true;
			await this.ReadDataAsync().ConfigureAwait(false);
		}

		// Token: 0x06000C55 RID: 3157 RVA: 0x0003E680 File Offset: 0x0003C880
		private Task InitTextReaderInputAsync(string baseUriStr, TextReader input)
		{
			return this.InitTextReaderInputAsync(baseUriStr, null, input);
		}

		// Token: 0x06000C56 RID: 3158 RVA: 0x0003E68C File Offset: 0x0003C88C
		private Task InitTextReaderInputAsync(string baseUriStr, Uri baseUri, TextReader input)
		{
			this.ps.textReader = input;
			this.ps.baseUriStr = baseUriStr;
			this.ps.baseUri = baseUri;
			if (this.ps.chars == null)
			{
				int num;
				if (this.laterInitParam != null && this.laterInitParam.useAsync)
				{
					num = 65536;
				}
				else
				{
					num = 4096;
				}
				this.ps.chars = new char[num + 1];
			}
			this.ps.encoding = Encoding.Unicode;
			this.ps.eolNormalized = !this.normalize;
			this.ps.appendMode = true;
			return this.ReadDataAsync();
		}

		// Token: 0x06000C57 RID: 3159 RVA: 0x0003E738 File Offset: 0x0003C938
		private Task ProcessDtdFromParserContextAsync(XmlParserContext context)
		{
			switch (this.dtdProcessing)
			{
			case DtdProcessing.Prohibit:
				this.ThrowWithoutLineInfo("For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.");
				break;
			case DtdProcessing.Parse:
				return this.ParseDtdFromParserContextAsync();
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000C58 RID: 3160 RVA: 0x0003E77C File Offset: 0x0003C97C
		private Task SwitchEncodingAsync(Encoding newEncoding)
		{
			if ((newEncoding.WebName != this.ps.encoding.WebName || this.ps.decoder is SafeAsciiDecoder) && !this.afterResetState)
			{
				this.UnDecodeChars();
				this.ps.appendMode = false;
				this.SetupEncoding(newEncoding);
				return this.ReadDataAsync();
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000C59 RID: 3161 RVA: 0x0003E7E5 File Offset: 0x0003C9E5
		private Task SwitchEncodingToUTF8Async()
		{
			return this.SwitchEncodingAsync(new UTF8Encoding(true, true));
		}

		// Token: 0x06000C5A RID: 3162 RVA: 0x0003E7F4 File Offset: 0x0003C9F4
		private async Task<int> ReadDataAsync()
		{
			int result;
			if (this.ps.isEof)
			{
				result = 0;
			}
			else
			{
				int charsRead;
				if (this.ps.appendMode)
				{
					if (this.ps.charsUsed == this.ps.chars.Length - 1)
					{
						for (int i = 0; i < this.attrCount; i++)
						{
							this.nodes[this.index + i + 1].OnBufferInvalidated();
						}
						char[] array = new char[this.ps.chars.Length * 2];
						XmlTextReaderImpl.BlockCopyChars(this.ps.chars, 0, array, 0, this.ps.chars.Length);
						this.ps.chars = array;
					}
					if (this.ps.stream != null && this.ps.bytesUsed - this.ps.bytePos < 6 && this.ps.bytes.Length - this.ps.bytesUsed < 6)
					{
						byte[] array2 = new byte[this.ps.bytes.Length * 2];
						XmlTextReaderImpl.BlockCopy(this.ps.bytes, 0, array2, 0, this.ps.bytesUsed);
						this.ps.bytes = array2;
					}
					charsRead = this.ps.chars.Length - this.ps.charsUsed - 1;
					if (charsRead > 80)
					{
						charsRead = 80;
					}
				}
				else
				{
					int num = this.ps.chars.Length;
					if (num - this.ps.charsUsed <= num / 2)
					{
						for (int j = 0; j < this.attrCount; j++)
						{
							this.nodes[this.index + j + 1].OnBufferInvalidated();
						}
						int num2 = this.ps.charsUsed - this.ps.charPos;
						if (num2 < num - 1)
						{
							this.ps.lineStartPos = this.ps.lineStartPos - this.ps.charPos;
							if (num2 > 0)
							{
								XmlTextReaderImpl.BlockCopyChars(this.ps.chars, this.ps.charPos, this.ps.chars, 0, num2);
							}
							this.ps.charPos = 0;
							this.ps.charsUsed = num2;
						}
						else
						{
							char[] array3 = new char[this.ps.chars.Length * 2];
							XmlTextReaderImpl.BlockCopyChars(this.ps.chars, 0, array3, 0, this.ps.chars.Length);
							this.ps.chars = array3;
						}
					}
					if (this.ps.stream != null)
					{
						int num3 = this.ps.bytesUsed - this.ps.bytePos;
						if (num3 <= 128)
						{
							if (num3 == 0)
							{
								this.ps.bytesUsed = 0;
							}
							else
							{
								XmlTextReaderImpl.BlockCopy(this.ps.bytes, this.ps.bytePos, this.ps.bytes, 0, num3);
								this.ps.bytesUsed = num3;
							}
							this.ps.bytePos = 0;
						}
					}
					charsRead = this.ps.chars.Length - this.ps.charsUsed - 1;
				}
				if (this.ps.stream != null)
				{
					if (!this.ps.isStreamEof && this.ps.bytePos == this.ps.bytesUsed && this.ps.bytes.Length - this.ps.bytesUsed > 0)
					{
						int num4 = await this.ps.stream.ReadAsync(this.ps.bytes, this.ps.bytesUsed, this.ps.bytes.Length - this.ps.bytesUsed).ConfigureAwait(false);
						if (num4 == 0)
						{
							this.ps.isStreamEof = true;
						}
						this.ps.bytesUsed = this.ps.bytesUsed + num4;
					}
					int bytePos = this.ps.bytePos;
					charsRead = this.GetChars(charsRead);
					if (charsRead == 0 && this.ps.bytePos != bytePos)
					{
						return await this.ReadDataAsync().ConfigureAwait(false);
					}
				}
				else if (this.ps.textReader != null)
				{
					charsRead = await this.ps.textReader.ReadAsync(this.ps.chars, this.ps.charsUsed, this.ps.chars.Length - this.ps.charsUsed - 1).ConfigureAwait(false);
					this.ps.charsUsed = this.ps.charsUsed + charsRead;
				}
				else
				{
					charsRead = 0;
				}
				this.RegisterConsumedCharacters((long)charsRead, this.InEntity);
				if (charsRead == 0)
				{
					this.ps.isEof = true;
				}
				this.ps.chars[this.ps.charsUsed] = '\0';
				result = charsRead;
			}
			return result;
		}

		// Token: 0x06000C5B RID: 3163 RVA: 0x0003E83C File Offset: 0x0003CA3C
		private async Task<bool> ParseXmlDeclarationAsync(bool isTextDecl)
		{
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			while (this.ps.charsUsed - this.ps.charPos < 6)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					IL_D47:
					if (!isTextDecl)
					{
						this.parsingFunction = this.nextParsingFunction;
					}
					if (this.afterResetState)
					{
						string webName = this.ps.encoding.WebName;
						if (webName != "utf-8" && webName != "utf-16" && webName != "utf-16BE" && !(this.ps.encoding is Ucs4Encoding))
						{
							this.Throw("'{0}' is an invalid value for the 'encoding' attribute. The encoding cannot be switched after a call to ResetState.", (this.ps.encoding.GetByteCount("A") == 1) ? "UTF-8" : "UTF-16");
						}
					}
					if (this.ps.decoder is SafeAsciiDecoder)
					{
						await this.SwitchEncodingToUTF8Async().ConfigureAwait(false);
					}
					this.ps.appendMode = false;
					return false;
				}
			}
			if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, 5, "<?xml") && !this.xmlCharType.IsNameSingleChar(this.ps.chars[this.ps.charPos + 5]))
			{
				if (!isTextDecl)
				{
					this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos + 2);
					this.curNode.SetNamedNode(XmlNodeType.XmlDeclaration, this.Xml);
				}
				this.ps.charPos = this.ps.charPos + 5;
				StringBuilder sb = isTextDecl ? new StringBuilder() : this.stringBuilder;
				int xmlDeclState = 0;
				Encoding encoding = null;
				for (;;)
				{
					int originalSbLen = sb.Length;
					int wsCount = await this.EatWhitespacesAsync((xmlDeclState == 0) ? null : sb).ConfigureAwait(false);
					if (this.ps.chars[this.ps.charPos] == '?')
					{
						sb.Length = originalSbLen;
						if (this.ps.chars[this.ps.charPos + 1] == '>')
						{
							break;
						}
						if (this.ps.charPos + 1 == this.ps.charsUsed)
						{
							goto IL_CB1;
						}
						this.ThrowUnexpectedToken("'>'");
					}
					if (wsCount == 0 && xmlDeclState != 0)
					{
						this.ThrowUnexpectedToken("?>");
					}
					int num = await this.ParseNameAsync().ConfigureAwait(false);
					XmlTextReaderImpl.NodeData attr = null;
					char c = this.ps.chars[this.ps.charPos];
					if (c != 'e')
					{
						if (c != 's')
						{
							if (c != 'v' || !XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num - this.ps.charPos, "version") || xmlDeclState != 0)
							{
								goto IL_6A5;
							}
							if (!isTextDecl)
							{
								attr = this.AddAttributeNoChecks("version", 1);
							}
						}
						else
						{
							if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num - this.ps.charPos, "standalone") || (xmlDeclState != 1 && xmlDeclState != 2) || isTextDecl)
							{
								goto IL_6A5;
							}
							if (!isTextDecl)
							{
								attr = this.AddAttributeNoChecks("standalone", 1);
							}
							xmlDeclState = 2;
						}
					}
					else
					{
						if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, num - this.ps.charPos, "encoding") || (xmlDeclState != 1 && (!isTextDecl || xmlDeclState != 0)))
						{
							goto IL_6A5;
						}
						if (!isTextDecl)
						{
							attr = this.AddAttributeNoChecks("encoding", 1);
						}
						xmlDeclState = 1;
					}
					IL_6BF:
					if (!isTextDecl)
					{
						attr.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
					}
					sb.Append(this.ps.chars, this.ps.charPos, num - this.ps.charPos);
					this.ps.charPos = num;
					if (this.ps.chars[this.ps.charPos] != '=')
					{
						await this.EatWhitespacesAsync(sb).ConfigureAwait(false);
						if (this.ps.chars[this.ps.charPos] != '=')
						{
							this.ThrowUnexpectedToken("=");
						}
					}
					sb.Append('=');
					this.ps.charPos = this.ps.charPos + 1;
					char quoteChar = this.ps.chars[this.ps.charPos];
					if (quoteChar != '"' && quoteChar != '\'')
					{
						await this.EatWhitespacesAsync(sb).ConfigureAwait(false);
						quoteChar = this.ps.chars[this.ps.charPos];
						if (quoteChar != '"' && quoteChar != '\'')
						{
							this.ThrowUnexpectedToken("\"", "'");
						}
					}
					sb.Append(quoteChar);
					this.ps.charPos = this.ps.charPos + 1;
					if (!isTextDecl)
					{
						attr.quoteChar = quoteChar;
						attr.SetLineInfo2(this.ps.LineNo, this.ps.LinePos);
					}
					int pos = this.ps.charPos;
					char[] chars;
					for (;;)
					{
						chars = this.ps.chars;
						while ((this.xmlCharType.charProperties[(int)chars[pos]] & 128) != 0)
						{
							pos++;
						}
						if (this.ps.chars[pos] == quoteChar)
						{
							break;
						}
						if (pos != this.ps.charsUsed)
						{
							goto IL_C97;
						}
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						}
						if (configuredTaskAwaiter.GetResult() == 0)
						{
							goto Block_59;
						}
					}
					switch (xmlDeclState)
					{
					case 0:
						if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, pos - this.ps.charPos, "1.0"))
						{
							if (!isTextDecl)
							{
								attr.SetValue(this.ps.chars, this.ps.charPos, pos - this.ps.charPos);
							}
							xmlDeclState = 1;
						}
						else
						{
							this.Throw("Version number '{0}' is invalid.", new string(this.ps.chars, this.ps.charPos, pos - this.ps.charPos));
						}
						break;
					case 1:
					{
						string text = new string(this.ps.chars, this.ps.charPos, pos - this.ps.charPos);
						encoding = this.CheckEncoding(text);
						if (!isTextDecl)
						{
							attr.SetValue(text);
						}
						xmlDeclState = 2;
						break;
					}
					case 2:
						if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, pos - this.ps.charPos, "yes"))
						{
							this.standalone = true;
						}
						else if (XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, pos - this.ps.charPos, "no"))
						{
							this.standalone = false;
						}
						else
						{
							this.Throw("Syntax for an XML declaration is invalid.", this.ps.LineNo, this.ps.LinePos - 1);
						}
						if (!isTextDecl)
						{
							attr.SetValue(this.ps.chars, this.ps.charPos, pos - this.ps.charPos);
						}
						xmlDeclState = 3;
						break;
					}
					sb.Append(chars, this.ps.charPos, pos - this.ps.charPos);
					sb.Append(quoteChar);
					this.ps.charPos = pos + 1;
					continue;
					Block_59:
					this.Throw("There is an unclosed literal string.");
					goto IL_CB1;
					IL_C97:
					this.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
					goto IL_CB1;
					IL_6A5:
					this.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
					goto IL_6BF;
					IL_CB1:
					bool flag = this.ps.isEof;
					if (!flag)
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						}
						flag = (configuredTaskAwaiter.GetResult() == 0);
					}
					if (flag)
					{
						this.Throw("Unexpected end of file has occurred.");
					}
					attr = null;
				}
				if (xmlDeclState == 0)
				{
					this.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
				}
				this.ps.charPos = this.ps.charPos + 2;
				if (!isTextDecl)
				{
					this.curNode.SetValue(sb.ToString());
					sb.Length = 0;
					this.nextParsingFunction = this.parsingFunction;
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ResetAttributesRootLevel;
				}
				if (encoding == null)
				{
					if (isTextDecl)
					{
						this.Throw("Invalid text declaration.");
					}
					if (this.afterResetState)
					{
						string webName2 = this.ps.encoding.WebName;
						if (webName2 != "utf-8" && webName2 != "utf-16" && webName2 != "utf-16BE" && !(this.ps.encoding is Ucs4Encoding))
						{
							this.Throw("'{0}' is an invalid value for the 'encoding' attribute. The encoding cannot be switched after a call to ResetState.", (this.ps.encoding.GetByteCount("A") == 1) ? "UTF-8" : "UTF-16");
						}
					}
					if (this.ps.decoder is SafeAsciiDecoder)
					{
						await this.SwitchEncodingToUTF8Async().ConfigureAwait(false);
					}
				}
				else
				{
					await this.SwitchEncodingAsync(encoding).ConfigureAwait(false);
				}
				this.ps.appendMode = false;
				return true;
			}
			goto IL_D47;
		}

		// Token: 0x06000C5C RID: 3164 RVA: 0x0003E88C File Offset: 0x0003CA8C
		private Task<bool> ParseDocumentContentAsync()
		{
			bool needMoreChars;
			int num;
			char[] chars;
			char c;
			for (;;)
			{
				needMoreChars = false;
				num = this.ps.charPos;
				chars = this.ps.chars;
				if (chars[num] != '<')
				{
					goto IL_24E;
				}
				needMoreChars = true;
				if (this.ps.charsUsed - num < 4)
				{
					break;
				}
				num++;
				c = chars[num];
				if (c != '!')
				{
					if (c != '/')
					{
						goto Block_3;
					}
					this.Throw(num + 1, "Unexpected end tag.");
				}
				else
				{
					num++;
					if (this.ps.charsUsed - num < 2)
					{
						goto Block_5;
					}
					if (chars[num] == '-')
					{
						if (chars[num + 1] == '-')
						{
							goto Block_7;
						}
						this.ThrowUnexpectedToken(num + 1, "-");
					}
					else if (chars[num] == '[')
					{
						if (this.fragmentType != XmlNodeType.Document)
						{
							num++;
							if (this.ps.charsUsed - num < 6)
							{
								goto Block_10;
							}
							if (XmlConvert.StrEqual(chars, num, 6, "CDATA["))
							{
								goto Block_11;
							}
							this.ThrowUnexpectedToken(num, "CDATA[");
						}
						else
						{
							this.Throw(this.ps.charPos, "Data at the root level is invalid.");
						}
					}
					else
					{
						if (this.fragmentType == XmlNodeType.Document || this.fragmentType == XmlNodeType.None)
						{
							goto IL_189;
						}
						if (this.ParseUnexpectedToken(num) == "DOCTYPE")
						{
							this.Throw("Unexpected DTD declaration.");
						}
						else
						{
							this.ThrowUnexpectedToken(num, "<!--", "<[CDATA[");
						}
					}
				}
			}
			return this.ParseDocumentContentAsync_ReadData(needMoreChars);
			Block_3:
			if (c == '?')
			{
				this.ps.charPos = num + 1;
				return this.ParsePIAsync().ContinueBoolTaskFuncWhenFalse(new Func<Task<bool>>(this.ParseDocumentContentAsync));
			}
			if (this.rootElementParsed)
			{
				if (this.fragmentType == XmlNodeType.Document)
				{
					this.Throw(num, "There are multiple root elements.");
				}
				if (this.fragmentType == XmlNodeType.None)
				{
					this.fragmentType = XmlNodeType.Element;
				}
			}
			this.ps.charPos = num;
			this.rootElementParsed = true;
			return this.ParseElementAsync().ReturnTaskBoolWhenFinish(true);
			Block_5:
			return this.ParseDocumentContentAsync_ReadData(needMoreChars);
			Block_7:
			this.ps.charPos = num + 2;
			return this.ParseCommentAsync().ContinueBoolTaskFuncWhenFalse(new Func<Task<bool>>(this.ParseDocumentContentAsync));
			Block_10:
			return this.ParseDocumentContentAsync_ReadData(needMoreChars);
			Block_11:
			this.ps.charPos = num + 6;
			return this.ParseCDataAsync().CallBoolTaskFuncWhenFinish(new Func<Task<bool>>(this.ParseDocumentContentAsync_CData));
			IL_189:
			this.fragmentType = XmlNodeType.Document;
			this.ps.charPos = num;
			return this.ParseDoctypeDeclAsync().ContinueBoolTaskFuncWhenFalse(new Func<Task<bool>>(this.ParseDocumentContentAsync));
			IL_24E:
			if (chars[num] == '&')
			{
				return this.ParseDocumentContentAsync_ParseEntity();
			}
			if (num == this.ps.charsUsed || (this.v1Compat && chars[num] == '\0'))
			{
				return this.ParseDocumentContentAsync_ReadData(needMoreChars);
			}
			if (this.fragmentType == XmlNodeType.Document)
			{
				return this.ParseRootLevelWhitespaceAsync().ContinueBoolTaskFuncWhenFalse(new Func<Task<bool>>(this.ParseDocumentContentAsync));
			}
			return this.ParseDocumentContentAsync_WhiteSpace();
		}

		// Token: 0x06000C5D RID: 3165 RVA: 0x0003EB40 File Offset: 0x0003CD40
		private Task<bool> ParseDocumentContentAsync_CData()
		{
			if (this.fragmentType == XmlNodeType.None)
			{
				this.fragmentType = XmlNodeType.Element;
			}
			return AsyncHelper.DoneTaskTrue;
		}

		// Token: 0x06000C5E RID: 3166 RVA: 0x0003EB58 File Offset: 0x0003CD58
		private async Task<bool> ParseDocumentContentAsync_ParseEntity()
		{
			int charPos = this.ps.charPos;
			bool result;
			if (this.fragmentType == XmlNodeType.Document)
			{
				this.Throw(charPos, "Data at the root level is invalid.");
				result = false;
			}
			else
			{
				if (this.fragmentType == XmlNodeType.None)
				{
					this.fragmentType = XmlNodeType.Element;
				}
				XmlTextReaderImpl.EntityType item = (await this.HandleEntityReferenceAsync(false, XmlTextReaderImpl.EntityExpandType.OnlyGeneral).ConfigureAwait(false)).Item2;
				if (item > XmlTextReaderImpl.EntityType.CharacterNamed)
				{
					if (item == XmlTextReaderImpl.EntityType.Unexpanded)
					{
						if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.EntityReference)
						{
							this.parsingFunction = this.nextParsingFunction;
						}
						await this.ParseEntityReferenceAsync().ConfigureAwait(false);
						result = true;
					}
					else
					{
						result = await this.ParseDocumentContentAsync().ConfigureAwait(false);
					}
				}
				else
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ParseTextAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult())
					{
						result = true;
					}
					else
					{
						result = await this.ParseDocumentContentAsync().ConfigureAwait(false);
					}
				}
			}
			return result;
		}

		// Token: 0x06000C5F RID: 3167 RVA: 0x0003EBA0 File Offset: 0x0003CDA0
		private Task<bool> ParseDocumentContentAsync_WhiteSpace()
		{
			Task<bool> task = this.ParseTextAsync();
			if (!task.IsSuccess())
			{
				return this._ParseDocumentContentAsync_WhiteSpace(task);
			}
			if (task.Result)
			{
				if (this.fragmentType == XmlNodeType.None && this.curNode.type == XmlNodeType.Text)
				{
					this.fragmentType = XmlNodeType.Element;
				}
				return AsyncHelper.DoneTaskTrue;
			}
			return this.ParseDocumentContentAsync();
		}

		// Token: 0x06000C60 RID: 3168 RVA: 0x0003EBF8 File Offset: 0x0003CDF8
		private async Task<bool> _ParseDocumentContentAsync_WhiteSpace(Task<bool> task)
		{
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			bool result;
			if (configuredTaskAwaiter.GetResult())
			{
				if (this.fragmentType == XmlNodeType.None && this.curNode.type == XmlNodeType.Text)
				{
					this.fragmentType = XmlNodeType.Element;
				}
				result = true;
			}
			else
			{
				result = await this.ParseDocumentContentAsync().ConfigureAwait(false);
			}
			return result;
		}

		// Token: 0x06000C61 RID: 3169 RVA: 0x0003EC48 File Offset: 0x0003CE48
		private async Task<bool> ParseDocumentContentAsync_ReadData(bool needMoreChars)
		{
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
			}
			bool result;
			if (configuredTaskAwaiter.GetResult() != 0)
			{
				result = await this.ParseDocumentContentAsync().ConfigureAwait(false);
			}
			else
			{
				if (needMoreChars)
				{
					this.Throw("Data at the root level is invalid.");
				}
				if (this.InEntity)
				{
					if (this.HandleEntityEnd(true))
					{
						this.SetupEndEntityNodeInContent();
						result = true;
					}
					else
					{
						result = await this.ParseDocumentContentAsync().ConfigureAwait(false);
					}
				}
				else
				{
					if (!this.rootElementParsed && this.fragmentType == XmlNodeType.Document)
					{
						this.ThrowWithoutLineInfo("Root element is missing.");
					}
					if (this.fragmentType == XmlNodeType.None)
					{
						this.fragmentType = (this.rootElementParsed ? XmlNodeType.Document : XmlNodeType.Element);
					}
					this.OnEof();
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06000C62 RID: 3170 RVA: 0x0003EC98 File Offset: 0x0003CE98
		private Task<bool> ParseElementContentAsync()
		{
			int num;
			char c;
			for (;;)
			{
				num = this.ps.charPos;
				char[] chars = this.ps.chars;
				c = chars[num];
				if (c == '&')
				{
					goto IL_1B4;
				}
				if (c != '<')
				{
					goto IL_1CC;
				}
				c = chars[num + 1];
				if (c != '!')
				{
					break;
				}
				num += 2;
				if (this.ps.charsUsed - num < 2)
				{
					goto Block_5;
				}
				if (chars[num] == '-')
				{
					if (chars[num + 1] == '-')
					{
						goto Block_7;
					}
					this.ThrowUnexpectedToken(num + 1, "-");
				}
				else if (chars[num] == '[')
				{
					num++;
					if (this.ps.charsUsed - num < 6)
					{
						goto Block_9;
					}
					if (XmlConvert.StrEqual(chars, num, 6, "CDATA["))
					{
						goto Block_10;
					}
					this.ThrowUnexpectedToken(num, "CDATA[");
				}
				else if (this.ParseUnexpectedToken(num) == "DOCTYPE")
				{
					this.Throw("Unexpected DTD declaration.");
				}
				else
				{
					this.ThrowUnexpectedToken(num, "<!--", "<[CDATA[");
				}
			}
			if (c == '/')
			{
				this.ps.charPos = num + 2;
				return this.ParseEndElementAsync().ReturnTaskBoolWhenFinish(true);
			}
			if (c == '?')
			{
				this.ps.charPos = num + 2;
				return this.ParsePIAsync().ContinueBoolTaskFuncWhenFalse(new Func<Task<bool>>(this.ParseElementContentAsync));
			}
			if (num + 1 == this.ps.charsUsed)
			{
				return this.ParseElementContent_ReadData();
			}
			this.ps.charPos = num + 1;
			return this.ParseElementAsync().ReturnTaskBoolWhenFinish(true);
			Block_5:
			return this.ParseElementContent_ReadData();
			Block_7:
			this.ps.charPos = num + 2;
			return this.ParseCommentAsync().ContinueBoolTaskFuncWhenFalse(new Func<Task<bool>>(this.ParseElementContentAsync));
			Block_9:
			return this.ParseElementContent_ReadData();
			Block_10:
			this.ps.charPos = num + 6;
			return this.ParseCDataAsync().ReturnTaskBoolWhenFinish(true);
			IL_1B4:
			return this.ParseTextAsync().ContinueBoolTaskFuncWhenFalse(new Func<Task<bool>>(this.ParseElementContentAsync));
			IL_1CC:
			if (num == this.ps.charsUsed)
			{
				return this.ParseElementContent_ReadData();
			}
			return this.ParseTextAsync().ContinueBoolTaskFuncWhenFalse(new Func<Task<bool>>(this.ParseElementContentAsync));
		}

		// Token: 0x06000C63 RID: 3171 RVA: 0x0003EEA0 File Offset: 0x0003D0A0
		private async Task<bool> ParseElementContent_ReadData()
		{
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() == 0)
			{
				if (this.ps.charsUsed - this.ps.charPos != 0)
				{
					this.ThrowUnclosedElements();
				}
				if (!this.InEntity)
				{
					if (this.index == 0 && this.fragmentType != XmlNodeType.Document)
					{
						this.OnEof();
						return false;
					}
					this.ThrowUnclosedElements();
				}
				if (this.HandleEntityEnd(true))
				{
					this.SetupEndEntityNodeInContent();
					return true;
				}
			}
			return await this.ParseElementContentAsync().ConfigureAwait(false);
		}

		// Token: 0x06000C64 RID: 3172 RVA: 0x0003EEE8 File Offset: 0x0003D0E8
		private Task ParseElementAsync()
		{
			int num = this.ps.charPos;
			char[] chars = this.ps.chars;
			int num2 = -1;
			this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			while ((this.xmlCharType.charProperties[(int)chars[num]] & 4) != 0)
			{
				num++;
				for (;;)
				{
					if ((this.xmlCharType.charProperties[(int)chars[num]] & 8) != 0)
					{
						num++;
					}
					else
					{
						if (chars[num] != ':')
						{
							goto IL_A2;
						}
						if (num2 == -1)
						{
							break;
						}
						if (this.supportNamespaces)
						{
							goto Block_5;
						}
						num++;
					}
				}
				num2 = num;
				num++;
				continue;
				Block_5:
				this.Throw(num, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
				break;
				IL_A2:
				if (num + 1 >= this.ps.charsUsed)
				{
					break;
				}
				return this.ParseElementAsync_SetElement(num2, num);
			}
			Task<Tuple<int, int>> task = this.ParseQNameAsync();
			return this.ParseElementAsync_ContinueWithSetElement(task);
		}

		// Token: 0x06000C65 RID: 3173 RVA: 0x0003EFC0 File Offset: 0x0003D1C0
		private Task ParseElementAsync_ContinueWithSetElement(Task<Tuple<int, int>> task)
		{
			if (task.IsSuccess())
			{
				Tuple<int, int> result = task.Result;
				int item = result.Item1;
				int item2 = result.Item2;
				return this.ParseElementAsync_SetElement(item, item2);
			}
			return this._ParseElementAsync_ContinueWithSetElement(task);
		}

		// Token: 0x06000C66 RID: 3174 RVA: 0x0003EFF8 File Offset: 0x0003D1F8
		private async Task _ParseElementAsync_ContinueWithSetElement(Task<Tuple<int, int>> task)
		{
			object obj = await task.ConfigureAwait(false);
			int item = obj.Item1;
			int item2 = obj.Item2;
			await this.ParseElementAsync_SetElement(item, item2).ConfigureAwait(false);
		}

		// Token: 0x06000C67 RID: 3175 RVA: 0x0003F048 File Offset: 0x0003D248
		private Task ParseElementAsync_SetElement(int colonPos, int pos)
		{
			char[] chars = this.ps.chars;
			this.namespaceManager.PushScope();
			if (colonPos == -1 || !this.supportNamespaces)
			{
				this.curNode.SetNamedNode(XmlNodeType.Element, this.nameTable.Add(chars, this.ps.charPos, pos - this.ps.charPos));
			}
			else
			{
				int charPos = this.ps.charPos;
				int num = colonPos - charPos;
				if (num == this.lastPrefix.Length && XmlConvert.StrEqual(chars, charPos, num, this.lastPrefix))
				{
					this.curNode.SetNamedNode(XmlNodeType.Element, this.nameTable.Add(chars, colonPos + 1, pos - colonPos - 1), this.lastPrefix, null);
				}
				else
				{
					this.curNode.SetNamedNode(XmlNodeType.Element, this.nameTable.Add(chars, colonPos + 1, pos - colonPos - 1), this.nameTable.Add(chars, this.ps.charPos, num), null);
					this.lastPrefix = this.curNode.prefix;
				}
			}
			char c = chars[pos];
			bool flag = (this.xmlCharType.charProperties[(int)c] & 1) > 0;
			this.ps.charPos = pos;
			if (flag)
			{
				return this.ParseAttributesAsync();
			}
			return this.ParseElementAsync_NoAttributes();
		}

		// Token: 0x06000C68 RID: 3176 RVA: 0x0003F180 File Offset: 0x0003D380
		private Task ParseElementAsync_NoAttributes()
		{
			int charPos = this.ps.charPos;
			char[] chars = this.ps.chars;
			char c = chars[charPos];
			if (c == '>')
			{
				this.ps.charPos = charPos + 1;
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.MoveToElementContent;
			}
			else if (c == '/')
			{
				if (charPos + 1 == this.ps.charsUsed)
				{
					this.ps.charPos = charPos;
					return this.ParseElementAsync_ReadData(charPos);
				}
				if (chars[charPos + 1] == '>')
				{
					this.curNode.IsEmptyElement = true;
					this.nextParsingFunction = this.parsingFunction;
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PopEmptyElementContext;
					this.ps.charPos = charPos + 2;
				}
				else
				{
					this.ThrowUnexpectedToken(charPos, ">");
				}
			}
			else
			{
				this.Throw(charPos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(chars, this.ps.charsUsed, charPos));
			}
			if (this.addDefaultAttributesAndNormalize)
			{
				this.AddDefaultAttributesAndNormalize();
			}
			this.ElementNamespaceLookup();
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000C69 RID: 3177 RVA: 0x0003F270 File Offset: 0x0003D470
		private async Task ParseElementAsync_ReadData(int pos)
		{
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() == 0)
			{
				this.Throw(pos, "Unexpected end of file while parsing {0} has occurred.", ">");
			}
			await this.ParseElementAsync_NoAttributes().ConfigureAwait(false);
		}

		// Token: 0x06000C6A RID: 3178 RVA: 0x0003F2C0 File Offset: 0x0003D4C0
		private Task ParseEndElementAsync()
		{
			XmlTextReaderImpl.NodeData nodeData = this.nodes[this.index - 1];
			int length = nodeData.prefix.Length;
			int length2 = nodeData.localName.Length;
			if (this.ps.charsUsed - this.ps.charPos < length + length2 + 1)
			{
				return this._ParseEndElmentAsync();
			}
			return this.ParseEndElementAsync_CheckNameAndParse();
		}

		// Token: 0x06000C6B RID: 3179 RVA: 0x0003F320 File Offset: 0x0003D520
		private async Task _ParseEndElmentAsync()
		{
			await this.ParseEndElmentAsync_PrepareData().ConfigureAwait(false);
			await this.ParseEndElementAsync_CheckNameAndParse().ConfigureAwait(false);
		}

		// Token: 0x06000C6C RID: 3180 RVA: 0x0003F368 File Offset: 0x0003D568
		private async Task ParseEndElmentAsync_PrepareData()
		{
			XmlTextReaderImpl.NodeData nodeData = this.nodes[this.index - 1];
			int prefLen = nodeData.prefix.Length;
			int locLen = nodeData.localName.Length;
			while (this.ps.charsUsed - this.ps.charPos < prefLen + locLen + 1)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					break;
				}
			}
		}

		// Token: 0x06000C6D RID: 3181 RVA: 0x0003F3B0 File Offset: 0x0003D5B0
		private Task ParseEndElementAsync_CheckNameAndParse()
		{
			XmlTextReaderImpl.NodeData nodeData = this.nodes[this.index - 1];
			int length = nodeData.prefix.Length;
			int length2 = nodeData.localName.Length;
			char[] chars = this.ps.chars;
			int nameLen;
			if (nodeData.prefix.Length == 0)
			{
				if (!XmlConvert.StrEqual(chars, this.ps.charPos, length2, nodeData.localName))
				{
					return this.ThrowTagMismatchAsync(nodeData);
				}
				nameLen = length2;
			}
			else
			{
				int num = this.ps.charPos + length;
				if (!XmlConvert.StrEqual(chars, this.ps.charPos, length, nodeData.prefix) || chars[num] != ':' || !XmlConvert.StrEqual(chars, num + 1, length2, nodeData.localName))
				{
					return this.ThrowTagMismatchAsync(nodeData);
				}
				nameLen = length2 + length + 1;
			}
			LineInfo endTagLineInfo = new LineInfo(this.ps.lineNo, this.ps.LinePos);
			return this.ParseEndElementAsync_Finish(nameLen, nodeData, endTagLineInfo);
		}

		// Token: 0x06000C6E RID: 3182 RVA: 0x0003F4A4 File Offset: 0x0003D6A4
		private Task ParseEndElementAsync_Finish(int nameLen, XmlTextReaderImpl.NodeData startTagNode, LineInfo endTagLineInfo)
		{
			Task task = this.ParseEndElementAsync_CheckEndTag(nameLen, startTagNode, endTagLineInfo);
			while (task.IsSuccess())
			{
				switch (this.parseEndElement_NextFunc)
				{
				case XmlTextReaderImpl.ParseEndElementParseFunction.CheckEndTag:
					task = this.ParseEndElementAsync_CheckEndTag(nameLen, startTagNode, endTagLineInfo);
					break;
				case XmlTextReaderImpl.ParseEndElementParseFunction.ReadData:
					task = this.ParseEndElementAsync_ReadData();
					break;
				case XmlTextReaderImpl.ParseEndElementParseFunction.Done:
					return task;
				}
			}
			return this.ParseEndElementAsync_Finish(task, nameLen, startTagNode, endTagLineInfo);
		}

		// Token: 0x06000C6F RID: 3183 RVA: 0x0003F500 File Offset: 0x0003D700
		private async Task ParseEndElementAsync_Finish(Task task, int nameLen, XmlTextReaderImpl.NodeData startTagNode, LineInfo endTagLineInfo)
		{
			for (;;)
			{
				await task.ConfigureAwait(false);
				switch (this.parseEndElement_NextFunc)
				{
				case XmlTextReaderImpl.ParseEndElementParseFunction.CheckEndTag:
					task = this.ParseEndElementAsync_CheckEndTag(nameLen, startTagNode, endTagLineInfo);
					break;
				case XmlTextReaderImpl.ParseEndElementParseFunction.ReadData:
					task = this.ParseEndElementAsync_ReadData();
					break;
				case XmlTextReaderImpl.ParseEndElementParseFunction.Done:
					return;
				}
			}
		}

		// Token: 0x06000C70 RID: 3184 RVA: 0x0003F568 File Offset: 0x0003D768
		private Task ParseEndElementAsync_CheckEndTag(int nameLen, XmlTextReaderImpl.NodeData startTagNode, LineInfo endTagLineInfo)
		{
			int num;
			for (;;)
			{
				num = this.ps.charPos + nameLen;
				char[] chars = this.ps.chars;
				if (num == this.ps.charsUsed)
				{
					break;
				}
				bool flag = false;
				if ((this.xmlCharType.charProperties[(int)chars[num]] & 8) != 0 || chars[num] == ':')
				{
					flag = true;
				}
				if (flag)
				{
					goto Block_2;
				}
				if (chars[num] != '>')
				{
					char c;
					while (this.xmlCharType.IsWhiteSpace(c = chars[num]))
					{
						num++;
						if (c != '\n')
						{
							if (c == '\r')
							{
								if (chars[num] == '\n')
								{
									num++;
								}
								else if (num == this.ps.charsUsed && !this.ps.isEof)
								{
									continue;
								}
								this.OnNewLine(num);
							}
						}
						else
						{
							this.OnNewLine(num);
						}
					}
				}
				if (chars[num] == '>')
				{
					goto IL_F4;
				}
				if (num == this.ps.charsUsed)
				{
					goto Block_9;
				}
				this.ThrowUnexpectedToken(num, ">");
			}
			this.parseEndElement_NextFunc = XmlTextReaderImpl.ParseEndElementParseFunction.ReadData;
			return AsyncHelper.DoneTask;
			Block_2:
			return this.ThrowTagMismatchAsync(startTagNode);
			Block_9:
			this.parseEndElement_NextFunc = XmlTextReaderImpl.ParseEndElementParseFunction.ReadData;
			return AsyncHelper.DoneTask;
			IL_F4:
			this.index--;
			this.curNode = this.nodes[this.index];
			startTagNode.lineInfo = endTagLineInfo;
			startTagNode.type = XmlNodeType.EndElement;
			this.ps.charPos = num + 1;
			this.nextParsingFunction = ((this.index > 0) ? this.parsingFunction : XmlTextReaderImpl.ParsingFunction.DocumentContent);
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PopElementContext;
			this.parseEndElement_NextFunc = XmlTextReaderImpl.ParseEndElementParseFunction.Done;
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000C71 RID: 3185 RVA: 0x0003F6D4 File Offset: 0x0003D8D4
		private async Task ParseEndElementAsync_ReadData()
		{
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() == 0)
			{
				this.ThrowUnclosedElements();
			}
			this.parseEndElement_NextFunc = XmlTextReaderImpl.ParseEndElementParseFunction.CheckEndTag;
		}

		// Token: 0x06000C72 RID: 3186 RVA: 0x0003F71C File Offset: 0x0003D91C
		private async Task ThrowTagMismatchAsync(XmlTextReaderImpl.NodeData startTag)
		{
			if (startTag.type == XmlNodeType.Element)
			{
				object obj = await this.ParseQNameAsync().ConfigureAwait(false);
				int item = obj.Item1;
				int item2 = obj.Item2;
				this.Throw("The '{0}' start tag on line {1} position {2} does not match the end tag of '{3}'.", new string[]
				{
					startTag.GetNameWPrefix(this.nameTable),
					startTag.lineInfo.lineNo.ToString(CultureInfo.InvariantCulture),
					startTag.lineInfo.linePos.ToString(CultureInfo.InvariantCulture),
					new string(this.ps.chars, this.ps.charPos, item2 - this.ps.charPos)
				});
			}
			else
			{
				this.Throw("Unexpected end tag.");
			}
		}

		// Token: 0x06000C73 RID: 3187 RVA: 0x0003F76C File Offset: 0x0003D96C
		private async Task ParseAttributesAsync()
		{
			int pos = this.ps.charPos;
			char[] chars = this.ps.chars;
			XmlTextReaderImpl.NodeData attr = null;
			for (;;)
			{
				IL_55:
				int lineNoDelta = 0;
				char tmpch0;
				int num;
				while ((this.xmlCharType.charProperties[(int)(tmpch0 = chars[pos])] & 1) != 0)
				{
					if (tmpch0 == '\n')
					{
						this.OnNewLine(pos + 1);
						num = lineNoDelta;
						lineNoDelta = num + 1;
					}
					else if (tmpch0 == '\r')
					{
						if (chars[pos + 1] == '\n')
						{
							this.OnNewLine(pos + 2);
							num = lineNoDelta;
							lineNoDelta = num + 1;
							num = pos;
							pos = num + 1;
						}
						else if (pos + 1 != this.ps.charsUsed)
						{
							this.OnNewLine(pos + 1);
							num = lineNoDelta;
							lineNoDelta = num + 1;
						}
						else
						{
							this.ps.charPos = pos;
							IL_8D7:
							this.ps.lineNo = this.ps.lineNo - lineNoDelta;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								await configuredTaskAwaiter;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							}
							if (configuredTaskAwaiter.GetResult() != 0)
							{
								pos = this.ps.charPos;
								chars = this.ps.chars;
								goto IL_55;
							}
							this.ThrowUnclosedElements();
							goto IL_55;
						}
					}
					num = pos;
					pos = num + 1;
				}
				int num2 = 0;
				char c;
				if ((this.xmlCharType.charProperties[(int)(c = chars[pos])] & 4) != 0)
				{
					num2 = 1;
				}
				if (num2 == 0)
				{
					if (c == '>')
					{
						break;
					}
					if (c == '/')
					{
						if (pos + 1 == this.ps.charsUsed)
						{
							goto IL_8D7;
						}
						if (chars[pos + 1] == '>')
						{
							goto Block_11;
						}
						this.ThrowUnexpectedToken(pos + 1, ">");
					}
					else
					{
						if (pos == this.ps.charsUsed)
						{
							goto IL_8D7;
						}
						if (c != ':' || this.supportNamespaces)
						{
							this.Throw(pos, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(chars, this.ps.charsUsed, pos));
						}
					}
				}
				if (pos == this.ps.charPos)
				{
					this.ThrowExpectingWhitespace(pos);
				}
				this.ps.charPos = pos;
				int attrNameLinePos = this.ps.LinePos;
				int num3 = -1;
				pos += num2;
				for (;;)
				{
					char tmpch;
					if ((this.xmlCharType.charProperties[(int)(tmpch = chars[pos])] & 8) != 0)
					{
						num = pos;
						pos = num + 1;
					}
					else
					{
						if (tmpch != ':')
						{
							goto IL_448;
						}
						if (num3 != -1)
						{
							if (this.supportNamespaces)
							{
								goto Block_18;
							}
							num = pos;
							pos = num + 1;
						}
						else
						{
							num3 = pos;
							num = pos;
							pos = num + 1;
							if ((this.xmlCharType.charProperties[(int)chars[pos]] & 4) == 0)
							{
								goto IL_3B2;
							}
							num = pos;
							pos = num + 1;
						}
					}
				}
				IL_4F1:
				attr = this.AddAttribute(pos, num3);
				attr.SetLineInfo(this.ps.LineNo, attrNameLinePos);
				if (chars[pos] != '=')
				{
					this.ps.charPos = pos;
					await this.EatWhitespacesAsync(null).ConfigureAwait(false);
					pos = this.ps.charPos;
					if (chars[pos] != '=')
					{
						this.ThrowUnexpectedToken("=");
					}
				}
				num = pos;
				pos = num + 1;
				char c2 = chars[pos];
				if (c2 != '"' && c2 != '\'')
				{
					this.ps.charPos = pos;
					await this.EatWhitespacesAsync(null).ConfigureAwait(false);
					pos = this.ps.charPos;
					c2 = chars[pos];
					if (c2 != '"' && c2 != '\'')
					{
						this.ThrowUnexpectedToken("\"", "'");
					}
				}
				num = pos;
				pos = num + 1;
				this.ps.charPos = pos;
				attr.quoteChar = c2;
				attr.SetLineInfo2(this.ps.LineNo, this.ps.LinePos);
				char c3;
				while ((this.xmlCharType.charProperties[(int)(c3 = chars[pos])] & 128) != 0)
				{
					num = pos;
					pos = num + 1;
				}
				if (c3 == c2)
				{
					attr.SetValue(chars, this.ps.charPos, pos - this.ps.charPos);
					num = pos;
					pos = num + 1;
					this.ps.charPos = pos;
				}
				else
				{
					await this.ParseAttributeValueSlowAsync(pos, c2, attr).ConfigureAwait(false);
					pos = this.ps.charPos;
					chars = this.ps.chars;
				}
				if (attr.prefix.Length == 0)
				{
					if (Ref.Equal(attr.localName, this.XmlNs))
					{
						this.OnDefaultNamespaceDecl(attr);
						continue;
					}
					continue;
				}
				else
				{
					if (Ref.Equal(attr.prefix, this.XmlNs))
					{
						this.OnNamespaceDecl(attr);
						continue;
					}
					if (Ref.Equal(attr.prefix, this.Xml))
					{
						this.OnXmlReservedAttribute(attr);
						continue;
					}
					continue;
				}
				Block_18:
				this.Throw(pos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
				goto IL_4F1;
				IL_3B2:
				Tuple<int, int> tuple = await this.ParseQNameAsync().ConfigureAwait(false);
				num3 = tuple.Item1;
				pos = tuple.Item2;
				chars = this.ps.chars;
				goto IL_4F1;
				IL_448:
				if (pos + 1 >= this.ps.charsUsed)
				{
					Tuple<int, int> tuple2 = await this.ParseQNameAsync().ConfigureAwait(false);
					num3 = tuple2.Item1;
					pos = tuple2.Item2;
					chars = this.ps.chars;
					goto IL_4F1;
				}
				goto IL_4F1;
			}
			this.ps.charPos = pos + 1;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.MoveToElementContent;
			goto IL_988;
			Block_11:
			this.ps.charPos = pos + 2;
			this.curNode.IsEmptyElement = true;
			this.nextParsingFunction = this.parsingFunction;
			this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PopEmptyElementContext;
			IL_988:
			if (this.addDefaultAttributesAndNormalize)
			{
				this.AddDefaultAttributesAndNormalize();
			}
			this.ElementNamespaceLookup();
			if (this.attrNeedNamespaceLookup)
			{
				this.AttributeNamespaceLookup();
				this.attrNeedNamespaceLookup = false;
			}
			if (this.attrDuplWalkCount >= 250)
			{
				this.AttributeDuplCheck();
			}
		}

		// Token: 0x06000C74 RID: 3188 RVA: 0x0003F7B4 File Offset: 0x0003D9B4
		private async Task ParseAttributeValueSlowAsync(int curPos, char quoteChar, XmlTextReaderImpl.NodeData attr)
		{
			int pos = curPos;
			char[] chars = this.ps.chars;
			int attributeBaseEntityId = this.ps.entityId;
			int valueChunkStartPos = 0;
			LineInfo valueChunkLineInfo = new LineInfo(this.ps.lineNo, this.ps.LinePos);
			XmlTextReaderImpl.NodeData lastChunk = null;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)chars[pos]] & 128) == 0)
				{
					if (pos - this.ps.charPos > 0)
					{
						this.stringBuilder.Append(chars, this.ps.charPos, pos - this.ps.charPos);
						this.ps.charPos = pos;
					}
					if (chars[pos] == quoteChar && attributeBaseEntityId == this.ps.entityId)
					{
						goto IL_994;
					}
					char c = chars[pos];
					int num;
					if (c <= '&')
					{
						switch (c)
						{
						case '\t':
							num = pos;
							pos = num + 1;
							if (this.normalize)
							{
								this.stringBuilder.Append(' ');
								this.ps.charPos = this.ps.charPos + 1;
								continue;
							}
							continue;
						case '\n':
							num = pos;
							pos = num + 1;
							this.OnNewLine(pos);
							if (this.normalize)
							{
								this.stringBuilder.Append(' ');
								this.ps.charPos = this.ps.charPos + 1;
								continue;
							}
							continue;
						case '\v':
						case '\f':
							goto IL_79F;
						case '\r':
							if (chars[pos + 1] == '\n')
							{
								pos += 2;
								if (this.normalize)
								{
									this.stringBuilder.Append(this.ps.eolNormalized ? "  " : " ");
									this.ps.charPos = pos;
								}
							}
							else
							{
								if (pos + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_822;
								}
								num = pos;
								pos = num + 1;
								if (this.normalize)
								{
									this.stringBuilder.Append(' ');
									this.ps.charPos = pos;
								}
							}
							this.OnNewLine(pos);
							continue;
						default:
							if (c != '"')
							{
								if (c != '&')
								{
									goto IL_79F;
								}
								if (pos - this.ps.charPos > 0)
								{
									this.stringBuilder.Append(chars, this.ps.charPos, pos - this.ps.charPos);
								}
								this.ps.charPos = pos;
								int enclosingEntityId = this.ps.entityId;
								LineInfo entityLineInfo = new LineInfo(this.ps.lineNo, this.ps.LinePos + 1);
								Tuple<int, XmlTextReaderImpl.EntityType> tuple = await this.HandleEntityReferenceAsync(true, XmlTextReaderImpl.EntityExpandType.All).ConfigureAwait(false);
								pos = tuple.Item1;
								switch (tuple.Item2)
								{
								case XmlTextReaderImpl.EntityType.CharacterDec:
								case XmlTextReaderImpl.EntityType.CharacterHex:
								case XmlTextReaderImpl.EntityType.CharacterNamed:
									break;
								case XmlTextReaderImpl.EntityType.Expanded:
								case XmlTextReaderImpl.EntityType.Skipped:
								case XmlTextReaderImpl.EntityType.FakeExpanded:
									goto IL_77D;
								case XmlTextReaderImpl.EntityType.Unexpanded:
									if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full && this.ps.entityId == attributeBaseEntityId)
									{
										int num2 = this.stringBuilder.Length - valueChunkStartPos;
										if (num2 > 0)
										{
											XmlTextReaderImpl.NodeData nodeData = new XmlTextReaderImpl.NodeData();
											nodeData.lineInfo = valueChunkLineInfo;
											nodeData.depth = attr.depth + 1;
											nodeData.SetValueNode(XmlNodeType.Text, this.stringBuilder.ToString(valueChunkStartPos, num2));
											this.AddAttributeChunkToList(attr, nodeData, ref lastChunk);
										}
										this.ps.charPos = this.ps.charPos + 1;
										string text = await this.ParseEntityNameAsync().ConfigureAwait(false);
										XmlTextReaderImpl.NodeData nodeData2 = new XmlTextReaderImpl.NodeData();
										nodeData2.lineInfo = entityLineInfo;
										nodeData2.depth = attr.depth + 1;
										nodeData2.SetNamedNode(XmlNodeType.EntityReference, text);
										this.AddAttributeChunkToList(attr, nodeData2, ref lastChunk);
										this.stringBuilder.Append('&');
										this.stringBuilder.Append(text);
										this.stringBuilder.Append(';');
										valueChunkStartPos = this.stringBuilder.Length;
										valueChunkLineInfo.Set(this.ps.LineNo, this.ps.LinePos);
										this.fullAttrCleanup = true;
									}
									else
									{
										this.ps.charPos = this.ps.charPos + 1;
										await this.ParseEntityNameAsync().ConfigureAwait(false);
									}
									pos = this.ps.charPos;
									break;
								case XmlTextReaderImpl.EntityType.ExpandedInAttribute:
									if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full && enclosingEntityId == attributeBaseEntityId)
									{
										int num3 = this.stringBuilder.Length - valueChunkStartPos;
										if (num3 > 0)
										{
											XmlTextReaderImpl.NodeData nodeData3 = new XmlTextReaderImpl.NodeData();
											nodeData3.lineInfo = valueChunkLineInfo;
											nodeData3.depth = attr.depth + 1;
											nodeData3.SetValueNode(XmlNodeType.Text, this.stringBuilder.ToString(valueChunkStartPos, num3));
											this.AddAttributeChunkToList(attr, nodeData3, ref lastChunk);
										}
										XmlTextReaderImpl.NodeData nodeData4 = new XmlTextReaderImpl.NodeData();
										nodeData4.lineInfo = entityLineInfo;
										nodeData4.depth = attr.depth + 1;
										nodeData4.SetNamedNode(XmlNodeType.EntityReference, this.ps.entity.Name);
										this.AddAttributeChunkToList(attr, nodeData4, ref lastChunk);
										this.fullAttrCleanup = true;
									}
									pos = this.ps.charPos;
									break;
								default:
									goto IL_77D;
								}
								IL_78E:
								chars = this.ps.chars;
								continue;
								IL_77D:
								pos = this.ps.charPos;
								goto IL_78E;
							}
							break;
						}
					}
					else if (c != '\'')
					{
						if (c == '<')
						{
							this.Throw(pos, "'{0}', hexadecimal value {1}, is an invalid attribute character.", XmlException.BuildCharExceptionArgs('<', '\0'));
							goto IL_822;
						}
						if (c != '>')
						{
							goto IL_79F;
						}
					}
					num = pos;
					pos = num + 1;
					continue;
					IL_79F:
					if (pos != this.ps.charsUsed)
					{
						if (XmlCharType.IsHighSurrogate((int)chars[pos]))
						{
							if (pos + 1 == this.ps.charsUsed)
							{
								goto IL_822;
							}
							num = pos;
							pos = num + 1;
							if (XmlCharType.IsLowSurrogate((int)chars[pos]))
							{
								num = pos;
								pos = num + 1;
								continue;
							}
						}
						this.ThrowInvalidChar(chars, this.ps.charsUsed, pos);
					}
					IL_822:
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == 0)
					{
						if (this.ps.charsUsed - this.ps.charPos > 0)
						{
							if (this.ps.chars[this.ps.charPos] != '\r')
							{
								this.Throw("Unexpected end of file has occurred.");
							}
						}
						else
						{
							if (!this.InEntity)
							{
								if (this.fragmentType == XmlNodeType.Attribute)
								{
									break;
								}
								this.Throw("There is an unclosed literal string.");
							}
							if (this.HandleEntityEnd(true))
							{
								this.Throw("An internal error has occurred.");
							}
							if (attributeBaseEntityId == this.ps.entityId)
							{
								valueChunkStartPos = this.stringBuilder.Length;
								valueChunkLineInfo.Set(this.ps.LineNo, this.ps.LinePos);
							}
						}
					}
					pos = this.ps.charPos;
					chars = this.ps.chars;
				}
				else
				{
					int num = pos;
					pos = num + 1;
				}
			}
			if (attributeBaseEntityId != this.ps.entityId)
			{
				this.Throw("Entity replacement text must nest properly within markup declarations.");
			}
			IL_994:
			if (attr.nextAttrValueChunk != null)
			{
				int num4 = this.stringBuilder.Length - valueChunkStartPos;
				if (num4 > 0)
				{
					XmlTextReaderImpl.NodeData nodeData5 = new XmlTextReaderImpl.NodeData();
					nodeData5.lineInfo = valueChunkLineInfo;
					nodeData5.depth = attr.depth + 1;
					nodeData5.SetValueNode(XmlNodeType.Text, this.stringBuilder.ToString(valueChunkStartPos, num4));
					this.AddAttributeChunkToList(attr, nodeData5, ref lastChunk);
				}
			}
			this.ps.charPos = pos + 1;
			attr.SetValue(this.stringBuilder.ToString());
			this.stringBuilder.Length = 0;
		}

		// Token: 0x06000C75 RID: 3189 RVA: 0x0003F814 File Offset: 0x0003DA14
		private Task<bool> ParseTextAsync()
		{
			int num = 0;
			if (this.parsingMode != XmlTextReaderImpl.ParsingMode.Full)
			{
				return this._ParseTextAsync(null);
			}
			this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			Task<Tuple<int, int, int, bool>> task = this.ParseTextAsync(num);
			if (!task.IsSuccess())
			{
				return this._ParseTextAsync(task);
			}
			Tuple<int, int, int, bool> result = task.Result;
			int item = result.Item1;
			int item2 = result.Item2;
			num = result.Item3;
			bool item3 = result.Item4;
			if (!item3)
			{
				return this._ParseTextAsync(task);
			}
			if (item2 - item == 0)
			{
				return this.ParseTextAsync_IgnoreNode();
			}
			XmlNodeType textNodeType = this.GetTextNodeType(num);
			if (textNodeType == XmlNodeType.None)
			{
				return this.ParseTextAsync_IgnoreNode();
			}
			this.curNode.SetValueNode(textNodeType, this.ps.chars, item, item2 - item);
			return AsyncHelper.DoneTaskTrue;
		}

		// Token: 0x06000C76 RID: 3190 RVA: 0x0003F8E0 File Offset: 0x0003DAE0
		private async Task<bool> _ParseTextAsync(Task<Tuple<int, int, int, bool>> parseTask)
		{
			int num = 0;
			int item;
			int item2;
			if (parseTask == null)
			{
				if (this.parsingMode != XmlTextReaderImpl.ParsingMode.Full)
				{
					Tuple<int, int, int, bool> tuple;
					do
					{
						tuple = await this.ParseTextAsync(num).ConfigureAwait(false);
						item = tuple.Item1;
						item2 = tuple.Item2;
						num = tuple.Item3;
					}
					while (!tuple.Item4);
					goto IL_560;
				}
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
				parseTask = this.ParseTextAsync(num);
			}
			object obj = await parseTask.ConfigureAwait(false);
			item = obj.Item1;
			item2 = obj.Item2;
			num = obj.Item3;
			if (obj.Item4)
			{
				if (item2 - item != 0)
				{
					XmlNodeType textNodeType = this.GetTextNodeType(num);
					if (textNodeType != XmlNodeType.None)
					{
						this.curNode.SetValueNode(textNodeType, this.ps.chars, item, item2 - item);
						return true;
					}
				}
			}
			else if (this.v1Compat)
			{
				Tuple<int, int, int, bool> tuple2;
				do
				{
					if (item2 - item > 0)
					{
						this.stringBuilder.Append(this.ps.chars, item, item2 - item);
					}
					tuple2 = await this.ParseTextAsync(num).ConfigureAwait(false);
					item = tuple2.Item1;
					item2 = tuple2.Item2;
					num = tuple2.Item3;
				}
				while (!tuple2.Item4);
				if (item2 - item > 0)
				{
					this.stringBuilder.Append(this.ps.chars, item, item2 - item);
				}
				XmlNodeType textNodeType2 = this.GetTextNodeType(num);
				if (textNodeType2 != XmlNodeType.None)
				{
					this.curNode.SetValueNode(textNodeType2, this.stringBuilder.ToString());
					this.stringBuilder.Length = 0;
					return true;
				}
				this.stringBuilder.Length = 0;
			}
			else
			{
				bool fullValue = false;
				if (num > 32)
				{
					this.curNode.SetValueNode(XmlNodeType.Text, this.ps.chars, item, item2 - item);
					this.nextParsingFunction = this.parsingFunction;
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PartialTextValue;
					return true;
				}
				if (item2 - item > 0)
				{
					this.stringBuilder.Append(this.ps.chars, item, item2 - item);
				}
				do
				{
					Tuple<int, int, int, bool> tuple3 = await this.ParseTextAsync(num).ConfigureAwait(false);
					item = tuple3.Item1;
					item2 = tuple3.Item2;
					num = tuple3.Item3;
					fullValue = tuple3.Item4;
					if (item2 - item > 0)
					{
						this.stringBuilder.Append(this.ps.chars, item, item2 - item);
					}
				}
				while (!fullValue && num <= 32 && this.stringBuilder.Length < 4096);
				XmlNodeType nodeType = (this.stringBuilder.Length < 4096) ? this.GetTextNodeType(num) : XmlNodeType.Text;
				if (nodeType != XmlNodeType.None)
				{
					this.curNode.SetValueNode(nodeType, this.stringBuilder.ToString());
					this.stringBuilder.Length = 0;
					if (!fullValue)
					{
						this.nextParsingFunction = this.parsingFunction;
						this.parsingFunction = XmlTextReaderImpl.ParsingFunction.PartialTextValue;
					}
					return true;
				}
				this.stringBuilder.Length = 0;
				if (!fullValue)
				{
					Tuple<int, int, int, bool> tuple4;
					do
					{
						tuple4 = await this.ParseTextAsync(num).ConfigureAwait(false);
						item = tuple4.Item1;
						item2 = tuple4.Item2;
						num = tuple4.Item3;
					}
					while (!tuple4.Item4);
				}
			}
			IL_560:
			return await this.ParseTextAsync_IgnoreNode().ConfigureAwait(false);
		}

		// Token: 0x06000C77 RID: 3191 RVA: 0x0003F930 File Offset: 0x0003DB30
		private Task<bool> ParseTextAsync_IgnoreNode()
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.ReportEndEntity)
			{
				this.SetupEndEntityNodeInContent();
				this.parsingFunction = this.nextParsingFunction;
				return AsyncHelper.DoneTaskTrue;
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.EntityReference)
			{
				this.parsingFunction = this.nextNextParsingFunction;
				return this.ParseEntityReferenceAsync().ReturnTaskBoolWhenFinish(true);
			}
			return AsyncHelper.DoneTaskFalse;
		}

		// Token: 0x06000C78 RID: 3192 RVA: 0x0003F988 File Offset: 0x0003DB88
		private Task<Tuple<int, int, int, bool>> ParseTextAsync(int outOrChars)
		{
			Task<Tuple<int, int, int, bool>> task = this.ParseTextAsync(outOrChars, this.ps.chars, this.ps.charPos, 0, -1, outOrChars, '\0');
			while (task.IsSuccess())
			{
				outOrChars = this.lastParseTextState.outOrChars;
				char[] chars = this.lastParseTextState.chars;
				int pos = this.lastParseTextState.pos;
				int rcount = this.lastParseTextState.rcount;
				int rpos = this.lastParseTextState.rpos;
				int orChars = this.lastParseTextState.orChars;
				char c = this.lastParseTextState.c;
				switch (this.parseText_NextFunction)
				{
				case XmlTextReaderImpl.ParseTextFunction.ParseText:
					task = this.ParseTextAsync(outOrChars, chars, pos, rcount, rpos, orChars, c);
					break;
				case XmlTextReaderImpl.ParseTextFunction.Entity:
					task = this.ParseTextAsync_ParseEntity(outOrChars, chars, pos, rcount, rpos, orChars, c);
					break;
				case XmlTextReaderImpl.ParseTextFunction.Surrogate:
					task = this.ParseTextAsync_Surrogate(outOrChars, chars, pos, rcount, rpos, orChars, c);
					break;
				case XmlTextReaderImpl.ParseTextFunction.ReadData:
					task = this.ParseTextAsync_ReadData(outOrChars, chars, pos, rcount, rpos, orChars, c);
					break;
				case XmlTextReaderImpl.ParseTextFunction.NoValue:
					return this.ParseTextAsync_NoValue(outOrChars, pos);
				case XmlTextReaderImpl.ParseTextFunction.PartialValue:
					return this.ParseTextAsync_PartialValue(pos, rcount, rpos, orChars, c);
				}
			}
			return this.ParseTextAsync_AsyncFunc(task);
		}

		// Token: 0x06000C79 RID: 3193 RVA: 0x0003FABC File Offset: 0x0003DCBC
		private async Task<Tuple<int, int, int, bool>> ParseTextAsync_AsyncFunc(Task<Tuple<int, int, int, bool>> task)
		{
			int outOrChars;
			int pos;
			int rcount;
			int rpos;
			int orChars;
			char c;
			for (;;)
			{
				await task.ConfigureAwait(false);
				outOrChars = this.lastParseTextState.outOrChars;
				char[] chars = this.lastParseTextState.chars;
				pos = this.lastParseTextState.pos;
				rcount = this.lastParseTextState.rcount;
				rpos = this.lastParseTextState.rpos;
				orChars = this.lastParseTextState.orChars;
				c = this.lastParseTextState.c;
				switch (this.parseText_NextFunction)
				{
				case XmlTextReaderImpl.ParseTextFunction.ParseText:
					task = this.ParseTextAsync(outOrChars, chars, pos, rcount, rpos, orChars, c);
					break;
				case XmlTextReaderImpl.ParseTextFunction.Entity:
					task = this.ParseTextAsync_ParseEntity(outOrChars, chars, pos, rcount, rpos, orChars, c);
					break;
				case XmlTextReaderImpl.ParseTextFunction.Surrogate:
					task = this.ParseTextAsync_Surrogate(outOrChars, chars, pos, rcount, rpos, orChars, c);
					break;
				case XmlTextReaderImpl.ParseTextFunction.ReadData:
					task = this.ParseTextAsync_ReadData(outOrChars, chars, pos, rcount, rpos, orChars, c);
					break;
				case XmlTextReaderImpl.ParseTextFunction.NoValue:
					goto IL_1EB;
				case XmlTextReaderImpl.ParseTextFunction.PartialValue:
					goto IL_260;
				}
			}
			IL_1EB:
			return await this.ParseTextAsync_NoValue(outOrChars, pos).ConfigureAwait(false);
			IL_260:
			return await this.ParseTextAsync_PartialValue(pos, rcount, rpos, orChars, c).ConfigureAwait(false);
		}

		// Token: 0x06000C7A RID: 3194 RVA: 0x0003FB0C File Offset: 0x0003DD0C
		private Task<Tuple<int, int, int, bool>> ParseTextAsync(int outOrChars, char[] chars, int pos, int rcount, int rpos, int orChars, char c)
		{
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)(c = chars[pos])] & 64) == 0)
				{
					if (c <= '&')
					{
						switch (c)
						{
						case '\t':
							pos++;
							continue;
						case '\n':
							pos++;
							this.OnNewLine(pos);
							continue;
						case '\v':
						case '\f':
							goto IL_214;
						case '\r':
							if (chars[pos + 1] == '\n')
							{
								if (!this.ps.eolNormalized && this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
								{
									if (pos - this.ps.charPos > 0)
									{
										if (rcount == 0)
										{
											rcount = 1;
											rpos = pos;
										}
										else
										{
											this.ShiftBuffer(rpos + rcount, rpos, pos - rpos - rcount);
											rpos = pos - rcount;
											rcount++;
										}
									}
									else
									{
										this.ps.charPos = this.ps.charPos + 1;
									}
								}
								pos += 2;
							}
							else
							{
								if (pos + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_12C;
								}
								if (!this.ps.eolNormalized)
								{
									chars[pos] = '\n';
								}
								pos++;
							}
							this.OnNewLine(pos);
							continue;
						}
						break;
					}
					if (c == '<')
					{
						goto IL_15C;
					}
					if (c != ']')
					{
						goto Block_6;
					}
					if (this.ps.charsUsed - pos < 3 && !this.ps.isEof)
					{
						goto Block_15;
					}
					if (chars[pos + 1] == ']' && chars[pos + 2] == '>')
					{
						this.Throw(pos, "']]>' is not allowed in character data.");
					}
					orChars |= 93;
					pos++;
				}
				else
				{
					orChars |= (int)c;
					pos++;
				}
			}
			if (c == '&')
			{
				this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
				this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.Entity;
				return this.parseText_dummyTask;
			}
			Block_6:
			goto IL_214;
			IL_12C:
			this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
			this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ReadData;
			return this.parseText_dummyTask;
			IL_15C:
			this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
			this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.PartialValue;
			return this.parseText_dummyTask;
			Block_15:
			this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
			this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ReadData;
			return this.parseText_dummyTask;
			IL_214:
			if (pos == this.ps.charsUsed)
			{
				this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
				this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ReadData;
				return this.parseText_dummyTask;
			}
			this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
			this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.Surrogate;
			return this.parseText_dummyTask;
		}

		// Token: 0x06000C7B RID: 3195 RVA: 0x0003FD84 File Offset: 0x0003DF84
		private async Task<Tuple<int, int, int, bool>> ParseTextAsync_ParseEntity(int outOrChars, char[] chars, int pos, int rcount, int rpos, int orChars, char c)
		{
			int num2;
			XmlTextReaderImpl.EntityType entityType;
			int num;
			if ((num = this.ParseCharRefInline(pos, out num2, out entityType)) > 0)
			{
				if (rcount > 0)
				{
					this.ShiftBuffer(rpos + rcount, rpos, pos - rpos - rcount);
				}
				rpos = pos - rcount;
				rcount += num - pos - num2;
				pos = num;
				if (!this.xmlCharType.IsWhiteSpace(chars[num - num2]) || (this.v1Compat && entityType == XmlTextReaderImpl.EntityType.CharacterDec))
				{
					orChars |= 255;
				}
			}
			else
			{
				if (pos > this.ps.charPos)
				{
					this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
					this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.PartialValue;
					return this.parseText_dummyTask.Result;
				}
				Tuple<int, XmlTextReaderImpl.EntityType> tuple = await this.HandleEntityReferenceAsync(false, XmlTextReaderImpl.EntityExpandType.All).ConfigureAwait(false);
				pos = tuple.Item1;
				switch (tuple.Item2)
				{
				case XmlTextReaderImpl.EntityType.CharacterDec:
					if (this.v1Compat)
					{
						orChars |= 255;
						goto IL_2A2;
					}
					break;
				case XmlTextReaderImpl.EntityType.CharacterHex:
				case XmlTextReaderImpl.EntityType.CharacterNamed:
					break;
				case XmlTextReaderImpl.EntityType.Expanded:
				case XmlTextReaderImpl.EntityType.Skipped:
				case XmlTextReaderImpl.EntityType.FakeExpanded:
					goto IL_291;
				case XmlTextReaderImpl.EntityType.Unexpanded:
					this.nextParsingFunction = this.parsingFunction;
					this.parsingFunction = XmlTextReaderImpl.ParsingFunction.EntityReference;
					this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
					this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.NoValue;
					return this.parseText_dummyTask.Result;
				default:
					goto IL_291;
				}
				if (!this.xmlCharType.IsWhiteSpace(this.ps.chars[pos - 1]))
				{
					orChars |= 255;
					goto IL_2A2;
				}
				goto IL_2A2;
				IL_291:
				pos = this.ps.charPos;
				IL_2A2:
				chars = this.ps.chars;
			}
			this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
			this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ParseText;
			return this.parseText_dummyTask.Result;
		}

		// Token: 0x06000C7C RID: 3196 RVA: 0x0003FE08 File Offset: 0x0003E008
		private async Task<Tuple<int, int, int, bool>> ParseTextAsync_Surrogate(int outOrChars, char[] chars, int pos, int rcount, int rpos, int orChars, char c)
		{
			char c2 = chars[pos];
			if (XmlCharType.IsHighSurrogate((int)c2))
			{
				if (pos + 1 == this.ps.charsUsed)
				{
					this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
					this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ReadData;
					return this.parseText_dummyTask.Result;
				}
				int num = pos;
				pos = num + 1;
				if (XmlCharType.IsLowSurrogate((int)chars[pos]))
				{
					num = pos;
					pos = num + 1;
					orChars |= (int)c2;
					this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
					this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ParseText;
					return this.parseText_dummyTask.Result;
				}
			}
			int offset = pos - this.ps.charPos;
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ZeroEndingStreamAsync(pos).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			if (!configuredTaskAwaiter.GetResult())
			{
				this.ThrowInvalidChar(this.ps.chars, this.ps.charsUsed, this.ps.charPos + offset);
				throw new Exception();
			}
			chars = this.ps.chars;
			pos = this.ps.charPos + offset;
			this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
			this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.PartialValue;
			return this.parseText_dummyTask.Result;
		}

		// Token: 0x06000C7D RID: 3197 RVA: 0x0003FE8C File Offset: 0x0003E08C
		private async Task<Tuple<int, int, int, bool>> ParseTextAsync_ReadData(int outOrChars, char[] chars, int pos, int rcount, int rpos, int orChars, char c)
		{
			Tuple<int, int, int, bool> result;
			if (pos > this.ps.charPos)
			{
				this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
				this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.PartialValue;
				result = this.parseText_dummyTask.Result;
			}
			else
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					if (this.ps.charsUsed - this.ps.charPos > 0)
					{
						if (this.ps.chars[this.ps.charPos] != '\r' && this.ps.chars[this.ps.charPos] != ']')
						{
							this.Throw("Unexpected end of file has occurred.");
						}
					}
					else
					{
						if (!this.InEntity)
						{
							this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
							this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.NoValue;
							return this.parseText_dummyTask.Result;
						}
						if (this.HandleEntityEnd(true))
						{
							this.nextParsingFunction = this.parsingFunction;
							this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ReportEndEntity;
							this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
							this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.NoValue;
							return this.parseText_dummyTask.Result;
						}
					}
				}
				pos = this.ps.charPos;
				chars = this.ps.chars;
				this.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
				this.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ParseText;
				result = this.parseText_dummyTask.Result;
			}
			return result;
		}

		// Token: 0x06000C7E RID: 3198 RVA: 0x0003FF0D File Offset: 0x0003E10D
		private Task<Tuple<int, int, int, bool>> ParseTextAsync_NoValue(int outOrChars, int pos)
		{
			return Task.FromResult<Tuple<int, int, int, bool>>(new Tuple<int, int, int, bool>(pos, pos, outOrChars, true));
		}

		// Token: 0x06000C7F RID: 3199 RVA: 0x0003FF20 File Offset: 0x0003E120
		private Task<Tuple<int, int, int, bool>> ParseTextAsync_PartialValue(int pos, int rcount, int rpos, int orChars, char c)
		{
			if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full && rcount > 0)
			{
				this.ShiftBuffer(rpos + rcount, rpos, pos - rpos - rcount);
			}
			int charPos = this.ps.charPos;
			int item = pos - rcount;
			this.ps.charPos = pos;
			return Task.FromResult<Tuple<int, int, int, bool>>(new Tuple<int, int, int, bool>(charPos, item, orChars, c == '<'));
		}

		// Token: 0x06000C80 RID: 3200 RVA: 0x0003FF78 File Offset: 0x0003E178
		private async Task FinishPartialValueAsync()
		{
			this.curNode.CopyTo(this.readValueOffset, this.stringBuilder);
			int outOrChars = 0;
			Tuple<int, int, int, bool> tuple = await this.ParseTextAsync(outOrChars).ConfigureAwait(false);
			int item = tuple.Item1;
			int item2 = tuple.Item2;
			outOrChars = tuple.Item3;
			while (!tuple.Item4)
			{
				this.stringBuilder.Append(this.ps.chars, item, item2 - item);
				tuple = await this.ParseTextAsync(outOrChars).ConfigureAwait(false);
				item = tuple.Item1;
				item2 = tuple.Item2;
				outOrChars = tuple.Item3;
			}
			this.stringBuilder.Append(this.ps.chars, item, item2 - item);
			this.curNode.SetValue(this.stringBuilder.ToString());
			this.stringBuilder.Length = 0;
		}

		// Token: 0x06000C81 RID: 3201 RVA: 0x0003FFC0 File Offset: 0x0003E1C0
		private async Task FinishOtherValueIteratorAsync()
		{
			switch (this.parsingFunction)
			{
			case XmlTextReaderImpl.ParsingFunction.InReadValueChunk:
				if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue)
				{
					await this.FinishPartialValueAsync().ConfigureAwait(false);
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue;
				}
				else if (this.readValueOffset > 0)
				{
					this.curNode.SetValue(this.curNode.StringValue.Substring(this.readValueOffset));
					this.readValueOffset = 0;
				}
				break;
			case XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary:
			case XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary:
				switch (this.incReadState)
				{
				case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue:
					if (this.readValueOffset > 0)
					{
						this.curNode.SetValue(this.curNode.StringValue.Substring(this.readValueOffset));
						this.readValueOffset = 0;
					}
					break;
				case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue:
					await this.FinishPartialValueAsync().ConfigureAwait(false);
					this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue;
					break;
				case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End:
					this.curNode.SetValue(string.Empty);
					break;
				}
				break;
			}
		}

		// Token: 0x06000C82 RID: 3202 RVA: 0x00040008 File Offset: 0x0003E208
		[MethodImpl(MethodImplOptions.NoInlining)]
		private async Task SkipPartialTextValueAsync()
		{
			int outOrChars = 0;
			this.parsingFunction = this.nextParsingFunction;
			Tuple<int, int, int, bool> tuple;
			do
			{
				tuple = await this.ParseTextAsync(outOrChars).ConfigureAwait(false);
				int item = tuple.Item1;
				int item2 = tuple.Item2;
				outOrChars = tuple.Item3;
			}
			while (!tuple.Item4);
		}

		// Token: 0x06000C83 RID: 3203 RVA: 0x0004004D File Offset: 0x0003E24D
		private Task FinishReadValueChunkAsync()
		{
			this.readValueOffset = 0;
			if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue)
			{
				return this.SkipPartialTextValueAsync();
			}
			this.parsingFunction = this.nextParsingFunction;
			this.nextParsingFunction = this.nextNextParsingFunction;
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000C84 RID: 3204 RVA: 0x00040084 File Offset: 0x0003E284
		private async Task FinishReadContentAsBinaryAsync()
		{
			this.readValueOffset = 0;
			if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue)
			{
				await this.SkipPartialTextValueAsync().ConfigureAwait(false);
			}
			else
			{
				this.parsingFunction = this.nextParsingFunction;
				this.nextParsingFunction = this.nextNextParsingFunction;
			}
			if (this.incReadState != XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End)
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				do
				{
					configuredTaskAwaiter = this.MoveToNextContentNodeAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
				}
				while (configuredTaskAwaiter.GetResult());
			}
		}

		// Token: 0x06000C85 RID: 3205 RVA: 0x000400CC File Offset: 0x0003E2CC
		private async Task FinishReadElementContentAsBinaryAsync()
		{
			await this.FinishReadContentAsBinaryAsync().ConfigureAwait(false);
			if (this.curNode.type != XmlNodeType.EndElement)
			{
				this.Throw("'{0}' is an invalid XmlNodeType.", this.curNode.type.ToString());
			}
			await this.outerReader.ReadAsync().ConfigureAwait(false);
		}

		// Token: 0x06000C86 RID: 3206 RVA: 0x00040114 File Offset: 0x0003E314
		private async Task<bool> ParseRootLevelWhitespaceAsync()
		{
			XmlNodeType nodeType = this.GetWhitespaceType();
			if (nodeType == XmlNodeType.None)
			{
				await this.EatWhitespacesAsync(null).ConfigureAwait(false);
				bool flag = this.ps.chars[this.ps.charPos] == '<' || this.ps.charsUsed - this.ps.charPos == 0;
				if (!flag)
				{
					flag = await this.ZeroEndingStreamAsync(this.ps.charPos).ConfigureAwait(false);
				}
				if (flag)
				{
					return false;
				}
			}
			else
			{
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
				await this.EatWhitespacesAsync(this.stringBuilder).ConfigureAwait(false);
				bool flag = this.ps.chars[this.ps.charPos] == '<' || this.ps.charsUsed - this.ps.charPos == 0;
				if (!flag)
				{
					flag = await this.ZeroEndingStreamAsync(this.ps.charPos).ConfigureAwait(false);
				}
				if (flag)
				{
					if (this.stringBuilder.Length > 0)
					{
						this.curNode.SetValueNode(nodeType, this.stringBuilder.ToString());
						this.stringBuilder.Length = 0;
						return true;
					}
					return false;
				}
			}
			if (this.xmlCharType.IsCharData(this.ps.chars[this.ps.charPos]))
			{
				this.Throw("Data at the root level is invalid.");
			}
			else
			{
				this.ThrowInvalidChar(this.ps.chars, this.ps.charsUsed, this.ps.charPos);
			}
			return false;
		}

		// Token: 0x06000C87 RID: 3207 RVA: 0x0004015C File Offset: 0x0003E35C
		private async Task ParseEntityReferenceAsync()
		{
			this.ps.charPos = this.ps.charPos + 1;
			this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			XmlTextReaderImpl.NodeData nodeData = this.curNode;
			string localName = await this.ParseEntityNameAsync().ConfigureAwait(false);
			nodeData.SetNamedNode(XmlNodeType.EntityReference, localName);
			nodeData = null;
		}

		// Token: 0x06000C88 RID: 3208 RVA: 0x000401A4 File Offset: 0x0003E3A4
		private async Task<Tuple<int, XmlTextReaderImpl.EntityType>> HandleEntityReferenceAsync(bool isInAttributeValue, XmlTextReaderImpl.EntityExpandType expandType)
		{
			if (this.ps.charPos + 1 == this.ps.charsUsed)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw("Unexpected end of file has occurred.");
				}
			}
			Tuple<int, XmlTextReaderImpl.EntityType> result;
			if (this.ps.chars[this.ps.charPos + 1] == '#')
			{
				Tuple<XmlTextReaderImpl.EntityType, int> tuple = await this.ParseNumericCharRefAsync(expandType != XmlTextReaderImpl.EntityExpandType.OnlyGeneral, null).ConfigureAwait(false);
				XmlTextReaderImpl.EntityType item = tuple.Item1;
				int charRefEndPos = tuple.Item2;
				result = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, item);
			}
			else
			{
				int charRefEndPos = await this.ParseNamedCharRefAsync(expandType != XmlTextReaderImpl.EntityExpandType.OnlyGeneral, null).ConfigureAwait(false);
				if (charRefEndPos >= 0)
				{
					result = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, XmlTextReaderImpl.EntityType.CharacterNamed);
				}
				else if (expandType == XmlTextReaderImpl.EntityExpandType.OnlyCharacter || (this.entityHandling != EntityHandling.ExpandEntities && (!isInAttributeValue || !this.validatingReaderCompatFlag)))
				{
					result = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, XmlTextReaderImpl.EntityType.Unexpanded);
				}
				else
				{
					this.ps.charPos = this.ps.charPos + 1;
					int savedLinePos = this.ps.LinePos;
					int endPos;
					try
					{
						endPos = await this.ParseNameAsync().ConfigureAwait(false);
					}
					catch (XmlException)
					{
						this.Throw("An error occurred while parsing EntityName.", this.ps.LineNo, savedLinePos);
						return new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, XmlTextReaderImpl.EntityType.Skipped);
					}
					if (this.ps.chars[endPos] != ';')
					{
						this.ThrowUnexpectedToken(endPos, ";");
					}
					int linePos = this.ps.LinePos;
					string name = this.nameTable.Add(this.ps.chars, this.ps.charPos, endPos - this.ps.charPos);
					this.ps.charPos = endPos + 1;
					charRefEndPos = -1;
					XmlTextReaderImpl.EntityType item2 = await this.HandleGeneralEntityReferenceAsync(name, isInAttributeValue, false, linePos).ConfigureAwait(false);
					this.reportedBaseUri = this.ps.baseUriStr;
					this.reportedEncoding = this.ps.encoding;
					result = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, item2);
				}
			}
			return result;
		}

		// Token: 0x06000C89 RID: 3209 RVA: 0x000401FC File Offset: 0x0003E3FC
		private async Task<XmlTextReaderImpl.EntityType> HandleGeneralEntityReferenceAsync(string name, bool isInAttributeValue, bool pushFakeEntityIfNullResolver, int entityStartLinePos)
		{
			IDtdEntityInfo entity = null;
			if (this.dtdInfo == null && this.fragmentParserContext != null && this.fragmentParserContext.HasDtdInfo && this.dtdProcessing == DtdProcessing.Parse)
			{
				await this.ParseDtdFromParserContextAsync().ConfigureAwait(false);
			}
			if (this.dtdInfo == null || (entity = this.dtdInfo.LookupEntity(name)) == null)
			{
				if (this.disableUndeclaredEntityCheck)
				{
					entity = new SchemaEntity(new XmlQualifiedName(name), false)
					{
						Text = string.Empty
					};
				}
				else
				{
					this.Throw("Reference to undeclared entity '{0}'.", name, this.ps.LineNo, entityStartLinePos);
				}
			}
			if (entity.IsUnparsedEntity)
			{
				if (this.disableUndeclaredEntityCheck)
				{
					entity = new SchemaEntity(new XmlQualifiedName(name), false)
					{
						Text = string.Empty
					};
				}
				else
				{
					this.Throw("Reference to unparsed entity '{0}'.", name, this.ps.LineNo, entityStartLinePos);
				}
			}
			if (this.standalone && entity.IsDeclaredInExternal)
			{
				this.Throw("Standalone document declaration must have a value of 'no' because an external entity '{0}' is referenced.", entity.Name, this.ps.LineNo, entityStartLinePos);
			}
			XmlTextReaderImpl.EntityType result;
			if (entity.IsExternal)
			{
				if (isInAttributeValue)
				{
					this.Throw("External entity '{0}' reference cannot appear in the attribute value.", name, this.ps.LineNo, entityStartLinePos);
					result = XmlTextReaderImpl.EntityType.Skipped;
				}
				else if (this.parsingMode == XmlTextReaderImpl.ParsingMode.SkipContent)
				{
					result = XmlTextReaderImpl.EntityType.Skipped;
				}
				else if (this.IsResolverNull)
				{
					if (pushFakeEntityIfNullResolver)
					{
						await this.PushExternalEntityAsync(entity).ConfigureAwait(false);
						this.curNode.entityId = this.ps.entityId;
						result = XmlTextReaderImpl.EntityType.FakeExpanded;
					}
					else
					{
						result = XmlTextReaderImpl.EntityType.Skipped;
					}
				}
				else
				{
					await this.PushExternalEntityAsync(entity).ConfigureAwait(false);
					this.curNode.entityId = this.ps.entityId;
					result = ((isInAttributeValue && this.validatingReaderCompatFlag) ? XmlTextReaderImpl.EntityType.ExpandedInAttribute : XmlTextReaderImpl.EntityType.Expanded);
				}
			}
			else if (this.parsingMode == XmlTextReaderImpl.ParsingMode.SkipContent)
			{
				result = XmlTextReaderImpl.EntityType.Skipped;
			}
			else
			{
				this.PushInternalEntity(entity);
				this.curNode.entityId = this.ps.entityId;
				result = ((isInAttributeValue && this.validatingReaderCompatFlag) ? XmlTextReaderImpl.EntityType.ExpandedInAttribute : XmlTextReaderImpl.EntityType.Expanded);
			}
			return result;
		}

		// Token: 0x06000C8A RID: 3210 RVA: 0x00040262 File Offset: 0x0003E462
		private Task<bool> ParsePIAsync()
		{
			return this.ParsePIAsync(null);
		}

		// Token: 0x06000C8B RID: 3211 RVA: 0x0004026C File Offset: 0x0003E46C
		private async Task<bool> ParsePIAsync(StringBuilder piInDtdStringBuilder)
		{
			if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
			{
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
			}
			int num = await this.ParseNameAsync().ConfigureAwait(false);
			string text = this.nameTable.Add(this.ps.chars, this.ps.charPos, num - this.ps.charPos);
			if (string.Compare(text, "xml", StringComparison.OrdinalIgnoreCase) == 0)
			{
				this.Throw(text.Equals("xml") ? "Unexpected XML declaration. The XML declaration must be the first node in the document, and no white space characters are allowed to appear before it." : "'{0}' is an invalid name for processing instructions.", text);
			}
			this.ps.charPos = num;
			if (piInDtdStringBuilder == null)
			{
				if (!this.ignorePIs && this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
				{
					this.curNode.SetNamedNode(XmlNodeType.ProcessingInstruction, text);
				}
			}
			else
			{
				piInDtdStringBuilder.Append(text);
			}
			char ch = this.ps.chars[this.ps.charPos];
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.EatWhitespacesAsync(piInDtdStringBuilder).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() == 0)
			{
				if (this.ps.charsUsed - this.ps.charPos < 2)
				{
					await this.ReadDataAsync().ConfigureAwait(false);
				}
				if (ch != '?' || this.ps.chars[this.ps.charPos + 1] != '>')
				{
					this.Throw("The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(this.ps.chars, this.ps.charsUsed, this.ps.charPos));
				}
			}
			object obj = await this.ParsePIValueAsync().ConfigureAwait(false);
			int item = obj.Item1;
			int item2 = obj.Item2;
			if (obj.Item3)
			{
				if (piInDtdStringBuilder == null)
				{
					if (this.ignorePIs)
					{
						return false;
					}
					if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
					{
						this.curNode.SetValue(this.ps.chars, item, item2 - item);
					}
				}
				else
				{
					piInDtdStringBuilder.Append(this.ps.chars, item, item2 - item);
				}
			}
			else
			{
				StringBuilder sb;
				if (piInDtdStringBuilder == null)
				{
					if (this.ignorePIs || this.parsingMode != XmlTextReaderImpl.ParsingMode.Full)
					{
						Tuple<int, int, bool> tuple;
						do
						{
							tuple = await this.ParsePIValueAsync().ConfigureAwait(false);
							item = tuple.Item1;
							item2 = tuple.Item2;
						}
						while (!tuple.Item3);
						return false;
					}
					sb = this.stringBuilder;
				}
				else
				{
					sb = piInDtdStringBuilder;
				}
				Tuple<int, int, bool> tuple2;
				do
				{
					sb.Append(this.ps.chars, item, item2 - item);
					tuple2 = await this.ParsePIValueAsync().ConfigureAwait(false);
					item = tuple2.Item1;
					item2 = tuple2.Item2;
				}
				while (!tuple2.Item3);
				sb.Append(this.ps.chars, item, item2 - item);
				if (piInDtdStringBuilder == null)
				{
					this.curNode.SetValue(this.stringBuilder.ToString());
					this.stringBuilder.Length = 0;
				}
				sb = null;
			}
			return true;
		}

		// Token: 0x06000C8C RID: 3212 RVA: 0x000402BC File Offset: 0x0003E4BC
		private async Task<Tuple<int, int, bool>> ParsePIValueAsync()
		{
			if (this.ps.charsUsed - this.ps.charPos < 2)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw(this.ps.charsUsed, "Unexpected end of file while parsing {0} has occurred.", "PI");
				}
			}
			int num = this.ps.charPos;
			char[] chars = this.ps.chars;
			int num2 = 0;
			int num3 = -1;
			for (;;)
			{
				byte[] charProperties = this.xmlCharType.charProperties;
				char c = chars[num];
				if ((charProperties[(int)c] & 64) == 0 || c == '?')
				{
					char c2 = chars[num];
					if (c2 <= '&')
					{
						switch (c2)
						{
						case '\t':
							break;
						case '\n':
							num++;
							this.OnNewLine(num);
							continue;
						case '\v':
						case '\f':
							goto IL_2A6;
						case '\r':
							if (chars[num + 1] == '\n')
							{
								if (!this.ps.eolNormalized && this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
								{
									if (num - this.ps.charPos > 0)
									{
										if (num2 == 0)
										{
											num2 = 1;
											num3 = num;
										}
										else
										{
											this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
											num3 = num - num2;
											num2++;
										}
									}
									else
									{
										this.ps.charPos = this.ps.charPos + 1;
									}
								}
								num += 2;
							}
							else
							{
								if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_309;
								}
								if (!this.ps.eolNormalized)
								{
									chars[num] = '\n';
								}
								num++;
							}
							this.OnNewLine(num);
							continue;
						default:
							if (c2 != '&')
							{
								goto IL_2A6;
							}
							break;
						}
					}
					else if (c2 != '<')
					{
						if (c2 != '?')
						{
							if (c2 != ']')
							{
								goto IL_2A6;
							}
						}
						else
						{
							if (chars[num + 1] == '>')
							{
								break;
							}
							if (num + 1 != this.ps.charsUsed)
							{
								num++;
								continue;
							}
							goto IL_309;
						}
					}
					num++;
					continue;
					IL_2A6:
					if (num == this.ps.charsUsed)
					{
						goto IL_309;
					}
					if (XmlCharType.IsHighSurrogate((int)chars[num]))
					{
						if (num + 1 == this.ps.charsUsed)
						{
							goto IL_309;
						}
						num++;
						if (XmlCharType.IsLowSurrogate((int)chars[num]))
						{
							num++;
							continue;
						}
					}
					this.ThrowInvalidChar(chars, this.ps.charsUsed, num);
				}
				else
				{
					num++;
				}
			}
			int item;
			if (num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
				item = num - num2;
			}
			else
			{
				item = num;
			}
			int charPos = this.ps.charPos;
			this.ps.charPos = num + 2;
			return new Tuple<int, int, bool>(charPos, item, true);
			IL_309:
			if (num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
				item = num - num2;
			}
			else
			{
				item = num;
			}
			int charPos2 = this.ps.charPos;
			this.ps.charPos = num;
			return new Tuple<int, int, bool>(charPos2, item, false);
		}

		// Token: 0x06000C8D RID: 3213 RVA: 0x00040304 File Offset: 0x0003E504
		private async Task<bool> ParseCommentAsync()
		{
			bool result;
			if (this.ignoreComments)
			{
				XmlTextReaderImpl.ParsingMode oldParsingMode = this.parsingMode;
				this.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
				await this.ParseCDataOrCommentAsync(XmlNodeType.Comment).ConfigureAwait(false);
				this.parsingMode = oldParsingMode;
				result = false;
			}
			else
			{
				await this.ParseCDataOrCommentAsync(XmlNodeType.Comment).ConfigureAwait(false);
				result = true;
			}
			return result;
		}

		// Token: 0x06000C8E RID: 3214 RVA: 0x00040349 File Offset: 0x0003E549
		private Task ParseCDataAsync()
		{
			return this.ParseCDataOrCommentAsync(XmlNodeType.CDATA);
		}

		// Token: 0x06000C8F RID: 3215 RVA: 0x00040354 File Offset: 0x0003E554
		private async Task ParseCDataOrCommentAsync(XmlNodeType type)
		{
			if (this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
			{
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
				object obj = await this.ParseCDataOrCommentTupleAsync(type).ConfigureAwait(false);
				int item = obj.Item1;
				int item2 = obj.Item2;
				if (obj.Item3)
				{
					this.curNode.SetValueNode(type, this.ps.chars, item, item2 - item);
				}
				else
				{
					Tuple<int, int, bool> tuple;
					do
					{
						this.stringBuilder.Append(this.ps.chars, item, item2 - item);
						tuple = await this.ParseCDataOrCommentTupleAsync(type).ConfigureAwait(false);
						item = tuple.Item1;
						item2 = tuple.Item2;
					}
					while (!tuple.Item3);
					this.stringBuilder.Append(this.ps.chars, item, item2 - item);
					this.curNode.SetValueNode(type, this.stringBuilder.ToString());
					this.stringBuilder.Length = 0;
				}
			}
			else
			{
				Tuple<int, int, bool> tuple2;
				do
				{
					tuple2 = await this.ParseCDataOrCommentTupleAsync(type).ConfigureAwait(false);
					int item = tuple2.Item1;
					int item2 = tuple2.Item2;
				}
				while (!tuple2.Item3);
			}
		}

		// Token: 0x06000C90 RID: 3216 RVA: 0x000403A4 File Offset: 0x0003E5A4
		private async Task<Tuple<int, int, bool>> ParseCDataOrCommentTupleAsync(XmlNodeType type)
		{
			if (this.ps.charsUsed - this.ps.charPos < 3)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw("Unexpected end of file while parsing {0} has occurred.", (type == XmlNodeType.Comment) ? "Comment" : "CDATA");
				}
			}
			int num = this.ps.charPos;
			char[] chars = this.ps.chars;
			int num2 = 0;
			int num3 = -1;
			char c = (type == XmlNodeType.Comment) ? '-' : ']';
			for (;;)
			{
				byte[] charProperties = this.xmlCharType.charProperties;
				char c2 = chars[num];
				if ((charProperties[(int)c2] & 64) == 0 || c2 == c)
				{
					if (chars[num] == c)
					{
						if (chars[num + 1] == c)
						{
							if (chars[num + 2] == '>')
							{
								break;
							}
							if (num + 2 == this.ps.charsUsed)
							{
								goto IL_35A;
							}
							if (type == XmlNodeType.Comment)
							{
								this.Throw(num, "An XML comment cannot contain '--', and '-' cannot be the last character.");
							}
						}
						else if (num + 1 == this.ps.charsUsed)
						{
							goto IL_35A;
						}
						num++;
					}
					else
					{
						char c3 = chars[num];
						if (c3 <= '&')
						{
							switch (c3)
							{
							case '\t':
								break;
							case '\n':
								num++;
								this.OnNewLine(num);
								continue;
							case '\v':
							case '\f':
								goto IL_2FC;
							case '\r':
								if (chars[num + 1] == '\n')
								{
									if (!this.ps.eolNormalized && this.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
									{
										if (num - this.ps.charPos > 0)
										{
											if (num2 == 0)
											{
												num2 = 1;
												num3 = num;
											}
											else
											{
												this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
												num3 = num - num2;
												num2++;
											}
										}
										else
										{
											this.ps.charPos = this.ps.charPos + 1;
										}
									}
									num += 2;
								}
								else
								{
									if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
									{
										goto IL_35A;
									}
									if (!this.ps.eolNormalized)
									{
										chars[num] = '\n';
									}
									num++;
								}
								this.OnNewLine(num);
								continue;
							default:
								if (c3 != '&')
								{
									goto IL_2FC;
								}
								break;
							}
						}
						else if (c3 != '<' && c3 != ']')
						{
							goto IL_2FC;
						}
						num++;
						continue;
						IL_2FC:
						if (num == this.ps.charsUsed)
						{
							goto IL_35A;
						}
						if (!XmlCharType.IsHighSurrogate((int)chars[num]))
						{
							goto IL_345;
						}
						if (num + 1 == this.ps.charsUsed)
						{
							goto IL_35A;
						}
						num++;
						if (!XmlCharType.IsLowSurrogate((int)chars[num]))
						{
							goto IL_345;
						}
						num++;
					}
				}
				else
				{
					num++;
				}
			}
			int item;
			if (num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
				item = num - num2;
			}
			else
			{
				item = num;
			}
			int charPos = this.ps.charPos;
			this.ps.charPos = num + 3;
			return new Tuple<int, int, bool>(charPos, item, true);
			IL_345:
			this.ThrowInvalidChar(chars, this.ps.charsUsed, num);
			IL_35A:
			if (num2 > 0)
			{
				this.ShiftBuffer(num3 + num2, num3, num - num3 - num2);
				item = num - num2;
			}
			else
			{
				item = num;
			}
			int charPos2 = this.ps.charPos;
			this.ps.charPos = num;
			return new Tuple<int, int, bool>(charPos2, item, false);
		}

		// Token: 0x06000C91 RID: 3217 RVA: 0x000403F4 File Offset: 0x0003E5F4
		private async Task<bool> ParseDoctypeDeclAsync()
		{
			if (this.dtdProcessing == DtdProcessing.Prohibit)
			{
				this.ThrowWithoutLineInfo(this.v1Compat ? "DTD is prohibited in this XML document." : "For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.");
			}
			while (this.ps.charsUsed - this.ps.charPos < 8)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw("Unexpected end of file while parsing {0} has occurred.", "DOCTYPE");
				}
			}
			if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, 7, "DOCTYPE"))
			{
				this.ThrowUnexpectedToken((!this.rootElementParsed && this.dtdInfo == null) ? "DOCTYPE" : "<!--");
			}
			if (!this.xmlCharType.IsWhiteSpace(this.ps.chars[this.ps.charPos + 7]))
			{
				this.ThrowExpectingWhitespace(this.ps.charPos + 7);
			}
			if (this.dtdInfo != null)
			{
				this.Throw(this.ps.charPos - 2, "Cannot have multiple DTDs.");
			}
			if (this.rootElementParsed)
			{
				this.Throw(this.ps.charPos - 2, "DTD must be defined before the document root element.");
			}
			this.ps.charPos = this.ps.charPos + 8;
			await this.EatWhitespacesAsync(null).ConfigureAwait(false);
			bool result;
			if (this.dtdProcessing == DtdProcessing.Parse)
			{
				this.curNode.SetLineInfo(this.ps.LineNo, this.ps.LinePos);
				await this.ParseDtdAsync().ConfigureAwait(false);
				this.nextParsingFunction = this.parsingFunction;
				this.parsingFunction = XmlTextReaderImpl.ParsingFunction.ResetAttributesRootLevel;
				result = true;
			}
			else
			{
				await this.SkipDtdAsync().ConfigureAwait(false);
				result = false;
			}
			return result;
		}

		// Token: 0x06000C92 RID: 3218 RVA: 0x0004043C File Offset: 0x0003E63C
		private async Task ParseDtdAsync()
		{
			IDtdInfo dtdInfo = await DtdParser.Create().ParseInternalDtdAsync(new XmlTextReaderImpl.DtdParserProxy(this), true).ConfigureAwait(false);
			this.dtdInfo = dtdInfo;
			if ((this.validatingReaderCompatFlag || !this.v1Compat) && (this.dtdInfo.HasDefaultAttributes || this.dtdInfo.HasNonCDataAttributes))
			{
				this.addDefaultAttributesAndNormalize = true;
			}
			this.curNode.SetNamedNode(XmlNodeType.DocumentType, this.dtdInfo.Name.ToString(), string.Empty, null);
			this.curNode.SetValue(this.dtdInfo.InternalDtdSubset);
		}

		// Token: 0x06000C93 RID: 3219 RVA: 0x00040484 File Offset: 0x0003E684
		private async Task SkipDtdAsync()
		{
			object obj = await this.ParseQNameAsync().ConfigureAwait(false);
			int item = obj.Item1;
			int item2 = obj.Item2;
			this.ps.charPos = item2;
			await this.EatWhitespacesAsync(null).ConfigureAwait(false);
			if (this.ps.chars[this.ps.charPos] == 'P')
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				while (this.ps.charsUsed - this.ps.charPos < 6)
				{
					configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == 0)
					{
						this.Throw("Unexpected end of file has occurred.");
					}
				}
				if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, 6, "PUBLIC"))
				{
					this.ThrowUnexpectedToken("PUBLIC");
				}
				this.ps.charPos = this.ps.charPos + 6;
				configuredTaskAwaiter = this.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.ThrowExpectingWhitespace(this.ps.charPos);
				}
				await this.SkipPublicOrSystemIdLiteralAsync().ConfigureAwait(false);
				configuredTaskAwaiter = this.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.ThrowExpectingWhitespace(this.ps.charPos);
				}
				await this.SkipPublicOrSystemIdLiteralAsync().ConfigureAwait(false);
				await this.EatWhitespacesAsync(null).ConfigureAwait(false);
			}
			else if (this.ps.chars[this.ps.charPos] == 'S')
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				while (this.ps.charsUsed - this.ps.charPos < 6)
				{
					configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == 0)
					{
						this.Throw("Unexpected end of file has occurred.");
					}
				}
				if (!XmlConvert.StrEqual(this.ps.chars, this.ps.charPos, 6, "SYSTEM"))
				{
					this.ThrowUnexpectedToken("SYSTEM");
				}
				this.ps.charPos = this.ps.charPos + 6;
				configuredTaskAwaiter = this.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.ThrowExpectingWhitespace(this.ps.charPos);
				}
				await this.SkipPublicOrSystemIdLiteralAsync().ConfigureAwait(false);
				await this.EatWhitespacesAsync(null).ConfigureAwait(false);
			}
			else if (this.ps.chars[this.ps.charPos] != '[' && this.ps.chars[this.ps.charPos] != '>')
			{
				this.Throw("Expecting external ID, '[' or '>'.");
			}
			if (this.ps.chars[this.ps.charPos] == '[')
			{
				this.ps.charPos = this.ps.charPos + 1;
				await this.SkipUntilAsync(']', true).ConfigureAwait(false);
				await this.EatWhitespacesAsync(null).ConfigureAwait(false);
				if (this.ps.chars[this.ps.charPos] != '>')
				{
					this.ThrowUnexpectedToken(">");
				}
			}
			else if (this.ps.chars[this.ps.charPos] == '>')
			{
				this.curNode.SetValue(string.Empty);
			}
			else
			{
				this.Throw("Expecting an internal subset or the end of the DOCTYPE declaration.");
			}
			this.ps.charPos = this.ps.charPos + 1;
		}

		// Token: 0x06000C94 RID: 3220 RVA: 0x000404CC File Offset: 0x0003E6CC
		private Task SkipPublicOrSystemIdLiteralAsync()
		{
			char c = this.ps.chars[this.ps.charPos];
			if (c != '"' && c != '\'')
			{
				this.ThrowUnexpectedToken("\"", "'");
			}
			this.ps.charPos = this.ps.charPos + 1;
			return this.SkipUntilAsync(c, false);
		}

		// Token: 0x06000C95 RID: 3221 RVA: 0x00040524 File Offset: 0x0003E724
		private async Task SkipUntilAsync(char stopChar, bool recognizeLiterals)
		{
			bool inLiteral = false;
			bool inComment = false;
			bool inPI = false;
			char literalQuote = '"';
			char[] chars = this.ps.chars;
			int num = this.ps.charPos;
			for (;;)
			{
				char c;
				if ((this.xmlCharType.charProperties[(int)(c = chars[num])] & 128) == 0 || chars[num] == stopChar || c == '-' || c == '?')
				{
					if (c == stopChar && !inLiteral)
					{
						break;
					}
					this.ps.charPos = num;
					if (c <= '&')
					{
						switch (c)
						{
						case '\t':
							break;
						case '\n':
							num++;
							this.OnNewLine(num);
							continue;
						case '\v':
						case '\f':
							goto IL_337;
						case '\r':
							if (chars[num + 1] == '\n')
							{
								num += 2;
							}
							else
							{
								if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_389;
								}
								num++;
							}
							this.OnNewLine(num);
							continue;
						default:
							if (c == '"')
							{
								goto IL_2EC;
							}
							if (c != '&')
							{
								goto IL_337;
							}
							break;
						}
					}
					else if (c <= '-')
					{
						if (c == '\'')
						{
							goto IL_2EC;
						}
						if (c != '-')
						{
							goto IL_337;
						}
						if (inComment)
						{
							if (num + 2 >= this.ps.charsUsed && !this.ps.isEof)
							{
								goto IL_389;
							}
							if (chars[num + 1] == '-' && chars[num + 2] == '>')
							{
								inComment = false;
								num += 2;
								continue;
							}
						}
						num++;
						continue;
					}
					else
					{
						switch (c)
						{
						case '<':
							if (chars[num + 1] == '?')
							{
								if (recognizeLiterals && !inLiteral && !inComment)
								{
									inPI = true;
									num += 2;
									continue;
								}
							}
							else if (chars[num + 1] == '!')
							{
								if (num + 3 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_389;
								}
								if (chars[num + 2] == '-' && chars[num + 3] == '-' && recognizeLiterals && !inLiteral && !inPI)
								{
									inComment = true;
									num += 4;
									continue;
								}
							}
							else if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
							{
								goto IL_389;
							}
							num++;
							continue;
						case '=':
							goto IL_337;
						case '>':
							break;
						case '?':
							if (inPI)
							{
								if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
								{
									goto IL_389;
								}
								if (chars[num + 1] == '>')
								{
									inPI = false;
									num++;
									continue;
								}
							}
							num++;
							continue;
						default:
							if (c != ']')
							{
								goto IL_337;
							}
							break;
						}
					}
					num++;
					continue;
					IL_2EC:
					if (inLiteral)
					{
						if (literalQuote == c)
						{
							inLiteral = false;
						}
					}
					else if (recognizeLiterals && !inComment && !inPI)
					{
						inLiteral = true;
						literalQuote = c;
					}
					num++;
					continue;
					IL_337:
					if (num != this.ps.charsUsed)
					{
						if (XmlCharType.IsHighSurrogate((int)chars[num]))
						{
							if (num + 1 == this.ps.charsUsed)
							{
								goto IL_389;
							}
							num++;
							if (XmlCharType.IsLowSurrogate((int)chars[num]))
							{
								num++;
								continue;
							}
						}
						this.ThrowInvalidChar(chars, this.ps.charsUsed, num);
					}
					IL_389:
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == 0)
					{
						if (this.ps.charsUsed - this.ps.charPos > 0)
						{
							if (this.ps.chars[this.ps.charPos] != '\r')
							{
								this.Throw("Unexpected end of file has occurred.");
							}
						}
						else
						{
							this.Throw("Unexpected end of file has occurred.");
						}
					}
					chars = this.ps.chars;
					num = this.ps.charPos;
				}
				else
				{
					num++;
				}
			}
			this.ps.charPos = num + 1;
		}

		// Token: 0x06000C96 RID: 3222 RVA: 0x0004057C File Offset: 0x0003E77C
		private async Task<int> EatWhitespacesAsync(StringBuilder sb)
		{
			int num = this.ps.charPos;
			int wsCount = 0;
			char[] chars = this.ps.chars;
			for (;;)
			{
				char c = chars[num];
				switch (c)
				{
				case '\t':
					break;
				case '\n':
					num++;
					this.OnNewLine(num);
					continue;
				case '\v':
				case '\f':
					goto IL_130;
				case '\r':
					if (chars[num + 1] == '\n')
					{
						int num2 = num - this.ps.charPos;
						if (sb != null && !this.ps.eolNormalized)
						{
							if (num2 > 0)
							{
								sb.Append(chars, this.ps.charPos, num2);
								wsCount += num2;
							}
							this.ps.charPos = num + 1;
						}
						num += 2;
					}
					else
					{
						if (num + 1 >= this.ps.charsUsed && !this.ps.isEof)
						{
							goto IL_1A5;
						}
						if (!this.ps.eolNormalized)
						{
							chars[num] = '\n';
						}
						num++;
					}
					this.OnNewLine(num);
					continue;
				default:
					if (c != ' ')
					{
						goto IL_130;
					}
					break;
				}
				num++;
				continue;
				IL_1A5:
				int num3 = num - this.ps.charPos;
				if (num3 > 0)
				{
					if (sb != null)
					{
						sb.Append(this.ps.chars, this.ps.charPos, num3);
					}
					this.ps.charPos = num;
					wsCount += num3;
				}
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					if (this.ps.charsUsed - this.ps.charPos == 0)
					{
						goto Block_16;
					}
					if (this.ps.chars[this.ps.charPos] != '\r')
					{
						this.Throw("Unexpected end of file has occurred.");
					}
				}
				num = this.ps.charPos;
				chars = this.ps.chars;
				continue;
				IL_130:
				if (num != this.ps.charsUsed)
				{
					break;
				}
				goto IL_1A5;
			}
			int num4 = num - this.ps.charPos;
			if (num4 > 0)
			{
				if (sb != null)
				{
					sb.Append(this.ps.chars, this.ps.charPos, num4);
				}
				this.ps.charPos = num;
				wsCount += num4;
			}
			return wsCount;
			Block_16:
			return wsCount;
		}

		// Token: 0x06000C97 RID: 3223 RVA: 0x000405CC File Offset: 0x0003E7CC
		private async Task<Tuple<XmlTextReaderImpl.EntityType, int>> ParseNumericCharRefAsync(bool expand, StringBuilder internalSubsetBuilder)
		{
			int charCount;
			XmlTextReaderImpl.EntityType entityType;
			int newPos;
			for (;;)
			{
				int num = newPos = this.ParseNumericCharRefInline(this.ps.charPos, expand, internalSubsetBuilder, out charCount, out entityType);
				if (num != -2)
				{
					break;
				}
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw("Unexpected end of file while parsing {0} has occurred.");
				}
			}
			if (expand)
			{
				this.ps.charPos = newPos - charCount;
			}
			return new Tuple<XmlTextReaderImpl.EntityType, int>(entityType, newPos);
		}

		// Token: 0x06000C98 RID: 3224 RVA: 0x00040624 File Offset: 0x0003E824
		private async Task<int> ParseNamedCharRefAsync(bool expand, StringBuilder internalSubsetBuilder)
		{
			int newPos;
			int num;
			for (;;)
			{
				num = (newPos = this.ParseNamedCharRefInline(this.ps.charPos, expand, internalSubsetBuilder));
				if (num != -2)
				{
					break;
				}
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					goto Block_3;
				}
			}
			if (num == -1)
			{
				return -1;
			}
			if (expand)
			{
				this.ps.charPos = newPos - 1;
			}
			return newPos;
			Block_3:
			return -1;
		}

		// Token: 0x06000C99 RID: 3225 RVA: 0x0004067C File Offset: 0x0003E87C
		private async Task<int> ParseNameAsync()
		{
			return (await this.ParseQNameAsync(false, 0).ConfigureAwait(false)).Item2;
		}

		// Token: 0x06000C9A RID: 3226 RVA: 0x000406C1 File Offset: 0x0003E8C1
		private Task<Tuple<int, int>> ParseQNameAsync()
		{
			return this.ParseQNameAsync(true, 0);
		}

		// Token: 0x06000C9B RID: 3227 RVA: 0x000406CC File Offset: 0x0003E8CC
		private async Task<Tuple<int, int>> ParseQNameAsync(bool isQName, int startOffset)
		{
			int colonOffset = -1;
			int num = this.ps.charPos + startOffset;
			for (;;)
			{
				char[] chars = this.ps.chars;
				bool flag = false;
				if ((this.xmlCharType.charProperties[(int)chars[num]] & 4) != 0)
				{
					num++;
				}
				else if (num + 1 >= this.ps.charsUsed)
				{
					flag = true;
				}
				else if (chars[num] != ':' || this.supportNamespaces)
				{
					this.Throw(num, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(chars, this.ps.charsUsed, num));
				}
				if (flag)
				{
					object obj = await this.ReadDataInNameAsync(num).ConfigureAwait(false);
					num = obj.Item1;
					if (obj.Item2)
					{
						continue;
					}
					this.Throw(num, "Unexpected end of file while parsing {0} has occurred.", "Name");
				}
				for (;;)
				{
					if ((this.xmlCharType.charProperties[(int)chars[num]] & 8) != 0)
					{
						num++;
					}
					else if (chars[num] == ':')
					{
						if (this.supportNamespaces)
						{
							break;
						}
						colonOffset = num - this.ps.charPos;
						num++;
					}
					else
					{
						if (num != this.ps.charsUsed)
						{
							goto IL_283;
						}
						object obj2 = await this.ReadDataInNameAsync(num).ConfigureAwait(false);
						num = obj2.Item1;
						if (!obj2.Item2)
						{
							goto IL_272;
						}
						chars = this.ps.chars;
					}
				}
				if (colonOffset != -1 || !isQName)
				{
					this.Throw(num, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
				}
				colonOffset = num - this.ps.charPos;
				num++;
			}
			IL_272:
			this.Throw(num, "Unexpected end of file while parsing {0} has occurred.", "Name");
			IL_283:
			return new Tuple<int, int>((colonOffset == -1) ? -1 : (this.ps.charPos + colonOffset), num);
		}

		// Token: 0x06000C9C RID: 3228 RVA: 0x00040724 File Offset: 0x0003E924
		private async Task<Tuple<int, bool>> ReadDataInNameAsync(int pos)
		{
			int offset = pos - this.ps.charPos;
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
			}
			bool item = configuredTaskAwaiter.GetResult() != 0;
			pos = this.ps.charPos + offset;
			return new Tuple<int, bool>(pos, item);
		}

		// Token: 0x06000C9D RID: 3229 RVA: 0x00040774 File Offset: 0x0003E974
		private async Task<string> ParseEntityNameAsync()
		{
			int endPos;
			try
			{
				int num = await this.ParseNameAsync().ConfigureAwait(false);
				endPos = num;
			}
			catch (XmlException)
			{
				this.Throw("An error occurred while parsing EntityName.");
				return null;
			}
			if (this.ps.chars[endPos] != ';')
			{
				this.Throw("An error occurred while parsing EntityName.");
			}
			string result = this.nameTable.Add(this.ps.chars, this.ps.charPos, endPos - this.ps.charPos);
			this.ps.charPos = endPos + 1;
			return result;
		}

		// Token: 0x06000C9E RID: 3230 RVA: 0x000407BC File Offset: 0x0003E9BC
		private async Task PushExternalEntityOrSubsetAsync(string publicId, string systemId, Uri baseUri, string entityName)
		{
			Uri uri;
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			if (!string.IsNullOrEmpty(publicId))
			{
				try
				{
					uri = this.xmlResolver.ResolveUri(baseUri, publicId);
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.OpenAndPushAsync(uri).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult())
					{
						return;
					}
				}
				catch (Exception)
				{
				}
			}
			uri = this.xmlResolver.ResolveUri(baseUri, systemId);
			try
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.OpenAndPushAsync(uri).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult())
				{
					return;
				}
			}
			catch (Exception ex)
			{
				if (this.v1Compat)
				{
					throw;
				}
				string message = ex.Message;
				this.Throw(new XmlException((entityName == null) ? "An error has occurred while opening external DTD '{0}': {1}" : "An error has occurred while opening external entity '{0}': {1}", new string[]
				{
					uri.ToString(),
					message
				}, ex, 0, 0));
			}
			if (entityName == null)
			{
				this.ThrowWithoutLineInfo("Cannot resolve external DTD subset - public ID = '{0}', system ID = '{1}'.", new string[]
				{
					(publicId != null) ? publicId : string.Empty,
					systemId
				}, null);
			}
			else
			{
				this.Throw((this.dtdProcessing == DtdProcessing.Ignore) ? "Cannot resolve entity reference '{0}' because the DTD has been ignored. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method." : "Cannot resolve entity reference '{0}'.", entityName);
			}
		}

		// Token: 0x06000C9F RID: 3231 RVA: 0x00040824 File Offset: 0x0003EA24
		private async Task<bool> OpenAndPushAsync(Uri uri)
		{
			if (this.xmlResolver.SupportsType(uri, typeof(TextReader)))
			{
				ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.xmlResolver.GetEntityAsync(uri, null, typeof(TextReader)).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
				}
				TextReader textReader = (TextReader)configuredTaskAwaiter.GetResult();
				if (textReader == null)
				{
					return false;
				}
				this.PushParsingState();
				await this.InitTextReaderInputAsync(uri.ToString(), uri, textReader).ConfigureAwait(false);
			}
			else
			{
				ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.xmlResolver.GetEntityAsync(uri, null, typeof(Stream)).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
				}
				Stream stream = (Stream)configuredTaskAwaiter.GetResult();
				if (stream == null)
				{
					return false;
				}
				this.PushParsingState();
				await this.InitStreamInputAsync(uri, stream, null).ConfigureAwait(false);
			}
			return true;
		}

		// Token: 0x06000CA0 RID: 3232 RVA: 0x00040874 File Offset: 0x0003EA74
		private async Task<bool> PushExternalEntityAsync(IDtdEntityInfo entity)
		{
			bool result;
			if (!this.IsResolverNull)
			{
				Uri baseUri = null;
				if (!string.IsNullOrEmpty(entity.BaseUriString))
				{
					baseUri = this.xmlResolver.ResolveUri(null, entity.BaseUriString);
				}
				await this.PushExternalEntityOrSubsetAsync(entity.PublicId, entity.SystemId, baseUri, entity.Name).ConfigureAwait(false);
				this.RegisterEntity(entity);
				int initialPos = this.ps.charPos;
				if (this.v1Compat)
				{
					await this.EatWhitespacesAsync(null).ConfigureAwait(false);
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ParseXmlDeclarationAsync(true).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					this.ps.charPos = initialPos;
				}
				result = true;
			}
			else
			{
				Encoding encoding = this.ps.encoding;
				this.PushParsingState();
				this.InitStringInput(entity.SystemId, encoding, string.Empty);
				this.RegisterEntity(entity);
				this.RegisterConsumedCharacters(0L, true);
				result = false;
			}
			return result;
		}

		// Token: 0x06000CA1 RID: 3233 RVA: 0x000408C4 File Offset: 0x0003EAC4
		private async Task<bool> ZeroEndingStreamAsync(int pos)
		{
			bool flag = this.v1Compat && pos == this.ps.charsUsed - 1 && this.ps.chars[pos] == '\0';
			if (flag)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				flag = (configuredTaskAwaiter.GetResult() == 0);
			}
			bool result;
			if (flag && this.ps.isStreamEof)
			{
				this.ps.charsUsed = this.ps.charsUsed - 1;
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000CA2 RID: 3234 RVA: 0x00040914 File Offset: 0x0003EB14
		private async Task ParseDtdFromParserContextAsync()
		{
			IDtdInfo dtdInfo = await DtdParser.Create().ParseFreeFloatingDtdAsync(this.fragmentParserContext.BaseURI, this.fragmentParserContext.DocTypeName, this.fragmentParserContext.PublicId, this.fragmentParserContext.SystemId, this.fragmentParserContext.InternalSubset, new XmlTextReaderImpl.DtdParserProxy(this)).ConfigureAwait(false);
			this.dtdInfo = dtdInfo;
			if ((this.validatingReaderCompatFlag || !this.v1Compat) && (this.dtdInfo.HasDefaultAttributes || this.dtdInfo.HasNonCDataAttributes))
			{
				this.addDefaultAttributesAndNormalize = true;
			}
		}

		// Token: 0x06000CA3 RID: 3235 RVA: 0x0004095C File Offset: 0x0003EB5C
		private async Task<bool> InitReadContentAsBinaryAsync()
		{
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadValueChunk)
			{
				throw new InvalidOperationException(Res.GetString("ReadValueChunk calls cannot be mixed with ReadContentAsBase64 or ReadContentAsBinHex."));
			}
			if (this.parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
			{
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadChars, ReadBase64, and ReadBinHex."));
			}
			if (!XmlReader.IsTextualNode(this.curNode.type))
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.MoveToNextContentNodeAsync(false).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return false;
				}
			}
			this.SetupReadContentAsBinaryState(XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary);
			this.incReadLineInfo.Set(this.curNode.LineNo, this.curNode.LinePos);
			return true;
		}

		// Token: 0x06000CA4 RID: 3236 RVA: 0x000409A4 File Offset: 0x0003EBA4
		private async Task<bool> InitReadElementContentAsBinaryAsync()
		{
			bool isEmpty = this.curNode.IsEmptyElement;
			await this.outerReader.ReadAsync().ConfigureAwait(false);
			bool result;
			if (isEmpty)
			{
				result = false;
			}
			else
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.MoveToNextContentNodeAsync(false).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					if (this.curNode.type != XmlNodeType.EndElement)
					{
						this.Throw("'{0}' is an invalid XmlNodeType.", this.curNode.type.ToString());
					}
					await this.outerReader.ReadAsync().ConfigureAwait(false);
					result = false;
				}
				else
				{
					this.SetupReadContentAsBinaryState(XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary);
					this.incReadLineInfo.Set(this.curNode.LineNo, this.curNode.LinePos);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000CA5 RID: 3237 RVA: 0x000409EC File Offset: 0x0003EBEC
		private async Task<bool> MoveToNextContentNodeAsync(bool moveIfOnContentNode)
		{
			for (;;)
			{
				switch (this.curNode.type)
				{
				case XmlNodeType.Attribute:
					goto IL_66;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					if (!moveIfOnContentNode)
					{
						goto Block_1;
					}
					goto IL_98;
				case XmlNodeType.EntityReference:
					this.outerReader.ResolveEntity();
					goto IL_98;
				case XmlNodeType.ProcessingInstruction:
				case XmlNodeType.Comment:
				case XmlNodeType.EndEntity:
					goto IL_98;
				}
				break;
				IL_98:
				moveIfOnContentNode = false;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					goto Block_3;
				}
			}
			goto IL_91;
			IL_66:
			return !moveIfOnContentNode;
			Block_1:
			return true;
			IL_91:
			return false;
			Block_3:
			return false;
		}

		// Token: 0x06000CA6 RID: 3238 RVA: 0x00040A3C File Offset: 0x0003EC3C
		private async Task<int> ReadContentAsBinaryAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End)
			{
				result = 0;
			}
			else
			{
				this.incReadDecoder.SetNextOutputBuffer(buffer, index, count);
				int charsRead;
				int num;
				int num2;
				XmlTextReaderImpl.ParsingFunction tmp;
				for (;;)
				{
					charsRead = 0;
					try
					{
						charsRead = this.curNode.CopyToBinary(this.incReadDecoder, this.readValueOffset);
					}
					catch (XmlException e)
					{
						this.curNode.AdjustLineInfo(this.readValueOffset, this.ps.eolNormalized, ref this.incReadLineInfo);
						this.ReThrow(e, this.incReadLineInfo.lineNo, this.incReadLineInfo.linePos);
					}
					this.readValueOffset += charsRead;
					if (this.incReadDecoder.IsFull)
					{
						break;
					}
					if (this.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue)
					{
						this.curNode.SetValue(string.Empty);
						bool flag = false;
						num = 0;
						num2 = 0;
						while (!this.incReadDecoder.IsFull && !flag)
						{
							int outOrChars = 0;
							this.incReadLineInfo.Set(this.ps.LineNo, this.ps.LinePos);
							object obj = await this.ParseTextAsync(outOrChars).ConfigureAwait(false);
							num = obj.Item1;
							num2 = obj.Item2;
							outOrChars = obj.Item3;
							flag = obj.Item4;
							try
							{
								charsRead = this.incReadDecoder.Decode(this.ps.chars, num, num2 - num);
							}
							catch (XmlException e2)
							{
								this.ReThrow(e2, this.incReadLineInfo.lineNo, this.incReadLineInfo.linePos);
							}
							num += charsRead;
						}
						this.incReadState = (flag ? XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue : XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue);
						this.readValueOffset = 0;
						if (this.incReadDecoder.IsFull)
						{
							goto Block_8;
						}
					}
					tmp = this.parsingFunction;
					this.parsingFunction = this.nextParsingFunction;
					this.nextParsingFunction = this.nextNextParsingFunction;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.MoveToNextContentNodeAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (!configuredTaskAwaiter.GetResult())
					{
						goto Block_10;
					}
					this.SetupReadContentAsBinaryState(tmp);
					this.incReadLineInfo.Set(this.curNode.LineNo, this.curNode.LinePos);
				}
				return this.incReadDecoder.DecodedCount;
				Block_8:
				this.curNode.SetValue(this.ps.chars, num, num2 - num);
				XmlTextReaderImpl.AdjustLineInfo(this.ps.chars, num - charsRead, num, this.ps.eolNormalized, ref this.incReadLineInfo);
				this.curNode.SetLineInfo(this.incReadLineInfo.lineNo, this.incReadLineInfo.linePos);
				return this.incReadDecoder.DecodedCount;
				Block_10:
				this.SetupReadContentAsBinaryState(tmp);
				this.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End;
				result = this.incReadDecoder.DecodedCount;
			}
			return result;
		}

		// Token: 0x06000CA7 RID: 3239 RVA: 0x00040A9C File Offset: 0x0003EC9C
		private async Task<int> ReadElementContentAsBinaryAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (count == 0)
			{
				result = 0;
			}
			else
			{
				int num = await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
				if (num > 0)
				{
					result = num;
				}
				else
				{
					if (this.curNode.type != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", this.curNode.type.ToString(), this);
					}
					this.parsingFunction = this.nextParsingFunction;
					this.nextParsingFunction = this.nextNextParsingFunction;
					await this.outerReader.ReadAsync().ConfigureAwait(false);
					result = 0;
				}
			}
			return result;
		}

		// Token: 0x04000643 RID: 1603
		private readonly bool useAsync;

		// Token: 0x04000644 RID: 1604
		private XmlTextReaderImpl.LaterInitParam laterInitParam;

		// Token: 0x04000645 RID: 1605
		private XmlCharType xmlCharType = XmlCharType.Instance;

		// Token: 0x04000646 RID: 1606
		private XmlTextReaderImpl.ParsingState ps;

		// Token: 0x04000647 RID: 1607
		private XmlTextReaderImpl.ParsingFunction parsingFunction;

		// Token: 0x04000648 RID: 1608
		private XmlTextReaderImpl.ParsingFunction nextParsingFunction;

		// Token: 0x04000649 RID: 1609
		private XmlTextReaderImpl.ParsingFunction nextNextParsingFunction;

		// Token: 0x0400064A RID: 1610
		private XmlTextReaderImpl.NodeData[] nodes;

		// Token: 0x0400064B RID: 1611
		private XmlTextReaderImpl.NodeData curNode;

		// Token: 0x0400064C RID: 1612
		private int index;

		// Token: 0x0400064D RID: 1613
		private int curAttrIndex = -1;

		// Token: 0x0400064E RID: 1614
		private int attrCount;

		// Token: 0x0400064F RID: 1615
		private int attrHashtable;

		// Token: 0x04000650 RID: 1616
		private int attrDuplWalkCount;

		// Token: 0x04000651 RID: 1617
		private bool attrNeedNamespaceLookup;

		// Token: 0x04000652 RID: 1618
		private bool fullAttrCleanup;

		// Token: 0x04000653 RID: 1619
		private XmlTextReaderImpl.NodeData[] attrDuplSortingArray;

		// Token: 0x04000654 RID: 1620
		private XmlNameTable nameTable;

		// Token: 0x04000655 RID: 1621
		private bool nameTableFromSettings;

		// Token: 0x04000656 RID: 1622
		private XmlResolver xmlResolver;

		// Token: 0x04000657 RID: 1623
		private string url = string.Empty;

		// Token: 0x04000658 RID: 1624
		private bool normalize;

		// Token: 0x04000659 RID: 1625
		private bool supportNamespaces = true;

		// Token: 0x0400065A RID: 1626
		private WhitespaceHandling whitespaceHandling;

		// Token: 0x0400065B RID: 1627
		private DtdProcessing dtdProcessing = DtdProcessing.Parse;

		// Token: 0x0400065C RID: 1628
		private EntityHandling entityHandling;

		// Token: 0x0400065D RID: 1629
		private bool ignorePIs;

		// Token: 0x0400065E RID: 1630
		private bool ignoreComments;

		// Token: 0x0400065F RID: 1631
		private bool checkCharacters;

		// Token: 0x04000660 RID: 1632
		private int lineNumberOffset;

		// Token: 0x04000661 RID: 1633
		private int linePositionOffset;

		// Token: 0x04000662 RID: 1634
		private bool closeInput;

		// Token: 0x04000663 RID: 1635
		private long maxCharactersInDocument;

		// Token: 0x04000664 RID: 1636
		private long maxCharactersFromEntities;

		// Token: 0x04000665 RID: 1637
		private bool v1Compat;

		// Token: 0x04000666 RID: 1638
		private XmlNamespaceManager namespaceManager;

		// Token: 0x04000667 RID: 1639
		private string lastPrefix = string.Empty;

		// Token: 0x04000668 RID: 1640
		private XmlTextReaderImpl.XmlContext xmlContext;

		// Token: 0x04000669 RID: 1641
		private XmlTextReaderImpl.ParsingState[] parsingStatesStack;

		// Token: 0x0400066A RID: 1642
		private int parsingStatesStackTop = -1;

		// Token: 0x0400066B RID: 1643
		private string reportedBaseUri;

		// Token: 0x0400066C RID: 1644
		private Encoding reportedEncoding;

		// Token: 0x0400066D RID: 1645
		private IDtdInfo dtdInfo;

		// Token: 0x0400066E RID: 1646
		private XmlNodeType fragmentType = XmlNodeType.Document;

		// Token: 0x0400066F RID: 1647
		private XmlParserContext fragmentParserContext;

		// Token: 0x04000670 RID: 1648
		private bool fragment;

		// Token: 0x04000671 RID: 1649
		private IncrementalReadDecoder incReadDecoder;

		// Token: 0x04000672 RID: 1650
		private XmlTextReaderImpl.IncrementalReadState incReadState;

		// Token: 0x04000673 RID: 1651
		private LineInfo incReadLineInfo;

		// Token: 0x04000674 RID: 1652
		private BinHexDecoder binHexDecoder;

		// Token: 0x04000675 RID: 1653
		private Base64Decoder base64Decoder;

		// Token: 0x04000676 RID: 1654
		private int incReadDepth;

		// Token: 0x04000677 RID: 1655
		private int incReadLeftStartPos;

		// Token: 0x04000678 RID: 1656
		private int incReadLeftEndPos;

		// Token: 0x04000679 RID: 1657
		private IncrementalReadCharsDecoder readCharsDecoder;

		// Token: 0x0400067A RID: 1658
		private int attributeValueBaseEntityId;

		// Token: 0x0400067B RID: 1659
		private bool emptyEntityInAttributeResolved;

		// Token: 0x0400067C RID: 1660
		private IValidationEventHandling validationEventHandling;

		// Token: 0x0400067D RID: 1661
		private XmlTextReaderImpl.OnDefaultAttributeUseDelegate onDefaultAttributeUse;

		// Token: 0x0400067E RID: 1662
		private bool validatingReaderCompatFlag;

		// Token: 0x0400067F RID: 1663
		private bool addDefaultAttributesAndNormalize;

		// Token: 0x04000680 RID: 1664
		private StringBuilder stringBuilder;

		// Token: 0x04000681 RID: 1665
		private bool rootElementParsed;

		// Token: 0x04000682 RID: 1666
		private bool standalone;

		// Token: 0x04000683 RID: 1667
		private int nextEntityId = 1;

		// Token: 0x04000684 RID: 1668
		private XmlTextReaderImpl.ParsingMode parsingMode;

		// Token: 0x04000685 RID: 1669
		private ReadState readState;

		// Token: 0x04000686 RID: 1670
		private IDtdEntityInfo lastEntity;

		// Token: 0x04000687 RID: 1671
		private bool afterResetState;

		// Token: 0x04000688 RID: 1672
		private int documentStartBytePos;

		// Token: 0x04000689 RID: 1673
		private int readValueOffset;

		// Token: 0x0400068A RID: 1674
		private long charactersInDocument;

		// Token: 0x0400068B RID: 1675
		private long charactersFromEntities;

		// Token: 0x0400068C RID: 1676
		private Dictionary<IDtdEntityInfo, IDtdEntityInfo> currentEntities;

		// Token: 0x0400068D RID: 1677
		private bool disableUndeclaredEntityCheck;

		// Token: 0x0400068E RID: 1678
		private XmlReader outerReader;

		// Token: 0x0400068F RID: 1679
		private bool xmlResolverIsSet;

		// Token: 0x04000690 RID: 1680
		private string Xml;

		// Token: 0x04000691 RID: 1681
		private string XmlNs;

		// Token: 0x04000692 RID: 1682
		private const int MaxBytesToMove = 128;

		// Token: 0x04000693 RID: 1683
		private const int ApproxXmlDeclLength = 80;

		// Token: 0x04000694 RID: 1684
		private const int NodesInitialSize = 8;

		// Token: 0x04000695 RID: 1685
		private const int InitialAttributesCount = 4;

		// Token: 0x04000696 RID: 1686
		private const int InitialParsingStateStackSize = 2;

		// Token: 0x04000697 RID: 1687
		private const int InitialParsingStatesDepth = 2;

		// Token: 0x04000698 RID: 1688
		private const int DtdChidrenInitialSize = 2;

		// Token: 0x04000699 RID: 1689
		private const int MaxByteSequenceLen = 6;

		// Token: 0x0400069A RID: 1690
		private const int MaxAttrDuplWalkCount = 250;

		// Token: 0x0400069B RID: 1691
		private const int MinWhitespaceLookahedCount = 4096;

		// Token: 0x0400069C RID: 1692
		private const string XmlDeclarationBegining = "<?xml";

		// Token: 0x0400069D RID: 1693
		private XmlTextReaderImpl.ParseEndElementParseFunction parseEndElement_NextFunc;

		// Token: 0x0400069E RID: 1694
		private XmlTextReaderImpl.ParseTextFunction parseText_NextFunction;

		// Token: 0x0400069F RID: 1695
		private XmlTextReaderImpl.ParseTextState lastParseTextState;

		// Token: 0x040006A0 RID: 1696
		private Task<Tuple<int, int, int, bool>> parseText_dummyTask = Task.FromResult<Tuple<int, int, int, bool>>(new Tuple<int, int, int, bool>(0, 0, 0, false));

		// Token: 0x02000124 RID: 292
		private enum ParsingFunction
		{
			// Token: 0x040006A2 RID: 1698
			ElementContent,
			// Token: 0x040006A3 RID: 1699
			NoData,
			// Token: 0x040006A4 RID: 1700
			OpenUrl,
			// Token: 0x040006A5 RID: 1701
			SwitchToInteractive,
			// Token: 0x040006A6 RID: 1702
			SwitchToInteractiveXmlDecl,
			// Token: 0x040006A7 RID: 1703
			DocumentContent,
			// Token: 0x040006A8 RID: 1704
			MoveToElementContent,
			// Token: 0x040006A9 RID: 1705
			PopElementContext,
			// Token: 0x040006AA RID: 1706
			PopEmptyElementContext,
			// Token: 0x040006AB RID: 1707
			ResetAttributesRootLevel,
			// Token: 0x040006AC RID: 1708
			Error,
			// Token: 0x040006AD RID: 1709
			Eof,
			// Token: 0x040006AE RID: 1710
			ReaderClosed,
			// Token: 0x040006AF RID: 1711
			EntityReference,
			// Token: 0x040006B0 RID: 1712
			InIncrementalRead,
			// Token: 0x040006B1 RID: 1713
			FragmentAttribute,
			// Token: 0x040006B2 RID: 1714
			ReportEndEntity,
			// Token: 0x040006B3 RID: 1715
			AfterResolveEntityInContent,
			// Token: 0x040006B4 RID: 1716
			AfterResolveEmptyEntityInContent,
			// Token: 0x040006B5 RID: 1717
			XmlDeclarationFragment,
			// Token: 0x040006B6 RID: 1718
			GoToEof,
			// Token: 0x040006B7 RID: 1719
			PartialTextValue,
			// Token: 0x040006B8 RID: 1720
			InReadAttributeValue,
			// Token: 0x040006B9 RID: 1721
			InReadValueChunk,
			// Token: 0x040006BA RID: 1722
			InReadContentAsBinary,
			// Token: 0x040006BB RID: 1723
			InReadElementContentAsBinary
		}

		// Token: 0x02000125 RID: 293
		private enum ParsingMode
		{
			// Token: 0x040006BD RID: 1725
			Full,
			// Token: 0x040006BE RID: 1726
			SkipNode,
			// Token: 0x040006BF RID: 1727
			SkipContent
		}

		// Token: 0x02000126 RID: 294
		private enum EntityType
		{
			// Token: 0x040006C1 RID: 1729
			CharacterDec,
			// Token: 0x040006C2 RID: 1730
			CharacterHex,
			// Token: 0x040006C3 RID: 1731
			CharacterNamed,
			// Token: 0x040006C4 RID: 1732
			Expanded,
			// Token: 0x040006C5 RID: 1733
			Skipped,
			// Token: 0x040006C6 RID: 1734
			FakeExpanded,
			// Token: 0x040006C7 RID: 1735
			Unexpanded,
			// Token: 0x040006C8 RID: 1736
			ExpandedInAttribute
		}

		// Token: 0x02000127 RID: 295
		private enum EntityExpandType
		{
			// Token: 0x040006CA RID: 1738
			All,
			// Token: 0x040006CB RID: 1739
			OnlyGeneral,
			// Token: 0x040006CC RID: 1740
			OnlyCharacter
		}

		// Token: 0x02000128 RID: 296
		private enum IncrementalReadState
		{
			// Token: 0x040006CE RID: 1742
			Text,
			// Token: 0x040006CF RID: 1743
			StartTag,
			// Token: 0x040006D0 RID: 1744
			PI,
			// Token: 0x040006D1 RID: 1745
			CDATA,
			// Token: 0x040006D2 RID: 1746
			Comment,
			// Token: 0x040006D3 RID: 1747
			Attributes,
			// Token: 0x040006D4 RID: 1748
			AttributeValue,
			// Token: 0x040006D5 RID: 1749
			ReadData,
			// Token: 0x040006D6 RID: 1750
			EndElement,
			// Token: 0x040006D7 RID: 1751
			End,
			// Token: 0x040006D8 RID: 1752
			ReadValueChunk_OnCachedValue,
			// Token: 0x040006D9 RID: 1753
			ReadValueChunk_OnPartialValue,
			// Token: 0x040006DA RID: 1754
			ReadContentAsBinary_OnCachedValue,
			// Token: 0x040006DB RID: 1755
			ReadContentAsBinary_OnPartialValue,
			// Token: 0x040006DC RID: 1756
			ReadContentAsBinary_End
		}

		// Token: 0x02000129 RID: 297
		private class LaterInitParam
		{
			// Token: 0x06000CA8 RID: 3240 RVA: 0x00040AF9 File Offset: 0x0003ECF9
			public LaterInitParam()
			{
			}

			// Token: 0x040006DD RID: 1757
			public bool useAsync;

			// Token: 0x040006DE RID: 1758
			public Stream inputStream;

			// Token: 0x040006DF RID: 1759
			public byte[] inputBytes;

			// Token: 0x040006E0 RID: 1760
			public int inputByteCount;

			// Token: 0x040006E1 RID: 1761
			public Uri inputbaseUri;

			// Token: 0x040006E2 RID: 1762
			public string inputUriStr;

			// Token: 0x040006E3 RID: 1763
			public XmlResolver inputUriResolver;

			// Token: 0x040006E4 RID: 1764
			public XmlParserContext inputContext;

			// Token: 0x040006E5 RID: 1765
			public TextReader inputTextReader;

			// Token: 0x040006E6 RID: 1766
			public XmlTextReaderImpl.InitInputType initType = XmlTextReaderImpl.InitInputType.Invalid;
		}

		// Token: 0x0200012A RID: 298
		private enum InitInputType
		{
			// Token: 0x040006E8 RID: 1768
			UriString,
			// Token: 0x040006E9 RID: 1769
			Stream,
			// Token: 0x040006EA RID: 1770
			TextReader,
			// Token: 0x040006EB RID: 1771
			Invalid
		}

		// Token: 0x0200012B RID: 299
		private enum ParseEndElementParseFunction
		{
			// Token: 0x040006ED RID: 1773
			CheckEndTag,
			// Token: 0x040006EE RID: 1774
			ReadData,
			// Token: 0x040006EF RID: 1775
			Done
		}

		// Token: 0x0200012C RID: 300
		private class ParseTextState
		{
			// Token: 0x06000CA9 RID: 3241 RVA: 0x00040B08 File Offset: 0x0003ED08
			public ParseTextState(int outOrChars, char[] chars, int pos, int rcount, int rpos, int orChars, char c)
			{
				this.outOrChars = outOrChars;
				this.chars = chars;
				this.pos = pos;
				this.rcount = rcount;
				this.rpos = rpos;
				this.orChars = orChars;
				this.c = c;
			}

			// Token: 0x040006F0 RID: 1776
			public int outOrChars;

			// Token: 0x040006F1 RID: 1777
			public char[] chars;

			// Token: 0x040006F2 RID: 1778
			public int pos;

			// Token: 0x040006F3 RID: 1779
			public int rcount;

			// Token: 0x040006F4 RID: 1780
			public int rpos;

			// Token: 0x040006F5 RID: 1781
			public int orChars;

			// Token: 0x040006F6 RID: 1782
			public char c;
		}

		// Token: 0x0200012D RID: 301
		private enum ParseTextFunction
		{
			// Token: 0x040006F8 RID: 1784
			ParseText,
			// Token: 0x040006F9 RID: 1785
			Entity,
			// Token: 0x040006FA RID: 1786
			Surrogate,
			// Token: 0x040006FB RID: 1787
			ReadData,
			// Token: 0x040006FC RID: 1788
			NoValue,
			// Token: 0x040006FD RID: 1789
			PartialValue
		}

		// Token: 0x0200012E RID: 302
		private struct ParsingState
		{
			// Token: 0x06000CAA RID: 3242 RVA: 0x00040B48 File Offset: 0x0003ED48
			internal void Clear()
			{
				this.chars = null;
				this.charPos = 0;
				this.charsUsed = 0;
				this.encoding = null;
				this.stream = null;
				this.decoder = null;
				this.bytes = null;
				this.bytePos = 0;
				this.bytesUsed = 0;
				this.textReader = null;
				this.lineNo = 1;
				this.lineStartPos = -1;
				this.baseUriStr = string.Empty;
				this.baseUri = null;
				this.isEof = false;
				this.isStreamEof = false;
				this.eolNormalized = true;
				this.entityResolvedManually = false;
			}

			// Token: 0x06000CAB RID: 3243 RVA: 0x00040BD7 File Offset: 0x0003EDD7
			internal void Close(bool closeInput)
			{
				if (closeInput)
				{
					if (this.stream != null)
					{
						this.stream.Close();
						return;
					}
					if (this.textReader != null)
					{
						this.textReader.Close();
					}
				}
			}

			// Token: 0x1700023A RID: 570
			// (get) Token: 0x06000CAC RID: 3244 RVA: 0x00040C03 File Offset: 0x0003EE03
			internal int LineNo
			{
				get
				{
					return this.lineNo;
				}
			}

			// Token: 0x1700023B RID: 571
			// (get) Token: 0x06000CAD RID: 3245 RVA: 0x00040C0B File Offset: 0x0003EE0B
			internal int LinePos
			{
				get
				{
					return this.charPos - this.lineStartPos;
				}
			}

			// Token: 0x040006FE RID: 1790
			internal char[] chars;

			// Token: 0x040006FF RID: 1791
			internal int charPos;

			// Token: 0x04000700 RID: 1792
			internal int charsUsed;

			// Token: 0x04000701 RID: 1793
			internal Encoding encoding;

			// Token: 0x04000702 RID: 1794
			internal bool appendMode;

			// Token: 0x04000703 RID: 1795
			internal Stream stream;

			// Token: 0x04000704 RID: 1796
			internal Decoder decoder;

			// Token: 0x04000705 RID: 1797
			internal byte[] bytes;

			// Token: 0x04000706 RID: 1798
			internal int bytePos;

			// Token: 0x04000707 RID: 1799
			internal int bytesUsed;

			// Token: 0x04000708 RID: 1800
			internal TextReader textReader;

			// Token: 0x04000709 RID: 1801
			internal int lineNo;

			// Token: 0x0400070A RID: 1802
			internal int lineStartPos;

			// Token: 0x0400070B RID: 1803
			internal string baseUriStr;

			// Token: 0x0400070C RID: 1804
			internal Uri baseUri;

			// Token: 0x0400070D RID: 1805
			internal bool isEof;

			// Token: 0x0400070E RID: 1806
			internal bool isStreamEof;

			// Token: 0x0400070F RID: 1807
			internal IDtdEntityInfo entity;

			// Token: 0x04000710 RID: 1808
			internal int entityId;

			// Token: 0x04000711 RID: 1809
			internal bool eolNormalized;

			// Token: 0x04000712 RID: 1810
			internal bool entityResolvedManually;
		}

		// Token: 0x0200012F RID: 303
		private class XmlContext
		{
			// Token: 0x06000CAE RID: 3246 RVA: 0x00040C1A File Offset: 0x0003EE1A
			internal XmlContext()
			{
				this.xmlSpace = XmlSpace.None;
				this.xmlLang = string.Empty;
				this.defaultNamespace = string.Empty;
				this.previousContext = null;
			}

			// Token: 0x06000CAF RID: 3247 RVA: 0x00040C46 File Offset: 0x0003EE46
			internal XmlContext(XmlTextReaderImpl.XmlContext previousContext)
			{
				this.xmlSpace = previousContext.xmlSpace;
				this.xmlLang = previousContext.xmlLang;
				this.defaultNamespace = previousContext.defaultNamespace;
				this.previousContext = previousContext;
			}

			// Token: 0x04000713 RID: 1811
			internal XmlSpace xmlSpace;

			// Token: 0x04000714 RID: 1812
			internal string xmlLang;

			// Token: 0x04000715 RID: 1813
			internal string defaultNamespace;

			// Token: 0x04000716 RID: 1814
			internal XmlTextReaderImpl.XmlContext previousContext;
		}

		// Token: 0x02000130 RID: 304
		private class NoNamespaceManager : XmlNamespaceManager
		{
			// Token: 0x06000CB0 RID: 3248 RVA: 0x00040C79 File Offset: 0x0003EE79
			public NoNamespaceManager()
			{
			}

			// Token: 0x1700023C RID: 572
			// (get) Token: 0x06000CB1 RID: 3249 RVA: 0x00003201 File Offset: 0x00001401
			public override string DefaultNamespace
			{
				get
				{
					return string.Empty;
				}
			}

			// Token: 0x06000CB2 RID: 3250 RVA: 0x000030EC File Offset: 0x000012EC
			public override void PushScope()
			{
			}

			// Token: 0x06000CB3 RID: 3251 RVA: 0x000020CD File Offset: 0x000002CD
			public override bool PopScope()
			{
				return false;
			}

			// Token: 0x06000CB4 RID: 3252 RVA: 0x000030EC File Offset: 0x000012EC
			public override void AddNamespace(string prefix, string uri)
			{
			}

			// Token: 0x06000CB5 RID: 3253 RVA: 0x000030EC File Offset: 0x000012EC
			public override void RemoveNamespace(string prefix, string uri)
			{
			}

			// Token: 0x06000CB6 RID: 3254 RVA: 0x000037FB File Offset: 0x000019FB
			public override IEnumerator GetEnumerator()
			{
				return null;
			}

			// Token: 0x06000CB7 RID: 3255 RVA: 0x000037FB File Offset: 0x000019FB
			public override IDictionary<string, string> GetNamespacesInScope(XmlNamespaceScope scope)
			{
				return null;
			}

			// Token: 0x06000CB8 RID: 3256 RVA: 0x00003201 File Offset: 0x00001401
			public override string LookupNamespace(string prefix)
			{
				return string.Empty;
			}

			// Token: 0x06000CB9 RID: 3257 RVA: 0x000037FB File Offset: 0x000019FB
			public override string LookupPrefix(string uri)
			{
				return null;
			}

			// Token: 0x06000CBA RID: 3258 RVA: 0x000020CD File Offset: 0x000002CD
			public override bool HasNamespace(string prefix)
			{
				return false;
			}
		}

		// Token: 0x02000131 RID: 305
		internal class DtdParserProxy : IDtdParserAdapterV1, IDtdParserAdapterWithValidation, IDtdParserAdapter
		{
			// Token: 0x06000CBB RID: 3259 RVA: 0x00040C81 File Offset: 0x0003EE81
			internal DtdParserProxy(XmlTextReaderImpl reader)
			{
				this.reader = reader;
			}

			// Token: 0x1700023D RID: 573
			// (get) Token: 0x06000CBC RID: 3260 RVA: 0x00040C90 File Offset: 0x0003EE90
			XmlNameTable IDtdParserAdapter.NameTable
			{
				get
				{
					return this.reader.DtdParserProxy_NameTable;
				}
			}

			// Token: 0x1700023E RID: 574
			// (get) Token: 0x06000CBD RID: 3261 RVA: 0x00040C9D File Offset: 0x0003EE9D
			IXmlNamespaceResolver IDtdParserAdapter.NamespaceResolver
			{
				get
				{
					return this.reader.DtdParserProxy_NamespaceResolver;
				}
			}

			// Token: 0x1700023F RID: 575
			// (get) Token: 0x06000CBE RID: 3262 RVA: 0x00040CAA File Offset: 0x0003EEAA
			Uri IDtdParserAdapter.BaseUri
			{
				get
				{
					return this.reader.DtdParserProxy_BaseUri;
				}
			}

			// Token: 0x17000240 RID: 576
			// (get) Token: 0x06000CBF RID: 3263 RVA: 0x00040CB7 File Offset: 0x0003EEB7
			bool IDtdParserAdapter.IsEof
			{
				get
				{
					return this.reader.DtdParserProxy_IsEof;
				}
			}

			// Token: 0x17000241 RID: 577
			// (get) Token: 0x06000CC0 RID: 3264 RVA: 0x00040CC4 File Offset: 0x0003EEC4
			char[] IDtdParserAdapter.ParsingBuffer
			{
				get
				{
					return this.reader.DtdParserProxy_ParsingBuffer;
				}
			}

			// Token: 0x17000242 RID: 578
			// (get) Token: 0x06000CC1 RID: 3265 RVA: 0x00040CD1 File Offset: 0x0003EED1
			int IDtdParserAdapter.ParsingBufferLength
			{
				get
				{
					return this.reader.DtdParserProxy_ParsingBufferLength;
				}
			}

			// Token: 0x17000243 RID: 579
			// (get) Token: 0x06000CC2 RID: 3266 RVA: 0x00040CDE File Offset: 0x0003EEDE
			// (set) Token: 0x06000CC3 RID: 3267 RVA: 0x00040CEB File Offset: 0x0003EEEB
			int IDtdParserAdapter.CurrentPosition
			{
				get
				{
					return this.reader.DtdParserProxy_CurrentPosition;
				}
				set
				{
					this.reader.DtdParserProxy_CurrentPosition = value;
				}
			}

			// Token: 0x17000244 RID: 580
			// (get) Token: 0x06000CC4 RID: 3268 RVA: 0x00040CF9 File Offset: 0x0003EEF9
			int IDtdParserAdapter.EntityStackLength
			{
				get
				{
					return this.reader.DtdParserProxy_EntityStackLength;
				}
			}

			// Token: 0x17000245 RID: 581
			// (get) Token: 0x06000CC5 RID: 3269 RVA: 0x00040D06 File Offset: 0x0003EF06
			bool IDtdParserAdapter.IsEntityEolNormalized
			{
				get
				{
					return this.reader.DtdParserProxy_IsEntityEolNormalized;
				}
			}

			// Token: 0x06000CC6 RID: 3270 RVA: 0x00040D13 File Offset: 0x0003EF13
			void IDtdParserAdapter.OnNewLine(int pos)
			{
				this.reader.DtdParserProxy_OnNewLine(pos);
			}

			// Token: 0x17000246 RID: 582
			// (get) Token: 0x06000CC7 RID: 3271 RVA: 0x00040D21 File Offset: 0x0003EF21
			int IDtdParserAdapter.LineNo
			{
				get
				{
					return this.reader.DtdParserProxy_LineNo;
				}
			}

			// Token: 0x17000247 RID: 583
			// (get) Token: 0x06000CC8 RID: 3272 RVA: 0x00040D2E File Offset: 0x0003EF2E
			int IDtdParserAdapter.LineStartPosition
			{
				get
				{
					return this.reader.DtdParserProxy_LineStartPosition;
				}
			}

			// Token: 0x06000CC9 RID: 3273 RVA: 0x00040D3B File Offset: 0x0003EF3B
			int IDtdParserAdapter.ReadData()
			{
				return this.reader.DtdParserProxy_ReadData();
			}

			// Token: 0x06000CCA RID: 3274 RVA: 0x00040D48 File Offset: 0x0003EF48
			int IDtdParserAdapter.ParseNumericCharRef(StringBuilder internalSubsetBuilder)
			{
				return this.reader.DtdParserProxy_ParseNumericCharRef(internalSubsetBuilder);
			}

			// Token: 0x06000CCB RID: 3275 RVA: 0x00040D56 File Offset: 0x0003EF56
			int IDtdParserAdapter.ParseNamedCharRef(bool expand, StringBuilder internalSubsetBuilder)
			{
				return this.reader.DtdParserProxy_ParseNamedCharRef(expand, internalSubsetBuilder);
			}

			// Token: 0x06000CCC RID: 3276 RVA: 0x00040D65 File Offset: 0x0003EF65
			void IDtdParserAdapter.ParsePI(StringBuilder sb)
			{
				this.reader.DtdParserProxy_ParsePI(sb);
			}

			// Token: 0x06000CCD RID: 3277 RVA: 0x00040D73 File Offset: 0x0003EF73
			void IDtdParserAdapter.ParseComment(StringBuilder sb)
			{
				this.reader.DtdParserProxy_ParseComment(sb);
			}

			// Token: 0x06000CCE RID: 3278 RVA: 0x00040D81 File Offset: 0x0003EF81
			bool IDtdParserAdapter.PushEntity(IDtdEntityInfo entity, out int entityId)
			{
				return this.reader.DtdParserProxy_PushEntity(entity, out entityId);
			}

			// Token: 0x06000CCF RID: 3279 RVA: 0x00040D90 File Offset: 0x0003EF90
			bool IDtdParserAdapter.PopEntity(out IDtdEntityInfo oldEntity, out int newEntityId)
			{
				return this.reader.DtdParserProxy_PopEntity(out oldEntity, out newEntityId);
			}

			// Token: 0x06000CD0 RID: 3280 RVA: 0x00040D9F File Offset: 0x0003EF9F
			bool IDtdParserAdapter.PushExternalSubset(string systemId, string publicId)
			{
				return this.reader.DtdParserProxy_PushExternalSubset(systemId, publicId);
			}

			// Token: 0x06000CD1 RID: 3281 RVA: 0x00040DAE File Offset: 0x0003EFAE
			void IDtdParserAdapter.PushInternalDtd(string baseUri, string internalDtd)
			{
				this.reader.DtdParserProxy_PushInternalDtd(baseUri, internalDtd);
			}

			// Token: 0x06000CD2 RID: 3282 RVA: 0x00040DBD File Offset: 0x0003EFBD
			void IDtdParserAdapter.Throw(Exception e)
			{
				this.reader.DtdParserProxy_Throw(e);
			}

			// Token: 0x06000CD3 RID: 3283 RVA: 0x00040DCB File Offset: 0x0003EFCB
			void IDtdParserAdapter.OnSystemId(string systemId, LineInfo keywordLineInfo, LineInfo systemLiteralLineInfo)
			{
				this.reader.DtdParserProxy_OnSystemId(systemId, keywordLineInfo, systemLiteralLineInfo);
			}

			// Token: 0x06000CD4 RID: 3284 RVA: 0x00040DDB File Offset: 0x0003EFDB
			void IDtdParserAdapter.OnPublicId(string publicId, LineInfo keywordLineInfo, LineInfo publicLiteralLineInfo)
			{
				this.reader.DtdParserProxy_OnPublicId(publicId, keywordLineInfo, publicLiteralLineInfo);
			}

			// Token: 0x17000248 RID: 584
			// (get) Token: 0x06000CD5 RID: 3285 RVA: 0x00040DEB File Offset: 0x0003EFEB
			bool IDtdParserAdapterWithValidation.DtdValidation
			{
				get
				{
					return this.reader.DtdParserProxy_DtdValidation;
				}
			}

			// Token: 0x17000249 RID: 585
			// (get) Token: 0x06000CD6 RID: 3286 RVA: 0x00040DF8 File Offset: 0x0003EFF8
			IValidationEventHandling IDtdParserAdapterWithValidation.ValidationEventHandling
			{
				get
				{
					return this.reader.DtdParserProxy_ValidationEventHandling;
				}
			}

			// Token: 0x1700024A RID: 586
			// (get) Token: 0x06000CD7 RID: 3287 RVA: 0x00040E05 File Offset: 0x0003F005
			bool IDtdParserAdapterV1.Normalization
			{
				get
				{
					return this.reader.DtdParserProxy_Normalization;
				}
			}

			// Token: 0x1700024B RID: 587
			// (get) Token: 0x06000CD8 RID: 3288 RVA: 0x00040E12 File Offset: 0x0003F012
			bool IDtdParserAdapterV1.Namespaces
			{
				get
				{
					return this.reader.DtdParserProxy_Namespaces;
				}
			}

			// Token: 0x1700024C RID: 588
			// (get) Token: 0x06000CD9 RID: 3289 RVA: 0x00040E1F File Offset: 0x0003F01F
			bool IDtdParserAdapterV1.V1CompatibilityMode
			{
				get
				{
					return this.reader.DtdParserProxy_V1CompatibilityMode;
				}
			}

			// Token: 0x06000CDA RID: 3290 RVA: 0x00040E2C File Offset: 0x0003F02C
			Task<int> IDtdParserAdapter.ReadDataAsync()
			{
				return this.reader.DtdParserProxy_ReadDataAsync();
			}

			// Token: 0x06000CDB RID: 3291 RVA: 0x00040E39 File Offset: 0x0003F039
			Task<int> IDtdParserAdapter.ParseNumericCharRefAsync(StringBuilder internalSubsetBuilder)
			{
				return this.reader.DtdParserProxy_ParseNumericCharRefAsync(internalSubsetBuilder);
			}

			// Token: 0x06000CDC RID: 3292 RVA: 0x00040E47 File Offset: 0x0003F047
			Task<int> IDtdParserAdapter.ParseNamedCharRefAsync(bool expand, StringBuilder internalSubsetBuilder)
			{
				return this.reader.DtdParserProxy_ParseNamedCharRefAsync(expand, internalSubsetBuilder);
			}

			// Token: 0x06000CDD RID: 3293 RVA: 0x00040E56 File Offset: 0x0003F056
			Task IDtdParserAdapter.ParsePIAsync(StringBuilder sb)
			{
				return this.reader.DtdParserProxy_ParsePIAsync(sb);
			}

			// Token: 0x06000CDE RID: 3294 RVA: 0x00040E64 File Offset: 0x0003F064
			Task IDtdParserAdapter.ParseCommentAsync(StringBuilder sb)
			{
				return this.reader.DtdParserProxy_ParseCommentAsync(sb);
			}

			// Token: 0x06000CDF RID: 3295 RVA: 0x00040E72 File Offset: 0x0003F072
			Task<Tuple<int, bool>> IDtdParserAdapter.PushEntityAsync(IDtdEntityInfo entity)
			{
				return this.reader.DtdParserProxy_PushEntityAsync(entity);
			}

			// Token: 0x06000CE0 RID: 3296 RVA: 0x00040E80 File Offset: 0x0003F080
			Task<bool> IDtdParserAdapter.PushExternalSubsetAsync(string systemId, string publicId)
			{
				return this.reader.DtdParserProxy_PushExternalSubsetAsync(systemId, publicId);
			}

			// Token: 0x04000717 RID: 1815
			private XmlTextReaderImpl reader;
		}

		// Token: 0x02000132 RID: 306
		private class NodeData : IComparable
		{
			// Token: 0x1700024D RID: 589
			// (get) Token: 0x06000CE1 RID: 3297 RVA: 0x00040E8F File Offset: 0x0003F08F
			internal static XmlTextReaderImpl.NodeData None
			{
				get
				{
					if (XmlTextReaderImpl.NodeData.s_None == null)
					{
						XmlTextReaderImpl.NodeData.s_None = new XmlTextReaderImpl.NodeData();
					}
					return XmlTextReaderImpl.NodeData.s_None;
				}
			}

			// Token: 0x06000CE2 RID: 3298 RVA: 0x00040EAD File Offset: 0x0003F0AD
			internal NodeData()
			{
				this.Clear(XmlNodeType.None);
				this.xmlContextPushed = false;
			}

			// Token: 0x1700024E RID: 590
			// (get) Token: 0x06000CE3 RID: 3299 RVA: 0x00040EC3 File Offset: 0x0003F0C3
			internal int LineNo
			{
				get
				{
					return this.lineInfo.lineNo;
				}
			}

			// Token: 0x1700024F RID: 591
			// (get) Token: 0x06000CE4 RID: 3300 RVA: 0x00040ED0 File Offset: 0x0003F0D0
			internal int LinePos
			{
				get
				{
					return this.lineInfo.linePos;
				}
			}

			// Token: 0x17000250 RID: 592
			// (get) Token: 0x06000CE5 RID: 3301 RVA: 0x00040EDD File Offset: 0x0003F0DD
			// (set) Token: 0x06000CE6 RID: 3302 RVA: 0x00040EF0 File Offset: 0x0003F0F0
			internal bool IsEmptyElement
			{
				get
				{
					return this.type == XmlNodeType.Element && this.isEmptyOrDefault;
				}
				set
				{
					this.isEmptyOrDefault = value;
				}
			}

			// Token: 0x17000251 RID: 593
			// (get) Token: 0x06000CE7 RID: 3303 RVA: 0x00040EF9 File Offset: 0x0003F0F9
			// (set) Token: 0x06000CE8 RID: 3304 RVA: 0x00040EF0 File Offset: 0x0003F0F0
			internal bool IsDefaultAttribute
			{
				get
				{
					return this.type == XmlNodeType.Attribute && this.isEmptyOrDefault;
				}
				set
				{
					this.isEmptyOrDefault = value;
				}
			}

			// Token: 0x17000252 RID: 594
			// (get) Token: 0x06000CE9 RID: 3305 RVA: 0x00040F0C File Offset: 0x0003F10C
			internal bool ValueBuffered
			{
				get
				{
					return this.value == null;
				}
			}

			// Token: 0x17000253 RID: 595
			// (get) Token: 0x06000CEA RID: 3306 RVA: 0x00040F17 File Offset: 0x0003F117
			internal string StringValue
			{
				get
				{
					if (this.value == null)
					{
						this.value = new string(this.chars, this.valueStartPos, this.valueLength);
					}
					return this.value;
				}
			}

			// Token: 0x06000CEB RID: 3307 RVA: 0x00040F44 File Offset: 0x0003F144
			internal void TrimSpacesInValue()
			{
				if (this.ValueBuffered)
				{
					XmlTextReaderImpl.StripSpaces(this.chars, this.valueStartPos, ref this.valueLength);
					return;
				}
				this.value = XmlTextReaderImpl.StripSpaces(this.value);
			}

			// Token: 0x06000CEC RID: 3308 RVA: 0x00040F77 File Offset: 0x0003F177
			internal void Clear(XmlNodeType type)
			{
				this.type = type;
				this.ClearName();
				this.value = string.Empty;
				this.valueStartPos = -1;
				this.nameWPrefix = string.Empty;
				this.schemaType = null;
				this.typedValue = null;
			}

			// Token: 0x06000CED RID: 3309 RVA: 0x00040FB1 File Offset: 0x0003F1B1
			internal void ClearName()
			{
				this.localName = string.Empty;
				this.prefix = string.Empty;
				this.ns = string.Empty;
				this.nameWPrefix = string.Empty;
			}

			// Token: 0x06000CEE RID: 3310 RVA: 0x00040FDF File Offset: 0x0003F1DF
			internal void SetLineInfo(int lineNo, int linePos)
			{
				this.lineInfo.Set(lineNo, linePos);
			}

			// Token: 0x06000CEF RID: 3311 RVA: 0x00040FEE File Offset: 0x0003F1EE
			internal void SetLineInfo2(int lineNo, int linePos)
			{
				this.lineInfo2.Set(lineNo, linePos);
			}

			// Token: 0x06000CF0 RID: 3312 RVA: 0x00040FFD File Offset: 0x0003F1FD
			internal void SetValueNode(XmlNodeType type, string value)
			{
				this.type = type;
				this.ClearName();
				this.value = value;
				this.valueStartPos = -1;
			}

			// Token: 0x06000CF1 RID: 3313 RVA: 0x0004101A File Offset: 0x0003F21A
			internal void SetValueNode(XmlNodeType type, char[] chars, int startPos, int len)
			{
				this.type = type;
				this.ClearName();
				this.value = null;
				this.chars = chars;
				this.valueStartPos = startPos;
				this.valueLength = len;
			}

			// Token: 0x06000CF2 RID: 3314 RVA: 0x00041046 File Offset: 0x0003F246
			internal void SetNamedNode(XmlNodeType type, string localName)
			{
				this.SetNamedNode(type, localName, string.Empty, localName);
			}

			// Token: 0x06000CF3 RID: 3315 RVA: 0x00041056 File Offset: 0x0003F256
			internal void SetNamedNode(XmlNodeType type, string localName, string prefix, string nameWPrefix)
			{
				this.type = type;
				this.localName = localName;
				this.prefix = prefix;
				this.nameWPrefix = nameWPrefix;
				this.ns = string.Empty;
				this.value = string.Empty;
				this.valueStartPos = -1;
			}

			// Token: 0x06000CF4 RID: 3316 RVA: 0x00041092 File Offset: 0x0003F292
			internal void SetValue(string value)
			{
				this.valueStartPos = -1;
				this.value = value;
			}

			// Token: 0x06000CF5 RID: 3317 RVA: 0x000410A2 File Offset: 0x0003F2A2
			internal void SetValue(char[] chars, int startPos, int len)
			{
				this.value = null;
				this.chars = chars;
				this.valueStartPos = startPos;
				this.valueLength = len;
			}

			// Token: 0x06000CF6 RID: 3318 RVA: 0x000410C0 File Offset: 0x0003F2C0
			internal void OnBufferInvalidated()
			{
				if (this.value == null)
				{
					this.value = new string(this.chars, this.valueStartPos, this.valueLength);
				}
				this.valueStartPos = -1;
			}

			// Token: 0x06000CF7 RID: 3319 RVA: 0x000410F0 File Offset: 0x0003F2F0
			internal void CopyTo(int valueOffset, StringBuilder sb)
			{
				if (this.value == null)
				{
					sb.Append(this.chars, this.valueStartPos + valueOffset, this.valueLength - valueOffset);
					return;
				}
				if (valueOffset <= 0)
				{
					sb.Append(this.value);
					return;
				}
				sb.Append(this.value, valueOffset, this.value.Length - valueOffset);
			}

			// Token: 0x06000CF8 RID: 3320 RVA: 0x00041150 File Offset: 0x0003F350
			internal int CopyTo(int valueOffset, char[] buffer, int offset, int length)
			{
				if (this.value == null)
				{
					int num = this.valueLength - valueOffset;
					if (num > length)
					{
						num = length;
					}
					XmlTextReaderImpl.BlockCopyChars(this.chars, this.valueStartPos + valueOffset, buffer, offset, num);
					return num;
				}
				int num2 = this.value.Length - valueOffset;
				if (num2 > length)
				{
					num2 = length;
				}
				this.value.CopyTo(valueOffset, buffer, offset, num2);
				return num2;
			}

			// Token: 0x06000CF9 RID: 3321 RVA: 0x000411B4 File Offset: 0x0003F3B4
			internal int CopyToBinary(IncrementalReadDecoder decoder, int valueOffset)
			{
				if (this.value == null)
				{
					return decoder.Decode(this.chars, this.valueStartPos + valueOffset, this.valueLength - valueOffset);
				}
				return decoder.Decode(this.value, valueOffset, this.value.Length - valueOffset);
			}

			// Token: 0x06000CFA RID: 3322 RVA: 0x00041200 File Offset: 0x0003F400
			internal void AdjustLineInfo(int valueOffset, bool isNormalized, ref LineInfo lineInfo)
			{
				if (valueOffset == 0)
				{
					return;
				}
				if (this.valueStartPos != -1)
				{
					XmlTextReaderImpl.AdjustLineInfo(this.chars, this.valueStartPos, this.valueStartPos + valueOffset, isNormalized, ref lineInfo);
					return;
				}
				XmlTextReaderImpl.AdjustLineInfo(this.value, 0, valueOffset, isNormalized, ref lineInfo);
			}

			// Token: 0x06000CFB RID: 3323 RVA: 0x0004123A File Offset: 0x0003F43A
			internal string GetNameWPrefix(XmlNameTable nt)
			{
				if (this.nameWPrefix != null)
				{
					return this.nameWPrefix;
				}
				return this.CreateNameWPrefix(nt);
			}

			// Token: 0x06000CFC RID: 3324 RVA: 0x00041254 File Offset: 0x0003F454
			internal string CreateNameWPrefix(XmlNameTable nt)
			{
				if (this.prefix.Length == 0)
				{
					this.nameWPrefix = this.localName;
				}
				else
				{
					this.nameWPrefix = nt.Add(this.prefix + ":" + this.localName);
				}
				return this.nameWPrefix;
			}

			// Token: 0x06000CFD RID: 3325 RVA: 0x000412A4 File Offset: 0x0003F4A4
			int IComparable.CompareTo(object obj)
			{
				XmlTextReaderImpl.NodeData nodeData = obj as XmlTextReaderImpl.NodeData;
				if (nodeData == null)
				{
					return 1;
				}
				if (!Ref.Equal(this.localName, nodeData.localName))
				{
					return string.CompareOrdinal(this.localName, nodeData.localName);
				}
				if (Ref.Equal(this.ns, nodeData.ns))
				{
					return 0;
				}
				return string.CompareOrdinal(this.ns, nodeData.ns);
			}

			// Token: 0x04000718 RID: 1816
			private static volatile XmlTextReaderImpl.NodeData s_None;

			// Token: 0x04000719 RID: 1817
			internal XmlNodeType type;

			// Token: 0x0400071A RID: 1818
			internal string localName;

			// Token: 0x0400071B RID: 1819
			internal string prefix;

			// Token: 0x0400071C RID: 1820
			internal string ns;

			// Token: 0x0400071D RID: 1821
			internal string nameWPrefix;

			// Token: 0x0400071E RID: 1822
			private string value;

			// Token: 0x0400071F RID: 1823
			private char[] chars;

			// Token: 0x04000720 RID: 1824
			private int valueStartPos;

			// Token: 0x04000721 RID: 1825
			private int valueLength;

			// Token: 0x04000722 RID: 1826
			internal LineInfo lineInfo;

			// Token: 0x04000723 RID: 1827
			internal LineInfo lineInfo2;

			// Token: 0x04000724 RID: 1828
			internal char quoteChar;

			// Token: 0x04000725 RID: 1829
			internal int depth;

			// Token: 0x04000726 RID: 1830
			private bool isEmptyOrDefault;

			// Token: 0x04000727 RID: 1831
			internal int entityId;

			// Token: 0x04000728 RID: 1832
			internal bool xmlContextPushed;

			// Token: 0x04000729 RID: 1833
			internal XmlTextReaderImpl.NodeData nextAttrValueChunk;

			// Token: 0x0400072A RID: 1834
			internal object schemaType;

			// Token: 0x0400072B RID: 1835
			internal object typedValue;
		}

		// Token: 0x02000133 RID: 307
		private class DtdDefaultAttributeInfoToNodeDataComparer : IComparer<object>
		{
			// Token: 0x17000254 RID: 596
			// (get) Token: 0x06000CFE RID: 3326 RVA: 0x00041308 File Offset: 0x0003F508
			internal static IComparer<object> Instance
			{
				get
				{
					return XmlTextReaderImpl.DtdDefaultAttributeInfoToNodeDataComparer.s_instance;
				}
			}

			// Token: 0x06000CFF RID: 3327 RVA: 0x00041310 File Offset: 0x0003F510
			public int Compare(object x, object y)
			{
				if (x == null)
				{
					if (y != null)
					{
						return -1;
					}
					return 0;
				}
				else
				{
					if (y == null)
					{
						return 1;
					}
					XmlTextReaderImpl.NodeData nodeData = x as XmlTextReaderImpl.NodeData;
					string localName;
					string prefix;
					if (nodeData != null)
					{
						localName = nodeData.localName;
						prefix = nodeData.prefix;
					}
					else
					{
						IDtdDefaultAttributeInfo dtdDefaultAttributeInfo = x as IDtdDefaultAttributeInfo;
						if (dtdDefaultAttributeInfo == null)
						{
							throw new XmlException("An XML error has occurred.", string.Empty);
						}
						localName = dtdDefaultAttributeInfo.LocalName;
						prefix = dtdDefaultAttributeInfo.Prefix;
					}
					nodeData = (y as XmlTextReaderImpl.NodeData);
					string localName2;
					string prefix2;
					if (nodeData != null)
					{
						localName2 = nodeData.localName;
						prefix2 = nodeData.prefix;
					}
					else
					{
						IDtdDefaultAttributeInfo dtdDefaultAttributeInfo2 = y as IDtdDefaultAttributeInfo;
						if (dtdDefaultAttributeInfo2 == null)
						{
							throw new XmlException("An XML error has occurred.", string.Empty);
						}
						localName2 = dtdDefaultAttributeInfo2.LocalName;
						prefix2 = dtdDefaultAttributeInfo2.Prefix;
					}
					int num = string.Compare(localName, localName2, StringComparison.Ordinal);
					if (num != 0)
					{
						return num;
					}
					return string.Compare(prefix, prefix2, StringComparison.Ordinal);
				}
			}

			// Token: 0x06000D00 RID: 3328 RVA: 0x00002103 File Offset: 0x00000303
			public DtdDefaultAttributeInfoToNodeDataComparer()
			{
			}

			// Token: 0x06000D01 RID: 3329 RVA: 0x000413DD File Offset: 0x0003F5DD
			// Note: this type is marked as 'beforefieldinit'.
			static DtdDefaultAttributeInfoToNodeDataComparer()
			{
			}

			// Token: 0x0400072C RID: 1836
			private static IComparer<object> s_instance = new XmlTextReaderImpl.DtdDefaultAttributeInfoToNodeDataComparer();
		}

		// Token: 0x02000134 RID: 308
		// (Invoke) Token: 0x06000D03 RID: 3331
		internal delegate void OnDefaultAttributeUseDelegate(IDtdDefaultAttributeInfo defaultAttribute, XmlTextReaderImpl coreReader);

		// Token: 0x02000135 RID: 309
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_GetValueAsync>d__473 : IAsyncStateMachine
		{
			// Token: 0x06000D06 RID: 3334 RVA: 0x000413EC File Offset: 0x0003F5EC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				string stringValue;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							if (xmlTextReaderImpl.parsingFunction < XmlTextReaderImpl.ParsingFunction.PartialTextValue)
							{
								goto IL_104;
							}
							if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.PartialTextValue)
							{
								configuredTaskAwaiter = xmlTextReaderImpl.FinishPartialValueAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_GetValueAsync>d__473>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_8D;
							}
							else
							{
								configuredTaskAwaiter = xmlTextReaderImpl.FinishOtherValueIteratorAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_GetValueAsync>d__473>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						goto IL_104;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_8D:
					configuredTaskAwaiter.GetResult();
					xmlTextReaderImpl.parsingFunction = xmlTextReaderImpl.nextParsingFunction;
					IL_104:
					stringValue = xmlTextReaderImpl.curNode.StringValue;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(stringValue);
			}

			// Token: 0x06000D07 RID: 3335 RVA: 0x00041554 File Offset: 0x0003F754
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400072D RID: 1837
			public int <>1__state;

			// Token: 0x0400072E RID: 1838
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x0400072F RID: 1839
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000730 RID: 1840
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000136 RID: 310
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishInitUriStringAsync>d__475 : IAsyncStateMachine
		{
			// Token: 0x06000D08 RID: 3336 RVA: 0x00041564 File Offset: 0x0003F764
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num - 1 <= 1)
						{
							goto IL_F1;
						}
						configuredTaskAwaiter = xmlTextReaderImpl.laterInitParam.inputUriResolver.GetEntityAsync(xmlTextReaderImpl.laterInitParam.inputbaseUri, string.Empty, typeof(Stream)).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishInitUriStringAsync>d__475>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					object result = configuredTaskAwaiter.GetResult();
					stream = (Stream)result;
					if (stream == null)
					{
						throw new XmlException("Cannot resolve '{0}'.", xmlTextReaderImpl.laterInitParam.inputUriStr);
					}
					Encoding encoding = null;
					if (xmlTextReaderImpl.laterInitParam.inputContext != null)
					{
						encoding = xmlTextReaderImpl.laterInitParam.inputContext.Encoding;
					}
					IL_F1:
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 1)
						{
							if (num == 2)
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
								configuredTaskAwaiter3 = configuredTaskAwaiter4;
								configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
								goto IL_21B;
							}
							configuredTaskAwaiter3 = xmlTextReaderImpl.InitStreamInputAsync(xmlTextReaderImpl.laterInitParam.inputbaseUri, xmlTextReaderImpl.reportedBaseUri, stream, null, 0, encoding).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishInitUriStringAsync>d__475>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter3.GetResult();
						xmlTextReaderImpl.reportedEncoding = xmlTextReaderImpl.ps.encoding;
						if (xmlTextReaderImpl.laterInitParam.inputContext == null || !xmlTextReaderImpl.laterInitParam.inputContext.HasDtdInfo)
						{
							goto IL_222;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.ProcessDtdFromParserContextAsync(xmlTextReaderImpl.laterInitParam.inputContext).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishInitUriStringAsync>d__475>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						IL_21B:
						configuredTaskAwaiter3.GetResult();
						IL_222:;
					}
					catch
					{
						stream.Close();
						throw;
					}
					xmlTextReaderImpl.laterInitParam = null;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D09 RID: 3337 RVA: 0x0004180C File Offset: 0x0003FA0C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000731 RID: 1841
			public int <>1__state;

			// Token: 0x04000732 RID: 1842
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000733 RID: 1843
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000734 RID: 1844
			private Stream <stream>5__1;

			// Token: 0x04000735 RID: 1845
			private ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000736 RID: 1846
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000137 RID: 311
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishInitStreamAsync>d__476 : IAsyncStateMachine
		{
			// Token: 0x06000D0A RID: 3338 RVA: 0x0004181C File Offset: 0x0003FA1C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_169;
						}
						Encoding encoding = null;
						if (xmlTextReaderImpl.laterInitParam.inputContext != null)
						{
							encoding = xmlTextReaderImpl.laterInitParam.inputContext.Encoding;
						}
						configuredTaskAwaiter = xmlTextReaderImpl.InitStreamInputAsync(xmlTextReaderImpl.laterInitParam.inputbaseUri, xmlTextReaderImpl.reportedBaseUri, xmlTextReaderImpl.laterInitParam.inputStream, xmlTextReaderImpl.laterInitParam.inputBytes, xmlTextReaderImpl.laterInitParam.inputByteCount, encoding).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishInitStreamAsync>d__476>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xmlTextReaderImpl.reportedEncoding = xmlTextReaderImpl.ps.encoding;
					if (xmlTextReaderImpl.laterInitParam.inputContext == null || !xmlTextReaderImpl.laterInitParam.inputContext.HasDtdInfo)
					{
						goto IL_170;
					}
					configuredTaskAwaiter = xmlTextReaderImpl.ProcessDtdFromParserContextAsync(xmlTextReaderImpl.laterInitParam.inputContext).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishInitStreamAsync>d__476>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_169:
					configuredTaskAwaiter.GetResult();
					IL_170:
					xmlTextReaderImpl.laterInitParam = null;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D0B RID: 3339 RVA: 0x000419EC File Offset: 0x0003FBEC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000737 RID: 1847
			public int <>1__state;

			// Token: 0x04000738 RID: 1848
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000739 RID: 1849
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400073A RID: 1850
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000138 RID: 312
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishInitTextReaderAsync>d__477 : IAsyncStateMachine
		{
			// Token: 0x06000D0C RID: 3340 RVA: 0x000419FC File Offset: 0x0003FBFC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_122;
						}
						configuredTaskAwaiter = xmlTextReaderImpl.InitTextReaderInputAsync(xmlTextReaderImpl.reportedBaseUri, xmlTextReaderImpl.laterInitParam.inputTextReader).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishInitTextReaderAsync>d__477>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xmlTextReaderImpl.reportedEncoding = xmlTextReaderImpl.ps.encoding;
					if (xmlTextReaderImpl.laterInitParam.inputContext == null || !xmlTextReaderImpl.laterInitParam.inputContext.HasDtdInfo)
					{
						goto IL_129;
					}
					configuredTaskAwaiter = xmlTextReaderImpl.ProcessDtdFromParserContextAsync(xmlTextReaderImpl.laterInitParam.inputContext).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishInitTextReaderAsync>d__477>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_122:
					configuredTaskAwaiter.GetResult();
					IL_129:
					xmlTextReaderImpl.laterInitParam = null;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D0D RID: 3341 RVA: 0x00041B84 File Offset: 0x0003FD84
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400073B RID: 1851
			public int <>1__state;

			// Token: 0x0400073C RID: 1852
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400073D RID: 1853
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400073E RID: 1854
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000139 RID: 313
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ReadAsync_SwitchToInteractiveXmlDecl>d__480 : IAsyncStateMachine
		{
			// Token: 0x06000D0E RID: 3342 RVA: 0x00041B94 File Offset: 0x0003FD94
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result2;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_E0;
						}
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ReadAsync_SwitchToInteractiveXmlDecl>d__480>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					bool result = configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlTextReaderImpl.ReadAsync_SwitchToInteractiveXmlDecl_Helper(result).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ReadAsync_SwitchToInteractiveXmlDecl>d__480>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_E0:
					result2 = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000D0F RID: 3343 RVA: 0x00041CC8 File Offset: 0x0003FEC8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400073F RID: 1855
			public int <>1__state;

			// Token: 0x04000740 RID: 1856
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000741 RID: 1857
			public Task<bool> task;

			// Token: 0x04000742 RID: 1858
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000743 RID: 1859
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200013A RID: 314
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SkipAsync>d__482 : IAsyncStateMachine
		{
			// Token: 0x06000D10 RID: 3344 RVA: 0x00041CD8 File Offset: 0x0003FED8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_167;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1D1;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_238;
					}
					case 4:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2E7;
					case 5:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_37B;
					default:
					{
						xmlTextReaderImpl.CheckAsyncCall();
						if (xmlTextReaderImpl.readState != ReadState.Interactive)
						{
							goto IL_39E;
						}
						if (xmlTextReaderImpl.InAttributeValueIterator)
						{
							xmlTextReaderImpl.FinishAttributeValueIterator();
							xmlTextReaderImpl.curNode = xmlTextReaderImpl.nodes[xmlTextReaderImpl.index];
							goto IL_23F;
						}
						XmlTextReaderImpl.ParsingFunction parsingFunction = xmlTextReaderImpl.parsingFunction;
						if (parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
						{
							xmlTextReaderImpl.FinishIncrementalRead();
							goto IL_23F;
						}
						switch (parsingFunction)
						{
						case XmlTextReaderImpl.ParsingFunction.PartialTextValue:
							configuredTaskAwaiter3 = xmlTextReaderImpl.SkipPartialTextValueAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipAsync>d__482>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						case XmlTextReaderImpl.ParsingFunction.InReadAttributeValue:
							goto IL_23F;
						case XmlTextReaderImpl.ParsingFunction.InReadValueChunk:
							configuredTaskAwaiter3 = xmlTextReaderImpl.FinishReadValueChunkAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipAsync>d__482>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_167;
						case XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary:
							configuredTaskAwaiter3 = xmlTextReaderImpl.FinishReadContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 2;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipAsync>d__482>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_1D1;
						case XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary:
							configuredTaskAwaiter3 = xmlTextReaderImpl.FinishReadElementContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 3;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipAsync>d__482>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_238;
						default:
							goto IL_23F;
						}
						break;
					}
					}
					configuredTaskAwaiter3.GetResult();
					goto IL_23F;
					IL_167:
					configuredTaskAwaiter3.GetResult();
					goto IL_23F;
					IL_1D1:
					configuredTaskAwaiter3.GetResult();
					goto IL_23F;
					IL_238:
					configuredTaskAwaiter3.GetResult();
					IL_23F:
					XmlNodeType type = xmlTextReaderImpl.curNode.type;
					if (type != XmlNodeType.Element)
					{
						if (type != XmlNodeType.Attribute)
						{
							goto IL_318;
						}
						xmlTextReaderImpl.outerReader.MoveToElement();
					}
					if (xmlTextReaderImpl.curNode.IsEmptyElement)
					{
						goto IL_318;
					}
					initialDepth = xmlTextReaderImpl.index;
					xmlTextReaderImpl.parsingMode = XmlTextReaderImpl.ParsingMode.SkipContent;
					IL_281:
					configuredTaskAwaiter5 = xmlTextReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 4;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipAsync>d__482>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_2E7:
					if (configuredTaskAwaiter5.GetResult() && xmlTextReaderImpl.index > initialDepth)
					{
						goto IL_281;
					}
					xmlTextReaderImpl.parsingMode = XmlTextReaderImpl.ParsingMode.Full;
					IL_318:
					configuredTaskAwaiter5 = xmlTextReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 5;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipAsync>d__482>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_37B:
					configuredTaskAwaiter5.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_39E:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D11 RID: 3345 RVA: 0x000420B4 File Offset: 0x000402B4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000744 RID: 1860
			public int <>1__state;

			// Token: 0x04000745 RID: 1861
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000746 RID: 1862
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000747 RID: 1863
			private int <initialDepth>5__1;

			// Token: 0x04000748 RID: 1864
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000749 RID: 1865
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200013B RID: 315
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBase64_AsyncHelper>d__483 : IAsyncStateMachine
		{
			// Token: 0x06000D12 RID: 3346 RVA: 0x000420C4 File Offset: 0x000402C4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_108;
						}
						configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadContentAsBase64_AsyncHelper>d__483>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter3.GetResult();
					if (!task.Result)
					{
						result = 0;
						goto IL_12B;
					}
					xmlTextReaderImpl.InitBase64Decoder();
					configuredTaskAwaiter = xmlTextReaderImpl.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadContentAsBase64_AsyncHelper>d__483>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_108:
					result = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_12B:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D13 RID: 3347 RVA: 0x0004222C File Offset: 0x0004042C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400074A RID: 1866
			public int <>1__state;

			// Token: 0x0400074B RID: 1867
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400074C RID: 1868
			public Task<bool> task;

			// Token: 0x0400074D RID: 1869
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400074E RID: 1870
			public byte[] buffer;

			// Token: 0x0400074F RID: 1871
			public int index;

			// Token: 0x04000750 RID: 1872
			public int count;

			// Token: 0x04000751 RID: 1873
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000752 RID: 1874
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200013C RID: 316
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBinHexAsync>d__485 : IAsyncStateMachine
		{
			// Token: 0x06000D14 RID: 3348 RVA: 0x0004223C File Offset: 0x0004043C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C7;
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_24A;
					}
					default:
						xmlTextReaderImpl.CheckAsyncCall();
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
						{
							if (xmlTextReaderImpl.incReadDecoder != xmlTextReaderImpl.binHexDecoder)
							{
								goto IL_1D7;
							}
							configuredTaskAwaiter3 = xmlTextReaderImpl.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadContentAsBinHexAsync>d__485>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							if (xmlTextReaderImpl.readState != ReadState.Interactive)
							{
								result = 0;
								goto IL_26D;
							}
							if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
							{
								throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
							}
							if (!XmlReader.CanReadContentAs(xmlTextReaderImpl.curNode.type))
							{
								throw xmlTextReaderImpl.CreateReadContentAsException("ReadContentAsBinHex");
							}
							configuredTaskAwaiter5 = xmlTextReaderImpl.InitReadContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 1;
								configuredTaskAwaiter2 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadContentAsBinHexAsync>d__485>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_1C7;
						}
						break;
					}
					result = configuredTaskAwaiter3.GetResult();
					goto IL_26D;
					IL_1C7:
					if (!configuredTaskAwaiter5.GetResult())
					{
						result = 0;
						goto IL_26D;
					}
					IL_1D7:
					xmlTextReaderImpl.InitBinHexDecoder();
					configuredTaskAwaiter3 = xmlTextReaderImpl.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadContentAsBinHexAsync>d__485>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_24A:
					result = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_26D:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D15 RID: 3349 RVA: 0x000424E8 File Offset: 0x000406E8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000753 RID: 1875
			public int <>1__state;

			// Token: 0x04000754 RID: 1876
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000755 RID: 1877
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000756 RID: 1878
			public byte[] buffer;

			// Token: 0x04000757 RID: 1879
			public int count;

			// Token: 0x04000758 RID: 1880
			public int index;

			// Token: 0x04000759 RID: 1881
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400075A RID: 1882
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200013D RID: 317
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBase64Async_Helper>d__486 : IAsyncStateMachine
		{
			// Token: 0x06000D16 RID: 3350 RVA: 0x000424F8 File Offset: 0x000406F8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_108;
						}
						configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadElementContentAsBase64Async_Helper>d__486>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter3.GetResult();
					if (!task.Result)
					{
						result = 0;
						goto IL_12B;
					}
					xmlTextReaderImpl.InitBase64Decoder();
					configuredTaskAwaiter = xmlTextReaderImpl.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadElementContentAsBase64Async_Helper>d__486>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_108:
					result = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_12B:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D17 RID: 3351 RVA: 0x00042660 File Offset: 0x00040860
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400075B RID: 1883
			public int <>1__state;

			// Token: 0x0400075C RID: 1884
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400075D RID: 1885
			public Task<bool> task;

			// Token: 0x0400075E RID: 1886
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400075F RID: 1887
			public byte[] buffer;

			// Token: 0x04000760 RID: 1888
			public int index;

			// Token: 0x04000761 RID: 1889
			public int count;

			// Token: 0x04000762 RID: 1890
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000763 RID: 1891
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200013E RID: 318
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBinHexAsync>d__488 : IAsyncStateMachine
		{
			// Token: 0x06000D18 RID: 3352 RVA: 0x00042670 File Offset: 0x00040870
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C3;
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_246;
					}
					default:
						xmlTextReaderImpl.CheckAsyncCall();
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary)
						{
							if (xmlTextReaderImpl.incReadDecoder != xmlTextReaderImpl.binHexDecoder)
							{
								goto IL_1D3;
							}
							configuredTaskAwaiter3 = xmlTextReaderImpl.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadElementContentAsBinHexAsync>d__488>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							if (xmlTextReaderImpl.readState != ReadState.Interactive)
							{
								result = 0;
								goto IL_269;
							}
							if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary)
							{
								throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
							}
							if (xmlTextReaderImpl.curNode.type != XmlNodeType.Element)
							{
								throw xmlTextReaderImpl.CreateReadElementContentAsException("ReadElementContentAsBinHex");
							}
							configuredTaskAwaiter5 = xmlTextReaderImpl.InitReadElementContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 1;
								configuredTaskAwaiter2 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadElementContentAsBinHexAsync>d__488>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_1C3;
						}
						break;
					}
					result = configuredTaskAwaiter3.GetResult();
					goto IL_269;
					IL_1C3:
					if (!configuredTaskAwaiter5.GetResult())
					{
						result = 0;
						goto IL_269;
					}
					IL_1D3:
					xmlTextReaderImpl.InitBinHexDecoder();
					configuredTaskAwaiter3 = xmlTextReaderImpl.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadElementContentAsBinHexAsync>d__488>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_246:
					result = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_269:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D19 RID: 3353 RVA: 0x00042918 File Offset: 0x00040B18
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000764 RID: 1892
			public int <>1__state;

			// Token: 0x04000765 RID: 1893
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000766 RID: 1894
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000767 RID: 1895
			public byte[] buffer;

			// Token: 0x04000768 RID: 1896
			public int count;

			// Token: 0x04000769 RID: 1897
			public int index;

			// Token: 0x0400076A RID: 1898
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400076B RID: 1899
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200013F RID: 319
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadValueChunkAsync>d__489 : IAsyncStateMachine
		{
			// Token: 0x06000D1A RID: 3354 RVA: 0x00042928 File Offset: 0x00040B28
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					bool flag;
					int num5;
					int num6;
					ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlTextReaderImpl.CheckAsyncCall();
						if (!XmlReader.HasValueInternal(xmlTextReaderImpl.curNode.type))
						{
							throw new InvalidOperationException(Res.GetString("The ReadValueAsChunk method is not supported on node type {0}.", new object[]
							{
								xmlTextReaderImpl.curNode.type
							}));
						}
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (xmlTextReaderImpl.parsingFunction != XmlTextReaderImpl.ParsingFunction.InReadValueChunk)
						{
							if (xmlTextReaderImpl.readState != ReadState.Interactive)
							{
								result = 0;
								goto IL_3B1;
							}
							if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.PartialTextValue)
							{
								xmlTextReaderImpl.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue;
							}
							else
							{
								xmlTextReaderImpl.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue;
								xmlTextReaderImpl.nextNextParsingFunction = xmlTextReaderImpl.nextParsingFunction;
								xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.parsingFunction;
							}
							xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.InReadValueChunk;
							xmlTextReaderImpl.readValueOffset = 0;
						}
						if (count == 0)
						{
							result = 0;
							goto IL_3B1;
						}
						readCount = 0;
						int num3 = xmlTextReaderImpl.curNode.CopyTo(xmlTextReaderImpl.readValueOffset, buffer, index + readCount, count - readCount);
						readCount += num3;
						xmlTextReaderImpl.readValueOffset += num3;
						if (readCount == count)
						{
							if (XmlCharType.IsHighSurrogate((int)buffer[index + count - 1]))
							{
								int num4 = readCount;
								readCount = num4 - 1;
								xmlTextReaderImpl.readValueOffset--;
								if (readCount == 0)
								{
									xmlTextReaderImpl.Throw("The buffer is not large enough to fit a surrogate pair. Please provide a buffer of size at least 2 characters.");
								}
							}
							result = readCount;
							goto IL_3B1;
						}
						if (xmlTextReaderImpl.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue)
						{
							xmlTextReaderImpl.curNode.SetValue(string.Empty);
							flag = false;
							num5 = 0;
							num6 = 0;
							goto IL_2F0;
						}
						goto IL_38F;
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_26E:
					Tuple<int, int, int, bool> result2 = configuredTaskAwaiter.GetResult();
					num5 = result2.Item1;
					num6 = result2.Item2;
					int outOrChars = result2.Item3;
					flag = result2.Item4;
					int num7 = count - readCount;
					if (num7 > num6 - num5)
					{
						num7 = num6 - num5;
					}
					XmlTextReaderImpl.BlockCopyChars(xmlTextReaderImpl.ps.chars, num5, buffer, index + readCount, num7);
					readCount += num7;
					num5 += num7;
					IL_2F0:
					if (readCount >= count || flag)
					{
						xmlTextReaderImpl.incReadState = (flag ? XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue : XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue);
						if (readCount == count && XmlCharType.IsHighSurrogate((int)buffer[index + count - 1]))
						{
							int num4 = readCount;
							readCount = num4 - 1;
							num5--;
							if (readCount == 0)
							{
								xmlTextReaderImpl.Throw("The buffer is not large enough to fit a surrogate pair. Please provide a buffer of size at least 2 characters.");
							}
						}
						xmlTextReaderImpl.readValueOffset = 0;
						xmlTextReaderImpl.curNode.SetValue(xmlTextReaderImpl.ps.chars, num5, num6 - num5);
					}
					else
					{
						outOrChars = 0;
						configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync(outOrChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadValueChunkAsync>d__489>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_26E;
					}
					IL_38F:
					result = readCount;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_3B1:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D1B RID: 3355 RVA: 0x00042D18 File Offset: 0x00040F18
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400076C RID: 1900
			public int <>1__state;

			// Token: 0x0400076D RID: 1901
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400076E RID: 1902
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400076F RID: 1903
			public char[] buffer;

			// Token: 0x04000770 RID: 1904
			public int count;

			// Token: 0x04000771 RID: 1905
			public int index;

			// Token: 0x04000772 RID: 1906
			private int <readCount>5__1;

			// Token: 0x04000773 RID: 1907
			private ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000140 RID: 320
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <DtdParserProxy_ParseNumericCharRefAsync>d__491 : IAsyncStateMachine
		{
			// Token: 0x06000D1C RID: 3356 RVA: 0x00042D28 File Offset: 0x00040F28
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int item;
				try
				{
					ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlTextReaderImpl.CheckAsyncCall();
						configuredTaskAwaiter = xmlTextReaderImpl.ParseNumericCharRefAsync(true, internalSubsetBuilder).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_ParseNumericCharRefAsync>d__491>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					item = configuredTaskAwaiter.GetResult().Item2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(item);
			}

			// Token: 0x06000D1D RID: 3357 RVA: 0x00042DFC File Offset: 0x00040FFC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000774 RID: 1908
			public int <>1__state;

			// Token: 0x04000775 RID: 1909
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000776 RID: 1910
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000777 RID: 1911
			public StringBuilder internalSubsetBuilder;

			// Token: 0x04000778 RID: 1912
			private ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000141 RID: 321
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <DtdParserProxy_ParsePIAsync>d__493 : IAsyncStateMachine
		{
			// Token: 0x06000D1E RID: 3358 RVA: 0x00042E0C File Offset: 0x0004100C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							xmlTextReaderImpl.CheckAsyncCall();
							if (sb == null)
							{
								pm = xmlTextReaderImpl.parsingMode;
								xmlTextReaderImpl.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
								configuredTaskAwaiter = xmlTextReaderImpl.ParsePIAsync(null).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_ParsePIAsync>d__493>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_9A;
							}
							else
							{
								configuredTaskAwaiter = xmlTextReaderImpl.ParsePIAsync(sb).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_ParsePIAsync>d__493>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						goto IL_118;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_9A:
					configuredTaskAwaiter.GetResult();
					xmlTextReaderImpl.parsingMode = pm;
					IL_118:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D1F RID: 3359 RVA: 0x00042F7C File Offset: 0x0004117C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000779 RID: 1913
			public int <>1__state;

			// Token: 0x0400077A RID: 1914
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400077B RID: 1915
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400077C RID: 1916
			public StringBuilder sb;

			// Token: 0x0400077D RID: 1917
			private XmlTextReaderImpl.ParsingMode <pm>5__1;

			// Token: 0x0400077E RID: 1918
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000142 RID: 322
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <DtdParserProxy_ParseCommentAsync>d__494 : IAsyncStateMachine
		{
			// Token: 0x06000D20 RID: 3360 RVA: 0x00042F8C File Offset: 0x0004118C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					if (num > 1)
					{
						xmlTextReaderImpl.CheckAsyncCall();
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						if (num != 0)
						{
							if (num != 1)
							{
								if (sb == null)
								{
									savedParsingMode = xmlTextReaderImpl.parsingMode;
									xmlTextReaderImpl.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
									configuredTaskAwaiter = xmlTextReaderImpl.ParseCDataOrCommentAsync(XmlNodeType.Comment).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num2 = 0;
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_ParseCommentAsync>d__494>(ref configuredTaskAwaiter, ref this);
										return;
									}
									goto IL_9F;
								}
								else
								{
									originalCurNode = xmlTextReaderImpl.curNode;
									xmlTextReaderImpl.curNode = xmlTextReaderImpl.AddNode(xmlTextReaderImpl.index + xmlTextReaderImpl.attrCount + 1, xmlTextReaderImpl.index);
									configuredTaskAwaiter = xmlTextReaderImpl.ParseCDataOrCommentAsync(XmlNodeType.Comment).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num2 = 1;
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_ParseCommentAsync>d__494>(ref configuredTaskAwaiter, ref this);
										return;
									}
								}
							}
							else
							{
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
							}
							configuredTaskAwaiter.GetResult();
							xmlTextReaderImpl.curNode.CopyTo(0, sb);
							xmlTextReaderImpl.curNode = originalCurNode;
							originalCurNode = null;
							goto IL_16E;
						}
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						IL_9F:
						configuredTaskAwaiter.GetResult();
						xmlTextReaderImpl.parsingMode = savedParsingMode;
						IL_16E:;
					}
					catch (XmlException ex)
					{
						if (!(ex.ResString == "Unexpected end of file while parsing {0} has occurred.") || xmlTextReaderImpl.ps.entity == null)
						{
							throw;
						}
						xmlTextReaderImpl.SendValidationEvent(XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", null, xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D21 RID: 3361 RVA: 0x000431B4 File Offset: 0x000413B4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400077F RID: 1919
			public int <>1__state;

			// Token: 0x04000780 RID: 1920
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000781 RID: 1921
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000782 RID: 1922
			public StringBuilder sb;

			// Token: 0x04000783 RID: 1923
			private XmlTextReaderImpl.ParsingMode <savedParsingMode>5__1;

			// Token: 0x04000784 RID: 1924
			private XmlTextReaderImpl.NodeData <originalCurNode>5__2;

			// Token: 0x04000785 RID: 1925
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000143 RID: 323
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <DtdParserProxy_PushEntityAsync>d__495 : IAsyncStateMachine
		{
			// Token: 0x06000D22 RID: 3362 RVA: 0x000431C4 File Offset: 0x000413C4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, bool> result;
				try
				{
					bool item;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlTextReaderImpl.CheckAsyncCall();
						if (!entity.IsExternal)
						{
							xmlTextReaderImpl.PushInternalEntity(entity);
							item = true;
							goto IL_B8;
						}
						if (xmlTextReaderImpl.IsResolverNull)
						{
							result = new Tuple<int, bool>(-1, false);
							goto IL_E5;
						}
						configuredTaskAwaiter = xmlTextReaderImpl.PushExternalEntityAsync(entity).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_PushEntityAsync>d__495>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					item = configuredTaskAwaiter.GetResult();
					IL_B8:
					result = new Tuple<int, bool>(xmlTextReaderImpl.ps.entityId, item);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_E5:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D23 RID: 3363 RVA: 0x000432DC File Offset: 0x000414DC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000786 RID: 1926
			public int <>1__state;

			// Token: 0x04000787 RID: 1927
			public AsyncTaskMethodBuilder<Tuple<int, bool>> <>t__builder;

			// Token: 0x04000788 RID: 1928
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000789 RID: 1929
			public IDtdEntityInfo entity;

			// Token: 0x0400078A RID: 1930
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000144 RID: 324
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <DtdParserProxy_PushExternalSubsetAsync>d__496 : IAsyncStateMachine
		{
			// Token: 0x06000D24 RID: 3364 RVA: 0x000432EC File Offset: 0x000414EC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_18C;
					}
					case 2:
						configuredTaskAwaiter7 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1F3;
					default:
						xmlTextReaderImpl.CheckAsyncCall();
						if (xmlTextReaderImpl.IsResolverNull)
						{
							result = false;
							goto IL_22A;
						}
						if (xmlTextReaderImpl.ps.baseUri == null && !string.IsNullOrEmpty(xmlTextReaderImpl.ps.baseUriStr))
						{
							xmlTextReaderImpl.ps.baseUri = xmlTextReaderImpl.xmlResolver.ResolveUri(null, xmlTextReaderImpl.ps.baseUriStr);
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.PushExternalEntityOrSubsetAsync(publicId, systemId, xmlTextReaderImpl.ps.baseUri, null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_PushExternalSubsetAsync>d__496>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter3.GetResult();
					xmlTextReaderImpl.ps.entity = null;
					xmlTextReaderImpl.ps.entityId = 0;
					initialPos = xmlTextReaderImpl.ps.charPos;
					if (!xmlTextReaderImpl.v1Compat)
					{
						goto IL_194;
					}
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_PushExternalSubsetAsync>d__496>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_18C:
					configuredTaskAwaiter5.GetResult();
					IL_194:
					configuredTaskAwaiter7 = xmlTextReaderImpl.ParseXmlDeclarationAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter7.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter7;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<DtdParserProxy_PushExternalSubsetAsync>d__496>(ref configuredTaskAwaiter7, ref this);
						return;
					}
					IL_1F3:
					if (!configuredTaskAwaiter7.GetResult())
					{
						xmlTextReaderImpl.ps.charPos = initialPos;
					}
					result = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_22A:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D25 RID: 3365 RVA: 0x00043554 File Offset: 0x00041754
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400078B RID: 1931
			public int <>1__state;

			// Token: 0x0400078C RID: 1932
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x0400078D RID: 1933
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400078E RID: 1934
			public string publicId;

			// Token: 0x0400078F RID: 1935
			public string systemId;

			// Token: 0x04000790 RID: 1936
			private int <initialPos>5__1;

			// Token: 0x04000791 RID: 1937
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000792 RID: 1938
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x04000793 RID: 1939
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000145 RID: 325
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InitStreamInputAsync>d__499 : IAsyncStateMachine
		{
			// Token: 0x06000D26 RID: 3366 RVA: 0x00043564 File Offset: 0x00041764
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num != 1)
						{
							xmlTextReaderImpl.ps.stream = stream;
							xmlTextReaderImpl.ps.baseUri = baseUri;
							xmlTextReaderImpl.ps.baseUriStr = baseUriStr;
							int num3;
							if (bytes != null)
							{
								xmlTextReaderImpl.ps.bytes = bytes;
								xmlTextReaderImpl.ps.bytesUsed = byteCount;
								num3 = xmlTextReaderImpl.ps.bytes.Length;
							}
							else
							{
								if (xmlTextReaderImpl.laterInitParam != null && xmlTextReaderImpl.laterInitParam.useAsync)
								{
									num3 = 65536;
								}
								else
								{
									num3 = XmlReader.CalcBufferSize(stream);
								}
								if (xmlTextReaderImpl.ps.bytes == null || xmlTextReaderImpl.ps.bytes.Length < num3)
								{
									xmlTextReaderImpl.ps.bytes = new byte[num3];
								}
							}
							if (xmlTextReaderImpl.ps.chars == null || xmlTextReaderImpl.ps.chars.Length < num3 + 1)
							{
								xmlTextReaderImpl.ps.chars = new char[num3 + 1];
							}
							xmlTextReaderImpl.ps.bytePos = 0;
							goto IL_1E3;
						}
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_31C;
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_1B7:
					int result = configuredTaskAwaiter.GetResult();
					if (result == 0)
					{
						xmlTextReaderImpl.ps.isStreamEof = true;
						goto IL_210;
					}
					XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
					xmlTextReaderImpl2.ps.bytesUsed = xmlTextReaderImpl2.ps.bytesUsed + result;
					IL_1E3:
					if (xmlTextReaderImpl.ps.bytesUsed < 4 && xmlTextReaderImpl.ps.bytes.Length - xmlTextReaderImpl.ps.bytesUsed > 0)
					{
						configuredTaskAwaiter = stream.ReadAsync(xmlTextReaderImpl.ps.bytes, xmlTextReaderImpl.ps.bytesUsed, xmlTextReaderImpl.ps.bytes.Length - xmlTextReaderImpl.ps.bytesUsed).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<InitStreamInputAsync>d__499>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_1B7;
					}
					IL_210:
					if (encoding == null)
					{
						encoding = xmlTextReaderImpl.DetectEncoding();
					}
					xmlTextReaderImpl.SetupEncoding(encoding);
					byte[] preamble = xmlTextReaderImpl.ps.encoding.GetPreamble();
					int num4 = preamble.Length;
					int num5 = 0;
					while (num5 < num4 && num5 < xmlTextReaderImpl.ps.bytesUsed && xmlTextReaderImpl.ps.bytes[num5] == preamble[num5])
					{
						num5++;
					}
					if (num5 == num4)
					{
						xmlTextReaderImpl.ps.bytePos = num4;
					}
					xmlTextReaderImpl.documentStartBytePos = xmlTextReaderImpl.ps.bytePos;
					xmlTextReaderImpl.ps.eolNormalized = !xmlTextReaderImpl.normalize;
					xmlTextReaderImpl.ps.appendMode = true;
					configuredTaskAwaiter = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<InitStreamInputAsync>d__499>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_31C:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D27 RID: 3367 RVA: 0x000438E0 File Offset: 0x00041AE0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000794 RID: 1940
			public int <>1__state;

			// Token: 0x04000795 RID: 1941
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000796 RID: 1942
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000797 RID: 1943
			public Stream stream;

			// Token: 0x04000798 RID: 1944
			public Uri baseUri;

			// Token: 0x04000799 RID: 1945
			public string baseUriStr;

			// Token: 0x0400079A RID: 1946
			public byte[] bytes;

			// Token: 0x0400079B RID: 1947
			public int byteCount;

			// Token: 0x0400079C RID: 1948
			public Encoding encoding;

			// Token: 0x0400079D RID: 1949
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000146 RID: 326
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadDataAsync>d__505 : IAsyncStateMachine
		{
			// Token: 0x06000D28 RID: 3368 RVA: 0x000438F0 File Offset: 0x00041AF0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_50E;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_5C7;
					}
					default:
						if (xmlTextReaderImpl.ps.isEof)
						{
							result = 0;
							goto IL_657;
						}
						if (xmlTextReaderImpl.ps.appendMode)
						{
							if (xmlTextReaderImpl.ps.charsUsed == xmlTextReaderImpl.ps.chars.Length - 1)
							{
								for (int i = 0; i < xmlTextReaderImpl.attrCount; i++)
								{
									xmlTextReaderImpl.nodes[xmlTextReaderImpl.index + i + 1].OnBufferInvalidated();
								}
								char[] array = new char[xmlTextReaderImpl.ps.chars.Length * 2];
								XmlTextReaderImpl.BlockCopyChars(xmlTextReaderImpl.ps.chars, 0, array, 0, xmlTextReaderImpl.ps.chars.Length);
								xmlTextReaderImpl.ps.chars = array;
							}
							if (xmlTextReaderImpl.ps.stream != null && xmlTextReaderImpl.ps.bytesUsed - xmlTextReaderImpl.ps.bytePos < 6 && xmlTextReaderImpl.ps.bytes.Length - xmlTextReaderImpl.ps.bytesUsed < 6)
							{
								byte[] array2 = new byte[xmlTextReaderImpl.ps.bytes.Length * 2];
								XmlTextReaderImpl.BlockCopy(xmlTextReaderImpl.ps.bytes, 0, array2, 0, xmlTextReaderImpl.ps.bytesUsed);
								xmlTextReaderImpl.ps.bytes = array2;
							}
							charsRead = xmlTextReaderImpl.ps.chars.Length - xmlTextReaderImpl.ps.charsUsed - 1;
							if (charsRead > 80)
							{
								charsRead = 80;
							}
						}
						else
						{
							int num3 = xmlTextReaderImpl.ps.chars.Length;
							if (num3 - xmlTextReaderImpl.ps.charsUsed <= num3 / 2)
							{
								for (int j = 0; j < xmlTextReaderImpl.attrCount; j++)
								{
									xmlTextReaderImpl.nodes[xmlTextReaderImpl.index + j + 1].OnBufferInvalidated();
								}
								int num4 = xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos;
								if (num4 < num3 - 1)
								{
									xmlTextReaderImpl.ps.lineStartPos = xmlTextReaderImpl.ps.lineStartPos - xmlTextReaderImpl.ps.charPos;
									if (num4 > 0)
									{
										XmlTextReaderImpl.BlockCopyChars(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, xmlTextReaderImpl.ps.chars, 0, num4);
									}
									xmlTextReaderImpl.ps.charPos = 0;
									xmlTextReaderImpl.ps.charsUsed = num4;
								}
								else
								{
									char[] array3 = new char[xmlTextReaderImpl.ps.chars.Length * 2];
									XmlTextReaderImpl.BlockCopyChars(xmlTextReaderImpl.ps.chars, 0, array3, 0, xmlTextReaderImpl.ps.chars.Length);
									xmlTextReaderImpl.ps.chars = array3;
								}
							}
							if (xmlTextReaderImpl.ps.stream != null)
							{
								int num5 = xmlTextReaderImpl.ps.bytesUsed - xmlTextReaderImpl.ps.bytePos;
								if (num5 <= 128)
								{
									if (num5 == 0)
									{
										xmlTextReaderImpl.ps.bytesUsed = 0;
									}
									else
									{
										XmlTextReaderImpl.BlockCopy(xmlTextReaderImpl.ps.bytes, xmlTextReaderImpl.ps.bytePos, xmlTextReaderImpl.ps.bytes, 0, num5);
										xmlTextReaderImpl.ps.bytesUsed = num5;
									}
									xmlTextReaderImpl.ps.bytePos = 0;
								}
							}
							charsRead = xmlTextReaderImpl.ps.chars.Length - xmlTextReaderImpl.ps.charsUsed - 1;
						}
						if (xmlTextReaderImpl.ps.stream != null)
						{
							if (xmlTextReaderImpl.ps.isStreamEof || xmlTextReaderImpl.ps.bytePos != xmlTextReaderImpl.ps.bytesUsed || xmlTextReaderImpl.ps.bytes.Length - xmlTextReaderImpl.ps.bytesUsed <= 0)
							{
								goto IL_471;
							}
							configuredTaskAwaiter = xmlTextReaderImpl.ps.stream.ReadAsync(xmlTextReaderImpl.ps.bytes, xmlTextReaderImpl.ps.bytesUsed, xmlTextReaderImpl.ps.bytes.Length - xmlTextReaderImpl.ps.bytesUsed).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadDataAsync>d__505>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							if (xmlTextReaderImpl.ps.textReader == null)
							{
								charsRead = 0;
								goto IL_5F6;
							}
							configuredTaskAwaiter = xmlTextReaderImpl.ps.textReader.ReadAsync(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charsUsed, xmlTextReaderImpl.ps.chars.Length - xmlTextReaderImpl.ps.charsUsed - 1).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 2;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadDataAsync>d__505>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_5C7;
						}
						break;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					if (result2 == 0)
					{
						xmlTextReaderImpl.ps.isStreamEof = true;
					}
					XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
					xmlTextReaderImpl2.ps.bytesUsed = xmlTextReaderImpl2.ps.bytesUsed + result2;
					IL_471:
					int bytePos = xmlTextReaderImpl.ps.bytePos;
					charsRead = xmlTextReaderImpl.GetChars(charsRead);
					if (charsRead != 0 || xmlTextReaderImpl.ps.bytePos == bytePos)
					{
						goto IL_5F6;
					}
					configuredTaskAwaiter = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadDataAsync>d__505>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_50E:
					result = configuredTaskAwaiter.GetResult();
					goto IL_657;
					IL_5C7:
					int result3 = configuredTaskAwaiter.GetResult();
					charsRead = result3;
					XmlTextReaderImpl xmlTextReaderImpl3 = xmlTextReaderImpl;
					xmlTextReaderImpl3.ps.charsUsed = xmlTextReaderImpl3.ps.charsUsed + charsRead;
					IL_5F6:
					xmlTextReaderImpl.RegisterConsumedCharacters((long)charsRead, xmlTextReaderImpl.InEntity);
					if (charsRead == 0)
					{
						xmlTextReaderImpl.ps.isEof = true;
					}
					xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charsUsed] = '\0';
					result = charsRead;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_657:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D29 RID: 3369 RVA: 0x00043F84 File Offset: 0x00042184
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400079E RID: 1950
			public int <>1__state;

			// Token: 0x0400079F RID: 1951
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x040007A0 RID: 1952
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007A1 RID: 1953
			private int <charsRead>5__1;

			// Token: 0x040007A2 RID: 1954
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000147 RID: 327
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseXmlDeclarationAsync>d__506 : IAsyncStateMachine
		{
			// Token: 0x06000D2A RID: 3370 RVA: 0x00043F94 File Offset: 0x00042194
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_20B;
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3F4;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_464;
					}
					case 4:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_51F;
					case 5:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_7A7;
					case 6:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_88E;
					case 7:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_C7E;
					case 8:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_D20;
					case 9:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_E51;
					}
					default:
						goto IL_A8;
					}
					IL_9C:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						goto IL_D47;
					}
					IL_A8:
					if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos >= 6)
					{
						if (!XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, 5, "<?xml") || xmlTextReaderImpl.xmlCharType.IsNameSingleChar(xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos + 5]))
						{
							goto IL_D47;
						}
						if (!isTextDecl)
						{
							xmlTextReaderImpl.curNode.SetLineInfo(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos + 2);
							xmlTextReaderImpl.curNode.SetNamedNode(XmlNodeType.XmlDeclaration, xmlTextReaderImpl.Xml);
						}
						XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
						xmlTextReaderImpl2.ps.charPos = xmlTextReaderImpl2.ps.charPos + 5;
						sb = (isTextDecl ? new StringBuilder() : xmlTextReaderImpl.stringBuilder);
						xmlDeclState = 0;
						encoding = null;
					}
					else
					{
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_9C;
					}
					IL_18B:
					originalSbLen = sb.Length;
					configuredTaskAwaiter3 = xmlTextReaderImpl.EatWhitespacesAsync((xmlDeclState == 0) ? null : sb).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_20B:
					int num3 = configuredTaskAwaiter3.GetResult();
					wsCount = num3;
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] == '?')
					{
						sb.Length = originalSbLen;
						if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos + 1] == '>')
						{
							if (xmlDeclState == 0)
							{
								xmlTextReaderImpl.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
							}
							XmlTextReaderImpl xmlTextReaderImpl3 = xmlTextReaderImpl;
							xmlTextReaderImpl3.ps.charPos = xmlTextReaderImpl3.ps.charPos + 2;
							if (!isTextDecl)
							{
								xmlTextReaderImpl.curNode.SetValue(sb.ToString());
								sb.Length = 0;
								xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.parsingFunction;
								xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.ResetAttributesRootLevel;
							}
							if (encoding == null)
							{
								if (isTextDecl)
								{
									xmlTextReaderImpl.Throw("Invalid text declaration.");
								}
								if (xmlTextReaderImpl.afterResetState)
								{
									string webName = xmlTextReaderImpl.ps.encoding.WebName;
									if (webName != "utf-8" && webName != "utf-16" && webName != "utf-16BE" && !(xmlTextReaderImpl.ps.encoding is Ucs4Encoding))
									{
										xmlTextReaderImpl.Throw("'{0}' is an invalid value for the 'encoding' attribute. The encoding cannot be switched after a call to ResetState.", (xmlTextReaderImpl.ps.encoding.GetByteCount("A") == 1) ? "UTF-8" : "UTF-16");
									}
								}
								if (!(xmlTextReaderImpl.ps.decoder is SafeAsciiDecoder))
								{
									goto IL_46B;
								}
								configuredTaskAwaiter4 = xmlTextReaderImpl.SwitchEncodingToUTF8Async().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter4.IsCompleted)
								{
									num2 = 2;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter4, ref this);
									return;
								}
								goto IL_3F4;
							}
							else
							{
								configuredTaskAwaiter4 = xmlTextReaderImpl.SwitchEncodingAsync(encoding).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter4.IsCompleted)
								{
									num2 = 3;
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter4, ref this);
									return;
								}
								goto IL_464;
							}
						}
						else
						{
							if (xmlTextReaderImpl.ps.charPos + 1 == xmlTextReaderImpl.ps.charsUsed)
							{
								goto IL_CB1;
							}
							xmlTextReaderImpl.ThrowUnexpectedToken("'>'");
						}
					}
					if (wsCount == 0 && xmlDeclState != 0)
					{
						xmlTextReaderImpl.ThrowUnexpectedToken("?>");
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ParseNameAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 4;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_51F;
					IL_3F4:
					configuredTaskAwaiter4.GetResult();
					goto IL_46B;
					IL_464:
					configuredTaskAwaiter4.GetResult();
					IL_46B:
					xmlTextReaderImpl.ps.appendMode = false;
					result = true;
					goto IL_E81;
					IL_51F:
					int result2 = configuredTaskAwaiter3.GetResult();
					attr = null;
					char c = xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos];
					if (c != 'e')
					{
						if (c != 's')
						{
							if (c == 'v' && XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, result2 - xmlTextReaderImpl.ps.charPos, "version") && xmlDeclState == 0)
							{
								if (!isTextDecl)
								{
									attr = xmlTextReaderImpl.AddAttributeNoChecks("version", 1);
									goto IL_6BF;
								}
								goto IL_6BF;
							}
						}
						else if (XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, result2 - xmlTextReaderImpl.ps.charPos, "standalone") && (xmlDeclState == 1 || xmlDeclState == 2) && !isTextDecl)
						{
							if (!isTextDecl)
							{
								attr = xmlTextReaderImpl.AddAttributeNoChecks("standalone", 1);
							}
							xmlDeclState = 2;
							goto IL_6BF;
						}
					}
					else if (XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, result2 - xmlTextReaderImpl.ps.charPos, "encoding") && (xmlDeclState == 1 || (isTextDecl && xmlDeclState == 0)))
					{
						if (!isTextDecl)
						{
							attr = xmlTextReaderImpl.AddAttributeNoChecks("encoding", 1);
						}
						xmlDeclState = 1;
						goto IL_6BF;
					}
					xmlTextReaderImpl.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
					IL_6BF:
					if (!isTextDecl)
					{
						attr.SetLineInfo(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
					}
					sb.Append(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, result2 - xmlTextReaderImpl.ps.charPos);
					xmlTextReaderImpl.ps.charPos = result2;
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] == '=')
					{
						goto IL_7D5;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.EatWhitespacesAsync(sb).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 5;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_7A7:
					configuredTaskAwaiter3.GetResult();
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != '=')
					{
						xmlTextReaderImpl.ThrowUnexpectedToken("=");
					}
					IL_7D5:
					sb.Append('=');
					XmlTextReaderImpl xmlTextReaderImpl4 = xmlTextReaderImpl;
					xmlTextReaderImpl4.ps.charPos = xmlTextReaderImpl4.ps.charPos + 1;
					quoteChar = xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos];
					if (quoteChar == '"' || quoteChar == '\'')
					{
						goto IL_8D7;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.EatWhitespacesAsync(sb).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 6;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_88E:
					configuredTaskAwaiter3.GetResult();
					quoteChar = xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos];
					if (quoteChar != '"' && quoteChar != '\'')
					{
						xmlTextReaderImpl.ThrowUnexpectedToken("\"", "'");
					}
					IL_8D7:
					sb.Append(quoteChar);
					XmlTextReaderImpl xmlTextReaderImpl5 = xmlTextReaderImpl;
					xmlTextReaderImpl5.ps.charPos = xmlTextReaderImpl5.ps.charPos + 1;
					if (!isTextDecl)
					{
						attr.quoteChar = quoteChar;
						attr.SetLineInfo2(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
					}
					pos = xmlTextReaderImpl.ps.charPos;
					IL_944:
					char[] chars = xmlTextReaderImpl.ps.chars;
					while ((xmlTextReaderImpl.xmlCharType.charProperties[(int)chars[pos]] & 128) != 0)
					{
						num3 = pos;
						pos = num3 + 1;
					}
					if (xmlTextReaderImpl.ps.chars[pos] == quoteChar)
					{
						switch (xmlDeclState)
						{
						case 0:
							if (XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos, "1.0"))
							{
								if (!isTextDecl)
								{
									attr.SetValue(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos);
								}
								xmlDeclState = 1;
							}
							else
							{
								string arg = new string(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos);
								xmlTextReaderImpl.Throw("Version number '{0}' is invalid.", arg);
							}
							break;
						case 1:
						{
							string text = new string(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos);
							encoding = xmlTextReaderImpl.CheckEncoding(text);
							if (!isTextDecl)
							{
								attr.SetValue(text);
							}
							xmlDeclState = 2;
							break;
						}
						case 2:
							if (XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos, "yes"))
							{
								xmlTextReaderImpl.standalone = true;
							}
							else if (XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos, "no"))
							{
								xmlTextReaderImpl.standalone = false;
							}
							else
							{
								xmlTextReaderImpl.Throw("Syntax for an XML declaration is invalid.", xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos - 1);
							}
							if (!isTextDecl)
							{
								attr.SetValue(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos);
							}
							xmlDeclState = 3;
							break;
						}
						sb.Append(chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos);
						sb.Append(quoteChar);
						xmlTextReaderImpl.ps.charPos = pos + 1;
						goto IL_18B;
					}
					if (pos != xmlTextReaderImpl.ps.charsUsed)
					{
						xmlTextReaderImpl.Throw(isTextDecl ? "Invalid text declaration." : "Syntax for an XML declaration is invalid.");
						goto IL_CB1;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 7;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_C7E:
					if (configuredTaskAwaiter3.GetResult() != 0)
					{
						goto IL_944;
					}
					xmlTextReaderImpl.Throw("There is an unclosed literal string.");
					IL_CB1:
					bool flag = xmlTextReaderImpl.ps.isEof;
					if (flag)
					{
						goto IL_D2C;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 8;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_D20:
					flag = (configuredTaskAwaiter3.GetResult() == 0);
					IL_D2C:
					if (flag)
					{
						xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
					}
					attr = null;
					goto IL_18B;
					IL_D47:
					if (!isTextDecl)
					{
						xmlTextReaderImpl.parsingFunction = xmlTextReaderImpl.nextParsingFunction;
					}
					if (xmlTextReaderImpl.afterResetState)
					{
						string webName2 = xmlTextReaderImpl.ps.encoding.WebName;
						if (webName2 != "utf-8" && webName2 != "utf-16" && webName2 != "utf-16BE" && !(xmlTextReaderImpl.ps.encoding is Ucs4Encoding))
						{
							xmlTextReaderImpl.Throw("'{0}' is an invalid value for the 'encoding' attribute. The encoding cannot be switched after a call to ResetState.", (xmlTextReaderImpl.ps.encoding.GetByteCount("A") == 1) ? "UTF-8" : "UTF-16");
						}
					}
					if (!(xmlTextReaderImpl.ps.decoder is SafeAsciiDecoder))
					{
						goto IL_E58;
					}
					configuredTaskAwaiter4 = xmlTextReaderImpl.SwitchEncodingToUTF8Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 9;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseXmlDeclarationAsync>d__506>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_E51:
					configuredTaskAwaiter4.GetResult();
					IL_E58:
					xmlTextReaderImpl.ps.appendMode = false;
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_E81:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D2B RID: 3371 RVA: 0x00044E54 File Offset: 0x00043054
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007A3 RID: 1955
			public int <>1__state;

			// Token: 0x040007A4 RID: 1956
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040007A5 RID: 1957
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007A6 RID: 1958
			public bool isTextDecl;

			// Token: 0x040007A7 RID: 1959
			private StringBuilder <sb>5__1;

			// Token: 0x040007A8 RID: 1960
			private int <xmlDeclState>5__2;

			// Token: 0x040007A9 RID: 1961
			private int <originalSbLen>5__3;

			// Token: 0x040007AA RID: 1962
			private Encoding <encoding>5__4;

			// Token: 0x040007AB RID: 1963
			private int <wsCount>5__5;

			// Token: 0x040007AC RID: 1964
			private XmlTextReaderImpl.NodeData <attr>5__6;

			// Token: 0x040007AD RID: 1965
			private int <pos>5__7;

			// Token: 0x040007AE RID: 1966
			private char <quoteChar>5__8;

			// Token: 0x040007AF RID: 1967
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040007B0 RID: 1968
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000148 RID: 328
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseDocumentContentAsync_ParseEntity>d__509 : IAsyncStateMachine
		{
			// Token: 0x06000D2C RID: 3372 RVA: 0x00044E64 File Offset: 0x00043064
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_158;
					}
					case 2:
						configuredTaskAwaiter7 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C7;
					case 3:
						configuredTaskAwaiter7 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_238;
					case 4:
						configuredTaskAwaiter7 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2A3;
					default:
					{
						int charPos = xmlTextReaderImpl.ps.charPos;
						if (xmlTextReaderImpl.fragmentType == XmlNodeType.Document)
						{
							xmlTextReaderImpl.Throw(charPos, "Data at the root level is invalid.");
							result = false;
							goto IL_2C6;
						}
						if (xmlTextReaderImpl.fragmentType == XmlNodeType.None)
						{
							xmlTextReaderImpl.fragmentType = XmlNodeType.Element;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.HandleEntityReferenceAsync(false, XmlTextReaderImpl.EntityExpandType.OnlyGeneral).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDocumentContentAsync_ParseEntity>d__509>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					}
					XmlTextReaderImpl.EntityType item = configuredTaskAwaiter3.GetResult().Item2;
					if (item > XmlTextReaderImpl.EntityType.CharacterNamed)
					{
						if (item == XmlTextReaderImpl.EntityType.Unexpanded)
						{
							if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.EntityReference)
							{
								xmlTextReaderImpl.parsingFunction = xmlTextReaderImpl.nextParsingFunction;
							}
							configuredTaskAwaiter5 = xmlTextReaderImpl.ParseEntityReferenceAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDocumentContentAsync_ParseEntity>d__509>(ref configuredTaskAwaiter5, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter7 = xmlTextReaderImpl.ParseDocumentContentAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter7.IsCompleted)
							{
								num2 = 4;
								configuredTaskAwaiter2 = configuredTaskAwaiter7;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDocumentContentAsync_ParseEntity>d__509>(ref configuredTaskAwaiter7, ref this);
								return;
							}
							goto IL_2A3;
						}
					}
					else
					{
						configuredTaskAwaiter7 = xmlTextReaderImpl.ParseTextAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter7.IsCompleted)
						{
							num2 = 2;
							configuredTaskAwaiter2 = configuredTaskAwaiter7;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDocumentContentAsync_ParseEntity>d__509>(ref configuredTaskAwaiter7, ref this);
							return;
						}
						goto IL_1C7;
					}
					IL_158:
					configuredTaskAwaiter5.GetResult();
					result = true;
					goto IL_2C6;
					IL_1C7:
					if (configuredTaskAwaiter7.GetResult())
					{
						result = true;
						goto IL_2C6;
					}
					configuredTaskAwaiter7 = xmlTextReaderImpl.ParseDocumentContentAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter7.IsCompleted)
					{
						num2 = 3;
						configuredTaskAwaiter2 = configuredTaskAwaiter7;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDocumentContentAsync_ParseEntity>d__509>(ref configuredTaskAwaiter7, ref this);
						return;
					}
					IL_238:
					result = configuredTaskAwaiter7.GetResult();
					goto IL_2C6;
					IL_2A3:
					result = configuredTaskAwaiter7.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2C6:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D2D RID: 3373 RVA: 0x00045168 File Offset: 0x00043368
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007B1 RID: 1969
			public int <>1__state;

			// Token: 0x040007B2 RID: 1970
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040007B3 RID: 1971
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007B4 RID: 1972
			private ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040007B5 RID: 1973
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x040007B6 RID: 1974
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000149 RID: 329
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ParseDocumentContentAsync_WhiteSpace>d__511 : IAsyncStateMachine
		{
			// Token: 0x06000D2E RID: 3374 RVA: 0x00045178 File Offset: 0x00043378
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_FB;
						}
						configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseDocumentContentAsync_WhiteSpace>d__511>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter3.GetResult())
					{
						if (xmlTextReaderImpl.fragmentType == XmlNodeType.None && xmlTextReaderImpl.curNode.type == XmlNodeType.Text)
						{
							xmlTextReaderImpl.fragmentType = XmlNodeType.Element;
						}
						result = true;
						goto IL_11E;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ParseDocumentContentAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseDocumentContentAsync_WhiteSpace>d__511>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_FB:
					result = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_11E:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D2F RID: 3375 RVA: 0x000452C8 File Offset: 0x000434C8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007B7 RID: 1975
			public int <>1__state;

			// Token: 0x040007B8 RID: 1976
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040007B9 RID: 1977
			public Task<bool> task;

			// Token: 0x040007BA RID: 1978
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007BB RID: 1979
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200014A RID: 330
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseDocumentContentAsync_ReadData>d__512 : IAsyncStateMachine
		{
			// Token: 0x06000D30 RID: 3376 RVA: 0x000452D8 File Offset: 0x000434D8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_E8;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_18A;
					}
					default:
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDocumentContentAsync_ReadData>d__512>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					if (configuredTaskAwaiter3.GetResult() != 0)
					{
						configuredTaskAwaiter4 = xmlTextReaderImpl.ParseDocumentContentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDocumentContentAsync_ReadData>d__512>(ref configuredTaskAwaiter4, ref this);
							return;
						}
					}
					else
					{
						if (needMoreChars)
						{
							xmlTextReaderImpl.Throw("Data at the root level is invalid.");
						}
						if (!xmlTextReaderImpl.InEntity)
						{
							if (!xmlTextReaderImpl.rootElementParsed && xmlTextReaderImpl.fragmentType == XmlNodeType.Document)
							{
								xmlTextReaderImpl.ThrowWithoutLineInfo("Root element is missing.");
							}
							if (xmlTextReaderImpl.fragmentType == XmlNodeType.None)
							{
								xmlTextReaderImpl.fragmentType = (xmlTextReaderImpl.rootElementParsed ? XmlNodeType.Document : XmlNodeType.Element);
							}
							xmlTextReaderImpl.OnEof();
							result = false;
							goto IL_1EF;
						}
						if (xmlTextReaderImpl.HandleEntityEnd(true))
						{
							xmlTextReaderImpl.SetupEndEntityNodeInContent();
							result = true;
							goto IL_1EF;
						}
						configuredTaskAwaiter4 = xmlTextReaderImpl.ParseDocumentContentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDocumentContentAsync_ReadData>d__512>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_18A;
					}
					IL_E8:
					result = configuredTaskAwaiter4.GetResult();
					goto IL_1EF;
					IL_18A:
					result = configuredTaskAwaiter4.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_1EF:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D31 RID: 3377 RVA: 0x00045504 File Offset: 0x00043704
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007BC RID: 1980
			public int <>1__state;

			// Token: 0x040007BD RID: 1981
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040007BE RID: 1982
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007BF RID: 1983
			public bool needMoreChars;

			// Token: 0x040007C0 RID: 1984
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040007C1 RID: 1985
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200014B RID: 331
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseElementContent_ReadData>d__514 : IAsyncStateMachine
		{
			// Token: 0x06000D32 RID: 3378 RVA: 0x00045514 File Offset: 0x00043714
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_13F;
						}
						configuredTaskAwaiter5 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseElementContent_ReadData>d__514>(ref configuredTaskAwaiter5, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter5.GetResult() == 0)
					{
						if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos != 0)
						{
							xmlTextReaderImpl.ThrowUnclosedElements();
						}
						if (!xmlTextReaderImpl.InEntity)
						{
							if (xmlTextReaderImpl.index == 0 && xmlTextReaderImpl.fragmentType != XmlNodeType.Document)
							{
								xmlTextReaderImpl.OnEof();
								result = false;
								goto IL_162;
							}
							xmlTextReaderImpl.ThrowUnclosedElements();
						}
						if (xmlTextReaderImpl.HandleEntityEnd(true))
						{
							xmlTextReaderImpl.SetupEndEntityNodeInContent();
							result = true;
							goto IL_162;
						}
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ParseElementContentAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseElementContent_ReadData>d__514>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_13F:
					result = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_162:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D33 RID: 3379 RVA: 0x000456B4 File Offset: 0x000438B4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007C2 RID: 1986
			public int <>1__state;

			// Token: 0x040007C3 RID: 1987
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040007C4 RID: 1988
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007C5 RID: 1989
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040007C6 RID: 1990
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200014C RID: 332
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ParseElementAsync_ContinueWithSetElement>d__517 : IAsyncStateMachine
		{
			// Token: 0x06000D34 RID: 3380 RVA: 0x000456C4 File Offset: 0x000438C4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_ED;
						}
						configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseElementAsync_ContinueWithSetElement>d__517>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Tuple<int, int> result = configuredTaskAwaiter3.GetResult();
					int item = result.Item1;
					int item2 = result.Item2;
					configuredTaskAwaiter = xmlTextReaderImpl.ParseElementAsync_SetElement(item, item2).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseElementAsync_ContinueWithSetElement>d__517>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_ED:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D35 RID: 3381 RVA: 0x00045804 File Offset: 0x00043A04
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007C7 RID: 1991
			public int <>1__state;

			// Token: 0x040007C8 RID: 1992
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007C9 RID: 1993
			public Task<Tuple<int, int>> task;

			// Token: 0x040007CA RID: 1994
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007CB RID: 1995
			private ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040007CC RID: 1996
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200014D RID: 333
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseElementAsync_ReadData>d__520 : IAsyncStateMachine
		{
			// Token: 0x06000D36 RID: 3382 RVA: 0x00045814 File Offset: 0x00043A14
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_F2;
						}
						configuredTaskAwaiter5 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseElementAsync_ReadData>d__520>(ref configuredTaskAwaiter5, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter5.GetResult() == 0)
					{
						xmlTextReaderImpl.Throw(pos, "Unexpected end of file while parsing {0} has occurred.", ">");
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ParseElementAsync_NoAttributes().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseElementAsync_ReadData>d__520>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_F2:
					configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D37 RID: 3383 RVA: 0x00045958 File Offset: 0x00043B58
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007CD RID: 1997
			public int <>1__state;

			// Token: 0x040007CE RID: 1998
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007CF RID: 1999
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007D0 RID: 2000
			public int pos;

			// Token: 0x040007D1 RID: 2001
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040007D2 RID: 2002
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200014E RID: 334
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ParseEndElmentAsync>d__522 : IAsyncStateMachine
		{
			// Token: 0x06000D38 RID: 3384 RVA: 0x00045968 File Offset: 0x00043B68
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_D6;
						}
						configuredTaskAwaiter = xmlTextReaderImpl.ParseEndElmentAsync_PrepareData().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseEndElmentAsync>d__522>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlTextReaderImpl.ParseEndElementAsync_CheckNameAndParse().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseEndElmentAsync>d__522>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_D6:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D39 RID: 3385 RVA: 0x00045A90 File Offset: 0x00043C90
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007D3 RID: 2003
			public int <>1__state;

			// Token: 0x040007D4 RID: 2004
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007D5 RID: 2005
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007D6 RID: 2006
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200014F RID: 335
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseEndElmentAsync_PrepareData>d__523 : IAsyncStateMachine
		{
			// Token: 0x06000D3A RID: 3386 RVA: 0x00045AA0 File Offset: 0x00043CA0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					if (num != 0)
					{
						XmlTextReaderImpl.NodeData nodeData = xmlTextReaderImpl.nodes[xmlTextReaderImpl.index - 1];
						prefLen = nodeData.prefix.Length;
						locLen = nodeData.localName.Length;
						goto IL_A9;
					}
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3 = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_A0:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						goto IL_D4;
					}
					IL_A9:
					if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos < prefLen + locLen + 1)
					{
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseEndElmentAsync_PrepareData>d__523>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_A0;
					}
					IL_D4:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D3B RID: 3387 RVA: 0x00045BC0 File Offset: 0x00043DC0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007D7 RID: 2007
			public int <>1__state;

			// Token: 0x040007D8 RID: 2008
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007D9 RID: 2009
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007DA RID: 2010
			private int <prefLen>5__1;

			// Token: 0x040007DB RID: 2011
			private int <locLen>5__2;

			// Token: 0x040007DC RID: 2012
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000150 RID: 336
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseEndElementAsync_Finish>d__528 : IAsyncStateMachine
		{
			// Token: 0x06000D3C RID: 3388 RVA: 0x00045BD0 File Offset: 0x00043DD0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_6E;
					}
					IL_11:
					configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseEndElementAsync_Finish>d__528>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_6E:
					configuredTaskAwaiter.GetResult();
					switch (xmlTextReaderImpl.parseEndElement_NextFunc)
					{
					case XmlTextReaderImpl.ParseEndElementParseFunction.CheckEndTag:
						task = xmlTextReaderImpl.ParseEndElementAsync_CheckEndTag(nameLen, startTagNode, endTagLineInfo);
						goto IL_11;
					case XmlTextReaderImpl.ParseEndElementParseFunction.ReadData:
						task = xmlTextReaderImpl.ParseEndElementAsync_ReadData();
						goto IL_11;
					case XmlTextReaderImpl.ParseEndElementParseFunction.Done:
						break;
					default:
						goto IL_11;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D3D RID: 3389 RVA: 0x00045CE4 File Offset: 0x00043EE4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007DD RID: 2013
			public int <>1__state;

			// Token: 0x040007DE RID: 2014
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007DF RID: 2015
			public Task task;

			// Token: 0x040007E0 RID: 2016
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007E1 RID: 2017
			public int nameLen;

			// Token: 0x040007E2 RID: 2018
			public XmlTextReaderImpl.NodeData startTagNode;

			// Token: 0x040007E3 RID: 2019
			public LineInfo endTagLineInfo;

			// Token: 0x040007E4 RID: 2020
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000151 RID: 337
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseEndElementAsync_ReadData>d__530 : IAsyncStateMachine
		{
			// Token: 0x06000D3E RID: 3390 RVA: 0x00045CF4 File Offset: 0x00043EF4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseEndElementAsync_ReadData>d__530>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						xmlTextReaderImpl.ThrowUnclosedElements();
					}
					xmlTextReaderImpl.parseEndElement_NextFunc = XmlTextReaderImpl.ParseEndElementParseFunction.CheckEndTag;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D3F RID: 3391 RVA: 0x00045DC0 File Offset: 0x00043FC0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007E5 RID: 2021
			public int <>1__state;

			// Token: 0x040007E6 RID: 2022
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007E7 RID: 2023
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007E8 RID: 2024
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000152 RID: 338
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ThrowTagMismatchAsync>d__531 : IAsyncStateMachine
		{
			// Token: 0x06000D40 RID: 3392 RVA: 0x00045DD0 File Offset: 0x00043FD0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (startTag.type != XmlNodeType.Element)
						{
							xmlTextReaderImpl.Throw("Unexpected end tag.");
							goto IL_130;
						}
						configuredTaskAwaiter = xmlTextReaderImpl.ParseQNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ThrowTagMismatchAsync>d__531>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Tuple<int, int> result = configuredTaskAwaiter.GetResult();
					int item = result.Item1;
					int item2 = result.Item2;
					xmlTextReaderImpl.Throw("The '{0}' start tag on line {1} position {2} does not match the end tag of '{3}'.", new string[]
					{
						startTag.GetNameWPrefix(xmlTextReaderImpl.nameTable),
						startTag.lineInfo.lineNo.ToString(CultureInfo.InvariantCulture),
						startTag.lineInfo.linePos.ToString(CultureInfo.InvariantCulture),
						new string(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, item2 - xmlTextReaderImpl.ps.charPos)
					});
					IL_130:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D41 RID: 3393 RVA: 0x00045F58 File Offset: 0x00044158
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007E9 RID: 2025
			public int <>1__state;

			// Token: 0x040007EA RID: 2026
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007EB RID: 2027
			public XmlTextReaderImpl.NodeData startTag;

			// Token: 0x040007EC RID: 2028
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007ED RID: 2029
			private ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000153 RID: 339
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseAttributesAsync>d__532 : IAsyncStateMachine
		{
			// Token: 0x06000D42 RID: 3394 RVA: 0x00045F68 File Offset: 0x00044168
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter6;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_413;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_4C1;
					}
					case 2:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_5A8;
					case 3:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_683;
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_81B;
					}
					case 5:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_94D;
					default:
						pos = xmlTextReaderImpl.ps.charPos;
						chars = xmlTextReaderImpl.ps.chars;
						attr = null;
						break;
					}
					IL_55:
					lineNoDelta = 0;
					int num3;
					while ((xmlTextReaderImpl.xmlCharType.charProperties[(int)(tmpch0 = chars[pos])] & 1) != 0)
					{
						if (tmpch0 == '\n')
						{
							xmlTextReaderImpl.OnNewLine(pos + 1);
							num3 = lineNoDelta;
							lineNoDelta = num3 + 1;
						}
						else if (tmpch0 == '\r')
						{
							if (chars[pos + 1] == '\n')
							{
								xmlTextReaderImpl.OnNewLine(pos + 2);
								num3 = lineNoDelta;
								lineNoDelta = num3 + 1;
								num3 = pos;
								pos = num3 + 1;
							}
							else if (pos + 1 != xmlTextReaderImpl.ps.charsUsed)
							{
								xmlTextReaderImpl.OnNewLine(pos + 1);
								num3 = lineNoDelta;
								lineNoDelta = num3 + 1;
							}
							else
							{
								xmlTextReaderImpl.ps.charPos = pos;
								IL_8D7:
								XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
								xmlTextReaderImpl2.ps.lineNo = xmlTextReaderImpl2.ps.lineNo - lineNoDelta;
								configuredTaskAwaiter5 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter5.IsCompleted)
								{
									num2 = 5;
									configuredTaskAwaiter2 = configuredTaskAwaiter5;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributesAsync>d__532>(ref configuredTaskAwaiter5, ref this);
									return;
								}
								goto IL_94D;
							}
						}
						num3 = pos;
						pos = num3 + 1;
					}
					int num4 = 0;
					char c;
					if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)(c = chars[pos])] & 4) != 0)
					{
						num4 = 1;
					}
					if (num4 == 0)
					{
						if (c == '>')
						{
							xmlTextReaderImpl.ps.charPos = pos + 1;
							xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.MoveToElementContent;
						}
						else if (c == '/')
						{
							if (pos + 1 == xmlTextReaderImpl.ps.charsUsed)
							{
								goto IL_8D7;
							}
							if (chars[pos + 1] != '>')
							{
								xmlTextReaderImpl.ThrowUnexpectedToken(pos + 1, ">");
								goto IL_287;
							}
							xmlTextReaderImpl.ps.charPos = pos + 2;
							xmlTextReaderImpl.curNode.IsEmptyElement = true;
							xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.parsingFunction;
							xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.PopEmptyElementContext;
						}
						else
						{
							if (pos == xmlTextReaderImpl.ps.charsUsed)
							{
								goto IL_8D7;
							}
							if (c != ':' || xmlTextReaderImpl.supportNamespaces)
							{
								xmlTextReaderImpl.Throw(pos, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(chars, xmlTextReaderImpl.ps.charsUsed, pos));
								goto IL_287;
							}
							goto IL_287;
						}
						if (xmlTextReaderImpl.addDefaultAttributesAndNormalize)
						{
							xmlTextReaderImpl.AddDefaultAttributesAndNormalize();
						}
						xmlTextReaderImpl.ElementNamespaceLookup();
						if (xmlTextReaderImpl.attrNeedNamespaceLookup)
						{
							xmlTextReaderImpl.AttributeNamespaceLookup();
							xmlTextReaderImpl.attrNeedNamespaceLookup = false;
						}
						if (xmlTextReaderImpl.attrDuplWalkCount >= 250)
						{
							xmlTextReaderImpl.AttributeDuplCheck();
						}
						goto IL_9DF;
					}
					IL_287:
					if (pos == xmlTextReaderImpl.ps.charPos)
					{
						xmlTextReaderImpl.ThrowExpectingWhitespace(pos);
					}
					xmlTextReaderImpl.ps.charPos = pos;
					attrNameLinePos = xmlTextReaderImpl.ps.LinePos;
					int num5 = -1;
					pos += num4;
					for (;;)
					{
						if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)(tmpch = chars[pos])] & 8) != 0)
						{
							num3 = pos;
							pos = num3 + 1;
						}
						else
						{
							if (tmpch != ':')
							{
								goto IL_448;
							}
							if (num5 != -1)
							{
								if (xmlTextReaderImpl.supportNamespaces)
								{
									break;
								}
								num3 = pos;
								pos = num3 + 1;
							}
							else
							{
								num5 = pos;
								num3 = pos;
								pos = num3 + 1;
								if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)chars[pos]] & 4) == 0)
								{
									goto IL_3B2;
								}
								num3 = pos;
								pos = num3 + 1;
							}
						}
					}
					xmlTextReaderImpl.Throw(pos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
					goto IL_4F1;
					IL_3B2:
					configuredTaskAwaiter3 = xmlTextReaderImpl.ParseQNameAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributesAsync>d__532>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_413;
					IL_448:
					if (pos + 1 < xmlTextReaderImpl.ps.charsUsed)
					{
						goto IL_4F1;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ParseQNameAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributesAsync>d__532>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_4C1;
					IL_413:
					Tuple<int, int> result = configuredTaskAwaiter3.GetResult();
					num5 = result.Item1;
					pos = result.Item2;
					chars = xmlTextReaderImpl.ps.chars;
					goto IL_4F1;
					IL_4C1:
					Tuple<int, int> result2 = configuredTaskAwaiter3.GetResult();
					num5 = result2.Item1;
					pos = result2.Item2;
					chars = xmlTextReaderImpl.ps.chars;
					IL_4F1:
					attr = xmlTextReaderImpl.AddAttribute(pos, num5);
					attr.SetLineInfo(xmlTextReaderImpl.ps.LineNo, attrNameLinePos);
					if (chars[pos] == '=')
					{
						goto IL_5DD;
					}
					xmlTextReaderImpl.ps.charPos = pos;
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributesAsync>d__532>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_5A8:
					configuredTaskAwaiter5.GetResult();
					pos = xmlTextReaderImpl.ps.charPos;
					if (chars[pos] != '=')
					{
						xmlTextReaderImpl.ThrowUnexpectedToken("=");
					}
					IL_5DD:
					num3 = pos;
					pos = num3 + 1;
					char c2 = chars[pos];
					if (c2 == '"' || c2 == '\'')
					{
						goto IL_6C7;
					}
					xmlTextReaderImpl.ps.charPos = pos;
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 3;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributesAsync>d__532>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_683:
					configuredTaskAwaiter5.GetResult();
					pos = xmlTextReaderImpl.ps.charPos;
					c2 = chars[pos];
					if (c2 != '"' && c2 != '\'')
					{
						xmlTextReaderImpl.ThrowUnexpectedToken("\"", "'");
					}
					IL_6C7:
					num3 = pos;
					pos = num3 + 1;
					xmlTextReaderImpl.ps.charPos = pos;
					attr.quoteChar = c2;
					attr.SetLineInfo2(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
					char c3;
					while ((xmlTextReaderImpl.xmlCharType.charProperties[(int)(c3 = chars[pos])] & 128) != 0)
					{
						num3 = pos;
						pos = num3 + 1;
					}
					if (c3 == c2)
					{
						attr.SetValue(chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos);
						num3 = pos;
						pos = num3 + 1;
						xmlTextReaderImpl.ps.charPos = pos;
						goto IL_844;
					}
					configuredTaskAwaiter6 = xmlTextReaderImpl.ParseAttributeValueSlowAsync(pos, c2, attr).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter6.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributesAsync>d__532>(ref configuredTaskAwaiter6, ref this);
						return;
					}
					IL_81B:
					configuredTaskAwaiter6.GetResult();
					pos = xmlTextReaderImpl.ps.charPos;
					chars = xmlTextReaderImpl.ps.chars;
					IL_844:
					if (attr.prefix.Length == 0)
					{
						if (Ref.Equal(attr.localName, xmlTextReaderImpl.XmlNs))
						{
							xmlTextReaderImpl.OnDefaultNamespaceDecl(attr);
							goto IL_55;
						}
						goto IL_55;
					}
					else
					{
						if (Ref.Equal(attr.prefix, xmlTextReaderImpl.XmlNs))
						{
							xmlTextReaderImpl.OnNamespaceDecl(attr);
							goto IL_55;
						}
						if (Ref.Equal(attr.prefix, xmlTextReaderImpl.Xml))
						{
							xmlTextReaderImpl.OnXmlReservedAttribute(attr);
							goto IL_55;
						}
						goto IL_55;
					}
					IL_94D:
					if (configuredTaskAwaiter5.GetResult() != 0)
					{
						pos = xmlTextReaderImpl.ps.charPos;
						chars = xmlTextReaderImpl.ps.chars;
						goto IL_55;
					}
					xmlTextReaderImpl.ThrowUnclosedElements();
					goto IL_55;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_9DF:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D43 RID: 3395 RVA: 0x00046984 File Offset: 0x00044B84
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007EE RID: 2030
			public int <>1__state;

			// Token: 0x040007EF RID: 2031
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007F0 RID: 2032
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007F1 RID: 2033
			private char <tmpch0>5__1;

			// Token: 0x040007F2 RID: 2034
			private int <pos>5__2;

			// Token: 0x040007F3 RID: 2035
			private char[] <chars>5__3;

			// Token: 0x040007F4 RID: 2036
			private char <tmpch2>5__4;

			// Token: 0x040007F5 RID: 2037
			private int <attrNameLinePos>5__5;

			// Token: 0x040007F6 RID: 2038
			private XmlTextReaderImpl.NodeData <attr>5__6;

			// Token: 0x040007F7 RID: 2039
			private int <lineNoDelta>5__7;

			// Token: 0x040007F8 RID: 2040
			private ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040007F9 RID: 2041
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x040007FA RID: 2042
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000154 RID: 340
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseAttributeValueSlowAsync>d__533 : IAsyncStateMachine
		{
			// Token: 0x06000D44 RID: 3396 RVA: 0x00046994 File Offset: 0x00044B94
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
					char[] chars;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3F3;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_540;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_665;
					}
					case 3:
						configuredTaskAwaiter7 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_883;
					default:
						pos = curPos;
						chars = xmlTextReaderImpl.ps.chars;
						attributeBaseEntityId = xmlTextReaderImpl.ps.entityId;
						valueChunkStartPos = 0;
						valueChunkLineInfo = new LineInfo(xmlTextReaderImpl.ps.lineNo, xmlTextReaderImpl.ps.LinePos);
						lastChunk = null;
						break;
					}
					for (;;)
					{
						IL_8E:
						if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)chars[pos]] & 128) == 0)
						{
							if (pos - xmlTextReaderImpl.ps.charPos > 0)
							{
								xmlTextReaderImpl.stringBuilder.Append(chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos);
								xmlTextReaderImpl.ps.charPos = pos;
							}
							if (chars[pos] == quoteChar && attributeBaseEntityId == xmlTextReaderImpl.ps.entityId)
							{
								goto IL_994;
							}
							char c = chars[pos];
							int num3;
							if (c <= '&')
							{
								switch (c)
								{
								case '\t':
									num3 = pos;
									pos = num3 + 1;
									if (xmlTextReaderImpl.normalize)
									{
										xmlTextReaderImpl.stringBuilder.Append(' ');
										XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
										xmlTextReaderImpl2.ps.charPos = xmlTextReaderImpl2.ps.charPos + 1;
										continue;
									}
									continue;
								case '\n':
									num3 = pos;
									pos = num3 + 1;
									xmlTextReaderImpl.OnNewLine(pos);
									if (xmlTextReaderImpl.normalize)
									{
										xmlTextReaderImpl.stringBuilder.Append(' ');
										XmlTextReaderImpl xmlTextReaderImpl3 = xmlTextReaderImpl;
										xmlTextReaderImpl3.ps.charPos = xmlTextReaderImpl3.ps.charPos + 1;
										continue;
									}
									continue;
								case '\v':
								case '\f':
									goto IL_79F;
								case '\r':
									if (chars[pos + 1] == '\n')
									{
										pos += 2;
										if (xmlTextReaderImpl.normalize)
										{
											xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.eolNormalized ? "  " : " ");
											xmlTextReaderImpl.ps.charPos = pos;
										}
									}
									else
									{
										if (pos + 1 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
										{
											goto IL_822;
										}
										num3 = pos;
										pos = num3 + 1;
										if (xmlTextReaderImpl.normalize)
										{
											xmlTextReaderImpl.stringBuilder.Append(' ');
											xmlTextReaderImpl.ps.charPos = pos;
										}
									}
									xmlTextReaderImpl.OnNewLine(pos);
									continue;
								default:
									if (c != '"')
									{
										if (c != '&')
										{
											goto IL_79F;
										}
										goto IL_30C;
									}
									break;
								}
							}
							else if (c != '\'')
							{
								if (c == '<')
								{
									break;
								}
								if (c != '>')
								{
									goto IL_79F;
								}
							}
							num3 = pos;
							pos = num3 + 1;
							continue;
							IL_79F:
							if (pos == xmlTextReaderImpl.ps.charsUsed)
							{
								goto IL_822;
							}
							if (!XmlCharType.IsHighSurrogate((int)chars[pos]))
							{
								goto IL_80A;
							}
							if (pos + 1 == xmlTextReaderImpl.ps.charsUsed)
							{
								goto IL_822;
							}
							num3 = pos;
							pos = num3 + 1;
							if (!XmlCharType.IsLowSurrogate((int)chars[pos]))
							{
								goto IL_80A;
							}
							num3 = pos;
							pos = num3 + 1;
						}
						else
						{
							int num3 = pos;
							pos = num3 + 1;
						}
					}
					xmlTextReaderImpl.Throw(pos, "'{0}', hexadecimal value {1}, is an invalid attribute character.", XmlException.BuildCharExceptionArgs('<', '\0'));
					goto IL_822;
					IL_30C:
					if (pos - xmlTextReaderImpl.ps.charPos > 0)
					{
						xmlTextReaderImpl.stringBuilder.Append(chars, xmlTextReaderImpl.ps.charPos, pos - xmlTextReaderImpl.ps.charPos);
					}
					xmlTextReaderImpl.ps.charPos = pos;
					enclosingEntityId = xmlTextReaderImpl.ps.entityId;
					entityLineInfo = new LineInfo(xmlTextReaderImpl.ps.lineNo, xmlTextReaderImpl.ps.LinePos + 1);
					configuredTaskAwaiter3 = xmlTextReaderImpl.HandleEntityReferenceAsync(true, XmlTextReaderImpl.EntityExpandType.All).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributeValueSlowAsync>d__533>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_3F3;
					IL_80A:
					xmlTextReaderImpl.ThrowInvalidChar(chars, xmlTextReaderImpl.ps.charsUsed, pos);
					IL_822:
					configuredTaskAwaiter7 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter7.IsCompleted)
					{
						num2 = 3;
						configuredTaskAwaiter2 = configuredTaskAwaiter7;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributeValueSlowAsync>d__533>(ref configuredTaskAwaiter7, ref this);
						return;
					}
					goto IL_883;
					IL_3F3:
					Tuple<int, XmlTextReaderImpl.EntityType> result = configuredTaskAwaiter3.GetResult();
					pos = result.Item1;
					switch (result.Item2)
					{
					case XmlTextReaderImpl.EntityType.CharacterDec:
					case XmlTextReaderImpl.EntityType.CharacterHex:
					case XmlTextReaderImpl.EntityType.CharacterNamed:
						goto IL_78E;
					case XmlTextReaderImpl.EntityType.Unexpanded:
						if (xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.Full && xmlTextReaderImpl.ps.entityId == attributeBaseEntityId)
						{
							int num4 = xmlTextReaderImpl.stringBuilder.Length - valueChunkStartPos;
							if (num4 > 0)
							{
								XmlTextReaderImpl.NodeData nodeData = new XmlTextReaderImpl.NodeData();
								nodeData.lineInfo = valueChunkLineInfo;
								nodeData.depth = attr.depth + 1;
								nodeData.SetValueNode(XmlNodeType.Text, xmlTextReaderImpl.stringBuilder.ToString(valueChunkStartPos, num4));
								xmlTextReaderImpl.AddAttributeChunkToList(attr, nodeData, ref lastChunk);
							}
							XmlTextReaderImpl xmlTextReaderImpl4 = xmlTextReaderImpl;
							xmlTextReaderImpl4.ps.charPos = xmlTextReaderImpl4.ps.charPos + 1;
							configuredTaskAwaiter5 = xmlTextReaderImpl.ParseEntityNameAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributeValueSlowAsync>d__533>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_540;
						}
						else
						{
							XmlTextReaderImpl xmlTextReaderImpl5 = xmlTextReaderImpl;
							xmlTextReaderImpl5.ps.charPos = xmlTextReaderImpl5.ps.charPos + 1;
							configuredTaskAwaiter5 = xmlTextReaderImpl.ParseEntityNameAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 2;
								ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseAttributeValueSlowAsync>d__533>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_665;
						}
						break;
					case XmlTextReaderImpl.EntityType.ExpandedInAttribute:
						if (xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.Full && enclosingEntityId == attributeBaseEntityId)
						{
							int num5 = xmlTextReaderImpl.stringBuilder.Length - valueChunkStartPos;
							if (num5 > 0)
							{
								XmlTextReaderImpl.NodeData nodeData2 = new XmlTextReaderImpl.NodeData();
								nodeData2.lineInfo = valueChunkLineInfo;
								nodeData2.depth = attr.depth + 1;
								nodeData2.SetValueNode(XmlNodeType.Text, xmlTextReaderImpl.stringBuilder.ToString(valueChunkStartPos, num5));
								xmlTextReaderImpl.AddAttributeChunkToList(attr, nodeData2, ref lastChunk);
							}
							XmlTextReaderImpl.NodeData nodeData3 = new XmlTextReaderImpl.NodeData();
							nodeData3.lineInfo = entityLineInfo;
							nodeData3.depth = attr.depth + 1;
							nodeData3.SetNamedNode(XmlNodeType.EntityReference, xmlTextReaderImpl.ps.entity.Name);
							xmlTextReaderImpl.AddAttributeChunkToList(attr, nodeData3, ref lastChunk);
							xmlTextReaderImpl.fullAttrCleanup = true;
						}
						pos = xmlTextReaderImpl.ps.charPos;
						goto IL_78E;
					}
					pos = xmlTextReaderImpl.ps.charPos;
					goto IL_78E;
					IL_540:
					string result2 = configuredTaskAwaiter5.GetResult();
					XmlTextReaderImpl.NodeData nodeData4 = new XmlTextReaderImpl.NodeData();
					nodeData4.lineInfo = entityLineInfo;
					nodeData4.depth = attr.depth + 1;
					nodeData4.SetNamedNode(XmlNodeType.EntityReference, result2);
					xmlTextReaderImpl.AddAttributeChunkToList(attr, nodeData4, ref lastChunk);
					xmlTextReaderImpl.stringBuilder.Append('&');
					xmlTextReaderImpl.stringBuilder.Append(result2);
					xmlTextReaderImpl.stringBuilder.Append(';');
					valueChunkStartPos = xmlTextReaderImpl.stringBuilder.Length;
					valueChunkLineInfo.Set(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
					xmlTextReaderImpl.fullAttrCleanup = true;
					goto IL_66D;
					IL_665:
					configuredTaskAwaiter5.GetResult();
					IL_66D:
					pos = xmlTextReaderImpl.ps.charPos;
					IL_78E:
					chars = xmlTextReaderImpl.ps.chars;
					goto IL_8E;
					IL_883:
					if (configuredTaskAwaiter7.GetResult() == 0)
					{
						if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos > 0)
						{
							if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != '\r')
							{
								xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
							}
						}
						else
						{
							if (!xmlTextReaderImpl.InEntity)
							{
								if (xmlTextReaderImpl.fragmentType == XmlNodeType.Attribute)
								{
									if (attributeBaseEntityId != xmlTextReaderImpl.ps.entityId)
									{
										xmlTextReaderImpl.Throw("Entity replacement text must nest properly within markup declarations.");
										goto IL_994;
									}
									goto IL_994;
								}
								else
								{
									xmlTextReaderImpl.Throw("There is an unclosed literal string.");
								}
							}
							if (xmlTextReaderImpl.HandleEntityEnd(true))
							{
								xmlTextReaderImpl.Throw("An internal error has occurred.");
							}
							if (attributeBaseEntityId == xmlTextReaderImpl.ps.entityId)
							{
								valueChunkStartPos = xmlTextReaderImpl.stringBuilder.Length;
								valueChunkLineInfo.Set(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
							}
						}
					}
					pos = xmlTextReaderImpl.ps.charPos;
					chars = xmlTextReaderImpl.ps.chars;
					goto IL_8E;
					IL_994:
					if (attr.nextAttrValueChunk != null)
					{
						int num6 = xmlTextReaderImpl.stringBuilder.Length - valueChunkStartPos;
						if (num6 > 0)
						{
							XmlTextReaderImpl.NodeData nodeData5 = new XmlTextReaderImpl.NodeData();
							nodeData5.lineInfo = valueChunkLineInfo;
							nodeData5.depth = attr.depth + 1;
							nodeData5.SetValueNode(XmlNodeType.Text, xmlTextReaderImpl.stringBuilder.ToString(valueChunkStartPos, num6));
							xmlTextReaderImpl.AddAttributeChunkToList(attr, nodeData5, ref lastChunk);
						}
					}
					xmlTextReaderImpl.ps.charPos = pos + 1;
					attr.SetValue(xmlTextReaderImpl.stringBuilder.ToString());
					xmlTextReaderImpl.stringBuilder.Length = 0;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D45 RID: 3397 RVA: 0x00047434 File Offset: 0x00045634
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040007FB RID: 2043
			public int <>1__state;

			// Token: 0x040007FC RID: 2044
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040007FD RID: 2045
			public int curPos;

			// Token: 0x040007FE RID: 2046
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040007FF RID: 2047
			public char quoteChar;

			// Token: 0x04000800 RID: 2048
			private int <attributeBaseEntityId>5__1;

			// Token: 0x04000801 RID: 2049
			private int <valueChunkStartPos>5__2;

			// Token: 0x04000802 RID: 2050
			private LineInfo <valueChunkLineInfo>5__3;

			// Token: 0x04000803 RID: 2051
			public XmlTextReaderImpl.NodeData attr;

			// Token: 0x04000804 RID: 2052
			private XmlTextReaderImpl.NodeData <lastChunk>5__4;

			// Token: 0x04000805 RID: 2053
			private LineInfo <entityLineInfo>5__5;

			// Token: 0x04000806 RID: 2054
			private int <enclosingEntityId>5__6;

			// Token: 0x04000807 RID: 2055
			private int <pos>5__7;

			// Token: 0x04000808 RID: 2056
			private ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000809 RID: 2057
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x0400080A RID: 2058
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000155 RID: 341
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ParseTextAsync>d__535 : IAsyncStateMachine
		{
			// Token: 0x06000D46 RID: 3398 RVA: 0x00047444 File Offset: 0x00045644
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result3;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					int num3;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_A8;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_16C;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_260;
					}
					case 3:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3C6;
					}
					case 4:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_4E4;
					}
					case 5:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_5BE;
					}
					default:
						num3 = 0;
						if (parseTask == null)
						{
							if (xmlTextReaderImpl.parsingMode != XmlTextReaderImpl.ParsingMode.Full)
							{
								break;
							}
							xmlTextReaderImpl.curNode.SetLineInfo(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
							parseTask = xmlTextReaderImpl.ParseTextAsync(num3);
						}
						configuredTaskAwaiter = parseTask.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseTextAsync>d__535>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_16C;
					}
					IL_45:
					configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync(num3).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseTextAsync>d__535>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_A8:
					Tuple<int, int, int, bool> result = configuredTaskAwaiter.GetResult();
					int item = result.Item1;
					int item2 = result.Item2;
					num3 = result.Item3;
					if (result.Item4)
					{
						goto IL_560;
					}
					goto IL_45;
					IL_16C:
					Tuple<int, int, int, bool> result2 = configuredTaskAwaiter.GetResult();
					item = result2.Item1;
					item2 = result2.Item2;
					num3 = result2.Item3;
					if (result2.Item4)
					{
						if (item2 - item == 0)
						{
							goto IL_560;
						}
						XmlNodeType textNodeType = xmlTextReaderImpl.GetTextNodeType(num3);
						if (textNodeType != XmlNodeType.None)
						{
							xmlTextReaderImpl.curNode.SetValueNode(textNodeType, xmlTextReaderImpl.ps.chars, item, item2 - item);
							result3 = true;
							goto IL_5E1;
						}
						goto IL_560;
					}
					else if (!xmlTextReaderImpl.v1Compat)
					{
						fullValue = false;
						if (num3 > 32)
						{
							xmlTextReaderImpl.curNode.SetValueNode(XmlNodeType.Text, xmlTextReaderImpl.ps.chars, item, item2 - item);
							xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.parsingFunction;
							xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.PartialTextValue;
							result3 = true;
							goto IL_5E1;
						}
						if (item2 - item > 0)
						{
							xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
							goto IL_363;
						}
						goto IL_363;
					}
					IL_1DA:
					if (item2 - item > 0)
					{
						xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
					}
					configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync(num3).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseTextAsync>d__535>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_260:
					Tuple<int, int, int, bool> result4 = configuredTaskAwaiter.GetResult();
					item = result4.Item1;
					item2 = result4.Item2;
					num3 = result4.Item3;
					if (!result4.Item4)
					{
						goto IL_1DA;
					}
					if (item2 - item > 0)
					{
						xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
					}
					XmlNodeType textNodeType2 = xmlTextReaderImpl.GetTextNodeType(num3);
					if (textNodeType2 == XmlNodeType.None)
					{
						xmlTextReaderImpl.stringBuilder.Length = 0;
						goto IL_560;
					}
					xmlTextReaderImpl.curNode.SetValueNode(textNodeType2, xmlTextReaderImpl.stringBuilder.ToString());
					xmlTextReaderImpl.stringBuilder.Length = 0;
					result3 = true;
					goto IL_5E1;
					IL_363:
					configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync(num3).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseTextAsync>d__535>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_3C6:
					Tuple<int, int, int, bool> result5 = configuredTaskAwaiter.GetResult();
					item = result5.Item1;
					item2 = result5.Item2;
					num3 = result5.Item3;
					fullValue = result5.Item4;
					if (item2 - item > 0)
					{
						xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
					}
					if (!fullValue && num3 <= 32 && xmlTextReaderImpl.stringBuilder.Length < 4096)
					{
						goto IL_363;
					}
					nodeType = ((xmlTextReaderImpl.stringBuilder.Length < 4096) ? xmlTextReaderImpl.GetTextNodeType(num3) : XmlNodeType.Text);
					if (nodeType != XmlNodeType.None)
					{
						xmlTextReaderImpl.curNode.SetValueNode(nodeType, xmlTextReaderImpl.stringBuilder.ToString());
						xmlTextReaderImpl.stringBuilder.Length = 0;
						if (!fullValue)
						{
							xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.parsingFunction;
							xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.PartialTextValue;
						}
						result3 = true;
						goto IL_5E1;
					}
					xmlTextReaderImpl.stringBuilder.Length = 0;
					if (fullValue)
					{
						goto IL_560;
					}
					IL_481:
					configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync(num3).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseTextAsync>d__535>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_4E4:
					Tuple<int, int, int, bool> result6 = configuredTaskAwaiter.GetResult();
					item = result6.Item1;
					item2 = result6.Item2;
					num3 = result6.Item3;
					if (!result6.Item4)
					{
						goto IL_481;
					}
					IL_560:
					configuredTaskAwaiter3 = xmlTextReaderImpl.ParseTextAsync_IgnoreNode().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<_ParseTextAsync>d__535>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_5BE:
					result3 = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_5E1:
				num2 = -2;
				this.<>t__builder.SetResult(result3);
			}

			// Token: 0x06000D47 RID: 3399 RVA: 0x00047A64 File Offset: 0x00045C64
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400080B RID: 2059
			public int <>1__state;

			// Token: 0x0400080C RID: 2060
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x0400080D RID: 2061
			public Task<Tuple<int, int, int, bool>> parseTask;

			// Token: 0x0400080E RID: 2062
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400080F RID: 2063
			private XmlNodeType <nodeType>5__1;

			// Token: 0x04000810 RID: 2064
			private bool <fullValue>5__2;

			// Token: 0x04000811 RID: 2065
			private ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000812 RID: 2066
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000156 RID: 342
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseTextAsync_AsyncFunc>d__543 : IAsyncStateMachine
		{
			// Token: 0x06000D48 RID: 3400 RVA: 0x00047A74 File Offset: 0x00045C74
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, int, int, bool> result;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_253;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2DC;
					}
					default:
						IL_20:
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseTextAsync_AsyncFunc>d__543>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					int outOrChars = xmlTextReaderImpl.lastParseTextState.outOrChars;
					char[] chars = xmlTextReaderImpl.lastParseTextState.chars;
					pos = xmlTextReaderImpl.lastParseTextState.pos;
					rcount = xmlTextReaderImpl.lastParseTextState.rcount;
					rpos = xmlTextReaderImpl.lastParseTextState.rpos;
					orChars = xmlTextReaderImpl.lastParseTextState.orChars;
					c = xmlTextReaderImpl.lastParseTextState.c;
					switch (xmlTextReaderImpl.parseText_NextFunction)
					{
					case XmlTextReaderImpl.ParseTextFunction.ParseText:
						task = xmlTextReaderImpl.ParseTextAsync(outOrChars, chars, pos, rcount, rpos, orChars, c);
						goto IL_20;
					case XmlTextReaderImpl.ParseTextFunction.Entity:
						task = xmlTextReaderImpl.ParseTextAsync_ParseEntity(outOrChars, chars, pos, rcount, rpos, orChars, c);
						goto IL_20;
					case XmlTextReaderImpl.ParseTextFunction.Surrogate:
						task = xmlTextReaderImpl.ParseTextAsync_Surrogate(outOrChars, chars, pos, rcount, rpos, orChars, c);
						goto IL_20;
					case XmlTextReaderImpl.ParseTextFunction.ReadData:
						task = xmlTextReaderImpl.ParseTextAsync_ReadData(outOrChars, chars, pos, rcount, rpos, orChars, c);
						goto IL_20;
					case XmlTextReaderImpl.ParseTextFunction.NoValue:
						configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync_NoValue(outOrChars, pos).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseTextAsync_AsyncFunc>d__543>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					case XmlTextReaderImpl.ParseTextFunction.PartialValue:
						configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync_PartialValue(pos, rcount, rpos, orChars, c).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseTextAsync_AsyncFunc>d__543>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_2DC;
					default:
						goto IL_20;
					}
					IL_253:
					result = configuredTaskAwaiter.GetResult();
					goto IL_2FF;
					IL_2DC:
					result = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2FF:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D49 RID: 3401 RVA: 0x00047DB0 File Offset: 0x00045FB0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000813 RID: 2067
			public int <>1__state;

			// Token: 0x04000814 RID: 2068
			public AsyncTaskMethodBuilder<Tuple<int, int, int, bool>> <>t__builder;

			// Token: 0x04000815 RID: 2069
			public Task<Tuple<int, int, int, bool>> task;

			// Token: 0x04000816 RID: 2070
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000817 RID: 2071
			private int <pos>5__1;

			// Token: 0x04000818 RID: 2072
			private int <rcount>5__2;

			// Token: 0x04000819 RID: 2073
			private int <rpos>5__3;

			// Token: 0x0400081A RID: 2074
			private int <orChars>5__4;

			// Token: 0x0400081B RID: 2075
			private char <c>5__5;

			// Token: 0x0400081C RID: 2076
			private ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000157 RID: 343
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseTextAsync_ParseEntity>d__545 : IAsyncStateMachine
		{
			// Token: 0x06000D4A RID: 3402 RVA: 0x00047DC0 File Offset: 0x00045FC0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, int, int, bool> result;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						int num4;
						XmlTextReaderImpl.EntityType entityType;
						int num3;
						if ((num3 = xmlTextReaderImpl.ParseCharRefInline(pos, out num4, out entityType)) > 0)
						{
							if (rcount > 0)
							{
								xmlTextReaderImpl.ShiftBuffer(rpos + rcount, rpos, pos - rpos - rcount);
							}
							rpos = pos - rcount;
							rcount += num3 - pos - num4;
							pos = num3;
							if (!xmlTextReaderImpl.xmlCharType.IsWhiteSpace(chars[num3 - num4]) || (xmlTextReaderImpl.v1Compat && entityType == XmlTextReaderImpl.EntityType.CharacterDec))
							{
								orChars |= 255;
								goto IL_2B3;
							}
							goto IL_2B3;
						}
						else
						{
							if (pos > xmlTextReaderImpl.ps.charPos)
							{
								xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
								xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.PartialValue;
								result = xmlTextReaderImpl.parseText_dummyTask.Result;
								goto IL_316;
							}
							configuredTaskAwaiter = xmlTextReaderImpl.HandleEntityReferenceAsync(false, XmlTextReaderImpl.EntityExpandType.All).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseTextAsync_ParseEntity>d__545>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Tuple<int, XmlTextReaderImpl.EntityType> result2 = configuredTaskAwaiter.GetResult();
					pos = result2.Item1;
					switch (result2.Item2)
					{
					case XmlTextReaderImpl.EntityType.CharacterDec:
						if (xmlTextReaderImpl.v1Compat)
						{
							orChars |= 255;
							goto IL_2A2;
						}
						break;
					case XmlTextReaderImpl.EntityType.CharacterHex:
					case XmlTextReaderImpl.EntityType.CharacterNamed:
						break;
					case XmlTextReaderImpl.EntityType.Expanded:
					case XmlTextReaderImpl.EntityType.Skipped:
					case XmlTextReaderImpl.EntityType.FakeExpanded:
						goto IL_291;
					case XmlTextReaderImpl.EntityType.Unexpanded:
						xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.parsingFunction;
						xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.EntityReference;
						xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
						xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.NoValue;
						result = xmlTextReaderImpl.parseText_dummyTask.Result;
						goto IL_316;
					default:
						goto IL_291;
					}
					if (!xmlTextReaderImpl.xmlCharType.IsWhiteSpace(xmlTextReaderImpl.ps.chars[pos - 1]))
					{
						orChars |= 255;
						goto IL_2A2;
					}
					goto IL_2A2;
					IL_291:
					pos = xmlTextReaderImpl.ps.charPos;
					IL_2A2:
					chars = xmlTextReaderImpl.ps.chars;
					IL_2B3:
					xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
					xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ParseText;
					result = xmlTextReaderImpl.parseText_dummyTask.Result;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_316:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D4B RID: 3403 RVA: 0x00048114 File Offset: 0x00046314
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400081D RID: 2077
			public int <>1__state;

			// Token: 0x0400081E RID: 2078
			public AsyncTaskMethodBuilder<Tuple<int, int, int, bool>> <>t__builder;

			// Token: 0x0400081F RID: 2079
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000820 RID: 2080
			public int pos;

			// Token: 0x04000821 RID: 2081
			public int rcount;

			// Token: 0x04000822 RID: 2082
			public int rpos;

			// Token: 0x04000823 RID: 2083
			public char[] chars;

			// Token: 0x04000824 RID: 2084
			public int orChars;

			// Token: 0x04000825 RID: 2085
			public int outOrChars;

			// Token: 0x04000826 RID: 2086
			public char c;

			// Token: 0x04000827 RID: 2087
			private ConfiguredTaskAwaitable<Tuple<int, XmlTextReaderImpl.EntityType>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000158 RID: 344
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseTextAsync_Surrogate>d__546 : IAsyncStateMachine
		{
			// Token: 0x06000D4C RID: 3404 RVA: 0x00048124 File Offset: 0x00046324
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, int, int, bool> result;
				try
				{
					char c;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						c = chars[pos];
						if (XmlCharType.IsHighSurrogate((int)c))
						{
							if (pos + 1 == xmlTextReaderImpl.ps.charsUsed)
							{
								xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
								xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ReadData;
								result = xmlTextReaderImpl.parseText_dummyTask.Result;
								goto IL_26A;
							}
							int num3 = pos;
							pos = num3 + 1;
							if (XmlCharType.IsLowSurrogate((int)chars[pos]))
							{
								num3 = pos;
								pos = num3 + 1;
								orChars |= (int)c;
								xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
								xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ParseText;
								result = xmlTextReaderImpl.parseText_dummyTask.Result;
								goto IL_26A;
							}
						}
						offset = pos - xmlTextReaderImpl.ps.charPos;
						configuredTaskAwaiter3 = xmlTextReaderImpl.ZeroEndingStreamAsync(pos).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseTextAsync_Surrogate>d__546>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						xmlTextReaderImpl.ThrowInvalidChar(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charsUsed, xmlTextReaderImpl.ps.charPos + offset);
						throw new Exception();
					}
					chars = xmlTextReaderImpl.ps.chars;
					pos = xmlTextReaderImpl.ps.charPos + offset;
					xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
					xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.PartialValue;
					result = xmlTextReaderImpl.parseText_dummyTask.Result;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_26A:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D4D RID: 3405 RVA: 0x000483CC File Offset: 0x000465CC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000828 RID: 2088
			public int <>1__state;

			// Token: 0x04000829 RID: 2089
			public AsyncTaskMethodBuilder<Tuple<int, int, int, bool>> <>t__builder;

			// Token: 0x0400082A RID: 2090
			public char[] chars;

			// Token: 0x0400082B RID: 2091
			public int pos;

			// Token: 0x0400082C RID: 2092
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400082D RID: 2093
			public int outOrChars;

			// Token: 0x0400082E RID: 2094
			public int rcount;

			// Token: 0x0400082F RID: 2095
			public int rpos;

			// Token: 0x04000830 RID: 2096
			public int orChars;

			// Token: 0x04000831 RID: 2097
			public char c;

			// Token: 0x04000832 RID: 2098
			private int <offset>5__1;

			// Token: 0x04000833 RID: 2099
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000159 RID: 345
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseTextAsync_ReadData>d__547 : IAsyncStateMachine
		{
			// Token: 0x06000D4E RID: 3406 RVA: 0x000483DC File Offset: 0x000465DC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, int, int, bool> result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (pos > xmlTextReaderImpl.ps.charPos)
						{
							xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
							xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.PartialValue;
							result = xmlTextReaderImpl.parseText_dummyTask.Result;
							goto IL_288;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseTextAsync_ReadData>d__547>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos > 0)
						{
							if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != '\r' && xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != ']')
							{
								xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
							}
						}
						else
						{
							if (!xmlTextReaderImpl.InEntity)
							{
								xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
								xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.NoValue;
								result = xmlTextReaderImpl.parseText_dummyTask.Result;
								goto IL_288;
							}
							if (xmlTextReaderImpl.HandleEntityEnd(true))
							{
								xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.parsingFunction;
								xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.ReportEndEntity;
								xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
								xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.NoValue;
								result = xmlTextReaderImpl.parseText_dummyTask.Result;
								goto IL_288;
							}
						}
					}
					pos = xmlTextReaderImpl.ps.charPos;
					chars = xmlTextReaderImpl.ps.chars;
					xmlTextReaderImpl.lastParseTextState = new XmlTextReaderImpl.ParseTextState(outOrChars, chars, pos, rcount, rpos, orChars, c);
					xmlTextReaderImpl.parseText_NextFunction = XmlTextReaderImpl.ParseTextFunction.ParseText;
					result = xmlTextReaderImpl.parseText_dummyTask.Result;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_288:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D4F RID: 3407 RVA: 0x000486A4 File Offset: 0x000468A4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000834 RID: 2100
			public int <>1__state;

			// Token: 0x04000835 RID: 2101
			public AsyncTaskMethodBuilder<Tuple<int, int, int, bool>> <>t__builder;

			// Token: 0x04000836 RID: 2102
			public int pos;

			// Token: 0x04000837 RID: 2103
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000838 RID: 2104
			public int outOrChars;

			// Token: 0x04000839 RID: 2105
			public char[] chars;

			// Token: 0x0400083A RID: 2106
			public int rcount;

			// Token: 0x0400083B RID: 2107
			public int rpos;

			// Token: 0x0400083C RID: 2108
			public int orChars;

			// Token: 0x0400083D RID: 2109
			public char c;

			// Token: 0x0400083E RID: 2110
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200015A RID: 346
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishPartialValueAsync>d__550 : IAsyncStateMachine
		{
			// Token: 0x06000D50 RID: 3408 RVA: 0x000486B4 File Offset: 0x000468B4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					int outOrChars;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_13A;
						}
						xmlTextReaderImpl.curNode.CopyTo(xmlTextReaderImpl.readValueOffset, xmlTextReaderImpl.stringBuilder);
						outOrChars = 0;
						configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync(outOrChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishPartialValueAsync>d__550>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Tuple<int, int, int, bool> result = configuredTaskAwaiter.GetResult();
					int item = result.Item1;
					int item2 = result.Item2;
					outOrChars = result.Item3;
					goto IL_15C;
					IL_13A:
					result = configuredTaskAwaiter.GetResult();
					item = result.Item1;
					item2 = result.Item2;
					outOrChars = result.Item3;
					IL_15C:
					if (result.Item4)
					{
						xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
						xmlTextReaderImpl.curNode.SetValue(xmlTextReaderImpl.stringBuilder.ToString());
						xmlTextReaderImpl.stringBuilder.Length = 0;
					}
					else
					{
						xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
						configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync(outOrChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishPartialValueAsync>d__550>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_13A;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D51 RID: 3409 RVA: 0x000488B0 File Offset: 0x00046AB0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400083F RID: 2111
			public int <>1__state;

			// Token: 0x04000840 RID: 2112
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000841 RID: 2113
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000842 RID: 2114
			private ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200015B RID: 347
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishOtherValueIteratorAsync>d__551 : IAsyncStateMachine
		{
			// Token: 0x06000D52 RID: 3410 RVA: 0x000488C0 File Offset: 0x00046AC0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							switch (xmlTextReaderImpl.parsingFunction)
							{
							case XmlTextReaderImpl.ParsingFunction.InReadAttributeValue:
								goto IL_1C7;
							case XmlTextReaderImpl.ParsingFunction.InReadValueChunk:
								if (xmlTextReaderImpl.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnPartialValue)
								{
									configuredTaskAwaiter = xmlTextReaderImpl.FinishPartialValueAsync().ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num2 = 0;
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishOtherValueIteratorAsync>d__551>(ref configuredTaskAwaiter, ref this);
										return;
									}
									goto IL_A5;
								}
								else
								{
									if (xmlTextReaderImpl.readValueOffset > 0)
									{
										xmlTextReaderImpl.curNode.SetValue(xmlTextReaderImpl.curNode.StringValue.Substring(xmlTextReaderImpl.readValueOffset));
										xmlTextReaderImpl.readValueOffset = 0;
										goto IL_1C7;
									}
									goto IL_1C7;
								}
								break;
							case XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary:
							case XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary:
								switch (xmlTextReaderImpl.incReadState)
								{
								case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue:
									if (xmlTextReaderImpl.readValueOffset > 0)
									{
										xmlTextReaderImpl.curNode.SetValue(xmlTextReaderImpl.curNode.StringValue.Substring(xmlTextReaderImpl.readValueOffset));
										xmlTextReaderImpl.readValueOffset = 0;
										goto IL_1C7;
									}
									goto IL_1C7;
								case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue:
									configuredTaskAwaiter = xmlTextReaderImpl.FinishPartialValueAsync().ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num2 = 1;
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishOtherValueIteratorAsync>d__551>(ref configuredTaskAwaiter, ref this);
										return;
									}
									break;
								case XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End:
									xmlTextReaderImpl.curNode.SetValue(string.Empty);
									goto IL_1C7;
								default:
									goto IL_1C7;
								}
								break;
							default:
								goto IL_1C7;
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						xmlTextReaderImpl.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue;
						goto IL_1C7;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_A5:
					configuredTaskAwaiter.GetResult();
					xmlTextReaderImpl.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadValueChunk_OnCachedValue;
					IL_1C7:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D53 RID: 3411 RVA: 0x00048AE0 File Offset: 0x00046CE0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000843 RID: 2115
			public int <>1__state;

			// Token: 0x04000844 RID: 2116
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000845 RID: 2117
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000846 RID: 2118
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200015C RID: 348
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SkipPartialTextValueAsync>d__552 : IAsyncStateMachine
		{
			// Token: 0x06000D54 RID: 3412 RVA: 0x00048AF0 File Offset: 0x00046CF0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_7E;
					}
					int outOrChars = 0;
					xmlTextReaderImpl.parsingFunction = xmlTextReaderImpl.nextParsingFunction;
					IL_1F:
					configuredTaskAwaiter = xmlTextReaderImpl.ParseTextAsync(outOrChars).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipPartialTextValueAsync>d__552>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_7E:
					Tuple<int, int, int, bool> result = configuredTaskAwaiter.GetResult();
					int item = result.Item1;
					int item2 = result.Item2;
					outOrChars = result.Item3;
					if (!result.Item4)
					{
						goto IL_1F;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D55 RID: 3413 RVA: 0x00048BE4 File Offset: 0x00046DE4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000847 RID: 2119
			public int <>1__state;

			// Token: 0x04000848 RID: 2120
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000849 RID: 2121
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400084A RID: 2122
			private ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200015D RID: 349
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishReadContentAsBinaryAsync>d__554 : IAsyncStateMachine
		{
			// Token: 0x06000D56 RID: 3414 RVA: 0x00048BF4 File Offset: 0x00046DF4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					if (num != 0)
					{
						if (num == 1)
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_110;
						}
						xmlTextReaderImpl.readValueOffset = 0;
						if (xmlTextReaderImpl.incReadState != XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue)
						{
							xmlTextReaderImpl.parsingFunction = xmlTextReaderImpl.nextParsingFunction;
							xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.nextNextParsingFunction;
							goto IL_A7;
						}
						configuredTaskAwaiter4 = xmlTextReaderImpl.SkipPartialTextValueAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishReadContentAsBinaryAsync>d__554>(ref configuredTaskAwaiter4, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter4.GetResult();
					IL_A7:
					if (xmlTextReaderImpl.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End)
					{
						goto IL_119;
					}
					IL_B1:
					configuredTaskAwaiter3 = xmlTextReaderImpl.MoveToNextContentNodeAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishReadContentAsBinaryAsync>d__554>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_110:
					if (configuredTaskAwaiter3.GetResult())
					{
						goto IL_B1;
					}
					IL_119:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D57 RID: 3415 RVA: 0x00048D64 File Offset: 0x00046F64
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400084B RID: 2123
			public int <>1__state;

			// Token: 0x0400084C RID: 2124
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400084D RID: 2125
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400084E RID: 2126
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400084F RID: 2127
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200015E RID: 350
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishReadElementContentAsBinaryAsync>d__555 : IAsyncStateMachine
		{
			// Token: 0x06000D58 RID: 3416 RVA: 0x00048D74 File Offset: 0x00046F74
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_10F;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.FinishReadContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishReadElementContentAsBinaryAsync>d__555>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter3.GetResult();
					if (xmlTextReaderImpl.curNode.type != XmlNodeType.EndElement)
					{
						xmlTextReaderImpl.Throw("'{0}' is an invalid XmlNodeType.", xmlTextReaderImpl.curNode.type.ToString());
					}
					configuredTaskAwaiter = xmlTextReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<FinishReadElementContentAsBinaryAsync>d__555>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_10F:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D59 RID: 3417 RVA: 0x00048EE4 File Offset: 0x000470E4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000850 RID: 2128
			public int <>1__state;

			// Token: 0x04000851 RID: 2129
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000852 RID: 2130
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000853 RID: 2131
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000854 RID: 2132
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200015F RID: 351
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseRootLevelWhitespaceAsync>d__556 : IAsyncStateMachine
		{
			// Token: 0x06000D5A RID: 3418 RVA: 0x00048EF4 File Offset: 0x000470F4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_14C;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1E8;
					}
					case 3:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_29A;
					}
					default:
						nodeType = xmlTextReaderImpl.GetWhitespaceType();
						if (nodeType == XmlNodeType.None)
						{
							configuredTaskAwaiter = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseRootLevelWhitespaceAsync>d__556>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							xmlTextReaderImpl.curNode.SetLineInfo(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
							configuredTaskAwaiter = xmlTextReaderImpl.EatWhitespacesAsync(xmlTextReaderImpl.stringBuilder).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 2;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseRootLevelWhitespaceAsync>d__556>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_1E8;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					bool flag = xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] == '<' || xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos == 0;
					if (flag)
					{
						goto IL_155;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ZeroEndingStreamAsync(xmlTextReaderImpl.ps.charPos).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseRootLevelWhitespaceAsync>d__556>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_14C:
					flag = configuredTaskAwaiter3.GetResult();
					IL_155:
					if (flag)
					{
						result = false;
						goto IL_35A;
					}
					goto IL_2E5;
					IL_1E8:
					configuredTaskAwaiter.GetResult();
					flag = (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] == '<' || xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos == 0);
					if (flag)
					{
						goto IL_2A3;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ZeroEndingStreamAsync(xmlTextReaderImpl.ps.charPos).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseRootLevelWhitespaceAsync>d__556>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_29A:
					flag = configuredTaskAwaiter3.GetResult();
					IL_2A3:
					if (flag)
					{
						if (xmlTextReaderImpl.stringBuilder.Length > 0)
						{
							xmlTextReaderImpl.curNode.SetValueNode(nodeType, xmlTextReaderImpl.stringBuilder.ToString());
							xmlTextReaderImpl.stringBuilder.Length = 0;
							result = true;
							goto IL_35A;
						}
						result = false;
						goto IL_35A;
					}
					IL_2E5:
					if (xmlTextReaderImpl.xmlCharType.IsCharData(xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos]))
					{
						xmlTextReaderImpl.Throw("Data at the root level is invalid.");
					}
					else
					{
						xmlTextReaderImpl.ThrowInvalidChar(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charsUsed, xmlTextReaderImpl.ps.charPos);
					}
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_35A:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D5B RID: 3419 RVA: 0x0004928C File Offset: 0x0004748C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000855 RID: 2133
			public int <>1__state;

			// Token: 0x04000856 RID: 2134
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000857 RID: 2135
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000858 RID: 2136
			private XmlNodeType <nodeType>5__1;

			// Token: 0x04000859 RID: 2137
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400085A RID: 2138
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000160 RID: 352
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseEntityReferenceAsync>d__557 : IAsyncStateMachine
		{
			// Token: 0x06000D5C RID: 3420 RVA: 0x0004929C File Offset: 0x0004749C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
						xmlTextReaderImpl2.ps.charPos = xmlTextReaderImpl2.ps.charPos + 1;
						xmlTextReaderImpl.curNode.SetLineInfo(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
						nodeData = xmlTextReaderImpl.curNode;
						configuredTaskAwaiter = xmlTextReaderImpl.ParseEntityNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseEntityReferenceAsync>d__557>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					string result = configuredTaskAwaiter.GetResult();
					nodeData.SetNamedNode(XmlNodeType.EntityReference, result);
					nodeData = null;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D5D RID: 3421 RVA: 0x000493AC File Offset: 0x000475AC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400085B RID: 2139
			public int <>1__state;

			// Token: 0x0400085C RID: 2140
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400085D RID: 2141
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400085E RID: 2142
			private XmlTextReaderImpl.NodeData <>7__wrap1;

			// Token: 0x0400085F RID: 2143
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000161 RID: 353
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <HandleEntityReferenceAsync>d__558 : IAsyncStateMachine
		{
			// Token: 0x06000D5E RID: 3422 RVA: 0x000493BC File Offset: 0x000475BC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, XmlTextReaderImpl.EntityType> result2;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					int result;
					ConfiguredTaskAwaitable<XmlTextReaderImpl.EntityType>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_142;
					}
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						goto IL_1DF;
					case 3:
					{
						IL_260:
						try
						{
							if (num != 3)
							{
								configuredTaskAwaiter3 = xmlTextReaderImpl.ParseNameAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 3;
									configuredTaskAwaiter2 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<HandleEntityReferenceAsync>d__558>(ref configuredTaskAwaiter3, ref this);
									return;
								}
							}
							else
							{
								configuredTaskAwaiter3 = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
								num2 = -1;
							}
							result = configuredTaskAwaiter3.GetResult();
							endPos = result;
						}
						catch (XmlException)
						{
							xmlTextReaderImpl.Throw("An error occurred while parsing EntityName.", xmlTextReaderImpl.ps.LineNo, savedLinePos);
							result2 = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, XmlTextReaderImpl.EntityType.Skipped);
							goto IL_448;
						}
						if (xmlTextReaderImpl.ps.chars[endPos] != ';')
						{
							xmlTextReaderImpl.ThrowUnexpectedToken(endPos, ";");
						}
						int linePos = xmlTextReaderImpl.ps.LinePos;
						string name = xmlTextReaderImpl.nameTable.Add(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, endPos - xmlTextReaderImpl.ps.charPos);
						xmlTextReaderImpl.ps.charPos = endPos + 1;
						charRefEndPos = -1;
						configuredTaskAwaiter6 = xmlTextReaderImpl.HandleGeneralEntityReferenceAsync(name, isInAttributeValue, false, linePos).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter6.IsCompleted)
						{
							num2 = 4;
							ConfiguredTaskAwaitable<XmlTextReaderImpl.EntityType>.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<XmlTextReaderImpl.EntityType>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<HandleEntityReferenceAsync>d__558>(ref configuredTaskAwaiter6, ref this);
							return;
						}
						goto IL_3F4;
					}
					case 4:
					{
						ConfiguredTaskAwaitable<XmlTextReaderImpl.EntityType>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable<XmlTextReaderImpl.EntityType>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3F4;
					}
					default:
						if (xmlTextReaderImpl.ps.charPos + 1 != xmlTextReaderImpl.ps.charsUsed)
						{
							goto IL_B4;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<HandleEntityReferenceAsync>d__558>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
					}
					IL_B4:
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos + 1] == '#')
					{
						configuredTaskAwaiter4 = xmlTextReaderImpl.ParseNumericCharRefAsync(expandType != XmlTextReaderImpl.EntityExpandType.OnlyGeneral, null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<HandleEntityReferenceAsync>d__558>(ref configuredTaskAwaiter4, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = xmlTextReaderImpl.ParseNamedCharRefAsync(expandType != XmlTextReaderImpl.EntityExpandType.OnlyGeneral, null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 2;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<HandleEntityReferenceAsync>d__558>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_1DF;
					}
					IL_142:
					Tuple<XmlTextReaderImpl.EntityType, int> result3 = configuredTaskAwaiter4.GetResult();
					XmlTextReaderImpl.EntityType item = result3.Item1;
					charRefEndPos = result3.Item2;
					result2 = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, item);
					goto IL_448;
					IL_1DF:
					result = configuredTaskAwaiter3.GetResult();
					charRefEndPos = result;
					if (charRefEndPos >= 0)
					{
						result2 = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, XmlTextReaderImpl.EntityType.CharacterNamed);
						goto IL_448;
					}
					if (expandType == XmlTextReaderImpl.EntityExpandType.OnlyCharacter || (xmlTextReaderImpl.entityHandling != EntityHandling.ExpandEntities && (!isInAttributeValue || !xmlTextReaderImpl.validatingReaderCompatFlag)))
					{
						result2 = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, XmlTextReaderImpl.EntityType.Unexpanded);
						goto IL_448;
					}
					XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
					xmlTextReaderImpl2.ps.charPos = xmlTextReaderImpl2.ps.charPos + 1;
					savedLinePos = xmlTextReaderImpl.ps.LinePos;
					goto IL_260;
					IL_3F4:
					XmlTextReaderImpl.EntityType result4 = configuredTaskAwaiter6.GetResult();
					xmlTextReaderImpl.reportedBaseUri = xmlTextReaderImpl.ps.baseUriStr;
					xmlTextReaderImpl.reportedEncoding = xmlTextReaderImpl.ps.encoding;
					result2 = new Tuple<int, XmlTextReaderImpl.EntityType>(charRefEndPos, result4);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_448:
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000D5F RID: 3423 RVA: 0x0004985C File Offset: 0x00047A5C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000860 RID: 2144
			public int <>1__state;

			// Token: 0x04000861 RID: 2145
			public AsyncTaskMethodBuilder<Tuple<int, XmlTextReaderImpl.EntityType>> <>t__builder;

			// Token: 0x04000862 RID: 2146
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000863 RID: 2147
			public XmlTextReaderImpl.EntityExpandType expandType;

			// Token: 0x04000864 RID: 2148
			public bool isInAttributeValue;

			// Token: 0x04000865 RID: 2149
			private int <savedLinePos>5__1;

			// Token: 0x04000866 RID: 2150
			private int <charRefEndPos>5__2;

			// Token: 0x04000867 RID: 2151
			private int <endPos>5__3;

			// Token: 0x04000868 RID: 2152
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000869 RID: 2153
			private ConfiguredTaskAwaitable<Tuple<XmlTextReaderImpl.EntityType, int>>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x0400086A RID: 2154
			private ConfiguredTaskAwaitable<XmlTextReaderImpl.EntityType>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000162 RID: 354
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <HandleGeneralEntityReferenceAsync>d__559 : IAsyncStateMachine
		{
			// Token: 0x06000D60 RID: 3424 RVA: 0x0004986C File Offset: 0x00047A6C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				XmlTextReaderImpl.EntityType result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_298;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_32B;
					}
					default:
						entity = null;
						if (xmlTextReaderImpl.dtdInfo != null || xmlTextReaderImpl.fragmentParserContext == null || !xmlTextReaderImpl.fragmentParserContext.HasDtdInfo || xmlTextReaderImpl.dtdProcessing != DtdProcessing.Parse)
						{
							goto IL_B5;
						}
						configuredTaskAwaiter = xmlTextReaderImpl.ParseDtdFromParserContextAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<HandleGeneralEntityReferenceAsync>d__559>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					IL_B5:
					if (xmlTextReaderImpl.dtdInfo == null || (entity = xmlTextReaderImpl.dtdInfo.LookupEntity(name)) == null)
					{
						if (xmlTextReaderImpl.disableUndeclaredEntityCheck)
						{
							entity = new SchemaEntity(new XmlQualifiedName(name), false)
							{
								Text = string.Empty
							};
						}
						else
						{
							xmlTextReaderImpl.Throw("Reference to undeclared entity '{0}'.", name, xmlTextReaderImpl.ps.LineNo, entityStartLinePos);
						}
					}
					if (entity.IsUnparsedEntity)
					{
						if (xmlTextReaderImpl.disableUndeclaredEntityCheck)
						{
							entity = new SchemaEntity(new XmlQualifiedName(name), false)
							{
								Text = string.Empty
							};
						}
						else
						{
							xmlTextReaderImpl.Throw("Reference to unparsed entity '{0}'.", name, xmlTextReaderImpl.ps.LineNo, entityStartLinePos);
						}
					}
					if (xmlTextReaderImpl.standalone && entity.IsDeclaredInExternal)
					{
						xmlTextReaderImpl.Throw("Standalone document declaration must have a value of 'no' because an external entity '{0}' is referenced.", entity.Name, xmlTextReaderImpl.ps.LineNo, entityStartLinePos);
					}
					if (entity.IsExternal)
					{
						if (isInAttributeValue)
						{
							xmlTextReaderImpl.Throw("External entity '{0}' reference cannot appear in the attribute value.", name, xmlTextReaderImpl.ps.LineNo, entityStartLinePos);
							result = XmlTextReaderImpl.EntityType.Skipped;
							goto IL_3BF;
						}
						if (xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.SkipContent)
						{
							result = XmlTextReaderImpl.EntityType.Skipped;
							goto IL_3BF;
						}
						if (xmlTextReaderImpl.IsResolverNull)
						{
							if (!pushFakeEntityIfNullResolver)
							{
								result = XmlTextReaderImpl.EntityType.Skipped;
								goto IL_3BF;
							}
							configuredTaskAwaiter3 = xmlTextReaderImpl.PushExternalEntityAsync(entity).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<HandleGeneralEntityReferenceAsync>d__559>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter3 = xmlTextReaderImpl.PushExternalEntityAsync(entity).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 2;
								ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<HandleGeneralEntityReferenceAsync>d__559>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_32B;
						}
					}
					else
					{
						if (xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.SkipContent)
						{
							result = XmlTextReaderImpl.EntityType.Skipped;
							goto IL_3BF;
						}
						xmlTextReaderImpl.PushInternalEntity(entity);
						xmlTextReaderImpl.curNode.entityId = xmlTextReaderImpl.ps.entityId;
						result = ((isInAttributeValue && xmlTextReaderImpl.validatingReaderCompatFlag) ? XmlTextReaderImpl.EntityType.ExpandedInAttribute : XmlTextReaderImpl.EntityType.Expanded);
						goto IL_3BF;
					}
					IL_298:
					configuredTaskAwaiter3.GetResult();
					xmlTextReaderImpl.curNode.entityId = xmlTextReaderImpl.ps.entityId;
					result = XmlTextReaderImpl.EntityType.FakeExpanded;
					goto IL_3BF;
					IL_32B:
					configuredTaskAwaiter3.GetResult();
					xmlTextReaderImpl.curNode.entityId = xmlTextReaderImpl.ps.entityId;
					result = ((isInAttributeValue && xmlTextReaderImpl.validatingReaderCompatFlag) ? XmlTextReaderImpl.EntityType.ExpandedInAttribute : XmlTextReaderImpl.EntityType.Expanded);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_3BF:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D61 RID: 3425 RVA: 0x00049C68 File Offset: 0x00047E68
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400086B RID: 2155
			public int <>1__state;

			// Token: 0x0400086C RID: 2156
			public AsyncTaskMethodBuilder<XmlTextReaderImpl.EntityType> <>t__builder;

			// Token: 0x0400086D RID: 2157
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400086E RID: 2158
			public string name;

			// Token: 0x0400086F RID: 2159
			public int entityStartLinePos;

			// Token: 0x04000870 RID: 2160
			private IDtdEntityInfo <entity>5__1;

			// Token: 0x04000871 RID: 2161
			public bool isInAttributeValue;

			// Token: 0x04000872 RID: 2162
			public bool pushFakeEntityIfNullResolver;

			// Token: 0x04000873 RID: 2163
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000874 RID: 2164
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000163 RID: 355
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParsePIAsync>d__561 : IAsyncStateMachine
		{
			// Token: 0x06000D62 RID: 3426 RVA: 0x00049C78 File Offset: 0x00047E78
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result3;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1E5;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_26C;
					case 3:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_32D;
					}
					case 4:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_431;
					}
					case 5:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_4F8;
					}
					default:
						if (xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
						{
							xmlTextReaderImpl.curNode.SetLineInfo(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.ParseNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParsePIAsync>d__561>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					int result = configuredTaskAwaiter3.GetResult();
					string text = xmlTextReaderImpl.nameTable.Add(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, result - xmlTextReaderImpl.ps.charPos);
					if (string.Compare(text, "xml", StringComparison.OrdinalIgnoreCase) == 0)
					{
						xmlTextReaderImpl.Throw(text.Equals("xml") ? "Unexpected XML declaration. The XML declaration must be the first node in the document, and no white space characters are allowed to appear before it." : "'{0}' is an invalid name for processing instructions.", text);
					}
					xmlTextReaderImpl.ps.charPos = result;
					if (piInDtdStringBuilder == null)
					{
						if (!xmlTextReaderImpl.ignorePIs && xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
						{
							xmlTextReaderImpl.curNode.SetNamedNode(XmlNodeType.ProcessingInstruction, text);
						}
					}
					else
					{
						piInDtdStringBuilder.Append(text);
					}
					ch = xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos];
					configuredTaskAwaiter3 = xmlTextReaderImpl.EatWhitespacesAsync(piInDtdStringBuilder).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParsePIAsync>d__561>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_1E5:
					if (configuredTaskAwaiter3.GetResult() != 0)
					{
						goto IL_2CC;
					}
					if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos >= 2)
					{
						goto IL_274;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParsePIAsync>d__561>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_26C:
					configuredTaskAwaiter3.GetResult();
					IL_274:
					if (ch != '?' || xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos + 1] != '>')
					{
						xmlTextReaderImpl.Throw("The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charsUsed, xmlTextReaderImpl.ps.charPos));
					}
					IL_2CC:
					configuredTaskAwaiter4 = xmlTextReaderImpl.ParsePIValueAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParsePIAsync>d__561>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_32D:
					Tuple<int, int, bool> result2 = configuredTaskAwaiter4.GetResult();
					int item = result2.Item1;
					int item2 = result2.Item2;
					if (result2.Item3)
					{
						if (piInDtdStringBuilder != null)
						{
							piInDtdStringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
							goto IL_56E;
						}
						if (xmlTextReaderImpl.ignorePIs)
						{
							result3 = false;
							goto IL_58B;
						}
						if (xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
						{
							xmlTextReaderImpl.curNode.SetValue(xmlTextReaderImpl.ps.chars, item, item2 - item);
							goto IL_56E;
						}
						goto IL_56E;
					}
					else
					{
						if (piInDtdStringBuilder != null)
						{
							sb = piInDtdStringBuilder;
							goto IL_479;
						}
						if (!xmlTextReaderImpl.ignorePIs && xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
						{
							sb = xmlTextReaderImpl.stringBuilder;
							goto IL_479;
						}
					}
					IL_3D0:
					configuredTaskAwaiter4 = xmlTextReaderImpl.ParsePIValueAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParsePIAsync>d__561>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_431:
					Tuple<int, int, bool> result4 = configuredTaskAwaiter4.GetResult();
					item = result4.Item1;
					item2 = result4.Item2;
					if (result4.Item3)
					{
						result3 = false;
						goto IL_58B;
					}
					goto IL_3D0;
					IL_479:
					sb.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
					configuredTaskAwaiter4 = xmlTextReaderImpl.ParsePIValueAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParsePIAsync>d__561>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_4F8:
					Tuple<int, int, bool> result5 = configuredTaskAwaiter4.GetResult();
					item = result5.Item1;
					item2 = result5.Item2;
					if (!result5.Item3)
					{
						goto IL_479;
					}
					sb.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
					if (piInDtdStringBuilder == null)
					{
						xmlTextReaderImpl.curNode.SetValue(xmlTextReaderImpl.stringBuilder.ToString());
						xmlTextReaderImpl.stringBuilder.Length = 0;
					}
					sb = null;
					IL_56E:
					result3 = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_58B:
				num2 = -2;
				this.<>t__builder.SetResult(result3);
			}

			// Token: 0x06000D63 RID: 3427 RVA: 0x0004A240 File Offset: 0x00048440
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000875 RID: 2165
			public int <>1__state;

			// Token: 0x04000876 RID: 2166
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000877 RID: 2167
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000878 RID: 2168
			public StringBuilder piInDtdStringBuilder;

			// Token: 0x04000879 RID: 2169
			private char <ch>5__1;

			// Token: 0x0400087A RID: 2170
			private StringBuilder <sb>5__2;

			// Token: 0x0400087B RID: 2171
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400087C RID: 2172
			private ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000164 RID: 356
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParsePIValueAsync>d__562 : IAsyncStateMachine
		{
			// Token: 0x06000D64 RID: 3428 RVA: 0x0004A250 File Offset: 0x00048450
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, int, bool> result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos >= 2)
						{
							goto IL_B3;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParsePIValueAsync>d__562>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						xmlTextReaderImpl.Throw(xmlTextReaderImpl.ps.charsUsed, "Unexpected end of file while parsing {0} has occurred.", "PI");
					}
					IL_B3:
					int num3 = xmlTextReaderImpl.ps.charPos;
					char[] chars = xmlTextReaderImpl.ps.chars;
					int num4 = 0;
					int num5 = -1;
					for (;;)
					{
						char c;
						if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)(c = chars[num3])] & 64) == 0 || c == '?')
						{
							char c2 = chars[num3];
							if (c2 <= '&')
							{
								switch (c2)
								{
								case '\t':
									break;
								case '\n':
									num3++;
									xmlTextReaderImpl.OnNewLine(num3);
									continue;
								case '\v':
								case '\f':
									goto IL_2A6;
								case '\r':
									if (chars[num3 + 1] == '\n')
									{
										if (!xmlTextReaderImpl.ps.eolNormalized && xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
										{
											if (num3 - xmlTextReaderImpl.ps.charPos > 0)
											{
												if (num4 == 0)
												{
													num4 = 1;
													num5 = num3;
												}
												else
												{
													xmlTextReaderImpl.ShiftBuffer(num5 + num4, num5, num3 - num5 - num4);
													num5 = num3 - num4;
													num4++;
												}
											}
											else
											{
												XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
												xmlTextReaderImpl2.ps.charPos = xmlTextReaderImpl2.ps.charPos + 1;
											}
										}
										num3 += 2;
									}
									else
									{
										if (num3 + 1 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
										{
											goto IL_309;
										}
										if (!xmlTextReaderImpl.ps.eolNormalized)
										{
											chars[num3] = '\n';
										}
										num3++;
									}
									xmlTextReaderImpl.OnNewLine(num3);
									continue;
								default:
									if (c2 != '&')
									{
										goto IL_2A6;
									}
									break;
								}
							}
							else if (c2 != '<')
							{
								if (c2 != '?')
								{
									if (c2 != ']')
									{
										goto IL_2A6;
									}
								}
								else
								{
									if (chars[num3 + 1] == '>')
									{
										break;
									}
									if (num3 + 1 != xmlTextReaderImpl.ps.charsUsed)
									{
										num3++;
										continue;
									}
									goto IL_309;
								}
							}
							num3++;
							continue;
							IL_2A6:
							if (num3 == xmlTextReaderImpl.ps.charsUsed)
							{
								goto IL_309;
							}
							if (XmlCharType.IsHighSurrogate((int)chars[num3]))
							{
								if (num3 + 1 == xmlTextReaderImpl.ps.charsUsed)
								{
									goto IL_309;
								}
								num3++;
								if (XmlCharType.IsLowSurrogate((int)chars[num3]))
								{
									num3++;
									continue;
								}
							}
							xmlTextReaderImpl.ThrowInvalidChar(chars, xmlTextReaderImpl.ps.charsUsed, num3);
						}
						else
						{
							num3++;
						}
					}
					int item;
					if (num4 > 0)
					{
						xmlTextReaderImpl.ShiftBuffer(num5 + num4, num5, num3 - num5 - num4);
						item = num3 - num4;
					}
					else
					{
						item = num3;
					}
					int charPos = xmlTextReaderImpl.ps.charPos;
					xmlTextReaderImpl.ps.charPos = num3 + 2;
					result = new Tuple<int, int, bool>(charPos, item, true);
					goto IL_369;
					IL_309:
					if (num4 > 0)
					{
						xmlTextReaderImpl.ShiftBuffer(num5 + num4, num5, num3 - num5 - num4);
						item = num3 - num4;
					}
					else
					{
						item = num3;
					}
					int charPos2 = xmlTextReaderImpl.ps.charPos;
					xmlTextReaderImpl.ps.charPos = num3;
					result = new Tuple<int, int, bool>(charPos2, item, false);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_369:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D65 RID: 3429 RVA: 0x0004A5F8 File Offset: 0x000487F8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400087D RID: 2173
			public int <>1__state;

			// Token: 0x0400087E RID: 2174
			public AsyncTaskMethodBuilder<Tuple<int, int, bool>> <>t__builder;

			// Token: 0x0400087F RID: 2175
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000880 RID: 2176
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000165 RID: 357
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseCommentAsync>d__563 : IAsyncStateMachine
		{
			// Token: 0x06000D66 RID: 3430 RVA: 0x0004A608 File Offset: 0x00048808
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							if (xmlTextReaderImpl.ignoreComments)
							{
								oldParsingMode = xmlTextReaderImpl.parsingMode;
								xmlTextReaderImpl.parsingMode = XmlTextReaderImpl.ParsingMode.SkipNode;
								configuredTaskAwaiter = xmlTextReaderImpl.ParseCDataOrCommentAsync(XmlNodeType.Comment).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseCommentAsync>d__563>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_95;
							}
							else
							{
								configuredTaskAwaiter = xmlTextReaderImpl.ParseCDataOrCommentAsync(XmlNodeType.Comment).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseCommentAsync>d__563>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						result = true;
						goto IL_12F;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_95:
					configuredTaskAwaiter.GetResult();
					xmlTextReaderImpl.parsingMode = oldParsingMode;
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_12F:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D67 RID: 3431 RVA: 0x0004A774 File Offset: 0x00048974
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000881 RID: 2177
			public int <>1__state;

			// Token: 0x04000882 RID: 2178
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000883 RID: 2179
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000884 RID: 2180
			private XmlTextReaderImpl.ParsingMode <oldParsingMode>5__1;

			// Token: 0x04000885 RID: 2181
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000166 RID: 358
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseCDataOrCommentAsync>d__565 : IAsyncStateMachine
		{
			// Token: 0x06000D68 RID: 3432 RVA: 0x0004A784 File Offset: 0x00048984
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_176;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_247;
					}
					default:
						if (xmlTextReaderImpl.parsingMode != XmlTextReaderImpl.ParsingMode.Full)
						{
							goto IL_1E3;
						}
						xmlTextReaderImpl.curNode.SetLineInfo(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
						configuredTaskAwaiter = xmlTextReaderImpl.ParseCDataOrCommentTupleAsync(type).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseCDataOrCommentAsync>d__565>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					Tuple<int, int, bool> result = configuredTaskAwaiter.GetResult();
					int item = result.Item1;
					int item2 = result.Item2;
					if (result.Item3)
					{
						xmlTextReaderImpl.curNode.SetValueNode(type, xmlTextReaderImpl.ps.chars, item, item2 - item);
						goto IL_26C;
					}
					IL_F4:
					xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
					configuredTaskAwaiter = xmlTextReaderImpl.ParseCDataOrCommentTupleAsync(type).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseCDataOrCommentAsync>d__565>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_176:
					Tuple<int, int, bool> result2 = configuredTaskAwaiter.GetResult();
					item = result2.Item1;
					item2 = result2.Item2;
					if (result2.Item3)
					{
						xmlTextReaderImpl.stringBuilder.Append(xmlTextReaderImpl.ps.chars, item, item2 - item);
						xmlTextReaderImpl.curNode.SetValueNode(type, xmlTextReaderImpl.stringBuilder.ToString());
						xmlTextReaderImpl.stringBuilder.Length = 0;
						goto IL_26C;
					}
					goto IL_F4;
					IL_1E3:
					configuredTaskAwaiter = xmlTextReaderImpl.ParseCDataOrCommentTupleAsync(type).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseCDataOrCommentAsync>d__565>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_247:
					Tuple<int, int, bool> result3 = configuredTaskAwaiter.GetResult();
					item = result3.Item1;
					item2 = result3.Item2;
					if (!result3.Item3)
					{
						goto IL_1E3;
					}
					IL_26C:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D69 RID: 3433 RVA: 0x0004AA48 File Offset: 0x00048C48
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000886 RID: 2182
			public int <>1__state;

			// Token: 0x04000887 RID: 2183
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000888 RID: 2184
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000889 RID: 2185
			public XmlNodeType type;

			// Token: 0x0400088A RID: 2186
			private ConfiguredTaskAwaitable<Tuple<int, int, bool>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000167 RID: 359
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseCDataOrCommentTupleAsync>d__566 : IAsyncStateMachine
		{
			// Token: 0x06000D6A RID: 3434 RVA: 0x0004AA58 File Offset: 0x00048C58
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, int, bool> result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos >= 3)
						{
							goto IL_B8;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseCDataOrCommentTupleAsync>d__566>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						xmlTextReaderImpl.Throw("Unexpected end of file while parsing {0} has occurred.", (type == XmlNodeType.Comment) ? "Comment" : "CDATA");
					}
					IL_B8:
					int num3 = xmlTextReaderImpl.ps.charPos;
					char[] chars = xmlTextReaderImpl.ps.chars;
					int num4 = 0;
					int num5 = -1;
					char c = (type == XmlNodeType.Comment) ? '-' : ']';
					for (;;)
					{
						char c2;
						if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)(c2 = chars[num3])] & 64) == 0 || c2 == c)
						{
							if (chars[num3] == c)
							{
								if (chars[num3 + 1] == c)
								{
									if (chars[num3 + 2] == '>')
									{
										break;
									}
									if (num3 + 2 == xmlTextReaderImpl.ps.charsUsed)
									{
										goto IL_35A;
									}
									if (type == XmlNodeType.Comment)
									{
										xmlTextReaderImpl.Throw(num3, "An XML comment cannot contain '--', and '-' cannot be the last character.");
									}
								}
								else if (num3 + 1 == xmlTextReaderImpl.ps.charsUsed)
								{
									goto IL_35A;
								}
								num3++;
							}
							else
							{
								char c3 = chars[num3];
								if (c3 <= '&')
								{
									switch (c3)
									{
									case '\t':
										break;
									case '\n':
										num3++;
										xmlTextReaderImpl.OnNewLine(num3);
										continue;
									case '\v':
									case '\f':
										goto IL_2FC;
									case '\r':
										if (chars[num3 + 1] == '\n')
										{
											if (!xmlTextReaderImpl.ps.eolNormalized && xmlTextReaderImpl.parsingMode == XmlTextReaderImpl.ParsingMode.Full)
											{
												if (num3 - xmlTextReaderImpl.ps.charPos > 0)
												{
													if (num4 == 0)
													{
														num4 = 1;
														num5 = num3;
													}
													else
													{
														xmlTextReaderImpl.ShiftBuffer(num5 + num4, num5, num3 - num5 - num4);
														num5 = num3 - num4;
														num4++;
													}
												}
												else
												{
													XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
													xmlTextReaderImpl2.ps.charPos = xmlTextReaderImpl2.ps.charPos + 1;
												}
											}
											num3 += 2;
										}
										else
										{
											if (num3 + 1 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
											{
												goto IL_35A;
											}
											if (!xmlTextReaderImpl.ps.eolNormalized)
											{
												chars[num3] = '\n';
											}
											num3++;
										}
										xmlTextReaderImpl.OnNewLine(num3);
										continue;
									default:
										if (c3 != '&')
										{
											goto IL_2FC;
										}
										break;
									}
								}
								else if (c3 != '<' && c3 != ']')
								{
									goto IL_2FC;
								}
								num3++;
								continue;
								IL_2FC:
								if (num3 == xmlTextReaderImpl.ps.charsUsed)
								{
									goto IL_35A;
								}
								if (!XmlCharType.IsHighSurrogate((int)chars[num3]))
								{
									goto IL_345;
								}
								if (num3 + 1 == xmlTextReaderImpl.ps.charsUsed)
								{
									goto IL_35A;
								}
								num3++;
								if (!XmlCharType.IsLowSurrogate((int)chars[num3]))
								{
									goto IL_345;
								}
								num3++;
							}
						}
						else
						{
							num3++;
						}
					}
					int item;
					if (num4 > 0)
					{
						xmlTextReaderImpl.ShiftBuffer(num5 + num4, num5, num3 - num5 - num4);
						item = num3 - num4;
					}
					else
					{
						item = num3;
					}
					int charPos = xmlTextReaderImpl.ps.charPos;
					xmlTextReaderImpl.ps.charPos = num3 + 3;
					result = new Tuple<int, int, bool>(charPos, item, true);
					goto IL_3BA;
					IL_345:
					xmlTextReaderImpl.ThrowInvalidChar(chars, xmlTextReaderImpl.ps.charsUsed, num3);
					IL_35A:
					if (num4 > 0)
					{
						xmlTextReaderImpl.ShiftBuffer(num5 + num4, num5, num3 - num5 - num4);
						item = num3 - num4;
					}
					else
					{
						item = num3;
					}
					int charPos2 = xmlTextReaderImpl.ps.charPos;
					xmlTextReaderImpl.ps.charPos = num3;
					result = new Tuple<int, int, bool>(charPos2, item, false);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_3BA:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D6B RID: 3435 RVA: 0x0004AE50 File Offset: 0x00049050
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400088B RID: 2187
			public int <>1__state;

			// Token: 0x0400088C RID: 2188
			public AsyncTaskMethodBuilder<Tuple<int, int, bool>> <>t__builder;

			// Token: 0x0400088D RID: 2189
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400088E RID: 2190
			public XmlNodeType type;

			// Token: 0x0400088F RID: 2191
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000168 RID: 360
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseDoctypeDeclAsync>d__567 : IAsyncStateMachine
		{
			// Token: 0x06000D6C RID: 3436 RVA: 0x0004AE60 File Offset: 0x00049060
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_20C;
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2A2;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_322;
					}
					default:
						if (xmlTextReaderImpl.dtdProcessing == DtdProcessing.Prohibit)
						{
							xmlTextReaderImpl.ThrowWithoutLineInfo(xmlTextReaderImpl.v1Compat ? "DTD is prohibited in this XML document." : "For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.");
							goto IL_C2;
						}
						goto IL_C2;
					}
					IL_A9:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						xmlTextReaderImpl.Throw("Unexpected end of file while parsing {0} has occurred.", "DOCTYPE");
					}
					IL_C2:
					if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos >= 8)
					{
						if (!XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, 7, "DOCTYPE"))
						{
							xmlTextReaderImpl.ThrowUnexpectedToken((!xmlTextReaderImpl.rootElementParsed && xmlTextReaderImpl.dtdInfo == null) ? "DOCTYPE" : "<!--");
						}
						if (!xmlTextReaderImpl.xmlCharType.IsWhiteSpace(xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos + 7]))
						{
							xmlTextReaderImpl.ThrowExpectingWhitespace(xmlTextReaderImpl.ps.charPos + 7);
						}
						if (xmlTextReaderImpl.dtdInfo != null)
						{
							xmlTextReaderImpl.Throw(xmlTextReaderImpl.ps.charPos - 2, "Cannot have multiple DTDs.");
						}
						if (xmlTextReaderImpl.rootElementParsed)
						{
							xmlTextReaderImpl.Throw(xmlTextReaderImpl.ps.charPos - 2, "DTD must be defined before the document root element.");
						}
						XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
						xmlTextReaderImpl2.ps.charPos = xmlTextReaderImpl2.ps.charPos + 8;
						configuredTaskAwaiter3 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDoctypeDeclAsync>d__567>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDoctypeDeclAsync>d__567>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_A9;
					}
					IL_20C:
					configuredTaskAwaiter3.GetResult();
					if (xmlTextReaderImpl.dtdProcessing == DtdProcessing.Parse)
					{
						xmlTextReaderImpl.curNode.SetLineInfo(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
						configuredTaskAwaiter4 = xmlTextReaderImpl.ParseDtdAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDoctypeDeclAsync>d__567>(ref configuredTaskAwaiter4, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter4 = xmlTextReaderImpl.SkipDtdAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDoctypeDeclAsync>d__567>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_322;
					}
					IL_2A2:
					configuredTaskAwaiter4.GetResult();
					xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.parsingFunction;
					xmlTextReaderImpl.parsingFunction = XmlTextReaderImpl.ParsingFunction.ResetAttributesRootLevel;
					result = true;
					goto IL_346;
					IL_322:
					configuredTaskAwaiter4.GetResult();
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_346:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D6D RID: 3437 RVA: 0x0004B1E4 File Offset: 0x000493E4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000890 RID: 2192
			public int <>1__state;

			// Token: 0x04000891 RID: 2193
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000892 RID: 2194
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000893 RID: 2195
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000894 RID: 2196
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000169 RID: 361
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseDtdAsync>d__568 : IAsyncStateMachine
		{
			// Token: 0x06000D6E RID: 3438 RVA: 0x0004B1F4 File Offset: 0x000493F4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = DtdParser.Create().ParseInternalDtdAsync(new XmlTextReaderImpl.DtdParserProxy(xmlTextReaderImpl), true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDtdAsync>d__568>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IDtdInfo result = configuredTaskAwaiter.GetResult();
					xmlTextReaderImpl.dtdInfo = result;
					if ((xmlTextReaderImpl.validatingReaderCompatFlag || !xmlTextReaderImpl.v1Compat) && (xmlTextReaderImpl.dtdInfo.HasDefaultAttributes || xmlTextReaderImpl.dtdInfo.HasNonCDataAttributes))
					{
						xmlTextReaderImpl.addDefaultAttributesAndNormalize = true;
					}
					xmlTextReaderImpl.curNode.SetNamedNode(XmlNodeType.DocumentType, xmlTextReaderImpl.dtdInfo.Name.ToString(), string.Empty, null);
					xmlTextReaderImpl.curNode.SetValue(xmlTextReaderImpl.dtdInfo.InternalDtdSubset);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D6F RID: 3439 RVA: 0x0004B334 File Offset: 0x00049534
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000895 RID: 2197
			public int <>1__state;

			// Token: 0x04000896 RID: 2198
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000897 RID: 2199
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000898 RID: 2200
			private ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200016A RID: 362
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SkipDtdAsync>d__569 : IAsyncStateMachine
		{
			// Token: 0x06000D70 RID: 3440 RVA: 0x0004B344 File Offset: 0x00049544
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter6;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_12C;
					case 2:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1B5;
					case 3:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_286;
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_301;
					}
					case 5:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_36A;
					case 6:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3E5;
					}
					case 7:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_44E;
					case 8:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_4DC;
					case 9:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_5AE;
					case 10:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_62A;
					}
					case 11:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_694;
					case 12:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_772;
					}
					case 13:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_7DC;
					default:
						configuredTaskAwaiter3 = xmlTextReaderImpl.ParseQNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					Tuple<int, int> result = configuredTaskAwaiter3.GetResult();
					int item = result.Item1;
					int item2 = result.Item2;
					xmlTextReaderImpl.ps.charPos = item2;
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_12C:
					configuredTaskAwaiter5.GetResult();
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] == 'P')
					{
						goto IL_1C9;
					}
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] == 'S')
					{
						goto IL_4F0;
					}
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != '[' && xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != '>')
					{
						xmlTextReaderImpl.Throw("Expecting external ID, '[' or '>'.");
						goto IL_6DF;
					}
					goto IL_6DF;
					IL_1B5:
					if (configuredTaskAwaiter5.GetResult() == 0)
					{
						xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
					}
					IL_1C9:
					if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos >= 6)
					{
						if (!XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, 6, "PUBLIC"))
						{
							xmlTextReaderImpl.ThrowUnexpectedToken("PUBLIC");
						}
						XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
						xmlTextReaderImpl2.ps.charPos = xmlTextReaderImpl2.ps.charPos + 6;
						configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 3;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter5 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 2;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
							return;
						}
						goto IL_1B5;
					}
					IL_286:
					if (configuredTaskAwaiter5.GetResult() == 0)
					{
						xmlTextReaderImpl.ThrowExpectingWhitespace(xmlTextReaderImpl.ps.charPos);
					}
					configuredTaskAwaiter6 = xmlTextReaderImpl.SkipPublicOrSystemIdLiteralAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter6.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter6, ref this);
						return;
					}
					IL_301:
					configuredTaskAwaiter6.GetResult();
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 5;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_36A:
					if (configuredTaskAwaiter5.GetResult() == 0)
					{
						xmlTextReaderImpl.ThrowExpectingWhitespace(xmlTextReaderImpl.ps.charPos);
					}
					configuredTaskAwaiter6 = xmlTextReaderImpl.SkipPublicOrSystemIdLiteralAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter6.IsCompleted)
					{
						num2 = 6;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter6, ref this);
						return;
					}
					IL_3E5:
					configuredTaskAwaiter6.GetResult();
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 7;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_44E:
					configuredTaskAwaiter5.GetResult();
					goto IL_6DF;
					IL_4DC:
					if (configuredTaskAwaiter5.GetResult() == 0)
					{
						xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
					}
					IL_4F0:
					if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos >= 6)
					{
						if (!XmlConvert.StrEqual(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, 6, "SYSTEM"))
						{
							xmlTextReaderImpl.ThrowUnexpectedToken("SYSTEM");
						}
						XmlTextReaderImpl xmlTextReaderImpl3 = xmlTextReaderImpl;
						xmlTextReaderImpl3.ps.charPos = xmlTextReaderImpl3.ps.charPos + 6;
						configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 9;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter5 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 8;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
							return;
						}
						goto IL_4DC;
					}
					IL_5AE:
					if (configuredTaskAwaiter5.GetResult() == 0)
					{
						xmlTextReaderImpl.ThrowExpectingWhitespace(xmlTextReaderImpl.ps.charPos);
					}
					configuredTaskAwaiter6 = xmlTextReaderImpl.SkipPublicOrSystemIdLiteralAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter6.IsCompleted)
					{
						num2 = 10;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter6, ref this);
						return;
					}
					IL_62A:
					configuredTaskAwaiter6.GetResult();
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 11;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_694:
					configuredTaskAwaiter5.GetResult();
					IL_6DF:
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] == '[')
					{
						XmlTextReaderImpl xmlTextReaderImpl4 = xmlTextReaderImpl;
						xmlTextReaderImpl4.ps.charPos = xmlTextReaderImpl4.ps.charPos + 1;
						configuredTaskAwaiter6 = xmlTextReaderImpl.SkipUntilAsync(']', true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter6.IsCompleted)
						{
							num2 = 12;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter6, ref this);
							return;
						}
					}
					else
					{
						if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] == '>')
						{
							xmlTextReaderImpl.curNode.SetValue(string.Empty);
							goto IL_844;
						}
						xmlTextReaderImpl.Throw("Expecting an internal subset or the end of the DOCTYPE declaration.");
						goto IL_844;
					}
					IL_772:
					configuredTaskAwaiter6.GetResult();
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 13;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipDtdAsync>d__569>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_7DC:
					configuredTaskAwaiter5.GetResult();
					if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != '>')
					{
						xmlTextReaderImpl.ThrowUnexpectedToken(">");
					}
					IL_844:
					XmlTextReaderImpl xmlTextReaderImpl5 = xmlTextReaderImpl;
					xmlTextReaderImpl5.ps.charPos = xmlTextReaderImpl5.ps.charPos + 1;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D71 RID: 3441 RVA: 0x0004BBF0 File Offset: 0x00049DF0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000899 RID: 2201
			public int <>1__state;

			// Token: 0x0400089A RID: 2202
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400089B RID: 2203
			public XmlTextReaderImpl <>4__this;

			// Token: 0x0400089C RID: 2204
			private ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400089D RID: 2205
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x0400089E RID: 2206
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x0200016B RID: 363
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SkipUntilAsync>d__571 : IAsyncStateMachine
		{
			// Token: 0x06000D72 RID: 3442 RVA: 0x0004BC00 File Offset: 0x00049E00
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3EA;
					}
					inLiteral = false;
					inComment = false;
					inPI = false;
					literalQuote = '"';
					char[] chars = xmlTextReaderImpl.ps.chars;
					int num3 = xmlTextReaderImpl.ps.charPos;
					for (;;)
					{
						IL_4F:
						char c;
						if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)(c = chars[num3])] & 128) == 0 || chars[num3] == stopChar || c == '-' || c == '?')
						{
							if (c == stopChar && !inLiteral)
							{
								break;
							}
							xmlTextReaderImpl.ps.charPos = num3;
							if (c <= '&')
							{
								switch (c)
								{
								case '\t':
									break;
								case '\n':
									num3++;
									xmlTextReaderImpl.OnNewLine(num3);
									continue;
								case '\v':
								case '\f':
									goto IL_337;
								case '\r':
									if (chars[num3 + 1] == '\n')
									{
										num3 += 2;
									}
									else
									{
										if (num3 + 1 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
										{
											goto IL_389;
										}
										num3++;
									}
									xmlTextReaderImpl.OnNewLine(num3);
									continue;
								default:
									if (c == '"')
									{
										goto IL_2EC;
									}
									if (c != '&')
									{
										goto IL_337;
									}
									break;
								}
							}
							else if (c <= '-')
							{
								if (c == '\'')
								{
									goto IL_2EC;
								}
								if (c != '-')
								{
									goto IL_337;
								}
								if (inComment)
								{
									if (num3 + 2 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
									{
										goto IL_389;
									}
									if (chars[num3 + 1] == '-' && chars[num3 + 2] == '>')
									{
										inComment = false;
										num3 += 2;
										continue;
									}
								}
								num3++;
								continue;
							}
							else
							{
								switch (c)
								{
								case '<':
									if (chars[num3 + 1] == '?')
									{
										if (recognizeLiterals && !inLiteral && !inComment)
										{
											inPI = true;
											num3 += 2;
											continue;
										}
									}
									else if (chars[num3 + 1] == '!')
									{
										if (num3 + 3 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
										{
											goto IL_389;
										}
										if (chars[num3 + 2] == '-' && chars[num3 + 3] == '-' && recognizeLiterals && !inLiteral && !inPI)
										{
											inComment = true;
											num3 += 4;
											continue;
										}
									}
									else if (num3 + 1 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
									{
										goto IL_389;
									}
									num3++;
									continue;
								case '=':
									goto IL_337;
								case '>':
									break;
								case '?':
									if (inPI)
									{
										if (num3 + 1 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
										{
											goto IL_389;
										}
										if (chars[num3 + 1] == '>')
										{
											inPI = false;
											num3++;
											continue;
										}
									}
									num3++;
									continue;
								default:
									if (c != ']')
									{
										goto IL_337;
									}
									break;
								}
							}
							num3++;
							continue;
							IL_2EC:
							if (inLiteral)
							{
								if (literalQuote == c)
								{
									inLiteral = false;
								}
							}
							else if (recognizeLiterals && !inComment && !inPI)
							{
								inLiteral = true;
								literalQuote = c;
							}
							num3++;
							continue;
							IL_337:
							if (num3 == xmlTextReaderImpl.ps.charsUsed)
							{
								goto IL_389;
							}
							if (!XmlCharType.IsHighSurrogate((int)chars[num3]))
							{
								goto IL_376;
							}
							if (num3 + 1 == xmlTextReaderImpl.ps.charsUsed)
							{
								goto IL_389;
							}
							num3++;
							if (!XmlCharType.IsLowSurrogate((int)chars[num3]))
							{
								goto IL_376;
							}
							num3++;
						}
						else
						{
							num3++;
						}
					}
					xmlTextReaderImpl.ps.charPos = num3 + 1;
					goto IL_476;
					IL_376:
					xmlTextReaderImpl.ThrowInvalidChar(chars, xmlTextReaderImpl.ps.charsUsed, num3);
					IL_389:
					configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<SkipUntilAsync>d__571>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_3EA:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos > 0)
						{
							if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != '\r')
							{
								xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
							}
						}
						else
						{
							xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
						}
					}
					chars = xmlTextReaderImpl.ps.chars;
					num3 = xmlTextReaderImpl.ps.charPos;
					goto IL_4F;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_476:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D73 RID: 3443 RVA: 0x0004C0B4 File Offset: 0x0004A2B4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400089F RID: 2207
			public int <>1__state;

			// Token: 0x040008A0 RID: 2208
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040008A1 RID: 2209
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008A2 RID: 2210
			public char stopChar;

			// Token: 0x040008A3 RID: 2211
			private bool <inLiteral>5__1;

			// Token: 0x040008A4 RID: 2212
			public bool recognizeLiterals;

			// Token: 0x040008A5 RID: 2213
			private bool <inComment>5__2;

			// Token: 0x040008A6 RID: 2214
			private bool <inPI>5__3;

			// Token: 0x040008A7 RID: 2215
			private char <literalQuote>5__4;

			// Token: 0x040008A8 RID: 2216
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200016C RID: 364
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <EatWhitespacesAsync>d__572 : IAsyncStateMachine
		{
			// Token: 0x06000D74 RID: 3444 RVA: 0x0004C0C4 File Offset: 0x0004A2C4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_261;
					}
					int num3 = xmlTextReaderImpl.ps.charPos;
					wsCount = 0;
					char[] chars = xmlTextReaderImpl.ps.chars;
					for (;;)
					{
						IL_34:
						char c = chars[num3];
						switch (c)
						{
						case '\t':
							break;
						case '\n':
							num3++;
							xmlTextReaderImpl.OnNewLine(num3);
							continue;
						case '\v':
						case '\f':
							goto IL_130;
						case '\r':
							if (chars[num3 + 1] == '\n')
							{
								int num4 = num3 - xmlTextReaderImpl.ps.charPos;
								if (sb != null && !xmlTextReaderImpl.ps.eolNormalized)
								{
									if (num4 > 0)
									{
										sb.Append(chars, xmlTextReaderImpl.ps.charPos, num4);
										wsCount += num4;
									}
									xmlTextReaderImpl.ps.charPos = num3 + 1;
								}
								num3 += 2;
							}
							else
							{
								if (num3 + 1 >= xmlTextReaderImpl.ps.charsUsed && !xmlTextReaderImpl.ps.isEof)
								{
									goto IL_1A5;
								}
								if (!xmlTextReaderImpl.ps.eolNormalized)
								{
									chars[num3] = '\n';
								}
								num3++;
							}
							xmlTextReaderImpl.OnNewLine(num3);
							continue;
						default:
							if (c != ' ')
							{
								goto Block_5;
							}
							break;
						}
						num3++;
					}
					Block_5:
					IL_130:
					if (num3 != xmlTextReaderImpl.ps.charsUsed)
					{
						int num5 = num3 - xmlTextReaderImpl.ps.charPos;
						if (num5 > 0)
						{
							if (sb != null)
							{
								sb.Append(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, num5);
							}
							xmlTextReaderImpl.ps.charPos = num3;
							wsCount += num5;
						}
						result = wsCount;
						goto IL_2E9;
					}
					IL_1A5:
					int num6 = num3 - xmlTextReaderImpl.ps.charPos;
					if (num6 > 0)
					{
						if (sb != null)
						{
							sb.Append(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, num6);
						}
						xmlTextReaderImpl.ps.charPos = num3;
						wsCount += num6;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<EatWhitespacesAsync>d__572>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_261:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						if (xmlTextReaderImpl.ps.charsUsed - xmlTextReaderImpl.ps.charPos == 0)
						{
							result = wsCount;
							goto IL_2E9;
						}
						if (xmlTextReaderImpl.ps.chars[xmlTextReaderImpl.ps.charPos] != '\r')
						{
							xmlTextReaderImpl.Throw("Unexpected end of file has occurred.");
						}
					}
					num3 = xmlTextReaderImpl.ps.charPos;
					chars = xmlTextReaderImpl.ps.chars;
					goto IL_34;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2E9:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D75 RID: 3445 RVA: 0x0004C3EC File Offset: 0x0004A5EC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008A9 RID: 2217
			public int <>1__state;

			// Token: 0x040008AA RID: 2218
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x040008AB RID: 2219
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008AC RID: 2220
			public StringBuilder sb;

			// Token: 0x040008AD RID: 2221
			private int <wsCount>5__1;

			// Token: 0x040008AE RID: 2222
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200016D RID: 365
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseNumericCharRefAsync>d__573 : IAsyncStateMachine
		{
			// Token: 0x06000D76 RID: 3446 RVA: 0x0004C3FC File Offset: 0x0004A5FC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<XmlTextReaderImpl.EntityType, int> result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_AA;
					}
					IL_11:
					int num3 = newPos = xmlTextReaderImpl.ParseNumericCharRefInline(xmlTextReaderImpl.ps.charPos, expand, internalSubsetBuilder, out charCount, out entityType);
					if (num3 != -2)
					{
						if (expand)
						{
							xmlTextReaderImpl.ps.charPos = newPos - charCount;
						}
						result = new Tuple<XmlTextReaderImpl.EntityType, int>(entityType, newPos);
						goto IL_113;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseNumericCharRefAsync>d__573>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_AA:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						xmlTextReaderImpl.Throw("Unexpected end of file while parsing {0} has occurred.");
						goto IL_11;
					}
					goto IL_11;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_113:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D77 RID: 3447 RVA: 0x0004C540 File Offset: 0x0004A740
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008AF RID: 2223
			public int <>1__state;

			// Token: 0x040008B0 RID: 2224
			public AsyncTaskMethodBuilder<Tuple<XmlTextReaderImpl.EntityType, int>> <>t__builder;

			// Token: 0x040008B1 RID: 2225
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008B2 RID: 2226
			public bool expand;

			// Token: 0x040008B3 RID: 2227
			public StringBuilder internalSubsetBuilder;

			// Token: 0x040008B4 RID: 2228
			private int <newPos>5__1;

			// Token: 0x040008B5 RID: 2229
			private int <charCount>5__2;

			// Token: 0x040008B6 RID: 2230
			private XmlTextReaderImpl.EntityType <entityType>5__3;

			// Token: 0x040008B7 RID: 2231
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200016E RID: 366
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseNamedCharRefAsync>d__574 : IAsyncStateMachine
		{
			// Token: 0x06000D78 RID: 3448 RVA: 0x0004C550 File Offset: 0x0004A750
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_A6;
					}
					IL_11:
					int num3 = newPos = xmlTextReaderImpl.ParseNamedCharRefInline(xmlTextReaderImpl.ps.charPos, expand, internalSubsetBuilder);
					if (num3 != -2)
					{
						if (num3 == -1)
						{
							result = -1;
							goto IL_F3;
						}
						if (expand)
						{
							xmlTextReaderImpl.ps.charPos = newPos - 1;
						}
						result = newPos;
						goto IL_F3;
					}
					else
					{
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseNamedCharRefAsync>d__574>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					IL_A6:
					if (configuredTaskAwaiter3.GetResult() != 0)
					{
						goto IL_11;
					}
					result = -1;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_F3:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D79 RID: 3449 RVA: 0x0004C674 File Offset: 0x0004A874
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008B8 RID: 2232
			public int <>1__state;

			// Token: 0x040008B9 RID: 2233
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x040008BA RID: 2234
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008BB RID: 2235
			public bool expand;

			// Token: 0x040008BC RID: 2236
			public StringBuilder internalSubsetBuilder;

			// Token: 0x040008BD RID: 2237
			private int <newPos>5__1;

			// Token: 0x040008BE RID: 2238
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200016F RID: 367
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseNameAsync>d__575 : IAsyncStateMachine
		{
			// Token: 0x06000D7A RID: 3450 RVA: 0x0004C684 File Offset: 0x0004A884
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int item;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = xmlTextReaderImpl.ParseQNameAsync(false, 0).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseNameAsync>d__575>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					item = configuredTaskAwaiter.GetResult().Item2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(item);
			}

			// Token: 0x06000D7B RID: 3451 RVA: 0x0004C74C File Offset: 0x0004A94C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008BF RID: 2239
			public int <>1__state;

			// Token: 0x040008C0 RID: 2240
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x040008C1 RID: 2241
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008C2 RID: 2242
			private ConfiguredTaskAwaitable<Tuple<int, int>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000170 RID: 368
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseQNameAsync>d__577 : IAsyncStateMachine
		{
			// Token: 0x06000D7C RID: 3452 RVA: 0x0004C75C File Offset: 0x0004A95C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, int> result3;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_11C;
					}
					int num3;
					if (num != 1)
					{
						colonOffset = -1;
						num3 = xmlTextReaderImpl.ps.charPos + startOffset;
						goto IL_35;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter);
					num2 = -1;
					goto IL_247;
					for (;;)
					{
						IL_145:
						if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)chars[num3]] & 8) != 0)
						{
							num3++;
						}
						else
						{
							if (chars[num3] != ':')
							{
								goto IL_1D4;
							}
							if (xmlTextReaderImpl.supportNamespaces)
							{
								break;
							}
							colonOffset = num3 - xmlTextReaderImpl.ps.charPos;
							num3++;
						}
					}
					if (colonOffset != -1 || !isQName)
					{
						xmlTextReaderImpl.Throw(num3, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
					}
					colonOffset = num3 - xmlTextReaderImpl.ps.charPos;
					num3++;
					goto IL_35;
					IL_1D4:
					if (num3 != xmlTextReaderImpl.ps.charsUsed)
					{
						goto IL_283;
					}
					configuredTaskAwaiter = xmlTextReaderImpl.ReadDataInNameAsync(num3).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseQNameAsync>d__577>(ref configuredTaskAwaiter, ref this);
						return;
					}
					goto IL_247;
					IL_35:
					chars = xmlTextReaderImpl.ps.chars;
					bool flag = false;
					if ((xmlTextReaderImpl.xmlCharType.charProperties[(int)chars[num3]] & 4) != 0)
					{
						num3++;
					}
					else if (num3 + 1 >= xmlTextReaderImpl.ps.charsUsed)
					{
						flag = true;
					}
					else if (chars[num3] != ':' || xmlTextReaderImpl.supportNamespaces)
					{
						xmlTextReaderImpl.Throw(num3, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(chars, xmlTextReaderImpl.ps.charsUsed, num3));
					}
					if (!flag)
					{
						goto IL_145;
					}
					configuredTaskAwaiter = xmlTextReaderImpl.ReadDataInNameAsync(num3).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseQNameAsync>d__577>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_11C:
					Tuple<int, bool> result = configuredTaskAwaiter.GetResult();
					num3 = result.Item1;
					if (!result.Item2)
					{
						xmlTextReaderImpl.Throw(num3, "Unexpected end of file while parsing {0} has occurred.", "Name");
						goto IL_145;
					}
					goto IL_35;
					IL_247:
					Tuple<int, bool> result2 = configuredTaskAwaiter.GetResult();
					num3 = result2.Item1;
					if (result2.Item2)
					{
						chars = xmlTextReaderImpl.ps.chars;
						goto IL_145;
					}
					xmlTextReaderImpl.Throw(num3, "Unexpected end of file while parsing {0} has occurred.", "Name");
					IL_283:
					result3 = new Tuple<int, int>((colonOffset == -1) ? -1 : (xmlTextReaderImpl.ps.charPos + colonOffset), num3);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result3);
			}

			// Token: 0x06000D7D RID: 3453 RVA: 0x0004CA5C File Offset: 0x0004AC5C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008C3 RID: 2243
			public int <>1__state;

			// Token: 0x040008C4 RID: 2244
			public AsyncTaskMethodBuilder<Tuple<int, int>> <>t__builder;

			// Token: 0x040008C5 RID: 2245
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008C6 RID: 2246
			public int startOffset;

			// Token: 0x040008C7 RID: 2247
			private char[] <chars>5__1;

			// Token: 0x040008C8 RID: 2248
			private int <colonOffset>5__2;

			// Token: 0x040008C9 RID: 2249
			public bool isQName;

			// Token: 0x040008CA RID: 2250
			private ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000171 RID: 369
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadDataInNameAsync>d__578 : IAsyncStateMachine
		{
			// Token: 0x06000D7E RID: 3454 RVA: 0x0004CA6C File Offset: 0x0004AC6C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				Tuple<int, bool> result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						offset = pos - xmlTextReaderImpl.ps.charPos;
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadDataInNameAsync>d__578>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					bool item = configuredTaskAwaiter3.GetResult() != 0;
					pos = xmlTextReaderImpl.ps.charPos + offset;
					result = new Tuple<int, bool>(pos, item);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D7F RID: 3455 RVA: 0x0004CB70 File Offset: 0x0004AD70
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008CB RID: 2251
			public int <>1__state;

			// Token: 0x040008CC RID: 2252
			public AsyncTaskMethodBuilder<Tuple<int, bool>> <>t__builder;

			// Token: 0x040008CD RID: 2253
			public int pos;

			// Token: 0x040008CE RID: 2254
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008CF RID: 2255
			private int <offset>5__1;

			// Token: 0x040008D0 RID: 2256
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000172 RID: 370
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseEntityNameAsync>d__579 : IAsyncStateMachine
		{
			// Token: 0x06000D80 RID: 3456 RVA: 0x0004CB80 File Offset: 0x0004AD80
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				string result2;
				try
				{
					try
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							configuredTaskAwaiter = xmlTextReaderImpl.ParseNameAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseEntityNameAsync>d__579>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						int result = configuredTaskAwaiter.GetResult();
						endPos = result;
					}
					catch (XmlException)
					{
						xmlTextReaderImpl.Throw("An error occurred while parsing EntityName.");
						result2 = null;
						goto IL_11C;
					}
					if (xmlTextReaderImpl.ps.chars[endPos] != ';')
					{
						xmlTextReaderImpl.Throw("An error occurred while parsing EntityName.");
					}
					string text = xmlTextReaderImpl.nameTable.Add(xmlTextReaderImpl.ps.chars, xmlTextReaderImpl.ps.charPos, endPos - xmlTextReaderImpl.ps.charPos);
					xmlTextReaderImpl.ps.charPos = endPos + 1;
					result2 = text;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_11C:
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000D81 RID: 3457 RVA: 0x0004CCDC File Offset: 0x0004AEDC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008D1 RID: 2257
			public int <>1__state;

			// Token: 0x040008D2 RID: 2258
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x040008D3 RID: 2259
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008D4 RID: 2260
			private int <endPos>5__1;

			// Token: 0x040008D5 RID: 2261
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000173 RID: 371
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <PushExternalEntityOrSubsetAsync>d__580 : IAsyncStateMachine
		{
			// Token: 0x06000D82 RID: 3458 RVA: 0x0004CCEC File Offset: 0x0004AEEC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					if (num != 0)
					{
						if (num == 1)
						{
							goto IL_DC;
						}
						if (string.IsNullOrEmpty(publicId))
						{
							goto IL_BF;
						}
					}
					try
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 0)
						{
							uri = xmlTextReaderImpl.xmlResolver.ResolveUri(baseUri, publicId);
							configuredTaskAwaiter3 = xmlTextReaderImpl.OpenAndPushAsync(uri).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 0);
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<PushExternalEntityOrSubsetAsync>d__580>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						if (configuredTaskAwaiter3.GetResult())
						{
							goto IL_220;
						}
					}
					catch (Exception)
					{
					}
					IL_BF:
					uri = xmlTextReaderImpl.xmlResolver.ResolveUri(baseUri, systemId);
					IL_DC:
					try
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 1)
						{
							configuredTaskAwaiter3 = xmlTextReaderImpl.OpenAndPushAsync(uri).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num = (num2 = 1);
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<PushExternalEntityOrSubsetAsync>d__580>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						if (configuredTaskAwaiter3.GetResult())
						{
							goto IL_220;
						}
					}
					catch (Exception ex)
					{
						if (xmlTextReaderImpl.v1Compat)
						{
							throw;
						}
						string message = ex.Message;
						xmlTextReaderImpl.Throw(new XmlException((entityName == null) ? "An error has occurred while opening external DTD '{0}': {1}" : "An error has occurred while opening external entity '{0}': {1}", new string[]
						{
							uri.ToString(),
							message
						}, ex, 0, 0));
					}
					if (entityName == null)
					{
						xmlTextReaderImpl.ThrowWithoutLineInfo("Cannot resolve external DTD subset - public ID = '{0}', system ID = '{1}'.", new string[]
						{
							(publicId != null) ? publicId : string.Empty,
							systemId
						}, null);
					}
					else
					{
						xmlTextReaderImpl.Throw((xmlTextReaderImpl.dtdProcessing == DtdProcessing.Ignore) ? "Cannot resolve entity reference '{0}' because the DTD has been ignored. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method." : "Cannot resolve entity reference '{0}'.", entityName);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_220:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D83 RID: 3459 RVA: 0x0004CF78 File Offset: 0x0004B178
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008D6 RID: 2262
			public int <>1__state;

			// Token: 0x040008D7 RID: 2263
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040008D8 RID: 2264
			public string publicId;

			// Token: 0x040008D9 RID: 2265
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008DA RID: 2266
			public Uri baseUri;

			// Token: 0x040008DB RID: 2267
			public string systemId;

			// Token: 0x040008DC RID: 2268
			public string entityName;

			// Token: 0x040008DD RID: 2269
			private Uri <uri>5__1;

			// Token: 0x040008DE RID: 2270
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000174 RID: 372
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <OpenAndPushAsync>d__581 : IAsyncStateMachine
		{
			// Token: 0x06000D84 RID: 3460 RVA: 0x0004CF88 File Offset: 0x0004B188
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_14B;
					}
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1CE;
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_254;
					}
					default:
						if (xmlTextReaderImpl.xmlResolver.SupportsType(uri, typeof(TextReader)))
						{
							configuredTaskAwaiter3 = xmlTextReaderImpl.xmlResolver.GetEntityAsync(uri, null, typeof(TextReader)).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<OpenAndPushAsync>d__581>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter3 = xmlTextReaderImpl.xmlResolver.GetEntityAsync(uri, null, typeof(Stream)).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 2;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<OpenAndPushAsync>d__581>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_1CE;
						}
						break;
					}
					TextReader textReader = (TextReader)configuredTaskAwaiter3.GetResult();
					if (textReader == null)
					{
						result = false;
						goto IL_278;
					}
					xmlTextReaderImpl.PushParsingState();
					configuredTaskAwaiter4 = xmlTextReaderImpl.InitTextReaderInputAsync(uri.ToString(), uri, textReader).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<OpenAndPushAsync>d__581>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_14B:
					configuredTaskAwaiter4.GetResult();
					goto IL_25B;
					IL_1CE:
					Stream stream = (Stream)configuredTaskAwaiter3.GetResult();
					if (stream == null)
					{
						result = false;
						goto IL_278;
					}
					xmlTextReaderImpl.PushParsingState();
					configuredTaskAwaiter4 = xmlTextReaderImpl.InitStreamInputAsync(uri, stream, null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<OpenAndPushAsync>d__581>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_254:
					configuredTaskAwaiter4.GetResult();
					IL_25B:
					result = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_278:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D85 RID: 3461 RVA: 0x0004D240 File Offset: 0x0004B440
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008DF RID: 2271
			public int <>1__state;

			// Token: 0x040008E0 RID: 2272
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040008E1 RID: 2273
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008E2 RID: 2274
			public Uri uri;

			// Token: 0x040008E3 RID: 2275
			private ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040008E4 RID: 2276
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000175 RID: 373
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <PushExternalEntityAsync>d__582 : IAsyncStateMachine
		{
			// Token: 0x06000D86 RID: 3462 RVA: 0x0004D250 File Offset: 0x0004B450
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_168;
					}
					case 2:
						configuredTaskAwaiter7 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1D2;
					default:
					{
						if (xmlTextReaderImpl.IsResolverNull)
						{
							Encoding encoding = xmlTextReaderImpl.ps.encoding;
							xmlTextReaderImpl.PushParsingState();
							xmlTextReaderImpl.InitStringInput(entity.SystemId, encoding, string.Empty);
							xmlTextReaderImpl.RegisterEntity(entity);
							xmlTextReaderImpl.RegisterConsumedCharacters(0L, true);
							result = false;
							goto IL_24D;
						}
						Uri baseUri = null;
						if (!string.IsNullOrEmpty(entity.BaseUriString))
						{
							baseUri = xmlTextReaderImpl.xmlResolver.ResolveUri(null, entity.BaseUriString);
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.PushExternalEntityOrSubsetAsync(entity.PublicId, entity.SystemId, baseUri, entity.Name).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlTextReaderImpl.<PushExternalEntityAsync>d__582>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					}
					configuredTaskAwaiter3.GetResult();
					xmlTextReaderImpl.RegisterEntity(entity);
					initialPos = xmlTextReaderImpl.ps.charPos;
					if (!xmlTextReaderImpl.v1Compat)
					{
						goto IL_170;
					}
					configuredTaskAwaiter5 = xmlTextReaderImpl.EatWhitespacesAsync(null).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<PushExternalEntityAsync>d__582>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_168:
					configuredTaskAwaiter5.GetResult();
					IL_170:
					configuredTaskAwaiter7 = xmlTextReaderImpl.ParseXmlDeclarationAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter7.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter7;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<PushExternalEntityAsync>d__582>(ref configuredTaskAwaiter7, ref this);
						return;
					}
					IL_1D2:
					if (!configuredTaskAwaiter7.GetResult())
					{
						xmlTextReaderImpl.ps.charPos = initialPos;
					}
					result = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_24D:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D87 RID: 3463 RVA: 0x0004D4DC File Offset: 0x0004B6DC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008E5 RID: 2277
			public int <>1__state;

			// Token: 0x040008E6 RID: 2278
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040008E7 RID: 2279
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008E8 RID: 2280
			public IDtdEntityInfo entity;

			// Token: 0x040008E9 RID: 2281
			private int <initialPos>5__1;

			// Token: 0x040008EA RID: 2282
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040008EB RID: 2283
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x040008EC RID: 2284
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000176 RID: 374
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ZeroEndingStreamAsync>d__583 : IAsyncStateMachine
		{
			// Token: 0x06000D88 RID: 3464 RVA: 0x0004D4EC File Offset: 0x0004B6EC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					bool flag;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						flag = (xmlTextReaderImpl.v1Compat && pos == xmlTextReaderImpl.ps.charsUsed - 1 && xmlTextReaderImpl.ps.chars[pos] == '\0');
						if (!flag)
						{
							goto IL_B3;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ZeroEndingStreamAsync>d__583>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					flag = (configuredTaskAwaiter3.GetResult() == 0);
					IL_B3:
					if (flag && xmlTextReaderImpl.ps.isStreamEof)
					{
						XmlTextReaderImpl xmlTextReaderImpl2 = xmlTextReaderImpl;
						xmlTextReaderImpl2.ps.charsUsed = xmlTextReaderImpl2.ps.charsUsed - 1;
						result = true;
					}
					else
					{
						result = false;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D89 RID: 3465 RVA: 0x0004D614 File Offset: 0x0004B814
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008ED RID: 2285
			public int <>1__state;

			// Token: 0x040008EE RID: 2286
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040008EF RID: 2287
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008F0 RID: 2288
			public int pos;

			// Token: 0x040008F1 RID: 2289
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000177 RID: 375
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseDtdFromParserContextAsync>d__584 : IAsyncStateMachine
		{
			// Token: 0x06000D8A RID: 3466 RVA: 0x0004D624 File Offset: 0x0004B824
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				try
				{
					ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = DtdParser.Create().ParseFreeFloatingDtdAsync(xmlTextReaderImpl.fragmentParserContext.BaseURI, xmlTextReaderImpl.fragmentParserContext.DocTypeName, xmlTextReaderImpl.fragmentParserContext.PublicId, xmlTextReaderImpl.fragmentParserContext.SystemId, xmlTextReaderImpl.fragmentParserContext.InternalSubset, new XmlTextReaderImpl.DtdParserProxy(xmlTextReaderImpl)).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ParseDtdFromParserContextAsync>d__584>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IDtdInfo result = configuredTaskAwaiter.GetResult();
					xmlTextReaderImpl.dtdInfo = result;
					if ((xmlTextReaderImpl.validatingReaderCompatFlag || !xmlTextReaderImpl.v1Compat) && (xmlTextReaderImpl.dtdInfo.HasDefaultAttributes || xmlTextReaderImpl.dtdInfo.HasNonCDataAttributes))
					{
						xmlTextReaderImpl.addDefaultAttributesAndNormalize = true;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000D8B RID: 3467 RVA: 0x0004D764 File Offset: 0x0004B964
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008F2 RID: 2290
			public int <>1__state;

			// Token: 0x040008F3 RID: 2291
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040008F4 RID: 2292
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008F5 RID: 2293
			private ConfiguredTaskAwaitable<IDtdInfo>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000178 RID: 376
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InitReadContentAsBinaryAsync>d__585 : IAsyncStateMachine
		{
			// Token: 0x06000D8C RID: 3468 RVA: 0x0004D774 File Offset: 0x0004B974
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.InReadValueChunk)
						{
							throw new InvalidOperationException(Res.GetString("ReadValueChunk calls cannot be mixed with ReadContentAsBase64 or ReadContentAsBinHex."));
						}
						if (xmlTextReaderImpl.parsingFunction == XmlTextReaderImpl.ParsingFunction.InIncrementalRead)
						{
							throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadChars, ReadBase64, and ReadBinHex."));
						}
						if (XmlReader.IsTextualNode(xmlTextReaderImpl.curNode.type))
						{
							goto IL_C6;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.MoveToNextContentNodeAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<InitReadContentAsBinaryAsync>d__585>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = false;
						goto IL_10C;
					}
					IL_C6:
					xmlTextReaderImpl.SetupReadContentAsBinaryState(XmlTextReaderImpl.ParsingFunction.InReadContentAsBinary);
					xmlTextReaderImpl.incReadLineInfo.Set(xmlTextReaderImpl.curNode.LineNo, xmlTextReaderImpl.curNode.LinePos);
					result = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_10C:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D8D RID: 3469 RVA: 0x0004D8B4 File Offset: 0x0004BAB4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008F6 RID: 2294
			public int <>1__state;

			// Token: 0x040008F7 RID: 2295
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040008F8 RID: 2296
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008F9 RID: 2297
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000179 RID: 377
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InitReadElementContentAsBinaryAsync>d__586 : IAsyncStateMachine
		{
			// Token: 0x06000D8E RID: 3470 RVA: 0x0004D8C4 File Offset: 0x0004BAC4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_10A;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1A9;
					default:
						isEmpty = xmlTextReaderImpl.curNode.IsEmptyElement;
						configuredTaskAwaiter3 = xmlTextReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<InitReadElementContentAsBinaryAsync>d__586>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter3.GetResult();
					if (isEmpty)
					{
						result = false;
						goto IL_1FB;
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.MoveToNextContentNodeAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<InitReadElementContentAsBinaryAsync>d__586>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_10A:
					if (configuredTaskAwaiter3.GetResult())
					{
						xmlTextReaderImpl.SetupReadContentAsBinaryState(XmlTextReaderImpl.ParsingFunction.InReadElementContentAsBinary);
						xmlTextReaderImpl.incReadLineInfo.Set(xmlTextReaderImpl.curNode.LineNo, xmlTextReaderImpl.curNode.LinePos);
						result = true;
						goto IL_1FB;
					}
					if (xmlTextReaderImpl.curNode.type != XmlNodeType.EndElement)
					{
						xmlTextReaderImpl.Throw("'{0}' is an invalid XmlNodeType.", xmlTextReaderImpl.curNode.type.ToString());
					}
					configuredTaskAwaiter3 = xmlTextReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<InitReadElementContentAsBinaryAsync>d__586>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_1A9:
					configuredTaskAwaiter3.GetResult();
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_1FB:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D8F RID: 3471 RVA: 0x0004DAFC File Offset: 0x0004BCFC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008FA RID: 2298
			public int <>1__state;

			// Token: 0x040008FB RID: 2299
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040008FC RID: 2300
			public XmlTextReaderImpl <>4__this;

			// Token: 0x040008FD RID: 2301
			private bool <isEmpty>5__1;

			// Token: 0x040008FE RID: 2302
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200017A RID: 378
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <MoveToNextContentNodeAsync>d__587 : IAsyncStateMachine
		{
			// Token: 0x06000D90 RID: 3472 RVA: 0x0004DB0C File Offset: 0x0004BD0C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_102;
					}
					IL_14:
					switch (xmlTextReaderImpl.curNode.type)
					{
					case XmlNodeType.Attribute:
						result = !moveIfOnContentNode;
						goto IL_12B;
					case XmlNodeType.Text:
					case XmlNodeType.CDATA:
					case XmlNodeType.Whitespace:
					case XmlNodeType.SignificantWhitespace:
						if (!moveIfOnContentNode)
						{
							result = true;
							goto IL_12B;
						}
						goto IL_98;
					case XmlNodeType.EntityReference:
						xmlTextReaderImpl.outerReader.ResolveEntity();
						goto IL_98;
					case XmlNodeType.ProcessingInstruction:
					case XmlNodeType.Comment:
					case XmlNodeType.EndEntity:
						goto IL_98;
					}
					result = false;
					goto IL_12B;
					IL_98:
					moveIfOnContentNode = false;
					configuredTaskAwaiter3 = xmlTextReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<MoveToNextContentNodeAsync>d__587>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_102:
					if (configuredTaskAwaiter3.GetResult())
					{
						goto IL_14;
					}
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_12B:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D91 RID: 3473 RVA: 0x0004DC74 File Offset: 0x0004BE74
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040008FF RID: 2303
			public int <>1__state;

			// Token: 0x04000900 RID: 2304
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000901 RID: 2305
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000902 RID: 2306
			public bool moveIfOnContentNode;

			// Token: 0x04000903 RID: 2307
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200017B RID: 379
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBinaryAsync>d__588 : IAsyncStateMachine
		{
			// Token: 0x06000D92 RID: 3474 RVA: 0x0004DC84 File Offset: 0x0004BE84
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_194;
					}
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					if (num == 1)
					{
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_345;
					}
					if (xmlTextReaderImpl.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End)
					{
						result = 0;
						goto IL_3BB;
					}
					xmlTextReaderImpl.incReadDecoder.SetNextOutputBuffer(buffer, index, count);
					IL_49:
					charsRead = 0;
					try
					{
						charsRead = xmlTextReaderImpl.curNode.CopyToBinary(xmlTextReaderImpl.incReadDecoder, xmlTextReaderImpl.readValueOffset);
					}
					catch (XmlException e)
					{
						xmlTextReaderImpl.curNode.AdjustLineInfo(xmlTextReaderImpl.readValueOffset, xmlTextReaderImpl.ps.eolNormalized, ref xmlTextReaderImpl.incReadLineInfo);
						xmlTextReaderImpl.ReThrow(e, xmlTextReaderImpl.incReadLineInfo.lineNo, xmlTextReaderImpl.incReadLineInfo.linePos);
					}
					xmlTextReaderImpl.readValueOffset += charsRead;
					if (xmlTextReaderImpl.incReadDecoder.IsFull)
					{
						result = xmlTextReaderImpl.incReadDecoder.DecodedCount;
						goto IL_3BB;
					}
					bool flag;
					int num3;
					int num4;
					if (xmlTextReaderImpl.incReadState == XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue)
					{
						xmlTextReaderImpl.curNode.SetValue(string.Empty);
						flag = false;
						num3 = 0;
						num4 = 0;
						goto IL_20C;
					}
					goto IL_2BF;
					IL_194:
					Tuple<int, int, int, bool> result2 = configuredTaskAwaiter3.GetResult();
					num3 = result2.Item1;
					num4 = result2.Item2;
					int outOrChars = result2.Item3;
					flag = result2.Item4;
					try
					{
						charsRead = xmlTextReaderImpl.incReadDecoder.Decode(xmlTextReaderImpl.ps.chars, num3, num4 - num3);
					}
					catch (XmlException e2)
					{
						xmlTextReaderImpl.ReThrow(e2, xmlTextReaderImpl.incReadLineInfo.lineNo, xmlTextReaderImpl.incReadLineInfo.linePos);
					}
					num3 += charsRead;
					IL_20C:
					if (xmlTextReaderImpl.incReadDecoder.IsFull || flag)
					{
						xmlTextReaderImpl.incReadState = (flag ? XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnCachedValue : XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_OnPartialValue);
						xmlTextReaderImpl.readValueOffset = 0;
						if (xmlTextReaderImpl.incReadDecoder.IsFull)
						{
							xmlTextReaderImpl.curNode.SetValue(xmlTextReaderImpl.ps.chars, num3, num4 - num3);
							XmlTextReaderImpl.AdjustLineInfo(xmlTextReaderImpl.ps.chars, num3 - charsRead, num3, xmlTextReaderImpl.ps.eolNormalized, ref xmlTextReaderImpl.incReadLineInfo);
							xmlTextReaderImpl.curNode.SetLineInfo(xmlTextReaderImpl.incReadLineInfo.lineNo, xmlTextReaderImpl.incReadLineInfo.linePos);
							result = xmlTextReaderImpl.incReadDecoder.DecodedCount;
							goto IL_3BB;
						}
					}
					else
					{
						outOrChars = 0;
						xmlTextReaderImpl.incReadLineInfo.Set(xmlTextReaderImpl.ps.LineNo, xmlTextReaderImpl.ps.LinePos);
						configuredTaskAwaiter3 = xmlTextReaderImpl.ParseTextAsync(outOrChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadContentAsBinaryAsync>d__588>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_194;
					}
					IL_2BF:
					tmp = xmlTextReaderImpl.parsingFunction;
					xmlTextReaderImpl.parsingFunction = xmlTextReaderImpl.nextParsingFunction;
					xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.nextNextParsingFunction;
					configuredTaskAwaiter5 = xmlTextReaderImpl.MoveToNextContentNodeAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadContentAsBinaryAsync>d__588>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_345:
					if (configuredTaskAwaiter5.GetResult())
					{
						xmlTextReaderImpl.SetupReadContentAsBinaryState(tmp);
						xmlTextReaderImpl.incReadLineInfo.Set(xmlTextReaderImpl.curNode.LineNo, xmlTextReaderImpl.curNode.LinePos);
						goto IL_49;
					}
					xmlTextReaderImpl.SetupReadContentAsBinaryState(tmp);
					xmlTextReaderImpl.incReadState = XmlTextReaderImpl.IncrementalReadState.ReadContentAsBinary_End;
					result = xmlTextReaderImpl.incReadDecoder.DecodedCount;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_3BB:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D93 RID: 3475 RVA: 0x0004E0AC File Offset: 0x0004C2AC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000904 RID: 2308
			public int <>1__state;

			// Token: 0x04000905 RID: 2309
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000906 RID: 2310
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000907 RID: 2311
			public byte[] buffer;

			// Token: 0x04000908 RID: 2312
			public int index;

			// Token: 0x04000909 RID: 2313
			public int count;

			// Token: 0x0400090A RID: 2314
			private int <charsRead>5__1;

			// Token: 0x0400090B RID: 2315
			private XmlTextReaderImpl.ParsingFunction <tmp>5__2;

			// Token: 0x0400090C RID: 2316
			private ConfiguredTaskAwaitable<Tuple<int, int, int, bool>>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400090D RID: 2317
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200017C RID: 380
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBinaryAsync>d__589 : IAsyncStateMachine
		{
			// Token: 0x06000D94 RID: 3476 RVA: 0x0004E0BC File Offset: 0x0004C2BC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlTextReaderImpl xmlTextReaderImpl = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_159;
						}
						if (count == 0)
						{
							result = 0;
							goto IL_17E;
						}
						configuredTaskAwaiter3 = xmlTextReaderImpl.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadElementContentAsBinaryAsync>d__589>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter3.GetResult();
					if (result2 > 0)
					{
						result = result2;
						goto IL_17E;
					}
					if (xmlTextReaderImpl.curNode.type != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", xmlTextReaderImpl.curNode.type.ToString(), xmlTextReaderImpl);
					}
					xmlTextReaderImpl.parsingFunction = xmlTextReaderImpl.nextParsingFunction;
					xmlTextReaderImpl.nextParsingFunction = xmlTextReaderImpl.nextNextParsingFunction;
					configuredTaskAwaiter = xmlTextReaderImpl.outerReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlTextReaderImpl.<ReadElementContentAsBinaryAsync>d__589>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_159:
					configuredTaskAwaiter.GetResult();
					result = 0;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_17E:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000D95 RID: 3477 RVA: 0x0004E278 File Offset: 0x0004C478
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400090E RID: 2318
			public int <>1__state;

			// Token: 0x0400090F RID: 2319
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000910 RID: 2320
			public int count;

			// Token: 0x04000911 RID: 2321
			public XmlTextReaderImpl <>4__this;

			// Token: 0x04000912 RID: 2322
			public byte[] buffer;

			// Token: 0x04000913 RID: 2323
			public int index;

			// Token: 0x04000914 RID: 2324
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000915 RID: 2325
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}
	}
}
