﻿using System;

namespace System.Xml
{
	// Token: 0x020000A6 RID: 166
	// (Invoke) Token: 0x06000571 RID: 1393
	internal delegate void OnRemoveWriter(XmlRawWriter writer);
}
