﻿using System;
using System.ComponentModel;
using System.IO;

namespace System.Xml
{
	/// <summary>Represents an application resource stream resolver.</summary>
	// Token: 0x02000247 RID: 583
	[Obsolete("This API supports the .NET Framework infrastructure and is not intended to be used directly from your code.", true)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public interface IApplicationResourceStreamResolver
	{
		/// <summary>Returns an application resource stream from the specified URI.</summary>
		/// <param name="relativeUri">The relative URI.</param>
		/// <returns>An application resource stream.</returns>
		// Token: 0x06001680 RID: 5760
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("This API supports the .NET Framework infrastructure and is not intended to be used directly from your code.", true)]
		Stream GetApplicationResourceStream(Uri relativeUri);
	}
}
