﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml.Schema;
using System.Xml.XmlConfiguration;

namespace System.Xml
{
	/// <summary>Specifies a set of features to support on the <see cref="T:System.Xml.XmlReader" /> object created by the <see cref="Overload:System.Xml.XmlReader.Create" /> method. </summary>
	// Token: 0x02000110 RID: 272
	public sealed class XmlReaderSettings
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlReaderSettings" /> class.</summary>
		// Token: 0x060009F2 RID: 2546 RVA: 0x0002CE4A File Offset: 0x0002B04A
		public XmlReaderSettings()
		{
			this.Initialize();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlReaderSettings" /> class.</summary>
		/// <param name="resolver">The XML resolver.</param>
		// Token: 0x060009F3 RID: 2547 RVA: 0x0002CE58 File Offset: 0x0002B058
		[Obsolete("This API supports the .NET Framework infrastructure and is not intended to be used directly from your code.", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public XmlReaderSettings(XmlResolver resolver)
		{
			this.Initialize(resolver);
		}

		/// <summary>Gets or sets whether asynchronous <see cref="T:System.Xml.XmlReader" /> methods can be used on a particular <see cref="T:System.Xml.XmlReader" /> instance.</summary>
		/// <returns>
		///     <see langword="true" /> if asynchronous methods can be used; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001AE RID: 430
		// (get) Token: 0x060009F4 RID: 2548 RVA: 0x0002CE67 File Offset: 0x0002B067
		// (set) Token: 0x060009F5 RID: 2549 RVA: 0x0002CE6F File Offset: 0x0002B06F
		public bool Async
		{
			get
			{
				return this.useAsync;
			}
			set
			{
				this.CheckReadOnly("Async");
				this.useAsync = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.XmlNameTable" /> used for atomized string comparisons.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlNameTable" /> that stores all the atomized strings used by all <see cref="T:System.Xml.XmlReader" /> instances created using this <see cref="T:System.Xml.XmlReaderSettings" /> object.The default is <see langword="null" />. The created <see cref="T:System.Xml.XmlReader" /> instance will use a new empty <see cref="T:System.Xml.NameTable" /> if this value is <see langword="null" />.</returns>
		// Token: 0x170001AF RID: 431
		// (get) Token: 0x060009F6 RID: 2550 RVA: 0x0002CE83 File Offset: 0x0002B083
		// (set) Token: 0x060009F7 RID: 2551 RVA: 0x0002CE8B File Offset: 0x0002B08B
		public XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
			set
			{
				this.CheckReadOnly("NameTable");
				this.nameTable = value;
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x060009F8 RID: 2552 RVA: 0x0002CE9F File Offset: 0x0002B09F
		// (set) Token: 0x060009F9 RID: 2553 RVA: 0x0002CEA7 File Offset: 0x0002B0A7
		internal bool IsXmlResolverSet
		{
			[CompilerGenerated]
			get
			{
				return this.<IsXmlResolverSet>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsXmlResolverSet>k__BackingField = value;
			}
		}

		/// <summary>Sets the <see cref="T:System.Xml.XmlResolver" /> used to access external documents.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlResolver" /> used to access external documents. If set to <see langword="null" />, an <see cref="T:System.Xml.XmlException" /> is thrown when the <see cref="T:System.Xml.XmlReader" /> tries to access an external resource. The default is a new <see cref="T:System.Xml.XmlUrlResolver" /> with no credentials.  Starting with the .NET Framework 4.5.2, this setting has a default value of <see langword="null" />.</returns>
		// Token: 0x170001B1 RID: 433
		// (set) Token: 0x060009FA RID: 2554 RVA: 0x0002CEB0 File Offset: 0x0002B0B0
		public XmlResolver XmlResolver
		{
			set
			{
				this.CheckReadOnly("XmlResolver");
				this.xmlResolver = value;
				this.IsXmlResolverSet = true;
			}
		}

		// Token: 0x060009FB RID: 2555 RVA: 0x0002CECB File Offset: 0x0002B0CB
		internal XmlResolver GetXmlResolver()
		{
			return this.xmlResolver;
		}

		// Token: 0x060009FC RID: 2556 RVA: 0x0002CED3 File Offset: 0x0002B0D3
		internal XmlResolver GetXmlResolver_CheckConfig()
		{
			if (XmlReaderSection.ProhibitDefaultUrlResolver && !this.IsXmlResolverSet)
			{
				return null;
			}
			return this.xmlResolver;
		}

		/// <summary>Gets or sets line number offset of the <see cref="T:System.Xml.XmlReader" /> object.</summary>
		/// <returns>The line number offset. The default is 0.</returns>
		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x060009FD RID: 2557 RVA: 0x0002CEEC File Offset: 0x0002B0EC
		// (set) Token: 0x060009FE RID: 2558 RVA: 0x0002CEF4 File Offset: 0x0002B0F4
		public int LineNumberOffset
		{
			get
			{
				return this.lineNumberOffset;
			}
			set
			{
				this.CheckReadOnly("LineNumberOffset");
				this.lineNumberOffset = value;
			}
		}

		/// <summary>Gets or sets line position offset of the <see cref="T:System.Xml.XmlReader" /> object.</summary>
		/// <returns>The line position offset. The default is 0.</returns>
		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x060009FF RID: 2559 RVA: 0x0002CF08 File Offset: 0x0002B108
		// (set) Token: 0x06000A00 RID: 2560 RVA: 0x0002CF10 File Offset: 0x0002B110
		public int LinePositionOffset
		{
			get
			{
				return this.linePositionOffset;
			}
			set
			{
				this.CheckReadOnly("LinePositionOffset");
				this.linePositionOffset = value;
			}
		}

		/// <summary>Gets or sets the level of conformance which the <see cref="T:System.Xml.XmlReader" /> will comply.</summary>
		/// <returns>One of the enumeration values that specifies the level of conformance that the XML reader will enforce. The default is <see cref="F:System.Xml.ConformanceLevel.Document" />.</returns>
		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06000A01 RID: 2561 RVA: 0x0002CF24 File Offset: 0x0002B124
		// (set) Token: 0x06000A02 RID: 2562 RVA: 0x0002CF2C File Offset: 0x0002B12C
		public ConformanceLevel ConformanceLevel
		{
			get
			{
				return this.conformanceLevel;
			}
			set
			{
				this.CheckReadOnly("ConformanceLevel");
				if (value > ConformanceLevel.Document)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.conformanceLevel = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to do character checking.</summary>
		/// <returns>
		///     <see langword="true" /> to do character checking; otherwise <see langword="false" />. The default is <see langword="true" />.If the <see cref="T:System.Xml.XmlReader" /> is processing text data, it always checks that the XML names and text content are valid, regardless of the property setting. Setting <see cref="P:System.Xml.XmlReaderSettings.CheckCharacters" /> to <see langword="false" /> turns off character checking for character entity references.</returns>
		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000A03 RID: 2563 RVA: 0x0002CF4F File Offset: 0x0002B14F
		// (set) Token: 0x06000A04 RID: 2564 RVA: 0x0002CF57 File Offset: 0x0002B157
		public bool CheckCharacters
		{
			get
			{
				return this.checkCharacters;
			}
			set
			{
				this.CheckReadOnly("CheckCharacters");
				this.checkCharacters = value;
			}
		}

		/// <summary>Gets or sets a value indicating the maximum allowable number of characters in an XML document. A zero (0) value means no limits on the size of the XML document. A non-zero value specifies the maximum size, in characters.</summary>
		/// <returns>The maximum allowable number of characters in an XML document. The default is 0.</returns>
		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000A05 RID: 2565 RVA: 0x0002CF6B File Offset: 0x0002B16B
		// (set) Token: 0x06000A06 RID: 2566 RVA: 0x0002CF73 File Offset: 0x0002B173
		public long MaxCharactersInDocument
		{
			get
			{
				return this.maxCharactersInDocument;
			}
			set
			{
				this.CheckReadOnly("MaxCharactersInDocument");
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.maxCharactersInDocument = value;
			}
		}

		/// <summary>Gets or sets a value indicating the maximum allowable number of characters in a document that result from expanding entities.</summary>
		/// <returns>The maximum allowable number of characters from expanded entities. The default is 0.</returns>
		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000A07 RID: 2567 RVA: 0x0002CF97 File Offset: 0x0002B197
		// (set) Token: 0x06000A08 RID: 2568 RVA: 0x0002CF9F File Offset: 0x0002B19F
		public long MaxCharactersFromEntities
		{
			get
			{
				return this.maxCharactersFromEntities;
			}
			set
			{
				this.CheckReadOnly("MaxCharactersFromEntities");
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.maxCharactersFromEntities = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to ignore insignificant white space.</summary>
		/// <returns>
		///     <see langword="true" /> to ignore white space; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06000A09 RID: 2569 RVA: 0x0002CFC3 File Offset: 0x0002B1C3
		// (set) Token: 0x06000A0A RID: 2570 RVA: 0x0002CFCB File Offset: 0x0002B1CB
		public bool IgnoreWhitespace
		{
			get
			{
				return this.ignoreWhitespace;
			}
			set
			{
				this.CheckReadOnly("IgnoreWhitespace");
				this.ignoreWhitespace = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to ignore processing instructions.</summary>
		/// <returns>
		///     <see langword="true" /> to ignore processing instructions; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x06000A0B RID: 2571 RVA: 0x0002CFDF File Offset: 0x0002B1DF
		// (set) Token: 0x06000A0C RID: 2572 RVA: 0x0002CFE7 File Offset: 0x0002B1E7
		public bool IgnoreProcessingInstructions
		{
			get
			{
				return this.ignorePIs;
			}
			set
			{
				this.CheckReadOnly("IgnoreProcessingInstructions");
				this.ignorePIs = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to ignore comments.</summary>
		/// <returns>
		///     <see langword="true" /> to ignore comments; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001BA RID: 442
		// (get) Token: 0x06000A0D RID: 2573 RVA: 0x0002CFFB File Offset: 0x0002B1FB
		// (set) Token: 0x06000A0E RID: 2574 RVA: 0x0002D003 File Offset: 0x0002B203
		public bool IgnoreComments
		{
			get
			{
				return this.ignoreComments;
			}
			set
			{
				this.CheckReadOnly("IgnoreComments");
				this.ignoreComments = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to prohibit document type definition (DTD) processing. This property is obsolete. Use <see cref="P:System.Xml.XmlTextReader.DtdProcessing" /> instead.</summary>
		/// <returns>
		///     <see langword="true" /> to prohibit DTD processing; otherwise <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x170001BB RID: 443
		// (get) Token: 0x06000A0F RID: 2575 RVA: 0x0002D017 File Offset: 0x0002B217
		// (set) Token: 0x06000A10 RID: 2576 RVA: 0x0002D022 File Offset: 0x0002B222
		[Obsolete("Use XmlReaderSettings.DtdProcessing property instead.")]
		public bool ProhibitDtd
		{
			get
			{
				return this.dtdProcessing == DtdProcessing.Prohibit;
			}
			set
			{
				this.CheckReadOnly("ProhibitDtd");
				this.dtdProcessing = (value ? DtdProcessing.Prohibit : DtdProcessing.Parse);
			}
		}

		/// <summary>Gets or sets a value that determines the processing of DTDs.</summary>
		/// <returns>One of the enumeration values that determines the processing of DTDs. The default is <see cref="F:System.Xml.DtdProcessing.Prohibit" />.</returns>
		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06000A11 RID: 2577 RVA: 0x0002D03C File Offset: 0x0002B23C
		// (set) Token: 0x06000A12 RID: 2578 RVA: 0x0002D044 File Offset: 0x0002B244
		public DtdProcessing DtdProcessing
		{
			get
			{
				return this.dtdProcessing;
			}
			set
			{
				this.CheckReadOnly("DtdProcessing");
				if (value > DtdProcessing.Parse)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.dtdProcessing = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the underlying stream or <see cref="T:System.IO.TextReader" /> should be closed when the reader is closed.</summary>
		/// <returns>
		///     <see langword="true" /> to close the underlying stream or <see cref="T:System.IO.TextReader" /> when the reader is closed; otherwise <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06000A13 RID: 2579 RVA: 0x0002D067 File Offset: 0x0002B267
		// (set) Token: 0x06000A14 RID: 2580 RVA: 0x0002D06F File Offset: 0x0002B26F
		public bool CloseInput
		{
			get
			{
				return this.closeInput;
			}
			set
			{
				this.CheckReadOnly("CloseInput");
				this.closeInput = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the <see cref="T:System.Xml.XmlReader" /> will perform validation or type assignment when reading.</summary>
		/// <returns>One of the <see cref="T:System.Xml.ValidationType" /> values that indicates whether XmlReader will perform validation or type assignment when reading. The default is <see langword="ValidationType.None" />.</returns>
		// Token: 0x170001BE RID: 446
		// (get) Token: 0x06000A15 RID: 2581 RVA: 0x0002D083 File Offset: 0x0002B283
		// (set) Token: 0x06000A16 RID: 2582 RVA: 0x0002D08B File Offset: 0x0002B28B
		public ValidationType ValidationType
		{
			get
			{
				return this.validationType;
			}
			set
			{
				this.CheckReadOnly("ValidationType");
				if (value > ValidationType.Schema)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.validationType = value;
			}
		}

		/// <summary>Gets or sets a value indicating the schema validation settings. This setting applies to <see cref="T:System.Xml.XmlReader" /> objects that validate schemas (<see cref="P:System.Xml.XmlReaderSettings.ValidationType" /> property set to <see langword="ValidationType.Schema" />).</summary>
		/// <returns>A bitwise combination of enumeration values that specify validation options. <see cref="F:System.Xml.Schema.XmlSchemaValidationFlags.ProcessIdentityConstraints" /> and <see cref="F:System.Xml.Schema.XmlSchemaValidationFlags.AllowXmlAttributes" /> are enabled by default. <see cref="F:System.Xml.Schema.XmlSchemaValidationFlags.ProcessInlineSchema" />, <see cref="F:System.Xml.Schema.XmlSchemaValidationFlags.ProcessSchemaLocation" />, and <see cref="F:System.Xml.Schema.XmlSchemaValidationFlags.ReportValidationWarnings" /> are disabled by default.</returns>
		// Token: 0x170001BF RID: 447
		// (get) Token: 0x06000A17 RID: 2583 RVA: 0x0002D0AE File Offset: 0x0002B2AE
		// (set) Token: 0x06000A18 RID: 2584 RVA: 0x0002D0B6 File Offset: 0x0002B2B6
		public XmlSchemaValidationFlags ValidationFlags
		{
			get
			{
				return this.validationFlags;
			}
			set
			{
				this.CheckReadOnly("ValidationFlags");
				if (value > (XmlSchemaValidationFlags.ProcessInlineSchema | XmlSchemaValidationFlags.ProcessSchemaLocation | XmlSchemaValidationFlags.ReportValidationWarnings | XmlSchemaValidationFlags.ProcessIdentityConstraints | XmlSchemaValidationFlags.AllowXmlAttributes))
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.validationFlags = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaSet" /> to use when performing schema validation.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaSet" /> to use when performing schema validation. The default is an empty <see cref="T:System.Xml.Schema.XmlSchemaSet" /> object.</returns>
		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x06000A19 RID: 2585 RVA: 0x0002D0DA File Offset: 0x0002B2DA
		// (set) Token: 0x06000A1A RID: 2586 RVA: 0x0002D0F5 File Offset: 0x0002B2F5
		public XmlSchemaSet Schemas
		{
			get
			{
				if (this.schemas == null)
				{
					this.schemas = new XmlSchemaSet();
				}
				return this.schemas;
			}
			set
			{
				this.CheckReadOnly("Schemas");
				this.schemas = value;
			}
		}

		/// <summary>Occurs when the reader encounters validation errors.</summary>
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000A1B RID: 2587 RVA: 0x0002D109 File Offset: 0x0002B309
		// (remove) Token: 0x06000A1C RID: 2588 RVA: 0x0002D12D File Offset: 0x0002B32D
		public event ValidationEventHandler ValidationEventHandler
		{
			add
			{
				this.CheckReadOnly("ValidationEventHandler");
				this.valEventHandler = (ValidationEventHandler)Delegate.Combine(this.valEventHandler, value);
			}
			remove
			{
				this.CheckReadOnly("ValidationEventHandler");
				this.valEventHandler = (ValidationEventHandler)Delegate.Remove(this.valEventHandler, value);
			}
		}

		/// <summary>Resets the members of the settings class to their default values.</summary>
		// Token: 0x06000A1D RID: 2589 RVA: 0x0002D151 File Offset: 0x0002B351
		public void Reset()
		{
			this.CheckReadOnly("Reset");
			this.Initialize();
		}

		/// <summary>Creates a copy of the <see cref="T:System.Xml.XmlReaderSettings" /> instance.</summary>
		/// <returns>The cloned <see cref="T:System.Xml.XmlReaderSettings" /> object.</returns>
		// Token: 0x06000A1E RID: 2590 RVA: 0x0002D164 File Offset: 0x0002B364
		public XmlReaderSettings Clone()
		{
			XmlReaderSettings xmlReaderSettings = base.MemberwiseClone() as XmlReaderSettings;
			xmlReaderSettings.ReadOnly = false;
			return xmlReaderSettings;
		}

		// Token: 0x06000A1F RID: 2591 RVA: 0x0002D178 File Offset: 0x0002B378
		internal ValidationEventHandler GetEventHandler()
		{
			return this.valEventHandler;
		}

		// Token: 0x06000A20 RID: 2592 RVA: 0x0002D180 File Offset: 0x0002B380
		internal XmlReader CreateReader(string inputUri, XmlParserContext inputContext)
		{
			if (inputUri == null)
			{
				throw new ArgumentNullException("inputUri");
			}
			if (inputUri.Length == 0)
			{
				throw new ArgumentException(Res.GetString("The string was not recognized as a valid Uri."), "inputUri");
			}
			XmlResolver xmlResolver = this.GetXmlResolver();
			if (xmlResolver == null)
			{
				xmlResolver = XmlReaderSettings.CreateDefaultResolver();
			}
			XmlReader xmlReader = new XmlTextReaderImpl(inputUri, this, inputContext, xmlResolver);
			if (this.ValidationType != ValidationType.None)
			{
				xmlReader = this.AddValidation(xmlReader);
			}
			if (this.useAsync)
			{
				xmlReader = XmlAsyncCheckReader.CreateAsyncCheckWrapper(xmlReader);
			}
			return xmlReader;
		}

		// Token: 0x06000A21 RID: 2593 RVA: 0x0002D1F4 File Offset: 0x0002B3F4
		internal XmlReader CreateReader(Stream input, Uri baseUri, string baseUriString, XmlParserContext inputContext)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			if (baseUriString == null)
			{
				if (baseUri == null)
				{
					baseUriString = string.Empty;
				}
				else
				{
					baseUriString = baseUri.ToString();
				}
			}
			XmlReader xmlReader = new XmlTextReaderImpl(input, null, 0, this, baseUri, baseUriString, inputContext, this.closeInput);
			if (this.ValidationType != ValidationType.None)
			{
				xmlReader = this.AddValidation(xmlReader);
			}
			if (this.useAsync)
			{
				xmlReader = XmlAsyncCheckReader.CreateAsyncCheckWrapper(xmlReader);
			}
			return xmlReader;
		}

		// Token: 0x06000A22 RID: 2594 RVA: 0x0002D260 File Offset: 0x0002B460
		internal XmlReader CreateReader(TextReader input, string baseUriString, XmlParserContext inputContext)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			if (baseUriString == null)
			{
				baseUriString = string.Empty;
			}
			XmlReader xmlReader = new XmlTextReaderImpl(input, this, baseUriString, inputContext);
			if (this.ValidationType != ValidationType.None)
			{
				xmlReader = this.AddValidation(xmlReader);
			}
			if (this.useAsync)
			{
				xmlReader = XmlAsyncCheckReader.CreateAsyncCheckWrapper(xmlReader);
			}
			return xmlReader;
		}

		// Token: 0x06000A23 RID: 2595 RVA: 0x0002D2AF File Offset: 0x0002B4AF
		internal XmlReader CreateReader(XmlReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			return this.AddValidationAndConformanceWrapper(reader);
		}

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x06000A24 RID: 2596 RVA: 0x0002D2C6 File Offset: 0x0002B4C6
		// (set) Token: 0x06000A25 RID: 2597 RVA: 0x0002D2CE File Offset: 0x0002B4CE
		internal bool ReadOnly
		{
			get
			{
				return this.isReadOnly;
			}
			set
			{
				this.isReadOnly = value;
			}
		}

		// Token: 0x06000A26 RID: 2598 RVA: 0x0002D2D7 File Offset: 0x0002B4D7
		private void CheckReadOnly(string propertyName)
		{
			if (this.isReadOnly)
			{
				throw new XmlException("The '{0}' property is read only and cannot be set.", base.GetType().Name + "." + propertyName);
			}
		}

		// Token: 0x06000A27 RID: 2599 RVA: 0x0002D302 File Offset: 0x0002B502
		private void Initialize()
		{
			this.Initialize(null);
		}

		// Token: 0x06000A28 RID: 2600 RVA: 0x0002D30C File Offset: 0x0002B50C
		private void Initialize(XmlResolver resolver)
		{
			this.nameTable = null;
			this.xmlResolver = ((resolver == null) ? XmlReaderSettings.CreateDefaultResolver() : resolver);
			this.maxCharactersFromEntities = 0L;
			this.lineNumberOffset = 0;
			this.linePositionOffset = 0;
			this.checkCharacters = true;
			this.conformanceLevel = ConformanceLevel.Document;
			this.ignoreWhitespace = false;
			this.ignorePIs = false;
			this.ignoreComments = false;
			this.dtdProcessing = DtdProcessing.Prohibit;
			this.closeInput = false;
			this.maxCharactersInDocument = 0L;
			this.schemas = null;
			this.validationType = ValidationType.None;
			this.validationFlags = XmlSchemaValidationFlags.ProcessIdentityConstraints;
			this.validationFlags |= XmlSchemaValidationFlags.AllowXmlAttributes;
			this.useAsync = false;
			this.isReadOnly = false;
			this.IsXmlResolverSet = false;
		}

		// Token: 0x06000A29 RID: 2601 RVA: 0x0002D3B9 File Offset: 0x0002B5B9
		private static XmlResolver CreateDefaultResolver()
		{
			return new XmlUrlResolver();
		}

		// Token: 0x06000A2A RID: 2602 RVA: 0x0002D3C0 File Offset: 0x0002B5C0
		internal XmlReader AddValidation(XmlReader reader)
		{
			if (this.validationType == ValidationType.Schema)
			{
				XmlResolver xmlResolver = this.GetXmlResolver_CheckConfig();
				if (xmlResolver == null && !this.IsXmlResolverSet && !XmlReaderSettings.EnableLegacyXmlSettings())
				{
					xmlResolver = new XmlUrlResolver();
				}
				reader = new XsdValidatingReader(reader, xmlResolver, this);
			}
			else if (this.validationType == ValidationType.DTD)
			{
				reader = this.CreateDtdValidatingReader(reader);
			}
			return reader;
		}

		// Token: 0x06000A2B RID: 2603 RVA: 0x0002D414 File Offset: 0x0002B614
		private XmlReader AddValidationAndConformanceWrapper(XmlReader reader)
		{
			if (this.validationType == ValidationType.DTD)
			{
				reader = this.CreateDtdValidatingReader(reader);
			}
			reader = this.AddConformanceWrapper(reader);
			if (this.validationType == ValidationType.Schema)
			{
				reader = new XsdValidatingReader(reader, this.GetXmlResolver_CheckConfig(), this);
			}
			return reader;
		}

		// Token: 0x06000A2C RID: 2604 RVA: 0x0002D44A File Offset: 0x0002B64A
		private XmlValidatingReaderImpl CreateDtdValidatingReader(XmlReader baseReader)
		{
			return new XmlValidatingReaderImpl(baseReader, this.GetEventHandler(), (this.ValidationFlags & XmlSchemaValidationFlags.ProcessIdentityConstraints) > XmlSchemaValidationFlags.None);
		}

		// Token: 0x06000A2D RID: 2605 RVA: 0x0002D464 File Offset: 0x0002B664
		internal XmlReader AddConformanceWrapper(XmlReader baseReader)
		{
			XmlReaderSettings settings = baseReader.Settings;
			bool flag = false;
			bool flag2 = false;
			bool flag3 = false;
			bool ignorePis = false;
			DtdProcessing dtdProcessing = (DtdProcessing)(-1);
			bool flag4 = false;
			if (settings == null)
			{
				if (this.conformanceLevel != ConformanceLevel.Auto && this.conformanceLevel != XmlReader.GetV1ConformanceLevel(baseReader))
				{
					throw new InvalidOperationException(Res.GetString("Cannot change conformance checking to {0}. Make sure the ConformanceLevel in XmlReaderSettings is set to Auto for wrapping scenarios.", new object[]
					{
						this.conformanceLevel.ToString()
					}));
				}
				XmlTextReader xmlTextReader = baseReader as XmlTextReader;
				if (xmlTextReader == null)
				{
					XmlValidatingReader xmlValidatingReader = baseReader as XmlValidatingReader;
					if (xmlValidatingReader != null)
					{
						xmlTextReader = (XmlTextReader)xmlValidatingReader.Reader;
					}
				}
				if (this.ignoreWhitespace)
				{
					WhitespaceHandling whitespaceHandling = WhitespaceHandling.All;
					if (xmlTextReader != null)
					{
						whitespaceHandling = xmlTextReader.WhitespaceHandling;
					}
					if (whitespaceHandling == WhitespaceHandling.All)
					{
						flag2 = true;
						flag4 = true;
					}
				}
				if (this.ignoreComments)
				{
					flag3 = true;
					flag4 = true;
				}
				if (this.ignorePIs)
				{
					ignorePis = true;
					flag4 = true;
				}
				DtdProcessing dtdProcessing2 = DtdProcessing.Parse;
				if (xmlTextReader != null)
				{
					dtdProcessing2 = xmlTextReader.DtdProcessing;
				}
				if ((this.dtdProcessing == DtdProcessing.Prohibit && dtdProcessing2 != DtdProcessing.Prohibit) || (this.dtdProcessing == DtdProcessing.Ignore && dtdProcessing2 == DtdProcessing.Parse))
				{
					dtdProcessing = this.dtdProcessing;
					flag4 = true;
				}
			}
			else
			{
				if (this.conformanceLevel != settings.ConformanceLevel && this.conformanceLevel != ConformanceLevel.Auto)
				{
					throw new InvalidOperationException(Res.GetString("Cannot change conformance checking to {0}. Make sure the ConformanceLevel in XmlReaderSettings is set to Auto for wrapping scenarios.", new object[]
					{
						this.conformanceLevel.ToString()
					}));
				}
				if (this.checkCharacters && !settings.CheckCharacters)
				{
					flag = true;
					flag4 = true;
				}
				if (this.ignoreWhitespace && !settings.IgnoreWhitespace)
				{
					flag2 = true;
					flag4 = true;
				}
				if (this.ignoreComments && !settings.IgnoreComments)
				{
					flag3 = true;
					flag4 = true;
				}
				if (this.ignorePIs && !settings.IgnoreProcessingInstructions)
				{
					ignorePis = true;
					flag4 = true;
				}
				if ((this.dtdProcessing == DtdProcessing.Prohibit && settings.DtdProcessing != DtdProcessing.Prohibit) || (this.dtdProcessing == DtdProcessing.Ignore && settings.DtdProcessing == DtdProcessing.Parse))
				{
					dtdProcessing = this.dtdProcessing;
					flag4 = true;
				}
			}
			if (!flag4)
			{
				return baseReader;
			}
			IXmlNamespaceResolver xmlNamespaceResolver = baseReader as IXmlNamespaceResolver;
			if (xmlNamespaceResolver != null)
			{
				return new XmlCharCheckingReaderWithNS(baseReader, xmlNamespaceResolver, flag, flag2, flag3, ignorePis, dtdProcessing);
			}
			return new XmlCharCheckingReader(baseReader, flag, flag2, flag3, ignorePis, dtdProcessing);
		}

		// Token: 0x06000A2E RID: 2606 RVA: 0x0002D660 File Offset: 0x0002B860
		internal static bool EnableLegacyXmlSettings()
		{
			if (XmlReaderSettings.s_enableLegacyXmlSettings != null)
			{
				return XmlReaderSettings.s_enableLegacyXmlSettings.Value;
			}
			if (!BinaryCompatibility.TargetsAtLeast_Desktop_V4_5_2)
			{
				XmlReaderSettings.s_enableLegacyXmlSettings = new bool?(true);
				return XmlReaderSettings.s_enableLegacyXmlSettings.Value;
			}
			bool value = false;
			XmlReaderSettings.s_enableLegacyXmlSettings = new bool?(value);
			return XmlReaderSettings.s_enableLegacyXmlSettings.Value;
		}

		// Token: 0x06000A2F RID: 2607 RVA: 0x000030EC File Offset: 0x000012EC
		// Note: this type is marked as 'beforefieldinit'.
		static XmlReaderSettings()
		{
		}

		// Token: 0x040005BB RID: 1467
		private bool useAsync;

		// Token: 0x040005BC RID: 1468
		private XmlNameTable nameTable;

		// Token: 0x040005BD RID: 1469
		private XmlResolver xmlResolver;

		// Token: 0x040005BE RID: 1470
		private int lineNumberOffset;

		// Token: 0x040005BF RID: 1471
		private int linePositionOffset;

		// Token: 0x040005C0 RID: 1472
		private ConformanceLevel conformanceLevel;

		// Token: 0x040005C1 RID: 1473
		private bool checkCharacters;

		// Token: 0x040005C2 RID: 1474
		private long maxCharactersInDocument;

		// Token: 0x040005C3 RID: 1475
		private long maxCharactersFromEntities;

		// Token: 0x040005C4 RID: 1476
		private bool ignoreWhitespace;

		// Token: 0x040005C5 RID: 1477
		private bool ignorePIs;

		// Token: 0x040005C6 RID: 1478
		private bool ignoreComments;

		// Token: 0x040005C7 RID: 1479
		private DtdProcessing dtdProcessing;

		// Token: 0x040005C8 RID: 1480
		private ValidationType validationType;

		// Token: 0x040005C9 RID: 1481
		private XmlSchemaValidationFlags validationFlags;

		// Token: 0x040005CA RID: 1482
		private XmlSchemaSet schemas;

		// Token: 0x040005CB RID: 1483
		private ValidationEventHandler valEventHandler;

		// Token: 0x040005CC RID: 1484
		private bool closeInput;

		// Token: 0x040005CD RID: 1485
		private bool isReadOnly;

		// Token: 0x040005CE RID: 1486
		[CompilerGenerated]
		private bool <IsXmlResolverSet>k__BackingField;

		// Token: 0x040005CF RID: 1487
		private static bool? s_enableLegacyXmlSettings;
	}
}
