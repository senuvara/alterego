﻿using System;

namespace System.Xml
{
	// Token: 0x020000CA RID: 202
	internal class XmlAsyncCheckReaderWithLineInfo : XmlAsyncCheckReader, IXmlLineInfo
	{
		// Token: 0x060006CD RID: 1741 RVA: 0x0001BF8A File Offset: 0x0001A18A
		public XmlAsyncCheckReaderWithLineInfo(XmlReader reader) : base(reader)
		{
			this.readerAsIXmlLineInfo = (IXmlLineInfo)reader;
		}

		// Token: 0x060006CE RID: 1742 RVA: 0x0001BF9F File Offset: 0x0001A19F
		public virtual bool HasLineInfo()
		{
			return this.readerAsIXmlLineInfo.HasLineInfo();
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060006CF RID: 1743 RVA: 0x0001BFAC File Offset: 0x0001A1AC
		public virtual int LineNumber
		{
			get
			{
				return this.readerAsIXmlLineInfo.LineNumber;
			}
		}

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060006D0 RID: 1744 RVA: 0x0001BFB9 File Offset: 0x0001A1B9
		public virtual int LinePosition
		{
			get
			{
				return this.readerAsIXmlLineInfo.LinePosition;
			}
		}

		// Token: 0x040003DA RID: 986
		private readonly IXmlLineInfo readerAsIXmlLineInfo;
	}
}
