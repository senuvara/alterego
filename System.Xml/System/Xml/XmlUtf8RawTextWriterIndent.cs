﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x0200019D RID: 413
	internal class XmlUtf8RawTextWriterIndent : XmlUtf8RawTextWriter
	{
		// Token: 0x06000E78 RID: 3704 RVA: 0x00056EC2 File Offset: 0x000550C2
		public XmlUtf8RawTextWriterIndent(Stream stream, XmlWriterSettings settings) : base(stream, settings)
		{
			this.Init(settings);
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06000E79 RID: 3705 RVA: 0x00056ED3 File Offset: 0x000550D3
		public override XmlWriterSettings Settings
		{
			get
			{
				XmlWriterSettings settings = base.Settings;
				settings.ReadOnly = false;
				settings.Indent = true;
				settings.IndentChars = this.indentChars;
				settings.NewLineOnAttributes = this.newLineOnAttributes;
				settings.ReadOnly = true;
				return settings;
			}
		}

		// Token: 0x06000E7A RID: 3706 RVA: 0x00056F08 File Offset: 0x00055108
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			base.WriteDocType(name, pubid, sysid, subset);
		}

		// Token: 0x06000E7B RID: 3707 RVA: 0x00056F34 File Offset: 0x00055134
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			this.indentLevel++;
			this.mixedContentStack.PushBit(this.mixedContent);
			base.WriteStartElement(prefix, localName, ns);
		}

		// Token: 0x06000E7C RID: 3708 RVA: 0x00056F85 File Offset: 0x00055185
		internal override void StartElementContent()
		{
			if (this.indentLevel == 1 && this.conformanceLevel == ConformanceLevel.Document)
			{
				this.mixedContent = false;
			}
			else
			{
				this.mixedContent = this.mixedContentStack.PeekBit();
			}
			base.StartElementContent();
		}

		// Token: 0x06000E7D RID: 3709 RVA: 0x00056FB9 File Offset: 0x000551B9
		internal override void OnRootElement(ConformanceLevel currentConformanceLevel)
		{
			this.conformanceLevel = currentConformanceLevel;
		}

		// Token: 0x06000E7E RID: 3710 RVA: 0x00056FC4 File Offset: 0x000551C4
		internal override void WriteEndElement(string prefix, string localName, string ns)
		{
			this.indentLevel--;
			if (!this.mixedContent && this.contentPos != this.bufPos && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			this.mixedContent = this.mixedContentStack.PopBit();
			base.WriteEndElement(prefix, localName, ns);
		}

		// Token: 0x06000E7F RID: 3711 RVA: 0x00057024 File Offset: 0x00055224
		internal override void WriteFullEndElement(string prefix, string localName, string ns)
		{
			this.indentLevel--;
			if (!this.mixedContent && this.contentPos != this.bufPos && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			this.mixedContent = this.mixedContentStack.PopBit();
			base.WriteFullEndElement(prefix, localName, ns);
		}

		// Token: 0x06000E80 RID: 3712 RVA: 0x00057083 File Offset: 0x00055283
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			if (this.newLineOnAttributes)
			{
				this.WriteIndent();
			}
			base.WriteStartAttribute(prefix, localName, ns);
		}

		// Token: 0x06000E81 RID: 3713 RVA: 0x0005709C File Offset: 0x0005529C
		public override void WriteCData(string text)
		{
			this.mixedContent = true;
			base.WriteCData(text);
		}

		// Token: 0x06000E82 RID: 3714 RVA: 0x000570AC File Offset: 0x000552AC
		public override void WriteComment(string text)
		{
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			base.WriteComment(text);
		}

		// Token: 0x06000E83 RID: 3715 RVA: 0x000570D1 File Offset: 0x000552D1
		public override void WriteProcessingInstruction(string target, string text)
		{
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			base.WriteProcessingInstruction(target, text);
		}

		// Token: 0x06000E84 RID: 3716 RVA: 0x000570F7 File Offset: 0x000552F7
		public override void WriteEntityRef(string name)
		{
			this.mixedContent = true;
			base.WriteEntityRef(name);
		}

		// Token: 0x06000E85 RID: 3717 RVA: 0x00057107 File Offset: 0x00055307
		public override void WriteCharEntity(char ch)
		{
			this.mixedContent = true;
			base.WriteCharEntity(ch);
		}

		// Token: 0x06000E86 RID: 3718 RVA: 0x00057117 File Offset: 0x00055317
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			this.mixedContent = true;
			base.WriteSurrogateCharEntity(lowChar, highChar);
		}

		// Token: 0x06000E87 RID: 3719 RVA: 0x00057128 File Offset: 0x00055328
		public override void WriteWhitespace(string ws)
		{
			this.mixedContent = true;
			base.WriteWhitespace(ws);
		}

		// Token: 0x06000E88 RID: 3720 RVA: 0x00057138 File Offset: 0x00055338
		public override void WriteString(string text)
		{
			this.mixedContent = true;
			base.WriteString(text);
		}

		// Token: 0x06000E89 RID: 3721 RVA: 0x00057148 File Offset: 0x00055348
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.mixedContent = true;
			base.WriteChars(buffer, index, count);
		}

		// Token: 0x06000E8A RID: 3722 RVA: 0x0005715A File Offset: 0x0005535A
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.mixedContent = true;
			base.WriteRaw(buffer, index, count);
		}

		// Token: 0x06000E8B RID: 3723 RVA: 0x0005716C File Offset: 0x0005536C
		public override void WriteRaw(string data)
		{
			this.mixedContent = true;
			base.WriteRaw(data);
		}

		// Token: 0x06000E8C RID: 3724 RVA: 0x0005717C File Offset: 0x0005537C
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			this.mixedContent = true;
			base.WriteBase64(buffer, index, count);
		}

		// Token: 0x06000E8D RID: 3725 RVA: 0x00057190 File Offset: 0x00055390
		private void Init(XmlWriterSettings settings)
		{
			this.indentLevel = 0;
			this.indentChars = settings.IndentChars;
			this.newLineOnAttributes = settings.NewLineOnAttributes;
			this.mixedContentStack = new BitStack();
			if (this.checkCharacters)
			{
				if (this.newLineOnAttributes)
				{
					base.ValidateContentChars(this.indentChars, "IndentChars", true);
					base.ValidateContentChars(this.newLineChars, "NewLineChars", true);
					return;
				}
				base.ValidateContentChars(this.indentChars, "IndentChars", false);
				if (this.newLineHandling != NewLineHandling.Replace)
				{
					base.ValidateContentChars(this.newLineChars, "NewLineChars", false);
				}
			}
		}

		// Token: 0x06000E8E RID: 3726 RVA: 0x00057228 File Offset: 0x00055428
		private void WriteIndent()
		{
			base.RawText(this.newLineChars);
			for (int i = this.indentLevel; i > 0; i--)
			{
				base.RawText(this.indentChars);
			}
		}

		// Token: 0x06000E8F RID: 3727 RVA: 0x00057260 File Offset: 0x00055460
		public override async Task WriteDocTypeAsync(string name, string pubid, string sysid, string subset)
		{
			base.CheckAsyncCall();
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			await base.WriteDocTypeAsync(name, pubid, sysid, subset).ConfigureAwait(false);
		}

		// Token: 0x06000E90 RID: 3728 RVA: 0x000572C8 File Offset: 0x000554C8
		public override async Task WriteStartElementAsync(string prefix, string localName, string ns)
		{
			base.CheckAsyncCall();
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			this.indentLevel++;
			this.mixedContentStack.PushBit(this.mixedContent);
			await base.WriteStartElementAsync(prefix, localName, ns).ConfigureAwait(false);
		}

		// Token: 0x06000E91 RID: 3729 RVA: 0x00057328 File Offset: 0x00055528
		internal override async Task WriteEndElementAsync(string prefix, string localName, string ns)
		{
			base.CheckAsyncCall();
			this.indentLevel--;
			if (!this.mixedContent && this.contentPos != this.bufPos && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			this.mixedContent = this.mixedContentStack.PopBit();
			await base.WriteEndElementAsync(prefix, localName, ns).ConfigureAwait(false);
		}

		// Token: 0x06000E92 RID: 3730 RVA: 0x00057388 File Offset: 0x00055588
		internal override async Task WriteFullEndElementAsync(string prefix, string localName, string ns)
		{
			base.CheckAsyncCall();
			this.indentLevel--;
			if (!this.mixedContent && this.contentPos != this.bufPos && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			this.mixedContent = this.mixedContentStack.PopBit();
			await base.WriteFullEndElementAsync(prefix, localName, ns).ConfigureAwait(false);
		}

		// Token: 0x06000E93 RID: 3731 RVA: 0x000573E8 File Offset: 0x000555E8
		protected internal override async Task WriteStartAttributeAsync(string prefix, string localName, string ns)
		{
			base.CheckAsyncCall();
			if (this.newLineOnAttributes)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			await base.WriteStartAttributeAsync(prefix, localName, ns).ConfigureAwait(false);
		}

		// Token: 0x06000E94 RID: 3732 RVA: 0x00057445 File Offset: 0x00055645
		public override Task WriteCDataAsync(string text)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteCDataAsync(text);
		}

		// Token: 0x06000E95 RID: 3733 RVA: 0x0005745C File Offset: 0x0005565C
		public override async Task WriteCommentAsync(string text)
		{
			base.CheckAsyncCall();
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			await base.WriteCommentAsync(text).ConfigureAwait(false);
		}

		// Token: 0x06000E96 RID: 3734 RVA: 0x000574AC File Offset: 0x000556AC
		public override async Task WriteProcessingInstructionAsync(string target, string text)
		{
			base.CheckAsyncCall();
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			await base.WriteProcessingInstructionAsync(target, text).ConfigureAwait(false);
		}

		// Token: 0x06000E97 RID: 3735 RVA: 0x00057501 File Offset: 0x00055701
		public override Task WriteEntityRefAsync(string name)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteEntityRefAsync(name);
		}

		// Token: 0x06000E98 RID: 3736 RVA: 0x00057517 File Offset: 0x00055717
		public override Task WriteCharEntityAsync(char ch)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteCharEntityAsync(ch);
		}

		// Token: 0x06000E99 RID: 3737 RVA: 0x0005752D File Offset: 0x0005572D
		public override Task WriteSurrogateCharEntityAsync(char lowChar, char highChar)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteSurrogateCharEntityAsync(lowChar, highChar);
		}

		// Token: 0x06000E9A RID: 3738 RVA: 0x00057544 File Offset: 0x00055744
		public override Task WriteWhitespaceAsync(string ws)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteWhitespaceAsync(ws);
		}

		// Token: 0x06000E9B RID: 3739 RVA: 0x0005755A File Offset: 0x0005575A
		public override Task WriteStringAsync(string text)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteStringAsync(text);
		}

		// Token: 0x06000E9C RID: 3740 RVA: 0x00057570 File Offset: 0x00055770
		public override Task WriteCharsAsync(char[] buffer, int index, int count)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteCharsAsync(buffer, index, count);
		}

		// Token: 0x06000E9D RID: 3741 RVA: 0x00057588 File Offset: 0x00055788
		public override Task WriteRawAsync(char[] buffer, int index, int count)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteRawAsync(buffer, index, count);
		}

		// Token: 0x06000E9E RID: 3742 RVA: 0x000575A0 File Offset: 0x000557A0
		public override Task WriteRawAsync(string data)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteRawAsync(data);
		}

		// Token: 0x06000E9F RID: 3743 RVA: 0x000575B6 File Offset: 0x000557B6
		public override Task WriteBase64Async(byte[] buffer, int index, int count)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteBase64Async(buffer, index, count);
		}

		// Token: 0x06000EA0 RID: 3744 RVA: 0x000575D0 File Offset: 0x000557D0
		private async Task WriteIndentAsync()
		{
			base.CheckAsyncCall();
			await base.RawTextAsync(this.newLineChars).ConfigureAwait(false);
			for (int i = this.indentLevel; i > 0; i--)
			{
				await base.RawTextAsync(this.indentChars).ConfigureAwait(false);
			}
		}

		// Token: 0x06000EA1 RID: 3745 RVA: 0x00057615 File Offset: 0x00055815
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__0(string name, string pubid, string sysid, string subset)
		{
			return base.WriteDocTypeAsync(name, pubid, sysid, subset);
		}

		// Token: 0x06000EA2 RID: 3746 RVA: 0x00057622 File Offset: 0x00055822
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__1(string prefix, string localName, string ns)
		{
			return base.WriteStartElementAsync(prefix, localName, ns);
		}

		// Token: 0x06000EA3 RID: 3747 RVA: 0x0005762D File Offset: 0x0005582D
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__2(string prefix, string localName, string ns)
		{
			return base.WriteEndElementAsync(prefix, localName, ns);
		}

		// Token: 0x06000EA4 RID: 3748 RVA: 0x00057638 File Offset: 0x00055838
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__3(string prefix, string localName, string ns)
		{
			return base.WriteFullEndElementAsync(prefix, localName, ns);
		}

		// Token: 0x06000EA5 RID: 3749 RVA: 0x00057643 File Offset: 0x00055843
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__4(string prefix, string localName, string ns)
		{
			return base.WriteStartAttributeAsync(prefix, localName, ns);
		}

		// Token: 0x06000EA6 RID: 3750 RVA: 0x0005764E File Offset: 0x0005584E
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__5(string text)
		{
			return base.WriteCommentAsync(text);
		}

		// Token: 0x06000EA7 RID: 3751 RVA: 0x00057657 File Offset: 0x00055857
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__6(string name, string text)
		{
			return base.WriteProcessingInstructionAsync(name, text);
		}

		// Token: 0x04000A25 RID: 2597
		protected int indentLevel;

		// Token: 0x04000A26 RID: 2598
		protected bool newLineOnAttributes;

		// Token: 0x04000A27 RID: 2599
		protected string indentChars;

		// Token: 0x04000A28 RID: 2600
		protected bool mixedContent;

		// Token: 0x04000A29 RID: 2601
		private BitStack mixedContentStack;

		// Token: 0x04000A2A RID: 2602
		protected ConformanceLevel conformanceLevel;

		// Token: 0x0200019E RID: 414
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteDocTypeAsync>d__30 : IAsyncStateMachine
		{
			// Token: 0x06000EA8 RID: 3752 RVA: 0x00057664 File Offset: 0x00055864
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriterIndent xmlUtf8RawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_10A;
						}
						xmlUtf8RawTextWriterIndent.CheckAsyncCall();
						if (xmlUtf8RawTextWriterIndent.mixedContent || xmlUtf8RawTextWriterIndent.textPos == xmlUtf8RawTextWriterIndent.bufPos)
						{
							goto IL_98;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteDocTypeAsync>d__30>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_98:
					configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.<>n__0(name, pubid, sysid, subset).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteDocTypeAsync>d__30>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_10A:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000EA9 RID: 3753 RVA: 0x000577CC File Offset: 0x000559CC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A2B RID: 2603
			public int <>1__state;

			// Token: 0x04000A2C RID: 2604
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A2D RID: 2605
			public XmlUtf8RawTextWriterIndent <>4__this;

			// Token: 0x04000A2E RID: 2606
			public string name;

			// Token: 0x04000A2F RID: 2607
			public string pubid;

			// Token: 0x04000A30 RID: 2608
			public string sysid;

			// Token: 0x04000A31 RID: 2609
			public string subset;

			// Token: 0x04000A32 RID: 2610
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200019F RID: 415
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartElementAsync>d__31 : IAsyncStateMachine
		{
			// Token: 0x06000EAA RID: 3754 RVA: 0x000577DC File Offset: 0x000559DC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriterIndent xmlUtf8RawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_123;
						}
						xmlUtf8RawTextWriterIndent.CheckAsyncCall();
						if (xmlUtf8RawTextWriterIndent.mixedContent || xmlUtf8RawTextWriterIndent.textPos == xmlUtf8RawTextWriterIndent.bufPos)
						{
							goto IL_98;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteStartElementAsync>d__31>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_98:
					xmlUtf8RawTextWriterIndent.indentLevel++;
					xmlUtf8RawTextWriterIndent.mixedContentStack.PushBit(xmlUtf8RawTextWriterIndent.mixedContent);
					configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.<>n__1(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteStartElementAsync>d__31>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_123:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000EAB RID: 3755 RVA: 0x00057960 File Offset: 0x00055B60
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A33 RID: 2611
			public int <>1__state;

			// Token: 0x04000A34 RID: 2612
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A35 RID: 2613
			public XmlUtf8RawTextWriterIndent <>4__this;

			// Token: 0x04000A36 RID: 2614
			public string prefix;

			// Token: 0x04000A37 RID: 2615
			public string localName;

			// Token: 0x04000A38 RID: 2616
			public string ns;

			// Token: 0x04000A39 RID: 2617
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001A0 RID: 416
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteEndElementAsync>d__32 : IAsyncStateMachine
		{
			// Token: 0x06000EAC RID: 3756 RVA: 0x00057970 File Offset: 0x00055B70
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriterIndent xmlUtf8RawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_137;
						}
						xmlUtf8RawTextWriterIndent.CheckAsyncCall();
						xmlUtf8RawTextWriterIndent.indentLevel--;
						if (xmlUtf8RawTextWriterIndent.mixedContent || xmlUtf8RawTextWriterIndent.contentPos == xmlUtf8RawTextWriterIndent.bufPos || xmlUtf8RawTextWriterIndent.textPos == xmlUtf8RawTextWriterIndent.bufPos)
						{
							goto IL_BA;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteEndElementAsync>d__32>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_BA:
					xmlUtf8RawTextWriterIndent.mixedContent = xmlUtf8RawTextWriterIndent.mixedContentStack.PopBit();
					configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.<>n__2(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteEndElementAsync>d__32>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_137:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000EAD RID: 3757 RVA: 0x00057B08 File Offset: 0x00055D08
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A3A RID: 2618
			public int <>1__state;

			// Token: 0x04000A3B RID: 2619
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A3C RID: 2620
			public XmlUtf8RawTextWriterIndent <>4__this;

			// Token: 0x04000A3D RID: 2621
			public string prefix;

			// Token: 0x04000A3E RID: 2622
			public string localName;

			// Token: 0x04000A3F RID: 2623
			public string ns;

			// Token: 0x04000A40 RID: 2624
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001A1 RID: 417
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteFullEndElementAsync>d__33 : IAsyncStateMachine
		{
			// Token: 0x06000EAE RID: 3758 RVA: 0x00057B18 File Offset: 0x00055D18
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriterIndent xmlUtf8RawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_137;
						}
						xmlUtf8RawTextWriterIndent.CheckAsyncCall();
						xmlUtf8RawTextWriterIndent.indentLevel--;
						if (xmlUtf8RawTextWriterIndent.mixedContent || xmlUtf8RawTextWriterIndent.contentPos == xmlUtf8RawTextWriterIndent.bufPos || xmlUtf8RawTextWriterIndent.textPos == xmlUtf8RawTextWriterIndent.bufPos)
						{
							goto IL_BA;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteFullEndElementAsync>d__33>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_BA:
					xmlUtf8RawTextWriterIndent.mixedContent = xmlUtf8RawTextWriterIndent.mixedContentStack.PopBit();
					configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.<>n__3(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteFullEndElementAsync>d__33>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_137:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000EAF RID: 3759 RVA: 0x00057CB0 File Offset: 0x00055EB0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A41 RID: 2625
			public int <>1__state;

			// Token: 0x04000A42 RID: 2626
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A43 RID: 2627
			public XmlUtf8RawTextWriterIndent <>4__this;

			// Token: 0x04000A44 RID: 2628
			public string prefix;

			// Token: 0x04000A45 RID: 2629
			public string localName;

			// Token: 0x04000A46 RID: 2630
			public string ns;

			// Token: 0x04000A47 RID: 2631
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001A2 RID: 418
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartAttributeAsync>d__34 : IAsyncStateMachine
		{
			// Token: 0x06000EB0 RID: 3760 RVA: 0x00057CC0 File Offset: 0x00055EC0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriterIndent xmlUtf8RawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_F6;
						}
						xmlUtf8RawTextWriterIndent.CheckAsyncCall();
						if (!xmlUtf8RawTextWriterIndent.newLineOnAttributes)
						{
							goto IL_8A;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteStartAttributeAsync>d__34>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_8A:
					configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.<>n__4(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteStartAttributeAsync>d__34>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_F6:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000EB1 RID: 3761 RVA: 0x00057E08 File Offset: 0x00056008
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A48 RID: 2632
			public int <>1__state;

			// Token: 0x04000A49 RID: 2633
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A4A RID: 2634
			public XmlUtf8RawTextWriterIndent <>4__this;

			// Token: 0x04000A4B RID: 2635
			public string prefix;

			// Token: 0x04000A4C RID: 2636
			public string localName;

			// Token: 0x04000A4D RID: 2637
			public string ns;

			// Token: 0x04000A4E RID: 2638
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001A3 RID: 419
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCommentAsync>d__36 : IAsyncStateMachine
		{
			// Token: 0x06000EB2 RID: 3762 RVA: 0x00057E18 File Offset: 0x00056018
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriterIndent xmlUtf8RawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_F8;
						}
						xmlUtf8RawTextWriterIndent.CheckAsyncCall();
						if (xmlUtf8RawTextWriterIndent.mixedContent || xmlUtf8RawTextWriterIndent.textPos == xmlUtf8RawTextWriterIndent.bufPos)
						{
							goto IL_98;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteCommentAsync>d__36>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_98:
					configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.<>n__5(text).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteCommentAsync>d__36>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_F8:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000EB3 RID: 3763 RVA: 0x00057F64 File Offset: 0x00056164
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A4F RID: 2639
			public int <>1__state;

			// Token: 0x04000A50 RID: 2640
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A51 RID: 2641
			public XmlUtf8RawTextWriterIndent <>4__this;

			// Token: 0x04000A52 RID: 2642
			public string text;

			// Token: 0x04000A53 RID: 2643
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001A4 RID: 420
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteProcessingInstructionAsync>d__37 : IAsyncStateMachine
		{
			// Token: 0x06000EB4 RID: 3764 RVA: 0x00057F74 File Offset: 0x00056174
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriterIndent xmlUtf8RawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_FE;
						}
						xmlUtf8RawTextWriterIndent.CheckAsyncCall();
						if (xmlUtf8RawTextWriterIndent.mixedContent || xmlUtf8RawTextWriterIndent.textPos == xmlUtf8RawTextWriterIndent.bufPos)
						{
							goto IL_98;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteProcessingInstructionAsync>d__37>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_98:
					configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.<>n__6(target, text).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteProcessingInstructionAsync>d__37>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_FE:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000EB5 RID: 3765 RVA: 0x000580C4 File Offset: 0x000562C4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A54 RID: 2644
			public int <>1__state;

			// Token: 0x04000A55 RID: 2645
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A56 RID: 2646
			public XmlUtf8RawTextWriterIndent <>4__this;

			// Token: 0x04000A57 RID: 2647
			public string target;

			// Token: 0x04000A58 RID: 2648
			public string text;

			// Token: 0x04000A59 RID: 2649
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001A5 RID: 421
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteIndentAsync>d__47 : IAsyncStateMachine
		{
			// Token: 0x06000EB6 RID: 3766 RVA: 0x000580D4 File Offset: 0x000562D4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriterIndent xmlUtf8RawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_F6;
						}
						xmlUtf8RawTextWriterIndent.CheckAsyncCall();
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.RawTextAsync(xmlUtf8RawTextWriterIndent.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteIndentAsync>d__47>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					i = xmlUtf8RawTextWriterIndent.indentLevel;
					goto IL_10F;
					IL_F6:
					configuredTaskAwaiter.GetResult();
					int num3 = i;
					i = num3 - 1;
					IL_10F:
					if (i > 0)
					{
						configuredTaskAwaiter = xmlUtf8RawTextWriterIndent.RawTextAsync(xmlUtf8RawTextWriterIndent.indentChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriterIndent.<WriteIndentAsync>d__47>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_F6;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000EB7 RID: 3767 RVA: 0x00058248 File Offset: 0x00056448
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A5A RID: 2650
			public int <>1__state;

			// Token: 0x04000A5B RID: 2651
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A5C RID: 2652
			public XmlUtf8RawTextWriterIndent <>4__this;

			// Token: 0x04000A5D RID: 2653
			private int <i>5__1;

			// Token: 0x04000A5E RID: 2654
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
