﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Xml.Resolvers
{
	/// <summary>Represents a class that is used to prepopulate the cache with DTDs or XML streams.</summary>
	// Token: 0x02000480 RID: 1152
	public class XmlPreloadedResolver : XmlResolver
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> class.</summary>
		// Token: 0x06002D2C RID: 11564 RVA: 0x000F5461 File Offset: 0x000F3661
		public XmlPreloadedResolver() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> class with the specified preloaded well-known DTDs.</summary>
		/// <param name="preloadedDtds">The well-known DTDs that should be prepopulated into the cache.</param>
		// Token: 0x06002D2D RID: 11565 RVA: 0x000F546A File Offset: 0x000F366A
		public XmlPreloadedResolver(XmlKnownDtds preloadedDtds) : this(null, preloadedDtds, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> class with the specified fallback resolver.</summary>
		/// <param name="fallbackResolver">The <see langword="XmlResolver" />, <see langword="XmlXapResolver" />, or your own resolver.</param>
		// Token: 0x06002D2E RID: 11566 RVA: 0x000F5475 File Offset: 0x000F3675
		public XmlPreloadedResolver(XmlResolver fallbackResolver) : this(fallbackResolver, XmlKnownDtds.All, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> class with the specified fallback resolver and preloaded well-known DTDs.</summary>
		/// <param name="fallbackResolver">The <see langword="XmlResolver" />, <see langword="XmlXapResolver" />, or your own resolver.</param>
		/// <param name="preloadedDtds">The well-known DTDs that should be prepopulated into the cache.</param>
		// Token: 0x06002D2F RID: 11567 RVA: 0x000F5484 File Offset: 0x000F3684
		public XmlPreloadedResolver(XmlResolver fallbackResolver, XmlKnownDtds preloadedDtds) : this(fallbackResolver, preloadedDtds, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> class with the specified fallback resolver, preloaded well-known DTDs, and URI equality comparer.</summary>
		/// <param name="fallbackResolver">The <see langword="XmlResolver" />, <see langword="XmlXapResolver" />, or your own resolver.</param>
		/// <param name="preloadedDtds">The well-known DTDs that should be prepopulated into cache.</param>
		/// <param name="uriComparer">The implementation of the <see cref="T:System.Collections.Generic.IEqualityComparer`1" /> generic interface to use when you compare URIs.</param>
		// Token: 0x06002D30 RID: 11568 RVA: 0x000F5490 File Offset: 0x000F3690
		public XmlPreloadedResolver(XmlResolver fallbackResolver, XmlKnownDtds preloadedDtds, IEqualityComparer<Uri> uriComparer)
		{
			this.fallbackResolver = fallbackResolver;
			this.mappings = new Dictionary<Uri, XmlPreloadedResolver.PreloadedData>(16, uriComparer);
			this.preloadedDtds = preloadedDtds;
			if (preloadedDtds != XmlKnownDtds.None)
			{
				if ((preloadedDtds & XmlKnownDtds.Xhtml10) != XmlKnownDtds.None)
				{
					this.AddKnownDtd(XmlPreloadedResolver.Xhtml10_Dtd);
				}
				if ((preloadedDtds & XmlKnownDtds.Rss091) != XmlKnownDtds.None)
				{
					this.AddKnownDtd(XmlPreloadedResolver.Rss091_Dtd);
				}
			}
		}

		/// <summary>Resolves the absolute URI from the base and relative URIs.</summary>
		/// <param name="baseUri">The base URI used to resolve the relative URI.</param>
		/// <param name="relativeUri">The URI to resolve. The URI can be absolute or relative. If absolute, this value effectively replaces the <paramref name="baseUri" /> value. If relative, it combines with the <paramref name="baseUri" /> to make an absolute URI.</param>
		/// <returns>The <see cref="T:System.Uri" /> representing the absolute URI or <see langword="null" /> if the relative URI cannot be resolved.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="uri" /> is <see langword="null" />.</exception>
		// Token: 0x06002D31 RID: 11569 RVA: 0x000F54E4 File Offset: 0x000F36E4
		public override Uri ResolveUri(Uri baseUri, string relativeUri)
		{
			if (relativeUri != null && relativeUri.StartsWith("-//", StringComparison.CurrentCulture))
			{
				if ((this.preloadedDtds & XmlKnownDtds.Xhtml10) != XmlKnownDtds.None && relativeUri.StartsWith("-//W3C//", StringComparison.CurrentCulture))
				{
					for (int i = 0; i < XmlPreloadedResolver.Xhtml10_Dtd.Length; i++)
					{
						if (relativeUri == XmlPreloadedResolver.Xhtml10_Dtd[i].publicId)
						{
							return new Uri(relativeUri, UriKind.Relative);
						}
					}
				}
				if ((this.preloadedDtds & XmlKnownDtds.Rss091) != XmlKnownDtds.None && relativeUri == XmlPreloadedResolver.Rss091_Dtd[0].publicId)
				{
					return new Uri(relativeUri, UriKind.Relative);
				}
			}
			return base.ResolveUri(baseUri, relativeUri);
		}

		/// <summary>Maps a URI to an object that contains the actual resource.</summary>
		/// <param name="absoluteUri">The URI returned from <see cref="M:System.Xml.XmlResolver.ResolveUri(System.Uri,System.String)" />.</param>
		/// <param name="role">The current version of the .NET Framework for Silverlight does not use this parameter when resolving URIs. This parameter is provided for future extensibility purposes. For example, this parameter can be mapped to the xlink:role and used as an implementation-specific argument in other scenarios.</param>
		/// <param name="ofObjectToReturn">The type of object to return. The <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> supports <see cref="T:System.IO.Stream" /> objects and <see cref="T:System.IO.TextReader" /> objects for URIs that were added as <see langword="String" />. If the requested type is not supported by the resolver, an exception will be thrown. Use the <see cref="M:System.Xml.Resolvers.XmlPreloadedResolver.SupportsType(System.Uri,System.Type)" /> method to determine whether a certain <see langword="Type" /> is supported by this resolver.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> or <see cref="T:System.IO.TextReader" /> object that corresponds to the actual source.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="absoluteUri" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Xml.XmlException">Cannot resolve URI passed in <paramref name="absoluteUri" />.-or-
		///         <paramref name="ofObjectToReturn" /> is not of a supported type.</exception>
		// Token: 0x06002D32 RID: 11570 RVA: 0x000F5578 File Offset: 0x000F3778
		public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
		{
			if (absoluteUri == null)
			{
				throw new ArgumentNullException("absoluteUri");
			}
			XmlPreloadedResolver.PreloadedData preloadedData;
			if (!this.mappings.TryGetValue(absoluteUri, out preloadedData))
			{
				if (this.fallbackResolver != null)
				{
					return this.fallbackResolver.GetEntity(absoluteUri, role, ofObjectToReturn);
				}
				throw new XmlException(Res.GetString("Cannot resolve '{0}'.", new object[]
				{
					absoluteUri.ToString()
				}));
			}
			else
			{
				if (ofObjectToReturn == null || ofObjectToReturn == typeof(Stream) || ofObjectToReturn == typeof(object))
				{
					return preloadedData.AsStream();
				}
				if (ofObjectToReturn == typeof(TextReader))
				{
					return preloadedData.AsTextReader();
				}
				throw new XmlException(Res.GetString("Object type is not supported."));
			}
		}

		/// <summary>Sets the credentials that are used to authenticate the underlying <see cref="T:System.Net.WebRequest" />.</summary>
		/// <returns>The credentials that are used to authenticate the underlying web request.</returns>
		// Token: 0x17000968 RID: 2408
		// (set) Token: 0x06002D33 RID: 11571 RVA: 0x000F563B File Offset: 0x000F383B
		public override ICredentials Credentials
		{
			set
			{
				if (this.fallbackResolver != null)
				{
					this.fallbackResolver.Credentials = value;
				}
			}
		}

		/// <summary>Determines whether the resolver supports other <see cref="T:System.Type" />s than just <see cref="T:System.IO.Stream" />.</summary>
		/// <param name="absoluteUri">The absolute URI to check.</param>
		/// <param name="type">The <see cref="T:System.Type" /> to return.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Type" /> is supported; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="uri" /> is <see langword="null" />.</exception>
		// Token: 0x06002D34 RID: 11572 RVA: 0x000F5654 File Offset: 0x000F3854
		public override bool SupportsType(Uri absoluteUri, Type type)
		{
			if (absoluteUri == null)
			{
				throw new ArgumentNullException("absoluteUri");
			}
			XmlPreloadedResolver.PreloadedData preloadedData;
			if (this.mappings.TryGetValue(absoluteUri, out preloadedData))
			{
				return preloadedData.SupportsType(type);
			}
			if (this.fallbackResolver != null)
			{
				return this.fallbackResolver.SupportsType(absoluteUri, type);
			}
			return base.SupportsType(absoluteUri, type);
		}

		/// <summary>Adds a byte array to the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store and maps it to a URI. If the store already contains a mapping for the same URI, the existing mapping is overridden.</summary>
		/// <param name="uri">The URI of the data that is being added to the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store.</param>
		/// <param name="value">A byte array with the data that corresponds to the provided URI.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="uri" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06002D35 RID: 11573 RVA: 0x000F56AB File Offset: 0x000F38AB
		public void Add(Uri uri, byte[] value)
		{
			if (uri == null)
			{
				throw new ArgumentNullException("uri");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.Add(uri, new XmlPreloadedResolver.ByteArrayChunk(value, 0, value.Length));
		}

		/// <summary>Adds a byte array to the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store and maps it to a URI. If the store already contains a mapping for the same URI, the existing mapping is overridden.</summary>
		/// <param name="uri">The URI of the data that is being added to the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store.</param>
		/// <param name="value">A byte array with the data that corresponds to the provided URI.</param>
		/// <param name="offset">The offset in the provided byte array where the data starts.</param>
		/// <param name="count">The number of bytes to read from the byte array, starting at the provided offset.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="uri" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is less than 0.-or-The length of the <paramref name="value" /> minus <paramref name="offset" /> is less than <paramref name="count." /></exception>
		// Token: 0x06002D36 RID: 11574 RVA: 0x000F56E0 File Offset: 0x000F38E0
		public void Add(Uri uri, byte[] value, int offset, int count)
		{
			if (uri == null)
			{
				throw new ArgumentNullException("uri");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (value.Length - offset < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			this.Add(uri, new XmlPreloadedResolver.ByteArrayChunk(value, offset, count));
		}

		/// <summary>Adds a <see cref="T:System.IO.Stream" /> to the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store and maps it to a URI. If the store already contains a mapping for the same URI, the existing mapping is overridden.</summary>
		/// <param name="uri">The URI of the data that is being added to the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store.</param>
		/// <param name="value">A <see cref="T:System.IO.Stream" /> with the data that corresponds to the provided URI.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="uri" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06002D37 RID: 11575 RVA: 0x000F5754 File Offset: 0x000F3954
		public void Add(Uri uri, Stream value)
		{
			if (uri == null)
			{
				throw new ArgumentNullException("uri");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			checked
			{
				if (value.CanSeek)
				{
					int num = (int)value.Length;
					byte[] array = new byte[num];
					value.Read(array, 0, num);
					this.Add(uri, new XmlPreloadedResolver.ByteArrayChunk(array));
					return;
				}
				MemoryStream memoryStream = new MemoryStream();
				byte[] array2 = new byte[4096];
				int count;
				while ((count = value.Read(array2, 0, array2.Length)) > 0)
				{
					memoryStream.Write(array2, 0, count);
				}
				int num2 = (int)memoryStream.Position;
				byte[] array3 = new byte[num2];
				Array.Copy(memoryStream.GetBuffer(), array3, num2);
				this.Add(uri, new XmlPreloadedResolver.ByteArrayChunk(array3));
			}
		}

		/// <summary>Adds a string with preloaded data to the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store and maps it to a URI. If the store already contains a mapping for the same URI, the existing mapping is overridden.</summary>
		/// <param name="uri">The URI of the data that is being added to the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store.</param>
		/// <param name="value">A <see langword="String" /> with the data that corresponds to the provided URI.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="uri" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06002D38 RID: 11576 RVA: 0x000F580F File Offset: 0x000F3A0F
		public void Add(Uri uri, string value)
		{
			if (uri == null)
			{
				throw new ArgumentNullException("uri");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.Add(uri, new XmlPreloadedResolver.StringData(value));
		}

		/// <summary>Gets a collection of preloaded URIs.</summary>
		/// <returns>The collection of preloaded URIs.</returns>
		// Token: 0x17000969 RID: 2409
		// (get) Token: 0x06002D39 RID: 11577 RVA: 0x000F5840 File Offset: 0x000F3A40
		public IEnumerable<Uri> PreloadedUris
		{
			get
			{
				return this.mappings.Keys;
			}
		}

		/// <summary>Removes the data that corresponds to the URI from the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" />.</summary>
		/// <param name="uri">The URI of the data that should be removed from the <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> store.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="uri" /> is <see langword="null" />.</exception>
		// Token: 0x06002D3A RID: 11578 RVA: 0x000F584D File Offset: 0x000F3A4D
		public void Remove(Uri uri)
		{
			if (uri == null)
			{
				throw new ArgumentNullException("uri");
			}
			this.mappings.Remove(uri);
		}

		// Token: 0x06002D3B RID: 11579 RVA: 0x000F5870 File Offset: 0x000F3A70
		private void Add(Uri uri, XmlPreloadedResolver.PreloadedData data)
		{
			if (this.mappings.ContainsKey(uri))
			{
				this.mappings[uri] = data;
				return;
			}
			this.mappings.Add(uri, data);
		}

		// Token: 0x06002D3C RID: 11580 RVA: 0x000F589C File Offset: 0x000F3A9C
		private void AddKnownDtd(XmlPreloadedResolver.XmlKnownDtdData[] dtdSet)
		{
			foreach (XmlPreloadedResolver.XmlKnownDtdData xmlKnownDtdData in dtdSet)
			{
				this.mappings.Add(new Uri(xmlKnownDtdData.publicId, UriKind.RelativeOrAbsolute), xmlKnownDtdData);
				this.mappings.Add(new Uri(xmlKnownDtdData.systemId, UriKind.RelativeOrAbsolute), xmlKnownDtdData);
			}
		}

		/// <summary>Asynchronously maps a URI to an object that contains the actual resource.</summary>
		/// <param name="absoluteUri">The URI returned from <see cref="M:System.Xml.XmlResolver.ResolveUri(System.Uri,System.String)" />.</param>
		/// <param name="role">The current version of the .NET Framework for Silverlight does not use this parameter when resolving URIs. This parameter is provided for future extensibility purposes. For example, this parameter can be mapped to the xlink:role and used as an implementation-specific argument in other scenarios.</param>
		/// <param name="ofObjectToReturn">The type of object to return. The <see cref="T:System.Xml.Resolvers.XmlPreloadedResolver" /> supports <see cref="T:System.IO.Stream" /> objects and <see cref="T:System.IO.TextReader" /> objects for URIs that were added as <see langword="String" />. If the requested type is not supported by the resolver, an exception will be thrown. Use the <see cref="M:System.Xml.Resolvers.XmlPreloadedResolver.SupportsType(System.Uri,System.Type)" /> method to determine whether a certain <see langword="Type" /> is supported by this resolver.</param>
		/// <returns>A <see cref="T:System.IO.Stream" /> or <see cref="T:System.IO.TextReader" /> object that corresponds to the actual source.</returns>
		// Token: 0x06002D3D RID: 11581 RVA: 0x000F58EC File Offset: 0x000F3AEC
		public override Task<object> GetEntityAsync(Uri absoluteUri, string role, Type ofObjectToReturn)
		{
			if (absoluteUri == null)
			{
				throw new ArgumentNullException("absoluteUri");
			}
			XmlPreloadedResolver.PreloadedData preloadedData;
			if (!this.mappings.TryGetValue(absoluteUri, out preloadedData))
			{
				if (this.fallbackResolver != null)
				{
					return this.fallbackResolver.GetEntityAsync(absoluteUri, role, ofObjectToReturn);
				}
				throw new XmlException(Res.GetString("Cannot resolve '{0}'.", new object[]
				{
					absoluteUri.ToString()
				}));
			}
			else
			{
				if (ofObjectToReturn == null || ofObjectToReturn == typeof(Stream) || ofObjectToReturn == typeof(object))
				{
					return Task.FromResult<object>(preloadedData.AsStream());
				}
				if (ofObjectToReturn == typeof(TextReader))
				{
					return Task.FromResult<object>(preloadedData.AsTextReader());
				}
				throw new XmlException(Res.GetString("Object type is not supported."));
			}
		}

		// Token: 0x06002D3E RID: 11582 RVA: 0x000F59BC File Offset: 0x000F3BBC
		// Note: this type is marked as 'beforefieldinit'.
		static XmlPreloadedResolver()
		{
		}

		// Token: 0x04001ED9 RID: 7897
		private XmlResolver fallbackResolver;

		// Token: 0x04001EDA RID: 7898
		private Dictionary<Uri, XmlPreloadedResolver.PreloadedData> mappings;

		// Token: 0x04001EDB RID: 7899
		private XmlKnownDtds preloadedDtds;

		// Token: 0x04001EDC RID: 7900
		private static XmlPreloadedResolver.XmlKnownDtdData[] Xhtml10_Dtd = new XmlPreloadedResolver.XmlKnownDtdData[]
		{
			new XmlPreloadedResolver.XmlKnownDtdData("-//W3C//DTD XHTML 1.0 Strict//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd", "xhtml1-strict.dtd"),
			new XmlPreloadedResolver.XmlKnownDtdData("-//W3C//DTD XHTML 1.0 Transitional//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd", "xhtml1-transitional.dtd"),
			new XmlPreloadedResolver.XmlKnownDtdData("-//W3C//DTD XHTML 1.0 Frameset//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd", "xhtml1-frameset.dtd"),
			new XmlPreloadedResolver.XmlKnownDtdData("-//W3C//ENTITIES Latin 1 for XHTML//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml-lat1.ent", "xhtml-lat1.ent"),
			new XmlPreloadedResolver.XmlKnownDtdData("-//W3C//ENTITIES Symbols for XHTML//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml-symbol.ent", "xhtml-symbol.ent"),
			new XmlPreloadedResolver.XmlKnownDtdData("-//W3C//ENTITIES Special for XHTML//EN", "http://www.w3.org/TR/xhtml1/DTD/xhtml-special.ent", "xhtml-special.ent")
		};

		// Token: 0x04001EDD RID: 7901
		private static XmlPreloadedResolver.XmlKnownDtdData[] Rss091_Dtd = new XmlPreloadedResolver.XmlKnownDtdData[]
		{
			new XmlPreloadedResolver.XmlKnownDtdData("-//Netscape Communications//DTD RSS 0.91//EN", "http://my.netscape.com/publish/formats/rss-0.91.dtd", "rss-0.91.dtd")
		};

		// Token: 0x02000481 RID: 1153
		private abstract class PreloadedData
		{
			// Token: 0x06002D3F RID: 11583
			internal abstract Stream AsStream();

			// Token: 0x06002D40 RID: 11584 RVA: 0x000F5A80 File Offset: 0x000F3C80
			internal virtual TextReader AsTextReader()
			{
				throw new XmlException(Res.GetString("Object type is not supported."));
			}

			// Token: 0x06002D41 RID: 11585 RVA: 0x000F5A91 File Offset: 0x000F3C91
			internal virtual bool SupportsType(Type type)
			{
				return type == null || type == typeof(Stream);
			}

			// Token: 0x06002D42 RID: 11586 RVA: 0x00002103 File Offset: 0x00000303
			protected PreloadedData()
			{
			}
		}

		// Token: 0x02000482 RID: 1154
		private class XmlKnownDtdData : XmlPreloadedResolver.PreloadedData
		{
			// Token: 0x06002D43 RID: 11587 RVA: 0x000F5AB1 File Offset: 0x000F3CB1
			internal XmlKnownDtdData(string publicId, string systemId, string resourceName)
			{
				this.publicId = publicId;
				this.systemId = systemId;
				this.resourceName = resourceName;
			}

			// Token: 0x06002D44 RID: 11588 RVA: 0x000F5ACE File Offset: 0x000F3CCE
			internal override Stream AsStream()
			{
				return Assembly.GetExecutingAssembly().GetManifestResourceStream(this.resourceName);
			}

			// Token: 0x04001EDE RID: 7902
			internal string publicId;

			// Token: 0x04001EDF RID: 7903
			internal string systemId;

			// Token: 0x04001EE0 RID: 7904
			private string resourceName;
		}

		// Token: 0x02000483 RID: 1155
		private class ByteArrayChunk : XmlPreloadedResolver.PreloadedData
		{
			// Token: 0x06002D45 RID: 11589 RVA: 0x000F5AE0 File Offset: 0x000F3CE0
			internal ByteArrayChunk(byte[] array) : this(array, 0, array.Length)
			{
			}

			// Token: 0x06002D46 RID: 11590 RVA: 0x000F5AED File Offset: 0x000F3CED
			internal ByteArrayChunk(byte[] array, int offset, int length)
			{
				this.array = array;
				this.offset = offset;
				this.length = length;
			}

			// Token: 0x06002D47 RID: 11591 RVA: 0x000F5B0A File Offset: 0x000F3D0A
			internal override Stream AsStream()
			{
				return new MemoryStream(this.array, this.offset, this.length);
			}

			// Token: 0x04001EE1 RID: 7905
			private byte[] array;

			// Token: 0x04001EE2 RID: 7906
			private int offset;

			// Token: 0x04001EE3 RID: 7907
			private int length;
		}

		// Token: 0x02000484 RID: 1156
		private class StringData : XmlPreloadedResolver.PreloadedData
		{
			// Token: 0x06002D48 RID: 11592 RVA: 0x000F5B23 File Offset: 0x000F3D23
			internal StringData(string str)
			{
				this.str = str;
			}

			// Token: 0x06002D49 RID: 11593 RVA: 0x000F5B32 File Offset: 0x000F3D32
			internal override Stream AsStream()
			{
				return new MemoryStream(Encoding.Unicode.GetBytes(this.str));
			}

			// Token: 0x06002D4A RID: 11594 RVA: 0x000F5B49 File Offset: 0x000F3D49
			internal override TextReader AsTextReader()
			{
				return new StringReader(this.str);
			}

			// Token: 0x06002D4B RID: 11595 RVA: 0x000F5B56 File Offset: 0x000F3D56
			internal override bool SupportsType(Type type)
			{
				return type == typeof(TextReader) || base.SupportsType(type);
			}

			// Token: 0x04001EE4 RID: 7908
			private string str;
		}
	}
}
