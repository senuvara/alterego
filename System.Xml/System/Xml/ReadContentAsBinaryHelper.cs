﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020000B0 RID: 176
	internal class ReadContentAsBinaryHelper
	{
		// Token: 0x060005C9 RID: 1481 RVA: 0x00018EB6 File Offset: 0x000170B6
		internal ReadContentAsBinaryHelper(XmlReader reader)
		{
			this.reader = reader;
			this.canReadValueChunk = reader.CanReadValueChunk;
			if (this.canReadValueChunk)
			{
				this.valueChunk = new char[256];
			}
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x00018EE9 File Offset: 0x000170E9
		internal static ReadContentAsBinaryHelper CreateOrReset(ReadContentAsBinaryHelper helper, XmlReader reader)
		{
			if (helper == null)
			{
				return new ReadContentAsBinaryHelper(reader);
			}
			helper.Reset();
			return helper;
		}

		// Token: 0x060005CB RID: 1483 RVA: 0x00018EFC File Offset: 0x000170FC
		internal int ReadContentAsBase64(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			switch (this.state)
			{
			case ReadContentAsBinaryHelper.State.None:
				if (!this.reader.CanReadContentAs())
				{
					throw this.reader.CreateReadContentAsException("ReadContentAsBase64");
				}
				if (!this.Init())
				{
					return 0;
				}
				break;
			case ReadContentAsBinaryHelper.State.InReadContent:
				if (this.decoder == this.base64Decoder)
				{
					return this.ReadContentAsBinary(buffer, index, count);
				}
				break;
			case ReadContentAsBinaryHelper.State.InReadElementContent:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			default:
				return 0;
			}
			this.InitBase64Decoder();
			return this.ReadContentAsBinary(buffer, index, count);
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x00018FC4 File Offset: 0x000171C4
		internal int ReadContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			switch (this.state)
			{
			case ReadContentAsBinaryHelper.State.None:
				if (!this.reader.CanReadContentAs())
				{
					throw this.reader.CreateReadContentAsException("ReadContentAsBinHex");
				}
				if (!this.Init())
				{
					return 0;
				}
				break;
			case ReadContentAsBinaryHelper.State.InReadContent:
				if (this.decoder == this.binHexDecoder)
				{
					return this.ReadContentAsBinary(buffer, index, count);
				}
				break;
			case ReadContentAsBinaryHelper.State.InReadElementContent:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			default:
				return 0;
			}
			this.InitBinHexDecoder();
			return this.ReadContentAsBinary(buffer, index, count);
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x0001908C File Offset: 0x0001728C
		internal int ReadElementContentAsBase64(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			switch (this.state)
			{
			case ReadContentAsBinaryHelper.State.None:
				if (this.reader.NodeType != XmlNodeType.Element)
				{
					throw this.reader.CreateReadElementContentAsException("ReadElementContentAsBase64");
				}
				if (!this.InitOnElement())
				{
					return 0;
				}
				break;
			case ReadContentAsBinaryHelper.State.InReadContent:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case ReadContentAsBinaryHelper.State.InReadElementContent:
				if (this.decoder == this.base64Decoder)
				{
					return this.ReadElementContentAsBinary(buffer, index, count);
				}
				break;
			default:
				return 0;
			}
			this.InitBase64Decoder();
			return this.ReadElementContentAsBinary(buffer, index, count);
		}

		// Token: 0x060005CE RID: 1486 RVA: 0x00019158 File Offset: 0x00017358
		internal int ReadElementContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			switch (this.state)
			{
			case ReadContentAsBinaryHelper.State.None:
				if (this.reader.NodeType != XmlNodeType.Element)
				{
					throw this.reader.CreateReadElementContentAsException("ReadElementContentAsBinHex");
				}
				if (!this.InitOnElement())
				{
					return 0;
				}
				break;
			case ReadContentAsBinaryHelper.State.InReadContent:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case ReadContentAsBinaryHelper.State.InReadElementContent:
				if (this.decoder == this.binHexDecoder)
				{
					return this.ReadElementContentAsBinary(buffer, index, count);
				}
				break;
			default:
				return 0;
			}
			this.InitBinHexDecoder();
			return this.ReadElementContentAsBinary(buffer, index, count);
		}

		// Token: 0x060005CF RID: 1487 RVA: 0x00019224 File Offset: 0x00017424
		internal void Finish()
		{
			if (this.state != ReadContentAsBinaryHelper.State.None)
			{
				while (this.MoveToNextContentNode(true))
				{
				}
				if (this.state == ReadContentAsBinaryHelper.State.InReadElementContent)
				{
					if (this.reader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
					}
					this.reader.Read();
				}
			}
			this.Reset();
		}

		// Token: 0x060005D0 RID: 1488 RVA: 0x0001929B File Offset: 0x0001749B
		internal void Reset()
		{
			this.state = ReadContentAsBinaryHelper.State.None;
			this.isEnd = false;
			this.valueOffset = 0;
		}

		// Token: 0x060005D1 RID: 1489 RVA: 0x000192B2 File Offset: 0x000174B2
		private bool Init()
		{
			if (!this.MoveToNextContentNode(false))
			{
				return false;
			}
			this.state = ReadContentAsBinaryHelper.State.InReadContent;
			this.isEnd = false;
			return true;
		}

		// Token: 0x060005D2 RID: 1490 RVA: 0x000192D0 File Offset: 0x000174D0
		private bool InitOnElement()
		{
			bool isEmptyElement = this.reader.IsEmptyElement;
			this.reader.Read();
			if (isEmptyElement)
			{
				return false;
			}
			if (this.MoveToNextContentNode(false))
			{
				this.state = ReadContentAsBinaryHelper.State.InReadElementContent;
				this.isEnd = false;
				return true;
			}
			if (this.reader.NodeType != XmlNodeType.EndElement)
			{
				throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
			}
			this.reader.Read();
			return false;
		}

		// Token: 0x060005D3 RID: 1491 RVA: 0x0001935C File Offset: 0x0001755C
		private void InitBase64Decoder()
		{
			if (this.base64Decoder == null)
			{
				this.base64Decoder = new Base64Decoder();
			}
			else
			{
				this.base64Decoder.Reset();
			}
			this.decoder = this.base64Decoder;
		}

		// Token: 0x060005D4 RID: 1492 RVA: 0x0001938A File Offset: 0x0001758A
		private void InitBinHexDecoder()
		{
			if (this.binHexDecoder == null)
			{
				this.binHexDecoder = new BinHexDecoder();
			}
			else
			{
				this.binHexDecoder.Reset();
			}
			this.decoder = this.binHexDecoder;
		}

		// Token: 0x060005D5 RID: 1493 RVA: 0x000193B8 File Offset: 0x000175B8
		private int ReadContentAsBinary(byte[] buffer, int index, int count)
		{
			if (this.isEnd)
			{
				this.Reset();
				return 0;
			}
			this.decoder.SetNextOutputBuffer(buffer, index, count);
			for (;;)
			{
				if (this.canReadValueChunk)
				{
					for (;;)
					{
						if (this.valueOffset < this.valueChunkLength)
						{
							int num = this.decoder.Decode(this.valueChunk, this.valueOffset, this.valueChunkLength - this.valueOffset);
							this.valueOffset += num;
						}
						if (this.decoder.IsFull)
						{
							goto Block_3;
						}
						if ((this.valueChunkLength = this.reader.ReadValueChunk(this.valueChunk, 0, 256)) == 0)
						{
							break;
						}
						this.valueOffset = 0;
					}
				}
				else
				{
					string value = this.reader.Value;
					int num2 = this.decoder.Decode(value, this.valueOffset, value.Length - this.valueOffset);
					this.valueOffset += num2;
					if (this.decoder.IsFull)
					{
						goto Block_5;
					}
				}
				this.valueOffset = 0;
				if (!this.MoveToNextContentNode(true))
				{
					goto Block_6;
				}
			}
			Block_3:
			return this.decoder.DecodedCount;
			Block_5:
			return this.decoder.DecodedCount;
			Block_6:
			this.isEnd = true;
			return this.decoder.DecodedCount;
		}

		// Token: 0x060005D6 RID: 1494 RVA: 0x000194F0 File Offset: 0x000176F0
		private int ReadElementContentAsBinary(byte[] buffer, int index, int count)
		{
			if (count == 0)
			{
				return 0;
			}
			int num = this.ReadContentAsBinary(buffer, index, count);
			if (num > 0)
			{
				return num;
			}
			if (this.reader.NodeType != XmlNodeType.EndElement)
			{
				throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
			}
			this.reader.Read();
			this.state = ReadContentAsBinaryHelper.State.None;
			return 0;
		}

		// Token: 0x060005D7 RID: 1495 RVA: 0x00019564 File Offset: 0x00017764
		private bool MoveToNextContentNode(bool moveIfOnContentNode)
		{
			for (;;)
			{
				switch (this.reader.NodeType)
				{
				case XmlNodeType.Attribute:
					goto IL_52;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					if (!moveIfOnContentNode)
					{
						return true;
					}
					goto IL_78;
				case XmlNodeType.EntityReference:
					if (this.reader.CanResolveEntity)
					{
						this.reader.ResolveEntity();
						goto IL_78;
					}
					break;
				case XmlNodeType.ProcessingInstruction:
				case XmlNodeType.Comment:
				case XmlNodeType.EndEntity:
					goto IL_78;
				}
				break;
				IL_78:
				moveIfOnContentNode = false;
				if (!this.reader.Read())
				{
					return false;
				}
			}
			return false;
			IL_52:
			return !moveIfOnContentNode;
		}

		// Token: 0x060005D8 RID: 1496 RVA: 0x00019600 File Offset: 0x00017800
		internal async Task<int> ReadContentAsBase64Async(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			switch (this.state)
			{
			case ReadContentAsBinaryHelper.State.None:
			{
				if (!this.reader.CanReadContentAs())
				{
					throw this.reader.CreateReadContentAsException("ReadContentAsBase64");
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.InitAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return 0;
				}
				break;
			}
			case ReadContentAsBinaryHelper.State.InReadContent:
				if (this.decoder == this.base64Decoder)
				{
					return await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
				}
				break;
			case ReadContentAsBinaryHelper.State.InReadElementContent:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			default:
				return 0;
			}
			this.InitBase64Decoder();
			return await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
		}

		// Token: 0x060005D9 RID: 1497 RVA: 0x00019660 File Offset: 0x00017860
		internal async Task<int> ReadContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			switch (this.state)
			{
			case ReadContentAsBinaryHelper.State.None:
			{
				if (!this.reader.CanReadContentAs())
				{
					throw this.reader.CreateReadContentAsException("ReadContentAsBinHex");
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.InitAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return 0;
				}
				break;
			}
			case ReadContentAsBinaryHelper.State.InReadContent:
				if (this.decoder == this.binHexDecoder)
				{
					return await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
				}
				break;
			case ReadContentAsBinaryHelper.State.InReadElementContent:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			default:
				return 0;
			}
			this.InitBinHexDecoder();
			return await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
		}

		// Token: 0x060005DA RID: 1498 RVA: 0x000196C0 File Offset: 0x000178C0
		internal async Task<int> ReadElementContentAsBase64Async(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			switch (this.state)
			{
			case ReadContentAsBinaryHelper.State.None:
			{
				if (this.reader.NodeType != XmlNodeType.Element)
				{
					throw this.reader.CreateReadElementContentAsException("ReadElementContentAsBase64");
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.InitOnElementAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return 0;
				}
				break;
			}
			case ReadContentAsBinaryHelper.State.InReadContent:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case ReadContentAsBinaryHelper.State.InReadElementContent:
				if (this.decoder == this.base64Decoder)
				{
					return await this.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
				}
				break;
			default:
				return 0;
			}
			this.InitBase64Decoder();
			return await this.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
		}

		// Token: 0x060005DB RID: 1499 RVA: 0x00019720 File Offset: 0x00017920
		internal async Task<int> ReadElementContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			switch (this.state)
			{
			case ReadContentAsBinaryHelper.State.None:
			{
				if (this.reader.NodeType != XmlNodeType.Element)
				{
					throw this.reader.CreateReadElementContentAsException("ReadElementContentAsBinHex");
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.InitOnElementAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return 0;
				}
				break;
			}
			case ReadContentAsBinaryHelper.State.InReadContent:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case ReadContentAsBinaryHelper.State.InReadElementContent:
				if (this.decoder == this.binHexDecoder)
				{
					return await this.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
				}
				break;
			default:
				return 0;
			}
			this.InitBinHexDecoder();
			return await this.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
		}

		// Token: 0x060005DC RID: 1500 RVA: 0x00019780 File Offset: 0x00017980
		internal async Task FinishAsync()
		{
			if (this.state != ReadContentAsBinaryHelper.State.None)
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				do
				{
					configuredTaskAwaiter = this.MoveToNextContentNodeAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
				}
				while (configuredTaskAwaiter.GetResult());
				if (this.state == ReadContentAsBinaryHelper.State.InReadElementContent)
				{
					if (this.reader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
					}
					await this.reader.ReadAsync().ConfigureAwait(false);
				}
			}
			this.Reset();
		}

		// Token: 0x060005DD RID: 1501 RVA: 0x000197C8 File Offset: 0x000179C8
		private async Task<bool> InitAsync()
		{
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.MoveToNextContentNodeAsync(false).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			bool result;
			if (!configuredTaskAwaiter.GetResult())
			{
				result = false;
			}
			else
			{
				this.state = ReadContentAsBinaryHelper.State.InReadContent;
				this.isEnd = false;
				result = true;
			}
			return result;
		}

		// Token: 0x060005DE RID: 1502 RVA: 0x00019810 File Offset: 0x00017A10
		private async Task<bool> InitOnElementAsync()
		{
			bool isEmpty = this.reader.IsEmptyElement;
			await this.reader.ReadAsync().ConfigureAwait(false);
			bool result;
			if (isEmpty)
			{
				result = false;
			}
			else
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.MoveToNextContentNodeAsync(false).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					if (this.reader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
					}
					await this.reader.ReadAsync().ConfigureAwait(false);
					result = false;
				}
				else
				{
					this.state = ReadContentAsBinaryHelper.State.InReadElementContent;
					this.isEnd = false;
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060005DF RID: 1503 RVA: 0x00019858 File Offset: 0x00017A58
		private async Task<int> ReadContentAsBinaryAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (this.isEnd)
			{
				this.Reset();
				result = 0;
			}
			else
			{
				this.decoder.SetNextOutputBuffer(buffer, index, count);
				for (;;)
				{
					if (this.canReadValueChunk)
					{
						for (;;)
						{
							if (this.valueOffset < this.valueChunkLength)
							{
								int num = this.decoder.Decode(this.valueChunk, this.valueOffset, this.valueChunkLength - this.valueOffset);
								this.valueOffset += num;
							}
							if (this.decoder.IsFull)
							{
								goto Block_3;
							}
							int num2 = await this.reader.ReadValueChunkAsync(this.valueChunk, 0, 256).ConfigureAwait(false);
							int num3 = num2;
							this.valueChunkLength = num3;
							if (num3 == 0)
							{
								break;
							}
							this.valueOffset = 0;
						}
					}
					else
					{
						string text = await this.reader.GetValueAsync().ConfigureAwait(false);
						int num4 = this.decoder.Decode(text, this.valueOffset, text.Length - this.valueOffset);
						this.valueOffset += num4;
						if (this.decoder.IsFull)
						{
							goto Block_5;
						}
					}
					this.valueOffset = 0;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.MoveToNextContentNodeAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (!configuredTaskAwaiter.GetResult())
					{
						goto Block_7;
					}
				}
				Block_3:
				return this.decoder.DecodedCount;
				Block_5:
				return this.decoder.DecodedCount;
				Block_7:
				this.isEnd = true;
				result = this.decoder.DecodedCount;
			}
			return result;
		}

		// Token: 0x060005E0 RID: 1504 RVA: 0x000198B8 File Offset: 0x00017AB8
		private async Task<int> ReadElementContentAsBinaryAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (count == 0)
			{
				result = 0;
			}
			else
			{
				int num = await this.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false);
				if (num > 0)
				{
					result = num;
				}
				else
				{
					if (this.reader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
					}
					await this.reader.ReadAsync().ConfigureAwait(false);
					this.state = ReadContentAsBinaryHelper.State.None;
					result = 0;
				}
			}
			return result;
		}

		// Token: 0x060005E1 RID: 1505 RVA: 0x00019918 File Offset: 0x00017B18
		private async Task<bool> MoveToNextContentNodeAsync(bool moveIfOnContentNode)
		{
			for (;;)
			{
				switch (this.reader.NodeType)
				{
				case XmlNodeType.Attribute:
					goto IL_66;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					if (!moveIfOnContentNode)
					{
						goto Block_1;
					}
					goto IL_A5;
				case XmlNodeType.EntityReference:
					if (this.reader.CanResolveEntity)
					{
						this.reader.ResolveEntity();
						goto IL_A5;
					}
					break;
				case XmlNodeType.ProcessingInstruction:
				case XmlNodeType.Comment:
				case XmlNodeType.EndEntity:
					goto IL_A5;
				}
				break;
				IL_A5:
				moveIfOnContentNode = false;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					goto Block_4;
				}
			}
			goto IL_9E;
			IL_66:
			return !moveIfOnContentNode;
			Block_1:
			return true;
			IL_9E:
			return false;
			Block_4:
			return false;
		}

		// Token: 0x04000353 RID: 851
		private XmlReader reader;

		// Token: 0x04000354 RID: 852
		private ReadContentAsBinaryHelper.State state;

		// Token: 0x04000355 RID: 853
		private int valueOffset;

		// Token: 0x04000356 RID: 854
		private bool isEnd;

		// Token: 0x04000357 RID: 855
		private bool canReadValueChunk;

		// Token: 0x04000358 RID: 856
		private char[] valueChunk;

		// Token: 0x04000359 RID: 857
		private int valueChunkLength;

		// Token: 0x0400035A RID: 858
		private IncrementalReadDecoder decoder;

		// Token: 0x0400035B RID: 859
		private Base64Decoder base64Decoder;

		// Token: 0x0400035C RID: 860
		private BinHexDecoder binHexDecoder;

		// Token: 0x0400035D RID: 861
		private const int ChunkSize = 256;

		// Token: 0x020000B1 RID: 177
		private enum State
		{
			// Token: 0x0400035F RID: 863
			None,
			// Token: 0x04000360 RID: 864
			InReadContent,
			// Token: 0x04000361 RID: 865
			InReadElementContent
		}

		// Token: 0x020000B2 RID: 178
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBase64Async>d__27 : IAsyncStateMachine
		{
			// Token: 0x060005E2 RID: 1506 RVA: 0x00019968 File Offset: 0x00017B68
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1B1;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_24B;
					}
					default:
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						switch (readContentAsBinaryHelper.state)
						{
						case ReadContentAsBinaryHelper.State.None:
							if (!readContentAsBinaryHelper.reader.CanReadContentAs())
							{
								throw readContentAsBinaryHelper.reader.CreateReadContentAsException("ReadContentAsBase64");
							}
							configuredTaskAwaiter3 = readContentAsBinaryHelper.InitAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBase64Async>d__27>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						case ReadContentAsBinaryHelper.State.InReadContent:
							if (readContentAsBinaryHelper.decoder != readContentAsBinaryHelper.base64Decoder)
							{
								goto IL_1D5;
							}
							configuredTaskAwaiter4 = readContentAsBinaryHelper.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBase64Async>d__27>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							goto IL_1B1;
						case ReadContentAsBinaryHelper.State.InReadElementContent:
							throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
						default:
							result = 0;
							goto IL_26E;
						}
						break;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = 0;
						goto IL_26E;
					}
					goto IL_1D5;
					IL_1B1:
					result = configuredTaskAwaiter4.GetResult();
					goto IL_26E;
					IL_1D5:
					readContentAsBinaryHelper.InitBase64Decoder();
					configuredTaskAwaiter4 = readContentAsBinaryHelper.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBase64Async>d__27>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_24B:
					result = configuredTaskAwaiter4.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_26E:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005E3 RID: 1507 RVA: 0x00019C14 File Offset: 0x00017E14
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000362 RID: 866
			public int <>1__state;

			// Token: 0x04000363 RID: 867
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000364 RID: 868
			public byte[] buffer;

			// Token: 0x04000365 RID: 869
			public int count;

			// Token: 0x04000366 RID: 870
			public int index;

			// Token: 0x04000367 RID: 871
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x04000368 RID: 872
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000369 RID: 873
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020000B3 RID: 179
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBinHexAsync>d__28 : IAsyncStateMachine
		{
			// Token: 0x060005E4 RID: 1508 RVA: 0x00019C24 File Offset: 0x00017E24
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1B1;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_24B;
					}
					default:
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						switch (readContentAsBinaryHelper.state)
						{
						case ReadContentAsBinaryHelper.State.None:
							if (!readContentAsBinaryHelper.reader.CanReadContentAs())
							{
								throw readContentAsBinaryHelper.reader.CreateReadContentAsException("ReadContentAsBinHex");
							}
							configuredTaskAwaiter3 = readContentAsBinaryHelper.InitAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBinHexAsync>d__28>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						case ReadContentAsBinaryHelper.State.InReadContent:
							if (readContentAsBinaryHelper.decoder != readContentAsBinaryHelper.binHexDecoder)
							{
								goto IL_1D5;
							}
							configuredTaskAwaiter4 = readContentAsBinaryHelper.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBinHexAsync>d__28>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							goto IL_1B1;
						case ReadContentAsBinaryHelper.State.InReadElementContent:
							throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
						default:
							result = 0;
							goto IL_26E;
						}
						break;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = 0;
						goto IL_26E;
					}
					goto IL_1D5;
					IL_1B1:
					result = configuredTaskAwaiter4.GetResult();
					goto IL_26E;
					IL_1D5:
					readContentAsBinaryHelper.InitBinHexDecoder();
					configuredTaskAwaiter4 = readContentAsBinaryHelper.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBinHexAsync>d__28>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_24B:
					result = configuredTaskAwaiter4.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_26E:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005E5 RID: 1509 RVA: 0x00019ED0 File Offset: 0x000180D0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400036A RID: 874
			public int <>1__state;

			// Token: 0x0400036B RID: 875
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400036C RID: 876
			public byte[] buffer;

			// Token: 0x0400036D RID: 877
			public int count;

			// Token: 0x0400036E RID: 878
			public int index;

			// Token: 0x0400036F RID: 879
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x04000370 RID: 880
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000371 RID: 881
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020000B4 RID: 180
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBase64Async>d__29 : IAsyncStateMachine
		{
			// Token: 0x060005E6 RID: 1510 RVA: 0x00019EE0 File Offset: 0x000180E0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C2;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_24C;
					}
					default:
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						switch (readContentAsBinaryHelper.state)
						{
						case ReadContentAsBinaryHelper.State.None:
							if (readContentAsBinaryHelper.reader.NodeType != XmlNodeType.Element)
							{
								throw readContentAsBinaryHelper.reader.CreateReadElementContentAsException("ReadElementContentAsBase64");
							}
							configuredTaskAwaiter3 = readContentAsBinaryHelper.InitOnElementAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadElementContentAsBase64Async>d__29>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						case ReadContentAsBinaryHelper.State.InReadContent:
							throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
						case ReadContentAsBinaryHelper.State.InReadElementContent:
							if (readContentAsBinaryHelper.decoder != readContentAsBinaryHelper.base64Decoder)
							{
								goto IL_1D6;
							}
							configuredTaskAwaiter4 = readContentAsBinaryHelper.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadElementContentAsBase64Async>d__29>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							goto IL_1C2;
						default:
							result = 0;
							goto IL_26F;
						}
						break;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = 0;
						goto IL_26F;
					}
					goto IL_1D6;
					IL_1C2:
					result = configuredTaskAwaiter4.GetResult();
					goto IL_26F;
					IL_1D6:
					readContentAsBinaryHelper.InitBase64Decoder();
					configuredTaskAwaiter4 = readContentAsBinaryHelper.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadElementContentAsBase64Async>d__29>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_24C:
					result = configuredTaskAwaiter4.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_26F:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005E7 RID: 1511 RVA: 0x0001A18C File Offset: 0x0001838C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000372 RID: 882
			public int <>1__state;

			// Token: 0x04000373 RID: 883
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000374 RID: 884
			public byte[] buffer;

			// Token: 0x04000375 RID: 885
			public int count;

			// Token: 0x04000376 RID: 886
			public int index;

			// Token: 0x04000377 RID: 887
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x04000378 RID: 888
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000379 RID: 889
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020000B5 RID: 181
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBinHexAsync>d__30 : IAsyncStateMachine
		{
			// Token: 0x060005E8 RID: 1512 RVA: 0x0001A19C File Offset: 0x0001839C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C2;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_24C;
					}
					default:
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						switch (readContentAsBinaryHelper.state)
						{
						case ReadContentAsBinaryHelper.State.None:
							if (readContentAsBinaryHelper.reader.NodeType != XmlNodeType.Element)
							{
								throw readContentAsBinaryHelper.reader.CreateReadElementContentAsException("ReadElementContentAsBinHex");
							}
							configuredTaskAwaiter3 = readContentAsBinaryHelper.InitOnElementAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadElementContentAsBinHexAsync>d__30>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						case ReadContentAsBinaryHelper.State.InReadContent:
							throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
						case ReadContentAsBinaryHelper.State.InReadElementContent:
							if (readContentAsBinaryHelper.decoder != readContentAsBinaryHelper.binHexDecoder)
							{
								goto IL_1D6;
							}
							configuredTaskAwaiter4 = readContentAsBinaryHelper.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadElementContentAsBinHexAsync>d__30>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							goto IL_1C2;
						default:
							result = 0;
							goto IL_26F;
						}
						break;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = 0;
						goto IL_26F;
					}
					goto IL_1D6;
					IL_1C2:
					result = configuredTaskAwaiter4.GetResult();
					goto IL_26F;
					IL_1D6:
					readContentAsBinaryHelper.InitBinHexDecoder();
					configuredTaskAwaiter4 = readContentAsBinaryHelper.ReadElementContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadElementContentAsBinHexAsync>d__30>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_24C:
					result = configuredTaskAwaiter4.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_26F:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005E9 RID: 1513 RVA: 0x0001A448 File Offset: 0x00018648
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400037A RID: 890
			public int <>1__state;

			// Token: 0x0400037B RID: 891
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400037C RID: 892
			public byte[] buffer;

			// Token: 0x0400037D RID: 893
			public int count;

			// Token: 0x0400037E RID: 894
			public int index;

			// Token: 0x0400037F RID: 895
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x04000380 RID: 896
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000381 RID: 897
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020000B6 RID: 182
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishAsync>d__31 : IAsyncStateMachine
		{
			// Token: 0x060005EA RID: 1514 RVA: 0x0001A458 File Offset: 0x00018658
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_81;
					}
					if (num == 1)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_134;
					}
					if (readContentAsBinaryHelper.state == ReadContentAsBinaryHelper.State.None)
					{
						goto IL_13C;
					}
					IL_23:
					configuredTaskAwaiter3 = readContentAsBinaryHelper.MoveToNextContentNodeAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<FinishAsync>d__31>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_81:
					if (configuredTaskAwaiter3.GetResult())
					{
						goto IL_23;
					}
					if (readContentAsBinaryHelper.state != ReadContentAsBinaryHelper.State.InReadElementContent)
					{
						goto IL_13C;
					}
					if (readContentAsBinaryHelper.reader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", readContentAsBinaryHelper.reader.NodeType.ToString(), readContentAsBinaryHelper.reader as IXmlLineInfo);
					}
					configuredTaskAwaiter3 = readContentAsBinaryHelper.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<FinishAsync>d__31>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_134:
					configuredTaskAwaiter3.GetResult();
					IL_13C:
					readContentAsBinaryHelper.Reset();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x060005EB RID: 1515 RVA: 0x0001A5F4 File Offset: 0x000187F4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000382 RID: 898
			public int <>1__state;

			// Token: 0x04000383 RID: 899
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000384 RID: 900
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x04000385 RID: 901
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000B7 RID: 183
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InitAsync>d__32 : IAsyncStateMachine
		{
			// Token: 0x060005EC RID: 1516 RVA: 0x0001A604 File Offset: 0x00018804
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						configuredTaskAwaiter3 = readContentAsBinaryHelper.MoveToNextContentNodeAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<InitAsync>d__32>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = false;
					}
					else
					{
						readContentAsBinaryHelper.state = ReadContentAsBinaryHelper.State.InReadContent;
						readContentAsBinaryHelper.isEnd = false;
						result = true;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005ED RID: 1517 RVA: 0x0001A6DC File Offset: 0x000188DC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000386 RID: 902
			public int <>1__state;

			// Token: 0x04000387 RID: 903
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000388 RID: 904
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x04000389 RID: 905
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000B8 RID: 184
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InitOnElementAsync>d__33 : IAsyncStateMachine
		{
			// Token: 0x060005EE RID: 1518 RVA: 0x0001A6EC File Offset: 0x000188EC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_10A;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1B5;
					default:
						isEmpty = readContentAsBinaryHelper.reader.IsEmptyElement;
						configuredTaskAwaiter3 = readContentAsBinaryHelper.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<InitOnElementAsync>d__33>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter3.GetResult();
					if (isEmpty)
					{
						result = false;
						goto IL_1EC;
					}
					configuredTaskAwaiter3 = readContentAsBinaryHelper.MoveToNextContentNodeAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<InitOnElementAsync>d__33>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_10A:
					if (configuredTaskAwaiter3.GetResult())
					{
						readContentAsBinaryHelper.state = ReadContentAsBinaryHelper.State.InReadElementContent;
						readContentAsBinaryHelper.isEnd = false;
						result = true;
						goto IL_1EC;
					}
					if (readContentAsBinaryHelper.reader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", readContentAsBinaryHelper.reader.NodeType.ToString(), readContentAsBinaryHelper.reader as IXmlLineInfo);
					}
					configuredTaskAwaiter3 = readContentAsBinaryHelper.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<InitOnElementAsync>d__33>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_1B5:
					configuredTaskAwaiter3.GetResult();
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_1EC:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005EF RID: 1519 RVA: 0x0001A918 File Offset: 0x00018B18
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400038A RID: 906
			public int <>1__state;

			// Token: 0x0400038B RID: 907
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x0400038C RID: 908
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x0400038D RID: 909
			private bool <isEmpty>5__1;

			// Token: 0x0400038E RID: 910
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000B9 RID: 185
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBinaryAsync>d__34 : IAsyncStateMachine
		{
			// Token: 0x060005F0 RID: 1520 RVA: 0x0001A928 File Offset: 0x00018B28
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_12E;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1BB;
					}
					case 2:
						configuredTaskAwaiter7 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_27A;
					default:
						if (readContentAsBinaryHelper.isEnd)
						{
							readContentAsBinaryHelper.Reset();
							result = 0;
							goto IL_2B4;
						}
						readContentAsBinaryHelper.decoder.SetNextOutputBuffer(buffer, index, count);
						break;
					}
					IL_52:
					if (!readContentAsBinaryHelper.canReadValueChunk)
					{
						configuredTaskAwaiter5 = readContentAsBinaryHelper.reader.GetValueAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBinaryAsync>d__34>(ref configuredTaskAwaiter5, ref this);
							return;
						}
						goto IL_1BB;
					}
					IL_5D:
					if (readContentAsBinaryHelper.valueOffset < readContentAsBinaryHelper.valueChunkLength)
					{
						int num3 = readContentAsBinaryHelper.decoder.Decode(readContentAsBinaryHelper.valueChunk, readContentAsBinaryHelper.valueOffset, readContentAsBinaryHelper.valueChunkLength - readContentAsBinaryHelper.valueOffset);
						readContentAsBinaryHelper.valueOffset += num3;
					}
					if (readContentAsBinaryHelper.decoder.IsFull)
					{
						result = readContentAsBinaryHelper.decoder.DecodedCount;
						goto IL_2B4;
					}
					configuredTaskAwaiter3 = readContentAsBinaryHelper.reader.ReadValueChunkAsync(readContentAsBinaryHelper.valueChunk, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBinaryAsync>d__34>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_12E:
					int result2 = configuredTaskAwaiter3.GetResult();
					if ((readContentAsBinaryHelper.valueChunkLength = result2) != 0)
					{
						readContentAsBinaryHelper.valueOffset = 0;
						goto IL_5D;
					}
					goto IL_214;
					IL_1BB:
					string result3 = configuredTaskAwaiter5.GetResult();
					int num4 = readContentAsBinaryHelper.decoder.Decode(result3, readContentAsBinaryHelper.valueOffset, result3.Length - readContentAsBinaryHelper.valueOffset);
					readContentAsBinaryHelper.valueOffset += num4;
					if (readContentAsBinaryHelper.decoder.IsFull)
					{
						result = readContentAsBinaryHelper.decoder.DecodedCount;
						goto IL_2B4;
					}
					IL_214:
					readContentAsBinaryHelper.valueOffset = 0;
					configuredTaskAwaiter7 = readContentAsBinaryHelper.MoveToNextContentNodeAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter7.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter7;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadContentAsBinaryAsync>d__34>(ref configuredTaskAwaiter7, ref this);
						return;
					}
					IL_27A:
					if (configuredTaskAwaiter7.GetResult())
					{
						goto IL_52;
					}
					readContentAsBinaryHelper.isEnd = true;
					result = readContentAsBinaryHelper.decoder.DecodedCount;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2B4:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005F1 RID: 1521 RVA: 0x0001AC1C File Offset: 0x00018E1C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400038F RID: 911
			public int <>1__state;

			// Token: 0x04000390 RID: 912
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000391 RID: 913
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x04000392 RID: 914
			public byte[] buffer;

			// Token: 0x04000393 RID: 915
			public int index;

			// Token: 0x04000394 RID: 916
			public int count;

			// Token: 0x04000395 RID: 917
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000396 RID: 918
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x04000397 RID: 919
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x020000BA RID: 186
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBinaryAsync>d__35 : IAsyncStateMachine
		{
			// Token: 0x060005F2 RID: 1522 RVA: 0x0001AC2C File Offset: 0x00018E2C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_14F;
						}
						if (count == 0)
						{
							result = 0;
							goto IL_17B;
						}
						configuredTaskAwaiter3 = readContentAsBinaryHelper.ReadContentAsBinaryAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadElementContentAsBinaryAsync>d__35>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter3.GetResult();
					if (result2 > 0)
					{
						result = result2;
						goto IL_17B;
					}
					if (readContentAsBinaryHelper.reader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", readContentAsBinaryHelper.reader.NodeType.ToString(), readContentAsBinaryHelper.reader as IXmlLineInfo);
					}
					configuredTaskAwaiter = readContentAsBinaryHelper.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<ReadElementContentAsBinaryAsync>d__35>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_14F:
					configuredTaskAwaiter.GetResult();
					readContentAsBinaryHelper.state = ReadContentAsBinaryHelper.State.None;
					result = 0;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_17B:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005F3 RID: 1523 RVA: 0x0001ADE4 File Offset: 0x00018FE4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000398 RID: 920
			public int <>1__state;

			// Token: 0x04000399 RID: 921
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400039A RID: 922
			public int count;

			// Token: 0x0400039B RID: 923
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x0400039C RID: 924
			public byte[] buffer;

			// Token: 0x0400039D RID: 925
			public int index;

			// Token: 0x0400039E RID: 926
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400039F RID: 927
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020000BB RID: 187
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <MoveToNextContentNodeAsync>d__36 : IAsyncStateMachine
		{
			// Token: 0x060005F4 RID: 1524 RVA: 0x0001ADF4 File Offset: 0x00018FF4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				ReadContentAsBinaryHelper readContentAsBinaryHelper = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_10F;
					}
					IL_14:
					switch (readContentAsBinaryHelper.reader.NodeType)
					{
					case XmlNodeType.Attribute:
						result = !moveIfOnContentNode;
						goto IL_138;
					case XmlNodeType.Text:
					case XmlNodeType.CDATA:
					case XmlNodeType.Whitespace:
					case XmlNodeType.SignificantWhitespace:
						if (!moveIfOnContentNode)
						{
							result = true;
							goto IL_138;
						}
						goto IL_A5;
					case XmlNodeType.EntityReference:
						if (readContentAsBinaryHelper.reader.CanResolveEntity)
						{
							readContentAsBinaryHelper.reader.ResolveEntity();
							goto IL_A5;
						}
						break;
					case XmlNodeType.ProcessingInstruction:
					case XmlNodeType.Comment:
					case XmlNodeType.EndEntity:
						goto IL_A5;
					}
					result = false;
					goto IL_138;
					IL_A5:
					moveIfOnContentNode = false;
					configuredTaskAwaiter3 = readContentAsBinaryHelper.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, ReadContentAsBinaryHelper.<MoveToNextContentNodeAsync>d__36>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_10F:
					if (configuredTaskAwaiter3.GetResult())
					{
						goto IL_14;
					}
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_138:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060005F5 RID: 1525 RVA: 0x0001AF6C File Offset: 0x0001916C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040003A0 RID: 928
			public int <>1__state;

			// Token: 0x040003A1 RID: 929
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040003A2 RID: 930
			public ReadContentAsBinaryHelper <>4__this;

			// Token: 0x040003A3 RID: 931
			public bool moveIfOnContentNode;

			// Token: 0x040003A4 RID: 932
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
