﻿using System;
using System.Collections;

namespace System.Xml
{
	// Token: 0x0200021F RID: 543
	internal sealed class XmlChildEnumerator : IEnumerator
	{
		// Token: 0x0600139D RID: 5021 RVA: 0x00072A8D File Offset: 0x00070C8D
		internal XmlChildEnumerator(XmlNode container)
		{
			this.container = container;
			this.child = container.FirstChild;
			this.isFirst = true;
		}

		// Token: 0x0600139E RID: 5022 RVA: 0x00072AAF File Offset: 0x00070CAF
		bool IEnumerator.MoveNext()
		{
			return this.MoveNext();
		}

		// Token: 0x0600139F RID: 5023 RVA: 0x00072AB8 File Offset: 0x00070CB8
		internal bool MoveNext()
		{
			if (this.isFirst)
			{
				this.child = this.container.FirstChild;
				this.isFirst = false;
			}
			else if (this.child != null)
			{
				this.child = this.child.NextSibling;
			}
			return this.child != null;
		}

		// Token: 0x060013A0 RID: 5024 RVA: 0x00072B09 File Offset: 0x00070D09
		void IEnumerator.Reset()
		{
			this.isFirst = true;
			this.child = this.container.FirstChild;
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x060013A1 RID: 5025 RVA: 0x00072B23 File Offset: 0x00070D23
		object IEnumerator.Current
		{
			get
			{
				return this.Current;
			}
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x060013A2 RID: 5026 RVA: 0x00072B2B File Offset: 0x00070D2B
		internal XmlNode Current
		{
			get
			{
				if (this.isFirst || this.child == null)
				{
					throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
				}
				return this.child;
			}
		}

		// Token: 0x04000D7E RID: 3454
		internal XmlNode container;

		// Token: 0x04000D7F RID: 3455
		internal XmlNode child;

		// Token: 0x04000D80 RID: 3456
		internal bool isFirst;
	}
}
