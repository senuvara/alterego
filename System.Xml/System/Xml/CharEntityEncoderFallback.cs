﻿using System;
using System.Text;

namespace System.Xml
{
	// Token: 0x02000093 RID: 147
	internal class CharEntityEncoderFallback : EncoderFallback
	{
		// Token: 0x060004D0 RID: 1232 RVA: 0x000164A1 File Offset: 0x000146A1
		internal CharEntityEncoderFallback()
		{
		}

		// Token: 0x060004D1 RID: 1233 RVA: 0x000164A9 File Offset: 0x000146A9
		public override EncoderFallbackBuffer CreateFallbackBuffer()
		{
			if (this.fallbackBuffer == null)
			{
				this.fallbackBuffer = new CharEntityEncoderFallbackBuffer(this);
			}
			return this.fallbackBuffer;
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x060004D2 RID: 1234 RVA: 0x000164C5 File Offset: 0x000146C5
		public override int MaxCharCount
		{
			get
			{
				return 12;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x000164C9 File Offset: 0x000146C9
		// (set) Token: 0x060004D4 RID: 1236 RVA: 0x000164D1 File Offset: 0x000146D1
		internal int StartOffset
		{
			get
			{
				return this.startOffset;
			}
			set
			{
				this.startOffset = value;
			}
		}

		// Token: 0x060004D5 RID: 1237 RVA: 0x000164DA File Offset: 0x000146DA
		internal void Reset(int[] textContentMarks, int endMarkPos)
		{
			this.textContentMarks = textContentMarks;
			this.endMarkPos = endMarkPos;
			this.curMarkPos = 0;
		}

		// Token: 0x060004D6 RID: 1238 RVA: 0x000164F4 File Offset: 0x000146F4
		internal bool CanReplaceAt(int index)
		{
			int num = this.curMarkPos;
			int num2 = this.startOffset + index;
			while (num < this.endMarkPos && num2 >= this.textContentMarks[num + 1])
			{
				num++;
			}
			this.curMarkPos = num;
			return (num & 1) != 0;
		}

		// Token: 0x04000301 RID: 769
		private CharEntityEncoderFallbackBuffer fallbackBuffer;

		// Token: 0x04000302 RID: 770
		private int[] textContentMarks;

		// Token: 0x04000303 RID: 771
		private int endMarkPos;

		// Token: 0x04000304 RID: 772
		private int curMarkPos;

		// Token: 0x04000305 RID: 773
		private int startOffset;
	}
}
