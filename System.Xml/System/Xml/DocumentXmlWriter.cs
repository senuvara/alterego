﻿using System;
using System.Collections.Generic;

namespace System.Xml
{
	// Token: 0x02000215 RID: 533
	internal sealed class DocumentXmlWriter : XmlRawWriter, IXmlNamespaceResolver
	{
		// Token: 0x060012FB RID: 4859 RVA: 0x00070B9C File Offset: 0x0006ED9C
		public DocumentXmlWriter(DocumentXmlWriterType type, XmlNode start, XmlDocument document)
		{
			this.type = type;
			this.start = start;
			this.document = document;
			this.state = this.StartState();
			this.fragment = new List<XmlNode>();
			this.settings = new XmlWriterSettings();
			this.settings.ReadOnly = false;
			this.settings.CheckCharacters = false;
			this.settings.CloseOutput = false;
			this.settings.ConformanceLevel = ((this.state == DocumentXmlWriter.State.Prolog) ? ConformanceLevel.Document : ConformanceLevel.Fragment);
			this.settings.ReadOnly = true;
		}

		// Token: 0x17000348 RID: 840
		// (set) Token: 0x060012FC RID: 4860 RVA: 0x00070C2E File Offset: 0x0006EE2E
		public XmlNamespaceManager NamespaceManager
		{
			set
			{
				this.namespaceManager = value;
			}
		}

		// Token: 0x17000349 RID: 841
		// (get) Token: 0x060012FD RID: 4861 RVA: 0x00070C37 File Offset: 0x0006EE37
		public override XmlWriterSettings Settings
		{
			get
			{
				return this.settings;
			}
		}

		// Token: 0x060012FE RID: 4862 RVA: 0x00070C3F File Offset: 0x0006EE3F
		internal void SetSettings(XmlWriterSettings value)
		{
			this.settings = value;
		}

		// Token: 0x1700034A RID: 842
		// (set) Token: 0x060012FF RID: 4863 RVA: 0x00070C48 File Offset: 0x0006EE48
		public DocumentXPathNavigator Navigator
		{
			set
			{
				this.navigator = value;
			}
		}

		// Token: 0x1700034B RID: 843
		// (set) Token: 0x06001300 RID: 4864 RVA: 0x00070C51 File Offset: 0x0006EE51
		public XmlNode EndNode
		{
			set
			{
				this.end = value;
			}
		}

		// Token: 0x06001301 RID: 4865 RVA: 0x00070C5C File Offset: 0x0006EE5C
		internal override void WriteXmlDeclaration(XmlStandalone standalone)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteXmlDeclaration);
			if (standalone != XmlStandalone.Omit)
			{
				XmlNode node = this.document.CreateXmlDeclaration("1.0", string.Empty, (standalone == XmlStandalone.Yes) ? "yes" : "no");
				this.AddChild(node, this.write);
			}
		}

		// Token: 0x06001302 RID: 4866 RVA: 0x00070CA8 File Offset: 0x0006EEA8
		internal override void WriteXmlDeclaration(string xmldecl)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteXmlDeclaration);
			string version;
			string encoding;
			string standalone;
			XmlLoader.ParseXmlDeclarationValue(xmldecl, out version, out encoding, out standalone);
			XmlNode node = this.document.CreateXmlDeclaration(version, encoding, standalone);
			this.AddChild(node, this.write);
		}

		// Token: 0x06001303 RID: 4867 RVA: 0x00070CE4 File Offset: 0x0006EEE4
		public override void WriteStartDocument()
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteStartDocument);
		}

		// Token: 0x06001304 RID: 4868 RVA: 0x00070CE4 File Offset: 0x0006EEE4
		public override void WriteStartDocument(bool standalone)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteStartDocument);
		}

		// Token: 0x06001305 RID: 4869 RVA: 0x00070CED File Offset: 0x0006EEED
		public override void WriteEndDocument()
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteEndDocument);
		}

		// Token: 0x06001306 RID: 4870 RVA: 0x00070CF8 File Offset: 0x0006EEF8
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteDocType);
			XmlNode node = this.document.CreateDocumentType(name, pubid, sysid, subset);
			this.AddChild(node, this.write);
		}

		// Token: 0x06001307 RID: 4871 RVA: 0x00070D2C File Offset: 0x0006EF2C
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteStartElement);
			XmlNode node = this.document.CreateElement(prefix, localName, ns);
			this.AddChild(node, this.write);
			this.write = node;
		}

		// Token: 0x06001308 RID: 4872 RVA: 0x00070D63 File Offset: 0x0006EF63
		public override void WriteEndElement()
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteEndElement);
			if (this.write == null)
			{
				throw new InvalidOperationException();
			}
			this.write = this.write.ParentNode;
		}

		// Token: 0x06001309 RID: 4873 RVA: 0x00070D8B File Offset: 0x0006EF8B
		internal override void WriteEndElement(string prefix, string localName, string ns)
		{
			this.WriteEndElement();
		}

		// Token: 0x0600130A RID: 4874 RVA: 0x00070D94 File Offset: 0x0006EF94
		public override void WriteFullEndElement()
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteFullEndElement);
			XmlElement xmlElement = this.write as XmlElement;
			if (xmlElement == null)
			{
				throw new InvalidOperationException();
			}
			xmlElement.IsEmpty = false;
			this.write = xmlElement.ParentNode;
		}

		// Token: 0x0600130B RID: 4875 RVA: 0x00070DD0 File Offset: 0x0006EFD0
		internal override void WriteFullEndElement(string prefix, string localName, string ns)
		{
			this.WriteFullEndElement();
		}

		// Token: 0x0600130C RID: 4876 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void StartElementContent()
		{
		}

		// Token: 0x0600130D RID: 4877 RVA: 0x00070DD8 File Offset: 0x0006EFD8
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteStartAttribute);
			XmlAttribute attr = this.document.CreateAttribute(prefix, localName, ns);
			this.AddAttribute(attr, this.write);
			this.write = attr;
		}

		// Token: 0x0600130E RID: 4878 RVA: 0x00070E10 File Offset: 0x0006F010
		public override void WriteEndAttribute()
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteEndAttribute);
			XmlAttribute xmlAttribute = this.write as XmlAttribute;
			if (xmlAttribute == null)
			{
				throw new InvalidOperationException();
			}
			if (!xmlAttribute.HasChildNodes)
			{
				XmlNode node = this.document.CreateTextNode(string.Empty);
				this.AddChild(node, xmlAttribute);
			}
			this.write = xmlAttribute.OwnerElement;
		}

		// Token: 0x0600130F RID: 4879 RVA: 0x00020601 File Offset: 0x0001E801
		internal override void WriteNamespaceDeclaration(string prefix, string ns)
		{
			this.WriteStartNamespaceDeclaration(prefix);
			this.WriteString(ns);
			this.WriteEndNamespaceDeclaration();
		}

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x06001310 RID: 4880 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool SupportsNamespaceDeclarationInChunks
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06001311 RID: 4881 RVA: 0x00070E68 File Offset: 0x0006F068
		internal override void WriteStartNamespaceDeclaration(string prefix)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteStartNamespaceDeclaration);
			XmlAttribute attr;
			if (prefix.Length == 0)
			{
				attr = this.document.CreateAttribute(prefix, this.document.strXmlns, this.document.strReservedXmlns);
			}
			else
			{
				attr = this.document.CreateAttribute(this.document.strXmlns, prefix, this.document.strReservedXmlns);
			}
			this.AddAttribute(attr, this.write);
			this.write = attr;
		}

		// Token: 0x06001312 RID: 4882 RVA: 0x00070EE4 File Offset: 0x0006F0E4
		internal override void WriteEndNamespaceDeclaration()
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteEndNamespaceDeclaration);
			XmlAttribute xmlAttribute = this.write as XmlAttribute;
			if (xmlAttribute == null)
			{
				throw new InvalidOperationException();
			}
			if (!xmlAttribute.HasChildNodes)
			{
				XmlNode node = this.document.CreateTextNode(string.Empty);
				this.AddChild(node, xmlAttribute);
			}
			this.write = xmlAttribute.OwnerElement;
		}

		// Token: 0x06001313 RID: 4883 RVA: 0x00070F3C File Offset: 0x0006F13C
		public override void WriteCData(string text)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteCData);
			XmlConvert.VerifyCharData(text, ExceptionType.ArgumentException);
			XmlNode node = this.document.CreateCDataSection(text);
			this.AddChild(node, this.write);
		}

		// Token: 0x06001314 RID: 4884 RVA: 0x00070F74 File Offset: 0x0006F174
		public override void WriteComment(string text)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteComment);
			XmlConvert.VerifyCharData(text, ExceptionType.ArgumentException);
			XmlNode node = this.document.CreateComment(text);
			this.AddChild(node, this.write);
		}

		// Token: 0x06001315 RID: 4885 RVA: 0x00070FAC File Offset: 0x0006F1AC
		public override void WriteProcessingInstruction(string name, string text)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteProcessingInstruction);
			XmlConvert.VerifyCharData(text, ExceptionType.ArgumentException);
			XmlNode node = this.document.CreateProcessingInstruction(name, text);
			this.AddChild(node, this.write);
		}

		// Token: 0x06001316 RID: 4886 RVA: 0x00070FE4 File Offset: 0x0006F1E4
		public override void WriteEntityRef(string name)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteEntityRef);
			XmlNode node = this.document.CreateEntityReference(name);
			this.AddChild(node, this.write);
		}

		// Token: 0x06001317 RID: 4887 RVA: 0x00071013 File Offset: 0x0006F213
		public override void WriteCharEntity(char ch)
		{
			this.WriteString(new string(ch, 1));
		}

		// Token: 0x06001318 RID: 4888 RVA: 0x00071024 File Offset: 0x0006F224
		public override void WriteWhitespace(string text)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteWhitespace);
			XmlConvert.VerifyCharData(text, ExceptionType.ArgumentException);
			if (this.document.PreserveWhitespace)
			{
				XmlNode node = this.document.CreateWhitespace(text);
				this.AddChild(node, this.write);
			}
		}

		// Token: 0x06001319 RID: 4889 RVA: 0x00071068 File Offset: 0x0006F268
		public override void WriteString(string text)
		{
			this.VerifyState(DocumentXmlWriter.Method.WriteString);
			XmlConvert.VerifyCharData(text, ExceptionType.ArgumentException);
			XmlNode node = this.document.CreateTextNode(text);
			this.AddChild(node, this.write);
		}

		// Token: 0x0600131A RID: 4890 RVA: 0x0007109E File Offset: 0x0006F29E
		public override void WriteSurrogateCharEntity(char lowCh, char highCh)
		{
			this.WriteString(new string(new char[]
			{
				highCh,
				lowCh
			}));
		}

		// Token: 0x0600131B RID: 4891 RVA: 0x0001CACF File Offset: 0x0001ACCF
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.WriteString(new string(buffer, index, count));
		}

		// Token: 0x0600131C RID: 4892 RVA: 0x0001CACF File Offset: 0x0001ACCF
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.WriteString(new string(buffer, index, count));
		}

		// Token: 0x0600131D RID: 4893 RVA: 0x00028DF6 File Offset: 0x00026FF6
		public override void WriteRaw(string data)
		{
			this.WriteString(data);
		}

		// Token: 0x0600131E RID: 4894 RVA: 0x000030EC File Offset: 0x000012EC
		public override void Close()
		{
		}

		// Token: 0x0600131F RID: 4895 RVA: 0x000710BC File Offset: 0x0006F2BC
		internal override void Close(WriteState currentState)
		{
			if (currentState == WriteState.Error)
			{
				return;
			}
			try
			{
				switch (this.type)
				{
				case DocumentXmlWriterType.InsertSiblingAfter:
				{
					XmlNode parentNode = this.start.ParentNode;
					if (parentNode == null)
					{
						throw new InvalidOperationException(Res.GetString("The current position of the navigator is missing a valid parent."));
					}
					for (int i = this.fragment.Count - 1; i >= 0; i--)
					{
						parentNode.InsertAfter(this.fragment[i], this.start);
					}
					break;
				}
				case DocumentXmlWriterType.InsertSiblingBefore:
				{
					XmlNode parentNode = this.start.ParentNode;
					if (parentNode == null)
					{
						throw new InvalidOperationException(Res.GetString("The current position of the navigator is missing a valid parent."));
					}
					for (int j = 0; j < this.fragment.Count; j++)
					{
						parentNode.InsertBefore(this.fragment[j], this.start);
					}
					break;
				}
				case DocumentXmlWriterType.PrependChild:
					for (int k = this.fragment.Count - 1; k >= 0; k--)
					{
						this.start.PrependChild(this.fragment[k]);
					}
					break;
				case DocumentXmlWriterType.AppendChild:
					for (int l = 0; l < this.fragment.Count; l++)
					{
						this.start.AppendChild(this.fragment[l]);
					}
					break;
				case DocumentXmlWriterType.AppendAttribute:
					this.CloseWithAppendAttribute();
					break;
				case DocumentXmlWriterType.ReplaceToFollowingSibling:
					if (this.fragment.Count == 0)
					{
						throw new InvalidOperationException(Res.GetString("No content generated as the result of the operation."));
					}
					this.CloseWithReplaceToFollowingSibling();
					break;
				}
			}
			finally
			{
				this.fragment.Clear();
			}
		}

		// Token: 0x06001320 RID: 4896 RVA: 0x00071264 File Offset: 0x0006F464
		private void CloseWithAppendAttribute()
		{
			XmlAttributeCollection attributes = (this.start as XmlElement).Attributes;
			for (int i = 0; i < this.fragment.Count; i++)
			{
				XmlAttribute xmlAttribute = this.fragment[i] as XmlAttribute;
				int num = attributes.FindNodeOffsetNS(xmlAttribute);
				if (num != -1 && ((XmlAttribute)attributes.nodes[num]).Specified)
				{
					throw new XmlException("'{0}' is a duplicate attribute name.", (xmlAttribute.Prefix.Length == 0) ? xmlAttribute.LocalName : (xmlAttribute.Prefix + ":" + xmlAttribute.LocalName));
				}
			}
			for (int j = 0; j < this.fragment.Count; j++)
			{
				XmlAttribute node = this.fragment[j] as XmlAttribute;
				attributes.Append(node);
			}
		}

		// Token: 0x06001321 RID: 4897 RVA: 0x0007133C File Offset: 0x0006F53C
		private void CloseWithReplaceToFollowingSibling()
		{
			XmlNode parentNode = this.start.ParentNode;
			if (parentNode == null)
			{
				throw new InvalidOperationException(Res.GetString("The current position of the navigator is missing a valid parent."));
			}
			if (this.start != this.end)
			{
				if (!DocumentXPathNavigator.IsFollowingSibling(this.start, this.end))
				{
					throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current position of the navigator."));
				}
				if (this.start.IsReadOnly)
				{
					throw new InvalidOperationException(Res.GetString("This node is read-only. It cannot be modified."));
				}
				DocumentXPathNavigator.DeleteToFollowingSibling(this.start.NextSibling, this.end);
			}
			XmlNode xmlNode = this.fragment[0];
			parentNode.ReplaceChild(xmlNode, this.start);
			for (int i = this.fragment.Count - 1; i >= 1; i--)
			{
				parentNode.InsertAfter(this.fragment[i], xmlNode);
			}
			this.navigator.ResetPosition(xmlNode);
		}

		// Token: 0x06001322 RID: 4898 RVA: 0x000030EC File Offset: 0x000012EC
		public override void Flush()
		{
		}

		// Token: 0x06001323 RID: 4899 RVA: 0x0007141F File Offset: 0x0006F61F
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return this.namespaceManager.GetNamespacesInScope(scope);
		}

		// Token: 0x06001324 RID: 4900 RVA: 0x0007142D File Offset: 0x0006F62D
		string IXmlNamespaceResolver.LookupNamespace(string prefix)
		{
			return this.namespaceManager.LookupNamespace(prefix);
		}

		// Token: 0x06001325 RID: 4901 RVA: 0x0007143B File Offset: 0x0006F63B
		string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
		{
			return this.namespaceManager.LookupPrefix(namespaceName);
		}

		// Token: 0x06001326 RID: 4902 RVA: 0x00071449 File Offset: 0x0006F649
		private void AddAttribute(XmlAttribute attr, XmlNode parent)
		{
			if (parent == null)
			{
				this.fragment.Add(attr);
				return;
			}
			XmlElement xmlElement = parent as XmlElement;
			if (xmlElement == null)
			{
				throw new InvalidOperationException();
			}
			xmlElement.Attributes.Append(attr);
		}

		// Token: 0x06001327 RID: 4903 RVA: 0x00071476 File Offset: 0x0006F676
		private void AddChild(XmlNode node, XmlNode parent)
		{
			if (parent == null)
			{
				this.fragment.Add(node);
				return;
			}
			parent.AppendChild(node);
		}

		// Token: 0x06001328 RID: 4904 RVA: 0x00071490 File Offset: 0x0006F690
		private DocumentXmlWriter.State StartState()
		{
			XmlNodeType xmlNodeType = XmlNodeType.None;
			switch (this.type)
			{
			case DocumentXmlWriterType.InsertSiblingAfter:
			case DocumentXmlWriterType.InsertSiblingBefore:
			{
				XmlNode parentNode = this.start.ParentNode;
				if (parentNode != null)
				{
					xmlNodeType = parentNode.NodeType;
				}
				if (xmlNodeType == XmlNodeType.Document)
				{
					return DocumentXmlWriter.State.Prolog;
				}
				if (xmlNodeType == XmlNodeType.DocumentFragment)
				{
					return DocumentXmlWriter.State.Fragment;
				}
				break;
			}
			case DocumentXmlWriterType.PrependChild:
			case DocumentXmlWriterType.AppendChild:
				xmlNodeType = this.start.NodeType;
				if (xmlNodeType == XmlNodeType.Document)
				{
					return DocumentXmlWriter.State.Prolog;
				}
				if (xmlNodeType == XmlNodeType.DocumentFragment)
				{
					return DocumentXmlWriter.State.Fragment;
				}
				break;
			case DocumentXmlWriterType.AppendAttribute:
				return DocumentXmlWriter.State.Attribute;
			}
			return DocumentXmlWriter.State.Content;
		}

		// Token: 0x06001329 RID: 4905 RVA: 0x00071507 File Offset: 0x0006F707
		private void VerifyState(DocumentXmlWriter.Method method)
		{
			this.state = DocumentXmlWriter.changeState[(int)(method * DocumentXmlWriter.Method.WriteEndElement + (int)this.state)];
			if (this.state == DocumentXmlWriter.State.Error)
			{
				throw new InvalidOperationException(Res.GetString("The Writer is closed or in error state."));
			}
		}

		// Token: 0x0600132A RID: 4906 RVA: 0x00071537 File Offset: 0x0006F737
		// Note: this type is marked as 'beforefieldinit'.
		static DocumentXmlWriter()
		{
		}

		// Token: 0x04000D4A RID: 3402
		private DocumentXmlWriterType type;

		// Token: 0x04000D4B RID: 3403
		private XmlNode start;

		// Token: 0x04000D4C RID: 3404
		private XmlDocument document;

		// Token: 0x04000D4D RID: 3405
		private XmlNamespaceManager namespaceManager;

		// Token: 0x04000D4E RID: 3406
		private DocumentXmlWriter.State state;

		// Token: 0x04000D4F RID: 3407
		private XmlNode write;

		// Token: 0x04000D50 RID: 3408
		private List<XmlNode> fragment;

		// Token: 0x04000D51 RID: 3409
		private XmlWriterSettings settings;

		// Token: 0x04000D52 RID: 3410
		private DocumentXPathNavigator navigator;

		// Token: 0x04000D53 RID: 3411
		private XmlNode end;

		// Token: 0x04000D54 RID: 3412
		private static DocumentXmlWriter.State[] changeState = new DocumentXmlWriter.State[]
		{
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Prolog,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Prolog,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Prolog,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Prolog,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Prolog,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Error,
			DocumentXmlWriter.State.Content,
			DocumentXmlWriter.State.Content
		};

		// Token: 0x02000216 RID: 534
		private enum State
		{
			// Token: 0x04000D56 RID: 3414
			Error,
			// Token: 0x04000D57 RID: 3415
			Attribute,
			// Token: 0x04000D58 RID: 3416
			Prolog,
			// Token: 0x04000D59 RID: 3417
			Fragment,
			// Token: 0x04000D5A RID: 3418
			Content,
			// Token: 0x04000D5B RID: 3419
			Last
		}

		// Token: 0x02000217 RID: 535
		private enum Method
		{
			// Token: 0x04000D5D RID: 3421
			WriteXmlDeclaration,
			// Token: 0x04000D5E RID: 3422
			WriteStartDocument,
			// Token: 0x04000D5F RID: 3423
			WriteEndDocument,
			// Token: 0x04000D60 RID: 3424
			WriteDocType,
			// Token: 0x04000D61 RID: 3425
			WriteStartElement,
			// Token: 0x04000D62 RID: 3426
			WriteEndElement,
			// Token: 0x04000D63 RID: 3427
			WriteFullEndElement,
			// Token: 0x04000D64 RID: 3428
			WriteStartAttribute,
			// Token: 0x04000D65 RID: 3429
			WriteEndAttribute,
			// Token: 0x04000D66 RID: 3430
			WriteStartNamespaceDeclaration,
			// Token: 0x04000D67 RID: 3431
			WriteEndNamespaceDeclaration,
			// Token: 0x04000D68 RID: 3432
			WriteCData,
			// Token: 0x04000D69 RID: 3433
			WriteComment,
			// Token: 0x04000D6A RID: 3434
			WriteProcessingInstruction,
			// Token: 0x04000D6B RID: 3435
			WriteEntityRef,
			// Token: 0x04000D6C RID: 3436
			WriteWhitespace,
			// Token: 0x04000D6D RID: 3437
			WriteString
		}
	}
}
