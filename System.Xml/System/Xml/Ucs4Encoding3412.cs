﻿using System;

namespace System.Xml
{
	// Token: 0x0200029C RID: 668
	internal class Ucs4Encoding3412 : Ucs4Encoding
	{
		// Token: 0x060018A5 RID: 6309 RVA: 0x0008E8BA File Offset: 0x0008CABA
		public Ucs4Encoding3412()
		{
			this.ucs4Decoder = new Ucs4Decoder3412();
		}

		// Token: 0x170004A4 RID: 1188
		// (get) Token: 0x060018A6 RID: 6310 RVA: 0x0008E8CD File Offset: 0x0008CACD
		public override string EncodingName
		{
			get
			{
				return "ucs-4 (order 3412)";
			}
		}

		// Token: 0x060018A7 RID: 6311 RVA: 0x0008E8D4 File Offset: 0x0008CAD4
		public override byte[] GetPreamble()
		{
			byte[] array = new byte[4];
			array[0] = 254;
			array[1] = byte.MaxValue;
			return array;
		}
	}
}
