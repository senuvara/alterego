﻿using System;
using System.Xml.Schema;

namespace System.Xml
{
	// Token: 0x02000233 RID: 563
	internal sealed class XmlNameEx : XmlName
	{
		// Token: 0x06001521 RID: 5409 RVA: 0x00077A94 File Offset: 0x00075C94
		internal XmlNameEx(string prefix, string localName, string ns, int hashCode, XmlDocument ownerDoc, XmlName next, IXmlSchemaInfo schemaInfo) : base(prefix, localName, ns, hashCode, ownerDoc, next)
		{
			this.SetValidity(schemaInfo.Validity);
			this.SetIsDefault(schemaInfo.IsDefault);
			this.SetIsNil(schemaInfo.IsNil);
			this.memberType = schemaInfo.MemberType;
			this.schemaType = schemaInfo.SchemaType;
			this.decl = ((schemaInfo.SchemaElement != null) ? schemaInfo.SchemaElement : schemaInfo.SchemaAttribute);
		}

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x06001522 RID: 5410 RVA: 0x00077B10 File Offset: 0x00075D10
		public override XmlSchemaValidity Validity
		{
			get
			{
				if (!this.ownerDoc.CanReportValidity)
				{
					return XmlSchemaValidity.NotKnown;
				}
				return (XmlSchemaValidity)(this.flags & 3);
			}
		}

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x06001523 RID: 5411 RVA: 0x00077B29 File Offset: 0x00075D29
		public override bool IsDefault
		{
			get
			{
				return (this.flags & 4) > 0;
			}
		}

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x06001524 RID: 5412 RVA: 0x00077B36 File Offset: 0x00075D36
		public override bool IsNil
		{
			get
			{
				return (this.flags & 8) > 0;
			}
		}

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x06001525 RID: 5413 RVA: 0x00077B43 File Offset: 0x00075D43
		public override XmlSchemaSimpleType MemberType
		{
			get
			{
				return this.memberType;
			}
		}

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x06001526 RID: 5414 RVA: 0x00077B4B File Offset: 0x00075D4B
		public override XmlSchemaType SchemaType
		{
			get
			{
				return this.schemaType;
			}
		}

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x06001527 RID: 5415 RVA: 0x00077B53 File Offset: 0x00075D53
		public override XmlSchemaElement SchemaElement
		{
			get
			{
				return this.decl as XmlSchemaElement;
			}
		}

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x06001528 RID: 5416 RVA: 0x00077B60 File Offset: 0x00075D60
		public override XmlSchemaAttribute SchemaAttribute
		{
			get
			{
				return this.decl as XmlSchemaAttribute;
			}
		}

		// Token: 0x06001529 RID: 5417 RVA: 0x00077B6D File Offset: 0x00075D6D
		public void SetValidity(XmlSchemaValidity value)
		{
			this.flags = (byte)(((int)this.flags & -4) | (int)((byte)value));
		}

		// Token: 0x0600152A RID: 5418 RVA: 0x00077B82 File Offset: 0x00075D82
		public void SetIsDefault(bool value)
		{
			if (value)
			{
				this.flags |= 4;
				return;
			}
			this.flags = (byte)((int)this.flags & -5);
		}

		// Token: 0x0600152B RID: 5419 RVA: 0x00077BA7 File Offset: 0x00075DA7
		public void SetIsNil(bool value)
		{
			if (value)
			{
				this.flags |= 8;
				return;
			}
			this.flags = (byte)((int)this.flags & -9);
		}

		// Token: 0x0600152C RID: 5420 RVA: 0x00077BCC File Offset: 0x00075DCC
		public override bool Equals(IXmlSchemaInfo schemaInfo)
		{
			return schemaInfo != null && schemaInfo.Validity == (XmlSchemaValidity)(this.flags & 3) && schemaInfo.IsDefault == (this.flags & 4) > 0 && schemaInfo.IsNil == (this.flags & 8) > 0 && schemaInfo.MemberType == this.memberType && schemaInfo.SchemaType == this.schemaType && schemaInfo.SchemaElement == this.decl as XmlSchemaElement && schemaInfo.SchemaAttribute == this.decl as XmlSchemaAttribute;
		}

		// Token: 0x04000DEC RID: 3564
		private byte flags;

		// Token: 0x04000DED RID: 3565
		private XmlSchemaSimpleType memberType;

		// Token: 0x04000DEE RID: 3566
		private XmlSchemaType schemaType;

		// Token: 0x04000DEF RID: 3567
		private object decl;

		// Token: 0x04000DF0 RID: 3568
		private const byte ValidityMask = 3;

		// Token: 0x04000DF1 RID: 3569
		private const byte IsDefaultBit = 4;

		// Token: 0x04000DF2 RID: 3570
		private const byte IsNilBit = 8;
	}
}
