﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;
using System.Threading.Tasks;

namespace System.Xml
{
	/// <summary>Resolves external XML resources named by a Uniform Resource Identifier (URI).</summary>
	// Token: 0x020002AF RID: 687
	public class XmlUrlResolver : XmlResolver
	{
		// Token: 0x170004B3 RID: 1203
		// (get) Token: 0x0600191C RID: 6428 RVA: 0x0008FF54 File Offset: 0x0008E154
		private static XmlDownloadManager DownloadManager
		{
			get
			{
				if (XmlUrlResolver.s_DownloadManager == null)
				{
					object value = new XmlDownloadManager();
					Interlocked.CompareExchange<object>(ref XmlUrlResolver.s_DownloadManager, value, null);
				}
				return (XmlDownloadManager)XmlUrlResolver.s_DownloadManager;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlUrlResolver" /> class.</summary>
		// Token: 0x0600191D RID: 6429 RVA: 0x0008FAEF File Offset: 0x0008DCEF
		public XmlUrlResolver()
		{
		}

		/// <summary>Sets credentials used to authenticate web requests.</summary>
		/// <returns>The credentials to be used to authenticate web requests. If this property is not set, the value defaults to <see langword="null" />; that is, the <see langword="XmlUrlResolver" /> has no user credentials.</returns>
		// Token: 0x170004B4 RID: 1204
		// (set) Token: 0x0600191E RID: 6430 RVA: 0x0008FF85 File Offset: 0x0008E185
		public override ICredentials Credentials
		{
			set
			{
				this._credentials = value;
			}
		}

		/// <summary>Gets or sets the network proxy for the underlying <see cref="T:System.Net.WebRequest" /> object.</summary>
		/// <returns>The <see cref="T:System.Net.IWebProxy" /> to use to access the Internet resource.</returns>
		// Token: 0x170004B5 RID: 1205
		// (set) Token: 0x0600191F RID: 6431 RVA: 0x0008FF8E File Offset: 0x0008E18E
		public IWebProxy Proxy
		{
			set
			{
				this._proxy = value;
			}
		}

		/// <summary>Gets or sets the cache policy for the underlying <see cref="T:System.Net.WebRequest" /> object.</summary>
		/// <returns>The cache policy for the underlying web request.</returns>
		// Token: 0x170004B6 RID: 1206
		// (set) Token: 0x06001920 RID: 6432 RVA: 0x0008FF97 File Offset: 0x0008E197
		public RequestCachePolicy CachePolicy
		{
			set
			{
				this._cachePolicy = value;
			}
		}

		/// <summary>Maps a URI to an object that contains the actual resource.</summary>
		/// <param name="absoluteUri">The URI returned from <see cref="M:System.Xml.XmlResolver.ResolveUri(System.Uri,System.String)" />.</param>
		/// <param name="role">Currently not used.</param>
		/// <param name="ofObjectToReturn">The type of object to return. The current implementation only returns <see cref="T:System.IO.Stream" /> objects.</param>
		/// <returns>A stream object or <see langword="null" /> if a type other than stream is specified.</returns>
		/// <exception cref="T:System.Xml.XmlException">
		///         <paramref name="ofObjectToReturn" /> is neither <see langword="null" /> nor a <see langword="Stream" /> type.</exception>
		/// <exception cref="T:System.UriFormatException">The specified URI is not an absolute URI.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="absoluteUri" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Exception">There is a runtime error (for example, an interrupted server connection).</exception>
		// Token: 0x06001921 RID: 6433 RVA: 0x0008FFA0 File Offset: 0x0008E1A0
		public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
		{
			if (ofObjectToReturn == null || ofObjectToReturn == typeof(Stream) || ofObjectToReturn == typeof(object))
			{
				return XmlUrlResolver.DownloadManager.GetStream(absoluteUri, this._credentials, this._proxy, this._cachePolicy);
			}
			throw new XmlException("Object type is not supported.", string.Empty);
		}

		/// <summary>Resolves the absolute URI from the base and relative URIs.</summary>
		/// <param name="baseUri">The base URI used to resolve the relative URI.</param>
		/// <param name="relativeUri">The URI to resolve. The URI can be absolute or relative. If absolute, this value effectively replaces the <paramref name="baseUri" /> value. If relative, it combines with the <paramref name="baseUri" /> to make an absolute URI.</param>
		/// <returns>The absolute URI, or <see langword="null" /> if the relative URI cannot be resolved.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="baseUri" /> is <see langword="null" /> or <paramref name="relativeUri" /> is <see langword="null" />.</exception>
		// Token: 0x06001922 RID: 6434 RVA: 0x00090007 File Offset: 0x0008E207
		[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
		public override Uri ResolveUri(Uri baseUri, string relativeUri)
		{
			return base.ResolveUri(baseUri, relativeUri);
		}

		/// <summary>Asynchronously maps a URI to an object that contains the actual resource.</summary>
		/// <param name="absoluteUri">The URI returned from <see cref="M:System.Xml.XmlResolver.ResolveUri(System.Uri,System.String)" />.</param>
		/// <param name="role">Currently not used.</param>
		/// <param name="ofObjectToReturn">The type of object to return. The current implementation only returns <see cref="T:System.IO.Stream" /> objects.</param>
		/// <returns>A stream object or <see langword="null" /> if a type other than stream is specified.</returns>
		// Token: 0x06001923 RID: 6435 RVA: 0x00090014 File Offset: 0x0008E214
		public override async Task<object> GetEntityAsync(Uri absoluteUri, string role, Type ofObjectToReturn)
		{
			if (ofObjectToReturn == null || ofObjectToReturn == typeof(Stream) || ofObjectToReturn == typeof(object))
			{
				return await XmlUrlResolver.DownloadManager.GetStreamAsync(absoluteUri, this._credentials, this._proxy, this._cachePolicy).ConfigureAwait(false);
			}
			throw new XmlException("Object type is not supported.", string.Empty);
		}

		// Token: 0x04001069 RID: 4201
		private static object s_DownloadManager;

		// Token: 0x0400106A RID: 4202
		private ICredentials _credentials;

		// Token: 0x0400106B RID: 4203
		private IWebProxy _proxy;

		// Token: 0x0400106C RID: 4204
		private RequestCachePolicy _cachePolicy;

		// Token: 0x020002B0 RID: 688
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetEntityAsync>d__15 : IAsyncStateMachine
		{
			// Token: 0x06001924 RID: 6436 RVA: 0x0009006C File Offset: 0x0008E26C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUrlResolver xmlUrlResolver = this;
				object result;
				try
				{
					ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (!(ofObjectToReturn == null) && !(ofObjectToReturn == typeof(Stream)) && !(ofObjectToReturn == typeof(object)))
						{
							throw new XmlException("Object type is not supported.", string.Empty);
						}
						configuredTaskAwaiter = XmlUrlResolver.DownloadManager.GetStreamAsync(absoluteUri, xmlUrlResolver._credentials, xmlUrlResolver._proxy, xmlUrlResolver._cachePolicy).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter, XmlUrlResolver.<GetEntityAsync>d__15>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					result = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001925 RID: 6437 RVA: 0x0009019C File Offset: 0x0008E39C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400106D RID: 4205
			public int <>1__state;

			// Token: 0x0400106E RID: 4206
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x0400106F RID: 4207
			public Type ofObjectToReturn;

			// Token: 0x04001070 RID: 4208
			public Uri absoluteUri;

			// Token: 0x04001071 RID: 4209
			public XmlUrlResolver <>4__this;

			// Token: 0x04001072 RID: 4210
			private ConfiguredTaskAwaitable<Stream>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
