﻿using System;

namespace System.Xml.Xsl
{
	// Token: 0x020004A0 RID: 1184
	internal interface IErrorHelper
	{
		// Token: 0x06002E8D RID: 11917
		void ReportError(string res, params string[] args);

		// Token: 0x06002E8E RID: 11918
		void ReportWarning(string res, params string[] args);
	}
}
