﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;

namespace System.Xml.Xsl.XPath
{
	// Token: 0x02000533 RID: 1331
	internal interface IXPathBuilder<Node>
	{
		// Token: 0x0600335D RID: 13149
		void StartBuild();

		// Token: 0x0600335E RID: 13150
		Node EndBuild(Node result);

		// Token: 0x0600335F RID: 13151
		Node String(string value);

		// Token: 0x06003360 RID: 13152
		Node Number(double value);

		// Token: 0x06003361 RID: 13153
		Node Operator(XPathOperator op, Node left, Node right);

		// Token: 0x06003362 RID: 13154
		Node Axis(XPathAxis xpathAxis, XPathNodeType nodeType, string prefix, string name);

		// Token: 0x06003363 RID: 13155
		Node JoinStep(Node left, Node right);

		// Token: 0x06003364 RID: 13156
		Node Predicate(Node node, Node condition, bool reverseStep);

		// Token: 0x06003365 RID: 13157
		Node Variable(string prefix, string name);

		// Token: 0x06003366 RID: 13158
		Node Function(string prefix, string name, IList<Node> args);
	}
}
