﻿using System;

namespace System.Xml.Xsl.XPath
{
	// Token: 0x02000539 RID: 1337
	internal enum LexKind
	{
		// Token: 0x040022AC RID: 8876
		Unknown,
		// Token: 0x040022AD RID: 8877
		Or,
		// Token: 0x040022AE RID: 8878
		And,
		// Token: 0x040022AF RID: 8879
		Eq,
		// Token: 0x040022B0 RID: 8880
		Ne,
		// Token: 0x040022B1 RID: 8881
		Lt,
		// Token: 0x040022B2 RID: 8882
		Le,
		// Token: 0x040022B3 RID: 8883
		Gt,
		// Token: 0x040022B4 RID: 8884
		Ge,
		// Token: 0x040022B5 RID: 8885
		Plus,
		// Token: 0x040022B6 RID: 8886
		Minus,
		// Token: 0x040022B7 RID: 8887
		Multiply,
		// Token: 0x040022B8 RID: 8888
		Divide,
		// Token: 0x040022B9 RID: 8889
		Modulo,
		// Token: 0x040022BA RID: 8890
		UnaryMinus,
		// Token: 0x040022BB RID: 8891
		Union,
		// Token: 0x040022BC RID: 8892
		LastOperator = 15,
		// Token: 0x040022BD RID: 8893
		DotDot,
		// Token: 0x040022BE RID: 8894
		ColonColon,
		// Token: 0x040022BF RID: 8895
		SlashSlash,
		// Token: 0x040022C0 RID: 8896
		Number,
		// Token: 0x040022C1 RID: 8897
		Axis,
		// Token: 0x040022C2 RID: 8898
		Name,
		// Token: 0x040022C3 RID: 8899
		String,
		// Token: 0x040022C4 RID: 8900
		Eof,
		// Token: 0x040022C5 RID: 8901
		FirstStringable = 21,
		// Token: 0x040022C6 RID: 8902
		LastNonChar = 23,
		// Token: 0x040022C7 RID: 8903
		LParens = 40,
		// Token: 0x040022C8 RID: 8904
		RParens,
		// Token: 0x040022C9 RID: 8905
		LBracket = 91,
		// Token: 0x040022CA RID: 8906
		RBracket = 93,
		// Token: 0x040022CB RID: 8907
		Dot = 46,
		// Token: 0x040022CC RID: 8908
		At = 64,
		// Token: 0x040022CD RID: 8909
		Comma = 44,
		// Token: 0x040022CE RID: 8910
		Star = 42,
		// Token: 0x040022CF RID: 8911
		Slash = 47,
		// Token: 0x040022D0 RID: 8912
		Dollar = 36,
		// Token: 0x040022D1 RID: 8913
		RBrace = 125
	}
}
