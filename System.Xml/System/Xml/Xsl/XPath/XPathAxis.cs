﻿using System;

namespace System.Xml.Xsl.XPath
{
	// Token: 0x02000534 RID: 1332
	internal enum XPathAxis
	{
		// Token: 0x0400226D RID: 8813
		Unknown,
		// Token: 0x0400226E RID: 8814
		Ancestor,
		// Token: 0x0400226F RID: 8815
		AncestorOrSelf,
		// Token: 0x04002270 RID: 8816
		Attribute,
		// Token: 0x04002271 RID: 8817
		Child,
		// Token: 0x04002272 RID: 8818
		Descendant,
		// Token: 0x04002273 RID: 8819
		DescendantOrSelf,
		// Token: 0x04002274 RID: 8820
		Following,
		// Token: 0x04002275 RID: 8821
		FollowingSibling,
		// Token: 0x04002276 RID: 8822
		Namespace,
		// Token: 0x04002277 RID: 8823
		Parent,
		// Token: 0x04002278 RID: 8824
		Preceding,
		// Token: 0x04002279 RID: 8825
		PrecedingSibling,
		// Token: 0x0400227A RID: 8826
		Self,
		// Token: 0x0400227B RID: 8827
		Root
	}
}
