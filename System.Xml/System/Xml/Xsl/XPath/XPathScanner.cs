﻿using System;

namespace System.Xml.Xsl.XPath
{
	// Token: 0x0200053A RID: 1338
	internal sealed class XPathScanner
	{
		// Token: 0x06003386 RID: 13190 RVA: 0x0010D750 File Offset: 0x0010B950
		public XPathScanner(string xpathExpr) : this(xpathExpr, 0)
		{
		}

		// Token: 0x06003387 RID: 13191 RVA: 0x0010D75A File Offset: 0x0010B95A
		public XPathScanner(string xpathExpr, int startFrom)
		{
			this.xpathExpr = xpathExpr;
			this.kind = LexKind.Unknown;
			this.SetSourceIndex(startFrom);
			this.NextLex();
		}

		// Token: 0x17000AC5 RID: 2757
		// (get) Token: 0x06003388 RID: 13192 RVA: 0x0010D788 File Offset: 0x0010B988
		public string Source
		{
			get
			{
				return this.xpathExpr;
			}
		}

		// Token: 0x17000AC6 RID: 2758
		// (get) Token: 0x06003389 RID: 13193 RVA: 0x0010D790 File Offset: 0x0010B990
		public LexKind Kind
		{
			get
			{
				return this.kind;
			}
		}

		// Token: 0x17000AC7 RID: 2759
		// (get) Token: 0x0600338A RID: 13194 RVA: 0x0010D798 File Offset: 0x0010B998
		public int LexStart
		{
			get
			{
				return this.lexStart;
			}
		}

		// Token: 0x17000AC8 RID: 2760
		// (get) Token: 0x0600338B RID: 13195 RVA: 0x0010D7A0 File Offset: 0x0010B9A0
		public int LexSize
		{
			get
			{
				return this.curIndex - this.lexStart;
			}
		}

		// Token: 0x17000AC9 RID: 2761
		// (get) Token: 0x0600338C RID: 13196 RVA: 0x0010D7AF File Offset: 0x0010B9AF
		public int PrevLexEnd
		{
			get
			{
				return this.prevLexEnd;
			}
		}

		// Token: 0x0600338D RID: 13197 RVA: 0x0010D7B7 File Offset: 0x0010B9B7
		private void SetSourceIndex(int index)
		{
			this.curIndex = index - 1;
			this.NextChar();
		}

		// Token: 0x0600338E RID: 13198 RVA: 0x0010D7C8 File Offset: 0x0010B9C8
		private void NextChar()
		{
			this.curIndex++;
			if (this.curIndex < this.xpathExpr.Length)
			{
				this.curChar = this.xpathExpr[this.curIndex];
				return;
			}
			this.curChar = '\0';
		}

		// Token: 0x17000ACA RID: 2762
		// (get) Token: 0x0600338F RID: 13199 RVA: 0x0010D815 File Offset: 0x0010BA15
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000ACB RID: 2763
		// (get) Token: 0x06003390 RID: 13200 RVA: 0x0010D81D File Offset: 0x0010BA1D
		public string Prefix
		{
			get
			{
				return this.prefix;
			}
		}

		// Token: 0x17000ACC RID: 2764
		// (get) Token: 0x06003391 RID: 13201 RVA: 0x0010D825 File Offset: 0x0010BA25
		public string RawValue
		{
			get
			{
				if (this.kind == LexKind.Eof)
				{
					return this.LexKindToString(this.kind);
				}
				return this.xpathExpr.Substring(this.lexStart, this.curIndex - this.lexStart);
			}
		}

		// Token: 0x17000ACD RID: 2765
		// (get) Token: 0x06003392 RID: 13202 RVA: 0x0010D85C File Offset: 0x0010BA5C
		public string StringValue
		{
			get
			{
				return this.stringValue;
			}
		}

		// Token: 0x17000ACE RID: 2766
		// (get) Token: 0x06003393 RID: 13203 RVA: 0x0010D864 File Offset: 0x0010BA64
		public bool CanBeFunction
		{
			get
			{
				return this.canBeFunction;
			}
		}

		// Token: 0x17000ACF RID: 2767
		// (get) Token: 0x06003394 RID: 13204 RVA: 0x0010D86C File Offset: 0x0010BA6C
		public XPathAxis Axis
		{
			get
			{
				return this.axis;
			}
		}

		// Token: 0x06003395 RID: 13205 RVA: 0x0010D874 File Offset: 0x0010BA74
		private void SkipSpace()
		{
			while (this.xmlCharType.IsWhiteSpace(this.curChar))
			{
				this.NextChar();
			}
		}

		// Token: 0x06003396 RID: 13206 RVA: 0x000F6633 File Offset: 0x000F4833
		private static bool IsAsciiDigit(char ch)
		{
			return ch - '0' <= '\t';
		}

		// Token: 0x06003397 RID: 13207 RVA: 0x0010D894 File Offset: 0x0010BA94
		public void NextLex()
		{
			this.prevLexEnd = this.curIndex;
			this.prevKind = this.kind;
			this.SkipSpace();
			this.lexStart = this.curIndex;
			char c = this.curChar;
			if (c <= '[')
			{
				if (c != '\0')
				{
					switch (c)
					{
					case '!':
						this.NextChar();
						if (this.curChar == '=')
						{
							this.kind = LexKind.Ne;
							this.NextChar();
							return;
						}
						this.kind = LexKind.Unknown;
						return;
					case '"':
					case '\'':
						this.kind = LexKind.String;
						this.ScanString();
						return;
					case '#':
					case '%':
					case '&':
					case ';':
					case '?':
						goto IL_27C;
					case '$':
					case '(':
					case ')':
					case ',':
					case '@':
						goto IL_F2;
					case '*':
						this.kind = LexKind.Star;
						this.NextChar();
						this.CheckOperator(true);
						return;
					case '+':
						this.kind = LexKind.Plus;
						this.NextChar();
						return;
					case '-':
						this.kind = LexKind.Minus;
						this.NextChar();
						return;
					case '.':
						this.NextChar();
						if (this.curChar == '.')
						{
							this.kind = LexKind.DotDot;
							this.NextChar();
							return;
						}
						if (!XPathScanner.IsAsciiDigit(this.curChar))
						{
							this.kind = LexKind.Dot;
							return;
						}
						this.SetSourceIndex(this.lexStart);
						break;
					case '/':
						this.NextChar();
						if (this.curChar == '/')
						{
							this.kind = LexKind.SlashSlash;
							this.NextChar();
							return;
						}
						this.kind = LexKind.Slash;
						return;
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						break;
					case ':':
						this.NextChar();
						if (this.curChar == ':')
						{
							this.kind = LexKind.ColonColon;
							this.NextChar();
							return;
						}
						this.kind = LexKind.Unknown;
						return;
					case '<':
						this.NextChar();
						if (this.curChar == '=')
						{
							this.kind = LexKind.Le;
							this.NextChar();
							return;
						}
						this.kind = LexKind.Lt;
						return;
					case '=':
						this.kind = LexKind.Eq;
						this.NextChar();
						return;
					case '>':
						this.NextChar();
						if (this.curChar == '=')
						{
							this.kind = LexKind.Ge;
							this.NextChar();
							return;
						}
						this.kind = LexKind.Gt;
						return;
					default:
						if (c != '[')
						{
							goto IL_27C;
						}
						goto IL_F2;
					}
					this.kind = LexKind.Number;
					this.ScanNumber();
					return;
				}
				this.kind = LexKind.Eof;
				return;
			}
			else if (c != ']')
			{
				if (c == '|')
				{
					this.kind = LexKind.Union;
					this.NextChar();
					return;
				}
				if (c != '}')
				{
					goto IL_27C;
				}
			}
			IL_F2:
			this.kind = (LexKind)this.curChar;
			this.NextChar();
			return;
			IL_27C:
			if (this.xmlCharType.IsStartNCNameSingleChar(this.curChar))
			{
				this.kind = LexKind.Name;
				this.name = this.ScanNCName();
				this.prefix = string.Empty;
				this.canBeFunction = false;
				this.axis = XPathAxis.Unknown;
				bool flag = false;
				int sourceIndex = this.curIndex;
				if (this.curChar == ':')
				{
					this.NextChar();
					if (this.curChar == ':')
					{
						this.NextChar();
						flag = true;
						this.SetSourceIndex(sourceIndex);
					}
					else if (this.curChar == '*')
					{
						this.NextChar();
						this.prefix = this.name;
						this.name = "*";
					}
					else if (this.xmlCharType.IsStartNCNameSingleChar(this.curChar))
					{
						this.prefix = this.name;
						this.name = this.ScanNCName();
						sourceIndex = this.curIndex;
						this.SkipSpace();
						this.canBeFunction = (this.curChar == '(');
						this.SetSourceIndex(sourceIndex);
					}
					else
					{
						this.SetSourceIndex(sourceIndex);
					}
				}
				else
				{
					this.SkipSpace();
					if (this.curChar == ':')
					{
						this.NextChar();
						if (this.curChar == ':')
						{
							this.NextChar();
							flag = true;
						}
						this.SetSourceIndex(sourceIndex);
					}
					else
					{
						this.canBeFunction = (this.curChar == '(');
					}
				}
				if (!this.CheckOperator(false) && flag)
				{
					this.axis = this.CheckAxis();
					return;
				}
			}
			else
			{
				this.kind = LexKind.Unknown;
				this.NextChar();
			}
		}

		// Token: 0x06003398 RID: 13208 RVA: 0x0010DC8C File Offset: 0x0010BE8C
		private bool CheckOperator(bool star)
		{
			LexKind lexKind;
			if (star)
			{
				lexKind = LexKind.Multiply;
			}
			else
			{
				if (this.prefix.Length != 0 || this.name.Length > 3)
				{
					return false;
				}
				string a = this.name;
				if (!(a == "or"))
				{
					if (!(a == "and"))
					{
						if (!(a == "div"))
						{
							if (!(a == "mod"))
							{
								return false;
							}
							lexKind = LexKind.Modulo;
						}
						else
						{
							lexKind = LexKind.Divide;
						}
					}
					else
					{
						lexKind = LexKind.And;
					}
				}
				else
				{
					lexKind = LexKind.Or;
				}
			}
			if (this.prevKind <= LexKind.Union)
			{
				return false;
			}
			LexKind lexKind2 = this.prevKind;
			if (lexKind2 <= LexKind.LParens)
			{
				if (lexKind2 - LexKind.ColonColon > 1 && lexKind2 != LexKind.Dollar && lexKind2 != LexKind.LParens)
				{
					goto IL_BE;
				}
			}
			else if (lexKind2 <= LexKind.Slash)
			{
				if (lexKind2 != LexKind.Comma && lexKind2 != LexKind.Slash)
				{
					goto IL_BE;
				}
			}
			else if (lexKind2 != LexKind.At && lexKind2 != LexKind.LBracket)
			{
				goto IL_BE;
			}
			return false;
			IL_BE:
			this.kind = lexKind;
			return true;
		}

		// Token: 0x06003399 RID: 13209 RVA: 0x0010DD60 File Offset: 0x0010BF60
		private XPathAxis CheckAxis()
		{
			this.kind = LexKind.Axis;
			string text = this.name;
			uint num = <PrivateImplementationDetails>.ComputeStringHash(text);
			if (num <= 2535512472U)
			{
				if (num <= 1047347951U)
				{
					if (num != 21436113U)
					{
						if (num != 510973315U)
						{
							if (num == 1047347951U)
							{
								if (text == "attribute")
								{
									return XPathAxis.Attribute;
								}
							}
						}
						else if (text == "ancestor-or-self")
						{
							return XPathAxis.AncestorOrSelf;
						}
					}
					else if (text == "preceding-sibling")
					{
						return XPathAxis.PrecedingSibling;
					}
				}
				else if (num != 1683726967U)
				{
					if (num != 2452897184U)
					{
						if (num == 2535512472U)
						{
							if (text == "following")
							{
								return XPathAxis.Following;
							}
						}
					}
					else if (text == "ancestor")
					{
						return XPathAxis.Ancestor;
					}
				}
				else if (text == "self")
				{
					return XPathAxis.Self;
				}
			}
			else if (num <= 3726896370U)
			{
				if (num != 2944295921U)
				{
					if (num != 3402529440U)
					{
						if (num == 3726896370U)
						{
							if (text == "preceding")
							{
								return XPathAxis.Preceding;
							}
						}
					}
					else if (text == "namespace")
					{
						return XPathAxis.Namespace;
					}
				}
				else if (text == "descendant-or-self")
				{
					return XPathAxis.DescendantOrSelf;
				}
			}
			else if (num <= 3939368189U)
			{
				if (num != 3852476509U)
				{
					if (num == 3939368189U)
					{
						if (text == "parent")
						{
							return XPathAxis.Parent;
						}
					}
				}
				else if (text == "child")
				{
					return XPathAxis.Child;
				}
			}
			else if (num != 3998959382U)
			{
				if (num == 4042989175U)
				{
					if (text == "following-sibling")
					{
						return XPathAxis.FollowingSibling;
					}
				}
			}
			else if (text == "descendant")
			{
				return XPathAxis.Descendant;
			}
			this.kind = LexKind.Name;
			return XPathAxis.Unknown;
		}

		// Token: 0x0600339A RID: 13210 RVA: 0x0010DF50 File Offset: 0x0010C150
		private void ScanNumber()
		{
			while (XPathScanner.IsAsciiDigit(this.curChar))
			{
				this.NextChar();
			}
			if (this.curChar == '.')
			{
				this.NextChar();
				while (XPathScanner.IsAsciiDigit(this.curChar))
				{
					this.NextChar();
				}
			}
			if (((int)this.curChar & -33) == 69)
			{
				this.NextChar();
				if (this.curChar == '+' || this.curChar == '-')
				{
					this.NextChar();
				}
				while (XPathScanner.IsAsciiDigit(this.curChar))
				{
					this.NextChar();
				}
				throw this.CreateException("Scientific notation is not allowed.", Array.Empty<string>());
			}
		}

		// Token: 0x0600339B RID: 13211 RVA: 0x0010DFEC File Offset: 0x0010C1EC
		private void ScanString()
		{
			int num = this.curIndex + 1;
			int num2 = this.xpathExpr.IndexOf(this.curChar, num);
			if (num2 < 0)
			{
				this.SetSourceIndex(this.xpathExpr.Length);
				throw this.CreateException("String literal was not closed.", Array.Empty<string>());
			}
			this.stringValue = this.xpathExpr.Substring(num, num2 - num);
			this.SetSourceIndex(num2 + 1);
		}

		// Token: 0x0600339C RID: 13212 RVA: 0x0010E05C File Offset: 0x0010C25C
		private string ScanNCName()
		{
			int num = this.curIndex;
			while (this.xmlCharType.IsNCNameSingleChar(this.curChar))
			{
				this.NextChar();
			}
			return this.xpathExpr.Substring(num, this.curIndex - num);
		}

		// Token: 0x0600339D RID: 13213 RVA: 0x0010E09F File Offset: 0x0010C29F
		public void PassToken(LexKind t)
		{
			this.CheckToken(t);
			this.NextLex();
		}

		// Token: 0x0600339E RID: 13214 RVA: 0x0010E0B0 File Offset: 0x0010C2B0
		public void CheckToken(LexKind t)
		{
			if (this.kind == t)
			{
				return;
			}
			if (t == LexKind.Eof)
			{
				throw this.CreateException("Expected end of the expression, found '{0}'.", new string[]
				{
					this.RawValue
				});
			}
			throw this.CreateException("Expected token '{0}', found '{1}'.", new string[]
			{
				this.LexKindToString(t),
				this.RawValue
			});
		}

		// Token: 0x0600339F RID: 13215 RVA: 0x0010E10B File Offset: 0x0010C30B
		private string LexKindToString(LexKind t)
		{
			if (LexKind.Eof < t)
			{
				return new string((char)t, 1);
			}
			switch (t)
			{
			case LexKind.Name:
				return "<name>";
			case LexKind.String:
				return "<string literal>";
			case LexKind.Eof:
				return "<eof>";
			default:
				return string.Empty;
			}
		}

		// Token: 0x060033A0 RID: 13216 RVA: 0x0010E149 File Offset: 0x0010C349
		public XPathCompileException CreateException(string resId, params string[] args)
		{
			return new XPathCompileException(this.xpathExpr, this.lexStart, this.curIndex, resId, args);
		}

		// Token: 0x040022D2 RID: 8914
		private string xpathExpr;

		// Token: 0x040022D3 RID: 8915
		private int curIndex;

		// Token: 0x040022D4 RID: 8916
		private char curChar;

		// Token: 0x040022D5 RID: 8917
		private LexKind kind;

		// Token: 0x040022D6 RID: 8918
		private string name;

		// Token: 0x040022D7 RID: 8919
		private string prefix;

		// Token: 0x040022D8 RID: 8920
		private string stringValue;

		// Token: 0x040022D9 RID: 8921
		private bool canBeFunction;

		// Token: 0x040022DA RID: 8922
		private int lexStart;

		// Token: 0x040022DB RID: 8923
		private int prevLexEnd;

		// Token: 0x040022DC RID: 8924
		private LexKind prevKind;

		// Token: 0x040022DD RID: 8925
		private XPathAxis axis;

		// Token: 0x040022DE RID: 8926
		private XmlCharType xmlCharType = XmlCharType.Instance;
	}
}
