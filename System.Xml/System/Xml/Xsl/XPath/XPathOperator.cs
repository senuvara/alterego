﻿using System;

namespace System.Xml.Xsl.XPath
{
	// Token: 0x02000537 RID: 1335
	internal enum XPathOperator
	{
		// Token: 0x04002284 RID: 8836
		Unknown,
		// Token: 0x04002285 RID: 8837
		Or,
		// Token: 0x04002286 RID: 8838
		And,
		// Token: 0x04002287 RID: 8839
		Eq,
		// Token: 0x04002288 RID: 8840
		Ne,
		// Token: 0x04002289 RID: 8841
		Lt,
		// Token: 0x0400228A RID: 8842
		Le,
		// Token: 0x0400228B RID: 8843
		Gt,
		// Token: 0x0400228C RID: 8844
		Ge,
		// Token: 0x0400228D RID: 8845
		Plus,
		// Token: 0x0400228E RID: 8846
		Minus,
		// Token: 0x0400228F RID: 8847
		Multiply,
		// Token: 0x04002290 RID: 8848
		Divide,
		// Token: 0x04002291 RID: 8849
		Modulo,
		// Token: 0x04002292 RID: 8850
		UnaryMinus,
		// Token: 0x04002293 RID: 8851
		Union,
		// Token: 0x04002294 RID: 8852
		LastXPath1Operator = 15,
		// Token: 0x04002295 RID: 8853
		UnaryPlus,
		// Token: 0x04002296 RID: 8854
		Idiv,
		// Token: 0x04002297 RID: 8855
		Is,
		// Token: 0x04002298 RID: 8856
		After,
		// Token: 0x04002299 RID: 8857
		Before,
		// Token: 0x0400229A RID: 8858
		Range,
		// Token: 0x0400229B RID: 8859
		Except,
		// Token: 0x0400229C RID: 8860
		Intersect,
		// Token: 0x0400229D RID: 8861
		ValEq,
		// Token: 0x0400229E RID: 8862
		ValNe,
		// Token: 0x0400229F RID: 8863
		ValLt,
		// Token: 0x040022A0 RID: 8864
		ValLe,
		// Token: 0x040022A1 RID: 8865
		ValGt,
		// Token: 0x040022A2 RID: 8866
		ValGe
	}
}
