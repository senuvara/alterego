﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace System.Xml.Xsl.XPath
{
	// Token: 0x02000535 RID: 1333
	[Serializable]
	internal class XPathCompileException : XslLoadException
	{
		// Token: 0x06003367 RID: 13159 RVA: 0x0010C924 File Offset: 0x0010AB24
		protected XPathCompileException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.queryString = (string)info.GetValue("QueryString", typeof(string));
			this.startChar = (int)info.GetValue("StartChar", typeof(int));
			this.endChar = (int)info.GetValue("EndChar", typeof(int));
		}

		// Token: 0x06003368 RID: 13160 RVA: 0x0010C999 File Offset: 0x0010AB99
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("QueryString", this.queryString);
			info.AddValue("StartChar", this.startChar);
			info.AddValue("EndChar", this.endChar);
		}

		// Token: 0x06003369 RID: 13161 RVA: 0x0010C9D6 File Offset: 0x0010ABD6
		internal XPathCompileException(string queryString, int startChar, int endChar, string resId, params string[] args) : base(resId, args)
		{
			this.queryString = queryString;
			this.startChar = startChar;
			this.endChar = endChar;
		}

		// Token: 0x0600336A RID: 13162 RVA: 0x0010C9F7 File Offset: 0x0010ABF7
		internal XPathCompileException(string resId, params string[] args) : base(resId, args)
		{
		}

		// Token: 0x0600336B RID: 13163 RVA: 0x0010CA04 File Offset: 0x0010AC04
		private static void AppendTrimmed(StringBuilder sb, string value, int startIndex, int count, XPathCompileException.TrimType trimType)
		{
			if (count <= 32)
			{
				sb.Append(value, startIndex, count);
				return;
			}
			switch (trimType)
			{
			case XPathCompileException.TrimType.Left:
				sb.Append("...");
				sb.Append(value, startIndex + count - 32, 32);
				return;
			case XPathCompileException.TrimType.Right:
				sb.Append(value, startIndex, 32);
				sb.Append("...");
				return;
			case XPathCompileException.TrimType.Middle:
				sb.Append(value, startIndex, 16);
				sb.Append("...");
				sb.Append(value, startIndex + count - 16, 16);
				return;
			default:
				return;
			}
		}

		// Token: 0x0600336C RID: 13164 RVA: 0x0010CA94 File Offset: 0x0010AC94
		internal string MarkOutError()
		{
			if (this.queryString == null || this.queryString.Trim(new char[]
			{
				' '
			}).Length == 0)
			{
				return null;
			}
			int num = this.endChar - this.startChar;
			StringBuilder stringBuilder = new StringBuilder();
			XPathCompileException.AppendTrimmed(stringBuilder, this.queryString, 0, this.startChar, XPathCompileException.TrimType.Left);
			if (num > 0)
			{
				stringBuilder.Append(" -->");
				XPathCompileException.AppendTrimmed(stringBuilder, this.queryString, this.startChar, num, XPathCompileException.TrimType.Middle);
			}
			stringBuilder.Append("<-- ");
			XPathCompileException.AppendTrimmed(stringBuilder, this.queryString, this.endChar, this.queryString.Length - this.endChar, XPathCompileException.TrimType.Right);
			return stringBuilder.ToString();
		}

		// Token: 0x0600336D RID: 13165 RVA: 0x0010CB4C File Offset: 0x0010AD4C
		internal override string FormatDetailedMessage()
		{
			string text = this.Message;
			string text2 = this.MarkOutError();
			if (text2 != null && text2.Length > 0)
			{
				if (text.Length > 0)
				{
					text += Environment.NewLine;
				}
				text += text2;
			}
			return text;
		}

		// Token: 0x0400227C RID: 8828
		public string queryString;

		// Token: 0x0400227D RID: 8829
		public int startChar;

		// Token: 0x0400227E RID: 8830
		public int endChar;

		// Token: 0x02000536 RID: 1334
		private enum TrimType
		{
			// Token: 0x04002280 RID: 8832
			Left,
			// Token: 0x04002281 RID: 8833
			Right,
			// Token: 0x04002282 RID: 8834
			Middle
		}
	}
}
