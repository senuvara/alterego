﻿using System;
using System.Globalization;
using System.Resources;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Utils;

namespace System.Xml.Xsl
{
	/// <summary>The exception that is thrown when an error occurs while processing an XSLT transformation.</summary>
	// Token: 0x020004A9 RID: 1193
	[Serializable]
	public class XsltException : SystemException
	{
		/// <summary>Initializes a new instance of the <see langword="XsltException" /> class using the information in the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" /> objects.</summary>
		/// <param name="info">The <see langword="SerializationInfo" /> object containing all the properties of an <see langword="XsltException" />. </param>
		/// <param name="context">The <see langword="StreamingContext" /> object. </param>
		// Token: 0x06002ED9 RID: 11993 RVA: 0x000FC898 File Offset: 0x000FAA98
		protected XsltException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.res = (string)info.GetValue("res", typeof(string));
			this.args = (string[])info.GetValue("args", typeof(string[]));
			this.sourceUri = (string)info.GetValue("sourceUri", typeof(string));
			this.lineNumber = (int)info.GetValue("lineNumber", typeof(int));
			this.linePosition = (int)info.GetValue("linePosition", typeof(int));
			string text = null;
			foreach (SerializationEntry serializationEntry in info)
			{
				if (serializationEntry.Name == "version")
				{
					text = (string)serializationEntry.Value;
				}
			}
			if (text == null)
			{
				this.message = XsltException.CreateMessage(this.res, this.args, this.sourceUri, this.lineNumber, this.linePosition);
				return;
			}
			this.message = null;
		}

		/// <summary>Streams all the <see langword="XsltException" /> properties into the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> class for the given <see cref="T:System.Runtime.Serialization.StreamingContext" />.</summary>
		/// <param name="info">The <see langword="SerializationInfo" /> object. </param>
		/// <param name="context">The <see langword="StreamingContext" /> object. </param>
		// Token: 0x06002EDA RID: 11994 RVA: 0x000FC9BC File Offset: 0x000FABBC
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("res", this.res);
			info.AddValue("args", this.args);
			info.AddValue("sourceUri", this.sourceUri);
			info.AddValue("lineNumber", this.lineNumber);
			info.AddValue("linePosition", this.linePosition);
			info.AddValue("version", "2.0");
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Xsl.XsltException" /> class.</summary>
		// Token: 0x06002EDB RID: 11995 RVA: 0x000FCA36 File Offset: 0x000FAC36
		public XsltException() : this(string.Empty, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Xsl.XsltException" /> class with a specified error message. </summary>
		/// <param name="message">The message that describes the error.</param>
		// Token: 0x06002EDC RID: 11996 RVA: 0x000FCA44 File Offset: 0x000FAC44
		public XsltException(string message) : this(message, null)
		{
		}

		/// <summary>Initializes a new instance of the <see langword="XsltException" /> class.</summary>
		/// <param name="message">The description of the error condition. </param>
		/// <param name="innerException">The <see cref="T:System.Exception" /> which threw the <see langword="XsltException" />, if any. This value can be <see langword="null" />. </param>
		// Token: 0x06002EDD RID: 11997 RVA: 0x000FCA4E File Offset: 0x000FAC4E
		public XsltException(string message, Exception innerException) : this("{0}", new string[]
		{
			message
		}, null, 0, 0, innerException)
		{
		}

		// Token: 0x06002EDE RID: 11998 RVA: 0x000FCA69 File Offset: 0x000FAC69
		internal static XsltException Create(string res, params string[] args)
		{
			return new XsltException(res, args, null, 0, 0, null);
		}

		// Token: 0x06002EDF RID: 11999 RVA: 0x000FCA76 File Offset: 0x000FAC76
		internal static XsltException Create(string res, string[] args, Exception inner)
		{
			return new XsltException(res, args, null, 0, 0, inner);
		}

		// Token: 0x06002EE0 RID: 12000 RVA: 0x000FCA83 File Offset: 0x000FAC83
		internal XsltException(string res, string[] args, string sourceUri, int lineNumber, int linePosition, Exception inner) : base(XsltException.CreateMessage(res, args, sourceUri, lineNumber, linePosition), inner)
		{
			base.HResult = -2146231998;
			this.res = res;
			this.sourceUri = sourceUri;
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		/// <summary>Gets the location path of the style sheet.</summary>
		/// <returns>The location path of the style sheet.</returns>
		// Token: 0x170009EE RID: 2542
		// (get) Token: 0x06002EE1 RID: 12001 RVA: 0x000FCAC2 File Offset: 0x000FACC2
		public virtual string SourceUri
		{
			get
			{
				return this.sourceUri;
			}
		}

		/// <summary>Gets the line number indicating where the error occurred in the style sheet.</summary>
		/// <returns>The line number indicating where the error occurred in the style sheet.</returns>
		// Token: 0x170009EF RID: 2543
		// (get) Token: 0x06002EE2 RID: 12002 RVA: 0x000FCACA File Offset: 0x000FACCA
		public virtual int LineNumber
		{
			get
			{
				return this.lineNumber;
			}
		}

		/// <summary>Gets the line position indicating where the error occurred in the style sheet.</summary>
		/// <returns>The line position indicating where the error occurred in the style sheet.</returns>
		// Token: 0x170009F0 RID: 2544
		// (get) Token: 0x06002EE3 RID: 12003 RVA: 0x000FCAD2 File Offset: 0x000FACD2
		public virtual int LinePosition
		{
			get
			{
				return this.linePosition;
			}
		}

		/// <summary>Gets the formatted error message describing the current exception.</summary>
		/// <returns>The formatted error message describing the current exception.</returns>
		// Token: 0x170009F1 RID: 2545
		// (get) Token: 0x06002EE4 RID: 12004 RVA: 0x000FCADA File Offset: 0x000FACDA
		public override string Message
		{
			get
			{
				if (this.message != null)
				{
					return this.message;
				}
				return base.Message;
			}
		}

		// Token: 0x06002EE5 RID: 12005 RVA: 0x000FCAF4 File Offset: 0x000FACF4
		private static string CreateMessage(string res, string[] args, string sourceUri, int lineNumber, int linePosition)
		{
			string result;
			try
			{
				string text = XsltException.FormatMessage(res, args);
				if (res != "XSLT compile error at {0}({1},{2}). See InnerException for details." && lineNumber != 0)
				{
					text = text + " " + XsltException.FormatMessage("An error occurred at {0}({1},{2}).", new string[]
					{
						sourceUri,
						lineNumber.ToString(CultureInfo.InvariantCulture),
						linePosition.ToString(CultureInfo.InvariantCulture)
					});
				}
				result = text;
			}
			catch (MissingManifestResourceException)
			{
				result = "UNKNOWN(" + res + ")";
			}
			return result;
		}

		// Token: 0x06002EE6 RID: 12006 RVA: 0x000FCB80 File Offset: 0x000FAD80
		private static string FormatMessage(string key, params string[] args)
		{
			string text = Res.GetString(key);
			if (text != null && args != null)
			{
				text = string.Format(CultureInfo.InvariantCulture, text, args);
			}
			return text;
		}

		// Token: 0x04001F95 RID: 8085
		private string res;

		// Token: 0x04001F96 RID: 8086
		private string[] args;

		// Token: 0x04001F97 RID: 8087
		private string sourceUri;

		// Token: 0x04001F98 RID: 8088
		private int lineNumber;

		// Token: 0x04001F99 RID: 8089
		private int linePosition;

		// Token: 0x04001F9A RID: 8090
		private string message;
	}
}
