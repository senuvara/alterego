﻿using System;
using System.Diagnostics;

namespace System.Xml.Xsl
{
	// Token: 0x0200048E RID: 1166
	[DebuggerDisplay("({Line},{Pos})")]
	internal struct Location
	{
		// Token: 0x17000985 RID: 2437
		// (get) Token: 0x06002D90 RID: 11664 RVA: 0x000F6221 File Offset: 0x000F4421
		public int Line
		{
			get
			{
				return (int)(this.value >> 32);
			}
		}

		// Token: 0x17000986 RID: 2438
		// (get) Token: 0x06002D91 RID: 11665 RVA: 0x000F622D File Offset: 0x000F442D
		public int Pos
		{
			get
			{
				return (int)this.value;
			}
		}

		// Token: 0x06002D92 RID: 11666 RVA: 0x000F6236 File Offset: 0x000F4436
		public Location(int line, int pos)
		{
			this.value = (ulong)((long)line << 32 | (long)((ulong)pos));
		}

		// Token: 0x06002D93 RID: 11667 RVA: 0x000F6246 File Offset: 0x000F4446
		public Location(Location that)
		{
			this.value = that.value;
		}

		// Token: 0x06002D94 RID: 11668 RVA: 0x000F6254 File Offset: 0x000F4454
		public bool LessOrEqual(Location that)
		{
			return this.value <= that.value;
		}

		// Token: 0x04001EFE RID: 7934
		private ulong value;
	}
}
