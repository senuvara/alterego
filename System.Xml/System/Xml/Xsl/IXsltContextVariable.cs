﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl
{
	/// <summary>Provides an interface to a given variable that is defined in the style sheet during runtime execution.</summary>
	// Token: 0x020004A7 RID: 1191
	public interface IXsltContextVariable
	{
		/// <summary>Gets a value indicating whether the variable is local.</summary>
		/// <returns>
		///     <see langword="true" /> if the variable is a local variable in the current context; otherwise, <see langword="false" />.</returns>
		// Token: 0x170009EA RID: 2538
		// (get) Token: 0x06002ECD RID: 11981
		bool IsLocal { get; }

		/// <summary>Gets a value indicating whether the variable is an Extensible Stylesheet Language Transformations (XSLT) parameter. This can be a parameter to a style sheet or a template.</summary>
		/// <returns>
		///     <see langword="true" /> if the variable is an XSLT parameter; otherwise, <see langword="false" />.</returns>
		// Token: 0x170009EB RID: 2539
		// (get) Token: 0x06002ECE RID: 11982
		bool IsParam { get; }

		/// <summary>Gets the <see cref="T:System.Xml.XPath.XPathResultType" /> representing the XML Path Language (XPath) type of the variable.</summary>
		/// <returns>The <see cref="T:System.Xml.XPath.XPathResultType" /> representing the XPath type of the variable.</returns>
		// Token: 0x170009EC RID: 2540
		// (get) Token: 0x06002ECF RID: 11983
		XPathResultType VariableType { get; }

		/// <summary>Evaluates the variable at runtime and returns an object that represents the value of the variable.</summary>
		/// <param name="xsltContext">An <see cref="T:System.Xml.Xsl.XsltContext" /> representing the execution context of the variable. </param>
		/// <returns>An <see cref="T:System.Object" /> representing the value of the variable. Possible return types include number, string, Boolean, document fragment, or node set.</returns>
		// Token: 0x06002ED0 RID: 11984
		object Evaluate(XsltContext xsltContext);
	}
}
