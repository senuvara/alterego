﻿using System;

namespace System.Xml.Xsl
{
	// Token: 0x0200048B RID: 1163
	internal struct Int32Pair
	{
		// Token: 0x06002D84 RID: 11652 RVA: 0x000F5F3D File Offset: 0x000F413D
		public Int32Pair(int left, int right)
		{
			this.left = left;
			this.right = right;
		}

		// Token: 0x17000980 RID: 2432
		// (get) Token: 0x06002D85 RID: 11653 RVA: 0x000F5F4D File Offset: 0x000F414D
		public int Left
		{
			get
			{
				return this.left;
			}
		}

		// Token: 0x17000981 RID: 2433
		// (get) Token: 0x06002D86 RID: 11654 RVA: 0x000F5F55 File Offset: 0x000F4155
		public int Right
		{
			get
			{
				return this.right;
			}
		}

		// Token: 0x06002D87 RID: 11655 RVA: 0x000F5F60 File Offset: 0x000F4160
		public override bool Equals(object other)
		{
			if (other is Int32Pair)
			{
				Int32Pair int32Pair = (Int32Pair)other;
				return this.left == int32Pair.left && this.right == int32Pair.right;
			}
			return false;
		}

		// Token: 0x06002D88 RID: 11656 RVA: 0x000F5F9C File Offset: 0x000F419C
		public override int GetHashCode()
		{
			return this.left.GetHashCode() ^ this.right.GetHashCode();
		}

		// Token: 0x04001EF1 RID: 7921
		private int left;

		// Token: 0x04001EF2 RID: 7922
		private int right;
	}
}
