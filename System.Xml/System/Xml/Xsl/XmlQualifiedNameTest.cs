﻿using System;

namespace System.Xml.Xsl
{
	// Token: 0x02000495 RID: 1173
	internal class XmlQualifiedNameTest : XmlQualifiedName
	{
		// Token: 0x17000998 RID: 2456
		// (get) Token: 0x06002DE0 RID: 11744 RVA: 0x000F94B1 File Offset: 0x000F76B1
		public static XmlQualifiedNameTest Wildcard
		{
			get
			{
				return XmlQualifiedNameTest.wc;
			}
		}

		// Token: 0x06002DE1 RID: 11745 RVA: 0x000F94B8 File Offset: 0x000F76B8
		private XmlQualifiedNameTest(string name, string ns, bool exclude) : base(name, ns)
		{
			this.exclude = exclude;
		}

		// Token: 0x06002DE2 RID: 11746 RVA: 0x000F94C9 File Offset: 0x000F76C9
		public static XmlQualifiedNameTest New(string name, string ns)
		{
			if (ns == null && name == null)
			{
				return XmlQualifiedNameTest.Wildcard;
			}
			return new XmlQualifiedNameTest((name == null) ? "*" : name, (ns == null) ? "*" : ns, false);
		}

		// Token: 0x17000999 RID: 2457
		// (get) Token: 0x06002DE3 RID: 11747 RVA: 0x000F94F3 File Offset: 0x000F76F3
		public bool IsWildcard
		{
			get
			{
				return this == XmlQualifiedNameTest.Wildcard;
			}
		}

		// Token: 0x1700099A RID: 2458
		// (get) Token: 0x06002DE4 RID: 11748 RVA: 0x000F94FD File Offset: 0x000F76FD
		public bool IsNameWildcard
		{
			get
			{
				return base.Name == "*";
			}
		}

		// Token: 0x1700099B RID: 2459
		// (get) Token: 0x06002DE5 RID: 11749 RVA: 0x000F950C File Offset: 0x000F770C
		public bool IsNamespaceWildcard
		{
			get
			{
				return base.Namespace == "*";
			}
		}

		// Token: 0x06002DE6 RID: 11750 RVA: 0x000F951B File Offset: 0x000F771B
		private bool IsNameSubsetOf(XmlQualifiedNameTest other)
		{
			return other.IsNameWildcard || base.Name == other.Name;
		}

		// Token: 0x06002DE7 RID: 11751 RVA: 0x000F9538 File Offset: 0x000F7738
		private bool IsNamespaceSubsetOf(XmlQualifiedNameTest other)
		{
			return other.IsNamespaceWildcard || (this.exclude == other.exclude && base.Namespace == other.Namespace) || (other.exclude && !this.exclude && base.Namespace != other.Namespace);
		}

		// Token: 0x06002DE8 RID: 11752 RVA: 0x000F9593 File Offset: 0x000F7793
		public bool IsSubsetOf(XmlQualifiedNameTest other)
		{
			return this.IsNameSubsetOf(other) && this.IsNamespaceSubsetOf(other);
		}

		// Token: 0x06002DE9 RID: 11753 RVA: 0x000F95A7 File Offset: 0x000F77A7
		public bool HasIntersection(XmlQualifiedNameTest other)
		{
			return (this.IsNamespaceSubsetOf(other) || other.IsNamespaceSubsetOf(this)) && (this.IsNameSubsetOf(other) || other.IsNameSubsetOf(this));
		}

		// Token: 0x06002DEA RID: 11754 RVA: 0x000F95D0 File Offset: 0x000F77D0
		public override string ToString()
		{
			if (this == XmlQualifiedNameTest.Wildcard)
			{
				return "*";
			}
			if (base.Namespace.Length == 0)
			{
				return base.Name;
			}
			if (base.Namespace == "*")
			{
				return "*:" + base.Name;
			}
			if (this.exclude)
			{
				return "{~" + base.Namespace + "}:" + base.Name;
			}
			return "{" + base.Namespace + "}:" + base.Name;
		}

		// Token: 0x06002DEB RID: 11755 RVA: 0x000F965C File Offset: 0x000F785C
		// Note: this type is marked as 'beforefieldinit'.
		static XmlQualifiedNameTest()
		{
		}

		// Token: 0x04001F22 RID: 7970
		private bool exclude;

		// Token: 0x04001F23 RID: 7971
		private const string wildcard = "*";

		// Token: 0x04001F24 RID: 7972
		private static XmlQualifiedNameTest wc = XmlQualifiedNameTest.New("*", "*");
	}
}
