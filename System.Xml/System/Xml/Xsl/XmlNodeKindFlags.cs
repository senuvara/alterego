﻿using System;

namespace System.Xml.Xsl
{
	// Token: 0x02000494 RID: 1172
	[Flags]
	internal enum XmlNodeKindFlags
	{
		// Token: 0x04001F18 RID: 7960
		None = 0,
		// Token: 0x04001F19 RID: 7961
		Document = 1,
		// Token: 0x04001F1A RID: 7962
		Element = 2,
		// Token: 0x04001F1B RID: 7963
		Attribute = 4,
		// Token: 0x04001F1C RID: 7964
		Text = 8,
		// Token: 0x04001F1D RID: 7965
		Comment = 16,
		// Token: 0x04001F1E RID: 7966
		PI = 32,
		// Token: 0x04001F1F RID: 7967
		Namespace = 64,
		// Token: 0x04001F20 RID: 7968
		Content = 58,
		// Token: 0x04001F21 RID: 7969
		Any = 127
	}
}
