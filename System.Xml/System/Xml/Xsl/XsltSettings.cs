﻿using System;

namespace System.Xml.Xsl
{
	/// <summary>Specifies the XSLT features to support during execution of the XSLT style sheet.</summary>
	// Token: 0x020004AB RID: 1195
	public sealed class XsltSettings
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Xsl.XsltSettings" /> class with default settings.</summary>
		// Token: 0x06002EED RID: 12013 RVA: 0x000FCC1F File Offset: 0x000FAE1F
		public XsltSettings()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Xsl.XsltSettings" /> class with the specified settings.</summary>
		/// <param name="enableDocumentFunction">
		///       <see langword="true" /> to enable support for the XSLT document() function; otherwise, <see langword="false" />.</param>
		/// <param name="enableScript">
		///       <see langword="true" /> to enable support for embedded scripts blocks; otherwise, <see langword="false" />.</param>
		// Token: 0x06002EEE RID: 12014 RVA: 0x000FCC2E File Offset: 0x000FAE2E
		public XsltSettings(bool enableDocumentFunction, bool enableScript)
		{
			this.enableDocumentFunction = enableDocumentFunction;
			this.enableScript = enableScript;
		}

		/// <summary>Gets an <see cref="T:System.Xml.Xsl.XsltSettings" /> object with default settings. Support for the XSLT document() function and embedded script blocks is disabled.</summary>
		/// <returns>An <see cref="T:System.Xml.Xsl.XsltSettings" /> object with the <see cref="P:System.Xml.Xsl.XsltSettings.EnableDocumentFunction" /> and <see cref="P:System.Xml.Xsl.XsltSettings.EnableScript" /> properties set to <see langword="false" />.</returns>
		// Token: 0x170009F2 RID: 2546
		// (get) Token: 0x06002EEF RID: 12015 RVA: 0x000FCC4B File Offset: 0x000FAE4B
		public static XsltSettings Default
		{
			get
			{
				return new XsltSettings(false, false);
			}
		}

		/// <summary>Gets an <see cref="T:System.Xml.Xsl.XsltSettings" /> object that enables support for the XSLT document() function and embedded script blocks.</summary>
		/// <returns>An <see cref="T:System.Xml.Xsl.XsltSettings" /> object with the <see cref="P:System.Xml.Xsl.XsltSettings.EnableDocumentFunction" /> and <see cref="P:System.Xml.Xsl.XsltSettings.EnableScript" /> properties set to <see langword="true" />.</returns>
		// Token: 0x170009F3 RID: 2547
		// (get) Token: 0x06002EF0 RID: 12016 RVA: 0x000FCC54 File Offset: 0x000FAE54
		public static XsltSettings TrustedXslt
		{
			get
			{
				return new XsltSettings(true, true);
			}
		}

		/// <summary>Gets or sets a value indicating whether to enable support for the XSLT document() function.</summary>
		/// <returns>
		///     <see langword="true" /> to support the XSLT document() function; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170009F4 RID: 2548
		// (get) Token: 0x06002EF1 RID: 12017 RVA: 0x000FCC5D File Offset: 0x000FAE5D
		// (set) Token: 0x06002EF2 RID: 12018 RVA: 0x000FCC65 File Offset: 0x000FAE65
		public bool EnableDocumentFunction
		{
			get
			{
				return this.enableDocumentFunction;
			}
			set
			{
				this.enableDocumentFunction = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to enable support for embedded script blocks.</summary>
		/// <returns>
		///     <see langword="true" /> to support script blocks in XSLT style sheets; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170009F5 RID: 2549
		// (get) Token: 0x06002EF3 RID: 12019 RVA: 0x000FCC6E File Offset: 0x000FAE6E
		// (set) Token: 0x06002EF4 RID: 12020 RVA: 0x000FCC76 File Offset: 0x000FAE76
		public bool EnableScript
		{
			get
			{
				return this.enableScript;
			}
			set
			{
				this.enableScript = value;
			}
		}

		// Token: 0x170009F6 RID: 2550
		// (get) Token: 0x06002EF5 RID: 12021 RVA: 0x000FCC7F File Offset: 0x000FAE7F
		// (set) Token: 0x06002EF6 RID: 12022 RVA: 0x000FCC87 File Offset: 0x000FAE87
		internal bool CheckOnly
		{
			get
			{
				return this.checkOnly;
			}
			set
			{
				this.checkOnly = value;
			}
		}

		// Token: 0x170009F7 RID: 2551
		// (get) Token: 0x06002EF7 RID: 12023 RVA: 0x000FCC90 File Offset: 0x000FAE90
		// (set) Token: 0x06002EF8 RID: 12024 RVA: 0x000FCC98 File Offset: 0x000FAE98
		internal bool IncludeDebugInformation
		{
			get
			{
				return this.includeDebugInformation;
			}
			set
			{
				this.includeDebugInformation = value;
			}
		}

		// Token: 0x170009F8 RID: 2552
		// (get) Token: 0x06002EF9 RID: 12025 RVA: 0x000FCCA1 File Offset: 0x000FAEA1
		// (set) Token: 0x06002EFA RID: 12026 RVA: 0x000FCCA9 File Offset: 0x000FAEA9
		internal int WarningLevel
		{
			get
			{
				return this.warningLevel;
			}
			set
			{
				this.warningLevel = value;
			}
		}

		// Token: 0x170009F9 RID: 2553
		// (get) Token: 0x06002EFB RID: 12027 RVA: 0x000FCCB2 File Offset: 0x000FAEB2
		// (set) Token: 0x06002EFC RID: 12028 RVA: 0x000FCCBA File Offset: 0x000FAEBA
		internal bool TreatWarningsAsErrors
		{
			get
			{
				return this.treatWarningsAsErrors;
			}
			set
			{
				this.treatWarningsAsErrors = value;
			}
		}

		// Token: 0x04001F9B RID: 8091
		private bool enableDocumentFunction;

		// Token: 0x04001F9C RID: 8092
		private bool enableScript;

		// Token: 0x04001F9D RID: 8093
		private bool checkOnly;

		// Token: 0x04001F9E RID: 8094
		private bool includeDebugInformation;

		// Token: 0x04001F9F RID: 8095
		private int warningLevel = -1;

		// Token: 0x04001FA0 RID: 8096
		private bool treatWarningsAsErrors;
	}
}
