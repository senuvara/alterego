﻿using System;

namespace System.Xml.Xsl.Xslt
{
	// Token: 0x02000532 RID: 1330
	internal class KeywordsTable
	{
		// Token: 0x0600335C RID: 13148 RVA: 0x0010C144 File Offset: 0x0010A344
		public KeywordsTable(XmlNameTable nt)
		{
			this.NameTable = nt;
			this.AnalyzeString = nt.Add("analyze-string");
			this.ApplyImports = nt.Add("apply-imports");
			this.ApplyTemplates = nt.Add("apply-templates");
			this.Assembly = nt.Add("assembly");
			this.Attribute = nt.Add("attribute");
			this.AttributeSet = nt.Add("attribute-set");
			this.CallTemplate = nt.Add("call-template");
			this.CaseOrder = nt.Add("case-order");
			this.CDataSectionElements = nt.Add("cdata-section-elements");
			this.Character = nt.Add("character");
			this.CharacterMap = nt.Add("character-map");
			this.Choose = nt.Add("choose");
			this.Comment = nt.Add("comment");
			this.Copy = nt.Add("copy");
			this.CopyOf = nt.Add("copy-of");
			this.Count = nt.Add("count");
			this.DataType = nt.Add("data-type");
			this.DecimalFormat = nt.Add("decimal-format");
			this.DecimalSeparator = nt.Add("decimal-separator");
			this.DefaultCollation = nt.Add("default-collation");
			this.DefaultValidation = nt.Add("default-validation");
			this.Digit = nt.Add("digit");
			this.DisableOutputEscaping = nt.Add("disable-output-escaping");
			this.DocTypePublic = nt.Add("doctype-public");
			this.DocTypeSystem = nt.Add("doctype-system");
			this.Document = nt.Add("document");
			this.Element = nt.Add("element");
			this.Elements = nt.Add("elements");
			this.Encoding = nt.Add("encoding");
			this.ExcludeResultPrefixes = nt.Add("exclude-result-prefixes");
			this.ExtensionElementPrefixes = nt.Add("extension-element-prefixes");
			this.Fallback = nt.Add("fallback");
			this.ForEach = nt.Add("for-each");
			this.ForEachGroup = nt.Add("for-each-group");
			this.Format = nt.Add("format");
			this.From = nt.Add("from");
			this.Function = nt.Add("function");
			this.GroupingSeparator = nt.Add("grouping-separator");
			this.GroupingSize = nt.Add("grouping-size");
			this.Href = nt.Add("href");
			this.Id = nt.Add("id");
			this.If = nt.Add("if");
			this.ImplementsPrefix = nt.Add("implements-prefix");
			this.Import = nt.Add("import");
			this.ImportSchema = nt.Add("import-schema");
			this.Include = nt.Add("include");
			this.Indent = nt.Add("indent");
			this.Infinity = nt.Add("infinity");
			this.Key = nt.Add("key");
			this.Lang = nt.Add("lang");
			this.Language = nt.Add("language");
			this.LetterValue = nt.Add("letter-value");
			this.Level = nt.Add("level");
			this.Match = nt.Add("match");
			this.MatchingSubstring = nt.Add("matching-substring");
			this.MediaType = nt.Add("media-type");
			this.Message = nt.Add("message");
			this.Method = nt.Add("method");
			this.MinusSign = nt.Add("minus-sign");
			this.Mode = nt.Add("mode");
			this.Name = nt.Add("name");
			this.Namespace = nt.Add("namespace");
			this.NamespaceAlias = nt.Add("namespace-alias");
			this.NaN = nt.Add("NaN");
			this.NextMatch = nt.Add("next-match");
			this.NonMatchingSubstring = nt.Add("non-matching-substring");
			this.Number = nt.Add("number");
			this.OmitXmlDeclaration = nt.Add("omit-xml-declaration");
			this.Otherwise = nt.Add("otherwise");
			this.Order = nt.Add("order");
			this.Output = nt.Add("output");
			this.OutputCharacter = nt.Add("output-character");
			this.OutputVersion = nt.Add("output-version");
			this.Param = nt.Add("param");
			this.PatternSeparator = nt.Add("pattern-separator");
			this.Percent = nt.Add("percent");
			this.PerformSort = nt.Add("perform-sort");
			this.PerMille = nt.Add("per-mille");
			this.PreserveSpace = nt.Add("preserve-space");
			this.Priority = nt.Add("priority");
			this.ProcessingInstruction = nt.Add("processing-instruction");
			this.Required = nt.Add("required");
			this.ResultDocument = nt.Add("result-document");
			this.ResultPrefix = nt.Add("result-prefix");
			this.Script = nt.Add("script");
			this.Select = nt.Add("select");
			this.Separator = nt.Add("separator");
			this.Sequence = nt.Add("sequence");
			this.Sort = nt.Add("sort");
			this.Space = nt.Add("space");
			this.Standalone = nt.Add("standalone");
			this.StripSpace = nt.Add("strip-space");
			this.Stylesheet = nt.Add("stylesheet");
			this.StylesheetPrefix = nt.Add("stylesheet-prefix");
			this.Template = nt.Add("template");
			this.Terminate = nt.Add("terminate");
			this.Test = nt.Add("test");
			this.Text = nt.Add("text");
			this.Transform = nt.Add("transform");
			this.UrnMsxsl = nt.Add("urn:schemas-microsoft-com:xslt");
			this.UriXml = nt.Add("http://www.w3.org/XML/1998/namespace");
			this.UriXsl = nt.Add("http://www.w3.org/1999/XSL/Transform");
			this.UriWdXsl = nt.Add("http://www.w3.org/TR/WD-xsl");
			this.Use = nt.Add("use");
			this.UseAttributeSets = nt.Add("use-attribute-sets");
			this.UseWhen = nt.Add("use-when");
			this.Using = nt.Add("using");
			this.Value = nt.Add("value");
			this.ValueOf = nt.Add("value-of");
			this.Variable = nt.Add("variable");
			this.Version = nt.Add("version");
			this.When = nt.Add("when");
			this.WithParam = nt.Add("with-param");
			this.Xml = nt.Add("xml");
			this.Xmlns = nt.Add("xmlns");
			this.XPathDefaultNamespace = nt.Add("xpath-default-namespace");
			this.ZeroDigit = nt.Add("zero-digit");
		}

		// Token: 0x040021F6 RID: 8694
		public XmlNameTable NameTable;

		// Token: 0x040021F7 RID: 8695
		public string AnalyzeString;

		// Token: 0x040021F8 RID: 8696
		public string ApplyImports;

		// Token: 0x040021F9 RID: 8697
		public string ApplyTemplates;

		// Token: 0x040021FA RID: 8698
		public string Assembly;

		// Token: 0x040021FB RID: 8699
		public string Attribute;

		// Token: 0x040021FC RID: 8700
		public string AttributeSet;

		// Token: 0x040021FD RID: 8701
		public string CallTemplate;

		// Token: 0x040021FE RID: 8702
		public string CaseOrder;

		// Token: 0x040021FF RID: 8703
		public string CDataSectionElements;

		// Token: 0x04002200 RID: 8704
		public string Character;

		// Token: 0x04002201 RID: 8705
		public string CharacterMap;

		// Token: 0x04002202 RID: 8706
		public string Choose;

		// Token: 0x04002203 RID: 8707
		public string Comment;

		// Token: 0x04002204 RID: 8708
		public string Copy;

		// Token: 0x04002205 RID: 8709
		public string CopyOf;

		// Token: 0x04002206 RID: 8710
		public string Count;

		// Token: 0x04002207 RID: 8711
		public string DataType;

		// Token: 0x04002208 RID: 8712
		public string DecimalFormat;

		// Token: 0x04002209 RID: 8713
		public string DecimalSeparator;

		// Token: 0x0400220A RID: 8714
		public string DefaultCollation;

		// Token: 0x0400220B RID: 8715
		public string DefaultValidation;

		// Token: 0x0400220C RID: 8716
		public string Digit;

		// Token: 0x0400220D RID: 8717
		public string DisableOutputEscaping;

		// Token: 0x0400220E RID: 8718
		public string DocTypePublic;

		// Token: 0x0400220F RID: 8719
		public string DocTypeSystem;

		// Token: 0x04002210 RID: 8720
		public string Document;

		// Token: 0x04002211 RID: 8721
		public string Element;

		// Token: 0x04002212 RID: 8722
		public string Elements;

		// Token: 0x04002213 RID: 8723
		public string Encoding;

		// Token: 0x04002214 RID: 8724
		public string ExcludeResultPrefixes;

		// Token: 0x04002215 RID: 8725
		public string ExtensionElementPrefixes;

		// Token: 0x04002216 RID: 8726
		public string Fallback;

		// Token: 0x04002217 RID: 8727
		public string ForEach;

		// Token: 0x04002218 RID: 8728
		public string ForEachGroup;

		// Token: 0x04002219 RID: 8729
		public string Format;

		// Token: 0x0400221A RID: 8730
		public string From;

		// Token: 0x0400221B RID: 8731
		public string Function;

		// Token: 0x0400221C RID: 8732
		public string GroupingSeparator;

		// Token: 0x0400221D RID: 8733
		public string GroupingSize;

		// Token: 0x0400221E RID: 8734
		public string Href;

		// Token: 0x0400221F RID: 8735
		public string Id;

		// Token: 0x04002220 RID: 8736
		public string If;

		// Token: 0x04002221 RID: 8737
		public string ImplementsPrefix;

		// Token: 0x04002222 RID: 8738
		public string Import;

		// Token: 0x04002223 RID: 8739
		public string ImportSchema;

		// Token: 0x04002224 RID: 8740
		public string Include;

		// Token: 0x04002225 RID: 8741
		public string Indent;

		// Token: 0x04002226 RID: 8742
		public string Infinity;

		// Token: 0x04002227 RID: 8743
		public string Key;

		// Token: 0x04002228 RID: 8744
		public string Lang;

		// Token: 0x04002229 RID: 8745
		public string Language;

		// Token: 0x0400222A RID: 8746
		public string LetterValue;

		// Token: 0x0400222B RID: 8747
		public string Level;

		// Token: 0x0400222C RID: 8748
		public string Match;

		// Token: 0x0400222D RID: 8749
		public string MatchingSubstring;

		// Token: 0x0400222E RID: 8750
		public string MediaType;

		// Token: 0x0400222F RID: 8751
		public string Message;

		// Token: 0x04002230 RID: 8752
		public string Method;

		// Token: 0x04002231 RID: 8753
		public string MinusSign;

		// Token: 0x04002232 RID: 8754
		public string Mode;

		// Token: 0x04002233 RID: 8755
		public string Name;

		// Token: 0x04002234 RID: 8756
		public string Namespace;

		// Token: 0x04002235 RID: 8757
		public string NamespaceAlias;

		// Token: 0x04002236 RID: 8758
		public string NaN;

		// Token: 0x04002237 RID: 8759
		public string NextMatch;

		// Token: 0x04002238 RID: 8760
		public string NonMatchingSubstring;

		// Token: 0x04002239 RID: 8761
		public string Number;

		// Token: 0x0400223A RID: 8762
		public string OmitXmlDeclaration;

		// Token: 0x0400223B RID: 8763
		public string Order;

		// Token: 0x0400223C RID: 8764
		public string Otherwise;

		// Token: 0x0400223D RID: 8765
		public string Output;

		// Token: 0x0400223E RID: 8766
		public string OutputCharacter;

		// Token: 0x0400223F RID: 8767
		public string OutputVersion;

		// Token: 0x04002240 RID: 8768
		public string Param;

		// Token: 0x04002241 RID: 8769
		public string PatternSeparator;

		// Token: 0x04002242 RID: 8770
		public string Percent;

		// Token: 0x04002243 RID: 8771
		public string PerformSort;

		// Token: 0x04002244 RID: 8772
		public string PerMille;

		// Token: 0x04002245 RID: 8773
		public string PreserveSpace;

		// Token: 0x04002246 RID: 8774
		public string Priority;

		// Token: 0x04002247 RID: 8775
		public string ProcessingInstruction;

		// Token: 0x04002248 RID: 8776
		public string Required;

		// Token: 0x04002249 RID: 8777
		public string ResultDocument;

		// Token: 0x0400224A RID: 8778
		public string ResultPrefix;

		// Token: 0x0400224B RID: 8779
		public string Script;

		// Token: 0x0400224C RID: 8780
		public string Select;

		// Token: 0x0400224D RID: 8781
		public string Separator;

		// Token: 0x0400224E RID: 8782
		public string Sequence;

		// Token: 0x0400224F RID: 8783
		public string Sort;

		// Token: 0x04002250 RID: 8784
		public string Space;

		// Token: 0x04002251 RID: 8785
		public string Standalone;

		// Token: 0x04002252 RID: 8786
		public string StripSpace;

		// Token: 0x04002253 RID: 8787
		public string Stylesheet;

		// Token: 0x04002254 RID: 8788
		public string StylesheetPrefix;

		// Token: 0x04002255 RID: 8789
		public string Template;

		// Token: 0x04002256 RID: 8790
		public string Terminate;

		// Token: 0x04002257 RID: 8791
		public string Test;

		// Token: 0x04002258 RID: 8792
		public string Text;

		// Token: 0x04002259 RID: 8793
		public string Transform;

		// Token: 0x0400225A RID: 8794
		public string UrnMsxsl;

		// Token: 0x0400225B RID: 8795
		public string UriXml;

		// Token: 0x0400225C RID: 8796
		public string UriXsl;

		// Token: 0x0400225D RID: 8797
		public string UriWdXsl;

		// Token: 0x0400225E RID: 8798
		public string Use;

		// Token: 0x0400225F RID: 8799
		public string UseAttributeSets;

		// Token: 0x04002260 RID: 8800
		public string UseWhen;

		// Token: 0x04002261 RID: 8801
		public string Using;

		// Token: 0x04002262 RID: 8802
		public string Value;

		// Token: 0x04002263 RID: 8803
		public string ValueOf;

		// Token: 0x04002264 RID: 8804
		public string Variable;

		// Token: 0x04002265 RID: 8805
		public string Version;

		// Token: 0x04002266 RID: 8806
		public string When;

		// Token: 0x04002267 RID: 8807
		public string WithParam;

		// Token: 0x04002268 RID: 8808
		public string Xml;

		// Token: 0x04002269 RID: 8809
		public string Xmlns;

		// Token: 0x0400226A RID: 8810
		public string XPathDefaultNamespace;

		// Token: 0x0400226B RID: 8811
		public string ZeroDigit;
	}
}
