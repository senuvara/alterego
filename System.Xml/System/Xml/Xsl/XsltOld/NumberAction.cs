﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml.XPath;
using System.Xml.Xsl.Runtime;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004F5 RID: 1269
	internal class NumberAction : ContainerAction
	{
		// Token: 0x06003149 RID: 12617 RVA: 0x001036E8 File Offset: 0x001018E8
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string text = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Level))
			{
				if (text != "any" && text != "multiple" && text != "single")
				{
					throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
					{
						"level",
						text
					});
				}
				this.level = text;
			}
			else if (Ref.Equal(localName, compiler.Atoms.Count))
			{
				this.countPattern = text;
				this.countKey = compiler.AddQuery(text, true, true, true);
			}
			else if (Ref.Equal(localName, compiler.Atoms.From))
			{
				this.from = text;
				this.fromKey = compiler.AddQuery(text, true, true, true);
			}
			else if (Ref.Equal(localName, compiler.Atoms.Value))
			{
				this.value = text;
				this.valueKey = compiler.AddQuery(text);
			}
			else if (Ref.Equal(localName, compiler.Atoms.Format))
			{
				this.formatAvt = Avt.CompileAvt(compiler, text);
			}
			else if (Ref.Equal(localName, compiler.Atoms.Lang))
			{
				this.langAvt = Avt.CompileAvt(compiler, text);
			}
			else if (Ref.Equal(localName, compiler.Atoms.LetterValue))
			{
				this.letterAvt = Avt.CompileAvt(compiler, text);
			}
			else if (Ref.Equal(localName, compiler.Atoms.GroupingSeparator))
			{
				this.groupingSepAvt = Avt.CompileAvt(compiler, text);
			}
			else
			{
				if (!Ref.Equal(localName, compiler.Atoms.GroupingSize))
				{
					return false;
				}
				this.groupingSizeAvt = Avt.CompileAvt(compiler, text);
			}
			return true;
		}

		// Token: 0x0600314A RID: 12618 RVA: 0x001038AC File Offset: 0x00101AAC
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckEmpty(compiler);
			this.forwardCompatibility = compiler.ForwardCompatibility;
			this.formatTokens = NumberAction.ParseFormat(CompiledAction.PrecalculateAvt(ref this.formatAvt));
			this.letter = this.ParseLetter(CompiledAction.PrecalculateAvt(ref this.letterAvt));
			this.lang = CompiledAction.PrecalculateAvt(ref this.langAvt);
			this.groupingSep = CompiledAction.PrecalculateAvt(ref this.groupingSepAvt);
			if (this.groupingSep != null && this.groupingSep.Length > 1)
			{
				throw XsltException.Create("The value of the '{0}' attribute must be a single character.", new string[]
				{
					"grouping-separator"
				});
			}
			this.groupingSize = CompiledAction.PrecalculateAvt(ref this.groupingSizeAvt);
		}

		// Token: 0x0600314B RID: 12619 RVA: 0x00103964 File Offset: 0x00101B64
		private int numberAny(Processor processor, ActionFrame frame)
		{
			int num = 0;
			XPathNavigator xpathNavigator = frame.Node;
			if (xpathNavigator.NodeType == XPathNodeType.Attribute || xpathNavigator.NodeType == XPathNodeType.Namespace)
			{
				xpathNavigator = xpathNavigator.Clone();
				xpathNavigator.MoveToParent();
			}
			XPathNavigator xpathNavigator2 = xpathNavigator.Clone();
			if (this.fromKey != -1)
			{
				bool flag = false;
				while (!processor.Matches(xpathNavigator2, this.fromKey))
				{
					if (!xpathNavigator2.MoveToParent())
					{
						IL_56:
						XPathNodeIterator xpathNodeIterator = xpathNavigator2.SelectDescendants(XPathNodeType.All, true);
						while (xpathNodeIterator.MoveNext())
						{
							if (processor.Matches(xpathNodeIterator.Current, this.fromKey))
							{
								flag = true;
								num = 0;
							}
							else if (this.MatchCountKey(processor, frame.Node, xpathNodeIterator.Current))
							{
								num++;
							}
							if (xpathNodeIterator.Current.IsSamePosition(xpathNavigator))
							{
								break;
							}
						}
						if (!flag)
						{
							return 0;
						}
						return num;
					}
				}
				flag = true;
				goto IL_56;
			}
			xpathNavigator2.MoveToRoot();
			XPathNodeIterator xpathNodeIterator2 = xpathNavigator2.SelectDescendants(XPathNodeType.All, true);
			while (xpathNodeIterator2.MoveNext())
			{
				if (this.MatchCountKey(processor, frame.Node, xpathNodeIterator2.Current))
				{
					num++;
				}
				if (xpathNodeIterator2.Current.IsSamePosition(xpathNavigator))
				{
					break;
				}
			}
			return num;
		}

		// Token: 0x0600314C RID: 12620 RVA: 0x00103A6E File Offset: 0x00101C6E
		private bool checkFrom(Processor processor, XPathNavigator nav)
		{
			if (this.fromKey == -1)
			{
				return true;
			}
			while (!processor.Matches(nav, this.fromKey))
			{
				if (!nav.MoveToParent())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600314D RID: 12621 RVA: 0x00103A95 File Offset: 0x00101C95
		private bool moveToCount(XPathNavigator nav, Processor processor, XPathNavigator contextNode)
		{
			while (this.fromKey == -1 || !processor.Matches(nav, this.fromKey))
			{
				if (this.MatchCountKey(processor, contextNode, nav))
				{
					return true;
				}
				if (!nav.MoveToParent())
				{
					return false;
				}
			}
			return false;
		}

		// Token: 0x0600314E RID: 12622 RVA: 0x00103AC8 File Offset: 0x00101CC8
		private int numberCount(XPathNavigator nav, Processor processor, XPathNavigator contextNode)
		{
			XPathNavigator xpathNavigator = nav.Clone();
			int num = 1;
			if (xpathNavigator.MoveToParent())
			{
				xpathNavigator.MoveToFirstChild();
				while (!xpathNavigator.IsSamePosition(nav))
				{
					if (this.MatchCountKey(processor, contextNode, xpathNavigator))
					{
						num++;
					}
					if (!xpathNavigator.MoveToNext())
					{
						break;
					}
				}
			}
			return num;
		}

		// Token: 0x0600314F RID: 12623 RVA: 0x00103B10 File Offset: 0x00101D10
		private static object SimplifyValue(object value)
		{
			if (Type.GetTypeCode(value.GetType()) == TypeCode.Object)
			{
				XPathNodeIterator xpathNodeIterator = value as XPathNodeIterator;
				if (xpathNodeIterator != null)
				{
					if (xpathNodeIterator.MoveNext())
					{
						return xpathNodeIterator.Current.Value;
					}
					return string.Empty;
				}
				else
				{
					XPathNavigator xpathNavigator = value as XPathNavigator;
					if (xpathNavigator != null)
					{
						return xpathNavigator.Value;
					}
				}
			}
			return value;
		}

		// Token: 0x06003150 RID: 12624 RVA: 0x00103B64 File Offset: 0x00101D64
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			ArrayList numberList = processor.NumberList;
			int state = frame.State;
			if (state != 0)
			{
				if (state != 2)
				{
					return;
				}
			}
			else
			{
				numberList.Clear();
				if (this.valueKey != -1)
				{
					numberList.Add(NumberAction.SimplifyValue(processor.Evaluate(frame, this.valueKey)));
				}
				else if (this.level == "any")
				{
					int num = this.numberAny(processor, frame);
					if (num != 0)
					{
						numberList.Add(num);
					}
				}
				else
				{
					bool flag = this.level == "multiple";
					XPathNavigator node = frame.Node;
					XPathNavigator xpathNavigator = frame.Node.Clone();
					if (xpathNavigator.NodeType == XPathNodeType.Attribute || xpathNavigator.NodeType == XPathNodeType.Namespace)
					{
						xpathNavigator.MoveToParent();
					}
					while (this.moveToCount(xpathNavigator, processor, node))
					{
						numberList.Insert(0, this.numberCount(xpathNavigator, processor, node));
						if (!flag || !xpathNavigator.MoveToParent())
						{
							break;
						}
					}
					if (!this.checkFrom(processor, xpathNavigator))
					{
						numberList.Clear();
					}
				}
				frame.StoredOutput = NumberAction.Format(numberList, (this.formatAvt == null) ? this.formatTokens : NumberAction.ParseFormat(this.formatAvt.Evaluate(processor, frame)), (this.langAvt == null) ? this.lang : this.langAvt.Evaluate(processor, frame), (this.letterAvt == null) ? this.letter : this.ParseLetter(this.letterAvt.Evaluate(processor, frame)), (this.groupingSepAvt == null) ? this.groupingSep : this.groupingSepAvt.Evaluate(processor, frame), (this.groupingSizeAvt == null) ? this.groupingSize : this.groupingSizeAvt.Evaluate(processor, frame));
			}
			if (!processor.TextEvent(frame.StoredOutput))
			{
				frame.State = 2;
				return;
			}
			frame.Finished();
		}

		// Token: 0x06003151 RID: 12625 RVA: 0x00103D34 File Offset: 0x00101F34
		private bool MatchCountKey(Processor processor, XPathNavigator contextNode, XPathNavigator nav)
		{
			if (this.countKey != -1)
			{
				return processor.Matches(nav, this.countKey);
			}
			return contextNode.Name == nav.Name && this.BasicNodeType(contextNode.NodeType) == this.BasicNodeType(nav.NodeType);
		}

		// Token: 0x06003152 RID: 12626 RVA: 0x00103D88 File Offset: 0x00101F88
		private XPathNodeType BasicNodeType(XPathNodeType type)
		{
			if (type == XPathNodeType.SignificantWhitespace || type == XPathNodeType.Whitespace)
			{
				return XPathNodeType.Text;
			}
			return type;
		}

		// Token: 0x06003153 RID: 12627 RVA: 0x00103D98 File Offset: 0x00101F98
		private static string Format(ArrayList numberlist, List<NumberAction.FormatInfo> tokens, string lang, string letter, string groupingSep, string groupingSize)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int num = 0;
			if (tokens != null)
			{
				num = tokens.Count;
			}
			NumberAction.NumberingFormat numberingFormat = new NumberAction.NumberingFormat();
			if (groupingSize != null)
			{
				try
				{
					numberingFormat.setGroupingSize(Convert.ToInt32(groupingSize, CultureInfo.InvariantCulture));
				}
				catch (FormatException)
				{
				}
				catch (OverflowException)
				{
				}
			}
			if (groupingSep != null)
			{
				int length = groupingSep.Length;
				numberingFormat.setGroupingSeparator(groupingSep);
			}
			if (0 < num)
			{
				NumberAction.FormatInfo formatInfo = tokens[0];
				NumberAction.FormatInfo formatInfo2 = null;
				if (num % 2 == 1)
				{
					formatInfo2 = tokens[num - 1];
					num--;
				}
				NumberAction.FormatInfo formatInfo3 = (2 < num) ? tokens[num - 2] : NumberAction.DefaultSeparator;
				NumberAction.FormatInfo formatInfo4 = (0 < num) ? tokens[num - 1] : NumberAction.DefaultFormat;
				if (formatInfo != null)
				{
					stringBuilder.Append(formatInfo.formatString);
				}
				int count = numberlist.Count;
				for (int i = 0; i < count; i++)
				{
					int num2 = i * 2;
					bool flag = num2 < num;
					if (0 < i)
					{
						NumberAction.FormatInfo formatInfo5 = flag ? tokens[num2] : formatInfo3;
						stringBuilder.Append(formatInfo5.formatString);
					}
					NumberAction.FormatInfo formatInfo6 = flag ? tokens[num2 + 1] : formatInfo4;
					numberingFormat.setNumberingType(formatInfo6.numSequence);
					numberingFormat.setMinLen(formatInfo6.length);
					stringBuilder.Append(numberingFormat.FormatItem(numberlist[i]));
				}
				if (formatInfo2 != null)
				{
					stringBuilder.Append(formatInfo2.formatString);
				}
			}
			else
			{
				numberingFormat.setNumberingType(NumberingSequence.FirstDecimal);
				for (int j = 0; j < numberlist.Count; j++)
				{
					if (j != 0)
					{
						stringBuilder.Append(".");
					}
					stringBuilder.Append(numberingFormat.FormatItem(numberlist[j]));
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06003154 RID: 12628 RVA: 0x00103F5C File Offset: 0x0010215C
		private static void mapFormatToken(string wsToken, int startLen, int tokLen, out NumberingSequence seq, out int pminlen)
		{
			char c = wsToken[startLen];
			bool flag = false;
			pminlen = 1;
			seq = NumberingSequence.Nil;
			int num = (int)c;
			if (num <= 2406)
			{
				if (num != 48 && num != 2406)
				{
					goto IL_71;
				}
			}
			else if (num != 3664 && num != 51067 && num != 65296)
			{
				goto IL_71;
			}
			do
			{
				pminlen++;
			}
			while (--tokLen > 0 && c == wsToken[++startLen]);
			if (wsToken[startLen] != c + '\u0001')
			{
				flag = true;
			}
			IL_71:
			if (!flag)
			{
				num = (int)wsToken[startLen];
				if (num <= 3665)
				{
					if (num <= 1072)
					{
						if (num <= 73)
						{
							if (num == 49)
							{
								seq = NumberingSequence.FirstDecimal;
								goto IL_31F;
							}
							if (num == 65)
							{
								seq = NumberingSequence.FirstAlpha;
								goto IL_31F;
							}
							if (num == 73)
							{
								seq = NumberingSequence.FirstSpecial;
								goto IL_31F;
							}
						}
						else if (num <= 105)
						{
							if (num == 97)
							{
								seq = NumberingSequence.LCLetter;
								goto IL_31F;
							}
							if (num == 105)
							{
								seq = NumberingSequence.LCRoman;
								goto IL_31F;
							}
						}
						else
						{
							if (num == 1040)
							{
								seq = NumberingSequence.UCRus;
								goto IL_31F;
							}
							if (num == 1072)
							{
								seq = NumberingSequence.LCRus;
								goto IL_31F;
							}
						}
					}
					else if (num <= 2309)
					{
						if (num == 1488)
						{
							seq = NumberingSequence.Hebrew;
							goto IL_31F;
						}
						if (num == 1571)
						{
							seq = NumberingSequence.ArabicScript;
							goto IL_31F;
						}
						if (num == 2309)
						{
							seq = NumberingSequence.Hindi2;
							goto IL_31F;
						}
					}
					else if (num <= 2407)
					{
						if (num == 2325)
						{
							seq = NumberingSequence.Hindi1;
							goto IL_31F;
						}
						if (num == 2407)
						{
							seq = NumberingSequence.Hindi3;
							goto IL_31F;
						}
					}
					else
					{
						if (num == 3585)
						{
							seq = NumberingSequence.Thai1;
							goto IL_31F;
						}
						if (num == 3665)
						{
							seq = NumberingSequence.Thai2;
							goto IL_31F;
						}
					}
				}
				else if (num <= 23376)
				{
					if (num <= 12593)
					{
						if (num == 12450)
						{
							seq = NumberingSequence.DAiueo;
							goto IL_31F;
						}
						if (num == 12452)
						{
							seq = NumberingSequence.DIroha;
							goto IL_31F;
						}
						if (num == 12593)
						{
							seq = NumberingSequence.DChosung;
							goto IL_31F;
						}
					}
					else if (num <= 22769)
					{
						if (num == 19968)
						{
							seq = NumberingSequence.FEDecimal;
							goto IL_31F;
						}
						if (num == 22769)
						{
							seq = NumberingSequence.DbNum3;
							goto IL_31F;
						}
					}
					else
					{
						if (num == 22777)
						{
							seq = NumberingSequence.ChnCmplx;
							goto IL_31F;
						}
						if (num == 23376)
						{
							seq = NumberingSequence.Zodiac2;
							goto IL_31F;
						}
					}
				}
				else if (num <= 51068)
				{
					if (num != 30002)
					{
						if (num == 44032)
						{
							seq = NumberingSequence.Ganada;
							goto IL_31F;
						}
						if (num == 51068)
						{
							seq = NumberingSequence.KorDbNum1;
							goto IL_31F;
						}
					}
					else
					{
						if (tokLen > 1 && wsToken[startLen + 1] == '子')
						{
							seq = NumberingSequence.Zodiac3;
							tokLen--;
							startLen++;
							goto IL_31F;
						}
						seq = NumberingSequence.Zodiac1;
						goto IL_31F;
					}
				}
				else if (num <= 65297)
				{
					if (num == 54616)
					{
						seq = NumberingSequence.KorDbNum3;
						goto IL_31F;
					}
					if (num == 65297)
					{
						seq = NumberingSequence.DArabic;
						goto IL_31F;
					}
				}
				else
				{
					if (num == 65393)
					{
						seq = NumberingSequence.Aiueo;
						goto IL_31F;
					}
					if (num == 65394)
					{
						seq = NumberingSequence.Iroha;
						goto IL_31F;
					}
				}
				seq = NumberingSequence.FirstDecimal;
			}
			IL_31F:
			if (flag)
			{
				seq = NumberingSequence.FirstDecimal;
				pminlen = 0;
			}
		}

		// Token: 0x06003155 RID: 12629 RVA: 0x00104294 File Offset: 0x00102494
		private static List<NumberAction.FormatInfo> ParseFormat(string formatString)
		{
			if (formatString == null || formatString.Length == 0)
			{
				return null;
			}
			int i = 0;
			bool flag = CharUtil.IsAlphaNumeric(formatString[i]);
			List<NumberAction.FormatInfo> list = new List<NumberAction.FormatInfo>();
			int num = 0;
			if (flag)
			{
				list.Add(null);
			}
			while (i <= formatString.Length)
			{
				bool flag2 = (i < formatString.Length) ? CharUtil.IsAlphaNumeric(formatString[i]) : (!flag);
				if (flag != flag2)
				{
					NumberAction.FormatInfo formatInfo = new NumberAction.FormatInfo();
					if (flag)
					{
						NumberAction.mapFormatToken(formatString, num, i - num, out formatInfo.numSequence, out formatInfo.length);
					}
					else
					{
						formatInfo.isSeparator = true;
						formatInfo.formatString = formatString.Substring(num, i - num);
					}
					num = i;
					i++;
					list.Add(formatInfo);
					flag = flag2;
				}
				else
				{
					i++;
				}
			}
			return list;
		}

		// Token: 0x06003156 RID: 12630 RVA: 0x00104354 File Offset: 0x00102554
		private string ParseLetter(string letter)
		{
			if (letter == null || letter == "traditional" || letter == "alphabetic")
			{
				return letter;
			}
			if (!this.forwardCompatibility)
			{
				throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
				{
					"letter-value",
					letter
				});
			}
			return null;
		}

		// Token: 0x06003157 RID: 12631 RVA: 0x001043A6 File Offset: 0x001025A6
		public NumberAction()
		{
		}

		// Token: 0x06003158 RID: 12632 RVA: 0x001043C3 File Offset: 0x001025C3
		// Note: this type is marked as 'beforefieldinit'.
		static NumberAction()
		{
		}

		// Token: 0x04002085 RID: 8325
		private const long msofnfcNil = 0L;

		// Token: 0x04002086 RID: 8326
		private const long msofnfcTraditional = 1L;

		// Token: 0x04002087 RID: 8327
		private const long msofnfcAlwaysFormat = 2L;

		// Token: 0x04002088 RID: 8328
		private const int cchMaxFormat = 63;

		// Token: 0x04002089 RID: 8329
		private const int cchMaxFormatDecimal = 11;

		// Token: 0x0400208A RID: 8330
		private static NumberAction.FormatInfo DefaultFormat = new NumberAction.FormatInfo(false, "0");

		// Token: 0x0400208B RID: 8331
		private static NumberAction.FormatInfo DefaultSeparator = new NumberAction.FormatInfo(true, ".");

		// Token: 0x0400208C RID: 8332
		private const int OutputNumber = 2;

		// Token: 0x0400208D RID: 8333
		private string level;

		// Token: 0x0400208E RID: 8334
		private string countPattern;

		// Token: 0x0400208F RID: 8335
		private int countKey = -1;

		// Token: 0x04002090 RID: 8336
		private string from;

		// Token: 0x04002091 RID: 8337
		private int fromKey = -1;

		// Token: 0x04002092 RID: 8338
		private string value;

		// Token: 0x04002093 RID: 8339
		private int valueKey = -1;

		// Token: 0x04002094 RID: 8340
		private Avt formatAvt;

		// Token: 0x04002095 RID: 8341
		private Avt langAvt;

		// Token: 0x04002096 RID: 8342
		private Avt letterAvt;

		// Token: 0x04002097 RID: 8343
		private Avt groupingSepAvt;

		// Token: 0x04002098 RID: 8344
		private Avt groupingSizeAvt;

		// Token: 0x04002099 RID: 8345
		private List<NumberAction.FormatInfo> formatTokens;

		// Token: 0x0400209A RID: 8346
		private string lang;

		// Token: 0x0400209B RID: 8347
		private string letter;

		// Token: 0x0400209C RID: 8348
		private string groupingSep;

		// Token: 0x0400209D RID: 8349
		private string groupingSize;

		// Token: 0x0400209E RID: 8350
		private bool forwardCompatibility;

		// Token: 0x020004F6 RID: 1270
		internal class FormatInfo
		{
			// Token: 0x06003159 RID: 12633 RVA: 0x001043E5 File Offset: 0x001025E5
			public FormatInfo(bool isSeparator, string formatString)
			{
				this.isSeparator = isSeparator;
				this.formatString = formatString;
			}

			// Token: 0x0600315A RID: 12634 RVA: 0x00002103 File Offset: 0x00000303
			public FormatInfo()
			{
			}

			// Token: 0x0400209F RID: 8351
			public bool isSeparator;

			// Token: 0x040020A0 RID: 8352
			public NumberingSequence numSequence;

			// Token: 0x040020A1 RID: 8353
			public int length;

			// Token: 0x040020A2 RID: 8354
			public string formatString;
		}

		// Token: 0x020004F7 RID: 1271
		private class NumberingFormat : NumberFormatterBase
		{
			// Token: 0x0600315B RID: 12635 RVA: 0x001043FB File Offset: 0x001025FB
			internal NumberingFormat()
			{
			}

			// Token: 0x0600315C RID: 12636 RVA: 0x00104403 File Offset: 0x00102603
			internal void setNumberingType(NumberingSequence seq)
			{
				this.seq = seq;
			}

			// Token: 0x0600315D RID: 12637 RVA: 0x0010440C File Offset: 0x0010260C
			internal void setMinLen(int cMinLen)
			{
				this.cMinLen = cMinLen;
			}

			// Token: 0x0600315E RID: 12638 RVA: 0x00104415 File Offset: 0x00102615
			internal void setGroupingSeparator(string separator)
			{
				this.separator = separator;
			}

			// Token: 0x0600315F RID: 12639 RVA: 0x0010441E File Offset: 0x0010261E
			internal void setGroupingSize(int sizeGroup)
			{
				if (0 <= sizeGroup && sizeGroup <= 9)
				{
					this.sizeGroup = sizeGroup;
				}
			}

			// Token: 0x06003160 RID: 12640 RVA: 0x00104430 File Offset: 0x00102630
			internal string FormatItem(object value)
			{
				double num;
				if (value is int)
				{
					num = (double)((int)value);
				}
				else
				{
					num = XmlConvert.ToXPathDouble(value);
					if (0.5 > num || double.IsPositiveInfinity(num))
					{
						return XmlConvert.ToXPathString(value);
					}
					num = XmlConvert.XPathRound(num);
				}
				NumberingSequence numberingSequence = this.seq;
				if (numberingSequence != NumberingSequence.FirstDecimal)
				{
					if (numberingSequence - NumberingSequence.FirstAlpha > 1)
					{
						if (numberingSequence - NumberingSequence.FirstSpecial <= 1)
						{
							if (num <= 32767.0)
							{
								StringBuilder stringBuilder = new StringBuilder();
								NumberFormatterBase.ConvertToRoman(stringBuilder, num, this.seq == NumberingSequence.FirstSpecial);
								return stringBuilder.ToString();
							}
						}
					}
					else if (num <= 2147483647.0)
					{
						StringBuilder stringBuilder2 = new StringBuilder();
						NumberFormatterBase.ConvertToAlphabetic(stringBuilder2, num, (this.seq == NumberingSequence.FirstAlpha) ? 'A' : 'a', 26);
						return stringBuilder2.ToString();
					}
				}
				return NumberAction.NumberingFormat.ConvertToArabic(num, this.cMinLen, this.sizeGroup, this.separator);
			}

			// Token: 0x06003161 RID: 12641 RVA: 0x00104504 File Offset: 0x00102704
			private static string ConvertToArabic(double val, int minLength, int groupSize, string groupSeparator)
			{
				string text;
				if (groupSize != 0 && groupSeparator != null)
				{
					NumberFormatInfo numberFormatInfo = new NumberFormatInfo();
					numberFormatInfo.NumberGroupSizes = new int[]
					{
						groupSize
					};
					numberFormatInfo.NumberGroupSeparator = groupSeparator;
					if (Math.Floor(val) == val)
					{
						numberFormatInfo.NumberDecimalDigits = 0;
					}
					text = val.ToString("N", numberFormatInfo);
				}
				else
				{
					text = Convert.ToString(val, CultureInfo.InvariantCulture);
				}
				if (text.Length >= minLength)
				{
					return text;
				}
				StringBuilder stringBuilder = new StringBuilder(minLength);
				stringBuilder.Append('0', minLength - text.Length);
				stringBuilder.Append(text);
				return stringBuilder.ToString();
			}

			// Token: 0x040020A3 RID: 8355
			private NumberingSequence seq;

			// Token: 0x040020A4 RID: 8356
			private int cMinLen;

			// Token: 0x040020A5 RID: 8357
			private string separator;

			// Token: 0x040020A6 RID: 8358
			private int sizeGroup;
		}
	}
}
