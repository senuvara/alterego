﻿using System;
using System.Collections;
using System.Text;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200052D RID: 1325
	internal class XsltOutput : CompiledAction
	{
		// Token: 0x17000AB8 RID: 2744
		// (get) Token: 0x06003342 RID: 13122 RVA: 0x0010BC8D File Offset: 0x00109E8D
		internal XsltOutput.OutputMethod Method
		{
			get
			{
				return this.method;
			}
		}

		// Token: 0x17000AB9 RID: 2745
		// (get) Token: 0x06003343 RID: 13123 RVA: 0x0010BC95 File Offset: 0x00109E95
		internal bool OmitXmlDeclaration
		{
			get
			{
				return this.omitXmlDecl;
			}
		}

		// Token: 0x17000ABA RID: 2746
		// (get) Token: 0x06003344 RID: 13124 RVA: 0x0010BC9D File Offset: 0x00109E9D
		internal bool HasStandalone
		{
			get
			{
				return this.standaloneSId != int.MaxValue;
			}
		}

		// Token: 0x17000ABB RID: 2747
		// (get) Token: 0x06003345 RID: 13125 RVA: 0x0010BCAF File Offset: 0x00109EAF
		internal bool Standalone
		{
			get
			{
				return this.standalone;
			}
		}

		// Token: 0x17000ABC RID: 2748
		// (get) Token: 0x06003346 RID: 13126 RVA: 0x0010BCB7 File Offset: 0x00109EB7
		internal string DoctypePublic
		{
			get
			{
				return this.doctypePublic;
			}
		}

		// Token: 0x17000ABD RID: 2749
		// (get) Token: 0x06003347 RID: 13127 RVA: 0x0010BCBF File Offset: 0x00109EBF
		internal string DoctypeSystem
		{
			get
			{
				return this.doctypeSystem;
			}
		}

		// Token: 0x17000ABE RID: 2750
		// (get) Token: 0x06003348 RID: 13128 RVA: 0x0010BCC7 File Offset: 0x00109EC7
		internal Hashtable CDataElements
		{
			get
			{
				return this.cdataElements;
			}
		}

		// Token: 0x17000ABF RID: 2751
		// (get) Token: 0x06003349 RID: 13129 RVA: 0x0010BCCF File Offset: 0x00109ECF
		internal bool Indent
		{
			get
			{
				return this.indent;
			}
		}

		// Token: 0x17000AC0 RID: 2752
		// (get) Token: 0x0600334A RID: 13130 RVA: 0x0010BCD7 File Offset: 0x00109ED7
		internal Encoding Encoding
		{
			get
			{
				return this.encoding;
			}
		}

		// Token: 0x17000AC1 RID: 2753
		// (get) Token: 0x0600334B RID: 13131 RVA: 0x0010BCDF File Offset: 0x00109EDF
		internal string MediaType
		{
			get
			{
				return this.mediaType;
			}
		}

		// Token: 0x0600334C RID: 13132 RVA: 0x0010BCE8 File Offset: 0x00109EE8
		internal XsltOutput CreateDerivedOutput(XsltOutput.OutputMethod method)
		{
			XsltOutput xsltOutput = (XsltOutput)base.MemberwiseClone();
			xsltOutput.method = method;
			if (method == XsltOutput.OutputMethod.Html && this.indentSId == 2147483647)
			{
				xsltOutput.indent = true;
			}
			return xsltOutput;
		}

		// Token: 0x0600334D RID: 13133 RVA: 0x0010BD21 File Offset: 0x00109F21
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckEmpty(compiler);
		}

		// Token: 0x0600334E RID: 13134 RVA: 0x0010BD34 File Offset: 0x00109F34
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Method))
			{
				if (compiler.Stylesheetid <= this.methodSId)
				{
					this.method = XsltOutput.ParseOutputMethod(value, compiler);
					this.methodSId = compiler.Stylesheetid;
					if (this.indentSId == 2147483647)
					{
						this.indent = (this.method == XsltOutput.OutputMethod.Html);
					}
				}
			}
			else if (Ref.Equal(localName, compiler.Atoms.Version))
			{
				if (compiler.Stylesheetid <= this.versionSId)
				{
					this.version = value;
					this.versionSId = compiler.Stylesheetid;
				}
			}
			else
			{
				if (Ref.Equal(localName, compiler.Atoms.Encoding))
				{
					if (compiler.Stylesheetid > this.encodingSId)
					{
						return true;
					}
					try
					{
						this.encoding = Encoding.GetEncoding(value);
						this.encodingSId = compiler.Stylesheetid;
						return true;
					}
					catch (NotSupportedException)
					{
						return true;
					}
					catch (ArgumentException)
					{
						return true;
					}
				}
				if (Ref.Equal(localName, compiler.Atoms.OmitXmlDeclaration))
				{
					if (compiler.Stylesheetid <= this.omitXmlDeclSId)
					{
						this.omitXmlDecl = compiler.GetYesNo(value);
						this.omitXmlDeclSId = compiler.Stylesheetid;
					}
				}
				else if (Ref.Equal(localName, compiler.Atoms.Standalone))
				{
					if (compiler.Stylesheetid <= this.standaloneSId)
					{
						this.standalone = compiler.GetYesNo(value);
						this.standaloneSId = compiler.Stylesheetid;
					}
				}
				else if (Ref.Equal(localName, compiler.Atoms.DocTypePublic))
				{
					if (compiler.Stylesheetid <= this.doctypePublicSId)
					{
						this.doctypePublic = value;
						this.doctypePublicSId = compiler.Stylesheetid;
					}
				}
				else if (Ref.Equal(localName, compiler.Atoms.DocTypeSystem))
				{
					if (compiler.Stylesheetid <= this.doctypeSystemSId)
					{
						this.doctypeSystem = value;
						this.doctypeSystemSId = compiler.Stylesheetid;
					}
				}
				else if (Ref.Equal(localName, compiler.Atoms.Indent))
				{
					if (compiler.Stylesheetid <= this.indentSId)
					{
						this.indent = compiler.GetYesNo(value);
						this.indentSId = compiler.Stylesheetid;
					}
				}
				else if (Ref.Equal(localName, compiler.Atoms.MediaType))
				{
					if (compiler.Stylesheetid <= this.mediaTypeSId)
					{
						this.mediaType = value;
						this.mediaTypeSId = compiler.Stylesheetid;
					}
				}
				else
				{
					if (!Ref.Equal(localName, compiler.Atoms.CDataSectionElements))
					{
						return false;
					}
					string[] array = XmlConvert.SplitString(value);
					if (this.cdataElements == null)
					{
						this.cdataElements = new Hashtable(array.Length);
					}
					for (int i = 0; i < array.Length; i++)
					{
						XmlQualifiedName xmlQualifiedName = compiler.CreateXmlQName(array[i]);
						this.cdataElements[xmlQualifiedName] = xmlQualifiedName;
					}
				}
			}
			return true;
		}

		// Token: 0x0600334F RID: 13135 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void Execute(Processor processor, ActionFrame frame)
		{
		}

		// Token: 0x06003350 RID: 13136 RVA: 0x0010C030 File Offset: 0x0010A230
		private static XsltOutput.OutputMethod ParseOutputMethod(string value, Compiler compiler)
		{
			XmlQualifiedName xmlQualifiedName = compiler.CreateXPathQName(value);
			if (xmlQualifiedName.Namespace.Length != 0)
			{
				return XsltOutput.OutputMethod.Other;
			}
			string name = xmlQualifiedName.Name;
			if (name == "xml")
			{
				return XsltOutput.OutputMethod.Xml;
			}
			if (name == "html")
			{
				return XsltOutput.OutputMethod.Html;
			}
			if (name == "text")
			{
				return XsltOutput.OutputMethod.Text;
			}
			if (compiler.ForwardCompatibility)
			{
				return XsltOutput.OutputMethod.Unknown;
			}
			throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
			{
				"method",
				value
			});
		}

		// Token: 0x06003351 RID: 13137 RVA: 0x0010C0B0 File Offset: 0x0010A2B0
		public XsltOutput()
		{
		}

		// Token: 0x040021DD RID: 8669
		private XsltOutput.OutputMethod method = XsltOutput.OutputMethod.Unknown;

		// Token: 0x040021DE RID: 8670
		private int methodSId = int.MaxValue;

		// Token: 0x040021DF RID: 8671
		private Encoding encoding = Encoding.UTF8;

		// Token: 0x040021E0 RID: 8672
		private int encodingSId = int.MaxValue;

		// Token: 0x040021E1 RID: 8673
		private string version;

		// Token: 0x040021E2 RID: 8674
		private int versionSId = int.MaxValue;

		// Token: 0x040021E3 RID: 8675
		private bool omitXmlDecl;

		// Token: 0x040021E4 RID: 8676
		private int omitXmlDeclSId = int.MaxValue;

		// Token: 0x040021E5 RID: 8677
		private bool standalone;

		// Token: 0x040021E6 RID: 8678
		private int standaloneSId = int.MaxValue;

		// Token: 0x040021E7 RID: 8679
		private string doctypePublic;

		// Token: 0x040021E8 RID: 8680
		private int doctypePublicSId = int.MaxValue;

		// Token: 0x040021E9 RID: 8681
		private string doctypeSystem;

		// Token: 0x040021EA RID: 8682
		private int doctypeSystemSId = int.MaxValue;

		// Token: 0x040021EB RID: 8683
		private bool indent;

		// Token: 0x040021EC RID: 8684
		private int indentSId = int.MaxValue;

		// Token: 0x040021ED RID: 8685
		private string mediaType = "text/html";

		// Token: 0x040021EE RID: 8686
		private int mediaTypeSId = int.MaxValue;

		// Token: 0x040021EF RID: 8687
		private Hashtable cdataElements;

		// Token: 0x0200052E RID: 1326
		internal enum OutputMethod
		{
			// Token: 0x040021F1 RID: 8689
			Xml,
			// Token: 0x040021F2 RID: 8690
			Html,
			// Token: 0x040021F3 RID: 8691
			Text,
			// Token: 0x040021F4 RID: 8692
			Other,
			// Token: 0x040021F5 RID: 8693
			Unknown
		}
	}
}
