﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004BB RID: 1211
	internal class CommentAction : ContainerAction
	{
		// Token: 0x06002F8E RID: 12174 RVA: 0x000FE529 File Offset: 0x000FC729
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			if (compiler.Recurse())
			{
				base.CompileTemplate(compiler);
				compiler.ToParent();
			}
		}

		// Token: 0x06002F8F RID: 12175 RVA: 0x000FE548 File Offset: 0x000FC748
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 1)
				{
					return;
				}
				if (processor.EndEvent(XPathNodeType.Comment))
				{
					frame.Finished();
				}
			}
			else if (processor.BeginEvent(XPathNodeType.Comment, string.Empty, string.Empty, string.Empty, false))
			{
				processor.PushActionFrame(frame);
				frame.State = 1;
				return;
			}
		}

		// Token: 0x06002F90 RID: 12176 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		public CommentAction()
		{
		}
	}
}
