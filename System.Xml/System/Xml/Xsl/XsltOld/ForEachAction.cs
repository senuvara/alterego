﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004E8 RID: 1256
	internal class ForEachAction : ContainerAction
	{
		// Token: 0x060030DC RID: 12508 RVA: 0x00101FB0 File Offset: 0x001001B0
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.selectKey != -1, "select");
			compiler.CanHaveApplyImports = false;
			if (compiler.Recurse())
			{
				this.CompileSortElements(compiler);
				base.CompileTemplate(compiler);
				compiler.ToParent();
			}
		}

		// Token: 0x060030DD RID: 12509 RVA: 0x00102000 File Offset: 0x00100200
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Select))
			{
				this.selectKey = compiler.AddQuery(value);
				return true;
			}
			return false;
		}

		// Token: 0x060030DE RID: 12510 RVA: 0x00102048 File Offset: 0x00100248
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			switch (frame.State)
			{
			case 0:
				if (this.sortContainer != null)
				{
					processor.InitSortArray();
					processor.PushActionFrame(this.sortContainer, frame.NodeSet);
					frame.State = 2;
					return;
				}
				break;
			case 1:
				return;
			case 2:
				break;
			case 3:
				goto IL_82;
			case 4:
				goto IL_9B;
			case 5:
				frame.State = 3;
				goto IL_82;
			default:
				return;
			}
			frame.InitNewNodeSet(processor.StartQuery(frame.NodeSet, this.selectKey));
			if (this.sortContainer != null)
			{
				frame.SortNewNodeSet(processor, processor.SortArray);
			}
			frame.State = 3;
			IL_82:
			if (!frame.NewNextNode(processor))
			{
				frame.Finished();
				return;
			}
			frame.State = 4;
			IL_9B:
			processor.PushActionFrame(frame, frame.NewNodeSet);
			frame.State = 5;
		}

		// Token: 0x060030DF RID: 12511 RVA: 0x00102110 File Offset: 0x00100310
		protected void CompileSortElements(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			for (;;)
			{
				switch (input.NodeType)
				{
				case XPathNodeType.Element:
					if (!Ref.Equal(input.NamespaceURI, input.Atoms.UriXsl) || !Ref.Equal(input.LocalName, input.Atoms.Sort))
					{
						return;
					}
					if (this.sortContainer == null)
					{
						this.sortContainer = new ContainerAction();
					}
					this.sortContainer.AddAction(compiler.CreateSortAction());
					break;
				case XPathNodeType.Text:
					return;
				case XPathNodeType.SignificantWhitespace:
					base.AddEvent(compiler.CreateTextEvent());
					break;
				}
				if (!input.Advance())
				{
					return;
				}
			}
		}

		// Token: 0x060030E0 RID: 12512 RVA: 0x001021B8 File Offset: 0x001003B8
		public ForEachAction()
		{
		}

		// Token: 0x04002051 RID: 8273
		private const int ProcessedSort = 2;

		// Token: 0x04002052 RID: 8274
		private const int ProcessNextNode = 3;

		// Token: 0x04002053 RID: 8275
		private const int PositionAdvanced = 4;

		// Token: 0x04002054 RID: 8276
		private const int ContentsProcessed = 5;

		// Token: 0x04002055 RID: 8277
		private int selectKey = -1;

		// Token: 0x04002056 RID: 8278
		private ContainerAction sortContainer;
	}
}
