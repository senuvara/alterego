﻿using System;
using System.Globalization;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000509 RID: 1289
	internal class SortAction : CompiledAction
	{
		// Token: 0x0600327A RID: 12922 RVA: 0x001089B0 File Offset: 0x00106BB0
		private string ParseLang(string value)
		{
			if (value == null)
			{
				return null;
			}
			if (XmlComplianceUtil.IsValidLanguageID(value.ToCharArray(), 0, value.Length) || (value.Length != 0 && CultureInfo.GetCultureInfo(value) != null))
			{
				return value;
			}
			if (this.forwardCompatibility)
			{
				return null;
			}
			throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
			{
				"lang",
				value
			});
		}

		// Token: 0x0600327B RID: 12923 RVA: 0x00108A10 File Offset: 0x00106C10
		private XmlDataType ParseDataType(string value, InputScopeManager manager)
		{
			if (value == null)
			{
				return XmlDataType.Text;
			}
			if (value == "text")
			{
				return XmlDataType.Text;
			}
			if (value == "number")
			{
				return XmlDataType.Number;
			}
			string text;
			string text2;
			PrefixQName.ParseQualifiedName(value, out text, out text2);
			manager.ResolveXmlNamespace(text);
			if (text.Length == 0 && !this.forwardCompatibility)
			{
				throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
				{
					"data-type",
					value
				});
			}
			return XmlDataType.Text;
		}

		// Token: 0x0600327C RID: 12924 RVA: 0x00108A80 File Offset: 0x00106C80
		private XmlSortOrder ParseOrder(string value)
		{
			if (value == null)
			{
				return XmlSortOrder.Ascending;
			}
			if (value == "ascending")
			{
				return XmlSortOrder.Ascending;
			}
			if (value == "descending")
			{
				return XmlSortOrder.Descending;
			}
			if (this.forwardCompatibility)
			{
				return XmlSortOrder.Ascending;
			}
			throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
			{
				"order",
				value
			});
		}

		// Token: 0x0600327D RID: 12925 RVA: 0x00108AD8 File Offset: 0x00106CD8
		private XmlCaseOrder ParseCaseOrder(string value)
		{
			if (value == null)
			{
				return XmlCaseOrder.None;
			}
			if (value == "upper-first")
			{
				return XmlCaseOrder.UpperFirst;
			}
			if (value == "lower-first")
			{
				return XmlCaseOrder.LowerFirst;
			}
			if (this.forwardCompatibility)
			{
				return XmlCaseOrder.None;
			}
			throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
			{
				"case-order",
				value
			});
		}

		// Token: 0x0600327E RID: 12926 RVA: 0x00108B30 File Offset: 0x00106D30
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckEmpty(compiler);
			if (this.selectKey == -1)
			{
				this.selectKey = compiler.AddQuery(".");
			}
			this.forwardCompatibility = compiler.ForwardCompatibility;
			this.manager = compiler.CloneScopeManager();
			this.lang = this.ParseLang(CompiledAction.PrecalculateAvt(ref this.langAvt));
			this.dataType = this.ParseDataType(CompiledAction.PrecalculateAvt(ref this.dataTypeAvt), this.manager);
			this.order = this.ParseOrder(CompiledAction.PrecalculateAvt(ref this.orderAvt));
			this.caseOrder = this.ParseCaseOrder(CompiledAction.PrecalculateAvt(ref this.caseOrderAvt));
			if (this.langAvt == null && this.dataTypeAvt == null && this.orderAvt == null && this.caseOrderAvt == null)
			{
				this.sort = new Sort(this.selectKey, this.lang, this.dataType, this.order, this.caseOrder);
			}
		}

		// Token: 0x0600327F RID: 12927 RVA: 0x00108C28 File Offset: 0x00106E28
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Select))
			{
				this.selectKey = compiler.AddQuery(value);
			}
			else if (Ref.Equal(localName, compiler.Atoms.Lang))
			{
				this.langAvt = Avt.CompileAvt(compiler, value);
			}
			else if (Ref.Equal(localName, compiler.Atoms.DataType))
			{
				this.dataTypeAvt = Avt.CompileAvt(compiler, value);
			}
			else if (Ref.Equal(localName, compiler.Atoms.Order))
			{
				this.orderAvt = Avt.CompileAvt(compiler, value);
			}
			else
			{
				if (!Ref.Equal(localName, compiler.Atoms.CaseOrder))
				{
					return false;
				}
				this.caseOrderAvt = Avt.CompileAvt(compiler, value);
			}
			return true;
		}

		// Token: 0x06003280 RID: 12928 RVA: 0x00108D00 File Offset: 0x00106F00
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			processor.AddSort((this.sort != null) ? this.sort : new Sort(this.selectKey, (this.langAvt == null) ? this.lang : this.ParseLang(this.langAvt.Evaluate(processor, frame)), (this.dataTypeAvt == null) ? this.dataType : this.ParseDataType(this.dataTypeAvt.Evaluate(processor, frame), this.manager), (this.orderAvt == null) ? this.order : this.ParseOrder(this.orderAvt.Evaluate(processor, frame)), (this.caseOrderAvt == null) ? this.caseOrder : this.ParseCaseOrder(this.caseOrderAvt.Evaluate(processor, frame))));
			frame.Finished();
		}

		// Token: 0x06003281 RID: 12929 RVA: 0x00108DC9 File Offset: 0x00106FC9
		public SortAction()
		{
		}

		// Token: 0x0400215F RID: 8543
		private int selectKey = -1;

		// Token: 0x04002160 RID: 8544
		private Avt langAvt;

		// Token: 0x04002161 RID: 8545
		private Avt dataTypeAvt;

		// Token: 0x04002162 RID: 8546
		private Avt orderAvt;

		// Token: 0x04002163 RID: 8547
		private Avt caseOrderAvt;

		// Token: 0x04002164 RID: 8548
		private string lang;

		// Token: 0x04002165 RID: 8549
		private XmlDataType dataType = XmlDataType.Text;

		// Token: 0x04002166 RID: 8550
		private XmlSortOrder order = XmlSortOrder.Ascending;

		// Token: 0x04002167 RID: 8551
		private XmlCaseOrder caseOrder;

		// Token: 0x04002168 RID: 8552
		private Sort sort;

		// Token: 0x04002169 RID: 8553
		private bool forwardCompatibility;

		// Token: 0x0400216A RID: 8554
		private InputScopeManager manager;
	}
}
