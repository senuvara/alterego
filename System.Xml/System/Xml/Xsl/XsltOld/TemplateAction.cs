﻿using System;
using System.Xml.XPath;
using MS.Internal.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200050E RID: 1294
	internal class TemplateAction : TemplateBaseAction
	{
		// Token: 0x17000A9F RID: 2719
		// (get) Token: 0x060032A7 RID: 12967 RVA: 0x0010975D File Offset: 0x0010795D
		internal int MatchKey
		{
			get
			{
				return this.matchKey;
			}
		}

		// Token: 0x17000AA0 RID: 2720
		// (get) Token: 0x060032A8 RID: 12968 RVA: 0x00109765 File Offset: 0x00107965
		internal XmlQualifiedName Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000AA1 RID: 2721
		// (get) Token: 0x060032A9 RID: 12969 RVA: 0x0010976D File Offset: 0x0010796D
		internal double Priority
		{
			get
			{
				return this.priority;
			}
		}

		// Token: 0x17000AA2 RID: 2722
		// (get) Token: 0x060032AA RID: 12970 RVA: 0x00109775 File Offset: 0x00107975
		internal XmlQualifiedName Mode
		{
			get
			{
				return this.mode;
			}
		}

		// Token: 0x17000AA3 RID: 2723
		// (get) Token: 0x060032AB RID: 12971 RVA: 0x0010977D File Offset: 0x0010797D
		// (set) Token: 0x060032AC RID: 12972 RVA: 0x00109785 File Offset: 0x00107985
		internal int TemplateId
		{
			get
			{
				return this.templateId;
			}
			set
			{
				this.templateId = value;
			}
		}

		// Token: 0x060032AD RID: 12973 RVA: 0x00109790 File Offset: 0x00107990
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			if (this.matchKey == -1)
			{
				if (this.name == null)
				{
					throw XsltException.Create("The 'xsl:template' instruction must have the 'match' and/or 'name' attribute present.", Array.Empty<string>());
				}
				if (this.mode != null)
				{
					throw XsltException.Create("An 'xsl:template' element without a 'match' attribute cannot have a 'mode' attribute.", Array.Empty<string>());
				}
			}
			compiler.BeginTemplate(this);
			if (compiler.Recurse())
			{
				this.CompileParameters(compiler);
				base.CompileTemplate(compiler);
				compiler.ToParent();
			}
			compiler.EndTemplate();
			this.AnalyzePriority(compiler);
		}

		// Token: 0x060032AE RID: 12974 RVA: 0x0010981A File Offset: 0x00107A1A
		internal virtual void CompileSingle(Compiler compiler)
		{
			this.matchKey = compiler.AddQuery("/", false, true, true);
			this.priority = 0.5;
			base.CompileOnceTemplate(compiler);
		}

		// Token: 0x060032AF RID: 12975 RVA: 0x00109848 File Offset: 0x00107A48
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Match))
			{
				this.matchKey = compiler.AddQuery(value, false, true, true);
			}
			else if (Ref.Equal(localName, compiler.Atoms.Name))
			{
				this.name = compiler.CreateXPathQName(value);
			}
			else if (Ref.Equal(localName, compiler.Atoms.Priority))
			{
				this.priority = XmlConvert.ToXPathDouble(value);
				if (double.IsNaN(this.priority) && !compiler.ForwardCompatibility)
				{
					throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
					{
						"priority",
						value
					});
				}
			}
			else
			{
				if (!Ref.Equal(localName, compiler.Atoms.Mode))
				{
					return false;
				}
				if (compiler.AllowBuiltInMode && value == "*")
				{
					this.mode = Compiler.BuiltInMode;
				}
				else
				{
					this.mode = compiler.CreateXPathQName(value);
				}
			}
			return true;
		}

		// Token: 0x060032B0 RID: 12976 RVA: 0x00109954 File Offset: 0x00107B54
		private void AnalyzePriority(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			if (!double.IsNaN(this.priority) || this.matchKey == -1)
			{
				return;
			}
			TheQuery theQuery = compiler.QueryStore[this.MatchKey];
			CompiledXpathExpr compiledQuery = theQuery.CompiledQuery;
			Query query = compiledQuery.QueryTree;
			UnionExpr unionExpr;
			while ((unionExpr = (query as UnionExpr)) != null)
			{
				TemplateAction templateAction = this.CloneWithoutName();
				compiler.QueryStore.Add(new TheQuery(new CompiledXpathExpr(unionExpr.qy2, compiledQuery.Expression, false), theQuery._ScopeManager));
				templateAction.matchKey = compiler.QueryStore.Count - 1;
				templateAction.priority = unionExpr.qy2.XsltDefaultPriority;
				compiler.AddTemplate(templateAction);
				query = unionExpr.qy1;
			}
			if (compiledQuery.QueryTree != query)
			{
				compiler.QueryStore[this.MatchKey] = new TheQuery(new CompiledXpathExpr(query, compiledQuery.Expression, false), theQuery._ScopeManager);
			}
			this.priority = query.XsltDefaultPriority;
		}

		// Token: 0x060032B1 RID: 12977 RVA: 0x00109A50 File Offset: 0x00107C50
		protected void CompileParameters(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			for (;;)
			{
				switch (input.NodeType)
				{
				case XPathNodeType.Element:
					if (!Ref.Equal(input.NamespaceURI, input.Atoms.UriXsl) || !Ref.Equal(input.LocalName, input.Atoms.Param))
					{
						return;
					}
					compiler.PushNamespaceScope();
					base.AddAction(compiler.CreateVariableAction(VariableType.LocalParameter));
					compiler.PopScope();
					break;
				case XPathNodeType.Text:
					return;
				case XPathNodeType.SignificantWhitespace:
					base.AddEvent(compiler.CreateTextEvent());
					break;
				}
				if (!input.Advance())
				{
					return;
				}
			}
		}

		// Token: 0x060032B2 RID: 12978 RVA: 0x00109AED File Offset: 0x00107CED
		private TemplateAction CloneWithoutName()
		{
			return new TemplateAction
			{
				containedActions = this.containedActions,
				mode = this.mode,
				variableCount = this.variableCount,
				replaceNSAliasesDone = true
			};
		}

		// Token: 0x060032B3 RID: 12979 RVA: 0x00109B1F File Offset: 0x00107D1F
		internal override void ReplaceNamespaceAlias(Compiler compiler)
		{
			if (!this.replaceNSAliasesDone)
			{
				base.ReplaceNamespaceAlias(compiler);
				this.replaceNSAliasesDone = true;
			}
		}

		// Token: 0x060032B4 RID: 12980 RVA: 0x00109B38 File Offset: 0x00107D38
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 1)
				{
					return;
				}
				frame.Finished();
				return;
			}
			else
			{
				if (this.variableCount > 0)
				{
					frame.AllocateVariables(this.variableCount);
				}
				if (this.containedActions != null && this.containedActions.Count > 0)
				{
					processor.PushActionFrame(frame);
					frame.State = 1;
					return;
				}
				frame.Finished();
				return;
			}
		}

		// Token: 0x060032B5 RID: 12981 RVA: 0x00109B9B File Offset: 0x00107D9B
		public TemplateAction()
		{
		}

		// Token: 0x040021A0 RID: 8608
		private int matchKey = -1;

		// Token: 0x040021A1 RID: 8609
		private XmlQualifiedName name;

		// Token: 0x040021A2 RID: 8610
		private double priority = double.NaN;

		// Token: 0x040021A3 RID: 8611
		private XmlQualifiedName mode;

		// Token: 0x040021A4 RID: 8612
		private int templateId;

		// Token: 0x040021A5 RID: 8613
		private bool replaceNSAliasesDone;
	}
}
