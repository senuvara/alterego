﻿using System;
using System.Collections;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000506 RID: 1286
	internal struct DocumentKeyList
	{
		// Token: 0x0600324A RID: 12874 RVA: 0x00107650 File Offset: 0x00105850
		public DocumentKeyList(XPathNavigator rootNav, Hashtable keyTable)
		{
			this.rootNav = rootNav;
			this.keyTable = keyTable;
		}

		// Token: 0x17000A92 RID: 2706
		// (get) Token: 0x0600324B RID: 12875 RVA: 0x00107660 File Offset: 0x00105860
		public XPathNavigator RootNav
		{
			get
			{
				return this.rootNav;
			}
		}

		// Token: 0x17000A93 RID: 2707
		// (get) Token: 0x0600324C RID: 12876 RVA: 0x00107668 File Offset: 0x00105868
		public Hashtable KeyTable
		{
			get
			{
				return this.keyTable;
			}
		}

		// Token: 0x0400211F RID: 8479
		private XPathNavigator rootNav;

		// Token: 0x04002120 RID: 8480
		private Hashtable keyTable;
	}
}
