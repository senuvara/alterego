﻿using System;
using System.Globalization;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004FA RID: 1274
	internal class OutputScopeManager
	{
		// Token: 0x17000A5D RID: 2653
		// (get) Token: 0x0600317C RID: 12668 RVA: 0x00104792 File Offset: 0x00102992
		internal string DefaultNamespace
		{
			get
			{
				return this.defaultNS;
			}
		}

		// Token: 0x17000A5E RID: 2654
		// (get) Token: 0x0600317D RID: 12669 RVA: 0x0010479A File Offset: 0x0010299A
		internal OutputScope CurrentElementScope
		{
			get
			{
				return (OutputScope)this.elementScopesStack.Peek();
			}
		}

		// Token: 0x17000A5F RID: 2655
		// (get) Token: 0x0600317E RID: 12670 RVA: 0x001047AC File Offset: 0x001029AC
		internal XmlSpace XmlSpace
		{
			get
			{
				return this.CurrentElementScope.Space;
			}
		}

		// Token: 0x17000A60 RID: 2656
		// (get) Token: 0x0600317F RID: 12671 RVA: 0x001047B9 File Offset: 0x001029B9
		internal string XmlLang
		{
			get
			{
				return this.CurrentElementScope.Lang;
			}
		}

		// Token: 0x06003180 RID: 12672 RVA: 0x001047C8 File Offset: 0x001029C8
		internal OutputScopeManager(XmlNameTable nameTable, OutKeywords atoms)
		{
			this.elementScopesStack = new HWStack(10);
			this.nameTable = nameTable;
			this.atoms = atoms;
			this.defaultNS = this.atoms.Empty;
			OutputScope outputScope = (OutputScope)this.elementScopesStack.Push();
			if (outputScope == null)
			{
				outputScope = new OutputScope();
				this.elementScopesStack.AddToTop(outputScope);
			}
			outputScope.Init(string.Empty, string.Empty, string.Empty, XmlSpace.None, string.Empty, false);
		}

		// Token: 0x06003181 RID: 12673 RVA: 0x00104849 File Offset: 0x00102A49
		internal void PushNamespace(string prefix, string nspace)
		{
			this.CurrentElementScope.AddNamespace(prefix, nspace, this.defaultNS);
			if (prefix == null || prefix.Length == 0)
			{
				this.defaultNS = nspace;
			}
		}

		// Token: 0x06003182 RID: 12674 RVA: 0x00104874 File Offset: 0x00102A74
		internal void PushScope(string name, string nspace, string prefix)
		{
			OutputScope currentElementScope = this.CurrentElementScope;
			OutputScope outputScope = (OutputScope)this.elementScopesStack.Push();
			if (outputScope == null)
			{
				outputScope = new OutputScope();
				this.elementScopesStack.AddToTop(outputScope);
			}
			outputScope.Init(name, nspace, prefix, currentElementScope.Space, currentElementScope.Lang, currentElementScope.Mixed);
		}

		// Token: 0x06003183 RID: 12675 RVA: 0x001048CC File Offset: 0x00102ACC
		internal void PopScope()
		{
			for (NamespaceDecl namespaceDecl = ((OutputScope)this.elementScopesStack.Pop()).Scopes; namespaceDecl != null; namespaceDecl = namespaceDecl.Next)
			{
				this.defaultNS = namespaceDecl.PrevDefaultNsUri;
			}
		}

		// Token: 0x06003184 RID: 12676 RVA: 0x00104908 File Offset: 0x00102B08
		internal string ResolveNamespace(string prefix)
		{
			bool flag;
			return this.ResolveNamespace(prefix, out flag);
		}

		// Token: 0x06003185 RID: 12677 RVA: 0x00104920 File Offset: 0x00102B20
		internal string ResolveNamespace(string prefix, out bool thisScope)
		{
			thisScope = true;
			if (prefix == null || prefix.Length == 0)
			{
				return this.defaultNS;
			}
			if (Ref.Equal(prefix, this.atoms.Xml))
			{
				return this.atoms.XmlNamespace;
			}
			if (Ref.Equal(prefix, this.atoms.Xmlns))
			{
				return this.atoms.XmlnsNamespace;
			}
			for (int i = this.elementScopesStack.Length - 1; i >= 0; i--)
			{
				string text = ((OutputScope)this.elementScopesStack[i]).ResolveAtom(prefix);
				if (text != null)
				{
					thisScope = (i == this.elementScopesStack.Length - 1);
					return text;
				}
			}
			return null;
		}

		// Token: 0x06003186 RID: 12678 RVA: 0x001049C8 File Offset: 0x00102BC8
		internal bool FindPrefix(string nspace, out string prefix)
		{
			int num = this.elementScopesStack.Length - 1;
			while (0 <= num)
			{
				OutputScope outputScope = (OutputScope)this.elementScopesStack[num];
				string text = null;
				if (outputScope.FindPrefix(nspace, out text))
				{
					string text2 = this.ResolveNamespace(text);
					if (text2 != null && Ref.Equal(text2, nspace))
					{
						prefix = text;
						return true;
					}
					break;
				}
				else
				{
					num--;
				}
			}
			prefix = null;
			return false;
		}

		// Token: 0x06003187 RID: 12679 RVA: 0x00104A28 File Offset: 0x00102C28
		internal string GeneratePrefix(string format)
		{
			string array;
			do
			{
				IFormatProvider invariantCulture = CultureInfo.InvariantCulture;
				int num = this.prefixIndex;
				this.prefixIndex = num + 1;
				array = string.Format(invariantCulture, format, num);
			}
			while (this.nameTable.Get(array) != null);
			return this.nameTable.Add(array);
		}

		// Token: 0x040020B6 RID: 8374
		private const int STACK_INCREMENT = 10;

		// Token: 0x040020B7 RID: 8375
		private HWStack elementScopesStack;

		// Token: 0x040020B8 RID: 8376
		private string defaultNS;

		// Token: 0x040020B9 RID: 8377
		private OutKeywords atoms;

		// Token: 0x040020BA RID: 8378
		private XmlNameTable nameTable;

		// Token: 0x040020BB RID: 8379
		private int prefixIndex;
	}
}
