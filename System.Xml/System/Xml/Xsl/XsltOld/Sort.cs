﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004BD RID: 1213
	internal class Sort
	{
		// Token: 0x06002F99 RID: 12185 RVA: 0x000FE6DD File Offset: 0x000FC8DD
		public Sort(int sortkey, string xmllang, XmlDataType datatype, XmlSortOrder xmlorder, XmlCaseOrder xmlcaseorder)
		{
			this.select = sortkey;
			this.lang = xmllang;
			this.dataType = datatype;
			this.order = xmlorder;
			this.caseOrder = xmlcaseorder;
		}

		// Token: 0x04001FDC RID: 8156
		internal int select;

		// Token: 0x04001FDD RID: 8157
		internal string lang;

		// Token: 0x04001FDE RID: 8158
		internal XmlDataType dataType;

		// Token: 0x04001FDF RID: 8159
		internal XmlSortOrder order;

		// Token: 0x04001FE0 RID: 8160
		internal XmlCaseOrder caseOrder;
	}
}
