﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000515 RID: 1301
	internal class TextEvent : Event
	{
		// Token: 0x060032CC RID: 13004 RVA: 0x0010A00D File Offset: 0x0010820D
		protected TextEvent()
		{
		}

		// Token: 0x060032CD RID: 13005 RVA: 0x0010A015 File Offset: 0x00108215
		public TextEvent(string text)
		{
			this.text = text;
		}

		// Token: 0x060032CE RID: 13006 RVA: 0x0010A024 File Offset: 0x00108224
		public TextEvent(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			this.text = input.Value;
		}

		// Token: 0x060032CF RID: 13007 RVA: 0x0010A04A File Offset: 0x0010824A
		public override bool Output(Processor processor, ActionFrame frame)
		{
			return processor.TextEvent(this.text);
		}

		// Token: 0x060032D0 RID: 13008 RVA: 0x0010A058 File Offset: 0x00108258
		public virtual string Evaluate(Processor processor, ActionFrame frame)
		{
			return this.text;
		}

		// Token: 0x040021B0 RID: 8624
		private string text;
	}
}
