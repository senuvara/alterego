﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000510 RID: 1296
	internal class TemplateLookupAction : Action
	{
		// Token: 0x060032B9 RID: 12985 RVA: 0x00109BE9 File Offset: 0x00107DE9
		internal void Initialize(XmlQualifiedName mode, Stylesheet importsOf)
		{
			this.mode = mode;
			this.importsOf = importsOf;
		}

		// Token: 0x060032BA RID: 12986 RVA: 0x00109BFC File Offset: 0x00107DFC
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			Action action;
			if (this.mode != null)
			{
				action = ((this.importsOf == null) ? processor.Stylesheet.FindTemplate(processor, frame.Node, this.mode) : this.importsOf.FindTemplateImports(processor, frame.Node, this.mode));
			}
			else
			{
				action = ((this.importsOf == null) ? processor.Stylesheet.FindTemplate(processor, frame.Node) : this.importsOf.FindTemplateImports(processor, frame.Node));
			}
			if (action == null)
			{
				action = this.BuiltInTemplate(frame.Node);
			}
			if (action != null)
			{
				frame.SetAction(action);
				return;
			}
			frame.Finished();
		}

		// Token: 0x060032BB RID: 12987 RVA: 0x00109CA8 File Offset: 0x00107EA8
		internal Action BuiltInTemplate(XPathNavigator node)
		{
			Action result = null;
			switch (node.NodeType)
			{
			case XPathNodeType.Root:
			case XPathNodeType.Element:
				result = ApplyTemplatesAction.BuiltInRule(this.mode);
				break;
			case XPathNodeType.Attribute:
			case XPathNodeType.Text:
			case XPathNodeType.SignificantWhitespace:
			case XPathNodeType.Whitespace:
				result = ValueOfAction.BuiltInRule();
				break;
			}
			return result;
		}

		// Token: 0x060032BC RID: 12988 RVA: 0x000FE6D5 File Offset: 0x000FC8D5
		public TemplateLookupAction()
		{
		}

		// Token: 0x040021A8 RID: 8616
		protected XmlQualifiedName mode;

		// Token: 0x040021A9 RID: 8617
		protected Stylesheet importsOf;
	}
}
