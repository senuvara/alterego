﻿using System;
using System.Diagnostics;
using System.Xml.XPath;
using System.Xml.Xsl.Xslt;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004F2 RID: 1266
	internal class NavigatorInput
	{
		// Token: 0x17000A3E RID: 2622
		// (get) Token: 0x06003126 RID: 12582 RVA: 0x001031D7 File Offset: 0x001013D7
		// (set) Token: 0x06003127 RID: 12583 RVA: 0x001031DF File Offset: 0x001013DF
		internal NavigatorInput Next
		{
			get
			{
				return this._Next;
			}
			set
			{
				this._Next = value;
			}
		}

		// Token: 0x17000A3F RID: 2623
		// (get) Token: 0x06003128 RID: 12584 RVA: 0x001031E8 File Offset: 0x001013E8
		internal string Href
		{
			get
			{
				return this._Href;
			}
		}

		// Token: 0x17000A40 RID: 2624
		// (get) Token: 0x06003129 RID: 12585 RVA: 0x001031F0 File Offset: 0x001013F0
		internal KeywordsTable Atoms
		{
			get
			{
				return this._Atoms;
			}
		}

		// Token: 0x17000A41 RID: 2625
		// (get) Token: 0x0600312A RID: 12586 RVA: 0x001031F8 File Offset: 0x001013F8
		internal XPathNavigator Navigator
		{
			get
			{
				return this._Navigator;
			}
		}

		// Token: 0x17000A42 RID: 2626
		// (get) Token: 0x0600312B RID: 12587 RVA: 0x00103200 File Offset: 0x00101400
		internal InputScopeManager InputScopeManager
		{
			get
			{
				return this._Manager;
			}
		}

		// Token: 0x0600312C RID: 12588 RVA: 0x00103208 File Offset: 0x00101408
		internal bool Advance()
		{
			return this._Navigator.MoveToNext();
		}

		// Token: 0x0600312D RID: 12589 RVA: 0x00103215 File Offset: 0x00101415
		internal bool Recurse()
		{
			return this._Navigator.MoveToFirstChild();
		}

		// Token: 0x0600312E RID: 12590 RVA: 0x00103222 File Offset: 0x00101422
		internal bool ToParent()
		{
			return this._Navigator.MoveToParent();
		}

		// Token: 0x0600312F RID: 12591 RVA: 0x0010322F File Offset: 0x0010142F
		internal void Close()
		{
			this._Navigator = null;
			this._PositionInfo = null;
		}

		// Token: 0x17000A43 RID: 2627
		// (get) Token: 0x06003130 RID: 12592 RVA: 0x0010323F File Offset: 0x0010143F
		internal int LineNumber
		{
			get
			{
				return this._PositionInfo.LineNumber;
			}
		}

		// Token: 0x17000A44 RID: 2628
		// (get) Token: 0x06003131 RID: 12593 RVA: 0x0010324C File Offset: 0x0010144C
		internal int LinePosition
		{
			get
			{
				return this._PositionInfo.LinePosition;
			}
		}

		// Token: 0x17000A45 RID: 2629
		// (get) Token: 0x06003132 RID: 12594 RVA: 0x00103259 File Offset: 0x00101459
		internal XPathNodeType NodeType
		{
			get
			{
				return this._Navigator.NodeType;
			}
		}

		// Token: 0x17000A46 RID: 2630
		// (get) Token: 0x06003133 RID: 12595 RVA: 0x00103266 File Offset: 0x00101466
		internal string Name
		{
			get
			{
				return this._Navigator.Name;
			}
		}

		// Token: 0x17000A47 RID: 2631
		// (get) Token: 0x06003134 RID: 12596 RVA: 0x00103273 File Offset: 0x00101473
		internal string LocalName
		{
			get
			{
				return this._Navigator.LocalName;
			}
		}

		// Token: 0x17000A48 RID: 2632
		// (get) Token: 0x06003135 RID: 12597 RVA: 0x00103280 File Offset: 0x00101480
		internal string NamespaceURI
		{
			get
			{
				return this._Navigator.NamespaceURI;
			}
		}

		// Token: 0x17000A49 RID: 2633
		// (get) Token: 0x06003136 RID: 12598 RVA: 0x0010328D File Offset: 0x0010148D
		internal string Prefix
		{
			get
			{
				return this._Navigator.Prefix;
			}
		}

		// Token: 0x17000A4A RID: 2634
		// (get) Token: 0x06003137 RID: 12599 RVA: 0x0010329A File Offset: 0x0010149A
		internal string Value
		{
			get
			{
				return this._Navigator.Value;
			}
		}

		// Token: 0x17000A4B RID: 2635
		// (get) Token: 0x06003138 RID: 12600 RVA: 0x001032A7 File Offset: 0x001014A7
		internal bool IsEmptyTag
		{
			get
			{
				return this._Navigator.IsEmptyElement;
			}
		}

		// Token: 0x17000A4C RID: 2636
		// (get) Token: 0x06003139 RID: 12601 RVA: 0x001032B4 File Offset: 0x001014B4
		internal string BaseURI
		{
			get
			{
				return this._Navigator.BaseURI;
			}
		}

		// Token: 0x0600313A RID: 12602 RVA: 0x001032C1 File Offset: 0x001014C1
		internal bool MoveToFirstAttribute()
		{
			return this._Navigator.MoveToFirstAttribute();
		}

		// Token: 0x0600313B RID: 12603 RVA: 0x001032CE File Offset: 0x001014CE
		internal bool MoveToNextAttribute()
		{
			return this._Navigator.MoveToNextAttribute();
		}

		// Token: 0x0600313C RID: 12604 RVA: 0x001032DB File Offset: 0x001014DB
		internal bool MoveToFirstNamespace()
		{
			return this._Navigator.MoveToFirstNamespace(XPathNamespaceScope.ExcludeXml);
		}

		// Token: 0x0600313D RID: 12605 RVA: 0x001032E9 File Offset: 0x001014E9
		internal bool MoveToNextNamespace()
		{
			return this._Navigator.MoveToNextNamespace(XPathNamespaceScope.ExcludeXml);
		}

		// Token: 0x0600313E RID: 12606 RVA: 0x001032F8 File Offset: 0x001014F8
		internal NavigatorInput(XPathNavigator navigator, string baseUri, InputScope rootScope)
		{
			if (navigator == null)
			{
				throw new ArgumentNullException("navigator");
			}
			if (baseUri == null)
			{
				throw new ArgumentNullException("baseUri");
			}
			this._Next = null;
			this._Href = baseUri;
			this._Atoms = new KeywordsTable(navigator.NameTable);
			this._Navigator = navigator;
			this._Manager = new InputScopeManager(this._Navigator, rootScope);
			this._PositionInfo = PositionInfo.GetPositionInfo(this._Navigator);
			if (this.NodeType == XPathNodeType.Root)
			{
				this._Navigator.MoveToFirstChild();
			}
		}

		// Token: 0x0600313F RID: 12607 RVA: 0x00103384 File Offset: 0x00101584
		internal NavigatorInput(XPathNavigator navigator) : this(navigator, navigator.BaseURI, null)
		{
		}

		// Token: 0x06003140 RID: 12608 RVA: 0x000030EC File Offset: 0x000012EC
		[Conditional("DEBUG")]
		internal void AssertInput()
		{
		}

		// Token: 0x04002079 RID: 8313
		private XPathNavigator _Navigator;

		// Token: 0x0400207A RID: 8314
		private PositionInfo _PositionInfo;

		// Token: 0x0400207B RID: 8315
		private InputScopeManager _Manager;

		// Token: 0x0400207C RID: 8316
		private NavigatorInput _Next;

		// Token: 0x0400207D RID: 8317
		private string _Href;

		// Token: 0x0400207E RID: 8318
		private KeywordsTable _Atoms;
	}
}
