﻿using System;
using System.Text;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004B8 RID: 1208
	internal class BuilderInfo
	{
		// Token: 0x06002F71 RID: 12145 RVA: 0x000FDF0D File Offset: 0x000FC10D
		internal BuilderInfo()
		{
			this.Initialize(string.Empty, string.Empty, string.Empty);
		}

		// Token: 0x06002F72 RID: 12146 RVA: 0x000FDF36 File Offset: 0x000FC136
		internal void Initialize(string prefix, string name, string nspace)
		{
			this.prefix = prefix;
			this.localName = name;
			this.namespaceURI = nspace;
			this.name = null;
			this.htmlProps = null;
			this.htmlAttrProps = null;
			this.TextInfoCount = 0;
		}

		// Token: 0x06002F73 RID: 12147 RVA: 0x000FDF6C File Offset: 0x000FC16C
		internal void Initialize(BuilderInfo src)
		{
			this.prefix = src.Prefix;
			this.localName = src.LocalName;
			this.namespaceURI = src.NamespaceURI;
			this.name = null;
			this.depth = src.Depth;
			this.nodeType = src.NodeType;
			this.htmlProps = src.htmlProps;
			this.htmlAttrProps = src.htmlAttrProps;
			this.TextInfoCount = 0;
			this.EnsureTextInfoSize(src.TextInfoCount);
			src.TextInfo.CopyTo(this.TextInfo, 0);
			this.TextInfoCount = src.TextInfoCount;
		}

		// Token: 0x06002F74 RID: 12148 RVA: 0x000FE008 File Offset: 0x000FC208
		private void EnsureTextInfoSize(int newSize)
		{
			if (this.TextInfo.Length < newSize)
			{
				string[] array = new string[newSize * 2];
				Array.Copy(this.TextInfo, array, this.TextInfoCount);
				this.TextInfo = array;
			}
		}

		// Token: 0x06002F75 RID: 12149 RVA: 0x000FE042 File Offset: 0x000FC242
		internal BuilderInfo Clone()
		{
			BuilderInfo builderInfo = new BuilderInfo();
			builderInfo.Initialize(this);
			return builderInfo;
		}

		// Token: 0x17000A09 RID: 2569
		// (get) Token: 0x06002F76 RID: 12150 RVA: 0x000FE050 File Offset: 0x000FC250
		internal string Name
		{
			get
			{
				if (this.name == null)
				{
					string text = this.Prefix;
					string text2 = this.LocalName;
					if (text != null && 0 < text.Length)
					{
						if (text2.Length > 0)
						{
							this.name = text + ":" + text2;
						}
						else
						{
							this.name = text;
						}
					}
					else
					{
						this.name = text2;
					}
				}
				return this.name;
			}
		}

		// Token: 0x17000A0A RID: 2570
		// (get) Token: 0x06002F77 RID: 12151 RVA: 0x000FE0B2 File Offset: 0x000FC2B2
		// (set) Token: 0x06002F78 RID: 12152 RVA: 0x000FE0BA File Offset: 0x000FC2BA
		internal string LocalName
		{
			get
			{
				return this.localName;
			}
			set
			{
				this.localName = value;
			}
		}

		// Token: 0x17000A0B RID: 2571
		// (get) Token: 0x06002F79 RID: 12153 RVA: 0x000FE0C3 File Offset: 0x000FC2C3
		// (set) Token: 0x06002F7A RID: 12154 RVA: 0x000FE0CB File Offset: 0x000FC2CB
		internal string NamespaceURI
		{
			get
			{
				return this.namespaceURI;
			}
			set
			{
				this.namespaceURI = value;
			}
		}

		// Token: 0x17000A0C RID: 2572
		// (get) Token: 0x06002F7B RID: 12155 RVA: 0x000FE0D4 File Offset: 0x000FC2D4
		// (set) Token: 0x06002F7C RID: 12156 RVA: 0x000FE0DC File Offset: 0x000FC2DC
		internal string Prefix
		{
			get
			{
				return this.prefix;
			}
			set
			{
				this.prefix = value;
			}
		}

		// Token: 0x17000A0D RID: 2573
		// (get) Token: 0x06002F7D RID: 12157 RVA: 0x000FE0E8 File Offset: 0x000FC2E8
		// (set) Token: 0x06002F7E RID: 12158 RVA: 0x000FE179 File Offset: 0x000FC379
		internal string Value
		{
			get
			{
				int textInfoCount = this.TextInfoCount;
				if (textInfoCount == 0)
				{
					return string.Empty;
				}
				if (textInfoCount != 1)
				{
					int num = 0;
					for (int i = 0; i < this.TextInfoCount; i++)
					{
						string text = this.TextInfo[i];
						if (text != null)
						{
							num += text.Length;
						}
					}
					StringBuilder stringBuilder = new StringBuilder(num);
					for (int j = 0; j < this.TextInfoCount; j++)
					{
						string text2 = this.TextInfo[j];
						if (text2 != null)
						{
							stringBuilder.Append(text2);
						}
					}
					return stringBuilder.ToString();
				}
				return this.TextInfo[0];
			}
			set
			{
				this.TextInfoCount = 0;
				this.ValueAppend(value, false);
			}
		}

		// Token: 0x06002F7F RID: 12159 RVA: 0x000FE18C File Offset: 0x000FC38C
		internal void ValueAppend(string s, bool disableEscaping)
		{
			if (s == null || s.Length == 0)
			{
				return;
			}
			this.EnsureTextInfoSize(this.TextInfoCount + (disableEscaping ? 2 : 1));
			int textInfoCount;
			if (disableEscaping)
			{
				string[] textInfo = this.TextInfo;
				textInfoCount = this.TextInfoCount;
				this.TextInfoCount = textInfoCount + 1;
				textInfo[textInfoCount] = null;
			}
			string[] textInfo2 = this.TextInfo;
			textInfoCount = this.TextInfoCount;
			this.TextInfoCount = textInfoCount + 1;
			textInfo2[textInfoCount] = s;
		}

		// Token: 0x17000A0E RID: 2574
		// (get) Token: 0x06002F80 RID: 12160 RVA: 0x000FE1EE File Offset: 0x000FC3EE
		// (set) Token: 0x06002F81 RID: 12161 RVA: 0x000FE1F6 File Offset: 0x000FC3F6
		internal XmlNodeType NodeType
		{
			get
			{
				return this.nodeType;
			}
			set
			{
				this.nodeType = value;
			}
		}

		// Token: 0x17000A0F RID: 2575
		// (get) Token: 0x06002F82 RID: 12162 RVA: 0x000FE1FF File Offset: 0x000FC3FF
		// (set) Token: 0x06002F83 RID: 12163 RVA: 0x000FE207 File Offset: 0x000FC407
		internal int Depth
		{
			get
			{
				return this.depth;
			}
			set
			{
				this.depth = value;
			}
		}

		// Token: 0x17000A10 RID: 2576
		// (get) Token: 0x06002F84 RID: 12164 RVA: 0x000FE210 File Offset: 0x000FC410
		// (set) Token: 0x06002F85 RID: 12165 RVA: 0x000FE218 File Offset: 0x000FC418
		internal bool IsEmptyTag
		{
			get
			{
				return this.isEmptyTag;
			}
			set
			{
				this.isEmptyTag = value;
			}
		}

		// Token: 0x04001FCD RID: 8141
		private string name;

		// Token: 0x04001FCE RID: 8142
		private string localName;

		// Token: 0x04001FCF RID: 8143
		private string namespaceURI;

		// Token: 0x04001FD0 RID: 8144
		private string prefix;

		// Token: 0x04001FD1 RID: 8145
		private XmlNodeType nodeType;

		// Token: 0x04001FD2 RID: 8146
		private int depth;

		// Token: 0x04001FD3 RID: 8147
		private bool isEmptyTag;

		// Token: 0x04001FD4 RID: 8148
		internal string[] TextInfo = new string[4];

		// Token: 0x04001FD5 RID: 8149
		internal int TextInfoCount;

		// Token: 0x04001FD6 RID: 8150
		internal bool search;

		// Token: 0x04001FD7 RID: 8151
		internal HtmlElementProps htmlProps;

		// Token: 0x04001FD8 RID: 8152
		internal HtmlAttributeProps htmlAttrProps;
	}
}
