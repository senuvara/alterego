﻿using System;
using System.Collections;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004E9 RID: 1257
	internal class HtmlElementProps
	{
		// Token: 0x060030E1 RID: 12513 RVA: 0x001021C7 File Offset: 0x001003C7
		public static HtmlElementProps Create(bool empty, bool abrParent, bool uriParent, bool noEntities, bool blockWS, bool head, bool nameParent)
		{
			return new HtmlElementProps
			{
				empty = empty,
				abrParent = abrParent,
				uriParent = uriParent,
				noEntities = noEntities,
				blockWS = blockWS,
				head = head,
				nameParent = nameParent
			};
		}

		// Token: 0x17000A28 RID: 2600
		// (get) Token: 0x060030E2 RID: 12514 RVA: 0x00102202 File Offset: 0x00100402
		public bool Empty
		{
			get
			{
				return this.empty;
			}
		}

		// Token: 0x17000A29 RID: 2601
		// (get) Token: 0x060030E3 RID: 12515 RVA: 0x0010220A File Offset: 0x0010040A
		public bool AbrParent
		{
			get
			{
				return this.abrParent;
			}
		}

		// Token: 0x17000A2A RID: 2602
		// (get) Token: 0x060030E4 RID: 12516 RVA: 0x00102212 File Offset: 0x00100412
		public bool UriParent
		{
			get
			{
				return this.uriParent;
			}
		}

		// Token: 0x17000A2B RID: 2603
		// (get) Token: 0x060030E5 RID: 12517 RVA: 0x0010221A File Offset: 0x0010041A
		public bool NoEntities
		{
			get
			{
				return this.noEntities;
			}
		}

		// Token: 0x17000A2C RID: 2604
		// (get) Token: 0x060030E6 RID: 12518 RVA: 0x00102222 File Offset: 0x00100422
		public bool BlockWS
		{
			get
			{
				return this.blockWS;
			}
		}

		// Token: 0x17000A2D RID: 2605
		// (get) Token: 0x060030E7 RID: 12519 RVA: 0x0010222A File Offset: 0x0010042A
		public bool Head
		{
			get
			{
				return this.head;
			}
		}

		// Token: 0x17000A2E RID: 2606
		// (get) Token: 0x060030E8 RID: 12520 RVA: 0x00102232 File Offset: 0x00100432
		public bool NameParent
		{
			get
			{
				return this.nameParent;
			}
		}

		// Token: 0x060030E9 RID: 12521 RVA: 0x0010223A File Offset: 0x0010043A
		public static HtmlElementProps GetProps(string name)
		{
			return (HtmlElementProps)HtmlElementProps.s_table[name];
		}

		// Token: 0x060030EA RID: 12522 RVA: 0x0010224C File Offset: 0x0010044C
		private static Hashtable CreatePropsTable()
		{
			bool flag = false;
			bool flag2 = true;
			return new Hashtable(71, StringComparer.OrdinalIgnoreCase)
			{
				{
					"a",
					HtmlElementProps.Create(flag, flag, flag2, flag, flag, flag, flag2)
				},
				{
					"address",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"applet",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"area",
					HtmlElementProps.Create(flag2, flag2, flag2, flag, flag2, flag, flag)
				},
				{
					"base",
					HtmlElementProps.Create(flag2, flag, flag2, flag, flag2, flag, flag)
				},
				{
					"basefont",
					HtmlElementProps.Create(flag2, flag, flag, flag, flag2, flag, flag)
				},
				{
					"blockquote",
					HtmlElementProps.Create(flag, flag, flag2, flag, flag2, flag, flag)
				},
				{
					"body",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"br",
					HtmlElementProps.Create(flag2, flag, flag, flag, flag, flag, flag)
				},
				{
					"button",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag, flag, flag)
				},
				{
					"caption",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"center",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"col",
					HtmlElementProps.Create(flag2, flag, flag, flag, flag2, flag, flag)
				},
				{
					"colgroup",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"dd",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"del",
					HtmlElementProps.Create(flag, flag, flag2, flag, flag2, flag, flag)
				},
				{
					"dir",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"div",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"dl",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"dt",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"fieldset",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"font",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"form",
					HtmlElementProps.Create(flag, flag, flag2, flag, flag2, flag, flag)
				},
				{
					"frame",
					HtmlElementProps.Create(flag2, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"frameset",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"h1",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"h2",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"h3",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"h4",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"h5",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"h6",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"head",
					HtmlElementProps.Create(flag, flag, flag2, flag, flag2, flag2, flag)
				},
				{
					"hr",
					HtmlElementProps.Create(flag2, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"html",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"iframe",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"img",
					HtmlElementProps.Create(flag2, flag2, flag2, flag, flag, flag, flag)
				},
				{
					"input",
					HtmlElementProps.Create(flag2, flag2, flag2, flag, flag, flag, flag)
				},
				{
					"ins",
					HtmlElementProps.Create(flag, flag, flag2, flag, flag2, flag, flag)
				},
				{
					"isindex",
					HtmlElementProps.Create(flag2, flag, flag, flag, flag2, flag, flag)
				},
				{
					"legend",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"li",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"link",
					HtmlElementProps.Create(flag2, flag, flag2, flag, flag2, flag, flag)
				},
				{
					"map",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"menu",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"meta",
					HtmlElementProps.Create(flag2, flag, flag, flag, flag2, flag, flag)
				},
				{
					"noframes",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"noscript",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"object",
					HtmlElementProps.Create(flag, flag2, flag2, flag, flag, flag, flag)
				},
				{
					"ol",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"optgroup",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"option",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"p",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"param",
					HtmlElementProps.Create(flag2, flag, flag, flag, flag2, flag, flag)
				},
				{
					"pre",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"q",
					HtmlElementProps.Create(flag, flag, flag2, flag, flag, flag, flag)
				},
				{
					"s",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"script",
					HtmlElementProps.Create(flag, flag2, flag2, flag2, flag, flag, flag)
				},
				{
					"select",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag, flag, flag)
				},
				{
					"strike",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"style",
					HtmlElementProps.Create(flag, flag, flag, flag2, flag2, flag, flag)
				},
				{
					"table",
					HtmlElementProps.Create(flag, flag, flag2, flag, flag2, flag, flag)
				},
				{
					"tbody",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"td",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"textarea",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag, flag, flag)
				},
				{
					"tfoot",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"th",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"thead",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"title",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"tr",
					HtmlElementProps.Create(flag, flag, flag, flag, flag2, flag, flag)
				},
				{
					"ul",
					HtmlElementProps.Create(flag, flag2, flag, flag, flag2, flag, flag)
				},
				{
					"xmp",
					HtmlElementProps.Create(flag, flag, flag, flag, flag, flag, flag)
				}
			};
		}

		// Token: 0x060030EB RID: 12523 RVA: 0x00002103 File Offset: 0x00000303
		public HtmlElementProps()
		{
		}

		// Token: 0x060030EC RID: 12524 RVA: 0x001028CA File Offset: 0x00100ACA
		// Note: this type is marked as 'beforefieldinit'.
		static HtmlElementProps()
		{
		}

		// Token: 0x04002057 RID: 8279
		private bool empty;

		// Token: 0x04002058 RID: 8280
		private bool abrParent;

		// Token: 0x04002059 RID: 8281
		private bool uriParent;

		// Token: 0x0400205A RID: 8282
		private bool noEntities;

		// Token: 0x0400205B RID: 8283
		private bool blockWS;

		// Token: 0x0400205C RID: 8284
		private bool head;

		// Token: 0x0400205D RID: 8285
		private bool nameParent;

		// Token: 0x0400205E RID: 8286
		private static Hashtable s_table = HtmlElementProps.CreatePropsTable();
	}
}
