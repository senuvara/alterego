﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004B9 RID: 1209
	internal class CallTemplateAction : ContainerAction
	{
		// Token: 0x06002F86 RID: 12166 RVA: 0x000FE221 File Offset: 0x000FC421
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.name, "name");
			this.CompileContent(compiler);
		}

		// Token: 0x06002F87 RID: 12167 RVA: 0x000FE244 File Offset: 0x000FC444
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Name))
			{
				this.name = compiler.CreateXPathQName(value);
				return true;
			}
			return false;
		}

		// Token: 0x06002F88 RID: 12168 RVA: 0x000FE28C File Offset: 0x000FC48C
		private void CompileContent(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			if (compiler.Recurse())
			{
				for (;;)
				{
					XPathNodeType nodeType = input.NodeType;
					if (nodeType != XPathNodeType.Element)
					{
						if (nodeType - XPathNodeType.SignificantWhitespace > 3)
						{
							break;
						}
					}
					else
					{
						compiler.PushNamespaceScope();
						string namespaceURI = input.NamespaceURI;
						string localName = input.LocalName;
						if (!Ref.Equal(namespaceURI, input.Atoms.UriXsl) || !Ref.Equal(localName, input.Atoms.WithParam))
						{
							goto IL_79;
						}
						WithParamAction withParamAction = compiler.CreateWithParamAction();
						base.CheckDuplicateParams(withParamAction.Name);
						base.AddAction(withParamAction);
						compiler.PopScope();
					}
					if (!compiler.Advance())
					{
						goto Block_5;
					}
				}
				throw XsltException.Create("The contents of '{0}' are invalid.", new string[]
				{
					"call-template"
				});
				IL_79:
				throw compiler.UnexpectedKeyword();
				Block_5:
				compiler.ToParent();
			}
		}

		// Token: 0x06002F89 RID: 12169 RVA: 0x000FE34C File Offset: 0x000FC54C
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			switch (frame.State)
			{
			case 0:
				processor.ResetParams();
				if (this.containedActions != null && this.containedActions.Count > 0)
				{
					processor.PushActionFrame(frame);
					frame.State = 2;
					return;
				}
				break;
			case 1:
				return;
			case 2:
				break;
			case 3:
				frame.Finished();
				return;
			default:
				return;
			}
			TemplateAction templateAction = processor.Stylesheet.FindTemplate(this.name);
			if (templateAction != null)
			{
				frame.State = 3;
				processor.PushActionFrame(templateAction, frame.NodeSet);
				return;
			}
			throw XsltException.Create("The named template '{0}' does not exist.", new string[]
			{
				this.name.ToString()
			});
		}

		// Token: 0x06002F8A RID: 12170 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		public CallTemplateAction()
		{
		}

		// Token: 0x04001FD9 RID: 8153
		private const int ProcessedChildren = 2;

		// Token: 0x04001FDA RID: 8154
		private const int ProcessedTemplate = 3;

		// Token: 0x04001FDB RID: 8155
		private XmlQualifiedName name;
	}
}
