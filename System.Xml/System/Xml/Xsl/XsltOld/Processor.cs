﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security;
using System.Text;
using System.Xml.XPath;
using System.Xml.Xsl.XsltOld.Debugger;
using MS.Internal.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004FD RID: 1277
	internal sealed class Processor : IXsltProcessor
	{
		// Token: 0x17000A61 RID: 2657
		// (get) Token: 0x06003192 RID: 12690 RVA: 0x00104DA4 File Offset: 0x00102FA4
		internal XPathNavigator Current
		{
			get
			{
				ActionFrame actionFrame = (ActionFrame)this.actionStack.Peek();
				if (actionFrame == null)
				{
					return null;
				}
				return actionFrame.Node;
			}
		}

		// Token: 0x17000A62 RID: 2658
		// (get) Token: 0x06003193 RID: 12691 RVA: 0x00104DCD File Offset: 0x00102FCD
		// (set) Token: 0x06003194 RID: 12692 RVA: 0x00104DD5 File Offset: 0x00102FD5
		internal Processor.ExecResult ExecutionResult
		{
			get
			{
				return this.execResult;
			}
			set
			{
				this.execResult = value;
			}
		}

		// Token: 0x17000A63 RID: 2659
		// (get) Token: 0x06003195 RID: 12693 RVA: 0x00104DDE File Offset: 0x00102FDE
		internal Stylesheet Stylesheet
		{
			get
			{
				return this.stylesheet;
			}
		}

		// Token: 0x17000A64 RID: 2660
		// (get) Token: 0x06003196 RID: 12694 RVA: 0x00104DE6 File Offset: 0x00102FE6
		internal XmlResolver Resolver
		{
			get
			{
				return this.resolver;
			}
		}

		// Token: 0x17000A65 RID: 2661
		// (get) Token: 0x06003197 RID: 12695 RVA: 0x00104DEE File Offset: 0x00102FEE
		internal ArrayList SortArray
		{
			get
			{
				return this.sortArray;
			}
		}

		// Token: 0x17000A66 RID: 2662
		// (get) Token: 0x06003198 RID: 12696 RVA: 0x00104DF6 File Offset: 0x00102FF6
		internal Key[] KeyList
		{
			get
			{
				return this.keyList;
			}
		}

		// Token: 0x06003199 RID: 12697 RVA: 0x00104E00 File Offset: 0x00103000
		internal XPathNavigator GetNavigator(Uri ruri)
		{
			XPathNavigator xpathNavigator;
			if (this.documentCache != null)
			{
				xpathNavigator = (this.documentCache[ruri] as XPathNavigator);
				if (xpathNavigator != null)
				{
					return xpathNavigator.Clone();
				}
			}
			else
			{
				this.documentCache = new Hashtable();
			}
			object entity = this.resolver.GetEntity(ruri, null, null);
			if (entity is Stream)
			{
				xpathNavigator = ((IXPathNavigable)Compiler.LoadDocument(new XmlTextReaderImpl(ruri.ToString(), (Stream)entity)
				{
					XmlResolver = this.resolver
				})).CreateNavigator();
			}
			else
			{
				if (!(entity is XPathNavigator))
				{
					throw XsltException.Create("Cannot resolve the referenced document '{0}'.", new string[]
					{
						ruri.ToString()
					});
				}
				xpathNavigator = (XPathNavigator)entity;
			}
			this.documentCache[ruri] = xpathNavigator.Clone();
			return xpathNavigator;
		}

		// Token: 0x0600319A RID: 12698 RVA: 0x00104EBD File Offset: 0x001030BD
		internal void AddSort(Sort sortinfo)
		{
			this.sortArray.Add(sortinfo);
		}

		// Token: 0x0600319B RID: 12699 RVA: 0x00104ECC File Offset: 0x001030CC
		internal void InitSortArray()
		{
			if (this.sortArray == null)
			{
				this.sortArray = new ArrayList();
				return;
			}
			this.sortArray.Clear();
		}

		// Token: 0x0600319C RID: 12700 RVA: 0x00104EF0 File Offset: 0x001030F0
		internal object GetGlobalParameter(XmlQualifiedName qname)
		{
			object obj = this.args.GetParam(qname.Name, qname.Namespace);
			if (obj == null)
			{
				return null;
			}
			if (!(obj is XPathNodeIterator) && !(obj is XPathNavigator) && !(obj is bool) && !(obj is double) && !(obj is string))
			{
				if (obj is short || obj is ushort || obj is int || obj is uint || obj is long || obj is ulong || obj is float || obj is decimal)
				{
					obj = XmlConvert.ToXPathDouble(obj);
				}
				else
				{
					obj = obj.ToString();
				}
			}
			return obj;
		}

		// Token: 0x0600319D RID: 12701 RVA: 0x00104F98 File Offset: 0x00103198
		internal object GetExtensionObject(string nsUri)
		{
			return this.args.GetExtensionObject(nsUri);
		}

		// Token: 0x0600319E RID: 12702 RVA: 0x00104FA6 File Offset: 0x001031A6
		internal object GetScriptObject(string nsUri)
		{
			return this.scriptExtensions[nsUri];
		}

		// Token: 0x17000A67 RID: 2663
		// (get) Token: 0x0600319F RID: 12703 RVA: 0x00104FB4 File Offset: 0x001031B4
		internal RootAction RootAction
		{
			get
			{
				return this.rootAction;
			}
		}

		// Token: 0x17000A68 RID: 2664
		// (get) Token: 0x060031A0 RID: 12704 RVA: 0x00104FBC File Offset: 0x001031BC
		internal XPathNavigator Document
		{
			get
			{
				return this.document;
			}
		}

		// Token: 0x060031A1 RID: 12705 RVA: 0x00104FC4 File Offset: 0x001031C4
		internal StringBuilder GetSharedStringBuilder()
		{
			if (this.sharedStringBuilder == null)
			{
				this.sharedStringBuilder = new StringBuilder();
			}
			else
			{
				this.sharedStringBuilder.Length = 0;
			}
			return this.sharedStringBuilder;
		}

		// Token: 0x060031A2 RID: 12706 RVA: 0x000030EC File Offset: 0x000012EC
		internal void ReleaseSharedStringBuilder()
		{
		}

		// Token: 0x17000A69 RID: 2665
		// (get) Token: 0x060031A3 RID: 12707 RVA: 0x00104FED File Offset: 0x001031ED
		internal ArrayList NumberList
		{
			get
			{
				if (this.numberList == null)
				{
					this.numberList = new ArrayList();
				}
				return this.numberList;
			}
		}

		// Token: 0x17000A6A RID: 2666
		// (get) Token: 0x060031A4 RID: 12708 RVA: 0x00105008 File Offset: 0x00103208
		internal IXsltDebugger Debugger
		{
			get
			{
				return this.debugger;
			}
		}

		// Token: 0x17000A6B RID: 2667
		// (get) Token: 0x060031A5 RID: 12709 RVA: 0x00105010 File Offset: 0x00103210
		internal HWStack ActionStack
		{
			get
			{
				return this.actionStack;
			}
		}

		// Token: 0x17000A6C RID: 2668
		// (get) Token: 0x060031A6 RID: 12710 RVA: 0x00105018 File Offset: 0x00103218
		internal RecordBuilder Builder
		{
			get
			{
				return this.builder;
			}
		}

		// Token: 0x17000A6D RID: 2669
		// (get) Token: 0x060031A7 RID: 12711 RVA: 0x00105020 File Offset: 0x00103220
		internal XsltOutput Output
		{
			get
			{
				return this.output;
			}
		}

		// Token: 0x060031A8 RID: 12712 RVA: 0x00105028 File Offset: 0x00103228
		public Processor(XPathNavigator doc, XsltArgumentList args, XmlResolver resolver, Stylesheet stylesheet, List<TheQuery> queryStore, RootAction rootAction, IXsltDebugger debugger)
		{
			this.stylesheet = stylesheet;
			this.queryStore = queryStore;
			this.rootAction = rootAction;
			this.queryList = new Query[queryStore.Count];
			for (int i = 0; i < queryStore.Count; i++)
			{
				this.queryList[i] = Query.Clone(queryStore[i].CompiledQuery.QueryTree);
			}
			this.xsm = new StateMachine();
			this.document = doc;
			this.builder = null;
			this.actionStack = new HWStack(10);
			this.output = this.rootAction.Output;
			this.permissions = this.rootAction.permissions;
			this.resolver = (resolver ?? XmlNullResolver.Singleton);
			this.args = (args ?? new XsltArgumentList());
			this.debugger = debugger;
			if (this.debugger != null)
			{
				this.debuggerStack = new HWStack(10, 1000);
				this.templateLookup = new TemplateLookupActionDbg();
			}
			if (this.rootAction.KeyList != null)
			{
				this.keyList = new Key[this.rootAction.KeyList.Count];
				for (int j = 0; j < this.keyList.Length; j++)
				{
					this.keyList[j] = this.rootAction.KeyList[j].Clone();
				}
			}
			this.scriptExtensions = new Hashtable(this.stylesheet.ScriptObjectTypes.Count);
			foreach (object obj in this.stylesheet.ScriptObjectTypes)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				string text = (string)dictionaryEntry.Key;
				if (this.GetExtensionObject(text) != null)
				{
					throw XsltException.Create("Namespace '{0}' has a duplicate implementation.", new string[]
					{
						text
					});
				}
				this.scriptExtensions.Add(text, Activator.CreateInstance((Type)dictionaryEntry.Value, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance, null, null, null));
			}
			this.PushActionFrame(this.rootAction, null);
		}

		// Token: 0x060031A9 RID: 12713 RVA: 0x00105264 File Offset: 0x00103464
		public ReaderOutput StartReader()
		{
			ReaderOutput result = new ReaderOutput(this);
			this.builder = new RecordBuilder(result, this.nameTable);
			return result;
		}

		// Token: 0x060031AA RID: 12714 RVA: 0x0010528C File Offset: 0x0010348C
		public void Execute(Stream stream)
		{
			RecordOutput recordOutput = null;
			switch (this.output.Method)
			{
			case XsltOutput.OutputMethod.Xml:
			case XsltOutput.OutputMethod.Html:
			case XsltOutput.OutputMethod.Other:
			case XsltOutput.OutputMethod.Unknown:
				recordOutput = new TextOutput(this, stream);
				break;
			case XsltOutput.OutputMethod.Text:
				recordOutput = new TextOnlyOutput(this, stream);
				break;
			}
			this.builder = new RecordBuilder(recordOutput, this.nameTable);
			this.Execute();
		}

		// Token: 0x060031AB RID: 12715 RVA: 0x001052F0 File Offset: 0x001034F0
		public void Execute(TextWriter writer)
		{
			RecordOutput recordOutput = null;
			switch (this.output.Method)
			{
			case XsltOutput.OutputMethod.Xml:
			case XsltOutput.OutputMethod.Html:
			case XsltOutput.OutputMethod.Other:
			case XsltOutput.OutputMethod.Unknown:
				recordOutput = new TextOutput(this, writer);
				break;
			case XsltOutput.OutputMethod.Text:
				recordOutput = new TextOnlyOutput(this, writer);
				break;
			}
			this.builder = new RecordBuilder(recordOutput, this.nameTable);
			this.Execute();
		}

		// Token: 0x060031AC RID: 12716 RVA: 0x00105351 File Offset: 0x00103551
		public void Execute(XmlWriter writer)
		{
			this.builder = new RecordBuilder(new WriterOutput(this, writer), this.nameTable);
			this.Execute();
		}

		// Token: 0x060031AD RID: 12717 RVA: 0x00105374 File Offset: 0x00103574
		internal void Execute()
		{
			while (this.execResult == Processor.ExecResult.Continue)
			{
				ActionFrame actionFrame = (ActionFrame)this.actionStack.Peek();
				if (actionFrame == null)
				{
					this.builder.TheEnd();
					this.ExecutionResult = Processor.ExecResult.Done;
					break;
				}
				if (actionFrame.Execute(this))
				{
					this.actionStack.Pop();
				}
			}
			if (this.execResult == Processor.ExecResult.Interrupt)
			{
				this.execResult = Processor.ExecResult.Continue;
			}
		}

		// Token: 0x060031AE RID: 12718 RVA: 0x001053D8 File Offset: 0x001035D8
		internal ActionFrame PushNewFrame()
		{
			ActionFrame actionFrame = (ActionFrame)this.actionStack.Peek();
			ActionFrame actionFrame2 = (ActionFrame)this.actionStack.Push();
			if (actionFrame2 == null)
			{
				actionFrame2 = new ActionFrame();
				this.actionStack.AddToTop(actionFrame2);
			}
			if (actionFrame != null)
			{
				actionFrame2.Inherit(actionFrame);
			}
			return actionFrame2;
		}

		// Token: 0x060031AF RID: 12719 RVA: 0x00105427 File Offset: 0x00103627
		internal void PushActionFrame(Action action, XPathNodeIterator nodeSet)
		{
			this.PushNewFrame().Init(action, nodeSet);
		}

		// Token: 0x060031B0 RID: 12720 RVA: 0x00105436 File Offset: 0x00103636
		internal void PushActionFrame(ActionFrame container)
		{
			this.PushActionFrame(container, container.NodeSet);
		}

		// Token: 0x060031B1 RID: 12721 RVA: 0x00105445 File Offset: 0x00103645
		internal void PushActionFrame(ActionFrame container, XPathNodeIterator nodeSet)
		{
			this.PushNewFrame().Init(container, nodeSet);
		}

		// Token: 0x060031B2 RID: 12722 RVA: 0x00105454 File Offset: 0x00103654
		internal void PushTemplateLookup(XPathNodeIterator nodeSet, XmlQualifiedName mode, Stylesheet importsOf)
		{
			this.templateLookup.Initialize(mode, importsOf);
			this.PushActionFrame(this.templateLookup, nodeSet);
		}

		// Token: 0x060031B3 RID: 12723 RVA: 0x00105470 File Offset: 0x00103670
		internal string GetQueryExpression(int key)
		{
			return this.queryStore[key].CompiledQuery.Expression;
		}

		// Token: 0x060031B4 RID: 12724 RVA: 0x00105488 File Offset: 0x00103688
		internal Query GetCompiledQuery(int key)
		{
			TheQuery theQuery = this.queryStore[key];
			theQuery.CompiledQuery.CheckErrors();
			Query query = Query.Clone(this.queryList[key]);
			query.SetXsltContext(new XsltCompileContext(theQuery._ScopeManager, this));
			return query;
		}

		// Token: 0x060031B5 RID: 12725 RVA: 0x001054CC File Offset: 0x001036CC
		internal Query GetValueQuery(int key)
		{
			return this.GetValueQuery(key, null);
		}

		// Token: 0x060031B6 RID: 12726 RVA: 0x001054D8 File Offset: 0x001036D8
		internal Query GetValueQuery(int key, XsltCompileContext context)
		{
			TheQuery theQuery = this.queryStore[key];
			theQuery.CompiledQuery.CheckErrors();
			Query query = this.queryList[key];
			if (context == null)
			{
				context = new XsltCompileContext(theQuery._ScopeManager, this);
			}
			else
			{
				context.Reinitialize(theQuery._ScopeManager, this);
			}
			query.SetXsltContext(context);
			return query;
		}

		// Token: 0x060031B7 RID: 12727 RVA: 0x0010552C File Offset: 0x0010372C
		private XsltCompileContext GetValueOfContext()
		{
			if (this.valueOfContext == null)
			{
				this.valueOfContext = new XsltCompileContext();
			}
			return this.valueOfContext;
		}

		// Token: 0x060031B8 RID: 12728 RVA: 0x00105547 File Offset: 0x00103747
		[Conditional("DEBUG")]
		private void RecycleValueOfContext()
		{
			if (this.valueOfContext != null)
			{
				this.valueOfContext.Recycle();
			}
		}

		// Token: 0x060031B9 RID: 12729 RVA: 0x0010555C File Offset: 0x0010375C
		private XsltCompileContext GetMatchesContext()
		{
			if (this.matchesContext == null)
			{
				this.matchesContext = new XsltCompileContext();
			}
			return this.matchesContext;
		}

		// Token: 0x060031BA RID: 12730 RVA: 0x00105577 File Offset: 0x00103777
		[Conditional("DEBUG")]
		private void RecycleMatchesContext()
		{
			if (this.matchesContext != null)
			{
				this.matchesContext.Recycle();
			}
		}

		// Token: 0x060031BB RID: 12731 RVA: 0x0010558C File Offset: 0x0010378C
		internal string ValueOf(ActionFrame context, int key)
		{
			Query valueQuery = this.GetValueQuery(key, this.GetValueOfContext());
			object obj = valueQuery.Evaluate(context.NodeSet);
			string result;
			if (obj is XPathNodeIterator)
			{
				XPathNavigator xpathNavigator = valueQuery.Advance();
				result = ((xpathNavigator != null) ? this.ValueOf(xpathNavigator) : string.Empty);
			}
			else
			{
				result = XmlConvert.ToXPathString(obj);
			}
			return result;
		}

		// Token: 0x060031BC RID: 12732 RVA: 0x001055E0 File Offset: 0x001037E0
		internal string ValueOf(XPathNavigator n)
		{
			if (this.stylesheet.Whitespace && n.NodeType == XPathNodeType.Element)
			{
				StringBuilder stringBuilder = this.GetSharedStringBuilder();
				this.ElementValueWithoutWS(n, stringBuilder);
				this.ReleaseSharedStringBuilder();
				return stringBuilder.ToString();
			}
			return n.Value;
		}

		// Token: 0x060031BD RID: 12733 RVA: 0x00105628 File Offset: 0x00103828
		private void ElementValueWithoutWS(XPathNavigator nav, StringBuilder builder)
		{
			bool flag = this.Stylesheet.PreserveWhiteSpace(this, nav);
			if (nav.MoveToFirstChild())
			{
				do
				{
					switch (nav.NodeType)
					{
					case XPathNodeType.Element:
						this.ElementValueWithoutWS(nav, builder);
						break;
					case XPathNodeType.Text:
					case XPathNodeType.SignificantWhitespace:
						builder.Append(nav.Value);
						break;
					case XPathNodeType.Whitespace:
						if (flag)
						{
							builder.Append(nav.Value);
						}
						break;
					}
				}
				while (nav.MoveToNext());
				nav.MoveToParent();
			}
		}

		// Token: 0x060031BE RID: 12734 RVA: 0x001056AC File Offset: 0x001038AC
		internal XPathNodeIterator StartQuery(XPathNodeIterator context, int key)
		{
			Query compiledQuery = this.GetCompiledQuery(key);
			if (compiledQuery.Evaluate(context) is XPathNodeIterator)
			{
				return new XPathSelectionIterator(context.Current, compiledQuery);
			}
			throw XsltException.Create("Expression must evaluate to a node-set.", Array.Empty<string>());
		}

		// Token: 0x060031BF RID: 12735 RVA: 0x001056EB File Offset: 0x001038EB
		internal object Evaluate(ActionFrame context, int key)
		{
			return this.GetValueQuery(key).Evaluate(context.NodeSet);
		}

		// Token: 0x060031C0 RID: 12736 RVA: 0x00105700 File Offset: 0x00103900
		internal object RunQuery(ActionFrame context, int key)
		{
			object obj = this.GetCompiledQuery(key).Evaluate(context.NodeSet);
			XPathNodeIterator xpathNodeIterator = obj as XPathNodeIterator;
			if (xpathNodeIterator != null)
			{
				return new XPathArrayIterator(xpathNodeIterator);
			}
			return obj;
		}

		// Token: 0x060031C1 RID: 12737 RVA: 0x00105734 File Offset: 0x00103934
		internal string EvaluateString(ActionFrame context, int key)
		{
			object obj = this.Evaluate(context, key);
			string text = null;
			if (obj != null)
			{
				text = XmlConvert.ToXPathString(obj);
			}
			if (text == null)
			{
				text = string.Empty;
			}
			return text;
		}

		// Token: 0x060031C2 RID: 12738 RVA: 0x00105760 File Offset: 0x00103960
		internal bool EvaluateBoolean(ActionFrame context, int key)
		{
			object obj = this.Evaluate(context, key);
			if (obj == null)
			{
				return false;
			}
			XPathNavigator xpathNavigator = obj as XPathNavigator;
			if (xpathNavigator == null)
			{
				return Convert.ToBoolean(obj, CultureInfo.InvariantCulture);
			}
			return Convert.ToBoolean(xpathNavigator.Value, CultureInfo.InvariantCulture);
		}

		// Token: 0x060031C3 RID: 12739 RVA: 0x001057A4 File Offset: 0x001039A4
		internal bool Matches(XPathNavigator context, int key)
		{
			Query valueQuery = this.GetValueQuery(key, this.GetMatchesContext());
			bool result;
			try
			{
				result = (valueQuery.MatchNode(context) != null);
			}
			catch (XPathException)
			{
				throw XsltException.Create("'{0}' is an invalid XSLT pattern.", new string[]
				{
					this.GetQueryExpression(key)
				});
			}
			return result;
		}

		// Token: 0x17000A6E RID: 2670
		// (get) Token: 0x060031C4 RID: 12740 RVA: 0x001057FC File Offset: 0x001039FC
		internal XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		// Token: 0x17000A6F RID: 2671
		// (get) Token: 0x060031C5 RID: 12741 RVA: 0x00105804 File Offset: 0x00103A04
		internal bool CanContinue
		{
			get
			{
				return this.execResult == Processor.ExecResult.Continue;
			}
		}

		// Token: 0x17000A70 RID: 2672
		// (get) Token: 0x060031C6 RID: 12742 RVA: 0x0010580F File Offset: 0x00103A0F
		internal bool ExecutionDone
		{
			get
			{
				return this.execResult == Processor.ExecResult.Done;
			}
		}

		// Token: 0x060031C7 RID: 12743 RVA: 0x0010581A File Offset: 0x00103A1A
		internal void ResetOutput()
		{
			this.builder.Reset();
		}

		// Token: 0x060031C8 RID: 12744 RVA: 0x00105827 File Offset: 0x00103A27
		internal bool BeginEvent(XPathNodeType nodeType, string prefix, string name, string nspace, bool empty)
		{
			return this.BeginEvent(nodeType, prefix, name, nspace, empty, null, true);
		}

		// Token: 0x060031C9 RID: 12745 RVA: 0x00105838 File Offset: 0x00103A38
		internal bool BeginEvent(XPathNodeType nodeType, string prefix, string name, string nspace, bool empty, object htmlProps, bool search)
		{
			int num = this.xsm.BeginOutlook(nodeType);
			if (this.ignoreLevel > 0 || num == 16)
			{
				this.ignoreLevel++;
				return true;
			}
			switch (this.builder.BeginEvent(num, nodeType, prefix, name, nspace, empty, htmlProps, search))
			{
			case Processor.OutputResult.Continue:
				this.xsm.Begin(nodeType);
				return true;
			case Processor.OutputResult.Interrupt:
				this.xsm.Begin(nodeType);
				this.ExecutionResult = Processor.ExecResult.Interrupt;
				return true;
			case Processor.OutputResult.Overflow:
				this.ExecutionResult = Processor.ExecResult.Interrupt;
				return false;
			case Processor.OutputResult.Error:
				this.ignoreLevel++;
				return true;
			case Processor.OutputResult.Ignore:
				return true;
			default:
				return true;
			}
		}

		// Token: 0x060031CA RID: 12746 RVA: 0x001058E5 File Offset: 0x00103AE5
		internal bool TextEvent(string text)
		{
			return this.TextEvent(text, false);
		}

		// Token: 0x060031CB RID: 12747 RVA: 0x001058F0 File Offset: 0x00103AF0
		internal bool TextEvent(string text, bool disableOutputEscaping)
		{
			if (this.ignoreLevel > 0)
			{
				return true;
			}
			int state = this.xsm.BeginOutlook(XPathNodeType.Text);
			switch (this.builder.TextEvent(state, text, disableOutputEscaping))
			{
			case Processor.OutputResult.Continue:
				this.xsm.Begin(XPathNodeType.Text);
				return true;
			case Processor.OutputResult.Interrupt:
				this.xsm.Begin(XPathNodeType.Text);
				this.ExecutionResult = Processor.ExecResult.Interrupt;
				return true;
			case Processor.OutputResult.Overflow:
				this.ExecutionResult = Processor.ExecResult.Interrupt;
				return false;
			case Processor.OutputResult.Error:
			case Processor.OutputResult.Ignore:
				return true;
			default:
				return true;
			}
		}

		// Token: 0x060031CC RID: 12748 RVA: 0x00105974 File Offset: 0x00103B74
		internal bool EndEvent(XPathNodeType nodeType)
		{
			if (this.ignoreLevel > 0)
			{
				this.ignoreLevel--;
				return true;
			}
			int state = this.xsm.EndOutlook(nodeType);
			switch (this.builder.EndEvent(state, nodeType))
			{
			case Processor.OutputResult.Continue:
				this.xsm.End(nodeType);
				return true;
			case Processor.OutputResult.Interrupt:
				this.xsm.End(nodeType);
				this.ExecutionResult = Processor.ExecResult.Interrupt;
				return true;
			case Processor.OutputResult.Overflow:
				this.ExecutionResult = Processor.ExecResult.Interrupt;
				return false;
			}
			return true;
		}

		// Token: 0x060031CD RID: 12749 RVA: 0x00105A00 File Offset: 0x00103C00
		internal bool CopyBeginEvent(XPathNavigator node, bool emptyflag)
		{
			switch (node.NodeType)
			{
			case XPathNodeType.Element:
			case XPathNodeType.Attribute:
			case XPathNodeType.ProcessingInstruction:
			case XPathNodeType.Comment:
				return this.BeginEvent(node.NodeType, node.Prefix, node.LocalName, node.NamespaceURI, emptyflag);
			case XPathNodeType.Namespace:
				return this.BeginEvent(XPathNodeType.Namespace, null, node.LocalName, node.Value, false);
			}
			return true;
		}

		// Token: 0x060031CE RID: 12750 RVA: 0x00105A7C File Offset: 0x00103C7C
		internal bool CopyTextEvent(XPathNavigator node)
		{
			switch (node.NodeType)
			{
			case XPathNodeType.Attribute:
			case XPathNodeType.Text:
			case XPathNodeType.SignificantWhitespace:
			case XPathNodeType.Whitespace:
			case XPathNodeType.ProcessingInstruction:
			case XPathNodeType.Comment:
			{
				string value = node.Value;
				return this.TextEvent(value);
			}
			}
			return true;
		}

		// Token: 0x060031CF RID: 12751 RVA: 0x00105AD0 File Offset: 0x00103CD0
		internal bool CopyEndEvent(XPathNavigator node)
		{
			switch (node.NodeType)
			{
			case XPathNodeType.Element:
			case XPathNodeType.Attribute:
			case XPathNodeType.Namespace:
			case XPathNodeType.ProcessingInstruction:
			case XPathNodeType.Comment:
				return this.EndEvent(node.NodeType);
			}
			return true;
		}

		// Token: 0x060031D0 RID: 12752 RVA: 0x00105B22 File Offset: 0x00103D22
		internal static bool IsRoot(XPathNavigator navigator)
		{
			if (navigator.NodeType == XPathNodeType.Root)
			{
				return true;
			}
			if (navigator.NodeType == XPathNodeType.Element)
			{
				XPathNavigator xpathNavigator = navigator.Clone();
				xpathNavigator.MoveToRoot();
				return xpathNavigator.IsSamePosition(navigator);
			}
			return false;
		}

		// Token: 0x060031D1 RID: 12753 RVA: 0x00105B4C File Offset: 0x00103D4C
		internal void PushOutput(RecordOutput output)
		{
			this.builder.OutputState = this.xsm.State;
			RecordBuilder next = this.builder;
			this.builder = new RecordBuilder(output, this.nameTable);
			this.builder.Next = next;
			this.xsm.Reset();
		}

		// Token: 0x060031D2 RID: 12754 RVA: 0x00105BA0 File Offset: 0x00103DA0
		internal RecordOutput PopOutput()
		{
			RecordBuilder recordBuilder = this.builder;
			this.builder = recordBuilder.Next;
			this.xsm.State = this.builder.OutputState;
			recordBuilder.TheEnd();
			return recordBuilder.Output;
		}

		// Token: 0x060031D3 RID: 12755 RVA: 0x00105BE2 File Offset: 0x00103DE2
		internal bool SetDefaultOutput(XsltOutput.OutputMethod method)
		{
			if (this.Output.Method != method)
			{
				this.output = this.output.CreateDerivedOutput(method);
				return true;
			}
			return false;
		}

		// Token: 0x060031D4 RID: 12756 RVA: 0x00105C08 File Offset: 0x00103E08
		internal object GetVariableValue(VariableAction variable)
		{
			int varKey = variable.VarKey;
			if (!variable.IsGlobal)
			{
				return ((ActionFrame)this.actionStack.Peek()).GetVariable(varKey);
			}
			ActionFrame actionFrame = (ActionFrame)this.actionStack[0];
			object variable2 = actionFrame.GetVariable(varKey);
			if (variable2 == VariableAction.BeingComputedMark)
			{
				throw XsltException.Create("Circular reference in the definition of variable '{0}'.", new string[]
				{
					variable.NameStr
				});
			}
			if (variable2 != null)
			{
				return variable2;
			}
			int length = this.actionStack.Length;
			ActionFrame actionFrame2 = this.PushNewFrame();
			actionFrame2.Inherit(actionFrame);
			actionFrame2.Init(variable, actionFrame.NodeSet);
			do
			{
				if (((ActionFrame)this.actionStack.Peek()).Execute(this))
				{
					this.actionStack.Pop();
				}
			}
			while (length < this.actionStack.Length);
			return actionFrame.GetVariable(varKey);
		}

		// Token: 0x060031D5 RID: 12757 RVA: 0x00105CDF File Offset: 0x00103EDF
		internal void SetParameter(XmlQualifiedName name, object value)
		{
			((ActionFrame)this.actionStack[this.actionStack.Length - 2]).SetParameter(name, value);
		}

		// Token: 0x060031D6 RID: 12758 RVA: 0x00105D05 File Offset: 0x00103F05
		internal void ResetParams()
		{
			((ActionFrame)this.actionStack[this.actionStack.Length - 1]).ResetParams();
		}

		// Token: 0x060031D7 RID: 12759 RVA: 0x00105D29 File Offset: 0x00103F29
		internal object GetParameter(XmlQualifiedName name)
		{
			return ((ActionFrame)this.actionStack[this.actionStack.Length - 3]).GetParameter(name);
		}

		// Token: 0x060031D8 RID: 12760 RVA: 0x00105D50 File Offset: 0x00103F50
		internal void PushDebuggerStack()
		{
			Processor.DebuggerFrame debuggerFrame = (Processor.DebuggerFrame)this.debuggerStack.Push();
			if (debuggerFrame == null)
			{
				debuggerFrame = new Processor.DebuggerFrame();
				this.debuggerStack.AddToTop(debuggerFrame);
			}
			debuggerFrame.actionFrame = (ActionFrame)this.actionStack.Peek();
		}

		// Token: 0x060031D9 RID: 12761 RVA: 0x00105D99 File Offset: 0x00103F99
		internal void PopDebuggerStack()
		{
			this.debuggerStack.Pop();
		}

		// Token: 0x060031DA RID: 12762 RVA: 0x00105DA7 File Offset: 0x00103FA7
		internal void OnInstructionExecute()
		{
			((Processor.DebuggerFrame)this.debuggerStack.Peek()).actionFrame = (ActionFrame)this.actionStack.Peek();
			this.Debugger.OnInstructionExecute(this);
		}

		// Token: 0x060031DB RID: 12763 RVA: 0x00105DDA File Offset: 0x00103FDA
		internal XmlQualifiedName GetPrevioseMode()
		{
			return ((Processor.DebuggerFrame)this.debuggerStack[this.debuggerStack.Length - 2]).currentMode;
		}

		// Token: 0x060031DC RID: 12764 RVA: 0x00105DFE File Offset: 0x00103FFE
		internal void SetCurrentMode(XmlQualifiedName mode)
		{
			((Processor.DebuggerFrame)this.debuggerStack[this.debuggerStack.Length - 1]).currentMode = mode;
		}

		// Token: 0x17000A71 RID: 2673
		// (get) Token: 0x060031DD RID: 12765 RVA: 0x00105E23 File Offset: 0x00104023
		int IXsltProcessor.StackDepth
		{
			get
			{
				return this.debuggerStack.Length;
			}
		}

		// Token: 0x060031DE RID: 12766 RVA: 0x00105E30 File Offset: 0x00104030
		IStackFrame IXsltProcessor.GetStackFrame(int depth)
		{
			return ((Processor.DebuggerFrame)this.debuggerStack[depth]).actionFrame;
		}

		// Token: 0x040020C9 RID: 8393
		private const int StackIncrement = 10;

		// Token: 0x040020CA RID: 8394
		private Processor.ExecResult execResult;

		// Token: 0x040020CB RID: 8395
		private Stylesheet stylesheet;

		// Token: 0x040020CC RID: 8396
		private RootAction rootAction;

		// Token: 0x040020CD RID: 8397
		private Key[] keyList;

		// Token: 0x040020CE RID: 8398
		private List<TheQuery> queryStore;

		// Token: 0x040020CF RID: 8399
		public PermissionSet permissions;

		// Token: 0x040020D0 RID: 8400
		private XPathNavigator document;

		// Token: 0x040020D1 RID: 8401
		private HWStack actionStack;

		// Token: 0x040020D2 RID: 8402
		private HWStack debuggerStack;

		// Token: 0x040020D3 RID: 8403
		private StringBuilder sharedStringBuilder;

		// Token: 0x040020D4 RID: 8404
		private int ignoreLevel;

		// Token: 0x040020D5 RID: 8405
		private StateMachine xsm;

		// Token: 0x040020D6 RID: 8406
		private RecordBuilder builder;

		// Token: 0x040020D7 RID: 8407
		private XsltOutput output;

		// Token: 0x040020D8 RID: 8408
		private XmlNameTable nameTable = new NameTable();

		// Token: 0x040020D9 RID: 8409
		private XmlResolver resolver;

		// Token: 0x040020DA RID: 8410
		private XsltArgumentList args;

		// Token: 0x040020DB RID: 8411
		private Hashtable scriptExtensions;

		// Token: 0x040020DC RID: 8412
		private ArrayList numberList;

		// Token: 0x040020DD RID: 8413
		private TemplateLookupAction templateLookup = new TemplateLookupAction();

		// Token: 0x040020DE RID: 8414
		private IXsltDebugger debugger;

		// Token: 0x040020DF RID: 8415
		private Query[] queryList;

		// Token: 0x040020E0 RID: 8416
		private ArrayList sortArray;

		// Token: 0x040020E1 RID: 8417
		private Hashtable documentCache;

		// Token: 0x040020E2 RID: 8418
		private XsltCompileContext valueOfContext;

		// Token: 0x040020E3 RID: 8419
		private XsltCompileContext matchesContext;

		// Token: 0x020004FE RID: 1278
		internal enum ExecResult
		{
			// Token: 0x040020E5 RID: 8421
			Continue,
			// Token: 0x040020E6 RID: 8422
			Interrupt,
			// Token: 0x040020E7 RID: 8423
			Done
		}

		// Token: 0x020004FF RID: 1279
		internal enum OutputResult
		{
			// Token: 0x040020E9 RID: 8425
			Continue,
			// Token: 0x040020EA RID: 8426
			Interrupt,
			// Token: 0x040020EB RID: 8427
			Overflow,
			// Token: 0x040020EC RID: 8428
			Error,
			// Token: 0x040020ED RID: 8429
			Ignore
		}

		// Token: 0x02000500 RID: 1280
		internal class DebuggerFrame
		{
			// Token: 0x060031DF RID: 12767 RVA: 0x00002103 File Offset: 0x00000303
			public DebuggerFrame()
			{
			}

			// Token: 0x040020EE RID: 8430
			internal ActionFrame actionFrame;

			// Token: 0x040020EF RID: 8431
			internal XmlQualifiedName currentMode;
		}
	}
}
