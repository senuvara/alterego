﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200051A RID: 1306
	internal class ValueOfAction : CompiledAction
	{
		// Token: 0x060032E2 RID: 13026 RVA: 0x0010A2E6 File Offset: 0x001084E6
		internal static Action BuiltInRule()
		{
			return ValueOfAction.s_BuiltInRule;
		}

		// Token: 0x060032E3 RID: 13027 RVA: 0x0010A2ED File Offset: 0x001084ED
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.selectKey != -1, "select");
			base.CheckEmpty(compiler);
		}

		// Token: 0x060032E4 RID: 13028 RVA: 0x0010A318 File Offset: 0x00108518
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Select))
			{
				this.selectKey = compiler.AddQuery(value);
			}
			else
			{
				if (!Ref.Equal(localName, compiler.Atoms.DisableOutputEscaping))
				{
					return false;
				}
				this.disableOutputEscaping = compiler.GetYesNo(value);
			}
			return true;
		}

		// Token: 0x060032E5 RID: 13029 RVA: 0x0010A384 File Offset: 0x00108584
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 2)
				{
					return;
				}
				processor.TextEvent(frame.StoredOutput);
				frame.Finished();
				return;
			}
			else
			{
				string text = processor.ValueOf(frame, this.selectKey);
				if (processor.TextEvent(text, this.disableOutputEscaping))
				{
					frame.Finished();
					return;
				}
				frame.StoredOutput = text;
				frame.State = 2;
				return;
			}
		}

		// Token: 0x060032E6 RID: 13030 RVA: 0x0010A3E6 File Offset: 0x001085E6
		public ValueOfAction()
		{
		}

		// Token: 0x060032E7 RID: 13031 RVA: 0x0010A3F5 File Offset: 0x001085F5
		// Note: this type is marked as 'beforefieldinit'.
		static ValueOfAction()
		{
		}

		// Token: 0x040021B9 RID: 8633
		private const int ResultStored = 2;

		// Token: 0x040021BA RID: 8634
		private int selectKey = -1;

		// Token: 0x040021BB RID: 8635
		private bool disableOutputEscaping;

		// Token: 0x040021BC RID: 8636
		private static Action s_BuiltInRule = new BuiltInRuleTextAction();
	}
}
