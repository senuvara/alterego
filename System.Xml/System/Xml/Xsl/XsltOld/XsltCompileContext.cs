﻿using System;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Security;
using System.Xml.XPath;
using System.Xml.Xsl.Runtime;
using MS.Internal.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000520 RID: 1312
	internal class XsltCompileContext : XsltContext
	{
		// Token: 0x06003300 RID: 13056 RVA: 0x0010AA11 File Offset: 0x00108C11
		internal XsltCompileContext(InputScopeManager manager, Processor processor) : base(false)
		{
			this.manager = manager;
			this.processor = processor;
		}

		// Token: 0x06003301 RID: 13057 RVA: 0x0010AA28 File Offset: 0x00108C28
		internal XsltCompileContext() : base(false)
		{
		}

		// Token: 0x06003302 RID: 13058 RVA: 0x0010AA31 File Offset: 0x00108C31
		internal void Recycle()
		{
			this.manager = null;
			this.processor = null;
		}

		// Token: 0x06003303 RID: 13059 RVA: 0x0010AA41 File Offset: 0x00108C41
		internal void Reinitialize(InputScopeManager manager, Processor processor)
		{
			this.manager = manager;
			this.processor = processor;
		}

		// Token: 0x06003304 RID: 13060 RVA: 0x0010AA51 File Offset: 0x00108C51
		public override int CompareDocument(string baseUri, string nextbaseUri)
		{
			return string.Compare(baseUri, nextbaseUri, StringComparison.Ordinal);
		}

		// Token: 0x17000AB2 RID: 2738
		// (get) Token: 0x06003305 RID: 13061 RVA: 0x00003201 File Offset: 0x00001401
		public override string DefaultNamespace
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x06003306 RID: 13062 RVA: 0x0010AA5B File Offset: 0x00108C5B
		public override string LookupNamespace(string prefix)
		{
			return this.manager.ResolveXPathNamespace(prefix);
		}

		// Token: 0x06003307 RID: 13063 RVA: 0x0010AA6C File Offset: 0x00108C6C
		public override IXsltContextVariable ResolveVariable(string prefix, string name)
		{
			string ns = this.LookupNamespace(prefix);
			XmlQualifiedName xmlQualifiedName = new XmlQualifiedName(name, ns);
			IXsltContextVariable xsltContextVariable = this.manager.VariableScope.ResolveVariable(xmlQualifiedName);
			if (xsltContextVariable == null)
			{
				throw XsltException.Create("The variable or parameter '{0}' is either not defined or it is out of scope.", new string[]
				{
					xmlQualifiedName.ToString()
				});
			}
			return xsltContextVariable;
		}

		// Token: 0x06003308 RID: 13064 RVA: 0x0010AABC File Offset: 0x00108CBC
		internal object EvaluateVariable(VariableAction variable)
		{
			object variableValue = this.processor.GetVariableValue(variable);
			if (variableValue == null && !variable.IsGlobal)
			{
				VariableAction variableAction = this.manager.VariableScope.ResolveGlobalVariable(variable.Name);
				if (variableAction != null)
				{
					variableValue = this.processor.GetVariableValue(variableAction);
				}
			}
			if (variableValue == null)
			{
				throw XsltException.Create("The variable or parameter '{0}' is either not defined or it is out of scope.", new string[]
				{
					variable.Name.ToString()
				});
			}
			return variableValue;
		}

		// Token: 0x17000AB3 RID: 2739
		// (get) Token: 0x06003309 RID: 13065 RVA: 0x0010AB2B File Offset: 0x00108D2B
		public override bool Whitespace
		{
			get
			{
				return this.processor.Stylesheet.Whitespace;
			}
		}

		// Token: 0x0600330A RID: 13066 RVA: 0x0010AB3D File Offset: 0x00108D3D
		public override bool PreserveWhitespace(XPathNavigator node)
		{
			node = node.Clone();
			node.MoveToParent();
			return this.processor.Stylesheet.PreserveWhiteSpace(this.processor, node);
		}

		// Token: 0x0600330B RID: 13067 RVA: 0x0010AB68 File Offset: 0x00108D68
		private MethodInfo FindBestMethod(MethodInfo[] methods, bool ignoreCase, bool publicOnly, string name, XPathResultType[] argTypes)
		{
			int num = methods.Length;
			int num2 = 0;
			for (int i = 0; i < num; i++)
			{
				if (string.Compare(name, methods[i].Name, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal) == 0 && (!publicOnly || methods[i].GetBaseDefinition().IsPublic))
				{
					methods[num2++] = methods[i];
				}
			}
			num = num2;
			if (num == 0)
			{
				return null;
			}
			if (argTypes == null)
			{
				return methods[0];
			}
			num2 = 0;
			for (int j = 0; j < num; j++)
			{
				if (methods[j].GetParameters().Length == argTypes.Length)
				{
					methods[num2++] = methods[j];
				}
			}
			num = num2;
			if (num <= 1)
			{
				return methods[0];
			}
			num2 = 0;
			for (int k = 0; k < num; k++)
			{
				bool flag = true;
				ParameterInfo[] parameters = methods[k].GetParameters();
				for (int l = 0; l < parameters.Length; l++)
				{
					XPathResultType xpathResultType = argTypes[l];
					if (xpathResultType != XPathResultType.Any)
					{
						XPathResultType xpathType = XsltCompileContext.GetXPathType(parameters[l].ParameterType);
						if (xpathType != xpathResultType && xpathType != XPathResultType.Any)
						{
							flag = false;
							break;
						}
					}
				}
				if (flag)
				{
					methods[num2++] = methods[k];
				}
			}
			return methods[0];
		}

		// Token: 0x0600330C RID: 13068 RVA: 0x0010AC70 File Offset: 0x00108E70
		private IXsltContextFunction GetExtentionMethod(string ns, string name, XPathResultType[] argTypes, out object extension)
		{
			XsltCompileContext.FuncExtension result = null;
			extension = this.processor.GetScriptObject(ns);
			if (extension != null)
			{
				MethodInfo methodInfo = this.FindBestMethod(extension.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic), true, false, name, argTypes);
				if (methodInfo != null)
				{
					result = new XsltCompileContext.FuncExtension(extension, methodInfo, null);
				}
				return result;
			}
			extension = this.processor.GetExtensionObject(ns);
			if (extension != null)
			{
				MethodInfo methodInfo2 = this.FindBestMethod(extension.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic), false, true, name, argTypes);
				if (methodInfo2 != null)
				{
					result = new XsltCompileContext.FuncExtension(extension, methodInfo2, this.processor.permissions);
				}
				return result;
			}
			return null;
		}

		// Token: 0x0600330D RID: 13069 RVA: 0x0010AD14 File Offset: 0x00108F14
		public override IXsltContextFunction ResolveFunction(string prefix, string name, XPathResultType[] argTypes)
		{
			IXsltContextFunction xsltContextFunction;
			if (prefix.Length == 0)
			{
				xsltContextFunction = (XsltCompileContext.s_FunctionTable[name] as IXsltContextFunction);
			}
			else
			{
				string text = this.LookupNamespace(prefix);
				if (text == "urn:schemas-microsoft-com:xslt" && name == "node-set")
				{
					xsltContextFunction = XsltCompileContext.s_FuncNodeSet;
				}
				else
				{
					object obj;
					xsltContextFunction = this.GetExtentionMethod(text, name, argTypes, out obj);
					if (obj == null)
					{
						throw XsltException.Create("Cannot find the script or external object that implements prefix '{0}'.", new string[]
						{
							prefix
						});
					}
				}
			}
			if (xsltContextFunction == null)
			{
				throw XsltException.Create("'{0}()' is an unknown XSLT function.", new string[]
				{
					name
				});
			}
			if (argTypes.Length < xsltContextFunction.Minargs || xsltContextFunction.Maxargs < argTypes.Length)
			{
				throw XsltException.Create("XSLT function '{0}()' has the wrong number of arguments.", new string[]
				{
					name,
					argTypes.Length.ToString(CultureInfo.InvariantCulture)
				});
			}
			return xsltContextFunction;
		}

		// Token: 0x0600330E RID: 13070 RVA: 0x0010ADE4 File Offset: 0x00108FE4
		private Uri ComposeUri(string thisUri, string baseUri)
		{
			XmlResolver resolver = this.processor.Resolver;
			Uri baseUri2 = null;
			if (baseUri.Length != 0)
			{
				baseUri2 = resolver.ResolveUri(null, baseUri);
			}
			return resolver.ResolveUri(baseUri2, thisUri);
		}

		// Token: 0x0600330F RID: 13071 RVA: 0x0010AE18 File Offset: 0x00109018
		private XPathNodeIterator Document(object arg0, string baseUri)
		{
			if (this.processor.permissions != null)
			{
				this.processor.permissions.PermitOnly();
			}
			XPathNodeIterator xpathNodeIterator = arg0 as XPathNodeIterator;
			if (xpathNodeIterator != null)
			{
				ArrayList arrayList = new ArrayList();
				Hashtable hashtable = new Hashtable();
				while (xpathNodeIterator.MoveNext())
				{
					Uri uri = this.ComposeUri(xpathNodeIterator.Current.Value, baseUri ?? xpathNodeIterator.Current.BaseURI);
					if (!hashtable.ContainsKey(uri))
					{
						hashtable.Add(uri, null);
						arrayList.Add(this.processor.GetNavigator(uri));
					}
				}
				return new XPathArrayIterator(arrayList);
			}
			return new XPathSingletonIterator(this.processor.GetNavigator(this.ComposeUri(XmlConvert.ToXPathString(arg0), baseUri ?? this.manager.Navigator.BaseURI)));
		}

		// Token: 0x06003310 RID: 13072 RVA: 0x0010AEE0 File Offset: 0x001090E0
		private Hashtable BuildKeyTable(Key key, XPathNavigator root)
		{
			Hashtable hashtable = new Hashtable();
			string queryExpression = this.processor.GetQueryExpression(key.MatchKey);
			Query compiledQuery = this.processor.GetCompiledQuery(key.MatchKey);
			Query compiledQuery2 = this.processor.GetCompiledQuery(key.UseKey);
			XPathNodeIterator xpathNodeIterator = root.SelectDescendants(XPathNodeType.All, false);
			while (xpathNodeIterator.MoveNext())
			{
				XPathNavigator xpathNavigator = xpathNodeIterator.Current;
				XsltCompileContext.EvaluateKey(xpathNavigator, compiledQuery, queryExpression, compiledQuery2, hashtable);
				if (xpathNavigator.MoveToFirstAttribute())
				{
					do
					{
						XsltCompileContext.EvaluateKey(xpathNavigator, compiledQuery, queryExpression, compiledQuery2, hashtable);
					}
					while (xpathNavigator.MoveToNextAttribute());
					xpathNavigator.MoveToParent();
				}
			}
			return hashtable;
		}

		// Token: 0x06003311 RID: 13073 RVA: 0x0010AF7C File Offset: 0x0010917C
		private static void AddKeyValue(Hashtable keyTable, string key, XPathNavigator value, bool checkDuplicates)
		{
			ArrayList arrayList = (ArrayList)keyTable[key];
			if (arrayList == null)
			{
				arrayList = new ArrayList();
				keyTable.Add(key, arrayList);
			}
			else if (checkDuplicates && value.ComparePosition((XPathNavigator)arrayList[arrayList.Count - 1]) == XmlNodeOrder.Same)
			{
				return;
			}
			arrayList.Add(value.Clone());
		}

		// Token: 0x06003312 RID: 13074 RVA: 0x0010AFD8 File Offset: 0x001091D8
		private static void EvaluateKey(XPathNavigator node, Query matchExpr, string matchStr, Query useExpr, Hashtable keyTable)
		{
			try
			{
				if (matchExpr.MatchNode(node) == null)
				{
					return;
				}
			}
			catch (XPathException)
			{
				throw XsltException.Create("'{0}' is an invalid XSLT pattern.", new string[]
				{
					matchStr
				});
			}
			object obj = useExpr.Evaluate(new XPathSingletonIterator(node, true));
			XPathNodeIterator xpathNodeIterator = obj as XPathNodeIterator;
			if (xpathNodeIterator != null)
			{
				bool checkDuplicates = false;
				while (xpathNodeIterator.MoveNext())
				{
					XPathNavigator xpathNavigator = xpathNodeIterator.Current;
					XsltCompileContext.AddKeyValue(keyTable, xpathNavigator.Value, node, checkDuplicates);
					checkDuplicates = true;
				}
				return;
			}
			string key = XmlConvert.ToXPathString(obj);
			XsltCompileContext.AddKeyValue(keyTable, key, node, false);
		}

		// Token: 0x06003313 RID: 13075 RVA: 0x0010B064 File Offset: 0x00109264
		private DecimalFormat ResolveFormatName(string formatName)
		{
			string ns = string.Empty;
			string empty = string.Empty;
			if (formatName != null)
			{
				string prefix;
				PrefixQName.ParseQualifiedName(formatName, out prefix, out empty);
				ns = this.LookupNamespace(prefix);
			}
			DecimalFormat decimalFormat = this.processor.RootAction.GetDecimalFormat(new XmlQualifiedName(empty, ns));
			if (decimalFormat == null)
			{
				if (formatName != null)
				{
					throw XsltException.Create("Decimal format '{0}' is not defined.", new string[]
					{
						formatName
					});
				}
				decimalFormat = new DecimalFormat(new NumberFormatInfo(), '#', '0', ';');
			}
			return decimalFormat;
		}

		// Token: 0x06003314 RID: 13076 RVA: 0x0010B0D8 File Offset: 0x001092D8
		private bool ElementAvailable(string qname)
		{
			string prefix;
			string a;
			PrefixQName.ParseQualifiedName(qname, out prefix, out a);
			return this.manager.ResolveXmlNamespace(prefix) == "http://www.w3.org/1999/XSL/Transform" && (a == "apply-imports" || a == "apply-templates" || a == "attribute" || a == "call-template" || a == "choose" || a == "comment" || a == "copy" || a == "copy-of" || a == "element" || a == "fallback" || a == "for-each" || a == "if" || a == "message" || a == "number" || a == "processing-instruction" || a == "text" || a == "value-of" || a == "variable");
		}

		// Token: 0x06003315 RID: 13077 RVA: 0x0010B210 File Offset: 0x00109410
		private bool FunctionAvailable(string qname)
		{
			string prefix;
			string text;
			PrefixQName.ParseQualifiedName(qname, out prefix, out text);
			string text2 = this.LookupNamespace(prefix);
			if (text2 == "urn:schemas-microsoft-com:xslt")
			{
				return text == "node-set";
			}
			if (text2.Length == 0)
			{
				return text == "last" || text == "position" || text == "name" || text == "namespace-uri" || text == "local-name" || text == "count" || text == "id" || text == "string" || text == "concat" || text == "starts-with" || text == "contains" || text == "substring-before" || text == "substring-after" || text == "substring" || text == "string-length" || text == "normalize-space" || text == "translate" || text == "boolean" || text == "not" || text == "true" || text == "false" || text == "lang" || text == "number" || text == "sum" || text == "floor" || text == "ceiling" || text == "round" || (XsltCompileContext.s_FunctionTable[text] != null && text != "unparsed-entity-uri");
			}
			object obj;
			return this.GetExtentionMethod(text2, text, null, out obj) != null;
		}

		// Token: 0x06003316 RID: 13078 RVA: 0x0010B418 File Offset: 0x00109618
		private XPathNodeIterator Current()
		{
			XPathNavigator xpathNavigator = this.processor.Current;
			if (xpathNavigator != null)
			{
				return new XPathSingletonIterator(xpathNavigator.Clone());
			}
			return XPathEmptyIterator.Instance;
		}

		// Token: 0x06003317 RID: 13079 RVA: 0x0010B448 File Offset: 0x00109648
		private string SystemProperty(string qname)
		{
			string result = string.Empty;
			string text;
			string a;
			PrefixQName.ParseQualifiedName(qname, out text, out a);
			string text2 = this.LookupNamespace(text);
			if (text2 == "http://www.w3.org/1999/XSL/Transform")
			{
				if (a == "version")
				{
					result = "1";
				}
				else if (a == "vendor")
				{
					result = "Microsoft";
				}
				else if (a == "vendor-url")
				{
					result = "http://www.microsoft.com";
				}
				return result;
			}
			if (text2 == null && text != null)
			{
				throw XsltException.Create("Prefix '{0}' is not defined.", new string[]
				{
					text
				});
			}
			return string.Empty;
		}

		// Token: 0x06003318 RID: 13080 RVA: 0x0010B4DC File Offset: 0x001096DC
		public static XPathResultType GetXPathType(Type type)
		{
			TypeCode typeCode = Type.GetTypeCode(type);
			if (typeCode <= TypeCode.Boolean)
			{
				if (typeCode != TypeCode.Object)
				{
					if (typeCode == TypeCode.Boolean)
					{
						return XPathResultType.Boolean;
					}
				}
				else
				{
					if (typeof(XPathNavigator).IsAssignableFrom(type) || typeof(IXPathNavigable).IsAssignableFrom(type))
					{
						return XPathResultType.String;
					}
					if (typeof(XPathNodeIterator).IsAssignableFrom(type))
					{
						return XPathResultType.NodeSet;
					}
					return XPathResultType.Any;
				}
			}
			else
			{
				if (typeCode == TypeCode.DateTime)
				{
					return XPathResultType.Error;
				}
				if (typeCode == TypeCode.String)
				{
					return XPathResultType.String;
				}
			}
			return XPathResultType.Number;
		}

		// Token: 0x06003319 RID: 13081 RVA: 0x0010B54C File Offset: 0x0010974C
		private static Hashtable CreateFunctionTable()
		{
			Hashtable hashtable = new Hashtable(10);
			hashtable["current"] = new XsltCompileContext.FuncCurrent();
			hashtable["unparsed-entity-uri"] = new XsltCompileContext.FuncUnEntityUri();
			hashtable["generate-id"] = new XsltCompileContext.FuncGenerateId();
			hashtable["system-property"] = new XsltCompileContext.FuncSystemProp();
			hashtable["element-available"] = new XsltCompileContext.FuncElementAvailable();
			hashtable["function-available"] = new XsltCompileContext.FuncFunctionAvailable();
			hashtable["document"] = new XsltCompileContext.FuncDocument();
			hashtable["key"] = new XsltCompileContext.FuncKey();
			hashtable["format-number"] = new XsltCompileContext.FuncFormatNumber();
			return hashtable;
		}

		// Token: 0x0600331A RID: 13082 RVA: 0x0010B5F0 File Offset: 0x001097F0
		// Note: this type is marked as 'beforefieldinit'.
		static XsltCompileContext()
		{
		}

		// Token: 0x040021CF RID: 8655
		private InputScopeManager manager;

		// Token: 0x040021D0 RID: 8656
		private Processor processor;

		// Token: 0x040021D1 RID: 8657
		private static Hashtable s_FunctionTable = XsltCompileContext.CreateFunctionTable();

		// Token: 0x040021D2 RID: 8658
		private static IXsltContextFunction s_FuncNodeSet = new XsltCompileContext.FuncNodeSet();

		// Token: 0x040021D3 RID: 8659
		private const string f_NodeSet = "node-set";

		// Token: 0x040021D4 RID: 8660
		private const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

		// Token: 0x02000521 RID: 1313
		private abstract class XsltFunctionImpl : IXsltContextFunction
		{
			// Token: 0x0600331B RID: 13083 RVA: 0x00002103 File Offset: 0x00000303
			public XsltFunctionImpl()
			{
			}

			// Token: 0x0600331C RID: 13084 RVA: 0x0010B606 File Offset: 0x00109806
			public XsltFunctionImpl(int minArgs, int maxArgs, XPathResultType returnType, XPathResultType[] argTypes)
			{
				this.Init(minArgs, maxArgs, returnType, argTypes);
			}

			// Token: 0x0600331D RID: 13085 RVA: 0x0010B619 File Offset: 0x00109819
			protected void Init(int minArgs, int maxArgs, XPathResultType returnType, XPathResultType[] argTypes)
			{
				this.minargs = minArgs;
				this.maxargs = maxArgs;
				this.returnType = returnType;
				this.argTypes = argTypes;
			}

			// Token: 0x17000AB4 RID: 2740
			// (get) Token: 0x0600331E RID: 13086 RVA: 0x0010B638 File Offset: 0x00109838
			public int Minargs
			{
				get
				{
					return this.minargs;
				}
			}

			// Token: 0x17000AB5 RID: 2741
			// (get) Token: 0x0600331F RID: 13087 RVA: 0x0010B640 File Offset: 0x00109840
			public int Maxargs
			{
				get
				{
					return this.maxargs;
				}
			}

			// Token: 0x17000AB6 RID: 2742
			// (get) Token: 0x06003320 RID: 13088 RVA: 0x0010B648 File Offset: 0x00109848
			public XPathResultType ReturnType
			{
				get
				{
					return this.returnType;
				}
			}

			// Token: 0x17000AB7 RID: 2743
			// (get) Token: 0x06003321 RID: 13089 RVA: 0x0010B650 File Offset: 0x00109850
			public XPathResultType[] ArgTypes
			{
				get
				{
					return this.argTypes;
				}
			}

			// Token: 0x06003322 RID: 13090
			public abstract object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext);

			// Token: 0x06003323 RID: 13091 RVA: 0x0010B658 File Offset: 0x00109858
			public static XPathNodeIterator ToIterator(object argument)
			{
				XPathNodeIterator xpathNodeIterator = argument as XPathNodeIterator;
				if (xpathNodeIterator == null)
				{
					throw XsltException.Create("Cannot convert the operand to a node-set.", Array.Empty<string>());
				}
				return xpathNodeIterator;
			}

			// Token: 0x06003324 RID: 13092 RVA: 0x0010B673 File Offset: 0x00109873
			public static XPathNavigator ToNavigator(object argument)
			{
				XPathNavigator xpathNavigator = argument as XPathNavigator;
				if (xpathNavigator == null)
				{
					throw XsltException.Create("Cannot convert the operand to 'Result tree fragment'.", Array.Empty<string>());
				}
				return xpathNavigator;
			}

			// Token: 0x06003325 RID: 13093 RVA: 0x0010B68E File Offset: 0x0010988E
			private static string IteratorToString(XPathNodeIterator it)
			{
				if (it.MoveNext())
				{
					return it.Current.Value;
				}
				return string.Empty;
			}

			// Token: 0x06003326 RID: 13094 RVA: 0x0010B6AC File Offset: 0x001098AC
			public static string ToString(object argument)
			{
				XPathNodeIterator xpathNodeIterator = argument as XPathNodeIterator;
				if (xpathNodeIterator != null)
				{
					return XsltCompileContext.XsltFunctionImpl.IteratorToString(xpathNodeIterator);
				}
				return XmlConvert.ToXPathString(argument);
			}

			// Token: 0x06003327 RID: 13095 RVA: 0x0010B6D0 File Offset: 0x001098D0
			public static bool ToBoolean(object argument)
			{
				XPathNodeIterator xpathNodeIterator = argument as XPathNodeIterator;
				if (xpathNodeIterator != null)
				{
					return Convert.ToBoolean(XsltCompileContext.XsltFunctionImpl.IteratorToString(xpathNodeIterator), CultureInfo.InvariantCulture);
				}
				XPathNavigator xpathNavigator = argument as XPathNavigator;
				if (xpathNavigator != null)
				{
					return Convert.ToBoolean(xpathNavigator.ToString(), CultureInfo.InvariantCulture);
				}
				return Convert.ToBoolean(argument, CultureInfo.InvariantCulture);
			}

			// Token: 0x06003328 RID: 13096 RVA: 0x0010B720 File Offset: 0x00109920
			public static double ToNumber(object argument)
			{
				XPathNodeIterator xpathNodeIterator = argument as XPathNodeIterator;
				if (xpathNodeIterator != null)
				{
					return XmlConvert.ToXPathDouble(XsltCompileContext.XsltFunctionImpl.IteratorToString(xpathNodeIterator));
				}
				XPathNavigator xpathNavigator = argument as XPathNavigator;
				if (xpathNavigator != null)
				{
					return XmlConvert.ToXPathDouble(xpathNavigator.ToString());
				}
				return XmlConvert.ToXPathDouble(argument);
			}

			// Token: 0x06003329 RID: 13097 RVA: 0x0010B75F File Offset: 0x0010995F
			private static object ToNumeric(object argument, TypeCode typeCode)
			{
				return Convert.ChangeType(XsltCompileContext.XsltFunctionImpl.ToNumber(argument), typeCode, CultureInfo.InvariantCulture);
			}

			// Token: 0x0600332A RID: 13098 RVA: 0x0010B778 File Offset: 0x00109978
			public static object ConvertToXPathType(object val, XPathResultType xt, TypeCode typeCode)
			{
				switch (xt)
				{
				case XPathResultType.Number:
					return XsltCompileContext.XsltFunctionImpl.ToNumeric(val, typeCode);
				case XPathResultType.String:
					if (typeCode == TypeCode.String)
					{
						return XsltCompileContext.XsltFunctionImpl.ToString(val);
					}
					return XsltCompileContext.XsltFunctionImpl.ToNavigator(val);
				case XPathResultType.Boolean:
					return XsltCompileContext.XsltFunctionImpl.ToBoolean(val);
				case XPathResultType.NodeSet:
					return XsltCompileContext.XsltFunctionImpl.ToIterator(val);
				case XPathResultType.Any:
				case XPathResultType.Error:
					return val;
				}
				return val;
			}

			// Token: 0x040021D5 RID: 8661
			private int minargs;

			// Token: 0x040021D6 RID: 8662
			private int maxargs;

			// Token: 0x040021D7 RID: 8663
			private XPathResultType returnType;

			// Token: 0x040021D8 RID: 8664
			private XPathResultType[] argTypes;
		}

		// Token: 0x02000522 RID: 1314
		private class FuncCurrent : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x0600332B RID: 13099 RVA: 0x0010B7DA File Offset: 0x001099DA
			public FuncCurrent() : base(0, 0, XPathResultType.NodeSet, new XPathResultType[0])
			{
			}

			// Token: 0x0600332C RID: 13100 RVA: 0x0010B7EB File Offset: 0x001099EB
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				return ((XsltCompileContext)xsltContext).Current();
			}
		}

		// Token: 0x02000523 RID: 1315
		private class FuncUnEntityUri : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x0600332D RID: 13101 RVA: 0x0010B7F8 File Offset: 0x001099F8
			public FuncUnEntityUri() : base(1, 1, XPathResultType.String, new XPathResultType[]
			{
				XPathResultType.String
			})
			{
			}

			// Token: 0x0600332E RID: 13102 RVA: 0x0010B80D File Offset: 0x00109A0D
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				throw XsltException.Create("'{0}()' is an unsupported XSLT function.", new string[]
				{
					"unparsed-entity-uri"
				});
			}
		}

		// Token: 0x02000524 RID: 1316
		private class FuncGenerateId : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x0600332F RID: 13103 RVA: 0x0010B827 File Offset: 0x00109A27
			public FuncGenerateId() : base(0, 1, XPathResultType.String, new XPathResultType[]
			{
				XPathResultType.NodeSet
			})
			{
			}

			// Token: 0x06003330 RID: 13104 RVA: 0x0010B83C File Offset: 0x00109A3C
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				if (args.Length == 0)
				{
					return docContext.UniqueId;
				}
				XPathNodeIterator xpathNodeIterator = XsltCompileContext.XsltFunctionImpl.ToIterator(args[0]);
				if (xpathNodeIterator.MoveNext())
				{
					return xpathNodeIterator.Current.UniqueId;
				}
				return string.Empty;
			}
		}

		// Token: 0x02000525 RID: 1317
		private class FuncSystemProp : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x06003331 RID: 13105 RVA: 0x0010B7F8 File Offset: 0x001099F8
			public FuncSystemProp() : base(1, 1, XPathResultType.String, new XPathResultType[]
			{
				XPathResultType.String
			})
			{
			}

			// Token: 0x06003332 RID: 13106 RVA: 0x0010B876 File Offset: 0x00109A76
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				return ((XsltCompileContext)xsltContext).SystemProperty(XsltCompileContext.XsltFunctionImpl.ToString(args[0]));
			}
		}

		// Token: 0x02000526 RID: 1318
		private class FuncElementAvailable : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x06003333 RID: 13107 RVA: 0x0010B88B File Offset: 0x00109A8B
			public FuncElementAvailable() : base(1, 1, XPathResultType.Boolean, new XPathResultType[]
			{
				XPathResultType.String
			})
			{
			}

			// Token: 0x06003334 RID: 13108 RVA: 0x0010B8A0 File Offset: 0x00109AA0
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				return ((XsltCompileContext)xsltContext).ElementAvailable(XsltCompileContext.XsltFunctionImpl.ToString(args[0]));
			}
		}

		// Token: 0x02000527 RID: 1319
		private class FuncFunctionAvailable : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x06003335 RID: 13109 RVA: 0x0010B88B File Offset: 0x00109A8B
			public FuncFunctionAvailable() : base(1, 1, XPathResultType.Boolean, new XPathResultType[]
			{
				XPathResultType.String
			})
			{
			}

			// Token: 0x06003336 RID: 13110 RVA: 0x0010B8BA File Offset: 0x00109ABA
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				return ((XsltCompileContext)xsltContext).FunctionAvailable(XsltCompileContext.XsltFunctionImpl.ToString(args[0]));
			}
		}

		// Token: 0x02000528 RID: 1320
		private class FuncDocument : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x06003337 RID: 13111 RVA: 0x0010B8D4 File Offset: 0x00109AD4
			public FuncDocument() : base(1, 2, XPathResultType.NodeSet, new XPathResultType[]
			{
				XPathResultType.Any,
				XPathResultType.NodeSet
			})
			{
			}

			// Token: 0x06003338 RID: 13112 RVA: 0x0010B8F0 File Offset: 0x00109AF0
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				string baseUri = null;
				if (args.Length == 2)
				{
					XPathNodeIterator xpathNodeIterator = XsltCompileContext.XsltFunctionImpl.ToIterator(args[1]);
					if (xpathNodeIterator.MoveNext())
					{
						baseUri = xpathNodeIterator.Current.BaseURI;
					}
					else
					{
						baseUri = string.Empty;
					}
				}
				object result;
				try
				{
					result = ((XsltCompileContext)xsltContext).Document(args[0], baseUri);
				}
				catch (Exception e)
				{
					if (!XmlException.IsCatchableException(e))
					{
						throw;
					}
					result = XPathEmptyIterator.Instance;
				}
				return result;
			}
		}

		// Token: 0x02000529 RID: 1321
		private class FuncKey : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x06003339 RID: 13113 RVA: 0x0010B960 File Offset: 0x00109B60
			public FuncKey() : base(2, 2, XPathResultType.NodeSet, new XPathResultType[]
			{
				XPathResultType.String,
				XPathResultType.Any
			})
			{
			}

			// Token: 0x0600333A RID: 13114 RVA: 0x0010B97C File Offset: 0x00109B7C
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				XsltCompileContext xsltCompileContext = (XsltCompileContext)xsltContext;
				string prefix;
				string name;
				PrefixQName.ParseQualifiedName(XsltCompileContext.XsltFunctionImpl.ToString(args[0]), out prefix, out name);
				string ns = xsltContext.LookupNamespace(prefix);
				XmlQualifiedName b = new XmlQualifiedName(name, ns);
				XPathNavigator xpathNavigator = docContext.Clone();
				xpathNavigator.MoveToRoot();
				ArrayList arrayList = null;
				foreach (Key key in xsltCompileContext.processor.KeyList)
				{
					if (key.Name == b)
					{
						Hashtable hashtable = key.GetKeys(xpathNavigator);
						if (hashtable == null)
						{
							hashtable = xsltCompileContext.BuildKeyTable(key, xpathNavigator);
							key.AddKey(xpathNavigator, hashtable);
						}
						XPathNodeIterator xpathNodeIterator = args[1] as XPathNodeIterator;
						if (xpathNodeIterator != null)
						{
							xpathNodeIterator = xpathNodeIterator.Clone();
							while (xpathNodeIterator.MoveNext())
							{
								XPathNavigator xpathNavigator2 = xpathNodeIterator.Current;
								arrayList = XsltCompileContext.FuncKey.AddToList(arrayList, (ArrayList)hashtable[xpathNavigator2.Value]);
							}
						}
						else
						{
							arrayList = XsltCompileContext.FuncKey.AddToList(arrayList, (ArrayList)hashtable[XsltCompileContext.XsltFunctionImpl.ToString(args[1])]);
						}
					}
				}
				if (arrayList == null)
				{
					return XPathEmptyIterator.Instance;
				}
				if (arrayList[0] is XPathNavigator)
				{
					return new XPathArrayIterator(arrayList);
				}
				return new XPathMultyIterator(arrayList);
			}

			// Token: 0x0600333B RID: 13115 RVA: 0x0010BAB4 File Offset: 0x00109CB4
			private static ArrayList AddToList(ArrayList resultCollection, ArrayList newList)
			{
				if (newList == null)
				{
					return resultCollection;
				}
				if (resultCollection == null)
				{
					return newList;
				}
				if (!(resultCollection[0] is ArrayList))
				{
					ArrayList value = resultCollection;
					resultCollection = new ArrayList();
					resultCollection.Add(value);
				}
				resultCollection.Add(newList);
				return resultCollection;
			}
		}

		// Token: 0x0200052A RID: 1322
		private class FuncFormatNumber : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x0600333C RID: 13116 RVA: 0x0010BAF3 File Offset: 0x00109CF3
			public FuncFormatNumber() : base(2, 3, XPathResultType.String, new XPathResultType[]
			{
				XPathResultType.Number,
				XPathResultType.String,
				XPathResultType.String
			})
			{
			}

			// Token: 0x0600333D RID: 13117 RVA: 0x0010BB0C File Offset: 0x00109D0C
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				DecimalFormat decimalFormat = ((XsltCompileContext)xsltContext).ResolveFormatName((args.Length == 3) ? XsltCompileContext.XsltFunctionImpl.ToString(args[2]) : null);
				return DecimalFormatter.Format(XsltCompileContext.XsltFunctionImpl.ToNumber(args[0]), XsltCompileContext.XsltFunctionImpl.ToString(args[1]), decimalFormat);
			}
		}

		// Token: 0x0200052B RID: 1323
		private class FuncNodeSet : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x0600333E RID: 13118 RVA: 0x0010BB4C File Offset: 0x00109D4C
			public FuncNodeSet() : base(1, 1, XPathResultType.NodeSet, new XPathResultType[]
			{
				XPathResultType.String
			})
			{
			}

			// Token: 0x0600333F RID: 13119 RVA: 0x0010BB61 File Offset: 0x00109D61
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				return new XPathSingletonIterator(XsltCompileContext.XsltFunctionImpl.ToNavigator(args[0]));
			}
		}

		// Token: 0x0200052C RID: 1324
		private class FuncExtension : XsltCompileContext.XsltFunctionImpl
		{
			// Token: 0x06003340 RID: 13120 RVA: 0x0010BB70 File Offset: 0x00109D70
			public FuncExtension(object extension, MethodInfo method, PermissionSet permissions)
			{
				this.extension = extension;
				this.method = method;
				this.permissions = permissions;
				XPathResultType xpathType = XsltCompileContext.GetXPathType(method.ReturnType);
				ParameterInfo[] parameters = method.GetParameters();
				int num = parameters.Length;
				int maxArgs = parameters.Length;
				this.typeCodes = new TypeCode[parameters.Length];
				XPathResultType[] array = new XPathResultType[parameters.Length];
				bool flag = true;
				int num2 = parameters.Length - 1;
				while (0 <= num2)
				{
					this.typeCodes[num2] = Type.GetTypeCode(parameters[num2].ParameterType);
					array[num2] = XsltCompileContext.GetXPathType(parameters[num2].ParameterType);
					if (flag)
					{
						if (parameters[num2].IsOptional)
						{
							num--;
						}
						else
						{
							flag = false;
						}
					}
					num2--;
				}
				base.Init(num, maxArgs, xpathType, array);
			}

			// Token: 0x06003341 RID: 13121 RVA: 0x0010BC30 File Offset: 0x00109E30
			public override object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext)
			{
				int num = args.Length - 1;
				while (0 <= num)
				{
					args[num] = XsltCompileContext.XsltFunctionImpl.ConvertToXPathType(args[num], base.ArgTypes[num], this.typeCodes[num]);
					num--;
				}
				if (this.permissions != null)
				{
					this.permissions.PermitOnly();
				}
				return this.method.Invoke(this.extension, args);
			}

			// Token: 0x040021D9 RID: 8665
			private object extension;

			// Token: 0x040021DA RID: 8666
			private MethodInfo method;

			// Token: 0x040021DB RID: 8667
			private TypeCode[] typeCodes;

			// Token: 0x040021DC RID: 8668
			private PermissionSet permissions;
		}
	}
}
