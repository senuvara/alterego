﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004FB RID: 1275
	internal sealed class PrefixQName
	{
		// Token: 0x06003188 RID: 12680 RVA: 0x00104A71 File Offset: 0x00102C71
		internal void ClearPrefix()
		{
			this.Prefix = string.Empty;
		}

		// Token: 0x06003189 RID: 12681 RVA: 0x00104A7E File Offset: 0x00102C7E
		internal void SetQName(string qname)
		{
			PrefixQName.ParseQualifiedName(qname, out this.Prefix, out this.Name);
		}

		// Token: 0x0600318A RID: 12682 RVA: 0x00104A94 File Offset: 0x00102C94
		public static void ParseQualifiedName(string qname, out string prefix, out string local)
		{
			prefix = string.Empty;
			local = string.Empty;
			int num = ValidateNames.ParseNCName(qname);
			if (num == 0)
			{
				throw XsltException.Create("'{0}' is an invalid QName.", new string[]
				{
					qname
				});
			}
			local = qname.Substring(0, num);
			if (num < qname.Length)
			{
				if (qname[num] == ':')
				{
					int startIndex;
					num = (startIndex = num + 1);
					prefix = local;
					int num2 = ValidateNames.ParseNCName(qname, num);
					num += num2;
					if (num2 == 0)
					{
						throw XsltException.Create("'{0}' is an invalid QName.", new string[]
						{
							qname
						});
					}
					local = qname.Substring(startIndex, num2);
				}
				if (num < qname.Length)
				{
					throw XsltException.Create("'{0}' is an invalid QName.", new string[]
					{
						qname
					});
				}
			}
		}

		// Token: 0x0600318B RID: 12683 RVA: 0x00104B42 File Offset: 0x00102D42
		public static bool ValidatePrefix(string prefix)
		{
			return prefix.Length != 0 && ValidateNames.ParseNCName(prefix, 0) == prefix.Length;
		}

		// Token: 0x0600318C RID: 12684 RVA: 0x00002103 File Offset: 0x00000303
		public PrefixQName()
		{
		}

		// Token: 0x040020BC RID: 8380
		public string Prefix;

		// Token: 0x040020BD RID: 8381
		public string Name;

		// Token: 0x040020BE RID: 8382
		public string Namespace;
	}
}
