﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004B4 RID: 1204
	internal class AttributeSetAction : ContainerAction
	{
		// Token: 0x17000A07 RID: 2567
		// (get) Token: 0x06002F60 RID: 12128 RVA: 0x000FDB74 File Offset: 0x000FBD74
		internal XmlQualifiedName Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x06002F61 RID: 12129 RVA: 0x000FDB7C File Offset: 0x000FBD7C
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.name, "name");
			this.CompileContent(compiler);
		}

		// Token: 0x06002F62 RID: 12130 RVA: 0x000FDBA0 File Offset: 0x000FBDA0
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Name))
			{
				this.name = compiler.CreateXPathQName(value);
			}
			else
			{
				if (!Ref.Equal(localName, compiler.Atoms.UseAttributeSets))
				{
					return false;
				}
				base.AddAction(compiler.CreateUseAttributeSetsAction());
			}
			return true;
		}

		// Token: 0x06002F63 RID: 12131 RVA: 0x000FDC0C File Offset: 0x000FBE0C
		private void CompileContent(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			if (compiler.Recurse())
			{
				for (;;)
				{
					XPathNodeType nodeType = input.NodeType;
					if (nodeType != XPathNodeType.Element)
					{
						if (nodeType - XPathNodeType.SignificantWhitespace > 3)
						{
							break;
						}
					}
					else
					{
						compiler.PushNamespaceScope();
						string namespaceURI = input.NamespaceURI;
						string localName = input.LocalName;
						if (!Ref.Equal(namespaceURI, input.Atoms.UriXsl) || !Ref.Equal(localName, input.Atoms.Attribute))
						{
							goto IL_6B;
						}
						base.AddAction(compiler.CreateAttributeAction());
						compiler.PopScope();
					}
					if (!compiler.Advance())
					{
						goto Block_5;
					}
				}
				throw XsltException.Create("The contents of '{0}' are invalid.", new string[]
				{
					"attribute-set"
				});
				IL_6B:
				throw compiler.UnexpectedKeyword();
				Block_5:
				compiler.ToParent();
			}
		}

		// Token: 0x06002F64 RID: 12132 RVA: 0x000FDCC0 File Offset: 0x000FBEC0
		internal void Merge(AttributeSetAction attributeAction)
		{
			int num = 0;
			Action action;
			while ((action = attributeAction.GetAction(num)) != null)
			{
				base.AddAction(action);
				num++;
			}
		}

		// Token: 0x06002F65 RID: 12133 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		public AttributeSetAction()
		{
		}

		// Token: 0x04001FC3 RID: 8131
		internal XmlQualifiedName name;
	}
}
