﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004F1 RID: 1265
	internal class NamespaceEvent : Event
	{
		// Token: 0x06003123 RID: 12579 RVA: 0x0010314A File Offset: 0x0010134A
		public NamespaceEvent(NavigatorInput input)
		{
			this.namespaceUri = input.Value;
			this.name = input.LocalName;
		}

		// Token: 0x06003124 RID: 12580 RVA: 0x0010316C File Offset: 0x0010136C
		public override void ReplaceNamespaceAlias(Compiler compiler)
		{
			if (this.namespaceUri.Length != 0)
			{
				NamespaceInfo namespaceInfo = compiler.FindNamespaceAlias(this.namespaceUri);
				if (namespaceInfo != null)
				{
					this.namespaceUri = namespaceInfo.nameSpace;
					if (namespaceInfo.prefix != null)
					{
						this.name = namespaceInfo.prefix;
					}
				}
			}
		}

		// Token: 0x06003125 RID: 12581 RVA: 0x001031B6 File Offset: 0x001013B6
		public override bool Output(Processor processor, ActionFrame frame)
		{
			processor.BeginEvent(XPathNodeType.Namespace, null, this.name, this.namespaceUri, false);
			processor.EndEvent(XPathNodeType.Namespace);
			return true;
		}

		// Token: 0x04002077 RID: 8311
		private string namespaceUri;

		// Token: 0x04002078 RID: 8312
		private string name;
	}
}
