﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004BE RID: 1214
	internal enum ScriptingLanguage
	{
		// Token: 0x04001FE2 RID: 8162
		JScript,
		// Token: 0x04001FE3 RID: 8163
		VisualBasic,
		// Token: 0x04001FE4 RID: 8164
		CSharp
	}
}
