﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000504 RID: 1284
	internal interface RecordOutput
	{
		// Token: 0x06003241 RID: 12865
		Processor.OutputResult RecordDone(RecordBuilder record);

		// Token: 0x06003242 RID: 12866
		void TheEnd();
	}
}
