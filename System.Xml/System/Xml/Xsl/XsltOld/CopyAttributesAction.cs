﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C4 RID: 1220
	internal sealed class CopyAttributesAction : Action
	{
		// Token: 0x06003024 RID: 12324 RVA: 0x00100CB6 File Offset: 0x000FEEB6
		internal static CopyAttributesAction GetAction()
		{
			return CopyAttributesAction.s_Action;
		}

		// Token: 0x06003025 RID: 12325 RVA: 0x00100CC0 File Offset: 0x000FEEC0
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			while (processor.CanContinue)
			{
				switch (frame.State)
				{
				case 0:
					if (!frame.Node.HasAttributes || !frame.Node.MoveToFirstAttribute())
					{
						frame.Finished();
						return;
					}
					frame.State = 2;
					break;
				case 1:
					return;
				case 2:
					break;
				case 3:
					if (CopyAttributesAction.SendTextEvent(processor, frame.Node))
					{
						frame.State = 4;
						continue;
					}
					return;
				case 4:
					if (CopyAttributesAction.SendEndEvent(processor, frame.Node))
					{
						frame.State = 5;
						continue;
					}
					return;
				case 5:
					if (frame.Node.MoveToNextAttribute())
					{
						frame.State = 2;
						continue;
					}
					frame.Node.MoveToParent();
					frame.Finished();
					return;
				default:
					return;
				}
				if (!CopyAttributesAction.SendBeginEvent(processor, frame.Node))
				{
					break;
				}
				frame.State = 3;
			}
		}

		// Token: 0x06003026 RID: 12326 RVA: 0x00100D99 File Offset: 0x000FEF99
		private static bool SendBeginEvent(Processor processor, XPathNavigator node)
		{
			return processor.BeginEvent(XPathNodeType.Attribute, node.Prefix, node.LocalName, node.NamespaceURI, false);
		}

		// Token: 0x06003027 RID: 12327 RVA: 0x00100DB5 File Offset: 0x000FEFB5
		private static bool SendTextEvent(Processor processor, XPathNavigator node)
		{
			return processor.TextEvent(node.Value);
		}

		// Token: 0x06003028 RID: 12328 RVA: 0x00100DC3 File Offset: 0x000FEFC3
		private static bool SendEndEvent(Processor processor, XPathNavigator node)
		{
			return processor.EndEvent(XPathNodeType.Attribute);
		}

		// Token: 0x06003029 RID: 12329 RVA: 0x000FE6D5 File Offset: 0x000FC8D5
		public CopyAttributesAction()
		{
		}

		// Token: 0x0600302A RID: 12330 RVA: 0x00100DCC File Offset: 0x000FEFCC
		// Note: this type is marked as 'beforefieldinit'.
		static CopyAttributesAction()
		{
		}

		// Token: 0x04002010 RID: 8208
		private const int BeginEvent = 2;

		// Token: 0x04002011 RID: 8209
		private const int TextEvent = 3;

		// Token: 0x04002012 RID: 8210
		private const int EndEvent = 4;

		// Token: 0x04002013 RID: 8211
		private const int Advance = 5;

		// Token: 0x04002014 RID: 8212
		private static CopyAttributesAction s_Action = new CopyAttributesAction();
	}
}
