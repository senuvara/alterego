﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004EB RID: 1259
	internal class IfAction : ContainerAction
	{
		// Token: 0x060030F5 RID: 12533 RVA: 0x00102B33 File Offset: 0x00100D33
		internal IfAction(IfAction.ConditionType type)
		{
			this.type = type;
		}

		// Token: 0x060030F6 RID: 12534 RVA: 0x00102B49 File Offset: 0x00100D49
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			if (this.type != IfAction.ConditionType.ConditionOtherwise)
			{
				base.CheckRequiredAttribute(compiler, this.testKey != -1, "test");
			}
			if (compiler.Recurse())
			{
				base.CompileTemplate(compiler);
				compiler.ToParent();
			}
		}

		// Token: 0x060030F7 RID: 12535 RVA: 0x00102B8C File Offset: 0x00100D8C
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (!Ref.Equal(localName, compiler.Atoms.Test))
			{
				return false;
			}
			if (this.type == IfAction.ConditionType.ConditionOtherwise)
			{
				return false;
			}
			this.testKey = compiler.AddBooleanQuery(value);
			return true;
		}

		// Token: 0x060030F8 RID: 12536 RVA: 0x00102BE0 File Offset: 0x00100DE0
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 1)
				{
					return;
				}
				if (this.type == IfAction.ConditionType.ConditionWhen || this.type == IfAction.ConditionType.ConditionOtherwise)
				{
					frame.Exit();
				}
				frame.Finished();
				return;
			}
			else
			{
				if ((this.type == IfAction.ConditionType.ConditionIf || this.type == IfAction.ConditionType.ConditionWhen) && !processor.EvaluateBoolean(frame, this.testKey))
				{
					frame.Finished();
					return;
				}
				processor.PushActionFrame(frame);
				frame.State = 1;
				return;
			}
		}

		// Token: 0x04002063 RID: 8291
		private IfAction.ConditionType type;

		// Token: 0x04002064 RID: 8292
		private int testKey = -1;

		// Token: 0x020004EC RID: 1260
		internal enum ConditionType
		{
			// Token: 0x04002066 RID: 8294
			ConditionIf,
			// Token: 0x04002067 RID: 8295
			ConditionWhen,
			// Token: 0x04002068 RID: 8296
			ConditionOtherwise
		}
	}
}
