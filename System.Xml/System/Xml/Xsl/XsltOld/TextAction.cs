﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000514 RID: 1300
	internal class TextAction : CompiledAction
	{
		// Token: 0x060032C7 RID: 12999 RVA: 0x00109F18 File Offset: 0x00108118
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			this.CompileContent(compiler);
		}

		// Token: 0x060032C8 RID: 13000 RVA: 0x00109F28 File Offset: 0x00108128
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.DisableOutputEscaping))
			{
				this.disableOutputEscaping = compiler.GetYesNo(value);
				return true;
			}
			return false;
		}

		// Token: 0x060032C9 RID: 13001 RVA: 0x00109F70 File Offset: 0x00108170
		private void CompileContent(Compiler compiler)
		{
			if (compiler.Recurse())
			{
				NavigatorInput input = compiler.Input;
				this.text = string.Empty;
				for (;;)
				{
					XPathNodeType nodeType = input.NodeType;
					if (nodeType - XPathNodeType.Text > 2)
					{
						if (nodeType - XPathNodeType.ProcessingInstruction > 1)
						{
							break;
						}
					}
					else
					{
						this.text += input.Value;
					}
					if (!compiler.Advance())
					{
						goto Block_4;
					}
				}
				throw compiler.UnexpectedKeyword();
				Block_4:
				compiler.ToParent();
			}
		}

		// Token: 0x060032CA RID: 13002 RVA: 0x00109FDC File Offset: 0x001081DC
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			if (frame.State == 0 && processor.TextEvent(this.text, this.disableOutputEscaping))
			{
				frame.Finished();
			}
		}

		// Token: 0x060032CB RID: 13003 RVA: 0x000FD562 File Offset: 0x000FB762
		public TextAction()
		{
		}

		// Token: 0x040021AE RID: 8622
		private bool disableOutputEscaping;

		// Token: 0x040021AF RID: 8623
		private string text;
	}
}
