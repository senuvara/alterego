﻿using System;
using System.Text;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200050B RID: 1291
	internal class StringOutput : SequentialOutput
	{
		// Token: 0x17000A97 RID: 2711
		// (get) Token: 0x0600328C RID: 12940 RVA: 0x0010906B File Offset: 0x0010726B
		internal string Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x0600328D RID: 12941 RVA: 0x00109073 File Offset: 0x00107273
		internal StringOutput(Processor processor) : base(processor)
		{
			this.builder = new StringBuilder();
		}

		// Token: 0x0600328E RID: 12942 RVA: 0x00109087 File Offset: 0x00107287
		internal override void Write(char outputChar)
		{
			this.builder.Append(outputChar);
		}

		// Token: 0x0600328F RID: 12943 RVA: 0x00109096 File Offset: 0x00107296
		internal override void Write(string outputText)
		{
			this.builder.Append(outputText);
		}

		// Token: 0x06003290 RID: 12944 RVA: 0x001090A5 File Offset: 0x001072A5
		internal override void Close()
		{
			this.result = this.builder.ToString();
		}

		// Token: 0x04002191 RID: 8593
		private StringBuilder builder;

		// Token: 0x04002192 RID: 8594
		private string result;
	}
}
