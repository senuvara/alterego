﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200051D RID: 1309
	internal class VariableAction : ContainerAction, IXsltContextVariable
	{
		// Token: 0x17000AA9 RID: 2729
		// (get) Token: 0x060032EA RID: 13034 RVA: 0x0010A465 File Offset: 0x00108665
		internal int Stylesheetid
		{
			get
			{
				return this.stylesheetid;
			}
		}

		// Token: 0x17000AAA RID: 2730
		// (get) Token: 0x060032EB RID: 13035 RVA: 0x0010A46D File Offset: 0x0010866D
		internal XmlQualifiedName Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000AAB RID: 2731
		// (get) Token: 0x060032EC RID: 13036 RVA: 0x0010A475 File Offset: 0x00108675
		internal string NameStr
		{
			get
			{
				return this.nameStr;
			}
		}

		// Token: 0x17000AAC RID: 2732
		// (get) Token: 0x060032ED RID: 13037 RVA: 0x0010A47D File Offset: 0x0010867D
		internal VariableType VarType
		{
			get
			{
				return this.varType;
			}
		}

		// Token: 0x17000AAD RID: 2733
		// (get) Token: 0x060032EE RID: 13038 RVA: 0x0010A485 File Offset: 0x00108685
		internal int VarKey
		{
			get
			{
				return this.varKey;
			}
		}

		// Token: 0x17000AAE RID: 2734
		// (get) Token: 0x060032EF RID: 13039 RVA: 0x0010A48D File Offset: 0x0010868D
		internal bool IsGlobal
		{
			get
			{
				return this.varType == VariableType.GlobalVariable || this.varType == VariableType.GlobalParameter;
			}
		}

		// Token: 0x060032F0 RID: 13040 RVA: 0x0010A4A2 File Offset: 0x001086A2
		internal VariableAction(VariableType type)
		{
			this.varType = type;
		}

		// Token: 0x060032F1 RID: 13041 RVA: 0x0010A4B8 File Offset: 0x001086B8
		internal override void Compile(Compiler compiler)
		{
			this.stylesheetid = compiler.Stylesheetid;
			this.baseUri = compiler.Input.BaseURI;
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.name, "name");
			if (compiler.Recurse())
			{
				base.CompileTemplate(compiler);
				compiler.ToParent();
				if (this.selectKey != -1 && this.containedActions != null)
				{
					throw XsltException.Create("The variable or parameter '{0}' cannot have both a 'select' attribute and non-empty content.", new string[]
					{
						this.nameStr
					});
				}
			}
			if (this.containedActions != null)
			{
				this.baseUri = this.baseUri + "#" + compiler.GetUnicRtfId();
			}
			else
			{
				this.baseUri = null;
			}
			this.varKey = compiler.InsertVariable(this);
		}

		// Token: 0x060032F2 RID: 13042 RVA: 0x0010A578 File Offset: 0x00108778
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Name))
			{
				this.nameStr = value;
				this.name = compiler.CreateXPathQName(this.nameStr);
			}
			else
			{
				if (!Ref.Equal(localName, compiler.Atoms.Select))
				{
					return false;
				}
				this.selectKey = compiler.AddQuery(value);
			}
			return true;
		}

		// Token: 0x060032F3 RID: 13043 RVA: 0x0010A5F0 File Offset: 0x001087F0
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			object obj = null;
			switch (frame.State)
			{
			case 0:
				if (this.IsGlobal)
				{
					if (frame.GetVariable(this.varKey) != null)
					{
						frame.Finished();
						return;
					}
					frame.SetVariable(this.varKey, VariableAction.BeingComputedMark);
				}
				if (this.varType == VariableType.GlobalParameter)
				{
					obj = processor.GetGlobalParameter(this.name);
				}
				else if (this.varType == VariableType.LocalParameter)
				{
					obj = processor.GetParameter(this.name);
				}
				if (obj == null)
				{
					if (this.selectKey != -1)
					{
						obj = processor.RunQuery(frame, this.selectKey);
					}
					else
					{
						if (this.containedActions != null)
						{
							NavigatorOutput output = new NavigatorOutput(this.baseUri);
							processor.PushOutput(output);
							processor.PushActionFrame(frame);
							frame.State = 1;
							return;
						}
						obj = string.Empty;
					}
				}
				break;
			case 1:
				obj = ((NavigatorOutput)processor.PopOutput()).Navigator;
				break;
			case 2:
				break;
			default:
				return;
			}
			frame.SetVariable(this.varKey, obj);
			frame.Finished();
		}

		// Token: 0x17000AAF RID: 2735
		// (get) Token: 0x060032F4 RID: 13044 RVA: 0x00003A7F File Offset: 0x00001C7F
		XPathResultType IXsltContextVariable.VariableType
		{
			get
			{
				return XPathResultType.Any;
			}
		}

		// Token: 0x060032F5 RID: 13045 RVA: 0x0010A6E7 File Offset: 0x001088E7
		object IXsltContextVariable.Evaluate(XsltContext xsltContext)
		{
			return ((XsltCompileContext)xsltContext).EvaluateVariable(this);
		}

		// Token: 0x17000AB0 RID: 2736
		// (get) Token: 0x060032F6 RID: 13046 RVA: 0x0010A6F5 File Offset: 0x001088F5
		bool IXsltContextVariable.IsLocal
		{
			get
			{
				return this.varType == VariableType.LocalVariable || this.varType == VariableType.LocalParameter;
			}
		}

		// Token: 0x17000AB1 RID: 2737
		// (get) Token: 0x060032F7 RID: 13047 RVA: 0x0010A70B File Offset: 0x0010890B
		bool IXsltContextVariable.IsParam
		{
			get
			{
				return this.varType == VariableType.LocalParameter || this.varType == VariableType.GlobalParameter;
			}
		}

		// Token: 0x060032F8 RID: 13048 RVA: 0x0010A721 File Offset: 0x00108921
		// Note: this type is marked as 'beforefieldinit'.
		static VariableAction()
		{
		}

		// Token: 0x040021C4 RID: 8644
		public static object BeingComputedMark = new object();

		// Token: 0x040021C5 RID: 8645
		private const int ValueCalculated = 2;

		// Token: 0x040021C6 RID: 8646
		protected XmlQualifiedName name;

		// Token: 0x040021C7 RID: 8647
		protected string nameStr;

		// Token: 0x040021C8 RID: 8648
		protected string baseUri;

		// Token: 0x040021C9 RID: 8649
		protected int selectKey = -1;

		// Token: 0x040021CA RID: 8650
		protected int stylesheetid;

		// Token: 0x040021CB RID: 8651
		protected VariableType varType;

		// Token: 0x040021CC RID: 8652
		private int varKey;
	}
}
