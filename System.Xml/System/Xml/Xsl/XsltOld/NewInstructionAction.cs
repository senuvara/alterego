﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004F4 RID: 1268
	internal class NewInstructionAction : ContainerAction
	{
		// Token: 0x06003145 RID: 12613 RVA: 0x001035BC File Offset: 0x001017BC
		internal override void Compile(Compiler compiler)
		{
			XPathNavigator xpathNavigator = compiler.Input.Navigator.Clone();
			this.name = xpathNavigator.Name;
			xpathNavigator.MoveToParent();
			this.parent = xpathNavigator.Name;
			if (compiler.Recurse())
			{
				this.CompileSelectiveTemplate(compiler);
				compiler.ToParent();
			}
		}

		// Token: 0x06003146 RID: 12614 RVA: 0x00103610 File Offset: 0x00101810
		internal void CompileSelectiveTemplate(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			do
			{
				if (Ref.Equal(input.NamespaceURI, input.Atoms.UriXsl) && Ref.Equal(input.LocalName, input.Atoms.Fallback))
				{
					this.fallback = true;
					if (compiler.Recurse())
					{
						base.CompileTemplate(compiler);
						compiler.ToParent();
					}
				}
			}
			while (compiler.Advance());
		}

		// Token: 0x06003147 RID: 12615 RVA: 0x0010367C File Offset: 0x0010187C
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 1)
				{
					return;
				}
			}
			else
			{
				if (!this.fallback)
				{
					throw XsltException.Create("'{0}' is not a recognized extension element.", new string[]
					{
						this.name
					});
				}
				if (this.containedActions != null && this.containedActions.Count > 0)
				{
					processor.PushActionFrame(frame);
					frame.State = 1;
					return;
				}
			}
			frame.Finished();
		}

		// Token: 0x06003148 RID: 12616 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		public NewInstructionAction()
		{
		}

		// Token: 0x04002082 RID: 8322
		private string name;

		// Token: 0x04002083 RID: 8323
		private string parent;

		// Token: 0x04002084 RID: 8324
		private bool fallback;
	}
}
