﻿using System;
using System.Collections;
using System.Globalization;
using System.Xml.XPath;
using System.Xml.Xsl.Runtime;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C2 RID: 1218
	internal class ContainerAction : CompiledAction
	{
		// Token: 0x06003003 RID: 12291 RVA: 0x0000A6CF File Offset: 0x000088CF
		internal override void Compile(Compiler compiler)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003004 RID: 12292 RVA: 0x000FF7DC File Offset: 0x000FD9DC
		internal void CompileStylesheetAttributes(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			string localName = input.LocalName;
			string text = null;
			string text2 = null;
			if (input.MoveToFirstAttribute())
			{
				for (;;)
				{
					string namespaceURI = input.NamespaceURI;
					string localName2 = input.LocalName;
					if (namespaceURI.Length == 0)
					{
						if (Ref.Equal(localName2, input.Atoms.Version))
						{
							text2 = input.Value;
							if (1.0 <= XmlConvert.ToXPathDouble(text2))
							{
								compiler.ForwardCompatibility = (text2 != "1.0");
							}
							else if (!compiler.ForwardCompatibility)
							{
								break;
							}
						}
						else if (Ref.Equal(localName2, input.Atoms.ExtensionElementPrefixes))
						{
							compiler.InsertExtensionNamespace(input.Value);
						}
						else if (Ref.Equal(localName2, input.Atoms.ExcludeResultPrefixes))
						{
							compiler.InsertExcludedNamespace(input.Value);
						}
						else if (!Ref.Equal(localName2, input.Atoms.Id))
						{
							text = localName2;
						}
					}
					if (!input.MoveToNextAttribute())
					{
						goto Block_8;
					}
				}
				throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
				{
					"version",
					text2
				});
				Block_8:
				input.ToParent();
			}
			if (text2 == null)
			{
				throw XsltException.Create("Missing mandatory attribute '{0}'.", new string[]
				{
					"version"
				});
			}
			if (text != null && !compiler.ForwardCompatibility)
			{
				throw XsltException.Create("'{0}' is an invalid attribute for the '{1}' element.", new string[]
				{
					text,
					localName
				});
			}
		}

		// Token: 0x06003005 RID: 12293 RVA: 0x000FF934 File Offset: 0x000FDB34
		internal void CompileSingleTemplate(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			string text = null;
			if (input.MoveToFirstAttribute())
			{
				do
				{
					string namespaceURI = input.NamespaceURI;
					string localName = input.LocalName;
					if (Ref.Equal(namespaceURI, input.Atoms.UriXsl) && Ref.Equal(localName, input.Atoms.Version))
					{
						text = input.Value;
					}
				}
				while (input.MoveToNextAttribute());
				input.ToParent();
			}
			if (text != null)
			{
				compiler.AddTemplate(compiler.CreateSingleTemplateAction());
				return;
			}
			if (Ref.Equal(input.LocalName, input.Atoms.Stylesheet) && input.NamespaceURI == "http://www.w3.org/TR/WD-xsl")
			{
				throw XsltException.Create("The 'http://www.w3.org/TR/WD-xsl' namespace is no longer supported.", Array.Empty<string>());
			}
			throw XsltException.Create("Stylesheet must start either with an 'xsl:stylesheet' or an 'xsl:transform' element, or with a literal result element that has an 'xsl:version' attribute, where prefix 'xsl' denotes the 'http://www.w3.org/1999/XSL/Transform' namespace.", Array.Empty<string>());
		}

		// Token: 0x06003006 RID: 12294 RVA: 0x000FF9F4 File Offset: 0x000FDBF4
		protected void CompileDocument(Compiler compiler, bool inInclude)
		{
			NavigatorInput input = compiler.Input;
			while (input.NodeType != XPathNodeType.Element)
			{
				if (!compiler.Advance())
				{
					throw XsltException.Create("Stylesheet must start either with an 'xsl:stylesheet' or an 'xsl:transform' element, or with a literal result element that has an 'xsl:version' attribute, where prefix 'xsl' denotes the 'http://www.w3.org/1999/XSL/Transform' namespace.", Array.Empty<string>());
				}
			}
			if (Ref.Equal(input.NamespaceURI, input.Atoms.UriXsl))
			{
				if (!Ref.Equal(input.LocalName, input.Atoms.Stylesheet) && !Ref.Equal(input.LocalName, input.Atoms.Transform))
				{
					throw XsltException.Create("Stylesheet must start either with an 'xsl:stylesheet' or an 'xsl:transform' element, or with a literal result element that has an 'xsl:version' attribute, where prefix 'xsl' denotes the 'http://www.w3.org/1999/XSL/Transform' namespace.", Array.Empty<string>());
				}
				compiler.PushNamespaceScope();
				this.CompileStylesheetAttributes(compiler);
				this.CompileTopLevelElements(compiler);
				if (!inInclude)
				{
					this.CompileImports(compiler);
				}
			}
			else
			{
				compiler.PushLiteralScope();
				this.CompileSingleTemplate(compiler);
			}
			compiler.PopScope();
		}

		// Token: 0x06003007 RID: 12295 RVA: 0x000FFAB8 File Offset: 0x000FDCB8
		internal Stylesheet CompileImport(Compiler compiler, Uri uri, int id)
		{
			NavigatorInput navigatorInput = compiler.ResolveDocument(uri);
			compiler.PushInputDocument(navigatorInput);
			try
			{
				compiler.PushStylesheet(new Stylesheet());
				compiler.Stylesheetid = id;
				this.CompileDocument(compiler, false);
			}
			catch (XsltCompileException)
			{
				throw;
			}
			catch (Exception inner)
			{
				throw new XsltCompileException(inner, navigatorInput.BaseURI, navigatorInput.LineNumber, navigatorInput.LinePosition);
			}
			finally
			{
				compiler.PopInputDocument();
			}
			return compiler.PopStylesheet();
		}

		// Token: 0x06003008 RID: 12296 RVA: 0x000FFB40 File Offset: 0x000FDD40
		private void CompileImports(Compiler compiler)
		{
			ArrayList imports = compiler.CompiledStylesheet.Imports;
			int stylesheetid = compiler.Stylesheetid;
			int num = imports.Count - 1;
			while (0 <= num)
			{
				Uri uri = imports[num] as Uri;
				ArrayList arrayList = imports;
				int index = num;
				Uri uri2 = uri;
				int id = this.maxid + 1;
				this.maxid = id;
				arrayList[index] = this.CompileImport(compiler, uri2, id);
				num--;
			}
			compiler.Stylesheetid = stylesheetid;
		}

		// Token: 0x06003009 RID: 12297 RVA: 0x000FFBAC File Offset: 0x000FDDAC
		private void CompileInclude(Compiler compiler)
		{
			Uri uri = compiler.ResolveUri(compiler.GetSingleAttribute(compiler.Input.Atoms.Href));
			string text = uri.ToString();
			if (compiler.IsCircularReference(text))
			{
				throw XsltException.Create("Stylesheet '{0}' cannot directly or indirectly include or import itself.", new string[]
				{
					text
				});
			}
			NavigatorInput navigatorInput = compiler.ResolveDocument(uri);
			compiler.PushInputDocument(navigatorInput);
			try
			{
				this.CompileDocument(compiler, true);
			}
			catch (XsltCompileException)
			{
				throw;
			}
			catch (Exception inner)
			{
				throw new XsltCompileException(inner, navigatorInput.BaseURI, navigatorInput.LineNumber, navigatorInput.LinePosition);
			}
			finally
			{
				compiler.PopInputDocument();
			}
			base.CheckEmpty(compiler);
		}

		// Token: 0x0600300A RID: 12298 RVA: 0x000FFC68 File Offset: 0x000FDE68
		internal void CompileNamespaceAlias(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			string localName = input.LocalName;
			string text = null;
			string text2 = null;
			string text3 = null;
			string prefix = null;
			if (input.MoveToFirstAttribute())
			{
				string localName2;
				for (;;)
				{
					string namespaceURI = input.NamespaceURI;
					localName2 = input.LocalName;
					if (namespaceURI.Length == 0)
					{
						if (Ref.Equal(localName2, input.Atoms.StylesheetPrefix))
						{
							text3 = input.Value;
							text = compiler.GetNsAlias(ref text3);
						}
						else if (Ref.Equal(localName2, input.Atoms.ResultPrefix))
						{
							prefix = input.Value;
							text2 = compiler.GetNsAlias(ref prefix);
						}
						else if (!compiler.ForwardCompatibility)
						{
							break;
						}
					}
					if (!input.MoveToNextAttribute())
					{
						goto Block_5;
					}
				}
				throw XsltException.Create("'{0}' is an invalid attribute for the '{1}' element.", new string[]
				{
					localName2,
					localName
				});
				Block_5:
				input.ToParent();
			}
			base.CheckRequiredAttribute(compiler, text, "stylesheet-prefix");
			base.CheckRequiredAttribute(compiler, text2, "result-prefix");
			base.CheckEmpty(compiler);
			compiler.AddNamespaceAlias(text, new NamespaceInfo(prefix, text2, compiler.Stylesheetid));
		}

		// Token: 0x0600300B RID: 12299 RVA: 0x000FFD68 File Offset: 0x000FDF68
		internal void CompileKey(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			string localName = input.LocalName;
			int num = -1;
			int num2 = -1;
			XmlQualifiedName xmlQualifiedName = null;
			if (input.MoveToFirstAttribute())
			{
				string localName2;
				for (;;)
				{
					string namespaceURI = input.NamespaceURI;
					localName2 = input.LocalName;
					string value = input.Value;
					if (namespaceURI.Length == 0)
					{
						if (Ref.Equal(localName2, input.Atoms.Name))
						{
							xmlQualifiedName = compiler.CreateXPathQName(value);
						}
						else if (Ref.Equal(localName2, input.Atoms.Match))
						{
							num = compiler.AddQuery(value, false, false, true);
						}
						else if (Ref.Equal(localName2, input.Atoms.Use))
						{
							num2 = compiler.AddQuery(value, false, false, false);
						}
						else if (!compiler.ForwardCompatibility)
						{
							break;
						}
					}
					if (!input.MoveToNextAttribute())
					{
						goto Block_6;
					}
				}
				throw XsltException.Create("'{0}' is an invalid attribute for the '{1}' element.", new string[]
				{
					localName2,
					localName
				});
				Block_6:
				input.ToParent();
			}
			base.CheckRequiredAttribute(compiler, num != -1, "match");
			base.CheckRequiredAttribute(compiler, num2 != -1, "use");
			base.CheckRequiredAttribute(compiler, xmlQualifiedName != null, "name");
			compiler.InsertKey(xmlQualifiedName, num, num2);
		}

		// Token: 0x0600300C RID: 12300 RVA: 0x000FFE94 File Offset: 0x000FE094
		protected void CompileDecimalFormat(Compiler compiler)
		{
			NumberFormatInfo numberFormatInfo = new NumberFormatInfo();
			DecimalFormat decimalFormat = new DecimalFormat(numberFormatInfo, '#', '0', ';');
			XmlQualifiedName xmlQualifiedName = null;
			NavigatorInput input = compiler.Input;
			if (input.MoveToFirstAttribute())
			{
				do
				{
					if (input.Prefix.Length == 0)
					{
						string localName = input.LocalName;
						string value = input.Value;
						if (Ref.Equal(localName, input.Atoms.Name))
						{
							xmlQualifiedName = compiler.CreateXPathQName(value);
						}
						else if (Ref.Equal(localName, input.Atoms.DecimalSeparator))
						{
							numberFormatInfo.NumberDecimalSeparator = value;
						}
						else if (Ref.Equal(localName, input.Atoms.GroupingSeparator))
						{
							numberFormatInfo.NumberGroupSeparator = value;
						}
						else if (Ref.Equal(localName, input.Atoms.Infinity))
						{
							numberFormatInfo.PositiveInfinitySymbol = value;
						}
						else if (Ref.Equal(localName, input.Atoms.MinusSign))
						{
							numberFormatInfo.NegativeSign = value;
						}
						else if (Ref.Equal(localName, input.Atoms.NaN))
						{
							numberFormatInfo.NaNSymbol = value;
						}
						else if (Ref.Equal(localName, input.Atoms.Percent))
						{
							numberFormatInfo.PercentSymbol = value;
						}
						else if (Ref.Equal(localName, input.Atoms.PerMille))
						{
							numberFormatInfo.PerMilleSymbol = value;
						}
						else if (Ref.Equal(localName, input.Atoms.Digit))
						{
							if (this.CheckAttribute(value.Length == 1, compiler))
							{
								decimalFormat.digit = value[0];
							}
						}
						else if (Ref.Equal(localName, input.Atoms.ZeroDigit))
						{
							if (this.CheckAttribute(value.Length == 1, compiler))
							{
								decimalFormat.zeroDigit = value[0];
							}
						}
						else if (Ref.Equal(localName, input.Atoms.PatternSeparator) && this.CheckAttribute(value.Length == 1, compiler))
						{
							decimalFormat.patternSeparator = value[0];
						}
					}
				}
				while (input.MoveToNextAttribute());
				input.ToParent();
			}
			numberFormatInfo.NegativeInfinitySymbol = numberFormatInfo.NegativeSign + numberFormatInfo.PositiveInfinitySymbol;
			if (xmlQualifiedName == null)
			{
				xmlQualifiedName = new XmlQualifiedName();
			}
			compiler.AddDecimalFormat(xmlQualifiedName, decimalFormat);
			base.CheckEmpty(compiler);
		}

		// Token: 0x0600300D RID: 12301 RVA: 0x001000DB File Offset: 0x000FE2DB
		internal bool CheckAttribute(bool valid, Compiler compiler)
		{
			if (valid)
			{
				return true;
			}
			if (!compiler.ForwardCompatibility)
			{
				throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
				{
					compiler.Input.LocalName,
					compiler.Input.Value
				});
			}
			return false;
		}

		// Token: 0x0600300E RID: 12302 RVA: 0x00100118 File Offset: 0x000FE318
		protected void CompileSpace(Compiler compiler, bool preserve)
		{
			string[] array = XmlConvert.SplitString(compiler.GetSingleAttribute(compiler.Input.Atoms.Elements));
			for (int i = 0; i < array.Length; i++)
			{
				double priority = this.NameTest(array[i]);
				compiler.CompiledStylesheet.AddSpace(compiler, array[i], priority, preserve);
			}
			base.CheckEmpty(compiler);
		}

		// Token: 0x0600300F RID: 12303 RVA: 0x00100174 File Offset: 0x000FE374
		private double NameTest(string name)
		{
			if (name == "*")
			{
				return -0.5;
			}
			int num = name.Length - 2;
			if (0 > num || name[num] != ':' || name[num + 1] != '*')
			{
				string text;
				string text2;
				PrefixQName.ParseQualifiedName(name, out text, out text2);
				return 0.0;
			}
			if (!PrefixQName.ValidatePrefix(name.Substring(0, num)))
			{
				throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
				{
					"elements",
					name
				});
			}
			return -0.25;
		}

		// Token: 0x06003010 RID: 12304 RVA: 0x00100208 File Offset: 0x000FE408
		protected void CompileTopLevelElements(Compiler compiler)
		{
			if (!compiler.Recurse())
			{
				return;
			}
			NavigatorInput input = compiler.Input;
			bool flag = false;
			string text;
			for (;;)
			{
				XPathNodeType nodeType = input.NodeType;
				if (nodeType != XPathNodeType.Element)
				{
					if (nodeType - XPathNodeType.SignificantWhitespace > 3)
					{
						break;
					}
				}
				else
				{
					string localName = input.LocalName;
					string namespaceURI = input.NamespaceURI;
					if (Ref.Equal(namespaceURI, input.Atoms.UriXsl))
					{
						if (Ref.Equal(localName, input.Atoms.Import))
						{
							if (flag)
							{
								goto Block_6;
							}
							Uri uri = compiler.ResolveUri(compiler.GetSingleAttribute(compiler.Input.Atoms.Href));
							text = uri.ToString();
							if (compiler.IsCircularReference(text))
							{
								goto Block_7;
							}
							compiler.CompiledStylesheet.Imports.Add(uri);
							base.CheckEmpty(compiler);
						}
						else if (Ref.Equal(localName, input.Atoms.Include))
						{
							flag = true;
							this.CompileInclude(compiler);
						}
						else
						{
							flag = true;
							compiler.PushNamespaceScope();
							if (Ref.Equal(localName, input.Atoms.StripSpace))
							{
								this.CompileSpace(compiler, false);
							}
							else if (Ref.Equal(localName, input.Atoms.PreserveSpace))
							{
								this.CompileSpace(compiler, true);
							}
							else if (Ref.Equal(localName, input.Atoms.Output))
							{
								this.CompileOutput(compiler);
							}
							else if (Ref.Equal(localName, input.Atoms.Key))
							{
								this.CompileKey(compiler);
							}
							else if (Ref.Equal(localName, input.Atoms.DecimalFormat))
							{
								this.CompileDecimalFormat(compiler);
							}
							else if (Ref.Equal(localName, input.Atoms.NamespaceAlias))
							{
								this.CompileNamespaceAlias(compiler);
							}
							else if (Ref.Equal(localName, input.Atoms.AttributeSet))
							{
								compiler.AddAttributeSet(compiler.CreateAttributeSetAction());
							}
							else if (Ref.Equal(localName, input.Atoms.Variable))
							{
								VariableAction variableAction = compiler.CreateVariableAction(VariableType.GlobalVariable);
								if (variableAction != null)
								{
									this.AddAction(variableAction);
								}
							}
							else if (Ref.Equal(localName, input.Atoms.Param))
							{
								VariableAction variableAction2 = compiler.CreateVariableAction(VariableType.GlobalParameter);
								if (variableAction2 != null)
								{
									this.AddAction(variableAction2);
								}
							}
							else if (Ref.Equal(localName, input.Atoms.Template))
							{
								compiler.AddTemplate(compiler.CreateTemplateAction());
							}
							else if (!compiler.ForwardCompatibility)
							{
								goto Block_21;
							}
							compiler.PopScope();
						}
					}
					else if (namespaceURI.Length == 0)
					{
						goto Block_22;
					}
				}
				if (!compiler.Advance())
				{
					goto Block_23;
				}
			}
			throw XsltException.Create("The contents of '{0}' are invalid.", new string[]
			{
				"stylesheet"
			});
			Block_6:
			throw XsltException.Create("'xsl:import' instructions must precede all other element children of an 'xsl:stylesheet' element.", Array.Empty<string>());
			Block_7:
			throw XsltException.Create("Stylesheet '{0}' cannot directly or indirectly include or import itself.", new string[]
			{
				text
			});
			Block_21:
			throw compiler.UnexpectedKeyword();
			Block_22:
			throw XsltException.Create("Top-level element '{0}' may not have a null namespace URI.", new string[]
			{
				input.Name
			});
			Block_23:
			compiler.ToParent();
		}

		// Token: 0x06003011 RID: 12305 RVA: 0x001004D5 File Offset: 0x000FE6D5
		protected void CompileTemplate(Compiler compiler)
		{
			do
			{
				this.CompileOnceTemplate(compiler);
			}
			while (compiler.Advance());
		}

		// Token: 0x06003012 RID: 12306 RVA: 0x001004E8 File Offset: 0x000FE6E8
		protected void CompileOnceTemplate(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			if (input.NodeType != XPathNodeType.Element)
			{
				this.CompileLiteral(compiler);
				return;
			}
			string namespaceURI = input.NamespaceURI;
			if (Ref.Equal(namespaceURI, input.Atoms.UriXsl))
			{
				compiler.PushNamespaceScope();
				this.CompileInstruction(compiler);
				compiler.PopScope();
				return;
			}
			compiler.PushLiteralScope();
			compiler.InsertExtensionNamespace();
			if (compiler.IsExtensionNamespace(namespaceURI))
			{
				this.AddAction(compiler.CreateNewInstructionAction());
			}
			else
			{
				this.CompileLiteral(compiler);
			}
			compiler.PopScope();
		}

		// Token: 0x06003013 RID: 12307 RVA: 0x0010056C File Offset: 0x000FE76C
		private void CompileInstruction(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			string localName = input.LocalName;
			CompiledAction action;
			if (Ref.Equal(localName, input.Atoms.ApplyImports))
			{
				action = compiler.CreateApplyImportsAction();
			}
			else if (Ref.Equal(localName, input.Atoms.ApplyTemplates))
			{
				action = compiler.CreateApplyTemplatesAction();
			}
			else if (Ref.Equal(localName, input.Atoms.Attribute))
			{
				action = compiler.CreateAttributeAction();
			}
			else if (Ref.Equal(localName, input.Atoms.CallTemplate))
			{
				action = compiler.CreateCallTemplateAction();
			}
			else if (Ref.Equal(localName, input.Atoms.Choose))
			{
				action = compiler.CreateChooseAction();
			}
			else if (Ref.Equal(localName, input.Atoms.Comment))
			{
				action = compiler.CreateCommentAction();
			}
			else if (Ref.Equal(localName, input.Atoms.Copy))
			{
				action = compiler.CreateCopyAction();
			}
			else if (Ref.Equal(localName, input.Atoms.CopyOf))
			{
				action = compiler.CreateCopyOfAction();
			}
			else if (Ref.Equal(localName, input.Atoms.Element))
			{
				action = compiler.CreateElementAction();
			}
			else
			{
				if (Ref.Equal(localName, input.Atoms.Fallback))
				{
					return;
				}
				if (Ref.Equal(localName, input.Atoms.ForEach))
				{
					action = compiler.CreateForEachAction();
				}
				else if (Ref.Equal(localName, input.Atoms.If))
				{
					action = compiler.CreateIfAction(IfAction.ConditionType.ConditionIf);
				}
				else if (Ref.Equal(localName, input.Atoms.Message))
				{
					action = compiler.CreateMessageAction();
				}
				else if (Ref.Equal(localName, input.Atoms.Number))
				{
					action = compiler.CreateNumberAction();
				}
				else if (Ref.Equal(localName, input.Atoms.ProcessingInstruction))
				{
					action = compiler.CreateProcessingInstructionAction();
				}
				else if (Ref.Equal(localName, input.Atoms.Text))
				{
					action = compiler.CreateTextAction();
				}
				else if (Ref.Equal(localName, input.Atoms.ValueOf))
				{
					action = compiler.CreateValueOfAction();
				}
				else if (Ref.Equal(localName, input.Atoms.Variable))
				{
					action = compiler.CreateVariableAction(VariableType.LocalVariable);
				}
				else
				{
					if (!compiler.ForwardCompatibility)
					{
						throw compiler.UnexpectedKeyword();
					}
					action = compiler.CreateNewInstructionAction();
				}
			}
			this.AddAction(action);
		}

		// Token: 0x06003014 RID: 12308 RVA: 0x001007C4 File Offset: 0x000FE9C4
		private void CompileLiteral(Compiler compiler)
		{
			switch (compiler.Input.NodeType)
			{
			case XPathNodeType.Element:
				this.AddEvent(compiler.CreateBeginEvent());
				this.CompileLiteralAttributesAndNamespaces(compiler);
				if (compiler.Recurse())
				{
					this.CompileTemplate(compiler);
					compiler.ToParent();
				}
				this.AddEvent(new EndEvent(XPathNodeType.Element));
				return;
			case XPathNodeType.Attribute:
			case XPathNodeType.Namespace:
			case XPathNodeType.Whitespace:
			case XPathNodeType.ProcessingInstruction:
			case XPathNodeType.Comment:
				break;
			case XPathNodeType.Text:
			case XPathNodeType.SignificantWhitespace:
				this.AddEvent(compiler.CreateTextEvent());
				break;
			default:
				return;
			}
		}

		// Token: 0x06003015 RID: 12309 RVA: 0x00100848 File Offset: 0x000FEA48
		private void CompileLiteralAttributesAndNamespaces(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			if (input.Navigator.MoveToAttribute("use-attribute-sets", input.Atoms.UriXsl))
			{
				this.AddAction(compiler.CreateUseAttributeSetsAction());
				input.Navigator.MoveToParent();
			}
			compiler.InsertExcludedNamespace();
			if (input.MoveToFirstNamespace())
			{
				do
				{
					string value = input.Value;
					if (!(value == "http://www.w3.org/1999/XSL/Transform") && !compiler.IsExcludedNamespace(value) && !compiler.IsExtensionNamespace(value) && !compiler.IsNamespaceAlias(value))
					{
						this.AddEvent(new NamespaceEvent(input));
					}
				}
				while (input.MoveToNextNamespace());
				input.ToParent();
			}
			if (input.MoveToFirstAttribute())
			{
				do
				{
					if (!Ref.Equal(input.NamespaceURI, input.Atoms.UriXsl))
					{
						this.AddEvent(compiler.CreateBeginEvent());
						this.AddEvents(compiler.CompileAvt(input.Value));
						this.AddEvent(new EndEvent(XPathNodeType.Attribute));
					}
				}
				while (input.MoveToNextAttribute());
				input.ToParent();
			}
		}

		// Token: 0x06003016 RID: 12310 RVA: 0x00100942 File Offset: 0x000FEB42
		private void CompileOutput(Compiler compiler)
		{
			compiler.RootAction.Output.Compile(compiler);
		}

		// Token: 0x06003017 RID: 12311 RVA: 0x00100955 File Offset: 0x000FEB55
		internal void AddAction(Action action)
		{
			if (this.containedActions == null)
			{
				this.containedActions = new ArrayList();
			}
			this.containedActions.Add(action);
			this.lastCopyCodeAction = null;
		}

		// Token: 0x06003018 RID: 12312 RVA: 0x00100980 File Offset: 0x000FEB80
		private void EnsureCopyCodeAction()
		{
			if (this.lastCopyCodeAction == null)
			{
				CopyCodeAction action = new CopyCodeAction();
				this.AddAction(action);
				this.lastCopyCodeAction = action;
			}
		}

		// Token: 0x06003019 RID: 12313 RVA: 0x001009A9 File Offset: 0x000FEBA9
		protected void AddEvent(Event copyEvent)
		{
			this.EnsureCopyCodeAction();
			this.lastCopyCodeAction.AddEvent(copyEvent);
		}

		// Token: 0x0600301A RID: 12314 RVA: 0x001009BD File Offset: 0x000FEBBD
		protected void AddEvents(ArrayList copyEvents)
		{
			this.EnsureCopyCodeAction();
			this.lastCopyCodeAction.AddEvents(copyEvents);
		}

		// Token: 0x0600301B RID: 12315 RVA: 0x001009D4 File Offset: 0x000FEBD4
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 1)
				{
					return;
				}
				frame.Finished();
				return;
			}
			else
			{
				if (this.containedActions != null && this.containedActions.Count > 0)
				{
					processor.PushActionFrame(frame);
					frame.State = 1;
					return;
				}
				frame.Finished();
				return;
			}
		}

		// Token: 0x0600301C RID: 12316 RVA: 0x00100A22 File Offset: 0x000FEC22
		internal Action GetAction(int actionIndex)
		{
			if (this.containedActions != null && actionIndex < this.containedActions.Count)
			{
				return (Action)this.containedActions[actionIndex];
			}
			return null;
		}

		// Token: 0x0600301D RID: 12317 RVA: 0x00100A50 File Offset: 0x000FEC50
		internal void CheckDuplicateParams(XmlQualifiedName name)
		{
			if (this.containedActions != null)
			{
				foreach (object obj in this.containedActions)
				{
					WithParamAction withParamAction = ((CompiledAction)obj) as WithParamAction;
					if (withParamAction != null && withParamAction.Name == name)
					{
						throw XsltException.Create("Value of parameter '{0}' cannot be specified more than once within a single 'xsl:call-template' or 'xsl:apply-templates' element.", new string[]
						{
							name.ToString()
						});
					}
				}
			}
		}

		// Token: 0x0600301E RID: 12318 RVA: 0x00100ADC File Offset: 0x000FECDC
		internal override void ReplaceNamespaceAlias(Compiler compiler)
		{
			if (this.containedActions == null)
			{
				return;
			}
			int count = this.containedActions.Count;
			for (int i = 0; i < this.containedActions.Count; i++)
			{
				((Action)this.containedActions[i]).ReplaceNamespaceAlias(compiler);
			}
		}

		// Token: 0x0600301F RID: 12319 RVA: 0x000FD562 File Offset: 0x000FB762
		public ContainerAction()
		{
		}

		// Token: 0x04002005 RID: 8197
		internal ArrayList containedActions;

		// Token: 0x04002006 RID: 8198
		internal CopyCodeAction lastCopyCodeAction;

		// Token: 0x04002007 RID: 8199
		private int maxid;

		// Token: 0x04002008 RID: 8200
		protected const int ProcessingChildren = 1;
	}
}
