﻿using System;
using System.Diagnostics;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004F8 RID: 1272
	internal class OutKeywords
	{
		// Token: 0x06003162 RID: 12642 RVA: 0x00104590 File Offset: 0x00102790
		internal OutKeywords(XmlNameTable nameTable)
		{
			this._AtomEmpty = nameTable.Add(string.Empty);
			this._AtomLang = nameTable.Add("lang");
			this._AtomSpace = nameTable.Add("space");
			this._AtomXmlns = nameTable.Add("xmlns");
			this._AtomXml = nameTable.Add("xml");
			this._AtomXmlNamespace = nameTable.Add("http://www.w3.org/XML/1998/namespace");
			this._AtomXmlnsNamespace = nameTable.Add("http://www.w3.org/2000/xmlns/");
		}

		// Token: 0x17000A4E RID: 2638
		// (get) Token: 0x06003163 RID: 12643 RVA: 0x0010461A File Offset: 0x0010281A
		internal string Empty
		{
			get
			{
				return this._AtomEmpty;
			}
		}

		// Token: 0x17000A4F RID: 2639
		// (get) Token: 0x06003164 RID: 12644 RVA: 0x00104622 File Offset: 0x00102822
		internal string Lang
		{
			get
			{
				return this._AtomLang;
			}
		}

		// Token: 0x17000A50 RID: 2640
		// (get) Token: 0x06003165 RID: 12645 RVA: 0x0010462A File Offset: 0x0010282A
		internal string Space
		{
			get
			{
				return this._AtomSpace;
			}
		}

		// Token: 0x17000A51 RID: 2641
		// (get) Token: 0x06003166 RID: 12646 RVA: 0x00104632 File Offset: 0x00102832
		internal string Xmlns
		{
			get
			{
				return this._AtomXmlns;
			}
		}

		// Token: 0x17000A52 RID: 2642
		// (get) Token: 0x06003167 RID: 12647 RVA: 0x0010463A File Offset: 0x0010283A
		internal string Xml
		{
			get
			{
				return this._AtomXml;
			}
		}

		// Token: 0x17000A53 RID: 2643
		// (get) Token: 0x06003168 RID: 12648 RVA: 0x00104642 File Offset: 0x00102842
		internal string XmlNamespace
		{
			get
			{
				return this._AtomXmlNamespace;
			}
		}

		// Token: 0x17000A54 RID: 2644
		// (get) Token: 0x06003169 RID: 12649 RVA: 0x0010464A File Offset: 0x0010284A
		internal string XmlnsNamespace
		{
			get
			{
				return this._AtomXmlnsNamespace;
			}
		}

		// Token: 0x0600316A RID: 12650 RVA: 0x000030EC File Offset: 0x000012EC
		[Conditional("DEBUG")]
		private void CheckKeyword(string keyword)
		{
		}

		// Token: 0x040020A7 RID: 8359
		private string _AtomEmpty;

		// Token: 0x040020A8 RID: 8360
		private string _AtomLang;

		// Token: 0x040020A9 RID: 8361
		private string _AtomSpace;

		// Token: 0x040020AA RID: 8362
		private string _AtomXmlns;

		// Token: 0x040020AB RID: 8363
		private string _AtomXml;

		// Token: 0x040020AC RID: 8364
		private string _AtomXmlNamespace;

		// Token: 0x040020AD RID: 8365
		private string _AtomXmlnsNamespace;
	}
}
