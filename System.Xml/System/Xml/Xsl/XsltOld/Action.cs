﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004AE RID: 1198
	internal abstract class Action
	{
		// Token: 0x06002F1F RID: 12063
		internal abstract void Execute(Processor processor, ActionFrame frame);

		// Token: 0x06002F20 RID: 12064 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void ReplaceNamespaceAlias(Compiler compiler)
		{
		}

		// Token: 0x06002F21 RID: 12065 RVA: 0x000FCF9C File Offset: 0x000FB19C
		internal virtual DbgData GetDbgData(ActionFrame frame)
		{
			return DbgData.Empty;
		}

		// Token: 0x06002F22 RID: 12066 RVA: 0x00002103 File Offset: 0x00000303
		protected Action()
		{
		}

		// Token: 0x04001FA5 RID: 8101
		internal const int Initialized = 0;

		// Token: 0x04001FA6 RID: 8102
		internal const int Finished = -1;
	}
}
