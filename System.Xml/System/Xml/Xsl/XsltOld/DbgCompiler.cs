﻿using System;
using System.Collections;
using System.Xml.Xsl.XsltOld.Debugger;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004CA RID: 1226
	internal class DbgCompiler : Compiler
	{
		// Token: 0x06003047 RID: 12359 RVA: 0x001012F4 File Offset: 0x000FF4F4
		public DbgCompiler(IXsltDebugger debugger)
		{
			this.debugger = debugger;
		}

		// Token: 0x17000A21 RID: 2593
		// (get) Token: 0x06003048 RID: 12360 RVA: 0x00101319 File Offset: 0x000FF519
		public override IXsltDebugger Debugger
		{
			get
			{
				return this.debugger;
			}
		}

		// Token: 0x17000A22 RID: 2594
		// (get) Token: 0x06003049 RID: 12361 RVA: 0x00101321 File Offset: 0x000FF521
		public virtual VariableAction[] GlobalVariables
		{
			get
			{
				if (this.globalVarsCache == null)
				{
					this.globalVarsCache = (VariableAction[])this.globalVars.ToArray(typeof(VariableAction));
				}
				return this.globalVarsCache;
			}
		}

		// Token: 0x17000A23 RID: 2595
		// (get) Token: 0x0600304A RID: 12362 RVA: 0x00101351 File Offset: 0x000FF551
		public virtual VariableAction[] LocalVariables
		{
			get
			{
				if (this.localVarsCache == null)
				{
					this.localVarsCache = (VariableAction[])this.localVars.ToArray(typeof(VariableAction));
				}
				return this.localVarsCache;
			}
		}

		// Token: 0x0600304B RID: 12363 RVA: 0x00101384 File Offset: 0x000FF584
		private void DefineVariable(VariableAction variable)
		{
			if (variable.IsGlobal)
			{
				for (int i = 0; i < this.globalVars.Count; i++)
				{
					VariableAction variableAction = (VariableAction)this.globalVars[i];
					if (variableAction.Name == variable.Name)
					{
						if (variable.Stylesheetid < variableAction.Stylesheetid)
						{
							this.globalVars[i] = variable;
							this.globalVarsCache = null;
						}
						return;
					}
				}
				this.globalVars.Add(variable);
				this.globalVarsCache = null;
				return;
			}
			this.localVars.Add(variable);
			this.localVarsCache = null;
		}

		// Token: 0x0600304C RID: 12364 RVA: 0x00101420 File Offset: 0x000FF620
		private void UnDefineVariables(int count)
		{
			if (count != 0)
			{
				this.localVars.RemoveRange(this.localVars.Count - count, count);
				this.localVarsCache = null;
			}
		}

		// Token: 0x0600304D RID: 12365 RVA: 0x00101445 File Offset: 0x000FF645
		internal override void PopScope()
		{
			this.UnDefineVariables(base.ScopeManager.CurrentScope.GetVeriablesCount());
			base.PopScope();
		}

		// Token: 0x0600304E RID: 12366 RVA: 0x00101463 File Offset: 0x000FF663
		public override ApplyImportsAction CreateApplyImportsAction()
		{
			DbgCompiler.ApplyImportsActionDbg applyImportsActionDbg = new DbgCompiler.ApplyImportsActionDbg();
			applyImportsActionDbg.Compile(this);
			return applyImportsActionDbg;
		}

		// Token: 0x0600304F RID: 12367 RVA: 0x00101471 File Offset: 0x000FF671
		public override ApplyTemplatesAction CreateApplyTemplatesAction()
		{
			DbgCompiler.ApplyTemplatesActionDbg applyTemplatesActionDbg = new DbgCompiler.ApplyTemplatesActionDbg();
			applyTemplatesActionDbg.Compile(this);
			return applyTemplatesActionDbg;
		}

		// Token: 0x06003050 RID: 12368 RVA: 0x0010147F File Offset: 0x000FF67F
		public override AttributeAction CreateAttributeAction()
		{
			DbgCompiler.AttributeActionDbg attributeActionDbg = new DbgCompiler.AttributeActionDbg();
			attributeActionDbg.Compile(this);
			return attributeActionDbg;
		}

		// Token: 0x06003051 RID: 12369 RVA: 0x0010148D File Offset: 0x000FF68D
		public override AttributeSetAction CreateAttributeSetAction()
		{
			DbgCompiler.AttributeSetActionDbg attributeSetActionDbg = new DbgCompiler.AttributeSetActionDbg();
			attributeSetActionDbg.Compile(this);
			return attributeSetActionDbg;
		}

		// Token: 0x06003052 RID: 12370 RVA: 0x0010149B File Offset: 0x000FF69B
		public override CallTemplateAction CreateCallTemplateAction()
		{
			DbgCompiler.CallTemplateActionDbg callTemplateActionDbg = new DbgCompiler.CallTemplateActionDbg();
			callTemplateActionDbg.Compile(this);
			return callTemplateActionDbg;
		}

		// Token: 0x06003053 RID: 12371 RVA: 0x000FF504 File Offset: 0x000FD704
		public override ChooseAction CreateChooseAction()
		{
			ChooseAction chooseAction = new ChooseAction();
			chooseAction.Compile(this);
			return chooseAction;
		}

		// Token: 0x06003054 RID: 12372 RVA: 0x001014A9 File Offset: 0x000FF6A9
		public override CommentAction CreateCommentAction()
		{
			DbgCompiler.CommentActionDbg commentActionDbg = new DbgCompiler.CommentActionDbg();
			commentActionDbg.Compile(this);
			return commentActionDbg;
		}

		// Token: 0x06003055 RID: 12373 RVA: 0x001014B7 File Offset: 0x000FF6B7
		public override CopyAction CreateCopyAction()
		{
			DbgCompiler.CopyActionDbg copyActionDbg = new DbgCompiler.CopyActionDbg();
			copyActionDbg.Compile(this);
			return copyActionDbg;
		}

		// Token: 0x06003056 RID: 12374 RVA: 0x001014C5 File Offset: 0x000FF6C5
		public override CopyOfAction CreateCopyOfAction()
		{
			DbgCompiler.CopyOfActionDbg copyOfActionDbg = new DbgCompiler.CopyOfActionDbg();
			copyOfActionDbg.Compile(this);
			return copyOfActionDbg;
		}

		// Token: 0x06003057 RID: 12375 RVA: 0x001014D3 File Offset: 0x000FF6D3
		public override ElementAction CreateElementAction()
		{
			DbgCompiler.ElementActionDbg elementActionDbg = new DbgCompiler.ElementActionDbg();
			elementActionDbg.Compile(this);
			return elementActionDbg;
		}

		// Token: 0x06003058 RID: 12376 RVA: 0x001014E1 File Offset: 0x000FF6E1
		public override ForEachAction CreateForEachAction()
		{
			DbgCompiler.ForEachActionDbg forEachActionDbg = new DbgCompiler.ForEachActionDbg();
			forEachActionDbg.Compile(this);
			return forEachActionDbg;
		}

		// Token: 0x06003059 RID: 12377 RVA: 0x001014EF File Offset: 0x000FF6EF
		public override IfAction CreateIfAction(IfAction.ConditionType type)
		{
			DbgCompiler.IfActionDbg ifActionDbg = new DbgCompiler.IfActionDbg(type);
			ifActionDbg.Compile(this);
			return ifActionDbg;
		}

		// Token: 0x0600305A RID: 12378 RVA: 0x001014FE File Offset: 0x000FF6FE
		public override MessageAction CreateMessageAction()
		{
			DbgCompiler.MessageActionDbg messageActionDbg = new DbgCompiler.MessageActionDbg();
			messageActionDbg.Compile(this);
			return messageActionDbg;
		}

		// Token: 0x0600305B RID: 12379 RVA: 0x0010150C File Offset: 0x000FF70C
		public override NewInstructionAction CreateNewInstructionAction()
		{
			DbgCompiler.NewInstructionActionDbg newInstructionActionDbg = new DbgCompiler.NewInstructionActionDbg();
			newInstructionActionDbg.Compile(this);
			return newInstructionActionDbg;
		}

		// Token: 0x0600305C RID: 12380 RVA: 0x0010151A File Offset: 0x000FF71A
		public override NumberAction CreateNumberAction()
		{
			DbgCompiler.NumberActionDbg numberActionDbg = new DbgCompiler.NumberActionDbg();
			numberActionDbg.Compile(this);
			return numberActionDbg;
		}

		// Token: 0x0600305D RID: 12381 RVA: 0x00101528 File Offset: 0x000FF728
		public override ProcessingInstructionAction CreateProcessingInstructionAction()
		{
			DbgCompiler.ProcessingInstructionActionDbg processingInstructionActionDbg = new DbgCompiler.ProcessingInstructionActionDbg();
			processingInstructionActionDbg.Compile(this);
			return processingInstructionActionDbg;
		}

		// Token: 0x0600305E RID: 12382 RVA: 0x00101536 File Offset: 0x000FF736
		public override void CreateRootAction()
		{
			base.RootAction = new DbgCompiler.RootActionDbg();
			base.RootAction.Compile(this);
		}

		// Token: 0x0600305F RID: 12383 RVA: 0x0010154F File Offset: 0x000FF74F
		public override SortAction CreateSortAction()
		{
			DbgCompiler.SortActionDbg sortActionDbg = new DbgCompiler.SortActionDbg();
			sortActionDbg.Compile(this);
			return sortActionDbg;
		}

		// Token: 0x06003060 RID: 12384 RVA: 0x0010155D File Offset: 0x000FF75D
		public override TemplateAction CreateTemplateAction()
		{
			DbgCompiler.TemplateActionDbg templateActionDbg = new DbgCompiler.TemplateActionDbg();
			templateActionDbg.Compile(this);
			return templateActionDbg;
		}

		// Token: 0x06003061 RID: 12385 RVA: 0x0010156B File Offset: 0x000FF76B
		public override TemplateAction CreateSingleTemplateAction()
		{
			DbgCompiler.TemplateActionDbg templateActionDbg = new DbgCompiler.TemplateActionDbg();
			templateActionDbg.CompileSingle(this);
			return templateActionDbg;
		}

		// Token: 0x06003062 RID: 12386 RVA: 0x00101579 File Offset: 0x000FF779
		public override TextAction CreateTextAction()
		{
			DbgCompiler.TextActionDbg textActionDbg = new DbgCompiler.TextActionDbg();
			textActionDbg.Compile(this);
			return textActionDbg;
		}

		// Token: 0x06003063 RID: 12387 RVA: 0x00101587 File Offset: 0x000FF787
		public override UseAttributeSetsAction CreateUseAttributeSetsAction()
		{
			DbgCompiler.UseAttributeSetsActionDbg useAttributeSetsActionDbg = new DbgCompiler.UseAttributeSetsActionDbg();
			useAttributeSetsActionDbg.Compile(this);
			return useAttributeSetsActionDbg;
		}

		// Token: 0x06003064 RID: 12388 RVA: 0x00101595 File Offset: 0x000FF795
		public override ValueOfAction CreateValueOfAction()
		{
			DbgCompiler.ValueOfActionDbg valueOfActionDbg = new DbgCompiler.ValueOfActionDbg();
			valueOfActionDbg.Compile(this);
			return valueOfActionDbg;
		}

		// Token: 0x06003065 RID: 12389 RVA: 0x001015A3 File Offset: 0x000FF7A3
		public override VariableAction CreateVariableAction(VariableType type)
		{
			DbgCompiler.VariableActionDbg variableActionDbg = new DbgCompiler.VariableActionDbg(type);
			variableActionDbg.Compile(this);
			return variableActionDbg;
		}

		// Token: 0x06003066 RID: 12390 RVA: 0x001015B2 File Offset: 0x000FF7B2
		public override WithParamAction CreateWithParamAction()
		{
			DbgCompiler.WithParamActionDbg withParamActionDbg = new DbgCompiler.WithParamActionDbg();
			withParamActionDbg.Compile(this);
			return withParamActionDbg;
		}

		// Token: 0x06003067 RID: 12391 RVA: 0x001015C0 File Offset: 0x000FF7C0
		public override BeginEvent CreateBeginEvent()
		{
			return new DbgCompiler.BeginEventDbg(this);
		}

		// Token: 0x06003068 RID: 12392 RVA: 0x001015C8 File Offset: 0x000FF7C8
		public override TextEvent CreateTextEvent()
		{
			return new DbgCompiler.TextEventDbg(this);
		}

		// Token: 0x04002029 RID: 8233
		private IXsltDebugger debugger;

		// Token: 0x0400202A RID: 8234
		private ArrayList globalVars = new ArrayList();

		// Token: 0x0400202B RID: 8235
		private ArrayList localVars = new ArrayList();

		// Token: 0x0400202C RID: 8236
		private VariableAction[] globalVarsCache;

		// Token: 0x0400202D RID: 8237
		private VariableAction[] localVarsCache;

		// Token: 0x020004CB RID: 1227
		private class ApplyImportsActionDbg : ApplyImportsAction
		{
			// Token: 0x06003069 RID: 12393 RVA: 0x001015D0 File Offset: 0x000FF7D0
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x0600306A RID: 12394 RVA: 0x001015D8 File Offset: 0x000FF7D8
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x0600306B RID: 12395 RVA: 0x001015ED File Offset: 0x000FF7ED
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x0600306C RID: 12396 RVA: 0x00101605 File Offset: 0x000FF805
			public ApplyImportsActionDbg()
			{
			}

			// Token: 0x0400202E RID: 8238
			private DbgData dbgData;
		}

		// Token: 0x020004CC RID: 1228
		private class ApplyTemplatesActionDbg : ApplyTemplatesAction
		{
			// Token: 0x0600306D RID: 12397 RVA: 0x0010160D File Offset: 0x000FF80D
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x0600306E RID: 12398 RVA: 0x00101615 File Offset: 0x000FF815
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x0600306F RID: 12399 RVA: 0x0010162A File Offset: 0x000FF82A
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x06003070 RID: 12400 RVA: 0x00101642 File Offset: 0x000FF842
			public ApplyTemplatesActionDbg()
			{
			}

			// Token: 0x0400202F RID: 8239
			private DbgData dbgData;
		}

		// Token: 0x020004CD RID: 1229
		private class AttributeActionDbg : AttributeAction
		{
			// Token: 0x06003071 RID: 12401 RVA: 0x0010164A File Offset: 0x000FF84A
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x06003072 RID: 12402 RVA: 0x00101652 File Offset: 0x000FF852
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x06003073 RID: 12403 RVA: 0x00101667 File Offset: 0x000FF867
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x06003074 RID: 12404 RVA: 0x0010167F File Offset: 0x000FF87F
			public AttributeActionDbg()
			{
			}

			// Token: 0x04002030 RID: 8240
			private DbgData dbgData;
		}

		// Token: 0x020004CE RID: 1230
		private class AttributeSetActionDbg : AttributeSetAction
		{
			// Token: 0x06003075 RID: 12405 RVA: 0x00101687 File Offset: 0x000FF887
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x06003076 RID: 12406 RVA: 0x0010168F File Offset: 0x000FF88F
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x06003077 RID: 12407 RVA: 0x001016A4 File Offset: 0x000FF8A4
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x06003078 RID: 12408 RVA: 0x001016BC File Offset: 0x000FF8BC
			public AttributeSetActionDbg()
			{
			}

			// Token: 0x04002031 RID: 8241
			private DbgData dbgData;
		}

		// Token: 0x020004CF RID: 1231
		private class CallTemplateActionDbg : CallTemplateAction
		{
			// Token: 0x06003079 RID: 12409 RVA: 0x001016C4 File Offset: 0x000FF8C4
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x0600307A RID: 12410 RVA: 0x001016CC File Offset: 0x000FF8CC
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x0600307B RID: 12411 RVA: 0x001016E1 File Offset: 0x000FF8E1
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x0600307C RID: 12412 RVA: 0x001016F9 File Offset: 0x000FF8F9
			public CallTemplateActionDbg()
			{
			}

			// Token: 0x04002032 RID: 8242
			private DbgData dbgData;
		}

		// Token: 0x020004D0 RID: 1232
		private class CommentActionDbg : CommentAction
		{
			// Token: 0x0600307D RID: 12413 RVA: 0x00101701 File Offset: 0x000FF901
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x0600307E RID: 12414 RVA: 0x00101709 File Offset: 0x000FF909
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x0600307F RID: 12415 RVA: 0x0010171E File Offset: 0x000FF91E
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x06003080 RID: 12416 RVA: 0x00101736 File Offset: 0x000FF936
			public CommentActionDbg()
			{
			}

			// Token: 0x04002033 RID: 8243
			private DbgData dbgData;
		}

		// Token: 0x020004D1 RID: 1233
		private class CopyActionDbg : CopyAction
		{
			// Token: 0x06003081 RID: 12417 RVA: 0x0010173E File Offset: 0x000FF93E
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x06003082 RID: 12418 RVA: 0x00101746 File Offset: 0x000FF946
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x06003083 RID: 12419 RVA: 0x0010175B File Offset: 0x000FF95B
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x06003084 RID: 12420 RVA: 0x00101773 File Offset: 0x000FF973
			public CopyActionDbg()
			{
			}

			// Token: 0x04002034 RID: 8244
			private DbgData dbgData;
		}

		// Token: 0x020004D2 RID: 1234
		private class CopyOfActionDbg : CopyOfAction
		{
			// Token: 0x06003085 RID: 12421 RVA: 0x0010177B File Offset: 0x000FF97B
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x06003086 RID: 12422 RVA: 0x00101783 File Offset: 0x000FF983
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x06003087 RID: 12423 RVA: 0x00101798 File Offset: 0x000FF998
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x06003088 RID: 12424 RVA: 0x001017B0 File Offset: 0x000FF9B0
			public CopyOfActionDbg()
			{
			}

			// Token: 0x04002035 RID: 8245
			private DbgData dbgData;
		}

		// Token: 0x020004D3 RID: 1235
		private class ElementActionDbg : ElementAction
		{
			// Token: 0x06003089 RID: 12425 RVA: 0x001017B8 File Offset: 0x000FF9B8
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x0600308A RID: 12426 RVA: 0x001017C0 File Offset: 0x000FF9C0
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x0600308B RID: 12427 RVA: 0x001017D5 File Offset: 0x000FF9D5
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x0600308C RID: 12428 RVA: 0x001017ED File Offset: 0x000FF9ED
			public ElementActionDbg()
			{
			}

			// Token: 0x04002036 RID: 8246
			private DbgData dbgData;
		}

		// Token: 0x020004D4 RID: 1236
		private class ForEachActionDbg : ForEachAction
		{
			// Token: 0x0600308D RID: 12429 RVA: 0x001017F5 File Offset: 0x000FF9F5
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x0600308E RID: 12430 RVA: 0x001017FD File Offset: 0x000FF9FD
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x0600308F RID: 12431 RVA: 0x00101812 File Offset: 0x000FFA12
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.PushDebuggerStack();
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
				if (frame.State == -1)
				{
					processor.PopDebuggerStack();
				}
			}

			// Token: 0x06003090 RID: 12432 RVA: 0x0010183F File Offset: 0x000FFA3F
			public ForEachActionDbg()
			{
			}

			// Token: 0x04002037 RID: 8247
			private DbgData dbgData;
		}

		// Token: 0x020004D5 RID: 1237
		private class IfActionDbg : IfAction
		{
			// Token: 0x06003091 RID: 12433 RVA: 0x00101847 File Offset: 0x000FFA47
			internal IfActionDbg(IfAction.ConditionType type) : base(type)
			{
			}

			// Token: 0x06003092 RID: 12434 RVA: 0x00101850 File Offset: 0x000FFA50
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x06003093 RID: 12435 RVA: 0x00101858 File Offset: 0x000FFA58
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x06003094 RID: 12436 RVA: 0x0010186D File Offset: 0x000FFA6D
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x04002038 RID: 8248
			private DbgData dbgData;
		}

		// Token: 0x020004D6 RID: 1238
		private class MessageActionDbg : MessageAction
		{
			// Token: 0x06003095 RID: 12437 RVA: 0x00101885 File Offset: 0x000FFA85
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x06003096 RID: 12438 RVA: 0x0010188D File Offset: 0x000FFA8D
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x06003097 RID: 12439 RVA: 0x001018A2 File Offset: 0x000FFAA2
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x06003098 RID: 12440 RVA: 0x001018BA File Offset: 0x000FFABA
			public MessageActionDbg()
			{
			}

			// Token: 0x04002039 RID: 8249
			private DbgData dbgData;
		}

		// Token: 0x020004D7 RID: 1239
		private class NewInstructionActionDbg : NewInstructionAction
		{
			// Token: 0x06003099 RID: 12441 RVA: 0x001018C2 File Offset: 0x000FFAC2
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x0600309A RID: 12442 RVA: 0x001018CA File Offset: 0x000FFACA
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x0600309B RID: 12443 RVA: 0x001018DF File Offset: 0x000FFADF
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x0600309C RID: 12444 RVA: 0x001018F7 File Offset: 0x000FFAF7
			public NewInstructionActionDbg()
			{
			}

			// Token: 0x0400203A RID: 8250
			private DbgData dbgData;
		}

		// Token: 0x020004D8 RID: 1240
		private class NumberActionDbg : NumberAction
		{
			// Token: 0x0600309D RID: 12445 RVA: 0x001018FF File Offset: 0x000FFAFF
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x0600309E RID: 12446 RVA: 0x00101907 File Offset: 0x000FFB07
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x0600309F RID: 12447 RVA: 0x0010191C File Offset: 0x000FFB1C
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x060030A0 RID: 12448 RVA: 0x00101934 File Offset: 0x000FFB34
			public NumberActionDbg()
			{
			}

			// Token: 0x0400203B RID: 8251
			private DbgData dbgData;
		}

		// Token: 0x020004D9 RID: 1241
		private class ProcessingInstructionActionDbg : ProcessingInstructionAction
		{
			// Token: 0x060030A1 RID: 12449 RVA: 0x0010193C File Offset: 0x000FFB3C
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030A2 RID: 12450 RVA: 0x00101944 File Offset: 0x000FFB44
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x060030A3 RID: 12451 RVA: 0x00101959 File Offset: 0x000FFB59
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x060030A4 RID: 12452 RVA: 0x00101971 File Offset: 0x000FFB71
			public ProcessingInstructionActionDbg()
			{
			}

			// Token: 0x0400203C RID: 8252
			private DbgData dbgData;
		}

		// Token: 0x020004DA RID: 1242
		private class RootActionDbg : RootAction
		{
			// Token: 0x060030A5 RID: 12453 RVA: 0x00101979 File Offset: 0x000FFB79
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030A6 RID: 12454 RVA: 0x00101984 File Offset: 0x000FFB84
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
				string builtInTemplatesUri = compiler.Debugger.GetBuiltInTemplatesUri();
				if (builtInTemplatesUri != null && builtInTemplatesUri.Length != 0)
				{
					compiler.AllowBuiltInMode = true;
					this.builtInSheet = compiler.RootAction.CompileImport(compiler, compiler.ResolveUri(builtInTemplatesUri), int.MaxValue);
					compiler.AllowBuiltInMode = false;
				}
				this.dbgData.ReplaceVariables(((DbgCompiler)compiler).GlobalVariables);
			}

			// Token: 0x060030A7 RID: 12455 RVA: 0x001019FD File Offset: 0x000FFBFD
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.PushDebuggerStack();
					processor.OnInstructionExecute();
					processor.PushDebuggerStack();
				}
				base.Execute(processor, frame);
				if (frame.State == -1)
				{
					processor.PopDebuggerStack();
					processor.PopDebuggerStack();
				}
			}

			// Token: 0x060030A8 RID: 12456 RVA: 0x00101A36 File Offset: 0x000FFC36
			public RootActionDbg()
			{
			}

			// Token: 0x0400203D RID: 8253
			private DbgData dbgData;
		}

		// Token: 0x020004DB RID: 1243
		private class SortActionDbg : SortAction
		{
			// Token: 0x060030A9 RID: 12457 RVA: 0x00101A3E File Offset: 0x000FFC3E
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030AA RID: 12458 RVA: 0x00101A46 File Offset: 0x000FFC46
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x060030AB RID: 12459 RVA: 0x00101A5B File Offset: 0x000FFC5B
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x060030AC RID: 12460 RVA: 0x00101A73 File Offset: 0x000FFC73
			public SortActionDbg()
			{
			}

			// Token: 0x0400203E RID: 8254
			private DbgData dbgData;
		}

		// Token: 0x020004DC RID: 1244
		private class TemplateActionDbg : TemplateAction
		{
			// Token: 0x060030AD RID: 12461 RVA: 0x00101A7B File Offset: 0x000FFC7B
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030AE RID: 12462 RVA: 0x00101A83 File Offset: 0x000FFC83
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x060030AF RID: 12463 RVA: 0x00101A98 File Offset: 0x000FFC98
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.PushDebuggerStack();
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
				if (frame.State == -1)
				{
					processor.PopDebuggerStack();
				}
			}

			// Token: 0x060030B0 RID: 12464 RVA: 0x00101AC5 File Offset: 0x000FFCC5
			public TemplateActionDbg()
			{
			}

			// Token: 0x0400203F RID: 8255
			private DbgData dbgData;
		}

		// Token: 0x020004DD RID: 1245
		private class TextActionDbg : TextAction
		{
			// Token: 0x060030B1 RID: 12465 RVA: 0x00101ACD File Offset: 0x000FFCCD
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030B2 RID: 12466 RVA: 0x00101AD5 File Offset: 0x000FFCD5
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x060030B3 RID: 12467 RVA: 0x00101AEA File Offset: 0x000FFCEA
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x060030B4 RID: 12468 RVA: 0x00101B02 File Offset: 0x000FFD02
			public TextActionDbg()
			{
			}

			// Token: 0x04002040 RID: 8256
			private DbgData dbgData;
		}

		// Token: 0x020004DE RID: 1246
		private class UseAttributeSetsActionDbg : UseAttributeSetsAction
		{
			// Token: 0x060030B5 RID: 12469 RVA: 0x00101B0A File Offset: 0x000FFD0A
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030B6 RID: 12470 RVA: 0x00101B12 File Offset: 0x000FFD12
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x060030B7 RID: 12471 RVA: 0x00101B27 File Offset: 0x000FFD27
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x060030B8 RID: 12472 RVA: 0x00101B3F File Offset: 0x000FFD3F
			public UseAttributeSetsActionDbg()
			{
			}

			// Token: 0x04002041 RID: 8257
			private DbgData dbgData;
		}

		// Token: 0x020004DF RID: 1247
		private class ValueOfActionDbg : ValueOfAction
		{
			// Token: 0x060030B9 RID: 12473 RVA: 0x00101B47 File Offset: 0x000FFD47
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030BA RID: 12474 RVA: 0x00101B4F File Offset: 0x000FFD4F
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x060030BB RID: 12475 RVA: 0x00101B64 File Offset: 0x000FFD64
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x060030BC RID: 12476 RVA: 0x00101B7C File Offset: 0x000FFD7C
			public ValueOfActionDbg()
			{
			}

			// Token: 0x04002042 RID: 8258
			private DbgData dbgData;
		}

		// Token: 0x020004E0 RID: 1248
		private class VariableActionDbg : VariableAction
		{
			// Token: 0x060030BD RID: 12477 RVA: 0x00101B84 File Offset: 0x000FFD84
			internal VariableActionDbg(VariableType type) : base(type)
			{
			}

			// Token: 0x060030BE RID: 12478 RVA: 0x00101B8D File Offset: 0x000FFD8D
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030BF RID: 12479 RVA: 0x00101B95 File Offset: 0x000FFD95
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
				((DbgCompiler)compiler).DefineVariable(this);
			}

			// Token: 0x060030C0 RID: 12480 RVA: 0x00101BB6 File Offset: 0x000FFDB6
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x04002043 RID: 8259
			private DbgData dbgData;
		}

		// Token: 0x020004E1 RID: 1249
		private class WithParamActionDbg : WithParamAction
		{
			// Token: 0x060030C1 RID: 12481 RVA: 0x00101BCE File Offset: 0x000FFDCE
			internal override DbgData GetDbgData(ActionFrame frame)
			{
				return this.dbgData;
			}

			// Token: 0x060030C2 RID: 12482 RVA: 0x00101BD6 File Offset: 0x000FFDD6
			internal override void Compile(Compiler compiler)
			{
				this.dbgData = new DbgData(compiler);
				base.Compile(compiler);
			}

			// Token: 0x060030C3 RID: 12483 RVA: 0x00101BEB File Offset: 0x000FFDEB
			internal override void Execute(Processor processor, ActionFrame frame)
			{
				if (frame.State == 0)
				{
					processor.OnInstructionExecute();
				}
				base.Execute(processor, frame);
			}

			// Token: 0x060030C4 RID: 12484 RVA: 0x00101C03 File Offset: 0x000FFE03
			public WithParamActionDbg()
			{
			}

			// Token: 0x04002044 RID: 8260
			private DbgData dbgData;
		}

		// Token: 0x020004E2 RID: 1250
		private class BeginEventDbg : BeginEvent
		{
			// Token: 0x17000A24 RID: 2596
			// (get) Token: 0x060030C5 RID: 12485 RVA: 0x00101C0B File Offset: 0x000FFE0B
			internal override DbgData DbgData
			{
				get
				{
					return this.dbgData;
				}
			}

			// Token: 0x060030C6 RID: 12486 RVA: 0x00101C13 File Offset: 0x000FFE13
			public BeginEventDbg(Compiler compiler) : base(compiler)
			{
				this.dbgData = new DbgData(compiler);
			}

			// Token: 0x060030C7 RID: 12487 RVA: 0x00101C28 File Offset: 0x000FFE28
			public override bool Output(Processor processor, ActionFrame frame)
			{
				base.OnInstructionExecute(processor);
				return base.Output(processor, frame);
			}

			// Token: 0x04002045 RID: 8261
			private DbgData dbgData;
		}

		// Token: 0x020004E3 RID: 1251
		private class TextEventDbg : TextEvent
		{
			// Token: 0x17000A25 RID: 2597
			// (get) Token: 0x060030C8 RID: 12488 RVA: 0x00101C39 File Offset: 0x000FFE39
			internal override DbgData DbgData
			{
				get
				{
					return this.dbgData;
				}
			}

			// Token: 0x060030C9 RID: 12489 RVA: 0x00101C41 File Offset: 0x000FFE41
			public TextEventDbg(Compiler compiler) : base(compiler)
			{
				this.dbgData = new DbgData(compiler);
			}

			// Token: 0x060030CA RID: 12490 RVA: 0x00101C56 File Offset: 0x000FFE56
			public override bool Output(Processor processor, ActionFrame frame)
			{
				base.OnInstructionExecute(processor);
				return base.Output(processor, frame);
			}

			// Token: 0x04002046 RID: 8262
			private DbgData dbgData;
		}
	}
}
