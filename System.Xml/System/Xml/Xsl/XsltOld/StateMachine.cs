﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200050A RID: 1290
	internal class StateMachine
	{
		// Token: 0x06003282 RID: 12930 RVA: 0x00108DE6 File Offset: 0x00106FE6
		internal StateMachine()
		{
			this._State = 0;
		}

		// Token: 0x17000A96 RID: 2710
		// (get) Token: 0x06003283 RID: 12931 RVA: 0x00108DF5 File Offset: 0x00106FF5
		// (set) Token: 0x06003284 RID: 12932 RVA: 0x00108DFD File Offset: 0x00106FFD
		internal int State
		{
			get
			{
				return this._State;
			}
			set
			{
				this._State = value;
			}
		}

		// Token: 0x06003285 RID: 12933 RVA: 0x00108E06 File Offset: 0x00107006
		internal void Reset()
		{
			this._State = 0;
		}

		// Token: 0x06003286 RID: 12934 RVA: 0x00108E0F File Offset: 0x0010700F
		internal static int StateOnly(int state)
		{
			return state & 15;
		}

		// Token: 0x06003287 RID: 12935 RVA: 0x00108E15 File Offset: 0x00107015
		internal int BeginOutlook(XPathNodeType nodeType)
		{
			return StateMachine.s_BeginTransitions[(int)nodeType][this._State];
		}

		// Token: 0x06003288 RID: 12936 RVA: 0x00108E28 File Offset: 0x00107028
		internal int Begin(XPathNodeType nodeType)
		{
			int num = StateMachine.s_BeginTransitions[(int)nodeType][this._State];
			if (num != 16 && num != 32)
			{
				this._State = (num & 15);
			}
			return num;
		}

		// Token: 0x06003289 RID: 12937 RVA: 0x00108E59 File Offset: 0x00107059
		internal int EndOutlook(XPathNodeType nodeType)
		{
			return StateMachine.s_EndTransitions[(int)nodeType][this._State];
		}

		// Token: 0x0600328A RID: 12938 RVA: 0x00108E6C File Offset: 0x0010706C
		internal int End(XPathNodeType nodeType)
		{
			int num = StateMachine.s_EndTransitions[(int)nodeType][this._State];
			if (num != 16 && num != 32)
			{
				this._State = (num & 15);
			}
			return num;
		}

		// Token: 0x0600328B RID: 12939 RVA: 0x00108EA0 File Offset: 0x001070A0
		// Note: this type is marked as 'beforefieldinit'.
		static StateMachine()
		{
		}

		// Token: 0x0400216B RID: 8555
		private const int Init = 0;

		// Token: 0x0400216C RID: 8556
		private const int Elem = 1;

		// Token: 0x0400216D RID: 8557
		private const int NsN = 2;

		// Token: 0x0400216E RID: 8558
		private const int NsV = 3;

		// Token: 0x0400216F RID: 8559
		private const int Ns = 4;

		// Token: 0x04002170 RID: 8560
		private const int AttrN = 5;

		// Token: 0x04002171 RID: 8561
		private const int AttrV = 6;

		// Token: 0x04002172 RID: 8562
		private const int Attr = 7;

		// Token: 0x04002173 RID: 8563
		private const int InElm = 8;

		// Token: 0x04002174 RID: 8564
		private const int EndEm = 9;

		// Token: 0x04002175 RID: 8565
		private const int InCmt = 10;

		// Token: 0x04002176 RID: 8566
		private const int InPI = 11;

		// Token: 0x04002177 RID: 8567
		private const int StateMask = 15;

		// Token: 0x04002178 RID: 8568
		internal const int Error = 16;

		// Token: 0x04002179 RID: 8569
		private const int Ignor = 32;

		// Token: 0x0400217A RID: 8570
		private const int Assrt = 48;

		// Token: 0x0400217B RID: 8571
		private const int U = 256;

		// Token: 0x0400217C RID: 8572
		private const int D = 512;

		// Token: 0x0400217D RID: 8573
		internal const int DepthMask = 768;

		// Token: 0x0400217E RID: 8574
		internal const int DepthUp = 256;

		// Token: 0x0400217F RID: 8575
		internal const int DepthDown = 512;

		// Token: 0x04002180 RID: 8576
		private const int C = 1024;

		// Token: 0x04002181 RID: 8577
		private const int H = 2048;

		// Token: 0x04002182 RID: 8578
		private const int M = 4096;

		// Token: 0x04002183 RID: 8579
		internal const int BeginChild = 1024;

		// Token: 0x04002184 RID: 8580
		internal const int HadChild = 2048;

		// Token: 0x04002185 RID: 8581
		internal const int EmptyTag = 4096;

		// Token: 0x04002186 RID: 8582
		private const int B = 8192;

		// Token: 0x04002187 RID: 8583
		private const int E = 16384;

		// Token: 0x04002188 RID: 8584
		internal const int BeginRecord = 8192;

		// Token: 0x04002189 RID: 8585
		internal const int EndRecord = 16384;

		// Token: 0x0400218A RID: 8586
		private const int S = 32768;

		// Token: 0x0400218B RID: 8587
		private const int P = 65536;

		// Token: 0x0400218C RID: 8588
		internal const int PushScope = 32768;

		// Token: 0x0400218D RID: 8589
		internal const int PopScope = 65536;

		// Token: 0x0400218E RID: 8590
		private int _State;

		// Token: 0x0400218F RID: 8591
		private static readonly int[][] s_BeginTransitions = new int[][]
		{
			new int[]
			{
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16
			},
			new int[]
			{
				40961,
				42241,
				16,
				16,
				41985,
				16,
				16,
				41985,
				40961,
				106497,
				16,
				16
			},
			new int[]
			{
				16,
				261,
				16,
				16,
				5,
				16,
				16,
				5,
				16,
				16,
				16,
				16
			},
			new int[]
			{
				16,
				258,
				16,
				16,
				2,
				16,
				16,
				16,
				16,
				16,
				16,
				16
			},
			new int[]
			{
				8200,
				9480,
				259,
				3,
				9224,
				262,
				6,
				9224,
				8,
				73736,
				10,
				11
			},
			new int[]
			{
				8200,
				9480,
				259,
				3,
				9224,
				262,
				6,
				9224,
				8,
				73736,
				10,
				11
			},
			new int[]
			{
				8200,
				9480,
				259,
				3,
				9224,
				262,
				6,
				9224,
				8,
				73736,
				10,
				11
			},
			new int[]
			{
				8203,
				9483,
				16,
				16,
				9227,
				16,
				16,
				9227,
				8203,
				73739,
				16,
				16
			},
			new int[]
			{
				8202,
				9482,
				16,
				16,
				9226,
				16,
				16,
				9226,
				8202,
				73738,
				16,
				16
			},
			new int[]
			{
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16,
				16
			}
		};

		// Token: 0x04002190 RID: 8592
		private static readonly int[][] s_EndTransitions = new int[][]
		{
			new int[]
			{
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48
			},
			new int[]
			{
				48,
				94217,
				48,
				48,
				94729,
				48,
				48,
				94729,
				92681,
				92681,
				48,
				48
			},
			new int[]
			{
				48,
				48,
				48,
				48,
				48,
				7,
				519,
				48,
				48,
				48,
				48,
				48
			},
			new int[]
			{
				48,
				48,
				4,
				516,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48
			},
			new int[]
			{
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48
			},
			new int[]
			{
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48
			},
			new int[]
			{
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48
			},
			new int[]
			{
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				16393
			},
			new int[]
			{
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				16393,
				48
			},
			new int[]
			{
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48,
				48
			}
		};
	}
}
