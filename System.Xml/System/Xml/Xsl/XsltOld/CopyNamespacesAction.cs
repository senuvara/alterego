﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C6 RID: 1222
	internal sealed class CopyNamespacesAction : Action
	{
		// Token: 0x06003031 RID: 12337 RVA: 0x00100ECE File Offset: 0x000FF0CE
		internal static CopyNamespacesAction GetAction()
		{
			return CopyNamespacesAction.s_Action;
		}

		// Token: 0x06003032 RID: 12338 RVA: 0x00100ED8 File Offset: 0x000FF0D8
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			while (processor.CanContinue)
			{
				switch (frame.State)
				{
				case 0:
					if (!frame.Node.MoveToFirstNamespace(XPathNamespaceScope.ExcludeXml))
					{
						frame.Finished();
						return;
					}
					frame.State = 2;
					break;
				case 1:
				case 3:
					return;
				case 2:
					break;
				case 4:
					if (processor.EndEvent(XPathNodeType.Namespace))
					{
						frame.State = 5;
						continue;
					}
					return;
				case 5:
					if (frame.Node.MoveToNextNamespace(XPathNamespaceScope.ExcludeXml))
					{
						frame.State = 2;
						continue;
					}
					frame.Node.MoveToParent();
					frame.Finished();
					return;
				default:
					return;
				}
				if (!processor.BeginEvent(XPathNodeType.Namespace, null, frame.Node.LocalName, frame.Node.Value, false))
				{
					break;
				}
				frame.State = 4;
			}
		}

		// Token: 0x06003033 RID: 12339 RVA: 0x000FE6D5 File Offset: 0x000FC8D5
		public CopyNamespacesAction()
		{
		}

		// Token: 0x06003034 RID: 12340 RVA: 0x00100F9D File Offset: 0x000FF19D
		// Note: this type is marked as 'beforefieldinit'.
		static CopyNamespacesAction()
		{
		}

		// Token: 0x04002017 RID: 8215
		private const int BeginEvent = 2;

		// Token: 0x04002018 RID: 8216
		private const int TextEvent = 3;

		// Token: 0x04002019 RID: 8217
		private const int EndEvent = 4;

		// Token: 0x0400201A RID: 8218
		private const int Advance = 5;

		// Token: 0x0400201B RID: 8219
		private static CopyNamespacesAction s_Action = new CopyNamespacesAction();
	}
}
