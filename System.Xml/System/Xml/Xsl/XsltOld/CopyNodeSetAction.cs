﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C7 RID: 1223
	internal sealed class CopyNodeSetAction : Action
	{
		// Token: 0x06003035 RID: 12341 RVA: 0x00100FA9 File Offset: 0x000FF1A9
		internal static CopyNodeSetAction GetAction()
		{
			return CopyNodeSetAction.s_Action;
		}

		// Token: 0x06003036 RID: 12342 RVA: 0x00100FB0 File Offset: 0x000FF1B0
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			while (processor.CanContinue)
			{
				switch (frame.State)
				{
				case 0:
					if (!frame.NextNode(processor))
					{
						frame.Finished();
						return;
					}
					frame.State = 2;
					break;
				case 1:
					return;
				case 2:
					break;
				case 3:
				{
					XPathNodeType nodeType = frame.Node.NodeType;
					if (nodeType == XPathNodeType.Element || nodeType == XPathNodeType.Root)
					{
						processor.PushActionFrame(CopyNamespacesAction.GetAction(), frame.NodeSet);
						frame.State = 4;
						return;
					}
					if (CopyNodeSetAction.SendTextEvent(processor, frame.Node))
					{
						frame.State = 7;
						continue;
					}
					return;
				}
				case 4:
					processor.PushActionFrame(CopyAttributesAction.GetAction(), frame.NodeSet);
					frame.State = 5;
					return;
				case 5:
					if (frame.Node.HasChildren)
					{
						processor.PushActionFrame(CopyNodeSetAction.GetAction(), frame.Node.SelectChildren(XPathNodeType.All));
						frame.State = 6;
						return;
					}
					frame.State = 7;
					goto IL_107;
				case 6:
					frame.State = 7;
					continue;
				case 7:
					goto IL_107;
				default:
					return;
				}
				if (CopyNodeSetAction.SendBeginEvent(processor, frame.Node))
				{
					frame.State = 3;
					continue;
				}
				break;
				IL_107:
				if (!CopyNodeSetAction.SendEndEvent(processor, frame.Node))
				{
					break;
				}
				frame.State = 0;
			}
		}

		// Token: 0x06003037 RID: 12343 RVA: 0x001010E4 File Offset: 0x000FF2E4
		private static bool SendBeginEvent(Processor processor, XPathNavigator node)
		{
			return processor.CopyBeginEvent(node, node.IsEmptyElement);
		}

		// Token: 0x06003038 RID: 12344 RVA: 0x001010F3 File Offset: 0x000FF2F3
		private static bool SendTextEvent(Processor processor, XPathNavigator node)
		{
			return processor.CopyTextEvent(node);
		}

		// Token: 0x06003039 RID: 12345 RVA: 0x001010FC File Offset: 0x000FF2FC
		private static bool SendEndEvent(Processor processor, XPathNavigator node)
		{
			return processor.CopyEndEvent(node);
		}

		// Token: 0x0600303A RID: 12346 RVA: 0x000FE6D5 File Offset: 0x000FC8D5
		public CopyNodeSetAction()
		{
		}

		// Token: 0x0600303B RID: 12347 RVA: 0x00101105 File Offset: 0x000FF305
		// Note: this type is marked as 'beforefieldinit'.
		static CopyNodeSetAction()
		{
		}

		// Token: 0x0400201C RID: 8220
		private const int BeginEvent = 2;

		// Token: 0x0400201D RID: 8221
		private const int Contents = 3;

		// Token: 0x0400201E RID: 8222
		private const int Namespaces = 4;

		// Token: 0x0400201F RID: 8223
		private const int Attributes = 5;

		// Token: 0x04002020 RID: 8224
		private const int Subtree = 6;

		// Token: 0x04002021 RID: 8225
		private const int EndEvent = 7;

		// Token: 0x04002022 RID: 8226
		private static CopyNodeSetAction s_Action = new CopyNodeSetAction();
	}
}
