﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004B7 RID: 1207
	internal class BeginEvent : Event
	{
		// Token: 0x06002F6E RID: 12142 RVA: 0x000FDE00 File Offset: 0x000FC000
		public BeginEvent(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			this.nodeType = input.NodeType;
			this.namespaceUri = input.NamespaceURI;
			this.name = input.LocalName;
			this.prefix = input.Prefix;
			this.empty = input.IsEmptyTag;
			if (this.nodeType == XPathNodeType.Element)
			{
				this.htmlProps = HtmlElementProps.GetProps(this.name);
				return;
			}
			if (this.nodeType == XPathNodeType.Attribute)
			{
				this.htmlProps = HtmlAttributeProps.GetProps(this.name);
			}
		}

		// Token: 0x06002F6F RID: 12143 RVA: 0x000FDE8C File Offset: 0x000FC08C
		public override void ReplaceNamespaceAlias(Compiler compiler)
		{
			if (this.nodeType == XPathNodeType.Attribute && this.namespaceUri.Length == 0)
			{
				return;
			}
			NamespaceInfo namespaceInfo = compiler.FindNamespaceAlias(this.namespaceUri);
			if (namespaceInfo != null)
			{
				this.namespaceUri = namespaceInfo.nameSpace;
				if (namespaceInfo.prefix != null)
				{
					this.prefix = namespaceInfo.prefix;
				}
			}
		}

		// Token: 0x06002F70 RID: 12144 RVA: 0x000FDEE0 File Offset: 0x000FC0E0
		public override bool Output(Processor processor, ActionFrame frame)
		{
			return processor.BeginEvent(this.nodeType, this.prefix, this.name, this.namespaceUri, this.empty, this.htmlProps, false);
		}

		// Token: 0x04001FC7 RID: 8135
		private XPathNodeType nodeType;

		// Token: 0x04001FC8 RID: 8136
		private string namespaceUri;

		// Token: 0x04001FC9 RID: 8137
		private string name;

		// Token: 0x04001FCA RID: 8138
		private string prefix;

		// Token: 0x04001FCB RID: 8139
		private bool empty;

		// Token: 0x04001FCC RID: 8140
		private object htmlProps;
	}
}
