﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml.Utils;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000501 RID: 1281
	internal class ReaderOutput : XmlReader, RecordOutput
	{
		// Token: 0x060031E0 RID: 12768 RVA: 0x00105E48 File Offset: 0x00104048
		internal ReaderOutput(Processor processor)
		{
			this.processor = processor;
			this.nameTable = processor.NameTable;
			this.Reset();
		}

		// Token: 0x17000A72 RID: 2674
		// (get) Token: 0x060031E1 RID: 12769 RVA: 0x00105E7F File Offset: 0x0010407F
		public override XmlNodeType NodeType
		{
			get
			{
				return this.currentInfo.NodeType;
			}
		}

		// Token: 0x17000A73 RID: 2675
		// (get) Token: 0x060031E2 RID: 12770 RVA: 0x00105E8C File Offset: 0x0010408C
		public override string Name
		{
			get
			{
				string prefix = this.Prefix;
				string localName = this.LocalName;
				if (prefix == null || prefix.Length <= 0)
				{
					return localName;
				}
				if (localName.Length > 0)
				{
					return this.nameTable.Add(prefix + ":" + localName);
				}
				return prefix;
			}
		}

		// Token: 0x17000A74 RID: 2676
		// (get) Token: 0x060031E3 RID: 12771 RVA: 0x00105ED7 File Offset: 0x001040D7
		public override string LocalName
		{
			get
			{
				return this.currentInfo.LocalName;
			}
		}

		// Token: 0x17000A75 RID: 2677
		// (get) Token: 0x060031E4 RID: 12772 RVA: 0x00105EE4 File Offset: 0x001040E4
		public override string NamespaceURI
		{
			get
			{
				return this.currentInfo.NamespaceURI;
			}
		}

		// Token: 0x17000A76 RID: 2678
		// (get) Token: 0x060031E5 RID: 12773 RVA: 0x00105EF1 File Offset: 0x001040F1
		public override string Prefix
		{
			get
			{
				return this.currentInfo.Prefix;
			}
		}

		// Token: 0x17000A77 RID: 2679
		// (get) Token: 0x060031E6 RID: 12774 RVA: 0x000297AD File Offset: 0x000279AD
		public override bool HasValue
		{
			get
			{
				return XmlReader.HasValueInternal(this.NodeType);
			}
		}

		// Token: 0x17000A78 RID: 2680
		// (get) Token: 0x060031E7 RID: 12775 RVA: 0x00105EFE File Offset: 0x001040FE
		public override string Value
		{
			get
			{
				return this.currentInfo.Value;
			}
		}

		// Token: 0x17000A79 RID: 2681
		// (get) Token: 0x060031E8 RID: 12776 RVA: 0x00105F0B File Offset: 0x0010410B
		public override int Depth
		{
			get
			{
				return this.currentInfo.Depth;
			}
		}

		// Token: 0x17000A7A RID: 2682
		// (get) Token: 0x060031E9 RID: 12777 RVA: 0x00003201 File Offset: 0x00001401
		public override string BaseURI
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000A7B RID: 2683
		// (get) Token: 0x060031EA RID: 12778 RVA: 0x00105F18 File Offset: 0x00104118
		public override bool IsEmptyElement
		{
			get
			{
				return this.currentInfo.IsEmptyTag;
			}
		}

		// Token: 0x17000A7C RID: 2684
		// (get) Token: 0x060031EB RID: 12779 RVA: 0x00105F25 File Offset: 0x00104125
		public override char QuoteChar
		{
			get
			{
				return this.encoder.QuoteChar;
			}
		}

		// Token: 0x17000A7D RID: 2685
		// (get) Token: 0x060031EC RID: 12780 RVA: 0x000020CD File Offset: 0x000002CD
		public override bool IsDefault
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A7E RID: 2686
		// (get) Token: 0x060031ED RID: 12781 RVA: 0x00105F32 File Offset: 0x00104132
		public override XmlSpace XmlSpace
		{
			get
			{
				if (this.manager == null)
				{
					return XmlSpace.None;
				}
				return this.manager.XmlSpace;
			}
		}

		// Token: 0x17000A7F RID: 2687
		// (get) Token: 0x060031EE RID: 12782 RVA: 0x00105F49 File Offset: 0x00104149
		public override string XmlLang
		{
			get
			{
				if (this.manager == null)
				{
					return string.Empty;
				}
				return this.manager.XmlLang;
			}
		}

		// Token: 0x17000A80 RID: 2688
		// (get) Token: 0x060031EF RID: 12783 RVA: 0x00105F64 File Offset: 0x00104164
		public override int AttributeCount
		{
			get
			{
				return this.attributeCount;
			}
		}

		// Token: 0x060031F0 RID: 12784 RVA: 0x00105F6C File Offset: 0x0010416C
		public override string GetAttribute(string name)
		{
			int index;
			if (this.FindAttribute(name, out index))
			{
				return ((BuilderInfo)this.attributeList[index]).Value;
			}
			return null;
		}

		// Token: 0x060031F1 RID: 12785 RVA: 0x00105F9C File Offset: 0x0010419C
		public override string GetAttribute(string localName, string namespaceURI)
		{
			int index;
			if (this.FindAttribute(localName, namespaceURI, out index))
			{
				return ((BuilderInfo)this.attributeList[index]).Value;
			}
			return null;
		}

		// Token: 0x060031F2 RID: 12786 RVA: 0x00105FCD File Offset: 0x001041CD
		public override string GetAttribute(int i)
		{
			return this.GetBuilderInfo(i).Value;
		}

		// Token: 0x17000A81 RID: 2689
		public override string this[int i]
		{
			get
			{
				return this.GetAttribute(i);
			}
		}

		// Token: 0x17000A82 RID: 2690
		public override string this[string name]
		{
			get
			{
				return this.GetAttribute(name);
			}
		}

		// Token: 0x17000A83 RID: 2691
		public override string this[string name, string namespaceURI]
		{
			get
			{
				return this.GetAttribute(name, namespaceURI);
			}
		}

		// Token: 0x060031F6 RID: 12790 RVA: 0x00105FDC File Offset: 0x001041DC
		public override bool MoveToAttribute(string name)
		{
			int attribute;
			if (this.FindAttribute(name, out attribute))
			{
				this.SetAttribute(attribute);
				return true;
			}
			return false;
		}

		// Token: 0x060031F7 RID: 12791 RVA: 0x00106000 File Offset: 0x00104200
		public override bool MoveToAttribute(string localName, string namespaceURI)
		{
			int attribute;
			if (this.FindAttribute(localName, namespaceURI, out attribute))
			{
				this.SetAttribute(attribute);
				return true;
			}
			return false;
		}

		// Token: 0x060031F8 RID: 12792 RVA: 0x00106023 File Offset: 0x00104223
		public override void MoveToAttribute(int i)
		{
			if (i < 0 || this.attributeCount <= i)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			this.SetAttribute(i);
		}

		// Token: 0x060031F9 RID: 12793 RVA: 0x00106044 File Offset: 0x00104244
		public override bool MoveToFirstAttribute()
		{
			if (this.attributeCount <= 0)
			{
				return false;
			}
			this.SetAttribute(0);
			return true;
		}

		// Token: 0x060031FA RID: 12794 RVA: 0x00106059 File Offset: 0x00104259
		public override bool MoveToNextAttribute()
		{
			if (this.currentIndex + 1 < this.attributeCount)
			{
				this.SetAttribute(this.currentIndex + 1);
				return true;
			}
			return false;
		}

		// Token: 0x060031FB RID: 12795 RVA: 0x0010607C File Offset: 0x0010427C
		public override bool MoveToElement()
		{
			if (this.NodeType == XmlNodeType.Attribute || this.currentInfo == this.attributeValue)
			{
				this.SetMainNode();
				return true;
			}
			return false;
		}

		// Token: 0x060031FC RID: 12796 RVA: 0x001060A0 File Offset: 0x001042A0
		public override bool Read()
		{
			if (this.state != ReadState.Interactive)
			{
				if (this.state != ReadState.Initial)
				{
					return false;
				}
				this.state = ReadState.Interactive;
			}
			for (;;)
			{
				if (this.haveRecord)
				{
					this.processor.ResetOutput();
					this.haveRecord = false;
				}
				this.processor.Execute();
				if (!this.haveRecord)
				{
					goto IL_A0;
				}
				XmlNodeType nodeType = this.NodeType;
				if (nodeType != XmlNodeType.Text)
				{
					if (nodeType != XmlNodeType.Whitespace)
					{
						break;
					}
				}
				else
				{
					if (!this.xmlCharType.IsOnlyWhitespace(this.Value))
					{
						break;
					}
					this.currentInfo.NodeType = XmlNodeType.Whitespace;
				}
				if (this.Value.Length != 0)
				{
					goto Block_8;
				}
			}
			goto IL_AD;
			Block_8:
			if (this.XmlSpace == XmlSpace.Preserve)
			{
				this.currentInfo.NodeType = XmlNodeType.SignificantWhitespace;
				goto IL_AD;
			}
			goto IL_AD;
			IL_A0:
			this.state = ReadState.EndOfFile;
			this.Reset();
			IL_AD:
			return this.haveRecord;
		}

		// Token: 0x17000A84 RID: 2692
		// (get) Token: 0x060031FD RID: 12797 RVA: 0x00106160 File Offset: 0x00104360
		public override bool EOF
		{
			get
			{
				return this.state == ReadState.EndOfFile;
			}
		}

		// Token: 0x060031FE RID: 12798 RVA: 0x0010616B File Offset: 0x0010436B
		public override void Close()
		{
			this.processor = null;
			this.state = ReadState.Closed;
			this.Reset();
		}

		// Token: 0x17000A85 RID: 2693
		// (get) Token: 0x060031FF RID: 12799 RVA: 0x00106181 File Offset: 0x00104381
		public override ReadState ReadState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x06003200 RID: 12800 RVA: 0x0010618C File Offset: 0x0010438C
		public override string ReadString()
		{
			string text = string.Empty;
			if (this.NodeType == XmlNodeType.Element || this.NodeType == XmlNodeType.Attribute || this.currentInfo == this.attributeValue)
			{
				if (this.mainNode.IsEmptyTag)
				{
					return text;
				}
				if (!this.Read())
				{
					throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
				}
			}
			StringBuilder stringBuilder = null;
			bool flag = true;
			do
			{
				XmlNodeType nodeType = this.NodeType;
				if (nodeType != XmlNodeType.Text && nodeType - XmlNodeType.Whitespace > 1)
				{
					goto IL_A0;
				}
				if (flag)
				{
					text = this.Value;
					flag = false;
				}
				else
				{
					if (stringBuilder == null)
					{
						stringBuilder = new StringBuilder(text);
					}
					stringBuilder.Append(this.Value);
				}
			}
			while (this.Read());
			throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			IL_A0:
			if (stringBuilder != null)
			{
				return stringBuilder.ToString();
			}
			return text;
		}

		// Token: 0x06003201 RID: 12801 RVA: 0x00106244 File Offset: 0x00104444
		public override string ReadInnerXml()
		{
			if (this.ReadState == ReadState.Interactive)
			{
				if (this.NodeType == XmlNodeType.Element && !this.IsEmptyElement)
				{
					StringOutput stringOutput = new StringOutput(this.processor);
					stringOutput.OmitXmlDecl();
					int i = this.Depth;
					this.Read();
					while (i < this.Depth)
					{
						stringOutput.RecordDone(this.builder);
						this.Read();
					}
					this.Read();
					stringOutput.TheEnd();
					return stringOutput.Result;
				}
				if (this.NodeType == XmlNodeType.Attribute)
				{
					return this.encoder.AtributeInnerXml(this.Value);
				}
				this.Read();
			}
			return string.Empty;
		}

		// Token: 0x06003202 RID: 12802 RVA: 0x001062E8 File Offset: 0x001044E8
		public override string ReadOuterXml()
		{
			if (this.ReadState == ReadState.Interactive)
			{
				if (this.NodeType == XmlNodeType.Element)
				{
					StringOutput stringOutput = new StringOutput(this.processor);
					stringOutput.OmitXmlDecl();
					bool isEmptyElement = this.IsEmptyElement;
					int i = this.Depth;
					stringOutput.RecordDone(this.builder);
					this.Read();
					while (i < this.Depth)
					{
						stringOutput.RecordDone(this.builder);
						this.Read();
					}
					if (!isEmptyElement)
					{
						stringOutput.RecordDone(this.builder);
						this.Read();
					}
					stringOutput.TheEnd();
					return stringOutput.Result;
				}
				if (this.NodeType == XmlNodeType.Attribute)
				{
					return this.encoder.AtributeOuterXml(this.Name, this.Value);
				}
				this.Read();
			}
			return string.Empty;
		}

		// Token: 0x17000A86 RID: 2694
		// (get) Token: 0x06003203 RID: 12803 RVA: 0x001063AE File Offset: 0x001045AE
		public override XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		// Token: 0x06003204 RID: 12804 RVA: 0x001063B6 File Offset: 0x001045B6
		public override string LookupNamespace(string prefix)
		{
			prefix = this.nameTable.Get(prefix);
			if (this.manager != null && prefix != null)
			{
				return this.manager.ResolveNamespace(prefix);
			}
			return null;
		}

		// Token: 0x06003205 RID: 12805 RVA: 0x001063DF File Offset: 0x001045DF
		public override void ResolveEntity()
		{
			if (this.NodeType != XmlNodeType.EntityReference)
			{
				throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			}
		}

		// Token: 0x06003206 RID: 12806 RVA: 0x001063FC File Offset: 0x001045FC
		public override bool ReadAttributeValue()
		{
			if (this.ReadState != ReadState.Interactive || this.NodeType != XmlNodeType.Attribute)
			{
				return false;
			}
			if (this.attributeValue == null)
			{
				this.attributeValue = new BuilderInfo();
				this.attributeValue.NodeType = XmlNodeType.Text;
			}
			if (this.currentInfo == this.attributeValue)
			{
				return false;
			}
			this.attributeValue.Value = this.currentInfo.Value;
			this.attributeValue.Depth = this.currentInfo.Depth + 1;
			this.currentInfo = this.attributeValue;
			return true;
		}

		// Token: 0x06003207 RID: 12807 RVA: 0x00106488 File Offset: 0x00104688
		public Processor.OutputResult RecordDone(RecordBuilder record)
		{
			this.builder = record;
			this.mainNode = record.MainNode;
			this.attributeList = record.AttributeList;
			this.attributeCount = record.AttributeCount;
			this.manager = record.Manager;
			this.haveRecord = true;
			this.SetMainNode();
			return Processor.OutputResult.Interrupt;
		}

		// Token: 0x06003208 RID: 12808 RVA: 0x000030EC File Offset: 0x000012EC
		public void TheEnd()
		{
		}

		// Token: 0x06003209 RID: 12809 RVA: 0x001064DA File Offset: 0x001046DA
		private void SetMainNode()
		{
			this.currentIndex = -1;
			this.currentInfo = this.mainNode;
		}

		// Token: 0x0600320A RID: 12810 RVA: 0x001064EF File Offset: 0x001046EF
		private void SetAttribute(int attrib)
		{
			this.currentIndex = attrib;
			this.currentInfo = (BuilderInfo)this.attributeList[attrib];
		}

		// Token: 0x0600320B RID: 12811 RVA: 0x0010650F File Offset: 0x0010470F
		private BuilderInfo GetBuilderInfo(int attrib)
		{
			if (attrib < 0 || this.attributeCount <= attrib)
			{
				throw new ArgumentOutOfRangeException("attrib");
			}
			return (BuilderInfo)this.attributeList[attrib];
		}

		// Token: 0x0600320C RID: 12812 RVA: 0x0010653C File Offset: 0x0010473C
		private bool FindAttribute(string localName, string namespaceURI, out int attrIndex)
		{
			if (namespaceURI == null)
			{
				namespaceURI = string.Empty;
			}
			if (localName == null)
			{
				localName = string.Empty;
			}
			for (int i = 0; i < this.attributeCount; i++)
			{
				BuilderInfo builderInfo = (BuilderInfo)this.attributeList[i];
				if (builderInfo.NamespaceURI == namespaceURI && builderInfo.LocalName == localName)
				{
					attrIndex = i;
					return true;
				}
			}
			attrIndex = -1;
			return false;
		}

		// Token: 0x0600320D RID: 12813 RVA: 0x001065A8 File Offset: 0x001047A8
		private bool FindAttribute(string name, out int attrIndex)
		{
			if (name == null)
			{
				name = string.Empty;
			}
			for (int i = 0; i < this.attributeCount; i++)
			{
				if (((BuilderInfo)this.attributeList[i]).Name == name)
				{
					attrIndex = i;
					return true;
				}
			}
			attrIndex = -1;
			return false;
		}

		// Token: 0x0600320E RID: 12814 RVA: 0x001065F7 File Offset: 0x001047F7
		private void Reset()
		{
			this.currentIndex = -1;
			this.currentInfo = ReaderOutput.s_DefaultInfo;
			this.mainNode = ReaderOutput.s_DefaultInfo;
			this.manager = null;
		}

		// Token: 0x0600320F RID: 12815 RVA: 0x000030EC File Offset: 0x000012EC
		[Conditional("DEBUG")]
		private void CheckCurrentInfo()
		{
		}

		// Token: 0x06003210 RID: 12816 RVA: 0x0010661D File Offset: 0x0010481D
		// Note: this type is marked as 'beforefieldinit'.
		static ReaderOutput()
		{
		}

		// Token: 0x040020F0 RID: 8432
		private Processor processor;

		// Token: 0x040020F1 RID: 8433
		private XmlNameTable nameTable;

		// Token: 0x040020F2 RID: 8434
		private RecordBuilder builder;

		// Token: 0x040020F3 RID: 8435
		private BuilderInfo mainNode;

		// Token: 0x040020F4 RID: 8436
		private ArrayList attributeList;

		// Token: 0x040020F5 RID: 8437
		private int attributeCount;

		// Token: 0x040020F6 RID: 8438
		private BuilderInfo attributeValue;

		// Token: 0x040020F7 RID: 8439
		private OutputScopeManager manager;

		// Token: 0x040020F8 RID: 8440
		private int currentIndex;

		// Token: 0x040020F9 RID: 8441
		private BuilderInfo currentInfo;

		// Token: 0x040020FA RID: 8442
		private ReadState state;

		// Token: 0x040020FB RID: 8443
		private bool haveRecord;

		// Token: 0x040020FC RID: 8444
		private static BuilderInfo s_DefaultInfo = new BuilderInfo();

		// Token: 0x040020FD RID: 8445
		private ReaderOutput.XmlEncoder encoder = new ReaderOutput.XmlEncoder();

		// Token: 0x040020FE RID: 8446
		private XmlCharType xmlCharType = XmlCharType.Instance;

		// Token: 0x02000502 RID: 1282
		private class XmlEncoder
		{
			// Token: 0x06003211 RID: 12817 RVA: 0x00106629 File Offset: 0x00104829
			private void Init()
			{
				this.buffer = new StringBuilder();
				this.encoder = new XmlTextEncoder(new StringWriter(this.buffer, CultureInfo.InvariantCulture));
			}

			// Token: 0x06003212 RID: 12818 RVA: 0x00106654 File Offset: 0x00104854
			public string AtributeInnerXml(string value)
			{
				if (this.encoder == null)
				{
					this.Init();
				}
				this.buffer.Length = 0;
				this.encoder.StartAttribute(false);
				this.encoder.Write(value);
				this.encoder.EndAttribute();
				return this.buffer.ToString();
			}

			// Token: 0x06003213 RID: 12819 RVA: 0x001066AC File Offset: 0x001048AC
			public string AtributeOuterXml(string name, string value)
			{
				if (this.encoder == null)
				{
					this.Init();
				}
				this.buffer.Length = 0;
				this.buffer.Append(name);
				this.buffer.Append('=');
				this.buffer.Append(this.QuoteChar);
				this.encoder.StartAttribute(false);
				this.encoder.Write(value);
				this.encoder.EndAttribute();
				this.buffer.Append(this.QuoteChar);
				return this.buffer.ToString();
			}

			// Token: 0x17000A87 RID: 2695
			// (get) Token: 0x06003214 RID: 12820 RVA: 0x000297BA File Offset: 0x000279BA
			public char QuoteChar
			{
				get
				{
					return '"';
				}
			}

			// Token: 0x06003215 RID: 12821 RVA: 0x00002103 File Offset: 0x00000303
			public XmlEncoder()
			{
			}

			// Token: 0x040020FF RID: 8447
			private StringBuilder buffer;

			// Token: 0x04002100 RID: 8448
			private XmlTextEncoder encoder;
		}
	}
}
