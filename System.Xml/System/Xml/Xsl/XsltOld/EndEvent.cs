﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004E6 RID: 1254
	internal class EndEvent : Event
	{
		// Token: 0x060030D5 RID: 12501 RVA: 0x00101F8A File Offset: 0x0010018A
		internal EndEvent(XPathNodeType nodeType)
		{
			this.nodeType = nodeType;
		}

		// Token: 0x060030D6 RID: 12502 RVA: 0x00101F99 File Offset: 0x00100199
		public override bool Output(Processor processor, ActionFrame frame)
		{
			return processor.EndEvent(this.nodeType);
		}

		// Token: 0x04002050 RID: 8272
		private XPathNodeType nodeType;
	}
}
