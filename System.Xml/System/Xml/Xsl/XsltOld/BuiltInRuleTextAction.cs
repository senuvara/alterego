﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200051B RID: 1307
	internal class BuiltInRuleTextAction : Action
	{
		// Token: 0x060032E8 RID: 13032 RVA: 0x0010A404 File Offset: 0x00108604
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 2)
				{
					return;
				}
				processor.TextEvent(frame.StoredOutput);
				frame.Finished();
				return;
			}
			else
			{
				string text = processor.ValueOf(frame.NodeSet.Current);
				if (processor.TextEvent(text, false))
				{
					frame.Finished();
					return;
				}
				frame.StoredOutput = text;
				frame.State = 2;
				return;
			}
		}

		// Token: 0x060032E9 RID: 13033 RVA: 0x000FE6D5 File Offset: 0x000FC8D5
		public BuiltInRuleTextAction()
		{
		}

		// Token: 0x040021BD RID: 8637
		private const int ResultStored = 2;
	}
}
