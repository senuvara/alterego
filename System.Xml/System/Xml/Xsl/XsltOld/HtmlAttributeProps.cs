﻿using System;
using System.Collections;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004EA RID: 1258
	internal class HtmlAttributeProps
	{
		// Token: 0x060030ED RID: 12525 RVA: 0x001028D6 File Offset: 0x00100AD6
		public static HtmlAttributeProps Create(bool abr, bool uri, bool name)
		{
			return new HtmlAttributeProps
			{
				abr = abr,
				uri = uri,
				name = name
			};
		}

		// Token: 0x17000A2F RID: 2607
		// (get) Token: 0x060030EE RID: 12526 RVA: 0x001028F2 File Offset: 0x00100AF2
		public bool Abr
		{
			get
			{
				return this.abr;
			}
		}

		// Token: 0x17000A30 RID: 2608
		// (get) Token: 0x060030EF RID: 12527 RVA: 0x001028FA File Offset: 0x00100AFA
		public bool Uri
		{
			get
			{
				return this.uri;
			}
		}

		// Token: 0x17000A31 RID: 2609
		// (get) Token: 0x060030F0 RID: 12528 RVA: 0x00102902 File Offset: 0x00100B02
		public bool Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x060030F1 RID: 12529 RVA: 0x0010290A File Offset: 0x00100B0A
		public static HtmlAttributeProps GetProps(string name)
		{
			return (HtmlAttributeProps)HtmlAttributeProps.s_table[name];
		}

		// Token: 0x060030F2 RID: 12530 RVA: 0x0010291C File Offset: 0x00100B1C
		private static Hashtable CreatePropsTable()
		{
			bool flag = false;
			bool flag2 = true;
			return new Hashtable(26, StringComparer.OrdinalIgnoreCase)
			{
				{
					"action",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"checked",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"cite",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"classid",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"codebase",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"compact",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"data",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"datasrc",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"declare",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"defer",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"disabled",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"for",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"href",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"ismap",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"longdesc",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"multiple",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"name",
					HtmlAttributeProps.Create(flag, flag, flag2)
				},
				{
					"nohref",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"noresize",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"noshade",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"nowrap",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"profile",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"readonly",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"selected",
					HtmlAttributeProps.Create(flag2, flag, flag)
				},
				{
					"src",
					HtmlAttributeProps.Create(flag, flag2, flag)
				},
				{
					"usemap",
					HtmlAttributeProps.Create(flag, flag2, flag)
				}
			};
		}

		// Token: 0x060030F3 RID: 12531 RVA: 0x00002103 File Offset: 0x00000303
		public HtmlAttributeProps()
		{
		}

		// Token: 0x060030F4 RID: 12532 RVA: 0x00102B27 File Offset: 0x00100D27
		// Note: this type is marked as 'beforefieldinit'.
		static HtmlAttributeProps()
		{
		}

		// Token: 0x0400205F RID: 8287
		private bool abr;

		// Token: 0x04002060 RID: 8288
		private bool uri;

		// Token: 0x04002061 RID: 8289
		private bool name;

		// Token: 0x04002062 RID: 8290
		private static Hashtable s_table = HtmlAttributeProps.CreatePropsTable();
	}
}
