﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld.Debugger
{
	// Token: 0x02000531 RID: 1329
	internal interface IXsltDebugger
	{
		// Token: 0x06003359 RID: 13145
		string GetBuiltInTemplatesUri();

		// Token: 0x0600335A RID: 13146
		void OnInstructionCompile(XPathNavigator styleSheetNavigator);

		// Token: 0x0600335B RID: 13147
		void OnInstructionExecute(IXsltProcessor xsltProcessor);
	}
}
