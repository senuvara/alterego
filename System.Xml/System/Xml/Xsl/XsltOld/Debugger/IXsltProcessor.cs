﻿using System;

namespace System.Xml.Xsl.XsltOld.Debugger
{
	// Token: 0x02000530 RID: 1328
	internal interface IXsltProcessor
	{
		// Token: 0x17000AC4 RID: 2756
		// (get) Token: 0x06003357 RID: 13143
		int StackDepth { get; }

		// Token: 0x06003358 RID: 13144
		IStackFrame GetStackFrame(int depth);
	}
}
