﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld.Debugger
{
	// Token: 0x0200052F RID: 1327
	internal interface IStackFrame
	{
		// Token: 0x17000AC2 RID: 2754
		// (get) Token: 0x06003352 RID: 13138
		XPathNavigator Instruction { get; }

		// Token: 0x17000AC3 RID: 2755
		// (get) Token: 0x06003353 RID: 13139
		XPathNodeIterator NodeSet { get; }

		// Token: 0x06003354 RID: 13140
		int GetVariablesCount();

		// Token: 0x06003355 RID: 13141
		XPathNavigator GetVariable(int varIndex);

		// Token: 0x06003356 RID: 13142
		object GetVariableValue(int varIndex);
	}
}
