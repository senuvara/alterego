﻿using System;
using System.Collections;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200050C RID: 1292
	internal class Stylesheet
	{
		// Token: 0x17000A98 RID: 2712
		// (get) Token: 0x06003291 RID: 12945 RVA: 0x001090B8 File Offset: 0x001072B8
		internal bool Whitespace
		{
			get
			{
				return this.whitespace;
			}
		}

		// Token: 0x17000A99 RID: 2713
		// (get) Token: 0x06003292 RID: 12946 RVA: 0x001090C0 File Offset: 0x001072C0
		internal ArrayList Imports
		{
			get
			{
				return this.imports;
			}
		}

		// Token: 0x17000A9A RID: 2714
		// (get) Token: 0x06003293 RID: 12947 RVA: 0x001090C8 File Offset: 0x001072C8
		internal Hashtable AttributeSetTable
		{
			get
			{
				return this.attributeSetTable;
			}
		}

		// Token: 0x06003294 RID: 12948 RVA: 0x001090D0 File Offset: 0x001072D0
		internal void AddSpace(Compiler compiler, string query, double Priority, bool PreserveSpace)
		{
			Stylesheet.WhitespaceElement whitespaceElement;
			if (this.queryKeyTable != null)
			{
				if (this.queryKeyTable.Contains(query))
				{
					whitespaceElement = (Stylesheet.WhitespaceElement)this.queryKeyTable[query];
					whitespaceElement.ReplaceValue(PreserveSpace);
					return;
				}
			}
			else
			{
				this.queryKeyTable = new Hashtable();
				this.whitespaceList = new ArrayList();
			}
			whitespaceElement = new Stylesheet.WhitespaceElement(compiler.AddQuery(query), Priority, PreserveSpace);
			this.queryKeyTable[query] = whitespaceElement;
			this.whitespaceList.Add(whitespaceElement);
		}

		// Token: 0x06003295 RID: 12949 RVA: 0x00109150 File Offset: 0x00107350
		internal void SortWhiteSpace()
		{
			if (this.queryKeyTable != null)
			{
				for (int i = 0; i < this.whitespaceList.Count; i++)
				{
					for (int j = this.whitespaceList.Count - 1; j > i; j--)
					{
						Stylesheet.WhitespaceElement whitespaceElement = (Stylesheet.WhitespaceElement)this.whitespaceList[j - 1];
						Stylesheet.WhitespaceElement whitespaceElement2 = (Stylesheet.WhitespaceElement)this.whitespaceList[j];
						if (whitespaceElement2.Priority < whitespaceElement.Priority)
						{
							this.whitespaceList[j - 1] = whitespaceElement2;
							this.whitespaceList[j] = whitespaceElement;
						}
					}
				}
				this.whitespace = true;
			}
			if (this.imports != null)
			{
				for (int k = this.imports.Count - 1; k >= 0; k--)
				{
					Stylesheet stylesheet = (Stylesheet)this.imports[k];
					if (stylesheet.Whitespace)
					{
						stylesheet.SortWhiteSpace();
						this.whitespace = true;
					}
				}
			}
		}

		// Token: 0x06003296 RID: 12950 RVA: 0x0010923C File Offset: 0x0010743C
		internal bool PreserveWhiteSpace(Processor proc, XPathNavigator node)
		{
			if (this.whitespaceList != null)
			{
				int num = this.whitespaceList.Count - 1;
				while (0 <= num)
				{
					Stylesheet.WhitespaceElement whitespaceElement = (Stylesheet.WhitespaceElement)this.whitespaceList[num];
					if (proc.Matches(node, whitespaceElement.Key))
					{
						return whitespaceElement.PreserveSpace;
					}
					num--;
				}
			}
			if (this.imports != null)
			{
				for (int i = this.imports.Count - 1; i >= 0; i--)
				{
					if (!((Stylesheet)this.imports[i]).PreserveWhiteSpace(proc, node))
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06003297 RID: 12951 RVA: 0x001092D0 File Offset: 0x001074D0
		internal void AddAttributeSet(AttributeSetAction attributeSet)
		{
			if (this.attributeSetTable == null)
			{
				this.attributeSetTable = new Hashtable();
			}
			if (!this.attributeSetTable.ContainsKey(attributeSet.Name))
			{
				this.attributeSetTable[attributeSet.Name] = attributeSet;
				return;
			}
			((AttributeSetAction)this.attributeSetTable[attributeSet.Name]).Merge(attributeSet);
		}

		// Token: 0x06003298 RID: 12952 RVA: 0x00109334 File Offset: 0x00107534
		internal void AddTemplate(TemplateAction template)
		{
			XmlQualifiedName xmlQualifiedName = template.Mode;
			if (template.Name != null)
			{
				if (this.templateNameTable.ContainsKey(template.Name))
				{
					throw XsltException.Create("'{0}' is a duplicate template name.", new string[]
					{
						template.Name.ToString()
					});
				}
				this.templateNameTable[template.Name] = template;
			}
			if (template.MatchKey != -1)
			{
				if (this.modeManagers == null)
				{
					this.modeManagers = new Hashtable();
				}
				if (xmlQualifiedName == null)
				{
					xmlQualifiedName = XmlQualifiedName.Empty;
				}
				TemplateManager templateManager = (TemplateManager)this.modeManagers[xmlQualifiedName];
				if (templateManager == null)
				{
					templateManager = new TemplateManager(this, xmlQualifiedName);
					this.modeManagers[xmlQualifiedName] = templateManager;
					if (xmlQualifiedName.IsEmpty)
					{
						this.templates = templateManager;
					}
				}
				int templateId = this.templateCount + 1;
				this.templateCount = templateId;
				template.TemplateId = templateId;
				templateManager.AddTemplate(template);
			}
		}

		// Token: 0x06003299 RID: 12953 RVA: 0x00109420 File Offset: 0x00107620
		internal void ProcessTemplates()
		{
			if (this.modeManagers != null)
			{
				IDictionaryEnumerator enumerator = this.modeManagers.GetEnumerator();
				while (enumerator.MoveNext())
				{
					((TemplateManager)enumerator.Value).ProcessTemplates();
				}
			}
			if (this.imports != null)
			{
				for (int i = this.imports.Count - 1; i >= 0; i--)
				{
					((Stylesheet)this.imports[i]).ProcessTemplates();
				}
			}
		}

		// Token: 0x0600329A RID: 12954 RVA: 0x00109494 File Offset: 0x00107694
		internal void ReplaceNamespaceAlias(Compiler compiler)
		{
			if (this.modeManagers != null)
			{
				IDictionaryEnumerator enumerator = this.modeManagers.GetEnumerator();
				while (enumerator.MoveNext())
				{
					TemplateManager templateManager = (TemplateManager)enumerator.Value;
					if (templateManager.templates != null)
					{
						for (int i = 0; i < templateManager.templates.Count; i++)
						{
							((TemplateAction)templateManager.templates[i]).ReplaceNamespaceAlias(compiler);
						}
					}
				}
			}
			if (this.templateNameTable != null)
			{
				IDictionaryEnumerator enumerator2 = this.templateNameTable.GetEnumerator();
				while (enumerator2.MoveNext())
				{
					((TemplateAction)enumerator2.Value).ReplaceNamespaceAlias(compiler);
				}
			}
			if (this.imports != null)
			{
				for (int j = this.imports.Count - 1; j >= 0; j--)
				{
					((Stylesheet)this.imports[j]).ReplaceNamespaceAlias(compiler);
				}
			}
		}

		// Token: 0x0600329B RID: 12955 RVA: 0x0010956C File Offset: 0x0010776C
		internal TemplateAction FindTemplate(Processor processor, XPathNavigator navigator, XmlQualifiedName mode)
		{
			TemplateAction templateAction = null;
			if (this.modeManagers != null)
			{
				TemplateManager templateManager = (TemplateManager)this.modeManagers[mode];
				if (templateManager != null)
				{
					templateAction = templateManager.FindTemplate(processor, navigator);
				}
			}
			if (templateAction == null)
			{
				templateAction = this.FindTemplateImports(processor, navigator, mode);
			}
			return templateAction;
		}

		// Token: 0x0600329C RID: 12956 RVA: 0x001095B0 File Offset: 0x001077B0
		internal TemplateAction FindTemplateImports(Processor processor, XPathNavigator navigator, XmlQualifiedName mode)
		{
			TemplateAction templateAction = null;
			if (this.imports != null)
			{
				for (int i = this.imports.Count - 1; i >= 0; i--)
				{
					templateAction = ((Stylesheet)this.imports[i]).FindTemplate(processor, navigator, mode);
					if (templateAction != null)
					{
						return templateAction;
					}
				}
			}
			return templateAction;
		}

		// Token: 0x0600329D RID: 12957 RVA: 0x00109600 File Offset: 0x00107800
		internal TemplateAction FindTemplate(Processor processor, XPathNavigator navigator)
		{
			TemplateAction templateAction = null;
			if (this.templates != null)
			{
				templateAction = this.templates.FindTemplate(processor, navigator);
			}
			if (templateAction == null)
			{
				templateAction = this.FindTemplateImports(processor, navigator);
			}
			return templateAction;
		}

		// Token: 0x0600329E RID: 12958 RVA: 0x00109634 File Offset: 0x00107834
		internal TemplateAction FindTemplate(XmlQualifiedName name)
		{
			TemplateAction templateAction = null;
			if (this.templateNameTable != null)
			{
				templateAction = (TemplateAction)this.templateNameTable[name];
			}
			if (templateAction == null && this.imports != null)
			{
				for (int i = this.imports.Count - 1; i >= 0; i--)
				{
					templateAction = ((Stylesheet)this.imports[i]).FindTemplate(name);
					if (templateAction != null)
					{
						return templateAction;
					}
				}
			}
			return templateAction;
		}

		// Token: 0x0600329F RID: 12959 RVA: 0x001096A0 File Offset: 0x001078A0
		internal TemplateAction FindTemplateImports(Processor processor, XPathNavigator navigator)
		{
			TemplateAction templateAction = null;
			if (this.imports != null)
			{
				for (int i = this.imports.Count - 1; i >= 0; i--)
				{
					templateAction = ((Stylesheet)this.imports[i]).FindTemplate(processor, navigator);
					if (templateAction != null)
					{
						return templateAction;
					}
				}
			}
			return templateAction;
		}

		// Token: 0x17000A9B RID: 2715
		// (get) Token: 0x060032A0 RID: 12960 RVA: 0x001096EE File Offset: 0x001078EE
		internal Hashtable ScriptObjectTypes
		{
			get
			{
				return this.scriptObjectTypes;
			}
		}

		// Token: 0x060032A1 RID: 12961 RVA: 0x001096F6 File Offset: 0x001078F6
		public Stylesheet()
		{
		}

		// Token: 0x04002193 RID: 8595
		private ArrayList imports = new ArrayList();

		// Token: 0x04002194 RID: 8596
		private Hashtable modeManagers;

		// Token: 0x04002195 RID: 8597
		private Hashtable templateNameTable = new Hashtable();

		// Token: 0x04002196 RID: 8598
		private Hashtable attributeSetTable;

		// Token: 0x04002197 RID: 8599
		private int templateCount;

		// Token: 0x04002198 RID: 8600
		private Hashtable queryKeyTable;

		// Token: 0x04002199 RID: 8601
		private ArrayList whitespaceList;

		// Token: 0x0400219A RID: 8602
		private bool whitespace;

		// Token: 0x0400219B RID: 8603
		private Hashtable scriptObjectTypes = new Hashtable();

		// Token: 0x0400219C RID: 8604
		private TemplateManager templates;

		// Token: 0x0200050D RID: 1293
		private class WhitespaceElement
		{
			// Token: 0x17000A9C RID: 2716
			// (get) Token: 0x060032A2 RID: 12962 RVA: 0x0010971F File Offset: 0x0010791F
			internal double Priority
			{
				get
				{
					return this.priority;
				}
			}

			// Token: 0x17000A9D RID: 2717
			// (get) Token: 0x060032A3 RID: 12963 RVA: 0x00109727 File Offset: 0x00107927
			internal int Key
			{
				get
				{
					return this.key;
				}
			}

			// Token: 0x17000A9E RID: 2718
			// (get) Token: 0x060032A4 RID: 12964 RVA: 0x0010972F File Offset: 0x0010792F
			internal bool PreserveSpace
			{
				get
				{
					return this.preserveSpace;
				}
			}

			// Token: 0x060032A5 RID: 12965 RVA: 0x00109737 File Offset: 0x00107937
			internal WhitespaceElement(int Key, double priority, bool PreserveSpace)
			{
				this.key = Key;
				this.priority = priority;
				this.preserveSpace = PreserveSpace;
			}

			// Token: 0x060032A6 RID: 12966 RVA: 0x00109754 File Offset: 0x00107954
			internal void ReplaceValue(bool PreserveSpace)
			{
				this.preserveSpace = PreserveSpace;
			}

			// Token: 0x0400219D RID: 8605
			private int key;

			// Token: 0x0400219E RID: 8606
			private double priority;

			// Token: 0x0400219F RID: 8607
			private bool preserveSpace;
		}
	}
}
