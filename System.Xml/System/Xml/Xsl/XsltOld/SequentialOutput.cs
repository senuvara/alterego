﻿using System;
using System.Collections;
using System.Globalization;
using System.Text;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000508 RID: 1288
	internal abstract class SequentialOutput : RecordOutput
	{
		// Token: 0x0600325A RID: 12890 RVA: 0x00107BF0 File Offset: 0x00105DF0
		private void CacheOuptutProps(XsltOutput output)
		{
			this.output = output;
			this.isXmlOutput = (this.output.Method == XsltOutput.OutputMethod.Xml);
			this.isHtmlOutput = (this.output.Method == XsltOutput.OutputMethod.Html);
			this.cdataElements = this.output.CDataElements;
			this.indentOutput = this.output.Indent;
			this.outputDoctype = (this.output.DoctypeSystem != null || (this.isHtmlOutput && this.output.DoctypePublic != null));
			this.outputXmlDecl = (this.isXmlOutput && !this.output.OmitXmlDeclaration && !this.omitXmlDeclCalled);
		}

		// Token: 0x0600325B RID: 12891 RVA: 0x00107CA4 File Offset: 0x00105EA4
		internal SequentialOutput(Processor processor)
		{
			this.processor = processor;
			this.CacheOuptutProps(processor.Output);
		}

		// Token: 0x0600325C RID: 12892 RVA: 0x00107CD1 File Offset: 0x00105ED1
		public void OmitXmlDecl()
		{
			this.omitXmlDeclCalled = true;
			this.outputXmlDecl = false;
		}

		// Token: 0x0600325D RID: 12893 RVA: 0x00107CE4 File Offset: 0x00105EE4
		private void WriteStartElement(RecordBuilder record)
		{
			BuilderInfo mainNode = record.MainNode;
			HtmlElementProps htmlElementProps = null;
			if (this.isHtmlOutput)
			{
				if (mainNode.Prefix.Length == 0)
				{
					htmlElementProps = mainNode.htmlProps;
					if (htmlElementProps == null && mainNode.search)
					{
						htmlElementProps = HtmlElementProps.GetProps(mainNode.LocalName);
					}
					record.Manager.CurrentElementScope.HtmlElementProps = htmlElementProps;
					mainNode.IsEmptyTag = false;
				}
			}
			else if (this.isXmlOutput && mainNode.Depth == 0)
			{
				if (this.secondRoot && (this.output.DoctypeSystem != null || this.output.Standalone))
				{
					throw XsltException.Create("There are multiple root elements in the output XML.", Array.Empty<string>());
				}
				this.secondRoot = true;
			}
			if (this.outputDoctype)
			{
				this.WriteDoctype(mainNode);
				this.outputDoctype = false;
			}
			if (this.cdataElements != null && this.cdataElements.Contains(new XmlQualifiedName(mainNode.LocalName, mainNode.NamespaceURI)) && this.isXmlOutput)
			{
				record.Manager.CurrentElementScope.ToCData = true;
			}
			this.Indent(record);
			this.Write('<');
			this.WriteName(mainNode.Prefix, mainNode.LocalName);
			this.WriteAttributes(record.AttributeList, record.AttributeCount, htmlElementProps);
			if (mainNode.IsEmptyTag)
			{
				this.Write(" />");
			}
			else
			{
				this.Write('>');
			}
			if (htmlElementProps != null && htmlElementProps.Head)
			{
				BuilderInfo builderInfo = mainNode;
				int depth = builderInfo.Depth;
				builderInfo.Depth = depth + 1;
				this.Indent(record);
				BuilderInfo builderInfo2 = mainNode;
				depth = builderInfo2.Depth;
				builderInfo2.Depth = depth - 1;
				this.Write("<META http-equiv=\"Content-Type\" content=\"");
				this.Write(this.output.MediaType);
				this.Write("; charset=");
				this.Write(this.encoding.WebName);
				this.Write("\">");
			}
		}

		// Token: 0x0600325E RID: 12894 RVA: 0x00107EB0 File Offset: 0x001060B0
		private void WriteTextNode(RecordBuilder record)
		{
			BuilderInfo mainNode = record.MainNode;
			OutputScope currentElementScope = record.Manager.CurrentElementScope;
			currentElementScope.Mixed = true;
			if (currentElementScope.HtmlElementProps != null && currentElementScope.HtmlElementProps.NoEntities)
			{
				this.Write(mainNode.Value);
				return;
			}
			if (currentElementScope.ToCData)
			{
				this.WriteCDataSection(mainNode.Value);
				return;
			}
			this.WriteTextNode(mainNode);
		}

		// Token: 0x0600325F RID: 12895 RVA: 0x00107F18 File Offset: 0x00106118
		private void WriteTextNode(BuilderInfo node)
		{
			for (int i = 0; i < node.TextInfoCount; i++)
			{
				string text = node.TextInfo[i];
				if (text == null)
				{
					i++;
					this.Write(node.TextInfo[i]);
				}
				else
				{
					this.WriteWithReplace(text, SequentialOutput.s_TextValueFind, SequentialOutput.s_TextValueReplace);
				}
			}
		}

		// Token: 0x06003260 RID: 12896 RVA: 0x00107F67 File Offset: 0x00106167
		private void WriteCDataSection(string value)
		{
			this.Write("<![CDATA[");
			this.WriteCData(value);
			this.Write("]]>");
		}

		// Token: 0x06003261 RID: 12897 RVA: 0x00107F88 File Offset: 0x00106188
		private void WriteDoctype(BuilderInfo mainNode)
		{
			this.Indent(0);
			this.Write("<!DOCTYPE ");
			if (this.isXmlOutput)
			{
				this.WriteName(mainNode.Prefix, mainNode.LocalName);
			}
			else
			{
				this.WriteName(string.Empty, "html");
			}
			this.Write(' ');
			if (this.output.DoctypePublic != null)
			{
				this.Write("PUBLIC ");
				this.Write('"');
				this.Write(this.output.DoctypePublic);
				this.Write("\" ");
			}
			else
			{
				this.Write("SYSTEM ");
			}
			if (this.output.DoctypeSystem != null)
			{
				this.Write('"');
				this.Write(this.output.DoctypeSystem);
				this.Write('"');
			}
			this.Write('>');
		}

		// Token: 0x06003262 RID: 12898 RVA: 0x0010805C File Offset: 0x0010625C
		private void WriteXmlDeclaration()
		{
			this.outputXmlDecl = false;
			this.Indent(0);
			this.Write("<?");
			this.WriteName(string.Empty, "xml");
			this.Write(" version=\"1.0\"");
			if (this.encoding != null)
			{
				this.Write(" encoding=\"");
				this.Write(this.encoding.WebName);
				this.Write('"');
			}
			if (this.output.HasStandalone)
			{
				this.Write(" standalone=\"");
				this.Write(this.output.Standalone ? "yes" : "no");
				this.Write('"');
			}
			this.Write("?>");
		}

		// Token: 0x06003263 RID: 12899 RVA: 0x00108113 File Offset: 0x00106313
		private void WriteProcessingInstruction(RecordBuilder record)
		{
			this.Indent(record);
			this.WriteProcessingInstruction(record.MainNode);
		}

		// Token: 0x06003264 RID: 12900 RVA: 0x00108128 File Offset: 0x00106328
		private void WriteProcessingInstruction(BuilderInfo node)
		{
			this.Write("<?");
			this.WriteName(node.Prefix, node.LocalName);
			this.Write(' ');
			this.Write(node.Value);
			if (this.isHtmlOutput)
			{
				this.Write('>');
				return;
			}
			this.Write("?>");
		}

		// Token: 0x06003265 RID: 12901 RVA: 0x00108184 File Offset: 0x00106384
		private void WriteEndElement(RecordBuilder record)
		{
			BuilderInfo mainNode = record.MainNode;
			HtmlElementProps htmlElementProps = record.Manager.CurrentElementScope.HtmlElementProps;
			if (htmlElementProps != null && htmlElementProps.Empty)
			{
				return;
			}
			this.Indent(record);
			this.Write("</");
			this.WriteName(record.MainNode.Prefix, record.MainNode.LocalName);
			this.Write('>');
		}

		// Token: 0x06003266 RID: 12902 RVA: 0x001081EC File Offset: 0x001063EC
		public Processor.OutputResult RecordDone(RecordBuilder record)
		{
			if (this.output.Method == XsltOutput.OutputMethod.Unknown)
			{
				if (!this.DecideDefaultOutput(record.MainNode))
				{
					this.CacheRecord(record);
				}
				else
				{
					this.OutputCachedRecords();
					this.OutputRecord(record);
				}
			}
			else
			{
				this.OutputRecord(record);
			}
			record.Reset();
			return Processor.OutputResult.Continue;
		}

		// Token: 0x06003267 RID: 12903 RVA: 0x0010823B File Offset: 0x0010643B
		public void TheEnd()
		{
			this.OutputCachedRecords();
			this.Close();
		}

		// Token: 0x06003268 RID: 12904 RVA: 0x0010824C File Offset: 0x0010644C
		private bool DecideDefaultOutput(BuilderInfo node)
		{
			XsltOutput.OutputMethod defaultOutput = XsltOutput.OutputMethod.Xml;
			XmlNodeType nodeType = node.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.Text && nodeType - XmlNodeType.Whitespace > 1)
				{
					return false;
				}
				if (this.xmlCharType.IsOnlyWhitespace(node.Value))
				{
					return false;
				}
				defaultOutput = XsltOutput.OutputMethod.Xml;
			}
			else if (node.NamespaceURI.Length == 0 && string.Compare("html", node.LocalName, StringComparison.OrdinalIgnoreCase) == 0)
			{
				defaultOutput = XsltOutput.OutputMethod.Html;
			}
			if (this.processor.SetDefaultOutput(defaultOutput))
			{
				this.CacheOuptutProps(this.processor.Output);
			}
			return true;
		}

		// Token: 0x06003269 RID: 12905 RVA: 0x001082D2 File Offset: 0x001064D2
		private void CacheRecord(RecordBuilder record)
		{
			if (this.outputCache == null)
			{
				this.outputCache = new ArrayList();
			}
			this.outputCache.Add(record.MainNode.Clone());
		}

		// Token: 0x0600326A RID: 12906 RVA: 0x00108300 File Offset: 0x00106500
		private void OutputCachedRecords()
		{
			if (this.outputCache == null)
			{
				return;
			}
			for (int i = 0; i < this.outputCache.Count; i++)
			{
				BuilderInfo node = (BuilderInfo)this.outputCache[i];
				this.OutputRecord(node);
			}
			this.outputCache = null;
		}

		// Token: 0x0600326B RID: 12907 RVA: 0x0010834C File Offset: 0x0010654C
		private void OutputRecord(RecordBuilder record)
		{
			BuilderInfo mainNode = record.MainNode;
			if (this.outputXmlDecl)
			{
				this.WriteXmlDeclaration();
			}
			switch (mainNode.NodeType)
			{
			case XmlNodeType.Element:
				this.WriteStartElement(record);
				return;
			case XmlNodeType.Attribute:
			case XmlNodeType.CDATA:
			case XmlNodeType.Entity:
			case XmlNodeType.Document:
			case XmlNodeType.DocumentFragment:
			case XmlNodeType.Notation:
				break;
			case XmlNodeType.Text:
			case XmlNodeType.Whitespace:
			case XmlNodeType.SignificantWhitespace:
				this.WriteTextNode(record);
				return;
			case XmlNodeType.EntityReference:
				this.Write('&');
				this.WriteName(mainNode.Prefix, mainNode.LocalName);
				this.Write(';');
				return;
			case XmlNodeType.ProcessingInstruction:
				this.WriteProcessingInstruction(record);
				return;
			case XmlNodeType.Comment:
				this.Indent(record);
				this.Write("<!--");
				this.Write(mainNode.Value);
				this.Write("-->");
				return;
			case XmlNodeType.DocumentType:
				this.Write(mainNode.Value);
				return;
			case XmlNodeType.EndElement:
				this.WriteEndElement(record);
				break;
			default:
				return;
			}
		}

		// Token: 0x0600326C RID: 12908 RVA: 0x00108434 File Offset: 0x00106634
		private void OutputRecord(BuilderInfo node)
		{
			if (this.outputXmlDecl)
			{
				this.WriteXmlDeclaration();
			}
			this.Indent(0);
			switch (node.NodeType)
			{
			case XmlNodeType.Element:
			case XmlNodeType.Attribute:
			case XmlNodeType.CDATA:
			case XmlNodeType.Entity:
			case XmlNodeType.Document:
			case XmlNodeType.DocumentFragment:
			case XmlNodeType.Notation:
			case XmlNodeType.EndElement:
				break;
			case XmlNodeType.Text:
			case XmlNodeType.Whitespace:
			case XmlNodeType.SignificantWhitespace:
				this.WriteTextNode(node);
				return;
			case XmlNodeType.EntityReference:
				this.Write('&');
				this.WriteName(node.Prefix, node.LocalName);
				this.Write(';');
				return;
			case XmlNodeType.ProcessingInstruction:
				this.WriteProcessingInstruction(node);
				return;
			case XmlNodeType.Comment:
				this.Write("<!--");
				this.Write(node.Value);
				this.Write("-->");
				return;
			case XmlNodeType.DocumentType:
				this.Write(node.Value);
				break;
			default:
				return;
			}
		}

		// Token: 0x0600326D RID: 12909 RVA: 0x00108504 File Offset: 0x00106704
		private void WriteName(string prefix, string name)
		{
			if (prefix != null && prefix.Length > 0)
			{
				this.Write(prefix);
				if (name == null || name.Length <= 0)
				{
					return;
				}
				this.Write(':');
			}
			this.Write(name);
		}

		// Token: 0x0600326E RID: 12910 RVA: 0x00108537 File Offset: 0x00106737
		private void WriteXmlAttributeValue(string value)
		{
			this.WriteWithReplace(value, SequentialOutput.s_XmlAttributeValueFind, SequentialOutput.s_XmlAttributeValueReplace);
		}

		// Token: 0x0600326F RID: 12911 RVA: 0x0010854C File Offset: 0x0010674C
		private void WriteHtmlAttributeValue(string value)
		{
			int length = value.Length;
			int i = 0;
			while (i < length)
			{
				char c = value[i];
				i++;
				if (c != '"')
				{
					if (c == '&')
					{
						if (i != length && value[i] == '{')
						{
							this.Write(c);
						}
						else
						{
							this.Write("&amp;");
						}
					}
					else
					{
						this.Write(c);
					}
				}
				else
				{
					this.Write("&quot;");
				}
			}
		}

		// Token: 0x06003270 RID: 12912 RVA: 0x001085B8 File Offset: 0x001067B8
		private void WriteHtmlUri(string value)
		{
			int length = value.Length;
			int i = 0;
			while (i < length)
			{
				char c = value[i];
				i++;
				if (c <= '\r')
				{
					if (c == '\n')
					{
						this.Write("&#xA;");
						continue;
					}
					if (c == '\r')
					{
						this.Write("&#xD;");
						continue;
					}
				}
				else
				{
					if (c == '"')
					{
						this.Write("&quot;");
						continue;
					}
					if (c == '&')
					{
						if (i != length && value[i] == '{')
						{
							this.Write(c);
							continue;
						}
						this.Write("&amp;");
						continue;
					}
				}
				if ('\u007f' < c)
				{
					if (this.utf8Encoding == null)
					{
						this.utf8Encoding = Encoding.UTF8;
						this.byteBuffer = new byte[this.utf8Encoding.GetMaxByteCount(1)];
					}
					int bytes = this.utf8Encoding.GetBytes(value, i - 1, 1, this.byteBuffer, 0);
					for (int j = 0; j < bytes; j++)
					{
						this.Write("%");
						uint num = (uint)this.byteBuffer[j];
						this.Write(num.ToString("X2", CultureInfo.InvariantCulture));
					}
				}
				else
				{
					this.Write(c);
				}
			}
		}

		// Token: 0x06003271 RID: 12913 RVA: 0x001086EC File Offset: 0x001068EC
		private void WriteWithReplace(string value, char[] find, string[] replace)
		{
			int length = value.Length;
			int i;
			for (i = 0; i < length; i++)
			{
				int num = value.IndexOfAny(find, i);
				if (num == -1)
				{
					break;
				}
				while (i < num)
				{
					this.Write(value[i]);
					i++;
				}
				char c = value[i];
				int num2 = find.Length - 1;
				while (0 <= num2)
				{
					if (find[num2] == c)
					{
						this.Write(replace[num2]);
						break;
					}
					num2--;
				}
			}
			if (i == 0)
			{
				this.Write(value);
				return;
			}
			while (i < length)
			{
				this.Write(value[i]);
				i++;
			}
		}

		// Token: 0x06003272 RID: 12914 RVA: 0x0010877F File Offset: 0x0010697F
		private void WriteCData(string value)
		{
			this.Write(value.Replace("]]>", "]]]]><![CDATA[>"));
		}

		// Token: 0x06003273 RID: 12915 RVA: 0x00108798 File Offset: 0x00106998
		private void WriteAttributes(ArrayList list, int count, HtmlElementProps htmlElementsProps)
		{
			for (int i = 0; i < count; i++)
			{
				BuilderInfo builderInfo = (BuilderInfo)list[i];
				string value = builderInfo.Value;
				bool flag = false;
				bool flag2 = false;
				if (htmlElementsProps != null && builderInfo.Prefix.Length == 0)
				{
					HtmlAttributeProps htmlAttributeProps = builderInfo.htmlAttrProps;
					if (htmlAttributeProps == null && builderInfo.search)
					{
						htmlAttributeProps = HtmlAttributeProps.GetProps(builderInfo.LocalName);
					}
					if (htmlAttributeProps != null)
					{
						flag = (htmlElementsProps.AbrParent && htmlAttributeProps.Abr);
						flag2 = (htmlElementsProps.UriParent && (htmlAttributeProps.Uri || (htmlElementsProps.NameParent && htmlAttributeProps.Name)));
					}
				}
				this.Write(' ');
				this.WriteName(builderInfo.Prefix, builderInfo.LocalName);
				if (!flag || string.Compare(builderInfo.LocalName, value, StringComparison.OrdinalIgnoreCase) != 0)
				{
					this.Write("=\"");
					if (flag2)
					{
						this.WriteHtmlUri(value);
					}
					else if (this.isHtmlOutput)
					{
						this.WriteHtmlAttributeValue(value);
					}
					else
					{
						this.WriteXmlAttributeValue(value);
					}
					this.Write('"');
				}
			}
		}

		// Token: 0x06003274 RID: 12916 RVA: 0x001088A7 File Offset: 0x00106AA7
		private void Indent(RecordBuilder record)
		{
			if (!record.Manager.CurrentElementScope.Mixed)
			{
				this.Indent(record.MainNode.Depth);
			}
		}

		// Token: 0x06003275 RID: 12917 RVA: 0x001088CC File Offset: 0x00106ACC
		private void Indent(int depth)
		{
			if (this.firstLine)
			{
				if (this.indentOutput)
				{
					this.firstLine = false;
				}
				return;
			}
			this.Write("\r\n");
			int num = 2 * depth;
			while (0 < num)
			{
				this.Write(" ");
				num--;
			}
		}

		// Token: 0x06003276 RID: 12918
		internal abstract void Write(char outputChar);

		// Token: 0x06003277 RID: 12919
		internal abstract void Write(string outputText);

		// Token: 0x06003278 RID: 12920
		internal abstract void Close();

		// Token: 0x06003279 RID: 12921 RVA: 0x00108918 File Offset: 0x00106B18
		// Note: this type is marked as 'beforefieldinit'.
		static SequentialOutput()
		{
		}

		// Token: 0x04002129 RID: 8489
		private const char s_Colon = ':';

		// Token: 0x0400212A RID: 8490
		private const char s_GreaterThan = '>';

		// Token: 0x0400212B RID: 8491
		private const char s_LessThan = '<';

		// Token: 0x0400212C RID: 8492
		private const char s_Space = ' ';

		// Token: 0x0400212D RID: 8493
		private const char s_Quote = '"';

		// Token: 0x0400212E RID: 8494
		private const char s_Semicolon = ';';

		// Token: 0x0400212F RID: 8495
		private const char s_NewLine = '\n';

		// Token: 0x04002130 RID: 8496
		private const char s_Return = '\r';

		// Token: 0x04002131 RID: 8497
		private const char s_Ampersand = '&';

		// Token: 0x04002132 RID: 8498
		private const string s_LessThanQuestion = "<?";

		// Token: 0x04002133 RID: 8499
		private const string s_QuestionGreaterThan = "?>";

		// Token: 0x04002134 RID: 8500
		private const string s_LessThanSlash = "</";

		// Token: 0x04002135 RID: 8501
		private const string s_SlashGreaterThan = " />";

		// Token: 0x04002136 RID: 8502
		private const string s_EqualQuote = "=\"";

		// Token: 0x04002137 RID: 8503
		private const string s_DocType = "<!DOCTYPE ";

		// Token: 0x04002138 RID: 8504
		private const string s_CommentBegin = "<!--";

		// Token: 0x04002139 RID: 8505
		private const string s_CommentEnd = "-->";

		// Token: 0x0400213A RID: 8506
		private const string s_CDataBegin = "<![CDATA[";

		// Token: 0x0400213B RID: 8507
		private const string s_CDataEnd = "]]>";

		// Token: 0x0400213C RID: 8508
		private const string s_VersionAll = " version=\"1.0\"";

		// Token: 0x0400213D RID: 8509
		private const string s_Standalone = " standalone=\"";

		// Token: 0x0400213E RID: 8510
		private const string s_EncodingStart = " encoding=\"";

		// Token: 0x0400213F RID: 8511
		private const string s_Public = "PUBLIC ";

		// Token: 0x04002140 RID: 8512
		private const string s_System = "SYSTEM ";

		// Token: 0x04002141 RID: 8513
		private const string s_Html = "html";

		// Token: 0x04002142 RID: 8514
		private const string s_QuoteSpace = "\" ";

		// Token: 0x04002143 RID: 8515
		private const string s_CDataSplit = "]]]]><![CDATA[>";

		// Token: 0x04002144 RID: 8516
		private const string s_EnLessThan = "&lt;";

		// Token: 0x04002145 RID: 8517
		private const string s_EnGreaterThan = "&gt;";

		// Token: 0x04002146 RID: 8518
		private const string s_EnAmpersand = "&amp;";

		// Token: 0x04002147 RID: 8519
		private const string s_EnQuote = "&quot;";

		// Token: 0x04002148 RID: 8520
		private const string s_EnNewLine = "&#xA;";

		// Token: 0x04002149 RID: 8521
		private const string s_EnReturn = "&#xD;";

		// Token: 0x0400214A RID: 8522
		private const string s_EndOfLine = "\r\n";

		// Token: 0x0400214B RID: 8523
		private static char[] s_TextValueFind = new char[]
		{
			'&',
			'>',
			'<'
		};

		// Token: 0x0400214C RID: 8524
		private static string[] s_TextValueReplace = new string[]
		{
			"&amp;",
			"&gt;",
			"&lt;"
		};

		// Token: 0x0400214D RID: 8525
		private static char[] s_XmlAttributeValueFind = new char[]
		{
			'&',
			'>',
			'<',
			'"',
			'\n',
			'\r'
		};

		// Token: 0x0400214E RID: 8526
		private static string[] s_XmlAttributeValueReplace = new string[]
		{
			"&amp;",
			"&gt;",
			"&lt;",
			"&quot;",
			"&#xA;",
			"&#xD;"
		};

		// Token: 0x0400214F RID: 8527
		private Processor processor;

		// Token: 0x04002150 RID: 8528
		protected Encoding encoding;

		// Token: 0x04002151 RID: 8529
		private ArrayList outputCache;

		// Token: 0x04002152 RID: 8530
		private bool firstLine = true;

		// Token: 0x04002153 RID: 8531
		private bool secondRoot;

		// Token: 0x04002154 RID: 8532
		private XsltOutput output;

		// Token: 0x04002155 RID: 8533
		private bool isHtmlOutput;

		// Token: 0x04002156 RID: 8534
		private bool isXmlOutput;

		// Token: 0x04002157 RID: 8535
		private Hashtable cdataElements;

		// Token: 0x04002158 RID: 8536
		private bool indentOutput;

		// Token: 0x04002159 RID: 8537
		private bool outputDoctype;

		// Token: 0x0400215A RID: 8538
		private bool outputXmlDecl;

		// Token: 0x0400215B RID: 8539
		private bool omitXmlDeclCalled;

		// Token: 0x0400215C RID: 8540
		private byte[] byteBuffer;

		// Token: 0x0400215D RID: 8541
		private Encoding utf8Encoding;

		// Token: 0x0400215E RID: 8542
		private XmlCharType xmlCharType = XmlCharType.Instance;
	}
}
