﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004F9 RID: 1273
	internal class OutputScope : DocumentScope
	{
		// Token: 0x17000A55 RID: 2645
		// (get) Token: 0x0600316B RID: 12651 RVA: 0x00104652 File Offset: 0x00102852
		internal string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000A56 RID: 2646
		// (get) Token: 0x0600316C RID: 12652 RVA: 0x0010465A File Offset: 0x0010285A
		internal string Namespace
		{
			get
			{
				return this.nsUri;
			}
		}

		// Token: 0x17000A57 RID: 2647
		// (get) Token: 0x0600316D RID: 12653 RVA: 0x00104662 File Offset: 0x00102862
		// (set) Token: 0x0600316E RID: 12654 RVA: 0x0010466A File Offset: 0x0010286A
		internal string Prefix
		{
			get
			{
				return this.prefix;
			}
			set
			{
				this.prefix = value;
			}
		}

		// Token: 0x17000A58 RID: 2648
		// (get) Token: 0x0600316F RID: 12655 RVA: 0x00104673 File Offset: 0x00102873
		// (set) Token: 0x06003170 RID: 12656 RVA: 0x0010467B File Offset: 0x0010287B
		internal XmlSpace Space
		{
			get
			{
				return this.space;
			}
			set
			{
				this.space = value;
			}
		}

		// Token: 0x17000A59 RID: 2649
		// (get) Token: 0x06003171 RID: 12657 RVA: 0x00104684 File Offset: 0x00102884
		// (set) Token: 0x06003172 RID: 12658 RVA: 0x0010468C File Offset: 0x0010288C
		internal string Lang
		{
			get
			{
				return this.lang;
			}
			set
			{
				this.lang = value;
			}
		}

		// Token: 0x17000A5A RID: 2650
		// (get) Token: 0x06003173 RID: 12659 RVA: 0x00104695 File Offset: 0x00102895
		// (set) Token: 0x06003174 RID: 12660 RVA: 0x0010469D File Offset: 0x0010289D
		internal bool Mixed
		{
			get
			{
				return this.mixed;
			}
			set
			{
				this.mixed = value;
			}
		}

		// Token: 0x17000A5B RID: 2651
		// (get) Token: 0x06003175 RID: 12661 RVA: 0x001046A6 File Offset: 0x001028A6
		// (set) Token: 0x06003176 RID: 12662 RVA: 0x001046AE File Offset: 0x001028AE
		internal bool ToCData
		{
			get
			{
				return this.toCData;
			}
			set
			{
				this.toCData = value;
			}
		}

		// Token: 0x17000A5C RID: 2652
		// (get) Token: 0x06003177 RID: 12663 RVA: 0x001046B7 File Offset: 0x001028B7
		// (set) Token: 0x06003178 RID: 12664 RVA: 0x001046BF File Offset: 0x001028BF
		internal HtmlElementProps HtmlElementProps
		{
			get
			{
				return this.htmlElementProps;
			}
			set
			{
				this.htmlElementProps = value;
			}
		}

		// Token: 0x06003179 RID: 12665 RVA: 0x001046C8 File Offset: 0x001028C8
		internal OutputScope()
		{
			this.Init(string.Empty, string.Empty, string.Empty, XmlSpace.None, string.Empty, false);
		}

		// Token: 0x0600317A RID: 12666 RVA: 0x001046EC File Offset: 0x001028EC
		internal void Init(string name, string nspace, string prefix, XmlSpace space, string lang, bool mixed)
		{
			this.scopes = null;
			this.name = name;
			this.nsUri = nspace;
			this.prefix = prefix;
			this.space = space;
			this.lang = lang;
			this.mixed = mixed;
			this.toCData = false;
			this.htmlElementProps = null;
		}

		// Token: 0x0600317B RID: 12667 RVA: 0x0010473C File Offset: 0x0010293C
		internal bool FindPrefix(string urn, out string prefix)
		{
			for (NamespaceDecl namespaceDecl = this.scopes; namespaceDecl != null; namespaceDecl = namespaceDecl.Next)
			{
				if (Ref.Equal(namespaceDecl.Uri, urn) && namespaceDecl.Prefix != null && namespaceDecl.Prefix.Length > 0)
				{
					prefix = namespaceDecl.Prefix;
					return true;
				}
			}
			prefix = string.Empty;
			return false;
		}

		// Token: 0x040020AE RID: 8366
		private string name;

		// Token: 0x040020AF RID: 8367
		private string nsUri;

		// Token: 0x040020B0 RID: 8368
		private string prefix;

		// Token: 0x040020B1 RID: 8369
		private XmlSpace space;

		// Token: 0x040020B2 RID: 8370
		private string lang;

		// Token: 0x040020B3 RID: 8371
		private bool mixed;

		// Token: 0x040020B4 RID: 8372
		private bool toCData;

		// Token: 0x040020B5 RID: 8373
		private HtmlElementProps htmlElementProps;
	}
}
