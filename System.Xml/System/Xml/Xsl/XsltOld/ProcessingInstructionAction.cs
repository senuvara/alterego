﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004FC RID: 1276
	internal class ProcessingInstructionAction : ContainerAction
	{
		// Token: 0x0600318D RID: 12685 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		internal ProcessingInstructionAction()
		{
		}

		// Token: 0x0600318E RID: 12686 RVA: 0x00104B60 File Offset: 0x00102D60
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.nameAvt, "name");
			if (this.nameAvt.IsConstant)
			{
				this.name = this.nameAvt.Evaluate(null, null);
				this.nameAvt = null;
				if (!ProcessingInstructionAction.IsProcessingInstructionName(this.name))
				{
					this.name = null;
				}
			}
			if (compiler.Recurse())
			{
				base.CompileTemplate(compiler);
				compiler.ToParent();
			}
		}

		// Token: 0x0600318F RID: 12687 RVA: 0x00104BD8 File Offset: 0x00102DD8
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Name))
			{
				this.nameAvt = Avt.CompileAvt(compiler, value);
				return true;
			}
			return false;
		}

		// Token: 0x06003190 RID: 12688 RVA: 0x00104C20 File Offset: 0x00102E20
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			switch (frame.State)
			{
			case 0:
				if (this.nameAvt == null)
				{
					frame.StoredOutput = this.name;
					if (this.name == null)
					{
						frame.Finished();
						return;
					}
				}
				else
				{
					frame.StoredOutput = this.nameAvt.Evaluate(processor, frame);
					if (!ProcessingInstructionAction.IsProcessingInstructionName(frame.StoredOutput))
					{
						frame.Finished();
						return;
					}
				}
				break;
			case 1:
				if (!processor.EndEvent(XPathNodeType.ProcessingInstruction))
				{
					frame.State = 1;
					return;
				}
				frame.Finished();
				return;
			case 2:
				goto IL_B5;
			case 3:
				break;
			default:
				goto IL_B5;
			}
			if (!processor.BeginEvent(XPathNodeType.ProcessingInstruction, string.Empty, frame.StoredOutput, string.Empty, false))
			{
				frame.State = 3;
				return;
			}
			processor.PushActionFrame(frame);
			frame.State = 1;
			return;
			IL_B5:
			frame.Finished();
		}

		// Token: 0x06003191 RID: 12689 RVA: 0x00104CE8 File Offset: 0x00102EE8
		internal static bool IsProcessingInstructionName(string name)
		{
			if (name == null)
			{
				return false;
			}
			int length = name.Length;
			int num = 0;
			XmlCharType instance = XmlCharType.Instance;
			while (num < length && instance.IsWhiteSpace(name[num]))
			{
				num++;
			}
			if (num >= length)
			{
				return false;
			}
			int num2 = ValidateNames.ParseNCName(name, num);
			if (num2 == 0)
			{
				return false;
			}
			num += num2;
			while (num < length && instance.IsWhiteSpace(name[num]))
			{
				num++;
			}
			return num >= length && (length != 3 || (name[0] != 'X' && name[0] != 'x') || (name[1] != 'M' && name[1] != 'm') || (name[2] != 'L' && name[2] != 'l'));
		}

		// Token: 0x040020BF RID: 8383
		private const int NameEvaluated = 2;

		// Token: 0x040020C0 RID: 8384
		private const int NameReady = 3;

		// Token: 0x040020C1 RID: 8385
		private Avt nameAvt;

		// Token: 0x040020C2 RID: 8386
		private string name;

		// Token: 0x040020C3 RID: 8387
		private const char CharX = 'X';

		// Token: 0x040020C4 RID: 8388
		private const char Charx = 'x';

		// Token: 0x040020C5 RID: 8389
		private const char CharM = 'M';

		// Token: 0x040020C6 RID: 8390
		private const char Charm = 'm';

		// Token: 0x040020C7 RID: 8391
		private const char CharL = 'L';

		// Token: 0x040020C8 RID: 8392
		private const char Charl = 'l';
	}
}
