﻿using System;
using System.Collections;
using System.Text;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000503 RID: 1283
	internal sealed class RecordBuilder
	{
		// Token: 0x06003216 RID: 12822 RVA: 0x00106740 File Offset: 0x00104940
		internal RecordBuilder(RecordOutput output, XmlNameTable nameTable)
		{
			this.output = output;
			this.nameTable = ((nameTable != null) ? nameTable : new NameTable());
			this.atoms = new OutKeywords(this.nameTable);
			this.scopeManager = new OutputScopeManager(this.nameTable, this.atoms);
		}

		// Token: 0x17000A88 RID: 2696
		// (get) Token: 0x06003217 RID: 12823 RVA: 0x001067BF File Offset: 0x001049BF
		// (set) Token: 0x06003218 RID: 12824 RVA: 0x001067C7 File Offset: 0x001049C7
		internal int OutputState
		{
			get
			{
				return this.outputState;
			}
			set
			{
				this.outputState = value;
			}
		}

		// Token: 0x17000A89 RID: 2697
		// (get) Token: 0x06003219 RID: 12825 RVA: 0x001067D0 File Offset: 0x001049D0
		// (set) Token: 0x0600321A RID: 12826 RVA: 0x001067D8 File Offset: 0x001049D8
		internal RecordBuilder Next
		{
			get
			{
				return this.next;
			}
			set
			{
				this.next = value;
			}
		}

		// Token: 0x17000A8A RID: 2698
		// (get) Token: 0x0600321B RID: 12827 RVA: 0x001067E1 File Offset: 0x001049E1
		internal RecordOutput Output
		{
			get
			{
				return this.output;
			}
		}

		// Token: 0x17000A8B RID: 2699
		// (get) Token: 0x0600321C RID: 12828 RVA: 0x001067E9 File Offset: 0x001049E9
		internal BuilderInfo MainNode
		{
			get
			{
				return this.mainNode;
			}
		}

		// Token: 0x17000A8C RID: 2700
		// (get) Token: 0x0600321D RID: 12829 RVA: 0x001067F1 File Offset: 0x001049F1
		internal ArrayList AttributeList
		{
			get
			{
				return this.attributeList;
			}
		}

		// Token: 0x17000A8D RID: 2701
		// (get) Token: 0x0600321E RID: 12830 RVA: 0x001067F9 File Offset: 0x001049F9
		internal int AttributeCount
		{
			get
			{
				return this.attributeCount;
			}
		}

		// Token: 0x17000A8E RID: 2702
		// (get) Token: 0x0600321F RID: 12831 RVA: 0x00106801 File Offset: 0x00104A01
		internal OutputScopeManager Manager
		{
			get
			{
				return this.scopeManager;
			}
		}

		// Token: 0x06003220 RID: 12832 RVA: 0x00106809 File Offset: 0x00104A09
		private void ValueAppend(string s, bool disableOutputEscaping)
		{
			this.currentInfo.ValueAppend(s, disableOutputEscaping);
		}

		// Token: 0x06003221 RID: 12833 RVA: 0x00106818 File Offset: 0x00104A18
		private bool CanOutput(int state)
		{
			if (this.recordState == 0 || (state & 8192) == 0)
			{
				return true;
			}
			this.recordState = 2;
			this.FinalizeRecord();
			this.SetEmptyFlag(state);
			return this.output.RecordDone(this) == Processor.OutputResult.Continue;
		}

		// Token: 0x06003222 RID: 12834 RVA: 0x00106850 File Offset: 0x00104A50
		internal Processor.OutputResult BeginEvent(int state, XPathNodeType nodeType, string prefix, string name, string nspace, bool empty, object htmlProps, bool search)
		{
			if (!this.CanOutput(state))
			{
				return Processor.OutputResult.Overflow;
			}
			this.AdjustDepth(state);
			this.ResetRecord(state);
			this.PopElementScope();
			prefix = ((prefix != null) ? this.nameTable.Add(prefix) : this.atoms.Empty);
			name = ((name != null) ? this.nameTable.Add(name) : this.atoms.Empty);
			nspace = ((nspace != null) ? this.nameTable.Add(nspace) : this.atoms.Empty);
			switch (nodeType)
			{
			case XPathNodeType.Element:
				this.mainNode.htmlProps = (htmlProps as HtmlElementProps);
				this.mainNode.search = search;
				this.BeginElement(prefix, name, nspace, empty);
				break;
			case XPathNodeType.Attribute:
				this.BeginAttribute(prefix, name, nspace, htmlProps, search);
				break;
			case XPathNodeType.Namespace:
				this.BeginNamespace(name, nspace);
				break;
			case XPathNodeType.ProcessingInstruction:
				if (!this.BeginProcessingInstruction(prefix, name, nspace))
				{
					return Processor.OutputResult.Error;
				}
				break;
			case XPathNodeType.Comment:
				this.BeginComment();
				break;
			}
			return this.CheckRecordBegin(state);
		}

		// Token: 0x06003223 RID: 12835 RVA: 0x00106974 File Offset: 0x00104B74
		internal Processor.OutputResult TextEvent(int state, string text, bool disableOutputEscaping)
		{
			if (!this.CanOutput(state))
			{
				return Processor.OutputResult.Overflow;
			}
			this.AdjustDepth(state);
			this.ResetRecord(state);
			this.PopElementScope();
			if ((state & 8192) != 0)
			{
				this.currentInfo.Depth = this.recordDepth;
				this.currentInfo.NodeType = XmlNodeType.Text;
			}
			this.ValueAppend(text, disableOutputEscaping);
			return this.CheckRecordBegin(state);
		}

		// Token: 0x06003224 RID: 12836 RVA: 0x001069D8 File Offset: 0x00104BD8
		internal Processor.OutputResult EndEvent(int state, XPathNodeType nodeType)
		{
			if (!this.CanOutput(state))
			{
				return Processor.OutputResult.Overflow;
			}
			this.AdjustDepth(state);
			this.PopElementScope();
			this.popScope = ((state & 65536) != 0);
			if ((state & 4096) != 0 && this.mainNode.IsEmptyTag)
			{
				return Processor.OutputResult.Continue;
			}
			this.ResetRecord(state);
			if ((state & 8192) != 0 && nodeType == XPathNodeType.Element)
			{
				this.EndElement();
			}
			return this.CheckRecordEnd(state);
		}

		// Token: 0x06003225 RID: 12837 RVA: 0x00106A46 File Offset: 0x00104C46
		internal void Reset()
		{
			if (this.recordState == 2)
			{
				this.recordState = 0;
			}
		}

		// Token: 0x06003226 RID: 12838 RVA: 0x00106A58 File Offset: 0x00104C58
		internal void TheEnd()
		{
			if (this.recordState == 1)
			{
				this.recordState = 2;
				this.FinalizeRecord();
				this.output.RecordDone(this);
			}
			this.output.TheEnd();
		}

		// Token: 0x06003227 RID: 12839 RVA: 0x00106A88 File Offset: 0x00104C88
		private int FindAttribute(string name, string nspace, ref string prefix)
		{
			for (int i = 0; i < this.attributeCount; i++)
			{
				BuilderInfo builderInfo = (BuilderInfo)this.attributeList[i];
				if (Ref.Equal(builderInfo.LocalName, name))
				{
					if (Ref.Equal(builderInfo.NamespaceURI, nspace))
					{
						return i;
					}
					if (Ref.Equal(builderInfo.Prefix, prefix))
					{
						prefix = string.Empty;
					}
				}
			}
			return -1;
		}

		// Token: 0x06003228 RID: 12840 RVA: 0x00106AF0 File Offset: 0x00104CF0
		private void BeginElement(string prefix, string name, string nspace, bool empty)
		{
			this.currentInfo.NodeType = XmlNodeType.Element;
			this.currentInfo.Prefix = prefix;
			this.currentInfo.LocalName = name;
			this.currentInfo.NamespaceURI = nspace;
			this.currentInfo.Depth = this.recordDepth;
			this.currentInfo.IsEmptyTag = empty;
			this.scopeManager.PushScope(name, nspace, prefix);
		}

		// Token: 0x06003229 RID: 12841 RVA: 0x00106B5C File Offset: 0x00104D5C
		private void EndElement()
		{
			OutputScope currentElementScope = this.scopeManager.CurrentElementScope;
			this.currentInfo.NodeType = XmlNodeType.EndElement;
			this.currentInfo.Prefix = currentElementScope.Prefix;
			this.currentInfo.LocalName = currentElementScope.Name;
			this.currentInfo.NamespaceURI = currentElementScope.Namespace;
			this.currentInfo.Depth = this.recordDepth;
		}

		// Token: 0x0600322A RID: 12842 RVA: 0x00106BC8 File Offset: 0x00104DC8
		private int NewAttribute()
		{
			if (this.attributeCount >= this.attributeList.Count)
			{
				this.attributeList.Add(new BuilderInfo());
			}
			int num = this.attributeCount;
			this.attributeCount = num + 1;
			return num;
		}

		// Token: 0x0600322B RID: 12843 RVA: 0x00106C0C File Offset: 0x00104E0C
		private void BeginAttribute(string prefix, string name, string nspace, object htmlAttrProps, bool search)
		{
			int num = this.FindAttribute(name, nspace, ref prefix);
			if (num == -1)
			{
				num = this.NewAttribute();
			}
			BuilderInfo builderInfo = (BuilderInfo)this.attributeList[num];
			builderInfo.Initialize(prefix, name, nspace);
			builderInfo.Depth = this.recordDepth;
			builderInfo.NodeType = XmlNodeType.Attribute;
			builderInfo.htmlAttrProps = (htmlAttrProps as HtmlAttributeProps);
			builderInfo.search = search;
			this.currentInfo = builderInfo;
		}

		// Token: 0x0600322C RID: 12844 RVA: 0x00106C7C File Offset: 0x00104E7C
		private void BeginNamespace(string name, string nspace)
		{
			bool flag = false;
			if (Ref.Equal(name, this.atoms.Empty))
			{
				if (!Ref.Equal(nspace, this.scopeManager.DefaultNamespace) && !Ref.Equal(this.mainNode.NamespaceURI, this.atoms.Empty))
				{
					this.DeclareNamespace(nspace, name);
				}
			}
			else
			{
				string text = this.scopeManager.ResolveNamespace(name, out flag);
				if (text != null)
				{
					if (!Ref.Equal(nspace, text) && !flag)
					{
						this.DeclareNamespace(nspace, name);
					}
				}
				else
				{
					this.DeclareNamespace(nspace, name);
				}
			}
			this.currentInfo = this.dummy;
			this.currentInfo.NodeType = XmlNodeType.Attribute;
		}

		// Token: 0x0600322D RID: 12845 RVA: 0x00106D20 File Offset: 0x00104F20
		private bool BeginProcessingInstruction(string prefix, string name, string nspace)
		{
			this.currentInfo.NodeType = XmlNodeType.ProcessingInstruction;
			this.currentInfo.Prefix = prefix;
			this.currentInfo.LocalName = name;
			this.currentInfo.NamespaceURI = nspace;
			this.currentInfo.Depth = this.recordDepth;
			return true;
		}

		// Token: 0x0600322E RID: 12846 RVA: 0x00106D6F File Offset: 0x00104F6F
		private void BeginComment()
		{
			this.currentInfo.NodeType = XmlNodeType.Comment;
			this.currentInfo.Depth = this.recordDepth;
		}

		// Token: 0x0600322F RID: 12847 RVA: 0x00106D90 File Offset: 0x00104F90
		private void AdjustDepth(int state)
		{
			int num = state & 768;
			if (num == 256)
			{
				this.recordDepth++;
				return;
			}
			if (num != 512)
			{
				return;
			}
			this.recordDepth--;
		}

		// Token: 0x06003230 RID: 12848 RVA: 0x00106DD4 File Offset: 0x00104FD4
		private void ResetRecord(int state)
		{
			if ((state & 8192) != 0)
			{
				this.attributeCount = 0;
				this.namespaceCount = 0;
				this.currentInfo = this.mainNode;
				this.currentInfo.Initialize(this.atoms.Empty, this.atoms.Empty, this.atoms.Empty);
				this.currentInfo.NodeType = XmlNodeType.None;
				this.currentInfo.IsEmptyTag = false;
				this.currentInfo.htmlProps = null;
				this.currentInfo.htmlAttrProps = null;
			}
		}

		// Token: 0x06003231 RID: 12849 RVA: 0x00106E60 File Offset: 0x00105060
		private void PopElementScope()
		{
			if (this.popScope)
			{
				this.scopeManager.PopScope();
				this.popScope = false;
			}
		}

		// Token: 0x06003232 RID: 12850 RVA: 0x00106E7C File Offset: 0x0010507C
		private Processor.OutputResult CheckRecordBegin(int state)
		{
			if ((state & 16384) != 0)
			{
				this.recordState = 2;
				this.FinalizeRecord();
				this.SetEmptyFlag(state);
				return this.output.RecordDone(this);
			}
			this.recordState = 1;
			return Processor.OutputResult.Continue;
		}

		// Token: 0x06003233 RID: 12851 RVA: 0x00106EB0 File Offset: 0x001050B0
		private Processor.OutputResult CheckRecordEnd(int state)
		{
			if ((state & 16384) != 0)
			{
				this.recordState = 2;
				this.FinalizeRecord();
				this.SetEmptyFlag(state);
				return this.output.RecordDone(this);
			}
			return Processor.OutputResult.Continue;
		}

		// Token: 0x06003234 RID: 12852 RVA: 0x00106EDD File Offset: 0x001050DD
		private void SetEmptyFlag(int state)
		{
			if ((state & 1024) != 0)
			{
				this.mainNode.IsEmptyTag = false;
			}
		}

		// Token: 0x06003235 RID: 12853 RVA: 0x00106EF4 File Offset: 0x001050F4
		private void AnalyzeSpaceLang()
		{
			for (int i = 0; i < this.attributeCount; i++)
			{
				BuilderInfo builderInfo = (BuilderInfo)this.attributeList[i];
				if (Ref.Equal(builderInfo.Prefix, this.atoms.Xml))
				{
					OutputScope currentElementScope = this.scopeManager.CurrentElementScope;
					if (Ref.Equal(builderInfo.LocalName, this.atoms.Lang))
					{
						currentElementScope.Lang = builderInfo.Value;
					}
					else if (Ref.Equal(builderInfo.LocalName, this.atoms.Space))
					{
						currentElementScope.Space = RecordBuilder.TranslateXmlSpace(builderInfo.Value);
					}
				}
			}
		}

		// Token: 0x06003236 RID: 12854 RVA: 0x00106FA0 File Offset: 0x001051A0
		private void FixupElement()
		{
			if (Ref.Equal(this.mainNode.NamespaceURI, this.atoms.Empty))
			{
				this.mainNode.Prefix = this.atoms.Empty;
			}
			if (Ref.Equal(this.mainNode.Prefix, this.atoms.Empty))
			{
				if (!Ref.Equal(this.mainNode.NamespaceURI, this.scopeManager.DefaultNamespace))
				{
					this.DeclareNamespace(this.mainNode.NamespaceURI, this.mainNode.Prefix);
				}
			}
			else
			{
				bool flag = false;
				string text = this.scopeManager.ResolveNamespace(this.mainNode.Prefix, out flag);
				if (text != null)
				{
					if (!Ref.Equal(this.mainNode.NamespaceURI, text))
					{
						if (flag)
						{
							this.mainNode.Prefix = this.GetPrefixForNamespace(this.mainNode.NamespaceURI);
						}
						else
						{
							this.DeclareNamespace(this.mainNode.NamespaceURI, this.mainNode.Prefix);
						}
					}
				}
				else
				{
					this.DeclareNamespace(this.mainNode.NamespaceURI, this.mainNode.Prefix);
				}
			}
			this.scopeManager.CurrentElementScope.Prefix = this.mainNode.Prefix;
		}

		// Token: 0x06003237 RID: 12855 RVA: 0x001070E8 File Offset: 0x001052E8
		private void FixupAttributes(int attributeCount)
		{
			for (int i = 0; i < attributeCount; i++)
			{
				BuilderInfo builderInfo = (BuilderInfo)this.attributeList[i];
				if (Ref.Equal(builderInfo.NamespaceURI, this.atoms.Empty))
				{
					builderInfo.Prefix = this.atoms.Empty;
				}
				else if (Ref.Equal(builderInfo.Prefix, this.atoms.Empty))
				{
					builderInfo.Prefix = this.GetPrefixForNamespace(builderInfo.NamespaceURI);
				}
				else
				{
					bool flag = false;
					string text = this.scopeManager.ResolveNamespace(builderInfo.Prefix, out flag);
					if (text != null)
					{
						if (!Ref.Equal(builderInfo.NamespaceURI, text))
						{
							if (flag)
							{
								builderInfo.Prefix = this.GetPrefixForNamespace(builderInfo.NamespaceURI);
							}
							else
							{
								this.DeclareNamespace(builderInfo.NamespaceURI, builderInfo.Prefix);
							}
						}
					}
					else
					{
						this.DeclareNamespace(builderInfo.NamespaceURI, builderInfo.Prefix);
					}
				}
			}
		}

		// Token: 0x06003238 RID: 12856 RVA: 0x001071D8 File Offset: 0x001053D8
		private void AppendNamespaces()
		{
			for (int i = this.namespaceCount - 1; i >= 0; i--)
			{
				((BuilderInfo)this.attributeList[this.NewAttribute()]).Initialize((BuilderInfo)this.namespaceList[i]);
			}
		}

		// Token: 0x06003239 RID: 12857 RVA: 0x00107224 File Offset: 0x00105424
		private void AnalyzeComment()
		{
			StringBuilder stringBuilder = null;
			string value = this.mainNode.Value;
			bool flag = false;
			int i = 0;
			int num = 0;
			while (i < value.Length)
			{
				char c = value[i];
				if (c == '-')
				{
					if (flag)
					{
						if (stringBuilder == null)
						{
							stringBuilder = new StringBuilder(value, num, i, 2 * value.Length);
						}
						else
						{
							stringBuilder.Append(value, num, i - num);
						}
						stringBuilder.Append(" -");
						num = i + 1;
					}
					flag = true;
				}
				else
				{
					flag = false;
				}
				i++;
			}
			if (stringBuilder != null)
			{
				if (num < value.Length)
				{
					stringBuilder.Append(value, num, value.Length - num);
				}
				if (flag)
				{
					stringBuilder.Append(" ");
				}
				this.mainNode.Value = stringBuilder.ToString();
				return;
			}
			if (flag)
			{
				this.mainNode.ValueAppend(" ", false);
			}
		}

		// Token: 0x0600323A RID: 12858 RVA: 0x001072F8 File Offset: 0x001054F8
		private void AnalyzeProcessingInstruction()
		{
			StringBuilder stringBuilder = null;
			string value = this.mainNode.Value;
			bool flag = false;
			int i = 0;
			int num = 0;
			while (i < value.Length)
			{
				char c = value[i];
				if (c != '>')
				{
					flag = (c == '?');
				}
				else
				{
					if (flag)
					{
						if (stringBuilder == null)
						{
							stringBuilder = new StringBuilder(value, num, i, 2 * value.Length);
						}
						else
						{
							stringBuilder.Append(value, num, i - num);
						}
						stringBuilder.Append(" >");
						num = i + 1;
					}
					flag = false;
				}
				i++;
			}
			if (stringBuilder != null)
			{
				if (num < value.Length)
				{
					stringBuilder.Append(value, num, value.Length - num);
				}
				this.mainNode.Value = stringBuilder.ToString();
			}
		}

		// Token: 0x0600323B RID: 12859 RVA: 0x001073B4 File Offset: 0x001055B4
		private void FinalizeRecord()
		{
			XmlNodeType nodeType = this.mainNode.NodeType;
			if (nodeType == XmlNodeType.Element)
			{
				int num = this.attributeCount;
				this.FixupElement();
				this.FixupAttributes(num);
				this.AnalyzeSpaceLang();
				this.AppendNamespaces();
				return;
			}
			if (nodeType == XmlNodeType.ProcessingInstruction)
			{
				this.AnalyzeProcessingInstruction();
				return;
			}
			if (nodeType != XmlNodeType.Comment)
			{
				return;
			}
			this.AnalyzeComment();
		}

		// Token: 0x0600323C RID: 12860 RVA: 0x00107408 File Offset: 0x00105608
		private int NewNamespace()
		{
			if (this.namespaceCount >= this.namespaceList.Count)
			{
				this.namespaceList.Add(new BuilderInfo());
			}
			int num = this.namespaceCount;
			this.namespaceCount = num + 1;
			return num;
		}

		// Token: 0x0600323D RID: 12861 RVA: 0x0010744C File Offset: 0x0010564C
		private void DeclareNamespace(string nspace, string prefix)
		{
			int index = this.NewNamespace();
			BuilderInfo builderInfo = (BuilderInfo)this.namespaceList[index];
			if (prefix == this.atoms.Empty)
			{
				builderInfo.Initialize(this.atoms.Empty, this.atoms.Xmlns, this.atoms.XmlnsNamespace);
			}
			else
			{
				builderInfo.Initialize(this.atoms.Xmlns, prefix, this.atoms.XmlnsNamespace);
			}
			builderInfo.Depth = this.recordDepth;
			builderInfo.NodeType = XmlNodeType.Attribute;
			builderInfo.Value = nspace;
			this.scopeManager.PushNamespace(prefix, nspace);
		}

		// Token: 0x0600323E RID: 12862 RVA: 0x001074F4 File Offset: 0x001056F4
		private string DeclareNewNamespace(string nspace)
		{
			string text = this.scopeManager.GeneratePrefix("xp_{0}");
			this.DeclareNamespace(nspace, text);
			return text;
		}

		// Token: 0x0600323F RID: 12863 RVA: 0x0010751C File Offset: 0x0010571C
		internal string GetPrefixForNamespace(string nspace)
		{
			string result = null;
			if (this.scopeManager.FindPrefix(nspace, out result))
			{
				return result;
			}
			return this.DeclareNewNamespace(nspace);
		}

		// Token: 0x06003240 RID: 12864 RVA: 0x00107544 File Offset: 0x00105744
		private static XmlSpace TranslateXmlSpace(string space)
		{
			if (space == "default")
			{
				return XmlSpace.Default;
			}
			if (space == "preserve")
			{
				return XmlSpace.Preserve;
			}
			return XmlSpace.None;
		}

		// Token: 0x04002101 RID: 8449
		private int outputState;

		// Token: 0x04002102 RID: 8450
		private RecordBuilder next;

		// Token: 0x04002103 RID: 8451
		private RecordOutput output;

		// Token: 0x04002104 RID: 8452
		private XmlNameTable nameTable;

		// Token: 0x04002105 RID: 8453
		private OutKeywords atoms;

		// Token: 0x04002106 RID: 8454
		private OutputScopeManager scopeManager;

		// Token: 0x04002107 RID: 8455
		private BuilderInfo mainNode = new BuilderInfo();

		// Token: 0x04002108 RID: 8456
		private ArrayList attributeList = new ArrayList();

		// Token: 0x04002109 RID: 8457
		private int attributeCount;

		// Token: 0x0400210A RID: 8458
		private ArrayList namespaceList = new ArrayList();

		// Token: 0x0400210B RID: 8459
		private int namespaceCount;

		// Token: 0x0400210C RID: 8460
		private BuilderInfo dummy = new BuilderInfo();

		// Token: 0x0400210D RID: 8461
		private BuilderInfo currentInfo;

		// Token: 0x0400210E RID: 8462
		private bool popScope;

		// Token: 0x0400210F RID: 8463
		private int recordState;

		// Token: 0x04002110 RID: 8464
		private int recordDepth;

		// Token: 0x04002111 RID: 8465
		private const int NoRecord = 0;

		// Token: 0x04002112 RID: 8466
		private const int SomeRecord = 1;

		// Token: 0x04002113 RID: 8467
		private const int HaveRecord = 2;

		// Token: 0x04002114 RID: 8468
		private const char s_Minus = '-';

		// Token: 0x04002115 RID: 8469
		private const string s_Space = " ";

		// Token: 0x04002116 RID: 8470
		private const string s_SpaceMinus = " -";

		// Token: 0x04002117 RID: 8471
		private const char s_Question = '?';

		// Token: 0x04002118 RID: 8472
		private const char s_Greater = '>';

		// Token: 0x04002119 RID: 8473
		private const string s_SpaceGreater = " >";

		// Token: 0x0400211A RID: 8474
		private const string PrefixFormat = "xp_{0}";
	}
}
