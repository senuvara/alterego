﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000519 RID: 1305
	internal class UseAttributeSetsAction : CompiledAction
	{
		// Token: 0x17000AA8 RID: 2728
		// (get) Token: 0x060032DE RID: 13022 RVA: 0x0010A1D2 File Offset: 0x001083D2
		internal XmlQualifiedName[] UsedSets
		{
			get
			{
				return this.useAttributeSets;
			}
		}

		// Token: 0x060032DF RID: 13023 RVA: 0x0010A1DC File Offset: 0x001083DC
		internal override void Compile(Compiler compiler)
		{
			this.useString = compiler.Input.Value;
			if (this.useString.Length == 0)
			{
				this.useAttributeSets = new XmlQualifiedName[0];
				return;
			}
			string[] array = XmlConvert.SplitString(this.useString);
			try
			{
				this.useAttributeSets = new XmlQualifiedName[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					this.useAttributeSets[i] = compiler.CreateXPathQName(array[i]);
				}
			}
			catch (XsltException)
			{
				if (!compiler.ForwardCompatibility)
				{
					throw;
				}
				this.useAttributeSets = new XmlQualifiedName[0];
			}
		}

		// Token: 0x060032E0 RID: 13024 RVA: 0x0010A278 File Offset: 0x00108478
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 2)
				{
					return;
				}
			}
			else
			{
				frame.Counter = 0;
				frame.State = 2;
			}
			if (frame.Counter < this.useAttributeSets.Length)
			{
				AttributeSetAction attributeSet = processor.RootAction.GetAttributeSet(this.useAttributeSets[frame.Counter]);
				frame.IncrementCounter();
				processor.PushActionFrame(attributeSet, frame.NodeSet);
				return;
			}
			frame.Finished();
		}

		// Token: 0x060032E1 RID: 13025 RVA: 0x000FD562 File Offset: 0x000FB762
		public UseAttributeSetsAction()
		{
		}

		// Token: 0x040021B6 RID: 8630
		private XmlQualifiedName[] useAttributeSets;

		// Token: 0x040021B7 RID: 8631
		private string useString;

		// Token: 0x040021B8 RID: 8632
		private const int ProcessingSets = 2;
	}
}
