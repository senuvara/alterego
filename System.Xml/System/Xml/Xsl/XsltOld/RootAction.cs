﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Security;
using System.Xml.XPath;
using System.Xml.Xsl.Runtime;
using MS.Internal.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000507 RID: 1287
	internal class RootAction : TemplateBaseAction
	{
		// Token: 0x17000A94 RID: 2708
		// (get) Token: 0x0600324D RID: 12877 RVA: 0x00107670 File Offset: 0x00105870
		internal XsltOutput Output
		{
			get
			{
				if (this.output == null)
				{
					this.output = new XsltOutput();
				}
				return this.output;
			}
		}

		// Token: 0x0600324E RID: 12878 RVA: 0x0010768B File Offset: 0x0010588B
		internal override void Compile(Compiler compiler)
		{
			base.CompileDocument(compiler, false);
		}

		// Token: 0x0600324F RID: 12879 RVA: 0x00107695 File Offset: 0x00105895
		internal void InsertKey(XmlQualifiedName name, int MatchKey, int UseKey)
		{
			if (this.keyList == null)
			{
				this.keyList = new List<Key>();
			}
			this.keyList.Add(new Key(name, MatchKey, UseKey));
		}

		// Token: 0x06003250 RID: 12880 RVA: 0x001076C0 File Offset: 0x001058C0
		internal AttributeSetAction GetAttributeSet(XmlQualifiedName name)
		{
			AttributeSetAction attributeSetAction = (AttributeSetAction)this.attributeSetTable[name];
			if (attributeSetAction == null)
			{
				throw XsltException.Create("A reference to attribute set '{0}' cannot be resolved. An 'xsl:attribute-set' of this name must be declared at the top level of the stylesheet.", new string[]
				{
					name.ToString()
				});
			}
			return attributeSetAction;
		}

		// Token: 0x06003251 RID: 12881 RVA: 0x00107700 File Offset: 0x00105900
		public void PorcessAttributeSets(Stylesheet rootStylesheet)
		{
			this.MirgeAttributeSets(rootStylesheet);
			foreach (object obj in this.attributeSetTable.Values)
			{
				AttributeSetAction attributeSetAction = (AttributeSetAction)obj;
				if (attributeSetAction.containedActions != null)
				{
					attributeSetAction.containedActions.Reverse();
				}
			}
			this.CheckAttributeSets_RecurceInList(new Hashtable(), this.attributeSetTable.Keys);
		}

		// Token: 0x06003252 RID: 12882 RVA: 0x00107788 File Offset: 0x00105988
		private void MirgeAttributeSets(Stylesheet stylesheet)
		{
			if (stylesheet.AttributeSetTable != null)
			{
				foreach (object obj in stylesheet.AttributeSetTable.Values)
				{
					AttributeSetAction attributeSetAction = (AttributeSetAction)obj;
					ArrayList containedActions = attributeSetAction.containedActions;
					AttributeSetAction attributeSetAction2 = (AttributeSetAction)this.attributeSetTable[attributeSetAction.Name];
					if (attributeSetAction2 == null)
					{
						attributeSetAction2 = new AttributeSetAction();
						attributeSetAction2.name = attributeSetAction.Name;
						attributeSetAction2.containedActions = new ArrayList();
						this.attributeSetTable[attributeSetAction.Name] = attributeSetAction2;
					}
					ArrayList containedActions2 = attributeSetAction2.containedActions;
					if (containedActions != null)
					{
						int num = containedActions.Count - 1;
						while (0 <= num)
						{
							containedActions2.Add(containedActions[num]);
							num--;
						}
					}
				}
			}
			foreach (object obj2 in stylesheet.Imports)
			{
				Stylesheet stylesheet2 = (Stylesheet)obj2;
				this.MirgeAttributeSets(stylesheet2);
			}
		}

		// Token: 0x06003253 RID: 12883 RVA: 0x001078C4 File Offset: 0x00105AC4
		private void CheckAttributeSets_RecurceInList(Hashtable markTable, ICollection setQNames)
		{
			foreach (object obj in setQNames)
			{
				XmlQualifiedName xmlQualifiedName = (XmlQualifiedName)obj;
				object obj2 = markTable[xmlQualifiedName];
				if (obj2 == "P")
				{
					throw XsltException.Create("Circular reference in the definition of attribute set '{0}'.", new string[]
					{
						xmlQualifiedName.ToString()
					});
				}
				if (obj2 != "D")
				{
					markTable[xmlQualifiedName] = "P";
					this.CheckAttributeSets_RecurceInContainer(markTable, this.GetAttributeSet(xmlQualifiedName));
					markTable[xmlQualifiedName] = "D";
				}
			}
		}

		// Token: 0x06003254 RID: 12884 RVA: 0x0010796C File Offset: 0x00105B6C
		private void CheckAttributeSets_RecurceInContainer(Hashtable markTable, ContainerAction container)
		{
			if (container.containedActions == null)
			{
				return;
			}
			foreach (object obj in container.containedActions)
			{
				Action action = (Action)obj;
				if (action is UseAttributeSetsAction)
				{
					this.CheckAttributeSets_RecurceInList(markTable, ((UseAttributeSetsAction)action).UsedSets);
				}
				else if (action is ContainerAction)
				{
					this.CheckAttributeSets_RecurceInContainer(markTable, (ContainerAction)action);
				}
			}
		}

		// Token: 0x06003255 RID: 12885 RVA: 0x001079F8 File Offset: 0x00105BF8
		internal void AddDecimalFormat(XmlQualifiedName name, DecimalFormat formatinfo)
		{
			DecimalFormat decimalFormat = (DecimalFormat)this.decimalFormatTable[name];
			if (decimalFormat != null)
			{
				NumberFormatInfo info = decimalFormat.info;
				NumberFormatInfo info2 = formatinfo.info;
				if (info.NumberDecimalSeparator != info2.NumberDecimalSeparator || info.NumberGroupSeparator != info2.NumberGroupSeparator || info.PositiveInfinitySymbol != info2.PositiveInfinitySymbol || info.NegativeSign != info2.NegativeSign || info.NaNSymbol != info2.NaNSymbol || info.PercentSymbol != info2.PercentSymbol || info.PerMilleSymbol != info2.PerMilleSymbol || decimalFormat.zeroDigit != formatinfo.zeroDigit || decimalFormat.digit != formatinfo.digit || decimalFormat.patternSeparator != formatinfo.patternSeparator)
				{
					throw XsltException.Create("Decimal format '{0}' has a duplicate declaration.", new string[]
					{
						name.ToString()
					});
				}
			}
			this.decimalFormatTable[name] = formatinfo;
		}

		// Token: 0x06003256 RID: 12886 RVA: 0x00107B07 File Offset: 0x00105D07
		internal DecimalFormat GetDecimalFormat(XmlQualifiedName name)
		{
			return this.decimalFormatTable[name] as DecimalFormat;
		}

		// Token: 0x17000A95 RID: 2709
		// (get) Token: 0x06003257 RID: 12887 RVA: 0x00107B1A File Offset: 0x00105D1A
		internal List<Key> KeyList
		{
			get
			{
				return this.keyList;
			}
		}

		// Token: 0x06003258 RID: 12888 RVA: 0x00107B24 File Offset: 0x00105D24
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			switch (frame.State)
			{
			case 0:
			{
				frame.AllocateVariables(this.variableCount);
				XPathNavigator xpathNavigator = processor.Document.Clone();
				xpathNavigator.MoveToRoot();
				frame.InitNodeSet(new XPathSingletonIterator(xpathNavigator));
				if (this.containedActions != null && this.containedActions.Count > 0)
				{
					processor.PushActionFrame(frame);
				}
				frame.State = 2;
				return;
			}
			case 1:
				break;
			case 2:
				frame.NextNode(processor);
				if (processor.Debugger != null)
				{
					processor.PopDebuggerStack();
				}
				processor.PushTemplateLookup(frame.NodeSet, null, null);
				frame.State = 3;
				return;
			case 3:
				frame.Finished();
				break;
			default:
				return;
			}
		}

		// Token: 0x06003259 RID: 12889 RVA: 0x00107BD0 File Offset: 0x00105DD0
		public RootAction()
		{
		}

		// Token: 0x04002121 RID: 8481
		private const int QueryInitialized = 2;

		// Token: 0x04002122 RID: 8482
		private const int RootProcessed = 3;

		// Token: 0x04002123 RID: 8483
		private Hashtable attributeSetTable = new Hashtable();

		// Token: 0x04002124 RID: 8484
		private Hashtable decimalFormatTable = new Hashtable();

		// Token: 0x04002125 RID: 8485
		private List<Key> keyList;

		// Token: 0x04002126 RID: 8486
		private XsltOutput output;

		// Token: 0x04002127 RID: 8487
		public Stylesheet builtInSheet;

		// Token: 0x04002128 RID: 8488
		public PermissionSet permissions;
	}
}
