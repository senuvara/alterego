﻿using System;
using MS.Internal.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000518 RID: 1304
	internal sealed class TheQuery
	{
		// Token: 0x17000AA7 RID: 2727
		// (get) Token: 0x060032DC RID: 13020 RVA: 0x0010A1AF File Offset: 0x001083AF
		internal CompiledXpathExpr CompiledQuery
		{
			get
			{
				return this._CompiledQuery;
			}
		}

		// Token: 0x060032DD RID: 13021 RVA: 0x0010A1B7 File Offset: 0x001083B7
		internal TheQuery(CompiledXpathExpr compiledQuery, InputScopeManager manager)
		{
			this._CompiledQuery = compiledQuery;
			this._ScopeManager = manager.Clone();
		}

		// Token: 0x040021B4 RID: 8628
		internal InputScopeManager _ScopeManager;

		// Token: 0x040021B5 RID: 8629
		private CompiledXpathExpr _CompiledQuery;
	}
}
