﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004B2 RID: 1202
	internal class ApplyTemplatesAction : ContainerAction
	{
		// Token: 0x06002F52 RID: 12114 RVA: 0x000FD56A File Offset: 0x000FB76A
		internal static ApplyTemplatesAction BuiltInRule()
		{
			return ApplyTemplatesAction.s_BuiltInRule;
		}

		// Token: 0x06002F53 RID: 12115 RVA: 0x000FD571 File Offset: 0x000FB771
		internal static ApplyTemplatesAction BuiltInRule(XmlQualifiedName mode)
		{
			if (!(mode == null) && !mode.IsEmpty)
			{
				return new ApplyTemplatesAction(mode);
			}
			return ApplyTemplatesAction.BuiltInRule();
		}

		// Token: 0x06002F54 RID: 12116 RVA: 0x000FD590 File Offset: 0x000FB790
		internal ApplyTemplatesAction()
		{
		}

		// Token: 0x06002F55 RID: 12117 RVA: 0x000FD59F File Offset: 0x000FB79F
		private ApplyTemplatesAction(XmlQualifiedName mode)
		{
			this.mode = mode;
		}

		// Token: 0x06002F56 RID: 12118 RVA: 0x000FD5B5 File Offset: 0x000FB7B5
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			this.CompileContent(compiler);
		}

		// Token: 0x06002F57 RID: 12119 RVA: 0x000FD5C8 File Offset: 0x000FB7C8
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Select))
			{
				this.selectKey = compiler.AddQuery(value);
			}
			else
			{
				if (!Ref.Equal(localName, compiler.Atoms.Mode))
				{
					return false;
				}
				if (compiler.AllowBuiltInMode && value == "*")
				{
					this.mode = Compiler.BuiltInMode;
				}
				else
				{
					this.mode = compiler.CreateXPathQName(value);
				}
			}
			return true;
		}

		// Token: 0x06002F58 RID: 12120 RVA: 0x000FD658 File Offset: 0x000FB858
		private void CompileContent(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			if (compiler.Recurse())
			{
				for (;;)
				{
					XPathNodeType nodeType = input.NodeType;
					if (nodeType != XPathNodeType.Element)
					{
						if (nodeType - XPathNodeType.SignificantWhitespace > 3)
						{
							break;
						}
					}
					else
					{
						compiler.PushNamespaceScope();
						string namespaceURI = input.NamespaceURI;
						string localName = input.LocalName;
						if (!Ref.Equal(namespaceURI, input.Atoms.UriXsl))
						{
							goto IL_A7;
						}
						if (Ref.Equal(localName, input.Atoms.Sort))
						{
							base.AddAction(compiler.CreateSortAction());
						}
						else
						{
							if (!Ref.Equal(localName, input.Atoms.WithParam))
							{
								goto IL_A0;
							}
							WithParamAction withParamAction = compiler.CreateWithParamAction();
							base.CheckDuplicateParams(withParamAction.Name);
							base.AddAction(withParamAction);
						}
						compiler.PopScope();
					}
					if (!compiler.Advance())
					{
						goto Block_6;
					}
				}
				throw XsltException.Create("The contents of '{0}' are invalid.", new string[]
				{
					"apply-templates"
				});
				IL_A0:
				throw compiler.UnexpectedKeyword();
				IL_A7:
				throw compiler.UnexpectedKeyword();
				Block_6:
				compiler.ToParent();
			}
		}

		// Token: 0x06002F59 RID: 12121 RVA: 0x000FD748 File Offset: 0x000FB948
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			switch (frame.State)
			{
			case 0:
				processor.ResetParams();
				processor.InitSortArray();
				if (this.containedActions != null && this.containedActions.Count > 0)
				{
					processor.PushActionFrame(frame);
					frame.State = 2;
					return;
				}
				break;
			case 1:
				return;
			case 2:
				break;
			case 3:
				goto IL_C2;
			case 4:
				goto IL_DB;
			case 5:
				frame.State = 3;
				goto IL_C2;
			default:
				return;
			}
			if (this.selectKey == -1)
			{
				if (!frame.Node.HasChildren)
				{
					frame.Finished();
					return;
				}
				frame.InitNewNodeSet(frame.Node.SelectChildren(XPathNodeType.All));
			}
			else
			{
				frame.InitNewNodeSet(processor.StartQuery(frame.NodeSet, this.selectKey));
			}
			if (processor.SortArray.Count != 0)
			{
				frame.SortNewNodeSet(processor, processor.SortArray);
			}
			frame.State = 3;
			IL_C2:
			if (!frame.NewNextNode(processor))
			{
				frame.Finished();
				return;
			}
			frame.State = 4;
			IL_DB:
			processor.PushTemplateLookup(frame.NewNodeSet, this.mode, null);
			frame.State = 5;
		}

		// Token: 0x06002F5A RID: 12122 RVA: 0x000FD854 File Offset: 0x000FBA54
		// Note: this type is marked as 'beforefieldinit'.
		static ApplyTemplatesAction()
		{
		}

		// Token: 0x04001FB5 RID: 8117
		private const int ProcessedChildren = 2;

		// Token: 0x04001FB6 RID: 8118
		private const int ProcessNextNode = 3;

		// Token: 0x04001FB7 RID: 8119
		private const int PositionAdvanced = 4;

		// Token: 0x04001FB8 RID: 8120
		private const int TemplateProcessed = 5;

		// Token: 0x04001FB9 RID: 8121
		private int selectKey = -1;

		// Token: 0x04001FBA RID: 8122
		private XmlQualifiedName mode;

		// Token: 0x04001FBB RID: 8123
		private static ApplyTemplatesAction s_BuiltInRule = new ApplyTemplatesAction();
	}
}
