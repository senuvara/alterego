﻿using System;
using System.Collections;
using System.Text;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004B5 RID: 1205
	internal sealed class Avt
	{
		// Token: 0x06002F66 RID: 12134 RVA: 0x000FDCE7 File Offset: 0x000FBEE7
		private Avt(string constAvt)
		{
			this.constAvt = constAvt;
		}

		// Token: 0x06002F67 RID: 12135 RVA: 0x000FDCF8 File Offset: 0x000FBEF8
		private Avt(ArrayList eventList)
		{
			this.events = new TextEvent[eventList.Count];
			for (int i = 0; i < eventList.Count; i++)
			{
				this.events[i] = (TextEvent)eventList[i];
			}
		}

		// Token: 0x17000A08 RID: 2568
		// (get) Token: 0x06002F68 RID: 12136 RVA: 0x000FDD41 File Offset: 0x000FBF41
		public bool IsConstant
		{
			get
			{
				return this.events == null;
			}
		}

		// Token: 0x06002F69 RID: 12137 RVA: 0x000FDD4C File Offset: 0x000FBF4C
		internal string Evaluate(Processor processor, ActionFrame frame)
		{
			if (this.IsConstant)
			{
				return this.constAvt;
			}
			StringBuilder sharedStringBuilder = processor.GetSharedStringBuilder();
			for (int i = 0; i < this.events.Length; i++)
			{
				sharedStringBuilder.Append(this.events[i].Evaluate(processor, frame));
			}
			processor.ReleaseSharedStringBuilder();
			return sharedStringBuilder.ToString();
		}

		// Token: 0x06002F6A RID: 12138 RVA: 0x000FDDA4 File Offset: 0x000FBFA4
		internal static Avt CompileAvt(Compiler compiler, string avtText)
		{
			bool flag;
			ArrayList eventList = compiler.CompileAvt(avtText, out flag);
			if (!flag)
			{
				return new Avt(eventList);
			}
			return new Avt(avtText);
		}

		// Token: 0x04001FC4 RID: 8132
		private string constAvt;

		// Token: 0x04001FC5 RID: 8133
		private TextEvent[] events;
	}
}
