﻿using System;
using System.Collections;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000512 RID: 1298
	internal class TemplateManager
	{
		// Token: 0x17000AA4 RID: 2724
		// (get) Token: 0x060032BF RID: 12991 RVA: 0x00109E0D File Offset: 0x0010800D
		internal XmlQualifiedName Mode
		{
			get
			{
				return this.mode;
			}
		}

		// Token: 0x060032C0 RID: 12992 RVA: 0x00109E15 File Offset: 0x00108015
		internal TemplateManager(Stylesheet stylesheet, XmlQualifiedName mode)
		{
			this.mode = mode;
			this.stylesheet = stylesheet;
		}

		// Token: 0x060032C1 RID: 12993 RVA: 0x00109E2B File Offset: 0x0010802B
		internal void AddTemplate(TemplateAction template)
		{
			if (this.templates == null)
			{
				this.templates = new ArrayList();
			}
			this.templates.Add(template);
		}

		// Token: 0x060032C2 RID: 12994 RVA: 0x00109E4D File Offset: 0x0010804D
		internal void ProcessTemplates()
		{
			if (this.templates != null)
			{
				this.templates.Sort(TemplateManager.s_TemplateComparer);
			}
		}

		// Token: 0x060032C3 RID: 12995 RVA: 0x00109E68 File Offset: 0x00108068
		internal TemplateAction FindTemplate(Processor processor, XPathNavigator navigator)
		{
			if (this.templates == null)
			{
				return null;
			}
			for (int i = this.templates.Count - 1; i >= 0; i--)
			{
				TemplateAction templateAction = (TemplateAction)this.templates[i];
				int matchKey = templateAction.MatchKey;
				if (matchKey != -1 && processor.Matches(navigator, matchKey))
				{
					return templateAction;
				}
			}
			return null;
		}

		// Token: 0x060032C4 RID: 12996 RVA: 0x00109EC1 File Offset: 0x001080C1
		// Note: this type is marked as 'beforefieldinit'.
		static TemplateManager()
		{
		}

		// Token: 0x040021AA RID: 8618
		private XmlQualifiedName mode;

		// Token: 0x040021AB RID: 8619
		internal ArrayList templates;

		// Token: 0x040021AC RID: 8620
		private Stylesheet stylesheet;

		// Token: 0x040021AD RID: 8621
		private static TemplateManager.TemplateComparer s_TemplateComparer = new TemplateManager.TemplateComparer();

		// Token: 0x02000513 RID: 1299
		private class TemplateComparer : IComparer
		{
			// Token: 0x060032C5 RID: 12997 RVA: 0x00109ED0 File Offset: 0x001080D0
			public int Compare(object x, object y)
			{
				TemplateAction templateAction = (TemplateAction)x;
				TemplateAction templateAction2 = (TemplateAction)y;
				if (templateAction.Priority == templateAction2.Priority)
				{
					return templateAction.TemplateId - templateAction2.TemplateId;
				}
				if (templateAction.Priority <= templateAction2.Priority)
				{
					return -1;
				}
				return 1;
			}

			// Token: 0x060032C6 RID: 12998 RVA: 0x00002103 File Offset: 0x00000303
			public TemplateComparer()
			{
			}
		}
	}
}
