﻿using System;
using System.Collections;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C5 RID: 1221
	internal class CopyCodeAction : Action
	{
		// Token: 0x0600302B RID: 12331 RVA: 0x00100DD8 File Offset: 0x000FEFD8
		internal CopyCodeAction()
		{
			this.copyEvents = new ArrayList();
		}

		// Token: 0x0600302C RID: 12332 RVA: 0x00100DEB File Offset: 0x000FEFEB
		internal void AddEvent(Event copyEvent)
		{
			this.copyEvents.Add(copyEvent);
		}

		// Token: 0x0600302D RID: 12333 RVA: 0x00100DFA File Offset: 0x000FEFFA
		internal void AddEvents(ArrayList copyEvents)
		{
			this.copyEvents.AddRange(copyEvents);
		}

		// Token: 0x0600302E RID: 12334 RVA: 0x00100E08 File Offset: 0x000FF008
		internal override void ReplaceNamespaceAlias(Compiler compiler)
		{
			int count = this.copyEvents.Count;
			for (int i = 0; i < count; i++)
			{
				((Event)this.copyEvents[i]).ReplaceNamespaceAlias(compiler);
			}
		}

		// Token: 0x0600302F RID: 12335 RVA: 0x00100E44 File Offset: 0x000FF044
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 2)
				{
					return;
				}
			}
			else
			{
				frame.Counter = 0;
				frame.State = 2;
			}
			while (processor.CanContinue && ((Event)this.copyEvents[frame.Counter]).Output(processor, frame))
			{
				if (frame.IncrementCounter() >= this.copyEvents.Count)
				{
					frame.Finished();
					return;
				}
			}
		}

		// Token: 0x06003030 RID: 12336 RVA: 0x00100EB1 File Offset: 0x000FF0B1
		internal override DbgData GetDbgData(ActionFrame frame)
		{
			return ((Event)this.copyEvents[frame.Counter]).DbgData;
		}

		// Token: 0x04002015 RID: 8213
		private const int Outputting = 2;

		// Token: 0x04002016 RID: 8214
		private ArrayList copyEvents;
	}
}
