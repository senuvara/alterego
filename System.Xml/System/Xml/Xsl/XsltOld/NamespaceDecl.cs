﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004F0 RID: 1264
	internal class NamespaceDecl
	{
		// Token: 0x17000A3A RID: 2618
		// (get) Token: 0x0600311D RID: 12573 RVA: 0x001030F8 File Offset: 0x001012F8
		internal string Prefix
		{
			get
			{
				return this.prefix;
			}
		}

		// Token: 0x17000A3B RID: 2619
		// (get) Token: 0x0600311E RID: 12574 RVA: 0x00103100 File Offset: 0x00101300
		internal string Uri
		{
			get
			{
				return this.nsUri;
			}
		}

		// Token: 0x17000A3C RID: 2620
		// (get) Token: 0x0600311F RID: 12575 RVA: 0x00103108 File Offset: 0x00101308
		internal string PrevDefaultNsUri
		{
			get
			{
				return this.prevDefaultNsUri;
			}
		}

		// Token: 0x17000A3D RID: 2621
		// (get) Token: 0x06003120 RID: 12576 RVA: 0x00103110 File Offset: 0x00101310
		internal NamespaceDecl Next
		{
			get
			{
				return this.next;
			}
		}

		// Token: 0x06003121 RID: 12577 RVA: 0x00103118 File Offset: 0x00101318
		internal NamespaceDecl(string prefix, string nsUri, string prevDefaultNsUri, NamespaceDecl next)
		{
			this.Init(prefix, nsUri, prevDefaultNsUri, next);
		}

		// Token: 0x06003122 RID: 12578 RVA: 0x0010312B File Offset: 0x0010132B
		internal void Init(string prefix, string nsUri, string prevDefaultNsUri, NamespaceDecl next)
		{
			this.prefix = prefix;
			this.nsUri = nsUri;
			this.prevDefaultNsUri = prevDefaultNsUri;
			this.next = next;
		}

		// Token: 0x04002073 RID: 8307
		private string prefix;

		// Token: 0x04002074 RID: 8308
		private string nsUri;

		// Token: 0x04002075 RID: 8309
		private string prevDefaultNsUri;

		// Token: 0x04002076 RID: 8310
		private NamespaceDecl next;
	}
}
