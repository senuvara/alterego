﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C3 RID: 1219
	internal class CopyAction : ContainerAction
	{
		// Token: 0x06003020 RID: 12320 RVA: 0x00100B2B File Offset: 0x000FED2B
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			if (compiler.Recurse())
			{
				base.CompileTemplate(compiler);
				compiler.ToParent();
			}
			if (this.containedActions == null)
			{
				this.empty = true;
			}
		}

		// Token: 0x06003021 RID: 12321 RVA: 0x00100B5C File Offset: 0x000FED5C
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.UseAttributeSets))
			{
				this.useAttributeSets = value;
				base.AddAction(compiler.CreateUseAttributeSetsAction());
				return true;
			}
			return false;
		}

		// Token: 0x06003022 RID: 12322 RVA: 0x00100BAC File Offset: 0x000FEDAC
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			while (processor.CanContinue)
			{
				switch (frame.State)
				{
				case 0:
					if (Processor.IsRoot(frame.Node))
					{
						processor.PushActionFrame(frame);
						frame.State = 8;
						return;
					}
					if (!processor.CopyBeginEvent(frame.Node, this.empty))
					{
						return;
					}
					frame.State = 5;
					break;
				case 1:
				case 2:
				case 3:
				case 4:
					return;
				case 5:
					frame.State = 6;
					if (frame.Node.NodeType == XPathNodeType.Element)
					{
						processor.PushActionFrame(CopyNamespacesAction.GetAction(), frame.NodeSet);
						return;
					}
					break;
				case 6:
					if (frame.Node.NodeType == XPathNodeType.Element && !this.empty)
					{
						processor.PushActionFrame(frame);
						frame.State = 7;
						return;
					}
					if (!processor.CopyTextEvent(frame.Node))
					{
						return;
					}
					frame.State = 7;
					break;
				case 7:
					if (processor.CopyEndEvent(frame.Node))
					{
						frame.Finished();
						return;
					}
					return;
				case 8:
					frame.Finished();
					return;
				default:
					return;
				}
			}
		}

		// Token: 0x06003023 RID: 12323 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		public CopyAction()
		{
		}

		// Token: 0x04002009 RID: 8201
		private const int CopyText = 4;

		// Token: 0x0400200A RID: 8202
		private const int NamespaceCopy = 5;

		// Token: 0x0400200B RID: 8203
		private const int ContentsCopy = 6;

		// Token: 0x0400200C RID: 8204
		private const int ProcessChildren = 7;

		// Token: 0x0400200D RID: 8205
		private const int ChildrenOnly = 8;

		// Token: 0x0400200E RID: 8206
		private string useAttributeSets;

		// Token: 0x0400200F RID: 8207
		private bool empty;
	}
}
