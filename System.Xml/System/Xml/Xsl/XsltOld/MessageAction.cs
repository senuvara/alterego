﻿using System;
using System.Globalization;
using System.IO;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004EF RID: 1263
	internal class MessageAction : ContainerAction
	{
		// Token: 0x06003119 RID: 12569 RVA: 0x000FE529 File Offset: 0x000FC729
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			if (compiler.Recurse())
			{
				base.CompileTemplate(compiler);
				compiler.ToParent();
			}
		}

		// Token: 0x0600311A RID: 12570 RVA: 0x00103024 File Offset: 0x00101224
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Terminate))
			{
				this._Terminate = compiler.GetYesNo(value);
				return true;
			}
			return false;
		}

		// Token: 0x0600311B RID: 12571 RVA: 0x0010306C File Offset: 0x0010126C
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state == 0)
			{
				TextOnlyOutput output = new TextOnlyOutput(processor, new StringWriter(CultureInfo.InvariantCulture));
				processor.PushOutput(output);
				processor.PushActionFrame(frame);
				frame.State = 1;
				return;
			}
			if (state != 1)
			{
				return;
			}
			TextOnlyOutput textOnlyOutput = processor.PopOutput() as TextOnlyOutput;
			Console.WriteLine(textOnlyOutput.Writer.ToString());
			if (this._Terminate)
			{
				throw XsltException.Create("Transform terminated: '{0}'.", new string[]
				{
					textOnlyOutput.Writer.ToString()
				});
			}
			frame.Finished();
		}

		// Token: 0x0600311C RID: 12572 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		public MessageAction()
		{
		}

		// Token: 0x04002072 RID: 8306
		private bool _Terminate;
	}
}
