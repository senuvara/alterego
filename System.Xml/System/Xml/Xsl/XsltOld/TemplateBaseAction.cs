﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200050F RID: 1295
	internal abstract class TemplateBaseAction : ContainerAction
	{
		// Token: 0x060032B6 RID: 12982 RVA: 0x00109BB9 File Offset: 0x00107DB9
		public int AllocateVariableSlot()
		{
			int result = this.variableFreeSlot;
			this.variableFreeSlot++;
			if (this.variableCount < this.variableFreeSlot)
			{
				this.variableCount = this.variableFreeSlot;
			}
			return result;
		}

		// Token: 0x060032B7 RID: 12983 RVA: 0x000030EC File Offset: 0x000012EC
		public void ReleaseVariableSlots(int n)
		{
		}

		// Token: 0x060032B8 RID: 12984 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		protected TemplateBaseAction()
		{
		}

		// Token: 0x040021A6 RID: 8614
		protected int variableCount;

		// Token: 0x040021A7 RID: 8615
		private int variableFreeSlot;
	}
}
