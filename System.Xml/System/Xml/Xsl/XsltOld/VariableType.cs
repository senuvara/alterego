﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200051C RID: 1308
	internal enum VariableType
	{
		// Token: 0x040021BF RID: 8639
		GlobalVariable,
		// Token: 0x040021C0 RID: 8640
		GlobalParameter,
		// Token: 0x040021C1 RID: 8641
		LocalVariable,
		// Token: 0x040021C2 RID: 8642
		LocalParameter,
		// Token: 0x040021C3 RID: 8643
		WithParameter
	}
}
