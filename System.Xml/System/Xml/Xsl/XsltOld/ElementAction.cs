﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004E5 RID: 1253
	internal class ElementAction : ContainerAction
	{
		// Token: 0x060030D0 RID: 12496 RVA: 0x000FDB6C File Offset: 0x000FBD6C
		internal ElementAction()
		{
		}

		// Token: 0x060030D1 RID: 12497 RVA: 0x00101CFC File Offset: 0x000FFEFC
		private static PrefixQName CreateElementQName(string name, string nsUri, InputScopeManager manager)
		{
			if (nsUri == "http://www.w3.org/2000/xmlns/")
			{
				throw XsltException.Create("Elements and attributes cannot belong to the reserved namespace '{0}'.", new string[]
				{
					nsUri
				});
			}
			PrefixQName prefixQName = new PrefixQName();
			prefixQName.SetQName(name);
			if (nsUri == null)
			{
				prefixQName.Namespace = manager.ResolveXmlNamespace(prefixQName.Prefix);
			}
			else
			{
				prefixQName.Namespace = nsUri;
			}
			return prefixQName;
		}

		// Token: 0x060030D2 RID: 12498 RVA: 0x00101D58 File Offset: 0x000FFF58
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.nameAvt, "name");
			this.name = CompiledAction.PrecalculateAvt(ref this.nameAvt);
			this.nsUri = CompiledAction.PrecalculateAvt(ref this.nsAvt);
			if (this.nameAvt == null && this.nsAvt == null)
			{
				if (this.name != "xmlns")
				{
					this.qname = ElementAction.CreateElementQName(this.name, this.nsUri, compiler.CloneScopeManager());
				}
			}
			else
			{
				this.manager = compiler.CloneScopeManager();
			}
			if (compiler.Recurse())
			{
				base.CompileTemplate(compiler);
				compiler.ToParent();
			}
			this.empty = (this.containedActions == null);
		}

		// Token: 0x060030D3 RID: 12499 RVA: 0x00101E14 File Offset: 0x00100014
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Name))
			{
				this.nameAvt = Avt.CompileAvt(compiler, value);
			}
			else if (Ref.Equal(localName, compiler.Atoms.Namespace))
			{
				this.nsAvt = Avt.CompileAvt(compiler, value);
			}
			else
			{
				if (!Ref.Equal(localName, compiler.Atoms.UseAttributeSets))
				{
					return false;
				}
				base.AddAction(compiler.CreateUseAttributeSetsAction());
			}
			return true;
		}

		// Token: 0x060030D4 RID: 12500 RVA: 0x00101EA4 File Offset: 0x001000A4
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			switch (frame.State)
			{
			case 0:
				if (this.qname != null)
				{
					frame.CalulatedName = this.qname;
				}
				else
				{
					frame.CalulatedName = ElementAction.CreateElementQName((this.nameAvt == null) ? this.name : this.nameAvt.Evaluate(processor, frame), (this.nsAvt == null) ? this.nsUri : this.nsAvt.Evaluate(processor, frame), this.manager);
				}
				break;
			case 1:
				goto IL_C2;
			case 2:
				break;
			default:
				return;
			}
			PrefixQName calulatedName = frame.CalulatedName;
			if (!processor.BeginEvent(XPathNodeType.Element, calulatedName.Prefix, calulatedName.Name, calulatedName.Namespace, this.empty))
			{
				frame.State = 2;
				return;
			}
			if (!this.empty)
			{
				processor.PushActionFrame(frame);
				frame.State = 1;
				return;
			}
			IL_C2:
			if (!processor.EndEvent(XPathNodeType.Element))
			{
				frame.State = 1;
				return;
			}
			frame.Finished();
		}

		// Token: 0x04002048 RID: 8264
		private const int NameDone = 2;

		// Token: 0x04002049 RID: 8265
		private Avt nameAvt;

		// Token: 0x0400204A RID: 8266
		private Avt nsAvt;

		// Token: 0x0400204B RID: 8267
		private bool empty;

		// Token: 0x0400204C RID: 8268
		private InputScopeManager manager;

		// Token: 0x0400204D RID: 8269
		private string name;

		// Token: 0x0400204E RID: 8270
		private string nsUri;

		// Token: 0x0400204F RID: 8271
		private PrefixQName qname;
	}
}
