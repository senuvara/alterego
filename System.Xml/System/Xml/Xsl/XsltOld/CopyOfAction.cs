﻿using System;
using System.Xml.XPath;
using MS.Internal.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C8 RID: 1224
	internal class CopyOfAction : CompiledAction
	{
		// Token: 0x0600303C RID: 12348 RVA: 0x00101111 File Offset: 0x000FF311
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.selectKey != -1, "select");
			base.CheckEmpty(compiler);
		}

		// Token: 0x0600303D RID: 12349 RVA: 0x0010113C File Offset: 0x000FF33C
		internal override bool CompileAttribute(Compiler compiler)
		{
			string localName = compiler.Input.LocalName;
			string value = compiler.Input.Value;
			if (Ref.Equal(localName, compiler.Atoms.Select))
			{
				this.selectKey = compiler.AddQuery(value);
				return true;
			}
			return false;
		}

		// Token: 0x0600303E RID: 12350 RVA: 0x00101184 File Offset: 0x000FF384
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			switch (frame.State)
			{
			case 0:
			{
				Query valueQuery = processor.GetValueQuery(this.selectKey);
				object obj = valueQuery.Evaluate(frame.NodeSet);
				if (obj is XPathNodeIterator)
				{
					processor.PushActionFrame(CopyNodeSetAction.GetAction(), new XPathArrayIterator(valueQuery));
					frame.State = 3;
					return;
				}
				XPathNavigator xpathNavigator = obj as XPathNavigator;
				if (xpathNavigator != null)
				{
					processor.PushActionFrame(CopyNodeSetAction.GetAction(), new XPathSingletonIterator(xpathNavigator));
					frame.State = 3;
					return;
				}
				string text = XmlConvert.ToXPathString(obj);
				if (processor.TextEvent(text))
				{
					frame.Finished();
					return;
				}
				frame.StoredOutput = text;
				frame.State = 2;
				return;
			}
			case 1:
				break;
			case 2:
				processor.TextEvent(frame.StoredOutput);
				frame.Finished();
				return;
			case 3:
				frame.Finished();
				break;
			default:
				return;
			}
		}

		// Token: 0x0600303F RID: 12351 RVA: 0x00101250 File Offset: 0x000FF450
		public CopyOfAction()
		{
		}

		// Token: 0x04002023 RID: 8227
		private const int ResultStored = 2;

		// Token: 0x04002024 RID: 8228
		private const int NodeSetCopied = 3;

		// Token: 0x04002025 RID: 8229
		private int selectKey = -1;
	}
}
