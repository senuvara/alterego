﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C9 RID: 1225
	internal class DbgData
	{
		// Token: 0x17000A1E RID: 2590
		// (get) Token: 0x06003040 RID: 12352 RVA: 0x0010125F File Offset: 0x000FF45F
		public XPathNavigator StyleSheet
		{
			get
			{
				return this.styleSheet;
			}
		}

		// Token: 0x17000A1F RID: 2591
		// (get) Token: 0x06003041 RID: 12353 RVA: 0x00101267 File Offset: 0x000FF467
		public VariableAction[] Variables
		{
			get
			{
				return this.variables;
			}
		}

		// Token: 0x06003042 RID: 12354 RVA: 0x00101270 File Offset: 0x000FF470
		public DbgData(Compiler compiler)
		{
			DbgCompiler dbgCompiler = (DbgCompiler)compiler;
			this.styleSheet = dbgCompiler.Input.Navigator.Clone();
			this.variables = dbgCompiler.LocalVariables;
			dbgCompiler.Debugger.OnInstructionCompile(this.StyleSheet);
		}

		// Token: 0x06003043 RID: 12355 RVA: 0x001012BD File Offset: 0x000FF4BD
		internal void ReplaceVariables(VariableAction[] vars)
		{
			this.variables = vars;
		}

		// Token: 0x06003044 RID: 12356 RVA: 0x001012C6 File Offset: 0x000FF4C6
		private DbgData()
		{
			this.styleSheet = null;
			this.variables = new VariableAction[0];
		}

		// Token: 0x17000A20 RID: 2592
		// (get) Token: 0x06003045 RID: 12357 RVA: 0x001012E1 File Offset: 0x000FF4E1
		public static DbgData Empty
		{
			get
			{
				return DbgData.s_nullDbgData;
			}
		}

		// Token: 0x06003046 RID: 12358 RVA: 0x001012E8 File Offset: 0x000FF4E8
		// Note: this type is marked as 'beforefieldinit'.
		static DbgData()
		{
		}

		// Token: 0x04002026 RID: 8230
		private XPathNavigator styleSheet;

		// Token: 0x04002027 RID: 8231
		private VariableAction[] variables;

		// Token: 0x04002028 RID: 8232
		private static DbgData s_nullDbgData = new DbgData();
	}
}
