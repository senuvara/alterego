﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004BC RID: 1212
	internal abstract class CompiledAction : Action
	{
		// Token: 0x06002F91 RID: 12177
		internal abstract void Compile(Compiler compiler);

		// Token: 0x06002F92 RID: 12178 RVA: 0x000020CD File Offset: 0x000002CD
		internal virtual bool CompileAttribute(Compiler compiler)
		{
			return false;
		}

		// Token: 0x06002F93 RID: 12179 RVA: 0x000FE59C File Offset: 0x000FC79C
		public void CompileAttributes(Compiler compiler)
		{
			NavigatorInput input = compiler.Input;
			string localName = input.LocalName;
			if (input.MoveToFirstAttribute())
			{
				do
				{
					if (input.NamespaceURI.Length == 0)
					{
						try
						{
							if (!this.CompileAttribute(compiler))
							{
								throw XsltException.Create("'{0}' is an invalid attribute for the '{1}' element.", new string[]
								{
									input.LocalName,
									localName
								});
							}
						}
						catch
						{
							if (!compiler.ForwardCompatibility)
							{
								throw;
							}
						}
					}
				}
				while (input.MoveToNextAttribute());
				input.ToParent();
			}
		}

		// Token: 0x06002F94 RID: 12180 RVA: 0x000FE624 File Offset: 0x000FC824
		internal static string PrecalculateAvt(ref Avt avt)
		{
			string result = null;
			if (avt != null && avt.IsConstant)
			{
				result = avt.Evaluate(null, null);
				avt = null;
			}
			return result;
		}

		// Token: 0x06002F95 RID: 12181 RVA: 0x000FE650 File Offset: 0x000FC850
		public void CheckEmpty(Compiler compiler)
		{
			string name = compiler.Input.Name;
			if (compiler.Recurse())
			{
				for (;;)
				{
					XPathNodeType nodeType = compiler.Input.NodeType;
					if (nodeType != XPathNodeType.Whitespace && nodeType != XPathNodeType.Comment && nodeType != XPathNodeType.ProcessingInstruction)
					{
						break;
					}
					if (!compiler.Advance())
					{
						goto Block_4;
					}
				}
				throw XsltException.Create("The contents of '{0}' must be empty.", new string[]
				{
					name
				});
				Block_4:
				compiler.ToParent();
			}
		}

		// Token: 0x06002F96 RID: 12182 RVA: 0x000FE6AD File Offset: 0x000FC8AD
		public void CheckRequiredAttribute(Compiler compiler, object attrValue, string attrName)
		{
			this.CheckRequiredAttribute(compiler, attrValue != null, attrName);
		}

		// Token: 0x06002F97 RID: 12183 RVA: 0x000FE6BB File Offset: 0x000FC8BB
		public void CheckRequiredAttribute(Compiler compiler, bool attr, string attrName)
		{
			if (!attr)
			{
				throw XsltException.Create("Missing mandatory attribute '{0}'.", new string[]
				{
					attrName
				});
			}
		}

		// Token: 0x06002F98 RID: 12184 RVA: 0x000FE6D5 File Offset: 0x000FC8D5
		protected CompiledAction()
		{
		}
	}
}
