﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004E7 RID: 1255
	internal abstract class Event
	{
		// Token: 0x060030D7 RID: 12503 RVA: 0x000030EC File Offset: 0x000012EC
		public virtual void ReplaceNamespaceAlias(Compiler compiler)
		{
		}

		// Token: 0x060030D8 RID: 12504
		public abstract bool Output(Processor processor, ActionFrame frame);

		// Token: 0x060030D9 RID: 12505 RVA: 0x00101FA7 File Offset: 0x001001A7
		internal void OnInstructionExecute(Processor processor)
		{
			processor.OnInstructionExecute();
		}

		// Token: 0x17000A27 RID: 2599
		// (get) Token: 0x060030DA RID: 12506 RVA: 0x000FCF9C File Offset: 0x000FB19C
		internal virtual DbgData DbgData
		{
			get
			{
				return DbgData.Empty;
			}
		}

		// Token: 0x060030DB RID: 12507 RVA: 0x00002103 File Offset: 0x00000303
		protected Event()
		{
		}
	}
}
