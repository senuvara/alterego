﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004F3 RID: 1267
	internal class NavigatorOutput : RecordOutput
	{
		// Token: 0x17000A4D RID: 2637
		// (get) Token: 0x06003141 RID: 12609 RVA: 0x00103394 File Offset: 0x00101594
		internal XPathNavigator Navigator
		{
			get
			{
				return ((IXPathNavigable)this.doc).CreateNavigator();
			}
		}

		// Token: 0x06003142 RID: 12610 RVA: 0x001033A1 File Offset: 0x001015A1
		internal NavigatorOutput(string baseUri)
		{
			this.doc = new XPathDocument();
			this.wr = this.doc.LoadFromWriter(XPathDocument.LoadFlags.AtomizeNames, baseUri);
		}

		// Token: 0x06003143 RID: 12611 RVA: 0x001033C8 File Offset: 0x001015C8
		public Processor.OutputResult RecordDone(RecordBuilder record)
		{
			BuilderInfo mainNode = record.MainNode;
			this.documentIndex++;
			switch (mainNode.NodeType)
			{
			case XmlNodeType.Element:
				this.wr.WriteStartElement(mainNode.Prefix, mainNode.LocalName, mainNode.NamespaceURI);
				for (int i = 0; i < record.AttributeCount; i++)
				{
					this.documentIndex++;
					BuilderInfo builderInfo = (BuilderInfo)record.AttributeList[i];
					if (builderInfo.NamespaceURI == "http://www.w3.org/2000/xmlns/")
					{
						if (builderInfo.Prefix.Length == 0)
						{
							this.wr.WriteNamespaceDeclaration(string.Empty, builderInfo.Value);
						}
						else
						{
							this.wr.WriteNamespaceDeclaration(builderInfo.LocalName, builderInfo.Value);
						}
					}
					else
					{
						this.wr.WriteAttributeString(builderInfo.Prefix, builderInfo.LocalName, builderInfo.NamespaceURI, builderInfo.Value);
					}
				}
				this.wr.StartElementContent();
				if (mainNode.IsEmptyTag)
				{
					this.wr.WriteEndElement(mainNode.Prefix, mainNode.LocalName, mainNode.NamespaceURI);
				}
				break;
			case XmlNodeType.Text:
				this.wr.WriteString(mainNode.Value);
				break;
			case XmlNodeType.ProcessingInstruction:
				this.wr.WriteProcessingInstruction(mainNode.LocalName, mainNode.Value);
				break;
			case XmlNodeType.Comment:
				this.wr.WriteComment(mainNode.Value);
				break;
			case XmlNodeType.SignificantWhitespace:
				this.wr.WriteString(mainNode.Value);
				break;
			case XmlNodeType.EndElement:
				this.wr.WriteEndElement(mainNode.Prefix, mainNode.LocalName, mainNode.NamespaceURI);
				break;
			}
			record.Reset();
			return Processor.OutputResult.Continue;
		}

		// Token: 0x06003144 RID: 12612 RVA: 0x001035AC File Offset: 0x001017AC
		public void TheEnd()
		{
			this.wr.Close();
		}

		// Token: 0x0400207F RID: 8319
		private XPathDocument doc;

		// Token: 0x04002080 RID: 8320
		private int documentIndex;

		// Token: 0x04002081 RID: 8321
		private XmlRawWriter wr;
	}
}
