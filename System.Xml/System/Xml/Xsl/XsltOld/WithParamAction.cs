﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200051E RID: 1310
	internal class WithParamAction : VariableAction
	{
		// Token: 0x060032F9 RID: 13049 RVA: 0x0010A72D File Offset: 0x0010892D
		internal WithParamAction() : base(VariableType.WithParameter)
		{
		}

		// Token: 0x060032FA RID: 13050 RVA: 0x0010A738 File Offset: 0x00108938
		internal override void Compile(Compiler compiler)
		{
			base.CompileAttributes(compiler);
			base.CheckRequiredAttribute(compiler, this.name, "name");
			if (compiler.Recurse())
			{
				base.CompileTemplate(compiler);
				compiler.ToParent();
				if (this.selectKey != -1 && this.containedActions != null)
				{
					throw XsltException.Create("The variable or parameter '{0}' cannot have both a 'select' attribute and non-empty content.", new string[]
					{
						this.nameStr
					});
				}
			}
		}

		// Token: 0x060032FB RID: 13051 RVA: 0x0010A7A0 File Offset: 0x001089A0
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state != 0)
			{
				if (state != 1)
				{
					return;
				}
				RecordOutput recordOutput = processor.PopOutput();
				processor.SetParameter(this.name, ((NavigatorOutput)recordOutput).Navigator);
				frame.Finished();
				return;
			}
			else
			{
				if (this.selectKey != -1)
				{
					object value = processor.RunQuery(frame, this.selectKey);
					processor.SetParameter(this.name, value);
					frame.Finished();
					return;
				}
				if (this.containedActions == null)
				{
					processor.SetParameter(this.name, string.Empty);
					frame.Finished();
					return;
				}
				NavigatorOutput output = new NavigatorOutput(this.baseUri);
				processor.PushOutput(output);
				processor.PushActionFrame(frame);
				frame.State = 1;
				return;
			}
		}
	}
}
