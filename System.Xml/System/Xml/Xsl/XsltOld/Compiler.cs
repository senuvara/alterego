﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Security;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Xsl.Runtime;
using System.Xml.Xsl.Xslt;
using System.Xml.Xsl.XsltOld.Debugger;
using MS.Internal.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004BF RID: 1215
	internal class Compiler
	{
		// Token: 0x17000A11 RID: 2577
		// (get) Token: 0x06002F9A RID: 12186 RVA: 0x000FE70A File Offset: 0x000FC90A
		internal KeywordsTable Atoms
		{
			get
			{
				return this.atoms;
			}
		}

		// Token: 0x17000A12 RID: 2578
		// (get) Token: 0x06002F9B RID: 12187 RVA: 0x000FE712 File Offset: 0x000FC912
		// (set) Token: 0x06002F9C RID: 12188 RVA: 0x000FE71A File Offset: 0x000FC91A
		internal int Stylesheetid
		{
			get
			{
				return this.stylesheetid;
			}
			set
			{
				this.stylesheetid = value;
			}
		}

		// Token: 0x17000A13 RID: 2579
		// (get) Token: 0x06002F9D RID: 12189 RVA: 0x000FE723 File Offset: 0x000FC923
		internal NavigatorInput Document
		{
			get
			{
				return this.input;
			}
		}

		// Token: 0x17000A14 RID: 2580
		// (get) Token: 0x06002F9E RID: 12190 RVA: 0x000FE723 File Offset: 0x000FC923
		internal NavigatorInput Input
		{
			get
			{
				return this.input;
			}
		}

		// Token: 0x06002F9F RID: 12191 RVA: 0x000FE72B File Offset: 0x000FC92B
		internal bool Advance()
		{
			return this.Document.Advance();
		}

		// Token: 0x06002FA0 RID: 12192 RVA: 0x000FE738 File Offset: 0x000FC938
		internal bool Recurse()
		{
			return this.Document.Recurse();
		}

		// Token: 0x06002FA1 RID: 12193 RVA: 0x000FE745 File Offset: 0x000FC945
		internal bool ToParent()
		{
			return this.Document.ToParent();
		}

		// Token: 0x17000A15 RID: 2581
		// (get) Token: 0x06002FA2 RID: 12194 RVA: 0x000FE752 File Offset: 0x000FC952
		internal Stylesheet CompiledStylesheet
		{
			get
			{
				return this.stylesheet;
			}
		}

		// Token: 0x17000A16 RID: 2582
		// (get) Token: 0x06002FA3 RID: 12195 RVA: 0x000FE75A File Offset: 0x000FC95A
		// (set) Token: 0x06002FA4 RID: 12196 RVA: 0x000FE762 File Offset: 0x000FC962
		internal RootAction RootAction
		{
			get
			{
				return this.rootAction;
			}
			set
			{
				this.rootAction = value;
				this.currentTemplate = this.rootAction;
			}
		}

		// Token: 0x17000A17 RID: 2583
		// (get) Token: 0x06002FA5 RID: 12197 RVA: 0x000FE777 File Offset: 0x000FC977
		internal List<TheQuery> QueryStore
		{
			get
			{
				return this.queryStore;
			}
		}

		// Token: 0x17000A18 RID: 2584
		// (get) Token: 0x06002FA6 RID: 12198 RVA: 0x000037FB File Offset: 0x000019FB
		public virtual IXsltDebugger Debugger
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06002FA7 RID: 12199 RVA: 0x000FE77F File Offset: 0x000FC97F
		internal string GetUnicRtfId()
		{
			this.rtfCount++;
			return this.rtfCount.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x06002FA8 RID: 12200 RVA: 0x000FE7A0 File Offset: 0x000FC9A0
		internal void Compile(NavigatorInput input, XmlResolver xmlResolver, Evidence evidence)
		{
			evidence = null;
			this.xmlResolver = xmlResolver;
			this.PushInputDocument(input);
			this.rootScope = this.scopeManager.PushScope();
			this.queryStore = new List<TheQuery>();
			try
			{
				this.rootStylesheet = new Stylesheet();
				this.PushStylesheet(this.rootStylesheet);
				try
				{
					this.CreateRootAction();
				}
				catch (XsltCompileException)
				{
					throw;
				}
				catch (Exception inner)
				{
					throw new XsltCompileException(inner, this.Input.BaseURI, this.Input.LineNumber, this.Input.LinePosition);
				}
				this.stylesheet.ProcessTemplates();
				this.rootAction.PorcessAttributeSets(this.rootStylesheet);
				this.stylesheet.SortWhiteSpace();
				if (evidence != null)
				{
					this.rootAction.permissions = SecurityManager.GetStandardSandbox(evidence);
				}
				if (this.globalNamespaceAliasTable != null)
				{
					this.stylesheet.ReplaceNamespaceAlias(this);
					this.rootAction.ReplaceNamespaceAlias(this);
				}
			}
			finally
			{
				this.PopInputDocument();
			}
		}

		// Token: 0x17000A19 RID: 2585
		// (get) Token: 0x06002FA9 RID: 12201 RVA: 0x000FE8B0 File Offset: 0x000FCAB0
		// (set) Token: 0x06002FAA RID: 12202 RVA: 0x000FE8C2 File Offset: 0x000FCAC2
		internal bool ForwardCompatibility
		{
			get
			{
				return this.scopeManager.CurrentScope.ForwardCompatibility;
			}
			set
			{
				this.scopeManager.CurrentScope.ForwardCompatibility = value;
			}
		}

		// Token: 0x17000A1A RID: 2586
		// (get) Token: 0x06002FAB RID: 12203 RVA: 0x000FE8D5 File Offset: 0x000FCAD5
		// (set) Token: 0x06002FAC RID: 12204 RVA: 0x000FE8E7 File Offset: 0x000FCAE7
		internal bool CanHaveApplyImports
		{
			get
			{
				return this.scopeManager.CurrentScope.CanHaveApplyImports;
			}
			set
			{
				this.scopeManager.CurrentScope.CanHaveApplyImports = value;
			}
		}

		// Token: 0x06002FAD RID: 12205 RVA: 0x000FE8FC File Offset: 0x000FCAFC
		internal void InsertExtensionNamespace(string value)
		{
			string[] array = this.ResolvePrefixes(value);
			if (array != null)
			{
				this.scopeManager.InsertExtensionNamespaces(array);
			}
		}

		// Token: 0x06002FAE RID: 12206 RVA: 0x000FE920 File Offset: 0x000FCB20
		internal void InsertExcludedNamespace(string value)
		{
			string[] array = this.ResolvePrefixes(value);
			if (array != null)
			{
				this.scopeManager.InsertExcludedNamespaces(array);
			}
		}

		// Token: 0x06002FAF RID: 12207 RVA: 0x000FE944 File Offset: 0x000FCB44
		internal void InsertExtensionNamespace()
		{
			this.InsertExtensionNamespace(this.Input.Navigator.GetAttribute(this.Input.Atoms.ExtensionElementPrefixes, this.Input.Atoms.UriXsl));
		}

		// Token: 0x06002FB0 RID: 12208 RVA: 0x000FE97C File Offset: 0x000FCB7C
		internal void InsertExcludedNamespace()
		{
			this.InsertExcludedNamespace(this.Input.Navigator.GetAttribute(this.Input.Atoms.ExcludeResultPrefixes, this.Input.Atoms.UriXsl));
		}

		// Token: 0x06002FB1 RID: 12209 RVA: 0x000FE9B4 File Offset: 0x000FCBB4
		internal bool IsExtensionNamespace(string nspace)
		{
			return this.scopeManager.IsExtensionNamespace(nspace);
		}

		// Token: 0x06002FB2 RID: 12210 RVA: 0x000FE9C2 File Offset: 0x000FCBC2
		internal bool IsExcludedNamespace(string nspace)
		{
			return this.scopeManager.IsExcludedNamespace(nspace);
		}

		// Token: 0x06002FB3 RID: 12211 RVA: 0x000FE9D0 File Offset: 0x000FCBD0
		internal void PushLiteralScope()
		{
			this.PushNamespaceScope();
			string attribute = this.Input.Navigator.GetAttribute(this.Atoms.Version, this.Atoms.UriXsl);
			if (attribute.Length != 0)
			{
				this.ForwardCompatibility = (attribute != "1.0");
			}
		}

		// Token: 0x06002FB4 RID: 12212 RVA: 0x000FEA24 File Offset: 0x000FCC24
		internal void PushNamespaceScope()
		{
			this.scopeManager.PushScope();
			NavigatorInput navigatorInput = this.Input;
			if (navigatorInput.MoveToFirstNamespace())
			{
				do
				{
					this.scopeManager.PushNamespace(navigatorInput.LocalName, navigatorInput.Value);
				}
				while (navigatorInput.MoveToNextNamespace());
				navigatorInput.ToParent();
			}
		}

		// Token: 0x17000A1B RID: 2587
		// (get) Token: 0x06002FB5 RID: 12213 RVA: 0x000FEA72 File Offset: 0x000FCC72
		protected InputScopeManager ScopeManager
		{
			get
			{
				return this.scopeManager;
			}
		}

		// Token: 0x06002FB6 RID: 12214 RVA: 0x000FEA7A File Offset: 0x000FCC7A
		internal virtual void PopScope()
		{
			this.currentTemplate.ReleaseVariableSlots(this.scopeManager.CurrentScope.GetVeriablesCount());
			this.scopeManager.PopScope();
		}

		// Token: 0x06002FB7 RID: 12215 RVA: 0x000FEAA2 File Offset: 0x000FCCA2
		internal InputScopeManager CloneScopeManager()
		{
			return this.scopeManager.Clone();
		}

		// Token: 0x06002FB8 RID: 12216 RVA: 0x000FEAB0 File Offset: 0x000FCCB0
		internal int InsertVariable(VariableAction variable)
		{
			InputScope variableScope;
			if (variable.IsGlobal)
			{
				variableScope = this.rootScope;
			}
			else
			{
				variableScope = this.scopeManager.VariableScope;
			}
			VariableAction variableAction = variableScope.ResolveVariable(variable.Name);
			if (variableAction != null)
			{
				if (!variableAction.IsGlobal)
				{
					throw XsltException.Create("Variable or parameter '{0}' was duplicated within the same scope.", new string[]
					{
						variable.NameStr
					});
				}
				if (variable.IsGlobal)
				{
					if (variable.Stylesheetid == variableAction.Stylesheetid)
					{
						throw XsltException.Create("Variable or parameter '{0}' was duplicated within the same scope.", new string[]
						{
							variable.NameStr
						});
					}
					if (variable.Stylesheetid < variableAction.Stylesheetid)
					{
						variableScope.InsertVariable(variable);
						return variableAction.VarKey;
					}
					return -1;
				}
			}
			variableScope.InsertVariable(variable);
			return this.currentTemplate.AllocateVariableSlot();
		}

		// Token: 0x06002FB9 RID: 12217 RVA: 0x000FEB6C File Offset: 0x000FCD6C
		internal void AddNamespaceAlias(string StylesheetURI, NamespaceInfo AliasInfo)
		{
			if (this.globalNamespaceAliasTable == null)
			{
				this.globalNamespaceAliasTable = new Hashtable();
			}
			NamespaceInfo namespaceInfo = this.globalNamespaceAliasTable[StylesheetURI] as NamespaceInfo;
			if (namespaceInfo == null || AliasInfo.stylesheetId <= namespaceInfo.stylesheetId)
			{
				this.globalNamespaceAliasTable[StylesheetURI] = AliasInfo;
			}
		}

		// Token: 0x06002FBA RID: 12218 RVA: 0x000FEBBC File Offset: 0x000FCDBC
		internal bool IsNamespaceAlias(string StylesheetURI)
		{
			return this.globalNamespaceAliasTable != null && this.globalNamespaceAliasTable.Contains(StylesheetURI);
		}

		// Token: 0x06002FBB RID: 12219 RVA: 0x000FEBD4 File Offset: 0x000FCDD4
		internal NamespaceInfo FindNamespaceAlias(string StylesheetURI)
		{
			if (this.globalNamespaceAliasTable != null)
			{
				return (NamespaceInfo)this.globalNamespaceAliasTable[StylesheetURI];
			}
			return null;
		}

		// Token: 0x06002FBC RID: 12220 RVA: 0x000FEBF1 File Offset: 0x000FCDF1
		internal string ResolveXmlNamespace(string prefix)
		{
			return this.scopeManager.ResolveXmlNamespace(prefix);
		}

		// Token: 0x06002FBD RID: 12221 RVA: 0x000FEBFF File Offset: 0x000FCDFF
		internal string ResolveXPathNamespace(string prefix)
		{
			return this.scopeManager.ResolveXPathNamespace(prefix);
		}

		// Token: 0x17000A1C RID: 2588
		// (get) Token: 0x06002FBE RID: 12222 RVA: 0x000FEC0D File Offset: 0x000FCE0D
		internal string DefaultNamespace
		{
			get
			{
				return this.scopeManager.DefaultNamespace;
			}
		}

		// Token: 0x06002FBF RID: 12223 RVA: 0x000FEC1A File Offset: 0x000FCE1A
		internal void InsertKey(XmlQualifiedName name, int MatchKey, int UseKey)
		{
			this.rootAction.InsertKey(name, MatchKey, UseKey);
		}

		// Token: 0x06002FC0 RID: 12224 RVA: 0x000FEC2A File Offset: 0x000FCE2A
		internal void AddDecimalFormat(XmlQualifiedName name, DecimalFormat formatinfo)
		{
			this.rootAction.AddDecimalFormat(name, formatinfo);
		}

		// Token: 0x06002FC1 RID: 12225 RVA: 0x000FEC3C File Offset: 0x000FCE3C
		private string[] ResolvePrefixes(string tokens)
		{
			if (tokens == null || tokens.Length == 0)
			{
				return null;
			}
			string[] array = XmlConvert.SplitString(tokens);
			try
			{
				for (int i = 0; i < array.Length; i++)
				{
					string text = array[i];
					array[i] = this.scopeManager.ResolveXmlNamespace((text == "#default") ? string.Empty : text);
				}
			}
			catch (XsltException)
			{
				if (!this.ForwardCompatibility)
				{
					throw;
				}
				return null;
			}
			return array;
		}

		// Token: 0x06002FC2 RID: 12226 RVA: 0x000FECB8 File Offset: 0x000FCEB8
		internal bool GetYesNo(string value)
		{
			if (value == "yes")
			{
				return true;
			}
			if (value == "no")
			{
				return false;
			}
			throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
			{
				this.Input.LocalName,
				value
			});
		}

		// Token: 0x06002FC3 RID: 12227 RVA: 0x000FED08 File Offset: 0x000FCF08
		internal string GetSingleAttribute(string attributeAtom)
		{
			NavigatorInput navigatorInput = this.Input;
			string localName = navigatorInput.LocalName;
			string text = null;
			if (navigatorInput.MoveToFirstAttribute())
			{
				string localName2;
				for (;;)
				{
					string namespaceURI = navigatorInput.NamespaceURI;
					localName2 = navigatorInput.LocalName;
					if (namespaceURI.Length == 0)
					{
						if (Ref.Equal(localName2, attributeAtom))
						{
							text = navigatorInput.Value;
						}
						else if (!this.ForwardCompatibility)
						{
							break;
						}
					}
					if (!navigatorInput.MoveToNextAttribute())
					{
						goto Block_4;
					}
				}
				throw XsltException.Create("'{0}' is an invalid attribute for the '{1}' element.", new string[]
				{
					localName2,
					localName
				});
				Block_4:
				navigatorInput.ToParent();
			}
			if (text == null)
			{
				throw XsltException.Create("Missing mandatory attribute '{0}'.", new string[]
				{
					attributeAtom
				});
			}
			return text;
		}

		// Token: 0x06002FC4 RID: 12228 RVA: 0x000FEDA4 File Offset: 0x000FCFA4
		internal XmlQualifiedName CreateXPathQName(string qname)
		{
			string prefix;
			string name;
			PrefixQName.ParseQualifiedName(qname, out prefix, out name);
			return new XmlQualifiedName(name, this.scopeManager.ResolveXPathNamespace(prefix));
		}

		// Token: 0x06002FC5 RID: 12229 RVA: 0x000FEDD0 File Offset: 0x000FCFD0
		internal XmlQualifiedName CreateXmlQName(string qname)
		{
			string prefix;
			string name;
			PrefixQName.ParseQualifiedName(qname, out prefix, out name);
			return new XmlQualifiedName(name, this.scopeManager.ResolveXmlNamespace(prefix));
		}

		// Token: 0x06002FC6 RID: 12230 RVA: 0x000FEDFC File Offset: 0x000FCFFC
		internal static XPathDocument LoadDocument(XmlTextReaderImpl reader)
		{
			reader.EntityHandling = EntityHandling.ExpandEntities;
			reader.XmlValidatingReaderCompatibilityMode = true;
			XPathDocument result;
			try
			{
				result = new XPathDocument(reader, XmlSpace.Preserve);
			}
			finally
			{
				reader.Close();
			}
			return result;
		}

		// Token: 0x06002FC7 RID: 12231 RVA: 0x000FEE3C File Offset: 0x000FD03C
		private void AddDocumentURI(string href)
		{
			this.documentURIs.Add(href, null);
		}

		// Token: 0x06002FC8 RID: 12232 RVA: 0x000FEE4B File Offset: 0x000FD04B
		private void RemoveDocumentURI(string href)
		{
			this.documentURIs.Remove(href);
		}

		// Token: 0x06002FC9 RID: 12233 RVA: 0x000FEE59 File Offset: 0x000FD059
		internal bool IsCircularReference(string href)
		{
			return this.documentURIs.Contains(href);
		}

		// Token: 0x06002FCA RID: 12234 RVA: 0x000FEE68 File Offset: 0x000FD068
		internal Uri ResolveUri(string relativeUri)
		{
			string baseURI = this.Input.BaseURI;
			Uri uri = this.xmlResolver.ResolveUri((baseURI.Length != 0) ? this.xmlResolver.ResolveUri(null, baseURI) : null, relativeUri);
			if (uri == null)
			{
				throw XsltException.Create("Cannot resolve the referenced document '{0}'.", new string[]
				{
					relativeUri
				});
			}
			return uri;
		}

		// Token: 0x06002FCB RID: 12235 RVA: 0x000FEEC8 File Offset: 0x000FD0C8
		internal NavigatorInput ResolveDocument(Uri absoluteUri)
		{
			object entity = this.xmlResolver.GetEntity(absoluteUri, null, null);
			string text = absoluteUri.ToString();
			if (entity is Stream)
			{
				return new NavigatorInput(Compiler.LoadDocument(new XmlTextReaderImpl(text, (Stream)entity)
				{
					XmlResolver = this.xmlResolver
				}).CreateNavigator(), text, this.rootScope);
			}
			if (entity is XPathNavigator)
			{
				return new NavigatorInput((XPathNavigator)entity, text, this.rootScope);
			}
			throw XsltException.Create("Cannot resolve the referenced document '{0}'.", new string[]
			{
				text
			});
		}

		// Token: 0x06002FCC RID: 12236 RVA: 0x000FEF54 File Offset: 0x000FD154
		internal void PushInputDocument(NavigatorInput newInput)
		{
			string href = newInput.Href;
			this.AddDocumentURI(href);
			newInput.Next = this.input;
			this.input = newInput;
			this.atoms = this.input.Atoms;
			this.scopeManager = this.input.InputScopeManager;
		}

		// Token: 0x06002FCD RID: 12237 RVA: 0x000FEFA4 File Offset: 0x000FD1A4
		internal void PopInputDocument()
		{
			NavigatorInput navigatorInput = this.input;
			this.input = navigatorInput.Next;
			navigatorInput.Next = null;
			if (this.input != null)
			{
				this.atoms = this.input.Atoms;
				this.scopeManager = this.input.InputScopeManager;
			}
			else
			{
				this.atoms = null;
				this.scopeManager = null;
			}
			this.RemoveDocumentURI(navigatorInput.Href);
			navigatorInput.Close();
		}

		// Token: 0x06002FCE RID: 12238 RVA: 0x000FF017 File Offset: 0x000FD217
		internal void PushStylesheet(Stylesheet stylesheet)
		{
			if (this.stylesheets == null)
			{
				this.stylesheets = new Stack();
			}
			this.stylesheets.Push(stylesheet);
			this.stylesheet = stylesheet;
		}

		// Token: 0x06002FCF RID: 12239 RVA: 0x000FF03F File Offset: 0x000FD23F
		internal Stylesheet PopStylesheet()
		{
			Stylesheet result = (Stylesheet)this.stylesheets.Pop();
			this.stylesheet = (Stylesheet)this.stylesheets.Peek();
			return result;
		}

		// Token: 0x06002FD0 RID: 12240 RVA: 0x000FF067 File Offset: 0x000FD267
		internal void AddAttributeSet(AttributeSetAction attributeSet)
		{
			this.stylesheet.AddAttributeSet(attributeSet);
		}

		// Token: 0x06002FD1 RID: 12241 RVA: 0x000FF075 File Offset: 0x000FD275
		internal void AddTemplate(TemplateAction template)
		{
			this.stylesheet.AddTemplate(template);
		}

		// Token: 0x06002FD2 RID: 12242 RVA: 0x000FF083 File Offset: 0x000FD283
		internal void BeginTemplate(TemplateAction template)
		{
			this.currentTemplate = template;
			this.currentMode = template.Mode;
			this.CanHaveApplyImports = (template.MatchKey != -1);
		}

		// Token: 0x06002FD3 RID: 12243 RVA: 0x000FF0AA File Offset: 0x000FD2AA
		internal void EndTemplate()
		{
			this.currentTemplate = this.rootAction;
		}

		// Token: 0x17000A1D RID: 2589
		// (get) Token: 0x06002FD4 RID: 12244 RVA: 0x000FF0B8 File Offset: 0x000FD2B8
		internal XmlQualifiedName CurrentMode
		{
			get
			{
				return this.currentMode;
			}
		}

		// Token: 0x06002FD5 RID: 12245 RVA: 0x000FF0C0 File Offset: 0x000FD2C0
		internal int AddQuery(string xpathQuery)
		{
			return this.AddQuery(xpathQuery, true, true, false);
		}

		// Token: 0x06002FD6 RID: 12246 RVA: 0x000FF0CC File Offset: 0x000FD2CC
		internal int AddQuery(string xpathQuery, bool allowVar, bool allowKey, bool isPattern)
		{
			CompiledXpathExpr compiledQuery;
			try
			{
				compiledQuery = new CompiledXpathExpr(isPattern ? this.queryBuilder.BuildPatternQuery(xpathQuery, allowVar, allowKey) : this.queryBuilder.Build(xpathQuery, allowVar, allowKey), xpathQuery, false);
			}
			catch (XPathException inner)
			{
				if (!this.ForwardCompatibility)
				{
					throw XsltException.Create("'{0}' is an invalid XPath expression.", new string[]
					{
						xpathQuery
					}, inner);
				}
				compiledQuery = new Compiler.ErrorXPathExpression(xpathQuery, this.Input.BaseURI, this.Input.LineNumber, this.Input.LinePosition);
			}
			this.queryStore.Add(new TheQuery(compiledQuery, this.scopeManager));
			return this.queryStore.Count - 1;
		}

		// Token: 0x06002FD7 RID: 12247 RVA: 0x000FF184 File Offset: 0x000FD384
		internal int AddStringQuery(string xpathQuery)
		{
			string xpathQuery2 = XmlCharType.Instance.IsOnlyWhitespace(xpathQuery) ? xpathQuery : ("string(" + xpathQuery + ")");
			return this.AddQuery(xpathQuery2);
		}

		// Token: 0x06002FD8 RID: 12248 RVA: 0x000FF1BC File Offset: 0x000FD3BC
		internal int AddBooleanQuery(string xpathQuery)
		{
			string xpathQuery2 = XmlCharType.Instance.IsOnlyWhitespace(xpathQuery) ? xpathQuery : ("boolean(" + xpathQuery + ")");
			return this.AddQuery(xpathQuery2);
		}

		// Token: 0x06002FD9 RID: 12249 RVA: 0x000FF1F4 File Offset: 0x000FD3F4
		private static string GenerateUniqueClassName()
		{
			return "ScriptClass_" + Interlocked.Increment(ref Compiler.scriptClassCounter);
		}

		// Token: 0x06002FDA RID: 12250 RVA: 0x000FF20F File Offset: 0x000FD40F
		private static void ValidateExtensionNamespace(string nsUri)
		{
			if (nsUri.Length == 0 || nsUri == "http://www.w3.org/1999/XSL/Transform")
			{
				throw XsltException.Create("Extension namespace cannot be 'null' or an XSLT namespace URI.", Array.Empty<string>());
			}
			XmlConvert.ToUri(nsUri);
		}

		// Token: 0x06002FDB RID: 12251 RVA: 0x000FF240 File Offset: 0x000FD440
		public string GetNsAlias(ref string prefix)
		{
			if (prefix == "#default")
			{
				prefix = string.Empty;
				return this.DefaultNamespace;
			}
			if (!PrefixQName.ValidatePrefix(prefix))
			{
				throw XsltException.Create("'{1}' is an invalid value for the '{0}' attribute.", new string[]
				{
					this.input.LocalName,
					prefix
				});
			}
			return this.ResolveXPathNamespace(prefix);
		}

		// Token: 0x06002FDC RID: 12252 RVA: 0x000FF2A0 File Offset: 0x000FD4A0
		private static void getTextLex(string avt, ref int start, StringBuilder lex)
		{
			int length = avt.Length;
			int i;
			for (i = start; i < length; i++)
			{
				char c = avt[i];
				if (c == '{')
				{
					if (i + 1 >= length || avt[i + 1] != '{')
					{
						break;
					}
					i++;
				}
				else if (c == '}')
				{
					if (i + 1 >= length || avt[i + 1] != '}')
					{
						throw XsltException.Create("Right curly brace in the attribute value template '{0}' must be doubled.", new string[]
						{
							avt
						});
					}
					i++;
				}
				lex.Append(c);
			}
			start = i;
		}

		// Token: 0x06002FDD RID: 12253 RVA: 0x000FF328 File Offset: 0x000FD528
		private static void getXPathLex(string avt, ref int start, StringBuilder lex)
		{
			int length = avt.Length;
			int num = 0;
			for (int i = start + 1; i < length; i++)
			{
				char c = avt[i];
				switch (num)
				{
				case 0:
					if (c <= '\'')
					{
						if (c != '"')
						{
							if (c == '\'')
							{
								num = 1;
							}
						}
						else
						{
							num = 2;
						}
					}
					else
					{
						if (c == '{')
						{
							throw XsltException.Create("AVT cannot be nested in AVT '{0}'.", new string[]
							{
								avt
							});
						}
						if (c == '}')
						{
							i++;
							if (i == start + 2)
							{
								throw XsltException.Create("XPath Expression in AVT cannot be empty: '{0}'.", new string[]
								{
									avt
								});
							}
							lex.Append(avt, start + 1, i - start - 2);
							start = i;
							return;
						}
					}
					break;
				case 1:
					if (c == '\'')
					{
						num = 0;
					}
					break;
				case 2:
					if (c == '"')
					{
						num = 0;
					}
					break;
				}
			}
			throw XsltException.Create((num == 0) ? "The braces are not closed in AVT expression '{0}'." : "The literal in AVT expression is not correctly closed '{0}'.", new string[]
			{
				avt
			});
		}

		// Token: 0x06002FDE RID: 12254 RVA: 0x000FF410 File Offset: 0x000FD610
		private static bool GetNextAvtLex(string avt, ref int start, StringBuilder lex, out bool isAvt)
		{
			isAvt = false;
			if (start == avt.Length)
			{
				return false;
			}
			lex.Length = 0;
			Compiler.getTextLex(avt, ref start, lex);
			if (lex.Length == 0)
			{
				isAvt = true;
				Compiler.getXPathLex(avt, ref start, lex);
			}
			return true;
		}

		// Token: 0x06002FDF RID: 12255 RVA: 0x000FF444 File Offset: 0x000FD644
		internal ArrayList CompileAvt(string avtText, out bool constant)
		{
			ArrayList arrayList = new ArrayList();
			constant = true;
			int num = 0;
			bool flag;
			while (Compiler.GetNextAvtLex(avtText, ref num, this.AvtStringBuilder, out flag))
			{
				string text = this.AvtStringBuilder.ToString();
				if (flag)
				{
					arrayList.Add(new AvtEvent(this.AddStringQuery(text)));
					constant = false;
				}
				else
				{
					arrayList.Add(new TextEvent(text));
				}
			}
			return arrayList;
		}

		// Token: 0x06002FE0 RID: 12256 RVA: 0x000FF4A8 File Offset: 0x000FD6A8
		internal ArrayList CompileAvt(string avtText)
		{
			bool flag;
			return this.CompileAvt(avtText, out flag);
		}

		// Token: 0x06002FE1 RID: 12257 RVA: 0x000FF4BE File Offset: 0x000FD6BE
		public virtual ApplyImportsAction CreateApplyImportsAction()
		{
			ApplyImportsAction applyImportsAction = new ApplyImportsAction();
			applyImportsAction.Compile(this);
			return applyImportsAction;
		}

		// Token: 0x06002FE2 RID: 12258 RVA: 0x000FF4CC File Offset: 0x000FD6CC
		public virtual ApplyTemplatesAction CreateApplyTemplatesAction()
		{
			ApplyTemplatesAction applyTemplatesAction = new ApplyTemplatesAction();
			applyTemplatesAction.Compile(this);
			return applyTemplatesAction;
		}

		// Token: 0x06002FE3 RID: 12259 RVA: 0x000FF4DA File Offset: 0x000FD6DA
		public virtual AttributeAction CreateAttributeAction()
		{
			AttributeAction attributeAction = new AttributeAction();
			attributeAction.Compile(this);
			return attributeAction;
		}

		// Token: 0x06002FE4 RID: 12260 RVA: 0x000FF4E8 File Offset: 0x000FD6E8
		public virtual AttributeSetAction CreateAttributeSetAction()
		{
			AttributeSetAction attributeSetAction = new AttributeSetAction();
			attributeSetAction.Compile(this);
			return attributeSetAction;
		}

		// Token: 0x06002FE5 RID: 12261 RVA: 0x000FF4F6 File Offset: 0x000FD6F6
		public virtual CallTemplateAction CreateCallTemplateAction()
		{
			CallTemplateAction callTemplateAction = new CallTemplateAction();
			callTemplateAction.Compile(this);
			return callTemplateAction;
		}

		// Token: 0x06002FE6 RID: 12262 RVA: 0x000FF504 File Offset: 0x000FD704
		public virtual ChooseAction CreateChooseAction()
		{
			ChooseAction chooseAction = new ChooseAction();
			chooseAction.Compile(this);
			return chooseAction;
		}

		// Token: 0x06002FE7 RID: 12263 RVA: 0x000FF512 File Offset: 0x000FD712
		public virtual CommentAction CreateCommentAction()
		{
			CommentAction commentAction = new CommentAction();
			commentAction.Compile(this);
			return commentAction;
		}

		// Token: 0x06002FE8 RID: 12264 RVA: 0x000FF520 File Offset: 0x000FD720
		public virtual CopyAction CreateCopyAction()
		{
			CopyAction copyAction = new CopyAction();
			copyAction.Compile(this);
			return copyAction;
		}

		// Token: 0x06002FE9 RID: 12265 RVA: 0x000FF52E File Offset: 0x000FD72E
		public virtual CopyOfAction CreateCopyOfAction()
		{
			CopyOfAction copyOfAction = new CopyOfAction();
			copyOfAction.Compile(this);
			return copyOfAction;
		}

		// Token: 0x06002FEA RID: 12266 RVA: 0x000FF53C File Offset: 0x000FD73C
		public virtual ElementAction CreateElementAction()
		{
			ElementAction elementAction = new ElementAction();
			elementAction.Compile(this);
			return elementAction;
		}

		// Token: 0x06002FEB RID: 12267 RVA: 0x000FF54A File Offset: 0x000FD74A
		public virtual ForEachAction CreateForEachAction()
		{
			ForEachAction forEachAction = new ForEachAction();
			forEachAction.Compile(this);
			return forEachAction;
		}

		// Token: 0x06002FEC RID: 12268 RVA: 0x000FF558 File Offset: 0x000FD758
		public virtual IfAction CreateIfAction(IfAction.ConditionType type)
		{
			IfAction ifAction = new IfAction(type);
			ifAction.Compile(this);
			return ifAction;
		}

		// Token: 0x06002FED RID: 12269 RVA: 0x000FF567 File Offset: 0x000FD767
		public virtual MessageAction CreateMessageAction()
		{
			MessageAction messageAction = new MessageAction();
			messageAction.Compile(this);
			return messageAction;
		}

		// Token: 0x06002FEE RID: 12270 RVA: 0x000FF575 File Offset: 0x000FD775
		public virtual NewInstructionAction CreateNewInstructionAction()
		{
			NewInstructionAction newInstructionAction = new NewInstructionAction();
			newInstructionAction.Compile(this);
			return newInstructionAction;
		}

		// Token: 0x06002FEF RID: 12271 RVA: 0x000FF583 File Offset: 0x000FD783
		public virtual NumberAction CreateNumberAction()
		{
			NumberAction numberAction = new NumberAction();
			numberAction.Compile(this);
			return numberAction;
		}

		// Token: 0x06002FF0 RID: 12272 RVA: 0x000FF591 File Offset: 0x000FD791
		public virtual ProcessingInstructionAction CreateProcessingInstructionAction()
		{
			ProcessingInstructionAction processingInstructionAction = new ProcessingInstructionAction();
			processingInstructionAction.Compile(this);
			return processingInstructionAction;
		}

		// Token: 0x06002FF1 RID: 12273 RVA: 0x000FF59F File Offset: 0x000FD79F
		public virtual void CreateRootAction()
		{
			this.RootAction = new RootAction();
			this.RootAction.Compile(this);
		}

		// Token: 0x06002FF2 RID: 12274 RVA: 0x000FF5B8 File Offset: 0x000FD7B8
		public virtual SortAction CreateSortAction()
		{
			SortAction sortAction = new SortAction();
			sortAction.Compile(this);
			return sortAction;
		}

		// Token: 0x06002FF3 RID: 12275 RVA: 0x000FF5C6 File Offset: 0x000FD7C6
		public virtual TemplateAction CreateTemplateAction()
		{
			TemplateAction templateAction = new TemplateAction();
			templateAction.Compile(this);
			return templateAction;
		}

		// Token: 0x06002FF4 RID: 12276 RVA: 0x000FF5D4 File Offset: 0x000FD7D4
		public virtual TemplateAction CreateSingleTemplateAction()
		{
			TemplateAction templateAction = new TemplateAction();
			templateAction.CompileSingle(this);
			return templateAction;
		}

		// Token: 0x06002FF5 RID: 12277 RVA: 0x000FF5E2 File Offset: 0x000FD7E2
		public virtual TextAction CreateTextAction()
		{
			TextAction textAction = new TextAction();
			textAction.Compile(this);
			return textAction;
		}

		// Token: 0x06002FF6 RID: 12278 RVA: 0x000FF5F0 File Offset: 0x000FD7F0
		public virtual UseAttributeSetsAction CreateUseAttributeSetsAction()
		{
			UseAttributeSetsAction useAttributeSetsAction = new UseAttributeSetsAction();
			useAttributeSetsAction.Compile(this);
			return useAttributeSetsAction;
		}

		// Token: 0x06002FF7 RID: 12279 RVA: 0x000FF5FE File Offset: 0x000FD7FE
		public virtual ValueOfAction CreateValueOfAction()
		{
			ValueOfAction valueOfAction = new ValueOfAction();
			valueOfAction.Compile(this);
			return valueOfAction;
		}

		// Token: 0x06002FF8 RID: 12280 RVA: 0x000FF60C File Offset: 0x000FD80C
		public virtual VariableAction CreateVariableAction(VariableType type)
		{
			VariableAction variableAction = new VariableAction(type);
			variableAction.Compile(this);
			if (variableAction.VarKey != -1)
			{
				return variableAction;
			}
			return null;
		}

		// Token: 0x06002FF9 RID: 12281 RVA: 0x000FF633 File Offset: 0x000FD833
		public virtual WithParamAction CreateWithParamAction()
		{
			WithParamAction withParamAction = new WithParamAction();
			withParamAction.Compile(this);
			return withParamAction;
		}

		// Token: 0x06002FFA RID: 12282 RVA: 0x000FF641 File Offset: 0x000FD841
		public virtual BeginEvent CreateBeginEvent()
		{
			return new BeginEvent(this);
		}

		// Token: 0x06002FFB RID: 12283 RVA: 0x000FF649 File Offset: 0x000FD849
		public virtual TextEvent CreateTextEvent()
		{
			return new TextEvent(this);
		}

		// Token: 0x06002FFC RID: 12284 RVA: 0x000FF654 File Offset: 0x000FD854
		public XsltException UnexpectedKeyword()
		{
			XPathNavigator xpathNavigator = this.Input.Navigator.Clone();
			string name = xpathNavigator.Name;
			xpathNavigator.MoveToParent();
			string name2 = xpathNavigator.Name;
			return XsltException.Create("'{0}' cannot be a child of the '{1}' element.", new string[]
			{
				name,
				name2
			});
		}

		// Token: 0x06002FFD RID: 12285 RVA: 0x000FF6A0 File Offset: 0x000FD8A0
		public Compiler()
		{
		}

		// Token: 0x06002FFE RID: 12286 RVA: 0x000FF704 File Offset: 0x000FD904
		// Note: this type is marked as 'beforefieldinit'.
		static Compiler()
		{
		}

		// Token: 0x04001FE5 RID: 8165
		internal const int InvalidQueryKey = -1;

		// Token: 0x04001FE6 RID: 8166
		internal const double RootPriority = 0.5;

		// Token: 0x04001FE7 RID: 8167
		internal StringBuilder AvtStringBuilder = new StringBuilder();

		// Token: 0x04001FE8 RID: 8168
		private int stylesheetid;

		// Token: 0x04001FE9 RID: 8169
		private InputScope rootScope;

		// Token: 0x04001FEA RID: 8170
		private XmlResolver xmlResolver;

		// Token: 0x04001FEB RID: 8171
		private TemplateBaseAction currentTemplate;

		// Token: 0x04001FEC RID: 8172
		private XmlQualifiedName currentMode;

		// Token: 0x04001FED RID: 8173
		private Hashtable globalNamespaceAliasTable;

		// Token: 0x04001FEE RID: 8174
		private Stack stylesheets;

		// Token: 0x04001FEF RID: 8175
		private HybridDictionary documentURIs = new HybridDictionary();

		// Token: 0x04001FF0 RID: 8176
		private NavigatorInput input;

		// Token: 0x04001FF1 RID: 8177
		private KeywordsTable atoms;

		// Token: 0x04001FF2 RID: 8178
		private InputScopeManager scopeManager;

		// Token: 0x04001FF3 RID: 8179
		internal Stylesheet stylesheet;

		// Token: 0x04001FF4 RID: 8180
		internal Stylesheet rootStylesheet;

		// Token: 0x04001FF5 RID: 8181
		private RootAction rootAction;

		// Token: 0x04001FF6 RID: 8182
		private List<TheQuery> queryStore;

		// Token: 0x04001FF7 RID: 8183
		private QueryBuilder queryBuilder = new QueryBuilder();

		// Token: 0x04001FF8 RID: 8184
		private int rtfCount;

		// Token: 0x04001FF9 RID: 8185
		public bool AllowBuiltInMode;

		// Token: 0x04001FFA RID: 8186
		public static XmlQualifiedName BuiltInMode = new XmlQualifiedName("*", string.Empty);

		// Token: 0x04001FFB RID: 8187
		private Hashtable[] _typeDeclsByLang = new Hashtable[]
		{
			new Hashtable(),
			new Hashtable(),
			new Hashtable()
		};

		// Token: 0x04001FFC RID: 8188
		private ArrayList scriptFiles = new ArrayList();

		// Token: 0x04001FFD RID: 8189
		private static string[] _defaultNamespaces = new string[]
		{
			"System",
			"System.Collections",
			"System.Text",
			"System.Text.RegularExpressions",
			"System.Xml",
			"System.Xml.Xsl",
			"System.Xml.XPath"
		};

		// Token: 0x04001FFE RID: 8190
		private static int scriptClassCounter = 0;

		// Token: 0x020004C0 RID: 1216
		internal class ErrorXPathExpression : CompiledXpathExpr
		{
			// Token: 0x06002FFF RID: 12287 RVA: 0x000FF76E File Offset: 0x000FD96E
			public ErrorXPathExpression(string expression, string baseUri, int lineNumber, int linePosition) : base(null, expression, false)
			{
				this.baseUri = baseUri;
				this.lineNumber = lineNumber;
				this.linePosition = linePosition;
			}

			// Token: 0x06003000 RID: 12288 RVA: 0x00002068 File Offset: 0x00000268
			public override XPathExpression Clone()
			{
				return this;
			}

			// Token: 0x06003001 RID: 12289 RVA: 0x000FF78F File Offset: 0x000FD98F
			public override void CheckErrors()
			{
				throw new XsltException("'{0}' is an invalid XPath expression.", new string[]
				{
					this.Expression
				}, this.baseUri, this.linePosition, this.lineNumber, null);
			}

			// Token: 0x04001FFF RID: 8191
			private string baseUri;

			// Token: 0x04002000 RID: 8192
			private int lineNumber;

			// Token: 0x04002001 RID: 8193
			private int linePosition;
		}
	}
}
