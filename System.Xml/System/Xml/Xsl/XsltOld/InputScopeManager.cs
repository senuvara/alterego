﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004EE RID: 1262
	internal class InputScopeManager
	{
		// Token: 0x06003109 RID: 12553 RVA: 0x00102DE2 File Offset: 0x00100FE2
		public InputScopeManager(XPathNavigator navigator, InputScope rootScope)
		{
			this.navigator = navigator;
			this.scopeStack = rootScope;
		}

		// Token: 0x17000A36 RID: 2614
		// (get) Token: 0x0600310A RID: 12554 RVA: 0x00102E03 File Offset: 0x00101003
		internal InputScope CurrentScope
		{
			get
			{
				return this.scopeStack;
			}
		}

		// Token: 0x17000A37 RID: 2615
		// (get) Token: 0x0600310B RID: 12555 RVA: 0x00102E0B File Offset: 0x0010100B
		internal InputScope VariableScope
		{
			get
			{
				return this.scopeStack.Parent;
			}
		}

		// Token: 0x0600310C RID: 12556 RVA: 0x00102E18 File Offset: 0x00101018
		internal InputScopeManager Clone()
		{
			return new InputScopeManager(this.navigator, null)
			{
				scopeStack = this.scopeStack,
				defaultNS = this.defaultNS
			};
		}

		// Token: 0x17000A38 RID: 2616
		// (get) Token: 0x0600310D RID: 12557 RVA: 0x00102E3E File Offset: 0x0010103E
		public XPathNavigator Navigator
		{
			get
			{
				return this.navigator;
			}
		}

		// Token: 0x0600310E RID: 12558 RVA: 0x00102E46 File Offset: 0x00101046
		internal InputScope PushScope()
		{
			this.scopeStack = new InputScope(this.scopeStack);
			return this.scopeStack;
		}

		// Token: 0x0600310F RID: 12559 RVA: 0x00102E60 File Offset: 0x00101060
		internal void PopScope()
		{
			if (this.scopeStack == null)
			{
				return;
			}
			for (NamespaceDecl namespaceDecl = this.scopeStack.Scopes; namespaceDecl != null; namespaceDecl = namespaceDecl.Next)
			{
				this.defaultNS = namespaceDecl.PrevDefaultNsUri;
			}
			this.scopeStack = this.scopeStack.Parent;
		}

		// Token: 0x06003110 RID: 12560 RVA: 0x00102EAB File Offset: 0x001010AB
		internal void PushNamespace(string prefix, string nspace)
		{
			this.scopeStack.AddNamespace(prefix, nspace, this.defaultNS);
			if (prefix == null || prefix.Length == 0)
			{
				this.defaultNS = nspace;
			}
		}

		// Token: 0x17000A39 RID: 2617
		// (get) Token: 0x06003111 RID: 12561 RVA: 0x00102ED3 File Offset: 0x001010D3
		public string DefaultNamespace
		{
			get
			{
				return this.defaultNS;
			}
		}

		// Token: 0x06003112 RID: 12562 RVA: 0x00102EDC File Offset: 0x001010DC
		private string ResolveNonEmptyPrefix(string prefix)
		{
			if (prefix == "xml")
			{
				return "http://www.w3.org/XML/1998/namespace";
			}
			if (prefix == "xmlns")
			{
				return "http://www.w3.org/2000/xmlns/";
			}
			for (InputScope parent = this.scopeStack; parent != null; parent = parent.Parent)
			{
				string text = parent.ResolveNonAtom(prefix);
				if (text != null)
				{
					return text;
				}
			}
			throw XsltException.Create("Prefix '{0}' is not defined.", new string[]
			{
				prefix
			});
		}

		// Token: 0x06003113 RID: 12563 RVA: 0x00102F43 File Offset: 0x00101143
		public string ResolveXmlNamespace(string prefix)
		{
			if (prefix.Length == 0)
			{
				return this.defaultNS;
			}
			return this.ResolveNonEmptyPrefix(prefix);
		}

		// Token: 0x06003114 RID: 12564 RVA: 0x00102F5B File Offset: 0x0010115B
		public string ResolveXPathNamespace(string prefix)
		{
			if (prefix.Length == 0)
			{
				return string.Empty;
			}
			return this.ResolveNonEmptyPrefix(prefix);
		}

		// Token: 0x06003115 RID: 12565 RVA: 0x00102F74 File Offset: 0x00101174
		internal void InsertExtensionNamespaces(string[] nsList)
		{
			for (int i = 0; i < nsList.Length; i++)
			{
				this.scopeStack.InsertExtensionNamespace(nsList[i]);
			}
		}

		// Token: 0x06003116 RID: 12566 RVA: 0x00102FA0 File Offset: 0x001011A0
		internal bool IsExtensionNamespace(string nspace)
		{
			for (InputScope parent = this.scopeStack; parent != null; parent = parent.Parent)
			{
				if (parent.IsExtensionNamespace(nspace))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003117 RID: 12567 RVA: 0x00102FCC File Offset: 0x001011CC
		internal void InsertExcludedNamespaces(string[] nsList)
		{
			for (int i = 0; i < nsList.Length; i++)
			{
				this.scopeStack.InsertExcludedNamespace(nsList[i]);
			}
		}

		// Token: 0x06003118 RID: 12568 RVA: 0x00102FF8 File Offset: 0x001011F8
		internal bool IsExcludedNamespace(string nspace)
		{
			for (InputScope parent = this.scopeStack; parent != null; parent = parent.Parent)
			{
				if (parent.IsExcludedNamespace(nspace))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0400206F RID: 8303
		private InputScope scopeStack;

		// Token: 0x04002070 RID: 8304
		private string defaultNS = string.Empty;

		// Token: 0x04002071 RID: 8305
		private XPathNavigator navigator;
	}
}
