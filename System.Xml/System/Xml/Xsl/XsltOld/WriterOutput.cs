﻿using System;
using System.Collections;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x0200051F RID: 1311
	internal class WriterOutput : RecordOutput
	{
		// Token: 0x060032FC RID: 13052 RVA: 0x0010A84D File Offset: 0x00108A4D
		internal WriterOutput(Processor processor, XmlWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			this.writer = writer;
			this.processor = processor;
		}

		// Token: 0x060032FD RID: 13053 RVA: 0x0010A874 File Offset: 0x00108A74
		public Processor.OutputResult RecordDone(RecordBuilder record)
		{
			BuilderInfo mainNode = record.MainNode;
			switch (mainNode.NodeType)
			{
			case XmlNodeType.Element:
				this.writer.WriteStartElement(mainNode.Prefix, mainNode.LocalName, mainNode.NamespaceURI);
				this.WriteAttributes(record.AttributeList, record.AttributeCount);
				if (mainNode.IsEmptyTag)
				{
					this.writer.WriteEndElement();
				}
				break;
			case XmlNodeType.Text:
			case XmlNodeType.Whitespace:
			case XmlNodeType.SignificantWhitespace:
				this.writer.WriteString(mainNode.Value);
				break;
			case XmlNodeType.CDATA:
				this.writer.WriteCData(mainNode.Value);
				break;
			case XmlNodeType.EntityReference:
				this.writer.WriteEntityRef(mainNode.LocalName);
				break;
			case XmlNodeType.ProcessingInstruction:
				this.writer.WriteProcessingInstruction(mainNode.LocalName, mainNode.Value);
				break;
			case XmlNodeType.Comment:
				this.writer.WriteComment(mainNode.Value);
				break;
			case XmlNodeType.DocumentType:
				this.writer.WriteRaw(mainNode.Value);
				break;
			case XmlNodeType.EndElement:
				this.writer.WriteFullEndElement();
				break;
			}
			record.Reset();
			return Processor.OutputResult.Continue;
		}

		// Token: 0x060032FE RID: 13054 RVA: 0x0010A9B2 File Offset: 0x00108BB2
		public void TheEnd()
		{
			this.writer.Flush();
			this.writer = null;
		}

		// Token: 0x060032FF RID: 13055 RVA: 0x0010A9C8 File Offset: 0x00108BC8
		private void WriteAttributes(ArrayList list, int count)
		{
			for (int i = 0; i < count; i++)
			{
				BuilderInfo builderInfo = (BuilderInfo)list[i];
				this.writer.WriteAttributeString(builderInfo.Prefix, builderInfo.LocalName, builderInfo.NamespaceURI, builderInfo.Value);
			}
		}

		// Token: 0x040021CD RID: 8653
		private XmlWriter writer;

		// Token: 0x040021CE RID: 8654
		private Processor processor;
	}
}
