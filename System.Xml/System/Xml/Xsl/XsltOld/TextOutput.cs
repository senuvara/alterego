﻿using System;
using System.IO;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000517 RID: 1303
	internal class TextOutput : SequentialOutput
	{
		// Token: 0x060032D7 RID: 13015 RVA: 0x0010A11B File Offset: 0x0010831B
		internal TextOutput(Processor processor, Stream stream) : base(processor)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			this.encoding = processor.Output.Encoding;
			this.writer = new StreamWriter(stream, this.encoding);
		}

		// Token: 0x060032D8 RID: 13016 RVA: 0x0010A155 File Offset: 0x00108355
		internal TextOutput(Processor processor, TextWriter writer) : base(processor)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			this.encoding = writer.Encoding;
			this.writer = writer;
		}

		// Token: 0x060032D9 RID: 13017 RVA: 0x0010A17F File Offset: 0x0010837F
		internal override void Write(char outputChar)
		{
			this.writer.Write(outputChar);
		}

		// Token: 0x060032DA RID: 13018 RVA: 0x0010A18D File Offset: 0x0010838D
		internal override void Write(string outputText)
		{
			this.writer.Write(outputText);
		}

		// Token: 0x060032DB RID: 13019 RVA: 0x0010A19B File Offset: 0x0010839B
		internal override void Close()
		{
			this.writer.Flush();
			this.writer = null;
		}

		// Token: 0x040021B3 RID: 8627
		private TextWriter writer;
	}
}
