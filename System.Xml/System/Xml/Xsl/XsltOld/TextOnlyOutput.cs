﻿using System;
using System.IO;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000516 RID: 1302
	internal class TextOnlyOutput : RecordOutput
	{
		// Token: 0x17000AA5 RID: 2725
		// (get) Token: 0x060032D1 RID: 13009 RVA: 0x0010A060 File Offset: 0x00108260
		internal XsltOutput Output
		{
			get
			{
				return this.processor.Output;
			}
		}

		// Token: 0x17000AA6 RID: 2726
		// (get) Token: 0x060032D2 RID: 13010 RVA: 0x0010A06D File Offset: 0x0010826D
		public TextWriter Writer
		{
			get
			{
				return this.writer;
			}
		}

		// Token: 0x060032D3 RID: 13011 RVA: 0x0010A075 File Offset: 0x00108275
		internal TextOnlyOutput(Processor processor, Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			this.processor = processor;
			this.writer = new StreamWriter(stream, this.Output.Encoding);
		}

		// Token: 0x060032D4 RID: 13012 RVA: 0x0010A0A9 File Offset: 0x001082A9
		internal TextOnlyOutput(Processor processor, TextWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			this.processor = processor;
			this.writer = writer;
		}

		// Token: 0x060032D5 RID: 13013 RVA: 0x0010A0D0 File Offset: 0x001082D0
		public Processor.OutputResult RecordDone(RecordBuilder record)
		{
			BuilderInfo mainNode = record.MainNode;
			XmlNodeType nodeType = mainNode.NodeType;
			if (nodeType == XmlNodeType.Text || nodeType - XmlNodeType.Whitespace <= 1)
			{
				this.writer.Write(mainNode.Value);
			}
			record.Reset();
			return Processor.OutputResult.Continue;
		}

		// Token: 0x060032D6 RID: 13014 RVA: 0x0010A10E File Offset: 0x0010830E
		public void TheEnd()
		{
			this.writer.Flush();
		}

		// Token: 0x040021B1 RID: 8625
		private Processor processor;

		// Token: 0x040021B2 RID: 8626
		private TextWriter writer;
	}
}
