﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.XPath;
using System.Xml.Xsl.XsltOld.Debugger;
using MS.Internal.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004AF RID: 1199
	internal class ActionFrame : IStackFrame
	{
		// Token: 0x170009FC RID: 2556
		// (get) Token: 0x06002F23 RID: 12067 RVA: 0x000FCFA3 File Offset: 0x000FB1A3
		// (set) Token: 0x06002F24 RID: 12068 RVA: 0x000FCFAB File Offset: 0x000FB1AB
		internal PrefixQName CalulatedName
		{
			get
			{
				return this.calulatedName;
			}
			set
			{
				this.calulatedName = value;
			}
		}

		// Token: 0x170009FD RID: 2557
		// (get) Token: 0x06002F25 RID: 12069 RVA: 0x000FCFB4 File Offset: 0x000FB1B4
		// (set) Token: 0x06002F26 RID: 12070 RVA: 0x000FCFBC File Offset: 0x000FB1BC
		internal string StoredOutput
		{
			get
			{
				return this.storedOutput;
			}
			set
			{
				this.storedOutput = value;
			}
		}

		// Token: 0x170009FE RID: 2558
		// (get) Token: 0x06002F27 RID: 12071 RVA: 0x000FCFC5 File Offset: 0x000FB1C5
		// (set) Token: 0x06002F28 RID: 12072 RVA: 0x000FCFCD File Offset: 0x000FB1CD
		internal int State
		{
			get
			{
				return this.state;
			}
			set
			{
				this.state = value;
			}
		}

		// Token: 0x170009FF RID: 2559
		// (get) Token: 0x06002F29 RID: 12073 RVA: 0x000FCFD6 File Offset: 0x000FB1D6
		// (set) Token: 0x06002F2A RID: 12074 RVA: 0x000FCFDE File Offset: 0x000FB1DE
		internal int Counter
		{
			get
			{
				return this.counter;
			}
			set
			{
				this.counter = value;
			}
		}

		// Token: 0x17000A00 RID: 2560
		// (get) Token: 0x06002F2B RID: 12075 RVA: 0x000FCFE7 File Offset: 0x000FB1E7
		internal ActionFrame Container
		{
			get
			{
				return this.container;
			}
		}

		// Token: 0x17000A01 RID: 2561
		// (get) Token: 0x06002F2C RID: 12076 RVA: 0x000FCFEF File Offset: 0x000FB1EF
		internal XPathNavigator Node
		{
			get
			{
				if (this.nodeSet != null)
				{
					return this.nodeSet.Current;
				}
				return null;
			}
		}

		// Token: 0x17000A02 RID: 2562
		// (get) Token: 0x06002F2D RID: 12077 RVA: 0x000FD006 File Offset: 0x000FB206
		internal XPathNodeIterator NodeSet
		{
			get
			{
				return this.nodeSet;
			}
		}

		// Token: 0x17000A03 RID: 2563
		// (get) Token: 0x06002F2E RID: 12078 RVA: 0x000FD00E File Offset: 0x000FB20E
		internal XPathNodeIterator NewNodeSet
		{
			get
			{
				return this.newNodeSet;
			}
		}

		// Token: 0x06002F2F RID: 12079 RVA: 0x000FD018 File Offset: 0x000FB218
		internal int IncrementCounter()
		{
			int result = this.counter + 1;
			this.counter = result;
			return result;
		}

		// Token: 0x06002F30 RID: 12080 RVA: 0x000FD036 File Offset: 0x000FB236
		internal void AllocateVariables(int count)
		{
			if (0 < count)
			{
				this.variables = new object[count];
				return;
			}
			this.variables = null;
		}

		// Token: 0x06002F31 RID: 12081 RVA: 0x000FD050 File Offset: 0x000FB250
		internal object GetVariable(int index)
		{
			return this.variables[index];
		}

		// Token: 0x06002F32 RID: 12082 RVA: 0x000FD05A File Offset: 0x000FB25A
		internal void SetVariable(int index, object value)
		{
			this.variables[index] = value;
		}

		// Token: 0x06002F33 RID: 12083 RVA: 0x000FD065 File Offset: 0x000FB265
		internal void SetParameter(XmlQualifiedName name, object value)
		{
			if (this.withParams == null)
			{
				this.withParams = new Hashtable();
			}
			this.withParams[name] = value;
		}

		// Token: 0x06002F34 RID: 12084 RVA: 0x000FD087 File Offset: 0x000FB287
		internal void ResetParams()
		{
			if (this.withParams != null)
			{
				this.withParams.Clear();
			}
		}

		// Token: 0x06002F35 RID: 12085 RVA: 0x000FD09C File Offset: 0x000FB29C
		internal object GetParameter(XmlQualifiedName name)
		{
			if (this.withParams != null)
			{
				return this.withParams[name];
			}
			return null;
		}

		// Token: 0x06002F36 RID: 12086 RVA: 0x000FD0B4 File Offset: 0x000FB2B4
		internal void InitNodeSet(XPathNodeIterator nodeSet)
		{
			this.nodeSet = nodeSet;
		}

		// Token: 0x06002F37 RID: 12087 RVA: 0x000FD0BD File Offset: 0x000FB2BD
		internal void InitNewNodeSet(XPathNodeIterator nodeSet)
		{
			this.newNodeSet = nodeSet;
		}

		// Token: 0x06002F38 RID: 12088 RVA: 0x000FD0C8 File Offset: 0x000FB2C8
		internal void SortNewNodeSet(Processor proc, ArrayList sortarray)
		{
			int count = sortarray.Count;
			XPathSortComparer xpathSortComparer = new XPathSortComparer(count);
			for (int i = 0; i < count; i++)
			{
				Sort sort = (Sort)sortarray[i];
				Query compiledQuery = proc.GetCompiledQuery(sort.select);
				xpathSortComparer.AddSort(compiledQuery, new XPathComparerHelper(sort.order, sort.caseOrder, sort.lang, sort.dataType));
			}
			List<SortKey> list = new List<SortKey>();
			while (this.NewNextNode(proc))
			{
				XPathNodeIterator xpathNodeIterator = this.nodeSet;
				this.nodeSet = this.newNodeSet;
				SortKey sortKey = new SortKey(count, list.Count, this.newNodeSet.Current.Clone());
				for (int j = 0; j < count; j++)
				{
					sortKey[j] = xpathSortComparer.Expression(j).Evaluate(this.newNodeSet);
				}
				list.Add(sortKey);
				this.nodeSet = xpathNodeIterator;
			}
			list.Sort(xpathSortComparer);
			this.newNodeSet = new ActionFrame.XPathSortArrayIterator(list);
		}

		// Token: 0x06002F39 RID: 12089 RVA: 0x000FD1C7 File Offset: 0x000FB3C7
		internal void Finished()
		{
			this.State = -1;
		}

		// Token: 0x06002F3A RID: 12090 RVA: 0x000FD1D0 File Offset: 0x000FB3D0
		internal void Inherit(ActionFrame parent)
		{
			this.variables = parent.variables;
		}

		// Token: 0x06002F3B RID: 12091 RVA: 0x000FD1DE File Offset: 0x000FB3DE
		private void Init(Action action, ActionFrame container, XPathNodeIterator nodeSet)
		{
			this.state = 0;
			this.action = action;
			this.container = container;
			this.currentAction = 0;
			this.nodeSet = nodeSet;
			this.newNodeSet = null;
		}

		// Token: 0x06002F3C RID: 12092 RVA: 0x000FD20A File Offset: 0x000FB40A
		internal void Init(Action action, XPathNodeIterator nodeSet)
		{
			this.Init(action, null, nodeSet);
		}

		// Token: 0x06002F3D RID: 12093 RVA: 0x000FD215 File Offset: 0x000FB415
		internal void Init(ActionFrame containerFrame, XPathNodeIterator nodeSet)
		{
			this.Init(containerFrame.GetAction(0), containerFrame, nodeSet);
		}

		// Token: 0x06002F3E RID: 12094 RVA: 0x000FD226 File Offset: 0x000FB426
		internal void SetAction(Action action)
		{
			this.SetAction(action, 0);
		}

		// Token: 0x06002F3F RID: 12095 RVA: 0x000FD230 File Offset: 0x000FB430
		internal void SetAction(Action action, int state)
		{
			this.action = action;
			this.state = state;
		}

		// Token: 0x06002F40 RID: 12096 RVA: 0x000FD240 File Offset: 0x000FB440
		private Action GetAction(int actionIndex)
		{
			return ((ContainerAction)this.action).GetAction(actionIndex);
		}

		// Token: 0x06002F41 RID: 12097 RVA: 0x000FD253 File Offset: 0x000FB453
		internal void Exit()
		{
			this.Finished();
			this.container = null;
		}

		// Token: 0x06002F42 RID: 12098 RVA: 0x000FD264 File Offset: 0x000FB464
		internal bool Execute(Processor processor)
		{
			if (this.action == null)
			{
				return true;
			}
			this.action.Execute(processor, this);
			if (this.State == -1)
			{
				if (this.container != null)
				{
					this.currentAction++;
					this.action = this.container.GetAction(this.currentAction);
					this.State = 0;
				}
				else
				{
					this.action = null;
				}
				return this.action == null;
			}
			return false;
		}

		// Token: 0x06002F43 RID: 12099 RVA: 0x000FD2DC File Offset: 0x000FB4DC
		internal bool NextNode(Processor proc)
		{
			bool flag = this.nodeSet.MoveNext();
			if (flag && proc.Stylesheet.Whitespace)
			{
				XPathNodeType nodeType = this.nodeSet.Current.NodeType;
				if (nodeType == XPathNodeType.Whitespace)
				{
					XPathNavigator xpathNavigator = this.nodeSet.Current.Clone();
					bool flag2;
					do
					{
						xpathNavigator.MoveTo(this.nodeSet.Current);
						xpathNavigator.MoveToParent();
						flag2 = (!proc.Stylesheet.PreserveWhiteSpace(proc, xpathNavigator) && (flag = this.nodeSet.MoveNext()));
						nodeType = this.nodeSet.Current.NodeType;
					}
					while (flag2 && nodeType == XPathNodeType.Whitespace);
				}
			}
			return flag;
		}

		// Token: 0x06002F44 RID: 12100 RVA: 0x000FD380 File Offset: 0x000FB580
		internal bool NewNextNode(Processor proc)
		{
			bool flag = this.newNodeSet.MoveNext();
			if (flag && proc.Stylesheet.Whitespace)
			{
				XPathNodeType nodeType = this.newNodeSet.Current.NodeType;
				if (nodeType == XPathNodeType.Whitespace)
				{
					XPathNavigator xpathNavigator = this.newNodeSet.Current.Clone();
					bool flag2;
					do
					{
						xpathNavigator.MoveTo(this.newNodeSet.Current);
						xpathNavigator.MoveToParent();
						flag2 = (!proc.Stylesheet.PreserveWhiteSpace(proc, xpathNavigator) && (flag = this.newNodeSet.MoveNext()));
						nodeType = this.newNodeSet.Current.NodeType;
					}
					while (flag2 && nodeType == XPathNodeType.Whitespace);
				}
			}
			return flag;
		}

		// Token: 0x17000A04 RID: 2564
		// (get) Token: 0x06002F45 RID: 12101 RVA: 0x000FD424 File Offset: 0x000FB624
		XPathNavigator IStackFrame.Instruction
		{
			get
			{
				if (this.action == null)
				{
					return null;
				}
				return this.action.GetDbgData(this).StyleSheet;
			}
		}

		// Token: 0x17000A05 RID: 2565
		// (get) Token: 0x06002F46 RID: 12102 RVA: 0x000FD441 File Offset: 0x000FB641
		XPathNodeIterator IStackFrame.NodeSet
		{
			get
			{
				return this.nodeSet.Clone();
			}
		}

		// Token: 0x06002F47 RID: 12103 RVA: 0x000FD44E File Offset: 0x000FB64E
		int IStackFrame.GetVariablesCount()
		{
			if (this.action == null)
			{
				return 0;
			}
			return this.action.GetDbgData(this).Variables.Length;
		}

		// Token: 0x06002F48 RID: 12104 RVA: 0x000FD46D File Offset: 0x000FB66D
		XPathNavigator IStackFrame.GetVariable(int varIndex)
		{
			return this.action.GetDbgData(this).Variables[varIndex].GetDbgData(null).StyleSheet;
		}

		// Token: 0x06002F49 RID: 12105 RVA: 0x000FD48D File Offset: 0x000FB68D
		object IStackFrame.GetVariableValue(int varIndex)
		{
			return this.GetVariable(this.action.GetDbgData(this).Variables[varIndex].VarKey);
		}

		// Token: 0x06002F4A RID: 12106 RVA: 0x00002103 File Offset: 0x00000303
		public ActionFrame()
		{
		}

		// Token: 0x04001FA7 RID: 8103
		private int state;

		// Token: 0x04001FA8 RID: 8104
		private int counter;

		// Token: 0x04001FA9 RID: 8105
		private object[] variables;

		// Token: 0x04001FAA RID: 8106
		private Hashtable withParams;

		// Token: 0x04001FAB RID: 8107
		private Action action;

		// Token: 0x04001FAC RID: 8108
		private ActionFrame container;

		// Token: 0x04001FAD RID: 8109
		private int currentAction;

		// Token: 0x04001FAE RID: 8110
		private XPathNodeIterator nodeSet;

		// Token: 0x04001FAF RID: 8111
		private XPathNodeIterator newNodeSet;

		// Token: 0x04001FB0 RID: 8112
		private PrefixQName calulatedName;

		// Token: 0x04001FB1 RID: 8113
		private string storedOutput;

		// Token: 0x020004B0 RID: 1200
		private class XPathSortArrayIterator : XPathArrayIterator
		{
			// Token: 0x06002F4B RID: 12107 RVA: 0x000FD4AD File Offset: 0x000FB6AD
			public XPathSortArrayIterator(List<SortKey> list) : base(list)
			{
			}

			// Token: 0x06002F4C RID: 12108 RVA: 0x000FD4B6 File Offset: 0x000FB6B6
			public XPathSortArrayIterator(ActionFrame.XPathSortArrayIterator it) : base(it)
			{
			}

			// Token: 0x06002F4D RID: 12109 RVA: 0x000FD4BF File Offset: 0x000FB6BF
			public override XPathNodeIterator Clone()
			{
				return new ActionFrame.XPathSortArrayIterator(this);
			}

			// Token: 0x17000A06 RID: 2566
			// (get) Token: 0x06002F4E RID: 12110 RVA: 0x000FD4C7 File Offset: 0x000FB6C7
			public override XPathNavigator Current
			{
				get
				{
					return ((SortKey)this.list[this.index - 1]).Node;
				}
			}
		}
	}
}
