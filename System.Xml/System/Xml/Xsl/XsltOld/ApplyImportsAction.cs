﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004B1 RID: 1201
	internal class ApplyImportsAction : CompiledAction
	{
		// Token: 0x06002F4F RID: 12111 RVA: 0x000FD4E6 File Offset: 0x000FB6E6
		internal override void Compile(Compiler compiler)
		{
			base.CheckEmpty(compiler);
			if (!compiler.CanHaveApplyImports)
			{
				throw XsltException.Create("The 'xsl:apply-imports' instruction cannot be included within the content of an 'xsl:for-each' instruction or within an 'xsl:template' instruction without the 'match' attribute.", Array.Empty<string>());
			}
			this.mode = compiler.CurrentMode;
			this.stylesheet = compiler.CompiledStylesheet;
		}

		// Token: 0x06002F50 RID: 12112 RVA: 0x000FD520 File Offset: 0x000FB720
		internal override void Execute(Processor processor, ActionFrame frame)
		{
			int state = frame.State;
			if (state == 0)
			{
				processor.PushTemplateLookup(frame.NodeSet, this.mode, this.stylesheet);
				frame.State = 2;
				return;
			}
			if (state != 2)
			{
				return;
			}
			frame.Finished();
		}

		// Token: 0x06002F51 RID: 12113 RVA: 0x000FD562 File Offset: 0x000FB762
		public ApplyImportsAction()
		{
		}

		// Token: 0x04001FB2 RID: 8114
		private XmlQualifiedName mode;

		// Token: 0x04001FB3 RID: 8115
		private Stylesheet stylesheet;

		// Token: 0x04001FB4 RID: 8116
		private const int TemplateProcessed = 2;
	}
}
