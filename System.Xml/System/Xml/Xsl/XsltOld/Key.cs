﻿using System;
using System.Collections;
using System.Xml.XPath;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x02000505 RID: 1285
	internal class Key
	{
		// Token: 0x06003243 RID: 12867 RVA: 0x00107565 File Offset: 0x00105765
		public Key(XmlQualifiedName name, int matchkey, int usekey)
		{
			this.name = name;
			this.matchKey = matchkey;
			this.useKey = usekey;
			this.keyNodes = null;
		}

		// Token: 0x17000A8F RID: 2703
		// (get) Token: 0x06003244 RID: 12868 RVA: 0x00107589 File Offset: 0x00105789
		public XmlQualifiedName Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000A90 RID: 2704
		// (get) Token: 0x06003245 RID: 12869 RVA: 0x00107591 File Offset: 0x00105791
		public int MatchKey
		{
			get
			{
				return this.matchKey;
			}
		}

		// Token: 0x17000A91 RID: 2705
		// (get) Token: 0x06003246 RID: 12870 RVA: 0x00107599 File Offset: 0x00105799
		public int UseKey
		{
			get
			{
				return this.useKey;
			}
		}

		// Token: 0x06003247 RID: 12871 RVA: 0x001075A1 File Offset: 0x001057A1
		public void AddKey(XPathNavigator root, Hashtable table)
		{
			if (this.keyNodes == null)
			{
				this.keyNodes = new ArrayList();
			}
			this.keyNodes.Add(new DocumentKeyList(root, table));
		}

		// Token: 0x06003248 RID: 12872 RVA: 0x001075D0 File Offset: 0x001057D0
		public Hashtable GetKeys(XPathNavigator root)
		{
			if (this.keyNodes != null)
			{
				for (int i = 0; i < this.keyNodes.Count; i++)
				{
					if (((DocumentKeyList)this.keyNodes[i]).RootNav.IsSamePosition(root))
					{
						return ((DocumentKeyList)this.keyNodes[i]).KeyTable;
					}
				}
			}
			return null;
		}

		// Token: 0x06003249 RID: 12873 RVA: 0x00107637 File Offset: 0x00105837
		public Key Clone()
		{
			return new Key(this.name, this.matchKey, this.useKey);
		}

		// Token: 0x0400211B RID: 8475
		private XmlQualifiedName name;

		// Token: 0x0400211C RID: 8476
		private int matchKey;

		// Token: 0x0400211D RID: 8477
		private int useKey;

		// Token: 0x0400211E RID: 8478
		private ArrayList keyNodes;
	}
}
