﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004E4 RID: 1252
	internal class DocumentScope
	{
		// Token: 0x17000A26 RID: 2598
		// (get) Token: 0x060030CB RID: 12491 RVA: 0x00101C67 File Offset: 0x000FFE67
		internal NamespaceDecl Scopes
		{
			get
			{
				return this.scopes;
			}
		}

		// Token: 0x060030CC RID: 12492 RVA: 0x00101C6F File Offset: 0x000FFE6F
		internal NamespaceDecl AddNamespace(string prefix, string uri, string prevDefaultNsUri)
		{
			this.scopes = new NamespaceDecl(prefix, uri, prevDefaultNsUri, this.scopes);
			return this.scopes;
		}

		// Token: 0x060030CD RID: 12493 RVA: 0x00101C8C File Offset: 0x000FFE8C
		internal string ResolveAtom(string prefix)
		{
			for (NamespaceDecl next = this.scopes; next != null; next = next.Next)
			{
				if (Ref.Equal(next.Prefix, prefix))
				{
					return next.Uri;
				}
			}
			return null;
		}

		// Token: 0x060030CE RID: 12494 RVA: 0x00101CC4 File Offset: 0x000FFEC4
		internal string ResolveNonAtom(string prefix)
		{
			for (NamespaceDecl next = this.scopes; next != null; next = next.Next)
			{
				if (next.Prefix == prefix)
				{
					return next.Uri;
				}
			}
			return null;
		}

		// Token: 0x060030CF RID: 12495 RVA: 0x00002103 File Offset: 0x00000303
		public DocumentScope()
		{
		}

		// Token: 0x04002047 RID: 8263
		protected NamespaceDecl scopes;
	}
}
