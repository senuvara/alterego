﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004B6 RID: 1206
	internal sealed class AvtEvent : TextEvent
	{
		// Token: 0x06002F6B RID: 12139 RVA: 0x000FDDCB File Offset: 0x000FBFCB
		public AvtEvent(int key)
		{
			this.key = key;
		}

		// Token: 0x06002F6C RID: 12140 RVA: 0x000FDDDA File Offset: 0x000FBFDA
		public override bool Output(Processor processor, ActionFrame frame)
		{
			return processor.TextEvent(processor.EvaluateString(frame, this.key));
		}

		// Token: 0x06002F6D RID: 12141 RVA: 0x000FDDEF File Offset: 0x000FBFEF
		public override string Evaluate(Processor processor, ActionFrame frame)
		{
			return processor.EvaluateString(frame, this.key);
		}

		// Token: 0x04001FC6 RID: 8134
		private int key;
	}
}
