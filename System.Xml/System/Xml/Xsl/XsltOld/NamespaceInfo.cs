﻿using System;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004C1 RID: 1217
	internal class NamespaceInfo
	{
		// Token: 0x06003002 RID: 12290 RVA: 0x000FF7BD File Offset: 0x000FD9BD
		internal NamespaceInfo(string prefix, string nameSpace, int stylesheetId)
		{
			this.prefix = prefix;
			this.nameSpace = nameSpace;
			this.stylesheetId = stylesheetId;
		}

		// Token: 0x04002002 RID: 8194
		internal string prefix;

		// Token: 0x04002003 RID: 8195
		internal string nameSpace;

		// Token: 0x04002004 RID: 8196
		internal int stylesheetId;
	}
}
