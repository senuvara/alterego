﻿using System;
using System.Collections;

namespace System.Xml.Xsl.XsltOld
{
	// Token: 0x020004ED RID: 1261
	internal class InputScope : DocumentScope
	{
		// Token: 0x17000A32 RID: 2610
		// (get) Token: 0x060030F9 RID: 12537 RVA: 0x00102C50 File Offset: 0x00100E50
		internal InputScope Parent
		{
			get
			{
				return this.parent;
			}
		}

		// Token: 0x17000A33 RID: 2611
		// (get) Token: 0x060030FA RID: 12538 RVA: 0x00102C58 File Offset: 0x00100E58
		internal Hashtable Variables
		{
			get
			{
				return this.variables;
			}
		}

		// Token: 0x17000A34 RID: 2612
		// (get) Token: 0x060030FB RID: 12539 RVA: 0x00102C60 File Offset: 0x00100E60
		// (set) Token: 0x060030FC RID: 12540 RVA: 0x00102C68 File Offset: 0x00100E68
		internal bool ForwardCompatibility
		{
			get
			{
				return this.forwardCompatibility;
			}
			set
			{
				this.forwardCompatibility = value;
			}
		}

		// Token: 0x17000A35 RID: 2613
		// (get) Token: 0x060030FD RID: 12541 RVA: 0x00102C71 File Offset: 0x00100E71
		// (set) Token: 0x060030FE RID: 12542 RVA: 0x00102C79 File Offset: 0x00100E79
		internal bool CanHaveApplyImports
		{
			get
			{
				return this.canHaveApplyImports;
			}
			set
			{
				this.canHaveApplyImports = value;
			}
		}

		// Token: 0x060030FF RID: 12543 RVA: 0x00102C82 File Offset: 0x00100E82
		internal InputScope(InputScope parent)
		{
			this.Init(parent);
		}

		// Token: 0x06003100 RID: 12544 RVA: 0x00102C91 File Offset: 0x00100E91
		internal void Init(InputScope parent)
		{
			this.scopes = null;
			this.parent = parent;
			if (this.parent != null)
			{
				this.forwardCompatibility = this.parent.forwardCompatibility;
				this.canHaveApplyImports = this.parent.canHaveApplyImports;
			}
		}

		// Token: 0x06003101 RID: 12545 RVA: 0x00102CCB File Offset: 0x00100ECB
		internal void InsertExtensionNamespace(string nspace)
		{
			if (this.extensionNamespaces == null)
			{
				this.extensionNamespaces = new Hashtable();
			}
			this.extensionNamespaces[nspace] = null;
		}

		// Token: 0x06003102 RID: 12546 RVA: 0x00102CED File Offset: 0x00100EED
		internal bool IsExtensionNamespace(string nspace)
		{
			return this.extensionNamespaces != null && this.extensionNamespaces.Contains(nspace);
		}

		// Token: 0x06003103 RID: 12547 RVA: 0x00102D05 File Offset: 0x00100F05
		internal void InsertExcludedNamespace(string nspace)
		{
			if (this.excludedNamespaces == null)
			{
				this.excludedNamespaces = new Hashtable();
			}
			this.excludedNamespaces[nspace] = null;
		}

		// Token: 0x06003104 RID: 12548 RVA: 0x00102D27 File Offset: 0x00100F27
		internal bool IsExcludedNamespace(string nspace)
		{
			return this.excludedNamespaces != null && this.excludedNamespaces.Contains(nspace);
		}

		// Token: 0x06003105 RID: 12549 RVA: 0x00102D3F File Offset: 0x00100F3F
		internal void InsertVariable(VariableAction variable)
		{
			if (this.variables == null)
			{
				this.variables = new Hashtable();
			}
			this.variables[variable.Name] = variable;
		}

		// Token: 0x06003106 RID: 12550 RVA: 0x00102D66 File Offset: 0x00100F66
		internal int GetVeriablesCount()
		{
			if (this.variables == null)
			{
				return 0;
			}
			return this.variables.Count;
		}

		// Token: 0x06003107 RID: 12551 RVA: 0x00102D80 File Offset: 0x00100F80
		public VariableAction ResolveVariable(XmlQualifiedName qname)
		{
			for (InputScope inputScope = this; inputScope != null; inputScope = inputScope.Parent)
			{
				if (inputScope.Variables != null)
				{
					VariableAction variableAction = (VariableAction)inputScope.Variables[qname];
					if (variableAction != null)
					{
						return variableAction;
					}
				}
			}
			return null;
		}

		// Token: 0x06003108 RID: 12552 RVA: 0x00102DBC File Offset: 0x00100FBC
		public VariableAction ResolveGlobalVariable(XmlQualifiedName qname)
		{
			InputScope inputScope = null;
			for (InputScope inputScope2 = this; inputScope2 != null; inputScope2 = inputScope2.Parent)
			{
				inputScope = inputScope2;
			}
			return inputScope.ResolveVariable(qname);
		}

		// Token: 0x04002069 RID: 8297
		private InputScope parent;

		// Token: 0x0400206A RID: 8298
		private bool forwardCompatibility;

		// Token: 0x0400206B RID: 8299
		private bool canHaveApplyImports;

		// Token: 0x0400206C RID: 8300
		private Hashtable variables;

		// Token: 0x0400206D RID: 8301
		private Hashtable extensionNamespaces;

		// Token: 0x0400206E RID: 8302
		private Hashtable excludedNamespaces;
	}
}
