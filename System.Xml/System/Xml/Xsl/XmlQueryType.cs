﻿using System;
using System.IO;
using System.Text;
using System.Xml.Schema;

namespace System.Xml.Xsl
{
	// Token: 0x02000497 RID: 1175
	internal abstract class XmlQueryType : ListBase<XmlQueryType>
	{
		// Token: 0x06002E08 RID: 11784 RVA: 0x000F9F88 File Offset: 0x000F8188
		static XmlQueryType()
		{
			XmlQueryType.TypeCodeDerivation = new XmlQueryType.BitMatrix(XmlQueryType.BaseTypeCodes.Length);
			for (int i = 0; i < XmlQueryType.BaseTypeCodes.Length; i++)
			{
				int num = i;
				for (;;)
				{
					XmlQueryType.TypeCodeDerivation[i, num] = true;
					if (XmlQueryType.BaseTypeCodes[num] == (XmlTypeCode)num)
					{
						break;
					}
					num = (int)XmlQueryType.BaseTypeCodes[num];
				}
			}
		}

		// Token: 0x170009A4 RID: 2468
		// (get) Token: 0x06002E09 RID: 11785
		public abstract XmlTypeCode TypeCode { get; }

		// Token: 0x170009A5 RID: 2469
		// (get) Token: 0x06002E0A RID: 11786
		public abstract XmlQualifiedNameTest NameTest { get; }

		// Token: 0x170009A6 RID: 2470
		// (get) Token: 0x06002E0B RID: 11787
		public abstract XmlSchemaType SchemaType { get; }

		// Token: 0x170009A7 RID: 2471
		// (get) Token: 0x06002E0C RID: 11788
		public abstract bool IsNillable { get; }

		// Token: 0x170009A8 RID: 2472
		// (get) Token: 0x06002E0D RID: 11789
		public abstract XmlNodeKindFlags NodeKinds { get; }

		// Token: 0x170009A9 RID: 2473
		// (get) Token: 0x06002E0E RID: 11790
		public abstract bool IsStrict { get; }

		// Token: 0x170009AA RID: 2474
		// (get) Token: 0x06002E0F RID: 11791
		public abstract XmlQueryCardinality Cardinality { get; }

		// Token: 0x170009AB RID: 2475
		// (get) Token: 0x06002E10 RID: 11792
		public abstract XmlQueryType Prime { get; }

		// Token: 0x170009AC RID: 2476
		// (get) Token: 0x06002E11 RID: 11793
		public abstract bool IsNotRtf { get; }

		// Token: 0x170009AD RID: 2477
		// (get) Token: 0x06002E12 RID: 11794
		public abstract bool IsDod { get; }

		// Token: 0x170009AE RID: 2478
		// (get) Token: 0x06002E13 RID: 11795
		public abstract XmlValueConverter ClrMapping { get; }

		// Token: 0x06002E14 RID: 11796 RVA: 0x000FA1FC File Offset: 0x000F83FC
		public bool IsSubtypeOf(XmlQueryType baseType)
		{
			if (!(this.Cardinality <= baseType.Cardinality) || (!this.IsDod && baseType.IsDod))
			{
				return false;
			}
			if (!this.IsDod && baseType.IsDod)
			{
				return false;
			}
			XmlQueryType prime = this.Prime;
			XmlQueryType prime2 = baseType.Prime;
			if (prime == prime2)
			{
				return true;
			}
			if (prime.Count == 1 && prime2.Count == 1)
			{
				return prime.IsSubtypeOfItemType(prime2);
			}
			foreach (XmlQueryType xmlQueryType in prime)
			{
				bool flag = false;
				foreach (XmlQueryType baseType2 in prime2)
				{
					if (xmlQueryType.IsSubtypeOfItemType(baseType2))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002E15 RID: 11797 RVA: 0x000FA300 File Offset: 0x000F8500
		public bool NeverSubtypeOf(XmlQueryType baseType)
		{
			if (this.Cardinality.NeverSubset(baseType.Cardinality))
			{
				return true;
			}
			if (this.MaybeEmpty && baseType.MaybeEmpty)
			{
				return false;
			}
			if (this.Count == 0)
			{
				return false;
			}
			foreach (XmlQueryType xmlQueryType in this)
			{
				foreach (XmlQueryType other in baseType)
				{
					if (xmlQueryType.HasIntersectionItemType(other))
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06002E16 RID: 11798 RVA: 0x000FA3C8 File Offset: 0x000F85C8
		public bool Equals(XmlQueryType that)
		{
			if (that == null)
			{
				return false;
			}
			if (this.Cardinality != that.Cardinality || this.IsDod != that.IsDod)
			{
				return false;
			}
			XmlQueryType prime = this.Prime;
			XmlQueryType prime2 = that.Prime;
			if (prime == prime2)
			{
				return true;
			}
			if (prime.Count != prime2.Count)
			{
				return false;
			}
			if (prime.Count == 1)
			{
				return prime.TypeCode == prime2.TypeCode && prime.NameTest == prime2.NameTest && prime.SchemaType == prime2.SchemaType && prime.IsStrict == prime2.IsStrict && prime.IsNotRtf == prime2.IsNotRtf;
			}
			foreach (XmlQueryType xmlQueryType in this)
			{
				bool flag = false;
				foreach (XmlQueryType xmlQueryType2 in that)
				{
					if (xmlQueryType.TypeCode == xmlQueryType2.TypeCode && xmlQueryType.NameTest == xmlQueryType2.NameTest && xmlQueryType.SchemaType == xmlQueryType2.SchemaType && xmlQueryType.IsStrict == xmlQueryType2.IsStrict && xmlQueryType.IsNotRtf == xmlQueryType2.IsNotRtf)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002E17 RID: 11799 RVA: 0x000FA55C File Offset: 0x000F875C
		public static bool operator ==(XmlQueryType left, XmlQueryType right)
		{
			if (left == null)
			{
				return right == null;
			}
			return left.Equals(right);
		}

		// Token: 0x06002E18 RID: 11800 RVA: 0x000FA56D File Offset: 0x000F876D
		public static bool operator !=(XmlQueryType left, XmlQueryType right)
		{
			if (left == null)
			{
				return right != null;
			}
			return !left.Equals(right);
		}

		// Token: 0x170009AF RID: 2479
		// (get) Token: 0x06002E19 RID: 11801 RVA: 0x000FA581 File Offset: 0x000F8781
		public bool IsEmpty
		{
			get
			{
				return this.Cardinality <= XmlQueryCardinality.Zero;
			}
		}

		// Token: 0x170009B0 RID: 2480
		// (get) Token: 0x06002E1A RID: 11802 RVA: 0x000FA593 File Offset: 0x000F8793
		public bool IsSingleton
		{
			get
			{
				return this.Cardinality <= XmlQueryCardinality.One;
			}
		}

		// Token: 0x170009B1 RID: 2481
		// (get) Token: 0x06002E1B RID: 11803 RVA: 0x000FA5A5 File Offset: 0x000F87A5
		public bool MaybeEmpty
		{
			get
			{
				return XmlQueryCardinality.Zero <= this.Cardinality;
			}
		}

		// Token: 0x170009B2 RID: 2482
		// (get) Token: 0x06002E1C RID: 11804 RVA: 0x000FA5B7 File Offset: 0x000F87B7
		public bool MaybeMany
		{
			get
			{
				return XmlQueryCardinality.More <= this.Cardinality;
			}
		}

		// Token: 0x170009B3 RID: 2483
		// (get) Token: 0x06002E1D RID: 11805 RVA: 0x000FA5C9 File Offset: 0x000F87C9
		public bool IsNode
		{
			get
			{
				return (XmlQueryType.TypeCodeToFlags[(int)this.TypeCode] & XmlQueryType.TypeFlags.IsNode) > XmlQueryType.TypeFlags.None;
			}
		}

		// Token: 0x170009B4 RID: 2484
		// (get) Token: 0x06002E1E RID: 11806 RVA: 0x000FA5DC File Offset: 0x000F87DC
		public bool IsAtomicValue
		{
			get
			{
				return (XmlQueryType.TypeCodeToFlags[(int)this.TypeCode] & XmlQueryType.TypeFlags.IsAtomicValue) > XmlQueryType.TypeFlags.None;
			}
		}

		// Token: 0x170009B5 RID: 2485
		// (get) Token: 0x06002E1F RID: 11807 RVA: 0x000FA5EF File Offset: 0x000F87EF
		public bool IsNumeric
		{
			get
			{
				return (XmlQueryType.TypeCodeToFlags[(int)this.TypeCode] & XmlQueryType.TypeFlags.IsNumeric) > XmlQueryType.TypeFlags.None;
			}
		}

		// Token: 0x06002E20 RID: 11808 RVA: 0x000FA604 File Offset: 0x000F8804
		public override bool Equals(object obj)
		{
			XmlQueryType xmlQueryType = obj as XmlQueryType;
			return !(xmlQueryType == null) && this.Equals(xmlQueryType);
		}

		// Token: 0x06002E21 RID: 11809 RVA: 0x000FA62C File Offset: 0x000F882C
		public override int GetHashCode()
		{
			if (this.hashCode == 0)
			{
				int num = (int)this.TypeCode;
				XmlSchemaType schemaType = this.SchemaType;
				if (schemaType != null)
				{
					num += (num << 7 ^ schemaType.GetHashCode());
				}
				num += (num << 7 ^ (int)this.NodeKinds);
				num += (num << 7 ^ this.Cardinality.GetHashCode());
				num += (num << 7 ^ (this.IsStrict ? 1 : 0));
				num -= num >> 17;
				num -= num >> 11;
				num -= num >> 5;
				this.hashCode = ((num == 0) ? 1 : num);
			}
			return this.hashCode;
		}

		// Token: 0x06002E22 RID: 11810 RVA: 0x000FA6C1 File Offset: 0x000F88C1
		public override string ToString()
		{
			return this.ToString("G");
		}

		// Token: 0x06002E23 RID: 11811 RVA: 0x000FA6D0 File Offset: 0x000F88D0
		public string ToString(string format)
		{
			StringBuilder stringBuilder;
			if (format == "S")
			{
				stringBuilder = new StringBuilder();
				stringBuilder.Append(this.Cardinality.ToString(format));
				stringBuilder.Append(';');
				for (int i = 0; i < this.Count; i++)
				{
					if (i != 0)
					{
						stringBuilder.Append("|");
					}
					stringBuilder.Append(this[i].TypeCode.ToString());
				}
				stringBuilder.Append(';');
				stringBuilder.Append(this.IsStrict);
				return stringBuilder.ToString();
			}
			bool flag = format == "X";
			if (this.Cardinality == XmlQueryCardinality.None)
			{
				return "none";
			}
			if (this.Cardinality == XmlQueryCardinality.Zero)
			{
				return "empty";
			}
			stringBuilder = new StringBuilder();
			int count = this.Count;
			if (count != 0)
			{
				if (count != 1)
				{
					string[] array = new string[this.Count];
					for (int j = 0; j < this.Count; j++)
					{
						array[j] = this[j].ItemTypeToString(flag);
					}
					Array.Sort<string>(array);
					stringBuilder = new StringBuilder();
					stringBuilder.Append('(');
					stringBuilder.Append(array[0]);
					for (int k = 1; k < array.Length; k++)
					{
						stringBuilder.Append(" | ");
						stringBuilder.Append(array[k]);
					}
					stringBuilder.Append(')');
				}
				else
				{
					stringBuilder.Append(this[0].ItemTypeToString(flag));
				}
			}
			else
			{
				stringBuilder.Append("none");
			}
			stringBuilder.Append(this.Cardinality.ToString());
			if (!flag && this.IsDod)
			{
				stringBuilder.Append('#');
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06002E24 RID: 11812
		public abstract void GetObjectData(BinaryWriter writer);

		// Token: 0x06002E25 RID: 11813 RVA: 0x000FA8B0 File Offset: 0x000F8AB0
		private bool IsSubtypeOfItemType(XmlQueryType baseType)
		{
			XmlSchemaType schemaType = baseType.SchemaType;
			if (this.TypeCode != baseType.TypeCode)
			{
				if (baseType.IsStrict)
				{
					return false;
				}
				XmlSchemaType builtInSimpleType = XmlSchemaType.GetBuiltInSimpleType(baseType.TypeCode);
				return (builtInSimpleType == null || schemaType == builtInSimpleType) && XmlQueryType.TypeCodeDerivation[this.TypeCode, baseType.TypeCode];
			}
			else
			{
				if (baseType.IsStrict)
				{
					return this.IsStrict && this.SchemaType == schemaType;
				}
				return (this.IsNotRtf || !baseType.IsNotRtf) && this.NameTest.IsSubsetOf(baseType.NameTest) && (schemaType == XmlSchemaComplexType.AnyType || XmlSchemaType.IsDerivedFrom(this.SchemaType, schemaType, XmlSchemaDerivationMethod.Empty)) && (!this.IsNillable || baseType.IsNillable);
			}
		}

		// Token: 0x06002E26 RID: 11814 RVA: 0x000FA970 File Offset: 0x000F8B70
		private bool HasIntersectionItemType(XmlQueryType other)
		{
			if (this.TypeCode == other.TypeCode && (this.NodeKinds & (XmlNodeKindFlags.Document | XmlNodeKindFlags.Element | XmlNodeKindFlags.Attribute)) != XmlNodeKindFlags.None)
			{
				return this.TypeCode == XmlTypeCode.Node || (this.NameTest.HasIntersection(other.NameTest) && (XmlSchemaType.IsDerivedFrom(this.SchemaType, other.SchemaType, XmlSchemaDerivationMethod.Empty) || XmlSchemaType.IsDerivedFrom(other.SchemaType, this.SchemaType, XmlSchemaDerivationMethod.Empty)));
			}
			return this.IsSubtypeOf(other) || other.IsSubtypeOf(this);
		}

		// Token: 0x06002E27 RID: 11815 RVA: 0x000FA9F8 File Offset: 0x000F8BF8
		private string ItemTypeToString(bool isXQ)
		{
			string text;
			if (this.IsNode)
			{
				text = XmlQueryType.TypeNames[(int)this.TypeCode];
				XmlTypeCode typeCode = this.TypeCode;
				if (typeCode != XmlTypeCode.Document)
				{
					if (typeCode - XmlTypeCode.Element > 1)
					{
						goto IL_B0;
					}
				}
				else if (isXQ)
				{
					text = text + "{(element" + this.NameAndType(true) + "?&text?&comment?&processing-instruction?)*}";
					goto IL_B0;
				}
				text += this.NameAndType(isXQ);
			}
			else if (this.SchemaType != XmlSchemaComplexType.AnyType)
			{
				if (this.SchemaType.QualifiedName.IsEmpty)
				{
					text = "<:" + XmlQueryType.TypeNames[(int)this.TypeCode];
				}
				else
				{
					text = XmlQueryType.QNameToString(this.SchemaType.QualifiedName);
				}
			}
			else
			{
				text = XmlQueryType.TypeNames[(int)this.TypeCode];
			}
			IL_B0:
			if (!isXQ && this.IsStrict)
			{
				text += "=";
			}
			return text;
		}

		// Token: 0x06002E28 RID: 11816 RVA: 0x000FAAD0 File Offset: 0x000F8CD0
		private string NameAndType(bool isXQ)
		{
			string text = this.NameTest.ToString();
			string text2 = "*";
			if (this.SchemaType.QualifiedName.IsEmpty)
			{
				text2 = "typeof(" + text + ")";
			}
			else if (isXQ || (this.SchemaType != XmlSchemaComplexType.AnyType && this.SchemaType != DatatypeImplementation.AnySimpleType))
			{
				text2 = XmlQueryType.QNameToString(this.SchemaType.QualifiedName);
			}
			if (this.IsNillable)
			{
				text2 += " nillable";
			}
			if (text == "*" && text2 == "*")
			{
				return "";
			}
			return string.Concat(new string[]
			{
				"(",
				text,
				", ",
				text2,
				")"
			});
		}

		// Token: 0x06002E29 RID: 11817 RVA: 0x000FABA4 File Offset: 0x000F8DA4
		private static string QNameToString(XmlQualifiedName name)
		{
			if (name.IsEmpty)
			{
				return "*";
			}
			if (name.Namespace.Length == 0)
			{
				return name.Name;
			}
			if (name.Namespace == "http://www.w3.org/2001/XMLSchema")
			{
				return "xs:" + name.Name;
			}
			if (name.Namespace == "http://www.w3.org/2003/11/xpath-datatypes")
			{
				return "xdt:" + name.Name;
			}
			return "{" + name.Namespace + "}" + name.Name;
		}

		// Token: 0x06002E2A RID: 11818 RVA: 0x000FAC34 File Offset: 0x000F8E34
		protected XmlQueryType()
		{
		}

		// Token: 0x04001F2A RID: 7978
		private static readonly XmlQueryType.BitMatrix TypeCodeDerivation;

		// Token: 0x04001F2B RID: 7979
		private int hashCode;

		// Token: 0x04001F2C RID: 7980
		private static readonly XmlQueryType.TypeFlags[] TypeCodeToFlags = new XmlQueryType.TypeFlags[]
		{
			(XmlQueryType.TypeFlags)7,
			XmlQueryType.TypeFlags.None,
			XmlQueryType.TypeFlags.IsNode,
			XmlQueryType.TypeFlags.IsNode,
			XmlQueryType.TypeFlags.IsNode,
			XmlQueryType.TypeFlags.IsNode,
			XmlQueryType.TypeFlags.IsNode,
			XmlQueryType.TypeFlags.IsNode,
			XmlQueryType.TypeFlags.IsNode,
			XmlQueryType.TypeFlags.IsNode,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			(XmlQueryType.TypeFlags)6,
			XmlQueryType.TypeFlags.IsAtomicValue,
			XmlQueryType.TypeFlags.IsAtomicValue
		};

		// Token: 0x04001F2D RID: 7981
		private static readonly XmlTypeCode[] BaseTypeCodes = new XmlTypeCode[]
		{
			XmlTypeCode.None,
			XmlTypeCode.Item,
			XmlTypeCode.Item,
			XmlTypeCode.Node,
			XmlTypeCode.Node,
			XmlTypeCode.Node,
			XmlTypeCode.Node,
			XmlTypeCode.Node,
			XmlTypeCode.Node,
			XmlTypeCode.Node,
			XmlTypeCode.Item,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.AnyAtomicType,
			XmlTypeCode.String,
			XmlTypeCode.NormalizedString,
			XmlTypeCode.Token,
			XmlTypeCode.Token,
			XmlTypeCode.Token,
			XmlTypeCode.Name,
			XmlTypeCode.NCName,
			XmlTypeCode.NCName,
			XmlTypeCode.NCName,
			XmlTypeCode.Decimal,
			XmlTypeCode.Integer,
			XmlTypeCode.NonPositiveInteger,
			XmlTypeCode.Integer,
			XmlTypeCode.Long,
			XmlTypeCode.Int,
			XmlTypeCode.Short,
			XmlTypeCode.Integer,
			XmlTypeCode.NonNegativeInteger,
			XmlTypeCode.UnsignedLong,
			XmlTypeCode.UnsignedInt,
			XmlTypeCode.UnsignedShort,
			XmlTypeCode.NonNegativeInteger,
			XmlTypeCode.Duration,
			XmlTypeCode.Duration
		};

		// Token: 0x04001F2E RID: 7982
		private static readonly string[] TypeNames = new string[]
		{
			"none",
			"item",
			"node",
			"document",
			"element",
			"attribute",
			"namespace",
			"processing-instruction",
			"comment",
			"text",
			"xdt:anyAtomicType",
			"xdt:untypedAtomic",
			"xs:string",
			"xs:boolean",
			"xs:decimal",
			"xs:float",
			"xs:double",
			"xs:duration",
			"xs:dateTime",
			"xs:time",
			"xs:date",
			"xs:gYearMonth",
			"xs:gYear",
			"xs:gMonthDay",
			"xs:gDay",
			"xs:gMonth",
			"xs:hexBinary",
			"xs:base64Binary",
			"xs:anyUri",
			"xs:QName",
			"xs:NOTATION",
			"xs:normalizedString",
			"xs:token",
			"xs:language",
			"xs:NMTOKEN",
			"xs:Name",
			"xs:NCName",
			"xs:ID",
			"xs:IDREF",
			"xs:ENTITY",
			"xs:integer",
			"xs:nonPositiveInteger",
			"xs:negativeInteger",
			"xs:long",
			"xs:int",
			"xs:short",
			"xs:byte",
			"xs:nonNegativeInteger",
			"xs:unsignedLong",
			"xs:unsignedInt",
			"xs:unsignedShort",
			"xs:unsignedByte",
			"xs:positiveInteger",
			"xdt:yearMonthDuration",
			"xdt:dayTimeDuration"
		};

		// Token: 0x02000498 RID: 1176
		private enum TypeFlags
		{
			// Token: 0x04001F30 RID: 7984
			None,
			// Token: 0x04001F31 RID: 7985
			IsNode,
			// Token: 0x04001F32 RID: 7986
			IsAtomicValue,
			// Token: 0x04001F33 RID: 7987
			IsNumeric = 4
		}

		// Token: 0x02000499 RID: 1177
		private sealed class BitMatrix
		{
			// Token: 0x06002E2B RID: 11819 RVA: 0x000FAC3C File Offset: 0x000F8E3C
			public BitMatrix(int count)
			{
				this.bits = new ulong[count];
			}

			// Token: 0x170009B6 RID: 2486
			public bool this[int index1, int index2]
			{
				get
				{
					return (this.bits[index1] & 1UL << index2) > 0UL;
				}
				set
				{
					if (value)
					{
						this.bits[index1] |= 1UL << index2;
						return;
					}
					this.bits[index1] &= ~(1UL << index2);
				}
			}

			// Token: 0x170009B7 RID: 2487
			public bool this[XmlTypeCode index1, XmlTypeCode index2]
			{
				get
				{
					return this[(int)index1, (int)index2];
				}
			}

			// Token: 0x04001F34 RID: 7988
			private ulong[] bits;
		}
	}
}
