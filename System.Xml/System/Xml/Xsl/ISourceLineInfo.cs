﻿using System;

namespace System.Xml.Xsl
{
	// Token: 0x02000488 RID: 1160
	internal interface ISourceLineInfo
	{
		// Token: 0x17000973 RID: 2419
		// (get) Token: 0x06002D5D RID: 11613
		string Uri { get; }

		// Token: 0x17000974 RID: 2420
		// (get) Token: 0x06002D5E RID: 11614
		bool IsNoSource { get; }

		// Token: 0x17000975 RID: 2421
		// (get) Token: 0x06002D5F RID: 11615
		Location Start { get; }

		// Token: 0x17000976 RID: 2422
		// (get) Token: 0x06002D60 RID: 11616
		Location End { get; }
	}
}
