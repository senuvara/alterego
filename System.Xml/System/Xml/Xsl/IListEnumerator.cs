﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Xml.Xsl
{
	// Token: 0x0200048A RID: 1162
	internal struct IListEnumerator<T> : IEnumerator<T>, IDisposable, IEnumerator
	{
		// Token: 0x06002D7E RID: 11646 RVA: 0x000F5E40 File Offset: 0x000F4040
		public IListEnumerator(IList<T> sequence)
		{
			this.sequence = sequence;
			this.index = 0;
			this.current = default(T);
		}

		// Token: 0x06002D7F RID: 11647 RVA: 0x000030EC File Offset: 0x000012EC
		public void Dispose()
		{
		}

		// Token: 0x1700097E RID: 2430
		// (get) Token: 0x06002D80 RID: 11648 RVA: 0x000F5E5C File Offset: 0x000F405C
		public T Current
		{
			get
			{
				return this.current;
			}
		}

		// Token: 0x1700097F RID: 2431
		// (get) Token: 0x06002D81 RID: 11649 RVA: 0x000F5E64 File Offset: 0x000F4064
		object IEnumerator.Current
		{
			get
			{
				if (this.index == 0)
				{
					throw new InvalidOperationException(Res.GetString("Enumeration has not started. Call MoveNext.", new object[]
					{
						string.Empty
					}));
				}
				if (this.index > this.sequence.Count)
				{
					throw new InvalidOperationException(Res.GetString("Enumeration has already finished.", new object[]
					{
						string.Empty
					}));
				}
				return this.current;
			}
		}

		// Token: 0x06002D82 RID: 11650 RVA: 0x000F5ED4 File Offset: 0x000F40D4
		public bool MoveNext()
		{
			if (this.index < this.sequence.Count)
			{
				this.current = this.sequence[this.index];
				this.index++;
				return true;
			}
			this.current = default(T);
			return false;
		}

		// Token: 0x06002D83 RID: 11651 RVA: 0x000F5F28 File Offset: 0x000F4128
		void IEnumerator.Reset()
		{
			this.index = 0;
			this.current = default(T);
		}

		// Token: 0x04001EEE RID: 7918
		private IList<T> sequence;

		// Token: 0x04001EEF RID: 7919
		private int index;

		// Token: 0x04001EF0 RID: 7920
		private T current;
	}
}
