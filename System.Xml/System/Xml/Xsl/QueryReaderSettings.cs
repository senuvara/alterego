﻿using System;
using System.IO;

namespace System.Xml.Xsl
{
	// Token: 0x0200048D RID: 1165
	internal class QueryReaderSettings
	{
		// Token: 0x06002D8C RID: 11660 RVA: 0x000F5FD8 File Offset: 0x000F41D8
		public QueryReaderSettings(XmlNameTable xmlNameTable)
		{
			this.xmlReaderSettings = new XmlReaderSettings();
			this.xmlReaderSettings.NameTable = xmlNameTable;
			this.xmlReaderSettings.ConformanceLevel = ConformanceLevel.Document;
			this.xmlReaderSettings.XmlResolver = null;
			this.xmlReaderSettings.DtdProcessing = DtdProcessing.Prohibit;
			this.xmlReaderSettings.CloseInput = true;
		}

		// Token: 0x06002D8D RID: 11661 RVA: 0x000F6034 File Offset: 0x000F4234
		public QueryReaderSettings(XmlReader reader)
		{
			XmlValidatingReader xmlValidatingReader = reader as XmlValidatingReader;
			if (xmlValidatingReader != null)
			{
				this.validatingReader = true;
				reader = xmlValidatingReader.Impl.Reader;
			}
			this.xmlReaderSettings = reader.Settings;
			if (this.xmlReaderSettings != null)
			{
				this.xmlReaderSettings = this.xmlReaderSettings.Clone();
				this.xmlReaderSettings.NameTable = reader.NameTable;
				this.xmlReaderSettings.CloseInput = true;
				this.xmlReaderSettings.LineNumberOffset = 0;
				this.xmlReaderSettings.LinePositionOffset = 0;
				XmlTextReaderImpl xmlTextReaderImpl = reader as XmlTextReaderImpl;
				if (xmlTextReaderImpl != null)
				{
					this.xmlReaderSettings.XmlResolver = xmlTextReaderImpl.GetResolver();
					return;
				}
			}
			else
			{
				this.xmlNameTable = reader.NameTable;
				XmlTextReader xmlTextReader = reader as XmlTextReader;
				if (xmlTextReader != null)
				{
					XmlTextReaderImpl impl = xmlTextReader.Impl;
					this.entityHandling = impl.EntityHandling;
					this.namespaces = impl.Namespaces;
					this.normalization = impl.Normalization;
					this.prohibitDtd = (impl.DtdProcessing == DtdProcessing.Prohibit);
					this.whitespaceHandling = impl.WhitespaceHandling;
					this.xmlResolver = impl.GetResolver();
					return;
				}
				this.entityHandling = EntityHandling.ExpandEntities;
				this.namespaces = true;
				this.normalization = true;
				this.prohibitDtd = true;
				this.whitespaceHandling = WhitespaceHandling.All;
				this.xmlResolver = null;
			}
		}

		// Token: 0x06002D8E RID: 11662 RVA: 0x000F6174 File Offset: 0x000F4374
		public XmlReader CreateReader(Stream stream, string baseUri)
		{
			XmlReader xmlReader;
			if (this.xmlReaderSettings != null)
			{
				xmlReader = XmlReader.Create(stream, this.xmlReaderSettings, baseUri);
			}
			else
			{
				xmlReader = new XmlTextReaderImpl(baseUri, stream, this.xmlNameTable)
				{
					EntityHandling = this.entityHandling,
					Namespaces = this.namespaces,
					Normalization = this.normalization,
					DtdProcessing = (this.prohibitDtd ? DtdProcessing.Prohibit : DtdProcessing.Parse),
					WhitespaceHandling = this.whitespaceHandling,
					XmlResolver = this.xmlResolver
				};
			}
			if (this.validatingReader)
			{
				xmlReader = new XmlValidatingReader(xmlReader);
			}
			return xmlReader;
		}

		// Token: 0x17000984 RID: 2436
		// (get) Token: 0x06002D8F RID: 11663 RVA: 0x000F6205 File Offset: 0x000F4405
		public XmlNameTable NameTable
		{
			get
			{
				if (this.xmlReaderSettings == null)
				{
					return this.xmlNameTable;
				}
				return this.xmlReaderSettings.NameTable;
			}
		}

		// Token: 0x04001EF5 RID: 7925
		private bool validatingReader;

		// Token: 0x04001EF6 RID: 7926
		private XmlReaderSettings xmlReaderSettings;

		// Token: 0x04001EF7 RID: 7927
		private XmlNameTable xmlNameTable;

		// Token: 0x04001EF8 RID: 7928
		private EntityHandling entityHandling;

		// Token: 0x04001EF9 RID: 7929
		private bool namespaces;

		// Token: 0x04001EFA RID: 7930
		private bool normalization;

		// Token: 0x04001EFB RID: 7931
		private bool prohibitDtd;

		// Token: 0x04001EFC RID: 7932
		private WhitespaceHandling whitespaceHandling;

		// Token: 0x04001EFD RID: 7933
		private XmlResolver xmlResolver;
	}
}
