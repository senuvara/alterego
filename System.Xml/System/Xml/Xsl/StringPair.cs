﻿using System;

namespace System.Xml.Xsl
{
	// Token: 0x0200048C RID: 1164
	internal struct StringPair
	{
		// Token: 0x06002D89 RID: 11657 RVA: 0x000F5FB5 File Offset: 0x000F41B5
		public StringPair(string left, string right)
		{
			this.left = left;
			this.right = right;
		}

		// Token: 0x17000982 RID: 2434
		// (get) Token: 0x06002D8A RID: 11658 RVA: 0x000F5FC5 File Offset: 0x000F41C5
		public string Left
		{
			get
			{
				return this.left;
			}
		}

		// Token: 0x17000983 RID: 2435
		// (get) Token: 0x06002D8B RID: 11659 RVA: 0x000F5FCD File Offset: 0x000F41CD
		public string Right
		{
			get
			{
				return this.right;
			}
		}

		// Token: 0x04001EF3 RID: 7923
		private string left;

		// Token: 0x04001EF4 RID: 7924
		private string right;
	}
}
