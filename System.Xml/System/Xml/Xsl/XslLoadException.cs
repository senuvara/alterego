﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Xml.Xsl
{
	// Token: 0x0200049F RID: 1183
	[Serializable]
	internal class XslLoadException : XslTransformException
	{
		// Token: 0x06002E82 RID: 11906 RVA: 0x000FBEB8 File Offset: 0x000FA0B8
		protected XslLoadException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if ((bool)info.GetValue("hasLineInfo", typeof(bool)))
			{
				string uriString = (string)info.GetValue("Uri", typeof(string));
				int startLine = (int)info.GetValue("StartLine", typeof(int));
				int startPos = (int)info.GetValue("StartPos", typeof(int));
				int endLine = (int)info.GetValue("EndLine", typeof(int));
				int endPos = (int)info.GetValue("EndPos", typeof(int));
				this.lineInfo = new SourceLineInfo(uriString, startLine, startPos, endLine, endPos);
			}
		}

		// Token: 0x06002E83 RID: 11907 RVA: 0x000FBF88 File Offset: 0x000FA188
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("hasLineInfo", this.lineInfo != null);
			if (this.lineInfo != null)
			{
				info.AddValue("Uri", this.lineInfo.Uri);
				info.AddValue("StartLine", this.lineInfo.Start.Line);
				info.AddValue("StartPos", this.lineInfo.Start.Pos);
				info.AddValue("EndLine", this.lineInfo.End.Line);
				info.AddValue("EndPos", this.lineInfo.End.Pos);
			}
		}

		// Token: 0x06002E84 RID: 11908 RVA: 0x000FBD79 File Offset: 0x000F9F79
		internal XslLoadException(string res, params string[] args) : base(null, res, args)
		{
		}

		// Token: 0x06002E85 RID: 11909 RVA: 0x000FC04A File Offset: 0x000FA24A
		internal XslLoadException(Exception inner, ISourceLineInfo lineInfo) : base(inner, "XSLT compile error.", null)
		{
			this.SetSourceLineInfo(lineInfo);
		}

		// Token: 0x06002E86 RID: 11910 RVA: 0x000FC060 File Offset: 0x000FA260
		internal void SetSourceLineInfo(ISourceLineInfo lineInfo)
		{
			this.lineInfo = lineInfo;
		}

		// Token: 0x170009DF RID: 2527
		// (get) Token: 0x06002E87 RID: 11911 RVA: 0x000FC069 File Offset: 0x000FA269
		public override string SourceUri
		{
			get
			{
				if (this.lineInfo == null)
				{
					return null;
				}
				return this.lineInfo.Uri;
			}
		}

		// Token: 0x170009E0 RID: 2528
		// (get) Token: 0x06002E88 RID: 11912 RVA: 0x000FC080 File Offset: 0x000FA280
		public override int LineNumber
		{
			get
			{
				if (this.lineInfo == null)
				{
					return 0;
				}
				return this.lineInfo.Start.Line;
			}
		}

		// Token: 0x170009E1 RID: 2529
		// (get) Token: 0x06002E89 RID: 11913 RVA: 0x000FC0AC File Offset: 0x000FA2AC
		public override int LinePosition
		{
			get
			{
				if (this.lineInfo == null)
				{
					return 0;
				}
				return this.lineInfo.Start.Pos;
			}
		}

		// Token: 0x06002E8A RID: 11914 RVA: 0x000FC0D8 File Offset: 0x000FA2D8
		private static string AppendLineInfoMessage(string message, ISourceLineInfo lineInfo)
		{
			if (lineInfo != null)
			{
				string fileName = SourceLineInfo.GetFileName(lineInfo.Uri);
				string text = XslTransformException.CreateMessage("An error occurred at {0}({1},{2}).", new string[]
				{
					fileName,
					lineInfo.Start.Line.ToString(CultureInfo.InvariantCulture),
					lineInfo.Start.Pos.ToString(CultureInfo.InvariantCulture)
				});
				if (text != null && text.Length > 0)
				{
					if (message.Length > 0 && !XmlCharType.Instance.IsWhiteSpace(message[message.Length - 1]))
					{
						message += " ";
					}
					message += text;
				}
			}
			return message;
		}

		// Token: 0x06002E8B RID: 11915 RVA: 0x000FC192 File Offset: 0x000FA392
		internal static string CreateMessage(ISourceLineInfo lineInfo, string res, params string[] args)
		{
			return XslLoadException.AppendLineInfoMessage(XslTransformException.CreateMessage(res, args), lineInfo);
		}

		// Token: 0x06002E8C RID: 11916 RVA: 0x000FC1A1 File Offset: 0x000FA3A1
		internal override string FormatDetailedMessage()
		{
			return XslLoadException.AppendLineInfoMessage(this.Message, this.lineInfo);
		}

		// Token: 0x04001F87 RID: 8071
		private ISourceLineInfo lineInfo;
	}
}
