﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl
{
	// Token: 0x020004AD RID: 1197
	internal class NoOperationDebugger
	{
		// Token: 0x06002F1C RID: 12060 RVA: 0x000030EC File Offset: 0x000012EC
		protected void OnCompile(XPathNavigator input)
		{
		}

		// Token: 0x06002F1D RID: 12061 RVA: 0x000030EC File Offset: 0x000012EC
		protected void OnExecute(XPathNodeIterator currentNodeset, XPathNavigator style, XsltContext context)
		{
		}

		// Token: 0x06002F1E RID: 12062 RVA: 0x00002103 File Offset: 0x00000303
		public NoOperationDebugger()
		{
		}
	}
}
