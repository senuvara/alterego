﻿using System;
using System.IO;

namespace System.Xml.Xsl
{
	// Token: 0x02000496 RID: 1174
	internal struct XmlQueryCardinality
	{
		// Token: 0x06002DEC RID: 11756 RVA: 0x000F9672 File Offset: 0x000F7872
		private XmlQueryCardinality(int value)
		{
			this.value = value;
		}

		// Token: 0x1700099C RID: 2460
		// (get) Token: 0x06002DED RID: 11757 RVA: 0x000F967B File Offset: 0x000F787B
		public static XmlQueryCardinality None
		{
			get
			{
				return new XmlQueryCardinality(0);
			}
		}

		// Token: 0x1700099D RID: 2461
		// (get) Token: 0x06002DEE RID: 11758 RVA: 0x000F9683 File Offset: 0x000F7883
		public static XmlQueryCardinality Zero
		{
			get
			{
				return new XmlQueryCardinality(1);
			}
		}

		// Token: 0x1700099E RID: 2462
		// (get) Token: 0x06002DEF RID: 11759 RVA: 0x000F968B File Offset: 0x000F788B
		public static XmlQueryCardinality One
		{
			get
			{
				return new XmlQueryCardinality(2);
			}
		}

		// Token: 0x1700099F RID: 2463
		// (get) Token: 0x06002DF0 RID: 11760 RVA: 0x000F9693 File Offset: 0x000F7893
		public static XmlQueryCardinality ZeroOrOne
		{
			get
			{
				return new XmlQueryCardinality(3);
			}
		}

		// Token: 0x170009A0 RID: 2464
		// (get) Token: 0x06002DF1 RID: 11761 RVA: 0x000F969B File Offset: 0x000F789B
		public static XmlQueryCardinality More
		{
			get
			{
				return new XmlQueryCardinality(4);
			}
		}

		// Token: 0x170009A1 RID: 2465
		// (get) Token: 0x06002DF2 RID: 11762 RVA: 0x000F96A3 File Offset: 0x000F78A3
		public static XmlQueryCardinality NotOne
		{
			get
			{
				return new XmlQueryCardinality(5);
			}
		}

		// Token: 0x170009A2 RID: 2466
		// (get) Token: 0x06002DF3 RID: 11763 RVA: 0x000F96AB File Offset: 0x000F78AB
		public static XmlQueryCardinality OneOrMore
		{
			get
			{
				return new XmlQueryCardinality(6);
			}
		}

		// Token: 0x170009A3 RID: 2467
		// (get) Token: 0x06002DF4 RID: 11764 RVA: 0x000F96B3 File Offset: 0x000F78B3
		public static XmlQueryCardinality ZeroOrMore
		{
			get
			{
				return new XmlQueryCardinality(7);
			}
		}

		// Token: 0x06002DF5 RID: 11765 RVA: 0x000F96BB File Offset: 0x000F78BB
		public bool Equals(XmlQueryCardinality other)
		{
			return this.value == other.value;
		}

		// Token: 0x06002DF6 RID: 11766 RVA: 0x000F96BB File Offset: 0x000F78BB
		public static bool operator ==(XmlQueryCardinality left, XmlQueryCardinality right)
		{
			return left.value == right.value;
		}

		// Token: 0x06002DF7 RID: 11767 RVA: 0x000F96CB File Offset: 0x000F78CB
		public static bool operator !=(XmlQueryCardinality left, XmlQueryCardinality right)
		{
			return left.value != right.value;
		}

		// Token: 0x06002DF8 RID: 11768 RVA: 0x000F96DE File Offset: 0x000F78DE
		public override bool Equals(object other)
		{
			return other is XmlQueryCardinality && this.Equals((XmlQueryCardinality)other);
		}

		// Token: 0x06002DF9 RID: 11769 RVA: 0x000F96F6 File Offset: 0x000F78F6
		public override int GetHashCode()
		{
			return this.value;
		}

		// Token: 0x06002DFA RID: 11770 RVA: 0x000F96FE File Offset: 0x000F78FE
		public static XmlQueryCardinality operator |(XmlQueryCardinality left, XmlQueryCardinality right)
		{
			return new XmlQueryCardinality(left.value | right.value);
		}

		// Token: 0x06002DFB RID: 11771 RVA: 0x000F9712 File Offset: 0x000F7912
		public static XmlQueryCardinality operator &(XmlQueryCardinality left, XmlQueryCardinality right)
		{
			return new XmlQueryCardinality(left.value & right.value);
		}

		// Token: 0x06002DFC RID: 11772 RVA: 0x000F9726 File Offset: 0x000F7926
		public static XmlQueryCardinality operator *(XmlQueryCardinality left, XmlQueryCardinality right)
		{
			return XmlQueryCardinality.cardinalityProduct[left.value, right.value];
		}

		// Token: 0x06002DFD RID: 11773 RVA: 0x000F973E File Offset: 0x000F793E
		public static XmlQueryCardinality operator +(XmlQueryCardinality left, XmlQueryCardinality right)
		{
			return XmlQueryCardinality.cardinalitySum[left.value, right.value];
		}

		// Token: 0x06002DFE RID: 11774 RVA: 0x000F9756 File Offset: 0x000F7956
		public static bool operator <=(XmlQueryCardinality left, XmlQueryCardinality right)
		{
			return (left.value & ~right.value) == 0;
		}

		// Token: 0x06002DFF RID: 11775 RVA: 0x000F9769 File Offset: 0x000F7969
		public static bool operator >=(XmlQueryCardinality left, XmlQueryCardinality right)
		{
			return (right.value & ~left.value) == 0;
		}

		// Token: 0x06002E00 RID: 11776 RVA: 0x000F977C File Offset: 0x000F797C
		public XmlQueryCardinality AtMost()
		{
			return new XmlQueryCardinality(this.value | this.value >> 1 | this.value >> 2);
		}

		// Token: 0x06002E01 RID: 11777 RVA: 0x000F979B File Offset: 0x000F799B
		public bool NeverSubset(XmlQueryCardinality other)
		{
			return this.value != 0 && (this.value & other.value) == 0;
		}

		// Token: 0x06002E02 RID: 11778 RVA: 0x000F97B7 File Offset: 0x000F79B7
		public string ToString(string format)
		{
			if (format == "S")
			{
				return XmlQueryCardinality.serialized[this.value];
			}
			return this.ToString();
		}

		// Token: 0x06002E03 RID: 11779 RVA: 0x000F97DF File Offset: 0x000F79DF
		public override string ToString()
		{
			return XmlQueryCardinality.toString[this.value];
		}

		// Token: 0x06002E04 RID: 11780 RVA: 0x000F97F0 File Offset: 0x000F79F0
		public XmlQueryCardinality(string s)
		{
			this.value = 0;
			for (int i = 0; i < XmlQueryCardinality.serialized.Length; i++)
			{
				if (s == XmlQueryCardinality.serialized[i])
				{
					this.value = i;
					return;
				}
			}
		}

		// Token: 0x06002E05 RID: 11781 RVA: 0x000F982D File Offset: 0x000F7A2D
		public void GetObjectData(BinaryWriter writer)
		{
			writer.Write((byte)this.value);
		}

		// Token: 0x06002E06 RID: 11782 RVA: 0x000F983C File Offset: 0x000F7A3C
		public XmlQueryCardinality(BinaryReader reader)
		{
			this = new XmlQueryCardinality((int)reader.ReadByte());
		}

		// Token: 0x06002E07 RID: 11783 RVA: 0x000F984C File Offset: 0x000F7A4C
		// Note: this type is marked as 'beforefieldinit'.
		static XmlQueryCardinality()
		{
			XmlQueryCardinality[,] array = new XmlQueryCardinality[8, 8];
			array[0, 0] = XmlQueryCardinality.None;
			array[0, 1] = XmlQueryCardinality.Zero;
			array[0, 2] = XmlQueryCardinality.None;
			array[0, 3] = XmlQueryCardinality.Zero;
			array[0, 4] = XmlQueryCardinality.None;
			array[0, 5] = XmlQueryCardinality.Zero;
			array[0, 6] = XmlQueryCardinality.None;
			array[0, 7] = XmlQueryCardinality.Zero;
			array[1, 0] = XmlQueryCardinality.Zero;
			array[1, 1] = XmlQueryCardinality.Zero;
			array[1, 2] = XmlQueryCardinality.Zero;
			array[1, 3] = XmlQueryCardinality.Zero;
			array[1, 4] = XmlQueryCardinality.Zero;
			array[1, 5] = XmlQueryCardinality.Zero;
			array[1, 6] = XmlQueryCardinality.Zero;
			array[1, 7] = XmlQueryCardinality.Zero;
			array[2, 0] = XmlQueryCardinality.None;
			array[2, 1] = XmlQueryCardinality.Zero;
			array[2, 2] = XmlQueryCardinality.One;
			array[2, 3] = XmlQueryCardinality.ZeroOrOne;
			array[2, 4] = XmlQueryCardinality.More;
			array[2, 5] = XmlQueryCardinality.NotOne;
			array[2, 6] = XmlQueryCardinality.OneOrMore;
			array[2, 7] = XmlQueryCardinality.ZeroOrMore;
			array[3, 0] = XmlQueryCardinality.Zero;
			array[3, 1] = XmlQueryCardinality.Zero;
			array[3, 2] = XmlQueryCardinality.ZeroOrOne;
			array[3, 3] = XmlQueryCardinality.ZeroOrOne;
			array[3, 4] = XmlQueryCardinality.NotOne;
			array[3, 5] = XmlQueryCardinality.NotOne;
			array[3, 6] = XmlQueryCardinality.ZeroOrMore;
			array[3, 7] = XmlQueryCardinality.ZeroOrMore;
			array[4, 0] = XmlQueryCardinality.None;
			array[4, 1] = XmlQueryCardinality.Zero;
			array[4, 2] = XmlQueryCardinality.More;
			array[4, 3] = XmlQueryCardinality.NotOne;
			array[4, 4] = XmlQueryCardinality.More;
			array[4, 5] = XmlQueryCardinality.NotOne;
			array[4, 6] = XmlQueryCardinality.More;
			array[4, 7] = XmlQueryCardinality.NotOne;
			array[5, 0] = XmlQueryCardinality.Zero;
			array[5, 1] = XmlQueryCardinality.Zero;
			array[5, 2] = XmlQueryCardinality.NotOne;
			array[5, 3] = XmlQueryCardinality.NotOne;
			array[5, 4] = XmlQueryCardinality.NotOne;
			array[5, 5] = XmlQueryCardinality.NotOne;
			array[5, 6] = XmlQueryCardinality.NotOne;
			array[5, 7] = XmlQueryCardinality.NotOne;
			array[6, 0] = XmlQueryCardinality.None;
			array[6, 1] = XmlQueryCardinality.Zero;
			array[6, 2] = XmlQueryCardinality.OneOrMore;
			array[6, 3] = XmlQueryCardinality.ZeroOrMore;
			array[6, 4] = XmlQueryCardinality.More;
			array[6, 5] = XmlQueryCardinality.NotOne;
			array[6, 6] = XmlQueryCardinality.OneOrMore;
			array[6, 7] = XmlQueryCardinality.ZeroOrMore;
			array[7, 0] = XmlQueryCardinality.Zero;
			array[7, 1] = XmlQueryCardinality.Zero;
			array[7, 2] = XmlQueryCardinality.ZeroOrMore;
			array[7, 3] = XmlQueryCardinality.ZeroOrMore;
			array[7, 4] = XmlQueryCardinality.NotOne;
			array[7, 5] = XmlQueryCardinality.NotOne;
			array[7, 6] = XmlQueryCardinality.ZeroOrMore;
			array[7, 7] = XmlQueryCardinality.ZeroOrMore;
			XmlQueryCardinality.cardinalityProduct = array;
			XmlQueryCardinality[,] array2 = new XmlQueryCardinality[8, 8];
			array2[0, 0] = XmlQueryCardinality.None;
			array2[0, 1] = XmlQueryCardinality.Zero;
			array2[0, 2] = XmlQueryCardinality.One;
			array2[0, 3] = XmlQueryCardinality.ZeroOrOne;
			array2[0, 4] = XmlQueryCardinality.More;
			array2[0, 5] = XmlQueryCardinality.NotOne;
			array2[0, 6] = XmlQueryCardinality.OneOrMore;
			array2[0, 7] = XmlQueryCardinality.ZeroOrMore;
			array2[1, 0] = XmlQueryCardinality.Zero;
			array2[1, 1] = XmlQueryCardinality.Zero;
			array2[1, 2] = XmlQueryCardinality.One;
			array2[1, 3] = XmlQueryCardinality.ZeroOrOne;
			array2[1, 4] = XmlQueryCardinality.More;
			array2[1, 5] = XmlQueryCardinality.NotOne;
			array2[1, 6] = XmlQueryCardinality.OneOrMore;
			array2[1, 7] = XmlQueryCardinality.ZeroOrMore;
			array2[2, 0] = XmlQueryCardinality.One;
			array2[2, 1] = XmlQueryCardinality.One;
			array2[2, 2] = XmlQueryCardinality.More;
			array2[2, 3] = XmlQueryCardinality.OneOrMore;
			array2[2, 4] = XmlQueryCardinality.More;
			array2[2, 5] = XmlQueryCardinality.OneOrMore;
			array2[2, 6] = XmlQueryCardinality.More;
			array2[2, 7] = XmlQueryCardinality.OneOrMore;
			array2[3, 0] = XmlQueryCardinality.ZeroOrOne;
			array2[3, 1] = XmlQueryCardinality.ZeroOrOne;
			array2[3, 2] = XmlQueryCardinality.OneOrMore;
			array2[3, 3] = XmlQueryCardinality.ZeroOrMore;
			array2[3, 4] = XmlQueryCardinality.More;
			array2[3, 5] = XmlQueryCardinality.ZeroOrMore;
			array2[3, 6] = XmlQueryCardinality.OneOrMore;
			array2[3, 7] = XmlQueryCardinality.ZeroOrMore;
			array2[4, 0] = XmlQueryCardinality.More;
			array2[4, 1] = XmlQueryCardinality.More;
			array2[4, 2] = XmlQueryCardinality.More;
			array2[4, 3] = XmlQueryCardinality.More;
			array2[4, 4] = XmlQueryCardinality.More;
			array2[4, 5] = XmlQueryCardinality.More;
			array2[4, 6] = XmlQueryCardinality.More;
			array2[4, 7] = XmlQueryCardinality.More;
			array2[5, 0] = XmlQueryCardinality.NotOne;
			array2[5, 1] = XmlQueryCardinality.NotOne;
			array2[5, 2] = XmlQueryCardinality.OneOrMore;
			array2[5, 3] = XmlQueryCardinality.ZeroOrMore;
			array2[5, 4] = XmlQueryCardinality.More;
			array2[5, 5] = XmlQueryCardinality.NotOne;
			array2[5, 6] = XmlQueryCardinality.OneOrMore;
			array2[5, 7] = XmlQueryCardinality.ZeroOrMore;
			array2[6, 0] = XmlQueryCardinality.OneOrMore;
			array2[6, 1] = XmlQueryCardinality.OneOrMore;
			array2[6, 2] = XmlQueryCardinality.More;
			array2[6, 3] = XmlQueryCardinality.OneOrMore;
			array2[6, 4] = XmlQueryCardinality.More;
			array2[6, 5] = XmlQueryCardinality.OneOrMore;
			array2[6, 6] = XmlQueryCardinality.More;
			array2[6, 7] = XmlQueryCardinality.OneOrMore;
			array2[7, 0] = XmlQueryCardinality.ZeroOrMore;
			array2[7, 1] = XmlQueryCardinality.ZeroOrMore;
			array2[7, 2] = XmlQueryCardinality.OneOrMore;
			array2[7, 3] = XmlQueryCardinality.ZeroOrMore;
			array2[7, 4] = XmlQueryCardinality.More;
			array2[7, 5] = XmlQueryCardinality.ZeroOrMore;
			array2[7, 6] = XmlQueryCardinality.OneOrMore;
			array2[7, 7] = XmlQueryCardinality.ZeroOrMore;
			XmlQueryCardinality.cardinalitySum = array2;
			XmlQueryCardinality.toString = new string[]
			{
				"",
				"?",
				"",
				"?",
				"+",
				"*",
				"+",
				"*"
			};
			XmlQueryCardinality.serialized = new string[]
			{
				"None",
				"Zero",
				"One",
				"ZeroOrOne",
				"More",
				"NotOne",
				"OneOrMore",
				"ZeroOrMore"
			};
		}

		// Token: 0x04001F25 RID: 7973
		private int value;

		// Token: 0x04001F26 RID: 7974
		private static readonly XmlQueryCardinality[,] cardinalityProduct;

		// Token: 0x04001F27 RID: 7975
		private static readonly XmlQueryCardinality[,] cardinalitySum;

		// Token: 0x04001F28 RID: 7976
		private static readonly string[] toString;

		// Token: 0x04001F29 RID: 7977
		private static readonly string[] serialized;
	}
}
