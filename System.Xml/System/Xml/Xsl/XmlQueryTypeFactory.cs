﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml.Schema;
using System.Xml.XPath;

namespace System.Xml.Xsl
{
	// Token: 0x0200049A RID: 1178
	internal static class XmlQueryTypeFactory
	{
		// Token: 0x06002E2F RID: 11823 RVA: 0x000FACA5 File Offset: 0x000F8EA5
		public static XmlQueryType Type(XmlTypeCode code, bool isStrict)
		{
			return XmlQueryTypeFactory.ItemType.Create(code, isStrict);
		}

		// Token: 0x06002E30 RID: 11824 RVA: 0x000FACB0 File Offset: 0x000F8EB0
		public static XmlQueryType Type(XmlSchemaSimpleType schemaType, bool isStrict)
		{
			if (schemaType.Datatype.Variety == XmlSchemaDatatypeVariety.Atomic)
			{
				if (schemaType == DatatypeImplementation.AnySimpleType)
				{
					return XmlQueryTypeFactory.AnyAtomicTypeS;
				}
				return XmlQueryTypeFactory.ItemType.Create(schemaType, isStrict);
			}
			else
			{
				while (schemaType.DerivedBy == XmlSchemaDerivationMethod.Restriction)
				{
					schemaType = (XmlSchemaSimpleType)schemaType.BaseXmlSchemaType;
				}
				if (schemaType.DerivedBy == XmlSchemaDerivationMethod.List)
				{
					return XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Type(((XmlSchemaSimpleTypeList)schemaType.Content).BaseItemType, isStrict), XmlQueryCardinality.ZeroOrMore);
				}
				XmlSchemaSimpleType[] baseMemberTypes = ((XmlSchemaSimpleTypeUnion)schemaType.Content).BaseMemberTypes;
				XmlQueryType[] array = new XmlQueryType[baseMemberTypes.Length];
				for (int i = 0; i < baseMemberTypes.Length; i++)
				{
					array[i] = XmlQueryTypeFactory.Type(baseMemberTypes[i], isStrict);
				}
				return XmlQueryTypeFactory.Choice(array);
			}
		}

		// Token: 0x06002E31 RID: 11825 RVA: 0x000FAD5A File Offset: 0x000F8F5A
		public static XmlQueryType Choice(XmlQueryType left, XmlQueryType right)
		{
			return XmlQueryTypeFactory.SequenceType.Create(XmlQueryTypeFactory.ChoiceType.Create(XmlQueryTypeFactory.PrimeChoice(new List<XmlQueryType>(left), right)), left.Cardinality | right.Cardinality);
		}

		// Token: 0x06002E32 RID: 11826 RVA: 0x000FAD84 File Offset: 0x000F8F84
		public static XmlQueryType Choice(params XmlQueryType[] types)
		{
			if (types.Length == 0)
			{
				return XmlQueryTypeFactory.None;
			}
			if (types.Length == 1)
			{
				return types[0];
			}
			List<XmlQueryType> list = new List<XmlQueryType>(types[0]);
			XmlQueryCardinality xmlQueryCardinality = types[0].Cardinality;
			for (int i = 1; i < types.Length; i++)
			{
				XmlQueryTypeFactory.PrimeChoice(list, types[i]);
				xmlQueryCardinality |= types[i].Cardinality;
			}
			return XmlQueryTypeFactory.SequenceType.Create(XmlQueryTypeFactory.ChoiceType.Create(list), xmlQueryCardinality);
		}

		// Token: 0x06002E33 RID: 11827 RVA: 0x000FADEA File Offset: 0x000F8FEA
		public static XmlQueryType NodeChoice(XmlNodeKindFlags kinds)
		{
			return XmlQueryTypeFactory.ChoiceType.Create(kinds);
		}

		// Token: 0x06002E34 RID: 11828 RVA: 0x000FADF2 File Offset: 0x000F8FF2
		public static XmlQueryType Sequence(XmlQueryType left, XmlQueryType right)
		{
			return XmlQueryTypeFactory.SequenceType.Create(XmlQueryTypeFactory.ChoiceType.Create(XmlQueryTypeFactory.PrimeChoice(new List<XmlQueryType>(left), right)), left.Cardinality + right.Cardinality);
		}

		// Token: 0x06002E35 RID: 11829 RVA: 0x000FAE1B File Offset: 0x000F901B
		public static XmlQueryType PrimeProduct(XmlQueryType t, XmlQueryCardinality c)
		{
			if (t.Cardinality == c && !t.IsDod)
			{
				return t;
			}
			return XmlQueryTypeFactory.SequenceType.Create(t.Prime, c);
		}

		// Token: 0x06002E36 RID: 11830 RVA: 0x000FAE41 File Offset: 0x000F9041
		public static XmlQueryType Product(XmlQueryType t, XmlQueryCardinality c)
		{
			return XmlQueryTypeFactory.PrimeProduct(t, t.Cardinality * c);
		}

		// Token: 0x06002E37 RID: 11831 RVA: 0x000FAE55 File Offset: 0x000F9055
		public static XmlQueryType AtMost(XmlQueryType t, XmlQueryCardinality c)
		{
			return XmlQueryTypeFactory.PrimeProduct(t, c.AtMost());
		}

		// Token: 0x06002E38 RID: 11832 RVA: 0x000FAE64 File Offset: 0x000F9064
		private static List<XmlQueryType> PrimeChoice(List<XmlQueryType> accumulator, IList<XmlQueryType> types)
		{
			foreach (XmlQueryType itemType in types)
			{
				XmlQueryTypeFactory.AddItemToChoice(accumulator, itemType);
			}
			return accumulator;
		}

		// Token: 0x06002E39 RID: 11833 RVA: 0x000FAEB0 File Offset: 0x000F90B0
		private static void AddItemToChoice(List<XmlQueryType> accumulator, XmlQueryType itemType)
		{
			bool flag = true;
			for (int i = 0; i < accumulator.Count; i++)
			{
				if (itemType.IsSubtypeOf(accumulator[i]))
				{
					return;
				}
				if (accumulator[i].IsSubtypeOf(itemType))
				{
					if (flag)
					{
						flag = false;
						accumulator[i] = itemType;
					}
					else
					{
						accumulator.RemoveAt(i);
						i--;
					}
				}
			}
			if (flag)
			{
				accumulator.Add(itemType);
			}
		}

		// Token: 0x06002E3A RID: 11834 RVA: 0x000FAF13 File Offset: 0x000F9113
		public static XmlQueryType Type(XPathNodeType kind, XmlQualifiedNameTest nameTest, XmlSchemaType contentType, bool isNillable)
		{
			return XmlQueryTypeFactory.ItemType.Create(XmlQueryTypeFactory.NodeKindToTypeCode[(int)kind], nameTest, contentType, isNillable);
		}

		// Token: 0x06002E3B RID: 11835 RVA: 0x000FAF24 File Offset: 0x000F9124
		[Conditional("DEBUG")]
		public static void CheckSerializability(XmlQueryType type)
		{
			type.GetObjectData(new BinaryWriter(Stream.Null));
		}

		// Token: 0x06002E3C RID: 11836 RVA: 0x000FAF38 File Offset: 0x000F9138
		public static void Serialize(BinaryWriter writer, XmlQueryType type)
		{
			sbyte value;
			if (type.GetType() == typeof(XmlQueryTypeFactory.ItemType))
			{
				value = 0;
			}
			else if (type.GetType() == typeof(XmlQueryTypeFactory.ChoiceType))
			{
				value = 1;
			}
			else if (type.GetType() == typeof(XmlQueryTypeFactory.SequenceType))
			{
				value = 2;
			}
			else
			{
				value = -1;
			}
			writer.Write(value);
			type.GetObjectData(writer);
		}

		// Token: 0x06002E3D RID: 11837 RVA: 0x000FAFA8 File Offset: 0x000F91A8
		public static XmlQueryType Deserialize(BinaryReader reader)
		{
			switch (reader.ReadByte())
			{
			case 0:
				return XmlQueryTypeFactory.ItemType.Create(reader);
			case 1:
				return XmlQueryTypeFactory.ChoiceType.Create(reader);
			case 2:
				return XmlQueryTypeFactory.SequenceType.Create(reader);
			default:
				return null;
			}
		}

		// Token: 0x06002E3E RID: 11838 RVA: 0x000FAFE8 File Offset: 0x000F91E8
		// Note: this type is marked as 'beforefieldinit'.
		static XmlQueryTypeFactory()
		{
		}

		// Token: 0x04001F35 RID: 7989
		public static readonly XmlQueryType None = XmlQueryTypeFactory.ChoiceType.None;

		// Token: 0x04001F36 RID: 7990
		public static readonly XmlQueryType Empty = XmlQueryTypeFactory.SequenceType.Zero;

		// Token: 0x04001F37 RID: 7991
		public static readonly XmlQueryType Item = XmlQueryTypeFactory.Type(XmlTypeCode.Item, false);

		// Token: 0x04001F38 RID: 7992
		public static readonly XmlQueryType ItemS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Item, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F39 RID: 7993
		public static readonly XmlQueryType Node = XmlQueryTypeFactory.Type(XmlTypeCode.Node, false);

		// Token: 0x04001F3A RID: 7994
		public static readonly XmlQueryType NodeS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Node, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F3B RID: 7995
		public static readonly XmlQueryType Element = XmlQueryTypeFactory.Type(XmlTypeCode.Element, false);

		// Token: 0x04001F3C RID: 7996
		public static readonly XmlQueryType ElementS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Element, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F3D RID: 7997
		public static readonly XmlQueryType Document = XmlQueryTypeFactory.Type(XmlTypeCode.Document, false);

		// Token: 0x04001F3E RID: 7998
		public static readonly XmlQueryType DocumentS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Document, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F3F RID: 7999
		public static readonly XmlQueryType Attribute = XmlQueryTypeFactory.Type(XmlTypeCode.Attribute, false);

		// Token: 0x04001F40 RID: 8000
		public static readonly XmlQueryType AttributeQ = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Attribute, XmlQueryCardinality.ZeroOrOne);

		// Token: 0x04001F41 RID: 8001
		public static readonly XmlQueryType AttributeS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Attribute, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F42 RID: 8002
		public static readonly XmlQueryType Namespace = XmlQueryTypeFactory.Type(XmlTypeCode.Namespace, false);

		// Token: 0x04001F43 RID: 8003
		public static readonly XmlQueryType NamespaceS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Namespace, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F44 RID: 8004
		public static readonly XmlQueryType Text = XmlQueryTypeFactory.Type(XmlTypeCode.Text, false);

		// Token: 0x04001F45 RID: 8005
		public static readonly XmlQueryType TextS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Text, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F46 RID: 8006
		public static readonly XmlQueryType Comment = XmlQueryTypeFactory.Type(XmlTypeCode.Comment, false);

		// Token: 0x04001F47 RID: 8007
		public static readonly XmlQueryType CommentS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Comment, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F48 RID: 8008
		public static readonly XmlQueryType PI = XmlQueryTypeFactory.Type(XmlTypeCode.ProcessingInstruction, false);

		// Token: 0x04001F49 RID: 8009
		public static readonly XmlQueryType PIS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.PI, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F4A RID: 8010
		public static readonly XmlQueryType DocumentOrElement = XmlQueryTypeFactory.Choice(XmlQueryTypeFactory.Document, XmlQueryTypeFactory.Element);

		// Token: 0x04001F4B RID: 8011
		public static readonly XmlQueryType DocumentOrElementQ = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.DocumentOrElement, XmlQueryCardinality.ZeroOrOne);

		// Token: 0x04001F4C RID: 8012
		public static readonly XmlQueryType DocumentOrElementS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.DocumentOrElement, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F4D RID: 8013
		public static readonly XmlQueryType Content = XmlQueryTypeFactory.Choice(new XmlQueryType[]
		{
			XmlQueryTypeFactory.Element,
			XmlQueryTypeFactory.Comment,
			XmlQueryTypeFactory.PI,
			XmlQueryTypeFactory.Text
		});

		// Token: 0x04001F4E RID: 8014
		public static readonly XmlQueryType ContentS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.Content, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F4F RID: 8015
		public static readonly XmlQueryType DocumentOrContent = XmlQueryTypeFactory.Choice(XmlQueryTypeFactory.Document, XmlQueryTypeFactory.Content);

		// Token: 0x04001F50 RID: 8016
		public static readonly XmlQueryType DocumentOrContentS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.DocumentOrContent, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F51 RID: 8017
		public static readonly XmlQueryType AttributeOrContent = XmlQueryTypeFactory.Choice(XmlQueryTypeFactory.Attribute, XmlQueryTypeFactory.Content);

		// Token: 0x04001F52 RID: 8018
		public static readonly XmlQueryType AttributeOrContentS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.AttributeOrContent, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F53 RID: 8019
		public static readonly XmlQueryType AnyAtomicType = XmlQueryTypeFactory.Type(XmlTypeCode.AnyAtomicType, false);

		// Token: 0x04001F54 RID: 8020
		public static readonly XmlQueryType AnyAtomicTypeS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.AnyAtomicType, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F55 RID: 8021
		public static readonly XmlQueryType String = XmlQueryTypeFactory.Type(XmlTypeCode.String, false);

		// Token: 0x04001F56 RID: 8022
		public static readonly XmlQueryType StringX = XmlQueryTypeFactory.Type(XmlTypeCode.String, true);

		// Token: 0x04001F57 RID: 8023
		public static readonly XmlQueryType StringXS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.StringX, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F58 RID: 8024
		public static readonly XmlQueryType Boolean = XmlQueryTypeFactory.Type(XmlTypeCode.Boolean, false);

		// Token: 0x04001F59 RID: 8025
		public static readonly XmlQueryType BooleanX = XmlQueryTypeFactory.Type(XmlTypeCode.Boolean, true);

		// Token: 0x04001F5A RID: 8026
		public static readonly XmlQueryType Int = XmlQueryTypeFactory.Type(XmlTypeCode.Int, false);

		// Token: 0x04001F5B RID: 8027
		public static readonly XmlQueryType IntX = XmlQueryTypeFactory.Type(XmlTypeCode.Int, true);

		// Token: 0x04001F5C RID: 8028
		public static readonly XmlQueryType IntXS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.IntX, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F5D RID: 8029
		public static readonly XmlQueryType IntegerX = XmlQueryTypeFactory.Type(XmlTypeCode.Integer, true);

		// Token: 0x04001F5E RID: 8030
		public static readonly XmlQueryType LongX = XmlQueryTypeFactory.Type(XmlTypeCode.Long, true);

		// Token: 0x04001F5F RID: 8031
		public static readonly XmlQueryType DecimalX = XmlQueryTypeFactory.Type(XmlTypeCode.Decimal, true);

		// Token: 0x04001F60 RID: 8032
		public static readonly XmlQueryType FloatX = XmlQueryTypeFactory.Type(XmlTypeCode.Float, true);

		// Token: 0x04001F61 RID: 8033
		public static readonly XmlQueryType Double = XmlQueryTypeFactory.Type(XmlTypeCode.Double, false);

		// Token: 0x04001F62 RID: 8034
		public static readonly XmlQueryType DoubleX = XmlQueryTypeFactory.Type(XmlTypeCode.Double, true);

		// Token: 0x04001F63 RID: 8035
		public static readonly XmlQueryType DateTimeX = XmlQueryTypeFactory.Type(XmlTypeCode.DateTime, true);

		// Token: 0x04001F64 RID: 8036
		public static readonly XmlQueryType QNameX = XmlQueryTypeFactory.Type(XmlTypeCode.QName, true);

		// Token: 0x04001F65 RID: 8037
		public static readonly XmlQueryType UntypedDocument = XmlQueryTypeFactory.ItemType.UntypedDocument;

		// Token: 0x04001F66 RID: 8038
		public static readonly XmlQueryType UntypedElement = XmlQueryTypeFactory.ItemType.UntypedElement;

		// Token: 0x04001F67 RID: 8039
		public static readonly XmlQueryType UntypedAttribute = XmlQueryTypeFactory.ItemType.UntypedAttribute;

		// Token: 0x04001F68 RID: 8040
		public static readonly XmlQueryType UntypedNode = XmlQueryTypeFactory.Choice(new XmlQueryType[]
		{
			XmlQueryTypeFactory.UntypedDocument,
			XmlQueryTypeFactory.UntypedElement,
			XmlQueryTypeFactory.UntypedAttribute,
			XmlQueryTypeFactory.Namespace,
			XmlQueryTypeFactory.Text,
			XmlQueryTypeFactory.Comment,
			XmlQueryTypeFactory.PI
		});

		// Token: 0x04001F69 RID: 8041
		public static readonly XmlQueryType UntypedNodeS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.UntypedNode, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F6A RID: 8042
		public static readonly XmlQueryType NodeNotRtf = XmlQueryTypeFactory.ItemType.NodeNotRtf;

		// Token: 0x04001F6B RID: 8043
		public static readonly XmlQueryType NodeNotRtfQ = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.NodeNotRtf, XmlQueryCardinality.ZeroOrOne);

		// Token: 0x04001F6C RID: 8044
		public static readonly XmlQueryType NodeNotRtfS = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.NodeNotRtf, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F6D RID: 8045
		public static readonly XmlQueryType NodeSDod = XmlQueryTypeFactory.PrimeProduct(XmlQueryTypeFactory.NodeNotRtf, XmlQueryCardinality.ZeroOrMore);

		// Token: 0x04001F6E RID: 8046
		private static readonly XmlTypeCode[] NodeKindToTypeCode = new XmlTypeCode[]
		{
			XmlTypeCode.Document,
			XmlTypeCode.Element,
			XmlTypeCode.Attribute,
			XmlTypeCode.Namespace,
			XmlTypeCode.Text,
			XmlTypeCode.Text,
			XmlTypeCode.Text,
			XmlTypeCode.ProcessingInstruction,
			XmlTypeCode.Comment,
			XmlTypeCode.Node
		};

		// Token: 0x0200049B RID: 1179
		private sealed class ItemType : XmlQueryType
		{
			// Token: 0x06002E3F RID: 11839 RVA: 0x000FB3E4 File Offset: 0x000F95E4
			static ItemType()
			{
				int num = 55;
				XmlQueryTypeFactory.ItemType.BuiltInItemTypes = new XmlQueryType[num];
				XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict = new XmlQueryType[num];
				for (int i = 0; i < num; i++)
				{
					XmlTypeCode typeCode = (XmlTypeCode)i;
					switch (i)
					{
					case 0:
						XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i] = XmlQueryTypeFactory.ChoiceType.None;
						XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict[i] = XmlQueryTypeFactory.ChoiceType.None;
						break;
					case 1:
					case 2:
						XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i] = new XmlQueryTypeFactory.ItemType(typeCode, XmlQualifiedNameTest.Wildcard, XmlSchemaComplexType.AnyType, false, false, false);
						XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict[i] = XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i];
						break;
					case 3:
					case 4:
					case 6:
					case 7:
					case 8:
					case 9:
						XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i] = new XmlQueryTypeFactory.ItemType(typeCode, XmlQualifiedNameTest.Wildcard, XmlSchemaComplexType.AnyType, false, false, true);
						XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict[i] = XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i];
						break;
					case 5:
						XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i] = new XmlQueryTypeFactory.ItemType(typeCode, XmlQualifiedNameTest.Wildcard, DatatypeImplementation.AnySimpleType, false, false, true);
						XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict[i] = XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i];
						break;
					case 10:
						XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i] = new XmlQueryTypeFactory.ItemType(typeCode, XmlQualifiedNameTest.Wildcard, DatatypeImplementation.AnyAtomicType, false, false, true);
						XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict[i] = XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i];
						break;
					case 11:
						XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i] = new XmlQueryTypeFactory.ItemType(typeCode, XmlQualifiedNameTest.Wildcard, DatatypeImplementation.UntypedAtomicType, false, true, true);
						XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict[i] = XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i];
						break;
					default:
					{
						XmlSchemaType builtInSimpleType = XmlSchemaType.GetBuiltInSimpleType(typeCode);
						XmlQueryTypeFactory.ItemType.BuiltInItemTypes[i] = new XmlQueryTypeFactory.ItemType(typeCode, XmlQualifiedNameTest.Wildcard, builtInSimpleType, false, false, true);
						XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict[i] = new XmlQueryTypeFactory.ItemType(typeCode, XmlQualifiedNameTest.Wildcard, builtInSimpleType, false, true, true);
						break;
					}
					}
				}
				XmlQueryTypeFactory.ItemType.UntypedDocument = new XmlQueryTypeFactory.ItemType(XmlTypeCode.Document, XmlQualifiedNameTest.Wildcard, XmlSchemaComplexType.UntypedAnyType, false, false, true);
				XmlQueryTypeFactory.ItemType.UntypedElement = new XmlQueryTypeFactory.ItemType(XmlTypeCode.Element, XmlQualifiedNameTest.Wildcard, XmlSchemaComplexType.UntypedAnyType, false, false, true);
				XmlQueryTypeFactory.ItemType.UntypedAttribute = new XmlQueryTypeFactory.ItemType(XmlTypeCode.Attribute, XmlQualifiedNameTest.Wildcard, DatatypeImplementation.UntypedAtomicType, false, false, true);
				XmlQueryTypeFactory.ItemType.NodeNotRtf = new XmlQueryTypeFactory.ItemType(XmlTypeCode.Node, XmlQualifiedNameTest.Wildcard, XmlSchemaComplexType.AnyType, false, false, true);
				XmlQueryTypeFactory.ItemType.SpecialBuiltInItemTypes = new XmlQueryType[]
				{
					XmlQueryTypeFactory.ItemType.UntypedDocument,
					XmlQueryTypeFactory.ItemType.UntypedElement,
					XmlQueryTypeFactory.ItemType.UntypedAttribute,
					XmlQueryTypeFactory.ItemType.NodeNotRtf
				};
			}

			// Token: 0x06002E40 RID: 11840 RVA: 0x000FB614 File Offset: 0x000F9814
			public static XmlQueryType Create(XmlTypeCode code, bool isStrict)
			{
				if (isStrict)
				{
					return XmlQueryTypeFactory.ItemType.BuiltInItemTypesStrict[(int)code];
				}
				return XmlQueryTypeFactory.ItemType.BuiltInItemTypes[(int)code];
			}

			// Token: 0x06002E41 RID: 11841 RVA: 0x000FB628 File Offset: 0x000F9828
			public static XmlQueryType Create(XmlSchemaSimpleType schemaType, bool isStrict)
			{
				XmlTypeCode typeCode = schemaType.Datatype.TypeCode;
				if (schemaType == XmlSchemaType.GetBuiltInSimpleType(typeCode))
				{
					return XmlQueryTypeFactory.ItemType.Create(typeCode, isStrict);
				}
				return new XmlQueryTypeFactory.ItemType(typeCode, XmlQualifiedNameTest.Wildcard, schemaType, false, isStrict, true);
			}

			// Token: 0x06002E42 RID: 11842 RVA: 0x000FB664 File Offset: 0x000F9864
			public static XmlQueryType Create(XmlTypeCode code, XmlQualifiedNameTest nameTest, XmlSchemaType contentType, bool isNillable)
			{
				if (code - XmlTypeCode.Document <= 1)
				{
					if (nameTest.IsWildcard)
					{
						if (contentType == XmlSchemaComplexType.AnyType)
						{
							return XmlQueryTypeFactory.ItemType.Create(code, false);
						}
						if (contentType == XmlSchemaComplexType.UntypedAnyType)
						{
							if (code == XmlTypeCode.Element)
							{
								return XmlQueryTypeFactory.ItemType.UntypedElement;
							}
							if (code == XmlTypeCode.Document)
							{
								return XmlQueryTypeFactory.ItemType.UntypedDocument;
							}
						}
					}
					return new XmlQueryTypeFactory.ItemType(code, nameTest, contentType, isNillable, false, true);
				}
				if (code != XmlTypeCode.Attribute)
				{
					return XmlQueryTypeFactory.ItemType.Create(code, false);
				}
				if (nameTest.IsWildcard)
				{
					if (contentType == DatatypeImplementation.AnySimpleType)
					{
						return XmlQueryTypeFactory.ItemType.Create(code, false);
					}
					if (contentType == DatatypeImplementation.UntypedAtomicType)
					{
						return XmlQueryTypeFactory.ItemType.UntypedAttribute;
					}
				}
				return new XmlQueryTypeFactory.ItemType(code, nameTest, contentType, isNillable, false, true);
			}

			// Token: 0x06002E43 RID: 11843 RVA: 0x000FB6F8 File Offset: 0x000F98F8
			private ItemType(XmlTypeCode code, XmlQualifiedNameTest nameTest, XmlSchemaType schemaType, bool isNillable, bool isStrict, bool isNotRtf)
			{
				this.code = code;
				this.nameTest = nameTest;
				this.schemaType = schemaType;
				this.isNillable = isNillable;
				this.isStrict = isStrict;
				this.isNotRtf = isNotRtf;
				switch (code)
				{
				case XmlTypeCode.Item:
					this.nodeKinds = XmlNodeKindFlags.Any;
					return;
				case XmlTypeCode.Node:
					this.nodeKinds = XmlNodeKindFlags.Any;
					return;
				case XmlTypeCode.Document:
					this.nodeKinds = XmlNodeKindFlags.Document;
					return;
				case XmlTypeCode.Element:
					this.nodeKinds = XmlNodeKindFlags.Element;
					return;
				case XmlTypeCode.Attribute:
					this.nodeKinds = XmlNodeKindFlags.Attribute;
					return;
				case XmlTypeCode.Namespace:
					this.nodeKinds = XmlNodeKindFlags.Namespace;
					return;
				case XmlTypeCode.ProcessingInstruction:
					this.nodeKinds = XmlNodeKindFlags.PI;
					return;
				case XmlTypeCode.Comment:
					this.nodeKinds = XmlNodeKindFlags.Comment;
					return;
				case XmlTypeCode.Text:
					this.nodeKinds = XmlNodeKindFlags.Text;
					return;
				default:
					this.nodeKinds = XmlNodeKindFlags.None;
					return;
				}
			}

			// Token: 0x06002E44 RID: 11844 RVA: 0x000FB7BC File Offset: 0x000F99BC
			public override void GetObjectData(BinaryWriter writer)
			{
				sbyte b = (sbyte)this.code;
				for (int i = 0; i < XmlQueryTypeFactory.ItemType.SpecialBuiltInItemTypes.Length; i++)
				{
					if (this == XmlQueryTypeFactory.ItemType.SpecialBuiltInItemTypes[i])
					{
						b = (sbyte)(~(sbyte)i);
						break;
					}
				}
				writer.Write(b);
				if (0 <= b)
				{
					writer.Write(this.isStrict);
				}
			}

			// Token: 0x06002E45 RID: 11845 RVA: 0x000FB80C File Offset: 0x000F9A0C
			public static XmlQueryType Create(BinaryReader reader)
			{
				sbyte b = reader.ReadSByte();
				if (0 <= b)
				{
					return XmlQueryTypeFactory.ItemType.Create((XmlTypeCode)b, reader.ReadBoolean());
				}
				return XmlQueryTypeFactory.ItemType.SpecialBuiltInItemTypes[(int)(~(int)b)];
			}

			// Token: 0x170009B8 RID: 2488
			// (get) Token: 0x06002E46 RID: 11846 RVA: 0x000FB839 File Offset: 0x000F9A39
			public override XmlTypeCode TypeCode
			{
				get
				{
					return this.code;
				}
			}

			// Token: 0x170009B9 RID: 2489
			// (get) Token: 0x06002E47 RID: 11847 RVA: 0x000FB841 File Offset: 0x000F9A41
			public override XmlQualifiedNameTest NameTest
			{
				get
				{
					return this.nameTest;
				}
			}

			// Token: 0x170009BA RID: 2490
			// (get) Token: 0x06002E48 RID: 11848 RVA: 0x000FB849 File Offset: 0x000F9A49
			public override XmlSchemaType SchemaType
			{
				get
				{
					return this.schemaType;
				}
			}

			// Token: 0x170009BB RID: 2491
			// (get) Token: 0x06002E49 RID: 11849 RVA: 0x000FB851 File Offset: 0x000F9A51
			public override bool IsNillable
			{
				get
				{
					return this.isNillable;
				}
			}

			// Token: 0x170009BC RID: 2492
			// (get) Token: 0x06002E4A RID: 11850 RVA: 0x000FB859 File Offset: 0x000F9A59
			public override XmlNodeKindFlags NodeKinds
			{
				get
				{
					return this.nodeKinds;
				}
			}

			// Token: 0x170009BD RID: 2493
			// (get) Token: 0x06002E4B RID: 11851 RVA: 0x000FB861 File Offset: 0x000F9A61
			public override bool IsStrict
			{
				get
				{
					return this.isStrict;
				}
			}

			// Token: 0x170009BE RID: 2494
			// (get) Token: 0x06002E4C RID: 11852 RVA: 0x000FB869 File Offset: 0x000F9A69
			public override bool IsNotRtf
			{
				get
				{
					return this.isNotRtf;
				}
			}

			// Token: 0x170009BF RID: 2495
			// (get) Token: 0x06002E4D RID: 11853 RVA: 0x000020CD File Offset: 0x000002CD
			public override bool IsDod
			{
				get
				{
					return false;
				}
			}

			// Token: 0x170009C0 RID: 2496
			// (get) Token: 0x06002E4E RID: 11854 RVA: 0x000FB871 File Offset: 0x000F9A71
			public override XmlQueryCardinality Cardinality
			{
				get
				{
					return XmlQueryCardinality.One;
				}
			}

			// Token: 0x170009C1 RID: 2497
			// (get) Token: 0x06002E4F RID: 11855 RVA: 0x00002068 File Offset: 0x00000268
			public override XmlQueryType Prime
			{
				get
				{
					return this;
				}
			}

			// Token: 0x170009C2 RID: 2498
			// (get) Token: 0x06002E50 RID: 11856 RVA: 0x000FB878 File Offset: 0x000F9A78
			public override XmlValueConverter ClrMapping
			{
				get
				{
					if (base.IsAtomicValue)
					{
						return this.SchemaType.ValueConverter;
					}
					if (base.IsNode)
					{
						return XmlNodeConverter.Node;
					}
					return XmlAnyConverter.Item;
				}
			}

			// Token: 0x170009C3 RID: 2499
			// (get) Token: 0x06002E51 RID: 11857 RVA: 0x000033DE File Offset: 0x000015DE
			public override int Count
			{
				get
				{
					return 1;
				}
			}

			// Token: 0x170009C4 RID: 2500
			public override XmlQueryType this[int index]
			{
				get
				{
					if (index != 0)
					{
						throw new IndexOutOfRangeException();
					}
					return this;
				}
				set
				{
					throw new NotSupportedException();
				}
			}

			// Token: 0x04001F6F RID: 8047
			public static readonly XmlQueryType UntypedDocument;

			// Token: 0x04001F70 RID: 8048
			public static readonly XmlQueryType UntypedElement;

			// Token: 0x04001F71 RID: 8049
			public static readonly XmlQueryType UntypedAttribute;

			// Token: 0x04001F72 RID: 8050
			public static readonly XmlQueryType NodeNotRtf;

			// Token: 0x04001F73 RID: 8051
			private static XmlQueryType[] BuiltInItemTypes;

			// Token: 0x04001F74 RID: 8052
			private static XmlQueryType[] BuiltInItemTypesStrict;

			// Token: 0x04001F75 RID: 8053
			private static XmlQueryType[] SpecialBuiltInItemTypes;

			// Token: 0x04001F76 RID: 8054
			private XmlTypeCode code;

			// Token: 0x04001F77 RID: 8055
			private XmlQualifiedNameTest nameTest;

			// Token: 0x04001F78 RID: 8056
			private XmlSchemaType schemaType;

			// Token: 0x04001F79 RID: 8057
			private bool isNillable;

			// Token: 0x04001F7A RID: 8058
			private XmlNodeKindFlags nodeKinds;

			// Token: 0x04001F7B RID: 8059
			private bool isStrict;

			// Token: 0x04001F7C RID: 8060
			private bool isNotRtf;
		}

		// Token: 0x0200049C RID: 1180
		private sealed class ChoiceType : XmlQueryType
		{
			// Token: 0x06002E54 RID: 11860 RVA: 0x000FB8B0 File Offset: 0x000F9AB0
			public static XmlQueryType Create(XmlNodeKindFlags nodeKinds)
			{
				if (Bits.ExactlyOne((uint)nodeKinds))
				{
					return XmlQueryTypeFactory.ItemType.Create(XmlQueryTypeFactory.ChoiceType.NodeKindToTypeCode[Bits.LeastPosition((uint)nodeKinds)], false);
				}
				List<XmlQueryType> list = new List<XmlQueryType>();
				while (nodeKinds != XmlNodeKindFlags.None)
				{
					list.Add(XmlQueryTypeFactory.ItemType.Create(XmlQueryTypeFactory.ChoiceType.NodeKindToTypeCode[Bits.LeastPosition((uint)nodeKinds)], false));
					nodeKinds = (XmlNodeKindFlags)Bits.ClearLeast((uint)nodeKinds);
				}
				return XmlQueryTypeFactory.ChoiceType.Create(list);
			}

			// Token: 0x06002E55 RID: 11861 RVA: 0x000FB909 File Offset: 0x000F9B09
			public static XmlQueryType Create(List<XmlQueryType> members)
			{
				if (members.Count == 0)
				{
					return XmlQueryTypeFactory.ChoiceType.None;
				}
				if (members.Count == 1)
				{
					return members[0];
				}
				return new XmlQueryTypeFactory.ChoiceType(members);
			}

			// Token: 0x06002E56 RID: 11862 RVA: 0x000FB930 File Offset: 0x000F9B30
			private ChoiceType(List<XmlQueryType> members)
			{
				this.members = members;
				for (int i = 0; i < members.Count; i++)
				{
					XmlQueryType xmlQueryType = members[i];
					if (this.code == XmlTypeCode.None)
					{
						this.code = xmlQueryType.TypeCode;
						this.schemaType = xmlQueryType.SchemaType;
					}
					else if (base.IsNode && xmlQueryType.IsNode)
					{
						if (this.code == xmlQueryType.TypeCode)
						{
							if (this.code == XmlTypeCode.Element)
							{
								this.schemaType = XmlSchemaComplexType.AnyType;
							}
							else if (this.code == XmlTypeCode.Attribute)
							{
								this.schemaType = DatatypeImplementation.AnySimpleType;
							}
						}
						else
						{
							this.code = XmlTypeCode.Node;
							this.schemaType = null;
						}
					}
					else if (base.IsAtomicValue && xmlQueryType.IsAtomicValue)
					{
						this.code = XmlTypeCode.AnyAtomicType;
						this.schemaType = DatatypeImplementation.AnyAtomicType;
					}
					else
					{
						this.code = XmlTypeCode.Item;
						this.schemaType = null;
					}
					this.nodeKinds |= xmlQueryType.NodeKinds;
				}
			}

			// Token: 0x06002E57 RID: 11863 RVA: 0x000FBA30 File Offset: 0x000F9C30
			public override void GetObjectData(BinaryWriter writer)
			{
				writer.Write(this.members.Count);
				for (int i = 0; i < this.members.Count; i++)
				{
					XmlQueryTypeFactory.Serialize(writer, this.members[i]);
				}
			}

			// Token: 0x06002E58 RID: 11864 RVA: 0x000FBA78 File Offset: 0x000F9C78
			public static XmlQueryType Create(BinaryReader reader)
			{
				int num = reader.ReadInt32();
				List<XmlQueryType> list = new List<XmlQueryType>(num);
				for (int i = 0; i < num; i++)
				{
					list.Add(XmlQueryTypeFactory.Deserialize(reader));
				}
				return XmlQueryTypeFactory.ChoiceType.Create(list);
			}

			// Token: 0x170009C5 RID: 2501
			// (get) Token: 0x06002E59 RID: 11865 RVA: 0x000FBAB1 File Offset: 0x000F9CB1
			public override XmlTypeCode TypeCode
			{
				get
				{
					return this.code;
				}
			}

			// Token: 0x170009C6 RID: 2502
			// (get) Token: 0x06002E5A RID: 11866 RVA: 0x000FBAB9 File Offset: 0x000F9CB9
			public override XmlQualifiedNameTest NameTest
			{
				get
				{
					return XmlQualifiedNameTest.Wildcard;
				}
			}

			// Token: 0x170009C7 RID: 2503
			// (get) Token: 0x06002E5B RID: 11867 RVA: 0x000FBAC0 File Offset: 0x000F9CC0
			public override XmlSchemaType SchemaType
			{
				get
				{
					return this.schemaType;
				}
			}

			// Token: 0x170009C8 RID: 2504
			// (get) Token: 0x06002E5C RID: 11868 RVA: 0x000020CD File Offset: 0x000002CD
			public override bool IsNillable
			{
				get
				{
					return false;
				}
			}

			// Token: 0x170009C9 RID: 2505
			// (get) Token: 0x06002E5D RID: 11869 RVA: 0x000FBAC8 File Offset: 0x000F9CC8
			public override XmlNodeKindFlags NodeKinds
			{
				get
				{
					return this.nodeKinds;
				}
			}

			// Token: 0x170009CA RID: 2506
			// (get) Token: 0x06002E5E RID: 11870 RVA: 0x000FBAD0 File Offset: 0x000F9CD0
			public override bool IsStrict
			{
				get
				{
					return this.members.Count == 0;
				}
			}

			// Token: 0x170009CB RID: 2507
			// (get) Token: 0x06002E5F RID: 11871 RVA: 0x000FBAE0 File Offset: 0x000F9CE0
			public override bool IsNotRtf
			{
				get
				{
					for (int i = 0; i < this.members.Count; i++)
					{
						if (!this.members[i].IsNotRtf)
						{
							return false;
						}
					}
					return true;
				}
			}

			// Token: 0x170009CC RID: 2508
			// (get) Token: 0x06002E60 RID: 11872 RVA: 0x000020CD File Offset: 0x000002CD
			public override bool IsDod
			{
				get
				{
					return false;
				}
			}

			// Token: 0x170009CD RID: 2509
			// (get) Token: 0x06002E61 RID: 11873 RVA: 0x000FBB19 File Offset: 0x000F9D19
			public override XmlQueryCardinality Cardinality
			{
				get
				{
					if (this.TypeCode != XmlTypeCode.None)
					{
						return XmlQueryCardinality.One;
					}
					return XmlQueryCardinality.None;
				}
			}

			// Token: 0x170009CE RID: 2510
			// (get) Token: 0x06002E62 RID: 11874 RVA: 0x00002068 File Offset: 0x00000268
			public override XmlQueryType Prime
			{
				get
				{
					return this;
				}
			}

			// Token: 0x170009CF RID: 2511
			// (get) Token: 0x06002E63 RID: 11875 RVA: 0x000FBB2E File Offset: 0x000F9D2E
			public override XmlValueConverter ClrMapping
			{
				get
				{
					if (this.code == XmlTypeCode.None || this.code == XmlTypeCode.Item)
					{
						return XmlAnyConverter.Item;
					}
					if (base.IsAtomicValue)
					{
						return this.SchemaType.ValueConverter;
					}
					return XmlNodeConverter.Node;
				}
			}

			// Token: 0x170009D0 RID: 2512
			// (get) Token: 0x06002E64 RID: 11876 RVA: 0x000FBB60 File Offset: 0x000F9D60
			public override int Count
			{
				get
				{
					return this.members.Count;
				}
			}

			// Token: 0x170009D1 RID: 2513
			public override XmlQueryType this[int index]
			{
				get
				{
					return this.members[index];
				}
				set
				{
					throw new NotSupportedException();
				}
			}

			// Token: 0x06002E67 RID: 11879 RVA: 0x000FBB7B File Offset: 0x000F9D7B
			// Note: this type is marked as 'beforefieldinit'.
			static ChoiceType()
			{
			}

			// Token: 0x04001F7D RID: 8061
			public static readonly XmlQueryType None = new XmlQueryTypeFactory.ChoiceType(new List<XmlQueryType>());

			// Token: 0x04001F7E RID: 8062
			private XmlTypeCode code;

			// Token: 0x04001F7F RID: 8063
			private XmlSchemaType schemaType;

			// Token: 0x04001F80 RID: 8064
			private XmlNodeKindFlags nodeKinds;

			// Token: 0x04001F81 RID: 8065
			private List<XmlQueryType> members;

			// Token: 0x04001F82 RID: 8066
			private static readonly XmlTypeCode[] NodeKindToTypeCode = new XmlTypeCode[]
			{
				XmlTypeCode.None,
				XmlTypeCode.Document,
				XmlTypeCode.Element,
				XmlTypeCode.Attribute,
				XmlTypeCode.Text,
				XmlTypeCode.Comment,
				XmlTypeCode.ProcessingInstruction,
				XmlTypeCode.Namespace
			};
		}

		// Token: 0x0200049D RID: 1181
		private sealed class SequenceType : XmlQueryType
		{
			// Token: 0x06002E68 RID: 11880 RVA: 0x000FBBA4 File Offset: 0x000F9DA4
			public static XmlQueryType Create(XmlQueryType prime, XmlQueryCardinality card)
			{
				if (prime.TypeCode == XmlTypeCode.None)
				{
					if (!(XmlQueryCardinality.Zero <= card))
					{
						return XmlQueryTypeFactory.None;
					}
					return XmlQueryTypeFactory.SequenceType.Zero;
				}
				else
				{
					if (card == XmlQueryCardinality.None)
					{
						return XmlQueryTypeFactory.None;
					}
					if (card == XmlQueryCardinality.Zero)
					{
						return XmlQueryTypeFactory.SequenceType.Zero;
					}
					if (card == XmlQueryCardinality.One)
					{
						return prime;
					}
					return new XmlQueryTypeFactory.SequenceType(prime, card);
				}
			}

			// Token: 0x06002E69 RID: 11881 RVA: 0x000FBC0E File Offset: 0x000F9E0E
			private SequenceType(XmlQueryType prime, XmlQueryCardinality card)
			{
				this.prime = prime;
				this.card = card;
			}

			// Token: 0x06002E6A RID: 11882 RVA: 0x000FBC24 File Offset: 0x000F9E24
			public override void GetObjectData(BinaryWriter writer)
			{
				writer.Write(this.IsDod);
				if (this.IsDod)
				{
					return;
				}
				XmlQueryTypeFactory.Serialize(writer, this.prime);
				this.card.GetObjectData(writer);
			}

			// Token: 0x06002E6B RID: 11883 RVA: 0x000FBC54 File Offset: 0x000F9E54
			public static XmlQueryType Create(BinaryReader reader)
			{
				if (reader.ReadBoolean())
				{
					return XmlQueryTypeFactory.NodeSDod;
				}
				XmlQueryType xmlQueryType = XmlQueryTypeFactory.Deserialize(reader);
				XmlQueryCardinality xmlQueryCardinality = new XmlQueryCardinality(reader);
				return XmlQueryTypeFactory.SequenceType.Create(xmlQueryType, xmlQueryCardinality);
			}

			// Token: 0x170009D2 RID: 2514
			// (get) Token: 0x06002E6C RID: 11884 RVA: 0x000FBC83 File Offset: 0x000F9E83
			public override XmlTypeCode TypeCode
			{
				get
				{
					return this.prime.TypeCode;
				}
			}

			// Token: 0x170009D3 RID: 2515
			// (get) Token: 0x06002E6D RID: 11885 RVA: 0x000FBC90 File Offset: 0x000F9E90
			public override XmlQualifiedNameTest NameTest
			{
				get
				{
					return this.prime.NameTest;
				}
			}

			// Token: 0x170009D4 RID: 2516
			// (get) Token: 0x06002E6E RID: 11886 RVA: 0x000FBC9D File Offset: 0x000F9E9D
			public override XmlSchemaType SchemaType
			{
				get
				{
					return this.prime.SchemaType;
				}
			}

			// Token: 0x170009D5 RID: 2517
			// (get) Token: 0x06002E6F RID: 11887 RVA: 0x000FBCAA File Offset: 0x000F9EAA
			public override bool IsNillable
			{
				get
				{
					return this.prime.IsNillable;
				}
			}

			// Token: 0x170009D6 RID: 2518
			// (get) Token: 0x06002E70 RID: 11888 RVA: 0x000FBCB7 File Offset: 0x000F9EB7
			public override XmlNodeKindFlags NodeKinds
			{
				get
				{
					return this.prime.NodeKinds;
				}
			}

			// Token: 0x170009D7 RID: 2519
			// (get) Token: 0x06002E71 RID: 11889 RVA: 0x000FBCC4 File Offset: 0x000F9EC4
			public override bool IsStrict
			{
				get
				{
					return this.prime.IsStrict;
				}
			}

			// Token: 0x170009D8 RID: 2520
			// (get) Token: 0x06002E72 RID: 11890 RVA: 0x000FBCD1 File Offset: 0x000F9ED1
			public override bool IsNotRtf
			{
				get
				{
					return this.prime.IsNotRtf;
				}
			}

			// Token: 0x170009D9 RID: 2521
			// (get) Token: 0x06002E73 RID: 11891 RVA: 0x000FBCDE File Offset: 0x000F9EDE
			public override bool IsDod
			{
				get
				{
					return this == XmlQueryTypeFactory.NodeSDod;
				}
			}

			// Token: 0x170009DA RID: 2522
			// (get) Token: 0x06002E74 RID: 11892 RVA: 0x000FBCE8 File Offset: 0x000F9EE8
			public override XmlQueryCardinality Cardinality
			{
				get
				{
					return this.card;
				}
			}

			// Token: 0x170009DB RID: 2523
			// (get) Token: 0x06002E75 RID: 11893 RVA: 0x000FBCF0 File Offset: 0x000F9EF0
			public override XmlQueryType Prime
			{
				get
				{
					return this.prime;
				}
			}

			// Token: 0x170009DC RID: 2524
			// (get) Token: 0x06002E76 RID: 11894 RVA: 0x000FBCF8 File Offset: 0x000F9EF8
			public override XmlValueConverter ClrMapping
			{
				get
				{
					if (this.converter == null)
					{
						this.converter = XmlListConverter.Create(this.prime.ClrMapping);
					}
					return this.converter;
				}
			}

			// Token: 0x170009DD RID: 2525
			// (get) Token: 0x06002E77 RID: 11895 RVA: 0x000FBD1E File Offset: 0x000F9F1E
			public override int Count
			{
				get
				{
					return this.prime.Count;
				}
			}

			// Token: 0x170009DE RID: 2526
			public override XmlQueryType this[int index]
			{
				get
				{
					return this.prime[index];
				}
				set
				{
					throw new NotSupportedException();
				}
			}

			// Token: 0x06002E7A RID: 11898 RVA: 0x000FBD39 File Offset: 0x000F9F39
			// Note: this type is marked as 'beforefieldinit'.
			static SequenceType()
			{
			}

			// Token: 0x04001F83 RID: 8067
			public static readonly XmlQueryType Zero = new XmlQueryTypeFactory.SequenceType(XmlQueryTypeFactory.ChoiceType.None, XmlQueryCardinality.Zero);

			// Token: 0x04001F84 RID: 8068
			private XmlQueryType prime;

			// Token: 0x04001F85 RID: 8069
			private XmlQueryCardinality card;

			// Token: 0x04001F86 RID: 8070
			private XmlValueConverter converter;
		}
	}
}
