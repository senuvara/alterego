﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Xml.Xsl
{
	// Token: 0x02000489 RID: 1161
	internal abstract class ListBase<T> : IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable, IList, ICollection
	{
		// Token: 0x17000977 RID: 2423
		// (get) Token: 0x06002D61 RID: 11617
		public abstract int Count { get; }

		// Token: 0x17000978 RID: 2424
		public abstract T this[int index]
		{
			get;
			set;
		}

		// Token: 0x06002D64 RID: 11620 RVA: 0x000F5BF4 File Offset: 0x000F3DF4
		public virtual bool Contains(T value)
		{
			return this.IndexOf(value) != -1;
		}

		// Token: 0x06002D65 RID: 11621 RVA: 0x000F5C04 File Offset: 0x000F3E04
		public virtual int IndexOf(T value)
		{
			for (int i = 0; i < this.Count; i++)
			{
				if (value.Equals(this[i]))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06002D66 RID: 11622 RVA: 0x000F5C40 File Offset: 0x000F3E40
		public virtual void CopyTo(T[] array, int index)
		{
			for (int i = 0; i < this.Count; i++)
			{
				array[index + i] = this[i];
			}
		}

		// Token: 0x06002D67 RID: 11623 RVA: 0x000F5C6E File Offset: 0x000F3E6E
		public virtual IListEnumerator<T> GetEnumerator()
		{
			return new IListEnumerator<T>(this);
		}

		// Token: 0x17000979 RID: 2425
		// (get) Token: 0x06002D68 RID: 11624 RVA: 0x000033DE File Offset: 0x000015DE
		public virtual bool IsFixedSize
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700097A RID: 2426
		// (get) Token: 0x06002D69 RID: 11625 RVA: 0x000033DE File Offset: 0x000015DE
		public virtual bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06002D6A RID: 11626 RVA: 0x000F5C76 File Offset: 0x000F3E76
		public virtual void Add(T value)
		{
			this.Insert(this.Count, value);
		}

		// Token: 0x06002D6B RID: 11627 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual void Insert(int index, T value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002D6C RID: 11628 RVA: 0x000F5C88 File Offset: 0x000F3E88
		public virtual bool Remove(T value)
		{
			int num = this.IndexOf(value);
			if (num >= 0)
			{
				this.RemoveAt(num);
				return true;
			}
			return false;
		}

		// Token: 0x06002D6D RID: 11629 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual void RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002D6E RID: 11630 RVA: 0x000F5CAC File Offset: 0x000F3EAC
		public virtual void Clear()
		{
			for (int i = this.Count - 1; i >= 0; i--)
			{
				this.RemoveAt(i);
			}
		}

		// Token: 0x06002D6F RID: 11631 RVA: 0x000F5CD3 File Offset: 0x000F3ED3
		IEnumerator<T> IEnumerable<!0>.GetEnumerator()
		{
			return new IListEnumerator<T>(this);
		}

		// Token: 0x06002D70 RID: 11632 RVA: 0x000F5CD3 File Offset: 0x000F3ED3
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new IListEnumerator<T>(this);
		}

		// Token: 0x1700097B RID: 2427
		// (get) Token: 0x06002D71 RID: 11633 RVA: 0x000F5CE0 File Offset: 0x000F3EE0
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.IsReadOnly;
			}
		}

		// Token: 0x1700097C RID: 2428
		// (get) Token: 0x06002D72 RID: 11634 RVA: 0x00002068 File Offset: 0x00000268
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06002D73 RID: 11635 RVA: 0x000F5CE8 File Offset: 0x000F3EE8
		void ICollection.CopyTo(Array array, int index)
		{
			for (int i = 0; i < this.Count; i++)
			{
				array.SetValue(this[i], index);
			}
		}

		// Token: 0x1700097D RID: 2429
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				if (!ListBase<T>.IsCompatibleType(value.GetType()))
				{
					throw new ArgumentException(Res.GetString("Type is incompatible."), "value");
				}
				this[index] = (T)((object)value);
			}
		}

		// Token: 0x06002D76 RID: 11638 RVA: 0x000F5D58 File Offset: 0x000F3F58
		int IList.Add(object value)
		{
			if (!ListBase<T>.IsCompatibleType(value.GetType()))
			{
				throw new ArgumentException(Res.GetString("Type is incompatible."), "value");
			}
			this.Add((T)((object)value));
			return this.Count - 1;
		}

		// Token: 0x06002D77 RID: 11639 RVA: 0x000F5D90 File Offset: 0x000F3F90
		void IList.Clear()
		{
			this.Clear();
		}

		// Token: 0x06002D78 RID: 11640 RVA: 0x000F5D98 File Offset: 0x000F3F98
		bool IList.Contains(object value)
		{
			return ListBase<T>.IsCompatibleType(value.GetType()) && this.Contains((T)((object)value));
		}

		// Token: 0x06002D79 RID: 11641 RVA: 0x000F5DB5 File Offset: 0x000F3FB5
		int IList.IndexOf(object value)
		{
			if (!ListBase<T>.IsCompatibleType(value.GetType()))
			{
				return -1;
			}
			return this.IndexOf((T)((object)value));
		}

		// Token: 0x06002D7A RID: 11642 RVA: 0x000F5DD2 File Offset: 0x000F3FD2
		void IList.Insert(int index, object value)
		{
			if (!ListBase<T>.IsCompatibleType(value.GetType()))
			{
				throw new ArgumentException(Res.GetString("Type is incompatible."), "value");
			}
			this.Insert(index, (T)((object)value));
		}

		// Token: 0x06002D7B RID: 11643 RVA: 0x000F5E03 File Offset: 0x000F4003
		void IList.Remove(object value)
		{
			if (ListBase<T>.IsCompatibleType(value.GetType()))
			{
				this.Remove((T)((object)value));
			}
		}

		// Token: 0x06002D7C RID: 11644 RVA: 0x000F5E1F File Offset: 0x000F401F
		private static bool IsCompatibleType(object value)
		{
			return (value == null && !typeof(T).IsValueType) || value is T;
		}

		// Token: 0x06002D7D RID: 11645 RVA: 0x00002103 File Offset: 0x00000303
		protected ListBase()
		{
		}
	}
}
