﻿using System;
using System.Diagnostics;

namespace System.Xml.Xsl
{
	// Token: 0x0200048F RID: 1167
	[DebuggerDisplay("{Uri} [{StartLine},{StartPos} -- {EndLine},{EndPos}]")]
	internal class SourceLineInfo : ISourceLineInfo
	{
		// Token: 0x06002D95 RID: 11669 RVA: 0x000F6267 File Offset: 0x000F4467
		public SourceLineInfo(string uriString, int startLine, int startPos, int endLine, int endPos) : this(uriString, new Location(startLine, startPos), new Location(endLine, endPos))
		{
		}

		// Token: 0x06002D96 RID: 11670 RVA: 0x000F6280 File Offset: 0x000F4480
		public SourceLineInfo(string uriString, Location start, Location end)
		{
			this.uriString = uriString;
			this.start = start;
			this.end = end;
		}

		// Token: 0x17000987 RID: 2439
		// (get) Token: 0x06002D97 RID: 11671 RVA: 0x000F629D File Offset: 0x000F449D
		public string Uri
		{
			get
			{
				return this.uriString;
			}
		}

		// Token: 0x17000988 RID: 2440
		// (get) Token: 0x06002D98 RID: 11672 RVA: 0x000F62A5 File Offset: 0x000F44A5
		public int StartLine
		{
			get
			{
				return this.start.Line;
			}
		}

		// Token: 0x17000989 RID: 2441
		// (get) Token: 0x06002D99 RID: 11673 RVA: 0x000F62B2 File Offset: 0x000F44B2
		public int StartPos
		{
			get
			{
				return this.start.Pos;
			}
		}

		// Token: 0x1700098A RID: 2442
		// (get) Token: 0x06002D9A RID: 11674 RVA: 0x000F62BF File Offset: 0x000F44BF
		public int EndLine
		{
			get
			{
				return this.end.Line;
			}
		}

		// Token: 0x1700098B RID: 2443
		// (get) Token: 0x06002D9B RID: 11675 RVA: 0x000F62CC File Offset: 0x000F44CC
		public int EndPos
		{
			get
			{
				return this.end.Pos;
			}
		}

		// Token: 0x1700098C RID: 2444
		// (get) Token: 0x06002D9C RID: 11676 RVA: 0x000F62D9 File Offset: 0x000F44D9
		public Location End
		{
			get
			{
				return this.end;
			}
		}

		// Token: 0x1700098D RID: 2445
		// (get) Token: 0x06002D9D RID: 11677 RVA: 0x000F62E1 File Offset: 0x000F44E1
		public Location Start
		{
			get
			{
				return this.start;
			}
		}

		// Token: 0x1700098E RID: 2446
		// (get) Token: 0x06002D9E RID: 11678 RVA: 0x000F62E9 File Offset: 0x000F44E9
		public bool IsNoSource
		{
			get
			{
				return this.StartLine == 16707566;
			}
		}

		// Token: 0x06002D9F RID: 11679 RVA: 0x000F62F8 File Offset: 0x000F44F8
		[Conditional("DEBUG")]
		public static void Validate(ISourceLineInfo lineInfo)
		{
			if (lineInfo.Start.Line != 0)
			{
				int line = lineInfo.Start.Line;
			}
		}

		// Token: 0x06002DA0 RID: 11680 RVA: 0x000F632C File Offset: 0x000F452C
		public static string GetFileName(string uriString)
		{
			Uri uri;
			if (uriString.Length != 0 && System.Uri.TryCreate(uriString, UriKind.Absolute, out uri) && uri.IsFile)
			{
				return uri.LocalPath;
			}
			return uriString;
		}

		// Token: 0x06002DA1 RID: 11681 RVA: 0x000F635C File Offset: 0x000F455C
		// Note: this type is marked as 'beforefieldinit'.
		static SourceLineInfo()
		{
		}

		// Token: 0x04001EFF RID: 7935
		protected string uriString;

		// Token: 0x04001F00 RID: 7936
		protected Location start;

		// Token: 0x04001F01 RID: 7937
		protected Location end;

		// Token: 0x04001F02 RID: 7938
		protected const int NoSourceMagicNumber = 16707566;

		// Token: 0x04001F03 RID: 7939
		public static SourceLineInfo NoSource = new SourceLineInfo(string.Empty, 16707566, 0, 16707566, 0);
	}
}
