﻿using System;
using System.Resources;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Utils;

namespace System.Xml.Xsl
{
	// Token: 0x0200049E RID: 1182
	[Serializable]
	internal class XslTransformException : XsltException
	{
		// Token: 0x06002E7B RID: 11899 RVA: 0x000FBD4F File Offset: 0x000F9F4F
		protected XslTransformException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06002E7C RID: 11900 RVA: 0x000FBD59 File Offset: 0x000F9F59
		public XslTransformException(Exception inner, string res, params string[] args) : base(XslTransformException.CreateMessage(res, args), inner)
		{
		}

		// Token: 0x06002E7D RID: 11901 RVA: 0x000FBD69 File Offset: 0x000F9F69
		public XslTransformException(string message) : base(XslTransformException.CreateMessage(message, null), null)
		{
		}

		// Token: 0x06002E7E RID: 11902 RVA: 0x000FBD79 File Offset: 0x000F9F79
		internal XslTransformException(string res, params string[] args) : this(null, res, args)
		{
		}

		// Token: 0x06002E7F RID: 11903 RVA: 0x000FBD84 File Offset: 0x000F9F84
		internal static string CreateMessage(string res, params string[] args)
		{
			string text = null;
			try
			{
				text = Res.GetString(res, args);
			}
			catch (MissingManifestResourceException)
			{
			}
			if (text != null)
			{
				return text;
			}
			StringBuilder stringBuilder = new StringBuilder(res);
			if (args != null && args.Length != 0)
			{
				stringBuilder.Append('(');
				stringBuilder.Append(args[0]);
				for (int i = 1; i < args.Length; i++)
				{
					stringBuilder.Append(", ");
					stringBuilder.Append(args[i]);
				}
				stringBuilder.Append(')');
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06002E80 RID: 11904 RVA: 0x000FBE0C File Offset: 0x000FA00C
		internal virtual string FormatDetailedMessage()
		{
			return this.Message;
		}

		// Token: 0x06002E81 RID: 11905 RVA: 0x000FBE14 File Offset: 0x000FA014
		public override string ToString()
		{
			string text = base.GetType().FullName;
			string text2 = this.FormatDetailedMessage();
			if (text2 != null && text2.Length > 0)
			{
				text = text + ": " + text2;
			}
			if (base.InnerException != null)
			{
				text = string.Concat(new string[]
				{
					text,
					" ---> ",
					base.InnerException.ToString(),
					Environment.NewLine,
					"   ",
					XslTransformException.CreateMessage("--- End of inner exception stack trace ---", Array.Empty<string>())
				});
			}
			if (this.StackTrace != null)
			{
				text = text + Environment.NewLine + this.StackTrace;
			}
			return text;
		}
	}
}
