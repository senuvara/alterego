﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000549 RID: 1353
	internal struct XmlNavigatorStack
	{
		// Token: 0x06003404 RID: 13316 RVA: 0x0010F0A0 File Offset: 0x0010D2A0
		public void Push(XPathNavigator nav)
		{
			if (this.stkNav == null)
			{
				this.stkNav = new XPathNavigator[8];
			}
			else if (this.sp >= this.stkNav.Length)
			{
				Array sourceArray = this.stkNav;
				this.stkNav = new XPathNavigator[2 * this.sp];
				Array.Copy(sourceArray, this.stkNav, this.sp);
			}
			XPathNavigator[] array = this.stkNav;
			int num = this.sp;
			this.sp = num + 1;
			array[num] = nav;
		}

		// Token: 0x06003405 RID: 13317 RVA: 0x0010F118 File Offset: 0x0010D318
		public XPathNavigator Pop()
		{
			XPathNavigator[] array = this.stkNav;
			int num = this.sp - 1;
			this.sp = num;
			return array[num];
		}

		// Token: 0x06003406 RID: 13318 RVA: 0x0010F13D File Offset: 0x0010D33D
		public XPathNavigator Peek()
		{
			return this.stkNav[this.sp - 1];
		}

		// Token: 0x06003407 RID: 13319 RVA: 0x0010F14E File Offset: 0x0010D34E
		public void Reset()
		{
			this.sp = 0;
		}

		// Token: 0x17000AE7 RID: 2791
		// (get) Token: 0x06003408 RID: 13320 RVA: 0x0010F157 File Offset: 0x0010D357
		public bool IsEmpty
		{
			get
			{
				return this.sp == 0;
			}
		}

		// Token: 0x0400233B RID: 9019
		private XPathNavigator[] stkNav;

		// Token: 0x0400233C RID: 9020
		private int sp;

		// Token: 0x0400233D RID: 9021
		private const int InitialStackSize = 8;
	}
}
