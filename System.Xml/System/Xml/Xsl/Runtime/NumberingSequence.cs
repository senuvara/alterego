﻿using System;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000540 RID: 1344
	internal enum NumberingSequence
	{
		// Token: 0x040022EE RID: 8942
		Nil = -1,
		// Token: 0x040022EF RID: 8943
		FirstDecimal,
		// Token: 0x040022F0 RID: 8944
		Arabic = 0,
		// Token: 0x040022F1 RID: 8945
		DArabic,
		// Token: 0x040022F2 RID: 8946
		Hindi3,
		// Token: 0x040022F3 RID: 8947
		Thai2,
		// Token: 0x040022F4 RID: 8948
		FEDecimal,
		// Token: 0x040022F5 RID: 8949
		KorDbNum1,
		// Token: 0x040022F6 RID: 8950
		LastNum = 5,
		// Token: 0x040022F7 RID: 8951
		FirstAlpha,
		// Token: 0x040022F8 RID: 8952
		UCLetter = 6,
		// Token: 0x040022F9 RID: 8953
		LCLetter,
		// Token: 0x040022FA RID: 8954
		UCRus,
		// Token: 0x040022FB RID: 8955
		LCRus,
		// Token: 0x040022FC RID: 8956
		Thai1,
		// Token: 0x040022FD RID: 8957
		Hindi1,
		// Token: 0x040022FE RID: 8958
		Hindi2,
		// Token: 0x040022FF RID: 8959
		Aiueo,
		// Token: 0x04002300 RID: 8960
		DAiueo,
		// Token: 0x04002301 RID: 8961
		Iroha,
		// Token: 0x04002302 RID: 8962
		DIroha,
		// Token: 0x04002303 RID: 8963
		DChosung,
		// Token: 0x04002304 RID: 8964
		Ganada,
		// Token: 0x04002305 RID: 8965
		ArabicScript,
		// Token: 0x04002306 RID: 8966
		LastAlpha = 19,
		// Token: 0x04002307 RID: 8967
		FirstSpecial,
		// Token: 0x04002308 RID: 8968
		UCRoman = 20,
		// Token: 0x04002309 RID: 8969
		LCRoman,
		// Token: 0x0400230A RID: 8970
		Hebrew,
		// Token: 0x0400230B RID: 8971
		DbNum3,
		// Token: 0x0400230C RID: 8972
		ChnCmplx,
		// Token: 0x0400230D RID: 8973
		KorDbNum3,
		// Token: 0x0400230E RID: 8974
		Zodiac1,
		// Token: 0x0400230F RID: 8975
		Zodiac2,
		// Token: 0x04002310 RID: 8976
		Zodiac3,
		// Token: 0x04002311 RID: 8977
		LastSpecial = 28
	}
}
