﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000542 RID: 1346
	internal abstract class RtfNavigator : XPathNavigator
	{
		// Token: 0x060033B5 RID: 13237
		public abstract void CopyToWriter(XmlWriter writer);

		// Token: 0x060033B6 RID: 13238
		public abstract XPathNavigator ToNavigator();

		// Token: 0x17000AD2 RID: 2770
		// (get) Token: 0x060033B7 RID: 13239 RVA: 0x000020CD File Offset: 0x000002CD
		public override XPathNodeType NodeType
		{
			get
			{
				return XPathNodeType.Root;
			}
		}

		// Token: 0x17000AD3 RID: 2771
		// (get) Token: 0x060033B8 RID: 13240 RVA: 0x00003201 File Offset: 0x00001401
		public override string LocalName
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000AD4 RID: 2772
		// (get) Token: 0x060033B9 RID: 13241 RVA: 0x00003201 File Offset: 0x00001401
		public override string NamespaceURI
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000AD5 RID: 2773
		// (get) Token: 0x060033BA RID: 13242 RVA: 0x00003201 File Offset: 0x00001401
		public override string Name
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000AD6 RID: 2774
		// (get) Token: 0x060033BB RID: 13243 RVA: 0x00003201 File Offset: 0x00001401
		public override string Prefix
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000AD7 RID: 2775
		// (get) Token: 0x060033BC RID: 13244 RVA: 0x000020CD File Offset: 0x000002CD
		public override bool IsEmptyElement
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000AD8 RID: 2776
		// (get) Token: 0x060033BD RID: 13245 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override XmlNameTable NameTable
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x060033BE RID: 13246 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToFirstAttribute()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033BF RID: 13247 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToNextAttribute()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C0 RID: 13248 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToFirstNamespace(XPathNamespaceScope namespaceScope)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C1 RID: 13249 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToNextNamespace(XPathNamespaceScope namespaceScope)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C2 RID: 13250 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToNext()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C3 RID: 13251 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToPrevious()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C4 RID: 13252 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToFirstChild()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C5 RID: 13253 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToParent()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C6 RID: 13254 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool MoveToId(string id)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C7 RID: 13255 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public override bool IsSamePosition(XPathNavigator other)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060033C8 RID: 13256 RVA: 0x000938B1 File Offset: 0x00091AB1
		protected RtfNavigator()
		{
		}
	}
}
