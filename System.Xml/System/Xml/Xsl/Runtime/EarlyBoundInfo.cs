﻿using System;
using System.Reflection;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x0200053E RID: 1342
	internal sealed class EarlyBoundInfo
	{
		// Token: 0x060033A9 RID: 13225 RVA: 0x0010E70A File Offset: 0x0010C90A
		public EarlyBoundInfo(string namespaceUri, Type ebType)
		{
			this.namespaceUri = namespaceUri;
			this.constrInfo = ebType.GetConstructor(Type.EmptyTypes);
		}

		// Token: 0x17000AD0 RID: 2768
		// (get) Token: 0x060033AA RID: 13226 RVA: 0x0010E72A File Offset: 0x0010C92A
		public string NamespaceUri
		{
			get
			{
				return this.namespaceUri;
			}
		}

		// Token: 0x17000AD1 RID: 2769
		// (get) Token: 0x060033AB RID: 13227 RVA: 0x0010E732 File Offset: 0x0010C932
		public Type EarlyBoundType
		{
			get
			{
				return this.constrInfo.DeclaringType;
			}
		}

		// Token: 0x060033AC RID: 13228 RVA: 0x0010E73F File Offset: 0x0010C93F
		public object CreateObject()
		{
			return this.constrInfo.Invoke(new object[0]);
		}

		// Token: 0x060033AD RID: 13229 RVA: 0x0010E754 File Offset: 0x0010C954
		public override bool Equals(object obj)
		{
			EarlyBoundInfo earlyBoundInfo = obj as EarlyBoundInfo;
			return earlyBoundInfo != null && this.namespaceUri == earlyBoundInfo.namespaceUri && this.constrInfo == earlyBoundInfo.constrInfo;
		}

		// Token: 0x060033AE RID: 13230 RVA: 0x0010E793 File Offset: 0x0010C993
		public override int GetHashCode()
		{
			return this.namespaceUri.GetHashCode();
		}

		// Token: 0x040022EB RID: 8939
		private string namespaceUri;

		// Token: 0x040022EC RID: 8940
		private ConstructorInfo constrInfo;
	}
}
