﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000544 RID: 1348
	internal sealed class RtfTextNavigator : RtfNavigator
	{
		// Token: 0x060033D1 RID: 13265 RVA: 0x0010E982 File Offset: 0x0010CB82
		public RtfTextNavigator(string text, string baseUri)
		{
			this.text = text;
			this.baseUri = baseUri;
			this.constr = new NavigatorConstructor();
		}

		// Token: 0x060033D2 RID: 13266 RVA: 0x0010E9A3 File Offset: 0x0010CBA3
		public RtfTextNavigator(RtfTextNavigator that)
		{
			this.text = that.text;
			this.baseUri = that.baseUri;
			this.constr = that.constr;
		}

		// Token: 0x060033D3 RID: 13267 RVA: 0x0010E9CF File Offset: 0x0010CBCF
		public override void CopyToWriter(XmlWriter writer)
		{
			writer.WriteString(this.Value);
		}

		// Token: 0x060033D4 RID: 13268 RVA: 0x0010E9DD File Offset: 0x0010CBDD
		public override XPathNavigator ToNavigator()
		{
			return this.constr.GetNavigator(this.text, this.baseUri, new NameTable());
		}

		// Token: 0x17000ADB RID: 2779
		// (get) Token: 0x060033D5 RID: 13269 RVA: 0x0010E9FB File Offset: 0x0010CBFB
		public override string Value
		{
			get
			{
				return this.text;
			}
		}

		// Token: 0x17000ADC RID: 2780
		// (get) Token: 0x060033D6 RID: 13270 RVA: 0x0010EA03 File Offset: 0x0010CC03
		public override string BaseURI
		{
			get
			{
				return this.baseUri;
			}
		}

		// Token: 0x060033D7 RID: 13271 RVA: 0x0010EA0B File Offset: 0x0010CC0B
		public override XPathNavigator Clone()
		{
			return new RtfTextNavigator(this);
		}

		// Token: 0x060033D8 RID: 13272 RVA: 0x0010EA14 File Offset: 0x0010CC14
		public override bool MoveTo(XPathNavigator other)
		{
			RtfTextNavigator rtfTextNavigator = other as RtfTextNavigator;
			if (rtfTextNavigator != null)
			{
				this.text = rtfTextNavigator.text;
				this.baseUri = rtfTextNavigator.baseUri;
				this.constr = rtfTextNavigator.constr;
				return true;
			}
			return false;
		}

		// Token: 0x04002322 RID: 8994
		private string text;

		// Token: 0x04002323 RID: 8995
		private string baseUri;

		// Token: 0x04002324 RID: 8996
		private NavigatorConstructor constr;
	}
}
