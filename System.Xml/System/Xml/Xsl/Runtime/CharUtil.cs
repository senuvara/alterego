﻿using System;
using System.Globalization;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x0200053F RID: 1343
	internal static class CharUtil
	{
		// Token: 0x060033AF RID: 13231 RVA: 0x0010E7A0 File Offset: 0x0010C9A0
		public static bool IsAlphaNumeric(char ch)
		{
			int unicodeCategory = (int)char.GetUnicodeCategory(ch);
			return unicodeCategory <= 4 || (unicodeCategory <= 10 && unicodeCategory >= 8);
		}

		// Token: 0x060033B0 RID: 13232 RVA: 0x0010E7C8 File Offset: 0x0010C9C8
		public static bool IsDecimalDigitOne(char ch)
		{
			return char.GetUnicodeCategory(ch -= '\u0001') == UnicodeCategory.DecimalDigitNumber && char.GetNumericValue(ch) == 0.0;
		}
	}
}
