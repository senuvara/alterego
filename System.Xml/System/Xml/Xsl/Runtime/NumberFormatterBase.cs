﻿using System;
using System.Text;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000541 RID: 1345
	internal class NumberFormatterBase
	{
		// Token: 0x060033B1 RID: 13233 RVA: 0x0010E7EC File Offset: 0x0010C9EC
		public static void ConvertToAlphabetic(StringBuilder sb, double val, char firstChar, int totalChars)
		{
			char[] array = new char[7];
			int num = 7;
			int i;
			int num2;
			for (i = (int)val; i > totalChars; i = num2)
			{
				num2 = --i / totalChars;
				array[--num] = (char)((int)firstChar + (i - num2 * totalChars));
			}
			array[--num] = (char)((int)firstChar + (i - 1));
			sb.Append(array, num, 7 - num);
		}

		// Token: 0x060033B2 RID: 13234 RVA: 0x0010E840 File Offset: 0x0010CA40
		public static void ConvertToRoman(StringBuilder sb, double val, bool upperCase)
		{
			int i = (int)val;
			string value = upperCase ? "IIVIXXLXCCDCM" : "iivixxlxccdcm";
			int num = NumberFormatterBase.RomanDigitValue.Length;
			while (num-- != 0)
			{
				while (i >= NumberFormatterBase.RomanDigitValue[num])
				{
					i -= NumberFormatterBase.RomanDigitValue[num];
					sb.Append(value, num, 1 + (num & 1));
				}
			}
		}

		// Token: 0x060033B3 RID: 13235 RVA: 0x00002103 File Offset: 0x00000303
		public NumberFormatterBase()
		{
		}

		// Token: 0x060033B4 RID: 13236 RVA: 0x0010E893 File Offset: 0x0010CA93
		// Note: this type is marked as 'beforefieldinit'.
		static NumberFormatterBase()
		{
		}

		// Token: 0x04002312 RID: 8978
		protected const int MaxAlphabeticValue = 2147483647;

		// Token: 0x04002313 RID: 8979
		private const int MaxAlphabeticLength = 7;

		// Token: 0x04002314 RID: 8980
		protected const int MaxRomanValue = 32767;

		// Token: 0x04002315 RID: 8981
		private const string RomanDigitsUC = "IIVIXXLXCCDCM";

		// Token: 0x04002316 RID: 8982
		private const string RomanDigitsLC = "iivixxlxccdcm";

		// Token: 0x04002317 RID: 8983
		private static readonly int[] RomanDigitValue = new int[]
		{
			1,
			4,
			5,
			9,
			10,
			40,
			50,
			90,
			100,
			400,
			500,
			900,
			1000
		};

		// Token: 0x04002318 RID: 8984
		private const string hiraganaAiueo = "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん";

		// Token: 0x04002319 RID: 8985
		private const string hiraganaIroha = "いろはにほへとちりぬるをわかよたれそつねならむうゐのおくやまけふこえてあさきゆめみしゑひもせす";

		// Token: 0x0400231A RID: 8986
		private const string katakanaAiueo = "アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲン";

		// Token: 0x0400231B RID: 8987
		private const string katakanaIroha = "イロハニホヘトチリヌルヲワカヨタレソツネナラムウヰノオクヤマケフコエテアサキユメミシヱヒモセスン";

		// Token: 0x0400231C RID: 8988
		private const string katakanaAiueoHw = "ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝ";

		// Token: 0x0400231D RID: 8989
		private const string katakanaIrohaHw = "ｲﾛﾊﾆﾎﾍﾄﾁﾘﾇﾙｦﾜｶﾖﾀﾚｿﾂﾈﾅﾗﾑｳヰﾉｵｸﾔﾏｹﾌｺｴﾃｱｻｷﾕﾒﾐｼヱﾋﾓｾｽﾝ";

		// Token: 0x0400231E RID: 8990
		private const string cjkIdeographic = "〇一二三四五六七八九";
	}
}
