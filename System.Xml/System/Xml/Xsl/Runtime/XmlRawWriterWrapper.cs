﻿using System;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x0200054A RID: 1354
	internal sealed class XmlRawWriterWrapper : XmlRawWriter
	{
		// Token: 0x06003409 RID: 13321 RVA: 0x0010F162 File Offset: 0x0010D362
		public XmlRawWriterWrapper(XmlWriter writer)
		{
			this.wrapped = writer;
		}

		// Token: 0x17000AE8 RID: 2792
		// (get) Token: 0x0600340A RID: 13322 RVA: 0x0010F171 File Offset: 0x0010D371
		public override XmlWriterSettings Settings
		{
			get
			{
				return this.wrapped.Settings;
			}
		}

		// Token: 0x0600340B RID: 13323 RVA: 0x0010F17E File Offset: 0x0010D37E
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			this.wrapped.WriteDocType(name, pubid, sysid, subset);
		}

		// Token: 0x0600340C RID: 13324 RVA: 0x0010F190 File Offset: 0x0010D390
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			this.wrapped.WriteStartElement(prefix, localName, ns);
		}

		// Token: 0x0600340D RID: 13325 RVA: 0x0010F1A0 File Offset: 0x0010D3A0
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			this.wrapped.WriteStartAttribute(prefix, localName, ns);
		}

		// Token: 0x0600340E RID: 13326 RVA: 0x0010F1B0 File Offset: 0x0010D3B0
		public override void WriteEndAttribute()
		{
			this.wrapped.WriteEndAttribute();
		}

		// Token: 0x0600340F RID: 13327 RVA: 0x0010F1BD File Offset: 0x0010D3BD
		public override void WriteCData(string text)
		{
			this.wrapped.WriteCData(text);
		}

		// Token: 0x06003410 RID: 13328 RVA: 0x0010F1CB File Offset: 0x0010D3CB
		public override void WriteComment(string text)
		{
			this.wrapped.WriteComment(text);
		}

		// Token: 0x06003411 RID: 13329 RVA: 0x0010F1D9 File Offset: 0x0010D3D9
		public override void WriteProcessingInstruction(string name, string text)
		{
			this.wrapped.WriteProcessingInstruction(name, text);
		}

		// Token: 0x06003412 RID: 13330 RVA: 0x0010F1E8 File Offset: 0x0010D3E8
		public override void WriteWhitespace(string ws)
		{
			this.wrapped.WriteWhitespace(ws);
		}

		// Token: 0x06003413 RID: 13331 RVA: 0x0010F1F6 File Offset: 0x0010D3F6
		public override void WriteString(string text)
		{
			this.wrapped.WriteString(text);
		}

		// Token: 0x06003414 RID: 13332 RVA: 0x0010F204 File Offset: 0x0010D404
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.wrapped.WriteChars(buffer, index, count);
		}

		// Token: 0x06003415 RID: 13333 RVA: 0x0010F214 File Offset: 0x0010D414
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.wrapped.WriteRaw(buffer, index, count);
		}

		// Token: 0x06003416 RID: 13334 RVA: 0x0010F224 File Offset: 0x0010D424
		public override void WriteRaw(string data)
		{
			this.wrapped.WriteRaw(data);
		}

		// Token: 0x06003417 RID: 13335 RVA: 0x0010F232 File Offset: 0x0010D432
		public override void WriteEntityRef(string name)
		{
			this.wrapped.WriteEntityRef(name);
		}

		// Token: 0x06003418 RID: 13336 RVA: 0x0010F240 File Offset: 0x0010D440
		public override void WriteCharEntity(char ch)
		{
			this.wrapped.WriteCharEntity(ch);
		}

		// Token: 0x06003419 RID: 13337 RVA: 0x0010F24E File Offset: 0x0010D44E
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			this.wrapped.WriteSurrogateCharEntity(lowChar, highChar);
		}

		// Token: 0x0600341A RID: 13338 RVA: 0x0010F25D File Offset: 0x0010D45D
		public override void Close()
		{
			this.wrapped.Close();
		}

		// Token: 0x0600341B RID: 13339 RVA: 0x0010F26A File Offset: 0x0010D46A
		public override void Flush()
		{
			this.wrapped.Flush();
		}

		// Token: 0x0600341C RID: 13340 RVA: 0x0010F277 File Offset: 0x0010D477
		public override void WriteValue(object value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x0600341D RID: 13341 RVA: 0x0010F285 File Offset: 0x0010D485
		public override void WriteValue(string value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x0600341E RID: 13342 RVA: 0x0010F293 File Offset: 0x0010D493
		public override void WriteValue(bool value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x0600341F RID: 13343 RVA: 0x0010F2A1 File Offset: 0x0010D4A1
		public override void WriteValue(DateTime value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x06003420 RID: 13344 RVA: 0x0010F2AF File Offset: 0x0010D4AF
		public override void WriteValue(float value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x06003421 RID: 13345 RVA: 0x0010F2BD File Offset: 0x0010D4BD
		public override void WriteValue(decimal value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x06003422 RID: 13346 RVA: 0x0010F2CB File Offset: 0x0010D4CB
		public override void WriteValue(double value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x06003423 RID: 13347 RVA: 0x0010F2D9 File Offset: 0x0010D4D9
		public override void WriteValue(int value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x06003424 RID: 13348 RVA: 0x0010F2E7 File Offset: 0x0010D4E7
		public override void WriteValue(long value)
		{
			this.wrapped.WriteValue(value);
		}

		// Token: 0x06003425 RID: 13349 RVA: 0x0010F2F8 File Offset: 0x0010D4F8
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					((IDisposable)this.wrapped).Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x06003426 RID: 13350 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void WriteXmlDeclaration(XmlStandalone standalone)
		{
		}

		// Token: 0x06003427 RID: 13351 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void WriteXmlDeclaration(string xmldecl)
		{
		}

		// Token: 0x06003428 RID: 13352 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void StartElementContent()
		{
		}

		// Token: 0x06003429 RID: 13353 RVA: 0x0010F330 File Offset: 0x0010D530
		internal override void WriteEndElement(string prefix, string localName, string ns)
		{
			this.wrapped.WriteEndElement();
		}

		// Token: 0x0600342A RID: 13354 RVA: 0x0010F33D File Offset: 0x0010D53D
		internal override void WriteFullEndElement(string prefix, string localName, string ns)
		{
			this.wrapped.WriteFullEndElement();
		}

		// Token: 0x0600342B RID: 13355 RVA: 0x0010F34A File Offset: 0x0010D54A
		internal override void WriteNamespaceDeclaration(string prefix, string ns)
		{
			if (prefix.Length == 0)
			{
				this.wrapped.WriteAttributeString(string.Empty, "xmlns", "http://www.w3.org/2000/xmlns/", ns);
				return;
			}
			this.wrapped.WriteAttributeString("xmlns", prefix, "http://www.w3.org/2000/xmlns/", ns);
		}

		// Token: 0x0400233E RID: 9022
		private XmlWriter wrapped;
	}
}
