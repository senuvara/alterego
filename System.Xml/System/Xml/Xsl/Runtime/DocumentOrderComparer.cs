﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x0200053D RID: 1341
	internal class DocumentOrderComparer : IComparer<XPathNavigator>
	{
		// Token: 0x060033A6 RID: 13222 RVA: 0x0010E640 File Offset: 0x0010C840
		public int Compare(XPathNavigator navThis, XPathNavigator navThat)
		{
			switch (navThis.ComparePosition(navThat))
			{
			case XmlNodeOrder.Before:
				return -1;
			case XmlNodeOrder.After:
				return 1;
			case XmlNodeOrder.Same:
				return 0;
			default:
				if (this.roots == null)
				{
					this.roots = new List<XPathNavigator>();
				}
				if (this.GetDocumentIndex(navThis) >= this.GetDocumentIndex(navThat))
				{
					return 1;
				}
				return -1;
			}
		}

		// Token: 0x060033A7 RID: 13223 RVA: 0x0010E698 File Offset: 0x0010C898
		public int GetDocumentIndex(XPathNavigator nav)
		{
			if (this.roots == null)
			{
				this.roots = new List<XPathNavigator>();
			}
			XPathNavigator xpathNavigator = nav.Clone();
			xpathNavigator.MoveToRoot();
			for (int i = 0; i < this.roots.Count; i++)
			{
				if (xpathNavigator.IsSamePosition(this.roots[i]))
				{
					return i;
				}
			}
			this.roots.Add(xpathNavigator);
			return this.roots.Count - 1;
		}

		// Token: 0x060033A8 RID: 13224 RVA: 0x00002103 File Offset: 0x00000303
		public DocumentOrderComparer()
		{
		}

		// Token: 0x040022EA RID: 8938
		private List<XPathNavigator> roots;
	}
}
