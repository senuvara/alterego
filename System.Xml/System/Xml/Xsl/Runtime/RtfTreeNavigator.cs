﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000543 RID: 1347
	internal sealed class RtfTreeNavigator : RtfNavigator
	{
		// Token: 0x060033C9 RID: 13257 RVA: 0x0010E8AC File Offset: 0x0010CAAC
		public RtfTreeNavigator(XmlEventCache events, XmlNameTable nameTable)
		{
			this.events = events;
			this.constr = new NavigatorConstructor();
			this.nameTable = nameTable;
		}

		// Token: 0x060033CA RID: 13258 RVA: 0x0010E8CD File Offset: 0x0010CACD
		public RtfTreeNavigator(RtfTreeNavigator that)
		{
			this.events = that.events;
			this.constr = that.constr;
			this.nameTable = that.nameTable;
		}

		// Token: 0x060033CB RID: 13259 RVA: 0x0010E8F9 File Offset: 0x0010CAF9
		public override void CopyToWriter(XmlWriter writer)
		{
			this.events.EventsToWriter(writer);
		}

		// Token: 0x060033CC RID: 13260 RVA: 0x0010E907 File Offset: 0x0010CB07
		public override XPathNavigator ToNavigator()
		{
			return this.constr.GetNavigator(this.events, this.nameTable);
		}

		// Token: 0x17000AD9 RID: 2777
		// (get) Token: 0x060033CD RID: 13261 RVA: 0x0010E920 File Offset: 0x0010CB20
		public override string Value
		{
			get
			{
				return this.events.EventsToString();
			}
		}

		// Token: 0x17000ADA RID: 2778
		// (get) Token: 0x060033CE RID: 13262 RVA: 0x0010E92D File Offset: 0x0010CB2D
		public override string BaseURI
		{
			get
			{
				return this.events.BaseUri;
			}
		}

		// Token: 0x060033CF RID: 13263 RVA: 0x0010E93A File Offset: 0x0010CB3A
		public override XPathNavigator Clone()
		{
			return new RtfTreeNavigator(this);
		}

		// Token: 0x060033D0 RID: 13264 RVA: 0x0010E944 File Offset: 0x0010CB44
		public override bool MoveTo(XPathNavigator other)
		{
			RtfTreeNavigator rtfTreeNavigator = other as RtfTreeNavigator;
			if (rtfTreeNavigator != null)
			{
				this.events = rtfTreeNavigator.events;
				this.constr = rtfTreeNavigator.constr;
				this.nameTable = rtfTreeNavigator.nameTable;
				return true;
			}
			return false;
		}

		// Token: 0x0400231F RID: 8991
		private XmlEventCache events;

		// Token: 0x04002320 RID: 8992
		private NavigatorConstructor constr;

		// Token: 0x04002321 RID: 8993
		private XmlNameTable nameTable;
	}
}
