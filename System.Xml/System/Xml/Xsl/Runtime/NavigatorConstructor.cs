﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000545 RID: 1349
	internal sealed class NavigatorConstructor
	{
		// Token: 0x060033D9 RID: 13273 RVA: 0x0010EA54 File Offset: 0x0010CC54
		public XPathNavigator GetNavigator(XmlEventCache events, XmlNameTable nameTable)
		{
			if (this.cache == null)
			{
				XPathDocument xpathDocument = new XPathDocument(nameTable);
				XmlRawWriter xmlRawWriter = xpathDocument.LoadFromWriter(XPathDocument.LoadFlags.AtomizeNames | (events.HasRootNode ? XPathDocument.LoadFlags.None : XPathDocument.LoadFlags.Fragment), events.BaseUri);
				events.EventsToWriter(xmlRawWriter);
				xmlRawWriter.Close();
				this.cache = xpathDocument;
			}
			return ((XPathDocument)this.cache).CreateNavigator();
		}

		// Token: 0x060033DA RID: 13274 RVA: 0x0010EAB0 File Offset: 0x0010CCB0
		public XPathNavigator GetNavigator(string text, string baseUri, XmlNameTable nameTable)
		{
			if (this.cache == null)
			{
				XPathDocument xpathDocument = new XPathDocument(nameTable);
				XmlRawWriter xmlRawWriter = xpathDocument.LoadFromWriter(XPathDocument.LoadFlags.AtomizeNames, baseUri);
				xmlRawWriter.WriteString(text);
				xmlRawWriter.Close();
				this.cache = xpathDocument;
			}
			return ((XPathDocument)this.cache).CreateNavigator();
		}

		// Token: 0x060033DB RID: 13275 RVA: 0x00002103 File Offset: 0x00000303
		public NavigatorConstructor()
		{
		}

		// Token: 0x04002325 RID: 8997
		private object cache;
	}
}
