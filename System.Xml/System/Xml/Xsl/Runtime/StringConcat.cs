﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000546 RID: 1350
	[EditorBrowsable(EditorBrowsableState.Never)]
	public struct StringConcat
	{
		// Token: 0x060033DC RID: 13276 RVA: 0x0010EAF7 File Offset: 0x0010CCF7
		public void Clear()
		{
			this.idxStr = 0;
			this.delimiter = null;
		}

		// Token: 0x17000ADD RID: 2781
		// (get) Token: 0x060033DD RID: 13277 RVA: 0x0010EB07 File Offset: 0x0010CD07
		// (set) Token: 0x060033DE RID: 13278 RVA: 0x0010EB0F File Offset: 0x0010CD0F
		public string Delimiter
		{
			get
			{
				return this.delimiter;
			}
			set
			{
				this.delimiter = value;
			}
		}

		// Token: 0x17000ADE RID: 2782
		// (get) Token: 0x060033DF RID: 13279 RVA: 0x0010EB18 File Offset: 0x0010CD18
		internal int Count
		{
			get
			{
				return this.idxStr;
			}
		}

		// Token: 0x060033E0 RID: 13280 RVA: 0x0010EB20 File Offset: 0x0010CD20
		public void Concat(string value)
		{
			if (this.delimiter != null && this.idxStr != 0)
			{
				this.ConcatNoDelimiter(this.delimiter);
			}
			this.ConcatNoDelimiter(value);
		}

		// Token: 0x060033E1 RID: 13281 RVA: 0x0010EB48 File Offset: 0x0010CD48
		public string GetResult()
		{
			switch (this.idxStr)
			{
			case 0:
				return string.Empty;
			case 1:
				return this.s1;
			case 2:
				return this.s1 + this.s2;
			case 3:
				return this.s1 + this.s2 + this.s3;
			case 4:
				return this.s1 + this.s2 + this.s3 + this.s4;
			default:
				return string.Concat(this.strList.ToArray());
			}
		}

		// Token: 0x060033E2 RID: 13282 RVA: 0x0010EBE0 File Offset: 0x0010CDE0
		internal void ConcatNoDelimiter(string s)
		{
			switch (this.idxStr)
			{
			case 0:
				this.s1 = s;
				goto IL_A8;
			case 1:
				this.s2 = s;
				goto IL_A8;
			case 2:
				this.s3 = s;
				goto IL_A8;
			case 3:
				this.s4 = s;
				goto IL_A8;
			case 4:
			{
				int capacity = (this.strList == null) ? 8 : this.strList.Count;
				List<string> list = this.strList = new List<string>(capacity);
				list.Add(this.s1);
				list.Add(this.s2);
				list.Add(this.s3);
				list.Add(this.s4);
				break;
			}
			}
			this.strList.Add(s);
			IL_A8:
			this.idxStr++;
		}

		// Token: 0x04002326 RID: 8998
		private string s1;

		// Token: 0x04002327 RID: 8999
		private string s2;

		// Token: 0x04002328 RID: 9000
		private string s3;

		// Token: 0x04002329 RID: 9001
		private string s4;

		// Token: 0x0400232A RID: 9002
		private string delimiter;

		// Token: 0x0400232B RID: 9003
		private List<string> strList;

		// Token: 0x0400232C RID: 9004
		private int idxStr;
	}
}
