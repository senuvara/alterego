﻿using System;
using System.Globalization;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x0200053B RID: 1339
	internal class DecimalFormat
	{
		// Token: 0x060033A1 RID: 13217 RVA: 0x0010E164 File Offset: 0x0010C364
		internal DecimalFormat(NumberFormatInfo info, char digit, char zeroDigit, char patternSeparator)
		{
			this.info = info;
			this.digit = digit;
			this.zeroDigit = zeroDigit;
			this.patternSeparator = patternSeparator;
		}

		// Token: 0x040022DF RID: 8927
		public NumberFormatInfo info;

		// Token: 0x040022E0 RID: 8928
		public char digit;

		// Token: 0x040022E1 RID: 8929
		public char zeroDigit;

		// Token: 0x040022E2 RID: 8930
		public char patternSeparator;
	}
}
