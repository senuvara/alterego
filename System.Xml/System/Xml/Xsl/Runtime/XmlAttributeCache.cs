﻿using System;
using System.Xml.Schema;

namespace System.Xml.Xsl.Runtime
{
	// Token: 0x02000547 RID: 1351
	internal sealed class XmlAttributeCache : XmlRawWriter, IRemovableWriter
	{
		// Token: 0x060033E3 RID: 13283 RVA: 0x0010ECA3 File Offset: 0x0010CEA3
		public void Init(XmlRawWriter wrapped)
		{
			this.SetWrappedWriter(wrapped);
			this.numEntries = 0;
			this.idxLastName = 0;
			this.hashCodeUnion = 0;
		}

		// Token: 0x17000ADF RID: 2783
		// (get) Token: 0x060033E4 RID: 13284 RVA: 0x0010ECC1 File Offset: 0x0010CEC1
		public int Count
		{
			get
			{
				return this.numEntries;
			}
		}

		// Token: 0x17000AE0 RID: 2784
		// (get) Token: 0x060033E5 RID: 13285 RVA: 0x0010ECC9 File Offset: 0x0010CEC9
		// (set) Token: 0x060033E6 RID: 13286 RVA: 0x0010ECD1 File Offset: 0x0010CED1
		public OnRemoveWriter OnRemoveWriterEvent
		{
			get
			{
				return this.onRemove;
			}
			set
			{
				this.onRemove = value;
			}
		}

		// Token: 0x060033E7 RID: 13287 RVA: 0x0010ECDC File Offset: 0x0010CEDC
		private void SetWrappedWriter(XmlRawWriter writer)
		{
			IRemovableWriter removableWriter = writer as IRemovableWriter;
			if (removableWriter != null)
			{
				removableWriter.OnRemoveWriterEvent = new OnRemoveWriter(this.SetWrappedWriter);
			}
			this.wrapped = writer;
		}

		// Token: 0x060033E8 RID: 13288 RVA: 0x0010ED0C File Offset: 0x0010CF0C
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			int num = 0;
			int num2 = 1 << (int)localName[0];
			if ((this.hashCodeUnion & num2) != 0)
			{
				while (!this.arrAttrs[num].IsDuplicate(localName, ns, num2))
				{
					num = this.arrAttrs[num].NextNameIndex;
					if (num == 0)
					{
						break;
					}
				}
			}
			else
			{
				this.hashCodeUnion |= num2;
			}
			this.EnsureAttributeCache();
			if (this.numEntries != 0)
			{
				this.arrAttrs[this.idxLastName].NextNameIndex = this.numEntries;
			}
			int num3 = this.numEntries;
			this.numEntries = num3 + 1;
			this.idxLastName = num3;
			this.arrAttrs[this.idxLastName].Init(prefix, localName, ns, num2);
		}

		// Token: 0x060033E9 RID: 13289 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteEndAttribute()
		{
		}

		// Token: 0x060033EA RID: 13290 RVA: 0x0010EDCB File Offset: 0x0010CFCB
		internal override void WriteNamespaceDeclaration(string prefix, string ns)
		{
			this.FlushAttributes();
			this.wrapped.WriteNamespaceDeclaration(prefix, ns);
		}

		// Token: 0x060033EB RID: 13291 RVA: 0x0010EDE0 File Offset: 0x0010CFE0
		public override void WriteString(string text)
		{
			this.EnsureAttributeCache();
			XmlAttributeCache.AttrNameVal[] array = this.arrAttrs;
			int num = this.numEntries;
			this.numEntries = num + 1;
			array[num].Init(text);
		}

		// Token: 0x060033EC RID: 13292 RVA: 0x0010EE18 File Offset: 0x0010D018
		public override void WriteValue(object value)
		{
			this.EnsureAttributeCache();
			XmlAttributeCache.AttrNameVal[] array = this.arrAttrs;
			int num = this.numEntries;
			this.numEntries = num + 1;
			array[num].Init((XmlAtomicValue)value);
		}

		// Token: 0x060033ED RID: 13293 RVA: 0x0010EE52 File Offset: 0x0010D052
		public override void WriteValue(string value)
		{
			this.WriteValue(value);
		}

		// Token: 0x060033EE RID: 13294 RVA: 0x0010EE5B File Offset: 0x0010D05B
		internal override void StartElementContent()
		{
			this.FlushAttributes();
			this.wrapped.StartElementContent();
		}

		// Token: 0x060033EF RID: 13295 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
		}

		// Token: 0x060033F0 RID: 13296 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void WriteEndElement(string prefix, string localName, string ns)
		{
		}

		// Token: 0x060033F1 RID: 13297 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteComment(string text)
		{
		}

		// Token: 0x060033F2 RID: 13298 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteProcessingInstruction(string name, string text)
		{
		}

		// Token: 0x060033F3 RID: 13299 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteEntityRef(string name)
		{
		}

		// Token: 0x060033F4 RID: 13300 RVA: 0x0010EE6E File Offset: 0x0010D06E
		public override void Close()
		{
			this.wrapped.Close();
		}

		// Token: 0x060033F5 RID: 13301 RVA: 0x0010EE7B File Offset: 0x0010D07B
		public override void Flush()
		{
			this.wrapped.Flush();
		}

		// Token: 0x060033F6 RID: 13302 RVA: 0x0010EE88 File Offset: 0x0010D088
		private void FlushAttributes()
		{
			int num = 0;
			while (num != this.numEntries)
			{
				int nextNameIndex = this.arrAttrs[num].NextNameIndex;
				if (nextNameIndex == 0)
				{
					nextNameIndex = this.numEntries;
				}
				string localName = this.arrAttrs[num].LocalName;
				if (localName != null)
				{
					string prefix = this.arrAttrs[num].Prefix;
					string @namespace = this.arrAttrs[num].Namespace;
					this.wrapped.WriteStartAttribute(prefix, localName, @namespace);
					while (++num != nextNameIndex)
					{
						string text = this.arrAttrs[num].Text;
						if (text != null)
						{
							this.wrapped.WriteString(text);
						}
						else
						{
							this.wrapped.WriteValue(this.arrAttrs[num].Value);
						}
					}
					this.wrapped.WriteEndAttribute();
				}
				else
				{
					num = nextNameIndex;
				}
			}
			if (this.onRemove != null)
			{
				this.onRemove(this.wrapped);
			}
		}

		// Token: 0x060033F7 RID: 13303 RVA: 0x0010EF84 File Offset: 0x0010D184
		private void EnsureAttributeCache()
		{
			if (this.arrAttrs == null)
			{
				this.arrAttrs = new XmlAttributeCache.AttrNameVal[32];
				return;
			}
			if (this.numEntries >= this.arrAttrs.Length)
			{
				XmlAttributeCache.AttrNameVal[] destinationArray = new XmlAttributeCache.AttrNameVal[this.numEntries * 2];
				Array.Copy(this.arrAttrs, destinationArray, this.numEntries);
				this.arrAttrs = destinationArray;
			}
		}

		// Token: 0x060033F8 RID: 13304 RVA: 0x0010EFDE File Offset: 0x0010D1DE
		public XmlAttributeCache()
		{
		}

		// Token: 0x0400232D RID: 9005
		private XmlRawWriter wrapped;

		// Token: 0x0400232E RID: 9006
		private OnRemoveWriter onRemove;

		// Token: 0x0400232F RID: 9007
		private XmlAttributeCache.AttrNameVal[] arrAttrs;

		// Token: 0x04002330 RID: 9008
		private int numEntries;

		// Token: 0x04002331 RID: 9009
		private int idxLastName;

		// Token: 0x04002332 RID: 9010
		private int hashCodeUnion;

		// Token: 0x04002333 RID: 9011
		private const int DefaultCacheSize = 32;

		// Token: 0x02000548 RID: 1352
		private struct AttrNameVal
		{
			// Token: 0x17000AE1 RID: 2785
			// (get) Token: 0x060033F9 RID: 13305 RVA: 0x0010EFE6 File Offset: 0x0010D1E6
			public string LocalName
			{
				get
				{
					return this.localName;
				}
			}

			// Token: 0x17000AE2 RID: 2786
			// (get) Token: 0x060033FA RID: 13306 RVA: 0x0010EFEE File Offset: 0x0010D1EE
			public string Prefix
			{
				get
				{
					return this.prefix;
				}
			}

			// Token: 0x17000AE3 RID: 2787
			// (get) Token: 0x060033FB RID: 13307 RVA: 0x0010EFF6 File Offset: 0x0010D1F6
			public string Namespace
			{
				get
				{
					return this.namespaceName;
				}
			}

			// Token: 0x17000AE4 RID: 2788
			// (get) Token: 0x060033FC RID: 13308 RVA: 0x0010EFFE File Offset: 0x0010D1FE
			public string Text
			{
				get
				{
					return this.text;
				}
			}

			// Token: 0x17000AE5 RID: 2789
			// (get) Token: 0x060033FD RID: 13309 RVA: 0x0010F006 File Offset: 0x0010D206
			public XmlAtomicValue Value
			{
				get
				{
					return this.value;
				}
			}

			// Token: 0x17000AE6 RID: 2790
			// (get) Token: 0x060033FE RID: 13310 RVA: 0x0010F00E File Offset: 0x0010D20E
			// (set) Token: 0x060033FF RID: 13311 RVA: 0x0010F016 File Offset: 0x0010D216
			public int NextNameIndex
			{
				get
				{
					return this.nextNameIndex;
				}
				set
				{
					this.nextNameIndex = value;
				}
			}

			// Token: 0x06003400 RID: 13312 RVA: 0x0010F01F File Offset: 0x0010D21F
			public void Init(string prefix, string localName, string ns, int hashCode)
			{
				this.localName = localName;
				this.prefix = prefix;
				this.namespaceName = ns;
				this.hashCode = hashCode;
				this.nextNameIndex = 0;
			}

			// Token: 0x06003401 RID: 13313 RVA: 0x0010F045 File Offset: 0x0010D245
			public void Init(string text)
			{
				this.text = text;
				this.value = null;
			}

			// Token: 0x06003402 RID: 13314 RVA: 0x0010F055 File Offset: 0x0010D255
			public void Init(XmlAtomicValue value)
			{
				this.text = null;
				this.value = value;
			}

			// Token: 0x06003403 RID: 13315 RVA: 0x0010F065 File Offset: 0x0010D265
			public bool IsDuplicate(string localName, string ns, int hashCode)
			{
				if (this.localName != null && this.hashCode == hashCode && this.localName.Equals(localName) && this.namespaceName.Equals(ns))
				{
					this.localName = null;
					return true;
				}
				return false;
			}

			// Token: 0x04002334 RID: 9012
			private string localName;

			// Token: 0x04002335 RID: 9013
			private string prefix;

			// Token: 0x04002336 RID: 9014
			private string namespaceName;

			// Token: 0x04002337 RID: 9015
			private string text;

			// Token: 0x04002338 RID: 9016
			private XmlAtomicValue value;

			// Token: 0x04002339 RID: 9017
			private int hashCode;

			// Token: 0x0400233A RID: 9018
			private int nextNameIndex;
		}
	}
}
