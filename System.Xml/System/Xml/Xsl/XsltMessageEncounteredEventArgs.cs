﻿using System;

namespace System.Xml.Xsl
{
	/// <summary>Provides data for the <see cref="E:System.Xml.Xsl.XsltArgumentList.XsltMessageEncountered" /> event.</summary>
	// Token: 0x020004A3 RID: 1187
	public abstract class XsltMessageEncounteredEventArgs : EventArgs
	{
		/// <summary>Gets the contents of the xsl:message element.</summary>
		/// <returns>The contents of the xsl:message element.</returns>
		// Token: 0x170009E5 RID: 2533
		// (get) Token: 0x06002EB7 RID: 11959
		public abstract string Message { get; }

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Xsl.XsltMessageEncounteredEventArgs" /> class.</summary>
		// Token: 0x06002EB8 RID: 11960 RVA: 0x000FC724 File Offset: 0x000FA924
		protected XsltMessageEncounteredEventArgs()
		{
		}
	}
}
