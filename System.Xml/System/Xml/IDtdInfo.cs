﻿using System;
using System.Collections.Generic;

namespace System.Xml
{
	// Token: 0x0200009D RID: 157
	internal interface IDtdInfo
	{
		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000523 RID: 1315
		XmlQualifiedName Name { get; }

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000524 RID: 1316
		string InternalDtdSubset { get; }

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000525 RID: 1317
		bool HasDefaultAttributes { get; }

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000526 RID: 1318
		bool HasNonCDataAttributes { get; }

		// Token: 0x06000527 RID: 1319
		IDtdAttributeListInfo LookupAttributeList(string prefix, string localName);

		// Token: 0x06000528 RID: 1320
		IEnumerable<IDtdAttributeListInfo> GetAttributeLists();

		// Token: 0x06000529 RID: 1321
		IDtdEntityInfo LookupEntity(string name);
	}
}
