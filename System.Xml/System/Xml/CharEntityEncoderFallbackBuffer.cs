﻿using System;
using System.Globalization;
using System.Text;

namespace System.Xml
{
	// Token: 0x02000094 RID: 148
	internal class CharEntityEncoderFallbackBuffer : EncoderFallbackBuffer
	{
		// Token: 0x060004D7 RID: 1239 RVA: 0x0001653A File Offset: 0x0001473A
		internal CharEntityEncoderFallbackBuffer(CharEntityEncoderFallback parent)
		{
			this.parent = parent;
		}

		// Token: 0x060004D8 RID: 1240 RVA: 0x0001655C File Offset: 0x0001475C
		public override bool Fallback(char charUnknown, int index)
		{
			if (this.charEntityIndex >= 0)
			{
				new EncoderExceptionFallback().CreateFallbackBuffer().Fallback(charUnknown, index);
			}
			if (this.parent.CanReplaceAt(index))
			{
				this.charEntity = string.Format(CultureInfo.InvariantCulture, "&#x{0:X};", new object[]
				{
					(int)charUnknown
				});
				this.charEntityIndex = 0;
				return true;
			}
			new EncoderExceptionFallback().CreateFallbackBuffer().Fallback(charUnknown, index);
			return false;
		}

		// Token: 0x060004D9 RID: 1241 RVA: 0x000165D4 File Offset: 0x000147D4
		public override bool Fallback(char charUnknownHigh, char charUnknownLow, int index)
		{
			if (!char.IsSurrogatePair(charUnknownHigh, charUnknownLow))
			{
				throw XmlConvert.CreateInvalidSurrogatePairException(charUnknownHigh, charUnknownLow);
			}
			if (this.charEntityIndex >= 0)
			{
				new EncoderExceptionFallback().CreateFallbackBuffer().Fallback(charUnknownHigh, charUnknownLow, index);
			}
			if (this.parent.CanReplaceAt(index))
			{
				this.charEntity = string.Format(CultureInfo.InvariantCulture, "&#x{0:X};", new object[]
				{
					this.SurrogateCharToUtf32(charUnknownHigh, charUnknownLow)
				});
				this.charEntityIndex = 0;
				return true;
			}
			new EncoderExceptionFallback().CreateFallbackBuffer().Fallback(charUnknownHigh, charUnknownLow, index);
			return false;
		}

		// Token: 0x060004DA RID: 1242 RVA: 0x00016664 File Offset: 0x00014864
		public override char GetNextChar()
		{
			if (this.charEntityIndex == this.charEntity.Length)
			{
				this.charEntityIndex = -1;
			}
			if (this.charEntityIndex == -1)
			{
				return '\0';
			}
			string text = this.charEntity;
			int num = this.charEntityIndex;
			this.charEntityIndex = num + 1;
			return text[num];
		}

		// Token: 0x060004DB RID: 1243 RVA: 0x000166B2 File Offset: 0x000148B2
		public override bool MovePrevious()
		{
			if (this.charEntityIndex == -1)
			{
				return false;
			}
			if (this.charEntityIndex > 0)
			{
				this.charEntityIndex--;
				return true;
			}
			return false;
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x060004DC RID: 1244 RVA: 0x000166D9 File Offset: 0x000148D9
		public override int Remaining
		{
			get
			{
				if (this.charEntityIndex == -1)
				{
					return 0;
				}
				return this.charEntity.Length - this.charEntityIndex;
			}
		}

		// Token: 0x060004DD RID: 1245 RVA: 0x000166F8 File Offset: 0x000148F8
		public override void Reset()
		{
			this.charEntityIndex = -1;
		}

		// Token: 0x060004DE RID: 1246 RVA: 0x00016701 File Offset: 0x00014901
		private int SurrogateCharToUtf32(char highSurrogate, char lowSurrogate)
		{
			return XmlCharType.CombineSurrogateChar((int)lowSurrogate, (int)highSurrogate);
		}

		// Token: 0x04000306 RID: 774
		private CharEntityEncoderFallback parent;

		// Token: 0x04000307 RID: 775
		private string charEntity = string.Empty;

		// Token: 0x04000308 RID: 776
		private int charEntityIndex = -1;
	}
}
