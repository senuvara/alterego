﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x02000213 RID: 531
	internal sealed class DocumentXPathNodeIterator_ElemChildren_AndSelf : DocumentXPathNodeIterator_ElemChildren
	{
		// Token: 0x060012F7 RID: 4855 RVA: 0x00070B34 File Offset: 0x0006ED34
		internal DocumentXPathNodeIterator_ElemChildren_AndSelf(DocumentXPathNavigator nav, string localNameAtom, string nsAtom) : base(nav, localNameAtom, nsAtom)
		{
		}

		// Token: 0x060012F8 RID: 4856 RVA: 0x00070B3F File Offset: 0x0006ED3F
		internal DocumentXPathNodeIterator_ElemChildren_AndSelf(DocumentXPathNodeIterator_ElemChildren_AndSelf other) : base(other)
		{
		}

		// Token: 0x060012F9 RID: 4857 RVA: 0x00070B48 File Offset: 0x0006ED48
		public override XPathNodeIterator Clone()
		{
			return new DocumentXPathNodeIterator_ElemChildren_AndSelf(this);
		}

		// Token: 0x060012FA RID: 4858 RVA: 0x00070B50 File Offset: 0x0006ED50
		public override bool MoveNext()
		{
			if (this.CurrentPosition == 0)
			{
				XmlNode xmlNode = (XmlNode)((DocumentXPathNavigator)this.Current).UnderlyingObject;
				if (xmlNode.NodeType == XmlNodeType.Element && this.Match(xmlNode))
				{
					base.SetPosition(1);
					return true;
				}
			}
			return base.MoveNext();
		}
	}
}
