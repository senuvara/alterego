﻿using System;

namespace System.Xml
{
	// Token: 0x0200009F RID: 159
	internal interface IDtdAttributeInfo
	{
		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000530 RID: 1328
		string Prefix { get; }

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000531 RID: 1329
		string LocalName { get; }

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000532 RID: 1330
		int LineNumber { get; }

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x06000533 RID: 1331
		int LinePosition { get; }

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x06000534 RID: 1332
		bool IsNonCDataType { get; }

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06000535 RID: 1333
		bool IsDeclaredInExternal { get; }

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x06000536 RID: 1334
		bool IsXmlAttribute { get; }
	}
}
