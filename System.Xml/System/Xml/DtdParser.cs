﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace System.Xml
{
	// Token: 0x02000251 RID: 593
	internal class DtdParser : IDtdParser
	{
		// Token: 0x0600169E RID: 5790 RVA: 0x000030EC File Offset: 0x000012EC
		static DtdParser()
		{
		}

		// Token: 0x0600169F RID: 5791 RVA: 0x0007C464 File Offset: 0x0007A664
		private DtdParser()
		{
		}

		// Token: 0x060016A0 RID: 5792 RVA: 0x0007C4D1 File Offset: 0x0007A6D1
		internal static IDtdParser Create()
		{
			return new DtdParser();
		}

		// Token: 0x060016A1 RID: 5793 RVA: 0x0007C4D8 File Offset: 0x0007A6D8
		private void Initialize(IDtdParserAdapter readerAdapter)
		{
			this.readerAdapter = readerAdapter;
			this.readerAdapterWithValidation = (readerAdapter as IDtdParserAdapterWithValidation);
			this.nameTable = readerAdapter.NameTable;
			IDtdParserAdapterWithValidation dtdParserAdapterWithValidation = readerAdapter as IDtdParserAdapterWithValidation;
			if (dtdParserAdapterWithValidation != null)
			{
				this.validate = dtdParserAdapterWithValidation.DtdValidation;
			}
			IDtdParserAdapterV1 dtdParserAdapterV = readerAdapter as IDtdParserAdapterV1;
			if (dtdParserAdapterV != null)
			{
				this.v1Compat = dtdParserAdapterV.V1CompatibilityMode;
				this.normalize = dtdParserAdapterV.Normalization;
				this.supportNamespaces = dtdParserAdapterV.Namespaces;
			}
			this.schemaInfo = new SchemaInfo();
			this.schemaInfo.SchemaType = SchemaType.DTD;
			this.stringBuilder = new StringBuilder();
			Uri baseUri = readerAdapter.BaseUri;
			if (baseUri != null)
			{
				this.documentBaseUri = baseUri.ToString();
			}
			this.freeFloatingDtd = false;
		}

		// Token: 0x060016A2 RID: 5794 RVA: 0x0007C590 File Offset: 0x0007A790
		private void InitializeFreeFloatingDtd(string baseUri, string docTypeName, string publicId, string systemId, string internalSubset, IDtdParserAdapter adapter)
		{
			this.Initialize(adapter);
			if (docTypeName == null || docTypeName.Length == 0)
			{
				throw XmlConvert.CreateInvalidNameArgumentException(docTypeName, "docTypeName");
			}
			XmlConvert.VerifyName(docTypeName);
			int num = docTypeName.IndexOf(':');
			if (num == -1)
			{
				this.schemaInfo.DocTypeName = new XmlQualifiedName(this.nameTable.Add(docTypeName));
			}
			else
			{
				this.schemaInfo.DocTypeName = new XmlQualifiedName(this.nameTable.Add(docTypeName.Substring(0, num)), this.nameTable.Add(docTypeName.Substring(num + 1)));
			}
			if (systemId != null && systemId.Length > 0)
			{
				int invCharPos;
				if ((invCharPos = this.xmlCharType.IsOnlyCharData(systemId)) >= 0)
				{
					this.ThrowInvalidChar(this.curPos, systemId, invCharPos);
				}
				this.systemId = systemId;
			}
			if (publicId != null && publicId.Length > 0)
			{
				int invCharPos;
				if ((invCharPos = this.xmlCharType.IsPublicId(publicId)) >= 0)
				{
					this.ThrowInvalidChar(this.curPos, publicId, invCharPos);
				}
				this.publicId = publicId;
			}
			if (internalSubset != null && internalSubset.Length > 0)
			{
				this.readerAdapter.PushInternalDtd(baseUri, internalSubset);
				this.hasFreeFloatingInternalSubset = true;
			}
			Uri baseUri2 = this.readerAdapter.BaseUri;
			if (baseUri2 != null)
			{
				this.documentBaseUri = baseUri2.ToString();
			}
			this.freeFloatingDtd = true;
		}

		// Token: 0x060016A3 RID: 5795 RVA: 0x0007C6D9 File Offset: 0x0007A8D9
		IDtdInfo IDtdParser.ParseInternalDtd(IDtdParserAdapter adapter, bool saveInternalSubset)
		{
			this.Initialize(adapter);
			this.Parse(saveInternalSubset);
			return this.schemaInfo;
		}

		// Token: 0x060016A4 RID: 5796 RVA: 0x0007C6EF File Offset: 0x0007A8EF
		IDtdInfo IDtdParser.ParseFreeFloatingDtd(string baseUri, string docTypeName, string publicId, string systemId, string internalSubset, IDtdParserAdapter adapter)
		{
			this.InitializeFreeFloatingDtd(baseUri, docTypeName, publicId, systemId, internalSubset, adapter);
			this.Parse(false);
			return this.schemaInfo;
		}

		// Token: 0x1700048A RID: 1162
		// (get) Token: 0x060016A5 RID: 5797 RVA: 0x0007C70D File Offset: 0x0007A90D
		private bool ParsingInternalSubset
		{
			get
			{
				return this.externalEntitiesDepth == 0;
			}
		}

		// Token: 0x1700048B RID: 1163
		// (get) Token: 0x060016A6 RID: 5798 RVA: 0x0007C718 File Offset: 0x0007A918
		private bool IgnoreEntityReferences
		{
			get
			{
				return this.scanningFunction == DtdParser.ScanningFunction.CondSection3;
			}
		}

		// Token: 0x1700048C RID: 1164
		// (get) Token: 0x060016A7 RID: 5799 RVA: 0x0007C724 File Offset: 0x0007A924
		private bool SaveInternalSubsetValue
		{
			get
			{
				return this.readerAdapter.EntityStackLength == 0 && this.internalSubsetValueSb != null;
			}
		}

		// Token: 0x1700048D RID: 1165
		// (get) Token: 0x060016A8 RID: 5800 RVA: 0x0007C73E File Offset: 0x0007A93E
		private bool ParsingTopLevelMarkup
		{
			get
			{
				return this.scanningFunction == DtdParser.ScanningFunction.SubsetContent || (this.scanningFunction == DtdParser.ScanningFunction.ParamEntitySpace && this.savedScanningFunction == DtdParser.ScanningFunction.SubsetContent);
			}
		}

		// Token: 0x1700048E RID: 1166
		// (get) Token: 0x060016A9 RID: 5801 RVA: 0x0007C75F File Offset: 0x0007A95F
		private bool SupportNamespaces
		{
			get
			{
				return this.supportNamespaces;
			}
		}

		// Token: 0x1700048F RID: 1167
		// (get) Token: 0x060016AA RID: 5802 RVA: 0x0007C767 File Offset: 0x0007A967
		private bool Normalize
		{
			get
			{
				return this.normalize;
			}
		}

		// Token: 0x060016AB RID: 5803 RVA: 0x0007C770 File Offset: 0x0007A970
		private void Parse(bool saveInternalSubset)
		{
			if (this.freeFloatingDtd)
			{
				this.ParseFreeFloatingDtd();
			}
			else
			{
				this.ParseInDocumentDtd(saveInternalSubset);
			}
			this.schemaInfo.Finish();
			if (this.validate && this.undeclaredNotations != null)
			{
				foreach (DtdParser.UndeclaredNotation undeclaredNotation in this.undeclaredNotations.Values)
				{
					for (DtdParser.UndeclaredNotation undeclaredNotation2 = undeclaredNotation; undeclaredNotation2 != null; undeclaredNotation2 = undeclaredNotation2.next)
					{
						this.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("The '{0}' notation is not declared.", undeclaredNotation.name, this.BaseUriStr, undeclaredNotation.lineNo, undeclaredNotation.linePos));
					}
				}
			}
		}

		// Token: 0x060016AC RID: 5804 RVA: 0x0007C82C File Offset: 0x0007AA2C
		private void ParseInDocumentDtd(bool saveInternalSubset)
		{
			this.LoadParsingBuffer();
			this.scanningFunction = DtdParser.ScanningFunction.QName;
			this.nextScaningFunction = DtdParser.ScanningFunction.Doctype1;
			if (this.GetToken(false) != DtdParser.Token.QName)
			{
				this.OnUnexpectedError();
			}
			this.schemaInfo.DocTypeName = this.GetNameQualified(true);
			DtdParser.Token token = this.GetToken(false);
			if (token == DtdParser.Token.SYSTEM || token == DtdParser.Token.PUBLIC)
			{
				this.ParseExternalId(token, DtdParser.Token.DOCTYPE, out this.publicId, out this.systemId);
				token = this.GetToken(false);
			}
			if (token != DtdParser.Token.GreaterThan)
			{
				if (token == DtdParser.Token.LeftBracket)
				{
					if (saveInternalSubset)
					{
						this.SaveParsingBuffer();
						this.internalSubsetValueSb = new StringBuilder();
					}
					this.ParseInternalSubset();
				}
				else
				{
					this.OnUnexpectedError();
				}
			}
			this.SaveParsingBuffer();
			if (this.systemId != null && this.systemId.Length > 0)
			{
				this.ParseExternalSubset();
			}
		}

		// Token: 0x060016AD RID: 5805 RVA: 0x0007C8ED File Offset: 0x0007AAED
		private void ParseFreeFloatingDtd()
		{
			if (this.hasFreeFloatingInternalSubset)
			{
				this.LoadParsingBuffer();
				this.ParseInternalSubset();
				this.SaveParsingBuffer();
			}
			if (this.systemId != null && this.systemId.Length > 0)
			{
				this.ParseExternalSubset();
			}
		}

		// Token: 0x060016AE RID: 5806 RVA: 0x0007C925 File Offset: 0x0007AB25
		private void ParseInternalSubset()
		{
			this.ParseSubset();
		}

		// Token: 0x060016AF RID: 5807 RVA: 0x0007C930 File Offset: 0x0007AB30
		private void ParseExternalSubset()
		{
			if (!this.readerAdapter.PushExternalSubset(this.systemId, this.publicId))
			{
				return;
			}
			Uri baseUri = this.readerAdapter.BaseUri;
			if (baseUri != null)
			{
				this.externalDtdBaseUri = baseUri.ToString();
			}
			this.externalEntitiesDepth++;
			this.LoadParsingBuffer();
			this.ParseSubset();
		}

		// Token: 0x060016B0 RID: 5808 RVA: 0x0007C994 File Offset: 0x0007AB94
		private void ParseSubset()
		{
			for (;;)
			{
				DtdParser.Token token = this.GetToken(false);
				int num = this.currentEntityId;
				switch (token)
				{
				case DtdParser.Token.AttlistDecl:
					this.ParseAttlistDecl();
					break;
				case DtdParser.Token.ElementDecl:
					this.ParseElementDecl();
					break;
				case DtdParser.Token.EntityDecl:
					this.ParseEntityDecl();
					break;
				case DtdParser.Token.NotationDecl:
					this.ParseNotationDecl();
					break;
				case DtdParser.Token.Comment:
					this.ParseComment();
					break;
				case DtdParser.Token.PI:
					this.ParsePI();
					break;
				case DtdParser.Token.CondSectionStart:
					if (this.ParsingInternalSubset)
					{
						this.Throw(this.curPos - 3, "A conditional section is not allowed in an internal subset.");
					}
					this.ParseCondSection();
					num = this.currentEntityId;
					break;
				case DtdParser.Token.CondSectionEnd:
					if (this.condSectionDepth > 0)
					{
						this.condSectionDepth--;
						if (this.validate && this.currentEntityId != this.condSectionEntityIds[this.condSectionDepth])
						{
							this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						}
					}
					else
					{
						this.Throw(this.curPos - 3, "']]>' is not expected.");
					}
					break;
				case DtdParser.Token.Eof:
					goto IL_1A9;
				default:
					if (token == DtdParser.Token.RightBracket)
					{
						goto IL_126;
					}
					break;
				}
				if (this.currentEntityId != num)
				{
					if (this.validate)
					{
						this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
					}
					else if (!this.v1Compat)
					{
						this.Throw(this.curPos, "The parameter entity replacement text must nest properly within markup declarations.");
					}
				}
			}
			IL_126:
			if (this.ParsingInternalSubset)
			{
				if (this.condSectionDepth != 0)
				{
					this.Throw(this.curPos, "There is an unclosed conditional section.");
				}
				if (this.internalSubsetValueSb != null)
				{
					this.SaveParsingBuffer(this.curPos - 1);
					this.schemaInfo.InternalDtdSubset = this.internalSubsetValueSb.ToString();
					this.internalSubsetValueSb = null;
				}
				if (this.GetToken(false) != DtdParser.Token.GreaterThan)
				{
					this.ThrowUnexpectedToken(this.curPos, ">");
					return;
				}
			}
			else
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			return;
			IL_1A9:
			if (this.ParsingInternalSubset && !this.freeFloatingDtd)
			{
				this.Throw(this.curPos, "Incomplete DTD content.");
			}
			if (this.condSectionDepth != 0)
			{
				this.Throw(this.curPos, "There is an unclosed conditional section.");
			}
		}

		// Token: 0x060016B1 RID: 5809 RVA: 0x0007CBD8 File Offset: 0x0007ADD8
		private void ParseAttlistDecl()
		{
			if (this.GetToken(true) == DtdParser.Token.QName)
			{
				XmlQualifiedName nameQualified = this.GetNameQualified(true);
				SchemaElementDecl schemaElementDecl;
				if (!this.schemaInfo.ElementDecls.TryGetValue(nameQualified, out schemaElementDecl) && !this.schemaInfo.UndeclaredElementDecls.TryGetValue(nameQualified, out schemaElementDecl))
				{
					schemaElementDecl = new SchemaElementDecl(nameQualified, nameQualified.Namespace);
					this.schemaInfo.UndeclaredElementDecls.Add(nameQualified, schemaElementDecl);
				}
				SchemaAttDef schemaAttDef = null;
				DtdParser.Token token;
				for (;;)
				{
					token = this.GetToken(false);
					if (token != DtdParser.Token.QName)
					{
						break;
					}
					XmlQualifiedName nameQualified2 = this.GetNameQualified(true);
					schemaAttDef = new SchemaAttDef(nameQualified2, nameQualified2.Namespace);
					schemaAttDef.IsDeclaredInExternal = !this.ParsingInternalSubset;
					schemaAttDef.LineNumber = this.LineNo;
					schemaAttDef.LinePosition = this.LinePos - (this.curPos - this.tokenStartPos);
					bool flag = schemaElementDecl.GetAttDef(schemaAttDef.Name) != null;
					this.ParseAttlistType(schemaAttDef, schemaElementDecl, flag);
					this.ParseAttlistDefault(schemaAttDef, flag);
					if (schemaAttDef.Prefix.Length > 0 && schemaAttDef.Prefix.Equals("xml"))
					{
						if (schemaAttDef.Name.Name == "space")
						{
							if (this.v1Compat)
							{
								string text = schemaAttDef.DefaultValueExpanded.Trim();
								if (text.Equals("preserve") || text.Equals("default"))
								{
									schemaAttDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
								}
							}
							else
							{
								schemaAttDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
								if (schemaAttDef.TokenizedType != XmlTokenizedType.ENUMERATION)
								{
									this.Throw("Enumeration data type required.", string.Empty, schemaAttDef.LineNumber, schemaAttDef.LinePosition);
								}
								if (this.validate)
								{
									schemaAttDef.CheckXmlSpace(this.readerAdapterWithValidation.ValidationEventHandling);
								}
							}
						}
						else if (schemaAttDef.Name.Name == "lang")
						{
							schemaAttDef.Reserved = SchemaAttDef.Reserve.XmlLang;
						}
					}
					if (!flag)
					{
						schemaElementDecl.AddAttDef(schemaAttDef);
					}
				}
				if (token == DtdParser.Token.GreaterThan)
				{
					if (this.v1Compat && schemaAttDef != null && schemaAttDef.Prefix.Length > 0 && schemaAttDef.Prefix.Equals("xml") && schemaAttDef.Name.Name == "space")
					{
						schemaAttDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
						if (schemaAttDef.Datatype.TokenizedType != XmlTokenizedType.ENUMERATION)
						{
							this.Throw("Enumeration data type required.", string.Empty, schemaAttDef.LineNumber, schemaAttDef.LinePosition);
						}
						if (this.validate)
						{
							schemaAttDef.CheckXmlSpace(this.readerAdapterWithValidation.ValidationEventHandling);
						}
					}
					return;
				}
			}
			this.OnUnexpectedError();
		}

		// Token: 0x060016B2 RID: 5810 RVA: 0x0007CE5C File Offset: 0x0007B05C
		private void ParseAttlistType(SchemaAttDef attrDef, SchemaElementDecl elementDecl, bool ignoreErrors)
		{
			DtdParser.Token token = this.GetToken(true);
			if (token != DtdParser.Token.CDATA)
			{
				elementDecl.HasNonCDataAttribute = true;
			}
			if (this.IsAttributeValueType(token))
			{
				attrDef.TokenizedType = (XmlTokenizedType)token;
				attrDef.SchemaType = XmlSchemaType.GetBuiltInSimpleType(attrDef.Datatype.TypeCode);
				if (token == DtdParser.Token.ID)
				{
					if (this.validate && elementDecl.IsIdDeclared)
					{
						SchemaAttDef attDef = elementDecl.GetAttDef(attrDef.Name);
						if ((attDef == null || attDef.Datatype.TokenizedType != XmlTokenizedType.ID) && !ignoreErrors)
						{
							this.SendValidationEvent(XmlSeverityType.Error, "The attribute of type ID is already declared on the '{0}' element.", elementDecl.Name.ToString());
						}
					}
					elementDecl.IsIdDeclared = true;
					return;
				}
				if (token != DtdParser.Token.NOTATION)
				{
					return;
				}
				if (this.validate)
				{
					if (elementDecl.IsNotationDeclared && !ignoreErrors)
					{
						this.SendValidationEvent(this.curPos - 8, XmlSeverityType.Error, "No element type can have more than one NOTATION attribute specified.", elementDecl.Name.ToString());
					}
					else
					{
						if (elementDecl.ContentValidator != null && elementDecl.ContentValidator.ContentType == XmlSchemaContentType.Empty && !ignoreErrors)
						{
							this.SendValidationEvent(this.curPos - 8, XmlSeverityType.Error, "An attribute of type NOTATION must not be declared on an element declared EMPTY.", elementDecl.Name.ToString());
						}
						elementDecl.IsNotationDeclared = true;
					}
				}
				if (this.GetToken(true) == DtdParser.Token.LeftParen && this.GetToken(false) == DtdParser.Token.Name)
				{
					do
					{
						string nameString = this.GetNameString();
						if (!this.schemaInfo.Notations.ContainsKey(nameString))
						{
							this.AddUndeclaredNotation(nameString);
						}
						if (this.validate && !this.v1Compat && attrDef.Values != null && attrDef.Values.Contains(nameString) && !ignoreErrors)
						{
							this.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("'{0}' is a duplicate notation value.", nameString, this.BaseUriStr, this.LineNo, this.LinePos));
						}
						attrDef.AddValue(nameString);
						DtdParser.Token token2 = this.GetToken(false);
						if (token2 == DtdParser.Token.RightParen)
						{
							return;
						}
						if (token2 != DtdParser.Token.Or)
						{
							break;
						}
					}
					while (this.GetToken(false) == DtdParser.Token.Name);
				}
			}
			else if (token == DtdParser.Token.LeftParen)
			{
				attrDef.TokenizedType = XmlTokenizedType.ENUMERATION;
				attrDef.SchemaType = XmlSchemaType.GetBuiltInSimpleType(attrDef.Datatype.TypeCode);
				if (this.GetToken(false) == DtdParser.Token.Nmtoken)
				{
					attrDef.AddValue(this.GetNameString());
					for (;;)
					{
						DtdParser.Token token2 = this.GetToken(false);
						if (token2 == DtdParser.Token.RightParen)
						{
							break;
						}
						if (token2 != DtdParser.Token.Or || this.GetToken(false) != DtdParser.Token.Nmtoken)
						{
							goto IL_280;
						}
						string nmtokenString = this.GetNmtokenString();
						if (this.validate && !this.v1Compat && attrDef.Values != null && attrDef.Values.Contains(nmtokenString) && !ignoreErrors)
						{
							this.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("'{0}' is a duplicate enumeration value.", nmtokenString, this.BaseUriStr, this.LineNo, this.LinePos));
						}
						attrDef.AddValue(nmtokenString);
					}
					return;
				}
			}
			IL_280:
			this.OnUnexpectedError();
		}

		// Token: 0x060016B3 RID: 5811 RVA: 0x0007D0F0 File Offset: 0x0007B2F0
		private void ParseAttlistDefault(SchemaAttDef attrDef, bool ignoreErrors)
		{
			DtdParser.Token token = this.GetToken(true);
			switch (token)
			{
			case DtdParser.Token.REQUIRED:
				attrDef.Presence = SchemaDeclBase.Use.Required;
				return;
			case DtdParser.Token.IMPLIED:
				attrDef.Presence = SchemaDeclBase.Use.Implied;
				return;
			case DtdParser.Token.FIXED:
				attrDef.Presence = SchemaDeclBase.Use.Fixed;
				if (this.GetToken(true) != DtdParser.Token.Literal)
				{
					goto IL_CF;
				}
				break;
			default:
				if (token != DtdParser.Token.Literal)
				{
					goto IL_CF;
				}
				break;
			}
			if (this.validate && attrDef.Datatype.TokenizedType == XmlTokenizedType.ID && !ignoreErrors)
			{
				this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "An attribute of type ID must have a declared default of either #IMPLIED or #REQUIRED.", string.Empty);
			}
			if (attrDef.TokenizedType != XmlTokenizedType.CDATA)
			{
				attrDef.DefaultValueExpanded = this.GetValueWithStrippedSpaces();
			}
			else
			{
				attrDef.DefaultValueExpanded = this.GetValue();
			}
			attrDef.ValueLineNumber = this.literalLineInfo.lineNo;
			attrDef.ValueLinePosition = this.literalLineInfo.linePos + 1;
			DtdValidator.SetDefaultTypedValue(attrDef, this.readerAdapter);
			return;
			IL_CF:
			this.OnUnexpectedError();
		}

		// Token: 0x060016B4 RID: 5812 RVA: 0x0007D1D4 File Offset: 0x0007B3D4
		private void ParseElementDecl()
		{
			if (this.GetToken(true) == DtdParser.Token.QName)
			{
				SchemaElementDecl schemaElementDecl = null;
				XmlQualifiedName nameQualified = this.GetNameQualified(true);
				if (this.schemaInfo.ElementDecls.TryGetValue(nameQualified, out schemaElementDecl))
				{
					if (this.validate)
					{
						this.SendValidationEvent(this.curPos - nameQualified.Name.Length, XmlSeverityType.Error, "The '{0}' element has already been declared.", this.GetNameString());
					}
				}
				else
				{
					if (this.schemaInfo.UndeclaredElementDecls.TryGetValue(nameQualified, out schemaElementDecl))
					{
						this.schemaInfo.UndeclaredElementDecls.Remove(nameQualified);
					}
					else
					{
						schemaElementDecl = new SchemaElementDecl(nameQualified, nameQualified.Namespace);
					}
					this.schemaInfo.ElementDecls.Add(nameQualified, schemaElementDecl);
				}
				schemaElementDecl.IsDeclaredInExternal = !this.ParsingInternalSubset;
				DtdParser.Token token = this.GetToken(true);
				if (token != DtdParser.Token.LeftParen)
				{
					if (token != DtdParser.Token.ANY)
					{
						if (token != DtdParser.Token.EMPTY)
						{
							goto IL_17E;
						}
						schemaElementDecl.ContentValidator = ContentValidator.Empty;
					}
					else
					{
						schemaElementDecl.ContentValidator = ContentValidator.Any;
					}
				}
				else
				{
					int startParenEntityId = this.currentEntityId;
					token = this.GetToken(false);
					if (token != DtdParser.Token.None)
					{
						if (token != DtdParser.Token.PCDATA)
						{
							goto IL_17E;
						}
						ParticleContentValidator particleContentValidator = new ParticleContentValidator(XmlSchemaContentType.Mixed);
						particleContentValidator.Start();
						particleContentValidator.OpenGroup();
						this.ParseElementMixedContent(particleContentValidator, startParenEntityId);
						schemaElementDecl.ContentValidator = particleContentValidator.Finish(true);
					}
					else
					{
						ParticleContentValidator particleContentValidator2 = new ParticleContentValidator(XmlSchemaContentType.ElementOnly);
						particleContentValidator2.Start();
						particleContentValidator2.OpenGroup();
						this.ParseElementOnlyContent(particleContentValidator2, startParenEntityId);
						schemaElementDecl.ContentValidator = particleContentValidator2.Finish(true);
					}
				}
				if (this.GetToken(false) != DtdParser.Token.GreaterThan)
				{
					this.ThrowUnexpectedToken(this.curPos, ">");
				}
				return;
			}
			IL_17E:
			this.OnUnexpectedError();
		}

		// Token: 0x060016B5 RID: 5813 RVA: 0x0007D368 File Offset: 0x0007B568
		private void ParseElementOnlyContent(ParticleContentValidator pcv, int startParenEntityId)
		{
			Stack<DtdParser.ParseElementOnlyContent_LocalFrame> stack = new Stack<DtdParser.ParseElementOnlyContent_LocalFrame>();
			DtdParser.ParseElementOnlyContent_LocalFrame parseElementOnlyContent_LocalFrame = new DtdParser.ParseElementOnlyContent_LocalFrame(startParenEntityId);
			stack.Push(parseElementOnlyContent_LocalFrame);
			for (;;)
			{
				DtdParser.Token token = this.GetToken(false);
				if (token != DtdParser.Token.QName)
				{
					if (token == DtdParser.Token.LeftParen)
					{
						pcv.OpenGroup();
						parseElementOnlyContent_LocalFrame = new DtdParser.ParseElementOnlyContent_LocalFrame(this.currentEntityId);
						stack.Push(parseElementOnlyContent_LocalFrame);
						continue;
					}
					if (token != DtdParser.Token.GreaterThan)
					{
						goto IL_148;
					}
					this.Throw(this.curPos, "Invalid content model.");
					goto IL_14E;
				}
				else
				{
					pcv.AddName(this.GetNameQualified(true), null);
					this.ParseHowMany(pcv);
				}
				IL_78:
				token = this.GetToken(false);
				switch (token)
				{
				case DtdParser.Token.RightParen:
					pcv.CloseGroup();
					if (this.validate && this.currentEntityId != parseElementOnlyContent_LocalFrame.startParenEntityId)
					{
						this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
					}
					this.ParseHowMany(pcv);
					break;
				case DtdParser.Token.GreaterThan:
					this.Throw(this.curPos, "Invalid content model.");
					break;
				case DtdParser.Token.Or:
					if (parseElementOnlyContent_LocalFrame.parsingSchema == DtdParser.Token.Comma)
					{
						this.Throw(this.curPos, "Invalid content model.");
					}
					pcv.AddChoice();
					parseElementOnlyContent_LocalFrame.parsingSchema = DtdParser.Token.Or;
					continue;
				default:
					if (token == DtdParser.Token.Comma)
					{
						if (parseElementOnlyContent_LocalFrame.parsingSchema == DtdParser.Token.Or)
						{
							this.Throw(this.curPos, "Invalid content model.");
						}
						pcv.AddSequence();
						parseElementOnlyContent_LocalFrame.parsingSchema = DtdParser.Token.Comma;
						continue;
					}
					goto IL_148;
				}
				IL_14E:
				stack.Pop();
				if (stack.Count > 0)
				{
					parseElementOnlyContent_LocalFrame = stack.Peek();
					goto IL_78;
				}
				break;
				IL_148:
				this.OnUnexpectedError();
				goto IL_14E;
			}
		}

		// Token: 0x060016B6 RID: 5814 RVA: 0x0007D4E0 File Offset: 0x0007B6E0
		private void ParseHowMany(ParticleContentValidator pcv)
		{
			switch (this.GetToken(false))
			{
			case DtdParser.Token.Star:
				pcv.AddStar();
				return;
			case DtdParser.Token.QMark:
				pcv.AddQMark();
				return;
			case DtdParser.Token.Plus:
				pcv.AddPlus();
				return;
			default:
				return;
			}
		}

		// Token: 0x060016B7 RID: 5815 RVA: 0x0007D520 File Offset: 0x0007B720
		private void ParseElementMixedContent(ParticleContentValidator pcv, int startParenEntityId)
		{
			bool flag = false;
			int num = -1;
			int num2 = this.currentEntityId;
			for (;;)
			{
				DtdParser.Token token = this.GetToken(false);
				if (token == DtdParser.Token.RightParen)
				{
					break;
				}
				if (token == DtdParser.Token.Or)
				{
					if (!flag)
					{
						flag = true;
					}
					else
					{
						pcv.AddChoice();
					}
					if (this.validate)
					{
						num = this.currentEntityId;
						if (num2 < num)
						{
							this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						}
					}
					if (this.GetToken(false) == DtdParser.Token.QName)
					{
						XmlQualifiedName nameQualified = this.GetNameQualified(true);
						if (pcv.Exists(nameQualified) && this.validate)
						{
							this.SendValidationEvent(XmlSeverityType.Error, "The '{0}' element already exists in the content model.", nameQualified.ToString());
						}
						pcv.AddName(nameQualified, null);
						if (!this.validate)
						{
							continue;
						}
						num2 = this.currentEntityId;
						if (num2 < num)
						{
							this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
							continue;
						}
						continue;
					}
				}
				this.OnUnexpectedError();
			}
			pcv.CloseGroup();
			if (this.validate && this.currentEntityId != startParenEntityId)
			{
				this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
			}
			if (this.GetToken(false) == DtdParser.Token.Star && flag)
			{
				pcv.AddStar();
				return;
			}
			if (flag)
			{
				this.ThrowUnexpectedToken(this.curPos, "*");
			}
		}

		// Token: 0x060016B8 RID: 5816 RVA: 0x0007D660 File Offset: 0x0007B860
		private void ParseEntityDecl()
		{
			bool flag = false;
			DtdParser.Token token = this.GetToken(true);
			if (token != DtdParser.Token.Name)
			{
				if (token != DtdParser.Token.Percent)
				{
					goto IL_1D9;
				}
				flag = true;
				if (this.GetToken(true) != DtdParser.Token.Name)
				{
					goto IL_1D9;
				}
			}
			XmlQualifiedName nameQualified = this.GetNameQualified(false);
			SchemaEntity schemaEntity = new SchemaEntity(nameQualified, flag);
			schemaEntity.BaseURI = this.BaseUriStr;
			schemaEntity.DeclaredURI = ((this.externalDtdBaseUri.Length == 0) ? this.documentBaseUri : this.externalDtdBaseUri);
			if (flag)
			{
				if (!this.schemaInfo.ParameterEntities.ContainsKey(nameQualified))
				{
					this.schemaInfo.ParameterEntities.Add(nameQualified, schemaEntity);
				}
			}
			else if (!this.schemaInfo.GeneralEntities.ContainsKey(nameQualified))
			{
				this.schemaInfo.GeneralEntities.Add(nameQualified, schemaEntity);
			}
			schemaEntity.DeclaredInExternal = !this.ParsingInternalSubset;
			schemaEntity.ParsingInProgress = true;
			DtdParser.Token token2 = this.GetToken(true);
			if (token2 - DtdParser.Token.PUBLIC > 1)
			{
				if (token2 != DtdParser.Token.Literal)
				{
					goto IL_1D9;
				}
				schemaEntity.Text = this.GetValue();
				schemaEntity.Line = this.literalLineInfo.lineNo;
				schemaEntity.Pos = this.literalLineInfo.linePos;
			}
			else
			{
				string pubid;
				string url;
				this.ParseExternalId(token2, DtdParser.Token.EntityDecl, out pubid, out url);
				schemaEntity.IsExternal = true;
				schemaEntity.Url = url;
				schemaEntity.Pubid = pubid;
				if (this.GetToken(false) == DtdParser.Token.NData)
				{
					if (flag)
					{
						this.ThrowUnexpectedToken(this.curPos - 5, ">");
					}
					if (!this.whitespaceSeen)
					{
						this.Throw(this.curPos - 5, "'{0}' is an unexpected token. Expecting white space.", "NDATA");
					}
					if (this.GetToken(true) != DtdParser.Token.Name)
					{
						goto IL_1D9;
					}
					schemaEntity.NData = this.GetNameQualified(false);
					string name = schemaEntity.NData.Name;
					if (!this.schemaInfo.Notations.ContainsKey(name))
					{
						this.AddUndeclaredNotation(name);
					}
				}
			}
			if (this.GetToken(false) == DtdParser.Token.GreaterThan)
			{
				schemaEntity.ParsingInProgress = false;
				return;
			}
			IL_1D9:
			this.OnUnexpectedError();
		}

		// Token: 0x060016B9 RID: 5817 RVA: 0x0007D84C File Offset: 0x0007BA4C
		private void ParseNotationDecl()
		{
			if (this.GetToken(true) != DtdParser.Token.Name)
			{
				this.OnUnexpectedError();
			}
			XmlQualifiedName nameQualified = this.GetNameQualified(false);
			SchemaNotation schemaNotation = null;
			if (!this.schemaInfo.Notations.ContainsKey(nameQualified.Name))
			{
				if (this.undeclaredNotations != null)
				{
					this.undeclaredNotations.Remove(nameQualified.Name);
				}
				schemaNotation = new SchemaNotation(nameQualified);
				this.schemaInfo.Notations.Add(schemaNotation.Name.Name, schemaNotation);
			}
			else if (this.validate)
			{
				this.SendValidationEvent(this.curPos - nameQualified.Name.Length, XmlSeverityType.Error, "The notation '{0}' has already been declared.", nameQualified.Name);
			}
			DtdParser.Token token = this.GetToken(true);
			if (token == DtdParser.Token.SYSTEM || token == DtdParser.Token.PUBLIC)
			{
				string pubid;
				string systemLiteral;
				this.ParseExternalId(token, DtdParser.Token.NOTATION, out pubid, out systemLiteral);
				if (schemaNotation != null)
				{
					schemaNotation.SystemLiteral = systemLiteral;
					schemaNotation.Pubid = pubid;
				}
			}
			else
			{
				this.OnUnexpectedError();
			}
			if (this.GetToken(false) != DtdParser.Token.GreaterThan)
			{
				this.OnUnexpectedError();
			}
		}

		// Token: 0x060016BA RID: 5818 RVA: 0x0007D940 File Offset: 0x0007BB40
		private void AddUndeclaredNotation(string notationName)
		{
			if (this.undeclaredNotations == null)
			{
				this.undeclaredNotations = new Dictionary<string, DtdParser.UndeclaredNotation>();
			}
			DtdParser.UndeclaredNotation undeclaredNotation = new DtdParser.UndeclaredNotation(notationName, this.LineNo, this.LinePos - notationName.Length);
			DtdParser.UndeclaredNotation undeclaredNotation2;
			if (this.undeclaredNotations.TryGetValue(notationName, out undeclaredNotation2))
			{
				undeclaredNotation.next = undeclaredNotation2.next;
				undeclaredNotation2.next = undeclaredNotation;
				return;
			}
			this.undeclaredNotations.Add(notationName, undeclaredNotation);
		}

		// Token: 0x060016BB RID: 5819 RVA: 0x0007D9AC File Offset: 0x0007BBAC
		private void ParseComment()
		{
			this.SaveParsingBuffer();
			try
			{
				if (this.SaveInternalSubsetValue)
				{
					this.readerAdapter.ParseComment(this.internalSubsetValueSb);
					this.internalSubsetValueSb.Append("-->");
				}
				else
				{
					this.readerAdapter.ParseComment(null);
				}
			}
			catch (XmlException ex)
			{
				if (!(ex.ResString == "Unexpected end of file while parsing {0} has occurred.") || this.currentEntityId == 0)
				{
					throw;
				}
				this.SendValidationEvent(XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", null);
			}
			this.LoadParsingBuffer();
		}

		// Token: 0x060016BC RID: 5820 RVA: 0x0007DA3C File Offset: 0x0007BC3C
		private void ParsePI()
		{
			this.SaveParsingBuffer();
			if (this.SaveInternalSubsetValue)
			{
				this.readerAdapter.ParsePI(this.internalSubsetValueSb);
				this.internalSubsetValueSb.Append("?>");
			}
			else
			{
				this.readerAdapter.ParsePI(null);
			}
			this.LoadParsingBuffer();
		}

		// Token: 0x060016BD RID: 5821 RVA: 0x0007DA90 File Offset: 0x0007BC90
		private void ParseCondSection()
		{
			int num = this.currentEntityId;
			DtdParser.Token token = this.GetToken(false);
			if (token != DtdParser.Token.IGNORE)
			{
				if (token == DtdParser.Token.INCLUDE && this.GetToken(false) == DtdParser.Token.LeftBracket)
				{
					if (this.validate && num != this.currentEntityId)
					{
						this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
					}
					if (this.validate)
					{
						if (this.condSectionEntityIds == null)
						{
							this.condSectionEntityIds = new int[2];
						}
						else if (this.condSectionEntityIds.Length == this.condSectionDepth)
						{
							int[] destinationArray = new int[this.condSectionEntityIds.Length * 2];
							Array.Copy(this.condSectionEntityIds, 0, destinationArray, 0, this.condSectionEntityIds.Length);
							this.condSectionEntityIds = destinationArray;
						}
						this.condSectionEntityIds[this.condSectionDepth] = num;
					}
					this.condSectionDepth++;
					return;
				}
			}
			else if (this.GetToken(false) == DtdParser.Token.LeftBracket)
			{
				if (this.validate && num != this.currentEntityId)
				{
					this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
				}
				if (this.GetToken(false) == DtdParser.Token.CondSectionEnd)
				{
					if (this.validate && num != this.currentEntityId)
					{
						this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						return;
					}
					return;
				}
			}
			this.OnUnexpectedError();
		}

		// Token: 0x060016BE RID: 5822 RVA: 0x0007DBD8 File Offset: 0x0007BDD8
		private void ParseExternalId(DtdParser.Token idTokenType, DtdParser.Token declType, out string publicId, out string systemId)
		{
			LineInfo keywordLineInfo = new LineInfo(this.LineNo, this.LinePos - 6);
			publicId = null;
			systemId = null;
			if (this.GetToken(true) != DtdParser.Token.Literal)
			{
				this.ThrowUnexpectedToken(this.curPos, "\"", "'");
			}
			if (idTokenType == DtdParser.Token.SYSTEM)
			{
				systemId = this.GetValue();
				if (systemId.IndexOf('#') >= 0)
				{
					this.Throw(this.curPos - systemId.Length - 1, "Fragment identifier '{0}' cannot be part of the system identifier '{1}'.", new string[]
					{
						systemId.Substring(systemId.IndexOf('#')),
						systemId
					});
				}
				if (declType == DtdParser.Token.DOCTYPE && !this.freeFloatingDtd)
				{
					this.literalLineInfo.linePos = this.literalLineInfo.linePos + 1;
					this.readerAdapter.OnSystemId(systemId, keywordLineInfo, this.literalLineInfo);
					return;
				}
			}
			else
			{
				publicId = this.GetValue();
				int num;
				if ((num = this.xmlCharType.IsPublicId(publicId)) >= 0)
				{
					this.ThrowInvalidChar(this.curPos - 1 - publicId.Length + num, publicId, num);
				}
				if (declType == DtdParser.Token.DOCTYPE && !this.freeFloatingDtd)
				{
					this.literalLineInfo.linePos = this.literalLineInfo.linePos + 1;
					this.readerAdapter.OnPublicId(publicId, keywordLineInfo, this.literalLineInfo);
					if (this.GetToken(false) == DtdParser.Token.Literal)
					{
						if (!this.whitespaceSeen)
						{
							this.Throw("'{0}' is an unexpected token. Expecting white space.", new string(this.literalQuoteChar, 1), this.literalLineInfo.lineNo, this.literalLineInfo.linePos);
						}
						systemId = this.GetValue();
						this.literalLineInfo.linePos = this.literalLineInfo.linePos + 1;
						this.readerAdapter.OnSystemId(systemId, keywordLineInfo, this.literalLineInfo);
						return;
					}
					this.ThrowUnexpectedToken(this.curPos, "\"", "'");
					return;
				}
				else
				{
					if (this.GetToken(false) == DtdParser.Token.Literal)
					{
						if (!this.whitespaceSeen)
						{
							this.Throw("'{0}' is an unexpected token. Expecting white space.", new string(this.literalQuoteChar, 1), this.literalLineInfo.lineNo, this.literalLineInfo.linePos);
						}
						systemId = this.GetValue();
						return;
					}
					if (declType != DtdParser.Token.NOTATION)
					{
						this.ThrowUnexpectedToken(this.curPos, "\"", "'");
					}
				}
			}
		}

		// Token: 0x060016BF RID: 5823 RVA: 0x0007DE0C File Offset: 0x0007C00C
		private DtdParser.Token GetToken(bool needWhiteSpace)
		{
			this.whitespaceSeen = false;
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c <= '\r')
				{
					if (c != '\0')
					{
						switch (c)
						{
						case '\t':
							goto IL_14D;
						case '\n':
							this.whitespaceSeen = true;
							this.curPos++;
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						case '\r':
							this.whitespaceSeen = true;
							if (this.chars[this.curPos + 1] == '\n')
							{
								if (this.Normalize)
								{
									this.SaveParsingBuffer();
									IDtdParserAdapter dtdParserAdapter = this.readerAdapter;
									int currentPosition = dtdParserAdapter.CurrentPosition;
									dtdParserAdapter.CurrentPosition = currentPosition + 1;
								}
								this.curPos += 2;
							}
							else
							{
								if (this.curPos + 1 >= this.charsUsed && !this.readerAdapter.IsEof)
								{
									goto IL_388;
								}
								this.chars[this.curPos] = '\n';
								this.curPos++;
							}
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						}
						break;
					}
					if (this.curPos != this.charsUsed)
					{
						this.ThrowInvalidChar(this.chars, this.charsUsed, this.curPos);
						goto IL_388;
					}
					goto IL_388;
				}
				else if (c != ' ')
				{
					if (c != '%')
					{
						break;
					}
					if (this.charsUsed - this.curPos < 2)
					{
						goto IL_388;
					}
					if (this.xmlCharType.IsWhiteSpace(this.chars[this.curPos + 1]))
					{
						break;
					}
					if (this.IgnoreEntityReferences)
					{
						this.curPos++;
						continue;
					}
					this.HandleEntityReference(true, false, false);
					continue;
				}
				IL_14D:
				this.whitespaceSeen = true;
				this.curPos++;
				continue;
				IL_388:
				if ((this.readerAdapter.IsEof || this.ReadData() == 0) && !this.HandleEntityEnd(false))
				{
					if (this.scanningFunction == DtdParser.ScanningFunction.SubsetContent)
					{
						return DtdParser.Token.Eof;
					}
					this.Throw(this.curPos, "Incomplete DTD content.");
				}
			}
			if (needWhiteSpace && !this.whitespaceSeen && this.scanningFunction != DtdParser.ScanningFunction.ParamEntitySpace)
			{
				this.Throw(this.curPos, "'{0}' is an unexpected token. Expecting white space.", this.ParseUnexpectedToken(this.curPos));
			}
			this.tokenStartPos = this.curPos;
			for (;;)
			{
				switch (this.scanningFunction)
				{
				case DtdParser.ScanningFunction.SubsetContent:
					goto IL_2A9;
				case DtdParser.ScanningFunction.Name:
					goto IL_294;
				case DtdParser.ScanningFunction.QName:
					goto IL_29B;
				case DtdParser.ScanningFunction.Nmtoken:
					goto IL_2A2;
				case DtdParser.ScanningFunction.Doctype1:
					goto IL_2B0;
				case DtdParser.ScanningFunction.Doctype2:
					goto IL_2B7;
				case DtdParser.ScanningFunction.Element1:
					goto IL_2BE;
				case DtdParser.ScanningFunction.Element2:
					goto IL_2C5;
				case DtdParser.ScanningFunction.Element3:
					goto IL_2CC;
				case DtdParser.ScanningFunction.Element4:
					goto IL_2D3;
				case DtdParser.ScanningFunction.Element5:
					goto IL_2DA;
				case DtdParser.ScanningFunction.Element6:
					goto IL_2E1;
				case DtdParser.ScanningFunction.Element7:
					goto IL_2E8;
				case DtdParser.ScanningFunction.Attlist1:
					goto IL_2EF;
				case DtdParser.ScanningFunction.Attlist2:
					goto IL_2F6;
				case DtdParser.ScanningFunction.Attlist3:
					goto IL_2FD;
				case DtdParser.ScanningFunction.Attlist4:
					goto IL_304;
				case DtdParser.ScanningFunction.Attlist5:
					goto IL_30B;
				case DtdParser.ScanningFunction.Attlist6:
					goto IL_312;
				case DtdParser.ScanningFunction.Attlist7:
					goto IL_319;
				case DtdParser.ScanningFunction.Entity1:
					goto IL_33C;
				case DtdParser.ScanningFunction.Entity2:
					goto IL_343;
				case DtdParser.ScanningFunction.Entity3:
					goto IL_34A;
				case DtdParser.ScanningFunction.Notation1:
					goto IL_320;
				case DtdParser.ScanningFunction.CondSection1:
					goto IL_351;
				case DtdParser.ScanningFunction.CondSection2:
					goto IL_358;
				case DtdParser.ScanningFunction.CondSection3:
					goto IL_35F;
				case DtdParser.ScanningFunction.SystemId:
					goto IL_327;
				case DtdParser.ScanningFunction.PublicId1:
					goto IL_32E;
				case DtdParser.ScanningFunction.PublicId2:
					goto IL_335;
				case DtdParser.ScanningFunction.ClosingTag:
					goto IL_366;
				case DtdParser.ScanningFunction.ParamEntitySpace:
					this.whitespaceSeen = true;
					this.scanningFunction = this.savedScanningFunction;
					continue;
				}
				break;
			}
			return DtdParser.Token.None;
			IL_294:
			return this.ScanNameExpected();
			IL_29B:
			return this.ScanQNameExpected();
			IL_2A2:
			return this.ScanNmtokenExpected();
			IL_2A9:
			return this.ScanSubsetContent();
			IL_2B0:
			return this.ScanDoctype1();
			IL_2B7:
			return this.ScanDoctype2();
			IL_2BE:
			return this.ScanElement1();
			IL_2C5:
			return this.ScanElement2();
			IL_2CC:
			return this.ScanElement3();
			IL_2D3:
			return this.ScanElement4();
			IL_2DA:
			return this.ScanElement5();
			IL_2E1:
			return this.ScanElement6();
			IL_2E8:
			return this.ScanElement7();
			IL_2EF:
			return this.ScanAttlist1();
			IL_2F6:
			return this.ScanAttlist2();
			IL_2FD:
			return this.ScanAttlist3();
			IL_304:
			return this.ScanAttlist4();
			IL_30B:
			return this.ScanAttlist5();
			IL_312:
			return this.ScanAttlist6();
			IL_319:
			return this.ScanAttlist7();
			IL_320:
			return this.ScanNotation1();
			IL_327:
			return this.ScanSystemId();
			IL_32E:
			return this.ScanPublicId1();
			IL_335:
			return this.ScanPublicId2();
			IL_33C:
			return this.ScanEntity1();
			IL_343:
			return this.ScanEntity2();
			IL_34A:
			return this.ScanEntity3();
			IL_351:
			return this.ScanCondSection1();
			IL_358:
			return this.ScanCondSection2();
			IL_35F:
			return this.ScanCondSection3();
			IL_366:
			return this.ScanClosingTag();
		}

		// Token: 0x060016C0 RID: 5824 RVA: 0x0007E1E8 File Offset: 0x0007C3E8
		private DtdParser.Token ScanSubsetContent()
		{
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c != '<')
				{
					if (c == ']')
					{
						if (this.charsUsed - this.curPos < 2 && !this.readerAdapter.IsEof)
						{
							goto IL_513;
						}
						if (this.chars[this.curPos + 1] != ']')
						{
							goto Block_40;
						}
						if (this.charsUsed - this.curPos < 3 && !this.readerAdapter.IsEof)
						{
							goto IL_513;
						}
						if (this.chars[this.curPos + 1] == ']' && this.chars[this.curPos + 2] == '>')
						{
							goto Block_43;
						}
					}
					if (this.charsUsed - this.curPos != 0)
					{
						this.Throw(this.curPos, "Expected DTD markup was not found.");
					}
				}
				else
				{
					c = this.chars[this.curPos + 1];
					if (c != '!')
					{
						if (c == '?')
						{
							goto IL_41B;
						}
						if (this.charsUsed - this.curPos >= 2)
						{
							goto Block_38;
						}
					}
					else
					{
						c = this.chars[this.curPos + 2];
						if (c <= 'A')
						{
							if (c != '-')
							{
								if (c == 'A')
								{
									if (this.charsUsed - this.curPos >= 9)
									{
										goto Block_22;
									}
									goto IL_513;
								}
							}
							else
							{
								if (this.chars[this.curPos + 3] == '-')
								{
									goto Block_35;
								}
								if (this.charsUsed - this.curPos >= 4)
								{
									this.Throw(this.curPos, "Expected DTD markup was not found.");
									goto IL_513;
								}
								goto IL_513;
							}
						}
						else if (c != 'E')
						{
							if (c != 'N')
							{
								if (c == '[')
								{
									goto IL_38A;
								}
							}
							else
							{
								if (this.charsUsed - this.curPos >= 10)
								{
									goto Block_28;
								}
								goto IL_513;
							}
						}
						else if (this.chars[this.curPos + 3] == 'L')
						{
							if (this.charsUsed - this.curPos >= 9)
							{
								break;
							}
							goto IL_513;
						}
						else if (this.chars[this.curPos + 3] == 'N')
						{
							if (this.charsUsed - this.curPos >= 8)
							{
								goto Block_17;
							}
							goto IL_513;
						}
						else
						{
							if (this.charsUsed - this.curPos >= 4)
							{
								goto Block_21;
							}
							goto IL_513;
						}
						if (this.charsUsed - this.curPos >= 3)
						{
							this.Throw(this.curPos + 2, "Expected DTD markup was not found.");
						}
					}
				}
				IL_513:
				if (this.ReadData() == 0)
				{
					this.Throw(this.charsUsed, "Incomplete DTD content.");
				}
			}
			if (this.chars[this.curPos + 4] != 'E' || this.chars[this.curPos + 5] != 'M' || this.chars[this.curPos + 6] != 'E' || this.chars[this.curPos + 7] != 'N' || this.chars[this.curPos + 8] != 'T')
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			this.curPos += 9;
			this.scanningFunction = DtdParser.ScanningFunction.QName;
			this.nextScaningFunction = DtdParser.ScanningFunction.Element1;
			return DtdParser.Token.ElementDecl;
			Block_17:
			if (this.chars[this.curPos + 4] != 'T' || this.chars[this.curPos + 5] != 'I' || this.chars[this.curPos + 6] != 'T' || this.chars[this.curPos + 7] != 'Y')
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			this.curPos += 8;
			this.scanningFunction = DtdParser.ScanningFunction.Entity1;
			return DtdParser.Token.EntityDecl;
			Block_21:
			this.Throw(this.curPos, "Expected DTD markup was not found.");
			return DtdParser.Token.None;
			Block_22:
			if (this.chars[this.curPos + 3] != 'T' || this.chars[this.curPos + 4] != 'T' || this.chars[this.curPos + 5] != 'L' || this.chars[this.curPos + 6] != 'I' || this.chars[this.curPos + 7] != 'S' || this.chars[this.curPos + 8] != 'T')
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			this.curPos += 9;
			this.scanningFunction = DtdParser.ScanningFunction.QName;
			this.nextScaningFunction = DtdParser.ScanningFunction.Attlist1;
			return DtdParser.Token.AttlistDecl;
			Block_28:
			if (this.chars[this.curPos + 3] != 'O' || this.chars[this.curPos + 4] != 'T' || this.chars[this.curPos + 5] != 'A' || this.chars[this.curPos + 6] != 'T' || this.chars[this.curPos + 7] != 'I' || this.chars[this.curPos + 8] != 'O' || this.chars[this.curPos + 9] != 'N')
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			this.curPos += 10;
			this.scanningFunction = DtdParser.ScanningFunction.Name;
			this.nextScaningFunction = DtdParser.ScanningFunction.Notation1;
			return DtdParser.Token.NotationDecl;
			IL_38A:
			this.curPos += 3;
			this.scanningFunction = DtdParser.ScanningFunction.CondSection1;
			return DtdParser.Token.CondSectionStart;
			Block_35:
			this.curPos += 4;
			return DtdParser.Token.Comment;
			IL_41B:
			this.curPos += 2;
			return DtdParser.Token.PI;
			Block_38:
			this.Throw(this.curPos, "Expected DTD markup was not found.");
			return DtdParser.Token.None;
			Block_40:
			this.curPos++;
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			return DtdParser.Token.RightBracket;
			Block_43:
			this.curPos += 3;
			return DtdParser.Token.CondSectionEnd;
		}

		// Token: 0x060016C1 RID: 5825 RVA: 0x0007E728 File Offset: 0x0007C928
		private DtdParser.Token ScanNameExpected()
		{
			this.ScanName();
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.Name;
		}

		// Token: 0x060016C2 RID: 5826 RVA: 0x0007E73E File Offset: 0x0007C93E
		private DtdParser.Token ScanQNameExpected()
		{
			this.ScanQName();
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.QName;
		}

		// Token: 0x060016C3 RID: 5827 RVA: 0x0007E754 File Offset: 0x0007C954
		private DtdParser.Token ScanNmtokenExpected()
		{
			this.ScanNmtoken();
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.Nmtoken;
		}

		// Token: 0x060016C4 RID: 5828 RVA: 0x0007E76C File Offset: 0x0007C96C
		private DtdParser.Token ScanDoctype1()
		{
			char c = this.chars[this.curPos];
			if (c <= 'P')
			{
				if (c == '>')
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
					return DtdParser.Token.GreaterThan;
				}
				if (c == 'P')
				{
					if (!this.EatPublicKeyword())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.Doctype2;
					this.scanningFunction = DtdParser.ScanningFunction.PublicId1;
					return DtdParser.Token.PUBLIC;
				}
			}
			else
			{
				if (c == 'S')
				{
					if (!this.EatSystemKeyword())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.Doctype2;
					this.scanningFunction = DtdParser.ScanningFunction.SystemId;
					return DtdParser.Token.SYSTEM;
				}
				if (c == '[')
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
					return DtdParser.Token.LeftBracket;
				}
			}
			this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
			return DtdParser.Token.None;
		}

		// Token: 0x060016C5 RID: 5829 RVA: 0x0007E848 File Offset: 0x0007CA48
		private DtdParser.Token ScanDoctype2()
		{
			char c = this.chars[this.curPos];
			if (c == '>')
			{
				this.curPos++;
				this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
				return DtdParser.Token.GreaterThan;
			}
			if (c == '[')
			{
				this.curPos++;
				this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
				return DtdParser.Token.LeftBracket;
			}
			this.Throw(this.curPos, "Expecting an internal subset or the end of the DOCTYPE declaration.");
			return DtdParser.Token.None;
		}

		// Token: 0x060016C6 RID: 5830 RVA: 0x0007E8B0 File Offset: 0x0007CAB0
		private DtdParser.Token ScanClosingTag()
		{
			if (this.chars[this.curPos] != '>')
			{
				this.ThrowUnexpectedToken(this.curPos, ">");
			}
			this.curPos++;
			this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
			return DtdParser.Token.GreaterThan;
		}

		// Token: 0x060016C7 RID: 5831 RVA: 0x0007E8EC File Offset: 0x0007CAEC
		private DtdParser.Token ScanElement1()
		{
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c != '(')
				{
					if (c != 'A')
					{
						if (c != 'E')
						{
							goto IL_10A;
						}
						if (this.charsUsed - this.curPos >= 5)
						{
							if (this.chars[this.curPos + 1] == 'M' && this.chars[this.curPos + 2] == 'P' && this.chars[this.curPos + 3] == 'T' && this.chars[this.curPos + 4] == 'Y')
							{
								goto Block_7;
							}
							goto IL_10A;
						}
					}
					else if (this.charsUsed - this.curPos >= 3)
					{
						if (this.chars[this.curPos + 1] == 'N' && this.chars[this.curPos + 2] == 'Y')
						{
							goto Block_10;
						}
						goto IL_10A;
					}
					IL_11B:
					if (this.ReadData() == 0)
					{
						this.Throw(this.curPos, "Incomplete DTD content.");
						continue;
					}
					continue;
					IL_10A:
					this.Throw(this.curPos, "Invalid content model.");
					goto IL_11B;
				}
				break;
			}
			this.scanningFunction = DtdParser.ScanningFunction.Element2;
			this.curPos++;
			return DtdParser.Token.LeftParen;
			Block_7:
			this.curPos += 5;
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			return DtdParser.Token.EMPTY;
			Block_10:
			this.curPos += 3;
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			return DtdParser.Token.ANY;
		}

		// Token: 0x060016C8 RID: 5832 RVA: 0x0007EA34 File Offset: 0x0007CC34
		private DtdParser.Token ScanElement2()
		{
			if (this.chars[this.curPos] == '#')
			{
				while (this.charsUsed - this.curPos < 7)
				{
					if (this.ReadData() == 0)
					{
						this.Throw(this.curPos, "Incomplete DTD content.");
					}
				}
				if (this.chars[this.curPos + 1] == 'P' && this.chars[this.curPos + 2] == 'C' && this.chars[this.curPos + 3] == 'D' && this.chars[this.curPos + 4] == 'A' && this.chars[this.curPos + 5] == 'T' && this.chars[this.curPos + 6] == 'A')
				{
					this.curPos += 7;
					this.scanningFunction = DtdParser.ScanningFunction.Element6;
					return DtdParser.Token.PCDATA;
				}
				this.Throw(this.curPos + 1, "Expecting 'PCDATA'.");
			}
			this.scanningFunction = DtdParser.ScanningFunction.Element3;
			return DtdParser.Token.None;
		}

		// Token: 0x060016C9 RID: 5833 RVA: 0x0007EB28 File Offset: 0x0007CD28
		private DtdParser.Token ScanElement3()
		{
			char c = this.chars[this.curPos];
			if (c == '(')
			{
				this.curPos++;
				return DtdParser.Token.LeftParen;
			}
			if (c != '>')
			{
				this.ScanQName();
				this.scanningFunction = DtdParser.ScanningFunction.Element4;
				return DtdParser.Token.QName;
			}
			this.curPos++;
			this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
			return DtdParser.Token.GreaterThan;
		}

		// Token: 0x060016CA RID: 5834 RVA: 0x0007EB88 File Offset: 0x0007CD88
		private DtdParser.Token ScanElement4()
		{
			this.scanningFunction = DtdParser.ScanningFunction.Element5;
			char c = this.chars[this.curPos];
			DtdParser.Token result;
			if (c != '*')
			{
				if (c != '+')
				{
					if (c != '?')
					{
						return DtdParser.Token.None;
					}
					result = DtdParser.Token.QMark;
				}
				else
				{
					result = DtdParser.Token.Plus;
				}
			}
			else
			{
				result = DtdParser.Token.Star;
			}
			if (this.whitespaceSeen)
			{
				this.Throw(this.curPos, "White space not allowed before '?', '*', or '+'.");
			}
			this.curPos++;
			return result;
		}

		// Token: 0x060016CB RID: 5835 RVA: 0x0007EBF8 File Offset: 0x0007CDF8
		private DtdParser.Token ScanElement5()
		{
			char c = this.chars[this.curPos];
			if (c <= ',')
			{
				if (c == ')')
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.Element4;
					return DtdParser.Token.RightParen;
				}
				if (c == ',')
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.Element3;
					return DtdParser.Token.Comma;
				}
			}
			else
			{
				if (c == '>')
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
					return DtdParser.Token.GreaterThan;
				}
				if (c == '|')
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.Element3;
					return DtdParser.Token.Or;
				}
			}
			this.Throw(this.curPos, "Expecting '?', '*', or '+'.");
			return DtdParser.Token.None;
		}

		// Token: 0x060016CC RID: 5836 RVA: 0x0007ECA4 File Offset: 0x0007CEA4
		private DtdParser.Token ScanElement6()
		{
			char c = this.chars[this.curPos];
			if (c == ')')
			{
				this.curPos++;
				this.scanningFunction = DtdParser.ScanningFunction.Element7;
				return DtdParser.Token.RightParen;
			}
			if (c != '|')
			{
				this.ThrowUnexpectedToken(this.curPos, ")", "|");
				return DtdParser.Token.None;
			}
			this.curPos++;
			this.nextScaningFunction = DtdParser.ScanningFunction.Element6;
			this.scanningFunction = DtdParser.ScanningFunction.QName;
			return DtdParser.Token.Or;
		}

		// Token: 0x060016CD RID: 5837 RVA: 0x0007ED1C File Offset: 0x0007CF1C
		private DtdParser.Token ScanElement7()
		{
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			if (this.chars[this.curPos] == '*' && !this.whitespaceSeen)
			{
				this.curPos++;
				return DtdParser.Token.Star;
			}
			return DtdParser.Token.None;
		}

		// Token: 0x060016CE RID: 5838 RVA: 0x0007ED54 File Offset: 0x0007CF54
		private DtdParser.Token ScanAttlist1()
		{
			char c = this.chars[this.curPos];
			if (c == '>')
			{
				this.curPos++;
				this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
				return DtdParser.Token.GreaterThan;
			}
			if (!this.whitespaceSeen)
			{
				this.Throw(this.curPos, "'{0}' is an unexpected token. Expecting white space.", this.ParseUnexpectedToken(this.curPos));
			}
			this.ScanQName();
			this.scanningFunction = DtdParser.ScanningFunction.Attlist2;
			return DtdParser.Token.QName;
		}

		// Token: 0x060016CF RID: 5839 RVA: 0x0007EDC4 File Offset: 0x0007CFC4
		private DtdParser.Token ScanAttlist2()
		{
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c <= 'C')
				{
					if (c == '(')
					{
						break;
					}
					if (c != 'C')
					{
						goto IL_44E;
					}
					if (this.charsUsed - this.curPos >= 5)
					{
						goto Block_6;
					}
				}
				else if (c != 'E')
				{
					if (c != 'I')
					{
						if (c != 'N')
						{
							goto IL_44E;
						}
						if (this.charsUsed - this.curPos >= 8 || this.readerAdapter.IsEof)
						{
							c = this.chars[this.curPos + 1];
							if (c == 'M')
							{
								goto IL_390;
							}
							if (c == 'O')
							{
								goto Block_24;
							}
							this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
						}
					}
					else if (this.charsUsed - this.curPos >= 6)
					{
						goto Block_17;
					}
				}
				else if (this.charsUsed - this.curPos >= 9)
				{
					this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
					if (this.chars[this.curPos + 1] != 'N' || this.chars[this.curPos + 2] != 'T' || this.chars[this.curPos + 3] != 'I' || this.chars[this.curPos + 4] != 'T')
					{
						this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
					}
					c = this.chars[this.curPos + 5];
					if (c == 'I')
					{
						goto IL_17C;
					}
					if (c == 'Y')
					{
						goto IL_1C3;
					}
					this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
				}
				IL_45F:
				if (this.ReadData() == 0)
				{
					this.Throw(this.curPos, "Incomplete DTD content.");
					continue;
				}
				continue;
				IL_44E:
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
				goto IL_45F;
			}
			this.curPos++;
			this.scanningFunction = DtdParser.ScanningFunction.Nmtoken;
			this.nextScaningFunction = DtdParser.ScanningFunction.Attlist5;
			return DtdParser.Token.LeftParen;
			Block_6:
			if (this.chars[this.curPos + 1] != 'D' || this.chars[this.curPos + 2] != 'A' || this.chars[this.curPos + 3] != 'T' || this.chars[this.curPos + 4] != 'A')
			{
				this.Throw(this.curPos, "Invalid attribute type.");
			}
			this.curPos += 5;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
			return DtdParser.Token.CDATA;
			IL_17C:
			if (this.chars[this.curPos + 6] != 'E' || this.chars[this.curPos + 7] != 'S')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			this.curPos += 8;
			return DtdParser.Token.ENTITIES;
			IL_1C3:
			this.curPos += 6;
			return DtdParser.Token.ENTITY;
			Block_17:
			this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
			if (this.chars[this.curPos + 1] != 'D')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			if (this.chars[this.curPos + 2] != 'R')
			{
				this.curPos += 2;
				return DtdParser.Token.ID;
			}
			if (this.chars[this.curPos + 3] != 'E' || this.chars[this.curPos + 4] != 'F')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			if (this.chars[this.curPos + 5] != 'S')
			{
				this.curPos += 5;
				return DtdParser.Token.IDREF;
			}
			this.curPos += 6;
			return DtdParser.Token.IDREFS;
			Block_24:
			if (this.chars[this.curPos + 2] != 'T' || this.chars[this.curPos + 3] != 'A' || this.chars[this.curPos + 4] != 'T' || this.chars[this.curPos + 5] != 'I' || this.chars[this.curPos + 6] != 'O' || this.chars[this.curPos + 7] != 'N')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			this.curPos += 8;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist3;
			return DtdParser.Token.NOTATION;
			IL_390:
			if (this.chars[this.curPos + 2] != 'T' || this.chars[this.curPos + 3] != 'O' || this.chars[this.curPos + 4] != 'K' || this.chars[this.curPos + 5] != 'E' || this.chars[this.curPos + 6] != 'N')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
			if (this.chars[this.curPos + 7] == 'S')
			{
				this.curPos += 8;
				return DtdParser.Token.NMTOKENS;
			}
			this.curPos += 7;
			return DtdParser.Token.NMTOKEN;
		}

		// Token: 0x060016D0 RID: 5840 RVA: 0x0007F250 File Offset: 0x0007D450
		private DtdParser.Token ScanAttlist3()
		{
			if (this.chars[this.curPos] == '(')
			{
				this.curPos++;
				this.scanningFunction = DtdParser.ScanningFunction.Name;
				this.nextScaningFunction = DtdParser.ScanningFunction.Attlist4;
				return DtdParser.Token.LeftParen;
			}
			this.ThrowUnexpectedToken(this.curPos, "(");
			return DtdParser.Token.None;
		}

		// Token: 0x060016D1 RID: 5841 RVA: 0x0007F2A4 File Offset: 0x0007D4A4
		private DtdParser.Token ScanAttlist4()
		{
			char c = this.chars[this.curPos];
			if (c == ')')
			{
				this.curPos++;
				this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
				return DtdParser.Token.RightParen;
			}
			if (c != '|')
			{
				this.ThrowUnexpectedToken(this.curPos, ")", "|");
				return DtdParser.Token.None;
			}
			this.curPos++;
			this.scanningFunction = DtdParser.ScanningFunction.Name;
			this.nextScaningFunction = DtdParser.ScanningFunction.Attlist4;
			return DtdParser.Token.Or;
		}

		// Token: 0x060016D2 RID: 5842 RVA: 0x0007F31C File Offset: 0x0007D51C
		private DtdParser.Token ScanAttlist5()
		{
			char c = this.chars[this.curPos];
			if (c == ')')
			{
				this.curPos++;
				this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
				return DtdParser.Token.RightParen;
			}
			if (c != '|')
			{
				this.ThrowUnexpectedToken(this.curPos, ")", "|");
				return DtdParser.Token.None;
			}
			this.curPos++;
			this.scanningFunction = DtdParser.ScanningFunction.Nmtoken;
			this.nextScaningFunction = DtdParser.ScanningFunction.Attlist5;
			return DtdParser.Token.Or;
		}

		// Token: 0x060016D3 RID: 5843 RVA: 0x0007F394 File Offset: 0x0007D594
		private DtdParser.Token ScanAttlist6()
		{
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c == '"')
				{
					break;
				}
				if (c != '#')
				{
					if (c == '\'')
					{
						break;
					}
					this.Throw(this.curPos, "Expecting an attribute type.");
				}
				else if (this.charsUsed - this.curPos >= 6)
				{
					c = this.chars[this.curPos + 1];
					if (c == 'F')
					{
						goto IL_1E1;
					}
					if (c != 'I')
					{
						if (c == 'R')
						{
							if (this.charsUsed - this.curPos >= 9)
							{
								goto Block_6;
							}
						}
						else
						{
							this.Throw(this.curPos, "Expecting an attribute type.");
						}
					}
					else if (this.charsUsed - this.curPos >= 8)
					{
						goto Block_13;
					}
				}
				if (this.ReadData() == 0)
				{
					this.Throw(this.curPos, "Incomplete DTD content.");
				}
			}
			this.ScanLiteral(DtdParser.LiteralType.AttributeValue);
			this.scanningFunction = DtdParser.ScanningFunction.Attlist1;
			return DtdParser.Token.Literal;
			Block_6:
			if (this.chars[this.curPos + 2] != 'E' || this.chars[this.curPos + 3] != 'Q' || this.chars[this.curPos + 4] != 'U' || this.chars[this.curPos + 5] != 'I' || this.chars[this.curPos + 6] != 'R' || this.chars[this.curPos + 7] != 'E' || this.chars[this.curPos + 8] != 'D')
			{
				this.Throw(this.curPos, "Expecting an attribute type.");
			}
			this.curPos += 9;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist1;
			return DtdParser.Token.REQUIRED;
			Block_13:
			if (this.chars[this.curPos + 2] != 'M' || this.chars[this.curPos + 3] != 'P' || this.chars[this.curPos + 4] != 'L' || this.chars[this.curPos + 5] != 'I' || this.chars[this.curPos + 6] != 'E' || this.chars[this.curPos + 7] != 'D')
			{
				this.Throw(this.curPos, "Expecting an attribute type.");
			}
			this.curPos += 8;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist1;
			return DtdParser.Token.IMPLIED;
			IL_1E1:
			if (this.chars[this.curPos + 2] != 'I' || this.chars[this.curPos + 3] != 'X' || this.chars[this.curPos + 4] != 'E' || this.chars[this.curPos + 5] != 'D')
			{
				this.Throw(this.curPos, "Expecting an attribute type.");
			}
			this.curPos += 6;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist7;
			return DtdParser.Token.FIXED;
		}

		// Token: 0x060016D4 RID: 5844 RVA: 0x0007F63C File Offset: 0x0007D83C
		private DtdParser.Token ScanAttlist7()
		{
			char c = this.chars[this.curPos];
			if (c == '"' || c == '\'')
			{
				this.ScanLiteral(DtdParser.LiteralType.AttributeValue);
				this.scanningFunction = DtdParser.ScanningFunction.Attlist1;
				return DtdParser.Token.Literal;
			}
			this.ThrowUnexpectedToken(this.curPos, "\"", "'");
			return DtdParser.Token.None;
		}

		// Token: 0x060016D5 RID: 5845 RVA: 0x0007F68C File Offset: 0x0007D88C
		private DtdParser.Token ScanLiteral(DtdParser.LiteralType literalType)
		{
			char c = this.chars[this.curPos];
			char value = (literalType == DtdParser.LiteralType.AttributeValue) ? ' ' : '\n';
			int num = this.currentEntityId;
			this.literalLineInfo.Set(this.LineNo, this.LinePos);
			this.curPos++;
			this.tokenStartPos = this.curPos;
			this.stringBuilder.Length = 0;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 128) == 0 || this.chars[this.curPos] == '%')
				{
					if (this.chars[this.curPos] == c && this.currentEntityId == num)
					{
						break;
					}
					int num2 = this.curPos - this.tokenStartPos;
					if (num2 > 0)
					{
						this.stringBuilder.Append(this.chars, this.tokenStartPos, num2);
						this.tokenStartPos = this.curPos;
					}
					char c2 = this.chars[this.curPos];
					if (c2 <= '\'')
					{
						switch (c2)
						{
						case '\t':
							if (literalType == DtdParser.LiteralType.AttributeValue && this.Normalize)
							{
								this.stringBuilder.Append(' ');
								this.tokenStartPos++;
							}
							this.curPos++;
							continue;
						case '\n':
							this.curPos++;
							if (this.Normalize)
							{
								this.stringBuilder.Append(value);
								this.tokenStartPos = this.curPos;
							}
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						case '\v':
						case '\f':
							goto IL_54D;
						case '\r':
							if (this.chars[this.curPos + 1] == '\n')
							{
								if (this.Normalize)
								{
									if (literalType == DtdParser.LiteralType.AttributeValue)
									{
										this.stringBuilder.Append(this.readerAdapter.IsEntityEolNormalized ? "  " : " ");
									}
									else
									{
										this.stringBuilder.Append(this.readerAdapter.IsEntityEolNormalized ? "\r\n" : "\n");
									}
									this.tokenStartPos = this.curPos + 2;
									this.SaveParsingBuffer();
									IDtdParserAdapter dtdParserAdapter = this.readerAdapter;
									int currentPosition = dtdParserAdapter.CurrentPosition;
									dtdParserAdapter.CurrentPosition = currentPosition + 1;
								}
								this.curPos += 2;
							}
							else
							{
								if (this.curPos + 1 == this.charsUsed)
								{
									goto IL_5CF;
								}
								this.curPos++;
								if (this.Normalize)
								{
									this.stringBuilder.Append(value);
									this.tokenStartPos = this.curPos;
								}
							}
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						default:
							switch (c2)
							{
							case '"':
							case '\'':
								break;
							case '#':
							case '$':
								goto IL_54D;
							case '%':
								if (literalType != DtdParser.LiteralType.EntityReplText)
								{
									this.curPos++;
									continue;
								}
								this.HandleEntityReference(true, true, literalType == DtdParser.LiteralType.AttributeValue);
								this.tokenStartPos = this.curPos;
								continue;
							case '&':
								if (literalType == DtdParser.LiteralType.SystemOrPublicID)
								{
									this.curPos++;
									continue;
								}
								if (this.curPos + 1 == this.charsUsed)
								{
									goto IL_5CF;
								}
								if (this.chars[this.curPos + 1] == '#')
								{
									this.SaveParsingBuffer();
									int num3 = this.readerAdapter.ParseNumericCharRef(this.SaveInternalSubsetValue ? this.internalSubsetValueSb : null);
									this.LoadParsingBuffer();
									this.stringBuilder.Append(this.chars, this.curPos, num3 - this.curPos);
									this.readerAdapter.CurrentPosition = num3;
									this.tokenStartPos = num3;
									this.curPos = num3;
									continue;
								}
								this.SaveParsingBuffer();
								if (literalType == DtdParser.LiteralType.AttributeValue)
								{
									int num4 = this.readerAdapter.ParseNamedCharRef(true, this.SaveInternalSubsetValue ? this.internalSubsetValueSb : null);
									this.LoadParsingBuffer();
									if (num4 >= 0)
									{
										this.stringBuilder.Append(this.chars, this.curPos, num4 - this.curPos);
										this.readerAdapter.CurrentPosition = num4;
										this.tokenStartPos = num4;
										this.curPos = num4;
										continue;
									}
									this.HandleEntityReference(false, true, true);
									this.tokenStartPos = this.curPos;
									continue;
								}
								else
								{
									int num5 = this.readerAdapter.ParseNamedCharRef(false, null);
									this.LoadParsingBuffer();
									if (num5 >= 0)
									{
										this.tokenStartPos = this.curPos;
										this.curPos = num5;
										continue;
									}
									this.stringBuilder.Append('&');
									this.curPos++;
									this.tokenStartPos = this.curPos;
									XmlQualifiedName entityName = this.ScanEntityName();
									this.VerifyEntityReference(entityName, false, false, false);
									continue;
								}
								break;
							default:
								goto IL_54D;
							}
							break;
						}
					}
					else
					{
						if (c2 == '<')
						{
							if (literalType == DtdParser.LiteralType.AttributeValue)
							{
								this.Throw(this.curPos, "'{0}', hexadecimal value {1}, is an invalid attribute character.", XmlException.BuildCharExceptionArgs('<', '\0'));
							}
							this.curPos++;
							continue;
						}
						if (c2 != '>')
						{
							goto IL_54D;
						}
					}
					this.curPos++;
					continue;
					IL_54D:
					if (this.curPos != this.charsUsed)
					{
						if (!XmlCharType.IsHighSurrogate((int)this.chars[this.curPos]))
						{
							goto IL_5B4;
						}
						if (this.curPos + 1 != this.charsUsed)
						{
							this.curPos++;
							if (XmlCharType.IsLowSurrogate((int)this.chars[this.curPos]))
							{
								this.curPos++;
								continue;
							}
							goto IL_5B4;
						}
					}
					IL_5CF:
					if ((this.readerAdapter.IsEof || this.ReadData() == 0) && (literalType == DtdParser.LiteralType.SystemOrPublicID || !this.HandleEntityEnd(true)))
					{
						this.Throw(this.curPos, "There is an unclosed literal string.");
					}
					this.tokenStartPos = this.curPos;
				}
				else
				{
					this.curPos++;
				}
			}
			if (this.stringBuilder.Length > 0)
			{
				this.stringBuilder.Append(this.chars, this.tokenStartPos, this.curPos - this.tokenStartPos);
			}
			this.curPos++;
			this.literalQuoteChar = c;
			return DtdParser.Token.Literal;
			IL_5B4:
			this.ThrowInvalidChar(this.chars, this.charsUsed, this.curPos);
			return DtdParser.Token.None;
		}

		// Token: 0x060016D6 RID: 5846 RVA: 0x0007FCAC File Offset: 0x0007DEAC
		private XmlQualifiedName ScanEntityName()
		{
			try
			{
				this.ScanName();
			}
			catch (XmlException ex)
			{
				this.Throw("An error occurred while parsing EntityName.", string.Empty, ex.LineNumber, ex.LinePosition);
			}
			if (this.chars[this.curPos] != ';')
			{
				this.ThrowUnexpectedToken(this.curPos, ";");
			}
			XmlQualifiedName nameQualified = this.GetNameQualified(false);
			this.curPos++;
			return nameQualified;
		}

		// Token: 0x060016D7 RID: 5847 RVA: 0x0007FD28 File Offset: 0x0007DF28
		private DtdParser.Token ScanNotation1()
		{
			char c = this.chars[this.curPos];
			if (c == 'P')
			{
				if (!this.EatPublicKeyword())
				{
					this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
				}
				this.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
				this.scanningFunction = DtdParser.ScanningFunction.PublicId1;
				return DtdParser.Token.PUBLIC;
			}
			if (c != 'S')
			{
				this.Throw(this.curPos, "Expecting a system identifier or a public identifier.");
				return DtdParser.Token.None;
			}
			if (!this.EatSystemKeyword())
			{
				this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
			}
			this.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
			this.scanningFunction = DtdParser.ScanningFunction.SystemId;
			return DtdParser.Token.SYSTEM;
		}

		// Token: 0x060016D8 RID: 5848 RVA: 0x0007FDBC File Offset: 0x0007DFBC
		private DtdParser.Token ScanSystemId()
		{
			if (this.chars[this.curPos] != '"' && this.chars[this.curPos] != '\'')
			{
				this.ThrowUnexpectedToken(this.curPos, "\"", "'");
			}
			this.ScanLiteral(DtdParser.LiteralType.SystemOrPublicID);
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.Literal;
		}

		// Token: 0x060016D9 RID: 5849 RVA: 0x0007FE18 File Offset: 0x0007E018
		private DtdParser.Token ScanEntity1()
		{
			if (this.chars[this.curPos] == '%')
			{
				this.curPos++;
				this.nextScaningFunction = DtdParser.ScanningFunction.Entity2;
				this.scanningFunction = DtdParser.ScanningFunction.Name;
				return DtdParser.Token.Percent;
			}
			this.ScanName();
			this.scanningFunction = DtdParser.ScanningFunction.Entity2;
			return DtdParser.Token.Name;
		}

		// Token: 0x060016DA RID: 5850 RVA: 0x0007FE68 File Offset: 0x0007E068
		private DtdParser.Token ScanEntity2()
		{
			char c = this.chars[this.curPos];
			if (c <= '\'')
			{
				if (c == '"' || c == '\'')
				{
					this.ScanLiteral(DtdParser.LiteralType.EntityReplText);
					this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
					return DtdParser.Token.Literal;
				}
			}
			else
			{
				if (c == 'P')
				{
					if (!this.EatPublicKeyword())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.Entity3;
					this.scanningFunction = DtdParser.ScanningFunction.PublicId1;
					return DtdParser.Token.PUBLIC;
				}
				if (c == 'S')
				{
					if (!this.EatSystemKeyword())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.Entity3;
					this.scanningFunction = DtdParser.ScanningFunction.SystemId;
					return DtdParser.Token.SYSTEM;
				}
			}
			this.Throw(this.curPos, "Expecting an external identifier or an entity value.");
			return DtdParser.Token.None;
		}

		// Token: 0x060016DB RID: 5851 RVA: 0x0007FF20 File Offset: 0x0007E120
		private DtdParser.Token ScanEntity3()
		{
			if (this.chars[this.curPos] == 'N')
			{
				while (this.charsUsed - this.curPos < 5)
				{
					if (this.ReadData() == 0)
					{
						goto IL_9A;
					}
				}
				if (this.chars[this.curPos + 1] == 'D' && this.chars[this.curPos + 2] == 'A' && this.chars[this.curPos + 3] == 'T' && this.chars[this.curPos + 4] == 'A')
				{
					this.curPos += 5;
					this.scanningFunction = DtdParser.ScanningFunction.Name;
					this.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
					return DtdParser.Token.NData;
				}
			}
			IL_9A:
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			return DtdParser.Token.None;
		}

		// Token: 0x060016DC RID: 5852 RVA: 0x0007FFD4 File Offset: 0x0007E1D4
		private DtdParser.Token ScanPublicId1()
		{
			if (this.chars[this.curPos] != '"' && this.chars[this.curPos] != '\'')
			{
				this.ThrowUnexpectedToken(this.curPos, "\"", "'");
			}
			this.ScanLiteral(DtdParser.LiteralType.SystemOrPublicID);
			this.scanningFunction = DtdParser.ScanningFunction.PublicId2;
			return DtdParser.Token.Literal;
		}

		// Token: 0x060016DD RID: 5853 RVA: 0x0008002C File Offset: 0x0007E22C
		private DtdParser.Token ScanPublicId2()
		{
			if (this.chars[this.curPos] != '"' && this.chars[this.curPos] != '\'')
			{
				this.scanningFunction = this.nextScaningFunction;
				return DtdParser.Token.None;
			}
			this.ScanLiteral(DtdParser.LiteralType.SystemOrPublicID);
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.Literal;
		}

		// Token: 0x060016DE RID: 5854 RVA: 0x00080080 File Offset: 0x0007E280
		private DtdParser.Token ScanCondSection1()
		{
			if (this.chars[this.curPos] != 'I')
			{
				this.Throw(this.curPos, "Conditional sections must specify the keyword 'IGNORE' or 'INCLUDE'.");
			}
			this.curPos++;
			for (;;)
			{
				if (this.charsUsed - this.curPos >= 5)
				{
					char c = this.chars[this.curPos];
					if (c == 'G')
					{
						goto IL_121;
					}
					if (c != 'N')
					{
						goto IL_1AA;
					}
					if (this.charsUsed - this.curPos >= 6)
					{
						break;
					}
				}
				if (this.ReadData() == 0)
				{
					this.Throw(this.curPos, "Incomplete DTD content.");
				}
			}
			if (this.chars[this.curPos + 1] == 'C' && this.chars[this.curPos + 2] == 'L' && this.chars[this.curPos + 3] == 'U' && this.chars[this.curPos + 4] == 'D' && this.chars[this.curPos + 5] == 'E' && !this.xmlCharType.IsNameSingleChar(this.chars[this.curPos + 6]))
			{
				this.nextScaningFunction = DtdParser.ScanningFunction.SubsetContent;
				this.scanningFunction = DtdParser.ScanningFunction.CondSection2;
				this.curPos += 6;
				return DtdParser.Token.INCLUDE;
			}
			goto IL_1AA;
			IL_121:
			if (this.chars[this.curPos + 1] == 'N' && this.chars[this.curPos + 2] == 'O' && this.chars[this.curPos + 3] == 'R' && this.chars[this.curPos + 4] == 'E' && !this.xmlCharType.IsNameSingleChar(this.chars[this.curPos + 5]))
			{
				this.nextScaningFunction = DtdParser.ScanningFunction.CondSection3;
				this.scanningFunction = DtdParser.ScanningFunction.CondSection2;
				this.curPos += 5;
				return DtdParser.Token.IGNORE;
			}
			IL_1AA:
			this.Throw(this.curPos - 1, "Conditional sections must specify the keyword 'IGNORE' or 'INCLUDE'.");
			return DtdParser.Token.None;
		}

		// Token: 0x060016DF RID: 5855 RVA: 0x0008026D File Offset: 0x0007E46D
		private DtdParser.Token ScanCondSection2()
		{
			if (this.chars[this.curPos] != '[')
			{
				this.ThrowUnexpectedToken(this.curPos, "[");
			}
			this.curPos++;
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.LeftBracket;
		}

		// Token: 0x060016E0 RID: 5856 RVA: 0x000802B0 File Offset: 0x0007E4B0
		private DtdParser.Token ScanCondSection3()
		{
			int num = 0;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 64) == 0 || this.chars[this.curPos] == ']')
				{
					char c = this.chars[this.curPos];
					if (c <= '&')
					{
						switch (c)
						{
						case '\t':
							break;
						case '\n':
							this.curPos++;
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						case '\v':
						case '\f':
							goto IL_21A;
						case '\r':
							if (this.chars[this.curPos + 1] == '\n')
							{
								this.curPos += 2;
							}
							else
							{
								if (this.curPos + 1 >= this.charsUsed && !this.readerAdapter.IsEof)
								{
									goto IL_29C;
								}
								this.curPos++;
							}
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						default:
							if (c != '"' && c != '&')
							{
								goto IL_21A;
							}
							break;
						}
					}
					else if (c != '\'')
					{
						if (c != '<')
						{
							if (c != ']')
							{
								goto IL_21A;
							}
							if (this.charsUsed - this.curPos < 3)
							{
								goto IL_29C;
							}
							if (this.chars[this.curPos + 1] != ']' || this.chars[this.curPos + 2] != '>')
							{
								this.curPos++;
								continue;
							}
							if (num > 0)
							{
								num--;
								this.curPos += 3;
								continue;
							}
							break;
						}
						else
						{
							if (this.charsUsed - this.curPos < 3)
							{
								goto IL_29C;
							}
							if (this.chars[this.curPos + 1] != '!' || this.chars[this.curPos + 2] != '[')
							{
								this.curPos++;
								continue;
							}
							num++;
							this.curPos += 3;
							continue;
						}
					}
					this.curPos++;
					continue;
					IL_21A:
					if (this.curPos != this.charsUsed)
					{
						if (!XmlCharType.IsHighSurrogate((int)this.chars[this.curPos]))
						{
							goto IL_281;
						}
						if (this.curPos + 1 != this.charsUsed)
						{
							this.curPos++;
							if (XmlCharType.IsLowSurrogate((int)this.chars[this.curPos]))
							{
								this.curPos++;
								continue;
							}
							goto IL_281;
						}
					}
					IL_29C:
					if (this.readerAdapter.IsEof || this.ReadData() == 0)
					{
						if (this.HandleEntityEnd(false))
						{
							continue;
						}
						this.Throw(this.curPos, "There is an unclosed conditional section.");
					}
					this.tokenStartPos = this.curPos;
				}
				else
				{
					this.curPos++;
				}
			}
			this.curPos += 3;
			this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
			return DtdParser.Token.CondSectionEnd;
			IL_281:
			this.ThrowInvalidChar(this.chars, this.charsUsed, this.curPos);
			return DtdParser.Token.None;
		}

		// Token: 0x060016E1 RID: 5857 RVA: 0x0008059B File Offset: 0x0007E79B
		private void ScanName()
		{
			this.ScanQName(false);
		}

		// Token: 0x060016E2 RID: 5858 RVA: 0x000805A4 File Offset: 0x0007E7A4
		private void ScanQName()
		{
			this.ScanQName(this.SupportNamespaces);
		}

		// Token: 0x060016E3 RID: 5859 RVA: 0x000805B4 File Offset: 0x0007E7B4
		private void ScanQName(bool isQName)
		{
			this.tokenStartPos = this.curPos;
			int num = -1;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 4) != 0 || this.chars[this.curPos] == ':')
				{
					this.curPos++;
				}
				else if (this.curPos + 1 >= this.charsUsed)
				{
					if (this.ReadDataInName())
					{
						continue;
					}
					this.Throw(this.curPos, "Unexpected end of file while parsing {0} has occurred.", "Name");
				}
				else
				{
					this.Throw(this.curPos, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(this.chars, this.charsUsed, this.curPos));
				}
				for (;;)
				{
					if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 8) != 0)
					{
						this.curPos++;
					}
					else if (this.chars[this.curPos] == ':')
					{
						if (isQName)
						{
							break;
						}
						this.curPos++;
					}
					else
					{
						if (this.curPos != this.charsUsed)
						{
							goto IL_173;
						}
						if (!this.ReadDataInName())
						{
							goto Block_9;
						}
					}
				}
				if (num != -1)
				{
					this.Throw(this.curPos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
				}
				num = this.curPos - this.tokenStartPos;
				this.curPos++;
			}
			Block_9:
			if (this.tokenStartPos == this.curPos)
			{
				this.Throw(this.curPos, "Unexpected end of file while parsing {0} has occurred.", "Name");
			}
			IL_173:
			this.colonPos = ((num == -1) ? -1 : (this.tokenStartPos + num));
		}

		// Token: 0x060016E4 RID: 5860 RVA: 0x0008074C File Offset: 0x0007E94C
		private bool ReadDataInName()
		{
			int num = this.curPos - this.tokenStartPos;
			this.curPos = this.tokenStartPos;
			bool result = this.ReadData() != 0;
			this.tokenStartPos = this.curPos;
			this.curPos += num;
			return result;
		}

		// Token: 0x060016E5 RID: 5861 RVA: 0x00080798 File Offset: 0x0007E998
		private void ScanNmtoken()
		{
			this.tokenStartPos = this.curPos;
			int num;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 8) != 0 || this.chars[this.curPos] == ':')
				{
					this.curPos++;
				}
				else
				{
					if (this.curPos < this.charsUsed)
					{
						break;
					}
					num = this.curPos - this.tokenStartPos;
					this.curPos = this.tokenStartPos;
					if (this.ReadData() == 0)
					{
						if (num > 0)
						{
							goto Block_5;
						}
						this.Throw(this.curPos, "Unexpected end of file while parsing {0} has occurred.", "NmToken");
					}
					this.tokenStartPos = this.curPos;
					this.curPos += num;
				}
			}
			if (this.curPos - this.tokenStartPos == 0)
			{
				this.Throw(this.curPos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(this.chars, this.charsUsed, this.curPos));
			}
			return;
			Block_5:
			this.tokenStartPos = this.curPos;
			this.curPos += num;
		}

		// Token: 0x060016E6 RID: 5862 RVA: 0x000808AC File Offset: 0x0007EAAC
		private bool EatPublicKeyword()
		{
			while (this.charsUsed - this.curPos < 6)
			{
				if (this.ReadData() == 0)
				{
					return false;
				}
			}
			if (this.chars[this.curPos + 1] != 'U' || this.chars[this.curPos + 2] != 'B' || this.chars[this.curPos + 3] != 'L' || this.chars[this.curPos + 4] != 'I' || this.chars[this.curPos + 5] != 'C')
			{
				return false;
			}
			this.curPos += 6;
			return true;
		}

		// Token: 0x060016E7 RID: 5863 RVA: 0x00080948 File Offset: 0x0007EB48
		private bool EatSystemKeyword()
		{
			while (this.charsUsed - this.curPos < 6)
			{
				if (this.ReadData() == 0)
				{
					return false;
				}
			}
			if (this.chars[this.curPos + 1] != 'Y' || this.chars[this.curPos + 2] != 'S' || this.chars[this.curPos + 3] != 'T' || this.chars[this.curPos + 4] != 'E' || this.chars[this.curPos + 5] != 'M')
			{
				return false;
			}
			this.curPos += 6;
			return true;
		}

		// Token: 0x060016E8 RID: 5864 RVA: 0x000809E4 File Offset: 0x0007EBE4
		private XmlQualifiedName GetNameQualified(bool canHavePrefix)
		{
			if (this.colonPos == -1)
			{
				return new XmlQualifiedName(this.nameTable.Add(this.chars, this.tokenStartPos, this.curPos - this.tokenStartPos));
			}
			if (canHavePrefix)
			{
				return new XmlQualifiedName(this.nameTable.Add(this.chars, this.colonPos + 1, this.curPos - this.colonPos - 1), this.nameTable.Add(this.chars, this.tokenStartPos, this.colonPos - this.tokenStartPos));
			}
			this.Throw(this.tokenStartPos, "'{0}' is an unqualified name and cannot contain the character ':'.", this.GetNameString());
			return null;
		}

		// Token: 0x060016E9 RID: 5865 RVA: 0x00080A91 File Offset: 0x0007EC91
		private string GetNameString()
		{
			return new string(this.chars, this.tokenStartPos, this.curPos - this.tokenStartPos);
		}

		// Token: 0x060016EA RID: 5866 RVA: 0x00080AB1 File Offset: 0x0007ECB1
		private string GetNmtokenString()
		{
			return this.GetNameString();
		}

		// Token: 0x060016EB RID: 5867 RVA: 0x00080AB9 File Offset: 0x0007ECB9
		private string GetValue()
		{
			if (this.stringBuilder.Length == 0)
			{
				return new string(this.chars, this.tokenStartPos, this.curPos - this.tokenStartPos - 1);
			}
			return this.stringBuilder.ToString();
		}

		// Token: 0x060016EC RID: 5868 RVA: 0x00080AF4 File Offset: 0x0007ECF4
		private string GetValueWithStrippedSpaces()
		{
			return DtdParser.StripSpaces((this.stringBuilder.Length == 0) ? new string(this.chars, this.tokenStartPos, this.curPos - this.tokenStartPos - 1) : this.stringBuilder.ToString());
		}

		// Token: 0x060016ED RID: 5869 RVA: 0x00080B40 File Offset: 0x0007ED40
		private int ReadData()
		{
			this.SaveParsingBuffer();
			int result = this.readerAdapter.ReadData();
			this.LoadParsingBuffer();
			return result;
		}

		// Token: 0x060016EE RID: 5870 RVA: 0x00080B59 File Offset: 0x0007ED59
		private void LoadParsingBuffer()
		{
			this.chars = this.readerAdapter.ParsingBuffer;
			this.charsUsed = this.readerAdapter.ParsingBufferLength;
			this.curPos = this.readerAdapter.CurrentPosition;
		}

		// Token: 0x060016EF RID: 5871 RVA: 0x00080B8E File Offset: 0x0007ED8E
		private void SaveParsingBuffer()
		{
			this.SaveParsingBuffer(this.curPos);
		}

		// Token: 0x060016F0 RID: 5872 RVA: 0x00080B9C File Offset: 0x0007ED9C
		private void SaveParsingBuffer(int internalSubsetValueEndPos)
		{
			if (this.SaveInternalSubsetValue)
			{
				int currentPosition = this.readerAdapter.CurrentPosition;
				if (internalSubsetValueEndPos - currentPosition > 0)
				{
					this.internalSubsetValueSb.Append(this.chars, currentPosition, internalSubsetValueEndPos - currentPosition);
				}
			}
			this.readerAdapter.CurrentPosition = this.curPos;
		}

		// Token: 0x060016F1 RID: 5873 RVA: 0x00080BEA File Offset: 0x0007EDEA
		private bool HandleEntityReference(bool paramEntity, bool inLiteral, bool inAttribute)
		{
			this.curPos++;
			return this.HandleEntityReference(this.ScanEntityName(), paramEntity, inLiteral, inAttribute);
		}

		// Token: 0x060016F2 RID: 5874 RVA: 0x00080C0C File Offset: 0x0007EE0C
		private bool HandleEntityReference(XmlQualifiedName entityName, bool paramEntity, bool inLiteral, bool inAttribute)
		{
			this.SaveParsingBuffer();
			if (paramEntity && this.ParsingInternalSubset && !this.ParsingTopLevelMarkup)
			{
				this.Throw(this.curPos - entityName.Name.Length - 1, "A parameter entity reference is not allowed in internal markup.");
			}
			SchemaEntity schemaEntity = this.VerifyEntityReference(entityName, paramEntity, true, inAttribute);
			if (schemaEntity == null)
			{
				return false;
			}
			if (schemaEntity.ParsingInProgress)
			{
				this.Throw(this.curPos - entityName.Name.Length - 1, paramEntity ? "Parameter entity '{0}' references itself." : "General entity '{0}' references itself.", entityName.Name);
			}
			int num;
			if (schemaEntity.IsExternal)
			{
				if (!this.readerAdapter.PushEntity(schemaEntity, out num))
				{
					return false;
				}
				this.externalEntitiesDepth++;
			}
			else
			{
				if (schemaEntity.Text.Length == 0)
				{
					return false;
				}
				if (!this.readerAdapter.PushEntity(schemaEntity, out num))
				{
					return false;
				}
			}
			this.currentEntityId = num;
			if (paramEntity && !inLiteral && this.scanningFunction != DtdParser.ScanningFunction.ParamEntitySpace)
			{
				this.savedScanningFunction = this.scanningFunction;
				this.scanningFunction = DtdParser.ScanningFunction.ParamEntitySpace;
			}
			this.LoadParsingBuffer();
			return true;
		}

		// Token: 0x060016F3 RID: 5875 RVA: 0x00080D18 File Offset: 0x0007EF18
		private bool HandleEntityEnd(bool inLiteral)
		{
			this.SaveParsingBuffer();
			IDtdEntityInfo dtdEntityInfo;
			if (!this.readerAdapter.PopEntity(out dtdEntityInfo, out this.currentEntityId))
			{
				return false;
			}
			this.LoadParsingBuffer();
			if (dtdEntityInfo == null)
			{
				if (this.scanningFunction == DtdParser.ScanningFunction.ParamEntitySpace)
				{
					this.scanningFunction = this.savedScanningFunction;
				}
				return false;
			}
			if (dtdEntityInfo.IsExternal)
			{
				this.externalEntitiesDepth--;
			}
			if (!inLiteral && this.scanningFunction != DtdParser.ScanningFunction.ParamEntitySpace)
			{
				this.savedScanningFunction = this.scanningFunction;
				this.scanningFunction = DtdParser.ScanningFunction.ParamEntitySpace;
			}
			return true;
		}

		// Token: 0x060016F4 RID: 5876 RVA: 0x00080D9C File Offset: 0x0007EF9C
		private SchemaEntity VerifyEntityReference(XmlQualifiedName entityName, bool paramEntity, bool mustBeDeclared, bool inAttribute)
		{
			SchemaEntity schemaEntity;
			if (paramEntity)
			{
				this.schemaInfo.ParameterEntities.TryGetValue(entityName, out schemaEntity);
			}
			else
			{
				this.schemaInfo.GeneralEntities.TryGetValue(entityName, out schemaEntity);
			}
			if (schemaEntity == null)
			{
				if (paramEntity)
				{
					if (this.validate)
					{
						this.SendValidationEvent(this.curPos - entityName.Name.Length - 1, XmlSeverityType.Error, "Reference to undeclared parameter entity '{0}'.", entityName.Name);
					}
				}
				else if (mustBeDeclared)
				{
					if (!this.ParsingInternalSubset)
					{
						if (this.validate)
						{
							this.SendValidationEvent(this.curPos - entityName.Name.Length - 1, XmlSeverityType.Error, "Reference to undeclared entity '{0}'.", entityName.Name);
						}
					}
					else
					{
						this.Throw(this.curPos - entityName.Name.Length - 1, "Reference to undeclared entity '{0}'.", entityName.Name);
					}
				}
				return null;
			}
			if (!schemaEntity.NData.IsEmpty)
			{
				this.Throw(this.curPos - entityName.Name.Length - 1, "Reference to unparsed entity '{0}'.", entityName.Name);
			}
			if (inAttribute && schemaEntity.IsExternal)
			{
				this.Throw(this.curPos - entityName.Name.Length - 1, "External entity '{0}' reference cannot appear in the attribute value.", entityName.Name);
			}
			return schemaEntity;
		}

		// Token: 0x060016F5 RID: 5877 RVA: 0x00080ED8 File Offset: 0x0007F0D8
		private void SendValidationEvent(int pos, XmlSeverityType severity, string code, string arg)
		{
			this.SendValidationEvent(severity, new XmlSchemaException(code, arg, this.BaseUriStr, this.LineNo, this.LinePos + (pos - this.curPos)));
		}

		// Token: 0x060016F6 RID: 5878 RVA: 0x00080F0F File Offset: 0x0007F10F
		private void SendValidationEvent(XmlSeverityType severity, string code, string arg)
		{
			this.SendValidationEvent(severity, new XmlSchemaException(code, arg, this.BaseUriStr, this.LineNo, this.LinePos));
		}

		// Token: 0x060016F7 RID: 5879 RVA: 0x00080F34 File Offset: 0x0007F134
		private void SendValidationEvent(XmlSeverityType severity, XmlSchemaException e)
		{
			IValidationEventHandling validationEventHandling = this.readerAdapterWithValidation.ValidationEventHandling;
			if (validationEventHandling != null)
			{
				validationEventHandling.SendEvent(e, severity);
			}
		}

		// Token: 0x060016F8 RID: 5880 RVA: 0x00080F58 File Offset: 0x0007F158
		private bool IsAttributeValueType(DtdParser.Token token)
		{
			return token >= DtdParser.Token.CDATA && token <= DtdParser.Token.NOTATION;
		}

		// Token: 0x17000490 RID: 1168
		// (get) Token: 0x060016F9 RID: 5881 RVA: 0x00080F67 File Offset: 0x0007F167
		private int LineNo
		{
			get
			{
				return this.readerAdapter.LineNo;
			}
		}

		// Token: 0x17000491 RID: 1169
		// (get) Token: 0x060016FA RID: 5882 RVA: 0x00080F74 File Offset: 0x0007F174
		private int LinePos
		{
			get
			{
				return this.curPos - this.readerAdapter.LineStartPosition;
			}
		}

		// Token: 0x17000492 RID: 1170
		// (get) Token: 0x060016FB RID: 5883 RVA: 0x00080F88 File Offset: 0x0007F188
		private string BaseUriStr
		{
			get
			{
				Uri baseUri = this.readerAdapter.BaseUri;
				if (!(baseUri != null))
				{
					return string.Empty;
				}
				return baseUri.ToString();
			}
		}

		// Token: 0x060016FC RID: 5884 RVA: 0x00080FB6 File Offset: 0x0007F1B6
		private void OnUnexpectedError()
		{
			this.Throw(this.curPos, "An internal error has occurred.");
		}

		// Token: 0x060016FD RID: 5885 RVA: 0x00080FC9 File Offset: 0x0007F1C9
		private void Throw(int curPos, string res)
		{
			this.Throw(curPos, res, string.Empty);
		}

		// Token: 0x060016FE RID: 5886 RVA: 0x00080FD8 File Offset: 0x0007F1D8
		private void Throw(int curPos, string res, string arg)
		{
			this.curPos = curPos;
			Uri baseUri = this.readerAdapter.BaseUri;
			this.readerAdapter.Throw(new XmlException(res, arg, this.LineNo, this.LinePos, (baseUri == null) ? null : baseUri.ToString()));
		}

		// Token: 0x060016FF RID: 5887 RVA: 0x00081028 File Offset: 0x0007F228
		private void Throw(int curPos, string res, string[] args)
		{
			this.curPos = curPos;
			Uri baseUri = this.readerAdapter.BaseUri;
			this.readerAdapter.Throw(new XmlException(res, args, this.LineNo, this.LinePos, (baseUri == null) ? null : baseUri.ToString()));
		}

		// Token: 0x06001700 RID: 5888 RVA: 0x00081078 File Offset: 0x0007F278
		private void Throw(string res, string arg, int lineNo, int linePos)
		{
			Uri baseUri = this.readerAdapter.BaseUri;
			this.readerAdapter.Throw(new XmlException(res, arg, lineNo, linePos, (baseUri == null) ? null : baseUri.ToString()));
		}

		// Token: 0x06001701 RID: 5889 RVA: 0x000810B8 File Offset: 0x0007F2B8
		private void ThrowInvalidChar(int pos, string data, int invCharPos)
		{
			this.Throw(pos, "'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(data, invCharPos));
		}

		// Token: 0x06001702 RID: 5890 RVA: 0x000810CD File Offset: 0x0007F2CD
		private void ThrowInvalidChar(char[] data, int length, int invCharPos)
		{
			this.Throw(invCharPos, "'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(data, length, invCharPos));
		}

		// Token: 0x06001703 RID: 5891 RVA: 0x000810E3 File Offset: 0x0007F2E3
		private void ThrowUnexpectedToken(int pos, string expectedToken)
		{
			this.ThrowUnexpectedToken(pos, expectedToken, null);
		}

		// Token: 0x06001704 RID: 5892 RVA: 0x000810F0 File Offset: 0x0007F2F0
		private void ThrowUnexpectedToken(int pos, string expectedToken1, string expectedToken2)
		{
			string text = this.ParseUnexpectedToken(pos);
			if (expectedToken2 != null)
			{
				this.Throw(this.curPos, "'{0}' is an unexpected token. The expected token is '{1}' or '{2}'.", new string[]
				{
					text,
					expectedToken1,
					expectedToken2
				});
				return;
			}
			this.Throw(this.curPos, "'{0}' is an unexpected token. The expected token is '{1}'.", new string[]
			{
				text,
				expectedToken1
			});
		}

		// Token: 0x06001705 RID: 5893 RVA: 0x0008114C File Offset: 0x0007F34C
		private string ParseUnexpectedToken(int startPos)
		{
			if (this.xmlCharType.IsNCNameSingleChar(this.chars[startPos]))
			{
				int num = startPos;
				while (this.xmlCharType.IsNCNameSingleChar(this.chars[num]))
				{
					num++;
				}
				int num2 = num - startPos;
				return new string(this.chars, startPos, (num2 > 0) ? num2 : 1);
			}
			return new string(this.chars, startPos, 1);
		}

		// Token: 0x06001706 RID: 5894 RVA: 0x000811B4 File Offset: 0x0007F3B4
		internal static string StripSpaces(string value)
		{
			int length = value.Length;
			if (length <= 0)
			{
				return string.Empty;
			}
			int num = 0;
			StringBuilder stringBuilder = null;
			while (value[num] == ' ')
			{
				num++;
				if (num == length)
				{
					return " ";
				}
			}
			int i;
			for (i = num; i < length; i++)
			{
				if (value[i] == ' ')
				{
					int num2 = i + 1;
					while (num2 < length && value[num2] == ' ')
					{
						num2++;
					}
					if (num2 == length)
					{
						if (stringBuilder == null)
						{
							return value.Substring(num, i - num);
						}
						stringBuilder.Append(value, num, i - num);
						return stringBuilder.ToString();
					}
					else if (num2 > i + 1)
					{
						if (stringBuilder == null)
						{
							stringBuilder = new StringBuilder(length);
						}
						stringBuilder.Append(value, num, i - num + 1);
						num = num2;
						i = num2 - 1;
					}
				}
			}
			if (stringBuilder != null)
			{
				if (i > num)
				{
					stringBuilder.Append(value, num, i - num);
				}
				return stringBuilder.ToString();
			}
			if (num != 0)
			{
				return value.Substring(num, length - num);
			}
			return value;
		}

		// Token: 0x06001707 RID: 5895 RVA: 0x0008129C File Offset: 0x0007F49C
		async Task<IDtdInfo> IDtdParser.ParseInternalDtdAsync(IDtdParserAdapter adapter, bool saveInternalSubset)
		{
			this.Initialize(adapter);
			await this.ParseAsync(saveInternalSubset).ConfigureAwait(false);
			return this.schemaInfo;
		}

		// Token: 0x06001708 RID: 5896 RVA: 0x000812F4 File Offset: 0x0007F4F4
		async Task<IDtdInfo> IDtdParser.ParseFreeFloatingDtdAsync(string baseUri, string docTypeName, string publicId, string systemId, string internalSubset, IDtdParserAdapter adapter)
		{
			this.InitializeFreeFloatingDtd(baseUri, docTypeName, publicId, systemId, internalSubset, adapter);
			await this.ParseAsync(false).ConfigureAwait(false);
			return this.schemaInfo;
		}

		// Token: 0x06001709 RID: 5897 RVA: 0x0008136C File Offset: 0x0007F56C
		private async Task ParseAsync(bool saveInternalSubset)
		{
			if (this.freeFloatingDtd)
			{
				await this.ParseFreeFloatingDtdAsync().ConfigureAwait(false);
			}
			else
			{
				await this.ParseInDocumentDtdAsync(saveInternalSubset).ConfigureAwait(false);
			}
			this.schemaInfo.Finish();
			if (this.validate && this.undeclaredNotations != null)
			{
				foreach (DtdParser.UndeclaredNotation undeclaredNotation in this.undeclaredNotations.Values)
				{
					DtdParser.UndeclaredNotation undeclaredNotation3;
					DtdParser.UndeclaredNotation undeclaredNotation2 = undeclaredNotation3 = undeclaredNotation;
					while (undeclaredNotation3 != null)
					{
						this.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("The '{0}' notation is not declared.", undeclaredNotation2.name, this.BaseUriStr, undeclaredNotation2.lineNo, undeclaredNotation2.linePos));
						undeclaredNotation3 = undeclaredNotation3.next;
					}
				}
			}
		}

		// Token: 0x0600170A RID: 5898 RVA: 0x000813BC File Offset: 0x0007F5BC
		private async Task ParseInDocumentDtdAsync(bool saveInternalSubset)
		{
			this.LoadParsingBuffer();
			this.scanningFunction = DtdParser.ScanningFunction.QName;
			this.nextScaningFunction = DtdParser.ScanningFunction.Doctype1;
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() != DtdParser.Token.QName)
			{
				this.OnUnexpectedError();
			}
			this.schemaInfo.DocTypeName = this.GetNameQualified(true);
			DtdParser.Token token = await this.GetTokenAsync(false).ConfigureAwait(false);
			if (token == DtdParser.Token.SYSTEM || token == DtdParser.Token.PUBLIC)
			{
				Tuple<string, string> tuple = await this.ParseExternalIdAsync(token, DtdParser.Token.DOCTYPE).ConfigureAwait(false);
				this.publicId = tuple.Item1;
				this.systemId = tuple.Item2;
				token = await this.GetTokenAsync(false).ConfigureAwait(false);
			}
			if (token != DtdParser.Token.GreaterThan)
			{
				if (token == DtdParser.Token.LeftBracket)
				{
					if (saveInternalSubset)
					{
						this.SaveParsingBuffer();
						this.internalSubsetValueSb = new StringBuilder();
					}
					await this.ParseInternalSubsetAsync().ConfigureAwait(false);
				}
				else
				{
					this.OnUnexpectedError();
				}
			}
			this.SaveParsingBuffer();
			if (this.systemId != null && this.systemId.Length > 0)
			{
				await this.ParseExternalSubsetAsync().ConfigureAwait(false);
			}
		}

		// Token: 0x0600170B RID: 5899 RVA: 0x0008140C File Offset: 0x0007F60C
		private async Task ParseFreeFloatingDtdAsync()
		{
			if (this.hasFreeFloatingInternalSubset)
			{
				this.LoadParsingBuffer();
				await this.ParseInternalSubsetAsync().ConfigureAwait(false);
				this.SaveParsingBuffer();
			}
			if (this.systemId != null && this.systemId.Length > 0)
			{
				await this.ParseExternalSubsetAsync().ConfigureAwait(false);
			}
		}

		// Token: 0x0600170C RID: 5900 RVA: 0x00081451 File Offset: 0x0007F651
		private Task ParseInternalSubsetAsync()
		{
			return this.ParseSubsetAsync();
		}

		// Token: 0x0600170D RID: 5901 RVA: 0x0008145C File Offset: 0x0007F65C
		private async Task ParseExternalSubsetAsync()
		{
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.readerAdapter.PushExternalSubsetAsync(this.systemId, this.publicId).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult())
			{
				Uri baseUri = this.readerAdapter.BaseUri;
				if (baseUri != null)
				{
					this.externalDtdBaseUri = baseUri.ToString();
				}
				this.externalEntitiesDepth++;
				this.LoadParsingBuffer();
				await this.ParseSubsetAsync().ConfigureAwait(false);
			}
		}

		// Token: 0x0600170E RID: 5902 RVA: 0x000814A4 File Offset: 0x0007F6A4
		private async Task ParseSubsetAsync()
		{
			for (;;)
			{
				DtdParser.Token token = await this.GetTokenAsync(false).ConfigureAwait(false);
				int startTagEntityId = this.currentEntityId;
				switch (token)
				{
				case DtdParser.Token.AttlistDecl:
					await this.ParseAttlistDeclAsync().ConfigureAwait(false);
					break;
				case DtdParser.Token.ElementDecl:
					await this.ParseElementDeclAsync().ConfigureAwait(false);
					break;
				case DtdParser.Token.EntityDecl:
					await this.ParseEntityDeclAsync().ConfigureAwait(false);
					break;
				case DtdParser.Token.NotationDecl:
					await this.ParseNotationDeclAsync().ConfigureAwait(false);
					break;
				case DtdParser.Token.Comment:
					await this.ParseCommentAsync().ConfigureAwait(false);
					break;
				case DtdParser.Token.PI:
					await this.ParsePIAsync().ConfigureAwait(false);
					break;
				case DtdParser.Token.CondSectionStart:
					if (this.ParsingInternalSubset)
					{
						this.Throw(this.curPos - 3, "A conditional section is not allowed in an internal subset.");
					}
					await this.ParseCondSectionAsync().ConfigureAwait(false);
					startTagEntityId = this.currentEntityId;
					break;
				case DtdParser.Token.CondSectionEnd:
					if (this.condSectionDepth > 0)
					{
						this.condSectionDepth--;
						if (this.validate && this.currentEntityId != this.condSectionEntityIds[this.condSectionDepth])
						{
							this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						}
					}
					else
					{
						this.Throw(this.curPos - 3, "']]>' is not expected.");
					}
					break;
				case DtdParser.Token.Eof:
					goto IL_55F;
				default:
					if (token == DtdParser.Token.RightBracket)
					{
						goto IL_475;
					}
					break;
				}
				if (this.currentEntityId != startTagEntityId)
				{
					if (this.validate)
					{
						this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
					}
					else if (!this.v1Compat)
					{
						this.Throw(this.curPos, "The parameter entity replacement text must nest properly within markup declarations.");
					}
				}
			}
			IL_475:
			if (this.ParsingInternalSubset)
			{
				if (this.condSectionDepth != 0)
				{
					this.Throw(this.curPos, "There is an unclosed conditional section.");
				}
				if (this.internalSubsetValueSb != null)
				{
					this.SaveParsingBuffer(this.curPos - 1);
					this.schemaInfo.InternalDtdSubset = this.internalSubsetValueSb.ToString();
					this.internalSubsetValueSb = null;
				}
				ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() != DtdParser.Token.GreaterThan)
				{
					this.ThrowUnexpectedToken(this.curPos, ">");
				}
			}
			else
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			return;
			IL_55F:
			if (this.ParsingInternalSubset && !this.freeFloatingDtd)
			{
				this.Throw(this.curPos, "Incomplete DTD content.");
			}
			if (this.condSectionDepth != 0)
			{
				this.Throw(this.curPos, "There is an unclosed conditional section.");
			}
		}

		// Token: 0x0600170F RID: 5903 RVA: 0x000814EC File Offset: 0x0007F6EC
		private async Task ParseAttlistDeclAsync()
		{
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() == DtdParser.Token.QName)
			{
				XmlQualifiedName nameQualified = this.GetNameQualified(true);
				SchemaElementDecl elementDecl;
				if (!this.schemaInfo.ElementDecls.TryGetValue(nameQualified, out elementDecl) && !this.schemaInfo.UndeclaredElementDecls.TryGetValue(nameQualified, out elementDecl))
				{
					elementDecl = new SchemaElementDecl(nameQualified, nameQualified.Namespace);
					this.schemaInfo.UndeclaredElementDecls.Add(nameQualified, elementDecl);
				}
				SchemaAttDef attrDef = null;
				DtdParser.Token token;
				for (;;)
				{
					token = await this.GetTokenAsync(false).ConfigureAwait(false);
					if (token != DtdParser.Token.QName)
					{
						break;
					}
					XmlQualifiedName nameQualified2 = this.GetNameQualified(true);
					attrDef = new SchemaAttDef(nameQualified2, nameQualified2.Namespace);
					attrDef.IsDeclaredInExternal = !this.ParsingInternalSubset;
					attrDef.LineNumber = this.LineNo;
					attrDef.LinePosition = this.LinePos - (this.curPos - this.tokenStartPos);
					bool attrDefAlreadyExists = elementDecl.GetAttDef(attrDef.Name) != null;
					await this.ParseAttlistTypeAsync(attrDef, elementDecl, attrDefAlreadyExists).ConfigureAwait(false);
					await this.ParseAttlistDefaultAsync(attrDef, attrDefAlreadyExists).ConfigureAwait(false);
					if (attrDef.Prefix.Length > 0 && attrDef.Prefix.Equals("xml"))
					{
						if (attrDef.Name.Name == "space")
						{
							if (this.v1Compat)
							{
								string text = attrDef.DefaultValueExpanded.Trim();
								if (text.Equals("preserve") || text.Equals("default"))
								{
									attrDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
								}
							}
							else
							{
								attrDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
								if (attrDef.TokenizedType != XmlTokenizedType.ENUMERATION)
								{
									this.Throw("Enumeration data type required.", string.Empty, attrDef.LineNumber, attrDef.LinePosition);
								}
								if (this.validate)
								{
									attrDef.CheckXmlSpace(this.readerAdapterWithValidation.ValidationEventHandling);
								}
							}
						}
						else if (attrDef.Name.Name == "lang")
						{
							attrDef.Reserved = SchemaAttDef.Reserve.XmlLang;
						}
					}
					if (!attrDefAlreadyExists)
					{
						elementDecl.AddAttDef(attrDef);
					}
				}
				if (token == DtdParser.Token.GreaterThan)
				{
					if (this.v1Compat && attrDef != null && attrDef.Prefix.Length > 0 && attrDef.Prefix.Equals("xml") && attrDef.Name.Name == "space")
					{
						attrDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
						if (attrDef.Datatype.TokenizedType != XmlTokenizedType.ENUMERATION)
						{
							this.Throw("Enumeration data type required.", string.Empty, attrDef.LineNumber, attrDef.LinePosition);
						}
						if (this.validate)
						{
							attrDef.CheckXmlSpace(this.readerAdapterWithValidation.ValidationEventHandling);
						}
					}
					return;
				}
			}
			this.OnUnexpectedError();
		}

		// Token: 0x06001710 RID: 5904 RVA: 0x00081534 File Offset: 0x0007F734
		private async Task ParseAttlistTypeAsync(SchemaAttDef attrDef, SchemaElementDecl elementDecl, bool ignoreErrors)
		{
			DtdParser.Token token2 = await this.GetTokenAsync(true).ConfigureAwait(false);
			DtdParser.Token token = token2;
			if (token != DtdParser.Token.CDATA)
			{
				elementDecl.HasNonCDataAttribute = true;
			}
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			if (this.IsAttributeValueType(token))
			{
				attrDef.TokenizedType = (XmlTokenizedType)token;
				attrDef.SchemaType = XmlSchemaType.GetBuiltInSimpleType(attrDef.Datatype.TypeCode);
				token2 = token;
				if (token2 != DtdParser.Token.ID)
				{
					if (token2 == DtdParser.Token.NOTATION)
					{
						if (this.validate)
						{
							if (elementDecl.IsNotationDeclared && !ignoreErrors)
							{
								this.SendValidationEvent(this.curPos - 8, XmlSeverityType.Error, "No element type can have more than one NOTATION attribute specified.", elementDecl.Name.ToString());
							}
							else
							{
								if (elementDecl.ContentValidator != null && elementDecl.ContentValidator.ContentType == XmlSchemaContentType.Empty && !ignoreErrors)
								{
									this.SendValidationEvent(this.curPos - 8, XmlSeverityType.Error, "An attribute of type NOTATION must not be declared on an element declared EMPTY.", elementDecl.Name.ToString());
								}
								elementDecl.IsNotationDeclared = true;
							}
						}
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						}
						if (configuredTaskAwaiter.GetResult() != DtdParser.Token.LeftParen)
						{
							goto IL_683;
						}
						configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						}
						if (configuredTaskAwaiter.GetResult() != DtdParser.Token.Name)
						{
							goto IL_683;
						}
						do
						{
							string nameString = this.GetNameString();
							if (!this.schemaInfo.Notations.ContainsKey(nameString))
							{
								this.AddUndeclaredNotation(nameString);
							}
							if (this.validate && !this.v1Compat && attrDef.Values != null && attrDef.Values.Contains(nameString) && !ignoreErrors)
							{
								this.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("'{0}' is a duplicate notation value.", nameString, this.BaseUriStr, this.LineNo, this.LinePos));
							}
							attrDef.AddValue(nameString);
							token2 = await this.GetTokenAsync(false).ConfigureAwait(false);
							if (token2 == DtdParser.Token.RightParen)
							{
								goto IL_46B;
							}
							if (token2 != DtdParser.Token.Or)
							{
								break;
							}
							configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								await configuredTaskAwaiter;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
							}
						}
						while (configuredTaskAwaiter.GetResult() == DtdParser.Token.Name);
						goto IL_683;
						IL_46B:;
					}
				}
				else
				{
					if (this.validate && elementDecl.IsIdDeclared)
					{
						SchemaAttDef attDef = elementDecl.GetAttDef(attrDef.Name);
						if ((attDef == null || attDef.Datatype.TokenizedType != XmlTokenizedType.ID) && !ignoreErrors)
						{
							this.SendValidationEvent(XmlSeverityType.Error, "The attribute of type ID is already declared on the '{0}' element.", elementDecl.Name.ToString());
						}
					}
					elementDecl.IsIdDeclared = true;
				}
				return;
			}
			if (token == DtdParser.Token.LeftParen)
			{
				attrDef.TokenizedType = XmlTokenizedType.ENUMERATION;
				attrDef.SchemaType = XmlSchemaType.GetBuiltInSimpleType(attrDef.Datatype.TypeCode);
				ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == DtdParser.Token.Nmtoken)
				{
					attrDef.AddValue(this.GetNameString());
					for (;;)
					{
						token2 = await this.GetTokenAsync(false).ConfigureAwait(false);
						if (token2 == DtdParser.Token.RightParen)
						{
							break;
						}
						if (token2 != DtdParser.Token.Or)
						{
							goto IL_683;
						}
						configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						}
						if (configuredTaskAwaiter.GetResult() != DtdParser.Token.Nmtoken)
						{
							goto IL_683;
						}
						string nmtokenString = this.GetNmtokenString();
						if (this.validate && !this.v1Compat && attrDef.Values != null && attrDef.Values.Contains(nmtokenString) && !ignoreErrors)
						{
							this.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("'{0}' is a duplicate enumeration value.", nmtokenString, this.BaseUriStr, this.LineNo, this.LinePos));
						}
						attrDef.AddValue(nmtokenString);
					}
					return;
				}
			}
			IL_683:
			this.OnUnexpectedError();
		}

		// Token: 0x06001711 RID: 5905 RVA: 0x00081594 File Offset: 0x0007F794
		private async Task ParseAttlistDefaultAsync(SchemaAttDef attrDef, bool ignoreErrors)
		{
			DtdParser.Token token = await this.GetTokenAsync(true).ConfigureAwait(false);
			switch (token)
			{
			case DtdParser.Token.REQUIRED:
				attrDef.Presence = SchemaDeclBase.Use.Required;
				return;
			case DtdParser.Token.IMPLIED:
				attrDef.Presence = SchemaDeclBase.Use.Implied;
				return;
			case DtdParser.Token.FIXED:
			{
				attrDef.Presence = SchemaDeclBase.Use.Fixed;
				ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() != DtdParser.Token.Literal)
				{
					goto IL_1E8;
				}
				break;
			}
			default:
				if (token != DtdParser.Token.Literal)
				{
					goto IL_1E8;
				}
				break;
			}
			if (this.validate && attrDef.Datatype.TokenizedType == XmlTokenizedType.ID && !ignoreErrors)
			{
				this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "An attribute of type ID must have a declared default of either #IMPLIED or #REQUIRED.", string.Empty);
			}
			if (attrDef.TokenizedType != XmlTokenizedType.CDATA)
			{
				attrDef.DefaultValueExpanded = this.GetValueWithStrippedSpaces();
			}
			else
			{
				attrDef.DefaultValueExpanded = this.GetValue();
			}
			attrDef.ValueLineNumber = this.literalLineInfo.lineNo;
			attrDef.ValueLinePosition = this.literalLineInfo.linePos + 1;
			DtdValidator.SetDefaultTypedValue(attrDef, this.readerAdapter);
			return;
			IL_1E8:
			this.OnUnexpectedError();
		}

		// Token: 0x06001712 RID: 5906 RVA: 0x000815EC File Offset: 0x0007F7EC
		private async Task ParseElementDeclAsync()
		{
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() == DtdParser.Token.QName)
			{
				SchemaElementDecl elementDecl = null;
				XmlQualifiedName nameQualified = this.GetNameQualified(true);
				if (this.schemaInfo.ElementDecls.TryGetValue(nameQualified, out elementDecl))
				{
					if (this.validate)
					{
						this.SendValidationEvent(this.curPos - nameQualified.Name.Length, XmlSeverityType.Error, "The '{0}' element has already been declared.", this.GetNameString());
					}
				}
				else
				{
					if (this.schemaInfo.UndeclaredElementDecls.TryGetValue(nameQualified, out elementDecl))
					{
						this.schemaInfo.UndeclaredElementDecls.Remove(nameQualified);
					}
					else
					{
						elementDecl = new SchemaElementDecl(nameQualified, nameQualified.Namespace);
					}
					this.schemaInfo.ElementDecls.Add(nameQualified, elementDecl);
				}
				elementDecl.IsDeclaredInExternal = !this.ParsingInternalSubset;
				DtdParser.Token token = await this.GetTokenAsync(true).ConfigureAwait(false);
				if (token != DtdParser.Token.LeftParen)
				{
					if (token != DtdParser.Token.ANY)
					{
						if (token != DtdParser.Token.EMPTY)
						{
							goto IL_466;
						}
						elementDecl.ContentValidator = ContentValidator.Empty;
					}
					else
					{
						elementDecl.ContentValidator = ContentValidator.Any;
					}
				}
				else
				{
					int startParenEntityId = this.currentEntityId;
					DtdParser.Token token2 = await this.GetTokenAsync(false).ConfigureAwait(false);
					if (token2 != DtdParser.Token.None)
					{
						if (token2 != DtdParser.Token.PCDATA)
						{
							goto IL_466;
						}
						ParticleContentValidator pcv = new ParticleContentValidator(XmlSchemaContentType.Mixed);
						pcv.Start();
						pcv.OpenGroup();
						await this.ParseElementMixedContentAsync(pcv, startParenEntityId).ConfigureAwait(false);
						elementDecl.ContentValidator = pcv.Finish(true);
					}
					else
					{
						ParticleContentValidator pcv2 = null;
						pcv2 = new ParticleContentValidator(XmlSchemaContentType.ElementOnly);
						pcv2.Start();
						pcv2.OpenGroup();
						await this.ParseElementOnlyContentAsync(pcv2, startParenEntityId).ConfigureAwait(false);
						elementDecl.ContentValidator = pcv2.Finish(true);
					}
				}
				configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() != DtdParser.Token.GreaterThan)
				{
					this.ThrowUnexpectedToken(this.curPos, ">");
				}
				return;
			}
			IL_466:
			this.OnUnexpectedError();
		}

		// Token: 0x06001713 RID: 5907 RVA: 0x00081634 File Offset: 0x0007F834
		private async Task ParseElementOnlyContentAsync(ParticleContentValidator pcv, int startParenEntityId)
		{
			Stack<DtdParser.ParseElementOnlyContent_LocalFrame> localFrames = new Stack<DtdParser.ParseElementOnlyContent_LocalFrame>();
			DtdParser.ParseElementOnlyContent_LocalFrame currentFrame = new DtdParser.ParseElementOnlyContent_LocalFrame(startParenEntityId);
			localFrames.Push(currentFrame);
			for (;;)
			{
				DtdParser.Token token = await this.GetTokenAsync(false).ConfigureAwait(false);
				if (token != DtdParser.Token.QName)
				{
					if (token == DtdParser.Token.LeftParen)
					{
						pcv.OpenGroup();
						currentFrame = new DtdParser.ParseElementOnlyContent_LocalFrame(this.currentEntityId);
						localFrames.Push(currentFrame);
						continue;
					}
					if (token != DtdParser.Token.GreaterThan)
					{
						goto IL_35B;
					}
					this.Throw(this.curPos, "Invalid content model.");
					goto IL_361;
				}
				else
				{
					pcv.AddName(this.GetNameQualified(true), null);
					await this.ParseHowManyAsync(pcv).ConfigureAwait(false);
				}
				IL_19D:
				token = await this.GetTokenAsync(false).ConfigureAwait(false);
				switch (token)
				{
				case DtdParser.Token.RightParen:
					pcv.CloseGroup();
					if (this.validate && this.currentEntityId != currentFrame.startParenEntityId)
					{
						this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
					}
					await this.ParseHowManyAsync(pcv).ConfigureAwait(false);
					break;
				case DtdParser.Token.GreaterThan:
					this.Throw(this.curPos, "Invalid content model.");
					break;
				case DtdParser.Token.Or:
					if (currentFrame.parsingSchema == DtdParser.Token.Comma)
					{
						this.Throw(this.curPos, "Invalid content model.");
					}
					pcv.AddChoice();
					currentFrame.parsingSchema = DtdParser.Token.Or;
					continue;
				default:
					if (token == DtdParser.Token.Comma)
					{
						if (currentFrame.parsingSchema == DtdParser.Token.Or)
						{
							this.Throw(this.curPos, "Invalid content model.");
						}
						pcv.AddSequence();
						currentFrame.parsingSchema = DtdParser.Token.Comma;
						continue;
					}
					goto IL_35B;
				}
				IL_361:
				localFrames.Pop();
				if (localFrames.Count > 0)
				{
					currentFrame = localFrames.Peek();
					goto IL_19D;
				}
				break;
				IL_35B:
				this.OnUnexpectedError();
				goto IL_361;
			}
		}

		// Token: 0x06001714 RID: 5908 RVA: 0x0008168C File Offset: 0x0007F88C
		private async Task ParseHowManyAsync(ParticleContentValidator pcv)
		{
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			switch (configuredTaskAwaiter.GetResult())
			{
			case DtdParser.Token.Star:
				pcv.AddStar();
				break;
			case DtdParser.Token.QMark:
				pcv.AddQMark();
				break;
			case DtdParser.Token.Plus:
				pcv.AddPlus();
				break;
			}
		}

		// Token: 0x06001715 RID: 5909 RVA: 0x000816DC File Offset: 0x0007F8DC
		private async Task ParseElementMixedContentAsync(ParticleContentValidator pcv, int startParenEntityId)
		{
			bool hasNames = false;
			int connectorEntityId = -1;
			int contentEntityId = this.currentEntityId;
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter;
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			for (;;)
			{
				DtdParser.Token token = await this.GetTokenAsync(false).ConfigureAwait(false);
				if (token == DtdParser.Token.RightParen)
				{
					break;
				}
				if (token == DtdParser.Token.Or)
				{
					if (!hasNames)
					{
						hasNames = true;
					}
					else
					{
						pcv.AddChoice();
					}
					if (this.validate)
					{
						connectorEntityId = this.currentEntityId;
						if (contentEntityId < connectorEntityId)
						{
							this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						}
					}
					configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == DtdParser.Token.QName)
					{
						XmlQualifiedName nameQualified = this.GetNameQualified(true);
						if (pcv.Exists(nameQualified) && this.validate)
						{
							this.SendValidationEvent(XmlSeverityType.Error, "The '{0}' element already exists in the content model.", nameQualified.ToString());
						}
						pcv.AddName(nameQualified, null);
						if (!this.validate)
						{
							continue;
						}
						contentEntityId = this.currentEntityId;
						if (contentEntityId < connectorEntityId)
						{
							this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
							continue;
						}
						continue;
					}
				}
				this.OnUnexpectedError();
			}
			pcv.CloseGroup();
			if (this.validate && this.currentEntityId != startParenEntityId)
			{
				this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
			}
			configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() == DtdParser.Token.Star && hasNames)
			{
				pcv.AddStar();
			}
			else if (hasNames)
			{
				this.ThrowUnexpectedToken(this.curPos, "*");
			}
		}

		// Token: 0x06001716 RID: 5910 RVA: 0x00081734 File Offset: 0x0007F934
		private async Task ParseEntityDeclAsync()
		{
			bool isParamEntity = false;
			SchemaEntity entity = null;
			DtdParser.Token token = await this.GetTokenAsync(true).ConfigureAwait(false);
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter;
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			if (token != DtdParser.Token.Name)
			{
				if (token != DtdParser.Token.Percent)
				{
					goto IL_531;
				}
				isParamEntity = true;
				configuredTaskAwaiter = this.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() != DtdParser.Token.Name)
				{
					goto IL_531;
				}
			}
			XmlQualifiedName nameQualified = this.GetNameQualified(false);
			entity = new SchemaEntity(nameQualified, isParamEntity);
			entity.BaseURI = this.BaseUriStr;
			entity.DeclaredURI = ((this.externalDtdBaseUri.Length == 0) ? this.documentBaseUri : this.externalDtdBaseUri);
			if (isParamEntity)
			{
				if (!this.schemaInfo.ParameterEntities.ContainsKey(nameQualified))
				{
					this.schemaInfo.ParameterEntities.Add(nameQualified, entity);
				}
			}
			else if (!this.schemaInfo.GeneralEntities.ContainsKey(nameQualified))
			{
				this.schemaInfo.GeneralEntities.Add(nameQualified, entity);
			}
			entity.DeclaredInExternal = !this.ParsingInternalSubset;
			entity.ParsingInProgress = true;
			DtdParser.Token token2 = await this.GetTokenAsync(true).ConfigureAwait(false);
			if (token2 - DtdParser.Token.PUBLIC > 1)
			{
				if (token2 != DtdParser.Token.Literal)
				{
					goto IL_531;
				}
				entity.Text = this.GetValue();
				entity.Line = this.literalLineInfo.lineNo;
				entity.Pos = this.literalLineInfo.linePos;
			}
			else
			{
				object obj = await this.ParseExternalIdAsync(token2, DtdParser.Token.EntityDecl).ConfigureAwait(false);
				string item = obj.Item1;
				string item2 = obj.Item2;
				entity.IsExternal = true;
				entity.Url = item2;
				entity.Pubid = item;
				configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == DtdParser.Token.NData)
				{
					if (isParamEntity)
					{
						this.ThrowUnexpectedToken(this.curPos - 5, ">");
					}
					if (!this.whitespaceSeen)
					{
						this.Throw(this.curPos - 5, "'{0}' is an unexpected token. Expecting white space.", "NDATA");
					}
					configuredTaskAwaiter = this.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() != DtdParser.Token.Name)
					{
						goto IL_531;
					}
					entity.NData = this.GetNameQualified(false);
					string name = entity.NData.Name;
					if (!this.schemaInfo.Notations.ContainsKey(name))
					{
						this.AddUndeclaredNotation(name);
					}
				}
			}
			configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() == DtdParser.Token.GreaterThan)
			{
				entity.ParsingInProgress = false;
				return;
			}
			IL_531:
			this.OnUnexpectedError();
		}

		// Token: 0x06001717 RID: 5911 RVA: 0x0008177C File Offset: 0x0007F97C
		private async Task ParseNotationDeclAsync()
		{
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() != DtdParser.Token.Name)
			{
				this.OnUnexpectedError();
			}
			XmlQualifiedName nameQualified = this.GetNameQualified(false);
			SchemaNotation notation = null;
			if (!this.schemaInfo.Notations.ContainsKey(nameQualified.Name))
			{
				if (this.undeclaredNotations != null)
				{
					this.undeclaredNotations.Remove(nameQualified.Name);
				}
				notation = new SchemaNotation(nameQualified);
				this.schemaInfo.Notations.Add(notation.Name.Name, notation);
			}
			else if (this.validate)
			{
				this.SendValidationEvent(this.curPos - nameQualified.Name.Length, XmlSeverityType.Error, "The notation '{0}' has already been declared.", nameQualified.Name);
			}
			DtdParser.Token token = await this.GetTokenAsync(true).ConfigureAwait(false);
			if (token == DtdParser.Token.SYSTEM || token == DtdParser.Token.PUBLIC)
			{
				object obj = await this.ParseExternalIdAsync(token, DtdParser.Token.NOTATION).ConfigureAwait(false);
				string item = obj.Item1;
				string item2 = obj.Item2;
				if (notation != null)
				{
					notation.SystemLiteral = item2;
					notation.Pubid = item;
				}
			}
			else
			{
				this.OnUnexpectedError();
			}
			configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() != DtdParser.Token.GreaterThan)
			{
				this.OnUnexpectedError();
			}
		}

		// Token: 0x06001718 RID: 5912 RVA: 0x000817C4 File Offset: 0x0007F9C4
		private async Task ParseCommentAsync()
		{
			this.SaveParsingBuffer();
			try
			{
				if (this.SaveInternalSubsetValue)
				{
					await this.readerAdapter.ParseCommentAsync(this.internalSubsetValueSb).ConfigureAwait(false);
					this.internalSubsetValueSb.Append("-->");
				}
				else
				{
					await this.readerAdapter.ParseCommentAsync(null).ConfigureAwait(false);
				}
			}
			catch (XmlException ex)
			{
				if (!(ex.ResString == "Unexpected end of file while parsing {0} has occurred.") || this.currentEntityId == 0)
				{
					throw;
				}
				this.SendValidationEvent(XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", null);
			}
			this.LoadParsingBuffer();
		}

		// Token: 0x06001719 RID: 5913 RVA: 0x0008180C File Offset: 0x0007FA0C
		private async Task ParsePIAsync()
		{
			this.SaveParsingBuffer();
			if (this.SaveInternalSubsetValue)
			{
				await this.readerAdapter.ParsePIAsync(this.internalSubsetValueSb).ConfigureAwait(false);
				this.internalSubsetValueSb.Append("?>");
			}
			else
			{
				await this.readerAdapter.ParsePIAsync(null).ConfigureAwait(false);
			}
			this.LoadParsingBuffer();
		}

		// Token: 0x0600171A RID: 5914 RVA: 0x00081854 File Offset: 0x0007FA54
		private async Task ParseCondSectionAsync()
		{
			int csEntityId = this.currentEntityId;
			DtdParser.Token token = await this.GetTokenAsync(false).ConfigureAwait(false);
			if (token != DtdParser.Token.IGNORE)
			{
				if (token == DtdParser.Token.INCLUDE)
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == DtdParser.Token.LeftBracket)
					{
						if (this.validate && csEntityId != this.currentEntityId)
						{
							this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						}
						if (this.validate)
						{
							if (this.condSectionEntityIds == null)
							{
								this.condSectionEntityIds = new int[2];
							}
							else if (this.condSectionEntityIds.Length == this.condSectionDepth)
							{
								int[] destinationArray = new int[this.condSectionEntityIds.Length * 2];
								Array.Copy(this.condSectionEntityIds, 0, destinationArray, 0, this.condSectionEntityIds.Length);
								this.condSectionEntityIds = destinationArray;
							}
							this.condSectionEntityIds[this.condSectionDepth] = csEntityId;
						}
						this.condSectionDepth++;
						return;
					}
				}
			}
			else
			{
				ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
				ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == DtdParser.Token.LeftBracket)
				{
					if (this.validate && csEntityId != this.currentEntityId)
					{
						this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
					}
					configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == DtdParser.Token.CondSectionEnd)
					{
						if (this.validate && csEntityId != this.currentEntityId)
						{
							this.SendValidationEvent(this.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
							return;
						}
						return;
					}
				}
			}
			this.OnUnexpectedError();
		}

		// Token: 0x0600171B RID: 5915 RVA: 0x0008189C File Offset: 0x0007FA9C
		private async Task<Tuple<string, string>> ParseExternalIdAsync(DtdParser.Token idTokenType, DtdParser.Token declType)
		{
			LineInfo keywordLineInfo = new LineInfo(this.LineNo, this.LinePos - 6);
			string publicId = null;
			string systemId = null;
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
			ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult() != DtdParser.Token.Literal)
			{
				this.ThrowUnexpectedToken(this.curPos, "\"", "'");
			}
			if (idTokenType == DtdParser.Token.SYSTEM)
			{
				systemId = this.GetValue();
				if (systemId.IndexOf('#') >= 0)
				{
					this.Throw(this.curPos - systemId.Length - 1, "Fragment identifier '{0}' cannot be part of the system identifier '{1}'.", new string[]
					{
						systemId.Substring(systemId.IndexOf('#')),
						systemId
					});
				}
				if (declType == DtdParser.Token.DOCTYPE && !this.freeFloatingDtd)
				{
					this.literalLineInfo.linePos = this.literalLineInfo.linePos + 1;
					this.readerAdapter.OnSystemId(systemId, keywordLineInfo, this.literalLineInfo);
				}
			}
			else
			{
				publicId = this.GetValue();
				int num = this.xmlCharType.IsPublicId(publicId);
				if (num >= 0)
				{
					this.ThrowInvalidChar(this.curPos - 1 - publicId.Length + num, publicId, num);
				}
				if (declType == DtdParser.Token.DOCTYPE && !this.freeFloatingDtd)
				{
					this.literalLineInfo.linePos = this.literalLineInfo.linePos + 1;
					this.readerAdapter.OnPublicId(publicId, keywordLineInfo, this.literalLineInfo);
					configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == DtdParser.Token.Literal)
					{
						if (!this.whitespaceSeen)
						{
							this.Throw("'{0}' is an unexpected token. Expecting white space.", new string(this.literalQuoteChar, 1), this.literalLineInfo.lineNo, this.literalLineInfo.linePos);
						}
						systemId = this.GetValue();
						this.literalLineInfo.linePos = this.literalLineInfo.linePos + 1;
						this.readerAdapter.OnSystemId(systemId, keywordLineInfo, this.literalLineInfo);
					}
					else
					{
						this.ThrowUnexpectedToken(this.curPos, "\"", "'");
					}
				}
				else
				{
					configuredTaskAwaiter = this.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == DtdParser.Token.Literal)
					{
						if (!this.whitespaceSeen)
						{
							this.Throw("'{0}' is an unexpected token. Expecting white space.", new string(this.literalQuoteChar, 1), this.literalLineInfo.lineNo, this.literalLineInfo.linePos);
						}
						systemId = this.GetValue();
					}
					else if (declType != DtdParser.Token.NOTATION)
					{
						this.ThrowUnexpectedToken(this.curPos, "\"", "'");
					}
				}
			}
			return new Tuple<string, string>(publicId, systemId);
		}

		// Token: 0x0600171C RID: 5916 RVA: 0x000818F4 File Offset: 0x0007FAF4
		private async Task<DtdParser.Token> GetTokenAsync(bool needWhiteSpace)
		{
			this.whitespaceSeen = false;
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c <= '\r')
				{
					if (c != '\0')
					{
						switch (c)
						{
						case '\t':
							goto IL_1BB;
						case '\n':
							this.whitespaceSeen = true;
							this.curPos++;
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						case '\r':
							this.whitespaceSeen = true;
							if (this.chars[this.curPos + 1] == '\n')
							{
								if (this.Normalize)
								{
									this.SaveParsingBuffer();
									IDtdParserAdapter dtdParserAdapter = this.readerAdapter;
									int currentPosition = dtdParserAdapter.CurrentPosition;
									dtdParserAdapter.CurrentPosition = currentPosition + 1;
								}
								this.curPos += 2;
							}
							else
							{
								if (this.curPos + 1 >= this.charsUsed && !this.readerAdapter.IsEof)
								{
									goto IL_CB6;
								}
								this.chars[this.curPos] = '\n';
								this.curPos++;
							}
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						}
						break;
					}
					if (this.curPos != this.charsUsed)
					{
						this.ThrowInvalidChar(this.chars, this.charsUsed, this.curPos);
						goto IL_CB6;
					}
					goto IL_CB6;
				}
				else if (c != ' ')
				{
					if (c != '%')
					{
						break;
					}
					if (this.charsUsed - this.curPos < 2)
					{
						goto IL_CB6;
					}
					if (this.xmlCharType.IsWhiteSpace(this.chars[this.curPos + 1]))
					{
						break;
					}
					if (this.IgnoreEntityReferences)
					{
						this.curPos++;
						continue;
					}
					await this.HandleEntityReferenceAsync(true, false, false).ConfigureAwait(false);
					continue;
				}
				IL_1BB:
				this.whitespaceSeen = true;
				this.curPos++;
				continue;
				IL_CB6:
				bool flag = this.readerAdapter.IsEof;
				if (!flag)
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					flag = (configuredTaskAwaiter.GetResult() == 0);
				}
				if (flag && !this.HandleEntityEnd(false))
				{
					if (this.scanningFunction == DtdParser.ScanningFunction.SubsetContent)
					{
						goto Block_21;
					}
					this.Throw(this.curPos, "Incomplete DTD content.");
				}
			}
			if (needWhiteSpace && !this.whitespaceSeen && this.scanningFunction != DtdParser.ScanningFunction.ParamEntitySpace)
			{
				this.Throw(this.curPos, "'{0}' is an unexpected token. Expecting white space.", this.ParseUnexpectedToken(this.curPos));
			}
			this.tokenStartPos = this.curPos;
			for (;;)
			{
				switch (this.scanningFunction)
				{
				case DtdParser.ScanningFunction.SubsetContent:
					goto IL_4B8;
				case DtdParser.ScanningFunction.Name:
					goto IL_36E;
				case DtdParser.ScanningFunction.QName:
					goto IL_3DC;
				case DtdParser.ScanningFunction.Nmtoken:
					goto IL_44A;
				case DtdParser.ScanningFunction.Doctype1:
					goto IL_526;
				case DtdParser.ScanningFunction.Doctype2:
					goto IL_594;
				case DtdParser.ScanningFunction.Element1:
					goto IL_5A0;
				case DtdParser.ScanningFunction.Element2:
					goto IL_60E;
				case DtdParser.ScanningFunction.Element3:
					goto IL_67C;
				case DtdParser.ScanningFunction.Element4:
					goto IL_6EA;
				case DtdParser.ScanningFunction.Element5:
					goto IL_6F6;
				case DtdParser.ScanningFunction.Element6:
					goto IL_702;
				case DtdParser.ScanningFunction.Element7:
					goto IL_70E;
				case DtdParser.ScanningFunction.Attlist1:
					goto IL_71A;
				case DtdParser.ScanningFunction.Attlist2:
					goto IL_789;
				case DtdParser.ScanningFunction.Attlist3:
					goto IL_7F8;
				case DtdParser.ScanningFunction.Attlist4:
					goto IL_804;
				case DtdParser.ScanningFunction.Attlist5:
					goto IL_810;
				case DtdParser.ScanningFunction.Attlist6:
					goto IL_81C;
				case DtdParser.ScanningFunction.Attlist7:
					goto IL_88B;
				case DtdParser.ScanningFunction.Entity1:
					goto IL_A53;
				case DtdParser.ScanningFunction.Entity2:
					goto IL_AC2;
				case DtdParser.ScanningFunction.Entity3:
					goto IL_B31;
				case DtdParser.ScanningFunction.Notation1:
					goto IL_897;
				case DtdParser.ScanningFunction.CondSection1:
					goto IL_BA0;
				case DtdParser.ScanningFunction.CondSection2:
					goto IL_C0F;
				case DtdParser.ScanningFunction.CondSection3:
					goto IL_C1B;
				case DtdParser.ScanningFunction.SystemId:
					goto IL_906;
				case DtdParser.ScanningFunction.PublicId1:
					goto IL_975;
				case DtdParser.ScanningFunction.PublicId2:
					goto IL_9E4;
				case DtdParser.ScanningFunction.ClosingTag:
					goto IL_C8A;
				case DtdParser.ScanningFunction.ParamEntitySpace:
					this.whitespaceSeen = true;
					this.scanningFunction = this.savedScanningFunction;
					continue;
				}
				break;
			}
			goto IL_CAE;
			IL_36E:
			return await this.ScanNameExpectedAsync().ConfigureAwait(false);
			IL_3DC:
			return await this.ScanQNameExpectedAsync().ConfigureAwait(false);
			IL_44A:
			return await this.ScanNmtokenExpectedAsync().ConfigureAwait(false);
			IL_4B8:
			return await this.ScanSubsetContentAsync().ConfigureAwait(false);
			IL_526:
			return await this.ScanDoctype1Async().ConfigureAwait(false);
			IL_594:
			return this.ScanDoctype2();
			IL_5A0:
			return await this.ScanElement1Async().ConfigureAwait(false);
			IL_60E:
			return await this.ScanElement2Async().ConfigureAwait(false);
			IL_67C:
			return await this.ScanElement3Async().ConfigureAwait(false);
			IL_6EA:
			return this.ScanElement4();
			IL_6F6:
			return this.ScanElement5();
			IL_702:
			return this.ScanElement6();
			IL_70E:
			return this.ScanElement7();
			IL_71A:
			return await this.ScanAttlist1Async().ConfigureAwait(false);
			IL_789:
			return await this.ScanAttlist2Async().ConfigureAwait(false);
			IL_7F8:
			return this.ScanAttlist3();
			IL_804:
			return this.ScanAttlist4();
			IL_810:
			return this.ScanAttlist5();
			IL_81C:
			return await this.ScanAttlist6Async().ConfigureAwait(false);
			IL_88B:
			return this.ScanAttlist7();
			IL_897:
			return await this.ScanNotation1Async().ConfigureAwait(false);
			IL_906:
			return await this.ScanSystemIdAsync().ConfigureAwait(false);
			IL_975:
			return await this.ScanPublicId1Async().ConfigureAwait(false);
			IL_9E4:
			return await this.ScanPublicId2Async().ConfigureAwait(false);
			IL_A53:
			return await this.ScanEntity1Async().ConfigureAwait(false);
			IL_AC2:
			return await this.ScanEntity2Async().ConfigureAwait(false);
			IL_B31:
			return await this.ScanEntity3Async().ConfigureAwait(false);
			IL_BA0:
			return await this.ScanCondSection1Async().ConfigureAwait(false);
			IL_C0F:
			return this.ScanCondSection2();
			IL_C1B:
			return await this.ScanCondSection3Async().ConfigureAwait(false);
			IL_C8A:
			return this.ScanClosingTag();
			IL_CAE:
			return DtdParser.Token.None;
			Block_21:
			return DtdParser.Token.Eof;
		}

		// Token: 0x0600171D RID: 5917 RVA: 0x00081944 File Offset: 0x0007FB44
		private async Task<DtdParser.Token> ScanSubsetContentAsync()
		{
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c != '<')
				{
					if (c == ']')
					{
						if (this.charsUsed - this.curPos < 2 && !this.readerAdapter.IsEof)
						{
							goto IL_55E;
						}
						if (this.chars[this.curPos + 1] != ']')
						{
							goto Block_40;
						}
						if (this.charsUsed - this.curPos < 3 && !this.readerAdapter.IsEof)
						{
							goto IL_55E;
						}
						if (this.chars[this.curPos + 1] == ']' && this.chars[this.curPos + 2] == '>')
						{
							goto Block_43;
						}
					}
					if (this.charsUsed - this.curPos != 0)
					{
						this.Throw(this.curPos, "Expected DTD markup was not found.");
					}
				}
				else
				{
					c = this.chars[this.curPos + 1];
					if (c != '!')
					{
						if (c == '?')
						{
							goto IL_452;
						}
						if (this.charsUsed - this.curPos >= 2)
						{
							goto Block_38;
						}
					}
					else
					{
						c = this.chars[this.curPos + 2];
						if (c <= 'A')
						{
							if (c != '-')
							{
								if (c == 'A')
								{
									if (this.charsUsed - this.curPos >= 9)
									{
										goto Block_22;
									}
									goto IL_55E;
								}
							}
							else
							{
								if (this.chars[this.curPos + 3] == '-')
								{
									goto Block_35;
								}
								if (this.charsUsed - this.curPos >= 4)
								{
									this.Throw(this.curPos, "Expected DTD markup was not found.");
									goto IL_55E;
								}
								goto IL_55E;
							}
						}
						else if (c != 'E')
						{
							if (c != 'N')
							{
								if (c == '[')
								{
									goto IL_3B7;
								}
							}
							else
							{
								if (this.charsUsed - this.curPos >= 10)
								{
									goto Block_28;
								}
								goto IL_55E;
							}
						}
						else if (this.chars[this.curPos + 3] == 'L')
						{
							if (this.charsUsed - this.curPos >= 9)
							{
								break;
							}
							goto IL_55E;
						}
						else if (this.chars[this.curPos + 3] == 'N')
						{
							if (this.charsUsed - this.curPos >= 8)
							{
								goto Block_17;
							}
							goto IL_55E;
						}
						else
						{
							if (this.charsUsed - this.curPos >= 4)
							{
								goto Block_21;
							}
							goto IL_55E;
						}
						if (this.charsUsed - this.curPos >= 3)
						{
							this.Throw(this.curPos + 2, "Expected DTD markup was not found.");
						}
					}
				}
				IL_55E:
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw(this.charsUsed, "Incomplete DTD content.");
				}
			}
			if (this.chars[this.curPos + 4] != 'E' || this.chars[this.curPos + 5] != 'M' || this.chars[this.curPos + 6] != 'E' || this.chars[this.curPos + 7] != 'N' || this.chars[this.curPos + 8] != 'T')
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			this.curPos += 9;
			this.scanningFunction = DtdParser.ScanningFunction.QName;
			this.nextScaningFunction = DtdParser.ScanningFunction.Element1;
			return DtdParser.Token.ElementDecl;
			Block_17:
			if (this.chars[this.curPos + 4] != 'T' || this.chars[this.curPos + 5] != 'I' || this.chars[this.curPos + 6] != 'T' || this.chars[this.curPos + 7] != 'Y')
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			this.curPos += 8;
			this.scanningFunction = DtdParser.ScanningFunction.Entity1;
			return DtdParser.Token.EntityDecl;
			Block_21:
			this.Throw(this.curPos, "Expected DTD markup was not found.");
			return DtdParser.Token.None;
			Block_22:
			if (this.chars[this.curPos + 3] != 'T' || this.chars[this.curPos + 4] != 'T' || this.chars[this.curPos + 5] != 'L' || this.chars[this.curPos + 6] != 'I' || this.chars[this.curPos + 7] != 'S' || this.chars[this.curPos + 8] != 'T')
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			this.curPos += 9;
			this.scanningFunction = DtdParser.ScanningFunction.QName;
			this.nextScaningFunction = DtdParser.ScanningFunction.Attlist1;
			return DtdParser.Token.AttlistDecl;
			Block_28:
			if (this.chars[this.curPos + 3] != 'O' || this.chars[this.curPos + 4] != 'T' || this.chars[this.curPos + 5] != 'A' || this.chars[this.curPos + 6] != 'T' || this.chars[this.curPos + 7] != 'I' || this.chars[this.curPos + 8] != 'O' || this.chars[this.curPos + 9] != 'N')
			{
				this.Throw(this.curPos, "Expected DTD markup was not found.");
			}
			this.curPos += 10;
			this.scanningFunction = DtdParser.ScanningFunction.Name;
			this.nextScaningFunction = DtdParser.ScanningFunction.Notation1;
			return DtdParser.Token.NotationDecl;
			IL_3B7:
			this.curPos += 3;
			this.scanningFunction = DtdParser.ScanningFunction.CondSection1;
			return DtdParser.Token.CondSectionStart;
			Block_35:
			this.curPos += 4;
			return DtdParser.Token.Comment;
			IL_452:
			this.curPos += 2;
			return DtdParser.Token.PI;
			Block_38:
			this.Throw(this.curPos, "Expected DTD markup was not found.");
			return DtdParser.Token.None;
			Block_40:
			this.curPos++;
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			return DtdParser.Token.RightBracket;
			Block_43:
			this.curPos += 3;
			return DtdParser.Token.CondSectionEnd;
		}

		// Token: 0x0600171E RID: 5918 RVA: 0x0008198C File Offset: 0x0007FB8C
		private async Task<DtdParser.Token> ScanNameExpectedAsync()
		{
			await this.ScanNameAsync().ConfigureAwait(false);
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.Name;
		}

		// Token: 0x0600171F RID: 5919 RVA: 0x000819D4 File Offset: 0x0007FBD4
		private async Task<DtdParser.Token> ScanQNameExpectedAsync()
		{
			await this.ScanQNameAsync().ConfigureAwait(false);
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.QName;
		}

		// Token: 0x06001720 RID: 5920 RVA: 0x00081A1C File Offset: 0x0007FC1C
		private async Task<DtdParser.Token> ScanNmtokenExpectedAsync()
		{
			await this.ScanNmtokenAsync().ConfigureAwait(false);
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.Nmtoken;
		}

		// Token: 0x06001721 RID: 5921 RVA: 0x00081A64 File Offset: 0x0007FC64
		private async Task<DtdParser.Token> ScanDoctype1Async()
		{
			char c = this.chars[this.curPos];
			if (c <= 'P')
			{
				if (c == '>')
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
					return DtdParser.Token.GreaterThan;
				}
				if (c == 'P')
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.EatPublicKeywordAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (!configuredTaskAwaiter.GetResult())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.Doctype2;
					this.scanningFunction = DtdParser.ScanningFunction.PublicId1;
					return DtdParser.Token.PUBLIC;
				}
			}
			else
			{
				if (c == 'S')
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.EatSystemKeywordAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (!configuredTaskAwaiter.GetResult())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.Doctype2;
					this.scanningFunction = DtdParser.ScanningFunction.SystemId;
					return DtdParser.Token.SYSTEM;
				}
				if (c == '[')
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
					return DtdParser.Token.LeftBracket;
				}
			}
			this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
			return DtdParser.Token.None;
		}

		// Token: 0x06001722 RID: 5922 RVA: 0x00081AAC File Offset: 0x0007FCAC
		private async Task<DtdParser.Token> ScanElement1Async()
		{
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c != '(')
				{
					if (c != 'A')
					{
						if (c != 'E')
						{
							goto IL_130;
						}
						if (this.charsUsed - this.curPos >= 5)
						{
							if (this.chars[this.curPos + 1] == 'M' && this.chars[this.curPos + 2] == 'P' && this.chars[this.curPos + 3] == 'T' && this.chars[this.curPos + 4] == 'Y')
							{
								goto Block_7;
							}
							goto IL_130;
						}
					}
					else if (this.charsUsed - this.curPos >= 3)
					{
						if (this.chars[this.curPos + 1] == 'N' && this.chars[this.curPos + 2] == 'Y')
						{
							goto Block_10;
						}
						goto IL_130;
					}
					IL_141:
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == 0)
					{
						this.Throw(this.curPos, "Incomplete DTD content.");
						continue;
					}
					continue;
					IL_130:
					this.Throw(this.curPos, "Invalid content model.");
					goto IL_141;
				}
				break;
			}
			this.scanningFunction = DtdParser.ScanningFunction.Element2;
			this.curPos++;
			return DtdParser.Token.LeftParen;
			Block_7:
			this.curPos += 5;
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			return DtdParser.Token.EMPTY;
			Block_10:
			this.curPos += 3;
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			return DtdParser.Token.ANY;
		}

		// Token: 0x06001723 RID: 5923 RVA: 0x00081AF4 File Offset: 0x0007FCF4
		private async Task<DtdParser.Token> ScanElement2Async()
		{
			if (this.chars[this.curPos] == '#')
			{
				while (this.charsUsed - this.curPos < 7)
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == 0)
					{
						this.Throw(this.curPos, "Incomplete DTD content.");
					}
				}
				if (this.chars[this.curPos + 1] == 'P' && this.chars[this.curPos + 2] == 'C' && this.chars[this.curPos + 3] == 'D' && this.chars[this.curPos + 4] == 'A' && this.chars[this.curPos + 5] == 'T' && this.chars[this.curPos + 6] == 'A')
				{
					this.curPos += 7;
					this.scanningFunction = DtdParser.ScanningFunction.Element6;
					return DtdParser.Token.PCDATA;
				}
				this.Throw(this.curPos + 1, "Expecting 'PCDATA'.");
			}
			this.scanningFunction = DtdParser.ScanningFunction.Element3;
			return DtdParser.Token.None;
		}

		// Token: 0x06001724 RID: 5924 RVA: 0x00081B3C File Offset: 0x0007FD3C
		private async Task<DtdParser.Token> ScanElement3Async()
		{
			char c = this.chars[this.curPos];
			DtdParser.Token result;
			if (c != '(')
			{
				if (c != '>')
				{
					await this.ScanQNameAsync().ConfigureAwait(false);
					this.scanningFunction = DtdParser.ScanningFunction.Element4;
					result = DtdParser.Token.QName;
				}
				else
				{
					this.curPos++;
					this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
					result = DtdParser.Token.GreaterThan;
				}
			}
			else
			{
				this.curPos++;
				result = DtdParser.Token.LeftParen;
			}
			return result;
		}

		// Token: 0x06001725 RID: 5925 RVA: 0x00081B84 File Offset: 0x0007FD84
		private async Task<DtdParser.Token> ScanAttlist1Async()
		{
			char c = this.chars[this.curPos];
			DtdParser.Token result;
			if (c == '>')
			{
				this.curPos++;
				this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
				result = DtdParser.Token.GreaterThan;
			}
			else
			{
				if (!this.whitespaceSeen)
				{
					this.Throw(this.curPos, "'{0}' is an unexpected token. Expecting white space.", this.ParseUnexpectedToken(this.curPos));
				}
				await this.ScanQNameAsync().ConfigureAwait(false);
				this.scanningFunction = DtdParser.ScanningFunction.Attlist2;
				result = DtdParser.Token.QName;
			}
			return result;
		}

		// Token: 0x06001726 RID: 5926 RVA: 0x00081BCC File Offset: 0x0007FDCC
		private async Task<DtdParser.Token> ScanAttlist2Async()
		{
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c <= 'C')
				{
					if (c == '(')
					{
						break;
					}
					if (c != 'C')
					{
						goto IL_494;
					}
					if (this.charsUsed - this.curPos >= 5)
					{
						goto Block_6;
					}
				}
				else if (c != 'E')
				{
					if (c != 'I')
					{
						if (c != 'N')
						{
							goto IL_494;
						}
						if (this.charsUsed - this.curPos >= 8 || this.readerAdapter.IsEof)
						{
							c = this.chars[this.curPos + 1];
							if (c == 'M')
							{
								goto IL_3CC;
							}
							if (c == 'O')
							{
								goto Block_24;
							}
							this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
						}
					}
					else if (this.charsUsed - this.curPos >= 6)
					{
						goto Block_17;
					}
				}
				else if (this.charsUsed - this.curPos >= 9)
				{
					this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
					if (this.chars[this.curPos + 1] != 'N' || this.chars[this.curPos + 2] != 'T' || this.chars[this.curPos + 3] != 'I' || this.chars[this.curPos + 4] != 'T')
					{
						this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
					}
					c = this.chars[this.curPos + 5];
					if (c == 'I')
					{
						goto IL_19A;
					}
					if (c == 'Y')
					{
						goto IL_1E6;
					}
					this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
				}
				IL_4A5:
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw(this.curPos, "Incomplete DTD content.");
					continue;
				}
				continue;
				IL_494:
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
				goto IL_4A5;
			}
			this.curPos++;
			this.scanningFunction = DtdParser.ScanningFunction.Nmtoken;
			this.nextScaningFunction = DtdParser.ScanningFunction.Attlist5;
			return DtdParser.Token.LeftParen;
			Block_6:
			if (this.chars[this.curPos + 1] != 'D' || this.chars[this.curPos + 2] != 'A' || this.chars[this.curPos + 3] != 'T' || this.chars[this.curPos + 4] != 'A')
			{
				this.Throw(this.curPos, "Invalid attribute type.");
			}
			this.curPos += 5;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
			return DtdParser.Token.CDATA;
			IL_19A:
			if (this.chars[this.curPos + 6] != 'E' || this.chars[this.curPos + 7] != 'S')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			this.curPos += 8;
			return DtdParser.Token.ENTITIES;
			IL_1E6:
			this.curPos += 6;
			return DtdParser.Token.ENTITY;
			Block_17:
			this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
			if (this.chars[this.curPos + 1] != 'D')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			if (this.chars[this.curPos + 2] != 'R')
			{
				this.curPos += 2;
				return DtdParser.Token.ID;
			}
			if (this.chars[this.curPos + 3] != 'E' || this.chars[this.curPos + 4] != 'F')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			if (this.chars[this.curPos + 5] != 'S')
			{
				this.curPos += 5;
				return DtdParser.Token.IDREF;
			}
			this.curPos += 6;
			return DtdParser.Token.IDREFS;
			Block_24:
			if (this.chars[this.curPos + 2] != 'T' || this.chars[this.curPos + 3] != 'A' || this.chars[this.curPos + 4] != 'T' || this.chars[this.curPos + 5] != 'I' || this.chars[this.curPos + 6] != 'O' || this.chars[this.curPos + 7] != 'N')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			this.curPos += 8;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist3;
			return DtdParser.Token.NOTATION;
			IL_3CC:
			if (this.chars[this.curPos + 2] != 'T' || this.chars[this.curPos + 3] != 'O' || this.chars[this.curPos + 4] != 'K' || this.chars[this.curPos + 5] != 'E' || this.chars[this.curPos + 6] != 'N')
			{
				this.Throw(this.curPos, "'{0}' is an invalid attribute type.");
			}
			this.scanningFunction = DtdParser.ScanningFunction.Attlist6;
			DtdParser.Token result;
			if (this.chars[this.curPos + 7] == 'S')
			{
				this.curPos += 8;
				result = DtdParser.Token.NMTOKENS;
			}
			else
			{
				this.curPos += 7;
				result = DtdParser.Token.NMTOKEN;
			}
			return result;
		}

		// Token: 0x06001727 RID: 5927 RVA: 0x00081C14 File Offset: 0x0007FE14
		private async Task<DtdParser.Token> ScanAttlist6Async()
		{
			for (;;)
			{
				char c = this.chars[this.curPos];
				if (c == '"')
				{
					break;
				}
				if (c != '#')
				{
					if (c == '\'')
					{
						break;
					}
					this.Throw(this.curPos, "Expecting an attribute type.");
				}
				else if (this.charsUsed - this.curPos >= 6)
				{
					c = this.chars[this.curPos + 1];
					if (c == 'F')
					{
						goto IL_26D;
					}
					if (c != 'I')
					{
						if (c == 'R')
						{
							if (this.charsUsed - this.curPos >= 9)
							{
								goto Block_6;
							}
						}
						else
						{
							this.Throw(this.curPos, "Expecting an attribute type.");
						}
					}
					else if (this.charsUsed - this.curPos >= 8)
					{
						goto Block_13;
					}
				}
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw(this.curPos, "Incomplete DTD content.");
				}
			}
			await this.ScanLiteralAsync(DtdParser.LiteralType.AttributeValue).ConfigureAwait(false);
			this.scanningFunction = DtdParser.ScanningFunction.Attlist1;
			return DtdParser.Token.Literal;
			Block_6:
			if (this.chars[this.curPos + 2] != 'E' || this.chars[this.curPos + 3] != 'Q' || this.chars[this.curPos + 4] != 'U' || this.chars[this.curPos + 5] != 'I' || this.chars[this.curPos + 6] != 'R' || this.chars[this.curPos + 7] != 'E' || this.chars[this.curPos + 8] != 'D')
			{
				this.Throw(this.curPos, "Expecting an attribute type.");
			}
			this.curPos += 9;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist1;
			return DtdParser.Token.REQUIRED;
			Block_13:
			if (this.chars[this.curPos + 2] != 'M' || this.chars[this.curPos + 3] != 'P' || this.chars[this.curPos + 4] != 'L' || this.chars[this.curPos + 5] != 'I' || this.chars[this.curPos + 6] != 'E' || this.chars[this.curPos + 7] != 'D')
			{
				this.Throw(this.curPos, "Expecting an attribute type.");
			}
			this.curPos += 8;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist1;
			return DtdParser.Token.IMPLIED;
			IL_26D:
			if (this.chars[this.curPos + 2] != 'I' || this.chars[this.curPos + 3] != 'X' || this.chars[this.curPos + 4] != 'E' || this.chars[this.curPos + 5] != 'D')
			{
				this.Throw(this.curPos, "Expecting an attribute type.");
			}
			this.curPos += 6;
			this.scanningFunction = DtdParser.ScanningFunction.Attlist7;
			return DtdParser.Token.FIXED;
		}

		// Token: 0x06001728 RID: 5928 RVA: 0x00081C5C File Offset: 0x0007FE5C
		private async Task<DtdParser.Token> ScanLiteralAsync(DtdParser.LiteralType literalType)
		{
			char quoteChar = this.chars[this.curPos];
			char replaceChar = (literalType == DtdParser.LiteralType.AttributeValue) ? ' ' : '\n';
			int startQuoteEntityId = this.currentEntityId;
			this.literalLineInfo.Set(this.LineNo, this.LinePos);
			this.curPos++;
			this.tokenStartPos = this.curPos;
			this.stringBuilder.Length = 0;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 128) == 0 || this.chars[this.curPos] == '%')
				{
					if (this.chars[this.curPos] == quoteChar && this.currentEntityId == startQuoteEntityId)
					{
						break;
					}
					int num = this.curPos - this.tokenStartPos;
					if (num > 0)
					{
						this.stringBuilder.Append(this.chars, this.tokenStartPos, num);
						this.tokenStartPos = this.curPos;
					}
					char c = this.chars[this.curPos];
					if (c <= '\'')
					{
						switch (c)
						{
						case '\t':
							if (literalType == DtdParser.LiteralType.AttributeValue && this.Normalize)
							{
								this.stringBuilder.Append(' ');
								this.tokenStartPos++;
							}
							this.curPos++;
							continue;
						case '\n':
							this.curPos++;
							if (this.Normalize)
							{
								this.stringBuilder.Append(replaceChar);
								this.tokenStartPos = this.curPos;
							}
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						case '\v':
						case '\f':
							goto IL_7BB;
						case '\r':
							if (this.chars[this.curPos + 1] == '\n')
							{
								if (this.Normalize)
								{
									if (literalType == DtdParser.LiteralType.AttributeValue)
									{
										this.stringBuilder.Append(this.readerAdapter.IsEntityEolNormalized ? "  " : " ");
									}
									else
									{
										this.stringBuilder.Append(this.readerAdapter.IsEntityEolNormalized ? "\r\n" : "\n");
									}
									this.tokenStartPos = this.curPos + 2;
									this.SaveParsingBuffer();
									IDtdParserAdapter dtdParserAdapter = this.readerAdapter;
									int currentPosition = dtdParserAdapter.CurrentPosition;
									dtdParserAdapter.CurrentPosition = currentPosition + 1;
								}
								this.curPos += 2;
							}
							else
							{
								if (this.curPos + 1 == this.charsUsed)
								{
									goto IL_842;
								}
								this.curPos++;
								if (this.Normalize)
								{
									this.stringBuilder.Append(replaceChar);
									this.tokenStartPos = this.curPos;
								}
							}
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						default:
							switch (c)
							{
							case '"':
							case '\'':
								break;
							case '#':
							case '$':
								goto IL_7BB;
							case '%':
								if (literalType != DtdParser.LiteralType.EntityReplText)
								{
									this.curPos++;
									continue;
								}
								await this.HandleEntityReferenceAsync(true, true, literalType == DtdParser.LiteralType.AttributeValue).ConfigureAwait(false);
								this.tokenStartPos = this.curPos;
								continue;
							case '&':
								if (literalType == DtdParser.LiteralType.SystemOrPublicID)
								{
									this.curPos++;
									continue;
								}
								if (this.curPos + 1 == this.charsUsed)
								{
									goto IL_842;
								}
								if (this.chars[this.curPos + 1] == '#')
								{
									this.SaveParsingBuffer();
									int num2 = await this.readerAdapter.ParseNumericCharRefAsync(this.SaveInternalSubsetValue ? this.internalSubsetValueSb : null).ConfigureAwait(false);
									this.LoadParsingBuffer();
									this.stringBuilder.Append(this.chars, this.curPos, num2 - this.curPos);
									this.readerAdapter.CurrentPosition = num2;
									this.tokenStartPos = num2;
									this.curPos = num2;
									continue;
								}
								this.SaveParsingBuffer();
								if (literalType == DtdParser.LiteralType.AttributeValue)
								{
									int num3 = await this.readerAdapter.ParseNamedCharRefAsync(true, this.SaveInternalSubsetValue ? this.internalSubsetValueSb : null).ConfigureAwait(false);
									this.LoadParsingBuffer();
									if (num3 >= 0)
									{
										this.stringBuilder.Append(this.chars, this.curPos, num3 - this.curPos);
										this.readerAdapter.CurrentPosition = num3;
										this.tokenStartPos = num3;
										this.curPos = num3;
										continue;
									}
									await this.HandleEntityReferenceAsync(false, true, true).ConfigureAwait(false);
									this.tokenStartPos = this.curPos;
									continue;
								}
								else
								{
									int num4 = await this.readerAdapter.ParseNamedCharRefAsync(false, null).ConfigureAwait(false);
									this.LoadParsingBuffer();
									if (num4 >= 0)
									{
										this.tokenStartPos = this.curPos;
										this.curPos = num4;
										continue;
									}
									this.stringBuilder.Append('&');
									this.curPos++;
									this.tokenStartPos = this.curPos;
									this.VerifyEntityReference(this.ScanEntityName(), false, false, false);
									continue;
								}
								break;
							default:
								goto IL_7BB;
							}
							break;
						}
					}
					else
					{
						if (c == '<')
						{
							if (literalType == DtdParser.LiteralType.AttributeValue)
							{
								this.Throw(this.curPos, "'{0}', hexadecimal value {1}, is an invalid attribute character.", XmlException.BuildCharExceptionArgs('<', '\0'));
							}
							this.curPos++;
							continue;
						}
						if (c != '>')
						{
							goto IL_7BB;
						}
					}
					this.curPos++;
					continue;
					IL_7BB:
					if (this.curPos != this.charsUsed)
					{
						if (!XmlCharType.IsHighSurrogate((int)this.chars[this.curPos]))
						{
							goto IL_822;
						}
						if (this.curPos + 1 != this.charsUsed)
						{
							this.curPos++;
							if (XmlCharType.IsLowSurrogate((int)this.chars[this.curPos]))
							{
								this.curPos++;
								continue;
							}
							goto IL_822;
						}
					}
					IL_842:
					bool flag = this.readerAdapter.IsEof;
					if (!flag)
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						}
						flag = (configuredTaskAwaiter.GetResult() == 0);
					}
					if (flag && (literalType == DtdParser.LiteralType.SystemOrPublicID || !this.HandleEntityEnd(true)))
					{
						this.Throw(this.curPos, "There is an unclosed literal string.");
					}
					this.tokenStartPos = this.curPos;
				}
				else
				{
					this.curPos++;
				}
			}
			if (this.stringBuilder.Length > 0)
			{
				this.stringBuilder.Append(this.chars, this.tokenStartPos, this.curPos - this.tokenStartPos);
			}
			this.curPos++;
			this.literalQuoteChar = quoteChar;
			return DtdParser.Token.Literal;
			IL_822:
			this.ThrowInvalidChar(this.chars, this.charsUsed, this.curPos);
			return DtdParser.Token.None;
		}

		// Token: 0x06001729 RID: 5929 RVA: 0x00081CAC File Offset: 0x0007FEAC
		private async Task<DtdParser.Token> ScanNotation1Async()
		{
			char c = this.chars[this.curPos];
			DtdParser.Token result;
			if (c != 'P')
			{
				if (c != 'S')
				{
					this.Throw(this.curPos, "Expecting a system identifier or a public identifier.");
					result = DtdParser.Token.None;
				}
				else
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.EatSystemKeywordAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (!configuredTaskAwaiter.GetResult())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
					this.scanningFunction = DtdParser.ScanningFunction.SystemId;
					result = DtdParser.Token.SYSTEM;
				}
			}
			else
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.EatPublicKeywordAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
				}
				this.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
				this.scanningFunction = DtdParser.ScanningFunction.PublicId1;
				result = DtdParser.Token.PUBLIC;
			}
			return result;
		}

		// Token: 0x0600172A RID: 5930 RVA: 0x00081CF4 File Offset: 0x0007FEF4
		private async Task<DtdParser.Token> ScanSystemIdAsync()
		{
			if (this.chars[this.curPos] != '"' && this.chars[this.curPos] != '\'')
			{
				this.ThrowUnexpectedToken(this.curPos, "\"", "'");
			}
			await this.ScanLiteralAsync(DtdParser.LiteralType.SystemOrPublicID).ConfigureAwait(false);
			this.scanningFunction = this.nextScaningFunction;
			return DtdParser.Token.Literal;
		}

		// Token: 0x0600172B RID: 5931 RVA: 0x00081D3C File Offset: 0x0007FF3C
		private async Task<DtdParser.Token> ScanEntity1Async()
		{
			DtdParser.Token result;
			if (this.chars[this.curPos] == '%')
			{
				this.curPos++;
				this.nextScaningFunction = DtdParser.ScanningFunction.Entity2;
				this.scanningFunction = DtdParser.ScanningFunction.Name;
				result = DtdParser.Token.Percent;
			}
			else
			{
				await this.ScanNameAsync().ConfigureAwait(false);
				this.scanningFunction = DtdParser.ScanningFunction.Entity2;
				result = DtdParser.Token.Name;
			}
			return result;
		}

		// Token: 0x0600172C RID: 5932 RVA: 0x00081D84 File Offset: 0x0007FF84
		private async Task<DtdParser.Token> ScanEntity2Async()
		{
			char c = this.chars[this.curPos];
			if (c <= '\'')
			{
				if (c == '"' || c == '\'')
				{
					await this.ScanLiteralAsync(DtdParser.LiteralType.EntityReplText).ConfigureAwait(false);
					this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
					return DtdParser.Token.Literal;
				}
			}
			else
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				if (c == 'P')
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.EatPublicKeywordAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (!configuredTaskAwaiter.GetResult())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.Entity3;
					this.scanningFunction = DtdParser.ScanningFunction.PublicId1;
					return DtdParser.Token.PUBLIC;
				}
				if (c == 'S')
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.EatSystemKeywordAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (!configuredTaskAwaiter.GetResult())
					{
						this.Throw(this.curPos, "Expecting external ID, '[' or '>'.");
					}
					this.nextScaningFunction = DtdParser.ScanningFunction.Entity3;
					this.scanningFunction = DtdParser.ScanningFunction.SystemId;
					return DtdParser.Token.SYSTEM;
				}
			}
			this.Throw(this.curPos, "Expecting an external identifier or an entity value.");
			return DtdParser.Token.None;
		}

		// Token: 0x0600172D RID: 5933 RVA: 0x00081DCC File Offset: 0x0007FFCC
		private async Task<DtdParser.Token> ScanEntity3Async()
		{
			if (this.chars[this.curPos] == 'N')
			{
				while (this.charsUsed - this.curPos < 5)
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == 0)
					{
						goto IL_10C;
					}
				}
				if (this.chars[this.curPos + 1] == 'D' && this.chars[this.curPos + 2] == 'A' && this.chars[this.curPos + 3] == 'T' && this.chars[this.curPos + 4] == 'A')
				{
					this.curPos += 5;
					this.scanningFunction = DtdParser.ScanningFunction.Name;
					this.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
					return DtdParser.Token.NData;
				}
			}
			IL_10C:
			this.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
			return DtdParser.Token.None;
		}

		// Token: 0x0600172E RID: 5934 RVA: 0x00081E14 File Offset: 0x00080014
		private async Task<DtdParser.Token> ScanPublicId1Async()
		{
			if (this.chars[this.curPos] != '"' && this.chars[this.curPos] != '\'')
			{
				this.ThrowUnexpectedToken(this.curPos, "\"", "'");
			}
			await this.ScanLiteralAsync(DtdParser.LiteralType.SystemOrPublicID).ConfigureAwait(false);
			this.scanningFunction = DtdParser.ScanningFunction.PublicId2;
			return DtdParser.Token.Literal;
		}

		// Token: 0x0600172F RID: 5935 RVA: 0x00081E5C File Offset: 0x0008005C
		private async Task<DtdParser.Token> ScanPublicId2Async()
		{
			DtdParser.Token result;
			if (this.chars[this.curPos] != '"' && this.chars[this.curPos] != '\'')
			{
				this.scanningFunction = this.nextScaningFunction;
				result = DtdParser.Token.None;
			}
			else
			{
				await this.ScanLiteralAsync(DtdParser.LiteralType.SystemOrPublicID).ConfigureAwait(false);
				this.scanningFunction = this.nextScaningFunction;
				result = DtdParser.Token.Literal;
			}
			return result;
		}

		// Token: 0x06001730 RID: 5936 RVA: 0x00081EA4 File Offset: 0x000800A4
		private async Task<DtdParser.Token> ScanCondSection1Async()
		{
			if (this.chars[this.curPos] != 'I')
			{
				this.Throw(this.curPos, "Conditional sections must specify the keyword 'IGNORE' or 'INCLUDE'.");
			}
			this.curPos++;
			for (;;)
			{
				if (this.charsUsed - this.curPos >= 5)
				{
					char c = this.chars[this.curPos];
					if (c == 'G')
					{
						goto IL_13A;
					}
					if (c != 'N')
					{
						goto IL_1C8;
					}
					if (this.charsUsed - this.curPos >= 6)
					{
						break;
					}
				}
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					this.Throw(this.curPos, "Incomplete DTD content.");
				}
			}
			if (this.chars[this.curPos + 1] == 'C' && this.chars[this.curPos + 2] == 'L' && this.chars[this.curPos + 3] == 'U' && this.chars[this.curPos + 4] == 'D' && this.chars[this.curPos + 5] == 'E' && !this.xmlCharType.IsNameSingleChar(this.chars[this.curPos + 6]))
			{
				this.nextScaningFunction = DtdParser.ScanningFunction.SubsetContent;
				this.scanningFunction = DtdParser.ScanningFunction.CondSection2;
				this.curPos += 6;
				return DtdParser.Token.INCLUDE;
			}
			goto IL_1C8;
			IL_13A:
			if (this.chars[this.curPos + 1] == 'N' && this.chars[this.curPos + 2] == 'O' && this.chars[this.curPos + 3] == 'R' && this.chars[this.curPos + 4] == 'E' && !this.xmlCharType.IsNameSingleChar(this.chars[this.curPos + 5]))
			{
				this.nextScaningFunction = DtdParser.ScanningFunction.CondSection3;
				this.scanningFunction = DtdParser.ScanningFunction.CondSection2;
				this.curPos += 5;
				return DtdParser.Token.IGNORE;
			}
			IL_1C8:
			this.Throw(this.curPos - 1, "Conditional sections must specify the keyword 'IGNORE' or 'INCLUDE'.");
			return DtdParser.Token.None;
		}

		// Token: 0x06001731 RID: 5937 RVA: 0x00081EEC File Offset: 0x000800EC
		private async Task<DtdParser.Token> ScanCondSection3Async()
		{
			int ignoreSectionDepth = 0;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 64) == 0 || this.chars[this.curPos] == ']')
				{
					char c = this.chars[this.curPos];
					if (c <= '&')
					{
						switch (c)
						{
						case '\t':
							break;
						case '\n':
							this.curPos++;
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						case '\v':
						case '\f':
							goto IL_259;
						case '\r':
							if (this.chars[this.curPos + 1] == '\n')
							{
								this.curPos += 2;
							}
							else
							{
								if (this.curPos + 1 >= this.charsUsed && !this.readerAdapter.IsEof)
								{
									goto IL_2E0;
								}
								this.curPos++;
							}
							this.readerAdapter.OnNewLine(this.curPos);
							continue;
						default:
							if (c != '"' && c != '&')
							{
								goto IL_259;
							}
							break;
						}
					}
					else if (c != '\'')
					{
						if (c != '<')
						{
							if (c != ']')
							{
								goto IL_259;
							}
							if (this.charsUsed - this.curPos < 3)
							{
								goto IL_2E0;
							}
							if (this.chars[this.curPos + 1] != ']' || this.chars[this.curPos + 2] != '>')
							{
								this.curPos++;
								continue;
							}
							if (ignoreSectionDepth > 0)
							{
								int num = ignoreSectionDepth;
								ignoreSectionDepth = num - 1;
								this.curPos += 3;
								continue;
							}
							break;
						}
						else
						{
							if (this.charsUsed - this.curPos < 3)
							{
								goto IL_2E0;
							}
							if (this.chars[this.curPos + 1] != '!' || this.chars[this.curPos + 2] != '[')
							{
								this.curPos++;
								continue;
							}
							int num = ignoreSectionDepth;
							ignoreSectionDepth = num + 1;
							this.curPos += 3;
							continue;
						}
					}
					this.curPos++;
					continue;
					IL_259:
					if (this.curPos != this.charsUsed)
					{
						if (!XmlCharType.IsHighSurrogate((int)this.chars[this.curPos]))
						{
							goto IL_2C0;
						}
						if (this.curPos + 1 != this.charsUsed)
						{
							this.curPos++;
							if (XmlCharType.IsLowSurrogate((int)this.chars[this.curPos]))
							{
								this.curPos++;
								continue;
							}
							goto IL_2C0;
						}
					}
					IL_2E0:
					bool flag = this.readerAdapter.IsEof;
					if (!flag)
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						}
						flag = (configuredTaskAwaiter.GetResult() == 0);
					}
					if (flag)
					{
						if (this.HandleEntityEnd(false))
						{
							continue;
						}
						this.Throw(this.curPos, "There is an unclosed conditional section.");
					}
					this.tokenStartPos = this.curPos;
				}
				else
				{
					this.curPos++;
				}
			}
			this.curPos += 3;
			this.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
			return DtdParser.Token.CondSectionEnd;
			IL_2C0:
			this.ThrowInvalidChar(this.chars, this.charsUsed, this.curPos);
			return DtdParser.Token.None;
		}

		// Token: 0x06001732 RID: 5938 RVA: 0x00081F31 File Offset: 0x00080131
		private Task ScanNameAsync()
		{
			return this.ScanQNameAsync(false);
		}

		// Token: 0x06001733 RID: 5939 RVA: 0x00081F3A File Offset: 0x0008013A
		private Task ScanQNameAsync()
		{
			return this.ScanQNameAsync(this.SupportNamespaces);
		}

		// Token: 0x06001734 RID: 5940 RVA: 0x00081F48 File Offset: 0x00080148
		private async Task ScanQNameAsync(bool isQName)
		{
			this.tokenStartPos = this.curPos;
			int colonOffset = -1;
			for (;;)
			{
				bool flag = false;
				if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 4) != 0 || this.chars[this.curPos] == ':')
				{
					this.curPos++;
				}
				else if (this.curPos + 1 >= this.charsUsed)
				{
					flag = true;
				}
				else
				{
					this.Throw(this.curPos, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(this.chars, this.charsUsed, this.curPos));
				}
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				if (flag)
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataInNameAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult())
					{
						continue;
					}
					this.Throw(this.curPos, "Unexpected end of file while parsing {0} has occurred.", "Name");
				}
				for (;;)
				{
					if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 8) != 0)
					{
						this.curPos++;
					}
					else if (this.chars[this.curPos] == ':')
					{
						if (isQName)
						{
							break;
						}
						this.curPos++;
					}
					else
					{
						if (this.curPos != this.charsUsed)
						{
							goto IL_270;
						}
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataInNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						}
						if (!configuredTaskAwaiter.GetResult())
						{
							goto Block_12;
						}
					}
				}
				if (colonOffset != -1)
				{
					this.Throw(this.curPos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
				}
				colonOffset = this.curPos - this.tokenStartPos;
				this.curPos++;
			}
			Block_12:
			if (this.tokenStartPos == this.curPos)
			{
				this.Throw(this.curPos, "Unexpected end of file while parsing {0} has occurred.", "Name");
			}
			IL_270:
			this.colonPos = ((colonOffset == -1) ? -1 : (this.tokenStartPos + colonOffset));
		}

		// Token: 0x06001735 RID: 5941 RVA: 0x00081F98 File Offset: 0x00080198
		private async Task<bool> ReadDataInNameAsync()
		{
			int offset = this.curPos - this.tokenStartPos;
			this.curPos = this.tokenStartPos;
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
			}
			bool result = configuredTaskAwaiter.GetResult() != 0;
			this.tokenStartPos = this.curPos;
			this.curPos += offset;
			return result;
		}

		// Token: 0x06001736 RID: 5942 RVA: 0x00081FE0 File Offset: 0x000801E0
		private async Task ScanNmtokenAsync()
		{
			this.tokenStartPos = this.curPos;
			int len;
			for (;;)
			{
				if ((this.xmlCharType.charProperties[(int)this.chars[this.curPos]] & 8) != 0 || this.chars[this.curPos] == ':')
				{
					this.curPos++;
				}
				else
				{
					if (this.curPos < this.charsUsed)
					{
						break;
					}
					len = this.curPos - this.tokenStartPos;
					this.curPos = this.tokenStartPos;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() == 0)
					{
						if (len > 0)
						{
							goto Block_6;
						}
						this.Throw(this.curPos, "Unexpected end of file while parsing {0} has occurred.", "NmToken");
					}
					this.tokenStartPos = this.curPos;
					this.curPos += len;
				}
			}
			if (this.curPos - this.tokenStartPos == 0)
			{
				this.Throw(this.curPos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(this.chars, this.charsUsed, this.curPos));
			}
			return;
			Block_6:
			this.tokenStartPos = this.curPos;
			this.curPos += len;
		}

		// Token: 0x06001737 RID: 5943 RVA: 0x00082028 File Offset: 0x00080228
		private async Task<bool> EatPublicKeywordAsync()
		{
			while (this.charsUsed - this.curPos < 6)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					return false;
				}
			}
			if (this.chars[this.curPos + 1] != 'U' || this.chars[this.curPos + 2] != 'B' || this.chars[this.curPos + 3] != 'L' || this.chars[this.curPos + 4] != 'I' || this.chars[this.curPos + 5] != 'C')
			{
				return false;
			}
			this.curPos += 6;
			return true;
		}

		// Token: 0x06001738 RID: 5944 RVA: 0x00082070 File Offset: 0x00080270
		private async Task<bool> EatSystemKeywordAsync()
		{
			while (this.charsUsed - this.curPos < 6)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult() == 0)
				{
					return false;
				}
			}
			if (this.chars[this.curPos + 1] != 'Y' || this.chars[this.curPos + 2] != 'S' || this.chars[this.curPos + 3] != 'T' || this.chars[this.curPos + 4] != 'E' || this.chars[this.curPos + 5] != 'M')
			{
				return false;
			}
			this.curPos += 6;
			return true;
		}

		// Token: 0x06001739 RID: 5945 RVA: 0x000820B8 File Offset: 0x000802B8
		private async Task<int> ReadDataAsync()
		{
			this.SaveParsingBuffer();
			int result = await this.readerAdapter.ReadDataAsync().ConfigureAwait(false);
			this.LoadParsingBuffer();
			return result;
		}

		// Token: 0x0600173A RID: 5946 RVA: 0x000820FD File Offset: 0x000802FD
		private Task<bool> HandleEntityReferenceAsync(bool paramEntity, bool inLiteral, bool inAttribute)
		{
			this.curPos++;
			return this.HandleEntityReferenceAsync(this.ScanEntityName(), paramEntity, inLiteral, inAttribute);
		}

		// Token: 0x0600173B RID: 5947 RVA: 0x0008211C File Offset: 0x0008031C
		private async Task<bool> HandleEntityReferenceAsync(XmlQualifiedName entityName, bool paramEntity, bool inLiteral, bool inAttribute)
		{
			this.SaveParsingBuffer();
			if (paramEntity && this.ParsingInternalSubset && !this.ParsingTopLevelMarkup)
			{
				this.Throw(this.curPos - entityName.Name.Length - 1, "A parameter entity reference is not allowed in internal markup.");
			}
			SchemaEntity entity = this.VerifyEntityReference(entityName, paramEntity, true, inAttribute);
			bool result;
			if (entity == null)
			{
				result = false;
			}
			else
			{
				if (entity.ParsingInProgress)
				{
					this.Throw(this.curPos - entityName.Name.Length - 1, paramEntity ? "Parameter entity '{0}' references itself." : "General entity '{0}' references itself.", entityName.Name);
				}
				int item;
				if (entity.IsExternal)
				{
					object obj = await this.readerAdapter.PushEntityAsync(entity).ConfigureAwait(false);
					item = obj.Item1;
					if (!obj.Item2)
					{
						return false;
					}
					this.externalEntitiesDepth++;
				}
				else
				{
					if (entity.Text.Length == 0)
					{
						return false;
					}
					object obj2 = await this.readerAdapter.PushEntityAsync(entity).ConfigureAwait(false);
					item = obj2.Item1;
					if (!obj2.Item2)
					{
						return false;
					}
				}
				this.currentEntityId = item;
				if (paramEntity && !inLiteral && this.scanningFunction != DtdParser.ScanningFunction.ParamEntitySpace)
				{
					this.savedScanningFunction = this.scanningFunction;
					this.scanningFunction = DtdParser.ScanningFunction.ParamEntitySpace;
				}
				this.LoadParsingBuffer();
				result = true;
			}
			return result;
		}

		// Token: 0x04000E36 RID: 3638
		private IDtdParserAdapter readerAdapter;

		// Token: 0x04000E37 RID: 3639
		private IDtdParserAdapterWithValidation readerAdapterWithValidation;

		// Token: 0x04000E38 RID: 3640
		private XmlNameTable nameTable;

		// Token: 0x04000E39 RID: 3641
		private SchemaInfo schemaInfo;

		// Token: 0x04000E3A RID: 3642
		private XmlCharType xmlCharType = XmlCharType.Instance;

		// Token: 0x04000E3B RID: 3643
		private string systemId = string.Empty;

		// Token: 0x04000E3C RID: 3644
		private string publicId = string.Empty;

		// Token: 0x04000E3D RID: 3645
		private bool normalize = true;

		// Token: 0x04000E3E RID: 3646
		private bool validate;

		// Token: 0x04000E3F RID: 3647
		private bool supportNamespaces = true;

		// Token: 0x04000E40 RID: 3648
		private bool v1Compat;

		// Token: 0x04000E41 RID: 3649
		private char[] chars;

		// Token: 0x04000E42 RID: 3650
		private int charsUsed;

		// Token: 0x04000E43 RID: 3651
		private int curPos;

		// Token: 0x04000E44 RID: 3652
		private DtdParser.ScanningFunction scanningFunction;

		// Token: 0x04000E45 RID: 3653
		private DtdParser.ScanningFunction nextScaningFunction;

		// Token: 0x04000E46 RID: 3654
		private DtdParser.ScanningFunction savedScanningFunction;

		// Token: 0x04000E47 RID: 3655
		private bool whitespaceSeen;

		// Token: 0x04000E48 RID: 3656
		private int tokenStartPos;

		// Token: 0x04000E49 RID: 3657
		private int colonPos;

		// Token: 0x04000E4A RID: 3658
		private StringBuilder internalSubsetValueSb;

		// Token: 0x04000E4B RID: 3659
		private int externalEntitiesDepth;

		// Token: 0x04000E4C RID: 3660
		private int currentEntityId;

		// Token: 0x04000E4D RID: 3661
		private bool freeFloatingDtd;

		// Token: 0x04000E4E RID: 3662
		private bool hasFreeFloatingInternalSubset;

		// Token: 0x04000E4F RID: 3663
		private StringBuilder stringBuilder;

		// Token: 0x04000E50 RID: 3664
		private int condSectionDepth;

		// Token: 0x04000E51 RID: 3665
		private LineInfo literalLineInfo = new LineInfo(0, 0);

		// Token: 0x04000E52 RID: 3666
		private char literalQuoteChar = '"';

		// Token: 0x04000E53 RID: 3667
		private string documentBaseUri = string.Empty;

		// Token: 0x04000E54 RID: 3668
		private string externalDtdBaseUri = string.Empty;

		// Token: 0x04000E55 RID: 3669
		private Dictionary<string, DtdParser.UndeclaredNotation> undeclaredNotations;

		// Token: 0x04000E56 RID: 3670
		private int[] condSectionEntityIds;

		// Token: 0x04000E57 RID: 3671
		private const int CondSectionEntityIdsInitialSize = 2;

		// Token: 0x02000252 RID: 594
		private enum Token
		{
			// Token: 0x04000E59 RID: 3673
			CDATA,
			// Token: 0x04000E5A RID: 3674
			ID,
			// Token: 0x04000E5B RID: 3675
			IDREF,
			// Token: 0x04000E5C RID: 3676
			IDREFS,
			// Token: 0x04000E5D RID: 3677
			ENTITY,
			// Token: 0x04000E5E RID: 3678
			ENTITIES,
			// Token: 0x04000E5F RID: 3679
			NMTOKEN,
			// Token: 0x04000E60 RID: 3680
			NMTOKENS,
			// Token: 0x04000E61 RID: 3681
			NOTATION,
			// Token: 0x04000E62 RID: 3682
			None,
			// Token: 0x04000E63 RID: 3683
			PERef,
			// Token: 0x04000E64 RID: 3684
			AttlistDecl,
			// Token: 0x04000E65 RID: 3685
			ElementDecl,
			// Token: 0x04000E66 RID: 3686
			EntityDecl,
			// Token: 0x04000E67 RID: 3687
			NotationDecl,
			// Token: 0x04000E68 RID: 3688
			Comment,
			// Token: 0x04000E69 RID: 3689
			PI,
			// Token: 0x04000E6A RID: 3690
			CondSectionStart,
			// Token: 0x04000E6B RID: 3691
			CondSectionEnd,
			// Token: 0x04000E6C RID: 3692
			Eof,
			// Token: 0x04000E6D RID: 3693
			REQUIRED,
			// Token: 0x04000E6E RID: 3694
			IMPLIED,
			// Token: 0x04000E6F RID: 3695
			FIXED,
			// Token: 0x04000E70 RID: 3696
			QName,
			// Token: 0x04000E71 RID: 3697
			Name,
			// Token: 0x04000E72 RID: 3698
			Nmtoken,
			// Token: 0x04000E73 RID: 3699
			Quote,
			// Token: 0x04000E74 RID: 3700
			LeftParen,
			// Token: 0x04000E75 RID: 3701
			RightParen,
			// Token: 0x04000E76 RID: 3702
			GreaterThan,
			// Token: 0x04000E77 RID: 3703
			Or,
			// Token: 0x04000E78 RID: 3704
			LeftBracket,
			// Token: 0x04000E79 RID: 3705
			RightBracket,
			// Token: 0x04000E7A RID: 3706
			PUBLIC,
			// Token: 0x04000E7B RID: 3707
			SYSTEM,
			// Token: 0x04000E7C RID: 3708
			Literal,
			// Token: 0x04000E7D RID: 3709
			DOCTYPE,
			// Token: 0x04000E7E RID: 3710
			NData,
			// Token: 0x04000E7F RID: 3711
			Percent,
			// Token: 0x04000E80 RID: 3712
			Star,
			// Token: 0x04000E81 RID: 3713
			QMark,
			// Token: 0x04000E82 RID: 3714
			Plus,
			// Token: 0x04000E83 RID: 3715
			PCDATA,
			// Token: 0x04000E84 RID: 3716
			Comma,
			// Token: 0x04000E85 RID: 3717
			ANY,
			// Token: 0x04000E86 RID: 3718
			EMPTY,
			// Token: 0x04000E87 RID: 3719
			IGNORE,
			// Token: 0x04000E88 RID: 3720
			INCLUDE
		}

		// Token: 0x02000253 RID: 595
		private enum ScanningFunction
		{
			// Token: 0x04000E8A RID: 3722
			SubsetContent,
			// Token: 0x04000E8B RID: 3723
			Name,
			// Token: 0x04000E8C RID: 3724
			QName,
			// Token: 0x04000E8D RID: 3725
			Nmtoken,
			// Token: 0x04000E8E RID: 3726
			Doctype1,
			// Token: 0x04000E8F RID: 3727
			Doctype2,
			// Token: 0x04000E90 RID: 3728
			Element1,
			// Token: 0x04000E91 RID: 3729
			Element2,
			// Token: 0x04000E92 RID: 3730
			Element3,
			// Token: 0x04000E93 RID: 3731
			Element4,
			// Token: 0x04000E94 RID: 3732
			Element5,
			// Token: 0x04000E95 RID: 3733
			Element6,
			// Token: 0x04000E96 RID: 3734
			Element7,
			// Token: 0x04000E97 RID: 3735
			Attlist1,
			// Token: 0x04000E98 RID: 3736
			Attlist2,
			// Token: 0x04000E99 RID: 3737
			Attlist3,
			// Token: 0x04000E9A RID: 3738
			Attlist4,
			// Token: 0x04000E9B RID: 3739
			Attlist5,
			// Token: 0x04000E9C RID: 3740
			Attlist6,
			// Token: 0x04000E9D RID: 3741
			Attlist7,
			// Token: 0x04000E9E RID: 3742
			Entity1,
			// Token: 0x04000E9F RID: 3743
			Entity2,
			// Token: 0x04000EA0 RID: 3744
			Entity3,
			// Token: 0x04000EA1 RID: 3745
			Notation1,
			// Token: 0x04000EA2 RID: 3746
			CondSection1,
			// Token: 0x04000EA3 RID: 3747
			CondSection2,
			// Token: 0x04000EA4 RID: 3748
			CondSection3,
			// Token: 0x04000EA5 RID: 3749
			Literal,
			// Token: 0x04000EA6 RID: 3750
			SystemId,
			// Token: 0x04000EA7 RID: 3751
			PublicId1,
			// Token: 0x04000EA8 RID: 3752
			PublicId2,
			// Token: 0x04000EA9 RID: 3753
			ClosingTag,
			// Token: 0x04000EAA RID: 3754
			ParamEntitySpace,
			// Token: 0x04000EAB RID: 3755
			None
		}

		// Token: 0x02000254 RID: 596
		private enum LiteralType
		{
			// Token: 0x04000EAD RID: 3757
			AttributeValue,
			// Token: 0x04000EAE RID: 3758
			EntityReplText,
			// Token: 0x04000EAF RID: 3759
			SystemOrPublicID
		}

		// Token: 0x02000255 RID: 597
		private class UndeclaredNotation
		{
			// Token: 0x0600173C RID: 5948 RVA: 0x00082182 File Offset: 0x00080382
			internal UndeclaredNotation(string name, int lineNo, int linePos)
			{
				this.name = name;
				this.lineNo = lineNo;
				this.linePos = linePos;
				this.next = null;
			}

			// Token: 0x04000EB0 RID: 3760
			internal string name;

			// Token: 0x04000EB1 RID: 3761
			internal int lineNo;

			// Token: 0x04000EB2 RID: 3762
			internal int linePos;

			// Token: 0x04000EB3 RID: 3763
			internal DtdParser.UndeclaredNotation next;
		}

		// Token: 0x02000256 RID: 598
		private class ParseElementOnlyContent_LocalFrame
		{
			// Token: 0x0600173D RID: 5949 RVA: 0x000821A6 File Offset: 0x000803A6
			public ParseElementOnlyContent_LocalFrame(int startParentEntityIdParam)
			{
				this.startParenEntityId = startParentEntityIdParam;
				this.parsingSchema = DtdParser.Token.None;
			}

			// Token: 0x04000EB4 RID: 3764
			public int startParenEntityId;

			// Token: 0x04000EB5 RID: 3765
			public DtdParser.Token parsingSchema;
		}

		// Token: 0x02000257 RID: 599
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <System-Xml-IDtdParser-ParseInternalDtdAsync>d__153 : IAsyncStateMachine
		{
			// Token: 0x0600173E RID: 5950 RVA: 0x000821C0 File Offset: 0x000803C0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				IDtdInfo schemaInfo;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						dtdParser.Initialize(adapter);
						configuredTaskAwaiter = dtdParser.ParseAsync(saveInternalSubset).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<System-Xml-IDtdParser-ParseInternalDtdAsync>d__153>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					schemaInfo = dtdParser.schemaInfo;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(schemaInfo);
			}

			// Token: 0x0600173F RID: 5951 RVA: 0x00082298 File Offset: 0x00080498
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000EB6 RID: 3766
			public int <>1__state;

			// Token: 0x04000EB7 RID: 3767
			public AsyncTaskMethodBuilder<IDtdInfo> <>t__builder;

			// Token: 0x04000EB8 RID: 3768
			public DtdParser <>4__this;

			// Token: 0x04000EB9 RID: 3769
			public IDtdParserAdapter adapter;

			// Token: 0x04000EBA RID: 3770
			public bool saveInternalSubset;

			// Token: 0x04000EBB RID: 3771
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000258 RID: 600
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <System-Xml-IDtdParser-ParseFreeFloatingDtdAsync>d__154 : IAsyncStateMachine
		{
			// Token: 0x06001740 RID: 5952 RVA: 0x000822A8 File Offset: 0x000804A8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				IDtdInfo schemaInfo;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						dtdParser.InitializeFreeFloatingDtd(baseUri, docTypeName, publicId, systemId, internalSubset, adapter);
						configuredTaskAwaiter = dtdParser.ParseAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<System-Xml-IDtdParser-ParseFreeFloatingDtdAsync>d__154>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					schemaInfo = dtdParser.schemaInfo;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(schemaInfo);
			}

			// Token: 0x06001741 RID: 5953 RVA: 0x0008239C File Offset: 0x0008059C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000EBC RID: 3772
			public int <>1__state;

			// Token: 0x04000EBD RID: 3773
			public AsyncTaskMethodBuilder<IDtdInfo> <>t__builder;

			// Token: 0x04000EBE RID: 3774
			public DtdParser <>4__this;

			// Token: 0x04000EBF RID: 3775
			public string baseUri;

			// Token: 0x04000EC0 RID: 3776
			public string docTypeName;

			// Token: 0x04000EC1 RID: 3777
			public string publicId;

			// Token: 0x04000EC2 RID: 3778
			public string systemId;

			// Token: 0x04000EC3 RID: 3779
			public string internalSubset;

			// Token: 0x04000EC4 RID: 3780
			public IDtdParserAdapter adapter;

			// Token: 0x04000EC5 RID: 3781
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000259 RID: 601
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseAsync>d__155 : IAsyncStateMachine
		{
			// Token: 0x06001742 RID: 5954 RVA: 0x000823AC File Offset: 0x000805AC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							if (dtdParser.freeFloatingDtd)
							{
								configuredTaskAwaiter = dtdParser.ParseFreeFloatingDtdAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 0);
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseAsync>d__155>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_7D;
							}
							else
							{
								configuredTaskAwaiter = dtdParser.ParseInDocumentDtdAsync(saveInternalSubset).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 1);
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseAsync>d__155>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						configuredTaskAwaiter.GetResult();
						goto IL_F0;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
					num = (num2 = -1);
					IL_7D:
					configuredTaskAwaiter.GetResult();
					IL_F0:
					dtdParser.schemaInfo.Finish();
					if (dtdParser.validate && dtdParser.undeclaredNotations != null)
					{
						Dictionary<string, DtdParser.UndeclaredNotation>.ValueCollection.Enumerator enumerator = dtdParser.undeclaredNotations.Values.GetEnumerator();
						try
						{
							while (enumerator.MoveNext())
							{
								DtdParser.UndeclaredNotation undeclaredNotation = enumerator.Current;
								for (DtdParser.UndeclaredNotation undeclaredNotation2 = undeclaredNotation; undeclaredNotation2 != null; undeclaredNotation2 = undeclaredNotation2.next)
								{
									dtdParser.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("The '{0}' notation is not declared.", undeclaredNotation.name, dtdParser.BaseUriStr, undeclaredNotation.lineNo, undeclaredNotation.linePos));
								}
							}
						}
						finally
						{
							if (num < 0)
							{
								((IDisposable)enumerator).Dispose();
							}
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001743 RID: 5955 RVA: 0x000825A4 File Offset: 0x000807A4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000EC6 RID: 3782
			public int <>1__state;

			// Token: 0x04000EC7 RID: 3783
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000EC8 RID: 3784
			public DtdParser <>4__this;

			// Token: 0x04000EC9 RID: 3785
			public bool saveInternalSubset;

			// Token: 0x04000ECA RID: 3786
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200025A RID: 602
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseInDocumentDtdAsync>d__156 : IAsyncStateMachine
		{
			// Token: 0x06001744 RID: 5956 RVA: 0x000825B4 File Offset: 0x000807B4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter6;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_121;
					case 2:
					{
						ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_19A;
					}
					case 3:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_21C;
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2AE;
					}
					case 5:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_337;
					}
					default:
						dtdParser.LoadParsingBuffer();
						dtdParser.scanningFunction = DtdParser.ScanningFunction.QName;
						dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Doctype1;
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseInDocumentDtdAsync>d__156>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.QName)
					{
						dtdParser.OnUnexpectedError();
					}
					dtdParser.schemaInfo.DocTypeName = dtdParser.GetNameQualified(true);
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseInDocumentDtdAsync>d__156>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_121:
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					if (result != DtdParser.Token.SYSTEM && result != DtdParser.Token.PUBLIC)
					{
						goto IL_224;
					}
					configuredTaskAwaiter4 = dtdParser.ParseExternalIdAsync(result, DtdParser.Token.DOCTYPE).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter, DtdParser.<ParseInDocumentDtdAsync>d__156>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_19A:
					Tuple<string, string> result2 = configuredTaskAwaiter4.GetResult();
					dtdParser.publicId = result2.Item1;
					dtdParser.systemId = result2.Item2;
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 3;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseInDocumentDtdAsync>d__156>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_21C:
					result = configuredTaskAwaiter3.GetResult();
					IL_224:
					if (result == DtdParser.Token.GreaterThan)
					{
						goto IL_2BD;
					}
					if (result != DtdParser.Token.LeftBracket)
					{
						dtdParser.OnUnexpectedError();
						goto IL_2BD;
					}
					if (saveInternalSubset)
					{
						dtdParser.SaveParsingBuffer();
						dtdParser.internalSubsetValueSb = new StringBuilder();
					}
					configuredTaskAwaiter6 = dtdParser.ParseInternalSubsetAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter6.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseInDocumentDtdAsync>d__156>(ref configuredTaskAwaiter6, ref this);
						return;
					}
					IL_2AE:
					configuredTaskAwaiter6.GetResult();
					IL_2BD:
					dtdParser.SaveParsingBuffer();
					if (dtdParser.systemId == null || dtdParser.systemId.Length <= 0)
					{
						goto IL_33E;
					}
					configuredTaskAwaiter6 = dtdParser.ParseExternalSubsetAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter6.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseInDocumentDtdAsync>d__156>(ref configuredTaskAwaiter6, ref this);
						return;
					}
					IL_337:
					configuredTaskAwaiter6.GetResult();
					IL_33E:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001745 RID: 5957 RVA: 0x0008294C File Offset: 0x00080B4C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000ECB RID: 3787
			public int <>1__state;

			// Token: 0x04000ECC RID: 3788
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000ECD RID: 3789
			public DtdParser <>4__this;

			// Token: 0x04000ECE RID: 3790
			public bool saveInternalSubset;

			// Token: 0x04000ECF RID: 3791
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000ED0 RID: 3792
			private ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x04000ED1 RID: 3793
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x0200025B RID: 603
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseFreeFloatingDtdAsync>d__157 : IAsyncStateMachine
		{
			// Token: 0x06001746 RID: 5958 RVA: 0x0008295C File Offset: 0x00080B5C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_100;
						}
						if (!dtdParser.hasFreeFloatingInternalSubset)
						{
							goto IL_90;
						}
						dtdParser.LoadParsingBuffer();
						configuredTaskAwaiter = dtdParser.ParseInternalSubsetAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseFreeFloatingDtdAsync>d__157>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.SaveParsingBuffer();
					IL_90:
					if (dtdParser.systemId == null || dtdParser.systemId.Length <= 0)
					{
						goto IL_107;
					}
					configuredTaskAwaiter = dtdParser.ParseExternalSubsetAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseFreeFloatingDtdAsync>d__157>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_100:
					configuredTaskAwaiter.GetResult();
					IL_107:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001747 RID: 5959 RVA: 0x00082AB0 File Offset: 0x00080CB0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000ED2 RID: 3794
			public int <>1__state;

			// Token: 0x04000ED3 RID: 3795
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000ED4 RID: 3796
			public DtdParser <>4__this;

			// Token: 0x04000ED5 RID: 3797
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200025C RID: 604
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseExternalSubsetAsync>d__159 : IAsyncStateMachine
		{
			// Token: 0x06001748 RID: 5960 RVA: 0x00082AC0 File Offset: 0x00080CC0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_128;
						}
						configuredTaskAwaiter5 = dtdParser.readerAdapter.PushExternalSubsetAsync(dtdParser.systemId, dtdParser.publicId).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ParseExternalSubsetAsync>d__159>(ref configuredTaskAwaiter5, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (!configuredTaskAwaiter5.GetResult())
					{
						goto IL_14A;
					}
					Uri baseUri = dtdParser.readerAdapter.BaseUri;
					if (baseUri != null)
					{
						dtdParser.externalDtdBaseUri = baseUri.ToString();
					}
					dtdParser.externalEntitiesDepth++;
					dtdParser.LoadParsingBuffer();
					configuredTaskAwaiter3 = dtdParser.ParseSubsetAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseExternalSubsetAsync>d__159>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_128:
					configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_14A:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001749 RID: 5961 RVA: 0x00082C48 File Offset: 0x00080E48
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000ED6 RID: 3798
			public int <>1__state;

			// Token: 0x04000ED7 RID: 3799
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000ED8 RID: 3800
			public DtdParser <>4__this;

			// Token: 0x04000ED9 RID: 3801
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000EDA RID: 3802
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200025D RID: 605
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseSubsetAsync>d__160 : IAsyncStateMachine
		{
			// Token: 0x0600174A RID: 5962 RVA: 0x00082C58 File Offset: 0x00080E58
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_146;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1B3;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_220;
					}
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_28D;
					}
					case 5:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2FA;
					}
					case 6:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_367;
					}
					case 7:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3EF;
					}
					case 8:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_52B;
					default:
						IL_38:
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					startTagEntityId = dtdParser.currentEntityId;
					switch (result)
					{
					case DtdParser.Token.AttlistDecl:
						configuredTaskAwaiter4 = dtdParser.ParseAttlistDeclAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						break;
					case DtdParser.Token.ElementDecl:
						configuredTaskAwaiter4 = dtdParser.ParseElementDeclAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_1B3;
					case DtdParser.Token.EntityDecl:
						configuredTaskAwaiter4 = dtdParser.ParseEntityDeclAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_220;
					case DtdParser.Token.NotationDecl:
						configuredTaskAwaiter4 = dtdParser.ParseNotationDeclAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 4;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_28D;
					case DtdParser.Token.Comment:
						configuredTaskAwaiter4 = dtdParser.ParseCommentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 5;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_2FA;
					case DtdParser.Token.PI:
						configuredTaskAwaiter4 = dtdParser.ParsePIAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 6;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_367;
					case DtdParser.Token.CondSectionStart:
						if (dtdParser.ParsingInternalSubset)
						{
							dtdParser.Throw(dtdParser.curPos - 3, "A conditional section is not allowed in an internal subset.");
						}
						configuredTaskAwaiter4 = dtdParser.ParseCondSectionAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 7;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_3EF;
					case DtdParser.Token.CondSectionEnd:
						if (dtdParser.condSectionDepth <= 0)
						{
							dtdParser.Throw(dtdParser.curPos - 3, "']]>' is not expected.");
							goto IL_59B;
						}
						dtdParser.condSectionDepth--;
						if (dtdParser.validate && dtdParser.currentEntityId != dtdParser.condSectionEntityIds[dtdParser.condSectionDepth])
						{
							dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
							goto IL_59B;
						}
						goto IL_59B;
					case DtdParser.Token.Eof:
						if (dtdParser.ParsingInternalSubset && !dtdParser.freeFloatingDtd)
						{
							dtdParser.Throw(dtdParser.curPos, "Incomplete DTD content.");
						}
						if (dtdParser.condSectionDepth != 0)
						{
							dtdParser.Throw(dtdParser.curPos, "There is an unclosed conditional section.");
						}
						goto IL_60A;
					default:
						if (result != DtdParser.Token.RightBracket)
						{
							goto IL_59B;
						}
						if (!dtdParser.ParsingInternalSubset)
						{
							dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
							goto IL_55A;
						}
						if (dtdParser.condSectionDepth != 0)
						{
							dtdParser.Throw(dtdParser.curPos, "There is an unclosed conditional section.");
						}
						if (dtdParser.internalSubsetValueSb != null)
						{
							dtdParser.SaveParsingBuffer(dtdParser.curPos - 1);
							dtdParser.schemaInfo.InternalDtdSubset = dtdParser.internalSubsetValueSb.ToString();
							dtdParser.internalSubsetValueSb = null;
						}
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 8;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseSubsetAsync>d__160>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_52B;
					}
					IL_146:
					configuredTaskAwaiter4.GetResult();
					goto IL_59B;
					IL_1B3:
					configuredTaskAwaiter4.GetResult();
					goto IL_59B;
					IL_220:
					configuredTaskAwaiter4.GetResult();
					goto IL_59B;
					IL_28D:
					configuredTaskAwaiter4.GetResult();
					goto IL_59B;
					IL_2FA:
					configuredTaskAwaiter4.GetResult();
					goto IL_59B;
					IL_367:
					configuredTaskAwaiter4.GetResult();
					goto IL_59B;
					IL_3EF:
					configuredTaskAwaiter4.GetResult();
					startTagEntityId = dtdParser.currentEntityId;
					goto IL_59B;
					IL_52B:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.GreaterThan)
					{
						dtdParser.ThrowUnexpectedToken(dtdParser.curPos, ">");
					}
					IL_55A:
					goto IL_60A;
					IL_59B:
					if (dtdParser.currentEntityId == startTagEntityId)
					{
						goto IL_38;
					}
					if (dtdParser.validate)
					{
						dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						goto IL_38;
					}
					if (!dtdParser.v1Compat)
					{
						dtdParser.Throw(dtdParser.curPos, "The parameter entity replacement text must nest properly within markup declarations.");
						goto IL_38;
					}
					goto IL_38;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_60A:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600174B RID: 5963 RVA: 0x000832A0 File Offset: 0x000814A0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000EDB RID: 3803
			public int <>1__state;

			// Token: 0x04000EDC RID: 3804
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000EDD RID: 3805
			public DtdParser <>4__this;

			// Token: 0x04000EDE RID: 3806
			private int <startTagEntityId>5__1;

			// Token: 0x04000EDF RID: 3807
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000EE0 RID: 3808
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200025E RID: 606
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseAttlistDeclAsync>d__161 : IAsyncStateMachine
		{
			// Token: 0x0600174C RID: 5964 RVA: 0x000832B0 File Offset: 0x000814B0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_15A;
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_337;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3AB;
					}
					default:
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistDeclAsync>d__161>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.QName)
					{
						goto IL_4F5;
					}
					XmlQualifiedName nameQualified = dtdParser.GetNameQualified(true);
					if (!dtdParser.schemaInfo.ElementDecls.TryGetValue(nameQualified, out elementDecl) && !dtdParser.schemaInfo.UndeclaredElementDecls.TryGetValue(nameQualified, out elementDecl))
					{
						elementDecl = new SchemaElementDecl(nameQualified, nameQualified.Namespace);
						dtdParser.schemaInfo.UndeclaredElementDecls.Add(nameQualified, elementDecl);
					}
					attrDef = null;
					IL_FB:
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistDeclAsync>d__161>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_15A:
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					if (result != DtdParser.Token.QName)
					{
						if (result != DtdParser.Token.GreaterThan)
						{
							goto IL_4F5;
						}
						if (dtdParser.v1Compat && attrDef != null && attrDef.Prefix.Length > 0 && attrDef.Prefix.Equals("xml") && attrDef.Name.Name == "space")
						{
							attrDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
							if (attrDef.Datatype.TokenizedType != XmlTokenizedType.ENUMERATION)
							{
								dtdParser.Throw("Enumeration data type required.", string.Empty, attrDef.LineNumber, attrDef.LinePosition);
							}
							if (dtdParser.validate)
							{
								attrDef.CheckXmlSpace(dtdParser.readerAdapterWithValidation.ValidationEventHandling);
							}
						}
						goto IL_516;
					}
					else
					{
						XmlQualifiedName nameQualified2 = dtdParser.GetNameQualified(true);
						attrDef = new SchemaAttDef(nameQualified2, nameQualified2.Namespace);
						attrDef.IsDeclaredInExternal = !dtdParser.ParsingInternalSubset;
						attrDef.LineNumber = dtdParser.LineNo;
						attrDef.LinePosition = dtdParser.LinePos - (dtdParser.curPos - dtdParser.tokenStartPos);
						attrDefAlreadyExists = (elementDecl.GetAttDef(attrDef.Name) != null);
						configuredTaskAwaiter4 = dtdParser.ParseAttlistTypeAsync(attrDef, elementDecl, attrDefAlreadyExists).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistDeclAsync>d__161>(ref configuredTaskAwaiter4, ref this);
							return;
						}
					}
					IL_337:
					configuredTaskAwaiter4.GetResult();
					configuredTaskAwaiter4 = dtdParser.ParseAttlistDefaultAsync(attrDef, attrDefAlreadyExists).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistDeclAsync>d__161>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_3AB:
					configuredTaskAwaiter4.GetResult();
					if (attrDef.Prefix.Length > 0 && attrDef.Prefix.Equals("xml"))
					{
						if (attrDef.Name.Name == "space")
						{
							if (dtdParser.v1Compat)
							{
								string text = attrDef.DefaultValueExpanded.Trim();
								if (text.Equals("preserve") || text.Equals("default"))
								{
									attrDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
								}
							}
							else
							{
								attrDef.Reserved = SchemaAttDef.Reserve.XmlSpace;
								if (attrDef.TokenizedType != XmlTokenizedType.ENUMERATION)
								{
									dtdParser.Throw("Enumeration data type required.", string.Empty, attrDef.LineNumber, attrDef.LinePosition);
								}
								if (dtdParser.validate)
								{
									attrDef.CheckXmlSpace(dtdParser.readerAdapterWithValidation.ValidationEventHandling);
								}
							}
						}
						else if (attrDef.Name.Name == "lang")
						{
							attrDef.Reserved = SchemaAttDef.Reserve.XmlLang;
						}
					}
					if (!attrDefAlreadyExists)
					{
						elementDecl.AddAttDef(attrDef);
						goto IL_FB;
					}
					goto IL_FB;
					IL_4F5:
					dtdParser.OnUnexpectedError();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_516:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600174D RID: 5965 RVA: 0x00083804 File Offset: 0x00081A04
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000EE1 RID: 3809
			public int <>1__state;

			// Token: 0x04000EE2 RID: 3810
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000EE3 RID: 3811
			public DtdParser <>4__this;

			// Token: 0x04000EE4 RID: 3812
			private SchemaAttDef <attrDef>5__1;

			// Token: 0x04000EE5 RID: 3813
			private SchemaElementDecl <elementDecl>5__2;

			// Token: 0x04000EE6 RID: 3814
			private bool <attrDefAlreadyExists>5__3;

			// Token: 0x04000EE7 RID: 3815
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000EE8 RID: 3816
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200025F RID: 607
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseAttlistTypeAsync>d__162 : IAsyncStateMachine
		{
			// Token: 0x0600174E RID: 5966 RVA: 0x00083814 File Offset: 0x00081A14
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_27B;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2E8;
					case 3:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3E4;
					case 4:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_458;
					case 5:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_509;
					case 6:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_587;
					case 7:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_5FE;
					default:
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistTypeAsync>d__162>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					DtdParser.Token token2 = configuredTaskAwaiter3.GetResult();
					token = token2;
					if (token != DtdParser.Token.CDATA)
					{
						elementDecl.HasNonCDataAttribute = true;
					}
					if (dtdParser.IsAttributeValueType(token))
					{
						attrDef.TokenizedType = (XmlTokenizedType)token;
						attrDef.SchemaType = XmlSchemaType.GetBuiltInSimpleType(attrDef.Datatype.TypeCode);
						token2 = token;
						if (token2 != DtdParser.Token.ID)
						{
							if (token2 == DtdParser.Token.NOTATION)
							{
								if (dtdParser.validate)
								{
									if (elementDecl.IsNotationDeclared && !ignoreErrors)
									{
										dtdParser.SendValidationEvent(dtdParser.curPos - 8, XmlSeverityType.Error, "No element type can have more than one NOTATION attribute specified.", elementDecl.Name.ToString());
									}
									else
									{
										if (elementDecl.ContentValidator != null && elementDecl.ContentValidator.ContentType == XmlSchemaContentType.Empty && !ignoreErrors)
										{
											dtdParser.SendValidationEvent(dtdParser.curPos - 8, XmlSeverityType.Error, "An attribute of type NOTATION must not be declared on an element declared EMPTY.", elementDecl.Name.ToString());
										}
										elementDecl.IsNotationDeclared = true;
									}
								}
								configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistTypeAsync>d__162>(ref configuredTaskAwaiter3, ref this);
									return;
								}
								goto IL_27B;
							}
						}
						else
						{
							if (dtdParser.validate && elementDecl.IsIdDeclared)
							{
								SchemaAttDef attDef = elementDecl.GetAttDef(attrDef.Name);
								if ((attDef == null || attDef.Datatype.TokenizedType != XmlTokenizedType.ID) && !ignoreErrors)
								{
									dtdParser.SendValidationEvent(XmlSeverityType.Error, "The attribute of type ID is already declared on the '{0}' element.", elementDecl.Name.ToString());
								}
							}
							elementDecl.IsIdDeclared = true;
						}
						goto IL_6A4;
					}
					if (token != DtdParser.Token.LeftParen)
					{
						goto IL_683;
					}
					attrDef.TokenizedType = XmlTokenizedType.ENUMERATION;
					attrDef.SchemaType = XmlSchemaType.GetBuiltInSimpleType(attrDef.Datatype.TypeCode);
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 5;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistTypeAsync>d__162>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_509;
					IL_27B:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.LeftParen)
					{
						goto IL_683;
					}
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistTypeAsync>d__162>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_2E8:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.Name)
					{
						goto IL_683;
					}
					IL_2F6:
					string nameString = dtdParser.GetNameString();
					if (!dtdParser.schemaInfo.Notations.ContainsKey(nameString))
					{
						dtdParser.AddUndeclaredNotation(nameString);
					}
					if (dtdParser.validate && !dtdParser.v1Compat && attrDef.Values != null && attrDef.Values.Contains(nameString) && !ignoreErrors)
					{
						dtdParser.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("'{0}' is a duplicate notation value.", nameString, dtdParser.BaseUriStr, dtdParser.LineNo, dtdParser.LinePos));
					}
					attrDef.AddValue(nameString);
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 3;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistTypeAsync>d__162>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_3E4:
					token2 = configuredTaskAwaiter3.GetResult();
					if (token2 == DtdParser.Token.RightParen)
					{
						goto IL_6A4;
					}
					if (token2 != DtdParser.Token.Or)
					{
						goto IL_683;
					}
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 4;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistTypeAsync>d__162>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_458:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.Name)
					{
						goto IL_683;
					}
					goto IL_2F6;
					IL_509:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.Nmtoken)
					{
						goto IL_683;
					}
					attrDef.AddValue(dtdParser.GetNameString());
					IL_528:
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 6;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistTypeAsync>d__162>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_587:
					token2 = configuredTaskAwaiter3.GetResult();
					if (token2 == DtdParser.Token.RightParen)
					{
						goto IL_6A4;
					}
					if (token2 != DtdParser.Token.Or)
					{
						goto IL_683;
					}
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 7;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistTypeAsync>d__162>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_5FE:
					if (configuredTaskAwaiter3.GetResult() == DtdParser.Token.Nmtoken)
					{
						string nmtokenString = dtdParser.GetNmtokenString();
						if (dtdParser.validate && !dtdParser.v1Compat && attrDef.Values != null && attrDef.Values.Contains(nmtokenString) && !ignoreErrors)
						{
							dtdParser.SendValidationEvent(XmlSeverityType.Error, new XmlSchemaException("'{0}' is a duplicate enumeration value.", nmtokenString, dtdParser.BaseUriStr, dtdParser.LineNo, dtdParser.LinePos));
						}
						attrDef.AddValue(nmtokenString);
						goto IL_528;
					}
					IL_683:
					dtdParser.OnUnexpectedError();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_6A4:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600174F RID: 5967 RVA: 0x00083EF4 File Offset: 0x000820F4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000EE9 RID: 3817
			public int <>1__state;

			// Token: 0x04000EEA RID: 3818
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000EEB RID: 3819
			public DtdParser <>4__this;

			// Token: 0x04000EEC RID: 3820
			public SchemaElementDecl elementDecl;

			// Token: 0x04000EED RID: 3821
			public SchemaAttDef attrDef;

			// Token: 0x04000EEE RID: 3822
			public bool ignoreErrors;

			// Token: 0x04000EEF RID: 3823
			private DtdParser.Token <token>5__1;

			// Token: 0x04000EF0 RID: 3824
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000260 RID: 608
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseAttlistDefaultAsync>d__163 : IAsyncStateMachine
		{
			// Token: 0x06001750 RID: 5968 RVA: 0x00083F04 File Offset: 0x00082104
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_12E;
						}
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistDefaultAsync>d__163>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					switch (result)
					{
					case DtdParser.Token.REQUIRED:
						attrDef.Presence = SchemaDeclBase.Use.Required;
						goto IL_209;
					case DtdParser.Token.IMPLIED:
						attrDef.Presence = SchemaDeclBase.Use.Implied;
						goto IL_209;
					case DtdParser.Token.FIXED:
						attrDef.Presence = SchemaDeclBase.Use.Fixed;
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseAttlistDefaultAsync>d__163>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					default:
						if (result != DtdParser.Token.Literal)
						{
							goto IL_1E8;
						}
						goto IL_13C;
					}
					IL_12E:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.Literal)
					{
						goto IL_1E8;
					}
					IL_13C:
					if (dtdParser.validate && attrDef.Datatype.TokenizedType == XmlTokenizedType.ID && !ignoreErrors)
					{
						dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "An attribute of type ID must have a declared default of either #IMPLIED or #REQUIRED.", string.Empty);
					}
					if (attrDef.TokenizedType != XmlTokenizedType.CDATA)
					{
						attrDef.DefaultValueExpanded = dtdParser.GetValueWithStrippedSpaces();
					}
					else
					{
						attrDef.DefaultValueExpanded = dtdParser.GetValue();
					}
					attrDef.ValueLineNumber = dtdParser.literalLineInfo.lineNo;
					attrDef.ValueLinePosition = dtdParser.literalLineInfo.linePos + 1;
					DtdValidator.SetDefaultTypedValue(attrDef, dtdParser.readerAdapter);
					goto IL_209;
					IL_1E8:
					dtdParser.OnUnexpectedError();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_209:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001751 RID: 5969 RVA: 0x0008414C File Offset: 0x0008234C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000EF1 RID: 3825
			public int <>1__state;

			// Token: 0x04000EF2 RID: 3826
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000EF3 RID: 3827
			public DtdParser <>4__this;

			// Token: 0x04000EF4 RID: 3828
			public SchemaAttDef attrDef;

			// Token: 0x04000EF5 RID: 3829
			public bool ignoreErrors;

			// Token: 0x04000EF6 RID: 3830
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000261 RID: 609
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseElementDeclAsync>d__164 : IAsyncStateMachine
		{
			// Token: 0x06001752 RID: 5970 RVA: 0x0008415C File Offset: 0x0008235C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1B8;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_26B;
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_315;
					}
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3CE;
					}
					case 5:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_448;
					default:
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementDeclAsync>d__164>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.QName)
					{
						goto IL_466;
					}
					elementDecl = null;
					XmlQualifiedName nameQualified = dtdParser.GetNameQualified(true);
					if (dtdParser.schemaInfo.ElementDecls.TryGetValue(nameQualified, out elementDecl))
					{
						if (dtdParser.validate)
						{
							dtdParser.SendValidationEvent(dtdParser.curPos - nameQualified.Name.Length, XmlSeverityType.Error, "The '{0}' element has already been declared.", dtdParser.GetNameString());
						}
					}
					else
					{
						if (dtdParser.schemaInfo.UndeclaredElementDecls.TryGetValue(nameQualified, out elementDecl))
						{
							dtdParser.schemaInfo.UndeclaredElementDecls.Remove(nameQualified);
						}
						else
						{
							elementDecl = new SchemaElementDecl(nameQualified, nameQualified.Namespace);
						}
						dtdParser.schemaInfo.ElementDecls.Add(nameQualified, elementDecl);
					}
					elementDecl.IsDeclaredInExternal = !dtdParser.ParsingInternalSubset;
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementDeclAsync>d__164>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_1B8:
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					if (result != DtdParser.Token.LeftParen)
					{
						if (result == DtdParser.Token.ANY)
						{
							elementDecl.ContentValidator = ContentValidator.Any;
							goto IL_3EC;
						}
						if (result == DtdParser.Token.EMPTY)
						{
							elementDecl.ContentValidator = ContentValidator.Empty;
							goto IL_3EC;
						}
						goto IL_466;
					}
					else
					{
						startParenEntityId = dtdParser.currentEntityId;
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 2;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementDeclAsync>d__164>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					IL_26B:
					DtdParser.Token result2 = configuredTaskAwaiter3.GetResult();
					if (result2 != DtdParser.Token.None)
					{
						if (result2 != DtdParser.Token.PCDATA)
						{
							goto IL_466;
						}
						pcv = new ParticleContentValidator(XmlSchemaContentType.Mixed);
						pcv.Start();
						pcv.OpenGroup();
						configuredTaskAwaiter4 = dtdParser.ParseElementMixedContentAsync(pcv, startParenEntityId).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseElementDeclAsync>d__164>(ref configuredTaskAwaiter4, ref this);
							return;
						}
					}
					else
					{
						pcv2 = null;
						pcv2 = new ParticleContentValidator(XmlSchemaContentType.ElementOnly);
						pcv2.Start();
						pcv2.OpenGroup();
						configuredTaskAwaiter4 = dtdParser.ParseElementOnlyContentAsync(pcv2, startParenEntityId).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 4;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseElementDeclAsync>d__164>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_3CE;
					}
					IL_315:
					configuredTaskAwaiter4.GetResult();
					elementDecl.ContentValidator = pcv.Finish(true);
					goto IL_3EC;
					IL_3CE:
					configuredTaskAwaiter4.GetResult();
					elementDecl.ContentValidator = pcv2.Finish(true);
					IL_3EC:
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 5;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementDeclAsync>d__164>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_448:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.GreaterThan)
					{
						dtdParser.ThrowUnexpectedToken(dtdParser.curPos, ">");
					}
					goto IL_487;
					IL_466:
					dtdParser.OnUnexpectedError();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_487:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001753 RID: 5971 RVA: 0x00084620 File Offset: 0x00082820
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000EF7 RID: 3831
			public int <>1__state;

			// Token: 0x04000EF8 RID: 3832
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000EF9 RID: 3833
			public DtdParser <>4__this;

			// Token: 0x04000EFA RID: 3834
			private SchemaElementDecl <elementDecl>5__1;

			// Token: 0x04000EFB RID: 3835
			private int <startParenEntityId>5__2;

			// Token: 0x04000EFC RID: 3836
			private ParticleContentValidator <pcv>5__3;

			// Token: 0x04000EFD RID: 3837
			private ParticleContentValidator <pcv>5__4;

			// Token: 0x04000EFE RID: 3838
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000EFF RID: 3839
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000262 RID: 610
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseElementOnlyContentAsync>d__165 : IAsyncStateMachine
		{
			// Token: 0x06001754 RID: 5972 RVA: 0x00084630 File Offset: 0x00082830
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_B0;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_14C;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1FC;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_33F;
					}
					default:
						localFrames = new Stack<DtdParser.ParseElementOnlyContent_LocalFrame>();
						currentFrame = new DtdParser.ParseElementOnlyContent_LocalFrame(startParenEntityId);
						localFrames.Push(currentFrame);
						break;
					}
					IL_51:
					configuredTaskAwaiter = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementOnlyContentAsync>d__165>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_B0:
					DtdParser.Token result = configuredTaskAwaiter.GetResult();
					if (result != DtdParser.Token.QName)
					{
						if (result == DtdParser.Token.LeftParen)
						{
							pcv.OpenGroup();
							currentFrame = new DtdParser.ParseElementOnlyContent_LocalFrame(dtdParser.currentEntityId);
							localFrames.Push(currentFrame);
							goto IL_51;
						}
						if (result != DtdParser.Token.GreaterThan)
						{
							goto IL_35B;
						}
						dtdParser.Throw(dtdParser.curPos, "Invalid content model.");
						goto IL_361;
					}
					else
					{
						pcv.AddName(dtdParser.GetNameQualified(true), null);
						configuredTaskAwaiter3 = dtdParser.ParseHowManyAsync(pcv).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseElementOnlyContentAsync>d__165>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					IL_14C:
					configuredTaskAwaiter3.GetResult();
					IL_19D:
					configuredTaskAwaiter = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementOnlyContentAsync>d__165>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_1FC:
					result = configuredTaskAwaiter.GetResult();
					switch (result)
					{
					case DtdParser.Token.RightParen:
						pcv.CloseGroup();
						if (dtdParser.validate && dtdParser.currentEntityId != currentFrame.startParenEntityId)
						{
							dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						}
						configuredTaskAwaiter3 = dtdParser.ParseHowManyAsync(pcv).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseElementOnlyContentAsync>d__165>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					case DtdParser.Token.GreaterThan:
						dtdParser.Throw(dtdParser.curPos, "Invalid content model.");
						goto IL_361;
					case DtdParser.Token.Or:
						if (currentFrame.parsingSchema == DtdParser.Token.Comma)
						{
							dtdParser.Throw(dtdParser.curPos, "Invalid content model.");
						}
						pcv.AddChoice();
						currentFrame.parsingSchema = DtdParser.Token.Or;
						goto IL_51;
					default:
						if (result == DtdParser.Token.Comma)
						{
							if (currentFrame.parsingSchema == DtdParser.Token.Or)
							{
								dtdParser.Throw(dtdParser.curPos, "Invalid content model.");
							}
							pcv.AddSequence();
							currentFrame.parsingSchema = DtdParser.Token.Comma;
							goto IL_51;
						}
						goto IL_35B;
					}
					IL_33F:
					configuredTaskAwaiter3.GetResult();
					goto IL_361;
					IL_35B:
					dtdParser.OnUnexpectedError();
					IL_361:
					localFrames.Pop();
					if (localFrames.Count > 0)
					{
						currentFrame = localFrames.Peek();
						goto IL_19D;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001755 RID: 5973 RVA: 0x00084A18 File Offset: 0x00082C18
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F00 RID: 3840
			public int <>1__state;

			// Token: 0x04000F01 RID: 3841
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F02 RID: 3842
			public int startParenEntityId;

			// Token: 0x04000F03 RID: 3843
			public DtdParser <>4__this;

			// Token: 0x04000F04 RID: 3844
			public ParticleContentValidator pcv;

			// Token: 0x04000F05 RID: 3845
			private Stack<DtdParser.ParseElementOnlyContent_LocalFrame> <localFrames>5__1;

			// Token: 0x04000F06 RID: 3846
			private DtdParser.ParseElementOnlyContent_LocalFrame <currentFrame>5__2;

			// Token: 0x04000F07 RID: 3847
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000F08 RID: 3848
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000263 RID: 611
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseHowManyAsync>d__166 : IAsyncStateMachine
		{
			// Token: 0x06001756 RID: 5974 RVA: 0x00084A28 File Offset: 0x00082C28
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseHowManyAsync>d__166>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					switch (configuredTaskAwaiter3.GetResult())
					{
					case DtdParser.Token.Star:
						pcv.AddStar();
						break;
					case DtdParser.Token.QMark:
						pcv.AddQMark();
						break;
					case DtdParser.Token.Plus:
						pcv.AddPlus();
						break;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001757 RID: 5975 RVA: 0x00084B28 File Offset: 0x00082D28
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F09 RID: 3849
			public int <>1__state;

			// Token: 0x04000F0A RID: 3850
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F0B RID: 3851
			public DtdParser <>4__this;

			// Token: 0x04000F0C RID: 3852
			public ParticleContentValidator pcv;

			// Token: 0x04000F0D RID: 3853
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000264 RID: 612
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseElementMixedContentAsync>d__167 : IAsyncStateMachine
		{
			// Token: 0x06001758 RID: 5976 RVA: 0x00084B38 File Offset: 0x00082D38
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_99;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_14A;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_23D;
					default:
						hasNames = false;
						connectorEntityId = -1;
						contentEntityId = dtdParser.currentEntityId;
						break;
					}
					IL_3A:
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementMixedContentAsync>d__167>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_99:
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					if (result != DtdParser.Token.RightParen)
					{
						if (result != DtdParser.Token.Or)
						{
							goto IL_2D0;
						}
						if (!hasNames)
						{
							hasNames = true;
						}
						else
						{
							pcv.AddChoice();
						}
						if (dtdParser.validate)
						{
							connectorEntityId = dtdParser.currentEntityId;
							if (contentEntityId < connectorEntityId)
							{
								dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
							}
						}
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 2;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementMixedContentAsync>d__167>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_23D;
					}
					else
					{
						pcv.CloseGroup();
						if (dtdParser.validate && dtdParser.currentEntityId != startParenEntityId)
						{
							dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						}
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseElementMixedContentAsync>d__167>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					IL_14A:
					if (configuredTaskAwaiter3.GetResult() == DtdParser.Token.Star && hasNames)
					{
						pcv.AddStar();
					}
					else if (hasNames)
					{
						dtdParser.ThrowUnexpectedToken(dtdParser.curPos, "*");
					}
					goto IL_2F4;
					IL_23D:
					if (configuredTaskAwaiter3.GetResult() == DtdParser.Token.QName)
					{
						XmlQualifiedName nameQualified = dtdParser.GetNameQualified(true);
						if (pcv.Exists(nameQualified) && dtdParser.validate)
						{
							dtdParser.SendValidationEvent(XmlSeverityType.Error, "The '{0}' element already exists in the content model.", nameQualified.ToString());
						}
						pcv.AddName(nameQualified, null);
						if (!dtdParser.validate)
						{
							goto IL_3A;
						}
						contentEntityId = dtdParser.currentEntityId;
						if (contentEntityId < connectorEntityId)
						{
							dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
							goto IL_3A;
						}
						goto IL_3A;
					}
					IL_2D0:
					dtdParser.OnUnexpectedError();
					goto IL_3A;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2F4:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001759 RID: 5977 RVA: 0x00084E68 File Offset: 0x00083068
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F0E RID: 3854
			public int <>1__state;

			// Token: 0x04000F0F RID: 3855
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F10 RID: 3856
			public DtdParser <>4__this;

			// Token: 0x04000F11 RID: 3857
			public ParticleContentValidator pcv;

			// Token: 0x04000F12 RID: 3858
			public int startParenEntityId;

			// Token: 0x04000F13 RID: 3859
			private bool <hasNames>5__1;

			// Token: 0x04000F14 RID: 3860
			private int <contentEntityId>5__2;

			// Token: 0x04000F15 RID: 3861
			private int <connectorEntityId>5__3;

			// Token: 0x04000F16 RID: 3862
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000265 RID: 613
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseEntityDeclAsync>d__168 : IAsyncStateMachine
		{
			// Token: 0x0600175A RID: 5978 RVA: 0x00084E78 File Offset: 0x00083078
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_11E;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_263;
					case 3:
					{
						ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2E3;
					}
					case 4:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_381;
					case 5:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_42C;
					case 6:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_518;
					default:
						isParamEntity = false;
						entity = null;
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseEntityDeclAsync>d__168>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					if (result == DtdParser.Token.Name)
					{
						goto IL_12C;
					}
					if (result != DtdParser.Token.Percent)
					{
						goto IL_531;
					}
					isParamEntity = true;
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseEntityDeclAsync>d__168>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_11E:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.Name)
					{
						goto IL_531;
					}
					IL_12C:
					XmlQualifiedName nameQualified = dtdParser.GetNameQualified(false);
					entity = new SchemaEntity(nameQualified, isParamEntity);
					entity.BaseURI = dtdParser.BaseUriStr;
					entity.DeclaredURI = ((dtdParser.externalDtdBaseUri.Length == 0) ? dtdParser.documentBaseUri : dtdParser.externalDtdBaseUri);
					if (isParamEntity)
					{
						if (!dtdParser.schemaInfo.ParameterEntities.ContainsKey(nameQualified))
						{
							dtdParser.schemaInfo.ParameterEntities.Add(nameQualified, entity);
						}
					}
					else if (!dtdParser.schemaInfo.GeneralEntities.ContainsKey(nameQualified))
					{
						dtdParser.schemaInfo.GeneralEntities.Add(nameQualified, entity);
					}
					entity.DeclaredInExternal = !dtdParser.ParsingInternalSubset;
					entity.ParsingInProgress = true;
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseEntityDeclAsync>d__168>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_263:
					DtdParser.Token result2 = configuredTaskAwaiter3.GetResult();
					if (result2 - DtdParser.Token.PUBLIC > 1)
					{
						if (result2 != DtdParser.Token.Literal)
						{
							goto IL_531;
						}
						entity.Text = dtdParser.GetValue();
						entity.Line = dtdParser.literalLineInfo.lineNo;
						entity.Pos = dtdParser.literalLineInfo.linePos;
						goto IL_4B9;
					}
					else
					{
						configuredTaskAwaiter4 = dtdParser.ParseExternalIdAsync(result2, DtdParser.Token.EntityDecl).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter, DtdParser.<ParseEntityDeclAsync>d__168>(ref configuredTaskAwaiter4, ref this);
							return;
						}
					}
					IL_2E3:
					Tuple<string, string> result3 = configuredTaskAwaiter4.GetResult();
					string item = result3.Item1;
					string item2 = result3.Item2;
					entity.IsExternal = true;
					entity.Url = item2;
					entity.Pubid = item;
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 4;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseEntityDeclAsync>d__168>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_381:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.NData)
					{
						goto IL_4B9;
					}
					if (isParamEntity)
					{
						dtdParser.ThrowUnexpectedToken(dtdParser.curPos - 5, ">");
					}
					if (!dtdParser.whitespaceSeen)
					{
						dtdParser.Throw(dtdParser.curPos - 5, "'{0}' is an unexpected token. Expecting white space.", "NDATA");
					}
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 5;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseEntityDeclAsync>d__168>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_42C:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.Name)
					{
						goto IL_531;
					}
					entity.NData = dtdParser.GetNameQualified(false);
					string name = entity.NData.Name;
					if (!dtdParser.schemaInfo.Notations.ContainsKey(name))
					{
						dtdParser.AddUndeclaredNotation(name);
					}
					IL_4B9:
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 6;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseEntityDeclAsync>d__168>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_518:
					if (configuredTaskAwaiter3.GetResult() == DtdParser.Token.GreaterThan)
					{
						entity.ParsingInProgress = false;
						goto IL_552;
					}
					IL_531:
					dtdParser.OnUnexpectedError();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_552:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600175B RID: 5979 RVA: 0x00085408 File Offset: 0x00083608
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F17 RID: 3863
			public int <>1__state;

			// Token: 0x04000F18 RID: 3864
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F19 RID: 3865
			public DtdParser <>4__this;

			// Token: 0x04000F1A RID: 3866
			private bool <isParamEntity>5__1;

			// Token: 0x04000F1B RID: 3867
			private SchemaEntity <entity>5__2;

			// Token: 0x04000F1C RID: 3868
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000F1D RID: 3869
			private ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000266 RID: 614
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseNotationDeclAsync>d__169 : IAsyncStateMachine
		{
			// Token: 0x0600175C RID: 5980 RVA: 0x00085418 File Offset: 0x00083618
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_19A;
					case 2:
					{
						ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_212;
					}
					case 3:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2B1;
					default:
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseNotationDeclAsync>d__169>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.Name)
					{
						dtdParser.OnUnexpectedError();
					}
					XmlQualifiedName nameQualified = dtdParser.GetNameQualified(false);
					notation = null;
					if (!dtdParser.schemaInfo.Notations.ContainsKey(nameQualified.Name))
					{
						if (dtdParser.undeclaredNotations != null)
						{
							dtdParser.undeclaredNotations.Remove(nameQualified.Name);
						}
						notation = new SchemaNotation(nameQualified);
						dtdParser.schemaInfo.Notations.Add(notation.Name.Name, notation);
					}
					else if (dtdParser.validate)
					{
						dtdParser.SendValidationEvent(dtdParser.curPos - nameQualified.Name.Length, XmlSeverityType.Error, "The notation '{0}' has already been declared.", nameQualified.Name);
					}
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseNotationDeclAsync>d__169>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_19A:
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					if (result != DtdParser.Token.SYSTEM && result != DtdParser.Token.PUBLIC)
					{
						dtdParser.OnUnexpectedError();
						goto IL_252;
					}
					configuredTaskAwaiter4 = dtdParser.ParseExternalIdAsync(result, DtdParser.Token.NOTATION).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter, DtdParser.<ParseNotationDeclAsync>d__169>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_212:
					Tuple<string, string> result2 = configuredTaskAwaiter4.GetResult();
					string item = result2.Item1;
					string item2 = result2.Item2;
					if (notation != null)
					{
						notation.SystemLiteral = item2;
						notation.Pubid = item;
					}
					IL_252:
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 3;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseNotationDeclAsync>d__169>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_2B1:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.GreaterThan)
					{
						dtdParser.OnUnexpectedError();
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600175D RID: 5981 RVA: 0x00085734 File Offset: 0x00083934
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F1E RID: 3870
			public int <>1__state;

			// Token: 0x04000F1F RID: 3871
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F20 RID: 3872
			public DtdParser <>4__this;

			// Token: 0x04000F21 RID: 3873
			private SchemaNotation <notation>5__1;

			// Token: 0x04000F22 RID: 3874
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000F23 RID: 3875
			private ConfiguredTaskAwaitable<Tuple<string, string>>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000267 RID: 615
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseCommentAsync>d__170 : IAsyncStateMachine
		{
			// Token: 0x0600175E RID: 5982 RVA: 0x00085744 File Offset: 0x00083944
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					if (num > 1)
					{
						dtdParser.SaveParsingBuffer();
					}
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						if (num != 0)
						{
							if (num != 1)
							{
								if (dtdParser.SaveInternalSubsetValue)
								{
									configuredTaskAwaiter = dtdParser.readerAdapter.ParseCommentAsync(dtdParser.internalSubsetValueSb).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num2 = 0;
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseCommentAsync>d__170>(ref configuredTaskAwaiter, ref this);
										return;
									}
									goto IL_96;
								}
								else
								{
									configuredTaskAwaiter = dtdParser.readerAdapter.ParseCommentAsync(null).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num2 = 1;
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParseCommentAsync>d__170>(ref configuredTaskAwaiter, ref this);
										return;
									}
								}
							}
							else
							{
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num2 = -1;
							}
							configuredTaskAwaiter.GetResult();
							goto IL_11A;
						}
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						IL_96:
						configuredTaskAwaiter.GetResult();
						dtdParser.internalSubsetValueSb.Append("-->");
						IL_11A:;
					}
					catch (XmlException ex)
					{
						if (!(ex.ResString == "Unexpected end of file while parsing {0} has occurred.") || dtdParser.currentEntityId == 0)
						{
							throw;
						}
						dtdParser.SendValidationEvent(XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", null);
					}
					dtdParser.LoadParsingBuffer();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600175F RID: 5983 RVA: 0x00085904 File Offset: 0x00083B04
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F24 RID: 3876
			public int <>1__state;

			// Token: 0x04000F25 RID: 3877
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F26 RID: 3878
			public DtdParser <>4__this;

			// Token: 0x04000F27 RID: 3879
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000268 RID: 616
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParsePIAsync>d__171 : IAsyncStateMachine
		{
			// Token: 0x06001760 RID: 5984 RVA: 0x00085914 File Offset: 0x00083B14
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							dtdParser.SaveParsingBuffer();
							if (dtdParser.SaveInternalSubsetValue)
							{
								configuredTaskAwaiter = dtdParser.readerAdapter.ParsePIAsync(dtdParser.internalSubsetValueSb).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParsePIAsync>d__171>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_91;
							}
							else
							{
								configuredTaskAwaiter = dtdParser.readerAdapter.ParsePIAsync(null).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ParsePIAsync>d__171>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						goto IL_112;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_91:
					configuredTaskAwaiter.GetResult();
					dtdParser.internalSubsetValueSb.Append("?>");
					IL_112:
					dtdParser.LoadParsingBuffer();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001761 RID: 5985 RVA: 0x00085A84 File Offset: 0x00083C84
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F28 RID: 3880
			public int <>1__state;

			// Token: 0x04000F29 RID: 3881
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F2A RID: 3882
			public DtdParser <>4__this;

			// Token: 0x04000F2B RID: 3883
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000269 RID: 617
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseCondSectionAsync>d__172 : IAsyncStateMachine
		{
			// Token: 0x06001762 RID: 5986 RVA: 0x00085A94 File Offset: 0x00083C94
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_106;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_224;
					case 3:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2BE;
					default:
						csEntityId = dtdParser.currentEntityId;
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseCondSectionAsync>d__172>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					DtdParser.Token result = configuredTaskAwaiter3.GetResult();
					if (result != DtdParser.Token.IGNORE)
					{
						if (result != DtdParser.Token.INCLUDE)
						{
							goto IL_2F8;
						}
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseCondSectionAsync>d__172>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 2;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseCondSectionAsync>d__172>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_224;
					}
					IL_106:
					if (configuredTaskAwaiter3.GetResult() == DtdParser.Token.LeftBracket)
					{
						if (dtdParser.validate && csEntityId != dtdParser.currentEntityId)
						{
							dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
						}
						if (dtdParser.validate)
						{
							if (dtdParser.condSectionEntityIds == null)
							{
								dtdParser.condSectionEntityIds = new int[2];
							}
							else if (dtdParser.condSectionEntityIds.Length == dtdParser.condSectionDepth)
							{
								int[] array = new int[dtdParser.condSectionEntityIds.Length * 2];
								Array.Copy(dtdParser.condSectionEntityIds, 0, array, 0, dtdParser.condSectionEntityIds.Length);
								dtdParser.condSectionEntityIds = array;
							}
							dtdParser.condSectionEntityIds[dtdParser.condSectionDepth] = csEntityId;
						}
						dtdParser.condSectionDepth++;
						goto IL_2FE;
					}
					goto IL_2F8;
					IL_224:
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.LeftBracket)
					{
						goto IL_2F8;
					}
					if (dtdParser.validate && csEntityId != dtdParser.currentEntityId)
					{
						dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
					}
					configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 3;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseCondSectionAsync>d__172>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_2BE:
					if (configuredTaskAwaiter3.GetResult() == DtdParser.Token.CondSectionEnd)
					{
						if (dtdParser.validate && csEntityId != dtdParser.currentEntityId)
						{
							dtdParser.SendValidationEvent(dtdParser.curPos, XmlSeverityType.Error, "The parameter entity replacement text must nest properly within markup declarations.", string.Empty);
							goto IL_2FE;
						}
						goto IL_2FE;
					}
					IL_2F8:
					dtdParser.OnUnexpectedError();
					IL_2FE:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001763 RID: 5987 RVA: 0x00085DEC File Offset: 0x00083FEC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F2C RID: 3884
			public int <>1__state;

			// Token: 0x04000F2D RID: 3885
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F2E RID: 3886
			public DtdParser <>4__this;

			// Token: 0x04000F2F RID: 3887
			private int <csEntityId>5__1;

			// Token: 0x04000F30 RID: 3888
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200026A RID: 618
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseExternalIdAsync>d__173 : IAsyncStateMachine
		{
			// Token: 0x06001764 RID: 5988 RVA: 0x00085DFC File Offset: 0x00083FFC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				Tuple<string, string> result;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_26F;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_367;
					default:
						keywordLineInfo = new LineInfo(dtdParser.LineNo, dtdParser.LinePos - 6);
						publicId = null;
						systemId = null;
						configuredTaskAwaiter3 = dtdParser.GetTokenAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseExternalIdAsync>d__173>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					if (configuredTaskAwaiter3.GetResult() != DtdParser.Token.Literal)
					{
						dtdParser.ThrowUnexpectedToken(dtdParser.curPos, "\"", "'");
					}
					if (idTokenType == DtdParser.Token.SYSTEM)
					{
						systemId = dtdParser.GetValue();
						if (systemId.IndexOf('#') >= 0)
						{
							dtdParser.Throw(dtdParser.curPos - systemId.Length - 1, "Fragment identifier '{0}' cannot be part of the system identifier '{1}'.", new string[]
							{
								systemId.Substring(systemId.IndexOf('#')),
								systemId
							});
						}
						if (declType == DtdParser.Token.DOCTYPE && !dtdParser.freeFloatingDtd)
						{
							DtdParser dtdParser2 = dtdParser;
							dtdParser2.literalLineInfo.linePos = dtdParser2.literalLineInfo.linePos + 1;
							dtdParser.readerAdapter.OnSystemId(systemId, keywordLineInfo, dtdParser.literalLineInfo);
							goto IL_3D4;
						}
						goto IL_3D4;
					}
					else
					{
						publicId = dtdParser.GetValue();
						int num3;
						if ((num3 = dtdParser.xmlCharType.IsPublicId(publicId)) >= 0)
						{
							dtdParser.ThrowInvalidChar(dtdParser.curPos - 1 - publicId.Length + num3, publicId, num3);
						}
						if (declType == DtdParser.Token.DOCTYPE && !dtdParser.freeFloatingDtd)
						{
							DtdParser dtdParser3 = dtdParser;
							dtdParser3.literalLineInfo.linePos = dtdParser3.literalLineInfo.linePos + 1;
							dtdParser.readerAdapter.OnPublicId(publicId, keywordLineInfo, dtdParser.literalLineInfo);
							configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 1;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseExternalIdAsync>d__173>(ref configuredTaskAwaiter3, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter3 = dtdParser.GetTokenAsync(false).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 2;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ParseExternalIdAsync>d__173>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_367;
						}
					}
					IL_26F:
					if (configuredTaskAwaiter3.GetResult() == DtdParser.Token.Literal)
					{
						if (!dtdParser.whitespaceSeen)
						{
							dtdParser.Throw("'{0}' is an unexpected token. Expecting white space.", new string(dtdParser.literalQuoteChar, 1), dtdParser.literalLineInfo.lineNo, dtdParser.literalLineInfo.linePos);
						}
						systemId = dtdParser.GetValue();
						DtdParser dtdParser4 = dtdParser;
						dtdParser4.literalLineInfo.linePos = dtdParser4.literalLineInfo.linePos + 1;
						dtdParser.readerAdapter.OnSystemId(systemId, keywordLineInfo, dtdParser.literalLineInfo);
						goto IL_3D4;
					}
					dtdParser.ThrowUnexpectedToken(dtdParser.curPos, "\"", "'");
					goto IL_3D4;
					IL_367:
					if (configuredTaskAwaiter3.GetResult() == DtdParser.Token.Literal)
					{
						if (!dtdParser.whitespaceSeen)
						{
							dtdParser.Throw("'{0}' is an unexpected token. Expecting white space.", new string(dtdParser.literalQuoteChar, 1), dtdParser.literalLineInfo.lineNo, dtdParser.literalLineInfo.linePos);
						}
						systemId = dtdParser.GetValue();
					}
					else if (declType != DtdParser.Token.NOTATION)
					{
						dtdParser.ThrowUnexpectedToken(dtdParser.curPos, "\"", "'");
					}
					IL_3D4:
					result = new Tuple<string, string>(publicId, systemId);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001765 RID: 5989 RVA: 0x0008623C File Offset: 0x0008443C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F31 RID: 3889
			public int <>1__state;

			// Token: 0x04000F32 RID: 3890
			public AsyncTaskMethodBuilder<Tuple<string, string>> <>t__builder;

			// Token: 0x04000F33 RID: 3891
			public DtdParser <>4__this;

			// Token: 0x04000F34 RID: 3892
			public DtdParser.Token idTokenType;

			// Token: 0x04000F35 RID: 3893
			public DtdParser.Token declType;

			// Token: 0x04000F36 RID: 3894
			private LineInfo <keywordLineInfo>5__1;

			// Token: 0x04000F37 RID: 3895
			private string <publicId>5__2;

			// Token: 0x04000F38 RID: 3896
			private string <systemId>5__3;

			// Token: 0x04000F39 RID: 3897
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200026B RID: 619
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetTokenAsync>d__174 : IAsyncStateMachine
		{
			// Token: 0x06001766 RID: 5990 RVA: 0x0008624C File Offset: 0x0008444C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_286;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3CF;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_43D;
					}
					case 3:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_4AB;
					}
					case 4:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_519;
					}
					case 5:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_587;
					}
					case 6:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_601;
					}
					case 7:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_66F;
					}
					case 8:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_6DD;
					}
					case 9:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_77C;
					}
					case 10:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_7EB;
					}
					case 11:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_87E;
					}
					case 12:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_8F9;
					}
					case 13:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_968;
					}
					case 14:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_9D7;
					}
					case 15:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_A46;
					}
					case 16:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_AB5;
					}
					case 17:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_B24;
					}
					case 18:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_B93;
					}
					case 19:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_C02;
					}
					case 20:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
						configuredTaskAwaiter5 = configuredTaskAwaiter6;
						configuredTaskAwaiter6 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_C7D;
					}
					case 21:
						configuredTaskAwaiter7 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_D29;
					default:
						dtdParser.whitespaceSeen = false;
						break;
					}
					for (;;)
					{
						IL_73:
						char c = dtdParser.chars[dtdParser.curPos];
						if (c <= '\r')
						{
							if (c != '\0')
							{
								switch (c)
								{
								case '\t':
									goto IL_1BB;
								case '\n':
									dtdParser.whitespaceSeen = true;
									dtdParser.curPos++;
									dtdParser.readerAdapter.OnNewLine(dtdParser.curPos);
									continue;
								case '\r':
									dtdParser.whitespaceSeen = true;
									if (dtdParser.chars[dtdParser.curPos + 1] == '\n')
									{
										if (dtdParser.Normalize)
										{
											dtdParser.SaveParsingBuffer();
											IDtdParserAdapter readerAdapter = dtdParser.readerAdapter;
											int currentPosition = readerAdapter.CurrentPosition;
											readerAdapter.CurrentPosition = currentPosition + 1;
										}
										dtdParser.curPos += 2;
									}
									else
									{
										if (dtdParser.curPos + 1 >= dtdParser.charsUsed && !dtdParser.readerAdapter.IsEof)
										{
											goto IL_CB6;
										}
										dtdParser.chars[dtdParser.curPos] = '\n';
										dtdParser.curPos++;
									}
									dtdParser.readerAdapter.OnNewLine(dtdParser.curPos);
									continue;
								}
								break;
							}
							goto IL_C0;
						}
						else if (c != ' ')
						{
							if (c != '%')
							{
								break;
							}
							if (dtdParser.charsUsed - dtdParser.curPos < 2)
							{
								goto IL_CB6;
							}
							if (dtdParser.xmlCharType.IsWhiteSpace(dtdParser.chars[dtdParser.curPos + 1]))
							{
								break;
							}
							if (dtdParser.IgnoreEntityReferences)
							{
								dtdParser.curPos++;
								continue;
							}
							goto IL_222;
						}
						IL_1BB:
						dtdParser.whitespaceSeen = true;
						dtdParser.curPos++;
					}
					goto IL_293;
					IL_C0:
					if (dtdParser.curPos != dtdParser.charsUsed)
					{
						dtdParser.ThrowInvalidChar(dtdParser.chars, dtdParser.charsUsed, dtdParser.curPos);
						goto IL_CB6;
					}
					goto IL_CB6;
					IL_222:
					configuredTaskAwaiter3 = dtdParser.HandleEntityReferenceAsync(true, false, false).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_286;
					IL_293:
					if (needWhiteSpace && !dtdParser.whitespaceSeen && dtdParser.scanningFunction != DtdParser.ScanningFunction.ParamEntitySpace)
					{
						dtdParser.Throw(dtdParser.curPos, "'{0}' is an unexpected token. Expecting white space.", dtdParser.ParseUnexpectedToken(dtdParser.curPos));
					}
					dtdParser.tokenStartPos = dtdParser.curPos;
					for (;;)
					{
						switch (dtdParser.scanningFunction)
						{
						case DtdParser.ScanningFunction.SubsetContent:
							goto IL_4B8;
						case DtdParser.ScanningFunction.Name:
							goto IL_36E;
						case DtdParser.ScanningFunction.QName:
							goto IL_3DC;
						case DtdParser.ScanningFunction.Nmtoken:
							goto IL_44A;
						case DtdParser.ScanningFunction.Doctype1:
							goto IL_526;
						case DtdParser.ScanningFunction.Doctype2:
							goto IL_594;
						case DtdParser.ScanningFunction.Element1:
							goto IL_5A0;
						case DtdParser.ScanningFunction.Element2:
							goto IL_60E;
						case DtdParser.ScanningFunction.Element3:
							goto IL_67C;
						case DtdParser.ScanningFunction.Element4:
							goto IL_6EA;
						case DtdParser.ScanningFunction.Element5:
							goto IL_6F6;
						case DtdParser.ScanningFunction.Element6:
							goto IL_702;
						case DtdParser.ScanningFunction.Element7:
							goto IL_70E;
						case DtdParser.ScanningFunction.Attlist1:
							goto IL_71A;
						case DtdParser.ScanningFunction.Attlist2:
							goto IL_789;
						case DtdParser.ScanningFunction.Attlist3:
							goto IL_7F8;
						case DtdParser.ScanningFunction.Attlist4:
							goto IL_804;
						case DtdParser.ScanningFunction.Attlist5:
							goto IL_810;
						case DtdParser.ScanningFunction.Attlist6:
							goto IL_81C;
						case DtdParser.ScanningFunction.Attlist7:
							goto IL_88B;
						case DtdParser.ScanningFunction.Entity1:
							goto IL_A53;
						case DtdParser.ScanningFunction.Entity2:
							goto IL_AC2;
						case DtdParser.ScanningFunction.Entity3:
							goto IL_B31;
						case DtdParser.ScanningFunction.Notation1:
							goto IL_897;
						case DtdParser.ScanningFunction.CondSection1:
							goto IL_BA0;
						case DtdParser.ScanningFunction.CondSection2:
							goto IL_C0F;
						case DtdParser.ScanningFunction.CondSection3:
							goto IL_C1B;
						case DtdParser.ScanningFunction.SystemId:
							goto IL_906;
						case DtdParser.ScanningFunction.PublicId1:
							goto IL_975;
						case DtdParser.ScanningFunction.PublicId2:
							goto IL_9E4;
						case DtdParser.ScanningFunction.ClosingTag:
							goto IL_C8A;
						case DtdParser.ScanningFunction.ParamEntitySpace:
							dtdParser.whitespaceSeen = true;
							dtdParser.scanningFunction = dtdParser.savedScanningFunction;
							continue;
						}
						break;
					}
					goto IL_CAE;
					IL_36E:
					configuredTaskAwaiter5 = dtdParser.ScanNameExpectedAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_3CF;
					IL_3DC:
					configuredTaskAwaiter5 = dtdParser.ScanQNameExpectedAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_43D;
					IL_44A:
					configuredTaskAwaiter5 = dtdParser.ScanNmtokenExpectedAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_4AB;
					IL_4B8:
					configuredTaskAwaiter5 = dtdParser.ScanSubsetContentAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_519;
					IL_526:
					configuredTaskAwaiter5 = dtdParser.ScanDoctype1Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_587;
					IL_594:
					result = dtdParser.ScanDoctype2();
					goto IL_D84;
					IL_5A0:
					configuredTaskAwaiter5 = dtdParser.ScanElement1Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 6;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_601;
					IL_60E:
					configuredTaskAwaiter5 = dtdParser.ScanElement2Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 7;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_66F;
					IL_67C:
					configuredTaskAwaiter5 = dtdParser.ScanElement3Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 8;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_6DD;
					IL_6EA:
					result = dtdParser.ScanElement4();
					goto IL_D84;
					IL_6F6:
					result = dtdParser.ScanElement5();
					goto IL_D84;
					IL_702:
					result = dtdParser.ScanElement6();
					goto IL_D84;
					IL_70E:
					result = dtdParser.ScanElement7();
					goto IL_D84;
					IL_71A:
					configuredTaskAwaiter5 = dtdParser.ScanAttlist1Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 9;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_77C;
					IL_789:
					configuredTaskAwaiter5 = dtdParser.ScanAttlist2Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 10;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_7EB;
					IL_7F8:
					result = dtdParser.ScanAttlist3();
					goto IL_D84;
					IL_804:
					result = dtdParser.ScanAttlist4();
					goto IL_D84;
					IL_810:
					result = dtdParser.ScanAttlist5();
					goto IL_D84;
					IL_81C:
					configuredTaskAwaiter5 = dtdParser.ScanAttlist6Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 11;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_87E;
					IL_88B:
					result = dtdParser.ScanAttlist7();
					goto IL_D84;
					IL_897:
					configuredTaskAwaiter5 = dtdParser.ScanNotation1Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 12;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_8F9;
					IL_906:
					configuredTaskAwaiter5 = dtdParser.ScanSystemIdAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 13;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_968;
					IL_975:
					configuredTaskAwaiter5 = dtdParser.ScanPublicId1Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 14;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_9D7;
					IL_9E4:
					configuredTaskAwaiter5 = dtdParser.ScanPublicId2Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 15;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_A46;
					IL_A53:
					configuredTaskAwaiter5 = dtdParser.ScanEntity1Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 16;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_AB5;
					IL_AC2:
					configuredTaskAwaiter5 = dtdParser.ScanEntity2Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 17;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_B24;
					IL_B31:
					configuredTaskAwaiter5 = dtdParser.ScanEntity3Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 18;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_B93;
					IL_BA0:
					configuredTaskAwaiter5 = dtdParser.ScanCondSection1Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 19;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_C02;
					IL_C0F:
					result = dtdParser.ScanCondSection2();
					goto IL_D84;
					IL_C1B:
					configuredTaskAwaiter5 = dtdParser.ScanCondSection3Async().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 20;
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter6 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_C7D;
					IL_C8A:
					result = dtdParser.ScanClosingTag();
					goto IL_D84;
					IL_CAE:
					result = DtdParser.Token.None;
					goto IL_D84;
					IL_CB6:
					bool flag = dtdParser.readerAdapter.IsEof;
					if (flag)
					{
						goto IL_D35;
					}
					configuredTaskAwaiter7 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter7.IsCompleted)
					{
						num2 = 21;
						configuredTaskAwaiter2 = configuredTaskAwaiter7;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<GetTokenAsync>d__174>(ref configuredTaskAwaiter7, ref this);
						return;
					}
					goto IL_D29;
					IL_286:
					configuredTaskAwaiter3.GetResult();
					goto IL_73;
					IL_3CF:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_43D:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_4AB:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_519:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_587:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_601:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_66F:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_6DD:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_77C:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_7EB:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_87E:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_8F9:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_968:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_9D7:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_A46:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_AB5:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_B24:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_B93:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_C02:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_C7D:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_D84;
					IL_D29:
					flag = (configuredTaskAwaiter7.GetResult() == 0);
					IL_D35:
					if (!flag || dtdParser.HandleEntityEnd(false))
					{
						goto IL_73;
					}
					if (dtdParser.scanningFunction != DtdParser.ScanningFunction.SubsetContent)
					{
						dtdParser.Throw(dtdParser.curPos, "Incomplete DTD content.");
						goto IL_73;
					}
					result = DtdParser.Token.Eof;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_D84:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001767 RID: 5991 RVA: 0x00087010 File Offset: 0x00085210
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F3A RID: 3898
			public int <>1__state;

			// Token: 0x04000F3B RID: 3899
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F3C RID: 3900
			public DtdParser <>4__this;

			// Token: 0x04000F3D RID: 3901
			public bool needWhiteSpace;

			// Token: 0x04000F3E RID: 3902
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000F3F RID: 3903
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x04000F40 RID: 3904
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x0200026C RID: 620
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanSubsetContentAsync>d__175 : IAsyncStateMachine
		{
			// Token: 0x06001768 RID: 5992 RVA: 0x00087020 File Offset: 0x00085220
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_5BC;
					}
					IL_14:
					char c = dtdParser.chars[dtdParser.curPos];
					if (c != '<')
					{
						if (c == ']')
						{
							if (dtdParser.charsUsed - dtdParser.curPos < 2 && !dtdParser.readerAdapter.IsEof)
							{
								goto IL_55E;
							}
							if (dtdParser.chars[dtdParser.curPos + 1] != ']')
							{
								dtdParser.curPos++;
								dtdParser.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
								result = DtdParser.Token.RightBracket;
								goto IL_5F7;
							}
							if (dtdParser.charsUsed - dtdParser.curPos < 3 && !dtdParser.readerAdapter.IsEof)
							{
								goto IL_55E;
							}
							if (dtdParser.chars[dtdParser.curPos + 1] == ']' && dtdParser.chars[dtdParser.curPos + 2] == '>')
							{
								dtdParser.curPos += 3;
								result = DtdParser.Token.CondSectionEnd;
								goto IL_5F7;
							}
						}
						if (dtdParser.charsUsed - dtdParser.curPos != 0)
						{
							dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
						}
					}
					else
					{
						c = dtdParser.chars[dtdParser.curPos + 1];
						if (c != '!')
						{
							if (c == '?')
							{
								dtdParser.curPos += 2;
								result = DtdParser.Token.PI;
								goto IL_5F7;
							}
							if (dtdParser.charsUsed - dtdParser.curPos >= 2)
							{
								dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
								result = DtdParser.Token.None;
								goto IL_5F7;
							}
						}
						else
						{
							c = dtdParser.chars[dtdParser.curPos + 2];
							if (c <= 'A')
							{
								if (c != '-')
								{
									if (c == 'A')
									{
										if (dtdParser.charsUsed - dtdParser.curPos >= 9)
										{
											if (dtdParser.chars[dtdParser.curPos + 3] != 'T' || dtdParser.chars[dtdParser.curPos + 4] != 'T' || dtdParser.chars[dtdParser.curPos + 5] != 'L' || dtdParser.chars[dtdParser.curPos + 6] != 'I' || dtdParser.chars[dtdParser.curPos + 7] != 'S' || dtdParser.chars[dtdParser.curPos + 8] != 'T')
											{
												dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
											}
											dtdParser.curPos += 9;
											dtdParser.scanningFunction = DtdParser.ScanningFunction.QName;
											dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Attlist1;
											result = DtdParser.Token.AttlistDecl;
											goto IL_5F7;
										}
										goto IL_55E;
									}
								}
								else
								{
									if (dtdParser.chars[dtdParser.curPos + 3] == '-')
									{
										dtdParser.curPos += 4;
										result = DtdParser.Token.Comment;
										goto IL_5F7;
									}
									if (dtdParser.charsUsed - dtdParser.curPos >= 4)
									{
										dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
										goto IL_55E;
									}
									goto IL_55E;
								}
							}
							else if (c != 'E')
							{
								if (c != 'N')
								{
									if (c == '[')
									{
										dtdParser.curPos += 3;
										dtdParser.scanningFunction = DtdParser.ScanningFunction.CondSection1;
										result = DtdParser.Token.CondSectionStart;
										goto IL_5F7;
									}
								}
								else
								{
									if (dtdParser.charsUsed - dtdParser.curPos >= 10)
									{
										if (dtdParser.chars[dtdParser.curPos + 3] != 'O' || dtdParser.chars[dtdParser.curPos + 4] != 'T' || dtdParser.chars[dtdParser.curPos + 5] != 'A' || dtdParser.chars[dtdParser.curPos + 6] != 'T' || dtdParser.chars[dtdParser.curPos + 7] != 'I' || dtdParser.chars[dtdParser.curPos + 8] != 'O' || dtdParser.chars[dtdParser.curPos + 9] != 'N')
										{
											dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
										}
										dtdParser.curPos += 10;
										dtdParser.scanningFunction = DtdParser.ScanningFunction.Name;
										dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Notation1;
										result = DtdParser.Token.NotationDecl;
										goto IL_5F7;
									}
									goto IL_55E;
								}
							}
							else if (dtdParser.chars[dtdParser.curPos + 3] == 'L')
							{
								if (dtdParser.charsUsed - dtdParser.curPos >= 9)
								{
									if (dtdParser.chars[dtdParser.curPos + 4] != 'E' || dtdParser.chars[dtdParser.curPos + 5] != 'M' || dtdParser.chars[dtdParser.curPos + 6] != 'E' || dtdParser.chars[dtdParser.curPos + 7] != 'N' || dtdParser.chars[dtdParser.curPos + 8] != 'T')
									{
										dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
									}
									dtdParser.curPos += 9;
									dtdParser.scanningFunction = DtdParser.ScanningFunction.QName;
									dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Element1;
									result = DtdParser.Token.ElementDecl;
									goto IL_5F7;
								}
								goto IL_55E;
							}
							else if (dtdParser.chars[dtdParser.curPos + 3] == 'N')
							{
								if (dtdParser.charsUsed - dtdParser.curPos >= 8)
								{
									if (dtdParser.chars[dtdParser.curPos + 4] != 'T' || dtdParser.chars[dtdParser.curPos + 5] != 'I' || dtdParser.chars[dtdParser.curPos + 6] != 'T' || dtdParser.chars[dtdParser.curPos + 7] != 'Y')
									{
										dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
									}
									dtdParser.curPos += 8;
									dtdParser.scanningFunction = DtdParser.ScanningFunction.Entity1;
									result = DtdParser.Token.EntityDecl;
									goto IL_5F7;
								}
								goto IL_55E;
							}
							else
							{
								if (dtdParser.charsUsed - dtdParser.curPos >= 4)
								{
									dtdParser.Throw(dtdParser.curPos, "Expected DTD markup was not found.");
									result = DtdParser.Token.None;
									goto IL_5F7;
								}
								goto IL_55E;
							}
							if (dtdParser.charsUsed - dtdParser.curPos >= 3)
							{
								dtdParser.Throw(dtdParser.curPos + 2, "Expected DTD markup was not found.");
							}
						}
					}
					IL_55E:
					configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanSubsetContentAsync>d__175>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_5BC:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						dtdParser.Throw(dtdParser.charsUsed, "Incomplete DTD content.");
						goto IL_14;
					}
					goto IL_14;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_5F7:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001769 RID: 5993 RVA: 0x00087654 File Offset: 0x00085854
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F41 RID: 3905
			public int <>1__state;

			// Token: 0x04000F42 RID: 3906
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F43 RID: 3907
			public DtdParser <>4__this;

			// Token: 0x04000F44 RID: 3908
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200026D RID: 621
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanNameExpectedAsync>d__176 : IAsyncStateMachine
		{
			// Token: 0x0600176A RID: 5994 RVA: 0x00087664 File Offset: 0x00085864
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = dtdParser.ScanNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ScanNameExpectedAsync>d__176>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = dtdParser.nextScaningFunction;
					result = DtdParser.Token.Name;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600176B RID: 5995 RVA: 0x00087734 File Offset: 0x00085934
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F45 RID: 3909
			public int <>1__state;

			// Token: 0x04000F46 RID: 3910
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F47 RID: 3911
			public DtdParser <>4__this;

			// Token: 0x04000F48 RID: 3912
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200026E RID: 622
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanQNameExpectedAsync>d__177 : IAsyncStateMachine
		{
			// Token: 0x0600176C RID: 5996 RVA: 0x00087744 File Offset: 0x00085944
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = dtdParser.ScanQNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ScanQNameExpectedAsync>d__177>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = dtdParser.nextScaningFunction;
					result = DtdParser.Token.QName;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600176D RID: 5997 RVA: 0x00087814 File Offset: 0x00085A14
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F49 RID: 3913
			public int <>1__state;

			// Token: 0x04000F4A RID: 3914
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F4B RID: 3915
			public DtdParser <>4__this;

			// Token: 0x04000F4C RID: 3916
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200026F RID: 623
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanNmtokenExpectedAsync>d__178 : IAsyncStateMachine
		{
			// Token: 0x0600176E RID: 5998 RVA: 0x00087824 File Offset: 0x00085A24
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = dtdParser.ScanNmtokenAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ScanNmtokenExpectedAsync>d__178>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = dtdParser.nextScaningFunction;
					result = DtdParser.Token.Nmtoken;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600176F RID: 5999 RVA: 0x000878F4 File Offset: 0x00085AF4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F4D RID: 3917
			public int <>1__state;

			// Token: 0x04000F4E RID: 3918
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F4F RID: 3919
			public DtdParser <>4__this;

			// Token: 0x04000F50 RID: 3920
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000270 RID: 624
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanDoctype1Async>d__179 : IAsyncStateMachine
		{
			// Token: 0x06001770 RID: 6000 RVA: 0x00087904 File Offset: 0x00085B04
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num != 1)
						{
							char c = dtdParser.chars[dtdParser.curPos];
							if (c <= 'P')
							{
								if (c == '>')
								{
									dtdParser.curPos++;
									dtdParser.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
									result = DtdParser.Token.GreaterThan;
									goto IL_1D9;
								}
								if (c == 'P')
								{
									configuredTaskAwaiter3 = dtdParser.EatPublicKeywordAsync().ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter3.IsCompleted)
									{
										num2 = 0;
										configuredTaskAwaiter2 = configuredTaskAwaiter3;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanDoctype1Async>d__179>(ref configuredTaskAwaiter3, ref this);
										return;
									}
									goto IL_B6;
								}
							}
							else if (c != 'S')
							{
								if (c == '[')
								{
									dtdParser.curPos++;
									dtdParser.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
									result = DtdParser.Token.LeftBracket;
									goto IL_1D9;
								}
							}
							else
							{
								configuredTaskAwaiter3 = dtdParser.EatSystemKeywordAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanDoctype1Async>d__179>(ref configuredTaskAwaiter3, ref this);
									return;
								}
								goto IL_148;
							}
							dtdParser.Throw(dtdParser.curPos, "Expecting external ID, '[' or '>'.");
							result = DtdParser.Token.None;
							goto IL_1D9;
						}
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						IL_148:
						if (!configuredTaskAwaiter3.GetResult())
						{
							dtdParser.Throw(dtdParser.curPos, "Expecting external ID, '[' or '>'.");
						}
						dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Doctype2;
						dtdParser.scanningFunction = DtdParser.ScanningFunction.SystemId;
						result = DtdParser.Token.SYSTEM;
						goto IL_1D9;
					}
					configuredTaskAwaiter3 = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_B6:
					if (!configuredTaskAwaiter3.GetResult())
					{
						dtdParser.Throw(dtdParser.curPos, "Expecting external ID, '[' or '>'.");
					}
					dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Doctype2;
					dtdParser.scanningFunction = DtdParser.ScanningFunction.PublicId1;
					result = DtdParser.Token.PUBLIC;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_1D9:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001771 RID: 6001 RVA: 0x00087B1C File Offset: 0x00085D1C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F51 RID: 3921
			public int <>1__state;

			// Token: 0x04000F52 RID: 3922
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F53 RID: 3923
			public DtdParser <>4__this;

			// Token: 0x04000F54 RID: 3924
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000271 RID: 625
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanElement1Async>d__180 : IAsyncStateMachine
		{
			// Token: 0x06001772 RID: 6002 RVA: 0x00087B2C File Offset: 0x00085D2C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_19F;
					}
					IL_14:
					char c = dtdParser.chars[dtdParser.curPos];
					if (c == '(')
					{
						dtdParser.scanningFunction = DtdParser.ScanningFunction.Element2;
						dtdParser.curPos++;
						result = DtdParser.Token.LeftParen;
						goto IL_1DA;
					}
					if (c != 'A')
					{
						if (c == 'E')
						{
							if (dtdParser.charsUsed - dtdParser.curPos < 5)
							{
								goto IL_141;
							}
							if (dtdParser.chars[dtdParser.curPos + 1] == 'M' && dtdParser.chars[dtdParser.curPos + 2] == 'P' && dtdParser.chars[dtdParser.curPos + 3] == 'T' && dtdParser.chars[dtdParser.curPos + 4] == 'Y')
							{
								dtdParser.curPos += 5;
								dtdParser.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
								result = DtdParser.Token.EMPTY;
								goto IL_1DA;
							}
						}
					}
					else
					{
						if (dtdParser.charsUsed - dtdParser.curPos < 3)
						{
							goto IL_141;
						}
						if (dtdParser.chars[dtdParser.curPos + 1] == 'N' && dtdParser.chars[dtdParser.curPos + 2] == 'Y')
						{
							dtdParser.curPos += 3;
							dtdParser.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
							result = DtdParser.Token.ANY;
							goto IL_1DA;
						}
					}
					dtdParser.Throw(dtdParser.curPos, "Invalid content model.");
					IL_141:
					configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanElement1Async>d__180>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_19F:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						dtdParser.Throw(dtdParser.curPos, "Incomplete DTD content.");
						goto IL_14;
					}
					goto IL_14;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_1DA:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001773 RID: 6003 RVA: 0x00087D44 File Offset: 0x00085F44
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F55 RID: 3925
			public int <>1__state;

			// Token: 0x04000F56 RID: 3926
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F57 RID: 3927
			public DtdParser <>4__this;

			// Token: 0x04000F58 RID: 3928
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000272 RID: 626
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanElement2Async>d__181 : IAsyncStateMachine
		{
			// Token: 0x06001774 RID: 6004 RVA: 0x00087D54 File Offset: 0x00085F54
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (dtdParser.chars[dtdParser.curPos] == '#')
						{
							goto IL_9F;
						}
						goto IL_152;
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_85:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						dtdParser.Throw(dtdParser.curPos, "Incomplete DTD content.");
					}
					IL_9F:
					if (dtdParser.charsUsed - dtdParser.curPos >= 7)
					{
						if (dtdParser.chars[dtdParser.curPos + 1] == 'P' && dtdParser.chars[dtdParser.curPos + 2] == 'C' && dtdParser.chars[dtdParser.curPos + 3] == 'D' && dtdParser.chars[dtdParser.curPos + 4] == 'A' && dtdParser.chars[dtdParser.curPos + 5] == 'T' && dtdParser.chars[dtdParser.curPos + 6] == 'A')
						{
							dtdParser.curPos += 7;
							dtdParser.scanningFunction = DtdParser.ScanningFunction.Element6;
							result = DtdParser.Token.PCDATA;
							goto IL_177;
						}
						dtdParser.Throw(dtdParser.curPos + 1, "Expecting 'PCDATA'.");
					}
					else
					{
						configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanElement2Async>d__181>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_85;
					}
					IL_152:
					dtdParser.scanningFunction = DtdParser.ScanningFunction.Element3;
					result = DtdParser.Token.None;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_177:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001775 RID: 6005 RVA: 0x00087F08 File Offset: 0x00086108
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F59 RID: 3929
			public int <>1__state;

			// Token: 0x04000F5A RID: 3930
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F5B RID: 3931
			public DtdParser <>4__this;

			// Token: 0x04000F5C RID: 3932
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000273 RID: 627
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanElement3Async>d__182 : IAsyncStateMachine
		{
			// Token: 0x06001776 RID: 6006 RVA: 0x00087F18 File Offset: 0x00086118
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						char c = dtdParser.chars[dtdParser.curPos];
						if (c == '(')
						{
							dtdParser.curPos++;
							result = DtdParser.Token.LeftParen;
							goto IL_EC;
						}
						if (c == '>')
						{
							dtdParser.curPos++;
							dtdParser.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
							result = DtdParser.Token.GreaterThan;
							goto IL_EC;
						}
						configuredTaskAwaiter = dtdParser.ScanQNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ScanElement3Async>d__182>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = DtdParser.ScanningFunction.Element4;
					result = DtdParser.Token.QName;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_EC:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001777 RID: 6007 RVA: 0x00088038 File Offset: 0x00086238
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F5D RID: 3933
			public int <>1__state;

			// Token: 0x04000F5E RID: 3934
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F5F RID: 3935
			public DtdParser <>4__this;

			// Token: 0x04000F60 RID: 3936
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000274 RID: 628
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanAttlist1Async>d__183 : IAsyncStateMachine
		{
			// Token: 0x06001778 RID: 6008 RVA: 0x00088048 File Offset: 0x00086248
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						char c = dtdParser.chars[dtdParser.curPos];
						if (c == '>')
						{
							dtdParser.curPos++;
							dtdParser.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
							result = DtdParser.Token.GreaterThan;
							goto IL_F4;
						}
						if (!dtdParser.whitespaceSeen)
						{
							dtdParser.Throw(dtdParser.curPos, "'{0}' is an unexpected token. Expecting white space.", dtdParser.ParseUnexpectedToken(dtdParser.curPos));
						}
						configuredTaskAwaiter = dtdParser.ScanQNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ScanAttlist1Async>d__183>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist2;
					result = DtdParser.Token.QName;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_F4:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001779 RID: 6009 RVA: 0x00088170 File Offset: 0x00086370
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F61 RID: 3937
			public int <>1__state;

			// Token: 0x04000F62 RID: 3938
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F63 RID: 3939
			public DtdParser <>4__this;

			// Token: 0x04000F64 RID: 3940
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000275 RID: 629
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanAttlist2Async>d__184 : IAsyncStateMachine
		{
			// Token: 0x0600177A RID: 6010 RVA: 0x00088180 File Offset: 0x00086380
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_503;
					}
					IL_14:
					char c = dtdParser.chars[dtdParser.curPos];
					if (c <= 'C')
					{
						if (c == '(')
						{
							dtdParser.curPos++;
							dtdParser.scanningFunction = DtdParser.ScanningFunction.Nmtoken;
							dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Attlist5;
							result = DtdParser.Token.LeftParen;
							goto IL_53E;
						}
						if (c == 'C')
						{
							if (dtdParser.charsUsed - dtdParser.curPos >= 5)
							{
								if (dtdParser.chars[dtdParser.curPos + 1] != 'D' || dtdParser.chars[dtdParser.curPos + 2] != 'A' || dtdParser.chars[dtdParser.curPos + 3] != 'T' || dtdParser.chars[dtdParser.curPos + 4] != 'A')
								{
									dtdParser.Throw(dtdParser.curPos, "Invalid attribute type.");
								}
								dtdParser.curPos += 5;
								dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist6;
								result = DtdParser.Token.CDATA;
								goto IL_53E;
							}
							goto IL_4A5;
						}
					}
					else if (c != 'E')
					{
						if (c != 'I')
						{
							if (c == 'N')
							{
								if (dtdParser.charsUsed - dtdParser.curPos < 8 && !dtdParser.readerAdapter.IsEof)
								{
									goto IL_4A5;
								}
								c = dtdParser.chars[dtdParser.curPos + 1];
								if (c != 'M')
								{
									if (c == 'O')
									{
										if (dtdParser.chars[dtdParser.curPos + 2] != 'T' || dtdParser.chars[dtdParser.curPos + 3] != 'A' || dtdParser.chars[dtdParser.curPos + 4] != 'T' || dtdParser.chars[dtdParser.curPos + 5] != 'I' || dtdParser.chars[dtdParser.curPos + 6] != 'O' || dtdParser.chars[dtdParser.curPos + 7] != 'N')
										{
											dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
										}
										dtdParser.curPos += 8;
										dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist3;
										result = DtdParser.Token.NOTATION;
										goto IL_53E;
									}
									dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
									goto IL_4A5;
								}
								else
								{
									if (dtdParser.chars[dtdParser.curPos + 2] != 'T' || dtdParser.chars[dtdParser.curPos + 3] != 'O' || dtdParser.chars[dtdParser.curPos + 4] != 'K' || dtdParser.chars[dtdParser.curPos + 5] != 'E' || dtdParser.chars[dtdParser.curPos + 6] != 'N')
									{
										dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
									}
									dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist6;
									if (dtdParser.chars[dtdParser.curPos + 7] == 'S')
									{
										dtdParser.curPos += 8;
										result = DtdParser.Token.NMTOKENS;
										goto IL_53E;
									}
									dtdParser.curPos += 7;
									result = DtdParser.Token.NMTOKEN;
									goto IL_53E;
								}
							}
						}
						else
						{
							if (dtdParser.charsUsed - dtdParser.curPos < 6)
							{
								goto IL_4A5;
							}
							dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist6;
							if (dtdParser.chars[dtdParser.curPos + 1] != 'D')
							{
								dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
							}
							if (dtdParser.chars[dtdParser.curPos + 2] != 'R')
							{
								dtdParser.curPos += 2;
								result = DtdParser.Token.ID;
								goto IL_53E;
							}
							if (dtdParser.chars[dtdParser.curPos + 3] != 'E' || dtdParser.chars[dtdParser.curPos + 4] != 'F')
							{
								dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
							}
							if (dtdParser.chars[dtdParser.curPos + 5] != 'S')
							{
								dtdParser.curPos += 5;
								result = DtdParser.Token.IDREF;
								goto IL_53E;
							}
							dtdParser.curPos += 6;
							result = DtdParser.Token.IDREFS;
							goto IL_53E;
						}
					}
					else
					{
						if (dtdParser.charsUsed - dtdParser.curPos < 9)
						{
							goto IL_4A5;
						}
						dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist6;
						if (dtdParser.chars[dtdParser.curPos + 1] != 'N' || dtdParser.chars[dtdParser.curPos + 2] != 'T' || dtdParser.chars[dtdParser.curPos + 3] != 'I' || dtdParser.chars[dtdParser.curPos + 4] != 'T')
						{
							dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
						}
						c = dtdParser.chars[dtdParser.curPos + 5];
						if (c == 'I')
						{
							if (dtdParser.chars[dtdParser.curPos + 6] != 'E' || dtdParser.chars[dtdParser.curPos + 7] != 'S')
							{
								dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
							}
							dtdParser.curPos += 8;
							result = DtdParser.Token.ENTITIES;
							goto IL_53E;
						}
						if (c != 'Y')
						{
							dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
							goto IL_4A5;
						}
						dtdParser.curPos += 6;
						result = DtdParser.Token.ENTITY;
						goto IL_53E;
					}
					dtdParser.Throw(dtdParser.curPos, "'{0}' is an invalid attribute type.");
					IL_4A5:
					configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanAttlist2Async>d__184>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_503:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						dtdParser.Throw(dtdParser.curPos, "Incomplete DTD content.");
						goto IL_14;
					}
					goto IL_14;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_53E:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600177B RID: 6011 RVA: 0x000886FC File Offset: 0x000868FC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F65 RID: 3941
			public int <>1__state;

			// Token: 0x04000F66 RID: 3942
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F67 RID: 3943
			public DtdParser <>4__this;

			// Token: 0x04000F68 RID: 3944
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000276 RID: 630
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanAttlist6Async>d__185 : IAsyncStateMachine
		{
			// Token: 0x0600177C RID: 6012 RVA: 0x0008870C File Offset: 0x0008690C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					if (num != 0)
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num == 1)
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_36A;
						}
						IL_18:
						char c = dtdParser.chars[dtdParser.curPos];
						if (c != '"')
						{
							if (c != '#')
							{
								if (c == '\'')
								{
									goto IL_3B;
								}
								dtdParser.Throw(dtdParser.curPos, "Expecting an attribute type.");
							}
							else if (dtdParser.charsUsed - dtdParser.curPos >= 6)
							{
								c = dtdParser.chars[dtdParser.curPos + 1];
								if (c == 'F')
								{
									if (dtdParser.chars[dtdParser.curPos + 2] != 'I' || dtdParser.chars[dtdParser.curPos + 3] != 'X' || dtdParser.chars[dtdParser.curPos + 4] != 'E' || dtdParser.chars[dtdParser.curPos + 5] != 'D')
									{
										dtdParser.Throw(dtdParser.curPos, "Expecting an attribute type.");
									}
									dtdParser.curPos += 6;
									dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist7;
									result = DtdParser.Token.FIXED;
									goto IL_3A5;
								}
								if (c != 'I')
								{
									if (c == 'R')
									{
										if (dtdParser.charsUsed - dtdParser.curPos >= 9)
										{
											if (dtdParser.chars[dtdParser.curPos + 2] != 'E' || dtdParser.chars[dtdParser.curPos + 3] != 'Q' || dtdParser.chars[dtdParser.curPos + 4] != 'U' || dtdParser.chars[dtdParser.curPos + 5] != 'I' || dtdParser.chars[dtdParser.curPos + 6] != 'R' || dtdParser.chars[dtdParser.curPos + 7] != 'E' || dtdParser.chars[dtdParser.curPos + 8] != 'D')
											{
												dtdParser.Throw(dtdParser.curPos, "Expecting an attribute type.");
											}
											dtdParser.curPos += 9;
											dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist1;
											result = DtdParser.Token.REQUIRED;
											goto IL_3A5;
										}
									}
									else
									{
										dtdParser.Throw(dtdParser.curPos, "Expecting an attribute type.");
									}
								}
								else if (dtdParser.charsUsed - dtdParser.curPos >= 8)
								{
									if (dtdParser.chars[dtdParser.curPos + 2] != 'M' || dtdParser.chars[dtdParser.curPos + 3] != 'P' || dtdParser.chars[dtdParser.curPos + 4] != 'L' || dtdParser.chars[dtdParser.curPos + 5] != 'I' || dtdParser.chars[dtdParser.curPos + 6] != 'E' || dtdParser.chars[dtdParser.curPos + 7] != 'D')
									{
										dtdParser.Throw(dtdParser.curPos, "Expecting an attribute type.");
									}
									dtdParser.curPos += 8;
									dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist1;
									result = DtdParser.Token.IMPLIED;
									goto IL_3A5;
								}
							}
							configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 1;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanAttlist6Async>d__185>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_36A;
						}
						IL_3B:
						configuredTaskAwaiter4 = dtdParser.ScanLiteralAsync(DtdParser.LiteralType.AttributeValue).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ScanAttlist6Async>d__185>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_9D;
						IL_36A:
						if (configuredTaskAwaiter3.GetResult() == 0)
						{
							dtdParser.Throw(dtdParser.curPos, "Incomplete DTD content.");
							goto IL_18;
						}
						goto IL_18;
					}
					else
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_9D:
					configuredTaskAwaiter4.GetResult();
					dtdParser.scanningFunction = DtdParser.ScanningFunction.Attlist1;
					result = DtdParser.Token.Literal;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_3A5:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600177D RID: 6013 RVA: 0x00088AF0 File Offset: 0x00086CF0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F69 RID: 3945
			public int <>1__state;

			// Token: 0x04000F6A RID: 3946
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F6B RID: 3947
			public DtdParser <>4__this;

			// Token: 0x04000F6C RID: 3948
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000F6D RID: 3949
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000277 RID: 631
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanLiteralAsync>d__186 : IAsyncStateMachine
		{
			// Token: 0x0600177E RID: 6014 RVA: 0x00088B00 File Offset: 0x00086D00
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_45B;
					}
					case 1:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_536;
					case 2:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_611;
					case 3:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_6CC;
					}
					case 4:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_74D;
					case 5:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_8B4;
					default:
						quoteChar = dtdParser.chars[dtdParser.curPos];
						replaceChar = ((literalType == DtdParser.LiteralType.AttributeValue) ? ' ' : '\n');
						startQuoteEntityId = dtdParser.currentEntityId;
						dtdParser.literalLineInfo.Set(dtdParser.LineNo, dtdParser.LinePos);
						dtdParser.curPos++;
						dtdParser.tokenStartPos = dtdParser.curPos;
						dtdParser.stringBuilder.Length = 0;
						break;
					}
					for (;;)
					{
						IL_AC:
						if ((dtdParser.xmlCharType.charProperties[(int)dtdParser.chars[dtdParser.curPos]] & 128) == 0 || dtdParser.chars[dtdParser.curPos] == '%')
						{
							if (dtdParser.chars[dtdParser.curPos] == quoteChar && dtdParser.currentEntityId == startQuoteEntityId)
							{
								break;
							}
							int num3 = dtdParser.curPos - dtdParser.tokenStartPos;
							if (num3 > 0)
							{
								dtdParser.stringBuilder.Append(dtdParser.chars, dtdParser.tokenStartPos, num3);
								dtdParser.tokenStartPos = dtdParser.curPos;
							}
							char c = dtdParser.chars[dtdParser.curPos];
							if (c <= '\'')
							{
								switch (c)
								{
								case '\t':
									if (literalType == DtdParser.LiteralType.AttributeValue && dtdParser.Normalize)
									{
										dtdParser.stringBuilder.Append(' ');
										dtdParser.tokenStartPos++;
									}
									dtdParser.curPos++;
									continue;
								case '\n':
									dtdParser.curPos++;
									if (dtdParser.Normalize)
									{
										dtdParser.stringBuilder.Append(replaceChar);
										dtdParser.tokenStartPos = dtdParser.curPos;
									}
									dtdParser.readerAdapter.OnNewLine(dtdParser.curPos);
									continue;
								case '\v':
								case '\f':
									goto IL_7BB;
								case '\r':
									if (dtdParser.chars[dtdParser.curPos + 1] == '\n')
									{
										if (dtdParser.Normalize)
										{
											if (literalType == DtdParser.LiteralType.AttributeValue)
											{
												dtdParser.stringBuilder.Append(dtdParser.readerAdapter.IsEntityEolNormalized ? "  " : " ");
											}
											else
											{
												dtdParser.stringBuilder.Append(dtdParser.readerAdapter.IsEntityEolNormalized ? "\r\n" : "\n");
											}
											dtdParser.tokenStartPos = dtdParser.curPos + 2;
											dtdParser.SaveParsingBuffer();
											IDtdParserAdapter readerAdapter = dtdParser.readerAdapter;
											int currentPosition = readerAdapter.CurrentPosition;
											readerAdapter.CurrentPosition = currentPosition + 1;
										}
										dtdParser.curPos += 2;
									}
									else
									{
										if (dtdParser.curPos + 1 == dtdParser.charsUsed)
										{
											goto IL_842;
										}
										dtdParser.curPos++;
										if (dtdParser.Normalize)
										{
											dtdParser.stringBuilder.Append(replaceChar);
											dtdParser.tokenStartPos = dtdParser.curPos;
										}
									}
									dtdParser.readerAdapter.OnNewLine(dtdParser.curPos);
									continue;
								default:
									switch (c)
									{
									case '"':
									case '\'':
										break;
									case '#':
									case '$':
										goto IL_7BB;
									case '%':
										if (literalType != DtdParser.LiteralType.EntityReplText)
										{
											dtdParser.curPos++;
											continue;
										}
										goto IL_3EF;
									case '&':
										if (literalType == DtdParser.LiteralType.SystemOrPublicID)
										{
											dtdParser.curPos++;
											continue;
										}
										goto IL_490;
									default:
										goto IL_7BB;
									}
									break;
								}
							}
							else
							{
								if (c == '<')
								{
									if (literalType == DtdParser.LiteralType.AttributeValue)
									{
										dtdParser.Throw(dtdParser.curPos, "'{0}', hexadecimal value {1}, is an invalid attribute character.", XmlException.BuildCharExceptionArgs('<', '\0'));
									}
									dtdParser.curPos++;
									continue;
								}
								if (c != '>')
								{
									goto IL_7BB;
								}
							}
							dtdParser.curPos++;
							continue;
							IL_7BB:
							if (dtdParser.curPos == dtdParser.charsUsed)
							{
								goto IL_842;
							}
							if (!XmlCharType.IsHighSurrogate((int)dtdParser.chars[dtdParser.curPos]))
							{
								goto IL_822;
							}
							if (dtdParser.curPos + 1 == dtdParser.charsUsed)
							{
								goto IL_842;
							}
							dtdParser.curPos++;
							if (!XmlCharType.IsLowSurrogate((int)dtdParser.chars[dtdParser.curPos]))
							{
								goto IL_822;
							}
							dtdParser.curPos++;
						}
						else
						{
							dtdParser.curPos++;
						}
					}
					if (dtdParser.stringBuilder.Length > 0)
					{
						dtdParser.stringBuilder.Append(dtdParser.chars, dtdParser.tokenStartPos, dtdParser.curPos - dtdParser.tokenStartPos);
					}
					dtdParser.curPos++;
					dtdParser.literalQuoteChar = quoteChar;
					result = DtdParser.Token.Literal;
					goto IL_911;
					IL_3EF:
					configuredTaskAwaiter3 = dtdParser.HandleEntityReferenceAsync(true, true, literalType == DtdParser.LiteralType.AttributeValue).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanLiteralAsync>d__186>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_45B;
					IL_490:
					if (dtdParser.curPos + 1 == dtdParser.charsUsed)
					{
						goto IL_842;
					}
					if (dtdParser.chars[dtdParser.curPos + 1] == '#')
					{
						dtdParser.SaveParsingBuffer();
						configuredTaskAwaiter5 = dtdParser.readerAdapter.ParseNumericCharRefAsync(dtdParser.SaveInternalSubsetValue ? dtdParser.internalSubsetValueSb : null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanLiteralAsync>d__186>(ref configuredTaskAwaiter5, ref this);
							return;
						}
						goto IL_536;
					}
					else
					{
						dtdParser.SaveParsingBuffer();
						if (literalType == DtdParser.LiteralType.AttributeValue)
						{
							configuredTaskAwaiter5 = dtdParser.readerAdapter.ParseNamedCharRefAsync(true, dtdParser.SaveInternalSubsetValue ? dtdParser.internalSubsetValueSb : null).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 2;
								configuredTaskAwaiter2 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanLiteralAsync>d__186>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_611;
						}
						else
						{
							configuredTaskAwaiter5 = dtdParser.readerAdapter.ParseNamedCharRefAsync(false, null).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 4;
								configuredTaskAwaiter2 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanLiteralAsync>d__186>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_74D;
						}
					}
					IL_822:
					dtdParser.ThrowInvalidChar(dtdParser.chars, dtdParser.charsUsed, dtdParser.curPos);
					result = DtdParser.Token.None;
					goto IL_911;
					IL_842:
					bool flag = dtdParser.readerAdapter.IsEof;
					if (flag)
					{
						goto IL_8C0;
					}
					configuredTaskAwaiter5 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 5;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanLiteralAsync>d__186>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					goto IL_8B4;
					IL_45B:
					configuredTaskAwaiter3.GetResult();
					dtdParser.tokenStartPos = dtdParser.curPos;
					goto IL_AC;
					IL_536:
					int result2 = configuredTaskAwaiter5.GetResult();
					dtdParser.LoadParsingBuffer();
					dtdParser.stringBuilder.Append(dtdParser.chars, dtdParser.curPos, result2 - dtdParser.curPos);
					dtdParser.readerAdapter.CurrentPosition = result2;
					dtdParser.tokenStartPos = result2;
					dtdParser.curPos = result2;
					goto IL_AC;
					IL_611:
					int result3 = configuredTaskAwaiter5.GetResult();
					dtdParser.LoadParsingBuffer();
					if (result3 >= 0)
					{
						dtdParser.stringBuilder.Append(dtdParser.chars, dtdParser.curPos, result3 - dtdParser.curPos);
						dtdParser.readerAdapter.CurrentPosition = result3;
						dtdParser.tokenStartPos = result3;
						dtdParser.curPos = result3;
						goto IL_AC;
					}
					configuredTaskAwaiter3 = dtdParser.HandleEntityReferenceAsync(false, true, true).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanLiteralAsync>d__186>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_6CC:
					configuredTaskAwaiter3.GetResult();
					dtdParser.tokenStartPos = dtdParser.curPos;
					goto IL_AC;
					IL_74D:
					int result4 = configuredTaskAwaiter5.GetResult();
					dtdParser.LoadParsingBuffer();
					if (result4 >= 0)
					{
						dtdParser.tokenStartPos = dtdParser.curPos;
						dtdParser.curPos = result4;
						goto IL_AC;
					}
					dtdParser.stringBuilder.Append('&');
					dtdParser.curPos++;
					dtdParser.tokenStartPos = dtdParser.curPos;
					XmlQualifiedName entityName = dtdParser.ScanEntityName();
					dtdParser.VerifyEntityReference(entityName, false, false, false);
					goto IL_AC;
					IL_8B4:
					flag = (configuredTaskAwaiter5.GetResult() == 0);
					IL_8C0:
					if (flag && (literalType == DtdParser.LiteralType.SystemOrPublicID || !dtdParser.HandleEntityEnd(true)))
					{
						dtdParser.Throw(dtdParser.curPos, "There is an unclosed literal string.");
					}
					dtdParser.tokenStartPos = dtdParser.curPos;
					goto IL_AC;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_911:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600177F RID: 6015 RVA: 0x00089450 File Offset: 0x00087650
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F6E RID: 3950
			public int <>1__state;

			// Token: 0x04000F6F RID: 3951
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F70 RID: 3952
			public DtdParser <>4__this;

			// Token: 0x04000F71 RID: 3953
			public DtdParser.LiteralType literalType;

			// Token: 0x04000F72 RID: 3954
			private char <quoteChar>5__1;

			// Token: 0x04000F73 RID: 3955
			private int <startQuoteEntityId>5__2;

			// Token: 0x04000F74 RID: 3956
			private char <replaceChar>5__3;

			// Token: 0x04000F75 RID: 3957
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000F76 RID: 3958
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000278 RID: 632
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanNotation1Async>d__187 : IAsyncStateMachine
		{
			// Token: 0x06001780 RID: 6016 RVA: 0x00089460 File Offset: 0x00087660
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num != 1)
						{
							char c = dtdParser.chars[dtdParser.curPos];
							if (c != 'P')
							{
								if (c != 'S')
								{
									dtdParser.Throw(dtdParser.curPos, "Expecting a system identifier or a public identifier.");
									result = DtdParser.Token.None;
									goto IL_18A;
								}
								configuredTaskAwaiter3 = dtdParser.EatSystemKeywordAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanNotation1Async>d__187>(ref configuredTaskAwaiter3, ref this);
									return;
								}
							}
							else
							{
								configuredTaskAwaiter3 = dtdParser.EatPublicKeywordAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanNotation1Async>d__187>(ref configuredTaskAwaiter3, ref this);
									return;
								}
								goto IL_99;
							}
						}
						else
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						if (!configuredTaskAwaiter3.GetResult())
						{
							dtdParser.Throw(dtdParser.curPos, "Expecting external ID, '[' or '>'.");
						}
						dtdParser.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
						dtdParser.scanningFunction = DtdParser.ScanningFunction.SystemId;
						result = DtdParser.Token.SYSTEM;
						goto IL_18A;
					}
					configuredTaskAwaiter3 = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_99:
					if (!configuredTaskAwaiter3.GetResult())
					{
						dtdParser.Throw(dtdParser.curPos, "Expecting external ID, '[' or '>'.");
					}
					dtdParser.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
					dtdParser.scanningFunction = DtdParser.ScanningFunction.PublicId1;
					result = DtdParser.Token.PUBLIC;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_18A:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001781 RID: 6017 RVA: 0x00089628 File Offset: 0x00087828
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F77 RID: 3959
			public int <>1__state;

			// Token: 0x04000F78 RID: 3960
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F79 RID: 3961
			public DtdParser <>4__this;

			// Token: 0x04000F7A RID: 3962
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000279 RID: 633
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanSystemIdAsync>d__188 : IAsyncStateMachine
		{
			// Token: 0x06001782 RID: 6018 RVA: 0x00089638 File Offset: 0x00087838
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (dtdParser.chars[dtdParser.curPos] != '"' && dtdParser.chars[dtdParser.curPos] != '\'')
						{
							dtdParser.ThrowUnexpectedToken(dtdParser.curPos, "\"", "'");
						}
						configuredTaskAwaiter = dtdParser.ScanLiteralAsync(DtdParser.LiteralType.SystemOrPublicID).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ScanSystemIdAsync>d__188>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = dtdParser.nextScaningFunction;
					result = DtdParser.Token.Literal;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001783 RID: 6019 RVA: 0x00089740 File Offset: 0x00087940
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F7B RID: 3963
			public int <>1__state;

			// Token: 0x04000F7C RID: 3964
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F7D RID: 3965
			public DtdParser <>4__this;

			// Token: 0x04000F7E RID: 3966
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200027A RID: 634
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanEntity1Async>d__189 : IAsyncStateMachine
		{
			// Token: 0x06001784 RID: 6020 RVA: 0x00089750 File Offset: 0x00087950
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (dtdParser.chars[dtdParser.curPos] == '%')
						{
							dtdParser.curPos++;
							dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Entity2;
							dtdParser.scanningFunction = DtdParser.ScanningFunction.Name;
							result = DtdParser.Token.Percent;
							goto IL_CF;
						}
						configuredTaskAwaiter = dtdParser.ScanNameAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, DtdParser.<ScanEntity1Async>d__189>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = DtdParser.ScanningFunction.Entity2;
					result = DtdParser.Token.Name;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_CF:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001785 RID: 6021 RVA: 0x00089850 File Offset: 0x00087A50
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F7F RID: 3967
			public int <>1__state;

			// Token: 0x04000F80 RID: 3968
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F81 RID: 3969
			public DtdParser <>4__this;

			// Token: 0x04000F82 RID: 3970
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200027B RID: 635
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanEntity2Async>d__190 : IAsyncStateMachine
		{
			// Token: 0x06001786 RID: 6022 RVA: 0x00089860 File Offset: 0x00087A60
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_14E;
					case 2:
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1DF;
					}
					default:
					{
						char c = dtdParser.chars[dtdParser.curPos];
						if (c <= '\'')
						{
							if (c == '"' || c == '\'')
							{
								configuredTaskAwaiter4 = dtdParser.ScanLiteralAsync(DtdParser.LiteralType.EntityReplText).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter4.IsCompleted)
								{
									num2 = 2;
									ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ScanEntity2Async>d__190>(ref configuredTaskAwaiter4, ref this);
									return;
								}
								goto IL_1DF;
							}
						}
						else if (c != 'P')
						{
							if (c == 'S')
							{
								configuredTaskAwaiter3 = dtdParser.EatSystemKeywordAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanEntity2Async>d__190>(ref configuredTaskAwaiter3, ref this);
									return;
								}
								goto IL_14E;
							}
						}
						else
						{
							configuredTaskAwaiter3 = dtdParser.EatPublicKeywordAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanEntity2Async>d__190>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						}
						dtdParser.Throw(dtdParser.curPos, "Expecting an external identifier or an entity value.");
						result = DtdParser.Token.None;
						goto IL_223;
					}
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						dtdParser.Throw(dtdParser.curPos, "Expecting external ID, '[' or '>'.");
					}
					dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Entity3;
					dtdParser.scanningFunction = DtdParser.ScanningFunction.PublicId1;
					result = DtdParser.Token.PUBLIC;
					goto IL_223;
					IL_14E:
					if (!configuredTaskAwaiter3.GetResult())
					{
						dtdParser.Throw(dtdParser.curPos, "Expecting external ID, '[' or '>'.");
					}
					dtdParser.nextScaningFunction = DtdParser.ScanningFunction.Entity3;
					dtdParser.scanningFunction = DtdParser.ScanningFunction.SystemId;
					result = DtdParser.Token.SYSTEM;
					goto IL_223;
					IL_1DF:
					configuredTaskAwaiter4.GetResult();
					dtdParser.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
					result = DtdParser.Token.Literal;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_223:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001787 RID: 6023 RVA: 0x00089AC0 File Offset: 0x00087CC0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F83 RID: 3971
			public int <>1__state;

			// Token: 0x04000F84 RID: 3972
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F85 RID: 3973
			public DtdParser <>4__this;

			// Token: 0x04000F86 RID: 3974
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000F87 RID: 3975
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200027C RID: 636
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanEntity3Async>d__191 : IAsyncStateMachine
		{
			// Token: 0x06001788 RID: 6024 RVA: 0x00089AD0 File Offset: 0x00087CD0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (dtdParser.chars[dtdParser.curPos] == 'N')
						{
							goto IL_8E;
						}
						goto IL_10C;
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_85:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						goto IL_10C;
					}
					IL_8E:
					if (dtdParser.charsUsed - dtdParser.curPos >= 5)
					{
						if (dtdParser.chars[dtdParser.curPos + 1] == 'D' && dtdParser.chars[dtdParser.curPos + 2] == 'A' && dtdParser.chars[dtdParser.curPos + 3] == 'T' && dtdParser.chars[dtdParser.curPos + 4] == 'A')
						{
							dtdParser.curPos += 5;
							dtdParser.scanningFunction = DtdParser.ScanningFunction.Name;
							dtdParser.nextScaningFunction = DtdParser.ScanningFunction.ClosingTag;
							result = DtdParser.Token.NData;
							goto IL_132;
						}
					}
					else
					{
						configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanEntity3Async>d__191>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_85;
					}
					IL_10C:
					dtdParser.scanningFunction = DtdParser.ScanningFunction.ClosingTag;
					result = DtdParser.Token.None;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_132:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001789 RID: 6025 RVA: 0x00089C40 File Offset: 0x00087E40
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F88 RID: 3976
			public int <>1__state;

			// Token: 0x04000F89 RID: 3977
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F8A RID: 3978
			public DtdParser <>4__this;

			// Token: 0x04000F8B RID: 3979
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200027D RID: 637
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanPublicId1Async>d__192 : IAsyncStateMachine
		{
			// Token: 0x0600178A RID: 6026 RVA: 0x00089C50 File Offset: 0x00087E50
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (dtdParser.chars[dtdParser.curPos] != '"' && dtdParser.chars[dtdParser.curPos] != '\'')
						{
							dtdParser.ThrowUnexpectedToken(dtdParser.curPos, "\"", "'");
						}
						configuredTaskAwaiter = dtdParser.ScanLiteralAsync(DtdParser.LiteralType.SystemOrPublicID).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ScanPublicId1Async>d__192>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = DtdParser.ScanningFunction.PublicId2;
					result = DtdParser.Token.Literal;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600178B RID: 6027 RVA: 0x00089D54 File Offset: 0x00087F54
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F8C RID: 3980
			public int <>1__state;

			// Token: 0x04000F8D RID: 3981
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F8E RID: 3982
			public DtdParser <>4__this;

			// Token: 0x04000F8F RID: 3983
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200027E RID: 638
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanPublicId2Async>d__193 : IAsyncStateMachine
		{
			// Token: 0x0600178C RID: 6028 RVA: 0x00089D64 File Offset: 0x00087F64
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (dtdParser.chars[dtdParser.curPos] != '"' && dtdParser.chars[dtdParser.curPos] != '\'')
						{
							dtdParser.scanningFunction = dtdParser.nextScaningFunction;
							result = DtdParser.Token.None;
							goto IL_D5;
						}
						configuredTaskAwaiter = dtdParser.ScanLiteralAsync(DtdParser.LiteralType.SystemOrPublicID).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter, DtdParser.<ScanPublicId2Async>d__193>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					dtdParser.scanningFunction = dtdParser.nextScaningFunction;
					result = DtdParser.Token.Literal;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_D5:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600178D RID: 6029 RVA: 0x00089E6C File Offset: 0x0008806C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F90 RID: 3984
			public int <>1__state;

			// Token: 0x04000F91 RID: 3985
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F92 RID: 3986
			public DtdParser <>4__this;

			// Token: 0x04000F93 RID: 3987
			private ConfiguredTaskAwaitable<DtdParser.Token>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200027F RID: 639
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanCondSection1Async>d__194 : IAsyncStateMachine
		{
			// Token: 0x0600178E RID: 6030 RVA: 0x00089E7C File Offset: 0x0008807C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_241;
					}
					if (dtdParser.chars[dtdParser.curPos] != 'I')
					{
						dtdParser.Throw(dtdParser.curPos, "Conditional sections must specify the keyword 'IGNORE' or 'INCLUDE'.");
					}
					dtdParser.curPos++;
					IL_44:
					if (dtdParser.charsUsed - dtdParser.curPos >= 5)
					{
						char c = dtdParser.chars[dtdParser.curPos];
						if (c != 'G')
						{
							if (c == 'N')
							{
								if (dtdParser.charsUsed - dtdParser.curPos < 6)
								{
									goto IL_1E3;
								}
								if (dtdParser.chars[dtdParser.curPos + 1] == 'C' && dtdParser.chars[dtdParser.curPos + 2] == 'L' && dtdParser.chars[dtdParser.curPos + 3] == 'U' && dtdParser.chars[dtdParser.curPos + 4] == 'D' && dtdParser.chars[dtdParser.curPos + 5] == 'E' && !dtdParser.xmlCharType.IsNameSingleChar(dtdParser.chars[dtdParser.curPos + 6]))
								{
									dtdParser.nextScaningFunction = DtdParser.ScanningFunction.SubsetContent;
									dtdParser.scanningFunction = DtdParser.ScanningFunction.CondSection2;
									dtdParser.curPos += 6;
									result = DtdParser.Token.INCLUDE;
									goto IL_27C;
								}
							}
						}
						else if (dtdParser.chars[dtdParser.curPos + 1] == 'N' && dtdParser.chars[dtdParser.curPos + 2] == 'O' && dtdParser.chars[dtdParser.curPos + 3] == 'R' && dtdParser.chars[dtdParser.curPos + 4] == 'E' && !dtdParser.xmlCharType.IsNameSingleChar(dtdParser.chars[dtdParser.curPos + 5]))
						{
							dtdParser.nextScaningFunction = DtdParser.ScanningFunction.CondSection3;
							dtdParser.scanningFunction = DtdParser.ScanningFunction.CondSection2;
							dtdParser.curPos += 5;
							result = DtdParser.Token.IGNORE;
							goto IL_27C;
						}
						dtdParser.Throw(dtdParser.curPos - 1, "Conditional sections must specify the keyword 'IGNORE' or 'INCLUDE'.");
						result = DtdParser.Token.None;
						goto IL_27C;
					}
					IL_1E3:
					configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanCondSection1Async>d__194>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_241:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						dtdParser.Throw(dtdParser.curPos, "Incomplete DTD content.");
						goto IL_44;
					}
					goto IL_44;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_27C:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600178F RID: 6031 RVA: 0x0008A138 File Offset: 0x00088338
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F94 RID: 3988
			public int <>1__state;

			// Token: 0x04000F95 RID: 3989
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F96 RID: 3990
			public DtdParser <>4__this;

			// Token: 0x04000F97 RID: 3991
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000280 RID: 640
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanCondSection3Async>d__195 : IAsyncStateMachine
		{
			// Token: 0x06001790 RID: 6032 RVA: 0x0008A148 File Offset: 0x00088348
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				DtdParser.Token result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_352;
					}
					ignoreSectionDepth = 0;
					for (;;)
					{
						IL_2B:
						if ((dtdParser.xmlCharType.charProperties[(int)dtdParser.chars[dtdParser.curPos]] & 64) == 0 || dtdParser.chars[dtdParser.curPos] == ']')
						{
							char c = dtdParser.chars[dtdParser.curPos];
							if (c <= '&')
							{
								switch (c)
								{
								case '\t':
									break;
								case '\n':
									dtdParser.curPos++;
									dtdParser.readerAdapter.OnNewLine(dtdParser.curPos);
									continue;
								case '\v':
								case '\f':
									goto IL_259;
								case '\r':
									if (dtdParser.chars[dtdParser.curPos + 1] == '\n')
									{
										dtdParser.curPos += 2;
									}
									else
									{
										if (dtdParser.curPos + 1 >= dtdParser.charsUsed && !dtdParser.readerAdapter.IsEof)
										{
											goto IL_2E0;
										}
										dtdParser.curPos++;
									}
									dtdParser.readerAdapter.OnNewLine(dtdParser.curPos);
									continue;
								default:
									if (c != '"' && c != '&')
									{
										goto IL_259;
									}
									break;
								}
							}
							else if (c != '\'')
							{
								if (c != '<')
								{
									if (c != ']')
									{
										goto IL_259;
									}
									if (dtdParser.charsUsed - dtdParser.curPos < 3)
									{
										goto IL_2E0;
									}
									if (dtdParser.chars[dtdParser.curPos + 1] != ']' || dtdParser.chars[dtdParser.curPos + 2] != '>')
									{
										dtdParser.curPos++;
										continue;
									}
									if (ignoreSectionDepth > 0)
									{
										int num3 = ignoreSectionDepth;
										ignoreSectionDepth = num3 - 1;
										dtdParser.curPos += 3;
										continue;
									}
									break;
								}
								else
								{
									if (dtdParser.charsUsed - dtdParser.curPos < 3)
									{
										goto IL_2E0;
									}
									if (dtdParser.chars[dtdParser.curPos + 1] != '!' || dtdParser.chars[dtdParser.curPos + 2] != '[')
									{
										dtdParser.curPos++;
										continue;
									}
									int num3 = ignoreSectionDepth;
									ignoreSectionDepth = num3 + 1;
									dtdParser.curPos += 3;
									continue;
								}
							}
							dtdParser.curPos++;
							continue;
							IL_259:
							if (dtdParser.curPos == dtdParser.charsUsed)
							{
								goto IL_2E0;
							}
							if (!XmlCharType.IsHighSurrogate((int)dtdParser.chars[dtdParser.curPos]))
							{
								goto IL_2C0;
							}
							if (dtdParser.curPos + 1 == dtdParser.charsUsed)
							{
								goto IL_2E0;
							}
							dtdParser.curPos++;
							if (!XmlCharType.IsLowSurrogate((int)dtdParser.chars[dtdParser.curPos]))
							{
								goto IL_2C0;
							}
							dtdParser.curPos++;
						}
						else
						{
							dtdParser.curPos++;
						}
					}
					dtdParser.curPos += 3;
					dtdParser.scanningFunction = DtdParser.ScanningFunction.SubsetContent;
					result = DtdParser.Token.CondSectionEnd;
					goto IL_3A9;
					IL_2C0:
					dtdParser.ThrowInvalidChar(dtdParser.chars, dtdParser.charsUsed, dtdParser.curPos);
					result = DtdParser.Token.None;
					goto IL_3A9;
					IL_2E0:
					bool flag = dtdParser.readerAdapter.IsEof;
					if (flag)
					{
						goto IL_35E;
					}
					configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanCondSection3Async>d__195>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_352:
					flag = (configuredTaskAwaiter3.GetResult() == 0);
					IL_35E:
					if (flag)
					{
						if (dtdParser.HandleEntityEnd(false))
						{
							goto IL_2B;
						}
						dtdParser.Throw(dtdParser.curPos, "There is an unclosed conditional section.");
					}
					dtdParser.tokenStartPos = dtdParser.curPos;
					goto IL_2B;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_3A9:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001791 RID: 6033 RVA: 0x0008A530 File Offset: 0x00088730
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F98 RID: 3992
			public int <>1__state;

			// Token: 0x04000F99 RID: 3993
			public AsyncTaskMethodBuilder<DtdParser.Token> <>t__builder;

			// Token: 0x04000F9A RID: 3994
			public DtdParser <>4__this;

			// Token: 0x04000F9B RID: 3995
			private int <ignoreSectionDepth>5__1;

			// Token: 0x04000F9C RID: 3996
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000281 RID: 641
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanQNameAsync>d__198 : IAsyncStateMachine
		{
			// Token: 0x06001792 RID: 6034 RVA: 0x0008A540 File Offset: 0x00088740
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_10E;
					}
					if (num != 1)
					{
						dtdParser.tokenStartPos = dtdParser.curPos;
						colonOffset = -1;
						goto IL_2E;
					}
					configuredTaskAwaiter3 = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					num2 = -1;
					goto IL_240;
					for (;;)
					{
						IL_130:
						if ((dtdParser.xmlCharType.charProperties[(int)dtdParser.chars[dtdParser.curPos]] & 8) != 0)
						{
							dtdParser.curPos++;
						}
						else
						{
							if (dtdParser.chars[dtdParser.curPos] != ':')
							{
								goto IL_1D1;
							}
							if (isQName)
							{
								break;
							}
							dtdParser.curPos++;
						}
					}
					if (colonOffset != -1)
					{
						dtdParser.Throw(dtdParser.curPos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(':', '\0'));
					}
					colonOffset = dtdParser.curPos - dtdParser.tokenStartPos;
					dtdParser.curPos++;
					goto IL_2E;
					IL_1D1:
					if (dtdParser.curPos != dtdParser.charsUsed)
					{
						goto IL_270;
					}
					configuredTaskAwaiter3 = dtdParser.ReadDataInNameAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanQNameAsync>d__198>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_240;
					IL_2E:
					bool flag = false;
					if ((dtdParser.xmlCharType.charProperties[(int)dtdParser.chars[dtdParser.curPos]] & 4) != 0 || dtdParser.chars[dtdParser.curPos] == ':')
					{
						dtdParser.curPos++;
					}
					else if (dtdParser.curPos + 1 >= dtdParser.charsUsed)
					{
						flag = true;
					}
					else
					{
						dtdParser.Throw(dtdParser.curPos, "Name cannot begin with the '{0}' character, hexadecimal value {1}.", XmlException.BuildCharExceptionArgs(dtdParser.chars, dtdParser.charsUsed, dtdParser.curPos));
					}
					if (!flag)
					{
						goto IL_130;
					}
					configuredTaskAwaiter3 = dtdParser.ReadDataInNameAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, DtdParser.<ScanQNameAsync>d__198>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_10E:
					if (!configuredTaskAwaiter3.GetResult())
					{
						dtdParser.Throw(dtdParser.curPos, "Unexpected end of file while parsing {0} has occurred.", "Name");
						goto IL_130;
					}
					goto IL_2E;
					IL_240:
					if (configuredTaskAwaiter3.GetResult())
					{
						goto IL_130;
					}
					if (dtdParser.tokenStartPos == dtdParser.curPos)
					{
						dtdParser.Throw(dtdParser.curPos, "Unexpected end of file while parsing {0} has occurred.", "Name");
					}
					IL_270:
					dtdParser.colonPos = ((colonOffset == -1) ? -1 : (dtdParser.tokenStartPos + colonOffset));
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001793 RID: 6035 RVA: 0x0008A828 File Offset: 0x00088A28
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000F9D RID: 3997
			public int <>1__state;

			// Token: 0x04000F9E RID: 3998
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000F9F RID: 3999
			public DtdParser <>4__this;

			// Token: 0x04000FA0 RID: 4000
			public bool isQName;

			// Token: 0x04000FA1 RID: 4001
			private int <colonOffset>5__1;

			// Token: 0x04000FA2 RID: 4002
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000282 RID: 642
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadDataInNameAsync>d__199 : IAsyncStateMachine
		{
			// Token: 0x06001794 RID: 6036 RVA: 0x0008A838 File Offset: 0x00088A38
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						offset = dtdParser.curPos - dtdParser.tokenStartPos;
						dtdParser.curPos = dtdParser.tokenStartPos;
						configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ReadDataInNameAsync>d__199>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					bool flag = configuredTaskAwaiter3.GetResult() != 0;
					dtdParser.tokenStartPos = dtdParser.curPos;
					dtdParser.curPos += offset;
					result = flag;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001795 RID: 6037 RVA: 0x0008A93C File Offset: 0x00088B3C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000FA3 RID: 4003
			public int <>1__state;

			// Token: 0x04000FA4 RID: 4004
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000FA5 RID: 4005
			public DtdParser <>4__this;

			// Token: 0x04000FA6 RID: 4006
			private int <offset>5__1;

			// Token: 0x04000FA7 RID: 4007
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000283 RID: 643
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ScanNmtokenAsync>d__200 : IAsyncStateMachine
		{
			// Token: 0x06001796 RID: 6038 RVA: 0x0008A94C File Offset: 0x00088B4C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_124;
					}
					dtdParser.tokenStartPos = dtdParser.curPos;
					IL_20:
					while ((dtdParser.xmlCharType.charProperties[(int)dtdParser.chars[dtdParser.curPos]] & 8) != 0 || dtdParser.chars[dtdParser.curPos] == ':')
					{
						dtdParser.curPos++;
					}
					if (dtdParser.curPos < dtdParser.charsUsed)
					{
						if (dtdParser.curPos - dtdParser.tokenStartPos == 0)
						{
							dtdParser.Throw(dtdParser.curPos, "The '{0}' character, hexadecimal value {1}, cannot be included in a name.", XmlException.BuildCharExceptionArgs(dtdParser.chars, dtdParser.charsUsed, dtdParser.curPos));
						}
						goto IL_1AA;
					}
					len = dtdParser.curPos - dtdParser.tokenStartPos;
					dtdParser.curPos = dtdParser.tokenStartPos;
					configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ScanNmtokenAsync>d__200>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_124:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						if (len > 0)
						{
							dtdParser.tokenStartPos = dtdParser.curPos;
							dtdParser.curPos += len;
							goto IL_1AA;
						}
						dtdParser.Throw(dtdParser.curPos, "Unexpected end of file while parsing {0} has occurred.", "NmToken");
					}
					dtdParser.tokenStartPos = dtdParser.curPos;
					dtdParser.curPos += len;
					goto IL_20;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_1AA:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001797 RID: 6039 RVA: 0x0008AB34 File Offset: 0x00088D34
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000FA8 RID: 4008
			public int <>1__state;

			// Token: 0x04000FA9 RID: 4009
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000FAA RID: 4010
			public DtdParser <>4__this;

			// Token: 0x04000FAB RID: 4011
			private int <len>5__1;

			// Token: 0x04000FAC RID: 4012
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000284 RID: 644
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <EatPublicKeywordAsync>d__201 : IAsyncStateMachine
		{
			// Token: 0x06001798 RID: 6040 RVA: 0x0008AB44 File Offset: 0x00088D44
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				bool result;
				try
				{
					if (num != 0)
					{
						goto IL_81;
					}
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3 = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_71:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						result = false;
						goto IL_11F;
					}
					IL_81:
					if (dtdParser.charsUsed - dtdParser.curPos >= 6)
					{
						if (dtdParser.chars[dtdParser.curPos + 1] != 'U' || dtdParser.chars[dtdParser.curPos + 2] != 'B' || dtdParser.chars[dtdParser.curPos + 3] != 'L' || dtdParser.chars[dtdParser.curPos + 4] != 'I' || dtdParser.chars[dtdParser.curPos + 5] != 'C')
						{
							result = false;
						}
						else
						{
							dtdParser.curPos += 6;
							result = true;
						}
					}
					else
					{
						configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<EatPublicKeywordAsync>d__201>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_71;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_11F:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001799 RID: 6041 RVA: 0x0008AC94 File Offset: 0x00088E94
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000FAD RID: 4013
			public int <>1__state;

			// Token: 0x04000FAE RID: 4014
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000FAF RID: 4015
			public DtdParser <>4__this;

			// Token: 0x04000FB0 RID: 4016
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000285 RID: 645
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <EatSystemKeywordAsync>d__202 : IAsyncStateMachine
		{
			// Token: 0x0600179A RID: 6042 RVA: 0x0008ACA4 File Offset: 0x00088EA4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				bool result;
				try
				{
					if (num != 0)
					{
						goto IL_81;
					}
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3 = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_71:
					if (configuredTaskAwaiter3.GetResult() == 0)
					{
						result = false;
						goto IL_11F;
					}
					IL_81:
					if (dtdParser.charsUsed - dtdParser.curPos >= 6)
					{
						if (dtdParser.chars[dtdParser.curPos + 1] != 'Y' || dtdParser.chars[dtdParser.curPos + 2] != 'S' || dtdParser.chars[dtdParser.curPos + 3] != 'T' || dtdParser.chars[dtdParser.curPos + 4] != 'E' || dtdParser.chars[dtdParser.curPos + 5] != 'M')
						{
							result = false;
						}
						else
						{
							dtdParser.curPos += 6;
							result = true;
						}
					}
					else
					{
						configuredTaskAwaiter3 = dtdParser.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<EatSystemKeywordAsync>d__202>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_71;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_11F:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600179B RID: 6043 RVA: 0x0008ADF4 File Offset: 0x00088FF4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000FB1 RID: 4017
			public int <>1__state;

			// Token: 0x04000FB2 RID: 4018
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000FB3 RID: 4019
			public DtdParser <>4__this;

			// Token: 0x04000FB4 RID: 4020
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000286 RID: 646
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadDataAsync>d__203 : IAsyncStateMachine
		{
			// Token: 0x0600179C RID: 6044 RVA: 0x0008AE04 File Offset: 0x00089004
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				int result2;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						dtdParser.SaveParsingBuffer();
						configuredTaskAwaiter = dtdParser.readerAdapter.ReadDataAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, DtdParser.<ReadDataAsync>d__203>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result = configuredTaskAwaiter.GetResult();
					dtdParser.LoadParsingBuffer();
					result2 = result;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x0600179D RID: 6045 RVA: 0x0008AED8 File Offset: 0x000890D8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000FB5 RID: 4021
			public int <>1__state;

			// Token: 0x04000FB6 RID: 4022
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000FB7 RID: 4023
			public DtdParser <>4__this;

			// Token: 0x04000FB8 RID: 4024
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000287 RID: 647
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <HandleEntityReferenceAsync>d__205 : IAsyncStateMachine
		{
			// Token: 0x0600179E RID: 6046 RVA: 0x0008AEE8 File Offset: 0x000890E8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				DtdParser dtdParser = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					int item;
					if (num != 0)
					{
						if (num != 1)
						{
							dtdParser.SaveParsingBuffer();
							if (paramEntity && dtdParser.ParsingInternalSubset && !dtdParser.ParsingTopLevelMarkup)
							{
								dtdParser.Throw(dtdParser.curPos - entityName.Name.Length - 1, "A parameter entity reference is not allowed in internal markup.");
							}
							entity = dtdParser.VerifyEntityReference(entityName, paramEntity, true, inAttribute);
							if (entity == null)
							{
								result = false;
								goto IL_277;
							}
							if (entity.ParsingInProgress)
							{
								dtdParser.Throw(dtdParser.curPos - entityName.Name.Length - 1, paramEntity ? "Parameter entity '{0}' references itself." : "General entity '{0}' references itself.", entityName.Name);
							}
							if (entity.IsExternal)
							{
								configuredTaskAwaiter = dtdParser.readerAdapter.PushEntityAsync(entity).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter, DtdParser.<HandleEntityReferenceAsync>d__205>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_152;
							}
							else
							{
								if (entity.Text.Length == 0)
								{
									result = false;
									goto IL_277;
								}
								configuredTaskAwaiter = dtdParser.readerAdapter.PushEntityAsync(entity).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 1;
									ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter, DtdParser.<HandleEntityReferenceAsync>d__205>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
						}
						else
						{
							ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						Tuple<int, bool> result2 = configuredTaskAwaiter.GetResult();
						item = result2.Item1;
						if (!result2.Item2)
						{
							result = false;
							goto IL_277;
						}
						goto IL_21F;
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_152:
					Tuple<int, bool> result3 = configuredTaskAwaiter.GetResult();
					item = result3.Item1;
					if (!result3.Item2)
					{
						result = false;
						goto IL_277;
					}
					dtdParser.externalEntitiesDepth++;
					IL_21F:
					dtdParser.currentEntityId = item;
					if (paramEntity && !inLiteral && dtdParser.scanningFunction != DtdParser.ScanningFunction.ParamEntitySpace)
					{
						dtdParser.savedScanningFunction = dtdParser.scanningFunction;
						dtdParser.scanningFunction = DtdParser.ScanningFunction.ParamEntitySpace;
					}
					dtdParser.LoadParsingBuffer();
					result = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_277:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600179F RID: 6047 RVA: 0x0008B19C File Offset: 0x0008939C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000FB9 RID: 4025
			public int <>1__state;

			// Token: 0x04000FBA RID: 4026
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000FBB RID: 4027
			public DtdParser <>4__this;

			// Token: 0x04000FBC RID: 4028
			public bool paramEntity;

			// Token: 0x04000FBD RID: 4029
			public XmlQualifiedName entityName;

			// Token: 0x04000FBE RID: 4030
			public bool inAttribute;

			// Token: 0x04000FBF RID: 4031
			private SchemaEntity <entity>5__1;

			// Token: 0x04000FC0 RID: 4032
			public bool inLiteral;

			// Token: 0x04000FC1 RID: 4033
			private ConfiguredTaskAwaitable<Tuple<int, bool>>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
