﻿using System;

namespace System.Xml
{
	// Token: 0x0200024A RID: 586
	internal class PositionInfo : IXmlLineInfo
	{
		// Token: 0x06001685 RID: 5765 RVA: 0x000020CD File Offset: 0x000002CD
		public virtual bool HasLineInfo()
		{
			return false;
		}

		// Token: 0x17000486 RID: 1158
		// (get) Token: 0x06001686 RID: 5766 RVA: 0x000020CD File Offset: 0x000002CD
		public virtual int LineNumber
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000487 RID: 1159
		// (get) Token: 0x06001687 RID: 5767 RVA: 0x000020CD File Offset: 0x000002CD
		public virtual int LinePosition
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06001688 RID: 5768 RVA: 0x0007C014 File Offset: 0x0007A214
		public static PositionInfo GetPositionInfo(object o)
		{
			IXmlLineInfo xmlLineInfo = o as IXmlLineInfo;
			if (xmlLineInfo != null)
			{
				return new ReaderPositionInfo(xmlLineInfo);
			}
			return new PositionInfo();
		}

		// Token: 0x06001689 RID: 5769 RVA: 0x00002103 File Offset: 0x00000303
		public PositionInfo()
		{
		}
	}
}
