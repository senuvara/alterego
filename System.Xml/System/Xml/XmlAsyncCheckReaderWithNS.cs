﻿using System;
using System.Collections.Generic;

namespace System.Xml
{
	// Token: 0x020000C9 RID: 201
	internal class XmlAsyncCheckReaderWithNS : XmlAsyncCheckReader, IXmlNamespaceResolver
	{
		// Token: 0x060006C9 RID: 1737 RVA: 0x0001BF4B File Offset: 0x0001A14B
		public XmlAsyncCheckReaderWithNS(XmlReader reader) : base(reader)
		{
			this.readerAsIXmlNamespaceResolver = (IXmlNamespaceResolver)reader;
		}

		// Token: 0x060006CA RID: 1738 RVA: 0x0001BF60 File Offset: 0x0001A160
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return this.readerAsIXmlNamespaceResolver.GetNamespacesInScope(scope);
		}

		// Token: 0x060006CB RID: 1739 RVA: 0x0001BF6E File Offset: 0x0001A16E
		string IXmlNamespaceResolver.LookupNamespace(string prefix)
		{
			return this.readerAsIXmlNamespaceResolver.LookupNamespace(prefix);
		}

		// Token: 0x060006CC RID: 1740 RVA: 0x0001BF7C File Offset: 0x0001A17C
		string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
		{
			return this.readerAsIXmlNamespaceResolver.LookupPrefix(namespaceName);
		}

		// Token: 0x040003D9 RID: 985
		private readonly IXmlNamespaceResolver readerAsIXmlNamespaceResolver;
	}
}
