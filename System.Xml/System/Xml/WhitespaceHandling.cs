﻿using System;

namespace System.Xml
{
	/// <summary>Specifies how white space is handled.</summary>
	// Token: 0x020000C7 RID: 199
	public enum WhitespaceHandling
	{
		/// <summary>Return <see langword="Whitespace" /> and <see langword="SignificantWhitespace" /> nodes. This is the default.</summary>
		// Token: 0x040003D4 RID: 980
		All,
		/// <summary>Return <see langword="SignificantWhitespace" /> nodes only.</summary>
		// Token: 0x040003D5 RID: 981
		Significant,
		/// <summary>Return no <see langword="Whitespace" /> and no <see langword="SignificantWhitespace" /> nodes.</summary>
		// Token: 0x040003D6 RID: 982
		None
	}
}
