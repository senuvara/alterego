﻿using System;

namespace System.Xml
{
	// Token: 0x020001EB RID: 491
	internal enum XmlStandalone
	{
		// Token: 0x04000C35 RID: 3125
		Omit,
		// Token: 0x04000C36 RID: 3126
		Yes,
		// Token: 0x04000C37 RID: 3127
		No
	}
}
