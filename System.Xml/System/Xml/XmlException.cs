﻿using System;
using System.Globalization;
using System.Resources;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Threading;

namespace System.Xml
{
	/// <summary>Returns detailed information about the last exception.</summary>
	// Token: 0x020002A2 RID: 674
	[Serializable]
	public class XmlException : SystemException
	{
		/// <summary>Initializes a new instance of the <see langword="XmlException" /> class using the information in the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" /> objects.</summary>
		/// <param name="info">The <see langword="SerializationInfo" /> object containing all the properties of an <see langword="XmlException" />. </param>
		/// <param name="context">The <see langword="StreamingContext" /> object containing the context information. </param>
		// Token: 0x060018B6 RID: 6326 RVA: 0x0008EDB8 File Offset: 0x0008CFB8
		protected XmlException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.res = (string)info.GetValue("res", typeof(string));
			this.args = (string[])info.GetValue("args", typeof(string[]));
			this.lineNumber = (int)info.GetValue("lineNumber", typeof(int));
			this.linePosition = (int)info.GetValue("linePosition", typeof(int));
			this.sourceUri = string.Empty;
			string text = null;
			foreach (SerializationEntry serializationEntry in info)
			{
				string name = serializationEntry.Name;
				if (!(name == "sourceUri"))
				{
					if (name == "version")
					{
						text = (string)serializationEntry.Value;
					}
				}
				else
				{
					this.sourceUri = (string)serializationEntry.Value;
				}
			}
			if (text == null)
			{
				this.message = XmlException.CreateMessage(this.res, this.args, this.lineNumber, this.linePosition);
				return;
			}
			this.message = null;
		}

		/// <summary>Streams all the <see langword="XmlException" /> properties into the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> class for the given <see cref="T:System.Runtime.Serialization.StreamingContext" />.</summary>
		/// <param name="info">The <see langword="SerializationInfo" /> object. </param>
		/// <param name="context">The <see langword="StreamingContext" /> object. </param>
		// Token: 0x060018B7 RID: 6327 RVA: 0x0008EEE8 File Offset: 0x0008D0E8
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("res", this.res);
			info.AddValue("args", this.args);
			info.AddValue("lineNumber", this.lineNumber);
			info.AddValue("linePosition", this.linePosition);
			info.AddValue("sourceUri", this.sourceUri);
			info.AddValue("version", "2.0");
		}

		/// <summary>Initializes a new instance of the <see langword="XmlException" /> class.</summary>
		// Token: 0x060018B8 RID: 6328 RVA: 0x0008EF62 File Offset: 0x0008D162
		public XmlException() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see langword="XmlException" /> class with a specified error message.</summary>
		/// <param name="message">The error description. </param>
		// Token: 0x060018B9 RID: 6329 RVA: 0x0008EF6B File Offset: 0x0008D16B
		public XmlException(string message) : this(message, null, 0, 0)
		{
		}

		/// <summary>Initializes a new instance of the <see langword="XmlException" /> class.</summary>
		/// <param name="message">The description of the error condition. </param>
		/// <param name="innerException">The <see cref="T:System.Exception" /> that threw the <see langword="XmlException" />, if any. This value can be <see langword="null" />. </param>
		// Token: 0x060018BA RID: 6330 RVA: 0x0008EF77 File Offset: 0x0008D177
		public XmlException(string message, Exception innerException) : this(message, innerException, 0, 0)
		{
		}

		/// <summary>Initializes a new instance of the <see langword="XmlException" /> class with the specified message, inner exception, line number, and line position.</summary>
		/// <param name="message">The error description. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. This value can be <see langword="null" />. </param>
		/// <param name="lineNumber">The line number indicating where the error occurred. </param>
		/// <param name="linePosition">The line position indicating where the error occurred. </param>
		// Token: 0x060018BB RID: 6331 RVA: 0x0008EF83 File Offset: 0x0008D183
		public XmlException(string message, Exception innerException, int lineNumber, int linePosition) : this(message, innerException, lineNumber, linePosition, null)
		{
		}

		// Token: 0x060018BC RID: 6332 RVA: 0x0008EF94 File Offset: 0x0008D194
		internal XmlException(string message, Exception innerException, int lineNumber, int linePosition, string sourceUri) : base(XmlException.FormatUserMessage(message, lineNumber, linePosition), innerException)
		{
			base.HResult = -2146232000;
			this.res = ((message == null) ? "An XML error has occurred." : "{0}");
			this.args = new string[]
			{
				message
			};
			this.sourceUri = sourceUri;
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		// Token: 0x060018BD RID: 6333 RVA: 0x0008EFF8 File Offset: 0x0008D1F8
		internal XmlException(string res, string[] args) : this(res, args, null, 0, 0, null)
		{
		}

		// Token: 0x060018BE RID: 6334 RVA: 0x0008F006 File Offset: 0x0008D206
		internal XmlException(string res, string[] args, string sourceUri) : this(res, args, null, 0, 0, sourceUri)
		{
		}

		// Token: 0x060018BF RID: 6335 RVA: 0x0008F014 File Offset: 0x0008D214
		internal XmlException(string res, string arg) : this(res, new string[]
		{
			arg
		}, null, 0, 0, null)
		{
		}

		// Token: 0x060018C0 RID: 6336 RVA: 0x0008F02B File Offset: 0x0008D22B
		internal XmlException(string res, string arg, string sourceUri) : this(res, new string[]
		{
			arg
		}, null, 0, 0, sourceUri)
		{
		}

		// Token: 0x060018C1 RID: 6337 RVA: 0x0008F042 File Offset: 0x0008D242
		internal XmlException(string res, string arg, IXmlLineInfo lineInfo) : this(res, new string[]
		{
			arg
		}, lineInfo, null)
		{
		}

		// Token: 0x060018C2 RID: 6338 RVA: 0x0008F057 File Offset: 0x0008D257
		internal XmlException(string res, string arg, Exception innerException, IXmlLineInfo lineInfo) : this(res, new string[]
		{
			arg
		}, innerException, (lineInfo == null) ? 0 : lineInfo.LineNumber, (lineInfo == null) ? 0 : lineInfo.LinePosition, null)
		{
		}

		// Token: 0x060018C3 RID: 6339 RVA: 0x0008F088 File Offset: 0x0008D288
		internal XmlException(string res, string arg, IXmlLineInfo lineInfo, string sourceUri) : this(res, new string[]
		{
			arg
		}, lineInfo, sourceUri)
		{
		}

		// Token: 0x060018C4 RID: 6340 RVA: 0x0008F09E File Offset: 0x0008D29E
		internal XmlException(string res, string[] args, IXmlLineInfo lineInfo) : this(res, args, lineInfo, null)
		{
		}

		// Token: 0x060018C5 RID: 6341 RVA: 0x0008F0AA File Offset: 0x0008D2AA
		internal XmlException(string res, string[] args, IXmlLineInfo lineInfo, string sourceUri) : this(res, args, null, (lineInfo == null) ? 0 : lineInfo.LineNumber, (lineInfo == null) ? 0 : lineInfo.LinePosition, sourceUri)
		{
		}

		// Token: 0x060018C6 RID: 6342 RVA: 0x0008F0CF File Offset: 0x0008D2CF
		internal XmlException(string res, int lineNumber, int linePosition) : this(res, null, null, lineNumber, linePosition)
		{
		}

		// Token: 0x060018C7 RID: 6343 RVA: 0x0008F0DC File Offset: 0x0008D2DC
		internal XmlException(string res, string arg, int lineNumber, int linePosition) : this(res, new string[]
		{
			arg
		}, null, lineNumber, linePosition, null)
		{
		}

		// Token: 0x060018C8 RID: 6344 RVA: 0x0008F0F4 File Offset: 0x0008D2F4
		internal XmlException(string res, string arg, int lineNumber, int linePosition, string sourceUri) : this(res, new string[]
		{
			arg
		}, null, lineNumber, linePosition, sourceUri)
		{
		}

		// Token: 0x060018C9 RID: 6345 RVA: 0x0008F10D File Offset: 0x0008D30D
		internal XmlException(string res, string[] args, int lineNumber, int linePosition) : this(res, args, null, lineNumber, linePosition, null)
		{
		}

		// Token: 0x060018CA RID: 6346 RVA: 0x0008F11C File Offset: 0x0008D31C
		internal XmlException(string res, string[] args, int lineNumber, int linePosition, string sourceUri) : this(res, args, null, lineNumber, linePosition, sourceUri)
		{
		}

		// Token: 0x060018CB RID: 6347 RVA: 0x0008F12C File Offset: 0x0008D32C
		internal XmlException(string res, string[] args, Exception innerException, int lineNumber, int linePosition) : this(res, args, innerException, lineNumber, linePosition, null)
		{
		}

		// Token: 0x060018CC RID: 6348 RVA: 0x0008F13C File Offset: 0x0008D33C
		internal XmlException(string res, string[] args, Exception innerException, int lineNumber, int linePosition, string sourceUri) : base(XmlException.CreateMessage(res, args, lineNumber, linePosition), innerException)
		{
			base.HResult = -2146232000;
			this.res = res;
			this.args = args;
			this.sourceUri = sourceUri;
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		// Token: 0x060018CD RID: 6349 RVA: 0x0008F18C File Offset: 0x0008D38C
		private static string FormatUserMessage(string message, int lineNumber, int linePosition)
		{
			if (message == null)
			{
				return XmlException.CreateMessage("An XML error has occurred.", null, lineNumber, linePosition);
			}
			if (lineNumber == 0 && linePosition == 0)
			{
				return message;
			}
			return XmlException.CreateMessage("{0}", new string[]
			{
				message
			}, lineNumber, linePosition);
		}

		// Token: 0x060018CE RID: 6350 RVA: 0x0008F1C0 File Offset: 0x0008D3C0
		private static string CreateMessage(string res, string[] args, int lineNumber, int linePosition)
		{
			string result;
			try
			{
				string @string;
				if (lineNumber == 0)
				{
					@string = Res.GetString(res, args);
				}
				else
				{
					string text = lineNumber.ToString(CultureInfo.InvariantCulture);
					string text2 = linePosition.ToString(CultureInfo.InvariantCulture);
					@string = Res.GetString(res, args);
					@string = Res.GetString("{0} Line {1}, position {2}.", new string[]
					{
						@string,
						text,
						text2
					});
				}
				result = @string;
			}
			catch (MissingManifestResourceException)
			{
				result = "UNKNOWN(" + res + ")";
			}
			return result;
		}

		// Token: 0x060018CF RID: 6351 RVA: 0x0008F244 File Offset: 0x0008D444
		internal static string[] BuildCharExceptionArgs(string data, int invCharIndex)
		{
			return XmlException.BuildCharExceptionArgs(data[invCharIndex], (invCharIndex + 1 < data.Length) ? data[invCharIndex + 1] : '\0');
		}

		// Token: 0x060018D0 RID: 6352 RVA: 0x0008F269 File Offset: 0x0008D469
		internal static string[] BuildCharExceptionArgs(char[] data, int invCharIndex)
		{
			return XmlException.BuildCharExceptionArgs(data, data.Length, invCharIndex);
		}

		// Token: 0x060018D1 RID: 6353 RVA: 0x0008F275 File Offset: 0x0008D475
		internal static string[] BuildCharExceptionArgs(char[] data, int length, int invCharIndex)
		{
			return XmlException.BuildCharExceptionArgs(data[invCharIndex], (invCharIndex + 1 < length) ? data[invCharIndex + 1] : '\0');
		}

		// Token: 0x060018D2 RID: 6354 RVA: 0x0008F290 File Offset: 0x0008D490
		internal static string[] BuildCharExceptionArgs(char invChar, char nextChar)
		{
			string[] array = new string[2];
			if (XmlCharType.IsHighSurrogate((int)invChar) && nextChar != '\0')
			{
				int num = XmlCharType.CombineSurrogateChar((int)nextChar, (int)invChar);
				array[0] = new string(new char[]
				{
					invChar,
					nextChar
				});
				array[1] = string.Format(CultureInfo.InvariantCulture, "0x{0:X2}", num);
			}
			else
			{
				if (invChar == '\0')
				{
					array[0] = ".";
				}
				else
				{
					array[0] = invChar.ToString(CultureInfo.InvariantCulture);
				}
				array[1] = string.Format(CultureInfo.InvariantCulture, "0x{0:X2}", (int)invChar);
			}
			return array;
		}

		/// <summary>Gets the line number indicating where the error occurred.</summary>
		/// <returns>The line number indicating where the error occurred.</returns>
		// Token: 0x170004A5 RID: 1189
		// (get) Token: 0x060018D3 RID: 6355 RVA: 0x0008F31C File Offset: 0x0008D51C
		public int LineNumber
		{
			get
			{
				return this.lineNumber;
			}
		}

		/// <summary>Gets the line position indicating where the error occurred.</summary>
		/// <returns>The line position indicating where the error occurred.</returns>
		// Token: 0x170004A6 RID: 1190
		// (get) Token: 0x060018D4 RID: 6356 RVA: 0x0008F324 File Offset: 0x0008D524
		public int LinePosition
		{
			get
			{
				return this.linePosition;
			}
		}

		/// <summary>Gets the location of the XML file.</summary>
		/// <returns>The source URI for the XML data. If there is no source URI, this property returns <see langword="null" />.</returns>
		// Token: 0x170004A7 RID: 1191
		// (get) Token: 0x060018D5 RID: 6357 RVA: 0x0008F32C File Offset: 0x0008D52C
		public string SourceUri
		{
			get
			{
				return this.sourceUri;
			}
		}

		/// <summary>Gets a message describing the current exception.</summary>
		/// <returns>The error message that explains the reason for the exception.</returns>
		// Token: 0x170004A8 RID: 1192
		// (get) Token: 0x060018D6 RID: 6358 RVA: 0x0008F334 File Offset: 0x0008D534
		public override string Message
		{
			get
			{
				if (this.message != null)
				{
					return this.message;
				}
				return base.Message;
			}
		}

		// Token: 0x170004A9 RID: 1193
		// (get) Token: 0x060018D7 RID: 6359 RVA: 0x0008F34B File Offset: 0x0008D54B
		internal string ResString
		{
			get
			{
				return this.res;
			}
		}

		// Token: 0x060018D8 RID: 6360 RVA: 0x0008F353 File Offset: 0x0008D553
		internal static bool IsCatchableException(Exception e)
		{
			return !(e is StackOverflowException) && !(e is OutOfMemoryException) && !(e is ThreadAbortException) && !(e is ThreadInterruptedException) && !(e is NullReferenceException) && !(e is AccessViolationException);
		}

		// Token: 0x04001019 RID: 4121
		private string res;

		// Token: 0x0400101A RID: 4122
		private string[] args;

		// Token: 0x0400101B RID: 4123
		private int lineNumber;

		// Token: 0x0400101C RID: 4124
		private int linePosition;

		// Token: 0x0400101D RID: 4125
		[OptionalField]
		private string sourceUri;

		// Token: 0x0400101E RID: 4126
		private string message;
	}
}
