﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020000DA RID: 218
	internal class XmlEncodedRawTextWriter : XmlRawWriter
	{
		// Token: 0x060007B6 RID: 1974 RVA: 0x0001FDB8 File Offset: 0x0001DFB8
		protected XmlEncodedRawTextWriter(XmlWriterSettings settings)
		{
			this.useAsync = settings.Async;
			this.newLineHandling = settings.NewLineHandling;
			this.omitXmlDeclaration = settings.OmitXmlDeclaration;
			this.newLineChars = settings.NewLineChars;
			this.checkCharacters = settings.CheckCharacters;
			this.closeOutput = settings.CloseOutput;
			this.standalone = settings.Standalone;
			this.outputMethod = settings.OutputMethod;
			this.mergeCDataSections = settings.MergeCDataSections;
			if (this.checkCharacters && this.newLineHandling == NewLineHandling.Replace)
			{
				this.ValidateContentChars(this.newLineChars, "NewLineChars", false);
			}
		}

		// Token: 0x060007B7 RID: 1975 RVA: 0x0001FE80 File Offset: 0x0001E080
		public XmlEncodedRawTextWriter(TextWriter writer, XmlWriterSettings settings) : this(settings)
		{
			this.writer = writer;
			this.encoding = writer.Encoding;
			if (settings.Async)
			{
				this.bufLen = 65536;
			}
			this.bufChars = new char[this.bufLen + 32];
			if (settings.AutoXmlDeclaration)
			{
				this.WriteXmlDeclaration(this.standalone);
				this.autoXmlDeclaration = true;
			}
		}

		// Token: 0x060007B8 RID: 1976 RVA: 0x0001FEEC File Offset: 0x0001E0EC
		public XmlEncodedRawTextWriter(Stream stream, XmlWriterSettings settings) : this(settings)
		{
			this.stream = stream;
			this.encoding = settings.Encoding;
			if (settings.Async)
			{
				this.bufLen = 65536;
			}
			this.bufChars = new char[this.bufLen + 32];
			this.bufBytes = new byte[this.bufChars.Length];
			this.bufBytesUsed = 0;
			this.trackTextContent = true;
			this.inTextContent = false;
			this.lastMarkPos = 0;
			this.textContentMarks = new int[64];
			this.textContentMarks[0] = 1;
			this.charEntityFallback = new CharEntityEncoderFallback();
			this.encoding = (Encoding)settings.Encoding.Clone();
			this.encoding.EncoderFallback = this.charEntityFallback;
			this.encoder = this.encoding.GetEncoder();
			if (!stream.CanSeek || stream.Position == 0L)
			{
				byte[] preamble = this.encoding.GetPreamble();
				if (preamble.Length != 0)
				{
					this.stream.Write(preamble, 0, preamble.Length);
				}
			}
			if (settings.AutoXmlDeclaration)
			{
				this.WriteXmlDeclaration(this.standalone);
				this.autoXmlDeclaration = true;
			}
		}

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x060007B9 RID: 1977 RVA: 0x00020010 File Offset: 0x0001E210
		public override XmlWriterSettings Settings
		{
			get
			{
				return new XmlWriterSettings
				{
					Encoding = this.encoding,
					OmitXmlDeclaration = this.omitXmlDeclaration,
					NewLineHandling = this.newLineHandling,
					NewLineChars = this.newLineChars,
					CloseOutput = this.closeOutput,
					ConformanceLevel = ConformanceLevel.Auto,
					CheckCharacters = this.checkCharacters,
					AutoXmlDeclaration = this.autoXmlDeclaration,
					Standalone = this.standalone,
					OutputMethod = this.outputMethod,
					ReadOnly = true
				};
			}
		}

		// Token: 0x060007BA RID: 1978 RVA: 0x0002009C File Offset: 0x0001E29C
		internal override void WriteXmlDeclaration(XmlStandalone standalone)
		{
			if (!this.omitXmlDeclaration && !this.autoXmlDeclaration)
			{
				if (this.trackTextContent && this.inTextContent)
				{
					this.ChangeTextContentMark(false);
				}
				this.RawText("<?xml version=\"");
				this.RawText("1.0");
				if (this.encoding != null)
				{
					this.RawText("\" encoding=\"");
					this.RawText(this.encoding.WebName);
				}
				if (standalone != XmlStandalone.Omit)
				{
					this.RawText("\" standalone=\"");
					this.RawText((standalone == XmlStandalone.Yes) ? "yes" : "no");
				}
				this.RawText("\"?>");
			}
		}

		// Token: 0x060007BB RID: 1979 RVA: 0x0002013F File Offset: 0x0001E33F
		internal override void WriteXmlDeclaration(string xmldecl)
		{
			if (!this.omitXmlDeclaration && !this.autoXmlDeclaration)
			{
				this.WriteProcessingInstruction("xml", xmldecl);
			}
		}

		// Token: 0x060007BC RID: 1980 RVA: 0x00020160 File Offset: 0x0001E360
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			this.RawText("<!DOCTYPE ");
			this.RawText(name);
			int num;
			if (pubid != null)
			{
				this.RawText(" PUBLIC \"");
				this.RawText(pubid);
				this.RawText("\" \"");
				if (sysid != null)
				{
					this.RawText(sysid);
				}
				char[] array = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 34;
			}
			else if (sysid != null)
			{
				this.RawText(" SYSTEM \"");
				this.RawText(sysid);
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 34;
			}
			else
			{
				char[] array3 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 32;
			}
			if (subset != null)
			{
				char[] array4 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 91;
				this.RawText(subset);
				char[] array5 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array5[num] = 93;
			}
			char[] array6 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 62;
		}

		// Token: 0x060007BD RID: 1981 RVA: 0x00020284 File Offset: 0x0001E484
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			if (prefix != null && prefix.Length != 0)
			{
				this.RawText(prefix);
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 58;
			}
			this.RawText(localName);
			this.attrEndPos = this.bufPos;
		}

		// Token: 0x060007BE RID: 1982 RVA: 0x00020304 File Offset: 0x0001E504
		internal override void StartElementContent()
		{
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 62;
			this.contentPos = this.bufPos;
		}

		// Token: 0x060007BF RID: 1983 RVA: 0x00020338 File Offset: 0x0001E538
		internal override void WriteEndElement(string prefix, string localName, string ns)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			int num;
			if (this.contentPos != this.bufPos)
			{
				char[] array = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 60;
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 47;
				if (prefix != null && prefix.Length != 0)
				{
					this.RawText(prefix);
					char[] array3 = this.bufChars;
					num = this.bufPos;
					this.bufPos = num + 1;
					array3[num] = 58;
				}
				this.RawText(localName);
				char[] array4 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 62;
				return;
			}
			this.bufPos--;
			char[] array5 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 32;
			char[] array6 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 47;
			char[] array7 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array7[num] = 62;
		}

		// Token: 0x060007C0 RID: 1984 RVA: 0x0002044C File Offset: 0x0001E64C
		internal override void WriteFullEndElement(string prefix, string localName, string ns)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 47;
			if (prefix != null && prefix.Length != 0)
			{
				this.RawText(prefix);
				char[] array3 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 58;
			}
			this.RawText(localName);
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 62;
		}

		// Token: 0x060007C1 RID: 1985 RVA: 0x000204F4 File Offset: 0x0001E6F4
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			int num;
			if (this.attrEndPos == this.bufPos)
			{
				char[] array = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 32;
			}
			if (prefix != null && prefix.Length > 0)
			{
				this.RawText(prefix);
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 58;
			}
			this.RawText(localName);
			char[] array3 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 61;
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 34;
			this.inAttributeValue = true;
		}

		// Token: 0x060007C2 RID: 1986 RVA: 0x000205B0 File Offset: 0x0001E7B0
		public override void WriteEndAttribute()
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 34;
			this.inAttributeValue = false;
			this.attrEndPos = this.bufPos;
		}

		// Token: 0x060007C3 RID: 1987 RVA: 0x00020601 File Offset: 0x0001E801
		internal override void WriteNamespaceDeclaration(string prefix, string namespaceName)
		{
			this.WriteStartNamespaceDeclaration(prefix);
			this.WriteString(namespaceName);
			this.WriteEndNamespaceDeclaration();
		}

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x060007C4 RID: 1988 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool SupportsNamespaceDeclarationInChunks
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060007C5 RID: 1989 RVA: 0x00020618 File Offset: 0x0001E818
		internal override void WriteStartNamespaceDeclaration(string prefix)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			if (prefix.Length == 0)
			{
				this.RawText(" xmlns=\"");
			}
			else
			{
				this.RawText(" xmlns:");
				this.RawText(prefix);
				char[] array = this.bufChars;
				int num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 61;
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 34;
			}
			this.inAttributeValue = true;
			if (this.trackTextContent && !this.inTextContent)
			{
				this.ChangeTextContentMark(true);
			}
		}

		// Token: 0x060007C6 RID: 1990 RVA: 0x000206B8 File Offset: 0x0001E8B8
		internal override void WriteEndNamespaceDeclaration()
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			this.inAttributeValue = false;
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 34;
			this.attrEndPos = this.bufPos;
		}

		// Token: 0x060007C7 RID: 1991 RVA: 0x0002070C File Offset: 0x0001E90C
		public override void WriteCData(string text)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			int num;
			if (this.mergeCDataSections && this.bufPos == this.cdataPos)
			{
				this.bufPos -= 3;
			}
			else
			{
				char[] array = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 60;
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 33;
				char[] array3 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 91;
				char[] array4 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 67;
				char[] array5 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array5[num] = 68;
				char[] array6 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array6[num] = 65;
				char[] array7 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array7[num] = 84;
				char[] array8 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array8[num] = 65;
				char[] array9 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array9[num] = 91;
			}
			this.WriteCDataSection(text);
			char[] array10 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array10[num] = 93;
			char[] array11 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array11[num] = 93;
			char[] array12 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array12[num] = 62;
			this.textPos = this.bufPos;
			this.cdataPos = this.bufPos;
		}

		// Token: 0x060007C8 RID: 1992 RVA: 0x000208B0 File Offset: 0x0001EAB0
		public override void WriteComment(string text)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 33;
			char[] array3 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 45;
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 45;
			this.WriteCommentOrPi(text, 45);
			char[] array5 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 45;
			char[] array6 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 45;
			char[] array7 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array7[num] = 62;
		}

		// Token: 0x060007C9 RID: 1993 RVA: 0x00020994 File Offset: 0x0001EB94
		public override void WriteProcessingInstruction(string name, string text)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 63;
			this.RawText(name);
			if (text.Length > 0)
			{
				char[] array3 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 32;
				this.WriteCommentOrPi(text, 63);
			}
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 63;
			char[] array5 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 62;
		}

		// Token: 0x060007CA RID: 1994 RVA: 0x00020A54 File Offset: 0x0001EC54
		public override void WriteEntityRef(string name)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 38;
			this.RawText(name);
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 59;
			if (this.bufPos > this.bufLen)
			{
				this.FlushBuffer();
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x060007CB RID: 1995 RVA: 0x00020AD4 File Offset: 0x0001ECD4
		public override void WriteCharEntity(char ch)
		{
			int num = (int)ch;
			string s = num.ToString("X", NumberFormatInfo.InvariantInfo);
			if (this.checkCharacters && !this.xmlCharType.IsCharData(ch))
			{
				throw XmlConvert.CreateInvalidCharException(ch, '\0');
			}
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 38;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 35;
			char[] array3 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 120;
			this.RawText(s);
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 59;
			if (this.bufPos > this.bufLen)
			{
				this.FlushBuffer();
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x060007CC RID: 1996 RVA: 0x00020BBC File Offset: 0x0001EDBC
		public unsafe override void WriteWhitespace(string ws)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			fixed (string text = ws)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char* pSrcEnd = ptr + ws.Length;
				if (this.inAttributeValue)
				{
					this.WriteAttributeTextBlock(ptr, pSrcEnd);
				}
				else
				{
					this.WriteElementTextBlock(ptr, pSrcEnd);
				}
			}
		}

		// Token: 0x060007CD RID: 1997 RVA: 0x00020C18 File Offset: 0x0001EE18
		public unsafe override void WriteString(string text)
		{
			if (this.trackTextContent && !this.inTextContent)
			{
				this.ChangeTextContentMark(true);
			}
			fixed (string text2 = text)
			{
				char* ptr = text2;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char* pSrcEnd = ptr + text.Length;
				if (this.inAttributeValue)
				{
					this.WriteAttributeTextBlock(ptr, pSrcEnd);
				}
				else
				{
					this.WriteElementTextBlock(ptr, pSrcEnd);
				}
			}
		}

		// Token: 0x060007CE RID: 1998 RVA: 0x00020C74 File Offset: 0x0001EE74
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			int num = XmlCharType.CombineSurrogateChar((int)lowChar, (int)highChar);
			char[] array = this.bufChars;
			int num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array[num2] = 38;
			char[] array2 = this.bufChars;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array2[num2] = 35;
			char[] array3 = this.bufChars;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array3[num2] = 120;
			this.RawText(num.ToString("X", NumberFormatInfo.InvariantInfo));
			char[] array4 = this.bufChars;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array4[num2] = 59;
			this.textPos = this.bufPos;
		}

		// Token: 0x060007CF RID: 1999 RVA: 0x00020D2C File Offset: 0x0001EF2C
		public unsafe override void WriteChars(char[] buffer, int index, int count)
		{
			if (this.trackTextContent && !this.inTextContent)
			{
				this.ChangeTextContentMark(true);
			}
			fixed (char* ptr = &buffer[index])
			{
				char* ptr2 = ptr;
				if (this.inAttributeValue)
				{
					this.WriteAttributeTextBlock(ptr2, ptr2 + count);
				}
				else
				{
					this.WriteElementTextBlock(ptr2, ptr2 + count);
				}
			}
		}

		// Token: 0x060007D0 RID: 2000 RVA: 0x00020D84 File Offset: 0x0001EF84
		public unsafe override void WriteRaw(char[] buffer, int index, int count)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			fixed (char* ptr = &buffer[index])
			{
				char* ptr2 = ptr;
				this.WriteRawWithCharChecking(ptr2, ptr2 + count);
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x060007D1 RID: 2001 RVA: 0x00020DD0 File Offset: 0x0001EFD0
		public unsafe override void WriteRaw(string data)
		{
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			fixed (string text = data)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				this.WriteRawWithCharChecking(ptr, ptr + data.Length);
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x060007D2 RID: 2002 RVA: 0x00020E24 File Offset: 0x0001F024
		public override void Close()
		{
			try
			{
				this.FlushBuffer();
				this.FlushEncoder();
			}
			finally
			{
				this.writeToNull = true;
				if (this.stream != null)
				{
					try
					{
						this.stream.Flush();
						goto IL_7D;
					}
					finally
					{
						try
						{
							if (this.closeOutput)
							{
								this.stream.Close();
							}
						}
						finally
						{
							this.stream = null;
						}
					}
				}
				if (this.writer != null)
				{
					try
					{
						this.writer.Flush();
					}
					finally
					{
						try
						{
							if (this.closeOutput)
							{
								this.writer.Close();
							}
						}
						finally
						{
							this.writer = null;
						}
					}
				}
				IL_7D:;
			}
		}

		// Token: 0x060007D3 RID: 2003 RVA: 0x00020EF0 File Offset: 0x0001F0F0
		public override void Flush()
		{
			this.FlushBuffer();
			this.FlushEncoder();
			if (this.stream != null)
			{
				this.stream.Flush();
				return;
			}
			if (this.writer != null)
			{
				this.writer.Flush();
			}
		}

		// Token: 0x060007D4 RID: 2004 RVA: 0x00020F28 File Offset: 0x0001F128
		protected virtual void FlushBuffer()
		{
			try
			{
				if (!this.writeToNull)
				{
					if (this.stream != null)
					{
						if (this.trackTextContent)
						{
							this.charEntityFallback.Reset(this.textContentMarks, this.lastMarkPos);
							if ((this.lastMarkPos & 1) != 0)
							{
								this.textContentMarks[1] = 1;
								this.lastMarkPos = 1;
							}
							else
							{
								this.lastMarkPos = 0;
							}
						}
						this.EncodeChars(1, this.bufPos, true);
					}
					else
					{
						this.writer.Write(this.bufChars, 1, this.bufPos - 1);
					}
				}
			}
			catch
			{
				this.writeToNull = true;
				throw;
			}
			finally
			{
				this.bufChars[0] = this.bufChars[this.bufPos - 1];
				this.textPos = ((this.textPos == this.bufPos) ? 1 : 0);
				this.attrEndPos = ((this.attrEndPos == this.bufPos) ? 1 : 0);
				this.contentPos = 0;
				this.cdataPos = 0;
				this.bufPos = 1;
			}
		}

		// Token: 0x060007D5 RID: 2005 RVA: 0x00021038 File Offset: 0x0001F238
		private void EncodeChars(int startOffset, int endOffset, bool writeAllToStream)
		{
			while (startOffset < endOffset)
			{
				if (this.charEntityFallback != null)
				{
					this.charEntityFallback.StartOffset = startOffset;
				}
				int num;
				int num2;
				bool flag;
				this.encoder.Convert(this.bufChars, startOffset, endOffset - startOffset, this.bufBytes, this.bufBytesUsed, this.bufBytes.Length - this.bufBytesUsed, false, out num, out num2, out flag);
				startOffset += num;
				this.bufBytesUsed += num2;
				if (this.bufBytesUsed >= this.bufBytes.Length - 16)
				{
					this.stream.Write(this.bufBytes, 0, this.bufBytesUsed);
					this.bufBytesUsed = 0;
				}
			}
			if (writeAllToStream && this.bufBytesUsed > 0)
			{
				this.stream.Write(this.bufBytes, 0, this.bufBytesUsed);
				this.bufBytesUsed = 0;
			}
		}

		// Token: 0x060007D6 RID: 2006 RVA: 0x0002110C File Offset: 0x0001F30C
		private void FlushEncoder()
		{
			if (this.stream != null)
			{
				int num;
				int num2;
				bool flag;
				this.encoder.Convert(this.bufChars, 1, 0, this.bufBytes, 0, this.bufBytes.Length, true, out num, out num2, out flag);
				if (num2 != 0)
				{
					this.stream.Write(this.bufBytes, 0, num2);
				}
			}
		}

		// Token: 0x060007D7 RID: 2007 RVA: 0x00021160 File Offset: 0x0001F360
		protected unsafe void WriteAttributeTextBlock(char* pSrc, char* pSrcEnd)
		{
			char[] array;
			char* ptr;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			char* ptr2 = ptr + this.bufPos;
			int num = 0;
			for (;;)
			{
				char* ptr3 = ptr2 + (long)(pSrcEnd - pSrc) * 2L / 2L;
				if (ptr3 != ptr + this.bufLen)
				{
					ptr3 = ptr + this.bufLen;
				}
				while (ptr2 < ptr3 && (this.xmlCharType.charProperties[num = (int)(*pSrc)] & 128) != 0)
				{
					*ptr2 = (char)num;
					ptr2++;
					pSrc++;
				}
				if (pSrc >= pSrcEnd)
				{
					break;
				}
				if (ptr2 >= ptr3)
				{
					this.bufPos = (int)((long)(ptr2 - ptr));
					this.FlushBuffer();
					ptr2 = ptr + 1;
				}
				else
				{
					if (num <= 38)
					{
						switch (num)
						{
						case 9:
							if (this.newLineHandling == NewLineHandling.None)
							{
								*ptr2 = (char)num;
								ptr2++;
								goto IL_1D3;
							}
							ptr2 = XmlEncodedRawTextWriter.TabEntity(ptr2);
							goto IL_1D3;
						case 10:
							if (this.newLineHandling == NewLineHandling.None)
							{
								*ptr2 = (char)num;
								ptr2++;
								goto IL_1D3;
							}
							ptr2 = XmlEncodedRawTextWriter.LineFeedEntity(ptr2);
							goto IL_1D3;
						case 11:
						case 12:
							break;
						case 13:
							if (this.newLineHandling == NewLineHandling.None)
							{
								*ptr2 = (char)num;
								ptr2++;
								goto IL_1D3;
							}
							ptr2 = XmlEncodedRawTextWriter.CarriageReturnEntity(ptr2);
							goto IL_1D3;
						default:
							if (num == 34)
							{
								ptr2 = XmlEncodedRawTextWriter.QuoteEntity(ptr2);
								goto IL_1D3;
							}
							if (num == 38)
							{
								ptr2 = XmlEncodedRawTextWriter.AmpEntity(ptr2);
								goto IL_1D3;
							}
							break;
						}
					}
					else
					{
						if (num == 39)
						{
							*ptr2 = (char)num;
							ptr2++;
							goto IL_1D3;
						}
						if (num == 60)
						{
							ptr2 = XmlEncodedRawTextWriter.LtEntity(ptr2);
							goto IL_1D3;
						}
						if (num == 62)
						{
							ptr2 = XmlEncodedRawTextWriter.GtEntity(ptr2);
							goto IL_1D3;
						}
					}
					if (XmlCharType.IsSurrogate(num))
					{
						ptr2 = XmlEncodedRawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, ptr2);
						pSrc += 2;
						continue;
					}
					if (num <= 127 || num >= 65534)
					{
						ptr2 = this.InvalidXmlChar(num, ptr2, true);
						pSrc++;
						continue;
					}
					*ptr2 = (char)num;
					ptr2++;
					pSrc++;
					continue;
					IL_1D3:
					pSrc++;
				}
			}
			this.bufPos = (int)((long)(ptr2 - ptr));
			array = null;
		}

		// Token: 0x060007D8 RID: 2008 RVA: 0x0002135C File Offset: 0x0001F55C
		protected unsafe void WriteElementTextBlock(char* pSrc, char* pSrcEnd)
		{
			char[] array;
			char* ptr;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			char* ptr2 = ptr + this.bufPos;
			int num = 0;
			for (;;)
			{
				char* ptr3 = ptr2 + (long)(pSrcEnd - pSrc) * 2L / 2L;
				if (ptr3 != ptr + this.bufLen)
				{
					ptr3 = ptr + this.bufLen;
				}
				while (ptr2 < ptr3 && (this.xmlCharType.charProperties[num = (int)(*pSrc)] & 128) != 0)
				{
					*ptr2 = (char)num;
					ptr2++;
					pSrc++;
				}
				if (pSrc >= pSrcEnd)
				{
					break;
				}
				if (ptr2 < ptr3)
				{
					if (num <= 38)
					{
						switch (num)
						{
						case 9:
							goto IL_10F;
						case 10:
							if (this.newLineHandling == NewLineHandling.Replace)
							{
								ptr2 = this.WriteNewLine(ptr2);
								goto IL_1D6;
							}
							*ptr2 = (char)num;
							ptr2++;
							goto IL_1D6;
						case 11:
						case 12:
							break;
						case 13:
							switch (this.newLineHandling)
							{
							case NewLineHandling.Replace:
								if (pSrc[1] == '\n')
								{
									pSrc++;
								}
								ptr2 = this.WriteNewLine(ptr2);
								goto IL_1D6;
							case NewLineHandling.Entitize:
								ptr2 = XmlEncodedRawTextWriter.CarriageReturnEntity(ptr2);
								goto IL_1D6;
							case NewLineHandling.None:
								*ptr2 = (char)num;
								ptr2++;
								goto IL_1D6;
							default:
								goto IL_1D6;
							}
							break;
						default:
							if (num == 34)
							{
								goto IL_10F;
							}
							if (num == 38)
							{
								ptr2 = XmlEncodedRawTextWriter.AmpEntity(ptr2);
								goto IL_1D6;
							}
							break;
						}
					}
					else
					{
						if (num == 39)
						{
							goto IL_10F;
						}
						if (num == 60)
						{
							ptr2 = XmlEncodedRawTextWriter.LtEntity(ptr2);
							goto IL_1D6;
						}
						if (num == 62)
						{
							ptr2 = XmlEncodedRawTextWriter.GtEntity(ptr2);
							goto IL_1D6;
						}
					}
					if (XmlCharType.IsSurrogate(num))
					{
						ptr2 = XmlEncodedRawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, ptr2);
						pSrc += 2;
						continue;
					}
					if (num <= 127 || num >= 65534)
					{
						ptr2 = this.InvalidXmlChar(num, ptr2, true);
						pSrc++;
						continue;
					}
					*ptr2 = (char)num;
					ptr2++;
					pSrc++;
					continue;
					IL_1D6:
					pSrc++;
					continue;
					IL_10F:
					*ptr2 = (char)num;
					ptr2++;
					goto IL_1D6;
				}
				this.bufPos = (int)((long)(ptr2 - ptr));
				this.FlushBuffer();
				ptr2 = ptr + 1;
			}
			this.bufPos = (int)((long)(ptr2 - ptr));
			this.textPos = this.bufPos;
			this.contentPos = 0;
			array = null;
		}

		// Token: 0x060007D9 RID: 2009 RVA: 0x0002156C File Offset: 0x0001F76C
		protected unsafe void RawText(string s)
		{
			fixed (string text = s)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				this.RawText(ptr, ptr + s.Length);
			}
		}

		// Token: 0x060007DA RID: 2010 RVA: 0x000215A0 File Offset: 0x0001F7A0
		protected unsafe void RawText(char* pSrcBegin, char* pSrcEnd)
		{
			char[] array;
			char* ptr;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			char* ptr2 = ptr + this.bufPos;
			char* ptr3 = pSrcBegin;
			int num = 0;
			for (;;)
			{
				char* ptr4 = ptr2 + (long)(pSrcEnd - ptr3) * 2L / 2L;
				if (ptr4 != ptr + this.bufLen)
				{
					ptr4 = ptr + this.bufLen;
				}
				while (ptr2 < ptr4 && (num = (int)(*ptr3)) < 55296)
				{
					ptr3++;
					*ptr2 = (char)num;
					ptr2++;
				}
				if (ptr3 >= pSrcEnd)
				{
					break;
				}
				if (ptr2 >= ptr4)
				{
					this.bufPos = (int)((long)(ptr2 - ptr));
					this.FlushBuffer();
					ptr2 = ptr + 1;
				}
				else if (XmlCharType.IsSurrogate(num))
				{
					ptr2 = XmlEncodedRawTextWriter.EncodeSurrogate(ptr3, pSrcEnd, ptr2);
					ptr3 += 2;
				}
				else if (num <= 127 || num >= 65534)
				{
					ptr2 = this.InvalidXmlChar(num, ptr2, false);
					ptr3++;
				}
				else
				{
					*ptr2 = (char)num;
					ptr2++;
					ptr3++;
				}
			}
			this.bufPos = (int)((long)(ptr2 - ptr));
			array = null;
		}

		// Token: 0x060007DB RID: 2011 RVA: 0x000216AC File Offset: 0x0001F8AC
		protected unsafe void WriteRawWithCharChecking(char* pSrcBegin, char* pSrcEnd)
		{
			char[] array;
			char* ptr;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			char* ptr2 = pSrcBegin;
			char* ptr3 = ptr + this.bufPos;
			int num = 0;
			for (;;)
			{
				char* ptr4 = ptr3 + (long)(pSrcEnd - ptr2) * 2L / 2L;
				if (ptr4 != ptr + this.bufLen)
				{
					ptr4 = ptr + this.bufLen;
				}
				while (ptr3 < ptr4 && (this.xmlCharType.charProperties[num = (int)(*ptr2)] & 64) != 0)
				{
					*ptr3 = (char)num;
					ptr3++;
					ptr2++;
				}
				if (ptr2 >= pSrcEnd)
				{
					break;
				}
				if (ptr3 < ptr4)
				{
					if (num <= 38)
					{
						switch (num)
						{
						case 9:
							goto IL_DF;
						case 10:
							if (this.newLineHandling == NewLineHandling.Replace)
							{
								ptr3 = this.WriteNewLine(ptr3);
								goto IL_186;
							}
							*ptr3 = (char)num;
							ptr3++;
							goto IL_186;
						case 11:
						case 12:
							break;
						case 13:
							if (this.newLineHandling == NewLineHandling.Replace)
							{
								if (ptr2[1] == '\n')
								{
									ptr2++;
								}
								ptr3 = this.WriteNewLine(ptr3);
								goto IL_186;
							}
							*ptr3 = (char)num;
							ptr3++;
							goto IL_186;
						default:
							if (num == 38)
							{
								goto IL_DF;
							}
							break;
						}
					}
					else if (num == 60 || num == 93)
					{
						goto IL_DF;
					}
					if (XmlCharType.IsSurrogate(num))
					{
						ptr3 = XmlEncodedRawTextWriter.EncodeSurrogate(ptr2, pSrcEnd, ptr3);
						ptr2 += 2;
						continue;
					}
					if (num <= 127 || num >= 65534)
					{
						ptr3 = this.InvalidXmlChar(num, ptr3, false);
						ptr2++;
						continue;
					}
					*ptr3 = (char)num;
					ptr3++;
					ptr2++;
					continue;
					IL_186:
					ptr2++;
					continue;
					IL_DF:
					*ptr3 = (char)num;
					ptr3++;
					goto IL_186;
				}
				this.bufPos = (int)((long)(ptr3 - ptr));
				this.FlushBuffer();
				ptr3 = ptr + 1;
			}
			this.bufPos = (int)((long)(ptr3 - ptr));
			array = null;
		}

		// Token: 0x060007DC RID: 2012 RVA: 0x00021858 File Offset: 0x0001FA58
		protected unsafe void WriteCommentOrPi(string text, int stopChar)
		{
			if (text.Length == 0)
			{
				if (this.bufPos >= this.bufLen)
				{
					this.FlushBuffer();
				}
				return;
			}
			fixed (string text2 = text)
			{
				char* ptr = text2;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char[] array;
				char* ptr2;
				if ((array = this.bufChars) == null || array.Length == 0)
				{
					ptr2 = null;
				}
				else
				{
					ptr2 = &array[0];
				}
				char* ptr3 = ptr;
				char* ptr4 = ptr + text.Length;
				char* ptr5 = ptr2 + this.bufPos;
				int num = 0;
				for (;;)
				{
					char* ptr6 = ptr5 + (long)(ptr4 - ptr3) * 2L / 2L;
					if (ptr6 != ptr2 + this.bufLen)
					{
						ptr6 = ptr2 + this.bufLen;
					}
					while (ptr5 < ptr6 && (this.xmlCharType.charProperties[num = (int)(*ptr3)] & 64) != 0 && num != stopChar)
					{
						*ptr5 = (char)num;
						ptr5++;
						ptr3++;
					}
					if (ptr3 >= ptr4)
					{
						break;
					}
					if (ptr5 < ptr6)
					{
						if (num <= 45)
						{
							switch (num)
							{
							case 9:
								goto IL_226;
							case 10:
								if (this.newLineHandling == NewLineHandling.Replace)
								{
									ptr5 = this.WriteNewLine(ptr5);
									goto IL_296;
								}
								*ptr5 = (char)num;
								ptr5++;
								goto IL_296;
							case 11:
							case 12:
								break;
							case 13:
								if (this.newLineHandling == NewLineHandling.Replace)
								{
									if (ptr3[1] == '\n')
									{
										ptr3++;
									}
									ptr5 = this.WriteNewLine(ptr5);
									goto IL_296;
								}
								*ptr5 = (char)num;
								ptr5++;
								goto IL_296;
							default:
								if (num == 38)
								{
									goto IL_226;
								}
								if (num == 45)
								{
									*ptr5 = '-';
									ptr5++;
									if (num == stopChar && (ptr3 + 1 == ptr4 || ptr3[1] == '-'))
									{
										*ptr5 = ' ';
										ptr5++;
										goto IL_296;
									}
									goto IL_296;
								}
								break;
							}
						}
						else
						{
							if (num == 60)
							{
								goto IL_226;
							}
							if (num != 63)
							{
								if (num == 93)
								{
									*ptr5 = ']';
									ptr5++;
									goto IL_296;
								}
							}
							else
							{
								*ptr5 = '?';
								ptr5++;
								if (num == stopChar && ptr3 + 1 < ptr4 && ptr3[1] == '>')
								{
									*ptr5 = ' ';
									ptr5++;
									goto IL_296;
								}
								goto IL_296;
							}
						}
						if (XmlCharType.IsSurrogate(num))
						{
							ptr5 = XmlEncodedRawTextWriter.EncodeSurrogate(ptr3, ptr4, ptr5);
							ptr3 += 2;
							continue;
						}
						if (num <= 127 || num >= 65534)
						{
							ptr5 = this.InvalidXmlChar(num, ptr5, false);
							ptr3++;
							continue;
						}
						*ptr5 = (char)num;
						ptr5++;
						ptr3++;
						continue;
						IL_296:
						ptr3++;
						continue;
						IL_226:
						*ptr5 = (char)num;
						ptr5++;
						goto IL_296;
					}
					this.bufPos = (int)((long)(ptr5 - ptr2));
					this.FlushBuffer();
					ptr5 = ptr2 + 1;
				}
				this.bufPos = (int)((long)(ptr5 - ptr2));
				array = null;
			}
		}

		// Token: 0x060007DD RID: 2013 RVA: 0x00021B18 File Offset: 0x0001FD18
		protected unsafe void WriteCDataSection(string text)
		{
			if (text.Length == 0)
			{
				if (this.bufPos >= this.bufLen)
				{
					this.FlushBuffer();
				}
				return;
			}
			fixed (string text2 = text)
			{
				char* ptr = text2;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char[] array;
				char* ptr2;
				if ((array = this.bufChars) == null || array.Length == 0)
				{
					ptr2 = null;
				}
				else
				{
					ptr2 = &array[0];
				}
				char* ptr3 = ptr;
				char* ptr4 = ptr + text.Length;
				char* ptr5 = ptr2 + this.bufPos;
				int num = 0;
				for (;;)
				{
					char* ptr6 = ptr5 + (long)(ptr4 - ptr3) * 2L / 2L;
					if (ptr6 != ptr2 + this.bufLen)
					{
						ptr6 = ptr2 + this.bufLen;
					}
					while (ptr5 < ptr6 && (this.xmlCharType.charProperties[num = (int)(*ptr3)] & 128) != 0 && num != 93)
					{
						*ptr5 = (char)num;
						ptr5++;
						ptr3++;
					}
					if (ptr3 >= ptr4)
					{
						break;
					}
					if (ptr5 < ptr6)
					{
						if (num <= 39)
						{
							switch (num)
							{
							case 9:
								goto IL_210;
							case 10:
								if (this.newLineHandling == NewLineHandling.Replace)
								{
									ptr5 = this.WriteNewLine(ptr5);
									goto IL_280;
								}
								*ptr5 = (char)num;
								ptr5++;
								goto IL_280;
							case 11:
							case 12:
								break;
							case 13:
								if (this.newLineHandling == NewLineHandling.Replace)
								{
									if (ptr3[1] == '\n')
									{
										ptr3++;
									}
									ptr5 = this.WriteNewLine(ptr5);
									goto IL_280;
								}
								*ptr5 = (char)num;
								ptr5++;
								goto IL_280;
							default:
								if (num == 34 || num - 38 <= 1)
								{
									goto IL_210;
								}
								break;
							}
						}
						else
						{
							if (num == 60)
							{
								goto IL_210;
							}
							if (num == 62)
							{
								if (this.hadDoubleBracket && ptr5[-1] == ']')
								{
									ptr5 = XmlEncodedRawTextWriter.RawEndCData(ptr5);
									ptr5 = XmlEncodedRawTextWriter.RawStartCData(ptr5);
								}
								*ptr5 = '>';
								ptr5++;
								goto IL_280;
							}
							if (num == 93)
							{
								if (ptr5[-1] == ']')
								{
									this.hadDoubleBracket = true;
								}
								else
								{
									this.hadDoubleBracket = false;
								}
								*ptr5 = ']';
								ptr5++;
								goto IL_280;
							}
						}
						if (XmlCharType.IsSurrogate(num))
						{
							ptr5 = XmlEncodedRawTextWriter.EncodeSurrogate(ptr3, ptr4, ptr5);
							ptr3 += 2;
							continue;
						}
						if (num <= 127 || num >= 65534)
						{
							ptr5 = this.InvalidXmlChar(num, ptr5, false);
							ptr3++;
							continue;
						}
						*ptr5 = (char)num;
						ptr5++;
						ptr3++;
						continue;
						IL_280:
						ptr3++;
						continue;
						IL_210:
						*ptr5 = (char)num;
						ptr5++;
						goto IL_280;
					}
					this.bufPos = (int)((long)(ptr5 - ptr2));
					this.FlushBuffer();
					ptr5 = ptr2 + 1;
				}
				this.bufPos = (int)((long)(ptr5 - ptr2));
				array = null;
			}
		}

		// Token: 0x060007DE RID: 2014 RVA: 0x00021DC4 File Offset: 0x0001FFC4
		private unsafe static char* EncodeSurrogate(char* pSrc, char* pSrcEnd, char* pDst)
		{
			int num = (int)(*pSrc);
			if (num > 56319)
			{
				throw XmlConvert.CreateInvalidHighSurrogateCharException((char)num);
			}
			if (pSrc + 1 >= pSrcEnd)
			{
				throw new ArgumentException(Res.GetString("The surrogate pair is invalid. Missing a low surrogate character."));
			}
			int num2 = (int)pSrc[1];
			if (num2 >= 56320 && (LocalAppContextSwitches.DontThrowOnInvalidSurrogatePairs || num2 <= 57343))
			{
				*pDst = (char)num;
				pDst[1] = (char)num2;
				pDst += 2;
				return pDst;
			}
			throw XmlConvert.CreateInvalidSurrogatePairException((char)num2, (char)num);
		}

		// Token: 0x060007DF RID: 2015 RVA: 0x00021E33 File Offset: 0x00020033
		private unsafe char* InvalidXmlChar(int ch, char* pDst, bool entitize)
		{
			if (this.checkCharacters)
			{
				throw XmlConvert.CreateInvalidCharException((char)ch, '\0');
			}
			if (entitize)
			{
				return XmlEncodedRawTextWriter.CharEntity(pDst, (char)ch);
			}
			*pDst = (char)ch;
			pDst++;
			return pDst;
		}

		// Token: 0x060007E0 RID: 2016 RVA: 0x00021E5C File Offset: 0x0002005C
		internal unsafe void EncodeChar(ref char* pSrc, char* pSrcEnd, ref char* pDst)
		{
			int num = (int)(*pSrc);
			if (XmlCharType.IsSurrogate(num))
			{
				pDst = XmlEncodedRawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, pDst);
				pSrc += (IntPtr)2 * 2;
				return;
			}
			if (num <= 127 || num >= 65534)
			{
				pDst = this.InvalidXmlChar(num, pDst, false);
				pSrc += 2;
				return;
			}
			*pDst = (short)((ushort)num);
			pDst += 2;
			pSrc += 2;
		}

		// Token: 0x060007E1 RID: 2017 RVA: 0x00021EBC File Offset: 0x000200BC
		protected void ChangeTextContentMark(bool value)
		{
			this.inTextContent = value;
			if (this.lastMarkPos + 1 == this.textContentMarks.Length)
			{
				this.GrowTextContentMarks();
			}
			int[] array = this.textContentMarks;
			int num = this.lastMarkPos + 1;
			this.lastMarkPos = num;
			array[num] = this.bufPos;
		}

		// Token: 0x060007E2 RID: 2018 RVA: 0x00021F08 File Offset: 0x00020108
		private void GrowTextContentMarks()
		{
			int[] destinationArray = new int[this.textContentMarks.Length * 2];
			Array.Copy(this.textContentMarks, destinationArray, this.textContentMarks.Length);
			this.textContentMarks = destinationArray;
		}

		// Token: 0x060007E3 RID: 2019 RVA: 0x00021F40 File Offset: 0x00020140
		protected unsafe char* WriteNewLine(char* pDst)
		{
			char[] array;
			char* ptr;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			this.bufPos = (int)((long)(pDst - ptr));
			this.RawText(this.newLineChars);
			return ptr + this.bufPos;
		}

		// Token: 0x060007E4 RID: 2020 RVA: 0x00021F8E File Offset: 0x0002018E
		protected unsafe static char* LtEntity(char* pDst)
		{
			*pDst = '&';
			pDst[1] = 'l';
			pDst[2] = 't';
			pDst[3] = ';';
			return pDst + 4;
		}

		// Token: 0x060007E5 RID: 2021 RVA: 0x00021FB2 File Offset: 0x000201B2
		protected unsafe static char* GtEntity(char* pDst)
		{
			*pDst = '&';
			pDst[1] = 'g';
			pDst[2] = 't';
			pDst[3] = ';';
			return pDst + 4;
		}

		// Token: 0x060007E6 RID: 2022 RVA: 0x00021FD6 File Offset: 0x000201D6
		protected unsafe static char* AmpEntity(char* pDst)
		{
			*pDst = '&';
			pDst[1] = 'a';
			pDst[2] = 'm';
			pDst[3] = 'p';
			pDst[4] = ';';
			return pDst + 5;
		}

		// Token: 0x060007E7 RID: 2023 RVA: 0x00022003 File Offset: 0x00020203
		protected unsafe static char* QuoteEntity(char* pDst)
		{
			*pDst = '&';
			pDst[1] = 'q';
			pDst[2] = 'u';
			pDst[3] = 'o';
			pDst[4] = 't';
			pDst[5] = ';';
			return pDst + 6;
		}

		// Token: 0x060007E8 RID: 2024 RVA: 0x00022039 File Offset: 0x00020239
		protected unsafe static char* TabEntity(char* pDst)
		{
			*pDst = '&';
			pDst[1] = '#';
			pDst[2] = 'x';
			pDst[3] = '9';
			pDst[4] = ';';
			return pDst + 5;
		}

		// Token: 0x060007E9 RID: 2025 RVA: 0x00022066 File Offset: 0x00020266
		protected unsafe static char* LineFeedEntity(char* pDst)
		{
			*pDst = '&';
			pDst[1] = '#';
			pDst[2] = 'x';
			pDst[3] = 'A';
			pDst[4] = ';';
			return pDst + 5;
		}

		// Token: 0x060007EA RID: 2026 RVA: 0x00022093 File Offset: 0x00020293
		protected unsafe static char* CarriageReturnEntity(char* pDst)
		{
			*pDst = '&';
			pDst[1] = '#';
			pDst[2] = 'x';
			pDst[3] = 'D';
			pDst[4] = ';';
			return pDst + 5;
		}

		// Token: 0x060007EB RID: 2027 RVA: 0x000220C0 File Offset: 0x000202C0
		private unsafe static char* CharEntity(char* pDst, char ch)
		{
			int num = (int)ch;
			string text = num.ToString("X", NumberFormatInfo.InvariantInfo);
			*pDst = '&';
			pDst[1] = '#';
			pDst[2] = 'x';
			pDst += 3;
			fixed (string text2 = text)
			{
				char* ptr = text2;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char* ptr2 = ptr;
				while ((*(pDst++) = *(ptr2++)) != '\0')
				{
				}
			}
			pDst[-1] = ';';
			return pDst;
		}

		// Token: 0x060007EC RID: 2028 RVA: 0x0002212C File Offset: 0x0002032C
		protected unsafe static char* RawStartCData(char* pDst)
		{
			*pDst = '<';
			pDst[1] = '!';
			pDst[2] = '[';
			pDst[3] = 'C';
			pDst[4] = 'D';
			pDst[5] = 'A';
			pDst[6] = 'T';
			pDst[7] = 'A';
			pDst[8] = '[';
			return pDst + 9;
		}

		// Token: 0x060007ED RID: 2029 RVA: 0x00022189 File Offset: 0x00020389
		protected unsafe static char* RawEndCData(char* pDst)
		{
			*pDst = ']';
			pDst[1] = ']';
			pDst[2] = '>';
			return pDst + 3;
		}

		// Token: 0x060007EE RID: 2030 RVA: 0x000221A4 File Offset: 0x000203A4
		protected void ValidateContentChars(string chars, string propertyName, bool allowOnlyWhitespace)
		{
			if (!allowOnlyWhitespace)
			{
				for (int i = 0; i < chars.Length; i++)
				{
					if (!this.xmlCharType.IsTextChar(chars[i]))
					{
						char c = chars[i];
						if (c <= '&')
						{
							switch (c)
							{
							case '\t':
							case '\n':
							case '\r':
								goto IL_119;
							case '\v':
							case '\f':
								goto IL_A0;
							default:
								if (c != '&')
								{
									goto IL_A0;
								}
								break;
							}
						}
						else if (c != '<' && c != ']')
						{
							goto IL_A0;
						}
						string @string = Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(chars, i));
						goto IL_12A;
						IL_A0:
						if (XmlCharType.IsHighSurrogate((int)chars[i]))
						{
							if (i + 1 < chars.Length && XmlCharType.IsLowSurrogate((int)chars[i + 1]))
							{
								i++;
								goto IL_119;
							}
							@string = Res.GetString("The surrogate pair is invalid. Missing a low surrogate character.");
						}
						else
						{
							if (!XmlCharType.IsLowSurrogate((int)chars[i]))
							{
								goto IL_119;
							}
							@string = Res.GetString("Invalid high surrogate character (0x{0}). A high surrogate character must have a value from range (0xD800 - 0xDBFF).", new object[]
							{
								((uint)chars[i]).ToString("X", CultureInfo.InvariantCulture)
							});
						}
						IL_12A:
						throw new ArgumentException(Res.GetString("XmlWriterSettings.{0} can contain only valid XML text content characters when XmlWriterSettings.CheckCharacters is true. {1}", new string[]
						{
							propertyName,
							@string
						}));
					}
					IL_119:;
				}
				return;
			}
			if (!this.xmlCharType.IsOnlyWhitespace(chars))
			{
				throw new ArgumentException(Res.GetString("XmlWriterSettings.{0} can contain only valid XML white space characters when XmlWriterSettings.CheckCharacters and XmlWriterSettings.NewLineOnAttributes are true.", new object[]
				{
					propertyName
				}));
			}
		}

		// Token: 0x060007EF RID: 2031 RVA: 0x000222F9 File Offset: 0x000204F9
		protected void CheckAsyncCall()
		{
			if (!this.useAsync)
			{
				throw new InvalidOperationException(Res.GetString("Set XmlWriterSettings.Async to true if you want to use Async Methods."));
			}
		}

		// Token: 0x060007F0 RID: 2032 RVA: 0x00022314 File Offset: 0x00020514
		internal override async Task WriteXmlDeclarationAsync(XmlStandalone standalone)
		{
			this.CheckAsyncCall();
			if (!this.omitXmlDeclaration && !this.autoXmlDeclaration)
			{
				if (this.trackTextContent && this.inTextContent)
				{
					this.ChangeTextContentMark(false);
				}
				await this.RawTextAsync("<?xml version=\"").ConfigureAwait(false);
				await this.RawTextAsync("1.0").ConfigureAwait(false);
				if (this.encoding != null)
				{
					await this.RawTextAsync("\" encoding=\"").ConfigureAwait(false);
					await this.RawTextAsync(this.encoding.WebName).ConfigureAwait(false);
				}
				if (standalone != XmlStandalone.Omit)
				{
					await this.RawTextAsync("\" standalone=\"").ConfigureAwait(false);
					await this.RawTextAsync((standalone == XmlStandalone.Yes) ? "yes" : "no").ConfigureAwait(false);
				}
				await this.RawTextAsync("\"?>").ConfigureAwait(false);
			}
		}

		// Token: 0x060007F1 RID: 2033 RVA: 0x00022361 File Offset: 0x00020561
		internal override Task WriteXmlDeclarationAsync(string xmldecl)
		{
			this.CheckAsyncCall();
			if (!this.omitXmlDeclaration && !this.autoXmlDeclaration)
			{
				return this.WriteProcessingInstructionAsync("xml", xmldecl);
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x060007F2 RID: 2034 RVA: 0x0002238C File Offset: 0x0002058C
		public override async Task WriteDocTypeAsync(string name, string pubid, string sysid, string subset)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			await this.RawTextAsync("<!DOCTYPE ").ConfigureAwait(false);
			await this.RawTextAsync(name).ConfigureAwait(false);
			int num;
			if (pubid != null)
			{
				await this.RawTextAsync(" PUBLIC \"").ConfigureAwait(false);
				await this.RawTextAsync(pubid).ConfigureAwait(false);
				await this.RawTextAsync("\" \"").ConfigureAwait(false);
				if (sysid != null)
				{
					await this.RawTextAsync(sysid).ConfigureAwait(false);
				}
				char[] array = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 34;
			}
			else if (sysid != null)
			{
				await this.RawTextAsync(" SYSTEM \"").ConfigureAwait(false);
				await this.RawTextAsync(sysid).ConfigureAwait(false);
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 34;
			}
			else
			{
				char[] array3 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 32;
			}
			if (subset != null)
			{
				char[] array4 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 91;
				await this.RawTextAsync(subset).ConfigureAwait(false);
				char[] array5 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array5[num] = 93;
			}
			char[] array6 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 62;
		}

		// Token: 0x060007F3 RID: 2035 RVA: 0x000223F4 File Offset: 0x000205F4
		public override Task WriteStartElementAsync(string prefix, string localName, string ns)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			Task task;
			if (prefix != null && prefix.Length != 0)
			{
				task = this.RawTextAsync(prefix + ":" + localName);
			}
			else
			{
				task = this.RawTextAsync(localName);
			}
			return task.CallVoidFuncWhenFinish(new Action(this.WriteStartElementAsync_SetAttEndPos));
		}

		// Token: 0x060007F4 RID: 2036 RVA: 0x00022472 File Offset: 0x00020672
		private void WriteStartElementAsync_SetAttEndPos()
		{
			this.attrEndPos = this.bufPos;
		}

		// Token: 0x060007F5 RID: 2037 RVA: 0x00022480 File Offset: 0x00020680
		internal override Task WriteEndElementAsync(string prefix, string localName, string ns)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			int num;
			if (this.contentPos == this.bufPos)
			{
				this.bufPos--;
				char[] array = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 32;
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 47;
				char[] array3 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 62;
				return AsyncHelper.DoneTask;
			}
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 60;
			char[] array5 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 47;
			if (prefix != null && prefix.Length != 0)
			{
				return this.RawTextAsync(prefix + ":" + localName + ">");
			}
			return this.RawTextAsync(localName + ">");
		}

		// Token: 0x060007F6 RID: 2038 RVA: 0x00022584 File Offset: 0x00020784
		internal override Task WriteFullEndElementAsync(string prefix, string localName, string ns)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 47;
			if (prefix != null && prefix.Length != 0)
			{
				return this.RawTextAsync(prefix + ":" + localName + ">");
			}
			return this.RawTextAsync(localName + ">");
		}

		// Token: 0x060007F7 RID: 2039 RVA: 0x00022618 File Offset: 0x00020818
		protected internal override Task WriteStartAttributeAsync(string prefix, string localName, string ns)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			if (this.attrEndPos == this.bufPos)
			{
				char[] array = this.bufChars;
				int num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 32;
			}
			Task task;
			if (prefix != null && prefix.Length > 0)
			{
				task = this.RawTextAsync(prefix + ":" + localName);
			}
			else
			{
				task = this.RawTextAsync(localName);
			}
			return task.CallVoidFuncWhenFinish(new Action(this.WriteStartAttribute_SetInAttribute));
		}

		// Token: 0x060007F8 RID: 2040 RVA: 0x000226A8 File Offset: 0x000208A8
		private void WriteStartAttribute_SetInAttribute()
		{
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 61;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 34;
			this.inAttributeValue = true;
		}

		// Token: 0x060007F9 RID: 2041 RVA: 0x000226F0 File Offset: 0x000208F0
		protected internal override Task WriteEndAttributeAsync()
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 34;
			this.inAttributeValue = false;
			this.attrEndPos = this.bufPos;
			return AsyncHelper.DoneTask;
		}

		// Token: 0x060007FA RID: 2042 RVA: 0x0002274C File Offset: 0x0002094C
		internal override async Task WriteNamespaceDeclarationAsync(string prefix, string namespaceName)
		{
			this.CheckAsyncCall();
			await this.WriteStartNamespaceDeclarationAsync(prefix).ConfigureAwait(false);
			await this.WriteStringAsync(namespaceName).ConfigureAwait(false);
			await this.WriteEndNamespaceDeclarationAsync().ConfigureAwait(false);
		}

		// Token: 0x060007FB RID: 2043 RVA: 0x000227A4 File Offset: 0x000209A4
		internal override async Task WriteStartNamespaceDeclarationAsync(string prefix)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			if (prefix.Length == 0)
			{
				await this.RawTextAsync(" xmlns=\"").ConfigureAwait(false);
			}
			else
			{
				await this.RawTextAsync(" xmlns:").ConfigureAwait(false);
				await this.RawTextAsync(prefix).ConfigureAwait(false);
				char[] array = this.bufChars;
				int num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 61;
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 34;
			}
			this.inAttributeValue = true;
			if (this.trackTextContent && !this.inTextContent)
			{
				this.ChangeTextContentMark(true);
			}
		}

		// Token: 0x060007FC RID: 2044 RVA: 0x000227F4 File Offset: 0x000209F4
		internal override Task WriteEndNamespaceDeclarationAsync()
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			this.inAttributeValue = false;
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 34;
			this.attrEndPos = this.bufPos;
			return AsyncHelper.DoneTask;
		}

		// Token: 0x060007FD RID: 2045 RVA: 0x00022850 File Offset: 0x00020A50
		public override async Task WriteCDataAsync(string text)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			int num;
			if (this.mergeCDataSections && this.bufPos == this.cdataPos)
			{
				this.bufPos -= 3;
			}
			else
			{
				char[] array = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 60;
				char[] array2 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 33;
				char[] array3 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 91;
				char[] array4 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 67;
				char[] array5 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array5[num] = 68;
				char[] array6 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array6[num] = 65;
				char[] array7 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array7[num] = 84;
				char[] array8 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array8[num] = 65;
				char[] array9 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array9[num] = 91;
			}
			await this.WriteCDataSectionAsync(text).ConfigureAwait(false);
			char[] array10 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array10[num] = 93;
			char[] array11 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array11[num] = 93;
			char[] array12 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array12[num] = 62;
			this.textPos = this.bufPos;
			this.cdataPos = this.bufPos;
		}

		// Token: 0x060007FE RID: 2046 RVA: 0x000228A0 File Offset: 0x00020AA0
		public override async Task WriteCommentAsync(string text)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 33;
			char[] array3 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 45;
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 45;
			await this.WriteCommentOrPiAsync(text, 45).ConfigureAwait(false);
			char[] array5 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 45;
			char[] array6 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 45;
			char[] array7 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array7[num] = 62;
		}

		// Token: 0x060007FF RID: 2047 RVA: 0x000228F0 File Offset: 0x00020AF0
		public override async Task WriteProcessingInstructionAsync(string name, string text)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 63;
			await this.RawTextAsync(name).ConfigureAwait(false);
			if (text.Length > 0)
			{
				char[] array3 = this.bufChars;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 32;
				await this.WriteCommentOrPiAsync(text, 63).ConfigureAwait(false);
			}
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 63;
			char[] array5 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 62;
		}

		// Token: 0x06000800 RID: 2048 RVA: 0x00022948 File Offset: 0x00020B48
		public override async Task WriteEntityRefAsync(string name)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 38;
			await this.RawTextAsync(name).ConfigureAwait(false);
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 59;
			if (this.bufPos > this.bufLen)
			{
				await this.FlushBufferAsync().ConfigureAwait(false);
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x06000801 RID: 2049 RVA: 0x00022998 File Offset: 0x00020B98
		public override async Task WriteCharEntityAsync(char ch)
		{
			this.CheckAsyncCall();
			int num = (int)ch;
			string text = num.ToString("X", NumberFormatInfo.InvariantInfo);
			if (this.checkCharacters && !this.xmlCharType.IsCharData(ch))
			{
				throw XmlConvert.CreateInvalidCharException(ch, '\0');
			}
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			char[] array = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 38;
			char[] array2 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 35;
			char[] array3 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 120;
			await this.RawTextAsync(text).ConfigureAwait(false);
			char[] array4 = this.bufChars;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 59;
			if (this.bufPos > this.bufLen)
			{
				await this.FlushBufferAsync().ConfigureAwait(false);
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x06000802 RID: 2050 RVA: 0x000229E5 File Offset: 0x00020BE5
		public override Task WriteWhitespaceAsync(string ws)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			if (this.inAttributeValue)
			{
				return this.WriteAttributeTextBlockAsync(ws);
			}
			return this.WriteElementTextBlockAsync(ws);
		}

		// Token: 0x06000803 RID: 2051 RVA: 0x00022A1B File Offset: 0x00020C1B
		public override Task WriteStringAsync(string text)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && !this.inTextContent)
			{
				this.ChangeTextContentMark(true);
			}
			if (this.inAttributeValue)
			{
				return this.WriteAttributeTextBlockAsync(text);
			}
			return this.WriteElementTextBlockAsync(text);
		}

		// Token: 0x06000804 RID: 2052 RVA: 0x00022A54 File Offset: 0x00020C54
		public override async Task WriteSurrogateCharEntityAsync(char lowChar, char highChar)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			int num = XmlCharType.CombineSurrogateChar((int)lowChar, (int)highChar);
			char[] array = this.bufChars;
			int num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array[num2] = 38;
			char[] array2 = this.bufChars;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array2[num2] = 35;
			char[] array3 = this.bufChars;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array3[num2] = 120;
			await this.RawTextAsync(num.ToString("X", NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			char[] array4 = this.bufChars;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array4[num2] = 59;
			this.textPos = this.bufPos;
		}

		// Token: 0x06000805 RID: 2053 RVA: 0x00022AA9 File Offset: 0x00020CA9
		public override Task WriteCharsAsync(char[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && !this.inTextContent)
			{
				this.ChangeTextContentMark(true);
			}
			if (this.inAttributeValue)
			{
				return this.WriteAttributeTextBlockAsync(buffer, index, count);
			}
			return this.WriteElementTextBlockAsync(buffer, index, count);
		}

		// Token: 0x06000806 RID: 2054 RVA: 0x00022AE4 File Offset: 0x00020CE4
		public override async Task WriteRawAsync(char[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			await this.WriteRawWithCharCheckingAsync(buffer, index, count).ConfigureAwait(false);
			this.textPos = this.bufPos;
		}

		// Token: 0x06000807 RID: 2055 RVA: 0x00022B44 File Offset: 0x00020D44
		public override async Task WriteRawAsync(string data)
		{
			this.CheckAsyncCall();
			if (this.trackTextContent && this.inTextContent)
			{
				this.ChangeTextContentMark(false);
			}
			await this.WriteRawWithCharCheckingAsync(data).ConfigureAwait(false);
			this.textPos = this.bufPos;
		}

		// Token: 0x06000808 RID: 2056 RVA: 0x00022B94 File Offset: 0x00020D94
		public override async Task FlushAsync()
		{
			this.CheckAsyncCall();
			await this.FlushBufferAsync().ConfigureAwait(false);
			await this.FlushEncoderAsync().ConfigureAwait(false);
			if (this.stream != null)
			{
				await this.stream.FlushAsync().ConfigureAwait(false);
			}
			else if (this.writer != null)
			{
				await this.writer.FlushAsync().ConfigureAwait(false);
			}
		}

		// Token: 0x06000809 RID: 2057 RVA: 0x00022BDC File Offset: 0x00020DDC
		protected virtual async Task FlushBufferAsync()
		{
			try
			{
				if (!this.writeToNull)
				{
					if (this.stream != null)
					{
						if (this.trackTextContent)
						{
							this.charEntityFallback.Reset(this.textContentMarks, this.lastMarkPos);
							if ((this.lastMarkPos & 1) != 0)
							{
								this.textContentMarks[1] = 1;
								this.lastMarkPos = 1;
							}
							else
							{
								this.lastMarkPos = 0;
							}
						}
						await this.EncodeCharsAsync(1, this.bufPos, true).ConfigureAwait(false);
					}
					else
					{
						await this.writer.WriteAsync(this.bufChars, 1, this.bufPos - 1).ConfigureAwait(false);
					}
				}
			}
			catch
			{
				this.writeToNull = true;
				throw;
			}
			finally
			{
				this.bufChars[0] = this.bufChars[this.bufPos - 1];
				this.textPos = ((this.textPos == this.bufPos) ? 1 : 0);
				this.attrEndPos = ((this.attrEndPos == this.bufPos) ? 1 : 0);
				this.contentPos = 0;
				this.cdataPos = 0;
				this.bufPos = 1;
			}
		}

		// Token: 0x0600080A RID: 2058 RVA: 0x00022C24 File Offset: 0x00020E24
		private async Task EncodeCharsAsync(int startOffset, int endOffset, bool writeAllToStream)
		{
			while (startOffset < endOffset)
			{
				if (this.charEntityFallback != null)
				{
					this.charEntityFallback.StartOffset = startOffset;
				}
				int num;
				int num2;
				bool flag;
				this.encoder.Convert(this.bufChars, startOffset, endOffset - startOffset, this.bufBytes, this.bufBytesUsed, this.bufBytes.Length - this.bufBytesUsed, false, out num, out num2, out flag);
				startOffset += num;
				this.bufBytesUsed += num2;
				if (this.bufBytesUsed >= this.bufBytes.Length - 16)
				{
					await this.stream.WriteAsync(this.bufBytes, 0, this.bufBytesUsed).ConfigureAwait(false);
					this.bufBytesUsed = 0;
				}
			}
			if (writeAllToStream && this.bufBytesUsed > 0)
			{
				await this.stream.WriteAsync(this.bufBytes, 0, this.bufBytesUsed).ConfigureAwait(false);
				this.bufBytesUsed = 0;
			}
		}

		// Token: 0x0600080B RID: 2059 RVA: 0x00022C84 File Offset: 0x00020E84
		private Task FlushEncoderAsync()
		{
			if (this.stream != null)
			{
				int num;
				int num2;
				bool flag;
				this.encoder.Convert(this.bufChars, 1, 0, this.bufBytes, 0, this.bufBytes.Length, true, out num, out num2, out flag);
				if (num2 != 0)
				{
					return this.stream.WriteAsync(this.bufBytes, 0, num2);
				}
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x0600080C RID: 2060 RVA: 0x00022CE0 File Offset: 0x00020EE0
		[SecuritySafeCritical]
		protected unsafe int WriteAttributeTextBlockNoFlush(char* pSrc, char* pSrcEnd)
		{
			char* ptr = pSrc;
			char[] array;
			char* ptr2;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr2 = null;
			}
			else
			{
				ptr2 = &array[0];
			}
			char* ptr3 = ptr2 + this.bufPos;
			int num = 0;
			for (;;)
			{
				char* ptr4 = ptr3 + (long)(pSrcEnd - pSrc) * 2L / 2L;
				if (ptr4 != ptr2 + this.bufLen)
				{
					ptr4 = ptr2 + this.bufLen;
				}
				while (ptr3 < ptr4 && (this.xmlCharType.charProperties[num = (int)(*pSrc)] & 128) != 0)
				{
					*ptr3 = (char)num;
					ptr3++;
					pSrc++;
				}
				if (pSrc >= pSrcEnd)
				{
					goto IL_1EE;
				}
				if (ptr3 >= ptr4)
				{
					break;
				}
				if (num <= 38)
				{
					switch (num)
					{
					case 9:
						if (this.newLineHandling == NewLineHandling.None)
						{
							*ptr3 = (char)num;
							ptr3++;
							goto IL_1E4;
						}
						ptr3 = XmlEncodedRawTextWriter.TabEntity(ptr3);
						goto IL_1E4;
					case 10:
						if (this.newLineHandling == NewLineHandling.None)
						{
							*ptr3 = (char)num;
							ptr3++;
							goto IL_1E4;
						}
						ptr3 = XmlEncodedRawTextWriter.LineFeedEntity(ptr3);
						goto IL_1E4;
					case 11:
					case 12:
						break;
					case 13:
						if (this.newLineHandling == NewLineHandling.None)
						{
							*ptr3 = (char)num;
							ptr3++;
							goto IL_1E4;
						}
						ptr3 = XmlEncodedRawTextWriter.CarriageReturnEntity(ptr3);
						goto IL_1E4;
					default:
						if (num == 34)
						{
							ptr3 = XmlEncodedRawTextWriter.QuoteEntity(ptr3);
							goto IL_1E4;
						}
						if (num == 38)
						{
							ptr3 = XmlEncodedRawTextWriter.AmpEntity(ptr3);
							goto IL_1E4;
						}
						break;
					}
				}
				else
				{
					if (num == 39)
					{
						*ptr3 = (char)num;
						ptr3++;
						goto IL_1E4;
					}
					if (num == 60)
					{
						ptr3 = XmlEncodedRawTextWriter.LtEntity(ptr3);
						goto IL_1E4;
					}
					if (num == 62)
					{
						ptr3 = XmlEncodedRawTextWriter.GtEntity(ptr3);
						goto IL_1E4;
					}
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr3 = XmlEncodedRawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, ptr3);
					pSrc += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr3 = this.InvalidXmlChar(num, ptr3, true);
					pSrc++;
					continue;
				}
				*ptr3 = (char)num;
				ptr3++;
				pSrc++;
				continue;
				IL_1E4:
				pSrc++;
			}
			this.bufPos = (int)((long)(ptr3 - ptr2));
			return (int)((long)(pSrc - ptr));
			IL_1EE:
			this.bufPos = (int)((long)(ptr3 - ptr2));
			array = null;
			return -1;
		}

		// Token: 0x0600080D RID: 2061 RVA: 0x00022EEC File Offset: 0x000210EC
		[SecuritySafeCritical]
		protected unsafe int WriteAttributeTextBlockNoFlush(char[] chars, int index, int count)
		{
			if (count == 0)
			{
				return -1;
			}
			fixed (char* ptr = &chars[index])
			{
				char* ptr2 = ptr;
				char* pSrcEnd = ptr2 + count;
				return this.WriteAttributeTextBlockNoFlush(ptr2, pSrcEnd);
			}
		}

		// Token: 0x0600080E RID: 2062 RVA: 0x00022F18 File Offset: 0x00021118
		[SecuritySafeCritical]
		protected unsafe int WriteAttributeTextBlockNoFlush(string text, int index, int count)
		{
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char* pSrcEnd = ptr2 + count;
			return this.WriteAttributeTextBlockNoFlush(ptr2, pSrcEnd);
		}

		// Token: 0x0600080F RID: 2063 RVA: 0x00022F50 File Offset: 0x00021150
		protected async Task WriteAttributeTextBlockAsync(char[] chars, int index, int count)
		{
			int writeLen = 0;
			int curIndex = index;
			int leftCount = count;
			do
			{
				writeLen = this.WriteAttributeTextBlockNoFlush(chars, curIndex, leftCount);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0);
		}

		// Token: 0x06000810 RID: 2064 RVA: 0x00022FB0 File Offset: 0x000211B0
		protected Task WriteAttributeTextBlockAsync(string text)
		{
			int num = 0;
			int num2 = text.Length;
			int num3 = this.WriteAttributeTextBlockNoFlush(text, num, num2);
			num += num3;
			num2 -= num3;
			if (num3 >= 0)
			{
				return this._WriteAttributeTextBlockAsync(text, num, num2);
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000811 RID: 2065 RVA: 0x00022FF0 File Offset: 0x000211F0
		private async Task _WriteAttributeTextBlockAsync(string text, int curIndex, int leftCount)
		{
			await this.FlushBufferAsync().ConfigureAwait(false);
			int writeLen;
			do
			{
				writeLen = this.WriteAttributeTextBlockNoFlush(text, curIndex, leftCount);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0);
		}

		// Token: 0x06000812 RID: 2066 RVA: 0x00023050 File Offset: 0x00021250
		[SecuritySafeCritical]
		protected unsafe int WriteElementTextBlockNoFlush(char* pSrc, char* pSrcEnd, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			char* ptr = pSrc;
			char[] array;
			char* ptr2;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr2 = null;
			}
			else
			{
				ptr2 = &array[0];
			}
			char* ptr3 = ptr2 + this.bufPos;
			int num = 0;
			for (;;)
			{
				char* ptr4 = ptr3 + (long)(pSrcEnd - pSrc) * 2L / 2L;
				if (ptr4 != ptr2 + this.bufLen)
				{
					ptr4 = ptr2 + this.bufLen;
				}
				while (ptr3 < ptr4 && (this.xmlCharType.charProperties[num = (int)(*pSrc)] & 128) != 0)
				{
					*ptr3 = (char)num;
					ptr3++;
					pSrc++;
				}
				if (pSrc >= pSrcEnd)
				{
					goto IL_20F;
				}
				if (ptr3 >= ptr4)
				{
					break;
				}
				if (num <= 38)
				{
					switch (num)
					{
					case 9:
						goto IL_11A;
					case 10:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_13;
						}
						*ptr3 = (char)num;
						ptr3++;
						goto IL_205;
					case 11:
					case 12:
						break;
					case 13:
						switch (this.newLineHandling)
						{
						case NewLineHandling.Replace:
							goto IL_176;
						case NewLineHandling.Entitize:
							ptr3 = XmlEncodedRawTextWriter.CarriageReturnEntity(ptr3);
							goto IL_205;
						case NewLineHandling.None:
							*ptr3 = (char)num;
							ptr3++;
							goto IL_205;
						default:
							goto IL_205;
						}
						break;
					default:
						if (num == 34)
						{
							goto IL_11A;
						}
						if (num == 38)
						{
							ptr3 = XmlEncodedRawTextWriter.AmpEntity(ptr3);
							goto IL_205;
						}
						break;
					}
				}
				else
				{
					if (num == 39)
					{
						goto IL_11A;
					}
					if (num == 60)
					{
						ptr3 = XmlEncodedRawTextWriter.LtEntity(ptr3);
						goto IL_205;
					}
					if (num == 62)
					{
						ptr3 = XmlEncodedRawTextWriter.GtEntity(ptr3);
						goto IL_205;
					}
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr3 = XmlEncodedRawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, ptr3);
					pSrc += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr3 = this.InvalidXmlChar(num, ptr3, true);
					pSrc++;
					continue;
				}
				*ptr3 = (char)num;
				ptr3++;
				pSrc++;
				continue;
				IL_205:
				pSrc++;
				continue;
				IL_11A:
				*ptr3 = (char)num;
				ptr3++;
				goto IL_205;
			}
			this.bufPos = (int)((long)(ptr3 - ptr2));
			return (int)((long)(pSrc - ptr));
			Block_13:
			this.bufPos = (int)((long)(ptr3 - ptr2));
			needWriteNewLine = true;
			return (int)((long)(pSrc - ptr));
			IL_176:
			if (pSrc[1] == '\n')
			{
				pSrc++;
			}
			this.bufPos = (int)((long)(ptr3 - ptr2));
			needWriteNewLine = true;
			return (int)((long)(pSrc - ptr));
			IL_20F:
			this.bufPos = (int)((long)(ptr3 - ptr2));
			this.textPos = this.bufPos;
			this.contentPos = 0;
			array = null;
			return -1;
		}

		// Token: 0x06000813 RID: 2067 RVA: 0x00023290 File Offset: 0x00021490
		[SecuritySafeCritical]
		protected unsafe int WriteElementTextBlockNoFlush(char[] chars, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				this.contentPos = 0;
				return -1;
			}
			fixed (char* ptr = &chars[index])
			{
				char* ptr2 = ptr;
				char* pSrcEnd = ptr2 + count;
				return this.WriteElementTextBlockNoFlush(ptr2, pSrcEnd, out needWriteNewLine);
			}
		}

		// Token: 0x06000814 RID: 2068 RVA: 0x000232CC File Offset: 0x000214CC
		[SecuritySafeCritical]
		protected unsafe int WriteElementTextBlockNoFlush(string text, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				this.contentPos = 0;
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char* pSrcEnd = ptr2 + count;
			return this.WriteElementTextBlockNoFlush(ptr2, pSrcEnd, out needWriteNewLine);
		}

		// Token: 0x06000815 RID: 2069 RVA: 0x00023314 File Offset: 0x00021514
		protected async Task WriteElementTextBlockAsync(char[] chars, int index, int count)
		{
			int writeLen = 0;
			int curIndex = index;
			int leftCount = count;
			bool needWriteNewLine = false;
			do
			{
				writeLen = this.WriteElementTextBlockNoFlush(chars, curIndex, leftCount, out needWriteNewLine);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (needWriteNewLine)
				{
					await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
					curIndex++;
					leftCount--;
				}
				else if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0 || needWriteNewLine);
		}

		// Token: 0x06000816 RID: 2070 RVA: 0x00023374 File Offset: 0x00021574
		protected Task WriteElementTextBlockAsync(string text)
		{
			int num = 0;
			int num2 = text.Length;
			bool flag = false;
			int num3 = this.WriteElementTextBlockNoFlush(text, num, num2, out flag);
			num += num3;
			num2 -= num3;
			if (flag)
			{
				return this._WriteElementTextBlockAsync(true, text, num, num2);
			}
			if (num3 >= 0)
			{
				return this._WriteElementTextBlockAsync(false, text, num, num2);
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000817 RID: 2071 RVA: 0x000233C4 File Offset: 0x000215C4
		private async Task _WriteElementTextBlockAsync(bool newLine, string text, int curIndex, int leftCount)
		{
			int writeLen = 0;
			bool needWriteNewLine = false;
			if (newLine)
			{
				await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
				curIndex++;
				leftCount--;
			}
			else
			{
				await this.FlushBufferAsync().ConfigureAwait(false);
			}
			do
			{
				writeLen = this.WriteElementTextBlockNoFlush(text, curIndex, leftCount, out needWriteNewLine);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (needWriteNewLine)
				{
					await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
					curIndex++;
					leftCount--;
				}
				else if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0 || needWriteNewLine);
		}

		// Token: 0x06000818 RID: 2072 RVA: 0x0002342C File Offset: 0x0002162C
		[SecuritySafeCritical]
		protected unsafe int RawTextNoFlush(char* pSrcBegin, char* pSrcEnd)
		{
			char[] array;
			char* ptr;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			char* ptr2 = ptr + this.bufPos;
			char* ptr3 = pSrcBegin;
			int num = 0;
			for (;;)
			{
				char* ptr4 = ptr2 + (long)(pSrcEnd - ptr3) * 2L / 2L;
				if (ptr4 != ptr + this.bufLen)
				{
					ptr4 = ptr + this.bufLen;
				}
				while (ptr2 < ptr4 && (num = (int)(*ptr3)) < 55296)
				{
					ptr3++;
					*ptr2 = (char)num;
					ptr2++;
				}
				if (ptr3 >= pSrcEnd)
				{
					goto IL_F9;
				}
				if (ptr2 >= ptr4)
				{
					break;
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr2 = XmlEncodedRawTextWriter.EncodeSurrogate(ptr3, pSrcEnd, ptr2);
					ptr3 += 2;
				}
				else if (num <= 127 || num >= 65534)
				{
					ptr2 = this.InvalidXmlChar(num, ptr2, false);
					ptr3++;
				}
				else
				{
					*ptr2 = (char)num;
					ptr2++;
					ptr3++;
				}
			}
			this.bufPos = (int)((long)(ptr2 - ptr));
			return (int)((long)(ptr3 - pSrcBegin));
			IL_F9:
			this.bufPos = (int)((long)(ptr2 - ptr));
			array = null;
			return -1;
		}

		// Token: 0x06000819 RID: 2073 RVA: 0x00023544 File Offset: 0x00021744
		[SecuritySafeCritical]
		protected unsafe int RawTextNoFlush(string text, int index, int count)
		{
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char* pSrcEnd = ptr2 + count;
			return this.RawTextNoFlush(ptr2, pSrcEnd);
		}

		// Token: 0x0600081A RID: 2074 RVA: 0x0002357C File Offset: 0x0002177C
		protected Task RawTextAsync(string text)
		{
			int num = 0;
			int num2 = text.Length;
			int num3 = this.RawTextNoFlush(text, num, num2);
			num += num3;
			num2 -= num3;
			if (num3 >= 0)
			{
				return this._RawTextAsync(text, num, num2);
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x0600081B RID: 2075 RVA: 0x000235BC File Offset: 0x000217BC
		private async Task _RawTextAsync(string text, int curIndex, int leftCount)
		{
			await this.FlushBufferAsync().ConfigureAwait(false);
			int writeLen = 0;
			do
			{
				writeLen = this.RawTextNoFlush(text, curIndex, leftCount);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0);
		}

		// Token: 0x0600081C RID: 2076 RVA: 0x0002361C File Offset: 0x0002181C
		[SecuritySafeCritical]
		protected unsafe int WriteRawWithCharCheckingNoFlush(char* pSrcBegin, char* pSrcEnd, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			char[] array;
			char* ptr;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			char* ptr2 = pSrcBegin;
			char* ptr3 = ptr + this.bufPos;
			int num = 0;
			for (;;)
			{
				char* ptr4 = ptr3 + (long)(pSrcEnd - ptr2) * 2L / 2L;
				if (ptr4 != ptr + this.bufLen)
				{
					ptr4 = ptr + this.bufLen;
				}
				while (ptr3 < ptr4 && (this.xmlCharType.charProperties[num = (int)(*ptr2)] & 64) != 0)
				{
					*ptr3 = (char)num;
					ptr3++;
					ptr2++;
				}
				if (ptr2 >= pSrcEnd)
				{
					goto IL_1CC;
				}
				if (ptr3 >= ptr4)
				{
					break;
				}
				if (num <= 38)
				{
					switch (num)
					{
					case 9:
						goto IL_EB;
					case 10:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_12;
						}
						*ptr3 = (char)num;
						ptr3++;
						goto IL_1C3;
					case 11:
					case 12:
						break;
					case 13:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_10;
						}
						*ptr3 = (char)num;
						ptr3++;
						goto IL_1C3;
					default:
						if (num == 38)
						{
							goto IL_EB;
						}
						break;
					}
				}
				else if (num == 60 || num == 93)
				{
					goto IL_EB;
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr3 = XmlEncodedRawTextWriter.EncodeSurrogate(ptr2, pSrcEnd, ptr3);
					ptr2 += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr3 = this.InvalidXmlChar(num, ptr3, false);
					ptr2++;
					continue;
				}
				*ptr3 = (char)num;
				ptr3++;
				ptr2++;
				continue;
				IL_1C3:
				ptr2++;
				continue;
				IL_EB:
				*ptr3 = (char)num;
				ptr3++;
				goto IL_1C3;
			}
			this.bufPos = (int)((long)(ptr3 - ptr));
			return (int)((long)(ptr2 - pSrcBegin));
			Block_10:
			if (ptr2[1] == '\n')
			{
				ptr2++;
			}
			this.bufPos = (int)((long)(ptr3 - ptr));
			needWriteNewLine = true;
			return (int)((long)(ptr2 - pSrcBegin));
			Block_12:
			this.bufPos = (int)((long)(ptr3 - ptr));
			needWriteNewLine = true;
			return (int)((long)(ptr2 - pSrcBegin));
			IL_1CC:
			this.bufPos = (int)((long)(ptr3 - ptr));
			array = null;
			return -1;
		}

		// Token: 0x0600081D RID: 2077 RVA: 0x00023808 File Offset: 0x00021A08
		[SecuritySafeCritical]
		protected unsafe int WriteRawWithCharCheckingNoFlush(char[] chars, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				return -1;
			}
			fixed (char* ptr = &chars[index])
			{
				char* ptr2 = ptr;
				char* pSrcEnd = ptr2 + count;
				return this.WriteRawWithCharCheckingNoFlush(ptr2, pSrcEnd, out needWriteNewLine);
			}
		}

		// Token: 0x0600081E RID: 2078 RVA: 0x0002383C File Offset: 0x00021A3C
		[SecuritySafeCritical]
		protected unsafe int WriteRawWithCharCheckingNoFlush(string text, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char* pSrcEnd = ptr2 + count;
			return this.WriteRawWithCharCheckingNoFlush(ptr2, pSrcEnd, out needWriteNewLine);
		}

		// Token: 0x0600081F RID: 2079 RVA: 0x0002387C File Offset: 0x00021A7C
		protected async Task WriteRawWithCharCheckingAsync(char[] chars, int index, int count)
		{
			int writeLen = 0;
			int curIndex = index;
			int leftCount = count;
			bool needWriteNewLine = false;
			do
			{
				writeLen = this.WriteRawWithCharCheckingNoFlush(chars, curIndex, leftCount, out needWriteNewLine);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (needWriteNewLine)
				{
					await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
					curIndex++;
					leftCount--;
				}
				else if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0 || needWriteNewLine);
		}

		// Token: 0x06000820 RID: 2080 RVA: 0x000238DC File Offset: 0x00021ADC
		protected async Task WriteRawWithCharCheckingAsync(string text)
		{
			int writeLen = 0;
			int curIndex = 0;
			int leftCount = text.Length;
			bool needWriteNewLine = false;
			do
			{
				writeLen = this.WriteRawWithCharCheckingNoFlush(text, curIndex, leftCount, out needWriteNewLine);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (needWriteNewLine)
				{
					await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
					curIndex++;
					leftCount--;
				}
				else if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0 || needWriteNewLine);
		}

		// Token: 0x06000821 RID: 2081 RVA: 0x0002392C File Offset: 0x00021B2C
		[SecuritySafeCritical]
		protected unsafe int WriteCommentOrPiNoFlush(string text, int index, int count, int stopChar, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char[] array;
			char* ptr3;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr3 = null;
			}
			else
			{
				ptr3 = &array[0];
			}
			char* ptr4 = ptr2;
			char* ptr5 = ptr4;
			char* ptr6 = ptr2 + count;
			char* ptr7 = ptr3 + this.bufPos;
			int num = 0;
			for (;;)
			{
				char* ptr8 = ptr7 + (long)(ptr6 - ptr4) * 2L / 2L;
				if (ptr8 != ptr3 + this.bufLen)
				{
					ptr8 = ptr3 + this.bufLen;
				}
				while (ptr7 < ptr8 && (this.xmlCharType.charProperties[num = (int)(*ptr4)] & 64) != 0 && num != stopChar)
				{
					*ptr7 = (char)num;
					ptr7++;
					ptr4++;
				}
				if (ptr4 >= ptr6)
				{
					goto IL_2AB;
				}
				if (ptr7 >= ptr8)
				{
					break;
				}
				if (num <= 45)
				{
					switch (num)
					{
					case 9:
						goto IL_230;
					case 10:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_23;
						}
						*ptr7 = (char)num;
						ptr7++;
						goto IL_2A0;
					case 11:
					case 12:
						break;
					case 13:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_21;
						}
						*ptr7 = (char)num;
						ptr7++;
						goto IL_2A0;
					default:
						if (num == 38)
						{
							goto IL_230;
						}
						if (num == 45)
						{
							*ptr7 = '-';
							ptr7++;
							if (num == stopChar && (ptr4 + 1 == ptr6 || ptr4[1] == '-'))
							{
								*ptr7 = ' ';
								ptr7++;
								goto IL_2A0;
							}
							goto IL_2A0;
						}
						break;
					}
				}
				else
				{
					if (num == 60)
					{
						goto IL_230;
					}
					if (num != 63)
					{
						if (num == 93)
						{
							*ptr7 = ']';
							ptr7++;
							goto IL_2A0;
						}
					}
					else
					{
						*ptr7 = '?';
						ptr7++;
						if (num == stopChar && ptr4 + 1 < ptr6 && ptr4[1] == '>')
						{
							*ptr7 = ' ';
							ptr7++;
							goto IL_2A0;
						}
						goto IL_2A0;
					}
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr7 = XmlEncodedRawTextWriter.EncodeSurrogate(ptr4, ptr6, ptr7);
					ptr4 += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr7 = this.InvalidXmlChar(num, ptr7, false);
					ptr4++;
					continue;
				}
				*ptr7 = (char)num;
				ptr7++;
				ptr4++;
				continue;
				IL_2A0:
				ptr4++;
				continue;
				IL_230:
				*ptr7 = (char)num;
				ptr7++;
				goto IL_2A0;
			}
			this.bufPos = (int)((long)(ptr7 - ptr3));
			return (int)((long)(ptr4 - ptr5));
			Block_21:
			if (ptr4[1] == '\n')
			{
				ptr4++;
			}
			this.bufPos = (int)((long)(ptr7 - ptr3));
			needWriteNewLine = true;
			return (int)((long)(ptr4 - ptr5));
			Block_23:
			this.bufPos = (int)((long)(ptr7 - ptr3));
			needWriteNewLine = true;
			return (int)((long)(ptr4 - ptr5));
			IL_2AB:
			this.bufPos = (int)((long)(ptr7 - ptr3));
			array = null;
			return -1;
		}

		// Token: 0x06000822 RID: 2082 RVA: 0x00023BF8 File Offset: 0x00021DF8
		protected async Task WriteCommentOrPiAsync(string text, int stopChar)
		{
			if (text.Length == 0)
			{
				if (this.bufPos >= this.bufLen)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			else
			{
				int writeLen = 0;
				int curIndex = 0;
				int leftCount = text.Length;
				bool needWriteNewLine = false;
				do
				{
					writeLen = this.WriteCommentOrPiNoFlush(text, curIndex, leftCount, stopChar, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
						curIndex++;
						leftCount--;
					}
					else if (writeLen >= 0)
					{
						await this.FlushBufferAsync().ConfigureAwait(false);
					}
				}
				while (writeLen >= 0 || needWriteNewLine);
			}
		}

		// Token: 0x06000823 RID: 2083 RVA: 0x00023C50 File Offset: 0x00021E50
		[SecuritySafeCritical]
		protected unsafe int WriteCDataSectionNoFlush(string text, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char[] array;
			char* ptr3;
			if ((array = this.bufChars) == null || array.Length == 0)
			{
				ptr3 = null;
			}
			else
			{
				ptr3 = &array[0];
			}
			char* ptr4 = ptr2;
			char* ptr5 = ptr2 + count;
			char* ptr6 = ptr4;
			char* ptr7 = ptr3 + this.bufPos;
			int num = 0;
			for (;;)
			{
				char* ptr8 = ptr7 + (long)(ptr5 - ptr4) * 2L / 2L;
				if (ptr8 != ptr3 + this.bufLen)
				{
					ptr8 = ptr3 + this.bufLen;
				}
				while (ptr7 < ptr8 && (this.xmlCharType.charProperties[num = (int)(*ptr4)] & 128) != 0 && num != 93)
				{
					*ptr7 = (char)num;
					ptr7++;
					ptr4++;
				}
				if (ptr4 >= ptr5)
				{
					goto IL_292;
				}
				if (ptr7 >= ptr8)
				{
					break;
				}
				if (num <= 39)
				{
					switch (num)
					{
					case 9:
						goto IL_217;
					case 10:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_21;
						}
						*ptr7 = (char)num;
						ptr7++;
						goto IL_287;
					case 11:
					case 12:
						break;
					case 13:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_19;
						}
						*ptr7 = (char)num;
						ptr7++;
						goto IL_287;
					default:
						if (num == 34 || num - 38 <= 1)
						{
							goto IL_217;
						}
						break;
					}
				}
				else
				{
					if (num == 60)
					{
						goto IL_217;
					}
					if (num == 62)
					{
						if (this.hadDoubleBracket && ptr7[-1] == ']')
						{
							ptr7 = XmlEncodedRawTextWriter.RawEndCData(ptr7);
							ptr7 = XmlEncodedRawTextWriter.RawStartCData(ptr7);
						}
						*ptr7 = '>';
						ptr7++;
						goto IL_287;
					}
					if (num == 93)
					{
						if (ptr7[-1] == ']')
						{
							this.hadDoubleBracket = true;
						}
						else
						{
							this.hadDoubleBracket = false;
						}
						*ptr7 = ']';
						ptr7++;
						goto IL_287;
					}
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr7 = XmlEncodedRawTextWriter.EncodeSurrogate(ptr4, ptr5, ptr7);
					ptr4 += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr7 = this.InvalidXmlChar(num, ptr7, false);
					ptr4++;
					continue;
				}
				*ptr7 = (char)num;
				ptr7++;
				ptr4++;
				continue;
				IL_287:
				ptr4++;
				continue;
				IL_217:
				*ptr7 = (char)num;
				ptr7++;
				goto IL_287;
			}
			this.bufPos = (int)((long)(ptr7 - ptr3));
			return (int)((long)(ptr4 - ptr6));
			Block_19:
			if (ptr4[1] == '\n')
			{
				ptr4++;
			}
			this.bufPos = (int)((long)(ptr7 - ptr3));
			needWriteNewLine = true;
			return (int)((long)(ptr4 - ptr6));
			Block_21:
			this.bufPos = (int)((long)(ptr7 - ptr3));
			needWriteNewLine = true;
			return (int)((long)(ptr4 - ptr6));
			IL_292:
			this.bufPos = (int)((long)(ptr7 - ptr3));
			array = null;
			return -1;
		}

		// Token: 0x06000824 RID: 2084 RVA: 0x00023F00 File Offset: 0x00022100
		protected async Task WriteCDataSectionAsync(string text)
		{
			if (text.Length == 0)
			{
				if (this.bufPos >= this.bufLen)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			else
			{
				int writeLen = 0;
				int curIndex = 0;
				int leftCount = text.Length;
				bool needWriteNewLine = false;
				do
				{
					writeLen = this.WriteCDataSectionNoFlush(text, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
						curIndex++;
						leftCount--;
					}
					else if (writeLen >= 0)
					{
						await this.FlushBufferAsync().ConfigureAwait(false);
					}
				}
				while (writeLen >= 0 || needWriteNewLine);
			}
		}

		// Token: 0x04000426 RID: 1062
		private readonly bool useAsync;

		// Token: 0x04000427 RID: 1063
		protected byte[] bufBytes;

		// Token: 0x04000428 RID: 1064
		protected Stream stream;

		// Token: 0x04000429 RID: 1065
		protected Encoding encoding;

		// Token: 0x0400042A RID: 1066
		protected XmlCharType xmlCharType = XmlCharType.Instance;

		// Token: 0x0400042B RID: 1067
		protected int bufPos = 1;

		// Token: 0x0400042C RID: 1068
		protected int textPos = 1;

		// Token: 0x0400042D RID: 1069
		protected int contentPos;

		// Token: 0x0400042E RID: 1070
		protected int cdataPos;

		// Token: 0x0400042F RID: 1071
		protected int attrEndPos;

		// Token: 0x04000430 RID: 1072
		protected int bufLen = 6144;

		// Token: 0x04000431 RID: 1073
		protected bool writeToNull;

		// Token: 0x04000432 RID: 1074
		protected bool hadDoubleBracket;

		// Token: 0x04000433 RID: 1075
		protected bool inAttributeValue;

		// Token: 0x04000434 RID: 1076
		protected int bufBytesUsed;

		// Token: 0x04000435 RID: 1077
		protected char[] bufChars;

		// Token: 0x04000436 RID: 1078
		protected Encoder encoder;

		// Token: 0x04000437 RID: 1079
		protected TextWriter writer;

		// Token: 0x04000438 RID: 1080
		protected bool trackTextContent;

		// Token: 0x04000439 RID: 1081
		protected bool inTextContent;

		// Token: 0x0400043A RID: 1082
		private int lastMarkPos;

		// Token: 0x0400043B RID: 1083
		private int[] textContentMarks;

		// Token: 0x0400043C RID: 1084
		private CharEntityEncoderFallback charEntityFallback;

		// Token: 0x0400043D RID: 1085
		protected NewLineHandling newLineHandling;

		// Token: 0x0400043E RID: 1086
		protected bool closeOutput;

		// Token: 0x0400043F RID: 1087
		protected bool omitXmlDeclaration;

		// Token: 0x04000440 RID: 1088
		protected string newLineChars;

		// Token: 0x04000441 RID: 1089
		protected bool checkCharacters;

		// Token: 0x04000442 RID: 1090
		protected XmlStandalone standalone;

		// Token: 0x04000443 RID: 1091
		protected XmlOutputMethod outputMethod;

		// Token: 0x04000444 RID: 1092
		protected bool autoXmlDeclaration;

		// Token: 0x04000445 RID: 1093
		protected bool mergeCDataSections;

		// Token: 0x04000446 RID: 1094
		private const int BUFSIZE = 6144;

		// Token: 0x04000447 RID: 1095
		private const int ASYNCBUFSIZE = 65536;

		// Token: 0x04000448 RID: 1096
		private const int OVERFLOW = 32;

		// Token: 0x04000449 RID: 1097
		private const int INIT_MARKS_COUNT = 64;

		// Token: 0x020000DB RID: 219
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteXmlDeclarationAsync>d__96 : IAsyncStateMachine
		{
			// Token: 0x06000825 RID: 2085 RVA: 0x00023F50 File Offset: 0x00022150
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_12E;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1A2;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_211;
					}
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_285;
					}
					case 5:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2FE;
					}
					case 6:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_364;
					}
					default:
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.omitXmlDeclaration || xmlEncodedRawTextWriter.autoXmlDeclaration)
						{
							goto IL_36B;
						}
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync("<?xml version=\"").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteXmlDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync("1.0").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteXmlDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_12E:
					configuredTaskAwaiter.GetResult();
					if (xmlEncodedRawTextWriter.encoding == null)
					{
						goto IL_218;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync("\" encoding=\"").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteXmlDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_1A2:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(xmlEncodedRawTextWriter.encoding.WebName).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteXmlDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_211:
					configuredTaskAwaiter.GetResult();
					IL_218:
					if (standalone == XmlStandalone.Omit)
					{
						goto IL_305;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync("\" standalone=\"").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteXmlDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_285:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync((standalone == XmlStandalone.Yes) ? "yes" : "no").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteXmlDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_2FE:
					configuredTaskAwaiter.GetResult();
					IL_305:
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync("\"?>").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 6;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteXmlDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_364:
					configuredTaskAwaiter.GetResult();
					IL_36B:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000826 RID: 2086 RVA: 0x00024314 File Offset: 0x00022514
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400044A RID: 1098
			public int <>1__state;

			// Token: 0x0400044B RID: 1099
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400044C RID: 1100
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x0400044D RID: 1101
			public XmlStandalone standalone;

			// Token: 0x0400044E RID: 1102
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000DC RID: 220
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteDocTypeAsync>d__98 : IAsyncStateMachine
		{
			// Token: 0x06000827 RID: 2087 RVA: 0x00024324 File Offset: 0x00022524
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_121;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_195;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1FF;
					}
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_268;
					}
					case 5:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2DA;
					}
					case 6:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_370;
					}
					case 7:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3DA;
					}
					case 8:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_4A8;
					}
					default:
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync("<!DOCTYPE ").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(name).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_121:
					configuredTaskAwaiter.GetResult();
					int bufPos;
					if (pubid != null)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(" PUBLIC \"").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (sysid == null)
						{
							char[] bufChars = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter2 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter2.bufPos = bufPos + 1;
							bufChars[bufPos] = 32;
							goto IL_41D;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(" SYSTEM \"").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 6;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_370;
					}
					IL_195:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(pubid).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_1FF:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync("\" \"").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_268:
					configuredTaskAwaiter.GetResult();
					if (sysid == null)
					{
						goto IL_2E1;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(sysid).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_2DA:
					configuredTaskAwaiter.GetResult();
					IL_2E1:
					char[] bufChars2 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter3 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter3.bufPos = bufPos + 1;
					bufChars2[bufPos] = 34;
					goto IL_41D;
					IL_370:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(sysid).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 7;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_3DA:
					configuredTaskAwaiter.GetResult();
					char[] bufChars3 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter4 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter4.bufPos = bufPos + 1;
					bufChars3[bufPos] = 34;
					IL_41D:
					if (subset == null)
					{
						goto IL_4CC;
					}
					char[] bufChars4 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter5 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter5.bufPos = bufPos + 1;
					bufChars4[bufPos] = 91;
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(subset).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 8;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteDocTypeAsync>d__98>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_4A8:
					configuredTaskAwaiter.GetResult();
					char[] bufChars5 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter6 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter6.bufPos = bufPos + 1;
					bufChars5[bufPos] = 93;
					IL_4CC:
					char[] bufChars6 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter7 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter7.bufPos = bufPos + 1;
					bufChars6[bufPos] = 62;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000828 RID: 2088 RVA: 0x00024864 File Offset: 0x00022A64
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400044F RID: 1103
			public int <>1__state;

			// Token: 0x04000450 RID: 1104
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000451 RID: 1105
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x04000452 RID: 1106
			public string name;

			// Token: 0x04000453 RID: 1107
			public string pubid;

			// Token: 0x04000454 RID: 1108
			public string sysid;

			// Token: 0x04000455 RID: 1109
			public string subset;

			// Token: 0x04000456 RID: 1110
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000DD RID: 221
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteNamespaceDeclarationAsync>d__106 : IAsyncStateMachine
		{
			// Token: 0x06000829 RID: 2089 RVA: 0x00024874 File Offset: 0x00022A74
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_F3;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_154;
					}
					default:
						xmlEncodedRawTextWriter.CheckAsyncCall();
						configuredTaskAwaiter = xmlEncodedRawTextWriter.WriteStartNamespaceDeclarationAsync(prefix).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteNamespaceDeclarationAsync>d__106>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.WriteStringAsync(namespaceName).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteNamespaceDeclarationAsync>d__106>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_F3:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.WriteEndNamespaceDeclarationAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteNamespaceDeclarationAsync>d__106>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_154:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600082A RID: 2090 RVA: 0x00024A28 File Offset: 0x00022C28
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000457 RID: 1111
			public int <>1__state;

			// Token: 0x04000458 RID: 1112
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000459 RID: 1113
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x0400045A RID: 1114
			public string prefix;

			// Token: 0x0400045B RID: 1115
			public string namespaceName;

			// Token: 0x0400045C RID: 1116
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000DE RID: 222
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartNamespaceDeclarationAsync>d__107 : IAsyncStateMachine
		{
			// Token: 0x0600082B RID: 2091 RVA: 0x00024A38 File Offset: 0x00022C38
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_11A;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_184;
					}
					default:
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						if (prefix.Length == 0)
						{
							configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(" xmlns=\"").ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteStartNamespaceDeclarationAsync>d__107>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(" xmlns:").ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteStartNamespaceDeclarationAsync>d__107>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_11A;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					goto IL_1C5;
					IL_11A:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(prefix).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteStartNamespaceDeclarationAsync>d__107>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_184:
					configuredTaskAwaiter.GetResult();
					char[] bufChars = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter2 = xmlEncodedRawTextWriter;
					int bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter2.bufPos = bufPos + 1;
					bufChars[bufPos] = 61;
					char[] bufChars2 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter3 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter3.bufPos = bufPos + 1;
					bufChars2[bufPos] = 34;
					IL_1C5:
					xmlEncodedRawTextWriter.inAttributeValue = true;
					if (xmlEncodedRawTextWriter.trackTextContent && !xmlEncodedRawTextWriter.inTextContent)
					{
						xmlEncodedRawTextWriter.ChangeTextContentMark(true);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600082C RID: 2092 RVA: 0x00024C74 File Offset: 0x00022E74
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400045D RID: 1117
			public int <>1__state;

			// Token: 0x0400045E RID: 1118
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400045F RID: 1119
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x04000460 RID: 1120
			public string prefix;

			// Token: 0x04000461 RID: 1121
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000DF RID: 223
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCDataAsync>d__109 : IAsyncStateMachine
		{
			// Token: 0x0600082D RID: 2093 RVA: 0x00024C84 File Offset: 0x00022E84
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					int bufPos;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						if (xmlEncodedRawTextWriter.mergeCDataSections && xmlEncodedRawTextWriter.bufPos == xmlEncodedRawTextWriter.cdataPos)
						{
							xmlEncodedRawTextWriter.bufPos -= 3;
						}
						else
						{
							char[] bufChars = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter2 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter2.bufPos = bufPos + 1;
							bufChars[bufPos] = 60;
							char[] bufChars2 = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter3 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter3.bufPos = bufPos + 1;
							bufChars2[bufPos] = 33;
							char[] bufChars3 = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter4 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter4.bufPos = bufPos + 1;
							bufChars3[bufPos] = 91;
							char[] bufChars4 = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter5 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter5.bufPos = bufPos + 1;
							bufChars4[bufPos] = 67;
							char[] bufChars5 = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter6 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter6.bufPos = bufPos + 1;
							bufChars5[bufPos] = 68;
							char[] bufChars6 = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter7 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter7.bufPos = bufPos + 1;
							bufChars6[bufPos] = 65;
							char[] bufChars7 = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter8 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter8.bufPos = bufPos + 1;
							bufChars7[bufPos] = 84;
							char[] bufChars8 = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter9 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter9.bufPos = bufPos + 1;
							bufChars8[bufPos] = 65;
							char[] bufChars9 = xmlEncodedRawTextWriter.bufChars;
							XmlEncodedRawTextWriter xmlEncodedRawTextWriter10 = xmlEncodedRawTextWriter;
							bufPos = xmlEncodedRawTextWriter.bufPos;
							xmlEncodedRawTextWriter10.bufPos = bufPos + 1;
							bufChars9[bufPos] = 91;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.WriteCDataSectionAsync(text).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCDataAsync>d__109>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					char[] bufChars10 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter11 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter11.bufPos = bufPos + 1;
					bufChars10[bufPos] = 93;
					char[] bufChars11 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter12 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter12.bufPos = bufPos + 1;
					bufChars11[bufPos] = 93;
					char[] bufChars12 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter13 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter13.bufPos = bufPos + 1;
					bufChars12[bufPos] = 62;
					xmlEncodedRawTextWriter.textPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter.cdataPos = xmlEncodedRawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600082E RID: 2094 RVA: 0x00024EF0 File Offset: 0x000230F0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000462 RID: 1122
			public int <>1__state;

			// Token: 0x04000463 RID: 1123
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000464 RID: 1124
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x04000465 RID: 1125
			public string text;

			// Token: 0x04000466 RID: 1126
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E0 RID: 224
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCommentAsync>d__110 : IAsyncStateMachine
		{
			// Token: 0x0600082F RID: 2095 RVA: 0x00024F00 File Offset: 0x00023100
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					int bufPos;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						char[] bufChars = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter2 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter2.bufPos = bufPos + 1;
						bufChars[bufPos] = 60;
						char[] bufChars2 = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter3 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter3.bufPos = bufPos + 1;
						bufChars2[bufPos] = 33;
						char[] bufChars3 = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter4 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter4.bufPos = bufPos + 1;
						bufChars3[bufPos] = 45;
						char[] bufChars4 = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter5 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter5.bufPos = bufPos + 1;
						bufChars4[bufPos] = 45;
						configuredTaskAwaiter = xmlEncodedRawTextWriter.WriteCommentOrPiAsync(text, 45).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCommentAsync>d__110>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					char[] bufChars5 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter6 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter6.bufPos = bufPos + 1;
					bufChars5[bufPos] = 45;
					char[] bufChars6 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter7 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter7.bufPos = bufPos + 1;
					bufChars6[bufPos] = 45;
					char[] bufChars7 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter8 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter8.bufPos = bufPos + 1;
					bufChars7[bufPos] = 62;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000830 RID: 2096 RVA: 0x000250AC File Offset: 0x000232AC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000467 RID: 1127
			public int <>1__state;

			// Token: 0x04000468 RID: 1128
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000469 RID: 1129
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x0400046A RID: 1130
			public string text;

			// Token: 0x0400046B RID: 1131
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E1 RID: 225
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteProcessingInstructionAsync>d__111 : IAsyncStateMachine
		{
			// Token: 0x06000831 RID: 2097 RVA: 0x000250BC File Offset: 0x000232BC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					int bufPos;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_168;
						}
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						char[] bufChars = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter2 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter2.bufPos = bufPos + 1;
						bufChars[bufPos] = 60;
						char[] bufChars2 = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter3 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter3.bufPos = bufPos + 1;
						bufChars2[bufPos] = 63;
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(name).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteProcessingInstructionAsync>d__111>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					if (text.Length <= 0)
					{
						goto IL_16F;
					}
					char[] bufChars3 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter4 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter4.bufPos = bufPos + 1;
					bufChars3[bufPos] = 32;
					configuredTaskAwaiter = xmlEncodedRawTextWriter.WriteCommentOrPiAsync(text, 63).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteProcessingInstructionAsync>d__111>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_168:
					configuredTaskAwaiter.GetResult();
					IL_16F:
					char[] bufChars4 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter5 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter5.bufPos = bufPos + 1;
					bufChars4[bufPos] = 63;
					char[] bufChars5 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter6 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter6.bufPos = bufPos + 1;
					bufChars5[bufPos] = 62;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000832 RID: 2098 RVA: 0x000252B8 File Offset: 0x000234B8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400046C RID: 1132
			public int <>1__state;

			// Token: 0x0400046D RID: 1133
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400046E RID: 1134
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x0400046F RID: 1135
			public string name;

			// Token: 0x04000470 RID: 1136
			public string text;

			// Token: 0x04000471 RID: 1137
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E2 RID: 226
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteEntityRefAsync>d__112 : IAsyncStateMachine
		{
			// Token: 0x06000833 RID: 2099 RVA: 0x000252C8 File Offset: 0x000234C8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					int bufPos;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_140;
						}
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						char[] bufChars = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter2 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter2.bufPos = bufPos + 1;
						bufChars[bufPos] = 38;
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(name).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteEntityRefAsync>d__112>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					char[] bufChars2 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter3 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter3.bufPos = bufPos + 1;
					bufChars2[bufPos] = 59;
					if (xmlEncodedRawTextWriter.bufPos <= xmlEncodedRawTextWriter.bufLen)
					{
						goto IL_147;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteEntityRefAsync>d__112>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_140:
					configuredTaskAwaiter.GetResult();
					IL_147:
					xmlEncodedRawTextWriter.textPos = xmlEncodedRawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000834 RID: 2100 RVA: 0x00025474 File Offset: 0x00023674
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000472 RID: 1138
			public int <>1__state;

			// Token: 0x04000473 RID: 1139
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000474 RID: 1140
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x04000475 RID: 1141
			public string name;

			// Token: 0x04000476 RID: 1142
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E3 RID: 227
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCharEntityAsync>d__113 : IAsyncStateMachine
		{
			// Token: 0x06000835 RID: 2101 RVA: 0x00025484 File Offset: 0x00023684
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					int num3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_1B6;
						}
						xmlEncodedRawTextWriter.CheckAsyncCall();
						num3 = (int)ch;
						string text = num3.ToString("X", NumberFormatInfo.InvariantInfo);
						if (xmlEncodedRawTextWriter.checkCharacters && !xmlEncodedRawTextWriter.xmlCharType.IsCharData(ch))
						{
							throw XmlConvert.CreateInvalidCharException(ch, '\0');
						}
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						char[] bufChars = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter2 = xmlEncodedRawTextWriter;
						num3 = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter2.bufPos = num3 + 1;
						bufChars[num3] = 38;
						char[] bufChars2 = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter3 = xmlEncodedRawTextWriter;
						num3 = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter3.bufPos = num3 + 1;
						bufChars2[num3] = 35;
						char[] bufChars3 = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter4 = xmlEncodedRawTextWriter;
						num3 = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter4.bufPos = num3 + 1;
						bufChars3[num3] = 120;
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(text).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCharEntityAsync>d__113>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					char[] bufChars4 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter5 = xmlEncodedRawTextWriter;
					num3 = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter5.bufPos = num3 + 1;
					bufChars4[num3] = 59;
					if (xmlEncodedRawTextWriter.bufPos <= xmlEncodedRawTextWriter.bufLen)
					{
						goto IL_1BD;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCharEntityAsync>d__113>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_1B6:
					configuredTaskAwaiter.GetResult();
					IL_1BD:
					xmlEncodedRawTextWriter.textPos = xmlEncodedRawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000836 RID: 2102 RVA: 0x000256A4 File Offset: 0x000238A4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000477 RID: 1143
			public int <>1__state;

			// Token: 0x04000478 RID: 1144
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000479 RID: 1145
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x0400047A RID: 1146
			public char ch;

			// Token: 0x0400047B RID: 1147
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E4 RID: 228
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteSurrogateCharEntityAsync>d__116 : IAsyncStateMachine
		{
			// Token: 0x06000837 RID: 2103 RVA: 0x000256B4 File Offset: 0x000238B4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					int bufPos;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						int num3 = XmlCharType.CombineSurrogateChar((int)lowChar, (int)highChar);
						char[] bufChars = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter2 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter2.bufPos = bufPos + 1;
						bufChars[bufPos] = 38;
						char[] bufChars2 = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter3 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter3.bufPos = bufPos + 1;
						bufChars2[bufPos] = 35;
						char[] bufChars3 = xmlEncodedRawTextWriter.bufChars;
						XmlEncodedRawTextWriter xmlEncodedRawTextWriter4 = xmlEncodedRawTextWriter;
						bufPos = xmlEncodedRawTextWriter.bufPos;
						xmlEncodedRawTextWriter4.bufPos = bufPos + 1;
						bufChars3[bufPos] = 120;
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(num3.ToString("X", NumberFormatInfo.InvariantInfo)).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteSurrogateCharEntityAsync>d__116>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					char[] bufChars4 = xmlEncodedRawTextWriter.bufChars;
					XmlEncodedRawTextWriter xmlEncodedRawTextWriter5 = xmlEncodedRawTextWriter;
					bufPos = xmlEncodedRawTextWriter.bufPos;
					xmlEncodedRawTextWriter5.bufPos = bufPos + 1;
					bufChars4[bufPos] = 59;
					xmlEncodedRawTextWriter.textPos = xmlEncodedRawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000838 RID: 2104 RVA: 0x00025838 File Offset: 0x00023A38
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400047C RID: 1148
			public int <>1__state;

			// Token: 0x0400047D RID: 1149
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400047E RID: 1150
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x0400047F RID: 1151
			public char lowChar;

			// Token: 0x04000480 RID: 1152
			public char highChar;

			// Token: 0x04000481 RID: 1153
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E5 RID: 229
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawAsync>d__118 : IAsyncStateMachine
		{
			// Token: 0x06000839 RID: 2105 RVA: 0x00025848 File Offset: 0x00023A48
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.WriteRawWithCharCheckingAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteRawAsync>d__118>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xmlEncodedRawTextWriter.textPos = xmlEncodedRawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600083A RID: 2106 RVA: 0x00025940 File Offset: 0x00023B40
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000482 RID: 1154
			public int <>1__state;

			// Token: 0x04000483 RID: 1155
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000484 RID: 1156
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x04000485 RID: 1157
			public char[] buffer;

			// Token: 0x04000486 RID: 1158
			public int index;

			// Token: 0x04000487 RID: 1159
			public int count;

			// Token: 0x04000488 RID: 1160
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E6 RID: 230
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawAsync>d__119 : IAsyncStateMachine
		{
			// Token: 0x0600083B RID: 2107 RVA: 0x00025950 File Offset: 0x00023B50
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlEncodedRawTextWriter.CheckAsyncCall();
						if (xmlEncodedRawTextWriter.trackTextContent && xmlEncodedRawTextWriter.inTextContent)
						{
							xmlEncodedRawTextWriter.ChangeTextContentMark(false);
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.WriteRawWithCharCheckingAsync(data).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteRawAsync>d__119>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xmlEncodedRawTextWriter.textPos = xmlEncodedRawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600083C RID: 2108 RVA: 0x00025A3C File Offset: 0x00023C3C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000489 RID: 1161
			public int <>1__state;

			// Token: 0x0400048A RID: 1162
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400048B RID: 1163
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x0400048C RID: 1164
			public string data;

			// Token: 0x0400048D RID: 1165
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E7 RID: 231
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FlushAsync>d__120 : IAsyncStateMachine
		{
			// Token: 0x0600083D RID: 2109 RVA: 0x00025A4C File Offset: 0x00023C4C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_EB;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_15C;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1CC;
					}
					default:
						xmlEncodedRawTextWriter.CheckAsyncCall();
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<FlushAsync>d__120>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushEncoderAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<FlushAsync>d__120>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_EB:
					configuredTaskAwaiter.GetResult();
					if (xmlEncodedRawTextWriter.stream != null)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriter.stream.FlushAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<FlushAsync>d__120>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (xmlEncodedRawTextWriter.writer == null)
						{
							goto IL_1D3;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.writer.FlushAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<FlushAsync>d__120>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_1CC;
					}
					IL_15C:
					configuredTaskAwaiter.GetResult();
					goto IL_1D3;
					IL_1CC:
					configuredTaskAwaiter.GetResult();
					IL_1D3:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600083E RID: 2110 RVA: 0x00025C78 File Offset: 0x00023E78
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400048E RID: 1166
			public int <>1__state;

			// Token: 0x0400048F RID: 1167
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000490 RID: 1168
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x04000491 RID: 1169
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E8 RID: 232
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FlushBufferAsync>d__121 : IAsyncStateMachine
		{
			// Token: 0x0600083F RID: 2111 RVA: 0x00025C88 File Offset: 0x00023E88
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						if (num != 0)
						{
							if (num != 1)
							{
								if (xmlEncodedRawTextWriter.writeToNull)
								{
									goto IL_15E;
								}
								if (xmlEncodedRawTextWriter.stream != null)
								{
									if (xmlEncodedRawTextWriter.trackTextContent)
									{
										xmlEncodedRawTextWriter.charEntityFallback.Reset(xmlEncodedRawTextWriter.textContentMarks, xmlEncodedRawTextWriter.lastMarkPos);
										if ((xmlEncodedRawTextWriter.lastMarkPos & 1) != 0)
										{
											xmlEncodedRawTextWriter.textContentMarks[1] = 1;
											xmlEncodedRawTextWriter.lastMarkPos = 1;
										}
										else
										{
											xmlEncodedRawTextWriter.lastMarkPos = 0;
										}
									}
									configuredTaskAwaiter = xmlEncodedRawTextWriter.EncodeCharsAsync(1, xmlEncodedRawTextWriter.bufPos, true).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num = (num2 = 0);
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<FlushBufferAsync>d__121>(ref configuredTaskAwaiter, ref this);
										return;
									}
									goto IL_DD;
								}
								else
								{
									configuredTaskAwaiter = xmlEncodedRawTextWriter.writer.WriteAsync(xmlEncodedRawTextWriter.bufChars, 1, xmlEncodedRawTextWriter.bufPos - 1).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num = (num2 = 1);
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<FlushBufferAsync>d__121>(ref configuredTaskAwaiter, ref this);
										return;
									}
								}
							}
							else
							{
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num = (num2 = -1);
							}
							configuredTaskAwaiter.GetResult();
							goto IL_15E;
						}
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						IL_DD:
						configuredTaskAwaiter.GetResult();
						IL_15E:;
					}
					catch
					{
						xmlEncodedRawTextWriter.writeToNull = true;
						throw;
					}
					finally
					{
						if (num < 0)
						{
							xmlEncodedRawTextWriter.bufChars[0] = xmlEncodedRawTextWriter.bufChars[xmlEncodedRawTextWriter.bufPos - 1];
							xmlEncodedRawTextWriter.textPos = ((xmlEncodedRawTextWriter.textPos == xmlEncodedRawTextWriter.bufPos) ? 1 : 0);
							xmlEncodedRawTextWriter.attrEndPos = ((xmlEncodedRawTextWriter.attrEndPos == xmlEncodedRawTextWriter.bufPos) ? 1 : 0);
							xmlEncodedRawTextWriter.contentPos = 0;
							xmlEncodedRawTextWriter.cdataPos = 0;
							xmlEncodedRawTextWriter.bufPos = 1;
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000840 RID: 2112 RVA: 0x00025EDC File Offset: 0x000240DC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000492 RID: 1170
			public int <>1__state;

			// Token: 0x04000493 RID: 1171
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000494 RID: 1172
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x04000495 RID: 1173
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000E9 RID: 233
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <EncodeCharsAsync>d__122 : IAsyncStateMachine
		{
			// Token: 0x06000841 RID: 2113 RVA: 0x00025EEC File Offset: 0x000240EC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_124;
					}
					if (num == 1)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C7;
					}
					IL_132:
					while (startOffset < endOffset)
					{
						if (xmlEncodedRawTextWriter.charEntityFallback != null)
						{
							xmlEncodedRawTextWriter.charEntityFallback.StartOffset = startOffset;
						}
						int num3;
						int num4;
						bool flag;
						xmlEncodedRawTextWriter.encoder.Convert(xmlEncodedRawTextWriter.bufChars, startOffset, endOffset - startOffset, xmlEncodedRawTextWriter.bufBytes, xmlEncodedRawTextWriter.bufBytesUsed, xmlEncodedRawTextWriter.bufBytes.Length - xmlEncodedRawTextWriter.bufBytesUsed, false, out num3, out num4, out flag);
						startOffset += num3;
						xmlEncodedRawTextWriter.bufBytesUsed += num4;
						if (xmlEncodedRawTextWriter.bufBytesUsed >= xmlEncodedRawTextWriter.bufBytes.Length - 16)
						{
							configuredTaskAwaiter = xmlEncodedRawTextWriter.stream.WriteAsync(xmlEncodedRawTextWriter.bufBytes, 0, xmlEncodedRawTextWriter.bufBytesUsed).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<EncodeCharsAsync>d__122>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_124;
						}
					}
					if (!writeAllToStream || xmlEncodedRawTextWriter.bufBytesUsed <= 0)
					{
						goto IL_1D5;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.stream.WriteAsync(xmlEncodedRawTextWriter.bufBytes, 0, xmlEncodedRawTextWriter.bufBytesUsed).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<EncodeCharsAsync>d__122>(ref configuredTaskAwaiter, ref this);
						return;
					}
					goto IL_1C7;
					IL_124:
					configuredTaskAwaiter.GetResult();
					xmlEncodedRawTextWriter.bufBytesUsed = 0;
					goto IL_132;
					IL_1C7:
					configuredTaskAwaiter.GetResult();
					xmlEncodedRawTextWriter.bufBytesUsed = 0;
					IL_1D5:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000842 RID: 2114 RVA: 0x00026118 File Offset: 0x00024318
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000496 RID: 1174
			public int <>1__state;

			// Token: 0x04000497 RID: 1175
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000498 RID: 1176
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x04000499 RID: 1177
			public int startOffset;

			// Token: 0x0400049A RID: 1178
			public int endOffset;

			// Token: 0x0400049B RID: 1179
			public bool writeAllToStream;

			// Token: 0x0400049C RID: 1180
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000EA RID: 234
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteAttributeTextBlockAsync>d__127 : IAsyncStateMachine
		{
			// Token: 0x06000843 RID: 2115 RVA: 0x00026128 File Offset: 0x00024328
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_DA;
					}
					writeLen = 0;
					curIndex = index;
					leftCount = count;
					IL_33:
					writeLen = xmlEncodedRawTextWriter.WriteAttributeTextBlockNoFlush(chars, curIndex, leftCount);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (writeLen < 0)
					{
						goto IL_E1;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteAttributeTextBlockAsync>d__127>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_DA:
					configuredTaskAwaiter.GetResult();
					IL_E1:
					if (writeLen >= 0)
					{
						goto IL_33;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000844 RID: 2116 RVA: 0x00026260 File Offset: 0x00024460
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400049D RID: 1181
			public int <>1__state;

			// Token: 0x0400049E RID: 1182
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400049F RID: 1183
			public int index;

			// Token: 0x040004A0 RID: 1184
			public int count;

			// Token: 0x040004A1 RID: 1185
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004A2 RID: 1186
			public char[] chars;

			// Token: 0x040004A3 RID: 1187
			private int <curIndex>5__1;

			// Token: 0x040004A4 RID: 1188
			private int <leftCount>5__2;

			// Token: 0x040004A5 RID: 1189
			private int <writeLen>5__3;

			// Token: 0x040004A6 RID: 1190
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000EB RID: 235
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_WriteAttributeTextBlockAsync>d__129 : IAsyncStateMachine
		{
			// Token: 0x06000845 RID: 2117 RVA: 0x00026270 File Offset: 0x00024470
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_123;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<_WriteAttributeTextBlockAsync>d__129>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_7C:
					writeLen = xmlEncodedRawTextWriter.WriteAttributeTextBlockNoFlush(text, curIndex, leftCount);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (writeLen < 0)
					{
						goto IL_12A;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<_WriteAttributeTextBlockAsync>d__129>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_123:
					configuredTaskAwaiter.GetResult();
					IL_12A:
					if (writeLen >= 0)
					{
						goto IL_7C;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000846 RID: 2118 RVA: 0x00026400 File Offset: 0x00024600
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004A7 RID: 1191
			public int <>1__state;

			// Token: 0x040004A8 RID: 1192
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004A9 RID: 1193
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004AA RID: 1194
			public string text;

			// Token: 0x040004AB RID: 1195
			public int curIndex;

			// Token: 0x040004AC RID: 1196
			public int leftCount;

			// Token: 0x040004AD RID: 1197
			private int <writeLen>5__1;

			// Token: 0x040004AE RID: 1198
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000EC RID: 236
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteElementTextBlockAsync>d__133 : IAsyncStateMachine
		{
			// Token: 0x06000847 RID: 2119 RVA: 0x00026410 File Offset: 0x00024610
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_F9;
					}
					if (num == 1)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_189;
					}
					writeLen = 0;
					curIndex = index;
					leftCount = count;
					needWriteNewLine = false;
					IL_41:
					writeLen = xmlEncodedRawTextWriter.WriteElementTextBlockNoFlush(chars, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(xmlEncodedRawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteElementTextBlockAsync>d__133>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_190;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteElementTextBlockAsync>d__133>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_189;
					}
					IL_F9:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_190;
					IL_189:
					configuredTaskAwaiter.GetResult();
					IL_190:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_41;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000848 RID: 2120 RVA: 0x00026610 File Offset: 0x00024810
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004AF RID: 1199
			public int <>1__state;

			// Token: 0x040004B0 RID: 1200
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004B1 RID: 1201
			public int index;

			// Token: 0x040004B2 RID: 1202
			public int count;

			// Token: 0x040004B3 RID: 1203
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004B4 RID: 1204
			public char[] chars;

			// Token: 0x040004B5 RID: 1205
			private int <curIndex>5__1;

			// Token: 0x040004B6 RID: 1206
			private int <leftCount>5__2;

			// Token: 0x040004B7 RID: 1207
			private int <writeLen>5__3;

			// Token: 0x040004B8 RID: 1208
			private bool <needWriteNewLine>5__4;

			// Token: 0x040004B9 RID: 1209
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000ED RID: 237
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_WriteElementTextBlockAsync>d__135 : IAsyncStateMachine
		{
			// Token: 0x06000849 RID: 2121 RVA: 0x00026620 File Offset: 0x00024820
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_12A;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1E9;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_279;
					}
					default:
						writeLen = 0;
						needWriteNewLine = false;
						if (newLine)
						{
							configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(xmlEncodedRawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<_WriteElementTextBlockAsync>d__135>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<_WriteElementTextBlockAsync>d__135>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_12A;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_131;
					IL_12A:
					configuredTaskAwaiter.GetResult();
					IL_131:
					writeLen = xmlEncodedRawTextWriter.WriteElementTextBlockNoFlush(text, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(xmlEncodedRawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<_WriteElementTextBlockAsync>d__135>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_280;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<_WriteElementTextBlockAsync>d__135>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_279;
					}
					IL_1E9:
					configuredTaskAwaiter.GetResult();
					num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_280;
					IL_279:
					configuredTaskAwaiter.GetResult();
					IL_280:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_131;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600084A RID: 2122 RVA: 0x00026910 File Offset: 0x00024B10
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004BA RID: 1210
			public int <>1__state;

			// Token: 0x040004BB RID: 1211
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004BC RID: 1212
			public bool newLine;

			// Token: 0x040004BD RID: 1213
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004BE RID: 1214
			public int curIndex;

			// Token: 0x040004BF RID: 1215
			public int leftCount;

			// Token: 0x040004C0 RID: 1216
			public string text;

			// Token: 0x040004C1 RID: 1217
			private int <writeLen>5__1;

			// Token: 0x040004C2 RID: 1218
			private bool <needWriteNewLine>5__2;

			// Token: 0x040004C3 RID: 1219
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000EE RID: 238
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_RawTextAsync>d__139 : IAsyncStateMachine
		{
			// Token: 0x0600084B RID: 2123 RVA: 0x00026920 File Offset: 0x00024B20
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_12A;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<_RawTextAsync>d__139>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					writeLen = 0;
					IL_83:
					writeLen = xmlEncodedRawTextWriter.RawTextNoFlush(text, curIndex, leftCount);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (writeLen < 0)
					{
						goto IL_131;
					}
					configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<_RawTextAsync>d__139>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_12A:
					configuredTaskAwaiter.GetResult();
					IL_131:
					if (writeLen >= 0)
					{
						goto IL_83;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600084C RID: 2124 RVA: 0x00026AB4 File Offset: 0x00024CB4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004C4 RID: 1220
			public int <>1__state;

			// Token: 0x040004C5 RID: 1221
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004C6 RID: 1222
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004C7 RID: 1223
			public string text;

			// Token: 0x040004C8 RID: 1224
			public int curIndex;

			// Token: 0x040004C9 RID: 1225
			public int leftCount;

			// Token: 0x040004CA RID: 1226
			private int <writeLen>5__1;

			// Token: 0x040004CB RID: 1227
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000EF RID: 239
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawWithCharCheckingAsync>d__143 : IAsyncStateMachine
		{
			// Token: 0x0600084D RID: 2125 RVA: 0x00026AC4 File Offset: 0x00024CC4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_F9;
					}
					if (num == 1)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_189;
					}
					writeLen = 0;
					curIndex = index;
					leftCount = count;
					needWriteNewLine = false;
					IL_41:
					writeLen = xmlEncodedRawTextWriter.WriteRawWithCharCheckingNoFlush(chars, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(xmlEncodedRawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteRawWithCharCheckingAsync>d__143>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_190;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteRawWithCharCheckingAsync>d__143>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_189;
					}
					IL_F9:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_190;
					IL_189:
					configuredTaskAwaiter.GetResult();
					IL_190:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_41;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600084E RID: 2126 RVA: 0x00026CC4 File Offset: 0x00024EC4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004CC RID: 1228
			public int <>1__state;

			// Token: 0x040004CD RID: 1229
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004CE RID: 1230
			public int index;

			// Token: 0x040004CF RID: 1231
			public int count;

			// Token: 0x040004D0 RID: 1232
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004D1 RID: 1233
			public char[] chars;

			// Token: 0x040004D2 RID: 1234
			private int <curIndex>5__1;

			// Token: 0x040004D3 RID: 1235
			private int <leftCount>5__2;

			// Token: 0x040004D4 RID: 1236
			private int <writeLen>5__3;

			// Token: 0x040004D5 RID: 1237
			private bool <needWriteNewLine>5__4;

			// Token: 0x040004D6 RID: 1238
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000F0 RID: 240
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawWithCharCheckingAsync>d__144 : IAsyncStateMachine
		{
			// Token: 0x0600084F RID: 2127 RVA: 0x00026CD4 File Offset: 0x00024ED4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_F9;
					}
					if (num == 1)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_189;
					}
					writeLen = 0;
					curIndex = 0;
					leftCount = text.Length;
					needWriteNewLine = false;
					IL_41:
					writeLen = xmlEncodedRawTextWriter.WriteRawWithCharCheckingNoFlush(text, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(xmlEncodedRawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteRawWithCharCheckingAsync>d__144>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_190;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteRawWithCharCheckingAsync>d__144>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_189;
					}
					IL_F9:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_190;
					IL_189:
					configuredTaskAwaiter.GetResult();
					IL_190:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_41;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000850 RID: 2128 RVA: 0x00026ED4 File Offset: 0x000250D4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004D7 RID: 1239
			public int <>1__state;

			// Token: 0x040004D8 RID: 1240
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004D9 RID: 1241
			public string text;

			// Token: 0x040004DA RID: 1242
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004DB RID: 1243
			private int <curIndex>5__1;

			// Token: 0x040004DC RID: 1244
			private int <leftCount>5__2;

			// Token: 0x040004DD RID: 1245
			private int <writeLen>5__3;

			// Token: 0x040004DE RID: 1246
			private bool <needWriteNewLine>5__4;

			// Token: 0x040004DF RID: 1247
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000F1 RID: 241
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCommentOrPiAsync>d__146 : IAsyncStateMachine
		{
			// Token: 0x06000851 RID: 2129 RVA: 0x00026EE4 File Offset: 0x000250E4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_188;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_218;
					}
					default:
						if (text.Length != 0)
						{
							writeLen = 0;
							curIndex = 0;
							leftCount = text.Length;
							needWriteNewLine = false;
							goto IL_CA;
						}
						if (xmlEncodedRawTextWriter.bufPos < xmlEncodedRawTextWriter.bufLen)
						{
							goto IL_9F;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCommentOrPiAsync>d__146>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					IL_9F:
					goto IL_252;
					IL_CA:
					writeLen = xmlEncodedRawTextWriter.WriteCommentOrPiNoFlush(text, curIndex, leftCount, stopChar, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(xmlEncodedRawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCommentOrPiAsync>d__146>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_21F;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCommentOrPiAsync>d__146>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_218;
					}
					IL_188:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_21F;
					IL_218:
					configuredTaskAwaiter.GetResult();
					IL_21F:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_CA;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_252:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000852 RID: 2130 RVA: 0x00027174 File Offset: 0x00025374
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004E0 RID: 1248
			public int <>1__state;

			// Token: 0x040004E1 RID: 1249
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004E2 RID: 1250
			public string text;

			// Token: 0x040004E3 RID: 1251
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004E4 RID: 1252
			private int <curIndex>5__1;

			// Token: 0x040004E5 RID: 1253
			private int <leftCount>5__2;

			// Token: 0x040004E6 RID: 1254
			public int stopChar;

			// Token: 0x040004E7 RID: 1255
			private int <writeLen>5__3;

			// Token: 0x040004E8 RID: 1256
			private bool <needWriteNewLine>5__4;

			// Token: 0x040004E9 RID: 1257
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000F2 RID: 242
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCDataSectionAsync>d__148 : IAsyncStateMachine
		{
			// Token: 0x06000853 RID: 2131 RVA: 0x00027184 File Offset: 0x00025384
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriter xmlEncodedRawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_182;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_212;
					}
					default:
						if (text.Length != 0)
						{
							writeLen = 0;
							curIndex = 0;
							leftCount = text.Length;
							needWriteNewLine = false;
							goto IL_CA;
						}
						if (xmlEncodedRawTextWriter.bufPos < xmlEncodedRawTextWriter.bufLen)
						{
							goto IL_9F;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCDataSectionAsync>d__148>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					IL_9F:
					goto IL_24C;
					IL_CA:
					writeLen = xmlEncodedRawTextWriter.WriteCDataSectionNoFlush(text, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriter.RawTextAsync(xmlEncodedRawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCDataSectionAsync>d__148>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_219;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriter.<WriteCDataSectionAsync>d__148>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_212;
					}
					IL_182:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_219;
					IL_212:
					configuredTaskAwaiter.GetResult();
					IL_219:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_CA;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_24C:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000854 RID: 2132 RVA: 0x0002740C File Offset: 0x0002560C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004EA RID: 1258
			public int <>1__state;

			// Token: 0x040004EB RID: 1259
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004EC RID: 1260
			public string text;

			// Token: 0x040004ED RID: 1261
			public XmlEncodedRawTextWriter <>4__this;

			// Token: 0x040004EE RID: 1262
			private int <curIndex>5__1;

			// Token: 0x040004EF RID: 1263
			private int <leftCount>5__2;

			// Token: 0x040004F0 RID: 1264
			private int <writeLen>5__3;

			// Token: 0x040004F1 RID: 1265
			private bool <needWriteNewLine>5__4;

			// Token: 0x040004F2 RID: 1266
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
