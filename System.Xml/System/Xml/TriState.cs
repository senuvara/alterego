﻿using System;

namespace System.Xml
{
	// Token: 0x020001EA RID: 490
	internal enum TriState
	{
		// Token: 0x04000C31 RID: 3121
		Unknown = -1,
		// Token: 0x04000C32 RID: 3122
		False,
		// Token: 0x04000C33 RID: 3123
		True
	}
}
