﻿using System;
using Unity;

namespace System.Xml
{
	/// <summary>Represents an entity declaration, such as &lt;!ENTITY... &gt;.</summary>
	// Token: 0x0200022C RID: 556
	public class XmlEntity : XmlNode
	{
		// Token: 0x060014C2 RID: 5314 RVA: 0x00075D14 File Offset: 0x00073F14
		internal XmlEntity(string name, string strdata, string publicId, string systemId, string notationName, XmlDocument doc) : base(doc)
		{
			this.name = doc.NameTable.Add(name);
			this.publicId = publicId;
			this.systemId = systemId;
			this.notationName = notationName;
			this.unparsedReplacementStr = strdata;
			this.childrenFoliating = false;
		}

		/// <summary>Creates a duplicate of this node. Entity nodes cannot be cloned. Calling this method on an <see cref="T:System.Xml.XmlEntity" /> object throws an exception.</summary>
		/// <param name="deep">
		///       <see langword="true" /> to recursively clone the subtree under the specified node; <see langword="false" /> to clone only the node itself.</param>
		/// <returns>Returns a copy of the <see cref="T:System.Xml.XmlNode" /> from which the method is called.</returns>
		/// <exception cref="T:System.InvalidOperationException">Entity nodes cannot be cloned. Calling this method on an <see cref="T:System.Xml.XmlEntity" /> object throws an exception.</exception>
		// Token: 0x060014C3 RID: 5315 RVA: 0x00075D61 File Offset: 0x00073F61
		public override XmlNode CloneNode(bool deep)
		{
			throw new InvalidOperationException(Res.GetString("'Entity' and 'Notation' nodes cannot be cloned."));
		}

		/// <summary>Gets a value indicating whether the node is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> if the node is read-only; otherwise <see langword="false" />.Because <see langword="XmlEntity" /> nodes are read-only, this property always returns <see langword="true" />.</returns>
		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x060014C4 RID: 5316 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets the name of the node.</summary>
		/// <returns>The name of the entity.</returns>
		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x060014C5 RID: 5317 RVA: 0x00075D72 File Offset: 0x00073F72
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Gets the name of the node without the namespace prefix.</summary>
		/// <returns>For <see langword="XmlEntity" /> nodes, this property returns the name of the entity.</returns>
		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x060014C6 RID: 5318 RVA: 0x00075D72 File Offset: 0x00073F72
		public override string LocalName
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Gets the concatenated values of the entity node and all its children.</summary>
		/// <returns>The concatenated values of the node and all its children.</returns>
		/// <exception cref="T:System.InvalidOperationException">Attempting to set the property. </exception>
		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x060014C7 RID: 5319 RVA: 0x000755A6 File Offset: 0x000737A6
		// (set) Token: 0x060014C8 RID: 5320 RVA: 0x00075D7A File Offset: 0x00073F7A
		public override string InnerText
		{
			get
			{
				return base.InnerText;
			}
			set
			{
				throw new InvalidOperationException(Res.GetString("The 'InnerText' of an 'Entity' node is read-only and cannot be set."));
			}
		}

		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x060014C9 RID: 5321 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool IsContainer
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x060014CA RID: 5322 RVA: 0x00075D8B File Offset: 0x00073F8B
		// (set) Token: 0x060014CB RID: 5323 RVA: 0x00075DB5 File Offset: 0x00073FB5
		internal override XmlLinkedNode LastNode
		{
			get
			{
				if (this.lastChild == null && !this.childrenFoliating)
				{
					this.childrenFoliating = true;
					new XmlLoader().ExpandEntity(this);
				}
				return this.lastChild;
			}
			set
			{
				this.lastChild = value;
			}
		}

		// Token: 0x060014CC RID: 5324 RVA: 0x00075DBE File Offset: 0x00073FBE
		internal override bool IsValidChildType(XmlNodeType type)
		{
			return type == XmlNodeType.Text || type == XmlNodeType.Element || type == XmlNodeType.ProcessingInstruction || type == XmlNodeType.Comment || type == XmlNodeType.CDATA || type == XmlNodeType.Whitespace || type == XmlNodeType.SignificantWhitespace || type == XmlNodeType.EntityReference;
		}

		/// <summary>Gets the type of the node.</summary>
		/// <returns>The node type. For <see langword="XmlEntity" /> nodes, the value is XmlNodeType.Entity.</returns>
		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x060014CD RID: 5325 RVA: 0x00006CB1 File Offset: 0x00004EB1
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.Entity;
			}
		}

		/// <summary>Gets the value of the public identifier on the entity declaration.</summary>
		/// <returns>The public identifier on the entity. If there is no public identifier, <see langword="null" /> is returned.</returns>
		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x060014CE RID: 5326 RVA: 0x00075DE4 File Offset: 0x00073FE4
		public string PublicId
		{
			get
			{
				return this.publicId;
			}
		}

		/// <summary>Gets the value of the system identifier on the entity declaration.</summary>
		/// <returns>The system identifier on the entity. If there is no system identifier, <see langword="null" /> is returned.</returns>
		// Token: 0x170003DA RID: 986
		// (get) Token: 0x060014CF RID: 5327 RVA: 0x00075DEC File Offset: 0x00073FEC
		public string SystemId
		{
			get
			{
				return this.systemId;
			}
		}

		/// <summary>Gets the name of the optional NDATA attribute on the entity declaration.</summary>
		/// <returns>The name of the NDATA attribute. If there is no NDATA, <see langword="null" /> is returned.</returns>
		// Token: 0x170003DB RID: 987
		// (get) Token: 0x060014D0 RID: 5328 RVA: 0x00075DF4 File Offset: 0x00073FF4
		public string NotationName
		{
			get
			{
				return this.notationName;
			}
		}

		/// <summary>Gets the markup representing this node and all its children.</summary>
		/// <returns>For <see langword="XmlEntity" /> nodes, String.Empty is returned.</returns>
		// Token: 0x170003DC RID: 988
		// (get) Token: 0x060014D1 RID: 5329 RVA: 0x00003201 File Offset: 0x00001401
		public override string OuterXml
		{
			get
			{
				return string.Empty;
			}
		}

		/// <summary>Gets the markup representing the children of this node.</summary>
		/// <returns>For <see langword="XmlEntity" /> nodes, String.Empty is returned.</returns>
		/// <exception cref="T:System.InvalidOperationException">Attempting to set the property. </exception>
		// Token: 0x170003DD RID: 989
		// (get) Token: 0x060014D2 RID: 5330 RVA: 0x00003201 File Offset: 0x00001401
		// (set) Token: 0x060014D3 RID: 5331 RVA: 0x00075DFC File Offset: 0x00073FFC
		public override string InnerXml
		{
			get
			{
				return string.Empty;
			}
			set
			{
				throw new InvalidOperationException(Res.GetString("Cannot set the 'InnerXml' for the current node because it is either read-only or cannot have children."));
			}
		}

		/// <summary>Saves the node to the specified <see cref="T:System.Xml.XmlWriter" />. For <see langword="XmlEntity" /> nodes, this method has no effect.</summary>
		/// <param name="w">The <see langword="XmlWriter" /> to which you want to save. </param>
		// Token: 0x060014D4 RID: 5332 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteTo(XmlWriter w)
		{
		}

		/// <summary>Saves all the children of the node to the specified <see cref="T:System.Xml.XmlWriter" />. For <see langword="XmlEntity" /> nodes, this method has no effect.</summary>
		/// <param name="w">The <see langword="XmlWriter" /> to which you want to save. </param>
		// Token: 0x060014D5 RID: 5333 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteContentTo(XmlWriter w)
		{
		}

		/// <summary>Gets the base Uniform Resource Identifier (URI) of the current node.</summary>
		/// <returns>The location from which the node was loaded.</returns>
		// Token: 0x170003DE RID: 990
		// (get) Token: 0x060014D6 RID: 5334 RVA: 0x00075E0D File Offset: 0x0007400D
		public override string BaseURI
		{
			get
			{
				return this.baseURI;
			}
		}

		// Token: 0x060014D7 RID: 5335 RVA: 0x00075E15 File Offset: 0x00074015
		internal void SetBaseURI(string inBaseURI)
		{
			this.baseURI = inBaseURI;
		}

		// Token: 0x060014D8 RID: 5336 RVA: 0x00072668 File Offset: 0x00070868
		internal XmlEntity()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000DD2 RID: 3538
		private string publicId;

		// Token: 0x04000DD3 RID: 3539
		private string systemId;

		// Token: 0x04000DD4 RID: 3540
		private string notationName;

		// Token: 0x04000DD5 RID: 3541
		private string name;

		// Token: 0x04000DD6 RID: 3542
		private string unparsedReplacementStr;

		// Token: 0x04000DD7 RID: 3543
		private string baseURI;

		// Token: 0x04000DD8 RID: 3544
		private XmlLinkedNode lastChild;

		// Token: 0x04000DD9 RID: 3545
		private bool childrenFoliating;
	}
}
