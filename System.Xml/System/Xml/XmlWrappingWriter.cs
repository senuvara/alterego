﻿using System;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020001DE RID: 478
	internal class XmlWrappingWriter : XmlWriter
	{
		// Token: 0x06001072 RID: 4210 RVA: 0x0006309E File Offset: 0x0006129E
		internal XmlWrappingWriter(XmlWriter baseWriter)
		{
			this.writer = baseWriter;
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06001073 RID: 4211 RVA: 0x000630AD File Offset: 0x000612AD
		public override XmlWriterSettings Settings
		{
			get
			{
				return this.writer.Settings;
			}
		}

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06001074 RID: 4212 RVA: 0x000630BA File Offset: 0x000612BA
		public override WriteState WriteState
		{
			get
			{
				return this.writer.WriteState;
			}
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06001075 RID: 4213 RVA: 0x000630C7 File Offset: 0x000612C7
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.writer.XmlSpace;
			}
		}

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06001076 RID: 4214 RVA: 0x000630D4 File Offset: 0x000612D4
		public override string XmlLang
		{
			get
			{
				return this.writer.XmlLang;
			}
		}

		// Token: 0x06001077 RID: 4215 RVA: 0x000630E1 File Offset: 0x000612E1
		public override void WriteStartDocument()
		{
			this.writer.WriteStartDocument();
		}

		// Token: 0x06001078 RID: 4216 RVA: 0x000630EE File Offset: 0x000612EE
		public override void WriteStartDocument(bool standalone)
		{
			this.writer.WriteStartDocument(standalone);
		}

		// Token: 0x06001079 RID: 4217 RVA: 0x000630FC File Offset: 0x000612FC
		public override void WriteEndDocument()
		{
			this.writer.WriteEndDocument();
		}

		// Token: 0x0600107A RID: 4218 RVA: 0x00063109 File Offset: 0x00061309
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			this.writer.WriteDocType(name, pubid, sysid, subset);
		}

		// Token: 0x0600107B RID: 4219 RVA: 0x0006311B File Offset: 0x0006131B
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			this.writer.WriteStartElement(prefix, localName, ns);
		}

		// Token: 0x0600107C RID: 4220 RVA: 0x0006312B File Offset: 0x0006132B
		public override void WriteEndElement()
		{
			this.writer.WriteEndElement();
		}

		// Token: 0x0600107D RID: 4221 RVA: 0x00063138 File Offset: 0x00061338
		public override void WriteFullEndElement()
		{
			this.writer.WriteFullEndElement();
		}

		// Token: 0x0600107E RID: 4222 RVA: 0x00063145 File Offset: 0x00061345
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			this.writer.WriteStartAttribute(prefix, localName, ns);
		}

		// Token: 0x0600107F RID: 4223 RVA: 0x00063155 File Offset: 0x00061355
		public override void WriteEndAttribute()
		{
			this.writer.WriteEndAttribute();
		}

		// Token: 0x06001080 RID: 4224 RVA: 0x00063162 File Offset: 0x00061362
		public override void WriteCData(string text)
		{
			this.writer.WriteCData(text);
		}

		// Token: 0x06001081 RID: 4225 RVA: 0x00063170 File Offset: 0x00061370
		public override void WriteComment(string text)
		{
			this.writer.WriteComment(text);
		}

		// Token: 0x06001082 RID: 4226 RVA: 0x0006317E File Offset: 0x0006137E
		public override void WriteProcessingInstruction(string name, string text)
		{
			this.writer.WriteProcessingInstruction(name, text);
		}

		// Token: 0x06001083 RID: 4227 RVA: 0x0006318D File Offset: 0x0006138D
		public override void WriteEntityRef(string name)
		{
			this.writer.WriteEntityRef(name);
		}

		// Token: 0x06001084 RID: 4228 RVA: 0x0006319B File Offset: 0x0006139B
		public override void WriteCharEntity(char ch)
		{
			this.writer.WriteCharEntity(ch);
		}

		// Token: 0x06001085 RID: 4229 RVA: 0x000631A9 File Offset: 0x000613A9
		public override void WriteWhitespace(string ws)
		{
			this.writer.WriteWhitespace(ws);
		}

		// Token: 0x06001086 RID: 4230 RVA: 0x000631B7 File Offset: 0x000613B7
		public override void WriteString(string text)
		{
			this.writer.WriteString(text);
		}

		// Token: 0x06001087 RID: 4231 RVA: 0x0001F302 File Offset: 0x0001D502
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			this.writer.WriteSurrogateCharEntity(lowChar, highChar);
		}

		// Token: 0x06001088 RID: 4232 RVA: 0x000631C5 File Offset: 0x000613C5
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.writer.WriteChars(buffer, index, count);
		}

		// Token: 0x06001089 RID: 4233 RVA: 0x000631D5 File Offset: 0x000613D5
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.writer.WriteRaw(buffer, index, count);
		}

		// Token: 0x0600108A RID: 4234 RVA: 0x000631E5 File Offset: 0x000613E5
		public override void WriteRaw(string data)
		{
			this.writer.WriteRaw(data);
		}

		// Token: 0x0600108B RID: 4235 RVA: 0x000631F3 File Offset: 0x000613F3
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			this.writer.WriteBase64(buffer, index, count);
		}

		// Token: 0x0600108C RID: 4236 RVA: 0x00063203 File Offset: 0x00061403
		public override void Close()
		{
			this.writer.Close();
		}

		// Token: 0x0600108D RID: 4237 RVA: 0x00063210 File Offset: 0x00061410
		public override void Flush()
		{
			this.writer.Flush();
		}

		// Token: 0x0600108E RID: 4238 RVA: 0x0006321D File Offset: 0x0006141D
		public override string LookupPrefix(string ns)
		{
			return this.writer.LookupPrefix(ns);
		}

		// Token: 0x0600108F RID: 4239 RVA: 0x0006322B File Offset: 0x0006142B
		public override void WriteValue(object value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001090 RID: 4240 RVA: 0x00063239 File Offset: 0x00061439
		public override void WriteValue(string value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001091 RID: 4241 RVA: 0x00063247 File Offset: 0x00061447
		public override void WriteValue(bool value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001092 RID: 4242 RVA: 0x00063255 File Offset: 0x00061455
		public override void WriteValue(DateTime value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001093 RID: 4243 RVA: 0x00063263 File Offset: 0x00061463
		public override void WriteValue(DateTimeOffset value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001094 RID: 4244 RVA: 0x00063271 File Offset: 0x00061471
		public override void WriteValue(double value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001095 RID: 4245 RVA: 0x0006327F File Offset: 0x0006147F
		public override void WriteValue(float value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001096 RID: 4246 RVA: 0x0006328D File Offset: 0x0006148D
		public override void WriteValue(decimal value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001097 RID: 4247 RVA: 0x0006329B File Offset: 0x0006149B
		public override void WriteValue(int value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001098 RID: 4248 RVA: 0x000632A9 File Offset: 0x000614A9
		public override void WriteValue(long value)
		{
			this.writer.WriteValue(value);
		}

		// Token: 0x06001099 RID: 4249 RVA: 0x000632B7 File Offset: 0x000614B7
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				((IDisposable)this.writer).Dispose();
			}
		}

		// Token: 0x0600109A RID: 4250 RVA: 0x000632C7 File Offset: 0x000614C7
		public override Task WriteStartDocumentAsync()
		{
			return this.writer.WriteStartDocumentAsync();
		}

		// Token: 0x0600109B RID: 4251 RVA: 0x000632D4 File Offset: 0x000614D4
		public override Task WriteStartDocumentAsync(bool standalone)
		{
			return this.writer.WriteStartDocumentAsync(standalone);
		}

		// Token: 0x0600109C RID: 4252 RVA: 0x000632E2 File Offset: 0x000614E2
		public override Task WriteEndDocumentAsync()
		{
			return this.writer.WriteEndDocumentAsync();
		}

		// Token: 0x0600109D RID: 4253 RVA: 0x000632EF File Offset: 0x000614EF
		public override Task WriteDocTypeAsync(string name, string pubid, string sysid, string subset)
		{
			return this.writer.WriteDocTypeAsync(name, pubid, sysid, subset);
		}

		// Token: 0x0600109E RID: 4254 RVA: 0x00063301 File Offset: 0x00061501
		public override Task WriteStartElementAsync(string prefix, string localName, string ns)
		{
			return this.writer.WriteStartElementAsync(prefix, localName, ns);
		}

		// Token: 0x0600109F RID: 4255 RVA: 0x00063311 File Offset: 0x00061511
		public override Task WriteEndElementAsync()
		{
			return this.writer.WriteEndElementAsync();
		}

		// Token: 0x060010A0 RID: 4256 RVA: 0x0006331E File Offset: 0x0006151E
		public override Task WriteFullEndElementAsync()
		{
			return this.writer.WriteFullEndElementAsync();
		}

		// Token: 0x060010A1 RID: 4257 RVA: 0x0006332B File Offset: 0x0006152B
		protected internal override Task WriteStartAttributeAsync(string prefix, string localName, string ns)
		{
			return this.writer.WriteStartAttributeAsync(prefix, localName, ns);
		}

		// Token: 0x060010A2 RID: 4258 RVA: 0x0006333B File Offset: 0x0006153B
		protected internal override Task WriteEndAttributeAsync()
		{
			return this.writer.WriteEndAttributeAsync();
		}

		// Token: 0x060010A3 RID: 4259 RVA: 0x00063348 File Offset: 0x00061548
		public override Task WriteCDataAsync(string text)
		{
			return this.writer.WriteCDataAsync(text);
		}

		// Token: 0x060010A4 RID: 4260 RVA: 0x00063356 File Offset: 0x00061556
		public override Task WriteCommentAsync(string text)
		{
			return this.writer.WriteCommentAsync(text);
		}

		// Token: 0x060010A5 RID: 4261 RVA: 0x00063364 File Offset: 0x00061564
		public override Task WriteProcessingInstructionAsync(string name, string text)
		{
			return this.writer.WriteProcessingInstructionAsync(name, text);
		}

		// Token: 0x060010A6 RID: 4262 RVA: 0x00063373 File Offset: 0x00061573
		public override Task WriteEntityRefAsync(string name)
		{
			return this.writer.WriteEntityRefAsync(name);
		}

		// Token: 0x060010A7 RID: 4263 RVA: 0x00063381 File Offset: 0x00061581
		public override Task WriteCharEntityAsync(char ch)
		{
			return this.writer.WriteCharEntityAsync(ch);
		}

		// Token: 0x060010A8 RID: 4264 RVA: 0x0006338F File Offset: 0x0006158F
		public override Task WriteWhitespaceAsync(string ws)
		{
			return this.writer.WriteWhitespaceAsync(ws);
		}

		// Token: 0x060010A9 RID: 4265 RVA: 0x0006339D File Offset: 0x0006159D
		public override Task WriteStringAsync(string text)
		{
			return this.writer.WriteStringAsync(text);
		}

		// Token: 0x060010AA RID: 4266 RVA: 0x0001FAB6 File Offset: 0x0001DCB6
		public override Task WriteSurrogateCharEntityAsync(char lowChar, char highChar)
		{
			return this.writer.WriteSurrogateCharEntityAsync(lowChar, highChar);
		}

		// Token: 0x060010AB RID: 4267 RVA: 0x000633AB File Offset: 0x000615AB
		public override Task WriteCharsAsync(char[] buffer, int index, int count)
		{
			return this.writer.WriteCharsAsync(buffer, index, count);
		}

		// Token: 0x060010AC RID: 4268 RVA: 0x000633BB File Offset: 0x000615BB
		public override Task WriteRawAsync(char[] buffer, int index, int count)
		{
			return this.writer.WriteRawAsync(buffer, index, count);
		}

		// Token: 0x060010AD RID: 4269 RVA: 0x000633CB File Offset: 0x000615CB
		public override Task WriteRawAsync(string data)
		{
			return this.writer.WriteRawAsync(data);
		}

		// Token: 0x060010AE RID: 4270 RVA: 0x000633D9 File Offset: 0x000615D9
		public override Task WriteBase64Async(byte[] buffer, int index, int count)
		{
			return this.writer.WriteBase64Async(buffer, index, count);
		}

		// Token: 0x060010AF RID: 4271 RVA: 0x000633E9 File Offset: 0x000615E9
		public override Task FlushAsync()
		{
			return this.writer.FlushAsync();
		}

		// Token: 0x04000BE2 RID: 3042
		protected XmlWriter writer;
	}
}
