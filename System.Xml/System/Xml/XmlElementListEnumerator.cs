﻿using System;
using System.Collections;

namespace System.Xml
{
	// Token: 0x02000229 RID: 553
	internal class XmlElementListEnumerator : IEnumerator
	{
		// Token: 0x060014B7 RID: 5303 RVA: 0x00075B42 File Offset: 0x00073D42
		public XmlElementListEnumerator(XmlElementList list)
		{
			this.list = list;
			this.curElem = null;
			this.changeCount = list.ChangeCount;
		}

		// Token: 0x060014B8 RID: 5304 RVA: 0x00075B64 File Offset: 0x00073D64
		public bool MoveNext()
		{
			if (this.list.ChangeCount != this.changeCount)
			{
				throw new InvalidOperationException(Res.GetString("The element list has changed. The enumeration operation failed to continue."));
			}
			this.curElem = this.list.GetNextNode(this.curElem);
			return this.curElem != null;
		}

		// Token: 0x060014B9 RID: 5305 RVA: 0x00075BB4 File Offset: 0x00073DB4
		public void Reset()
		{
			this.curElem = null;
			this.changeCount = this.list.ChangeCount;
		}

		// Token: 0x170003D0 RID: 976
		// (get) Token: 0x060014BA RID: 5306 RVA: 0x00075BCE File Offset: 0x00073DCE
		public object Current
		{
			get
			{
				return this.curElem;
			}
		}

		// Token: 0x04000DCC RID: 3532
		private XmlElementList list;

		// Token: 0x04000DCD RID: 3533
		private XmlNode curElem;

		// Token: 0x04000DCE RID: 3534
		private int changeCount;
	}
}
