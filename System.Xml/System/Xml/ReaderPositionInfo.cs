﻿using System;

namespace System.Xml
{
	// Token: 0x0200024B RID: 587
	internal class ReaderPositionInfo : PositionInfo
	{
		// Token: 0x0600168A RID: 5770 RVA: 0x0007C037 File Offset: 0x0007A237
		public ReaderPositionInfo(IXmlLineInfo lineInfo)
		{
			this.lineInfo = lineInfo;
		}

		// Token: 0x0600168B RID: 5771 RVA: 0x0007C046 File Offset: 0x0007A246
		public override bool HasLineInfo()
		{
			return this.lineInfo.HasLineInfo();
		}

		// Token: 0x17000488 RID: 1160
		// (get) Token: 0x0600168C RID: 5772 RVA: 0x0007C053 File Offset: 0x0007A253
		public override int LineNumber
		{
			get
			{
				return this.lineInfo.LineNumber;
			}
		}

		// Token: 0x17000489 RID: 1161
		// (get) Token: 0x0600168D RID: 5773 RVA: 0x0007C060 File Offset: 0x0007A260
		public override int LinePosition
		{
			get
			{
				return this.lineInfo.LinePosition;
			}
		}

		// Token: 0x04000E2C RID: 3628
		private IXmlLineInfo lineInfo;
	}
}
