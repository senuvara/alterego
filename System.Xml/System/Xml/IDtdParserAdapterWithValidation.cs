﻿using System;

namespace System.Xml
{
	// Token: 0x020000A4 RID: 164
	internal interface IDtdParserAdapterWithValidation : IDtdParserAdapter
	{
		// Token: 0x17000121 RID: 289
		// (get) Token: 0x0600056B RID: 1387
		bool DtdValidation { get; }

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x0600056C RID: 1388
		IValidationEventHandling ValidationEventHandling { get; }
	}
}
