﻿using System;
using System.Text;

namespace System.Xml
{
	// Token: 0x02000298 RID: 664
	internal class Ucs4Encoding : Encoding
	{
		// Token: 0x1700049B RID: 1179
		// (get) Token: 0x0600188A RID: 6282 RVA: 0x0008E7B2 File Offset: 0x0008C9B2
		public override string WebName
		{
			get
			{
				return this.EncodingName;
			}
		}

		// Token: 0x0600188B RID: 6283 RVA: 0x0008E7BA File Offset: 0x0008C9BA
		public override Decoder GetDecoder()
		{
			return this.ucs4Decoder;
		}

		// Token: 0x0600188C RID: 6284 RVA: 0x0008E7C2 File Offset: 0x0008C9C2
		public override int GetByteCount(char[] chars, int index, int count)
		{
			return checked(count * 4);
		}

		// Token: 0x0600188D RID: 6285 RVA: 0x0008E7C7 File Offset: 0x0008C9C7
		public override int GetByteCount(char[] chars)
		{
			return chars.Length * 4;
		}

		// Token: 0x0600188E RID: 6286 RVA: 0x000037FB File Offset: 0x000019FB
		public override byte[] GetBytes(string s)
		{
			return null;
		}

		// Token: 0x0600188F RID: 6287 RVA: 0x000020CD File Offset: 0x000002CD
		public override int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex)
		{
			return 0;
		}

		// Token: 0x06001890 RID: 6288 RVA: 0x000020CD File Offset: 0x000002CD
		public override int GetMaxByteCount(int charCount)
		{
			return 0;
		}

		// Token: 0x06001891 RID: 6289 RVA: 0x0008E7CE File Offset: 0x0008C9CE
		public override int GetCharCount(byte[] bytes, int index, int count)
		{
			return this.ucs4Decoder.GetCharCount(bytes, index, count);
		}

		// Token: 0x06001892 RID: 6290 RVA: 0x0008E7DE File Offset: 0x0008C9DE
		public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
		{
			return this.ucs4Decoder.GetChars(bytes, byteIndex, byteCount, chars, charIndex);
		}

		// Token: 0x06001893 RID: 6291 RVA: 0x0008E7F2 File Offset: 0x0008C9F2
		public override int GetMaxCharCount(int byteCount)
		{
			return (byteCount + 3) / 4;
		}

		// Token: 0x1700049C RID: 1180
		// (get) Token: 0x06001894 RID: 6292 RVA: 0x000020CD File Offset: 0x000002CD
		public override int CodePage
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06001895 RID: 6293 RVA: 0x0008E7F9 File Offset: 0x0008C9F9
		public override int GetCharCount(byte[] bytes)
		{
			return bytes.Length / 4;
		}

		// Token: 0x06001896 RID: 6294 RVA: 0x000037FB File Offset: 0x000019FB
		public override Encoder GetEncoder()
		{
			return null;
		}

		// Token: 0x1700049D RID: 1181
		// (get) Token: 0x06001897 RID: 6295 RVA: 0x0008E800 File Offset: 0x0008CA00
		internal static Encoding UCS4_Littleendian
		{
			get
			{
				return new Ucs4Encoding4321();
			}
		}

		// Token: 0x1700049E RID: 1182
		// (get) Token: 0x06001898 RID: 6296 RVA: 0x0008E807 File Offset: 0x0008CA07
		internal static Encoding UCS4_Bigendian
		{
			get
			{
				return new Ucs4Encoding1234();
			}
		}

		// Token: 0x1700049F RID: 1183
		// (get) Token: 0x06001899 RID: 6297 RVA: 0x0008E80E File Offset: 0x0008CA0E
		internal static Encoding UCS4_2143
		{
			get
			{
				return new Ucs4Encoding2143();
			}
		}

		// Token: 0x170004A0 RID: 1184
		// (get) Token: 0x0600189A RID: 6298 RVA: 0x0008E815 File Offset: 0x0008CA15
		internal static Encoding UCS4_3412
		{
			get
			{
				return new Ucs4Encoding3412();
			}
		}

		// Token: 0x0600189B RID: 6299 RVA: 0x0008E81C File Offset: 0x0008CA1C
		public Ucs4Encoding()
		{
		}

		// Token: 0x04001016 RID: 4118
		internal Ucs4Decoder ucs4Decoder;
	}
}
