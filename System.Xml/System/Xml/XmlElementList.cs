﻿using System;
using System.Collections;

namespace System.Xml
{
	// Token: 0x02000228 RID: 552
	internal class XmlElementList : XmlNodeList
	{
		// Token: 0x060014A6 RID: 5286 RVA: 0x00075660 File Offset: 0x00073860
		private XmlElementList(XmlNode parent)
		{
			this.rootNode = parent;
			this.curInd = -1;
			this.curElem = this.rootNode;
			this.changeCount = 0;
			this.empty = false;
			this.atomized = true;
			this.matchCount = -1;
			this.listener = new WeakReference(new XmlElementListListener(parent.Document, this));
		}

		// Token: 0x060014A7 RID: 5287 RVA: 0x000756C0 File Offset: 0x000738C0
		~XmlElementList()
		{
			this.Dispose(false);
		}

		// Token: 0x060014A8 RID: 5288 RVA: 0x000756F0 File Offset: 0x000738F0
		internal void ConcurrencyCheck(XmlNodeChangedEventArgs args)
		{
			if (!this.atomized)
			{
				XmlNameTable nameTable = this.rootNode.Document.NameTable;
				this.localName = nameTable.Add(this.localName);
				this.namespaceURI = nameTable.Add(this.namespaceURI);
				this.atomized = true;
			}
			if (this.IsMatch(args.Node))
			{
				this.changeCount++;
				this.curInd = -1;
				this.curElem = this.rootNode;
				if (args.Action == XmlNodeChangedAction.Insert)
				{
					this.empty = false;
				}
			}
			this.matchCount = -1;
		}

		// Token: 0x060014A9 RID: 5289 RVA: 0x00075788 File Offset: 0x00073988
		internal XmlElementList(XmlNode parent, string name) : this(parent)
		{
			XmlNameTable nameTable = parent.Document.NameTable;
			this.asterisk = nameTable.Add("*");
			this.name = nameTable.Add(name);
			this.localName = null;
			this.namespaceURI = null;
		}

		// Token: 0x060014AA RID: 5290 RVA: 0x000757D4 File Offset: 0x000739D4
		internal XmlElementList(XmlNode parent, string localName, string namespaceURI) : this(parent)
		{
			XmlNameTable nameTable = parent.Document.NameTable;
			this.asterisk = nameTable.Add("*");
			this.localName = nameTable.Get(localName);
			this.namespaceURI = nameTable.Get(namespaceURI);
			if (this.localName == null || this.namespaceURI == null)
			{
				this.empty = true;
				this.atomized = false;
				this.localName = localName;
				this.namespaceURI = namespaceURI;
			}
			this.name = null;
		}

		// Token: 0x170003CE RID: 974
		// (get) Token: 0x060014AB RID: 5291 RVA: 0x00075852 File Offset: 0x00073A52
		internal int ChangeCount
		{
			get
			{
				return this.changeCount;
			}
		}

		// Token: 0x060014AC RID: 5292 RVA: 0x0007585C File Offset: 0x00073A5C
		private XmlNode NextElemInPreOrder(XmlNode curNode)
		{
			XmlNode xmlNode = curNode.FirstChild;
			if (xmlNode == null)
			{
				xmlNode = curNode;
				while (xmlNode != null && xmlNode != this.rootNode && xmlNode.NextSibling == null)
				{
					xmlNode = xmlNode.ParentNode;
				}
				if (xmlNode != null && xmlNode != this.rootNode)
				{
					xmlNode = xmlNode.NextSibling;
				}
			}
			if (xmlNode == this.rootNode)
			{
				xmlNode = null;
			}
			return xmlNode;
		}

		// Token: 0x060014AD RID: 5293 RVA: 0x000758B4 File Offset: 0x00073AB4
		private XmlNode PrevElemInPreOrder(XmlNode curNode)
		{
			XmlNode xmlNode = curNode.PreviousSibling;
			while (xmlNode != null && xmlNode.LastChild != null)
			{
				xmlNode = xmlNode.LastChild;
			}
			if (xmlNode == null)
			{
				xmlNode = curNode.ParentNode;
			}
			if (xmlNode == this.rootNode)
			{
				xmlNode = null;
			}
			return xmlNode;
		}

		// Token: 0x060014AE RID: 5294 RVA: 0x000758F4 File Offset: 0x00073AF4
		private bool IsMatch(XmlNode curNode)
		{
			if (curNode.NodeType == XmlNodeType.Element)
			{
				if (this.name != null)
				{
					if (Ref.Equal(this.name, this.asterisk) || Ref.Equal(curNode.Name, this.name))
					{
						return true;
					}
				}
				else if ((Ref.Equal(this.localName, this.asterisk) || Ref.Equal(curNode.LocalName, this.localName)) && (Ref.Equal(this.namespaceURI, this.asterisk) || curNode.NamespaceURI == this.namespaceURI))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060014AF RID: 5295 RVA: 0x0007598C File Offset: 0x00073B8C
		private XmlNode GetMatchingNode(XmlNode n, bool bNext)
		{
			XmlNode xmlNode = n;
			do
			{
				if (bNext)
				{
					xmlNode = this.NextElemInPreOrder(xmlNode);
				}
				else
				{
					xmlNode = this.PrevElemInPreOrder(xmlNode);
				}
			}
			while (xmlNode != null && !this.IsMatch(xmlNode));
			return xmlNode;
		}

		// Token: 0x060014B0 RID: 5296 RVA: 0x000759C0 File Offset: 0x00073BC0
		private XmlNode GetNthMatchingNode(XmlNode n, bool bNext, int nCount)
		{
			XmlNode xmlNode = n;
			for (int i = 0; i < nCount; i++)
			{
				xmlNode = this.GetMatchingNode(xmlNode, bNext);
				if (xmlNode == null)
				{
					return null;
				}
			}
			return xmlNode;
		}

		// Token: 0x060014B1 RID: 5297 RVA: 0x000759EC File Offset: 0x00073BEC
		public XmlNode GetNextNode(XmlNode n)
		{
			if (this.empty)
			{
				return null;
			}
			XmlNode n2 = (n == null) ? this.rootNode : n;
			return this.GetMatchingNode(n2, true);
		}

		// Token: 0x060014B2 RID: 5298 RVA: 0x00075A18 File Offset: 0x00073C18
		public override XmlNode Item(int index)
		{
			if (this.rootNode == null || index < 0)
			{
				return null;
			}
			if (this.empty)
			{
				return null;
			}
			if (this.curInd == index)
			{
				return this.curElem;
			}
			int num = index - this.curInd;
			bool bNext = num > 0;
			if (num < 0)
			{
				num = -num;
			}
			XmlNode nthMatchingNode;
			if ((nthMatchingNode = this.GetNthMatchingNode(this.curElem, bNext, num)) != null)
			{
				this.curInd = index;
				this.curElem = nthMatchingNode;
				return this.curElem;
			}
			return null;
		}

		// Token: 0x170003CF RID: 975
		// (get) Token: 0x060014B3 RID: 5299 RVA: 0x00075A8C File Offset: 0x00073C8C
		public override int Count
		{
			get
			{
				if (this.empty)
				{
					return 0;
				}
				if (this.matchCount < 0)
				{
					int num = 0;
					int num2 = this.changeCount;
					XmlNode matchingNode = this.rootNode;
					while ((matchingNode = this.GetMatchingNode(matchingNode, true)) != null)
					{
						num++;
					}
					if (num2 != this.changeCount)
					{
						return num;
					}
					this.matchCount = num;
				}
				return this.matchCount;
			}
		}

		// Token: 0x060014B4 RID: 5300 RVA: 0x00075AE6 File Offset: 0x00073CE6
		public override IEnumerator GetEnumerator()
		{
			if (this.empty)
			{
				return new XmlEmptyElementListEnumerator(this);
			}
			return new XmlElementListEnumerator(this);
		}

		// Token: 0x060014B5 RID: 5301 RVA: 0x00075AFD File Offset: 0x00073CFD
		protected override void PrivateDisposeNodeList()
		{
			GC.SuppressFinalize(this);
			this.Dispose(true);
		}

		// Token: 0x060014B6 RID: 5302 RVA: 0x00075B0C File Offset: 0x00073D0C
		protected virtual void Dispose(bool disposing)
		{
			if (this.listener != null)
			{
				XmlElementListListener xmlElementListListener = (XmlElementListListener)this.listener.Target;
				if (xmlElementListListener != null)
				{
					xmlElementListListener.Unregister();
				}
				this.listener = null;
			}
		}

		// Token: 0x04000DC0 RID: 3520
		private string asterisk;

		// Token: 0x04000DC1 RID: 3521
		private int changeCount;

		// Token: 0x04000DC2 RID: 3522
		private string name;

		// Token: 0x04000DC3 RID: 3523
		private string localName;

		// Token: 0x04000DC4 RID: 3524
		private string namespaceURI;

		// Token: 0x04000DC5 RID: 3525
		private XmlNode rootNode;

		// Token: 0x04000DC6 RID: 3526
		private int curInd;

		// Token: 0x04000DC7 RID: 3527
		private XmlNode curElem;

		// Token: 0x04000DC8 RID: 3528
		private bool empty;

		// Token: 0x04000DC9 RID: 3529
		private bool atomized;

		// Token: 0x04000DCA RID: 3530
		private int matchCount;

		// Token: 0x04000DCB RID: 3531
		private WeakReference listener;
	}
}
