﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x02000290 RID: 656
	internal class XmlDownloadManager
	{
		// Token: 0x06001861 RID: 6241 RVA: 0x0008DD5D File Offset: 0x0008BF5D
		internal Stream GetStream(Uri uri, ICredentials credentials, IWebProxy proxy, RequestCachePolicy cachePolicy)
		{
			if (uri.Scheme == "file")
			{
				return new FileStream(uri.LocalPath, FileMode.Open, FileAccess.Read, FileShare.Read, 1);
			}
			return this.GetNonFileStream(uri, credentials, proxy, cachePolicy);
		}

		// Token: 0x06001862 RID: 6242 RVA: 0x0008DD8C File Offset: 0x0008BF8C
		private Stream GetNonFileStream(Uri uri, ICredentials credentials, IWebProxy proxy, RequestCachePolicy cachePolicy)
		{
			WebRequest webRequest = WebRequest.Create(uri);
			if (credentials != null)
			{
				webRequest.Credentials = credentials;
			}
			if (proxy != null)
			{
				webRequest.Proxy = proxy;
			}
			if (cachePolicy != null)
			{
				webRequest.CachePolicy = cachePolicy;
			}
			WebResponse response = webRequest.GetResponse();
			HttpWebRequest httpWebRequest = webRequest as HttpWebRequest;
			if (httpWebRequest != null)
			{
				lock (this)
				{
					if (this.connections == null)
					{
						this.connections = new Hashtable();
					}
					OpenedHost openedHost = (OpenedHost)this.connections[httpWebRequest.Address.Host];
					if (openedHost == null)
					{
						openedHost = new OpenedHost();
					}
					if (openedHost.nonCachedConnectionsCount < httpWebRequest.ServicePoint.ConnectionLimit - 1)
					{
						if (openedHost.nonCachedConnectionsCount == 0)
						{
							this.connections.Add(httpWebRequest.Address.Host, openedHost);
						}
						openedHost.nonCachedConnectionsCount++;
						return new XmlRegisteredNonCachedStream(response.GetResponseStream(), this, httpWebRequest.Address.Host);
					}
					return new XmlCachedStream(response.ResponseUri, response.GetResponseStream());
				}
			}
			return response.GetResponseStream();
		}

		// Token: 0x06001863 RID: 6243 RVA: 0x0008DEB8 File Offset: 0x0008C0B8
		internal void Remove(string host)
		{
			lock (this)
			{
				OpenedHost openedHost = (OpenedHost)this.connections[host];
				if (openedHost != null)
				{
					OpenedHost openedHost2 = openedHost;
					int num = openedHost2.nonCachedConnectionsCount - 1;
					openedHost2.nonCachedConnectionsCount = num;
					if (num == 0)
					{
						this.connections.Remove(host);
					}
				}
			}
		}

		// Token: 0x06001864 RID: 6244 RVA: 0x0008DF24 File Offset: 0x0008C124
		internal Task<Stream> GetStreamAsync(Uri uri, ICredentials credentials, IWebProxy proxy, RequestCachePolicy cachePolicy)
		{
			if (uri.Scheme == "file")
			{
				return Task.Run<Stream>(() => new FileStream(uri.LocalPath, FileMode.Open, FileAccess.Read, FileShare.Read, 1, true));
			}
			return this.GetNonFileStreamAsync(uri, credentials, proxy, cachePolicy);
		}

		// Token: 0x06001865 RID: 6245 RVA: 0x0008DF78 File Offset: 0x0008C178
		private async Task<Stream> GetNonFileStreamAsync(Uri uri, ICredentials credentials, IWebProxy proxy, RequestCachePolicy cachePolicy)
		{
			WebRequest req = WebRequest.Create(uri);
			if (credentials != null)
			{
				req.Credentials = credentials;
			}
			if (proxy != null)
			{
				req.Proxy = proxy;
			}
			if (cachePolicy != null)
			{
				req.CachePolicy = cachePolicy;
			}
			WebResponse webResponse = await Task<WebResponse>.Factory.FromAsync(new Func<AsyncCallback, object, IAsyncResult>(req.BeginGetResponse), new Func<IAsyncResult, WebResponse>(req.EndGetResponse), null).ConfigureAwait(false);
			HttpWebRequest httpWebRequest = req as HttpWebRequest;
			if (httpWebRequest != null)
			{
				lock (this)
				{
					if (this.connections == null)
					{
						this.connections = new Hashtable();
					}
					OpenedHost openedHost = (OpenedHost)this.connections[httpWebRequest.Address.Host];
					if (openedHost == null)
					{
						openedHost = new OpenedHost();
					}
					if (openedHost.nonCachedConnectionsCount < httpWebRequest.ServicePoint.ConnectionLimit - 1)
					{
						if (openedHost.nonCachedConnectionsCount == 0)
						{
							this.connections.Add(httpWebRequest.Address.Host, openedHost);
						}
						openedHost.nonCachedConnectionsCount++;
						return new XmlRegisteredNonCachedStream(webResponse.GetResponseStream(), this, httpWebRequest.Address.Host);
					}
					return new XmlCachedStream(webResponse.ResponseUri, webResponse.GetResponseStream());
				}
			}
			return webResponse.GetResponseStream();
		}

		// Token: 0x06001866 RID: 6246 RVA: 0x00002103 File Offset: 0x00000303
		public XmlDownloadManager()
		{
		}

		// Token: 0x04001002 RID: 4098
		private Hashtable connections;

		// Token: 0x02000291 RID: 657
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x06001867 RID: 6247 RVA: 0x00002103 File Offset: 0x00000303
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x06001868 RID: 6248 RVA: 0x0008DFDE File Offset: 0x0008C1DE
			internal Stream <GetStreamAsync>b__0()
			{
				return new FileStream(this.uri.LocalPath, FileMode.Open, FileAccess.Read, FileShare.Read, 1, true);
			}

			// Token: 0x04001003 RID: 4099
			public Uri uri;
		}

		// Token: 0x02000292 RID: 658
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <GetNonFileStreamAsync>d__5 : IAsyncStateMachine
		{
			// Token: 0x06001869 RID: 6249 RVA: 0x0008DFF8 File Offset: 0x0008C1F8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlDownloadManager xmlDownloadManager = this;
				Stream result2;
				try
				{
					ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						req = WebRequest.Create(uri);
						if (credentials != null)
						{
							req.Credentials = credentials;
						}
						if (proxy != null)
						{
							req.Proxy = proxy;
						}
						if (cachePolicy != null)
						{
							req.CachePolicy = cachePolicy;
						}
						configuredTaskAwaiter = Task<WebResponse>.Factory.FromAsync(new Func<AsyncCallback, object, IAsyncResult>(req.BeginGetResponse), new Func<IAsyncResult, WebResponse>(req.EndGetResponse), null).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter, XmlDownloadManager.<GetNonFileStreamAsync>d__5>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					WebResponse result = configuredTaskAwaiter.GetResult();
					HttpWebRequest httpWebRequest = req as HttpWebRequest;
					if (httpWebRequest != null)
					{
						XmlDownloadManager obj = xmlDownloadManager;
						bool flag = false;
						try
						{
							Monitor.Enter(obj, ref flag);
							if (xmlDownloadManager.connections == null)
							{
								xmlDownloadManager.connections = new Hashtable();
							}
							OpenedHost openedHost = (OpenedHost)xmlDownloadManager.connections[httpWebRequest.Address.Host];
							if (openedHost == null)
							{
								openedHost = new OpenedHost();
							}
							if (openedHost.nonCachedConnectionsCount < httpWebRequest.ServicePoint.ConnectionLimit - 1)
							{
								if (openedHost.nonCachedConnectionsCount == 0)
								{
									xmlDownloadManager.connections.Add(httpWebRequest.Address.Host, openedHost);
								}
								openedHost.nonCachedConnectionsCount++;
								result2 = new XmlRegisteredNonCachedStream(result.GetResponseStream(), xmlDownloadManager, httpWebRequest.Address.Host);
								goto IL_20A;
							}
							result2 = new XmlCachedStream(result.ResponseUri, result.GetResponseStream());
							goto IL_20A;
						}
						finally
						{
							if (num < 0 && flag)
							{
								Monitor.Exit(obj);
							}
						}
					}
					result2 = result.GetResponseStream();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_20A:
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x0600186A RID: 6250 RVA: 0x0008E258 File Offset: 0x0008C458
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04001004 RID: 4100
			public int <>1__state;

			// Token: 0x04001005 RID: 4101
			public AsyncTaskMethodBuilder<Stream> <>t__builder;

			// Token: 0x04001006 RID: 4102
			public Uri uri;

			// Token: 0x04001007 RID: 4103
			public ICredentials credentials;

			// Token: 0x04001008 RID: 4104
			public IWebProxy proxy;

			// Token: 0x04001009 RID: 4105
			public RequestCachePolicy cachePolicy;

			// Token: 0x0400100A RID: 4106
			private WebRequest <req>5__1;

			// Token: 0x0400100B RID: 4107
			public XmlDownloadManager <>4__this;

			// Token: 0x0400100C RID: 4108
			private ConfiguredTaskAwaitable<WebResponse>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
