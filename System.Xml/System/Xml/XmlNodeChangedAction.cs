﻿using System;

namespace System.Xml
{
	/// <summary>Specifies the type of node change.</summary>
	// Token: 0x0200022E RID: 558
	public enum XmlNodeChangedAction
	{
		/// <summary>A node is being inserted in the tree.</summary>
		// Token: 0x04000DDD RID: 3549
		Insert,
		/// <summary>A node is being removed from the tree.</summary>
		// Token: 0x04000DDE RID: 3550
		Remove,
		/// <summary>A node value is being changed.</summary>
		// Token: 0x04000DDF RID: 3551
		Change
	}
}
