﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x02000081 RID: 129
	internal static class BinHexEncoder
	{
		// Token: 0x060003BC RID: 956 RVA: 0x0000E970 File Offset: 0x0000CB70
		internal static void Encode(byte[] buffer, int index, int count, XmlWriter writer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (count > buffer.Length - index)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			char[] array = new char[(count * 2 < 128) ? (count * 2) : 128];
			int num = index + count;
			while (index < num)
			{
				int num2 = (count < 64) ? count : 64;
				int count2 = BinHexEncoder.Encode(buffer, index, num2, array);
				writer.WriteRaw(array, 0, count2);
				index += num2;
				count -= num2;
			}
		}

		// Token: 0x060003BD RID: 957 RVA: 0x0000EA08 File Offset: 0x0000CC08
		internal static string Encode(byte[] inArray, int offsetIn, int count)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (0 > offsetIn)
			{
				throw new ArgumentOutOfRangeException("offsetIn");
			}
			if (0 > count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (count > inArray.Length - offsetIn)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			char[] array = new char[2 * count];
			int length = BinHexEncoder.Encode(inArray, offsetIn, count, array);
			return new string(array, 0, length);
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0000EA70 File Offset: 0x0000CC70
		private static int Encode(byte[] inArray, int offsetIn, int count, char[] outArray)
		{
			int num = 0;
			int num2 = 0;
			int num3 = outArray.Length;
			for (int i = 0; i < count; i++)
			{
				byte b = inArray[offsetIn++];
				outArray[num++] = "0123456789ABCDEF"[b >> 4];
				if (num == num3)
				{
					break;
				}
				outArray[num++] = "0123456789ABCDEF"[(int)(b & 15)];
				if (num == num3)
				{
					break;
				}
			}
			return num - num2;
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0000EAD4 File Offset: 0x0000CCD4
		internal static async Task EncodeAsync(byte[] buffer, int index, int count, XmlWriter writer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (count > buffer.Length - index)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			char[] chars = new char[(count * 2 < 128) ? (count * 2) : 128];
			int endIndex = index + count;
			while (index < endIndex)
			{
				int cnt = (count < 64) ? count : 64;
				int count2 = BinHexEncoder.Encode(buffer, index, cnt, chars);
				await writer.WriteRawAsync(chars, 0, count2).ConfigureAwait(false);
				index += cnt;
				count -= cnt;
			}
		}

		// Token: 0x0400022E RID: 558
		private const string s_hexDigits = "0123456789ABCDEF";

		// Token: 0x0400022F RID: 559
		private const int CharsChunkSize = 128;

		// Token: 0x02000082 RID: 130
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <EncodeAsync>d__5 : IAsyncStateMachine
		{
			// Token: 0x060003C0 RID: 960 RVA: 0x0000EB34 File Offset: 0x0000CD34
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (count > buffer.Length - index)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						chars = new char[(count * 2 < 128) ? (count * 2) : 128];
						endIndex = index + count;
						goto IL_17A;
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					IL_14D:
					configuredTaskAwaiter.GetResult();
					index += cnt;
					count -= cnt;
					IL_17A:
					if (index < endIndex)
					{
						cnt = ((count < 64) ? count : 64);
						int num3 = BinHexEncoder.Encode(buffer, index, cnt, chars);
						configuredTaskAwaiter = writer.WriteRawAsync(chars, 0, num3).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BinHexEncoder.<EncodeAsync>d__5>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_14D;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x060003C1 RID: 961 RVA: 0x0000ED18 File Offset: 0x0000CF18
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000230 RID: 560
			public int <>1__state;

			// Token: 0x04000231 RID: 561
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000232 RID: 562
			public byte[] buffer;

			// Token: 0x04000233 RID: 563
			public int index;

			// Token: 0x04000234 RID: 564
			public int count;

			// Token: 0x04000235 RID: 565
			private char[] <chars>5__1;

			// Token: 0x04000236 RID: 566
			public XmlWriter writer;

			// Token: 0x04000237 RID: 567
			private int <cnt>5__2;

			// Token: 0x04000238 RID: 568
			private int <endIndex>5__3;

			// Token: 0x04000239 RID: 569
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
