﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Xml.XmlConfiguration
{
	/// <summary>Represents an XML reader section.</summary>
	// Token: 0x02000486 RID: 1158
	[EditorBrowsable(EditorBrowsableState.Never)]
	public sealed class XmlReaderSection
	{
		// Token: 0x1700096A RID: 2410
		// (get) Token: 0x06002D4D RID: 11597 RVA: 0x000020CD File Offset: 0x000002CD
		internal static bool ProhibitDefaultUrlResolver
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06002D4E RID: 11598 RVA: 0x000F5BB1 File Offset: 0x000F3DB1
		internal static XmlResolver CreateDefaultResolver()
		{
			if (XmlReaderSection.ProhibitDefaultUrlResolver)
			{
				return null;
			}
			return new XmlUrlResolver();
		}

		/// <summary>Gets or sets the string that represents a boolean value indicating whether white spaces are collapsed into empty strings. The default value is "false".</summary>
		/// <returns>A string that represents a boolean value indicating whether white spaces are collapsed into empty strings.</returns>
		// Token: 0x1700096B RID: 2411
		// (get) Token: 0x06002D4F RID: 11599 RVA: 0x000037FB File Offset: 0x000019FB
		// (set) Token: 0x06002D50 RID: 11600 RVA: 0x000030EC File Offset: 0x000012EC
		public string CollapseWhiteSpaceIntoEmptyStringString
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x1700096C RID: 2412
		// (get) Token: 0x06002D51 RID: 11601 RVA: 0x000F5BC4 File Offset: 0x000F3DC4
		private bool _CollapseWhiteSpaceIntoEmptyString
		{
			get
			{
				bool result;
				XmlConvert.TryToBoolean(this.CollapseWhiteSpaceIntoEmptyStringString, out result);
				return result;
			}
		}

		// Token: 0x1700096D RID: 2413
		// (get) Token: 0x06002D52 RID: 11602 RVA: 0x000020CD File Offset: 0x000002CD
		internal static bool CollapseWhiteSpaceIntoEmptyString
		{
			get
			{
				return false;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlConfiguration.XmlReaderSection" /> class.</summary>
		// Token: 0x06002D53 RID: 11603 RVA: 0x00002103 File Offset: 0x00000303
		public XmlReaderSection()
		{
		}

		/// <summary>Gets or sets the string that represents the prohibit default resolver.</summary>
		/// <returns>A <see cref="T:System.String" /> that represents the prohibit default resolver.</returns>
		// Token: 0x1700096E RID: 2414
		// (get) Token: 0x06002D54 RID: 11604 RVA: 0x000A7058 File Offset: 0x000A5258
		// (set) Token: 0x06002D55 RID: 11605 RVA: 0x00072668 File Offset: 0x00070868
		public string ProhibitDefaultResolverString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
