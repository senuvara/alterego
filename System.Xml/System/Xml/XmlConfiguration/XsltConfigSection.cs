﻿using System;
using System.ComponentModel;
using Unity;

namespace System.Xml.XmlConfiguration
{
	/// <summary>Represents an XSLT configuration section.</summary>
	// Token: 0x02000487 RID: 1159
	[EditorBrowsable(EditorBrowsableState.Never)]
	public sealed class XsltConfigSection
	{
		// Token: 0x1700096F RID: 2415
		// (get) Token: 0x06002D56 RID: 11606 RVA: 0x000020CD File Offset: 0x000002CD
		private static bool s_ProhibitDefaultUrlResolver
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06002D57 RID: 11607 RVA: 0x000F5BE0 File Offset: 0x000F3DE0
		internal static XmlResolver CreateDefaultResolver()
		{
			if (XsltConfigSection.s_ProhibitDefaultUrlResolver)
			{
				return XmlNullResolver.Singleton;
			}
			return new XmlUrlResolver();
		}

		// Token: 0x17000970 RID: 2416
		// (get) Token: 0x06002D58 RID: 11608 RVA: 0x000033DE File Offset: 0x000015DE
		internal static bool LimitXPathComplexity
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000971 RID: 2417
		// (get) Token: 0x06002D59 RID: 11609 RVA: 0x000020CD File Offset: 0x000002CD
		internal static bool EnableMemberAccessForXslCompiledTransform
		{
			get
			{
				return false;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlConfiguration.XsltConfigSection" /> class.</summary>
		// Token: 0x06002D5A RID: 11610 RVA: 0x00002103 File Offset: 0x00000303
		public XsltConfigSection()
		{
		}

		/// <summary>Gets or sets a string that represents the XSLT prohibit default resolver.</summary>
		/// <returns>A string that represents the XSLT prohibit default resolver.</returns>
		// Token: 0x17000972 RID: 2418
		// (get) Token: 0x06002D5B RID: 11611 RVA: 0x000A7058 File Offset: 0x000A5258
		// (set) Token: 0x06002D5C RID: 11612 RVA: 0x00072668 File Offset: 0x00070868
		public string ProhibitDefaultResolverString
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
