﻿using System;
using System.Globalization;

namespace System.Xml.XmlConfiguration
{
	// Token: 0x02000485 RID: 1157
	internal static class XmlConfigurationString
	{
		// Token: 0x06002D4C RID: 11596 RVA: 0x000F5B73 File Offset: 0x000F3D73
		// Note: this type is marked as 'beforefieldinit'.
		static XmlConfigurationString()
		{
		}

		// Token: 0x04001EE5 RID: 7909
		internal const string XmlReaderSectionName = "xmlReader";

		// Token: 0x04001EE6 RID: 7910
		internal const string XsltSectionName = "xslt";

		// Token: 0x04001EE7 RID: 7911
		internal const string ProhibitDefaultResolverName = "prohibitDefaultResolver";

		// Token: 0x04001EE8 RID: 7912
		internal const string LimitXPathComplexityName = "limitXPathComplexity";

		// Token: 0x04001EE9 RID: 7913
		internal const string EnableMemberAccessForXslCompiledTransformName = "enableMemberAccessForXslCompiledTransform";

		// Token: 0x04001EEA RID: 7914
		internal const string CollapseWhiteSpaceIntoEmptyStringName = "CollapseWhiteSpaceIntoEmptyString";

		// Token: 0x04001EEB RID: 7915
		internal const string XmlConfigurationSectionName = "system.xml";

		// Token: 0x04001EEC RID: 7916
		internal static string XmlReaderSectionPath = string.Format(CultureInfo.InvariantCulture, "{0}/{1}", "system.xml", "xmlReader");

		// Token: 0x04001EED RID: 7917
		internal static string XsltSectionPath = string.Format(CultureInfo.InvariantCulture, "{0}/{1}", "system.xml", "xslt");
	}
}
