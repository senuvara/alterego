﻿using System;

namespace System.Xml
{
	// Token: 0x0200008F RID: 143
	internal class BitStack
	{
		// Token: 0x060004BB RID: 1211 RVA: 0x0001611E File Offset: 0x0001431E
		public BitStack()
		{
			this.curr = 1U;
		}

		// Token: 0x060004BC RID: 1212 RVA: 0x0001612D File Offset: 0x0001432D
		public void PushBit(bool bit)
		{
			if ((this.curr & 2147483648U) != 0U)
			{
				this.PushCurr();
			}
			this.curr = (this.curr << 1 | (bit ? 1U : 0U));
		}

		// Token: 0x060004BD RID: 1213 RVA: 0x00016159 File Offset: 0x00014359
		public bool PopBit()
		{
			bool result = (this.curr & 1U) > 0U;
			this.curr >>= 1;
			if (this.curr == 1U)
			{
				this.PopCurr();
			}
			return result;
		}

		// Token: 0x060004BE RID: 1214 RVA: 0x00016183 File Offset: 0x00014383
		public bool PeekBit()
		{
			return (this.curr & 1U) > 0U;
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x060004BF RID: 1215 RVA: 0x00016190 File Offset: 0x00014390
		public bool IsEmpty
		{
			get
			{
				return this.curr == 1U;
			}
		}

		// Token: 0x060004C0 RID: 1216 RVA: 0x0001619C File Offset: 0x0001439C
		private void PushCurr()
		{
			if (this.bitStack == null)
			{
				this.bitStack = new uint[16];
			}
			uint[] array = this.bitStack;
			int num = this.stackPos;
			this.stackPos = num + 1;
			array[num] = this.curr;
			this.curr = 1U;
			int num2 = this.bitStack.Length;
			if (this.stackPos >= num2)
			{
				uint[] destinationArray = new uint[2 * num2];
				Array.Copy(this.bitStack, destinationArray, num2);
				this.bitStack = destinationArray;
			}
		}

		// Token: 0x060004C1 RID: 1217 RVA: 0x00016214 File Offset: 0x00014414
		private void PopCurr()
		{
			if (this.stackPos > 0)
			{
				uint[] array = this.bitStack;
				int num = this.stackPos - 1;
				this.stackPos = num;
				this.curr = array[num];
			}
		}

		// Token: 0x040002F4 RID: 756
		private uint[] bitStack;

		// Token: 0x040002F5 RID: 757
		private int stackPos;

		// Token: 0x040002F6 RID: 758
		private uint curr;
	}
}
