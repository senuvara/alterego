﻿using System;
using System.Collections;

namespace System.Xml
{
	// Token: 0x02000220 RID: 544
	internal class XmlChildNodes : XmlNodeList
	{
		// Token: 0x060013A3 RID: 5027 RVA: 0x00072B53 File Offset: 0x00070D53
		public XmlChildNodes(XmlNode container)
		{
			this.container = container;
		}

		// Token: 0x060013A4 RID: 5028 RVA: 0x00072B64 File Offset: 0x00070D64
		public override XmlNode Item(int i)
		{
			if (i < 0)
			{
				return null;
			}
			XmlNode xmlNode = this.container.FirstChild;
			while (xmlNode != null)
			{
				if (i == 0)
				{
					return xmlNode;
				}
				xmlNode = xmlNode.NextSibling;
				i--;
			}
			return null;
		}

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x060013A5 RID: 5029 RVA: 0x00072B9C File Offset: 0x00070D9C
		public override int Count
		{
			get
			{
				int num = 0;
				for (XmlNode xmlNode = this.container.FirstChild; xmlNode != null; xmlNode = xmlNode.NextSibling)
				{
					num++;
				}
				return num;
			}
		}

		// Token: 0x060013A6 RID: 5030 RVA: 0x00072BC8 File Offset: 0x00070DC8
		public override IEnumerator GetEnumerator()
		{
			if (this.container.FirstChild == null)
			{
				return XmlDocument.EmptyEnumerator;
			}
			return new XmlChildEnumerator(this.container);
		}

		// Token: 0x04000D81 RID: 3457
		private XmlNode container;
	}
}
