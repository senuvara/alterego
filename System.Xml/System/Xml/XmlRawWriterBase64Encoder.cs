﻿using System;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x0200007E RID: 126
	internal class XmlRawWriterBase64Encoder : Base64Encoder
	{
		// Token: 0x060003AD RID: 941 RVA: 0x0000E5D2 File Offset: 0x0000C7D2
		internal XmlRawWriterBase64Encoder(XmlRawWriter rawWriter)
		{
			this.rawWriter = rawWriter;
		}

		// Token: 0x060003AE RID: 942 RVA: 0x0000E5E1 File Offset: 0x0000C7E1
		internal override void WriteChars(char[] chars, int index, int count)
		{
			this.rawWriter.WriteRaw(chars, index, count);
		}

		// Token: 0x060003AF RID: 943 RVA: 0x0000E5F1 File Offset: 0x0000C7F1
		internal override Task WriteCharsAsync(char[] chars, int index, int count)
		{
			return this.rawWriter.WriteRawAsync(chars, index, count);
		}

		// Token: 0x04000226 RID: 550
		private XmlRawWriter rawWriter;
	}
}
