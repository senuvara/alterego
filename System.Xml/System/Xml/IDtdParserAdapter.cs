﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020000A3 RID: 163
	internal interface IDtdParserAdapter
	{
		// Token: 0x17000116 RID: 278
		// (get) Token: 0x0600054B RID: 1355
		XmlNameTable NameTable { get; }

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x0600054C RID: 1356
		IXmlNamespaceResolver NamespaceResolver { get; }

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x0600054D RID: 1357
		Uri BaseUri { get; }

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x0600054E RID: 1358
		char[] ParsingBuffer { get; }

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x0600054F RID: 1359
		int ParsingBufferLength { get; }

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06000550 RID: 1360
		// (set) Token: 0x06000551 RID: 1361
		int CurrentPosition { get; set; }

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x06000552 RID: 1362
		int LineNo { get; }

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x06000553 RID: 1363
		int LineStartPosition { get; }

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x06000554 RID: 1364
		bool IsEof { get; }

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000555 RID: 1365
		int EntityStackLength { get; }

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000556 RID: 1366
		bool IsEntityEolNormalized { get; }

		// Token: 0x06000557 RID: 1367
		int ReadData();

		// Token: 0x06000558 RID: 1368
		void OnNewLine(int pos);

		// Token: 0x06000559 RID: 1369
		int ParseNumericCharRef(StringBuilder internalSubsetBuilder);

		// Token: 0x0600055A RID: 1370
		int ParseNamedCharRef(bool expand, StringBuilder internalSubsetBuilder);

		// Token: 0x0600055B RID: 1371
		void ParsePI(StringBuilder sb);

		// Token: 0x0600055C RID: 1372
		void ParseComment(StringBuilder sb);

		// Token: 0x0600055D RID: 1373
		bool PushEntity(IDtdEntityInfo entity, out int entityId);

		// Token: 0x0600055E RID: 1374
		bool PopEntity(out IDtdEntityInfo oldEntity, out int newEntityId);

		// Token: 0x0600055F RID: 1375
		bool PushExternalSubset(string systemId, string publicId);

		// Token: 0x06000560 RID: 1376
		void PushInternalDtd(string baseUri, string internalDtd);

		// Token: 0x06000561 RID: 1377
		void OnSystemId(string systemId, LineInfo keywordLineInfo, LineInfo systemLiteralLineInfo);

		// Token: 0x06000562 RID: 1378
		void OnPublicId(string publicId, LineInfo keywordLineInfo, LineInfo publicLiteralLineInfo);

		// Token: 0x06000563 RID: 1379
		void Throw(Exception e);

		// Token: 0x06000564 RID: 1380
		Task<int> ReadDataAsync();

		// Token: 0x06000565 RID: 1381
		Task<int> ParseNumericCharRefAsync(StringBuilder internalSubsetBuilder);

		// Token: 0x06000566 RID: 1382
		Task<int> ParseNamedCharRefAsync(bool expand, StringBuilder internalSubsetBuilder);

		// Token: 0x06000567 RID: 1383
		Task ParsePIAsync(StringBuilder sb);

		// Token: 0x06000568 RID: 1384
		Task ParseCommentAsync(StringBuilder sb);

		// Token: 0x06000569 RID: 1385
		Task<Tuple<int, bool>> PushEntityAsync(IDtdEntityInfo entity);

		// Token: 0x0600056A RID: 1386
		Task<bool> PushExternalSubsetAsync(string systemId, string publicId);
	}
}
