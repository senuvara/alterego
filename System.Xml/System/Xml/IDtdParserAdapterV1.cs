﻿using System;

namespace System.Xml
{
	// Token: 0x020000A5 RID: 165
	internal interface IDtdParserAdapterV1 : IDtdParserAdapterWithValidation, IDtdParserAdapter
	{
		// Token: 0x17000123 RID: 291
		// (get) Token: 0x0600056D RID: 1389
		bool V1CompatibilityMode { get; }

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x0600056E RID: 1390
		bool Normalization { get; }

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x0600056F RID: 1391
		bool Namespaces { get; }
	}
}
