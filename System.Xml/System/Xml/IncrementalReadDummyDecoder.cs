﻿using System;

namespace System.Xml
{
	// Token: 0x020000AA RID: 170
	internal class IncrementalReadDummyDecoder : IncrementalReadDecoder
	{
		// Token: 0x1700012A RID: 298
		// (get) Token: 0x0600057F RID: 1407 RVA: 0x000183C2 File Offset: 0x000165C2
		internal override int DecodedCount
		{
			get
			{
				return -1;
			}
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06000580 RID: 1408 RVA: 0x000020CD File Offset: 0x000002CD
		internal override bool IsFull
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000581 RID: 1409 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void SetNextOutputBuffer(Array array, int offset, int len)
		{
		}

		// Token: 0x06000582 RID: 1410 RVA: 0x000183C5 File Offset: 0x000165C5
		internal override int Decode(char[] chars, int startPos, int len)
		{
			return len;
		}

		// Token: 0x06000583 RID: 1411 RVA: 0x000183C5 File Offset: 0x000165C5
		internal override int Decode(string str, int startPos, int len)
		{
			return len;
		}

		// Token: 0x06000584 RID: 1412 RVA: 0x000030EC File Offset: 0x000012EC
		internal override void Reset()
		{
		}

		// Token: 0x06000585 RID: 1413 RVA: 0x0000DF25 File Offset: 0x0000C125
		public IncrementalReadDummyDecoder()
		{
		}
	}
}
