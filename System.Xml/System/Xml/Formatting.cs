﻿using System;

namespace System.Xml
{
	/// <summary>Specifies formatting options for the <see cref="T:System.Xml.XmlTextWriter" />.</summary>
	// Token: 0x0200017D RID: 381
	public enum Formatting
	{
		/// <summary>No special formatting is applied. This is the default.</summary>
		// Token: 0x04000917 RID: 2327
		None,
		/// <summary>Causes child elements to be indented according to the <see cref="P:System.Xml.XmlTextWriter.Indentation" /> and <see cref="P:System.Xml.XmlTextWriter.IndentChar" /> settings. </summary>
		// Token: 0x04000918 RID: 2328
		Indented
	}
}
