﻿using System;
using System.Collections.Generic;

namespace System.Xml
{
	// Token: 0x020000CB RID: 203
	internal class XmlAsyncCheckReaderWithLineInfoNS : XmlAsyncCheckReaderWithLineInfo, IXmlNamespaceResolver
	{
		// Token: 0x060006D1 RID: 1745 RVA: 0x0001BFC6 File Offset: 0x0001A1C6
		public XmlAsyncCheckReaderWithLineInfoNS(XmlReader reader) : base(reader)
		{
			this.readerAsIXmlNamespaceResolver = (IXmlNamespaceResolver)reader;
		}

		// Token: 0x060006D2 RID: 1746 RVA: 0x0001BFDB File Offset: 0x0001A1DB
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return this.readerAsIXmlNamespaceResolver.GetNamespacesInScope(scope);
		}

		// Token: 0x060006D3 RID: 1747 RVA: 0x0001BFE9 File Offset: 0x0001A1E9
		string IXmlNamespaceResolver.LookupNamespace(string prefix)
		{
			return this.readerAsIXmlNamespaceResolver.LookupNamespace(prefix);
		}

		// Token: 0x060006D4 RID: 1748 RVA: 0x0001BFF7 File Offset: 0x0001A1F7
		string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
		{
			return this.readerAsIXmlNamespaceResolver.LookupPrefix(namespaceName);
		}

		// Token: 0x040003DB RID: 987
		private readonly IXmlNamespaceResolver readerAsIXmlNamespaceResolver;
	}
}
