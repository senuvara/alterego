﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x0200020E RID: 526
	internal class DocumentXPathNodeIterator_AllElemChildren : DocumentXPathNodeIterator_ElemDescendants
	{
		// Token: 0x060012E3 RID: 4835 RVA: 0x00070996 File Offset: 0x0006EB96
		internal DocumentXPathNodeIterator_AllElemChildren(DocumentXPathNavigator nav) : base(nav)
		{
		}

		// Token: 0x060012E4 RID: 4836 RVA: 0x0007099F File Offset: 0x0006EB9F
		internal DocumentXPathNodeIterator_AllElemChildren(DocumentXPathNodeIterator_AllElemChildren other) : base(other)
		{
		}

		// Token: 0x060012E5 RID: 4837 RVA: 0x000709A8 File Offset: 0x0006EBA8
		public override XPathNodeIterator Clone()
		{
			return new DocumentXPathNodeIterator_AllElemChildren(this);
		}

		// Token: 0x060012E6 RID: 4838 RVA: 0x000709B0 File Offset: 0x0006EBB0
		protected override bool Match(XmlNode node)
		{
			return node.NodeType == XmlNodeType.Element;
		}
	}
}
