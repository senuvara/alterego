﻿using System;

namespace System.Xml
{
	// Token: 0x02000299 RID: 665
	internal class Ucs4Encoding1234 : Ucs4Encoding
	{
		// Token: 0x0600189C RID: 6300 RVA: 0x0008E824 File Offset: 0x0008CA24
		public Ucs4Encoding1234()
		{
			this.ucs4Decoder = new Ucs4Decoder1234();
		}

		// Token: 0x170004A1 RID: 1185
		// (get) Token: 0x0600189D RID: 6301 RVA: 0x0008E837 File Offset: 0x0008CA37
		public override string EncodingName
		{
			get
			{
				return "ucs-4 (Bigendian)";
			}
		}

		// Token: 0x0600189E RID: 6302 RVA: 0x0008E83E File Offset: 0x0008CA3E
		public override byte[] GetPreamble()
		{
			return new byte[]
			{
				0,
				0,
				254,
				byte.MaxValue
			};
		}
	}
}
