﻿using System;

namespace System.Xml
{
	// Token: 0x020000BD RID: 189
	internal enum ElementProperties : uint
	{
		// Token: 0x040003AB RID: 939
		DEFAULT,
		// Token: 0x040003AC RID: 940
		URI_PARENT,
		// Token: 0x040003AD RID: 941
		BOOL_PARENT,
		// Token: 0x040003AE RID: 942
		NAME_PARENT = 4U,
		// Token: 0x040003AF RID: 943
		EMPTY = 8U,
		// Token: 0x040003B0 RID: 944
		NO_ENTITIES = 16U,
		// Token: 0x040003B1 RID: 945
		HEAD = 32U,
		// Token: 0x040003B2 RID: 946
		BLOCK_WS = 64U,
		// Token: 0x040003B3 RID: 947
		HAS_NS = 128U
	}
}
