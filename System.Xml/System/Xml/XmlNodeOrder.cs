﻿using System;

namespace System.Xml
{
	/// <summary>Describes the document order of a node compared to a second node.</summary>
	// Token: 0x020002A7 RID: 679
	public enum XmlNodeOrder
	{
		/// <summary>The current node of this navigator is before the current node of the supplied navigator.</summary>
		// Token: 0x04001032 RID: 4146
		Before,
		/// <summary>The current node of this navigator is after the current node of the supplied navigator.</summary>
		// Token: 0x04001033 RID: 4147
		After,
		/// <summary>The two navigators are positioned on the same node.</summary>
		// Token: 0x04001034 RID: 4148
		Same,
		/// <summary>The node positions cannot be determined in document order, relative to each other. This could occur if the two nodes reside in different trees.</summary>
		// Token: 0x04001035 RID: 4149
		Unknown
	}
}
