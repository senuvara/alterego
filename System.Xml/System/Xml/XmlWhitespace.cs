﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	/// <summary>Represents white space in element content.</summary>
	// Token: 0x02000244 RID: 580
	public class XmlWhitespace : XmlCharacterData
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlWhitespace" /> class.</summary>
		/// <param name="strData">The white space characters of the node.</param>
		/// <param name="doc">The <see cref="T:System.Xml.XmlDocument" /> object.</param>
		// Token: 0x06001664 RID: 5732 RVA: 0x0007BAB2 File Offset: 0x00079CB2
		protected internal XmlWhitespace(string strData, XmlDocument doc) : base(strData, doc)
		{
			if (!doc.IsLoading && !base.CheckOnData(strData))
			{
				throw new ArgumentException(Res.GetString("The string for white space contains an invalid character."));
			}
		}

		/// <summary>Gets the qualified name of the node.</summary>
		/// <returns>For <see langword="XmlWhitespace" /> nodes, this property returns <see langword="#whitespace" />.</returns>
		// Token: 0x17000479 RID: 1145
		// (get) Token: 0x06001665 RID: 5733 RVA: 0x0007BDA2 File Offset: 0x00079FA2
		public override string Name
		{
			get
			{
				return this.OwnerDocument.strNonSignificantWhitespaceName;
			}
		}

		/// <summary>Gets the local name of the node.</summary>
		/// <returns>For <see langword="XmlWhitespace" /> nodes, this property returns <see langword="#whitespace" />.</returns>
		// Token: 0x1700047A RID: 1146
		// (get) Token: 0x06001666 RID: 5734 RVA: 0x0007BDA2 File Offset: 0x00079FA2
		public override string LocalName
		{
			get
			{
				return this.OwnerDocument.strNonSignificantWhitespaceName;
			}
		}

		/// <summary>Gets the type of the node.</summary>
		/// <returns>For <see langword="XmlWhitespace" /> nodes, the value is <see cref="F:System.Xml.XmlNodeType.Whitespace" />.</returns>
		// Token: 0x1700047B RID: 1147
		// (get) Token: 0x06001667 RID: 5735 RVA: 0x0007BDAF File Offset: 0x00079FAF
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.Whitespace;
			}
		}

		/// <summary>Gets the parent of the current node.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> parent node of the current node.</returns>
		// Token: 0x1700047C RID: 1148
		// (get) Token: 0x06001668 RID: 5736 RVA: 0x0007BDB4 File Offset: 0x00079FB4
		public override XmlNode ParentNode
		{
			get
			{
				XmlNodeType nodeType = this.parentNode.NodeType;
				if (nodeType - XmlNodeType.Text > 1)
				{
					if (nodeType == XmlNodeType.Document)
					{
						return base.ParentNode;
					}
					if (nodeType - XmlNodeType.Whitespace > 1)
					{
						return this.parentNode;
					}
				}
				XmlNode parentNode = this.parentNode.parentNode;
				while (parentNode.IsText)
				{
					parentNode = parentNode.parentNode;
				}
				return parentNode;
			}
		}

		/// <summary>Gets or sets the value of the node.</summary>
		/// <returns>The white space characters found in the node.</returns>
		/// <exception cref="T:System.ArgumentException">Setting <see cref="P:System.Xml.XmlWhitespace.Value" /> to invalid white space characters. </exception>
		// Token: 0x1700047D RID: 1149
		// (get) Token: 0x06001669 RID: 5737 RVA: 0x00072724 File Offset: 0x00070924
		// (set) Token: 0x0600166A RID: 5738 RVA: 0x0007BB5C File Offset: 0x00079D5C
		public override string Value
		{
			get
			{
				return this.Data;
			}
			set
			{
				if (base.CheckOnData(value))
				{
					this.Data = value;
					return;
				}
				throw new ArgumentException(Res.GetString("The string for white space contains an invalid character."));
			}
		}

		/// <summary>Creates a duplicate of this node.</summary>
		/// <param name="deep">
		///       <see langword="true" /> to recursively clone the subtree under the specified node; <see langword="false" /> to clone only the node itself. For white space nodes, the cloned node always includes the data value, regardless of the parameter setting. </param>
		/// <returns>The cloned node.</returns>
		// Token: 0x0600166B RID: 5739 RVA: 0x0007BE0D File Offset: 0x0007A00D
		public override XmlNode CloneNode(bool deep)
		{
			return this.OwnerDocument.CreateWhitespace(this.Data);
		}

		/// <summary>Saves the node to the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="w">The <see cref="T:System.Xml.XmlWriter" /> to which you want to save.</param>
		// Token: 0x0600166C RID: 5740 RVA: 0x0007BE20 File Offset: 0x0007A020
		public override void WriteTo(XmlWriter w)
		{
			w.WriteWhitespace(this.Data);
		}

		/// <summary>Saves all the children of the node to the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="w">The <see cref="T:System.Xml.XmlWriter" /> to which you want to save. </param>
		// Token: 0x0600166D RID: 5741 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteContentTo(XmlWriter w)
		{
		}

		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x0600166E RID: 5742 RVA: 0x0007BE30 File Offset: 0x0007A030
		internal override XPathNodeType XPNodeType
		{
			get
			{
				XPathNodeType result = XPathNodeType.Whitespace;
				base.DecideXPNodeTypeForTextNodes(this, ref result);
				return result;
			}
		}

		// Token: 0x1700047F RID: 1151
		// (get) Token: 0x0600166F RID: 5743 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool IsText
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets the text node that immediately precedes this node.</summary>
		/// <returns>Returns <see cref="T:System.Xml.XmlNode" />.</returns>
		// Token: 0x17000480 RID: 1152
		// (get) Token: 0x06001670 RID: 5744 RVA: 0x000726FD File Offset: 0x000708FD
		public override XmlNode PreviousText
		{
			get
			{
				if (this.parentNode.IsText)
				{
					return this.parentNode;
				}
				return null;
			}
		}
	}
}
