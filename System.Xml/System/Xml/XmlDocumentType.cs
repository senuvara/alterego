﻿using System;
using System.Xml.Schema;

namespace System.Xml
{
	/// <summary>Represents the document type declaration.</summary>
	// Token: 0x02000225 RID: 549
	public class XmlDocumentType : XmlLinkedNode
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlDocumentType" /> class.</summary>
		/// <param name="name">The qualified name; see the <see cref="P:System.Xml.XmlDocumentType.Name" /> property.</param>
		/// <param name="publicId">The public identifier; see the <see cref="P:System.Xml.XmlDocumentType.PublicId" /> property.</param>
		/// <param name="systemId">The system identifier; see the <see cref="P:System.Xml.XmlDocumentType.SystemId" /> property.</param>
		/// <param name="internalSubset">The DTD internal subset; see the <see cref="P:System.Xml.XmlDocumentType.InternalSubset" /> property.</param>
		/// <param name="doc">The parent document.</param>
		// Token: 0x06001458 RID: 5208 RVA: 0x00074CB0 File Offset: 0x00072EB0
		protected internal XmlDocumentType(string name, string publicId, string systemId, string internalSubset, XmlDocument doc) : base(doc)
		{
			this.name = name;
			this.publicId = publicId;
			this.systemId = systemId;
			this.namespaces = true;
			this.internalSubset = internalSubset;
			if (!doc.IsLoading)
			{
				doc.IsLoading = true;
				new XmlLoader().ParseDocumentType(this);
				doc.IsLoading = false;
			}
		}

		/// <summary>Gets the qualified name of the node.</summary>
		/// <returns>For DocumentType nodes, this property returns the name of the document type.</returns>
		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x06001459 RID: 5209 RVA: 0x00074D0D File Offset: 0x00072F0D
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Gets the local name of the node.</summary>
		/// <returns>For DocumentType nodes, this property returns the name of the document type.</returns>
		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x0600145A RID: 5210 RVA: 0x00074D0D File Offset: 0x00072F0D
		public override string LocalName
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Gets the type of the current node.</summary>
		/// <returns>For DocumentType nodes, this value is XmlNodeType.DocumentType.</returns>
		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x0600145B RID: 5211 RVA: 0x00074D15 File Offset: 0x00072F15
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.DocumentType;
			}
		}

		/// <summary>Creates a duplicate of this node.</summary>
		/// <param name="deep">
		///       <see langword="true" /> to recursively clone the subtree under the specified node; <see langword="false" /> to clone only the node itself. For document type nodes, the cloned node always includes the subtree, regardless of the parameter setting. </param>
		/// <returns>The cloned node.</returns>
		// Token: 0x0600145C RID: 5212 RVA: 0x00074D19 File Offset: 0x00072F19
		public override XmlNode CloneNode(bool deep)
		{
			return this.OwnerDocument.CreateDocumentType(this.name, this.publicId, this.systemId, this.internalSubset);
		}

		/// <summary>Gets a value indicating whether the node is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> if the node is read-only; otherwise <see langword="false" />.Because DocumentType nodes are read-only, this property always returns <see langword="true" />.</returns>
		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x0600145D RID: 5213 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets the collection of <see cref="T:System.Xml.XmlEntity" /> nodes declared in the document type declaration.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlNamedNodeMap" /> containing the <see langword="XmlEntity" /> nodes. The returned <see langword="XmlNamedNodeMap" /> is read-only.</returns>
		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x0600145E RID: 5214 RVA: 0x00074D3E File Offset: 0x00072F3E
		public XmlNamedNodeMap Entities
		{
			get
			{
				if (this.entities == null)
				{
					this.entities = new XmlNamedNodeMap(this);
				}
				return this.entities;
			}
		}

		/// <summary>Gets the collection of <see cref="T:System.Xml.XmlNotation" /> nodes present in the document type declaration.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlNamedNodeMap" /> containing the <see langword="XmlNotation" /> nodes. The returned <see langword="XmlNamedNodeMap" /> is read-only.</returns>
		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x0600145F RID: 5215 RVA: 0x00074D5A File Offset: 0x00072F5A
		public XmlNamedNodeMap Notations
		{
			get
			{
				if (this.notations == null)
				{
					this.notations = new XmlNamedNodeMap(this);
				}
				return this.notations;
			}
		}

		/// <summary>Gets the value of the public identifier on the DOCTYPE declaration.</summary>
		/// <returns>The public identifier on the DOCTYPE. If there is no public identifier, <see langword="null" /> is returned.</returns>
		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x06001460 RID: 5216 RVA: 0x00074D76 File Offset: 0x00072F76
		public string PublicId
		{
			get
			{
				return this.publicId;
			}
		}

		/// <summary>Gets the value of the system identifier on the DOCTYPE declaration.</summary>
		/// <returns>The system identifier on the DOCTYPE. If there is no system identifier, <see langword="null" /> is returned.</returns>
		// Token: 0x170003B7 RID: 951
		// (get) Token: 0x06001461 RID: 5217 RVA: 0x00074D7E File Offset: 0x00072F7E
		public string SystemId
		{
			get
			{
				return this.systemId;
			}
		}

		/// <summary>Gets the value of the document type definition (DTD) internal subset on the DOCTYPE declaration.</summary>
		/// <returns>The DTD internal subset on the DOCTYPE. If there is no DTD internal subset, String.Empty is returned.</returns>
		// Token: 0x170003B8 RID: 952
		// (get) Token: 0x06001462 RID: 5218 RVA: 0x00074D86 File Offset: 0x00072F86
		public string InternalSubset
		{
			get
			{
				return this.internalSubset;
			}
		}

		// Token: 0x170003B9 RID: 953
		// (get) Token: 0x06001463 RID: 5219 RVA: 0x00074D8E File Offset: 0x00072F8E
		// (set) Token: 0x06001464 RID: 5220 RVA: 0x00074D96 File Offset: 0x00072F96
		internal bool ParseWithNamespaces
		{
			get
			{
				return this.namespaces;
			}
			set
			{
				this.namespaces = value;
			}
		}

		/// <summary>Saves the node to the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="w">The <see langword="XmlWriter" /> to which you want to save. </param>
		// Token: 0x06001465 RID: 5221 RVA: 0x00074D9F File Offset: 0x00072F9F
		public override void WriteTo(XmlWriter w)
		{
			w.WriteDocType(this.name, this.publicId, this.systemId, this.internalSubset);
		}

		/// <summary>Saves all the children of the node to the specified <see cref="T:System.Xml.XmlWriter" />. For <see langword="XmlDocumentType" /> nodes, this method has no effect.</summary>
		/// <param name="w">The <see langword="XmlWriter" /> to which you want to save. </param>
		// Token: 0x06001466 RID: 5222 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteContentTo(XmlWriter w)
		{
		}

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x06001467 RID: 5223 RVA: 0x00074DBF File Offset: 0x00072FBF
		// (set) Token: 0x06001468 RID: 5224 RVA: 0x00074DC7 File Offset: 0x00072FC7
		internal SchemaInfo DtdSchemaInfo
		{
			get
			{
				return this.schemaInfo;
			}
			set
			{
				this.schemaInfo = value;
			}
		}

		// Token: 0x04000DB5 RID: 3509
		private string name;

		// Token: 0x04000DB6 RID: 3510
		private string publicId;

		// Token: 0x04000DB7 RID: 3511
		private string systemId;

		// Token: 0x04000DB8 RID: 3512
		private string internalSubset;

		// Token: 0x04000DB9 RID: 3513
		private bool namespaces;

		// Token: 0x04000DBA RID: 3514
		private XmlNamedNodeMap entities;

		// Token: 0x04000DBB RID: 3515
		private XmlNamedNodeMap notations;

		// Token: 0x04000DBC RID: 3516
		private SchemaInfo schemaInfo;
	}
}
