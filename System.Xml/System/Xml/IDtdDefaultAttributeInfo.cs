﻿using System;

namespace System.Xml
{
	// Token: 0x020000A0 RID: 160
	internal interface IDtdDefaultAttributeInfo : IDtdAttributeInfo
	{
		// Token: 0x17000106 RID: 262
		// (get) Token: 0x06000537 RID: 1335
		string DefaultValueExpanded { get; }

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x06000538 RID: 1336
		object DefaultValueTyped { get; }

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x06000539 RID: 1337
		int ValueLineNumber { get; }

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x0600053A RID: 1338
		int ValueLinePosition { get; }
	}
}
