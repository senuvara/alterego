﻿using System;

namespace System.Xml
{
	// Token: 0x020000A9 RID: 169
	internal abstract class IncrementalReadDecoder
	{
		// Token: 0x17000128 RID: 296
		// (get) Token: 0x06000578 RID: 1400
		internal abstract int DecodedCount { get; }

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000579 RID: 1401
		internal abstract bool IsFull { get; }

		// Token: 0x0600057A RID: 1402
		internal abstract void SetNextOutputBuffer(Array array, int offset, int len);

		// Token: 0x0600057B RID: 1403
		internal abstract int Decode(char[] chars, int startPos, int len);

		// Token: 0x0600057C RID: 1404
		internal abstract int Decode(string str, int startPos, int len);

		// Token: 0x0600057D RID: 1405
		internal abstract void Reset();

		// Token: 0x0600057E RID: 1406 RVA: 0x00002103 File Offset: 0x00000303
		protected IncrementalReadDecoder()
		{
		}
	}
}
