﻿using System;
using System.Xml.Schema;

namespace System.Xml
{
	// Token: 0x020001F2 RID: 498
	internal class AttributePSVIInfo
	{
		// Token: 0x060011A7 RID: 4519 RVA: 0x00067BB2 File Offset: 0x00065DB2
		internal AttributePSVIInfo()
		{
			this.attributeSchemaInfo = new XmlSchemaInfo();
		}

		// Token: 0x060011A8 RID: 4520 RVA: 0x00067BC5 File Offset: 0x00065DC5
		internal void Reset()
		{
			this.typedAttributeValue = null;
			this.localName = string.Empty;
			this.namespaceUri = string.Empty;
			this.attributeSchemaInfo.Clear();
		}

		// Token: 0x04000C73 RID: 3187
		internal string localName;

		// Token: 0x04000C74 RID: 3188
		internal string namespaceUri;

		// Token: 0x04000C75 RID: 3189
		internal object typedAttributeValue;

		// Token: 0x04000C76 RID: 3190
		internal XmlSchemaInfo attributeSchemaInfo;
	}
}
