﻿using System;

namespace System.Xml
{
	// Token: 0x02000250 RID: 592
	internal static class Ref
	{
		// Token: 0x0600169C RID: 5788 RVA: 0x0007C45B File Offset: 0x0007A65B
		public static bool Equal(string strA, string strB)
		{
			return strA == strB;
		}

		// Token: 0x0600169D RID: 5789 RVA: 0x000030EC File Offset: 0x000012EC
		public new static void Equals(object objA, object objB)
		{
		}
	}
}
