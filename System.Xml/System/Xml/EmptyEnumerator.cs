﻿using System;
using System.Collections;

namespace System.Xml
{
	// Token: 0x02000245 RID: 581
	internal sealed class EmptyEnumerator : IEnumerator
	{
		// Token: 0x06001671 RID: 5745 RVA: 0x000020CD File Offset: 0x000002CD
		bool IEnumerator.MoveNext()
		{
			return false;
		}

		// Token: 0x06001672 RID: 5746 RVA: 0x000030EC File Offset: 0x000012EC
		void IEnumerator.Reset()
		{
		}

		// Token: 0x17000481 RID: 1153
		// (get) Token: 0x06001673 RID: 5747 RVA: 0x00016D08 File Offset: 0x00014F08
		object IEnumerator.Current
		{
			get
			{
				throw new InvalidOperationException(Res.GetString("Operation is not valid due to the current state of the object."));
			}
		}

		// Token: 0x06001674 RID: 5748 RVA: 0x00002103 File Offset: 0x00000303
		public EmptyEnumerator()
		{
		}
	}
}
