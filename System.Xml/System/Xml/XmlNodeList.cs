﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace System.Xml
{
	/// <summary>Represents an ordered collection of nodes.</summary>
	// Token: 0x0200023B RID: 571
	public abstract class XmlNodeList : IEnumerable, IDisposable
	{
		/// <summary>Retrieves a node at the given index.</summary>
		/// <param name="index">The zero-based index into the list of nodes.</param>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> with the specified index in the collection. If <paramref name="index" /> is greater than or equal to the number of nodes in the list, this returns <see langword="null" />.</returns>
		// Token: 0x060015AA RID: 5546
		public abstract XmlNode Item(int index);

		/// <summary>Gets the number of nodes in the <see langword="XmlNodeList" />.</summary>
		/// <returns>The number of nodes in the <see langword="XmlNodeList" />.</returns>
		// Token: 0x1700042A RID: 1066
		// (get) Token: 0x060015AB RID: 5547
		public abstract int Count { get; }

		/// <summary>Gets an enumerator that iterates through the collection of nodes.</summary>
		/// <returns>An enumerator used to iterate through the collection of nodes.</returns>
		// Token: 0x060015AC RID: 5548
		public abstract IEnumerator GetEnumerator();

		/// <summary>Gets a node at the given index.</summary>
		/// <param name="i">The zero-based index into the list of nodes.</param>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> with the specified index in the collection. If index is greater than or equal to the number of nodes in the list, this returns <see langword="null" />.</returns>
		// Token: 0x1700042B RID: 1067
		[IndexerName("ItemOf")]
		public virtual XmlNode this[int i]
		{
			get
			{
				return this.Item(i);
			}
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Xml.XmlNodeList" /> class.</summary>
		// Token: 0x060015AE RID: 5550 RVA: 0x00079830 File Offset: 0x00077A30
		void IDisposable.Dispose()
		{
			this.PrivateDisposeNodeList();
		}

		/// <summary>Disposes resources in the node list privately.</summary>
		// Token: 0x060015AF RID: 5551 RVA: 0x000030EC File Offset: 0x000012EC
		protected virtual void PrivateDisposeNodeList()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlNodeList" /> class.</summary>
		// Token: 0x060015B0 RID: 5552 RVA: 0x00002103 File Offset: 0x00000303
		protected XmlNodeList()
		{
		}
	}
}
