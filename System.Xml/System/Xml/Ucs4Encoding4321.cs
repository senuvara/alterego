﻿using System;

namespace System.Xml
{
	// Token: 0x0200029A RID: 666
	internal class Ucs4Encoding4321 : Ucs4Encoding
	{
		// Token: 0x0600189F RID: 6303 RVA: 0x0008E856 File Offset: 0x0008CA56
		public Ucs4Encoding4321()
		{
			this.ucs4Decoder = new Ucs4Decoder4321();
		}

		// Token: 0x170004A2 RID: 1186
		// (get) Token: 0x060018A0 RID: 6304 RVA: 0x0008E869 File Offset: 0x0008CA69
		public override string EncodingName
		{
			get
			{
				return "ucs-4";
			}
		}

		// Token: 0x060018A1 RID: 6305 RVA: 0x0008E870 File Offset: 0x0008CA70
		public override byte[] GetPreamble()
		{
			byte[] array = new byte[4];
			array[0] = byte.MaxValue;
			array[1] = 254;
			return array;
		}
	}
}
