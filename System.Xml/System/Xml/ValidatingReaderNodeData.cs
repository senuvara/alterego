﻿using System;

namespace System.Xml
{
	// Token: 0x020000C5 RID: 197
	internal class ValidatingReaderNodeData
	{
		// Token: 0x06000630 RID: 1584 RVA: 0x0001B1AF File Offset: 0x000193AF
		public ValidatingReaderNodeData()
		{
			this.Clear(XmlNodeType.None);
		}

		// Token: 0x06000631 RID: 1585 RVA: 0x0001B1BE File Offset: 0x000193BE
		public ValidatingReaderNodeData(XmlNodeType nodeType)
		{
			this.Clear(nodeType);
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000632 RID: 1586 RVA: 0x0001B1CD File Offset: 0x000193CD
		// (set) Token: 0x06000633 RID: 1587 RVA: 0x0001B1D5 File Offset: 0x000193D5
		public string LocalName
		{
			get
			{
				return this.localName;
			}
			set
			{
				this.localName = value;
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06000634 RID: 1588 RVA: 0x0001B1DE File Offset: 0x000193DE
		// (set) Token: 0x06000635 RID: 1589 RVA: 0x0001B1E6 File Offset: 0x000193E6
		public string Namespace
		{
			get
			{
				return this.namespaceUri;
			}
			set
			{
				this.namespaceUri = value;
			}
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06000636 RID: 1590 RVA: 0x0001B1EF File Offset: 0x000193EF
		// (set) Token: 0x06000637 RID: 1591 RVA: 0x0001B1F7 File Offset: 0x000193F7
		public string Prefix
		{
			get
			{
				return this.prefix;
			}
			set
			{
				this.prefix = value;
			}
		}

		// Token: 0x06000638 RID: 1592 RVA: 0x0001B200 File Offset: 0x00019400
		public string GetAtomizedNameWPrefix(XmlNameTable nameTable)
		{
			if (this.nameWPrefix == null)
			{
				if (this.prefix.Length == 0)
				{
					this.nameWPrefix = this.localName;
				}
				else
				{
					this.nameWPrefix = nameTable.Add(this.prefix + ":" + this.localName);
				}
			}
			return this.nameWPrefix;
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000639 RID: 1593 RVA: 0x0001B258 File Offset: 0x00019458
		// (set) Token: 0x0600063A RID: 1594 RVA: 0x0001B260 File Offset: 0x00019460
		public int Depth
		{
			get
			{
				return this.depth;
			}
			set
			{
				this.depth = value;
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x0600063B RID: 1595 RVA: 0x0001B269 File Offset: 0x00019469
		// (set) Token: 0x0600063C RID: 1596 RVA: 0x0001B271 File Offset: 0x00019471
		public string RawValue
		{
			get
			{
				return this.rawValue;
			}
			set
			{
				this.rawValue = value;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x0600063D RID: 1597 RVA: 0x0001B27A File Offset: 0x0001947A
		// (set) Token: 0x0600063E RID: 1598 RVA: 0x0001B282 File Offset: 0x00019482
		public string OriginalStringValue
		{
			get
			{
				return this.originalStringValue;
			}
			set
			{
				this.originalStringValue = value;
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x0600063F RID: 1599 RVA: 0x0001B28B File Offset: 0x0001948B
		// (set) Token: 0x06000640 RID: 1600 RVA: 0x0001B293 File Offset: 0x00019493
		public XmlNodeType NodeType
		{
			get
			{
				return this.nodeType;
			}
			set
			{
				this.nodeType = value;
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000641 RID: 1601 RVA: 0x0001B29C File Offset: 0x0001949C
		// (set) Token: 0x06000642 RID: 1602 RVA: 0x0001B2A4 File Offset: 0x000194A4
		public AttributePSVIInfo AttInfo
		{
			get
			{
				return this.attributePSVIInfo;
			}
			set
			{
				this.attributePSVIInfo = value;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000643 RID: 1603 RVA: 0x0001B2AD File Offset: 0x000194AD
		public int LineNumber
		{
			get
			{
				return this.lineNo;
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000644 RID: 1604 RVA: 0x0001B2B5 File Offset: 0x000194B5
		public int LinePosition
		{
			get
			{
				return this.linePos;
			}
		}

		// Token: 0x06000645 RID: 1605 RVA: 0x0001B2C0 File Offset: 0x000194C0
		internal void Clear(XmlNodeType nodeType)
		{
			this.nodeType = nodeType;
			this.localName = string.Empty;
			this.prefix = string.Empty;
			this.namespaceUri = string.Empty;
			this.rawValue = string.Empty;
			if (this.attributePSVIInfo != null)
			{
				this.attributePSVIInfo.Reset();
			}
			this.nameWPrefix = null;
			this.lineNo = 0;
			this.linePos = 0;
		}

		// Token: 0x06000646 RID: 1606 RVA: 0x0001B328 File Offset: 0x00019528
		internal void ClearName()
		{
			this.localName = string.Empty;
			this.prefix = string.Empty;
			this.namespaceUri = string.Empty;
		}

		// Token: 0x06000647 RID: 1607 RVA: 0x0001B34B File Offset: 0x0001954B
		internal void SetLineInfo(int lineNo, int linePos)
		{
			this.lineNo = lineNo;
			this.linePos = linePos;
		}

		// Token: 0x06000648 RID: 1608 RVA: 0x0001B35B File Offset: 0x0001955B
		internal void SetLineInfo(IXmlLineInfo lineInfo)
		{
			if (lineInfo != null)
			{
				this.lineNo = lineInfo.LineNumber;
				this.linePos = lineInfo.LinePosition;
			}
		}

		// Token: 0x06000649 RID: 1609 RVA: 0x0001B378 File Offset: 0x00019578
		internal void SetItemData(string localName, string prefix, string ns, string value)
		{
			this.localName = localName;
			this.prefix = prefix;
			this.namespaceUri = ns;
			this.rawValue = value;
		}

		// Token: 0x0600064A RID: 1610 RVA: 0x0001B397 File Offset: 0x00019597
		internal void SetItemData(string localName, string prefix, string ns, int depth)
		{
			this.localName = localName;
			this.prefix = prefix;
			this.namespaceUri = ns;
			this.depth = depth;
			this.rawValue = string.Empty;
		}

		// Token: 0x0600064B RID: 1611 RVA: 0x0001B3C1 File Offset: 0x000195C1
		internal void SetItemData(string value)
		{
			this.SetItemData(value, value);
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x0001B3CB File Offset: 0x000195CB
		internal void SetItemData(string value, string originalStringValue)
		{
			this.rawValue = value;
			this.originalStringValue = originalStringValue;
		}

		// Token: 0x040003C2 RID: 962
		private string localName;

		// Token: 0x040003C3 RID: 963
		private string namespaceUri;

		// Token: 0x040003C4 RID: 964
		private string prefix;

		// Token: 0x040003C5 RID: 965
		private string nameWPrefix;

		// Token: 0x040003C6 RID: 966
		private string rawValue;

		// Token: 0x040003C7 RID: 967
		private string originalStringValue;

		// Token: 0x040003C8 RID: 968
		private int depth;

		// Token: 0x040003C9 RID: 969
		private AttributePSVIInfo attributePSVIInfo;

		// Token: 0x040003CA RID: 970
		private XmlNodeType nodeType;

		// Token: 0x040003CB RID: 971
		private int lineNo;

		// Token: 0x040003CC RID: 972
		private int linePos;
	}
}
