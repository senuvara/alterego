﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x02000112 RID: 274
	internal sealed class XmlSubtreeReader : XmlWrappingReader, IXmlLineInfo, IXmlNamespaceResolver
	{
		// Token: 0x06000A30 RID: 2608 RVA: 0x0002D6B8 File Offset: 0x0002B8B8
		internal XmlSubtreeReader(XmlReader reader) : base(reader)
		{
			this.initialDepth = reader.Depth;
			this.state = XmlSubtreeReader.State.Initial;
			this.nsManager = new XmlNamespaceManager(reader.NameTable);
			this.xmlns = reader.NameTable.Add("xmlns");
			this.xmlnsUri = reader.NameTable.Add("http://www.w3.org/2000/xmlns/");
			this.tmpNode = new XmlSubtreeReader.NodeData();
			this.tmpNode.Set(XmlNodeType.None, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
			this.SetCurrentNode(this.tmpNode);
		}

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x06000A31 RID: 2609 RVA: 0x0002D766 File Offset: 0x0002B966
		public override XmlNodeType NodeType
		{
			get
			{
				if (!this.useCurNode)
				{
					return this.reader.NodeType;
				}
				return this.curNode.type;
			}
		}

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06000A32 RID: 2610 RVA: 0x0002D787 File Offset: 0x0002B987
		public override string Name
		{
			get
			{
				if (!this.useCurNode)
				{
					return this.reader.Name;
				}
				return this.curNode.name;
			}
		}

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000A33 RID: 2611 RVA: 0x0002D7A8 File Offset: 0x0002B9A8
		public override string LocalName
		{
			get
			{
				if (!this.useCurNode)
				{
					return this.reader.LocalName;
				}
				return this.curNode.localName;
			}
		}

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000A34 RID: 2612 RVA: 0x0002D7C9 File Offset: 0x0002B9C9
		public override string NamespaceURI
		{
			get
			{
				if (!this.useCurNode)
				{
					return this.reader.NamespaceURI;
				}
				return this.curNode.namespaceUri;
			}
		}

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000A35 RID: 2613 RVA: 0x0002D7EA File Offset: 0x0002B9EA
		public override string Prefix
		{
			get
			{
				if (!this.useCurNode)
				{
					return this.reader.Prefix;
				}
				return this.curNode.prefix;
			}
		}

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000A36 RID: 2614 RVA: 0x0002D80B File Offset: 0x0002BA0B
		public override string Value
		{
			get
			{
				if (!this.useCurNode)
				{
					return this.reader.Value;
				}
				return this.curNode.value;
			}
		}

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06000A37 RID: 2615 RVA: 0x0002D82C File Offset: 0x0002BA2C
		public override int Depth
		{
			get
			{
				int num = this.reader.Depth - this.initialDepth;
				if (this.curNsAttr != -1)
				{
					if (this.curNode.type == XmlNodeType.Text)
					{
						num += 2;
					}
					else
					{
						num++;
					}
				}
				return num;
			}
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x06000A38 RID: 2616 RVA: 0x0002D86E File Offset: 0x0002BA6E
		public override string BaseURI
		{
			get
			{
				return this.reader.BaseURI;
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06000A39 RID: 2617 RVA: 0x0002D87B File Offset: 0x0002BA7B
		public override bool IsEmptyElement
		{
			get
			{
				return this.reader.IsEmptyElement;
			}
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x06000A3A RID: 2618 RVA: 0x0002D888 File Offset: 0x0002BA88
		public override bool EOF
		{
			get
			{
				return this.state == XmlSubtreeReader.State.EndOfFile || this.state == XmlSubtreeReader.State.Closed;
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x06000A3B RID: 2619 RVA: 0x0002D89E File Offset: 0x0002BA9E
		public override ReadState ReadState
		{
			get
			{
				if (this.reader.ReadState == ReadState.Error)
				{
					return ReadState.Error;
				}
				if (this.state <= XmlSubtreeReader.State.Closed)
				{
					return (ReadState)this.state;
				}
				return ReadState.Interactive;
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x06000A3C RID: 2620 RVA: 0x0002D8C1 File Offset: 0x0002BAC1
		public override XmlNameTable NameTable
		{
			get
			{
				return this.reader.NameTable;
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x06000A3D RID: 2621 RVA: 0x0002D8CE File Offset: 0x0002BACE
		public override int AttributeCount
		{
			get
			{
				if (!this.InAttributeActiveState)
				{
					return 0;
				}
				return this.reader.AttributeCount + this.nsAttrCount;
			}
		}

		// Token: 0x06000A3E RID: 2622 RVA: 0x0002D8EC File Offset: 0x0002BAEC
		public override string GetAttribute(string name)
		{
			if (!this.InAttributeActiveState)
			{
				return null;
			}
			string attribute = this.reader.GetAttribute(name);
			if (attribute != null)
			{
				return attribute;
			}
			for (int i = 0; i < this.nsAttrCount; i++)
			{
				if (name == this.nsAttributes[i].name)
				{
					return this.nsAttributes[i].value;
				}
			}
			return null;
		}

		// Token: 0x06000A3F RID: 2623 RVA: 0x0002D94C File Offset: 0x0002BB4C
		public override string GetAttribute(string name, string namespaceURI)
		{
			if (!this.InAttributeActiveState)
			{
				return null;
			}
			string attribute = this.reader.GetAttribute(name, namespaceURI);
			if (attribute != null)
			{
				return attribute;
			}
			for (int i = 0; i < this.nsAttrCount; i++)
			{
				if (name == this.nsAttributes[i].localName && namespaceURI == this.xmlnsUri)
				{
					return this.nsAttributes[i].value;
				}
			}
			return null;
		}

		// Token: 0x06000A40 RID: 2624 RVA: 0x0002D9BC File Offset: 0x0002BBBC
		public override string GetAttribute(int i)
		{
			if (!this.InAttributeActiveState)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			int attributeCount = this.reader.AttributeCount;
			if (i < attributeCount)
			{
				return this.reader.GetAttribute(i);
			}
			if (i - attributeCount < this.nsAttrCount)
			{
				return this.nsAttributes[i - attributeCount].value;
			}
			throw new ArgumentOutOfRangeException("i");
		}

		// Token: 0x06000A41 RID: 2625 RVA: 0x0002DA20 File Offset: 0x0002BC20
		public override bool MoveToAttribute(string name)
		{
			if (!this.InAttributeActiveState)
			{
				return false;
			}
			if (this.reader.MoveToAttribute(name))
			{
				this.curNsAttr = -1;
				this.useCurNode = false;
				return true;
			}
			for (int i = 0; i < this.nsAttrCount; i++)
			{
				if (name == this.nsAttributes[i].name)
				{
					this.MoveToNsAttribute(i);
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000A42 RID: 2626 RVA: 0x0002DA88 File Offset: 0x0002BC88
		public override bool MoveToAttribute(string name, string ns)
		{
			if (!this.InAttributeActiveState)
			{
				return false;
			}
			if (this.reader.MoveToAttribute(name, ns))
			{
				this.curNsAttr = -1;
				this.useCurNode = false;
				return true;
			}
			for (int i = 0; i < this.nsAttrCount; i++)
			{
				if (name == this.nsAttributes[i].localName && ns == this.xmlnsUri)
				{
					this.MoveToNsAttribute(i);
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000A43 RID: 2627 RVA: 0x0002DAFC File Offset: 0x0002BCFC
		public override void MoveToAttribute(int i)
		{
			if (!this.InAttributeActiveState)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			int attributeCount = this.reader.AttributeCount;
			if (i < attributeCount)
			{
				this.reader.MoveToAttribute(i);
				this.curNsAttr = -1;
				this.useCurNode = false;
				return;
			}
			if (i - attributeCount < this.nsAttrCount)
			{
				this.MoveToNsAttribute(i - attributeCount);
				return;
			}
			throw new ArgumentOutOfRangeException("i");
		}

		// Token: 0x06000A44 RID: 2628 RVA: 0x0002DB66 File Offset: 0x0002BD66
		public override bool MoveToFirstAttribute()
		{
			if (!this.InAttributeActiveState)
			{
				return false;
			}
			if (this.reader.MoveToFirstAttribute())
			{
				this.useCurNode = false;
				return true;
			}
			if (this.nsAttrCount > 0)
			{
				this.MoveToNsAttribute(0);
				return true;
			}
			return false;
		}

		// Token: 0x06000A45 RID: 2629 RVA: 0x0002DB9C File Offset: 0x0002BD9C
		public override bool MoveToNextAttribute()
		{
			if (!this.InAttributeActiveState)
			{
				return false;
			}
			if (this.curNsAttr == -1 && this.reader.MoveToNextAttribute())
			{
				return true;
			}
			if (this.curNsAttr + 1 < this.nsAttrCount)
			{
				this.MoveToNsAttribute(this.curNsAttr + 1);
				return true;
			}
			return false;
		}

		// Token: 0x06000A46 RID: 2630 RVA: 0x0002DBEC File Offset: 0x0002BDEC
		public override bool MoveToElement()
		{
			if (!this.InAttributeActiveState)
			{
				return false;
			}
			this.useCurNode = false;
			if (this.curNsAttr >= 0)
			{
				this.curNsAttr = -1;
				return true;
			}
			return this.reader.MoveToElement();
		}

		// Token: 0x06000A47 RID: 2631 RVA: 0x0002DC1C File Offset: 0x0002BE1C
		public override bool ReadAttributeValue()
		{
			if (!this.InAttributeActiveState)
			{
				return false;
			}
			if (this.curNsAttr == -1)
			{
				return this.reader.ReadAttributeValue();
			}
			if (this.curNode.type == XmlNodeType.Text)
			{
				return false;
			}
			this.tmpNode.type = XmlNodeType.Text;
			this.tmpNode.value = this.curNode.value;
			this.SetCurrentNode(this.tmpNode);
			return true;
		}

		// Token: 0x06000A48 RID: 2632 RVA: 0x0002DC88 File Offset: 0x0002BE88
		public override bool Read()
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
				this.useCurNode = false;
				this.state = XmlSubtreeReader.State.Interactive;
				this.ProcessNamespaces();
				return true;
			case XmlSubtreeReader.State.Interactive:
				break;
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return false;
			case XmlSubtreeReader.State.PopNamespaceScope:
				this.nsManager.PopScope();
				goto IL_E5;
			case XmlSubtreeReader.State.ClearNsAttributes:
				goto IL_E5;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
				return this.FinishReadElementContentAsBinary() && this.Read();
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				return this.FinishReadContentAsBinary() && this.Read();
			default:
				return false;
			}
			IL_54:
			this.curNsAttr = -1;
			this.useCurNode = false;
			this.reader.MoveToElement();
			if (this.reader.Depth == this.initialDepth && (this.reader.NodeType == XmlNodeType.EndElement || (this.reader.NodeType == XmlNodeType.Element && this.reader.IsEmptyElement)))
			{
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
				return false;
			}
			if (this.reader.Read())
			{
				this.ProcessNamespaces();
				return true;
			}
			this.SetEmptyNode();
			return false;
			IL_E5:
			this.nsAttrCount = 0;
			this.state = XmlSubtreeReader.State.Interactive;
			goto IL_54;
		}

		// Token: 0x06000A49 RID: 2633 RVA: 0x0002DDB0 File Offset: 0x0002BFB0
		public override void Close()
		{
			if (this.state == XmlSubtreeReader.State.Closed)
			{
				return;
			}
			try
			{
				if (this.state != XmlSubtreeReader.State.EndOfFile)
				{
					this.reader.MoveToElement();
					if (this.reader.Depth == this.initialDepth && this.reader.NodeType == XmlNodeType.Element && !this.reader.IsEmptyElement)
					{
						this.reader.Read();
					}
					while (this.reader.Depth > this.initialDepth && this.reader.Read())
					{
					}
				}
			}
			catch
			{
			}
			finally
			{
				this.curNsAttr = -1;
				this.useCurNode = false;
				this.state = XmlSubtreeReader.State.Closed;
				this.SetEmptyNode();
			}
		}

		// Token: 0x06000A4A RID: 2634 RVA: 0x0002DE74 File Offset: 0x0002C074
		public override void Skip()
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
				this.Read();
				return;
			case XmlSubtreeReader.State.Interactive:
				break;
			case XmlSubtreeReader.State.Error:
				return;
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return;
			case XmlSubtreeReader.State.PopNamespaceScope:
				this.nsManager.PopScope();
				goto IL_11A;
			case XmlSubtreeReader.State.ClearNsAttributes:
				goto IL_11A;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
				if (this.FinishReadElementContentAsBinary())
				{
					this.Skip();
					return;
				}
				return;
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				if (this.FinishReadContentAsBinary())
				{
					this.Skip();
					return;
				}
				return;
			default:
				return;
			}
			IL_42:
			this.curNsAttr = -1;
			this.useCurNode = false;
			this.reader.MoveToElement();
			if (this.reader.Depth == this.initialDepth)
			{
				if (this.reader.NodeType == XmlNodeType.Element && !this.reader.IsEmptyElement && this.reader.Read())
				{
					while (this.reader.NodeType != XmlNodeType.EndElement && this.reader.Depth > this.initialDepth)
					{
						this.reader.Skip();
					}
				}
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
				return;
			}
			if (this.reader.NodeType == XmlNodeType.Element && !this.reader.IsEmptyElement)
			{
				this.nsManager.PopScope();
			}
			this.reader.Skip();
			this.ProcessNamespaces();
			return;
			IL_11A:
			this.nsAttrCount = 0;
			this.state = XmlSubtreeReader.State.Interactive;
			goto IL_42;
		}

		// Token: 0x06000A4B RID: 2635 RVA: 0x0002DFD0 File Offset: 0x0002C1D0
		public override object ReadContentAsObject()
		{
			object result;
			try
			{
				this.InitReadContentAsType("ReadContentAsObject");
				object obj = this.reader.ReadContentAsObject();
				this.FinishReadContentAsType();
				result = obj;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A4C RID: 2636 RVA: 0x0002E018 File Offset: 0x0002C218
		public override bool ReadContentAsBoolean()
		{
			bool result;
			try
			{
				this.InitReadContentAsType("ReadContentAsBoolean");
				bool flag = this.reader.ReadContentAsBoolean();
				this.FinishReadContentAsType();
				result = flag;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A4D RID: 2637 RVA: 0x0002E060 File Offset: 0x0002C260
		public override DateTime ReadContentAsDateTime()
		{
			DateTime result;
			try
			{
				this.InitReadContentAsType("ReadContentAsDateTime");
				DateTime dateTime = this.reader.ReadContentAsDateTime();
				this.FinishReadContentAsType();
				result = dateTime;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A4E RID: 2638 RVA: 0x0002E0A8 File Offset: 0x0002C2A8
		public override double ReadContentAsDouble()
		{
			double result;
			try
			{
				this.InitReadContentAsType("ReadContentAsDouble");
				double num = this.reader.ReadContentAsDouble();
				this.FinishReadContentAsType();
				result = num;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A4F RID: 2639 RVA: 0x0002E0F0 File Offset: 0x0002C2F0
		public override float ReadContentAsFloat()
		{
			float result;
			try
			{
				this.InitReadContentAsType("ReadContentAsFloat");
				float num = this.reader.ReadContentAsFloat();
				this.FinishReadContentAsType();
				result = num;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A50 RID: 2640 RVA: 0x0002E138 File Offset: 0x0002C338
		public override decimal ReadContentAsDecimal()
		{
			decimal result;
			try
			{
				this.InitReadContentAsType("ReadContentAsDecimal");
				decimal num = this.reader.ReadContentAsDecimal();
				this.FinishReadContentAsType();
				result = num;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A51 RID: 2641 RVA: 0x0002E180 File Offset: 0x0002C380
		public override int ReadContentAsInt()
		{
			int result;
			try
			{
				this.InitReadContentAsType("ReadContentAsInt");
				int num = this.reader.ReadContentAsInt();
				this.FinishReadContentAsType();
				result = num;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A52 RID: 2642 RVA: 0x0002E1C8 File Offset: 0x0002C3C8
		public override long ReadContentAsLong()
		{
			long result;
			try
			{
				this.InitReadContentAsType("ReadContentAsLong");
				long num = this.reader.ReadContentAsLong();
				this.FinishReadContentAsType();
				result = num;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A53 RID: 2643 RVA: 0x0002E210 File Offset: 0x0002C410
		public override string ReadContentAsString()
		{
			string result;
			try
			{
				this.InitReadContentAsType("ReadContentAsString");
				string text = this.reader.ReadContentAsString();
				this.FinishReadContentAsType();
				result = text;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A54 RID: 2644 RVA: 0x0002E258 File Offset: 0x0002C458
		public override object ReadContentAs(Type returnType, IXmlNamespaceResolver namespaceResolver)
		{
			object result;
			try
			{
				this.InitReadContentAsType("ReadContentAs");
				object obj = this.reader.ReadContentAs(returnType, namespaceResolver);
				this.FinishReadContentAsType();
				result = obj;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000A55 RID: 2645 RVA: 0x0002E2A4 File Offset: 0x0002C4A4
		public override bool CanReadBinaryContent
		{
			get
			{
				return this.reader.CanReadBinaryContent;
			}
		}

		// Token: 0x06000A56 RID: 2646 RVA: 0x0002E2B4 File Offset: 0x0002C4B4
		public override int ReadContentAsBase64(byte[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
				this.state = XmlSubtreeReader.State.ReadContentAsBase64;
				break;
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
			{
				XmlNodeType nodeType = this.NodeType;
				switch (nodeType)
				{
				case XmlNodeType.Element:
					throw base.CreateReadContentAsException("ReadContentAsBase64");
				case XmlNodeType.Attribute:
					if (this.curNsAttr != -1 && this.reader.CanReadBinaryContent)
					{
						this.CheckBuffer(buffer, index, count);
						if (count == 0)
						{
							return 0;
						}
						if (this.nsIncReadOffset == 0)
						{
							if (this.binDecoder != null && this.binDecoder is Base64Decoder)
							{
								this.binDecoder.Reset();
							}
							else
							{
								this.binDecoder = new Base64Decoder();
							}
						}
						if (this.nsIncReadOffset == this.curNode.value.Length)
						{
							return 0;
						}
						this.binDecoder.SetNextOutputBuffer(buffer, index, count);
						this.nsIncReadOffset += this.binDecoder.Decode(this.curNode.value, this.nsIncReadOffset, this.curNode.value.Length - this.nsIncReadOffset);
						return this.binDecoder.DecodedCount;
					}
					break;
				case XmlNodeType.Text:
					break;
				default:
					if (nodeType != XmlNodeType.EndElement)
					{
						return 0;
					}
					return 0;
				}
				return this.reader.ReadContentAsBase64(buffer, index, count);
			}
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case XmlSubtreeReader.State.ReadContentAsBase64:
				break;
			default:
				return 0;
			}
			int num = this.reader.ReadContentAsBase64(buffer, index, count);
			if (num == 0)
			{
				this.state = XmlSubtreeReader.State.Interactive;
				this.ProcessNamespaces();
			}
			return num;
		}

		// Token: 0x06000A57 RID: 2647 RVA: 0x0002E450 File Offset: 0x0002C650
		public override int ReadElementContentAsBase64(byte[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
				if (!this.InitReadElementContentAsBinary(XmlSubtreeReader.State.ReadElementContentAsBase64))
				{
					return 0;
				}
				break;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
				break;
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			default:
				return 0;
			}
			int num = this.reader.ReadContentAsBase64(buffer, index, count);
			if (num > 0 || count == 0)
			{
				return num;
			}
			if (this.NodeType != XmlNodeType.EndElement)
			{
				throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
			}
			this.state = XmlSubtreeReader.State.Interactive;
			this.ProcessNamespaces();
			if (this.reader.Depth == this.initialDepth)
			{
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
			}
			else
			{
				this.Read();
			}
			return 0;
		}

		// Token: 0x06000A58 RID: 2648 RVA: 0x0002E544 File Offset: 0x0002C744
		public override int ReadContentAsBinHex(byte[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
				this.state = XmlSubtreeReader.State.ReadContentAsBinHex;
				break;
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
			{
				XmlNodeType nodeType = this.NodeType;
				switch (nodeType)
				{
				case XmlNodeType.Element:
					throw base.CreateReadContentAsException("ReadContentAsBinHex");
				case XmlNodeType.Attribute:
					if (this.curNsAttr != -1 && this.reader.CanReadBinaryContent)
					{
						this.CheckBuffer(buffer, index, count);
						if (count == 0)
						{
							return 0;
						}
						if (this.nsIncReadOffset == 0)
						{
							if (this.binDecoder != null && this.binDecoder is BinHexDecoder)
							{
								this.binDecoder.Reset();
							}
							else
							{
								this.binDecoder = new BinHexDecoder();
							}
						}
						if (this.nsIncReadOffset == this.curNode.value.Length)
						{
							return 0;
						}
						this.binDecoder.SetNextOutputBuffer(buffer, index, count);
						this.nsIncReadOffset += this.binDecoder.Decode(this.curNode.value, this.nsIncReadOffset, this.curNode.value.Length - this.nsIncReadOffset);
						return this.binDecoder.DecodedCount;
					}
					break;
				case XmlNodeType.Text:
					break;
				default:
					if (nodeType != XmlNodeType.EndElement)
					{
						return 0;
					}
					return 0;
				}
				return this.reader.ReadContentAsBinHex(buffer, index, count);
			}
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBase64:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				break;
			default:
				return 0;
			}
			int num = this.reader.ReadContentAsBinHex(buffer, index, count);
			if (num == 0)
			{
				this.state = XmlSubtreeReader.State.Interactive;
				this.ProcessNamespaces();
			}
			return num;
		}

		// Token: 0x06000A59 RID: 2649 RVA: 0x0002E6E0 File Offset: 0x0002C8E0
		public override int ReadElementContentAsBinHex(byte[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
				if (!this.InitReadElementContentAsBinary(XmlSubtreeReader.State.ReadElementContentAsBinHex))
				{
					return 0;
				}
				break;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
				break;
			default:
				return 0;
			}
			int num = this.reader.ReadContentAsBinHex(buffer, index, count);
			if (num > 0 || count == 0)
			{
				return num;
			}
			if (this.NodeType != XmlNodeType.EndElement)
			{
				throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
			}
			this.state = XmlSubtreeReader.State.Interactive;
			this.ProcessNamespaces();
			if (this.reader.Depth == this.initialDepth)
			{
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
			}
			else
			{
				this.Read();
			}
			return 0;
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x06000A5A RID: 2650 RVA: 0x0002E7D2 File Offset: 0x0002C9D2
		public override bool CanReadValueChunk
		{
			get
			{
				return this.reader.CanReadValueChunk;
			}
		}

		// Token: 0x06000A5B RID: 2651 RVA: 0x0002E7E0 File Offset: 0x0002C9E0
		public override int ReadValueChunk(char[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
				break;
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
				if (this.curNsAttr != -1 && this.reader.CanReadValueChunk)
				{
					this.CheckBuffer(buffer, index, count);
					int num = this.curNode.value.Length - this.nsIncReadOffset;
					if (num > count)
					{
						num = count;
					}
					if (num > 0)
					{
						this.curNode.value.CopyTo(this.nsIncReadOffset, buffer, index, num);
					}
					this.nsIncReadOffset += num;
					return num;
				}
				break;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadValueChunk calls cannot be mixed with ReadContentAsBase64 or ReadContentAsBinHex."));
			default:
				return 0;
			}
			return this.reader.ReadValueChunk(buffer, index, count);
		}

		// Token: 0x06000A5C RID: 2652 RVA: 0x0002E8B7 File Offset: 0x0002CAB7
		public override string LookupNamespace(string prefix)
		{
			return ((IXmlNamespaceResolver)this).LookupNamespace(prefix);
		}

		// Token: 0x06000A5D RID: 2653 RVA: 0x0002E8C0 File Offset: 0x0002CAC0
		protected override void Dispose(bool disposing)
		{
			this.Close();
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000A5E RID: 2654 RVA: 0x0002E8C8 File Offset: 0x0002CAC8
		int IXmlLineInfo.LineNumber
		{
			get
			{
				if (!this.useCurNode)
				{
					IXmlLineInfo xmlLineInfo = this.reader as IXmlLineInfo;
					if (xmlLineInfo != null)
					{
						return xmlLineInfo.LineNumber;
					}
				}
				return 0;
			}
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000A5F RID: 2655 RVA: 0x0002E8F4 File Offset: 0x0002CAF4
		int IXmlLineInfo.LinePosition
		{
			get
			{
				if (!this.useCurNode)
				{
					IXmlLineInfo xmlLineInfo = this.reader as IXmlLineInfo;
					if (xmlLineInfo != null)
					{
						return xmlLineInfo.LinePosition;
					}
				}
				return 0;
			}
		}

		// Token: 0x06000A60 RID: 2656 RVA: 0x0002E920 File Offset: 0x0002CB20
		bool IXmlLineInfo.HasLineInfo()
		{
			return this.reader is IXmlLineInfo;
		}

		// Token: 0x06000A61 RID: 2657 RVA: 0x0002E930 File Offset: 0x0002CB30
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			if (!this.InNamespaceActiveState)
			{
				return new Dictionary<string, string>();
			}
			return this.nsManager.GetNamespacesInScope(scope);
		}

		// Token: 0x06000A62 RID: 2658 RVA: 0x0002E94C File Offset: 0x0002CB4C
		string IXmlNamespaceResolver.LookupNamespace(string prefix)
		{
			if (!this.InNamespaceActiveState)
			{
				return null;
			}
			return this.nsManager.LookupNamespace(prefix);
		}

		// Token: 0x06000A63 RID: 2659 RVA: 0x0002E964 File Offset: 0x0002CB64
		string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
		{
			if (!this.InNamespaceActiveState)
			{
				return null;
			}
			return this.nsManager.LookupPrefix(namespaceName);
		}

		// Token: 0x06000A64 RID: 2660 RVA: 0x0002E97C File Offset: 0x0002CB7C
		private void ProcessNamespaces()
		{
			XmlNodeType nodeType = this.reader.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.EndElement)
				{
					return;
				}
				this.state = XmlSubtreeReader.State.PopNamespaceScope;
			}
			else
			{
				this.nsManager.PushScope();
				string text = this.reader.Prefix;
				string namespaceURI = this.reader.NamespaceURI;
				if (this.nsManager.LookupNamespace(text) != namespaceURI)
				{
					this.AddNamespace(text, namespaceURI);
				}
				if (this.reader.MoveToFirstAttribute())
				{
					do
					{
						text = this.reader.Prefix;
						namespaceURI = this.reader.NamespaceURI;
						if (Ref.Equal(namespaceURI, this.xmlnsUri))
						{
							if (text.Length == 0)
							{
								this.nsManager.AddNamespace(string.Empty, this.reader.Value);
								this.RemoveNamespace(string.Empty, this.xmlns);
							}
							else
							{
								text = this.reader.LocalName;
								this.nsManager.AddNamespace(text, this.reader.Value);
								this.RemoveNamespace(this.xmlns, text);
							}
						}
						else if (text.Length != 0 && this.nsManager.LookupNamespace(text) != namespaceURI)
						{
							this.AddNamespace(text, namespaceURI);
						}
					}
					while (this.reader.MoveToNextAttribute());
					this.reader.MoveToElement();
				}
				if (this.reader.IsEmptyElement)
				{
					this.state = XmlSubtreeReader.State.PopNamespaceScope;
					return;
				}
			}
		}

		// Token: 0x06000A65 RID: 2661 RVA: 0x0002EADC File Offset: 0x0002CCDC
		private void AddNamespace(string prefix, string ns)
		{
			this.nsManager.AddNamespace(prefix, ns);
			int num = this.nsAttrCount;
			this.nsAttrCount = num + 1;
			int num2 = num;
			if (this.nsAttributes == null)
			{
				this.nsAttributes = new XmlSubtreeReader.NodeData[this.InitialNamespaceAttributeCount];
			}
			if (num2 == this.nsAttributes.Length)
			{
				XmlSubtreeReader.NodeData[] destinationArray = new XmlSubtreeReader.NodeData[this.nsAttributes.Length * 2];
				Array.Copy(this.nsAttributes, 0, destinationArray, 0, num2);
				this.nsAttributes = destinationArray;
			}
			if (this.nsAttributes[num2] == null)
			{
				this.nsAttributes[num2] = new XmlSubtreeReader.NodeData();
			}
			if (prefix.Length == 0)
			{
				this.nsAttributes[num2].Set(XmlNodeType.Attribute, this.xmlns, string.Empty, this.xmlns, this.xmlnsUri, ns);
			}
			else
			{
				this.nsAttributes[num2].Set(XmlNodeType.Attribute, prefix, this.xmlns, this.reader.NameTable.Add(this.xmlns + ":" + prefix), this.xmlnsUri, ns);
			}
			this.state = XmlSubtreeReader.State.ClearNsAttributes;
			this.curNsAttr = -1;
		}

		// Token: 0x06000A66 RID: 2662 RVA: 0x0002EBE4 File Offset: 0x0002CDE4
		private void RemoveNamespace(string prefix, string localName)
		{
			for (int i = 0; i < this.nsAttrCount; i++)
			{
				if (Ref.Equal(prefix, this.nsAttributes[i].prefix) && Ref.Equal(localName, this.nsAttributes[i].localName))
				{
					if (i < this.nsAttrCount - 1)
					{
						XmlSubtreeReader.NodeData nodeData = this.nsAttributes[i];
						this.nsAttributes[i] = this.nsAttributes[this.nsAttrCount - 1];
						this.nsAttributes[this.nsAttrCount - 1] = nodeData;
					}
					this.nsAttrCount--;
					return;
				}
			}
		}

		// Token: 0x06000A67 RID: 2663 RVA: 0x0002EC79 File Offset: 0x0002CE79
		private void MoveToNsAttribute(int index)
		{
			this.reader.MoveToElement();
			this.curNsAttr = index;
			this.nsIncReadOffset = 0;
			this.SetCurrentNode(this.nsAttributes[index]);
		}

		// Token: 0x06000A68 RID: 2664 RVA: 0x0002ECA4 File Offset: 0x0002CEA4
		private bool InitReadElementContentAsBinary(XmlSubtreeReader.State binaryState)
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw this.reader.CreateReadElementContentAsException("ReadElementContentAsBase64");
			}
			bool isEmptyElement = this.IsEmptyElement;
			if (!this.Read() || isEmptyElement)
			{
				return false;
			}
			XmlNodeType nodeType = this.NodeType;
			if (nodeType == XmlNodeType.Element)
			{
				throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
			}
			if (nodeType != XmlNodeType.EndElement)
			{
				this.state = binaryState;
				return true;
			}
			this.ProcessNamespaces();
			this.Read();
			return false;
		}

		// Token: 0x06000A69 RID: 2665 RVA: 0x0002ED3C File Offset: 0x0002CF3C
		private bool FinishReadElementContentAsBinary()
		{
			byte[] buffer = new byte[256];
			if (this.state == XmlSubtreeReader.State.ReadElementContentAsBase64)
			{
				while (this.reader.ReadContentAsBase64(buffer, 0, 256) > 0)
				{
				}
			}
			else
			{
				while (this.reader.ReadContentAsBinHex(buffer, 0, 256) > 0)
				{
				}
			}
			if (this.NodeType != XmlNodeType.EndElement)
			{
				throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
			}
			this.state = XmlSubtreeReader.State.Interactive;
			this.ProcessNamespaces();
			if (this.reader.Depth == this.initialDepth)
			{
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
				return false;
			}
			return this.Read();
		}

		// Token: 0x06000A6A RID: 2666 RVA: 0x0002EDF8 File Offset: 0x0002CFF8
		private bool FinishReadContentAsBinary()
		{
			byte[] buffer = new byte[256];
			if (this.state == XmlSubtreeReader.State.ReadContentAsBase64)
			{
				while (this.reader.ReadContentAsBase64(buffer, 0, 256) > 0)
				{
				}
			}
			else
			{
				while (this.reader.ReadContentAsBinHex(buffer, 0, 256) > 0)
				{
				}
			}
			this.state = XmlSubtreeReader.State.Interactive;
			this.ProcessNamespaces();
			if (this.reader.Depth == this.initialDepth)
			{
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
				return false;
			}
			return true;
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000A6B RID: 2667 RVA: 0x0002EE76 File Offset: 0x0002D076
		private bool InAttributeActiveState
		{
			get
			{
				return (98 & 1 << (int)this.state) != 0;
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06000A6C RID: 2668 RVA: 0x0002EE89 File Offset: 0x0002D089
		private bool InNamespaceActiveState
		{
			get
			{
				return (2018 & 1 << (int)this.state) != 0;
			}
		}

		// Token: 0x06000A6D RID: 2669 RVA: 0x0002EE9F File Offset: 0x0002D09F
		private void SetEmptyNode()
		{
			this.tmpNode.type = XmlNodeType.None;
			this.tmpNode.value = string.Empty;
			this.curNode = this.tmpNode;
			this.useCurNode = true;
		}

		// Token: 0x06000A6E RID: 2670 RVA: 0x0002EED0 File Offset: 0x0002D0D0
		private void SetCurrentNode(XmlSubtreeReader.NodeData node)
		{
			this.curNode = node;
			this.useCurNode = true;
		}

		// Token: 0x06000A6F RID: 2671 RVA: 0x0002EEE0 File Offset: 0x0002D0E0
		private void InitReadContentAsType(string methodName)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				throw new InvalidOperationException(Res.GetString("The XmlReader is closed or in error state."));
			case XmlSubtreeReader.State.Interactive:
				return;
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
				return;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadValueChunk calls cannot be mixed with ReadContentAsBase64 or ReadContentAsBinHex."));
			default:
				throw base.CreateReadContentAsException(methodName);
			}
		}

		// Token: 0x06000A70 RID: 2672 RVA: 0x0002EF54 File Offset: 0x0002D154
		private void FinishReadContentAsType()
		{
			XmlNodeType nodeType = this.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.Attribute)
				{
					if (nodeType != XmlNodeType.EndElement)
					{
						return;
					}
					this.state = XmlSubtreeReader.State.PopNamespaceScope;
				}
				return;
			}
			this.ProcessNamespaces();
		}

		// Token: 0x06000A71 RID: 2673 RVA: 0x0002EF84 File Offset: 0x0002D184
		private void CheckBuffer(Array buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
		}

		// Token: 0x06000A72 RID: 2674 RVA: 0x0002EFD3 File Offset: 0x0002D1D3
		public override Task<string> GetValueAsync()
		{
			if (this.useCurNode)
			{
				return Task.FromResult<string>(this.curNode.value);
			}
			return this.reader.GetValueAsync();
		}

		// Token: 0x06000A73 RID: 2675 RVA: 0x0002EFFC File Offset: 0x0002D1FC
		public override async Task<bool> ReadAsync()
		{
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
				this.useCurNode = false;
				this.state = XmlSubtreeReader.State.Interactive;
				this.ProcessNamespaces();
				return true;
			case XmlSubtreeReader.State.Interactive:
				break;
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return false;
			case XmlSubtreeReader.State.PopNamespaceScope:
				this.nsManager.PopScope();
				goto IL_188;
			case XmlSubtreeReader.State.ClearNsAttributes:
				goto IL_188;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
				configuredTaskAwaiter = this.FinishReadElementContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return false;
				}
				return await this.ReadAsync().ConfigureAwait(false);
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				configuredTaskAwaiter = this.FinishReadContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return false;
				}
				return await this.ReadAsync().ConfigureAwait(false);
			default:
				return false;
			}
			IL_81:
			this.curNsAttr = -1;
			this.useCurNode = false;
			this.reader.MoveToElement();
			if (this.reader.Depth == this.initialDepth && (this.reader.NodeType == XmlNodeType.EndElement || (this.reader.NodeType == XmlNodeType.Element && this.reader.IsEmptyElement)))
			{
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
				return false;
			}
			configuredTaskAwaiter = this.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult())
			{
				this.ProcessNamespaces();
				return true;
			}
			this.SetEmptyNode();
			return false;
			IL_188:
			this.nsAttrCount = 0;
			this.state = XmlSubtreeReader.State.Interactive;
			goto IL_81;
		}

		// Token: 0x06000A74 RID: 2676 RVA: 0x0002F044 File Offset: 0x0002D244
		public override async Task SkipAsync()
		{
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
				await this.ReadAsync().ConfigureAwait(false);
				return;
			case XmlSubtreeReader.State.Interactive:
				break;
			case XmlSubtreeReader.State.Error:
				return;
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				goto IL_2DD;
			case XmlSubtreeReader.State.PopNamespaceScope:
				this.nsManager.PopScope();
				goto IL_2EE;
			case XmlSubtreeReader.State.ClearNsAttributes:
				goto IL_2EE;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.FinishReadElementContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult())
				{
					await this.SkipAsync().ConfigureAwait(false);
					return;
				}
				return;
			}
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.FinishReadContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (configuredTaskAwaiter.GetResult())
				{
					await this.SkipAsync().ConfigureAwait(false);
					return;
				}
				return;
			}
			default:
				return;
			}
			IL_DD:
			this.curNsAttr = -1;
			this.useCurNode = false;
			this.reader.MoveToElement();
			if (this.reader.Depth == this.initialDepth)
			{
				if (this.reader.NodeType == XmlNodeType.Element && !this.reader.IsEmptyElement)
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult())
					{
						while (this.reader.NodeType != XmlNodeType.EndElement && this.reader.Depth > this.initialDepth)
						{
							await this.reader.SkipAsync().ConfigureAwait(false);
						}
					}
				}
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
			}
			else
			{
				if (this.reader.NodeType == XmlNodeType.Element && !this.reader.IsEmptyElement)
				{
					this.nsManager.PopScope();
				}
				await this.reader.SkipAsync().ConfigureAwait(false);
				this.ProcessNamespaces();
			}
			IL_2DD:
			return;
			IL_2EE:
			this.nsAttrCount = 0;
			this.state = XmlSubtreeReader.State.Interactive;
			goto IL_DD;
		}

		// Token: 0x06000A75 RID: 2677 RVA: 0x0002F08C File Offset: 0x0002D28C
		public override async Task<object> ReadContentAsObjectAsync()
		{
			object result;
			try
			{
				this.InitReadContentAsType("ReadContentAsObject");
				object obj = await this.reader.ReadContentAsObjectAsync().ConfigureAwait(false);
				this.FinishReadContentAsType();
				result = obj;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A76 RID: 2678 RVA: 0x0002F0D4 File Offset: 0x0002D2D4
		public override async Task<string> ReadContentAsStringAsync()
		{
			string result;
			try
			{
				this.InitReadContentAsType("ReadContentAsString");
				string text = await this.reader.ReadContentAsStringAsync().ConfigureAwait(false);
				this.FinishReadContentAsType();
				result = text;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A77 RID: 2679 RVA: 0x0002F11C File Offset: 0x0002D31C
		public override async Task<object> ReadContentAsAsync(Type returnType, IXmlNamespaceResolver namespaceResolver)
		{
			object result;
			try
			{
				this.InitReadContentAsType("ReadContentAs");
				object obj = await this.reader.ReadContentAsAsync(returnType, namespaceResolver).ConfigureAwait(false);
				this.FinishReadContentAsType();
				result = obj;
			}
			catch
			{
				this.state = XmlSubtreeReader.State.Error;
				throw;
			}
			return result;
		}

		// Token: 0x06000A78 RID: 2680 RVA: 0x0002F174 File Offset: 0x0002D374
		public override async Task<int> ReadContentAsBase64Async(byte[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
				this.state = XmlSubtreeReader.State.ReadContentAsBase64;
				break;
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
			{
				XmlNodeType nodeType = this.NodeType;
				switch (nodeType)
				{
				case XmlNodeType.Element:
					throw base.CreateReadContentAsException("ReadContentAsBase64");
				case XmlNodeType.Attribute:
					if (this.curNsAttr != -1 && this.reader.CanReadBinaryContent)
					{
						this.CheckBuffer(buffer, index, count);
						if (count == 0)
						{
							return 0;
						}
						if (this.nsIncReadOffset == 0)
						{
							if (this.binDecoder != null && this.binDecoder is Base64Decoder)
							{
								this.binDecoder.Reset();
							}
							else
							{
								this.binDecoder = new Base64Decoder();
							}
						}
						if (this.nsIncReadOffset == this.curNode.value.Length)
						{
							return 0;
						}
						this.binDecoder.SetNextOutputBuffer(buffer, index, count);
						this.nsIncReadOffset += this.binDecoder.Decode(this.curNode.value, this.nsIncReadOffset, this.curNode.value.Length - this.nsIncReadOffset);
						return this.binDecoder.DecodedCount;
					}
					break;
				case XmlNodeType.Text:
					break;
				default:
					if (nodeType != XmlNodeType.EndElement)
					{
						return 0;
					}
					return 0;
				}
				return await this.reader.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
			}
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case XmlSubtreeReader.State.ReadContentAsBase64:
				break;
			default:
				return 0;
			}
			object obj = await this.reader.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
			if (obj == null)
			{
				this.state = XmlSubtreeReader.State.Interactive;
				this.ProcessNamespaces();
			}
			return obj;
		}

		// Token: 0x06000A79 RID: 2681 RVA: 0x0002F1D4 File Offset: 0x0002D3D4
		public override async Task<int> ReadElementContentAsBase64Async(byte[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.InitReadElementContentAsBinaryAsync(XmlSubtreeReader.State.ReadElementContentAsBase64).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return 0;
				}
				break;
			}
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
				break;
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			default:
				return 0;
			}
			int num = await this.reader.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
			int result;
			if (num > 0 || count == 0)
			{
				result = num;
			}
			else
			{
				if (this.NodeType != XmlNodeType.EndElement)
				{
					throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
				}
				this.state = XmlSubtreeReader.State.Interactive;
				this.ProcessNamespaces();
				if (this.reader.Depth == this.initialDepth)
				{
					this.state = XmlSubtreeReader.State.EndOfFile;
					this.SetEmptyNode();
				}
				else
				{
					await this.ReadAsync().ConfigureAwait(false);
				}
				result = 0;
			}
			return result;
		}

		// Token: 0x06000A7A RID: 2682 RVA: 0x0002F234 File Offset: 0x0002D434
		public override async Task<int> ReadContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
				this.state = XmlSubtreeReader.State.ReadContentAsBinHex;
				break;
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
			{
				XmlNodeType nodeType = this.NodeType;
				switch (nodeType)
				{
				case XmlNodeType.Element:
					throw base.CreateReadContentAsException("ReadContentAsBinHex");
				case XmlNodeType.Attribute:
					if (this.curNsAttr != -1 && this.reader.CanReadBinaryContent)
					{
						this.CheckBuffer(buffer, index, count);
						if (count == 0)
						{
							return 0;
						}
						if (this.nsIncReadOffset == 0)
						{
							if (this.binDecoder != null && this.binDecoder is BinHexDecoder)
							{
								this.binDecoder.Reset();
							}
							else
							{
								this.binDecoder = new BinHexDecoder();
							}
						}
						if (this.nsIncReadOffset == this.curNode.value.Length)
						{
							return 0;
						}
						this.binDecoder.SetNextOutputBuffer(buffer, index, count);
						this.nsIncReadOffset += this.binDecoder.Decode(this.curNode.value, this.nsIncReadOffset, this.curNode.value.Length - this.nsIncReadOffset);
						return this.binDecoder.DecodedCount;
					}
					break;
				case XmlNodeType.Text:
					break;
				default:
					if (nodeType != XmlNodeType.EndElement)
					{
						return 0;
					}
					return 0;
				}
				return await this.reader.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
			}
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBase64:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				break;
			default:
				return 0;
			}
			object obj = await this.reader.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
			if (obj == null)
			{
				this.state = XmlSubtreeReader.State.Interactive;
				this.ProcessNamespaces();
			}
			return obj;
		}

		// Token: 0x06000A7B RID: 2683 RVA: 0x0002F294 File Offset: 0x0002D494
		public override async Task<int> ReadElementContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return 0;
			case XmlSubtreeReader.State.Interactive:
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
			{
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.InitReadElementContentAsBinaryAsync(XmlSubtreeReader.State.ReadElementContentAsBinHex).ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
				}
				if (!configuredTaskAwaiter.GetResult())
				{
					return 0;
				}
				break;
			}
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
				break;
			default:
				return 0;
			}
			int num = await this.reader.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
			int result;
			if (num > 0 || count == 0)
			{
				result = num;
			}
			else
			{
				if (this.NodeType != XmlNodeType.EndElement)
				{
					throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
				}
				this.state = XmlSubtreeReader.State.Interactive;
				this.ProcessNamespaces();
				if (this.reader.Depth == this.initialDepth)
				{
					this.state = XmlSubtreeReader.State.EndOfFile;
					this.SetEmptyNode();
				}
				else
				{
					await this.ReadAsync().ConfigureAwait(false);
				}
				result = 0;
			}
			return result;
		}

		// Token: 0x06000A7C RID: 2684 RVA: 0x0002F2F4 File Offset: 0x0002D4F4
		public override Task<int> ReadValueChunkAsync(char[] buffer, int index, int count)
		{
			switch (this.state)
			{
			case XmlSubtreeReader.State.Initial:
			case XmlSubtreeReader.State.Error:
			case XmlSubtreeReader.State.EndOfFile:
			case XmlSubtreeReader.State.Closed:
				return Task.FromResult<int>(0);
			case XmlSubtreeReader.State.Interactive:
				break;
			case XmlSubtreeReader.State.PopNamespaceScope:
			case XmlSubtreeReader.State.ClearNsAttributes:
				if (this.curNsAttr != -1 && this.reader.CanReadValueChunk)
				{
					this.CheckBuffer(buffer, index, count);
					int num = this.curNode.value.Length - this.nsIncReadOffset;
					if (num > count)
					{
						num = count;
					}
					if (num > 0)
					{
						this.curNode.value.CopyTo(this.nsIncReadOffset, buffer, index, num);
					}
					this.nsIncReadOffset += num;
					return Task.FromResult<int>(num);
				}
				break;
			case XmlSubtreeReader.State.ReadElementContentAsBase64:
			case XmlSubtreeReader.State.ReadElementContentAsBinHex:
			case XmlSubtreeReader.State.ReadContentAsBase64:
			case XmlSubtreeReader.State.ReadContentAsBinHex:
				throw new InvalidOperationException(Res.GetString("ReadValueChunk calls cannot be mixed with ReadContentAsBase64 or ReadContentAsBinHex."));
			default:
				return Task.FromResult<int>(0);
			}
			return this.reader.ReadValueChunkAsync(buffer, index, count);
		}

		// Token: 0x06000A7D RID: 2685 RVA: 0x0002F3DC File Offset: 0x0002D5DC
		private async Task<bool> InitReadElementContentAsBinaryAsync(XmlSubtreeReader.State binaryState)
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw this.reader.CreateReadElementContentAsException("ReadElementContentAsBase64");
			}
			bool isEmpty = this.IsEmptyElement;
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			bool result;
			if (!configuredTaskAwaiter.GetResult() || isEmpty)
			{
				result = false;
			}
			else
			{
				XmlNodeType nodeType = this.NodeType;
				if (nodeType == XmlNodeType.Element)
				{
					throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
				}
				if (nodeType != XmlNodeType.EndElement)
				{
					this.state = binaryState;
					result = true;
				}
				else
				{
					this.ProcessNamespaces();
					await this.ReadAsync().ConfigureAwait(false);
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06000A7E RID: 2686 RVA: 0x0002F42C File Offset: 0x0002D62C
		private async Task<bool> FinishReadElementContentAsBinaryAsync()
		{
			byte[] bytes = new byte[256];
			if (this.state == XmlSubtreeReader.State.ReadElementContentAsBase64)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				do
				{
					configuredTaskAwaiter = this.reader.ReadContentAsBase64Async(bytes, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
				}
				while (configuredTaskAwaiter.GetResult() > 0);
			}
			else
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				do
				{
					configuredTaskAwaiter = this.reader.ReadContentAsBinHexAsync(bytes, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
				}
				while (configuredTaskAwaiter.GetResult() > 0);
			}
			if (this.NodeType != XmlNodeType.EndElement)
			{
				throw new XmlException("'{0}' is an invalid XmlNodeType.", this.reader.NodeType.ToString(), this.reader as IXmlLineInfo);
			}
			this.state = XmlSubtreeReader.State.Interactive;
			this.ProcessNamespaces();
			bool result;
			if (this.reader.Depth == this.initialDepth)
			{
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
				result = false;
			}
			else
			{
				result = await this.ReadAsync().ConfigureAwait(false);
			}
			return result;
		}

		// Token: 0x06000A7F RID: 2687 RVA: 0x0002F474 File Offset: 0x0002D674
		private async Task<bool> FinishReadContentAsBinaryAsync()
		{
			byte[] bytes = new byte[256];
			if (this.state == XmlSubtreeReader.State.ReadContentAsBase64)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				do
				{
					configuredTaskAwaiter = this.reader.ReadContentAsBase64Async(bytes, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
				}
				while (configuredTaskAwaiter.GetResult() > 0);
			}
			else
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				do
				{
					configuredTaskAwaiter = this.reader.ReadContentAsBinHexAsync(bytes, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
				}
				while (configuredTaskAwaiter.GetResult() > 0);
			}
			this.state = XmlSubtreeReader.State.Interactive;
			this.ProcessNamespaces();
			bool result;
			if (this.reader.Depth == this.initialDepth)
			{
				this.state = XmlSubtreeReader.State.EndOfFile;
				this.SetEmptyNode();
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x040005D4 RID: 1492
		private const int AttributeActiveStates = 98;

		// Token: 0x040005D5 RID: 1493
		private const int NamespaceActiveStates = 2018;

		// Token: 0x040005D6 RID: 1494
		private int initialDepth;

		// Token: 0x040005D7 RID: 1495
		private XmlSubtreeReader.State state;

		// Token: 0x040005D8 RID: 1496
		private XmlNamespaceManager nsManager;

		// Token: 0x040005D9 RID: 1497
		private XmlSubtreeReader.NodeData[] nsAttributes;

		// Token: 0x040005DA RID: 1498
		private int nsAttrCount;

		// Token: 0x040005DB RID: 1499
		private int curNsAttr = -1;

		// Token: 0x040005DC RID: 1500
		private string xmlns;

		// Token: 0x040005DD RID: 1501
		private string xmlnsUri;

		// Token: 0x040005DE RID: 1502
		private int nsIncReadOffset;

		// Token: 0x040005DF RID: 1503
		private IncrementalReadDecoder binDecoder;

		// Token: 0x040005E0 RID: 1504
		private bool useCurNode;

		// Token: 0x040005E1 RID: 1505
		private XmlSubtreeReader.NodeData curNode;

		// Token: 0x040005E2 RID: 1506
		private XmlSubtreeReader.NodeData tmpNode;

		// Token: 0x040005E3 RID: 1507
		internal int InitialNamespaceAttributeCount = 4;

		// Token: 0x02000113 RID: 275
		private class NodeData
		{
			// Token: 0x06000A80 RID: 2688 RVA: 0x00002103 File Offset: 0x00000303
			internal NodeData()
			{
			}

			// Token: 0x06000A81 RID: 2689 RVA: 0x0002F4B9 File Offset: 0x0002D6B9
			internal void Set(XmlNodeType nodeType, string localName, string prefix, string name, string namespaceUri, string value)
			{
				this.type = nodeType;
				this.localName = localName;
				this.prefix = prefix;
				this.name = name;
				this.namespaceUri = namespaceUri;
				this.value = value;
			}

			// Token: 0x040005E4 RID: 1508
			internal XmlNodeType type;

			// Token: 0x040005E5 RID: 1509
			internal string localName;

			// Token: 0x040005E6 RID: 1510
			internal string prefix;

			// Token: 0x040005E7 RID: 1511
			internal string name;

			// Token: 0x040005E8 RID: 1512
			internal string namespaceUri;

			// Token: 0x040005E9 RID: 1513
			internal string value;
		}

		// Token: 0x02000114 RID: 276
		private enum State
		{
			// Token: 0x040005EB RID: 1515
			Initial,
			// Token: 0x040005EC RID: 1516
			Interactive,
			// Token: 0x040005ED RID: 1517
			Error,
			// Token: 0x040005EE RID: 1518
			EndOfFile,
			// Token: 0x040005EF RID: 1519
			Closed,
			// Token: 0x040005F0 RID: 1520
			PopNamespaceScope,
			// Token: 0x040005F1 RID: 1521
			ClearNsAttributes,
			// Token: 0x040005F2 RID: 1522
			ReadElementContentAsBase64,
			// Token: 0x040005F3 RID: 1523
			ReadElementContentAsBinHex,
			// Token: 0x040005F4 RID: 1524
			ReadContentAsBase64,
			// Token: 0x040005F5 RID: 1525
			ReadContentAsBinHex
		}

		// Token: 0x02000115 RID: 277
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsync>d__104 : IAsyncStateMachine
		{
			// Token: 0x06000A82 RID: 2690 RVA: 0x0002F4E8 File Offset: 0x0002D6E8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1FC;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_26D;
					case 3:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2DB;
					case 4:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_349;
					default:
						switch (xmlSubtreeReader.state)
						{
						case XmlSubtreeReader.State.Initial:
							xmlSubtreeReader.useCurNode = false;
							xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
							xmlSubtreeReader.ProcessNamespaces();
							result = true;
							goto IL_370;
						case XmlSubtreeReader.State.Interactive:
							break;
						case XmlSubtreeReader.State.Error:
						case XmlSubtreeReader.State.EndOfFile:
						case XmlSubtreeReader.State.Closed:
							result = false;
							goto IL_370;
						case XmlSubtreeReader.State.PopNamespaceScope:
							xmlSubtreeReader.nsManager.PopScope();
							goto IL_188;
						case XmlSubtreeReader.State.ClearNsAttributes:
							goto IL_188;
						case XmlSubtreeReader.State.ReadElementContentAsBase64:
						case XmlSubtreeReader.State.ReadElementContentAsBinHex:
							configuredTaskAwaiter3 = xmlSubtreeReader.FinishReadElementContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 1;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadAsync>d__104>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_1FC;
						case XmlSubtreeReader.State.ReadContentAsBase64:
						case XmlSubtreeReader.State.ReadContentAsBinHex:
							configuredTaskAwaiter3 = xmlSubtreeReader.FinishReadContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 3;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadAsync>d__104>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_2DB;
						default:
							result = false;
							goto IL_370;
						}
						IL_81:
						xmlSubtreeReader.curNsAttr = -1;
						xmlSubtreeReader.useCurNode = false;
						xmlSubtreeReader.reader.MoveToElement();
						if (xmlSubtreeReader.reader.Depth == xmlSubtreeReader.initialDepth && (xmlSubtreeReader.reader.NodeType == XmlNodeType.EndElement || (xmlSubtreeReader.reader.NodeType == XmlNodeType.Element && xmlSubtreeReader.reader.IsEmptyElement)))
						{
							xmlSubtreeReader.state = XmlSubtreeReader.State.EndOfFile;
							xmlSubtreeReader.SetEmptyNode();
							result = false;
							goto IL_370;
						}
						configuredTaskAwaiter3 = xmlSubtreeReader.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadAsync>d__104>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
						IL_188:
						xmlSubtreeReader.nsAttrCount = 0;
						xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
						goto IL_81;
					}
					if (configuredTaskAwaiter3.GetResult())
					{
						xmlSubtreeReader.ProcessNamespaces();
						result = true;
						goto IL_370;
					}
					xmlSubtreeReader.SetEmptyNode();
					result = false;
					goto IL_370;
					IL_1FC:
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = false;
						goto IL_370;
					}
					configuredTaskAwaiter3 = xmlSubtreeReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadAsync>d__104>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_26D:
					result = configuredTaskAwaiter3.GetResult();
					goto IL_370;
					IL_2DB:
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = false;
						goto IL_370;
					}
					configuredTaskAwaiter3 = xmlSubtreeReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 4;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadAsync>d__104>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_349:
					result = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_370:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000A83 RID: 2691 RVA: 0x0002F898 File Offset: 0x0002DA98
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040005F6 RID: 1526
			public int <>1__state;

			// Token: 0x040005F7 RID: 1527
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040005F8 RID: 1528
			public XmlSubtreeReader <>4__this;

			// Token: 0x040005F9 RID: 1529
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000116 RID: 278
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SkipAsync>d__105 : IAsyncStateMachine
		{
			// Token: 0x06000A84 RID: 2692 RVA: 0x0002F8A8 File Offset: 0x0002DAA8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_191;
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_205;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2D0;
					}
					case 4:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_35F;
					case 5:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3CC;
					}
					case 6:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_436;
					case 7:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_49D;
					}
					default:
						switch (xmlSubtreeReader.state)
						{
						case XmlSubtreeReader.State.Initial:
							configuredTaskAwaiter3 = xmlSubtreeReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<SkipAsync>d__105>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_D0;
						case XmlSubtreeReader.State.Interactive:
							break;
						case XmlSubtreeReader.State.Error:
							goto IL_4A4;
						case XmlSubtreeReader.State.EndOfFile:
						case XmlSubtreeReader.State.Closed:
							goto IL_2DD;
						case XmlSubtreeReader.State.PopNamespaceScope:
							xmlSubtreeReader.nsManager.PopScope();
							goto IL_2EE;
						case XmlSubtreeReader.State.ClearNsAttributes:
							goto IL_2EE;
						case XmlSubtreeReader.State.ReadElementContentAsBase64:
						case XmlSubtreeReader.State.ReadElementContentAsBinHex:
							configuredTaskAwaiter3 = xmlSubtreeReader.FinishReadElementContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 4;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<SkipAsync>d__105>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_35F;
						case XmlSubtreeReader.State.ReadContentAsBase64:
						case XmlSubtreeReader.State.ReadContentAsBinHex:
							configuredTaskAwaiter3 = xmlSubtreeReader.FinishReadContentAsBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 6;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<SkipAsync>d__105>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_436;
						default:
							goto IL_4A4;
						}
						IL_DD:
						xmlSubtreeReader.curNsAttr = -1;
						xmlSubtreeReader.useCurNode = false;
						xmlSubtreeReader.reader.MoveToElement();
						if (xmlSubtreeReader.reader.Depth == xmlSubtreeReader.initialDepth)
						{
							if (xmlSubtreeReader.reader.NodeType != XmlNodeType.Element || xmlSubtreeReader.reader.IsEmptyElement)
							{
								goto IL_231;
							}
							configuredTaskAwaiter3 = xmlSubtreeReader.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 1;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<SkipAsync>d__105>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_191;
						}
						else
						{
							if (xmlSubtreeReader.reader.NodeType == XmlNodeType.Element && !xmlSubtreeReader.reader.IsEmptyElement)
							{
								xmlSubtreeReader.nsManager.PopScope();
							}
							configuredTaskAwaiter4 = xmlSubtreeReader.reader.SkipAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 3;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlSubtreeReader.<SkipAsync>d__105>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							goto IL_2D0;
						}
						IL_2EE:
						xmlSubtreeReader.nsAttrCount = 0;
						xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
						goto IL_DD;
					}
					IL_D0:
					configuredTaskAwaiter3.GetResult();
					goto IL_4BF;
					IL_191:
					if (configuredTaskAwaiter3.GetResult())
					{
						goto IL_20C;
					}
					goto IL_231;
					IL_205:
					configuredTaskAwaiter4.GetResult();
					IL_20C:
					if (xmlSubtreeReader.reader.NodeType != XmlNodeType.EndElement && xmlSubtreeReader.reader.Depth > xmlSubtreeReader.initialDepth)
					{
						configuredTaskAwaiter4 = xmlSubtreeReader.reader.SkipAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlSubtreeReader.<SkipAsync>d__105>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						goto IL_205;
					}
					IL_231:
					xmlSubtreeReader.state = XmlSubtreeReader.State.EndOfFile;
					xmlSubtreeReader.SetEmptyNode();
					goto IL_4BF;
					IL_2D0:
					configuredTaskAwaiter4.GetResult();
					xmlSubtreeReader.ProcessNamespaces();
					IL_2DD:
					goto IL_4BF;
					IL_35F:
					if (!configuredTaskAwaiter3.GetResult())
					{
						goto IL_4A4;
					}
					configuredTaskAwaiter4 = xmlSubtreeReader.SkipAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlSubtreeReader.<SkipAsync>d__105>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_3CC:
					configuredTaskAwaiter4.GetResult();
					goto IL_4A4;
					IL_436:
					if (!configuredTaskAwaiter3.GetResult())
					{
						goto IL_4A4;
					}
					configuredTaskAwaiter4 = xmlSubtreeReader.SkipAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 7;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlSubtreeReader.<SkipAsync>d__105>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_49D:
					configuredTaskAwaiter4.GetResult();
					IL_4A4:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_4BF:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000A85 RID: 2693 RVA: 0x0002FDA4 File Offset: 0x0002DFA4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040005FA RID: 1530
			public int <>1__state;

			// Token: 0x040005FB RID: 1531
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040005FC RID: 1532
			public XmlSubtreeReader <>4__this;

			// Token: 0x040005FD RID: 1533
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040005FE RID: 1534
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000117 RID: 279
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsObjectAsync>d__106 : IAsyncStateMachine
		{
			// Token: 0x06000A86 RID: 2694 RVA: 0x0002FDB4 File Offset: 0x0002DFB4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				object result2;
				try
				{
					try
					{
						ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							xmlSubtreeReader.InitReadContentAsType("ReadContentAsObject");
							configuredTaskAwaiter = xmlSubtreeReader.reader.ReadContentAsObjectAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadContentAsObjectAsync>d__106>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						object result = configuredTaskAwaiter.GetResult();
						xmlSubtreeReader.FinishReadContentAsType();
						result2 = result;
					}
					catch
					{
						xmlSubtreeReader.state = XmlSubtreeReader.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000A87 RID: 2695 RVA: 0x0002FEA4 File Offset: 0x0002E0A4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040005FF RID: 1535
			public int <>1__state;

			// Token: 0x04000600 RID: 1536
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x04000601 RID: 1537
			public XmlSubtreeReader <>4__this;

			// Token: 0x04000602 RID: 1538
			private ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000118 RID: 280
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsStringAsync>d__107 : IAsyncStateMachine
		{
			// Token: 0x06000A88 RID: 2696 RVA: 0x0002FEB4 File Offset: 0x0002E0B4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				string result2;
				try
				{
					try
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							xmlSubtreeReader.InitReadContentAsType("ReadContentAsString");
							configuredTaskAwaiter = xmlSubtreeReader.reader.ReadContentAsStringAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadContentAsStringAsync>d__107>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						string result = configuredTaskAwaiter.GetResult();
						xmlSubtreeReader.FinishReadContentAsType();
						result2 = result;
					}
					catch
					{
						xmlSubtreeReader.state = XmlSubtreeReader.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000A89 RID: 2697 RVA: 0x0002FFA4 File Offset: 0x0002E1A4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000603 RID: 1539
			public int <>1__state;

			// Token: 0x04000604 RID: 1540
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x04000605 RID: 1541
			public XmlSubtreeReader <>4__this;

			// Token: 0x04000606 RID: 1542
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000119 RID: 281
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsAsync>d__108 : IAsyncStateMachine
		{
			// Token: 0x06000A8A RID: 2698 RVA: 0x0002FFB4 File Offset: 0x0002E1B4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				object result2;
				try
				{
					try
					{
						ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							xmlSubtreeReader.InitReadContentAsType("ReadContentAs");
							configuredTaskAwaiter = xmlSubtreeReader.reader.ReadContentAsAsync(returnType, namespaceResolver).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadContentAsAsync>d__108>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						object result = configuredTaskAwaiter.GetResult();
						xmlSubtreeReader.FinishReadContentAsType();
						result2 = result;
					}
					catch
					{
						xmlSubtreeReader.state = XmlSubtreeReader.State.Error;
						throw;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06000A8B RID: 2699 RVA: 0x000300B0 File Offset: 0x0002E2B0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000607 RID: 1543
			public int <>1__state;

			// Token: 0x04000608 RID: 1544
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x04000609 RID: 1545
			public XmlSubtreeReader <>4__this;

			// Token: 0x0400060A RID: 1546
			public Type returnType;

			// Token: 0x0400060B RID: 1547
			public IXmlNamespaceResolver namespaceResolver;

			// Token: 0x0400060C RID: 1548
			private ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200011A RID: 282
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBase64Async>d__109 : IAsyncStateMachine
		{
			// Token: 0x06000A8C RID: 2700 RVA: 0x000300C0 File Offset: 0x0002E2C0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							switch (xmlSubtreeReader.state)
							{
							case XmlSubtreeReader.State.Initial:
							case XmlSubtreeReader.State.Error:
							case XmlSubtreeReader.State.EndOfFile:
							case XmlSubtreeReader.State.Closed:
								result = 0;
								goto IL_2F0;
							case XmlSubtreeReader.State.Interactive:
								xmlSubtreeReader.state = XmlSubtreeReader.State.ReadContentAsBase64;
								break;
							case XmlSubtreeReader.State.PopNamespaceScope:
							case XmlSubtreeReader.State.ClearNsAttributes:
							{
								XmlNodeType nodeType = xmlSubtreeReader.NodeType;
								switch (nodeType)
								{
								case XmlNodeType.Element:
									throw xmlSubtreeReader.CreateReadContentAsException("ReadContentAsBase64");
								case XmlNodeType.Attribute:
									if (xmlSubtreeReader.curNsAttr != -1 && xmlSubtreeReader.reader.CanReadBinaryContent)
									{
										xmlSubtreeReader.CheckBuffer(buffer, index, count);
										if (count == 0)
										{
											result = 0;
											goto IL_2F0;
										}
										if (xmlSubtreeReader.nsIncReadOffset == 0)
										{
											if (xmlSubtreeReader.binDecoder != null && xmlSubtreeReader.binDecoder is Base64Decoder)
											{
												xmlSubtreeReader.binDecoder.Reset();
											}
											else
											{
												xmlSubtreeReader.binDecoder = new Base64Decoder();
											}
										}
										if (xmlSubtreeReader.nsIncReadOffset == xmlSubtreeReader.curNode.value.Length)
										{
											result = 0;
											goto IL_2F0;
										}
										xmlSubtreeReader.binDecoder.SetNextOutputBuffer(buffer, index, count);
										xmlSubtreeReader.nsIncReadOffset += xmlSubtreeReader.binDecoder.Decode(xmlSubtreeReader.curNode.value, xmlSubtreeReader.nsIncReadOffset, xmlSubtreeReader.curNode.value.Length - xmlSubtreeReader.nsIncReadOffset);
										result = xmlSubtreeReader.binDecoder.DecodedCount;
										goto IL_2F0;
									}
									break;
								case XmlNodeType.Text:
									break;
								default:
									if (nodeType != XmlNodeType.EndElement)
									{
										result = 0;
										goto IL_2F0;
									}
									result = 0;
									goto IL_2F0;
								}
								configuredTaskAwaiter = xmlSubtreeReader.reader.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadContentAsBase64Async>d__109>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_218;
							}
							case XmlSubtreeReader.State.ReadElementContentAsBase64:
							case XmlSubtreeReader.State.ReadElementContentAsBinHex:
							case XmlSubtreeReader.State.ReadContentAsBinHex:
								throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
							case XmlSubtreeReader.State.ReadContentAsBase64:
								break;
							default:
								result = 0;
								goto IL_2F0;
							}
							configuredTaskAwaiter = xmlSubtreeReader.reader.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadContentAsBase64Async>d__109>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						int result2 = configuredTaskAwaiter.GetResult();
						if (result2 == 0)
						{
							xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
							xmlSubtreeReader.ProcessNamespaces();
						}
						result = result2;
						goto IL_2F0;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_218:
					result = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2F0:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000A8D RID: 2701 RVA: 0x000303F0 File Offset: 0x0002E5F0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400060D RID: 1549
			public int <>1__state;

			// Token: 0x0400060E RID: 1550
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400060F RID: 1551
			public XmlSubtreeReader <>4__this;

			// Token: 0x04000610 RID: 1552
			public byte[] buffer;

			// Token: 0x04000611 RID: 1553
			public int index;

			// Token: 0x04000612 RID: 1554
			public int count;

			// Token: 0x04000613 RID: 1555
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200011B RID: 283
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBase64Async>d__110 : IAsyncStateMachine
		{
			// Token: 0x06000A8E RID: 2702 RVA: 0x00030400 File Offset: 0x0002E600
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_14F;
					}
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_234;
					default:
						switch (xmlSubtreeReader.state)
						{
						case XmlSubtreeReader.State.Initial:
						case XmlSubtreeReader.State.Error:
						case XmlSubtreeReader.State.EndOfFile:
						case XmlSubtreeReader.State.Closed:
							result = 0;
							goto IL_26D;
						case XmlSubtreeReader.State.Interactive:
						case XmlSubtreeReader.State.PopNamespaceScope:
						case XmlSubtreeReader.State.ClearNsAttributes:
							configuredTaskAwaiter3 = xmlSubtreeReader.InitReadElementContentAsBinaryAsync(XmlSubtreeReader.State.ReadElementContentAsBase64).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadElementContentAsBase64Async>d__110>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						case XmlSubtreeReader.State.ReadElementContentAsBase64:
							goto IL_D7;
						case XmlSubtreeReader.State.ReadElementContentAsBinHex:
						case XmlSubtreeReader.State.ReadContentAsBase64:
						case XmlSubtreeReader.State.ReadContentAsBinHex:
							throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
						default:
							result = 0;
							goto IL_26D;
						}
						break;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = 0;
						goto IL_26D;
					}
					IL_D7:
					configuredTaskAwaiter4 = xmlSubtreeReader.reader.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadElementContentAsBase64Async>d__110>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_14F:
					int result2 = configuredTaskAwaiter4.GetResult();
					if (result2 > 0 || count == 0)
					{
						result = result2;
						goto IL_26D;
					}
					if (xmlSubtreeReader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", xmlSubtreeReader.reader.NodeType.ToString(), xmlSubtreeReader.reader as IXmlLineInfo);
					}
					xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
					xmlSubtreeReader.ProcessNamespaces();
					if (xmlSubtreeReader.reader.Depth == xmlSubtreeReader.initialDepth)
					{
						xmlSubtreeReader.state = XmlSubtreeReader.State.EndOfFile;
						xmlSubtreeReader.SetEmptyNode();
						goto IL_23C;
					}
					configuredTaskAwaiter3 = xmlSubtreeReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadElementContentAsBase64Async>d__110>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_234:
					configuredTaskAwaiter3.GetResult();
					IL_23C:
					result = 0;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_26D:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000A8F RID: 2703 RVA: 0x000306AC File Offset: 0x0002E8AC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000614 RID: 1556
			public int <>1__state;

			// Token: 0x04000615 RID: 1557
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000616 RID: 1558
			public XmlSubtreeReader <>4__this;

			// Token: 0x04000617 RID: 1559
			public byte[] buffer;

			// Token: 0x04000618 RID: 1560
			public int index;

			// Token: 0x04000619 RID: 1561
			public int count;

			// Token: 0x0400061A RID: 1562
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400061B RID: 1563
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200011C RID: 284
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBinHexAsync>d__111 : IAsyncStateMachine
		{
			// Token: 0x06000A90 RID: 2704 RVA: 0x000306BC File Offset: 0x0002E8BC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							switch (xmlSubtreeReader.state)
							{
							case XmlSubtreeReader.State.Initial:
							case XmlSubtreeReader.State.Error:
							case XmlSubtreeReader.State.EndOfFile:
							case XmlSubtreeReader.State.Closed:
								result = 0;
								goto IL_2F0;
							case XmlSubtreeReader.State.Interactive:
								xmlSubtreeReader.state = XmlSubtreeReader.State.ReadContentAsBinHex;
								break;
							case XmlSubtreeReader.State.PopNamespaceScope:
							case XmlSubtreeReader.State.ClearNsAttributes:
							{
								XmlNodeType nodeType = xmlSubtreeReader.NodeType;
								switch (nodeType)
								{
								case XmlNodeType.Element:
									throw xmlSubtreeReader.CreateReadContentAsException("ReadContentAsBinHex");
								case XmlNodeType.Attribute:
									if (xmlSubtreeReader.curNsAttr != -1 && xmlSubtreeReader.reader.CanReadBinaryContent)
									{
										xmlSubtreeReader.CheckBuffer(buffer, index, count);
										if (count == 0)
										{
											result = 0;
											goto IL_2F0;
										}
										if (xmlSubtreeReader.nsIncReadOffset == 0)
										{
											if (xmlSubtreeReader.binDecoder != null && xmlSubtreeReader.binDecoder is BinHexDecoder)
											{
												xmlSubtreeReader.binDecoder.Reset();
											}
											else
											{
												xmlSubtreeReader.binDecoder = new BinHexDecoder();
											}
										}
										if (xmlSubtreeReader.nsIncReadOffset == xmlSubtreeReader.curNode.value.Length)
										{
											result = 0;
											goto IL_2F0;
										}
										xmlSubtreeReader.binDecoder.SetNextOutputBuffer(buffer, index, count);
										xmlSubtreeReader.nsIncReadOffset += xmlSubtreeReader.binDecoder.Decode(xmlSubtreeReader.curNode.value, xmlSubtreeReader.nsIncReadOffset, xmlSubtreeReader.curNode.value.Length - xmlSubtreeReader.nsIncReadOffset);
										result = xmlSubtreeReader.binDecoder.DecodedCount;
										goto IL_2F0;
									}
									break;
								case XmlNodeType.Text:
									break;
								default:
									if (nodeType != XmlNodeType.EndElement)
									{
										result = 0;
										goto IL_2F0;
									}
									result = 0;
									goto IL_2F0;
								}
								configuredTaskAwaiter = xmlSubtreeReader.reader.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadContentAsBinHexAsync>d__111>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_218;
							}
							case XmlSubtreeReader.State.ReadElementContentAsBase64:
							case XmlSubtreeReader.State.ReadElementContentAsBinHex:
							case XmlSubtreeReader.State.ReadContentAsBase64:
								throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
							case XmlSubtreeReader.State.ReadContentAsBinHex:
								break;
							default:
								result = 0;
								goto IL_2F0;
							}
							configuredTaskAwaiter = xmlSubtreeReader.reader.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadContentAsBinHexAsync>d__111>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						int result2 = configuredTaskAwaiter.GetResult();
						if (result2 == 0)
						{
							xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
							xmlSubtreeReader.ProcessNamespaces();
						}
						result = result2;
						goto IL_2F0;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_218:
					result = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2F0:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000A91 RID: 2705 RVA: 0x000309EC File Offset: 0x0002EBEC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400061C RID: 1564
			public int <>1__state;

			// Token: 0x0400061D RID: 1565
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400061E RID: 1566
			public XmlSubtreeReader <>4__this;

			// Token: 0x0400061F RID: 1567
			public byte[] buffer;

			// Token: 0x04000620 RID: 1568
			public int index;

			// Token: 0x04000621 RID: 1569
			public int count;

			// Token: 0x04000622 RID: 1570
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200011D RID: 285
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBinHexAsync>d__112 : IAsyncStateMachine
		{
			// Token: 0x06000A92 RID: 2706 RVA: 0x000309FC File Offset: 0x0002EBFC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_14F;
					}
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_234;
					default:
						switch (xmlSubtreeReader.state)
						{
						case XmlSubtreeReader.State.Initial:
						case XmlSubtreeReader.State.Error:
						case XmlSubtreeReader.State.EndOfFile:
						case XmlSubtreeReader.State.Closed:
							result = 0;
							goto IL_26D;
						case XmlSubtreeReader.State.Interactive:
						case XmlSubtreeReader.State.PopNamespaceScope:
						case XmlSubtreeReader.State.ClearNsAttributes:
							configuredTaskAwaiter3 = xmlSubtreeReader.InitReadElementContentAsBinaryAsync(XmlSubtreeReader.State.ReadElementContentAsBinHex).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadElementContentAsBinHexAsync>d__112>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						case XmlSubtreeReader.State.ReadElementContentAsBase64:
						case XmlSubtreeReader.State.ReadContentAsBase64:
						case XmlSubtreeReader.State.ReadContentAsBinHex:
							throw new InvalidOperationException(Res.GetString("ReadContentAsBase64 and ReadContentAsBinHex method calls cannot be mixed with calls to ReadElementContentAsBase64 and ReadElementContentAsBinHex."));
						case XmlSubtreeReader.State.ReadElementContentAsBinHex:
							goto IL_D7;
						default:
							result = 0;
							goto IL_26D;
						}
						break;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						result = 0;
						goto IL_26D;
					}
					IL_D7:
					configuredTaskAwaiter4 = xmlSubtreeReader.reader.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadElementContentAsBinHexAsync>d__112>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_14F:
					int result2 = configuredTaskAwaiter4.GetResult();
					if (result2 > 0 || count == 0)
					{
						result = result2;
						goto IL_26D;
					}
					if (xmlSubtreeReader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", xmlSubtreeReader.reader.NodeType.ToString(), xmlSubtreeReader.reader as IXmlLineInfo);
					}
					xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
					xmlSubtreeReader.ProcessNamespaces();
					if (xmlSubtreeReader.reader.Depth == xmlSubtreeReader.initialDepth)
					{
						xmlSubtreeReader.state = XmlSubtreeReader.State.EndOfFile;
						xmlSubtreeReader.SetEmptyNode();
						goto IL_23C;
					}
					configuredTaskAwaiter3 = xmlSubtreeReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<ReadElementContentAsBinHexAsync>d__112>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_234:
					configuredTaskAwaiter3.GetResult();
					IL_23C:
					result = 0;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_26D:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000A93 RID: 2707 RVA: 0x00030CA8 File Offset: 0x0002EEA8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000623 RID: 1571
			public int <>1__state;

			// Token: 0x04000624 RID: 1572
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000625 RID: 1573
			public XmlSubtreeReader <>4__this;

			// Token: 0x04000626 RID: 1574
			public byte[] buffer;

			// Token: 0x04000627 RID: 1575
			public int index;

			// Token: 0x04000628 RID: 1576
			public int count;

			// Token: 0x04000629 RID: 1577
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x0400062A RID: 1578
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x0200011E RID: 286
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InitReadElementContentAsBinaryAsync>d__114 : IAsyncStateMachine
		{
			// Token: 0x06000A94 RID: 2708 RVA: 0x00030CB8 File Offset: 0x0002EEB8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_15F;
						}
						if (xmlSubtreeReader.NodeType != XmlNodeType.Element)
						{
							throw xmlSubtreeReader.reader.CreateReadElementContentAsException("ReadElementContentAsBase64");
						}
						isEmpty = xmlSubtreeReader.IsEmptyElement;
						configuredTaskAwaiter3 = xmlSubtreeReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<InitReadElementContentAsBinaryAsync>d__114>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (!configuredTaskAwaiter3.GetResult() || isEmpty)
					{
						result = false;
						goto IL_194;
					}
					XmlNodeType nodeType = xmlSubtreeReader.NodeType;
					if (nodeType == XmlNodeType.Element)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", xmlSubtreeReader.reader.NodeType.ToString(), xmlSubtreeReader.reader as IXmlLineInfo);
					}
					if (nodeType != XmlNodeType.EndElement)
					{
						xmlSubtreeReader.state = binaryState;
						result = true;
						goto IL_194;
					}
					xmlSubtreeReader.ProcessNamespaces();
					configuredTaskAwaiter3 = xmlSubtreeReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<InitReadElementContentAsBinaryAsync>d__114>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_15F:
					configuredTaskAwaiter3.GetResult();
					result = false;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_194:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000A95 RID: 2709 RVA: 0x00030E8C File Offset: 0x0002F08C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400062B RID: 1579
			public int <>1__state;

			// Token: 0x0400062C RID: 1580
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x0400062D RID: 1581
			public XmlSubtreeReader <>4__this;

			// Token: 0x0400062E RID: 1582
			private bool <isEmpty>5__1;

			// Token: 0x0400062F RID: 1583
			public XmlSubtreeReader.State binaryState;

			// Token: 0x04000630 RID: 1584
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200011F RID: 287
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishReadElementContentAsBinaryAsync>d__115 : IAsyncStateMachine
		{
			// Token: 0x06000A96 RID: 2710 RVA: 0x00030E9C File Offset: 0x0002F09C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_A8;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_123;
					case 2:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1F9;
					}
					default:
						bytes = new byte[256];
						if (xmlSubtreeReader.state != XmlSubtreeReader.State.ReadElementContentAsBase64)
						{
							goto IL_B4;
						}
						break;
					}
					IL_39:
					configuredTaskAwaiter3 = xmlSubtreeReader.reader.ReadContentAsBase64Async(bytes, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<FinishReadElementContentAsBinaryAsync>d__115>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_A8:
					if (configuredTaskAwaiter3.GetResult() <= 0)
					{
						goto IL_12D;
					}
					goto IL_39;
					IL_B4:
					configuredTaskAwaiter3 = xmlSubtreeReader.reader.ReadContentAsBinHexAsync(bytes, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<FinishReadElementContentAsBinaryAsync>d__115>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_123:
					if (configuredTaskAwaiter3.GetResult() > 0)
					{
						goto IL_B4;
					}
					IL_12D:
					if (xmlSubtreeReader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("'{0}' is an invalid XmlNodeType.", xmlSubtreeReader.reader.NodeType.ToString(), xmlSubtreeReader.reader as IXmlLineInfo);
					}
					xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
					xmlSubtreeReader.ProcessNamespaces();
					if (xmlSubtreeReader.reader.Depth == xmlSubtreeReader.initialDepth)
					{
						xmlSubtreeReader.state = XmlSubtreeReader.State.EndOfFile;
						xmlSubtreeReader.SetEmptyNode();
						result = false;
						goto IL_21C;
					}
					configuredTaskAwaiter4 = xmlSubtreeReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter4.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlSubtreeReader.<FinishReadElementContentAsBinaryAsync>d__115>(ref configuredTaskAwaiter4, ref this);
						return;
					}
					IL_1F9:
					result = configuredTaskAwaiter4.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_21C:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000A97 RID: 2711 RVA: 0x000310F8 File Offset: 0x0002F2F8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000631 RID: 1585
			public int <>1__state;

			// Token: 0x04000632 RID: 1586
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000633 RID: 1587
			public XmlSubtreeReader <>4__this;

			// Token: 0x04000634 RID: 1588
			private byte[] <bytes>5__1;

			// Token: 0x04000635 RID: 1589
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000636 RID: 1590
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000120 RID: 288
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishReadContentAsBinaryAsync>d__116 : IAsyncStateMachine
		{
			// Token: 0x06000A98 RID: 2712 RVA: 0x00031108 File Offset: 0x0002F308
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlSubtreeReader xmlSubtreeReader = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num == 0)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_A1;
					}
					if (num == 1)
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_11C;
					}
					bytes = new byte[256];
					if (xmlSubtreeReader.state == XmlSubtreeReader.State.ReadContentAsBase64)
					{
						goto IL_32;
					}
					IL_AD:
					configuredTaskAwaiter3 = xmlSubtreeReader.reader.ReadContentAsBinHexAsync(bytes, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<FinishReadContentAsBinaryAsync>d__116>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_11C:
					if (configuredTaskAwaiter3.GetResult() <= 0)
					{
						goto IL_126;
					}
					goto IL_AD;
					IL_32:
					configuredTaskAwaiter3 = xmlSubtreeReader.reader.ReadContentAsBase64Async(bytes, 0, 256).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlSubtreeReader.<FinishReadContentAsBinaryAsync>d__116>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_A1:
					if (configuredTaskAwaiter3.GetResult() > 0)
					{
						goto IL_32;
					}
					IL_126:
					xmlSubtreeReader.state = XmlSubtreeReader.State.Interactive;
					xmlSubtreeReader.ProcessNamespaces();
					if (xmlSubtreeReader.reader.Depth == xmlSubtreeReader.initialDepth)
					{
						xmlSubtreeReader.state = XmlSubtreeReader.State.EndOfFile;
						xmlSubtreeReader.SetEmptyNode();
						result = false;
					}
					else
					{
						result = true;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000A99 RID: 2713 RVA: 0x000312BC File Offset: 0x0002F4BC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000637 RID: 1591
			public int <>1__state;

			// Token: 0x04000638 RID: 1592
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000639 RID: 1593
			public XmlSubtreeReader <>4__this;

			// Token: 0x0400063A RID: 1594
			private byte[] <bytes>5__1;

			// Token: 0x0400063B RID: 1595
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
