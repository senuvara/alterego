﻿using System;
using System.Globalization;

namespace System.Xml
{
	// Token: 0x02000085 RID: 133
	internal struct BinXmlSqlMoney
	{
		// Token: 0x060003D7 RID: 983 RVA: 0x0000F712 File Offset: 0x0000D912
		public BinXmlSqlMoney(int v)
		{
			this.data = (long)v;
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x0000F71C File Offset: 0x0000D91C
		public BinXmlSqlMoney(long v)
		{
			this.data = v;
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x0000F728 File Offset: 0x0000D928
		public decimal ToDecimal()
		{
			bool isNegative;
			ulong num;
			if (this.data < 0L)
			{
				isNegative = true;
				num = (ulong)(-(ulong)this.data);
			}
			else
			{
				isNegative = false;
				num = (ulong)this.data;
			}
			return new decimal((int)num, (int)(num >> 32), 0, isNegative, 4);
		}

		// Token: 0x060003DA RID: 986 RVA: 0x0000F764 File Offset: 0x0000D964
		public override string ToString()
		{
			return this.ToDecimal().ToString("#0.00##", CultureInfo.InvariantCulture);
		}

		// Token: 0x04000292 RID: 658
		private long data;
	}
}
