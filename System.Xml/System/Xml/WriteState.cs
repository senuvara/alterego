﻿using System;

namespace System.Xml
{
	/// <summary>Specifies the state of the <see cref="T:System.Xml.XmlWriter" />.</summary>
	// Token: 0x020001DF RID: 479
	public enum WriteState
	{
		/// <summary>Indicates that a Write method has not yet been called.</summary>
		// Token: 0x04000BE4 RID: 3044
		Start,
		/// <summary>Indicates that the prolog is being written.</summary>
		// Token: 0x04000BE5 RID: 3045
		Prolog,
		/// <summary>Indicates that an element start tag is being written.</summary>
		// Token: 0x04000BE6 RID: 3046
		Element,
		/// <summary>Indicates that an attribute value is being written.</summary>
		// Token: 0x04000BE7 RID: 3047
		Attribute,
		/// <summary>Indicates that element content is being written.</summary>
		// Token: 0x04000BE8 RID: 3048
		Content,
		/// <summary>Indicates that the <see cref="M:System.Xml.XmlWriter.Close" /> method has been called.</summary>
		// Token: 0x04000BE9 RID: 3049
		Closed,
		/// <summary>An exception has been thrown, which has left the <see cref="T:System.Xml.XmlWriter" /> in an invalid state. You can call the <see cref="M:System.Xml.XmlWriter.Close" /> method to put the <see cref="T:System.Xml.XmlWriter" /> in the <see cref="F:System.Xml.WriteState.Closed" /> state. Any other <see cref="T:System.Xml.XmlWriter" /> method calls results in an <see cref="T:System.InvalidOperationException" />.</summary>
		// Token: 0x04000BEA RID: 3050
		Error
	}
}
