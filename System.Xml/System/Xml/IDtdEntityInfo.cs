﻿using System;

namespace System.Xml
{
	// Token: 0x020000A1 RID: 161
	internal interface IDtdEntityInfo
	{
		// Token: 0x1700010A RID: 266
		// (get) Token: 0x0600053B RID: 1339
		string Name { get; }

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x0600053C RID: 1340
		bool IsExternal { get; }

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x0600053D RID: 1341
		bool IsDeclaredInExternal { get; }

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x0600053E RID: 1342
		bool IsUnparsedEntity { get; }

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x0600053F RID: 1343
		bool IsParameterEntity { get; }

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x06000540 RID: 1344
		string BaseUriString { get; }

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x06000541 RID: 1345
		string DeclaredUriString { get; }

		// Token: 0x17000111 RID: 273
		// (get) Token: 0x06000542 RID: 1346
		string SystemId { get; }

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x06000543 RID: 1347
		string PublicId { get; }

		// Token: 0x17000113 RID: 275
		// (get) Token: 0x06000544 RID: 1348
		string Text { get; }

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x06000545 RID: 1349
		int LineNumber { get; }

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000546 RID: 1350
		int LinePosition { get; }
	}
}
