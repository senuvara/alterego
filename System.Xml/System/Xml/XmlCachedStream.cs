﻿using System;
using System.IO;

namespace System.Xml
{
	// Token: 0x02000295 RID: 661
	internal class XmlCachedStream : MemoryStream
	{
		// Token: 0x06001880 RID: 6272 RVA: 0x0008E428 File Offset: 0x0008C628
		internal XmlCachedStream(Uri uri, Stream stream)
		{
			this.uri = uri;
			try
			{
				byte[] buffer = new byte[4096];
				int count;
				while ((count = stream.Read(buffer, 0, 4096)) > 0)
				{
					this.Write(buffer, 0, count);
				}
				base.Position = 0L;
			}
			finally
			{
				stream.Close();
			}
		}

		// Token: 0x04001011 RID: 4113
		private const int MoveBufferSize = 4096;

		// Token: 0x04001012 RID: 4114
		private Uri uri;
	}
}
