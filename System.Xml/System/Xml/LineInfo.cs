﻿using System;

namespace System.Xml
{
	// Token: 0x0200024D RID: 589
	internal struct LineInfo
	{
		// Token: 0x06001691 RID: 5777 RVA: 0x0007C06D File Offset: 0x0007A26D
		public LineInfo(int lineNo, int linePos)
		{
			this.lineNo = lineNo;
			this.linePos = linePos;
		}

		// Token: 0x06001692 RID: 5778 RVA: 0x0007C06D File Offset: 0x0007A26D
		public void Set(int lineNo, int linePos)
		{
			this.lineNo = lineNo;
			this.linePos = linePos;
		}

		// Token: 0x04000E2D RID: 3629
		internal int lineNo;

		// Token: 0x04000E2E RID: 3630
		internal int linePos;
	}
}
