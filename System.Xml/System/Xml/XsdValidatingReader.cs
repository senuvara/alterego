﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace System.Xml
{
	// Token: 0x020001F3 RID: 499
	internal class XsdValidatingReader : XmlReader, IXmlSchemaInfo, IXmlLineInfo, IXmlNamespaceResolver
	{
		// Token: 0x060011A9 RID: 4521 RVA: 0x00067BF0 File Offset: 0x00065DF0
		internal XsdValidatingReader(XmlReader reader, XmlResolver xmlResolver, XmlReaderSettings readerSettings, XmlSchemaObject partialValidationType)
		{
			this.coreReader = reader;
			this.coreReaderNSResolver = (reader as IXmlNamespaceResolver);
			this.lineInfo = (reader as IXmlLineInfo);
			this.coreReaderNameTable = this.coreReader.NameTable;
			if (this.coreReaderNSResolver == null)
			{
				this.nsManager = new XmlNamespaceManager(this.coreReaderNameTable);
				this.manageNamespaces = true;
			}
			this.thisNSResolver = this;
			this.xmlResolver = xmlResolver;
			this.processInlineSchema = ((readerSettings.ValidationFlags & XmlSchemaValidationFlags.ProcessInlineSchema) > XmlSchemaValidationFlags.None);
			this.Init();
			this.SetupValidator(readerSettings, reader, partialValidationType);
			this.validationEvent = readerSettings.GetEventHandler();
		}

		// Token: 0x060011AA RID: 4522 RVA: 0x00067C99 File Offset: 0x00065E99
		internal XsdValidatingReader(XmlReader reader, XmlResolver xmlResolver, XmlReaderSettings readerSettings) : this(reader, xmlResolver, readerSettings, null)
		{
		}

		// Token: 0x060011AB RID: 4523 RVA: 0x00067CA8 File Offset: 0x00065EA8
		private void Init()
		{
			this.validationState = XsdValidatingReader.ValidatingReaderState.Init;
			this.defaultAttributes = new ArrayList();
			this.currentAttrIndex = -1;
			this.attributePSVINodes = new AttributePSVIInfo[8];
			this.valueGetter = new XmlValueGetter(this.GetStringValue);
			XsdValidatingReader.TypeOfString = typeof(string);
			this.xmlSchemaInfo = new XmlSchemaInfo();
			this.NsXmlNs = this.coreReaderNameTable.Add("http://www.w3.org/2000/xmlns/");
			this.NsXs = this.coreReaderNameTable.Add("http://www.w3.org/2001/XMLSchema");
			this.NsXsi = this.coreReaderNameTable.Add("http://www.w3.org/2001/XMLSchema-instance");
			this.XsiType = this.coreReaderNameTable.Add("type");
			this.XsiNil = this.coreReaderNameTable.Add("nil");
			this.XsiSchemaLocation = this.coreReaderNameTable.Add("schemaLocation");
			this.XsiNoNamespaceSchemaLocation = this.coreReaderNameTable.Add("noNamespaceSchemaLocation");
			this.XsdSchema = this.coreReaderNameTable.Add("schema");
		}

		// Token: 0x060011AC RID: 4524 RVA: 0x00067DB8 File Offset: 0x00065FB8
		private void SetupValidator(XmlReaderSettings readerSettings, XmlReader reader, XmlSchemaObject partialValidationType)
		{
			this.validator = new XmlSchemaValidator(this.coreReaderNameTable, readerSettings.Schemas, this.thisNSResolver, readerSettings.ValidationFlags);
			this.validator.XmlResolver = this.xmlResolver;
			this.validator.SourceUri = XmlConvert.ToUri(reader.BaseURI);
			this.validator.ValidationEventSender = this;
			this.validator.ValidationEventHandler += readerSettings.GetEventHandler();
			this.validator.LineInfoProvider = this.lineInfo;
			if (this.validator.ProcessSchemaHints)
			{
				this.validator.SchemaSet.ReaderSettings.DtdProcessing = readerSettings.DtdProcessing;
			}
			this.validator.SetDtdSchemaInfo(reader.DtdInfo);
			if (partialValidationType != null)
			{
				this.validator.Initialize(partialValidationType);
				return;
			}
			this.validator.Initialize();
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x060011AD RID: 4525 RVA: 0x00067E94 File Offset: 0x00066094
		public override XmlReaderSettings Settings
		{
			get
			{
				XmlReaderSettings xmlReaderSettings = this.coreReader.Settings;
				if (xmlReaderSettings != null)
				{
					xmlReaderSettings = xmlReaderSettings.Clone();
				}
				if (xmlReaderSettings == null)
				{
					xmlReaderSettings = new XmlReaderSettings();
				}
				xmlReaderSettings.Schemas = this.validator.SchemaSet;
				xmlReaderSettings.ValidationType = ValidationType.Schema;
				xmlReaderSettings.ValidationFlags = this.validator.ValidationFlags;
				xmlReaderSettings.ReadOnly = true;
				return xmlReaderSettings;
			}
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x060011AE RID: 4526 RVA: 0x00067EF4 File Offset: 0x000660F4
		public override XmlNodeType NodeType
		{
			get
			{
				if (this.validationState < XsdValidatingReader.ValidatingReaderState.None)
				{
					return this.cachedNode.NodeType;
				}
				XmlNodeType nodeType = this.coreReader.NodeType;
				if (nodeType == XmlNodeType.Whitespace && (this.validator.CurrentContentType == XmlSchemaContentType.TextOnly || this.validator.CurrentContentType == XmlSchemaContentType.Mixed))
				{
					return XmlNodeType.SignificantWhitespace;
				}
				return nodeType;
			}
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x060011AF RID: 4527 RVA: 0x00067F48 File Offset: 0x00066148
		public override string Name
		{
			get
			{
				if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute)
				{
					return this.coreReader.Name;
				}
				string defaultAttributePrefix = this.validator.GetDefaultAttributePrefix(this.cachedNode.Namespace);
				if (defaultAttributePrefix != null && defaultAttributePrefix.Length != 0)
				{
					return string.Concat(new string[]
					{
						defaultAttributePrefix + ":" + this.cachedNode.LocalName
					});
				}
				return this.cachedNode.LocalName;
			}
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x060011B0 RID: 4528 RVA: 0x00067FBC File Offset: 0x000661BC
		public override string LocalName
		{
			get
			{
				if (this.validationState < XsdValidatingReader.ValidatingReaderState.None)
				{
					return this.cachedNode.LocalName;
				}
				return this.coreReader.LocalName;
			}
		}

		// Token: 0x1700030B RID: 779
		// (get) Token: 0x060011B1 RID: 4529 RVA: 0x00067FDE File Offset: 0x000661DE
		public override string NamespaceURI
		{
			get
			{
				if (this.validationState < XsdValidatingReader.ValidatingReaderState.None)
				{
					return this.cachedNode.Namespace;
				}
				return this.coreReader.NamespaceURI;
			}
		}

		// Token: 0x1700030C RID: 780
		// (get) Token: 0x060011B2 RID: 4530 RVA: 0x00068000 File Offset: 0x00066200
		public override string Prefix
		{
			get
			{
				if (this.validationState < XsdValidatingReader.ValidatingReaderState.None)
				{
					return this.cachedNode.Prefix;
				}
				return this.coreReader.Prefix;
			}
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x060011B3 RID: 4531 RVA: 0x00068022 File Offset: 0x00066222
		public override bool HasValue
		{
			get
			{
				return this.validationState < XsdValidatingReader.ValidatingReaderState.None || this.coreReader.HasValue;
			}
		}

		// Token: 0x1700030E RID: 782
		// (get) Token: 0x060011B4 RID: 4532 RVA: 0x0006803A File Offset: 0x0006623A
		public override string Value
		{
			get
			{
				if (this.validationState < XsdValidatingReader.ValidatingReaderState.None)
				{
					return this.cachedNode.RawValue;
				}
				return this.coreReader.Value;
			}
		}

		// Token: 0x1700030F RID: 783
		// (get) Token: 0x060011B5 RID: 4533 RVA: 0x0006805C File Offset: 0x0006625C
		public override int Depth
		{
			get
			{
				if (this.validationState < XsdValidatingReader.ValidatingReaderState.None)
				{
					return this.cachedNode.Depth;
				}
				return this.coreReader.Depth;
			}
		}

		// Token: 0x17000310 RID: 784
		// (get) Token: 0x060011B6 RID: 4534 RVA: 0x0006807E File Offset: 0x0006627E
		public override string BaseURI
		{
			get
			{
				return this.coreReader.BaseURI;
			}
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x060011B7 RID: 4535 RVA: 0x0006808B File Offset: 0x0006628B
		public override bool IsEmptyElement
		{
			get
			{
				return this.coreReader.IsEmptyElement;
			}
		}

		// Token: 0x17000312 RID: 786
		// (get) Token: 0x060011B8 RID: 4536 RVA: 0x00068098 File Offset: 0x00066298
		public override bool IsDefault
		{
			get
			{
				return this.validationState == XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute || this.coreReader.IsDefault;
			}
		}

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x060011B9 RID: 4537 RVA: 0x000680B0 File Offset: 0x000662B0
		public override char QuoteChar
		{
			get
			{
				return this.coreReader.QuoteChar;
			}
		}

		// Token: 0x17000314 RID: 788
		// (get) Token: 0x060011BA RID: 4538 RVA: 0x000680BD File Offset: 0x000662BD
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.coreReader.XmlSpace;
			}
		}

		// Token: 0x17000315 RID: 789
		// (get) Token: 0x060011BB RID: 4539 RVA: 0x000680CA File Offset: 0x000662CA
		public override string XmlLang
		{
			get
			{
				return this.coreReader.XmlLang;
			}
		}

		// Token: 0x17000316 RID: 790
		// (get) Token: 0x060011BC RID: 4540 RVA: 0x00002068 File Offset: 0x00000268
		public override IXmlSchemaInfo SchemaInfo
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000317 RID: 791
		// (get) Token: 0x060011BD RID: 4541 RVA: 0x000680D8 File Offset: 0x000662D8
		public override Type ValueType
		{
			get
			{
				XmlNodeType nodeType = this.NodeType;
				if (nodeType != XmlNodeType.Element)
				{
					if (nodeType != XmlNodeType.Attribute)
					{
						if (nodeType != XmlNodeType.EndElement)
						{
							goto IL_62;
						}
					}
					else
					{
						if (this.attributePSVI != null && this.AttributeSchemaInfo.ContentType == XmlSchemaContentType.TextOnly)
						{
							return this.AttributeSchemaInfo.SchemaType.Datatype.ValueType;
						}
						goto IL_62;
					}
				}
				if (this.xmlSchemaInfo.ContentType == XmlSchemaContentType.TextOnly)
				{
					return this.xmlSchemaInfo.SchemaType.Datatype.ValueType;
				}
				IL_62:
				return XsdValidatingReader.TypeOfString;
			}
		}

		// Token: 0x060011BE RID: 4542 RVA: 0x0006814E File Offset: 0x0006634E
		public override object ReadContentAsObject()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsObject");
			}
			return this.InternalReadContentAsObject(true);
		}

		// Token: 0x060011BF RID: 4543 RVA: 0x00068170 File Offset: 0x00066370
		public override bool ReadContentAsBoolean()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsBoolean");
			}
			object value = this.InternalReadContentAsObject();
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			bool result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToBoolean(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToBoolean(value);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Boolean", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Boolean", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Boolean", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C0 RID: 4544 RVA: 0x0006823C File Offset: 0x0006643C
		public override DateTime ReadContentAsDateTime()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsDateTime");
			}
			object value = this.InternalReadContentAsObject();
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			DateTime result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToDateTime(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToDateTime(value);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "DateTime", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "DateTime", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "DateTime", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C1 RID: 4545 RVA: 0x00068308 File Offset: 0x00066508
		public override double ReadContentAsDouble()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsDouble");
			}
			object value = this.InternalReadContentAsObject();
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			double result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToDouble(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToDouble(value);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Double", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Double", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Double", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C2 RID: 4546 RVA: 0x000683D4 File Offset: 0x000665D4
		public override float ReadContentAsFloat()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsFloat");
			}
			object value = this.InternalReadContentAsObject();
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			float result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToSingle(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToSingle(value);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Float", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Float", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Float", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C3 RID: 4547 RVA: 0x000684A0 File Offset: 0x000666A0
		public override decimal ReadContentAsDecimal()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsDecimal");
			}
			object value = this.InternalReadContentAsObject();
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			decimal result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToDecimal(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToDecimal(value);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Decimal", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Decimal", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Decimal", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C4 RID: 4548 RVA: 0x0006856C File Offset: 0x0006676C
		public override int ReadContentAsInt()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsInt");
			}
			object value = this.InternalReadContentAsObject();
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			int result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToInt32(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToInt32(value);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Int", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Int", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Int", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C5 RID: 4549 RVA: 0x00068638 File Offset: 0x00066838
		public override long ReadContentAsLong()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsLong");
			}
			object value = this.InternalReadContentAsObject();
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			long result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToInt64(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToInt64(value);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Long", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Long", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Long", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C6 RID: 4550 RVA: 0x00068704 File Offset: 0x00066904
		public override string ReadContentAsString()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsString");
			}
			object obj = this.InternalReadContentAsObject();
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			string result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToString(obj);
				}
				else
				{
					result = (obj as string);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C7 RID: 4551 RVA: 0x000687CC File Offset: 0x000669CC
		public override object ReadContentAs(Type returnType, IXmlNamespaceResolver namespaceResolver)
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAs");
			}
			string text;
			object value = this.InternalReadContentAsObject(false, out text);
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			object result;
			try
			{
				if (xmlSchemaType != null)
				{
					if (returnType == typeof(DateTimeOffset) && xmlSchemaType.Datatype is Datatype_dateTimeBase)
					{
						value = text;
					}
					result = xmlSchemaType.ValueConverter.ChangeType(value, returnType);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ChangeType(value, returnType, namespaceResolver);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException3, this);
			}
			return result;
		}

		// Token: 0x060011C8 RID: 4552 RVA: 0x000688C4 File Offset: 0x00066AC4
		public override object ReadElementContentAsObject()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsObject");
			}
			XmlSchemaType xmlSchemaType;
			return this.InternalReadElementContentAsObject(out xmlSchemaType, true);
		}

		// Token: 0x060011C9 RID: 4553 RVA: 0x000688F0 File Offset: 0x00066AF0
		public override bool ReadElementContentAsBoolean()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsBoolean");
			}
			XmlSchemaType xmlSchemaType;
			object value = this.InternalReadElementContentAsObject(out xmlSchemaType);
			bool result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToBoolean(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToBoolean(value);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Boolean", innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Boolean", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Boolean", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011CA RID: 4554 RVA: 0x000689A4 File Offset: 0x00066BA4
		public override DateTime ReadElementContentAsDateTime()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsDateTime");
			}
			XmlSchemaType xmlSchemaType;
			object value = this.InternalReadElementContentAsObject(out xmlSchemaType);
			DateTime result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToDateTime(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToDateTime(value);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "DateTime", innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "DateTime", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "DateTime", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011CB RID: 4555 RVA: 0x00068A58 File Offset: 0x00066C58
		public override double ReadElementContentAsDouble()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsDouble");
			}
			XmlSchemaType xmlSchemaType;
			object value = this.InternalReadElementContentAsObject(out xmlSchemaType);
			double result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToDouble(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToDouble(value);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Double", innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Double", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Double", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011CC RID: 4556 RVA: 0x00068B0C File Offset: 0x00066D0C
		public override float ReadElementContentAsFloat()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsFloat");
			}
			XmlSchemaType xmlSchemaType;
			object value = this.InternalReadElementContentAsObject(out xmlSchemaType);
			float result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToSingle(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToSingle(value);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Float", innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Float", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Float", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011CD RID: 4557 RVA: 0x00068BC0 File Offset: 0x00066DC0
		public override decimal ReadElementContentAsDecimal()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsDecimal");
			}
			XmlSchemaType xmlSchemaType;
			object value = this.InternalReadElementContentAsObject(out xmlSchemaType);
			decimal result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToDecimal(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToDecimal(value);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Decimal", innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Decimal", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Decimal", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011CE RID: 4558 RVA: 0x00068C74 File Offset: 0x00066E74
		public override int ReadElementContentAsInt()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsInt");
			}
			XmlSchemaType xmlSchemaType;
			object value = this.InternalReadElementContentAsObject(out xmlSchemaType);
			int result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToInt32(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToInt32(value);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Int", innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Int", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Int", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011CF RID: 4559 RVA: 0x00068D28 File Offset: 0x00066F28
		public override long ReadElementContentAsLong()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsLong");
			}
			XmlSchemaType xmlSchemaType;
			object value = this.InternalReadElementContentAsObject(out xmlSchemaType);
			long result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToInt64(value);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ToInt64(value);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Long", innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Long", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "Long", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011D0 RID: 4560 RVA: 0x00068DDC File Offset: 0x00066FDC
		public override string ReadElementContentAsString()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsString");
			}
			XmlSchemaType xmlSchemaType;
			object obj = this.InternalReadElementContentAsObject(out xmlSchemaType);
			string result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToString(obj);
				}
				else
				{
					result = (obj as string);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException3, this);
			}
			return result;
		}

		// Token: 0x060011D1 RID: 4561 RVA: 0x00068E88 File Offset: 0x00067088
		public override object ReadElementContentAs(Type returnType, IXmlNamespaceResolver namespaceResolver)
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAs");
			}
			XmlSchemaType xmlSchemaType;
			string text;
			object value = this.InternalReadElementContentAsObject(out xmlSchemaType, false, out text);
			object result;
			try
			{
				if (xmlSchemaType != null)
				{
					if (returnType == typeof(DateTimeOffset) && xmlSchemaType.Datatype is Datatype_dateTimeBase)
					{
						value = text;
					}
					result = xmlSchemaType.ValueConverter.ChangeType(value, returnType, namespaceResolver);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ChangeType(value, returnType, namespaceResolver);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException3, this);
			}
			return result;
		}

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x060011D2 RID: 4562 RVA: 0x00068F68 File Offset: 0x00067168
		public override int AttributeCount
		{
			get
			{
				return this.attributeCount;
			}
		}

		// Token: 0x060011D3 RID: 4563 RVA: 0x00068F70 File Offset: 0x00067170
		public override string GetAttribute(string name)
		{
			string text = this.coreReader.GetAttribute(name);
			if (text == null && this.attributeCount > 0)
			{
				ValidatingReaderNodeData defaultAttribute = this.GetDefaultAttribute(name, false);
				if (defaultAttribute != null)
				{
					text = defaultAttribute.RawValue;
				}
			}
			return text;
		}

		// Token: 0x060011D4 RID: 4564 RVA: 0x00068FAC File Offset: 0x000671AC
		public override string GetAttribute(string name, string namespaceURI)
		{
			string attribute = this.coreReader.GetAttribute(name, namespaceURI);
			if (attribute == null && this.attributeCount > 0)
			{
				namespaceURI = ((namespaceURI == null) ? string.Empty : this.coreReaderNameTable.Get(namespaceURI));
				name = this.coreReaderNameTable.Get(name);
				if (name == null || namespaceURI == null)
				{
					return null;
				}
				ValidatingReaderNodeData defaultAttribute = this.GetDefaultAttribute(name, namespaceURI, false);
				if (defaultAttribute != null)
				{
					return defaultAttribute.RawValue;
				}
			}
			return attribute;
		}

		// Token: 0x060011D5 RID: 4565 RVA: 0x00069018 File Offset: 0x00067218
		public override string GetAttribute(int i)
		{
			if (this.attributeCount == 0)
			{
				return null;
			}
			if (i < this.coreReaderAttributeCount)
			{
				return this.coreReader.GetAttribute(i);
			}
			int index = i - this.coreReaderAttributeCount;
			return ((ValidatingReaderNodeData)this.defaultAttributes[index]).RawValue;
		}

		// Token: 0x060011D6 RID: 4566 RVA: 0x00069064 File Offset: 0x00067264
		public override bool MoveToAttribute(string name)
		{
			if (!this.coreReader.MoveToAttribute(name))
			{
				if (this.attributeCount > 0)
				{
					ValidatingReaderNodeData defaultAttribute = this.GetDefaultAttribute(name, true);
					if (defaultAttribute != null)
					{
						this.validationState = XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute;
						this.attributePSVI = defaultAttribute.AttInfo;
						this.cachedNode = defaultAttribute;
						goto IL_57;
					}
				}
				return false;
			}
			this.validationState = XsdValidatingReader.ValidatingReaderState.OnAttribute;
			this.attributePSVI = this.GetAttributePSVI(name);
			IL_57:
			if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper.Finish();
				this.validationState = this.savedState;
			}
			return true;
		}

		// Token: 0x060011D7 RID: 4567 RVA: 0x000690EC File Offset: 0x000672EC
		public override bool MoveToAttribute(string name, string ns)
		{
			name = this.coreReaderNameTable.Get(name);
			ns = ((ns != null) ? this.coreReaderNameTable.Get(ns) : string.Empty);
			if (name == null || ns == null)
			{
				return false;
			}
			if (this.coreReader.MoveToAttribute(name, ns))
			{
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnAttribute;
				if (this.inlineSchemaParser == null)
				{
					this.attributePSVI = this.GetAttributePSVI(name, ns);
				}
				else
				{
					this.attributePSVI = null;
				}
			}
			else
			{
				ValidatingReaderNodeData defaultAttribute = this.GetDefaultAttribute(name, ns, true);
				if (defaultAttribute == null)
				{
					return false;
				}
				this.attributePSVI = defaultAttribute.AttInfo;
				this.cachedNode = defaultAttribute;
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute;
			}
			if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper.Finish();
				this.validationState = this.savedState;
			}
			return true;
		}

		// Token: 0x060011D8 RID: 4568 RVA: 0x000691AC File Offset: 0x000673AC
		public override void MoveToAttribute(int i)
		{
			if (i < 0 || i >= this.attributeCount)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			this.currentAttrIndex = i;
			if (i < this.coreReaderAttributeCount)
			{
				this.coreReader.MoveToAttribute(i);
				if (this.inlineSchemaParser == null)
				{
					this.attributePSVI = this.attributePSVINodes[i];
				}
				else
				{
					this.attributePSVI = null;
				}
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnAttribute;
			}
			else
			{
				int index = i - this.coreReaderAttributeCount;
				this.cachedNode = (ValidatingReaderNodeData)this.defaultAttributes[index];
				this.attributePSVI = this.cachedNode.AttInfo;
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute;
			}
			if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper.Finish();
				this.validationState = this.savedState;
			}
		}

		// Token: 0x060011D9 RID: 4569 RVA: 0x00069270 File Offset: 0x00067470
		public override bool MoveToFirstAttribute()
		{
			if (this.coreReader.MoveToFirstAttribute())
			{
				this.currentAttrIndex = 0;
				if (this.inlineSchemaParser == null)
				{
					this.attributePSVI = this.attributePSVINodes[0];
				}
				else
				{
					this.attributePSVI = null;
				}
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnAttribute;
			}
			else
			{
				if (this.defaultAttributes.Count <= 0)
				{
					return false;
				}
				this.cachedNode = (ValidatingReaderNodeData)this.defaultAttributes[0];
				this.attributePSVI = this.cachedNode.AttInfo;
				this.currentAttrIndex = 0;
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute;
			}
			if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper.Finish();
				this.validationState = this.savedState;
			}
			return true;
		}

		// Token: 0x060011DA RID: 4570 RVA: 0x00069324 File Offset: 0x00067524
		public override bool MoveToNextAttribute()
		{
			if (this.currentAttrIndex + 1 < this.coreReaderAttributeCount)
			{
				this.coreReader.MoveToNextAttribute();
				this.currentAttrIndex++;
				if (this.inlineSchemaParser == null)
				{
					this.attributePSVI = this.attributePSVINodes[this.currentAttrIndex];
				}
				else
				{
					this.attributePSVI = null;
				}
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnAttribute;
			}
			else
			{
				if (this.currentAttrIndex + 1 >= this.attributeCount)
				{
					return false;
				}
				int num = this.currentAttrIndex + 1;
				this.currentAttrIndex = num;
				int index = num - this.coreReaderAttributeCount;
				this.cachedNode = (ValidatingReaderNodeData)this.defaultAttributes[index];
				this.attributePSVI = this.cachedNode.AttInfo;
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute;
			}
			if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper.Finish();
				this.validationState = this.savedState;
			}
			return true;
		}

		// Token: 0x060011DB RID: 4571 RVA: 0x00069405 File Offset: 0x00067605
		public override bool MoveToElement()
		{
			if (this.coreReader.MoveToElement() || this.validationState < XsdValidatingReader.ValidatingReaderState.None)
			{
				this.currentAttrIndex = -1;
				this.validationState = XsdValidatingReader.ValidatingReaderState.ClearAttributes;
				return true;
			}
			return false;
		}

		// Token: 0x060011DC RID: 4572 RVA: 0x00069430 File Offset: 0x00067630
		public override bool Read()
		{
			switch (this.validationState)
			{
			case XsdValidatingReader.ValidatingReaderState.OnReadAttributeValue:
			case XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute:
			case XsdValidatingReader.ValidatingReaderState.OnAttribute:
			case XsdValidatingReader.ValidatingReaderState.ClearAttributes:
				this.ClearAttributesInfo();
				if (this.inlineSchemaParser != null)
				{
					this.validationState = XsdValidatingReader.ValidatingReaderState.ParseInlineSchema;
					goto IL_7C;
				}
				this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
				break;
			case XsdValidatingReader.ValidatingReaderState.None:
				return false;
			case XsdValidatingReader.ValidatingReaderState.Init:
				this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
				if (this.coreReader.ReadState == ReadState.Interactive)
				{
					this.ProcessReaderEvent();
					return true;
				}
				break;
			case XsdValidatingReader.ValidatingReaderState.Read:
				break;
			case XsdValidatingReader.ValidatingReaderState.ParseInlineSchema:
				goto IL_7C;
			case XsdValidatingReader.ValidatingReaderState.ReadAhead:
				this.ClearAttributesInfo();
				this.ProcessReaderEvent();
				this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
				return true;
			case XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent:
				this.validationState = this.savedState;
				this.readBinaryHelper.Finish();
				return this.Read();
			case XsdValidatingReader.ValidatingReaderState.ReaderClosed:
			case XsdValidatingReader.ValidatingReaderState.EOF:
				return false;
			default:
				return false;
			}
			if (this.coreReader.Read())
			{
				this.ProcessReaderEvent();
				return true;
			}
			this.validator.EndValidation();
			if (this.coreReader.EOF)
			{
				this.validationState = XsdValidatingReader.ValidatingReaderState.EOF;
			}
			return false;
			IL_7C:
			this.ProcessInlineSchema();
			return true;
		}

		// Token: 0x17000319 RID: 793
		// (get) Token: 0x060011DD RID: 4573 RVA: 0x00069537 File Offset: 0x00067737
		public override bool EOF
		{
			get
			{
				return this.coreReader.EOF;
			}
		}

		// Token: 0x060011DE RID: 4574 RVA: 0x00069544 File Offset: 0x00067744
		public override void Close()
		{
			this.coreReader.Close();
			this.validationState = XsdValidatingReader.ValidatingReaderState.ReaderClosed;
		}

		// Token: 0x1700031A RID: 794
		// (get) Token: 0x060011DF RID: 4575 RVA: 0x00069558 File Offset: 0x00067758
		public override ReadState ReadState
		{
			get
			{
				if (this.validationState != XsdValidatingReader.ValidatingReaderState.Init)
				{
					return this.coreReader.ReadState;
				}
				return ReadState.Initial;
			}
		}

		// Token: 0x060011E0 RID: 4576 RVA: 0x00069570 File Offset: 0x00067770
		public override void Skip()
		{
			int depth = this.Depth;
			XmlNodeType nodeType = this.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.Attribute)
				{
					goto IL_81;
				}
				this.MoveToElement();
			}
			if (!this.coreReader.IsEmptyElement)
			{
				bool flag = true;
				if ((this.xmlSchemaInfo.IsUnionType || this.xmlSchemaInfo.IsDefault) && this.coreReader is XsdCachingReader)
				{
					flag = false;
				}
				this.coreReader.Skip();
				this.validationState = XsdValidatingReader.ValidatingReaderState.ReadAhead;
				if (flag)
				{
					this.validator.SkipToEndElement(this.xmlSchemaInfo);
				}
			}
			IL_81:
			this.Read();
		}

		// Token: 0x1700031B RID: 795
		// (get) Token: 0x060011E1 RID: 4577 RVA: 0x00069605 File Offset: 0x00067805
		public override XmlNameTable NameTable
		{
			get
			{
				return this.coreReaderNameTable;
			}
		}

		// Token: 0x060011E2 RID: 4578 RVA: 0x0006960D File Offset: 0x0006780D
		public override string LookupNamespace(string prefix)
		{
			return this.thisNSResolver.LookupNamespace(prefix);
		}

		// Token: 0x060011E3 RID: 4579 RVA: 0x00007AE0 File Offset: 0x00005CE0
		public override void ResolveEntity()
		{
			throw new InvalidOperationException();
		}

		// Token: 0x060011E4 RID: 4580 RVA: 0x0006961C File Offset: 0x0006781C
		public override bool ReadAttributeValue()
		{
			if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper.Finish();
				this.validationState = this.savedState;
			}
			if (this.NodeType != XmlNodeType.Attribute)
			{
				return false;
			}
			if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute)
			{
				this.cachedNode = this.CreateDummyTextNode(this.cachedNode.RawValue, this.cachedNode.Depth + 1);
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadAttributeValue;
				return true;
			}
			return this.coreReader.ReadAttributeValue();
		}

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x060011E5 RID: 4581 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool CanReadBinaryContent
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060011E6 RID: 4582 RVA: 0x00069698 File Offset: 0x00067898
		public override int ReadContentAsBase64(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
				this.savedState = this.validationState;
			}
			this.validationState = this.savedState;
			int result = this.readBinaryHelper.ReadContentAsBase64(buffer, index, count);
			this.savedState = this.validationState;
			this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
			return result;
		}

		// Token: 0x060011E7 RID: 4583 RVA: 0x00069704 File Offset: 0x00067904
		public override int ReadContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
				this.savedState = this.validationState;
			}
			this.validationState = this.savedState;
			int result = this.readBinaryHelper.ReadContentAsBinHex(buffer, index, count);
			this.savedState = this.validationState;
			this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
			return result;
		}

		// Token: 0x060011E8 RID: 4584 RVA: 0x00069770 File Offset: 0x00067970
		public override int ReadElementContentAsBase64(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
				this.savedState = this.validationState;
			}
			this.validationState = this.savedState;
			int result = this.readBinaryHelper.ReadElementContentAsBase64(buffer, index, count);
			this.savedState = this.validationState;
			this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
			return result;
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x000697DC File Offset: 0x000679DC
		public override int ReadElementContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
			{
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
				this.savedState = this.validationState;
			}
			this.validationState = this.savedState;
			int result = this.readBinaryHelper.ReadElementContentAsBinHex(buffer, index, count);
			this.savedState = this.validationState;
			this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
			return result;
		}

		// Token: 0x1700031D RID: 797
		// (get) Token: 0x060011EA RID: 4586 RVA: 0x00069848 File Offset: 0x00067A48
		bool IXmlSchemaInfo.IsDefault
		{
			get
			{
				XmlNodeType nodeType = this.NodeType;
				if (nodeType != XmlNodeType.Element)
				{
					if (nodeType != XmlNodeType.Attribute)
					{
						if (nodeType == XmlNodeType.EndElement)
						{
							return this.xmlSchemaInfo.IsDefault;
						}
					}
					else if (this.attributePSVI != null)
					{
						return this.AttributeSchemaInfo.IsDefault;
					}
					return false;
				}
				if (!this.coreReader.IsEmptyElement)
				{
					this.GetIsDefault();
				}
				return this.xmlSchemaInfo.IsDefault;
			}
		}

		// Token: 0x1700031E RID: 798
		// (get) Token: 0x060011EB RID: 4587 RVA: 0x000698AC File Offset: 0x00067AAC
		bool IXmlSchemaInfo.IsNil
		{
			get
			{
				XmlNodeType nodeType = this.NodeType;
				return (nodeType == XmlNodeType.Element || nodeType == XmlNodeType.EndElement) && this.xmlSchemaInfo.IsNil;
			}
		}

		// Token: 0x1700031F RID: 799
		// (get) Token: 0x060011EC RID: 4588 RVA: 0x000698D8 File Offset: 0x00067AD8
		XmlSchemaValidity IXmlSchemaInfo.Validity
		{
			get
			{
				XmlNodeType nodeType = this.NodeType;
				if (nodeType != XmlNodeType.Element)
				{
					if (nodeType != XmlNodeType.Attribute)
					{
						if (nodeType == XmlNodeType.EndElement)
						{
							return this.xmlSchemaInfo.Validity;
						}
					}
					else if (this.attributePSVI != null)
					{
						return this.AttributeSchemaInfo.Validity;
					}
					return XmlSchemaValidity.NotKnown;
				}
				if (this.coreReader.IsEmptyElement)
				{
					return this.xmlSchemaInfo.Validity;
				}
				if (this.xmlSchemaInfo.Validity == XmlSchemaValidity.Valid)
				{
					return XmlSchemaValidity.NotKnown;
				}
				return this.xmlSchemaInfo.Validity;
			}
		}

		// Token: 0x17000320 RID: 800
		// (get) Token: 0x060011ED RID: 4589 RVA: 0x00069954 File Offset: 0x00067B54
		XmlSchemaSimpleType IXmlSchemaInfo.MemberType
		{
			get
			{
				XmlNodeType nodeType = this.NodeType;
				if (nodeType == XmlNodeType.Element)
				{
					if (!this.coreReader.IsEmptyElement)
					{
						this.GetMemberType();
					}
					return this.xmlSchemaInfo.MemberType;
				}
				if (nodeType != XmlNodeType.Attribute)
				{
					if (nodeType != XmlNodeType.EndElement)
					{
						return null;
					}
					return this.xmlSchemaInfo.MemberType;
				}
				else
				{
					if (this.attributePSVI != null)
					{
						return this.AttributeSchemaInfo.MemberType;
					}
					return null;
				}
			}
		}

		// Token: 0x17000321 RID: 801
		// (get) Token: 0x060011EE RID: 4590 RVA: 0x000699BC File Offset: 0x00067BBC
		XmlSchemaType IXmlSchemaInfo.SchemaType
		{
			get
			{
				XmlNodeType nodeType = this.NodeType;
				if (nodeType != XmlNodeType.Element)
				{
					if (nodeType != XmlNodeType.Attribute)
					{
						if (nodeType != XmlNodeType.EndElement)
						{
							return null;
						}
					}
					else
					{
						if (this.attributePSVI != null)
						{
							return this.AttributeSchemaInfo.SchemaType;
						}
						return null;
					}
				}
				return this.xmlSchemaInfo.SchemaType;
			}
		}

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x060011EF RID: 4591 RVA: 0x00069A00 File Offset: 0x00067C00
		XmlSchemaElement IXmlSchemaInfo.SchemaElement
		{
			get
			{
				if (this.NodeType == XmlNodeType.Element || this.NodeType == XmlNodeType.EndElement)
				{
					return this.xmlSchemaInfo.SchemaElement;
				}
				return null;
			}
		}

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x060011F0 RID: 4592 RVA: 0x00069A22 File Offset: 0x00067C22
		XmlSchemaAttribute IXmlSchemaInfo.SchemaAttribute
		{
			get
			{
				if (this.NodeType == XmlNodeType.Attribute && this.attributePSVI != null)
				{
					return this.AttributeSchemaInfo.SchemaAttribute;
				}
				return null;
			}
		}

		// Token: 0x060011F1 RID: 4593 RVA: 0x000033DE File Offset: 0x000015DE
		public bool HasLineInfo()
		{
			return true;
		}

		// Token: 0x17000324 RID: 804
		// (get) Token: 0x060011F2 RID: 4594 RVA: 0x00069A42 File Offset: 0x00067C42
		public int LineNumber
		{
			get
			{
				if (this.lineInfo != null)
				{
					return this.lineInfo.LineNumber;
				}
				return 0;
			}
		}

		// Token: 0x17000325 RID: 805
		// (get) Token: 0x060011F3 RID: 4595 RVA: 0x00069A59 File Offset: 0x00067C59
		public int LinePosition
		{
			get
			{
				if (this.lineInfo != null)
				{
					return this.lineInfo.LinePosition;
				}
				return 0;
			}
		}

		// Token: 0x060011F4 RID: 4596 RVA: 0x00069A70 File Offset: 0x00067C70
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			if (this.coreReaderNSResolver != null)
			{
				return this.coreReaderNSResolver.GetNamespacesInScope(scope);
			}
			return this.nsManager.GetNamespacesInScope(scope);
		}

		// Token: 0x060011F5 RID: 4597 RVA: 0x00069A93 File Offset: 0x00067C93
		string IXmlNamespaceResolver.LookupNamespace(string prefix)
		{
			if (this.coreReaderNSResolver != null)
			{
				return this.coreReaderNSResolver.LookupNamespace(prefix);
			}
			return this.nsManager.LookupNamespace(prefix);
		}

		// Token: 0x060011F6 RID: 4598 RVA: 0x00069AB6 File Offset: 0x00067CB6
		string IXmlNamespaceResolver.LookupPrefix(string namespaceName)
		{
			if (this.coreReaderNSResolver != null)
			{
				return this.coreReaderNSResolver.LookupPrefix(namespaceName);
			}
			return this.nsManager.LookupPrefix(namespaceName);
		}

		// Token: 0x060011F7 RID: 4599 RVA: 0x00069AD9 File Offset: 0x00067CD9
		private object GetStringValue()
		{
			return this.coreReader.Value;
		}

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x060011F8 RID: 4600 RVA: 0x00069AE6 File Offset: 0x00067CE6
		private XmlSchemaType ElementXmlType
		{
			get
			{
				return this.xmlSchemaInfo.XmlType;
			}
		}

		// Token: 0x17000327 RID: 807
		// (get) Token: 0x060011F9 RID: 4601 RVA: 0x00069AF3 File Offset: 0x00067CF3
		private XmlSchemaType AttributeXmlType
		{
			get
			{
				if (this.attributePSVI != null)
				{
					return this.AttributeSchemaInfo.XmlType;
				}
				return null;
			}
		}

		// Token: 0x17000328 RID: 808
		// (get) Token: 0x060011FA RID: 4602 RVA: 0x00069B0A File Offset: 0x00067D0A
		private XmlSchemaInfo AttributeSchemaInfo
		{
			get
			{
				return this.attributePSVI.attributeSchemaInfo;
			}
		}

		// Token: 0x060011FB RID: 4603 RVA: 0x00069B18 File Offset: 0x00067D18
		private void ProcessReaderEvent()
		{
			if (this.replayCache)
			{
				return;
			}
			switch (this.coreReader.NodeType)
			{
			case XmlNodeType.Element:
				this.ProcessElementEvent();
				return;
			case XmlNodeType.Attribute:
			case XmlNodeType.Entity:
			case XmlNodeType.ProcessingInstruction:
			case XmlNodeType.Comment:
			case XmlNodeType.Document:
			case XmlNodeType.DocumentFragment:
			case XmlNodeType.Notation:
				break;
			case XmlNodeType.Text:
			case XmlNodeType.CDATA:
				this.validator.ValidateText(new XmlValueGetter(this.GetStringValue));
				return;
			case XmlNodeType.EntityReference:
				throw new InvalidOperationException();
			case XmlNodeType.DocumentType:
				this.validator.SetDtdSchemaInfo(this.coreReader.DtdInfo);
				break;
			case XmlNodeType.Whitespace:
			case XmlNodeType.SignificantWhitespace:
				this.validator.ValidateWhitespace(new XmlValueGetter(this.GetStringValue));
				return;
			case XmlNodeType.EndElement:
				this.ProcessEndElementEvent();
				return;
			default:
				return;
			}
		}

		// Token: 0x060011FC RID: 4604 RVA: 0x00069BDC File Offset: 0x00067DDC
		private void ProcessElementEvent()
		{
			if (!this.processInlineSchema || !this.IsXSDRoot(this.coreReader.LocalName, this.coreReader.NamespaceURI) || this.coreReader.Depth <= 0)
			{
				this.atomicValue = null;
				this.originalAtomicValueString = null;
				this.xmlSchemaInfo.Clear();
				if (this.manageNamespaces)
				{
					this.nsManager.PushScope();
				}
				string xsiSchemaLocation = null;
				string xsiNoNamespaceSchemaLocation = null;
				string xsiNil = null;
				string xsiType = null;
				if (this.coreReader.MoveToFirstAttribute())
				{
					do
					{
						string namespaceURI = this.coreReader.NamespaceURI;
						string localName = this.coreReader.LocalName;
						if (Ref.Equal(namespaceURI, this.NsXsi))
						{
							if (Ref.Equal(localName, this.XsiSchemaLocation))
							{
								xsiSchemaLocation = this.coreReader.Value;
							}
							else if (Ref.Equal(localName, this.XsiNoNamespaceSchemaLocation))
							{
								xsiNoNamespaceSchemaLocation = this.coreReader.Value;
							}
							else if (Ref.Equal(localName, this.XsiType))
							{
								xsiType = this.coreReader.Value;
							}
							else if (Ref.Equal(localName, this.XsiNil))
							{
								xsiNil = this.coreReader.Value;
							}
						}
						if (this.manageNamespaces && Ref.Equal(this.coreReader.NamespaceURI, this.NsXmlNs))
						{
							this.nsManager.AddNamespace((this.coreReader.Prefix.Length == 0) ? string.Empty : this.coreReader.LocalName, this.coreReader.Value);
						}
					}
					while (this.coreReader.MoveToNextAttribute());
					this.coreReader.MoveToElement();
				}
				this.validator.ValidateElement(this.coreReader.LocalName, this.coreReader.NamespaceURI, this.xmlSchemaInfo, xsiType, xsiNil, xsiSchemaLocation, xsiNoNamespaceSchemaLocation);
				this.ValidateAttributes();
				this.validator.ValidateEndOfAttributes(this.xmlSchemaInfo);
				if (this.coreReader.IsEmptyElement)
				{
					this.ProcessEndElementEvent();
				}
				this.validationState = XsdValidatingReader.ValidatingReaderState.ClearAttributes;
				return;
			}
			this.xmlSchemaInfo.Clear();
			this.attributeCount = (this.coreReaderAttributeCount = this.coreReader.AttributeCount);
			if (!this.coreReader.IsEmptyElement)
			{
				this.inlineSchemaParser = new Parser(SchemaType.XSD, this.coreReaderNameTable, this.validator.SchemaSet.GetSchemaNames(this.coreReaderNameTable), this.validationEvent);
				this.inlineSchemaParser.StartParsing(this.coreReader, null);
				this.inlineSchemaParser.ParseReaderNode();
				this.validationState = XsdValidatingReader.ValidatingReaderState.ParseInlineSchema;
				return;
			}
			this.validationState = XsdValidatingReader.ValidatingReaderState.ClearAttributes;
		}

		// Token: 0x060011FD RID: 4605 RVA: 0x00069E64 File Offset: 0x00068064
		private void ProcessEndElementEvent()
		{
			this.atomicValue = this.validator.ValidateEndElement(this.xmlSchemaInfo);
			this.originalAtomicValueString = this.GetOriginalAtomicValueStringOfElement();
			if (this.xmlSchemaInfo.IsDefault)
			{
				int depth = this.coreReader.Depth;
				this.coreReader = this.GetCachingReader();
				this.cachingReader.RecordTextNode(this.xmlSchemaInfo.XmlType.ValueConverter.ToString(this.atomicValue), this.originalAtomicValueString, depth + 1, 0, 0);
				this.cachingReader.RecordEndElementNode();
				this.cachingReader.SetToReplayMode();
				this.replayCache = true;
				return;
			}
			if (this.manageNamespaces)
			{
				this.nsManager.PopScope();
			}
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x00069F20 File Offset: 0x00068120
		private void ValidateAttributes()
		{
			this.attributeCount = (this.coreReaderAttributeCount = this.coreReader.AttributeCount);
			int num = 0;
			bool flag = false;
			if (this.coreReader.MoveToFirstAttribute())
			{
				do
				{
					string localName = this.coreReader.LocalName;
					string namespaceURI = this.coreReader.NamespaceURI;
					AttributePSVIInfo attributePSVIInfo = this.AddAttributePSVI(num);
					attributePSVIInfo.localName = localName;
					attributePSVIInfo.namespaceUri = namespaceURI;
					if (namespaceURI == this.NsXmlNs)
					{
						num++;
					}
					else
					{
						attributePSVIInfo.typedAttributeValue = this.validator.ValidateAttribute(localName, namespaceURI, this.valueGetter, attributePSVIInfo.attributeSchemaInfo);
						if (!flag)
						{
							flag = (attributePSVIInfo.attributeSchemaInfo.Validity == XmlSchemaValidity.Invalid);
						}
						num++;
					}
				}
				while (this.coreReader.MoveToNextAttribute());
			}
			this.coreReader.MoveToElement();
			if (flag)
			{
				this.xmlSchemaInfo.Validity = XmlSchemaValidity.Invalid;
			}
			this.validator.GetUnspecifiedDefaultAttributes(this.defaultAttributes, true);
			this.attributeCount += this.defaultAttributes.Count;
		}

		// Token: 0x060011FF RID: 4607 RVA: 0x0006A029 File Offset: 0x00068229
		private void ClearAttributesInfo()
		{
			this.attributeCount = 0;
			this.coreReaderAttributeCount = 0;
			this.currentAttrIndex = -1;
			this.defaultAttributes.Clear();
			this.attributePSVI = null;
		}

		// Token: 0x06001200 RID: 4608 RVA: 0x0006A054 File Offset: 0x00068254
		private AttributePSVIInfo GetAttributePSVI(string name)
		{
			if (this.inlineSchemaParser != null)
			{
				return null;
			}
			string text;
			string text2;
			ValidateNames.SplitQName(name, out text, out text2);
			text = this.coreReaderNameTable.Add(text);
			text2 = this.coreReaderNameTable.Add(text2);
			string ns;
			if (text.Length == 0)
			{
				ns = string.Empty;
			}
			else
			{
				ns = this.thisNSResolver.LookupNamespace(text);
			}
			return this.GetAttributePSVI(text2, ns);
		}

		// Token: 0x06001201 RID: 4609 RVA: 0x0006A0B4 File Offset: 0x000682B4
		private AttributePSVIInfo GetAttributePSVI(string localName, string ns)
		{
			for (int i = 0; i < this.coreReaderAttributeCount; i++)
			{
				AttributePSVIInfo attributePSVIInfo = this.attributePSVINodes[i];
				if (attributePSVIInfo != null && Ref.Equal(localName, attributePSVIInfo.localName) && Ref.Equal(ns, attributePSVIInfo.namespaceUri))
				{
					this.currentAttrIndex = i;
					return attributePSVIInfo;
				}
			}
			return null;
		}

		// Token: 0x06001202 RID: 4610 RVA: 0x0006A108 File Offset: 0x00068308
		private ValidatingReaderNodeData GetDefaultAttribute(string name, bool updatePosition)
		{
			string text;
			string text2;
			ValidateNames.SplitQName(name, out text, out text2);
			text = this.coreReaderNameTable.Add(text);
			text2 = this.coreReaderNameTable.Add(text2);
			string ns;
			if (text.Length == 0)
			{
				ns = string.Empty;
			}
			else
			{
				ns = this.thisNSResolver.LookupNamespace(text);
			}
			return this.GetDefaultAttribute(text2, ns, updatePosition);
		}

		// Token: 0x06001203 RID: 4611 RVA: 0x0006A160 File Offset: 0x00068360
		private ValidatingReaderNodeData GetDefaultAttribute(string attrLocalName, string ns, bool updatePosition)
		{
			for (int i = 0; i < this.defaultAttributes.Count; i++)
			{
				ValidatingReaderNodeData validatingReaderNodeData = (ValidatingReaderNodeData)this.defaultAttributes[i];
				if (Ref.Equal(validatingReaderNodeData.LocalName, attrLocalName) && Ref.Equal(validatingReaderNodeData.Namespace, ns))
				{
					if (updatePosition)
					{
						this.currentAttrIndex = this.coreReader.AttributeCount + i;
					}
					return validatingReaderNodeData;
				}
			}
			return null;
		}

		// Token: 0x06001204 RID: 4612 RVA: 0x0006A1CC File Offset: 0x000683CC
		private AttributePSVIInfo AddAttributePSVI(int attIndex)
		{
			AttributePSVIInfo attributePSVIInfo = this.attributePSVINodes[attIndex];
			if (attributePSVIInfo != null)
			{
				attributePSVIInfo.Reset();
				return attributePSVIInfo;
			}
			if (attIndex >= this.attributePSVINodes.Length - 1)
			{
				AttributePSVIInfo[] destinationArray = new AttributePSVIInfo[this.attributePSVINodes.Length * 2];
				Array.Copy(this.attributePSVINodes, 0, destinationArray, 0, this.attributePSVINodes.Length);
				this.attributePSVINodes = destinationArray;
			}
			attributePSVIInfo = this.attributePSVINodes[attIndex];
			if (attributePSVIInfo == null)
			{
				attributePSVIInfo = new AttributePSVIInfo();
				this.attributePSVINodes[attIndex] = attributePSVIInfo;
			}
			return attributePSVIInfo;
		}

		// Token: 0x06001205 RID: 4613 RVA: 0x0006A243 File Offset: 0x00068443
		private bool IsXSDRoot(string localName, string ns)
		{
			return Ref.Equal(ns, this.NsXs) && Ref.Equal(localName, this.XsdSchema);
		}

		// Token: 0x06001206 RID: 4614 RVA: 0x0006A264 File Offset: 0x00068464
		private void ProcessInlineSchema()
		{
			if (this.coreReader.Read())
			{
				if (this.coreReader.NodeType == XmlNodeType.Element)
				{
					this.attributeCount = (this.coreReaderAttributeCount = this.coreReader.AttributeCount);
				}
				else
				{
					this.ClearAttributesInfo();
				}
				if (!this.inlineSchemaParser.ParseReaderNode())
				{
					this.inlineSchemaParser.FinishParsing();
					XmlSchema xmlSchema = this.inlineSchemaParser.XmlSchema;
					this.validator.AddSchema(xmlSchema);
					this.inlineSchemaParser = null;
					this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
				}
			}
		}

		// Token: 0x06001207 RID: 4615 RVA: 0x0006A2ED File Offset: 0x000684ED
		private object InternalReadContentAsObject()
		{
			return this.InternalReadContentAsObject(false);
		}

		// Token: 0x06001208 RID: 4616 RVA: 0x0006A2F8 File Offset: 0x000684F8
		private object InternalReadContentAsObject(bool unwrapTypedValue)
		{
			string text;
			return this.InternalReadContentAsObject(unwrapTypedValue, out text);
		}

		// Token: 0x06001209 RID: 4617 RVA: 0x0006A310 File Offset: 0x00068510
		private object InternalReadContentAsObject(bool unwrapTypedValue, out string originalStringValue)
		{
			XmlNodeType nodeType = this.NodeType;
			if (nodeType == XmlNodeType.Attribute)
			{
				originalStringValue = this.Value;
				if (this.attributePSVI != null && this.attributePSVI.typedAttributeValue != null)
				{
					if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute)
					{
						XmlSchemaAttribute schemaAttribute = this.attributePSVI.attributeSchemaInfo.SchemaAttribute;
						originalStringValue = ((schemaAttribute.DefaultValue != null) ? schemaAttribute.DefaultValue : schemaAttribute.FixedValue);
					}
					return this.ReturnBoxedValue(this.attributePSVI.typedAttributeValue, this.AttributeSchemaInfo.XmlType, unwrapTypedValue);
				}
				return this.Value;
			}
			else if (nodeType == XmlNodeType.EndElement)
			{
				if (this.atomicValue != null)
				{
					originalStringValue = this.originalAtomicValueString;
					return this.atomicValue;
				}
				originalStringValue = string.Empty;
				return string.Empty;
			}
			else
			{
				if (this.validator.CurrentContentType == XmlSchemaContentType.TextOnly)
				{
					object result = this.ReturnBoxedValue(this.ReadTillEndElement(), this.xmlSchemaInfo.XmlType, unwrapTypedValue);
					originalStringValue = this.originalAtomicValueString;
					return result;
				}
				XsdCachingReader xsdCachingReader = this.coreReader as XsdCachingReader;
				if (xsdCachingReader != null)
				{
					originalStringValue = xsdCachingReader.ReadOriginalContentAsString();
				}
				else
				{
					originalStringValue = base.InternalReadContentAsString();
				}
				return originalStringValue;
			}
		}

		// Token: 0x0600120A RID: 4618 RVA: 0x0006A416 File Offset: 0x00068616
		private object InternalReadElementContentAsObject(out XmlSchemaType xmlType)
		{
			return this.InternalReadElementContentAsObject(out xmlType, false);
		}

		// Token: 0x0600120B RID: 4619 RVA: 0x0006A420 File Offset: 0x00068620
		private object InternalReadElementContentAsObject(out XmlSchemaType xmlType, bool unwrapTypedValue)
		{
			string text;
			return this.InternalReadElementContentAsObject(out xmlType, unwrapTypedValue, out text);
		}

		// Token: 0x0600120C RID: 4620 RVA: 0x0006A438 File Offset: 0x00068638
		private object InternalReadElementContentAsObject(out XmlSchemaType xmlType, bool unwrapTypedValue, out string originalString)
		{
			xmlType = null;
			object result;
			if (this.IsEmptyElement)
			{
				if (this.xmlSchemaInfo.ContentType == XmlSchemaContentType.TextOnly)
				{
					result = this.ReturnBoxedValue(this.atomicValue, this.xmlSchemaInfo.XmlType, unwrapTypedValue);
				}
				else
				{
					result = this.atomicValue;
				}
				originalString = this.originalAtomicValueString;
				xmlType = this.ElementXmlType;
				this.Read();
				return result;
			}
			this.Read();
			if (this.NodeType == XmlNodeType.EndElement)
			{
				if (this.xmlSchemaInfo.IsDefault)
				{
					if (this.xmlSchemaInfo.ContentType == XmlSchemaContentType.TextOnly)
					{
						result = this.ReturnBoxedValue(this.atomicValue, this.xmlSchemaInfo.XmlType, unwrapTypedValue);
					}
					else
					{
						result = this.atomicValue;
					}
					originalString = this.originalAtomicValueString;
				}
				else
				{
					result = string.Empty;
					originalString = string.Empty;
				}
			}
			else
			{
				if (this.NodeType == XmlNodeType.Element)
				{
					throw new XmlException("ReadElementContentAs() methods cannot be called on an element that has child elements.", string.Empty, this);
				}
				result = this.InternalReadContentAsObject(unwrapTypedValue, out originalString);
				if (this.NodeType != XmlNodeType.EndElement)
				{
					throw new XmlException("ReadElementContentAs() methods cannot be called on an element that has child elements.", string.Empty, this);
				}
			}
			xmlType = this.ElementXmlType;
			this.Read();
			return result;
		}

		// Token: 0x0600120D RID: 4621 RVA: 0x0006A550 File Offset: 0x00068750
		private object ReadTillEndElement()
		{
			if (this.atomicValue == null)
			{
				while (this.coreReader.Read())
				{
					if (!this.replayCache)
					{
						switch (this.coreReader.NodeType)
						{
						case XmlNodeType.Element:
							this.ProcessReaderEvent();
							goto IL_10B;
						case XmlNodeType.Text:
						case XmlNodeType.CDATA:
							this.validator.ValidateText(new XmlValueGetter(this.GetStringValue));
							break;
						case XmlNodeType.Whitespace:
						case XmlNodeType.SignificantWhitespace:
							this.validator.ValidateWhitespace(new XmlValueGetter(this.GetStringValue));
							break;
						case XmlNodeType.EndElement:
							this.atomicValue = this.validator.ValidateEndElement(this.xmlSchemaInfo);
							this.originalAtomicValueString = this.GetOriginalAtomicValueStringOfElement();
							if (this.manageNamespaces)
							{
								this.nsManager.PopScope();
								goto IL_10B;
							}
							goto IL_10B;
						}
					}
				}
			}
			else
			{
				if (this.atomicValue == this)
				{
					this.atomicValue = null;
				}
				this.SwitchReader();
			}
			IL_10B:
			return this.atomicValue;
		}

		// Token: 0x0600120E RID: 4622 RVA: 0x0006A670 File Offset: 0x00068870
		private void SwitchReader()
		{
			XsdCachingReader xsdCachingReader = this.coreReader as XsdCachingReader;
			if (xsdCachingReader != null)
			{
				this.coreReader = xsdCachingReader.GetCoreReader();
			}
			this.replayCache = false;
		}

		// Token: 0x0600120F RID: 4623 RVA: 0x0006A6A0 File Offset: 0x000688A0
		private void ReadAheadForMemberType()
		{
			while (this.coreReader.Read())
			{
				switch (this.coreReader.NodeType)
				{
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
					this.validator.ValidateText(new XmlValueGetter(this.GetStringValue));
					break;
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					this.validator.ValidateWhitespace(new XmlValueGetter(this.GetStringValue));
					break;
				case XmlNodeType.EndElement:
					this.atomicValue = this.validator.ValidateEndElement(this.xmlSchemaInfo);
					this.originalAtomicValueString = this.GetOriginalAtomicValueStringOfElement();
					if (this.atomicValue == null)
					{
						this.atomicValue = this;
						return;
					}
					if (this.xmlSchemaInfo.IsDefault)
					{
						this.cachingReader.SwitchTextNodeAndEndElement(this.xmlSchemaInfo.XmlType.ValueConverter.ToString(this.atomicValue), this.originalAtomicValueString);
						return;
					}
					return;
				}
			}
		}

		// Token: 0x06001210 RID: 4624 RVA: 0x0006A7BC File Offset: 0x000689BC
		private void GetIsDefault()
		{
			if (!(this.coreReader is XsdCachingReader) && this.xmlSchemaInfo.HasDefaultValue)
			{
				this.coreReader = this.GetCachingReader();
				if (this.xmlSchemaInfo.IsUnionType && !this.xmlSchemaInfo.IsNil)
				{
					this.ReadAheadForMemberType();
				}
				else if (this.coreReader.Read())
				{
					switch (this.coreReader.NodeType)
					{
					case XmlNodeType.Text:
					case XmlNodeType.CDATA:
						this.validator.ValidateText(new XmlValueGetter(this.GetStringValue));
						break;
					case XmlNodeType.Whitespace:
					case XmlNodeType.SignificantWhitespace:
						this.validator.ValidateWhitespace(new XmlValueGetter(this.GetStringValue));
						break;
					case XmlNodeType.EndElement:
						this.atomicValue = this.validator.ValidateEndElement(this.xmlSchemaInfo);
						this.originalAtomicValueString = this.GetOriginalAtomicValueStringOfElement();
						if (this.xmlSchemaInfo.IsDefault)
						{
							this.cachingReader.SwitchTextNodeAndEndElement(this.xmlSchemaInfo.XmlType.ValueConverter.ToString(this.atomicValue), this.originalAtomicValueString);
						}
						break;
					}
				}
				this.cachingReader.SetToReplayMode();
				this.replayCache = true;
			}
		}

		// Token: 0x06001211 RID: 4625 RVA: 0x0006A920 File Offset: 0x00068B20
		private void GetMemberType()
		{
			if (this.xmlSchemaInfo.MemberType != null || this.atomicValue == this)
			{
				return;
			}
			if (!(this.coreReader is XsdCachingReader) && this.xmlSchemaInfo.IsUnionType && !this.xmlSchemaInfo.IsNil)
			{
				this.coreReader = this.GetCachingReader();
				this.ReadAheadForMemberType();
				this.cachingReader.SetToReplayMode();
				this.replayCache = true;
			}
		}

		// Token: 0x06001212 RID: 4626 RVA: 0x0006A990 File Offset: 0x00068B90
		private object ReturnBoxedValue(object typedValue, XmlSchemaType xmlType, bool unWrap)
		{
			if (typedValue != null)
			{
				if (unWrap && xmlType.Datatype.Variety == XmlSchemaDatatypeVariety.List && (xmlType.Datatype as Datatype_List).ItemType.Variety == XmlSchemaDatatypeVariety.Union)
				{
					typedValue = xmlType.ValueConverter.ChangeType(typedValue, xmlType.Datatype.ValueType, this.thisNSResolver);
				}
				return typedValue;
			}
			typedValue = this.validator.GetConcatenatedValue();
			return typedValue;
		}

		// Token: 0x06001213 RID: 4627 RVA: 0x0006A9F8 File Offset: 0x00068BF8
		private XsdCachingReader GetCachingReader()
		{
			if (this.cachingReader == null)
			{
				this.cachingReader = new XsdCachingReader(this.coreReader, this.lineInfo, new CachingEventHandler(this.CachingCallBack));
			}
			else
			{
				this.cachingReader.Reset(this.coreReader);
			}
			this.lineInfo = this.cachingReader;
			return this.cachingReader;
		}

		// Token: 0x06001214 RID: 4628 RVA: 0x0006AA55 File Offset: 0x00068C55
		internal ValidatingReaderNodeData CreateDummyTextNode(string attributeValue, int depth)
		{
			if (this.textNode == null)
			{
				this.textNode = new ValidatingReaderNodeData(XmlNodeType.Text);
			}
			this.textNode.Depth = depth;
			this.textNode.RawValue = attributeValue;
			return this.textNode;
		}

		// Token: 0x06001215 RID: 4629 RVA: 0x0006AA89 File Offset: 0x00068C89
		internal void CachingCallBack(XsdCachingReader cachingReader)
		{
			this.coreReader = cachingReader.GetCoreReader();
			this.lineInfo = cachingReader.GetLineInfo();
			this.replayCache = false;
		}

		// Token: 0x06001216 RID: 4630 RVA: 0x0006AAAC File Offset: 0x00068CAC
		private string GetOriginalAtomicValueStringOfElement()
		{
			if (!this.xmlSchemaInfo.IsDefault)
			{
				return this.validator.GetConcatenatedValue();
			}
			XmlSchemaElement schemaElement = this.xmlSchemaInfo.SchemaElement;
			if (schemaElement == null)
			{
				return string.Empty;
			}
			if (schemaElement.DefaultValue == null)
			{
				return schemaElement.FixedValue;
			}
			return schemaElement.DefaultValue;
		}

		// Token: 0x06001217 RID: 4631 RVA: 0x0006AAFC File Offset: 0x00068CFC
		public override Task<string> GetValueAsync()
		{
			if (this.validationState < XsdValidatingReader.ValidatingReaderState.None)
			{
				return Task.FromResult<string>(this.cachedNode.RawValue);
			}
			return this.coreReader.GetValueAsync();
		}

		// Token: 0x06001218 RID: 4632 RVA: 0x0006AB23 File Offset: 0x00068D23
		public override Task<object> ReadContentAsObjectAsync()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsObject");
			}
			return this.InternalReadContentAsObjectAsync(true);
		}

		// Token: 0x06001219 RID: 4633 RVA: 0x0006AB48 File Offset: 0x00068D48
		public override async Task<string> ReadContentAsStringAsync()
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAsString");
			}
			object obj = await this.InternalReadContentAsObjectAsync().ConfigureAwait(false);
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			string result;
			try
			{
				if (xmlSchemaType != null)
				{
					result = xmlSchemaType.ValueConverter.ToString(obj);
				}
				else
				{
					result = (obj as string);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException3, this);
			}
			return result;
		}

		// Token: 0x0600121A RID: 4634 RVA: 0x0006AB90 File Offset: 0x00068D90
		public override async Task<object> ReadContentAsAsync(Type returnType, IXmlNamespaceResolver namespaceResolver)
		{
			if (!XmlReader.CanReadContentAs(this.NodeType))
			{
				throw base.CreateReadContentAsException("ReadContentAs");
			}
			object obj = await this.InternalReadContentAsObjectTupleAsync(false).ConfigureAwait(false);
			string item = obj.Item1;
			object value = obj.Item2;
			XmlSchemaType xmlSchemaType = (this.NodeType == XmlNodeType.Attribute) ? this.AttributeXmlType : this.ElementXmlType;
			object result;
			try
			{
				if (xmlSchemaType != null)
				{
					if (returnType == typeof(DateTimeOffset) && xmlSchemaType.Datatype is Datatype_dateTimeBase)
					{
						value = item;
					}
					result = xmlSchemaType.ValueConverter.ChangeType(value, returnType);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ChangeType(value, returnType, namespaceResolver);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException3, this);
			}
			return result;
		}

		// Token: 0x0600121B RID: 4635 RVA: 0x0006ABE8 File Offset: 0x00068DE8
		public override async Task<object> ReadElementContentAsObjectAsync()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsObject");
			}
			return (await this.InternalReadElementContentAsObjectAsync(true).ConfigureAwait(false)).Item2;
		}

		// Token: 0x0600121C RID: 4636 RVA: 0x0006AC30 File Offset: 0x00068E30
		public override async Task<string> ReadElementContentAsStringAsync()
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAsString");
			}
			object obj = await this.InternalReadElementContentAsObjectAsync().ConfigureAwait(false);
			XmlSchemaType item = obj.Item1;
			object item2 = obj.Item2;
			string result;
			try
			{
				if (item != null)
				{
					result = item.ValueConverter.ToString(item2);
				}
				else
				{
					result = (item2 as string);
				}
			}
			catch (InvalidCastException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException, this);
			}
			catch (FormatException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException3, this);
			}
			return result;
		}

		// Token: 0x0600121D RID: 4637 RVA: 0x0006AC78 File Offset: 0x00068E78
		public override async Task<object> ReadElementContentAsAsync(Type returnType, IXmlNamespaceResolver namespaceResolver)
		{
			if (this.NodeType != XmlNodeType.Element)
			{
				throw base.CreateReadElementContentAsException("ReadElementContentAs");
			}
			object obj = await this.InternalReadElementContentAsObjectTupleAsync(false).ConfigureAwait(false);
			XmlSchemaType item = obj.Item1;
			string item2 = obj.Item2;
			object value = obj.Item3;
			object result;
			try
			{
				if (item != null)
				{
					if (returnType == typeof(DateTimeOffset) && item.Datatype is Datatype_dateTimeBase)
					{
						value = item2;
					}
					result = item.ValueConverter.ChangeType(value, returnType, namespaceResolver);
				}
				else
				{
					result = XmlUntypedConverter.Untyped.ChangeType(value, returnType, namespaceResolver);
				}
			}
			catch (FormatException innerException)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException, this);
			}
			catch (InvalidCastException innerException2)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException2, this);
			}
			catch (OverflowException innerException3)
			{
				throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException3, this);
			}
			return result;
		}

		// Token: 0x0600121E RID: 4638 RVA: 0x0006ACD0 File Offset: 0x00068ED0
		private Task<bool> ReadAsync_Read(Task<bool> task)
		{
			if (!task.IsSuccess())
			{
				return this._ReadAsync_Read(task);
			}
			if (task.Result)
			{
				return this.ProcessReaderEventAsync().ReturnTaskBoolWhenFinish(true);
			}
			this.validator.EndValidation();
			if (this.coreReader.EOF)
			{
				this.validationState = XsdValidatingReader.ValidatingReaderState.EOF;
			}
			return AsyncHelper.DoneTaskFalse;
		}

		// Token: 0x0600121F RID: 4639 RVA: 0x0006AD28 File Offset: 0x00068F28
		private async Task<bool> _ReadAsync_Read(Task<bool> task)
		{
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			bool result;
			if (configuredTaskAwaiter.GetResult())
			{
				await this.ProcessReaderEventAsync().ConfigureAwait(false);
				result = true;
			}
			else
			{
				this.validator.EndValidation();
				if (this.coreReader.EOF)
				{
					this.validationState = XsdValidatingReader.ValidatingReaderState.EOF;
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06001220 RID: 4640 RVA: 0x0006AD75 File Offset: 0x00068F75
		private Task<bool> ReadAsync_ReadAhead(Task task)
		{
			if (task.IsSuccess())
			{
				this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
				return AsyncHelper.DoneTaskTrue;
			}
			return this._ReadAsync_ReadAhead(task);
		}

		// Token: 0x06001221 RID: 4641 RVA: 0x0006AD94 File Offset: 0x00068F94
		private async Task<bool> _ReadAsync_ReadAhead(Task task)
		{
			await task.ConfigureAwait(false);
			this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
			return true;
		}

		// Token: 0x06001222 RID: 4642 RVA: 0x0006ADE4 File Offset: 0x00068FE4
		public override Task<bool> ReadAsync()
		{
			switch (this.validationState)
			{
			case XsdValidatingReader.ValidatingReaderState.OnReadAttributeValue:
			case XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute:
			case XsdValidatingReader.ValidatingReaderState.OnAttribute:
			case XsdValidatingReader.ValidatingReaderState.ClearAttributes:
				this.ClearAttributesInfo();
				if (this.inlineSchemaParser != null)
				{
					this.validationState = XsdValidatingReader.ValidatingReaderState.ParseInlineSchema;
					goto IL_59;
				}
				this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
				break;
			case XsdValidatingReader.ValidatingReaderState.None:
				goto IL_F0;
			case XsdValidatingReader.ValidatingReaderState.Init:
				this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
				if (this.coreReader.ReadState == ReadState.Interactive)
				{
					return this.ProcessReaderEventAsync().ReturnTaskBoolWhenFinish(true);
				}
				break;
			case XsdValidatingReader.ValidatingReaderState.Read:
				break;
			case XsdValidatingReader.ValidatingReaderState.ParseInlineSchema:
				goto IL_59;
			case XsdValidatingReader.ValidatingReaderState.ReadAhead:
			{
				this.ClearAttributesInfo();
				Task task = this.ProcessReaderEventAsync();
				return this.ReadAsync_ReadAhead(task);
			}
			case XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent:
				this.validationState = this.savedState;
				return this.readBinaryHelper.FinishAsync().CallBoolTaskFuncWhenFinish(new Func<Task<bool>>(this.ReadAsync));
			case XsdValidatingReader.ValidatingReaderState.ReaderClosed:
			case XsdValidatingReader.ValidatingReaderState.EOF:
				return AsyncHelper.DoneTaskFalse;
			default:
				goto IL_F0;
			}
			Task<bool> task2 = this.coreReader.ReadAsync();
			return this.ReadAsync_Read(task2);
			IL_59:
			return this.ProcessInlineSchemaAsync().ReturnTaskBoolWhenFinish(true);
			IL_F0:
			return AsyncHelper.DoneTaskFalse;
		}

		// Token: 0x06001223 RID: 4643 RVA: 0x0006AEE8 File Offset: 0x000690E8
		public override async Task SkipAsync()
		{
			int depth = this.Depth;
			XmlNodeType nodeType = this.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.Attribute)
				{
					goto IL_116;
				}
				this.MoveToElement();
			}
			if (!this.coreReader.IsEmptyElement)
			{
				bool callSkipToEndElem = true;
				if ((this.xmlSchemaInfo.IsUnionType || this.xmlSchemaInfo.IsDefault) && this.coreReader is XsdCachingReader)
				{
					callSkipToEndElem = false;
				}
				await this.coreReader.SkipAsync().ConfigureAwait(false);
				this.validationState = XsdValidatingReader.ValidatingReaderState.ReadAhead;
				if (callSkipToEndElem)
				{
					this.validator.SkipToEndElement(this.xmlSchemaInfo);
				}
			}
			IL_116:
			await this.ReadAsync().ConfigureAwait(false);
		}

		// Token: 0x06001224 RID: 4644 RVA: 0x0006AF30 File Offset: 0x00069130
		public override async Task<int> ReadContentAsBase64Async(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
				{
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
					this.savedState = this.validationState;
				}
				this.validationState = this.savedState;
				int num = await this.readBinaryHelper.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
				this.savedState = this.validationState;
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
				result = num;
			}
			return result;
		}

		// Token: 0x06001225 RID: 4645 RVA: 0x0006AF90 File Offset: 0x00069190
		public override async Task<int> ReadContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
				{
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
					this.savedState = this.validationState;
				}
				this.validationState = this.savedState;
				int num = await this.readBinaryHelper.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
				this.savedState = this.validationState;
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
				result = num;
			}
			return result;
		}

		// Token: 0x06001226 RID: 4646 RVA: 0x0006AFF0 File Offset: 0x000691F0
		public override async Task<int> ReadElementContentAsBase64Async(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
				{
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
					this.savedState = this.validationState;
				}
				this.validationState = this.savedState;
				int num = await this.readBinaryHelper.ReadElementContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
				this.savedState = this.validationState;
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
				result = num;
			}
			return result;
		}

		// Token: 0x06001227 RID: 4647 RVA: 0x0006B050 File Offset: 0x00069250
		public override async Task<int> ReadElementContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
				{
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
					this.savedState = this.validationState;
				}
				this.validationState = this.savedState;
				int num = await this.readBinaryHelper.ReadElementContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
				this.savedState = this.validationState;
				this.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
				result = num;
			}
			return result;
		}

		// Token: 0x06001228 RID: 4648 RVA: 0x0006B0B0 File Offset: 0x000692B0
		private Task ProcessReaderEventAsync()
		{
			if (this.replayCache)
			{
				return AsyncHelper.DoneTask;
			}
			switch (this.coreReader.NodeType)
			{
			case XmlNodeType.Element:
				return this.ProcessElementEventAsync();
			case XmlNodeType.Text:
			case XmlNodeType.CDATA:
				this.validator.ValidateText(new XmlValueGetter(this.GetStringValue));
				break;
			case XmlNodeType.EntityReference:
				throw new InvalidOperationException();
			case XmlNodeType.DocumentType:
				this.validator.SetDtdSchemaInfo(this.coreReader.DtdInfo);
				break;
			case XmlNodeType.Whitespace:
			case XmlNodeType.SignificantWhitespace:
				this.validator.ValidateWhitespace(new XmlValueGetter(this.GetStringValue));
				break;
			case XmlNodeType.EndElement:
				return this.ProcessEndElementEventAsync();
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06001229 RID: 4649 RVA: 0x0006B180 File Offset: 0x00069380
		private async Task ProcessElementEventAsync()
		{
			if (this.processInlineSchema && this.IsXSDRoot(this.coreReader.LocalName, this.coreReader.NamespaceURI) && this.coreReader.Depth > 0)
			{
				this.xmlSchemaInfo.Clear();
				this.attributeCount = (this.coreReaderAttributeCount = this.coreReader.AttributeCount);
				if (!this.coreReader.IsEmptyElement)
				{
					this.inlineSchemaParser = new Parser(SchemaType.XSD, this.coreReaderNameTable, this.validator.SchemaSet.GetSchemaNames(this.coreReaderNameTable), this.validationEvent);
					await this.inlineSchemaParser.StartParsingAsync(this.coreReader, null).ConfigureAwait(false);
					this.inlineSchemaParser.ParseReaderNode();
					this.validationState = XsdValidatingReader.ValidatingReaderState.ParseInlineSchema;
				}
				else
				{
					this.validationState = XsdValidatingReader.ValidatingReaderState.ClearAttributes;
				}
			}
			else
			{
				this.atomicValue = null;
				this.originalAtomicValueString = null;
				this.xmlSchemaInfo.Clear();
				if (this.manageNamespaces)
				{
					this.nsManager.PushScope();
				}
				string xsiSchemaLocation = null;
				string xsiNoNamespaceSchemaLocation = null;
				string xsiNil = null;
				string xsiType = null;
				if (this.coreReader.MoveToFirstAttribute())
				{
					do
					{
						string namespaceURI = this.coreReader.NamespaceURI;
						string localName = this.coreReader.LocalName;
						if (Ref.Equal(namespaceURI, this.NsXsi))
						{
							if (Ref.Equal(localName, this.XsiSchemaLocation))
							{
								xsiSchemaLocation = this.coreReader.Value;
							}
							else if (Ref.Equal(localName, this.XsiNoNamespaceSchemaLocation))
							{
								xsiNoNamespaceSchemaLocation = this.coreReader.Value;
							}
							else if (Ref.Equal(localName, this.XsiType))
							{
								xsiType = this.coreReader.Value;
							}
							else if (Ref.Equal(localName, this.XsiNil))
							{
								xsiNil = this.coreReader.Value;
							}
						}
						if (this.manageNamespaces && Ref.Equal(this.coreReader.NamespaceURI, this.NsXmlNs))
						{
							this.nsManager.AddNamespace((this.coreReader.Prefix.Length == 0) ? string.Empty : this.coreReader.LocalName, this.coreReader.Value);
						}
					}
					while (this.coreReader.MoveToNextAttribute());
					this.coreReader.MoveToElement();
				}
				this.validator.ValidateElement(this.coreReader.LocalName, this.coreReader.NamespaceURI, this.xmlSchemaInfo, xsiType, xsiNil, xsiSchemaLocation, xsiNoNamespaceSchemaLocation);
				this.ValidateAttributes();
				this.validator.ValidateEndOfAttributes(this.xmlSchemaInfo);
				if (this.coreReader.IsEmptyElement)
				{
					await this.ProcessEndElementEventAsync().ConfigureAwait(false);
				}
				this.validationState = XsdValidatingReader.ValidatingReaderState.ClearAttributes;
			}
		}

		// Token: 0x0600122A RID: 4650 RVA: 0x0006B1C8 File Offset: 0x000693C8
		private async Task ProcessEndElementEventAsync()
		{
			this.atomicValue = this.validator.ValidateEndElement(this.xmlSchemaInfo);
			this.originalAtomicValueString = this.GetOriginalAtomicValueStringOfElement();
			if (this.xmlSchemaInfo.IsDefault)
			{
				int depth = this.coreReader.Depth;
				this.coreReader = this.GetCachingReader();
				this.cachingReader.RecordTextNode(this.xmlSchemaInfo.XmlType.ValueConverter.ToString(this.atomicValue), this.originalAtomicValueString, depth + 1, 0, 0);
				this.cachingReader.RecordEndElementNode();
				await this.cachingReader.SetToReplayModeAsync().ConfigureAwait(false);
				this.replayCache = true;
			}
			else if (this.manageNamespaces)
			{
				this.nsManager.PopScope();
			}
		}

		// Token: 0x0600122B RID: 4651 RVA: 0x0006B210 File Offset: 0x00069410
		private async Task ProcessInlineSchemaAsync()
		{
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			if (configuredTaskAwaiter.GetResult())
			{
				if (this.coreReader.NodeType == XmlNodeType.Element)
				{
					int num = this.coreReader.AttributeCount;
					this.coreReaderAttributeCount = num;
					this.attributeCount = num;
				}
				else
				{
					this.ClearAttributesInfo();
				}
				if (!this.inlineSchemaParser.ParseReaderNode())
				{
					this.inlineSchemaParser.FinishParsing();
					XmlSchema xmlSchema = this.inlineSchemaParser.XmlSchema;
					this.validator.AddSchema(xmlSchema);
					this.inlineSchemaParser = null;
					this.validationState = XsdValidatingReader.ValidatingReaderState.Read;
				}
			}
		}

		// Token: 0x0600122C RID: 4652 RVA: 0x0006B255 File Offset: 0x00069455
		private Task<object> InternalReadContentAsObjectAsync()
		{
			return this.InternalReadContentAsObjectAsync(false);
		}

		// Token: 0x0600122D RID: 4653 RVA: 0x0006B260 File Offset: 0x00069460
		private async Task<object> InternalReadContentAsObjectAsync(bool unwrapTypedValue)
		{
			return (await this.InternalReadContentAsObjectTupleAsync(unwrapTypedValue).ConfigureAwait(false)).Item2;
		}

		// Token: 0x0600122E RID: 4654 RVA: 0x0006B2B0 File Offset: 0x000694B0
		private async Task<Tuple<string, object>> InternalReadContentAsObjectTupleAsync(bool unwrapTypedValue)
		{
			XmlNodeType nodeType = this.NodeType;
			Tuple<string, object> result;
			if (nodeType == XmlNodeType.Attribute)
			{
				string text = this.Value;
				if (this.attributePSVI != null && this.attributePSVI.typedAttributeValue != null)
				{
					if (this.validationState == XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute)
					{
						XmlSchemaAttribute schemaAttribute = this.attributePSVI.attributeSchemaInfo.SchemaAttribute;
						text = ((schemaAttribute.DefaultValue != null) ? schemaAttribute.DefaultValue : schemaAttribute.FixedValue);
					}
					result = new Tuple<string, object>(text, this.ReturnBoxedValue(this.attributePSVI.typedAttributeValue, this.AttributeSchemaInfo.XmlType, unwrapTypedValue));
				}
				else
				{
					result = new Tuple<string, object>(text, this.Value);
				}
			}
			else if (nodeType == XmlNodeType.EndElement)
			{
				if (this.atomicValue != null)
				{
					string text = this.originalAtomicValueString;
					result = new Tuple<string, object>(text, this.atomicValue);
				}
				else
				{
					string text = string.Empty;
					result = new Tuple<string, object>(text, string.Empty);
				}
			}
			else if (this.validator.CurrentContentType == XmlSchemaContentType.TextOnly)
			{
				object typedValue = await this.ReadTillEndElementAsync().ConfigureAwait(false);
				object item = this.ReturnBoxedValue(typedValue, this.xmlSchemaInfo.XmlType, unwrapTypedValue);
				string text = this.originalAtomicValueString;
				result = new Tuple<string, object>(text, item);
			}
			else
			{
				XsdCachingReader xsdCachingReader = this.coreReader as XsdCachingReader;
				string text;
				if (xsdCachingReader != null)
				{
					text = xsdCachingReader.ReadOriginalContentAsString();
				}
				else
				{
					text = await base.InternalReadContentAsStringAsync().ConfigureAwait(false);
				}
				result = new Tuple<string, object>(text, text);
			}
			return result;
		}

		// Token: 0x0600122F RID: 4655 RVA: 0x0006B2FD File Offset: 0x000694FD
		private Task<Tuple<XmlSchemaType, object>> InternalReadElementContentAsObjectAsync()
		{
			return this.InternalReadElementContentAsObjectAsync(false);
		}

		// Token: 0x06001230 RID: 4656 RVA: 0x0006B308 File Offset: 0x00069508
		private async Task<Tuple<XmlSchemaType, object>> InternalReadElementContentAsObjectAsync(bool unwrapTypedValue)
		{
			Tuple<XmlSchemaType, string, object> tuple = await this.InternalReadElementContentAsObjectTupleAsync(unwrapTypedValue).ConfigureAwait(false);
			return new Tuple<XmlSchemaType, object>(tuple.Item1, tuple.Item3);
		}

		// Token: 0x06001231 RID: 4657 RVA: 0x0006B358 File Offset: 0x00069558
		private async Task<Tuple<XmlSchemaType, string, object>> InternalReadElementContentAsObjectTupleAsync(bool unwrapTypedValue)
		{
			object typedValue = null;
			XmlSchemaType xmlType = null;
			Tuple<XmlSchemaType, string, object> result;
			if (this.IsEmptyElement)
			{
				if (this.xmlSchemaInfo.ContentType == XmlSchemaContentType.TextOnly)
				{
					typedValue = this.ReturnBoxedValue(this.atomicValue, this.xmlSchemaInfo.XmlType, unwrapTypedValue);
				}
				else
				{
					typedValue = this.atomicValue;
				}
				string originalString = this.originalAtomicValueString;
				xmlType = this.ElementXmlType;
				await this.ReadAsync().ConfigureAwait(false);
				result = new Tuple<XmlSchemaType, string, object>(xmlType, originalString, typedValue);
			}
			else
			{
				await this.ReadAsync().ConfigureAwait(false);
				string originalString;
				if (this.NodeType == XmlNodeType.EndElement)
				{
					if (this.xmlSchemaInfo.IsDefault)
					{
						if (this.xmlSchemaInfo.ContentType == XmlSchemaContentType.TextOnly)
						{
							typedValue = this.ReturnBoxedValue(this.atomicValue, this.xmlSchemaInfo.XmlType, unwrapTypedValue);
						}
						else
						{
							typedValue = this.atomicValue;
						}
						originalString = this.originalAtomicValueString;
					}
					else
					{
						typedValue = string.Empty;
						originalString = string.Empty;
					}
				}
				else
				{
					if (this.NodeType == XmlNodeType.Element)
					{
						throw new XmlException("ReadElementContentAs() methods cannot be called on an element that has child elements.", string.Empty, this);
					}
					Tuple<string, object> tuple = await this.InternalReadContentAsObjectTupleAsync(unwrapTypedValue).ConfigureAwait(false);
					originalString = tuple.Item1;
					typedValue = tuple.Item2;
					if (this.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("ReadElementContentAs() methods cannot be called on an element that has child elements.", string.Empty, this);
					}
				}
				xmlType = this.ElementXmlType;
				await this.ReadAsync().ConfigureAwait(false);
				result = new Tuple<XmlSchemaType, string, object>(xmlType, originalString, typedValue);
			}
			return result;
		}

		// Token: 0x06001232 RID: 4658 RVA: 0x0006B3A8 File Offset: 0x000695A8
		private async Task<object> ReadTillEndElementAsync()
		{
			if (this.atomicValue == null)
			{
				for (;;)
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
					if (!configuredTaskAwaiter.GetResult())
					{
						goto Block_6;
					}
					if (!this.replayCache)
					{
						switch (this.coreReader.NodeType)
						{
						case XmlNodeType.Element:
							goto IL_8B;
						case XmlNodeType.Text:
						case XmlNodeType.CDATA:
							this.validator.ValidateText(new XmlValueGetter(this.GetStringValue));
							break;
						case XmlNodeType.Whitespace:
						case XmlNodeType.SignificantWhitespace:
							this.validator.ValidateWhitespace(new XmlValueGetter(this.GetStringValue));
							break;
						case XmlNodeType.EndElement:
							goto IL_12A;
						}
					}
				}
				IL_8B:
				await this.ProcessReaderEventAsync().ConfigureAwait(false);
				goto IL_1F0;
				IL_12A:
				this.atomicValue = this.validator.ValidateEndElement(this.xmlSchemaInfo);
				this.originalAtomicValueString = this.GetOriginalAtomicValueStringOfElement();
				if (this.manageNamespaces)
				{
					this.nsManager.PopScope();
				}
				Block_6:;
			}
			else
			{
				if (this.atomicValue == this)
				{
					this.atomicValue = null;
				}
				this.SwitchReader();
			}
			IL_1F0:
			return this.atomicValue;
		}

		// Token: 0x04000C77 RID: 3191
		private XmlReader coreReader;

		// Token: 0x04000C78 RID: 3192
		private IXmlNamespaceResolver coreReaderNSResolver;

		// Token: 0x04000C79 RID: 3193
		private IXmlNamespaceResolver thisNSResolver;

		// Token: 0x04000C7A RID: 3194
		private XmlSchemaValidator validator;

		// Token: 0x04000C7B RID: 3195
		private XmlResolver xmlResolver;

		// Token: 0x04000C7C RID: 3196
		private ValidationEventHandler validationEvent;

		// Token: 0x04000C7D RID: 3197
		private XsdValidatingReader.ValidatingReaderState validationState;

		// Token: 0x04000C7E RID: 3198
		private XmlValueGetter valueGetter;

		// Token: 0x04000C7F RID: 3199
		private XmlNamespaceManager nsManager;

		// Token: 0x04000C80 RID: 3200
		private bool manageNamespaces;

		// Token: 0x04000C81 RID: 3201
		private bool processInlineSchema;

		// Token: 0x04000C82 RID: 3202
		private bool replayCache;

		// Token: 0x04000C83 RID: 3203
		private ValidatingReaderNodeData cachedNode;

		// Token: 0x04000C84 RID: 3204
		private AttributePSVIInfo attributePSVI;

		// Token: 0x04000C85 RID: 3205
		private int attributeCount;

		// Token: 0x04000C86 RID: 3206
		private int coreReaderAttributeCount;

		// Token: 0x04000C87 RID: 3207
		private int currentAttrIndex;

		// Token: 0x04000C88 RID: 3208
		private AttributePSVIInfo[] attributePSVINodes;

		// Token: 0x04000C89 RID: 3209
		private ArrayList defaultAttributes;

		// Token: 0x04000C8A RID: 3210
		private Parser inlineSchemaParser;

		// Token: 0x04000C8B RID: 3211
		private object atomicValue;

		// Token: 0x04000C8C RID: 3212
		private XmlSchemaInfo xmlSchemaInfo;

		// Token: 0x04000C8D RID: 3213
		private string originalAtomicValueString;

		// Token: 0x04000C8E RID: 3214
		private XmlNameTable coreReaderNameTable;

		// Token: 0x04000C8F RID: 3215
		private XsdCachingReader cachingReader;

		// Token: 0x04000C90 RID: 3216
		private ValidatingReaderNodeData textNode;

		// Token: 0x04000C91 RID: 3217
		private string NsXmlNs;

		// Token: 0x04000C92 RID: 3218
		private string NsXs;

		// Token: 0x04000C93 RID: 3219
		private string NsXsi;

		// Token: 0x04000C94 RID: 3220
		private string XsiType;

		// Token: 0x04000C95 RID: 3221
		private string XsiNil;

		// Token: 0x04000C96 RID: 3222
		private string XsdSchema;

		// Token: 0x04000C97 RID: 3223
		private string XsiSchemaLocation;

		// Token: 0x04000C98 RID: 3224
		private string XsiNoNamespaceSchemaLocation;

		// Token: 0x04000C99 RID: 3225
		private XmlCharType xmlCharType = XmlCharType.Instance;

		// Token: 0x04000C9A RID: 3226
		private IXmlLineInfo lineInfo;

		// Token: 0x04000C9B RID: 3227
		private ReadContentAsBinaryHelper readBinaryHelper;

		// Token: 0x04000C9C RID: 3228
		private XsdValidatingReader.ValidatingReaderState savedState;

		// Token: 0x04000C9D RID: 3229
		private const int InitialAttributeCount = 8;

		// Token: 0x04000C9E RID: 3230
		private static volatile Type TypeOfString;

		// Token: 0x020001F4 RID: 500
		private enum ValidatingReaderState
		{
			// Token: 0x04000CA0 RID: 3232
			None,
			// Token: 0x04000CA1 RID: 3233
			Init,
			// Token: 0x04000CA2 RID: 3234
			Read,
			// Token: 0x04000CA3 RID: 3235
			OnDefaultAttribute = -1,
			// Token: 0x04000CA4 RID: 3236
			OnReadAttributeValue = -2,
			// Token: 0x04000CA5 RID: 3237
			OnAttribute = 3,
			// Token: 0x04000CA6 RID: 3238
			ClearAttributes,
			// Token: 0x04000CA7 RID: 3239
			ParseInlineSchema,
			// Token: 0x04000CA8 RID: 3240
			ReadAhead,
			// Token: 0x04000CA9 RID: 3241
			OnReadBinaryContent,
			// Token: 0x04000CAA RID: 3242
			ReaderClosed,
			// Token: 0x04000CAB RID: 3243
			EOF,
			// Token: 0x04000CAC RID: 3244
			Error
		}

		// Token: 0x020001F5 RID: 501
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsStringAsync>d__187 : IAsyncStateMachine
		{
			// Token: 0x06001233 RID: 4659 RVA: 0x0006B3F0 File Offset: 0x000695F0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				string result2;
				try
				{
					ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (!XmlReader.CanReadContentAs(xsdValidatingReader.NodeType))
						{
							throw xsdValidatingReader.CreateReadContentAsException("ReadContentAsString");
						}
						configuredTaskAwaiter = xsdValidatingReader.InternalReadContentAsObjectAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadContentAsStringAsync>d__187>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					object result = configuredTaskAwaiter.GetResult();
					XmlSchemaType xmlSchemaType = (xsdValidatingReader.NodeType == XmlNodeType.Attribute) ? xsdValidatingReader.AttributeXmlType : xsdValidatingReader.ElementXmlType;
					try
					{
						if (xmlSchemaType != null)
						{
							result2 = xmlSchemaType.ValueConverter.ToString(result);
						}
						else
						{
							result2 = (result as string);
						}
					}
					catch (InvalidCastException innerException)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException, xsdValidatingReader);
					}
					catch (FormatException innerException2)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException2, xsdValidatingReader);
					}
					catch (OverflowException innerException3)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException3, xsdValidatingReader);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06001234 RID: 4660 RVA: 0x0006B568 File Offset: 0x00069768
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CAD RID: 3245
			public int <>1__state;

			// Token: 0x04000CAE RID: 3246
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x04000CAF RID: 3247
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CB0 RID: 3248
			private ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001F6 RID: 502
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsAsync>d__188 : IAsyncStateMachine
		{
			// Token: 0x06001235 RID: 4661 RVA: 0x0006B578 File Offset: 0x00069778
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				object result2;
				try
				{
					ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (!XmlReader.CanReadContentAs(xsdValidatingReader.NodeType))
						{
							throw xsdValidatingReader.CreateReadContentAsException("ReadContentAs");
						}
						configuredTaskAwaiter = xsdValidatingReader.InternalReadContentAsObjectTupleAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadContentAsAsync>d__188>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Tuple<string, object> result = configuredTaskAwaiter.GetResult();
					string item = result.Item1;
					object value = result.Item2;
					XmlSchemaType xmlSchemaType = (xsdValidatingReader.NodeType == XmlNodeType.Attribute) ? xsdValidatingReader.AttributeXmlType : xsdValidatingReader.ElementXmlType;
					try
					{
						if (xmlSchemaType != null)
						{
							if (returnType == typeof(DateTimeOffset) && xmlSchemaType.Datatype is Datatype_dateTimeBase)
							{
								value = item;
							}
							result2 = xmlSchemaType.ValueConverter.ChangeType(value, returnType);
						}
						else
						{
							result2 = XmlUntypedConverter.Untyped.ChangeType(value, returnType, namespaceResolver);
						}
					}
					catch (FormatException innerException)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException, xsdValidatingReader);
					}
					catch (InvalidCastException innerException2)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException2, xsdValidatingReader);
					}
					catch (OverflowException innerException3)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException3, xsdValidatingReader);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06001236 RID: 4662 RVA: 0x0006B784 File Offset: 0x00069984
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CB1 RID: 3249
			public int <>1__state;

			// Token: 0x04000CB2 RID: 3250
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x04000CB3 RID: 3251
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CB4 RID: 3252
			public Type returnType;

			// Token: 0x04000CB5 RID: 3253
			public IXmlNamespaceResolver namespaceResolver;

			// Token: 0x04000CB6 RID: 3254
			private ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001F7 RID: 503
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsObjectAsync>d__189 : IAsyncStateMachine
		{
			// Token: 0x06001237 RID: 4663 RVA: 0x0006B794 File Offset: 0x00069994
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				object item;
				try
				{
					ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xsdValidatingReader.NodeType != XmlNodeType.Element)
						{
							throw xsdValidatingReader.CreateReadElementContentAsException("ReadElementContentAsObject");
						}
						configuredTaskAwaiter = xsdValidatingReader.InternalReadElementContentAsObjectAsync(true).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadElementContentAsObjectAsync>d__189>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					item = configuredTaskAwaiter.GetResult().Item2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(item);
			}

			// Token: 0x06001238 RID: 4664 RVA: 0x0006B870 File Offset: 0x00069A70
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CB7 RID: 3255
			public int <>1__state;

			// Token: 0x04000CB8 RID: 3256
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x04000CB9 RID: 3257
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CBA RID: 3258
			private ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001F8 RID: 504
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsStringAsync>d__190 : IAsyncStateMachine
		{
			// Token: 0x06001239 RID: 4665 RVA: 0x0006B880 File Offset: 0x00069A80
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				string result2;
				try
				{
					ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xsdValidatingReader.NodeType != XmlNodeType.Element)
						{
							throw xsdValidatingReader.CreateReadElementContentAsException("ReadElementContentAsString");
						}
						configuredTaskAwaiter = xsdValidatingReader.InternalReadElementContentAsObjectAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadElementContentAsStringAsync>d__190>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Tuple<XmlSchemaType, object> result = configuredTaskAwaiter.GetResult();
					XmlSchemaType item = result.Item1;
					object item2 = result.Item2;
					try
					{
						if (item != null)
						{
							result2 = item.ValueConverter.ToString(item2);
						}
						else
						{
							result2 = (item2 as string);
						}
					}
					catch (InvalidCastException innerException)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException, xsdValidatingReader);
					}
					catch (FormatException innerException2)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException2, xsdValidatingReader);
					}
					catch (OverflowException innerException3)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", "String", innerException3, xsdValidatingReader);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x0600123A RID: 4666 RVA: 0x0006B9E8 File Offset: 0x00069BE8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CBB RID: 3259
			public int <>1__state;

			// Token: 0x04000CBC RID: 3260
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x04000CBD RID: 3261
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CBE RID: 3262
			private ConfiguredTaskAwaitable<Tuple<XmlSchemaType, object>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001F9 RID: 505
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsAsync>d__191 : IAsyncStateMachine
		{
			// Token: 0x0600123B RID: 4667 RVA: 0x0006B9F8 File Offset: 0x00069BF8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				object result2;
				try
				{
					ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xsdValidatingReader.NodeType != XmlNodeType.Element)
						{
							throw xsdValidatingReader.CreateReadElementContentAsException("ReadElementContentAs");
						}
						configuredTaskAwaiter = xsdValidatingReader.InternalReadElementContentAsObjectTupleAsync(false).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadElementContentAsAsync>d__191>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Tuple<XmlSchemaType, string, object> result = configuredTaskAwaiter.GetResult();
					XmlSchemaType item = result.Item1;
					string item2 = result.Item2;
					object value = result.Item3;
					try
					{
						if (item != null)
						{
							if (returnType == typeof(DateTimeOffset) && item.Datatype is Datatype_dateTimeBase)
							{
								value = item2;
							}
							result2 = item.ValueConverter.ChangeType(value, returnType, namespaceResolver);
						}
						else
						{
							result2 = XmlUntypedConverter.Untyped.ChangeType(value, returnType, namespaceResolver);
						}
					}
					catch (FormatException innerException)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException, xsdValidatingReader);
					}
					catch (InvalidCastException innerException2)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException2, xsdValidatingReader);
					}
					catch (OverflowException innerException3)
					{
						throw new XmlException("Content cannot be converted to the type {0}.", returnType.ToString(), innerException3, xsdValidatingReader);
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x0600123C RID: 4668 RVA: 0x0006BBF4 File Offset: 0x00069DF4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CBF RID: 3263
			public int <>1__state;

			// Token: 0x04000CC0 RID: 3264
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x04000CC1 RID: 3265
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CC2 RID: 3266
			public Type returnType;

			// Token: 0x04000CC3 RID: 3267
			public IXmlNamespaceResolver namespaceResolver;

			// Token: 0x04000CC4 RID: 3268
			private ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001FA RID: 506
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ReadAsync_Read>d__193 : IAsyncStateMachine
		{
			// Token: 0x0600123D RID: 4669 RVA: 0x0006BC04 File Offset: 0x00069E04
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_DD;
						}
						configuredTaskAwaiter5 = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter5.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter5;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdValidatingReader.<_ReadAsync_Read>d__193>(ref configuredTaskAwaiter5, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (!configuredTaskAwaiter5.GetResult())
					{
						xsdValidatingReader.validator.EndValidation();
						if (xsdValidatingReader.coreReader.EOF)
						{
							xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.EOF;
						}
						result = false;
						goto IL_125;
					}
					configuredTaskAwaiter3 = xsdValidatingReader.ProcessReaderEventAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XsdValidatingReader.<_ReadAsync_Read>d__193>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_DD:
					configuredTaskAwaiter3.GetResult();
					result = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_125:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600123E RID: 4670 RVA: 0x0006BD5C File Offset: 0x00069F5C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CC5 RID: 3269
			public int <>1__state;

			// Token: 0x04000CC6 RID: 3270
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000CC7 RID: 3271
			public Task<bool> task;

			// Token: 0x04000CC8 RID: 3272
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CC9 RID: 3273
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000CCA RID: 3274
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020001FB RID: 507
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ReadAsync_ReadAhead>d__195 : IAsyncStateMachine
		{
			// Token: 0x0600123F RID: 4671 RVA: 0x0006BD6C File Offset: 0x00069F6C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XsdValidatingReader.<_ReadAsync_ReadAhead>d__195>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.Read;
					result = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001240 RID: 4672 RVA: 0x0006BE34 File Offset: 0x0006A034
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CCB RID: 3275
			public int <>1__state;

			// Token: 0x04000CCC RID: 3276
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000CCD RID: 3277
			public Task task;

			// Token: 0x04000CCE RID: 3278
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CCF RID: 3279
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001FC RID: 508
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SkipAsync>d__197 : IAsyncStateMachine
		{
			// Token: 0x06001241 RID: 4673 RVA: 0x0006BE44 File Offset: 0x0006A044
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_174;
						}
						int depth = xsdValidatingReader.Depth;
						XmlNodeType nodeType = xsdValidatingReader.NodeType;
						if (nodeType != XmlNodeType.Element)
						{
							if (nodeType != XmlNodeType.Attribute)
							{
								goto IL_116;
							}
							xsdValidatingReader.MoveToElement();
						}
						if (xsdValidatingReader.coreReader.IsEmptyElement)
						{
							goto IL_116;
						}
						callSkipToEndElem = true;
						if ((xsdValidatingReader.xmlSchemaInfo.IsUnionType || xsdValidatingReader.xmlSchemaInfo.IsDefault) && xsdValidatingReader.coreReader is XsdCachingReader)
						{
							callSkipToEndElem = false;
						}
						configuredTaskAwaiter3 = xsdValidatingReader.coreReader.SkipAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XsdValidatingReader.<SkipAsync>d__197>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter3.GetResult();
					xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.ReadAhead;
					if (callSkipToEndElem)
					{
						xsdValidatingReader.validator.SkipToEndElement(xsdValidatingReader.xmlSchemaInfo);
					}
					IL_116:
					configuredTaskAwaiter = xsdValidatingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdValidatingReader.<SkipAsync>d__197>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_174:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001242 RID: 4674 RVA: 0x0006C018 File Offset: 0x0006A218
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CD0 RID: 3280
			public int <>1__state;

			// Token: 0x04000CD1 RID: 3281
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000CD2 RID: 3282
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CD3 RID: 3283
			private bool <callSkipToEndElem>5__1;

			// Token: 0x04000CD4 RID: 3284
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000CD5 RID: 3285
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020001FD RID: 509
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBase64Async>d__198 : IAsyncStateMachine
		{
			// Token: 0x06001243 RID: 4675 RVA: 0x0006C028 File Offset: 0x0006A228
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xsdValidatingReader.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_FF;
						}
						if (xsdValidatingReader.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
						{
							xsdValidatingReader.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xsdValidatingReader.readBinaryHelper, xsdValidatingReader);
							xsdValidatingReader.savedState = xsdValidatingReader.validationState;
						}
						xsdValidatingReader.validationState = xsdValidatingReader.savedState;
						configuredTaskAwaiter = xsdValidatingReader.readBinaryHelper.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadContentAsBase64Async>d__198>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					xsdValidatingReader.savedState = xsdValidatingReader.validationState;
					xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_FF:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001244 RID: 4676 RVA: 0x0006C158 File Offset: 0x0006A358
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CD6 RID: 3286
			public int <>1__state;

			// Token: 0x04000CD7 RID: 3287
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000CD8 RID: 3288
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CD9 RID: 3289
			public byte[] buffer;

			// Token: 0x04000CDA RID: 3290
			public int index;

			// Token: 0x04000CDB RID: 3291
			public int count;

			// Token: 0x04000CDC RID: 3292
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001FE RID: 510
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBinHexAsync>d__199 : IAsyncStateMachine
		{
			// Token: 0x06001245 RID: 4677 RVA: 0x0006C168 File Offset: 0x0006A368
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xsdValidatingReader.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_FF;
						}
						if (xsdValidatingReader.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
						{
							xsdValidatingReader.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xsdValidatingReader.readBinaryHelper, xsdValidatingReader);
							xsdValidatingReader.savedState = xsdValidatingReader.validationState;
						}
						xsdValidatingReader.validationState = xsdValidatingReader.savedState;
						configuredTaskAwaiter = xsdValidatingReader.readBinaryHelper.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadContentAsBinHexAsync>d__199>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					xsdValidatingReader.savedState = xsdValidatingReader.validationState;
					xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_FF:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001246 RID: 4678 RVA: 0x0006C298 File Offset: 0x0006A498
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CDD RID: 3293
			public int <>1__state;

			// Token: 0x04000CDE RID: 3294
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000CDF RID: 3295
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CE0 RID: 3296
			public byte[] buffer;

			// Token: 0x04000CE1 RID: 3297
			public int index;

			// Token: 0x04000CE2 RID: 3298
			public int count;

			// Token: 0x04000CE3 RID: 3299
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020001FF RID: 511
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBase64Async>d__200 : IAsyncStateMachine
		{
			// Token: 0x06001247 RID: 4679 RVA: 0x0006C2A8 File Offset: 0x0006A4A8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xsdValidatingReader.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_FF;
						}
						if (xsdValidatingReader.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
						{
							xsdValidatingReader.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xsdValidatingReader.readBinaryHelper, xsdValidatingReader);
							xsdValidatingReader.savedState = xsdValidatingReader.validationState;
						}
						xsdValidatingReader.validationState = xsdValidatingReader.savedState;
						configuredTaskAwaiter = xsdValidatingReader.readBinaryHelper.ReadElementContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadElementContentAsBase64Async>d__200>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					xsdValidatingReader.savedState = xsdValidatingReader.validationState;
					xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_FF:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001248 RID: 4680 RVA: 0x0006C3D8 File Offset: 0x0006A5D8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CE4 RID: 3300
			public int <>1__state;

			// Token: 0x04000CE5 RID: 3301
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000CE6 RID: 3302
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CE7 RID: 3303
			public byte[] buffer;

			// Token: 0x04000CE8 RID: 3304
			public int index;

			// Token: 0x04000CE9 RID: 3305
			public int count;

			// Token: 0x04000CEA RID: 3306
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000200 RID: 512
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBinHexAsync>d__201 : IAsyncStateMachine
		{
			// Token: 0x06001249 RID: 4681 RVA: 0x0006C3E8 File Offset: 0x0006A5E8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (xsdValidatingReader.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_FF;
						}
						if (xsdValidatingReader.validationState != XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent)
						{
							xsdValidatingReader.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xsdValidatingReader.readBinaryHelper, xsdValidatingReader);
							xsdValidatingReader.savedState = xsdValidatingReader.validationState;
						}
						xsdValidatingReader.validationState = xsdValidatingReader.savedState;
						configuredTaskAwaiter = xsdValidatingReader.readBinaryHelper.ReadElementContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadElementContentAsBinHexAsync>d__201>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					int result2 = configuredTaskAwaiter.GetResult();
					xsdValidatingReader.savedState = xsdValidatingReader.validationState;
					xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.OnReadBinaryContent;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_FF:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600124A RID: 4682 RVA: 0x0006C518 File Offset: 0x0006A718
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CEB RID: 3307
			public int <>1__state;

			// Token: 0x04000CEC RID: 3308
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000CED RID: 3309
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CEE RID: 3310
			public byte[] buffer;

			// Token: 0x04000CEF RID: 3311
			public int index;

			// Token: 0x04000CF0 RID: 3312
			public int count;

			// Token: 0x04000CF1 RID: 3313
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000201 RID: 513
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ProcessElementEventAsync>d__203 : IAsyncStateMachine
		{
			// Token: 0x0600124B RID: 4683 RVA: 0x0006C528 File Offset: 0x0006A728
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num != 1)
						{
							if (xsdValidatingReader.processInlineSchema && xsdValidatingReader.IsXSDRoot(xsdValidatingReader.coreReader.LocalName, xsdValidatingReader.coreReader.NamespaceURI) && xsdValidatingReader.coreReader.Depth > 0)
							{
								xsdValidatingReader.xmlSchemaInfo.Clear();
								xsdValidatingReader.attributeCount = (xsdValidatingReader.coreReaderAttributeCount = xsdValidatingReader.coreReader.AttributeCount);
								if (xsdValidatingReader.coreReader.IsEmptyElement)
								{
									xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.ClearAttributes;
									goto IL_365;
								}
								xsdValidatingReader.inlineSchemaParser = new Parser(SchemaType.XSD, xsdValidatingReader.coreReaderNameTable, xsdValidatingReader.validator.SchemaSet.GetSchemaNames(xsdValidatingReader.coreReaderNameTable), xsdValidatingReader.validationEvent);
								configuredTaskAwaiter = xsdValidatingReader.inlineSchemaParser.StartParsingAsync(xsdValidatingReader.coreReader, null).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XsdValidatingReader.<ProcessElementEventAsync>d__203>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_125;
							}
							else
							{
								xsdValidatingReader.atomicValue = null;
								xsdValidatingReader.originalAtomicValueString = null;
								xsdValidatingReader.xmlSchemaInfo.Clear();
								if (xsdValidatingReader.manageNamespaces)
								{
									xsdValidatingReader.nsManager.PushScope();
								}
								string xsiSchemaLocation = null;
								string xsiNoNamespaceSchemaLocation = null;
								string xsiNil = null;
								string xsiType = null;
								if (xsdValidatingReader.coreReader.MoveToFirstAttribute())
								{
									do
									{
										string namespaceURI = xsdValidatingReader.coreReader.NamespaceURI;
										string localName = xsdValidatingReader.coreReader.LocalName;
										if (Ref.Equal(namespaceURI, xsdValidatingReader.NsXsi))
										{
											if (Ref.Equal(localName, xsdValidatingReader.XsiSchemaLocation))
											{
												xsiSchemaLocation = xsdValidatingReader.coreReader.Value;
											}
											else if (Ref.Equal(localName, xsdValidatingReader.XsiNoNamespaceSchemaLocation))
											{
												xsiNoNamespaceSchemaLocation = xsdValidatingReader.coreReader.Value;
											}
											else if (Ref.Equal(localName, xsdValidatingReader.XsiType))
											{
												xsiType = xsdValidatingReader.coreReader.Value;
											}
											else if (Ref.Equal(localName, xsdValidatingReader.XsiNil))
											{
												xsiNil = xsdValidatingReader.coreReader.Value;
											}
										}
										if (xsdValidatingReader.manageNamespaces && Ref.Equal(xsdValidatingReader.coreReader.NamespaceURI, xsdValidatingReader.NsXmlNs))
										{
											xsdValidatingReader.nsManager.AddNamespace((xsdValidatingReader.coreReader.Prefix.Length == 0) ? string.Empty : xsdValidatingReader.coreReader.LocalName, xsdValidatingReader.coreReader.Value);
										}
									}
									while (xsdValidatingReader.coreReader.MoveToNextAttribute());
									xsdValidatingReader.coreReader.MoveToElement();
								}
								xsdValidatingReader.validator.ValidateElement(xsdValidatingReader.coreReader.LocalName, xsdValidatingReader.coreReader.NamespaceURI, xsdValidatingReader.xmlSchemaInfo, xsiType, xsiNil, xsiSchemaLocation, xsiNoNamespaceSchemaLocation);
								xsdValidatingReader.ValidateAttributes();
								xsdValidatingReader.validator.ValidateEndOfAttributes(xsdValidatingReader.xmlSchemaInfo);
								if (!xsdValidatingReader.coreReader.IsEmptyElement)
								{
									goto IL_35E;
								}
								configuredTaskAwaiter = xsdValidatingReader.ProcessEndElementEventAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 1;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XsdValidatingReader.<ProcessElementEventAsync>d__203>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
						}
						else
						{
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						configuredTaskAwaiter.GetResult();
						IL_35E:
						xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.ClearAttributes;
						goto IL_365;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_125:
					configuredTaskAwaiter.GetResult();
					xsdValidatingReader.inlineSchemaParser.ParseReaderNode();
					xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.ParseInlineSchema;
					IL_365:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600124C RID: 4684 RVA: 0x0006C8E4 File Offset: 0x0006AAE4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CF2 RID: 3314
			public int <>1__state;

			// Token: 0x04000CF3 RID: 3315
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000CF4 RID: 3316
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CF5 RID: 3317
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000202 RID: 514
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ProcessEndElementEventAsync>d__204 : IAsyncStateMachine
		{
			// Token: 0x0600124D RID: 4685 RVA: 0x0006C8F4 File Offset: 0x0006AAF4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xsdValidatingReader.atomicValue = xsdValidatingReader.validator.ValidateEndElement(xsdValidatingReader.xmlSchemaInfo);
						xsdValidatingReader.originalAtomicValueString = xsdValidatingReader.GetOriginalAtomicValueStringOfElement();
						if (xsdValidatingReader.xmlSchemaInfo.IsDefault)
						{
							int depth = xsdValidatingReader.coreReader.Depth;
							xsdValidatingReader.coreReader = xsdValidatingReader.GetCachingReader();
							xsdValidatingReader.cachingReader.RecordTextNode(xsdValidatingReader.xmlSchemaInfo.XmlType.ValueConverter.ToString(xsdValidatingReader.atomicValue), xsdValidatingReader.originalAtomicValueString, depth + 1, 0, 0);
							xsdValidatingReader.cachingReader.RecordEndElementNode();
							configuredTaskAwaiter = xsdValidatingReader.cachingReader.SetToReplayModeAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XsdValidatingReader.<ProcessEndElementEventAsync>d__204>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							if (xsdValidatingReader.manageNamespaces)
							{
								xsdValidatingReader.nsManager.PopScope();
								goto IL_120;
							}
							goto IL_120;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xsdValidatingReader.replayCache = true;
					IL_120:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600124E RID: 4686 RVA: 0x0006CA6C File Offset: 0x0006AC6C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CF6 RID: 3318
			public int <>1__state;

			// Token: 0x04000CF7 RID: 3319
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000CF8 RID: 3320
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CF9 RID: 3321
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000203 RID: 515
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ProcessInlineSchemaAsync>d__205 : IAsyncStateMachine
		{
			// Token: 0x0600124F RID: 4687 RVA: 0x0006CA7C File Offset: 0x0006AC7C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						configuredTaskAwaiter3 = xsdValidatingReader.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdValidatingReader.<ProcessInlineSchemaAsync>d__205>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter3.GetResult())
					{
						if (xsdValidatingReader.coreReader.NodeType == XmlNodeType.Element)
						{
							xsdValidatingReader.attributeCount = (xsdValidatingReader.coreReaderAttributeCount = xsdValidatingReader.coreReader.AttributeCount);
						}
						else
						{
							xsdValidatingReader.ClearAttributesInfo();
						}
						if (!xsdValidatingReader.inlineSchemaParser.ParseReaderNode())
						{
							xsdValidatingReader.inlineSchemaParser.FinishParsing();
							XmlSchema xmlSchema = xsdValidatingReader.inlineSchemaParser.XmlSchema;
							xsdValidatingReader.validator.AddSchema(xmlSchema);
							xsdValidatingReader.inlineSchemaParser = null;
							xsdValidatingReader.validationState = XsdValidatingReader.ValidatingReaderState.Read;
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06001250 RID: 4688 RVA: 0x0006CBB8 File Offset: 0x0006ADB8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CFA RID: 3322
			public int <>1__state;

			// Token: 0x04000CFB RID: 3323
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000CFC RID: 3324
			public XsdValidatingReader <>4__this;

			// Token: 0x04000CFD RID: 3325
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000204 RID: 516
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InternalReadContentAsObjectAsync>d__207 : IAsyncStateMachine
		{
			// Token: 0x06001251 RID: 4689 RVA: 0x0006CBC8 File Offset: 0x0006ADC8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				object item;
				try
				{
					ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = xsdValidatingReader.InternalReadContentAsObjectTupleAsync(unwrapTypedValue).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter, XsdValidatingReader.<InternalReadContentAsObjectAsync>d__207>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					item = configuredTaskAwaiter.GetResult().Item2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(item);
			}

			// Token: 0x06001252 RID: 4690 RVA: 0x0006CC94 File Offset: 0x0006AE94
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000CFE RID: 3326
			public int <>1__state;

			// Token: 0x04000CFF RID: 3327
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x04000D00 RID: 3328
			public XsdValidatingReader <>4__this;

			// Token: 0x04000D01 RID: 3329
			public bool unwrapTypedValue;

			// Token: 0x04000D02 RID: 3330
			private ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000205 RID: 517
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InternalReadContentAsObjectTupleAsync>d__208 : IAsyncStateMachine
		{
			// Token: 0x06001253 RID: 4691 RVA: 0x0006CCA4 File Offset: 0x0006AEA4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				Tuple<string, object> result;
				try
				{
					string text;
					ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num != 1)
						{
							XmlNodeType nodeType = xsdValidatingReader.NodeType;
							if (nodeType == XmlNodeType.Attribute)
							{
								text = xsdValidatingReader.Value;
								if (xsdValidatingReader.attributePSVI != null && xsdValidatingReader.attributePSVI.typedAttributeValue != null)
								{
									if (xsdValidatingReader.validationState == XsdValidatingReader.ValidatingReaderState.OnDefaultAttribute)
									{
										XmlSchemaAttribute schemaAttribute = xsdValidatingReader.attributePSVI.attributeSchemaInfo.SchemaAttribute;
										text = ((schemaAttribute.DefaultValue != null) ? schemaAttribute.DefaultValue : schemaAttribute.FixedValue);
									}
									result = new Tuple<string, object>(text, xsdValidatingReader.ReturnBoxedValue(xsdValidatingReader.attributePSVI.typedAttributeValue, xsdValidatingReader.AttributeSchemaInfo.XmlType, unwrapTypedValue));
									goto IL_248;
								}
								result = new Tuple<string, object>(text, xsdValidatingReader.Value);
								goto IL_248;
							}
							else if (nodeType == XmlNodeType.EndElement)
							{
								if (xsdValidatingReader.atomicValue != null)
								{
									text = xsdValidatingReader.originalAtomicValueString;
									result = new Tuple<string, object>(text, xsdValidatingReader.atomicValue);
									goto IL_248;
								}
								text = string.Empty;
								result = new Tuple<string, object>(text, string.Empty);
								goto IL_248;
							}
							else if (xsdValidatingReader.validator.CurrentContentType == XmlSchemaContentType.TextOnly)
							{
								configuredTaskAwaiter = xsdValidatingReader.ReadTillEndElementAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter, XsdValidatingReader.<InternalReadContentAsObjectTupleAsync>d__208>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_16B;
							}
							else
							{
								XsdCachingReader xsdCachingReader = xsdValidatingReader.coreReader as XsdCachingReader;
								if (xsdCachingReader != null)
								{
									text = xsdCachingReader.ReadOriginalContentAsString();
									goto IL_225;
								}
								configuredTaskAwaiter3 = xsdValidatingReader.InternalReadContentAsStringAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 1;
									ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XsdValidatingReader.<InternalReadContentAsObjectTupleAsync>d__208>(ref configuredTaskAwaiter3, ref this);
									return;
								}
							}
						}
						else
						{
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
							num2 = -1;
						}
						text = configuredTaskAwaiter3.GetResult();
						IL_225:
						result = new Tuple<string, object>(text, text);
						goto IL_248;
					}
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter);
					num2 = -1;
					IL_16B:
					object result2 = configuredTaskAwaiter.GetResult();
					object item = xsdValidatingReader.ReturnBoxedValue(result2, xsdValidatingReader.xmlSchemaInfo.XmlType, unwrapTypedValue);
					text = xsdValidatingReader.originalAtomicValueString;
					result = new Tuple<string, object>(text, item);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_248:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001254 RID: 4692 RVA: 0x0006CF2C File Offset: 0x0006B12C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000D03 RID: 3331
			public int <>1__state;

			// Token: 0x04000D04 RID: 3332
			public AsyncTaskMethodBuilder<Tuple<string, object>> <>t__builder;

			// Token: 0x04000D05 RID: 3333
			public XsdValidatingReader <>4__this;

			// Token: 0x04000D06 RID: 3334
			public bool unwrapTypedValue;

			// Token: 0x04000D07 RID: 3335
			private ConfiguredTaskAwaitable<object>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000D08 RID: 3336
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000206 RID: 518
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InternalReadElementContentAsObjectAsync>d__210 : IAsyncStateMachine
		{
			// Token: 0x06001255 RID: 4693 RVA: 0x0006CF3C File Offset: 0x0006B13C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				Tuple<XmlSchemaType, object> result2;
				try
				{
					ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = xsdValidatingReader.InternalReadElementContentAsObjectTupleAsync(unwrapTypedValue).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter, XsdValidatingReader.<InternalReadElementContentAsObjectAsync>d__210>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					Tuple<XmlSchemaType, string, object> result = configuredTaskAwaiter.GetResult();
					result2 = new Tuple<XmlSchemaType, object>(result.Item1, result.Item3);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result2);
			}

			// Token: 0x06001256 RID: 4694 RVA: 0x0006D018 File Offset: 0x0006B218
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000D09 RID: 3337
			public int <>1__state;

			// Token: 0x04000D0A RID: 3338
			public AsyncTaskMethodBuilder<Tuple<XmlSchemaType, object>> <>t__builder;

			// Token: 0x04000D0B RID: 3339
			public XsdValidatingReader <>4__this;

			// Token: 0x04000D0C RID: 3340
			public bool unwrapTypedValue;

			// Token: 0x04000D0D RID: 3341
			private ConfiguredTaskAwaitable<Tuple<XmlSchemaType, string, object>>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000207 RID: 519
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <InternalReadElementContentAsObjectTupleAsync>d__211 : IAsyncStateMachine
		{
			// Token: 0x06001257 RID: 4695 RVA: 0x0006D028 File Offset: 0x0006B228
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				Tuple<XmlSchemaType, string, object> result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_174;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_27E;
					}
					case 3:
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_323;
					}
					default:
						typedValue = null;
						xmlType = null;
						if (xsdValidatingReader.IsEmptyElement)
						{
							if (xsdValidatingReader.xmlSchemaInfo.ContentType == XmlSchemaContentType.TextOnly)
							{
								typedValue = xsdValidatingReader.ReturnBoxedValue(xsdValidatingReader.atomicValue, xsdValidatingReader.xmlSchemaInfo.XmlType, unwrapTypedValue);
							}
							else
							{
								typedValue = xsdValidatingReader.atomicValue;
							}
							originalString = xsdValidatingReader.originalAtomicValueString;
							xmlType = xsdValidatingReader.ElementXmlType;
							configuredTaskAwaiter = xsdValidatingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdValidatingReader.<InternalReadElementContentAsObjectTupleAsync>d__211>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xsdValidatingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdValidatingReader.<InternalReadElementContentAsObjectTupleAsync>d__211>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_174;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					result = new Tuple<XmlSchemaType, string, object>(xmlType, originalString, typedValue);
					goto IL_35E;
					IL_174:
					configuredTaskAwaiter.GetResult();
					if (xsdValidatingReader.NodeType == XmlNodeType.EndElement)
					{
						if (xsdValidatingReader.xmlSchemaInfo.IsDefault)
						{
							if (xsdValidatingReader.xmlSchemaInfo.ContentType == XmlSchemaContentType.TextOnly)
							{
								typedValue = xsdValidatingReader.ReturnBoxedValue(xsdValidatingReader.atomicValue, xsdValidatingReader.xmlSchemaInfo.XmlType, unwrapTypedValue);
							}
							else
							{
								typedValue = xsdValidatingReader.atomicValue;
							}
							originalString = xsdValidatingReader.originalAtomicValueString;
							goto IL_2BC;
						}
						typedValue = string.Empty;
						originalString = string.Empty;
						goto IL_2BC;
					}
					else
					{
						if (xsdValidatingReader.NodeType == XmlNodeType.Element)
						{
							throw new XmlException("ReadElementContentAs() methods cannot be called on an element that has child elements.", string.Empty, xsdValidatingReader);
						}
						configuredTaskAwaiter3 = xsdValidatingReader.InternalReadContentAsObjectTupleAsync(unwrapTypedValue).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter, XsdValidatingReader.<InternalReadElementContentAsObjectTupleAsync>d__211>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					IL_27E:
					Tuple<string, object> result2 = configuredTaskAwaiter3.GetResult();
					originalString = result2.Item1;
					typedValue = result2.Item2;
					if (xsdValidatingReader.NodeType != XmlNodeType.EndElement)
					{
						throw new XmlException("ReadElementContentAs() methods cannot be called on an element that has child elements.", string.Empty, xsdValidatingReader);
					}
					IL_2BC:
					xmlType = xsdValidatingReader.ElementXmlType;
					configuredTaskAwaiter = xsdValidatingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdValidatingReader.<InternalReadElementContentAsObjectTupleAsync>d__211>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_323:
					configuredTaskAwaiter.GetResult();
					result = new Tuple<XmlSchemaType, string, object>(xmlType, originalString, typedValue);
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_35E:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06001258 RID: 4696 RVA: 0x0006D3C4 File Offset: 0x0006B5C4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000D0E RID: 3342
			public int <>1__state;

			// Token: 0x04000D0F RID: 3343
			public AsyncTaskMethodBuilder<Tuple<XmlSchemaType, string, object>> <>t__builder;

			// Token: 0x04000D10 RID: 3344
			public XsdValidatingReader <>4__this;

			// Token: 0x04000D11 RID: 3345
			public bool unwrapTypedValue;

			// Token: 0x04000D12 RID: 3346
			private XmlSchemaType <xmlType>5__1;

			// Token: 0x04000D13 RID: 3347
			private string <originalString>5__2;

			// Token: 0x04000D14 RID: 3348
			private object <typedValue>5__3;

			// Token: 0x04000D15 RID: 3349
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000D16 RID: 3350
			private ConfiguredTaskAwaitable<Tuple<string, object>>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000208 RID: 520
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadTillEndElementAsync>d__212 : IAsyncStateMachine
		{
			// Token: 0x06001259 RID: 4697 RVA: 0x0006D3D4 File Offset: 0x0006B5D4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdValidatingReader xsdValidatingReader = this;
				object atomicValue;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					if (num != 0)
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						if (num == 1)
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_1CC;
						}
						if (xsdValidatingReader.atomicValue != null)
						{
							if (xsdValidatingReader.atomicValue == xsdValidatingReader)
							{
								xsdValidatingReader.atomicValue = null;
							}
							xsdValidatingReader.SwitchReader();
							goto IL_1F0;
						}
						IL_169:
						configuredTaskAwaiter3 = xsdValidatingReader.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadTillEndElementAsync>d__212>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						IL_1CC:
						if (!configuredTaskAwaiter3.GetResult())
						{
							goto IL_1F0;
						}
						if (xsdValidatingReader.replayCache)
						{
							goto IL_169;
						}
						switch (xsdValidatingReader.coreReader.NodeType)
						{
						case XmlNodeType.Element:
							configuredTaskAwaiter4 = xsdValidatingReader.ProcessReaderEventAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter4.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XsdValidatingReader.<ReadTillEndElementAsync>d__212>(ref configuredTaskAwaiter4, ref this);
								return;
							}
							break;
						case XmlNodeType.Attribute:
						case XmlNodeType.EntityReference:
						case XmlNodeType.Entity:
						case XmlNodeType.ProcessingInstruction:
						case XmlNodeType.Comment:
						case XmlNodeType.Document:
						case XmlNodeType.DocumentType:
						case XmlNodeType.DocumentFragment:
						case XmlNodeType.Notation:
							goto IL_169;
						case XmlNodeType.Text:
						case XmlNodeType.CDATA:
							xsdValidatingReader.validator.ValidateText(new XmlValueGetter(xsdValidatingReader.GetStringValue));
							goto IL_169;
						case XmlNodeType.Whitespace:
						case XmlNodeType.SignificantWhitespace:
							xsdValidatingReader.validator.ValidateWhitespace(new XmlValueGetter(xsdValidatingReader.GetStringValue));
							goto IL_169;
						case XmlNodeType.EndElement:
							xsdValidatingReader.atomicValue = xsdValidatingReader.validator.ValidateEndElement(xsdValidatingReader.xmlSchemaInfo);
							xsdValidatingReader.originalAtomicValueString = xsdValidatingReader.GetOriginalAtomicValueStringOfElement();
							if (xsdValidatingReader.manageNamespaces)
							{
								xsdValidatingReader.nsManager.PopScope();
								goto IL_1F0;
							}
							goto IL_1F0;
						default:
							goto IL_169;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter4.GetResult();
					IL_1F0:
					atomicValue = xsdValidatingReader.atomicValue;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(atomicValue);
			}

			// Token: 0x0600125A RID: 4698 RVA: 0x0006D624 File Offset: 0x0006B824
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000D17 RID: 3351
			public int <>1__state;

			// Token: 0x04000D18 RID: 3352
			public AsyncTaskMethodBuilder<object> <>t__builder;

			// Token: 0x04000D19 RID: 3353
			public XsdValidatingReader <>4__this;

			// Token: 0x04000D1A RID: 3354
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000D1B RID: 3355
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}
	}
}
