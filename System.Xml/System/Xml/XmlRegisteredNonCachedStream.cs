﻿using System;
using System.IO;

namespace System.Xml
{
	// Token: 0x02000294 RID: 660
	internal class XmlRegisteredNonCachedStream : Stream
	{
		// Token: 0x0600186C RID: 6252 RVA: 0x0008E266 File Offset: 0x0008C466
		internal XmlRegisteredNonCachedStream(Stream stream, XmlDownloadManager downloadManager, string host)
		{
			this.stream = stream;
			this.downloadManager = downloadManager;
			this.host = host;
		}

		// Token: 0x0600186D RID: 6253 RVA: 0x0008E284 File Offset: 0x0008C484
		~XmlRegisteredNonCachedStream()
		{
			if (this.downloadManager != null)
			{
				this.downloadManager.Remove(this.host);
			}
			this.stream = null;
		}

		// Token: 0x0600186E RID: 6254 RVA: 0x0008E2CC File Offset: 0x0008C4CC
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this.stream != null)
				{
					if (this.downloadManager != null)
					{
						this.downloadManager.Remove(this.host);
					}
					this.stream.Close();
				}
				this.stream = null;
				GC.SuppressFinalize(this);
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x0600186F RID: 6255 RVA: 0x0008E330 File Offset: 0x0008C530
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return this.stream.BeginRead(buffer, offset, count, callback, state);
		}

		// Token: 0x06001870 RID: 6256 RVA: 0x0008E344 File Offset: 0x0008C544
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return this.stream.BeginWrite(buffer, offset, count, callback, state);
		}

		// Token: 0x06001871 RID: 6257 RVA: 0x0008E358 File Offset: 0x0008C558
		public override int EndRead(IAsyncResult asyncResult)
		{
			return this.stream.EndRead(asyncResult);
		}

		// Token: 0x06001872 RID: 6258 RVA: 0x0008E366 File Offset: 0x0008C566
		public override void EndWrite(IAsyncResult asyncResult)
		{
			this.stream.EndWrite(asyncResult);
		}

		// Token: 0x06001873 RID: 6259 RVA: 0x0008E374 File Offset: 0x0008C574
		public override void Flush()
		{
			this.stream.Flush();
		}

		// Token: 0x06001874 RID: 6260 RVA: 0x0008E381 File Offset: 0x0008C581
		public override int Read(byte[] buffer, int offset, int count)
		{
			return this.stream.Read(buffer, offset, count);
		}

		// Token: 0x06001875 RID: 6261 RVA: 0x0008E391 File Offset: 0x0008C591
		public override int ReadByte()
		{
			return this.stream.ReadByte();
		}

		// Token: 0x06001876 RID: 6262 RVA: 0x0008E39E File Offset: 0x0008C59E
		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.stream.Seek(offset, origin);
		}

		// Token: 0x06001877 RID: 6263 RVA: 0x0008E3AD File Offset: 0x0008C5AD
		public override void SetLength(long value)
		{
			this.stream.SetLength(value);
		}

		// Token: 0x06001878 RID: 6264 RVA: 0x0008E3BB File Offset: 0x0008C5BB
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.stream.Write(buffer, offset, count);
		}

		// Token: 0x06001879 RID: 6265 RVA: 0x0008E3CB File Offset: 0x0008C5CB
		public override void WriteByte(byte value)
		{
			this.stream.WriteByte(value);
		}

		// Token: 0x17000496 RID: 1174
		// (get) Token: 0x0600187A RID: 6266 RVA: 0x0008E3D9 File Offset: 0x0008C5D9
		public override bool CanRead
		{
			get
			{
				return this.stream.CanRead;
			}
		}

		// Token: 0x17000497 RID: 1175
		// (get) Token: 0x0600187B RID: 6267 RVA: 0x0008E3E6 File Offset: 0x0008C5E6
		public override bool CanSeek
		{
			get
			{
				return this.stream.CanSeek;
			}
		}

		// Token: 0x17000498 RID: 1176
		// (get) Token: 0x0600187C RID: 6268 RVA: 0x0008E3F3 File Offset: 0x0008C5F3
		public override bool CanWrite
		{
			get
			{
				return this.stream.CanWrite;
			}
		}

		// Token: 0x17000499 RID: 1177
		// (get) Token: 0x0600187D RID: 6269 RVA: 0x0008E400 File Offset: 0x0008C600
		public override long Length
		{
			get
			{
				return this.stream.Length;
			}
		}

		// Token: 0x1700049A RID: 1178
		// (get) Token: 0x0600187E RID: 6270 RVA: 0x0008E40D File Offset: 0x0008C60D
		// (set) Token: 0x0600187F RID: 6271 RVA: 0x0008E41A File Offset: 0x0008C61A
		public override long Position
		{
			get
			{
				return this.stream.Position;
			}
			set
			{
				this.stream.Position = value;
			}
		}

		// Token: 0x0400100E RID: 4110
		protected Stream stream;

		// Token: 0x0400100F RID: 4111
		private XmlDownloadManager downloadManager;

		// Token: 0x04001010 RID: 4112
		private string host;
	}
}
