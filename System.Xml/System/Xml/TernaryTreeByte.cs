﻿using System;

namespace System.Xml
{
	// Token: 0x020000BC RID: 188
	internal enum TernaryTreeByte
	{
		// Token: 0x040003A6 RID: 934
		characterByte,
		// Token: 0x040003A7 RID: 935
		leftTree,
		// Token: 0x040003A8 RID: 936
		rightTree,
		// Token: 0x040003A9 RID: 937
		data
	}
}
