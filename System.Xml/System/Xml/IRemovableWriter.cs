﻿using System;

namespace System.Xml
{
	// Token: 0x020000A7 RID: 167
	internal interface IRemovableWriter
	{
		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06000574 RID: 1396
		// (set) Token: 0x06000575 RID: 1397
		OnRemoveWriter OnRemoveWriterEvent { get; set; }
	}
}
