﻿using System;

namespace System.Xml
{
	/// <summary>Defines the context for a set of <see cref="T:System.Xml.XmlDocument" /> objects.</summary>
	// Token: 0x0200022F RID: 559
	public class XmlImplementation
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlImplementation" /> class.</summary>
		// Token: 0x060014EC RID: 5356 RVA: 0x0007605B File Offset: 0x0007425B
		public XmlImplementation() : this(new NameTable())
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlImplementation" /> class with the <see cref="T:System.Xml.XmlNameTable" /> specified.</summary>
		/// <param name="nt">An <see cref="T:System.Xml.XmlNameTable" /> object.</param>
		// Token: 0x060014ED RID: 5357 RVA: 0x00076068 File Offset: 0x00074268
		public XmlImplementation(XmlNameTable nt)
		{
			this.nameTable = nt;
		}

		/// <summary>Tests if the Document Object Model (DOM) implementation implements a specific feature.</summary>
		/// <param name="strFeature">The package name of the feature to test. This name is not case-sensitive. </param>
		/// <param name="strVersion">This is the version number of the package name to test. If the version is not specified (<see langword="null" />), supporting any version of the feature causes the method to return <see langword="true" />. </param>
		/// <returns>
		///     <see langword="true" /> if the feature is implemented in the specified version; otherwise, <see langword="false" />.The following table shows the combinations that cause <see langword="HasFeature" /> to return <see langword="true" />.strFeature strVersion XML 1.0 XML 2.0 </returns>
		// Token: 0x060014EE RID: 5358 RVA: 0x00076077 File Offset: 0x00074277
		public bool HasFeature(string strFeature, string strVersion)
		{
			return string.Compare("XML", strFeature, StringComparison.OrdinalIgnoreCase) == 0 && (strVersion == null || strVersion == "1.0" || strVersion == "2.0");
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlDocument" />.</summary>
		/// <returns>The new <see langword="XmlDocument" /> object.</returns>
		// Token: 0x060014EF RID: 5359 RVA: 0x000760A7 File Offset: 0x000742A7
		public virtual XmlDocument CreateDocument()
		{
			return new XmlDocument(this);
		}

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x060014F0 RID: 5360 RVA: 0x000760AF File Offset: 0x000742AF
		internal XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		// Token: 0x04000DE0 RID: 3552
		private XmlNameTable nameTable;
	}
}
