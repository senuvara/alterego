﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Permissions;
using System.Text;

namespace System.Xml
{
	/// <summary>Specifies a set of features to support on the <see cref="T:System.Xml.XmlWriter" /> object created by the <see cref="Overload:System.Xml.XmlWriter.Create" /> method.</summary>
	// Token: 0x020001EC RID: 492
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public sealed class XmlWriterSettings
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlWriterSettings" /> class.</summary>
		// Token: 0x06001128 RID: 4392 RVA: 0x0006624A File Offset: 0x0006444A
		public XmlWriterSettings()
		{
			this.Initialize();
		}

		/// <summary>Gets or sets a value that indicates whether asynchronous <see cref="T:System.Xml.XmlWriter" /> methods can be used on a particular <see cref="T:System.Xml.XmlWriter" /> instance.</summary>
		/// <returns>
		///     <see langword="true" /> if asynchronous methods can be used; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x06001129 RID: 4393 RVA: 0x00066263 File Offset: 0x00064463
		// (set) Token: 0x0600112A RID: 4394 RVA: 0x0006626B File Offset: 0x0006446B
		public bool Async
		{
			get
			{
				return this.useAsync;
			}
			set
			{
				this.CheckReadOnly("Async");
				this.useAsync = value;
			}
		}

		/// <summary>Gets or sets the type of text encoding to use.</summary>
		/// <returns>The text encoding to use. The default is <see langword="Encoding.UTF8" />.</returns>
		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x0600112B RID: 4395 RVA: 0x0006627F File Offset: 0x0006447F
		// (set) Token: 0x0600112C RID: 4396 RVA: 0x00066287 File Offset: 0x00064487
		public Encoding Encoding
		{
			get
			{
				return this.encoding;
			}
			set
			{
				this.CheckReadOnly("Encoding");
				this.encoding = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to omit an XML declaration.</summary>
		/// <returns>
		///     <see langword="true" /> to omit the XML declaration; otherwise, <see langword="false" />. The default is <see langword="false" />, an XML declaration is written.</returns>
		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x0600112D RID: 4397 RVA: 0x0006629B File Offset: 0x0006449B
		// (set) Token: 0x0600112E RID: 4398 RVA: 0x000662A3 File Offset: 0x000644A3
		public bool OmitXmlDeclaration
		{
			get
			{
				return this.omitXmlDecl;
			}
			set
			{
				this.CheckReadOnly("OmitXmlDeclaration");
				this.omitXmlDecl = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to normalize line breaks in the output.</summary>
		/// <returns>One of the <see cref="T:System.Xml.NewLineHandling" /> values. The default is <see cref="F:System.Xml.NewLineHandling.Replace" />.</returns>
		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x0600112F RID: 4399 RVA: 0x000662B7 File Offset: 0x000644B7
		// (set) Token: 0x06001130 RID: 4400 RVA: 0x000662BF File Offset: 0x000644BF
		public NewLineHandling NewLineHandling
		{
			get
			{
				return this.newLineHandling;
			}
			set
			{
				this.CheckReadOnly("NewLineHandling");
				if (value > NewLineHandling.None)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.newLineHandling = value;
			}
		}

		/// <summary>Gets or sets the character string to use for line breaks.</summary>
		/// <returns>The character string to use for line breaks. This can be set to any string value. However, to ensure valid XML, you should specify only valid white space characters, such as space characters, tabs, carriage returns, or line feeds. The default is \r\n (carriage return, new line).</returns>
		/// <exception cref="T:System.ArgumentNullException">The value assigned to the <see cref="P:System.Xml.XmlWriterSettings.NewLineChars" /> is <see langword="null" />.</exception>
		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06001131 RID: 4401 RVA: 0x000662E2 File Offset: 0x000644E2
		// (set) Token: 0x06001132 RID: 4402 RVA: 0x000662EA File Offset: 0x000644EA
		public string NewLineChars
		{
			get
			{
				return this.newLineChars;
			}
			set
			{
				this.CheckReadOnly("NewLineChars");
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.newLineChars = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to indent elements.</summary>
		/// <returns>
		///     <see langword="true" /> to write individual elements on new lines and indent; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06001133 RID: 4403 RVA: 0x0006630C File Offset: 0x0006450C
		// (set) Token: 0x06001134 RID: 4404 RVA: 0x00066317 File Offset: 0x00064517
		public bool Indent
		{
			get
			{
				return this.indent == TriState.True;
			}
			set
			{
				this.CheckReadOnly("Indent");
				this.indent = (value ? TriState.True : TriState.False);
			}
		}

		/// <summary>Gets or sets the character string to use when indenting. This setting is used when the <see cref="P:System.Xml.XmlWriterSettings.Indent" /> property is set to <see langword="true" />.</summary>
		/// <returns>The character string to use when indenting. This can be set to any string value. However, to ensure valid XML, you should specify only valid white space characters, such as space characters, tabs, carriage returns, or line feeds. The default is two spaces.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value assigned to the <see cref="P:System.Xml.XmlWriterSettings.IndentChars" /> is <see langword="null" />.</exception>
		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06001135 RID: 4405 RVA: 0x00066331 File Offset: 0x00064531
		// (set) Token: 0x06001136 RID: 4406 RVA: 0x00066339 File Offset: 0x00064539
		public string IndentChars
		{
			get
			{
				return this.indentChars;
			}
			set
			{
				this.CheckReadOnly("IndentChars");
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.indentChars = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether to write attributes on a new line.</summary>
		/// <returns>
		///     <see langword="true" /> to write attributes on individual lines; otherwise, <see langword="false" />. The default is <see langword="false" />.This setting has no effect when the <see cref="P:System.Xml.XmlWriterSettings.Indent" /> property value is <see langword="false" />.When <see cref="P:System.Xml.XmlWriterSettings.NewLineOnAttributes" /> is set to <see langword="true" />, each attribute is pre-pended with a new line and one extra level of indentation.</returns>
		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06001137 RID: 4407 RVA: 0x0006635B File Offset: 0x0006455B
		// (set) Token: 0x06001138 RID: 4408 RVA: 0x00066363 File Offset: 0x00064563
		public bool NewLineOnAttributes
		{
			get
			{
				return this.newLineOnAttributes;
			}
			set
			{
				this.CheckReadOnly("NewLineOnAttributes");
				this.newLineOnAttributes = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the <see cref="T:System.Xml.XmlWriter" /> should also close the underlying stream or <see cref="T:System.IO.TextWriter" /> when the <see cref="M:System.Xml.XmlWriter.Close" /> method is called.</summary>
		/// <returns>
		///     <see langword="true" /> to also close the underlying stream or <see cref="T:System.IO.TextWriter" />; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06001139 RID: 4409 RVA: 0x00066377 File Offset: 0x00064577
		// (set) Token: 0x0600113A RID: 4410 RVA: 0x0006637F File Offset: 0x0006457F
		public bool CloseOutput
		{
			get
			{
				return this.closeOutput;
			}
			set
			{
				this.CheckReadOnly("CloseOutput");
				this.closeOutput = value;
			}
		}

		/// <summary>Gets or sets the level of conformance that the XML writer checks the XML output for.</summary>
		/// <returns>One of the enumeration values that specifies the level of conformance (document, fragment, or automatic detection). The default is <see cref="F:System.Xml.ConformanceLevel.Document" />.</returns>
		// Token: 0x170002DF RID: 735
		// (get) Token: 0x0600113B RID: 4411 RVA: 0x00066393 File Offset: 0x00064593
		// (set) Token: 0x0600113C RID: 4412 RVA: 0x0006639B File Offset: 0x0006459B
		public ConformanceLevel ConformanceLevel
		{
			get
			{
				return this.conformanceLevel;
			}
			set
			{
				this.CheckReadOnly("ConformanceLevel");
				if (value > ConformanceLevel.Document)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.conformanceLevel = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the XML writer should check to ensure that all characters in the document conform to the "2.2 Characters" section of the W3C XML 1.0 Recommendation.</summary>
		/// <returns>
		///     <see langword="true" /> to do character checking; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x0600113D RID: 4413 RVA: 0x000663BE File Offset: 0x000645BE
		// (set) Token: 0x0600113E RID: 4414 RVA: 0x000663C6 File Offset: 0x000645C6
		public bool CheckCharacters
		{
			get
			{
				return this.checkCharacters;
			}
			set
			{
				this.CheckReadOnly("CheckCharacters");
				this.checkCharacters = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="T:System.Xml.XmlWriter" /> should remove duplicate namespace declarations when writing XML content. The default behavior is for the writer to output all namespace declarations that are present in the writer's namespace resolver.</summary>
		/// <returns>The <see cref="T:System.Xml.NamespaceHandling" /> enumeration used to specify whether to remove duplicate namespace declarations in the <see cref="T:System.Xml.XmlWriter" />.</returns>
		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x0600113F RID: 4415 RVA: 0x000663DA File Offset: 0x000645DA
		// (set) Token: 0x06001140 RID: 4416 RVA: 0x000663E2 File Offset: 0x000645E2
		public NamespaceHandling NamespaceHandling
		{
			get
			{
				return this.namespaceHandling;
			}
			set
			{
				this.CheckReadOnly("NamespaceHandling");
				if (value > NamespaceHandling.OmitDuplicates)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.namespaceHandling = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="T:System.Xml.XmlWriter" /> will add closing tags to all unclosed element tags when the <see cref="M:System.Xml.XmlWriter.Close" /> method is called.</summary>
		/// <returns>
		///     <see langword="true" /> if all unclosed element tags will be closed out; otherwise, <see langword="false" />. The default value is <see langword="true" />. </returns>
		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06001141 RID: 4417 RVA: 0x00066405 File Offset: 0x00064605
		// (set) Token: 0x06001142 RID: 4418 RVA: 0x0006640D File Offset: 0x0006460D
		public bool WriteEndDocumentOnClose
		{
			get
			{
				return this.writeEndDocumentOnClose;
			}
			set
			{
				this.CheckReadOnly("WriteEndDocumentOnClose");
				this.writeEndDocumentOnClose = value;
			}
		}

		/// <summary>Gets the method used to serialize the <see cref="T:System.Xml.XmlWriter" /> output.</summary>
		/// <returns>One of the <see cref="T:System.Xml.XmlOutputMethod" /> values. The default is <see cref="F:System.Xml.XmlOutputMethod.Xml" />.</returns>
		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06001143 RID: 4419 RVA: 0x00066421 File Offset: 0x00064621
		// (set) Token: 0x06001144 RID: 4420 RVA: 0x00066429 File Offset: 0x00064629
		public XmlOutputMethod OutputMethod
		{
			get
			{
				return this.outputMethod;
			}
			internal set
			{
				this.outputMethod = value;
			}
		}

		/// <summary>Resets the members of the settings class to their default values.</summary>
		// Token: 0x06001145 RID: 4421 RVA: 0x00066432 File Offset: 0x00064632
		public void Reset()
		{
			this.CheckReadOnly("Reset");
			this.Initialize();
		}

		/// <summary>Creates a copy of the <see cref="T:System.Xml.XmlWriterSettings" /> instance.</summary>
		/// <returns>The cloned <see cref="T:System.Xml.XmlWriterSettings" /> object.</returns>
		// Token: 0x06001146 RID: 4422 RVA: 0x00066445 File Offset: 0x00064645
		public XmlWriterSettings Clone()
		{
			XmlWriterSettings xmlWriterSettings = base.MemberwiseClone() as XmlWriterSettings;
			xmlWriterSettings.cdataSections = new List<XmlQualifiedName>(this.cdataSections);
			xmlWriterSettings.isReadOnly = false;
			return xmlWriterSettings;
		}

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06001147 RID: 4423 RVA: 0x0006646A File Offset: 0x0006466A
		internal List<XmlQualifiedName> CDataSectionElements
		{
			get
			{
				return this.cdataSections;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="T:System.Xml.XmlWriter" /> does not escape URI attributes.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Xml.XmlWriter" /> do not escape URI attributes; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06001148 RID: 4424 RVA: 0x00066472 File Offset: 0x00064672
		// (set) Token: 0x06001149 RID: 4425 RVA: 0x0006647A File Offset: 0x0006467A
		public bool DoNotEscapeUriAttributes
		{
			get
			{
				return this.doNotEscapeUriAttributes;
			}
			set
			{
				this.CheckReadOnly("DoNotEscapeUriAttributes");
				this.doNotEscapeUriAttributes = value;
			}
		}

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x0600114A RID: 4426 RVA: 0x0006648E File Offset: 0x0006468E
		// (set) Token: 0x0600114B RID: 4427 RVA: 0x00066496 File Offset: 0x00064696
		internal bool MergeCDataSections
		{
			get
			{
				return this.mergeCDataSections;
			}
			set
			{
				this.CheckReadOnly("MergeCDataSections");
				this.mergeCDataSections = value;
			}
		}

		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x0600114C RID: 4428 RVA: 0x000664AA File Offset: 0x000646AA
		// (set) Token: 0x0600114D RID: 4429 RVA: 0x000664B2 File Offset: 0x000646B2
		internal string MediaType
		{
			get
			{
				return this.mediaType;
			}
			set
			{
				this.CheckReadOnly("MediaType");
				this.mediaType = value;
			}
		}

		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x0600114E RID: 4430 RVA: 0x000664C6 File Offset: 0x000646C6
		// (set) Token: 0x0600114F RID: 4431 RVA: 0x000664CE File Offset: 0x000646CE
		internal string DocTypeSystem
		{
			get
			{
				return this.docTypeSystem;
			}
			set
			{
				this.CheckReadOnly("DocTypeSystem");
				this.docTypeSystem = value;
			}
		}

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06001150 RID: 4432 RVA: 0x000664E2 File Offset: 0x000646E2
		// (set) Token: 0x06001151 RID: 4433 RVA: 0x000664EA File Offset: 0x000646EA
		internal string DocTypePublic
		{
			get
			{
				return this.docTypePublic;
			}
			set
			{
				this.CheckReadOnly("DocTypePublic");
				this.docTypePublic = value;
			}
		}

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06001152 RID: 4434 RVA: 0x000664FE File Offset: 0x000646FE
		// (set) Token: 0x06001153 RID: 4435 RVA: 0x00066506 File Offset: 0x00064706
		internal XmlStandalone Standalone
		{
			get
			{
				return this.standalone;
			}
			set
			{
				this.CheckReadOnly("Standalone");
				this.standalone = value;
			}
		}

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06001154 RID: 4436 RVA: 0x0006651A File Offset: 0x0006471A
		// (set) Token: 0x06001155 RID: 4437 RVA: 0x00066522 File Offset: 0x00064722
		internal bool AutoXmlDeclaration
		{
			get
			{
				return this.autoXmlDecl;
			}
			set
			{
				this.CheckReadOnly("AutoXmlDeclaration");
				this.autoXmlDecl = value;
			}
		}

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06001156 RID: 4438 RVA: 0x00066536 File Offset: 0x00064736
		// (set) Token: 0x06001157 RID: 4439 RVA: 0x0006653E File Offset: 0x0006473E
		internal TriState IndentInternal
		{
			get
			{
				return this.indent;
			}
			set
			{
				this.indent = value;
			}
		}

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06001158 RID: 4440 RVA: 0x00066547 File Offset: 0x00064747
		internal bool IsQuerySpecific
		{
			get
			{
				return this.cdataSections.Count != 0 || this.docTypePublic != null || this.docTypeSystem != null || this.standalone == XmlStandalone.Yes;
			}
		}

		// Token: 0x06001159 RID: 4441 RVA: 0x00066574 File Offset: 0x00064774
		internal XmlWriter CreateWriter(string outputFileName)
		{
			if (outputFileName == null)
			{
				throw new ArgumentNullException("outputFileName");
			}
			XmlWriterSettings xmlWriterSettings = this;
			if (!xmlWriterSettings.CloseOutput)
			{
				xmlWriterSettings = xmlWriterSettings.Clone();
				xmlWriterSettings.CloseOutput = true;
			}
			FileStream fileStream = null;
			XmlWriter result;
			try
			{
				fileStream = new FileStream(outputFileName, FileMode.Create, FileAccess.Write, FileShare.Read, 4096, this.useAsync);
				result = xmlWriterSettings.CreateWriter(fileStream);
			}
			catch
			{
				if (fileStream != null)
				{
					fileStream.Close();
				}
				throw;
			}
			return result;
		}

		// Token: 0x0600115A RID: 4442 RVA: 0x000665E8 File Offset: 0x000647E8
		internal XmlWriter CreateWriter(Stream output)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			XmlWriter xmlWriter;
			if (this.Encoding.WebName == "utf-8")
			{
				switch (this.OutputMethod)
				{
				case XmlOutputMethod.Xml:
					if (this.Indent)
					{
						xmlWriter = new XmlUtf8RawTextWriterIndent(output, this);
					}
					else
					{
						xmlWriter = new XmlUtf8RawTextWriter(output, this);
					}
					break;
				case XmlOutputMethod.Html:
					if (this.Indent)
					{
						xmlWriter = new HtmlUtf8RawTextWriterIndent(output, this);
					}
					else
					{
						xmlWriter = new HtmlUtf8RawTextWriter(output, this);
					}
					break;
				case XmlOutputMethod.Text:
					xmlWriter = new TextUtf8RawTextWriter(output, this);
					break;
				case XmlOutputMethod.AutoDetect:
					xmlWriter = new XmlAutoDetectWriter(output, this);
					break;
				default:
					return null;
				}
			}
			else
			{
				switch (this.OutputMethod)
				{
				case XmlOutputMethod.Xml:
					if (this.Indent)
					{
						xmlWriter = new XmlEncodedRawTextWriterIndent(output, this);
					}
					else
					{
						xmlWriter = new XmlEncodedRawTextWriter(output, this);
					}
					break;
				case XmlOutputMethod.Html:
					if (this.Indent)
					{
						xmlWriter = new HtmlEncodedRawTextWriterIndent(output, this);
					}
					else
					{
						xmlWriter = new HtmlEncodedRawTextWriter(output, this);
					}
					break;
				case XmlOutputMethod.Text:
					xmlWriter = new TextEncodedRawTextWriter(output, this);
					break;
				case XmlOutputMethod.AutoDetect:
					xmlWriter = new XmlAutoDetectWriter(output, this);
					break;
				default:
					return null;
				}
			}
			if (this.OutputMethod != XmlOutputMethod.AutoDetect && this.IsQuerySpecific)
			{
				xmlWriter = new QueryOutputWriter((XmlRawWriter)xmlWriter, this);
			}
			xmlWriter = new XmlWellFormedWriter(xmlWriter, this);
			if (this.useAsync)
			{
				xmlWriter = new XmlAsyncCheckWriter(xmlWriter);
			}
			return xmlWriter;
		}

		// Token: 0x0600115B RID: 4443 RVA: 0x00066738 File Offset: 0x00064938
		internal XmlWriter CreateWriter(TextWriter output)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			XmlWriter xmlWriter;
			switch (this.OutputMethod)
			{
			case XmlOutputMethod.Xml:
				if (this.Indent)
				{
					xmlWriter = new XmlEncodedRawTextWriterIndent(output, this);
				}
				else
				{
					xmlWriter = new XmlEncodedRawTextWriter(output, this);
				}
				break;
			case XmlOutputMethod.Html:
				if (this.Indent)
				{
					xmlWriter = new HtmlEncodedRawTextWriterIndent(output, this);
				}
				else
				{
					xmlWriter = new HtmlEncodedRawTextWriter(output, this);
				}
				break;
			case XmlOutputMethod.Text:
				xmlWriter = new TextEncodedRawTextWriter(output, this);
				break;
			case XmlOutputMethod.AutoDetect:
				xmlWriter = new XmlAutoDetectWriter(output, this);
				break;
			default:
				return null;
			}
			if (this.OutputMethod != XmlOutputMethod.AutoDetect && this.IsQuerySpecific)
			{
				xmlWriter = new QueryOutputWriter((XmlRawWriter)xmlWriter, this);
			}
			xmlWriter = new XmlWellFormedWriter(xmlWriter, this);
			if (this.useAsync)
			{
				xmlWriter = new XmlAsyncCheckWriter(xmlWriter);
			}
			return xmlWriter;
		}

		// Token: 0x0600115C RID: 4444 RVA: 0x000667F6 File Offset: 0x000649F6
		internal XmlWriter CreateWriter(XmlWriter output)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			return this.AddConformanceWrapper(output);
		}

		// Token: 0x170002EE RID: 750
		// (get) Token: 0x0600115D RID: 4445 RVA: 0x0006680D File Offset: 0x00064A0D
		// (set) Token: 0x0600115E RID: 4446 RVA: 0x00066815 File Offset: 0x00064A15
		internal bool ReadOnly
		{
			get
			{
				return this.isReadOnly;
			}
			set
			{
				this.isReadOnly = value;
			}
		}

		// Token: 0x0600115F RID: 4447 RVA: 0x0006681E File Offset: 0x00064A1E
		private void CheckReadOnly(string propertyName)
		{
			if (this.isReadOnly)
			{
				throw new XmlException("The '{0}' property is read only and cannot be set.", base.GetType().Name + "." + propertyName);
			}
		}

		// Token: 0x06001160 RID: 4448 RVA: 0x0006684C File Offset: 0x00064A4C
		private void Initialize()
		{
			this.encoding = Encoding.UTF8;
			this.omitXmlDecl = false;
			this.newLineHandling = NewLineHandling.Replace;
			this.newLineChars = Environment.NewLine;
			this.indent = TriState.Unknown;
			this.indentChars = "  ";
			this.newLineOnAttributes = false;
			this.closeOutput = false;
			this.namespaceHandling = NamespaceHandling.Default;
			this.conformanceLevel = ConformanceLevel.Document;
			this.checkCharacters = true;
			this.writeEndDocumentOnClose = true;
			this.outputMethod = XmlOutputMethod.Xml;
			this.cdataSections.Clear();
			this.mergeCDataSections = false;
			this.mediaType = null;
			this.docTypeSystem = null;
			this.docTypePublic = null;
			this.standalone = XmlStandalone.Omit;
			this.doNotEscapeUriAttributes = false;
			this.useAsync = false;
			this.isReadOnly = false;
		}

		// Token: 0x06001161 RID: 4449 RVA: 0x00066904 File Offset: 0x00064B04
		private XmlWriter AddConformanceWrapper(XmlWriter baseWriter)
		{
			ConformanceLevel conformanceLevel = ConformanceLevel.Auto;
			XmlWriterSettings settings = baseWriter.Settings;
			bool flag = false;
			bool checkNames = false;
			bool flag2 = false;
			bool flag3 = false;
			if (settings == null)
			{
				if (this.newLineHandling == NewLineHandling.Replace)
				{
					flag2 = true;
					flag3 = true;
				}
				if (this.checkCharacters)
				{
					flag = true;
					flag3 = true;
				}
			}
			else
			{
				if (this.conformanceLevel != settings.ConformanceLevel)
				{
					conformanceLevel = this.ConformanceLevel;
					flag3 = true;
				}
				if (this.checkCharacters && !settings.CheckCharacters)
				{
					flag = true;
					checkNames = (conformanceLevel == ConformanceLevel.Auto);
					flag3 = true;
				}
				if (this.newLineHandling == NewLineHandling.Replace && settings.NewLineHandling == NewLineHandling.None)
				{
					flag2 = true;
					flag3 = true;
				}
			}
			XmlWriter xmlWriter = baseWriter;
			if (flag3)
			{
				if (conformanceLevel != ConformanceLevel.Auto)
				{
					xmlWriter = new XmlWellFormedWriter(xmlWriter, this);
				}
				if (flag || flag2)
				{
					xmlWriter = new XmlCharCheckingWriter(xmlWriter, flag, checkNames, flag2, this.NewLineChars);
				}
			}
			if (this.IsQuerySpecific && (settings == null || !settings.IsQuerySpecific))
			{
				xmlWriter = new QueryOutputWriterV1(xmlWriter, this);
			}
			return xmlWriter;
		}

		// Token: 0x04000C38 RID: 3128
		private bool useAsync;

		// Token: 0x04000C39 RID: 3129
		private Encoding encoding;

		// Token: 0x04000C3A RID: 3130
		private bool omitXmlDecl;

		// Token: 0x04000C3B RID: 3131
		private NewLineHandling newLineHandling;

		// Token: 0x04000C3C RID: 3132
		private string newLineChars;

		// Token: 0x04000C3D RID: 3133
		private TriState indent;

		// Token: 0x04000C3E RID: 3134
		private string indentChars;

		// Token: 0x04000C3F RID: 3135
		private bool newLineOnAttributes;

		// Token: 0x04000C40 RID: 3136
		private bool closeOutput;

		// Token: 0x04000C41 RID: 3137
		private NamespaceHandling namespaceHandling;

		// Token: 0x04000C42 RID: 3138
		private ConformanceLevel conformanceLevel;

		// Token: 0x04000C43 RID: 3139
		private bool checkCharacters;

		// Token: 0x04000C44 RID: 3140
		private bool writeEndDocumentOnClose;

		// Token: 0x04000C45 RID: 3141
		private XmlOutputMethod outputMethod;

		// Token: 0x04000C46 RID: 3142
		private List<XmlQualifiedName> cdataSections = new List<XmlQualifiedName>();

		// Token: 0x04000C47 RID: 3143
		private bool doNotEscapeUriAttributes;

		// Token: 0x04000C48 RID: 3144
		private bool mergeCDataSections;

		// Token: 0x04000C49 RID: 3145
		private string mediaType;

		// Token: 0x04000C4A RID: 3146
		private string docTypeSystem;

		// Token: 0x04000C4B RID: 3147
		private string docTypePublic;

		// Token: 0x04000C4C RID: 3148
		private XmlStandalone standalone;

		// Token: 0x04000C4D RID: 3149
		private bool autoXmlDecl;

		// Token: 0x04000C4E RID: 3150
		private bool isReadOnly;
	}
}
