﻿using System;
using System.Text;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	/// <summary>Represents an attribute that specifies the derived types that the <see cref="T:System.Xml.Serialization.XmlSerializer" /> can place in a serialized array.</summary>
	// Token: 0x02000308 RID: 776
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue, AllowMultiple = true)]
	public class XmlArrayItemAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlArrayItemAttribute" /> class.</summary>
		// Token: 0x06001C9D RID: 7325 RVA: 0x0000D3B3 File Offset: 0x0000B5B3
		public XmlArrayItemAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlArrayItemAttribute" /> class and specifies the name of the XML element generated in the XML document.</summary>
		/// <param name="elementName">The name of the XML element. </param>
		// Token: 0x06001C9E RID: 7326 RVA: 0x000A4A34 File Offset: 0x000A2C34
		public XmlArrayItemAttribute(string elementName)
		{
			this.elementName = elementName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlArrayItemAttribute" /> class and specifies the <see cref="T:System.Type" /> that can be inserted into the serialized array.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of the object to serialize. </param>
		// Token: 0x06001C9F RID: 7327 RVA: 0x000A4A43 File Offset: 0x000A2C43
		public XmlArrayItemAttribute(Type type)
		{
			this.type = type;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlArrayItemAttribute" /> class and specifies the name of the XML element generated in the XML document and the <see cref="T:System.Type" /> that can be inserted into the generated XML document.</summary>
		/// <param name="elementName">The name of the XML element. </param>
		/// <param name="type">The <see cref="T:System.Type" /> of the object to serialize. </param>
		// Token: 0x06001CA0 RID: 7328 RVA: 0x000A4A52 File Offset: 0x000A2C52
		public XmlArrayItemAttribute(string elementName, Type type)
		{
			this.elementName = elementName;
			this.type = type;
		}

		/// <summary>Gets or sets the XML data type of the generated XML element.</summary>
		/// <returns>An XML schema definition (XSD) data type, as defined by the World Wide Web Consortium (www.w3.org) document "XML Schema Part 2: DataTypes".</returns>
		// Token: 0x17000577 RID: 1399
		// (get) Token: 0x06001CA1 RID: 7329 RVA: 0x000A4A68 File Offset: 0x000A2C68
		// (set) Token: 0x06001CA2 RID: 7330 RVA: 0x000A4A7E File Offset: 0x000A2C7E
		public string DataType
		{
			get
			{
				if (this.dataType == null)
				{
					return string.Empty;
				}
				return this.dataType;
			}
			set
			{
				this.dataType = value;
			}
		}

		/// <summary>Gets or sets the name of the generated XML element.</summary>
		/// <returns>The name of the generated XML element. The default is the member identifier.</returns>
		// Token: 0x17000578 RID: 1400
		// (get) Token: 0x06001CA3 RID: 7331 RVA: 0x000A4A87 File Offset: 0x000A2C87
		// (set) Token: 0x06001CA4 RID: 7332 RVA: 0x000A4A9D File Offset: 0x000A2C9D
		public string ElementName
		{
			get
			{
				if (this.elementName == null)
				{
					return string.Empty;
				}
				return this.elementName;
			}
			set
			{
				this.elementName = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the name of the generated XML element is qualified.</summary>
		/// <returns>One of the <see cref="T:System.Xml.Schema.XmlSchemaForm" /> values. The default is <see langword="XmlSchemaForm.None" />.</returns>
		/// <exception cref="T:System.Exception">The <see cref="P:System.Xml.Serialization.XmlArrayItemAttribute.Form" /> property is set to <see langword="XmlSchemaForm.Unqualified" /> and a <see cref="P:System.Xml.Serialization.XmlArrayItemAttribute.Namespace" /> value is specified. </exception>
		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x06001CA5 RID: 7333 RVA: 0x000A4AA6 File Offset: 0x000A2CA6
		// (set) Token: 0x06001CA6 RID: 7334 RVA: 0x000A4AAE File Offset: 0x000A2CAE
		public XmlSchemaForm Form
		{
			get
			{
				return this.form;
			}
			set
			{
				this.form = value;
			}
		}

		/// <summary>Gets or sets the namespace of the generated XML element.</summary>
		/// <returns>The namespace of the generated XML element.</returns>
		// Token: 0x1700057A RID: 1402
		// (get) Token: 0x06001CA7 RID: 7335 RVA: 0x000A4AB7 File Offset: 0x000A2CB7
		// (set) Token: 0x06001CA8 RID: 7336 RVA: 0x000A4ABF File Offset: 0x000A2CBF
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="T:System.Xml.Serialization.XmlSerializer" /> must serialize a member as an empty XML tag with the <see langword="xsi:nil" /> attribute set to <see langword="true" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Xml.Serialization.XmlSerializer" /> generates the <see langword="xsi:nil" /> attribute; otherwise, <see langword="false" />, and no instance is generated. The default is <see langword="true" />.</returns>
		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x06001CA9 RID: 7337 RVA: 0x000A4AC8 File Offset: 0x000A2CC8
		// (set) Token: 0x06001CAA RID: 7338 RVA: 0x000A4AD0 File Offset: 0x000A2CD0
		public bool IsNullable
		{
			get
			{
				return this.isNullable;
			}
			set
			{
				this.isNullableSpecified = true;
				this.isNullable = value;
			}
		}

		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x06001CAB RID: 7339 RVA: 0x000A4AE0 File Offset: 0x000A2CE0
		internal bool IsNullableSpecified
		{
			get
			{
				return this.isNullableSpecified;
			}
		}

		/// <summary>Gets or sets the type allowed in an array.</summary>
		/// <returns>A <see cref="T:System.Type" /> that is allowed in the array.</returns>
		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x06001CAC RID: 7340 RVA: 0x000A4AE8 File Offset: 0x000A2CE8
		// (set) Token: 0x06001CAD RID: 7341 RVA: 0x000A4AF0 File Offset: 0x000A2CF0
		public Type Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		/// <summary>Gets or sets the level in a hierarchy of XML elements that the <see cref="T:System.Xml.Serialization.XmlArrayItemAttribute" /> affects.</summary>
		/// <returns>The zero-based index of a set of indexes in an array of arrays.</returns>
		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x06001CAE RID: 7342 RVA: 0x000A4AF9 File Offset: 0x000A2CF9
		// (set) Token: 0x06001CAF RID: 7343 RVA: 0x000A4B01 File Offset: 0x000A2D01
		public int NestingLevel
		{
			get
			{
				return this.nestingLevel;
			}
			set
			{
				this.nestingLevel = value;
			}
		}

		// Token: 0x06001CB0 RID: 7344 RVA: 0x000A4B0C File Offset: 0x000A2D0C
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XAIA ");
			KeyHelper.AddField(sb, 1, this.ns);
			KeyHelper.AddField(sb, 2, this.elementName);
			KeyHelper.AddField(sb, 3, this.form.ToString(), XmlSchemaForm.None.ToString());
			KeyHelper.AddField(sb, 4, this.isNullable, true);
			KeyHelper.AddField(sb, 5, this.dataType);
			KeyHelper.AddField(sb, 6, this.nestingLevel, 0);
			KeyHelper.AddField(sb, 7, this.type);
			sb.Append('|');
		}

		// Token: 0x0400164D RID: 5709
		private string dataType;

		// Token: 0x0400164E RID: 5710
		private string elementName;

		// Token: 0x0400164F RID: 5711
		private XmlSchemaForm form;

		// Token: 0x04001650 RID: 5712
		private string ns;

		// Token: 0x04001651 RID: 5713
		private bool isNullable;

		// Token: 0x04001652 RID: 5714
		private bool isNullableSpecified;

		// Token: 0x04001653 RID: 5715
		private int nestingLevel;

		// Token: 0x04001654 RID: 5716
		private Type type;
	}
}
