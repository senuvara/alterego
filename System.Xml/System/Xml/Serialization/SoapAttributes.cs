﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Represents a collection of attribute objects that control how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes and deserializes SOAP methods.</summary>
	// Token: 0x020002F8 RID: 760
	public class SoapAttributes
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.SoapAttributes" /> class.</summary>
		// Token: 0x06001BF6 RID: 7158 RVA: 0x000A19F4 File Offset: 0x0009FBF4
		public SoapAttributes()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.SoapAttributes" /> class using the specified custom type.</summary>
		/// <param name="provider">Any object that implements the <see cref="T:System.Reflection.ICustomAttributeProvider" /> interface, such as the <see cref="T:System.Type" /> class.</param>
		// Token: 0x06001BF7 RID: 7159 RVA: 0x000A1A08 File Offset: 0x0009FC08
		public SoapAttributes(ICustomAttributeProvider provider)
		{
			foreach (object obj in provider.GetCustomAttributes(false))
			{
				if (obj is SoapAttributeAttribute)
				{
					this.soapAttribute = (SoapAttributeAttribute)obj;
				}
				else if (obj is DefaultValueAttribute)
				{
					this.soapDefaultValue = ((DefaultValueAttribute)obj).Value;
				}
				else if (obj is SoapElementAttribute)
				{
					this.soapElement = (SoapElementAttribute)obj;
				}
				else if (obj is SoapEnumAttribute)
				{
					this.soapEnum = (SoapEnumAttribute)obj;
				}
				else if (obj is SoapIgnoreAttribute)
				{
					this.soapIgnore = true;
				}
				else if (obj is SoapTypeAttribute)
				{
					this.soapType = (SoapTypeAttribute)obj;
				}
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Serialization.SoapAttributeAttribute" /> to override.</summary>
		/// <returns>A <see cref="T:System.Xml.Serialization.SoapAttributeAttribute" /> that overrides the behavior of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> when the member is serialized.</returns>
		// Token: 0x17000547 RID: 1351
		// (get) Token: 0x06001BF8 RID: 7160 RVA: 0x000A1AC8 File Offset: 0x0009FCC8
		// (set) Token: 0x06001BF9 RID: 7161 RVA: 0x000A1AD0 File Offset: 0x0009FCD0
		public SoapAttributeAttribute SoapAttribute
		{
			get
			{
				return this.soapAttribute;
			}
			set
			{
				this.soapAttribute = value;
			}
		}

		/// <summary>Gets or sets the default value of an XML element or attribute.</summary>
		/// <returns>An object that represents the default value of an XML element or attribute.</returns>
		// Token: 0x17000548 RID: 1352
		// (get) Token: 0x06001BFA RID: 7162 RVA: 0x000A1AD9 File Offset: 0x0009FCD9
		// (set) Token: 0x06001BFB RID: 7163 RVA: 0x000A1AE1 File Offset: 0x0009FCE1
		public object SoapDefaultValue
		{
			get
			{
				return this.soapDefaultValue;
			}
			set
			{
				this.soapDefaultValue = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Xml.Serialization.SoapElementAttribute" /> to override.</summary>
		/// <returns>The <see cref="T:System.Xml.Serialization.SoapElementAttribute" /> to override.</returns>
		// Token: 0x17000549 RID: 1353
		// (get) Token: 0x06001BFC RID: 7164 RVA: 0x000A1AEA File Offset: 0x0009FCEA
		// (set) Token: 0x06001BFD RID: 7165 RVA: 0x000A1AF2 File Offset: 0x0009FCF2
		public SoapElementAttribute SoapElement
		{
			get
			{
				return this.soapElement;
			}
			set
			{
				this.soapElement = value;
			}
		}

		/// <summary>Gets or sets an object that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a SOAP enumeration.</summary>
		/// <returns>An object that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes an enumeration member.</returns>
		// Token: 0x1700054A RID: 1354
		// (get) Token: 0x06001BFE RID: 7166 RVA: 0x000A1AFB File Offset: 0x0009FCFB
		// (set) Token: 0x06001BFF RID: 7167 RVA: 0x000A1B03 File Offset: 0x0009FD03
		public SoapEnumAttribute SoapEnum
		{
			get
			{
				return this.soapEnum;
			}
			set
			{
				this.soapEnum = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a public field or property as encoded SOAP XML.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Xml.Serialization.XmlSerializer" /> must not serialize the field or property; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700054B RID: 1355
		// (get) Token: 0x06001C00 RID: 7168 RVA: 0x000A1B0C File Offset: 0x0009FD0C
		// (set) Token: 0x06001C01 RID: 7169 RVA: 0x000A1B14 File Offset: 0x0009FD14
		public bool SoapIgnore
		{
			get
			{
				return this.soapIgnore;
			}
			set
			{
				this.soapIgnore = value;
			}
		}

		/// <summary>Gets or sets an object that instructs the <see cref="T:System.Xml.Serialization.XmlSerializer" /> how to serialize an object type into encoded SOAP XML.</summary>
		/// <returns>A <see cref="T:System.Xml.Serialization.SoapTypeAttribute" /> that either overrides a <see cref="T:System.Xml.Serialization.SoapTypeAttribute" /> applied to a class declaration, or is applied to a class declaration.</returns>
		// Token: 0x1700054C RID: 1356
		// (get) Token: 0x06001C02 RID: 7170 RVA: 0x000A1B1D File Offset: 0x0009FD1D
		// (set) Token: 0x06001C03 RID: 7171 RVA: 0x000A1B25 File Offset: 0x0009FD25
		public SoapTypeAttribute SoapType
		{
			get
			{
				return this.soapType;
			}
			set
			{
				this.soapType = value;
			}
		}

		// Token: 0x06001C04 RID: 7172 RVA: 0x000A1B30 File Offset: 0x0009FD30
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("SA ");
			if (this.soapIgnore)
			{
				sb.Append('i');
			}
			if (this.soapAttribute != null)
			{
				this.soapAttribute.AddKeyHash(sb);
			}
			if (this.soapElement != null)
			{
				this.soapElement.AddKeyHash(sb);
			}
			if (this.soapEnum != null)
			{
				this.soapEnum.AddKeyHash(sb);
			}
			if (this.soapType != null)
			{
				this.soapType.AddKeyHash(sb);
			}
			if (this.soapDefaultValue == null)
			{
				sb.Append("n");
			}
			else if (!(this.soapDefaultValue is DBNull))
			{
				string str = XmlCustomFormatter.ToXmlString(TypeTranslator.GetTypeData(this.soapDefaultValue.GetType()), this.soapDefaultValue);
				sb.Append("v" + str);
			}
			sb.Append("|");
		}

		// Token: 0x04001616 RID: 5654
		private SoapAttributeAttribute soapAttribute;

		// Token: 0x04001617 RID: 5655
		private object soapDefaultValue = DBNull.Value;

		// Token: 0x04001618 RID: 5656
		private SoapElementAttribute soapElement;

		// Token: 0x04001619 RID: 5657
		private SoapEnumAttribute soapEnum;

		// Token: 0x0400161A RID: 5658
		private bool soapIgnore;

		// Token: 0x0400161B RID: 5659
		private SoapTypeAttribute soapType;
	}
}
