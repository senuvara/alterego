﻿using System;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Provides data for the <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownElement" /> event.</summary>
	// Token: 0x020002DC RID: 732
	public class XmlElementEventArgs : EventArgs
	{
		// Token: 0x06001B27 RID: 6951 RVA: 0x00098C6B File Offset: 0x00096E6B
		internal XmlElementEventArgs(XmlElement elem, int lineNumber, int linePosition, object o, string qnames)
		{
			this.elem = elem;
			this.o = o;
			this.qnames = qnames;
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		/// <summary>Gets the object the <see cref="T:System.Xml.Serialization.XmlSerializer" /> is deserializing.</summary>
		/// <returns>The object that is being deserialized by the <see cref="T:System.Xml.Serialization.XmlSerializer" />.</returns>
		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x06001B28 RID: 6952 RVA: 0x00098C98 File Offset: 0x00096E98
		public object ObjectBeingDeserialized
		{
			get
			{
				return this.o;
			}
		}

		/// <summary>Gets the object that represents the unknown XML element.</summary>
		/// <returns>The object that represents the unknown XML element.</returns>
		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x06001B29 RID: 6953 RVA: 0x00098CA0 File Offset: 0x00096EA0
		public XmlElement Element
		{
			get
			{
				return this.elem;
			}
		}

		/// <summary>Gets the line number where the unknown element was encountered if the XML reader is an <see cref="T:System.Xml.XmlTextReader" />.</summary>
		/// <returns>The line number where the unknown element was encountered if the XML reader is an <see cref="T:System.Xml.XmlTextReader" />; otherwise, -1.</returns>
		// Token: 0x1700052D RID: 1325
		// (get) Token: 0x06001B2A RID: 6954 RVA: 0x00098CA8 File Offset: 0x00096EA8
		public int LineNumber
		{
			get
			{
				return this.lineNumber;
			}
		}

		/// <summary>Gets the place in the line where the unknown element occurs if the XML reader is an <see cref="T:System.Xml.XmlTextReader" />.</summary>
		/// <returns>The number in the line where the unknown element occurs if the XML reader is an <see cref="T:System.Xml.XmlTextReader" />; otherwise, -1.</returns>
		// Token: 0x1700052E RID: 1326
		// (get) Token: 0x06001B2B RID: 6955 RVA: 0x00098CB0 File Offset: 0x00096EB0
		public int LinePosition
		{
			get
			{
				return this.linePosition;
			}
		}

		/// <summary>Gets a comma-delimited list of XML element names expected to be in an XML document instance.</summary>
		/// <returns>A comma-delimited list of XML element names. Each name is in the following format: <paramref name="namespace" />:<paramref name="name" />.</returns>
		// Token: 0x1700052F RID: 1327
		// (get) Token: 0x06001B2C RID: 6956 RVA: 0x00098CB8 File Offset: 0x00096EB8
		public string ExpectedElements
		{
			get
			{
				if (this.qnames != null)
				{
					return this.qnames;
				}
				return string.Empty;
			}
		}

		// Token: 0x06001B2D RID: 6957 RVA: 0x00072668 File Offset: 0x00070868
		internal XmlElementEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040015A4 RID: 5540
		private object o;

		// Token: 0x040015A5 RID: 5541
		private XmlElement elem;

		// Token: 0x040015A6 RID: 5542
		private string qnames;

		// Token: 0x040015A7 RID: 5543
		private int lineNumber;

		// Token: 0x040015A8 RID: 5544
		private int linePosition;
	}
}
