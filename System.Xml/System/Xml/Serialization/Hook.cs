﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002EE RID: 750
	[XmlType("hook")]
	internal class Hook
	{
		// Token: 0x06001BD6 RID: 7126 RVA: 0x000A14FE File Offset: 0x0009F6FE
		public string GetCode(HookAction action)
		{
			if (action == HookAction.InsertBefore)
			{
				return this.InsertBefore;
			}
			if (action == HookAction.InsertAfter)
			{
				return this.InsertAfter;
			}
			return this.Replace;
		}

		// Token: 0x06001BD7 RID: 7127 RVA: 0x00002103 File Offset: 0x00000303
		public Hook()
		{
		}

		// Token: 0x040015F3 RID: 5619
		[XmlAttribute("type")]
		public HookType HookType;

		// Token: 0x040015F4 RID: 5620
		[XmlElement("select")]
		public Select Select;

		// Token: 0x040015F5 RID: 5621
		[XmlElement("insertBefore")]
		public string InsertBefore;

		// Token: 0x040015F6 RID: 5622
		[XmlElement("insertAfter")]
		public string InsertAfter;

		// Token: 0x040015F7 RID: 5623
		[XmlElement("replace")]
		public string Replace;
	}
}
