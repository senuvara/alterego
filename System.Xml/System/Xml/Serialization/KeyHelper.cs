﻿using System;
using System.Globalization;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x020002E6 RID: 742
	internal class KeyHelper
	{
		// Token: 0x06001B51 RID: 6993 RVA: 0x00098E88 File Offset: 0x00097088
		public static void AddField(StringBuilder sb, int n, string val)
		{
			KeyHelper.AddField(sb, n, val, null);
		}

		// Token: 0x06001B52 RID: 6994 RVA: 0x00098E94 File Offset: 0x00097094
		public static void AddField(StringBuilder sb, int n, string val, string def)
		{
			if (val != def)
			{
				sb.Append(n.ToString());
				sb.Append(val.Length.ToString(CultureInfo.InvariantCulture));
				sb.Append(val);
			}
		}

		// Token: 0x06001B53 RID: 6995 RVA: 0x00098EDA File Offset: 0x000970DA
		public static void AddField(StringBuilder sb, int n, bool val)
		{
			KeyHelper.AddField(sb, n, val, false);
		}

		// Token: 0x06001B54 RID: 6996 RVA: 0x00098EE5 File Offset: 0x000970E5
		public static void AddField(StringBuilder sb, int n, bool val, bool def)
		{
			if (val != def)
			{
				sb.Append(n.ToString());
			}
		}

		// Token: 0x06001B55 RID: 6997 RVA: 0x00098EF9 File Offset: 0x000970F9
		public static void AddField(StringBuilder sb, int n, int val, int def)
		{
			if (val != def)
			{
				sb.Append(n.ToString());
				sb.Append(val.ToString(CultureInfo.InvariantCulture));
			}
		}

		// Token: 0x06001B56 RID: 6998 RVA: 0x00098F20 File Offset: 0x00097120
		public static void AddField(StringBuilder sb, int n, Type val)
		{
			if (val != null)
			{
				sb.Append(n.ToString(CultureInfo.InvariantCulture));
				sb.Append(val.ToString());
			}
		}

		// Token: 0x06001B57 RID: 6999 RVA: 0x00002103 File Offset: 0x00000303
		public KeyHelper()
		{
		}
	}
}
