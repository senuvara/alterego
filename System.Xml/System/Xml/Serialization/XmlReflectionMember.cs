﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Provides mappings between code entities in .NET Framework Web service methods and the content of Web Services Description Language (WSDL) messages that are defined for SOAP Web services. </summary>
	// Token: 0x0200031E RID: 798
	public class XmlReflectionMember
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlReflectionMember" /> class. </summary>
		// Token: 0x06001D94 RID: 7572 RVA: 0x00002103 File Offset: 0x00000303
		public XmlReflectionMember()
		{
		}

		// Token: 0x06001D95 RID: 7573 RVA: 0x000A93C8 File Offset: 0x000A75C8
		internal XmlReflectionMember(string name, Type type, XmlAttributes attributes)
		{
			this.memberName = name;
			this.memberType = type;
			this.xmlAttributes = attributes;
		}

		// Token: 0x06001D96 RID: 7574 RVA: 0x000A93E5 File Offset: 0x000A75E5
		internal XmlReflectionMember(string name, Type type, SoapAttributes attributes)
		{
			this.memberName = name;
			this.memberType = type;
			this.soapAttributes = attributes;
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="T:System.Xml.Serialization.XmlReflectionMember" /> represents a Web service method return value, as opposed to an output parameter. </summary>
		/// <returns>
		///     <see langword="true" />, if the member represents a Web service return value; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005C1 RID: 1473
		// (get) Token: 0x06001D97 RID: 7575 RVA: 0x000A9402 File Offset: 0x000A7602
		// (set) Token: 0x06001D98 RID: 7576 RVA: 0x000A940A File Offset: 0x000A760A
		public bool IsReturnValue
		{
			get
			{
				return this.isReturnValue;
			}
			set
			{
				this.isReturnValue = value;
			}
		}

		/// <summary>Gets or sets the name of the Web service method member for this mapping. </summary>
		/// <returns>The name of the Web service method.</returns>
		// Token: 0x170005C2 RID: 1474
		// (get) Token: 0x06001D99 RID: 7577 RVA: 0x000A9413 File Offset: 0x000A7613
		// (set) Token: 0x06001D9A RID: 7578 RVA: 0x000A941B File Offset: 0x000A761B
		public string MemberName
		{
			get
			{
				return this.memberName;
			}
			set
			{
				this.memberName = value;
			}
		}

		/// <summary>Gets or sets the type of the Web service method member code entity that is represented by this mapping. </summary>
		/// <returns>The <see cref="T:System.Type" /> of the Web service method member code entity that is represented by this mapping.</returns>
		// Token: 0x170005C3 RID: 1475
		// (get) Token: 0x06001D9B RID: 7579 RVA: 0x000A9424 File Offset: 0x000A7624
		// (set) Token: 0x06001D9C RID: 7580 RVA: 0x000A942C File Offset: 0x000A762C
		public Type MemberType
		{
			get
			{
				return this.memberType;
			}
			set
			{
				this.memberType = value;
			}
		}

		/// <summary>Gets or sets a value that indicates that the value of the corresponding XML element definition's isNullable attribute is <see langword="false" />.</summary>
		/// <returns>
		///     <see langword="True" /> to override the <see cref="P:System.Xml.Serialization.XmlElementAttribute.IsNullable" /> property; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005C4 RID: 1476
		// (get) Token: 0x06001D9D RID: 7581 RVA: 0x000A9435 File Offset: 0x000A7635
		// (set) Token: 0x06001D9E RID: 7582 RVA: 0x000A943D File Offset: 0x000A763D
		public bool OverrideIsNullable
		{
			get
			{
				return this.overrideIsNullable;
			}
			set
			{
				this.overrideIsNullable = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Xml.Serialization.SoapAttributes" /> with the collection of SOAP-related attributes that have been applied to the member code entity. </summary>
		/// <returns>A <see cref="T:System.Xml.Serialization.SoapAttributes" /> that contains the objects that represent SOAP attributes applied to the member.</returns>
		// Token: 0x170005C5 RID: 1477
		// (get) Token: 0x06001D9F RID: 7583 RVA: 0x000A9446 File Offset: 0x000A7646
		// (set) Token: 0x06001DA0 RID: 7584 RVA: 0x000A9461 File Offset: 0x000A7661
		public SoapAttributes SoapAttributes
		{
			get
			{
				if (this.soapAttributes == null)
				{
					this.soapAttributes = new SoapAttributes();
				}
				return this.soapAttributes;
			}
			set
			{
				this.soapAttributes = value;
			}
		}

		/// <summary>Gets or sets an <see cref="T:System.Xml.Serialization.XmlAttributes" /> with the collection of <see cref="T:System.Xml.Serialization.XmlSerializer" />-related attributes that have been applied to the member code entity. </summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlAttributes" /> that represents XML attributes that have been applied to the member code.</returns>
		// Token: 0x170005C6 RID: 1478
		// (get) Token: 0x06001DA1 RID: 7585 RVA: 0x000A946A File Offset: 0x000A766A
		// (set) Token: 0x06001DA2 RID: 7586 RVA: 0x000A9485 File Offset: 0x000A7685
		public XmlAttributes XmlAttributes
		{
			get
			{
				if (this.xmlAttributes == null)
				{
					this.xmlAttributes = new XmlAttributes();
				}
				return this.xmlAttributes;
			}
			set
			{
				this.xmlAttributes = value;
			}
		}

		// Token: 0x170005C7 RID: 1479
		// (get) Token: 0x06001DA3 RID: 7587 RVA: 0x000A948E File Offset: 0x000A768E
		// (set) Token: 0x06001DA4 RID: 7588 RVA: 0x000A9496 File Offset: 0x000A7696
		internal Type DeclaringType
		{
			get
			{
				return this.declaringType;
			}
			set
			{
				this.declaringType = value;
			}
		}

		// Token: 0x06001DA5 RID: 7589 RVA: 0x000A94A0 File Offset: 0x000A76A0
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XRM ");
			KeyHelper.AddField(sb, 1, this.isReturnValue);
			KeyHelper.AddField(sb, 1, this.memberName);
			KeyHelper.AddField(sb, 1, this.memberType);
			KeyHelper.AddField(sb, 1, this.overrideIsNullable);
			if (this.soapAttributes != null)
			{
				this.soapAttributes.AddKeyHash(sb);
			}
			if (this.xmlAttributes != null)
			{
				this.xmlAttributes.AddKeyHash(sb);
			}
			sb.Append('|');
		}

		// Token: 0x0400169B RID: 5787
		private bool isReturnValue;

		// Token: 0x0400169C RID: 5788
		private string memberName;

		// Token: 0x0400169D RID: 5789
		private Type memberType;

		// Token: 0x0400169E RID: 5790
		private bool overrideIsNullable;

		// Token: 0x0400169F RID: 5791
		private SoapAttributes soapAttributes;

		// Token: 0x040016A0 RID: 5792
		private XmlAttributes xmlAttributes;

		// Token: 0x040016A1 RID: 5793
		private Type declaringType;
	}
}
