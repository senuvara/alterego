﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Contains fields that can be used to pass event delegates to a thread-safe <see cref="Overload:System.Xml.Serialization.XmlSerializer.Deserialize" /> method of the <see cref="T:System.Xml.Serialization.XmlSerializer" />.</summary>
	// Token: 0x0200030F RID: 783
	public struct XmlDeserializationEvents
	{
		/// <summary>Gets or sets an object that represents the method that handles the <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownAttribute" /> event.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlAttributeEventHandler" /> that points to the event handler.</returns>
		// Token: 0x17000599 RID: 1433
		// (get) Token: 0x06001D13 RID: 7443 RVA: 0x000A6B14 File Offset: 0x000A4D14
		// (set) Token: 0x06001D14 RID: 7444 RVA: 0x000A6B1C File Offset: 0x000A4D1C
		public XmlAttributeEventHandler OnUnknownAttribute
		{
			get
			{
				return this.onUnknownAttribute;
			}
			set
			{
				this.onUnknownAttribute = value;
			}
		}

		/// <summary>Gets or sets an object that represents the method that handles the <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownElement" /> event.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlElementEventHandler" /> that points to the event handler.</returns>
		// Token: 0x1700059A RID: 1434
		// (get) Token: 0x06001D15 RID: 7445 RVA: 0x000A6B25 File Offset: 0x000A4D25
		// (set) Token: 0x06001D16 RID: 7446 RVA: 0x000A6B2D File Offset: 0x000A4D2D
		public XmlElementEventHandler OnUnknownElement
		{
			get
			{
				return this.onUnknownElement;
			}
			set
			{
				this.onUnknownElement = value;
			}
		}

		/// <summary>Gets or sets an object that represents the method that handles the <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownNode" /> event.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlNodeEventHandler" /> that points to the event handler.</returns>
		// Token: 0x1700059B RID: 1435
		// (get) Token: 0x06001D17 RID: 7447 RVA: 0x000A6B36 File Offset: 0x000A4D36
		// (set) Token: 0x06001D18 RID: 7448 RVA: 0x000A6B3E File Offset: 0x000A4D3E
		public XmlNodeEventHandler OnUnknownNode
		{
			get
			{
				return this.onUnknownNode;
			}
			set
			{
				this.onUnknownNode = value;
			}
		}

		/// <summary>Gets or sets an object that represents the method that handles the <see cref="E:System.Xml.Serialization.XmlSerializer.UnreferencedObject" /> event.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.UnreferencedObjectEventHandler" /> that points to the event handler.</returns>
		// Token: 0x1700059C RID: 1436
		// (get) Token: 0x06001D19 RID: 7449 RVA: 0x000A6B47 File Offset: 0x000A4D47
		// (set) Token: 0x06001D1A RID: 7450 RVA: 0x000A6B4F File Offset: 0x000A4D4F
		public UnreferencedObjectEventHandler OnUnreferencedObject
		{
			get
			{
				return this.onUnreferencedObject;
			}
			set
			{
				this.onUnreferencedObject = value;
			}
		}

		// Token: 0x0400166C RID: 5740
		private XmlAttributeEventHandler onUnknownAttribute;

		// Token: 0x0400166D RID: 5741
		private XmlElementEventHandler onUnknownElement;

		// Token: 0x0400166E RID: 5742
		private XmlNodeEventHandler onUnknownNode;

		// Token: 0x0400166F RID: 5743
		private UnreferencedObjectEventHandler onUnreferencedObject;
	}
}
