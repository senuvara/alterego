﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Xml.Schema;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Generates internal mappings to .NET Framework types for XML schema element declarations, including literal XSD message parts in a WSDL document. </summary>
	// Token: 0x02000322 RID: 802
	public class XmlSchemaImporter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" /> class, taking a collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects representing the XML schemas used by SOAP literal messages defined in a WSDL document. </summary>
		/// <param name="schemas">A collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects.</param>
		// Token: 0x06001DDC RID: 7644 RVA: 0x000AB0E0 File Offset: 0x000A92E0
		public XmlSchemaImporter(XmlSchemas schemas)
		{
			this.elemIdentifiers = new CodeIdentifiers();
			this.mappedTypes = new Hashtable();
			this.primitiveDerivedMappedTypes = new Hashtable();
			this.dataMappedTypes = new Hashtable();
			this.pendingMaps = new Queue();
			this.sharedAnonymousTypes = new Hashtable();
			this.fixup_registered_types = new ArrayList();
			base..ctor();
			this.schemas = schemas;
			this.typeIdentifiers = new CodeIdentifiers();
			this.InitializeExtensions();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" /> class, taking a collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects that represents the XML schemas used by SOAP literal messages, plus classes being generated for bindings defined in a Web Services Description Language (WSDL) document. </summary>
		/// <param name="schemas">An <see cref="T:System.Xml.Serialization.XmlSchemas" /> object.</param>
		/// <param name="typeIdentifiers">A <see cref="T:System.Xml.Serialization.CodeIdentifiers" /> object that specifies a collection of classes being generated for bindings defined in a WSDL document.</param>
		// Token: 0x06001DDD RID: 7645 RVA: 0x000AB158 File Offset: 0x000A9358
		public XmlSchemaImporter(XmlSchemas schemas, CodeIdentifiers typeIdentifiers) : this(schemas)
		{
			this.typeIdentifiers = typeIdentifiers;
		}

		// Token: 0x170005CF RID: 1487
		// (get) Token: 0x06001DDE RID: 7646 RVA: 0x000AB168 File Offset: 0x000A9368
		// (set) Token: 0x06001DDF RID: 7647 RVA: 0x000AB170 File Offset: 0x000A9370
		internal bool UseEncodedFormat
		{
			get
			{
				return this.encodedFormat;
			}
			set
			{
				this.encodedFormat = value;
			}
		}

		// Token: 0x06001DE0 RID: 7648 RVA: 0x000030EC File Offset: 0x000012EC
		private void InitializeExtensions()
		{
		}

		/// <summary>Generates internal type mapping information for a single, (SOAP) literal element part defined in a WSDL document.</summary>
		/// <param name="typeName">An <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the name of an element's type for which a .NET Framework type is generated.</param>
		/// <param name="elementName">The name of the part element in the WSDL document.</param>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlMembersMapping" /> representing the .NET Framework type mapping for a single element part of a WSDL message definition.</returns>
		// Token: 0x06001DE1 RID: 7649 RVA: 0x000AB17C File Offset: 0x000A937C
		public XmlMembersMapping ImportAnyType(XmlQualifiedName typeName, string elementName)
		{
			if (typeName == XmlQualifiedName.Empty)
			{
				XmlTypeMapMemberAnyElement xmlTypeMapMemberAnyElement = new XmlTypeMapMemberAnyElement();
				xmlTypeMapMemberAnyElement.Name = typeName.Name;
				xmlTypeMapMemberAnyElement.TypeData = TypeTranslator.GetTypeData(typeof(XmlNode));
				xmlTypeMapMemberAnyElement.ElementInfo.Add(this.CreateElementInfo(typeName.Namespace, xmlTypeMapMemberAnyElement, typeName.Name, xmlTypeMapMemberAnyElement.TypeData, true, XmlSchemaForm.None, -1));
				return new XmlMembersMapping(new XmlMemberMapping[]
				{
					new XmlMemberMapping(typeName.Name, typeName.Namespace, xmlTypeMapMemberAnyElement, this.encodedFormat)
				});
			}
			XmlSchemaComplexType xmlSchemaComplexType = (XmlSchemaComplexType)this.schemas.Find(typeName, typeof(XmlSchemaComplexType));
			if (xmlSchemaComplexType == null)
			{
				throw new InvalidOperationException("Referenced type '" + typeName + "' not found");
			}
			if (!this.CanBeAnyElement(xmlSchemaComplexType))
			{
				throw new InvalidOperationException("The type '" + typeName + "' is not valid for a collection of any elements");
			}
			ClassMap classMap = new ClassMap();
			CodeIdentifiers classIds = new CodeIdentifiers();
			bool isMixed = xmlSchemaComplexType.IsMixed;
			this.ImportSequenceContent(typeName, classMap, ((XmlSchemaSequence)xmlSchemaComplexType.Particle).Items, classIds, false, ref isMixed);
			XmlTypeMapMemberAnyElement xmlTypeMapMemberAnyElement2 = (XmlTypeMapMemberAnyElement)classMap.AllMembers[0];
			xmlTypeMapMemberAnyElement2.Name = typeName.Name;
			return new XmlMembersMapping(new XmlMemberMapping[]
			{
				new XmlMemberMapping(typeName.Name, typeName.Namespace, xmlTypeMapMemberAnyElement2, this.encodedFormat)
			});
		}

		/// <summary>Generates internal type mapping information for an element defined in an XML schema document. </summary>
		/// <param name="name">An <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the name of an element defined in an XML schema document.</param>
		/// <param name="baseType">A base type for the .NET Framework type that is generated to correspond to an XSD element's type.</param>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlTypeMapping" /> representing the.NET Framework type mapping information for an XML schema element.</returns>
		// Token: 0x06001DE2 RID: 7650 RVA: 0x000AB2D7 File Offset: 0x000A94D7
		public XmlTypeMapping ImportDerivedTypeMapping(XmlQualifiedName name, Type baseType)
		{
			return this.ImportDerivedTypeMapping(name, baseType, true);
		}

		/// <summary>Generates internal type mapping information for an element defined in an XML schema document or as a part in a WSDL document.</summary>
		/// <param name="name">An <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the name of an element defined in an XML schema document.</param>
		/// <param name="baseType">A base type for the .NET Framework type that is generated to correspond to an XSD element's type.</param>
		/// <param name="baseTypeCanBeIndirect">
		///       <see langword="true" /> to indicate that the type corresponding to an XSD element can indirectly inherit from the base type; otherwise, <see langword="false" />.</param>
		/// <returns>The .NET Framework type mapping information for an XML schema element.</returns>
		// Token: 0x06001DE3 RID: 7651 RVA: 0x000AB2E4 File Offset: 0x000A94E4
		public XmlTypeMapping ImportDerivedTypeMapping(XmlQualifiedName name, Type baseType, bool baseTypeCanBeIndirect)
		{
			XmlQualifiedName typeQName;
			XmlSchemaType xmlSchemaType;
			if (this.encodedFormat)
			{
				typeQName = name;
				xmlSchemaType = (this.schemas.Find(name, typeof(XmlSchemaComplexType)) as XmlSchemaComplexType);
				if (xmlSchemaType == null)
				{
					throw new InvalidOperationException("Schema type '" + name + "' not found or not valid");
				}
			}
			else if (!this.LocateElement(name, out typeQName, out xmlSchemaType))
			{
				return null;
			}
			XmlTypeMapping xmlTypeMapping = this.GetRegisteredTypeMapping(typeQName, baseType);
			if (xmlTypeMapping != null)
			{
				this.SetMapBaseType(xmlTypeMapping, baseType);
				xmlTypeMapping.UpdateRoot(name);
				return xmlTypeMapping;
			}
			xmlTypeMapping = this.CreateTypeMapping(typeQName, SchemaTypes.Class, name);
			if (xmlSchemaType != null)
			{
				xmlTypeMapping.Documentation = this.GetDocumentation(xmlSchemaType);
				this.RegisterMapFixup(xmlTypeMapping, typeQName, (XmlSchemaComplexType)xmlSchemaType);
			}
			else
			{
				ClassMap classMap = new ClassMap();
				CodeIdentifiers classIds = new CodeIdentifiers();
				xmlTypeMapping.ObjectMap = classMap;
				this.AddTextMember(typeQName, classMap, classIds);
			}
			this.BuildPendingMaps();
			this.SetMapBaseType(xmlTypeMapping, baseType);
			return xmlTypeMapping;
		}

		// Token: 0x06001DE4 RID: 7652 RVA: 0x000AB3B4 File Offset: 0x000A95B4
		private void SetMapBaseType(XmlTypeMapping map, Type baseType)
		{
			XmlTypeMapping xmlTypeMapping = null;
			while (map != null)
			{
				if (map.TypeData.Type == baseType)
				{
					return;
				}
				xmlTypeMapping = map;
				map = map.BaseMap;
			}
			XmlTypeMapping xmlTypeMapping2 = this.ReflectType(baseType);
			xmlTypeMapping.BaseMap = xmlTypeMapping2;
			xmlTypeMapping2.DerivedTypes.Add(xmlTypeMapping);
			xmlTypeMapping2.DerivedTypes.AddRange(xmlTypeMapping.DerivedTypes);
			ClassMap classMap = (ClassMap)xmlTypeMapping2.ObjectMap;
			ClassMap classMap2 = (ClassMap)xmlTypeMapping.ObjectMap;
			foreach (object obj in classMap.AllMembers)
			{
				XmlTypeMapMember member = (XmlTypeMapMember)obj;
				classMap2.AddMember(member);
			}
			foreach (object obj2 in xmlTypeMapping.DerivedTypes)
			{
				classMap2 = (ClassMap)((XmlTypeMapping)obj2).ObjectMap;
				foreach (object obj3 in classMap.AllMembers)
				{
					XmlTypeMapMember member2 = (XmlTypeMapMember)obj3;
					classMap2.AddMember(member2);
				}
			}
		}

		/// <summary>Generates internal type mapping information for a single element part of a literal-use SOAP message defined in a WSDL document. </summary>
		/// <param name="name">An <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the name of the message part.</param>
		/// <returns>The .NET Framework type mapping for a WSDL message definition containing a single element part.</returns>
		// Token: 0x06001DE5 RID: 7653 RVA: 0x000AB524 File Offset: 0x000A9724
		public XmlMembersMapping ImportMembersMapping(XmlQualifiedName name)
		{
			XmlSchemaElement xmlSchemaElement = (XmlSchemaElement)this.schemas.Find(name, typeof(XmlSchemaElement));
			if (xmlSchemaElement == null)
			{
				throw new InvalidOperationException("Schema element '" + name + "' not found or not valid");
			}
			XmlSchemaComplexType xmlSchemaComplexType;
			if (xmlSchemaElement.SchemaType != null)
			{
				xmlSchemaComplexType = (xmlSchemaElement.SchemaType as XmlSchemaComplexType);
			}
			else
			{
				if (xmlSchemaElement.SchemaTypeName.IsEmpty)
				{
					return null;
				}
				object obj = this.schemas.Find(xmlSchemaElement.SchemaTypeName, typeof(XmlSchemaComplexType));
				if (obj == null)
				{
					if (this.IsPrimitiveTypeNamespace(xmlSchemaElement.SchemaTypeName.Namespace))
					{
						return null;
					}
					throw new InvalidOperationException("Schema type '" + xmlSchemaElement.SchemaTypeName + "' not found");
				}
				else
				{
					xmlSchemaComplexType = (obj as XmlSchemaComplexType);
				}
			}
			if (xmlSchemaComplexType == null)
			{
				throw new InvalidOperationException("Schema element '" + name + "' not found or not valid");
			}
			XmlMemberMapping[] mapping = this.ImportMembersMappingComposite(xmlSchemaComplexType, name);
			return new XmlMembersMapping(name.Name, name.Namespace, mapping);
		}

		/// <summary>Generates internal type mapping information for the element parts of a literal-use SOAP message defined in a WSDL document. </summary>
		/// <param name="names">An array of type <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the names of the message parts.</param>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlMembersMapping" /> that represents the .NET Framework type mappings for the element parts of a WSDL message definition.</returns>
		// Token: 0x06001DE6 RID: 7654 RVA: 0x000AB618 File Offset: 0x000A9818
		public XmlMembersMapping ImportMembersMapping(XmlQualifiedName[] names)
		{
			XmlMemberMapping[] array = new XmlMemberMapping[names.Length];
			for (int i = 0; i < names.Length; i++)
			{
				XmlSchemaElement xmlSchemaElement = (XmlSchemaElement)this.schemas.Find(names[i], typeof(XmlSchemaElement));
				if (xmlSchemaElement == null)
				{
					throw new InvalidOperationException("Schema element '" + names[i] + "' not found");
				}
				XmlQualifiedName xmlQualifiedName = new XmlQualifiedName("Message", names[i].Namespace);
				XmlTypeMapping emap;
				TypeData elementTypeData = this.GetElementTypeData(xmlQualifiedName, xmlSchemaElement, names[i], out emap);
				array[i] = this.ImportMemberMapping(xmlSchemaElement.Name, xmlQualifiedName.Namespace, xmlSchemaElement.IsNillable, elementTypeData, emap, i);
			}
			this.BuildPendingMaps();
			return new XmlMembersMapping(array);
		}

		/// <summary>Generates internal type mapping information for the element parts of a literal-use SOAP message defined in a WSDL document.</summary>
		/// <param name="name">The name of the element for which to generate a mapping.</param>
		/// <param name="ns">The namespace of the element for which to generate a mapping.</param>
		/// <param name="members">An array of <see cref="T:System.Xml.Serialization.SoapSchemaMember" /> instances that specifies the members of the element for which to generate a mapping.</param>
		/// <returns>A <see cref="T:System.Xml.Serialization.XmlMembersMapping" /> that contains type mapping information.</returns>
		// Token: 0x06001DE7 RID: 7655 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		public XmlMembersMapping ImportMembersMapping(string name, string ns, SoapSchemaMember[] members)
		{
			throw new NotImplementedException();
		}

		/// <summary>Generates internal type mapping information for an element defined in an XML schema document. </summary>
		/// <param name="typeName">A <see cref="T:System.Xml.XmlQualifiedName" /> that specifies an XML element.</param>
		/// <returns>A <see cref="T:System.Xml.Serialization.XmlTypeMapping" /> object that describes a type mapping.</returns>
		// Token: 0x06001DE8 RID: 7656 RVA: 0x000AB6C6 File Offset: 0x000A98C6
		public XmlTypeMapping ImportSchemaType(XmlQualifiedName typeName)
		{
			return this.ImportSchemaType(typeName, typeof(object));
		}

		/// <summary>Generates internal type mapping information for an element defined in an XML schema document. </summary>
		/// <param name="typeName">A <see cref="T:System.Xml.XmlQualifiedName" /> that specifies an XML element.</param>
		/// <param name="baseType">A <see cref="T:System.Type" /> object that specifies a base type.</param>
		/// <returns>A <see cref="T:System.Xml.Serialization.XmlTypeMapping" /> object that describes a type mapping.</returns>
		// Token: 0x06001DE9 RID: 7657 RVA: 0x000AB6D9 File Offset: 0x000A98D9
		public XmlTypeMapping ImportSchemaType(XmlQualifiedName typeName, Type baseType)
		{
			return this.ImportSchemaType(typeName, typeof(object), false);
		}

		/// <summary>Generates internal type mapping information for an element defined in an XML schema document. </summary>
		/// <param name="typeName">A <see cref="T:System.Xml.XmlQualifiedName" /> that specifies an XML element.</param>
		/// <param name="baseType">A <see cref="T:System.Type" /> object that specifies a base type.</param>
		/// <param name="baseTypeCanBeIndirect">A <see cref="T:System.Boolean" /> value that specifies whether the generated type can indirectly inherit the <paramref name="baseType" />.</param>
		/// <returns>A <see cref="T:System.Xml.Serialization.XmlTypeMapping" /> object that describes a type mapping.</returns>
		// Token: 0x06001DEA RID: 7658 RVA: 0x000AB6F0 File Offset: 0x000A98F0
		[MonoTODO("baseType and baseTypeCanBeIndirect are ignored")]
		public XmlTypeMapping ImportSchemaType(XmlQualifiedName typeName, Type baseType, bool baseTypeCanBeIndirect)
		{
			XmlSchemaType stype = ((XmlSchemaType)this.schemas.Find(typeName, typeof(XmlSchemaComplexType))) ?? ((XmlSchemaType)this.schemas.Find(typeName, typeof(XmlSchemaSimpleType)));
			return this.ImportTypeCommon(typeName, typeName, stype, true);
		}

		// Token: 0x06001DEB RID: 7659 RVA: 0x000AB744 File Offset: 0x000A9944
		internal XmlMembersMapping ImportEncodedMembersMapping(string name, string ns, SoapSchemaMember[] members, bool hasWrapperElement)
		{
			XmlMemberMapping[] array = new XmlMemberMapping[members.Length];
			for (int i = 0; i < members.Length; i++)
			{
				TypeData typeData = this.GetTypeData(members[i].MemberType, null, false);
				XmlTypeMapping typeMapping = this.GetTypeMapping(typeData);
				array[i] = this.ImportMemberMapping(members[i].MemberName, members[i].MemberType.Namespace, true, typeData, typeMapping, i);
			}
			this.BuildPendingMaps();
			return new XmlMembersMapping(name, ns, hasWrapperElement, false, array);
		}

		// Token: 0x06001DEC RID: 7660 RVA: 0x000AB7B4 File Offset: 0x000A99B4
		internal XmlMembersMapping ImportEncodedMembersMapping(string name, string ns, SoapSchemaMember member)
		{
			XmlSchemaComplexType xmlSchemaComplexType = this.schemas.Find(member.MemberType, typeof(XmlSchemaComplexType)) as XmlSchemaComplexType;
			if (xmlSchemaComplexType == null)
			{
				throw new InvalidOperationException("Schema type '" + member.MemberType + "' not found or not valid");
			}
			XmlMemberMapping[] mapping = this.ImportMembersMappingComposite(xmlSchemaComplexType, member.MemberType);
			return new XmlMembersMapping(name, ns, mapping);
		}

		// Token: 0x06001DED RID: 7661 RVA: 0x000AB818 File Offset: 0x000A9A18
		private XmlMemberMapping[] ImportMembersMappingComposite(XmlSchemaComplexType stype, XmlQualifiedName refer)
		{
			if (stype.Particle == null)
			{
				return new XmlMemberMapping[0];
			}
			ClassMap classMap = new ClassMap();
			XmlSchemaSequence xmlSchemaSequence = stype.Particle as XmlSchemaSequence;
			if (xmlSchemaSequence == null)
			{
				throw new InvalidOperationException("Schema element '" + refer + "' cannot be imported as XmlMembersMapping");
			}
			CodeIdentifiers classIds = new CodeIdentifiers();
			this.ImportParticleComplexContent(refer, classMap, xmlSchemaSequence, classIds, false);
			this.ImportAttributes(refer, classMap, stype.Attributes, stype.AnyAttribute, classIds);
			this.BuildPendingMaps();
			int num = 0;
			XmlMemberMapping[] array = new XmlMemberMapping[classMap.AllMembers.Count];
			foreach (object obj in classMap.AllMembers)
			{
				XmlTypeMapMember xmlTypeMapMember = (XmlTypeMapMember)obj;
				array[num++] = new XmlMemberMapping(xmlTypeMapMember.Name, refer.Namespace, xmlTypeMapMember, this.encodedFormat);
			}
			return array;
		}

		// Token: 0x06001DEE RID: 7662 RVA: 0x000AB910 File Offset: 0x000A9B10
		private XmlMemberMapping ImportMemberMapping(string name, string ns, bool isNullable, TypeData type, XmlTypeMapping emap, int order)
		{
			XmlTypeMapMemberElement xmlTypeMapMemberElement;
			if (type.IsListType)
			{
				xmlTypeMapMemberElement = new XmlTypeMapMemberList();
			}
			else
			{
				xmlTypeMapMemberElement = new XmlTypeMapMemberElement();
			}
			xmlTypeMapMemberElement.Name = name;
			xmlTypeMapMemberElement.TypeData = type;
			xmlTypeMapMemberElement.ElementInfo.Add(this.CreateElementInfo(ns, xmlTypeMapMemberElement, name, type, isNullable, XmlSchemaForm.None, emap, order));
			return new XmlMemberMapping(name, ns, xmlTypeMapMemberElement, this.encodedFormat);
		}

		/// <summary>Generates internal type mapping information for the element parts of a literal-use SOAP message defined in a WSDL document.</summary>
		/// <param name="names">An array of type <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the names of the message parts.</param>
		/// <param name="baseType">A base type for all .NET Framework types that are generated to correspond to message parts.</param>
		/// <param name="baseTypeCanBeIndirect">
		///       <see langword="true" /> to indicate that the types corresponding to message parts can indirectly inherit from the base type; otherwise, <see langword="false" />.</param>
		/// <returns>The .NET Framework type mappings for the element parts of a WSDL message definition.</returns>
		// Token: 0x06001DEF RID: 7663 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		public XmlMembersMapping ImportMembersMapping(XmlQualifiedName[] names, Type baseType, bool baseTypeCanBeIndirect)
		{
			throw new NotImplementedException();
		}

		/// <summary>Generates internal type mapping information for an element defined in an XML schema document. </summary>
		/// <param name="name">An <see cref="T:System.Xml.XmlQualifiedName" /> that specifies the name of an element defined in an XML schema document.</param>
		/// <returns>The .NET Framework type mapping information for an XML schema element.</returns>
		// Token: 0x06001DF0 RID: 7664 RVA: 0x000AB970 File Offset: 0x000A9B70
		public XmlTypeMapping ImportTypeMapping(XmlQualifiedName name)
		{
			if (!this.schemas.IsCompiled)
			{
				this.schemas.Compile(null, true);
			}
			XmlSchemaElement xmlSchemaElement = (XmlSchemaElement)this.schemas.Find(name, typeof(XmlSchemaElement));
			XmlQualifiedName qname;
			XmlSchemaType stype;
			if (!this.LocateElement(xmlSchemaElement, out qname, out stype))
			{
				throw new InvalidOperationException(string.Format("'{0}' is missing.", name));
			}
			return this.ImportTypeCommon(name, qname, stype, xmlSchemaElement.IsNillable);
		}

		// Token: 0x06001DF1 RID: 7665 RVA: 0x000AB9E0 File Offset: 0x000A9BE0
		private XmlTypeMapping ImportTypeCommon(XmlQualifiedName name, XmlQualifiedName qname, XmlSchemaType stype, bool isNullable)
		{
			if (stype == null)
			{
				if (qname == XmlSchemaImporter.anyType)
				{
					XmlTypeMapping typeMapping = this.GetTypeMapping(TypeTranslator.GetTypeData(typeof(object)));
					this.BuildPendingMaps();
					return typeMapping;
				}
				TypeData primitiveTypeData = TypeTranslator.GetPrimitiveTypeData(qname.Name);
				return this.ReflectType(primitiveTypeData, name.Namespace);
			}
			else
			{
				XmlTypeMapping xmlTypeMapping = this.GetRegisteredTypeMapping(qname);
				if (xmlTypeMapping != null)
				{
					return xmlTypeMapping;
				}
				if (stype is XmlSchemaSimpleType)
				{
					return this.ImportClassSimpleType(stype.QualifiedName, (XmlSchemaSimpleType)stype, name);
				}
				xmlTypeMapping = this.CreateTypeMapping(qname, SchemaTypes.Class, name);
				xmlTypeMapping.Documentation = this.GetDocumentation(stype);
				xmlTypeMapping.IsNullable = isNullable;
				this.RegisterMapFixup(xmlTypeMapping, qname, (XmlSchemaComplexType)stype);
				this.BuildPendingMaps();
				return xmlTypeMapping;
			}
		}

		// Token: 0x06001DF2 RID: 7666 RVA: 0x000ABA90 File Offset: 0x000A9C90
		private bool LocateElement(XmlQualifiedName name, out XmlQualifiedName qname, out XmlSchemaType stype)
		{
			XmlSchemaElement elem = (XmlSchemaElement)this.schemas.Find(name, typeof(XmlSchemaElement));
			return this.LocateElement(elem, out qname, out stype);
		}

		// Token: 0x06001DF3 RID: 7667 RVA: 0x000ABAC4 File Offset: 0x000A9CC4
		private bool LocateElement(XmlSchemaElement elem, out XmlQualifiedName qname, out XmlSchemaType stype)
		{
			qname = null;
			stype = null;
			if (elem == null)
			{
				return false;
			}
			if (elem.SchemaType != null)
			{
				stype = elem.SchemaType;
				qname = elem.QualifiedName;
			}
			else
			{
				if (elem.ElementSchemaType == XmlSchemaComplexType.AnyType)
				{
					qname = XmlSchemaImporter.anyType;
					return true;
				}
				if (elem.SchemaTypeName.IsEmpty)
				{
					return false;
				}
				object obj = this.schemas.Find(elem.SchemaTypeName, typeof(XmlSchemaComplexType));
				if (obj == null)
				{
					obj = this.schemas.Find(elem.SchemaTypeName, typeof(XmlSchemaSimpleType));
				}
				if (obj == null)
				{
					if (this.IsPrimitiveTypeNamespace(elem.SchemaTypeName.Namespace))
					{
						qname = elem.SchemaTypeName;
						return true;
					}
					throw new InvalidOperationException("Schema type '" + elem.SchemaTypeName + "' not found");
				}
				else
				{
					stype = (XmlSchemaType)obj;
					qname = stype.QualifiedName;
					XmlSchemaType xmlSchemaType = stype.BaseSchemaType as XmlSchemaType;
					if (xmlSchemaType != null && xmlSchemaType.QualifiedName == elem.SchemaTypeName)
					{
						throw new InvalidOperationException(string.Concat(new string[]
						{
							"Cannot import schema for type '",
							elem.SchemaTypeName.Name,
							"' from namespace '",
							elem.SchemaTypeName.Namespace,
							"'. Redefine not supported"
						}));
					}
				}
			}
			return true;
		}

		// Token: 0x06001DF4 RID: 7668 RVA: 0x000ABC10 File Offset: 0x000A9E10
		private XmlTypeMapping ImportType(XmlQualifiedName name, XmlQualifiedName root, bool throwOnError)
		{
			XmlTypeMapping registeredTypeMapping = this.GetRegisteredTypeMapping(name);
			if (registeredTypeMapping != null)
			{
				registeredTypeMapping.UpdateRoot(root);
				return registeredTypeMapping;
			}
			XmlSchemaType xmlSchemaType = (XmlSchemaType)this.schemas.Find(name, typeof(XmlSchemaComplexType));
			if (xmlSchemaType == null)
			{
				xmlSchemaType = (XmlSchemaType)this.schemas.Find(name, typeof(XmlSchemaSimpleType));
			}
			if (xmlSchemaType != null)
			{
				return this.ImportType(name, xmlSchemaType, root);
			}
			if (!throwOnError)
			{
				return null;
			}
			if (name.Namespace == "http://schemas.xmlsoap.org/soap/encoding/")
			{
				throw new InvalidOperationException("Referenced type '" + name + "' valid only for encoded SOAP.");
			}
			throw new InvalidOperationException("Referenced type '" + name + "' not found.");
		}

		// Token: 0x06001DF5 RID: 7669 RVA: 0x000ABCBC File Offset: 0x000A9EBC
		private XmlTypeMapping ImportClass(XmlQualifiedName name)
		{
			XmlTypeMapping xmlTypeMapping = this.ImportType(name, null, true);
			if (xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Class)
			{
				return xmlTypeMapping;
			}
			XmlSchemaComplexType stype = this.schemas.Find(name, typeof(XmlSchemaComplexType)) as XmlSchemaComplexType;
			return this.CreateClassMap(name, stype, new XmlQualifiedName(xmlTypeMapping.ElementName, xmlTypeMapping.Namespace));
		}

		// Token: 0x06001DF6 RID: 7670 RVA: 0x000ABD18 File Offset: 0x000A9F18
		private XmlTypeMapping ImportType(XmlQualifiedName name, XmlSchemaType stype, XmlQualifiedName root)
		{
			XmlTypeMapping registeredTypeMapping = this.GetRegisteredTypeMapping(name);
			if (registeredTypeMapping != null)
			{
				XmlSchemaComplexType xmlSchemaComplexType = stype as XmlSchemaComplexType;
				if (registeredTypeMapping.TypeData.SchemaType != SchemaTypes.Class || xmlSchemaComplexType == null || !this.CanBeArray(name, xmlSchemaComplexType))
				{
					registeredTypeMapping.UpdateRoot(root);
					return registeredTypeMapping;
				}
			}
			if (stype is XmlSchemaComplexType)
			{
				return this.ImportClassComplexType(name, (XmlSchemaComplexType)stype, root);
			}
			if (stype is XmlSchemaSimpleType)
			{
				return this.ImportClassSimpleType(name, (XmlSchemaSimpleType)stype, root);
			}
			throw new NotSupportedException("Schema type not supported: " + stype.GetType());
		}

		// Token: 0x06001DF7 RID: 7671 RVA: 0x000ABDA0 File Offset: 0x000A9FA0
		private XmlTypeMapping ImportClassComplexType(XmlQualifiedName typeQName, XmlSchemaComplexType stype, XmlQualifiedName root)
		{
			if (this.CanBeArray(typeQName, stype))
			{
				TypeData arrayTypeData;
				ListMap listMap = this.BuildArrayMap(typeQName, stype, out arrayTypeData);
				if (listMap != null)
				{
					XmlTypeMapping xmlTypeMapping = this.CreateArrayTypeMapping(typeQName, arrayTypeData);
					xmlTypeMapping.ObjectMap = listMap;
					return xmlTypeMapping;
				}
			}
			else if (this.CanBeIXmlSerializable(stype))
			{
				return this.ImportXmlSerializableMapping(typeQName.Namespace);
			}
			return this.CreateClassMap(typeQName, stype, root);
		}

		// Token: 0x06001DF8 RID: 7672 RVA: 0x000ABDF4 File Offset: 0x000A9FF4
		private XmlTypeMapping CreateClassMap(XmlQualifiedName typeQName, XmlSchemaComplexType stype, XmlQualifiedName root)
		{
			XmlTypeMapping xmlTypeMapping = this.CreateTypeMapping(typeQName, SchemaTypes.Class, root);
			xmlTypeMapping.Documentation = this.GetDocumentation(stype);
			this.RegisterMapFixup(xmlTypeMapping, typeQName, stype);
			return xmlTypeMapping;
		}

		// Token: 0x06001DF9 RID: 7673 RVA: 0x000ABE24 File Offset: 0x000AA024
		private void RegisterMapFixup(XmlTypeMapping map, XmlQualifiedName typeQName, XmlSchemaComplexType stype)
		{
			if (this.fixup_registered_types.Contains(stype))
			{
				throw new InvalidOperationException(string.Format("Circular dependency for schema type {0} in namespace {1}", map.ElementName, map.Namespace));
			}
			this.fixup_registered_types.Add(stype);
			XmlSchemaImporter.MapFixup mapFixup = new XmlSchemaImporter.MapFixup();
			mapFixup.Map = map;
			mapFixup.SchemaType = stype;
			mapFixup.TypeName = typeQName;
			this.pendingMaps.Enqueue(mapFixup);
		}

		// Token: 0x06001DFA RID: 7674 RVA: 0x000ABE90 File Offset: 0x000AA090
		private void BuildPendingMaps()
		{
			while (this.pendingMaps.Count > 0)
			{
				XmlSchemaImporter.MapFixup mapFixup = (XmlSchemaImporter.MapFixup)this.pendingMaps.Dequeue();
				if (mapFixup.Map.ObjectMap == null)
				{
					this.BuildClassMap(mapFixup.Map, mapFixup.TypeName, mapFixup.SchemaType);
					if (mapFixup.Map.ObjectMap == null)
					{
						this.pendingMaps.Enqueue(mapFixup);
					}
				}
			}
		}

		// Token: 0x06001DFB RID: 7675 RVA: 0x000ABEFC File Offset: 0x000AA0FC
		private void BuildPendingMap(XmlTypeMapping map)
		{
			if (map.ObjectMap != null)
			{
				return;
			}
			foreach (object obj in this.pendingMaps)
			{
				XmlSchemaImporter.MapFixup mapFixup = (XmlSchemaImporter.MapFixup)obj;
				if (mapFixup.Map == map)
				{
					this.BuildClassMap(mapFixup.Map, mapFixup.TypeName, mapFixup.SchemaType);
					return;
				}
			}
			throw new InvalidOperationException("Can't complete map of type " + map.XmlType + " : " + map.Namespace);
		}

		// Token: 0x06001DFC RID: 7676 RVA: 0x000ABF9C File Offset: 0x000AA19C
		private void BuildClassMap(XmlTypeMapping map, XmlQualifiedName typeQName, XmlSchemaComplexType stype)
		{
			CodeIdentifiers codeIdentifiers = new CodeIdentifiers();
			codeIdentifiers.AddReserved(map.TypeData.TypeName);
			ClassMap classMap = new ClassMap();
			map.ObjectMap = classMap;
			bool isMixed = stype.IsMixed;
			if (stype.Particle != null)
			{
				this.ImportParticleComplexContent(typeQName, classMap, stype.Particle, codeIdentifiers, isMixed);
			}
			else if (stype.ContentModel is XmlSchemaSimpleContent)
			{
				this.ImportSimpleContent(typeQName, map, (XmlSchemaSimpleContent)stype.ContentModel, codeIdentifiers, isMixed);
			}
			else if (stype.ContentModel is XmlSchemaComplexContent)
			{
				this.ImportComplexContent(typeQName, map, (XmlSchemaComplexContent)stype.ContentModel, codeIdentifiers, isMixed);
			}
			this.ImportAttributes(typeQName, classMap, stype.Attributes, stype.AnyAttribute, codeIdentifiers);
			this.ImportExtensionTypes(typeQName);
			if (isMixed)
			{
				this.AddTextMember(typeQName, classMap, codeIdentifiers);
			}
			this.AddObjectDerivedMap(map);
		}

		// Token: 0x06001DFD RID: 7677 RVA: 0x000AC064 File Offset: 0x000AA264
		private void ImportAttributes(XmlQualifiedName typeQName, ClassMap cmap, XmlSchemaObjectCollection atts, XmlSchemaAnyAttribute anyat, CodeIdentifiers classIds)
		{
			atts = this.CollectAttributeUsesNonOverlap(atts, cmap);
			if (anyat != null)
			{
				XmlTypeMapMemberAnyAttribute xmlTypeMapMemberAnyAttribute = new XmlTypeMapMemberAnyAttribute();
				xmlTypeMapMemberAnyAttribute.Name = classIds.AddUnique("AnyAttribute", xmlTypeMapMemberAnyAttribute);
				xmlTypeMapMemberAnyAttribute.TypeData = TypeTranslator.GetTypeData(typeof(XmlAttribute[]));
				cmap.AddMember(xmlTypeMapMemberAnyAttribute);
			}
			foreach (XmlSchemaObject xmlSchemaObject in atts)
			{
				if (xmlSchemaObject is XmlSchemaAttribute)
				{
					XmlSchemaAttribute xmlSchemaAttribute = (XmlSchemaAttribute)xmlSchemaObject;
					string @namespace;
					XmlSchemaAttribute refAttribute = this.GetRefAttribute(typeQName, xmlSchemaAttribute, out @namespace);
					XmlTypeMapMemberAttribute xmlTypeMapMemberAttribute = new XmlTypeMapMemberAttribute();
					xmlTypeMapMemberAttribute.Name = classIds.AddUnique(CodeIdentifier.MakeValid(refAttribute.Name), xmlTypeMapMemberAttribute);
					xmlTypeMapMemberAttribute.Documentation = this.GetDocumentation(xmlSchemaAttribute);
					xmlTypeMapMemberAttribute.AttributeName = refAttribute.Name;
					xmlTypeMapMemberAttribute.Namespace = @namespace;
					xmlTypeMapMemberAttribute.Form = refAttribute.Form;
					xmlTypeMapMemberAttribute.TypeData = this.GetAttributeTypeData(typeQName, xmlSchemaAttribute);
					if (refAttribute.DefaultValue != null)
					{
						xmlTypeMapMemberAttribute.DefaultValue = this.ImportDefaultValue(xmlTypeMapMemberAttribute.TypeData, refAttribute.DefaultValue);
					}
					else if (xmlTypeMapMemberAttribute.TypeData.IsValueType)
					{
						xmlTypeMapMemberAttribute.IsOptionalValueType = (refAttribute.Use != XmlSchemaUse.Required);
					}
					if (xmlTypeMapMemberAttribute.TypeData.IsComplexType)
					{
						xmlTypeMapMemberAttribute.MappedType = this.GetTypeMapping(xmlTypeMapMemberAttribute.TypeData);
					}
					cmap.AddMember(xmlTypeMapMemberAttribute);
				}
				else if (xmlSchemaObject is XmlSchemaAttributeGroupRef)
				{
					XmlSchemaAttributeGroupRef xmlSchemaAttributeGroupRef = (XmlSchemaAttributeGroupRef)xmlSchemaObject;
					XmlSchemaAttributeGroup xmlSchemaAttributeGroup = this.FindRefAttributeGroup(xmlSchemaAttributeGroupRef.RefName);
					this.ImportAttributes(typeQName, cmap, xmlSchemaAttributeGroup.Attributes, xmlSchemaAttributeGroup.AnyAttribute, classIds);
				}
			}
		}

		// Token: 0x06001DFE RID: 7678 RVA: 0x000AC238 File Offset: 0x000AA438
		private IEnumerable<XmlSchemaAttribute> EnumerateAttributes(XmlSchemaObjectCollection col, List<XmlSchemaAttributeGroup> recurse)
		{
			foreach (XmlSchemaObject o in col)
			{
				if (o is XmlSchemaAttributeGroupRef)
				{
					XmlSchemaAttributeGroupRef xmlSchemaAttributeGroupRef = (XmlSchemaAttributeGroupRef)o;
					XmlSchemaAttributeGroup xmlSchemaAttributeGroup = this.FindRefAttributeGroup(xmlSchemaAttributeGroupRef.RefName);
					if (recurse.Contains(xmlSchemaAttributeGroup))
					{
						continue;
					}
					recurse.Add(xmlSchemaAttributeGroup);
					if (xmlSchemaAttributeGroup == null)
					{
						throw new InvalidOperationException(string.Format("Referenced AttributeGroup '{0}' was not found.", xmlSchemaAttributeGroupRef.RefName));
					}
					foreach (XmlSchemaAttribute xmlSchemaAttribute in this.EnumerateAttributes(xmlSchemaAttributeGroup.Attributes, recurse))
					{
						yield return xmlSchemaAttribute;
					}
					IEnumerator<XmlSchemaAttribute> enumerator = null;
				}
				else
				{
					yield return (XmlSchemaAttribute)o;
				}
				o = null;
			}
			XmlSchemaObjectEnumerator xmlSchemaObjectEnumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x06001DFF RID: 7679 RVA: 0x000AC258 File Offset: 0x000AA458
		private XmlSchemaObjectCollection CollectAttributeUsesNonOverlap(XmlSchemaObjectCollection src, ClassMap map)
		{
			XmlSchemaObjectCollection xmlSchemaObjectCollection = new XmlSchemaObjectCollection();
			foreach (XmlSchemaAttribute xmlSchemaAttribute in this.EnumerateAttributes(src, new List<XmlSchemaAttributeGroup>()))
			{
				if (map.GetAttribute(xmlSchemaAttribute.QualifiedName.Name, xmlSchemaAttribute.QualifiedName.Namespace) == null)
				{
					xmlSchemaObjectCollection.Add(xmlSchemaAttribute);
				}
			}
			return xmlSchemaObjectCollection;
		}

		// Token: 0x06001E00 RID: 7680 RVA: 0x000AC2D4 File Offset: 0x000AA4D4
		private ListMap BuildArrayMap(XmlQualifiedName typeQName, XmlSchemaComplexType stype, out TypeData arrayTypeData)
		{
			if (this.encodedFormat)
			{
				XmlSchemaComplexContentRestriction xmlSchemaComplexContentRestriction = (stype.ContentModel as XmlSchemaComplexContent).Content as XmlSchemaComplexContentRestriction;
				XmlSchemaAttribute xmlSchemaAttribute = this.FindArrayAttribute(xmlSchemaComplexContentRestriction.Attributes);
				if (xmlSchemaAttribute != null)
				{
					XmlAttribute[] unhandledAttributes = xmlSchemaAttribute.UnhandledAttributes;
					if (unhandledAttributes == null || unhandledAttributes.Length == 0)
					{
						throw new InvalidOperationException("arrayType attribute not specified in array declaration: " + typeQName);
					}
					XmlAttribute xmlAttribute = null;
					foreach (XmlAttribute xmlAttribute2 in unhandledAttributes)
					{
						if (xmlAttribute2.LocalName == "arrayType" && xmlAttribute2.NamespaceURI == "http://schemas.xmlsoap.org/wsdl/")
						{
							xmlAttribute = xmlAttribute2;
							break;
						}
					}
					if (xmlAttribute == null)
					{
						throw new InvalidOperationException("arrayType attribute not specified in array declaration: " + typeQName);
					}
					string str;
					string ns;
					string str2;
					TypeTranslator.ParseArrayType(xmlAttribute.Value, out str, out ns, out str2);
					return this.BuildEncodedArrayMap(str + str2, ns, out arrayTypeData);
				}
				else
				{
					XmlSchemaElement xmlSchemaElement = null;
					XmlSchemaSequence xmlSchemaSequence = xmlSchemaComplexContentRestriction.Particle as XmlSchemaSequence;
					if (xmlSchemaSequence != null && xmlSchemaSequence.Items.Count == 1)
					{
						xmlSchemaElement = (xmlSchemaSequence.Items[0] as XmlSchemaElement);
					}
					else
					{
						XmlSchemaAll xmlSchemaAll = xmlSchemaComplexContentRestriction.Particle as XmlSchemaAll;
						if (xmlSchemaAll != null && xmlSchemaAll.Items.Count == 1)
						{
							xmlSchemaElement = (xmlSchemaAll.Items[0] as XmlSchemaElement);
						}
					}
					if (xmlSchemaElement == null)
					{
						throw new InvalidOperationException("Unknown array format");
					}
					return this.BuildEncodedArrayMap(xmlSchemaElement.SchemaTypeName.Name + "[]", xmlSchemaElement.SchemaTypeName.Namespace, out arrayTypeData);
				}
			}
			else
			{
				ClassMap classMap = new ClassMap();
				CodeIdentifiers classIds = new CodeIdentifiers();
				this.ImportParticleComplexContent(typeQName, classMap, stype.Particle, classIds, stype.IsMixed);
				XmlTypeMapMemberFlatList xmlTypeMapMemberFlatList = (classMap.AllMembers.Count == 1) ? (classMap.AllMembers[0] as XmlTypeMapMemberFlatList) : null;
				if (xmlTypeMapMemberFlatList != null && xmlTypeMapMemberFlatList.ChoiceMember == null)
				{
					arrayTypeData = xmlTypeMapMemberFlatList.TypeData;
					return xmlTypeMapMemberFlatList.ListMap;
				}
				arrayTypeData = null;
				return null;
			}
		}

		// Token: 0x06001E01 RID: 7681 RVA: 0x000AC4CC File Offset: 0x000AA6CC
		private ListMap BuildEncodedArrayMap(string type, string ns, out TypeData arrayTypeData)
		{
			ListMap listMap = new ListMap();
			int num = type.LastIndexOf("[");
			if (num == -1)
			{
				throw new InvalidOperationException("Invalid arrayType value: " + type);
			}
			if (type.IndexOf(",", num) != -1)
			{
				throw new InvalidOperationException("Multidimensional arrays are not supported");
			}
			string text = type.Substring(0, num);
			TypeData typeData;
			if (text.IndexOf("[") != -1)
			{
				ListMap objectMap = this.BuildEncodedArrayMap(text, ns, out typeData);
				int dimensions = text.Split(new char[]
				{
					'['
				}).Length - 1;
				XmlQualifiedName typeQName = new XmlQualifiedName(TypeTranslator.GetArrayName(type, dimensions), ns);
				this.CreateArrayTypeMapping(typeQName, typeData).ObjectMap = objectMap;
			}
			else
			{
				typeData = this.GetTypeData(new XmlQualifiedName(text, ns), null, false);
			}
			arrayTypeData = typeData.ListTypeData;
			listMap.ItemInfo = new XmlTypeMapElementInfoList();
			listMap.ItemInfo.Add(this.CreateElementInfo("", null, "Item", typeData, true, XmlSchemaForm.None, -1));
			return listMap;
		}

		// Token: 0x06001E02 RID: 7682 RVA: 0x000AC5BC File Offset: 0x000AA7BC
		private XmlSchemaAttribute FindArrayAttribute(XmlSchemaObjectCollection atts)
		{
			foreach (object obj in atts)
			{
				XmlSchemaAttribute xmlSchemaAttribute = obj as XmlSchemaAttribute;
				if (xmlSchemaAttribute != null && xmlSchemaAttribute.RefName == XmlSchemaImporter.arrayTypeRefName)
				{
					return xmlSchemaAttribute;
				}
				XmlSchemaAttributeGroupRef xmlSchemaAttributeGroupRef = obj as XmlSchemaAttributeGroupRef;
				if (xmlSchemaAttributeGroupRef != null)
				{
					XmlSchemaAttributeGroup xmlSchemaAttributeGroup = this.FindRefAttributeGroup(xmlSchemaAttributeGroupRef.RefName);
					xmlSchemaAttribute = this.FindArrayAttribute(xmlSchemaAttributeGroup.Attributes);
					if (xmlSchemaAttribute != null)
					{
						return xmlSchemaAttribute;
					}
				}
			}
			return null;
		}

		// Token: 0x06001E03 RID: 7683 RVA: 0x000AC65C File Offset: 0x000AA85C
		private void ImportParticleComplexContent(XmlQualifiedName typeQName, ClassMap cmap, XmlSchemaParticle particle, CodeIdentifiers classIds, bool isMixed)
		{
			this.ImportParticleContent(typeQName, cmap, particle, classIds, false, ref isMixed);
			if (isMixed)
			{
				this.AddTextMember(typeQName, cmap, classIds);
			}
		}

		// Token: 0x06001E04 RID: 7684 RVA: 0x000AC67C File Offset: 0x000AA87C
		private void AddTextMember(XmlQualifiedName typeQName, ClassMap cmap, CodeIdentifiers classIds)
		{
			if (cmap.XmlTextCollector == null)
			{
				XmlTypeMapMemberFlatList xmlTypeMapMemberFlatList = new XmlTypeMapMemberFlatList();
				xmlTypeMapMemberFlatList.Name = classIds.AddUnique("Text", xmlTypeMapMemberFlatList);
				xmlTypeMapMemberFlatList.TypeData = TypeTranslator.GetTypeData(typeof(string[]));
				xmlTypeMapMemberFlatList.ElementInfo.Add(this.CreateTextElementInfo(typeQName.Namespace, xmlTypeMapMemberFlatList, xmlTypeMapMemberFlatList.TypeData.ListItemTypeData));
				xmlTypeMapMemberFlatList.IsXmlTextCollector = true;
				xmlTypeMapMemberFlatList.ListMap = new ListMap();
				xmlTypeMapMemberFlatList.ListMap.ItemInfo = xmlTypeMapMemberFlatList.ElementInfo;
				cmap.AddMember(xmlTypeMapMemberFlatList);
			}
		}

		// Token: 0x06001E05 RID: 7685 RVA: 0x000AC70C File Offset: 0x000AA90C
		private void ImportParticleContent(XmlQualifiedName typeQName, ClassMap cmap, XmlSchemaParticle particle, CodeIdentifiers classIds, bool multiValue, ref bool isMixed)
		{
			if (particle == null)
			{
				return;
			}
			if (particle is XmlSchemaGroupRef)
			{
				particle = this.GetRefGroupParticle((XmlSchemaGroupRef)particle);
			}
			if (particle.MaxOccurs > 1m)
			{
				multiValue = true;
			}
			if (particle is XmlSchemaSequence)
			{
				this.ImportSequenceContent(typeQName, cmap, ((XmlSchemaSequence)particle).Items, classIds, multiValue, ref isMixed);
				return;
			}
			if (!(particle is XmlSchemaChoice))
			{
				if (particle is XmlSchemaAll)
				{
					this.ImportSequenceContent(typeQName, cmap, ((XmlSchemaAll)particle).Items, classIds, multiValue, ref isMixed);
				}
				return;
			}
			if (((XmlSchemaChoice)particle).Items.Count == 1)
			{
				this.ImportSequenceContent(typeQName, cmap, ((XmlSchemaChoice)particle).Items, classIds, multiValue, ref isMixed);
				return;
			}
			this.ImportChoiceContent(typeQName, cmap, (XmlSchemaChoice)particle, classIds, multiValue);
		}

		// Token: 0x06001E06 RID: 7686 RVA: 0x000AC7D4 File Offset: 0x000AA9D4
		private void ImportSequenceContent(XmlQualifiedName typeQName, ClassMap cmap, XmlSchemaObjectCollection items, CodeIdentifiers classIds, bool multiValue, ref bool isMixed)
		{
			foreach (XmlSchemaObject xmlSchemaObject in items)
			{
				if (xmlSchemaObject is XmlSchemaElement)
				{
					XmlSchemaElement xmlSchemaElement = (XmlSchemaElement)xmlSchemaObject;
					XmlTypeMapping emap;
					TypeData typeData = this.GetElementTypeData(typeQName, xmlSchemaElement, null, out emap);
					string ns;
					XmlSchemaElement refElement = this.GetRefElement(typeQName, xmlSchemaElement, out ns);
					if (xmlSchemaElement.MaxOccurs == 1m && !multiValue)
					{
						XmlTypeMapMemberElement xmlTypeMapMemberElement;
						if (typeData.SchemaType != SchemaTypes.Array)
						{
							xmlTypeMapMemberElement = new XmlTypeMapMemberElement();
							if (refElement.DefaultValue != null)
							{
								xmlTypeMapMemberElement.DefaultValue = this.ImportDefaultValue(typeData, refElement.DefaultValue);
							}
						}
						else if (this.GetTypeMapping(typeData).IsSimpleType)
						{
							xmlTypeMapMemberElement = new XmlTypeMapMemberElement();
							typeData = TypeTranslator.GetTypeData(typeof(string));
						}
						else
						{
							xmlTypeMapMemberElement = new XmlTypeMapMemberList();
						}
						if (xmlSchemaElement.MinOccurs == 0m && typeData.IsValueType)
						{
							xmlTypeMapMemberElement.IsOptionalValueType = true;
						}
						xmlTypeMapMemberElement.Name = classIds.AddUnique(CodeIdentifier.MakeValid(refElement.Name), xmlTypeMapMemberElement);
						xmlTypeMapMemberElement.Documentation = this.GetDocumentation(xmlSchemaElement);
						xmlTypeMapMemberElement.TypeData = typeData;
						xmlTypeMapMemberElement.ElementInfo.Add(this.CreateElementInfo(ns, xmlTypeMapMemberElement, refElement.Name, typeData, refElement.IsNillable, refElement.Form, emap, items.IndexOf(xmlSchemaObject)));
						cmap.AddMember(xmlTypeMapMemberElement);
					}
					else
					{
						XmlTypeMapMemberFlatList xmlTypeMapMemberFlatList = new XmlTypeMapMemberFlatList();
						xmlTypeMapMemberFlatList.ListMap = new ListMap();
						xmlTypeMapMemberFlatList.Name = classIds.AddUnique(CodeIdentifier.MakeValid(refElement.Name), xmlTypeMapMemberFlatList);
						xmlTypeMapMemberFlatList.Documentation = this.GetDocumentation(xmlSchemaElement);
						xmlTypeMapMemberFlatList.TypeData = typeData.ListTypeData;
						xmlTypeMapMemberFlatList.ElementInfo.Add(this.CreateElementInfo(ns, xmlTypeMapMemberFlatList, refElement.Name, typeData, refElement.IsNillable, refElement.Form, emap, items.IndexOf(xmlSchemaObject)));
						xmlTypeMapMemberFlatList.ListMap.ItemInfo = xmlTypeMapMemberFlatList.ElementInfo;
						cmap.AddMember(xmlTypeMapMemberFlatList);
					}
				}
				else if (xmlSchemaObject is XmlSchemaAny)
				{
					XmlSchemaAny xmlSchemaAny = (XmlSchemaAny)xmlSchemaObject;
					XmlTypeMapMemberAnyElement xmlTypeMapMemberAnyElement = new XmlTypeMapMemberAnyElement();
					xmlTypeMapMemberAnyElement.Name = classIds.AddUnique("Any", xmlTypeMapMemberAnyElement);
					xmlTypeMapMemberAnyElement.Documentation = this.GetDocumentation(xmlSchemaAny);
					Type type;
					if (xmlSchemaAny.MaxOccurs != 1m || multiValue)
					{
						type = (isMixed ? typeof(XmlNode[]) : typeof(XmlElement[]));
					}
					else
					{
						type = (isMixed ? typeof(XmlNode) : typeof(XmlElement));
					}
					xmlTypeMapMemberAnyElement.TypeData = TypeTranslator.GetTypeData(type);
					XmlTypeMapElementInfo xmlTypeMapElementInfo = new XmlTypeMapElementInfo(xmlTypeMapMemberAnyElement, xmlTypeMapMemberAnyElement.TypeData);
					xmlTypeMapElementInfo.IsUnnamedAnyElement = true;
					xmlTypeMapMemberAnyElement.ElementInfo.Add(xmlTypeMapElementInfo);
					if (isMixed)
					{
						xmlTypeMapElementInfo = this.CreateTextElementInfo(typeQName.Namespace, xmlTypeMapMemberAnyElement, xmlTypeMapMemberAnyElement.TypeData);
						xmlTypeMapMemberAnyElement.ElementInfo.Add(xmlTypeMapElementInfo);
						xmlTypeMapMemberAnyElement.IsXmlTextCollector = true;
						isMixed = false;
					}
					cmap.AddMember(xmlTypeMapMemberAnyElement);
				}
				else if (xmlSchemaObject is XmlSchemaParticle)
				{
					this.ImportParticleContent(typeQName, cmap, (XmlSchemaParticle)xmlSchemaObject, classIds, multiValue, ref isMixed);
				}
			}
		}

		// Token: 0x06001E07 RID: 7687 RVA: 0x000ACB3C File Offset: 0x000AAD3C
		private object ImportDefaultValue(TypeData typeData, string value)
		{
			if (typeData.SchemaType != SchemaTypes.Enum)
			{
				return XmlCustomFormatter.FromXmlString(typeData, value);
			}
			XmlTypeMapping typeMapping = this.GetTypeMapping(typeData);
			string enumName = ((EnumMap)typeMapping.ObjectMap).GetEnumName(typeMapping.TypeFullName, value);
			if (enumName == null)
			{
				throw new InvalidOperationException("'" + value + "' is not a valid enumeration value");
			}
			return enumName;
		}

		// Token: 0x06001E08 RID: 7688 RVA: 0x000ACB94 File Offset: 0x000AAD94
		private void ImportChoiceContent(XmlQualifiedName typeQName, ClassMap cmap, XmlSchemaChoice choice, CodeIdentifiers classIds, bool multiValue)
		{
			XmlTypeMapElementInfoList xmlTypeMapElementInfoList = new XmlTypeMapElementInfoList();
			multiValue = (this.ImportChoices(typeQName, null, xmlTypeMapElementInfoList, choice.Items) || multiValue);
			if (xmlTypeMapElementInfoList.Count == 0)
			{
				return;
			}
			if (choice.MaxOccurs > 1m)
			{
				multiValue = true;
			}
			XmlTypeMapMemberElement xmlTypeMapMemberElement;
			if (multiValue)
			{
				xmlTypeMapMemberElement = new XmlTypeMapMemberFlatList();
				xmlTypeMapMemberElement.Name = classIds.AddUnique("Items", xmlTypeMapMemberElement);
				ListMap listMap = new ListMap();
				listMap.ItemInfo = xmlTypeMapElementInfoList;
				((XmlTypeMapMemberFlatList)xmlTypeMapMemberElement).ListMap = listMap;
			}
			else
			{
				xmlTypeMapMemberElement = new XmlTypeMapMemberElement();
				xmlTypeMapMemberElement.Name = classIds.AddUnique("Item", xmlTypeMapMemberElement);
			}
			TypeData typeData = null;
			bool flag = false;
			bool flag2 = true;
			Hashtable hashtable = new Hashtable();
			for (int i = xmlTypeMapElementInfoList.Count - 1; i >= 0; i--)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)xmlTypeMapElementInfoList[i];
				if (cmap.GetElement(xmlTypeMapElementInfo.ElementName, xmlTypeMapElementInfo.Namespace, xmlTypeMapElementInfo.ExplicitOrder) != null || xmlTypeMapElementInfoList.IndexOfElement(xmlTypeMapElementInfo.ElementName, xmlTypeMapElementInfo.Namespace) != i)
				{
					xmlTypeMapElementInfoList.RemoveAt(i);
				}
				else
				{
					if (hashtable.ContainsKey(xmlTypeMapElementInfo.TypeData))
					{
						flag = true;
					}
					else
					{
						hashtable.Add(xmlTypeMapElementInfo.TypeData, xmlTypeMapElementInfo);
					}
					TypeData typeData2 = xmlTypeMapElementInfo.TypeData;
					if (typeData2.SchemaType == SchemaTypes.Class)
					{
						XmlTypeMapping xmlTypeMapping = this.GetTypeMapping(typeData2);
						this.BuildPendingMap(xmlTypeMapping);
						while (xmlTypeMapping.BaseMap != null)
						{
							xmlTypeMapping = xmlTypeMapping.BaseMap;
							this.BuildPendingMap(xmlTypeMapping);
							typeData2 = xmlTypeMapping.TypeData;
						}
					}
					if (typeData == null)
					{
						typeData = typeData2;
					}
					else if (typeData != typeData2)
					{
						flag2 = false;
					}
				}
			}
			if (!flag2)
			{
				typeData = TypeTranslator.GetTypeData(typeof(object));
			}
			if (flag)
			{
				XmlTypeMapMemberElement xmlTypeMapMemberElement2 = new XmlTypeMapMemberElement();
				xmlTypeMapMemberElement2.Ignore = true;
				xmlTypeMapMemberElement2.Name = classIds.AddUnique(xmlTypeMapMemberElement.Name + "ElementName", xmlTypeMapMemberElement2);
				xmlTypeMapMemberElement.ChoiceMember = xmlTypeMapMemberElement2.Name;
				XmlTypeMapping xmlTypeMapping2 = this.CreateTypeMapping(new XmlQualifiedName(xmlTypeMapMemberElement.Name + "ChoiceType", typeQName.Namespace), SchemaTypes.Enum, null);
				xmlTypeMapping2.IncludeInSchema = false;
				CodeIdentifiers codeIdentifiers = new CodeIdentifiers();
				EnumMap.EnumMapMember[] array = new EnumMap.EnumMapMember[xmlTypeMapElementInfoList.Count];
				for (int j = 0; j < xmlTypeMapElementInfoList.Count; j++)
				{
					XmlTypeMapElementInfo xmlTypeMapElementInfo2 = (XmlTypeMapElementInfo)xmlTypeMapElementInfoList[j];
					string xmlName = (xmlTypeMapElementInfo2.Namespace != null && xmlTypeMapElementInfo2.Namespace != "" && xmlTypeMapElementInfo2.Namespace != typeQName.Namespace) ? (xmlTypeMapElementInfo2.Namespace + ":" + xmlTypeMapElementInfo2.ElementName) : xmlTypeMapElementInfo2.ElementName;
					string enumName = codeIdentifiers.AddUnique(CodeIdentifier.MakeValid(xmlTypeMapElementInfo2.ElementName), xmlTypeMapElementInfo2);
					array[j] = new EnumMap.EnumMapMember(xmlName, enumName);
				}
				xmlTypeMapping2.ObjectMap = new EnumMap(array, false);
				xmlTypeMapMemberElement2.TypeData = (multiValue ? xmlTypeMapping2.TypeData.ListTypeData : xmlTypeMapping2.TypeData);
				xmlTypeMapMemberElement2.ElementInfo.Add(this.CreateElementInfo(typeQName.Namespace, xmlTypeMapMemberElement2, xmlTypeMapMemberElement2.Name, xmlTypeMapMemberElement2.TypeData, false, XmlSchemaForm.None, -1));
				cmap.AddMember(xmlTypeMapMemberElement2);
			}
			if (typeData == null)
			{
				return;
			}
			if (multiValue)
			{
				typeData = typeData.ListTypeData;
			}
			xmlTypeMapMemberElement.ElementInfo = xmlTypeMapElementInfoList;
			xmlTypeMapMemberElement.Documentation = this.GetDocumentation(choice);
			xmlTypeMapMemberElement.TypeData = typeData;
			cmap.AddMember(xmlTypeMapMemberElement);
		}

		// Token: 0x06001E09 RID: 7689 RVA: 0x000ACEF4 File Offset: 0x000AB0F4
		private bool ImportChoices(XmlQualifiedName typeQName, XmlTypeMapMember member, XmlTypeMapElementInfoList choices, XmlSchemaObjectCollection items)
		{
			bool flag = false;
			foreach (XmlSchemaObject xmlSchemaObject in items)
			{
				if (xmlSchemaObject is XmlSchemaGroupRef)
				{
					xmlSchemaObject = this.GetRefGroupParticle((XmlSchemaGroupRef)xmlSchemaObject);
				}
				if (xmlSchemaObject is XmlSchemaElement)
				{
					XmlSchemaElement xmlSchemaElement = (XmlSchemaElement)xmlSchemaObject;
					XmlTypeMapping emap;
					TypeData elementTypeData = this.GetElementTypeData(typeQName, xmlSchemaElement, null, out emap);
					string ns;
					XmlSchemaElement refElement = this.GetRefElement(typeQName, xmlSchemaElement, out ns);
					choices.Add(this.CreateElementInfo(ns, member, refElement.Name, elementTypeData, refElement.IsNillable, refElement.Form, emap, -1));
					if (xmlSchemaElement.MaxOccurs > 1m)
					{
						flag = true;
					}
				}
				else if (xmlSchemaObject is XmlSchemaAny)
				{
					choices.Add(new XmlTypeMapElementInfo(member, TypeTranslator.GetTypeData(typeof(XmlElement)))
					{
						IsUnnamedAnyElement = true
					});
				}
				else if (xmlSchemaObject is XmlSchemaChoice)
				{
					flag = (this.ImportChoices(typeQName, member, choices, ((XmlSchemaChoice)xmlSchemaObject).Items) || flag);
				}
				else if (xmlSchemaObject is XmlSchemaSequence)
				{
					flag = (this.ImportChoices(typeQName, member, choices, ((XmlSchemaSequence)xmlSchemaObject).Items) || flag);
				}
			}
			return flag;
		}

		// Token: 0x06001E0A RID: 7690 RVA: 0x000AD048 File Offset: 0x000AB248
		private void ImportSimpleContent(XmlQualifiedName typeQName, XmlTypeMapping map, XmlSchemaSimpleContent content, CodeIdentifiers classIds, bool isMixed)
		{
			XmlSchemaSimpleContentExtension xmlSchemaSimpleContentExtension = content.Content as XmlSchemaSimpleContentExtension;
			ClassMap classMap = (ClassMap)map.ObjectMap;
			XmlQualifiedName contentBaseType = this.GetContentBaseType(content.Content);
			TypeData typeData = null;
			if (!this.IsPrimitiveTypeNamespace(contentBaseType.Namespace))
			{
				XmlTypeMapping xmlTypeMapping = this.ImportType(contentBaseType, null, true);
				this.BuildPendingMap(xmlTypeMapping);
				if (xmlTypeMapping.IsSimpleType)
				{
					typeData = xmlTypeMapping.TypeData;
				}
				else
				{
					foreach (object obj in ((ClassMap)xmlTypeMapping.ObjectMap).AllMembers)
					{
						XmlTypeMapMember member = (XmlTypeMapMember)obj;
						classMap.AddMember(member);
					}
					map.BaseMap = xmlTypeMapping;
					xmlTypeMapping.DerivedTypes.Add(map);
				}
			}
			else
			{
				typeData = this.FindBuiltInType(contentBaseType);
			}
			if (typeData != null)
			{
				XmlTypeMapMemberElement xmlTypeMapMemberElement = new XmlTypeMapMemberElement();
				xmlTypeMapMemberElement.Name = classIds.AddUnique("Value", xmlTypeMapMemberElement);
				xmlTypeMapMemberElement.TypeData = typeData;
				xmlTypeMapMemberElement.ElementInfo.Add(this.CreateTextElementInfo(typeQName.Namespace, xmlTypeMapMemberElement, xmlTypeMapMemberElement.TypeData));
				xmlTypeMapMemberElement.IsXmlTextCollector = true;
				classMap.AddMember(xmlTypeMapMemberElement);
			}
			if (xmlSchemaSimpleContentExtension != null)
			{
				this.ImportAttributes(typeQName, classMap, xmlSchemaSimpleContentExtension.Attributes, xmlSchemaSimpleContentExtension.AnyAttribute, classIds);
			}
		}

		// Token: 0x06001E0B RID: 7691 RVA: 0x000AD1A8 File Offset: 0x000AB3A8
		private TypeData FindBuiltInType(XmlQualifiedName qname)
		{
			XmlSchemaComplexType xmlSchemaComplexType = (XmlSchemaComplexType)this.schemas.Find(qname, typeof(XmlSchemaComplexType));
			if (xmlSchemaComplexType != null)
			{
				XmlSchemaSimpleContent xmlSchemaSimpleContent = xmlSchemaComplexType.ContentModel as XmlSchemaSimpleContent;
				if (xmlSchemaSimpleContent == null)
				{
					throw new InvalidOperationException("Invalid schema");
				}
				return this.FindBuiltInType(this.GetContentBaseType(xmlSchemaSimpleContent.Content));
			}
			else
			{
				XmlSchemaSimpleType xmlSchemaSimpleType = (XmlSchemaSimpleType)this.schemas.Find(qname, typeof(XmlSchemaSimpleType));
				if (xmlSchemaSimpleType != null)
				{
					return this.FindBuiltInType(qname, xmlSchemaSimpleType);
				}
				if (this.IsPrimitiveTypeNamespace(qname.Namespace))
				{
					return TypeTranslator.GetPrimitiveTypeData(qname.Name);
				}
				throw new InvalidOperationException("Definition of type '" + qname + "' not found");
			}
		}

		// Token: 0x06001E0C RID: 7692 RVA: 0x000AD258 File Offset: 0x000AB458
		private TypeData FindBuiltInType(XmlQualifiedName qname, XmlSchemaSimpleType st)
		{
			if (this.CanBeEnum(st) && qname != null)
			{
				return this.ImportType(qname, null, true).TypeData;
			}
			if (st.Content is XmlSchemaSimpleTypeRestriction)
			{
				XmlSchemaSimpleTypeRestriction xmlSchemaSimpleTypeRestriction = (XmlSchemaSimpleTypeRestriction)st.Content;
				XmlQualifiedName contentBaseType = this.GetContentBaseType(xmlSchemaSimpleTypeRestriction);
				if (contentBaseType == XmlQualifiedName.Empty && xmlSchemaSimpleTypeRestriction.BaseType != null)
				{
					return this.FindBuiltInType(qname, xmlSchemaSimpleTypeRestriction.BaseType);
				}
				return this.FindBuiltInType(contentBaseType);
			}
			else
			{
				if (st.Content is XmlSchemaSimpleTypeList)
				{
					return this.FindBuiltInType(this.GetContentBaseType(st.Content)).ListTypeData;
				}
				if (st.Content is XmlSchemaSimpleTypeUnion)
				{
					return this.FindBuiltInType(new XmlQualifiedName("string", "http://www.w3.org/2001/XMLSchema"));
				}
				return null;
			}
		}

		// Token: 0x06001E0D RID: 7693 RVA: 0x000AD31C File Offset: 0x000AB51C
		private XmlQualifiedName GetContentBaseType(XmlSchemaObject ob)
		{
			if (ob is XmlSchemaSimpleContentExtension)
			{
				return ((XmlSchemaSimpleContentExtension)ob).BaseTypeName;
			}
			if (ob is XmlSchemaSimpleContentRestriction)
			{
				return ((XmlSchemaSimpleContentRestriction)ob).BaseTypeName;
			}
			if (ob is XmlSchemaSimpleTypeRestriction)
			{
				return ((XmlSchemaSimpleTypeRestriction)ob).BaseTypeName;
			}
			if (ob is XmlSchemaSimpleTypeList)
			{
				return ((XmlSchemaSimpleTypeList)ob).ItemTypeName;
			}
			return null;
		}

		// Token: 0x06001E0E RID: 7694 RVA: 0x000AD37C File Offset: 0x000AB57C
		private void ImportComplexContent(XmlQualifiedName typeQName, XmlTypeMapping map, XmlSchemaComplexContent content, CodeIdentifiers classIds, bool isMixed)
		{
			ClassMap classMap = (ClassMap)map.ObjectMap;
			XmlSchemaComplexContentExtension xmlSchemaComplexContentExtension = content.Content as XmlSchemaComplexContentExtension;
			XmlQualifiedName baseTypeName;
			if (xmlSchemaComplexContentExtension != null)
			{
				baseTypeName = xmlSchemaComplexContentExtension.BaseTypeName;
			}
			else
			{
				baseTypeName = ((XmlSchemaComplexContentRestriction)content.Content).BaseTypeName;
			}
			if (baseTypeName == typeQName)
			{
				throw new InvalidOperationException(string.Concat(new string[]
				{
					"Cannot import schema for type '",
					typeQName.Name,
					"' from namespace '",
					typeQName.Namespace,
					"'. Redefine not supported"
				}));
			}
			XmlTypeMapping xmlTypeMapping = this.ImportClass(baseTypeName);
			this.BuildPendingMap(xmlTypeMapping);
			ClassMap classMap2 = (ClassMap)xmlTypeMapping.ObjectMap;
			foreach (object obj in classMap2.AllMembers)
			{
				XmlTypeMapMember member = (XmlTypeMapMember)obj;
				classMap.AddMember(member);
			}
			if (classMap2.XmlTextCollector != null)
			{
				isMixed = false;
			}
			else if (content.IsMixed)
			{
				isMixed = true;
			}
			map.BaseMap = xmlTypeMapping;
			xmlTypeMapping.DerivedTypes.Add(map);
			if (xmlSchemaComplexContentExtension != null)
			{
				this.ImportParticleComplexContent(typeQName, classMap, xmlSchemaComplexContentExtension.Particle, classIds, isMixed);
				this.ImportAttributes(typeQName, classMap, xmlSchemaComplexContentExtension.Attributes, xmlSchemaComplexContentExtension.AnyAttribute, classIds);
				return;
			}
			if (isMixed)
			{
				this.ImportParticleComplexContent(typeQName, classMap, null, classIds, true);
			}
		}

		// Token: 0x06001E0F RID: 7695 RVA: 0x000AD4E0 File Offset: 0x000AB6E0
		private void ImportExtensionTypes(XmlQualifiedName qname)
		{
			foreach (object obj in this.schemas)
			{
				XmlSchema xmlSchema = (XmlSchema)obj;
				foreach (XmlSchemaObject xmlSchemaObject in xmlSchema.Items)
				{
					XmlSchemaComplexType xmlSchemaComplexType = xmlSchemaObject as XmlSchemaComplexType;
					if (xmlSchemaComplexType != null && xmlSchemaComplexType.ContentModel is XmlSchemaComplexContent)
					{
						XmlSchemaComplexContentExtension xmlSchemaComplexContentExtension = xmlSchemaComplexType.ContentModel.Content as XmlSchemaComplexContentExtension;
						XmlQualifiedName baseTypeName;
						if (xmlSchemaComplexContentExtension != null)
						{
							baseTypeName = xmlSchemaComplexContentExtension.BaseTypeName;
						}
						else
						{
							baseTypeName = ((XmlSchemaComplexContentRestriction)xmlSchemaComplexType.ContentModel.Content).BaseTypeName;
						}
						if (baseTypeName == qname)
						{
							this.ImportType(new XmlQualifiedName(xmlSchemaComplexType.Name, xmlSchema.TargetNamespace), xmlSchemaComplexType, null);
						}
					}
				}
			}
		}

		// Token: 0x06001E10 RID: 7696 RVA: 0x000AD5EC File Offset: 0x000AB7EC
		private XmlTypeMapping ImportClassSimpleType(XmlQualifiedName typeQName, XmlSchemaSimpleType stype, XmlQualifiedName root)
		{
			if (this.CanBeEnum(stype))
			{
				CodeIdentifiers codeIdentifiers = new CodeIdentifiers();
				XmlTypeMapping xmlTypeMapping = this.CreateTypeMapping(typeQName, SchemaTypes.Enum, root);
				xmlTypeMapping.Documentation = this.GetDocumentation(stype);
				bool isFlags = false;
				if (stype.Content is XmlSchemaSimpleTypeList)
				{
					stype = ((XmlSchemaSimpleTypeList)stype.Content).ItemType;
					isFlags = true;
				}
				XmlSchemaSimpleTypeRestriction xmlSchemaSimpleTypeRestriction = (XmlSchemaSimpleTypeRestriction)stype.Content;
				codeIdentifiers.AddReserved(xmlTypeMapping.TypeData.TypeName);
				EnumMap.EnumMapMember[] array = new EnumMap.EnumMapMember[xmlSchemaSimpleTypeRestriction.Facets.Count];
				for (int i = 0; i < xmlSchemaSimpleTypeRestriction.Facets.Count; i++)
				{
					XmlSchemaEnumerationFacet xmlSchemaEnumerationFacet = (XmlSchemaEnumerationFacet)xmlSchemaSimpleTypeRestriction.Facets[i];
					string enumName = codeIdentifiers.AddUnique(CodeIdentifier.MakeValid(xmlSchemaEnumerationFacet.Value), xmlSchemaEnumerationFacet);
					array[i] = new EnumMap.EnumMapMember(xmlSchemaEnumerationFacet.Value, enumName);
					array[i].Documentation = this.GetDocumentation(xmlSchemaEnumerationFacet);
				}
				xmlTypeMapping.ObjectMap = new EnumMap(array, isFlags);
				xmlTypeMapping.IsSimpleType = true;
				return xmlTypeMapping;
			}
			if (stype.Content is XmlSchemaSimpleTypeList)
			{
				XmlSchemaSimpleTypeList xmlSchemaSimpleTypeList = (XmlSchemaSimpleTypeList)stype.Content;
				TypeData typeData = this.FindBuiltInType(xmlSchemaSimpleTypeList.ItemTypeName, stype);
				ListMap listMap = new ListMap();
				listMap.ItemInfo = new XmlTypeMapElementInfoList();
				listMap.ItemInfo.Add(this.CreateElementInfo(typeQName.Namespace, null, "Item", typeData.ListItemTypeData, false, XmlSchemaForm.None, -1));
				XmlTypeMapping xmlTypeMapping2 = this.CreateArrayTypeMapping(typeQName, typeData);
				xmlTypeMapping2.ObjectMap = listMap;
				xmlTypeMapping2.IsSimpleType = true;
				return xmlTypeMapping2;
			}
			TypeData typeData2 = this.FindBuiltInType(typeQName, stype);
			XmlTypeMapping typeMapping = this.GetTypeMapping(typeData2);
			typeMapping.IsSimpleType = true;
			return typeMapping;
		}

		// Token: 0x06001E11 RID: 7697 RVA: 0x000AD790 File Offset: 0x000AB990
		private bool CanBeEnum(XmlSchemaSimpleType stype)
		{
			if (stype.Content is XmlSchemaSimpleTypeRestriction)
			{
				XmlSchemaSimpleTypeRestriction xmlSchemaSimpleTypeRestriction = (XmlSchemaSimpleTypeRestriction)stype.Content;
				if (xmlSchemaSimpleTypeRestriction.Facets.Count == 0)
				{
					return false;
				}
				using (XmlSchemaObjectEnumerator enumerator = xmlSchemaSimpleTypeRestriction.Facets.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (!(enumerator.Current is XmlSchemaEnumerationFacet))
						{
							return false;
						}
					}
				}
				return true;
			}
			else
			{
				if (stype.Content is XmlSchemaSimpleTypeList)
				{
					XmlSchemaSimpleTypeList xmlSchemaSimpleTypeList = (XmlSchemaSimpleTypeList)stype.Content;
					return xmlSchemaSimpleTypeList.ItemType != null && this.CanBeEnum(xmlSchemaSimpleTypeList.ItemType);
				}
				return false;
			}
		}

		// Token: 0x06001E12 RID: 7698 RVA: 0x000AD848 File Offset: 0x000ABA48
		private bool CanBeArray(XmlQualifiedName typeQName, XmlSchemaComplexType stype)
		{
			if (!this.encodedFormat)
			{
				return stype.Attributes.Count <= 0 && stype.AnyAttribute == null && !stype.IsMixed && this.CanBeArray(typeQName, stype.Particle, false);
			}
			XmlSchemaComplexContent xmlSchemaComplexContent = stype.ContentModel as XmlSchemaComplexContent;
			if (xmlSchemaComplexContent == null)
			{
				return false;
			}
			XmlSchemaComplexContentRestriction xmlSchemaComplexContentRestriction = xmlSchemaComplexContent.Content as XmlSchemaComplexContentRestriction;
			return xmlSchemaComplexContentRestriction != null && xmlSchemaComplexContentRestriction.BaseTypeName == XmlSchemaImporter.arrayType;
		}

		// Token: 0x06001E13 RID: 7699 RVA: 0x000AD8C0 File Offset: 0x000ABAC0
		private bool CanBeArray(XmlQualifiedName typeQName, XmlSchemaParticle particle, bool multiValue)
		{
			if (particle == null)
			{
				return false;
			}
			multiValue = (multiValue || particle.MaxOccurs > 1m);
			if (particle is XmlSchemaGroupRef)
			{
				return this.CanBeArray(typeQName, this.GetRefGroupParticle((XmlSchemaGroupRef)particle), multiValue);
			}
			if (particle is XmlSchemaElement)
			{
				XmlSchemaElement xmlSchemaElement = (XmlSchemaElement)particle;
				if (!xmlSchemaElement.RefName.IsEmpty)
				{
					return this.CanBeArray(typeQName, this.FindRefElement(xmlSchemaElement), multiValue);
				}
				return multiValue && !typeQName.Equals(((XmlSchemaElement)particle).SchemaTypeName);
			}
			else
			{
				if (particle is XmlSchemaAny)
				{
					return multiValue;
				}
				if (particle is XmlSchemaSequence)
				{
					XmlSchemaSequence xmlSchemaSequence = particle as XmlSchemaSequence;
					return xmlSchemaSequence.Items.Count == 1 && this.CanBeArray(typeQName, (XmlSchemaParticle)xmlSchemaSequence.Items[0], multiValue);
				}
				if (particle is XmlSchemaChoice)
				{
					ArrayList types = new ArrayList();
					return this.CheckChoiceType(typeQName, particle, types, ref multiValue) && multiValue;
				}
				return false;
			}
		}

		// Token: 0x06001E14 RID: 7700 RVA: 0x000AD9B0 File Offset: 0x000ABBB0
		private bool CheckChoiceType(XmlQualifiedName typeQName, XmlSchemaParticle particle, ArrayList types, ref bool multiValue)
		{
			XmlQualifiedName xmlQualifiedName = null;
			multiValue = (multiValue || particle.MaxOccurs > 1m);
			if (particle is XmlSchemaGroupRef)
			{
				return this.CheckChoiceType(typeQName, this.GetRefGroupParticle((XmlSchemaGroupRef)particle), types, ref multiValue);
			}
			if (particle is XmlSchemaElement)
			{
				XmlSchemaElement elem = (XmlSchemaElement)particle;
				string text;
				XmlSchemaElement refElement = this.GetRefElement(typeQName, elem, out text);
				if (refElement.SchemaType != null)
				{
					return true;
				}
				xmlQualifiedName = refElement.SchemaTypeName;
			}
			else
			{
				if (!(particle is XmlSchemaAny))
				{
					if (particle is XmlSchemaSequence)
					{
						foreach (XmlSchemaObject xmlSchemaObject in (particle as XmlSchemaSequence).Items)
						{
							XmlSchemaParticle particle2 = (XmlSchemaParticle)xmlSchemaObject;
							if (!this.CheckChoiceType(typeQName, particle2, types, ref multiValue))
							{
								return false;
							}
						}
						return true;
					}
					if (particle is XmlSchemaChoice)
					{
						foreach (XmlSchemaObject xmlSchemaObject2 in ((XmlSchemaChoice)particle).Items)
						{
							XmlSchemaParticle particle3 = (XmlSchemaParticle)xmlSchemaObject2;
							if (!this.CheckChoiceType(typeQName, particle3, types, ref multiValue))
							{
								return false;
							}
						}
						return true;
					}
					goto IL_146;
				}
				xmlQualifiedName = XmlSchemaImporter.anyType;
			}
			IL_146:
			if (typeQName.Equals(xmlQualifiedName))
			{
				return false;
			}
			string text2;
			if (this.IsPrimitiveTypeNamespace(xmlQualifiedName.Namespace))
			{
				text2 = TypeTranslator.GetPrimitiveTypeData(xmlQualifiedName.Name).FullTypeName + ":" + xmlQualifiedName.Namespace;
			}
			else
			{
				text2 = xmlQualifiedName.Name + ":" + xmlQualifiedName.Namespace;
			}
			if (types.Contains(text2))
			{
				return false;
			}
			types.Add(text2);
			return true;
		}

		// Token: 0x06001E15 RID: 7701 RVA: 0x000ADB8C File Offset: 0x000ABD8C
		private bool CanBeAnyElement(XmlSchemaComplexType stype)
		{
			XmlSchemaSequence xmlSchemaSequence = stype.Particle as XmlSchemaSequence;
			return xmlSchemaSequence != null && xmlSchemaSequence.Items.Count == 1 && xmlSchemaSequence.Items[0] is XmlSchemaAny;
		}

		// Token: 0x06001E16 RID: 7702 RVA: 0x000ADBCC File Offset: 0x000ABDCC
		private Type GetAnyElementType(XmlSchemaComplexType stype)
		{
			XmlSchemaSequence xmlSchemaSequence = stype.Particle as XmlSchemaSequence;
			if (xmlSchemaSequence == null || xmlSchemaSequence.Items.Count != 1 || !(xmlSchemaSequence.Items[0] is XmlSchemaAny))
			{
				return null;
			}
			if (this.encodedFormat)
			{
				return typeof(object);
			}
			if ((xmlSchemaSequence.Items[0] as XmlSchemaAny).MaxOccurs == 1m)
			{
				if (stype.IsMixed)
				{
					return typeof(XmlNode);
				}
				return typeof(XmlElement);
			}
			else
			{
				if (stype.IsMixed)
				{
					return typeof(XmlNode[]);
				}
				return typeof(XmlElement[]);
			}
		}

		// Token: 0x06001E17 RID: 7703 RVA: 0x000ADC7C File Offset: 0x000ABE7C
		private bool CanBeIXmlSerializable(XmlSchemaComplexType stype)
		{
			XmlSchemaSequence xmlSchemaSequence = stype.Particle as XmlSchemaSequence;
			if (xmlSchemaSequence == null)
			{
				return false;
			}
			if (xmlSchemaSequence.Items.Count != 2)
			{
				return false;
			}
			XmlSchemaElement xmlSchemaElement = xmlSchemaSequence.Items[0] as XmlSchemaElement;
			return xmlSchemaElement != null && !(xmlSchemaElement.RefName != new XmlQualifiedName("schema", "http://www.w3.org/2001/XMLSchema")) && xmlSchemaSequence.Items[1] is XmlSchemaAny;
		}

		// Token: 0x06001E18 RID: 7704 RVA: 0x000ADCF4 File Offset: 0x000ABEF4
		private XmlTypeMapping ImportXmlSerializableMapping(string ns)
		{
			XmlQualifiedName xmlQualifiedName = new XmlQualifiedName("System.Data.DataSet", ns);
			XmlTypeMapping xmlTypeMapping = this.GetRegisteredTypeMapping(xmlQualifiedName);
			if (xmlTypeMapping != null)
			{
				return xmlTypeMapping;
			}
			TypeData typeData = new TypeData("System.Data.DataSet", "System.Data.DataSet", "System.Data.DataSet", SchemaTypes.XmlSerializable, null);
			xmlTypeMapping = new XmlTypeMapping("System.Data.DataSet", "", typeData, "System.Data.DataSet", ns);
			xmlTypeMapping.IncludeInSchema = true;
			this.RegisterTypeMapping(xmlQualifiedName, typeData, xmlTypeMapping);
			return xmlTypeMapping;
		}

		// Token: 0x06001E19 RID: 7705 RVA: 0x000ADD5C File Offset: 0x000ABF5C
		private XmlTypeMapElementInfo CreateElementInfo(string ns, XmlTypeMapMember member, string name, TypeData typeData, bool isNillable, XmlSchemaForm form, int order)
		{
			if (typeData.IsComplexType)
			{
				return this.CreateElementInfo(ns, member, name, typeData, isNillable, form, this.GetTypeMapping(typeData), order);
			}
			return this.CreateElementInfo(ns, member, name, typeData, isNillable, form, null, order);
		}

		// Token: 0x06001E1A RID: 7706 RVA: 0x000ADDA0 File Offset: 0x000ABFA0
		private XmlTypeMapElementInfo CreateElementInfo(string ns, XmlTypeMapMember member, string name, TypeData typeData, bool isNillable, XmlSchemaForm form, XmlTypeMapping emap, int order)
		{
			XmlTypeMapElementInfo xmlTypeMapElementInfo = new XmlTypeMapElementInfo(member, typeData);
			xmlTypeMapElementInfo.ElementName = name;
			xmlTypeMapElementInfo.Namespace = ns;
			xmlTypeMapElementInfo.IsNullable = isNillable;
			xmlTypeMapElementInfo.Form = this.GetForm(form, ns, true);
			if (typeData.IsComplexType)
			{
				xmlTypeMapElementInfo.MappedType = emap;
			}
			xmlTypeMapElementInfo.ExplicitOrder = order;
			return xmlTypeMapElementInfo;
		}

		// Token: 0x06001E1B RID: 7707 RVA: 0x000ADDF8 File Offset: 0x000ABFF8
		private XmlSchemaForm GetForm(XmlSchemaForm form, string ns, bool forElement)
		{
			if (form != XmlSchemaForm.None)
			{
				return form;
			}
			XmlSchema xmlSchema = this.schemas[ns];
			if (xmlSchema == null)
			{
				return XmlSchemaForm.Unqualified;
			}
			XmlSchemaForm xmlSchemaForm = forElement ? xmlSchema.ElementFormDefault : xmlSchema.AttributeFormDefault;
			if (xmlSchemaForm != XmlSchemaForm.None)
			{
				return xmlSchemaForm;
			}
			return XmlSchemaForm.Unqualified;
		}

		// Token: 0x06001E1C RID: 7708 RVA: 0x000ADE34 File Offset: 0x000AC034
		private XmlTypeMapElementInfo CreateTextElementInfo(string ns, XmlTypeMapMember member, TypeData typeData)
		{
			XmlTypeMapElementInfo xmlTypeMapElementInfo = new XmlTypeMapElementInfo(member, typeData);
			xmlTypeMapElementInfo.IsTextElement = true;
			xmlTypeMapElementInfo.WrappedElement = false;
			if (typeData.IsComplexType)
			{
				xmlTypeMapElementInfo.MappedType = this.GetTypeMapping(typeData);
			}
			return xmlTypeMapElementInfo;
		}

		// Token: 0x06001E1D RID: 7709 RVA: 0x000ADE70 File Offset: 0x000AC070
		private XmlTypeMapping CreateTypeMapping(XmlQualifiedName typeQName, SchemaTypes schemaType, XmlQualifiedName root)
		{
			string text = CodeIdentifier.MakeValid(typeQName.Name);
			text = this.typeIdentifiers.AddUnique(text, null);
			TypeData typeData = new TypeData(text, text, text, schemaType, null);
			string name;
			string ns;
			if (root != null)
			{
				name = root.Name;
				ns = root.Namespace;
			}
			else
			{
				name = typeQName.Name;
				ns = "";
			}
			XmlTypeMapping xmlTypeMapping = new XmlTypeMapping(name, ns, typeData, typeQName.Name, typeQName.Namespace);
			xmlTypeMapping.IncludeInSchema = true;
			this.RegisterTypeMapping(typeQName, typeData, xmlTypeMapping);
			return xmlTypeMapping;
		}

		// Token: 0x06001E1E RID: 7710 RVA: 0x000ADEF4 File Offset: 0x000AC0F4
		private XmlTypeMapping CreateArrayTypeMapping(XmlQualifiedName typeQName, TypeData arrayTypeData)
		{
			XmlTypeMapping xmlTypeMapping;
			if (this.encodedFormat)
			{
				xmlTypeMapping = new XmlTypeMapping("Array", "http://schemas.xmlsoap.org/soap/encoding/", arrayTypeData, "Array", "http://schemas.xmlsoap.org/soap/encoding/");
			}
			else
			{
				xmlTypeMapping = new XmlTypeMapping(arrayTypeData.XmlType, typeQName.Namespace, arrayTypeData, arrayTypeData.XmlType, typeQName.Namespace);
			}
			xmlTypeMapping.IncludeInSchema = true;
			this.RegisterTypeMapping(typeQName, arrayTypeData, xmlTypeMapping);
			return xmlTypeMapping;
		}

		// Token: 0x06001E1F RID: 7711 RVA: 0x000ADF56 File Offset: 0x000AC156
		private XmlSchemaElement GetRefElement(XmlQualifiedName typeQName, XmlSchemaElement elem, out string ns)
		{
			if (!elem.RefName.IsEmpty)
			{
				ns = elem.RefName.Namespace;
				return this.FindRefElement(elem);
			}
			ns = typeQName.Namespace;
			return elem;
		}

		// Token: 0x06001E20 RID: 7712 RVA: 0x000ADF84 File Offset: 0x000AC184
		private XmlSchemaAttribute GetRefAttribute(XmlQualifiedName typeQName, XmlSchemaAttribute attr, out string ns)
		{
			if (attr.RefName.IsEmpty)
			{
				ns = ((attr.Parent is XmlSchema) ? typeQName.Namespace : string.Empty);
				return attr;
			}
			ns = attr.RefName.Namespace;
			XmlSchemaAttribute xmlSchemaAttribute = this.FindRefAttribute(attr.RefName);
			if (xmlSchemaAttribute == null)
			{
				throw new InvalidOperationException("The attribute " + attr.RefName + " is missing");
			}
			return xmlSchemaAttribute;
		}

		// Token: 0x06001E21 RID: 7713 RVA: 0x000ADFF4 File Offset: 0x000AC1F4
		private TypeData GetElementTypeData(XmlQualifiedName typeQName, XmlSchemaElement elem, XmlQualifiedName root, out XmlTypeMapping map)
		{
			bool sharedAnnType = false;
			map = null;
			if (!elem.RefName.IsEmpty)
			{
				XmlSchemaElement xmlSchemaElement = this.FindRefElement(elem);
				if (xmlSchemaElement == null)
				{
					throw new InvalidOperationException("Global element not found: " + elem.RefName);
				}
				root = elem.RefName;
				elem = xmlSchemaElement;
				sharedAnnType = true;
			}
			TypeData typeData;
			if (!elem.SchemaTypeName.IsEmpty)
			{
				typeData = this.GetTypeData(elem.SchemaTypeName, root, elem.IsNillable);
				map = this.GetRegisteredTypeMapping(typeData);
			}
			else if (elem.SchemaType == null)
			{
				typeData = TypeTranslator.GetTypeData(typeof(object));
			}
			else
			{
				typeData = this.GetTypeData(elem.SchemaType, typeQName, elem.Name, sharedAnnType, root);
			}
			if (map == null && typeData.IsComplexType)
			{
				map = this.GetTypeMapping(typeData);
			}
			return typeData;
		}

		// Token: 0x06001E22 RID: 7714 RVA: 0x000AE0B8 File Offset: 0x000AC2B8
		private TypeData GetAttributeTypeData(XmlQualifiedName typeQName, XmlSchemaAttribute attr)
		{
			bool sharedAnnType = false;
			if (!attr.RefName.IsEmpty)
			{
				XmlSchemaAttribute xmlSchemaAttribute = this.FindRefAttribute(attr.RefName);
				if (xmlSchemaAttribute == null)
				{
					throw new InvalidOperationException("Global attribute not found: " + attr.RefName);
				}
				attr = xmlSchemaAttribute;
				sharedAnnType = true;
			}
			if (!attr.SchemaTypeName.IsEmpty)
			{
				return this.GetTypeData(attr.SchemaTypeName, null, false);
			}
			if (attr.SchemaType == null)
			{
				return TypeTranslator.GetTypeData(typeof(string));
			}
			return this.GetTypeData(attr.SchemaType, typeQName, attr.Name, sharedAnnType, null);
		}

		// Token: 0x06001E23 RID: 7715 RVA: 0x000AE148 File Offset: 0x000AC348
		private TypeData GetTypeData(XmlQualifiedName typeQName, XmlQualifiedName root, bool isNullable)
		{
			if (this.IsPrimitiveTypeNamespace(typeQName.Namespace))
			{
				XmlTypeMapping xmlTypeMapping = this.ImportType(typeQName, root, false);
				if (xmlTypeMapping != null)
				{
					return xmlTypeMapping.TypeData;
				}
				return TypeTranslator.GetPrimitiveTypeData(typeQName.Name, isNullable);
			}
			else
			{
				if (this.encodedFormat && typeQName.Namespace == "")
				{
					return TypeTranslator.GetPrimitiveTypeData(typeQName.Name);
				}
				return this.ImportType(typeQName, root, true).TypeData;
			}
		}

		// Token: 0x06001E24 RID: 7716 RVA: 0x000AE1B8 File Offset: 0x000AC3B8
		private TypeData GetTypeData(XmlSchemaType stype, XmlQualifiedName typeQNname, string propertyName, bool sharedAnnType, XmlQualifiedName root)
		{
			string text;
			if (sharedAnnType)
			{
				TypeData typeData = this.sharedAnonymousTypes[stype] as TypeData;
				if (typeData != null)
				{
					return typeData;
				}
				text = propertyName;
			}
			else
			{
				text = typeQNname.Name + this.typeIdentifiers.MakeRightCase(propertyName);
			}
			text = this.elemIdentifiers.AddUnique(text, stype);
			XmlQualifiedName name = new XmlQualifiedName(text, typeQNname.Namespace);
			XmlTypeMapping xmlTypeMapping = this.ImportType(name, stype, root);
			if (sharedAnnType)
			{
				this.sharedAnonymousTypes[stype] = xmlTypeMapping.TypeData;
			}
			return xmlTypeMapping.TypeData;
		}

		// Token: 0x06001E25 RID: 7717 RVA: 0x000AE240 File Offset: 0x000AC440
		private XmlTypeMapping GetTypeMapping(TypeData typeData)
		{
			if (typeData.Type == typeof(object) && !this.anyTypeImported)
			{
				this.ImportAllObjectTypes();
			}
			XmlTypeMapping xmlTypeMapping = this.GetRegisteredTypeMapping(typeData);
			if (xmlTypeMapping != null)
			{
				return xmlTypeMapping;
			}
			if (typeData.IsListType)
			{
				XmlTypeMapping typeMapping = this.GetTypeMapping(typeData.ListItemTypeData);
				xmlTypeMapping = new XmlTypeMapping(typeData.XmlType, typeMapping.Namespace, typeData, typeData.XmlType, typeMapping.Namespace);
				xmlTypeMapping.IncludeInSchema = true;
				xmlTypeMapping.ObjectMap = new ListMap
				{
					ItemInfo = new XmlTypeMapElementInfoList(),
					ItemInfo = 
					{
						this.CreateElementInfo(typeMapping.Namespace, null, typeData.ListItemTypeData.XmlType, typeData.ListItemTypeData, false, XmlSchemaForm.None, -1)
					}
				};
				this.RegisterTypeMapping(new XmlQualifiedName(xmlTypeMapping.ElementName, xmlTypeMapping.Namespace), typeData, xmlTypeMapping);
				return xmlTypeMapping;
			}
			if (typeData.SchemaType == SchemaTypes.Primitive || typeData.Type == typeof(object) || typeof(XmlNode).IsAssignableFrom(typeData.Type))
			{
				return this.CreateSystemMap(typeData);
			}
			throw new InvalidOperationException("Map for type " + typeData.TypeName + " not found");
		}

		// Token: 0x06001E26 RID: 7718 RVA: 0x000AE378 File Offset: 0x000AC578
		private void AddObjectDerivedMap(XmlTypeMapping map)
		{
			TypeData typeData = TypeTranslator.GetTypeData(typeof(object));
			XmlTypeMapping xmlTypeMapping = this.GetRegisteredTypeMapping(typeData);
			if (xmlTypeMapping == null)
			{
				xmlTypeMapping = this.CreateSystemMap(typeData);
			}
			xmlTypeMapping.DerivedTypes.Add(map);
		}

		// Token: 0x06001E27 RID: 7719 RVA: 0x000AE3B8 File Offset: 0x000AC5B8
		private XmlTypeMapping CreateSystemMap(TypeData typeData)
		{
			XmlTypeMapping xmlTypeMapping = new XmlTypeMapping(typeData.XmlType, "http://www.w3.org/2001/XMLSchema", typeData, typeData.XmlType, "http://www.w3.org/2001/XMLSchema");
			xmlTypeMapping.IncludeInSchema = false;
			xmlTypeMapping.ObjectMap = new ClassMap();
			this.dataMappedTypes[typeData] = xmlTypeMapping;
			return xmlTypeMapping;
		}

		// Token: 0x06001E28 RID: 7720 RVA: 0x000AE404 File Offset: 0x000AC604
		private void ImportAllObjectTypes()
		{
			this.anyTypeImported = true;
			foreach (object obj in this.schemas)
			{
				XmlSchema xmlSchema = (XmlSchema)obj;
				foreach (XmlSchemaObject xmlSchemaObject in xmlSchema.Items)
				{
					XmlSchemaComplexType xmlSchemaComplexType = xmlSchemaObject as XmlSchemaComplexType;
					if (xmlSchemaComplexType != null)
					{
						this.ImportType(new XmlQualifiedName(xmlSchemaComplexType.Name, xmlSchema.TargetNamespace), xmlSchemaComplexType, null);
					}
				}
			}
		}

		// Token: 0x06001E29 RID: 7721 RVA: 0x000AE4C4 File Offset: 0x000AC6C4
		private XmlTypeMapping GetRegisteredTypeMapping(XmlQualifiedName typeQName, Type baseType)
		{
			if (this.IsPrimitiveTypeNamespace(typeQName.Namespace))
			{
				return (XmlTypeMapping)this.primitiveDerivedMappedTypes[typeQName];
			}
			return (XmlTypeMapping)this.mappedTypes[typeQName];
		}

		// Token: 0x06001E2A RID: 7722 RVA: 0x000AE4F7 File Offset: 0x000AC6F7
		private XmlTypeMapping GetRegisteredTypeMapping(XmlQualifiedName typeQName)
		{
			return (XmlTypeMapping)this.mappedTypes[typeQName];
		}

		// Token: 0x06001E2B RID: 7723 RVA: 0x000AE50A File Offset: 0x000AC70A
		private XmlTypeMapping GetRegisteredTypeMapping(TypeData typeData)
		{
			return (XmlTypeMapping)this.dataMappedTypes[typeData];
		}

		// Token: 0x06001E2C RID: 7724 RVA: 0x000AE51D File Offset: 0x000AC71D
		private void RegisterTypeMapping(XmlQualifiedName qname, TypeData typeData, XmlTypeMapping map)
		{
			this.dataMappedTypes[typeData] = map;
			if (this.IsPrimitiveTypeNamespace(qname.Namespace) && !map.IsSimpleType)
			{
				this.primitiveDerivedMappedTypes[qname] = map;
				return;
			}
			this.mappedTypes[qname] = map;
		}

		// Token: 0x06001E2D RID: 7725 RVA: 0x000AE55D File Offset: 0x000AC75D
		private XmlSchemaParticle GetRefGroupParticle(XmlSchemaGroupRef refGroup)
		{
			return ((XmlSchemaGroup)this.schemas.Find(refGroup.RefName, typeof(XmlSchemaGroup))).Particle;
		}

		// Token: 0x06001E2E RID: 7726 RVA: 0x000AE584 File Offset: 0x000AC784
		private XmlSchemaElement FindRefElement(XmlSchemaElement elem)
		{
			XmlSchemaElement xmlSchemaElement = (XmlSchemaElement)this.schemas.Find(elem.RefName, typeof(XmlSchemaElement));
			if (xmlSchemaElement != null)
			{
				return xmlSchemaElement;
			}
			if (!this.IsPrimitiveTypeNamespace(elem.RefName.Namespace))
			{
				return null;
			}
			if (this.anyElement != null)
			{
				return this.anyElement;
			}
			this.anyElement = new XmlSchemaElement();
			this.anyElement.Name = "any";
			this.anyElement.SchemaTypeName = XmlSchemaImporter.anyType;
			return this.anyElement;
		}

		// Token: 0x06001E2F RID: 7727 RVA: 0x000AE60C File Offset: 0x000AC80C
		private XmlSchemaAttribute FindRefAttribute(XmlQualifiedName refName)
		{
			if (refName.Namespace == "http://www.w3.org/XML/1998/namespace")
			{
				return new XmlSchemaAttribute
				{
					Name = refName.Name,
					SchemaTypeName = new XmlQualifiedName("string", "http://www.w3.org/2001/XMLSchema")
				};
			}
			return (XmlSchemaAttribute)this.schemas.Find(refName, typeof(XmlSchemaAttribute));
		}

		// Token: 0x06001E30 RID: 7728 RVA: 0x000AE670 File Offset: 0x000AC870
		private XmlSchemaAttributeGroup FindRefAttributeGroup(XmlQualifiedName refName)
		{
			XmlSchemaAttributeGroup xmlSchemaAttributeGroup = (XmlSchemaAttributeGroup)this.schemas.Find(refName, typeof(XmlSchemaAttributeGroup));
			foreach (XmlSchemaObject xmlSchemaObject in xmlSchemaAttributeGroup.Attributes)
			{
				if (xmlSchemaObject is XmlSchemaAttributeGroupRef && ((XmlSchemaAttributeGroupRef)xmlSchemaObject).RefName == refName)
				{
					throw new InvalidOperationException(string.Concat(new string[]
					{
						"Cannot import attribute group '",
						refName.Name,
						"' from namespace '",
						refName.Namespace,
						"'. Redefine not supported"
					}));
				}
			}
			return xmlSchemaAttributeGroup;
		}

		// Token: 0x06001E31 RID: 7729 RVA: 0x000AE730 File Offset: 0x000AC930
		private XmlTypeMapping ReflectType(Type type)
		{
			TypeData typeData = TypeTranslator.GetTypeData(type);
			return this.ReflectType(typeData, null);
		}

		// Token: 0x06001E32 RID: 7730 RVA: 0x000AE74C File Offset: 0x000AC94C
		private XmlTypeMapping ReflectType(TypeData typeData, string ns)
		{
			if (!this.encodedFormat)
			{
				if (this.auxXmlRefImporter == null)
				{
					this.auxXmlRefImporter = new XmlReflectionImporter();
				}
				return this.auxXmlRefImporter.ImportTypeMapping(typeData, ns);
			}
			if (this.auxSoapRefImporter == null)
			{
				this.auxSoapRefImporter = new SoapReflectionImporter();
			}
			return this.auxSoapRefImporter.ImportTypeMapping(typeData, ns);
		}

		// Token: 0x06001E33 RID: 7731 RVA: 0x000AE7A4 File Offset: 0x000AC9A4
		private string GetDocumentation(XmlSchemaAnnotated elem)
		{
			string text = "";
			XmlSchemaAnnotation annotation = elem.Annotation;
			if (annotation == null || annotation.Items == null)
			{
				return null;
			}
			foreach (XmlSchemaObject xmlSchemaObject in annotation.Items)
			{
				XmlSchemaDocumentation xmlSchemaDocumentation = xmlSchemaObject as XmlSchemaDocumentation;
				if (xmlSchemaDocumentation != null && xmlSchemaDocumentation.Markup != null && xmlSchemaDocumentation.Markup.Length != 0)
				{
					if (text != string.Empty)
					{
						text += "\n";
					}
					foreach (XmlNode xmlNode in xmlSchemaDocumentation.Markup)
					{
						text += xmlNode.Value;
					}
				}
			}
			return text;
		}

		// Token: 0x06001E34 RID: 7732 RVA: 0x000AE874 File Offset: 0x000ACA74
		private bool IsPrimitiveTypeNamespace(string ns)
		{
			return ns == "http://www.w3.org/2001/XMLSchema" || (this.encodedFormat && ns == "http://schemas.xmlsoap.org/soap/encoding/");
		}

		// Token: 0x06001E35 RID: 7733 RVA: 0x000AE89A File Offset: 0x000ACA9A
		// Note: this type is marked as 'beforefieldinit'.
		static XmlSchemaImporter()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" /> class. </summary>
		/// <param name="schemas">A collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects.</param>
		/// <param name="options">A bitwise combination of the <see cref="T:System.Xml.Serialization.CodeGenerationOptions" /> values that specifies the options to use when generating .NET Framework types for a Web service.</param>
		/// <param name="codeProvider">A <see cref="T:System.CodeDom.Compiler.CodeDomProvider" /> used to generate the serialization code.</param>
		/// <param name="context">A <see cref="T:System.Xml.Serialization.ImportContext" /> instance that specifies the import context.</param>
		// Token: 0x06001E36 RID: 7734 RVA: 0x00072668 File Offset: 0x00070868
		public XmlSchemaImporter(XmlSchemas schemas, CodeGenerationOptions options, CodeDomProvider codeProvider, ImportContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" /> class for a collection of XML schemas, using the specified code generation options and import context.</summary>
		/// <param name="schemas">A collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects.</param>
		/// <param name="options">A <see cref="T:System.Xml.Serialization.CodeGenerationOptions" /> enumeration that specifies code generation options.</param>
		/// <param name="context">A <see cref="T:System.Xml.Serialization.ImportContext" /> instance that specifies the import context.</param>
		// Token: 0x06001E37 RID: 7735 RVA: 0x00072668 File Offset: 0x00070868
		public XmlSchemaImporter(XmlSchemas schemas, CodeGenerationOptions options, ImportContext context)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" /> class, taking a collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects that represents the XML schemas used by SOAP literal messages, plus classes being generated for bindings defined in a WSDL document, and a <see cref="T:System.Xml.Serialization.CodeGenerationOptions" /> enumeration value.</summary>
		/// <param name="schemas">A collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects.</param>
		/// <param name="typeIdentifiers">A <see cref="T:System.Xml.Serialization.CodeIdentifiers" /> object that specifies a collection of classes being generated for bindings defined in a WSDL document.</param>
		/// <param name="options">A bitwise combination of the <see cref="T:System.Xml.Serialization.CodeGenerationOptions" /> values that specifies the options to use when generating .NET Framework types for a Web service.</param>
		// Token: 0x06001E38 RID: 7736 RVA: 0x00072668 File Offset: 0x00070868
		public XmlSchemaImporter(XmlSchemas schemas, CodeIdentifiers typeIdentifiers, CodeGenerationOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040016AC RID: 5804
		private XmlSchemas schemas;

		// Token: 0x040016AD RID: 5805
		private CodeIdentifiers typeIdentifiers;

		// Token: 0x040016AE RID: 5806
		private CodeIdentifiers elemIdentifiers;

		// Token: 0x040016AF RID: 5807
		private Hashtable mappedTypes;

		// Token: 0x040016B0 RID: 5808
		private Hashtable primitiveDerivedMappedTypes;

		// Token: 0x040016B1 RID: 5809
		private Hashtable dataMappedTypes;

		// Token: 0x040016B2 RID: 5810
		private Queue pendingMaps;

		// Token: 0x040016B3 RID: 5811
		private Hashtable sharedAnonymousTypes;

		// Token: 0x040016B4 RID: 5812
		private bool encodedFormat;

		// Token: 0x040016B5 RID: 5813
		private XmlReflectionImporter auxXmlRefImporter;

		// Token: 0x040016B6 RID: 5814
		private SoapReflectionImporter auxSoapRefImporter;

		// Token: 0x040016B7 RID: 5815
		private bool anyTypeImported;

		// Token: 0x040016B8 RID: 5816
		private static readonly XmlQualifiedName anyType = new XmlQualifiedName("anyType", "http://www.w3.org/2001/XMLSchema");

		// Token: 0x040016B9 RID: 5817
		private static readonly XmlQualifiedName arrayType = new XmlQualifiedName("Array", "http://schemas.xmlsoap.org/soap/encoding/");

		// Token: 0x040016BA RID: 5818
		private static readonly XmlQualifiedName arrayTypeRefName = new XmlQualifiedName("arrayType", "http://schemas.xmlsoap.org/soap/encoding/");

		// Token: 0x040016BB RID: 5819
		private const string XmlNamespace = "http://www.w3.org/XML/1998/namespace";

		// Token: 0x040016BC RID: 5820
		private XmlSchemaElement anyElement;

		// Token: 0x040016BD RID: 5821
		private ArrayList fixup_registered_types;

		// Token: 0x02000323 RID: 803
		private class MapFixup
		{
			// Token: 0x06001E39 RID: 7737 RVA: 0x00002103 File Offset: 0x00000303
			public MapFixup()
			{
			}

			// Token: 0x040016BE RID: 5822
			public XmlTypeMapping Map;

			// Token: 0x040016BF RID: 5823
			public XmlSchemaComplexType SchemaType;

			// Token: 0x040016C0 RID: 5824
			public XmlQualifiedName TypeName;
		}

		// Token: 0x02000324 RID: 804
		[CompilerGenerated]
		private sealed class <EnumerateAttributes>d__54 : IEnumerable<XmlSchemaAttribute>, IEnumerable, IEnumerator<XmlSchemaAttribute>, IDisposable, IEnumerator
		{
			// Token: 0x06001E3A RID: 7738 RVA: 0x000AE8D8 File Offset: 0x000ACAD8
			[DebuggerHidden]
			public <EnumerateAttributes>d__54(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06001E3B RID: 7739 RVA: 0x000AE8F4 File Offset: 0x000ACAF4
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num - -4 <= 1 || num - 1 <= 1)
				{
					try
					{
						if (num == -4 || num == 1)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally2();
							}
						}
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06001E3C RID: 7740 RVA: 0x000AE950 File Offset: 0x000ACB50
			bool IEnumerator.MoveNext()
			{
				try
				{
					int num = this.<>1__state;
					XmlSchemaImporter xmlSchemaImporter = this;
					switch (num)
					{
					case 0:
						this.<>1__state = -1;
						xmlSchemaObjectEnumerator = col.GetEnumerator();
						this.<>1__state = -3;
						break;
					case 1:
						this.<>1__state = -4;
						goto IL_110;
					case 2:
						this.<>1__state = -3;
						goto IL_150;
					default:
						return false;
					}
					IL_157:
					while (xmlSchemaObjectEnumerator.MoveNext())
					{
						o = xmlSchemaObjectEnumerator.Current;
						if (!(o is XmlSchemaAttributeGroupRef))
						{
							this.<>2__current = (XmlSchemaAttribute)o;
							this.<>1__state = 2;
							return true;
						}
						XmlSchemaAttributeGroupRef xmlSchemaAttributeGroupRef = (XmlSchemaAttributeGroupRef)o;
						XmlSchemaAttributeGroup xmlSchemaAttributeGroup = xmlSchemaImporter.FindRefAttributeGroup(xmlSchemaAttributeGroupRef.RefName);
						if (!recurse.Contains(xmlSchemaAttributeGroup))
						{
							recurse.Add(xmlSchemaAttributeGroup);
							if (xmlSchemaAttributeGroup == null)
							{
								throw new InvalidOperationException(string.Format("Referenced AttributeGroup '{0}' was not found.", xmlSchemaAttributeGroupRef.RefName));
							}
							enumerator = xmlSchemaImporter.EnumerateAttributes(xmlSchemaAttributeGroup.Attributes, recurse).GetEnumerator();
							this.<>1__state = -4;
							goto IL_110;
						}
					}
					this.<>m__Finally1();
					xmlSchemaObjectEnumerator = null;
					return false;
					IL_110:
					if (enumerator.MoveNext())
					{
						XmlSchemaAttribute xmlSchemaAttribute = enumerator.Current;
						this.<>2__current = xmlSchemaAttribute;
						this.<>1__state = 1;
						return true;
					}
					this.<>m__Finally2();
					enumerator = null;
					IL_150:
					o = null;
					goto IL_157;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				bool result;
				return result;
			}

			// Token: 0x06001E3D RID: 7741 RVA: 0x000AEAFC File Offset: 0x000ACCFC
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				IDisposable disposable = xmlSchemaObjectEnumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}

			// Token: 0x06001E3E RID: 7742 RVA: 0x000AEB25 File Offset: 0x000ACD25
			private void <>m__Finally2()
			{
				this.<>1__state = -3;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x170005D0 RID: 1488
			// (get) Token: 0x06001E3F RID: 7743 RVA: 0x000AEB42 File Offset: 0x000ACD42
			XmlSchemaAttribute IEnumerator<XmlSchemaAttribute>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06001E40 RID: 7744 RVA: 0x00010D4A File Offset: 0x0000EF4A
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170005D1 RID: 1489
			// (get) Token: 0x06001E41 RID: 7745 RVA: 0x000AEB42 File Offset: 0x000ACD42
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06001E42 RID: 7746 RVA: 0x000AEB4C File Offset: 0x000ACD4C
			[DebuggerHidden]
			IEnumerator<XmlSchemaAttribute> IEnumerable<XmlSchemaAttribute>.GetEnumerator()
			{
				XmlSchemaImporter.<EnumerateAttributes>d__54 <EnumerateAttributes>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<EnumerateAttributes>d__ = this;
				}
				else
				{
					<EnumerateAttributes>d__ = new XmlSchemaImporter.<EnumerateAttributes>d__54(0);
					<EnumerateAttributes>d__.<>4__this = this;
				}
				<EnumerateAttributes>d__.col = col;
				<EnumerateAttributes>d__.recurse = recurse;
				return <EnumerateAttributes>d__;
			}

			// Token: 0x06001E43 RID: 7747 RVA: 0x000AEBA7 File Offset: 0x000ACDA7
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.Schema.XmlSchemaAttribute>.GetEnumerator();
			}

			// Token: 0x040016C1 RID: 5825
			private int <>1__state;

			// Token: 0x040016C2 RID: 5826
			private XmlSchemaAttribute <>2__current;

			// Token: 0x040016C3 RID: 5827
			private int <>l__initialThreadId;

			// Token: 0x040016C4 RID: 5828
			private XmlSchemaObjectCollection col;

			// Token: 0x040016C5 RID: 5829
			public XmlSchemaObjectCollection <>3__col;

			// Token: 0x040016C6 RID: 5830
			public XmlSchemaImporter <>4__this;

			// Token: 0x040016C7 RID: 5831
			private List<XmlSchemaAttributeGroup> recurse;

			// Token: 0x040016C8 RID: 5832
			public List<XmlSchemaAttributeGroup> <>3__recurse;

			// Token: 0x040016C9 RID: 5833
			private XmlSchemaObject <o>5__1;

			// Token: 0x040016CA RID: 5834
			private XmlSchemaObjectEnumerator <>7__wrap1;

			// Token: 0x040016CB RID: 5835
			private IEnumerator<XmlSchemaAttribute> <>7__wrap2;
		}
	}
}
