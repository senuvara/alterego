﻿using System;
using System.Collections;
using System.Reflection;

namespace System.Xml.Serialization
{
	// Token: 0x0200032F RID: 815
	internal class XmlSerializationReaderInterpreter : XmlSerializationReader
	{
		// Token: 0x06001EC0 RID: 7872 RVA: 0x000B04DA File Offset: 0x000AE6DA
		public XmlSerializationReaderInterpreter(XmlMapping typeMap)
		{
			this._typeMap = typeMap;
			this._format = typeMap.Format;
		}

		// Token: 0x06001EC1 RID: 7873 RVA: 0x000B04F8 File Offset: 0x000AE6F8
		protected override void InitCallbacks()
		{
			ArrayList relatedMaps = this._typeMap.RelatedMaps;
			if (relatedMaps != null)
			{
				foreach (object obj in relatedMaps)
				{
					XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)obj;
					if (xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Class || xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Enum)
					{
						XmlSerializationReaderInterpreter.ReaderCallbackInfo @object = new XmlSerializationReaderInterpreter.ReaderCallbackInfo(this, xmlTypeMapping);
						base.AddReadCallback(xmlTypeMapping.XmlType, xmlTypeMapping.Namespace, xmlTypeMapping.TypeData.Type, new XmlSerializationReadCallback(@object.ReadObject));
					}
				}
			}
		}

		// Token: 0x06001EC2 RID: 7874 RVA: 0x000030EC File Offset: 0x000012EC
		protected override void InitIDs()
		{
		}

		// Token: 0x06001EC3 RID: 7875 RVA: 0x000B05A8 File Offset: 0x000AE7A8
		protected XmlTypeMapping GetTypeMap(Type type)
		{
			ArrayList relatedMaps = this._typeMap.RelatedMaps;
			if (relatedMaps != null)
			{
				foreach (object obj in relatedMaps)
				{
					XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)obj;
					if (xmlTypeMapping.TypeData.Type == type)
					{
						return xmlTypeMapping;
					}
				}
			}
			throw new InvalidOperationException("Type " + type + " not mapped");
		}

		// Token: 0x06001EC4 RID: 7876 RVA: 0x000B0638 File Offset: 0x000AE838
		public object ReadRoot()
		{
			base.Reader.MoveToContent();
			if (!(this._typeMap is XmlTypeMapping))
			{
				return this.ReadMessage((XmlMembersMapping)this._typeMap);
			}
			if (this._format == SerializationFormat.Literal)
			{
				return this.ReadRoot((XmlTypeMapping)this._typeMap);
			}
			return this.ReadEncodedObject((XmlTypeMapping)this._typeMap);
		}

		// Token: 0x06001EC5 RID: 7877 RVA: 0x000B069C File Offset: 0x000AE89C
		private object ReadEncodedObject(XmlTypeMapping typeMap)
		{
			object result = null;
			base.Reader.MoveToContent();
			if (base.Reader.NodeType == XmlNodeType.Element)
			{
				if (!(base.Reader.LocalName == typeMap.ElementName) || !(base.Reader.NamespaceURI == typeMap.Namespace))
				{
					throw base.CreateUnknownNodeException();
				}
				result = base.ReadReferencedElement();
			}
			else
			{
				base.UnknownNode(null);
			}
			base.ReadReferencedElements();
			return result;
		}

		// Token: 0x06001EC6 RID: 7878 RVA: 0x000B0714 File Offset: 0x000AE914
		protected virtual object ReadMessage(XmlMembersMapping typeMap)
		{
			object[] array = new object[typeMap.Count];
			if (typeMap.HasWrapperElement)
			{
				ArrayList allMembers = ((ClassMap)typeMap.ObjectMap).AllMembers;
				for (int i = 0; i < allMembers.Count; i++)
				{
					XmlTypeMapMember xmlTypeMapMember = (XmlTypeMapMember)allMembers[i];
					if (!xmlTypeMapMember.IsReturnValue && xmlTypeMapMember.TypeData.IsValueType)
					{
						this.SetMemberValueFromAttr(xmlTypeMapMember, array, this.CreateInstance(xmlTypeMapMember.TypeData.Type), true);
					}
				}
				if (this._format == SerializationFormat.Encoded)
				{
					while (base.Reader.NodeType == XmlNodeType.Element)
					{
						string attribute = base.Reader.GetAttribute("root", "http://schemas.xmlsoap.org/soap/encoding/");
						if (attribute == null || XmlConvert.ToBoolean(attribute))
						{
							break;
						}
						base.ReadReferencedElement();
						base.Reader.MoveToContent();
					}
				}
				while (base.Reader.NodeType != XmlNodeType.EndElement)
				{
					if (base.Reader.ReadState != ReadState.Interactive)
					{
						break;
					}
					if (base.Reader.IsStartElement(typeMap.ElementName, typeMap.Namespace) || this._format == SerializationFormat.Encoded)
					{
						this.ReadAttributeMembers((ClassMap)typeMap.ObjectMap, array, true);
						if (!base.Reader.IsEmptyElement)
						{
							base.Reader.ReadStartElement();
							this.ReadMembers((ClassMap)typeMap.ObjectMap, array, true, false);
							base.ReadEndElement();
							break;
						}
						base.Reader.Skip();
						base.Reader.MoveToContent();
					}
					else
					{
						base.UnknownNode(null);
						base.Reader.MoveToContent();
					}
				}
			}
			else
			{
				this.ReadMembers((ClassMap)typeMap.ObjectMap, array, true, this._format == SerializationFormat.Encoded);
			}
			if (this._format == SerializationFormat.Encoded)
			{
				base.ReadReferencedElements();
			}
			return array;
		}

		// Token: 0x06001EC7 RID: 7879 RVA: 0x000B08D4 File Offset: 0x000AEAD4
		private object ReadRoot(XmlTypeMapping rootMap)
		{
			if (rootMap.TypeData.SchemaType == SchemaTypes.XmlNode)
			{
				return this.ReadXmlNodeElement(rootMap, true);
			}
			if (!rootMap.IsAny && (base.Reader.LocalName != rootMap.ElementName || base.Reader.NamespaceURI != rootMap.Namespace))
			{
				throw base.CreateUnknownNodeException();
			}
			return this.ReadObject(rootMap, rootMap.IsNullable, true);
		}

		// Token: 0x06001EC8 RID: 7880 RVA: 0x000B0948 File Offset: 0x000AEB48
		protected virtual object ReadObject(XmlTypeMapping typeMap, bool isNullable, bool checkType)
		{
			switch (typeMap.TypeData.SchemaType)
			{
			case SchemaTypes.Primitive:
				return this.ReadPrimitiveElement(typeMap, isNullable);
			case SchemaTypes.Enum:
				return this.ReadEnumElement(typeMap, isNullable);
			case SchemaTypes.Array:
				return this.ReadListElement(typeMap, isNullable, null, true);
			case SchemaTypes.Class:
				return this.ReadClassInstance(typeMap, isNullable, checkType);
			case SchemaTypes.XmlSerializable:
				return this.ReadXmlSerializableElement(typeMap, isNullable);
			case SchemaTypes.XmlNode:
				return this.ReadXmlNodeElement(typeMap, isNullable);
			default:
				throw new Exception("Unsupported map type");
			}
		}

		// Token: 0x06001EC9 RID: 7881 RVA: 0x000B09C8 File Offset: 0x000AEBC8
		protected virtual object ReadClassInstance(XmlTypeMapping typeMap, bool isNullable, bool checkType)
		{
			if (isNullable && base.ReadNull())
			{
				return null;
			}
			if (checkType)
			{
				XmlQualifiedName xsiType = base.GetXsiType();
				if (xsiType != null)
				{
					XmlTypeMapping realElementMap = typeMap.GetRealElementMap(xsiType.Name, xsiType.Namespace);
					if (realElementMap == null)
					{
						if (typeMap.TypeData.Type == typeof(object))
						{
							return base.ReadTypedPrimitive(xsiType);
						}
						throw base.CreateUnknownTypeException(xsiType);
					}
					else if (realElementMap != typeMap)
					{
						return this.ReadObject(realElementMap, false, false);
					}
				}
				else if (typeMap.TypeData.Type == typeof(object))
				{
					return base.ReadTypedPrimitive(XmlSerializationReaderInterpreter.AnyType);
				}
			}
			object obj = XmlSerializationReaderInterpreter.CreateInstance(typeMap.TypeData.Type, true);
			base.Reader.MoveToElement();
			bool isEmptyElement = base.Reader.IsEmptyElement;
			this.ReadClassInstanceMembers(typeMap, obj);
			if (isEmptyElement)
			{
				base.Reader.Skip();
			}
			else
			{
				base.ReadEndElement();
			}
			return obj;
		}

		// Token: 0x06001ECA RID: 7882 RVA: 0x000B0AB7 File Offset: 0x000AECB7
		protected virtual void ReadClassInstanceMembers(XmlTypeMapping typeMap, object ob)
		{
			this.ReadMembers((ClassMap)typeMap.ObjectMap, ob, false, false);
		}

		// Token: 0x06001ECB RID: 7883 RVA: 0x000B0AD0 File Offset: 0x000AECD0
		private void ReadAttributeMembers(ClassMap map, object ob, bool isValueList)
		{
			XmlTypeMapMember defaultAnyAttributeMember = map.DefaultAnyAttributeMember;
			int length = 0;
			object obj = null;
			while (base.Reader.MoveToNextAttribute())
			{
				XmlTypeMapMemberAttribute attribute = map.GetAttribute(base.Reader.LocalName, base.Reader.NamespaceURI);
				if (attribute != null)
				{
					this.SetMemberValue(attribute, ob, this.GetValueFromXmlString(base.Reader.Value, attribute.TypeData, attribute.MappedType), isValueList);
				}
				else if (base.IsXmlnsAttribute(base.Reader.Name))
				{
					if (map.NamespaceDeclarations != null)
					{
						XmlSerializerNamespaces xmlSerializerNamespaces = this.GetMemberValue(map.NamespaceDeclarations, ob, isValueList) as XmlSerializerNamespaces;
						if (xmlSerializerNamespaces == null)
						{
							xmlSerializerNamespaces = new XmlSerializerNamespaces();
							this.SetMemberValue(map.NamespaceDeclarations, ob, xmlSerializerNamespaces, isValueList);
						}
						if (base.Reader.Prefix == "xmlns")
						{
							xmlSerializerNamespaces.Add(base.Reader.LocalName, base.Reader.Value);
						}
						else
						{
							xmlSerializerNamespaces.Add("", base.Reader.Value);
						}
					}
				}
				else if (defaultAnyAttributeMember != null)
				{
					XmlAttribute xmlAttribute = (XmlAttribute)base.Document.ReadNode(base.Reader);
					base.ParseWsdlArrayType(xmlAttribute);
					this.AddListValue(defaultAnyAttributeMember.TypeData, ref obj, length++, xmlAttribute, true);
				}
				else
				{
					this.ProcessUnknownAttribute(ob);
				}
			}
			if (defaultAnyAttributeMember != null)
			{
				obj = base.ShrinkArray((Array)obj, length, defaultAnyAttributeMember.TypeData.Type.GetElementType(), true);
				this.SetMemberValue(defaultAnyAttributeMember, ob, obj, isValueList);
			}
			base.Reader.MoveToElement();
		}

		// Token: 0x06001ECC RID: 7884 RVA: 0x000B0C64 File Offset: 0x000AEE64
		private void ReadMembers(ClassMap map, object ob, bool isValueList, bool readBySoapOrder)
		{
			this.ReadAttributeMembers(map, ob, isValueList);
			if (!isValueList)
			{
				base.Reader.MoveToElement();
				if (base.Reader.IsEmptyElement)
				{
					this.SetListMembersDefaults(map, ob, isValueList);
					return;
				}
				base.Reader.ReadStartElement();
			}
			bool[] array = new bool[(map.ElementMembers != null) ? map.ElementMembers.Count : 0];
			bool flag = isValueList && this._format == SerializationFormat.Encoded && map.ReturnMember != null;
			base.Reader.MoveToContent();
			int[] array2 = null;
			object[] array3 = null;
			object[] array4 = null;
			XmlSerializationReader.Fixup fixup = null;
			int num = -1;
			int num2;
			if (readBySoapOrder)
			{
				if (map.ElementMembers != null)
				{
					num2 = map.ElementMembers.Count;
				}
				else
				{
					num2 = -1;
				}
			}
			else
			{
				num2 = int.MaxValue;
			}
			if (map.FlatLists != null)
			{
				array2 = new int[map.FlatLists.Count];
				array3 = new object[map.FlatLists.Count];
				foreach (object obj in map.FlatLists)
				{
					XmlTypeMapMemberExpandable xmlTypeMapMemberExpandable = (XmlTypeMapMemberExpandable)obj;
					if (this.IsReadOnly(xmlTypeMapMemberExpandable, xmlTypeMapMemberExpandable.TypeData, ob, isValueList))
					{
						array3[xmlTypeMapMemberExpandable.FlatArrayIndex] = xmlTypeMapMemberExpandable.GetValue(ob);
					}
					else if (xmlTypeMapMemberExpandable.TypeData.Type.IsArray)
					{
						array3[xmlTypeMapMemberExpandable.FlatArrayIndex] = this.InitializeList(xmlTypeMapMemberExpandable.TypeData);
					}
					else
					{
						object obj2 = xmlTypeMapMemberExpandable.GetValue(ob);
						if (obj2 == null)
						{
							obj2 = this.InitializeList(xmlTypeMapMemberExpandable.TypeData);
							this.SetMemberValue(xmlTypeMapMemberExpandable, ob, obj2, isValueList);
						}
						array3[xmlTypeMapMemberExpandable.FlatArrayIndex] = obj2;
					}
					if (xmlTypeMapMemberExpandable.ChoiceMember != null)
					{
						if (array4 == null)
						{
							array4 = new object[map.FlatLists.Count];
						}
						array4[xmlTypeMapMemberExpandable.FlatArrayIndex] = this.InitializeList(xmlTypeMapMemberExpandable.ChoiceTypeData);
					}
				}
			}
			if (this._format == SerializationFormat.Encoded && map.ElementMembers != null)
			{
				XmlSerializationReaderInterpreter.FixupCallbackInfo @object = new XmlSerializationReaderInterpreter.FixupCallbackInfo(this, map, isValueList);
				fixup = new XmlSerializationReader.Fixup(ob, new XmlSerializationFixupCallback(@object.FixupMembers), map.ElementMembers.Count);
				base.AddFixup(fixup);
			}
			XmlTypeMapMember xmlTypeMapMember = null;
			while (base.Reader.NodeType != XmlNodeType.EndElement && num < num2 - 1)
			{
				if (base.Reader.NodeType == XmlNodeType.Element)
				{
					XmlTypeMapElementInfo xmlTypeMapElementInfo;
					if (readBySoapOrder)
					{
						xmlTypeMapElementInfo = map.GetElement(base.Reader.LocalName, base.Reader.NamespaceURI, num);
					}
					else if (flag)
					{
						xmlTypeMapElementInfo = (XmlTypeMapElementInfo)((XmlTypeMapMemberElement)map.ReturnMember).ElementInfo[0];
						flag = false;
					}
					else if (map.IsOrderDependentMap)
					{
						xmlTypeMapElementInfo = map.GetElement(base.Reader.LocalName, base.Reader.NamespaceURI, num);
					}
					else
					{
						xmlTypeMapElementInfo = map.GetElement(base.Reader.LocalName, base.Reader.NamespaceURI);
					}
					if (xmlTypeMapElementInfo != null && !array[xmlTypeMapElementInfo.Member.Index])
					{
						if (xmlTypeMapElementInfo.Member != xmlTypeMapMember)
						{
							num = xmlTypeMapElementInfo.ExplicitOrder + 1;
							if (xmlTypeMapElementInfo.Member is XmlTypeMapMemberFlatList)
							{
								num--;
							}
							xmlTypeMapMember = xmlTypeMapElementInfo.Member;
						}
						if (xmlTypeMapElementInfo.Member.GetType() == typeof(XmlTypeMapMemberList))
						{
							if (this._format == SerializationFormat.Encoded && xmlTypeMapElementInfo.MultiReferenceType)
							{
								object obj3 = base.ReadReferencingElement(out fixup.Ids[xmlTypeMapElementInfo.Member.Index]);
								if (fixup.Ids[xmlTypeMapElementInfo.Member.Index] == null)
								{
									if (this.IsReadOnly(xmlTypeMapElementInfo.Member, xmlTypeMapElementInfo.TypeData, ob, isValueList))
									{
										throw base.CreateReadOnlyCollectionException(xmlTypeMapElementInfo.TypeData.FullTypeName);
									}
									this.SetMemberValue(xmlTypeMapElementInfo.Member, ob, obj3, isValueList);
								}
								else if (!xmlTypeMapElementInfo.MappedType.TypeData.Type.IsArray)
								{
									if (this.IsReadOnly(xmlTypeMapElementInfo.Member, xmlTypeMapElementInfo.TypeData, ob, isValueList))
									{
										obj3 = this.GetMemberValue(xmlTypeMapElementInfo.Member, ob, isValueList);
									}
									else
									{
										obj3 = this.CreateList(xmlTypeMapElementInfo.MappedType.TypeData.Type);
										this.SetMemberValue(xmlTypeMapElementInfo.Member, ob, obj3, isValueList);
									}
									base.AddFixup(new XmlSerializationReader.CollectionFixup(obj3, new XmlSerializationCollectionFixupCallback(this.FillList), fixup.Ids[xmlTypeMapElementInfo.Member.Index]));
									fixup.Ids[xmlTypeMapElementInfo.Member.Index] = null;
								}
							}
							else if (this.IsReadOnly(xmlTypeMapElementInfo.Member, xmlTypeMapElementInfo.TypeData, ob, isValueList))
							{
								this.ReadListElement(xmlTypeMapElementInfo.MappedType, xmlTypeMapElementInfo.IsNullable, this.GetMemberValue(xmlTypeMapElementInfo.Member, ob, isValueList), false);
							}
							else if (xmlTypeMapElementInfo.MappedType.TypeData.Type.IsArray)
							{
								object obj4 = this.ReadListElement(xmlTypeMapElementInfo.MappedType, xmlTypeMapElementInfo.IsNullable, null, true);
								if (obj4 != null || xmlTypeMapElementInfo.IsNullable)
								{
									this.SetMemberValue(xmlTypeMapElementInfo.Member, ob, obj4, isValueList);
								}
							}
							else
							{
								object obj5 = this.GetMemberValue(xmlTypeMapElementInfo.Member, ob, isValueList);
								if (obj5 == null)
								{
									obj5 = this.CreateList(xmlTypeMapElementInfo.MappedType.TypeData.Type);
									this.SetMemberValue(xmlTypeMapElementInfo.Member, ob, obj5, isValueList);
								}
								this.ReadListElement(xmlTypeMapElementInfo.MappedType, xmlTypeMapElementInfo.IsNullable, obj5, true);
							}
							array[xmlTypeMapElementInfo.Member.Index] = true;
						}
						else if (xmlTypeMapElementInfo.Member.GetType() == typeof(XmlTypeMapMemberFlatList))
						{
							XmlTypeMapMemberFlatList xmlTypeMapMemberFlatList = (XmlTypeMapMemberFlatList)xmlTypeMapElementInfo.Member;
							TypeData typeData = xmlTypeMapMemberFlatList.TypeData;
							object[] array5 = array3;
							int flatArrayIndex = xmlTypeMapMemberFlatList.FlatArrayIndex;
							int[] array6 = array2;
							int flatArrayIndex2 = xmlTypeMapMemberFlatList.FlatArrayIndex;
							int num3 = array6[flatArrayIndex2];
							array6[flatArrayIndex2] = num3 + 1;
							this.AddListValue(typeData, ref array5[flatArrayIndex], num3, this.ReadObjectElement(xmlTypeMapElementInfo), !this.IsReadOnly(xmlTypeMapElementInfo.Member, xmlTypeMapElementInfo.TypeData, ob, isValueList));
							if (xmlTypeMapMemberFlatList.ChoiceMember != null)
							{
								this.AddListValue(xmlTypeMapMemberFlatList.ChoiceTypeData, ref array4[xmlTypeMapMemberFlatList.FlatArrayIndex], array2[xmlTypeMapMemberFlatList.FlatArrayIndex] - 1, xmlTypeMapElementInfo.ChoiceValue, true);
							}
						}
						else if (xmlTypeMapElementInfo.Member.GetType() == typeof(XmlTypeMapMemberAnyElement))
						{
							XmlTypeMapMemberAnyElement xmlTypeMapMemberAnyElement = (XmlTypeMapMemberAnyElement)xmlTypeMapElementInfo.Member;
							if (xmlTypeMapMemberAnyElement.TypeData.IsListType)
							{
								TypeData typeData2 = xmlTypeMapMemberAnyElement.TypeData;
								object[] array7 = array3;
								int flatArrayIndex3 = xmlTypeMapMemberAnyElement.FlatArrayIndex;
								int[] array8 = array2;
								int flatArrayIndex4 = xmlTypeMapMemberAnyElement.FlatArrayIndex;
								int num3 = array8[flatArrayIndex4];
								array8[flatArrayIndex4] = num3 + 1;
								this.AddListValue(typeData2, ref array7[flatArrayIndex3], num3, this.ReadXmlNode(xmlTypeMapMemberAnyElement.TypeData.ListItemTypeData, false), true);
							}
							else
							{
								this.SetMemberValue(xmlTypeMapMemberAnyElement, ob, this.ReadXmlNode(xmlTypeMapMemberAnyElement.TypeData, false), isValueList);
							}
						}
						else
						{
							if (!(xmlTypeMapElementInfo.Member.GetType() == typeof(XmlTypeMapMemberElement)))
							{
								throw new InvalidOperationException("Unknown member type");
							}
							array[xmlTypeMapElementInfo.Member.Index] = true;
							if (this._format == SerializationFormat.Encoded)
							{
								object obj6;
								if (xmlTypeMapElementInfo.Member.TypeData.SchemaType != SchemaTypes.Primitive)
								{
									obj6 = base.ReadReferencingElement(out fixup.Ids[xmlTypeMapElementInfo.Member.Index]);
								}
								else
								{
									obj6 = base.ReadReferencingElement(xmlTypeMapElementInfo.Member.TypeData.XmlType, "http://www.w3.org/2001/XMLSchema", out fixup.Ids[xmlTypeMapElementInfo.Member.Index]);
								}
								if (xmlTypeMapElementInfo.MultiReferenceType)
								{
									if (fixup.Ids[xmlTypeMapElementInfo.Member.Index] == null)
									{
										this.SetMemberValue(xmlTypeMapElementInfo.Member, ob, obj6, isValueList);
									}
								}
								else if (obj6 != null)
								{
									this.SetMemberValue(xmlTypeMapElementInfo.Member, ob, obj6, isValueList);
								}
							}
							else
							{
								this.SetMemberValue(xmlTypeMapElementInfo.Member, ob, this.ReadObjectElement(xmlTypeMapElementInfo), isValueList);
								if (xmlTypeMapElementInfo.ChoiceValue != null)
								{
									((XmlTypeMapMemberElement)xmlTypeMapElementInfo.Member).SetChoice(ob, xmlTypeMapElementInfo.ChoiceValue);
								}
							}
						}
					}
					else if (map.DefaultAnyElementMember != null)
					{
						XmlTypeMapMemberAnyElement defaultAnyElementMember = map.DefaultAnyElementMember;
						if (defaultAnyElementMember.TypeData.IsListType)
						{
							TypeData typeData3 = defaultAnyElementMember.TypeData;
							object[] array9 = array3;
							int flatArrayIndex5 = defaultAnyElementMember.FlatArrayIndex;
							int[] array10 = array2;
							int flatArrayIndex6 = defaultAnyElementMember.FlatArrayIndex;
							int num3 = array10[flatArrayIndex6];
							array10[flatArrayIndex6] = num3 + 1;
							this.AddListValue(typeData3, ref array9[flatArrayIndex5], num3, this.ReadXmlNode(defaultAnyElementMember.TypeData.ListItemTypeData, false), true);
						}
						else
						{
							this.SetMemberValue(defaultAnyElementMember, ob, this.ReadXmlNode(defaultAnyElementMember.TypeData, false), isValueList);
						}
					}
					else
					{
						this.ProcessUnknownElement(ob);
					}
				}
				else if ((base.Reader.NodeType == XmlNodeType.Text || base.Reader.NodeType == XmlNodeType.CDATA) && map.XmlTextCollector != null)
				{
					if (map.XmlTextCollector is XmlTypeMapMemberExpandable)
					{
						XmlTypeMapMemberExpandable xmlTypeMapMemberExpandable2 = (XmlTypeMapMemberExpandable)map.XmlTextCollector;
						XmlTypeMapMemberFlatList xmlTypeMapMemberFlatList2 = xmlTypeMapMemberExpandable2 as XmlTypeMapMemberFlatList;
						TypeData typeData4 = (xmlTypeMapMemberFlatList2 == null) ? xmlTypeMapMemberExpandable2.TypeData.ListItemTypeData : xmlTypeMapMemberFlatList2.ListMap.FindTextElement().TypeData;
						object value = (typeData4.Type == typeof(string)) ? base.Reader.ReadString() : this.ReadXmlNode(typeData4, false);
						TypeData typeData5 = xmlTypeMapMemberExpandable2.TypeData;
						object[] array11 = array3;
						int flatArrayIndex7 = xmlTypeMapMemberExpandable2.FlatArrayIndex;
						int[] array12 = array2;
						int flatArrayIndex8 = xmlTypeMapMemberExpandable2.FlatArrayIndex;
						int num3 = array12[flatArrayIndex8];
						array12[flatArrayIndex8] = num3 + 1;
						this.AddListValue(typeData5, ref array11[flatArrayIndex7], num3, value, true);
					}
					else
					{
						XmlTypeMapMemberElement xmlTypeMapMemberElement = (XmlTypeMapMemberElement)map.XmlTextCollector;
						XmlTypeMapElementInfo xmlTypeMapElementInfo2 = (XmlTypeMapElementInfo)xmlTypeMapMemberElement.ElementInfo[0];
						if (xmlTypeMapElementInfo2.TypeData.Type == typeof(string))
						{
							this.SetMemberValue(xmlTypeMapMemberElement, ob, base.Reader.ReadString(), isValueList);
						}
						else
						{
							this.SetMemberValue(xmlTypeMapMemberElement, ob, this.GetValueFromXmlString(base.Reader.ReadString(), xmlTypeMapElementInfo2.TypeData, xmlTypeMapElementInfo2.MappedType), isValueList);
						}
					}
				}
				else
				{
					base.UnknownNode(ob);
				}
				base.Reader.MoveToContent();
			}
			if (array3 != null)
			{
				foreach (object obj7 in map.FlatLists)
				{
					XmlTypeMapMemberExpandable xmlTypeMapMemberExpandable3 = (XmlTypeMapMemberExpandable)obj7;
					object obj8 = array3[xmlTypeMapMemberExpandable3.FlatArrayIndex];
					if (xmlTypeMapMemberExpandable3.TypeData.Type.IsArray)
					{
						obj8 = base.ShrinkArray((Array)obj8, array2[xmlTypeMapMemberExpandable3.FlatArrayIndex], xmlTypeMapMemberExpandable3.TypeData.Type.GetElementType(), true);
					}
					if (!this.IsReadOnly(xmlTypeMapMemberExpandable3, xmlTypeMapMemberExpandable3.TypeData, ob, isValueList) && xmlTypeMapMemberExpandable3.TypeData.Type.IsArray)
					{
						this.SetMemberValue(xmlTypeMapMemberExpandable3, ob, obj8, isValueList);
					}
				}
			}
			if (array4 != null)
			{
				foreach (object obj9 in map.FlatLists)
				{
					XmlTypeMapMemberExpandable xmlTypeMapMemberExpandable4 = (XmlTypeMapMemberExpandable)obj9;
					object obj10 = array4[xmlTypeMapMemberExpandable4.FlatArrayIndex];
					if (obj10 != null)
					{
						obj10 = base.ShrinkArray((Array)obj10, array2[xmlTypeMapMemberExpandable4.FlatArrayIndex], xmlTypeMapMemberExpandable4.ChoiceTypeData.Type.GetElementType(), true);
						XmlTypeMapMember.SetValue(ob, xmlTypeMapMemberExpandable4.ChoiceMember, obj10);
					}
				}
			}
			this.SetListMembersDefaults(map, ob, isValueList);
		}

		// Token: 0x06001ECD RID: 7885 RVA: 0x000B1830 File Offset: 0x000AFA30
		private void SetListMembersDefaults(ClassMap map, object ob, bool isValueList)
		{
			if (map.ListMembers != null)
			{
				ArrayList listMembers = map.ListMembers;
				for (int i = 0; i < listMembers.Count; i++)
				{
					XmlTypeMapMember xmlTypeMapMember = (XmlTypeMapMember)listMembers[i];
					if (!this.IsReadOnly(xmlTypeMapMember, xmlTypeMapMember.TypeData, ob, isValueList) && this.GetMemberValue(xmlTypeMapMember, ob, isValueList) == null)
					{
						this.SetMemberValue(xmlTypeMapMember, ob, this.InitializeList(xmlTypeMapMember.TypeData), isValueList);
					}
				}
			}
		}

		// Token: 0x06001ECE RID: 7886 RVA: 0x000B189C File Offset: 0x000AFA9C
		internal void FixupMembers(ClassMap map, object obfixup, bool isValueList)
		{
			XmlSerializationReader.Fixup fixup = (XmlSerializationReader.Fixup)obfixup;
			IEnumerable elementMembers = map.ElementMembers;
			string[] ids = fixup.Ids;
			foreach (object obj in elementMembers)
			{
				XmlTypeMapMember xmlTypeMapMember = (XmlTypeMapMember)obj;
				if (ids[xmlTypeMapMember.Index] != null)
				{
					this.SetMemberValue(xmlTypeMapMember, fixup.Source, base.GetTarget(ids[xmlTypeMapMember.Index]), isValueList);
				}
			}
		}

		// Token: 0x06001ECF RID: 7887 RVA: 0x000B1928 File Offset: 0x000AFB28
		protected virtual void ProcessUnknownAttribute(object target)
		{
			base.UnknownNode(target);
		}

		// Token: 0x06001ED0 RID: 7888 RVA: 0x000B1928 File Offset: 0x000AFB28
		protected virtual void ProcessUnknownElement(object target)
		{
			base.UnknownNode(target);
		}

		// Token: 0x06001ED1 RID: 7889 RVA: 0x000B1931 File Offset: 0x000AFB31
		private bool IsReadOnly(XmlTypeMapMember member, TypeData memType, object ob, bool isValueList)
		{
			if (isValueList)
			{
				return !memType.HasPublicConstructor;
			}
			return member.IsReadOnly(ob.GetType()) || !memType.HasPublicConstructor;
		}

		// Token: 0x06001ED2 RID: 7890 RVA: 0x000B195C File Offset: 0x000AFB5C
		private void SetMemberValue(XmlTypeMapMember member, object ob, object value, bool isValueList)
		{
			Type type = member.TypeData.Type;
			if (value != null && !value.GetType().IsAssignableFrom(type))
			{
				value = XmlSerializationWriterInterpreter.ImplicitConvert(value, type);
			}
			if (isValueList)
			{
				((object[])ob)[member.GlobalIndex] = value;
			}
			else
			{
				member.SetValue(ob, value);
			}
			if (member.IsOptionalValueType)
			{
				member.SetValueSpecified(ob, true);
			}
		}

		// Token: 0x06001ED3 RID: 7891 RVA: 0x000B19BB File Offset: 0x000AFBBB
		private void SetMemberValueFromAttr(XmlTypeMapMember member, object ob, object value, bool isValueList)
		{
			if (member.TypeData.Type.IsEnum)
			{
				value = Enum.ToObject(member.TypeData.Type, value);
			}
			this.SetMemberValue(member, ob, value, isValueList);
		}

		// Token: 0x06001ED4 RID: 7892 RVA: 0x000B19ED File Offset: 0x000AFBED
		private object GetMemberValue(XmlTypeMapMember member, object ob, bool isValueList)
		{
			if (isValueList)
			{
				return ((object[])ob)[member.GlobalIndex];
			}
			return member.GetValue(ob);
		}

		// Token: 0x06001ED5 RID: 7893 RVA: 0x000B1A08 File Offset: 0x000AFC08
		private object ReadObjectElement(XmlTypeMapElementInfo elem)
		{
			switch (elem.TypeData.SchemaType)
			{
			case SchemaTypes.Primitive:
			case SchemaTypes.Enum:
				return this.ReadPrimitiveValue(elem);
			case SchemaTypes.Array:
				return this.ReadListElement(elem.MappedType, elem.IsNullable, null, true);
			case SchemaTypes.Class:
				return this.ReadObject(elem.MappedType, elem.IsNullable, true);
			case SchemaTypes.XmlSerializable:
			{
				object obj = XmlSerializationReaderInterpreter.CreateInstance(elem.TypeData.Type, true);
				return base.ReadSerializable((IXmlSerializable)obj);
			}
			case SchemaTypes.XmlNode:
				return this.ReadXmlNode(elem.TypeData, true);
			default:
				throw new NotSupportedException("Invalid value type");
			}
		}

		// Token: 0x06001ED6 RID: 7894 RVA: 0x000B1AAC File Offset: 0x000AFCAC
		private object ReadPrimitiveValue(XmlTypeMapElementInfo elem)
		{
			if (elem.TypeData.Type == typeof(XmlQualifiedName))
			{
				if (elem.IsNullable)
				{
					return base.ReadNullableQualifiedName();
				}
				return base.ReadElementQualifiedName();
			}
			else
			{
				if (elem.IsNullable)
				{
					return this.GetValueFromXmlString(base.ReadNullableString(), elem.TypeData, elem.MappedType);
				}
				return this.GetValueFromXmlString(base.Reader.ReadElementString(), elem.TypeData, elem.MappedType);
			}
		}

		// Token: 0x06001ED7 RID: 7895 RVA: 0x000B1B2C File Offset: 0x000AFD2C
		private object GetValueFromXmlString(string value, TypeData typeData, XmlTypeMapping typeMap)
		{
			if (typeData.SchemaType == SchemaTypes.Array)
			{
				return this.ReadListString(typeMap, value);
			}
			if (typeData.SchemaType == SchemaTypes.Enum)
			{
				return this.GetEnumValue(typeMap, value);
			}
			if (typeData.Type == typeof(XmlQualifiedName))
			{
				return base.ToXmlQualifiedName(value);
			}
			return XmlCustomFormatter.FromXmlString(typeData, value);
		}

		// Token: 0x06001ED8 RID: 7896 RVA: 0x000B1B84 File Offset: 0x000AFD84
		private object ReadListElement(XmlTypeMapping typeMap, bool isNullable, object list, bool canCreateInstance)
		{
			Type type = typeMap.TypeData.Type;
			ListMap listMap = (ListMap)typeMap.ObjectMap;
			if (type.IsArray && base.ReadNull())
			{
				return null;
			}
			if (list == null)
			{
				if (!canCreateInstance || !typeMap.TypeData.HasPublicConstructor)
				{
					throw base.CreateReadOnlyCollectionException(typeMap.TypeFullName);
				}
				list = this.CreateList(type);
			}
			if (base.Reader.IsEmptyElement)
			{
				base.Reader.Skip();
				if (type.IsArray)
				{
					list = base.ShrinkArray((Array)list, 0, type.GetElementType(), false);
				}
				return list;
			}
			int length = 0;
			base.Reader.ReadStartElement();
			base.Reader.MoveToContent();
			while (base.Reader.NodeType != XmlNodeType.EndElement)
			{
				if (base.Reader.NodeType == XmlNodeType.Element)
				{
					XmlTypeMapElementInfo xmlTypeMapElementInfo = listMap.FindElement(base.Reader.LocalName, base.Reader.NamespaceURI);
					if (xmlTypeMapElementInfo != null)
					{
						this.AddListValue(typeMap.TypeData, ref list, length++, this.ReadObjectElement(xmlTypeMapElementInfo), false);
					}
					else
					{
						base.UnknownNode(null);
					}
				}
				else
				{
					base.UnknownNode(null);
				}
				base.Reader.MoveToContent();
			}
			base.ReadEndElement();
			if (type.IsArray)
			{
				list = base.ShrinkArray((Array)list, length, type.GetElementType(), false);
			}
			return list;
		}

		// Token: 0x06001ED9 RID: 7897 RVA: 0x000B1CD8 File Offset: 0x000AFED8
		private object ReadListString(XmlTypeMapping typeMap, string values)
		{
			Type type = typeMap.TypeData.Type;
			ListMap listMap = (ListMap)typeMap.ObjectMap;
			values = values.Trim();
			if (values == string.Empty)
			{
				return Array.CreateInstance(type.GetElementType(), 0);
			}
			string[] array = values.Split(new char[]
			{
				' '
			});
			Array array2 = Array.CreateInstance(type.GetElementType(), array.Length);
			XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)listMap.ItemInfo[0];
			for (int i = 0; i < array.Length; i++)
			{
				array2.SetValue(this.GetValueFromXmlString(array[i], xmlTypeMapElementInfo.TypeData, xmlTypeMapElementInfo.MappedType), i);
			}
			return array2;
		}

		// Token: 0x06001EDA RID: 7898 RVA: 0x000B1D88 File Offset: 0x000AFF88
		private void AddListValue(TypeData listType, ref object list, int index, object value, bool canCreateInstance)
		{
			Type type = listType.Type;
			if (type.IsArray)
			{
				list = base.EnsureArrayIndex((Array)list, index, type.GetElementType());
				listType.ConvertForAssignment(ref value);
				((Array)list).SetValue(value, index);
				return;
			}
			if (list == null)
			{
				if (!canCreateInstance)
				{
					throw base.CreateReadOnlyCollectionException(type.FullName);
				}
				list = XmlSerializationReaderInterpreter.CreateInstance(type, true);
			}
			type.GetMethod("Add", new Type[]
			{
				listType.ListItemType
			}).Invoke(list, new object[]
			{
				value
			});
		}

		// Token: 0x06001EDB RID: 7899 RVA: 0x000B1E1F File Offset: 0x000B001F
		private static object CreateInstance(Type type, bool nonPublic)
		{
			return Activator.CreateInstance(type, nonPublic);
		}

		// Token: 0x06001EDC RID: 7900 RVA: 0x000B1E28 File Offset: 0x000B0028
		private object CreateInstance(Type type)
		{
			return Activator.CreateInstance(type, XmlSerializationReaderInterpreter.empty_array);
		}

		// Token: 0x06001EDD RID: 7901 RVA: 0x000B1E35 File Offset: 0x000B0035
		private object CreateList(Type listType)
		{
			if (listType.IsArray)
			{
				return base.EnsureArrayIndex(null, 0, listType.GetElementType());
			}
			return XmlSerializationReaderInterpreter.CreateInstance(listType, true);
		}

		// Token: 0x06001EDE RID: 7902 RVA: 0x000B1E55 File Offset: 0x000B0055
		private object InitializeList(TypeData listType)
		{
			if (listType.Type.IsArray)
			{
				return null;
			}
			return XmlSerializationReaderInterpreter.CreateInstance(listType.Type, true);
		}

		// Token: 0x06001EDF RID: 7903 RVA: 0x000B1E72 File Offset: 0x000B0072
		private void FillList(object list, object items)
		{
			this.CopyEnumerableList(items, list);
		}

		// Token: 0x06001EE0 RID: 7904 RVA: 0x000B1E7C File Offset: 0x000B007C
		private void CopyEnumerableList(object source, object dest)
		{
			if (dest == null)
			{
				throw base.CreateReadOnlyCollectionException(source.GetType().FullName);
			}
			object[] array = new object[1];
			MethodInfo method = dest.GetType().GetMethod("Add");
			foreach (object obj in ((IEnumerable)source))
			{
				array[0] = obj;
				method.Invoke(dest, array);
			}
		}

		// Token: 0x06001EE1 RID: 7905 RVA: 0x000B1F08 File Offset: 0x000B0108
		private object ReadXmlNodeElement(XmlTypeMapping typeMap, bool isNullable)
		{
			return this.ReadXmlNode(typeMap.TypeData, false);
		}

		// Token: 0x06001EE2 RID: 7906 RVA: 0x000B1F17 File Offset: 0x000B0117
		private object ReadXmlNode(TypeData type, bool wrapped)
		{
			if (type.Type == typeof(XmlDocument))
			{
				return base.ReadXmlDocument(wrapped);
			}
			return base.ReadXmlNode(wrapped);
		}

		// Token: 0x06001EE3 RID: 7907 RVA: 0x000B1F40 File Offset: 0x000B0140
		private object ReadPrimitiveElement(XmlTypeMapping typeMap, bool isNullable)
		{
			XmlQualifiedName xmlQualifiedName = base.GetXsiType();
			if (xmlQualifiedName == null)
			{
				xmlQualifiedName = new XmlQualifiedName(typeMap.XmlType, typeMap.Namespace);
			}
			return base.ReadTypedPrimitive(xmlQualifiedName);
		}

		// Token: 0x06001EE4 RID: 7908 RVA: 0x000B1F76 File Offset: 0x000B0176
		private object ReadEnumElement(XmlTypeMapping typeMap, bool isNullable)
		{
			base.Reader.ReadStartElement();
			object enumValue = this.GetEnumValue(typeMap, base.Reader.ReadString());
			base.ReadEndElement();
			return enumValue;
		}

		// Token: 0x06001EE5 RID: 7909 RVA: 0x000B1F9C File Offset: 0x000B019C
		private object GetEnumValue(XmlTypeMapping typeMap, string val)
		{
			if (val == null)
			{
				return null;
			}
			string enumName = ((EnumMap)typeMap.ObjectMap).GetEnumName(typeMap.TypeFullName, val);
			if (enumName == null)
			{
				throw base.CreateUnknownConstantException(val, typeMap.TypeData.Type);
			}
			return Enum.Parse(typeMap.TypeData.Type, enumName, false);
		}

		// Token: 0x06001EE6 RID: 7910 RVA: 0x000B1FF0 File Offset: 0x000B01F0
		private object ReadXmlSerializableElement(XmlTypeMapping typeMap, bool isNullable)
		{
			base.Reader.MoveToContent();
			if (base.Reader.NodeType != XmlNodeType.Element)
			{
				base.UnknownNode(null);
				return null;
			}
			if (typeMap.IsAny || (base.Reader.LocalName == typeMap.ElementName && base.Reader.NamespaceURI == typeMap.Namespace))
			{
				object obj = XmlSerializationReaderInterpreter.CreateInstance(typeMap.TypeData.Type, true);
				return base.ReadSerializable((IXmlSerializable)obj);
			}
			throw base.CreateUnknownNodeException();
		}

		// Token: 0x06001EE7 RID: 7911 RVA: 0x000B207D File Offset: 0x000B027D
		// Note: this type is marked as 'beforefieldinit'.
		static XmlSerializationReaderInterpreter()
		{
		}

		// Token: 0x040016F5 RID: 5877
		private XmlMapping _typeMap;

		// Token: 0x040016F6 RID: 5878
		private SerializationFormat _format;

		// Token: 0x040016F7 RID: 5879
		private static readonly XmlQualifiedName AnyType = new XmlQualifiedName("anyType", "http://www.w3.org/2001/XMLSchema");

		// Token: 0x040016F8 RID: 5880
		private static readonly object[] empty_array = new object[0];

		// Token: 0x02000330 RID: 816
		private class FixupCallbackInfo
		{
			// Token: 0x06001EE8 RID: 7912 RVA: 0x000B209E File Offset: 0x000B029E
			public FixupCallbackInfo(XmlSerializationReaderInterpreter sri, ClassMap map, bool isValueList)
			{
				this._sri = sri;
				this._map = map;
				this._isValueList = isValueList;
			}

			// Token: 0x06001EE9 RID: 7913 RVA: 0x000B20BB File Offset: 0x000B02BB
			public void FixupMembers(object fixup)
			{
				this._sri.FixupMembers(this._map, fixup, this._isValueList);
			}

			// Token: 0x040016F9 RID: 5881
			private XmlSerializationReaderInterpreter _sri;

			// Token: 0x040016FA RID: 5882
			private ClassMap _map;

			// Token: 0x040016FB RID: 5883
			private bool _isValueList;
		}

		// Token: 0x02000331 RID: 817
		private class ReaderCallbackInfo
		{
			// Token: 0x06001EEA RID: 7914 RVA: 0x000B20D5 File Offset: 0x000B02D5
			public ReaderCallbackInfo(XmlSerializationReaderInterpreter sri, XmlTypeMapping typeMap)
			{
				this._sri = sri;
				this._typeMap = typeMap;
			}

			// Token: 0x06001EEB RID: 7915 RVA: 0x000B20EB File Offset: 0x000B02EB
			internal object ReadObject()
			{
				return this._sri.ReadObject(this._typeMap, true, true);
			}

			// Token: 0x040016FC RID: 5884
			private XmlSerializationReaderInterpreter _sri;

			// Token: 0x040016FD RID: 5885
			private XmlTypeMapping _typeMap;
		}
	}
}
