﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>When applied to a type, stores the name of a static method of the type that returns an XML schema and a <see cref="T:System.Xml.XmlQualifiedName" /> (or <see cref="T:System.Xml.Schema.XmlSchemaType" /> for anonymous types) that controls the serialization of the type.</summary>
	// Token: 0x02000325 RID: 805
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface)]
	public sealed class XmlSchemaProviderAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute" /> class, taking the name of the static method that supplies the type's XML schema.</summary>
		/// <param name="methodName">The name of the static method that must be implemented.</param>
		// Token: 0x06001E44 RID: 7748 RVA: 0x000AEBAF File Offset: 0x000ACDAF
		public XmlSchemaProviderAttribute(string methodName)
		{
			this._methodName = methodName;
		}

		/// <summary>Gets the name of the static method that supplies the type's XML schema and the name of its XML Schema data type.</summary>
		/// <returns>The name of the method that is invoked by the XML infrastructure to return an XML schema.</returns>
		// Token: 0x170005D2 RID: 1490
		// (get) Token: 0x06001E45 RID: 7749 RVA: 0x000AEBBE File Offset: 0x000ACDBE
		public string MethodName
		{
			get
			{
				return this._methodName;
			}
		}

		/// <summary>Gets or sets a value that determines whether the target class is a wildcard, or that the schema for the class has contains only an <see langword="xs:any" /> element.</summary>
		/// <returns>
		///     <see langword="true" />, if the class is a wildcard, or if the schema contains only the <see langword="xs:any" /> element; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005D3 RID: 1491
		// (get) Token: 0x06001E46 RID: 7750 RVA: 0x000AEBC6 File Offset: 0x000ACDC6
		// (set) Token: 0x06001E47 RID: 7751 RVA: 0x000AEBCE File Offset: 0x000ACDCE
		public bool IsAny
		{
			get
			{
				return this._isAny;
			}
			set
			{
				this._isAny = value;
			}
		}

		// Token: 0x040016CC RID: 5836
		private string _methodName;

		// Token: 0x040016CD RID: 5837
		private bool _isAny;
	}
}
