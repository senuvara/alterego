﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Allows the <see cref="T:System.Xml.Serialization.XmlSerializer" /> to recognize a type when it serializes or deserializes an object.</summary>
	// Token: 0x02000314 RID: 788
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Interface, AllowMultiple = true)]
	public class XmlIncludeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlIncludeAttribute" /> class.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of the object to include. </param>
		// Token: 0x06001D40 RID: 7488 RVA: 0x000A6DE8 File Offset: 0x000A4FE8
		public XmlIncludeAttribute(Type type)
		{
			this.type = type;
		}

		/// <summary>Gets or sets the type of the object to include.</summary>
		/// <returns>The <see cref="T:System.Type" /> of the object to include.</returns>
		// Token: 0x170005A8 RID: 1448
		// (get) Token: 0x06001D41 RID: 7489 RVA: 0x000A6DF7 File Offset: 0x000A4FF7
		// (set) Token: 0x06001D42 RID: 7490 RVA: 0x000A6DFF File Offset: 0x000A4FFF
		public Type Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x04001679 RID: 5753
		private Type type;
	}
}
