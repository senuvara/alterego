﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x020002D1 RID: 721
	internal class XmlAttributeComparer : IComparer
	{
		// Token: 0x06001A99 RID: 6809 RVA: 0x00094E78 File Offset: 0x00093078
		public int Compare(object o1, object o2)
		{
			XmlAttribute xmlAttribute = (XmlAttribute)o1;
			XmlAttribute xmlAttribute2 = (XmlAttribute)o2;
			int num = string.Compare(xmlAttribute.NamespaceURI, xmlAttribute2.NamespaceURI, StringComparison.Ordinal);
			if (num == 0)
			{
				return string.Compare(xmlAttribute.Name, xmlAttribute2.Name, StringComparison.Ordinal);
			}
			return num;
		}

		// Token: 0x06001A9A RID: 6810 RVA: 0x00002103 File Offset: 0x00000303
		public XmlAttributeComparer()
		{
		}
	}
}
