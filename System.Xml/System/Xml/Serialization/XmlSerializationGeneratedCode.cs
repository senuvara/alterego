﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>An abstract class that is the base class for <see cref="T:System.Xml.Serialization.XmlSerializationReader" /> and <see cref="T:System.Xml.Serialization.XmlSerializationWriter" /> and that contains methods common to both of these types.</summary>
	// Token: 0x02000328 RID: 808
	public abstract class XmlSerializationGeneratedCode
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Xml.Serialization.XmlSerializationGeneratedCode" /> class. </summary>
		// Token: 0x06001E50 RID: 7760 RVA: 0x00002103 File Offset: 0x00000303
		protected XmlSerializationGeneratedCode()
		{
		}
	}
}
