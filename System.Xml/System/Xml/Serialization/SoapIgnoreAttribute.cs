﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Instructs the <see cref="T:System.Xml.Serialization.XmlSerializer" /> not to serialize the public field or public read/write property value.</summary>
	// Token: 0x020002FB RID: 763
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class SoapIgnoreAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.SoapIgnoreAttribute" /> class.</summary>
		// Token: 0x06001C13 RID: 7187 RVA: 0x0000D3B3 File Offset: 0x0000B5B3
		public SoapIgnoreAttribute()
		{
		}
	}
}
