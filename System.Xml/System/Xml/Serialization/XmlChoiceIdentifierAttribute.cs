﻿using System;
using System.Reflection;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Specifies that the member can be further detected by using an enumeration.</summary>
	// Token: 0x0200030D RID: 781
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class XmlChoiceIdentifierAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlChoiceIdentifierAttribute" /> class.</summary>
		// Token: 0x06001CEE RID: 7406 RVA: 0x0000D3B3 File Offset: 0x0000B5B3
		public XmlChoiceIdentifierAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlChoiceIdentifierAttribute" /> class.</summary>
		/// <param name="name">The member name that returns the enumeration used to detect a choice. </param>
		// Token: 0x06001CEF RID: 7407 RVA: 0x000A5371 File Offset: 0x000A3571
		public XmlChoiceIdentifierAttribute(string name)
		{
			this.memberName = name;
		}

		/// <summary>Gets or sets the name of the field that returns the enumeration to use when detecting types.</summary>
		/// <returns>The name of a field that returns an enumeration.</returns>
		// Token: 0x17000597 RID: 1431
		// (get) Token: 0x06001CF0 RID: 7408 RVA: 0x000A5380 File Offset: 0x000A3580
		// (set) Token: 0x06001CF1 RID: 7409 RVA: 0x000A5396 File Offset: 0x000A3596
		public string MemberName
		{
			get
			{
				if (this.memberName == null)
				{
					return string.Empty;
				}
				return this.memberName;
			}
			set
			{
				this.memberName = value;
			}
		}

		// Token: 0x17000598 RID: 1432
		// (get) Token: 0x06001CF2 RID: 7410 RVA: 0x000A539F File Offset: 0x000A359F
		// (set) Token: 0x06001CF3 RID: 7411 RVA: 0x000A53A7 File Offset: 0x000A35A7
		internal MemberInfo MemberInfo
		{
			get
			{
				return this.member;
			}
			set
			{
				this.MemberName = ((value != null) ? value.Name : null);
				this.member = value;
			}
		}

		// Token: 0x06001CF4 RID: 7412 RVA: 0x000A53C8 File Offset: 0x000A35C8
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XCA ");
			KeyHelper.AddField(sb, 1, this.memberName);
			sb.Append('|');
		}

		// Token: 0x04001669 RID: 5737
		private string memberName;

		// Token: 0x0400166A RID: 5738
		private MemberInfo member;
	}
}
