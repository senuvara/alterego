﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Represents the method that handles the <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownAttribute" /></summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">An <see cref="T:System.Xml.Serialization.XmlAttributeEventArgs" /> that contains the event data. </param>
	// Token: 0x020002D9 RID: 729
	// (Invoke) Token: 0x06001B19 RID: 6937
	public delegate void XmlAttributeEventHandler(object sender, XmlAttributeEventArgs e);
}
