﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002F0 RID: 752
	internal enum HookAction
	{
		// Token: 0x040015FC RID: 5628
		InsertBefore,
		// Token: 0x040015FD RID: 5629
		InsertAfter,
		// Token: 0x040015FE RID: 5630
		Replace
	}
}
