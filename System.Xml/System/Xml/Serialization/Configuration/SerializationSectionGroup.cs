﻿using System;
using System.Configuration;
using Unity;

namespace System.Xml.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure XML serialization.</summary>
	// Token: 0x02000574 RID: 1396
	public sealed class SerializationSectionGroup : ConfigurationSectionGroup
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.SerializationSectionGroup" /> class.</summary>
		// Token: 0x06003483 RID: 13443 RVA: 0x00072668 File Offset: 0x00070868
		public SerializationSectionGroup()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the object that represents the <see cref="T:System.DateTime" /> serialization configuration element.</summary>
		/// <returns>The <see cref="T:System.Xml.Serialization.Configuration.DateTimeSerializationSection" /> object that represents the configuration element.</returns>
		// Token: 0x17000AF4 RID: 2804
		// (get) Token: 0x06003484 RID: 13444 RVA: 0x000A7058 File Offset: 0x000A5258
		public DateTimeSerializationSection DateTimeSerialization
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object that represents the section that contains configuration elements for the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" />.</summary>
		/// <returns>The <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionsSection" /> that represents the <see langword="schemaImporterExtenstion" /> element in the configuration file.</returns>
		// Token: 0x17000AF5 RID: 2805
		// (get) Token: 0x06003485 RID: 13445 RVA: 0x000A7058 File Offset: 0x000A5258
		public SchemaImporterExtensionsSection SchemaImporterExtensions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object that represents the configuration group for the <see cref="T:System.Xml.Serialization.XmlSerializer" />.</summary>
		/// <returns>The <see cref="T:System.Xml.Serialization.Configuration.XmlSerializerSection" /> that represents the <see cref="T:System.Xml.Serialization.XmlSerializer" />.</returns>
		// Token: 0x17000AF6 RID: 2806
		// (get) Token: 0x06003486 RID: 13446 RVA: 0x000A7058 File Offset: 0x000A5258
		public XmlSerializerSection XmlSerializer
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
