﻿using System;
using System.Configuration;
using Unity;

namespace System.Xml.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure XML serialization. </summary>
	// Token: 0x02000575 RID: 1397
	public sealed class XmlSerializerSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.XmlSerializerSection" /> class. </summary>
		// Token: 0x06003487 RID: 13447 RVA: 0x00072668 File Offset: 0x00070868
		public XmlSerializerSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a value that determines whether an additional check of progress of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> is done.</summary>
		/// <returns>
		///     <see langword="true" /> if the check is made; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000AF7 RID: 2807
		// (get) Token: 0x06003488 RID: 13448 RVA: 0x0010F484 File Offset: 0x0010D684
		// (set) Token: 0x06003489 RID: 13449 RVA: 0x00072668 File Offset: 0x00070868
		public bool CheckDeserializeAdvances
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000AF8 RID: 2808
		// (get) Token: 0x0600348A RID: 13450 RVA: 0x000A7058 File Offset: 0x000A5258
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns the location that was specified for the creation of the temporary file.</summary>
		/// <returns>The location that was specified for the creation of the temporary file.</returns>
		// Token: 0x17000AF9 RID: 2809
		// (get) Token: 0x0600348B RID: 13451 RVA: 0x000A7058 File Offset: 0x000A5258
		// (set) Token: 0x0600348C RID: 13452 RVA: 0x00072668 File Offset: 0x00070868
		public string TempFilesLocation
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Gets or sets a value that indicates whether the specified object uses legacy serializer generation.</summary>
		/// <returns>
		///     <see langword="true" /> if the object uses legacy serializer generation; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000AFA RID: 2810
		// (get) Token: 0x0600348D RID: 13453 RVA: 0x0010F4A0 File Offset: 0x0010D6A0
		// (set) Token: 0x0600348E RID: 13454 RVA: 0x00072668 File Offset: 0x00070868
		public bool UseLegacySerializerGeneration
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(bool);
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
