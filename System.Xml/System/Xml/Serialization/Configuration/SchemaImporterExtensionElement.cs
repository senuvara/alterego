﻿using System;
using System.Configuration;
using Unity;

namespace System.Xml.Serialization.Configuration
{
	/// <summary>Handles the configuration for the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" /> class. This class cannot be inherited.</summary>
	// Token: 0x02000571 RID: 1393
	public sealed class SchemaImporterExtensionElement : ConfigurationElement
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElement" /> class.</summary>
		// Token: 0x0600346A RID: 13418 RVA: 0x00072668 File Offset: 0x00070868
		public SchemaImporterExtensionElement()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElement" /> class and specifies the name and type of the extension.</summary>
		/// <param name="name">The name of the new extension. The name must be unique.</param>
		/// <param name="type">The type of the new extension, specified as a string.</param>
		// Token: 0x0600346B RID: 13419 RVA: 0x00072668 File Offset: 0x00070868
		public SchemaImporterExtensionElement(string name, string type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElement" /> class using the specified name and type.</summary>
		/// <param name="name">The name of the new extension. The name must be unique.</param>
		/// <param name="type">The <see cref="T:System.Type" /> of the new extension.</param>
		// Token: 0x0600346C RID: 13420 RVA: 0x00072668 File Offset: 0x00070868
		public SchemaImporterExtensionElement(string name, Type type)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the name of the extension.</summary>
		/// <returns>The name of the extension.</returns>
		// Token: 0x17000AEE RID: 2798
		// (get) Token: 0x0600346D RID: 13421 RVA: 0x000A7058 File Offset: 0x000A5258
		// (set) Token: 0x0600346E RID: 13422 RVA: 0x00072668 File Offset: 0x00070868
		public string Name
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000AEF RID: 2799
		// (get) Token: 0x0600346F RID: 13423 RVA: 0x000A7058 File Offset: 0x000A5258
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets or sets the type of the extension.</summary>
		/// <returns>A type of the extension.</returns>
		// Token: 0x17000AF0 RID: 2800
		// (get) Token: 0x06003470 RID: 13424 RVA: 0x000A7058 File Offset: 0x000A5258
		// (set) Token: 0x06003471 RID: 13425 RVA: 0x00072668 File Offset: 0x00070868
		public Type Type
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
