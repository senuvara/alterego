﻿using System;
using System.Configuration;
using Unity;

namespace System.Xml.Serialization.Configuration
{
	/// <summary>Handles the XML elements used to configure the operation of the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" />. This class cannot be inherited.</summary>
	// Token: 0x02000572 RID: 1394
	[ConfigurationCollection(typeof(SchemaImporterExtensionElement))]
	public sealed class SchemaImporterExtensionElementCollection : ConfigurationElementCollection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElementCollection" /> class.</summary>
		// Token: 0x06003472 RID: 13426 RVA: 0x00072668 File Offset: 0x00070868
		public SchemaImporterExtensionElementCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003473 RID: 13427 RVA: 0x000A7058 File Offset: 0x000A5258
		public SchemaImporterExtensionElement get_Item(int index)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06003474 RID: 13428 RVA: 0x00072668 File Offset: 0x00070868
		public void set_Item(int index, SchemaImporterExtensionElement value)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets the item with the specified name.</summary>
		/// <param name="name">The name of the item to get or set.</param>
		/// <returns>The <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElement" /> with the specified name.</returns>
		// Token: 0x17000AF1 RID: 2801
		public SchemaImporterExtensionElement this[string name]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds an item to the end of the collection.</summary>
		/// <param name="element">The <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElement" /> to add to the collection.</param>
		// Token: 0x06003477 RID: 13431 RVA: 0x00072668 File Offset: 0x00070868
		public void Add(SchemaImporterExtensionElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes all items from the collection.</summary>
		// Token: 0x06003478 RID: 13432 RVA: 0x00072668 File Offset: 0x00070868
		public void Clear()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06003479 RID: 13433 RVA: 0x000A7058 File Offset: 0x000A5258
		protected override ConfigurationElement CreateNewElement()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600347A RID: 13434 RVA: 0x000A7058 File Offset: 0x000A5258
		protected override object GetElementKey(ConfigurationElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the zero-based index of the first element in the collection with the specified value.</summary>
		/// <param name="element">The <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElement" /> to find.</param>
		/// <returns>The index of the found element.</returns>
		// Token: 0x0600347B RID: 13435 RVA: 0x0010F468 File Offset: 0x0010D668
		public int IndexOf(SchemaImporterExtensionElement element)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Removes the item with the specified name from the collection.</summary>
		/// <param name="name">The name of the item to remove.</param>
		// Token: 0x0600347C RID: 13436 RVA: 0x00072668 File Offset: 0x00070868
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the first occurrence of a specific item from the collection.</summary>
		/// <param name="element">The <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElement" /> to remove.</param>
		// Token: 0x0600347D RID: 13437 RVA: 0x00072668 File Offset: 0x00070868
		public void Remove(SchemaImporterExtensionElement element)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the item at the specified index from the collection.</summary>
		/// <param name="index">The index of the object to remove.</param>
		// Token: 0x0600347E RID: 13438 RVA: 0x00072668 File Offset: 0x00070868
		public void RemoveAt(int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
