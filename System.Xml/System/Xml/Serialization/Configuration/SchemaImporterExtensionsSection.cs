﻿using System;
using System.Configuration;
using Unity;

namespace System.Xml.Serialization.Configuration
{
	/// <summary>Handles the configuration for the <see cref="T:System.Xml.Serialization.XmlSchemaImporter" /> class. This class cannot be inherited.</summary>
	// Token: 0x02000573 RID: 1395
	public sealed class SchemaImporterExtensionsSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionsSection" /> class.</summary>
		// Token: 0x0600347F RID: 13439 RVA: 0x00072668 File Offset: 0x00070868
		public SchemaImporterExtensionsSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x17000AF2 RID: 2802
		// (get) Token: 0x06003480 RID: 13440 RVA: 0x000A7058 File Offset: 0x000A5258
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Gets the object that represents the collection of extensions.</summary>
		/// <returns>A <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElementCollection" /> that contains the objects that represent configuration elements.</returns>
		// Token: 0x17000AF3 RID: 2803
		// (get) Token: 0x06003481 RID: 13441 RVA: 0x000A7058 File Offset: 0x000A5258
		public SchemaImporterExtensionElementCollection SchemaImporterExtensions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x06003482 RID: 13442 RVA: 0x00072668 File Offset: 0x00070868
		protected override void InitializeDefault()
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
