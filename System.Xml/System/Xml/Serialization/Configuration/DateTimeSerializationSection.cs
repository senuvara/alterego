﻿using System;
using System.Configuration;
using Unity;

namespace System.Xml.Serialization.Configuration
{
	/// <summary>Handles configuration settings for XML serialization of <see cref="T:System.DateTime" /> instances.</summary>
	// Token: 0x0200056E RID: 1390
	public sealed class DateTimeSerializationSection : ConfigurationSection
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.DateTimeSerializationSection" /> class.</summary>
		// Token: 0x06003463 RID: 13411 RVA: 0x00072668 File Offset: 0x00070868
		public DateTimeSerializationSection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets or sets a value that determines the serialization format.</summary>
		/// <returns>One of the <see cref="T:System.Xml.Serialization.Configuration.DateTimeSerializationSection.DateTimeSerializationMode" /> values.</returns>
		// Token: 0x17000AEC RID: 2796
		// (get) Token: 0x06003464 RID: 13412 RVA: 0x0010F430 File Offset: 0x0010D630
		// (set) Token: 0x06003465 RID: 13413 RVA: 0x00072668 File Offset: 0x00070868
		public DateTimeSerializationSection.DateTimeSerializationMode Mode
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return DateTimeSerializationSection.DateTimeSerializationMode.Default;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		// Token: 0x17000AED RID: 2797
		// (get) Token: 0x06003466 RID: 13414 RVA: 0x000A7058 File Offset: 0x000A5258
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Determines XML serialization format of <see cref="T:System.DateTime" /> objects.</summary>
		// Token: 0x0200056F RID: 1391
		public enum DateTimeSerializationMode
		{
			/// <summary>Same as <see langword="Roundtrip" />.</summary>
			// Token: 0x04002380 RID: 9088
			Default,
			/// <summary>The serializer formats all <see cref="T:System.DateTime" /> objects as local time. This is for version 1.0 and 1.1 compatibility.</summary>
			// Token: 0x04002381 RID: 9089
			Local = 2,
			/// <summary>The serializer examines individual <see cref="T:System.DateTime" /> instances to determine the serialization format: UTC, local, or unspecified.</summary>
			// Token: 0x04002382 RID: 9090
			Roundtrip = 1
		}
	}
}
