﻿using System;
using System.Configuration;
using Unity;

namespace System.Xml.Serialization.Configuration
{
	/// <summary>Validates the rules governing the use of the tempFilesLocation configuration switch. </summary>
	// Token: 0x02000570 RID: 1392
	public class RootedPathValidator : ConfigurationValidatorBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Configuration.RootedPathValidator" /> class. </summary>
		// Token: 0x06003467 RID: 13415 RVA: 0x00072668 File Offset: 0x00070868
		public RootedPathValidator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Determines whether the type of the object can be validated.</summary>
		/// <param name="type">The type of the object.</param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="type" /> parameter matches a valid <see langword="XMLSerializer" /> object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003468 RID: 13416 RVA: 0x0010F44C File Offset: 0x0010D64C
		public override bool CanValidate(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether the value of an object is valid.</summary>
		/// <param name="value">The value of an object.</param>
		// Token: 0x06003469 RID: 13417 RVA: 0x00072668 File Offset: 0x00070868
		public override void Validate(object value)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
