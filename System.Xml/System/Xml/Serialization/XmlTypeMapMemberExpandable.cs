﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000346 RID: 838
	internal class XmlTypeMapMemberExpandable : XmlTypeMapMemberElement
	{
		// Token: 0x17000623 RID: 1571
		// (get) Token: 0x0600204D RID: 8269 RVA: 0x000B5B88 File Offset: 0x000B3D88
		// (set) Token: 0x0600204E RID: 8270 RVA: 0x000B5B90 File Offset: 0x000B3D90
		public int FlatArrayIndex
		{
			get
			{
				return this._flatArrayIndex;
			}
			set
			{
				this._flatArrayIndex = value;
			}
		}

		// Token: 0x0600204F RID: 8271 RVA: 0x000B5B80 File Offset: 0x000B3D80
		public XmlTypeMapMemberExpandable()
		{
		}

		// Token: 0x0400175A RID: 5978
		private int _flatArrayIndex;
	}
}
