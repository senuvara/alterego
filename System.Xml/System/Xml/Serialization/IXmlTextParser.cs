﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Establishes a <see cref="P:System.Xml.Serialization.IXmlTextParser.Normalized" /> property for use by the .NET Framework infrastructure.</summary>
	// Token: 0x020002E4 RID: 740
	public interface IXmlTextParser
	{
		/// <summary>Gets or sets whether white space and attribute values are normalized.</summary>
		/// <returns>
		///     <see langword="true" /> if white space attributes values are normalized; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x06001B49 RID: 6985
		// (set) Token: 0x06001B4A RID: 6986
		bool Normalized { get; set; }

		/// <summary>Gets or sets how white space is handled when parsing XML.</summary>
		/// <returns>A member of the <see cref="T:System.Xml.WhitespaceHandling" /> enumeration that describes how whites pace is handled when parsing XML.</returns>
		// Token: 0x1700053B RID: 1339
		// (get) Token: 0x06001B4B RID: 6987
		// (set) Token: 0x06001B4C RID: 6988
		WhitespaceHandling WhitespaceHandling { get; set; }
	}
}
