﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Provides data for the known, but unreferenced, object found in an encoded SOAP XML stream during deserialization.</summary>
	// Token: 0x020002E0 RID: 736
	public class UnreferencedObjectEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.UnreferencedObjectEventArgs" /> class.</summary>
		/// <param name="o">The unreferenced object. </param>
		/// <param name="id">A unique string used to identify the unreferenced object. </param>
		// Token: 0x06001B40 RID: 6976 RVA: 0x00098D4C File Offset: 0x00096F4C
		public UnreferencedObjectEventArgs(object o, string id)
		{
			this.o = o;
			this.id = id;
		}

		/// <summary>Gets the deserialized, but unreferenced, object.</summary>
		/// <returns>The deserialized, but unreferenced, object.</returns>
		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x06001B41 RID: 6977 RVA: 0x00098D62 File Offset: 0x00096F62
		public object UnreferencedObject
		{
			get
			{
				return this.o;
			}
		}

		/// <summary>Gets the ID of the object.</summary>
		/// <returns>The ID of the object.</returns>
		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x06001B42 RID: 6978 RVA: 0x00098D6A File Offset: 0x00096F6A
		public string UnreferencedId
		{
			get
			{
				return this.id;
			}
		}

		// Token: 0x040015AD RID: 5549
		private object o;

		// Token: 0x040015AE RID: 5550
		private string id;
	}
}
