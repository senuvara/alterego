﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x02000344 RID: 836
	internal class XmlTypeMapMemberElement : XmlTypeMapMember
	{
		// Token: 0x0600203D RID: 8253 RVA: 0x000B5921 File Offset: 0x000B3B21
		public XmlTypeMapMemberElement()
		{
		}

		// Token: 0x1700061B RID: 1563
		// (get) Token: 0x0600203E RID: 8254 RVA: 0x000B5929 File Offset: 0x000B3B29
		// (set) Token: 0x0600203F RID: 8255 RVA: 0x000B5944 File Offset: 0x000B3B44
		public XmlTypeMapElementInfoList ElementInfo
		{
			get
			{
				if (this._elementInfo == null)
				{
					this._elementInfo = new XmlTypeMapElementInfoList();
				}
				return this._elementInfo;
			}
			set
			{
				this._elementInfo = value;
			}
		}

		// Token: 0x1700061C RID: 1564
		// (get) Token: 0x06002040 RID: 8256 RVA: 0x000B594D File Offset: 0x000B3B4D
		// (set) Token: 0x06002041 RID: 8257 RVA: 0x000B5955 File Offset: 0x000B3B55
		public string ChoiceMember
		{
			get
			{
				return this._choiceMember;
			}
			set
			{
				this._choiceMember = value;
			}
		}

		// Token: 0x1700061D RID: 1565
		// (get) Token: 0x06002042 RID: 8258 RVA: 0x000B595E File Offset: 0x000B3B5E
		// (set) Token: 0x06002043 RID: 8259 RVA: 0x000B5966 File Offset: 0x000B3B66
		public TypeData ChoiceTypeData
		{
			get
			{
				return this._choiceTypeData;
			}
			set
			{
				this._choiceTypeData = value;
			}
		}

		// Token: 0x06002044 RID: 8260 RVA: 0x000B5970 File Offset: 0x000B3B70
		public XmlTypeMapElementInfo FindElement(object ob, object memberValue)
		{
			if (this._elementInfo.Count == 1)
			{
				return (XmlTypeMapElementInfo)this._elementInfo[0];
			}
			if (this._choiceMember != null)
			{
				object value = XmlTypeMapMember.GetValue(ob, this._choiceMember);
				using (IEnumerator enumerator = this._elementInfo.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
						if (xmlTypeMapElementInfo.ChoiceValue != null && xmlTypeMapElementInfo.ChoiceValue.Equals(value))
						{
							return xmlTypeMapElementInfo;
						}
					}
					goto IL_11C;
				}
				goto IL_8D;
				IL_11C:
				return null;
			}
			IL_8D:
			if (memberValue == null)
			{
				return (XmlTypeMapElementInfo)this._elementInfo[0];
			}
			XmlTypeMapElementInfo xmlTypeMapElementInfo2 = null;
			foreach (object obj2 in this._elementInfo)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo3 = (XmlTypeMapElementInfo)obj2;
				if (xmlTypeMapElementInfo3.TypeData.Type.IsInstanceOfType(memberValue) && (xmlTypeMapElementInfo2 == null || xmlTypeMapElementInfo3.TypeData.Type.IsSubclassOf(xmlTypeMapElementInfo2.TypeData.Type)))
				{
					xmlTypeMapElementInfo2 = xmlTypeMapElementInfo3;
				}
			}
			return xmlTypeMapElementInfo2;
		}

		// Token: 0x06002045 RID: 8261 RVA: 0x000B5AB8 File Offset: 0x000B3CB8
		public void SetChoice(object ob, object choice)
		{
			XmlTypeMapMember.SetValue(ob, this._choiceMember, choice);
		}

		// Token: 0x1700061E RID: 1566
		// (get) Token: 0x06002046 RID: 8262 RVA: 0x000B5AC7 File Offset: 0x000B3CC7
		// (set) Token: 0x06002047 RID: 8263 RVA: 0x000B5ACF File Offset: 0x000B3CCF
		public bool IsXmlTextCollector
		{
			get
			{
				return this._isTextCollector;
			}
			set
			{
				this._isTextCollector = value;
			}
		}

		// Token: 0x1700061F RID: 1567
		// (get) Token: 0x06002048 RID: 8264 RVA: 0x000B5AD8 File Offset: 0x000B3CD8
		public override bool RequiresNullable
		{
			get
			{
				using (IEnumerator enumerator = this.ElementInfo.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (((XmlTypeMapElementInfo)enumerator.Current).IsNullable)
						{
							return true;
						}
					}
				}
				return false;
			}
		}

		// Token: 0x04001756 RID: 5974
		private XmlTypeMapElementInfoList _elementInfo;

		// Token: 0x04001757 RID: 5975
		private string _choiceMember;

		// Token: 0x04001758 RID: 5976
		private bool _isTextCollector;

		// Token: 0x04001759 RID: 5977
		private TypeData _choiceTypeData;
	}
}
