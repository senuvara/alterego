﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002EC RID: 748
	[XmlType("configuration")]
	internal class SerializationCodeGeneratorConfiguration
	{
		// Token: 0x06001BD2 RID: 7122 RVA: 0x00002103 File Offset: 0x00000303
		public SerializationCodeGeneratorConfiguration()
		{
		}

		// Token: 0x040015E3 RID: 5603
		[XmlElement("serializer")]
		public SerializerInfo[] Serializers;
	}
}
