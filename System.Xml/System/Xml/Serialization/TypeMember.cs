﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000302 RID: 770
	internal sealed class TypeMember
	{
		// Token: 0x06001C62 RID: 7266 RVA: 0x000A3960 File Offset: 0x000A1B60
		internal TypeMember(Type type, string member)
		{
			this.type = type;
			this.member = member;
		}

		// Token: 0x06001C63 RID: 7267 RVA: 0x000A3976 File Offset: 0x000A1B76
		public override int GetHashCode()
		{
			return this.type.GetHashCode() + this.member.GetHashCode();
		}

		// Token: 0x06001C64 RID: 7268 RVA: 0x000A398F File Offset: 0x000A1B8F
		public override bool Equals(object obj)
		{
			return obj is TypeMember && TypeMember.Equals(this, (TypeMember)obj);
		}

		// Token: 0x06001C65 RID: 7269 RVA: 0x000A39A7 File Offset: 0x000A1BA7
		public static bool Equals(TypeMember tm1, TypeMember tm2)
		{
			return tm1 == tm2 || (tm1 != null && tm2 != null && (tm1.type == tm2.type && tm1.member == tm2.member));
		}

		// Token: 0x06001C66 RID: 7270 RVA: 0x000A39E0 File Offset: 0x000A1BE0
		public static bool operator ==(TypeMember tm1, TypeMember tm2)
		{
			return TypeMember.Equals(tm1, tm2);
		}

		// Token: 0x06001C67 RID: 7271 RVA: 0x000A39E9 File Offset: 0x000A1BE9
		public static bool operator !=(TypeMember tm1, TypeMember tm2)
		{
			return !TypeMember.Equals(tm1, tm2);
		}

		// Token: 0x06001C68 RID: 7272 RVA: 0x000A39F5 File Offset: 0x000A1BF5
		public override string ToString()
		{
			return this.type.ToString() + " " + this.member;
		}

		// Token: 0x0400163D RID: 5693
		private Type type;

		// Token: 0x0400163E RID: 5694
		private string member;
	}
}
