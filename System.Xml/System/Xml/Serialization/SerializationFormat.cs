﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000317 RID: 791
	internal enum SerializationFormat
	{
		// Token: 0x04001682 RID: 5762
		Encoded,
		// Token: 0x04001683 RID: 5763
		Literal
	}
}
