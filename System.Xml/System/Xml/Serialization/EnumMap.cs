﻿using System;
using System.Globalization;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x0200034F RID: 847
	internal class EnumMap : ObjectMap
	{
		// Token: 0x060020A5 RID: 8357 RVA: 0x000B6EF4 File Offset: 0x000B50F4
		public EnumMap(EnumMap.EnumMapMember[] members, bool isFlags)
		{
			this._members = members;
			this._isFlags = isFlags;
			this._enumNames = new string[this._members.Length];
			this._xmlNames = new string[this._members.Length];
			this._values = new long[this._members.Length];
			for (int i = 0; i < this._members.Length; i++)
			{
				EnumMap.EnumMapMember enumMapMember = this._members[i];
				this._enumNames[i] = enumMapMember.EnumName;
				this._xmlNames[i] = enumMapMember.XmlName;
				this._values[i] = enumMapMember.Value;
			}
		}

		// Token: 0x1700064E RID: 1614
		// (get) Token: 0x060020A6 RID: 8358 RVA: 0x000B6F94 File Offset: 0x000B5194
		public bool IsFlags
		{
			get
			{
				return this._isFlags;
			}
		}

		// Token: 0x1700064F RID: 1615
		// (get) Token: 0x060020A7 RID: 8359 RVA: 0x000B6F9C File Offset: 0x000B519C
		public EnumMap.EnumMapMember[] Members
		{
			get
			{
				return this._members;
			}
		}

		// Token: 0x17000650 RID: 1616
		// (get) Token: 0x060020A8 RID: 8360 RVA: 0x000B6FA4 File Offset: 0x000B51A4
		public string[] EnumNames
		{
			get
			{
				return this._enumNames;
			}
		}

		// Token: 0x17000651 RID: 1617
		// (get) Token: 0x060020A9 RID: 8361 RVA: 0x000B6FAC File Offset: 0x000B51AC
		public string[] XmlNames
		{
			get
			{
				return this._xmlNames;
			}
		}

		// Token: 0x17000652 RID: 1618
		// (get) Token: 0x060020AA RID: 8362 RVA: 0x000B6FB4 File Offset: 0x000B51B4
		public long[] Values
		{
			get
			{
				return this._values;
			}
		}

		// Token: 0x060020AB RID: 8363 RVA: 0x000B6FBC File Offset: 0x000B51BC
		public string GetXmlName(string typeName, object enumValue)
		{
			if (enumValue is string)
			{
				throw new InvalidCastException();
			}
			long num = 0L;
			try
			{
				num = ((IConvertible)enumValue).ToInt64(CultureInfo.CurrentCulture);
			}
			catch (FormatException)
			{
				throw new InvalidCastException();
			}
			for (int i = 0; i < this.Values.Length; i++)
			{
				if (this.Values[i] == num)
				{
					return this.XmlNames[i];
				}
			}
			if (this.IsFlags && num == 0L)
			{
				return string.Empty;
			}
			string text = string.Empty;
			if (this.IsFlags)
			{
				text = XmlCustomFormatter.FromEnum(num, this.XmlNames, this.Values, typeName);
			}
			if (text.Length == 0)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "'{0}' is not a valid value for {1}.", num, typeName));
			}
			return text;
		}

		// Token: 0x060020AC RID: 8364 RVA: 0x000B7084 File Offset: 0x000B5284
		public string GetEnumName(string typeName, string xmlName)
		{
			if (!this._isFlags)
			{
				foreach (EnumMap.EnumMapMember enumMapMember in this._members)
				{
					if (enumMapMember.XmlName == xmlName)
					{
						return enumMapMember.EnumName;
					}
				}
				return null;
			}
			xmlName = xmlName.Trim();
			if (xmlName.Length == 0)
			{
				return "0";
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (string text in xmlName.Split(null))
			{
				if (!(text == string.Empty))
				{
					string text2 = null;
					for (int j = 0; j < this.XmlNames.Length; j++)
					{
						if (this.XmlNames[j] == text)
						{
							text2 = this.EnumNames[j];
							break;
						}
					}
					if (text2 == null)
					{
						throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "'{0}' is not a valid value for {1}.", text, typeName));
					}
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Append(',');
					}
					stringBuilder.Append(text2);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0400177F RID: 6015
		private readonly EnumMap.EnumMapMember[] _members;

		// Token: 0x04001780 RID: 6016
		private readonly bool _isFlags;

		// Token: 0x04001781 RID: 6017
		private readonly string[] _enumNames;

		// Token: 0x04001782 RID: 6018
		private readonly string[] _xmlNames;

		// Token: 0x04001783 RID: 6019
		private readonly long[] _values;

		// Token: 0x02000350 RID: 848
		public class EnumMapMember
		{
			// Token: 0x060020AD RID: 8365 RVA: 0x000B7191 File Offset: 0x000B5391
			public EnumMapMember(string xmlName, string enumName) : this(xmlName, enumName, 0L)
			{
			}

			// Token: 0x060020AE RID: 8366 RVA: 0x000B719D File Offset: 0x000B539D
			public EnumMapMember(string xmlName, string enumName, long value)
			{
				this._xmlName = xmlName;
				this._enumName = enumName;
				this._value = value;
			}

			// Token: 0x17000653 RID: 1619
			// (get) Token: 0x060020AF RID: 8367 RVA: 0x000B71BA File Offset: 0x000B53BA
			public string XmlName
			{
				get
				{
					return this._xmlName;
				}
			}

			// Token: 0x17000654 RID: 1620
			// (get) Token: 0x060020B0 RID: 8368 RVA: 0x000B71C2 File Offset: 0x000B53C2
			public string EnumName
			{
				get
				{
					return this._enumName;
				}
			}

			// Token: 0x17000655 RID: 1621
			// (get) Token: 0x060020B1 RID: 8369 RVA: 0x000B71CA File Offset: 0x000B53CA
			public long Value
			{
				get
				{
					return this._value;
				}
			}

			// Token: 0x17000656 RID: 1622
			// (get) Token: 0x060020B2 RID: 8370 RVA: 0x000B71D2 File Offset: 0x000B53D2
			// (set) Token: 0x060020B3 RID: 8371 RVA: 0x000B71DA File Offset: 0x000B53DA
			public string Documentation
			{
				get
				{
					return this._documentation;
				}
				set
				{
					this._documentation = value;
				}
			}

			// Token: 0x04001784 RID: 6020
			private readonly string _xmlName;

			// Token: 0x04001785 RID: 6021
			private readonly string _enumName;

			// Token: 0x04001786 RID: 6022
			private readonly long _value;

			// Token: 0x04001787 RID: 6023
			private string _documentation;
		}
	}
}
