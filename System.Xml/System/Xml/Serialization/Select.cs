﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002EF RID: 751
	[XmlType("select")]
	internal class Select
	{
		// Token: 0x06001BD8 RID: 7128 RVA: 0x00002103 File Offset: 0x00000303
		public Select()
		{
		}

		// Token: 0x040015F8 RID: 5624
		[XmlElement("typeName")]
		public string TypeName;

		// Token: 0x040015F9 RID: 5625
		[XmlElement("typeAttribute")]
		public string[] TypeAttributes;

		// Token: 0x040015FA RID: 5626
		[XmlElement("typeMember")]
		public string TypeMember;
	}
}
