﻿using System;
using System.Runtime.CompilerServices;

namespace System.Xml.Serialization
{
	// Token: 0x02000300 RID: 768
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	internal class XmlTypeConvertorAttribute : Attribute
	{
		// Token: 0x17000557 RID: 1367
		// (get) Token: 0x06001C40 RID: 7232 RVA: 0x000A2CA6 File Offset: 0x000A0EA6
		// (set) Token: 0x06001C41 RID: 7233 RVA: 0x000A2CAE File Offset: 0x000A0EAE
		public string Method
		{
			[CompilerGenerated]
			get
			{
				return this.<Method>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Method>k__BackingField = value;
			}
		}

		// Token: 0x06001C42 RID: 7234 RVA: 0x000A2CB7 File Offset: 0x000A0EB7
		public XmlTypeConvertorAttribute(string method)
		{
			this.Method = method;
		}

		// Token: 0x0400162B RID: 5675
		[CompilerGenerated]
		private string <Method>k__BackingField;
	}
}
