﻿using System;
using System.Collections;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Represents a collection of <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> objects used by the <see cref="T:System.Xml.Serialization.XmlSerializer" /> to override the default way it serializes a class.</summary>
	// Token: 0x02000311 RID: 785
	public class XmlElementAttributes : CollectionBase
	{
		/// <summary>Gets or sets the element at the specified index.</summary>
		/// <param name="index">The zero-based index of the element to get or set. </param>
		/// <returns>The element at the specified index.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is not a valid index in the <see cref="T:System.Xml.Serialization.XmlElementAttributes" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The property is set and the <see cref="T:System.Xml.Serialization.XmlElementAttributes" /> is read-only. </exception>
		// Token: 0x170005A5 RID: 1445
		public XmlElementAttribute this[int index]
		{
			get
			{
				return (XmlElementAttribute)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		/// <summary>Adds an <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> to the collection.</summary>
		/// <param name="attribute">The <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> to add. </param>
		/// <returns>The zero-based index of the newly added item.</returns>
		// Token: 0x06001D31 RID: 7473 RVA: 0x000A4874 File Offset: 0x000A2A74
		public int Add(XmlElementAttribute attribute)
		{
			return base.List.Add(attribute);
		}

		/// <summary>Determines whether the collection contains the specified object.</summary>
		/// <param name="attribute">The <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> to look for. </param>
		/// <returns>
		///     <see langword="true" /> if the object exists in the collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001D32 RID: 7474 RVA: 0x000976D4 File Offset: 0x000958D4
		public bool Contains(XmlElementAttribute attribute)
		{
			return base.List.Contains(attribute);
		}

		/// <summary>Gets the index of the specified <see cref="T:System.Xml.Serialization.XmlElementAttribute" />.</summary>
		/// <param name="attribute">The <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> whose index is being retrieved.</param>
		/// <returns>The zero-based index of the <see cref="T:System.Xml.Serialization.XmlElementAttribute" />.</returns>
		// Token: 0x06001D33 RID: 7475 RVA: 0x000976C6 File Offset: 0x000958C6
		public int IndexOf(XmlElementAttribute attribute)
		{
			return base.List.IndexOf(attribute);
		}

		/// <summary>Inserts an <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> into the collection.</summary>
		/// <param name="index">The zero-based index where the member is inserted. </param>
		/// <param name="attribute">The <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> to insert. </param>
		// Token: 0x06001D34 RID: 7476 RVA: 0x000976B7 File Offset: 0x000958B7
		public void Insert(int index, XmlElementAttribute attribute)
		{
			base.List.Insert(index, attribute);
		}

		/// <summary>Removes the specified object from the collection.</summary>
		/// <param name="attribute">The <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> to remove from the collection. </param>
		// Token: 0x06001D35 RID: 7477 RVA: 0x000976F0 File Offset: 0x000958F0
		public void Remove(XmlElementAttribute attribute)
		{
			base.List.Remove(attribute);
		}

		/// <summary>Copies the <see cref="T:System.Xml.Serialization.XmlElementAttributes" />, or a portion of it to a one-dimensional array.</summary>
		/// <param name="array">The <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> array to hold the copied elements. </param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		// Token: 0x06001D36 RID: 7478 RVA: 0x000976FE File Offset: 0x000958FE
		public void CopyTo(XmlElementAttribute[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x06001D37 RID: 7479 RVA: 0x000A6CF4 File Offset: 0x000A4EF4
		internal void AddKeyHash(StringBuilder sb)
		{
			if (base.Count == 0)
			{
				return;
			}
			sb.Append("XEAS ");
			for (int i = 0; i < base.Count; i++)
			{
				this[i].AddKeyHash(sb);
			}
			sb.Append('|');
		}

		// Token: 0x170005A6 RID: 1446
		// (get) Token: 0x06001D38 RID: 7480 RVA: 0x000A6D40 File Offset: 0x000A4F40
		internal int Order
		{
			get
			{
				foreach (object obj in this)
				{
					XmlElementAttribute xmlElementAttribute = (XmlElementAttribute)obj;
					if (xmlElementAttribute.Order >= 0)
					{
						return xmlElementAttribute.Order;
					}
				}
				return -1;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlElementAttributes" /> class. </summary>
		// Token: 0x06001D39 RID: 7481 RVA: 0x000A4934 File Offset: 0x000A2B34
		public XmlElementAttributes()
		{
		}
	}
}
