﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x020002CF RID: 719
	internal class SchemaObjectCache
	{
		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x06001A88 RID: 6792 RVA: 0x00094248 File Offset: 0x00092448
		private Hashtable Graph
		{
			get
			{
				if (this.graph == null)
				{
					this.graph = new Hashtable();
				}
				return this.graph;
			}
		}

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x06001A89 RID: 6793 RVA: 0x00094263 File Offset: 0x00092463
		private Hashtable Hash
		{
			get
			{
				if (this.hash == null)
				{
					this.hash = new Hashtable();
				}
				return this.hash;
			}
		}

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x06001A8A RID: 6794 RVA: 0x0009427E File Offset: 0x0009247E
		private Hashtable ObjectCache
		{
			get
			{
				if (this.objectCache == null)
				{
					this.objectCache = new Hashtable();
				}
				return this.objectCache;
			}
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x06001A8B RID: 6795 RVA: 0x00094299 File Offset: 0x00092499
		internal StringCollection Warnings
		{
			get
			{
				if (this.warnings == null)
				{
					this.warnings = new StringCollection();
				}
				return this.warnings;
			}
		}

		// Token: 0x06001A8C RID: 6796 RVA: 0x000942B4 File Offset: 0x000924B4
		internal XmlSchemaObject AddItem(XmlSchemaObject item, XmlQualifiedName qname, XmlSchemas schemas)
		{
			if (item == null)
			{
				return null;
			}
			if (qname == null || qname.IsEmpty)
			{
				return null;
			}
			string key = item.GetType().Name + ":" + qname.ToString();
			ArrayList arrayList = (ArrayList)this.ObjectCache[key];
			if (arrayList == null)
			{
				arrayList = new ArrayList();
				this.ObjectCache[key] = arrayList;
			}
			for (int i = 0; i < arrayList.Count; i++)
			{
				XmlSchemaObject xmlSchemaObject = (XmlSchemaObject)arrayList[i];
				if (xmlSchemaObject == item)
				{
					return xmlSchemaObject;
				}
				if (this.Match(xmlSchemaObject, item, true))
				{
					return xmlSchemaObject;
				}
				this.Warnings.Add(Res.GetString("Warning: Cannot share {0} named '{1}' from '{2}' namespace. Several mismatched schema declarations were found.", new object[]
				{
					item.GetType().Name,
					qname.Name,
					qname.Namespace
				}));
				this.Warnings.Add("DEBUG:Cached item key:\r\n" + (string)this.looks[xmlSchemaObject] + "\r\nnew item key:\r\n" + (string)this.looks[item]);
			}
			arrayList.Add(item);
			return item;
		}

		// Token: 0x06001A8D RID: 6797 RVA: 0x000943D8 File Offset: 0x000925D8
		internal bool Match(XmlSchemaObject o1, XmlSchemaObject o2, bool shareTypes)
		{
			if (o1 == o2)
			{
				return true;
			}
			if (o1.GetType() != o2.GetType())
			{
				return false;
			}
			if (this.Hash[o1] == null)
			{
				this.Hash[o1] = this.GetHash(o1);
			}
			int num = (int)this.Hash[o1];
			int num2 = this.GetHash(o2);
			return num == num2 && (!shareTypes || this.CompositeHash(o1, num) == this.CompositeHash(o2, num2));
		}

		// Token: 0x06001A8E RID: 6798 RVA: 0x00094460 File Offset: 0x00092660
		private ArrayList GetDependencies(XmlSchemaObject o, ArrayList deps, Hashtable refs)
		{
			if (refs[o] == null)
			{
				refs[o] = o;
				deps.Add(o);
				ArrayList arrayList = this.Graph[o] as ArrayList;
				if (arrayList != null)
				{
					for (int i = 0; i < arrayList.Count; i++)
					{
						this.GetDependencies((XmlSchemaObject)arrayList[i], deps, refs);
					}
				}
			}
			return deps;
		}

		// Token: 0x06001A8F RID: 6799 RVA: 0x000944C4 File Offset: 0x000926C4
		private int CompositeHash(XmlSchemaObject o, int hash)
		{
			ArrayList dependencies = this.GetDependencies(o, new ArrayList(), new Hashtable());
			double num = 0.0;
			for (int i = 0; i < dependencies.Count; i++)
			{
				object obj = this.Hash[dependencies[i]];
				if (obj is int)
				{
					num += (double)((int)obj / dependencies.Count);
				}
			}
			return (int)num;
		}

		// Token: 0x06001A90 RID: 6800 RVA: 0x0009452C File Offset: 0x0009272C
		internal void GenerateSchemaGraph(XmlSchemas schemas)
		{
			ArrayList items = new SchemaGraph(this.Graph, schemas).GetItems();
			for (int i = 0; i < items.Count; i++)
			{
				this.GetHash((XmlSchemaObject)items[i]);
			}
		}

		// Token: 0x06001A91 RID: 6801 RVA: 0x00094570 File Offset: 0x00092770
		private int GetHash(XmlSchemaObject o)
		{
			object obj = this.Hash[o];
			if (obj != null && !(obj is XmlSchemaObject))
			{
				return (int)obj;
			}
			string text = this.ToString(o, new SchemaObjectWriter());
			this.looks[o] = text;
			int hashCode = text.GetHashCode();
			this.Hash[o] = hashCode;
			return hashCode;
		}

		// Token: 0x06001A92 RID: 6802 RVA: 0x000945D0 File Offset: 0x000927D0
		private string ToString(XmlSchemaObject o, SchemaObjectWriter writer)
		{
			return writer.WriteXmlSchemaObject(o);
		}

		// Token: 0x06001A93 RID: 6803 RVA: 0x000945D9 File Offset: 0x000927D9
		public SchemaObjectCache()
		{
		}

		// Token: 0x04001585 RID: 5509
		private Hashtable graph;

		// Token: 0x04001586 RID: 5510
		private Hashtable hash;

		// Token: 0x04001587 RID: 5511
		private Hashtable objectCache;

		// Token: 0x04001588 RID: 5512
		private StringCollection warnings;

		// Token: 0x04001589 RID: 5513
		internal Hashtable looks = new Hashtable();
	}
}
