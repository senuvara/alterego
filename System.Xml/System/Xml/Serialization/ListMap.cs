﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x0200034E RID: 846
	internal class ListMap : ObjectMap
	{
		// Token: 0x1700064A RID: 1610
		// (get) Token: 0x06002097 RID: 8343 RVA: 0x000B6A0B File Offset: 0x000B4C0B
		public bool IsMultiArray
		{
			get
			{
				return this.NestedArrayMapping != null;
			}
		}

		// Token: 0x1700064B RID: 1611
		// (get) Token: 0x06002098 RID: 8344 RVA: 0x000B6A16 File Offset: 0x000B4C16
		// (set) Token: 0x06002099 RID: 8345 RVA: 0x000B6A1E File Offset: 0x000B4C1E
		public string ChoiceMember
		{
			get
			{
				return this._choiceMember;
			}
			set
			{
				this._choiceMember = value;
			}
		}

		// Token: 0x1700064C RID: 1612
		// (get) Token: 0x0600209A RID: 8346 RVA: 0x000B6A28 File Offset: 0x000B4C28
		public XmlTypeMapping NestedArrayMapping
		{
			get
			{
				if (this._gotNestedMapping)
				{
					return this._nestedArrayMapping;
				}
				this._gotNestedMapping = true;
				this._nestedArrayMapping = ((XmlTypeMapElementInfo)this._itemInfo[0]).MappedType;
				if (this._nestedArrayMapping == null)
				{
					return null;
				}
				if (this._nestedArrayMapping.TypeData.SchemaType != SchemaTypes.Array)
				{
					this._nestedArrayMapping = null;
					return null;
				}
				using (IEnumerator enumerator = this._itemInfo.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (((XmlTypeMapElementInfo)enumerator.Current).MappedType != this._nestedArrayMapping)
						{
							this._nestedArrayMapping = null;
							return null;
						}
					}
				}
				return this._nestedArrayMapping;
			}
		}

		// Token: 0x1700064D RID: 1613
		// (get) Token: 0x0600209B RID: 8347 RVA: 0x000B6AF4 File Offset: 0x000B4CF4
		// (set) Token: 0x0600209C RID: 8348 RVA: 0x000B6AFC File Offset: 0x000B4CFC
		public XmlTypeMapElementInfoList ItemInfo
		{
			get
			{
				return this._itemInfo;
			}
			set
			{
				this._itemInfo = value;
			}
		}

		// Token: 0x0600209D RID: 8349 RVA: 0x000B6B08 File Offset: 0x000B4D08
		public XmlTypeMapElementInfo FindElement(object ob, int index, object memberValue)
		{
			if (this._itemInfo.Count == 1)
			{
				return (XmlTypeMapElementInfo)this._itemInfo[0];
			}
			if (this._choiceMember != null && index != -1)
			{
				Array array = (Array)XmlTypeMapMember.GetValue(ob, this._choiceMember);
				if (array == null || index >= array.Length)
				{
					throw new InvalidOperationException("Invalid or missing choice enum value in member '" + this._choiceMember + "'.");
				}
				object value = array.GetValue(index);
				using (IEnumerator enumerator = this._itemInfo.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
						if (xmlTypeMapElementInfo.ChoiceValue != null && xmlTypeMapElementInfo.ChoiceValue.Equals(value))
						{
							return xmlTypeMapElementInfo;
						}
					}
					goto IL_16F;
				}
				goto IL_CC;
				IL_16F:
				return null;
			}
			IL_CC:
			if (memberValue == null)
			{
				return null;
			}
			Type type = memberValue.GetType();
			XmlTypeMapElementInfo xmlTypeMapElementInfo2 = null;
			foreach (object obj2 in this._itemInfo)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo3 = (XmlTypeMapElementInfo)obj2;
				if (xmlTypeMapElementInfo3.TypeData.Type == type)
				{
					return xmlTypeMapElementInfo3;
				}
				if (xmlTypeMapElementInfo3.TypeData.Type.IsAssignableFrom(type) && (xmlTypeMapElementInfo2 == null || xmlTypeMapElementInfo3.TypeData.Type.IsAssignableFrom(xmlTypeMapElementInfo2.TypeData.Type)))
				{
					xmlTypeMapElementInfo2 = xmlTypeMapElementInfo3;
				}
			}
			return xmlTypeMapElementInfo2;
		}

		// Token: 0x0600209E RID: 8350 RVA: 0x000B6CA4 File Offset: 0x000B4EA4
		public XmlTypeMapElementInfo FindElement(string elementName, string ns)
		{
			foreach (object obj in this._itemInfo)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
				if (xmlTypeMapElementInfo.ElementName == elementName && xmlTypeMapElementInfo.Namespace == ns)
				{
					return xmlTypeMapElementInfo;
				}
			}
			return null;
		}

		// Token: 0x0600209F RID: 8351 RVA: 0x000B6D1C File Offset: 0x000B4F1C
		public XmlTypeMapElementInfo FindTextElement()
		{
			foreach (object obj in this._itemInfo)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
				if (xmlTypeMapElementInfo.IsTextElement)
				{
					return xmlTypeMapElementInfo;
				}
			}
			return null;
		}

		// Token: 0x060020A0 RID: 8352 RVA: 0x000B6D80 File Offset: 0x000B4F80
		public string GetSchemaArrayName()
		{
			XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)this._itemInfo[0];
			if (xmlTypeMapElementInfo.MappedType != null)
			{
				return TypeTranslator.GetArrayName(xmlTypeMapElementInfo.MappedType.XmlType);
			}
			return TypeTranslator.GetArrayName(xmlTypeMapElementInfo.TypeData.XmlType);
		}

		// Token: 0x060020A1 RID: 8353 RVA: 0x000B6DC8 File Offset: 0x000B4FC8
		public void GetArrayType(int itemCount, out string localName, out string ns)
		{
			string str;
			if (itemCount != -1)
			{
				str = "[" + itemCount + "]";
			}
			else
			{
				str = "[]";
			}
			XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)this._itemInfo[0];
			if (xmlTypeMapElementInfo.TypeData.SchemaType == SchemaTypes.Array)
			{
				string str2;
				((ListMap)xmlTypeMapElementInfo.MappedType.ObjectMap).GetArrayType(-1, out str2, out ns);
				localName = str2 + str;
				return;
			}
			if (xmlTypeMapElementInfo.MappedType != null)
			{
				localName = xmlTypeMapElementInfo.MappedType.XmlType + str;
				ns = xmlTypeMapElementInfo.MappedType.Namespace;
				return;
			}
			localName = xmlTypeMapElementInfo.TypeData.XmlType + str;
			ns = xmlTypeMapElementInfo.DataTypeNamespace;
		}

		// Token: 0x060020A2 RID: 8354 RVA: 0x000B6E80 File Offset: 0x000B5080
		public override bool Equals(object other)
		{
			ListMap listMap = other as ListMap;
			if (listMap == null)
			{
				return false;
			}
			if (this._itemInfo.Count != listMap._itemInfo.Count)
			{
				return false;
			}
			for (int i = 0; i < this._itemInfo.Count; i++)
			{
				if (!this._itemInfo[i].Equals(listMap._itemInfo[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060020A3 RID: 8355 RVA: 0x000B52AC File Offset: 0x000B34AC
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x060020A4 RID: 8356 RVA: 0x000B6EEB File Offset: 0x000B50EB
		public ListMap()
		{
		}

		// Token: 0x0400177B RID: 6011
		private XmlTypeMapElementInfoList _itemInfo;

		// Token: 0x0400177C RID: 6012
		private bool _gotNestedMapping;

		// Token: 0x0400177D RID: 6013
		private XmlTypeMapping _nestedArrayMapping;

		// Token: 0x0400177E RID: 6014
		private string _choiceMember;
	}
}
