﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	/// <summary>Supports mappings between .NET Framework types and XML Schema data types. </summary>
	// Token: 0x02000315 RID: 789
	public abstract class XmlMapping
	{
		// Token: 0x06001D43 RID: 7491 RVA: 0x00002103 File Offset: 0x00000303
		internal XmlMapping()
		{
		}

		// Token: 0x06001D44 RID: 7492 RVA: 0x000A6E08 File Offset: 0x000A5008
		internal XmlMapping(string elementName, string ns)
		{
			this._elementName = elementName;
			this._namespace = ns;
		}

		/// <summary>Gets the name of the XSD element of the mapping.</summary>
		/// <returns>The XSD element name.</returns>
		// Token: 0x170005A9 RID: 1449
		// (get) Token: 0x06001D45 RID: 7493 RVA: 0x000A6E1E File Offset: 0x000A501E
		[MonoTODO]
		public string XsdElementName
		{
			get
			{
				return this._elementName;
			}
		}

		/// <summary>Get the name of the mapped element.</summary>
		/// <returns>The name of the mapped element.</returns>
		// Token: 0x170005AA RID: 1450
		// (get) Token: 0x06001D46 RID: 7494 RVA: 0x000A6E1E File Offset: 0x000A501E
		public string ElementName
		{
			get
			{
				return this._elementName;
			}
		}

		/// <summary>Gets the namespace of the mapped element.</summary>
		/// <returns>The namespace of the mapped element.</returns>
		// Token: 0x170005AB RID: 1451
		// (get) Token: 0x06001D47 RID: 7495 RVA: 0x000A6E26 File Offset: 0x000A5026
		public string Namespace
		{
			get
			{
				return this._namespace;
			}
		}

		/// <summary>Sets the key used to look up the mapping.</summary>
		/// <param name="key">A <see cref="T:System.String" /> that contains the lookup key.</param>
		// Token: 0x06001D48 RID: 7496 RVA: 0x000A6E2E File Offset: 0x000A502E
		public void SetKey(string key)
		{
			this.key = key;
		}

		// Token: 0x06001D49 RID: 7497 RVA: 0x000A6E37 File Offset: 0x000A5037
		internal string GetKey()
		{
			return this.key;
		}

		// Token: 0x170005AC RID: 1452
		// (get) Token: 0x06001D4A RID: 7498 RVA: 0x000A6E3F File Offset: 0x000A503F
		// (set) Token: 0x06001D4B RID: 7499 RVA: 0x000A6E47 File Offset: 0x000A5047
		internal ObjectMap ObjectMap
		{
			get
			{
				return this.map;
			}
			set
			{
				this.map = value;
			}
		}

		// Token: 0x170005AD RID: 1453
		// (get) Token: 0x06001D4C RID: 7500 RVA: 0x000A6E50 File Offset: 0x000A5050
		// (set) Token: 0x06001D4D RID: 7501 RVA: 0x000A6E58 File Offset: 0x000A5058
		internal ArrayList RelatedMaps
		{
			get
			{
				return this.relatedMaps;
			}
			set
			{
				this.relatedMaps = value;
			}
		}

		// Token: 0x170005AE RID: 1454
		// (get) Token: 0x06001D4E RID: 7502 RVA: 0x000A6E61 File Offset: 0x000A5061
		// (set) Token: 0x06001D4F RID: 7503 RVA: 0x000A6E69 File Offset: 0x000A5069
		internal SerializationFormat Format
		{
			get
			{
				return this.format;
			}
			set
			{
				this.format = value;
			}
		}

		// Token: 0x170005AF RID: 1455
		// (get) Token: 0x06001D50 RID: 7504 RVA: 0x000A6E72 File Offset: 0x000A5072
		// (set) Token: 0x06001D51 RID: 7505 RVA: 0x000A6E7A File Offset: 0x000A507A
		internal SerializationSource Source
		{
			get
			{
				return this.source;
			}
			set
			{
				this.source = value;
			}
		}

		// Token: 0x0400167A RID: 5754
		private ObjectMap map;

		// Token: 0x0400167B RID: 5755
		private ArrayList relatedMaps;

		// Token: 0x0400167C RID: 5756
		private SerializationFormat format;

		// Token: 0x0400167D RID: 5757
		private SerializationSource source;

		// Token: 0x0400167E RID: 5758
		internal string _elementName;

		// Token: 0x0400167F RID: 5759
		internal string _namespace;

		// Token: 0x04001680 RID: 5760
		private string key;
	}
}
