﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002F2 RID: 754
	internal abstract class SerializationSource
	{
		// Token: 0x06001BD9 RID: 7129 RVA: 0x000A151B File Offset: 0x0009F71B
		public SerializationSource(string namspace, Type[] includedTypes)
		{
			this.namspace = namspace;
			this.includedTypes = includedTypes;
		}

		// Token: 0x06001BDA RID: 7130 RVA: 0x000A1538 File Offset: 0x0009F738
		protected bool BaseEquals(SerializationSource other)
		{
			if (this.namspace != other.namspace)
			{
				return false;
			}
			if (this.canBeGenerated != other.canBeGenerated)
			{
				return false;
			}
			if (this.includedTypes == null)
			{
				return other.includedTypes == null;
			}
			if (other.includedTypes == null || this.includedTypes.Length != other.includedTypes.Length)
			{
				return false;
			}
			for (int i = 0; i < this.includedTypes.Length; i++)
			{
				if (!this.includedTypes[i].Equals(other.includedTypes[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x17000541 RID: 1345
		// (get) Token: 0x06001BDB RID: 7131 RVA: 0x000A15C5 File Offset: 0x0009F7C5
		// (set) Token: 0x06001BDC RID: 7132 RVA: 0x000A15CD File Offset: 0x0009F7CD
		public virtual bool CanBeGenerated
		{
			get
			{
				return this.canBeGenerated;
			}
			set
			{
				this.canBeGenerated = value;
			}
		}

		// Token: 0x04001606 RID: 5638
		private Type[] includedTypes;

		// Token: 0x04001607 RID: 5639
		private string namspace;

		// Token: 0x04001608 RID: 5640
		private bool canBeGenerated = true;
	}
}
