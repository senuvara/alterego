﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000347 RID: 839
	internal class XmlTypeMapMemberFlatList : XmlTypeMapMemberExpandable
	{
		// Token: 0x17000624 RID: 1572
		// (get) Token: 0x06002050 RID: 8272 RVA: 0x000B5B99 File Offset: 0x000B3D99
		// (set) Token: 0x06002051 RID: 8273 RVA: 0x000B5BA1 File Offset: 0x000B3DA1
		public ListMap ListMap
		{
			get
			{
				return this._listMap;
			}
			set
			{
				this._listMap = value;
			}
		}

		// Token: 0x06002052 RID: 8274 RVA: 0x000B5BAA File Offset: 0x000B3DAA
		public XmlTypeMapMemberFlatList()
		{
		}

		// Token: 0x0400175B RID: 5979
		private ListMap _listMap;
	}
}
