﻿using System;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x020002E1 RID: 737
	internal class TypeScope
	{
		// Token: 0x06001B43 RID: 6979 RVA: 0x00098D74 File Offset: 0x00096F74
		internal static XmlQualifiedName ParseWsdlArrayType(string type, out string dims, XmlSchemaObject parent)
		{
			int num = type.LastIndexOf(':');
			string text;
			if (num <= 0)
			{
				text = "";
			}
			else
			{
				text = type.Substring(0, num);
			}
			int num2 = type.IndexOf('[', num + 1);
			if (num2 <= num)
			{
				throw new InvalidOperationException(Res.GetString("Invalid wsd:arrayType syntax: '{0}'.", new object[]
				{
					type
				}));
			}
			string name = type.Substring(num + 1, num2 - num - 1);
			dims = type.Substring(num2);
			while (parent != null)
			{
				if (parent.Namespaces != null)
				{
					string text2 = (string)parent.Namespaces.Namespaces[text];
					if (text2 != null)
					{
						text = text2;
						break;
					}
				}
				parent = parent.Parent;
			}
			return new XmlQualifiedName(name, text);
		}

		// Token: 0x06001B44 RID: 6980 RVA: 0x00002103 File Offset: 0x00000303
		public TypeScope()
		{
		}
	}
}
