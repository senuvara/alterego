﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Applied to a Web service client proxy, enables you to specify an assembly that contains custom-made serializers. </summary>
	// Token: 0x0200033A RID: 826
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface)]
	public sealed class XmlSerializerAssemblyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializerAssemblyAttribute" /> class. </summary>
		// Token: 0x06001FB8 RID: 8120 RVA: 0x0000D3B3 File Offset: 0x0000B5B3
		public XmlSerializerAssemblyAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializerAssemblyAttribute" /> class with the specified assembly name.</summary>
		/// <param name="assemblyName">The simple, unencrypted name of the assembly. </param>
		// Token: 0x06001FB9 RID: 8121 RVA: 0x000B4D00 File Offset: 0x000B2F00
		public XmlSerializerAssemblyAttribute(string assemblyName)
		{
			this._assemblyName = assemblyName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializerAssemblyAttribute" /> class with the specified assembly name and location of the assembly.</summary>
		/// <param name="assemblyName">The simple, unencrypted name of the assembly. </param>
		/// <param name="codeBase">A string that is the URL location of the assembly.</param>
		// Token: 0x06001FBA RID: 8122 RVA: 0x000B4D0F File Offset: 0x000B2F0F
		public XmlSerializerAssemblyAttribute(string assemblyName, string codeBase) : this(assemblyName)
		{
			this._codeBase = codeBase;
		}

		/// <summary>Gets or sets the name of the assembly that contains serializers for a specific set of types.</summary>
		/// <returns>The simple, unencrypted name of the assembly. </returns>
		// Token: 0x170005E7 RID: 1511
		// (get) Token: 0x06001FBB RID: 8123 RVA: 0x000B4D1F File Offset: 0x000B2F1F
		// (set) Token: 0x06001FBC RID: 8124 RVA: 0x000B4D27 File Offset: 0x000B2F27
		public string AssemblyName
		{
			get
			{
				return this._assemblyName;
			}
			set
			{
				this._assemblyName = value;
			}
		}

		/// <summary>Gets or sets the location of the assembly that contains the serializers.</summary>
		/// <returns>A location, such as a path or URI, that points to the assembly.</returns>
		// Token: 0x170005E8 RID: 1512
		// (get) Token: 0x06001FBD RID: 8125 RVA: 0x000B4D30 File Offset: 0x000B2F30
		// (set) Token: 0x06001FBE RID: 8126 RVA: 0x000B4D38 File Offset: 0x000B2F38
		public string CodeBase
		{
			get
			{
				return this._codeBase;
			}
			set
			{
				this._codeBase = value;
			}
		}

		// Token: 0x0400172C RID: 5932
		private string _assemblyName;

		// Token: 0x0400172D RID: 5933
		private string _codeBase;
	}
}
