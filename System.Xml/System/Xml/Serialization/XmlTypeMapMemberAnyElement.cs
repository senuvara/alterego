﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x02000348 RID: 840
	internal class XmlTypeMapMemberAnyElement : XmlTypeMapMemberExpandable
	{
		// Token: 0x06002053 RID: 8275 RVA: 0x000B5BB4 File Offset: 0x000B3DB4
		public bool IsElementDefined(string name, string ns)
		{
			foreach (object obj in base.ElementInfo)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
				if (xmlTypeMapElementInfo.IsUnnamedAnyElement)
				{
					return true;
				}
				if (xmlTypeMapElementInfo.ElementName == name && xmlTypeMapElementInfo.Namespace == ns)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x17000625 RID: 1573
		// (get) Token: 0x06002054 RID: 8276 RVA: 0x000B5C38 File Offset: 0x000B3E38
		public bool IsDefaultAny
		{
			get
			{
				using (IEnumerator enumerator = base.ElementInfo.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (((XmlTypeMapElementInfo)enumerator.Current).IsUnnamedAnyElement)
						{
							return true;
						}
					}
				}
				return false;
			}
		}

		// Token: 0x17000626 RID: 1574
		// (get) Token: 0x06002055 RID: 8277 RVA: 0x000B5C98 File Offset: 0x000B3E98
		public bool CanBeText
		{
			get
			{
				return base.ElementInfo.Count > 0 && ((XmlTypeMapElementInfo)base.ElementInfo[0]).IsTextElement;
			}
		}

		// Token: 0x06002056 RID: 8278 RVA: 0x000B5BAA File Offset: 0x000B3DAA
		public XmlTypeMapMemberAnyElement()
		{
		}
	}
}
