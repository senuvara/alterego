﻿using System;
using System.Collections;
using System.ComponentModel;

namespace System.Xml.Serialization
{
	// Token: 0x020002ED RID: 749
	[XmlType("serializer")]
	internal class SerializerInfo
	{
		// Token: 0x06001BD3 RID: 7123 RVA: 0x000A1314 File Offset: 0x0009F514
		public ArrayList GetHooks(HookType hookType, XmlMappingAccess dir, HookAction action, Type type, string member)
		{
			if ((dir & XmlMappingAccess.Read) != XmlMappingAccess.None)
			{
				return this.FindHook(this.ReaderHooks, hookType, action, type, member);
			}
			if ((dir & XmlMappingAccess.Write) != XmlMappingAccess.None)
			{
				return this.FindHook(this.WriterHooks, hookType, action, type, member);
			}
			throw new Exception("INTERNAL ERROR");
		}

		// Token: 0x06001BD4 RID: 7124 RVA: 0x000A1350 File Offset: 0x0009F550
		private ArrayList FindHook(Hook[] hooks, HookType hookType, HookAction action, Type type, string member)
		{
			ArrayList arrayList = new ArrayList();
			if (hooks == null)
			{
				return arrayList;
			}
			foreach (Hook hook in hooks)
			{
				if ((action != HookAction.InsertBefore || (hook.InsertBefore != null && !(hook.InsertBefore == ""))) && (action != HookAction.InsertAfter || (hook.InsertAfter != null && !(hook.InsertAfter == ""))) && (action != HookAction.Replace || (hook.Replace != null && !(hook.Replace == ""))) && hook.HookType == hookType)
				{
					if (hook.Select != null)
					{
						if ((hook.Select.TypeName != null && hook.Select.TypeName != "" && hook.Select.TypeName != type.FullName) || (hook.Select.TypeMember != null && hook.Select.TypeMember != "" && hook.Select.TypeMember != member))
						{
							goto IL_184;
						}
						if (hook.Select.TypeAttributes != null && hook.Select.TypeAttributes.Length != 0)
						{
							object[] customAttributes = type.GetCustomAttributes(true);
							bool flag = false;
							foreach (object obj in customAttributes)
							{
								if (Array.IndexOf<string>(hook.Select.TypeAttributes, obj.GetType().FullName) != -1)
								{
									flag = true;
									break;
								}
							}
							if (!flag)
							{
								goto IL_184;
							}
						}
					}
					arrayList.Add(hook);
				}
				IL_184:;
			}
			return arrayList;
		}

		// Token: 0x06001BD5 RID: 7125 RVA: 0x000A14EF File Offset: 0x0009F6EF
		public SerializerInfo()
		{
		}

		// Token: 0x040015E4 RID: 5604
		[XmlAttribute("class")]
		public string ClassName;

		// Token: 0x040015E5 RID: 5605
		[XmlAttribute("assembly")]
		public string Assembly;

		// Token: 0x040015E6 RID: 5606
		[XmlElement("reader")]
		public string ReaderClassName;

		// Token: 0x040015E7 RID: 5607
		[XmlElement("writer")]
		public string WriterClassName;

		// Token: 0x040015E8 RID: 5608
		[XmlElement("baseSerializer")]
		public string BaseSerializerClassName;

		// Token: 0x040015E9 RID: 5609
		[XmlElement("implementation")]
		public string ImplementationClassName;

		// Token: 0x040015EA RID: 5610
		[XmlElement("noreader")]
		public bool NoReader;

		// Token: 0x040015EB RID: 5611
		[XmlElement("nowriter")]
		public bool NoWriter;

		// Token: 0x040015EC RID: 5612
		[XmlElement("generateAsInternal")]
		public bool GenerateAsInternal;

		// Token: 0x040015ED RID: 5613
		[XmlElement("namespace")]
		public string Namespace;

		// Token: 0x040015EE RID: 5614
		[XmlArray("namespaceImports")]
		[XmlArrayItem("namespaceImport")]
		public string[] NamespaceImports;

		// Token: 0x040015EF RID: 5615
		[DefaultValue(SerializationFormat.Literal)]
		public SerializationFormat SerializationFormat = SerializationFormat.Literal;

		// Token: 0x040015F0 RID: 5616
		[XmlElement("outFileName")]
		public string OutFileName;

		// Token: 0x040015F1 RID: 5617
		[XmlArray("readerHooks")]
		public Hook[] ReaderHooks;

		// Token: 0x040015F2 RID: 5618
		[XmlArray("writerHooks")]
		public Hook[] WriterHooks;
	}
}
