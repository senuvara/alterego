﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace System.Xml.Serialization
{
	/// <summary>Describes the context in which a set of schema is bound to .NET Framework code entities.</summary>
	// Token: 0x020002E5 RID: 741
	public class ImportContext
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.ImportContext" /> class for the given code identifiers, with the given type-sharing option.</summary>
		/// <param name="identifiers">The code entities to which the context applies.</param>
		/// <param name="shareTypes">A <see cref="T:System.Boolean" /> value that determines whether custom types are shared among schema.</param>
		// Token: 0x06001B4D RID: 6989 RVA: 0x00098E20 File Offset: 0x00097020
		public ImportContext(CodeIdentifiers identifiers, bool shareTypes)
		{
			this._typeIdentifiers = identifiers;
			this._shareTypes = shareTypes;
			if (shareTypes)
			{
				this.MappedTypes = new Hashtable();
				this.DataMappedTypes = new Hashtable();
				this.SharedAnonymousTypes = new Hashtable();
			}
		}

		/// <summary>Gets a value that determines whether custom types are shared.</summary>
		/// <returns>
		///     <see langword="true" />, if custom types are shared among schema; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700053C RID: 1340
		// (get) Token: 0x06001B4E RID: 6990 RVA: 0x00098E70 File Offset: 0x00097070
		public bool ShareTypes
		{
			get
			{
				return this._shareTypes;
			}
		}

		/// <summary>Gets a set of code entities to which the context applies.</summary>
		/// <returns>A <see cref="T:System.Xml.Serialization.CodeIdentifiers" /> that specifies the code entities to which the context applies.</returns>
		// Token: 0x1700053D RID: 1341
		// (get) Token: 0x06001B4F RID: 6991 RVA: 0x00098E78 File Offset: 0x00097078
		public CodeIdentifiers TypeIdentifiers
		{
			get
			{
				return this._typeIdentifiers;
			}
		}

		/// <summary>Gets a collection of warnings that are generated when importing the code entity descriptions.</summary>
		/// <returns>A <see cref="T:System.Collections.Specialized.StringCollection" /> that contains warnings that were generated when importing the code entity descriptions.</returns>
		// Token: 0x1700053E RID: 1342
		// (get) Token: 0x06001B50 RID: 6992 RVA: 0x00098E80 File Offset: 0x00097080
		public StringCollection Warnings
		{
			get
			{
				return this._warnings;
			}
		}

		// Token: 0x040015B1 RID: 5553
		private bool _shareTypes;

		// Token: 0x040015B2 RID: 5554
		private CodeIdentifiers _typeIdentifiers;

		// Token: 0x040015B3 RID: 5555
		private StringCollection _warnings = new StringCollection();

		// Token: 0x040015B4 RID: 5556
		internal Hashtable MappedTypes;

		// Token: 0x040015B5 RID: 5557
		internal Hashtable DataMappedTypes;

		// Token: 0x040015B6 RID: 5558
		internal Hashtable SharedAnonymousTypes;
	}
}
