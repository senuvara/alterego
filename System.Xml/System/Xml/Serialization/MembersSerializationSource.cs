﻿using System;
using System.Globalization;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x020002F5 RID: 757
	internal class MembersSerializationSource : SerializationSource
	{
		// Token: 0x06001BE3 RID: 7139 RVA: 0x000A1730 File Offset: 0x0009F930
		public MembersSerializationSource(string elementName, bool hasWrapperElement, XmlReflectionMember[] members, bool writeAccessors, bool literalFormat, string namspace, Type[] includedTypes) : base(namspace, includedTypes)
		{
			this.elementName = elementName;
			this.hasWrapperElement = hasWrapperElement;
			this.literalFormat = literalFormat;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(members.Length.ToString(CultureInfo.InvariantCulture));
			for (int i = 0; i < members.Length; i++)
			{
				members[i].AddKeyHash(stringBuilder);
			}
			this.membersHash = stringBuilder.ToString();
		}

		// Token: 0x06001BE4 RID: 7140 RVA: 0x000A17A0 File Offset: 0x0009F9A0
		public override bool Equals(object o)
		{
			MembersSerializationSource membersSerializationSource = o as MembersSerializationSource;
			return membersSerializationSource != null && !(this.literalFormat = membersSerializationSource.literalFormat) && !(this.elementName != membersSerializationSource.elementName) && this.hasWrapperElement == membersSerializationSource.hasWrapperElement && !(this.membersHash != membersSerializationSource.membersHash) && base.BaseEquals(membersSerializationSource);
		}

		// Token: 0x06001BE5 RID: 7141 RVA: 0x000A180D File Offset: 0x0009FA0D
		public override int GetHashCode()
		{
			return this.membersHash.GetHashCode();
		}

		// Token: 0x0400160E RID: 5646
		private string elementName;

		// Token: 0x0400160F RID: 5647
		private bool hasWrapperElement;

		// Token: 0x04001610 RID: 5648
		private string membersHash;

		// Token: 0x04001611 RID: 5649
		private bool literalFormat;
	}
}
