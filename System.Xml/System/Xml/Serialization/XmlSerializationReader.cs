﻿using System;
using System.Collections;
using System.Globalization;
using System.Reflection;

namespace System.Xml.Serialization
{
	/// <summary>Controls deserialization by the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class. </summary>
	// Token: 0x0200032A RID: 810
	[MonoTODO]
	public abstract class XmlSerializationReader : XmlSerializationGeneratedCode
	{
		// Token: 0x06001E55 RID: 7765 RVA: 0x000AEBD8 File Offset: 0x000ACDD8
		internal void Initialize(XmlReader reader, XmlSerializer eventSource)
		{
			this.w3SchemaNS = reader.NameTable.Add("http://www.w3.org/2001/XMLSchema");
			this.w3InstanceNS = reader.NameTable.Add("http://www.w3.org/2001/XMLSchema-instance");
			this.w3InstanceNS2000 = reader.NameTable.Add("http://www.w3.org/2000/10/XMLSchema-instance");
			this.w3InstanceNS1999 = reader.NameTable.Add("http://www.w3.org/1999/XMLSchema-instance");
			this.soapNS = reader.NameTable.Add("http://schemas.xmlsoap.org/soap/encoding/");
			this.wsdlNS = reader.NameTable.Add("http://schemas.xmlsoap.org/wsdl/");
			this.nullX = reader.NameTable.Add("null");
			this.nil = reader.NameTable.Add("nil");
			this.typeX = reader.NameTable.Add("type");
			this.arrayType = reader.NameTable.Add("arrayType");
			this.reader = reader;
			this.eventSource = eventSource;
			this.arrayQName = new XmlQualifiedName("Array", this.soapNS);
			this.InitIDs();
		}

		// Token: 0x06001E56 RID: 7766 RVA: 0x000AECEB File Offset: 0x000ACEEB
		private ArrayList EnsureArrayList(ArrayList list)
		{
			if (list == null)
			{
				list = new ArrayList();
			}
			return list;
		}

		// Token: 0x06001E57 RID: 7767 RVA: 0x000AECF8 File Offset: 0x000ACEF8
		private Hashtable EnsureHashtable(Hashtable hash)
		{
			if (hash == null)
			{
				hash = new Hashtable();
			}
			return hash;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializationReader" /> class.</summary>
		// Token: 0x06001E58 RID: 7768 RVA: 0x000AED05 File Offset: 0x000ACF05
		protected XmlSerializationReader()
		{
		}

		/// <summary>Gets the XML document object into which the XML document is being deserialized. </summary>
		/// <returns>An <see cref="T:System.Xml.XmlDocument" /> that represents the deserialized <see cref="T:System.Xml.XmlDocument" /> data.</returns>
		// Token: 0x170005D4 RID: 1492
		// (get) Token: 0x06001E59 RID: 7769 RVA: 0x000AED0D File Offset: 0x000ACF0D
		protected XmlDocument Document
		{
			get
			{
				if (this.document == null)
				{
					this.document = new XmlDocument(this.reader.NameTable);
				}
				return this.document;
			}
		}

		/// <summary>Gets the <see cref="T:System.Xml.XmlReader" /> object that is being used by <see cref="T:System.Xml.Serialization.XmlSerializationReader" />. </summary>
		/// <returns>The <see cref="T:System.Xml.XmlReader" /> that is being used by the <see cref="T:System.Xml.Serialization.XmlSerializationReader" />.</returns>
		// Token: 0x170005D5 RID: 1493
		// (get) Token: 0x06001E5A RID: 7770 RVA: 0x000AED33 File Offset: 0x000ACF33
		protected XmlReader Reader
		{
			get
			{
				return this.reader;
			}
		}

		/// <summary>Gets or sets a value that should be <see langword="true" /> for a SOAP 1.1 return value.</summary>
		/// <returns>
		///     <see langword="true" />, if the value is a return value. </returns>
		// Token: 0x170005D6 RID: 1494
		// (get) Token: 0x06001E5B RID: 7771 RVA: 0x0000A6CF File Offset: 0x000088CF
		// (set) Token: 0x06001E5C RID: 7772 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected bool IsReturnValue
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the current count of the <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <returns>The current count of an <see cref="T:System.Xml.XmlReader" />.</returns>
		// Token: 0x170005D7 RID: 1495
		// (get) Token: 0x06001E5D RID: 7773 RVA: 0x000AED3B File Offset: 0x000ACF3B
		protected int ReaderCount
		{
			get
			{
				return this.readCount;
			}
		}

		/// <summary>Stores an object that contains a callback method that will be called, as necessary, to fill in .NET Framework collections or enumerations that map to SOAP-encoded arrays or SOAP-encoded, multi-referenced elements. </summary>
		/// <param name="fixup">A <see cref="T:System.Xml.Serialization.XmlSerializationCollectionFixupCallback" /> delegate and the callback method's input data.</param>
		// Token: 0x06001E5E RID: 7774 RVA: 0x000AED44 File Offset: 0x000ACF44
		protected void AddFixup(XmlSerializationReader.CollectionFixup fixup)
		{
			this.collFixups = this.EnsureHashtable(this.collFixups);
			this.collFixups[fixup.Id] = fixup;
			if (this.delayedListFixups != null && this.delayedListFixups.ContainsKey(fixup.Id))
			{
				fixup.CollectionItems = this.delayedListFixups[fixup.Id];
				this.delayedListFixups.Remove(fixup.Id);
			}
		}

		/// <summary>Stores an object that contains a callback method instance that will be called, as necessary, to fill in the objects in a SOAP-encoded array. </summary>
		/// <param name="fixup">An <see cref="T:System.Xml.Serialization.XmlSerializationFixupCallback" /> delegate and the callback method's input data.</param>
		// Token: 0x06001E5F RID: 7775 RVA: 0x000AEDB8 File Offset: 0x000ACFB8
		protected void AddFixup(XmlSerializationReader.Fixup fixup)
		{
			this.fixups = this.EnsureArrayList(this.fixups);
			this.fixups.Add(fixup);
		}

		// Token: 0x06001E60 RID: 7776 RVA: 0x000AEDD9 File Offset: 0x000ACFD9
		private void AddFixup(XmlSerializationReader.CollectionItemFixup fixup)
		{
			this.collItemFixups = this.EnsureArrayList(this.collItemFixups);
			this.collItemFixups.Add(fixup);
		}

		/// <summary>Stores an implementation of the <see cref="T:System.Xml.Serialization.XmlSerializationReadCallback" /> delegate and its input data for a later invocation. </summary>
		/// <param name="name">The name of the .NET Framework type that is being deserialized.</param>
		/// <param name="ns">The namespace of the .NET Framework type that is being deserialized.</param>
		/// <param name="type">The <see cref="T:System.Type" /> to be deserialized.</param>
		/// <param name="read">An <see cref="T:System.Xml.Serialization.XmlSerializationReadCallback" /> delegate.</param>
		// Token: 0x06001E61 RID: 7777 RVA: 0x000AEDFC File Offset: 0x000ACFFC
		protected void AddReadCallback(string name, string ns, Type type, XmlSerializationReadCallback read)
		{
			XmlSerializationReader.WriteCallbackInfo writeCallbackInfo = new XmlSerializationReader.WriteCallbackInfo();
			writeCallbackInfo.Type = type;
			writeCallbackInfo.TypeName = name;
			writeCallbackInfo.TypeNs = ns;
			writeCallbackInfo.Callback = read;
			this.typesCallbacks = this.EnsureHashtable(this.typesCallbacks);
			this.typesCallbacks.Add(new XmlQualifiedName(name, ns), writeCallbackInfo);
		}

		/// <summary>Stores an object that is being deserialized from a SOAP-encoded <see langword="multiRef" /> element for later access through the <see cref="M:System.Xml.Serialization.XmlSerializationReader.GetTarget(System.String)" /> method. </summary>
		/// <param name="id">The value of the <see langword="id" /> attribute of a <see langword="multiRef" /> element that identifies the element.</param>
		/// <param name="o">The object that is deserialized from the XML element.</param>
		// Token: 0x06001E62 RID: 7778 RVA: 0x000AEE54 File Offset: 0x000AD054
		protected void AddTarget(string id, object o)
		{
			if (id != null)
			{
				this.targets = this.EnsureHashtable(this.targets);
				if (this.targets[id] == null)
				{
					this.targets.Add(id, o);
					return;
				}
			}
			else
			{
				if (o != null)
				{
					return;
				}
				this.noIDTargets = this.EnsureArrayList(this.noIDTargets);
				this.noIDTargets.Add(o);
			}
		}

		// Token: 0x06001E63 RID: 7779 RVA: 0x000AEEB8 File Offset: 0x000AD0B8
		private string CurrentTag()
		{
			XmlNodeType nodeType = this.reader.NodeType;
			switch (nodeType)
			{
			case XmlNodeType.Element:
				return string.Format("<{0} xmlns='{1}'>", this.reader.LocalName, this.reader.NamespaceURI);
			case XmlNodeType.Attribute:
				return this.reader.Value;
			case XmlNodeType.Text:
				return "CDATA";
			case XmlNodeType.CDATA:
			case XmlNodeType.EntityReference:
				break;
			case XmlNodeType.Entity:
				return "<?";
			case XmlNodeType.ProcessingInstruction:
				return "<--";
			default:
				if (nodeType == XmlNodeType.EndElement)
				{
					return ">";
				}
				break;
			}
			return "(unknown)";
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that an object being deserialized cannot be instantiated because the constructor throws a security exception.</summary>
		/// <param name="typeName">The name of the type.</param>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001E64 RID: 7780 RVA: 0x000AEF46 File Offset: 0x000AD146
		protected Exception CreateCtorHasSecurityException(string typeName)
		{
			return new InvalidOperationException(string.Format("The type '{0}' cannot be serialized because its parameterless constructor is decorated with declarative security permission attributes. Consider using imperative asserts or demands in the constructor.", typeName));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that an object being deserialized cannot be instantiated because there is no constructor available.</summary>
		/// <param name="typeName">The name of the type.</param>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001E65 RID: 7781 RVA: 0x000AEF58 File Offset: 0x000AD158
		protected Exception CreateInaccessibleConstructorException(string typeName)
		{
			return new InvalidOperationException(string.Format("{0} cannot be serialized because it does not have a default public constructor.", typeName));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that an object being deserialized should be abstract. </summary>
		/// <param name="name">The name of the abstract type.</param>
		/// <param name="ns">The .NET Framework namespace of the abstract type.</param>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001E66 RID: 7782 RVA: 0x000AEF6A File Offset: 0x000AD16A
		protected Exception CreateAbstractTypeException(string name, string ns)
		{
			return new InvalidOperationException(string.Concat(new string[]
			{
				"The specified type is abstrace: name='",
				name,
				"' namespace='",
				ns,
				"', at ",
				this.CurrentTag()
			}));
		}

		/// <summary>Creates an <see cref="T:System.InvalidCastException" /> that indicates that an explicit reference conversion failed.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> that an object cannot be cast to. This type is incorporated into the exception message.</param>
		/// <param name="value">The object that cannot be cast. This object is incorporated into the exception message.</param>
		/// <returns>An <see cref="T:System.InvalidCastException" /> exception.</returns>
		// Token: 0x06001E67 RID: 7783 RVA: 0x000AEFA5 File Offset: 0x000AD1A5
		protected Exception CreateInvalidCastException(Type type, object value)
		{
			return new InvalidCastException(string.Format(CultureInfo.InvariantCulture, "Cannot assign object of type {0} to an object of type {1}.", value.GetType(), type));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a SOAP-encoded collection type cannot be modified and its values cannot be filled in. </summary>
		/// <param name="name">The fully qualified name of the .NET Framework type for which there is a mapping.</param>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001E68 RID: 7784 RVA: 0x000AEFC2 File Offset: 0x000AD1C2
		protected Exception CreateReadOnlyCollectionException(string name)
		{
			return new InvalidOperationException(string.Format("Could not serialize {0}. Default constructors are required for collections and enumerators.", name));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that an enumeration value is not valid. </summary>
		/// <param name="value">The enumeration value that is not valid.</param>
		/// <param name="enumType">The enumeration type.</param>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001E69 RID: 7785 RVA: 0x000AEFD4 File Offset: 0x000AD1D4
		protected Exception CreateUnknownConstantException(string value, Type enumType)
		{
			return new InvalidOperationException(string.Format("'{0}' is not a valid value for {1}.", value, enumType));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that the current position of <see cref="T:System.Xml.XmlReader" /> represents an unknown XML node. </summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001E6A RID: 7786 RVA: 0x000AEFE7 File Offset: 0x000AD1E7
		protected Exception CreateUnknownNodeException()
		{
			return new InvalidOperationException(this.CurrentTag() + " was not expected");
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a type is unknown. </summary>
		/// <param name="type">An <see cref="T:System.Xml.XmlQualifiedName" /> that represents the name of the unknown type.</param>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001E6B RID: 7787 RVA: 0x000AF000 File Offset: 0x000AD200
		protected Exception CreateUnknownTypeException(XmlQualifiedName type)
		{
			return new InvalidOperationException(string.Concat(new string[]
			{
				"The specified type was not recognized: name='",
				type.Name,
				"' namespace='",
				type.Namespace,
				"', at ",
				this.CurrentTag()
			}));
		}

		/// <summary>Checks whether the deserializer has advanced.</summary>
		/// <param name="whileIterations">The current <see langword="count" /> in a while loop.</param>
		/// <param name="readerCount">The current <see cref="P:System.Xml.Serialization.XmlSerializationReader.ReaderCount" />. </param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Xml.Serialization.XmlSerializationReader.ReaderCount" /> has not advanced. </exception>
		// Token: 0x06001E6C RID: 7788 RVA: 0x000AF050 File Offset: 0x000AD250
		protected void CheckReaderCount(ref int whileIterations, ref int readerCount)
		{
			whileIterations = this.whileIterationCount;
			readerCount = this.readCount;
		}

		/// <summary>Ensures that a given array, or a copy, is large enough to contain a specified index. </summary>
		/// <param name="a">The <see cref="T:System.Array" /> that is being checked.</param>
		/// <param name="index">The required index.</param>
		/// <param name="elementType">The <see cref="T:System.Type" /> of the array's elements.</param>
		/// <returns>The existing <see cref="T:System.Array" />, if it is already large enough; otherwise, a new, larger array that contains the original array's elements.</returns>
		// Token: 0x06001E6D RID: 7789 RVA: 0x000AF064 File Offset: 0x000AD264
		protected Array EnsureArrayIndex(Array a, int index, Type elementType)
		{
			if (a != null && index < a.Length)
			{
				return a;
			}
			int length;
			if (a == null)
			{
				length = 32;
			}
			else
			{
				length = a.Length * 2;
			}
			Array array = Array.CreateInstance(elementType, length);
			if (a != null)
			{
				Array.Copy(a, array, index);
			}
			return array;
		}

		/// <summary>Fills in the values of a SOAP-encoded array whose data type maps to a .NET Framework reference type.</summary>
		/// <param name="fixup">An object that contains the array whose values are filled in.</param>
		// Token: 0x06001E6E RID: 7790 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected void FixupArrayRefs(object fixup)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the length of the SOAP-encoded array where the <see cref="T:System.Xml.XmlReader" /> is currently positioned.</summary>
		/// <param name="name">The local name that the array should have.</param>
		/// <param name="ns">The namespace that the array should have.</param>
		/// <returns>The length of the SOAP array.</returns>
		// Token: 0x06001E6F RID: 7791 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected int GetArrayLength(string name, string ns)
		{
			throw new NotImplementedException();
		}

		/// <summary>Determines whether the XML element where the <see cref="T:System.Xml.XmlReader" /> is currently positioned has a null attribute set to the value <see langword="true" />.</summary>
		/// <returns>
		///     <see langword="true" /> if <see cref="T:System.Xml.XmlReader" /> is currently positioned over a null attribute with the value <see langword="true" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001E70 RID: 7792 RVA: 0x000AF0A4 File Offset: 0x000AD2A4
		protected bool GetNullAttr()
		{
			string attribute = this.reader.GetAttribute(this.nullX, this.w3InstanceNS);
			if (attribute == null)
			{
				attribute = this.reader.GetAttribute(this.nil, this.w3InstanceNS);
				if (attribute == null)
				{
					attribute = this.reader.GetAttribute(this.nullX, this.w3InstanceNS2000);
					if (attribute == null)
					{
						attribute = this.reader.GetAttribute(this.nullX, this.w3InstanceNS1999);
					}
				}
			}
			return attribute != null;
		}

		/// <summary>Gets an object that is being deserialized from a SOAP-encoded <see langword="multiRef" /> element and that was stored earlier by <see cref="M:System.Xml.Serialization.XmlSerializationReader.AddTarget(System.String,System.Object)" />.  </summary>
		/// <param name="id">The value of the <see langword="id" /> attribute of a <see langword="multiRef" /> element that identifies the element.</param>
		/// <returns>An object to be deserialized from a SOAP-encoded <see langword="multiRef" /> element.</returns>
		// Token: 0x06001E71 RID: 7793 RVA: 0x000AF120 File Offset: 0x000AD320
		protected object GetTarget(string id)
		{
			if (this.targets == null)
			{
				return null;
			}
			object obj = this.targets[id];
			if (obj != null)
			{
				if (this.referencedObjects == null)
				{
					this.referencedObjects = new Hashtable();
				}
				this.referencedObjects[obj] = obj;
			}
			return obj;
		}

		// Token: 0x06001E72 RID: 7794 RVA: 0x000AF168 File Offset: 0x000AD368
		private bool TargetReady(string id)
		{
			return this.targets != null && this.targets.ContainsKey(id);
		}

		/// <summary>Gets the value of the <see langword="xsi:type" /> attribute for the XML element at the current location of the <see cref="T:System.Xml.XmlReader" />. </summary>
		/// <returns>An XML qualified name that indicates the data type of an XML element.</returns>
		// Token: 0x06001E73 RID: 7795 RVA: 0x000AF180 File Offset: 0x000AD380
		protected XmlQualifiedName GetXsiType()
		{
			string attribute = this.Reader.GetAttribute(this.typeX, "http://www.w3.org/2001/XMLSchema-instance");
			if (attribute == string.Empty || attribute == null)
			{
				attribute = this.Reader.GetAttribute(this.typeX, this.w3InstanceNS1999);
				if (attribute == string.Empty || attribute == null)
				{
					attribute = this.Reader.GetAttribute(this.typeX, this.w3InstanceNS2000);
					if (attribute == string.Empty || attribute == null)
					{
						return null;
					}
				}
			}
			int num = attribute.IndexOf(":");
			if (num == -1)
			{
				return new XmlQualifiedName(attribute, this.Reader.NamespaceURI);
			}
			string prefix = attribute.Substring(0, num);
			return new XmlQualifiedName(attribute.Substring(num + 1), this.Reader.LookupNamespace(prefix));
		}

		/// <summary>Initializes callback methods that populate objects that map to SOAP-encoded XML data. </summary>
		// Token: 0x06001E74 RID: 7796
		protected abstract void InitCallbacks();

		/// <summary>Stores element and attribute names in a <see cref="T:System.Xml.NameTable" /> object. </summary>
		// Token: 0x06001E75 RID: 7797
		protected abstract void InitIDs();

		/// <summary>Determines whether an XML attribute name indicates an XML namespace. </summary>
		/// <param name="name">The name of an XML attribute.</param>
		/// <returns>
		///     <see langword="true " />if the XML attribute name indicates an XML namespace; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001E76 RID: 7798 RVA: 0x000AF24C File Offset: 0x000AD44C
		protected bool IsXmlnsAttribute(string name)
		{
			int length = name.Length;
			if (length < 5)
			{
				return false;
			}
			if (length == 5)
			{
				return name == "xmlns";
			}
			return name.StartsWith("xmlns:");
		}

		/// <summary>Sets the value of the XML attribute if it is of type <see langword="arrayType" /> from the Web Services Description Language (WSDL) namespace. </summary>
		/// <param name="attr">An <see cref="T:System.Xml.XmlAttribute" /> that may have the type <see langword="wsdl:array" />.</param>
		// Token: 0x06001E77 RID: 7799 RVA: 0x000AF284 File Offset: 0x000AD484
		protected void ParseWsdlArrayType(XmlAttribute attr)
		{
			if (attr.NamespaceURI == this.wsdlNS && attr.LocalName == this.arrayType)
			{
				string text = "";
				string str;
				string str2;
				TypeTranslator.ParseArrayType(attr.Value, out str, out text, out str2);
				if (text != "")
				{
					text = this.Reader.LookupNamespace(text) + ":";
				}
				attr.Value = text + str + str2;
			}
		}

		/// <summary>Makes the <see cref="T:System.Xml.XmlReader" /> read the fully qualified name of the element where it is currently positioned. </summary>
		/// <returns>The fully qualified name of the current XML element.</returns>
		// Token: 0x06001E78 RID: 7800 RVA: 0x000AF300 File Offset: 0x000AD500
		protected XmlQualifiedName ReadElementQualifiedName()
		{
			this.readCount++;
			if (this.reader.IsEmptyElement)
			{
				this.reader.Skip();
				return this.ToXmlQualifiedName(string.Empty);
			}
			this.reader.ReadStartElement();
			XmlQualifiedName result = this.ToXmlQualifiedName(this.reader.ReadString());
			this.reader.ReadEndElement();
			return result;
		}

		/// <summary>Makes the <see cref="T:System.Xml.XmlReader" /> read an XML end tag.</summary>
		// Token: 0x06001E79 RID: 7801 RVA: 0x000AF368 File Offset: 0x000AD568
		protected void ReadEndElement()
		{
			this.readCount++;
			while (this.reader.NodeType == XmlNodeType.Whitespace)
			{
				this.reader.Skip();
			}
			if (this.reader.NodeType != XmlNodeType.None)
			{
				this.reader.ReadEndElement();
				return;
			}
			this.reader.Skip();
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlReader" /> to read the current XML element if the element has a null attribute with the value true. </summary>
		/// <returns>
		///     <see langword="true" /> if the element has a null="true" attribute value and has been read; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001E7A RID: 7802 RVA: 0x000AF3C4 File Offset: 0x000AD5C4
		protected bool ReadNull()
		{
			if (!this.GetNullAttr())
			{
				return false;
			}
			this.readCount++;
			if (this.reader.IsEmptyElement)
			{
				this.reader.Skip();
				return true;
			}
			this.reader.ReadStartElement();
			while (this.reader.NodeType != XmlNodeType.EndElement)
			{
				this.UnknownNode(null);
			}
			this.ReadEndElement();
			return true;
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlReader" /> to read the fully qualified name of the element where it is currently positioned. </summary>
		/// <returns>A <see cref="T:System.Xml.XmlQualifiedName" /> that represents the fully qualified name of the current XML element; otherwise, <see langword="null" /> if a null="true" attribute value is present.</returns>
		// Token: 0x06001E7B RID: 7803 RVA: 0x000AF42D File Offset: 0x000AD62D
		protected XmlQualifiedName ReadNullableQualifiedName()
		{
			if (this.ReadNull())
			{
				return null;
			}
			return this.ReadElementQualifiedName();
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlReader" /> to read a simple, text-only XML element that could be <see langword="null" />. </summary>
		/// <returns>The string value; otherwise, <see langword="null" />.</returns>
		// Token: 0x06001E7C RID: 7804 RVA: 0x000AF43F File Offset: 0x000AD63F
		protected string ReadNullableString()
		{
			if (this.ReadNull())
			{
				return null;
			}
			this.readCount++;
			return this.reader.ReadElementString();
		}

		/// <summary>Reads the value of the <see langword="href" /> attribute (<see langword="ref" /> attribute for SOAP 1.2) that is used to refer to an XML element in SOAP encoding. </summary>
		/// <param name="fixupReference">An output string into which the <see langword="href" /> attribute value is read.</param>
		/// <returns>
		///     <see langword="true" /> if the value was read; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001E7D RID: 7805 RVA: 0x000AF464 File Offset: 0x000AD664
		protected bool ReadReference(out string fixupReference)
		{
			string attribute = this.reader.GetAttribute("href");
			if (attribute == null)
			{
				fixupReference = null;
				return false;
			}
			if (attribute[0] != '#')
			{
				throw new InvalidOperationException("href not found: " + attribute);
			}
			fixupReference = attribute.Substring(1);
			this.readCount++;
			if (!this.reader.IsEmptyElement)
			{
				this.reader.ReadStartElement();
				this.ReadEndElement();
			}
			else
			{
				this.reader.Skip();
			}
			return true;
		}

		/// <summary>Deserializes an object from a SOAP-encoded <see langword="multiRef" /> XML element. </summary>
		/// <returns>The value of the referenced element in the document.</returns>
		// Token: 0x06001E7E RID: 7806 RVA: 0x000AF4E9 File Offset: 0x000AD6E9
		protected object ReadReferencedElement()
		{
			return this.ReadReferencedElement(this.Reader.LocalName, this.Reader.NamespaceURI);
		}

		// Token: 0x06001E7F RID: 7807 RVA: 0x000AF507 File Offset: 0x000AD707
		private XmlSerializationReader.WriteCallbackInfo GetCallbackInfo(XmlQualifiedName qname)
		{
			if (this.typesCallbacks == null)
			{
				this.typesCallbacks = new Hashtable();
				this.InitCallbacks();
			}
			return (XmlSerializationReader.WriteCallbackInfo)this.typesCallbacks[qname];
		}

		/// <summary>Deserializes an object from a SOAP-encoded <see langword="multiRef" /> XML element. </summary>
		/// <param name="name">The local name of the element's XML Schema data type.</param>
		/// <param name="ns">The namespace of the element's XML Schema data type.</param>
		/// <returns>The value of the referenced element in the document.</returns>
		// Token: 0x06001E80 RID: 7808 RVA: 0x000AF534 File Offset: 0x000AD734
		protected object ReadReferencedElement(string name, string ns)
		{
			XmlQualifiedName xmlQualifiedName = this.GetXsiType();
			if (xmlQualifiedName == null)
			{
				xmlQualifiedName = new XmlQualifiedName(name, ns);
			}
			string attribute = this.Reader.GetAttribute("id");
			string attribute2 = this.Reader.GetAttribute(this.arrayType, this.soapNS);
			object obj;
			if (xmlQualifiedName == this.arrayQName || (attribute2 != null && attribute2.Length > 0))
			{
				XmlSerializationReader.CollectionFixup collectionFixup = (this.collFixups != null) ? ((XmlSerializationReader.CollectionFixup)this.collFixups[attribute]) : null;
				if (this.ReadList(out obj))
				{
					if (collectionFixup != null)
					{
						collectionFixup.Callback(collectionFixup.Collection, obj);
						this.collFixups.Remove(attribute);
						obj = collectionFixup.Collection;
					}
				}
				else if (collectionFixup != null)
				{
					collectionFixup.CollectionItems = (object[])obj;
					obj = collectionFixup.Collection;
				}
			}
			else
			{
				XmlSerializationReader.WriteCallbackInfo callbackInfo = this.GetCallbackInfo(xmlQualifiedName);
				if (callbackInfo == null)
				{
					obj = this.ReadTypedPrimitive(xmlQualifiedName, attribute != null);
				}
				else
				{
					obj = callbackInfo.Callback();
				}
			}
			this.AddTarget(attribute, obj);
			return obj;
		}

		// Token: 0x06001E81 RID: 7809 RVA: 0x000AF640 File Offset: 0x000AD840
		private bool ReadList(out object resultList)
		{
			string attribute = this.Reader.GetAttribute(this.arrayType, this.soapNS);
			if (attribute == null)
			{
				attribute = this.Reader.GetAttribute(this.arrayType, this.wsdlNS);
			}
			XmlQualifiedName xmlQualifiedName = this.ToXmlQualifiedName(attribute);
			int num = xmlQualifiedName.Name.LastIndexOf('[');
			string text = xmlQualifiedName.Name.Substring(num);
			string text2 = xmlQualifiedName.Name.Substring(0, num);
			int num2 = int.Parse(text.Substring(1, text.Length - 2), CultureInfo.InvariantCulture);
			num = text2.IndexOf('[');
			if (num == -1)
			{
				num = text2.Length;
			}
			string text3 = text2.Substring(0, num);
			string typeName;
			if (xmlQualifiedName.Namespace == this.w3SchemaNS)
			{
				typeName = TypeTranslator.GetPrimitiveTypeData(text3).Type.FullName + text2.Substring(num);
			}
			else
			{
				XmlSerializationReader.WriteCallbackInfo callbackInfo = this.GetCallbackInfo(new XmlQualifiedName(text3, xmlQualifiedName.Namespace));
				typeName = callbackInfo.Type.FullName + text2.Substring(num) + ", " + callbackInfo.Type.Assembly.FullName;
			}
			Array array = Array.CreateInstance(Type.GetType(typeName), num2);
			bool result = true;
			if (this.Reader.IsEmptyElement)
			{
				this.readCount++;
				this.Reader.Skip();
			}
			else
			{
				this.Reader.ReadStartElement();
				for (int i = 0; i < num2; i++)
				{
					this.whileIterationCount++;
					this.readCount++;
					this.Reader.MoveToContent();
					string text4;
					object value = this.ReadReferencingElement(text2, xmlQualifiedName.Namespace, out text4);
					if (text4 == null)
					{
						array.SetValue(value, i);
					}
					else
					{
						this.AddFixup(new XmlSerializationReader.CollectionItemFixup(array, i, text4));
						result = false;
					}
				}
				this.whileIterationCount = 0;
				this.Reader.ReadEndElement();
			}
			resultList = array;
			return result;
		}

		/// <summary>Deserializes objects from the SOAP-encoded <see langword="multiRef" /> elements in a SOAP message. </summary>
		// Token: 0x06001E82 RID: 7810 RVA: 0x000AF83C File Offset: 0x000ADA3C
		protected void ReadReferencedElements()
		{
			this.reader.MoveToContent();
			XmlNodeType nodeType = this.reader.NodeType;
			while (nodeType != XmlNodeType.EndElement && nodeType != XmlNodeType.None)
			{
				this.whileIterationCount++;
				this.readCount++;
				this.ReadReferencedElement();
				this.reader.MoveToContent();
				nodeType = this.reader.NodeType;
			}
			this.whileIterationCount = 0;
			if (this.delayedListFixups != null)
			{
				foreach (object obj in this.delayedListFixups)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					this.AddTarget((string)dictionaryEntry.Key, dictionaryEntry.Value);
				}
			}
			if (this.collItemFixups != null)
			{
				foreach (object obj2 in this.collItemFixups)
				{
					XmlSerializationReader.CollectionItemFixup collectionItemFixup = (XmlSerializationReader.CollectionItemFixup)obj2;
					collectionItemFixup.Collection.SetValue(this.GetTarget(collectionItemFixup.Id), collectionItemFixup.Index);
				}
			}
			if (this.collFixups != null)
			{
				foreach (object obj3 in this.collFixups.Values)
				{
					XmlSerializationReader.CollectionFixup collectionFixup = (XmlSerializationReader.CollectionFixup)obj3;
					collectionFixup.Callback(collectionFixup.Collection, collectionFixup.CollectionItems);
				}
			}
			if (this.fixups != null)
			{
				foreach (object obj4 in this.fixups)
				{
					XmlSerializationReader.Fixup fixup = (XmlSerializationReader.Fixup)obj4;
					fixup.Callback(fixup);
				}
			}
			if (this.targets != null)
			{
				foreach (object obj5 in this.targets)
				{
					DictionaryEntry dictionaryEntry2 = (DictionaryEntry)obj5;
					if (dictionaryEntry2.Value != null && (this.referencedObjects == null || !this.referencedObjects.Contains(dictionaryEntry2.Value)))
					{
						this.UnreferencedObject((string)dictionaryEntry2.Key, dictionaryEntry2.Value);
					}
				}
			}
		}

		/// <summary>Deserializes an object from an XML element in a SOAP message that contains a reference to a <see langword="multiRef" /> element. </summary>
		/// <param name="fixupReference">An output string into which the <see langword="href" /> attribute value is read.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06001E83 RID: 7811 RVA: 0x000AFAD4 File Offset: 0x000ADCD4
		protected object ReadReferencingElement(out string fixupReference)
		{
			return this.ReadReferencingElement(this.Reader.LocalName, this.Reader.NamespaceURI, false, out fixupReference);
		}

		/// <summary>Deserializes an object from an XML element in a SOAP message that contains a reference to a <see langword="multiRef" /> element. </summary>
		/// <param name="name">The local name of the element's XML Schema data type.</param>
		/// <param name="ns">The namespace of the element's XML Schema data type.</param>
		/// <param name="fixupReference">An output string into which the <see langword="href" /> attribute value is read.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06001E84 RID: 7812 RVA: 0x000AFAF4 File Offset: 0x000ADCF4
		protected object ReadReferencingElement(string name, string ns, out string fixupReference)
		{
			return this.ReadReferencingElement(name, ns, false, out fixupReference);
		}

		/// <summary>Deserializes an object from an XML element in a SOAP message that contains a reference to a <see langword="multiRef" /> element.</summary>
		/// <param name="name">The local name of the element's XML Schema data type.</param>
		/// <param name="ns">The namespace of the element's XML Schema data type.</param>
		/// <param name="elementCanBeType">
		///       <see langword="true" /> if the element name is also the XML Schema data type name; otherwise, <see langword="false" />.</param>
		/// <param name="fixupReference">An output string into which the value of the <see langword="href" /> attribute is read.</param>
		/// <returns>The deserialized object.</returns>
		// Token: 0x06001E85 RID: 7813 RVA: 0x000AFB00 File Offset: 0x000ADD00
		protected object ReadReferencingElement(string name, string ns, bool elementCanBeType, out string fixupReference)
		{
			if (this.ReadNull())
			{
				fixupReference = null;
				return null;
			}
			string text = this.Reader.GetAttribute("href");
			if (text == string.Empty || text == null)
			{
				fixupReference = null;
				XmlQualifiedName xmlQualifiedName = this.GetXsiType();
				if (xmlQualifiedName == null)
				{
					xmlQualifiedName = new XmlQualifiedName(name, ns);
				}
				string attribute = this.Reader.GetAttribute(this.arrayType, this.soapNS);
				if (xmlQualifiedName == this.arrayQName || attribute != null)
				{
					this.delayedListFixups = this.EnsureHashtable(this.delayedListFixups);
					object arg = "__<";
					int num = this.delayedFixupId;
					this.delayedFixupId = num + 1;
					fixupReference = arg + num + ">";
					object value;
					this.ReadList(out value);
					this.delayedListFixups[fixupReference] = value;
					return null;
				}
				XmlSerializationReader.WriteCallbackInfo callbackInfo = this.GetCallbackInfo(xmlQualifiedName);
				if (callbackInfo == null)
				{
					return this.ReadTypedPrimitive(xmlQualifiedName, true);
				}
				return callbackInfo.Callback();
			}
			else
			{
				if (text.StartsWith("#"))
				{
					text = text.Substring(1);
				}
				this.readCount++;
				this.Reader.Skip();
				if (this.TargetReady(text))
				{
					fixupReference = null;
					return this.GetTarget(text);
				}
				fixupReference = text;
				return null;
			}
		}

		/// <summary>Populates an object from its XML representation at the current location of the <see cref="T:System.Xml.XmlReader" />. </summary>
		/// <param name="serializable">An <see cref="T:System.Xml.Serialization.IXmlSerializable" /> that corresponds to the current position of the <see cref="T:System.Xml.XmlReader" />.</param>
		/// <returns>An object that implements the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> interface with its members populated from the location of the <see cref="T:System.Xml.XmlReader" />.</returns>
		// Token: 0x06001E86 RID: 7814 RVA: 0x000AFC48 File Offset: 0x000ADE48
		protected IXmlSerializable ReadSerializable(IXmlSerializable serializable)
		{
			if (this.ReadNull())
			{
				return null;
			}
			int depth = this.reader.Depth;
			this.readCount++;
			serializable.ReadXml(this.reader);
			this.Reader.MoveToContent();
			while (this.reader.Depth > depth)
			{
				this.reader.Skip();
			}
			if (this.reader.Depth == depth && this.reader.NodeType == XmlNodeType.EndElement)
			{
				this.reader.ReadEndElement();
			}
			return serializable;
		}

		/// <summary>This method supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		/// <param name="serializable">An IXmlSerializable object that corresponds to the current position of the XMLReader.</param>
		/// <param name="wrappedAny">Specifies whether the serializable object is wrapped.</param>
		/// <returns>An object that implements the IXmlSerializable interface with its members populated from the location of the XmlReader.</returns>
		// Token: 0x06001E87 RID: 7815 RVA: 0x000AFCD8 File Offset: 0x000ADED8
		protected IXmlSerializable ReadSerializable(IXmlSerializable serializable, bool wrappedAny)
		{
			string b = null;
			string b2 = null;
			if (wrappedAny)
			{
				b = this.reader.LocalName;
				b2 = this.reader.NamespaceURI;
				this.reader.Read();
				this.reader.MoveToContent();
			}
			serializable.ReadXml(this.reader);
			if (wrappedAny)
			{
				while (this.reader.NodeType == XmlNodeType.Whitespace)
				{
					this.reader.Skip();
				}
				if (this.reader.NodeType == XmlNodeType.None)
				{
					this.reader.Skip();
				}
				if (this.reader.NodeType == XmlNodeType.EndElement && this.reader.LocalName == b && this.reader.NamespaceURI == b2)
				{
					this.reader.Read();
				}
			}
			return serializable;
		}

		/// <summary>Produces the result of a call to the <see cref="M:System.Xml.XmlReader.ReadString" /> method appended to the input value. </summary>
		/// <param name="value">A string to prefix to the result of a call to the <see cref="M:System.Xml.XmlReader.ReadString" /> method.</param>
		/// <returns>The result of call to the <see cref="M:System.Xml.XmlReader.ReadString" /> method appended to the input value.</returns>
		// Token: 0x06001E88 RID: 7816 RVA: 0x000AFDA1 File Offset: 0x000ADFA1
		protected string ReadString(string value)
		{
			this.readCount++;
			if (value == null || value == string.Empty)
			{
				return this.reader.ReadString();
			}
			return value + this.reader.ReadString();
		}

		/// <summary>Gets the value of the XML node at which the <see cref="T:System.Xml.XmlReader" /> is currently positioned. </summary>
		/// <param name="type">The <see cref="T:System.Xml.XmlQualifiedName" /> that represents the simple data type for the current location of the <see cref="T:System.Xml.XmlReader" />.</param>
		/// <returns>The value of the node as a .NET Framework value type, if the value is a simple XML Schema data type.</returns>
		// Token: 0x06001E89 RID: 7817 RVA: 0x000AFDDE File Offset: 0x000ADFDE
		protected object ReadTypedPrimitive(XmlQualifiedName type)
		{
			return this.ReadTypedPrimitive(type, false);
		}

		// Token: 0x06001E8A RID: 7818 RVA: 0x000AFDE8 File Offset: 0x000ADFE8
		private object ReadTypedPrimitive(XmlQualifiedName qname, bool reportUnknown)
		{
			if (qname == null)
			{
				qname = this.GetXsiType();
			}
			TypeData typeData = TypeTranslator.FindPrimitiveTypeData(qname.Name);
			if (typeData == null || typeData.SchemaType != SchemaTypes.Primitive)
			{
				this.readCount++;
				XmlNode xmlNode = this.Document.ReadNode(this.reader);
				if (reportUnknown)
				{
					this.OnUnknownNode(xmlNode, null, null);
				}
				if (xmlNode.ChildNodes.Count == 0 && xmlNode.Attributes.Count == 0)
				{
					return new object();
				}
				XmlElement xmlElement = xmlNode as XmlElement;
				if (xmlElement == null)
				{
					return new XmlNode[]
					{
						xmlNode
					};
				}
				XmlNode[] array = new XmlNode[xmlElement.Attributes.Count + xmlElement.ChildNodes.Count];
				int num = 0;
				foreach (object obj in xmlElement.Attributes)
				{
					XmlNode xmlNode2 = (XmlNode)obj;
					array[num++] = xmlNode2;
				}
				foreach (object obj2 in xmlElement.ChildNodes)
				{
					XmlNode xmlNode3 = (XmlNode)obj2;
					array[num++] = xmlNode3;
				}
				return array;
			}
			else
			{
				if (typeData.Type == typeof(XmlQualifiedName))
				{
					return this.ReadNullableQualifiedName();
				}
				this.readCount++;
				return XmlCustomFormatter.FromXmlString(typeData, this.Reader.ReadElementString());
			}
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlReader" /> to read the XML node at its current position. </summary>
		/// <param name="wrapped">
		///       <see langword="true" /> to read content only after reading the element's start element; otherwise, <see langword="false" />.</param>
		/// <returns>An <see cref="T:System.Xml.XmlNode" /> that represents the XML node that has been read.</returns>
		// Token: 0x06001E8B RID: 7819 RVA: 0x000AFF90 File Offset: 0x000AE190
		protected XmlNode ReadXmlNode(bool wrapped)
		{
			this.readCount++;
			XmlNode xmlNode = this.Document.ReadNode(this.reader);
			if (wrapped)
			{
				return xmlNode.FirstChild;
			}
			return xmlNode;
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlReader" /> to read an XML document root element at its current position.</summary>
		/// <param name="wrapped">
		///       <see langword="true" /> if the method should read content only after reading the element's start element; otherwise, <see langword="false" />.</param>
		/// <returns>An <see cref="T:System.Xml.XmlDocument" /> that contains the root element that has been read.</returns>
		// Token: 0x06001E8C RID: 7820 RVA: 0x000AFFC8 File Offset: 0x000AE1C8
		protected XmlDocument ReadXmlDocument(bool wrapped)
		{
			this.readCount++;
			if (wrapped)
			{
				this.reader.ReadStartElement();
			}
			this.reader.MoveToContent();
			XmlDocument xmlDocument = new XmlDocument(this.reader.NameTable);
			XmlNode newChild = xmlDocument.ReadNode(this.reader);
			xmlDocument.AppendChild(newChild);
			if (wrapped)
			{
				this.reader.ReadEndElement();
			}
			return xmlDocument;
		}

		/// <summary>Stores an object to be deserialized from a SOAP-encoded <see langword="multiRef" /> element.</summary>
		/// <param name="o">The object to be deserialized.</param>
		// Token: 0x06001E8D RID: 7821 RVA: 0x000B0030 File Offset: 0x000AE230
		protected void Referenced(object o)
		{
			if (o != null)
			{
				if (this.referencedObjects == null)
				{
					this.referencedObjects = new Hashtable();
				}
				this.referencedObjects[o] = o;
			}
		}

		/// <summary>Ensures that a given array, or a copy, is no larger than a specified length. </summary>
		/// <param name="a">The array that is being checked.</param>
		/// <param name="length">The maximum length of the array.</param>
		/// <param name="elementType">The <see cref="T:System.Type" /> of the array's elements.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> if <see langword="null" /> for the array, if present for the input array, can be returned; otherwise, a new, smaller array.</param>
		/// <returns>The existing <see cref="T:System.Array" />, if it is already small enough; otherwise, a new, smaller array that contains the original array's elements up to the size of<paramref name=" length" />.</returns>
		// Token: 0x06001E8E RID: 7822 RVA: 0x000B0058 File Offset: 0x000AE258
		protected Array ShrinkArray(Array a, int length, Type elementType, bool isNullable)
		{
			if (length == 0 && isNullable)
			{
				return null;
			}
			if (a == null)
			{
				return Array.CreateInstance(elementType, length);
			}
			if (a.Length == length)
			{
				return a;
			}
			Array array = Array.CreateInstance(elementType, length);
			Array.Copy(a, array, length);
			return array;
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlReader" /> to read the string value at its current position and return it as a base-64 byte array.</summary>
		/// <param name="isNull">
		///       <see langword="true" /> to return <see langword="null" />; <see langword="false" /> to return a base-64 byte array.</param>
		/// <returns>A base-64 byte array; otherwise, <see langword="null" /> if the value of the <paramref name="isNull" /> parameter is <see langword="true" />.</returns>
		// Token: 0x06001E8F RID: 7823 RVA: 0x000B0097 File Offset: 0x000AE297
		protected byte[] ToByteArrayBase64(bool isNull)
		{
			this.readCount++;
			if (isNull)
			{
				this.Reader.ReadString();
				return null;
			}
			return XmlSerializationReader.ToByteArrayBase64(this.Reader.ReadString());
		}

		/// <summary>Produces a base-64 byte array from an input string. </summary>
		/// <param name="value">A string to translate into a base-64 byte array.</param>
		/// <returns>A base-64 byte array.</returns>
		// Token: 0x06001E90 RID: 7824 RVA: 0x000A55B7 File Offset: 0x000A37B7
		protected static byte[] ToByteArrayBase64(string value)
		{
			return Convert.FromBase64String(value);
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlReader" /> to read the string value at its current position and return it as a hexadecimal byte array.</summary>
		/// <param name="isNull">
		///       <see langword="true" /> to return <see langword="null" />; <see langword="false" /> to return a hexadecimal byte array.</param>
		/// <returns>A hexadecimal byte array; otherwise, <see langword="null" /> if the value of the <paramref name="isNull" /> parameter is true. </returns>
		// Token: 0x06001E91 RID: 7825 RVA: 0x000B00C8 File Offset: 0x000AE2C8
		protected byte[] ToByteArrayHex(bool isNull)
		{
			this.readCount++;
			if (isNull)
			{
				this.Reader.ReadString();
				return null;
			}
			return XmlSerializationReader.ToByteArrayHex(this.Reader.ReadString());
		}

		/// <summary>Produces a hexadecimal byte array from an input string.</summary>
		/// <param name="value">A string to translate into a hexadecimal byte array.</param>
		/// <returns>A hexadecimal byte array.</returns>
		// Token: 0x06001E92 RID: 7826 RVA: 0x000B00F9 File Offset: 0x000AE2F9
		protected static byte[] ToByteArrayHex(string value)
		{
			return XmlConvert.FromBinHexString(value);
		}

		/// <summary>Produces a <see cref="T:System.Char" /> object from an input string. </summary>
		/// <param name="value">A string to translate into a <see cref="T:System.Char" /> object.</param>
		/// <returns>A <see cref="T:System.Char" /> object.</returns>
		// Token: 0x06001E93 RID: 7827 RVA: 0x000B0101 File Offset: 0x000AE301
		protected static char ToChar(string value)
		{
			return XmlCustomFormatter.ToChar(value);
		}

		/// <summary>Produces a <see cref="T:System.DateTime" /> object from an input string. </summary>
		/// <param name="value">A string to translate into a <see cref="T:System.DateTime" /> class object.</param>
		/// <returns>A <see cref="T:System.DateTime" />object.</returns>
		// Token: 0x06001E94 RID: 7828 RVA: 0x000B0109 File Offset: 0x000AE309
		protected static DateTime ToDate(string value)
		{
			return XmlCustomFormatter.ToDate(value);
		}

		/// <summary>Produces a <see cref="T:System.DateTime" /> object from an input string. </summary>
		/// <param name="value">A string to translate into a <see cref="T:System.DateTime" /> object.</param>
		/// <returns>A <see cref="T:System.DateTime" /> object.</returns>
		// Token: 0x06001E95 RID: 7829 RVA: 0x000A55C7 File Offset: 0x000A37C7
		protected static DateTime ToDateTime(string value)
		{
			return XmlCustomFormatter.ToDateTime(value);
		}

		/// <summary>Produces a numeric enumeration value from a string that consists of delimited identifiers that represent constants from the enumerator list. </summary>
		/// <param name="value">A string that consists of delimited identifiers where each identifier represents a constant from the set enumerator list.</param>
		/// <param name="h">A <see cref="T:System.Collections.Hashtable" /> that consists of the identifiers as keys and the constants as integral numbers.</param>
		/// <param name="typeName">The name of the enumeration type.</param>
		/// <returns>A long value that consists of the enumeration value as a series of bitwise <see langword="OR" /> operations.</returns>
		// Token: 0x06001E96 RID: 7830 RVA: 0x000B0111 File Offset: 0x000AE311
		protected static long ToEnum(string value, Hashtable h, string typeName)
		{
			return XmlCustomFormatter.ToEnum(value, h, typeName, true);
		}

		/// <summary>Produces a <see cref="T:System.DateTime" /> from a string that represents the time. </summary>
		/// <param name="value">A string to translate into a <see cref="T:System.DateTime" /> object.</param>
		/// <returns>A <see cref="T:System.DateTime" /> object.</returns>
		// Token: 0x06001E97 RID: 7831 RVA: 0x000B011C File Offset: 0x000AE31C
		protected static DateTime ToTime(string value)
		{
			return XmlCustomFormatter.ToTime(value);
		}

		/// <summary>Decodes an XML name.</summary>
		/// <param name="value">An XML name to be decoded.</param>
		/// <returns>A decoded string.</returns>
		// Token: 0x06001E98 RID: 7832 RVA: 0x000A564B File Offset: 0x000A384B
		protected static string ToXmlName(string value)
		{
			return XmlCustomFormatter.ToXmlName(value);
		}

		/// <summary>Decodes an XML name.</summary>
		/// <param name="value">An XML name to be decoded.</param>
		/// <returns>A decoded string.</returns>
		// Token: 0x06001E99 RID: 7833 RVA: 0x000B0124 File Offset: 0x000AE324
		protected static string ToXmlNCName(string value)
		{
			return XmlCustomFormatter.ToXmlNCName(value);
		}

		/// <summary>Decodes an XML name.</summary>
		/// <param name="value">An XML name to be decoded.</param>
		/// <returns>A decoded string.</returns>
		// Token: 0x06001E9A RID: 7834 RVA: 0x000B012C File Offset: 0x000AE32C
		protected static string ToXmlNmToken(string value)
		{
			return XmlCustomFormatter.ToXmlNmToken(value);
		}

		/// <summary>Decodes an XML name.</summary>
		/// <param name="value">An XML name to be decoded.</param>
		/// <returns>A decoded string.</returns>
		// Token: 0x06001E9B RID: 7835 RVA: 0x000B0134 File Offset: 0x000AE334
		protected static string ToXmlNmTokens(string value)
		{
			return XmlCustomFormatter.ToXmlNmTokens(value);
		}

		/// <summary>Obtains an <see cref="T:System.Xml.XmlQualifiedName" /> from a name that may contain a prefix. </summary>
		/// <param name="value">A name that may contain a prefix.</param>
		/// <returns>An <see cref="T:System.Xml.XmlQualifiedName" /> that represents a namespace-qualified XML name.</returns>
		// Token: 0x06001E9C RID: 7836 RVA: 0x000B013C File Offset: 0x000AE33C
		protected XmlQualifiedName ToXmlQualifiedName(string value)
		{
			int num = value.LastIndexOf(':');
			string array = XmlConvert.DecodeName(value);
			string name;
			string text;
			if (num < 0)
			{
				name = this.reader.NameTable.Add(array);
				text = this.reader.LookupNamespace(string.Empty);
			}
			else
			{
				string text2 = value.Substring(0, num);
				text = this.reader.LookupNamespace(text2);
				if (text == null)
				{
					throw new InvalidOperationException("namespace " + text2 + " not defined");
				}
				name = this.reader.NameTable.Add(value.Substring(num + 1));
			}
			return new XmlQualifiedName(name, text);
		}

		/// <summary>Raises an <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownAttribute" /> event for the current position of the <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="o">An object that the <see cref="T:System.Xml.Serialization.XmlSerializer" /> is attempting to deserialize, subsequently accessible through the <see cref="P:System.Xml.Serialization.XmlAttributeEventArgs.ObjectBeingDeserialized" /> property.</param>
		/// <param name="attr">An <see cref="T:System.Xml.XmlAttribute" /> that represents the attribute in question.</param>
		// Token: 0x06001E9D RID: 7837 RVA: 0x000B01D5 File Offset: 0x000AE3D5
		protected void UnknownAttribute(object o, XmlAttribute attr)
		{
			this.UnknownAttribute(o, attr, null);
		}

		/// <summary>Raises an <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownAttribute" /> event for the current position of the <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="o">An object that the <see cref="T:System.Xml.Serialization.XmlSerializer" /> is attempting to deserialize, subsequently accessible through the <see cref="P:System.Xml.Serialization.XmlAttributeEventArgs.ObjectBeingDeserialized" /> property.</param>
		/// <param name="attr">A <see cref="T:System.Xml.XmlAttribute" /> that represents the attribute in question.</param>
		/// <param name="qnames">A comma-delimited list of XML qualified names.</param>
		// Token: 0x06001E9E RID: 7838 RVA: 0x000B01E0 File Offset: 0x000AE3E0
		protected void UnknownAttribute(object o, XmlAttribute attr, string qnames)
		{
			int lineNumber;
			int linePosition;
			if (this.Reader is XmlTextReader)
			{
				lineNumber = ((XmlTextReader)this.Reader).LineNumber;
				linePosition = ((XmlTextReader)this.Reader).LinePosition;
			}
			else
			{
				lineNumber = 0;
				linePosition = 0;
			}
			XmlAttributeEventArgs e = new XmlAttributeEventArgs(attr, lineNumber, linePosition, o, qnames);
			if (this.eventSource != null)
			{
				this.eventSource.OnUnknownAttribute(e);
			}
		}

		/// <summary>Raises an <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownElement" /> event for the current position of the <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="o">The <see cref="T:System.Object" /> that is being deserialized.</param>
		/// <param name="elem">The <see cref="T:System.Xml.XmlElement" /> for which an event is raised.</param>
		// Token: 0x06001E9F RID: 7839 RVA: 0x000B0241 File Offset: 0x000AE441
		protected void UnknownElement(object o, XmlElement elem)
		{
			this.UnknownElement(o, elem, null);
		}

		/// <summary>Raises an <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownElement" /> event for the current position of the <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="o">An object that the <see cref="T:System.Xml.Serialization.XmlSerializer" /> is attempting to deserialize, subsequently accessible through the <see cref="P:System.Xml.Serialization.XmlAttributeEventArgs.ObjectBeingDeserialized" /> property.</param>
		/// <param name="elem">The <see cref="T:System.Xml.XmlElement" /> for which an event is raised.</param>
		/// <param name="qnames">A comma-delimited list of XML qualified names.</param>
		// Token: 0x06001EA0 RID: 7840 RVA: 0x000B024C File Offset: 0x000AE44C
		protected void UnknownElement(object o, XmlElement elem, string qnames)
		{
			int lineNumber;
			int linePosition;
			if (this.Reader is XmlTextReader)
			{
				lineNumber = ((XmlTextReader)this.Reader).LineNumber;
				linePosition = ((XmlTextReader)this.Reader).LinePosition;
			}
			else
			{
				lineNumber = 0;
				linePosition = 0;
			}
			XmlElementEventArgs e = new XmlElementEventArgs(elem, lineNumber, linePosition, o, qnames);
			if (this.eventSource != null)
			{
				this.eventSource.OnUnknownElement(e);
			}
		}

		/// <summary>Raises an <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownNode" /> event for the current position of the <see cref="T:System.Xml.XmlReader" />. </summary>
		/// <param name="o">The object that is being deserialized.</param>
		// Token: 0x06001EA1 RID: 7841 RVA: 0x000B02AD File Offset: 0x000AE4AD
		protected void UnknownNode(object o)
		{
			this.UnknownNode(o, null);
		}

		/// <summary>Raises an <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownNode" /> event for the current position of the <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="o">The object being deserialized.</param>
		/// <param name="qnames">A comma-delimited list of XML qualified names.</param>
		// Token: 0x06001EA2 RID: 7842 RVA: 0x000B02B7 File Offset: 0x000AE4B7
		protected void UnknownNode(object o, string qnames)
		{
			this.OnUnknownNode(this.ReadXmlNode(false), o, qnames);
		}

		// Token: 0x06001EA3 RID: 7843 RVA: 0x000B02C8 File Offset: 0x000AE4C8
		private void OnUnknownNode(XmlNode node, object o, string qnames)
		{
			int lineNumber;
			int linePosition;
			if (this.Reader is XmlTextReader)
			{
				lineNumber = ((XmlTextReader)this.Reader).LineNumber;
				linePosition = ((XmlTextReader)this.Reader).LinePosition;
			}
			else
			{
				lineNumber = 0;
				linePosition = 0;
			}
			if (node is XmlAttribute)
			{
				this.UnknownAttribute(o, (XmlAttribute)node, qnames);
				return;
			}
			if (node is XmlElement)
			{
				this.UnknownElement(o, (XmlElement)node, qnames);
				return;
			}
			if (this.eventSource != null)
			{
				this.eventSource.OnUnknownNode(new XmlNodeEventArgs(node, lineNumber, linePosition, o));
			}
			if (this.Reader.ReadState == ReadState.EndOfFile)
			{
				throw new InvalidOperationException("End of document found");
			}
		}

		/// <summary>Raises an <see cref="E:System.Xml.Serialization.XmlSerializer.UnreferencedObject" /> event for the current position of the <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="id">A unique string that is used to identify the unreferenced object, subsequently accessible through the <see cref="P:System.Xml.Serialization.UnreferencedObjectEventArgs.UnreferencedId" /> property.</param>
		/// <param name="o">An object that the <see cref="T:System.Xml.Serialization.XmlSerializer" /> is attempting to deserialize, subsequently accessible through the <see cref="P:System.Xml.Serialization.UnreferencedObjectEventArgs.UnreferencedObject" /> property.</param>
		// Token: 0x06001EA4 RID: 7844 RVA: 0x000B036D File Offset: 0x000AE56D
		protected void UnreferencedObject(string id, object o)
		{
			if (this.eventSource != null)
			{
				this.eventSource.OnUnreferencedObject(new UnreferencedObjectEventArgs(o, id));
			}
		}

		/// <summary>Gets or sets a value that determines whether XML strings are translated into valid .NET Framework type names.</summary>
		/// <returns>
		///     <see langword="true" /> if XML strings are decoded into valid .NET Framework type names; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005D8 RID: 1496
		// (get) Token: 0x06001EA5 RID: 7845 RVA: 0x0000A6CF File Offset: 0x000088CF
		// (set) Token: 0x06001EA6 RID: 7846 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected bool DecodeName
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Removes all occurrences of white space characters from the beginning and end of the specified string.</summary>
		/// <param name="value">The string that will have its white space trimmed.</param>
		/// <returns>The trimmed string.</returns>
		// Token: 0x06001EA7 RID: 7847 RVA: 0x000B0389 File Offset: 0x000AE589
		protected string CollapseWhitespace(string value)
		{
			if (value != null)
			{
				return value.Trim();
			}
			return null;
		}

		/// <summary>Populates an object from its XML representation at the current location of the <see cref="T:System.Xml.XmlReader" />, with an option to read the inner element.</summary>
		/// <param name="xsdDerived">The local name of the derived XML Schema data type.</param>
		/// <param name="nsDerived">The namespace of the derived XML Schema data type.</param>
		/// <param name="xsdBase">The local name of the base XML Schema data type.</param>
		/// <param name="nsBase">The namespace of the base XML Schema data type.</param>
		/// <param name="clrDerived">The namespace of the derived .NET Framework type.</param>
		/// <param name="clrBase">The name of the base .NET Framework type.</param>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001EA8 RID: 7848 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected Exception CreateBadDerivationException(string xsdDerived, string nsDerived, string xsdBase, string nsBase, string clrDerived, string clrBase)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates an <see cref="T:System.InvalidCastException" /> that indicates that an explicit reference conversion failed.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> that an object cannot be cast to. This type is incorporated into the exception message.</param>
		/// <param name="value">The object that cannot be cast. This object is incorporated into the exception message.</param>
		/// <param name="id">A string identifier.</param>
		/// <returns>An <see cref="T:System.InvalidCastException" /> exception.</returns>
		// Token: 0x06001EA9 RID: 7849 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected Exception CreateInvalidCastException(Type type, object value, string id)
		{
			throw new NotImplementedException();
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a derived type that is mapped to an XML Schema data type cannot be located.</summary>
		/// <param name="name">The local name of the XML Schema data type that is mapped to the unavailable derived type.</param>
		/// <param name="ns">The namespace of the XML Schema data type that is mapped to the unavailable derived type.</param>
		/// <param name="clrType">The full name of the .NET Framework base type for which a derived type cannot be located.</param>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		// Token: 0x06001EAA RID: 7850 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected Exception CreateMissingIXmlSerializableType(string name, string ns, string clrType)
		{
			throw new NotImplementedException();
		}

		/// <summary>Returns the result of a call to the <see cref="M:System.Xml.XmlReader.ReadString" /> method of the <see cref="T:System.Xml.XmlReader" /> class, trimmed of white space if needed, and appended to the input value.</summary>
		/// <param name="value">A string that will be appended to.</param>
		/// <param name="trim">
		///       <see langword="true" /> if the result of the read operation should be trimmed; otherwise, <see langword="false" />.</param>
		/// <returns>The result of the read operation appended to the input value.</returns>
		// Token: 0x06001EAB RID: 7851 RVA: 0x000B0398 File Offset: 0x000AE598
		protected string ReadString(string value, bool trim)
		{
			this.readCount++;
			string text = this.reader.ReadString();
			if (text != null && trim)
			{
				text = text.Trim();
			}
			if (value == null || value.Length == 0)
			{
				return text;
			}
			return value + text;
		}

		/// <summary>Reads an XML element that allows null values (<see langword="xsi:nil = 'true'" />) and returns a generic <see cref="T:System.Nullable`1" /> value. </summary>
		/// <param name="type">The <see cref="T:System.Xml.XmlQualifiedName" /> that represents the simple data type for the current location of the <see cref="T:System.Xml.XmlReader" />.</param>
		/// <returns>A generic <see cref="T:System.Nullable`1" /> that represents a null XML value.</returns>
		// Token: 0x06001EAC RID: 7852 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected object ReadTypedNull(XmlQualifiedName type)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a dynamically generated assembly by name.</summary>
		/// <param name="assemblyFullName">The full name of the assembly.</param>
		/// <returns>A dynamically generated <see cref="T:System.Reflection.Assembly" />.</returns>
		// Token: 0x06001EAD RID: 7853 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected static Assembly ResolveDynamicAssembly(string assemblyFullName)
		{
			throw new NotImplementedException();
		}

		// Token: 0x040016CE RID: 5838
		private XmlDocument document;

		// Token: 0x040016CF RID: 5839
		private XmlReader reader;

		// Token: 0x040016D0 RID: 5840
		private ArrayList fixups;

		// Token: 0x040016D1 RID: 5841
		private Hashtable collFixups;

		// Token: 0x040016D2 RID: 5842
		private ArrayList collItemFixups;

		// Token: 0x040016D3 RID: 5843
		private Hashtable typesCallbacks;

		// Token: 0x040016D4 RID: 5844
		private ArrayList noIDTargets;

		// Token: 0x040016D5 RID: 5845
		private Hashtable targets;

		// Token: 0x040016D6 RID: 5846
		private Hashtable delayedListFixups;

		// Token: 0x040016D7 RID: 5847
		private XmlSerializer eventSource;

		// Token: 0x040016D8 RID: 5848
		private int delayedFixupId;

		// Token: 0x040016D9 RID: 5849
		private Hashtable referencedObjects;

		// Token: 0x040016DA RID: 5850
		private int readCount;

		// Token: 0x040016DB RID: 5851
		private int whileIterationCount;

		// Token: 0x040016DC RID: 5852
		private string w3SchemaNS;

		// Token: 0x040016DD RID: 5853
		private string w3InstanceNS;

		// Token: 0x040016DE RID: 5854
		private string w3InstanceNS2000;

		// Token: 0x040016DF RID: 5855
		private string w3InstanceNS1999;

		// Token: 0x040016E0 RID: 5856
		private string soapNS;

		// Token: 0x040016E1 RID: 5857
		private string wsdlNS;

		// Token: 0x040016E2 RID: 5858
		private string nullX;

		// Token: 0x040016E3 RID: 5859
		private string nil;

		// Token: 0x040016E4 RID: 5860
		private string typeX;

		// Token: 0x040016E5 RID: 5861
		private string arrayType;

		// Token: 0x040016E6 RID: 5862
		private XmlQualifiedName arrayQName;

		// Token: 0x0200032B RID: 811
		private class WriteCallbackInfo
		{
			// Token: 0x06001EAE RID: 7854 RVA: 0x00002103 File Offset: 0x00000303
			public WriteCallbackInfo()
			{
			}

			// Token: 0x040016E7 RID: 5863
			public Type Type;

			// Token: 0x040016E8 RID: 5864
			public string TypeName;

			// Token: 0x040016E9 RID: 5865
			public string TypeNs;

			// Token: 0x040016EA RID: 5866
			public XmlSerializationReadCallback Callback;
		}

		/// <summary>Holds an <see cref="T:System.Xml.Serialization.XmlSerializationCollectionFixupCallback" /> delegate instance, plus the method's inputs; also supplies the method's parameters. </summary>
		// Token: 0x0200032C RID: 812
		protected class CollectionFixup
		{
			/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializationReader.CollectionFixup" /> class with parameters for a callback method. </summary>
			/// <param name="collection">A collection into which the callback method copies the collection items array.</param>
			/// <param name="callback">A method that instantiates the <see cref="T:System.Xml.Serialization.XmlSerializationCollectionFixupCallback" /> delegate.</param>
			/// <param name="collectionItems">An array into which the callback method copies a collection.</param>
			// Token: 0x06001EAF RID: 7855 RVA: 0x000B03E2 File Offset: 0x000AE5E2
			public CollectionFixup(object collection, XmlSerializationCollectionFixupCallback callback, object collectionItems)
			{
				this.callback = callback;
				this.collection = collection;
				this.collectionItems = collectionItems;
			}

			// Token: 0x06001EB0 RID: 7856 RVA: 0x000B03FF File Offset: 0x000AE5FF
			internal CollectionFixup(object collection, XmlSerializationCollectionFixupCallback callback, string id)
			{
				this.callback = callback;
				this.collection = collection;
				this.id = id;
			}

			/// <summary>Gets the callback method that instantiates the <see cref="T:System.Xml.Serialization.XmlSerializationCollectionFixupCallback" /> delegate. </summary>
			/// <returns>The <see cref="T:System.Xml.Serialization.XmlSerializationCollectionFixupCallback" /> delegate that points to the callback method.</returns>
			// Token: 0x170005D9 RID: 1497
			// (get) Token: 0x06001EB1 RID: 7857 RVA: 0x000B041C File Offset: 0x000AE61C
			public XmlSerializationCollectionFixupCallback Callback
			{
				get
				{
					return this.callback;
				}
			}

			/// <summary>Gets the <paramref name="object collection" /> for the callback method. </summary>
			/// <returns>The collection that is used for the fixup.</returns>
			// Token: 0x170005DA RID: 1498
			// (get) Token: 0x06001EB2 RID: 7858 RVA: 0x000B0424 File Offset: 0x000AE624
			public object Collection
			{
				get
				{
					return this.collection;
				}
			}

			// Token: 0x170005DB RID: 1499
			// (get) Token: 0x06001EB3 RID: 7859 RVA: 0x000B042C File Offset: 0x000AE62C
			internal object Id
			{
				get
				{
					return this.id;
				}
			}

			/// <summary>Gets the array into which the callback method copies a collection. </summary>
			/// <returns>The array into which the callback method copies a collection.</returns>
			// Token: 0x170005DC RID: 1500
			// (get) Token: 0x06001EB4 RID: 7860 RVA: 0x000B0434 File Offset: 0x000AE634
			// (set) Token: 0x06001EB5 RID: 7861 RVA: 0x000B043C File Offset: 0x000AE63C
			public object CollectionItems
			{
				get
				{
					return this.collectionItems;
				}
				internal set
				{
					this.collectionItems = value;
				}
			}

			// Token: 0x040016EB RID: 5867
			private XmlSerializationCollectionFixupCallback callback;

			// Token: 0x040016EC RID: 5868
			private object collection;

			// Token: 0x040016ED RID: 5869
			private object collectionItems;

			// Token: 0x040016EE RID: 5870
			private string id;
		}

		/// <summary>Holds an <see cref="T:System.Xml.Serialization.XmlSerializationFixupCallback" /> delegate instance, plus the method's inputs; also serves as the parameter for the method.</summary>
		// Token: 0x0200032D RID: 813
		protected class Fixup
		{
			/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializationReader.Fixup" /> class.</summary>
			/// <param name="o">The object that contains other objects whose values get filled in by the callback implementation.</param>
			/// <param name="callback">A method that instantiates the <see cref="T:System.Xml.Serialization.XmlSerializationFixupCallback" /> delegate.</param>
			/// <param name="count">The size of the string array obtained through the <see cref="P:System.Xml.Serialization.XmlSerializationReader.Fixup.Ids" /> property.</param>
			// Token: 0x06001EB6 RID: 7862 RVA: 0x000B0445 File Offset: 0x000AE645
			public Fixup(object o, XmlSerializationFixupCallback callback, int count)
			{
				this.source = o;
				this.callback = callback;
				this.ids = new string[count];
			}

			/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializationReader.Fixup" /> class.</summary>
			/// <param name="o">The object that contains other objects whose values get filled in by the callback implementation.</param>
			/// <param name="callback">A method that instantiates the <see cref="T:System.Xml.Serialization.XmlSerializationFixupCallback" /> delegate.</param>
			/// <param name="ids">The string array obtained through the <see cref="P:System.Xml.Serialization.XmlSerializationReader.Fixup.Ids" /> property.</param>
			// Token: 0x06001EB7 RID: 7863 RVA: 0x000B0467 File Offset: 0x000AE667
			public Fixup(object o, XmlSerializationFixupCallback callback, string[] ids)
			{
				this.source = o;
				this.ids = ids;
				this.callback = callback;
			}

			/// <summary>Gets the callback method that creates an instance of the <see cref="T:System.Xml.Serialization.XmlSerializationFixupCallback" /> delegate.</summary>
			/// <returns>The callback method that creates an instance of the <see cref="T:System.Xml.Serialization.XmlSerializationFixupCallback" /> delegate.</returns>
			// Token: 0x170005DD RID: 1501
			// (get) Token: 0x06001EB8 RID: 7864 RVA: 0x000B0484 File Offset: 0x000AE684
			public XmlSerializationFixupCallback Callback
			{
				get
				{
					return this.callback;
				}
			}

			/// <summary>Gets or sets an array of keys for the objects that belong to the <see cref="P:System.Xml.Serialization.XmlSerializationReader.Fixup.Source" /> property whose values get filled in by the callback implementation.</summary>
			/// <returns>The array of keys.</returns>
			// Token: 0x170005DE RID: 1502
			// (get) Token: 0x06001EB9 RID: 7865 RVA: 0x000B048C File Offset: 0x000AE68C
			public string[] Ids
			{
				get
				{
					return this.ids;
				}
			}

			/// <summary>Gets or sets the object that contains other objects whose values get filled in by the callback implementation.</summary>
			/// <returns>The source containing objects with values to fill.</returns>
			// Token: 0x170005DF RID: 1503
			// (get) Token: 0x06001EBA RID: 7866 RVA: 0x000B0494 File Offset: 0x000AE694
			// (set) Token: 0x06001EBB RID: 7867 RVA: 0x000B049C File Offset: 0x000AE69C
			public object Source
			{
				get
				{
					return this.source;
				}
				set
				{
					this.source = value;
				}
			}

			// Token: 0x040016EF RID: 5871
			private object source;

			// Token: 0x040016F0 RID: 5872
			private string[] ids;

			// Token: 0x040016F1 RID: 5873
			private XmlSerializationFixupCallback callback;
		}

		// Token: 0x0200032E RID: 814
		private class CollectionItemFixup
		{
			// Token: 0x06001EBC RID: 7868 RVA: 0x000B04A5 File Offset: 0x000AE6A5
			public CollectionItemFixup(Array list, int index, string id)
			{
				this.list = list;
				this.index = index;
				this.id = id;
			}

			// Token: 0x170005E0 RID: 1504
			// (get) Token: 0x06001EBD RID: 7869 RVA: 0x000B04C2 File Offset: 0x000AE6C2
			public Array Collection
			{
				get
				{
					return this.list;
				}
			}

			// Token: 0x170005E1 RID: 1505
			// (get) Token: 0x06001EBE RID: 7870 RVA: 0x000B04CA File Offset: 0x000AE6CA
			public int Index
			{
				get
				{
					return this.index;
				}
			}

			// Token: 0x170005E2 RID: 1506
			// (get) Token: 0x06001EBF RID: 7871 RVA: 0x000B04D2 File Offset: 0x000AE6D2
			public string Id
			{
				get
				{
					return this.id;
				}
			}

			// Token: 0x040016F2 RID: 5874
			private Array list;

			// Token: 0x040016F3 RID: 5875
			private int index;

			// Token: 0x040016F4 RID: 5876
			private string id;
		}
	}
}
