﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002E8 RID: 744
	internal enum SchemaTypes
	{
		// Token: 0x040015BB RID: 5563
		NotSet,
		// Token: 0x040015BC RID: 5564
		Primitive,
		// Token: 0x040015BD RID: 5565
		Enum,
		// Token: 0x040015BE RID: 5566
		Array,
		// Token: 0x040015BF RID: 5567
		Class,
		// Token: 0x040015C0 RID: 5568
		XmlSerializable,
		// Token: 0x040015C1 RID: 5569
		XmlNode,
		// Token: 0x040015C2 RID: 5570
		Void
	}
}
