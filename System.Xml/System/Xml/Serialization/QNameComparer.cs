﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x020002D3 RID: 723
	internal class QNameComparer : IComparer
	{
		// Token: 0x06001A9D RID: 6813 RVA: 0x00094F18 File Offset: 0x00093118
		public int Compare(object o1, object o2)
		{
			XmlQualifiedName xmlQualifiedName = (XmlQualifiedName)o1;
			XmlQualifiedName xmlQualifiedName2 = (XmlQualifiedName)o2;
			int num = string.Compare(xmlQualifiedName.Namespace, xmlQualifiedName2.Namespace, StringComparison.Ordinal);
			if (num == 0)
			{
				return string.Compare(xmlQualifiedName.Name, xmlQualifiedName2.Name, StringComparison.Ordinal);
			}
			return num;
		}

		// Token: 0x06001A9E RID: 6814 RVA: 0x00002103 File Offset: 0x00000303
		public QNameComparer()
		{
		}
	}
}
