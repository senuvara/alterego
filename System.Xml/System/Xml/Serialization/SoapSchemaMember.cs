﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Represents certain attributes of a XSD &lt;<see langword="part" />&gt; element in a WSDL document for generating classes from the document. </summary>
	// Token: 0x020002FE RID: 766
	public class SoapSchemaMember
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.SoapSchemaMember" /> class. </summary>
		// Token: 0x06001C31 RID: 7217 RVA: 0x000A2BA2 File Offset: 0x000A0DA2
		public SoapSchemaMember()
		{
		}

		/// <summary>Gets or sets a value that corresponds to the name attribute of the WSDL part element. </summary>
		/// <returns>The element name.</returns>
		// Token: 0x17000552 RID: 1362
		// (get) Token: 0x06001C32 RID: 7218 RVA: 0x000A2BB5 File Offset: 0x000A0DB5
		// (set) Token: 0x06001C33 RID: 7219 RVA: 0x000A2BCB File Offset: 0x000A0DCB
		public string MemberName
		{
			get
			{
				if (this.memberName == null)
				{
					return string.Empty;
				}
				return this.memberName;
			}
			set
			{
				this.memberName = value;
			}
		}

		/// <summary>Gets or sets a value that corresponds to the type attribute of the WSDL part element.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlQualifiedName" /> that corresponds to the XML type.</returns>
		// Token: 0x17000553 RID: 1363
		// (get) Token: 0x06001C34 RID: 7220 RVA: 0x000A2BD4 File Offset: 0x000A0DD4
		// (set) Token: 0x06001C35 RID: 7221 RVA: 0x000A2BDC File Offset: 0x000A0DDC
		public XmlQualifiedName MemberType
		{
			get
			{
				return this.memberType;
			}
			set
			{
				this.memberType = value;
			}
		}

		// Token: 0x04001626 RID: 5670
		private string memberName;

		// Token: 0x04001627 RID: 5671
		private XmlQualifiedName memberType = XmlQualifiedName.Empty;
	}
}
