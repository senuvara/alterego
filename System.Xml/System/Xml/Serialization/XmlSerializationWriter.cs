﻿using System;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization;

namespace System.Xml.Serialization
{
	/// <summary>Represents an abstract class used for controlling serialization by the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class.</summary>
	// Token: 0x02000333 RID: 819
	public abstract class XmlSerializationWriter : XmlSerializationGeneratedCode
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializationWriter" /> class.</summary>
		// Token: 0x06001EF0 RID: 7920 RVA: 0x000B2100 File Offset: 0x000B0300
		protected XmlSerializationWriter()
		{
			this.qnameCount = 0;
			this.serializedObjects = new Hashtable();
		}

		// Token: 0x06001EF1 RID: 7921 RVA: 0x000B211C File Offset: 0x000B031C
		internal void Initialize(XmlWriter writer, XmlSerializerNamespaces nss)
		{
			this.writer = writer;
			if (nss != null)
			{
				this.namespaces = new ArrayList();
				foreach (XmlQualifiedName xmlQualifiedName in nss.ToArray())
				{
					if (xmlQualifiedName.Name != "" && xmlQualifiedName.Namespace != "")
					{
						this.namespaces.Add(xmlQualifiedName);
					}
				}
			}
		}

		/// <summary>Gets or sets a list of XML qualified name objects that contain the namespaces and prefixes used to produce qualified names in XML documents.</summary>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> that contains the namespaces and prefix pairs.</returns>
		// Token: 0x170005E3 RID: 1507
		// (get) Token: 0x06001EF2 RID: 7922 RVA: 0x000B2188 File Offset: 0x000B0388
		// (set) Token: 0x06001EF3 RID: 7923 RVA: 0x000B2190 File Offset: 0x000B0390
		protected ArrayList Namespaces
		{
			get
			{
				return this.namespaces;
			}
			set
			{
				this.namespaces = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.XmlWriter" /> that is being used by the <see cref="T:System.Xml.Serialization.XmlSerializationWriter" />.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlWriter" /> used by the class instance.</returns>
		// Token: 0x170005E4 RID: 1508
		// (get) Token: 0x06001EF4 RID: 7924 RVA: 0x000B2199 File Offset: 0x000B0399
		// (set) Token: 0x06001EF5 RID: 7925 RVA: 0x000B21A1 File Offset: 0x000B03A1
		protected XmlWriter Writer
		{
			get
			{
				return this.writer;
			}
			set
			{
				this.writer = value;
			}
		}

		/// <summary>Stores an implementation of the <see cref="T:System.Xml.Serialization.XmlSerializationWriteCallback" /> delegate and the type it applies to, for a later invocation.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of objects that are serialized.</param>
		/// <param name="typeName">The name of the type of objects that are serialized.</param>
		/// <param name="typeNs">The namespace of the type of objects that are serialized.</param>
		/// <param name="callback">An instance of the <see cref="T:System.Xml.Serialization.XmlSerializationWriteCallback" /> delegate.</param>
		// Token: 0x06001EF6 RID: 7926 RVA: 0x000B21AC File Offset: 0x000B03AC
		protected void AddWriteCallback(Type type, string typeName, string typeNs, XmlSerializationWriteCallback callback)
		{
			XmlSerializationWriter.WriteCallbackInfo writeCallbackInfo = new XmlSerializationWriter.WriteCallbackInfo();
			writeCallbackInfo.Type = type;
			writeCallbackInfo.TypeName = typeName;
			writeCallbackInfo.TypeNs = typeNs;
			writeCallbackInfo.Callback = callback;
			if (this.callbacks == null)
			{
				this.callbacks = new Hashtable();
			}
			this.callbacks.Add(type, writeCallbackInfo);
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates an unexpected name for an element that adheres to an XML Schema <see langword="choice" /> element declaration.</summary>
		/// <param name="value">The name that is not valid.</param>
		/// <param name="identifier">The <see langword="choice" /> element declaration that the name belongs to.</param>
		/// <param name="name">The expected local name of an element.</param>
		/// <param name="ns">The expected namespace of an element.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001EF7 RID: 7927 RVA: 0x000B21FC File Offset: 0x000B03FC
		protected Exception CreateChoiceIdentifierValueException(string value, string identifier, string name, string ns)
		{
			return new InvalidOperationException(string.Format("Value '{0}' of the choice identifier '{1}' does not match element '{2}' from namespace '{3}'.", new object[]
			{
				value,
				identifier,
				name,
				ns
			}));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates a failure while writing an array where an XML Schema <see langword="choice" /> element declaration is applied.</summary>
		/// <param name="type">The type being serialized.</param>
		/// <param name="identifier">A name for the <see langword="choice" /> element declaration.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001EF8 RID: 7928 RVA: 0x000B2224 File Offset: 0x000B0424
		protected Exception CreateInvalidChoiceIdentifierValueException(string type, string identifier)
		{
			return new InvalidOperationException(string.Format("Invalid or missing choice identifier '{0}' of type '{1}'.", identifier, type));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a value for an XML element does not match an enumeration type.</summary>
		/// <param name="value">The value that is not valid.</param>
		/// <param name="elementName">The name of the XML element with an invalid value.</param>
		/// <param name="enumValue">The valid value.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001EF9 RID: 7929 RVA: 0x000B2237 File Offset: 0x000B0437
		protected Exception CreateMismatchChoiceException(string value, string elementName, string enumValue)
		{
			return new InvalidOperationException(string.Format("Value of {0} mismatches the type of {1}, you need to set it to {2}.", elementName, value, enumValue));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that an XML element that should adhere to the XML Schema <see langword="any" /> element declaration cannot be processed.</summary>
		/// <param name="name">The XML element that cannot be processed.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001EFA RID: 7930 RVA: 0x000B224B File Offset: 0x000B044B
		protected Exception CreateUnknownAnyElementException(string name, string ns)
		{
			return new InvalidOperationException(string.Format("The XML element named '{0}' from namespace '{1}' was not expected. The XML element name and namespace must match those provided via XmlAnyElementAttribute(s).", name, ns));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a type being serialized is not being used in a valid manner or is unexpectedly encountered.</summary>
		/// <param name="o">The object whose type cannot be serialized.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001EFB RID: 7931 RVA: 0x000B225E File Offset: 0x000B045E
		protected Exception CreateUnknownTypeException(object o)
		{
			return this.CreateUnknownTypeException(o.GetType());
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a type being serialized is not being used in a valid manner or is unexpectedly encountered.</summary>
		/// <param name="type">The type that cannot be serialized.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001EFC RID: 7932 RVA: 0x000B226C File Offset: 0x000B046C
		protected Exception CreateUnknownTypeException(Type type)
		{
			return new InvalidOperationException(string.Format("The type {0} may not be used in this context.", type));
		}

		/// <summary>Processes a base-64 byte array.</summary>
		/// <param name="value">A base-64 <see cref="T:System.Byte" /> array.</param>
		/// <returns>The same byte array that was passed in as an argument.</returns>
		// Token: 0x06001EFD RID: 7933 RVA: 0x00002068 File Offset: 0x00000268
		protected static byte[] FromByteArrayBase64(byte[] value)
		{
			return value;
		}

		/// <summary>Produces a string from an input hexadecimal byte array.</summary>
		/// <param name="value">A hexadecimal byte array to translate to a string.</param>
		/// <returns>The byte array value converted to a string.</returns>
		// Token: 0x06001EFE RID: 7934 RVA: 0x000B227E File Offset: 0x000B047E
		protected static string FromByteArrayHex(byte[] value)
		{
			return XmlCustomFormatter.FromByteArrayHex(value);
		}

		/// <summary>Produces a string from an input <see cref="T:System.Char" />.</summary>
		/// <param name="value">A <see cref="T:System.Char" /> to translate to a string.</param>
		/// <returns>The <see cref="T:System.Char" /> value converted to a string.</returns>
		// Token: 0x06001EFF RID: 7935 RVA: 0x000B2286 File Offset: 0x000B0486
		protected static string FromChar(char value)
		{
			return XmlCustomFormatter.FromChar(value);
		}

		/// <summary>Produces a string from a <see cref="T:System.DateTime" /> object.</summary>
		/// <param name="value">A <see cref="T:System.DateTime" /> to translate to a string.</param>
		/// <returns>A string representation of the <see cref="T:System.DateTime" /> that shows the date but no time.</returns>
		// Token: 0x06001F00 RID: 7936 RVA: 0x000B228E File Offset: 0x000B048E
		protected static string FromDate(DateTime value)
		{
			return XmlCustomFormatter.FromDate(value);
		}

		/// <summary>Produces a string from an input <see cref="T:System.DateTime" />.</summary>
		/// <param name="value">A <see cref="T:System.DateTime" /> to translate to a string.</param>
		/// <returns>A string representation of the <see cref="T:System.DateTime" /> that shows the date and time.</returns>
		// Token: 0x06001F01 RID: 7937 RVA: 0x000B2296 File Offset: 0x000B0496
		protected static string FromDateTime(DateTime value)
		{
			return XmlCustomFormatter.FromDateTime(value);
		}

		/// <summary>Produces a string that consists of delimited identifiers that represent the enumeration members that have been set.</summary>
		/// <param name="value">The enumeration value as a series of bitwise <see langword="OR" /> operations.</param>
		/// <param name="values">The enumeration's name values.</param>
		/// <param name="ids">The enumeration's constant values.</param>
		/// <returns>A string that consists of delimited identifiers, where each represents a member from the set enumerator list.</returns>
		// Token: 0x06001F02 RID: 7938 RVA: 0x000B229E File Offset: 0x000B049E
		protected static string FromEnum(long value, string[] values, long[] ids)
		{
			return XmlCustomFormatter.FromEnum(value, values, ids);
		}

		/// <summary>Produces a string from a <see cref="T:System.DateTime" /> object.</summary>
		/// <param name="value">A <see cref="T:System.DateTime" /> that is translated to a string.</param>
		/// <returns>A string representation of the <see cref="T:System.DateTime" /> object that shows the time but no date.</returns>
		// Token: 0x06001F03 RID: 7939 RVA: 0x000B22A8 File Offset: 0x000B04A8
		protected static string FromTime(DateTime value)
		{
			return XmlCustomFormatter.FromTime(value);
		}

		/// <summary>Encodes a valid XML name by replacing characters that are not valid with escape sequences.</summary>
		/// <param name="name">A string to be used as an XML name.</param>
		/// <returns>An encoded string.</returns>
		// Token: 0x06001F04 RID: 7940 RVA: 0x000B22B0 File Offset: 0x000B04B0
		protected static string FromXmlName(string name)
		{
			return XmlCustomFormatter.FromXmlName(name);
		}

		/// <summary>Encodes a valid XML local name by replacing characters that are not valid with escape sequences.</summary>
		/// <param name="ncName">A string to be used as a local (unqualified) XML name.</param>
		/// <returns>An encoded string.</returns>
		// Token: 0x06001F05 RID: 7941 RVA: 0x000B22B8 File Offset: 0x000B04B8
		protected static string FromXmlNCName(string ncName)
		{
			return XmlCustomFormatter.FromXmlNCName(ncName);
		}

		/// <summary>Encodes an XML name.</summary>
		/// <param name="nmToken">An XML name to be encoded.</param>
		/// <returns>An encoded string.</returns>
		// Token: 0x06001F06 RID: 7942 RVA: 0x000B22C0 File Offset: 0x000B04C0
		protected static string FromXmlNmToken(string nmToken)
		{
			return XmlCustomFormatter.FromXmlNmToken(nmToken);
		}

		/// <summary>Encodes a space-delimited sequence of XML names into a single XML name.</summary>
		/// <param name="nmTokens">A space-delimited sequence of XML names to be encoded.</param>
		/// <returns>An encoded string.</returns>
		// Token: 0x06001F07 RID: 7943 RVA: 0x000B22C8 File Offset: 0x000B04C8
		protected static string FromXmlNmTokens(string nmTokens)
		{
			return XmlCustomFormatter.FromXmlNmTokens(nmTokens);
		}

		/// <summary>Returns an XML qualified name, with invalid characters replaced by escape sequences.</summary>
		/// <param name="xmlQualifiedName">An <see cref="T:System.Xml.XmlQualifiedName" /> that represents the XML to be written.</param>
		/// <returns>An XML qualified name, with invalid characters replaced by escape sequences.</returns>
		// Token: 0x06001F08 RID: 7944 RVA: 0x000B22D0 File Offset: 0x000B04D0
		protected string FromXmlQualifiedName(XmlQualifiedName xmlQualifiedName)
		{
			if (xmlQualifiedName == null || xmlQualifiedName == XmlQualifiedName.Empty)
			{
				return null;
			}
			return this.GetQualifiedName(xmlQualifiedName.Name, xmlQualifiedName.Namespace);
		}

		// Token: 0x06001F09 RID: 7945 RVA: 0x000B22FC File Offset: 0x000B04FC
		private string GetId(object o, bool addToReferencesList)
		{
			if (this.idGenerator == null)
			{
				this.idGenerator = new ObjectIDGenerator();
			}
			bool flag;
			long id = this.idGenerator.GetId(o, out flag);
			return string.Format(CultureInfo.InvariantCulture, "id{0}", id);
		}

		// Token: 0x06001F0A RID: 7946 RVA: 0x000B2340 File Offset: 0x000B0540
		private bool AlreadyQueued(object ob)
		{
			if (this.idGenerator == null)
			{
				return false;
			}
			bool flag;
			this.idGenerator.HasId(ob, out flag);
			return !flag;
		}

		// Token: 0x06001F0B RID: 7947 RVA: 0x000B236C File Offset: 0x000B056C
		private string GetNamespacePrefix(string ns)
		{
			string text = this.Writer.LookupPrefix(ns);
			if (text == null)
			{
				IFormatProvider invariantCulture = CultureInfo.InvariantCulture;
				string format = "q{0}";
				int num = this.qnameCount + 1;
				this.qnameCount = num;
				text = string.Format(invariantCulture, format, num);
				this.WriteAttribute("xmlns", text, null, ns);
			}
			return text;
		}

		// Token: 0x06001F0C RID: 7948 RVA: 0x000B23C0 File Offset: 0x000B05C0
		private string GetQualifiedName(string name, string ns)
		{
			if (ns == string.Empty)
			{
				return name;
			}
			string namespacePrefix = this.GetNamespacePrefix(ns);
			if (namespacePrefix == string.Empty)
			{
				return name;
			}
			return string.Format("{0}:{1}", namespacePrefix, name);
		}

		/// <summary>Initializes an instances of the <see cref="T:System.Xml.Serialization.XmlSerializationWriteCallback" /> delegate to serialize SOAP-encoded XML data.</summary>
		// Token: 0x06001F0D RID: 7949
		protected abstract void InitCallbacks();

		/// <summary>Initializes object references only while serializing a SOAP-encoded SOAP message.</summary>
		// Token: 0x06001F0E RID: 7950 RVA: 0x000B23FF File Offset: 0x000B05FF
		protected void TopLevelElement()
		{
			this.topLevelElement = true;
		}

		/// <summary>Instructs an <see cref="T:System.Xml.XmlWriter" /> object to write an XML attribute that has no namespace specified for its name.</summary>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="value">The value of the XML attribute as a byte array.</param>
		// Token: 0x06001F0F RID: 7951 RVA: 0x000B2408 File Offset: 0x000B0608
		protected void WriteAttribute(string localName, byte[] value)
		{
			this.WriteAttribute(localName, string.Empty, value);
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlWriter" /> to write an XML attribute that has no namespace specified for its name.</summary>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="value">The value of the XML attribute as a string.</param>
		// Token: 0x06001F10 RID: 7952 RVA: 0x000B2417 File Offset: 0x000B0617
		protected void WriteAttribute(string localName, string value)
		{
			this.WriteAttribute(string.Empty, localName, string.Empty, value);
		}

		/// <summary>Instructs an <see cref="T:System.Xml.XmlWriter" /> object to write an XML attribute.</summary>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="ns">The namespace of the XML attribute.</param>
		/// <param name="value">The value of the XML attribute as a byte array.</param>
		// Token: 0x06001F11 RID: 7953 RVA: 0x000B242B File Offset: 0x000B062B
		protected void WriteAttribute(string localName, string ns, byte[] value)
		{
			if (value == null)
			{
				return;
			}
			this.Writer.WriteStartAttribute(localName, ns);
			this.WriteValue(value);
			this.Writer.WriteEndAttribute();
		}

		/// <summary>Writes an XML attribute.</summary>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="ns">The namespace of the XML attribute.</param>
		/// <param name="value">The value of the XML attribute as a string.</param>
		// Token: 0x06001F12 RID: 7954 RVA: 0x000B2450 File Offset: 0x000B0650
		protected void WriteAttribute(string localName, string ns, string value)
		{
			this.WriteAttribute(null, localName, ns, value);
		}

		/// <summary>Writes an XML attribute where the namespace prefix is provided manually.</summary>
		/// <param name="prefix">The namespace prefix to write.</param>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="ns">The namespace represented by the prefix.</param>
		/// <param name="value">The value of the XML attribute as a string.</param>
		// Token: 0x06001F13 RID: 7955 RVA: 0x000B245C File Offset: 0x000B065C
		protected void WriteAttribute(string prefix, string localName, string ns, string value)
		{
			if (value == null)
			{
				return;
			}
			this.Writer.WriteAttributeString(prefix, localName, ns, value);
		}

		// Token: 0x06001F14 RID: 7956 RVA: 0x000B2473 File Offset: 0x000B0673
		private void WriteXmlNode(XmlNode node)
		{
			if (node is XmlDocument)
			{
				node = ((XmlDocument)node).DocumentElement;
			}
			node.WriteTo(this.Writer);
		}

		/// <summary>Writes an XML node object within the body of a named XML element.</summary>
		/// <param name="node">The XML node to write, possibly a child XML element.</param>
		/// <param name="name">The local name of the parent XML element to write.</param>
		/// <param name="ns">The namespace of the parent XML element to write.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> to write an <see langword="xsi:nil='true'" /> attribute if the object to serialize is <see langword="null" />; otherwise, <see langword="false" />.</param>
		/// <param name="any">
		///       <see langword="true" /> to indicate that the node, if an XML element, adheres to an XML Schema <see langword="any" /> element declaration; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F15 RID: 7957 RVA: 0x000B2498 File Offset: 0x000B0698
		protected void WriteElementEncoded(XmlNode node, string name, string ns, bool isNullable, bool any)
		{
			if (name != string.Empty)
			{
				if (node == null)
				{
					if (isNullable)
					{
						this.WriteNullTagEncoded(name, ns);
						return;
					}
				}
				else
				{
					if (any)
					{
						this.WriteXmlNode(node);
						return;
					}
					this.Writer.WriteStartElement(name, ns);
					this.WriteXmlNode(node);
					this.Writer.WriteEndElement();
					return;
				}
			}
			else
			{
				this.WriteXmlNode(node);
			}
		}

		/// <summary>Instructs an <see cref="T:System.Xml.XmlWriter" /> object to write an <see cref="T:System.Xml.XmlNode" /> object within the body of a named XML element.</summary>
		/// <param name="node">The XML node to write, possibly a child XML element.</param>
		/// <param name="name">The local name of the parent XML element to write.</param>
		/// <param name="ns">The namespace of the parent XML element to write.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> to write an <see langword="xsi:nil='true'" /> attribute if the object to serialize is <see langword="null" />; otherwise, <see langword="false" />.</param>
		/// <param name="any">
		///       <see langword="true" /> to indicate that the node, if an XML element, adheres to an XML Schema <see langword="any" /> element declaration; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F16 RID: 7958 RVA: 0x000B24F8 File Offset: 0x000B06F8
		protected void WriteElementLiteral(XmlNode node, string name, string ns, bool isNullable, bool any)
		{
			if (name != string.Empty)
			{
				if (node == null)
				{
					if (isNullable)
					{
						this.WriteNullTagLiteral(name, ns);
						return;
					}
				}
				else
				{
					if (any)
					{
						this.WriteXmlNode(node);
						return;
					}
					this.Writer.WriteStartElement(name, ns);
					this.WriteXmlNode(node);
					this.Writer.WriteEndElement();
					return;
				}
			}
			else
			{
				this.WriteXmlNode(node);
			}
		}

		/// <summary>Writes an XML element with a specified qualified name in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The name to write, using its prefix if namespace-qualified, in the element text.</param>
		// Token: 0x06001F17 RID: 7959 RVA: 0x000B2555 File Offset: 0x000B0755
		protected void WriteElementQualifiedName(string localName, XmlQualifiedName value)
		{
			this.WriteElementQualifiedName(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified qualified name in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The name to write, using its prefix if namespace-qualified, in the element text.</param>
		// Token: 0x06001F18 RID: 7960 RVA: 0x000B2565 File Offset: 0x000B0765
		protected void WriteElementQualifiedName(string localName, string ns, XmlQualifiedName value)
		{
			this.WriteElementQualifiedName(localName, ns, value, null);
		}

		/// <summary>Writes an XML element with a specified qualified name in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The name to write, using its prefix if namespace-qualified, in the element text.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F19 RID: 7961 RVA: 0x000B2571 File Offset: 0x000B0771
		protected void WriteElementQualifiedName(string localName, XmlQualifiedName value, XmlQualifiedName xsiType)
		{
			this.WriteElementQualifiedName(localName, string.Empty, value, xsiType);
		}

		/// <summary>Writes an XML element with a specified qualified name in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The name to write, using its prefix if namespace-qualified, in the element text.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F1A RID: 7962 RVA: 0x000B2584 File Offset: 0x000B0784
		protected void WriteElementQualifiedName(string localName, string ns, XmlQualifiedName value, XmlQualifiedName xsiType)
		{
			localName = XmlCustomFormatter.FromXmlNCName(localName);
			this.WriteStartElement(localName, ns);
			if (xsiType != null)
			{
				this.WriteXsiType(xsiType.Name, xsiType.Namespace);
			}
			this.Writer.WriteString(this.FromXmlQualifiedName(value));
			this.WriteEndElement();
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element to be written without namespace qualification.</param>
		/// <param name="value">The text value of the XML element.</param>
		// Token: 0x06001F1B RID: 7963 RVA: 0x000B25D7 File Offset: 0x000B07D7
		protected void WriteElementString(string localName, string value)
		{
			this.WriteElementString(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		// Token: 0x06001F1C RID: 7964 RVA: 0x000B25E7 File Offset: 0x000B07E7
		protected void WriteElementString(string localName, string ns, string value)
		{
			this.WriteElementString(localName, ns, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F1D RID: 7965 RVA: 0x000B25F3 File Offset: 0x000B07F3
		protected void WriteElementString(string localName, string value, XmlQualifiedName xsiType)
		{
			this.WriteElementString(localName, string.Empty, value, xsiType);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F1E RID: 7966 RVA: 0x000B2604 File Offset: 0x000B0804
		protected void WriteElementString(string localName, string ns, string value, XmlQualifiedName xsiType)
		{
			if (value == null)
			{
				return;
			}
			if (xsiType != null)
			{
				localName = XmlCustomFormatter.FromXmlNCName(localName);
				this.WriteStartElement(localName, ns);
				this.WriteXsiType(xsiType.Name, xsiType.Namespace);
				this.Writer.WriteString(value);
				this.WriteEndElement();
				return;
			}
			this.Writer.WriteElementString(localName, ns, value);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		// Token: 0x06001F1F RID: 7967 RVA: 0x000B2664 File Offset: 0x000B0864
		protected void WriteElementStringRaw(string localName, byte[] value)
		{
			this.WriteElementStringRaw(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		// Token: 0x06001F20 RID: 7968 RVA: 0x000B2674 File Offset: 0x000B0874
		protected void WriteElementStringRaw(string localName, string value)
		{
			this.WriteElementStringRaw(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F21 RID: 7969 RVA: 0x000B2684 File Offset: 0x000B0884
		protected void WriteElementStringRaw(string localName, byte[] value, XmlQualifiedName xsiType)
		{
			this.WriteElementStringRaw(localName, string.Empty, value, xsiType);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		// Token: 0x06001F22 RID: 7970 RVA: 0x000B2694 File Offset: 0x000B0894
		protected void WriteElementStringRaw(string localName, string ns, byte[] value)
		{
			this.WriteElementStringRaw(localName, ns, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		// Token: 0x06001F23 RID: 7971 RVA: 0x000B26A0 File Offset: 0x000B08A0
		protected void WriteElementStringRaw(string localName, string ns, string value)
		{
			this.WriteElementStringRaw(localName, ns, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F24 RID: 7972 RVA: 0x000B2674 File Offset: 0x000B0874
		protected void WriteElementStringRaw(string localName, string value, XmlQualifiedName xsiType)
		{
			this.WriteElementStringRaw(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F25 RID: 7973 RVA: 0x000B26AC File Offset: 0x000B08AC
		protected void WriteElementStringRaw(string localName, string ns, byte[] value, XmlQualifiedName xsiType)
		{
			if (value == null)
			{
				return;
			}
			this.WriteStartElement(localName, ns);
			if (xsiType != null)
			{
				this.WriteXsiType(xsiType.Name, xsiType.Namespace);
			}
			if (value.Length != 0)
			{
				this.Writer.WriteBase64(value, 0, value.Length);
			}
			this.WriteEndElement();
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F26 RID: 7974 RVA: 0x000B2700 File Offset: 0x000B0900
		protected void WriteElementStringRaw(string localName, string ns, string value, XmlQualifiedName xsiType)
		{
			localName = XmlCustomFormatter.FromXmlNCName(localName);
			this.WriteStartElement(localName, ns);
			if (xsiType != null)
			{
				this.WriteXsiType(xsiType.Name, xsiType.Namespace);
			}
			this.Writer.WriteRaw(value);
			this.WriteEndElement();
		}

		/// <summary>Writes an XML element whose body is empty.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		// Token: 0x06001F27 RID: 7975 RVA: 0x000B274D File Offset: 0x000B094D
		protected void WriteEmptyTag(string name)
		{
			this.WriteEmptyTag(name, string.Empty);
		}

		/// <summary>Writes an XML element whose body is empty.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		// Token: 0x06001F28 RID: 7976 RVA: 0x000B275B File Offset: 0x000B095B
		protected void WriteEmptyTag(string name, string ns)
		{
			name = XmlCustomFormatter.FromXmlName(name);
			this.WriteStartElement(name, ns);
			this.WriteEndElement();
		}

		/// <summary>Writes a <see langword="&lt;closing&gt;" /> element tag.</summary>
		// Token: 0x06001F29 RID: 7977 RVA: 0x000B2773 File Offset: 0x000B0973
		protected void WriteEndElement()
		{
			this.WriteEndElement(null);
		}

		/// <summary>Writes a <see langword="&lt;closing&gt;" /> element tag.</summary>
		/// <param name="o">The object being serialized.</param>
		// Token: 0x06001F2A RID: 7978 RVA: 0x000B277C File Offset: 0x000B097C
		protected void WriteEndElement(object o)
		{
			if (o != null)
			{
				this.serializedObjects.Remove(o);
			}
			this.Writer.WriteEndElement();
		}

		/// <summary>Writes an <see langword="id" /> attribute that appears in a SOAP-encoded <see langword="multiRef" /> element.</summary>
		/// <param name="o">The object being serialized.</param>
		// Token: 0x06001F2B RID: 7979 RVA: 0x000B2798 File Offset: 0x000B0998
		protected void WriteId(object o)
		{
			this.WriteAttribute("id", this.GetId(o, true));
		}

		/// <summary>Writes the namespace declaration attributes.</summary>
		/// <param name="xmlns">The XML namespaces to declare.</param>
		// Token: 0x06001F2C RID: 7980 RVA: 0x000B27B0 File Offset: 0x000B09B0
		protected void WriteNamespaceDeclarations(XmlSerializerNamespaces xmlns)
		{
			if (xmlns == null)
			{
				return;
			}
			foreach (object obj in xmlns.Namespaces)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				if ((string)dictionaryEntry.Value != string.Empty && this.Writer.LookupPrefix((string)dictionaryEntry.Value) != (string)dictionaryEntry.Key)
				{
					this.WriteAttribute("xmlns", (string)dictionaryEntry.Key, "http://www.w3.org/2000/xmlns/", (string)dictionaryEntry.Value);
				}
			}
		}

		/// <summary>Writes an XML element whose body contains a valid XML qualified name. <see cref="T:System.Xml.XmlWriter" /> inserts an <see langword="xsi:nil='true'" /> attribute if the string's value is <see langword="null" />.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The XML qualified name to write in the body of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F2D RID: 7981 RVA: 0x000B2870 File Offset: 0x000B0A70
		protected void WriteNullableQualifiedNameEncoded(string name, string ns, XmlQualifiedName value, XmlQualifiedName xsiType)
		{
			if (value != null)
			{
				this.WriteElementQualifiedName(name, ns, value, xsiType);
				return;
			}
			this.WriteNullTagEncoded(name, ns);
		}

		/// <summary>Writes an XML element whose body contains a valid XML qualified name. <see cref="T:System.Xml.XmlWriter" /> inserts an <see langword="xsi:nil='true'" /> attribute if the string's value is <see langword="null" />.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The XML qualified name to write in the body of the XML element.</param>
		// Token: 0x06001F2E RID: 7982 RVA: 0x000B288F File Offset: 0x000B0A8F
		protected void WriteNullableQualifiedNameLiteral(string name, string ns, XmlQualifiedName value)
		{
			if (value != null)
			{
				this.WriteElementQualifiedName(name, ns, value);
				return;
			}
			this.WriteNullTagLiteral(name, ns);
		}

		/// <summary>Writes an XML element that contains a string as the body. <see cref="T:System.Xml.XmlWriter" /> inserts an <see langword="xsi:nil='true'" /> attribute if the string's value is <see langword="null" />.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The string to write in the body of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F2F RID: 7983 RVA: 0x000B28AC File Offset: 0x000B0AAC
		protected void WriteNullableStringEncoded(string name, string ns, string value, XmlQualifiedName xsiType)
		{
			if (value != null)
			{
				this.WriteElementString(name, ns, value, xsiType);
				return;
			}
			this.WriteNullTagEncoded(name, ns);
		}

		/// <summary>Writes a byte array as the body of an XML element. <see cref="T:System.Xml.XmlWriter" /> inserts an <see langword="xsi:nil='true'" /> attribute if the string's value is <see langword="null" />.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The byte array to write in the body of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F30 RID: 7984 RVA: 0x000B28C5 File Offset: 0x000B0AC5
		protected void WriteNullableStringEncodedRaw(string name, string ns, byte[] value, XmlQualifiedName xsiType)
		{
			if (value == null)
			{
				this.WriteNullTagEncoded(name, ns);
				return;
			}
			this.WriteElementStringRaw(name, ns, value, xsiType);
		}

		/// <summary>Writes an XML element that contains a string as the body. <see cref="T:System.Xml.XmlWriter" /> inserts an <see langword="xsi:nil='true'" /> attribute if the string's value is <see langword="null" />.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The string to write in the body of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the <see langword="xsi:type" /> attribute.</param>
		// Token: 0x06001F31 RID: 7985 RVA: 0x000B28DE File Offset: 0x000B0ADE
		protected void WriteNullableStringEncodedRaw(string name, string ns, string value, XmlQualifiedName xsiType)
		{
			if (value == null)
			{
				this.WriteNullTagEncoded(name, ns);
				return;
			}
			this.WriteElementStringRaw(name, ns, value, xsiType);
		}

		/// <summary>Writes an XML element that contains a string as the body. <see cref="T:System.Xml.XmlWriter" /> inserts an <see langword="xsi:nil='true'" /> attribute if the string's value is <see langword="null" />.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The string to write in the body of the XML element.</param>
		// Token: 0x06001F32 RID: 7986 RVA: 0x000B28F7 File Offset: 0x000B0AF7
		protected void WriteNullableStringLiteral(string name, string ns, string value)
		{
			if (value != null)
			{
				this.WriteElementString(name, ns, value, null);
				return;
			}
			this.WriteNullTagLiteral(name, ns);
		}

		/// <summary>Writes a byte array as the body of an XML element. <see cref="T:System.Xml.XmlWriter" /> inserts an <see langword="xsi:nil='true'" /> attribute if the string's value is <see langword="null" />.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The byte array to write in the body of the XML element.</param>
		// Token: 0x06001F33 RID: 7987 RVA: 0x000B290F File Offset: 0x000B0B0F
		protected void WriteNullableStringLiteralRaw(string name, string ns, byte[] value)
		{
			if (value == null)
			{
				this.WriteNullTagLiteral(name, ns);
				return;
			}
			this.WriteElementStringRaw(name, ns, value);
		}

		/// <summary>Writes an XML element that contains a string as the body. <see cref="T:System.Xml.XmlWriter" /> inserts a <see langword="xsi:nil='true'" /> attribute if the string's value is <see langword="null" />.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The string to write in the body of the XML element.</param>
		// Token: 0x06001F34 RID: 7988 RVA: 0x000B2926 File Offset: 0x000B0B26
		protected void WriteNullableStringLiteralRaw(string name, string ns, string value)
		{
			if (value == null)
			{
				this.WriteNullTagLiteral(name, ns);
				return;
			}
			this.WriteElementStringRaw(name, ns, value);
		}

		/// <summary>Writes an XML element with an <see langword="xsi:nil='true'" /> attribute.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		// Token: 0x06001F35 RID: 7989 RVA: 0x000B293D File Offset: 0x000B0B3D
		protected void WriteNullTagEncoded(string name)
		{
			this.WriteNullTagEncoded(name, string.Empty);
		}

		/// <summary>Writes an XML element with an <see langword="xsi:nil='true'" /> attribute.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		// Token: 0x06001F36 RID: 7990 RVA: 0x000B294B File Offset: 0x000B0B4B
		protected void WriteNullTagEncoded(string name, string ns)
		{
			this.Writer.WriteStartElement(name, ns);
			this.Writer.WriteAttributeString("nil", "http://www.w3.org/2001/XMLSchema-instance", "true");
			this.Writer.WriteEndElement();
		}

		/// <summary>Writes an XML element with an <see langword="xsi:nil='true'" /> attribute.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		// Token: 0x06001F37 RID: 7991 RVA: 0x000B297F File Offset: 0x000B0B7F
		protected void WriteNullTagLiteral(string name)
		{
			this.WriteNullTagLiteral(name, string.Empty);
		}

		/// <summary>Writes an XML element with an <see langword="xsi:nil='true'" /> attribute.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		// Token: 0x06001F38 RID: 7992 RVA: 0x000B298D File Offset: 0x000B0B8D
		protected void WriteNullTagLiteral(string name, string ns)
		{
			this.WriteStartElement(name, ns);
			this.Writer.WriteAttributeString("nil", "http://www.w3.org/2001/XMLSchema-instance", "true");
			this.WriteEndElement();
		}

		/// <summary>Writes a SOAP message XML element that can contain a reference to a <see langword="&lt;multiRef&gt;" /> XML element for a given object.</summary>
		/// <param name="n">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized either in the current XML element or a <see langword="multiRef" /> element that is referenced by the current element.</param>
		// Token: 0x06001F39 RID: 7993 RVA: 0x000B29B7 File Offset: 0x000B0BB7
		protected void WritePotentiallyReferencingElement(string n, string ns, object o)
		{
			this.WritePotentiallyReferencingElement(n, ns, o, null, false, false);
		}

		/// <summary>Writes a SOAP message XML element that can contain a reference to a <see langword="&lt;multiRef&gt;" /> XML element for a given object.</summary>
		/// <param name="n">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized either in the current XML element or a <see langword="multiRef" /> element that referenced by the current element.</param>
		/// <param name="ambientType">The type stored in the object's type mapping (as opposed to the object's type found directly through the <see langword="typeof" /> operation).</param>
		// Token: 0x06001F3A RID: 7994 RVA: 0x000B29C5 File Offset: 0x000B0BC5
		protected void WritePotentiallyReferencingElement(string n, string ns, object o, Type ambientType)
		{
			this.WritePotentiallyReferencingElement(n, ns, o, ambientType, false, false);
		}

		/// <summary>Writes a SOAP message XML element that can contain a reference to a <see langword="&lt;multiRef&gt;" /> XML element for a given object.</summary>
		/// <param name="n">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized either in the current XML element or a <see langword="multiRef" /> element that is referenced by the current element.</param>
		/// <param name="ambientType">The type stored in the object's type mapping (as opposed to the object's type found directly through the <see langword="typeof" /> operation).</param>
		/// <param name="suppressReference">
		///       <see langword="true" /> to serialize the object directly into the XML element rather than make the element reference another element that contains the data; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F3B RID: 7995 RVA: 0x000B29D4 File Offset: 0x000B0BD4
		protected void WritePotentiallyReferencingElement(string n, string ns, object o, Type ambientType, bool suppressReference)
		{
			this.WritePotentiallyReferencingElement(n, ns, o, ambientType, suppressReference, false);
		}

		/// <summary>Writes a SOAP message XML element that can contain a reference to a <see langword="multiRef" /> XML element for a given object.</summary>
		/// <param name="n">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized either in the current XML element or a <see langword="multiRef" /> element that referenced by the current element.</param>
		/// <param name="ambientType">The type stored in the object's type mapping (as opposed to the object's type found directly through the <see langword="typeof" /> operation).</param>
		/// <param name="suppressReference">
		///       <see langword="true" /> to serialize the object directly into the XML element rather than make the element reference another element that contains the data; otherwise, <see langword="false" />.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> to write an <see langword="xsi:nil='true'" /> attribute if the object to serialize is <see langword="null" />; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F3C RID: 7996 RVA: 0x000B29E4 File Offset: 0x000B0BE4
		protected void WritePotentiallyReferencingElement(string n, string ns, object o, Type ambientType, bool suppressReference, bool isNullable)
		{
			if (o == null)
			{
				if (isNullable)
				{
					this.WriteNullTagEncoded(n, ns);
				}
				return;
			}
			Type type = o.GetType();
			this.WriteStartElement(n, ns, true);
			this.CheckReferenceQueue();
			if (this.callbacks != null && this.callbacks.ContainsKey(o.GetType()))
			{
				XmlSerializationWriter.WriteCallbackInfo writeCallbackInfo = (XmlSerializationWriter.WriteCallbackInfo)this.callbacks[type];
				if (type.IsEnum)
				{
					writeCallbackInfo.Callback(o);
				}
				else if (suppressReference)
				{
					this.Writer.WriteAttributeString("id", this.GetId(o, false));
					if (ambientType != type)
					{
						this.WriteXsiType(writeCallbackInfo.TypeName, writeCallbackInfo.TypeNs);
					}
					writeCallbackInfo.Callback(o);
				}
				else
				{
					if (!this.AlreadyQueued(o))
					{
						this.referencedElements.Enqueue(o);
					}
					this.Writer.WriteAttributeString("href", "#" + this.GetId(o, true));
				}
			}
			else
			{
				TypeData typeData = TypeTranslator.GetTypeData(type, null, true);
				if (typeData.SchemaType == SchemaTypes.Primitive)
				{
					if (type != ambientType)
					{
						this.WriteXsiType(typeData.XmlType, "http://www.w3.org/2001/XMLSchema");
					}
					this.Writer.WriteString(XmlCustomFormatter.ToXmlString(typeData, o));
				}
				else
				{
					if (!this.IsPrimitiveArray(typeData))
					{
						throw new InvalidOperationException("Invalid type: " + type.FullName);
					}
					if (!this.AlreadyQueued(o))
					{
						this.referencedElements.Enqueue(o);
					}
					this.Writer.WriteAttributeString("href", "#" + this.GetId(o, true));
				}
			}
			this.WriteEndElement();
		}

		/// <summary>Serializes objects into SOAP-encoded <see langword="multiRef" /> XML elements in a SOAP message.</summary>
		// Token: 0x06001F3D RID: 7997 RVA: 0x000B2B88 File Offset: 0x000B0D88
		protected void WriteReferencedElements()
		{
			if (this.referencedElements == null)
			{
				return;
			}
			if (this.callbacks == null)
			{
				return;
			}
			while (this.referencedElements.Count > 0)
			{
				object obj = this.referencedElements.Dequeue();
				TypeData typeData = TypeTranslator.GetTypeData(obj.GetType());
				XmlSerializationWriter.WriteCallbackInfo writeCallbackInfo = (XmlSerializationWriter.WriteCallbackInfo)this.callbacks[obj.GetType()];
				if (writeCallbackInfo != null)
				{
					this.WriteStartElement(writeCallbackInfo.TypeName, writeCallbackInfo.TypeNs, true);
					this.Writer.WriteAttributeString("id", this.GetId(obj, false));
					if (typeData.SchemaType != SchemaTypes.Array)
					{
						this.WriteXsiType(writeCallbackInfo.TypeName, writeCallbackInfo.TypeNs);
					}
					writeCallbackInfo.Callback(obj);
					this.WriteEndElement();
				}
				else if (this.IsPrimitiveArray(typeData))
				{
					this.WriteArray(obj, typeData);
				}
			}
		}

		// Token: 0x06001F3E RID: 7998 RVA: 0x000B2C58 File Offset: 0x000B0E58
		private bool IsPrimitiveArray(TypeData td)
		{
			return td.SchemaType == SchemaTypes.Array && (td.ListItemTypeData.SchemaType == SchemaTypes.Primitive || td.ListItemType == typeof(object) || this.IsPrimitiveArray(td.ListItemTypeData));
		}

		// Token: 0x06001F3F RID: 7999 RVA: 0x000B2C98 File Offset: 0x000B0E98
		private void WriteArray(object o, TypeData td)
		{
			TypeData typeData = td;
			int num = -1;
			string text;
			do
			{
				typeData = typeData.ListItemTypeData;
				text = typeData.XmlType;
				num++;
			}
			while (typeData.SchemaType == SchemaTypes.Array);
			while (num-- > 0)
			{
				text += "[]";
			}
			this.WriteStartElement("Array", "http://schemas.xmlsoap.org/soap/encoding/", true);
			this.Writer.WriteAttributeString("id", this.GetId(o, false));
			if (td.SchemaType == SchemaTypes.Array)
			{
				Array array = (Array)o;
				int length = array.Length;
				this.Writer.WriteAttributeString("arrayType", "http://schemas.xmlsoap.org/soap/encoding/", this.GetQualifiedName(text, "http://www.w3.org/2001/XMLSchema") + "[" + length.ToString() + "]");
				for (int i = 0; i < length; i++)
				{
					this.WritePotentiallyReferencingElement("Item", "", array.GetValue(i), td.ListItemType, false, true);
				}
			}
			this.WriteEndElement();
		}

		/// <summary>Writes a SOAP message XML element that contains a reference to a <see langword="multiRef " />element for a given object.</summary>
		/// <param name="n">The local name of the referencing element being written.</param>
		/// <param name="ns">The namespace of the referencing element being written.</param>
		/// <param name="o">The object being serialized.</param>
		// Token: 0x06001F40 RID: 8000 RVA: 0x000B2D89 File Offset: 0x000B0F89
		protected void WriteReferencingElement(string n, string ns, object o)
		{
			this.WriteReferencingElement(n, ns, o, false);
		}

		/// <summary>Writes a SOAP message XML element that contains a reference to a <see langword="multiRef" /> element for a given object.</summary>
		/// <param name="n">The local name of the referencing element being written.</param>
		/// <param name="ns">The namespace of the referencing element being written.</param>
		/// <param name="o">The object being serialized.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> to write an <see langword="xsi:nil='true'" /> attribute if the object to serialize is <see langword="null" />; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F41 RID: 8001 RVA: 0x000B2D98 File Offset: 0x000B0F98
		protected void WriteReferencingElement(string n, string ns, object o, bool isNullable)
		{
			if (o == null)
			{
				if (isNullable)
				{
					this.WriteNullTagEncoded(n, ns);
				}
				return;
			}
			this.CheckReferenceQueue();
			if (!this.AlreadyQueued(o))
			{
				this.referencedElements.Enqueue(o);
			}
			this.Writer.WriteStartElement(n, ns);
			this.Writer.WriteAttributeString("href", "#" + this.GetId(o, true));
			this.Writer.WriteEndElement();
		}

		// Token: 0x06001F42 RID: 8002 RVA: 0x000B2E0A File Offset: 0x000B100A
		private void CheckReferenceQueue()
		{
			if (this.referencedElements == null)
			{
				this.referencedElements = new Queue();
				this.InitCallbacks();
			}
		}

		/// <summary>Writes a SOAP 1.2 RPC result element with a specified qualified name in its body.</summary>
		/// <param name="name">The local name of the result body.</param>
		/// <param name="ns">The namespace of the result body.</param>
		// Token: 0x06001F43 RID: 8003 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected void WriteRpcResult(string name, string ns)
		{
			throw new NotImplementedException();
		}

		/// <summary>Writes an object that uses custom XML formatting as an XML element.</summary>
		/// <param name="serializable">An object that implements the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> interface that uses custom XML formatting.</param>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> to write an <see langword="xsi:nil='true'" /> attribute if the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> class object is <see langword="null" />; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F44 RID: 8004 RVA: 0x000B2E25 File Offset: 0x000B1025
		protected void WriteSerializable(IXmlSerializable serializable, string name, string ns, bool isNullable)
		{
			this.WriteSerializable(serializable, name, ns, isNullable, true);
		}

		/// <summary>Instructs <see cref="T:System.Xml.XmlNode" /> to write an object that uses custom XML formatting as an XML element.</summary>
		/// <param name="serializable">An object that implements the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> interface that uses custom XML formatting.</param>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="isNullable">
		///       <see langword="true" /> to write an <see langword="xsi:nil='true'" /> attribute if the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> object is <see langword="null" />; otherwise, <see langword="false" />.</param>
		/// <param name="wrapped">
		///       <see langword="true" /> to ignore writing the opening element tag; otherwise, <see langword="false" /> to write the opening element tag.</param>
		// Token: 0x06001F45 RID: 8005 RVA: 0x000B2E34 File Offset: 0x000B1034
		protected void WriteSerializable(IXmlSerializable serializable, string name, string ns, bool isNullable, bool wrapped)
		{
			if (serializable == null)
			{
				if (isNullable && wrapped)
				{
					this.WriteNullTagLiteral(name, ns);
				}
				return;
			}
			if (wrapped)
			{
				this.Writer.WriteStartElement(name, ns);
			}
			serializable.WriteXml(this.Writer);
			if (wrapped)
			{
				this.Writer.WriteEndElement();
			}
		}

		/// <summary>Writes the XML declaration if the writer is positioned at the start of an XML document.</summary>
		// Token: 0x06001F46 RID: 8006 RVA: 0x000B2E80 File Offset: 0x000B1080
		protected void WriteStartDocument()
		{
			if (this.Writer.WriteState == WriteState.Start)
			{
				this.Writer.WriteStartDocument();
			}
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		// Token: 0x06001F47 RID: 8007 RVA: 0x000B2E9A File Offset: 0x000B109A
		protected void WriteStartElement(string name)
		{
			this.WriteStartElement(name, string.Empty, null, false);
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		// Token: 0x06001F48 RID: 8008 RVA: 0x000B2EAA File Offset: 0x000B10AA
		protected void WriteStartElement(string name, string ns)
		{
			this.WriteStartElement(name, ns, null, false);
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="writePrefixed">
		///       <see langword="true" /> to write the element name with a prefix if none is available for the specified namespace; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F49 RID: 8009 RVA: 0x000B2EB6 File Offset: 0x000B10B6
		protected void WriteStartElement(string name, string ns, bool writePrefixed)
		{
			this.WriteStartElement(name, ns, null, writePrefixed);
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized as an XML element.</param>
		// Token: 0x06001F4A RID: 8010 RVA: 0x000B2EC2 File Offset: 0x000B10C2
		protected void WriteStartElement(string name, string ns, object o)
		{
			this.WriteStartElement(name, ns, o, false);
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized as an XML element.</param>
		/// <param name="writePrefixed">
		///       <see langword="true" /> to write the element name with a prefix if none is available for the specified namespace; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F4B RID: 8011 RVA: 0x000B2ECE File Offset: 0x000B10CE
		protected void WriteStartElement(string name, string ns, object o, bool writePrefixed)
		{
			this.WriteStartElement(name, ns, o, writePrefixed, this.namespaces);
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized as an XML element.</param>
		/// <param name="writePrefixed">
		///       <see langword="true" /> to write the element name with a prefix if none is available for the specified namespace; otherwise, <see langword="false" />.</param>
		/// <param name="xmlns">An instance of the <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> class that contains prefix and namespace pairs to be used in the generated XML.</param>
		// Token: 0x06001F4C RID: 8012 RVA: 0x000B2EE1 File Offset: 0x000B10E1
		protected void WriteStartElement(string name, string ns, object o, bool writePrefixed, XmlSerializerNamespaces xmlns)
		{
			this.WriteStartElement(name, ns, o, writePrefixed, (xmlns != null) ? xmlns.ToArray() : null);
		}

		// Token: 0x06001F4D RID: 8013 RVA: 0x000B2EFC File Offset: 0x000B10FC
		private void WriteStartElement(string name, string ns, object o, bool writePrefixed, ICollection namespaces)
		{
			if (o != null)
			{
				if (this.serializedObjects.Contains(o))
				{
					throw new InvalidOperationException("A circular reference was detected while serializing an object of type " + o.GetType().Name);
				}
				this.serializedObjects[o] = o;
			}
			string text = null;
			if (this.topLevelElement && ns != null && ns.Length != 0 && namespaces != null)
			{
				foreach (object obj in namespaces)
				{
					XmlQualifiedName xmlQualifiedName = (XmlQualifiedName)obj;
					if (xmlQualifiedName.Namespace == ns)
					{
						text = xmlQualifiedName.Name;
						writePrefixed = true;
						break;
					}
				}
			}
			if (writePrefixed && ns != string.Empty)
			{
				name = XmlCustomFormatter.FromXmlName(name);
				if (text == null)
				{
					text = this.Writer.LookupPrefix(ns);
				}
				if (text == null || text.Length == 0)
				{
					object arg = "q";
					int num = this.qnameCount + 1;
					this.qnameCount = num;
					text = arg + num;
				}
				this.Writer.WriteStartElement(text, name, ns);
			}
			else
			{
				this.Writer.WriteStartElement(name, ns);
			}
			if (this.topLevelElement)
			{
				if (namespaces != null)
				{
					foreach (object obj2 in namespaces)
					{
						XmlQualifiedName xmlQualifiedName2 = (XmlQualifiedName)obj2;
						string text2 = this.Writer.LookupPrefix(xmlQualifiedName2.Namespace);
						if (text2 == null || text2.Length == 0)
						{
							this.WriteAttribute("xmlns", xmlQualifiedName2.Name, "http://www.w3.org/2000/xmlns/", xmlQualifiedName2.Namespace);
						}
					}
				}
				this.topLevelElement = false;
			}
		}

		/// <summary>Writes an XML element whose text body is a value of a simple XML Schema data type.</summary>
		/// <param name="name">The local name of the element to write.</param>
		/// <param name="ns">The namespace of the element to write.</param>
		/// <param name="o">The object to be serialized in the element body.</param>
		/// <param name="xsiType">
		///       <see langword="true" /> if the XML element explicitly specifies the text value's type using the <see langword="xsi:type" /> attribute; otherwise, <see langword="false" />.</param>
		// Token: 0x06001F4E RID: 8014 RVA: 0x000B30BC File Offset: 0x000B12BC
		protected void WriteTypedPrimitive(string name, string ns, object o, bool xsiType)
		{
			TypeData typeData = TypeTranslator.GetTypeData(o.GetType(), null, true);
			if (typeData.SchemaType != SchemaTypes.Primitive)
			{
				throw new InvalidOperationException(string.Format("The type of the argument object '{0}' is not primitive.", typeData.FullTypeName));
			}
			if (name == null)
			{
				ns = (typeData.IsXsdType ? "http://www.w3.org/2001/XMLSchema" : "http://microsoft.com/wsdl/types/");
				name = typeData.XmlType;
			}
			else
			{
				name = XmlCustomFormatter.FromXmlName(name);
			}
			this.Writer.WriteStartElement(name, ns);
			string value;
			if (o is XmlQualifiedName)
			{
				value = this.FromXmlQualifiedName((XmlQualifiedName)o);
			}
			else
			{
				value = XmlCustomFormatter.ToXmlString(typeData, o);
			}
			if (xsiType)
			{
				if (typeData.SchemaType != SchemaTypes.Primitive)
				{
					throw new InvalidOperationException(string.Format("The type {0} was not expected. Use the XmlInclude or SoapInclude attribute to specify types that are not known statically.", o.GetType().FullName));
				}
				this.WriteXsiType(typeData.XmlType, typeData.IsXsdType ? "http://www.w3.org/2001/XMLSchema" : "http://microsoft.com/wsdl/types/");
			}
			this.WriteValue(value);
			this.Writer.WriteEndElement();
		}

		/// <summary>Writes a base-64 byte array.</summary>
		/// <param name="value">The byte array to write.</param>
		// Token: 0x06001F4F RID: 8015 RVA: 0x000B31A7 File Offset: 0x000B13A7
		protected void WriteValue(byte[] value)
		{
			this.Writer.WriteBase64(value, 0, value.Length);
		}

		/// <summary>Writes a specified string value.</summary>
		/// <param name="value">The value of the string to write.</param>
		// Token: 0x06001F50 RID: 8016 RVA: 0x000B31B9 File Offset: 0x000B13B9
		protected void WriteValue(string value)
		{
			if (value != null)
			{
				this.Writer.WriteString(value);
			}
		}

		/// <summary>Writes the specified <see cref="T:System.Xml.XmlNode" /> as an XML attribute.</summary>
		/// <param name="node">The XML node to write.</param>
		// Token: 0x06001F51 RID: 8017 RVA: 0x000B31CA File Offset: 0x000B13CA
		protected void WriteXmlAttribute(XmlNode node)
		{
			this.WriteXmlAttribute(node, null);
		}

		/// <summary>Writes the specified <see cref="T:System.Xml.XmlNode" /> object as an XML attribute.</summary>
		/// <param name="node">The XML node to write.</param>
		/// <param name="container">An <see cref="T:System.Xml.Schema.XmlSchemaObject" /> object (or <see langword="null" />) used to generate a qualified name value for an <see langword="arrayType" /> attribute from the Web Services Description Language (WSDL) namespace ("http://schemas.xmlsoap.org/wsdl/").</param>
		// Token: 0x06001F52 RID: 8018 RVA: 0x000B31D4 File Offset: 0x000B13D4
		protected void WriteXmlAttribute(XmlNode node, object container)
		{
			XmlAttribute xmlAttribute = node as XmlAttribute;
			if (xmlAttribute == null)
			{
				throw new InvalidOperationException("The node must be either type XmlAttribute or a derived type.");
			}
			if (xmlAttribute.NamespaceURI == "http://schemas.xmlsoap.org/wsdl/" && xmlAttribute.LocalName == "arrayType")
			{
				string str;
				string ns;
				string str2;
				TypeTranslator.ParseArrayType(xmlAttribute.Value, out str, out ns, out str2);
				string qualifiedName = this.GetQualifiedName(str + str2, ns);
				this.WriteAttribute(xmlAttribute.Prefix, xmlAttribute.LocalName, xmlAttribute.NamespaceURI, qualifiedName);
				return;
			}
			this.WriteAttribute(xmlAttribute.Prefix, xmlAttribute.LocalName, xmlAttribute.NamespaceURI, xmlAttribute.Value);
		}

		/// <summary>Writes an <see langword="xsi:type" /> attribute for an XML element that is being serialized into a document.</summary>
		/// <param name="name">The local name of an XML Schema data type.</param>
		/// <param name="ns">The namespace of an XML Schema data type.</param>
		// Token: 0x06001F53 RID: 8019 RVA: 0x000B3274 File Offset: 0x000B1474
		protected void WriteXsiType(string name, string ns)
		{
			if (ns != null && ns != string.Empty)
			{
				this.WriteAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", this.GetQualifiedName(name, ns));
				return;
			}
			this.WriteAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", name);
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates the <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> which has been invalidly applied to a member; only members that are of type <see cref="T:System.Xml.XmlNode" />, or derived from <see cref="T:System.Xml.XmlNode" />, are valid.</summary>
		/// <param name="o">The object that represents the invalid member.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001F54 RID: 8020 RVA: 0x000B32B0 File Offset: 0x000B14B0
		protected Exception CreateInvalidAnyTypeException(object o)
		{
			if (o == null)
			{
				return new InvalidOperationException("null is invalid as anyType in XmlSerializer");
			}
			return this.CreateInvalidAnyTypeException(o.GetType());
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates the <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> which has been invalidly applied to a member; only members that are of type <see cref="T:System.Xml.XmlNode" />, or derived from <see cref="T:System.Xml.XmlNode" />, are valid.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> that is invalid.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001F55 RID: 8021 RVA: 0x000B32CC File Offset: 0x000B14CC
		protected Exception CreateInvalidAnyTypeException(Type type)
		{
			return new InvalidOperationException(string.Format("An object of type '{0}' is invalid as anyType in XmlSerializer", type));
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> for an invalid enumeration value.</summary>
		/// <param name="value">An object that represents the invalid enumeration.</param>
		/// <param name="typeName">The XML type name.</param>
		/// <returns>The newly created exception.</returns>
		// Token: 0x06001F56 RID: 8022 RVA: 0x000B32DE File Offset: 0x000B14DE
		protected Exception CreateInvalidEnumValueException(object value, string typeName)
		{
			return new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "'{0}' is not a valid value for {1}.", value, typeName));
		}

		/// <summary>Takes a numeric enumeration value and the names and constants from the enumerator list for the enumeration and returns a string that consists of delimited identifiers that represent the enumeration members that have been set.</summary>
		/// <param name="value">The enumeration value as a series of bitwise <see langword="OR" /> operations.</param>
		/// <param name="values">The values of the enumeration.</param>
		/// <param name="ids">The constants of the enumeration.</param>
		/// <param name="typeName">The name of the type </param>
		/// <returns>A string that consists of delimited identifiers, where each item is one of the values set by the bitwise operation.</returns>
		// Token: 0x06001F57 RID: 8023 RVA: 0x000B32F6 File Offset: 0x000B14F6
		protected static string FromEnum(long value, string[] values, long[] ids, string typeName)
		{
			return XmlCustomFormatter.FromEnum(value, values, ids, typeName);
		}

		/// <summary>Produces a string that can be written as an XML qualified name, with invalid characters replaced by escape sequences.</summary>
		/// <param name="xmlQualifiedName">An <see cref="T:System.Xml.XmlQualifiedName" /> that represents the XML to be written.</param>
		/// <param name="ignoreEmpty">
		///       <see langword="true" /> to ignore empty spaces in the string; otherwise, <see langword="false" />.</param>
		/// <returns>An XML qualified name, with invalid characters replaced by escape sequences.</returns>
		// Token: 0x06001F58 RID: 8024 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected string FromXmlQualifiedName(XmlQualifiedName xmlQualifiedName, bool ignoreEmpty)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a dynamically generated assembly by name.</summary>
		/// <param name="assemblyFullName">The full name of the assembly.</param>
		/// <returns>A dynamically generated assembly.</returns>
		// Token: 0x06001F59 RID: 8025 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected static Assembly ResolveDynamicAssembly(string assemblyFullName)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="M:System.Xml.XmlConvert.EncodeName(System.String)" /> method is used to write valid XML.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="M:System.Xml.Serialization.XmlSerializationWriter.FromXmlQualifiedName(System.Xml.XmlQualifiedName)" /> method returns an encoded name; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005E5 RID: 1509
		// (get) Token: 0x06001F5A RID: 8026 RVA: 0x0000A6CF File Offset: 0x000088CF
		// (set) Token: 0x06001F5B RID: 8027 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		protected bool EscapeName
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x040016FE RID: 5886
		private ObjectIDGenerator idGenerator;

		// Token: 0x040016FF RID: 5887
		private int qnameCount;

		// Token: 0x04001700 RID: 5888
		private bool topLevelElement;

		// Token: 0x04001701 RID: 5889
		private ArrayList namespaces;

		// Token: 0x04001702 RID: 5890
		private XmlWriter writer;

		// Token: 0x04001703 RID: 5891
		private Queue referencedElements;

		// Token: 0x04001704 RID: 5892
		private Hashtable callbacks;

		// Token: 0x04001705 RID: 5893
		private Hashtable serializedObjects;

		// Token: 0x04001706 RID: 5894
		private const string xmlNamespace = "http://www.w3.org/2000/xmlns/";

		// Token: 0x04001707 RID: 5895
		private const string unexpectedTypeError = "The type {0} was not expected. Use the XmlInclude or SoapInclude attribute to specify types that are not known statically.";

		// Token: 0x02000334 RID: 820
		private class WriteCallbackInfo
		{
			// Token: 0x06001F5C RID: 8028 RVA: 0x00002103 File Offset: 0x00000303
			public WriteCallbackInfo()
			{
			}

			// Token: 0x04001708 RID: 5896
			public Type Type;

			// Token: 0x04001709 RID: 5897
			public string TypeName;

			// Token: 0x0400170A RID: 5898
			public string TypeNs;

			// Token: 0x0400170B RID: 5899
			public XmlSerializationWriteCallback Callback;
		}
	}
}
