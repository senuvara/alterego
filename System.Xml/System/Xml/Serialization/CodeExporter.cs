﻿using System;
using System.CodeDom;
using System.Security.Permissions;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Represents a class that can generate proxy code from an XML representation of a data structure.</summary>
	// Token: 0x02000568 RID: 1384
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public abstract class CodeExporter
	{
		// Token: 0x0600343C RID: 13372 RVA: 0x00072668 File Offset: 0x00070868
		internal CodeExporter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a collection of code attribute metadata that is included when the code is exported.</summary>
		/// <returns>A collection of <see cref="T:System.CodeDom.CodeAttributeDeclaration" /> objects that represent metadata that is included when the code is exported.</returns>
		// Token: 0x17000AE9 RID: 2793
		// (get) Token: 0x0600343D RID: 13373 RVA: 0x000A7058 File Offset: 0x000A5258
		public CodeAttributeDeclarationCollection IncludeMetadata
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
