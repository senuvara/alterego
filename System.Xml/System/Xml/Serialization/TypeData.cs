﻿using System;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x02000301 RID: 769
	internal class TypeData
	{
		// Token: 0x06001C43 RID: 7235 RVA: 0x000A2CC6 File Offset: 0x000A0EC6
		public TypeData(Type type, string elementName, bool isPrimitive) : this(type, elementName, isPrimitive, null, null)
		{
		}

		// Token: 0x06001C44 RID: 7236 RVA: 0x000A2CD4 File Offset: 0x000A0ED4
		public TypeData(Type type, string elementName, bool isPrimitive, TypeData mappedType, XmlSchemaPatternFacet facet)
		{
			this.hasPublicConstructor = true;
			base..ctor();
			if (type.IsGenericTypeDefinition)
			{
				throw new InvalidOperationException("Generic type definition cannot be used in serialization. Only specific generic types can be used.");
			}
			this.mappedType = mappedType;
			this.facet = facet;
			this.type = type;
			this.typeName = type.Name;
			this.fullTypeName = type.FullName.Replace('+', '.');
			if (isPrimitive)
			{
				this.sType = SchemaTypes.Primitive;
			}
			else if (type.IsEnum)
			{
				this.sType = SchemaTypes.Enum;
			}
			else if (typeof(IXmlSerializable).IsAssignableFrom(type))
			{
				this.sType = SchemaTypes.XmlSerializable;
			}
			else if (typeof(XmlNode).IsAssignableFrom(type))
			{
				this.sType = SchemaTypes.XmlNode;
			}
			else if (type.IsArray || typeof(IEnumerable).IsAssignableFrom(type))
			{
				this.sType = SchemaTypes.Array;
			}
			else
			{
				this.sType = SchemaTypes.Class;
			}
			if (this.IsListType)
			{
				this.elementName = TypeTranslator.GetArrayName(this.ListItemTypeData.XmlType);
			}
			else
			{
				this.elementName = elementName;
			}
			if (this.sType == SchemaTypes.Array || this.sType == SchemaTypes.Class)
			{
				this.hasPublicConstructor = (!type.IsInterface && (type.IsArray || type.GetConstructor(Type.EmptyTypes) != null || type.IsAbstract || type.IsValueType));
			}
			this.LookupTypeConvertor();
		}

		// Token: 0x06001C45 RID: 7237 RVA: 0x000A2E34 File Offset: 0x000A1034
		internal TypeData(string typeName, string fullTypeName, string xmlType, SchemaTypes schemaType, TypeData listItemTypeData)
		{
			this.hasPublicConstructor = true;
			base..ctor();
			this.elementName = xmlType;
			this.typeName = typeName;
			this.fullTypeName = fullTypeName.Replace('+', '.');
			this.listItemTypeData = listItemTypeData;
			this.sType = schemaType;
			this.hasPublicConstructor = true;
		}

		// Token: 0x06001C46 RID: 7238 RVA: 0x000A2E84 File Offset: 0x000A1084
		private void LookupTypeConvertor()
		{
			Type elementType = this.type;
			if (elementType.IsArray)
			{
				elementType = elementType.GetElementType();
			}
			XmlTypeConvertorAttribute customAttribute = elementType.GetCustomAttribute<XmlTypeConvertorAttribute>();
			if (customAttribute != null)
			{
				this.typeConvertor = elementType.GetMethod(customAttribute.Method, BindingFlags.Static | BindingFlags.NonPublic);
			}
		}

		// Token: 0x06001C47 RID: 7239 RVA: 0x000A2EC5 File Offset: 0x000A10C5
		internal void ConvertForAssignment(ref object value)
		{
			if (this.typeConvertor != null)
			{
				value = this.typeConvertor.Invoke(null, new object[]
				{
					value
				});
			}
		}

		// Token: 0x17000558 RID: 1368
		// (get) Token: 0x06001C48 RID: 7240 RVA: 0x000A2EEE File Offset: 0x000A10EE
		public string TypeName
		{
			get
			{
				return this.typeName;
			}
		}

		// Token: 0x17000559 RID: 1369
		// (get) Token: 0x06001C49 RID: 7241 RVA: 0x000A2EF6 File Offset: 0x000A10F6
		public string XmlType
		{
			get
			{
				return this.elementName;
			}
		}

		// Token: 0x1700055A RID: 1370
		// (get) Token: 0x06001C4A RID: 7242 RVA: 0x000A2EFE File Offset: 0x000A10FE
		public Type Type
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x06001C4B RID: 7243 RVA: 0x000A2F06 File Offset: 0x000A1106
		public string FullTypeName
		{
			get
			{
				return this.fullTypeName;
			}
		}

		// Token: 0x1700055C RID: 1372
		// (get) Token: 0x06001C4C RID: 7244 RVA: 0x000A2F0E File Offset: 0x000A110E
		public string CSharpName
		{
			get
			{
				if (this.csharpName == null)
				{
					this.csharpName = ((this.Type == null) ? this.TypeName : TypeData.ToCSharpName(this.Type, false));
				}
				return this.csharpName;
			}
		}

		// Token: 0x1700055D RID: 1373
		// (get) Token: 0x06001C4D RID: 7245 RVA: 0x000A2F46 File Offset: 0x000A1146
		public string CSharpFullName
		{
			get
			{
				if (this.csharpFullName == null)
				{
					this.csharpFullName = ((this.Type == null) ? this.TypeName : TypeData.ToCSharpName(this.Type, true));
				}
				return this.csharpFullName;
			}
		}

		// Token: 0x06001C4E RID: 7246 RVA: 0x000A2F80 File Offset: 0x000A1180
		public static string ToCSharpName(Type type, bool full)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (type.IsArray)
			{
				stringBuilder.Append(TypeData.ToCSharpName(type.GetElementType(), full));
				stringBuilder.Append('[');
				int arrayRank = type.GetArrayRank();
				for (int i = 1; i < arrayRank; i++)
				{
					stringBuilder.Append(',');
				}
				stringBuilder.Append(']');
				return stringBuilder.ToString();
			}
			if (type.IsGenericType && !type.IsGenericTypeDefinition)
			{
				Type[] genericArguments = type.GetGenericArguments();
				int j = genericArguments.Length - 1;
				Type type2 = type;
				while (type2 != null)
				{
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Insert(0, '.');
					}
					int num = type2.Name.IndexOf('`');
					if (num != -1)
					{
						int num2 = j - int.Parse(type2.Name.Substring(num + 1));
						stringBuilder.Insert(0, '>');
						while (j > num2)
						{
							stringBuilder.Insert(0, TypeData.ToCSharpName(genericArguments[j], full));
							if (j - 1 != num2)
							{
								stringBuilder.Insert(0, ',');
							}
							j--;
						}
						stringBuilder.Insert(0, '<');
						stringBuilder.Insert(0, type2.Name.Substring(0, num));
					}
					else
					{
						stringBuilder.Insert(0, type2.Name);
					}
					type2 = type2.DeclaringType;
				}
				if (full && type.Namespace.Length > 0)
				{
					stringBuilder.Insert(0, type.Namespace + ".");
				}
				return stringBuilder.ToString();
			}
			if (type.DeclaringType != null)
			{
				stringBuilder.Append(TypeData.ToCSharpName(type.DeclaringType, full)).Append('.');
				stringBuilder.Append(type.Name);
			}
			else
			{
				if (full && !string.IsNullOrEmpty(type.Namespace))
				{
					stringBuilder.Append(type.Namespace).Append('.');
				}
				stringBuilder.Append(type.Name);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001C4F RID: 7247 RVA: 0x000A3174 File Offset: 0x000A1374
		private static bool IsKeyword(string name)
		{
			if (TypeData.keywordsTable == null)
			{
				Hashtable hashtable = new Hashtable();
				foreach (string text in TypeData.keywords)
				{
					hashtable[text] = text;
				}
				TypeData.keywordsTable = hashtable;
			}
			return TypeData.keywordsTable.Contains(name);
		}

		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x06001C50 RID: 7248 RVA: 0x000A31BF File Offset: 0x000A13BF
		public SchemaTypes SchemaType
		{
			get
			{
				return this.sType;
			}
		}

		// Token: 0x1700055F RID: 1375
		// (get) Token: 0x06001C51 RID: 7249 RVA: 0x000A31C7 File Offset: 0x000A13C7
		public bool IsListType
		{
			get
			{
				return this.SchemaType == SchemaTypes.Array;
			}
		}

		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x06001C52 RID: 7250 RVA: 0x000A31D2 File Offset: 0x000A13D2
		public bool IsComplexType
		{
			get
			{
				return this.SchemaType == SchemaTypes.Class || this.SchemaType == SchemaTypes.Array || this.SchemaType == SchemaTypes.Enum || this.SchemaType == SchemaTypes.XmlNode || this.SchemaType == SchemaTypes.XmlSerializable || !this.IsXsdType;
			}
		}

		// Token: 0x17000561 RID: 1377
		// (get) Token: 0x06001C53 RID: 7251 RVA: 0x000A320C File Offset: 0x000A140C
		public bool IsValueType
		{
			get
			{
				if (this.type != null)
				{
					return this.type.IsValueType;
				}
				return this.sType == SchemaTypes.Primitive || this.sType == SchemaTypes.Enum;
			}
		}

		// Token: 0x17000562 RID: 1378
		// (get) Token: 0x06001C54 RID: 7252 RVA: 0x000A323C File Offset: 0x000A143C
		public bool NullableOverride
		{
			get
			{
				return this.nullableOverride;
			}
		}

		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x06001C55 RID: 7253 RVA: 0x000A3244 File Offset: 0x000A1444
		// (set) Token: 0x06001C56 RID: 7254 RVA: 0x000A329C File Offset: 0x000A149C
		public bool IsNullable
		{
			get
			{
				return this.nullableOverride || !this.IsValueType || (this.type != null && this.type.IsGenericType && this.type.GetGenericTypeDefinition() == typeof(Nullable<>));
			}
			set
			{
				this.nullableOverride = value;
			}
		}

		// Token: 0x17000564 RID: 1380
		// (get) Token: 0x06001C57 RID: 7255 RVA: 0x000A32A5 File Offset: 0x000A14A5
		public TypeData ListItemTypeData
		{
			get
			{
				if (this.listItemTypeData == null && this.type != null)
				{
					this.listItemTypeData = TypeTranslator.GetTypeData(this.ListItemType);
				}
				return this.listItemTypeData;
			}
		}

		// Token: 0x17000565 RID: 1381
		// (get) Token: 0x06001C58 RID: 7256 RVA: 0x000A32D4 File Offset: 0x000A14D4
		public Type ListItemType
		{
			get
			{
				if (this.type == null)
				{
					throw new InvalidOperationException("Property ListItemType is not supported for custom types");
				}
				if (this.listItemType != null)
				{
					return this.listItemType;
				}
				Type left = null;
				if (this.SchemaType != SchemaTypes.Array)
				{
					throw new InvalidOperationException(this.Type.FullName + " is not a collection");
				}
				if (this.type.IsArray)
				{
					this.listItemType = this.type.GetElementType();
				}
				else if (typeof(ICollection).IsAssignableFrom(this.type) || (left = TypeData.GetGenericListItemType(this.type)) != null)
				{
					if (typeof(IDictionary).IsAssignableFrom(this.type))
					{
						throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "The type {0} is not supported because it implements IDictionary.", this.type.FullName));
					}
					if (left != null)
					{
						this.listItemType = left;
					}
					else
					{
						PropertyInfo indexerProperty = TypeData.GetIndexerProperty(this.type);
						if (indexerProperty == null)
						{
							throw new InvalidOperationException("You must implement a default accessor on " + this.type.FullName + " because it inherits from ICollection");
						}
						this.listItemType = indexerProperty.PropertyType;
					}
					if (this.type.GetMethod("Add", new Type[]
					{
						this.listItemType
					}) == null)
					{
						throw TypeData.CreateMissingAddMethodException(this.type, "ICollection", this.listItemType);
					}
				}
				else
				{
					MethodInfo method = this.type.GetMethod("GetEnumerator", Type.EmptyTypes);
					if (method == null)
					{
						method = this.type.GetMethod("System.Collections.IEnumerable.GetEnumerator", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
					}
					PropertyInfo property = method.ReturnType.GetProperty("Current");
					if (property == null)
					{
						this.listItemType = typeof(object);
					}
					else
					{
						this.listItemType = property.PropertyType;
					}
					if (this.type.GetMethod("Add", new Type[]
					{
						this.listItemType
					}) == null)
					{
						throw TypeData.CreateMissingAddMethodException(this.type, "IEnumerable", this.listItemType);
					}
				}
				return this.listItemType;
			}
		}

		// Token: 0x17000566 RID: 1382
		// (get) Token: 0x06001C59 RID: 7257 RVA: 0x000A3508 File Offset: 0x000A1708
		public TypeData ListTypeData
		{
			get
			{
				if (this.listTypeData != null)
				{
					return this.listTypeData;
				}
				this.listTypeData = new TypeData(this.TypeName + "[]", this.FullTypeName + "[]", TypeTranslator.GetArrayName(this.XmlType), SchemaTypes.Array, this);
				return this.listTypeData;
			}
		}

		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x06001C5A RID: 7258 RVA: 0x000A3562 File Offset: 0x000A1762
		public bool IsXsdType
		{
			get
			{
				return this.mappedType == null;
			}
		}

		// Token: 0x17000568 RID: 1384
		// (get) Token: 0x06001C5B RID: 7259 RVA: 0x000A356D File Offset: 0x000A176D
		public TypeData MappedType
		{
			get
			{
				if (this.mappedType == null)
				{
					return this;
				}
				return this.mappedType;
			}
		}

		// Token: 0x17000569 RID: 1385
		// (get) Token: 0x06001C5C RID: 7260 RVA: 0x000A357F File Offset: 0x000A177F
		public XmlSchemaPatternFacet XmlSchemaPatternFacet
		{
			get
			{
				return this.facet;
			}
		}

		// Token: 0x1700056A RID: 1386
		// (get) Token: 0x06001C5D RID: 7261 RVA: 0x000A3587 File Offset: 0x000A1787
		public bool HasPublicConstructor
		{
			get
			{
				return this.hasPublicConstructor;
			}
		}

		// Token: 0x06001C5E RID: 7262 RVA: 0x000A3590 File Offset: 0x000A1790
		public static PropertyInfo GetIndexerProperty(Type collectionType)
		{
			foreach (PropertyInfo propertyInfo in collectionType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
			{
				ParameterInfo[] indexParameters = propertyInfo.GetIndexParameters();
				if (indexParameters != null && indexParameters.Length == 1 && indexParameters[0].ParameterType == typeof(int))
				{
					return propertyInfo;
				}
			}
			return null;
		}

		// Token: 0x06001C5F RID: 7263 RVA: 0x000A35E4 File Offset: 0x000A17E4
		private static InvalidOperationException CreateMissingAddMethodException(Type type, string inheritFrom, Type argumentType)
		{
			return new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "To be XML serializable, types which inherit from {0} must have an implementation of Add({1}) at all levels of their inheritance hierarchy. {2} does not implement Add({1}).", inheritFrom, argumentType.FullName, type.FullName));
		}

		// Token: 0x06001C60 RID: 7264 RVA: 0x000A3608 File Offset: 0x000A1808
		internal static Type GetGenericListItemType(Type type)
		{
			if (type.IsGenericType && typeof(IEnumerable).IsAssignableFrom(type.GetGenericTypeDefinition()))
			{
				Type[] genericArguments = type.GetGenericArguments();
				if (type.GetMethod("Add", genericArguments) != null)
				{
					return genericArguments[0];
				}
			}
			Type[] interfaces = type.GetInterfaces();
			for (int i = 0; i < interfaces.Length; i++)
			{
				Type genericListItemType;
				if ((genericListItemType = TypeData.GetGenericListItemType(interfaces[i])) != null)
				{
					return genericListItemType;
				}
			}
			return null;
		}

		// Token: 0x06001C61 RID: 7265 RVA: 0x000A3680 File Offset: 0x000A1880
		// Note: this type is marked as 'beforefieldinit'.
		static TypeData()
		{
		}

		// Token: 0x0400162C RID: 5676
		private Type type;

		// Token: 0x0400162D RID: 5677
		private string elementName;

		// Token: 0x0400162E RID: 5678
		private SchemaTypes sType;

		// Token: 0x0400162F RID: 5679
		private Type listItemType;

		// Token: 0x04001630 RID: 5680
		private string typeName;

		// Token: 0x04001631 RID: 5681
		private string fullTypeName;

		// Token: 0x04001632 RID: 5682
		private string csharpName;

		// Token: 0x04001633 RID: 5683
		private string csharpFullName;

		// Token: 0x04001634 RID: 5684
		private TypeData listItemTypeData;

		// Token: 0x04001635 RID: 5685
		private TypeData listTypeData;

		// Token: 0x04001636 RID: 5686
		private TypeData mappedType;

		// Token: 0x04001637 RID: 5687
		private XmlSchemaPatternFacet facet;

		// Token: 0x04001638 RID: 5688
		private MethodInfo typeConvertor;

		// Token: 0x04001639 RID: 5689
		private bool hasPublicConstructor;

		// Token: 0x0400163A RID: 5690
		private bool nullableOverride;

		// Token: 0x0400163B RID: 5691
		private static Hashtable keywordsTable;

		// Token: 0x0400163C RID: 5692
		private static string[] keywords = new string[]
		{
			"abstract",
			"event",
			"new",
			"struct",
			"as",
			"explicit",
			"null",
			"switch",
			"base",
			"extern",
			"this",
			"false",
			"operator",
			"throw",
			"break",
			"finally",
			"out",
			"true",
			"fixed",
			"override",
			"try",
			"case",
			"params",
			"typeof",
			"catch",
			"for",
			"private",
			"foreach",
			"protected",
			"checked",
			"goto",
			"public",
			"unchecked",
			"class",
			"if",
			"readonly",
			"unsafe",
			"const",
			"implicit",
			"ref",
			"continue",
			"in",
			"return",
			"using",
			"virtual",
			"default",
			"interface",
			"sealed",
			"volatile",
			"delegate",
			"internal",
			"do",
			"is",
			"sizeof",
			"while",
			"lock",
			"stackalloc",
			"else",
			"static",
			"enum",
			"namespace",
			"object",
			"bool",
			"byte",
			"float",
			"uint",
			"char",
			"ulong",
			"ushort",
			"decimal",
			"int",
			"sbyte",
			"short",
			"double",
			"long",
			"string",
			"void",
			"partial",
			"yield",
			"where"
		};
	}
}
