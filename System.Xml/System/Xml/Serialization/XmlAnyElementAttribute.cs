﻿using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Specifies that the member (a field that returns an array of <see cref="T:System.Xml.XmlElement" /> or <see cref="T:System.Xml.XmlNode" /> objects) contains objects that represent any XML element that has no corresponding member in the object being serialized or deserialized.</summary>
	// Token: 0x02000305 RID: 773
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue, AllowMultiple = true)]
	public class XmlAnyElementAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> class.</summary>
		// Token: 0x06001C78 RID: 7288 RVA: 0x000A478D File Offset: 0x000A298D
		public XmlAnyElementAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> class and specifies the XML element name generated in the XML document.</summary>
		/// <param name="name">The name of the XML element that the <see cref="T:System.Xml.Serialization.XmlSerializer" /> generates. </param>
		// Token: 0x06001C79 RID: 7289 RVA: 0x000A479C File Offset: 0x000A299C
		public XmlAnyElementAttribute(string name)
		{
			this.elementName = name;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> class and specifies the XML element name generated in the XML document and its XML namespace.</summary>
		/// <param name="name">The name of the XML element that the <see cref="T:System.Xml.Serialization.XmlSerializer" /> generates. </param>
		/// <param name="ns">The XML namespace of the XML element. </param>
		// Token: 0x06001C7A RID: 7290 RVA: 0x000A47B2 File Offset: 0x000A29B2
		public XmlAnyElementAttribute(string name, string ns)
		{
			this.elementName = name;
			this.ns = ns;
		}

		/// <summary>Gets or sets the XML element name.</summary>
		/// <returns>The name of the XML element.</returns>
		/// <exception cref="T:System.InvalidOperationException">The element name of an array member does not match the element name specified by the <see cref="P:System.Xml.Serialization.XmlAnyElementAttribute.Name" /> property. </exception>
		// Token: 0x1700056B RID: 1387
		// (get) Token: 0x06001C7B RID: 7291 RVA: 0x000A47CF File Offset: 0x000A29CF
		// (set) Token: 0x06001C7C RID: 7292 RVA: 0x000A47E5 File Offset: 0x000A29E5
		public string Name
		{
			get
			{
				if (this.elementName == null)
				{
					return string.Empty;
				}
				return this.elementName;
			}
			set
			{
				this.elementName = value;
			}
		}

		/// <summary>Gets or sets the XML namespace generated in the XML document.</summary>
		/// <returns>An XML namespace.</returns>
		// Token: 0x1700056C RID: 1388
		// (get) Token: 0x06001C7D RID: 7293 RVA: 0x000A47EE File Offset: 0x000A29EE
		// (set) Token: 0x06001C7E RID: 7294 RVA: 0x000A47F6 File Offset: 0x000A29F6
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.isNamespaceSpecified = true;
				this.ns = value;
			}
		}

		// Token: 0x1700056D RID: 1389
		// (get) Token: 0x06001C7F RID: 7295 RVA: 0x000A4806 File Offset: 0x000A2A06
		internal bool NamespaceSpecified
		{
			get
			{
				return this.isNamespaceSpecified;
			}
		}

		// Token: 0x1700056E RID: 1390
		// (get) Token: 0x06001C80 RID: 7296 RVA: 0x000A480E File Offset: 0x000A2A0E
		// (set) Token: 0x06001C81 RID: 7297 RVA: 0x000A4816 File Offset: 0x000A2A16
		[MonoTODO]
		internal bool IsNullableSpecified
		{
			[CompilerGenerated]
			get
			{
				return this.<IsNullableSpecified>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<IsNullableSpecified>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the explicit order in which the elements are serialized or deserialized.</summary>
		/// <returns>The order of the code generation.</returns>
		// Token: 0x1700056F RID: 1391
		// (get) Token: 0x06001C82 RID: 7298 RVA: 0x000A481F File Offset: 0x000A2A1F
		// (set) Token: 0x06001C83 RID: 7299 RVA: 0x000A4827 File Offset: 0x000A2A27
		public int Order
		{
			get
			{
				return this.order;
			}
			set
			{
				this.order = value;
			}
		}

		// Token: 0x06001C84 RID: 7300 RVA: 0x000A4830 File Offset: 0x000A2A30
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XAEA ");
			KeyHelper.AddField(sb, 1, this.ns);
			KeyHelper.AddField(sb, 2, this.elementName);
			sb.Append('|');
		}

		// Token: 0x04001643 RID: 5699
		private string elementName;

		// Token: 0x04001644 RID: 5700
		private string ns;

		// Token: 0x04001645 RID: 5701
		private bool isNamespaceSpecified;

		// Token: 0x04001646 RID: 5702
		private int order = -1;

		// Token: 0x04001647 RID: 5703
		[CompilerGenerated]
		private bool <IsNullableSpecified>k__BackingField;
	}
}
