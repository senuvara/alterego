﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002E2 RID: 738
	internal class Wsdl
	{
		// Token: 0x06001B45 RID: 6981 RVA: 0x00002103 File Offset: 0x00000303
		private Wsdl()
		{
		}

		// Token: 0x040015AF RID: 5551
		internal const string Namespace = "http://schemas.xmlsoap.org/wsdl/";

		// Token: 0x040015B0 RID: 5552
		internal const string ArrayType = "arrayType";
	}
}
