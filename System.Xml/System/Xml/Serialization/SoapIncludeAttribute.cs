﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Allows the <see cref="T:System.Xml.Serialization.XmlSerializer" /> to recognize a type when it serializes or deserializes an object as encoded SOAP XML.</summary>
	// Token: 0x020002FC RID: 764
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Interface, AllowMultiple = true)]
	public class SoapIncludeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.SoapIncludeAttribute" /> class using the specified type.</summary>
		/// <param name="type">The type of the object to include. </param>
		// Token: 0x06001C14 RID: 7188 RVA: 0x000A1CF5 File Offset: 0x0009FEF5
		public SoapIncludeAttribute(Type type)
		{
			this.type = type;
		}

		/// <summary>Gets or sets the type of the object to use when serializing or deserializing an object.</summary>
		/// <returns>The type of the object to include.</returns>
		// Token: 0x17000551 RID: 1361
		// (get) Token: 0x06001C15 RID: 7189 RVA: 0x000A1D04 File Offset: 0x0009FF04
		// (set) Token: 0x06001C16 RID: 7190 RVA: 0x000A1D0C File Offset: 0x0009FF0C
		public Type Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x04001620 RID: 5664
		private Type type;
	}
}
