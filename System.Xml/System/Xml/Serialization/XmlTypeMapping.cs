﻿using System;
using System.Collections;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Contains a mapping of one type to another.</summary>
	// Token: 0x0200034B RID: 843
	public class XmlTypeMapping : XmlMapping
	{
		// Token: 0x06002059 RID: 8281 RVA: 0x000B5CC0 File Offset: 0x000B3EC0
		internal XmlTypeMapping(string elementName, string ns, TypeData typeData, string xmlType, string xmlTypeNamespace)
		{
			this.isNullable = true;
			this._derivedTypes = new ArrayList();
			base..ctor(elementName, ns);
			this.type = typeData;
			this.xmlType = xmlType;
			this.xmlTypeNamespace = xmlTypeNamespace;
		}

		/// <summary>The fully qualified type name that includes the namespace (or namespaces) and type.</summary>
		/// <returns>The fully qualified type name.</returns>
		// Token: 0x17000627 RID: 1575
		// (get) Token: 0x0600205A RID: 8282 RVA: 0x000B5CF3 File Offset: 0x000B3EF3
		public string TypeFullName
		{
			get
			{
				return this.type.FullTypeName;
			}
		}

		/// <summary>Gets the type name of the mapped object.</summary>
		/// <returns>The type name of the mapped object.</returns>
		// Token: 0x17000628 RID: 1576
		// (get) Token: 0x0600205B RID: 8283 RVA: 0x000B5D00 File Offset: 0x000B3F00
		public string TypeName
		{
			get
			{
				return this.type.TypeName;
			}
		}

		/// <summary>Gets the XML element name of the mapped object.</summary>
		/// <returns>The XML element name of the mapped object. The default is the class name of the object.</returns>
		// Token: 0x17000629 RID: 1577
		// (get) Token: 0x0600205C RID: 8284 RVA: 0x000B5D0D File Offset: 0x000B3F0D
		public string XsdTypeName
		{
			get
			{
				return this.XmlType;
			}
		}

		/// <summary>Gets the XML namespace of the mapped object.</summary>
		/// <returns>The XML namespace of the mapped object. The default is an empty string ("").</returns>
		// Token: 0x1700062A RID: 1578
		// (get) Token: 0x0600205D RID: 8285 RVA: 0x000B5D15 File Offset: 0x000B3F15
		public string XsdTypeNamespace
		{
			get
			{
				return this.XmlTypeNamespace;
			}
		}

		// Token: 0x1700062B RID: 1579
		// (get) Token: 0x0600205E RID: 8286 RVA: 0x000B5D1D File Offset: 0x000B3F1D
		internal TypeData TypeData
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x1700062C RID: 1580
		// (get) Token: 0x0600205F RID: 8287 RVA: 0x000B5D25 File Offset: 0x000B3F25
		// (set) Token: 0x06002060 RID: 8288 RVA: 0x000B5D2D File Offset: 0x000B3F2D
		internal string XmlType
		{
			get
			{
				return this.xmlType;
			}
			set
			{
				this.xmlType = value;
			}
		}

		// Token: 0x1700062D RID: 1581
		// (get) Token: 0x06002061 RID: 8289 RVA: 0x000B5D36 File Offset: 0x000B3F36
		// (set) Token: 0x06002062 RID: 8290 RVA: 0x000B5D47 File Offset: 0x000B3F47
		internal string XmlTypeNamespace
		{
			get
			{
				return this.xmlTypeNamespace ?? string.Empty;
			}
			set
			{
				this.xmlTypeNamespace = value;
			}
		}

		// Token: 0x1700062E RID: 1582
		// (get) Token: 0x06002063 RID: 8291 RVA: 0x000B5D50 File Offset: 0x000B3F50
		internal bool HasXmlTypeNamespace
		{
			get
			{
				return this.xmlTypeNamespace != null;
			}
		}

		// Token: 0x1700062F RID: 1583
		// (get) Token: 0x06002064 RID: 8292 RVA: 0x000B5D5B File Offset: 0x000B3F5B
		// (set) Token: 0x06002065 RID: 8293 RVA: 0x000B5D63 File Offset: 0x000B3F63
		internal ArrayList DerivedTypes
		{
			get
			{
				return this._derivedTypes;
			}
			set
			{
				this._derivedTypes = value;
			}
		}

		// Token: 0x17000630 RID: 1584
		// (get) Token: 0x06002066 RID: 8294 RVA: 0x000B5D6C File Offset: 0x000B3F6C
		// (set) Token: 0x06002067 RID: 8295 RVA: 0x000B5D74 File Offset: 0x000B3F74
		internal bool MultiReferenceType
		{
			get
			{
				return this.multiReferenceType;
			}
			set
			{
				this.multiReferenceType = value;
			}
		}

		// Token: 0x17000631 RID: 1585
		// (get) Token: 0x06002068 RID: 8296 RVA: 0x000B5D7D File Offset: 0x000B3F7D
		// (set) Token: 0x06002069 RID: 8297 RVA: 0x000B5D85 File Offset: 0x000B3F85
		internal XmlTypeMapping BaseMap
		{
			get
			{
				return this.baseMap;
			}
			set
			{
				this.baseMap = value;
			}
		}

		// Token: 0x17000632 RID: 1586
		// (get) Token: 0x0600206A RID: 8298 RVA: 0x000B5D8E File Offset: 0x000B3F8E
		// (set) Token: 0x0600206B RID: 8299 RVA: 0x000B5D96 File Offset: 0x000B3F96
		internal bool IsSimpleType
		{
			get
			{
				return this.isSimpleType;
			}
			set
			{
				this.isSimpleType = value;
			}
		}

		// Token: 0x17000633 RID: 1587
		// (get) Token: 0x0600206D RID: 8301 RVA: 0x000B5DA8 File Offset: 0x000B3FA8
		// (set) Token: 0x0600206C RID: 8300 RVA: 0x000B5D9F File Offset: 0x000B3F9F
		internal string Documentation
		{
			get
			{
				return this.documentation;
			}
			set
			{
				this.documentation = value;
			}
		}

		// Token: 0x17000634 RID: 1588
		// (get) Token: 0x0600206E RID: 8302 RVA: 0x000B5DB0 File Offset: 0x000B3FB0
		// (set) Token: 0x0600206F RID: 8303 RVA: 0x000B5DB8 File Offset: 0x000B3FB8
		internal bool IncludeInSchema
		{
			get
			{
				return this.includeInSchema;
			}
			set
			{
				this.includeInSchema = value;
			}
		}

		// Token: 0x17000635 RID: 1589
		// (get) Token: 0x06002070 RID: 8304 RVA: 0x000B5DC1 File Offset: 0x000B3FC1
		// (set) Token: 0x06002071 RID: 8305 RVA: 0x000B5DC9 File Offset: 0x000B3FC9
		internal bool IsNullable
		{
			get
			{
				return this.isNullable;
			}
			set
			{
				this.isNullable = value;
			}
		}

		// Token: 0x17000636 RID: 1590
		// (get) Token: 0x06002072 RID: 8306 RVA: 0x000B5DD2 File Offset: 0x000B3FD2
		// (set) Token: 0x06002073 RID: 8307 RVA: 0x000B5DDA File Offset: 0x000B3FDA
		internal bool IsAny
		{
			get
			{
				return this.isAny;
			}
			set
			{
				this.isAny = value;
			}
		}

		// Token: 0x06002074 RID: 8308 RVA: 0x000B5DE4 File Offset: 0x000B3FE4
		internal XmlTypeMapping GetRealTypeMap(Type objectType)
		{
			if (this.TypeData.SchemaType == SchemaTypes.Enum)
			{
				return this;
			}
			if (this.TypeData.Type == objectType)
			{
				return this;
			}
			for (int i = 0; i < this._derivedTypes.Count; i++)
			{
				XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)this._derivedTypes[i];
				if (xmlTypeMapping.TypeData.Type == objectType)
				{
					return xmlTypeMapping;
				}
			}
			return null;
		}

		// Token: 0x06002075 RID: 8309 RVA: 0x000B5E54 File Offset: 0x000B4054
		internal XmlTypeMapping GetRealElementMap(string name, string ens)
		{
			if (this.xmlType == name && this.XmlTypeNamespace == ens)
			{
				return this;
			}
			foreach (object obj in this._derivedTypes)
			{
				XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)obj;
				if (xmlTypeMapping.xmlType == name && xmlTypeMapping.XmlTypeNamespace == ens)
				{
					return xmlTypeMapping;
				}
			}
			return null;
		}

		// Token: 0x06002076 RID: 8310 RVA: 0x000B5EE8 File Offset: 0x000B40E8
		internal void UpdateRoot(XmlQualifiedName qname)
		{
			if (qname != null)
			{
				this._elementName = qname.Name;
				this._namespace = qname.Namespace;
			}
		}

		// Token: 0x06002077 RID: 8311 RVA: 0x00072668 File Offset: 0x00070868
		internal XmlTypeMapping()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400175C RID: 5980
		private string xmlType;

		// Token: 0x0400175D RID: 5981
		private string xmlTypeNamespace;

		// Token: 0x0400175E RID: 5982
		private TypeData type;

		// Token: 0x0400175F RID: 5983
		private XmlTypeMapping baseMap;

		// Token: 0x04001760 RID: 5984
		private bool multiReferenceType;

		// Token: 0x04001761 RID: 5985
		private bool isSimpleType;

		// Token: 0x04001762 RID: 5986
		private string documentation;

		// Token: 0x04001763 RID: 5987
		private bool includeInSchema;

		// Token: 0x04001764 RID: 5988
		private bool isNullable;

		// Token: 0x04001765 RID: 5989
		private bool isAny;

		// Token: 0x04001766 RID: 5990
		private ArrayList _derivedTypes;
	}
}
