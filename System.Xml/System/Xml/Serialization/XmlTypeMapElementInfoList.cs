﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x02000341 RID: 833
	internal class XmlTypeMapElementInfoList : ArrayList
	{
		// Token: 0x0600200F RID: 8207 RVA: 0x000B52B4 File Offset: 0x000B34B4
		public int IndexOfElement(string name, string namspace)
		{
			for (int i = 0; i < this.Count; i++)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)base[i];
				if (xmlTypeMapElementInfo.ElementName == name && xmlTypeMapElementInfo.Namespace == namspace)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06002010 RID: 8208 RVA: 0x000B52FE File Offset: 0x000B34FE
		public XmlTypeMapElementInfoList()
		{
		}
	}
}
