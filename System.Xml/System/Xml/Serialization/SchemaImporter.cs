﻿using System;
using System.Security.Permissions;
using System.Xml.Serialization.Advanced;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Describes a schema importer.</summary>
	// Token: 0x0200056A RID: 1386
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public abstract class SchemaImporter
	{
		// Token: 0x06003448 RID: 13384 RVA: 0x00072668 File Offset: 0x00070868
		internal SchemaImporter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets a collection of schema importer extensions.</summary>
		/// <returns>A <see cref="T:System.Xml.Serialization.Configuration.SchemaImporterExtensionElementCollection" /> containing a collection of schema importer extensions.</returns>
		// Token: 0x17000AEA RID: 2794
		// (get) Token: 0x06003449 RID: 13385 RVA: 0x000A7058 File Offset: 0x000A5258
		public SchemaImporterExtensionCollection Extensions
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
