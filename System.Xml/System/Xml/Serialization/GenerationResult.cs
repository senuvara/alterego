﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002EB RID: 747
	internal class GenerationResult
	{
		// Token: 0x06001BD1 RID: 7121 RVA: 0x00002103 File Offset: 0x00000303
		public GenerationResult()
		{
		}

		// Token: 0x040015DA RID: 5594
		public XmlMapping Mapping;

		// Token: 0x040015DB RID: 5595
		public string ReaderClassName;

		// Token: 0x040015DC RID: 5596
		public string ReadMethodName;

		// Token: 0x040015DD RID: 5597
		public string WriterClassName;

		// Token: 0x040015DE RID: 5598
		public string WriteMethodName;

		// Token: 0x040015DF RID: 5599
		public string Namespace;

		// Token: 0x040015E0 RID: 5600
		public string SerializerClassName;

		// Token: 0x040015E1 RID: 5601
		public string BaseSerializerClassName;

		// Token: 0x040015E2 RID: 5602
		public string ImplementationClassName;
	}
}
