﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Controls the XML schema that is generated when the attribute target is serialized by the <see cref="T:System.Xml.Serialization.XmlSerializer" />.</summary>
	// Token: 0x0200033F RID: 831
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface)]
	public class XmlTypeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlTypeAttribute" /> class.</summary>
		// Token: 0x06001FE3 RID: 8163 RVA: 0x000B4F5B File Offset: 0x000B315B
		public XmlTypeAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlTypeAttribute" /> class and specifies the name of the XML type.</summary>
		/// <param name="typeName">The name of the XML type that the <see cref="T:System.Xml.Serialization.XmlSerializer" /> generates when it serializes the class instance (and recognizes when it deserializes the class instance). </param>
		// Token: 0x06001FE4 RID: 8164 RVA: 0x000B4F6A File Offset: 0x000B316A
		public XmlTypeAttribute(string typeName)
		{
			this.typeName = typeName;
		}

		/// <summary>Gets or sets a value that determines whether the resulting schema type is an XSD anonymous type.</summary>
		/// <returns>
		///     <see langword="true" />, if the resulting schema type is an XSD anonymous type; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005F4 RID: 1524
		// (get) Token: 0x06001FE5 RID: 8165 RVA: 0x000B4F80 File Offset: 0x000B3180
		// (set) Token: 0x06001FE6 RID: 8166 RVA: 0x000B4F88 File Offset: 0x000B3188
		public bool AnonymousType
		{
			get
			{
				return this.anonymousType;
			}
			set
			{
				this.anonymousType = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether to include the type in XML schema documents.</summary>
		/// <returns>
		///     <see langword="true" /> to include the type in XML schema documents; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005F5 RID: 1525
		// (get) Token: 0x06001FE7 RID: 8167 RVA: 0x000B4F91 File Offset: 0x000B3191
		// (set) Token: 0x06001FE8 RID: 8168 RVA: 0x000B4F99 File Offset: 0x000B3199
		public bool IncludeInSchema
		{
			get
			{
				return this.includeInSchema;
			}
			set
			{
				this.includeInSchema = value;
			}
		}

		/// <summary>Gets or sets the namespace of the XML type.</summary>
		/// <returns>The namespace of the XML type.</returns>
		// Token: 0x170005F6 RID: 1526
		// (get) Token: 0x06001FE9 RID: 8169 RVA: 0x000B4FA2 File Offset: 0x000B31A2
		// (set) Token: 0x06001FEA RID: 8170 RVA: 0x000B4FAA File Offset: 0x000B31AA
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		/// <summary>Gets or sets the name of the XML type.</summary>
		/// <returns>The name of the XML type.</returns>
		// Token: 0x170005F7 RID: 1527
		// (get) Token: 0x06001FEB RID: 8171 RVA: 0x000B4FB3 File Offset: 0x000B31B3
		// (set) Token: 0x06001FEC RID: 8172 RVA: 0x000B4FC9 File Offset: 0x000B31C9
		public string TypeName
		{
			get
			{
				if (this.typeName == null)
				{
					return string.Empty;
				}
				return this.typeName;
			}
			set
			{
				this.typeName = value;
			}
		}

		// Token: 0x06001FED RID: 8173 RVA: 0x000B4FD2 File Offset: 0x000B31D2
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XTA ");
			KeyHelper.AddField(sb, 1, this.ns);
			KeyHelper.AddField(sb, 2, this.typeName);
			KeyHelper.AddField(sb, 4, this.includeInSchema);
			sb.Append('|');
		}

		// Token: 0x04001735 RID: 5941
		private bool includeInSchema = true;

		// Token: 0x04001736 RID: 5942
		private string ns;

		// Token: 0x04001737 RID: 5943
		private string typeName;

		// Token: 0x04001738 RID: 5944
		private bool anonymousType;
	}
}
