﻿using System;
using System.Collections;
using System.Globalization;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x0200030E RID: 782
	internal class XmlCustomFormatter
	{
		// Token: 0x06001CF5 RID: 7413 RVA: 0x000A53EC File Offset: 0x000A35EC
		internal static string FromByteArrayBase64(byte[] value)
		{
			if (value != null)
			{
				return Convert.ToBase64String(value);
			}
			return string.Empty;
		}

		// Token: 0x06001CF6 RID: 7414 RVA: 0x000A5400 File Offset: 0x000A3600
		internal static string FromByteArrayHex(byte[] value)
		{
			if (value == null)
			{
				return null;
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (byte b in value)
			{
				stringBuilder.Append(b.ToString("X2", CultureInfo.InvariantCulture));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001CF7 RID: 7415 RVA: 0x000A544C File Offset: 0x000A364C
		internal static string FromChar(char value)
		{
			int num = (int)value;
			return num.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x06001CF8 RID: 7416 RVA: 0x000A5467 File Offset: 0x000A3667
		internal static string FromDate(DateTime value)
		{
			return XmlConvert.ToString(value, "yyyy-MM-dd");
		}

		// Token: 0x06001CF9 RID: 7417 RVA: 0x000A5474 File Offset: 0x000A3674
		internal static string FromDateTime(DateTime value)
		{
			return XmlConvert.ToString(value, XmlDateTimeSerializationMode.RoundtripKind);
		}

		// Token: 0x06001CFA RID: 7418 RVA: 0x000A547D File Offset: 0x000A367D
		internal static string FromTime(DateTime value)
		{
			return XmlConvert.ToString(value, "HH:mm:ss.fffffffzzz");
		}

		// Token: 0x06001CFB RID: 7419 RVA: 0x000A548A File Offset: 0x000A368A
		internal static string FromEnum(long value, string[] values, long[] ids)
		{
			return XmlCustomFormatter.FromEnum(value, values, ids, null);
		}

		// Token: 0x06001CFC RID: 7420 RVA: 0x000A5498 File Offset: 0x000A3698
		internal static string FromEnum(long value, string[] values, long[] ids, string typeName)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int num = ids.Length;
			long num2 = value;
			int num3 = -1;
			for (int i = 0; i < num; i++)
			{
				if (ids[i] == 0L)
				{
					num3 = i;
				}
				else
				{
					if (num2 == 0L)
					{
						break;
					}
					if ((ids[i] & value) == ids[i])
					{
						if (stringBuilder.Length != 0)
						{
							stringBuilder.Append(' ');
						}
						stringBuilder.Append(values[i]);
						num2 &= ~ids[i];
					}
				}
			}
			if (num2 == 0L)
			{
				if (stringBuilder.Length == 0 && num3 != -1)
				{
					stringBuilder.Append(values[num3]);
				}
				return stringBuilder.ToString();
			}
			if (typeName != null)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "'{0}' is not a valid value for {1}.", value, typeName));
			}
			throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "'{0}' is not a valid value.", value));
		}

		// Token: 0x06001CFD RID: 7421 RVA: 0x000A555A File Offset: 0x000A375A
		internal static string FromXmlName(string name)
		{
			return XmlConvert.EncodeName(name);
		}

		// Token: 0x06001CFE RID: 7422 RVA: 0x000A5562 File Offset: 0x000A3762
		internal static string FromXmlNCName(string ncName)
		{
			return XmlConvert.EncodeLocalName(ncName);
		}

		// Token: 0x06001CFF RID: 7423 RVA: 0x000A556A File Offset: 0x000A376A
		internal static string FromXmlNmToken(string nmToken)
		{
			return XmlConvert.EncodeNmToken(nmToken);
		}

		// Token: 0x06001D00 RID: 7424 RVA: 0x000A5574 File Offset: 0x000A3774
		internal static string FromXmlNmTokens(string nmTokens)
		{
			string[] array = nmTokens.Split(new char[]
			{
				' '
			});
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = XmlCustomFormatter.FromXmlNmToken(array[i]);
			}
			return string.Join(" ", array);
		}

		// Token: 0x06001D01 RID: 7425 RVA: 0x000A55B7 File Offset: 0x000A37B7
		internal static byte[] ToByteArrayBase64(string value)
		{
			return Convert.FromBase64String(value);
		}

		// Token: 0x06001D02 RID: 7426 RVA: 0x000A55BF File Offset: 0x000A37BF
		internal static char ToChar(string value)
		{
			return (char)XmlConvert.ToUInt16(value);
		}

		// Token: 0x06001D03 RID: 7427 RVA: 0x000A55C7 File Offset: 0x000A37C7
		internal static DateTime ToDate(string value)
		{
			return XmlCustomFormatter.ToDateTime(value);
		}

		// Token: 0x06001D04 RID: 7428 RVA: 0x000A55CF File Offset: 0x000A37CF
		internal static DateTime ToDateTime(string value)
		{
			return XmlConvert.ToDateTime(value, XmlDateTimeSerializationMode.RoundtripKind);
		}

		// Token: 0x06001D05 RID: 7429 RVA: 0x000A55C7 File Offset: 0x000A37C7
		internal static DateTime ToTime(string value)
		{
			return XmlCustomFormatter.ToDateTime(value);
		}

		// Token: 0x06001D06 RID: 7430 RVA: 0x000A55D8 File Offset: 0x000A37D8
		internal static long ToEnum(string value, Hashtable values, string typeName, bool validate)
		{
			long num = 0L;
			foreach (string text in value.Split(new char[]
			{
				' '
			}))
			{
				object obj = values[text];
				if (obj != null)
				{
					num |= (long)obj;
				}
				else if (validate && text.Length != 0)
				{
					throw new InvalidOperationException(string.Format("'{0}' is not a valid member of type {1}.", text, typeName));
				}
			}
			return num;
		}

		// Token: 0x06001D07 RID: 7431 RVA: 0x000A5643 File Offset: 0x000A3843
		internal static string ToXmlName(string value)
		{
			return XmlConvert.DecodeName(value);
		}

		// Token: 0x06001D08 RID: 7432 RVA: 0x000A564B File Offset: 0x000A384B
		internal static string ToXmlNCName(string value)
		{
			return XmlCustomFormatter.ToXmlName(value);
		}

		// Token: 0x06001D09 RID: 7433 RVA: 0x000A564B File Offset: 0x000A384B
		internal static string ToXmlNmToken(string value)
		{
			return XmlCustomFormatter.ToXmlName(value);
		}

		// Token: 0x06001D0A RID: 7434 RVA: 0x000A564B File Offset: 0x000A384B
		internal static string ToXmlNmTokens(string value)
		{
			return XmlCustomFormatter.ToXmlName(value);
		}

		// Token: 0x06001D0B RID: 7435 RVA: 0x000A5654 File Offset: 0x000A3854
		internal static string ToXmlString(TypeData type, object value)
		{
			if (value == null)
			{
				return null;
			}
			string xmlType = type.XmlType;
			uint num = <PrivateImplementationDetails>.ComputeStringHash(xmlType);
			if (num <= 1683620383U)
			{
				if (num <= 1042916651U)
				{
					if (num <= 735213196U)
					{
						if (num != 520654156U)
						{
							if (num != 735213196U)
							{
								goto IL_428;
							}
							if (!(xmlType == "unsignedByte"))
							{
								goto IL_428;
							}
							return XmlConvert.ToString((byte)value);
						}
						else
						{
							if (!(xmlType == "decimal"))
							{
								goto IL_428;
							}
							return XmlConvert.ToString((decimal)value);
						}
					}
					else if (num != 748388108U)
					{
						if (num != 799079693U)
						{
							if (num != 1042916651U)
							{
								goto IL_428;
							}
							if (!(xmlType == "hexBinary"))
							{
								goto IL_428;
							}
							if (value != null)
							{
								return XmlConvert.ToBinHexString((byte[])value);
							}
							return string.Empty;
						}
						else
						{
							if (!(xmlType == "duration"))
							{
								goto IL_428;
							}
							return (string)value;
						}
					}
					else
					{
						if (!(xmlType == "guid"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((Guid)value);
					}
				}
				else if (num <= 1412591975U)
				{
					if (num != 1293334960U)
					{
						if (num != 1412591975U)
						{
							goto IL_428;
						}
						if (!(xmlType == "unsignedInt"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((uint)value);
					}
					else
					{
						if (!(xmlType == "dateTime"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((DateTime)value, XmlDateTimeSerializationMode.RoundtripKind);
					}
				}
				else if (num != 1467875291U)
				{
					if (num != 1564253156U)
					{
						if (num != 1683620383U)
						{
							goto IL_428;
						}
						if (!(xmlType == "byte"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((sbyte)value);
					}
					else
					{
						if (!(xmlType == "time"))
						{
							goto IL_428;
						}
						return ((DateTime)value).ToString("HH:mm:ss.FFFFFFF", CultureInfo.InvariantCulture);
					}
				}
				else if (!(xmlType == "base64Binary"))
				{
					goto IL_428;
				}
			}
			else if (num <= 2699759368U)
			{
				if (num <= 2282863728U)
				{
					if (num != 1710517951U)
					{
						if (num != 2282863728U)
						{
							goto IL_428;
						}
						if (!(xmlType == "unsignedLong"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((ulong)value);
					}
					else
					{
						if (!(xmlType == "boolean"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((bool)value);
					}
				}
				else if (num != 2515107422U)
				{
					if (num != 2673764304U)
					{
						if (num != 2699759368U)
						{
							goto IL_428;
						}
						if (!(xmlType == "double"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((double)value);
					}
					else
					{
						if (!(xmlType == "unsignedShort"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((ushort)value);
					}
				}
				else
				{
					if (!(xmlType == "int"))
					{
						goto IL_428;
					}
					return XmlConvert.ToString((int)value);
				}
			}
			else if (num <= 3122818005U)
			{
				if (num != 2797886853U)
				{
					if (num != 2823553821U)
					{
						if (num != 3122818005U)
						{
							goto IL_428;
						}
						if (!(xmlType == "short"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((short)value);
					}
					else
					{
						if (!(xmlType == "char"))
						{
							goto IL_428;
						}
						return XmlConvert.ToString((int)((char)value));
					}
				}
				else
				{
					if (!(xmlType == "float"))
					{
						goto IL_428;
					}
					return XmlConvert.ToString((float)value);
				}
			}
			else if (num != 3270303571U)
			{
				if (num != 3564297305U)
				{
					if (num != 4031671994U)
					{
						goto IL_428;
					}
					if (!(xmlType == "base64"))
					{
						goto IL_428;
					}
				}
				else
				{
					if (!(xmlType == "date"))
					{
						goto IL_428;
					}
					return ((DateTime)value).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
				}
			}
			else
			{
				if (!(xmlType == "long"))
				{
					goto IL_428;
				}
				return XmlConvert.ToString((long)value);
			}
			if (value != null)
			{
				return Convert.ToBase64String((byte[])value);
			}
			return string.Empty;
			IL_428:
			if (!(value is IFormattable))
			{
				return value.ToString();
			}
			return ((IFormattable)value).ToString(null, CultureInfo.InvariantCulture);
		}

		// Token: 0x06001D0C RID: 7436 RVA: 0x000A5AAC File Offset: 0x000A3CAC
		internal static object FromXmlString(TypeData type, string value)
		{
			if (value == null)
			{
				return null;
			}
			string xmlType = type.XmlType;
			uint num = <PrivateImplementationDetails>.ComputeStringHash(xmlType);
			if (num <= 1683620383U)
			{
				if (num <= 1042916651U)
				{
					if (num <= 735213196U)
					{
						if (num != 520654156U)
						{
							if (num != 735213196U)
							{
								goto IL_405;
							}
							if (!(xmlType == "unsignedByte"))
							{
								goto IL_405;
							}
							return XmlConvert.ToByte(value);
						}
						else
						{
							if (!(xmlType == "decimal"))
							{
								goto IL_405;
							}
							return XmlConvert.ToDecimal(value);
						}
					}
					else if (num != 748388108U)
					{
						if (num != 799079693U)
						{
							if (num != 1042916651U)
							{
								goto IL_405;
							}
							if (!(xmlType == "hexBinary"))
							{
								goto IL_405;
							}
							return XmlConvert.FromBinHexString(value);
						}
						else
						{
							if (!(xmlType == "duration"))
							{
								goto IL_405;
							}
							return value;
						}
					}
					else
					{
						if (!(xmlType == "guid"))
						{
							goto IL_405;
						}
						return XmlConvert.ToGuid(value);
					}
				}
				else if (num <= 1412591975U)
				{
					if (num != 1293334960U)
					{
						if (num != 1412591975U)
						{
							goto IL_405;
						}
						if (!(xmlType == "unsignedInt"))
						{
							goto IL_405;
						}
						return XmlConvert.ToUInt32(value);
					}
					else
					{
						if (!(xmlType == "dateTime"))
						{
							goto IL_405;
						}
						return XmlConvert.ToDateTime(value, XmlDateTimeSerializationMode.RoundtripKind);
					}
				}
				else if (num != 1467875291U)
				{
					if (num != 1564253156U)
					{
						if (num != 1683620383U)
						{
							goto IL_405;
						}
						if (!(xmlType == "byte"))
						{
							goto IL_405;
						}
						return XmlConvert.ToSByte(value);
					}
					else
					{
						if (!(xmlType == "time"))
						{
							goto IL_405;
						}
						return DateTime.ParseExact(value, XmlCustomFormatter.allTimeFormats, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowLeadingWhite | DateTimeStyles.AllowTrailingWhite | DateTimeStyles.NoCurrentDateDefault | DateTimeStyles.RoundtripKind);
					}
				}
				else if (!(xmlType == "base64Binary"))
				{
					goto IL_405;
				}
			}
			else if (num <= 2699759368U)
			{
				if (num <= 2282863728U)
				{
					if (num != 1710517951U)
					{
						if (num != 2282863728U)
						{
							goto IL_405;
						}
						if (!(xmlType == "unsignedLong"))
						{
							goto IL_405;
						}
						return XmlConvert.ToUInt64(value);
					}
					else
					{
						if (!(xmlType == "boolean"))
						{
							goto IL_405;
						}
						return XmlConvert.ToBoolean(value);
					}
				}
				else if (num != 2515107422U)
				{
					if (num != 2673764304U)
					{
						if (num != 2699759368U)
						{
							goto IL_405;
						}
						if (!(xmlType == "double"))
						{
							goto IL_405;
						}
						return XmlConvert.ToDouble(value);
					}
					else
					{
						if (!(xmlType == "unsignedShort"))
						{
							goto IL_405;
						}
						return XmlConvert.ToUInt16(value);
					}
				}
				else
				{
					if (!(xmlType == "int"))
					{
						goto IL_405;
					}
					return XmlConvert.ToInt32(value);
				}
			}
			else if (num <= 3122818005U)
			{
				if (num != 2797886853U)
				{
					if (num != 2823553821U)
					{
						if (num != 3122818005U)
						{
							goto IL_405;
						}
						if (!(xmlType == "short"))
						{
							goto IL_405;
						}
						return XmlConvert.ToInt16(value);
					}
					else
					{
						if (!(xmlType == "char"))
						{
							goto IL_405;
						}
						return (char)XmlConvert.ToInt32(value);
					}
				}
				else
				{
					if (!(xmlType == "float"))
					{
						goto IL_405;
					}
					return XmlConvert.ToSingle(value);
				}
			}
			else if (num != 3270303571U)
			{
				if (num != 3564297305U)
				{
					if (num != 4031671994U)
					{
						goto IL_405;
					}
					if (!(xmlType == "base64"))
					{
						goto IL_405;
					}
				}
				else
				{
					if (!(xmlType == "date"))
					{
						goto IL_405;
					}
					return XmlConvert.ToDateTime(value).Date;
				}
			}
			else
			{
				if (!(xmlType == "long"))
				{
					goto IL_405;
				}
				return XmlConvert.ToInt64(value);
			}
			return Convert.FromBase64String(value);
			IL_405:
			if (type.Type != null)
			{
				return Convert.ChangeType(value, type.Type, null);
			}
			return value;
		}

		// Token: 0x06001D0D RID: 7437 RVA: 0x000A5EDC File Offset: 0x000A40DC
		internal static string GenerateToXmlString(TypeData type, string value)
		{
			if (type.NullableOverride)
			{
				return string.Concat(new string[]
				{
					"(",
					value,
					" != null ? ",
					XmlCustomFormatter.GenerateToXmlStringCore(type, value),
					" : null)"
				});
			}
			return XmlCustomFormatter.GenerateToXmlStringCore(type, value);
		}

		// Token: 0x06001D0E RID: 7438 RVA: 0x000A5F2C File Offset: 0x000A412C
		private static string GenerateToXmlStringCore(TypeData type, string value)
		{
			if (type.NullableOverride)
			{
				value += ".Value";
			}
			string xmlType = type.XmlType;
			uint num = <PrivateImplementationDetails>.ComputeStringHash(xmlType);
			if (num <= 1710517951U)
			{
				if (num <= 799079693U)
				{
					if (num <= 411976257U)
					{
						if (num <= 305428189U)
						{
							if (num != 266367750U)
							{
								if (num != 305428189U)
								{
									goto IL_608;
								}
								if (!(xmlType == "IDREF"))
								{
									goto IL_608;
								}
								return value;
							}
							else
							{
								if (!(xmlType == "Name"))
								{
									goto IL_608;
								}
								return value;
							}
						}
						else if (num != 398550328U)
						{
							if (num != 411976257U)
							{
								goto IL_608;
							}
							if (!(xmlType == "normalizedString"))
							{
								goto IL_608;
							}
							return value;
						}
						else
						{
							if (!(xmlType == "string"))
							{
								goto IL_608;
							}
							return value;
						}
					}
					else if (num <= 735213196U)
					{
						if (num != 520654156U)
						{
							if (num != 735213196U)
							{
								goto IL_608;
							}
							if (!(xmlType == "unsignedByte"))
							{
								goto IL_608;
							}
							return value + ".ToString(CultureInfo.InvariantCulture)";
						}
						else
						{
							if (!(xmlType == "decimal"))
							{
								goto IL_608;
							}
							return "XmlConvert.ToString (" + value + ")";
						}
					}
					else if (num != 748388108U)
					{
						if (num != 799079693U)
						{
							goto IL_608;
						}
						if (!(xmlType == "duration"))
						{
							goto IL_608;
						}
						return value;
					}
					else
					{
						if (!(xmlType == "guid"))
						{
							goto IL_608;
						}
						return "XmlConvert.ToString (" + value + ")";
					}
				}
				else if (num <= 1458105184U)
				{
					if (num <= 1293334960U)
					{
						if (num != 1042916651U)
						{
							if (num != 1293334960U)
							{
								goto IL_608;
							}
							if (!(xmlType == "dateTime"))
							{
								goto IL_608;
							}
							return "XmlConvert.ToString (" + value + ", XmlDateTimeSerializationMode.RoundtripKind)";
						}
						else
						{
							if (!(xmlType == "hexBinary"))
							{
								goto IL_608;
							}
							return value + " == null ? String.Empty : ToBinHexString (" + value + ")";
						}
					}
					else if (num != 1412591975U)
					{
						if (num != 1458105184U)
						{
							goto IL_608;
						}
						if (!(xmlType == "ID"))
						{
							goto IL_608;
						}
						return value;
					}
					else
					{
						if (!(xmlType == "unsignedInt"))
						{
							goto IL_608;
						}
						return value + ".ToString(CultureInfo.InvariantCulture)";
					}
				}
				else if (num <= 1564253156U)
				{
					if (num != 1467875291U)
					{
						if (num != 1564253156U)
						{
							goto IL_608;
						}
						if (!(xmlType == "time"))
						{
							goto IL_608;
						}
						return value + ".ToString(\"HH:mm:ss.FFFFFFF\", CultureInfo.InvariantCulture)";
					}
					else if (!(xmlType == "base64Binary"))
					{
						goto IL_608;
					}
				}
				else if (num != 1683620383U)
				{
					if (num != 1710517951U)
					{
						goto IL_608;
					}
					if (!(xmlType == "boolean"))
					{
						goto IL_608;
					}
					return "(" + value + "?\"true\":\"false\")";
				}
				else
				{
					if (!(xmlType == "byte"))
					{
						goto IL_608;
					}
					return value + ".ToString(CultureInfo.InvariantCulture)";
				}
			}
			else if (num <= 2823553821U)
			{
				if (num <= 2515107422U)
				{
					if (num <= 2362871194U)
					{
						if (num != 2282863728U)
						{
							if (num != 2362871194U)
							{
								goto IL_608;
							}
							if (!(xmlType == "ENTITY"))
							{
								goto IL_608;
							}
							return value;
						}
						else
						{
							if (!(xmlType == "unsignedLong"))
							{
								goto IL_608;
							}
							return value + ".ToString(CultureInfo.InvariantCulture)";
						}
					}
					else if (num != 2491017778U)
					{
						if (num != 2515107422U)
						{
							goto IL_608;
						}
						if (!(xmlType == "int"))
						{
							goto IL_608;
						}
						return value + ".ToString(CultureInfo.InvariantCulture)";
					}
					else
					{
						if (!(xmlType == "token"))
						{
							goto IL_608;
						}
						return value;
					}
				}
				else if (num <= 2699759368U)
				{
					if (num != 2673764304U)
					{
						if (num != 2699759368U)
						{
							goto IL_608;
						}
						if (!(xmlType == "double"))
						{
							goto IL_608;
						}
						return "XmlConvert.ToString (" + value + ")";
					}
					else
					{
						if (!(xmlType == "unsignedShort"))
						{
							goto IL_608;
						}
						return value + ".ToString(CultureInfo.InvariantCulture)";
					}
				}
				else if (num != 2797886853U)
				{
					if (num != 2823553821U)
					{
						goto IL_608;
					}
					if (!(xmlType == "char"))
					{
						goto IL_608;
					}
					return "((int)(" + value + ")).ToString(CultureInfo.InvariantCulture)";
				}
				else
				{
					if (!(xmlType == "float"))
					{
						goto IL_608;
					}
					return "XmlConvert.ToString (" + value + ")";
				}
			}
			else if (num <= 3490567765U)
			{
				if (num <= 3122818005U)
				{
					if (num != 3119462523U)
					{
						if (num != 3122818005U)
						{
							goto IL_608;
						}
						if (!(xmlType == "short"))
						{
							goto IL_608;
						}
						return value + ".ToString(CultureInfo.InvariantCulture)";
					}
					else
					{
						if (!(xmlType == "language"))
						{
							goto IL_608;
						}
						return value;
					}
				}
				else if (num != 3270303571U)
				{
					if (num != 3490567765U)
					{
						goto IL_608;
					}
					if (!(xmlType == "NCName"))
					{
						goto IL_608;
					}
					return value;
				}
				else
				{
					if (!(xmlType == "long"))
					{
						goto IL_608;
					}
					return value + ".ToString(CultureInfo.InvariantCulture)";
				}
			}
			else if (num <= 3564297305U)
			{
				if (num != 3534358459U)
				{
					if (num != 3564297305U)
					{
						goto IL_608;
					}
					if (!(xmlType == "date"))
					{
						goto IL_608;
					}
					return value + ".ToString(\"yyyy-MM-dd\", CultureInfo.InvariantCulture)";
				}
				else
				{
					if (!(xmlType == "NOTATION"))
					{
						goto IL_608;
					}
					return value;
				}
			}
			else if (num != 4031671994U)
			{
				if (num != 4263162417U)
				{
					goto IL_608;
				}
				if (!(xmlType == "NMTOKEN"))
				{
					goto IL_608;
				}
				return value;
			}
			else if (!(xmlType == "base64"))
			{
				goto IL_608;
			}
			return value + " == null ? String.Empty : Convert.ToBase64String (" + value + ")";
			IL_608:
			return string.Concat(new string[]
			{
				"((",
				value,
				" != null) ? (",
				value,
				").ToString() : null)"
			});
		}

		// Token: 0x06001D0F RID: 7439 RVA: 0x000A656C File Offset: 0x000A476C
		internal static string GenerateFromXmlString(TypeData type, string value)
		{
			if (type.NullableOverride)
			{
				return string.Concat(new string[]
				{
					"(",
					value,
					" != null ? (",
					type.CSharpName,
					"?)",
					XmlCustomFormatter.GenerateFromXmlStringCore(type, value),
					" : null)"
				});
			}
			return XmlCustomFormatter.GenerateFromXmlStringCore(type, value);
		}

		// Token: 0x06001D10 RID: 7440 RVA: 0x000A65CC File Offset: 0x000A47CC
		private static string GenerateFromXmlStringCore(TypeData type, string value)
		{
			string xmlType = type.XmlType;
			uint num = <PrivateImplementationDetails>.ComputeStringHash(xmlType);
			if (num <= 1683620383U)
			{
				if (num <= 1042916651U)
				{
					if (num <= 735213196U)
					{
						if (num != 520654156U)
						{
							if (num != 735213196U)
							{
								return value;
							}
							if (!(xmlType == "unsignedByte"))
							{
								return value;
							}
							return "byte.Parse (" + value + ", CultureInfo.InvariantCulture)";
						}
						else
						{
							if (!(xmlType == "decimal"))
							{
								return value;
							}
							return "Decimal.Parse (" + value + ", CultureInfo.InvariantCulture)";
						}
					}
					else if (num != 748388108U)
					{
						if (num != 799079693U)
						{
							if (num != 1042916651U)
							{
								return value;
							}
							if (!(xmlType == "hexBinary"))
							{
								return value;
							}
							return "FromBinHexString (" + value + ")";
						}
						else
						{
							if (!(xmlType == "duration"))
							{
								return value;
							}
							return value;
						}
					}
					else
					{
						if (!(xmlType == "guid"))
						{
							return value;
						}
						return "XmlConvert.ToGuid (" + value + ")";
					}
				}
				else if (num <= 1412591975U)
				{
					if (num != 1293334960U)
					{
						if (num != 1412591975U)
						{
							return value;
						}
						if (!(xmlType == "unsignedInt"))
						{
							return value;
						}
						return "UInt32.Parse (" + value + ", CultureInfo.InvariantCulture)";
					}
					else
					{
						if (!(xmlType == "dateTime"))
						{
							return value;
						}
						return "XmlConvert.ToDateTime (" + value + ", XmlDateTimeSerializationMode.RoundtripKind)";
					}
				}
				else if (num != 1467875291U)
				{
					if (num != 1564253156U)
					{
						if (num != 1683620383U)
						{
							return value;
						}
						if (!(xmlType == "byte"))
						{
							return value;
						}
						return "SByte.Parse (" + value + ", CultureInfo.InvariantCulture)";
					}
					else
					{
						if (!(xmlType == "time"))
						{
							return value;
						}
						return "DateTime.ParseExact (" + value + ", \"HH:mm:ss.FFFFFFF\", CultureInfo.InvariantCulture)";
					}
				}
				else if (!(xmlType == "base64Binary"))
				{
					return value;
				}
			}
			else if (num <= 2699759368U)
			{
				if (num <= 2282863728U)
				{
					if (num != 1710517951U)
					{
						if (num != 2282863728U)
						{
							return value;
						}
						if (!(xmlType == "unsignedLong"))
						{
							return value;
						}
						return "UInt64.Parse (" + value + ", CultureInfo.InvariantCulture)";
					}
					else
					{
						if (!(xmlType == "boolean"))
						{
							return value;
						}
						return "XmlConvert.ToBoolean (" + value + ")";
					}
				}
				else if (num != 2515107422U)
				{
					if (num != 2673764304U)
					{
						if (num != 2699759368U)
						{
							return value;
						}
						if (!(xmlType == "double"))
						{
							return value;
						}
						return "XmlConvert.ToDouble (" + value + ")";
					}
					else
					{
						if (!(xmlType == "unsignedShort"))
						{
							return value;
						}
						return "UInt16.Parse (" + value + ", CultureInfo.InvariantCulture)";
					}
				}
				else
				{
					if (!(xmlType == "int"))
					{
						return value;
					}
					return "Int32.Parse (" + value + ", CultureInfo.InvariantCulture)";
				}
			}
			else if (num <= 3122818005U)
			{
				if (num != 2797886853U)
				{
					if (num != 2823553821U)
					{
						if (num != 3122818005U)
						{
							return value;
						}
						if (!(xmlType == "short"))
						{
							return value;
						}
						return "Int16.Parse (" + value + ", CultureInfo.InvariantCulture)";
					}
					else
					{
						if (!(xmlType == "char"))
						{
							return value;
						}
						return "(char)Int32.Parse (" + value + ", CultureInfo.InvariantCulture)";
					}
				}
				else
				{
					if (!(xmlType == "float"))
					{
						return value;
					}
					return "XmlConvert.ToSingle (" + value + ")";
				}
			}
			else if (num != 3270303571U)
			{
				if (num != 3413635968U)
				{
					if (num != 3564297305U)
					{
						return value;
					}
					if (!(xmlType == "date"))
					{
						return value;
					}
					return "XmlConvert.ToDateTime (" + value + ").Date";
				}
				else if (!(xmlType == "base64:"))
				{
					return value;
				}
			}
			else
			{
				if (!(xmlType == "long"))
				{
					return value;
				}
				return "Int64.Parse (" + value + ", CultureInfo.InvariantCulture)";
			}
			return "Convert.FromBase64String (" + value + ")";
		}

		// Token: 0x06001D11 RID: 7441 RVA: 0x00002103 File Offset: 0x00000303
		public XmlCustomFormatter()
		{
		}

		// Token: 0x06001D12 RID: 7442 RVA: 0x000A6A2C File Offset: 0x000A4C2C
		// Note: this type is marked as 'beforefieldinit'.
		static XmlCustomFormatter()
		{
		}

		// Token: 0x0400166B RID: 5739
		private static string[] allTimeFormats = new string[]
		{
			"HH:mm:ss.fffffffzzzzzz",
			"HH:mm:ss",
			"HH:mm:ss.f",
			"HH:mm:ss.ff",
			"HH:mm:ss.fff",
			"HH:mm:ss.ffff",
			"HH:mm:ss.fffff",
			"HH:mm:ss.ffffff",
			"HH:mm:ss.fffffff",
			"HH:mm:ssZ",
			"HH:mm:ss.fZ",
			"HH:mm:ss.ffZ",
			"HH:mm:ss.fffZ",
			"HH:mm:ss.ffffZ",
			"HH:mm:ss.fffffZ",
			"HH:mm:ss.ffffffZ",
			"HH:mm:ss.fffffffZ",
			"HH:mm:sszzzzzz",
			"HH:mm:ss.fzzzzzz",
			"HH:mm:ss.ffzzzzzz",
			"HH:mm:ss.fffzzzzzz",
			"HH:mm:ss.ffffzzzzzz",
			"HH:mm:ss.fffffzzzzzz",
			"HH:mm:ss.ffffffzzzzzz"
		};
	}
}
