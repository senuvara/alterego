﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x020002F3 RID: 755
	internal class XmlTypeSerializationSource : SerializationSource
	{
		// Token: 0x06001BDD RID: 7133 RVA: 0x000A15D8 File Offset: 0x0009F7D8
		public XmlTypeSerializationSource(Type type, XmlRootAttribute root, XmlAttributeOverrides attributeOverrides, string namspace, Type[] includedTypes) : base(namspace, includedTypes)
		{
			if (attributeOverrides != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				attributeOverrides.AddKeyHash(stringBuilder);
				this.attributeOverridesHash = stringBuilder.ToString();
			}
			if (root != null)
			{
				StringBuilder stringBuilder2 = new StringBuilder();
				root.AddKeyHash(stringBuilder2);
				this.rootHash = stringBuilder2.ToString();
			}
			this.type = type;
		}

		// Token: 0x06001BDE RID: 7134 RVA: 0x000A1630 File Offset: 0x0009F830
		public override bool Equals(object o)
		{
			XmlTypeSerializationSource xmlTypeSerializationSource = o as XmlTypeSerializationSource;
			return xmlTypeSerializationSource != null && this.type.Equals(xmlTypeSerializationSource.type) && !(this.rootHash != xmlTypeSerializationSource.rootHash) && !(this.attributeOverridesHash != xmlTypeSerializationSource.attributeOverridesHash) && base.BaseEquals(xmlTypeSerializationSource);
		}

		// Token: 0x06001BDF RID: 7135 RVA: 0x000A168F File Offset: 0x0009F88F
		public override int GetHashCode()
		{
			return this.type.GetHashCode();
		}

		// Token: 0x04001609 RID: 5641
		private string attributeOverridesHash;

		// Token: 0x0400160A RID: 5642
		private Type type;

		// Token: 0x0400160B RID: 5643
		private string rootHash;
	}
}
