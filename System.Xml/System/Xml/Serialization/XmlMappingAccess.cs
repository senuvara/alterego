﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Specifies whether a mapping is read, write, or both.</summary>
	// Token: 0x02000318 RID: 792
	[Flags]
	public enum XmlMappingAccess
	{
		/// <summary>Both read and write methods are generated.</summary>
		// Token: 0x04001685 RID: 5765
		None = 0,
		/// <summary>Read methods are generated.</summary>
		// Token: 0x04001686 RID: 5766
		Read = 1,
		/// <summary>Write methods are generated.</summary>
		// Token: 0x04001687 RID: 5767
		Write = 2
	}
}
