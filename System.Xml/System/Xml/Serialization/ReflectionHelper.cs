﻿using System;
using System.Collections;
using System.Reflection;

namespace System.Xml.Serialization
{
	// Token: 0x020002E7 RID: 743
	internal class ReflectionHelper
	{
		// Token: 0x06001B58 RID: 7000 RVA: 0x00098F4C File Offset: 0x0009714C
		public void RegisterSchemaType(XmlTypeMapping map, string xmlType, string ns)
		{
			string key = xmlType + "/" + ns;
			if (!this._schemaTypes.ContainsKey(key))
			{
				this._schemaTypes.Add(key, map);
			}
		}

		// Token: 0x06001B59 RID: 7001 RVA: 0x00098F84 File Offset: 0x00097184
		public XmlTypeMapping GetRegisteredSchemaType(string xmlType, string ns)
		{
			string key = xmlType + "/" + ns;
			return this._schemaTypes[key] as XmlTypeMapping;
		}

		// Token: 0x06001B5A RID: 7002 RVA: 0x00098FB0 File Offset: 0x000971B0
		public void RegisterClrType(XmlTypeMapping map, Type type, string ns)
		{
			if (type == typeof(object))
			{
				ns = "";
			}
			string key = type.FullName + "/" + ns;
			if (!this._clrTypes.ContainsKey(key))
			{
				this._clrTypes.Add(key, map);
			}
		}

		// Token: 0x06001B5B RID: 7003 RVA: 0x00099004 File Offset: 0x00097204
		public XmlTypeMapping GetRegisteredClrType(Type type, string ns)
		{
			if (type == typeof(object))
			{
				ns = "";
			}
			string key = type.FullName + "/" + ns;
			return this._clrTypes[key] as XmlTypeMapping;
		}

		// Token: 0x06001B5C RID: 7004 RVA: 0x0009904D File Offset: 0x0009724D
		public Exception CreateError(XmlTypeMapping map, string message)
		{
			return new InvalidOperationException("There was an error reflecting '" + map.TypeFullName + "': " + message);
		}

		// Token: 0x06001B5D RID: 7005 RVA: 0x0009906C File Offset: 0x0009726C
		public static void CheckSerializableType(Type type, bool allowPrivateConstructors)
		{
			if (type.IsArray)
			{
				return;
			}
			if (!allowPrivateConstructors && type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Type.EmptyTypes, ReflectionHelper.empty_modifiers) == null && !type.IsAbstract && !type.IsValueType)
			{
				throw new InvalidOperationException(type.FullName + " cannot be serialized because it does not have a default public constructor");
			}
			if (type.IsInterface && !TypeTranslator.GetTypeData(type).IsListType)
			{
				throw new InvalidOperationException(type.FullName + " cannot be serialized because it is an interface");
			}
			Type type2 = type;
			while (type2.IsPublic || type2.IsNestedPublic)
			{
				Type right = type2;
				type2 = type2.DeclaringType;
				if (!(type2 != null) || !(type2 != right))
				{
					return;
				}
			}
			throw new InvalidOperationException(type.FullName + " is inaccessible due to its protection level. Only public types can be processed");
		}

		// Token: 0x06001B5E RID: 7006 RVA: 0x00099136 File Offset: 0x00097336
		public static string BuildMapKey(Type type)
		{
			return type.FullName + "::";
		}

		// Token: 0x06001B5F RID: 7007 RVA: 0x00099148 File Offset: 0x00097348
		public static string BuildMapKey(MethodInfo method, string tag)
		{
			string text = string.Concat(new string[]
			{
				method.DeclaringType.FullName,
				":",
				method.ReturnType.FullName,
				" ",
				method.Name,
				"("
			});
			ParameterInfo[] parameters = method.GetParameters();
			for (int i = 0; i < parameters.Length; i++)
			{
				if (i > 0)
				{
					text += ", ";
				}
				text += parameters[i].ParameterType.FullName;
			}
			text += ")";
			if (tag != null)
			{
				text = text + ":" + tag;
			}
			return text;
		}

		// Token: 0x06001B60 RID: 7008 RVA: 0x000991F4 File Offset: 0x000973F4
		public ReflectionHelper()
		{
		}

		// Token: 0x06001B61 RID: 7009 RVA: 0x00099212 File Offset: 0x00097412
		// Note: this type is marked as 'beforefieldinit'.
		static ReflectionHelper()
		{
		}

		// Token: 0x040015B7 RID: 5559
		private Hashtable _clrTypes = new Hashtable();

		// Token: 0x040015B8 RID: 5560
		private Hashtable _schemaTypes = new Hashtable();

		// Token: 0x040015B9 RID: 5561
		private static readonly ParameterModifier[] empty_modifiers = new ParameterModifier[0];
	}
}
