﻿using System;
using System.Collections;
using System.Globalization;

namespace System.Xml.Serialization
{
	// Token: 0x020002CD RID: 717
	internal class CaseInsensitiveKeyComparer : CaseInsensitiveComparer, IEqualityComparer
	{
		// Token: 0x06001A76 RID: 6774 RVA: 0x00093FEB File Offset: 0x000921EB
		public CaseInsensitiveKeyComparer() : base(CultureInfo.CurrentCulture)
		{
		}

		// Token: 0x06001A77 RID: 6775 RVA: 0x00093FF8 File Offset: 0x000921F8
		bool IEqualityComparer.Equals(object x, object y)
		{
			return base.Compare(x, y) == 0;
		}

		// Token: 0x06001A78 RID: 6776 RVA: 0x00094005 File Offset: 0x00092205
		int IEqualityComparer.GetHashCode(object obj)
		{
			string text = obj as string;
			if (text == null)
			{
				throw new ArgumentException(null, "obj");
			}
			return text.ToUpper(CultureInfo.CurrentCulture).GetHashCode();
		}
	}
}
