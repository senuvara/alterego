﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x020002E9 RID: 745
	internal class SerializationCodeGenerator
	{
		// Token: 0x06001B62 RID: 7010 RVA: 0x0009921F File Offset: 0x0009741F
		public SerializationCodeGenerator(XmlMapping[] xmlMaps) : this(xmlMaps, null)
		{
		}

		// Token: 0x06001B63 RID: 7011 RVA: 0x0009922C File Offset: 0x0009742C
		public SerializationCodeGenerator(XmlMapping[] xmlMaps, SerializerInfo config)
		{
			this._uniqueNames = new Hashtable();
			this._mapsToGenerate = new ArrayList();
			this._referencedTypes = new ArrayList();
			this.classNames = new CodeIdentifiers();
			this._listsToFill = new ArrayList();
			base..ctor();
			this._xmlMaps = xmlMaps;
			this._config = config;
		}

		// Token: 0x06001B64 RID: 7012 RVA: 0x00099284 File Offset: 0x00097484
		public SerializationCodeGenerator(XmlMapping xmlMap, SerializerInfo config)
		{
			this._uniqueNames = new Hashtable();
			this._mapsToGenerate = new ArrayList();
			this._referencedTypes = new ArrayList();
			this.classNames = new CodeIdentifiers();
			this._listsToFill = new ArrayList();
			base..ctor();
			this._xmlMaps = new XmlMapping[]
			{
				xmlMap
			};
			this._config = config;
		}

		// Token: 0x06001B65 RID: 7013 RVA: 0x000992E8 File Offset: 0x000974E8
		public static void Generate(string configFileName, string outputPath)
		{
			SerializationCodeGeneratorConfiguration serializationCodeGeneratorConfiguration = null;
			StreamReader streamReader = new StreamReader(configFileName);
			try
			{
				serializationCodeGeneratorConfiguration = (SerializationCodeGeneratorConfiguration)new XmlSerializer(new XmlReflectionImporter
				{
					AllowPrivateTypes = true
				}.ImportTypeMapping(typeof(SerializationCodeGeneratorConfiguration))).Deserialize(streamReader);
			}
			finally
			{
				streamReader.Close();
			}
			if (outputPath == null)
			{
				outputPath = "";
			}
			CodeIdentifiers codeIdentifiers = new CodeIdentifiers();
			if (serializationCodeGeneratorConfiguration.Serializers != null)
			{
				foreach (SerializerInfo serializerInfo in serializationCodeGeneratorConfiguration.Serializers)
				{
					Type type;
					if (serializerInfo.Assembly != null)
					{
						Assembly assembly;
						try
						{
							assembly = Assembly.Load(serializerInfo.Assembly);
						}
						catch
						{
							assembly = Assembly.LoadFrom(serializerInfo.Assembly);
						}
						type = assembly.GetType(serializerInfo.ClassName, true);
					}
					else
					{
						type = Type.GetType(serializerInfo.ClassName);
					}
					if (type == null)
					{
						throw new InvalidOperationException("Type " + serializerInfo.ClassName + " not found");
					}
					string text = serializerInfo.OutFileName;
					if (text == null || text.Length == 0)
					{
						int num = serializerInfo.ClassName.LastIndexOf(".");
						if (num != -1)
						{
							text = serializerInfo.ClassName.Substring(num + 1);
						}
						else
						{
							text = serializerInfo.ClassName;
						}
						text = codeIdentifiers.AddUnique(text, type) + "Serializer.cs";
					}
					StreamWriter streamWriter = new StreamWriter(Path.Combine(outputPath, text));
					try
					{
						XmlTypeMapping xmlMap;
						if (serializerInfo.SerializationFormat == SerializationFormat.Literal)
						{
							xmlMap = new XmlReflectionImporter().ImportTypeMapping(type);
						}
						else
						{
							xmlMap = new SoapReflectionImporter().ImportTypeMapping(type);
						}
						new SerializationCodeGenerator(xmlMap, serializerInfo).GenerateSerializers(streamWriter);
					}
					finally
					{
						streamWriter.Close();
					}
				}
			}
		}

		// Token: 0x06001B66 RID: 7014 RVA: 0x000994C0 File Offset: 0x000976C0
		public void GenerateSerializers(TextWriter writer)
		{
			this._writer = writer;
			this._results = new GenerationResult[this._xmlMaps.Length];
			this.WriteLine("// It is automatically generated");
			this.WriteLine("using System;");
			this.WriteLine("using System.Xml;");
			this.WriteLine("using System.Xml.Schema;");
			this.WriteLine("using System.Xml.Serialization;");
			this.WriteLine("using System.Text;");
			this.WriteLine("using System.Collections;");
			this.WriteLine("using System.Globalization;");
			if (this._config != null && this._config.NamespaceImports != null && this._config.NamespaceImports.Length != 0)
			{
				foreach (string str in this._config.NamespaceImports)
				{
					this.WriteLine("using " + str + ";");
				}
			}
			this.WriteLine("");
			string text = null;
			string text2 = null;
			string text3 = null;
			string text4 = null;
			string text5 = null;
			if (this._config != null)
			{
				text = this._config.ReaderClassName;
				text2 = this._config.WriterClassName;
				text3 = this._config.BaseSerializerClassName;
				text4 = this._config.ImplementationClassName;
				text5 = this._config.Namespace;
			}
			if (text == null || text.Length == 0)
			{
				text = "GeneratedReader";
			}
			if (text2 == null || text2.Length == 0)
			{
				text2 = "GeneratedWriter";
			}
			if (text3 == null || text3.Length == 0)
			{
				text3 = "BaseXmlSerializer";
			}
			if (text4 == null || text4.Length == 0)
			{
				text4 = "XmlSerializerContract";
			}
			text = this.GetUniqueClassName(text);
			text2 = this.GetUniqueClassName(text2);
			text3 = this.GetUniqueClassName(text3);
			text4 = this.GetUniqueClassName(text4);
			Hashtable hashtable = new Hashtable();
			Hashtable hashtable2 = new Hashtable();
			for (int j = 0; j < this._xmlMaps.Length; j++)
			{
				this._typeMap = this._xmlMaps[j];
				if (this._typeMap != null)
				{
					this._result = (hashtable2[this._typeMap] as GenerationResult);
					if (this._result != null)
					{
						this._results[j] = this._result;
					}
					else
					{
						this._result = new GenerationResult();
						this._results[j] = this._result;
						hashtable2[this._typeMap] = this._result;
						string str2;
						if (this._typeMap is XmlTypeMapping)
						{
							str2 = CodeIdentifier.MakeValid(((XmlTypeMapping)this._typeMap).TypeData.CSharpName);
						}
						else
						{
							str2 = ((XmlMembersMapping)this._typeMap).ElementName;
						}
						this._result.ReaderClassName = text;
						this._result.WriterClassName = text2;
						this._result.BaseSerializerClassName = text3;
						this._result.ImplementationClassName = text4;
						if (text5 == null || text5.Length == 0)
						{
							this._result.Namespace = "Mono.GeneratedSerializers." + this._typeMap.Format;
						}
						else
						{
							this._result.Namespace = text5;
						}
						this._result.WriteMethodName = this.GetUniqueName("rwo", this._typeMap, "WriteRoot_" + str2);
						this._result.ReadMethodName = this.GetUniqueName("rro", this._typeMap, "ReadRoot_" + str2);
						this._result.Mapping = this._typeMap;
						ArrayList arrayList = (ArrayList)hashtable[this._result.Namespace];
						if (arrayList == null)
						{
							arrayList = new ArrayList();
							hashtable[this._result.Namespace] = arrayList;
						}
						arrayList.Add(this._result);
					}
				}
			}
			foreach (object obj in hashtable)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				ArrayList arrayList2 = (ArrayList)dictionaryEntry.Value;
				this.WriteLine("namespace " + dictionaryEntry.Key);
				this.WriteLineInd("{");
				if (this._config == null || !this._config.NoReader)
				{
					this.GenerateReader(text, arrayList2);
				}
				this.WriteLine("");
				if (this._config == null || !this._config.NoWriter)
				{
					this.GenerateWriter(text2, arrayList2);
				}
				this.WriteLine("");
				this.GenerateContract(arrayList2);
				this.WriteLineUni("}");
				this.WriteLine("");
			}
		}

		// Token: 0x1700053F RID: 1343
		// (get) Token: 0x06001B67 RID: 7015 RVA: 0x0009994C File Offset: 0x00097B4C
		public GenerationResult[] GenerationResults
		{
			get
			{
				return this._results;
			}
		}

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x06001B68 RID: 7016 RVA: 0x00099954 File Offset: 0x00097B54
		public ArrayList ReferencedTypes
		{
			get
			{
				return this._referencedTypes;
			}
		}

		// Token: 0x06001B69 RID: 7017 RVA: 0x0009995C File Offset: 0x00097B5C
		private void UpdateGeneratedTypes(ArrayList list)
		{
			for (int i = 0; i < list.Count; i++)
			{
				XmlTypeMapping xmlTypeMapping = list[i] as XmlTypeMapping;
				if (xmlTypeMapping != null && !this._referencedTypes.Contains(xmlTypeMapping.TypeData.Type))
				{
					this._referencedTypes.Add(xmlTypeMapping.TypeData.Type);
				}
			}
		}

		// Token: 0x06001B6A RID: 7018 RVA: 0x000999B9 File Offset: 0x00097BB9
		private static string ToCSharpFullName(Type type)
		{
			return TypeData.ToCSharpName(type, true);
		}

		// Token: 0x06001B6B RID: 7019 RVA: 0x000999C4 File Offset: 0x00097BC4
		public void GenerateContract(ArrayList generatedMaps)
		{
			if (generatedMaps.Count == 0)
			{
				return;
			}
			GenerationResult generationResult = (GenerationResult)generatedMaps[0];
			string baseSerializerClassName = generationResult.BaseSerializerClassName;
			string text = (this._config == null || !this._config.GenerateAsInternal) ? "public" : "internal";
			this.WriteLine("");
			this.WriteLine(text + " class " + baseSerializerClassName + " : System.Xml.Serialization.XmlSerializer");
			this.WriteLineInd("{");
			this.WriteLineInd("protected override System.Xml.Serialization.XmlSerializationReader CreateReader () {");
			this.WriteLine("return new " + generationResult.ReaderClassName + " ();");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLineInd("protected override System.Xml.Serialization.XmlSerializationWriter CreateWriter () {");
			this.WriteLine("return new " + generationResult.WriterClassName + " ();");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLineInd("public override bool CanDeserialize (System.Xml.XmlReader xmlReader) {");
			this.WriteLine("return true;");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLine("");
			foreach (object obj in generatedMaps)
			{
				GenerationResult generationResult2 = (GenerationResult)obj;
				generationResult2.SerializerClassName = this.GetUniqueClassName(generationResult2.Mapping.ElementName + "Serializer");
				this.WriteLine(string.Concat(new string[]
				{
					text,
					" sealed class ",
					generationResult2.SerializerClassName,
					" : ",
					baseSerializerClassName
				}));
				this.WriteLineInd("{");
				this.WriteLineInd("protected override void Serialize (object obj, System.Xml.Serialization.XmlSerializationWriter writer) {");
				this.WriteLine(string.Concat(new string[]
				{
					"((",
					generationResult2.WriterClassName,
					")writer).",
					generationResult2.WriteMethodName,
					"(obj);"
				}));
				this.WriteLineUni("}");
				this.WriteLine("");
				this.WriteLineInd("protected override object Deserialize (System.Xml.Serialization.XmlSerializationReader reader) {");
				this.WriteLine(string.Concat(new string[]
				{
					"return ((",
					generationResult2.ReaderClassName,
					")reader).",
					generationResult2.ReadMethodName,
					"();"
				}));
				this.WriteLineUni("}");
				this.WriteLineUni("}");
				this.WriteLine("");
			}
			this.WriteLine("#if !TARGET_JVM");
			this.WriteLine(text + " class " + generationResult.ImplementationClassName + " : System.Xml.Serialization.XmlSerializerImplementation");
			this.WriteLineInd("{");
			this.WriteLine("System.Collections.Hashtable readMethods = null;");
			this.WriteLine("System.Collections.Hashtable writeMethods = null;");
			this.WriteLine("System.Collections.Hashtable typedSerializers = null;");
			this.WriteLine("");
			this.WriteLineInd("public override System.Xml.Serialization.XmlSerializationReader Reader {");
			this.WriteLineInd("get {");
			this.WriteLine("return new " + generationResult.ReaderClassName + "();");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLineInd("public override System.Xml.Serialization.XmlSerializationWriter Writer {");
			this.WriteLineInd("get {");
			this.WriteLine("return new " + generationResult.WriterClassName + "();");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLineInd("public override System.Collections.Hashtable ReadMethods {");
			this.WriteLineInd("get {");
			this.WriteLineInd("lock (this) {");
			this.WriteLineInd("if (readMethods == null) {");
			this.WriteLine("readMethods = new System.Collections.Hashtable ();");
			foreach (object obj2 in generatedMaps)
			{
				GenerationResult generationResult3 = (GenerationResult)obj2;
				this.WriteLine(string.Concat(new string[]
				{
					"readMethods.Add (@\"",
					generationResult3.Mapping.GetKey(),
					"\", @\"",
					generationResult3.ReadMethodName,
					"\");"
				}));
			}
			this.WriteLineUni("}");
			this.WriteLine("return readMethods;");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLineInd("public override System.Collections.Hashtable WriteMethods {");
			this.WriteLineInd("get {");
			this.WriteLineInd("lock (this) {");
			this.WriteLineInd("if (writeMethods == null) {");
			this.WriteLine("writeMethods = new System.Collections.Hashtable ();");
			foreach (object obj3 in generatedMaps)
			{
				GenerationResult generationResult4 = (GenerationResult)obj3;
				this.WriteLine(string.Concat(new string[]
				{
					"writeMethods.Add (@\"",
					generationResult4.Mapping.GetKey(),
					"\", @\"",
					generationResult4.WriteMethodName,
					"\");"
				}));
			}
			this.WriteLineUni("}");
			this.WriteLine("return writeMethods;");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLineInd("public override System.Collections.Hashtable TypedSerializers {");
			this.WriteLineInd("get {");
			this.WriteLineInd("lock (this) {");
			this.WriteLineInd("if (typedSerializers == null) {");
			this.WriteLine("typedSerializers = new System.Collections.Hashtable ();");
			foreach (object obj4 in generatedMaps)
			{
				GenerationResult generationResult5 = (GenerationResult)obj4;
				this.WriteLine(string.Concat(new string[]
				{
					"typedSerializers.Add (@\"",
					generationResult5.Mapping.GetKey(),
					"\", new ",
					generationResult5.SerializerClassName,
					"());"
				}));
			}
			this.WriteLineUni("}");
			this.WriteLine("return typedSerializers;");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLine("public override XmlSerializer GetSerializer (Type type)");
			this.WriteLineInd("{");
			this.WriteLine("switch (type.FullName) {");
			foreach (object obj5 in generatedMaps)
			{
				GenerationResult generationResult6 = (GenerationResult)obj5;
				if (generationResult6.Mapping is XmlTypeMapping)
				{
					this.WriteLineInd("case \"" + ((XmlTypeMapping)generationResult6.Mapping).TypeData.CSharpFullName + "\":");
					this.WriteLine("return (XmlSerializer) TypedSerializers [\"" + generationResult6.Mapping.GetKey() + "\"];");
					this.WriteLineUni("");
				}
			}
			this.WriteLine("}");
			this.WriteLine("return base.GetSerializer (type);");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLineInd("public override bool CanSerialize (System.Type type) {");
			foreach (object obj6 in generatedMaps)
			{
				GenerationResult generationResult7 = (GenerationResult)obj6;
				if (generationResult7.Mapping is XmlTypeMapping)
				{
					this.WriteLine("if (type == typeof(" + (generationResult7.Mapping as XmlTypeMapping).TypeData.CSharpFullName + ")) return true;");
				}
			}
			this.WriteLine("return false;");
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLine("#endif");
		}

		// Token: 0x06001B6C RID: 7020 RVA: 0x0009A248 File Offset: 0x00098448
		public void GenerateWriter(string writerClassName, ArrayList maps)
		{
			this._mapsToGenerate = new ArrayList();
			this.InitHooks();
			if (this._config == null || !this._config.GenerateAsInternal)
			{
				this.WriteLine("public class " + writerClassName + " : XmlSerializationWriter");
			}
			else
			{
				this.WriteLine("internal class " + writerClassName + " : XmlSerializationWriter");
			}
			this.WriteLineInd("{");
			this.WriteLine("const string xmlNamespace = \"http://www.w3.org/2000/xmlns/\";");
			this.WriteLine("static readonly System.Reflection.MethodInfo toBinHexStringMethod = typeof (XmlConvert).GetMethod (\"ToBinHexString\", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic, null, new Type [] {typeof (byte [])}, null);");
			this.WriteLine("static string ToBinHexString (byte [] input)");
			this.WriteLineInd("{");
			this.WriteLine("return input == null ? null : (string) toBinHexStringMethod.Invoke (null, new object [] {input});");
			this.WriteLineUni("}");
			for (int i = 0; i < maps.Count; i++)
			{
				GenerationResult generationResult = (GenerationResult)maps[i];
				this._typeMap = generationResult.Mapping;
				this._format = this._typeMap.Format;
				this._result = generationResult;
				this.GenerateWriteRoot();
			}
			for (int j = 0; j < this._mapsToGenerate.Count; j++)
			{
				XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)this._mapsToGenerate[j];
				this.GenerateWriteObject(xmlTypeMapping);
				if (xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Enum)
				{
					this.GenerateGetXmlEnumValue(xmlTypeMapping);
				}
			}
			this.GenerateWriteInitCallbacks();
			this.UpdateGeneratedTypes(this._mapsToGenerate);
			this.WriteLineUni("}");
		}

		// Token: 0x06001B6D RID: 7021 RVA: 0x0009A3A0 File Offset: 0x000985A0
		private void GenerateWriteRoot()
		{
			this.WriteLine("public void " + this._result.WriteMethodName + " (object o)");
			this.WriteLineInd("{");
			this.WriteLine("WriteStartDocument ();");
			if (this._typeMap is XmlTypeMapping)
			{
				this.WriteLine(this.GetRootTypeName() + " ob = (" + this.GetRootTypeName() + ") o;");
				XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)this._typeMap;
				if (xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Class || xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Array)
				{
					this.WriteLine("TopLevelElement ();");
				}
				if (this._format == SerializationFormat.Literal)
				{
					this.WriteLine(string.Concat(new string[]
					{
						this.GetWriteObjectName(xmlTypeMapping),
						" (ob, ",
						this.GetLiteral(xmlTypeMapping.ElementName),
						", ",
						this.GetLiteral(xmlTypeMapping.Namespace),
						", true, false, true);"
					}));
				}
				else
				{
					this.RegisterReferencingMap(xmlTypeMapping);
					this.WriteLine(string.Concat(new string[]
					{
						"WritePotentiallyReferencingElement (",
						this.GetLiteral(xmlTypeMapping.ElementName),
						", ",
						this.GetLiteral(xmlTypeMapping.Namespace),
						", ob, ",
						this.GetTypeOf(xmlTypeMapping.TypeData),
						", true, false);"
					}));
				}
			}
			else
			{
				if (!(this._typeMap is XmlMembersMapping))
				{
					throw new InvalidOperationException("Unknown type map");
				}
				this.WriteLine("object[] pars = (object[]) o;");
				this.GenerateWriteMessage((XmlMembersMapping)this._typeMap);
			}
			if (this._format == SerializationFormat.Encoded)
			{
				this.WriteLine("WriteReferencedElements ();");
			}
			this.WriteLineUni("}");
			this.WriteLine("");
		}

		// Token: 0x06001B6E RID: 7022 RVA: 0x0009A56C File Offset: 0x0009876C
		private void GenerateWriteMessage(XmlMembersMapping membersMap)
		{
			if (membersMap.HasWrapperElement)
			{
				this.WriteLine("TopLevelElement ();");
				this.WriteLine(string.Concat(new string[]
				{
					"WriteStartElement (",
					this.GetLiteral(membersMap.ElementName),
					", ",
					this.GetLiteral(membersMap.Namespace),
					", (",
					this.GetLiteral(this._format == SerializationFormat.Encoded),
					"));"
				}));
			}
			this.GenerateWriteObjectElement(membersMap, "pars", true);
			if (membersMap.HasWrapperElement)
			{
				this.WriteLine("WriteEndElement();");
			}
		}

		// Token: 0x06001B6F RID: 7023 RVA: 0x0009A614 File Offset: 0x00098814
		private void GenerateGetXmlEnumValue(XmlTypeMapping map)
		{
			EnumMap enumMap = (EnumMap)map.ObjectMap;
			string str = null;
			string text = null;
			if (enumMap.IsFlags)
			{
				str = this.GetUniqueName("gxe", map, "_xmlNames" + map.XmlType);
				this.Write("static readonly string[] " + str + " = { ");
				for (int i = 0; i < enumMap.XmlNames.Length; i++)
				{
					if (i > 0)
					{
						this._writer.Write(',');
					}
					this._writer.Write('"');
					this._writer.Write(enumMap.XmlNames[i]);
					this._writer.Write('"');
				}
				this._writer.WriteLine(" };");
				text = this.GetUniqueName("gve", map, "_values" + map.XmlType);
				this.Write("static readonly long[] " + text + " = { ");
				for (int j = 0; j < enumMap.Values.Length; j++)
				{
					if (j > 0)
					{
						this._writer.Write(',');
					}
					this._writer.Write(enumMap.Values[j].ToString(CultureInfo.InvariantCulture));
					this._writer.Write("L");
				}
				this._writer.WriteLine(" };");
				this.WriteLine(string.Empty);
			}
			this.WriteLine(string.Concat(new string[]
			{
				"string ",
				this.GetGetEnumValueName(map),
				" (",
				this.GetTypeFullName(map.TypeData),
				" val)"
			}));
			this.WriteLineInd("{");
			this.WriteLineInd("switch (val) {");
			for (int k = 0; k < enumMap.EnumNames.Length; k++)
			{
				this.WriteLine(string.Concat(new string[]
				{
					"case ",
					map.TypeData.CSharpFullName,
					".@",
					enumMap.EnumNames[k],
					": return ",
					this.GetLiteral(enumMap.XmlNames[k]),
					";"
				}));
			}
			if (enumMap.IsFlags)
			{
				this.WriteLineInd("default:");
				this.WriteLine("if (val.ToString () == \"0\") return string.Empty;");
				this.Write("return FromEnum ((long) val, " + str + ", " + text);
				this._writer.Write(", typeof (");
				this._writer.Write(map.TypeData.CSharpFullName);
				this._writer.Write(").FullName");
				this._writer.Write(')');
				this.WriteUni(";");
			}
			else
			{
				this.WriteLine("default: throw CreateInvalidEnumValueException ((long) val, typeof (" + map.TypeData.CSharpFullName + ").FullName);");
			}
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			this.WriteLine("");
		}

		// Token: 0x06001B70 RID: 7024 RVA: 0x0009A90C File Offset: 0x00098B0C
		private void GenerateWriteObject(XmlTypeMapping typeMap)
		{
			this.WriteLine(string.Concat(new string[]
			{
				"void ",
				this.GetWriteObjectName(typeMap),
				" (",
				this.GetTypeFullName(typeMap.TypeData),
				" ob, string element, string namesp, bool isNullable, bool needType, bool writeWrappingElem)"
			}));
			this.WriteLineInd("{");
			this.PushHookContext();
			this.SetHookVar("$TYPE", typeMap.TypeData.CSharpName);
			this.SetHookVar("$FULLTYPE", typeMap.TypeData.CSharpFullName);
			this.SetHookVar("$OBJECT", "ob");
			this.SetHookVar("$NULLABLE", "isNullable");
			if (this.GenerateWriteHook(HookType.type, typeMap.TypeData.Type))
			{
				this.WriteLineUni("}");
				this.WriteLine("");
				this.PopHookContext();
				return;
			}
			if (!typeMap.TypeData.IsValueType)
			{
				this.WriteLine("if (((object)ob) == null)");
				this.WriteLineInd("{");
				this.WriteLineInd("if (isNullable)");
				if (this._format == SerializationFormat.Literal)
				{
					this.WriteLine("WriteNullTagLiteral(element, namesp);");
				}
				else
				{
					this.WriteLine("WriteNullTagEncoded (element, namesp);");
				}
				this.WriteLineUni("return;");
				this.WriteLineUni("}");
				this.WriteLine("");
			}
			if (typeMap.TypeData.SchemaType == SchemaTypes.XmlNode)
			{
				if (this._format == SerializationFormat.Literal)
				{
					this.WriteLine("WriteElementLiteral (ob, \"\", \"\", true, " + this.GetLiteral(typeMap.IsAny) + ");");
				}
				else
				{
					this.WriteLine("WriteElementEncoded (ob, \"\", \"\", true, " + this.GetLiteral(typeMap.IsAny) + ");");
				}
				this.GenerateEndHook();
				this.WriteLineUni("}");
				this.WriteLine("");
				this.PopHookContext();
				return;
			}
			if (typeMap.TypeData.SchemaType == SchemaTypes.XmlSerializable)
			{
				this.WriteLine("WriteSerializable (ob, element, namesp, isNullable, " + this.GetLiteral(!typeMap.IsAny) + ");");
				this.GenerateEndHook();
				this.WriteLineUni("}");
				this.WriteLine("");
				this.PopHookContext();
				return;
			}
			ArrayList derivedTypes = typeMap.DerivedTypes;
			this.WriteLine("System.Type type = ob.GetType ();");
			this.WriteLine("if (type == typeof(" + typeMap.TypeData.CSharpFullName + "))");
			this.WriteLine("{ }");
			for (int i = 0; i < derivedTypes.Count; i++)
			{
				XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)derivedTypes[i];
				this.WriteLineInd("else if (type == typeof(" + xmlTypeMapping.TypeData.CSharpFullName + ")) { ");
				this.WriteLine(this.GetWriteObjectName(xmlTypeMapping) + "((" + xmlTypeMapping.TypeData.CSharpFullName + ")ob, element, namesp, isNullable, true, writeWrappingElem);");
				this.WriteLine("return;");
				this.WriteLineUni("}");
			}
			if (typeMap.TypeData.Type == typeof(object))
			{
				this.WriteLineInd("else {");
				this.WriteLineInd("if (ob.GetType().IsArray && typeof(XmlNode).IsAssignableFrom(ob.GetType().GetElementType())) {");
				this.WriteLine(string.Concat(new string[]
				{
					"Writer.WriteStartElement (",
					this.GetLiteral(typeMap.ElementName),
					", ",
					this.GetLiteral(typeMap.Namespace),
					");"
				}));
				this.WriteLineInd("foreach (XmlNode node in (System.Collections.IEnumerable) ob)");
				this.WriteLineUni("node.WriteTo (Writer);");
				this.WriteLineUni("Writer.WriteEndElement ();");
				this.WriteLine("}");
				this.WriteLineInd("else");
				this.WriteLineUni("WriteTypedPrimitive (element, namesp, ob, true);");
				this.WriteLine("return;");
				this.WriteLineUni("}");
			}
			else
			{
				this.WriteLineInd("else {");
				this.WriteLine("throw CreateUnknownTypeException (ob);");
				this.WriteLineUni("}");
				this.WriteLine("");
				this.WriteLineInd("if (writeWrappingElem) {");
				if (this._format == SerializationFormat.Encoded)
				{
					this.WriteLine("needType = true;");
				}
				this.WriteLine("WriteStartElement (element, namesp, ob);");
				this.WriteLineUni("}");
				this.WriteLine("");
				this.WriteLine(string.Concat(new string[]
				{
					"if (needType) WriteXsiType(",
					this.GetLiteral(typeMap.XmlType),
					", ",
					this.GetLiteral(typeMap.XmlTypeNamespace),
					");"
				}));
				this.WriteLine("");
				switch (typeMap.TypeData.SchemaType)
				{
				case SchemaTypes.Primitive:
					this.GenerateWritePrimitiveElement(typeMap, "ob");
					break;
				case SchemaTypes.Enum:
					this.GenerateWriteEnumElement(typeMap, "ob");
					break;
				case SchemaTypes.Array:
					this.GenerateWriteListElement(typeMap, "ob");
					break;
				case SchemaTypes.Class:
					this.GenerateWriteObjectElement(typeMap, "ob", false);
					break;
				}
				this.WriteLine("if (writeWrappingElem) WriteEndElement (ob);");
			}
			this.GenerateEndHook();
			this.WriteLineUni("}");
			this.WriteLine("");
			this.PopHookContext();
		}

		// Token: 0x06001B71 RID: 7025 RVA: 0x0009AE14 File Offset: 0x00099014
		private void GenerateWriteObjectElement(XmlMapping xmlMap, string ob, bool isValueList)
		{
			XmlTypeMapping xmlTypeMapping = xmlMap as XmlTypeMapping;
			Type type = (xmlTypeMapping != null) ? xmlTypeMapping.TypeData.Type : typeof(object[]);
			ClassMap classMap = (ClassMap)xmlMap.ObjectMap;
			if (!this.GenerateWriteHook(HookType.attributes, type))
			{
				if (classMap.NamespaceDeclarations != null)
				{
					this.WriteLine(string.Concat(new string[]
					{
						"WriteNamespaceDeclarations ((XmlSerializerNamespaces) ",
						ob,
						".@",
						classMap.NamespaceDeclarations.Name,
						");"
					}));
					this.WriteLine("");
				}
				XmlTypeMapMember defaultAnyAttributeMember = classMap.DefaultAnyAttributeMember;
				if (defaultAnyAttributeMember != null && !this.GenerateWriteMemberHook(type, defaultAnyAttributeMember))
				{
					string text = this.GenerateMemberHasValueCondition(defaultAnyAttributeMember, ob, isValueList);
					if (text != null)
					{
						this.WriteLineInd("if (" + text + ") {");
					}
					string obTempVar = this.GetObTempVar();
					this.WriteLine(string.Concat(new string[]
					{
						"ICollection ",
						obTempVar,
						" = ",
						this.GenerateGetMemberValue(defaultAnyAttributeMember, ob, isValueList),
						";"
					}));
					this.WriteLineInd("if (" + obTempVar + " != null) {");
					string obTempVar2 = this.GetObTempVar();
					this.WriteLineInd(string.Concat(new string[]
					{
						"foreach (XmlAttribute ",
						obTempVar2,
						" in ",
						obTempVar,
						")"
					}));
					this.WriteLineInd("if (" + obTempVar2 + ".NamespaceURI != xmlNamespace)");
					this.WriteLine(string.Concat(new string[]
					{
						"WriteXmlAttribute (",
						obTempVar2,
						", ",
						ob,
						");"
					}));
					this.Unindent();
					this.Unindent();
					this.WriteLineUni("}");
					if (text != null)
					{
						this.WriteLineUni("}");
					}
					this.WriteLine("");
					this.GenerateEndHook();
				}
				ICollection attributeMembers = classMap.AttributeMembers;
				if (attributeMembers != null)
				{
					foreach (object obj in attributeMembers)
					{
						XmlTypeMapMemberAttribute xmlTypeMapMemberAttribute = (XmlTypeMapMemberAttribute)obj;
						if (!this.GenerateWriteMemberHook(type, xmlTypeMapMemberAttribute))
						{
							string value = this.GenerateGetMemberValue(xmlTypeMapMemberAttribute, ob, isValueList);
							string text2 = this.GenerateMemberHasValueCondition(xmlTypeMapMemberAttribute, ob, isValueList);
							if (text2 != null)
							{
								this.WriteLineInd("if (" + text2 + ") {");
							}
							string text3 = this.GenerateGetStringValue(xmlTypeMapMemberAttribute.MappedType, xmlTypeMapMemberAttribute.TypeData, value, false);
							this.WriteLine(string.Concat(new string[]
							{
								"WriteAttribute (",
								this.GetLiteral(xmlTypeMapMemberAttribute.AttributeName),
								", ",
								this.GetLiteral(xmlTypeMapMemberAttribute.Namespace),
								", ",
								text3,
								");"
							}));
							if (text2 != null)
							{
								this.WriteLineUni("}");
							}
							this.GenerateEndHook();
						}
					}
					this.WriteLine("");
				}
				this.GenerateEndHook();
			}
			if (!this.GenerateWriteHook(HookType.elements, type))
			{
				ICollection elementMembers = classMap.ElementMembers;
				if (elementMembers != null)
				{
					foreach (object obj2 in elementMembers)
					{
						XmlTypeMapMemberElement xmlTypeMapMemberElement = (XmlTypeMapMemberElement)obj2;
						if (!this.GenerateWriteMemberHook(type, xmlTypeMapMemberElement))
						{
							string text4 = this.GenerateMemberHasValueCondition(xmlTypeMapMemberElement, ob, isValueList);
							if (text4 != null)
							{
								this.WriteLineInd("if (" + text4 + ") {");
							}
							string text5 = this.GenerateGetMemberValue(xmlTypeMapMemberElement, ob, isValueList);
							Type type2 = xmlTypeMapMemberElement.GetType();
							if (type2 == typeof(XmlTypeMapMemberList))
							{
								this.GenerateWriteMemberElement((XmlTypeMapElementInfo)xmlTypeMapMemberElement.ElementInfo[0], text5);
							}
							else if (type2 == typeof(XmlTypeMapMemberFlatList))
							{
								this.WriteLineInd("if (" + text5 + " != null) {");
								this.GenerateWriteListContent(ob, xmlTypeMapMemberElement.TypeData, ((XmlTypeMapMemberFlatList)xmlTypeMapMemberElement).ListMap, text5, false);
								this.WriteLineUni("}");
							}
							else if (type2 == typeof(XmlTypeMapMemberAnyElement))
							{
								this.WriteLineInd("if (" + text5 + " != null) {");
								this.GenerateWriteAnyElementContent((XmlTypeMapMemberAnyElement)xmlTypeMapMemberElement, text5);
								this.WriteLineUni("}");
							}
							else if (type2 == typeof(XmlTypeMapMemberAnyElement))
							{
								this.WriteLineInd("if (" + text5 + " != null) {");
								this.GenerateWriteAnyElementContent((XmlTypeMapMemberAnyElement)xmlTypeMapMemberElement, text5);
								this.WriteLineUni("}");
							}
							else if (!(type2 == typeof(XmlTypeMapMemberAnyAttribute)))
							{
								if (type2 == typeof(XmlTypeMapMemberElement))
								{
									if (xmlTypeMapMemberElement.ElementInfo.Count == 1)
									{
										XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)xmlTypeMapMemberElement.ElementInfo[0];
										this.GenerateWriteMemberElement(xmlTypeMapElementInfo, this.GetCast(xmlTypeMapElementInfo.TypeData, xmlTypeMapMemberElement.TypeData, text5));
										goto IL_6A7;
									}
									if (xmlTypeMapMemberElement.ChoiceMember != null)
									{
										string text6 = ob + ".@" + xmlTypeMapMemberElement.ChoiceMember;
										using (IEnumerator enumerator2 = xmlTypeMapMemberElement.ElementInfo.GetEnumerator())
										{
											while (enumerator2.MoveNext())
											{
												object obj3 = enumerator2.Current;
												XmlTypeMapElementInfo xmlTypeMapElementInfo2 = (XmlTypeMapElementInfo)obj3;
												this.WriteLineInd(string.Concat(new string[]
												{
													"if (",
													text6,
													" == ",
													this.GetLiteral(xmlTypeMapElementInfo2.ChoiceValue),
													") {"
												}));
												this.GenerateWriteMemberElement(xmlTypeMapElementInfo2, this.GetCast(xmlTypeMapElementInfo2.TypeData, xmlTypeMapMemberElement.TypeData, text5));
												this.WriteLineUni("}");
											}
											goto IL_6A7;
										}
									}
									bool flag = true;
									using (IEnumerator enumerator2 = xmlTypeMapMemberElement.ElementInfo.GetEnumerator())
									{
										while (enumerator2.MoveNext())
										{
											object obj4 = enumerator2.Current;
											XmlTypeMapElementInfo xmlTypeMapElementInfo3 = (XmlTypeMapElementInfo)obj4;
											this.WriteLineInd(string.Concat(new string[]
											{
												flag ? "" : "else ",
												"if (",
												text5,
												" is ",
												xmlTypeMapElementInfo3.TypeData.CSharpFullName,
												") {"
											}));
											this.GenerateWriteMemberElement(xmlTypeMapElementInfo3, this.GetCast(xmlTypeMapElementInfo3.TypeData, xmlTypeMapMemberElement.TypeData, text5));
											this.WriteLineUni("}");
											flag = false;
										}
										goto IL_6A7;
									}
								}
								throw new InvalidOperationException("Unknown member type");
							}
							IL_6A7:
							if (text4 != null)
							{
								this.WriteLineUni("}");
							}
							this.GenerateEndHook();
						}
					}
				}
				this.GenerateEndHook();
			}
		}

		// Token: 0x06001B72 RID: 7026 RVA: 0x0009B56C File Offset: 0x0009976C
		private void GenerateWriteMemberElement(XmlTypeMapElementInfo elem, string memberValue)
		{
			switch (elem.TypeData.SchemaType)
			{
			case SchemaTypes.Primitive:
			case SchemaTypes.Enum:
				if (this._format == SerializationFormat.Literal)
				{
					this.GenerateWritePrimitiveValueLiteral(memberValue, elem.ElementName, elem.Namespace, elem.MappedType, elem.TypeData, elem.WrappedElement, elem.IsNullable);
					return;
				}
				this.GenerateWritePrimitiveValueEncoded(memberValue, elem.ElementName, elem.Namespace, new XmlQualifiedName(elem.TypeData.XmlType, elem.DataTypeNamespace), elem.MappedType, elem.TypeData, elem.WrappedElement, elem.IsNullable);
				return;
			case SchemaTypes.Array:
				this.WriteLineInd("if (" + memberValue + " != null) {");
				if (elem.MappedType.MultiReferenceType)
				{
					this.WriteMetCall("WriteReferencingElement", new string[]
					{
						this.GetLiteral(elem.ElementName),
						this.GetLiteral(elem.Namespace),
						memberValue,
						this.GetLiteral(elem.IsNullable)
					});
					this.RegisterReferencingMap(elem.MappedType);
				}
				else
				{
					this.WriteMetCall("WriteStartElement", new string[]
					{
						this.GetLiteral(elem.ElementName),
						this.GetLiteral(elem.Namespace),
						memberValue
					});
					this.GenerateWriteListContent(null, elem.TypeData, (ListMap)elem.MappedType.ObjectMap, memberValue, false);
					this.WriteMetCall("WriteEndElement", new string[]
					{
						memberValue
					});
				}
				this.WriteLineUni("}");
				if (elem.IsNullable)
				{
					this.WriteLineInd("else");
					if (this._format == SerializationFormat.Literal)
					{
						this.WriteMetCall("WriteNullTagLiteral", new string[]
						{
							this.GetLiteral(elem.ElementName),
							this.GetLiteral(elem.Namespace)
						});
					}
					else
					{
						this.WriteMetCall("WriteNullTagEncoded", new string[]
						{
							this.GetLiteral(elem.ElementName),
							this.GetLiteral(elem.Namespace)
						});
					}
					this.Unindent();
					return;
				}
				return;
			case SchemaTypes.Class:
				if (!elem.MappedType.MultiReferenceType)
				{
					this.WriteMetCall(this.GetWriteObjectName(elem.MappedType), new string[]
					{
						memberValue,
						this.GetLiteral(elem.ElementName),
						this.GetLiteral(elem.Namespace),
						this.GetLiteral(elem.IsNullable),
						"false",
						"true"
					});
					return;
				}
				this.RegisterReferencingMap(elem.MappedType);
				if (elem.MappedType.TypeData.Type == typeof(object))
				{
					this.WriteMetCall("WritePotentiallyReferencingElement", new string[]
					{
						this.GetLiteral(elem.ElementName),
						this.GetLiteral(elem.Namespace),
						memberValue,
						"null",
						"false",
						this.GetLiteral(elem.IsNullable)
					});
					return;
				}
				this.WriteMetCall("WriteReferencingElement", new string[]
				{
					this.GetLiteral(elem.ElementName),
					this.GetLiteral(elem.Namespace),
					memberValue,
					this.GetLiteral(elem.IsNullable)
				});
				return;
			case SchemaTypes.XmlSerializable:
				this.WriteMetCall("WriteSerializable", new string[]
				{
					"(" + SerializationCodeGenerator.ToCSharpFullName(elem.MappedType.TypeData.Type) + ") " + memberValue,
					this.GetLiteral(elem.ElementName),
					this.GetLiteral(elem.Namespace),
					this.GetLiteral(elem.IsNullable)
				});
				return;
			case SchemaTypes.XmlNode:
			{
				string ob = elem.WrappedElement ? elem.ElementName : "";
				if (this._format == SerializationFormat.Literal)
				{
					this.WriteMetCall("WriteElementLiteral", new string[]
					{
						memberValue,
						this.GetLiteral(ob),
						this.GetLiteral(elem.Namespace),
						this.GetLiteral(elem.IsNullable),
						"false"
					});
					return;
				}
				this.WriteMetCall("WriteElementEncoded", new string[]
				{
					memberValue,
					this.GetLiteral(ob),
					this.GetLiteral(elem.Namespace),
					this.GetLiteral(elem.IsNullable),
					"false"
				});
				return;
			}
			default:
				throw new NotSupportedException("Invalid value type");
			}
		}

		// Token: 0x06001B73 RID: 7027 RVA: 0x0009B9FC File Offset: 0x00099BFC
		private void GenerateWriteListElement(XmlTypeMapping typeMap, string ob)
		{
			if (this._format == SerializationFormat.Encoded)
			{
				string itemCount = this.GenerateGetListCount(typeMap.TypeData, ob);
				string text;
				string text2;
				this.GenerateGetArrayType((ListMap)typeMap.ObjectMap, itemCount, out text, out text2);
				string text3;
				if (text2 != string.Empty)
				{
					text3 = string.Concat(new string[]
					{
						"FromXmlQualifiedName (new XmlQualifiedName(",
						text,
						",",
						text2,
						"))"
					});
				}
				else
				{
					text3 = this.GetLiteral(text);
				}
				this.WriteMetCall("WriteAttribute", new string[]
				{
					this.GetLiteral("arrayType"),
					this.GetLiteral("http://schemas.xmlsoap.org/soap/encoding/"),
					text3
				});
			}
			this.GenerateWriteListContent(null, typeMap.TypeData, (ListMap)typeMap.ObjectMap, ob, false);
		}

		// Token: 0x06001B74 RID: 7028 RVA: 0x0009BAC8 File Offset: 0x00099CC8
		private void GenerateWriteAnyElementContent(XmlTypeMapMemberAnyElement member, string memberValue)
		{
			bool flag = member.TypeData.Type == typeof(XmlElement) || member.TypeData.Type == typeof(XmlNode);
			string obTempVar = this.GetObTempVar();
			string text;
			if (flag)
			{
				text = memberValue;
			}
			else
			{
				text = this.GetObTempVar();
				this.WriteLineInd(string.Concat(new string[]
				{
					"foreach (object ",
					text,
					" in ",
					memberValue,
					") {"
				}));
			}
			this.WriteLine(string.Concat(new string[]
			{
				"XmlNode ",
				obTempVar,
				" = ",
				text,
				" as XmlNode;"
			}));
			this.WriteLine(string.Concat(new string[]
			{
				"if (",
				obTempVar,
				" == null && ",
				text,
				"!= null) throw new InvalidOperationException (\"A member with XmlAnyElementAttribute can only serialize and deserialize certain XmlNode types.\");"
			}));
			string obTempVar2 = this.GetObTempVar();
			this.WriteLine(string.Concat(new string[]
			{
				"XmlNode ",
				obTempVar2,
				" = ",
				obTempVar,
				";"
			}));
			this.WriteLineInd("if (" + obTempVar2 + " is XmlElement) {");
			if (!member.IsDefaultAny)
			{
				for (int i = 0; i < member.ElementInfo.Count; i++)
				{
					XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)member.ElementInfo[i];
					string text2 = string.Concat(new string[]
					{
						"(",
						obTempVar2,
						".LocalName == ",
						this.GetLiteral(xmlTypeMapElementInfo.ElementName),
						" && ",
						obTempVar2,
						".NamespaceURI == ",
						this.GetLiteral(xmlTypeMapElementInfo.Namespace),
						")"
					});
					if (i == member.ElementInfo.Count - 1)
					{
						text2 += ") {";
					}
					if (i == 0)
					{
						this.WriteLineInd("if (" + text2);
					}
					else
					{
						this.WriteLine("|| " + text2);
					}
				}
			}
			if (this._format == SerializationFormat.Literal)
			{
				this.WriteLine("WriteElementLiteral (" + obTempVar2 + ", \"\", \"\", false, true);");
			}
			else
			{
				this.WriteLine("WriteElementEncoded (" + obTempVar2 + ", \"\", \"\", false, true);");
			}
			if (!member.IsDefaultAny)
			{
				this.WriteLineUni("}");
				this.WriteLineInd("else");
				this.WriteLine(string.Concat(new string[]
				{
					"throw CreateUnknownAnyElementException (",
					obTempVar2,
					".Name, ",
					obTempVar2,
					".NamespaceURI);"
				}));
				this.Unindent();
			}
			this.WriteLineUni("}");
			this.WriteLine("else " + obTempVar2 + ".WriteTo (Writer);");
			if (!flag)
			{
				this.WriteLineUni("}");
			}
		}

		// Token: 0x06001B75 RID: 7029 RVA: 0x0009BDA8 File Offset: 0x00099FA8
		private void GenerateWritePrimitiveElement(XmlTypeMapping typeMap, string ob)
		{
			string str = this.GenerateGetStringValue(typeMap, typeMap.TypeData, ob, false);
			this.WriteLine("Writer.WriteString (" + str + ");");
		}

		// Token: 0x06001B76 RID: 7030 RVA: 0x0009BDDC File Offset: 0x00099FDC
		private void GenerateWriteEnumElement(XmlTypeMapping typeMap, string ob)
		{
			string str = this.GenerateGetEnumXmlValue(typeMap, ob);
			this.WriteLine("Writer.WriteString (" + str + ");");
		}

		// Token: 0x06001B77 RID: 7031 RVA: 0x0009BE08 File Offset: 0x0009A008
		private string GenerateGetStringValue(XmlTypeMapping typeMap, TypeData type, string value, bool isNullable)
		{
			if (type.SchemaType == SchemaTypes.Array)
			{
				string strTempVar = this.GetStrTempVar();
				this.WriteLine("string " + strTempVar + " = null;");
				this.WriteLineInd("if (" + value + " != null) {");
				string str = this.GenerateWriteListContent(null, typeMap.TypeData, (ListMap)typeMap.ObjectMap, value, true);
				this.WriteLine(strTempVar + " = " + str + ".ToString ().Trim ();");
				this.WriteLineUni("}");
				return strTempVar;
			}
			if (type.SchemaType == SchemaTypes.Enum)
			{
				if (isNullable)
				{
					return string.Concat(new string[]
					{
						"(",
						value,
						").HasValue ? ",
						this.GenerateGetEnumXmlValue(typeMap, "(" + value + ").Value"),
						" : null"
					});
				}
				return this.GenerateGetEnumXmlValue(typeMap, value);
			}
			else
			{
				if (type.Type == typeof(XmlQualifiedName))
				{
					return "FromXmlQualifiedName (" + value + ")";
				}
				if (value == null)
				{
					return null;
				}
				return XmlCustomFormatter.GenerateToXmlString(type, value);
			}
		}

		// Token: 0x06001B78 RID: 7032 RVA: 0x0009BF1C File Offset: 0x0009A11C
		private string GenerateGetEnumXmlValue(XmlTypeMapping typeMap, string ob)
		{
			return this.GetGetEnumValueName(typeMap) + " (" + ob + ")";
		}

		// Token: 0x06001B79 RID: 7033 RVA: 0x0009BF35 File Offset: 0x0009A135
		private string GenerateGetListCount(TypeData listType, string ob)
		{
			if (listType.Type.IsArray)
			{
				return "ob.Length";
			}
			return "ob.Count";
		}

		// Token: 0x06001B7A RID: 7034 RVA: 0x0009BF50 File Offset: 0x0009A150
		private void GenerateGetArrayType(ListMap map, string itemCount, out string localName, out string ns)
		{
			string str;
			if (itemCount != "")
			{
				str = "";
			}
			else
			{
				str = "[]";
			}
			XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)map.ItemInfo[0];
			if (xmlTypeMapElementInfo.TypeData.SchemaType == SchemaTypes.Array)
			{
				string str2;
				this.GenerateGetArrayType((ListMap)xmlTypeMapElementInfo.MappedType.ObjectMap, "", out str2, out ns);
				localName = str2 + str;
			}
			else if (xmlTypeMapElementInfo.MappedType != null)
			{
				localName = xmlTypeMapElementInfo.MappedType.XmlType + str;
				ns = xmlTypeMapElementInfo.MappedType.Namespace;
			}
			else
			{
				localName = xmlTypeMapElementInfo.TypeData.XmlType + str;
				ns = xmlTypeMapElementInfo.DataTypeNamespace;
			}
			if (itemCount != "")
			{
				localName = string.Concat(new string[]
				{
					"\"",
					localName,
					"[\" + ",
					itemCount,
					" + \"]\""
				});
				ns = this.GetLiteral(ns);
			}
		}

		// Token: 0x06001B7B RID: 7035 RVA: 0x0009C050 File Offset: 0x0009A250
		private string GenerateWriteListContent(string container, TypeData listType, ListMap map, string ob, bool writeToString)
		{
			string text = null;
			if (writeToString)
			{
				text = this.GetStrTempVar();
				this.WriteLine("System.Text.StringBuilder " + text + " = new System.Text.StringBuilder();");
			}
			if (listType.Type.IsArray)
			{
				string numTempVar = this.GetNumTempVar();
				this.WriteLineInd(string.Concat(new string[]
				{
					"for (int ",
					numTempVar,
					" = 0; ",
					numTempVar,
					" < ",
					ob,
					".Length; ",
					numTempVar,
					"++) {"
				}));
				this.GenerateListLoop(container, map, ob + "[" + numTempVar + "]", numTempVar, listType.ListItemTypeData, text);
				this.WriteLineUni("}");
			}
			else if (typeof(ICollection).IsAssignableFrom(listType.Type))
			{
				string numTempVar2 = this.GetNumTempVar();
				this.WriteLineInd(string.Concat(new string[]
				{
					"for (int ",
					numTempVar2,
					" = 0; ",
					numTempVar2,
					" < ",
					ob,
					".Count; ",
					numTempVar2,
					"++) {"
				}));
				this.GenerateListLoop(container, map, ob + "[" + numTempVar2 + "]", numTempVar2, listType.ListItemTypeData, text);
				this.WriteLineUni("}");
			}
			else
			{
				if (!typeof(IEnumerable).IsAssignableFrom(listType.Type))
				{
					throw new Exception("Unsupported collection type");
				}
				string obTempVar = this.GetObTempVar();
				this.WriteLineInd(string.Concat(new string[]
				{
					"foreach (",
					listType.ListItemTypeData.CSharpFullName,
					" ",
					obTempVar,
					" in ",
					ob,
					") {"
				}));
				this.GenerateListLoop(container, map, obTempVar, null, listType.ListItemTypeData, text);
				this.WriteLineUni("}");
			}
			return text;
		}

		// Token: 0x06001B7C RID: 7036 RVA: 0x0009C244 File Offset: 0x0009A444
		private void GenerateListLoop(string container, ListMap map, string item, string index, TypeData itemTypeData, string targetString)
		{
			bool flag = map.ItemInfo.Count > 1;
			if (map.ChoiceMember != null && container != null && index != null)
			{
				this.WriteLineInd(string.Concat(new string[]
				{
					"if ((",
					container,
					".@",
					map.ChoiceMember,
					" == null) || (",
					index,
					" >= ",
					container,
					".@",
					map.ChoiceMember,
					".Length))"
				}));
				this.WriteLine(string.Concat(new string[]
				{
					"throw CreateInvalidChoiceIdentifierValueException (",
					container,
					".GetType().ToString(), \"",
					map.ChoiceMember,
					"\");"
				}));
				this.Unindent();
			}
			if (flag)
			{
				this.WriteLine("if (((object)" + item + ") == null) { }");
			}
			foreach (object obj in map.ItemInfo)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
				if (map.ChoiceMember != null && flag)
				{
					this.WriteLineInd(string.Concat(new string[]
					{
						"else if (",
						container,
						".@",
						map.ChoiceMember,
						"[",
						index,
						"] == ",
						this.GetLiteral(xmlTypeMapElementInfo.ChoiceValue),
						") {"
					}));
				}
				else if (flag)
				{
					this.WriteLineInd(string.Concat(new string[]
					{
						"else if (",
						item,
						".GetType() == typeof(",
						xmlTypeMapElementInfo.TypeData.CSharpFullName,
						")) {"
					}));
				}
				if (targetString == null)
				{
					this.GenerateWriteMemberElement(xmlTypeMapElementInfo, this.GetCast(xmlTypeMapElementInfo.TypeData, itemTypeData, item));
				}
				else
				{
					string str = this.GenerateGetStringValue(xmlTypeMapElementInfo.MappedType, xmlTypeMapElementInfo.TypeData, this.GetCast(xmlTypeMapElementInfo.TypeData, itemTypeData, item), false);
					this.WriteLine(targetString + ".Append (" + str + ").Append (\" \");");
				}
				if (flag)
				{
					this.WriteLineUni("}");
				}
			}
			if (flag)
			{
				this.WriteLine("else throw CreateUnknownTypeException (" + item + ");");
			}
		}

		// Token: 0x06001B7D RID: 7037 RVA: 0x0009C4B0 File Offset: 0x0009A6B0
		private void GenerateWritePrimitiveValueLiteral(string memberValue, string name, string ns, XmlTypeMapping mappedType, TypeData typeData, bool wrapped, bool isNullable)
		{
			if (!wrapped)
			{
				string text = this.GenerateGetStringValue(mappedType, typeData, memberValue, false);
				this.WriteMetCall("WriteValue", new string[]
				{
					text
				});
				return;
			}
			if (isNullable)
			{
				if (typeData.Type == typeof(XmlQualifiedName))
				{
					this.WriteMetCall("WriteNullableQualifiedNameLiteral", new string[]
					{
						this.GetLiteral(name),
						this.GetLiteral(ns),
						memberValue
					});
					return;
				}
				string text2 = this.GenerateGetStringValue(mappedType, typeData, memberValue, true);
				this.WriteMetCall("WriteNullableStringLiteral", new string[]
				{
					this.GetLiteral(name),
					this.GetLiteral(ns),
					text2
				});
				return;
			}
			else
			{
				if (typeData.Type == typeof(XmlQualifiedName))
				{
					this.WriteMetCall("WriteElementQualifiedName", new string[]
					{
						this.GetLiteral(name),
						this.GetLiteral(ns),
						memberValue
					});
					return;
				}
				string text3 = this.GenerateGetStringValue(mappedType, typeData, memberValue, false);
				this.WriteMetCall("WriteElementString", new string[]
				{
					this.GetLiteral(name),
					this.GetLiteral(ns),
					text3
				});
				return;
			}
		}

		// Token: 0x06001B7E RID: 7038 RVA: 0x0009C5DC File Offset: 0x0009A7DC
		private void GenerateWritePrimitiveValueEncoded(string memberValue, string name, string ns, XmlQualifiedName xsiType, XmlTypeMapping mappedType, TypeData typeData, bool wrapped, bool isNullable)
		{
			if (!wrapped)
			{
				string text = this.GenerateGetStringValue(mappedType, typeData, memberValue, false);
				this.WriteMetCall("WriteValue", new string[]
				{
					text
				});
				return;
			}
			if (isNullable)
			{
				if (typeData.Type == typeof(XmlQualifiedName))
				{
					this.WriteMetCall("WriteNullableQualifiedNameEncoded", new string[]
					{
						this.GetLiteral(name),
						this.GetLiteral(ns),
						memberValue,
						this.GetLiteral(xsiType)
					});
					return;
				}
				string text2 = this.GenerateGetStringValue(mappedType, typeData, memberValue, true);
				this.WriteMetCall("WriteNullableStringEncoded", new string[]
				{
					this.GetLiteral(name),
					this.GetLiteral(ns),
					text2,
					this.GetLiteral(xsiType)
				});
				return;
			}
			else
			{
				if (typeData.Type == typeof(XmlQualifiedName))
				{
					this.WriteMetCall("WriteElementQualifiedName", new string[]
					{
						this.GetLiteral(name),
						this.GetLiteral(ns),
						memberValue,
						this.GetLiteral(xsiType)
					});
					return;
				}
				string text3 = this.GenerateGetStringValue(mappedType, typeData, memberValue, false);
				this.WriteMetCall("WriteElementString", new string[]
				{
					this.GetLiteral(name),
					this.GetLiteral(ns),
					text3,
					this.GetLiteral(xsiType)
				});
				return;
			}
		}

		// Token: 0x06001B7F RID: 7039 RVA: 0x0009C734 File Offset: 0x0009A934
		private string GenerateGetMemberValue(XmlTypeMapMember member, string ob, bool isValueList)
		{
			if (isValueList)
			{
				return this.GetCast(member.TypeData, TypeTranslator.GetTypeData(typeof(object)), string.Concat(new object[]
				{
					ob,
					"[",
					member.GlobalIndex,
					"]"
				}));
			}
			return ob + ".@" + member.Name;
		}

		// Token: 0x06001B80 RID: 7040 RVA: 0x0009C7A0 File Offset: 0x0009A9A0
		private string GenerateMemberHasValueCondition(XmlTypeMapMember member, string ob, bool isValueList)
		{
			if (isValueList)
			{
				if (member.IsOptionalValueType)
				{
					return string.Concat(new object[]
					{
						ob,
						".Length > ",
						Math.Max(member.GlobalIndex, member.SpecifiedGlobalIndex),
						" && ",
						this.GetCast(typeof(bool), string.Concat(new object[]
						{
							ob,
							"[",
							member.SpecifiedGlobalIndex,
							"]"
						}))
					});
				}
				return ob + ".Length > " + member.GlobalIndex;
			}
			else if (member.DefaultValue != DBNull.Value)
			{
				string str = ob + ".@" + member.Name;
				if (member.DefaultValue == null)
				{
					return str + " != null";
				}
				if (member.TypeData.SchemaType == SchemaTypes.Enum)
				{
					return str + " != " + this.GetCast(member.TypeData, this.GetLiteral(member.DefaultValue));
				}
				return str + " != " + this.GetLiteral(member.DefaultValue);
			}
			else
			{
				if (member.HasSpecified)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.AppendFormat("{0}.@{1}Specified", ob, member.Name);
					if (member.HasShouldSerialize)
					{
						stringBuilder.AppendFormat(" && {0}.@ShouldSerialize{1} ()", ob, member.Name);
					}
					return stringBuilder.ToString();
				}
				if (member.HasShouldSerialize)
				{
					return ob + ".@ShouldSerialize" + member.Name + " ()";
				}
				return null;
			}
		}

		// Token: 0x06001B81 RID: 7041 RVA: 0x0009C92C File Offset: 0x0009AB2C
		private void GenerateWriteInitCallbacks()
		{
			this.WriteLine("protected override void InitCallbacks ()");
			this.WriteLineInd("{");
			if (this._format == SerializationFormat.Encoded)
			{
				foreach (object obj in this._mapsToGenerate)
				{
					XmlTypeMapping xmlTypeMapping = ((XmlMapping)obj) as XmlTypeMapping;
					if (xmlTypeMapping != null)
					{
						this.WriteMetCall("AddWriteCallback", new string[]
						{
							this.GetTypeOf(xmlTypeMapping.TypeData),
							this.GetLiteral(xmlTypeMapping.XmlType),
							this.GetLiteral(xmlTypeMapping.Namespace),
							"new XmlSerializationWriteCallback (" + this.GetWriteObjectCallbackName(xmlTypeMapping) + ")"
						});
					}
				}
			}
			this.WriteLineUni("}");
			this.WriteLine("");
			if (this._format == SerializationFormat.Encoded)
			{
				foreach (object obj2 in this._mapsToGenerate)
				{
					XmlTypeMapping xmlTypeMapping2 = (XmlTypeMapping)obj2;
					if (xmlTypeMapping2 != null)
					{
						if (xmlTypeMapping2.TypeData.SchemaType == SchemaTypes.Enum)
						{
							this.WriteWriteEnumCallback(xmlTypeMapping2);
						}
						else
						{
							this.WriteWriteObjectCallback(xmlTypeMapping2);
						}
					}
				}
			}
		}

		// Token: 0x06001B82 RID: 7042 RVA: 0x0009CA80 File Offset: 0x0009AC80
		private void WriteWriteEnumCallback(XmlTypeMapping map)
		{
			this.WriteLine("void " + this.GetWriteObjectCallbackName(map) + " (object ob)");
			this.WriteLineInd("{");
			this.WriteMetCall(this.GetWriteObjectName(map), new string[]
			{
				this.GetCast(map.TypeData, "ob"),
				this.GetLiteral(map.ElementName),
				this.GetLiteral(map.Namespace),
				"false",
				"true",
				"false"
			});
			this.WriteLineUni("}");
			this.WriteLine("");
		}

		// Token: 0x06001B83 RID: 7043 RVA: 0x0009CB28 File Offset: 0x0009AD28
		private void WriteWriteObjectCallback(XmlTypeMapping map)
		{
			this.WriteLine("void " + this.GetWriteObjectCallbackName(map) + " (object ob)");
			this.WriteLineInd("{");
			this.WriteMetCall(this.GetWriteObjectName(map), new string[]
			{
				this.GetCast(map.TypeData, "ob"),
				this.GetLiteral(map.ElementName),
				this.GetLiteral(map.Namespace),
				"false",
				"false",
				"false"
			});
			this.WriteLineUni("}");
			this.WriteLine("");
		}

		// Token: 0x06001B84 RID: 7044 RVA: 0x0009CBD0 File Offset: 0x0009ADD0
		public void GenerateReader(string readerClassName, ArrayList maps)
		{
			if (this._config == null || !this._config.GenerateAsInternal)
			{
				this.WriteLine("public class " + readerClassName + " : XmlSerializationReader");
			}
			else
			{
				this.WriteLine("internal class " + readerClassName + " : XmlSerializationReader");
			}
			this.WriteLineInd("{");
			this.WriteLine("static readonly System.Reflection.MethodInfo fromBinHexStringMethod = typeof (XmlConvert).GetMethod (\"FromBinHexString\", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic, null, new Type [] {typeof (string)}, null);");
			this.WriteLine("static byte [] FromBinHexString (string input)");
			this.WriteLineInd("{");
			this.WriteLine("return input == null ? null : (byte []) fromBinHexStringMethod.Invoke (null, new object [] {input});");
			this.WriteLineUni("}");
			this._mapsToGenerate = new ArrayList();
			this._fixupCallbacks = new ArrayList();
			this.InitHooks();
			for (int i = 0; i < maps.Count; i++)
			{
				GenerationResult generationResult = (GenerationResult)maps[i];
				this._typeMap = generationResult.Mapping;
				this._format = this._typeMap.Format;
				this._result = generationResult;
				this.GenerateReadRoot();
			}
			for (int j = 0; j < this._mapsToGenerate.Count; j++)
			{
				XmlTypeMapping xmlTypeMapping = this._mapsToGenerate[j] as XmlTypeMapping;
				if (xmlTypeMapping != null)
				{
					this.GenerateReadObject(xmlTypeMapping);
					if (xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Enum)
					{
						this.GenerateGetEnumValueMethod(xmlTypeMapping);
					}
				}
			}
			this.GenerateReadInitCallbacks();
			if (this._format == SerializationFormat.Encoded)
			{
				this.GenerateFixupCallbacks();
				this.GenerateFillerCallbacks();
			}
			this.WriteLineUni("}");
			this.UpdateGeneratedTypes(this._mapsToGenerate);
		}

		// Token: 0x06001B85 RID: 7045 RVA: 0x0009CD40 File Offset: 0x0009AF40
		private void GenerateReadRoot()
		{
			this.WriteLine("public object " + this._result.ReadMethodName + " ()");
			this.WriteLineInd("{");
			this.WriteLine("Reader.MoveToContent();");
			if (this._typeMap is XmlTypeMapping)
			{
				XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)this._typeMap;
				if (this._format == SerializationFormat.Literal)
				{
					if (xmlTypeMapping.TypeData.SchemaType == SchemaTypes.XmlNode)
					{
						if (xmlTypeMapping.TypeData.Type == typeof(XmlDocument))
						{
							this.WriteLine("return ReadXmlDocument (false);");
						}
						else
						{
							this.WriteLine("return ReadXmlNode (false);");
						}
					}
					else
					{
						if (!xmlTypeMapping.IsAny)
						{
							this.WriteLineInd(string.Concat(new string[]
							{
								"if (Reader.LocalName != ",
								this.GetLiteral(xmlTypeMapping.ElementName),
								" || Reader.NamespaceURI != ",
								this.GetLiteral(xmlTypeMapping.Namespace),
								")"
							}));
							this.WriteLine("throw CreateUnknownNodeException();");
							this.Unindent();
						}
						this.WriteLine("return " + this.GetReadObjectCall(xmlTypeMapping, this.GetLiteral(xmlTypeMapping.IsNullable), "true") + ";");
					}
				}
				else
				{
					this.WriteLine("object ob = null;");
					this.WriteLine("Reader.MoveToContent();");
					this.WriteLine("if (Reader.NodeType == System.Xml.XmlNodeType.Element) ");
					this.WriteLineInd("{");
					if (!xmlTypeMapping.IsAny)
					{
						this.WriteLineInd(string.Concat(new string[]
						{
							"if (Reader.LocalName == ",
							this.GetLiteral(xmlTypeMapping.ElementName),
							" && Reader.NamespaceURI == ",
							this.GetLiteral(xmlTypeMapping.Namespace),
							")"
						}));
					}
					this.WriteLine("ob = ReadReferencedElement();");
					this.Unindent();
					if (!xmlTypeMapping.IsAny)
					{
						this.WriteLineInd("else ");
						this.WriteLine("throw CreateUnknownNodeException();");
						this.Unindent();
					}
					this.WriteLineUni("}");
					this.WriteLineInd("else ");
					this.WriteLine("UnknownNode(null);");
					this.Unindent();
					this.WriteLine("");
					this.WriteLine("ReadReferencedElements();");
					this.WriteLine("return ob;");
					this.RegisterReferencingMap(xmlTypeMapping);
				}
			}
			else
			{
				this.WriteLine("return " + this.GenerateReadMessage((XmlMembersMapping)this._typeMap) + ";");
			}
			this.WriteLineUni("}");
			this.WriteLine("");
		}

		// Token: 0x06001B86 RID: 7046 RVA: 0x0009CFCC File Offset: 0x0009B1CC
		private string GenerateReadMessage(XmlMembersMapping typeMap)
		{
			this.WriteLine("object[] parameters = new object[" + typeMap.Count + "];");
			this.WriteLine("");
			if (typeMap.HasWrapperElement)
			{
				if (this._format == SerializationFormat.Encoded)
				{
					this.WriteLine("while (Reader.NodeType == System.Xml.XmlNodeType.Element)");
					this.WriteLineInd("{");
					this.WriteLine("string root = Reader.GetAttribute (\"root\", " + this.GetLiteral("http://schemas.xmlsoap.org/soap/encoding/") + ");");
					this.WriteLine("if (root == null || System.Xml.XmlConvert.ToBoolean(root)) break;");
					this.WriteLine("ReadReferencedElement ();");
					this.WriteLine("Reader.MoveToContent ();");
					this.WriteLineUni("}");
					this.WriteLine("");
					this.WriteLine("if (Reader.NodeType != System.Xml.XmlNodeType.EndElement)");
					this.WriteLineInd("{");
					this.WriteLineInd("if (Reader.IsEmptyElement) {");
					this.WriteLine("Reader.Skip();");
					this.WriteLine("Reader.MoveToContent();");
					this.WriteLineUni("}");
					this.WriteLineInd("else {");
					this.WriteLine("Reader.ReadStartElement();");
					this.GenerateReadMembers(typeMap, (ClassMap)typeMap.ObjectMap, "parameters", true, false);
					this.WriteLine("ReadEndElement();");
					this.WriteLineUni("}");
					this.WriteLine("");
					this.WriteLine("Reader.MoveToContent();");
					this.WriteLineUni("}");
				}
				else
				{
					ArrayList allMembers = ((ClassMap)typeMap.ObjectMap).AllMembers;
					for (int i = 0; i < allMembers.Count; i++)
					{
						XmlTypeMapMember xmlTypeMapMember = (XmlTypeMapMember)allMembers[i];
						if (!xmlTypeMapMember.IsReturnValue && xmlTypeMapMember.TypeData.IsValueType)
						{
							this.GenerateSetMemberValueFromAttr(xmlTypeMapMember, "parameters", string.Format("({0}) Activator.CreateInstance(typeof({0}), true)", xmlTypeMapMember.TypeData.FullTypeName), true);
						}
					}
					this.WriteLine("while (Reader.NodeType != System.Xml.XmlNodeType.EndElement && Reader.ReadState == ReadState.Interactive)");
					this.WriteLineInd("{");
					this.WriteLine(string.Concat(new string[]
					{
						"if (Reader.IsStartElement(",
						this.GetLiteral(typeMap.ElementName),
						", ",
						this.GetLiteral(typeMap.Namespace),
						"))"
					}));
					this.WriteLineInd("{");
					bool flag = false;
					this.GenerateReadAttributeMembers(typeMap, (ClassMap)typeMap.ObjectMap, "parameters", true, ref flag);
					this.WriteLine("if (Reader.IsEmptyElement)");
					this.WriteLineInd("{");
					this.WriteLine("Reader.Skip(); Reader.MoveToContent(); continue;");
					this.WriteLineUni("}");
					this.WriteLine("Reader.ReadStartElement();");
					this.GenerateReadMembers(typeMap, (ClassMap)typeMap.ObjectMap, "parameters", true, false);
					this.WriteLine("ReadEndElement();");
					this.WriteLine("break;");
					this.WriteLineUni("}");
					this.WriteLineInd("else ");
					this.WriteLine("UnknownNode(null);");
					this.Unindent();
					this.WriteLine("");
					this.WriteLine("Reader.MoveToContent();");
					this.WriteLineUni("}");
				}
			}
			else
			{
				this.GenerateReadMembers(typeMap, (ClassMap)typeMap.ObjectMap, "parameters", true, this._format == SerializationFormat.Encoded);
			}
			if (this._format == SerializationFormat.Encoded)
			{
				this.WriteLine("ReadReferencedElements();");
			}
			return "parameters";
		}

		// Token: 0x06001B87 RID: 7047 RVA: 0x0009D308 File Offset: 0x0009B508
		private void GenerateReadObject(XmlTypeMapping typeMap)
		{
			string isNullable;
			if (this._format == SerializationFormat.Literal)
			{
				this.WriteLine(string.Concat(new string[]
				{
					"public ",
					this.GetTypeFullName(typeMap.TypeData),
					" ",
					this.GetReadObjectName(typeMap),
					" (bool isNullable, bool checkType)"
				}));
				isNullable = "isNullable";
			}
			else
			{
				this.WriteLine("public object " + this.GetReadObjectName(typeMap) + " ()");
				isNullable = "true";
			}
			this.WriteLineInd("{");
			this.PushHookContext();
			this.SetHookVar("$TYPE", typeMap.TypeData.CSharpName);
			this.SetHookVar("$FULLTYPE", typeMap.TypeData.CSharpFullName);
			this.SetHookVar("$NULLABLE", "isNullable");
			switch (typeMap.TypeData.SchemaType)
			{
			case SchemaTypes.Primitive:
				this.GenerateReadPrimitiveElement(typeMap, isNullable);
				break;
			case SchemaTypes.Enum:
				this.GenerateReadEnumElement(typeMap, isNullable);
				break;
			case SchemaTypes.Array:
			{
				string text = this.GenerateReadListElement(typeMap, null, isNullable, true);
				if (text != null)
				{
					this.WriteLine("return " + text + ";");
				}
				break;
			}
			case SchemaTypes.Class:
				this.GenerateReadClassInstance(typeMap, isNullable, "checkType");
				break;
			case SchemaTypes.XmlSerializable:
				this.GenerateReadXmlSerializableElement(typeMap, isNullable);
				break;
			case SchemaTypes.XmlNode:
				this.GenerateReadXmlNodeElement(typeMap, isNullable);
				break;
			default:
				throw new Exception("Unsupported map type");
			}
			this.WriteLineUni("}");
			this.WriteLine("");
			this.PopHookContext();
		}

		// Token: 0x06001B88 RID: 7048 RVA: 0x0009D48C File Offset: 0x0009B68C
		private void GenerateReadClassInstance(XmlTypeMapping typeMap, string isNullable, string checkType)
		{
			this.SetHookVar("$OBJECT", "ob");
			if (!typeMap.TypeData.IsValueType)
			{
				this.WriteLine(typeMap.TypeData.CSharpFullName + " ob = null;");
				if (this.GenerateReadHook(HookType.type, typeMap.TypeData.Type))
				{
					this.WriteLine("return ob;");
					return;
				}
				if (this._format == SerializationFormat.Literal)
				{
					this.WriteLine("if (" + isNullable + " && ReadNull()) return null;");
					this.WriteLine("");
					this.WriteLine("if (checkType) ");
					this.WriteLineInd("{");
				}
				else
				{
					this.WriteLine("if (ReadNull()) return null;");
					this.WriteLine("");
				}
			}
			else
			{
				this.WriteLine(typeMap.TypeData.CSharpFullName + string.Format(" ob = ({0}) Activator.CreateInstance(typeof({0}), true);", typeMap.TypeData.CSharpFullName));
				if (this.GenerateReadHook(HookType.type, typeMap.TypeData.Type))
				{
					this.WriteLine("return ob;");
					return;
				}
			}
			this.WriteLine("System.Xml.XmlQualifiedName t = GetXsiType();");
			this.WriteLine("if (t == null)");
			if (typeMap.TypeData.Type != typeof(object))
			{
				this.WriteLine("{ }");
			}
			else
			{
				this.WriteLine("\treturn " + this.GetCast(typeMap.TypeData, "ReadTypedPrimitive (new System.Xml.XmlQualifiedName(\"anyType\", System.Xml.Schema.XmlSchema.Namespace))") + ";");
			}
			foreach (object obj in typeMap.DerivedTypes)
			{
				XmlTypeMapping xmlTypeMapping = (XmlTypeMapping)obj;
				this.WriteLineInd(string.Concat(new string[]
				{
					"else if (t.Name == ",
					this.GetLiteral(xmlTypeMapping.XmlType),
					" && t.Namespace == ",
					this.GetLiteral(xmlTypeMapping.XmlTypeNamespace),
					")"
				}));
				this.WriteLine("return " + this.GetReadObjectCall(xmlTypeMapping, isNullable, checkType) + ";");
				this.Unindent();
			}
			this.WriteLine(string.Concat(new string[]
			{
				"else if (t.Name != ",
				this.GetLiteral(typeMap.XmlType),
				" || t.Namespace != ",
				this.GetLiteral(typeMap.XmlTypeNamespace),
				")"
			}));
			if (typeMap.TypeData.Type == typeof(object))
			{
				this.WriteLine("\treturn " + this.GetCast(typeMap.TypeData, "ReadTypedPrimitive (t)") + ";");
			}
			else
			{
				this.WriteLine("\tthrow CreateUnknownTypeException(t);");
			}
			if (!typeMap.TypeData.IsValueType)
			{
				if (this._format == SerializationFormat.Literal)
				{
					this.WriteLineUni("}");
				}
				if (typeMap.TypeData.Type.IsAbstract)
				{
					this.GenerateEndHook();
					this.WriteLine("return ob;");
					return;
				}
				this.WriteLine("");
				this.WriteLine(string.Format("ob = ({0}) Activator.CreateInstance(typeof({0}), true);", typeMap.TypeData.CSharpFullName));
			}
			this.WriteLine("");
			this.WriteLine("Reader.MoveToElement();");
			this.WriteLine("");
			this.GenerateReadMembers(typeMap, (ClassMap)typeMap.ObjectMap, "ob", false, false);
			this.WriteLine("");
			this.GenerateEndHook();
			this.WriteLine("return ob;");
		}

		// Token: 0x06001B89 RID: 7049 RVA: 0x0009D808 File Offset: 0x0009BA08
		private void GenerateReadMembers(XmlMapping xmlMap, ClassMap map, string ob, bool isValueList, bool readBySoapOrder)
		{
			XmlTypeMapping xmlTypeMapping = xmlMap as XmlTypeMapping;
			Type type = (xmlTypeMapping != null) ? xmlTypeMapping.TypeData.Type : typeof(object[]);
			bool flag = false;
			this.GenerateReadAttributeMembers(xmlMap, map, ob, isValueList, ref flag);
			if (!isValueList)
			{
				this.WriteLine("Reader.MoveToElement();");
				this.WriteLineInd("if (Reader.IsEmptyElement) {");
				this.WriteLine("Reader.Skip ();");
				this.GenerateSetListMembersDefaults(xmlTypeMapping, map, ob, isValueList);
				this.WriteLine("return " + ob + ";");
				this.WriteLineUni("}");
				this.WriteLine("");
				this.WriteLine("Reader.ReadStartElement();");
			}
			this.WriteLine("Reader.MoveToContent();");
			this.WriteLine("");
			if (!this.GenerateReadHook(HookType.elements, type))
			{
				string[] array = null;
				if (map.ElementMembers != null && !readBySoapOrder)
				{
					string text = string.Empty;
					array = new string[map.ElementMembers.Count];
					int num = 0;
					foreach (object obj in map.ElementMembers)
					{
						XmlTypeMapMember xmlTypeMapMember = (XmlTypeMapMember)obj;
						if (!(xmlTypeMapMember is XmlTypeMapMemberElement) || !((XmlTypeMapMemberElement)xmlTypeMapMember).IsXmlTextCollector)
						{
							array[num] = this.GetBoolTempVar();
							if (text.Length > 0)
							{
								text += ", ";
							}
							text = text + array[num] + "=false";
						}
						num++;
					}
					if (text.Length > 0)
					{
						text = "bool " + text;
						this.WriteLine(text + ";");
					}
					using (IEnumerator enumerator = map.AllElementInfos.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							if (((XmlTypeMapElementInfo)enumerator.Current).ExplicitOrder >= 0)
							{
								this.WriteLine("int idx = -1;");
								break;
							}
						}
					}
					this.WriteLine("");
				}
				string[] array2 = null;
				string[] array3 = null;
				string[] array4 = null;
				if (map.FlatLists != null)
				{
					array2 = new string[map.FlatLists.Count];
					array3 = new string[map.FlatLists.Count];
					string str = "int ";
					for (int i = 0; i < map.FlatLists.Count; i++)
					{
						XmlTypeMapMemberElement xmlTypeMapMemberElement = (XmlTypeMapMemberElement)map.FlatLists[i];
						array2[i] = this.GetNumTempVar();
						if (i > 0)
						{
							str += ", ";
						}
						str = str + array2[i] + "=0";
						if (!this.MemberHasReadReplaceHook(type, xmlTypeMapMemberElement))
						{
							array3[i] = this.GetObTempVar();
							this.WriteLine(xmlTypeMapMemberElement.TypeData.CSharpFullName + " " + array3[i] + ";");
							if (this.IsReadOnly(xmlTypeMapping, xmlTypeMapMemberElement, xmlTypeMapMemberElement.TypeData, isValueList))
							{
								string str2 = this.GenerateGetMemberValue(xmlTypeMapMemberElement, ob, isValueList);
								this.WriteLine(array3[i] + " = " + str2 + ";");
							}
							else if (xmlTypeMapMemberElement.TypeData.Type.IsArray)
							{
								string str2 = this.GenerateInitializeList(xmlTypeMapMemberElement.TypeData);
								this.WriteLine(array3[i] + " = " + str2 + ";");
							}
							else
							{
								this.WriteLine(array3[i] + " = " + this.GenerateGetMemberValue(xmlTypeMapMemberElement, ob, isValueList) + ";");
								this.WriteLineInd("if (((object)" + array3[i] + ") == null) {");
								this.WriteLine(array3[i] + " = " + this.GenerateInitializeList(xmlTypeMapMemberElement.TypeData) + ";");
								this.GenerateSetMemberValue(xmlTypeMapMemberElement, ob, array3[i], isValueList);
								this.WriteLineUni("}");
							}
						}
						if (xmlTypeMapMemberElement.ChoiceMember != null)
						{
							if (array4 == null)
							{
								array4 = new string[map.FlatLists.Count];
							}
							array4[i] = this.GetObTempVar();
							string text2 = this.GenerateInitializeList(xmlTypeMapMemberElement.ChoiceTypeData);
							this.WriteLine(string.Concat(new string[]
							{
								xmlTypeMapMemberElement.ChoiceTypeData.CSharpFullName,
								" ",
								array4[i],
								" = ",
								text2,
								";"
							}));
						}
					}
					this.WriteLine(str + ";");
					this.WriteLine("");
				}
				if (this._format == SerializationFormat.Encoded && map.ElementMembers != null)
				{
					this._fixupCallbacks.Add(xmlMap);
					this.WriteLine(string.Concat(new object[]
					{
						"Fixup fixup = new Fixup(",
						ob,
						", new XmlSerializationFixupCallback(",
						this.GetFixupCallbackName(xmlMap),
						"), ",
						map.ElementMembers.Count,
						");"
					}));
					this.WriteLine("AddFixup (fixup);");
					this.WriteLine("");
				}
				ArrayList arrayList = null;
				int num2;
				if (readBySoapOrder)
				{
					if (map.ElementMembers != null)
					{
						num2 = map.ElementMembers.Count;
					}
					else
					{
						num2 = 0;
					}
				}
				else
				{
					arrayList = new ArrayList();
					arrayList.AddRange(map.AllElementInfos);
					num2 = arrayList.Count;
					this.WriteLine("while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) ");
					this.WriteLineInd("{");
					this.WriteLine("if (Reader.NodeType == System.Xml.XmlNodeType.Element) ");
					this.WriteLineInd("{");
				}
				flag = true;
				int j = 0;
				while (j < num2)
				{
					XmlTypeMapElementInfo xmlTypeMapElementInfo = readBySoapOrder ? map.GetElement(j) : ((XmlTypeMapElementInfo)arrayList[j]);
					if (readBySoapOrder)
					{
						goto IL_6BE;
					}
					if (!xmlTypeMapElementInfo.IsTextElement && !xmlTypeMapElementInfo.IsUnnamedAnyElement)
					{
						string text3 = flag ? "" : "else ";
						text3 += "if (";
						if (xmlTypeMapElementInfo.ExplicitOrder >= 0)
						{
							text3 = string.Concat(new object[]
							{
								text3,
								"idx < ",
								xmlTypeMapElementInfo.ExplicitOrder,
								"&& "
							});
						}
						if (!xmlTypeMapElementInfo.Member.IsReturnValue || this._format != SerializationFormat.Encoded)
						{
							text3 = text3 + "Reader.LocalName == " + this.GetLiteral(xmlTypeMapElementInfo.ElementName);
							if (!map.IgnoreMemberNamespace)
							{
								text3 = text3 + " && Reader.NamespaceURI == " + this.GetLiteral(xmlTypeMapElementInfo.Namespace);
							}
							text3 += " && ";
						}
						if (array[xmlTypeMapElementInfo.Member.Index] != null)
						{
							text3 = text3 + "!" + array[xmlTypeMapElementInfo.Member.Index] + ") {";
						}
						else
						{
							text3 += "true) {";
						}
						this.WriteLineInd(text3);
						goto IL_6BE;
					}
					IL_100F:
					j++;
					continue;
					IL_6BE:
					if (xmlTypeMapElementInfo.Member.GetType() == typeof(XmlTypeMapMemberList))
					{
						if (this._format == SerializationFormat.Encoded && xmlTypeMapElementInfo.MultiReferenceType)
						{
							string obTempVar = this.GetObTempVar();
							this.WriteLine(string.Concat(new object[]
							{
								"object ",
								obTempVar,
								" = ReadReferencingElement (out fixup.Ids[",
								xmlTypeMapElementInfo.Member.Index,
								"]);"
							}));
							this.RegisterReferencingMap(xmlTypeMapElementInfo.MappedType);
							this.WriteLineInd("if (fixup.Ids[" + xmlTypeMapElementInfo.Member.Index + "] == null) {");
							if (this.IsReadOnly(xmlTypeMapping, xmlTypeMapElementInfo.Member, xmlTypeMapElementInfo.TypeData, isValueList))
							{
								this.WriteLine("throw CreateReadOnlyCollectionException (" + this.GetLiteral(xmlTypeMapElementInfo.TypeData.CSharpFullName) + ");");
							}
							else
							{
								this.GenerateSetMemberValue(xmlTypeMapElementInfo.Member, ob, this.GetCast(xmlTypeMapElementInfo.Member.TypeData, obTempVar), isValueList);
							}
							this.WriteLineUni("}");
							if (!xmlTypeMapElementInfo.MappedType.TypeData.Type.IsArray)
							{
								this.WriteLineInd("else {");
								if (this.IsReadOnly(xmlTypeMapping, xmlTypeMapElementInfo.Member, xmlTypeMapElementInfo.TypeData, isValueList))
								{
									this.WriteLine(obTempVar + " = " + this.GenerateGetMemberValue(xmlTypeMapElementInfo.Member, ob, isValueList) + ";");
								}
								else
								{
									this.WriteLine(obTempVar + " = " + this.GenerateCreateList(xmlTypeMapElementInfo.MappedType.TypeData.Type) + ";");
									this.GenerateSetMemberValue(xmlTypeMapElementInfo.Member, ob, this.GetCast(xmlTypeMapElementInfo.Member.TypeData, obTempVar), isValueList);
								}
								this.WriteLine(string.Concat(new object[]
								{
									"AddFixup (new CollectionFixup (",
									obTempVar,
									", new XmlSerializationCollectionFixupCallback (",
									this.GetFillListName(xmlTypeMapElementInfo.Member.TypeData),
									"), fixup.Ids[",
									xmlTypeMapElementInfo.Member.Index,
									"]));"
								}));
								this.WriteLine("fixup.Ids[" + xmlTypeMapElementInfo.Member.Index + "] = null;");
								this.WriteLineUni("}");
							}
						}
						else if (!this.GenerateReadMemberHook(type, xmlTypeMapElementInfo.Member))
						{
							if (this.IsReadOnly(xmlTypeMapping, xmlTypeMapElementInfo.Member, xmlTypeMapElementInfo.TypeData, isValueList))
							{
								this.GenerateReadListElement(xmlTypeMapElementInfo.MappedType, this.GenerateGetMemberValue(xmlTypeMapElementInfo.Member, ob, isValueList), this.GetLiteral(xmlTypeMapElementInfo.IsNullable), false);
							}
							else if (xmlTypeMapElementInfo.MappedType.TypeData.Type.IsArray)
							{
								if (xmlTypeMapElementInfo.IsNullable)
								{
									this.GenerateSetMemberValue(xmlTypeMapElementInfo.Member, ob, this.GenerateReadListElement(xmlTypeMapElementInfo.MappedType, null, this.GetLiteral(xmlTypeMapElementInfo.IsNullable), true), isValueList);
								}
								else
								{
									string obTempVar2 = this.GetObTempVar();
									this.WriteLine(string.Concat(new string[]
									{
										xmlTypeMapElementInfo.MappedType.TypeData.CSharpFullName,
										" ",
										obTempVar2,
										" = ",
										this.GenerateReadListElement(xmlTypeMapElementInfo.MappedType, null, this.GetLiteral(xmlTypeMapElementInfo.IsNullable), true),
										";"
									}));
									this.WriteLineInd("if (((object)" + obTempVar2 + ") != null) {");
									this.GenerateSetMemberValue(xmlTypeMapElementInfo.Member, ob, obTempVar2, isValueList);
									this.WriteLineUni("}");
								}
							}
							else
							{
								string obTempVar3 = this.GetObTempVar();
								this.WriteLine(string.Concat(new string[]
								{
									xmlTypeMapElementInfo.MappedType.TypeData.CSharpFullName,
									" ",
									obTempVar3,
									" = ",
									this.GenerateGetMemberValue(xmlTypeMapElementInfo.Member, ob, isValueList),
									";"
								}));
								this.WriteLineInd("if (((object)" + obTempVar3 + ") == null) {");
								this.WriteLine(obTempVar3 + " = " + this.GenerateCreateList(xmlTypeMapElementInfo.MappedType.TypeData.Type) + ";");
								this.GenerateSetMemberValue(xmlTypeMapElementInfo.Member, ob, obTempVar3, isValueList);
								this.WriteLineUni("}");
								this.GenerateReadListElement(xmlTypeMapElementInfo.MappedType, obTempVar3, this.GetLiteral(xmlTypeMapElementInfo.IsNullable), true);
							}
							this.GenerateEndHook();
						}
						if (!readBySoapOrder)
						{
							this.WriteLine(array[xmlTypeMapElementInfo.Member.Index] + " = true;");
						}
					}
					else if (xmlTypeMapElementInfo.Member.GetType() == typeof(XmlTypeMapMemberFlatList))
					{
						XmlTypeMapMemberFlatList xmlTypeMapMemberFlatList = (XmlTypeMapMemberFlatList)xmlTypeMapElementInfo.Member;
						if (!this.GenerateReadArrayMemberHook(type, xmlTypeMapElementInfo.Member, array2[xmlTypeMapMemberFlatList.FlatArrayIndex]))
						{
							this.GenerateAddListValue(xmlTypeMapMemberFlatList.TypeData, array3[xmlTypeMapMemberFlatList.FlatArrayIndex], array2[xmlTypeMapMemberFlatList.FlatArrayIndex], this.GenerateReadObjectElement(xmlTypeMapElementInfo), !this.IsReadOnly(xmlTypeMapping, xmlTypeMapElementInfo.Member, xmlTypeMapElementInfo.TypeData, isValueList));
							if (xmlTypeMapMemberFlatList.ChoiceMember != null)
							{
								this.GenerateAddListValue(xmlTypeMapMemberFlatList.ChoiceTypeData, array4[xmlTypeMapMemberFlatList.FlatArrayIndex], array2[xmlTypeMapMemberFlatList.FlatArrayIndex], this.GetLiteral(xmlTypeMapElementInfo.ChoiceValue), true);
							}
							this.GenerateEndHook();
						}
						this.WriteLine(array2[xmlTypeMapMemberFlatList.FlatArrayIndex] + "++;");
					}
					else if (xmlTypeMapElementInfo.Member.GetType() == typeof(XmlTypeMapMemberAnyElement))
					{
						XmlTypeMapMemberAnyElement xmlTypeMapMemberAnyElement = (XmlTypeMapMemberAnyElement)xmlTypeMapElementInfo.Member;
						if (xmlTypeMapMemberAnyElement.TypeData.IsListType)
						{
							if (!this.GenerateReadArrayMemberHook(type, xmlTypeMapElementInfo.Member, array2[xmlTypeMapMemberAnyElement.FlatArrayIndex]))
							{
								this.GenerateAddListValue(xmlTypeMapMemberAnyElement.TypeData, array3[xmlTypeMapMemberAnyElement.FlatArrayIndex], array2[xmlTypeMapMemberAnyElement.FlatArrayIndex], this.GetReadXmlNode(xmlTypeMapMemberAnyElement.TypeData.ListItemTypeData, false), true);
								this.GenerateEndHook();
							}
							this.WriteLine(array2[xmlTypeMapMemberAnyElement.FlatArrayIndex] + "++;");
						}
						else if (!this.GenerateReadMemberHook(type, xmlTypeMapElementInfo.Member))
						{
							this.GenerateSetMemberValue(xmlTypeMapMemberAnyElement, ob, this.GetReadXmlNode(xmlTypeMapMemberAnyElement.TypeData, false), isValueList);
							this.GenerateEndHook();
						}
					}
					else
					{
						if (!(xmlTypeMapElementInfo.Member.GetType() == typeof(XmlTypeMapMemberElement)))
						{
							throw new InvalidOperationException("Unknown member type");
						}
						if (!readBySoapOrder)
						{
							this.WriteLine(array[xmlTypeMapElementInfo.Member.Index] + " = true;");
						}
						if (xmlTypeMapElementInfo.ExplicitOrder >= 0)
						{
							this.WriteLine("idx = " + xmlTypeMapElementInfo.ExplicitOrder + ";");
						}
						if (this._format == SerializationFormat.Encoded)
						{
							string obTempVar4 = this.GetObTempVar();
							this.RegisterReferencingMap(xmlTypeMapElementInfo.MappedType);
							if (xmlTypeMapElementInfo.Member.TypeData.SchemaType != SchemaTypes.Primitive)
							{
								this.WriteLine(string.Concat(new object[]
								{
									"object ",
									obTempVar4,
									" = ReadReferencingElement (out fixup.Ids[",
									xmlTypeMapElementInfo.Member.Index,
									"]);"
								}));
							}
							else
							{
								this.WriteLine(string.Concat(new object[]
								{
									"object ",
									obTempVar4,
									" = ReadReferencingElement (",
									this.GetLiteral(xmlTypeMapElementInfo.Member.TypeData.XmlType),
									", ",
									this.GetLiteral("http://www.w3.org/2001/XMLSchema"),
									", out fixup.Ids[",
									xmlTypeMapElementInfo.Member.Index,
									"]);"
								}));
							}
							if (xmlTypeMapElementInfo.MultiReferenceType)
							{
								this.WriteLineInd("if (fixup.Ids[" + xmlTypeMapElementInfo.Member.Index + "] == null) {");
							}
							else
							{
								this.WriteLineInd("if (" + obTempVar4 + " != null) {");
							}
							this.GenerateSetMemberValue(xmlTypeMapElementInfo.Member, ob, this.GetCast(xmlTypeMapElementInfo.Member.TypeData, obTempVar4), isValueList);
							this.WriteLineUni("}");
						}
						else if (!this.GenerateReadMemberHook(type, xmlTypeMapElementInfo.Member))
						{
							if (xmlTypeMapElementInfo.ChoiceValue != null)
							{
								XmlTypeMapMemberElement xmlTypeMapMemberElement2 = (XmlTypeMapMemberElement)xmlTypeMapElementInfo.Member;
								this.WriteLine(string.Concat(new string[]
								{
									ob,
									".@",
									xmlTypeMapMemberElement2.ChoiceMember,
									" = ",
									this.GetLiteral(xmlTypeMapElementInfo.ChoiceValue),
									";"
								}));
							}
							this.GenerateSetMemberValue(xmlTypeMapElementInfo.Member, ob, this.GenerateReadObjectElement(xmlTypeMapElementInfo), isValueList);
							this.GenerateEndHook();
						}
					}
					if (!readBySoapOrder)
					{
						this.WriteLineUni("}");
					}
					else
					{
						this.WriteLine("Reader.MoveToContent();");
					}
					flag = false;
					goto IL_100F;
				}
				if (!readBySoapOrder)
				{
					if (!flag)
					{
						this.WriteLineInd("else {");
					}
					if (map.DefaultAnyElementMember != null)
					{
						XmlTypeMapMemberAnyElement defaultAnyElementMember = map.DefaultAnyElementMember;
						if (defaultAnyElementMember.TypeData.IsListType)
						{
							if (!this.GenerateReadArrayMemberHook(type, defaultAnyElementMember, array2[defaultAnyElementMember.FlatArrayIndex]))
							{
								this.GenerateAddListValue(defaultAnyElementMember.TypeData, array3[defaultAnyElementMember.FlatArrayIndex], array2[defaultAnyElementMember.FlatArrayIndex], this.GetReadXmlNode(defaultAnyElementMember.TypeData.ListItemTypeData, false), true);
								this.GenerateEndHook();
							}
							this.WriteLine(array2[defaultAnyElementMember.FlatArrayIndex] + "++;");
						}
						else if (!this.GenerateReadMemberHook(type, defaultAnyElementMember))
						{
							this.GenerateSetMemberValue(defaultAnyElementMember, ob, this.GetReadXmlNode(defaultAnyElementMember.TypeData, false), isValueList);
							this.GenerateEndHook();
						}
					}
					else if (!this.GenerateReadHook(HookType.unknownElement, type))
					{
						this.WriteLine("UnknownNode (" + ob + ");");
						this.GenerateEndHook();
					}
					if (!flag)
					{
						this.WriteLineUni("}");
					}
					this.WriteLineUni("}");
					if (map.XmlTextCollector != null)
					{
						this.WriteLine("else if (Reader.NodeType == System.Xml.XmlNodeType.Text || Reader.NodeType == System.Xml.XmlNodeType.CDATA)");
						this.WriteLineInd("{");
						if (map.XmlTextCollector is XmlTypeMapMemberExpandable)
						{
							XmlTypeMapMemberExpandable xmlTypeMapMemberExpandable = (XmlTypeMapMemberExpandable)map.XmlTextCollector;
							XmlTypeMapMemberFlatList xmlTypeMapMemberFlatList2 = xmlTypeMapMemberExpandable as XmlTypeMapMemberFlatList;
							TypeData typeData = (xmlTypeMapMemberFlatList2 == null) ? xmlTypeMapMemberExpandable.TypeData.ListItemTypeData : xmlTypeMapMemberFlatList2.ListMap.FindTextElement().TypeData;
							if (!this.GenerateReadArrayMemberHook(type, map.XmlTextCollector, array2[xmlTypeMapMemberExpandable.FlatArrayIndex]))
							{
								string value = (typeData.Type == typeof(string)) ? "Reader.ReadString()" : this.GetReadXmlNode(typeData, false);
								this.GenerateAddListValue(xmlTypeMapMemberExpandable.TypeData, array3[xmlTypeMapMemberExpandable.FlatArrayIndex], array2[xmlTypeMapMemberExpandable.FlatArrayIndex], value, true);
								this.GenerateEndHook();
							}
							this.WriteLine(array2[xmlTypeMapMemberExpandable.FlatArrayIndex] + "++;");
						}
						else if (!this.GenerateReadMemberHook(type, map.XmlTextCollector))
						{
							XmlTypeMapMemberElement xmlTypeMapMemberElement3 = (XmlTypeMapMemberElement)map.XmlTextCollector;
							XmlTypeMapElementInfo xmlTypeMapElementInfo2 = (XmlTypeMapElementInfo)xmlTypeMapMemberElement3.ElementInfo[0];
							string strTempVar = this.GetStrTempVar();
							this.WriteLine("string " + strTempVar + " = Reader.ReadString();");
							if (xmlTypeMapElementInfo2.TypeData.Type == typeof(string))
							{
								this.GenerateSetMemberValue(xmlTypeMapMemberElement3, ob, strTempVar, isValueList);
							}
							else
							{
								this.GenerateSetMemberValue(xmlTypeMapMemberElement3, ob, this.GenerateGetValueFromXmlString(strTempVar, xmlTypeMapElementInfo2.TypeData, xmlTypeMapElementInfo2.MappedType, xmlTypeMapElementInfo2.IsNullable), isValueList);
							}
							this.GenerateEndHook();
						}
						this.WriteLineUni("}");
					}
					this.WriteLine("else");
					this.WriteLine("\tUnknownNode(" + ob + ");");
					this.WriteLine("");
					this.WriteLine("Reader.MoveToContent();");
					this.WriteLineUni("}");
				}
				else
				{
					this.WriteLine("Reader.MoveToContent();");
				}
				if (array3 != null)
				{
					this.WriteLine("");
					foreach (object obj2 in map.FlatLists)
					{
						XmlTypeMapMemberExpandable xmlTypeMapMemberExpandable2 = (XmlTypeMapMemberExpandable)obj2;
						if (!this.MemberHasReadReplaceHook(type, xmlTypeMapMemberExpandable2))
						{
							string text4 = array3[xmlTypeMapMemberExpandable2.FlatArrayIndex];
							if (xmlTypeMapMemberExpandable2.TypeData.Type.IsArray)
							{
								this.WriteLine(string.Concat(new string[]
								{
									text4,
									" = (",
									xmlTypeMapMemberExpandable2.TypeData.CSharpFullName,
									") ShrinkArray (",
									text4,
									", ",
									array2[xmlTypeMapMemberExpandable2.FlatArrayIndex],
									", ",
									this.GetTypeOf(xmlTypeMapMemberExpandable2.TypeData.Type.GetElementType()),
									", true);"
								}));
							}
							if (!this.IsReadOnly(xmlTypeMapping, xmlTypeMapMemberExpandable2, xmlTypeMapMemberExpandable2.TypeData, isValueList) && xmlTypeMapMemberExpandable2.TypeData.Type.IsArray)
							{
								this.GenerateSetMemberValue(xmlTypeMapMemberExpandable2, ob, text4, isValueList);
							}
						}
					}
				}
				if (array4 != null)
				{
					this.WriteLine("");
					foreach (object obj3 in map.FlatLists)
					{
						XmlTypeMapMemberExpandable xmlTypeMapMemberExpandable3 = (XmlTypeMapMemberExpandable)obj3;
						if (!this.MemberHasReadReplaceHook(type, xmlTypeMapMemberExpandable3) && xmlTypeMapMemberExpandable3.ChoiceMember != null)
						{
							string text5 = array4[xmlTypeMapMemberExpandable3.FlatArrayIndex];
							this.WriteLine(string.Concat(new string[]
							{
								text5,
								" = (",
								xmlTypeMapMemberExpandable3.ChoiceTypeData.CSharpFullName,
								") ShrinkArray (",
								text5,
								", ",
								array2[xmlTypeMapMemberExpandable3.FlatArrayIndex],
								", ",
								this.GetTypeOf(xmlTypeMapMemberExpandable3.ChoiceTypeData.Type.GetElementType()),
								", true);"
							}));
							this.WriteLine(string.Concat(new string[]
							{
								ob,
								".@",
								xmlTypeMapMemberExpandable3.ChoiceMember,
								" = ",
								text5,
								";"
							}));
						}
					}
				}
				this.GenerateSetListMembersDefaults(xmlTypeMapping, map, ob, isValueList);
				this.GenerateEndHook();
			}
			if (!isValueList)
			{
				this.WriteLine("");
				this.WriteLine("ReadEndElement();");
			}
		}

		// Token: 0x06001B8A RID: 7050 RVA: 0x0009EE00 File Offset: 0x0009D000
		private void GenerateReadAttributeMembers(XmlMapping xmlMap, ClassMap map, string ob, bool isValueList, ref bool first)
		{
			XmlTypeMapping xmlTypeMapping = xmlMap as XmlTypeMapping;
			Type type = (xmlTypeMapping != null) ? xmlTypeMapping.TypeData.Type : typeof(object[]);
			if (this.GenerateReadHook(HookType.attributes, type))
			{
				return;
			}
			XmlTypeMapMember defaultAnyAttributeMember = map.DefaultAnyAttributeMember;
			if (defaultAnyAttributeMember != null)
			{
				this.WriteLine("int anyAttributeIndex = 0;");
				this.WriteLine(defaultAnyAttributeMember.TypeData.CSharpFullName + " anyAttributeArray = null;");
			}
			this.WriteLine("while (Reader.MoveToNextAttribute())");
			this.WriteLineInd("{");
			first = true;
			if (map.AttributeMembers != null)
			{
				foreach (object obj in map.AttributeMembers)
				{
					XmlTypeMapMemberAttribute xmlTypeMapMemberAttribute = (XmlTypeMapMemberAttribute)obj;
					this.WriteLineInd(string.Concat(new string[]
					{
						first ? "" : "else ",
						"if (Reader.LocalName == ",
						this.GetLiteral(xmlTypeMapMemberAttribute.AttributeName),
						" && Reader.NamespaceURI == ",
						this.GetLiteral(xmlTypeMapMemberAttribute.Namespace),
						") {"
					}));
					if (!this.GenerateReadMemberHook(type, xmlTypeMapMemberAttribute))
					{
						this.GenerateSetMemberValue(xmlTypeMapMemberAttribute, ob, this.GenerateGetValueFromXmlString("Reader.Value", xmlTypeMapMemberAttribute.TypeData, xmlTypeMapMemberAttribute.MappedType, false), isValueList);
						this.GenerateEndHook();
					}
					this.WriteLineUni("}");
					first = false;
				}
			}
			this.WriteLineInd((first ? "" : "else ") + "if (IsXmlnsAttribute (Reader.Name)) {");
			if (map.NamespaceDeclarations != null && !this.GenerateReadMemberHook(type, map.NamespaceDeclarations))
			{
				string text = ob + ".@" + map.NamespaceDeclarations.Name;
				this.WriteLine(string.Concat(new string[]
				{
					"if (",
					text,
					" == null) ",
					text,
					" = new XmlSerializerNamespaces ();"
				}));
				this.WriteLineInd("if (Reader.Prefix == \"xmlns\")");
				this.WriteLine(text + ".Add (Reader.LocalName, Reader.Value);");
				this.Unindent();
				this.WriteLineInd("else");
				this.WriteLine(text + ".Add (\"\", Reader.Value);");
				this.Unindent();
				this.GenerateEndHook();
			}
			this.WriteLineUni("}");
			this.WriteLineInd("else {");
			if (defaultAnyAttributeMember != null)
			{
				if (!this.GenerateReadArrayMemberHook(type, defaultAnyAttributeMember, "anyAttributeIndex"))
				{
					this.WriteLine("System.Xml.XmlAttribute attr = (System.Xml.XmlAttribute) Document.ReadNode(Reader);");
					if (typeof(XmlSchemaAnnotated).IsAssignableFrom(type))
					{
						this.WriteLine("ParseWsdlArrayType (attr);");
					}
					this.GenerateAddListValue(defaultAnyAttributeMember.TypeData, "anyAttributeArray", "anyAttributeIndex", this.GetCast(defaultAnyAttributeMember.TypeData.ListItemTypeData, "attr"), true);
					this.GenerateEndHook();
				}
				this.WriteLine("anyAttributeIndex++;");
			}
			else if (!this.GenerateReadHook(HookType.unknownAttribute, type))
			{
				this.WriteLine("UnknownNode (" + ob + ");");
				this.GenerateEndHook();
			}
			this.WriteLineUni("}");
			this.WriteLineUni("}");
			if (defaultAnyAttributeMember != null && !this.MemberHasReadReplaceHook(type, defaultAnyAttributeMember))
			{
				this.WriteLine("");
				this.WriteLine(string.Concat(new string[]
				{
					"anyAttributeArray = (",
					defaultAnyAttributeMember.TypeData.CSharpFullName,
					") ShrinkArray (anyAttributeArray, anyAttributeIndex, ",
					this.GetTypeOf(defaultAnyAttributeMember.TypeData.Type.GetElementType()),
					", true);"
				}));
				this.GenerateSetMemberValue(defaultAnyAttributeMember, ob, "anyAttributeArray", isValueList);
			}
			this.WriteLine("");
			this.WriteLine("Reader.MoveToElement ();");
			this.GenerateEndHook();
		}

		// Token: 0x06001B8B RID: 7051 RVA: 0x0009F1B4 File Offset: 0x0009D3B4
		private void GenerateSetListMembersDefaults(XmlTypeMapping typeMap, ClassMap map, string ob, bool isValueList)
		{
			if (map.ListMembers != null)
			{
				ArrayList listMembers = map.ListMembers;
				for (int i = 0; i < listMembers.Count; i++)
				{
					XmlTypeMapMember xmlTypeMapMember = (XmlTypeMapMember)listMembers[i];
					if (!this.IsReadOnly(typeMap, xmlTypeMapMember, xmlTypeMapMember.TypeData, isValueList))
					{
						this.WriteLineInd("if (" + this.GenerateGetMemberValue(xmlTypeMapMember, ob, isValueList) + " == null) {");
						this.GenerateSetMemberValue(xmlTypeMapMember, ob, this.GenerateInitializeList(xmlTypeMapMember.TypeData), isValueList);
						this.WriteLineUni("}");
					}
				}
			}
		}

		// Token: 0x06001B8C RID: 7052 RVA: 0x0009F240 File Offset: 0x0009D440
		private bool IsReadOnly(XmlTypeMapping map, XmlTypeMapMember member, TypeData memType, bool isValueList)
		{
			if (isValueList)
			{
				return !memType.HasPublicConstructor;
			}
			return member.IsReadOnly(map.TypeData.Type) || !memType.HasPublicConstructor;
		}

		// Token: 0x06001B8D RID: 7053 RVA: 0x0009F26E File Offset: 0x0009D46E
		private void GenerateSetMemberValue(XmlTypeMapMember member, string ob, string value, bool isValueList)
		{
			this.GenerateSetMemberValue(member, ob, value, isValueList, false);
		}

		// Token: 0x06001B8E RID: 7054 RVA: 0x0009F27C File Offset: 0x0009D47C
		private void GenerateSetMemberValue(XmlTypeMapMember member, string ob, string value, bool isValueList, bool initializingMember)
		{
			if (isValueList)
			{
				this.WriteLine(string.Concat(new object[]
				{
					ob,
					"[",
					member.GlobalIndex,
					"] = ",
					value,
					";"
				}));
				if (member.IsOptionalValueType)
				{
					string text = initializingMember ? "false" : "true";
					this.WriteLine(string.Concat(new object[]
					{
						ob,
						"[",
						member.SpecifiedGlobalIndex,
						"] = ",
						text,
						";"
					}));
					return;
				}
			}
			else
			{
				this.WriteLine(string.Concat(new string[]
				{
					ob,
					".@",
					member.Name,
					" = ",
					value,
					";"
				}));
				if (member.IsOptionalValueType && member.IsValueSpecifiedSettable())
				{
					string text2 = initializingMember ? "false" : "true";
					this.WriteLine(string.Concat(new string[]
					{
						ob,
						".",
						member.Name,
						"Specified = ",
						text2,
						";"
					}));
				}
			}
		}

		// Token: 0x06001B8F RID: 7055 RVA: 0x0009F3C0 File Offset: 0x0009D5C0
		private void GenerateSetMemberValueFromAttr(XmlTypeMapMember member, string ob, string value, bool isValueList)
		{
			if (member.TypeData.Type.IsEnum)
			{
				value = this.GetCast(member.TypeData.Type, value);
			}
			this.GenerateSetMemberValue(member, ob, value, isValueList, true);
		}

		// Token: 0x06001B90 RID: 7056 RVA: 0x0009F3F4 File Offset: 0x0009D5F4
		private string GenerateReadObjectElement(XmlTypeMapElementInfo elem)
		{
			switch (elem.TypeData.SchemaType)
			{
			case SchemaTypes.Primitive:
			case SchemaTypes.Enum:
				return this.GenerateReadPrimitiveValue(elem);
			case SchemaTypes.Array:
				return this.GenerateReadListElement(elem.MappedType, null, this.GetLiteral(elem.IsNullable), true);
			case SchemaTypes.Class:
				return this.GetReadObjectCall(elem.MappedType, this.GetLiteral(elem.IsNullable), "true");
			case SchemaTypes.XmlSerializable:
				return this.GetCast(elem.TypeData, string.Format("({0}) ReadSerializable (({0}) Activator.CreateInstance(typeof({0}), true))", elem.TypeData.CSharpFullName));
			case SchemaTypes.XmlNode:
				return this.GetReadXmlNode(elem.TypeData, true);
			default:
				throw new NotSupportedException("Invalid value type");
			}
		}

		// Token: 0x06001B91 RID: 7057 RVA: 0x0009F4B4 File Offset: 0x0009D6B4
		private string GenerateReadPrimitiveValue(XmlTypeMapElementInfo elem)
		{
			if (elem.TypeData.Type == typeof(XmlQualifiedName))
			{
				if (elem.IsNullable)
				{
					return "ReadNullableQualifiedName ()";
				}
				return "ReadElementQualifiedName ()";
			}
			else
			{
				if (elem.IsNullable)
				{
					string strTempVar = this.GetStrTempVar();
					this.WriteLine("string " + strTempVar + " = ReadNullableString ();");
					return this.GenerateGetValueFromXmlString(strTempVar, elem.TypeData, elem.MappedType, true);
				}
				string strTempVar2 = this.GetStrTempVar();
				this.WriteLine("string " + strTempVar2 + " = Reader.ReadElementString ();");
				return this.GenerateGetValueFromXmlString(strTempVar2, elem.TypeData, elem.MappedType, false);
			}
		}

		// Token: 0x06001B92 RID: 7058 RVA: 0x0009F55C File Offset: 0x0009D75C
		private string GenerateGetValueFromXmlString(string value, TypeData typeData, XmlTypeMapping typeMap, bool isNullable)
		{
			if (typeData.SchemaType == SchemaTypes.Array)
			{
				return this.GenerateReadListString(typeMap, value);
			}
			if (typeData.SchemaType == SchemaTypes.Enum)
			{
				return this.GenerateGetEnumValue(typeMap, value, isNullable);
			}
			if (typeData.Type == typeof(XmlQualifiedName))
			{
				return "ToXmlQualifiedName (" + value + ")";
			}
			return XmlCustomFormatter.GenerateFromXmlString(typeData, value);
		}

		// Token: 0x06001B93 RID: 7059 RVA: 0x0009F5C0 File Offset: 0x0009D7C0
		private string GenerateReadListElement(XmlTypeMapping typeMap, string list, string isNullable, bool canCreateInstance)
		{
			Type type = typeMap.TypeData.Type;
			ListMap listMap = (ListMap)typeMap.ObjectMap;
			bool flag = typeMap.TypeData.Type.IsArray;
			if (canCreateInstance && typeMap.TypeData.HasPublicConstructor)
			{
				if (list == null)
				{
					list = this.GetObTempVar();
					this.WriteLine(typeMap.TypeData.CSharpFullName + " " + list + " = null;");
					if (flag)
					{
						this.WriteLineInd("if (!ReadNull()) {");
					}
					this.WriteLine(list + " = " + this.GenerateCreateList(type) + ";");
				}
				else if (flag)
				{
					this.WriteLineInd("if (!ReadNull()) {");
				}
			}
			else
			{
				if (list == null)
				{
					this.WriteLine("throw CreateReadOnlyCollectionException (" + this.GetLiteral(typeMap.TypeData.CSharpFullName) + ");");
					return list;
				}
				this.WriteLineInd("if (((object)" + list + ") == null)");
				this.WriteLine("throw CreateReadOnlyCollectionException (" + this.GetLiteral(typeMap.TypeData.CSharpFullName) + ");");
				this.Unindent();
				flag = false;
			}
			this.WriteLineInd("if (Reader.IsEmptyElement) {");
			this.WriteLine("Reader.Skip();");
			if (type.IsArray)
			{
				this.WriteLine(string.Concat(new string[]
				{
					list,
					" = (",
					typeMap.TypeData.CSharpFullName,
					") ShrinkArray (",
					list,
					", 0, ",
					this.GetTypeOf(type.GetElementType()),
					", false);"
				}));
			}
			this.Unindent();
			this.WriteLineInd("} else {");
			string numTempVar = this.GetNumTempVar();
			this.WriteLine("int " + numTempVar + " = 0;");
			this.WriteLine("Reader.ReadStartElement();");
			this.WriteLine("Reader.MoveToContent();");
			this.WriteLine("");
			this.WriteLine("while (Reader.NodeType != System.Xml.XmlNodeType.EndElement) ");
			this.WriteLineInd("{");
			this.WriteLine("if (Reader.NodeType == System.Xml.XmlNodeType.Element) ");
			this.WriteLineInd("{");
			bool flag2 = true;
			foreach (object obj in listMap.ItemInfo)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
				this.WriteLineInd(string.Concat(new string[]
				{
					flag2 ? "" : "else ",
					"if (Reader.LocalName == ",
					this.GetLiteral(xmlTypeMapElementInfo.ElementName),
					" && Reader.NamespaceURI == ",
					this.GetLiteral(xmlTypeMapElementInfo.Namespace),
					") {"
				}));
				this.GenerateAddListValue(typeMap.TypeData, list, numTempVar, this.GenerateReadObjectElement(xmlTypeMapElementInfo), false);
				this.WriteLine(numTempVar + "++;");
				this.WriteLineUni("}");
				flag2 = false;
			}
			if (!flag2)
			{
				this.WriteLine("else UnknownNode (null);");
			}
			else
			{
				this.WriteLine("UnknownNode (null);");
			}
			this.WriteLineUni("}");
			this.WriteLine("else UnknownNode (null);");
			this.WriteLine("");
			this.WriteLine("Reader.MoveToContent();");
			this.WriteLineUni("}");
			this.WriteLine("ReadEndElement();");
			if (type.IsArray)
			{
				this.WriteLine(string.Concat(new string[]
				{
					list,
					" = (",
					typeMap.TypeData.CSharpFullName,
					") ShrinkArray (",
					list,
					", ",
					numTempVar,
					", ",
					this.GetTypeOf(type.GetElementType()),
					", false);"
				}));
			}
			this.WriteLineUni("}");
			if (flag)
			{
				this.WriteLineUni("}");
			}
			return list;
		}

		// Token: 0x06001B94 RID: 7060 RVA: 0x0009F9A0 File Offset: 0x0009DBA0
		private string GenerateReadListString(XmlTypeMapping typeMap, string values)
		{
			Type type = typeMap.TypeData.Type;
			ListMap listMap = (ListMap)typeMap.ObjectMap;
			string str = SerializationCodeGenerator.ToCSharpFullName(type.GetElementType());
			string obTempVar = this.GetObTempVar();
			this.WriteLine(str + "[] " + obTempVar + ";");
			string strTempVar = this.GetStrTempVar();
			this.WriteLine(string.Concat(new string[]
			{
				"string ",
				strTempVar,
				" = ",
				values,
				".Trim();"
			}));
			this.WriteLineInd("if (" + strTempVar + " != string.Empty) {");
			string obTempVar2 = this.GetObTempVar();
			this.WriteLine(string.Concat(new string[]
			{
				"string[] ",
				obTempVar2,
				" = ",
				strTempVar,
				".Split (' ');"
			}));
			this.WriteLine(obTempVar + " = new " + this.GetArrayDeclaration(type, obTempVar2 + ".Length") + ";");
			XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)listMap.ItemInfo[0];
			string numTempVar = this.GetNumTempVar();
			this.WriteLineInd(string.Concat(new string[]
			{
				"for (int ",
				numTempVar,
				" = 0; ",
				numTempVar,
				" < ",
				obTempVar2,
				".Length; ",
				numTempVar,
				"++)"
			}));
			this.WriteLine(string.Concat(new string[]
			{
				obTempVar,
				"[",
				numTempVar,
				"] = ",
				this.GenerateGetValueFromXmlString(obTempVar2 + "[" + numTempVar + "]", xmlTypeMapElementInfo.TypeData, xmlTypeMapElementInfo.MappedType, xmlTypeMapElementInfo.IsNullable),
				";"
			}));
			this.Unindent();
			this.WriteLineUni("}");
			this.WriteLine("else");
			this.WriteLine(string.Concat(new string[]
			{
				"\t",
				obTempVar,
				" = new ",
				this.GetArrayDeclaration(type, "0"),
				";"
			}));
			return obTempVar;
		}

		// Token: 0x06001B95 RID: 7061 RVA: 0x0009FBCC File Offset: 0x0009DDCC
		private string GetArrayDeclaration(Type type, string length)
		{
			Type elementType = type.GetElementType();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append('[').Append(length).Append(']');
			while (elementType.IsArray)
			{
				stringBuilder.Append("[]");
				elementType = elementType.GetElementType();
			}
			stringBuilder.Insert(0, SerializationCodeGenerator.ToCSharpFullName(elementType));
			return stringBuilder.ToString();
		}

		// Token: 0x06001B96 RID: 7062 RVA: 0x0009FC30 File Offset: 0x0009DE30
		private void GenerateAddListValue(TypeData listType, string list, string index, string value, bool canCreateInstance)
		{
			Type type = listType.Type;
			if (type.IsArray)
			{
				this.WriteLine(string.Concat(new string[]
				{
					list,
					" = (",
					SerializationCodeGenerator.ToCSharpFullName(type),
					") EnsureArrayIndex (",
					list,
					", ",
					index,
					", ",
					this.GetTypeOf(type.GetElementType()),
					");"
				}));
				this.WriteLine(string.Concat(new string[]
				{
					list,
					"[",
					index,
					"] = ",
					value,
					";"
				}));
				return;
			}
			this.WriteLine("if (((object)" + list + ") == null)");
			if (canCreateInstance)
			{
				this.WriteLine("\t" + list + string.Format(" = ({0}) Activator.CreateInstance(typeof({0}), true);", listType.CSharpFullName));
			}
			else
			{
				this.WriteLine("\tthrow CreateReadOnlyCollectionException (" + this.GetLiteral(listType.CSharpFullName) + ");");
			}
			this.WriteLine(list + ".Add (" + value + ");");
		}

		// Token: 0x06001B97 RID: 7063 RVA: 0x0009FD5C File Offset: 0x0009DF5C
		private string GenerateCreateList(Type listType)
		{
			if (listType.IsArray)
			{
				return string.Concat(new string[]
				{
					"(",
					SerializationCodeGenerator.ToCSharpFullName(listType),
					") EnsureArrayIndex (null, 0, ",
					this.GetTypeOf(listType.GetElementType()),
					")"
				});
			}
			return "new " + SerializationCodeGenerator.ToCSharpFullName(listType) + "()";
		}

		// Token: 0x06001B98 RID: 7064 RVA: 0x0009FDC2 File Offset: 0x0009DFC2
		private string GenerateInitializeList(TypeData listType)
		{
			if (listType.Type.IsArray)
			{
				return "null";
			}
			return "new " + listType.CSharpFullName + "()";
		}

		// Token: 0x06001B99 RID: 7065 RVA: 0x0009FDEC File Offset: 0x0009DFEC
		private void GenerateFillerCallbacks()
		{
			foreach (object obj in this._listsToFill)
			{
				TypeData typeData = (TypeData)obj;
				string fillListName = this.GetFillListName(typeData);
				this.WriteLine("void " + fillListName + " (object list, object source)");
				this.WriteLineInd("{");
				this.WriteLine("if (((object)list) == null) throw CreateReadOnlyCollectionException (" + this.GetLiteral(typeData.CSharpFullName) + ");");
				this.WriteLine("");
				this.WriteLine(typeData.CSharpFullName + " dest = (" + typeData.CSharpFullName + ") list;");
				this.WriteLine("foreach (object ob in (IEnumerable)source)");
				this.WriteLine("\tdest.Add (" + this.GetCast(typeData.ListItemTypeData, "ob") + ");");
				this.WriteLineUni("}");
				this.WriteLine("");
			}
		}

		// Token: 0x06001B9A RID: 7066 RVA: 0x0009FF04 File Offset: 0x0009E104
		private void GenerateReadXmlNodeElement(XmlTypeMapping typeMap, string isNullable)
		{
			this.WriteLine("return " + this.GetReadXmlNode(typeMap.TypeData, false) + ";");
		}

		// Token: 0x06001B9B RID: 7067 RVA: 0x0009FF28 File Offset: 0x0009E128
		private void GenerateReadPrimitiveElement(XmlTypeMapping typeMap, string isNullable)
		{
			this.WriteLine("XmlQualifiedName t = GetXsiType();");
			this.WriteLine(string.Concat(new string[]
			{
				"if (t == null) t = new XmlQualifiedName (",
				this.GetLiteral(typeMap.XmlType),
				", ",
				this.GetLiteral(typeMap.Namespace),
				");"
			}));
			this.WriteLine("return " + this.GetCast(typeMap.TypeData, "ReadTypedPrimitive (t)") + ";");
		}

		// Token: 0x06001B9C RID: 7068 RVA: 0x0009FFB0 File Offset: 0x0009E1B0
		private void GenerateReadEnumElement(XmlTypeMapping typeMap, string isNullable)
		{
			this.WriteLine("Reader.ReadStartElement ();");
			this.WriteLine(typeMap.TypeData.CSharpFullName + " res = " + this.GenerateGetEnumValue(typeMap, "Reader.ReadString()", false) + ";");
			this.WriteLineInd("if (Reader.NodeType != XmlNodeType.None)");
			this.WriteLineUni("Reader.ReadEndElement ();");
			this.WriteLine("return res;");
		}

		// Token: 0x06001B9D RID: 7069 RVA: 0x000A0018 File Offset: 0x0009E218
		private string GenerateGetEnumValue(XmlTypeMapping typeMap, string val, bool isNullable)
		{
			if (isNullable)
			{
				return string.Concat(new string[]
				{
					"(",
					val,
					") != null ? ",
					this.GetGetEnumValueName(typeMap),
					" (",
					val,
					") : (",
					typeMap.TypeData.CSharpFullName,
					"?) null"
				});
			}
			return this.GetGetEnumValueName(typeMap) + " (" + val + ")";
		}

		// Token: 0x06001B9E RID: 7070 RVA: 0x000A0094 File Offset: 0x0009E294
		private void GenerateGetEnumValueMethod(XmlTypeMapping typeMap)
		{
			string text = this.GetGetEnumValueName(typeMap);
			if (((EnumMap)typeMap.ObjectMap).IsFlags)
			{
				string text2 = text + "_Switch";
				this.WriteLine(typeMap.TypeData.CSharpFullName + " " + text + " (string xmlName)");
				this.WriteLineInd("{");
				this.WriteLine("xmlName = xmlName.Trim();");
				this.WriteLine("if (xmlName.Length == 0) return (" + typeMap.TypeData.CSharpFullName + ")0;");
				this.WriteLine(typeMap.TypeData.CSharpFullName + " sb = (" + typeMap.TypeData.CSharpFullName + ")0;");
				this.WriteLine("string[] enumNames = xmlName.Split (null);");
				this.WriteLine("foreach (string name in enumNames)");
				this.WriteLineInd("{");
				this.WriteLine("if (name == string.Empty) continue;");
				this.WriteLine("sb |= " + text2 + " (name); ");
				this.WriteLineUni("}");
				this.WriteLine("return sb;");
				this.WriteLineUni("}");
				this.WriteLine("");
				text = text2;
			}
			this.WriteLine(typeMap.TypeData.CSharpFullName + " " + text + " (string xmlName)");
			this.WriteLineInd("{");
			this.GenerateGetSingleEnumValue(typeMap, "xmlName");
			this.WriteLineUni("}");
			this.WriteLine("");
		}

		// Token: 0x06001B9F RID: 7071 RVA: 0x000A020C File Offset: 0x0009E40C
		private void GenerateGetSingleEnumValue(XmlTypeMapping typeMap, string val)
		{
			EnumMap enumMap = (EnumMap)typeMap.ObjectMap;
			this.WriteLine("switch (" + val + ")");
			this.WriteLineInd("{");
			foreach (EnumMap.EnumMapMember enumMapMember in enumMap.Members)
			{
				this.WriteLine(string.Concat(new string[]
				{
					"case ",
					this.GetLiteral(enumMapMember.XmlName),
					": return ",
					typeMap.TypeData.CSharpFullName,
					".@",
					enumMapMember.EnumName,
					";"
				}));
			}
			this.WriteLineInd("default:");
			this.WriteLine(string.Concat(new string[]
			{
				"throw CreateUnknownConstantException (",
				val,
				", typeof(",
				typeMap.TypeData.CSharpFullName,
				"));"
			}));
			this.Unindent();
			this.WriteLineUni("}");
		}

		// Token: 0x06001BA0 RID: 7072 RVA: 0x000A030C File Offset: 0x0009E50C
		private void GenerateReadXmlSerializableElement(XmlTypeMapping typeMap, string isNullable)
		{
			this.WriteLine("Reader.MoveToContent ();");
			this.WriteLine("if (Reader.NodeType == XmlNodeType.Element)");
			this.WriteLineInd("{");
			if (!typeMap.IsAny)
			{
				this.WriteLineInd(string.Concat(new string[]
				{
					"if (Reader.LocalName == ",
					this.GetLiteral(typeMap.ElementName),
					" && Reader.NamespaceURI == ",
					this.GetLiteral(typeMap.Namespace),
					")"
				}));
			}
			this.WriteLine(string.Format("\treturn ({0}) ReadSerializable (({0}) Activator.CreateInstance(typeof({0}), true));", typeMap.TypeData.CSharpFullName));
			this.Unindent();
			if (!typeMap.IsAny)
			{
				this.WriteLine("else");
				this.WriteLine("\tthrow CreateUnknownNodeException ();");
			}
			this.WriteLineUni("}");
			this.WriteLine("else UnknownNode (null);");
			this.WriteLine("");
			this.WriteLine("return default (" + typeMap.TypeData.CSharpFullName + ");");
		}

		// Token: 0x06001BA1 RID: 7073 RVA: 0x000A040C File Offset: 0x0009E60C
		private void GenerateReadInitCallbacks()
		{
			this.WriteLine("protected override void InitCallbacks ()");
			this.WriteLineInd("{");
			if (this._format == SerializationFormat.Encoded)
			{
				foreach (object obj in this._mapsToGenerate)
				{
					XmlTypeMapping xmlTypeMapping = ((XmlMapping)obj) as XmlTypeMapping;
					if (xmlTypeMapping != null && (xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Class || xmlTypeMapping.TypeData.SchemaType == SchemaTypes.Enum))
					{
						this.WriteMetCall("AddReadCallback", new string[]
						{
							this.GetLiteral(xmlTypeMapping.XmlType),
							this.GetLiteral(xmlTypeMapping.Namespace),
							this.GetTypeOf(xmlTypeMapping.TypeData.Type),
							"new XmlSerializationReadCallback (" + this.GetReadObjectName(xmlTypeMapping) + ")"
						});
					}
				}
			}
			this.WriteLineUni("}");
			this.WriteLine("");
			this.WriteLine("protected override void InitIDs ()");
			this.WriteLine("{");
			this.WriteLine("}");
			this.WriteLine("");
		}

		// Token: 0x06001BA2 RID: 7074 RVA: 0x000A0548 File Offset: 0x0009E748
		private void GenerateFixupCallbacks()
		{
			foreach (object obj in this._fixupCallbacks)
			{
				XmlMapping xmlMapping = (XmlMapping)obj;
				bool flag = xmlMapping is XmlMembersMapping;
				string text = (!flag) ? ((XmlTypeMapping)xmlMapping).TypeData.CSharpFullName : "object[]";
				this.WriteLine("void " + this.GetFixupCallbackName(xmlMapping) + " (object obfixup)");
				this.WriteLineInd("{");
				this.WriteLine("Fixup fixup = (Fixup)obfixup;");
				this.WriteLine(text + " source = (" + text + ") fixup.Source;");
				this.WriteLine("string[] ids = fixup.Ids;");
				this.WriteLine("");
				ICollection elementMembers = ((ClassMap)xmlMapping.ObjectMap).ElementMembers;
				if (elementMembers != null)
				{
					foreach (object obj2 in elementMembers)
					{
						XmlTypeMapMember xmlTypeMapMember = (XmlTypeMapMember)obj2;
						this.WriteLineInd("if (ids[" + xmlTypeMapMember.Index + "] != null)");
						string text2 = "GetTarget(ids[" + xmlTypeMapMember.Index + "])";
						if (!flag)
						{
							text2 = this.GetCast(xmlTypeMapMember.TypeData, text2);
						}
						this.GenerateSetMemberValue(xmlTypeMapMember, "source", text2, flag);
						this.Unindent();
					}
				}
				this.WriteLineUni("}");
				this.WriteLine("");
			}
		}

		// Token: 0x06001BA3 RID: 7075 RVA: 0x000A071C File Offset: 0x0009E91C
		private string GetReadXmlNode(TypeData type, bool wrapped)
		{
			if (type.Type == typeof(XmlDocument))
			{
				return this.GetCast(type, TypeTranslator.GetTypeData(typeof(XmlDocument)), "ReadXmlDocument (" + this.GetLiteral(wrapped) + ")");
			}
			return this.GetCast(type, TypeTranslator.GetTypeData(typeof(XmlNode)), "ReadXmlNode (" + this.GetLiteral(wrapped) + ")");
		}

		// Token: 0x06001BA4 RID: 7076 RVA: 0x000A07A3 File Offset: 0x0009E9A3
		private void InitHooks()
		{
			this._hookContexts = new Stack();
			this._hookOpenHooks = new Stack();
			this._hookVariables = new Hashtable();
		}

		// Token: 0x06001BA5 RID: 7077 RVA: 0x000A07C6 File Offset: 0x0009E9C6
		private void PushHookContext()
		{
			this._hookContexts.Push(this._hookVariables);
			this._hookVariables = (Hashtable)this._hookVariables.Clone();
		}

		// Token: 0x06001BA6 RID: 7078 RVA: 0x000A07EF File Offset: 0x0009E9EF
		private void PopHookContext()
		{
			this._hookVariables = (Hashtable)this._hookContexts.Pop();
		}

		// Token: 0x06001BA7 RID: 7079 RVA: 0x000A0807 File Offset: 0x0009EA07
		private void SetHookVar(string var, string value)
		{
			this._hookVariables[var] = value;
		}

		// Token: 0x06001BA8 RID: 7080 RVA: 0x000A0816 File Offset: 0x0009EA16
		private bool GenerateReadHook(HookType hookType, Type type)
		{
			return this.GenerateHook(hookType, XmlMappingAccess.Read, type, null);
		}

		// Token: 0x06001BA9 RID: 7081 RVA: 0x000A0822 File Offset: 0x0009EA22
		private bool GenerateWriteHook(HookType hookType, Type type)
		{
			return this.GenerateHook(hookType, XmlMappingAccess.Write, type, null);
		}

		// Token: 0x06001BAA RID: 7082 RVA: 0x000A082E File Offset: 0x0009EA2E
		private bool GenerateWriteMemberHook(Type type, XmlTypeMapMember member)
		{
			this.SetHookVar("$MEMBER", member.Name);
			return this.GenerateHook(HookType.member, XmlMappingAccess.Write, type, member.Name);
		}

		// Token: 0x06001BAB RID: 7083 RVA: 0x000A0850 File Offset: 0x0009EA50
		private bool GenerateReadMemberHook(Type type, XmlTypeMapMember member)
		{
			this.SetHookVar("$MEMBER", member.Name);
			return this.GenerateHook(HookType.member, XmlMappingAccess.Read, type, member.Name);
		}

		// Token: 0x06001BAC RID: 7084 RVA: 0x000A0872 File Offset: 0x0009EA72
		private bool GenerateReadArrayMemberHook(Type type, XmlTypeMapMember member, string index)
		{
			this.SetHookVar("$INDEX", index);
			return this.GenerateReadMemberHook(type, member);
		}

		// Token: 0x06001BAD RID: 7085 RVA: 0x000A0888 File Offset: 0x0009EA88
		private bool MemberHasReadReplaceHook(Type type, XmlTypeMapMember member)
		{
			return this._config != null && this._config.GetHooks(HookType.member, XmlMappingAccess.Read, HookAction.Replace, type, member.Name).Count > 0;
		}

		// Token: 0x06001BAE RID: 7086 RVA: 0x000A08B4 File Offset: 0x0009EAB4
		private bool GenerateHook(HookType hookType, XmlMappingAccess dir, Type type, string member)
		{
			this.GenerateHooks(hookType, dir, type, null, HookAction.InsertBefore);
			if (this.GenerateHooks(hookType, dir, type, null, HookAction.Replace))
			{
				this.GenerateHooks(hookType, dir, type, null, HookAction.InsertAfter);
				return true;
			}
			SerializationCodeGenerator.HookInfo hookInfo = new SerializationCodeGenerator.HookInfo();
			hookInfo.HookType = hookType;
			hookInfo.Type = type;
			hookInfo.Member = member;
			hookInfo.Direction = dir;
			this._hookOpenHooks.Push(hookInfo);
			return false;
		}

		// Token: 0x06001BAF RID: 7087 RVA: 0x000A0918 File Offset: 0x0009EB18
		private void GenerateEndHook()
		{
			SerializationCodeGenerator.HookInfo hookInfo = (SerializationCodeGenerator.HookInfo)this._hookOpenHooks.Pop();
			this.GenerateHooks(hookInfo.HookType, hookInfo.Direction, hookInfo.Type, hookInfo.Member, HookAction.InsertAfter);
		}

		// Token: 0x06001BB0 RID: 7088 RVA: 0x000A0958 File Offset: 0x0009EB58
		private bool GenerateHooks(HookType hookType, XmlMappingAccess dir, Type type, string member, HookAction action)
		{
			if (this._config == null)
			{
				return false;
			}
			ArrayList hooks = this._config.GetHooks(hookType, dir, action, type, null);
			if (hooks.Count == 0)
			{
				return false;
			}
			foreach (object obj in hooks)
			{
				string text = ((Hook)obj).GetCode(action);
				foreach (object obj2 in this._hookVariables)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj2;
					text = text.Replace((string)dictionaryEntry.Key, (string)dictionaryEntry.Value);
				}
				this.WriteMultilineCode(text);
			}
			return true;
		}

		// Token: 0x06001BB1 RID: 7089 RVA: 0x000A0A44 File Offset: 0x0009EC44
		private string GetRootTypeName()
		{
			if (this._typeMap is XmlTypeMapping)
			{
				return this.GetTypeFullName(((XmlTypeMapping)this._typeMap).TypeData);
			}
			return "object[]";
		}

		// Token: 0x06001BB2 RID: 7090 RVA: 0x000A0A70 File Offset: 0x0009EC70
		private string GetNumTempVar()
		{
			object arg = "n";
			int tempVarId = this._tempVarId;
			this._tempVarId = tempVarId + 1;
			return arg + tempVarId;
		}

		// Token: 0x06001BB3 RID: 7091 RVA: 0x000A0AA0 File Offset: 0x0009ECA0
		private string GetObTempVar()
		{
			object arg = "o";
			int tempVarId = this._tempVarId;
			this._tempVarId = tempVarId + 1;
			return arg + tempVarId;
		}

		// Token: 0x06001BB4 RID: 7092 RVA: 0x000A0AD0 File Offset: 0x0009ECD0
		private string GetStrTempVar()
		{
			object arg = "s";
			int tempVarId = this._tempVarId;
			this._tempVarId = tempVarId + 1;
			return arg + tempVarId;
		}

		// Token: 0x06001BB5 RID: 7093 RVA: 0x000A0B00 File Offset: 0x0009ED00
		private string GetBoolTempVar()
		{
			object arg = "b";
			int tempVarId = this._tempVarId;
			this._tempVarId = tempVarId + 1;
			return arg + tempVarId;
		}

		// Token: 0x06001BB6 RID: 7094 RVA: 0x000A0B30 File Offset: 0x0009ED30
		private string GetUniqueName(string uniqueGroup, object ob, string name)
		{
			name = CodeIdentifier.MakeValid(name.Replace("[]", "_array"));
			Hashtable hashtable = (Hashtable)this._uniqueNames[uniqueGroup];
			if (hashtable == null)
			{
				hashtable = new Hashtable();
				this._uniqueNames[uniqueGroup] = hashtable;
			}
			string text = (string)hashtable[ob];
			if (text != null)
			{
				return text;
			}
			using (IEnumerator enumerator = hashtable.Values.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if ((string)enumerator.Current == name)
					{
						object arg = name;
						int methodId = this._methodId;
						this._methodId = methodId + 1;
						return this.GetUniqueName(uniqueGroup, ob, arg + methodId);
					}
				}
			}
			hashtable[ob] = name;
			return name;
		}

		// Token: 0x06001BB7 RID: 7095 RVA: 0x000A0C14 File Offset: 0x0009EE14
		private void RegisterReferencingMap(XmlTypeMapping typeMap)
		{
			if (typeMap != null && !this._mapsToGenerate.Contains(typeMap))
			{
				this._mapsToGenerate.Add(typeMap);
			}
		}

		// Token: 0x06001BB8 RID: 7096 RVA: 0x000A0C34 File Offset: 0x0009EE34
		private string GetWriteObjectName(XmlTypeMapping typeMap)
		{
			if (!this._mapsToGenerate.Contains(typeMap))
			{
				this._mapsToGenerate.Add(typeMap);
			}
			return this.GetUniqueName("rw", typeMap, "WriteObject_" + typeMap.XmlType);
		}

		// Token: 0x06001BB9 RID: 7097 RVA: 0x000A0C6D File Offset: 0x0009EE6D
		private string GetReadObjectName(XmlTypeMapping typeMap)
		{
			if (!this._mapsToGenerate.Contains(typeMap))
			{
				this._mapsToGenerate.Add(typeMap);
			}
			return this.GetUniqueName("rr", typeMap, "ReadObject_" + typeMap.XmlType);
		}

		// Token: 0x06001BBA RID: 7098 RVA: 0x000A0CA6 File Offset: 0x0009EEA6
		private string GetGetEnumValueName(XmlTypeMapping typeMap)
		{
			if (!this._mapsToGenerate.Contains(typeMap))
			{
				this._mapsToGenerate.Add(typeMap);
			}
			return this.GetUniqueName("ge", typeMap, "GetEnumValue_" + typeMap.XmlType);
		}

		// Token: 0x06001BBB RID: 7099 RVA: 0x000A0CDF File Offset: 0x0009EEDF
		private string GetWriteObjectCallbackName(XmlTypeMapping typeMap)
		{
			if (!this._mapsToGenerate.Contains(typeMap))
			{
				this._mapsToGenerate.Add(typeMap);
			}
			return this.GetUniqueName("wc", typeMap, "WriteCallback_" + typeMap.XmlType);
		}

		// Token: 0x06001BBC RID: 7100 RVA: 0x000A0D18 File Offset: 0x0009EF18
		private string GetFixupCallbackName(XmlMapping typeMap)
		{
			if (!this._mapsToGenerate.Contains(typeMap))
			{
				this._mapsToGenerate.Add(typeMap);
			}
			if (typeMap is XmlTypeMapping)
			{
				return this.GetUniqueName("fc", typeMap, "FixupCallback_" + ((XmlTypeMapping)typeMap).XmlType);
			}
			return this.GetUniqueName("fc", typeMap, "FixupCallback__Message");
		}

		// Token: 0x06001BBD RID: 7101 RVA: 0x000A0D7B File Offset: 0x0009EF7B
		private string GetUniqueClassName(string s)
		{
			return this.classNames.AddUnique(CodeIdentifier.MakeValid(s), null);
		}

		// Token: 0x06001BBE RID: 7102 RVA: 0x000A0D90 File Offset: 0x0009EF90
		private string GetReadObjectCall(XmlTypeMapping typeMap, string isNullable, string checkType)
		{
			if (this._format == SerializationFormat.Literal)
			{
				return string.Concat(new string[]
				{
					this.GetReadObjectName(typeMap),
					" (",
					isNullable,
					", ",
					checkType,
					")"
				});
			}
			return this.GetCast(typeMap.TypeData, this.GetReadObjectName(typeMap) + " ()");
		}

		// Token: 0x06001BBF RID: 7103 RVA: 0x000A0DF9 File Offset: 0x0009EFF9
		private string GetFillListName(TypeData td)
		{
			if (!this._listsToFill.Contains(td))
			{
				this._listsToFill.Add(td);
			}
			return this.GetUniqueName("fl", td, "Fill_" + CodeIdentifier.MakeValid(td.CSharpName));
		}

		// Token: 0x06001BC0 RID: 7104 RVA: 0x000A0E37 File Offset: 0x0009F037
		private string GetCast(TypeData td, TypeData tdval, string val)
		{
			if (td.CSharpFullName == tdval.CSharpFullName)
			{
				return val;
			}
			return this.GetCast(td, val);
		}

		// Token: 0x06001BC1 RID: 7105 RVA: 0x000A0E56 File Offset: 0x0009F056
		private string GetCast(TypeData td, string val)
		{
			return string.Concat(new string[]
			{
				"((",
				this.GetTypeFullName(td),
				") ",
				val,
				")"
			});
		}

		// Token: 0x06001BC2 RID: 7106 RVA: 0x000A0E89 File Offset: 0x0009F089
		private string GetCast(Type td, string val)
		{
			return string.Concat(new string[]
			{
				"((",
				SerializationCodeGenerator.ToCSharpFullName(td),
				") ",
				val,
				")"
			});
		}

		// Token: 0x06001BC3 RID: 7107 RVA: 0x000A0EBB File Offset: 0x0009F0BB
		private string GetTypeOf(TypeData td)
		{
			return "typeof(" + this.GetTypeFullName(td) + ")";
		}

		// Token: 0x06001BC4 RID: 7108 RVA: 0x000A0ED3 File Offset: 0x0009F0D3
		private string GetTypeOf(Type td)
		{
			return "typeof(" + SerializationCodeGenerator.ToCSharpFullName(td) + ")";
		}

		// Token: 0x06001BC5 RID: 7109 RVA: 0x000A0EEA File Offset: 0x0009F0EA
		private string GetTypeFullName(TypeData td)
		{
			if (td.IsNullable && td.IsValueType)
			{
				return td.CSharpFullName + "?";
			}
			return td.CSharpFullName;
		}

		// Token: 0x06001BC6 RID: 7110 RVA: 0x000A0F14 File Offset: 0x0009F114
		private string GetLiteral(object ob)
		{
			if (ob == null)
			{
				return "null";
			}
			if (ob is string)
			{
				return "\"" + ob.ToString().Replace("\"", "\"\"") + "\"";
			}
			if (ob is char)
			{
				if ((char)ob != '\'')
				{
					return "'" + ob.ToString() + "'";
				}
				return "'\\''";
			}
			else
			{
				if (ob is DateTime)
				{
					return "new DateTime (" + ((DateTime)ob).Ticks + ")";
				}
				if (ob is DateTimeOffset)
				{
					return "new DateTimeOffset (" + ((DateTimeOffset)ob).Ticks + ")";
				}
				if (ob is TimeSpan)
				{
					return "new TimeSpan (" + ((TimeSpan)ob).Ticks + ")";
				}
				if (ob is bool)
				{
					if (!(bool)ob)
					{
						return "false";
					}
					return "true";
				}
				else
				{
					if (ob is XmlQualifiedName)
					{
						XmlQualifiedName xmlQualifiedName = (XmlQualifiedName)ob;
						return string.Concat(new string[]
						{
							"new XmlQualifiedName (",
							this.GetLiteral(xmlQualifiedName.Name),
							",",
							this.GetLiteral(xmlQualifiedName.Namespace),
							")"
						});
					}
					if (ob is Enum)
					{
						string value = SerializationCodeGenerator.ToCSharpFullName(ob.GetType());
						StringBuilder stringBuilder = new StringBuilder();
						string[] array = Enum.Format(ob.GetType(), ob, "g").Split(new char[]
						{
							','
						});
						for (int i = 0; i < array.Length; i++)
						{
							string text = array[i].Trim();
							if (text.Length != 0)
							{
								if (stringBuilder.Length > 0)
								{
									stringBuilder.Append(" | ");
								}
								stringBuilder.Append(value);
								stringBuilder.Append('.');
								stringBuilder.Append(text);
							}
						}
						return stringBuilder.ToString();
					}
					if (!(ob is IFormattable))
					{
						return ob.ToString();
					}
					return ((IFormattable)ob).ToString(null, CultureInfo.InvariantCulture);
				}
			}
		}

		// Token: 0x06001BC7 RID: 7111 RVA: 0x000A113A File Offset: 0x0009F33A
		private void WriteLineInd(string code)
		{
			this.WriteLine(code);
			this._indent++;
		}

		// Token: 0x06001BC8 RID: 7112 RVA: 0x000A1151 File Offset: 0x0009F351
		private void WriteLineUni(string code)
		{
			if (this._indent > 0)
			{
				this._indent--;
			}
			this.WriteLine(code);
		}

		// Token: 0x06001BC9 RID: 7113 RVA: 0x000A1171 File Offset: 0x0009F371
		private void Write(string code)
		{
			if (code.Length > 0)
			{
				this._writer.Write(new string('\t', this._indent));
			}
			this._writer.Write(code);
		}

		// Token: 0x06001BCA RID: 7114 RVA: 0x000A11A0 File Offset: 0x0009F3A0
		private void WriteUni(string code)
		{
			if (this._indent > 0)
			{
				this._indent--;
			}
			this._writer.Write(code);
			this._writer.WriteLine(string.Empty);
		}

		// Token: 0x06001BCB RID: 7115 RVA: 0x000A11D5 File Offset: 0x0009F3D5
		private void WriteLine(string code)
		{
			if (code.Length > 0)
			{
				this._writer.Write(new string('\t', this._indent));
			}
			this._writer.WriteLine(code);
		}

		// Token: 0x06001BCC RID: 7116 RVA: 0x000A1204 File Offset: 0x0009F404
		private void WriteMultilineCode(string code)
		{
			string str = new string('\t', this._indent);
			code = code.Replace("\r", "");
			code = code.Replace("\t", "");
			while (code.StartsWith("\n"))
			{
				code = code.Substring(1);
			}
			while (code.EndsWith("\n"))
			{
				code = code.Substring(0, code.Length - 1);
			}
			code = code.Replace("\n", "\n" + str);
			this.WriteLine(code);
		}

		// Token: 0x06001BCD RID: 7117 RVA: 0x000A129C File Offset: 0x0009F49C
		private string Params(params string[] pars)
		{
			string text = "";
			foreach (string str in pars)
			{
				if (text != "")
				{
					text += ", ";
				}
				text += str;
			}
			return text;
		}

		// Token: 0x06001BCE RID: 7118 RVA: 0x000A12E5 File Offset: 0x0009F4E5
		private void WriteMetCall(string method, params string[] pars)
		{
			this.WriteLine(method + " (" + this.Params(pars) + ");");
		}

		// Token: 0x06001BCF RID: 7119 RVA: 0x000A1304 File Offset: 0x0009F504
		private void Unindent()
		{
			this._indent--;
		}

		// Token: 0x040015C3 RID: 5571
		private XmlMapping _typeMap;

		// Token: 0x040015C4 RID: 5572
		private SerializationFormat _format;

		// Token: 0x040015C5 RID: 5573
		private TextWriter _writer;

		// Token: 0x040015C6 RID: 5574
		private int _tempVarId;

		// Token: 0x040015C7 RID: 5575
		private int _indent;

		// Token: 0x040015C8 RID: 5576
		private Hashtable _uniqueNames;

		// Token: 0x040015C9 RID: 5577
		private int _methodId;

		// Token: 0x040015CA RID: 5578
		private SerializerInfo _config;

		// Token: 0x040015CB RID: 5579
		private ArrayList _mapsToGenerate;

		// Token: 0x040015CC RID: 5580
		private ArrayList _fixupCallbacks;

		// Token: 0x040015CD RID: 5581
		private ArrayList _referencedTypes;

		// Token: 0x040015CE RID: 5582
		private GenerationResult[] _results;

		// Token: 0x040015CF RID: 5583
		private GenerationResult _result;

		// Token: 0x040015D0 RID: 5584
		private XmlMapping[] _xmlMaps;

		// Token: 0x040015D1 RID: 5585
		private CodeIdentifiers classNames;

		// Token: 0x040015D2 RID: 5586
		private ArrayList _listsToFill;

		// Token: 0x040015D3 RID: 5587
		private Hashtable _hookVariables;

		// Token: 0x040015D4 RID: 5588
		private Stack _hookContexts;

		// Token: 0x040015D5 RID: 5589
		private Stack _hookOpenHooks;

		// Token: 0x020002EA RID: 746
		private class HookInfo
		{
			// Token: 0x06001BD0 RID: 7120 RVA: 0x00002103 File Offset: 0x00000303
			public HookInfo()
			{
			}

			// Token: 0x040015D6 RID: 5590
			public HookType HookType;

			// Token: 0x040015D7 RID: 5591
			public Type Type;

			// Token: 0x040015D8 RID: 5592
			public string Member;

			// Token: 0x040015D9 RID: 5593
			public XmlMappingAccess Direction;
		}
	}
}
