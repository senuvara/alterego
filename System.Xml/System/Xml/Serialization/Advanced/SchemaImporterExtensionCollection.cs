﻿using System;
using System.Collections;
using Unity;

namespace System.Xml.Serialization.Advanced
{
	/// <summary>Represents a collection of <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> objects.</summary>
	// Token: 0x0200056B RID: 1387
	public class SchemaImporterExtensionCollection : CollectionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection" /> class. </summary>
		// Token: 0x0600344A RID: 13386 RVA: 0x00072668 File Offset: 0x00070868
		public SchemaImporterExtensionCollection()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets the <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection" /> at the specified index.</summary>
		/// <param name="index">The index of the item to find.</param>
		/// <returns>The <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection" /> at the specified index.</returns>
		// Token: 0x17000AEB RID: 2795
		public SchemaImporterExtension this[int index]
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
			set
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Adds the specified importer extension to the collection. The name parameter allows you to supply a custom name for the extension.</summary>
		/// <param name="name">A custom name for the extension.</param>
		/// <param name="type">The <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection" /> to add.</param>
		/// <returns>The index of the newly added item.</returns>
		/// <exception cref="T:System.ArgumentException">The value of type does not inherit from <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection" />.</exception>
		// Token: 0x0600344D RID: 13389 RVA: 0x0010F3C0 File Offset: 0x0010D5C0
		public int Add(string name, Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Adds the specified importer extension to the collection.</summary>
		/// <param name="extension">The <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection" /> to add.</param>
		/// <returns>The index of the added extension.</returns>
		// Token: 0x0600344E RID: 13390 RVA: 0x0010F3DC File Offset: 0x0010D5DC
		public int Add(SchemaImporterExtension extension)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Gets a value that indicates whether the specified importer extension exists in the collection.</summary>
		/// <param name="extension">The <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection" /> to search for.</param>
		/// <returns>
		///     <see langword="true" /> if the extension is found; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600344F RID: 13391 RVA: 0x0010F3F8 File Offset: 0x0010D5F8
		public bool Contains(SchemaImporterExtension extension)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Copies all the elements of the current <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection" /> to the specified array of <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> objects at the specified index. </summary>
		/// <param name="array">The <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> to copy the current collection to.</param>
		/// <param name="index">The zero-based index at which the collection is added.</param>
		// Token: 0x06003450 RID: 13392 RVA: 0x00072668 File Offset: 0x00070868
		public void CopyTo(SchemaImporterExtension[] array, int index)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Searches for the specified item and returns the zero-based index of the first occurrence within the collection.</summary>
		/// <param name="extension">The <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> to search for.</param>
		/// <returns>The index of the found item.</returns>
		// Token: 0x06003451 RID: 13393 RVA: 0x0010F414 File Offset: 0x0010D614
		public int IndexOf(SchemaImporterExtension extension)
		{
			ThrowStub.ThrowNotSupportedException();
			return 0;
		}

		/// <summary>Inserts the specified <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> into the collection at the specified index.</summary>
		/// <param name="index">The zero-base index at which the <paramref name="extension" /> should be inserted.</param>
		/// <param name="extension">The <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> to insert.</param>
		// Token: 0x06003452 RID: 13394 RVA: 0x00072668 File Offset: 0x00070868
		public void Insert(int index, SchemaImporterExtension extension)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" />, specified by name, from the collection.</summary>
		/// <param name="name">The name of the <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> to remove. The name is set using the <see cref="M:System.Xml.Serialization.Advanced.SchemaImporterExtensionCollection.Add(System.String,System.Type)" /> method.</param>
		// Token: 0x06003453 RID: 13395 RVA: 0x00072668 File Offset: 0x00070868
		public void Remove(string name)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Removes the specified <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> from the collection.</summary>
		/// <param name="extension">The <see cref="T:System.Xml.Serialization.Advanced.SchemaImporterExtension" /> to remove. </param>
		// Token: 0x06003454 RID: 13396 RVA: 0x00072668 File Offset: 0x00070868
		public void Remove(SchemaImporterExtension extension)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
