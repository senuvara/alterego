﻿using System;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Provides data for the <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownNode" /> event.</summary>
	// Token: 0x020002DE RID: 734
	public class XmlNodeEventArgs : EventArgs
	{
		// Token: 0x06001B32 RID: 6962 RVA: 0x00098CCE File Offset: 0x00096ECE
		internal XmlNodeEventArgs(XmlNode xmlNode, int lineNumber, int linePosition, object o)
		{
			this.o = o;
			this.xmlNode = xmlNode;
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		/// <summary>Gets the object being deserialized.</summary>
		/// <returns>The <see cref="T:System.Object" /> being deserialized.</returns>
		// Token: 0x17000530 RID: 1328
		// (get) Token: 0x06001B33 RID: 6963 RVA: 0x00098CF3 File Offset: 0x00096EF3
		public object ObjectBeingDeserialized
		{
			get
			{
				return this.o;
			}
		}

		/// <summary>Gets the type of the XML node being deserialized.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlNodeType" /> that represents the XML node being deserialized.</returns>
		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x06001B34 RID: 6964 RVA: 0x00098CFB File Offset: 0x00096EFB
		public XmlNodeType NodeType
		{
			get
			{
				return this.xmlNode.NodeType;
			}
		}

		/// <summary>Gets the name of the XML node being deserialized.</summary>
		/// <returns>The name of the node being deserialized.</returns>
		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x06001B35 RID: 6965 RVA: 0x00098D08 File Offset: 0x00096F08
		public string Name
		{
			get
			{
				return this.xmlNode.Name;
			}
		}

		/// <summary>Gets the XML local name of the XML node being deserialized.</summary>
		/// <returns>The XML local name of the node being deserialized.</returns>
		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x06001B36 RID: 6966 RVA: 0x00098D15 File Offset: 0x00096F15
		public string LocalName
		{
			get
			{
				return this.xmlNode.LocalName;
			}
		}

		/// <summary>Gets the namespace URI that is associated with the XML node being deserialized.</summary>
		/// <returns>The namespace URI that is associated with the XML node being deserialized.</returns>
		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x06001B37 RID: 6967 RVA: 0x00098D22 File Offset: 0x00096F22
		public string NamespaceURI
		{
			get
			{
				return this.xmlNode.NamespaceURI;
			}
		}

		/// <summary>Gets the text of the XML node being deserialized.</summary>
		/// <returns>The text of the XML node being deserialized.</returns>
		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x06001B38 RID: 6968 RVA: 0x00098D2F File Offset: 0x00096F2F
		public string Text
		{
			get
			{
				return this.xmlNode.Value;
			}
		}

		/// <summary>Gets the line number of the unknown XML node.</summary>
		/// <returns>The line number of the unknown XML node.</returns>
		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x06001B39 RID: 6969 RVA: 0x00098D3C File Offset: 0x00096F3C
		public int LineNumber
		{
			get
			{
				return this.lineNumber;
			}
		}

		/// <summary>Gets the position in the line of the unknown XML node.</summary>
		/// <returns>The position number of the unknown XML node.</returns>
		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x06001B3A RID: 6970 RVA: 0x00098D44 File Offset: 0x00096F44
		public int LinePosition
		{
			get
			{
				return this.linePosition;
			}
		}

		// Token: 0x06001B3B RID: 6971 RVA: 0x00072668 File Offset: 0x00070868
		internal XmlNodeEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040015A9 RID: 5545
		private object o;

		// Token: 0x040015AA RID: 5546
		private XmlNode xmlNode;

		// Token: 0x040015AB RID: 5547
		private int lineNumber;

		// Token: 0x040015AC RID: 5548
		private int linePosition;
	}
}
