﻿using System;
using System.Collections;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x020002D2 RID: 722
	internal class XmlFacetComparer : IComparer
	{
		// Token: 0x06001A9B RID: 6811 RVA: 0x00094EC0 File Offset: 0x000930C0
		public int Compare(object o1, object o2)
		{
			XmlSchemaFacet xmlSchemaFacet = (XmlSchemaFacet)o1;
			XmlSchemaFacet xmlSchemaFacet2 = (XmlSchemaFacet)o2;
			return string.Compare(xmlSchemaFacet.GetType().Name + ":" + xmlSchemaFacet.Value, xmlSchemaFacet2.GetType().Name + ":" + xmlSchemaFacet2.Value, StringComparison.Ordinal);
		}

		// Token: 0x06001A9C RID: 6812 RVA: 0x00002103 File Offset: 0x00000303
		public XmlFacetComparer()
		{
		}
	}
}
