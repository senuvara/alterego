﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Specifies that the target property, parameter, return value, or class member contains prefixes associated with namespaces that are used within an XML document.</summary>
	// Token: 0x0200031B RID: 795
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class XmlNamespaceDeclarationsAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlNamespaceDeclarationsAttribute" /> class.</summary>
		// Token: 0x06001D6A RID: 7530 RVA: 0x0000D3B3 File Offset: 0x0000B5B3
		public XmlNamespaceDeclarationsAttribute()
		{
		}
	}
}
