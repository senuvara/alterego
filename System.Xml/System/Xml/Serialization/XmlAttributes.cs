﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Represents a collection of attribute objects that control how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes and deserializes an object.</summary>
	// Token: 0x0200030C RID: 780
	public class XmlAttributes
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlAttributes" /> class.</summary>
		// Token: 0x06001CD1 RID: 7377 RVA: 0x000A4E5C File Offset: 0x000A305C
		public XmlAttributes()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlAttributes" /> class and customizes how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes and deserializes an object. </summary>
		/// <param name="provider">A class that can provide alternative implementations of attributes that control XML serialization.</param>
		// Token: 0x06001CD2 RID: 7378 RVA: 0x000A4E90 File Offset: 0x000A3090
		public XmlAttributes(ICustomAttributeProvider provider)
		{
			foreach (object obj in provider.GetCustomAttributes(false))
			{
				if (obj is XmlAnyAttributeAttribute)
				{
					this.xmlAnyAttribute = (XmlAnyAttributeAttribute)obj;
				}
				else if (obj is XmlAnyElementAttribute)
				{
					this.xmlAnyElements.Add((XmlAnyElementAttribute)obj);
				}
				else if (obj is XmlArrayAttribute)
				{
					this.xmlArray = (XmlArrayAttribute)obj;
				}
				else if (obj is XmlArrayItemAttribute)
				{
					this.xmlArrayItems.Add((XmlArrayItemAttribute)obj);
				}
				else if (obj is XmlAttributeAttribute)
				{
					this.xmlAttribute = (XmlAttributeAttribute)obj;
				}
				else if (obj is XmlChoiceIdentifierAttribute)
				{
					this.xmlChoiceIdentifier = (XmlChoiceIdentifierAttribute)obj;
				}
				else if (obj is DefaultValueAttribute)
				{
					this.xmlDefaultValue = ((DefaultValueAttribute)obj).Value;
				}
				else if (obj is XmlElementAttribute)
				{
					this.xmlElements.Add((XmlElementAttribute)obj);
				}
				else if (obj is XmlEnumAttribute)
				{
					this.xmlEnum = (XmlEnumAttribute)obj;
				}
				else if (obj is XmlIgnoreAttribute)
				{
					this.xmlIgnore = true;
				}
				else if (obj is XmlNamespaceDeclarationsAttribute)
				{
					this.xmlns = true;
				}
				else if (obj is XmlRootAttribute)
				{
					this.xmlRoot = (XmlRootAttribute)obj;
				}
				else if (obj is XmlTextAttribute)
				{
					this.xmlText = (XmlTextAttribute)obj;
				}
				else if (obj is XmlTypeAttribute)
				{
					this.xmlType = (XmlTypeAttribute)obj;
				}
			}
			if (this.xmlIgnore)
			{
				this.xmlAnyAttribute = null;
				this.xmlAnyElements.Clear();
				this.xmlArray = null;
				this.xmlArrayItems.Clear();
				this.xmlAttribute = null;
				this.xmlChoiceIdentifier = null;
				this.xmlDefaultValue = null;
				this.xmlElements.Clear();
				this.xmlEnum = null;
				this.xmlns = false;
				this.xmlRoot = null;
				this.xmlText = null;
				this.xmlType = null;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Serialization.XmlAnyAttributeAttribute" /> to override.</summary>
		/// <returns>The <see cref="T:System.Xml.Serialization.XmlAnyAttributeAttribute" /> to override.</returns>
		// Token: 0x17000587 RID: 1415
		// (get) Token: 0x06001CD3 RID: 7379 RVA: 0x000A50B2 File Offset: 0x000A32B2
		// (set) Token: 0x06001CD4 RID: 7380 RVA: 0x000A50BA File Offset: 0x000A32BA
		public XmlAnyAttributeAttribute XmlAnyAttribute
		{
			get
			{
				return this.xmlAnyAttribute;
			}
			set
			{
				this.xmlAnyAttribute = value;
			}
		}

		/// <summary>Gets the collection of <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> objects to override.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlAnyElementAttributes" /> object that represents the collection of <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> objects.</returns>
		// Token: 0x17000588 RID: 1416
		// (get) Token: 0x06001CD5 RID: 7381 RVA: 0x000A50C3 File Offset: 0x000A32C3
		public XmlAnyElementAttributes XmlAnyElements
		{
			get
			{
				return this.xmlAnyElements;
			}
		}

		/// <summary>Gets or sets an object that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a public field or read/write property that returns an array.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlArrayAttribute" /> that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a public field or read/write property that returns an array.</returns>
		// Token: 0x17000589 RID: 1417
		// (get) Token: 0x06001CD6 RID: 7382 RVA: 0x000A50CB File Offset: 0x000A32CB
		// (set) Token: 0x06001CD7 RID: 7383 RVA: 0x000A50D3 File Offset: 0x000A32D3
		public XmlArrayAttribute XmlArray
		{
			get
			{
				return this.xmlArray;
			}
			set
			{
				this.xmlArray = value;
			}
		}

		/// <summary>Gets or sets a collection of objects that specify how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes items inserted into an array returned by a public field or read/write property.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlArrayItemAttributes" /> object that contains a collection of <see cref="T:System.Xml.Serialization.XmlArrayItemAttribute" /> objects.</returns>
		// Token: 0x1700058A RID: 1418
		// (get) Token: 0x06001CD8 RID: 7384 RVA: 0x000A50DC File Offset: 0x000A32DC
		public XmlArrayItemAttributes XmlArrayItems
		{
			get
			{
				return this.xmlArrayItems;
			}
		}

		/// <summary>Gets or sets an object that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a public field or public read/write property as an XML attribute.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlAttributeAttribute" /> that controls the serialization of a public field or read/write property as an XML attribute.</returns>
		// Token: 0x1700058B RID: 1419
		// (get) Token: 0x06001CD9 RID: 7385 RVA: 0x000A50E4 File Offset: 0x000A32E4
		// (set) Token: 0x06001CDA RID: 7386 RVA: 0x000A50EC File Offset: 0x000A32EC
		public XmlAttributeAttribute XmlAttribute
		{
			get
			{
				return this.xmlAttribute;
			}
			set
			{
				this.xmlAttribute = value;
			}
		}

		/// <summary>Gets or sets an object that allows you to distinguish between a set of choices.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlChoiceIdentifierAttribute" /> that can be applied to a class member that is serialized as an <see langword="xsi:choice" /> element.</returns>
		// Token: 0x1700058C RID: 1420
		// (get) Token: 0x06001CDB RID: 7387 RVA: 0x000A50F5 File Offset: 0x000A32F5
		public XmlChoiceIdentifierAttribute XmlChoiceIdentifier
		{
			get
			{
				return this.xmlChoiceIdentifier;
			}
		}

		/// <summary>Gets or sets the default value of an XML element or attribute.</summary>
		/// <returns>An <see cref="T:System.Object" /> that represents the default value of an XML element or attribute.</returns>
		// Token: 0x1700058D RID: 1421
		// (get) Token: 0x06001CDC RID: 7388 RVA: 0x000A50FD File Offset: 0x000A32FD
		// (set) Token: 0x06001CDD RID: 7389 RVA: 0x000A5105 File Offset: 0x000A3305
		public object XmlDefaultValue
		{
			get
			{
				return this.xmlDefaultValue;
			}
			set
			{
				this.xmlDefaultValue = value;
			}
		}

		/// <summary>Gets a collection of objects that specify how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a public field or read/write property as an XML element.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlElementAttributes" /> that contains a collection of <see cref="T:System.Xml.Serialization.XmlElementAttribute" /> objects.</returns>
		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x06001CDE RID: 7390 RVA: 0x000A510E File Offset: 0x000A330E
		public XmlElementAttributes XmlElements
		{
			get
			{
				return this.xmlElements;
			}
		}

		/// <summary>Gets or sets an object that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes an enumeration member.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlEnumAttribute" /> that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes an enumeration member.</returns>
		// Token: 0x1700058F RID: 1423
		// (get) Token: 0x06001CDF RID: 7391 RVA: 0x000A5116 File Offset: 0x000A3316
		// (set) Token: 0x06001CE0 RID: 7392 RVA: 0x000A511E File Offset: 0x000A331E
		public XmlEnumAttribute XmlEnum
		{
			get
			{
				return this.xmlEnum;
			}
			set
			{
				this.xmlEnum = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether or not the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a public field or public read/write property.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Xml.Serialization.XmlSerializer" /> must not serialize the field or property; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000590 RID: 1424
		// (get) Token: 0x06001CE1 RID: 7393 RVA: 0x000A5127 File Offset: 0x000A3327
		// (set) Token: 0x06001CE2 RID: 7394 RVA: 0x000A512F File Offset: 0x000A332F
		public bool XmlIgnore
		{
			get
			{
				return this.xmlIgnore;
			}
			set
			{
				this.xmlIgnore = value;
			}
		}

		/// <summary>Gets or sets a value that specifies whether to keep all namespace declarations when an object containing a member that returns an <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> object is overridden.</summary>
		/// <returns>
		///     <see langword="true" /> if the namespace declarations should be kept; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000591 RID: 1425
		// (get) Token: 0x06001CE3 RID: 7395 RVA: 0x000A5138 File Offset: 0x000A3338
		// (set) Token: 0x06001CE4 RID: 7396 RVA: 0x000A5140 File Offset: 0x000A3340
		public bool Xmlns
		{
			get
			{
				return this.xmlns;
			}
			set
			{
				this.xmlns = value;
			}
		}

		/// <summary>Gets or sets an object that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a class as an XML root element.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlRootAttribute" /> that overrides a class attributed as an XML root element.</returns>
		// Token: 0x17000592 RID: 1426
		// (get) Token: 0x06001CE5 RID: 7397 RVA: 0x000A5149 File Offset: 0x000A3349
		// (set) Token: 0x06001CE6 RID: 7398 RVA: 0x000A5151 File Offset: 0x000A3351
		public XmlRootAttribute XmlRoot
		{
			get
			{
				return this.xmlRoot;
			}
			set
			{
				this.xmlRoot = value;
			}
		}

		/// <summary>Gets or sets an object that instructs the <see cref="T:System.Xml.Serialization.XmlSerializer" /> to serialize a public field or public read/write property as XML text.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlTextAttribute" /> that overrides the default serialization of a public property or field.</returns>
		// Token: 0x17000593 RID: 1427
		// (get) Token: 0x06001CE7 RID: 7399 RVA: 0x000A515A File Offset: 0x000A335A
		// (set) Token: 0x06001CE8 RID: 7400 RVA: 0x000A5162 File Offset: 0x000A3362
		public XmlTextAttribute XmlText
		{
			get
			{
				return this.xmlText;
			}
			set
			{
				this.xmlText = value;
			}
		}

		/// <summary>Gets or sets an object that specifies how the <see cref="T:System.Xml.Serialization.XmlSerializer" /> serializes a class to which the <see cref="T:System.Xml.Serialization.XmlTypeAttribute" /> has been applied.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlTypeAttribute" /> that overrides an <see cref="T:System.Xml.Serialization.XmlTypeAttribute" /> applied to a class declaration.</returns>
		// Token: 0x17000594 RID: 1428
		// (get) Token: 0x06001CE9 RID: 7401 RVA: 0x000A516B File Offset: 0x000A336B
		// (set) Token: 0x06001CEA RID: 7402 RVA: 0x000A5173 File Offset: 0x000A3373
		public XmlTypeAttribute XmlType
		{
			get
			{
				return this.xmlType;
			}
			set
			{
				this.xmlType = value;
			}
		}

		// Token: 0x06001CEB RID: 7403 RVA: 0x000A517C File Offset: 0x000A337C
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XA ");
			KeyHelper.AddField(sb, 1, this.xmlIgnore);
			KeyHelper.AddField(sb, 2, this.xmlns);
			KeyHelper.AddField(sb, 3, this.xmlAnyAttribute != null);
			this.xmlAnyElements.AddKeyHash(sb);
			this.xmlArrayItems.AddKeyHash(sb);
			this.xmlElements.AddKeyHash(sb);
			if (this.xmlArray != null)
			{
				this.xmlArray.AddKeyHash(sb);
			}
			if (this.xmlAttribute != null)
			{
				this.xmlAttribute.AddKeyHash(sb);
			}
			if (this.xmlDefaultValue == null)
			{
				sb.Append("n");
			}
			else if (!(this.xmlDefaultValue is DBNull))
			{
				string str = XmlCustomFormatter.ToXmlString(TypeTranslator.GetTypeData(this.xmlDefaultValue.GetType()), this.xmlDefaultValue);
				sb.Append("v" + str);
			}
			if (this.xmlEnum != null)
			{
				this.xmlEnum.AddKeyHash(sb);
			}
			if (this.xmlRoot != null)
			{
				this.xmlRoot.AddKeyHash(sb);
			}
			if (this.xmlText != null)
			{
				this.xmlText.AddKeyHash(sb);
			}
			if (this.xmlType != null)
			{
				this.xmlType.AddKeyHash(sb);
			}
			if (this.xmlChoiceIdentifier != null)
			{
				this.xmlChoiceIdentifier.AddKeyHash(sb);
			}
			sb.Append("|");
		}

		// Token: 0x17000595 RID: 1429
		// (get) Token: 0x06001CEC RID: 7404 RVA: 0x000A52CC File Offset: 0x000A34CC
		internal int? Order
		{
			get
			{
				int? result = null;
				if (this.XmlElements.Count > 0)
				{
					result = new int?(this.XmlElements.Order);
				}
				else if (this.XmlArray != null)
				{
					result = new int?(this.XmlArray.Order);
				}
				else if (this.XmlAnyElements.Count > 0)
				{
					result = new int?(this.XmlAnyElements.Order);
				}
				return result;
			}
		}

		// Token: 0x17000596 RID: 1430
		// (get) Token: 0x06001CED RID: 7405 RVA: 0x000A5340 File Offset: 0x000A3540
		internal int SortableOrder
		{
			get
			{
				if (this.Order == null)
				{
					return int.MinValue;
				}
				return this.Order.Value;
			}
		}

		// Token: 0x0400165B RID: 5723
		private XmlAnyAttributeAttribute xmlAnyAttribute;

		// Token: 0x0400165C RID: 5724
		private XmlAnyElementAttributes xmlAnyElements = new XmlAnyElementAttributes();

		// Token: 0x0400165D RID: 5725
		private XmlArrayAttribute xmlArray;

		// Token: 0x0400165E RID: 5726
		private XmlArrayItemAttributes xmlArrayItems = new XmlArrayItemAttributes();

		// Token: 0x0400165F RID: 5727
		private XmlAttributeAttribute xmlAttribute;

		// Token: 0x04001660 RID: 5728
		private XmlChoiceIdentifierAttribute xmlChoiceIdentifier;

		// Token: 0x04001661 RID: 5729
		private object xmlDefaultValue = DBNull.Value;

		// Token: 0x04001662 RID: 5730
		private XmlElementAttributes xmlElements = new XmlElementAttributes();

		// Token: 0x04001663 RID: 5731
		private XmlEnumAttribute xmlEnum;

		// Token: 0x04001664 RID: 5732
		private bool xmlIgnore;

		// Token: 0x04001665 RID: 5733
		private bool xmlns;

		// Token: 0x04001666 RID: 5734
		private XmlRootAttribute xmlRoot;

		// Token: 0x04001667 RID: 5735
		private XmlTextAttribute xmlText;

		// Token: 0x04001668 RID: 5736
		private XmlTypeAttribute xmlType;
	}
}
