﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	/// <summary>Defines the reader, writer, and methods for pre-generated, typed serializers.</summary>
	// Token: 0x0200033C RID: 828
	public abstract class XmlSerializerImplementation
	{
		/// <summary>Gets the XML reader object that is used by the serializer.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlSerializationReader" /> that is used to read an XML document or data stream.</returns>
		// Token: 0x170005E9 RID: 1513
		// (get) Token: 0x06001FCA RID: 8138 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual XmlSerializationReader Reader
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Gets the collection of methods that is used to read a data stream.</summary>
		/// <returns>A <see cref="T:System.Collections.Hashtable" /> that contains the methods.</returns>
		// Token: 0x170005EA RID: 1514
		// (get) Token: 0x06001FCB RID: 8139 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual Hashtable ReadMethods
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Gets the collection of typed serializers that is found in the assembly.</summary>
		/// <returns>A <see cref="T:System.Collections.Hashtable" /> that contains the typed serializers.</returns>
		// Token: 0x170005EB RID: 1515
		// (get) Token: 0x06001FCC RID: 8140 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual Hashtable TypedSerializers
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Get the collection of methods that is used to write to a data stream.</summary>
		/// <returns>A <see cref="T:System.Collections.Hashtable" /> that contains the methods.</returns>
		// Token: 0x170005EC RID: 1516
		// (get) Token: 0x06001FCD RID: 8141 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual Hashtable WriteMethods
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Gets the XML writer object for the serializer.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlSerializationWriter" /> that is used to write to an XML data stream or document.</returns>
		// Token: 0x170005ED RID: 1517
		// (get) Token: 0x06001FCE RID: 8142 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual XmlSerializationWriter Writer
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Gets a value that determines whether a type can be serialized.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> to be serialized.</param>
		/// <returns>
		///     <see langword="true" /> if the type can be serialized; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001FCF RID: 8143 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual bool CanSerialize(Type type)
		{
			throw new NotSupportedException();
		}

		/// <summary>Returns a serializer for the specified type.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> to be serialized.</param>
		/// <returns>An instance of a type derived from the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class. </returns>
		// Token: 0x06001FD0 RID: 8144 RVA: 0x00010D4A File Offset: 0x0000EF4A
		public virtual XmlSerializer GetSerializer(Type type)
		{
			throw new NotSupportedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializerImplementation" /> class. </summary>
		// Token: 0x06001FD1 RID: 8145 RVA: 0x00002103 File Offset: 0x00000303
		protected XmlSerializerImplementation()
		{
		}
	}
}
