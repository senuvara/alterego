﻿using System;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x02000343 RID: 835
	internal class XmlTypeMapMemberAttribute : XmlTypeMapMember
	{
		// Token: 0x06002033 RID: 8243 RVA: 0x000B58AF File Offset: 0x000B3AAF
		public XmlTypeMapMemberAttribute()
		{
		}

		// Token: 0x17000616 RID: 1558
		// (get) Token: 0x06002034 RID: 8244 RVA: 0x000B58C2 File Offset: 0x000B3AC2
		// (set) Token: 0x06002035 RID: 8245 RVA: 0x000B58CA File Offset: 0x000B3ACA
		public string AttributeName
		{
			get
			{
				return this._attributeName;
			}
			set
			{
				this._attributeName = value;
			}
		}

		// Token: 0x17000617 RID: 1559
		// (get) Token: 0x06002036 RID: 8246 RVA: 0x000B58D3 File Offset: 0x000B3AD3
		// (set) Token: 0x06002037 RID: 8247 RVA: 0x000B58DB File Offset: 0x000B3ADB
		public string Namespace
		{
			get
			{
				return this._namespace;
			}
			set
			{
				this._namespace = value;
			}
		}

		// Token: 0x17000618 RID: 1560
		// (get) Token: 0x06002038 RID: 8248 RVA: 0x000B58E4 File Offset: 0x000B3AE4
		public string DataTypeNamespace
		{
			get
			{
				if (this._mappedType == null)
				{
					return "http://www.w3.org/2001/XMLSchema";
				}
				return this._mappedType.Namespace;
			}
		}

		// Token: 0x17000619 RID: 1561
		// (get) Token: 0x06002039 RID: 8249 RVA: 0x000B58FF File Offset: 0x000B3AFF
		// (set) Token: 0x0600203A RID: 8250 RVA: 0x000B5907 File Offset: 0x000B3B07
		public XmlSchemaForm Form
		{
			get
			{
				return this._form;
			}
			set
			{
				this._form = value;
			}
		}

		// Token: 0x1700061A RID: 1562
		// (get) Token: 0x0600203B RID: 8251 RVA: 0x000B5910 File Offset: 0x000B3B10
		// (set) Token: 0x0600203C RID: 8252 RVA: 0x000B5918 File Offset: 0x000B3B18
		public XmlTypeMapping MappedType
		{
			get
			{
				return this._mappedType;
			}
			set
			{
				this._mappedType = value;
			}
		}

		// Token: 0x04001752 RID: 5970
		private string _attributeName;

		// Token: 0x04001753 RID: 5971
		private string _namespace = "";

		// Token: 0x04001754 RID: 5972
		private XmlSchemaForm _form;

		// Token: 0x04001755 RID: 5973
		private XmlTypeMapping _mappedType;
	}
}
