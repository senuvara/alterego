﻿using System;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Provides data for the <see cref="E:System.Xml.Serialization.XmlSerializer.UnknownAttribute" /> event.</summary>
	// Token: 0x020002DA RID: 730
	public class XmlAttributeEventArgs : EventArgs
	{
		// Token: 0x06001B1C RID: 6940 RVA: 0x00098C08 File Offset: 0x00096E08
		internal XmlAttributeEventArgs(XmlAttribute attr, int lineNumber, int linePosition, object o, string qnames)
		{
			this.attr = attr;
			this.o = o;
			this.qnames = qnames;
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		/// <summary>Gets the object being deserialized.</summary>
		/// <returns>The object being deserialized.</returns>
		// Token: 0x17000526 RID: 1318
		// (get) Token: 0x06001B1D RID: 6941 RVA: 0x00098C35 File Offset: 0x00096E35
		public object ObjectBeingDeserialized
		{
			get
			{
				return this.o;
			}
		}

		/// <summary>Gets an object that represents the unknown XML attribute.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlAttribute" /> that represents the unknown XML attribute.</returns>
		// Token: 0x17000527 RID: 1319
		// (get) Token: 0x06001B1E RID: 6942 RVA: 0x00098C3D File Offset: 0x00096E3D
		public XmlAttribute Attr
		{
			get
			{
				return this.attr;
			}
		}

		/// <summary>Gets the line number of the unknown XML attribute.</summary>
		/// <returns>The line number of the unknown XML attribute.</returns>
		// Token: 0x17000528 RID: 1320
		// (get) Token: 0x06001B1F RID: 6943 RVA: 0x00098C45 File Offset: 0x00096E45
		public int LineNumber
		{
			get
			{
				return this.lineNumber;
			}
		}

		/// <summary>Gets the position in the line of the unknown XML attribute.</summary>
		/// <returns>The position number of the unknown XML attribute.</returns>
		// Token: 0x17000529 RID: 1321
		// (get) Token: 0x06001B20 RID: 6944 RVA: 0x00098C4D File Offset: 0x00096E4D
		public int LinePosition
		{
			get
			{
				return this.linePosition;
			}
		}

		/// <summary>Gets a comma-delimited list of XML attribute names expected to be in an XML document instance.</summary>
		/// <returns>A comma-delimited list of XML attribute names. Each name is in the following format: <paramref name="namespace" />:<paramref name="name" />.</returns>
		// Token: 0x1700052A RID: 1322
		// (get) Token: 0x06001B21 RID: 6945 RVA: 0x00098C55 File Offset: 0x00096E55
		public string ExpectedAttributes
		{
			get
			{
				if (this.qnames != null)
				{
					return this.qnames;
				}
				return string.Empty;
			}
		}

		// Token: 0x06001B22 RID: 6946 RVA: 0x00072668 File Offset: 0x00070868
		internal XmlAttributeEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400159F RID: 5535
		private object o;

		// Token: 0x040015A0 RID: 5536
		private XmlAttribute attr;

		// Token: 0x040015A1 RID: 5537
		private string qnames;

		// Token: 0x040015A2 RID: 5538
		private int lineNumber;

		// Token: 0x040015A3 RID: 5539
		private int linePosition;
	}
}
