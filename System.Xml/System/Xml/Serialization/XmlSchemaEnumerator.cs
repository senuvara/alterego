﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	/// <summary>Enables iteration over a collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects. </summary>
	// Token: 0x020002D7 RID: 727
	public class XmlSchemaEnumerator : IEnumerator<XmlSchema>, IDisposable, IEnumerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSchemaEnumerator" /> class. </summary>
		/// <param name="list">The <see cref="T:System.Xml.Serialization.XmlSchemas" /> object you want to iterate over.</param>
		// Token: 0x06001B07 RID: 6919 RVA: 0x0009897F File Offset: 0x00096B7F
		public XmlSchemaEnumerator(XmlSchemas list)
		{
			this.list = list;
			this.idx = -1;
			this.end = list.Count - 1;
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Xml.Serialization.XmlSchemaEnumerator" />.</summary>
		// Token: 0x06001B08 RID: 6920 RVA: 0x000030EC File Offset: 0x000012EC
		public void Dispose()
		{
		}

		/// <summary>Advances the enumerator to the next item in the collection.</summary>
		/// <returns>
		///     <see langword="true" /> if the move is successful; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001B09 RID: 6921 RVA: 0x000989A3 File Offset: 0x00096BA3
		public bool MoveNext()
		{
			if (this.idx >= this.end)
			{
				return false;
			}
			this.idx++;
			return true;
		}

		/// <summary>Gets the current element in the collection.</summary>
		/// <returns>The current <see cref="T:System.Xml.Schema.XmlSchema" /> object in the collection.</returns>
		// Token: 0x17000521 RID: 1313
		// (get) Token: 0x06001B0A RID: 6922 RVA: 0x000989C4 File Offset: 0x00096BC4
		public XmlSchema Current
		{
			get
			{
				return this.list[this.idx];
			}
		}

		/// <summary>Gets the current element in the collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects.</summary>
		/// <returns>The current element in the collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects.</returns>
		// Token: 0x17000522 RID: 1314
		// (get) Token: 0x06001B0B RID: 6923 RVA: 0x000989C4 File Offset: 0x00096BC4
		object IEnumerator.Current
		{
			get
			{
				return this.list[this.idx];
			}
		}

		/// <summary>Sets the enumerator to its initial position, which is before the first element in the collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects.</summary>
		// Token: 0x06001B0C RID: 6924 RVA: 0x000989D7 File Offset: 0x00096BD7
		void IEnumerator.Reset()
		{
			this.idx = -1;
		}

		// Token: 0x0400159B RID: 5531
		private XmlSchemas list;

		// Token: 0x0400159C RID: 5532
		private int idx;

		// Token: 0x0400159D RID: 5533
		private int end;
	}
}
