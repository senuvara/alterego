﻿using System;
using System.Reflection;

namespace System.Xml.Serialization
{
	// Token: 0x02000342 RID: 834
	internal class XmlTypeMapMember
	{
		// Token: 0x06002011 RID: 8209 RVA: 0x000B5306 File Offset: 0x000B3506
		public XmlTypeMapMember()
		{
		}

		// Token: 0x17000609 RID: 1545
		// (get) Token: 0x06002012 RID: 8210 RVA: 0x000B5327 File Offset: 0x000B3527
		// (set) Token: 0x06002013 RID: 8211 RVA: 0x000B532F File Offset: 0x000B352F
		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		// Token: 0x1700060A RID: 1546
		// (get) Token: 0x06002014 RID: 8212 RVA: 0x000B5338 File Offset: 0x000B3538
		// (set) Token: 0x06002015 RID: 8213 RVA: 0x000B5340 File Offset: 0x000B3540
		public object DefaultValue
		{
			get
			{
				return this._defaultValue;
			}
			set
			{
				this._defaultValue = value;
			}
		}

		// Token: 0x1700060B RID: 1547
		// (get) Token: 0x06002017 RID: 8215 RVA: 0x000B5352 File Offset: 0x000B3552
		// (set) Token: 0x06002016 RID: 8214 RVA: 0x000B5349 File Offset: 0x000B3549
		public string Documentation
		{
			get
			{
				return this.documentation;
			}
			set
			{
				this.documentation = value;
			}
		}

		// Token: 0x06002018 RID: 8216 RVA: 0x000B535A File Offset: 0x000B355A
		public bool IsReadOnly(Type type)
		{
			if (this._member == null)
			{
				this.InitMember(type);
			}
			return this._member is PropertyInfo && !((PropertyInfo)this._member).CanWrite;
		}

		// Token: 0x06002019 RID: 8217 RVA: 0x000B5394 File Offset: 0x000B3594
		public static object GetValue(object ob, string name)
		{
			MemberInfo[] member = ob.GetType().GetMember(name, BindingFlags.Instance | BindingFlags.Public);
			if (member[0] is PropertyInfo)
			{
				return ((PropertyInfo)member[0]).GetValue(ob, null);
			}
			return ((FieldInfo)member[0]).GetValue(ob);
		}

		// Token: 0x0600201A RID: 8218 RVA: 0x000B53D8 File Offset: 0x000B35D8
		public object GetValue(object ob)
		{
			if (this._member == null)
			{
				this.InitMember(ob.GetType());
			}
			if (this._member is PropertyInfo)
			{
				return ((PropertyInfo)this._member).GetValue(ob, null);
			}
			return ((FieldInfo)this._member).GetValue(ob);
		}

		// Token: 0x0600201B RID: 8219 RVA: 0x000B5430 File Offset: 0x000B3630
		public void SetValue(object ob, object value)
		{
			if (this._member == null)
			{
				this.InitMember(ob.GetType());
			}
			this._typeData.ConvertForAssignment(ref value);
			if (this._member is PropertyInfo)
			{
				((PropertyInfo)this._member).SetValue(ob, value, null);
				return;
			}
			((FieldInfo)this._member).SetValue(ob, value);
		}

		// Token: 0x0600201C RID: 8220 RVA: 0x000B5498 File Offset: 0x000B3698
		public static void SetValue(object ob, string name, object value)
		{
			MemberInfo[] member = ob.GetType().GetMember(name, BindingFlags.Instance | BindingFlags.Public);
			if (member[0] is PropertyInfo)
			{
				((PropertyInfo)member[0]).SetValue(ob, value, null);
				return;
			}
			((FieldInfo)member[0]).SetValue(ob, value);
		}

		// Token: 0x0600201D RID: 8221 RVA: 0x000B54E0 File Offset: 0x000B36E0
		private void InitMember(Type type)
		{
			MemberInfo[] member = type.GetMember(this._name, BindingFlags.Instance | BindingFlags.Public);
			this._member = member[0];
			member = type.GetMember(this._name + "Specified", BindingFlags.Instance | BindingFlags.Public);
			if (member.Length != 0)
			{
				this._specifiedMember = member[0];
			}
			if (this._specifiedMember is PropertyInfo && !((PropertyInfo)this._specifiedMember).CanRead)
			{
				this._specifiedMember = null;
			}
			MethodInfo method = type.GetMethod("ShouldSerialize" + this._name, BindingFlags.Instance | BindingFlags.Public, null, Type.EmptyTypes, null);
			if (method != null && method.ReturnType == typeof(bool) && !method.IsGenericMethod)
			{
				this._shouldSerialize = method;
			}
		}

		// Token: 0x1700060C RID: 1548
		// (get) Token: 0x0600201E RID: 8222 RVA: 0x000B55A0 File Offset: 0x000B37A0
		// (set) Token: 0x0600201F RID: 8223 RVA: 0x000B55A8 File Offset: 0x000B37A8
		public TypeData TypeData
		{
			get
			{
				return this._typeData;
			}
			set
			{
				this._typeData = value;
			}
		}

		// Token: 0x1700060D RID: 1549
		// (get) Token: 0x06002020 RID: 8224 RVA: 0x000B55B1 File Offset: 0x000B37B1
		// (set) Token: 0x06002021 RID: 8225 RVA: 0x000B55B9 File Offset: 0x000B37B9
		public int Index
		{
			get
			{
				return this._index;
			}
			set
			{
				this._index = value;
			}
		}

		// Token: 0x1700060E RID: 1550
		// (get) Token: 0x06002022 RID: 8226 RVA: 0x000B55C2 File Offset: 0x000B37C2
		// (set) Token: 0x06002023 RID: 8227 RVA: 0x000B55CA File Offset: 0x000B37CA
		public int GlobalIndex
		{
			get
			{
				return this._globalIndex;
			}
			set
			{
				this._globalIndex = value;
			}
		}

		// Token: 0x1700060F RID: 1551
		// (get) Token: 0x06002024 RID: 8228 RVA: 0x000B55D3 File Offset: 0x000B37D3
		public int SpecifiedGlobalIndex
		{
			get
			{
				return this._specifiedGlobalIndex;
			}
		}

		// Token: 0x17000610 RID: 1552
		// (get) Token: 0x06002025 RID: 8229 RVA: 0x000B55DB File Offset: 0x000B37DB
		// (set) Token: 0x06002026 RID: 8230 RVA: 0x000B55E8 File Offset: 0x000B37E8
		public bool IsOptionalValueType
		{
			get
			{
				return (this._flags & 1) != 0;
			}
			set
			{
				this._flags = (value ? (this._flags | 1) : (this._flags & -2));
			}
		}

		// Token: 0x17000611 RID: 1553
		// (get) Token: 0x06002027 RID: 8231 RVA: 0x000B5606 File Offset: 0x000B3806
		// (set) Token: 0x06002028 RID: 8232 RVA: 0x000B5613 File Offset: 0x000B3813
		public bool IsReturnValue
		{
			get
			{
				return (this._flags & 2) != 0;
			}
			set
			{
				this._flags = (value ? (this._flags | 2) : (this._flags & -3));
			}
		}

		// Token: 0x17000612 RID: 1554
		// (get) Token: 0x06002029 RID: 8233 RVA: 0x000B5631 File Offset: 0x000B3831
		// (set) Token: 0x0600202A RID: 8234 RVA: 0x000B563E File Offset: 0x000B383E
		public bool Ignore
		{
			get
			{
				return (this._flags & 4) != 0;
			}
			set
			{
				this._flags = (value ? (this._flags | 4) : (this._flags & -5));
			}
		}

		// Token: 0x0600202B RID: 8235 RVA: 0x000B565C File Offset: 0x000B385C
		public void CheckOptionalValueType(Type type)
		{
			if (this._member == null)
			{
				this.InitMember(type);
			}
			this.IsOptionalValueType = (this._specifiedMember != null || this._shouldSerialize != null);
		}

		// Token: 0x0600202C RID: 8236 RVA: 0x000B5698 File Offset: 0x000B3898
		public void CheckOptionalValueType(XmlReflectionMember[] members)
		{
			for (int i = 0; i < members.Length; i++)
			{
				XmlReflectionMember xmlReflectionMember = members[i];
				if (xmlReflectionMember.MemberName == this.Name + "Specified" && xmlReflectionMember.MemberType == typeof(bool) && xmlReflectionMember.XmlAttributes.XmlIgnore)
				{
					this.IsOptionalValueType = true;
					this._specifiedGlobalIndex = i;
					return;
				}
			}
		}

		// Token: 0x17000613 RID: 1555
		// (get) Token: 0x0600202D RID: 8237 RVA: 0x000B5707 File Offset: 0x000B3907
		public bool HasSpecified
		{
			get
			{
				return this._specifiedMember != null;
			}
		}

		// Token: 0x17000614 RID: 1556
		// (get) Token: 0x0600202E RID: 8238 RVA: 0x000B5715 File Offset: 0x000B3915
		public bool HasShouldSerialize
		{
			get
			{
				return this._shouldSerialize != null;
			}
		}

		// Token: 0x0600202F RID: 8239 RVA: 0x000B5724 File Offset: 0x000B3924
		public bool GetValueSpecified(object ob)
		{
			if (this._specifiedGlobalIndex != -1)
			{
				object[] array = (object[])ob;
				return this._specifiedGlobalIndex < array.Length && (bool)array[this._specifiedGlobalIndex];
			}
			bool flag = true;
			if (this._specifiedMember != null)
			{
				if (this._specifiedMember is PropertyInfo)
				{
					flag = (bool)((PropertyInfo)this._specifiedMember).GetValue(ob, null);
				}
				else
				{
					flag = (bool)((FieldInfo)this._specifiedMember).GetValue(ob);
				}
			}
			if (this._shouldSerialize != null)
			{
				flag = (flag && (bool)this._shouldSerialize.Invoke(ob, new object[0]));
			}
			return flag;
		}

		// Token: 0x06002030 RID: 8240 RVA: 0x000B57D8 File Offset: 0x000B39D8
		public bool IsValueSpecifiedSettable()
		{
			if (this._specifiedMember is PropertyInfo)
			{
				return ((PropertyInfo)this._specifiedMember).CanWrite;
			}
			return this._specifiedMember is FieldInfo && !((FieldInfo)this._specifiedMember).IsInitOnly;
		}

		// Token: 0x06002031 RID: 8241 RVA: 0x000B5828 File Offset: 0x000B3A28
		public void SetValueSpecified(object ob, bool value)
		{
			if (this._specifiedGlobalIndex != -1)
			{
				((object[])ob)[this._specifiedGlobalIndex] = value;
				return;
			}
			if (!(this._specifiedMember is PropertyInfo))
			{
				if (this._specifiedMember is FieldInfo)
				{
					((FieldInfo)this._specifiedMember).SetValue(ob, value);
				}
				return;
			}
			if (!((PropertyInfo)this._specifiedMember).CanWrite)
			{
				return;
			}
			((PropertyInfo)this._specifiedMember).SetValue(ob, value, null);
		}

		// Token: 0x17000615 RID: 1557
		// (get) Token: 0x06002032 RID: 8242 RVA: 0x000020CD File Offset: 0x000002CD
		public virtual bool RequiresNullable
		{
			get
			{
				return false;
			}
		}

		// Token: 0x04001744 RID: 5956
		private string _name;

		// Token: 0x04001745 RID: 5957
		private int _index;

		// Token: 0x04001746 RID: 5958
		private int _globalIndex = -1;

		// Token: 0x04001747 RID: 5959
		private int _specifiedGlobalIndex = -1;

		// Token: 0x04001748 RID: 5960
		private TypeData _typeData;

		// Token: 0x04001749 RID: 5961
		private MemberInfo _member;

		// Token: 0x0400174A RID: 5962
		private MemberInfo _specifiedMember;

		// Token: 0x0400174B RID: 5963
		private MethodInfo _shouldSerialize;

		// Token: 0x0400174C RID: 5964
		private object _defaultValue = DBNull.Value;

		// Token: 0x0400174D RID: 5965
		private string documentation;

		// Token: 0x0400174E RID: 5966
		private int _flags;

		// Token: 0x0400174F RID: 5967
		private const int OPTIONAL = 1;

		// Token: 0x04001750 RID: 5968
		private const int RETURN_VALUE = 2;

		// Token: 0x04001751 RID: 5969
		private const int IGNORE = 4;
	}
}
