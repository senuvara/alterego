﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000345 RID: 837
	internal class XmlTypeMapMemberList : XmlTypeMapMemberElement
	{
		// Token: 0x17000620 RID: 1568
		// (get) Token: 0x06002049 RID: 8265 RVA: 0x000B5B38 File Offset: 0x000B3D38
		public XmlTypeMapping ListTypeMapping
		{
			get
			{
				return ((XmlTypeMapElementInfo)base.ElementInfo[0]).MappedType;
			}
		}

		// Token: 0x17000621 RID: 1569
		// (get) Token: 0x0600204A RID: 8266 RVA: 0x000B5B50 File Offset: 0x000B3D50
		public string ElementName
		{
			get
			{
				return ((XmlTypeMapElementInfo)base.ElementInfo[0]).ElementName;
			}
		}

		// Token: 0x17000622 RID: 1570
		// (get) Token: 0x0600204B RID: 8267 RVA: 0x000B5B68 File Offset: 0x000B3D68
		public string Namespace
		{
			get
			{
				return ((XmlTypeMapElementInfo)base.ElementInfo[0]).Namespace;
			}
		}

		// Token: 0x0600204C RID: 8268 RVA: 0x000B5B80 File Offset: 0x000B3D80
		public XmlTypeMapMemberList()
		{
		}
	}
}
