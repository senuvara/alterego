﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002F1 RID: 753
	[XmlType("hookType")]
	internal enum HookType
	{
		// Token: 0x04001600 RID: 5632
		attributes,
		// Token: 0x04001601 RID: 5633
		elements,
		// Token: 0x04001602 RID: 5634
		unknownAttribute,
		// Token: 0x04001603 RID: 5635
		unknownElement,
		// Token: 0x04001604 RID: 5636
		member,
		// Token: 0x04001605 RID: 5637
		type
	}
}
