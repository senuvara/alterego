﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x020002F4 RID: 756
	internal class SoapTypeSerializationSource : SerializationSource
	{
		// Token: 0x06001BE0 RID: 7136 RVA: 0x000A169C File Offset: 0x0009F89C
		public SoapTypeSerializationSource(Type type, SoapAttributeOverrides attributeOverrides, string namspace, Type[] includedTypes) : base(namspace, includedTypes)
		{
			if (attributeOverrides != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				attributeOverrides.AddKeyHash(stringBuilder);
				this.attributeOverridesHash = stringBuilder.ToString();
			}
			this.type = type;
		}

		// Token: 0x06001BE1 RID: 7137 RVA: 0x000A16D8 File Offset: 0x0009F8D8
		public override bool Equals(object o)
		{
			SoapTypeSerializationSource soapTypeSerializationSource = o as SoapTypeSerializationSource;
			return soapTypeSerializationSource != null && this.type.Equals(soapTypeSerializationSource.type) && !(this.attributeOverridesHash != soapTypeSerializationSource.attributeOverridesHash) && base.BaseEquals(soapTypeSerializationSource);
		}

		// Token: 0x06001BE2 RID: 7138 RVA: 0x000A1722 File Offset: 0x0009F922
		public override int GetHashCode()
		{
			return this.type.GetHashCode();
		}

		// Token: 0x0400160C RID: 5644
		private string attributeOverridesHash;

		// Token: 0x0400160D RID: 5645
		private Type type;
	}
}
