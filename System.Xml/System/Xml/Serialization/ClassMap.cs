﻿using System;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x0200034D RID: 845
	internal class ClassMap : ObjectMap
	{
		// Token: 0x0600207C RID: 8316 RVA: 0x000B61B0 File Offset: 0x000B43B0
		public void AddMember(XmlTypeMapMember member)
		{
			if (member.GlobalIndex == -1)
			{
				member.GlobalIndex = this._allMembers.Count;
			}
			this._allMembers.Add(member);
			if (!(member.DefaultValue is DBNull) && member.DefaultValue != null)
			{
				if (this._membersWithDefault == null)
				{
					this._membersWithDefault = new ArrayList();
				}
				this._membersWithDefault.Add(member);
			}
			if (member.IsReturnValue)
			{
				this._returnMember = member;
			}
			if (!(member is XmlTypeMapMemberAttribute))
			{
				if (member is XmlTypeMapMemberFlatList)
				{
					this.RegisterFlatList((XmlTypeMapMemberFlatList)member);
				}
				else if (member is XmlTypeMapMemberAnyElement)
				{
					XmlTypeMapMemberAnyElement xmlTypeMapMemberAnyElement = (XmlTypeMapMemberAnyElement)member;
					if (xmlTypeMapMemberAnyElement.IsDefaultAny)
					{
						this._defaultAnyElement = xmlTypeMapMemberAnyElement;
					}
					if (xmlTypeMapMemberAnyElement.TypeData.IsListType)
					{
						this.RegisterFlatList(xmlTypeMapMemberAnyElement);
					}
				}
				else
				{
					if (member is XmlTypeMapMemberAnyAttribute)
					{
						this._defaultAnyAttribute = (XmlTypeMapMemberAnyAttribute)member;
						return;
					}
					if (member is XmlTypeMapMemberNamespaces)
					{
						this._namespaceDeclarations = (XmlTypeMapMemberNamespaces)member;
						return;
					}
				}
				if (member is XmlTypeMapMemberElement && ((XmlTypeMapMemberElement)member).IsXmlTextCollector)
				{
					if (this._xmlTextCollector != null)
					{
						throw new InvalidOperationException("XmlTextAttribute can only be applied once in a class");
					}
					this._xmlTextCollector = member;
				}
				if (this._elementMembers == null)
				{
					this._elementMembers = new ArrayList();
					this._elements = new Hashtable();
				}
				member.Index = this._elementMembers.Count;
				this._elementMembers.Add(member);
				foreach (object obj in ((IEnumerable)((XmlTypeMapMemberElement)member).ElementInfo))
				{
					XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
					string key = this.BuildKey(xmlTypeMapElementInfo.ElementName, xmlTypeMapElementInfo.Namespace, xmlTypeMapElementInfo.ExplicitOrder);
					if (this._elements.ContainsKey(key))
					{
						throw new InvalidOperationException(string.Concat(new string[]
						{
							"The XML element named '",
							xmlTypeMapElementInfo.ElementName,
							"' from namespace '",
							xmlTypeMapElementInfo.Namespace,
							"' is already present in the current scope. Use XML attributes to specify another XML name or namespace for the element."
						}));
					}
					this._elements.Add(key, xmlTypeMapElementInfo);
				}
				if (member.TypeData.IsListType && member.TypeData.Type != null && !member.TypeData.Type.IsArray)
				{
					if (this._listMembers == null)
					{
						this._listMembers = new ArrayList();
					}
					this._listMembers.Add(member);
				}
				return;
			}
			XmlTypeMapMemberAttribute xmlTypeMapMemberAttribute = (XmlTypeMapMemberAttribute)member;
			if (this._attributeMembers == null)
			{
				this._attributeMembers = new Hashtable();
			}
			string key2 = this.BuildKey(xmlTypeMapMemberAttribute.AttributeName, xmlTypeMapMemberAttribute.Namespace, -1);
			if (this._attributeMembers.ContainsKey(key2))
			{
				throw new InvalidOperationException(string.Concat(new string[]
				{
					"The XML attribute named '",
					xmlTypeMapMemberAttribute.AttributeName,
					"' from namespace '",
					xmlTypeMapMemberAttribute.Namespace,
					"' is already present in the current scope. Use XML attributes to specify another XML name or namespace for the attribute."
				}));
			}
			member.Index = this._attributeMembers.Count;
			this._attributeMembers.Add(key2, member);
		}

		// Token: 0x0600207D RID: 8317 RVA: 0x000B64C8 File Offset: 0x000B46C8
		private void RegisterFlatList(XmlTypeMapMemberExpandable member)
		{
			if (this._flatLists == null)
			{
				this._flatLists = new ArrayList();
			}
			member.FlatArrayIndex = this._flatLists.Count;
			this._flatLists.Add(member);
		}

		// Token: 0x0600207E RID: 8318 RVA: 0x000B64FB File Offset: 0x000B46FB
		public XmlTypeMapMemberAttribute GetAttribute(string name, string ns)
		{
			if (this._attributeMembers == null)
			{
				return null;
			}
			return (XmlTypeMapMemberAttribute)this._attributeMembers[this.BuildKey(name, ns, -1)];
		}

		// Token: 0x0600207F RID: 8319 RVA: 0x000B6520 File Offset: 0x000B4720
		public XmlTypeMapElementInfo GetElement(string name, string ns, int minimalOrder)
		{
			if (this._elements == null)
			{
				return null;
			}
			XmlTypeMapElementInfo xmlTypeMapElementInfo = null;
			foreach (object obj in this._elements.Values)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo2 = (XmlTypeMapElementInfo)obj;
				if (xmlTypeMapElementInfo2.ElementName == name && xmlTypeMapElementInfo2.Namespace == ns && xmlTypeMapElementInfo2.ExplicitOrder >= minimalOrder && (xmlTypeMapElementInfo == null || xmlTypeMapElementInfo.ExplicitOrder > xmlTypeMapElementInfo2.ExplicitOrder))
				{
					xmlTypeMapElementInfo = xmlTypeMapElementInfo2;
				}
			}
			return xmlTypeMapElementInfo;
		}

		// Token: 0x06002080 RID: 8320 RVA: 0x000B65BC File Offset: 0x000B47BC
		public XmlTypeMapElementInfo GetElement(string name, string ns)
		{
			if (this._elements == null)
			{
				return null;
			}
			foreach (object obj in this._elements.Values)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
				if (xmlTypeMapElementInfo.ElementName == name && xmlTypeMapElementInfo.Namespace == ns)
				{
					return xmlTypeMapElementInfo;
				}
			}
			return null;
		}

		// Token: 0x06002081 RID: 8321 RVA: 0x000B6640 File Offset: 0x000B4840
		public XmlTypeMapElementInfo GetElement(int index)
		{
			if (this._elements == null)
			{
				return null;
			}
			if (this._elementsByIndex == null)
			{
				this._elementsByIndex = new XmlTypeMapElementInfo[this._elementMembers.Count];
				foreach (object obj in this._elementMembers)
				{
					XmlTypeMapMemberElement xmlTypeMapMemberElement = (XmlTypeMapMemberElement)obj;
					if (xmlTypeMapMemberElement.ElementInfo.Count != 1)
					{
						throw new InvalidOperationException("Read by order only possible for encoded/bare format");
					}
					this._elementsByIndex[xmlTypeMapMemberElement.Index] = (XmlTypeMapElementInfo)xmlTypeMapMemberElement.ElementInfo[0];
				}
			}
			if (index >= this._elementMembers.Count)
			{
				return null;
			}
			return this._elementsByIndex[index];
		}

		// Token: 0x06002082 RID: 8322 RVA: 0x000B670C File Offset: 0x000B490C
		private string BuildKey(string name, string ns, int explicitOrder)
		{
			if (this._ignoreMemberNamespace)
			{
				return name;
			}
			return name + " / " + ns + ((explicitOrder < 0) ? "" : ("/" + explicitOrder));
		}

		// Token: 0x1700063A RID: 1594
		// (get) Token: 0x06002083 RID: 8323 RVA: 0x000B673F File Offset: 0x000B493F
		public ICollection AllElementInfos
		{
			get
			{
				return this._elements.Values;
			}
		}

		// Token: 0x1700063B RID: 1595
		// (get) Token: 0x06002084 RID: 8324 RVA: 0x000B674C File Offset: 0x000B494C
		// (set) Token: 0x06002085 RID: 8325 RVA: 0x000B6754 File Offset: 0x000B4954
		public bool IgnoreMemberNamespace
		{
			get
			{
				return this._ignoreMemberNamespace;
			}
			set
			{
				this._ignoreMemberNamespace = value;
			}
		}

		// Token: 0x1700063C RID: 1596
		// (get) Token: 0x06002086 RID: 8326 RVA: 0x000B6760 File Offset: 0x000B4960
		public bool IsOrderDependentMap
		{
			get
			{
				if (this._isOrderDependentMap == null)
				{
					this._isOrderDependentMap = new bool?(false);
					using (IEnumerator enumerator = this._elements.Values.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							if (((XmlTypeMapElementInfo)enumerator.Current).ExplicitOrder >= 0)
							{
								this._isOrderDependentMap = new bool?(true);
								break;
							}
						}
					}
				}
				return this._isOrderDependentMap.Value;
			}
		}

		// Token: 0x06002087 RID: 8327 RVA: 0x000B67F0 File Offset: 0x000B49F0
		public XmlTypeMapMember FindMember(string name)
		{
			for (int i = 0; i < this._allMembers.Count; i++)
			{
				if (((XmlTypeMapMember)this._allMembers[i]).Name == name)
				{
					return (XmlTypeMapMember)this._allMembers[i];
				}
			}
			return null;
		}

		// Token: 0x1700063D RID: 1597
		// (get) Token: 0x06002088 RID: 8328 RVA: 0x000B6844 File Offset: 0x000B4A44
		public XmlTypeMapMemberAnyElement DefaultAnyElementMember
		{
			get
			{
				return this._defaultAnyElement;
			}
		}

		// Token: 0x1700063E RID: 1598
		// (get) Token: 0x06002089 RID: 8329 RVA: 0x000B684C File Offset: 0x000B4A4C
		public XmlTypeMapMemberAnyAttribute DefaultAnyAttributeMember
		{
			get
			{
				return this._defaultAnyAttribute;
			}
		}

		// Token: 0x1700063F RID: 1599
		// (get) Token: 0x0600208A RID: 8330 RVA: 0x000B6854 File Offset: 0x000B4A54
		public XmlTypeMapMemberNamespaces NamespaceDeclarations
		{
			get
			{
				return this._namespaceDeclarations;
			}
		}

		// Token: 0x17000640 RID: 1600
		// (get) Token: 0x0600208B RID: 8331 RVA: 0x000B685C File Offset: 0x000B4A5C
		public ICollection AttributeMembers
		{
			get
			{
				if (this._attributeMembers == null)
				{
					return null;
				}
				if (this._attributeMembersArray != null)
				{
					return this._attributeMembersArray;
				}
				this._attributeMembersArray = new XmlTypeMapMemberAttribute[this._attributeMembers.Count];
				foreach (object obj in this._attributeMembers.Values)
				{
					XmlTypeMapMemberAttribute xmlTypeMapMemberAttribute = (XmlTypeMapMemberAttribute)obj;
					this._attributeMembersArray[xmlTypeMapMemberAttribute.Index] = xmlTypeMapMemberAttribute;
				}
				return this._attributeMembersArray;
			}
		}

		// Token: 0x17000641 RID: 1601
		// (get) Token: 0x0600208C RID: 8332 RVA: 0x000B68F8 File Offset: 0x000B4AF8
		public ICollection ElementMembers
		{
			get
			{
				return this._elementMembers;
			}
		}

		// Token: 0x17000642 RID: 1602
		// (get) Token: 0x0600208D RID: 8333 RVA: 0x000B6900 File Offset: 0x000B4B00
		public ArrayList AllMembers
		{
			get
			{
				return this._allMembers;
			}
		}

		// Token: 0x17000643 RID: 1603
		// (get) Token: 0x0600208E RID: 8334 RVA: 0x000B6908 File Offset: 0x000B4B08
		public ArrayList FlatLists
		{
			get
			{
				return this._flatLists;
			}
		}

		// Token: 0x17000644 RID: 1604
		// (get) Token: 0x0600208F RID: 8335 RVA: 0x000B6910 File Offset: 0x000B4B10
		public ArrayList MembersWithDefault
		{
			get
			{
				return this._membersWithDefault;
			}
		}

		// Token: 0x17000645 RID: 1605
		// (get) Token: 0x06002090 RID: 8336 RVA: 0x000B6918 File Offset: 0x000B4B18
		public ArrayList ListMembers
		{
			get
			{
				return this._listMembers;
			}
		}

		// Token: 0x17000646 RID: 1606
		// (get) Token: 0x06002091 RID: 8337 RVA: 0x000B6920 File Offset: 0x000B4B20
		public XmlTypeMapMember XmlTextCollector
		{
			get
			{
				return this._xmlTextCollector;
			}
		}

		// Token: 0x17000647 RID: 1607
		// (get) Token: 0x06002092 RID: 8338 RVA: 0x000B6928 File Offset: 0x000B4B28
		public XmlTypeMapMember ReturnMember
		{
			get
			{
				return this._returnMember;
			}
		}

		// Token: 0x17000648 RID: 1608
		// (get) Token: 0x06002093 RID: 8339 RVA: 0x000B6930 File Offset: 0x000B4B30
		public XmlQualifiedName SimpleContentBaseType
		{
			get
			{
				if (!this._canBeSimpleType || this._elementMembers == null || this._elementMembers.Count != 1)
				{
					return null;
				}
				XmlTypeMapMemberElement xmlTypeMapMemberElement = (XmlTypeMapMemberElement)this._elementMembers[0];
				if (xmlTypeMapMemberElement.ElementInfo.Count != 1)
				{
					return null;
				}
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)xmlTypeMapMemberElement.ElementInfo[0];
				if (!xmlTypeMapElementInfo.IsTextElement)
				{
					return null;
				}
				if (xmlTypeMapMemberElement.TypeData.SchemaType == SchemaTypes.Primitive || xmlTypeMapMemberElement.TypeData.SchemaType == SchemaTypes.Enum)
				{
					return new XmlQualifiedName(xmlTypeMapElementInfo.TypeData.XmlType, xmlTypeMapElementInfo.DataTypeNamespace);
				}
				return null;
			}
		}

		// Token: 0x06002094 RID: 8340 RVA: 0x000B69CF File Offset: 0x000B4BCF
		public void SetCanBeSimpleType(bool can)
		{
			this._canBeSimpleType = can;
		}

		// Token: 0x17000649 RID: 1609
		// (get) Token: 0x06002095 RID: 8341 RVA: 0x000B69D8 File Offset: 0x000B4BD8
		public bool HasSimpleContent
		{
			get
			{
				return this.SimpleContentBaseType != null;
			}
		}

		// Token: 0x06002096 RID: 8342 RVA: 0x000B69E6 File Offset: 0x000B4BE6
		public ClassMap()
		{
		}

		// Token: 0x0400176A RID: 5994
		private Hashtable _elements = new Hashtable();

		// Token: 0x0400176B RID: 5995
		private ArrayList _elementMembers;

		// Token: 0x0400176C RID: 5996
		private Hashtable _attributeMembers;

		// Token: 0x0400176D RID: 5997
		private XmlTypeMapMemberAttribute[] _attributeMembersArray;

		// Token: 0x0400176E RID: 5998
		private XmlTypeMapElementInfo[] _elementsByIndex;

		// Token: 0x0400176F RID: 5999
		private ArrayList _flatLists;

		// Token: 0x04001770 RID: 6000
		private ArrayList _allMembers = new ArrayList();

		// Token: 0x04001771 RID: 6001
		private ArrayList _membersWithDefault;

		// Token: 0x04001772 RID: 6002
		private ArrayList _listMembers;

		// Token: 0x04001773 RID: 6003
		private XmlTypeMapMemberAnyElement _defaultAnyElement;

		// Token: 0x04001774 RID: 6004
		private XmlTypeMapMemberAnyAttribute _defaultAnyAttribute;

		// Token: 0x04001775 RID: 6005
		private XmlTypeMapMemberNamespaces _namespaceDeclarations;

		// Token: 0x04001776 RID: 6006
		private XmlTypeMapMember _xmlTextCollector;

		// Token: 0x04001777 RID: 6007
		private XmlTypeMapMember _returnMember;

		// Token: 0x04001778 RID: 6008
		private bool _ignoreMemberNamespace;

		// Token: 0x04001779 RID: 6009
		private bool _canBeSimpleType = true;

		// Token: 0x0400177A RID: 6010
		private bool? _isOrderDependentMap;
	}
}
