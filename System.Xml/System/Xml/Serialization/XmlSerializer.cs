﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Serializes and deserializes objects into and from XML documents. The <see cref="T:System.Xml.Serialization.XmlSerializer" /> enables you to control how objects are encoded into XML.</summary>
	// Token: 0x02000337 RID: 823
	public class XmlSerializer
	{
		// Token: 0x06001F7A RID: 8058 RVA: 0x000B45A0 File Offset: 0x000B27A0
		static XmlSerializer()
		{
			string text = null;
			XmlSerializer.generationThreshold = -1;
			XmlSerializer.backgroundGeneration = false;
			XmlSerializer.deleteTempFiles = (text == null || text == "no");
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class.</summary>
		// Token: 0x06001F7B RID: 8059 RVA: 0x000B45F7 File Offset: 0x000B27F7
		protected XmlSerializer()
		{
			this.customSerializer = true;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class that can serialize objects of the specified type into XML documents, and deserialize XML documents into objects of the specified type.</summary>
		/// <param name="type">The type of the object that this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can serialize. </param>
		// Token: 0x06001F7C RID: 8060 RVA: 0x000B4606 File Offset: 0x000B2806
		public XmlSerializer(Type type) : this(type, null, null, null, null)
		{
		}

		/// <summary>Initializes an instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class using an object that maps one type to another.</summary>
		/// <param name="xmlTypeMapping">An <see cref="T:System.Xml.Serialization.XmlTypeMapping" /> that maps one type to another. </param>
		// Token: 0x06001F7D RID: 8061 RVA: 0x000B4613 File Offset: 0x000B2813
		public XmlSerializer(XmlTypeMapping xmlTypeMapping)
		{
			this.typeMapping = xmlTypeMapping;
		}

		// Token: 0x06001F7E RID: 8062 RVA: 0x000B4622 File Offset: 0x000B2822
		internal XmlSerializer(XmlMapping mapping, XmlSerializer.SerializerData data)
		{
			this.typeMapping = mapping;
			this.serializerData = data;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class that can serialize objects of the specified type into XML documents, and deserialize XML documents into objects of the specified type. Specifies the default namespace for all the XML elements.</summary>
		/// <param name="type">The type of the object that this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can serialize. </param>
		/// <param name="defaultNamespace">The default namespace to use for all the XML elements. </param>
		// Token: 0x06001F7F RID: 8063 RVA: 0x000B4638 File Offset: 0x000B2838
		public XmlSerializer(Type type, string defaultNamespace) : this(type, null, null, null, defaultNamespace)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class that can serialize objects of the specified type into XML documents, and deserialize XML documents into object of a specified type. If a property or field returns an array, the <paramref name="extraTypes" /> parameter specifies objects that can be inserted into the array.</summary>
		/// <param name="type">The type of the object that this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can serialize. </param>
		/// <param name="extraTypes">A <see cref="T:System.Type" /> array of additional object types to serialize. </param>
		// Token: 0x06001F80 RID: 8064 RVA: 0x000B4645 File Offset: 0x000B2845
		public XmlSerializer(Type type, Type[] extraTypes) : this(type, null, extraTypes, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class that can serialize objects of type <see cref="T:System.Object" /> into XML document instances, and deserialize XML document instances into objects of type <see cref="T:System.Object" />. Each object to be serialized can itself contain instances of classes, which this overload overrides with other classes. This overload also specifies the default namespace for all the XML elements and the class to use as the XML root element.</summary>
		/// <param name="type">The type of the object that this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can serialize.</param>
		/// <param name="overrides">An <see cref="T:System.Xml.Serialization.XmlAttributeOverrides" /> that extends or overrides the behavior of the class specified in the <paramref name="type" /> parameter.</param>
		/// <param name="extraTypes">A <see cref="T:System.Type" /> array of additional object types to serialize.</param>
		/// <param name="root">An <see cref="T:System.Xml.Serialization.XmlRootAttribute" /> that defines the XML root element properties.</param>
		/// <param name="defaultNamespace">The default namespace of all XML elements in the XML document.</param>
		/// <param name="location">The location of the types.</param>
		// Token: 0x06001F81 RID: 8065 RVA: 0x000B4652 File Offset: 0x000B2852
		public XmlSerializer(Type type, XmlAttributeOverrides overrides, Type[] extraTypes, XmlRootAttribute root, string defaultNamespace, string location) : this(type, overrides, extraTypes, root, defaultNamespace, location, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class that can serialize objects of the specified type into XML documents, and deserialize XML documents into objects of the specified type. Each object to be serialized can itself contain instances of classes, which this overload can override with other classes.</summary>
		/// <param name="type">The type of the object to serialize. </param>
		/// <param name="overrides">An <see cref="T:System.Xml.Serialization.XmlAttributeOverrides" />. </param>
		// Token: 0x06001F82 RID: 8066 RVA: 0x000B4664 File Offset: 0x000B2864
		public XmlSerializer(Type type, XmlAttributeOverrides overrides) : this(type, overrides, null, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class that can serialize objects of the specified type into XML documents, and deserialize an XML document into object of the specified type. It also specifies the class to use as the XML root element.</summary>
		/// <param name="type">The type of the object that this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can serialize. </param>
		/// <param name="root">An <see cref="T:System.Xml.Serialization.XmlRootAttribute" /> that represents the XML root element. </param>
		// Token: 0x06001F83 RID: 8067 RVA: 0x000B4671 File Offset: 0x000B2871
		public XmlSerializer(Type type, XmlRootAttribute root) : this(type, null, null, root, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class that can serialize objects of type <see cref="T:System.Object" /> into XML document instances, and deserialize XML document instances into objects of type <see cref="T:System.Object" />. Each object to be serialized can itself contain instances of classes, which this overload overrides with other classes. This overload also specifies the default namespace for all the XML elements and the class to use as the XML root element.</summary>
		/// <param name="type">The type of the object that this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can serialize. </param>
		/// <param name="overrides">An <see cref="T:System.Xml.Serialization.XmlAttributeOverrides" /> that extends or overrides the behavior of the class specified in the <paramref name="type" /> parameter. </param>
		/// <param name="extraTypes">A <see cref="T:System.Type" /> array of additional object types to serialize. </param>
		/// <param name="root">An <see cref="T:System.Xml.Serialization.XmlRootAttribute" /> that defines the XML root element properties. </param>
		/// <param name="defaultNamespace">The default namespace of all XML elements in the XML document. </param>
		// Token: 0x06001F84 RID: 8068 RVA: 0x000B4680 File Offset: 0x000B2880
		public XmlSerializer(Type type, XmlAttributeOverrides overrides, Type[] extraTypes, XmlRootAttribute root, string defaultNamespace)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			XmlReflectionImporter xmlReflectionImporter = new XmlReflectionImporter(overrides, defaultNamespace);
			if (extraTypes != null)
			{
				foreach (Type type2 in extraTypes)
				{
					xmlReflectionImporter.IncludeType(type2);
				}
			}
			this.typeMapping = xmlReflectionImporter.ImportTypeMapping(type, root, defaultNamespace);
		}

		// Token: 0x170005E6 RID: 1510
		// (get) Token: 0x06001F85 RID: 8069 RVA: 0x000B46DF File Offset: 0x000B28DF
		internal XmlMapping Mapping
		{
			get
			{
				return this.typeMapping;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class that can serialize objects of the specified type into XML document instances, and deserialize XML document instances into objects of the specified type. This overload allows you to supply other types that can be encountered during a serialization or deserialization operation, as well as a default namespace for all XML elements, the class to use as the XML root element, its location, and credentials required for access.</summary>
		/// <param name="type">The type of the object that this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can serialize.</param>
		/// <param name="overrides">An <see cref="T:System.Xml.Serialization.XmlAttributeOverrides" /> that extends or overrides the behavior of the class specified in the <paramref name="type" /> parameter.</param>
		/// <param name="extraTypes">A <see cref="T:System.Type" /> array of additional object types to serialize.</param>
		/// <param name="root">An <see cref="T:System.Xml.Serialization.XmlRootAttribute" /> that defines the XML root element properties.</param>
		/// <param name="defaultNamespace">The default namespace of all XML elements in the XML document.</param>
		/// <param name="location">The location of the types.</param>
		/// <param name="evidence">An instance of the <see cref="T:System.Security.Policy.Evidence" /> class that contains credentials required to access types.</param>
		// Token: 0x06001F86 RID: 8070 RVA: 0x00002103 File Offset: 0x00000303
		[MonoTODO]
		public XmlSerializer(Type type, XmlAttributeOverrides overrides, Type[] extraTypes, XmlRootAttribute root, string defaultNamespace, string location, Evidence evidence)
		{
		}

		/// <summary>Occurs when the <see cref="T:System.Xml.Serialization.XmlSerializer" /> encounters an XML attribute of unknown type during deserialization.</summary>
		// Token: 0x1400000A RID: 10
		// (add) Token: 0x06001F87 RID: 8071 RVA: 0x000B46E7 File Offset: 0x000B28E7
		// (remove) Token: 0x06001F88 RID: 8072 RVA: 0x000B4700 File Offset: 0x000B2900
		public event XmlAttributeEventHandler UnknownAttribute
		{
			add
			{
				this.onUnknownAttribute = (XmlAttributeEventHandler)Delegate.Combine(this.onUnknownAttribute, value);
			}
			remove
			{
				this.onUnknownAttribute = (XmlAttributeEventHandler)Delegate.Remove(this.onUnknownAttribute, value);
			}
		}

		/// <summary>Occurs when the <see cref="T:System.Xml.Serialization.XmlSerializer" /> encounters an XML element of unknown type during deserialization.</summary>
		// Token: 0x1400000B RID: 11
		// (add) Token: 0x06001F89 RID: 8073 RVA: 0x000B4719 File Offset: 0x000B2919
		// (remove) Token: 0x06001F8A RID: 8074 RVA: 0x000B4732 File Offset: 0x000B2932
		public event XmlElementEventHandler UnknownElement
		{
			add
			{
				this.onUnknownElement = (XmlElementEventHandler)Delegate.Combine(this.onUnknownElement, value);
			}
			remove
			{
				this.onUnknownElement = (XmlElementEventHandler)Delegate.Remove(this.onUnknownElement, value);
			}
		}

		/// <summary>Occurs when the <see cref="T:System.Xml.Serialization.XmlSerializer" /> encounters an XML node of unknown type during deserialization.</summary>
		// Token: 0x1400000C RID: 12
		// (add) Token: 0x06001F8B RID: 8075 RVA: 0x000B474B File Offset: 0x000B294B
		// (remove) Token: 0x06001F8C RID: 8076 RVA: 0x000B4764 File Offset: 0x000B2964
		public event XmlNodeEventHandler UnknownNode
		{
			add
			{
				this.onUnknownNode = (XmlNodeEventHandler)Delegate.Combine(this.onUnknownNode, value);
			}
			remove
			{
				this.onUnknownNode = (XmlNodeEventHandler)Delegate.Remove(this.onUnknownNode, value);
			}
		}

		// Token: 0x06001F8D RID: 8077 RVA: 0x000B477D File Offset: 0x000B297D
		internal virtual void OnUnknownAttribute(XmlAttributeEventArgs e)
		{
			if (this.onUnknownAttribute != null)
			{
				this.onUnknownAttribute(this, e);
			}
		}

		// Token: 0x06001F8E RID: 8078 RVA: 0x000B4794 File Offset: 0x000B2994
		internal virtual void OnUnknownElement(XmlElementEventArgs e)
		{
			if (this.onUnknownElement != null)
			{
				this.onUnknownElement(this, e);
			}
		}

		// Token: 0x06001F8F RID: 8079 RVA: 0x000B47AB File Offset: 0x000B29AB
		internal virtual void OnUnknownNode(XmlNodeEventArgs e)
		{
			if (this.onUnknownNode != null)
			{
				this.onUnknownNode(this, e);
			}
		}

		// Token: 0x06001F90 RID: 8080 RVA: 0x000B47C2 File Offset: 0x000B29C2
		internal virtual void OnUnreferencedObject(UnreferencedObjectEventArgs e)
		{
			if (this.onUnreferencedObject != null)
			{
				this.onUnreferencedObject(this, e);
			}
		}

		/// <summary>Occurs during deserialization of a SOAP-encoded XML stream, when the <see cref="T:System.Xml.Serialization.XmlSerializer" /> encounters a recognized type that is not used or is unreferenced.</summary>
		// Token: 0x1400000D RID: 13
		// (add) Token: 0x06001F91 RID: 8081 RVA: 0x000B47D9 File Offset: 0x000B29D9
		// (remove) Token: 0x06001F92 RID: 8082 RVA: 0x000B47F2 File Offset: 0x000B29F2
		public event UnreferencedObjectEventHandler UnreferencedObject
		{
			add
			{
				this.onUnreferencedObject = (UnreferencedObjectEventHandler)Delegate.Combine(this.onUnreferencedObject, value);
			}
			remove
			{
				this.onUnreferencedObject = (UnreferencedObjectEventHandler)Delegate.Remove(this.onUnreferencedObject, value);
			}
		}

		/// <summary>Gets a value that indicates whether this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can deserialize a specified XML document.</summary>
		/// <param name="xmlReader">An <see cref="T:System.Xml.XmlReader" /> that points to the document to deserialize. </param>
		/// <returns>
		///     <see langword="true" /> if this <see cref="T:System.Xml.Serialization.XmlSerializer" /> can deserialize the object that the <see cref="T:System.Xml.XmlReader" /> points to; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001F93 RID: 8083 RVA: 0x000B480B File Offset: 0x000B2A0B
		public virtual bool CanDeserialize(XmlReader xmlReader)
		{
			xmlReader.MoveToContent();
			return this.typeMapping is XmlMembersMapping || ((XmlTypeMapping)this.typeMapping).ElementName == xmlReader.LocalName;
		}

		/// <summary>Returns an object used to read the XML document to be serialized.</summary>
		/// <returns>An <see cref="T:System.Xml.Serialization.XmlSerializationReader" /> used to read the XML document.</returns>
		/// <exception cref="T:System.NotImplementedException">Any attempt is made to access the method when the method is not overridden in a descendant class. </exception>
		// Token: 0x06001F94 RID: 8084 RVA: 0x0000A6CF File Offset: 0x000088CF
		protected virtual XmlSerializationReader CreateReader()
		{
			throw new NotImplementedException();
		}

		/// <summary>When overridden in a derived class, returns a writer used to serialize the object.</summary>
		/// <returns>An instance that implements the <see cref="T:System.Xml.Serialization.XmlSerializationWriter" /> class.</returns>
		/// <exception cref="T:System.NotImplementedException">Any attempt is made to access the method when the method is not overridden in a descendant class. </exception>
		// Token: 0x06001F95 RID: 8085 RVA: 0x0000A6CF File Offset: 0x000088CF
		protected virtual XmlSerializationWriter CreateWriter()
		{
			throw new NotImplementedException();
		}

		/// <summary>Deserializes the XML document contained by the specified <see cref="T:System.IO.Stream" />.</summary>
		/// <param name="stream">The <see cref="T:System.IO.Stream" /> that contains the XML document to deserialize. </param>
		/// <returns>The <see cref="T:System.Object" /> being deserialized.</returns>
		// Token: 0x06001F96 RID: 8086 RVA: 0x000B4840 File Offset: 0x000B2A40
		public object Deserialize(Stream stream)
		{
			return this.Deserialize(new XmlTextReader(stream)
			{
				Normalization = true,
				WhitespaceHandling = WhitespaceHandling.Significant
			});
		}

		/// <summary>Deserializes the XML document contained by the specified <see cref="T:System.IO.TextReader" />.</summary>
		/// <param name="textReader">The <see cref="T:System.IO.TextReader" /> that contains the XML document to deserialize. </param>
		/// <returns>The <see cref="T:System.Object" /> being deserialized.</returns>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during deserialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001F97 RID: 8087 RVA: 0x000B486C File Offset: 0x000B2A6C
		public object Deserialize(TextReader textReader)
		{
			return this.Deserialize(new XmlTextReader(textReader)
			{
				Normalization = true,
				WhitespaceHandling = WhitespaceHandling.Significant
			});
		}

		/// <summary>Deserializes the XML document contained by the specified <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="xmlReader">The <see cref="T:System.Xml.XmlReader" /> that contains the XML document to deserialize. </param>
		/// <returns>The <see cref="T:System.Object" /> being deserialized.</returns>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during deserialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001F98 RID: 8088 RVA: 0x000B4898 File Offset: 0x000B2A98
		public object Deserialize(XmlReader xmlReader)
		{
			XmlSerializationReader xmlSerializationReader;
			if (this.customSerializer)
			{
				xmlSerializationReader = this.CreateReader();
			}
			else
			{
				xmlSerializationReader = this.CreateReader(this.typeMapping);
			}
			xmlSerializationReader.Initialize(xmlReader, this);
			return this.Deserialize(xmlSerializationReader);
		}

		/// <summary>Deserializes the XML document contained by the specified <see cref="T:System.Xml.Serialization.XmlSerializationReader" />.</summary>
		/// <param name="reader">The <see cref="T:System.Xml.Serialization.XmlSerializationReader" /> that contains the XML document to deserialize. </param>
		/// <returns>The deserialized object.</returns>
		/// <exception cref="T:System.NotImplementedException">Any attempt is made to access the method when the method is not overridden in a descendant class. </exception>
		// Token: 0x06001F99 RID: 8089 RVA: 0x000B48D4 File Offset: 0x000B2AD4
		protected virtual object Deserialize(XmlSerializationReader reader)
		{
			if (this.customSerializer)
			{
				throw new NotImplementedException();
			}
			object result;
			try
			{
				if (reader is XmlSerializationReaderInterpreter)
				{
					result = ((XmlSerializationReaderInterpreter)reader).ReadRoot();
				}
				else
				{
					try
					{
						result = this.serializerData.ReaderMethod.Invoke(reader, null);
					}
					catch (TargetInvocationException ex)
					{
						throw ex.InnerException;
					}
				}
			}
			catch (Exception ex2)
			{
				if (ex2 is InvalidOperationException || ex2 is InvalidCastException)
				{
					throw new InvalidOperationException("There is an error in XML document.", ex2);
				}
				throw;
			}
			return result;
		}

		/// <summary>Returns an array of <see cref="T:System.Xml.Serialization.XmlSerializer" /> objects created from an array of <see cref="T:System.Xml.Serialization.XmlTypeMapping" /> objects.</summary>
		/// <param name="mappings">An array of <see cref="T:System.Xml.Serialization.XmlTypeMapping" /> that maps one type to another. </param>
		/// <returns>An array of <see cref="T:System.Xml.Serialization.XmlSerializer" /> objects.</returns>
		// Token: 0x06001F9A RID: 8090 RVA: 0x000B4960 File Offset: 0x000B2B60
		public static XmlSerializer[] FromMappings(XmlMapping[] mappings)
		{
			XmlSerializer[] array = new XmlSerializer[mappings.Length];
			XmlSerializer.SerializerData[] array2 = new XmlSerializer.SerializerData[mappings.Length];
			XmlSerializer.GenerationBatch generationBatch = new XmlSerializer.GenerationBatch();
			generationBatch.Maps = mappings;
			generationBatch.Datas = array2;
			for (int i = 0; i < mappings.Length; i++)
			{
				if (mappings[i] != null)
				{
					XmlSerializer.SerializerData serializerData = new XmlSerializer.SerializerData();
					serializerData.Batch = generationBatch;
					array[i] = new XmlSerializer(mappings[i], serializerData);
					array2[i] = serializerData;
				}
			}
			return array;
		}

		/// <summary>Returns an array of <see cref="T:System.Xml.Serialization.XmlSerializer" /> objects created from an array of types.</summary>
		/// <param name="types">An array of <see cref="T:System.Type" /> objects. </param>
		/// <returns>An array of <see cref="T:System.Xml.Serialization.XmlSerializer" /> objects.</returns>
		// Token: 0x06001F9B RID: 8091 RVA: 0x000B49C8 File Offset: 0x000B2BC8
		public static XmlSerializer[] FromTypes(Type[] types)
		{
			XmlSerializer[] array = new XmlSerializer[types.Length];
			for (int i = 0; i < types.Length; i++)
			{
				array[i] = new XmlSerializer(types[i]);
			}
			return array;
		}

		/// <summary>Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file using the specified <see cref="T:System.Xml.Serialization.XmlSerializationWriter" />.</summary>
		/// <param name="o">The <see cref="T:System.Object" /> to serialize. </param>
		/// <param name="writer">The <see cref="T:System.Xml.Serialization.XmlSerializationWriter" /> used to write the XML document. </param>
		/// <exception cref="T:System.NotImplementedException">Any attempt is made to access the method when the method is not overridden in a descendant class. </exception>
		// Token: 0x06001F9C RID: 8092 RVA: 0x000B49F8 File Offset: 0x000B2BF8
		protected virtual void Serialize(object o, XmlSerializationWriter writer)
		{
			if (this.customSerializer)
			{
				throw new NotImplementedException();
			}
			if (writer is XmlSerializationWriterInterpreter)
			{
				((XmlSerializationWriterInterpreter)writer).WriteRoot(o);
				return;
			}
			try
			{
				this.serializerData.WriterMethod.Invoke(writer, new object[]
				{
					o
				});
			}
			catch (TargetInvocationException ex)
			{
				throw ex.InnerException;
			}
		}

		/// <summary>Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file using the specified <see cref="T:System.IO.Stream" />.</summary>
		/// <param name="stream">The <see cref="T:System.IO.Stream" /> used to write the XML document. </param>
		/// <param name="o">The <see cref="T:System.Object" /> to serialize. </param>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during serialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001F9D RID: 8093 RVA: 0x000B4A60 File Offset: 0x000B2C60
		public void Serialize(Stream stream, object o)
		{
			this.Serialize(new XmlTextWriter(stream, XmlSerializer.DefaultEncoding)
			{
				Formatting = Formatting.Indented
			}, o, null);
		}

		/// <summary>Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file using the specified <see cref="T:System.IO.TextWriter" />.</summary>
		/// <param name="textWriter">The <see cref="T:System.IO.TextWriter" /> used to write the XML document. </param>
		/// <param name="o">The <see cref="T:System.Object" /> to serialize. </param>
		// Token: 0x06001F9E RID: 8094 RVA: 0x000B4A8C File Offset: 0x000B2C8C
		public void Serialize(TextWriter textWriter, object o)
		{
			this.Serialize(new XmlTextWriter(textWriter)
			{
				Formatting = Formatting.Indented
			}, o, null);
		}

		/// <summary>Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file using the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="xmlWriter">The <see cref="T:System.Xml.XmlWriter" /> used to write the XML document. </param>
		/// <param name="o">The <see cref="T:System.Object" /> to serialize. </param>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during serialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001F9F RID: 8095 RVA: 0x000B4AB0 File Offset: 0x000B2CB0
		public void Serialize(XmlWriter xmlWriter, object o)
		{
			this.Serialize(xmlWriter, o, null);
		}

		/// <summary>Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file using the specified <see cref="T:System.IO.Stream" />that references the specified namespaces.</summary>
		/// <param name="stream">The <see cref="T:System.IO.Stream" /> used to write the XML document. </param>
		/// <param name="o">The <see cref="T:System.Object" /> to serialize. </param>
		/// <param name="namespaces">The <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> referenced by the object. </param>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during serialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001FA0 RID: 8096 RVA: 0x000B4ABC File Offset: 0x000B2CBC
		public void Serialize(Stream stream, object o, XmlSerializerNamespaces namespaces)
		{
			this.Serialize(new XmlTextWriter(stream, XmlSerializer.DefaultEncoding)
			{
				Formatting = Formatting.Indented
			}, o, namespaces);
		}

		/// <summary>Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file using the specified <see cref="T:System.IO.TextWriter" /> and references the specified namespaces.</summary>
		/// <param name="textWriter">The <see cref="T:System.IO.TextWriter" /> used to write the XML document. </param>
		/// <param name="o">The <see cref="T:System.Object" /> to serialize. </param>
		/// <param name="namespaces">The <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> that contains namespaces for the generated XML document. </param>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during serialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001FA1 RID: 8097 RVA: 0x000B4AE8 File Offset: 0x000B2CE8
		public void Serialize(TextWriter textWriter, object o, XmlSerializerNamespaces namespaces)
		{
			XmlTextWriter xmlTextWriter = new XmlTextWriter(textWriter);
			xmlTextWriter.Formatting = Formatting.Indented;
			this.Serialize(xmlTextWriter, o, namespaces);
			xmlTextWriter.Flush();
		}

		/// <summary>Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file using the specified <see cref="T:System.Xml.XmlWriter" /> and references the specified namespaces.</summary>
		/// <param name="xmlWriter">The <see cref="T:System.Xml.XmlWriter" /> used to write the XML document. </param>
		/// <param name="o">The <see cref="T:System.Object" /> to serialize. </param>
		/// <param name="namespaces">The <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> referenced by the object. </param>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during serialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001FA2 RID: 8098 RVA: 0x000B4B14 File Offset: 0x000B2D14
		public void Serialize(XmlWriter xmlWriter, object o, XmlSerializerNamespaces namespaces)
		{
			try
			{
				XmlSerializationWriter xmlSerializationWriter;
				if (this.customSerializer)
				{
					xmlSerializationWriter = this.CreateWriter();
				}
				else
				{
					xmlSerializationWriter = this.CreateWriter(this.typeMapping);
				}
				if (namespaces == null || namespaces.Count == 0)
				{
					namespaces = new XmlSerializerNamespaces();
					namespaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
					namespaces.Add("xsd", "http://www.w3.org/2001/XMLSchema");
				}
				xmlSerializationWriter.Initialize(xmlWriter, namespaces);
				this.Serialize(o, xmlSerializationWriter);
				xmlWriter.Flush();
			}
			catch (Exception innerException)
			{
				if (innerException is TargetInvocationException)
				{
					innerException = innerException.InnerException;
				}
				if (innerException is InvalidOperationException || innerException is InvalidCastException)
				{
					throw new InvalidOperationException("There was an error generating the XML document.", innerException);
				}
				throw;
			}
		}

		/// <summary>Deserializes the object using the data contained by the specified <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="xmlReader">An instance of the <see cref="T:System.Xml.XmlReader" /> class used to read the document.</param>
		/// <param name="encodingStyle">The encoding used.</param>
		/// <param name="events">An instance of the <see cref="T:System.Xml.Serialization.XmlDeserializationEvents" /> class. </param>
		/// <returns>The object being deserialized.</returns>
		// Token: 0x06001FA3 RID: 8099 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		public object Deserialize(XmlReader xmlReader, string encodingStyle, XmlDeserializationEvents events)
		{
			throw new NotImplementedException();
		}

		/// <summary>Deserializes the XML document contained by the specified <see cref="T:System.Xml.XmlReader" /> and encoding style.</summary>
		/// <param name="xmlReader">The <see cref="T:System.Xml.XmlReader" /> that contains the XML document to deserialize. </param>
		/// <param name="encodingStyle">The encoding style of the serialized XML. </param>
		/// <returns>The deserialized object.</returns>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during deserialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001FA4 RID: 8100 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		public object Deserialize(XmlReader xmlReader, string encodingStyle)
		{
			throw new NotImplementedException();
		}

		/// <summary>Deserializes an XML document contained by the specified <see cref="T:System.Xml.XmlReader" /> and allows the overriding of events that occur during deserialization.</summary>
		/// <param name="xmlReader">The <see cref="T:System.Xml.XmlReader" /> that contains the document to deserialize.</param>
		/// <param name="events">An instance of the <see cref="T:System.Xml.Serialization.XmlDeserializationEvents" /> class. </param>
		/// <returns>The <see cref="T:System.Object" /> being deserialized.</returns>
		// Token: 0x06001FA5 RID: 8101 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		public object Deserialize(XmlReader xmlReader, XmlDeserializationEvents events)
		{
			throw new NotImplementedException();
		}

		/// <summary>Returns an instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class created from mappings of one XML type to another.</summary>
		/// <param name="mappings">An array of <see cref="T:System.Xml.Serialization.XmlMapping" /> objects used to map one type to another.</param>
		/// <param name="evidence">An instance of the <see cref="T:System.Security.Policy.Evidence" /> class that contains host and assembly data presented to the common language runtime policy system.</param>
		/// <returns>An instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class.</returns>
		// Token: 0x06001FA6 RID: 8102 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		public static XmlSerializer[] FromMappings(XmlMapping[] mappings, Evidence evidence)
		{
			throw new NotImplementedException();
		}

		/// <summary>Returns an instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class from the specified mappings.</summary>
		/// <param name="mappings">An array of <see cref="T:System.Xml.Serialization.XmlMapping" /> objects.</param>
		/// <param name="type">The <see cref="T:System.Type" /> of the deserialized object.</param>
		/// <returns>An instance of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class.</returns>
		// Token: 0x06001FA7 RID: 8103 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		public static XmlSerializer[] FromMappings(XmlMapping[] mappings, Type type)
		{
			throw new NotImplementedException();
		}

		/// <summary>Returns the name of the assembly that contains one or more versions of the <see cref="T:System.Xml.Serialization.XmlSerializer" /> especially created to serialize or deserialize the specified type.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> you are deserializing.</param>
		/// <returns>The name of the assembly that contains an <see cref="T:System.Xml.Serialization.XmlSerializer" /> for the type.</returns>
		// Token: 0x06001FA8 RID: 8104 RVA: 0x000B4BC8 File Offset: 0x000B2DC8
		public static string GetXmlSerializerAssemblyName(Type type)
		{
			return type.Assembly.GetName().Name + ".XmlSerializers";
		}

		/// <summary>Returns the name of the assembly that contains the serializer for the specified type in the specified namespace.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> you are interested in.</param>
		/// <param name="defaultNamespace">The namespace of the type.</param>
		/// <returns>The name of the assembly that contains specially built serializers.</returns>
		// Token: 0x06001FA9 RID: 8105 RVA: 0x000B4BE4 File Offset: 0x000B2DE4
		public static string GetXmlSerializerAssemblyName(Type type, string defaultNamespace)
		{
			return XmlSerializer.GetXmlSerializerAssemblyName(type) + "." + defaultNamespace.GetHashCode();
		}

		/// <summary>Serializes the specified object and writes the XML document to a file using the specified <see cref="T:System.Xml.XmlWriter" /> and references the specified namespaces and encoding style.</summary>
		/// <param name="xmlWriter">The <see cref="T:System.Xml.XmlWriter" /> used to write the XML document. </param>
		/// <param name="o">The object to serialize. </param>
		/// <param name="namespaces">The <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> referenced by the object. </param>
		/// <param name="encodingStyle">The encoding style of the serialized XML. </param>
		/// <exception cref="T:System.InvalidOperationException">An error occurred during serialization. The original exception is available using the <see cref="P:System.Exception.InnerException" /> property. </exception>
		// Token: 0x06001FAA RID: 8106 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoTODO]
		public void Serialize(XmlWriter xmlWriter, object o, XmlSerializerNamespaces namespaces, string encodingStyle)
		{
			throw new NotImplementedException();
		}

		/// <summary>Serializes the specified <see cref="T:System.Object" /> and writes the XML document to a file using the specified <see cref="T:System.Xml.XmlWriter" />, XML namespaces, and encoding. </summary>
		/// <param name="xmlWriter">The <see cref="T:System.Xml.XmlWriter" /> used to write the XML document.</param>
		/// <param name="o">The object to serialize.</param>
		/// <param name="namespaces">An instance of the <see langword="XmlSerializaerNamespaces" /> that contains namespaces and prefixes to use.</param>
		/// <param name="encodingStyle">The encoding used in the document.</param>
		/// <param name="id">For SOAP encoded messages, the base used to generate id attributes. </param>
		// Token: 0x06001FAB RID: 8107 RVA: 0x0000A6CF File Offset: 0x000088CF
		[MonoNotSupported("")]
		public void Serialize(XmlWriter xmlWriter, object o, XmlSerializerNamespaces namespaces, string encodingStyle, string id)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001FAC RID: 8108 RVA: 0x000B4C04 File Offset: 0x000B2E04
		private XmlSerializationWriter CreateWriter(XmlMapping typeMapping)
		{
			lock (this)
			{
				if (this.serializerData != null)
				{
					XmlSerializer.SerializerData obj = this.serializerData;
					XmlSerializationWriter xmlSerializationWriter;
					lock (obj)
					{
						xmlSerializationWriter = this.serializerData.CreateWriter();
					}
					if (xmlSerializationWriter != null)
					{
						return xmlSerializationWriter;
					}
				}
			}
			return new XmlSerializationWriterInterpreter(typeMapping);
		}

		// Token: 0x06001FAD RID: 8109 RVA: 0x000B4C8C File Offset: 0x000B2E8C
		private XmlSerializationReader CreateReader(XmlMapping typeMapping)
		{
			return new XmlSerializationReaderInterpreter(typeMapping);
		}

		// Token: 0x06001FAE RID: 8110 RVA: 0x0000A6CF File Offset: 0x000088CF
		private void CheckGeneratedTypes(XmlMapping typeMapping)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001FAF RID: 8111 RVA: 0x0000A6CF File Offset: 0x000088CF
		private void GenerateSerializersAsync(XmlSerializer.GenerationBatch batch)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001FB0 RID: 8112 RVA: 0x0000A6CF File Offset: 0x000088CF
		private void RunSerializerGeneration(object obj)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001FB1 RID: 8113 RVA: 0x0000206B File Offset: 0x0000026B
		private XmlSerializer.GenerationBatch LoadFromSatelliteAssembly(XmlSerializer.GenerationBatch batch)
		{
			return batch;
		}

		/// <summary>Returns an assembly that contains custom-made serializers used to serialize or deserialize the specified type or types, using the specified mappings.</summary>
		/// <param name="types">A collection of types.</param>
		/// <param name="mappings">A collection of <see cref="T:System.Xml.Serialization.XmlMapping" /> objects used to convert one type to another.</param>
		/// <returns>An <see cref="T:System.Reflection.Assembly" /> object that contains serializers for the supplied types and mappings.</returns>
		// Token: 0x06001FB2 RID: 8114 RVA: 0x000A7058 File Offset: 0x000A5258
		[PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
		public static Assembly GenerateSerializer(Type[] types, XmlMapping[] mappings)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an assembly that contains custom-made serializers used to serialize or deserialize the specified type or types, using the specified mappings and compiler settings and options. </summary>
		/// <param name="types">An array of type <see cref="T:System.Type" /> that contains objects used to serialize and deserialize data.</param>
		/// <param name="mappings">An array of type <see cref="T:System.Xml.Serialization.XmlMapping" /> that maps the XML data to the type data.</param>
		/// <param name="parameters">An instance of the <see cref="T:System.CodeDom.Compiler.CompilerParameters" /> class that represents the parameters used to invoke a compiler.</param>
		/// <returns>An <see cref="T:System.Reflection.Assembly" /> that contains special versions of the <see cref="T:System.Xml.Serialization.XmlSerializer" />.</returns>
		// Token: 0x06001FB3 RID: 8115 RVA: 0x000A7058 File Offset: 0x000A5258
		[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
		public static Assembly GenerateSerializer(Type[] types, XmlMapping[] mappings, CompilerParameters parameters)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x04001711 RID: 5905
		internal const string WsdlNamespace = "http://schemas.xmlsoap.org/wsdl/";

		// Token: 0x04001712 RID: 5906
		internal const string EncodingNamespace = "http://schemas.xmlsoap.org/soap/encoding/";

		// Token: 0x04001713 RID: 5907
		internal const string WsdlTypesNamespace = "http://microsoft.com/wsdl/types/";

		// Token: 0x04001714 RID: 5908
		private static int generationThreshold;

		// Token: 0x04001715 RID: 5909
		private static bool backgroundGeneration = true;

		// Token: 0x04001716 RID: 5910
		private static bool deleteTempFiles = true;

		// Token: 0x04001717 RID: 5911
		private static bool generatorFallback = true;

		// Token: 0x04001718 RID: 5912
		private bool customSerializer;

		// Token: 0x04001719 RID: 5913
		private XmlMapping typeMapping;

		// Token: 0x0400171A RID: 5914
		private XmlSerializer.SerializerData serializerData;

		// Token: 0x0400171B RID: 5915
		private static Hashtable serializerTypes = new Hashtable();

		// Token: 0x0400171C RID: 5916
		private UnreferencedObjectEventHandler onUnreferencedObject;

		// Token: 0x0400171D RID: 5917
		private XmlAttributeEventHandler onUnknownAttribute;

		// Token: 0x0400171E RID: 5918
		private XmlElementEventHandler onUnknownElement;

		// Token: 0x0400171F RID: 5919
		private XmlNodeEventHandler onUnknownNode;

		// Token: 0x04001720 RID: 5920
		private static Encoding DefaultEncoding = Encoding.Default;

		// Token: 0x02000338 RID: 824
		internal class SerializerData
		{
			// Token: 0x06001FB4 RID: 8116 RVA: 0x000B4C94 File Offset: 0x000B2E94
			public XmlSerializationReader CreateReader()
			{
				if (this.ReaderType != null)
				{
					return (XmlSerializationReader)Activator.CreateInstance(this.ReaderType);
				}
				if (this.Implementation != null)
				{
					return this.Implementation.Reader;
				}
				return null;
			}

			// Token: 0x06001FB5 RID: 8117 RVA: 0x000B4CCA File Offset: 0x000B2ECA
			public XmlSerializationWriter CreateWriter()
			{
				if (this.WriterType != null)
				{
					return (XmlSerializationWriter)Activator.CreateInstance(this.WriterType);
				}
				if (this.Implementation != null)
				{
					return this.Implementation.Writer;
				}
				return null;
			}

			// Token: 0x06001FB6 RID: 8118 RVA: 0x00002103 File Offset: 0x00000303
			public SerializerData()
			{
			}

			// Token: 0x04001721 RID: 5921
			public int UsageCount;

			// Token: 0x04001722 RID: 5922
			public bool Generated;

			// Token: 0x04001723 RID: 5923
			public Type ReaderType;

			// Token: 0x04001724 RID: 5924
			public MethodInfo ReaderMethod;

			// Token: 0x04001725 RID: 5925
			public Type WriterType;

			// Token: 0x04001726 RID: 5926
			public MethodInfo WriterMethod;

			// Token: 0x04001727 RID: 5927
			public XmlSerializer.GenerationBatch Batch;

			// Token: 0x04001728 RID: 5928
			public XmlSerializerImplementation Implementation;
		}

		// Token: 0x02000339 RID: 825
		internal class GenerationBatch
		{
			// Token: 0x06001FB7 RID: 8119 RVA: 0x00002103 File Offset: 0x00000303
			public GenerationBatch()
			{
			}

			// Token: 0x04001729 RID: 5929
			public bool Done;

			// Token: 0x0400172A RID: 5930
			public XmlMapping[] Maps;

			// Token: 0x0400172B RID: 5931
			public XmlSerializer.SerializerData[] Datas;
		}
	}
}
