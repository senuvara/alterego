﻿using System;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Populates <see cref="T:System.Xml.Schema.XmlSchema" /> objects with XML Schema data type definitions for .NET Framework types that are serialized using SOAP encoding.</summary>
	// Token: 0x0200056D RID: 1389
	public class SoapSchemaExporter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.SoapSchemaExporter" /> class, which supplies the collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects to which XML Schema element declarations are to be added.</summary>
		/// <param name="schemas">A collection of <see cref="T:System.Xml.Schema.XmlSchema" /> objects to which element declarations obtained from type mappings are to be added.</param>
		// Token: 0x0600345F RID: 13407 RVA: 0x00072668 File Offset: 0x00070868
		public SoapSchemaExporter(XmlSchemas schemas)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds to the applicable <see cref="T:System.Xml.Schema.XmlSchema" /> object a data type definition for each of the element parts of a SOAP-encoded message definition.</summary>
		/// <param name="xmlMembersMapping">Internal .NET Framework type mappings for the element parts of a WSDL message definition.</param>
		// Token: 0x06003460 RID: 13408 RVA: 0x00072668 File Offset: 0x00070868
		public void ExportMembersMapping(XmlMembersMapping xmlMembersMapping)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds to the applicable <see cref="T:System.Xml.Schema.XmlSchema" /> object a data type definition for each of the element parts of a SOAP-encoded message definition.</summary>
		/// <param name="xmlMembersMapping">Internal .NET Framework type mappings for the element parts of a WSDL message definition.</param>
		/// <param name="exportEnclosingType">
		///       <see langword="true" /> to export a type definition for the parent element of the WSDL parts; otherwise, <see langword="false" />.</param>
		// Token: 0x06003461 RID: 13409 RVA: 0x00072668 File Offset: 0x00070868
		public void ExportMembersMapping(XmlMembersMapping xmlMembersMapping, bool exportEnclosingType)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds to the applicable <see cref="T:System.Xml.Schema.XmlSchema" /> object a data type definition for a .NET Framework type.</summary>
		/// <param name="xmlTypeMapping">An internal mapping between a .NET Framework type and an XML Schema element.</param>
		// Token: 0x06003462 RID: 13410 RVA: 0x00072668 File Offset: 0x00070868
		public void ExportTypeMapping(XmlTypeMapping xmlTypeMapping)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
