﻿using System;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x02000340 RID: 832
	internal class XmlTypeMapElementInfo
	{
		// Token: 0x06001FEE RID: 8174 RVA: 0x000B5010 File Offset: 0x000B3210
		public XmlTypeMapElementInfo(XmlTypeMapMember member, TypeData type)
		{
			this._member = member;
			this._type = type;
			if (type.IsValueType && type.IsNullable)
			{
				this._isNullable = true;
			}
		}

		// Token: 0x170005F8 RID: 1528
		// (get) Token: 0x06001FEF RID: 8175 RVA: 0x000B5061 File Offset: 0x000B3261
		// (set) Token: 0x06001FF0 RID: 8176 RVA: 0x000B5069 File Offset: 0x000B3269
		public TypeData TypeData
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}

		// Token: 0x170005F9 RID: 1529
		// (get) Token: 0x06001FF1 RID: 8177 RVA: 0x000B5072 File Offset: 0x000B3272
		// (set) Token: 0x06001FF2 RID: 8178 RVA: 0x000B507A File Offset: 0x000B327A
		public object ChoiceValue
		{
			get
			{
				return this._choiceValue;
			}
			set
			{
				this._choiceValue = value;
			}
		}

		// Token: 0x170005FA RID: 1530
		// (get) Token: 0x06001FF3 RID: 8179 RVA: 0x000B5083 File Offset: 0x000B3283
		// (set) Token: 0x06001FF4 RID: 8180 RVA: 0x000B508B File Offset: 0x000B328B
		public string ElementName
		{
			get
			{
				return this._elementName;
			}
			set
			{
				this._elementName = value;
			}
		}

		// Token: 0x170005FB RID: 1531
		// (get) Token: 0x06001FF5 RID: 8181 RVA: 0x000B5094 File Offset: 0x000B3294
		// (set) Token: 0x06001FF6 RID: 8182 RVA: 0x000B509C File Offset: 0x000B329C
		public string Namespace
		{
			get
			{
				return this._namespace;
			}
			set
			{
				this._namespace = value;
			}
		}

		// Token: 0x170005FC RID: 1532
		// (get) Token: 0x06001FF7 RID: 8183 RVA: 0x000B50A5 File Offset: 0x000B32A5
		public string DataTypeNamespace
		{
			get
			{
				if (this._mappedType == null)
				{
					return "http://www.w3.org/2001/XMLSchema";
				}
				return this._mappedType.XmlTypeNamespace;
			}
		}

		// Token: 0x170005FD RID: 1533
		// (get) Token: 0x06001FF8 RID: 8184 RVA: 0x000B50C0 File Offset: 0x000B32C0
		public string DataTypeName
		{
			get
			{
				if (this._mappedType == null)
				{
					return this.TypeData.XmlType;
				}
				return this._mappedType.XmlType;
			}
		}

		// Token: 0x170005FE RID: 1534
		// (get) Token: 0x06001FF9 RID: 8185 RVA: 0x000B50E1 File Offset: 0x000B32E1
		// (set) Token: 0x06001FFA RID: 8186 RVA: 0x000B50E9 File Offset: 0x000B32E9
		public XmlSchemaForm Form
		{
			get
			{
				return this._form;
			}
			set
			{
				this._form = value;
			}
		}

		// Token: 0x170005FF RID: 1535
		// (get) Token: 0x06001FFB RID: 8187 RVA: 0x000B50F2 File Offset: 0x000B32F2
		// (set) Token: 0x06001FFC RID: 8188 RVA: 0x000B50FA File Offset: 0x000B32FA
		public XmlTypeMapping MappedType
		{
			get
			{
				return this._mappedType;
			}
			set
			{
				this._mappedType = value;
			}
		}

		// Token: 0x17000600 RID: 1536
		// (get) Token: 0x06001FFD RID: 8189 RVA: 0x000B5103 File Offset: 0x000B3303
		// (set) Token: 0x06001FFE RID: 8190 RVA: 0x000B510B File Offset: 0x000B330B
		public bool IsNullable
		{
			get
			{
				return this._isNullable;
			}
			set
			{
				this._isNullable = value;
			}
		}

		// Token: 0x17000601 RID: 1537
		// (get) Token: 0x06001FFF RID: 8191 RVA: 0x000B5114 File Offset: 0x000B3314
		internal bool IsPrimitive
		{
			get
			{
				return this._mappedType == null;
			}
		}

		// Token: 0x17000602 RID: 1538
		// (get) Token: 0x06002000 RID: 8192 RVA: 0x000B511F File Offset: 0x000B331F
		// (set) Token: 0x06002001 RID: 8193 RVA: 0x000B5127 File Offset: 0x000B3327
		public XmlTypeMapMember Member
		{
			get
			{
				return this._member;
			}
			set
			{
				this._member = value;
			}
		}

		// Token: 0x17000603 RID: 1539
		// (get) Token: 0x06002002 RID: 8194 RVA: 0x000B5130 File Offset: 0x000B3330
		// (set) Token: 0x06002003 RID: 8195 RVA: 0x000B5138 File Offset: 0x000B3338
		public int NestingLevel
		{
			get
			{
				return this._nestingLevel;
			}
			set
			{
				this._nestingLevel = value;
			}
		}

		// Token: 0x17000604 RID: 1540
		// (get) Token: 0x06002004 RID: 8196 RVA: 0x000B5141 File Offset: 0x000B3341
		public bool MultiReferenceType
		{
			get
			{
				return this._mappedType != null && this._mappedType.MultiReferenceType;
			}
		}

		// Token: 0x17000605 RID: 1541
		// (get) Token: 0x06002005 RID: 8197 RVA: 0x000B5158 File Offset: 0x000B3358
		// (set) Token: 0x06002006 RID: 8198 RVA: 0x000B5160 File Offset: 0x000B3360
		public bool WrappedElement
		{
			get
			{
				return this._wrappedElement;
			}
			set
			{
				this._wrappedElement = value;
			}
		}

		// Token: 0x17000606 RID: 1542
		// (get) Token: 0x06002007 RID: 8199 RVA: 0x000B5169 File Offset: 0x000B3369
		// (set) Token: 0x06002008 RID: 8200 RVA: 0x000B517B File Offset: 0x000B337B
		public bool IsTextElement
		{
			get
			{
				return this.ElementName == "<text>";
			}
			set
			{
				if (!value)
				{
					throw new Exception("INTERNAL ERROR; someone wrote unexpected code in sys.xml");
				}
				this.ElementName = "<text>";
				this.Namespace = string.Empty;
			}
		}

		// Token: 0x17000607 RID: 1543
		// (get) Token: 0x06002009 RID: 8201 RVA: 0x000B51A1 File Offset: 0x000B33A1
		// (set) Token: 0x0600200A RID: 8202 RVA: 0x000B51B3 File Offset: 0x000B33B3
		public bool IsUnnamedAnyElement
		{
			get
			{
				return this.ElementName == string.Empty;
			}
			set
			{
				if (!value)
				{
					throw new Exception("INTERNAL ERROR; someone wrote unexpected code in sys.xml");
				}
				this.ElementName = string.Empty;
				this.Namespace = string.Empty;
			}
		}

		// Token: 0x17000608 RID: 1544
		// (get) Token: 0x0600200B RID: 8203 RVA: 0x000B51D9 File Offset: 0x000B33D9
		// (set) Token: 0x0600200C RID: 8204 RVA: 0x000B51E1 File Offset: 0x000B33E1
		public int ExplicitOrder
		{
			get
			{
				return this._explicitOrder;
			}
			set
			{
				this._explicitOrder = value;
			}
		}

		// Token: 0x0600200D RID: 8205 RVA: 0x000B51EC File Offset: 0x000B33EC
		public override bool Equals(object other)
		{
			if (other == null)
			{
				return false;
			}
			XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)other;
			return !(this._elementName != xmlTypeMapElementInfo._elementName) && !(this._type.XmlType != xmlTypeMapElementInfo._type.XmlType) && !(this._namespace != xmlTypeMapElementInfo._namespace) && this._form == xmlTypeMapElementInfo._form && this._type == xmlTypeMapElementInfo._type && this._isNullable == xmlTypeMapElementInfo._isNullable && (this._choiceValue == null || this._choiceValue.Equals(xmlTypeMapElementInfo._choiceValue)) && this._choiceValue == xmlTypeMapElementInfo._choiceValue;
		}

		// Token: 0x0600200E RID: 8206 RVA: 0x000B52AC File Offset: 0x000B34AC
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x04001739 RID: 5945
		private string _elementName;

		// Token: 0x0400173A RID: 5946
		private string _namespace = "";

		// Token: 0x0400173B RID: 5947
		private XmlSchemaForm _form;

		// Token: 0x0400173C RID: 5948
		private XmlTypeMapMember _member;

		// Token: 0x0400173D RID: 5949
		private object _choiceValue;

		// Token: 0x0400173E RID: 5950
		private bool _isNullable;

		// Token: 0x0400173F RID: 5951
		private int _nestingLevel;

		// Token: 0x04001740 RID: 5952
		private XmlTypeMapping _mappedType;

		// Token: 0x04001741 RID: 5953
		private TypeData _type;

		// Token: 0x04001742 RID: 5954
		private bool _wrappedElement = true;

		// Token: 0x04001743 RID: 5955
		private int _explicitOrder = -1;
	}
}
