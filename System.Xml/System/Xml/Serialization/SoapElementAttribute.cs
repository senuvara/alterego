﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Specifies that the public member value be serialized by the <see cref="T:System.Xml.Serialization.XmlSerializer" /> as an encoded SOAP XML element.</summary>
	// Token: 0x020002F9 RID: 761
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class SoapElementAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.SoapElementAttribute" /> class.</summary>
		// Token: 0x06001C05 RID: 7173 RVA: 0x0000D3B3 File Offset: 0x0000B5B3
		public SoapElementAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.SoapElementAttribute" /> class and specifies the name of the XML element.</summary>
		/// <param name="elementName">The XML element name of the serialized member. </param>
		// Token: 0x06001C06 RID: 7174 RVA: 0x000A1C07 File Offset: 0x0009FE07
		public SoapElementAttribute(string elementName)
		{
			this.elementName = elementName;
		}

		/// <summary>Gets or sets the XML Schema definition language (XSD) data type of the generated XML element.</summary>
		/// <returns>One of the XML Schema data types.</returns>
		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x06001C07 RID: 7175 RVA: 0x000A1C16 File Offset: 0x0009FE16
		// (set) Token: 0x06001C08 RID: 7176 RVA: 0x000A1C2C File Offset: 0x0009FE2C
		public string DataType
		{
			get
			{
				if (this.dataType == null)
				{
					return string.Empty;
				}
				return this.dataType;
			}
			set
			{
				this.dataType = value;
			}
		}

		/// <summary>Gets or sets the name of the generated XML element.</summary>
		/// <returns>The name of the generated XML element. The default is the member identifier.</returns>
		// Token: 0x1700054E RID: 1358
		// (get) Token: 0x06001C09 RID: 7177 RVA: 0x000A1C35 File Offset: 0x0009FE35
		// (set) Token: 0x06001C0A RID: 7178 RVA: 0x000A1C4B File Offset: 0x0009FE4B
		public string ElementName
		{
			get
			{
				if (this.elementName == null)
				{
					return string.Empty;
				}
				return this.elementName;
			}
			set
			{
				this.elementName = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="T:System.Xml.Serialization.XmlSerializer" /> must serialize a member that has the <see langword="xsi:null" /> attribute set to "1".</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Xml.Serialization.XmlSerializer" /> generates the <see langword="xsi:null" /> attribute; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700054F RID: 1359
		// (get) Token: 0x06001C0B RID: 7179 RVA: 0x000A1C54 File Offset: 0x0009FE54
		// (set) Token: 0x06001C0C RID: 7180 RVA: 0x000A1C5C File Offset: 0x0009FE5C
		public bool IsNullable
		{
			get
			{
				return this.isNullable;
			}
			set
			{
				this.isNullable = value;
			}
		}

		// Token: 0x06001C0D RID: 7181 RVA: 0x000A1C65 File Offset: 0x0009FE65
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("SEA ");
			KeyHelper.AddField(sb, 1, this.elementName);
			KeyHelper.AddField(sb, 2, this.dataType);
			KeyHelper.AddField(sb, 3, this.isNullable);
			sb.Append('|');
		}

		// Token: 0x0400161C RID: 5660
		private string dataType;

		// Token: 0x0400161D RID: 5661
		private string elementName;

		// Token: 0x0400161E RID: 5662
		private bool isNullable;
	}
}
