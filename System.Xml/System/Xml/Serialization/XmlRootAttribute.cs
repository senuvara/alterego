﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	/// <summary>Controls XML serialization of the attribute target as an XML root element.</summary>
	// Token: 0x0200031F RID: 799
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.ReturnValue)]
	public class XmlRootAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlRootAttribute" /> class.</summary>
		// Token: 0x06001DA6 RID: 7590 RVA: 0x000A951E File Offset: 0x000A771E
		public XmlRootAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlRootAttribute" /> class and specifies the name of the XML root element.</summary>
		/// <param name="elementName">The name of the XML root element. </param>
		// Token: 0x06001DA7 RID: 7591 RVA: 0x000A952D File Offset: 0x000A772D
		public XmlRootAttribute(string elementName)
		{
			this.elementName = elementName;
		}

		/// <summary>Gets or sets the XSD data type of the XML root element.</summary>
		/// <returns>An XSD (XML Schema Document) data type, as defined by the World Wide Web Consortium (www.w3.org) document named "XML Schema: DataTypes".</returns>
		// Token: 0x170005C8 RID: 1480
		// (get) Token: 0x06001DA8 RID: 7592 RVA: 0x000A9543 File Offset: 0x000A7743
		// (set) Token: 0x06001DA9 RID: 7593 RVA: 0x000A9559 File Offset: 0x000A7759
		public string DataType
		{
			get
			{
				if (this.dataType == null)
				{
					return string.Empty;
				}
				return this.dataType;
			}
			set
			{
				this.dataType = value;
			}
		}

		/// <summary>Gets or sets the name of the XML element that is generated and recognized by the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class's <see cref="M:System.Xml.Serialization.XmlSerializer.Serialize(System.IO.TextWriter,System.Object)" /> and <see cref="M:System.Xml.Serialization.XmlSerializer.Deserialize(System.IO.Stream)" /> methods, respectively.</summary>
		/// <returns>The name of the XML root element that is generated and recognized in an XML-document instance. The default is the name of the serialized class.</returns>
		// Token: 0x170005C9 RID: 1481
		// (get) Token: 0x06001DAA RID: 7594 RVA: 0x000A9562 File Offset: 0x000A7762
		// (set) Token: 0x06001DAB RID: 7595 RVA: 0x000A9578 File Offset: 0x000A7778
		public string ElementName
		{
			get
			{
				if (this.elementName == null)
				{
					return string.Empty;
				}
				return this.elementName;
			}
			set
			{
				this.elementName = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="T:System.Xml.Serialization.XmlSerializer" /> must serialize a member that is set to <see langword="null" /> into the <see langword="xsi:nil" /> attribute set to <see langword="true" />.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Xml.Serialization.XmlSerializer" /> generates the <see langword="xsi:nil" /> attribute; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005CA RID: 1482
		// (get) Token: 0x06001DAC RID: 7596 RVA: 0x000A9581 File Offset: 0x000A7781
		// (set) Token: 0x06001DAD RID: 7597 RVA: 0x000A9589 File Offset: 0x000A7789
		public bool IsNullable
		{
			get
			{
				return this.isNullable;
			}
			set
			{
				this.isNullable = value;
			}
		}

		/// <summary>Gets or sets the namespace for the XML root element.</summary>
		/// <returns>The namespace for the XML element.</returns>
		// Token: 0x170005CB RID: 1483
		// (get) Token: 0x06001DAE RID: 7598 RVA: 0x000A9592 File Offset: 0x000A7792
		// (set) Token: 0x06001DAF RID: 7599 RVA: 0x000A959A File Offset: 0x000A779A
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		// Token: 0x06001DB0 RID: 7600 RVA: 0x000A95A4 File Offset: 0x000A77A4
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XRA ");
			KeyHelper.AddField(sb, 1, this.ns);
			KeyHelper.AddField(sb, 2, this.elementName);
			KeyHelper.AddField(sb, 3, this.dataType);
			KeyHelper.AddField(sb, 4, this.isNullable);
			sb.Append('|');
		}

		// Token: 0x170005CC RID: 1484
		// (get) Token: 0x06001DB1 RID: 7601 RVA: 0x000A95FC File Offset: 0x000A77FC
		internal string Key
		{
			get
			{
				StringBuilder stringBuilder = new StringBuilder();
				this.AddKeyHash(stringBuilder);
				return stringBuilder.ToString();
			}
		}

		// Token: 0x040016A2 RID: 5794
		private string dataType;

		// Token: 0x040016A3 RID: 5795
		private string elementName;

		// Token: 0x040016A4 RID: 5796
		private bool isNullable = true;

		// Token: 0x040016A5 RID: 5797
		private string ns;
	}
}
