﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Delegate used by the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class for deserialization of types from SOAP-encoded, non-root XML data. </summary>
	/// <returns>The object returned by the callback.</returns>
	// Token: 0x02000329 RID: 809
	// (Invoke) Token: 0x06001E52 RID: 7762
	public delegate object XmlSerializationReadCallback();
}
