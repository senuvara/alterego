﻿using System;

namespace System.Xml.Serialization
{
	/// <summary>Provides mappings between .NET Framework Web service methods and Web Services Description Language (WSDL) messages that are defined for SOAP Web services. </summary>
	// Token: 0x0200031A RID: 794
	public class XmlMembersMapping : XmlMapping
	{
		// Token: 0x06001D61 RID: 7521 RVA: 0x000A7060 File Offset: 0x000A5260
		internal XmlMembersMapping()
		{
		}

		// Token: 0x06001D62 RID: 7522 RVA: 0x000A7068 File Offset: 0x000A5268
		internal XmlMembersMapping(XmlMemberMapping[] mapping) : this("", null, false, false, mapping)
		{
		}

		// Token: 0x06001D63 RID: 7523 RVA: 0x000A7079 File Offset: 0x000A5279
		internal XmlMembersMapping(string elementName, string ns, XmlMemberMapping[] mapping) : this(elementName, ns, true, false, mapping)
		{
		}

		// Token: 0x06001D64 RID: 7524 RVA: 0x000A7088 File Offset: 0x000A5288
		internal XmlMembersMapping(string elementName, string ns, bool hasWrapperElement, bool writeAccessors, XmlMemberMapping[] mapping) : base(elementName, ns)
		{
			this._hasWrapperElement = hasWrapperElement;
			this._mapping = mapping;
			ClassMap classMap = new ClassMap();
			classMap.IgnoreMemberNamespace = writeAccessors;
			foreach (XmlMemberMapping xmlMemberMapping in mapping)
			{
				classMap.AddMember(xmlMemberMapping.TypeMapMember);
			}
			base.ObjectMap = classMap;
		}

		/// <summary>Gets the number of .NET Framework code entities that belong to a Web service method to which a SOAP message is being mapped. </summary>
		/// <returns>The number of mappings in the collection.</returns>
		// Token: 0x170005BB RID: 1467
		// (get) Token: 0x06001D65 RID: 7525 RVA: 0x000A70E2 File Offset: 0x000A52E2
		public int Count
		{
			get
			{
				return this._mapping.Length;
			}
		}

		/// <summary>Gets an item that contains internal type mapping information for a .NET Framework code entity that belongs to a Web service method being mapped to a SOAP message.</summary>
		/// <param name="index">The index of the mapping to return.</param>
		/// <returns>The requested <see cref="T:System.Xml.Serialization.XmlMemberMapping" />.</returns>
		// Token: 0x170005BC RID: 1468
		public XmlMemberMapping this[int index]
		{
			get
			{
				return this._mapping[index];
			}
		}

		/// <summary>Gets the name of the .NET Framework type being mapped to the data type of an XML Schema element that represents a SOAP message.</summary>
		/// <returns>The name of the .NET Framework type.</returns>
		// Token: 0x170005BD RID: 1469
		// (get) Token: 0x06001D67 RID: 7527 RVA: 0x000037FB File Offset: 0x000019FB
		public string TypeName
		{
			[MonoTODO]
			get
			{
				return null;
			}
		}

		/// <summary>Gets the namespace of the .NET Framework type being mapped to the data type of an XML Schema element that represents a SOAP message.</summary>
		/// <returns>The .NET Framework namespace of the mapping.</returns>
		// Token: 0x170005BE RID: 1470
		// (get) Token: 0x06001D68 RID: 7528 RVA: 0x000037FB File Offset: 0x000019FB
		public string TypeNamespace
		{
			[MonoTODO]
			get
			{
				return null;
			}
		}

		// Token: 0x170005BF RID: 1471
		// (get) Token: 0x06001D69 RID: 7529 RVA: 0x000A70F6 File Offset: 0x000A52F6
		internal bool HasWrapperElement
		{
			get
			{
				return this._hasWrapperElement;
			}
		}

		// Token: 0x0400168E RID: 5774
		private bool _hasWrapperElement;

		// Token: 0x0400168F RID: 5775
		private XmlMemberMapping[] _mapping;
	}
}
