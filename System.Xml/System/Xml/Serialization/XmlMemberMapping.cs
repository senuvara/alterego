﻿using System;
using System.CodeDom.Compiler;
using System.Xml.Schema;
using Unity;

namespace System.Xml.Serialization
{
	/// <summary>Maps a code entity in a .NET Framework Web service method to an element in a Web Services Description Language (WSDL) message.</summary>
	// Token: 0x02000319 RID: 793
	public class XmlMemberMapping
	{
		// Token: 0x06001D53 RID: 7507 RVA: 0x000A6E84 File Offset: 0x000A5084
		internal XmlMemberMapping(string memberName, string defaultNamespace, XmlTypeMapMember mapMem, bool encodedFormat)
		{
			this._mapMember = mapMem;
			this._memberName = memberName;
			if (mapMem is XmlTypeMapMemberAnyElement)
			{
				XmlTypeMapMemberAnyElement xmlTypeMapMemberAnyElement = (XmlTypeMapMemberAnyElement)mapMem;
				XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)xmlTypeMapMemberAnyElement.ElementInfo[xmlTypeMapMemberAnyElement.ElementInfo.Count - 1];
				this._elementName = xmlTypeMapElementInfo.ElementName;
				this._namespace = xmlTypeMapElementInfo.Namespace;
				if (xmlTypeMapElementInfo.MappedType != null)
				{
					this._typeNamespace = xmlTypeMapElementInfo.MappedType.Namespace;
				}
				else
				{
					this._typeNamespace = "";
				}
			}
			else if (mapMem is XmlTypeMapMemberElement)
			{
				XmlTypeMapElementInfo xmlTypeMapElementInfo2 = (XmlTypeMapElementInfo)((XmlTypeMapMemberElement)mapMem).ElementInfo[0];
				this._elementName = xmlTypeMapElementInfo2.ElementName;
				if (encodedFormat)
				{
					this._namespace = defaultNamespace;
					if (xmlTypeMapElementInfo2.MappedType != null)
					{
						this._typeNamespace = "";
					}
					else
					{
						this._typeNamespace = xmlTypeMapElementInfo2.DataTypeNamespace;
					}
				}
				else
				{
					this._namespace = xmlTypeMapElementInfo2.Namespace;
					if (xmlTypeMapElementInfo2.MappedType != null)
					{
						this._typeNamespace = xmlTypeMapElementInfo2.MappedType.Namespace;
					}
					else
					{
						this._typeNamespace = "";
					}
					this._form = xmlTypeMapElementInfo2.Form;
				}
			}
			else
			{
				this._elementName = this._memberName;
				this._namespace = "";
			}
			if (this._form == XmlSchemaForm.None)
			{
				this._form = XmlSchemaForm.Qualified;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the .NET Framework type maps to an XML element or attribute of any type. </summary>
		/// <returns>
		///     <see langword="true" />, if the type maps to an XML any element or attribute; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005B0 RID: 1456
		// (get) Token: 0x06001D54 RID: 7508 RVA: 0x000A6FDA File Offset: 0x000A51DA
		public bool Any
		{
			get
			{
				return this._mapMember is XmlTypeMapMemberAnyElement;
			}
		}

		/// <summary>Gets the unqualified name of the XML element declaration that applies to this mapping. </summary>
		/// <returns>The unqualified name of the XML element declaration that applies to this mapping.</returns>
		// Token: 0x170005B1 RID: 1457
		// (get) Token: 0x06001D55 RID: 7509 RVA: 0x000A6FEA File Offset: 0x000A51EA
		public string ElementName
		{
			get
			{
				return this._elementName;
			}
		}

		/// <summary>Gets the name of the Web service method member that is represented by this mapping. </summary>
		/// <returns>The name of the Web service method member represented by this mapping.</returns>
		// Token: 0x170005B2 RID: 1458
		// (get) Token: 0x06001D56 RID: 7510 RVA: 0x000A6FF2 File Offset: 0x000A51F2
		public string MemberName
		{
			get
			{
				return this._memberName;
			}
		}

		/// <summary>Gets the XML namespace that applies to this mapping. </summary>
		/// <returns>The XML namespace that applies to this mapping.</returns>
		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x06001D57 RID: 7511 RVA: 0x000A6FFA File Offset: 0x000A51FA
		public string Namespace
		{
			get
			{
				return this._namespace;
			}
		}

		/// <summary>Gets the fully qualified type name of the .NET Framework type for this mapping. </summary>
		/// <returns>The fully qualified type name of the .NET Framework type for this mapping.</returns>
		// Token: 0x170005B4 RID: 1460
		// (get) Token: 0x06001D58 RID: 7512 RVA: 0x000A7002 File Offset: 0x000A5202
		public string TypeFullName
		{
			get
			{
				return this._mapMember.TypeData.FullTypeName;
			}
		}

		/// <summary>Gets the type name of the .NET Framework type for this mapping. </summary>
		/// <returns>The type name of the .NET Framework type for this mapping.</returns>
		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x06001D59 RID: 7513 RVA: 0x000A7014 File Offset: 0x000A5214
		public string TypeName
		{
			get
			{
				return this._mapMember.TypeData.XmlType;
			}
		}

		/// <summary>Gets the namespace of the .NET Framework type for this mapping.</summary>
		/// <returns>The namespace of the .NET Framework type for this mapping.</returns>
		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x06001D5A RID: 7514 RVA: 0x000A7026 File Offset: 0x000A5226
		public string TypeNamespace
		{
			get
			{
				return this._typeNamespace;
			}
		}

		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x06001D5B RID: 7515 RVA: 0x000A702E File Offset: 0x000A522E
		internal XmlTypeMapMember TypeMapMember
		{
			get
			{
				return this._mapMember;
			}
		}

		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x06001D5C RID: 7516 RVA: 0x000A7036 File Offset: 0x000A5236
		internal XmlSchemaForm Form
		{
			get
			{
				return this._form;
			}
		}

		/// <summary>Gets the XML element name as it appears in the service description document.</summary>
		/// <returns>The XML element name.</returns>
		// Token: 0x170005B9 RID: 1465
		// (get) Token: 0x06001D5D RID: 7517 RVA: 0x000A703E File Offset: 0x000A523E
		public string XsdElementName
		{
			get
			{
				return this._mapMember.Name;
			}
		}

		/// <summary>Gets a value that indicates whether the accompanying field in the .NET Framework type has a value specified.</summary>
		/// <returns>
		///     <see langword="true" />, if the accompanying field has a value specified; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x06001D5E RID: 7518 RVA: 0x000A704B File Offset: 0x000A524B
		public bool CheckSpecified
		{
			get
			{
				return this._mapMember.IsOptionalValueType;
			}
		}

		// Token: 0x06001D5F RID: 7519 RVA: 0x00072668 File Offset: 0x00070868
		internal XmlMemberMapping()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns the name of the type associated with the specified <see cref="T:System.CodeDom.Compiler.CodeDomProvider" />.</summary>
		/// <param name="codeProvider">A <see cref="T:System.CodeDom.Compiler.CodeDomProvider" />  that contains the name of the type.</param>
		/// <returns>The name of the type.</returns>
		// Token: 0x06001D60 RID: 7520 RVA: 0x000A7058 File Offset: 0x000A5258
		public string GenerateTypeName(CodeDomProvider codeProvider)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x04001688 RID: 5768
		private XmlTypeMapMember _mapMember;

		// Token: 0x04001689 RID: 5769
		private string _elementName;

		// Token: 0x0400168A RID: 5770
		private string _memberName;

		// Token: 0x0400168B RID: 5771
		private string _namespace;

		// Token: 0x0400168C RID: 5772
		private string _typeNamespace;

		// Token: 0x0400168D RID: 5773
		private XmlSchemaForm _form;
	}
}
