﻿using System;

namespace System.Xml
{
	// Token: 0x020002AC RID: 684
	internal static class XmlReservedNs
	{
		// Token: 0x0400104F RID: 4175
		internal const string NsXml = "http://www.w3.org/XML/1998/namespace";

		// Token: 0x04001050 RID: 4176
		internal const string NsXmlNs = "http://www.w3.org/2000/xmlns/";

		// Token: 0x04001051 RID: 4177
		internal const string NsDataType = "urn:schemas-microsoft-com:datatypes";

		// Token: 0x04001052 RID: 4178
		internal const string NsDataTypeAlias = "uuid:C2F41010-65B3-11D1-A29F-00AA00C14882";

		// Token: 0x04001053 RID: 4179
		internal const string NsDataTypeOld = "urn:uuid:C2F41010-65B3-11D1-A29F-00AA00C14882/";

		// Token: 0x04001054 RID: 4180
		internal const string NsMsxsl = "urn:schemas-microsoft-com:xslt";

		// Token: 0x04001055 RID: 4181
		internal const string NsXdr = "urn:schemas-microsoft-com:xml-data";

		// Token: 0x04001056 RID: 4182
		internal const string NsXslDebug = "urn:schemas-microsoft-com:xslt-debug";

		// Token: 0x04001057 RID: 4183
		internal const string NsXdrAlias = "uuid:BDC6E3F0-6DA3-11D1-A2A3-00AA00C14882";

		// Token: 0x04001058 RID: 4184
		internal const string NsWdXsl = "http://www.w3.org/TR/WD-xsl";

		// Token: 0x04001059 RID: 4185
		internal const string NsXs = "http://www.w3.org/2001/XMLSchema";

		// Token: 0x0400105A RID: 4186
		internal const string NsXsd = "http://www.w3.org/2001/XMLSchema-datatypes";

		// Token: 0x0400105B RID: 4187
		internal const string NsXsi = "http://www.w3.org/2001/XMLSchema-instance";

		// Token: 0x0400105C RID: 4188
		internal const string NsXslt = "http://www.w3.org/1999/XSL/Transform";

		// Token: 0x0400105D RID: 4189
		internal const string NsExsltCommon = "http://exslt.org/common";

		// Token: 0x0400105E RID: 4190
		internal const string NsExsltDates = "http://exslt.org/dates-and-times";

		// Token: 0x0400105F RID: 4191
		internal const string NsExsltMath = "http://exslt.org/math";

		// Token: 0x04001060 RID: 4192
		internal const string NsExsltRegExps = "http://exslt.org/regular-expressions";

		// Token: 0x04001061 RID: 4193
		internal const string NsExsltSets = "http://exslt.org/sets";

		// Token: 0x04001062 RID: 4194
		internal const string NsExsltStrings = "http://exslt.org/strings";

		// Token: 0x04001063 RID: 4195
		internal const string NsXQueryFunc = "http://www.w3.org/2003/11/xpath-functions";

		// Token: 0x04001064 RID: 4196
		internal const string NsXQueryDataType = "http://www.w3.org/2003/11/xpath-datatypes";

		// Token: 0x04001065 RID: 4197
		internal const string NsCollationBase = "http://collations.microsoft.com";

		// Token: 0x04001066 RID: 4198
		internal const string NsCollCodePoint = "http://www.w3.org/2004/10/xpath-functions/collation/codepoint";

		// Token: 0x04001067 RID: 4199
		internal const string NsXsltInternal = "http://schemas.microsoft.com/framework/2003/xml/xslt/internal";
	}
}
