﻿using System;
using System.Xml.Schema;

namespace System.Xml
{
	// Token: 0x020000A8 RID: 168
	internal interface IValidationEventHandling
	{
		// Token: 0x17000127 RID: 295
		// (get) Token: 0x06000576 RID: 1398
		object EventHandler { get; }

		// Token: 0x06000577 RID: 1399
		void SendEvent(Exception exception, XmlSeverityType severity);
	}
}
