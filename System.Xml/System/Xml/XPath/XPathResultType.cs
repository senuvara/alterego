﻿using System;

namespace System.Xml.XPath
{
	/// <summary>Specifies the return type of the XPath expression.</summary>
	// Token: 0x020002BB RID: 699
	public enum XPathResultType
	{
		/// <summary>A numeric value.</summary>
		// Token: 0x0400153A RID: 5434
		Number,
		/// <summary>A <see cref="T:System.String" /> value.</summary>
		// Token: 0x0400153B RID: 5435
		String,
		/// <summary>A <see cref="T:System.Boolean" /><see langword="true" /> or <see langword="false" /> value.</summary>
		// Token: 0x0400153C RID: 5436
		Boolean,
		/// <summary>A node collection.</summary>
		// Token: 0x0400153D RID: 5437
		NodeSet,
		/// <summary>A tree fragment.</summary>
		// Token: 0x0400153E RID: 5438
		Navigator = 1,
		/// <summary>Any of the XPath node types.</summary>
		// Token: 0x0400153F RID: 5439
		Any = 5,
		/// <summary>The expression does not evaluate to the correct XPath type.</summary>
		// Token: 0x04001540 RID: 5440
		Error
	}
}
