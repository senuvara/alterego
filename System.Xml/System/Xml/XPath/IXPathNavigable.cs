﻿using System;

namespace System.Xml.XPath
{
	/// <summary>Provides an accessor to the <see cref="T:System.Xml.XPath.XPathNavigator" /> class.</summary>
	// Token: 0x020002B4 RID: 692
	public interface IXPathNavigable
	{
		/// <summary>Returns a new <see cref="T:System.Xml.XPath.XPathNavigator" /> object. </summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNavigator" /> object.</returns>
		// Token: 0x0600192D RID: 6445
		XPathNavigator CreateNavigator();
	}
}
