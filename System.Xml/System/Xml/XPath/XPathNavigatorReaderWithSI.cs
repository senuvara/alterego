﻿using System;
using System.Xml.Schema;

namespace System.Xml.XPath
{
	// Token: 0x020002C5 RID: 709
	internal class XPathNavigatorReaderWithSI : XPathNavigatorReader, IXmlSchemaInfo
	{
		// Token: 0x06001A30 RID: 6704 RVA: 0x00093805 File Offset: 0x00091A05
		internal XPathNavigatorReaderWithSI(XPathNavigator navToRead, IXmlLineInfo xli, IXmlSchemaInfo xsi) : base(navToRead, xli, xsi)
		{
		}

		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x06001A31 RID: 6705 RVA: 0x00093810 File Offset: 0x00091A10
		public virtual XmlSchemaValidity Validity
		{
			get
			{
				if (!base.IsReading)
				{
					return XmlSchemaValidity.NotKnown;
				}
				return this.schemaInfo.Validity;
			}
		}

		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x06001A32 RID: 6706 RVA: 0x00093827 File Offset: 0x00091A27
		public override bool IsDefault
		{
			get
			{
				return base.IsReading && this.schemaInfo.IsDefault;
			}
		}

		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x06001A33 RID: 6707 RVA: 0x0009383E File Offset: 0x00091A3E
		public virtual bool IsNil
		{
			get
			{
				return base.IsReading && this.schemaInfo.IsNil;
			}
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x06001A34 RID: 6708 RVA: 0x00093855 File Offset: 0x00091A55
		public virtual XmlSchemaSimpleType MemberType
		{
			get
			{
				if (!base.IsReading)
				{
					return null;
				}
				return this.schemaInfo.MemberType;
			}
		}

		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x06001A35 RID: 6709 RVA: 0x0009386C File Offset: 0x00091A6C
		public virtual XmlSchemaType SchemaType
		{
			get
			{
				if (!base.IsReading)
				{
					return null;
				}
				return this.schemaInfo.SchemaType;
			}
		}

		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x06001A36 RID: 6710 RVA: 0x00093883 File Offset: 0x00091A83
		public virtual XmlSchemaElement SchemaElement
		{
			get
			{
				if (!base.IsReading)
				{
					return null;
				}
				return this.schemaInfo.SchemaElement;
			}
		}

		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06001A37 RID: 6711 RVA: 0x0009389A File Offset: 0x00091A9A
		public virtual XmlSchemaAttribute SchemaAttribute
		{
			get
			{
				if (!base.IsReading)
				{
					return null;
				}
				return this.schemaInfo.SchemaAttribute;
			}
		}
	}
}
