﻿using System;

namespace System.Xml.XPath
{
	/// <summary>Specifies the sort order for uppercase and lowercase letters.</summary>
	// Token: 0x020002B9 RID: 697
	public enum XmlCaseOrder
	{
		/// <summary>Ignore the case.</summary>
		// Token: 0x04001533 RID: 5427
		None,
		/// <summary>Uppercase letters are sorted before lowercase letters.</summary>
		// Token: 0x04001534 RID: 5428
		UpperFirst,
		/// <summary>Lowercase letters are sorted before uppercase letters.</summary>
		// Token: 0x04001535 RID: 5429
		LowerFirst
	}
}
