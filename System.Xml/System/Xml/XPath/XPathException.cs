﻿using System;
using System.Resources;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Xml.XPath
{
	/// <summary>Provides the exception thrown when an error occurs while processing an XPath expression. </summary>
	// Token: 0x020002B7 RID: 695
	[Serializable]
	public class XPathException : SystemException
	{
		/// <summary>Uses the information in the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" /> objects to initialize a new instance of the <see cref="T:System.Xml.XPath.XPathException" /> class.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that contains all the properties of an <see cref="T:System.Xml.XPath.XPathException" />. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> object. </param>
		// Token: 0x06001946 RID: 6470 RVA: 0x00090714 File Offset: 0x0008E914
		protected XPathException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.res = (string)info.GetValue("res", typeof(string));
			this.args = (string[])info.GetValue("args", typeof(string[]));
			string text = null;
			foreach (SerializationEntry serializationEntry in info)
			{
				if (serializationEntry.Name == "version")
				{
					text = (string)serializationEntry.Value;
				}
			}
			if (text == null)
			{
				this.message = XPathException.CreateMessage(this.res, this.args);
				return;
			}
			this.message = null;
		}

		/// <summary>Streams all the <see cref="T:System.Xml.XPath.XPathException" /> properties into the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> class for the specified <see cref="T:System.Runtime.Serialization.StreamingContext" />.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> object.</param>
		// Token: 0x06001947 RID: 6471 RVA: 0x000907C5 File Offset: 0x0008E9C5
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("res", this.res);
			info.AddValue("args", this.args);
			info.AddValue("version", "2.0");
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XPath.XPathException" /> class.</summary>
		// Token: 0x06001948 RID: 6472 RVA: 0x00090801 File Offset: 0x0008EA01
		public XPathException() : this(string.Empty, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XPath.XPathException" /> class with the specified exception message.</summary>
		/// <param name="message">The description of the error condition.</param>
		// Token: 0x06001949 RID: 6473 RVA: 0x0009080F File Offset: 0x0008EA0F
		public XPathException(string message) : this(message, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XPath.XPathException" /> class using the specified exception message and <see cref="T:System.Exception" /> object.</summary>
		/// <param name="message">The description of the error condition. </param>
		/// <param name="innerException">The <see cref="T:System.Exception" /> that threw the <see cref="T:System.Xml.XPath.XPathException" />, if any. This value can be <see langword="null" />. </param>
		// Token: 0x0600194A RID: 6474 RVA: 0x00090819 File Offset: 0x0008EA19
		public XPathException(string message, Exception innerException) : this("{0}", new string[]
		{
			message
		}, innerException)
		{
		}

		// Token: 0x0600194B RID: 6475 RVA: 0x00090831 File Offset: 0x0008EA31
		internal static XPathException Create(string res)
		{
			return new XPathException(res, null);
		}

		// Token: 0x0600194C RID: 6476 RVA: 0x0009083A File Offset: 0x0008EA3A
		internal static XPathException Create(string res, string arg)
		{
			return new XPathException(res, new string[]
			{
				arg
			});
		}

		// Token: 0x0600194D RID: 6477 RVA: 0x0009084C File Offset: 0x0008EA4C
		internal static XPathException Create(string res, string arg, string arg2)
		{
			return new XPathException(res, new string[]
			{
				arg,
				arg2
			});
		}

		// Token: 0x0600194E RID: 6478 RVA: 0x00090862 File Offset: 0x0008EA62
		internal static XPathException Create(string res, string arg, Exception innerException)
		{
			return new XPathException(res, new string[]
			{
				arg
			}, innerException);
		}

		// Token: 0x0600194F RID: 6479 RVA: 0x00090875 File Offset: 0x0008EA75
		private XPathException(string res, string[] args) : this(res, args, null)
		{
		}

		// Token: 0x06001950 RID: 6480 RVA: 0x00090880 File Offset: 0x0008EA80
		private XPathException(string res, string[] args, Exception inner) : base(XPathException.CreateMessage(res, args), inner)
		{
			base.HResult = -2146231997;
			this.res = res;
			this.args = args;
		}

		// Token: 0x06001951 RID: 6481 RVA: 0x000908AC File Offset: 0x0008EAAC
		private static string CreateMessage(string res, string[] args)
		{
			string result;
			try
			{
				string text = Res.GetString(res, args);
				if (text == null)
				{
					text = "UNKNOWN(" + res + ")";
				}
				result = text;
			}
			catch (MissingManifestResourceException)
			{
				result = "UNKNOWN(" + res + ")";
			}
			return result;
		}

		/// <summary>Gets the description of the error condition for this exception.</summary>
		/// <returns>The <see langword="string" /> description of the error condition for this exception.</returns>
		// Token: 0x170004B9 RID: 1209
		// (get) Token: 0x06001952 RID: 6482 RVA: 0x00090900 File Offset: 0x0008EB00
		public override string Message
		{
			get
			{
				if (this.message != null)
				{
					return this.message;
				}
				return base.Message;
			}
		}

		// Token: 0x0400152C RID: 5420
		private string res;

		// Token: 0x0400152D RID: 5421
		private string[] args;

		// Token: 0x0400152E RID: 5422
		private string message;
	}
}
