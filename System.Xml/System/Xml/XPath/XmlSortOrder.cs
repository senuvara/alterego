﻿using System;

namespace System.Xml.XPath
{
	/// <summary>Specifies the sort order.</summary>
	// Token: 0x020002B8 RID: 696
	public enum XmlSortOrder
	{
		/// <summary>Nodes are sorted in ascending order. For example, if the numbers 1,2,3, and 4 are sorted in ascending order, they appear as 1,2,3,4.</summary>
		// Token: 0x04001530 RID: 5424
		Ascending = 1,
		/// <summary>Nodes are sorted in descending order. For example, if the numbers 1,2,3, and 4 are sorted in descending order, they appear as, 4,3,2,1. </summary>
		// Token: 0x04001531 RID: 5425
		Descending
	}
}
