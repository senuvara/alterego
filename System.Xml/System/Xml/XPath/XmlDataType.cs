﻿using System;

namespace System.Xml.XPath
{
	/// <summary>Specifies the data type used to determine sort order.</summary>
	// Token: 0x020002BA RID: 698
	public enum XmlDataType
	{
		/// <summary>Values are sorted alphabetically.</summary>
		// Token: 0x04001537 RID: 5431
		Text = 1,
		/// <summary>Values are sorted numerically.</summary>
		// Token: 0x04001538 RID: 5432
		Number
	}
}
