﻿using System;
using System.Xml.Schema;

namespace System.Xml.XPath
{
	/// <summary>Represents an item in the XQuery 1.0 and XPath 2.0 Data Model.</summary>
	// Token: 0x020002BD RID: 701
	public abstract class XPathItem
	{
		/// <summary>When overridden in a derived class, gets a value indicating whether the item represents an XPath node or an atomic value.</summary>
		/// <returns>
		///     <see langword="true" /> if the item represents an XPath node; <see langword="false" /> if the item represents an atomic value.</returns>
		// Token: 0x170004BC RID: 1212
		// (get) Token: 0x0600195E RID: 6494
		public abstract bool IsNode { get; }

		/// <summary>When overridden in a derived class, gets the <see cref="T:System.Xml.Schema.XmlSchemaType" /> for the item.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaType" /> for the item.</returns>
		// Token: 0x170004BD RID: 1213
		// (get) Token: 0x0600195F RID: 6495
		public abstract XmlSchemaType XmlType { get; }

		/// <summary>When overridden in a derived class, gets the <see langword="string" /> value of the item.</summary>
		/// <returns>The <see langword="string" /> value of the item.</returns>
		// Token: 0x170004BE RID: 1214
		// (get) Token: 0x06001960 RID: 6496
		public abstract string Value { get; }

		/// <summary>When overridden in a derived class, gets the current item as a boxed object of the most appropriate .NET Framework 2.0 type according to its schema type.</summary>
		/// <returns>The current item as a boxed object of the most appropriate .NET Framework type.</returns>
		// Token: 0x170004BF RID: 1215
		// (get) Token: 0x06001961 RID: 6497
		public abstract object TypedValue { get; }

		/// <summary>When overridden in a derived class, gets the .NET Framework 2.0 type of the item.</summary>
		/// <returns>The .NET Framework type of the item. The default value is <see cref="T:System.String" />.</returns>
		// Token: 0x170004C0 RID: 1216
		// (get) Token: 0x06001962 RID: 6498
		public abstract Type ValueType { get; }

		/// <summary>When overridden in a derived class, gets the item's value as a <see cref="T:System.Boolean" />.</summary>
		/// <returns>The item's value as a <see cref="T:System.Boolean" />.</returns>
		/// <exception cref="T:System.FormatException">The item's value is not in the correct format for the <see cref="T:System.Boolean" /> type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.Boolean" /> is not valid.</exception>
		// Token: 0x170004C1 RID: 1217
		// (get) Token: 0x06001963 RID: 6499
		public abstract bool ValueAsBoolean { get; }

		/// <summary>When overridden in a derived class, gets the item's value as a <see cref="T:System.DateTime" />.</summary>
		/// <returns>The item's value as a <see cref="T:System.DateTime" />.</returns>
		/// <exception cref="T:System.FormatException">The item's value is not in the correct format for the <see cref="T:System.DateTime" /> type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.DateTime" /> is not valid.</exception>
		// Token: 0x170004C2 RID: 1218
		// (get) Token: 0x06001964 RID: 6500
		public abstract DateTime ValueAsDateTime { get; }

		/// <summary>When overridden in a derived class, gets the item's value as a <see cref="T:System.Double" />.</summary>
		/// <returns>The item's value as a <see cref="T:System.Double" />.</returns>
		/// <exception cref="T:System.FormatException">The item's value is not in the correct format for the <see cref="T:System.Double" /> type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.Double" /> is not valid.</exception>
		/// <exception cref="T:System.OverflowException">The attempted cast resulted in an overflow.</exception>
		// Token: 0x170004C3 RID: 1219
		// (get) Token: 0x06001965 RID: 6501
		public abstract double ValueAsDouble { get; }

		/// <summary>When overridden in a derived class, gets the item's value as an <see cref="T:System.Int32" />.</summary>
		/// <returns>The item's value as an <see cref="T:System.Int32" />.</returns>
		/// <exception cref="T:System.FormatException">The item's value is not in the correct format for the <see cref="T:System.Int32" /> type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.Int32" /> is not valid.</exception>
		/// <exception cref="T:System.OverflowException">The attempted cast resulted in an overflow.</exception>
		// Token: 0x170004C4 RID: 1220
		// (get) Token: 0x06001966 RID: 6502
		public abstract int ValueAsInt { get; }

		/// <summary>When overridden in a derived class, gets the item's value as an <see cref="T:System.Int64" />.</summary>
		/// <returns>The item's value as an <see cref="T:System.Int64" />.</returns>
		/// <exception cref="T:System.FormatException">The item's value is not in the correct format for the <see cref="T:System.Int64" /> type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.Int64" /> is not valid.</exception>
		/// <exception cref="T:System.OverflowException">The attempted cast resulted in an overflow.</exception>
		// Token: 0x170004C5 RID: 1221
		// (get) Token: 0x06001967 RID: 6503
		public abstract long ValueAsLong { get; }

		/// <summary>Returns the item's value as the specified type.</summary>
		/// <param name="returnType">The type to return the item value as.</param>
		/// <returns>The value of the item as the type requested.</returns>
		/// <exception cref="T:System.FormatException">The item's value is not in the correct format for the target type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.OverflowException">The attempted cast resulted in an overflow.</exception>
		// Token: 0x06001968 RID: 6504 RVA: 0x00090960 File Offset: 0x0008EB60
		public virtual object ValueAs(Type returnType)
		{
			return this.ValueAs(returnType, null);
		}

		/// <summary>When overridden in a derived class, returns the item's value as the type specified using the <see cref="T:System.Xml.IXmlNamespaceResolver" /> object specified to resolve namespace prefixes.</summary>
		/// <param name="returnType">The type to return the item's value as.</param>
		/// <param name="nsResolver">The <see cref="T:System.Xml.IXmlNamespaceResolver" /> object used to resolve namespace prefixes.</param>
		/// <returns>The value of the item as the type requested.</returns>
		/// <exception cref="T:System.FormatException">The item's value is not in the correct format for the target type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.OverflowException">The attempted cast resulted in an overflow.</exception>
		// Token: 0x06001969 RID: 6505
		public abstract object ValueAs(Type returnType, IXmlNamespaceResolver nsResolver);

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XPath.XPathItem" /> class.</summary>
		// Token: 0x0600196A RID: 6506 RVA: 0x00002103 File Offset: 0x00000303
		protected XPathItem()
		{
		}
	}
}
