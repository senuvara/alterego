﻿using System;
using System.Collections;

namespace System.Xml
{
	// Token: 0x0200022A RID: 554
	internal class XmlEmptyElementListEnumerator : IEnumerator
	{
		// Token: 0x060014BB RID: 5307 RVA: 0x00002103 File Offset: 0x00000303
		public XmlEmptyElementListEnumerator(XmlElementList list)
		{
		}

		// Token: 0x060014BC RID: 5308 RVA: 0x000020CD File Offset: 0x000002CD
		public bool MoveNext()
		{
			return false;
		}

		// Token: 0x060014BD RID: 5309 RVA: 0x000030EC File Offset: 0x000012EC
		public void Reset()
		{
		}

		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x060014BE RID: 5310 RVA: 0x000037FB File Offset: 0x000019FB
		public object Current
		{
			get
			{
				return null;
			}
		}
	}
}
