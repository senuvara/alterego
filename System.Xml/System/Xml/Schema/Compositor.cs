﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003D9 RID: 985
	internal enum Compositor
	{
		// Token: 0x0400196B RID: 6507
		Root,
		// Token: 0x0400196C RID: 6508
		Include,
		// Token: 0x0400196D RID: 6509
		Import,
		// Token: 0x0400196E RID: 6510
		Redefine
	}
}
