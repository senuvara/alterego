﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200037C RID: 892
	internal class XsdSimpleValue
	{
		// Token: 0x060021EA RID: 8682 RVA: 0x000BBB8C File Offset: 0x000B9D8C
		public XsdSimpleValue(XmlSchemaSimpleType st, object value)
		{
			this.xmlType = st;
			this.typedValue = value;
		}

		// Token: 0x170006A5 RID: 1701
		// (get) Token: 0x060021EB RID: 8683 RVA: 0x000BBBA2 File Offset: 0x000B9DA2
		public XmlSchemaSimpleType XmlType
		{
			get
			{
				return this.xmlType;
			}
		}

		// Token: 0x170006A6 RID: 1702
		// (get) Token: 0x060021EC RID: 8684 RVA: 0x000BBBAA File Offset: 0x000B9DAA
		public object TypedValue
		{
			get
			{
				return this.typedValue;
			}
		}

		// Token: 0x0400181F RID: 6175
		private XmlSchemaSimpleType xmlType;

		// Token: 0x04001820 RID: 6176
		private object typedValue;
	}
}
