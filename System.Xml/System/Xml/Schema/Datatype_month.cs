﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200039B RID: 923
	internal class Datatype_month : Datatype_dateTimeBase
	{
		// Token: 0x17000704 RID: 1796
		// (get) Token: 0x060022B6 RID: 8886 RVA: 0x000BDF69 File Offset: 0x000BC169
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.GMonth;
			}
		}

		// Token: 0x060022B7 RID: 8887 RVA: 0x000BDF6D File Offset: 0x000BC16D
		internal Datatype_month() : base(XsdDateTimeFlags.GMonth)
		{
		}
	}
}
