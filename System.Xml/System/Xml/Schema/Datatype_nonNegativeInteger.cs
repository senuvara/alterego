﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B3 RID: 947
	internal class Datatype_nonNegativeInteger : Datatype_integer
	{
		// Token: 0x17000750 RID: 1872
		// (get) Token: 0x0600233D RID: 9021 RVA: 0x000BE7D2 File Offset: 0x000BC9D2
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_nonNegativeInteger.numeric10FacetsChecker;
			}
		}

		// Token: 0x17000751 RID: 1873
		// (get) Token: 0x0600233E RID: 9022 RVA: 0x000BE7D9 File Offset: 0x000BC9D9
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.NonNegativeInteger;
			}
		}

		// Token: 0x17000752 RID: 1874
		// (get) Token: 0x0600233F RID: 9023 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool HasValueFacets
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06002340 RID: 9024 RVA: 0x000BE45B File Offset: 0x000BC65B
		public Datatype_nonNegativeInteger()
		{
		}

		// Token: 0x06002341 RID: 9025 RVA: 0x000BE7DD File Offset: 0x000BC9DD
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_nonNegativeInteger()
		{
		}

		// Token: 0x040018CC RID: 6348
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(0m, decimal.MaxValue);
	}
}
