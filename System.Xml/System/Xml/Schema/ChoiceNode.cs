﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200036F RID: 879
	internal sealed class ChoiceNode : InteriorNode
	{
		// Token: 0x0600218F RID: 8591 RVA: 0x000BA104 File Offset: 0x000B8304
		private static void ConstructChildPos(SyntaxTreeNode child, BitSet firstpos, BitSet lastpos, BitSet[] followpos)
		{
			BitSet bitSet = new BitSet(firstpos.Count);
			BitSet bitSet2 = new BitSet(lastpos.Count);
			child.ConstructPos(bitSet, bitSet2, followpos);
			firstpos.Or(bitSet);
			lastpos.Or(bitSet2);
		}

		// Token: 0x06002190 RID: 8592 RVA: 0x000BA140 File Offset: 0x000B8340
		public override void ConstructPos(BitSet firstpos, BitSet lastpos, BitSet[] followpos)
		{
			BitSet bitSet = new BitSet(firstpos.Count);
			BitSet bitSet2 = new BitSet(lastpos.Count);
			ChoiceNode choiceNode = this;
			SyntaxTreeNode leftChild;
			do
			{
				ChoiceNode.ConstructChildPos(choiceNode.RightChild, bitSet, bitSet2, followpos);
				leftChild = choiceNode.LeftChild;
				choiceNode = (leftChild as ChoiceNode);
			}
			while (choiceNode != null);
			leftChild.ConstructPos(firstpos, lastpos, followpos);
			firstpos.Or(bitSet);
			lastpos.Or(bitSet2);
		}

		// Token: 0x17000698 RID: 1688
		// (get) Token: 0x06002191 RID: 8593 RVA: 0x000BA1A0 File Offset: 0x000B83A0
		public override bool IsNullable
		{
			get
			{
				ChoiceNode choiceNode = this;
				while (!choiceNode.RightChild.IsNullable)
				{
					SyntaxTreeNode leftChild = choiceNode.LeftChild;
					choiceNode = (leftChild as ChoiceNode);
					if (choiceNode == null)
					{
						return leftChild.IsNullable;
					}
				}
				return true;
			}
		}

		// Token: 0x06002192 RID: 8594 RVA: 0x000BA0C9 File Offset: 0x000B82C9
		public override void ExpandTree(InteriorNode parent, SymbolsDictionary symbols, Positions positions)
		{
			base.ExpandTreeNoRecursive(parent, symbols, positions);
		}

		// Token: 0x06002193 RID: 8595 RVA: 0x000BA0D4 File Offset: 0x000B82D4
		public ChoiceNode()
		{
		}
	}
}
