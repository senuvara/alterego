﻿using System;
using MS.Internal.Xml.XPath;

namespace System.Xml.Schema
{
	// Token: 0x02000354 RID: 852
	internal class DoubleLinkAxis : Axis
	{
		// Token: 0x1700065B RID: 1627
		// (get) Token: 0x060020C8 RID: 8392 RVA: 0x000B7818 File Offset: 0x000B5A18
		// (set) Token: 0x060020C9 RID: 8393 RVA: 0x000B7820 File Offset: 0x000B5A20
		internal Axis Next
		{
			get
			{
				return this.next;
			}
			set
			{
				this.next = value;
			}
		}

		// Token: 0x060020CA RID: 8394 RVA: 0x000B782C File Offset: 0x000B5A2C
		internal DoubleLinkAxis(Axis axis, DoubleLinkAxis inputaxis) : base(axis.TypeOfAxis, inputaxis, axis.Prefix, axis.Name, axis.NodeType)
		{
			this.next = null;
			base.Urn = axis.Urn;
			this.abbrAxis = axis.AbbrAxis;
			if (inputaxis != null)
			{
				inputaxis.Next = this;
			}
		}

		// Token: 0x060020CB RID: 8395 RVA: 0x000B7881 File Offset: 0x000B5A81
		internal static DoubleLinkAxis ConvertTree(Axis axis)
		{
			if (axis == null)
			{
				return null;
			}
			return new DoubleLinkAxis(axis, DoubleLinkAxis.ConvertTree((Axis)axis.Input));
		}

		// Token: 0x04001793 RID: 6035
		internal Axis next;
	}
}
