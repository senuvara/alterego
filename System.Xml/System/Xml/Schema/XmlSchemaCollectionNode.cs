﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200040E RID: 1038
	internal sealed class XmlSchemaCollectionNode
	{
		// Token: 0x17000850 RID: 2128
		// (get) Token: 0x0600278F RID: 10127 RVA: 0x000DEDCF File Offset: 0x000DCFCF
		// (set) Token: 0x06002790 RID: 10128 RVA: 0x000DEDD7 File Offset: 0x000DCFD7
		internal string NamespaceURI
		{
			get
			{
				return this.namespaceUri;
			}
			set
			{
				this.namespaceUri = value;
			}
		}

		// Token: 0x17000851 RID: 2129
		// (get) Token: 0x06002791 RID: 10129 RVA: 0x000DEDE0 File Offset: 0x000DCFE0
		// (set) Token: 0x06002792 RID: 10130 RVA: 0x000DEDE8 File Offset: 0x000DCFE8
		internal SchemaInfo SchemaInfo
		{
			get
			{
				return this.schemaInfo;
			}
			set
			{
				this.schemaInfo = value;
			}
		}

		// Token: 0x17000852 RID: 2130
		// (get) Token: 0x06002793 RID: 10131 RVA: 0x000DEDF1 File Offset: 0x000DCFF1
		// (set) Token: 0x06002794 RID: 10132 RVA: 0x000DEDF9 File Offset: 0x000DCFF9
		internal XmlSchema Schema
		{
			get
			{
				return this.schema;
			}
			set
			{
				this.schema = value;
			}
		}

		// Token: 0x06002795 RID: 10133 RVA: 0x00002103 File Offset: 0x00000303
		public XmlSchemaCollectionNode()
		{
		}

		// Token: 0x04001C0B RID: 7179
		private string namespaceUri;

		// Token: 0x04001C0C RID: 7180
		private SchemaInfo schemaInfo;

		// Token: 0x04001C0D RID: 7181
		private XmlSchema schema;
	}
}
