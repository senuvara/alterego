﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000395 RID: 917
	internal class Datatype_time : Datatype_dateTimeBase
	{
		// Token: 0x170006FE RID: 1790
		// (get) Token: 0x060022AA RID: 8874 RVA: 0x000BDF29 File Offset: 0x000BC129
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Time;
			}
		}

		// Token: 0x060022AB RID: 8875 RVA: 0x000BDF20 File Offset: 0x000BC120
		internal Datatype_time() : base(XsdDateTimeFlags.Time)
		{
		}
	}
}
