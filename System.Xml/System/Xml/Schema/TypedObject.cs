﻿using System;
using System.Globalization;

namespace System.Xml.Schema
{
	// Token: 0x02000362 RID: 866
	internal class TypedObject
	{
		// Token: 0x1700067D RID: 1661
		// (get) Token: 0x0600213E RID: 8510 RVA: 0x000B916E File Offset: 0x000B736E
		public int Dim
		{
			get
			{
				return this.dim;
			}
		}

		// Token: 0x1700067E RID: 1662
		// (get) Token: 0x0600213F RID: 8511 RVA: 0x000B9176 File Offset: 0x000B7376
		public bool IsList
		{
			get
			{
				return this.isList;
			}
		}

		// Token: 0x1700067F RID: 1663
		// (get) Token: 0x06002140 RID: 8512 RVA: 0x000B917E File Offset: 0x000B737E
		public bool IsDecimal
		{
			get
			{
				return this.dstruct.IsDecimal;
			}
		}

		// Token: 0x17000680 RID: 1664
		// (get) Token: 0x06002141 RID: 8513 RVA: 0x000B918B File Offset: 0x000B738B
		public decimal[] Dvalue
		{
			get
			{
				return this.dstruct.Dvalue;
			}
		}

		// Token: 0x17000681 RID: 1665
		// (get) Token: 0x06002142 RID: 8514 RVA: 0x000B9198 File Offset: 0x000B7398
		// (set) Token: 0x06002143 RID: 8515 RVA: 0x000B91A0 File Offset: 0x000B73A0
		public object Value
		{
			get
			{
				return this.ovalue;
			}
			set
			{
				this.ovalue = value;
			}
		}

		// Token: 0x17000682 RID: 1666
		// (get) Token: 0x06002144 RID: 8516 RVA: 0x000B91A9 File Offset: 0x000B73A9
		// (set) Token: 0x06002145 RID: 8517 RVA: 0x000B91B1 File Offset: 0x000B73B1
		public XmlSchemaDatatype Type
		{
			get
			{
				return this.xsdtype;
			}
			set
			{
				this.xsdtype = value;
			}
		}

		// Token: 0x06002146 RID: 8518 RVA: 0x000B91BC File Offset: 0x000B73BC
		public TypedObject(object obj, string svalue, XmlSchemaDatatype xsdtype)
		{
			this.ovalue = obj;
			this.svalue = svalue;
			this.xsdtype = xsdtype;
			if (xsdtype.Variety == XmlSchemaDatatypeVariety.List || xsdtype is Datatype_base64Binary || xsdtype is Datatype_hexBinary)
			{
				this.isList = true;
				this.dim = ((Array)obj).Length;
			}
		}

		// Token: 0x06002147 RID: 8519 RVA: 0x000B921C File Offset: 0x000B741C
		public override string ToString()
		{
			return this.svalue;
		}

		// Token: 0x06002148 RID: 8520 RVA: 0x000B9224 File Offset: 0x000B7424
		public void SetDecimal()
		{
			if (this.dstruct != null)
			{
				return;
			}
			XmlTypeCode typeCode = this.xsdtype.TypeCode;
			if (typeCode == XmlTypeCode.Decimal || typeCode - XmlTypeCode.Integer <= 12)
			{
				if (this.isList)
				{
					this.dstruct = new TypedObject.DecimalStruct(this.dim);
					for (int i = 0; i < this.dim; i++)
					{
						this.dstruct.Dvalue[i] = Convert.ToDecimal(((Array)this.ovalue).GetValue(i), NumberFormatInfo.InvariantInfo);
					}
				}
				else
				{
					this.dstruct = new TypedObject.DecimalStruct();
					this.dstruct.Dvalue[0] = Convert.ToDecimal(this.ovalue, NumberFormatInfo.InvariantInfo);
				}
				this.dstruct.IsDecimal = true;
				return;
			}
			if (this.isList)
			{
				this.dstruct = new TypedObject.DecimalStruct(this.dim);
				return;
			}
			this.dstruct = new TypedObject.DecimalStruct();
		}

		// Token: 0x06002149 RID: 8521 RVA: 0x000B930C File Offset: 0x000B750C
		private bool ListDValueEquals(TypedObject other)
		{
			for (int i = 0; i < this.Dim; i++)
			{
				if (this.Dvalue[i] != other.Dvalue[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600214A RID: 8522 RVA: 0x000B934C File Offset: 0x000B754C
		public bool Equals(TypedObject other)
		{
			if (this.Dim != other.Dim)
			{
				return false;
			}
			if (this.Type != other.Type)
			{
				if (!this.Type.IsComparable(other.Type))
				{
					return false;
				}
				other.SetDecimal();
				this.SetDecimal();
				if (this.IsDecimal && other.IsDecimal)
				{
					return this.ListDValueEquals(other);
				}
			}
			if (this.IsList)
			{
				if (other.IsList)
				{
					return this.Type.Compare(this.Value, other.Value) == 0;
				}
				Array array = this.Value as Array;
				XmlAtomicValue[] array2 = array as XmlAtomicValue[];
				if (array2 != null)
				{
					return array2.Length == 1 && array2.GetValue(0).Equals(other.Value);
				}
				return array.Length == 1 && array.GetValue(0).Equals(other.Value);
			}
			else
			{
				if (!other.IsList)
				{
					return this.Value.Equals(other.Value);
				}
				Array array3 = other.Value as Array;
				XmlAtomicValue[] array4 = array3 as XmlAtomicValue[];
				if (array4 != null)
				{
					return array4.Length == 1 && array4.GetValue(0).Equals(this.Value);
				}
				return array3.Length == 1 && array3.GetValue(0).Equals(this.Value);
			}
		}

		// Token: 0x040017D4 RID: 6100
		private TypedObject.DecimalStruct dstruct;

		// Token: 0x040017D5 RID: 6101
		private object ovalue;

		// Token: 0x040017D6 RID: 6102
		private string svalue;

		// Token: 0x040017D7 RID: 6103
		private XmlSchemaDatatype xsdtype;

		// Token: 0x040017D8 RID: 6104
		private int dim = 1;

		// Token: 0x040017D9 RID: 6105
		private bool isList;

		// Token: 0x02000363 RID: 867
		private class DecimalStruct
		{
			// Token: 0x17000683 RID: 1667
			// (get) Token: 0x0600214B RID: 8523 RVA: 0x000B9492 File Offset: 0x000B7692
			// (set) Token: 0x0600214C RID: 8524 RVA: 0x000B949A File Offset: 0x000B769A
			public bool IsDecimal
			{
				get
				{
					return this.isDecimal;
				}
				set
				{
					this.isDecimal = value;
				}
			}

			// Token: 0x17000684 RID: 1668
			// (get) Token: 0x0600214D RID: 8525 RVA: 0x000B94A3 File Offset: 0x000B76A3
			public decimal[] Dvalue
			{
				get
				{
					return this.dvalue;
				}
			}

			// Token: 0x0600214E RID: 8526 RVA: 0x000B94AB File Offset: 0x000B76AB
			public DecimalStruct()
			{
				this.dvalue = new decimal[1];
			}

			// Token: 0x0600214F RID: 8527 RVA: 0x000B94BF File Offset: 0x000B76BF
			public DecimalStruct(int dim)
			{
				this.dvalue = new decimal[dim];
			}

			// Token: 0x040017DA RID: 6106
			private bool isDecimal;

			// Token: 0x040017DB RID: 6107
			private decimal[] dvalue;
		}
	}
}
