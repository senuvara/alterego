﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200035C RID: 860
	internal class CompiledIdentityConstraint
	{
		// Token: 0x17000676 RID: 1654
		// (get) Token: 0x0600212C RID: 8492 RVA: 0x000B8DB5 File Offset: 0x000B6FB5
		public CompiledIdentityConstraint.ConstraintRole Role
		{
			get
			{
				return this.role;
			}
		}

		// Token: 0x17000677 RID: 1655
		// (get) Token: 0x0600212D RID: 8493 RVA: 0x000B8DBD File Offset: 0x000B6FBD
		public Asttree Selector
		{
			get
			{
				return this.selector;
			}
		}

		// Token: 0x17000678 RID: 1656
		// (get) Token: 0x0600212E RID: 8494 RVA: 0x000B8DC5 File Offset: 0x000B6FC5
		public Asttree[] Fields
		{
			get
			{
				return this.fields;
			}
		}

		// Token: 0x0600212F RID: 8495 RVA: 0x000B8DCD File Offset: 0x000B6FCD
		private CompiledIdentityConstraint()
		{
		}

		// Token: 0x06002130 RID: 8496 RVA: 0x000B8DEC File Offset: 0x000B6FEC
		public CompiledIdentityConstraint(XmlSchemaIdentityConstraint constraint, XmlNamespaceManager nsmgr)
		{
			this.name = constraint.QualifiedName;
			try
			{
				this.selector = new Asttree(constraint.Selector.XPath, false, nsmgr);
			}
			catch (XmlSchemaException ex)
			{
				ex.SetSource(constraint.Selector);
				throw ex;
			}
			XmlSchemaObjectCollection xmlSchemaObjectCollection = constraint.Fields;
			this.fields = new Asttree[xmlSchemaObjectCollection.Count];
			for (int i = 0; i < xmlSchemaObjectCollection.Count; i++)
			{
				try
				{
					this.fields[i] = new Asttree(((XmlSchemaXPath)xmlSchemaObjectCollection[i]).XPath, true, nsmgr);
				}
				catch (XmlSchemaException ex2)
				{
					ex2.SetSource(constraint.Fields[i]);
					throw ex2;
				}
			}
			if (constraint is XmlSchemaUnique)
			{
				this.role = CompiledIdentityConstraint.ConstraintRole.Unique;
				return;
			}
			if (constraint is XmlSchemaKey)
			{
				this.role = CompiledIdentityConstraint.ConstraintRole.Key;
				return;
			}
			this.role = CompiledIdentityConstraint.ConstraintRole.Keyref;
			this.refer = ((XmlSchemaKeyref)constraint).Refer;
		}

		// Token: 0x06002131 RID: 8497 RVA: 0x000B8EFC File Offset: 0x000B70FC
		// Note: this type is marked as 'beforefieldinit'.
		static CompiledIdentityConstraint()
		{
		}

		// Token: 0x040017BB RID: 6075
		internal XmlQualifiedName name = XmlQualifiedName.Empty;

		// Token: 0x040017BC RID: 6076
		private CompiledIdentityConstraint.ConstraintRole role;

		// Token: 0x040017BD RID: 6077
		private Asttree selector;

		// Token: 0x040017BE RID: 6078
		private Asttree[] fields;

		// Token: 0x040017BF RID: 6079
		internal XmlQualifiedName refer = XmlQualifiedName.Empty;

		// Token: 0x040017C0 RID: 6080
		public static readonly CompiledIdentityConstraint Empty = new CompiledIdentityConstraint();

		// Token: 0x0200035D RID: 861
		public enum ConstraintRole
		{
			// Token: 0x040017C2 RID: 6082
			Unique,
			// Token: 0x040017C3 RID: 6083
			Key,
			// Token: 0x040017C4 RID: 6084
			Keyref
		}
	}
}
