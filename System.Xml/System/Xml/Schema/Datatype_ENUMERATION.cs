﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003BC RID: 956
	internal class Datatype_ENUMERATION : Datatype_NMTOKEN
	{
		// Token: 0x17000768 RID: 1896
		// (get) Token: 0x06002370 RID: 9072 RVA: 0x0007339E File Offset: 0x0007159E
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.ENUMERATION;
			}
		}

		// Token: 0x06002371 RID: 9073 RVA: 0x000BECB6 File Offset: 0x000BCEB6
		public Datatype_ENUMERATION()
		{
		}
	}
}
