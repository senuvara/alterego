﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the W3C XML Schema Definition Language (XSD) schema types.</summary>
	// Token: 0x0200045F RID: 1119
	public enum XmlTypeCode
	{
		/// <summary>No type information.</summary>
		// Token: 0x04001D4E RID: 7502
		None,
		/// <summary>An item such as a node or atomic value.</summary>
		// Token: 0x04001D4F RID: 7503
		Item,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D50 RID: 7504
		Node,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D51 RID: 7505
		Document,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D52 RID: 7506
		Element,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D53 RID: 7507
		Attribute,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D54 RID: 7508
		Namespace,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D55 RID: 7509
		ProcessingInstruction,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D56 RID: 7510
		Comment,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D57 RID: 7511
		Text,
		/// <summary>Any atomic value of a union.</summary>
		// Token: 0x04001D58 RID: 7512
		AnyAtomicType,
		/// <summary>An untyped atomic value.</summary>
		// Token: 0x04001D59 RID: 7513
		UntypedAtomic,
		/// <summary>A W3C XML Schema <see langword="xs:string" /> type.</summary>
		// Token: 0x04001D5A RID: 7514
		String,
		/// <summary>A W3C XML Schema <see langword="xs:boolean" /> type.</summary>
		// Token: 0x04001D5B RID: 7515
		Boolean,
		/// <summary>A W3C XML Schema <see langword="xs:decimal" /> type.</summary>
		// Token: 0x04001D5C RID: 7516
		Decimal,
		/// <summary>A W3C XML Schema <see langword="xs:float" /> type.</summary>
		// Token: 0x04001D5D RID: 7517
		Float,
		/// <summary>A W3C XML Schema <see langword="xs:double" /> type.</summary>
		// Token: 0x04001D5E RID: 7518
		Double,
		/// <summary>A W3C XML Schema <see langword="xs:Duration" /> type.</summary>
		// Token: 0x04001D5F RID: 7519
		Duration,
		/// <summary>A W3C XML Schema <see langword="xs:dateTime" /> type.</summary>
		// Token: 0x04001D60 RID: 7520
		DateTime,
		/// <summary>A W3C XML Schema <see langword="xs:time" /> type.</summary>
		// Token: 0x04001D61 RID: 7521
		Time,
		/// <summary>A W3C XML Schema <see langword="xs:date" /> type.</summary>
		// Token: 0x04001D62 RID: 7522
		Date,
		/// <summary>A W3C XML Schema <see langword="xs:gYearMonth" /> type.</summary>
		// Token: 0x04001D63 RID: 7523
		GYearMonth,
		/// <summary>A W3C XML Schema <see langword="xs:gYear" /> type.</summary>
		// Token: 0x04001D64 RID: 7524
		GYear,
		/// <summary>A W3C XML Schema <see langword="xs:gMonthDay" /> type.</summary>
		// Token: 0x04001D65 RID: 7525
		GMonthDay,
		/// <summary>A W3C XML Schema <see langword="xs:gDay" /> type.</summary>
		// Token: 0x04001D66 RID: 7526
		GDay,
		/// <summary>A W3C XML Schema <see langword="xs:gMonth" /> type.</summary>
		// Token: 0x04001D67 RID: 7527
		GMonth,
		/// <summary>A W3C XML Schema <see langword="xs:hexBinary" /> type.</summary>
		// Token: 0x04001D68 RID: 7528
		HexBinary,
		/// <summary>A W3C XML Schema <see langword="xs:base64Binary" /> type.</summary>
		// Token: 0x04001D69 RID: 7529
		Base64Binary,
		/// <summary>A W3C XML Schema <see langword="xs:anyURI" /> type.</summary>
		// Token: 0x04001D6A RID: 7530
		AnyUri,
		/// <summary>A W3C XML Schema <see langword="xs:QName" /> type.</summary>
		// Token: 0x04001D6B RID: 7531
		QName,
		/// <summary>A W3C XML Schema <see langword="xs:NOTATION" /> type.</summary>
		// Token: 0x04001D6C RID: 7532
		Notation,
		/// <summary>A W3C XML Schema <see langword="xs:normalizedString" /> type.</summary>
		// Token: 0x04001D6D RID: 7533
		NormalizedString,
		/// <summary>A W3C XML Schema <see langword="xs:token" /> type.</summary>
		// Token: 0x04001D6E RID: 7534
		Token,
		/// <summary>A W3C XML Schema <see langword="xs:language" /> type.</summary>
		// Token: 0x04001D6F RID: 7535
		Language,
		/// <summary>A W3C XML Schema <see langword="xs:NMTOKEN" /> type.</summary>
		// Token: 0x04001D70 RID: 7536
		NmToken,
		/// <summary>A W3C XML Schema <see langword="xs:Name" /> type.</summary>
		// Token: 0x04001D71 RID: 7537
		Name,
		/// <summary>A W3C XML Schema <see langword="xs:NCName" /> type.</summary>
		// Token: 0x04001D72 RID: 7538
		NCName,
		/// <summary>A W3C XML Schema <see langword="xs:ID" /> type.</summary>
		// Token: 0x04001D73 RID: 7539
		Id,
		/// <summary>A W3C XML Schema <see langword="xs:IDREF" /> type.</summary>
		// Token: 0x04001D74 RID: 7540
		Idref,
		/// <summary>A W3C XML Schema <see langword="xs:ENTITY" /> type.</summary>
		// Token: 0x04001D75 RID: 7541
		Entity,
		/// <summary>A W3C XML Schema <see langword="xs:integer" /> type.</summary>
		// Token: 0x04001D76 RID: 7542
		Integer,
		/// <summary>A W3C XML Schema <see langword="xs:nonPositiveInteger" /> type.</summary>
		// Token: 0x04001D77 RID: 7543
		NonPositiveInteger,
		/// <summary>A W3C XML Schema <see langword="xs:negativeInteger" /> type.</summary>
		// Token: 0x04001D78 RID: 7544
		NegativeInteger,
		/// <summary>A W3C XML Schema <see langword="xs:long" /> type.</summary>
		// Token: 0x04001D79 RID: 7545
		Long,
		/// <summary>A W3C XML Schema <see langword="xs:int" /> type.</summary>
		// Token: 0x04001D7A RID: 7546
		Int,
		/// <summary>A W3C XML Schema <see langword="xs:short" /> type.</summary>
		// Token: 0x04001D7B RID: 7547
		Short,
		/// <summary>A W3C XML Schema <see langword="xs:byte" /> type.</summary>
		// Token: 0x04001D7C RID: 7548
		Byte,
		/// <summary>A W3C XML Schema <see langword="xs:nonNegativeInteger" /> type.</summary>
		// Token: 0x04001D7D RID: 7549
		NonNegativeInteger,
		/// <summary>A W3C XML Schema <see langword="xs:unsignedLong" /> type.</summary>
		// Token: 0x04001D7E RID: 7550
		UnsignedLong,
		/// <summary>A W3C XML Schema <see langword="xs:unsignedInt" /> type.</summary>
		// Token: 0x04001D7F RID: 7551
		UnsignedInt,
		/// <summary>A W3C XML Schema <see langword="xs:unsignedShort" /> type.</summary>
		// Token: 0x04001D80 RID: 7552
		UnsignedShort,
		/// <summary>A W3C XML Schema <see langword="xs:unsignedByte" /> type.</summary>
		// Token: 0x04001D81 RID: 7553
		UnsignedByte,
		/// <summary>A W3C XML Schema <see langword="xs:positiveInteger" /> type.</summary>
		// Token: 0x04001D82 RID: 7554
		PositiveInteger,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D83 RID: 7555
		YearMonthDuration,
		/// <summary>This value supports the .NET Framework infrastructure and is not intended to be used directly from your code.</summary>
		// Token: 0x04001D84 RID: 7556
		DayTimeDuration
	}
}
