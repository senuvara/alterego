﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003AC RID: 940
	internal class Datatype_integer : Datatype_decimal
	{
		// Token: 0x17000739 RID: 1849
		// (get) Token: 0x06002310 RID: 8976 RVA: 0x000BD96F File Offset: 0x000BBB6F
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Integer;
			}
		}

		// Token: 0x06002311 RID: 8977 RVA: 0x000BE3FC File Offset: 0x000BC5FC
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = this.FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				decimal num;
				ex = XmlConvert.TryToInteger(s, out num);
				if (ex == null)
				{
					ex = this.FacetsChecker.CheckValueFacets(num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002312 RID: 8978 RVA: 0x000BE448 File Offset: 0x000BC648
		public Datatype_integer()
		{
		}
	}
}
