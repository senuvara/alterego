﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="group" /> element with <see langword="ref" /> attribute from the XML Schema as specified by the World Wide Web Consortium (W3C). This class is used within complex types that reference a <see langword="group" /> defined at the <see langword="schema" /> level.</summary>
	// Token: 0x02000431 RID: 1073
	public class XmlSchemaGroupRef : XmlSchemaParticle
	{
		/// <summary>Gets or sets the name of a group defined in this schema (or another schema indicated by the specified namespace).</summary>
		/// <returns>The name of a group defined in this schema.</returns>
		// Token: 0x170008B7 RID: 2231
		// (get) Token: 0x06002896 RID: 10390 RVA: 0x000E0775 File Offset: 0x000DE975
		// (set) Token: 0x06002897 RID: 10391 RVA: 0x000E077D File Offset: 0x000DE97D
		[XmlAttribute("ref")]
		public XmlQualifiedName RefName
		{
			get
			{
				return this.refName;
			}
			set
			{
				this.refName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets one of the <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, <see cref="T:System.Xml.Schema.XmlSchemaAll" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" /> classes, which holds the post-compilation value of the <see langword="Particle" /> property.</summary>
		/// <returns>The post-compilation value of the <see langword="Particle" /> property, which is one of the <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, <see cref="T:System.Xml.Schema.XmlSchemaAll" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" /> classes.</returns>
		// Token: 0x170008B8 RID: 2232
		// (get) Token: 0x06002898 RID: 10392 RVA: 0x000E0796 File Offset: 0x000DE996
		[XmlIgnore]
		public XmlSchemaGroupBase Particle
		{
			get
			{
				return this.particle;
			}
		}

		// Token: 0x06002899 RID: 10393 RVA: 0x000E079E File Offset: 0x000DE99E
		internal void SetParticle(XmlSchemaGroupBase value)
		{
			this.particle = value;
		}

		// Token: 0x170008B9 RID: 2233
		// (get) Token: 0x0600289A RID: 10394 RVA: 0x000E07A7 File Offset: 0x000DE9A7
		// (set) Token: 0x0600289B RID: 10395 RVA: 0x000E07AF File Offset: 0x000DE9AF
		[XmlIgnore]
		internal XmlSchemaGroup Redefined
		{
			get
			{
				return this.refined;
			}
			set
			{
				this.refined = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaGroupRef" /> class.</summary>
		// Token: 0x0600289C RID: 10396 RVA: 0x000E07B8 File Offset: 0x000DE9B8
		public XmlSchemaGroupRef()
		{
		}

		// Token: 0x04001C80 RID: 7296
		private XmlQualifiedName refName = XmlQualifiedName.Empty;

		// Token: 0x04001C81 RID: 7297
		private XmlSchemaGroupBase particle;

		// Token: 0x04001C82 RID: 7298
		private XmlSchemaGroup refined;
	}
}
