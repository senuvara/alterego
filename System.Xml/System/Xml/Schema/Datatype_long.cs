﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003AF RID: 943
	internal class Datatype_long : Datatype_integer
	{
		// Token: 0x1700073F RID: 1855
		// (get) Token: 0x0600231C RID: 8988 RVA: 0x000BE4AC File Offset: 0x000BC6AC
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_long.numeric10FacetsChecker;
			}
		}

		// Token: 0x17000740 RID: 1856
		// (get) Token: 0x0600231D RID: 8989 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool HasValueFacets
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000741 RID: 1857
		// (get) Token: 0x0600231E RID: 8990 RVA: 0x000BE4B3 File Offset: 0x000BC6B3
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Long;
			}
		}

		// Token: 0x0600231F RID: 8991 RVA: 0x000BE4B8 File Offset: 0x000BC6B8
		internal override int Compare(object value1, object value2)
		{
			return ((long)value1).CompareTo(value2);
		}

		// Token: 0x17000742 RID: 1858
		// (get) Token: 0x06002320 RID: 8992 RVA: 0x000BE4D4 File Offset: 0x000BC6D4
		public override Type ValueType
		{
			get
			{
				return Datatype_long.atomicValueType;
			}
		}

		// Token: 0x17000743 RID: 1859
		// (get) Token: 0x06002321 RID: 8993 RVA: 0x000BE4DB File Offset: 0x000BC6DB
		internal override Type ListValueType
		{
			get
			{
				return Datatype_long.listValueType;
			}
		}

		// Token: 0x06002322 RID: 8994 RVA: 0x000BE4E4 File Offset: 0x000BC6E4
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_long.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				long num;
				ex = XmlConvert.TryToInt64(s, out num);
				if (ex == null)
				{
					ex = Datatype_long.numeric10FacetsChecker.CheckValueFacets(num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002323 RID: 8995 RVA: 0x000BE45B File Offset: 0x000BC65B
		public Datatype_long()
		{
		}

		// Token: 0x06002324 RID: 8996 RVA: 0x000BE530 File Offset: 0x000BC730
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_long()
		{
		}

		// Token: 0x040018C0 RID: 6336
		private static readonly Type atomicValueType = typeof(long);

		// Token: 0x040018C1 RID: 6337
		private static readonly Type listValueType = typeof(long[]);

		// Token: 0x040018C2 RID: 6338
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(-9223372036854775808m, 9223372036854775807m);
	}
}
