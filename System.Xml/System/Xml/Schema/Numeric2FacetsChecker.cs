﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x020003C6 RID: 966
	internal class Numeric2FacetsChecker : FacetsChecker
	{
		// Token: 0x060023D8 RID: 9176 RVA: 0x000C18B8 File Offset: 0x000BFAB8
		internal override Exception CheckValueFacets(object value, XmlSchemaDatatype datatype)
		{
			double value2 = datatype.ValueConverter.ToDouble(value);
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023D9 RID: 9177 RVA: 0x000C18DC File Offset: 0x000BFADC
		internal override Exception CheckValueFacets(double value, XmlSchemaDatatype datatype)
		{
			RestrictionFacets restriction = datatype.Restriction;
			RestrictionFlags restrictionFlags = (restriction != null) ? restriction.Flags : ((RestrictionFlags)0);
			XmlValueConverter valueConverter = datatype.ValueConverter;
			if ((restrictionFlags & RestrictionFlags.MaxInclusive) != (RestrictionFlags)0 && value > valueConverter.ToDouble(restriction.MaxInclusive))
			{
				return new XmlSchemaException("The MaxInclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MaxExclusive) != (RestrictionFlags)0 && value >= valueConverter.ToDouble(restriction.MaxExclusive))
			{
				return new XmlSchemaException("The MaxExclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MinInclusive) != (RestrictionFlags)0 && value < valueConverter.ToDouble(restriction.MinInclusive))
			{
				return new XmlSchemaException("The MinInclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MinExclusive) != (RestrictionFlags)0 && value <= valueConverter.ToDouble(restriction.MinExclusive))
			{
				return new XmlSchemaException("The MinExclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.Enumeration) != (RestrictionFlags)0 && !this.MatchEnumeration(value, restriction.Enumeration, valueConverter))
			{
				return new XmlSchemaException("The Enumeration constraint failed.", string.Empty);
			}
			return null;
		}

		// Token: 0x060023DA RID: 9178 RVA: 0x000C19C8 File Offset: 0x000BFBC8
		internal override Exception CheckValueFacets(float value, XmlSchemaDatatype datatype)
		{
			double value2 = (double)value;
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023DB RID: 9179 RVA: 0x000C19E0 File Offset: 0x000BFBE0
		internal override bool MatchEnumeration(object value, ArrayList enumeration, XmlSchemaDatatype datatype)
		{
			return this.MatchEnumeration(datatype.ValueConverter.ToDouble(value), enumeration, datatype.ValueConverter);
		}

		// Token: 0x060023DC RID: 9180 RVA: 0x000C19FC File Offset: 0x000BFBFC
		private bool MatchEnumeration(double value, ArrayList enumeration, XmlValueConverter valueConverter)
		{
			for (int i = 0; i < enumeration.Count; i++)
			{
				if (value == valueConverter.ToDouble(enumeration[i]))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060023DD RID: 9181 RVA: 0x000C1A2D File Offset: 0x000BFC2D
		public Numeric2FacetsChecker()
		{
		}
	}
}
