﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000378 RID: 888
	internal struct RangePositionInfo
	{
		// Token: 0x0400180E RID: 6158
		public BitSet curpos;

		// Token: 0x0400180F RID: 6159
		public decimal[] rangeCounters;
	}
}
