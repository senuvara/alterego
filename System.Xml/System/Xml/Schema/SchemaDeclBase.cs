﻿using System;
using System.Collections.Generic;

namespace System.Xml.Schema
{
	// Token: 0x020003E2 RID: 994
	internal abstract class SchemaDeclBase
	{
		// Token: 0x0600251B RID: 9499 RVA: 0x000D2250 File Offset: 0x000D0450
		protected SchemaDeclBase(XmlQualifiedName name, string prefix)
		{
			this.name = name;
			this.prefix = prefix;
			this.maxLength = -1L;
			this.minLength = -1L;
		}

		// Token: 0x0600251C RID: 9500 RVA: 0x000D2281 File Offset: 0x000D0481
		protected SchemaDeclBase()
		{
		}

		// Token: 0x1700079B RID: 1947
		// (get) Token: 0x0600251D RID: 9501 RVA: 0x000D2294 File Offset: 0x000D0494
		// (set) Token: 0x0600251E RID: 9502 RVA: 0x000D229C File Offset: 0x000D049C
		internal XmlQualifiedName Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x1700079C RID: 1948
		// (get) Token: 0x0600251F RID: 9503 RVA: 0x000D22A5 File Offset: 0x000D04A5
		// (set) Token: 0x06002520 RID: 9504 RVA: 0x000D22BB File Offset: 0x000D04BB
		internal string Prefix
		{
			get
			{
				if (this.prefix != null)
				{
					return this.prefix;
				}
				return string.Empty;
			}
			set
			{
				this.prefix = value;
			}
		}

		// Token: 0x1700079D RID: 1949
		// (get) Token: 0x06002521 RID: 9505 RVA: 0x000D22C4 File Offset: 0x000D04C4
		// (set) Token: 0x06002522 RID: 9506 RVA: 0x000D22CC File Offset: 0x000D04CC
		internal bool IsDeclaredInExternal
		{
			get
			{
				return this.isDeclaredInExternal;
			}
			set
			{
				this.isDeclaredInExternal = value;
			}
		}

		// Token: 0x1700079E RID: 1950
		// (get) Token: 0x06002523 RID: 9507 RVA: 0x000D22D5 File Offset: 0x000D04D5
		// (set) Token: 0x06002524 RID: 9508 RVA: 0x000D22DD File Offset: 0x000D04DD
		internal SchemaDeclBase.Use Presence
		{
			get
			{
				return this.presence;
			}
			set
			{
				this.presence = value;
			}
		}

		// Token: 0x1700079F RID: 1951
		// (get) Token: 0x06002525 RID: 9509 RVA: 0x000D22E6 File Offset: 0x000D04E6
		// (set) Token: 0x06002526 RID: 9510 RVA: 0x000D22EE File Offset: 0x000D04EE
		internal long MaxLength
		{
			get
			{
				return this.maxLength;
			}
			set
			{
				this.maxLength = value;
			}
		}

		// Token: 0x170007A0 RID: 1952
		// (get) Token: 0x06002527 RID: 9511 RVA: 0x000D22F7 File Offset: 0x000D04F7
		// (set) Token: 0x06002528 RID: 9512 RVA: 0x000D22FF File Offset: 0x000D04FF
		internal long MinLength
		{
			get
			{
				return this.minLength;
			}
			set
			{
				this.minLength = value;
			}
		}

		// Token: 0x170007A1 RID: 1953
		// (get) Token: 0x06002529 RID: 9513 RVA: 0x000D2308 File Offset: 0x000D0508
		// (set) Token: 0x0600252A RID: 9514 RVA: 0x000D2310 File Offset: 0x000D0510
		internal XmlSchemaType SchemaType
		{
			get
			{
				return this.schemaType;
			}
			set
			{
				this.schemaType = value;
			}
		}

		// Token: 0x170007A2 RID: 1954
		// (get) Token: 0x0600252B RID: 9515 RVA: 0x000D2319 File Offset: 0x000D0519
		// (set) Token: 0x0600252C RID: 9516 RVA: 0x000D2321 File Offset: 0x000D0521
		internal XmlSchemaDatatype Datatype
		{
			get
			{
				return this.datatype;
			}
			set
			{
				this.datatype = value;
			}
		}

		// Token: 0x0600252D RID: 9517 RVA: 0x000D232A File Offset: 0x000D052A
		internal void AddValue(string value)
		{
			if (this.values == null)
			{
				this.values = new List<string>();
			}
			this.values.Add(value);
		}

		// Token: 0x170007A3 RID: 1955
		// (get) Token: 0x0600252E RID: 9518 RVA: 0x000D234B File Offset: 0x000D054B
		// (set) Token: 0x0600252F RID: 9519 RVA: 0x000D2353 File Offset: 0x000D0553
		internal List<string> Values
		{
			get
			{
				return this.values;
			}
			set
			{
				this.values = value;
			}
		}

		// Token: 0x170007A4 RID: 1956
		// (get) Token: 0x06002530 RID: 9520 RVA: 0x000D235C File Offset: 0x000D055C
		// (set) Token: 0x06002531 RID: 9521 RVA: 0x000D2372 File Offset: 0x000D0572
		internal string DefaultValueRaw
		{
			get
			{
				if (this.defaultValueRaw == null)
				{
					return string.Empty;
				}
				return this.defaultValueRaw;
			}
			set
			{
				this.defaultValueRaw = value;
			}
		}

		// Token: 0x170007A5 RID: 1957
		// (get) Token: 0x06002532 RID: 9522 RVA: 0x000D237B File Offset: 0x000D057B
		// (set) Token: 0x06002533 RID: 9523 RVA: 0x000D2383 File Offset: 0x000D0583
		internal object DefaultValueTyped
		{
			get
			{
				return this.defaultValueTyped;
			}
			set
			{
				this.defaultValueTyped = value;
			}
		}

		// Token: 0x06002534 RID: 9524 RVA: 0x000D238C File Offset: 0x000D058C
		internal bool CheckEnumeration(object pVal)
		{
			return (this.datatype.TokenizedType != XmlTokenizedType.NOTATION && this.datatype.TokenizedType != XmlTokenizedType.ENUMERATION) || this.values.Contains(pVal.ToString());
		}

		// Token: 0x06002535 RID: 9525 RVA: 0x000D23BE File Offset: 0x000D05BE
		internal bool CheckValue(object pVal)
		{
			return (this.presence != SchemaDeclBase.Use.Fixed && this.presence != SchemaDeclBase.Use.RequiredFixed) || (this.defaultValueTyped != null && this.datatype.IsEqual(pVal, this.defaultValueTyped));
		}

		// Token: 0x040019B3 RID: 6579
		protected XmlQualifiedName name = XmlQualifiedName.Empty;

		// Token: 0x040019B4 RID: 6580
		protected string prefix;

		// Token: 0x040019B5 RID: 6581
		protected bool isDeclaredInExternal;

		// Token: 0x040019B6 RID: 6582
		protected SchemaDeclBase.Use presence;

		// Token: 0x040019B7 RID: 6583
		protected XmlSchemaType schemaType;

		// Token: 0x040019B8 RID: 6584
		protected XmlSchemaDatatype datatype;

		// Token: 0x040019B9 RID: 6585
		protected string defaultValueRaw;

		// Token: 0x040019BA RID: 6586
		protected object defaultValueTyped;

		// Token: 0x040019BB RID: 6587
		protected long maxLength;

		// Token: 0x040019BC RID: 6588
		protected long minLength;

		// Token: 0x040019BD RID: 6589
		protected List<string> values;

		// Token: 0x020003E3 RID: 995
		internal enum Use
		{
			// Token: 0x040019BF RID: 6591
			Default,
			// Token: 0x040019C0 RID: 6592
			Required,
			// Token: 0x040019C1 RID: 6593
			Implied,
			// Token: 0x040019C2 RID: 6594
			Fixed,
			// Token: 0x040019C3 RID: 6595
			RequiredFixed
		}
	}
}
