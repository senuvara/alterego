﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B9 RID: 953
	internal class Datatype_doubleXdr : Datatype_double
	{
		// Token: 0x06002366 RID: 9062 RVA: 0x000BEB2C File Offset: 0x000BCD2C
		public override object ParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr)
		{
			double num;
			try
			{
				num = XmlConvert.ToDouble(s);
			}
			catch (Exception innerException)
			{
				throw new XmlSchemaException(Res.GetString("The value '{0}' is invalid according to its data type.", new object[]
				{
					s
				}), innerException);
			}
			if (double.IsInfinity(num) || double.IsNaN(num))
			{
				throw new XmlSchemaException("The value '{0}' is invalid according to its data type.", s);
			}
			return num;
		}

		// Token: 0x06002367 RID: 9063 RVA: 0x000BEB90 File Offset: 0x000BCD90
		public Datatype_doubleXdr()
		{
		}
	}
}
