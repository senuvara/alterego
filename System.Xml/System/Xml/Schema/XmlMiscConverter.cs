﻿using System;
using System.Xml.XPath;

namespace System.Xml.Schema
{
	// Token: 0x02000466 RID: 1126
	internal class XmlMiscConverter : XmlBaseConverter
	{
		// Token: 0x06002BC1 RID: 11201 RVA: 0x000E9094 File Offset: 0x000E7294
		protected XmlMiscConverter(XmlSchemaType schemaType) : base(schemaType)
		{
		}

		// Token: 0x06002BC2 RID: 11202 RVA: 0x000EAE3E File Offset: 0x000E903E
		public static XmlValueConverter Create(XmlSchemaType schemaType)
		{
			return new XmlMiscConverter(schemaType);
		}

		// Token: 0x06002BC3 RID: 11203 RVA: 0x000E938F File Offset: 0x000E758F
		public override string ToString(string value, IXmlNamespaceResolver nsResolver)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return value;
		}

		// Token: 0x06002BC4 RID: 11204 RVA: 0x000EAE48 File Offset: 0x000E9048
		public override string ToString(object value, IXmlNamespaceResolver nsResolver)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			Type type = value.GetType();
			if (type == XmlBaseConverter.ByteArrayType)
			{
				XmlTypeCode typeCode = base.TypeCode;
				if (typeCode == XmlTypeCode.HexBinary)
				{
					return XmlConvert.ToBinHexString((byte[])value);
				}
				if (typeCode == XmlTypeCode.Base64Binary)
				{
					return XmlBaseConverter.Base64BinaryToString((byte[])value);
				}
			}
			if (type == XmlBaseConverter.StringType)
			{
				return (string)value;
			}
			if (XmlBaseConverter.IsDerivedFrom(type, XmlBaseConverter.UriType) && base.TypeCode == XmlTypeCode.AnyUri)
			{
				return XmlBaseConverter.AnyUriToString((Uri)value);
			}
			if (type == XmlBaseConverter.TimeSpanType)
			{
				XmlTypeCode typeCode = base.TypeCode;
				if (typeCode == XmlTypeCode.Duration)
				{
					return XmlBaseConverter.DurationToString((TimeSpan)value);
				}
				if (typeCode == XmlTypeCode.YearMonthDuration)
				{
					return XmlBaseConverter.YearMonthDurationToString((TimeSpan)value);
				}
				if (typeCode == XmlTypeCode.DayTimeDuration)
				{
					return XmlBaseConverter.DayTimeDurationToString((TimeSpan)value);
				}
			}
			if (XmlBaseConverter.IsDerivedFrom(type, XmlBaseConverter.XmlQualifiedNameType))
			{
				XmlTypeCode typeCode = base.TypeCode;
				if (typeCode == XmlTypeCode.QName)
				{
					return XmlBaseConverter.QNameToString((XmlQualifiedName)value, nsResolver);
				}
				if (typeCode == XmlTypeCode.Notation)
				{
					return XmlBaseConverter.QNameToString((XmlQualifiedName)value, nsResolver);
				}
			}
			return (string)this.ChangeTypeWildcardDestination(value, XmlBaseConverter.StringType, nsResolver);
		}

		// Token: 0x06002BC5 RID: 11205 RVA: 0x000EAF68 File Offset: 0x000E9168
		public override object ChangeType(string value, Type destinationType, IXmlNamespaceResolver nsResolver)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}
			if (destinationType == XmlBaseConverter.ObjectType)
			{
				destinationType = base.DefaultClrType;
			}
			if (destinationType == XmlBaseConverter.ByteArrayType)
			{
				XmlTypeCode typeCode = base.TypeCode;
				if (typeCode == XmlTypeCode.HexBinary)
				{
					return XmlBaseConverter.StringToHexBinary(value);
				}
				if (typeCode == XmlTypeCode.Base64Binary)
				{
					return XmlBaseConverter.StringToBase64Binary(value);
				}
			}
			if (destinationType == XmlBaseConverter.XmlQualifiedNameType)
			{
				XmlTypeCode typeCode = base.TypeCode;
				if (typeCode == XmlTypeCode.QName)
				{
					return XmlBaseConverter.StringToQName(value, nsResolver);
				}
				if (typeCode == XmlTypeCode.Notation)
				{
					return XmlBaseConverter.StringToQName(value, nsResolver);
				}
			}
			if (destinationType == XmlBaseConverter.StringType)
			{
				return value;
			}
			if (destinationType == XmlBaseConverter.TimeSpanType)
			{
				XmlTypeCode typeCode = base.TypeCode;
				if (typeCode == XmlTypeCode.Duration)
				{
					return XmlBaseConverter.StringToDuration(value);
				}
				if (typeCode == XmlTypeCode.YearMonthDuration)
				{
					return XmlBaseConverter.StringToYearMonthDuration(value);
				}
				if (typeCode == XmlTypeCode.DayTimeDuration)
				{
					return XmlBaseConverter.StringToDayTimeDuration(value);
				}
			}
			if (destinationType == XmlBaseConverter.UriType && base.TypeCode == XmlTypeCode.AnyUri)
			{
				return XmlConvert.ToUri(value);
			}
			if (destinationType == XmlBaseConverter.XmlAtomicValueType)
			{
				return new XmlAtomicValue(base.SchemaType, value, nsResolver);
			}
			return this.ChangeTypeWildcardSource(value, destinationType, nsResolver);
		}

		// Token: 0x06002BC6 RID: 11206 RVA: 0x000EB0A0 File Offset: 0x000E92A0
		public override object ChangeType(object value, Type destinationType, IXmlNamespaceResolver nsResolver)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}
			Type type = value.GetType();
			if (destinationType == XmlBaseConverter.ObjectType)
			{
				destinationType = base.DefaultClrType;
			}
			if (destinationType == XmlBaseConverter.ByteArrayType)
			{
				if (type == XmlBaseConverter.ByteArrayType)
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.HexBinary)
					{
						return (byte[])value;
					}
					if (typeCode == XmlTypeCode.Base64Binary)
					{
						return (byte[])value;
					}
				}
				if (type == XmlBaseConverter.StringType)
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.HexBinary)
					{
						return XmlBaseConverter.StringToHexBinary((string)value);
					}
					if (typeCode == XmlTypeCode.Base64Binary)
					{
						return XmlBaseConverter.StringToBase64Binary((string)value);
					}
				}
			}
			if (destinationType == XmlBaseConverter.XmlQualifiedNameType)
			{
				if (type == XmlBaseConverter.StringType)
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.QName)
					{
						return XmlBaseConverter.StringToQName((string)value, nsResolver);
					}
					if (typeCode == XmlTypeCode.Notation)
					{
						return XmlBaseConverter.StringToQName((string)value, nsResolver);
					}
				}
				if (XmlBaseConverter.IsDerivedFrom(type, XmlBaseConverter.XmlQualifiedNameType))
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.QName)
					{
						return (XmlQualifiedName)value;
					}
					if (typeCode == XmlTypeCode.Notation)
					{
						return (XmlQualifiedName)value;
					}
				}
			}
			if (destinationType == XmlBaseConverter.StringType)
			{
				return this.ToString(value, nsResolver);
			}
			if (destinationType == XmlBaseConverter.TimeSpanType)
			{
				if (type == XmlBaseConverter.StringType)
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.Duration)
					{
						return XmlBaseConverter.StringToDuration((string)value);
					}
					if (typeCode == XmlTypeCode.YearMonthDuration)
					{
						return XmlBaseConverter.StringToYearMonthDuration((string)value);
					}
					if (typeCode == XmlTypeCode.DayTimeDuration)
					{
						return XmlBaseConverter.StringToDayTimeDuration((string)value);
					}
				}
				if (type == XmlBaseConverter.TimeSpanType)
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.Duration)
					{
						return (TimeSpan)value;
					}
					if (typeCode == XmlTypeCode.YearMonthDuration)
					{
						return (TimeSpan)value;
					}
					if (typeCode == XmlTypeCode.DayTimeDuration)
					{
						return (TimeSpan)value;
					}
				}
			}
			if (destinationType == XmlBaseConverter.UriType)
			{
				if (type == XmlBaseConverter.StringType && base.TypeCode == XmlTypeCode.AnyUri)
				{
					return XmlConvert.ToUri((string)value);
				}
				if (XmlBaseConverter.IsDerivedFrom(type, XmlBaseConverter.UriType) && base.TypeCode == XmlTypeCode.AnyUri)
				{
					return (Uri)value;
				}
			}
			if (destinationType == XmlBaseConverter.XmlAtomicValueType)
			{
				if (type == XmlBaseConverter.ByteArrayType)
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.HexBinary)
					{
						return new XmlAtomicValue(base.SchemaType, value);
					}
					if (typeCode == XmlTypeCode.Base64Binary)
					{
						return new XmlAtomicValue(base.SchemaType, value);
					}
				}
				if (type == XmlBaseConverter.StringType)
				{
					return new XmlAtomicValue(base.SchemaType, (string)value, nsResolver);
				}
				if (type == XmlBaseConverter.TimeSpanType)
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.Duration)
					{
						return new XmlAtomicValue(base.SchemaType, value);
					}
					if (typeCode == XmlTypeCode.YearMonthDuration)
					{
						return new XmlAtomicValue(base.SchemaType, value);
					}
					if (typeCode == XmlTypeCode.DayTimeDuration)
					{
						return new XmlAtomicValue(base.SchemaType, value);
					}
				}
				if (XmlBaseConverter.IsDerivedFrom(type, XmlBaseConverter.UriType) && base.TypeCode == XmlTypeCode.AnyUri)
				{
					return new XmlAtomicValue(base.SchemaType, value);
				}
				if (type == XmlBaseConverter.XmlAtomicValueType)
				{
					return (XmlAtomicValue)value;
				}
				if (XmlBaseConverter.IsDerivedFrom(type, XmlBaseConverter.XmlQualifiedNameType))
				{
					XmlTypeCode typeCode = base.TypeCode;
					if (typeCode == XmlTypeCode.QName)
					{
						return new XmlAtomicValue(base.SchemaType, value, nsResolver);
					}
					if (typeCode == XmlTypeCode.Notation)
					{
						return new XmlAtomicValue(base.SchemaType, value, nsResolver);
					}
				}
			}
			if (destinationType == XmlBaseConverter.XPathItemType && type == XmlBaseConverter.XmlAtomicValueType)
			{
				return (XmlAtomicValue)value;
			}
			if (destinationType == XmlBaseConverter.XPathItemType)
			{
				return (XPathItem)this.ChangeType(value, XmlBaseConverter.XmlAtomicValueType, nsResolver);
			}
			if (type == XmlBaseConverter.XmlAtomicValueType)
			{
				return ((XmlAtomicValue)value).ValueAs(destinationType, nsResolver);
			}
			return this.ChangeListType(value, destinationType, nsResolver);
		}

		// Token: 0x06002BC7 RID: 11207 RVA: 0x000EB465 File Offset: 0x000E9665
		private object ChangeTypeWildcardDestination(object value, Type destinationType, IXmlNamespaceResolver nsResolver)
		{
			if (value.GetType() == XmlBaseConverter.XmlAtomicValueType)
			{
				return ((XmlAtomicValue)value).ValueAs(destinationType, nsResolver);
			}
			return this.ChangeListType(value, destinationType, nsResolver);
		}

		// Token: 0x06002BC8 RID: 11208 RVA: 0x000EB490 File Offset: 0x000E9690
		private object ChangeTypeWildcardSource(object value, Type destinationType, IXmlNamespaceResolver nsResolver)
		{
			if (destinationType == XmlBaseConverter.XPathItemType)
			{
				return (XPathItem)this.ChangeType(value, XmlBaseConverter.XmlAtomicValueType, nsResolver);
			}
			return this.ChangeListType(value, destinationType, nsResolver);
		}
	}
}
