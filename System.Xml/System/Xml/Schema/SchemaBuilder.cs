﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003DE RID: 990
	internal abstract class SchemaBuilder
	{
		// Token: 0x060024B0 RID: 9392
		internal abstract bool ProcessElement(string prefix, string name, string ns);

		// Token: 0x060024B1 RID: 9393
		internal abstract void ProcessAttribute(string prefix, string name, string ns, string value);

		// Token: 0x060024B2 RID: 9394
		internal abstract bool IsContentParsed();

		// Token: 0x060024B3 RID: 9395
		internal abstract void ProcessMarkup(XmlNode[] markup);

		// Token: 0x060024B4 RID: 9396
		internal abstract void ProcessCData(string value);

		// Token: 0x060024B5 RID: 9397
		internal abstract void StartChildren();

		// Token: 0x060024B6 RID: 9398
		internal abstract void EndChildren();

		// Token: 0x060024B7 RID: 9399 RVA: 0x00002103 File Offset: 0x00000303
		protected SchemaBuilder()
		{
		}
	}
}
