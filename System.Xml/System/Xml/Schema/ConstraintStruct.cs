﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x0200035E RID: 862
	internal sealed class ConstraintStruct
	{
		// Token: 0x17000679 RID: 1657
		// (get) Token: 0x06002132 RID: 8498 RVA: 0x000B8F08 File Offset: 0x000B7108
		internal int TableDim
		{
			get
			{
				return this.tableDim;
			}
		}

		// Token: 0x06002133 RID: 8499 RVA: 0x000B8F10 File Offset: 0x000B7110
		internal ConstraintStruct(CompiledIdentityConstraint constraint)
		{
			this.constraint = constraint;
			this.tableDim = constraint.Fields.Length;
			this.axisFields = new ArrayList();
			this.axisSelector = new SelectorActiveAxis(constraint.Selector, this);
			if (this.constraint.Role != CompiledIdentityConstraint.ConstraintRole.Keyref)
			{
				this.qualifiedTable = new Hashtable();
			}
		}

		// Token: 0x040017C5 RID: 6085
		internal CompiledIdentityConstraint constraint;

		// Token: 0x040017C6 RID: 6086
		internal SelectorActiveAxis axisSelector;

		// Token: 0x040017C7 RID: 6087
		internal ArrayList axisFields;

		// Token: 0x040017C8 RID: 6088
		internal Hashtable qualifiedTable;

		// Token: 0x040017C9 RID: 6089
		internal Hashtable keyrefTable;

		// Token: 0x040017CA RID: 6090
		private int tableDim;
	}
}
