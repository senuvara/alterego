﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003D5 RID: 981
	internal class NamespaceListV1Compat : NamespaceList
	{
		// Token: 0x06002443 RID: 9283 RVA: 0x000C6659 File Offset: 0x000C4859
		public NamespaceListV1Compat(string namespaces, string targetNamespace) : base(namespaces, targetNamespace)
		{
		}

		// Token: 0x06002444 RID: 9284 RVA: 0x000C6663 File Offset: 0x000C4863
		public override bool Allows(string ns)
		{
			if (base.Type == NamespaceList.ListType.Other)
			{
				return ns != base.Excluded;
			}
			return base.Allows(ns);
		}
	}
}
