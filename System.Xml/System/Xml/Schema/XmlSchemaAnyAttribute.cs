﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the World Wide Web Consortium (W3C) <see langword="anyAttribute" /> element.</summary>
	// Token: 0x02000407 RID: 1031
	public class XmlSchemaAnyAttribute : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the namespaces containing the attributes that can be used.</summary>
		/// <returns>Namespaces for attributes that are available for use. The default is <see langword="##any" />.Optional.</returns>
		// Token: 0x17000826 RID: 2086
		// (get) Token: 0x06002726 RID: 10022 RVA: 0x000DE38B File Offset: 0x000DC58B
		// (set) Token: 0x06002727 RID: 10023 RVA: 0x000DE393 File Offset: 0x000DC593
		[XmlAttribute("namespace")]
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		/// <summary>Gets or sets information about how an application or XML processor should handle the validation of XML documents for the attributes specified by the <see langword="anyAttribute" /> element.</summary>
		/// <returns>One of the <see cref="T:System.Xml.Schema.XmlSchemaContentProcessing" /> values. If no <see langword="processContents" /> attribute is specified, the default is <see langword="Strict" />.</returns>
		// Token: 0x17000827 RID: 2087
		// (get) Token: 0x06002728 RID: 10024 RVA: 0x000DE39C File Offset: 0x000DC59C
		// (set) Token: 0x06002729 RID: 10025 RVA: 0x000DE3A4 File Offset: 0x000DC5A4
		[DefaultValue(XmlSchemaContentProcessing.None)]
		[XmlAttribute("processContents")]
		public XmlSchemaContentProcessing ProcessContents
		{
			get
			{
				return this.processContents;
			}
			set
			{
				this.processContents = value;
			}
		}

		// Token: 0x17000828 RID: 2088
		// (get) Token: 0x0600272A RID: 10026 RVA: 0x000DE3AD File Offset: 0x000DC5AD
		[XmlIgnore]
		internal NamespaceList NamespaceList
		{
			get
			{
				return this.namespaceList;
			}
		}

		// Token: 0x17000829 RID: 2089
		// (get) Token: 0x0600272B RID: 10027 RVA: 0x000DE3B5 File Offset: 0x000DC5B5
		[XmlIgnore]
		internal XmlSchemaContentProcessing ProcessContentsCorrect
		{
			get
			{
				if (this.processContents != XmlSchemaContentProcessing.None)
				{
					return this.processContents;
				}
				return XmlSchemaContentProcessing.Strict;
			}
		}

		// Token: 0x0600272C RID: 10028 RVA: 0x000DE3C7 File Offset: 0x000DC5C7
		internal void BuildNamespaceList(string targetNamespace)
		{
			if (this.ns != null)
			{
				this.namespaceList = new NamespaceList(this.ns, targetNamespace);
				return;
			}
			this.namespaceList = new NamespaceList();
		}

		// Token: 0x0600272D RID: 10029 RVA: 0x000DE3EF File Offset: 0x000DC5EF
		internal void BuildNamespaceListV1Compat(string targetNamespace)
		{
			if (this.ns != null)
			{
				this.namespaceList = new NamespaceListV1Compat(this.ns, targetNamespace);
				return;
			}
			this.namespaceList = new NamespaceList();
		}

		// Token: 0x0600272E RID: 10030 RVA: 0x000DE417 File Offset: 0x000DC617
		internal bool Allows(XmlQualifiedName qname)
		{
			return this.namespaceList.Allows(qname.Namespace);
		}

		// Token: 0x0600272F RID: 10031 RVA: 0x000DE42A File Offset: 0x000DC62A
		internal static bool IsSubset(XmlSchemaAnyAttribute sub, XmlSchemaAnyAttribute super)
		{
			return NamespaceList.IsSubset(sub.NamespaceList, super.NamespaceList);
		}

		// Token: 0x06002730 RID: 10032 RVA: 0x000DE440 File Offset: 0x000DC640
		internal static XmlSchemaAnyAttribute Intersection(XmlSchemaAnyAttribute o1, XmlSchemaAnyAttribute o2, bool v1Compat)
		{
			NamespaceList namespaceList = NamespaceList.Intersection(o1.NamespaceList, o2.NamespaceList, v1Compat);
			if (namespaceList != null)
			{
				return new XmlSchemaAnyAttribute
				{
					namespaceList = namespaceList,
					ProcessContents = o1.ProcessContents,
					Annotation = o1.Annotation
				};
			}
			return null;
		}

		// Token: 0x06002731 RID: 10033 RVA: 0x000DE48C File Offset: 0x000DC68C
		internal static XmlSchemaAnyAttribute Union(XmlSchemaAnyAttribute o1, XmlSchemaAnyAttribute o2, bool v1Compat)
		{
			NamespaceList namespaceList = NamespaceList.Union(o1.NamespaceList, o2.NamespaceList, v1Compat);
			if (namespaceList != null)
			{
				return new XmlSchemaAnyAttribute
				{
					namespaceList = namespaceList,
					processContents = o1.processContents,
					Annotation = o1.Annotation
				};
			}
			return null;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAnyAttribute" /> class.</summary>
		// Token: 0x06002732 RID: 10034 RVA: 0x000DE4D5 File Offset: 0x000DC6D5
		public XmlSchemaAnyAttribute()
		{
		}

		// Token: 0x04001BE9 RID: 7145
		private string ns;

		// Token: 0x04001BEA RID: 7146
		private XmlSchemaContentProcessing processContents;

		// Token: 0x04001BEB RID: 7147
		private NamespaceList namespaceList;
	}
}
