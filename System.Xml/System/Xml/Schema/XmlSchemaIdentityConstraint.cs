﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Class for the identity constraints: <see langword="key" />, <see langword="keyref" />, and <see langword="unique" /> elements.</summary>
	// Token: 0x02000432 RID: 1074
	public class XmlSchemaIdentityConstraint : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the name of the identity constraint.</summary>
		/// <returns>The name of the identity constraint.</returns>
		// Token: 0x170008BA RID: 2234
		// (get) Token: 0x0600289D RID: 10397 RVA: 0x000E07CB File Offset: 0x000DE9CB
		// (set) Token: 0x0600289E RID: 10398 RVA: 0x000E07D3 File Offset: 0x000DE9D3
		[XmlAttribute("name")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets the XPath expression <see langword="selector" /> element.</summary>
		/// <returns>The XPath expression <see langword="selector" /> element.</returns>
		// Token: 0x170008BB RID: 2235
		// (get) Token: 0x0600289F RID: 10399 RVA: 0x000E07DC File Offset: 0x000DE9DC
		// (set) Token: 0x060028A0 RID: 10400 RVA: 0x000E07E4 File Offset: 0x000DE9E4
		[XmlElement("selector", typeof(XmlSchemaXPath))]
		public XmlSchemaXPath Selector
		{
			get
			{
				return this.selector;
			}
			set
			{
				this.selector = value;
			}
		}

		/// <summary>Gets the collection of fields that apply as children for the XML Path Language (XPath) expression selector.</summary>
		/// <returns>The collection of fields.</returns>
		// Token: 0x170008BC RID: 2236
		// (get) Token: 0x060028A1 RID: 10401 RVA: 0x000E07ED File Offset: 0x000DE9ED
		[XmlElement("field", typeof(XmlSchemaXPath))]
		public XmlSchemaObjectCollection Fields
		{
			get
			{
				return this.fields;
			}
		}

		/// <summary>Gets the qualified name of the identity constraint, which holds the post-compilation value of the <see langword="QualifiedName" /> property.</summary>
		/// <returns>The post-compilation value of the <see langword="QualifiedName" /> property.</returns>
		// Token: 0x170008BD RID: 2237
		// (get) Token: 0x060028A2 RID: 10402 RVA: 0x000E07F5 File Offset: 0x000DE9F5
		[XmlIgnore]
		public XmlQualifiedName QualifiedName
		{
			get
			{
				return this.qualifiedName;
			}
		}

		// Token: 0x060028A3 RID: 10403 RVA: 0x000E07FD File Offset: 0x000DE9FD
		internal void SetQualifiedName(XmlQualifiedName value)
		{
			this.qualifiedName = value;
		}

		// Token: 0x170008BE RID: 2238
		// (get) Token: 0x060028A4 RID: 10404 RVA: 0x000E0806 File Offset: 0x000DEA06
		// (set) Token: 0x060028A5 RID: 10405 RVA: 0x000E080E File Offset: 0x000DEA0E
		[XmlIgnore]
		internal CompiledIdentityConstraint CompiledConstraint
		{
			get
			{
				return this.compiledConstraint;
			}
			set
			{
				this.compiledConstraint = value;
			}
		}

		// Token: 0x170008BF RID: 2239
		// (get) Token: 0x060028A6 RID: 10406 RVA: 0x000E0817 File Offset: 0x000DEA17
		// (set) Token: 0x060028A7 RID: 10407 RVA: 0x000E081F File Offset: 0x000DEA1F
		[XmlIgnore]
		internal override string NameAttribute
		{
			get
			{
				return this.Name;
			}
			set
			{
				this.Name = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaIdentityConstraint" /> class.</summary>
		// Token: 0x060028A8 RID: 10408 RVA: 0x000E0828 File Offset: 0x000DEA28
		public XmlSchemaIdentityConstraint()
		{
		}

		// Token: 0x04001C83 RID: 7299
		private string name;

		// Token: 0x04001C84 RID: 7300
		private XmlSchemaXPath selector;

		// Token: 0x04001C85 RID: 7301
		private XmlSchemaObjectCollection fields = new XmlSchemaObjectCollection();

		// Token: 0x04001C86 RID: 7302
		private XmlQualifiedName qualifiedName = XmlQualifiedName.Empty;

		// Token: 0x04001C87 RID: 7303
		private CompiledIdentityConstraint compiledConstraint;
	}
}
