﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="attributeGroup" /> element from the XML Schema as specified by the World Wide Web Consortium (W3C). AttributesGroups provides a mechanism to group a set of attribute declarations so that they can be incorporated as a group into complex type definitions.</summary>
	// Token: 0x0200040A RID: 1034
	public class XmlSchemaAttributeGroup : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the name of the attribute group.</summary>
		/// <returns>The name of the attribute group.</returns>
		// Token: 0x1700083B RID: 2107
		// (get) Token: 0x06002756 RID: 10070 RVA: 0x000DE6EA File Offset: 0x000DC8EA
		// (set) Token: 0x06002757 RID: 10071 RVA: 0x000DE6F2 File Offset: 0x000DC8F2
		[XmlAttribute("name")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets the collection of attributes for the attribute group. Contains <see langword="XmlSchemaAttribute" /> and <see langword="XmlSchemaAttributeGroupRef" /> elements.</summary>
		/// <returns>The collection of attributes for the attribute group.</returns>
		// Token: 0x1700083C RID: 2108
		// (get) Token: 0x06002758 RID: 10072 RVA: 0x000DE6FB File Offset: 0x000DC8FB
		[XmlElement("attributeGroup", typeof(XmlSchemaAttributeGroupRef))]
		[XmlElement("attribute", typeof(XmlSchemaAttribute))]
		public XmlSchemaObjectCollection Attributes
		{
			get
			{
				return this.attributes;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaAnyAttribute" /> component of the attribute group.</summary>
		/// <returns>The World Wide Web Consortium (W3C) <see langword="anyAttribute" /> element.</returns>
		// Token: 0x1700083D RID: 2109
		// (get) Token: 0x06002759 RID: 10073 RVA: 0x000DE703 File Offset: 0x000DC903
		// (set) Token: 0x0600275A RID: 10074 RVA: 0x000DE70B File Offset: 0x000DC90B
		[XmlElement("anyAttribute")]
		public XmlSchemaAnyAttribute AnyAttribute
		{
			get
			{
				return this.anyAttribute;
			}
			set
			{
				this.anyAttribute = value;
			}
		}

		/// <summary>Gets the qualified name of the attribute group.</summary>
		/// <returns>The qualified name of the attribute group.</returns>
		// Token: 0x1700083E RID: 2110
		// (get) Token: 0x0600275B RID: 10075 RVA: 0x000DE714 File Offset: 0x000DC914
		[XmlIgnore]
		public XmlQualifiedName QualifiedName
		{
			get
			{
				return this.qname;
			}
		}

		// Token: 0x1700083F RID: 2111
		// (get) Token: 0x0600275C RID: 10076 RVA: 0x000DE71C File Offset: 0x000DC91C
		[XmlIgnore]
		internal XmlSchemaObjectTable AttributeUses
		{
			get
			{
				if (this.attributeUses == null)
				{
					this.attributeUses = new XmlSchemaObjectTable();
				}
				return this.attributeUses;
			}
		}

		// Token: 0x17000840 RID: 2112
		// (get) Token: 0x0600275D RID: 10077 RVA: 0x000DE737 File Offset: 0x000DC937
		// (set) Token: 0x0600275E RID: 10078 RVA: 0x000DE73F File Offset: 0x000DC93F
		[XmlIgnore]
		internal XmlSchemaAnyAttribute AttributeWildcard
		{
			get
			{
				return this.attributeWildcard;
			}
			set
			{
				this.attributeWildcard = value;
			}
		}

		/// <summary>Gets the redefined attribute group property from the XML Schema.</summary>
		/// <returns>The redefined attribute group property.</returns>
		// Token: 0x17000841 RID: 2113
		// (get) Token: 0x0600275F RID: 10079 RVA: 0x000DE748 File Offset: 0x000DC948
		[XmlIgnore]
		public XmlSchemaAttributeGroup RedefinedAttributeGroup
		{
			get
			{
				return this.redefined;
			}
		}

		// Token: 0x17000842 RID: 2114
		// (get) Token: 0x06002760 RID: 10080 RVA: 0x000DE748 File Offset: 0x000DC948
		// (set) Token: 0x06002761 RID: 10081 RVA: 0x000DE750 File Offset: 0x000DC950
		[XmlIgnore]
		internal XmlSchemaAttributeGroup Redefined
		{
			get
			{
				return this.redefined;
			}
			set
			{
				this.redefined = value;
			}
		}

		// Token: 0x17000843 RID: 2115
		// (get) Token: 0x06002762 RID: 10082 RVA: 0x000DE759 File Offset: 0x000DC959
		// (set) Token: 0x06002763 RID: 10083 RVA: 0x000DE761 File Offset: 0x000DC961
		[XmlIgnore]
		internal int SelfReferenceCount
		{
			get
			{
				return this.selfReferenceCount;
			}
			set
			{
				this.selfReferenceCount = value;
			}
		}

		// Token: 0x17000844 RID: 2116
		// (get) Token: 0x06002764 RID: 10084 RVA: 0x000DE76A File Offset: 0x000DC96A
		// (set) Token: 0x06002765 RID: 10085 RVA: 0x000DE772 File Offset: 0x000DC972
		[XmlIgnore]
		internal override string NameAttribute
		{
			get
			{
				return this.Name;
			}
			set
			{
				this.Name = value;
			}
		}

		// Token: 0x06002766 RID: 10086 RVA: 0x000DE77B File Offset: 0x000DC97B
		internal void SetQualifiedName(XmlQualifiedName value)
		{
			this.qname = value;
		}

		// Token: 0x06002767 RID: 10087 RVA: 0x000DE784 File Offset: 0x000DC984
		internal override XmlSchemaObject Clone()
		{
			XmlSchemaAttributeGroup xmlSchemaAttributeGroup = (XmlSchemaAttributeGroup)base.MemberwiseClone();
			if (XmlSchemaComplexType.HasAttributeQNameRef(this.attributes))
			{
				xmlSchemaAttributeGroup.attributes = XmlSchemaComplexType.CloneAttributes(this.attributes);
				xmlSchemaAttributeGroup.attributeUses = null;
			}
			return xmlSchemaAttributeGroup;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAttributeGroup" /> class.</summary>
		// Token: 0x06002768 RID: 10088 RVA: 0x000DE7C3 File Offset: 0x000DC9C3
		public XmlSchemaAttributeGroup()
		{
		}

		// Token: 0x04001BF9 RID: 7161
		private string name;

		// Token: 0x04001BFA RID: 7162
		private XmlSchemaObjectCollection attributes = new XmlSchemaObjectCollection();

		// Token: 0x04001BFB RID: 7163
		private XmlSchemaAnyAttribute anyAttribute;

		// Token: 0x04001BFC RID: 7164
		private XmlQualifiedName qname = XmlQualifiedName.Empty;

		// Token: 0x04001BFD RID: 7165
		private XmlSchemaAttributeGroup redefined;

		// Token: 0x04001BFE RID: 7166
		private XmlSchemaObjectTable attributeUses;

		// Token: 0x04001BFF RID: 7167
		private XmlSchemaAnyAttribute attributeWildcard;

		// Token: 0x04001C00 RID: 7168
		private int selfReferenceCount;
	}
}
