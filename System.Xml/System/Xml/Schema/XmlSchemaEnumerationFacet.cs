﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="enumeration" /> facet from XML Schema as specified by the World Wide Web Consortium (W3C). This class specifies a list of valid values for a simpleType element. Declaration is contained within a <see langword="restriction" /> declaration.</summary>
	// Token: 0x02000426 RID: 1062
	public class XmlSchemaEnumerationFacet : XmlSchemaFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaEnumerationFacet" /> class.</summary>
		// Token: 0x0600287A RID: 10362 RVA: 0x000E061B File Offset: 0x000DE81B
		public XmlSchemaEnumerationFacet()
		{
			base.FacetType = FacetType.Enumeration;
		}
	}
}
