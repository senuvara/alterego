﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000384 RID: 900
	internal class Datatype_anySimpleType : DatatypeImplementation
	{
		// Token: 0x06002240 RID: 8768 RVA: 0x000BD89C File Offset: 0x000BBA9C
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlUntypedConverter.Untyped;
		}

		// Token: 0x170006C8 RID: 1736
		// (get) Token: 0x06002241 RID: 8769 RVA: 0x000BCDB2 File Offset: 0x000BAFB2
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.miscFacetsChecker;
			}
		}

		// Token: 0x170006C9 RID: 1737
		// (get) Token: 0x06002242 RID: 8770 RVA: 0x000BD8A3 File Offset: 0x000BBAA3
		public override Type ValueType
		{
			get
			{
				return Datatype_anySimpleType.atomicValueType;
			}
		}

		// Token: 0x170006CA RID: 1738
		// (get) Token: 0x06002243 RID: 8771 RVA: 0x00074D15 File Offset: 0x00072F15
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.AnyAtomicType;
			}
		}

		// Token: 0x170006CB RID: 1739
		// (get) Token: 0x06002244 RID: 8772 RVA: 0x000BD8AA File Offset: 0x000BBAAA
		internal override Type ListValueType
		{
			get
			{
				return Datatype_anySimpleType.listValueType;
			}
		}

		// Token: 0x170006CC RID: 1740
		// (get) Token: 0x06002245 RID: 8773 RVA: 0x000164C5 File Offset: 0x000146C5
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.None;
			}
		}

		// Token: 0x170006CD RID: 1741
		// (get) Token: 0x06002246 RID: 8774 RVA: 0x000020CD File Offset: 0x000002CD
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return (RestrictionFlags)0;
			}
		}

		// Token: 0x170006CE RID: 1742
		// (get) Token: 0x06002247 RID: 8775 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x06002248 RID: 8776 RVA: 0x000BD8B1 File Offset: 0x000BBAB1
		internal override int Compare(object value1, object value2)
		{
			return string.Compare(value1.ToString(), value2.ToString(), StringComparison.Ordinal);
		}

		// Token: 0x06002249 RID: 8777 RVA: 0x000BD8C5 File Offset: 0x000BBAC5
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = XmlComplianceUtil.NonCDataNormalize(s);
			return null;
		}

		// Token: 0x0600224A RID: 8778 RVA: 0x000BD8D1 File Offset: 0x000BBAD1
		public Datatype_anySimpleType()
		{
		}

		// Token: 0x0600224B RID: 8779 RVA: 0x000BD8D9 File Offset: 0x000BBAD9
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_anySimpleType()
		{
		}

		// Token: 0x040018A4 RID: 6308
		private static readonly Type atomicValueType = typeof(string);

		// Token: 0x040018A5 RID: 6309
		private static readonly Type listValueType = typeof(string[]);
	}
}
