﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200039F RID: 927
	internal class Datatype_QName : Datatype_anySimpleType
	{
		// Token: 0x060022DA RID: 8922 RVA: 0x000BDBF8 File Offset: 0x000BBDF8
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlMiscConverter.Create(schemaType);
		}

		// Token: 0x17000718 RID: 1816
		// (get) Token: 0x060022DB RID: 8923 RVA: 0x000BE16A File Offset: 0x000BC36A
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.qnameFacetsChecker;
			}
		}

		// Token: 0x17000719 RID: 1817
		// (get) Token: 0x060022DC RID: 8924 RVA: 0x000BE171 File Offset: 0x000BC371
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.QName;
			}
		}

		// Token: 0x1700071A RID: 1818
		// (get) Token: 0x060022DD RID: 8925 RVA: 0x00074D15 File Offset: 0x00072F15
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.QName;
			}
		}

		// Token: 0x1700071B RID: 1819
		// (get) Token: 0x060022DE RID: 8926 RVA: 0x000BD2C1 File Offset: 0x000BB4C1
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Length | RestrictionFlags.MinLength | RestrictionFlags.MaxLength | RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace;
			}
		}

		// Token: 0x1700071C RID: 1820
		// (get) Token: 0x060022DF RID: 8927 RVA: 0x000BE175 File Offset: 0x000BC375
		public override Type ValueType
		{
			get
			{
				return Datatype_QName.atomicValueType;
			}
		}

		// Token: 0x1700071D RID: 1821
		// (get) Token: 0x060022E0 RID: 8928 RVA: 0x000BE17C File Offset: 0x000BC37C
		internal override Type ListValueType
		{
			get
			{
				return Datatype_QName.listValueType;
			}
		}

		// Token: 0x1700071E RID: 1822
		// (get) Token: 0x060022E1 RID: 8929 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x060022E2 RID: 8930 RVA: 0x000BE184 File Offset: 0x000BC384
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			if (s == null || s.Length == 0)
			{
				return new XmlSchemaException("The attribute value cannot be empty.", string.Empty);
			}
			Exception ex = DatatypeImplementation.qnameFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				XmlQualifiedName xmlQualifiedName = null;
				try
				{
					string text;
					xmlQualifiedName = XmlQualifiedName.Parse(s, nsmgr, out text);
				}
				catch (ArgumentException ex)
				{
					return ex;
				}
				catch (XmlException ex)
				{
					return ex;
				}
				ex = DatatypeImplementation.qnameFacetsChecker.CheckValueFacets(xmlQualifiedName, this);
				if (ex == null)
				{
					typedValue = xmlQualifiedName;
					return null;
				}
			}
			return ex;
		}

		// Token: 0x060022E3 RID: 8931 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_QName()
		{
		}

		// Token: 0x060022E4 RID: 8932 RVA: 0x000BE208 File Offset: 0x000BC408
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_QName()
		{
		}

		// Token: 0x040018BA RID: 6330
		private static readonly Type atomicValueType = typeof(XmlQualifiedName);

		// Token: 0x040018BB RID: 6331
		private static readonly Type listValueType = typeof(XmlQualifiedName[]);
	}
}
