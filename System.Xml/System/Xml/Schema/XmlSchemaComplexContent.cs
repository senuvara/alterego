﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="complexContent" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class represents the complex content model for complex types. It contains extensions or restrictions on a complex type that has either only elements or mixed content.</summary>
	// Token: 0x02000411 RID: 1041
	public class XmlSchemaComplexContent : XmlSchemaContentModel
	{
		/// <summary>Gets or sets information that determines if the type has a mixed content model.</summary>
		/// <returns>If this property is <see langword="true" />, character data is allowed to appear between the child elements of the complex type (mixed content model). The default is <see langword="false" />.Optional.</returns>
		// Token: 0x17000857 RID: 2135
		// (get) Token: 0x060027A1 RID: 10145 RVA: 0x000DEE93 File Offset: 0x000DD093
		// (set) Token: 0x060027A2 RID: 10146 RVA: 0x000DEE9B File Offset: 0x000DD09B
		[XmlAttribute("mixed")]
		public bool IsMixed
		{
			get
			{
				return this.isMixed;
			}
			set
			{
				this.isMixed = value;
				this.hasMixedAttribute = true;
			}
		}

		/// <summary>Gets or sets the content.</summary>
		/// <returns>One of either the <see cref="T:System.Xml.Schema.XmlSchemaComplexContentRestriction" /> or <see cref="T:System.Xml.Schema.XmlSchemaComplexContentExtension" /> classes.</returns>
		// Token: 0x17000858 RID: 2136
		// (get) Token: 0x060027A3 RID: 10147 RVA: 0x000DEEAB File Offset: 0x000DD0AB
		// (set) Token: 0x060027A4 RID: 10148 RVA: 0x000DEEB3 File Offset: 0x000DD0B3
		[XmlElement("extension", typeof(XmlSchemaComplexContentExtension))]
		[XmlElement("restriction", typeof(XmlSchemaComplexContentRestriction))]
		public override XmlSchemaContent Content
		{
			get
			{
				return this.content;
			}
			set
			{
				this.content = value;
			}
		}

		// Token: 0x17000859 RID: 2137
		// (get) Token: 0x060027A5 RID: 10149 RVA: 0x000DEEBC File Offset: 0x000DD0BC
		[XmlIgnore]
		internal bool HasMixedAttribute
		{
			get
			{
				return this.hasMixedAttribute;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaComplexContent" /> class.</summary>
		// Token: 0x060027A6 RID: 10150 RVA: 0x000DEEC4 File Offset: 0x000DD0C4
		public XmlSchemaComplexContent()
		{
		}

		// Token: 0x04001C10 RID: 7184
		private XmlSchemaContent content;

		// Token: 0x04001C11 RID: 7185
		private bool isMixed;

		// Token: 0x04001C12 RID: 7186
		private bool hasMixedAttribute;
	}
}
