﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A8 RID: 936
	internal class Datatype_ID : Datatype_NCName
	{
		// Token: 0x1700072C RID: 1836
		// (get) Token: 0x060022FB RID: 8955 RVA: 0x000BE2A5 File Offset: 0x000BC4A5
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Id;
			}
		}

		// Token: 0x1700072D RID: 1837
		// (get) Token: 0x060022FC RID: 8956 RVA: 0x000033DE File Offset: 0x000015DE
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.ID;
			}
		}

		// Token: 0x060022FD RID: 8957 RVA: 0x000BE2A9 File Offset: 0x000BC4A9
		public Datatype_ID()
		{
		}
	}
}
