﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>An abstract class for <see cref="T:System.Xml.Schema.XmlSchemaAll" />, <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" />.</summary>
	// Token: 0x02000430 RID: 1072
	public abstract class XmlSchemaGroupBase : XmlSchemaParticle
	{
		/// <summary>This collection is used to add new elements to the compositor.</summary>
		/// <returns>An <see langword="XmlSchemaObjectCollection" />.</returns>
		// Token: 0x170008B6 RID: 2230
		// (get) Token: 0x06002893 RID: 10387
		[XmlIgnore]
		public abstract XmlSchemaObjectCollection Items { get; }

		// Token: 0x06002894 RID: 10388
		internal abstract void SetItems(XmlSchemaObjectCollection newItems);

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaGroupBase" /> class.</summary>
		// Token: 0x06002895 RID: 10389 RVA: 0x000DE383 File Offset: 0x000DC583
		protected XmlSchemaGroupBase()
		{
		}
	}
}
