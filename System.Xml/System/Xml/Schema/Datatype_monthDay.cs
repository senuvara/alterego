﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000399 RID: 921
	internal class Datatype_monthDay : Datatype_dateTimeBase
	{
		// Token: 0x17000702 RID: 1794
		// (get) Token: 0x060022B2 RID: 8882 RVA: 0x00002BA6 File Offset: 0x00000DA6
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.GMonthDay;
			}
		}

		// Token: 0x060022B3 RID: 8883 RVA: 0x000BDF55 File Offset: 0x000BC155
		internal Datatype_monthDay() : base(XsdDateTimeFlags.GMonthDay)
		{
		}
	}
}
