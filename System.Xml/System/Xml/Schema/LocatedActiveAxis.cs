﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200035F RID: 863
	internal class LocatedActiveAxis : ActiveAxis
	{
		// Token: 0x1700067A RID: 1658
		// (get) Token: 0x06002134 RID: 8500 RVA: 0x000B8F6E File Offset: 0x000B716E
		internal int Column
		{
			get
			{
				return this.column;
			}
		}

		// Token: 0x06002135 RID: 8501 RVA: 0x000B8F76 File Offset: 0x000B7176
		internal LocatedActiveAxis(Asttree astfield, KeySequence ks, int column) : base(astfield)
		{
			this.Ks = ks;
			this.column = column;
			this.isMatched = false;
		}

		// Token: 0x06002136 RID: 8502 RVA: 0x000B8F94 File Offset: 0x000B7194
		internal void Reactivate(KeySequence ks)
		{
			base.Reactivate();
			this.Ks = ks;
		}

		// Token: 0x040017CB RID: 6091
		private int column;

		// Token: 0x040017CC RID: 6092
		internal bool isMatched;

		// Token: 0x040017CD RID: 6093
		internal KeySequence Ks;
	}
}
