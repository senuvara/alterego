﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000355 RID: 853
	internal class ForwardAxis
	{
		// Token: 0x1700065C RID: 1628
		// (get) Token: 0x060020CC RID: 8396 RVA: 0x000B789E File Offset: 0x000B5A9E
		internal DoubleLinkAxis RootNode
		{
			get
			{
				return this.rootNode;
			}
		}

		// Token: 0x1700065D RID: 1629
		// (get) Token: 0x060020CD RID: 8397 RVA: 0x000B78A6 File Offset: 0x000B5AA6
		internal DoubleLinkAxis TopNode
		{
			get
			{
				return this.topNode;
			}
		}

		// Token: 0x1700065E RID: 1630
		// (get) Token: 0x060020CE RID: 8398 RVA: 0x000B78AE File Offset: 0x000B5AAE
		internal bool IsAttribute
		{
			get
			{
				return this.isAttribute;
			}
		}

		// Token: 0x1700065F RID: 1631
		// (get) Token: 0x060020CF RID: 8399 RVA: 0x000B78B6 File Offset: 0x000B5AB6
		internal bool IsDss
		{
			get
			{
				return this.isDss;
			}
		}

		// Token: 0x17000660 RID: 1632
		// (get) Token: 0x060020D0 RID: 8400 RVA: 0x000B78BE File Offset: 0x000B5ABE
		internal bool IsSelfAxis
		{
			get
			{
				return this.isSelfAxis;
			}
		}

		// Token: 0x060020D1 RID: 8401 RVA: 0x000B78C8 File Offset: 0x000B5AC8
		public ForwardAxis(DoubleLinkAxis axis, bool isdesorself)
		{
			this.isDss = isdesorself;
			this.isAttribute = Asttree.IsAttribute(axis);
			this.topNode = axis;
			this.rootNode = axis;
			while (this.rootNode.Input != null)
			{
				this.rootNode = (DoubleLinkAxis)this.rootNode.Input;
			}
			this.isSelfAxis = Asttree.IsSelf(this.topNode);
		}

		// Token: 0x04001794 RID: 6036
		private DoubleLinkAxis topNode;

		// Token: 0x04001795 RID: 6037
		private DoubleLinkAxis rootNode;

		// Token: 0x04001796 RID: 6038
		private bool isAttribute;

		// Token: 0x04001797 RID: 6039
		private bool isDss;

		// Token: 0x04001798 RID: 6040
		private bool isSelfAxis;
	}
}
