﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="extension" /> element for simple content from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to derive simple types by extension. Such derivations are used to extend the simple type content of the element by adding attributes.</summary>
	// Token: 0x0200044C RID: 1100
	public class XmlSchemaSimpleContentExtension : XmlSchemaContent
	{
		/// <summary>Gets or sets the name of a built-in data type or simple type from which this type is extended.</summary>
		/// <returns>The base type name.</returns>
		// Token: 0x1700090D RID: 2317
		// (get) Token: 0x06002987 RID: 10631 RVA: 0x000E34F2 File Offset: 0x000E16F2
		// (set) Token: 0x06002988 RID: 10632 RVA: 0x000E34FA File Offset: 0x000E16FA
		[XmlAttribute("base")]
		public XmlQualifiedName BaseTypeName
		{
			get
			{
				return this.baseTypeName;
			}
			set
			{
				this.baseTypeName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets the collection of <see cref="T:System.Xml.Schema.XmlSchemaAttribute" /> and <see cref="T:System.Xml.Schema.XmlSchemaAttributeGroupRef" />.</summary>
		/// <returns>The collection of attributes for the <see langword="simpleType" /> element.</returns>
		// Token: 0x1700090E RID: 2318
		// (get) Token: 0x06002989 RID: 10633 RVA: 0x000E3513 File Offset: 0x000E1713
		[XmlElement("attributeGroup", typeof(XmlSchemaAttributeGroupRef))]
		[XmlElement("attribute", typeof(XmlSchemaAttribute))]
		public XmlSchemaObjectCollection Attributes
		{
			get
			{
				return this.attributes;
			}
		}

		/// <summary>Gets or sets the <see langword="XmlSchemaAnyAttribute" /> to be used for the attribute value.</summary>
		/// <returns>The <see langword="XmlSchemaAnyAttribute" />.Optional.</returns>
		// Token: 0x1700090F RID: 2319
		// (get) Token: 0x0600298A RID: 10634 RVA: 0x000E351B File Offset: 0x000E171B
		// (set) Token: 0x0600298B RID: 10635 RVA: 0x000E3523 File Offset: 0x000E1723
		[XmlElement("anyAttribute")]
		public XmlSchemaAnyAttribute AnyAttribute
		{
			get
			{
				return this.anyAttribute;
			}
			set
			{
				this.anyAttribute = value;
			}
		}

		// Token: 0x0600298C RID: 10636 RVA: 0x000E352C File Offset: 0x000E172C
		internal void SetAttributes(XmlSchemaObjectCollection newAttributes)
		{
			this.attributes = newAttributes;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaSimpleContentExtension" /> class.</summary>
		// Token: 0x0600298D RID: 10637 RVA: 0x000E3535 File Offset: 0x000E1735
		public XmlSchemaSimpleContentExtension()
		{
		}

		// Token: 0x04001CD5 RID: 7381
		private XmlSchemaObjectCollection attributes = new XmlSchemaObjectCollection();

		// Token: 0x04001CD6 RID: 7382
		private XmlSchemaAnyAttribute anyAttribute;

		// Token: 0x04001CD7 RID: 7383
		private XmlQualifiedName baseTypeName = XmlQualifiedName.Empty;
	}
}
