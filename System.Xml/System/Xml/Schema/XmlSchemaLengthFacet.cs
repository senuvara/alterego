﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="length" /> facet from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to specify a restriction on the length of a <see langword="simpleType" /> element on the data type.</summary>
	// Token: 0x02000422 RID: 1058
	public class XmlSchemaLengthFacet : XmlSchemaNumericFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaLengthFacet" /> class.</summary>
		// Token: 0x06002876 RID: 10358 RVA: 0x000E05DF File Offset: 0x000DE7DF
		public XmlSchemaLengthFacet()
		{
			base.FacetType = FacetType.Length;
		}
	}
}
