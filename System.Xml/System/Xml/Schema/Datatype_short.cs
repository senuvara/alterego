﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B1 RID: 945
	internal class Datatype_short : Datatype_int
	{
		// Token: 0x17000748 RID: 1864
		// (get) Token: 0x0600232D RID: 9005 RVA: 0x000BE648 File Offset: 0x000BC848
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_short.numeric10FacetsChecker;
			}
		}

		// Token: 0x17000749 RID: 1865
		// (get) Token: 0x0600232E RID: 9006 RVA: 0x000BE64F File Offset: 0x000BC84F
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Short;
			}
		}

		// Token: 0x0600232F RID: 9007 RVA: 0x000BE654 File Offset: 0x000BC854
		internal override int Compare(object value1, object value2)
		{
			return ((short)value1).CompareTo(value2);
		}

		// Token: 0x1700074A RID: 1866
		// (get) Token: 0x06002330 RID: 9008 RVA: 0x000BE670 File Offset: 0x000BC870
		public override Type ValueType
		{
			get
			{
				return Datatype_short.atomicValueType;
			}
		}

		// Token: 0x1700074B RID: 1867
		// (get) Token: 0x06002331 RID: 9009 RVA: 0x000BE677 File Offset: 0x000BC877
		internal override Type ListValueType
		{
			get
			{
				return Datatype_short.listValueType;
			}
		}

		// Token: 0x06002332 RID: 9010 RVA: 0x000BE680 File Offset: 0x000BC880
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_short.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				short num;
				ex = XmlConvert.TryToInt16(s, out num);
				if (ex == null)
				{
					ex = Datatype_short.numeric10FacetsChecker.CheckValueFacets(num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002333 RID: 9011 RVA: 0x000BE6CA File Offset: 0x000BC8CA
		public Datatype_short()
		{
		}

		// Token: 0x06002334 RID: 9012 RVA: 0x000BE6D2 File Offset: 0x000BC8D2
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_short()
		{
		}

		// Token: 0x040018C6 RID: 6342
		private static readonly Type atomicValueType = typeof(short);

		// Token: 0x040018C7 RID: 6343
		private static readonly Type listValueType = typeof(short[]);

		// Token: 0x040018C8 RID: 6344
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(-32768m, 32767m);
	}
}
