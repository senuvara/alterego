﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="simpleContent" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class is for simple and complex types with simple content model.</summary>
	// Token: 0x0200044B RID: 1099
	public class XmlSchemaSimpleContent : XmlSchemaContentModel
	{
		/// <summary>Gets one of the <see cref="T:System.Xml.Schema.XmlSchemaSimpleContentRestriction" /> or <see cref="T:System.Xml.Schema.XmlSchemaSimpleContentExtension" />.</summary>
		/// <returns>The content contained within the <see langword="XmlSchemaSimpleContentRestriction" /> or <see langword="XmlSchemaSimpleContentExtension" /> class.</returns>
		// Token: 0x1700090C RID: 2316
		// (get) Token: 0x06002984 RID: 10628 RVA: 0x000E34E1 File Offset: 0x000E16E1
		// (set) Token: 0x06002985 RID: 10629 RVA: 0x000E34E9 File Offset: 0x000E16E9
		[XmlElement("restriction", typeof(XmlSchemaSimpleContentRestriction))]
		[XmlElement("extension", typeof(XmlSchemaSimpleContentExtension))]
		public override XmlSchemaContent Content
		{
			get
			{
				return this.content;
			}
			set
			{
				this.content = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaSimpleContent" /> class.</summary>
		// Token: 0x06002986 RID: 10630 RVA: 0x000DEEC4 File Offset: 0x000DD0C4
		public XmlSchemaSimpleContent()
		{
		}

		// Token: 0x04001CD4 RID: 7380
		private XmlSchemaContent content;
	}
}
