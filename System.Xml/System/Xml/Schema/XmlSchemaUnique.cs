﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="unique" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to identify a unique constraint among a set of elements.</summary>
	// Token: 0x02000434 RID: 1076
	public class XmlSchemaUnique : XmlSchemaIdentityConstraint
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaUnique" /> class.</summary>
		// Token: 0x060028AC RID: 10412 RVA: 0x000E0857 File Offset: 0x000DEA57
		public XmlSchemaUnique()
		{
		}
	}
}
