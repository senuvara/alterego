﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="attribute" /> element from the XML Schema as specified by the World Wide Web Consortium (W3C). Attributes provide additional information for other document elements. The attribute tag is nested between the tags of a document's element for the schema. The XML document displays attributes as named items in the opening tag of an element.</summary>
	// Token: 0x02000409 RID: 1033
	public class XmlSchemaAttribute : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the default value for the attribute.</summary>
		/// <returns>The default value for the attribute. The default is a null reference.Optional.</returns>
		// Token: 0x1700082C RID: 2092
		// (get) Token: 0x06002738 RID: 10040 RVA: 0x000DE4FF File Offset: 0x000DC6FF
		// (set) Token: 0x06002739 RID: 10041 RVA: 0x000DE507 File Offset: 0x000DC707
		[XmlAttribute("default")]
		[DefaultValue(null)]
		public string DefaultValue
		{
			get
			{
				return this.defaultValue;
			}
			set
			{
				this.defaultValue = value;
			}
		}

		/// <summary>Gets or sets the fixed value for the attribute.</summary>
		/// <returns>The fixed value for the attribute. The default is null.Optional.</returns>
		// Token: 0x1700082D RID: 2093
		// (get) Token: 0x0600273A RID: 10042 RVA: 0x000DE510 File Offset: 0x000DC710
		// (set) Token: 0x0600273B RID: 10043 RVA: 0x000DE518 File Offset: 0x000DC718
		[XmlAttribute("fixed")]
		[DefaultValue(null)]
		public string FixedValue
		{
			get
			{
				return this.fixedValue;
			}
			set
			{
				this.fixedValue = value;
			}
		}

		/// <summary>Gets or sets the form for the attribute.</summary>
		/// <returns>One of the <see cref="T:System.Xml.Schema.XmlSchemaForm" /> values. The default is the value of the <see cref="P:System.Xml.Schema.XmlSchema.AttributeFormDefault" /> of the schema element containing the attribute.Optional.</returns>
		// Token: 0x1700082E RID: 2094
		// (get) Token: 0x0600273C RID: 10044 RVA: 0x000DE521 File Offset: 0x000DC721
		// (set) Token: 0x0600273D RID: 10045 RVA: 0x000DE529 File Offset: 0x000DC729
		[DefaultValue(XmlSchemaForm.None)]
		[XmlAttribute("form")]
		public XmlSchemaForm Form
		{
			get
			{
				return this.form;
			}
			set
			{
				this.form = value;
			}
		}

		/// <summary>Gets or sets the name of the attribute.</summary>
		/// <returns>The name of the attribute.</returns>
		// Token: 0x1700082F RID: 2095
		// (get) Token: 0x0600273E RID: 10046 RVA: 0x000DE532 File Offset: 0x000DC732
		// (set) Token: 0x0600273F RID: 10047 RVA: 0x000DE53A File Offset: 0x000DC73A
		[XmlAttribute("name")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets the name of an attribute declared in this schema (or another schema indicated by the specified namespace).</summary>
		/// <returns>The name of the attribute declared.</returns>
		// Token: 0x17000830 RID: 2096
		// (get) Token: 0x06002740 RID: 10048 RVA: 0x000DE543 File Offset: 0x000DC743
		// (set) Token: 0x06002741 RID: 10049 RVA: 0x000DE54B File Offset: 0x000DC74B
		[XmlAttribute("ref")]
		public XmlQualifiedName RefName
		{
			get
			{
				return this.refName;
			}
			set
			{
				this.refName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets or sets the name of the simple type defined in this schema (or another schema indicated by the specified namespace).</summary>
		/// <returns>The name of the simple type.</returns>
		// Token: 0x17000831 RID: 2097
		// (get) Token: 0x06002742 RID: 10050 RVA: 0x000DE564 File Offset: 0x000DC764
		// (set) Token: 0x06002743 RID: 10051 RVA: 0x000DE56C File Offset: 0x000DC76C
		[XmlAttribute("type")]
		public XmlQualifiedName SchemaTypeName
		{
			get
			{
				return this.typeName;
			}
			set
			{
				this.typeName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets or sets the attribute type to a simple type.</summary>
		/// <returns>The simple type defined in this schema.</returns>
		// Token: 0x17000832 RID: 2098
		// (get) Token: 0x06002744 RID: 10052 RVA: 0x000DE585 File Offset: 0x000DC785
		// (set) Token: 0x06002745 RID: 10053 RVA: 0x000DE58D File Offset: 0x000DC78D
		[XmlElement("simpleType")]
		public XmlSchemaSimpleType SchemaType
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		/// <summary>Gets or sets information about how the attribute is used.</summary>
		/// <returns>One of the following values: None, Prohibited, Optional, or Required. The default is Optional.Optional.</returns>
		// Token: 0x17000833 RID: 2099
		// (get) Token: 0x06002746 RID: 10054 RVA: 0x000DE596 File Offset: 0x000DC796
		// (set) Token: 0x06002747 RID: 10055 RVA: 0x000DE59E File Offset: 0x000DC79E
		[DefaultValue(XmlSchemaUse.None)]
		[XmlAttribute("use")]
		public XmlSchemaUse Use
		{
			get
			{
				return this.use;
			}
			set
			{
				this.use = value;
			}
		}

		/// <summary>Gets the qualified name for the attribute.</summary>
		/// <returns>The post-compilation value of the <see langword="QualifiedName" /> property.</returns>
		// Token: 0x17000834 RID: 2100
		// (get) Token: 0x06002748 RID: 10056 RVA: 0x000DE5A7 File Offset: 0x000DC7A7
		[XmlIgnore]
		public XmlQualifiedName QualifiedName
		{
			get
			{
				return this.qualifiedName;
			}
		}

		/// <summary>Gets the common language runtime (CLR) object based on the <see cref="P:System.Xml.Schema.XmlSchemaAttribute.SchemaType" /> or <see cref="P:System.Xml.Schema.XmlSchemaAttribute.SchemaTypeName" /> of the attribute that holds the post-compilation value of the <see langword="AttributeType" /> property.</summary>
		/// <returns>The common runtime library (CLR) object that holds the post-compilation value of the <see langword="AttributeType" /> property.</returns>
		// Token: 0x17000835 RID: 2101
		// (get) Token: 0x06002749 RID: 10057 RVA: 0x000DE5AF File Offset: 0x000DC7AF
		[XmlIgnore]
		[Obsolete("This property has been deprecated. Please use AttributeSchemaType property that returns a strongly typed attribute type. http://go.microsoft.com/fwlink/?linkid=14202")]
		public object AttributeType
		{
			get
			{
				if (this.attributeType == null)
				{
					return null;
				}
				if (this.attributeType.QualifiedName.Namespace == "http://www.w3.org/2001/XMLSchema")
				{
					return this.attributeType.Datatype;
				}
				return this.attributeType;
			}
		}

		/// <summary>Gets an <see cref="T:System.Xml.Schema.XmlSchemaSimpleType" /> object representing the type of the attribute based on the <see cref="P:System.Xml.Schema.XmlSchemaAttribute.SchemaType" /> or <see cref="P:System.Xml.Schema.XmlSchemaAttribute.SchemaTypeName" /> of the attribute.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaSimpleType" /> object.</returns>
		// Token: 0x17000836 RID: 2102
		// (get) Token: 0x0600274A RID: 10058 RVA: 0x000DE5E9 File Offset: 0x000DC7E9
		[XmlIgnore]
		public XmlSchemaSimpleType AttributeSchemaType
		{
			get
			{
				return this.attributeType;
			}
		}

		// Token: 0x0600274B RID: 10059 RVA: 0x000DE5F4 File Offset: 0x000DC7F4
		internal XmlReader Validate(XmlReader reader, XmlResolver resolver, XmlSchemaSet schemaSet, ValidationEventHandler valEventHandler)
		{
			if (schemaSet != null)
			{
				XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
				xmlReaderSettings.ValidationType = ValidationType.Schema;
				xmlReaderSettings.Schemas = schemaSet;
				xmlReaderSettings.ValidationEventHandler += valEventHandler;
				return new XsdValidatingReader(reader, resolver, xmlReaderSettings, this);
			}
			return null;
		}

		// Token: 0x17000837 RID: 2103
		// (get) Token: 0x0600274C RID: 10060 RVA: 0x000DE62B File Offset: 0x000DC82B
		[XmlIgnore]
		internal XmlSchemaDatatype Datatype
		{
			get
			{
				if (this.attributeType != null)
				{
					return this.attributeType.Datatype;
				}
				return null;
			}
		}

		// Token: 0x0600274D RID: 10061 RVA: 0x000DE642 File Offset: 0x000DC842
		internal void SetQualifiedName(XmlQualifiedName value)
		{
			this.qualifiedName = value;
		}

		// Token: 0x0600274E RID: 10062 RVA: 0x000DE64B File Offset: 0x000DC84B
		internal void SetAttributeType(XmlSchemaSimpleType value)
		{
			this.attributeType = value;
		}

		// Token: 0x17000838 RID: 2104
		// (get) Token: 0x0600274F RID: 10063 RVA: 0x000DE654 File Offset: 0x000DC854
		// (set) Token: 0x06002750 RID: 10064 RVA: 0x000DE65C File Offset: 0x000DC85C
		internal SchemaAttDef AttDef
		{
			get
			{
				return this.attDef;
			}
			set
			{
				this.attDef = value;
			}
		}

		// Token: 0x17000839 RID: 2105
		// (get) Token: 0x06002751 RID: 10065 RVA: 0x000DE665 File Offset: 0x000DC865
		internal bool HasDefault
		{
			get
			{
				return this.defaultValue != null;
			}
		}

		// Token: 0x1700083A RID: 2106
		// (get) Token: 0x06002752 RID: 10066 RVA: 0x000DE670 File Offset: 0x000DC870
		// (set) Token: 0x06002753 RID: 10067 RVA: 0x000DE678 File Offset: 0x000DC878
		[XmlIgnore]
		internal override string NameAttribute
		{
			get
			{
				return this.Name;
			}
			set
			{
				this.Name = value;
			}
		}

		// Token: 0x06002754 RID: 10068 RVA: 0x000DE681 File Offset: 0x000DC881
		internal override XmlSchemaObject Clone()
		{
			XmlSchemaAttribute xmlSchemaAttribute = (XmlSchemaAttribute)base.MemberwiseClone();
			xmlSchemaAttribute.refName = this.refName.Clone();
			xmlSchemaAttribute.typeName = this.typeName.Clone();
			xmlSchemaAttribute.qualifiedName = this.qualifiedName.Clone();
			return xmlSchemaAttribute;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAttribute" /> class.</summary>
		// Token: 0x06002755 RID: 10069 RVA: 0x000DE6C1 File Offset: 0x000DC8C1
		public XmlSchemaAttribute()
		{
		}

		// Token: 0x04001BEE RID: 7150
		private string defaultValue;

		// Token: 0x04001BEF RID: 7151
		private string fixedValue;

		// Token: 0x04001BF0 RID: 7152
		private string name;

		// Token: 0x04001BF1 RID: 7153
		private XmlSchemaForm form;

		// Token: 0x04001BF2 RID: 7154
		private XmlSchemaUse use;

		// Token: 0x04001BF3 RID: 7155
		private XmlQualifiedName refName = XmlQualifiedName.Empty;

		// Token: 0x04001BF4 RID: 7156
		private XmlQualifiedName typeName = XmlQualifiedName.Empty;

		// Token: 0x04001BF5 RID: 7157
		private XmlQualifiedName qualifiedName = XmlQualifiedName.Empty;

		// Token: 0x04001BF6 RID: 7158
		private XmlSchemaSimpleType type;

		// Token: 0x04001BF7 RID: 7159
		private XmlSchemaSimpleType attributeType;

		// Token: 0x04001BF8 RID: 7160
		private SchemaAttDef attDef;
	}
}
