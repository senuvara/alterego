﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="import" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class is used to import schema components from other schemas.</summary>
	// Token: 0x02000437 RID: 1079
	public class XmlSchemaImport : XmlSchemaExternal
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaImport" /> class.</summary>
		// Token: 0x060028B1 RID: 10417 RVA: 0x000E0893 File Offset: 0x000DEA93
		public XmlSchemaImport()
		{
			base.Compositor = Compositor.Import;
		}

		/// <summary>Gets or sets the target namespace for the imported schema as a Uniform Resource Identifier (URI) reference.</summary>
		/// <returns>The target namespace for the imported schema as a URI reference.Optional.</returns>
		// Token: 0x170008C2 RID: 2242
		// (get) Token: 0x060028B2 RID: 10418 RVA: 0x000E08A2 File Offset: 0x000DEAA2
		// (set) Token: 0x060028B3 RID: 10419 RVA: 0x000E08AA File Offset: 0x000DEAAA
		[XmlAttribute("namespace", DataType = "anyURI")]
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		/// <summary>Gets or sets the <see langword="annotation" /> property.</summary>
		/// <returns>The annotation.</returns>
		// Token: 0x170008C3 RID: 2243
		// (get) Token: 0x060028B4 RID: 10420 RVA: 0x000E08B3 File Offset: 0x000DEAB3
		// (set) Token: 0x060028B5 RID: 10421 RVA: 0x000E08BB File Offset: 0x000DEABB
		[XmlElement("annotation", typeof(XmlSchemaAnnotation))]
		public XmlSchemaAnnotation Annotation
		{
			get
			{
				return this.annotation;
			}
			set
			{
				this.annotation = value;
			}
		}

		// Token: 0x060028B6 RID: 10422 RVA: 0x000E08BB File Offset: 0x000DEABB
		internal override void AddAnnotation(XmlSchemaAnnotation annotation)
		{
			this.annotation = annotation;
		}

		// Token: 0x04001C8A RID: 7306
		private string ns;

		// Token: 0x04001C8B RID: 7307
		private XmlSchemaAnnotation annotation;
	}
}
