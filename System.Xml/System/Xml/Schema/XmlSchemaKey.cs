﻿using System;

namespace System.Xml.Schema
{
	/// <summary>This class represents the <see langword="key" /> element from XMLSchema as specified by the World Wide Web Consortium (W3C).</summary>
	// Token: 0x02000435 RID: 1077
	public class XmlSchemaKey : XmlSchemaIdentityConstraint
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaKey" /> class.</summary>
		// Token: 0x060028AD RID: 10413 RVA: 0x000E0857 File Offset: 0x000DEA57
		public XmlSchemaKey()
		{
		}
	}
}
