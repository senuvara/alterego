﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Provides different methods for preventing derivation.</summary>
	// Token: 0x0200041A RID: 1050
	[Flags]
	public enum XmlSchemaDerivationMethod
	{
		/// <summary>Override default derivation method to allow any derivation.</summary>
		// Token: 0x04001C37 RID: 7223
		[XmlEnum("")]
		Empty = 0,
		/// <summary>Refers to derivations by <see langword="Substitution" />.</summary>
		// Token: 0x04001C38 RID: 7224
		[XmlEnum("substitution")]
		Substitution = 1,
		/// <summary>Refers to derivations by <see langword="Extension" />.</summary>
		// Token: 0x04001C39 RID: 7225
		[XmlEnum("extension")]
		Extension = 2,
		/// <summary>Refers to derivations by <see langword="Restriction" />.</summary>
		// Token: 0x04001C3A RID: 7226
		[XmlEnum("restriction")]
		Restriction = 4,
		/// <summary>Refers to derivations by <see langword="List" />.</summary>
		// Token: 0x04001C3B RID: 7227
		[XmlEnum("list")]
		List = 8,
		/// <summary>Refers to derivations by <see langword="Union" />.</summary>
		// Token: 0x04001C3C RID: 7228
		[XmlEnum("union")]
		Union = 16,
		/// <summary>
		///     <see langword="#all" />. Refers to all derivation methods.</summary>
		// Token: 0x04001C3D RID: 7229
		[XmlEnum("#all")]
		All = 255,
		/// <summary>Accepts the default derivation method.</summary>
		// Token: 0x04001C3E RID: 7230
		[XmlIgnore]
		None = 256
	}
}
