﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200037E RID: 894
	internal enum XmlSchemaWhiteSpace
	{
		// Token: 0x0400182F RID: 6191
		Preserve,
		// Token: 0x04001830 RID: 6192
		Replace,
		// Token: 0x04001831 RID: 6193
		Collapse
	}
}
