﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="maxExclusive" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to specify a restriction on the maximum value of a <see langword="simpleType" /> element. The element value must be less than the value of the <see langword="maxExclusive" /> element.</summary>
	// Token: 0x02000429 RID: 1065
	public class XmlSchemaMaxExclusiveFacet : XmlSchemaFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaMaxExclusiveFacet" /> class.</summary>
		// Token: 0x0600287D RID: 10365 RVA: 0x000E0648 File Offset: 0x000DE848
		public XmlSchemaMaxExclusiveFacet()
		{
			base.FacetType = FacetType.MaxExclusive;
		}
	}
}
