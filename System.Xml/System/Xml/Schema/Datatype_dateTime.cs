﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000392 RID: 914
	internal class Datatype_dateTime : Datatype_dateTimeBase
	{
		// Token: 0x060022A7 RID: 8871 RVA: 0x000BDF0A File Offset: 0x000BC10A
		internal Datatype_dateTime() : base(XsdDateTimeFlags.DateTime)
		{
		}
	}
}
