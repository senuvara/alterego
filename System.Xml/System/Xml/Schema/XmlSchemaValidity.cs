﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the validity of an XML item validated by the <see cref="T:System.Xml.Schema.XmlSchemaValidator" /> class.</summary>
	// Token: 0x0200045D RID: 1117
	public enum XmlSchemaValidity
	{
		/// <summary>The validity of the XML item is not known.</summary>
		// Token: 0x04001D47 RID: 7495
		NotKnown,
		/// <summary>The XML item is valid.</summary>
		// Token: 0x04001D48 RID: 7496
		Valid,
		/// <summary>The XML item is invalid.</summary>
		// Token: 0x04001D49 RID: 7497
		Invalid
	}
}
