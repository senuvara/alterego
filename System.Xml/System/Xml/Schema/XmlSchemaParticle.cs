﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Abstract class for that is the base class for all particle types (e.g. <see cref="T:System.Xml.Schema.XmlSchemaAny" />).</summary>
	// Token: 0x02000445 RID: 1093
	public abstract class XmlSchemaParticle : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the number as a string value. The minimum number of times the particle can occur.</summary>
		/// <returns>The number as a string value. <see langword="String.Empty" /> indicates that <see langword="MinOccurs" /> is equal to the default value. The default is a null reference.</returns>
		// Token: 0x170008EE RID: 2286
		// (get) Token: 0x06002930 RID: 10544 RVA: 0x000E1276 File Offset: 0x000DF476
		// (set) Token: 0x06002931 RID: 10545 RVA: 0x000E1290 File Offset: 0x000DF490
		[XmlAttribute("minOccurs")]
		public string MinOccursString
		{
			get
			{
				if ((this.flags & XmlSchemaParticle.Occurs.Min) != XmlSchemaParticle.Occurs.None)
				{
					return XmlConvert.ToString(this.minOccurs);
				}
				return null;
			}
			set
			{
				if (value == null)
				{
					this.minOccurs = 1m;
					this.flags &= ~XmlSchemaParticle.Occurs.Min;
					return;
				}
				this.minOccurs = XmlConvert.ToInteger(value);
				if (this.minOccurs < 0m)
				{
					throw new XmlSchemaException("The value for the 'minOccurs' attribute must be xsd:nonNegativeInteger.", string.Empty);
				}
				this.flags |= XmlSchemaParticle.Occurs.Min;
			}
		}

		/// <summary>Gets or sets the number as a string value. Maximum number of times the particle can occur.</summary>
		/// <returns>The number as a string value. <see langword="String.Empty" /> indicates that <see langword="MaxOccurs" /> is equal to the default value. The default is a null reference.</returns>
		// Token: 0x170008EF RID: 2287
		// (get) Token: 0x06002932 RID: 10546 RVA: 0x000E12F7 File Offset: 0x000DF4F7
		// (set) Token: 0x06002933 RID: 10547 RVA: 0x000E1330 File Offset: 0x000DF530
		[XmlAttribute("maxOccurs")]
		public string MaxOccursString
		{
			get
			{
				if ((this.flags & XmlSchemaParticle.Occurs.Max) == XmlSchemaParticle.Occurs.None)
				{
					return null;
				}
				if (!(this.maxOccurs == 79228162514264337593543950335m))
				{
					return XmlConvert.ToString(this.maxOccurs);
				}
				return "unbounded";
			}
			set
			{
				if (value == null)
				{
					this.maxOccurs = 1m;
					this.flags &= ~XmlSchemaParticle.Occurs.Max;
					return;
				}
				if (value == "unbounded")
				{
					this.maxOccurs = decimal.MaxValue;
				}
				else
				{
					this.maxOccurs = XmlConvert.ToInteger(value);
					if (this.maxOccurs < 0m)
					{
						throw new XmlSchemaException("The value for the 'maxOccurs' attribute must be xsd:nonNegativeInteger or 'unbounded'.", string.Empty);
					}
					if (this.maxOccurs == 0m && (this.flags & XmlSchemaParticle.Occurs.Min) == XmlSchemaParticle.Occurs.None)
					{
						this.minOccurs = 0m;
					}
				}
				this.flags |= XmlSchemaParticle.Occurs.Max;
			}
		}

		/// <summary>Gets or sets the minimum number of times the particle can occur.</summary>
		/// <returns>The minimum number of times the particle can occur. The default is 1.</returns>
		// Token: 0x170008F0 RID: 2288
		// (get) Token: 0x06002934 RID: 10548 RVA: 0x000E13DE File Offset: 0x000DF5DE
		// (set) Token: 0x06002935 RID: 10549 RVA: 0x000E13E8 File Offset: 0x000DF5E8
		[XmlIgnore]
		public decimal MinOccurs
		{
			get
			{
				return this.minOccurs;
			}
			set
			{
				if (value < 0m || value != decimal.Truncate(value))
				{
					throw new XmlSchemaException("The value for the 'minOccurs' attribute must be xsd:nonNegativeInteger.", string.Empty);
				}
				this.minOccurs = value;
				this.flags |= XmlSchemaParticle.Occurs.Min;
			}
		}

		/// <summary>Gets or sets the maximum number of times the particle can occur.</summary>
		/// <returns>The maximum number of times the particle can occur. The default is 1.</returns>
		// Token: 0x170008F1 RID: 2289
		// (get) Token: 0x06002936 RID: 10550 RVA: 0x000E1435 File Offset: 0x000DF635
		// (set) Token: 0x06002937 RID: 10551 RVA: 0x000E1440 File Offset: 0x000DF640
		[XmlIgnore]
		public decimal MaxOccurs
		{
			get
			{
				return this.maxOccurs;
			}
			set
			{
				if (value < 0m || value != decimal.Truncate(value))
				{
					throw new XmlSchemaException("The value for the 'maxOccurs' attribute must be xsd:nonNegativeInteger or 'unbounded'.", string.Empty);
				}
				this.maxOccurs = value;
				if (this.maxOccurs == 0m && (this.flags & XmlSchemaParticle.Occurs.Min) == XmlSchemaParticle.Occurs.None)
				{
					this.minOccurs = 0m;
				}
				this.flags |= XmlSchemaParticle.Occurs.Max;
			}
		}

		// Token: 0x170008F2 RID: 2290
		// (get) Token: 0x06002938 RID: 10552 RVA: 0x000E14B5 File Offset: 0x000DF6B5
		internal virtual bool IsEmpty
		{
			get
			{
				return this.maxOccurs == 0m;
			}
		}

		// Token: 0x170008F3 RID: 2291
		// (get) Token: 0x06002939 RID: 10553 RVA: 0x000E14C7 File Offset: 0x000DF6C7
		internal bool IsMultipleOccurrence
		{
			get
			{
				return this.maxOccurs > 1m;
			}
		}

		// Token: 0x170008F4 RID: 2292
		// (get) Token: 0x0600293A RID: 10554 RVA: 0x00003201 File Offset: 0x00001401
		internal virtual string NameString
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x0600293B RID: 10555 RVA: 0x000E14DC File Offset: 0x000DF6DC
		internal XmlQualifiedName GetQualifiedName()
		{
			XmlSchemaElement xmlSchemaElement = this as XmlSchemaElement;
			if (xmlSchemaElement != null)
			{
				return xmlSchemaElement.QualifiedName;
			}
			XmlSchemaAny xmlSchemaAny = this as XmlSchemaAny;
			if (xmlSchemaAny != null)
			{
				string text = xmlSchemaAny.Namespace;
				if (text != null)
				{
					text = text.Trim();
				}
				else
				{
					text = string.Empty;
				}
				return new XmlQualifiedName("*", (text.Length == 0) ? "##any" : text);
			}
			return XmlQualifiedName.Empty;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaParticle" /> class.</summary>
		// Token: 0x0600293C RID: 10556 RVA: 0x000E153D File Offset: 0x000DF73D
		protected XmlSchemaParticle()
		{
		}

		// Token: 0x0600293D RID: 10557 RVA: 0x000E155B File Offset: 0x000DF75B
		// Note: this type is marked as 'beforefieldinit'.
		static XmlSchemaParticle()
		{
		}

		// Token: 0x04001CB3 RID: 7347
		private decimal minOccurs = 1m;

		// Token: 0x04001CB4 RID: 7348
		private decimal maxOccurs = 1m;

		// Token: 0x04001CB5 RID: 7349
		private XmlSchemaParticle.Occurs flags;

		// Token: 0x04001CB6 RID: 7350
		internal static readonly XmlSchemaParticle Empty = new XmlSchemaParticle.EmptyParticle();

		// Token: 0x02000446 RID: 1094
		[Flags]
		private enum Occurs
		{
			// Token: 0x04001CB8 RID: 7352
			None = 0,
			// Token: 0x04001CB9 RID: 7353
			Min = 1,
			// Token: 0x04001CBA RID: 7354
			Max = 2
		}

		// Token: 0x02000447 RID: 1095
		private class EmptyParticle : XmlSchemaParticle
		{
			// Token: 0x170008F5 RID: 2293
			// (get) Token: 0x0600293E RID: 10558 RVA: 0x000033DE File Offset: 0x000015DE
			internal override bool IsEmpty
			{
				get
				{
					return true;
				}
			}

			// Token: 0x0600293F RID: 10559 RVA: 0x000DE383 File Offset: 0x000DC583
			public EmptyParticle()
			{
			}
		}
	}
}
