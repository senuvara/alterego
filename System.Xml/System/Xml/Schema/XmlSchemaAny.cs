﻿using System;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the World Wide Web Consortium (W3C) <see langword="any" /> element.</summary>
	// Token: 0x02000406 RID: 1030
	public class XmlSchemaAny : XmlSchemaParticle
	{
		/// <summary>Gets or sets the namespaces containing the elements that can be used.</summary>
		/// <returns>Namespaces for elements that are available for use. The default is <see langword="##any" />.Optional.</returns>
		// Token: 0x17000820 RID: 2080
		// (get) Token: 0x0600271A RID: 10010 RVA: 0x000DE1EC File Offset: 0x000DC3EC
		// (set) Token: 0x0600271B RID: 10011 RVA: 0x000DE1F4 File Offset: 0x000DC3F4
		[XmlAttribute("namespace")]
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		/// <summary>Gets or sets information about how an application or XML processor should handle the validation of XML documents for the elements specified by the <see langword="any" /> element.</summary>
		/// <returns>One of the <see cref="T:System.Xml.Schema.XmlSchemaContentProcessing" /> values. If no <see langword="processContents" /> attribute is specified, the default is <see langword="Strict" />.</returns>
		// Token: 0x17000821 RID: 2081
		// (get) Token: 0x0600271C RID: 10012 RVA: 0x000DE1FD File Offset: 0x000DC3FD
		// (set) Token: 0x0600271D RID: 10013 RVA: 0x000DE205 File Offset: 0x000DC405
		[DefaultValue(XmlSchemaContentProcessing.None)]
		[XmlAttribute("processContents")]
		public XmlSchemaContentProcessing ProcessContents
		{
			get
			{
				return this.processContents;
			}
			set
			{
				this.processContents = value;
			}
		}

		// Token: 0x17000822 RID: 2082
		// (get) Token: 0x0600271E RID: 10014 RVA: 0x000DE20E File Offset: 0x000DC40E
		[XmlIgnore]
		internal NamespaceList NamespaceList
		{
			get
			{
				return this.namespaceList;
			}
		}

		// Token: 0x17000823 RID: 2083
		// (get) Token: 0x0600271F RID: 10015 RVA: 0x000DE216 File Offset: 0x000DC416
		[XmlIgnore]
		internal string ResolvedNamespace
		{
			get
			{
				if (this.ns == null || this.ns.Length == 0)
				{
					return "##any";
				}
				return this.ns;
			}
		}

		// Token: 0x17000824 RID: 2084
		// (get) Token: 0x06002720 RID: 10016 RVA: 0x000DE239 File Offset: 0x000DC439
		[XmlIgnore]
		internal XmlSchemaContentProcessing ProcessContentsCorrect
		{
			get
			{
				if (this.processContents != XmlSchemaContentProcessing.None)
				{
					return this.processContents;
				}
				return XmlSchemaContentProcessing.Strict;
			}
		}

		// Token: 0x17000825 RID: 2085
		// (get) Token: 0x06002721 RID: 10017 RVA: 0x000DE24C File Offset: 0x000DC44C
		internal override string NameString
		{
			get
			{
				switch (this.namespaceList.Type)
				{
				case NamespaceList.ListType.Any:
					return "##any:*";
				case NamespaceList.ListType.Other:
					return "##other:*";
				case NamespaceList.ListType.Set:
				{
					StringBuilder stringBuilder = new StringBuilder();
					int num = 1;
					foreach (object obj in this.namespaceList.Enumerate)
					{
						string str = (string)obj;
						stringBuilder.Append(str + ":*");
						if (num < this.namespaceList.Enumerate.Count)
						{
							stringBuilder.Append(" ");
						}
						num++;
					}
					return stringBuilder.ToString();
				}
				default:
					return string.Empty;
				}
			}
		}

		// Token: 0x06002722 RID: 10018 RVA: 0x000DE320 File Offset: 0x000DC520
		internal void BuildNamespaceList(string targetNamespace)
		{
			if (this.ns != null)
			{
				this.namespaceList = new NamespaceList(this.ns, targetNamespace);
				return;
			}
			this.namespaceList = new NamespaceList();
		}

		// Token: 0x06002723 RID: 10019 RVA: 0x000DE348 File Offset: 0x000DC548
		internal void BuildNamespaceListV1Compat(string targetNamespace)
		{
			if (this.ns != null)
			{
				this.namespaceList = new NamespaceListV1Compat(this.ns, targetNamespace);
				return;
			}
			this.namespaceList = new NamespaceList();
		}

		// Token: 0x06002724 RID: 10020 RVA: 0x000DE370 File Offset: 0x000DC570
		internal bool Allows(XmlQualifiedName qname)
		{
			return this.namespaceList.Allows(qname.Namespace);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAny" /> class.</summary>
		// Token: 0x06002725 RID: 10021 RVA: 0x000DE383 File Offset: 0x000DC583
		public XmlSchemaAny()
		{
		}

		// Token: 0x04001BE6 RID: 7142
		private string ns;

		// Token: 0x04001BE7 RID: 7143
		private XmlSchemaContentProcessing processContents;

		// Token: 0x04001BE8 RID: 7144
		private NamespaceList namespaceList;
	}
}
