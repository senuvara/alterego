﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000371 RID: 881
	internal sealed class QmarkNode : InteriorNode
	{
		// Token: 0x06002197 RID: 8599 RVA: 0x000BA21F File Offset: 0x000B841F
		public override void ConstructPos(BitSet firstpos, BitSet lastpos, BitSet[] followpos)
		{
			base.LeftChild.ConstructPos(firstpos, lastpos, followpos);
		}

		// Token: 0x1700069A RID: 1690
		// (get) Token: 0x06002198 RID: 8600 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool IsNullable
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06002199 RID: 8601 RVA: 0x000BA0D4 File Offset: 0x000B82D4
		public QmarkNode()
		{
		}
	}
}
