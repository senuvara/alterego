﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B8 RID: 952
	internal class Datatype_positiveInteger : Datatype_nonNegativeInteger
	{
		// Token: 0x17000763 RID: 1891
		// (get) Token: 0x06002362 RID: 9058 RVA: 0x000BEB03 File Offset: 0x000BCD03
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_positiveInteger.numeric10FacetsChecker;
			}
		}

		// Token: 0x17000764 RID: 1892
		// (get) Token: 0x06002363 RID: 9059 RVA: 0x000BEB0A File Offset: 0x000BCD0A
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.PositiveInteger;
			}
		}

		// Token: 0x06002364 RID: 9060 RVA: 0x000BE87F File Offset: 0x000BCA7F
		public Datatype_positiveInteger()
		{
		}

		// Token: 0x06002365 RID: 9061 RVA: 0x000BEB0E File Offset: 0x000BCD0E
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_positiveInteger()
		{
		}

		// Token: 0x040018D9 RID: 6361
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(1m, decimal.MaxValue);
	}
}
