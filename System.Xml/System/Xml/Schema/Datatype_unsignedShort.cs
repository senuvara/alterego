﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B6 RID: 950
	internal class Datatype_unsignedShort : Datatype_unsignedInt
	{
		// Token: 0x1700075B RID: 1883
		// (get) Token: 0x06002352 RID: 9042 RVA: 0x000BE97C File Offset: 0x000BCB7C
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_unsignedShort.numeric10FacetsChecker;
			}
		}

		// Token: 0x1700075C RID: 1884
		// (get) Token: 0x06002353 RID: 9043 RVA: 0x000BE983 File Offset: 0x000BCB83
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.UnsignedShort;
			}
		}

		// Token: 0x06002354 RID: 9044 RVA: 0x000BE988 File Offset: 0x000BCB88
		internal override int Compare(object value1, object value2)
		{
			return ((ushort)value1).CompareTo(value2);
		}

		// Token: 0x1700075D RID: 1885
		// (get) Token: 0x06002355 RID: 9045 RVA: 0x000BE9A4 File Offset: 0x000BCBA4
		public override Type ValueType
		{
			get
			{
				return Datatype_unsignedShort.atomicValueType;
			}
		}

		// Token: 0x1700075E RID: 1886
		// (get) Token: 0x06002356 RID: 9046 RVA: 0x000BE9AB File Offset: 0x000BCBAB
		internal override Type ListValueType
		{
			get
			{
				return Datatype_unsignedShort.listValueType;
			}
		}

		// Token: 0x06002357 RID: 9047 RVA: 0x000BE9B4 File Offset: 0x000BCBB4
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_unsignedShort.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				ushort num;
				ex = XmlConvert.TryToUInt16(s, out num);
				if (ex == null)
				{
					ex = Datatype_unsignedShort.numeric10FacetsChecker.CheckValueFacets((int)num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002358 RID: 9048 RVA: 0x000BE9FE File Offset: 0x000BCBFE
		public Datatype_unsignedShort()
		{
		}

		// Token: 0x06002359 RID: 9049 RVA: 0x000BEA06 File Offset: 0x000BCC06
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_unsignedShort()
		{
		}

		// Token: 0x040018D3 RID: 6355
		private static readonly Type atomicValueType = typeof(ushort);

		// Token: 0x040018D4 RID: 6356
		private static readonly Type listValueType = typeof(ushort[]);

		// Token: 0x040018D5 RID: 6357
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(0m, 65535m);
	}
}
