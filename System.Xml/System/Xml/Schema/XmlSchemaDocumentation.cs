﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="documentation" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class specifies information to be read or used by humans within an <see langword="annotation" />.</summary>
	// Token: 0x0200041B RID: 1051
	public class XmlSchemaDocumentation : XmlSchemaObject
	{
		/// <summary>Gets or sets the Uniform Resource Identifier (URI) source of the information.</summary>
		/// <returns>A URI reference. The default is <see langword="String.Empty" />.Optional.</returns>
		// Token: 0x17000881 RID: 2177
		// (get) Token: 0x0600280A RID: 10250 RVA: 0x000DFE44 File Offset: 0x000DE044
		// (set) Token: 0x0600280B RID: 10251 RVA: 0x000DFE4C File Offset: 0x000DE04C
		[XmlAttribute("source", DataType = "anyURI")]
		public string Source
		{
			get
			{
				return this.source;
			}
			set
			{
				this.source = value;
			}
		}

		/// <summary>Gets or sets the <see langword="xml:lang" /> attribute. This serves as an indicator of the language used in the contents.</summary>
		/// <returns>The <see langword="xml:lang" /> attribute.Optional.</returns>
		// Token: 0x17000882 RID: 2178
		// (get) Token: 0x0600280C RID: 10252 RVA: 0x000DFE55 File Offset: 0x000DE055
		// (set) Token: 0x0600280D RID: 10253 RVA: 0x000DFE5D File Offset: 0x000DE05D
		[XmlAttribute("xml:lang")]
		public string Language
		{
			get
			{
				return this.language;
			}
			set
			{
				this.language = (string)XmlSchemaDocumentation.languageType.Datatype.ParseValue(value, null, null);
			}
		}

		/// <summary>Gets or sets an array of <see langword="XmlNodes" /> that represents the documentation child nodes.</summary>
		/// <returns>The array that represents the documentation child nodes.</returns>
		// Token: 0x17000883 RID: 2179
		// (get) Token: 0x0600280E RID: 10254 RVA: 0x000DFE7C File Offset: 0x000DE07C
		// (set) Token: 0x0600280F RID: 10255 RVA: 0x000DFE84 File Offset: 0x000DE084
		[XmlText]
		[XmlAnyElement]
		public XmlNode[] Markup
		{
			get
			{
				return this.markup;
			}
			set
			{
				this.markup = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaDocumentation" /> class.</summary>
		// Token: 0x06002810 RID: 10256 RVA: 0x000DE196 File Offset: 0x000DC396
		public XmlSchemaDocumentation()
		{
		}

		// Token: 0x06002811 RID: 10257 RVA: 0x000DFE8D File Offset: 0x000DE08D
		// Note: this type is marked as 'beforefieldinit'.
		static XmlSchemaDocumentation()
		{
		}

		// Token: 0x04001C3F RID: 7231
		private string source;

		// Token: 0x04001C40 RID: 7232
		private string language;

		// Token: 0x04001C41 RID: 7233
		private XmlNode[] markup;

		// Token: 0x04001C42 RID: 7234
		private static XmlSchemaSimpleType languageType = DatatypeImplementation.GetSimpleTypeFromXsdType(new XmlQualifiedName("language", "http://www.w3.org/2001/XMLSchema"));
	}
}
