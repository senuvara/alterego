﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003E5 RID: 997
	internal sealed class SchemaEntity : IDtdEntityInfo
	{
		// Token: 0x0600255F RID: 9567 RVA: 0x000D2730 File Offset: 0x000D0930
		internal SchemaEntity(XmlQualifiedName qname, bool isParameter)
		{
			this.qname = qname;
			this.isParameter = isParameter;
		}

		// Token: 0x170007B8 RID: 1976
		// (get) Token: 0x06002560 RID: 9568 RVA: 0x000D2751 File Offset: 0x000D0951
		string IDtdEntityInfo.Name
		{
			get
			{
				return this.Name.Name;
			}
		}

		// Token: 0x170007B9 RID: 1977
		// (get) Token: 0x06002561 RID: 9569 RVA: 0x000D275E File Offset: 0x000D095E
		bool IDtdEntityInfo.IsExternal
		{
			get
			{
				return this.IsExternal;
			}
		}

		// Token: 0x170007BA RID: 1978
		// (get) Token: 0x06002562 RID: 9570 RVA: 0x000D2766 File Offset: 0x000D0966
		bool IDtdEntityInfo.IsDeclaredInExternal
		{
			get
			{
				return this.DeclaredInExternal;
			}
		}

		// Token: 0x170007BB RID: 1979
		// (get) Token: 0x06002563 RID: 9571 RVA: 0x000D276E File Offset: 0x000D096E
		bool IDtdEntityInfo.IsUnparsedEntity
		{
			get
			{
				return !this.NData.IsEmpty;
			}
		}

		// Token: 0x170007BC RID: 1980
		// (get) Token: 0x06002564 RID: 9572 RVA: 0x000D277E File Offset: 0x000D097E
		bool IDtdEntityInfo.IsParameterEntity
		{
			get
			{
				return this.isParameter;
			}
		}

		// Token: 0x170007BD RID: 1981
		// (get) Token: 0x06002565 RID: 9573 RVA: 0x000D2786 File Offset: 0x000D0986
		string IDtdEntityInfo.BaseUriString
		{
			get
			{
				return this.BaseURI;
			}
		}

		// Token: 0x170007BE RID: 1982
		// (get) Token: 0x06002566 RID: 9574 RVA: 0x000D278E File Offset: 0x000D098E
		string IDtdEntityInfo.DeclaredUriString
		{
			get
			{
				return this.DeclaredURI;
			}
		}

		// Token: 0x170007BF RID: 1983
		// (get) Token: 0x06002567 RID: 9575 RVA: 0x000D2796 File Offset: 0x000D0996
		string IDtdEntityInfo.SystemId
		{
			get
			{
				return this.Url;
			}
		}

		// Token: 0x170007C0 RID: 1984
		// (get) Token: 0x06002568 RID: 9576 RVA: 0x000D279E File Offset: 0x000D099E
		string IDtdEntityInfo.PublicId
		{
			get
			{
				return this.Pubid;
			}
		}

		// Token: 0x170007C1 RID: 1985
		// (get) Token: 0x06002569 RID: 9577 RVA: 0x000D27A6 File Offset: 0x000D09A6
		string IDtdEntityInfo.Text
		{
			get
			{
				return this.Text;
			}
		}

		// Token: 0x170007C2 RID: 1986
		// (get) Token: 0x0600256A RID: 9578 RVA: 0x000D27AE File Offset: 0x000D09AE
		int IDtdEntityInfo.LineNumber
		{
			get
			{
				return this.Line;
			}
		}

		// Token: 0x170007C3 RID: 1987
		// (get) Token: 0x0600256B RID: 9579 RVA: 0x000D27B6 File Offset: 0x000D09B6
		int IDtdEntityInfo.LinePosition
		{
			get
			{
				return this.Pos;
			}
		}

		// Token: 0x0600256C RID: 9580 RVA: 0x000D27C0 File Offset: 0x000D09C0
		internal static bool IsPredefinedEntity(string n)
		{
			return n == "lt" || n == "gt" || n == "amp" || n == "apos" || n == "quot";
		}

		// Token: 0x170007C4 RID: 1988
		// (get) Token: 0x0600256D RID: 9581 RVA: 0x000D280E File Offset: 0x000D0A0E
		internal XmlQualifiedName Name
		{
			get
			{
				return this.qname;
			}
		}

		// Token: 0x170007C5 RID: 1989
		// (get) Token: 0x0600256E RID: 9582 RVA: 0x000D2816 File Offset: 0x000D0A16
		// (set) Token: 0x0600256F RID: 9583 RVA: 0x000D281E File Offset: 0x000D0A1E
		internal string Url
		{
			get
			{
				return this.url;
			}
			set
			{
				this.url = value;
				this.isExternal = true;
			}
		}

		// Token: 0x170007C6 RID: 1990
		// (get) Token: 0x06002570 RID: 9584 RVA: 0x000D282E File Offset: 0x000D0A2E
		// (set) Token: 0x06002571 RID: 9585 RVA: 0x000D2836 File Offset: 0x000D0A36
		internal string Pubid
		{
			get
			{
				return this.pubid;
			}
			set
			{
				this.pubid = value;
			}
		}

		// Token: 0x170007C7 RID: 1991
		// (get) Token: 0x06002572 RID: 9586 RVA: 0x000D283F File Offset: 0x000D0A3F
		// (set) Token: 0x06002573 RID: 9587 RVA: 0x000D2847 File Offset: 0x000D0A47
		internal bool IsExternal
		{
			get
			{
				return this.isExternal;
			}
			set
			{
				this.isExternal = value;
			}
		}

		// Token: 0x170007C8 RID: 1992
		// (get) Token: 0x06002574 RID: 9588 RVA: 0x000D2850 File Offset: 0x000D0A50
		// (set) Token: 0x06002575 RID: 9589 RVA: 0x000D2858 File Offset: 0x000D0A58
		internal bool DeclaredInExternal
		{
			get
			{
				return this.isDeclaredInExternal;
			}
			set
			{
				this.isDeclaredInExternal = value;
			}
		}

		// Token: 0x170007C9 RID: 1993
		// (get) Token: 0x06002576 RID: 9590 RVA: 0x000D2861 File Offset: 0x000D0A61
		// (set) Token: 0x06002577 RID: 9591 RVA: 0x000D2869 File Offset: 0x000D0A69
		internal XmlQualifiedName NData
		{
			get
			{
				return this.ndata;
			}
			set
			{
				this.ndata = value;
			}
		}

		// Token: 0x170007CA RID: 1994
		// (get) Token: 0x06002578 RID: 9592 RVA: 0x000D2872 File Offset: 0x000D0A72
		// (set) Token: 0x06002579 RID: 9593 RVA: 0x000D287A File Offset: 0x000D0A7A
		internal string Text
		{
			get
			{
				return this.text;
			}
			set
			{
				this.text = value;
				this.isExternal = false;
			}
		}

		// Token: 0x170007CB RID: 1995
		// (get) Token: 0x0600257A RID: 9594 RVA: 0x000D288A File Offset: 0x000D0A8A
		// (set) Token: 0x0600257B RID: 9595 RVA: 0x000D2892 File Offset: 0x000D0A92
		internal int Line
		{
			get
			{
				return this.lineNumber;
			}
			set
			{
				this.lineNumber = value;
			}
		}

		// Token: 0x170007CC RID: 1996
		// (get) Token: 0x0600257C RID: 9596 RVA: 0x000D289B File Offset: 0x000D0A9B
		// (set) Token: 0x0600257D RID: 9597 RVA: 0x000D28A3 File Offset: 0x000D0AA3
		internal int Pos
		{
			get
			{
				return this.linePosition;
			}
			set
			{
				this.linePosition = value;
			}
		}

		// Token: 0x170007CD RID: 1997
		// (get) Token: 0x0600257E RID: 9598 RVA: 0x000D28AC File Offset: 0x000D0AAC
		// (set) Token: 0x0600257F RID: 9599 RVA: 0x000D28C2 File Offset: 0x000D0AC2
		internal string BaseURI
		{
			get
			{
				if (this.baseURI != null)
				{
					return this.baseURI;
				}
				return string.Empty;
			}
			set
			{
				this.baseURI = value;
			}
		}

		// Token: 0x170007CE RID: 1998
		// (get) Token: 0x06002580 RID: 9600 RVA: 0x000D28CB File Offset: 0x000D0ACB
		// (set) Token: 0x06002581 RID: 9601 RVA: 0x000D28D3 File Offset: 0x000D0AD3
		internal bool ParsingInProgress
		{
			get
			{
				return this.parsingInProgress;
			}
			set
			{
				this.parsingInProgress = value;
			}
		}

		// Token: 0x170007CF RID: 1999
		// (get) Token: 0x06002582 RID: 9602 RVA: 0x000D28DC File Offset: 0x000D0ADC
		// (set) Token: 0x06002583 RID: 9603 RVA: 0x000D28F2 File Offset: 0x000D0AF2
		internal string DeclaredURI
		{
			get
			{
				if (this.declaredURI != null)
				{
					return this.declaredURI;
				}
				return string.Empty;
			}
			set
			{
				this.declaredURI = value;
			}
		}

		// Token: 0x040019D3 RID: 6611
		private XmlQualifiedName qname;

		// Token: 0x040019D4 RID: 6612
		private string url;

		// Token: 0x040019D5 RID: 6613
		private string pubid;

		// Token: 0x040019D6 RID: 6614
		private string text;

		// Token: 0x040019D7 RID: 6615
		private XmlQualifiedName ndata = XmlQualifiedName.Empty;

		// Token: 0x040019D8 RID: 6616
		private int lineNumber;

		// Token: 0x040019D9 RID: 6617
		private int linePosition;

		// Token: 0x040019DA RID: 6618
		private bool isParameter;

		// Token: 0x040019DB RID: 6619
		private bool isExternal;

		// Token: 0x040019DC RID: 6620
		private bool parsingInProgress;

		// Token: 0x040019DD RID: 6621
		private bool isDeclaredInExternal;

		// Token: 0x040019DE RID: 6622
		private string baseURI;

		// Token: 0x040019DF RID: 6623
		private string declaredURI;
	}
}
