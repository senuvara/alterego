﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x02000368 RID: 872
	internal class Positions
	{
		// Token: 0x0600216B RID: 8555 RVA: 0x000B9C50 File Offset: 0x000B7E50
		public int Add(int symbol, object particle)
		{
			return this.positions.Add(new Position(symbol, particle));
		}

		// Token: 0x1700068E RID: 1678
		public Position this[int pos]
		{
			get
			{
				return (Position)this.positions[pos];
			}
		}

		// Token: 0x1700068F RID: 1679
		// (get) Token: 0x0600216D RID: 8557 RVA: 0x000B9C7C File Offset: 0x000B7E7C
		public int Count
		{
			get
			{
				return this.positions.Count;
			}
		}

		// Token: 0x0600216E RID: 8558 RVA: 0x000B9C89 File Offset: 0x000B7E89
		public Positions()
		{
		}

		// Token: 0x040017EB RID: 6123
		private ArrayList positions = new ArrayList();
	}
}
