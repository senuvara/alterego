﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>This class represents the <see langword="keyref" /> element from XMLSchema as specified by the World Wide Web Consortium (W3C).</summary>
	// Token: 0x02000436 RID: 1078
	public class XmlSchemaKeyref : XmlSchemaIdentityConstraint
	{
		/// <summary>Gets or sets the name of the key that this constraint refers to in another simple or complex type.</summary>
		/// <returns>The QName of the key that this constraint refers to.</returns>
		// Token: 0x170008C1 RID: 2241
		// (get) Token: 0x060028AE RID: 10414 RVA: 0x000E085F File Offset: 0x000DEA5F
		// (set) Token: 0x060028AF RID: 10415 RVA: 0x000E0867 File Offset: 0x000DEA67
		[XmlAttribute("refer")]
		public XmlQualifiedName Refer
		{
			get
			{
				return this.refer;
			}
			set
			{
				this.refer = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaKeyref" /> class.</summary>
		// Token: 0x060028B0 RID: 10416 RVA: 0x000E0880 File Offset: 0x000DEA80
		public XmlSchemaKeyref()
		{
		}

		// Token: 0x04001C89 RID: 7305
		private XmlQualifiedName refer = XmlQualifiedName.Empty;
	}
}
