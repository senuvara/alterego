﻿using System;
using System.Resources;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Xml.Schema
{
	/// <summary>Returns detailed information about the schema exception.</summary>
	// Token: 0x0200041D RID: 1053
	[Serializable]
	public class XmlSchemaException : SystemException
	{
		/// <summary>Constructs a new <see langword="XmlSchemaException" /> object with the given <see langword="SerializationInfo" /> and <see langword="StreamingContext" /> information that contains all the properties of the <see langword="XmlSchemaException" />.</summary>
		/// <param name="info">SerializationInfo.</param>
		/// <param name="context">StreamingContext.</param>
		// Token: 0x06002843 RID: 10307 RVA: 0x000E01FC File Offset: 0x000DE3FC
		protected XmlSchemaException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.res = (string)info.GetValue("res", typeof(string));
			this.args = (string[])info.GetValue("args", typeof(string[]));
			this.sourceUri = (string)info.GetValue("sourceUri", typeof(string));
			this.lineNumber = (int)info.GetValue("lineNumber", typeof(int));
			this.linePosition = (int)info.GetValue("linePosition", typeof(int));
			string text = null;
			foreach (SerializationEntry serializationEntry in info)
			{
				if (serializationEntry.Name == "version")
				{
					text = (string)serializationEntry.Value;
				}
			}
			if (text == null)
			{
				this.message = XmlSchemaException.CreateMessage(this.res, this.args);
				return;
			}
			this.message = null;
		}

		/// <summary>Streams all the <see langword="XmlSchemaException" /> properties into the <see langword="SerializationInfo" /> class for the given <see langword="StreamingContext" />.</summary>
		/// <param name="info">The <see langword="SerializationInfo" />. </param>
		/// <param name="context">The <see langword="StreamingContext" /> information. </param>
		// Token: 0x06002844 RID: 10308 RVA: 0x000E0310 File Offset: 0x000DE510
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("res", this.res);
			info.AddValue("args", this.args);
			info.AddValue("sourceUri", this.sourceUri);
			info.AddValue("lineNumber", this.lineNumber);
			info.AddValue("linePosition", this.linePosition);
			info.AddValue("version", "2.0");
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaException" /> class.</summary>
		// Token: 0x06002845 RID: 10309 RVA: 0x000C5DF3 File Offset: 0x000C3FF3
		public XmlSchemaException() : this(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaException" /> class with the exception message specified.</summary>
		/// <param name="message">A <see langword="string" /> description of the error condition.</param>
		// Token: 0x06002846 RID: 10310 RVA: 0x000C5DFC File Offset: 0x000C3FFC
		public XmlSchemaException(string message) : this(message, null, 0, 0)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaException" /> class with the exception message and original <see cref="T:System.Exception" /> object that caused this exception specified.</summary>
		/// <param name="message">A <see langword="string" /> description of the error condition.</param>
		/// <param name="innerException">The original T:System.Exception object that caused this exception.</param>
		// Token: 0x06002847 RID: 10311 RVA: 0x000C5E08 File Offset: 0x000C4008
		public XmlSchemaException(string message, Exception innerException) : this(message, innerException, 0, 0)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaException" /> class with the exception message specified, and the original <see cref="T:System.Exception" /> object, line number, and line position of the XML that cause this exception specified.</summary>
		/// <param name="message">A <see langword="string" /> description of the error condition.</param>
		/// <param name="innerException">The original T:System.Exception object that caused this exception.</param>
		/// <param name="lineNumber">The line number of the XML that caused this exception.</param>
		/// <param name="linePosition">The line position of the XML that caused this exception.</param>
		// Token: 0x06002848 RID: 10312 RVA: 0x000E038A File Offset: 0x000DE58A
		public XmlSchemaException(string message, Exception innerException, int lineNumber, int linePosition) : this((message == null) ? "A schema error occurred." : "{0}", new string[]
		{
			message
		}, innerException, null, lineNumber, linePosition, null)
		{
		}

		// Token: 0x06002849 RID: 10313 RVA: 0x000C5E21 File Offset: 0x000C4021
		internal XmlSchemaException(string res, string[] args) : this(res, args, null, null, 0, 0, null)
		{
		}

		// Token: 0x0600284A RID: 10314 RVA: 0x000C5E30 File Offset: 0x000C4030
		internal XmlSchemaException(string res, string arg) : this(res, new string[]
		{
			arg
		}, null, null, 0, 0, null)
		{
		}

		// Token: 0x0600284B RID: 10315 RVA: 0x000C5E48 File Offset: 0x000C4048
		internal XmlSchemaException(string res, string arg, string sourceUri, int lineNumber, int linePosition) : this(res, new string[]
		{
			arg
		}, null, sourceUri, lineNumber, linePosition, null)
		{
		}

		// Token: 0x0600284C RID: 10316 RVA: 0x000C5E62 File Offset: 0x000C4062
		internal XmlSchemaException(string res, string sourceUri, int lineNumber, int linePosition) : this(res, null, null, sourceUri, lineNumber, linePosition, null)
		{
		}

		// Token: 0x0600284D RID: 10317 RVA: 0x000C5E72 File Offset: 0x000C4072
		internal XmlSchemaException(string res, string[] args, string sourceUri, int lineNumber, int linePosition) : this(res, args, null, sourceUri, lineNumber, linePosition, null)
		{
		}

		// Token: 0x0600284E RID: 10318 RVA: 0x000E03B1 File Offset: 0x000DE5B1
		internal XmlSchemaException(string res, XmlSchemaObject source) : this(res, null, source)
		{
		}

		// Token: 0x0600284F RID: 10319 RVA: 0x000E03BC File Offset: 0x000DE5BC
		internal XmlSchemaException(string res, string arg, XmlSchemaObject source) : this(res, new string[]
		{
			arg
		}, source)
		{
		}

		// Token: 0x06002850 RID: 10320 RVA: 0x000E03D0 File Offset: 0x000DE5D0
		internal XmlSchemaException(string res, string[] args, XmlSchemaObject source) : this(res, args, null, source.SourceUri, source.LineNumber, source.LinePosition, source)
		{
		}

		// Token: 0x06002851 RID: 10321 RVA: 0x000E03F0 File Offset: 0x000DE5F0
		internal XmlSchemaException(string res, string[] args, Exception innerException, string sourceUri, int lineNumber, int linePosition, XmlSchemaObject source) : base(XmlSchemaException.CreateMessage(res, args), innerException)
		{
			base.HResult = -2146231999;
			this.res = res;
			this.args = args;
			this.sourceUri = sourceUri;
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
			this.sourceSchemaObject = source;
		}

		// Token: 0x06002852 RID: 10322 RVA: 0x000E0444 File Offset: 0x000DE644
		internal static string CreateMessage(string res, string[] args)
		{
			string result;
			try
			{
				result = Res.GetString(res, args);
			}
			catch (MissingManifestResourceException)
			{
				result = "UNKNOWN(" + res + ")";
			}
			return result;
		}

		// Token: 0x1700089E RID: 2206
		// (get) Token: 0x06002853 RID: 10323 RVA: 0x000E0480 File Offset: 0x000DE680
		internal string GetRes
		{
			get
			{
				return this.res;
			}
		}

		// Token: 0x1700089F RID: 2207
		// (get) Token: 0x06002854 RID: 10324 RVA: 0x000E0488 File Offset: 0x000DE688
		internal string[] Args
		{
			get
			{
				return this.args;
			}
		}

		/// <summary>Gets the Uniform Resource Identifier (URI) location of the schema that caused the exception.</summary>
		/// <returns>The URI location of the schema that caused the exception.</returns>
		// Token: 0x170008A0 RID: 2208
		// (get) Token: 0x06002855 RID: 10325 RVA: 0x000E0490 File Offset: 0x000DE690
		public string SourceUri
		{
			get
			{
				return this.sourceUri;
			}
		}

		/// <summary>Gets the line number indicating where the error occurred.</summary>
		/// <returns>The line number indicating where the error occurred.</returns>
		// Token: 0x170008A1 RID: 2209
		// (get) Token: 0x06002856 RID: 10326 RVA: 0x000E0498 File Offset: 0x000DE698
		public int LineNumber
		{
			get
			{
				return this.lineNumber;
			}
		}

		/// <summary>Gets the line position indicating where the error occurred.</summary>
		/// <returns>The line position indicating where the error occurred.</returns>
		// Token: 0x170008A2 RID: 2210
		// (get) Token: 0x06002857 RID: 10327 RVA: 0x000E04A0 File Offset: 0x000DE6A0
		public int LinePosition
		{
			get
			{
				return this.linePosition;
			}
		}

		/// <summary>The <see langword="XmlSchemaObject" /> that produced the <see langword="XmlSchemaException" />.</summary>
		/// <returns>A valid object instance represents a structural validation error in the XML Schema Object Model (SOM).</returns>
		// Token: 0x170008A3 RID: 2211
		// (get) Token: 0x06002858 RID: 10328 RVA: 0x000E04A8 File Offset: 0x000DE6A8
		public XmlSchemaObject SourceSchemaObject
		{
			get
			{
				return this.sourceSchemaObject;
			}
		}

		// Token: 0x06002859 RID: 10329 RVA: 0x000E04B0 File Offset: 0x000DE6B0
		internal void SetSource(string sourceUri, int lineNumber, int linePosition)
		{
			this.sourceUri = sourceUri;
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		// Token: 0x0600285A RID: 10330 RVA: 0x000E04C7 File Offset: 0x000DE6C7
		internal void SetSchemaObject(XmlSchemaObject source)
		{
			this.sourceSchemaObject = source;
		}

		// Token: 0x0600285B RID: 10331 RVA: 0x000E04D0 File Offset: 0x000DE6D0
		internal void SetSource(XmlSchemaObject source)
		{
			this.sourceSchemaObject = source;
			this.sourceUri = source.SourceUri;
			this.lineNumber = source.LineNumber;
			this.linePosition = source.LinePosition;
		}

		// Token: 0x0600285C RID: 10332 RVA: 0x000E04FD File Offset: 0x000DE6FD
		internal void SetResourceId(string resourceId)
		{
			this.res = resourceId;
		}

		/// <summary>Gets the description of the error condition of this exception.</summary>
		/// <returns>The description of the error condition of this exception.</returns>
		// Token: 0x170008A4 RID: 2212
		// (get) Token: 0x0600285D RID: 10333 RVA: 0x000E0506 File Offset: 0x000DE706
		public override string Message
		{
			get
			{
				if (this.message != null)
				{
					return this.message;
				}
				return base.Message;
			}
		}

		// Token: 0x04001C58 RID: 7256
		private string res;

		// Token: 0x04001C59 RID: 7257
		private string[] args;

		// Token: 0x04001C5A RID: 7258
		private string sourceUri;

		// Token: 0x04001C5B RID: 7259
		private int lineNumber;

		// Token: 0x04001C5C RID: 7260
		private int linePosition;

		// Token: 0x04001C5D RID: 7261
		[NonSerialized]
		private XmlSchemaObject sourceSchemaObject;

		// Token: 0x04001C5E RID: 7262
		private string message;
	}
}
