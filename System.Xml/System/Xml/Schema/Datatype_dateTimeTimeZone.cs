﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000391 RID: 913
	internal class Datatype_dateTimeTimeZone : Datatype_dateTimeBase
	{
		// Token: 0x060022A6 RID: 8870 RVA: 0x000BDEFD File Offset: 0x000BC0FD
		internal Datatype_dateTimeTimeZone() : base(XsdDateTimeFlags.XdrDateTime)
		{
		}
	}
}
