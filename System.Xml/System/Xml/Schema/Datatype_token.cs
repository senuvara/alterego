﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A2 RID: 930
	internal class Datatype_token : Datatype_normalizedString
	{
		// Token: 0x17000724 RID: 1828
		// (get) Token: 0x060022EC RID: 8940 RVA: 0x000BE234 File Offset: 0x000BC434
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Token;
			}
		}

		// Token: 0x17000725 RID: 1829
		// (get) Token: 0x060022ED RID: 8941 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x060022EE RID: 8942 RVA: 0x000BE238 File Offset: 0x000BC438
		public Datatype_token()
		{
		}
	}
}
