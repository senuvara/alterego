﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Abstract class for all facets that are used when simple types are derived by restriction.</summary>
	// Token: 0x02000420 RID: 1056
	public abstract class XmlSchemaFacet : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the <see langword="value" /> attribute of the facet.</summary>
		/// <returns>The value attribute.</returns>
		// Token: 0x170008AC RID: 2220
		// (get) Token: 0x0600286E RID: 10350 RVA: 0x000E0594 File Offset: 0x000DE794
		// (set) Token: 0x0600286F RID: 10351 RVA: 0x000E059C File Offset: 0x000DE79C
		[XmlAttribute("value")]
		public string Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		/// <summary>Gets or sets information that indicates that this facet is fixed.</summary>
		/// <returns>If <see langword="true" />, value is fixed; otherwise, <see langword="false" />. The default is <see langword="false" />.Optional.</returns>
		// Token: 0x170008AD RID: 2221
		// (get) Token: 0x06002870 RID: 10352 RVA: 0x000E05A5 File Offset: 0x000DE7A5
		// (set) Token: 0x06002871 RID: 10353 RVA: 0x000E05AD File Offset: 0x000DE7AD
		[XmlAttribute("fixed")]
		[DefaultValue(false)]
		public virtual bool IsFixed
		{
			get
			{
				return this.isFixed;
			}
			set
			{
				if (!(this is XmlSchemaEnumerationFacet) && !(this is XmlSchemaPatternFacet))
				{
					this.isFixed = value;
				}
			}
		}

		// Token: 0x170008AE RID: 2222
		// (get) Token: 0x06002872 RID: 10354 RVA: 0x000E05C6 File Offset: 0x000DE7C6
		// (set) Token: 0x06002873 RID: 10355 RVA: 0x000E05CE File Offset: 0x000DE7CE
		internal FacetType FacetType
		{
			get
			{
				return this.facetType;
			}
			set
			{
				this.facetType = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaFacet" /> class.</summary>
		// Token: 0x06002874 RID: 10356 RVA: 0x000DE4D5 File Offset: 0x000DC6D5
		protected XmlSchemaFacet()
		{
		}

		// Token: 0x04001C73 RID: 7283
		private string value;

		// Token: 0x04001C74 RID: 7284
		private bool isFixed;

		// Token: 0x04001C75 RID: 7285
		private FacetType facetType;
	}
}
