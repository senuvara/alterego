﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x0200046B RID: 1131
	internal class XmlAnyListConverter : XmlListConverter
	{
		// Token: 0x06002C14 RID: 11284 RVA: 0x000ED2E7 File Offset: 0x000EB4E7
		protected XmlAnyListConverter(XmlBaseConverter atomicConverter) : base(atomicConverter)
		{
		}

		// Token: 0x06002C15 RID: 11285 RVA: 0x000ED2F0 File Offset: 0x000EB4F0
		public override object ChangeType(object value, Type destinationType, IXmlNamespaceResolver nsResolver)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}
			if (!(value is IEnumerable) || value.GetType() == XmlBaseConverter.StringType || value.GetType() == XmlBaseConverter.ByteArrayType)
			{
				value = new object[]
				{
					value
				};
			}
			return this.ChangeListType(value, destinationType, nsResolver);
		}

		// Token: 0x06002C16 RID: 11286 RVA: 0x000ED360 File Offset: 0x000EB560
		// Note: this type is marked as 'beforefieldinit'.
		static XmlAnyListConverter()
		{
		}

		// Token: 0x04001DAB RID: 7595
		public static readonly XmlValueConverter ItemList = new XmlAnyListConverter((XmlBaseConverter)XmlAnyConverter.Item);

		// Token: 0x04001DAC RID: 7596
		public static readonly XmlValueConverter AnyAtomicList = new XmlAnyListConverter((XmlBaseConverter)XmlAnyConverter.AnyAtomic);
	}
}
