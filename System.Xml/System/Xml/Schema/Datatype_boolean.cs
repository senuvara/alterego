﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000388 RID: 904
	internal class Datatype_boolean : Datatype_anySimpleType
	{
		// Token: 0x0600225C RID: 8796 RVA: 0x000BD959 File Offset: 0x000BBB59
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlBooleanConverter.Create(schemaType);
		}

		// Token: 0x170006D8 RID: 1752
		// (get) Token: 0x0600225D RID: 8797 RVA: 0x000BCDB2 File Offset: 0x000BAFB2
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.miscFacetsChecker;
			}
		}

		// Token: 0x170006D9 RID: 1753
		// (get) Token: 0x0600225E RID: 8798 RVA: 0x0007BDAF File Offset: 0x00079FAF
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Boolean;
			}
		}

		// Token: 0x170006DA RID: 1754
		// (get) Token: 0x0600225F RID: 8799 RVA: 0x000BD961 File Offset: 0x000BBB61
		public override Type ValueType
		{
			get
			{
				return Datatype_boolean.atomicValueType;
			}
		}

		// Token: 0x170006DB RID: 1755
		// (get) Token: 0x06002260 RID: 8800 RVA: 0x000BD968 File Offset: 0x000BBB68
		internal override Type ListValueType
		{
			get
			{
				return Datatype_boolean.listValueType;
			}
		}

		// Token: 0x170006DC RID: 1756
		// (get) Token: 0x06002261 RID: 8801 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x170006DD RID: 1757
		// (get) Token: 0x06002262 RID: 8802 RVA: 0x000BD96F File Offset: 0x000BBB6F
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Pattern | RestrictionFlags.WhiteSpace;
			}
		}

		// Token: 0x06002263 RID: 8803 RVA: 0x000BD974 File Offset: 0x000BBB74
		internal override int Compare(object value1, object value2)
		{
			return ((bool)value1).CompareTo(value2);
		}

		// Token: 0x06002264 RID: 8804 RVA: 0x000BD990 File Offset: 0x000BBB90
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.miscFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				bool flag;
				ex = XmlConvert.TryToBoolean(s, out flag);
				if (ex == null)
				{
					typedValue = flag;
					return null;
				}
			}
			return ex;
		}

		// Token: 0x06002265 RID: 8805 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_boolean()
		{
		}

		// Token: 0x06002266 RID: 8806 RVA: 0x000BD9CA File Offset: 0x000BBBCA
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_boolean()
		{
		}

		// Token: 0x040018A6 RID: 6310
		private static readonly Type atomicValueType = typeof(bool);

		// Token: 0x040018A7 RID: 6311
		private static readonly Type listValueType = typeof(bool[]);
	}
}
