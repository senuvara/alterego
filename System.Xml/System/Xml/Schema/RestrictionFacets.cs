﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x0200037F RID: 895
	internal class RestrictionFacets
	{
		// Token: 0x060021ED RID: 8685 RVA: 0x00002103 File Offset: 0x00000303
		public RestrictionFacets()
		{
		}

		// Token: 0x04001832 RID: 6194
		internal int Length;

		// Token: 0x04001833 RID: 6195
		internal int MinLength;

		// Token: 0x04001834 RID: 6196
		internal int MaxLength;

		// Token: 0x04001835 RID: 6197
		internal ArrayList Patterns;

		// Token: 0x04001836 RID: 6198
		internal ArrayList Enumeration;

		// Token: 0x04001837 RID: 6199
		internal XmlSchemaWhiteSpace WhiteSpace;

		// Token: 0x04001838 RID: 6200
		internal object MaxInclusive;

		// Token: 0x04001839 RID: 6201
		internal object MaxExclusive;

		// Token: 0x0400183A RID: 6202
		internal object MinInclusive;

		// Token: 0x0400183B RID: 6203
		internal object MinExclusive;

		// Token: 0x0400183C RID: 6204
		internal int TotalDigits;

		// Token: 0x0400183D RID: 6205
		internal int FractionDigits;

		// Token: 0x0400183E RID: 6206
		internal RestrictionFlags Flags;

		// Token: 0x0400183F RID: 6207
		internal RestrictionFlags FixedFlags;
	}
}
