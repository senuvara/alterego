﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003BD RID: 957
	internal class Datatype_char : Datatype_anySimpleType
	{
		// Token: 0x17000769 RID: 1897
		// (get) Token: 0x06002372 RID: 9074 RVA: 0x000BECBE File Offset: 0x000BCEBE
		public override Type ValueType
		{
			get
			{
				return Datatype_char.atomicValueType;
			}
		}

		// Token: 0x1700076A RID: 1898
		// (get) Token: 0x06002373 RID: 9075 RVA: 0x000BECC5 File Offset: 0x000BCEC5
		internal override Type ListValueType
		{
			get
			{
				return Datatype_char.listValueType;
			}
		}

		// Token: 0x1700076B RID: 1899
		// (get) Token: 0x06002374 RID: 9076 RVA: 0x000020CD File Offset: 0x000002CD
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return (RestrictionFlags)0;
			}
		}

		// Token: 0x06002375 RID: 9077 RVA: 0x000BECCC File Offset: 0x000BCECC
		internal override int Compare(object value1, object value2)
		{
			return ((char)value1).CompareTo(value2);
		}

		// Token: 0x06002376 RID: 9078 RVA: 0x000BECE8 File Offset: 0x000BCEE8
		public override object ParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr)
		{
			object result;
			try
			{
				result = XmlConvert.ToChar(s);
			}
			catch (XmlSchemaException ex)
			{
				throw ex;
			}
			catch (Exception innerException)
			{
				throw new XmlSchemaException(Res.GetString("The value '{0}' is invalid according to its data type.", new object[]
				{
					s
				}), innerException);
			}
			return result;
		}

		// Token: 0x06002377 RID: 9079 RVA: 0x000BED40 File Offset: 0x000BCF40
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			char c;
			Exception ex = XmlConvert.TryToChar(s, out c);
			if (ex == null)
			{
				typedValue = c;
				return null;
			}
			return ex;
		}

		// Token: 0x06002378 RID: 9080 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_char()
		{
		}

		// Token: 0x06002379 RID: 9081 RVA: 0x000BED69 File Offset: 0x000BCF69
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_char()
		{
		}

		// Token: 0x040018DC RID: 6364
		private static readonly Type atomicValueType = typeof(char);

		// Token: 0x040018DD RID: 6365
		private static readonly Type listValueType = typeof(char[]);
	}
}
