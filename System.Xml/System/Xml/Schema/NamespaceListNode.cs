﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x0200036B RID: 875
	internal class NamespaceListNode : SyntaxTreeNode
	{
		// Token: 0x0600217C RID: 8572 RVA: 0x000B9D05 File Offset: 0x000B7F05
		public NamespaceListNode(NamespaceList namespaceList, object particle)
		{
			this.namespaceList = namespaceList;
			this.particle = particle;
		}

		// Token: 0x0600217D RID: 8573 RVA: 0x00007AE0 File Offset: 0x00005CE0
		public override SyntaxTreeNode Clone(Positions positions)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x0600217E RID: 8574 RVA: 0x000B9D1B File Offset: 0x000B7F1B
		public virtual ICollection GetResolvedSymbols(SymbolsDictionary symbols)
		{
			return symbols.GetNamespaceListSymbols(this.namespaceList);
		}

		// Token: 0x0600217F RID: 8575 RVA: 0x000B9D2C File Offset: 0x000B7F2C
		public override void ExpandTree(InteriorNode parent, SymbolsDictionary symbols, Positions positions)
		{
			SyntaxTreeNode syntaxTreeNode = null;
			foreach (object obj in this.GetResolvedSymbols(symbols))
			{
				int symbol = (int)obj;
				if (symbols.GetParticle(symbol) != this.particle)
				{
					symbols.IsUpaEnforced = false;
				}
				LeafNode leafNode = new LeafNode(positions.Add(symbol, this.particle));
				if (syntaxTreeNode == null)
				{
					syntaxTreeNode = leafNode;
				}
				else
				{
					syntaxTreeNode = new ChoiceNode
					{
						LeftChild = syntaxTreeNode,
						RightChild = leafNode
					};
				}
			}
			if (parent.LeftChild == this)
			{
				parent.LeftChild = syntaxTreeNode;
				return;
			}
			parent.RightChild = syntaxTreeNode;
		}

		// Token: 0x06002180 RID: 8576 RVA: 0x00007AE0 File Offset: 0x00005CE0
		public override void ConstructPos(BitSet firstpos, BitSet lastpos, BitSet[] followpos)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x17000694 RID: 1684
		// (get) Token: 0x06002181 RID: 8577 RVA: 0x00007AE0 File Offset: 0x00005CE0
		public override bool IsNullable
		{
			get
			{
				throw new InvalidOperationException();
			}
		}

		// Token: 0x040017ED RID: 6125
		protected NamespaceList namespaceList;

		// Token: 0x040017EE RID: 6126
		protected object particle;
	}
}
