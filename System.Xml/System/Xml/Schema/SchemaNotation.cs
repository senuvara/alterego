﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003EC RID: 1004
	internal sealed class SchemaNotation
	{
		// Token: 0x060025BB RID: 9659 RVA: 0x000D47C0 File Offset: 0x000D29C0
		internal SchemaNotation(XmlQualifiedName name)
		{
			this.name = name;
		}

		// Token: 0x170007E3 RID: 2019
		// (get) Token: 0x060025BC RID: 9660 RVA: 0x000D47CF File Offset: 0x000D29CF
		internal XmlQualifiedName Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170007E4 RID: 2020
		// (get) Token: 0x060025BD RID: 9661 RVA: 0x000D47D7 File Offset: 0x000D29D7
		// (set) Token: 0x060025BE RID: 9662 RVA: 0x000D47DF File Offset: 0x000D29DF
		internal string SystemLiteral
		{
			get
			{
				return this.systemLiteral;
			}
			set
			{
				this.systemLiteral = value;
			}
		}

		// Token: 0x170007E5 RID: 2021
		// (get) Token: 0x060025BF RID: 9663 RVA: 0x000D47E8 File Offset: 0x000D29E8
		// (set) Token: 0x060025C0 RID: 9664 RVA: 0x000D47F0 File Offset: 0x000D29F0
		internal string Pubid
		{
			get
			{
				return this.pubid;
			}
			set
			{
				this.pubid = value;
			}
		}

		// Token: 0x04001B08 RID: 6920
		internal const int SYSTEM = 0;

		// Token: 0x04001B09 RID: 6921
		internal const int PUBLIC = 1;

		// Token: 0x04001B0A RID: 6922
		private XmlQualifiedName name;

		// Token: 0x04001B0B RID: 6923
		private string systemLiteral;

		// Token: 0x04001B0C RID: 6924
		private string pubid;
	}
}
