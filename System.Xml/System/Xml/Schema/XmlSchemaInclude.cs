﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="include" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class is used to include declarations and definitions from an external schema. The included declarations and definitions are then available for processing in the containing schema.</summary>
	// Token: 0x02000438 RID: 1080
	public class XmlSchemaInclude : XmlSchemaExternal
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaInclude" /> class.</summary>
		// Token: 0x060028B7 RID: 10423 RVA: 0x000E08C4 File Offset: 0x000DEAC4
		public XmlSchemaInclude()
		{
			base.Compositor = Compositor.Include;
		}

		/// <summary>Gets or sets the <see langword="annotation" /> property.</summary>
		/// <returns>The annotation.</returns>
		// Token: 0x170008C4 RID: 2244
		// (get) Token: 0x060028B8 RID: 10424 RVA: 0x000E08D3 File Offset: 0x000DEAD3
		// (set) Token: 0x060028B9 RID: 10425 RVA: 0x000E08DB File Offset: 0x000DEADB
		[XmlElement("annotation", typeof(XmlSchemaAnnotation))]
		public XmlSchemaAnnotation Annotation
		{
			get
			{
				return this.annotation;
			}
			set
			{
				this.annotation = value;
			}
		}

		// Token: 0x060028BA RID: 10426 RVA: 0x000E08DB File Offset: 0x000DEADB
		internal override void AddAnnotation(XmlSchemaAnnotation annotation)
		{
			this.annotation = annotation;
		}

		// Token: 0x04001C8C RID: 7308
		private XmlSchemaAnnotation annotation;
	}
}
