﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="minInclusive" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to specify a restriction on the minimum value of a simpleType element. The element value must be greater than or equal to the value of the <see langword="minInclusive" /> element.</summary>
	// Token: 0x02000428 RID: 1064
	public class XmlSchemaMinInclusiveFacet : XmlSchemaFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaMinInclusiveFacet" /> class.</summary>
		// Token: 0x0600287C RID: 10364 RVA: 0x000E0639 File Offset: 0x000DE839
		public XmlSchemaMinInclusiveFacet()
		{
			base.FacetType = FacetType.MinInclusive;
		}
	}
}
