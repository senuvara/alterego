﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003BB RID: 955
	internal class Datatype_QNameXdr : Datatype_anySimpleType
	{
		// Token: 0x17000765 RID: 1893
		// (get) Token: 0x0600236A RID: 9066 RVA: 0x00074D15 File Offset: 0x00072F15
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.QName;
			}
		}

		// Token: 0x0600236B RID: 9067 RVA: 0x000BEC04 File Offset: 0x000BCE04
		public override object ParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr)
		{
			if (s == null || s.Length == 0)
			{
				throw new XmlSchemaException("The attribute value cannot be empty.", string.Empty);
			}
			if (nsmgr == null)
			{
				throw new ArgumentNullException("nsmgr");
			}
			object result;
			try
			{
				string text;
				result = XmlQualifiedName.Parse(s.Trim(), nsmgr, out text);
			}
			catch (XmlSchemaException ex)
			{
				throw ex;
			}
			catch (Exception innerException)
			{
				throw new XmlSchemaException(Res.GetString("The value '{0}' is invalid according to its data type.", new object[]
				{
					s
				}), innerException);
			}
			return result;
		}

		// Token: 0x17000766 RID: 1894
		// (get) Token: 0x0600236C RID: 9068 RVA: 0x000BEC88 File Offset: 0x000BCE88
		public override Type ValueType
		{
			get
			{
				return Datatype_QNameXdr.atomicValueType;
			}
		}

		// Token: 0x17000767 RID: 1895
		// (get) Token: 0x0600236D RID: 9069 RVA: 0x000BEC8F File Offset: 0x000BCE8F
		internal override Type ListValueType
		{
			get
			{
				return Datatype_QNameXdr.listValueType;
			}
		}

		// Token: 0x0600236E RID: 9070 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_QNameXdr()
		{
		}

		// Token: 0x0600236F RID: 9071 RVA: 0x000BEC96 File Offset: 0x000BCE96
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_QNameXdr()
		{
		}

		// Token: 0x040018DA RID: 6362
		private static readonly Type atomicValueType = typeof(XmlQualifiedName);

		// Token: 0x040018DB RID: 6363
		private static readonly Type listValueType = typeof(XmlQualifiedName[]);
	}
}
