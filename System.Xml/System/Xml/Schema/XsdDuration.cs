﻿using System;
using System.Text;

namespace System.Xml.Schema
{
	// Token: 0x0200047B RID: 1147
	internal struct XsdDuration
	{
		// Token: 0x06002CED RID: 11501 RVA: 0x000F2B20 File Offset: 0x000F0D20
		public XsdDuration(bool isNegative, int years, int months, int days, int hours, int minutes, int seconds, int nanoseconds)
		{
			if (years < 0)
			{
				throw new ArgumentOutOfRangeException("years");
			}
			if (months < 0)
			{
				throw new ArgumentOutOfRangeException("months");
			}
			if (days < 0)
			{
				throw new ArgumentOutOfRangeException("days");
			}
			if (hours < 0)
			{
				throw new ArgumentOutOfRangeException("hours");
			}
			if (minutes < 0)
			{
				throw new ArgumentOutOfRangeException("minutes");
			}
			if (seconds < 0)
			{
				throw new ArgumentOutOfRangeException("seconds");
			}
			if (nanoseconds < 0 || nanoseconds > 999999999)
			{
				throw new ArgumentOutOfRangeException("nanoseconds");
			}
			this.years = years;
			this.months = months;
			this.days = days;
			this.hours = hours;
			this.minutes = minutes;
			this.seconds = seconds;
			this.nanoseconds = (uint)nanoseconds;
			if (isNegative)
			{
				this.nanoseconds |= 2147483648U;
			}
		}

		// Token: 0x06002CEE RID: 11502 RVA: 0x000F2BEF File Offset: 0x000F0DEF
		public XsdDuration(TimeSpan timeSpan)
		{
			this = new XsdDuration(timeSpan, XsdDuration.DurationType.Duration);
		}

		// Token: 0x06002CEF RID: 11503 RVA: 0x000F2BFC File Offset: 0x000F0DFC
		public XsdDuration(TimeSpan timeSpan, XsdDuration.DurationType durationType)
		{
			long ticks = timeSpan.Ticks;
			bool flag;
			ulong num;
			if (ticks < 0L)
			{
				flag = true;
				num = (ulong)(-(ulong)ticks);
			}
			else
			{
				flag = false;
				num = (ulong)ticks;
			}
			if (durationType == XsdDuration.DurationType.YearMonthDuration)
			{
				int num2 = (int)(num / 315360000000000UL);
				int num3 = (int)(num % 315360000000000UL / 25920000000000UL);
				if (num3 == 12)
				{
					num2++;
					num3 = 0;
				}
				this = new XsdDuration(flag, num2, num3, 0, 0, 0, 0, 0);
				return;
			}
			this.nanoseconds = (uint)(num % 10000000UL) * 100U;
			if (flag)
			{
				this.nanoseconds |= 2147483648U;
			}
			this.years = 0;
			this.months = 0;
			this.days = (int)(num / 864000000000UL);
			this.hours = (int)(num / 36000000000UL % 24UL);
			this.minutes = (int)(num / 600000000UL % 60UL);
			this.seconds = (int)(num / 10000000UL % 60UL);
		}

		// Token: 0x06002CF0 RID: 11504 RVA: 0x000F2CEF File Offset: 0x000F0EEF
		public XsdDuration(string s)
		{
			this = new XsdDuration(s, XsdDuration.DurationType.Duration);
		}

		// Token: 0x06002CF1 RID: 11505 RVA: 0x000F2CFC File Offset: 0x000F0EFC
		public XsdDuration(string s, XsdDuration.DurationType durationType)
		{
			XsdDuration xsdDuration;
			Exception ex = XsdDuration.TryParse(s, durationType, out xsdDuration);
			if (ex != null)
			{
				throw ex;
			}
			this.years = xsdDuration.Years;
			this.months = xsdDuration.Months;
			this.days = xsdDuration.Days;
			this.hours = xsdDuration.Hours;
			this.minutes = xsdDuration.Minutes;
			this.seconds = xsdDuration.Seconds;
			this.nanoseconds = (uint)xsdDuration.Nanoseconds;
			if (xsdDuration.IsNegative)
			{
				this.nanoseconds |= 2147483648U;
			}
		}

		// Token: 0x17000958 RID: 2392
		// (get) Token: 0x06002CF2 RID: 11506 RVA: 0x000F2D8E File Offset: 0x000F0F8E
		public bool IsNegative
		{
			get
			{
				return (this.nanoseconds & 2147483648U) > 0U;
			}
		}

		// Token: 0x17000959 RID: 2393
		// (get) Token: 0x06002CF3 RID: 11507 RVA: 0x000F2D9F File Offset: 0x000F0F9F
		public int Years
		{
			get
			{
				return this.years;
			}
		}

		// Token: 0x1700095A RID: 2394
		// (get) Token: 0x06002CF4 RID: 11508 RVA: 0x000F2DA7 File Offset: 0x000F0FA7
		public int Months
		{
			get
			{
				return this.months;
			}
		}

		// Token: 0x1700095B RID: 2395
		// (get) Token: 0x06002CF5 RID: 11509 RVA: 0x000F2DAF File Offset: 0x000F0FAF
		public int Days
		{
			get
			{
				return this.days;
			}
		}

		// Token: 0x1700095C RID: 2396
		// (get) Token: 0x06002CF6 RID: 11510 RVA: 0x000F2DB7 File Offset: 0x000F0FB7
		public int Hours
		{
			get
			{
				return this.hours;
			}
		}

		// Token: 0x1700095D RID: 2397
		// (get) Token: 0x06002CF7 RID: 11511 RVA: 0x000F2DBF File Offset: 0x000F0FBF
		public int Minutes
		{
			get
			{
				return this.minutes;
			}
		}

		// Token: 0x1700095E RID: 2398
		// (get) Token: 0x06002CF8 RID: 11512 RVA: 0x000F2DC7 File Offset: 0x000F0FC7
		public int Seconds
		{
			get
			{
				return this.seconds;
			}
		}

		// Token: 0x1700095F RID: 2399
		// (get) Token: 0x06002CF9 RID: 11513 RVA: 0x000F2DCF File Offset: 0x000F0FCF
		public int Nanoseconds
		{
			get
			{
				return (int)(this.nanoseconds & 2147483647U);
			}
		}

		// Token: 0x17000960 RID: 2400
		// (get) Token: 0x06002CFA RID: 11514 RVA: 0x000F2DDD File Offset: 0x000F0FDD
		public int Microseconds
		{
			get
			{
				return this.Nanoseconds / 1000;
			}
		}

		// Token: 0x17000961 RID: 2401
		// (get) Token: 0x06002CFB RID: 11515 RVA: 0x000F2DEB File Offset: 0x000F0FEB
		public int Milliseconds
		{
			get
			{
				return this.Nanoseconds / 1000000;
			}
		}

		// Token: 0x06002CFC RID: 11516 RVA: 0x000F2DFC File Offset: 0x000F0FFC
		public XsdDuration Normalize()
		{
			int num = this.Years;
			int num2 = this.Months;
			int num3 = this.Days;
			int num4 = this.Hours;
			int num5 = this.Minutes;
			int num6 = this.Seconds;
			checked
			{
				try
				{
					if (num2 >= 12)
					{
						num += num2 / 12;
						num2 %= 12;
					}
					if (num6 >= 60)
					{
						num5 += num6 / 60;
						num6 %= 60;
					}
					if (num5 >= 60)
					{
						num4 += num5 / 60;
						num5 %= 60;
					}
					if (num4 >= 24)
					{
						num3 += num4 / 24;
						num4 %= 24;
					}
				}
				catch (OverflowException)
				{
					throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new object[]
					{
						this.ToString(),
						"Duration"
					}));
				}
				return new XsdDuration(this.IsNegative, num, num2, num3, num4, num5, num6, this.Nanoseconds);
			}
		}

		// Token: 0x06002CFD RID: 11517 RVA: 0x000F2EDC File Offset: 0x000F10DC
		public TimeSpan ToTimeSpan()
		{
			return this.ToTimeSpan(XsdDuration.DurationType.Duration);
		}

		// Token: 0x06002CFE RID: 11518 RVA: 0x000F2EE8 File Offset: 0x000F10E8
		public TimeSpan ToTimeSpan(XsdDuration.DurationType durationType)
		{
			TimeSpan result;
			Exception ex = this.TryToTimeSpan(durationType, out result);
			if (ex != null)
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x06002CFF RID: 11519 RVA: 0x000F2F05 File Offset: 0x000F1105
		internal Exception TryToTimeSpan(out TimeSpan result)
		{
			return this.TryToTimeSpan(XsdDuration.DurationType.Duration, out result);
		}

		// Token: 0x06002D00 RID: 11520 RVA: 0x000F2F10 File Offset: 0x000F1110
		internal Exception TryToTimeSpan(XsdDuration.DurationType durationType, out TimeSpan result)
		{
			Exception result2 = null;
			ulong num = 0UL;
			checked
			{
				try
				{
					if (durationType != XsdDuration.DurationType.DayTimeDuration)
					{
						num += ((ulong)this.years + (ulong)this.months / 12UL) * 365UL;
						num += (ulong)this.months % 12UL * 30UL;
					}
					if (durationType != XsdDuration.DurationType.YearMonthDuration)
					{
						num += (ulong)this.days;
						num *= 24UL;
						num += (ulong)this.hours;
						num *= 60UL;
						num += (ulong)this.minutes;
						num *= 60UL;
						num += (ulong)this.seconds;
						num *= 10000000UL;
						num += (ulong)this.Nanoseconds / 100UL;
					}
					else
					{
						num *= 864000000000UL;
					}
					if (this.IsNegative)
					{
						if (num == 9223372036854775808UL)
						{
							result = new TimeSpan(long.MinValue);
						}
						else
						{
							result = new TimeSpan(0L - (long)num);
						}
					}
					else
					{
						result = new TimeSpan((long)num);
					}
					return null;
				}
				catch (OverflowException)
				{
					result = TimeSpan.MinValue;
					result2 = new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new object[]
					{
						durationType,
						"TimeSpan"
					}));
				}
				return result2;
			}
		}

		// Token: 0x06002D01 RID: 11521 RVA: 0x000F3050 File Offset: 0x000F1250
		public override string ToString()
		{
			return this.ToString(XsdDuration.DurationType.Duration);
		}

		// Token: 0x06002D02 RID: 11522 RVA: 0x000F305C File Offset: 0x000F125C
		internal string ToString(XsdDuration.DurationType durationType)
		{
			StringBuilder stringBuilder = new StringBuilder(20);
			if (this.IsNegative)
			{
				stringBuilder.Append('-');
			}
			stringBuilder.Append('P');
			if (durationType != XsdDuration.DurationType.DayTimeDuration)
			{
				if (this.years != 0)
				{
					stringBuilder.Append(XmlConvert.ToString(this.years));
					stringBuilder.Append('Y');
				}
				if (this.months != 0)
				{
					stringBuilder.Append(XmlConvert.ToString(this.months));
					stringBuilder.Append('M');
				}
			}
			if (durationType != XsdDuration.DurationType.YearMonthDuration)
			{
				if (this.days != 0)
				{
					stringBuilder.Append(XmlConvert.ToString(this.days));
					stringBuilder.Append('D');
				}
				if (this.hours != 0 || this.minutes != 0 || this.seconds != 0 || this.Nanoseconds != 0)
				{
					stringBuilder.Append('T');
					if (this.hours != 0)
					{
						stringBuilder.Append(XmlConvert.ToString(this.hours));
						stringBuilder.Append('H');
					}
					if (this.minutes != 0)
					{
						stringBuilder.Append(XmlConvert.ToString(this.minutes));
						stringBuilder.Append('M');
					}
					int num = this.Nanoseconds;
					if (this.seconds != 0 || num != 0)
					{
						stringBuilder.Append(XmlConvert.ToString(this.seconds));
						if (num != 0)
						{
							stringBuilder.Append('.');
							int length = stringBuilder.Length;
							stringBuilder.Length += 9;
							int num2 = stringBuilder.Length - 1;
							for (int i = num2; i >= length; i--)
							{
								int num3 = num % 10;
								stringBuilder[i] = (char)(num3 + 48);
								if (num2 == i && num3 == 0)
								{
									num2--;
								}
								num /= 10;
							}
							stringBuilder.Length = num2 + 1;
						}
						stringBuilder.Append('S');
					}
				}
				if (stringBuilder[stringBuilder.Length - 1] == 'P')
				{
					stringBuilder.Append("T0S");
				}
			}
			else if (stringBuilder[stringBuilder.Length - 1] == 'P')
			{
				stringBuilder.Append("0M");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06002D03 RID: 11523 RVA: 0x000F324E File Offset: 0x000F144E
		internal static Exception TryParse(string s, out XsdDuration result)
		{
			return XsdDuration.TryParse(s, XsdDuration.DurationType.Duration, out result);
		}

		// Token: 0x06002D04 RID: 11524 RVA: 0x000F3258 File Offset: 0x000F1458
		internal static Exception TryParse(string s, XsdDuration.DurationType durationType, out XsdDuration result)
		{
			XsdDuration.Parts parts = XsdDuration.Parts.HasNone;
			result = default(XsdDuration);
			s = s.Trim();
			int length = s.Length;
			int num = 0;
			int i = 0;
			if (num < length)
			{
				if (s[num] == '-')
				{
					num++;
					result.nanoseconds = 2147483648U;
				}
				else
				{
					result.nanoseconds = 0U;
				}
				if (num < length && s[num++] == 'P')
				{
					int num2;
					if (XsdDuration.TryParseDigits(s, ref num, false, out num2, out i) == null)
					{
						if (num >= length)
						{
							goto IL_2B5;
						}
						if (s[num] == 'Y')
						{
							if (i == 0)
							{
								goto IL_2B5;
							}
							parts |= XsdDuration.Parts.HasYears;
							result.years = num2;
							if (++num == length)
							{
								goto IL_298;
							}
							if (XsdDuration.TryParseDigits(s, ref num, false, out num2, out i) != null)
							{
								goto IL_2D8;
							}
							if (num >= length)
							{
								goto IL_2B5;
							}
						}
						if (s[num] == 'M')
						{
							if (i == 0)
							{
								goto IL_2B5;
							}
							parts |= XsdDuration.Parts.HasMonths;
							result.months = num2;
							if (++num == length)
							{
								goto IL_298;
							}
							if (XsdDuration.TryParseDigits(s, ref num, false, out num2, out i) != null)
							{
								goto IL_2D8;
							}
							if (num >= length)
							{
								goto IL_2B5;
							}
						}
						if (s[num] == 'D')
						{
							if (i == 0)
							{
								goto IL_2B5;
							}
							parts |= XsdDuration.Parts.HasDays;
							result.days = num2;
							if (++num == length)
							{
								goto IL_298;
							}
							if (XsdDuration.TryParseDigits(s, ref num, false, out num2, out i) != null)
							{
								goto IL_2D8;
							}
							if (num >= length)
							{
								goto IL_2B5;
							}
						}
						if (s[num] == 'T')
						{
							if (i != 0)
							{
								goto IL_2B5;
							}
							num++;
							if (XsdDuration.TryParseDigits(s, ref num, false, out num2, out i) != null)
							{
								goto IL_2D8;
							}
							if (num >= length)
							{
								goto IL_2B5;
							}
							if (s[num] == 'H')
							{
								if (i == 0)
								{
									goto IL_2B5;
								}
								parts |= XsdDuration.Parts.HasHours;
								result.hours = num2;
								if (++num == length)
								{
									goto IL_298;
								}
								if (XsdDuration.TryParseDigits(s, ref num, false, out num2, out i) != null)
								{
									goto IL_2D8;
								}
								if (num >= length)
								{
									goto IL_2B5;
								}
							}
							if (s[num] == 'M')
							{
								if (i == 0)
								{
									goto IL_2B5;
								}
								parts |= XsdDuration.Parts.HasMinutes;
								result.minutes = num2;
								if (++num == length)
								{
									goto IL_298;
								}
								if (XsdDuration.TryParseDigits(s, ref num, false, out num2, out i) != null)
								{
									goto IL_2D8;
								}
								if (num >= length)
								{
									goto IL_2B5;
								}
							}
							if (s[num] == '.')
							{
								num++;
								parts |= XsdDuration.Parts.HasSeconds;
								result.seconds = num2;
								if (XsdDuration.TryParseDigits(s, ref num, true, out num2, out i) != null)
								{
									goto IL_2D8;
								}
								if (i == 0)
								{
									num2 = 0;
								}
								while (i > 9)
								{
									num2 /= 10;
									i--;
								}
								while (i < 9)
								{
									num2 *= 10;
									i++;
								}
								result.nanoseconds |= (uint)num2;
								if (num >= length || s[num] != 'S')
								{
									goto IL_2B5;
								}
								if (++num == length)
								{
									goto IL_298;
								}
							}
							else if (s[num] == 'S')
							{
								if (i == 0)
								{
									goto IL_2B5;
								}
								parts |= XsdDuration.Parts.HasSeconds;
								result.seconds = num2;
								if (++num == length)
								{
									goto IL_298;
								}
							}
						}
						if (i != 0 || num != length)
						{
							goto IL_2B5;
						}
						IL_298:
						if (parts != XsdDuration.Parts.HasNone)
						{
							if (durationType == XsdDuration.DurationType.DayTimeDuration)
							{
								if ((parts & (XsdDuration.Parts)3) != XsdDuration.Parts.HasNone)
								{
									goto IL_2B5;
								}
							}
							else if (durationType == XsdDuration.DurationType.YearMonthDuration && (parts & (XsdDuration.Parts)(-4)) != XsdDuration.Parts.HasNone)
							{
								goto IL_2B5;
							}
							return null;
						}
						goto IL_2B5;
					}
					IL_2D8:
					return new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new object[]
					{
						s,
						durationType
					}));
				}
			}
			IL_2B5:
			return new FormatException(Res.GetString("The string '{0}' is not a valid {1} value.", new object[]
			{
				s,
				durationType
			}));
		}

		// Token: 0x06002D05 RID: 11525 RVA: 0x000F3560 File Offset: 0x000F1760
		private static string TryParseDigits(string s, ref int offset, bool eatDigits, out int result, out int numDigits)
		{
			int num = offset;
			int length = s.Length;
			result = 0;
			numDigits = 0;
			while (offset < length && s[offset] >= '0' && s[offset] <= '9')
			{
				int num2 = (int)(s[offset] - '0');
				if (result > (2147483647 - num2) / 10)
				{
					if (!eatDigits)
					{
						return "Value '{0}' was either too large or too small for {1}.";
					}
					numDigits = offset - num;
					while (offset < length && s[offset] >= '0' && s[offset] <= '9')
					{
						offset++;
					}
					return null;
				}
				else
				{
					result = result * 10 + num2;
					offset++;
				}
			}
			numDigits = offset - num;
			return null;
		}

		// Token: 0x04001EAB RID: 7851
		private int years;

		// Token: 0x04001EAC RID: 7852
		private int months;

		// Token: 0x04001EAD RID: 7853
		private int days;

		// Token: 0x04001EAE RID: 7854
		private int hours;

		// Token: 0x04001EAF RID: 7855
		private int minutes;

		// Token: 0x04001EB0 RID: 7856
		private int seconds;

		// Token: 0x04001EB1 RID: 7857
		private uint nanoseconds;

		// Token: 0x04001EB2 RID: 7858
		private const uint NegativeBit = 2147483648U;

		// Token: 0x0200047C RID: 1148
		private enum Parts
		{
			// Token: 0x04001EB4 RID: 7860
			HasNone,
			// Token: 0x04001EB5 RID: 7861
			HasYears,
			// Token: 0x04001EB6 RID: 7862
			HasMonths,
			// Token: 0x04001EB7 RID: 7863
			HasDays = 4,
			// Token: 0x04001EB8 RID: 7864
			HasHours = 8,
			// Token: 0x04001EB9 RID: 7865
			HasMinutes = 16,
			// Token: 0x04001EBA RID: 7866
			HasSeconds = 32
		}

		// Token: 0x0200047D RID: 1149
		public enum DurationType
		{
			// Token: 0x04001EBC RID: 7868
			Duration,
			// Token: 0x04001EBD RID: 7869
			YearMonthDuration,
			// Token: 0x04001EBE RID: 7870
			DayTimeDuration
		}
	}
}
