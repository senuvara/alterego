﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000367 RID: 871
	internal struct Position
	{
		// Token: 0x0600216A RID: 8554 RVA: 0x000B9C40 File Offset: 0x000B7E40
		public Position(int symbol, object particle)
		{
			this.symbol = symbol;
			this.particle = particle;
		}

		// Token: 0x040017E9 RID: 6121
		public int symbol;

		// Token: 0x040017EA RID: 6122
		public object particle;
	}
}
