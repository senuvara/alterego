﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000373 RID: 883
	internal sealed class LeafRangeNode : LeafNode
	{
		// Token: 0x0600219D RID: 8605 RVA: 0x000BA26A File Offset: 0x000B846A
		public LeafRangeNode(decimal min, decimal max) : this(-1, min, max)
		{
		}

		// Token: 0x0600219E RID: 8606 RVA: 0x000BA275 File Offset: 0x000B8475
		public LeafRangeNode(int pos, decimal min, decimal max) : base(pos)
		{
			this.min = min;
			this.max = max;
		}

		// Token: 0x1700069C RID: 1692
		// (get) Token: 0x0600219F RID: 8607 RVA: 0x000BA28C File Offset: 0x000B848C
		public decimal Max
		{
			get
			{
				return this.max;
			}
		}

		// Token: 0x1700069D RID: 1693
		// (get) Token: 0x060021A0 RID: 8608 RVA: 0x000BA294 File Offset: 0x000B8494
		public decimal Min
		{
			get
			{
				return this.min;
			}
		}

		// Token: 0x1700069E RID: 1694
		// (get) Token: 0x060021A1 RID: 8609 RVA: 0x000BA29C File Offset: 0x000B849C
		// (set) Token: 0x060021A2 RID: 8610 RVA: 0x000BA2A4 File Offset: 0x000B84A4
		public BitSet NextIteration
		{
			get
			{
				return this.nextIteration;
			}
			set
			{
				this.nextIteration = value;
			}
		}

		// Token: 0x060021A3 RID: 8611 RVA: 0x000BA2AD File Offset: 0x000B84AD
		public override SyntaxTreeNode Clone(Positions positions)
		{
			return new LeafRangeNode(base.Pos, this.min, this.max);
		}

		// Token: 0x1700069F RID: 1695
		// (get) Token: 0x060021A4 RID: 8612 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool IsRangeNode
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060021A5 RID: 8613 RVA: 0x000BA2C6 File Offset: 0x000B84C6
		public override void ExpandTree(InteriorNode parent, SymbolsDictionary symbols, Positions positions)
		{
			if (parent.LeftChild.IsNullable)
			{
				this.min = 0m;
			}
		}

		// Token: 0x040017F6 RID: 6134
		private decimal min;

		// Token: 0x040017F7 RID: 6135
		private decimal max;

		// Token: 0x040017F8 RID: 6136
		private BitSet nextIteration;
	}
}
