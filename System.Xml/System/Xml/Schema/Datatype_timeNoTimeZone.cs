﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000393 RID: 915
	internal class Datatype_timeNoTimeZone : Datatype_dateTimeBase
	{
		// Token: 0x060022A8 RID: 8872 RVA: 0x000BDF13 File Offset: 0x000BC113
		internal Datatype_timeNoTimeZone() : base(XsdDateTimeFlags.XdrTimeNoTz)
		{
		}
	}
}
