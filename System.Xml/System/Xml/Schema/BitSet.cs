﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200035A RID: 858
	internal sealed class BitSet
	{
		// Token: 0x06002117 RID: 8471 RVA: 0x00002103 File Offset: 0x00000303
		private BitSet()
		{
		}

		// Token: 0x06002118 RID: 8472 RVA: 0x000B88E8 File Offset: 0x000B6AE8
		public BitSet(int count)
		{
			this.count = count;
			this.bits = new uint[this.Subscript(count + 31)];
		}

		// Token: 0x17000673 RID: 1651
		// (get) Token: 0x06002119 RID: 8473 RVA: 0x000B890C File Offset: 0x000B6B0C
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000674 RID: 1652
		public bool this[int index]
		{
			get
			{
				return this.Get(index);
			}
		}

		// Token: 0x0600211B RID: 8475 RVA: 0x000B8920 File Offset: 0x000B6B20
		public void Clear()
		{
			int num = this.bits.Length;
			while (num-- > 0)
			{
				this.bits[num] = 0U;
			}
		}

		// Token: 0x0600211C RID: 8476 RVA: 0x000B894C File Offset: 0x000B6B4C
		public void Clear(int index)
		{
			int num = this.Subscript(index);
			this.EnsureLength(num + 1);
			this.bits[num] &= ~(1U << index);
		}

		// Token: 0x0600211D RID: 8477 RVA: 0x000B8984 File Offset: 0x000B6B84
		public void Set(int index)
		{
			int num = this.Subscript(index);
			this.EnsureLength(num + 1);
			this.bits[num] |= 1U << index;
		}

		// Token: 0x0600211E RID: 8478 RVA: 0x000B89BC File Offset: 0x000B6BBC
		public bool Get(int index)
		{
			bool result = false;
			if (index < this.count)
			{
				int num = this.Subscript(index);
				result = (((ulong)this.bits[num] & (ulong)(1L << (index & 31 & 31))) > 0UL);
			}
			return result;
		}

		// Token: 0x0600211F RID: 8479 RVA: 0x000B89F8 File Offset: 0x000B6BF8
		public int NextSet(int startFrom)
		{
			int num = startFrom + 1;
			if (num == this.count)
			{
				return -1;
			}
			int num2 = this.Subscript(num);
			num &= 31;
			uint num3;
			for (num3 = this.bits[num2] >> num; num3 == 0U; num3 = this.bits[num2])
			{
				if (++num2 == this.bits.Length)
				{
					return -1;
				}
				num = 0;
			}
			while ((num3 & 1U) == 0U)
			{
				num3 >>= 1;
				num++;
			}
			return (num2 << 5) + num;
		}

		// Token: 0x06002120 RID: 8480 RVA: 0x000B8A64 File Offset: 0x000B6C64
		public void And(BitSet other)
		{
			if (this == other)
			{
				return;
			}
			int num = this.bits.Length;
			int num2 = other.bits.Length;
			int i = (num > num2) ? num2 : num;
			int num3 = i;
			while (num3-- > 0)
			{
				this.bits[num3] &= other.bits[num3];
			}
			while (i < num)
			{
				this.bits[i] = 0U;
				i++;
			}
		}

		// Token: 0x06002121 RID: 8481 RVA: 0x000B8AC8 File Offset: 0x000B6CC8
		public void Or(BitSet other)
		{
			if (this == other)
			{
				return;
			}
			int num = other.bits.Length;
			this.EnsureLength(num);
			int num2 = num;
			while (num2-- > 0)
			{
				this.bits[num2] |= other.bits[num2];
			}
		}

		// Token: 0x06002122 RID: 8482 RVA: 0x000B8B10 File Offset: 0x000B6D10
		public override int GetHashCode()
		{
			int num = 1234;
			int num2 = this.bits.Length;
			while (--num2 >= 0)
			{
				num ^= (int)(this.bits[num2] * (uint)(num2 + 1));
			}
			return num ^ num;
		}

		// Token: 0x06002123 RID: 8483 RVA: 0x000B8B48 File Offset: 0x000B6D48
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (this == obj)
			{
				return true;
			}
			BitSet bitSet = (BitSet)obj;
			int num = this.bits.Length;
			int num2 = bitSet.bits.Length;
			int num3 = (num > num2) ? num2 : num;
			int num4 = num3;
			while (num4-- > 0)
			{
				if (this.bits[num4] != bitSet.bits[num4])
				{
					return false;
				}
			}
			if (num > num3)
			{
				int num5 = num;
				while (num5-- > num3)
				{
					if (this.bits[num5] != 0U)
					{
						return false;
					}
				}
			}
			else
			{
				int num6 = num2;
				while (num6-- > num3)
				{
					if (bitSet.bits[num6] != 0U)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06002124 RID: 8484 RVA: 0x000B8BE9 File Offset: 0x000B6DE9
		public BitSet Clone()
		{
			return new BitSet
			{
				count = this.count,
				bits = (uint[])this.bits.Clone()
			};
		}

		// Token: 0x17000675 RID: 1653
		// (get) Token: 0x06002125 RID: 8485 RVA: 0x000B8C14 File Offset: 0x000B6E14
		public bool IsEmpty
		{
			get
			{
				uint num = 0U;
				for (int i = 0; i < this.bits.Length; i++)
				{
					num |= this.bits[i];
				}
				return num == 0U;
			}
		}

		// Token: 0x06002126 RID: 8486 RVA: 0x000B8C48 File Offset: 0x000B6E48
		public bool Intersects(BitSet other)
		{
			int num = Math.Min(this.bits.Length, other.bits.Length);
			while (--num >= 0)
			{
				if ((this.bits[num] & other.bits[num]) != 0U)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002127 RID: 8487 RVA: 0x000B8C8B File Offset: 0x000B6E8B
		private int Subscript(int bitIndex)
		{
			return bitIndex >> 5;
		}

		// Token: 0x06002128 RID: 8488 RVA: 0x000B8C90 File Offset: 0x000B6E90
		private void EnsureLength(int nRequiredLength)
		{
			if (nRequiredLength > this.bits.Length)
			{
				int num = 2 * this.bits.Length;
				if (num < nRequiredLength)
				{
					num = nRequiredLength;
				}
				uint[] destinationArray = new uint[num];
				Array.Copy(this.bits, destinationArray, this.bits.Length);
				this.bits = destinationArray;
			}
		}

		// Token: 0x040017B3 RID: 6067
		private const int bitSlotShift = 5;

		// Token: 0x040017B4 RID: 6068
		private const int bitSlotMask = 31;

		// Token: 0x040017B5 RID: 6069
		private int count;

		// Token: 0x040017B6 RID: 6070
		private uint[] bits;
	}
}
