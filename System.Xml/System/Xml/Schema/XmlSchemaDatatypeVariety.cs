﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Specifies the W3C XML schema data type variety of the type.</summary>
	// Token: 0x0200037B RID: 891
	public enum XmlSchemaDatatypeVariety
	{
		/// <summary>A W3C XML schema atomic type.</summary>
		// Token: 0x0400181C RID: 6172
		Atomic,
		/// <summary>A W3C XML schema list type.</summary>
		// Token: 0x0400181D RID: 6173
		List,
		/// <summary>A W3C XML schema union type.</summary>
		// Token: 0x0400181E RID: 6174
		Union
	}
}
