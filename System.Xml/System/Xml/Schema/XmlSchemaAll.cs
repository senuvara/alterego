﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the World Wide Web Consortium (W3C) <see langword="all" /> element (compositor).</summary>
	// Token: 0x02000403 RID: 1027
	public class XmlSchemaAll : XmlSchemaGroupBase
	{
		/// <summary>Gets the collection of <see langword="XmlSchemaElement" /> elements contained within the <see langword="all" /> compositor.</summary>
		/// <returns>The collection of elements contained in <see langword="XmlSchemaAll" />.</returns>
		// Token: 0x17000816 RID: 2070
		// (get) Token: 0x06002702 RID: 9986 RVA: 0x000DE114 File Offset: 0x000DC314
		[XmlElement("element", typeof(XmlSchemaElement))]
		public override XmlSchemaObjectCollection Items
		{
			get
			{
				return this.items;
			}
		}

		// Token: 0x17000817 RID: 2071
		// (get) Token: 0x06002703 RID: 9987 RVA: 0x000DE11C File Offset: 0x000DC31C
		internal override bool IsEmpty
		{
			get
			{
				return base.IsEmpty || this.items.Count == 0;
			}
		}

		// Token: 0x06002704 RID: 9988 RVA: 0x000DE136 File Offset: 0x000DC336
		internal override void SetItems(XmlSchemaObjectCollection newItems)
		{
			this.items = newItems;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAll" /> class.</summary>
		// Token: 0x06002705 RID: 9989 RVA: 0x000DE13F File Offset: 0x000DC33F
		public XmlSchemaAll()
		{
		}

		// Token: 0x04001BDF RID: 7135
		private XmlSchemaObjectCollection items = new XmlSchemaObjectCollection();
	}
}
