﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200035B RID: 859
	internal class ChameleonKey
	{
		// Token: 0x06002129 RID: 8489 RVA: 0x000B8CDB File Offset: 0x000B6EDB
		public ChameleonKey(string ns, XmlSchema originalSchema)
		{
			this.targetNS = ns;
			this.chameleonLocation = originalSchema.BaseUri;
			if (this.chameleonLocation.OriginalString.Length == 0)
			{
				this.originalSchema = originalSchema;
			}
		}

		// Token: 0x0600212A RID: 8490 RVA: 0x000B8D10 File Offset: 0x000B6F10
		public override int GetHashCode()
		{
			if (this.hashCode == 0)
			{
				this.hashCode = this.targetNS.GetHashCode() + this.chameleonLocation.GetHashCode() + ((this.originalSchema == null) ? 0 : this.originalSchema.GetHashCode());
			}
			return this.hashCode;
		}

		// Token: 0x0600212B RID: 8491 RVA: 0x000B8D60 File Offset: 0x000B6F60
		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			ChameleonKey chameleonKey = obj as ChameleonKey;
			return chameleonKey != null && (this.targetNS.Equals(chameleonKey.targetNS) && this.chameleonLocation.Equals(chameleonKey.chameleonLocation)) && this.originalSchema == chameleonKey.originalSchema;
		}

		// Token: 0x040017B7 RID: 6071
		internal string targetNS;

		// Token: 0x040017B8 RID: 6072
		internal Uri chameleonLocation;

		// Token: 0x040017B9 RID: 6073
		internal XmlSchema originalSchema;

		// Token: 0x040017BA RID: 6074
		private int hashCode;
	}
}
