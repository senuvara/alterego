﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x020003C8 RID: 968
	internal class DateTimeFacetsChecker : FacetsChecker
	{
		// Token: 0x060023E3 RID: 9187 RVA: 0x000C1BAC File Offset: 0x000BFDAC
		internal override Exception CheckValueFacets(object value, XmlSchemaDatatype datatype)
		{
			DateTime value2 = datatype.ValueConverter.ToDateTime(value);
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023E4 RID: 9188 RVA: 0x000C1BD0 File Offset: 0x000BFDD0
		internal override Exception CheckValueFacets(DateTime value, XmlSchemaDatatype datatype)
		{
			RestrictionFacets restriction = datatype.Restriction;
			RestrictionFlags restrictionFlags = (restriction != null) ? restriction.Flags : ((RestrictionFlags)0);
			if ((restrictionFlags & RestrictionFlags.MaxInclusive) != (RestrictionFlags)0 && datatype.Compare(value, (DateTime)restriction.MaxInclusive) > 0)
			{
				return new XmlSchemaException("The MaxInclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MaxExclusive) != (RestrictionFlags)0 && datatype.Compare(value, (DateTime)restriction.MaxExclusive) >= 0)
			{
				return new XmlSchemaException("The MaxExclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MinInclusive) != (RestrictionFlags)0 && datatype.Compare(value, (DateTime)restriction.MinInclusive) < 0)
			{
				return new XmlSchemaException("The MinInclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MinExclusive) != (RestrictionFlags)0 && datatype.Compare(value, (DateTime)restriction.MinExclusive) <= 0)
			{
				return new XmlSchemaException("The MinExclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.Enumeration) != (RestrictionFlags)0 && !this.MatchEnumeration(value, restriction.Enumeration, datatype))
			{
				return new XmlSchemaException("The Enumeration constraint failed.", string.Empty);
			}
			return null;
		}

		// Token: 0x060023E5 RID: 9189 RVA: 0x000C1CF5 File Offset: 0x000BFEF5
		internal override bool MatchEnumeration(object value, ArrayList enumeration, XmlSchemaDatatype datatype)
		{
			return this.MatchEnumeration(datatype.ValueConverter.ToDateTime(value), enumeration, datatype);
		}

		// Token: 0x060023E6 RID: 9190 RVA: 0x000C1D0C File Offset: 0x000BFF0C
		private bool MatchEnumeration(DateTime value, ArrayList enumeration, XmlSchemaDatatype datatype)
		{
			for (int i = 0; i < enumeration.Count; i++)
			{
				if (datatype.Compare(value, (DateTime)enumeration[i]) == 0)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060023E7 RID: 9191 RVA: 0x000C1A2D File Offset: 0x000BFC2D
		public DateTimeFacetsChecker()
		{
		}
	}
}
