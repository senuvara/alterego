﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000476 RID: 1142
	[Flags]
	internal enum XsdDateTimeFlags
	{
		// Token: 0x04001E5E RID: 7774
		DateTime = 1,
		// Token: 0x04001E5F RID: 7775
		Time = 2,
		// Token: 0x04001E60 RID: 7776
		Date = 4,
		// Token: 0x04001E61 RID: 7777
		GYearMonth = 8,
		// Token: 0x04001E62 RID: 7778
		GYear = 16,
		// Token: 0x04001E63 RID: 7779
		GMonthDay = 32,
		// Token: 0x04001E64 RID: 7780
		GDay = 64,
		// Token: 0x04001E65 RID: 7781
		GMonth = 128,
		// Token: 0x04001E66 RID: 7782
		XdrDateTimeNoTz = 256,
		// Token: 0x04001E67 RID: 7783
		XdrDateTime = 512,
		// Token: 0x04001E68 RID: 7784
		XdrTimeNoTz = 1024,
		// Token: 0x04001E69 RID: 7785
		AllXsd = 255
	}
}
