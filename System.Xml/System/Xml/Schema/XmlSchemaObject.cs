﻿using System;
using System.Security.Permissions;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the root class for the Xml schema object model hierarchy and serves as a base class for classes such as the <see cref="T:System.Xml.Schema.XmlSchema" /> class.</summary>
	// Token: 0x0200043B RID: 1083
	[PermissionSet(SecurityAction.InheritanceDemand, Name = "FullTrust")]
	public abstract class XmlSchemaObject
	{
		/// <summary>Gets or sets the line number in the file to which the <see langword="schema" /> element refers.</summary>
		/// <returns>The line number.</returns>
		// Token: 0x170008D5 RID: 2261
		// (get) Token: 0x060028DC RID: 10460 RVA: 0x000E0AC2 File Offset: 0x000DECC2
		// (set) Token: 0x060028DD RID: 10461 RVA: 0x000E0ACA File Offset: 0x000DECCA
		[XmlIgnore]
		public int LineNumber
		{
			get
			{
				return this.lineNum;
			}
			set
			{
				this.lineNum = value;
			}
		}

		/// <summary>Gets or sets the line position in the file to which the <see langword="schema" /> element refers.</summary>
		/// <returns>The line position.</returns>
		// Token: 0x170008D6 RID: 2262
		// (get) Token: 0x060028DE RID: 10462 RVA: 0x000E0AD3 File Offset: 0x000DECD3
		// (set) Token: 0x060028DF RID: 10463 RVA: 0x000E0ADB File Offset: 0x000DECDB
		[XmlIgnore]
		public int LinePosition
		{
			get
			{
				return this.linePos;
			}
			set
			{
				this.linePos = value;
			}
		}

		/// <summary>Gets or sets the source location for the file that loaded the schema.</summary>
		/// <returns>The source location (URI) for the file.</returns>
		// Token: 0x170008D7 RID: 2263
		// (get) Token: 0x060028E0 RID: 10464 RVA: 0x000E0AE4 File Offset: 0x000DECE4
		// (set) Token: 0x060028E1 RID: 10465 RVA: 0x000E0AEC File Offset: 0x000DECEC
		[XmlIgnore]
		public string SourceUri
		{
			get
			{
				return this.sourceUri;
			}
			set
			{
				this.sourceUri = value;
			}
		}

		/// <summary>Gets or sets the parent of this <see cref="T:System.Xml.Schema.XmlSchemaObject" />.</summary>
		/// <returns>The parent <see cref="T:System.Xml.Schema.XmlSchemaObject" /> of this <see cref="T:System.Xml.Schema.XmlSchemaObject" />.</returns>
		// Token: 0x170008D8 RID: 2264
		// (get) Token: 0x060028E2 RID: 10466 RVA: 0x000E0AF5 File Offset: 0x000DECF5
		// (set) Token: 0x060028E3 RID: 10467 RVA: 0x000E0AFD File Offset: 0x000DECFD
		[XmlIgnore]
		public XmlSchemaObject Parent
		{
			get
			{
				return this.parent;
			}
			set
			{
				this.parent = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> to use with this schema object.</summary>
		/// <returns>The <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> property for the schema object.</returns>
		// Token: 0x170008D9 RID: 2265
		// (get) Token: 0x060028E4 RID: 10468 RVA: 0x000E0B06 File Offset: 0x000DED06
		// (set) Token: 0x060028E5 RID: 10469 RVA: 0x000E0B21 File Offset: 0x000DED21
		[XmlNamespaceDeclarations]
		public XmlSerializerNamespaces Namespaces
		{
			get
			{
				if (this.namespaces == null)
				{
					this.namespaces = new XmlSerializerNamespaces();
				}
				return this.namespaces;
			}
			set
			{
				this.namespaces = value;
			}
		}

		// Token: 0x060028E6 RID: 10470 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void OnAdd(XmlSchemaObjectCollection container, object item)
		{
		}

		// Token: 0x060028E7 RID: 10471 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void OnRemove(XmlSchemaObjectCollection container, object item)
		{
		}

		// Token: 0x060028E8 RID: 10472 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void OnClear(XmlSchemaObjectCollection container)
		{
		}

		// Token: 0x170008DA RID: 2266
		// (get) Token: 0x060028E9 RID: 10473 RVA: 0x000037FB File Offset: 0x000019FB
		// (set) Token: 0x060028EA RID: 10474 RVA: 0x000030EC File Offset: 0x000012EC
		[XmlIgnore]
		internal virtual string IdAttribute
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x060028EB RID: 10475 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void SetUnhandledAttributes(XmlAttribute[] moreAttributes)
		{
		}

		// Token: 0x060028EC RID: 10476 RVA: 0x000030EC File Offset: 0x000012EC
		internal virtual void AddAnnotation(XmlSchemaAnnotation annotation)
		{
		}

		// Token: 0x170008DB RID: 2267
		// (get) Token: 0x060028ED RID: 10477 RVA: 0x000037FB File Offset: 0x000019FB
		// (set) Token: 0x060028EE RID: 10478 RVA: 0x000030EC File Offset: 0x000012EC
		[XmlIgnore]
		internal virtual string NameAttribute
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x170008DC RID: 2268
		// (get) Token: 0x060028EF RID: 10479 RVA: 0x000E0B2A File Offset: 0x000DED2A
		// (set) Token: 0x060028F0 RID: 10480 RVA: 0x000E0B32 File Offset: 0x000DED32
		[XmlIgnore]
		internal bool IsProcessing
		{
			get
			{
				return this.isProcessing;
			}
			set
			{
				this.isProcessing = value;
			}
		}

		// Token: 0x060028F1 RID: 10481 RVA: 0x000E0B3B File Offset: 0x000DED3B
		internal virtual XmlSchemaObject Clone()
		{
			return (XmlSchemaObject)base.MemberwiseClone();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaObject" /> class.</summary>
		// Token: 0x060028F2 RID: 10482 RVA: 0x00002103 File Offset: 0x00000303
		protected XmlSchemaObject()
		{
		}

		// Token: 0x04001C99 RID: 7321
		private int lineNum;

		// Token: 0x04001C9A RID: 7322
		private int linePos;

		// Token: 0x04001C9B RID: 7323
		private string sourceUri;

		// Token: 0x04001C9C RID: 7324
		private XmlSerializerNamespaces namespaces;

		// Token: 0x04001C9D RID: 7325
		private XmlSchemaObject parent;

		// Token: 0x04001C9E RID: 7326
		private bool isProcessing;
	}
}
