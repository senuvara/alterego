﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="pattern" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to specify a restriction on the value entered for a <see langword="simpleType" /> element.</summary>
	// Token: 0x02000425 RID: 1061
	public class XmlSchemaPatternFacet : XmlSchemaFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaPatternFacet" /> class.</summary>
		// Token: 0x06002879 RID: 10361 RVA: 0x000E060C File Offset: 0x000DE80C
		public XmlSchemaPatternFacet()
		{
			base.FacetType = FacetType.Pattern;
		}
	}
}
