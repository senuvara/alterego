﻿using System;
using System.Collections;
using System.Text;

namespace System.Xml.Schema
{
	// Token: 0x02000359 RID: 857
	internal class BaseValidator
	{
		// Token: 0x060020F5 RID: 8437 RVA: 0x000B82F0 File Offset: 0x000B64F0
		public BaseValidator(BaseValidator other)
		{
			this.reader = other.reader;
			this.schemaCollection = other.schemaCollection;
			this.eventHandling = other.eventHandling;
			this.nameTable = other.nameTable;
			this.schemaNames = other.schemaNames;
			this.positionInfo = other.positionInfo;
			this.xmlResolver = other.xmlResolver;
			this.baseUri = other.baseUri;
			this.elementName = other.elementName;
		}

		// Token: 0x060020F6 RID: 8438 RVA: 0x000B836F File Offset: 0x000B656F
		public BaseValidator(XmlValidatingReaderImpl reader, XmlSchemaCollection schemaCollection, IValidationEventHandling eventHandling)
		{
			this.reader = reader;
			this.schemaCollection = schemaCollection;
			this.eventHandling = eventHandling;
			this.nameTable = reader.NameTable;
			this.positionInfo = PositionInfo.GetPositionInfo(reader);
			this.elementName = new XmlQualifiedName();
		}

		// Token: 0x17000668 RID: 1640
		// (get) Token: 0x060020F7 RID: 8439 RVA: 0x000B83AF File Offset: 0x000B65AF
		public XmlValidatingReaderImpl Reader
		{
			get
			{
				return this.reader;
			}
		}

		// Token: 0x17000669 RID: 1641
		// (get) Token: 0x060020F8 RID: 8440 RVA: 0x000B83B7 File Offset: 0x000B65B7
		public XmlSchemaCollection SchemaCollection
		{
			get
			{
				return this.schemaCollection;
			}
		}

		// Token: 0x1700066A RID: 1642
		// (get) Token: 0x060020F9 RID: 8441 RVA: 0x000B83BF File Offset: 0x000B65BF
		public XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		// Token: 0x1700066B RID: 1643
		// (get) Token: 0x060020FA RID: 8442 RVA: 0x000B83C8 File Offset: 0x000B65C8
		public SchemaNames SchemaNames
		{
			get
			{
				if (this.schemaNames != null)
				{
					return this.schemaNames;
				}
				if (this.schemaCollection != null)
				{
					this.schemaNames = this.schemaCollection.GetSchemaNames(this.nameTable);
				}
				else
				{
					this.schemaNames = new SchemaNames(this.nameTable);
				}
				return this.schemaNames;
			}
		}

		// Token: 0x1700066C RID: 1644
		// (get) Token: 0x060020FB RID: 8443 RVA: 0x000B841C File Offset: 0x000B661C
		public PositionInfo PositionInfo
		{
			get
			{
				return this.positionInfo;
			}
		}

		// Token: 0x1700066D RID: 1645
		// (get) Token: 0x060020FC RID: 8444 RVA: 0x000B8424 File Offset: 0x000B6624
		// (set) Token: 0x060020FD RID: 8445 RVA: 0x000B842C File Offset: 0x000B662C
		public XmlResolver XmlResolver
		{
			get
			{
				return this.xmlResolver;
			}
			set
			{
				this.xmlResolver = value;
			}
		}

		// Token: 0x1700066E RID: 1646
		// (get) Token: 0x060020FE RID: 8446 RVA: 0x000B8435 File Offset: 0x000B6635
		// (set) Token: 0x060020FF RID: 8447 RVA: 0x000B843D File Offset: 0x000B663D
		public Uri BaseUri
		{
			get
			{
				return this.baseUri;
			}
			set
			{
				this.baseUri = value;
			}
		}

		// Token: 0x1700066F RID: 1647
		// (get) Token: 0x06002100 RID: 8448 RVA: 0x000B8446 File Offset: 0x000B6646
		public ValidationEventHandler EventHandler
		{
			get
			{
				return (ValidationEventHandler)this.eventHandling.EventHandler;
			}
		}

		// Token: 0x17000670 RID: 1648
		// (get) Token: 0x06002101 RID: 8449 RVA: 0x000B8458 File Offset: 0x000B6658
		// (set) Token: 0x06002102 RID: 8450 RVA: 0x000B8460 File Offset: 0x000B6660
		public SchemaInfo SchemaInfo
		{
			get
			{
				return this.schemaInfo;
			}
			set
			{
				this.schemaInfo = value;
			}
		}

		// Token: 0x17000671 RID: 1649
		// (get) Token: 0x06002103 RID: 8451 RVA: 0x000B8458 File Offset: 0x000B6658
		// (set) Token: 0x06002104 RID: 8452 RVA: 0x000B846C File Offset: 0x000B666C
		public IDtdInfo DtdInfo
		{
			get
			{
				return this.schemaInfo;
			}
			set
			{
				SchemaInfo schemaInfo = value as SchemaInfo;
				if (schemaInfo == null)
				{
					throw new XmlException("An internal error has occurred.", string.Empty);
				}
				this.schemaInfo = schemaInfo;
			}
		}

		// Token: 0x17000672 RID: 1650
		// (get) Token: 0x06002105 RID: 8453 RVA: 0x000020CD File Offset: 0x000002CD
		public virtual bool PreserveWhitespace
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06002106 RID: 8454 RVA: 0x000030EC File Offset: 0x000012EC
		public virtual void Validate()
		{
		}

		// Token: 0x06002107 RID: 8455 RVA: 0x000030EC File Offset: 0x000012EC
		public virtual void CompleteValidation()
		{
		}

		// Token: 0x06002108 RID: 8456 RVA: 0x000037FB File Offset: 0x000019FB
		public virtual object FindId(string name)
		{
			return null;
		}

		// Token: 0x06002109 RID: 8457 RVA: 0x000B849C File Offset: 0x000B669C
		public void ValidateText()
		{
			if (this.context.NeedValidateChildren)
			{
				if (this.context.IsNill)
				{
					this.SendValidationEvent("Element '{0}' must have no character or element children.", XmlSchemaValidator.QNameString(this.context.LocalName, this.context.Namespace));
					return;
				}
				ContentValidator contentValidator = this.context.ElementDecl.ContentValidator;
				XmlSchemaContentType contentType = contentValidator.ContentType;
				if (contentType == XmlSchemaContentType.ElementOnly)
				{
					ArrayList arrayList = contentValidator.ExpectedElements(this.context, false);
					if (arrayList == null)
					{
						this.SendValidationEvent("The element {0} cannot contain text.", XmlSchemaValidator.BuildElementName(this.context.LocalName, this.context.Namespace));
					}
					else
					{
						this.SendValidationEvent("The element {0} cannot contain text. List of possible elements expected: {1}.", new string[]
						{
							XmlSchemaValidator.BuildElementName(this.context.LocalName, this.context.Namespace),
							XmlSchemaValidator.PrintExpectedElements(arrayList, false)
						});
					}
				}
				else if (contentType == XmlSchemaContentType.Empty)
				{
					this.SendValidationEvent("The element cannot contain text. Content model is empty.", string.Empty);
				}
				if (this.checkDatatype)
				{
					this.SaveTextValue(this.reader.Value);
				}
			}
		}

		// Token: 0x0600210A RID: 8458 RVA: 0x000B85AC File Offset: 0x000B67AC
		public void ValidateWhitespace()
		{
			if (this.context.NeedValidateChildren)
			{
				int contentType = (int)this.context.ElementDecl.ContentValidator.ContentType;
				if (this.context.IsNill)
				{
					this.SendValidationEvent("Element '{0}' must have no character or element children.", XmlSchemaValidator.QNameString(this.context.LocalName, this.context.Namespace));
				}
				if (contentType == 1)
				{
					this.SendValidationEvent("The element cannot contain white space. Content model is empty.", string.Empty);
				}
				if (this.checkDatatype)
				{
					this.SaveTextValue(this.reader.Value);
				}
			}
		}

		// Token: 0x0600210B RID: 8459 RVA: 0x000B863C File Offset: 0x000B683C
		private void SaveTextValue(string value)
		{
			if (this.textString.Length == 0)
			{
				this.textString = value;
				return;
			}
			if (!this.hasSibling)
			{
				this.textValue.Append(this.textString);
				this.hasSibling = true;
			}
			this.textValue.Append(value);
		}

		// Token: 0x0600210C RID: 8460 RVA: 0x000B868C File Offset: 0x000B688C
		protected void SendValidationEvent(string code)
		{
			this.SendValidationEvent(code, string.Empty);
		}

		// Token: 0x0600210D RID: 8461 RVA: 0x000B869A File Offset: 0x000B689A
		protected void SendValidationEvent(string code, string[] args)
		{
			this.SendValidationEvent(new XmlSchemaException(code, args, this.reader.BaseURI, this.positionInfo.LineNumber, this.positionInfo.LinePosition));
		}

		// Token: 0x0600210E RID: 8462 RVA: 0x000B86CA File Offset: 0x000B68CA
		protected void SendValidationEvent(string code, string arg)
		{
			this.SendValidationEvent(new XmlSchemaException(code, arg, this.reader.BaseURI, this.positionInfo.LineNumber, this.positionInfo.LinePosition));
		}

		// Token: 0x0600210F RID: 8463 RVA: 0x000B86FA File Offset: 0x000B68FA
		protected void SendValidationEvent(string code, string arg1, string arg2)
		{
			this.SendValidationEvent(new XmlSchemaException(code, new string[]
			{
				arg1,
				arg2
			}, this.reader.BaseURI, this.positionInfo.LineNumber, this.positionInfo.LinePosition));
		}

		// Token: 0x06002110 RID: 8464 RVA: 0x000B8737 File Offset: 0x000B6937
		protected void SendValidationEvent(XmlSchemaException e)
		{
			this.SendValidationEvent(e, XmlSeverityType.Error);
		}

		// Token: 0x06002111 RID: 8465 RVA: 0x000B8741 File Offset: 0x000B6941
		protected void SendValidationEvent(string code, string msg, XmlSeverityType severity)
		{
			this.SendValidationEvent(new XmlSchemaException(code, msg, this.reader.BaseURI, this.positionInfo.LineNumber, this.positionInfo.LinePosition), severity);
		}

		// Token: 0x06002112 RID: 8466 RVA: 0x000B8772 File Offset: 0x000B6972
		protected void SendValidationEvent(string code, string[] args, XmlSeverityType severity)
		{
			this.SendValidationEvent(new XmlSchemaException(code, args, this.reader.BaseURI, this.positionInfo.LineNumber, this.positionInfo.LinePosition), severity);
		}

		// Token: 0x06002113 RID: 8467 RVA: 0x000B87A3 File Offset: 0x000B69A3
		protected void SendValidationEvent(XmlSchemaException e, XmlSeverityType severity)
		{
			if (this.eventHandling != null)
			{
				this.eventHandling.SendEvent(e, severity);
				return;
			}
			if (severity == XmlSeverityType.Error)
			{
				throw e;
			}
		}

		// Token: 0x06002114 RID: 8468 RVA: 0x000B87C0 File Offset: 0x000B69C0
		protected static void ProcessEntity(SchemaInfo sinfo, string name, object sender, ValidationEventHandler eventhandler, string baseUri, int lineNumber, int linePosition)
		{
			XmlSchemaException ex = null;
			SchemaEntity schemaEntity;
			if (!sinfo.GeneralEntities.TryGetValue(new XmlQualifiedName(name), out schemaEntity))
			{
				ex = new XmlSchemaException("Reference to an undeclared entity, '{0}'.", name, baseUri, lineNumber, linePosition);
			}
			else if (schemaEntity.NData.IsEmpty)
			{
				ex = new XmlSchemaException("Reference to an unparsed entity, '{0}'.", name, baseUri, lineNumber, linePosition);
			}
			if (ex == null)
			{
				return;
			}
			if (eventhandler != null)
			{
				eventhandler(sender, new ValidationEventArgs(ex));
				return;
			}
			throw ex;
		}

		// Token: 0x06002115 RID: 8469 RVA: 0x000B8830 File Offset: 0x000B6A30
		protected static void ProcessEntity(SchemaInfo sinfo, string name, IValidationEventHandling eventHandling, string baseUriStr, int lineNumber, int linePosition)
		{
			string text = null;
			SchemaEntity schemaEntity;
			if (!sinfo.GeneralEntities.TryGetValue(new XmlQualifiedName(name), out schemaEntity))
			{
				text = "Reference to an undeclared entity, '{0}'.";
			}
			else if (schemaEntity.NData.IsEmpty)
			{
				text = "Reference to an unparsed entity, '{0}'.";
			}
			if (text == null)
			{
				return;
			}
			XmlSchemaException ex = new XmlSchemaException(text, name, baseUriStr, lineNumber, linePosition);
			if (eventHandling != null)
			{
				eventHandling.SendEvent(ex, XmlSeverityType.Error);
				return;
			}
			throw ex;
		}

		// Token: 0x06002116 RID: 8470 RVA: 0x000B8890 File Offset: 0x000B6A90
		public static BaseValidator CreateInstance(ValidationType valType, XmlValidatingReaderImpl reader, XmlSchemaCollection schemaCollection, IValidationEventHandling eventHandling, bool processIdentityConstraints)
		{
			switch (valType)
			{
			case ValidationType.None:
				return new BaseValidator(reader, schemaCollection, eventHandling);
			case ValidationType.Auto:
				return new AutoValidator(reader, schemaCollection, eventHandling);
			case ValidationType.DTD:
				return new DtdValidator(reader, eventHandling, processIdentityConstraints);
			case ValidationType.XDR:
				return new XdrValidator(reader, schemaCollection, eventHandling);
			case ValidationType.Schema:
				return new XsdValidator(reader, schemaCollection, eventHandling);
			default:
				return null;
			}
		}

		// Token: 0x040017A4 RID: 6052
		private XmlSchemaCollection schemaCollection;

		// Token: 0x040017A5 RID: 6053
		private IValidationEventHandling eventHandling;

		// Token: 0x040017A6 RID: 6054
		private XmlNameTable nameTable;

		// Token: 0x040017A7 RID: 6055
		private SchemaNames schemaNames;

		// Token: 0x040017A8 RID: 6056
		private PositionInfo positionInfo;

		// Token: 0x040017A9 RID: 6057
		private XmlResolver xmlResolver;

		// Token: 0x040017AA RID: 6058
		private Uri baseUri;

		// Token: 0x040017AB RID: 6059
		protected SchemaInfo schemaInfo;

		// Token: 0x040017AC RID: 6060
		protected XmlValidatingReaderImpl reader;

		// Token: 0x040017AD RID: 6061
		protected XmlQualifiedName elementName;

		// Token: 0x040017AE RID: 6062
		protected ValidationState context;

		// Token: 0x040017AF RID: 6063
		protected StringBuilder textValue;

		// Token: 0x040017B0 RID: 6064
		protected string textString;

		// Token: 0x040017B1 RID: 6065
		protected bool hasSibling;

		// Token: 0x040017B2 RID: 6066
		protected bool checkDatatype;
	}
}
