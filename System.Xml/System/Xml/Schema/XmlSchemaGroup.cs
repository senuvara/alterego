﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="group" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class defines groups at the <see langword="schema" /> level that are referenced from the complex types. It groups a set of element declarations so that they can be incorporated as a group into complex type definitions.</summary>
	// Token: 0x0200042F RID: 1071
	public class XmlSchemaGroup : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the name of the schema group.</summary>
		/// <returns>The name of the schema group.</returns>
		// Token: 0x170008AF RID: 2223
		// (get) Token: 0x06002882 RID: 10370 RVA: 0x000E0697 File Offset: 0x000DE897
		// (set) Token: 0x06002883 RID: 10371 RVA: 0x000E069F File Offset: 0x000DE89F
		[XmlAttribute("name")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets one of the <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, <see cref="T:System.Xml.Schema.XmlSchemaAll" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" /> classes.</summary>
		/// <returns>One of the <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, <see cref="T:System.Xml.Schema.XmlSchemaAll" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" /> classes.</returns>
		// Token: 0x170008B0 RID: 2224
		// (get) Token: 0x06002884 RID: 10372 RVA: 0x000E06A8 File Offset: 0x000DE8A8
		// (set) Token: 0x06002885 RID: 10373 RVA: 0x000E06B0 File Offset: 0x000DE8B0
		[XmlElement("sequence", typeof(XmlSchemaSequence))]
		[XmlElement("choice", typeof(XmlSchemaChoice))]
		[XmlElement("all", typeof(XmlSchemaAll))]
		public XmlSchemaGroupBase Particle
		{
			get
			{
				return this.particle;
			}
			set
			{
				this.particle = value;
			}
		}

		/// <summary>Gets the qualified name of the schema group.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlQualifiedName" /> object representing the qualified name of the schema group.</returns>
		// Token: 0x170008B1 RID: 2225
		// (get) Token: 0x06002886 RID: 10374 RVA: 0x000E06B9 File Offset: 0x000DE8B9
		[XmlIgnore]
		public XmlQualifiedName QualifiedName
		{
			get
			{
				return this.qname;
			}
		}

		// Token: 0x170008B2 RID: 2226
		// (get) Token: 0x06002887 RID: 10375 RVA: 0x000E06C1 File Offset: 0x000DE8C1
		// (set) Token: 0x06002888 RID: 10376 RVA: 0x000E06C9 File Offset: 0x000DE8C9
		[XmlIgnore]
		internal XmlSchemaParticle CanonicalParticle
		{
			get
			{
				return this.canonicalParticle;
			}
			set
			{
				this.canonicalParticle = value;
			}
		}

		// Token: 0x170008B3 RID: 2227
		// (get) Token: 0x06002889 RID: 10377 RVA: 0x000E06D2 File Offset: 0x000DE8D2
		// (set) Token: 0x0600288A RID: 10378 RVA: 0x000E06DA File Offset: 0x000DE8DA
		[XmlIgnore]
		internal XmlSchemaGroup Redefined
		{
			get
			{
				return this.redefined;
			}
			set
			{
				this.redefined = value;
			}
		}

		// Token: 0x170008B4 RID: 2228
		// (get) Token: 0x0600288B RID: 10379 RVA: 0x000E06E3 File Offset: 0x000DE8E3
		// (set) Token: 0x0600288C RID: 10380 RVA: 0x000E06EB File Offset: 0x000DE8EB
		[XmlIgnore]
		internal int SelfReferenceCount
		{
			get
			{
				return this.selfReferenceCount;
			}
			set
			{
				this.selfReferenceCount = value;
			}
		}

		// Token: 0x170008B5 RID: 2229
		// (get) Token: 0x0600288D RID: 10381 RVA: 0x000E06F4 File Offset: 0x000DE8F4
		// (set) Token: 0x0600288E RID: 10382 RVA: 0x000E06FC File Offset: 0x000DE8FC
		[XmlIgnore]
		internal override string NameAttribute
		{
			get
			{
				return this.Name;
			}
			set
			{
				this.Name = value;
			}
		}

		// Token: 0x0600288F RID: 10383 RVA: 0x000E0705 File Offset: 0x000DE905
		internal void SetQualifiedName(XmlQualifiedName value)
		{
			this.qname = value;
		}

		// Token: 0x06002890 RID: 10384 RVA: 0x000E070E File Offset: 0x000DE90E
		internal override XmlSchemaObject Clone()
		{
			return this.Clone(null);
		}

		// Token: 0x06002891 RID: 10385 RVA: 0x000E0718 File Offset: 0x000DE918
		internal XmlSchemaObject Clone(XmlSchema parentSchema)
		{
			XmlSchemaGroup xmlSchemaGroup = (XmlSchemaGroup)base.MemberwiseClone();
			if (XmlSchemaComplexType.HasParticleRef(this.particle, parentSchema))
			{
				xmlSchemaGroup.particle = (XmlSchemaComplexType.CloneParticle(this.particle, parentSchema) as XmlSchemaGroupBase);
			}
			xmlSchemaGroup.canonicalParticle = XmlSchemaParticle.Empty;
			return xmlSchemaGroup;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaGroup" /> class.</summary>
		// Token: 0x06002892 RID: 10386 RVA: 0x000E0762 File Offset: 0x000DE962
		public XmlSchemaGroup()
		{
		}

		// Token: 0x04001C7A RID: 7290
		private string name;

		// Token: 0x04001C7B RID: 7291
		private XmlSchemaGroupBase particle;

		// Token: 0x04001C7C RID: 7292
		private XmlSchemaParticle canonicalParticle;

		// Token: 0x04001C7D RID: 7293
		private XmlQualifiedName qname = XmlQualifiedName.Empty;

		// Token: 0x04001C7E RID: 7294
		private XmlSchemaGroup redefined;

		// Token: 0x04001C7F RID: 7295
		private int selfReferenceCount;
	}
}
