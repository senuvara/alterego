﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using System.Xml.XmlConfiguration;

namespace System.Xml.Schema
{
	/// <summary>An in-memory representation of an XML Schema, as specified in the World Wide Web Consortium (W3C) XML Schema Part 1: Structures and XML Schema Part 2: Datatypes specifications.</summary>
	// Token: 0x02000402 RID: 1026
	[XmlRoot("schema", Namespace = "http://www.w3.org/2001/XMLSchema")]
	public class XmlSchema : XmlSchemaObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchema" /> class.</summary>
		// Token: 0x060026BF RID: 9919 RVA: 0x000DD83C File Offset: 0x000DBA3C
		public XmlSchema()
		{
		}

		/// <summary>Reads an XML Schema from the supplied <see cref="T:System.IO.TextReader" />.</summary>
		/// <param name="reader">The <see langword="TextReader" /> containing the XML Schema to read. </param>
		/// <param name="validationEventHandler">The validation event handler that receives information about the XML Schema syntax errors. </param>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchema" /> object representing the XML Schema.</returns>
		/// <exception cref="T:System.Xml.Schema.XmlSchemaException">An <see cref="T:System.Xml.Schema.XmlSchemaException" /> is raised if no <see cref="T:System.Xml.Schema.ValidationEventHandler" /> is specified.</exception>
		// Token: 0x060026C0 RID: 9920 RVA: 0x000DD8CF File Offset: 0x000DBACF
		public static XmlSchema Read(TextReader reader, ValidationEventHandler validationEventHandler)
		{
			return XmlSchema.Read(new XmlTextReader(reader), validationEventHandler);
		}

		/// <summary>Reads an XML Schema  from the supplied stream.</summary>
		/// <param name="stream">The supplied data stream. </param>
		/// <param name="validationEventHandler">The validation event handler that receives information about XML Schema syntax errors. </param>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchema" /> object representing the XML Schema.</returns>
		/// <exception cref="T:System.Xml.Schema.XmlSchemaException">An <see cref="T:System.Xml.Schema.XmlSchemaException" /> is raised if no <see cref="T:System.Xml.Schema.ValidationEventHandler" /> is specified.</exception>
		// Token: 0x060026C1 RID: 9921 RVA: 0x000DD8DD File Offset: 0x000DBADD
		public static XmlSchema Read(Stream stream, ValidationEventHandler validationEventHandler)
		{
			return XmlSchema.Read(new XmlTextReader(stream), validationEventHandler);
		}

		/// <summary>Reads an XML Schema from the supplied <see cref="T:System.Xml.XmlReader" />.</summary>
		/// <param name="reader">The <see langword="XmlReader" /> containing the XML Schema to read. </param>
		/// <param name="validationEventHandler">The validation event handler that receives information about the XML Schema syntax errors. </param>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchema" /> object representing the XML Schema.</returns>
		/// <exception cref="T:System.Xml.Schema.XmlSchemaException">An <see cref="T:System.Xml.Schema.XmlSchemaException" /> is raised if no <see cref="T:System.Xml.Schema.ValidationEventHandler" /> is specified.</exception>
		// Token: 0x060026C2 RID: 9922 RVA: 0x000DD8EC File Offset: 0x000DBAEC
		public static XmlSchema Read(XmlReader reader, ValidationEventHandler validationEventHandler)
		{
			XmlNameTable xmlNameTable = reader.NameTable;
			Parser parser = new Parser(SchemaType.XSD, xmlNameTable, new SchemaNames(xmlNameTable), validationEventHandler);
			try
			{
				parser.Parse(reader, null);
			}
			catch (XmlSchemaException ex)
			{
				if (validationEventHandler != null)
				{
					validationEventHandler(null, new ValidationEventArgs(ex));
					return null;
				}
				throw ex;
			}
			return parser.XmlSchema;
		}

		/// <summary>Writes the XML Schema to the supplied data stream.</summary>
		/// <param name="stream">The supplied data stream. </param>
		// Token: 0x060026C3 RID: 9923 RVA: 0x000DD94C File Offset: 0x000DBB4C
		public void Write(Stream stream)
		{
			this.Write(stream, null);
		}

		/// <summary>Writes the XML Schema to the supplied <see cref="T:System.IO.Stream" /> using the <see cref="T:System.Xml.XmlNamespaceManager" /> specified.</summary>
		/// <param name="stream">The supplied data stream. </param>
		/// <param name="namespaceManager">The <see cref="T:System.Xml.XmlNamespaceManager" />.</param>
		// Token: 0x060026C4 RID: 9924 RVA: 0x000DD958 File Offset: 0x000DBB58
		public void Write(Stream stream, XmlNamespaceManager namespaceManager)
		{
			this.Write(new XmlTextWriter(stream, null)
			{
				Formatting = Formatting.Indented
			}, namespaceManager);
		}

		/// <summary>Writes the XML Schema to the supplied <see cref="T:System.IO.TextWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.IO.TextWriter" /> to write to.</param>
		// Token: 0x060026C5 RID: 9925 RVA: 0x000DD97C File Offset: 0x000DBB7C
		public void Write(TextWriter writer)
		{
			this.Write(writer, null);
		}

		/// <summary>Writes the XML Schema to the supplied <see cref="T:System.IO.TextWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.IO.TextWriter" /> to write to.</param>
		/// <param name="namespaceManager">The <see cref="T:System.Xml.XmlNamespaceManager" />. </param>
		// Token: 0x060026C6 RID: 9926 RVA: 0x000DD988 File Offset: 0x000DBB88
		public void Write(TextWriter writer, XmlNamespaceManager namespaceManager)
		{
			this.Write(new XmlTextWriter(writer)
			{
				Formatting = Formatting.Indented
			}, namespaceManager);
		}

		/// <summary>Writes the XML Schema to the supplied <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> to write to. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="writer" /> parameter is null.</exception>
		// Token: 0x060026C7 RID: 9927 RVA: 0x000DD9AB File Offset: 0x000DBBAB
		public void Write(XmlWriter writer)
		{
			this.Write(writer, null);
		}

		/// <summary>Writes the XML Schema to the supplied <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> to write to.</param>
		/// <param name="namespaceManager">The <see cref="T:System.Xml.XmlNamespaceManager" />. </param>
		// Token: 0x060026C8 RID: 9928 RVA: 0x000DD9B8 File Offset: 0x000DBBB8
		public void Write(XmlWriter writer, XmlNamespaceManager namespaceManager)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(XmlSchema));
			XmlSerializerNamespaces xmlSerializerNamespaces;
			if (namespaceManager != null)
			{
				xmlSerializerNamespaces = new XmlSerializerNamespaces();
				bool flag = false;
				if (base.Namespaces != null)
				{
					flag = (base.Namespaces.Namespaces["xs"] != null || base.Namespaces.Namespaces.ContainsValue("http://www.w3.org/2001/XMLSchema"));
				}
				if (!flag && namespaceManager.LookupPrefix("http://www.w3.org/2001/XMLSchema") == null && namespaceManager.LookupNamespace("xs") == null)
				{
					xmlSerializerNamespaces.Add("xs", "http://www.w3.org/2001/XMLSchema");
				}
				using (IEnumerator enumerator = namespaceManager.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						string text = (string)obj;
						if (text != "xml" && text != "xmlns")
						{
							xmlSerializerNamespaces.Add(text, namespaceManager.LookupNamespace(text));
						}
					}
					goto IL_17B;
				}
			}
			if (base.Namespaces != null && base.Namespaces.Count > 0)
			{
				Hashtable namespaces = base.Namespaces.Namespaces;
				if (namespaces["xs"] == null && !namespaces.ContainsValue("http://www.w3.org/2001/XMLSchema"))
				{
					namespaces.Add("xs", "http://www.w3.org/2001/XMLSchema");
				}
				xmlSerializerNamespaces = base.Namespaces;
			}
			else
			{
				xmlSerializerNamespaces = new XmlSerializerNamespaces();
				xmlSerializerNamespaces.Add("xs", "http://www.w3.org/2001/XMLSchema");
				if (this.targetNs != null && this.targetNs.Length != 0)
				{
					xmlSerializerNamespaces.Add("tns", this.targetNs);
				}
			}
			IL_17B:
			xmlSerializer.Serialize(writer, this, xmlSerializerNamespaces);
		}

		/// <summary>Compiles the XML Schema Object Model (SOM) into schema information for validation. Used to check the syntactic and semantic structure of the programmatically built SOM. Semantic validation checking is performed during compilation.</summary>
		/// <param name="validationEventHandler">The validation event handler that receives information about XML Schema validation errors. </param>
		// Token: 0x060026C9 RID: 9929 RVA: 0x000DDB5C File Offset: 0x000DBD5C
		[Obsolete("Use System.Xml.Schema.XmlSchemaSet for schema compilation and validation. http://go.microsoft.com/fwlink/?linkid=14202")]
		public void Compile(ValidationEventHandler validationEventHandler)
		{
			SchemaInfo schemaInfo = new SchemaInfo();
			schemaInfo.SchemaType = SchemaType.XSD;
			this.CompileSchema(null, XmlReaderSection.CreateDefaultResolver(), schemaInfo, null, validationEventHandler, this.NameTable, false);
		}

		/// <summary>Compiles the XML Schema Object Model (SOM) into schema information for validation. Used to check the syntactic and semantic structure of the programmatically built SOM. Semantic validation checking is performed during compilation.</summary>
		/// <param name="validationEventHandler">The validation event handler that receives information about the XML Schema validation errors. </param>
		/// <param name="resolver">The <see langword="XmlResolver" /> used to resolve namespaces referenced in <see langword="include" /> and <see langword="import" /> elements. </param>
		// Token: 0x060026CA RID: 9930 RVA: 0x000DDB90 File Offset: 0x000DBD90
		[Obsolete("Use System.Xml.Schema.XmlSchemaSet for schema compilation and validation. http://go.microsoft.com/fwlink/?linkid=14202")]
		public void Compile(ValidationEventHandler validationEventHandler, XmlResolver resolver)
		{
			this.CompileSchema(null, resolver, new SchemaInfo
			{
				SchemaType = SchemaType.XSD
			}, null, validationEventHandler, this.NameTable, false);
		}

		// Token: 0x060026CB RID: 9931 RVA: 0x000DDBC0 File Offset: 0x000DBDC0
		internal bool CompileSchema(XmlSchemaCollection xsc, XmlResolver resolver, SchemaInfo schemaInfo, string ns, ValidationEventHandler validationEventHandler, XmlNameTable nameTable, bool CompileContentModel)
		{
			bool result;
			lock (this)
			{
				if (!new SchemaCollectionPreprocessor(nameTable, null, validationEventHandler)
				{
					XmlResolver = resolver
				}.Execute(this, ns, true, xsc))
				{
					result = false;
				}
				else
				{
					SchemaCollectionCompiler schemaCollectionCompiler = new SchemaCollectionCompiler(nameTable, validationEventHandler);
					this.isCompiled = schemaCollectionCompiler.Execute(this, schemaInfo, CompileContentModel);
					this.SetIsCompiled(this.isCompiled);
					result = this.isCompiled;
				}
			}
			return result;
		}

		// Token: 0x060026CC RID: 9932 RVA: 0x000DDC44 File Offset: 0x000DBE44
		internal void CompileSchemaInSet(XmlNameTable nameTable, ValidationEventHandler eventHandler, XmlSchemaCompilationSettings compilationSettings)
		{
			Compiler compiler = new Compiler(nameTable, eventHandler, null, compilationSettings);
			compiler.Prepare(this, true);
			this.isCompiledBySet = compiler.Compile();
		}

		/// <summary>Gets or sets the form for attributes declared in the target namespace of the schema.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaForm" /> value that indicates if attributes from the target namespace are required to be qualified with the namespace prefix. The default is <see cref="F:System.Xml.Schema.XmlSchemaForm.None" />.</returns>
		// Token: 0x170007F7 RID: 2039
		// (get) Token: 0x060026CD RID: 9933 RVA: 0x000DDC6F File Offset: 0x000DBE6F
		// (set) Token: 0x060026CE RID: 9934 RVA: 0x000DDC77 File Offset: 0x000DBE77
		[XmlAttribute("attributeFormDefault")]
		[DefaultValue(XmlSchemaForm.None)]
		public XmlSchemaForm AttributeFormDefault
		{
			get
			{
				return this.attributeFormDefault;
			}
			set
			{
				this.attributeFormDefault = value;
			}
		}

		/// <summary>Gets or sets the <see langword="blockDefault" /> attribute which sets the default value of the <see langword="block" /> attribute on element and complex types in the <see langword="targetNamespace" /> of the schema.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaDerivationMethod" /> value representing the different methods for preventing derivation. The default value is <see langword="XmlSchemaDerivationMethod.None" />.</returns>
		// Token: 0x170007F8 RID: 2040
		// (get) Token: 0x060026CF RID: 9935 RVA: 0x000DDC80 File Offset: 0x000DBE80
		// (set) Token: 0x060026D0 RID: 9936 RVA: 0x000DDC88 File Offset: 0x000DBE88
		[DefaultValue(XmlSchemaDerivationMethod.None)]
		[XmlAttribute("blockDefault")]
		public XmlSchemaDerivationMethod BlockDefault
		{
			get
			{
				return this.blockDefault;
			}
			set
			{
				this.blockDefault = value;
			}
		}

		/// <summary>Gets or sets the <see langword="finalDefault" /> attribute which sets the default value of the <see langword="final" /> attribute on elements and complex types in the target namespace of the schema.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaDerivationMethod" /> value representing the different methods for preventing derivation. The default value is <see langword="XmlSchemaDerivationMethod.None" />.</returns>
		// Token: 0x170007F9 RID: 2041
		// (get) Token: 0x060026D1 RID: 9937 RVA: 0x000DDC91 File Offset: 0x000DBE91
		// (set) Token: 0x060026D2 RID: 9938 RVA: 0x000DDC99 File Offset: 0x000DBE99
		[DefaultValue(XmlSchemaDerivationMethod.None)]
		[XmlAttribute("finalDefault")]
		public XmlSchemaDerivationMethod FinalDefault
		{
			get
			{
				return this.finalDefault;
			}
			set
			{
				this.finalDefault = value;
			}
		}

		/// <summary>Gets or sets the form for elements declared in the target namespace of the schema.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaForm" /> value that indicates if elements from the target namespace are required to be qualified with the namespace prefix. The default is <see cref="F:System.Xml.Schema.XmlSchemaForm.None" />.</returns>
		// Token: 0x170007FA RID: 2042
		// (get) Token: 0x060026D3 RID: 9939 RVA: 0x000DDCA2 File Offset: 0x000DBEA2
		// (set) Token: 0x060026D4 RID: 9940 RVA: 0x000DDCAA File Offset: 0x000DBEAA
		[DefaultValue(XmlSchemaForm.None)]
		[XmlAttribute("elementFormDefault")]
		public XmlSchemaForm ElementFormDefault
		{
			get
			{
				return this.elementFormDefault;
			}
			set
			{
				this.elementFormDefault = value;
			}
		}

		/// <summary>Gets or sets the Uniform Resource Identifier (URI) of the schema target namespace.</summary>
		/// <returns>The schema target namespace.</returns>
		// Token: 0x170007FB RID: 2043
		// (get) Token: 0x060026D5 RID: 9941 RVA: 0x000DDCB3 File Offset: 0x000DBEB3
		// (set) Token: 0x060026D6 RID: 9942 RVA: 0x000DDCBB File Offset: 0x000DBEBB
		[XmlAttribute("targetNamespace", DataType = "anyURI")]
		public string TargetNamespace
		{
			get
			{
				return this.targetNs;
			}
			set
			{
				this.targetNs = value;
			}
		}

		/// <summary>Gets or sets the version of the schema.</summary>
		/// <returns>The version of the schema. The default value is <see langword="String.Empty" />.</returns>
		// Token: 0x170007FC RID: 2044
		// (get) Token: 0x060026D7 RID: 9943 RVA: 0x000DDCC4 File Offset: 0x000DBEC4
		// (set) Token: 0x060026D8 RID: 9944 RVA: 0x000DDCCC File Offset: 0x000DBECC
		[XmlAttribute("version", DataType = "token")]
		public string Version
		{
			get
			{
				return this.version;
			}
			set
			{
				this.version = value;
			}
		}

		/// <summary>Gets the collection of included and imported schemas.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectCollection" /> of the included and imported schemas.</returns>
		// Token: 0x170007FD RID: 2045
		// (get) Token: 0x060026D9 RID: 9945 RVA: 0x000DDCD5 File Offset: 0x000DBED5
		[XmlElement("import", typeof(XmlSchemaImport))]
		[XmlElement("redefine", typeof(XmlSchemaRedefine))]
		[XmlElement("include", typeof(XmlSchemaInclude))]
		public XmlSchemaObjectCollection Includes
		{
			get
			{
				return this.includes;
			}
		}

		/// <summary>Gets the collection of schema elements in the schema and is used to add new element types at the <see langword="schema" /> element level.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectCollection" /> of schema elements in the schema.</returns>
		// Token: 0x170007FE RID: 2046
		// (get) Token: 0x060026DA RID: 9946 RVA: 0x000DDCDD File Offset: 0x000DBEDD
		[XmlElement("attributeGroup", typeof(XmlSchemaAttributeGroup))]
		[XmlElement("complexType", typeof(XmlSchemaComplexType))]
		[XmlElement("notation", typeof(XmlSchemaNotation))]
		[XmlElement("attribute", typeof(XmlSchemaAttribute))]
		[XmlElement("simpleType", typeof(XmlSchemaSimpleType))]
		[XmlElement("group", typeof(XmlSchemaGroup))]
		[XmlElement("element", typeof(XmlSchemaElement))]
		[XmlElement("annotation", typeof(XmlSchemaAnnotation))]
		public XmlSchemaObjectCollection Items
		{
			get
			{
				return this.items;
			}
		}

		/// <summary>Indicates if the schema has been compiled.</summary>
		/// <returns>
		///     <see langword="true" /> if schema has been compiled, otherwise, <see langword="false" />. The default value is <see langword="false" />.</returns>
		// Token: 0x170007FF RID: 2047
		// (get) Token: 0x060026DB RID: 9947 RVA: 0x000DDCE5 File Offset: 0x000DBEE5
		[XmlIgnore]
		public bool IsCompiled
		{
			get
			{
				return this.isCompiled || this.isCompiledBySet;
			}
		}

		// Token: 0x17000800 RID: 2048
		// (get) Token: 0x060026DC RID: 9948 RVA: 0x000DDCF7 File Offset: 0x000DBEF7
		// (set) Token: 0x060026DD RID: 9949 RVA: 0x000DDCFF File Offset: 0x000DBEFF
		[XmlIgnore]
		internal bool IsCompiledBySet
		{
			get
			{
				return this.isCompiledBySet;
			}
			set
			{
				this.isCompiledBySet = value;
			}
		}

		// Token: 0x17000801 RID: 2049
		// (get) Token: 0x060026DE RID: 9950 RVA: 0x000DDD08 File Offset: 0x000DBF08
		// (set) Token: 0x060026DF RID: 9951 RVA: 0x000DDD10 File Offset: 0x000DBF10
		[XmlIgnore]
		internal bool IsPreprocessed
		{
			get
			{
				return this.isPreprocessed;
			}
			set
			{
				this.isPreprocessed = value;
			}
		}

		// Token: 0x17000802 RID: 2050
		// (get) Token: 0x060026E0 RID: 9952 RVA: 0x000DDD19 File Offset: 0x000DBF19
		// (set) Token: 0x060026E1 RID: 9953 RVA: 0x000DDD21 File Offset: 0x000DBF21
		[XmlIgnore]
		internal bool IsRedefined
		{
			get
			{
				return this.isRedefined;
			}
			set
			{
				this.isRedefined = value;
			}
		}

		/// <summary>Gets the post-schema-compilation value for all the attributes in the schema.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> collection of all the attributes in the schema.</returns>
		// Token: 0x17000803 RID: 2051
		// (get) Token: 0x060026E2 RID: 9954 RVA: 0x000DDD2A File Offset: 0x000DBF2A
		[XmlIgnore]
		public XmlSchemaObjectTable Attributes
		{
			get
			{
				if (this.attributes == null)
				{
					this.attributes = new XmlSchemaObjectTable();
				}
				return this.attributes;
			}
		}

		/// <summary>Gets the post-schema-compilation value of all the global attribute groups in the schema.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> collection of all the global attribute groups in the schema.</returns>
		// Token: 0x17000804 RID: 2052
		// (get) Token: 0x060026E3 RID: 9955 RVA: 0x000DDD45 File Offset: 0x000DBF45
		[XmlIgnore]
		public XmlSchemaObjectTable AttributeGroups
		{
			get
			{
				if (this.attributeGroups == null)
				{
					this.attributeGroups = new XmlSchemaObjectTable();
				}
				return this.attributeGroups;
			}
		}

		/// <summary>Gets the post-schema-compilation value of all schema types in the schema.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectCollection" /> of all schema types in the schema.</returns>
		// Token: 0x17000805 RID: 2053
		// (get) Token: 0x060026E4 RID: 9956 RVA: 0x000DDD60 File Offset: 0x000DBF60
		[XmlIgnore]
		public XmlSchemaObjectTable SchemaTypes
		{
			get
			{
				if (this.types == null)
				{
					this.types = new XmlSchemaObjectTable();
				}
				return this.types;
			}
		}

		/// <summary>Gets the post-schema-compilation value for all the elements in the schema.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> collection of all the elements in the schema.</returns>
		// Token: 0x17000806 RID: 2054
		// (get) Token: 0x060026E5 RID: 9957 RVA: 0x000DDD7B File Offset: 0x000DBF7B
		[XmlIgnore]
		public XmlSchemaObjectTable Elements
		{
			get
			{
				if (this.elements == null)
				{
					this.elements = new XmlSchemaObjectTable();
				}
				return this.elements;
			}
		}

		/// <summary>Gets or sets the string ID.</summary>
		/// <returns>The ID of the string. The default value is <see langword="String.Empty" />.</returns>
		// Token: 0x17000807 RID: 2055
		// (get) Token: 0x060026E6 RID: 9958 RVA: 0x000DDD96 File Offset: 0x000DBF96
		// (set) Token: 0x060026E7 RID: 9959 RVA: 0x000DDD9E File Offset: 0x000DBF9E
		[XmlAttribute("id", DataType = "ID")]
		public string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}

		/// <summary>Gets and sets the qualified attributes which do not belong to the schema target namespace.</summary>
		/// <returns>An array of qualified <see cref="T:System.Xml.XmlAttribute" /> objects that do not belong to the schema target namespace.</returns>
		// Token: 0x17000808 RID: 2056
		// (get) Token: 0x060026E8 RID: 9960 RVA: 0x000DDDA7 File Offset: 0x000DBFA7
		// (set) Token: 0x060026E9 RID: 9961 RVA: 0x000DDDAF File Offset: 0x000DBFAF
		[XmlAnyAttribute]
		public XmlAttribute[] UnhandledAttributes
		{
			get
			{
				return this.moreAttributes;
			}
			set
			{
				this.moreAttributes = value;
			}
		}

		/// <summary>Gets the post-schema-compilation value of all the groups in the schema.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> collection of all the groups in the schema.</returns>
		// Token: 0x17000809 RID: 2057
		// (get) Token: 0x060026EA RID: 9962 RVA: 0x000DDDB8 File Offset: 0x000DBFB8
		[XmlIgnore]
		public XmlSchemaObjectTable Groups
		{
			get
			{
				return this.groups;
			}
		}

		/// <summary>Gets the post-schema-compilation value for all notations in the schema.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> collection of all notations in the schema.</returns>
		// Token: 0x1700080A RID: 2058
		// (get) Token: 0x060026EB RID: 9963 RVA: 0x000DDDC0 File Offset: 0x000DBFC0
		[XmlIgnore]
		public XmlSchemaObjectTable Notations
		{
			get
			{
				return this.notations;
			}
		}

		// Token: 0x1700080B RID: 2059
		// (get) Token: 0x060026EC RID: 9964 RVA: 0x000DDDC8 File Offset: 0x000DBFC8
		[XmlIgnore]
		internal XmlSchemaObjectTable IdentityConstraints
		{
			get
			{
				return this.identityConstraints;
			}
		}

		// Token: 0x1700080C RID: 2060
		// (get) Token: 0x060026ED RID: 9965 RVA: 0x000DDDD0 File Offset: 0x000DBFD0
		// (set) Token: 0x060026EE RID: 9966 RVA: 0x000DDDD8 File Offset: 0x000DBFD8
		[XmlIgnore]
		internal Uri BaseUri
		{
			get
			{
				return this.baseUri;
			}
			set
			{
				this.baseUri = value;
			}
		}

		// Token: 0x1700080D RID: 2061
		// (get) Token: 0x060026EF RID: 9967 RVA: 0x000DDDE1 File Offset: 0x000DBFE1
		[XmlIgnore]
		internal int SchemaId
		{
			get
			{
				if (this.schemaId == -1)
				{
					this.schemaId = Interlocked.Increment(ref XmlSchema.globalIdCounter);
				}
				return this.schemaId;
			}
		}

		// Token: 0x1700080E RID: 2062
		// (get) Token: 0x060026F0 RID: 9968 RVA: 0x000DDE02 File Offset: 0x000DC002
		// (set) Token: 0x060026F1 RID: 9969 RVA: 0x000DDE0A File Offset: 0x000DC00A
		[XmlIgnore]
		internal bool IsChameleon
		{
			get
			{
				return this.isChameleon;
			}
			set
			{
				this.isChameleon = value;
			}
		}

		// Token: 0x1700080F RID: 2063
		// (get) Token: 0x060026F2 RID: 9970 RVA: 0x000DDE13 File Offset: 0x000DC013
		[XmlIgnore]
		internal Hashtable Ids
		{
			get
			{
				return this.ids;
			}
		}

		// Token: 0x17000810 RID: 2064
		// (get) Token: 0x060026F3 RID: 9971 RVA: 0x000DDE1B File Offset: 0x000DC01B
		[XmlIgnore]
		internal XmlDocument Document
		{
			get
			{
				if (this.document == null)
				{
					this.document = new XmlDocument();
				}
				return this.document;
			}
		}

		// Token: 0x17000811 RID: 2065
		// (get) Token: 0x060026F4 RID: 9972 RVA: 0x000DDE36 File Offset: 0x000DC036
		// (set) Token: 0x060026F5 RID: 9973 RVA: 0x000DDE3E File Offset: 0x000DC03E
		[XmlIgnore]
		internal int ErrorCount
		{
			get
			{
				return this.errorCount;
			}
			set
			{
				this.errorCount = value;
			}
		}

		// Token: 0x060026F6 RID: 9974 RVA: 0x000DDE48 File Offset: 0x000DC048
		internal new XmlSchema Clone()
		{
			XmlSchema xmlSchema = new XmlSchema();
			xmlSchema.attributeFormDefault = this.attributeFormDefault;
			xmlSchema.elementFormDefault = this.elementFormDefault;
			xmlSchema.blockDefault = this.blockDefault;
			xmlSchema.finalDefault = this.finalDefault;
			xmlSchema.targetNs = this.targetNs;
			xmlSchema.version = this.version;
			xmlSchema.includes = this.includes;
			xmlSchema.Namespaces = base.Namespaces;
			xmlSchema.items = this.items;
			xmlSchema.BaseUri = this.BaseUri;
			SchemaCollectionCompiler.Cleanup(xmlSchema);
			return xmlSchema;
		}

		// Token: 0x060026F7 RID: 9975 RVA: 0x000DDED8 File Offset: 0x000DC0D8
		internal XmlSchema DeepClone()
		{
			XmlSchema xmlSchema = new XmlSchema();
			xmlSchema.attributeFormDefault = this.attributeFormDefault;
			xmlSchema.elementFormDefault = this.elementFormDefault;
			xmlSchema.blockDefault = this.blockDefault;
			xmlSchema.finalDefault = this.finalDefault;
			xmlSchema.targetNs = this.targetNs;
			xmlSchema.version = this.version;
			xmlSchema.isPreprocessed = this.isPreprocessed;
			for (int i = 0; i < this.items.Count; i++)
			{
				XmlSchemaComplexType xmlSchemaComplexType;
				XmlSchemaObject item;
				XmlSchemaElement xmlSchemaElement;
				XmlSchemaGroup xmlSchemaGroup;
				if ((xmlSchemaComplexType = (this.items[i] as XmlSchemaComplexType)) != null)
				{
					item = xmlSchemaComplexType.Clone(this);
				}
				else if ((xmlSchemaElement = (this.items[i] as XmlSchemaElement)) != null)
				{
					item = xmlSchemaElement.Clone(this);
				}
				else if ((xmlSchemaGroup = (this.items[i] as XmlSchemaGroup)) != null)
				{
					item = xmlSchemaGroup.Clone(this);
				}
				else
				{
					item = this.items[i].Clone();
				}
				xmlSchema.Items.Add(item);
			}
			for (int j = 0; j < this.includes.Count; j++)
			{
				XmlSchemaExternal item2 = (XmlSchemaExternal)this.includes[j].Clone();
				xmlSchema.Includes.Add(item2);
			}
			xmlSchema.Namespaces = base.Namespaces;
			xmlSchema.BaseUri = this.BaseUri;
			return xmlSchema;
		}

		// Token: 0x17000812 RID: 2066
		// (get) Token: 0x060026F8 RID: 9976 RVA: 0x000DE035 File Offset: 0x000DC235
		// (set) Token: 0x060026F9 RID: 9977 RVA: 0x000DE03D File Offset: 0x000DC23D
		[XmlIgnore]
		internal override string IdAttribute
		{
			get
			{
				return this.Id;
			}
			set
			{
				this.Id = value;
			}
		}

		// Token: 0x060026FA RID: 9978 RVA: 0x000DE046 File Offset: 0x000DC246
		internal void SetIsCompiled(bool isCompiled)
		{
			this.isCompiled = isCompiled;
		}

		// Token: 0x060026FB RID: 9979 RVA: 0x000DDDAF File Offset: 0x000DBFAF
		internal override void SetUnhandledAttributes(XmlAttribute[] moreAttributes)
		{
			this.moreAttributes = moreAttributes;
		}

		// Token: 0x060026FC RID: 9980 RVA: 0x000DE04F File Offset: 0x000DC24F
		internal override void AddAnnotation(XmlSchemaAnnotation annotation)
		{
			this.items.Add(annotation);
		}

		// Token: 0x17000813 RID: 2067
		// (get) Token: 0x060026FD RID: 9981 RVA: 0x000DE05E File Offset: 0x000DC25E
		internal XmlNameTable NameTable
		{
			get
			{
				if (this.nameTable == null)
				{
					this.nameTable = new NameTable();
				}
				return this.nameTable;
			}
		}

		// Token: 0x17000814 RID: 2068
		// (get) Token: 0x060026FE RID: 9982 RVA: 0x000DE079 File Offset: 0x000DC279
		internal ArrayList ImportedSchemas
		{
			get
			{
				if (this.importedSchemas == null)
				{
					this.importedSchemas = new ArrayList();
				}
				return this.importedSchemas;
			}
		}

		// Token: 0x17000815 RID: 2069
		// (get) Token: 0x060026FF RID: 9983 RVA: 0x000DE094 File Offset: 0x000DC294
		internal ArrayList ImportedNamespaces
		{
			get
			{
				if (this.importedNamespaces == null)
				{
					this.importedNamespaces = new ArrayList();
				}
				return this.importedNamespaces;
			}
		}

		// Token: 0x06002700 RID: 9984 RVA: 0x000DE0B0 File Offset: 0x000DC2B0
		internal void GetExternalSchemasList(IList extList, XmlSchema schema)
		{
			if (extList.Contains(schema))
			{
				return;
			}
			extList.Add(schema);
			for (int i = 0; i < schema.Includes.Count; i++)
			{
				XmlSchemaExternal xmlSchemaExternal = (XmlSchemaExternal)schema.Includes[i];
				if (xmlSchemaExternal.Schema != null)
				{
					this.GetExternalSchemasList(extList, xmlSchemaExternal.Schema);
				}
			}
		}

		// Token: 0x06002701 RID: 9985 RVA: 0x000DE10C File Offset: 0x000DC30C
		// Note: this type is marked as 'beforefieldinit'.
		static XmlSchema()
		{
		}

		/// <summary>The XML schema namespace. This field is constant.</summary>
		// Token: 0x04001BBE RID: 7102
		public const string Namespace = "http://www.w3.org/2001/XMLSchema";

		/// <summary>The XML schema instance namespace. This field is constant. </summary>
		// Token: 0x04001BBF RID: 7103
		public const string InstanceNamespace = "http://www.w3.org/2001/XMLSchema-instance";

		// Token: 0x04001BC0 RID: 7104
		private XmlSchemaForm attributeFormDefault;

		// Token: 0x04001BC1 RID: 7105
		private XmlSchemaForm elementFormDefault;

		// Token: 0x04001BC2 RID: 7106
		private XmlSchemaDerivationMethod blockDefault = XmlSchemaDerivationMethod.None;

		// Token: 0x04001BC3 RID: 7107
		private XmlSchemaDerivationMethod finalDefault = XmlSchemaDerivationMethod.None;

		// Token: 0x04001BC4 RID: 7108
		private string targetNs;

		// Token: 0x04001BC5 RID: 7109
		private string version;

		// Token: 0x04001BC6 RID: 7110
		private XmlSchemaObjectCollection includes = new XmlSchemaObjectCollection();

		// Token: 0x04001BC7 RID: 7111
		private XmlSchemaObjectCollection items = new XmlSchemaObjectCollection();

		// Token: 0x04001BC8 RID: 7112
		private string id;

		// Token: 0x04001BC9 RID: 7113
		private XmlAttribute[] moreAttributes;

		// Token: 0x04001BCA RID: 7114
		private bool isCompiled;

		// Token: 0x04001BCB RID: 7115
		private bool isCompiledBySet;

		// Token: 0x04001BCC RID: 7116
		private bool isPreprocessed;

		// Token: 0x04001BCD RID: 7117
		private bool isRedefined;

		// Token: 0x04001BCE RID: 7118
		private int errorCount;

		// Token: 0x04001BCF RID: 7119
		private XmlSchemaObjectTable attributes;

		// Token: 0x04001BD0 RID: 7120
		private XmlSchemaObjectTable attributeGroups = new XmlSchemaObjectTable();

		// Token: 0x04001BD1 RID: 7121
		private XmlSchemaObjectTable elements = new XmlSchemaObjectTable();

		// Token: 0x04001BD2 RID: 7122
		private XmlSchemaObjectTable types = new XmlSchemaObjectTable();

		// Token: 0x04001BD3 RID: 7123
		private XmlSchemaObjectTable groups = new XmlSchemaObjectTable();

		// Token: 0x04001BD4 RID: 7124
		private XmlSchemaObjectTable notations = new XmlSchemaObjectTable();

		// Token: 0x04001BD5 RID: 7125
		private XmlSchemaObjectTable identityConstraints = new XmlSchemaObjectTable();

		// Token: 0x04001BD6 RID: 7126
		private static int globalIdCounter = -1;

		// Token: 0x04001BD7 RID: 7127
		private ArrayList importedSchemas;

		// Token: 0x04001BD8 RID: 7128
		private ArrayList importedNamespaces;

		// Token: 0x04001BD9 RID: 7129
		private int schemaId = -1;

		// Token: 0x04001BDA RID: 7130
		private Uri baseUri;

		// Token: 0x04001BDB RID: 7131
		private bool isChameleon;

		// Token: 0x04001BDC RID: 7132
		private Hashtable ids = new Hashtable();

		// Token: 0x04001BDD RID: 7133
		private XmlDocument document;

		// Token: 0x04001BDE RID: 7134
		private XmlNameTable nameTable;
	}
}
