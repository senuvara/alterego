﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Provides schema compilation options for the <see cref="T:System.Xml.Schema.XmlSchemaSet" /> class This class cannot be inherited.</summary>
	// Token: 0x02000410 RID: 1040
	public sealed class XmlSchemaCompilationSettings
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaCompilationSettings" /> class. </summary>
		// Token: 0x0600279E RID: 10142 RVA: 0x000DEE73 File Offset: 0x000DD073
		public XmlSchemaCompilationSettings()
		{
			this.enableUpaCheck = true;
		}

		/// <summary>Gets or sets a value indicating whether the <see cref="T:System.Xml.Schema.XmlSchemaSet" /> should check for Unique Particle Attribution (UPA) violations.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Xml.Schema.XmlSchemaSet" /> should check for Unique Particle Attribution (UPA) violations; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000856 RID: 2134
		// (get) Token: 0x0600279F RID: 10143 RVA: 0x000DEE82 File Offset: 0x000DD082
		// (set) Token: 0x060027A0 RID: 10144 RVA: 0x000DEE8A File Offset: 0x000DD08A
		public bool EnableUpaCheck
		{
			get
			{
				return this.enableUpaCheck;
			}
			set
			{
				this.enableUpaCheck = value;
			}
		}

		// Token: 0x04001C0F RID: 7183
		private bool enableUpaCheck;
	}
}
