﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000387 RID: 903
	internal class Datatype_string : Datatype_anySimpleType
	{
		// Token: 0x06002254 RID: 8788 RVA: 0x000BD910 File Offset: 0x000BBB10
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlStringConverter.Create(schemaType);
		}

		// Token: 0x170006D3 RID: 1747
		// (get) Token: 0x06002255 RID: 8789 RVA: 0x000020CD File Offset: 0x000002CD
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Preserve;
			}
		}

		// Token: 0x170006D4 RID: 1748
		// (get) Token: 0x06002256 RID: 8790 RVA: 0x000BD918 File Offset: 0x000BBB18
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.stringFacetsChecker;
			}
		}

		// Token: 0x170006D5 RID: 1749
		// (get) Token: 0x06002257 RID: 8791 RVA: 0x000164C5 File Offset: 0x000146C5
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.String;
			}
		}

		// Token: 0x170006D6 RID: 1750
		// (get) Token: 0x06002258 RID: 8792 RVA: 0x000020CD File Offset: 0x000002CD
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.CDATA;
			}
		}

		// Token: 0x170006D7 RID: 1751
		// (get) Token: 0x06002259 RID: 8793 RVA: 0x000BD2C1 File Offset: 0x000BB4C1
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Length | RestrictionFlags.MinLength | RestrictionFlags.MaxLength | RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace;
			}
		}

		// Token: 0x0600225A RID: 8794 RVA: 0x000BD920 File Offset: 0x000BBB20
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.stringFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				ex = DatatypeImplementation.stringFacetsChecker.CheckValueFacets(s, this);
				if (ex == null)
				{
					typedValue = s;
					return null;
				}
			}
			return ex;
		}

		// Token: 0x0600225B RID: 8795 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_string()
		{
		}
	}
}
