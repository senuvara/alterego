﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B0 RID: 944
	internal class Datatype_int : Datatype_long
	{
		// Token: 0x17000744 RID: 1860
		// (get) Token: 0x06002325 RID: 8997 RVA: 0x000BE581 File Offset: 0x000BC781
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_int.numeric10FacetsChecker;
			}
		}

		// Token: 0x17000745 RID: 1861
		// (get) Token: 0x06002326 RID: 8998 RVA: 0x000BE588 File Offset: 0x000BC788
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Int;
			}
		}

		// Token: 0x06002327 RID: 8999 RVA: 0x000BE58C File Offset: 0x000BC78C
		internal override int Compare(object value1, object value2)
		{
			return ((int)value1).CompareTo(value2);
		}

		// Token: 0x17000746 RID: 1862
		// (get) Token: 0x06002328 RID: 9000 RVA: 0x000BE5A8 File Offset: 0x000BC7A8
		public override Type ValueType
		{
			get
			{
				return Datatype_int.atomicValueType;
			}
		}

		// Token: 0x17000747 RID: 1863
		// (get) Token: 0x06002329 RID: 9001 RVA: 0x000BE5AF File Offset: 0x000BC7AF
		internal override Type ListValueType
		{
			get
			{
				return Datatype_int.listValueType;
			}
		}

		// Token: 0x0600232A RID: 9002 RVA: 0x000BE5B8 File Offset: 0x000BC7B8
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_int.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				int num;
				ex = XmlConvert.TryToInt32(s, out num);
				if (ex == null)
				{
					ex = Datatype_int.numeric10FacetsChecker.CheckValueFacets(num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x0600232B RID: 9003 RVA: 0x000BE602 File Offset: 0x000BC802
		public Datatype_int()
		{
		}

		// Token: 0x0600232C RID: 9004 RVA: 0x000BE60A File Offset: 0x000BC80A
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_int()
		{
		}

		// Token: 0x040018C3 RID: 6339
		private static readonly Type atomicValueType = typeof(int);

		// Token: 0x040018C4 RID: 6340
		private static readonly Type listValueType = typeof(int[]);

		// Token: 0x040018C5 RID: 6341
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(-2147483648m, 2147483647m);
	}
}
