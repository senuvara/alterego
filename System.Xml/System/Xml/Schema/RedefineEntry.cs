﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003DA RID: 986
	internal class RedefineEntry
	{
		// Token: 0x06002459 RID: 9305 RVA: 0x000C760E File Offset: 0x000C580E
		public RedefineEntry(XmlSchemaRedefine external, XmlSchema schema)
		{
			this.redefine = external;
			this.schemaToUpdate = schema;
		}

		// Token: 0x0400196F RID: 6511
		internal XmlSchemaRedefine redefine;

		// Token: 0x04001970 RID: 6512
		internal XmlSchema schemaToUpdate;
	}
}
