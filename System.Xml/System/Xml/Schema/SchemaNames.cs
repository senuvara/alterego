﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003E9 RID: 1001
	internal sealed class SchemaNames
	{
		// Token: 0x170007E2 RID: 2018
		// (get) Token: 0x060025B1 RID: 9649 RVA: 0x000D3197 File Offset: 0x000D1397
		public XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		// Token: 0x060025B2 RID: 9650 RVA: 0x000D31A0 File Offset: 0x000D13A0
		public SchemaNames(XmlNameTable nameTable)
		{
			this.nameTable = nameTable;
			this.NsDataType = nameTable.Add("urn:schemas-microsoft-com:datatypes");
			this.NsDataTypeAlias = nameTable.Add("uuid:C2F41010-65B3-11D1-A29F-00AA00C14882");
			this.NsDataTypeOld = nameTable.Add("urn:uuid:C2F41010-65B3-11D1-A29F-00AA00C14882/");
			this.NsXml = nameTable.Add("http://www.w3.org/XML/1998/namespace");
			this.NsXmlNs = nameTable.Add("http://www.w3.org/2000/xmlns/");
			this.NsXdr = nameTable.Add("urn:schemas-microsoft-com:xml-data");
			this.NsXdrAlias = nameTable.Add("uuid:BDC6E3F0-6DA3-11D1-A2A3-00AA00C14882");
			this.NsXs = nameTable.Add("http://www.w3.org/2001/XMLSchema");
			this.NsXsi = nameTable.Add("http://www.w3.org/2001/XMLSchema-instance");
			this.XsiType = nameTable.Add("type");
			this.XsiNil = nameTable.Add("nil");
			this.XsiSchemaLocation = nameTable.Add("schemaLocation");
			this.XsiNoNamespaceSchemaLocation = nameTable.Add("noNamespaceSchemaLocation");
			this.XsdSchema = nameTable.Add("schema");
			this.XdrSchema = nameTable.Add("Schema");
			this.QnPCData = new XmlQualifiedName(nameTable.Add("#PCDATA"));
			this.QnXml = new XmlQualifiedName(nameTable.Add("xml"));
			this.QnXmlNs = new XmlQualifiedName(nameTable.Add("xmlns"), this.NsXmlNs);
			this.QnDtDt = new XmlQualifiedName(nameTable.Add("dt"), this.NsDataType);
			this.QnXmlLang = new XmlQualifiedName(nameTable.Add("lang"), this.NsXml);
			this.QnName = new XmlQualifiedName(nameTable.Add("name"));
			this.QnType = new XmlQualifiedName(nameTable.Add("type"));
			this.QnMaxOccurs = new XmlQualifiedName(nameTable.Add("maxOccurs"));
			this.QnMinOccurs = new XmlQualifiedName(nameTable.Add("minOccurs"));
			this.QnInfinite = new XmlQualifiedName(nameTable.Add("*"));
			this.QnModel = new XmlQualifiedName(nameTable.Add("model"));
			this.QnOpen = new XmlQualifiedName(nameTable.Add("open"));
			this.QnClosed = new XmlQualifiedName(nameTable.Add("closed"));
			this.QnContent = new XmlQualifiedName(nameTable.Add("content"));
			this.QnMixed = new XmlQualifiedName(nameTable.Add("mixed"));
			this.QnEmpty = new XmlQualifiedName(nameTable.Add("empty"));
			this.QnEltOnly = new XmlQualifiedName(nameTable.Add("eltOnly"));
			this.QnTextOnly = new XmlQualifiedName(nameTable.Add("textOnly"));
			this.QnOrder = new XmlQualifiedName(nameTable.Add("order"));
			this.QnSeq = new XmlQualifiedName(nameTable.Add("seq"));
			this.QnOne = new XmlQualifiedName(nameTable.Add("one"));
			this.QnMany = new XmlQualifiedName(nameTable.Add("many"));
			this.QnRequired = new XmlQualifiedName(nameTable.Add("required"));
			this.QnYes = new XmlQualifiedName(nameTable.Add("yes"));
			this.QnNo = new XmlQualifiedName(nameTable.Add("no"));
			this.QnString = new XmlQualifiedName(nameTable.Add("string"));
			this.QnID = new XmlQualifiedName(nameTable.Add("id"));
			this.QnIDRef = new XmlQualifiedName(nameTable.Add("idref"));
			this.QnIDRefs = new XmlQualifiedName(nameTable.Add("idrefs"));
			this.QnEntity = new XmlQualifiedName(nameTable.Add("entity"));
			this.QnEntities = new XmlQualifiedName(nameTable.Add("entities"));
			this.QnNmToken = new XmlQualifiedName(nameTable.Add("nmtoken"));
			this.QnNmTokens = new XmlQualifiedName(nameTable.Add("nmtokens"));
			this.QnEnumeration = new XmlQualifiedName(nameTable.Add("enumeration"));
			this.QnDefault = new XmlQualifiedName(nameTable.Add("default"));
			this.QnTargetNamespace = new XmlQualifiedName(nameTable.Add("targetNamespace"));
			this.QnVersion = new XmlQualifiedName(nameTable.Add("version"));
			this.QnFinalDefault = new XmlQualifiedName(nameTable.Add("finalDefault"));
			this.QnBlockDefault = new XmlQualifiedName(nameTable.Add("blockDefault"));
			this.QnFixed = new XmlQualifiedName(nameTable.Add("fixed"));
			this.QnAbstract = new XmlQualifiedName(nameTable.Add("abstract"));
			this.QnBlock = new XmlQualifiedName(nameTable.Add("block"));
			this.QnSubstitutionGroup = new XmlQualifiedName(nameTable.Add("substitutionGroup"));
			this.QnFinal = new XmlQualifiedName(nameTable.Add("final"));
			this.QnNillable = new XmlQualifiedName(nameTable.Add("nillable"));
			this.QnRef = new XmlQualifiedName(nameTable.Add("ref"));
			this.QnBase = new XmlQualifiedName(nameTable.Add("base"));
			this.QnDerivedBy = new XmlQualifiedName(nameTable.Add("derivedBy"));
			this.QnNamespace = new XmlQualifiedName(nameTable.Add("namespace"));
			this.QnProcessContents = new XmlQualifiedName(nameTable.Add("processContents"));
			this.QnRefer = new XmlQualifiedName(nameTable.Add("refer"));
			this.QnPublic = new XmlQualifiedName(nameTable.Add("public"));
			this.QnSystem = new XmlQualifiedName(nameTable.Add("system"));
			this.QnSchemaLocation = new XmlQualifiedName(nameTable.Add("schemaLocation"));
			this.QnValue = new XmlQualifiedName(nameTable.Add("value"));
			this.QnUse = new XmlQualifiedName(nameTable.Add("use"));
			this.QnForm = new XmlQualifiedName(nameTable.Add("form"));
			this.QnAttributeFormDefault = new XmlQualifiedName(nameTable.Add("attributeFormDefault"));
			this.QnElementFormDefault = new XmlQualifiedName(nameTable.Add("elementFormDefault"));
			this.QnSource = new XmlQualifiedName(nameTable.Add("source"));
			this.QnMemberTypes = new XmlQualifiedName(nameTable.Add("memberTypes"));
			this.QnItemType = new XmlQualifiedName(nameTable.Add("itemType"));
			this.QnXPath = new XmlQualifiedName(nameTable.Add("xpath"));
			this.QnXdrSchema = new XmlQualifiedName(this.XdrSchema, this.NsXdr);
			this.QnXdrElementType = new XmlQualifiedName(nameTable.Add("ElementType"), this.NsXdr);
			this.QnXdrElement = new XmlQualifiedName(nameTable.Add("element"), this.NsXdr);
			this.QnXdrGroup = new XmlQualifiedName(nameTable.Add("group"), this.NsXdr);
			this.QnXdrAttributeType = new XmlQualifiedName(nameTable.Add("AttributeType"), this.NsXdr);
			this.QnXdrAttribute = new XmlQualifiedName(nameTable.Add("attribute"), this.NsXdr);
			this.QnXdrDataType = new XmlQualifiedName(nameTable.Add("datatype"), this.NsXdr);
			this.QnXdrDescription = new XmlQualifiedName(nameTable.Add("description"), this.NsXdr);
			this.QnXdrExtends = new XmlQualifiedName(nameTable.Add("extends"), this.NsXdr);
			this.QnXdrAliasSchema = new XmlQualifiedName(nameTable.Add("Schema"), this.NsDataTypeAlias);
			this.QnDtType = new XmlQualifiedName(nameTable.Add("type"), this.NsDataType);
			this.QnDtValues = new XmlQualifiedName(nameTable.Add("values"), this.NsDataType);
			this.QnDtMaxLength = new XmlQualifiedName(nameTable.Add("maxLength"), this.NsDataType);
			this.QnDtMinLength = new XmlQualifiedName(nameTable.Add("minLength"), this.NsDataType);
			this.QnDtMax = new XmlQualifiedName(nameTable.Add("max"), this.NsDataType);
			this.QnDtMin = new XmlQualifiedName(nameTable.Add("min"), this.NsDataType);
			this.QnDtMinExclusive = new XmlQualifiedName(nameTable.Add("minExclusive"), this.NsDataType);
			this.QnDtMaxExclusive = new XmlQualifiedName(nameTable.Add("maxExclusive"), this.NsDataType);
			this.QnXsdSchema = new XmlQualifiedName(this.XsdSchema, this.NsXs);
			this.QnXsdAnnotation = new XmlQualifiedName(nameTable.Add("annotation"), this.NsXs);
			this.QnXsdInclude = new XmlQualifiedName(nameTable.Add("include"), this.NsXs);
			this.QnXsdImport = new XmlQualifiedName(nameTable.Add("import"), this.NsXs);
			this.QnXsdElement = new XmlQualifiedName(nameTable.Add("element"), this.NsXs);
			this.QnXsdAttribute = new XmlQualifiedName(nameTable.Add("attribute"), this.NsXs);
			this.QnXsdAttributeGroup = new XmlQualifiedName(nameTable.Add("attributeGroup"), this.NsXs);
			this.QnXsdAnyAttribute = new XmlQualifiedName(nameTable.Add("anyAttribute"), this.NsXs);
			this.QnXsdGroup = new XmlQualifiedName(nameTable.Add("group"), this.NsXs);
			this.QnXsdAll = new XmlQualifiedName(nameTable.Add("all"), this.NsXs);
			this.QnXsdChoice = new XmlQualifiedName(nameTable.Add("choice"), this.NsXs);
			this.QnXsdSequence = new XmlQualifiedName(nameTable.Add("sequence"), this.NsXs);
			this.QnXsdAny = new XmlQualifiedName(nameTable.Add("any"), this.NsXs);
			this.QnXsdNotation = new XmlQualifiedName(nameTable.Add("notation"), this.NsXs);
			this.QnXsdSimpleType = new XmlQualifiedName(nameTable.Add("simpleType"), this.NsXs);
			this.QnXsdComplexType = new XmlQualifiedName(nameTable.Add("complexType"), this.NsXs);
			this.QnXsdUnique = new XmlQualifiedName(nameTable.Add("unique"), this.NsXs);
			this.QnXsdKey = new XmlQualifiedName(nameTable.Add("key"), this.NsXs);
			this.QnXsdKeyRef = new XmlQualifiedName(nameTable.Add("keyref"), this.NsXs);
			this.QnXsdSelector = new XmlQualifiedName(nameTable.Add("selector"), this.NsXs);
			this.QnXsdField = new XmlQualifiedName(nameTable.Add("field"), this.NsXs);
			this.QnXsdMinExclusive = new XmlQualifiedName(nameTable.Add("minExclusive"), this.NsXs);
			this.QnXsdMinInclusive = new XmlQualifiedName(nameTable.Add("minInclusive"), this.NsXs);
			this.QnXsdMaxInclusive = new XmlQualifiedName(nameTable.Add("maxInclusive"), this.NsXs);
			this.QnXsdMaxExclusive = new XmlQualifiedName(nameTable.Add("maxExclusive"), this.NsXs);
			this.QnXsdTotalDigits = new XmlQualifiedName(nameTable.Add("totalDigits"), this.NsXs);
			this.QnXsdFractionDigits = new XmlQualifiedName(nameTable.Add("fractionDigits"), this.NsXs);
			this.QnXsdLength = new XmlQualifiedName(nameTable.Add("length"), this.NsXs);
			this.QnXsdMinLength = new XmlQualifiedName(nameTable.Add("minLength"), this.NsXs);
			this.QnXsdMaxLength = new XmlQualifiedName(nameTable.Add("maxLength"), this.NsXs);
			this.QnXsdEnumeration = new XmlQualifiedName(nameTable.Add("enumeration"), this.NsXs);
			this.QnXsdPattern = new XmlQualifiedName(nameTable.Add("pattern"), this.NsXs);
			this.QnXsdDocumentation = new XmlQualifiedName(nameTable.Add("documentation"), this.NsXs);
			this.QnXsdAppinfo = new XmlQualifiedName(nameTable.Add("appinfo"), this.NsXs);
			this.QnXsdComplexContent = new XmlQualifiedName(nameTable.Add("complexContent"), this.NsXs);
			this.QnXsdSimpleContent = new XmlQualifiedName(nameTable.Add("simpleContent"), this.NsXs);
			this.QnXsdRestriction = new XmlQualifiedName(nameTable.Add("restriction"), this.NsXs);
			this.QnXsdExtension = new XmlQualifiedName(nameTable.Add("extension"), this.NsXs);
			this.QnXsdUnion = new XmlQualifiedName(nameTable.Add("union"), this.NsXs);
			this.QnXsdList = new XmlQualifiedName(nameTable.Add("list"), this.NsXs);
			this.QnXsdWhiteSpace = new XmlQualifiedName(nameTable.Add("whiteSpace"), this.NsXs);
			this.QnXsdRedefine = new XmlQualifiedName(nameTable.Add("redefine"), this.NsXs);
			this.QnXsdAnyType = new XmlQualifiedName(nameTable.Add("anyType"), this.NsXs);
			this.CreateTokenToQNameTable();
		}

		// Token: 0x060025B3 RID: 9651 RVA: 0x000D3EEC File Offset: 0x000D20EC
		public void CreateTokenToQNameTable()
		{
			this.TokenToQName[1] = this.QnName;
			this.TokenToQName[2] = this.QnType;
			this.TokenToQName[3] = this.QnMaxOccurs;
			this.TokenToQName[4] = this.QnMinOccurs;
			this.TokenToQName[5] = this.QnInfinite;
			this.TokenToQName[6] = this.QnModel;
			this.TokenToQName[7] = this.QnOpen;
			this.TokenToQName[8] = this.QnClosed;
			this.TokenToQName[9] = this.QnContent;
			this.TokenToQName[10] = this.QnMixed;
			this.TokenToQName[11] = this.QnEmpty;
			this.TokenToQName[12] = this.QnEltOnly;
			this.TokenToQName[13] = this.QnTextOnly;
			this.TokenToQName[14] = this.QnOrder;
			this.TokenToQName[15] = this.QnSeq;
			this.TokenToQName[16] = this.QnOne;
			this.TokenToQName[17] = this.QnMany;
			this.TokenToQName[18] = this.QnRequired;
			this.TokenToQName[19] = this.QnYes;
			this.TokenToQName[20] = this.QnNo;
			this.TokenToQName[21] = this.QnString;
			this.TokenToQName[22] = this.QnID;
			this.TokenToQName[23] = this.QnIDRef;
			this.TokenToQName[24] = this.QnIDRefs;
			this.TokenToQName[25] = this.QnEntity;
			this.TokenToQName[26] = this.QnEntities;
			this.TokenToQName[27] = this.QnNmToken;
			this.TokenToQName[28] = this.QnNmTokens;
			this.TokenToQName[29] = this.QnEnumeration;
			this.TokenToQName[30] = this.QnDefault;
			this.TokenToQName[31] = this.QnXdrSchema;
			this.TokenToQName[32] = this.QnXdrElementType;
			this.TokenToQName[33] = this.QnXdrElement;
			this.TokenToQName[34] = this.QnXdrGroup;
			this.TokenToQName[35] = this.QnXdrAttributeType;
			this.TokenToQName[36] = this.QnXdrAttribute;
			this.TokenToQName[37] = this.QnXdrDataType;
			this.TokenToQName[38] = this.QnXdrDescription;
			this.TokenToQName[39] = this.QnXdrExtends;
			this.TokenToQName[40] = this.QnXdrAliasSchema;
			this.TokenToQName[41] = this.QnDtType;
			this.TokenToQName[42] = this.QnDtValues;
			this.TokenToQName[43] = this.QnDtMaxLength;
			this.TokenToQName[44] = this.QnDtMinLength;
			this.TokenToQName[45] = this.QnDtMax;
			this.TokenToQName[46] = this.QnDtMin;
			this.TokenToQName[47] = this.QnDtMinExclusive;
			this.TokenToQName[48] = this.QnDtMaxExclusive;
			this.TokenToQName[49] = this.QnTargetNamespace;
			this.TokenToQName[50] = this.QnVersion;
			this.TokenToQName[51] = this.QnFinalDefault;
			this.TokenToQName[52] = this.QnBlockDefault;
			this.TokenToQName[53] = this.QnFixed;
			this.TokenToQName[54] = this.QnAbstract;
			this.TokenToQName[55] = this.QnBlock;
			this.TokenToQName[56] = this.QnSubstitutionGroup;
			this.TokenToQName[57] = this.QnFinal;
			this.TokenToQName[58] = this.QnNillable;
			this.TokenToQName[59] = this.QnRef;
			this.TokenToQName[60] = this.QnBase;
			this.TokenToQName[61] = this.QnDerivedBy;
			this.TokenToQName[62] = this.QnNamespace;
			this.TokenToQName[63] = this.QnProcessContents;
			this.TokenToQName[64] = this.QnRefer;
			this.TokenToQName[65] = this.QnPublic;
			this.TokenToQName[66] = this.QnSystem;
			this.TokenToQName[67] = this.QnSchemaLocation;
			this.TokenToQName[68] = this.QnValue;
			this.TokenToQName[119] = this.QnItemType;
			this.TokenToQName[120] = this.QnMemberTypes;
			this.TokenToQName[121] = this.QnXPath;
			this.TokenToQName[74] = this.QnXsdSchema;
			this.TokenToQName[75] = this.QnXsdAnnotation;
			this.TokenToQName[76] = this.QnXsdInclude;
			this.TokenToQName[77] = this.QnXsdImport;
			this.TokenToQName[78] = this.QnXsdElement;
			this.TokenToQName[79] = this.QnXsdAttribute;
			this.TokenToQName[80] = this.QnXsdAttributeGroup;
			this.TokenToQName[81] = this.QnXsdAnyAttribute;
			this.TokenToQName[82] = this.QnXsdGroup;
			this.TokenToQName[83] = this.QnXsdAll;
			this.TokenToQName[84] = this.QnXsdChoice;
			this.TokenToQName[85] = this.QnXsdSequence;
			this.TokenToQName[86] = this.QnXsdAny;
			this.TokenToQName[87] = this.QnXsdNotation;
			this.TokenToQName[88] = this.QnXsdSimpleType;
			this.TokenToQName[89] = this.QnXsdComplexType;
			this.TokenToQName[90] = this.QnXsdUnique;
			this.TokenToQName[91] = this.QnXsdKey;
			this.TokenToQName[92] = this.QnXsdKeyRef;
			this.TokenToQName[93] = this.QnXsdSelector;
			this.TokenToQName[94] = this.QnXsdField;
			this.TokenToQName[95] = this.QnXsdMinExclusive;
			this.TokenToQName[96] = this.QnXsdMinInclusive;
			this.TokenToQName[97] = this.QnXsdMaxExclusive;
			this.TokenToQName[98] = this.QnXsdMaxInclusive;
			this.TokenToQName[99] = this.QnXsdTotalDigits;
			this.TokenToQName[100] = this.QnXsdFractionDigits;
			this.TokenToQName[101] = this.QnXsdLength;
			this.TokenToQName[102] = this.QnXsdMinLength;
			this.TokenToQName[103] = this.QnXsdMaxLength;
			this.TokenToQName[104] = this.QnXsdEnumeration;
			this.TokenToQName[105] = this.QnXsdPattern;
			this.TokenToQName[117] = this.QnXsdWhiteSpace;
			this.TokenToQName[106] = this.QnXsdDocumentation;
			this.TokenToQName[107] = this.QnXsdAppinfo;
			this.TokenToQName[108] = this.QnXsdComplexContent;
			this.TokenToQName[110] = this.QnXsdRestriction;
			this.TokenToQName[113] = this.QnXsdRestriction;
			this.TokenToQName[115] = this.QnXsdRestriction;
			this.TokenToQName[109] = this.QnXsdExtension;
			this.TokenToQName[112] = this.QnXsdExtension;
			this.TokenToQName[111] = this.QnXsdSimpleContent;
			this.TokenToQName[116] = this.QnXsdUnion;
			this.TokenToQName[114] = this.QnXsdList;
			this.TokenToQName[118] = this.QnXsdRedefine;
			this.TokenToQName[69] = this.QnSource;
			this.TokenToQName[72] = this.QnUse;
			this.TokenToQName[73] = this.QnForm;
			this.TokenToQName[71] = this.QnElementFormDefault;
			this.TokenToQName[70] = this.QnAttributeFormDefault;
			this.TokenToQName[122] = this.QnXmlLang;
			this.TokenToQName[0] = XmlQualifiedName.Empty;
		}

		// Token: 0x060025B4 RID: 9652 RVA: 0x000D4624 File Offset: 0x000D2824
		public SchemaType SchemaTypeFromRoot(string localName, string ns)
		{
			if (this.IsXSDRoot(localName, ns))
			{
				return SchemaType.XSD;
			}
			if (this.IsXDRRoot(localName, XmlSchemaDatatype.XdrCanonizeUri(ns, this.nameTable, this)))
			{
				return SchemaType.XDR;
			}
			return SchemaType.None;
		}

		// Token: 0x060025B5 RID: 9653 RVA: 0x000D464B File Offset: 0x000D284B
		public bool IsXSDRoot(string localName, string ns)
		{
			return localName == this.XsdSchema && ns == this.NsXs;
		}

		// Token: 0x060025B6 RID: 9654 RVA: 0x000D4669 File Offset: 0x000D2869
		public bool IsXDRRoot(string localName, string ns)
		{
			return localName == this.XdrSchema && ns == this.NsXdr;
		}

		// Token: 0x060025B7 RID: 9655 RVA: 0x000D4687 File Offset: 0x000D2887
		public XmlQualifiedName GetName(SchemaNames.Token token)
		{
			return this.TokenToQName[(int)token];
		}

		// Token: 0x040019FE RID: 6654
		private XmlNameTable nameTable;

		// Token: 0x040019FF RID: 6655
		public string NsDataType;

		// Token: 0x04001A00 RID: 6656
		public string NsDataTypeAlias;

		// Token: 0x04001A01 RID: 6657
		public string NsDataTypeOld;

		// Token: 0x04001A02 RID: 6658
		public string NsXml;

		// Token: 0x04001A03 RID: 6659
		public string NsXmlNs;

		// Token: 0x04001A04 RID: 6660
		public string NsXdr;

		// Token: 0x04001A05 RID: 6661
		public string NsXdrAlias;

		// Token: 0x04001A06 RID: 6662
		public string NsXs;

		// Token: 0x04001A07 RID: 6663
		public string NsXsi;

		// Token: 0x04001A08 RID: 6664
		public string XsiType;

		// Token: 0x04001A09 RID: 6665
		public string XsiNil;

		// Token: 0x04001A0A RID: 6666
		public string XsiSchemaLocation;

		// Token: 0x04001A0B RID: 6667
		public string XsiNoNamespaceSchemaLocation;

		// Token: 0x04001A0C RID: 6668
		public string XsdSchema;

		// Token: 0x04001A0D RID: 6669
		public string XdrSchema;

		// Token: 0x04001A0E RID: 6670
		public XmlQualifiedName QnPCData;

		// Token: 0x04001A0F RID: 6671
		public XmlQualifiedName QnXml;

		// Token: 0x04001A10 RID: 6672
		public XmlQualifiedName QnXmlNs;

		// Token: 0x04001A11 RID: 6673
		public XmlQualifiedName QnDtDt;

		// Token: 0x04001A12 RID: 6674
		public XmlQualifiedName QnXmlLang;

		// Token: 0x04001A13 RID: 6675
		public XmlQualifiedName QnName;

		// Token: 0x04001A14 RID: 6676
		public XmlQualifiedName QnType;

		// Token: 0x04001A15 RID: 6677
		public XmlQualifiedName QnMaxOccurs;

		// Token: 0x04001A16 RID: 6678
		public XmlQualifiedName QnMinOccurs;

		// Token: 0x04001A17 RID: 6679
		public XmlQualifiedName QnInfinite;

		// Token: 0x04001A18 RID: 6680
		public XmlQualifiedName QnModel;

		// Token: 0x04001A19 RID: 6681
		public XmlQualifiedName QnOpen;

		// Token: 0x04001A1A RID: 6682
		public XmlQualifiedName QnClosed;

		// Token: 0x04001A1B RID: 6683
		public XmlQualifiedName QnContent;

		// Token: 0x04001A1C RID: 6684
		public XmlQualifiedName QnMixed;

		// Token: 0x04001A1D RID: 6685
		public XmlQualifiedName QnEmpty;

		// Token: 0x04001A1E RID: 6686
		public XmlQualifiedName QnEltOnly;

		// Token: 0x04001A1F RID: 6687
		public XmlQualifiedName QnTextOnly;

		// Token: 0x04001A20 RID: 6688
		public XmlQualifiedName QnOrder;

		// Token: 0x04001A21 RID: 6689
		public XmlQualifiedName QnSeq;

		// Token: 0x04001A22 RID: 6690
		public XmlQualifiedName QnOne;

		// Token: 0x04001A23 RID: 6691
		public XmlQualifiedName QnMany;

		// Token: 0x04001A24 RID: 6692
		public XmlQualifiedName QnRequired;

		// Token: 0x04001A25 RID: 6693
		public XmlQualifiedName QnYes;

		// Token: 0x04001A26 RID: 6694
		public XmlQualifiedName QnNo;

		// Token: 0x04001A27 RID: 6695
		public XmlQualifiedName QnString;

		// Token: 0x04001A28 RID: 6696
		public XmlQualifiedName QnID;

		// Token: 0x04001A29 RID: 6697
		public XmlQualifiedName QnIDRef;

		// Token: 0x04001A2A RID: 6698
		public XmlQualifiedName QnIDRefs;

		// Token: 0x04001A2B RID: 6699
		public XmlQualifiedName QnEntity;

		// Token: 0x04001A2C RID: 6700
		public XmlQualifiedName QnEntities;

		// Token: 0x04001A2D RID: 6701
		public XmlQualifiedName QnNmToken;

		// Token: 0x04001A2E RID: 6702
		public XmlQualifiedName QnNmTokens;

		// Token: 0x04001A2F RID: 6703
		public XmlQualifiedName QnEnumeration;

		// Token: 0x04001A30 RID: 6704
		public XmlQualifiedName QnDefault;

		// Token: 0x04001A31 RID: 6705
		public XmlQualifiedName QnXdrSchema;

		// Token: 0x04001A32 RID: 6706
		public XmlQualifiedName QnXdrElementType;

		// Token: 0x04001A33 RID: 6707
		public XmlQualifiedName QnXdrElement;

		// Token: 0x04001A34 RID: 6708
		public XmlQualifiedName QnXdrGroup;

		// Token: 0x04001A35 RID: 6709
		public XmlQualifiedName QnXdrAttributeType;

		// Token: 0x04001A36 RID: 6710
		public XmlQualifiedName QnXdrAttribute;

		// Token: 0x04001A37 RID: 6711
		public XmlQualifiedName QnXdrDataType;

		// Token: 0x04001A38 RID: 6712
		public XmlQualifiedName QnXdrDescription;

		// Token: 0x04001A39 RID: 6713
		public XmlQualifiedName QnXdrExtends;

		// Token: 0x04001A3A RID: 6714
		public XmlQualifiedName QnXdrAliasSchema;

		// Token: 0x04001A3B RID: 6715
		public XmlQualifiedName QnDtType;

		// Token: 0x04001A3C RID: 6716
		public XmlQualifiedName QnDtValues;

		// Token: 0x04001A3D RID: 6717
		public XmlQualifiedName QnDtMaxLength;

		// Token: 0x04001A3E RID: 6718
		public XmlQualifiedName QnDtMinLength;

		// Token: 0x04001A3F RID: 6719
		public XmlQualifiedName QnDtMax;

		// Token: 0x04001A40 RID: 6720
		public XmlQualifiedName QnDtMin;

		// Token: 0x04001A41 RID: 6721
		public XmlQualifiedName QnDtMinExclusive;

		// Token: 0x04001A42 RID: 6722
		public XmlQualifiedName QnDtMaxExclusive;

		// Token: 0x04001A43 RID: 6723
		public XmlQualifiedName QnTargetNamespace;

		// Token: 0x04001A44 RID: 6724
		public XmlQualifiedName QnVersion;

		// Token: 0x04001A45 RID: 6725
		public XmlQualifiedName QnFinalDefault;

		// Token: 0x04001A46 RID: 6726
		public XmlQualifiedName QnBlockDefault;

		// Token: 0x04001A47 RID: 6727
		public XmlQualifiedName QnFixed;

		// Token: 0x04001A48 RID: 6728
		public XmlQualifiedName QnAbstract;

		// Token: 0x04001A49 RID: 6729
		public XmlQualifiedName QnBlock;

		// Token: 0x04001A4A RID: 6730
		public XmlQualifiedName QnSubstitutionGroup;

		// Token: 0x04001A4B RID: 6731
		public XmlQualifiedName QnFinal;

		// Token: 0x04001A4C RID: 6732
		public XmlQualifiedName QnNillable;

		// Token: 0x04001A4D RID: 6733
		public XmlQualifiedName QnRef;

		// Token: 0x04001A4E RID: 6734
		public XmlQualifiedName QnBase;

		// Token: 0x04001A4F RID: 6735
		public XmlQualifiedName QnDerivedBy;

		// Token: 0x04001A50 RID: 6736
		public XmlQualifiedName QnNamespace;

		// Token: 0x04001A51 RID: 6737
		public XmlQualifiedName QnProcessContents;

		// Token: 0x04001A52 RID: 6738
		public XmlQualifiedName QnRefer;

		// Token: 0x04001A53 RID: 6739
		public XmlQualifiedName QnPublic;

		// Token: 0x04001A54 RID: 6740
		public XmlQualifiedName QnSystem;

		// Token: 0x04001A55 RID: 6741
		public XmlQualifiedName QnSchemaLocation;

		// Token: 0x04001A56 RID: 6742
		public XmlQualifiedName QnValue;

		// Token: 0x04001A57 RID: 6743
		public XmlQualifiedName QnUse;

		// Token: 0x04001A58 RID: 6744
		public XmlQualifiedName QnForm;

		// Token: 0x04001A59 RID: 6745
		public XmlQualifiedName QnElementFormDefault;

		// Token: 0x04001A5A RID: 6746
		public XmlQualifiedName QnAttributeFormDefault;

		// Token: 0x04001A5B RID: 6747
		public XmlQualifiedName QnItemType;

		// Token: 0x04001A5C RID: 6748
		public XmlQualifiedName QnMemberTypes;

		// Token: 0x04001A5D RID: 6749
		public XmlQualifiedName QnXPath;

		// Token: 0x04001A5E RID: 6750
		public XmlQualifiedName QnXsdSchema;

		// Token: 0x04001A5F RID: 6751
		public XmlQualifiedName QnXsdAnnotation;

		// Token: 0x04001A60 RID: 6752
		public XmlQualifiedName QnXsdInclude;

		// Token: 0x04001A61 RID: 6753
		public XmlQualifiedName QnXsdImport;

		// Token: 0x04001A62 RID: 6754
		public XmlQualifiedName QnXsdElement;

		// Token: 0x04001A63 RID: 6755
		public XmlQualifiedName QnXsdAttribute;

		// Token: 0x04001A64 RID: 6756
		public XmlQualifiedName QnXsdAttributeGroup;

		// Token: 0x04001A65 RID: 6757
		public XmlQualifiedName QnXsdAnyAttribute;

		// Token: 0x04001A66 RID: 6758
		public XmlQualifiedName QnXsdGroup;

		// Token: 0x04001A67 RID: 6759
		public XmlQualifiedName QnXsdAll;

		// Token: 0x04001A68 RID: 6760
		public XmlQualifiedName QnXsdChoice;

		// Token: 0x04001A69 RID: 6761
		public XmlQualifiedName QnXsdSequence;

		// Token: 0x04001A6A RID: 6762
		public XmlQualifiedName QnXsdAny;

		// Token: 0x04001A6B RID: 6763
		public XmlQualifiedName QnXsdNotation;

		// Token: 0x04001A6C RID: 6764
		public XmlQualifiedName QnXsdSimpleType;

		// Token: 0x04001A6D RID: 6765
		public XmlQualifiedName QnXsdComplexType;

		// Token: 0x04001A6E RID: 6766
		public XmlQualifiedName QnXsdUnique;

		// Token: 0x04001A6F RID: 6767
		public XmlQualifiedName QnXsdKey;

		// Token: 0x04001A70 RID: 6768
		public XmlQualifiedName QnXsdKeyRef;

		// Token: 0x04001A71 RID: 6769
		public XmlQualifiedName QnXsdSelector;

		// Token: 0x04001A72 RID: 6770
		public XmlQualifiedName QnXsdField;

		// Token: 0x04001A73 RID: 6771
		public XmlQualifiedName QnXsdMinExclusive;

		// Token: 0x04001A74 RID: 6772
		public XmlQualifiedName QnXsdMinInclusive;

		// Token: 0x04001A75 RID: 6773
		public XmlQualifiedName QnXsdMaxInclusive;

		// Token: 0x04001A76 RID: 6774
		public XmlQualifiedName QnXsdMaxExclusive;

		// Token: 0x04001A77 RID: 6775
		public XmlQualifiedName QnXsdTotalDigits;

		// Token: 0x04001A78 RID: 6776
		public XmlQualifiedName QnXsdFractionDigits;

		// Token: 0x04001A79 RID: 6777
		public XmlQualifiedName QnXsdLength;

		// Token: 0x04001A7A RID: 6778
		public XmlQualifiedName QnXsdMinLength;

		// Token: 0x04001A7B RID: 6779
		public XmlQualifiedName QnXsdMaxLength;

		// Token: 0x04001A7C RID: 6780
		public XmlQualifiedName QnXsdEnumeration;

		// Token: 0x04001A7D RID: 6781
		public XmlQualifiedName QnXsdPattern;

		// Token: 0x04001A7E RID: 6782
		public XmlQualifiedName QnXsdDocumentation;

		// Token: 0x04001A7F RID: 6783
		public XmlQualifiedName QnXsdAppinfo;

		// Token: 0x04001A80 RID: 6784
		public XmlQualifiedName QnSource;

		// Token: 0x04001A81 RID: 6785
		public XmlQualifiedName QnXsdComplexContent;

		// Token: 0x04001A82 RID: 6786
		public XmlQualifiedName QnXsdSimpleContent;

		// Token: 0x04001A83 RID: 6787
		public XmlQualifiedName QnXsdRestriction;

		// Token: 0x04001A84 RID: 6788
		public XmlQualifiedName QnXsdExtension;

		// Token: 0x04001A85 RID: 6789
		public XmlQualifiedName QnXsdUnion;

		// Token: 0x04001A86 RID: 6790
		public XmlQualifiedName QnXsdList;

		// Token: 0x04001A87 RID: 6791
		public XmlQualifiedName QnXsdWhiteSpace;

		// Token: 0x04001A88 RID: 6792
		public XmlQualifiedName QnXsdRedefine;

		// Token: 0x04001A89 RID: 6793
		public XmlQualifiedName QnXsdAnyType;

		// Token: 0x04001A8A RID: 6794
		internal XmlQualifiedName[] TokenToQName = new XmlQualifiedName[123];

		// Token: 0x020003EA RID: 1002
		public enum Token
		{
			// Token: 0x04001A8C RID: 6796
			Empty,
			// Token: 0x04001A8D RID: 6797
			SchemaName,
			// Token: 0x04001A8E RID: 6798
			SchemaType,
			// Token: 0x04001A8F RID: 6799
			SchemaMaxOccurs,
			// Token: 0x04001A90 RID: 6800
			SchemaMinOccurs,
			// Token: 0x04001A91 RID: 6801
			SchemaInfinite,
			// Token: 0x04001A92 RID: 6802
			SchemaModel,
			// Token: 0x04001A93 RID: 6803
			SchemaOpen,
			// Token: 0x04001A94 RID: 6804
			SchemaClosed,
			// Token: 0x04001A95 RID: 6805
			SchemaContent,
			// Token: 0x04001A96 RID: 6806
			SchemaMixed,
			// Token: 0x04001A97 RID: 6807
			SchemaEmpty,
			// Token: 0x04001A98 RID: 6808
			SchemaElementOnly,
			// Token: 0x04001A99 RID: 6809
			SchemaTextOnly,
			// Token: 0x04001A9A RID: 6810
			SchemaOrder,
			// Token: 0x04001A9B RID: 6811
			SchemaSeq,
			// Token: 0x04001A9C RID: 6812
			SchemaOne,
			// Token: 0x04001A9D RID: 6813
			SchemaMany,
			// Token: 0x04001A9E RID: 6814
			SchemaRequired,
			// Token: 0x04001A9F RID: 6815
			SchemaYes,
			// Token: 0x04001AA0 RID: 6816
			SchemaNo,
			// Token: 0x04001AA1 RID: 6817
			SchemaString,
			// Token: 0x04001AA2 RID: 6818
			SchemaId,
			// Token: 0x04001AA3 RID: 6819
			SchemaIdref,
			// Token: 0x04001AA4 RID: 6820
			SchemaIdrefs,
			// Token: 0x04001AA5 RID: 6821
			SchemaEntity,
			// Token: 0x04001AA6 RID: 6822
			SchemaEntities,
			// Token: 0x04001AA7 RID: 6823
			SchemaNmtoken,
			// Token: 0x04001AA8 RID: 6824
			SchemaNmtokens,
			// Token: 0x04001AA9 RID: 6825
			SchemaEnumeration,
			// Token: 0x04001AAA RID: 6826
			SchemaDefault,
			// Token: 0x04001AAB RID: 6827
			XdrRoot,
			// Token: 0x04001AAC RID: 6828
			XdrElementType,
			// Token: 0x04001AAD RID: 6829
			XdrElement,
			// Token: 0x04001AAE RID: 6830
			XdrGroup,
			// Token: 0x04001AAF RID: 6831
			XdrAttributeType,
			// Token: 0x04001AB0 RID: 6832
			XdrAttribute,
			// Token: 0x04001AB1 RID: 6833
			XdrDatatype,
			// Token: 0x04001AB2 RID: 6834
			XdrDescription,
			// Token: 0x04001AB3 RID: 6835
			XdrExtends,
			// Token: 0x04001AB4 RID: 6836
			SchemaXdrRootAlias,
			// Token: 0x04001AB5 RID: 6837
			SchemaDtType,
			// Token: 0x04001AB6 RID: 6838
			SchemaDtValues,
			// Token: 0x04001AB7 RID: 6839
			SchemaDtMaxLength,
			// Token: 0x04001AB8 RID: 6840
			SchemaDtMinLength,
			// Token: 0x04001AB9 RID: 6841
			SchemaDtMax,
			// Token: 0x04001ABA RID: 6842
			SchemaDtMin,
			// Token: 0x04001ABB RID: 6843
			SchemaDtMinExclusive,
			// Token: 0x04001ABC RID: 6844
			SchemaDtMaxExclusive,
			// Token: 0x04001ABD RID: 6845
			SchemaTargetNamespace,
			// Token: 0x04001ABE RID: 6846
			SchemaVersion,
			// Token: 0x04001ABF RID: 6847
			SchemaFinalDefault,
			// Token: 0x04001AC0 RID: 6848
			SchemaBlockDefault,
			// Token: 0x04001AC1 RID: 6849
			SchemaFixed,
			// Token: 0x04001AC2 RID: 6850
			SchemaAbstract,
			// Token: 0x04001AC3 RID: 6851
			SchemaBlock,
			// Token: 0x04001AC4 RID: 6852
			SchemaSubstitutionGroup,
			// Token: 0x04001AC5 RID: 6853
			SchemaFinal,
			// Token: 0x04001AC6 RID: 6854
			SchemaNillable,
			// Token: 0x04001AC7 RID: 6855
			SchemaRef,
			// Token: 0x04001AC8 RID: 6856
			SchemaBase,
			// Token: 0x04001AC9 RID: 6857
			SchemaDerivedBy,
			// Token: 0x04001ACA RID: 6858
			SchemaNamespace,
			// Token: 0x04001ACB RID: 6859
			SchemaProcessContents,
			// Token: 0x04001ACC RID: 6860
			SchemaRefer,
			// Token: 0x04001ACD RID: 6861
			SchemaPublic,
			// Token: 0x04001ACE RID: 6862
			SchemaSystem,
			// Token: 0x04001ACF RID: 6863
			SchemaSchemaLocation,
			// Token: 0x04001AD0 RID: 6864
			SchemaValue,
			// Token: 0x04001AD1 RID: 6865
			SchemaSource,
			// Token: 0x04001AD2 RID: 6866
			SchemaAttributeFormDefault,
			// Token: 0x04001AD3 RID: 6867
			SchemaElementFormDefault,
			// Token: 0x04001AD4 RID: 6868
			SchemaUse,
			// Token: 0x04001AD5 RID: 6869
			SchemaForm,
			// Token: 0x04001AD6 RID: 6870
			XsdSchema,
			// Token: 0x04001AD7 RID: 6871
			XsdAnnotation,
			// Token: 0x04001AD8 RID: 6872
			XsdInclude,
			// Token: 0x04001AD9 RID: 6873
			XsdImport,
			// Token: 0x04001ADA RID: 6874
			XsdElement,
			// Token: 0x04001ADB RID: 6875
			XsdAttribute,
			// Token: 0x04001ADC RID: 6876
			xsdAttributeGroup,
			// Token: 0x04001ADD RID: 6877
			XsdAnyAttribute,
			// Token: 0x04001ADE RID: 6878
			XsdGroup,
			// Token: 0x04001ADF RID: 6879
			XsdAll,
			// Token: 0x04001AE0 RID: 6880
			XsdChoice,
			// Token: 0x04001AE1 RID: 6881
			XsdSequence,
			// Token: 0x04001AE2 RID: 6882
			XsdAny,
			// Token: 0x04001AE3 RID: 6883
			XsdNotation,
			// Token: 0x04001AE4 RID: 6884
			XsdSimpleType,
			// Token: 0x04001AE5 RID: 6885
			XsdComplexType,
			// Token: 0x04001AE6 RID: 6886
			XsdUnique,
			// Token: 0x04001AE7 RID: 6887
			XsdKey,
			// Token: 0x04001AE8 RID: 6888
			XsdKeyref,
			// Token: 0x04001AE9 RID: 6889
			XsdSelector,
			// Token: 0x04001AEA RID: 6890
			XsdField,
			// Token: 0x04001AEB RID: 6891
			XsdMinExclusive,
			// Token: 0x04001AEC RID: 6892
			XsdMinInclusive,
			// Token: 0x04001AED RID: 6893
			XsdMaxExclusive,
			// Token: 0x04001AEE RID: 6894
			XsdMaxInclusive,
			// Token: 0x04001AEF RID: 6895
			XsdTotalDigits,
			// Token: 0x04001AF0 RID: 6896
			XsdFractionDigits,
			// Token: 0x04001AF1 RID: 6897
			XsdLength,
			// Token: 0x04001AF2 RID: 6898
			XsdMinLength,
			// Token: 0x04001AF3 RID: 6899
			XsdMaxLength,
			// Token: 0x04001AF4 RID: 6900
			XsdEnumeration,
			// Token: 0x04001AF5 RID: 6901
			XsdPattern,
			// Token: 0x04001AF6 RID: 6902
			XsdDocumentation,
			// Token: 0x04001AF7 RID: 6903
			XsdAppInfo,
			// Token: 0x04001AF8 RID: 6904
			XsdComplexContent,
			// Token: 0x04001AF9 RID: 6905
			XsdComplexContentExtension,
			// Token: 0x04001AFA RID: 6906
			XsdComplexContentRestriction,
			// Token: 0x04001AFB RID: 6907
			XsdSimpleContent,
			// Token: 0x04001AFC RID: 6908
			XsdSimpleContentExtension,
			// Token: 0x04001AFD RID: 6909
			XsdSimpleContentRestriction,
			// Token: 0x04001AFE RID: 6910
			XsdSimpleTypeList,
			// Token: 0x04001AFF RID: 6911
			XsdSimpleTypeRestriction,
			// Token: 0x04001B00 RID: 6912
			XsdSimpleTypeUnion,
			// Token: 0x04001B01 RID: 6913
			XsdWhitespace,
			// Token: 0x04001B02 RID: 6914
			XsdRedefine,
			// Token: 0x04001B03 RID: 6915
			SchemaItemType,
			// Token: 0x04001B04 RID: 6916
			SchemaMemberTypes,
			// Token: 0x04001B05 RID: 6917
			SchemaXPath,
			// Token: 0x04001B06 RID: 6918
			XmlLang
		}
	}
}
