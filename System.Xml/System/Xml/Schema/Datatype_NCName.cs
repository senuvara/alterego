﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A7 RID: 935
	internal class Datatype_NCName : Datatype_Name
	{
		// Token: 0x1700072B RID: 1835
		// (get) Token: 0x060022F8 RID: 8952 RVA: 0x000BE258 File Offset: 0x000BC458
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.NCName;
			}
		}

		// Token: 0x060022F9 RID: 8953 RVA: 0x000BE25C File Offset: 0x000BC45C
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.stringFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				ex = DatatypeImplementation.stringFacetsChecker.CheckValueFacets(s, this);
				if (ex == null)
				{
					nameTable.Add(s);
					typedValue = s;
					return null;
				}
			}
			return ex;
		}

		// Token: 0x060022FA RID: 8954 RVA: 0x000BE29D File Offset: 0x000BC49D
		public Datatype_NCName()
		{
		}
	}
}
