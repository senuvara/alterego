﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200045A RID: 1114
	internal enum ValidatorState
	{
		// Token: 0x04001D02 RID: 7426
		None,
		// Token: 0x04001D03 RID: 7427
		Start,
		// Token: 0x04001D04 RID: 7428
		TopLevelAttribute,
		// Token: 0x04001D05 RID: 7429
		TopLevelTextOrWS,
		// Token: 0x04001D06 RID: 7430
		Element,
		// Token: 0x04001D07 RID: 7431
		Attribute,
		// Token: 0x04001D08 RID: 7432
		EndOfAttributes,
		// Token: 0x04001D09 RID: 7433
		Text,
		// Token: 0x04001D0A RID: 7434
		Whitespace,
		// Token: 0x04001D0B RID: 7435
		EndElement,
		// Token: 0x04001D0C RID: 7436
		SkipToEndElement,
		// Token: 0x04001D0D RID: 7437
		Finish
	}
}
