﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000351 RID: 849
	internal class AxisElement
	{
		// Token: 0x17000657 RID: 1623
		// (get) Token: 0x060020B4 RID: 8372 RVA: 0x000B71E3 File Offset: 0x000B53E3
		internal DoubleLinkAxis CurNode
		{
			get
			{
				return this.curNode;
			}
		}

		// Token: 0x060020B5 RID: 8373 RVA: 0x000B71EC File Offset: 0x000B53EC
		internal AxisElement(DoubleLinkAxis node, int depth)
		{
			this.curNode = node;
			this.curDepth = depth;
			this.rootDepth = depth;
			this.isMatch = false;
		}

		// Token: 0x060020B6 RID: 8374 RVA: 0x000B7220 File Offset: 0x000B5420
		internal void SetDepth(int depth)
		{
			this.curDepth = depth;
			this.rootDepth = depth;
		}

		// Token: 0x060020B7 RID: 8375 RVA: 0x000B7240 File Offset: 0x000B5440
		internal void MoveToParent(int depth, ForwardAxis parent)
		{
			if (depth != this.curDepth - 1)
			{
				if (depth == this.curDepth && this.isMatch)
				{
					this.isMatch = false;
				}
				return;
			}
			if (this.curNode.Input == parent.RootNode && parent.IsDss)
			{
				this.curNode = parent.RootNode;
				this.rootDepth = (this.curDepth = -1);
				return;
			}
			if (this.curNode.Input != null)
			{
				this.curNode = (DoubleLinkAxis)this.curNode.Input;
				this.curDepth--;
				return;
			}
		}

		// Token: 0x060020B8 RID: 8376 RVA: 0x000B72DC File Offset: 0x000B54DC
		internal bool MoveToChild(string name, string URN, int depth, ForwardAxis parent)
		{
			if (Asttree.IsAttribute(this.curNode))
			{
				return false;
			}
			if (this.isMatch)
			{
				this.isMatch = false;
			}
			if (!AxisStack.Equal(this.curNode.Name, this.curNode.Urn, name, URN))
			{
				return false;
			}
			if (this.curDepth == -1)
			{
				this.SetDepth(depth);
			}
			else if (depth > this.curDepth)
			{
				return false;
			}
			if (this.curNode == parent.TopNode)
			{
				this.isMatch = true;
				return true;
			}
			DoubleLinkAxis ast = (DoubleLinkAxis)this.curNode.Next;
			if (Asttree.IsAttribute(ast))
			{
				this.isMatch = true;
				return false;
			}
			this.curNode = ast;
			this.curDepth++;
			return false;
		}

		// Token: 0x04001788 RID: 6024
		internal DoubleLinkAxis curNode;

		// Token: 0x04001789 RID: 6025
		internal int rootDepth;

		// Token: 0x0400178A RID: 6026
		internal int curDepth;

		// Token: 0x0400178B RID: 6027
		internal bool isMatch;
	}
}
