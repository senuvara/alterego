﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200038E RID: 910
	internal class Datatype_dayTimeDuration : Datatype_duration
	{
		// Token: 0x06002296 RID: 8854 RVA: 0x000BDD3C File Offset: 0x000BBF3C
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			if (s == null || s.Length == 0)
			{
				return new XmlSchemaException("The attribute value cannot be empty.", string.Empty);
			}
			Exception ex = DatatypeImplementation.durationFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				XsdDuration xsdDuration;
				ex = XsdDuration.TryParse(s, XsdDuration.DurationType.DayTimeDuration, out xsdDuration);
				if (ex == null)
				{
					TimeSpan timeSpan;
					ex = xsdDuration.TryToTimeSpan(XsdDuration.DurationType.DayTimeDuration, out timeSpan);
					if (ex == null)
					{
						ex = DatatypeImplementation.durationFacetsChecker.CheckValueFacets(timeSpan, this);
						if (ex == null)
						{
							typedValue = timeSpan;
							return null;
						}
					}
				}
			}
			return ex;
		}

		// Token: 0x170006F7 RID: 1783
		// (get) Token: 0x06002297 RID: 8855 RVA: 0x000BDDB0 File Offset: 0x000BBFB0
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.DayTimeDuration;
			}
		}

		// Token: 0x06002298 RID: 8856 RVA: 0x000BDD34 File Offset: 0x000BBF34
		public Datatype_dayTimeDuration()
		{
		}
	}
}
