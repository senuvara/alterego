﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003AD RID: 941
	internal class Datatype_nonPositiveInteger : Datatype_integer
	{
		// Token: 0x1700073A RID: 1850
		// (get) Token: 0x06002313 RID: 8979 RVA: 0x000BE450 File Offset: 0x000BC650
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_nonPositiveInteger.numeric10FacetsChecker;
			}
		}

		// Token: 0x1700073B RID: 1851
		// (get) Token: 0x06002314 RID: 8980 RVA: 0x000BE457 File Offset: 0x000BC657
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.NonPositiveInteger;
			}
		}

		// Token: 0x1700073C RID: 1852
		// (get) Token: 0x06002315 RID: 8981 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool HasValueFacets
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06002316 RID: 8982 RVA: 0x000BE45B File Offset: 0x000BC65B
		public Datatype_nonPositiveInteger()
		{
		}

		// Token: 0x06002317 RID: 8983 RVA: 0x000BE463 File Offset: 0x000BC663
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_nonPositiveInteger()
		{
		}

		// Token: 0x040018BE RID: 6334
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(decimal.MinValue, 0m);
	}
}
