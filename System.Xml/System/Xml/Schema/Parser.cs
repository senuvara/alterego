﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml.XmlConfiguration;

namespace System.Xml.Schema
{
	// Token: 0x020003D6 RID: 982
	internal sealed class Parser
	{
		// Token: 0x06002445 RID: 9285 RVA: 0x000C6684 File Offset: 0x000C4884
		public Parser(SchemaType schemaType, XmlNameTable nameTable, SchemaNames schemaNames, ValidationEventHandler eventHandler)
		{
			this.schemaType = schemaType;
			this.nameTable = nameTable;
			this.schemaNames = schemaNames;
			this.eventHandler = eventHandler;
			this.xmlResolver = XmlReaderSection.CreateDefaultResolver();
			this.processMarkup = true;
			this.dummyDocument = new XmlDocument();
		}

		// Token: 0x06002446 RID: 9286 RVA: 0x000C66DC File Offset: 0x000C48DC
		public SchemaType Parse(XmlReader reader, string targetNamespace)
		{
			this.StartParsing(reader, targetNamespace);
			while (this.ParseReaderNode() && reader.Read())
			{
			}
			return this.FinishParsing();
		}

		// Token: 0x06002447 RID: 9287 RVA: 0x000C66FC File Offset: 0x000C48FC
		public void StartParsing(XmlReader reader, string targetNamespace)
		{
			this.reader = reader;
			this.positionInfo = PositionInfo.GetPositionInfo(reader);
			this.namespaceManager = reader.NamespaceManager;
			if (this.namespaceManager == null)
			{
				this.namespaceManager = new XmlNamespaceManager(this.nameTable);
				this.isProcessNamespaces = true;
			}
			else
			{
				this.isProcessNamespaces = false;
			}
			while (reader.NodeType != XmlNodeType.Element && reader.Read())
			{
			}
			this.markupDepth = int.MaxValue;
			this.schemaXmlDepth = reader.Depth;
			SchemaType rootType = this.schemaNames.SchemaTypeFromRoot(reader.LocalName, reader.NamespaceURI);
			string res;
			if (!this.CheckSchemaRoot(rootType, out res))
			{
				throw new XmlSchemaException(res, reader.BaseURI, this.positionInfo.LineNumber, this.positionInfo.LinePosition);
			}
			if (this.schemaType == SchemaType.XSD)
			{
				this.schema = new XmlSchema();
				this.schema.BaseUri = new Uri(reader.BaseURI, UriKind.RelativeOrAbsolute);
				this.builder = new XsdBuilder(reader, this.namespaceManager, this.schema, this.nameTable, this.schemaNames, this.eventHandler);
				return;
			}
			this.xdrSchema = new SchemaInfo();
			this.xdrSchema.SchemaType = SchemaType.XDR;
			this.builder = new XdrBuilder(reader, this.namespaceManager, this.xdrSchema, targetNamespace, this.nameTable, this.schemaNames, this.eventHandler);
			((XdrBuilder)this.builder).XmlResolver = this.xmlResolver;
		}

		// Token: 0x06002448 RID: 9288 RVA: 0x000C6870 File Offset: 0x000C4A70
		private bool CheckSchemaRoot(SchemaType rootType, out string code)
		{
			code = null;
			if (this.schemaType == SchemaType.None)
			{
				this.schemaType = rootType;
			}
			switch (rootType)
			{
			case SchemaType.None:
			case SchemaType.DTD:
				code = "Expected schema root. Make sure the root element is <schema> and the namespace is 'http://www.w3.org/2001/XMLSchema' for an XSD schema or 'urn:schemas-microsoft-com:xml-data' for an XDR schema.";
				if (this.schemaType == SchemaType.XSD)
				{
					code = "The root element of a W3C XML Schema should be <schema> and its namespace should be 'http://www.w3.org/2001/XMLSchema'.";
				}
				return false;
			case SchemaType.XDR:
				if (this.schemaType == SchemaType.XSD)
				{
					code = "'XmlSchemaSet' can load only W3C XML Schemas.";
					return false;
				}
				if (this.schemaType != SchemaType.XDR)
				{
					code = "Different schema types cannot be mixed.";
					return false;
				}
				break;
			case SchemaType.XSD:
				if (this.schemaType != SchemaType.XSD)
				{
					code = "Different schema types cannot be mixed.";
					return false;
				}
				break;
			}
			return true;
		}

		// Token: 0x06002449 RID: 9289 RVA: 0x000C68F7 File Offset: 0x000C4AF7
		public SchemaType FinishParsing()
		{
			return this.schemaType;
		}

		// Token: 0x1700077D RID: 1917
		// (get) Token: 0x0600244A RID: 9290 RVA: 0x000C68FF File Offset: 0x000C4AFF
		public XmlSchema XmlSchema
		{
			get
			{
				return this.schema;
			}
		}

		// Token: 0x1700077E RID: 1918
		// (set) Token: 0x0600244B RID: 9291 RVA: 0x000C6907 File Offset: 0x000C4B07
		internal XmlResolver XmlResolver
		{
			set
			{
				this.xmlResolver = value;
			}
		}

		// Token: 0x1700077F RID: 1919
		// (get) Token: 0x0600244C RID: 9292 RVA: 0x000C6910 File Offset: 0x000C4B10
		public SchemaInfo XdrSchema
		{
			get
			{
				return this.xdrSchema;
			}
		}

		// Token: 0x0600244D RID: 9293 RVA: 0x000C6918 File Offset: 0x000C4B18
		public bool ParseReaderNode()
		{
			if (this.reader.Depth > this.markupDepth)
			{
				if (this.processMarkup)
				{
					this.ProcessAppInfoDocMarkup(false);
				}
				return true;
			}
			if (this.reader.NodeType == XmlNodeType.Element)
			{
				if (this.builder.ProcessElement(this.reader.Prefix, this.reader.LocalName, this.reader.NamespaceURI))
				{
					this.namespaceManager.PushScope();
					if (this.reader.MoveToFirstAttribute())
					{
						do
						{
							this.builder.ProcessAttribute(this.reader.Prefix, this.reader.LocalName, this.reader.NamespaceURI, this.reader.Value);
							if (Ref.Equal(this.reader.NamespaceURI, this.schemaNames.NsXmlNs) && this.isProcessNamespaces)
							{
								this.namespaceManager.AddNamespace((this.reader.Prefix.Length == 0) ? string.Empty : this.reader.LocalName, this.reader.Value);
							}
						}
						while (this.reader.MoveToNextAttribute());
						this.reader.MoveToElement();
					}
					this.builder.StartChildren();
					if (this.reader.IsEmptyElement)
					{
						this.namespaceManager.PopScope();
						this.builder.EndChildren();
						if (this.reader.Depth == this.schemaXmlDepth)
						{
							return false;
						}
					}
					else if (!this.builder.IsContentParsed())
					{
						this.markupDepth = this.reader.Depth;
						this.processMarkup = true;
						if (this.annotationNSManager == null)
						{
							this.annotationNSManager = new XmlNamespaceManager(this.nameTable);
							this.xmlns = this.nameTable.Add("xmlns");
						}
						this.ProcessAppInfoDocMarkup(true);
					}
				}
				else if (!this.reader.IsEmptyElement)
				{
					this.markupDepth = this.reader.Depth;
					this.processMarkup = false;
				}
			}
			else if (this.reader.NodeType == XmlNodeType.Text)
			{
				if (!this.xmlCharType.IsOnlyWhitespace(this.reader.Value))
				{
					this.builder.ProcessCData(this.reader.Value);
				}
			}
			else if (this.reader.NodeType == XmlNodeType.EntityReference || this.reader.NodeType == XmlNodeType.SignificantWhitespace || this.reader.NodeType == XmlNodeType.CDATA)
			{
				this.builder.ProcessCData(this.reader.Value);
			}
			else if (this.reader.NodeType == XmlNodeType.EndElement)
			{
				if (this.reader.Depth == this.markupDepth)
				{
					if (this.processMarkup)
					{
						XmlNodeList childNodes = this.parentNode.ChildNodes;
						XmlNode[] array = new XmlNode[childNodes.Count];
						for (int i = 0; i < childNodes.Count; i++)
						{
							array[i] = childNodes[i];
						}
						this.builder.ProcessMarkup(array);
						this.namespaceManager.PopScope();
						this.builder.EndChildren();
					}
					this.markupDepth = int.MaxValue;
				}
				else
				{
					this.namespaceManager.PopScope();
					this.builder.EndChildren();
				}
				if (this.reader.Depth == this.schemaXmlDepth)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600244E RID: 9294 RVA: 0x000C6C78 File Offset: 0x000C4E78
		private void ProcessAppInfoDocMarkup(bool root)
		{
			XmlNode newChild = null;
			switch (this.reader.NodeType)
			{
			case XmlNodeType.Element:
				this.annotationNSManager.PushScope();
				newChild = this.LoadElementNode(root);
				return;
			case XmlNodeType.Text:
				newChild = this.dummyDocument.CreateTextNode(this.reader.Value);
				break;
			case XmlNodeType.CDATA:
				newChild = this.dummyDocument.CreateCDataSection(this.reader.Value);
				break;
			case XmlNodeType.EntityReference:
				newChild = this.dummyDocument.CreateEntityReference(this.reader.Name);
				break;
			case XmlNodeType.ProcessingInstruction:
				newChild = this.dummyDocument.CreateProcessingInstruction(this.reader.Name, this.reader.Value);
				break;
			case XmlNodeType.Comment:
				newChild = this.dummyDocument.CreateComment(this.reader.Value);
				break;
			case XmlNodeType.Whitespace:
			case XmlNodeType.EndEntity:
				return;
			case XmlNodeType.SignificantWhitespace:
				newChild = this.dummyDocument.CreateSignificantWhitespace(this.reader.Value);
				break;
			case XmlNodeType.EndElement:
				this.annotationNSManager.PopScope();
				this.parentNode = this.parentNode.ParentNode;
				return;
			}
			this.parentNode.AppendChild(newChild);
		}

		// Token: 0x0600244F RID: 9295 RVA: 0x000C6DC8 File Offset: 0x000C4FC8
		private XmlElement LoadElementNode(bool root)
		{
			XmlReader xmlReader = this.reader;
			bool isEmptyElement = xmlReader.IsEmptyElement;
			XmlElement xmlElement = this.dummyDocument.CreateElement(xmlReader.Prefix, xmlReader.LocalName, xmlReader.NamespaceURI);
			xmlElement.IsEmpty = isEmptyElement;
			if (root)
			{
				this.parentNode = xmlElement;
			}
			else
			{
				XmlAttributeCollection attributes = xmlElement.Attributes;
				if (xmlReader.MoveToFirstAttribute())
				{
					do
					{
						if (Ref.Equal(xmlReader.NamespaceURI, this.schemaNames.NsXmlNs))
						{
							this.annotationNSManager.AddNamespace((xmlReader.Prefix.Length == 0) ? string.Empty : this.reader.LocalName, this.reader.Value);
						}
						XmlAttribute node = this.LoadAttributeNode();
						attributes.Append(node);
					}
					while (xmlReader.MoveToNextAttribute());
				}
				xmlReader.MoveToElement();
				string text = this.annotationNSManager.LookupNamespace(xmlReader.Prefix);
				if (text == null)
				{
					XmlAttribute node2 = this.CreateXmlNsAttribute(xmlReader.Prefix, this.namespaceManager.LookupNamespace(xmlReader.Prefix));
					attributes.Append(node2);
				}
				else if (text.Length == 0)
				{
					string text2 = this.namespaceManager.LookupNamespace(xmlReader.Prefix);
					if (text2 != string.Empty)
					{
						XmlAttribute node3 = this.CreateXmlNsAttribute(xmlReader.Prefix, text2);
						attributes.Append(node3);
					}
				}
				while (xmlReader.MoveToNextAttribute())
				{
					if (xmlReader.Prefix.Length != 0 && this.annotationNSManager.LookupNamespace(xmlReader.Prefix) == null)
					{
						XmlAttribute node4 = this.CreateXmlNsAttribute(xmlReader.Prefix, this.namespaceManager.LookupNamespace(xmlReader.Prefix));
						attributes.Append(node4);
					}
				}
				xmlReader.MoveToElement();
				this.parentNode.AppendChild(xmlElement);
				if (!xmlReader.IsEmptyElement)
				{
					this.parentNode = xmlElement;
				}
			}
			return xmlElement;
		}

		// Token: 0x06002450 RID: 9296 RVA: 0x000C6F94 File Offset: 0x000C5194
		private XmlAttribute CreateXmlNsAttribute(string prefix, string value)
		{
			XmlAttribute xmlAttribute;
			if (prefix.Length == 0)
			{
				xmlAttribute = this.dummyDocument.CreateAttribute(string.Empty, this.xmlns, "http://www.w3.org/2000/xmlns/");
			}
			else
			{
				xmlAttribute = this.dummyDocument.CreateAttribute(this.xmlns, prefix, "http://www.w3.org/2000/xmlns/");
			}
			xmlAttribute.AppendChild(this.dummyDocument.CreateTextNode(value));
			this.annotationNSManager.AddNamespace(prefix, value);
			return xmlAttribute;
		}

		// Token: 0x06002451 RID: 9297 RVA: 0x000C7000 File Offset: 0x000C5200
		private XmlAttribute LoadAttributeNode()
		{
			XmlReader xmlReader = this.reader;
			XmlAttribute xmlAttribute = this.dummyDocument.CreateAttribute(xmlReader.Prefix, xmlReader.LocalName, xmlReader.NamespaceURI);
			while (xmlReader.ReadAttributeValue())
			{
				XmlNodeType nodeType = xmlReader.NodeType;
				if (nodeType != XmlNodeType.Text)
				{
					if (nodeType != XmlNodeType.EntityReference)
					{
						throw XmlLoader.UnexpectedNodeType(xmlReader.NodeType);
					}
					xmlAttribute.AppendChild(this.LoadEntityReferenceInAttribute());
				}
				else
				{
					xmlAttribute.AppendChild(this.dummyDocument.CreateTextNode(xmlReader.Value));
				}
			}
			return xmlAttribute;
		}

		// Token: 0x06002452 RID: 9298 RVA: 0x000C7084 File Offset: 0x000C5284
		private XmlEntityReference LoadEntityReferenceInAttribute()
		{
			XmlEntityReference xmlEntityReference = this.dummyDocument.CreateEntityReference(this.reader.LocalName);
			if (!this.reader.CanResolveEntity)
			{
				return xmlEntityReference;
			}
			this.reader.ResolveEntity();
			while (this.reader.ReadAttributeValue())
			{
				XmlNodeType nodeType = this.reader.NodeType;
				if (nodeType != XmlNodeType.Text)
				{
					if (nodeType != XmlNodeType.EntityReference)
					{
						if (nodeType != XmlNodeType.EndEntity)
						{
							throw XmlLoader.UnexpectedNodeType(this.reader.NodeType);
						}
						if (xmlEntityReference.ChildNodes.Count == 0)
						{
							xmlEntityReference.AppendChild(this.dummyDocument.CreateTextNode(string.Empty));
						}
						return xmlEntityReference;
					}
					else
					{
						xmlEntityReference.AppendChild(this.LoadEntityReferenceInAttribute());
					}
				}
				else
				{
					xmlEntityReference.AppendChild(this.dummyDocument.CreateTextNode(this.reader.Value));
				}
			}
			return xmlEntityReference;
		}

		// Token: 0x06002453 RID: 9299 RVA: 0x000C7158 File Offset: 0x000C5358
		public async Task<SchemaType> ParseAsync(XmlReader reader, string targetNamespace)
		{
			await this.StartParsingAsync(reader, targetNamespace).ConfigureAwait(false);
			bool flag;
			do
			{
				flag = this.ParseReaderNode();
				if (flag)
				{
					flag = await reader.ReadAsync().ConfigureAwait(false);
				}
			}
			while (flag);
			return this.FinishParsing();
		}

		// Token: 0x06002454 RID: 9300 RVA: 0x000C71B0 File Offset: 0x000C53B0
		public async Task StartParsingAsync(XmlReader reader, string targetNamespace)
		{
			this.reader = reader;
			this.positionInfo = PositionInfo.GetPositionInfo(reader);
			this.namespaceManager = reader.NamespaceManager;
			if (this.namespaceManager == null)
			{
				this.namespaceManager = new XmlNamespaceManager(this.nameTable);
				this.isProcessNamespaces = true;
			}
			else
			{
				this.isProcessNamespaces = false;
			}
			bool flag;
			do
			{
				flag = (reader.NodeType != XmlNodeType.Element);
				if (flag)
				{
					flag = await reader.ReadAsync().ConfigureAwait(false);
				}
			}
			while (flag);
			this.markupDepth = int.MaxValue;
			this.schemaXmlDepth = reader.Depth;
			SchemaType rootType = this.schemaNames.SchemaTypeFromRoot(reader.LocalName, reader.NamespaceURI);
			string res;
			if (!this.CheckSchemaRoot(rootType, out res))
			{
				throw new XmlSchemaException(res, reader.BaseURI, this.positionInfo.LineNumber, this.positionInfo.LinePosition);
			}
			if (this.schemaType == SchemaType.XSD)
			{
				this.schema = new XmlSchema();
				this.schema.BaseUri = new Uri(reader.BaseURI, UriKind.RelativeOrAbsolute);
				this.builder = new XsdBuilder(reader, this.namespaceManager, this.schema, this.nameTable, this.schemaNames, this.eventHandler);
			}
			else
			{
				this.xdrSchema = new SchemaInfo();
				this.xdrSchema.SchemaType = SchemaType.XDR;
				this.builder = new XdrBuilder(reader, this.namespaceManager, this.xdrSchema, targetNamespace, this.nameTable, this.schemaNames, this.eventHandler);
				((XdrBuilder)this.builder).XmlResolver = this.xmlResolver;
			}
		}

		// Token: 0x04001949 RID: 6473
		private SchemaType schemaType;

		// Token: 0x0400194A RID: 6474
		private XmlNameTable nameTable;

		// Token: 0x0400194B RID: 6475
		private SchemaNames schemaNames;

		// Token: 0x0400194C RID: 6476
		private ValidationEventHandler eventHandler;

		// Token: 0x0400194D RID: 6477
		private XmlNamespaceManager namespaceManager;

		// Token: 0x0400194E RID: 6478
		private XmlReader reader;

		// Token: 0x0400194F RID: 6479
		private PositionInfo positionInfo;

		// Token: 0x04001950 RID: 6480
		private bool isProcessNamespaces;

		// Token: 0x04001951 RID: 6481
		private int schemaXmlDepth;

		// Token: 0x04001952 RID: 6482
		private int markupDepth;

		// Token: 0x04001953 RID: 6483
		private SchemaBuilder builder;

		// Token: 0x04001954 RID: 6484
		private XmlSchema schema;

		// Token: 0x04001955 RID: 6485
		private SchemaInfo xdrSchema;

		// Token: 0x04001956 RID: 6486
		private XmlResolver xmlResolver;

		// Token: 0x04001957 RID: 6487
		private XmlDocument dummyDocument;

		// Token: 0x04001958 RID: 6488
		private bool processMarkup;

		// Token: 0x04001959 RID: 6489
		private XmlNode parentNode;

		// Token: 0x0400195A RID: 6490
		private XmlNamespaceManager annotationNSManager;

		// Token: 0x0400195B RID: 6491
		private string xmlns;

		// Token: 0x0400195C RID: 6492
		private XmlCharType xmlCharType = XmlCharType.Instance;

		// Token: 0x020003D7 RID: 983
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ParseAsync>d__37 : IAsyncStateMachine
		{
			// Token: 0x06002455 RID: 9301 RVA: 0x000C7208 File Offset: 0x000C5408
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				Parser parser = this;
				SchemaType result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_F8;
						}
						configuredTaskAwaiter3 = parser.StartParsingAsync(reader, targetNamespace).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, Parser.<ParseAsync>d__37>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter3.GetResult();
					IL_89:
					bool flag = parser.ParseReaderNode();
					if (!flag)
					{
						goto IL_101;
					}
					configuredTaskAwaiter = reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, Parser.<ParseAsync>d__37>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_F8:
					flag = configuredTaskAwaiter.GetResult();
					IL_101:
					if (flag)
					{
						goto IL_89;
					}
					result = parser.FinishParsing();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06002456 RID: 9302 RVA: 0x000C736C File Offset: 0x000C556C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400195D RID: 6493
			public int <>1__state;

			// Token: 0x0400195E RID: 6494
			public AsyncTaskMethodBuilder<SchemaType> <>t__builder;

			// Token: 0x0400195F RID: 6495
			public Parser <>4__this;

			// Token: 0x04001960 RID: 6496
			public XmlReader reader;

			// Token: 0x04001961 RID: 6497
			public string targetNamespace;

			// Token: 0x04001962 RID: 6498
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04001963 RID: 6499
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020003D8 RID: 984
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <StartParsingAsync>d__38 : IAsyncStateMachine
		{
			// Token: 0x06002457 RID: 9303 RVA: 0x000C737C File Offset: 0x000C557C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				Parser parser = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_E8;
					}
					parser.reader = reader;
					parser.positionInfo = PositionInfo.GetPositionInfo(reader);
					parser.namespaceManager = reader.NamespaceManager;
					if (parser.namespaceManager == null)
					{
						parser.namespaceManager = new XmlNamespaceManager(parser.nameTable);
						parser.isProcessNamespaces = true;
					}
					else
					{
						parser.isProcessNamespaces = false;
					}
					IL_6B:
					bool flag = reader.NodeType != XmlNodeType.Element;
					if (!flag)
					{
						goto IL_F1;
					}
					configuredTaskAwaiter = reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, Parser.<StartParsingAsync>d__38>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_E8:
					flag = configuredTaskAwaiter.GetResult();
					IL_F1:
					if (flag)
					{
						goto IL_6B;
					}
					parser.markupDepth = int.MaxValue;
					parser.schemaXmlDepth = reader.Depth;
					SchemaType rootType = parser.schemaNames.SchemaTypeFromRoot(reader.LocalName, reader.NamespaceURI);
					string res;
					if (!parser.CheckSchemaRoot(rootType, out res))
					{
						throw new XmlSchemaException(res, reader.BaseURI, parser.positionInfo.LineNumber, parser.positionInfo.LinePosition);
					}
					if (parser.schemaType == SchemaType.XSD)
					{
						parser.schema = new XmlSchema();
						parser.schema.BaseUri = new Uri(reader.BaseURI, UriKind.RelativeOrAbsolute);
						parser.builder = new XsdBuilder(reader, parser.namespaceManager, parser.schema, parser.nameTable, parser.schemaNames, parser.eventHandler);
					}
					else
					{
						parser.xdrSchema = new SchemaInfo();
						parser.xdrSchema.SchemaType = SchemaType.XDR;
						parser.builder = new XdrBuilder(reader, parser.namespaceManager, parser.xdrSchema, targetNamespace, parser.nameTable, parser.schemaNames, parser.eventHandler);
						((XdrBuilder)parser.builder).XmlResolver = parser.xmlResolver;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06002458 RID: 9304 RVA: 0x000C7600 File Offset: 0x000C5800
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04001964 RID: 6500
			public int <>1__state;

			// Token: 0x04001965 RID: 6501
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04001966 RID: 6502
			public Parser <>4__this;

			// Token: 0x04001967 RID: 6503
			public XmlReader reader;

			// Token: 0x04001968 RID: 6504
			public string targetNamespace;

			// Token: 0x04001969 RID: 6505
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
