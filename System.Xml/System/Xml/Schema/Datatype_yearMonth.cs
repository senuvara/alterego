﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000397 RID: 919
	internal class Datatype_yearMonth : Datatype_dateTimeBase
	{
		// Token: 0x17000700 RID: 1792
		// (get) Token: 0x060022AE RID: 8878 RVA: 0x000BDF3A File Offset: 0x000BC13A
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.GYearMonth;
			}
		}

		// Token: 0x060022AF RID: 8879 RVA: 0x000BDF3E File Offset: 0x000BC13E
		internal Datatype_yearMonth() : base(XsdDateTimeFlags.GYearMonth)
		{
		}
	}
}
