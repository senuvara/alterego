﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="restriction" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class is for complex types with a complex content model derived by restriction. It restricts the contents of the complex type to a subset of the inherited complex type.</summary>
	// Token: 0x02000413 RID: 1043
	public class XmlSchemaComplexContentRestriction : XmlSchemaContent
	{
		/// <summary>Gets or sets the name of a complex type from which this type is derived by restriction.</summary>
		/// <returns>The name of the complex type from which this type is derived by restriction.</returns>
		// Token: 0x1700085E RID: 2142
		// (get) Token: 0x060027B0 RID: 10160 RVA: 0x000DEF3E File Offset: 0x000DD13E
		// (set) Token: 0x060027B1 RID: 10161 RVA: 0x000DEF46 File Offset: 0x000DD146
		[XmlAttribute("base")]
		public XmlQualifiedName BaseTypeName
		{
			get
			{
				return this.baseTypeName;
			}
			set
			{
				this.baseTypeName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets or sets one of the <see cref="T:System.Xml.Schema.XmlSchemaGroupRef" />, <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, <see cref="T:System.Xml.Schema.XmlSchemaAll" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" /> classes.</summary>
		/// <returns>One of the <see cref="T:System.Xml.Schema.XmlSchemaGroupRef" />, <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, <see cref="T:System.Xml.Schema.XmlSchemaAll" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" /> classes.</returns>
		// Token: 0x1700085F RID: 2143
		// (get) Token: 0x060027B2 RID: 10162 RVA: 0x000DEF5F File Offset: 0x000DD15F
		// (set) Token: 0x060027B3 RID: 10163 RVA: 0x000DEF67 File Offset: 0x000DD167
		[XmlElement("choice", typeof(XmlSchemaChoice))]
		[XmlElement("group", typeof(XmlSchemaGroupRef))]
		[XmlElement("sequence", typeof(XmlSchemaSequence))]
		[XmlElement("all", typeof(XmlSchemaAll))]
		public XmlSchemaParticle Particle
		{
			get
			{
				return this.particle;
			}
			set
			{
				this.particle = value;
			}
		}

		/// <summary>Gets the collection of attributes for the complex type. Contains the <see cref="T:System.Xml.Schema.XmlSchemaAttribute" /> and <see cref="T:System.Xml.Schema.XmlSchemaAttributeGroupRef" /> elements.</summary>
		/// <returns>The collection of attributes for the complex type.</returns>
		// Token: 0x17000860 RID: 2144
		// (get) Token: 0x060027B4 RID: 10164 RVA: 0x000DEF70 File Offset: 0x000DD170
		[XmlElement("attribute", typeof(XmlSchemaAttribute))]
		[XmlElement("attributeGroup", typeof(XmlSchemaAttributeGroupRef))]
		public XmlSchemaObjectCollection Attributes
		{
			get
			{
				return this.attributes;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaAnyAttribute" /> component of the complex content model.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaAnyAttribute" /> component of the complex content model.</returns>
		// Token: 0x17000861 RID: 2145
		// (get) Token: 0x060027B5 RID: 10165 RVA: 0x000DEF78 File Offset: 0x000DD178
		// (set) Token: 0x060027B6 RID: 10166 RVA: 0x000DEF80 File Offset: 0x000DD180
		[XmlElement("anyAttribute")]
		public XmlSchemaAnyAttribute AnyAttribute
		{
			get
			{
				return this.anyAttribute;
			}
			set
			{
				this.anyAttribute = value;
			}
		}

		// Token: 0x060027B7 RID: 10167 RVA: 0x000DEF89 File Offset: 0x000DD189
		internal void SetAttributes(XmlSchemaObjectCollection newAttributes)
		{
			this.attributes = newAttributes;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaComplexContentRestriction" /> class.</summary>
		// Token: 0x060027B8 RID: 10168 RVA: 0x000DEF92 File Offset: 0x000DD192
		public XmlSchemaComplexContentRestriction()
		{
		}

		// Token: 0x04001C17 RID: 7191
		private XmlSchemaParticle particle;

		// Token: 0x04001C18 RID: 7192
		private XmlSchemaObjectCollection attributes = new XmlSchemaObjectCollection();

		// Token: 0x04001C19 RID: 7193
		private XmlSchemaAnyAttribute anyAttribute;

		// Token: 0x04001C1A RID: 7194
		private XmlQualifiedName baseTypeName = XmlQualifiedName.Empty;
	}
}
