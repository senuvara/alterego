﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x02000353 RID: 851
	internal class ActiveAxis
	{
		// Token: 0x1700065A RID: 1626
		// (get) Token: 0x060020C2 RID: 8386 RVA: 0x000B7626 File Offset: 0x000B5826
		public int CurrentDepth
		{
			get
			{
				return this.currentDepth;
			}
		}

		// Token: 0x060020C3 RID: 8387 RVA: 0x000B762E File Offset: 0x000B582E
		internal void Reactivate()
		{
			this.isActive = true;
			this.currentDepth = -1;
		}

		// Token: 0x060020C4 RID: 8388 RVA: 0x000B7640 File Offset: 0x000B5840
		internal ActiveAxis(Asttree axisTree)
		{
			this.axisTree = axisTree;
			this.currentDepth = -1;
			this.axisStack = new ArrayList(axisTree.SubtreeArray.Count);
			for (int i = 0; i < axisTree.SubtreeArray.Count; i++)
			{
				AxisStack value = new AxisStack((ForwardAxis)axisTree.SubtreeArray[i], this);
				this.axisStack.Add(value);
			}
			this.isActive = true;
		}

		// Token: 0x060020C5 RID: 8389 RVA: 0x000B76BC File Offset: 0x000B58BC
		public bool MoveToStartElement(string localname, string URN)
		{
			if (!this.isActive)
			{
				return false;
			}
			this.currentDepth++;
			bool result = false;
			for (int i = 0; i < this.axisStack.Count; i++)
			{
				AxisStack axisStack = (AxisStack)this.axisStack[i];
				if (axisStack.Subtree.IsSelfAxis)
				{
					if (axisStack.Subtree.IsDss || this.CurrentDepth == 0)
					{
						result = true;
					}
				}
				else if (this.CurrentDepth != 0 && axisStack.MoveToChild(localname, URN, this.currentDepth))
				{
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060020C6 RID: 8390 RVA: 0x000B774C File Offset: 0x000B594C
		public virtual bool EndElement(string localname, string URN)
		{
			if (this.currentDepth == 0)
			{
				this.isActive = false;
				this.currentDepth--;
			}
			if (!this.isActive)
			{
				return false;
			}
			for (int i = 0; i < this.axisStack.Count; i++)
			{
				((AxisStack)this.axisStack[i]).MoveToParent(localname, URN, this.currentDepth);
			}
			this.currentDepth--;
			return false;
		}

		// Token: 0x060020C7 RID: 8391 RVA: 0x000B77C4 File Offset: 0x000B59C4
		public bool MoveToAttribute(string localname, string URN)
		{
			if (!this.isActive)
			{
				return false;
			}
			bool result = false;
			for (int i = 0; i < this.axisStack.Count; i++)
			{
				if (((AxisStack)this.axisStack[i]).MoveToAttribute(localname, URN, this.currentDepth + 1))
				{
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0400178F RID: 6031
		private int currentDepth;

		// Token: 0x04001790 RID: 6032
		private bool isActive;

		// Token: 0x04001791 RID: 6033
		private Asttree axisTree;

		// Token: 0x04001792 RID: 6034
		private ArrayList axisStack;
	}
}
