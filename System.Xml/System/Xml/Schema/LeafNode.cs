﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200036A RID: 874
	internal class LeafNode : SyntaxTreeNode
	{
		// Token: 0x06002175 RID: 8565 RVA: 0x000B9C9C File Offset: 0x000B7E9C
		public LeafNode(int pos)
		{
			this.pos = pos;
		}

		// Token: 0x17000692 RID: 1682
		// (get) Token: 0x06002176 RID: 8566 RVA: 0x000B9CAB File Offset: 0x000B7EAB
		// (set) Token: 0x06002177 RID: 8567 RVA: 0x000B9CB3 File Offset: 0x000B7EB3
		public int Pos
		{
			get
			{
				return this.pos;
			}
			set
			{
				this.pos = value;
			}
		}

		// Token: 0x06002178 RID: 8568 RVA: 0x000030EC File Offset: 0x000012EC
		public override void ExpandTree(InteriorNode parent, SymbolsDictionary symbols, Positions positions)
		{
		}

		// Token: 0x06002179 RID: 8569 RVA: 0x000B9CBC File Offset: 0x000B7EBC
		public override SyntaxTreeNode Clone(Positions positions)
		{
			return new LeafNode(positions.Add(positions[this.pos].symbol, positions[this.pos].particle));
		}

		// Token: 0x0600217A RID: 8570 RVA: 0x000B9CEB File Offset: 0x000B7EEB
		public override void ConstructPos(BitSet firstpos, BitSet lastpos, BitSet[] followpos)
		{
			firstpos.Set(this.pos);
			lastpos.Set(this.pos);
		}

		// Token: 0x17000693 RID: 1683
		// (get) Token: 0x0600217B RID: 8571 RVA: 0x000020CD File Offset: 0x000002CD
		public override bool IsNullable
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040017EC RID: 6124
		private int pos;
	}
}
