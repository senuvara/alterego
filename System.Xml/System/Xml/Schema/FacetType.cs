﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200041F RID: 1055
	internal enum FacetType
	{
		// Token: 0x04001C66 RID: 7270
		None,
		// Token: 0x04001C67 RID: 7271
		Length,
		// Token: 0x04001C68 RID: 7272
		MinLength,
		// Token: 0x04001C69 RID: 7273
		MaxLength,
		// Token: 0x04001C6A RID: 7274
		Pattern,
		// Token: 0x04001C6B RID: 7275
		Whitespace,
		// Token: 0x04001C6C RID: 7276
		Enumeration,
		// Token: 0x04001C6D RID: 7277
		MinExclusive,
		// Token: 0x04001C6E RID: 7278
		MinInclusive,
		// Token: 0x04001C6F RID: 7279
		MaxExclusive,
		// Token: 0x04001C70 RID: 7280
		MaxInclusive,
		// Token: 0x04001C71 RID: 7281
		TotalDigits,
		// Token: 0x04001C72 RID: 7282
		FractionDigits
	}
}
