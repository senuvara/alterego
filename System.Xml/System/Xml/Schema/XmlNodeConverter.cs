﻿using System;
using System.Xml.XPath;

namespace System.Xml.Schema
{
	// Token: 0x02000469 RID: 1129
	internal class XmlNodeConverter : XmlBaseConverter
	{
		// Token: 0x06002BFA RID: 11258 RVA: 0x000EC79E File Offset: 0x000EA99E
		protected XmlNodeConverter() : base(XmlTypeCode.Node)
		{
		}

		// Token: 0x06002BFB RID: 11259 RVA: 0x000EC7A8 File Offset: 0x000EA9A8
		public override object ChangeType(object value, Type destinationType, IXmlNamespaceResolver nsResolver)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}
			Type type = value.GetType();
			if (destinationType == XmlBaseConverter.ObjectType)
			{
				destinationType = base.DefaultClrType;
			}
			if (destinationType == XmlBaseConverter.XPathNavigatorType && XmlBaseConverter.IsDerivedFrom(type, XmlBaseConverter.XPathNavigatorType))
			{
				return (XPathNavigator)value;
			}
			if (destinationType == XmlBaseConverter.XPathItemType && XmlBaseConverter.IsDerivedFrom(type, XmlBaseConverter.XPathNavigatorType))
			{
				return (XPathItem)value;
			}
			return this.ChangeListType(value, destinationType, nsResolver);
		}

		// Token: 0x06002BFC RID: 11260 RVA: 0x000EC83E File Offset: 0x000EAA3E
		// Note: this type is marked as 'beforefieldinit'.
		static XmlNodeConverter()
		{
		}

		// Token: 0x04001DA8 RID: 7592
		public static readonly XmlValueConverter Node = new XmlNodeConverter();
	}
}
