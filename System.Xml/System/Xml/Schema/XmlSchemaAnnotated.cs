﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>The base class for any element that can contain annotation elements.</summary>
	// Token: 0x02000404 RID: 1028
	public class XmlSchemaAnnotated : XmlSchemaObject
	{
		/// <summary>Gets or sets the string id.</summary>
		/// <returns>The string id. The default is <see langword="String.Empty" />.Optional.</returns>
		// Token: 0x17000818 RID: 2072
		// (get) Token: 0x06002706 RID: 9990 RVA: 0x000DE152 File Offset: 0x000DC352
		// (set) Token: 0x06002707 RID: 9991 RVA: 0x000DE15A File Offset: 0x000DC35A
		[XmlAttribute("id", DataType = "ID")]
		public string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}

		/// <summary>Gets or sets the <see langword="annotation" /> property.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaAnnotation" /> representing the <see langword="annotation" /> property.</returns>
		// Token: 0x17000819 RID: 2073
		// (get) Token: 0x06002708 RID: 9992 RVA: 0x000DE163 File Offset: 0x000DC363
		// (set) Token: 0x06002709 RID: 9993 RVA: 0x000DE16B File Offset: 0x000DC36B
		[XmlElement("annotation", typeof(XmlSchemaAnnotation))]
		public XmlSchemaAnnotation Annotation
		{
			get
			{
				return this.annotation;
			}
			set
			{
				this.annotation = value;
			}
		}

		/// <summary>Gets or sets the qualified attributes that do not belong to the current schema's target namespace.</summary>
		/// <returns>An array of qualified <see cref="T:System.Xml.XmlAttribute" /> objects that do not belong to the schema's target namespace.</returns>
		// Token: 0x1700081A RID: 2074
		// (get) Token: 0x0600270A RID: 9994 RVA: 0x000DE174 File Offset: 0x000DC374
		// (set) Token: 0x0600270B RID: 9995 RVA: 0x000DE17C File Offset: 0x000DC37C
		[XmlAnyAttribute]
		public XmlAttribute[] UnhandledAttributes
		{
			get
			{
				return this.moreAttributes;
			}
			set
			{
				this.moreAttributes = value;
			}
		}

		// Token: 0x1700081B RID: 2075
		// (get) Token: 0x0600270C RID: 9996 RVA: 0x000DE185 File Offset: 0x000DC385
		// (set) Token: 0x0600270D RID: 9997 RVA: 0x000DE18D File Offset: 0x000DC38D
		[XmlIgnore]
		internal override string IdAttribute
		{
			get
			{
				return this.Id;
			}
			set
			{
				this.Id = value;
			}
		}

		// Token: 0x0600270E RID: 9998 RVA: 0x000DE17C File Offset: 0x000DC37C
		internal override void SetUnhandledAttributes(XmlAttribute[] moreAttributes)
		{
			this.moreAttributes = moreAttributes;
		}

		// Token: 0x0600270F RID: 9999 RVA: 0x000DE16B File Offset: 0x000DC36B
		internal override void AddAnnotation(XmlSchemaAnnotation annotation)
		{
			this.annotation = annotation;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAnnotated" /> class.</summary>
		// Token: 0x06002710 RID: 10000 RVA: 0x000DE196 File Offset: 0x000DC396
		public XmlSchemaAnnotated()
		{
		}

		// Token: 0x04001BE0 RID: 7136
		private string id;

		// Token: 0x04001BE1 RID: 7137
		private XmlSchemaAnnotation annotation;

		// Token: 0x04001BE2 RID: 7138
		private XmlAttribute[] moreAttributes;
	}
}
