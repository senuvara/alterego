﻿using System;
using System.Collections;
using System.Xml.XPath;

namespace System.Xml.Schema
{
	// Token: 0x02000461 RID: 1121
	internal abstract class XmlBaseConverter : XmlValueConverter
	{
		// Token: 0x06002AC9 RID: 10953 RVA: 0x000E7BBC File Offset: 0x000E5DBC
		protected XmlBaseConverter(XmlSchemaType schemaType)
		{
			XmlSchemaDatatype datatype = schemaType.Datatype;
			while (schemaType != null && !(schemaType is XmlSchemaSimpleType))
			{
				schemaType = schemaType.BaseXmlSchemaType;
			}
			if (schemaType == null)
			{
				schemaType = XmlSchemaType.GetBuiltInSimpleType(datatype.TypeCode);
			}
			this.schemaType = schemaType;
			this.typeCode = schemaType.TypeCode;
			this.clrTypeDefault = schemaType.Datatype.ValueType;
		}

		// Token: 0x06002ACA RID: 10954 RVA: 0x000E7C20 File Offset: 0x000E5E20
		protected XmlBaseConverter(XmlTypeCode typeCode)
		{
			if (typeCode != XmlTypeCode.Item)
			{
				if (typeCode != XmlTypeCode.Node)
				{
					if (typeCode == XmlTypeCode.AnyAtomicType)
					{
						this.clrTypeDefault = XmlBaseConverter.XmlAtomicValueType;
					}
				}
				else
				{
					this.clrTypeDefault = XmlBaseConverter.XPathNavigatorType;
				}
			}
			else
			{
				this.clrTypeDefault = XmlBaseConverter.XPathItemType;
			}
			this.typeCode = typeCode;
		}

		// Token: 0x06002ACB RID: 10955 RVA: 0x000E7C6E File Offset: 0x000E5E6E
		protected XmlBaseConverter(XmlBaseConverter converterAtomic)
		{
			this.schemaType = converterAtomic.schemaType;
			this.typeCode = converterAtomic.typeCode;
			this.clrTypeDefault = Array.CreateInstance(converterAtomic.DefaultClrType, 0).GetType();
		}

		// Token: 0x06002ACC RID: 10956 RVA: 0x000E7CA5 File Offset: 0x000E5EA5
		protected XmlBaseConverter(XmlBaseConverter converterAtomic, Type clrTypeDefault)
		{
			this.schemaType = converterAtomic.schemaType;
			this.typeCode = converterAtomic.typeCode;
			this.clrTypeDefault = clrTypeDefault;
		}

		// Token: 0x06002ACD RID: 10957 RVA: 0x000E7CCC File Offset: 0x000E5ECC
		public override bool ToBoolean(bool value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002ACE RID: 10958 RVA: 0x000E7CE5 File Offset: 0x000E5EE5
		public override bool ToBoolean(DateTime value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002ACF RID: 10959 RVA: 0x000E7CFE File Offset: 0x000E5EFE
		public override bool ToBoolean(DateTimeOffset value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002AD0 RID: 10960 RVA: 0x000E7D17 File Offset: 0x000E5F17
		public override bool ToBoolean(decimal value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002AD1 RID: 10961 RVA: 0x000E7D30 File Offset: 0x000E5F30
		public override bool ToBoolean(double value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002AD2 RID: 10962 RVA: 0x000E7D49 File Offset: 0x000E5F49
		public override bool ToBoolean(int value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002AD3 RID: 10963 RVA: 0x000E7D62 File Offset: 0x000E5F62
		public override bool ToBoolean(long value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002AD4 RID: 10964 RVA: 0x000E7D7B File Offset: 0x000E5F7B
		public override bool ToBoolean(float value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002AD5 RID: 10965 RVA: 0x000E7D94 File Offset: 0x000E5F94
		public override bool ToBoolean(string value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002AD6 RID: 10966 RVA: 0x000E7D94 File Offset: 0x000E5F94
		public override bool ToBoolean(object value)
		{
			return (bool)this.ChangeType(value, XmlBaseConverter.BooleanType, null);
		}

		// Token: 0x06002AD7 RID: 10967 RVA: 0x000E7DA8 File Offset: 0x000E5FA8
		public override DateTime ToDateTime(bool value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002AD8 RID: 10968 RVA: 0x000E7DC1 File Offset: 0x000E5FC1
		public override DateTime ToDateTime(DateTime value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002AD9 RID: 10969 RVA: 0x000E7DDA File Offset: 0x000E5FDA
		public override DateTime ToDateTime(DateTimeOffset value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002ADA RID: 10970 RVA: 0x000E7DF3 File Offset: 0x000E5FF3
		public override DateTime ToDateTime(decimal value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002ADB RID: 10971 RVA: 0x000E7E0C File Offset: 0x000E600C
		public override DateTime ToDateTime(double value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002ADC RID: 10972 RVA: 0x000E7E25 File Offset: 0x000E6025
		public override DateTime ToDateTime(int value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002ADD RID: 10973 RVA: 0x000E7E3E File Offset: 0x000E603E
		public override DateTime ToDateTime(long value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002ADE RID: 10974 RVA: 0x000E7E57 File Offset: 0x000E6057
		public override DateTime ToDateTime(float value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002ADF RID: 10975 RVA: 0x000E7E70 File Offset: 0x000E6070
		public override DateTime ToDateTime(string value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002AE0 RID: 10976 RVA: 0x000E7E70 File Offset: 0x000E6070
		public override DateTime ToDateTime(object value)
		{
			return (DateTime)this.ChangeType(value, XmlBaseConverter.DateTimeType, null);
		}

		// Token: 0x06002AE1 RID: 10977 RVA: 0x000E7E84 File Offset: 0x000E6084
		public override DateTimeOffset ToDateTimeOffset(bool value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AE2 RID: 10978 RVA: 0x000E7E9D File Offset: 0x000E609D
		public override DateTimeOffset ToDateTimeOffset(DateTime value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AE3 RID: 10979 RVA: 0x000E7EB6 File Offset: 0x000E60B6
		public override DateTimeOffset ToDateTimeOffset(DateTimeOffset value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AE4 RID: 10980 RVA: 0x000E7ECF File Offset: 0x000E60CF
		public override DateTimeOffset ToDateTimeOffset(decimal value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AE5 RID: 10981 RVA: 0x000E7EE8 File Offset: 0x000E60E8
		public override DateTimeOffset ToDateTimeOffset(double value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AE6 RID: 10982 RVA: 0x000E7F01 File Offset: 0x000E6101
		public override DateTimeOffset ToDateTimeOffset(int value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AE7 RID: 10983 RVA: 0x000E7F1A File Offset: 0x000E611A
		public override DateTimeOffset ToDateTimeOffset(long value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AE8 RID: 10984 RVA: 0x000E7F33 File Offset: 0x000E6133
		public override DateTimeOffset ToDateTimeOffset(float value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AE9 RID: 10985 RVA: 0x000E7F4C File Offset: 0x000E614C
		public override DateTimeOffset ToDateTimeOffset(string value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AEA RID: 10986 RVA: 0x000E7F4C File Offset: 0x000E614C
		public override DateTimeOffset ToDateTimeOffset(object value)
		{
			return (DateTimeOffset)this.ChangeType(value, XmlBaseConverter.DateTimeOffsetType, null);
		}

		// Token: 0x06002AEB RID: 10987 RVA: 0x000E7F60 File Offset: 0x000E6160
		public override decimal ToDecimal(bool value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AEC RID: 10988 RVA: 0x000E7F79 File Offset: 0x000E6179
		public override decimal ToDecimal(DateTime value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AED RID: 10989 RVA: 0x000E7F92 File Offset: 0x000E6192
		public override decimal ToDecimal(DateTimeOffset value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AEE RID: 10990 RVA: 0x000E7FAB File Offset: 0x000E61AB
		public override decimal ToDecimal(decimal value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AEF RID: 10991 RVA: 0x000E7FC4 File Offset: 0x000E61C4
		public override decimal ToDecimal(double value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AF0 RID: 10992 RVA: 0x000E7FDD File Offset: 0x000E61DD
		public override decimal ToDecimal(int value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AF1 RID: 10993 RVA: 0x000E7FF6 File Offset: 0x000E61F6
		public override decimal ToDecimal(long value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AF2 RID: 10994 RVA: 0x000E800F File Offset: 0x000E620F
		public override decimal ToDecimal(float value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AF3 RID: 10995 RVA: 0x000E8028 File Offset: 0x000E6228
		public override decimal ToDecimal(string value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AF4 RID: 10996 RVA: 0x000E8028 File Offset: 0x000E6228
		public override decimal ToDecimal(object value)
		{
			return (decimal)this.ChangeType(value, XmlBaseConverter.DecimalType, null);
		}

		// Token: 0x06002AF5 RID: 10997 RVA: 0x000E803C File Offset: 0x000E623C
		public override double ToDouble(bool value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AF6 RID: 10998 RVA: 0x000E8055 File Offset: 0x000E6255
		public override double ToDouble(DateTime value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AF7 RID: 10999 RVA: 0x000E806E File Offset: 0x000E626E
		public override double ToDouble(DateTimeOffset value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AF8 RID: 11000 RVA: 0x000E8087 File Offset: 0x000E6287
		public override double ToDouble(decimal value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AF9 RID: 11001 RVA: 0x000E80A0 File Offset: 0x000E62A0
		public override double ToDouble(double value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AFA RID: 11002 RVA: 0x000E80B9 File Offset: 0x000E62B9
		public override double ToDouble(int value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AFB RID: 11003 RVA: 0x000E80D2 File Offset: 0x000E62D2
		public override double ToDouble(long value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AFC RID: 11004 RVA: 0x000E80EB File Offset: 0x000E62EB
		public override double ToDouble(float value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AFD RID: 11005 RVA: 0x000E8104 File Offset: 0x000E6304
		public override double ToDouble(string value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AFE RID: 11006 RVA: 0x000E8104 File Offset: 0x000E6304
		public override double ToDouble(object value)
		{
			return (double)this.ChangeType(value, XmlBaseConverter.DoubleType, null);
		}

		// Token: 0x06002AFF RID: 11007 RVA: 0x000E8118 File Offset: 0x000E6318
		public override int ToInt32(bool value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B00 RID: 11008 RVA: 0x000E8131 File Offset: 0x000E6331
		public override int ToInt32(DateTime value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B01 RID: 11009 RVA: 0x000E814A File Offset: 0x000E634A
		public override int ToInt32(DateTimeOffset value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B02 RID: 11010 RVA: 0x000E8163 File Offset: 0x000E6363
		public override int ToInt32(decimal value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B03 RID: 11011 RVA: 0x000E817C File Offset: 0x000E637C
		public override int ToInt32(double value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B04 RID: 11012 RVA: 0x000E8195 File Offset: 0x000E6395
		public override int ToInt32(int value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B05 RID: 11013 RVA: 0x000E81AE File Offset: 0x000E63AE
		public override int ToInt32(long value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B06 RID: 11014 RVA: 0x000E81C7 File Offset: 0x000E63C7
		public override int ToInt32(float value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B07 RID: 11015 RVA: 0x000E81E0 File Offset: 0x000E63E0
		public override int ToInt32(string value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B08 RID: 11016 RVA: 0x000E81E0 File Offset: 0x000E63E0
		public override int ToInt32(object value)
		{
			return (int)this.ChangeType(value, XmlBaseConverter.Int32Type, null);
		}

		// Token: 0x06002B09 RID: 11017 RVA: 0x000E81F4 File Offset: 0x000E63F4
		public override long ToInt64(bool value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B0A RID: 11018 RVA: 0x000E820D File Offset: 0x000E640D
		public override long ToInt64(DateTime value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B0B RID: 11019 RVA: 0x000E8226 File Offset: 0x000E6426
		public override long ToInt64(DateTimeOffset value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B0C RID: 11020 RVA: 0x000E823F File Offset: 0x000E643F
		public override long ToInt64(decimal value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B0D RID: 11021 RVA: 0x000E8258 File Offset: 0x000E6458
		public override long ToInt64(double value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B0E RID: 11022 RVA: 0x000E8271 File Offset: 0x000E6471
		public override long ToInt64(int value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B0F RID: 11023 RVA: 0x000E828A File Offset: 0x000E648A
		public override long ToInt64(long value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B10 RID: 11024 RVA: 0x000E82A3 File Offset: 0x000E64A3
		public override long ToInt64(float value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B11 RID: 11025 RVA: 0x000E82BC File Offset: 0x000E64BC
		public override long ToInt64(string value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B12 RID: 11026 RVA: 0x000E82BC File Offset: 0x000E64BC
		public override long ToInt64(object value)
		{
			return (long)this.ChangeType(value, XmlBaseConverter.Int64Type, null);
		}

		// Token: 0x06002B13 RID: 11027 RVA: 0x000E82D0 File Offset: 0x000E64D0
		public override float ToSingle(bool value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B14 RID: 11028 RVA: 0x000E82E9 File Offset: 0x000E64E9
		public override float ToSingle(DateTime value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B15 RID: 11029 RVA: 0x000E8302 File Offset: 0x000E6502
		public override float ToSingle(DateTimeOffset value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B16 RID: 11030 RVA: 0x000E831B File Offset: 0x000E651B
		public override float ToSingle(decimal value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B17 RID: 11031 RVA: 0x000E8334 File Offset: 0x000E6534
		public override float ToSingle(double value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B18 RID: 11032 RVA: 0x000E834D File Offset: 0x000E654D
		public override float ToSingle(int value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B19 RID: 11033 RVA: 0x000E8366 File Offset: 0x000E6566
		public override float ToSingle(long value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B1A RID: 11034 RVA: 0x000E837F File Offset: 0x000E657F
		public override float ToSingle(float value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B1B RID: 11035 RVA: 0x000E8398 File Offset: 0x000E6598
		public override float ToSingle(string value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B1C RID: 11036 RVA: 0x000E8398 File Offset: 0x000E6598
		public override float ToSingle(object value)
		{
			return (float)this.ChangeType(value, XmlBaseConverter.SingleType, null);
		}

		// Token: 0x06002B1D RID: 11037 RVA: 0x000E83AC File Offset: 0x000E65AC
		public override string ToString(bool value)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, null);
		}

		// Token: 0x06002B1E RID: 11038 RVA: 0x000E83C5 File Offset: 0x000E65C5
		public override string ToString(DateTime value)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, null);
		}

		// Token: 0x06002B1F RID: 11039 RVA: 0x000E83DE File Offset: 0x000E65DE
		public override string ToString(DateTimeOffset value)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, null);
		}

		// Token: 0x06002B20 RID: 11040 RVA: 0x000E83F7 File Offset: 0x000E65F7
		public override string ToString(decimal value)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, null);
		}

		// Token: 0x06002B21 RID: 11041 RVA: 0x000E8410 File Offset: 0x000E6610
		public override string ToString(double value)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, null);
		}

		// Token: 0x06002B22 RID: 11042 RVA: 0x000E8429 File Offset: 0x000E6629
		public override string ToString(int value)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, null);
		}

		// Token: 0x06002B23 RID: 11043 RVA: 0x000E8442 File Offset: 0x000E6642
		public override string ToString(long value)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, null);
		}

		// Token: 0x06002B24 RID: 11044 RVA: 0x000E845B File Offset: 0x000E665B
		public override string ToString(float value)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, null);
		}

		// Token: 0x06002B25 RID: 11045 RVA: 0x000E8474 File Offset: 0x000E6674
		public override string ToString(string value, IXmlNamespaceResolver nsResolver)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, nsResolver);
		}

		// Token: 0x06002B26 RID: 11046 RVA: 0x000E8474 File Offset: 0x000E6674
		public override string ToString(object value, IXmlNamespaceResolver nsResolver)
		{
			return (string)this.ChangeType(value, XmlBaseConverter.StringType, nsResolver);
		}

		// Token: 0x06002B27 RID: 11047 RVA: 0x000E8488 File Offset: 0x000E6688
		public override string ToString(string value)
		{
			return this.ToString(value, null);
		}

		// Token: 0x06002B28 RID: 11048 RVA: 0x000E8492 File Offset: 0x000E6692
		public override string ToString(object value)
		{
			return this.ToString(value, null);
		}

		// Token: 0x06002B29 RID: 11049 RVA: 0x000E849C File Offset: 0x000E669C
		public override object ChangeType(bool value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B2A RID: 11050 RVA: 0x000E84AC File Offset: 0x000E66AC
		public override object ChangeType(DateTime value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B2B RID: 11051 RVA: 0x000E84BC File Offset: 0x000E66BC
		public override object ChangeType(DateTimeOffset value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B2C RID: 11052 RVA: 0x000E84CC File Offset: 0x000E66CC
		public override object ChangeType(decimal value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B2D RID: 11053 RVA: 0x000E84DC File Offset: 0x000E66DC
		public override object ChangeType(double value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B2E RID: 11054 RVA: 0x000E84EC File Offset: 0x000E66EC
		public override object ChangeType(int value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B2F RID: 11055 RVA: 0x000E84FC File Offset: 0x000E66FC
		public override object ChangeType(long value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B30 RID: 11056 RVA: 0x000E850C File Offset: 0x000E670C
		public override object ChangeType(float value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B31 RID: 11057 RVA: 0x000E851C File Offset: 0x000E671C
		public override object ChangeType(string value, Type destinationType, IXmlNamespaceResolver nsResolver)
		{
			return this.ChangeType(value, destinationType, nsResolver);
		}

		// Token: 0x06002B32 RID: 11058 RVA: 0x000E8527 File Offset: 0x000E6727
		public override object ChangeType(string value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x06002B33 RID: 11059 RVA: 0x000E8532 File Offset: 0x000E6732
		public override object ChangeType(object value, Type destinationType)
		{
			return this.ChangeType(value, destinationType, null);
		}

		// Token: 0x17000944 RID: 2372
		// (get) Token: 0x06002B34 RID: 11060 RVA: 0x000E853D File Offset: 0x000E673D
		protected XmlSchemaType SchemaType
		{
			get
			{
				return this.schemaType;
			}
		}

		// Token: 0x17000945 RID: 2373
		// (get) Token: 0x06002B35 RID: 11061 RVA: 0x000E8545 File Offset: 0x000E6745
		protected XmlTypeCode TypeCode
		{
			get
			{
				return this.typeCode;
			}
		}

		// Token: 0x17000946 RID: 2374
		// (get) Token: 0x06002B36 RID: 11062 RVA: 0x000E8550 File Offset: 0x000E6750
		protected string XmlTypeName
		{
			get
			{
				XmlSchemaType baseXmlSchemaType = this.schemaType;
				if (baseXmlSchemaType != null)
				{
					while (baseXmlSchemaType.QualifiedName.IsEmpty)
					{
						baseXmlSchemaType = baseXmlSchemaType.BaseXmlSchemaType;
					}
					return XmlBaseConverter.QNameToString(baseXmlSchemaType.QualifiedName);
				}
				if (this.typeCode == XmlTypeCode.Node)
				{
					return "node";
				}
				if (this.typeCode == XmlTypeCode.AnyAtomicType)
				{
					return "xdt:anyAtomicType";
				}
				return "item";
			}
		}

		// Token: 0x17000947 RID: 2375
		// (get) Token: 0x06002B37 RID: 11063 RVA: 0x000E85AD File Offset: 0x000E67AD
		protected Type DefaultClrType
		{
			get
			{
				return this.clrTypeDefault;
			}
		}

		// Token: 0x06002B38 RID: 11064 RVA: 0x000E85B5 File Offset: 0x000E67B5
		protected static bool IsDerivedFrom(Type derivedType, Type baseType)
		{
			while (derivedType != null)
			{
				if (derivedType == baseType)
				{
					return true;
				}
				derivedType = derivedType.BaseType;
			}
			return false;
		}

		// Token: 0x06002B39 RID: 11065 RVA: 0x000E85D8 File Offset: 0x000E67D8
		protected Exception CreateInvalidClrMappingException(Type sourceType, Type destinationType)
		{
			if (sourceType == destinationType)
			{
				return new InvalidCastException(Res.GetString("Xml type '{0}' does not support Clr type '{1}'.", new object[]
				{
					this.XmlTypeName,
					sourceType.Name
				}));
			}
			return new InvalidCastException(Res.GetString("Xml type '{0}' does not support a conversion from Clr type '{1}' to Clr type '{2}'.", new object[]
			{
				this.XmlTypeName,
				sourceType.Name,
				destinationType.Name
			}));
		}

		// Token: 0x06002B3A RID: 11066 RVA: 0x000E8648 File Offset: 0x000E6848
		protected static string QNameToString(XmlQualifiedName name)
		{
			if (name.Namespace.Length == 0)
			{
				return name.Name;
			}
			if (name.Namespace == "http://www.w3.org/2001/XMLSchema")
			{
				return "xs:" + name.Name;
			}
			if (name.Namespace == "http://www.w3.org/2003/11/xpath-datatypes")
			{
				return "xdt:" + name.Name;
			}
			return "{" + name.Namespace + "}" + name.Name;
		}

		// Token: 0x06002B3B RID: 11067 RVA: 0x000E86CA File Offset: 0x000E68CA
		protected virtual object ChangeListType(object value, Type destinationType, IXmlNamespaceResolver nsResolver)
		{
			throw this.CreateInvalidClrMappingException(value.GetType(), destinationType);
		}

		// Token: 0x06002B3C RID: 11068 RVA: 0x000E86D9 File Offset: 0x000E68D9
		protected static byte[] StringToBase64Binary(string value)
		{
			return Convert.FromBase64String(XmlConvert.TrimString(value));
		}

		// Token: 0x06002B3D RID: 11069 RVA: 0x000E86E6 File Offset: 0x000E68E6
		protected static DateTime StringToDate(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.Date);
		}

		// Token: 0x06002B3E RID: 11070 RVA: 0x000E86F4 File Offset: 0x000E68F4
		protected static DateTime StringToDateTime(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.DateTime);
		}

		// Token: 0x06002B3F RID: 11071 RVA: 0x000E8704 File Offset: 0x000E6904
		protected static TimeSpan StringToDayTimeDuration(string value)
		{
			return new XsdDuration(value, XsdDuration.DurationType.DayTimeDuration).ToTimeSpan(XsdDuration.DurationType.DayTimeDuration);
		}

		// Token: 0x06002B40 RID: 11072 RVA: 0x000E8724 File Offset: 0x000E6924
		protected static TimeSpan StringToDuration(string value)
		{
			return new XsdDuration(value, XsdDuration.DurationType.Duration).ToTimeSpan(XsdDuration.DurationType.Duration);
		}

		// Token: 0x06002B41 RID: 11073 RVA: 0x000E8741 File Offset: 0x000E6941
		protected static DateTime StringToGDay(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GDay);
		}

		// Token: 0x06002B42 RID: 11074 RVA: 0x000E8750 File Offset: 0x000E6950
		protected static DateTime StringToGMonth(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GMonth);
		}

		// Token: 0x06002B43 RID: 11075 RVA: 0x000E8762 File Offset: 0x000E6962
		protected static DateTime StringToGMonthDay(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GMonthDay);
		}

		// Token: 0x06002B44 RID: 11076 RVA: 0x000E8771 File Offset: 0x000E6971
		protected static DateTime StringToGYear(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GYear);
		}

		// Token: 0x06002B45 RID: 11077 RVA: 0x000E8780 File Offset: 0x000E6980
		protected static DateTime StringToGYearMonth(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GYearMonth);
		}

		// Token: 0x06002B46 RID: 11078 RVA: 0x000E878E File Offset: 0x000E698E
		protected static DateTimeOffset StringToDateOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.Date);
		}

		// Token: 0x06002B47 RID: 11079 RVA: 0x000E879C File Offset: 0x000E699C
		protected static DateTimeOffset StringToDateTimeOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.DateTime);
		}

		// Token: 0x06002B48 RID: 11080 RVA: 0x000E87AA File Offset: 0x000E69AA
		protected static DateTimeOffset StringToGDayOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GDay);
		}

		// Token: 0x06002B49 RID: 11081 RVA: 0x000E87B9 File Offset: 0x000E69B9
		protected static DateTimeOffset StringToGMonthOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GMonth);
		}

		// Token: 0x06002B4A RID: 11082 RVA: 0x000E87CB File Offset: 0x000E69CB
		protected static DateTimeOffset StringToGMonthDayOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GMonthDay);
		}

		// Token: 0x06002B4B RID: 11083 RVA: 0x000E87DA File Offset: 0x000E69DA
		protected static DateTimeOffset StringToGYearOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GYear);
		}

		// Token: 0x06002B4C RID: 11084 RVA: 0x000E87E9 File Offset: 0x000E69E9
		protected static DateTimeOffset StringToGYearMonthOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GYearMonth);
		}

		// Token: 0x06002B4D RID: 11085 RVA: 0x000E87F8 File Offset: 0x000E69F8
		protected static byte[] StringToHexBinary(string value)
		{
			byte[] result;
			try
			{
				result = XmlConvert.FromBinHexString(XmlConvert.TrimString(value), false);
			}
			catch (XmlException ex)
			{
				throw new FormatException(ex.Message);
			}
			return result;
		}

		// Token: 0x06002B4E RID: 11086 RVA: 0x000E8830 File Offset: 0x000E6A30
		protected static XmlQualifiedName StringToQName(string value, IXmlNamespaceResolver nsResolver)
		{
			value = value.Trim();
			string text;
			string name;
			try
			{
				ValidateNames.ParseQNameThrow(value, out text, out name);
			}
			catch (XmlException ex)
			{
				throw new FormatException(ex.Message);
			}
			if (nsResolver == null)
			{
				throw new InvalidCastException(Res.GetString("The String '{0}' cannot be represented as an XmlQualifiedName.  A namespace for prefix '{1}' cannot be found.", new object[]
				{
					value,
					text
				}));
			}
			string text2 = nsResolver.LookupNamespace(text);
			if (text2 == null)
			{
				throw new InvalidCastException(Res.GetString("The String '{0}' cannot be represented as an XmlQualifiedName.  A namespace for prefix '{1}' cannot be found.", new object[]
				{
					value,
					text
				}));
			}
			return new XmlQualifiedName(name, text2);
		}

		// Token: 0x06002B4F RID: 11087 RVA: 0x000E88C0 File Offset: 0x000E6AC0
		protected static DateTime StringToTime(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.Time);
		}

		// Token: 0x06002B50 RID: 11088 RVA: 0x000E88CE File Offset: 0x000E6ACE
		protected static DateTimeOffset StringToTimeOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.Time);
		}

		// Token: 0x06002B51 RID: 11089 RVA: 0x000E88DC File Offset: 0x000E6ADC
		protected static TimeSpan StringToYearMonthDuration(string value)
		{
			return new XsdDuration(value, XsdDuration.DurationType.YearMonthDuration).ToTimeSpan(XsdDuration.DurationType.YearMonthDuration);
		}

		// Token: 0x06002B52 RID: 11090 RVA: 0x000E88F9 File Offset: 0x000E6AF9
		protected static string AnyUriToString(Uri value)
		{
			return value.OriginalString;
		}

		// Token: 0x06002B53 RID: 11091 RVA: 0x000E8901 File Offset: 0x000E6B01
		protected static string Base64BinaryToString(byte[] value)
		{
			return Convert.ToBase64String(value);
		}

		// Token: 0x06002B54 RID: 11092 RVA: 0x000E890C File Offset: 0x000E6B0C
		protected static string DateToString(DateTime value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.Date).ToString();
		}

		// Token: 0x06002B55 RID: 11093 RVA: 0x000E8930 File Offset: 0x000E6B30
		protected static string DateTimeToString(DateTime value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.DateTime).ToString();
		}

		// Token: 0x06002B56 RID: 11094 RVA: 0x000E8954 File Offset: 0x000E6B54
		protected static string DayTimeDurationToString(TimeSpan value)
		{
			return new XsdDuration(value, XsdDuration.DurationType.DayTimeDuration).ToString(XsdDuration.DurationType.DayTimeDuration);
		}

		// Token: 0x06002B57 RID: 11095 RVA: 0x000E8974 File Offset: 0x000E6B74
		protected static string DurationToString(TimeSpan value)
		{
			return new XsdDuration(value, XsdDuration.DurationType.Duration).ToString(XsdDuration.DurationType.Duration);
		}

		// Token: 0x06002B58 RID: 11096 RVA: 0x000E8994 File Offset: 0x000E6B94
		protected static string GDayToString(DateTime value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GDay).ToString();
		}

		// Token: 0x06002B59 RID: 11097 RVA: 0x000E89B8 File Offset: 0x000E6BB8
		protected static string GMonthToString(DateTime value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GMonth).ToString();
		}

		// Token: 0x06002B5A RID: 11098 RVA: 0x000E89E0 File Offset: 0x000E6BE0
		protected static string GMonthDayToString(DateTime value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GMonthDay).ToString();
		}

		// Token: 0x06002B5B RID: 11099 RVA: 0x000E8A04 File Offset: 0x000E6C04
		protected static string GYearToString(DateTime value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GYear).ToString();
		}

		// Token: 0x06002B5C RID: 11100 RVA: 0x000E8A28 File Offset: 0x000E6C28
		protected static string GYearMonthToString(DateTime value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GYearMonth).ToString();
		}

		// Token: 0x06002B5D RID: 11101 RVA: 0x000E8A4C File Offset: 0x000E6C4C
		protected static string DateOffsetToString(DateTimeOffset value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.Date).ToString();
		}

		// Token: 0x06002B5E RID: 11102 RVA: 0x000E8A70 File Offset: 0x000E6C70
		protected static string DateTimeOffsetToString(DateTimeOffset value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.DateTime).ToString();
		}

		// Token: 0x06002B5F RID: 11103 RVA: 0x000E8A94 File Offset: 0x000E6C94
		protected static string GDayOffsetToString(DateTimeOffset value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GDay).ToString();
		}

		// Token: 0x06002B60 RID: 11104 RVA: 0x000E8AB8 File Offset: 0x000E6CB8
		protected static string GMonthOffsetToString(DateTimeOffset value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GMonth).ToString();
		}

		// Token: 0x06002B61 RID: 11105 RVA: 0x000E8AE0 File Offset: 0x000E6CE0
		protected static string GMonthDayOffsetToString(DateTimeOffset value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GMonthDay).ToString();
		}

		// Token: 0x06002B62 RID: 11106 RVA: 0x000E8B04 File Offset: 0x000E6D04
		protected static string GYearOffsetToString(DateTimeOffset value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GYear).ToString();
		}

		// Token: 0x06002B63 RID: 11107 RVA: 0x000E8B28 File Offset: 0x000E6D28
		protected static string GYearMonthOffsetToString(DateTimeOffset value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.GYearMonth).ToString();
		}

		// Token: 0x06002B64 RID: 11108 RVA: 0x000E8B4C File Offset: 0x000E6D4C
		protected static string QNameToString(XmlQualifiedName qname, IXmlNamespaceResolver nsResolver)
		{
			if (nsResolver == null)
			{
				return "{" + qname.Namespace + "}" + qname.Name;
			}
			string text = nsResolver.LookupPrefix(qname.Namespace);
			if (text == null)
			{
				throw new InvalidCastException(Res.GetString("The QName '{0}' cannot be represented as a String.  A prefix for namespace '{1}' cannot be found.", new object[]
				{
					qname.ToString(),
					qname.Namespace
				}));
			}
			if (text.Length == 0)
			{
				return qname.Name;
			}
			return text + ":" + qname.Name;
		}

		// Token: 0x06002B65 RID: 11109 RVA: 0x000E8BD0 File Offset: 0x000E6DD0
		protected static string TimeToString(DateTime value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.Time).ToString();
		}

		// Token: 0x06002B66 RID: 11110 RVA: 0x000E8BF4 File Offset: 0x000E6DF4
		protected static string TimeOffsetToString(DateTimeOffset value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.Time).ToString();
		}

		// Token: 0x06002B67 RID: 11111 RVA: 0x000E8C18 File Offset: 0x000E6E18
		protected static string YearMonthDurationToString(TimeSpan value)
		{
			return new XsdDuration(value, XsdDuration.DurationType.YearMonthDuration).ToString(XsdDuration.DurationType.YearMonthDuration);
		}

		// Token: 0x06002B68 RID: 11112 RVA: 0x000E8C35 File Offset: 0x000E6E35
		internal static DateTime DateTimeOffsetToDateTime(DateTimeOffset value)
		{
			return value.LocalDateTime;
		}

		// Token: 0x06002B69 RID: 11113 RVA: 0x000E8C40 File Offset: 0x000E6E40
		internal static int DecimalToInt32(decimal value)
		{
			if (value < -2147483648m || value > 2147483647m)
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"Int32"
				}));
			}
			return (int)value;
		}

		// Token: 0x06002B6A RID: 11114 RVA: 0x000E8CA0 File Offset: 0x000E6EA0
		protected static long DecimalToInt64(decimal value)
		{
			if (value < -9223372036854775808m || value > 9223372036854775807m)
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"Int64"
				}));
			}
			return (long)value;
		}

		// Token: 0x06002B6B RID: 11115 RVA: 0x000E8D08 File Offset: 0x000E6F08
		protected static ulong DecimalToUInt64(decimal value)
		{
			if (value < 0m || value > 18446744073709551615m)
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"UInt64"
				}));
			}
			return (ulong)value;
		}

		// Token: 0x06002B6C RID: 11116 RVA: 0x000E8D5E File Offset: 0x000E6F5E
		protected static byte Int32ToByte(int value)
		{
			if (value < 0 || value > 255)
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"Byte"
				}));
			}
			return (byte)value;
		}

		// Token: 0x06002B6D RID: 11117 RVA: 0x000E8D95 File Offset: 0x000E6F95
		protected static short Int32ToInt16(int value)
		{
			if (value < -32768 || value > 32767)
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"Int16"
				}));
			}
			return (short)value;
		}

		// Token: 0x06002B6E RID: 11118 RVA: 0x000E8DD0 File Offset: 0x000E6FD0
		protected static sbyte Int32ToSByte(int value)
		{
			if (value < -128 || value > 127)
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"SByte"
				}));
			}
			return (sbyte)value;
		}

		// Token: 0x06002B6F RID: 11119 RVA: 0x000E8E05 File Offset: 0x000E7005
		protected static ushort Int32ToUInt16(int value)
		{
			if (value < 0 || value > 65535)
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"UInt16"
				}));
			}
			return (ushort)value;
		}

		// Token: 0x06002B70 RID: 11120 RVA: 0x000E8E3C File Offset: 0x000E703C
		protected static int Int64ToInt32(long value)
		{
			if (value < -2147483648L || value > 2147483647L)
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"Int32"
				}));
			}
			return (int)value;
		}

		// Token: 0x06002B71 RID: 11121 RVA: 0x000E8E79 File Offset: 0x000E7079
		protected static uint Int64ToUInt32(long value)
		{
			if (value < 0L || value > (long)((ulong)-1))
			{
				throw new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new string[]
				{
					XmlConvert.ToString(value),
					"UInt32"
				}));
			}
			return (uint)value;
		}

		// Token: 0x06002B72 RID: 11122 RVA: 0x000E8EAE File Offset: 0x000E70AE
		protected static DateTime UntypedAtomicToDateTime(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.AllXsd);
		}

		// Token: 0x06002B73 RID: 11123 RVA: 0x000E8EC0 File Offset: 0x000E70C0
		protected static DateTimeOffset UntypedAtomicToDateTimeOffset(string value)
		{
			return new XsdDateTime(value, XsdDateTimeFlags.AllXsd);
		}

		// Token: 0x06002B74 RID: 11124 RVA: 0x000E8ED4 File Offset: 0x000E70D4
		// Note: this type is marked as 'beforefieldinit'.
		static XmlBaseConverter()
		{
		}

		// Token: 0x04001D85 RID: 7557
		private XmlSchemaType schemaType;

		// Token: 0x04001D86 RID: 7558
		private XmlTypeCode typeCode;

		// Token: 0x04001D87 RID: 7559
		private Type clrTypeDefault;

		// Token: 0x04001D88 RID: 7560
		protected static readonly Type ICollectionType = typeof(ICollection);

		// Token: 0x04001D89 RID: 7561
		protected static readonly Type IEnumerableType = typeof(IEnumerable);

		// Token: 0x04001D8A RID: 7562
		protected static readonly Type IListType = typeof(IList);

		// Token: 0x04001D8B RID: 7563
		protected static readonly Type ObjectArrayType = typeof(object[]);

		// Token: 0x04001D8C RID: 7564
		protected static readonly Type StringArrayType = typeof(string[]);

		// Token: 0x04001D8D RID: 7565
		protected static readonly Type XmlAtomicValueArrayType = typeof(XmlAtomicValue[]);

		// Token: 0x04001D8E RID: 7566
		protected static readonly Type DecimalType = typeof(decimal);

		// Token: 0x04001D8F RID: 7567
		protected static readonly Type Int32Type = typeof(int);

		// Token: 0x04001D90 RID: 7568
		protected static readonly Type Int64Type = typeof(long);

		// Token: 0x04001D91 RID: 7569
		protected static readonly Type StringType = typeof(string);

		// Token: 0x04001D92 RID: 7570
		protected static readonly Type XmlAtomicValueType = typeof(XmlAtomicValue);

		// Token: 0x04001D93 RID: 7571
		protected static readonly Type ObjectType = typeof(object);

		// Token: 0x04001D94 RID: 7572
		protected static readonly Type ByteType = typeof(byte);

		// Token: 0x04001D95 RID: 7573
		protected static readonly Type Int16Type = typeof(short);

		// Token: 0x04001D96 RID: 7574
		protected static readonly Type SByteType = typeof(sbyte);

		// Token: 0x04001D97 RID: 7575
		protected static readonly Type UInt16Type = typeof(ushort);

		// Token: 0x04001D98 RID: 7576
		protected static readonly Type UInt32Type = typeof(uint);

		// Token: 0x04001D99 RID: 7577
		protected static readonly Type UInt64Type = typeof(ulong);

		// Token: 0x04001D9A RID: 7578
		protected static readonly Type XPathItemType = typeof(XPathItem);

		// Token: 0x04001D9B RID: 7579
		protected static readonly Type DoubleType = typeof(double);

		// Token: 0x04001D9C RID: 7580
		protected static readonly Type SingleType = typeof(float);

		// Token: 0x04001D9D RID: 7581
		protected static readonly Type DateTimeType = typeof(DateTime);

		// Token: 0x04001D9E RID: 7582
		protected static readonly Type DateTimeOffsetType = typeof(DateTimeOffset);

		// Token: 0x04001D9F RID: 7583
		protected static readonly Type BooleanType = typeof(bool);

		// Token: 0x04001DA0 RID: 7584
		protected static readonly Type ByteArrayType = typeof(byte[]);

		// Token: 0x04001DA1 RID: 7585
		protected static readonly Type XmlQualifiedNameType = typeof(XmlQualifiedName);

		// Token: 0x04001DA2 RID: 7586
		protected static readonly Type UriType = typeof(Uri);

		// Token: 0x04001DA3 RID: 7587
		protected static readonly Type TimeSpanType = typeof(TimeSpan);

		// Token: 0x04001DA4 RID: 7588
		protected static readonly Type XPathNavigatorType = typeof(XPathNavigator);
	}
}
