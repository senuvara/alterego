﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="choice" /> element (compositor) from the XML Schema as specified by the World Wide Web Consortium (W3C). The <see langword="choice" /> allows only one of its children to appear in an instance. </summary>
	// Token: 0x0200040C RID: 1036
	public class XmlSchemaChoice : XmlSchemaGroupBase
	{
		/// <summary>Gets the collection of the elements contained with the compositor (<see langword="choice" />): <see langword="XmlSchemaElement" />, <see langword="XmlSchemaGroupRef" />, <see langword="XmlSchemaChoice" />, <see langword="XmlSchemaSequence" />, or <see langword="XmlSchemaAny" />.</summary>
		/// <returns>The collection of elements contained within <see langword="XmlSchemaChoice" />.</returns>
		// Token: 0x17000846 RID: 2118
		// (get) Token: 0x0600276C RID: 10092 RVA: 0x000DE815 File Offset: 0x000DCA15
		[XmlElement("choice", typeof(XmlSchemaChoice))]
		[XmlElement("sequence", typeof(XmlSchemaSequence))]
		[XmlElement("element", typeof(XmlSchemaElement))]
		[XmlElement("group", typeof(XmlSchemaGroupRef))]
		[XmlElement("any", typeof(XmlSchemaAny))]
		public override XmlSchemaObjectCollection Items
		{
			get
			{
				return this.items;
			}
		}

		// Token: 0x17000847 RID: 2119
		// (get) Token: 0x0600276D RID: 10093 RVA: 0x000DE81D File Offset: 0x000DCA1D
		internal override bool IsEmpty
		{
			get
			{
				return base.IsEmpty;
			}
		}

		// Token: 0x0600276E RID: 10094 RVA: 0x000DE825 File Offset: 0x000DCA25
		internal override void SetItems(XmlSchemaObjectCollection newItems)
		{
			this.items = newItems;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaChoice" /> class.</summary>
		// Token: 0x0600276F RID: 10095 RVA: 0x000DE82E File Offset: 0x000DCA2E
		public XmlSchemaChoice()
		{
		}

		// Token: 0x04001C02 RID: 7170
		private XmlSchemaObjectCollection items = new XmlSchemaObjectCollection();
	}
}
