﻿using System;
using System.Collections;
using Unity;

namespace System.Xml.Schema
{
	/// <summary>Represents the enumerator for the <see cref="T:System.Xml.Schema.XmlSchemaObjectCollection" />.</summary>
	// Token: 0x0200043D RID: 1085
	public class XmlSchemaObjectEnumerator : IEnumerator
	{
		// Token: 0x06002904 RID: 10500 RVA: 0x000E0C01 File Offset: 0x000DEE01
		internal XmlSchemaObjectEnumerator(IEnumerator enumerator)
		{
			this.enumerator = enumerator;
		}

		/// <summary>Resets the enumerator to the start of the collection.</summary>
		// Token: 0x06002905 RID: 10501 RVA: 0x000E0C10 File Offset: 0x000DEE10
		public void Reset()
		{
			this.enumerator.Reset();
		}

		/// <summary>Moves to the next item in the collection.</summary>
		/// <returns>
		///     <see langword="false" /> at the end of the collection.</returns>
		// Token: 0x06002906 RID: 10502 RVA: 0x000E0C1D File Offset: 0x000DEE1D
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		/// <summary>Gets the current <see cref="T:System.Xml.Schema.XmlSchemaObject" /> in the collection.</summary>
		/// <returns>The current <see cref="T:System.Xml.Schema.XmlSchemaObject" />.</returns>
		// Token: 0x170008DE RID: 2270
		// (get) Token: 0x06002907 RID: 10503 RVA: 0x000E0C2A File Offset: 0x000DEE2A
		public XmlSchemaObject Current
		{
			get
			{
				return (XmlSchemaObject)this.enumerator.Current;
			}
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.Schema.XmlSchemaObjectEnumerator.Reset" />.</summary>
		// Token: 0x06002908 RID: 10504 RVA: 0x000E0C10 File Offset: 0x000DEE10
		void IEnumerator.Reset()
		{
			this.enumerator.Reset();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.Schema.XmlSchemaObjectEnumerator.MoveNext" />.</summary>
		/// <returns>The next <see cref="T:System.Xml.Schema.XmlSchemaObject" />.</returns>
		// Token: 0x06002909 RID: 10505 RVA: 0x000E0C1D File Offset: 0x000DEE1D
		bool IEnumerator.MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		/// <summary>For a description of this member, see <see cref="P:System.Xml.Schema.XmlSchemaObjectEnumerator.Current" />.</summary>
		/// <returns>The current <see cref="T:System.Xml.Schema.XmlSchemaObject" />.</returns>
		// Token: 0x170008DF RID: 2271
		// (get) Token: 0x0600290A RID: 10506 RVA: 0x000E0C3C File Offset: 0x000DEE3C
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x0600290B RID: 10507 RVA: 0x00072668 File Offset: 0x00070868
		internal XmlSchemaObjectEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001CA0 RID: 7328
		private IEnumerator enumerator;
	}
}
