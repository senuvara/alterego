﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Abstract class for simple type content classes.</summary>
	// Token: 0x0200044F RID: 1103
	public abstract class XmlSchemaSimpleTypeContent : XmlSchemaAnnotated
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaSimpleTypeContent" /> class.</summary>
		// Token: 0x0600299D RID: 10653 RVA: 0x000DE4D5 File Offset: 0x000DC6D5
		protected XmlSchemaSimpleTypeContent()
		{
		}
	}
}
