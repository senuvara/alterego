﻿using System;
using System.Text;

namespace System.Xml.Schema
{
	// Token: 0x02000364 RID: 868
	internal class KeySequence
	{
		// Token: 0x06002150 RID: 8528 RVA: 0x000B94D3 File Offset: 0x000B76D3
		internal KeySequence(int dim, int line, int col)
		{
			this.dim = dim;
			this.ks = new TypedObject[dim];
			this.posline = line;
			this.poscol = col;
		}

		// Token: 0x17000685 RID: 1669
		// (get) Token: 0x06002151 RID: 8529 RVA: 0x000B9503 File Offset: 0x000B7703
		public int PosLine
		{
			get
			{
				return this.posline;
			}
		}

		// Token: 0x17000686 RID: 1670
		// (get) Token: 0x06002152 RID: 8530 RVA: 0x000B950B File Offset: 0x000B770B
		public int PosCol
		{
			get
			{
				return this.poscol;
			}
		}

		// Token: 0x06002153 RID: 8531 RVA: 0x000B9514 File Offset: 0x000B7714
		public KeySequence(TypedObject[] ks)
		{
			this.ks = ks;
			this.dim = ks.Length;
			this.posline = (this.poscol = 0);
		}

		// Token: 0x17000687 RID: 1671
		public object this[int index]
		{
			get
			{
				return this.ks[index];
			}
			set
			{
				this.ks[index] = (TypedObject)value;
			}
		}

		// Token: 0x06002156 RID: 8534 RVA: 0x000B9568 File Offset: 0x000B7768
		internal bool IsQualified()
		{
			for (int i = 0; i < this.ks.Length; i++)
			{
				if (this.ks[i] == null || this.ks[i].Value == null)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002157 RID: 8535 RVA: 0x000B95A4 File Offset: 0x000B77A4
		public override int GetHashCode()
		{
			if (this.hashcode != -1)
			{
				return this.hashcode;
			}
			this.hashcode = 0;
			for (int i = 0; i < this.ks.Length; i++)
			{
				this.ks[i].SetDecimal();
				if (this.ks[i].IsDecimal)
				{
					for (int j = 0; j < this.ks[i].Dim; j++)
					{
						this.hashcode += this.ks[i].Dvalue[j].GetHashCode();
					}
				}
				else
				{
					Array array = this.ks[i].Value as Array;
					if (array != null)
					{
						XmlAtomicValue[] array2 = array as XmlAtomicValue[];
						if (array2 != null)
						{
							for (int k = 0; k < array2.Length; k++)
							{
								this.hashcode += ((XmlAtomicValue)array2.GetValue(k)).TypedValue.GetHashCode();
							}
						}
						else
						{
							for (int l = 0; l < ((Array)this.ks[i].Value).Length; l++)
							{
								this.hashcode += ((Array)this.ks[i].Value).GetValue(l).GetHashCode();
							}
						}
					}
					else
					{
						this.hashcode += this.ks[i].Value.GetHashCode();
					}
				}
			}
			return this.hashcode;
		}

		// Token: 0x06002158 RID: 8536 RVA: 0x000B9714 File Offset: 0x000B7914
		public override bool Equals(object other)
		{
			KeySequence keySequence = (KeySequence)other;
			for (int i = 0; i < this.ks.Length; i++)
			{
				if (!this.ks[i].Equals(keySequence.ks[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002159 RID: 8537 RVA: 0x000B9758 File Offset: 0x000B7958
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.ks[0].ToString());
			for (int i = 1; i < this.ks.Length; i++)
			{
				stringBuilder.Append(" ");
				stringBuilder.Append(this.ks[i].ToString());
			}
			return stringBuilder.ToString();
		}

		// Token: 0x040017DC RID: 6108
		private TypedObject[] ks;

		// Token: 0x040017DD RID: 6109
		private int dim;

		// Token: 0x040017DE RID: 6110
		private int hashcode = -1;

		// Token: 0x040017DF RID: 6111
		private int posline;

		// Token: 0x040017E0 RID: 6112
		private int poscol;
	}
}
