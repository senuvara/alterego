﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Abstract class for defining <see langword="numeric" /> facets. This class is the base class for numeric facet classes such as <see cref="T:System.Xml.Schema.XmlSchemaMinLengthFacet" /></summary>
	// Token: 0x02000421 RID: 1057
	public abstract class XmlSchemaNumericFacet : XmlSchemaFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaNumericFacet" /> class.</summary>
		// Token: 0x06002875 RID: 10357 RVA: 0x000E05D7 File Offset: 0x000DE7D7
		protected XmlSchemaNumericFacet()
		{
		}
	}
}
