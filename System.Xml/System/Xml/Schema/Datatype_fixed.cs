﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003BE RID: 958
	internal class Datatype_fixed : Datatype_decimal
	{
		// Token: 0x0600237A RID: 9082 RVA: 0x000BED8C File Offset: 0x000BCF8C
		public override object ParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr)
		{
			Exception ex;
			try
			{
				Numeric10FacetsChecker numeric10FacetsChecker = this.FacetsChecker as Numeric10FacetsChecker;
				decimal num = XmlConvert.ToDecimal(s);
				ex = numeric10FacetsChecker.CheckTotalAndFractionDigits(num, 18, 4, true, true);
				if (ex == null)
				{
					return num;
				}
			}
			catch (XmlSchemaException ex2)
			{
				throw ex2;
			}
			catch (Exception innerException)
			{
				throw new XmlSchemaException(Res.GetString("The value '{0}' is invalid according to its data type.", new object[]
				{
					s
				}), innerException);
			}
			throw ex;
		}

		// Token: 0x0600237B RID: 9083 RVA: 0x000BEE04 File Offset: 0x000BD004
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			decimal num;
			Exception ex = XmlConvert.TryToDecimal(s, out num);
			if (ex == null)
			{
				ex = (this.FacetsChecker as Numeric10FacetsChecker).CheckTotalAndFractionDigits(num, 18, 4, true, true);
				if (ex == null)
				{
					typedValue = num;
					return null;
				}
			}
			return ex;
		}

		// Token: 0x0600237C RID: 9084 RVA: 0x000BE448 File Offset: 0x000BC648
		public Datatype_fixed()
		{
		}
	}
}
