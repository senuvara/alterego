﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000370 RID: 880
	internal sealed class PlusNode : InteriorNode
	{
		// Token: 0x06002194 RID: 8596 RVA: 0x000BA1D8 File Offset: 0x000B83D8
		public override void ConstructPos(BitSet firstpos, BitSet lastpos, BitSet[] followpos)
		{
			base.LeftChild.ConstructPos(firstpos, lastpos, followpos);
			for (int num = lastpos.NextSet(-1); num != -1; num = lastpos.NextSet(num))
			{
				followpos[num].Or(firstpos);
			}
		}

		// Token: 0x17000699 RID: 1689
		// (get) Token: 0x06002195 RID: 8597 RVA: 0x000BA212 File Offset: 0x000B8412
		public override bool IsNullable
		{
			get
			{
				return base.LeftChild.IsNullable;
			}
		}

		// Token: 0x06002196 RID: 8598 RVA: 0x000BA0D4 File Offset: 0x000B82D4
		public PlusNode()
		{
		}
	}
}
