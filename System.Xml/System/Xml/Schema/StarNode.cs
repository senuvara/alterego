﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000372 RID: 882
	internal sealed class StarNode : InteriorNode
	{
		// Token: 0x0600219A RID: 8602 RVA: 0x000BA230 File Offset: 0x000B8430
		public override void ConstructPos(BitSet firstpos, BitSet lastpos, BitSet[] followpos)
		{
			base.LeftChild.ConstructPos(firstpos, lastpos, followpos);
			for (int num = lastpos.NextSet(-1); num != -1; num = lastpos.NextSet(num))
			{
				followpos[num].Or(firstpos);
			}
		}

		// Token: 0x1700069B RID: 1691
		// (get) Token: 0x0600219B RID: 8603 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool IsNullable
		{
			get
			{
				return true;
			}
		}

		// Token: 0x0600219C RID: 8604 RVA: 0x000BA0D4 File Offset: 0x000B82D4
		public StarNode()
		{
		}
	}
}
