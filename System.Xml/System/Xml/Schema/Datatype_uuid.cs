﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003BF RID: 959
	internal class Datatype_uuid : Datatype_anySimpleType
	{
		// Token: 0x1700076C RID: 1900
		// (get) Token: 0x0600237D RID: 9085 RVA: 0x000BEE47 File Offset: 0x000BD047
		public override Type ValueType
		{
			get
			{
				return Datatype_uuid.atomicValueType;
			}
		}

		// Token: 0x1700076D RID: 1901
		// (get) Token: 0x0600237E RID: 9086 RVA: 0x000BEE4E File Offset: 0x000BD04E
		internal override Type ListValueType
		{
			get
			{
				return Datatype_uuid.listValueType;
			}
		}

		// Token: 0x1700076E RID: 1902
		// (get) Token: 0x0600237F RID: 9087 RVA: 0x000020CD File Offset: 0x000002CD
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return (RestrictionFlags)0;
			}
		}

		// Token: 0x06002380 RID: 9088 RVA: 0x000BEE58 File Offset: 0x000BD058
		internal override int Compare(object value1, object value2)
		{
			if (!((Guid)value1).Equals(value2))
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x06002381 RID: 9089 RVA: 0x000BEE80 File Offset: 0x000BD080
		public override object ParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr)
		{
			object result;
			try
			{
				result = XmlConvert.ToGuid(s);
			}
			catch (XmlSchemaException ex)
			{
				throw ex;
			}
			catch (Exception innerException)
			{
				throw new XmlSchemaException(Res.GetString("The value '{0}' is invalid according to its data type.", new object[]
				{
					s
				}), innerException);
			}
			return result;
		}

		// Token: 0x06002382 RID: 9090 RVA: 0x000BEED8 File Offset: 0x000BD0D8
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Guid guid;
			Exception ex = XmlConvert.TryToGuid(s, out guid);
			if (ex == null)
			{
				typedValue = guid;
				return null;
			}
			return ex;
		}

		// Token: 0x06002383 RID: 9091 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_uuid()
		{
		}

		// Token: 0x06002384 RID: 9092 RVA: 0x000BEF01 File Offset: 0x000BD101
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_uuid()
		{
		}

		// Token: 0x040018DE RID: 6366
		private static readonly Type atomicValueType = typeof(Guid);

		// Token: 0x040018DF RID: 6367
		private static readonly Type listValueType = typeof(Guid[]);
	}
}
