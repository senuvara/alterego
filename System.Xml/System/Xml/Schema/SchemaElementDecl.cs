﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Xml.Schema
{
	// Token: 0x020003E4 RID: 996
	internal sealed class SchemaElementDecl : SchemaDeclBase, IDtdAttributeListInfo
	{
		// Token: 0x06002536 RID: 9526 RVA: 0x000D23F0 File Offset: 0x000D05F0
		internal SchemaElementDecl()
		{
		}

		// Token: 0x06002537 RID: 9527 RVA: 0x000D240E File Offset: 0x000D060E
		internal SchemaElementDecl(XmlSchemaDatatype dtype)
		{
			base.Datatype = dtype;
			this.contentValidator = ContentValidator.TextOnly;
		}

		// Token: 0x06002538 RID: 9528 RVA: 0x000D243E File Offset: 0x000D063E
		internal SchemaElementDecl(XmlQualifiedName name, string prefix) : base(name, prefix)
		{
		}

		// Token: 0x06002539 RID: 9529 RVA: 0x000D245E File Offset: 0x000D065E
		internal static SchemaElementDecl CreateAnyTypeElementDecl()
		{
			return new SchemaElementDecl
			{
				Datatype = DatatypeImplementation.AnySimpleType.Datatype
			};
		}

		// Token: 0x170007A6 RID: 1958
		// (get) Token: 0x0600253A RID: 9530 RVA: 0x000CAC1C File Offset: 0x000C8E1C
		string IDtdAttributeListInfo.Prefix
		{
			get
			{
				return base.Prefix;
			}
		}

		// Token: 0x170007A7 RID: 1959
		// (get) Token: 0x0600253B RID: 9531 RVA: 0x000CAC24 File Offset: 0x000C8E24
		string IDtdAttributeListInfo.LocalName
		{
			get
			{
				return base.Name.Name;
			}
		}

		// Token: 0x170007A8 RID: 1960
		// (get) Token: 0x0600253C RID: 9532 RVA: 0x000D2475 File Offset: 0x000D0675
		bool IDtdAttributeListInfo.HasNonCDataAttributes
		{
			get
			{
				return this.hasNonCDataAttribute;
			}
		}

		// Token: 0x0600253D RID: 9533 RVA: 0x000D2480 File Offset: 0x000D0680
		IDtdAttributeInfo IDtdAttributeListInfo.LookupAttribute(string prefix, string localName)
		{
			XmlQualifiedName key = new XmlQualifiedName(localName, prefix);
			SchemaAttDef result;
			if (this.attdefs.TryGetValue(key, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x0600253E RID: 9534 RVA: 0x000D24A8 File Offset: 0x000D06A8
		IEnumerable<IDtdDefaultAttributeInfo> IDtdAttributeListInfo.LookupDefaultAttributes()
		{
			return this.defaultAttdefs;
		}

		// Token: 0x0600253F RID: 9535 RVA: 0x000D24B0 File Offset: 0x000D06B0
		IDtdAttributeInfo IDtdAttributeListInfo.LookupIdAttribute()
		{
			foreach (SchemaAttDef schemaAttDef in this.attdefs.Values)
			{
				if (schemaAttDef.TokenizedType == XmlTokenizedType.ID)
				{
					return schemaAttDef;
				}
			}
			return null;
		}

		// Token: 0x170007A9 RID: 1961
		// (get) Token: 0x06002540 RID: 9536 RVA: 0x000D2514 File Offset: 0x000D0714
		// (set) Token: 0x06002541 RID: 9537 RVA: 0x000D251C File Offset: 0x000D071C
		internal bool IsIdDeclared
		{
			get
			{
				return this.isIdDeclared;
			}
			set
			{
				this.isIdDeclared = value;
			}
		}

		// Token: 0x170007AA RID: 1962
		// (get) Token: 0x06002542 RID: 9538 RVA: 0x000D2475 File Offset: 0x000D0675
		// (set) Token: 0x06002543 RID: 9539 RVA: 0x000D2525 File Offset: 0x000D0725
		internal bool HasNonCDataAttribute
		{
			get
			{
				return this.hasNonCDataAttribute;
			}
			set
			{
				this.hasNonCDataAttribute = value;
			}
		}

		// Token: 0x06002544 RID: 9540 RVA: 0x000D252E File Offset: 0x000D072E
		internal SchemaElementDecl Clone()
		{
			return (SchemaElementDecl)base.MemberwiseClone();
		}

		// Token: 0x170007AB RID: 1963
		// (get) Token: 0x06002545 RID: 9541 RVA: 0x000D253B File Offset: 0x000D073B
		// (set) Token: 0x06002546 RID: 9542 RVA: 0x000D2543 File Offset: 0x000D0743
		internal bool IsAbstract
		{
			get
			{
				return this.isAbstract;
			}
			set
			{
				this.isAbstract = value;
			}
		}

		// Token: 0x170007AC RID: 1964
		// (get) Token: 0x06002547 RID: 9543 RVA: 0x000D254C File Offset: 0x000D074C
		// (set) Token: 0x06002548 RID: 9544 RVA: 0x000D2554 File Offset: 0x000D0754
		internal bool IsNillable
		{
			get
			{
				return this.isNillable;
			}
			set
			{
				this.isNillable = value;
			}
		}

		// Token: 0x170007AD RID: 1965
		// (get) Token: 0x06002549 RID: 9545 RVA: 0x000D255D File Offset: 0x000D075D
		// (set) Token: 0x0600254A RID: 9546 RVA: 0x000D2565 File Offset: 0x000D0765
		internal XmlSchemaDerivationMethod Block
		{
			get
			{
				return this.block;
			}
			set
			{
				this.block = value;
			}
		}

		// Token: 0x170007AE RID: 1966
		// (get) Token: 0x0600254B RID: 9547 RVA: 0x000D256E File Offset: 0x000D076E
		// (set) Token: 0x0600254C RID: 9548 RVA: 0x000D2576 File Offset: 0x000D0776
		internal bool IsNotationDeclared
		{
			get
			{
				return this.isNotationDeclared;
			}
			set
			{
				this.isNotationDeclared = value;
			}
		}

		// Token: 0x170007AF RID: 1967
		// (get) Token: 0x0600254D RID: 9549 RVA: 0x000D257F File Offset: 0x000D077F
		internal bool HasDefaultAttribute
		{
			get
			{
				return this.defaultAttdefs != null;
			}
		}

		// Token: 0x170007B0 RID: 1968
		// (get) Token: 0x0600254E RID: 9550 RVA: 0x000D258A File Offset: 0x000D078A
		// (set) Token: 0x0600254F RID: 9551 RVA: 0x000D2592 File Offset: 0x000D0792
		internal bool HasRequiredAttribute
		{
			get
			{
				return this.hasRequiredAttribute;
			}
			set
			{
				this.hasRequiredAttribute = value;
			}
		}

		// Token: 0x170007B1 RID: 1969
		// (get) Token: 0x06002550 RID: 9552 RVA: 0x000D259B File Offset: 0x000D079B
		// (set) Token: 0x06002551 RID: 9553 RVA: 0x000D25A3 File Offset: 0x000D07A3
		internal ContentValidator ContentValidator
		{
			get
			{
				return this.contentValidator;
			}
			set
			{
				this.contentValidator = value;
			}
		}

		// Token: 0x170007B2 RID: 1970
		// (get) Token: 0x06002552 RID: 9554 RVA: 0x000D25AC File Offset: 0x000D07AC
		// (set) Token: 0x06002553 RID: 9555 RVA: 0x000D25B4 File Offset: 0x000D07B4
		internal XmlSchemaAnyAttribute AnyAttribute
		{
			get
			{
				return this.anyAttribute;
			}
			set
			{
				this.anyAttribute = value;
			}
		}

		// Token: 0x170007B3 RID: 1971
		// (get) Token: 0x06002554 RID: 9556 RVA: 0x000D25BD File Offset: 0x000D07BD
		// (set) Token: 0x06002555 RID: 9557 RVA: 0x000D25C5 File Offset: 0x000D07C5
		internal CompiledIdentityConstraint[] Constraints
		{
			get
			{
				return this.constraints;
			}
			set
			{
				this.constraints = value;
			}
		}

		// Token: 0x170007B4 RID: 1972
		// (get) Token: 0x06002556 RID: 9558 RVA: 0x000D25CE File Offset: 0x000D07CE
		// (set) Token: 0x06002557 RID: 9559 RVA: 0x000D25D6 File Offset: 0x000D07D6
		internal XmlSchemaElement SchemaElement
		{
			get
			{
				return this.schemaElement;
			}
			set
			{
				this.schemaElement = value;
			}
		}

		// Token: 0x06002558 RID: 9560 RVA: 0x000D25E0 File Offset: 0x000D07E0
		internal void AddAttDef(SchemaAttDef attdef)
		{
			this.attdefs.Add(attdef.Name, attdef);
			if (attdef.Presence == SchemaDeclBase.Use.Required || attdef.Presence == SchemaDeclBase.Use.RequiredFixed)
			{
				this.hasRequiredAttribute = true;
			}
			if (attdef.Presence == SchemaDeclBase.Use.Default || attdef.Presence == SchemaDeclBase.Use.Fixed)
			{
				if (this.defaultAttdefs == null)
				{
					this.defaultAttdefs = new List<IDtdDefaultAttributeInfo>();
				}
				this.defaultAttdefs.Add(attdef);
			}
		}

		// Token: 0x06002559 RID: 9561 RVA: 0x000D2648 File Offset: 0x000D0848
		internal SchemaAttDef GetAttDef(XmlQualifiedName qname)
		{
			SchemaAttDef result;
			if (this.attdefs.TryGetValue(qname, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x170007B5 RID: 1973
		// (get) Token: 0x0600255A RID: 9562 RVA: 0x000D24A8 File Offset: 0x000D06A8
		internal IList<IDtdDefaultAttributeInfo> DefaultAttDefs
		{
			get
			{
				return this.defaultAttdefs;
			}
		}

		// Token: 0x170007B6 RID: 1974
		// (get) Token: 0x0600255B RID: 9563 RVA: 0x000D2668 File Offset: 0x000D0868
		internal Dictionary<XmlQualifiedName, SchemaAttDef> AttDefs
		{
			get
			{
				return this.attdefs;
			}
		}

		// Token: 0x170007B7 RID: 1975
		// (get) Token: 0x0600255C RID: 9564 RVA: 0x000D2670 File Offset: 0x000D0870
		internal Dictionary<XmlQualifiedName, XmlQualifiedName> ProhibitedAttributes
		{
			get
			{
				return this.prohibitedAttributes;
			}
		}

		// Token: 0x0600255D RID: 9565 RVA: 0x000D2678 File Offset: 0x000D0878
		internal void CheckAttributes(Hashtable presence, bool standalone)
		{
			foreach (SchemaAttDef schemaAttDef in this.attdefs.Values)
			{
				if (presence[schemaAttDef.Name] == null)
				{
					if (schemaAttDef.Presence == SchemaDeclBase.Use.Required)
					{
						throw new XmlSchemaException("The required attribute '{0}' is missing.", schemaAttDef.Name.ToString());
					}
					if (standalone && schemaAttDef.IsDeclaredInExternal && (schemaAttDef.Presence == SchemaDeclBase.Use.Default || schemaAttDef.Presence == SchemaDeclBase.Use.Fixed))
					{
						throw new XmlSchemaException("The standalone document declaration must have a value of 'no'.", string.Empty);
					}
				}
			}
		}

		// Token: 0x0600255E RID: 9566 RVA: 0x000D2724 File Offset: 0x000D0924
		// Note: this type is marked as 'beforefieldinit'.
		static SchemaElementDecl()
		{
		}

		// Token: 0x040019C4 RID: 6596
		private Dictionary<XmlQualifiedName, SchemaAttDef> attdefs = new Dictionary<XmlQualifiedName, SchemaAttDef>();

		// Token: 0x040019C5 RID: 6597
		private List<IDtdDefaultAttributeInfo> defaultAttdefs;

		// Token: 0x040019C6 RID: 6598
		private bool isIdDeclared;

		// Token: 0x040019C7 RID: 6599
		private bool hasNonCDataAttribute;

		// Token: 0x040019C8 RID: 6600
		private bool isAbstract;

		// Token: 0x040019C9 RID: 6601
		private bool isNillable;

		// Token: 0x040019CA RID: 6602
		private bool hasRequiredAttribute;

		// Token: 0x040019CB RID: 6603
		private bool isNotationDeclared;

		// Token: 0x040019CC RID: 6604
		private Dictionary<XmlQualifiedName, XmlQualifiedName> prohibitedAttributes = new Dictionary<XmlQualifiedName, XmlQualifiedName>();

		// Token: 0x040019CD RID: 6605
		private ContentValidator contentValidator;

		// Token: 0x040019CE RID: 6606
		private XmlSchemaAnyAttribute anyAttribute;

		// Token: 0x040019CF RID: 6607
		private XmlSchemaDerivationMethod block;

		// Token: 0x040019D0 RID: 6608
		private CompiledIdentityConstraint[] constraints;

		// Token: 0x040019D1 RID: 6609
		private XmlSchemaElement schemaElement;

		// Token: 0x040019D2 RID: 6610
		internal static readonly SchemaElementDecl Empty = new SchemaElementDecl();
	}
}
