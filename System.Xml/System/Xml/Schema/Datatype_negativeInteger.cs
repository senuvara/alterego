﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003AE RID: 942
	internal class Datatype_negativeInteger : Datatype_nonPositiveInteger
	{
		// Token: 0x1700073D RID: 1853
		// (get) Token: 0x06002318 RID: 8984 RVA: 0x000BE47E File Offset: 0x000BC67E
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_negativeInteger.numeric10FacetsChecker;
			}
		}

		// Token: 0x1700073E RID: 1854
		// (get) Token: 0x06002319 RID: 8985 RVA: 0x000BE485 File Offset: 0x000BC685
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.NegativeInteger;
			}
		}

		// Token: 0x0600231A RID: 8986 RVA: 0x000BE489 File Offset: 0x000BC689
		public Datatype_negativeInteger()
		{
		}

		// Token: 0x0600231B RID: 8987 RVA: 0x000BE491 File Offset: 0x000BC691
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_negativeInteger()
		{
		}

		// Token: 0x040018BF RID: 6335
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(decimal.MinValue, -1m);
	}
}
