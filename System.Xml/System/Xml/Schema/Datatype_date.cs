﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000396 RID: 918
	internal class Datatype_date : Datatype_dateTimeBase
	{
		// Token: 0x170006FF RID: 1791
		// (get) Token: 0x060022AC RID: 8876 RVA: 0x000BDF2D File Offset: 0x000BC12D
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Date;
			}
		}

		// Token: 0x060022AD RID: 8877 RVA: 0x000BDF31 File Offset: 0x000BC131
		internal Datatype_date() : base(XsdDateTimeFlags.Date)
		{
		}
	}
}
