﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000365 RID: 869
	internal class UpaException : Exception
	{
		// Token: 0x0600215A RID: 8538 RVA: 0x000B97B8 File Offset: 0x000B79B8
		public UpaException(object particle1, object particle2)
		{
			this.particle1 = particle1;
			this.particle2 = particle2;
		}

		// Token: 0x17000688 RID: 1672
		// (get) Token: 0x0600215B RID: 8539 RVA: 0x000B97CE File Offset: 0x000B79CE
		public object Particle1
		{
			get
			{
				return this.particle1;
			}
		}

		// Token: 0x17000689 RID: 1673
		// (get) Token: 0x0600215C RID: 8540 RVA: 0x000B97D6 File Offset: 0x000B79D6
		public object Particle2
		{
			get
			{
				return this.particle2;
			}
		}

		// Token: 0x040017E1 RID: 6113
		private object particle1;

		// Token: 0x040017E2 RID: 6114
		private object particle2;
	}
}
