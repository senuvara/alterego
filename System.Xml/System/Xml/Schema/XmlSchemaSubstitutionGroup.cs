﻿using System;
using System.Collections;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	// Token: 0x02000453 RID: 1107
	internal class XmlSchemaSubstitutionGroup : XmlSchemaObject
	{
		// Token: 0x17000920 RID: 2336
		// (get) Token: 0x060029B4 RID: 10676 RVA: 0x000E37E8 File Offset: 0x000E19E8
		[XmlIgnore]
		internal ArrayList Members
		{
			get
			{
				return this.membersList;
			}
		}

		// Token: 0x17000921 RID: 2337
		// (get) Token: 0x060029B5 RID: 10677 RVA: 0x000E37F0 File Offset: 0x000E19F0
		// (set) Token: 0x060029B6 RID: 10678 RVA: 0x000E37F8 File Offset: 0x000E19F8
		[XmlIgnore]
		internal XmlQualifiedName Examplar
		{
			get
			{
				return this.examplar;
			}
			set
			{
				this.examplar = value;
			}
		}

		// Token: 0x060029B7 RID: 10679 RVA: 0x000E3801 File Offset: 0x000E1A01
		public XmlSchemaSubstitutionGroup()
		{
		}

		// Token: 0x04001CE7 RID: 7399
		private ArrayList membersList = new ArrayList();

		// Token: 0x04001CE8 RID: 7400
		private XmlQualifiedName examplar = XmlQualifiedName.Empty;
	}
}
