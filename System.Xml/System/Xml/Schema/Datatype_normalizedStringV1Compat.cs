﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A1 RID: 929
	internal class Datatype_normalizedStringV1Compat : Datatype_string
	{
		// Token: 0x17000722 RID: 1826
		// (get) Token: 0x060022E9 RID: 8937 RVA: 0x000BE228 File Offset: 0x000BC428
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.NormalizedString;
			}
		}

		// Token: 0x17000723 RID: 1827
		// (get) Token: 0x060022EA RID: 8938 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool HasValueFacets
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060022EB RID: 8939 RVA: 0x000BE22C File Offset: 0x000BC42C
		public Datatype_normalizedStringV1Compat()
		{
		}
	}
}
