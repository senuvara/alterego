﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003AB RID: 939
	internal class Datatype_NOTATION : Datatype_anySimpleType
	{
		// Token: 0x06002304 RID: 8964 RVA: 0x000BDBF8 File Offset: 0x000BBDF8
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlMiscConverter.Create(schemaType);
		}

		// Token: 0x17000732 RID: 1842
		// (get) Token: 0x06002305 RID: 8965 RVA: 0x000BE16A File Offset: 0x000BC36A
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.qnameFacetsChecker;
			}
		}

		// Token: 0x17000733 RID: 1843
		// (get) Token: 0x06002306 RID: 8966 RVA: 0x000BE2B9 File Offset: 0x000BC4B9
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Notation;
			}
		}

		// Token: 0x17000734 RID: 1844
		// (get) Token: 0x06002307 RID: 8967 RVA: 0x00072BF5 File Offset: 0x00070DF5
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.NOTATION;
			}
		}

		// Token: 0x17000735 RID: 1845
		// (get) Token: 0x06002308 RID: 8968 RVA: 0x000BD2C1 File Offset: 0x000BB4C1
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Length | RestrictionFlags.MinLength | RestrictionFlags.MaxLength | RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace;
			}
		}

		// Token: 0x17000736 RID: 1846
		// (get) Token: 0x06002309 RID: 8969 RVA: 0x000BE2BD File Offset: 0x000BC4BD
		public override Type ValueType
		{
			get
			{
				return Datatype_NOTATION.atomicValueType;
			}
		}

		// Token: 0x17000737 RID: 1847
		// (get) Token: 0x0600230A RID: 8970 RVA: 0x000BE2C4 File Offset: 0x000BC4C4
		internal override Type ListValueType
		{
			get
			{
				return Datatype_NOTATION.listValueType;
			}
		}

		// Token: 0x17000738 RID: 1848
		// (get) Token: 0x0600230B RID: 8971 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x0600230C RID: 8972 RVA: 0x000BE2CC File Offset: 0x000BC4CC
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			if (s == null || s.Length == 0)
			{
				return new XmlSchemaException("The attribute value cannot be empty.", string.Empty);
			}
			Exception ex = DatatypeImplementation.qnameFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				XmlQualifiedName xmlQualifiedName = null;
				try
				{
					string text;
					xmlQualifiedName = XmlQualifiedName.Parse(s, nsmgr, out text);
				}
				catch (ArgumentException ex)
				{
					return ex;
				}
				catch (XmlException ex)
				{
					return ex;
				}
				ex = DatatypeImplementation.qnameFacetsChecker.CheckValueFacets(xmlQualifiedName, this);
				if (ex == null)
				{
					typedValue = xmlQualifiedName;
					return null;
				}
			}
			return ex;
		}

		// Token: 0x0600230D RID: 8973 RVA: 0x000BE350 File Offset: 0x000BC550
		internal override void VerifySchemaValid(XmlSchemaObjectTable notations, XmlSchemaObject caller)
		{
			for (Datatype_NOTATION datatype_NOTATION = this; datatype_NOTATION != null; datatype_NOTATION = (Datatype_NOTATION)datatype_NOTATION.Base)
			{
				if (datatype_NOTATION.Restriction != null && (datatype_NOTATION.Restriction.Flags & RestrictionFlags.Enumeration) != (RestrictionFlags)0)
				{
					for (int i = 0; i < datatype_NOTATION.Restriction.Enumeration.Count; i++)
					{
						XmlQualifiedName name = (XmlQualifiedName)datatype_NOTATION.Restriction.Enumeration[i];
						if (!notations.Contains(name))
						{
							throw new XmlSchemaException("NOTATION cannot be used directly in a schema; only data types derived from NOTATION by specifying an enumeration value can be used in a schema. All enumeration facet values must match the name of a notation declared in the current schema.", caller);
						}
					}
					return;
				}
			}
			throw new XmlSchemaException("NOTATION cannot be used directly in a schema; only data types derived from NOTATION by specifying an enumeration value can be used in a schema. All enumeration facet values must match the name of a notation declared in the current schema.", caller);
		}

		// Token: 0x0600230E RID: 8974 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_NOTATION()
		{
		}

		// Token: 0x0600230F RID: 8975 RVA: 0x000BE3DB File Offset: 0x000BC5DB
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_NOTATION()
		{
		}

		// Token: 0x040018BC RID: 6332
		private static readonly Type atomicValueType = typeof(XmlQualifiedName);

		// Token: 0x040018BD RID: 6333
		private static readonly Type listValueType = typeof(XmlQualifiedName[]);
	}
}
