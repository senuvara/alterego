﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000369 RID: 873
	internal abstract class SyntaxTreeNode
	{
		// Token: 0x0600216F RID: 8559
		public abstract void ExpandTree(InteriorNode parent, SymbolsDictionary symbols, Positions positions);

		// Token: 0x06002170 RID: 8560
		public abstract SyntaxTreeNode Clone(Positions positions);

		// Token: 0x06002171 RID: 8561
		public abstract void ConstructPos(BitSet firstpos, BitSet lastpos, BitSet[] followpos);

		// Token: 0x17000690 RID: 1680
		// (get) Token: 0x06002172 RID: 8562
		public abstract bool IsNullable { get; }

		// Token: 0x17000691 RID: 1681
		// (get) Token: 0x06002173 RID: 8563 RVA: 0x000020CD File Offset: 0x000002CD
		public virtual bool IsRangeNode
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06002174 RID: 8564 RVA: 0x00002103 File Offset: 0x00000303
		protected SyntaxTreeNode()
		{
		}
	}
}
