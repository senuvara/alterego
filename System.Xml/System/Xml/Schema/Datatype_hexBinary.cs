﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200039C RID: 924
	internal class Datatype_hexBinary : Datatype_anySimpleType
	{
		// Token: 0x060022B8 RID: 8888 RVA: 0x000BDBF8 File Offset: 0x000BBDF8
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlMiscConverter.Create(schemaType);
		}

		// Token: 0x17000705 RID: 1797
		// (get) Token: 0x060022B9 RID: 8889 RVA: 0x000BDF7A File Offset: 0x000BC17A
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.binaryFacetsChecker;
			}
		}

		// Token: 0x17000706 RID: 1798
		// (get) Token: 0x060022BA RID: 8890 RVA: 0x000BDF81 File Offset: 0x000BC181
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.HexBinary;
			}
		}

		// Token: 0x17000707 RID: 1799
		// (get) Token: 0x060022BB RID: 8891 RVA: 0x000BDF85 File Offset: 0x000BC185
		public override Type ValueType
		{
			get
			{
				return Datatype_hexBinary.atomicValueType;
			}
		}

		// Token: 0x17000708 RID: 1800
		// (get) Token: 0x060022BC RID: 8892 RVA: 0x000BDF8C File Offset: 0x000BC18C
		internal override Type ListValueType
		{
			get
			{
				return Datatype_hexBinary.listValueType;
			}
		}

		// Token: 0x17000709 RID: 1801
		// (get) Token: 0x060022BD RID: 8893 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x1700070A RID: 1802
		// (get) Token: 0x060022BE RID: 8894 RVA: 0x000BD2C1 File Offset: 0x000BB4C1
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Length | RestrictionFlags.MinLength | RestrictionFlags.MaxLength | RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace;
			}
		}

		// Token: 0x060022BF RID: 8895 RVA: 0x000BDF93 File Offset: 0x000BC193
		internal override int Compare(object value1, object value2)
		{
			return base.Compare((byte[])value1, (byte[])value2);
		}

		// Token: 0x060022C0 RID: 8896 RVA: 0x000BDFA8 File Offset: 0x000BC1A8
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.binaryFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				byte[] array = null;
				try
				{
					array = XmlConvert.FromBinHexString(s, false);
				}
				catch (ArgumentException ex)
				{
					return ex;
				}
				catch (XmlException ex)
				{
					return ex;
				}
				ex = DatatypeImplementation.binaryFacetsChecker.CheckValueFacets(array, this);
				if (ex == null)
				{
					typedValue = array;
					return null;
				}
			}
			return ex;
		}

		// Token: 0x060022C1 RID: 8897 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_hexBinary()
		{
		}

		// Token: 0x060022C2 RID: 8898 RVA: 0x000BE010 File Offset: 0x000BC210
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_hexBinary()
		{
		}

		// Token: 0x040018B4 RID: 6324
		private static readonly Type atomicValueType = typeof(byte[]);

		// Token: 0x040018B5 RID: 6325
		private static readonly Type listValueType = typeof(byte[][]);
	}
}
