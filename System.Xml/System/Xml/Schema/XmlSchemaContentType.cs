﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Enumerations for the content model of the complex type. This represents the content in the post-schema-validation information set (infoset).</summary>
	// Token: 0x02000418 RID: 1048
	public enum XmlSchemaContentType
	{
		/// <summary>Text-only content.</summary>
		// Token: 0x04001C32 RID: 7218
		TextOnly,
		/// <summary>Empty content.</summary>
		// Token: 0x04001C33 RID: 7219
		Empty,
		/// <summary>Element-only content.</summary>
		// Token: 0x04001C34 RID: 7220
		ElementOnly,
		/// <summary>Mixed content.</summary>
		// Token: 0x04001C35 RID: 7221
		Mixed
	}
}
