﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000460 RID: 1120
	internal abstract class XmlValueConverter
	{
		// Token: 0x06002A60 RID: 10848
		public abstract bool ToBoolean(bool value);

		// Token: 0x06002A61 RID: 10849
		public abstract bool ToBoolean(long value);

		// Token: 0x06002A62 RID: 10850
		public abstract bool ToBoolean(int value);

		// Token: 0x06002A63 RID: 10851
		public abstract bool ToBoolean(decimal value);

		// Token: 0x06002A64 RID: 10852
		public abstract bool ToBoolean(float value);

		// Token: 0x06002A65 RID: 10853
		public abstract bool ToBoolean(double value);

		// Token: 0x06002A66 RID: 10854
		public abstract bool ToBoolean(DateTime value);

		// Token: 0x06002A67 RID: 10855
		public abstract bool ToBoolean(DateTimeOffset value);

		// Token: 0x06002A68 RID: 10856
		public abstract bool ToBoolean(string value);

		// Token: 0x06002A69 RID: 10857
		public abstract bool ToBoolean(object value);

		// Token: 0x06002A6A RID: 10858
		public abstract int ToInt32(bool value);

		// Token: 0x06002A6B RID: 10859
		public abstract int ToInt32(int value);

		// Token: 0x06002A6C RID: 10860
		public abstract int ToInt32(long value);

		// Token: 0x06002A6D RID: 10861
		public abstract int ToInt32(decimal value);

		// Token: 0x06002A6E RID: 10862
		public abstract int ToInt32(float value);

		// Token: 0x06002A6F RID: 10863
		public abstract int ToInt32(double value);

		// Token: 0x06002A70 RID: 10864
		public abstract int ToInt32(DateTime value);

		// Token: 0x06002A71 RID: 10865
		public abstract int ToInt32(DateTimeOffset value);

		// Token: 0x06002A72 RID: 10866
		public abstract int ToInt32(string value);

		// Token: 0x06002A73 RID: 10867
		public abstract int ToInt32(object value);

		// Token: 0x06002A74 RID: 10868
		public abstract long ToInt64(bool value);

		// Token: 0x06002A75 RID: 10869
		public abstract long ToInt64(int value);

		// Token: 0x06002A76 RID: 10870
		public abstract long ToInt64(long value);

		// Token: 0x06002A77 RID: 10871
		public abstract long ToInt64(decimal value);

		// Token: 0x06002A78 RID: 10872
		public abstract long ToInt64(float value);

		// Token: 0x06002A79 RID: 10873
		public abstract long ToInt64(double value);

		// Token: 0x06002A7A RID: 10874
		public abstract long ToInt64(DateTime value);

		// Token: 0x06002A7B RID: 10875
		public abstract long ToInt64(DateTimeOffset value);

		// Token: 0x06002A7C RID: 10876
		public abstract long ToInt64(string value);

		// Token: 0x06002A7D RID: 10877
		public abstract long ToInt64(object value);

		// Token: 0x06002A7E RID: 10878
		public abstract decimal ToDecimal(bool value);

		// Token: 0x06002A7F RID: 10879
		public abstract decimal ToDecimal(int value);

		// Token: 0x06002A80 RID: 10880
		public abstract decimal ToDecimal(long value);

		// Token: 0x06002A81 RID: 10881
		public abstract decimal ToDecimal(decimal value);

		// Token: 0x06002A82 RID: 10882
		public abstract decimal ToDecimal(float value);

		// Token: 0x06002A83 RID: 10883
		public abstract decimal ToDecimal(double value);

		// Token: 0x06002A84 RID: 10884
		public abstract decimal ToDecimal(DateTime value);

		// Token: 0x06002A85 RID: 10885
		public abstract decimal ToDecimal(DateTimeOffset value);

		// Token: 0x06002A86 RID: 10886
		public abstract decimal ToDecimal(string value);

		// Token: 0x06002A87 RID: 10887
		public abstract decimal ToDecimal(object value);

		// Token: 0x06002A88 RID: 10888
		public abstract double ToDouble(bool value);

		// Token: 0x06002A89 RID: 10889
		public abstract double ToDouble(int value);

		// Token: 0x06002A8A RID: 10890
		public abstract double ToDouble(long value);

		// Token: 0x06002A8B RID: 10891
		public abstract double ToDouble(decimal value);

		// Token: 0x06002A8C RID: 10892
		public abstract double ToDouble(float value);

		// Token: 0x06002A8D RID: 10893
		public abstract double ToDouble(double value);

		// Token: 0x06002A8E RID: 10894
		public abstract double ToDouble(DateTime value);

		// Token: 0x06002A8F RID: 10895
		public abstract double ToDouble(DateTimeOffset value);

		// Token: 0x06002A90 RID: 10896
		public abstract double ToDouble(string value);

		// Token: 0x06002A91 RID: 10897
		public abstract double ToDouble(object value);

		// Token: 0x06002A92 RID: 10898
		public abstract float ToSingle(bool value);

		// Token: 0x06002A93 RID: 10899
		public abstract float ToSingle(int value);

		// Token: 0x06002A94 RID: 10900
		public abstract float ToSingle(long value);

		// Token: 0x06002A95 RID: 10901
		public abstract float ToSingle(decimal value);

		// Token: 0x06002A96 RID: 10902
		public abstract float ToSingle(float value);

		// Token: 0x06002A97 RID: 10903
		public abstract float ToSingle(double value);

		// Token: 0x06002A98 RID: 10904
		public abstract float ToSingle(DateTime value);

		// Token: 0x06002A99 RID: 10905
		public abstract float ToSingle(DateTimeOffset value);

		// Token: 0x06002A9A RID: 10906
		public abstract float ToSingle(string value);

		// Token: 0x06002A9B RID: 10907
		public abstract float ToSingle(object value);

		// Token: 0x06002A9C RID: 10908
		public abstract DateTime ToDateTime(bool value);

		// Token: 0x06002A9D RID: 10909
		public abstract DateTime ToDateTime(int value);

		// Token: 0x06002A9E RID: 10910
		public abstract DateTime ToDateTime(long value);

		// Token: 0x06002A9F RID: 10911
		public abstract DateTime ToDateTime(decimal value);

		// Token: 0x06002AA0 RID: 10912
		public abstract DateTime ToDateTime(float value);

		// Token: 0x06002AA1 RID: 10913
		public abstract DateTime ToDateTime(double value);

		// Token: 0x06002AA2 RID: 10914
		public abstract DateTime ToDateTime(DateTime value);

		// Token: 0x06002AA3 RID: 10915
		public abstract DateTime ToDateTime(DateTimeOffset value);

		// Token: 0x06002AA4 RID: 10916
		public abstract DateTime ToDateTime(string value);

		// Token: 0x06002AA5 RID: 10917
		public abstract DateTime ToDateTime(object value);

		// Token: 0x06002AA6 RID: 10918
		public abstract DateTimeOffset ToDateTimeOffset(bool value);

		// Token: 0x06002AA7 RID: 10919
		public abstract DateTimeOffset ToDateTimeOffset(int value);

		// Token: 0x06002AA8 RID: 10920
		public abstract DateTimeOffset ToDateTimeOffset(long value);

		// Token: 0x06002AA9 RID: 10921
		public abstract DateTimeOffset ToDateTimeOffset(decimal value);

		// Token: 0x06002AAA RID: 10922
		public abstract DateTimeOffset ToDateTimeOffset(float value);

		// Token: 0x06002AAB RID: 10923
		public abstract DateTimeOffset ToDateTimeOffset(double value);

		// Token: 0x06002AAC RID: 10924
		public abstract DateTimeOffset ToDateTimeOffset(DateTime value);

		// Token: 0x06002AAD RID: 10925
		public abstract DateTimeOffset ToDateTimeOffset(DateTimeOffset value);

		// Token: 0x06002AAE RID: 10926
		public abstract DateTimeOffset ToDateTimeOffset(string value);

		// Token: 0x06002AAF RID: 10927
		public abstract DateTimeOffset ToDateTimeOffset(object value);

		// Token: 0x06002AB0 RID: 10928
		public abstract string ToString(bool value);

		// Token: 0x06002AB1 RID: 10929
		public abstract string ToString(int value);

		// Token: 0x06002AB2 RID: 10930
		public abstract string ToString(long value);

		// Token: 0x06002AB3 RID: 10931
		public abstract string ToString(decimal value);

		// Token: 0x06002AB4 RID: 10932
		public abstract string ToString(float value);

		// Token: 0x06002AB5 RID: 10933
		public abstract string ToString(double value);

		// Token: 0x06002AB6 RID: 10934
		public abstract string ToString(DateTime value);

		// Token: 0x06002AB7 RID: 10935
		public abstract string ToString(DateTimeOffset value);

		// Token: 0x06002AB8 RID: 10936
		public abstract string ToString(string value);

		// Token: 0x06002AB9 RID: 10937
		public abstract string ToString(string value, IXmlNamespaceResolver nsResolver);

		// Token: 0x06002ABA RID: 10938
		public abstract string ToString(object value);

		// Token: 0x06002ABB RID: 10939
		public abstract string ToString(object value, IXmlNamespaceResolver nsResolver);

		// Token: 0x06002ABC RID: 10940
		public abstract object ChangeType(bool value, Type destinationType);

		// Token: 0x06002ABD RID: 10941
		public abstract object ChangeType(int value, Type destinationType);

		// Token: 0x06002ABE RID: 10942
		public abstract object ChangeType(long value, Type destinationType);

		// Token: 0x06002ABF RID: 10943
		public abstract object ChangeType(decimal value, Type destinationType);

		// Token: 0x06002AC0 RID: 10944
		public abstract object ChangeType(float value, Type destinationType);

		// Token: 0x06002AC1 RID: 10945
		public abstract object ChangeType(double value, Type destinationType);

		// Token: 0x06002AC2 RID: 10946
		public abstract object ChangeType(DateTime value, Type destinationType);

		// Token: 0x06002AC3 RID: 10947
		public abstract object ChangeType(DateTimeOffset value, Type destinationType);

		// Token: 0x06002AC4 RID: 10948
		public abstract object ChangeType(string value, Type destinationType);

		// Token: 0x06002AC5 RID: 10949
		public abstract object ChangeType(string value, Type destinationType, IXmlNamespaceResolver nsResolver);

		// Token: 0x06002AC6 RID: 10950
		public abstract object ChangeType(object value, Type destinationType);

		// Token: 0x06002AC7 RID: 10951
		public abstract object ChangeType(object value, Type destinationType, IXmlNamespaceResolver nsResolver);

		// Token: 0x06002AC8 RID: 10952 RVA: 0x00002103 File Offset: 0x00000303
		protected XmlValueConverter()
		{
		}
	}
}
