﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="attributeGroup" /> element with the <see langword="ref" /> attribute from the XML Schema as specified by the World Wide Web Consortium (W3C). AttributesGroupRef is the reference for an attributeGroup, name property contains the attribute group being referenced. </summary>
	// Token: 0x0200040B RID: 1035
	public class XmlSchemaAttributeGroupRef : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the name of the referenced <see langword="attributeGroup" /> element.</summary>
		/// <returns>The name of the referenced attribute group. The value must be a QName.</returns>
		// Token: 0x17000845 RID: 2117
		// (get) Token: 0x06002769 RID: 10089 RVA: 0x000DE7E1 File Offset: 0x000DC9E1
		// (set) Token: 0x0600276A RID: 10090 RVA: 0x000DE7E9 File Offset: 0x000DC9E9
		[XmlAttribute("ref")]
		public XmlQualifiedName RefName
		{
			get
			{
				return this.refName;
			}
			set
			{
				this.refName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAttributeGroupRef" /> class.</summary>
		// Token: 0x0600276B RID: 10091 RVA: 0x000DE802 File Offset: 0x000DCA02
		public XmlSchemaAttributeGroupRef()
		{
		}

		// Token: 0x04001C01 RID: 7169
		private XmlQualifiedName refName = XmlQualifiedName.Empty;
	}
}
