﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A0 RID: 928
	internal class Datatype_normalizedString : Datatype_string
	{
		// Token: 0x1700071F RID: 1823
		// (get) Token: 0x060022E5 RID: 8933 RVA: 0x000BE228 File Offset: 0x000BC428
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.NormalizedString;
			}
		}

		// Token: 0x17000720 RID: 1824
		// (get) Token: 0x060022E6 RID: 8934 RVA: 0x000033DE File Offset: 0x000015DE
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Replace;
			}
		}

		// Token: 0x17000721 RID: 1825
		// (get) Token: 0x060022E7 RID: 8935 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool HasValueFacets
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060022E8 RID: 8936 RVA: 0x000BE22C File Offset: 0x000BC42C
		public Datatype_normalizedString()
		{
		}
	}
}
