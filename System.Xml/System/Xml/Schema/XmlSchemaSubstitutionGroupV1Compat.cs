﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	// Token: 0x02000454 RID: 1108
	internal class XmlSchemaSubstitutionGroupV1Compat : XmlSchemaSubstitutionGroup
	{
		// Token: 0x17000922 RID: 2338
		// (get) Token: 0x060029B8 RID: 10680 RVA: 0x000E381F File Offset: 0x000E1A1F
		[XmlIgnore]
		internal XmlSchemaChoice Choice
		{
			get
			{
				return this.choice;
			}
		}

		// Token: 0x060029B9 RID: 10681 RVA: 0x000E3827 File Offset: 0x000E1A27
		public XmlSchemaSubstitutionGroupV1Compat()
		{
		}

		// Token: 0x04001CE9 RID: 7401
		private XmlSchemaChoice choice = new XmlSchemaChoice();
	}
}
