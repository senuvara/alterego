﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200038C RID: 908
	internal class Datatype_duration : Datatype_anySimpleType
	{
		// Token: 0x06002288 RID: 8840 RVA: 0x000BDBF8 File Offset: 0x000BBDF8
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlMiscConverter.Create(schemaType);
		}

		// Token: 0x170006F0 RID: 1776
		// (get) Token: 0x06002289 RID: 8841 RVA: 0x000BDC00 File Offset: 0x000BBE00
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.durationFacetsChecker;
			}
		}

		// Token: 0x170006F1 RID: 1777
		// (get) Token: 0x0600228A RID: 8842 RVA: 0x00072E73 File Offset: 0x00071073
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Duration;
			}
		}

		// Token: 0x170006F2 RID: 1778
		// (get) Token: 0x0600228B RID: 8843 RVA: 0x000BDC07 File Offset: 0x000BBE07
		public override Type ValueType
		{
			get
			{
				return Datatype_duration.atomicValueType;
			}
		}

		// Token: 0x170006F3 RID: 1779
		// (get) Token: 0x0600228C RID: 8844 RVA: 0x000BDC0E File Offset: 0x000BBE0E
		internal override Type ListValueType
		{
			get
			{
				return Datatype_duration.listValueType;
			}
		}

		// Token: 0x170006F4 RID: 1780
		// (get) Token: 0x0600228D RID: 8845 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x170006F5 RID: 1781
		// (get) Token: 0x0600228E RID: 8846 RVA: 0x000BDA0B File Offset: 0x000BBC0B
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace | RestrictionFlags.MaxInclusive | RestrictionFlags.MaxExclusive | RestrictionFlags.MinInclusive | RestrictionFlags.MinExclusive;
			}
		}

		// Token: 0x0600228F RID: 8847 RVA: 0x000BDC18 File Offset: 0x000BBE18
		internal override int Compare(object value1, object value2)
		{
			return ((TimeSpan)value1).CompareTo(value2);
		}

		// Token: 0x06002290 RID: 8848 RVA: 0x000BDC34 File Offset: 0x000BBE34
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			if (s == null || s.Length == 0)
			{
				return new XmlSchemaException("The attribute value cannot be empty.", string.Empty);
			}
			Exception ex = DatatypeImplementation.durationFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				TimeSpan timeSpan;
				ex = XmlConvert.TryToTimeSpan(s, out timeSpan);
				if (ex == null)
				{
					ex = DatatypeImplementation.durationFacetsChecker.CheckValueFacets(timeSpan, this);
					if (ex == null)
					{
						typedValue = timeSpan;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002291 RID: 8849 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_duration()
		{
		}

		// Token: 0x06002292 RID: 8850 RVA: 0x000BDC99 File Offset: 0x000BBE99
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_duration()
		{
		}

		// Token: 0x040018AF RID: 6319
		private static readonly Type atomicValueType = typeof(TimeSpan);

		// Token: 0x040018B0 RID: 6320
		private static readonly Type listValueType = typeof(TimeSpan[]);
	}
}
