﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Specifies a restriction on the number of digits that can be entered for the fraction value of a simpleType element. The value of fractionDigits must be a positive integer. Represents the World Wide Web Consortium (W3C) <see langword="fractionDigits" /> facet.</summary>
	// Token: 0x0200042C RID: 1068
	public class XmlSchemaFractionDigitsFacet : XmlSchemaNumericFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaFractionDigitsFacet" /> class.</summary>
		// Token: 0x06002880 RID: 10368 RVA: 0x000E0678 File Offset: 0x000DE878
		public XmlSchemaFractionDigitsFacet()
		{
			base.FacetType = FacetType.FractionDigits;
		}
	}
}
