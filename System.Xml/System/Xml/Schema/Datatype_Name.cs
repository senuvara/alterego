﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A6 RID: 934
	internal class Datatype_Name : Datatype_token
	{
		// Token: 0x1700072A RID: 1834
		// (get) Token: 0x060022F6 RID: 8950 RVA: 0x000BE254 File Offset: 0x000BC454
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Name;
			}
		}

		// Token: 0x060022F7 RID: 8951 RVA: 0x000BE24C File Offset: 0x000BC44C
		public Datatype_Name()
		{
		}
	}
}
