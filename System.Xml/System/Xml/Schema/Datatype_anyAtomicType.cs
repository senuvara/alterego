﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000385 RID: 901
	internal class Datatype_anyAtomicType : Datatype_anySimpleType
	{
		// Token: 0x0600224C RID: 8780 RVA: 0x000BD8F9 File Offset: 0x000BBAF9
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlAnyConverter.AnyAtomic;
		}

		// Token: 0x170006CF RID: 1743
		// (get) Token: 0x0600224D RID: 8781 RVA: 0x000020CD File Offset: 0x000002CD
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Preserve;
			}
		}

		// Token: 0x170006D0 RID: 1744
		// (get) Token: 0x0600224E RID: 8782 RVA: 0x00074D15 File Offset: 0x00072F15
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.AnyAtomicType;
			}
		}

		// Token: 0x0600224F RID: 8783 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_anyAtomicType()
		{
		}
	}
}
