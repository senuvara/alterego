﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x02000375 RID: 885
	internal sealed class ParticleContentValidator : ContentValidator
	{
		// Token: 0x060021B5 RID: 8629 RVA: 0x000BA458 File Offset: 0x000B8658
		public ParticleContentValidator(XmlSchemaContentType contentType) : this(contentType, true)
		{
		}

		// Token: 0x060021B6 RID: 8630 RVA: 0x000BA462 File Offset: 0x000B8662
		public ParticleContentValidator(XmlSchemaContentType contentType, bool enableUpaCheck) : base(contentType)
		{
			this.enableUpaCheck = enableUpaCheck;
		}

		// Token: 0x060021B7 RID: 8631 RVA: 0x00007AE0 File Offset: 0x00005CE0
		public override void InitValidation(ValidationState context)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x060021B8 RID: 8632 RVA: 0x00007AE0 File Offset: 0x00005CE0
		public override object ValidateElement(XmlQualifiedName name, ValidationState context, out int errorCode)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x060021B9 RID: 8633 RVA: 0x00007AE0 File Offset: 0x00005CE0
		public override bool CompleteValidation(ValidationState context)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x060021BA RID: 8634 RVA: 0x000BA472 File Offset: 0x000B8672
		public void Start()
		{
			this.symbols = new SymbolsDictionary();
			this.positions = new Positions();
			this.stack = new Stack();
		}

		// Token: 0x060021BB RID: 8635 RVA: 0x000BA495 File Offset: 0x000B8695
		public void OpenGroup()
		{
			this.stack.Push(null);
		}

		// Token: 0x060021BC RID: 8636 RVA: 0x000BA4A4 File Offset: 0x000B86A4
		public void CloseGroup()
		{
			SyntaxTreeNode syntaxTreeNode = (SyntaxTreeNode)this.stack.Pop();
			if (syntaxTreeNode == null)
			{
				return;
			}
			if (this.stack.Count == 0)
			{
				this.contentNode = syntaxTreeNode;
				this.isPartial = false;
				return;
			}
			InteriorNode interiorNode = (InteriorNode)this.stack.Pop();
			if (interiorNode != null)
			{
				interiorNode.RightChild = syntaxTreeNode;
				syntaxTreeNode = interiorNode;
				this.isPartial = true;
			}
			else
			{
				this.isPartial = false;
			}
			this.stack.Push(syntaxTreeNode);
		}

		// Token: 0x060021BD RID: 8637 RVA: 0x000BA51B File Offset: 0x000B871B
		public bool Exists(XmlQualifiedName name)
		{
			return this.symbols.Exists(name);
		}

		// Token: 0x060021BE RID: 8638 RVA: 0x000BA52E File Offset: 0x000B872E
		public void AddName(XmlQualifiedName name, object particle)
		{
			this.AddLeafNode(new LeafNode(this.positions.Add(this.symbols.AddName(name, particle), particle)));
		}

		// Token: 0x060021BF RID: 8639 RVA: 0x000BA554 File Offset: 0x000B8754
		public void AddNamespaceList(NamespaceList namespaceList, object particle)
		{
			this.symbols.AddNamespaceList(namespaceList, particle, false);
			this.AddLeafNode(new NamespaceListNode(namespaceList, particle));
		}

		// Token: 0x060021C0 RID: 8640 RVA: 0x000BA574 File Offset: 0x000B8774
		private void AddLeafNode(SyntaxTreeNode node)
		{
			if (this.stack.Count > 0)
			{
				InteriorNode interiorNode = (InteriorNode)this.stack.Pop();
				if (interiorNode != null)
				{
					interiorNode.RightChild = node;
					node = interiorNode;
				}
			}
			this.stack.Push(node);
			this.isPartial = true;
		}

		// Token: 0x060021C1 RID: 8641 RVA: 0x000BA5C0 File Offset: 0x000B87C0
		public void AddChoice()
		{
			SyntaxTreeNode leftChild = (SyntaxTreeNode)this.stack.Pop();
			InteriorNode interiorNode = new ChoiceNode();
			interiorNode.LeftChild = leftChild;
			this.stack.Push(interiorNode);
		}

		// Token: 0x060021C2 RID: 8642 RVA: 0x000BA5F8 File Offset: 0x000B87F8
		public void AddSequence()
		{
			SyntaxTreeNode leftChild = (SyntaxTreeNode)this.stack.Pop();
			InteriorNode interiorNode = new SequenceNode();
			interiorNode.LeftChild = leftChild;
			this.stack.Push(interiorNode);
		}

		// Token: 0x060021C3 RID: 8643 RVA: 0x000BA62F File Offset: 0x000B882F
		public void AddStar()
		{
			this.Closure(new StarNode());
		}

		// Token: 0x060021C4 RID: 8644 RVA: 0x000BA63C File Offset: 0x000B883C
		public void AddPlus()
		{
			this.Closure(new PlusNode());
		}

		// Token: 0x060021C5 RID: 8645 RVA: 0x000BA649 File Offset: 0x000B8849
		public void AddQMark()
		{
			this.Closure(new QmarkNode());
		}

		// Token: 0x060021C6 RID: 8646 RVA: 0x000BA658 File Offset: 0x000B8858
		public void AddLeafRange(decimal min, decimal max)
		{
			LeafRangeNode leafRangeNode = new LeafRangeNode(min, max);
			int pos = this.positions.Add(-2, leafRangeNode);
			leafRangeNode.Pos = pos;
			this.Closure(new SequenceNode
			{
				RightChild = leafRangeNode
			});
			this.minMaxNodesCount++;
		}

		// Token: 0x060021C7 RID: 8647 RVA: 0x000BA6A8 File Offset: 0x000B88A8
		private void Closure(InteriorNode node)
		{
			if (this.stack.Count > 0)
			{
				SyntaxTreeNode syntaxTreeNode = (SyntaxTreeNode)this.stack.Pop();
				InteriorNode interiorNode = syntaxTreeNode as InteriorNode;
				if (this.isPartial && interiorNode != null)
				{
					node.LeftChild = interiorNode.RightChild;
					interiorNode.RightChild = node;
				}
				else
				{
					node.LeftChild = syntaxTreeNode;
					syntaxTreeNode = node;
				}
				this.stack.Push(syntaxTreeNode);
				return;
			}
			if (this.contentNode != null)
			{
				node.LeftChild = this.contentNode;
				this.contentNode = node;
			}
		}

		// Token: 0x060021C8 RID: 8648 RVA: 0x000BA72C File Offset: 0x000B892C
		public ContentValidator Finish()
		{
			return this.Finish(true);
		}

		// Token: 0x060021C9 RID: 8649 RVA: 0x000BA738 File Offset: 0x000B8938
		public ContentValidator Finish(bool useDFA)
		{
			if (this.contentNode == null)
			{
				if (base.ContentType != XmlSchemaContentType.Mixed)
				{
					return ContentValidator.Empty;
				}
				bool isOpen = base.IsOpen;
				if (!base.IsOpen)
				{
					return ContentValidator.TextOnly;
				}
				return ContentValidator.Any;
			}
			else
			{
				InteriorNode interiorNode = new SequenceNode();
				interiorNode.LeftChild = this.contentNode;
				LeafNode leafNode = new LeafNode(this.positions.Add(this.symbols.AddName(XmlQualifiedName.Empty, null), null));
				interiorNode.RightChild = leafNode;
				this.contentNode.ExpandTree(interiorNode, this.symbols, this.positions);
				int count = this.symbols.Count;
				int count2 = this.positions.Count;
				BitSet bitSet = new BitSet(count2);
				BitSet lastpos = new BitSet(count2);
				BitSet[] array = new BitSet[count2];
				for (int i = 0; i < count2; i++)
				{
					array[i] = new BitSet(count2);
				}
				interiorNode.ConstructPos(bitSet, lastpos, array);
				if (this.minMaxNodesCount > 0)
				{
					BitSet bitSet2;
					BitSet[] minmaxFollowPos = this.CalculateTotalFollowposForRangeNodes(bitSet, array, out bitSet2);
					if (this.enableUpaCheck)
					{
						this.CheckCMUPAWithLeafRangeNodes(this.GetApplicableMinMaxFollowPos(bitSet, bitSet2, minmaxFollowPos));
						for (int j = 0; j < count2; j++)
						{
							this.CheckCMUPAWithLeafRangeNodes(this.GetApplicableMinMaxFollowPos(array[j], bitSet2, minmaxFollowPos));
						}
					}
					return new RangeContentValidator(bitSet, array, this.symbols, this.positions, leafNode.Pos, base.ContentType, interiorNode.LeftChild.IsNullable, bitSet2, this.minMaxNodesCount);
				}
				int[][] array2 = null;
				if (!this.symbols.IsUpaEnforced)
				{
					if (this.enableUpaCheck)
					{
						this.CheckUniqueParticleAttribution(bitSet, array);
					}
				}
				else if (useDFA)
				{
					array2 = this.BuildTransitionTable(bitSet, array, leafNode.Pos);
				}
				if (array2 != null)
				{
					return new DfaContentValidator(array2, this.symbols, base.ContentType, base.IsOpen, interiorNode.LeftChild.IsNullable);
				}
				return new NfaContentValidator(bitSet, array, this.symbols, this.positions, leafNode.Pos, base.ContentType, base.IsOpen, interiorNode.LeftChild.IsNullable);
			}
		}

		// Token: 0x060021CA RID: 8650 RVA: 0x000BA93C File Offset: 0x000B8B3C
		private BitSet[] CalculateTotalFollowposForRangeNodes(BitSet firstpos, BitSet[] followpos, out BitSet posWithRangeTerminals)
		{
			int count = this.positions.Count;
			posWithRangeTerminals = new BitSet(count);
			BitSet[] array = new BitSet[this.minMaxNodesCount];
			int num = 0;
			for (int i = count - 1; i >= 0; i--)
			{
				Position position = this.positions[i];
				if (position.symbol == -2)
				{
					LeafRangeNode leafRangeNode = position.particle as LeafRangeNode;
					BitSet bitSet = new BitSet(count);
					bitSet.Clear();
					bitSet.Or(followpos[i]);
					if (leafRangeNode.Min != leafRangeNode.Max)
					{
						bitSet.Or(leafRangeNode.NextIteration);
					}
					for (int num2 = bitSet.NextSet(-1); num2 != -1; num2 = bitSet.NextSet(num2))
					{
						if (num2 > i)
						{
							Position position2 = this.positions[num2];
							if (position2.symbol == -2)
							{
								LeafRangeNode leafRangeNode2 = position2.particle as LeafRangeNode;
								bitSet.Or(array[leafRangeNode2.Pos]);
							}
						}
					}
					array[num] = bitSet;
					leafRangeNode.Pos = num++;
					posWithRangeTerminals.Set(i);
				}
			}
			return array;
		}

		// Token: 0x060021CB RID: 8651 RVA: 0x000BAA58 File Offset: 0x000B8C58
		private void CheckCMUPAWithLeafRangeNodes(BitSet curpos)
		{
			object[] array = new object[this.symbols.Count];
			for (int num = curpos.NextSet(-1); num != -1; num = curpos.NextSet(num))
			{
				Position position = this.positions[num];
				int symbol = position.symbol;
				if (symbol >= 0)
				{
					if (array[symbol] != null)
					{
						throw new UpaException(array[symbol], position.particle);
					}
					array[symbol] = position.particle;
				}
			}
		}

		// Token: 0x060021CC RID: 8652 RVA: 0x000BAAC4 File Offset: 0x000B8CC4
		private BitSet GetApplicableMinMaxFollowPos(BitSet curpos, BitSet posWithRangeTerminals, BitSet[] minmaxFollowPos)
		{
			if (curpos.Intersects(posWithRangeTerminals))
			{
				BitSet bitSet = new BitSet(this.positions.Count);
				bitSet.Or(curpos);
				bitSet.And(posWithRangeTerminals);
				curpos = curpos.Clone();
				for (int num = bitSet.NextSet(-1); num != -1; num = bitSet.NextSet(num))
				{
					LeafRangeNode leafRangeNode = this.positions[num].particle as LeafRangeNode;
					curpos.Or(minmaxFollowPos[leafRangeNode.Pos]);
				}
			}
			return curpos;
		}

		// Token: 0x060021CD RID: 8653 RVA: 0x000BAB40 File Offset: 0x000B8D40
		private void CheckUniqueParticleAttribution(BitSet firstpos, BitSet[] followpos)
		{
			this.CheckUniqueParticleAttribution(firstpos);
			for (int i = 0; i < this.positions.Count; i++)
			{
				this.CheckUniqueParticleAttribution(followpos[i]);
			}
		}

		// Token: 0x060021CE RID: 8654 RVA: 0x000BAB74 File Offset: 0x000B8D74
		private void CheckUniqueParticleAttribution(BitSet curpos)
		{
			object[] array = new object[this.symbols.Count];
			for (int num = curpos.NextSet(-1); num != -1; num = curpos.NextSet(num))
			{
				int symbol = this.positions[num].symbol;
				if (array[symbol] == null)
				{
					array[symbol] = this.positions[num].particle;
				}
				else if (array[symbol] != this.positions[num].particle)
				{
					throw new UpaException(array[symbol], this.positions[num].particle);
				}
			}
		}

		// Token: 0x060021CF RID: 8655 RVA: 0x000BAC08 File Offset: 0x000B8E08
		private int[][] BuildTransitionTable(BitSet firstpos, BitSet[] followpos, int endMarkerPos)
		{
			int count = this.positions.Count;
			int num = 8192 / count;
			int count2 = this.symbols.Count;
			ArrayList arrayList = new ArrayList();
			Hashtable hashtable = new Hashtable();
			hashtable.Add(new BitSet(count), -1);
			Queue queue = new Queue();
			int num2 = 0;
			queue.Enqueue(firstpos);
			hashtable.Add(firstpos, 0);
			arrayList.Add(new int[count2 + 1]);
			while (queue.Count > 0)
			{
				BitSet bitSet = (BitSet)queue.Dequeue();
				int[] array = (int[])arrayList[num2];
				if (bitSet[endMarkerPos])
				{
					array[count2] = 1;
				}
				for (int i = 0; i < count2; i++)
				{
					BitSet bitSet2 = new BitSet(count);
					for (int num3 = bitSet.NextSet(-1); num3 != -1; num3 = bitSet.NextSet(num3))
					{
						if (i == this.positions[num3].symbol)
						{
							bitSet2.Or(followpos[num3]);
						}
					}
					object obj = hashtable[bitSet2];
					if (obj != null)
					{
						array[i] = (int)obj;
					}
					else
					{
						int num4 = hashtable.Count - 1;
						if (num4 >= num)
						{
							return null;
						}
						queue.Enqueue(bitSet2);
						hashtable.Add(bitSet2, num4);
						arrayList.Add(new int[count2 + 1]);
						array[i] = num4;
					}
				}
				num2++;
			}
			return (int[][])arrayList.ToArray(typeof(int[]));
		}

		// Token: 0x04001800 RID: 6144
		private SymbolsDictionary symbols;

		// Token: 0x04001801 RID: 6145
		private Positions positions;

		// Token: 0x04001802 RID: 6146
		private Stack stack;

		// Token: 0x04001803 RID: 6147
		private SyntaxTreeNode contentNode;

		// Token: 0x04001804 RID: 6148
		private bool isPartial;

		// Token: 0x04001805 RID: 6149
		private int minMaxNodesCount;

		// Token: 0x04001806 RID: 6150
		private bool enableUpaCheck;
	}
}
