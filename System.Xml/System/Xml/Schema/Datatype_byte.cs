﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B2 RID: 946
	internal class Datatype_byte : Datatype_short
	{
		// Token: 0x1700074C RID: 1868
		// (get) Token: 0x06002335 RID: 9013 RVA: 0x000BE710 File Offset: 0x000BC910
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_byte.numeric10FacetsChecker;
			}
		}

		// Token: 0x1700074D RID: 1869
		// (get) Token: 0x06002336 RID: 9014 RVA: 0x000BE717 File Offset: 0x000BC917
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Byte;
			}
		}

		// Token: 0x06002337 RID: 9015 RVA: 0x000BE71C File Offset: 0x000BC91C
		internal override int Compare(object value1, object value2)
		{
			return ((sbyte)value1).CompareTo(value2);
		}

		// Token: 0x1700074E RID: 1870
		// (get) Token: 0x06002338 RID: 9016 RVA: 0x000BE738 File Offset: 0x000BC938
		public override Type ValueType
		{
			get
			{
				return Datatype_byte.atomicValueType;
			}
		}

		// Token: 0x1700074F RID: 1871
		// (get) Token: 0x06002339 RID: 9017 RVA: 0x000BE73F File Offset: 0x000BC93F
		internal override Type ListValueType
		{
			get
			{
				return Datatype_byte.listValueType;
			}
		}

		// Token: 0x0600233A RID: 9018 RVA: 0x000BE748 File Offset: 0x000BC948
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_byte.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				sbyte b;
				ex = XmlConvert.TryToSByte(s, out b);
				if (ex == null)
				{
					ex = Datatype_byte.numeric10FacetsChecker.CheckValueFacets((short)b, this);
					if (ex == null)
					{
						typedValue = b;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x0600233B RID: 9019 RVA: 0x000BE792 File Offset: 0x000BC992
		public Datatype_byte()
		{
		}

		// Token: 0x0600233C RID: 9020 RVA: 0x000BE79A File Offset: 0x000BC99A
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_byte()
		{
		}

		// Token: 0x040018C9 RID: 6345
		private static readonly Type atomicValueType = typeof(sbyte);

		// Token: 0x040018CA RID: 6346
		private static readonly Type listValueType = typeof(sbyte[]);

		// Token: 0x040018CB RID: 6347
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(-128m, 127m);
	}
}
