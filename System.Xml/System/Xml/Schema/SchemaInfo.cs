﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System.Xml.Schema
{
	// Token: 0x020003E7 RID: 999
	internal class SchemaInfo : IDtdInfo
	{
		// Token: 0x06002584 RID: 9604 RVA: 0x000D28FC File Offset: 0x000D0AFC
		internal SchemaInfo()
		{
			this.schemaType = SchemaType.None;
		}

		// Token: 0x170007D0 RID: 2000
		// (get) Token: 0x06002585 RID: 9605 RVA: 0x000D2963 File Offset: 0x000D0B63
		// (set) Token: 0x06002586 RID: 9606 RVA: 0x000D296B File Offset: 0x000D0B6B
		public XmlQualifiedName DocTypeName
		{
			get
			{
				return this.docTypeName;
			}
			set
			{
				this.docTypeName = value;
			}
		}

		// Token: 0x170007D1 RID: 2001
		// (get) Token: 0x06002587 RID: 9607 RVA: 0x000D2974 File Offset: 0x000D0B74
		// (set) Token: 0x06002588 RID: 9608 RVA: 0x000D297C File Offset: 0x000D0B7C
		internal string InternalDtdSubset
		{
			get
			{
				return this.internalDtdSubset;
			}
			set
			{
				this.internalDtdSubset = value;
			}
		}

		// Token: 0x170007D2 RID: 2002
		// (get) Token: 0x06002589 RID: 9609 RVA: 0x000D2985 File Offset: 0x000D0B85
		internal Dictionary<XmlQualifiedName, SchemaElementDecl> ElementDecls
		{
			get
			{
				return this.elementDecls;
			}
		}

		// Token: 0x170007D3 RID: 2003
		// (get) Token: 0x0600258A RID: 9610 RVA: 0x000D298D File Offset: 0x000D0B8D
		internal Dictionary<XmlQualifiedName, SchemaElementDecl> UndeclaredElementDecls
		{
			get
			{
				return this.undeclaredElementDecls;
			}
		}

		// Token: 0x170007D4 RID: 2004
		// (get) Token: 0x0600258B RID: 9611 RVA: 0x000D2995 File Offset: 0x000D0B95
		internal Dictionary<XmlQualifiedName, SchemaEntity> GeneralEntities
		{
			get
			{
				if (this.generalEntities == null)
				{
					this.generalEntities = new Dictionary<XmlQualifiedName, SchemaEntity>();
				}
				return this.generalEntities;
			}
		}

		// Token: 0x170007D5 RID: 2005
		// (get) Token: 0x0600258C RID: 9612 RVA: 0x000D29B0 File Offset: 0x000D0BB0
		internal Dictionary<XmlQualifiedName, SchemaEntity> ParameterEntities
		{
			get
			{
				if (this.parameterEntities == null)
				{
					this.parameterEntities = new Dictionary<XmlQualifiedName, SchemaEntity>();
				}
				return this.parameterEntities;
			}
		}

		// Token: 0x170007D6 RID: 2006
		// (get) Token: 0x0600258D RID: 9613 RVA: 0x000D29CB File Offset: 0x000D0BCB
		// (set) Token: 0x0600258E RID: 9614 RVA: 0x000D29D3 File Offset: 0x000D0BD3
		internal SchemaType SchemaType
		{
			get
			{
				return this.schemaType;
			}
			set
			{
				this.schemaType = value;
			}
		}

		// Token: 0x170007D7 RID: 2007
		// (get) Token: 0x0600258F RID: 9615 RVA: 0x000D29DC File Offset: 0x000D0BDC
		internal Dictionary<string, bool> TargetNamespaces
		{
			get
			{
				return this.targetNamespaces;
			}
		}

		// Token: 0x170007D8 RID: 2008
		// (get) Token: 0x06002590 RID: 9616 RVA: 0x000D29E4 File Offset: 0x000D0BE4
		internal Dictionary<XmlQualifiedName, SchemaElementDecl> ElementDeclsByType
		{
			get
			{
				return this.elementDeclsByType;
			}
		}

		// Token: 0x170007D9 RID: 2009
		// (get) Token: 0x06002591 RID: 9617 RVA: 0x000D29EC File Offset: 0x000D0BEC
		internal Dictionary<XmlQualifiedName, SchemaAttDef> AttributeDecls
		{
			get
			{
				return this.attributeDecls;
			}
		}

		// Token: 0x170007DA RID: 2010
		// (get) Token: 0x06002592 RID: 9618 RVA: 0x000D29F4 File Offset: 0x000D0BF4
		internal Dictionary<string, SchemaNotation> Notations
		{
			get
			{
				if (this.notations == null)
				{
					this.notations = new Dictionary<string, SchemaNotation>();
				}
				return this.notations;
			}
		}

		// Token: 0x170007DB RID: 2011
		// (get) Token: 0x06002593 RID: 9619 RVA: 0x000D2A0F File Offset: 0x000D0C0F
		// (set) Token: 0x06002594 RID: 9620 RVA: 0x000D2A17 File Offset: 0x000D0C17
		internal int ErrorCount
		{
			get
			{
				return this.errorCount;
			}
			set
			{
				this.errorCount = value;
			}
		}

		// Token: 0x06002595 RID: 9621 RVA: 0x000D2A20 File Offset: 0x000D0C20
		internal SchemaElementDecl GetElementDecl(XmlQualifiedName qname)
		{
			SchemaElementDecl result;
			if (this.elementDecls.TryGetValue(qname, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x06002596 RID: 9622 RVA: 0x000D2A40 File Offset: 0x000D0C40
		internal SchemaElementDecl GetTypeDecl(XmlQualifiedName qname)
		{
			SchemaElementDecl result;
			if (this.elementDeclsByType.TryGetValue(qname, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x06002597 RID: 9623 RVA: 0x000D2A60 File Offset: 0x000D0C60
		internal XmlSchemaElement GetElement(XmlQualifiedName qname)
		{
			SchemaElementDecl elementDecl = this.GetElementDecl(qname);
			if (elementDecl != null)
			{
				return elementDecl.SchemaElement;
			}
			return null;
		}

		// Token: 0x06002598 RID: 9624 RVA: 0x000D2A80 File Offset: 0x000D0C80
		internal XmlSchemaAttribute GetAttribute(XmlQualifiedName qname)
		{
			SchemaAttDef schemaAttDef = this.attributeDecls[qname];
			if (schemaAttDef != null)
			{
				return schemaAttDef.SchemaAttribute;
			}
			return null;
		}

		// Token: 0x06002599 RID: 9625 RVA: 0x000D2AA8 File Offset: 0x000D0CA8
		internal XmlSchemaElement GetType(XmlQualifiedName qname)
		{
			SchemaElementDecl elementDecl = this.GetElementDecl(qname);
			if (elementDecl != null)
			{
				return elementDecl.SchemaElement;
			}
			return null;
		}

		// Token: 0x0600259A RID: 9626 RVA: 0x000D2AC8 File Offset: 0x000D0CC8
		internal bool HasSchema(string ns)
		{
			return this.targetNamespaces.ContainsKey(ns);
		}

		// Token: 0x0600259B RID: 9627 RVA: 0x000D2AC8 File Offset: 0x000D0CC8
		internal bool Contains(string ns)
		{
			return this.targetNamespaces.ContainsKey(ns);
		}

		// Token: 0x0600259C RID: 9628 RVA: 0x000D2AD8 File Offset: 0x000D0CD8
		internal SchemaAttDef GetAttributeXdr(SchemaElementDecl ed, XmlQualifiedName qname)
		{
			SchemaAttDef schemaAttDef = null;
			if (ed != null)
			{
				schemaAttDef = ed.GetAttDef(qname);
				if (schemaAttDef == null)
				{
					if (!ed.ContentValidator.IsOpen || qname.Namespace.Length == 0)
					{
						throw new XmlSchemaException("The '{0}' attribute is not declared.", qname.ToString());
					}
					if (!this.attributeDecls.TryGetValue(qname, out schemaAttDef) && this.targetNamespaces.ContainsKey(qname.Namespace))
					{
						throw new XmlSchemaException("The '{0}' attribute is not declared.", qname.ToString());
					}
				}
			}
			return schemaAttDef;
		}

		// Token: 0x0600259D RID: 9629 RVA: 0x000D2B58 File Offset: 0x000D0D58
		internal SchemaAttDef GetAttributeXsd(SchemaElementDecl ed, XmlQualifiedName qname, XmlSchemaObject partialValidationType, out AttributeMatchState attributeMatchState)
		{
			SchemaAttDef schemaAttDef = null;
			attributeMatchState = AttributeMatchState.UndeclaredAttribute;
			if (ed != null)
			{
				schemaAttDef = ed.GetAttDef(qname);
				if (schemaAttDef != null)
				{
					attributeMatchState = AttributeMatchState.AttributeFound;
					return schemaAttDef;
				}
				XmlSchemaAnyAttribute anyAttribute = ed.AnyAttribute;
				if (anyAttribute != null)
				{
					if (!anyAttribute.NamespaceList.Allows(qname))
					{
						attributeMatchState = AttributeMatchState.ProhibitedAnyAttribute;
					}
					else if (anyAttribute.ProcessContentsCorrect != XmlSchemaContentProcessing.Skip)
					{
						if (this.attributeDecls.TryGetValue(qname, out schemaAttDef))
						{
							if (schemaAttDef.Datatype.TypeCode == XmlTypeCode.Id)
							{
								attributeMatchState = AttributeMatchState.AnyIdAttributeFound;
							}
							else
							{
								attributeMatchState = AttributeMatchState.AttributeFound;
							}
						}
						else if (anyAttribute.ProcessContentsCorrect == XmlSchemaContentProcessing.Lax)
						{
							attributeMatchState = AttributeMatchState.AnyAttributeLax;
						}
					}
					else
					{
						attributeMatchState = AttributeMatchState.AnyAttributeSkip;
					}
				}
				else if (ed.ProhibitedAttributes.ContainsKey(qname))
				{
					attributeMatchState = AttributeMatchState.ProhibitedAttribute;
				}
			}
			else if (partialValidationType != null)
			{
				XmlSchemaAttribute xmlSchemaAttribute = partialValidationType as XmlSchemaAttribute;
				if (xmlSchemaAttribute != null)
				{
					if (qname.Equals(xmlSchemaAttribute.QualifiedName))
					{
						schemaAttDef = xmlSchemaAttribute.AttDef;
						attributeMatchState = AttributeMatchState.AttributeFound;
					}
					else
					{
						attributeMatchState = AttributeMatchState.AttributeNameMismatch;
					}
				}
				else
				{
					attributeMatchState = AttributeMatchState.ValidateAttributeInvalidCall;
				}
			}
			else if (this.attributeDecls.TryGetValue(qname, out schemaAttDef))
			{
				attributeMatchState = AttributeMatchState.AttributeFound;
			}
			else
			{
				attributeMatchState = AttributeMatchState.UndeclaredElementAndAttribute;
			}
			return schemaAttDef;
		}

		// Token: 0x0600259E RID: 9630 RVA: 0x000D2C50 File Offset: 0x000D0E50
		internal SchemaAttDef GetAttributeXsd(SchemaElementDecl ed, XmlQualifiedName qname, ref bool skip)
		{
			AttributeMatchState attributeMatchState;
			SchemaAttDef attributeXsd = this.GetAttributeXsd(ed, qname, null, out attributeMatchState);
			switch (attributeMatchState)
			{
			case AttributeMatchState.UndeclaredAttribute:
				throw new XmlSchemaException("The '{0}' attribute is not declared.", qname.ToString());
			case AttributeMatchState.AnyAttributeSkip:
				skip = true;
				break;
			case AttributeMatchState.ProhibitedAnyAttribute:
			case AttributeMatchState.ProhibitedAttribute:
				throw new XmlSchemaException("The '{0}' attribute is not allowed.", qname.ToString());
			}
			return attributeXsd;
		}

		// Token: 0x0600259F RID: 9631 RVA: 0x000D2CB8 File Offset: 0x000D0EB8
		internal void Add(SchemaInfo sinfo, ValidationEventHandler eventhandler)
		{
			if (this.schemaType == SchemaType.None)
			{
				this.schemaType = sinfo.SchemaType;
			}
			else if (this.schemaType != sinfo.SchemaType)
			{
				if (eventhandler != null)
				{
					eventhandler(this, new ValidationEventArgs(new XmlSchemaException("Different schema types cannot be mixed.", string.Empty)));
				}
				return;
			}
			foreach (string key in sinfo.TargetNamespaces.Keys)
			{
				if (!this.targetNamespaces.ContainsKey(key))
				{
					this.targetNamespaces.Add(key, true);
				}
			}
			foreach (KeyValuePair<XmlQualifiedName, SchemaElementDecl> keyValuePair in sinfo.elementDecls)
			{
				if (!this.elementDecls.ContainsKey(keyValuePair.Key))
				{
					this.elementDecls.Add(keyValuePair.Key, keyValuePair.Value);
				}
			}
			foreach (KeyValuePair<XmlQualifiedName, SchemaElementDecl> keyValuePair2 in sinfo.elementDeclsByType)
			{
				if (!this.elementDeclsByType.ContainsKey(keyValuePair2.Key))
				{
					this.elementDeclsByType.Add(keyValuePair2.Key, keyValuePair2.Value);
				}
			}
			foreach (SchemaAttDef schemaAttDef in sinfo.AttributeDecls.Values)
			{
				if (!this.attributeDecls.ContainsKey(schemaAttDef.Name))
				{
					this.attributeDecls.Add(schemaAttDef.Name, schemaAttDef);
				}
			}
			foreach (SchemaNotation schemaNotation in sinfo.Notations.Values)
			{
				if (!this.Notations.ContainsKey(schemaNotation.Name.Name))
				{
					this.Notations.Add(schemaNotation.Name.Name, schemaNotation);
				}
			}
		}

		// Token: 0x060025A0 RID: 9632 RVA: 0x000D2F18 File Offset: 0x000D1118
		internal void Finish()
		{
			Dictionary<XmlQualifiedName, SchemaElementDecl> dictionary = this.elementDecls;
			for (int i = 0; i < 2; i++)
			{
				foreach (SchemaElementDecl schemaElementDecl in dictionary.Values)
				{
					if (schemaElementDecl.HasNonCDataAttribute)
					{
						this.hasNonCDataAttributes = true;
					}
					if (schemaElementDecl.DefaultAttDefs != null)
					{
						this.hasDefaultAttributes = true;
					}
				}
				dictionary = this.undeclaredElementDecls;
			}
		}

		// Token: 0x170007DC RID: 2012
		// (get) Token: 0x060025A1 RID: 9633 RVA: 0x000D2F9C File Offset: 0x000D119C
		bool IDtdInfo.HasDefaultAttributes
		{
			get
			{
				return this.hasDefaultAttributes;
			}
		}

		// Token: 0x170007DD RID: 2013
		// (get) Token: 0x060025A2 RID: 9634 RVA: 0x000D2FA4 File Offset: 0x000D11A4
		bool IDtdInfo.HasNonCDataAttributes
		{
			get
			{
				return this.hasNonCDataAttributes;
			}
		}

		// Token: 0x060025A3 RID: 9635 RVA: 0x000D2FAC File Offset: 0x000D11AC
		IDtdAttributeListInfo IDtdInfo.LookupAttributeList(string prefix, string localName)
		{
			XmlQualifiedName key = new XmlQualifiedName(prefix, localName);
			SchemaElementDecl result;
			if (!this.elementDecls.TryGetValue(key, out result))
			{
				this.undeclaredElementDecls.TryGetValue(key, out result);
			}
			return result;
		}

		// Token: 0x060025A4 RID: 9636 RVA: 0x000D2FE1 File Offset: 0x000D11E1
		IEnumerable<IDtdAttributeListInfo> IDtdInfo.GetAttributeLists()
		{
			foreach (IDtdAttributeListInfo dtdAttributeListInfo in this.elementDecls.Values)
			{
				yield return dtdAttributeListInfo;
			}
			Dictionary<XmlQualifiedName, SchemaElementDecl>.ValueCollection.Enumerator enumerator = default(Dictionary<XmlQualifiedName, SchemaElementDecl>.ValueCollection.Enumerator);
			yield break;
			yield break;
		}

		// Token: 0x060025A5 RID: 9637 RVA: 0x000D2FF4 File Offset: 0x000D11F4
		IDtdEntityInfo IDtdInfo.LookupEntity(string name)
		{
			if (this.generalEntities == null)
			{
				return null;
			}
			XmlQualifiedName key = new XmlQualifiedName(name);
			SchemaEntity result;
			if (this.generalEntities.TryGetValue(key, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x170007DE RID: 2014
		// (get) Token: 0x060025A6 RID: 9638 RVA: 0x000D2963 File Offset: 0x000D0B63
		XmlQualifiedName IDtdInfo.Name
		{
			get
			{
				return this.docTypeName;
			}
		}

		// Token: 0x170007DF RID: 2015
		// (get) Token: 0x060025A7 RID: 9639 RVA: 0x000D2974 File Offset: 0x000D0B74
		string IDtdInfo.InternalDtdSubset
		{
			get
			{
				return this.internalDtdSubset;
			}
		}

		// Token: 0x040019EB RID: 6635
		private Dictionary<XmlQualifiedName, SchemaElementDecl> elementDecls = new Dictionary<XmlQualifiedName, SchemaElementDecl>();

		// Token: 0x040019EC RID: 6636
		private Dictionary<XmlQualifiedName, SchemaElementDecl> undeclaredElementDecls = new Dictionary<XmlQualifiedName, SchemaElementDecl>();

		// Token: 0x040019ED RID: 6637
		private Dictionary<XmlQualifiedName, SchemaEntity> generalEntities;

		// Token: 0x040019EE RID: 6638
		private Dictionary<XmlQualifiedName, SchemaEntity> parameterEntities;

		// Token: 0x040019EF RID: 6639
		private XmlQualifiedName docTypeName = XmlQualifiedName.Empty;

		// Token: 0x040019F0 RID: 6640
		private string internalDtdSubset = string.Empty;

		// Token: 0x040019F1 RID: 6641
		private bool hasNonCDataAttributes;

		// Token: 0x040019F2 RID: 6642
		private bool hasDefaultAttributes;

		// Token: 0x040019F3 RID: 6643
		private Dictionary<string, bool> targetNamespaces = new Dictionary<string, bool>();

		// Token: 0x040019F4 RID: 6644
		private Dictionary<XmlQualifiedName, SchemaAttDef> attributeDecls = new Dictionary<XmlQualifiedName, SchemaAttDef>();

		// Token: 0x040019F5 RID: 6645
		private int errorCount;

		// Token: 0x040019F6 RID: 6646
		private SchemaType schemaType;

		// Token: 0x040019F7 RID: 6647
		private Dictionary<XmlQualifiedName, SchemaElementDecl> elementDeclsByType = new Dictionary<XmlQualifiedName, SchemaElementDecl>();

		// Token: 0x040019F8 RID: 6648
		private Dictionary<string, SchemaNotation> notations;

		// Token: 0x020003E8 RID: 1000
		[CompilerGenerated]
		private sealed class <System-Xml-IDtdInfo-GetAttributeLists>d__60 : IEnumerable<IDtdAttributeListInfo>, IEnumerable, IEnumerator<IDtdAttributeListInfo>, IDisposable, IEnumerator
		{
			// Token: 0x060025A8 RID: 9640 RVA: 0x000D3025 File Offset: 0x000D1225
			[DebuggerHidden]
			public <System-Xml-IDtdInfo-GetAttributeLists>d__60(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060025A9 RID: 9641 RVA: 0x000D3040 File Offset: 0x000D1240
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x060025AA RID: 9642 RVA: 0x000D3078 File Offset: 0x000D1278
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					SchemaInfo schemaInfo = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = schemaInfo.elementDecls.Values.GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = default(Dictionary<XmlQualifiedName, SchemaElementDecl>.ValueCollection.Enumerator);
						result = false;
					}
					else
					{
						IDtdAttributeListInfo dtdAttributeListInfo = enumerator.Current;
						this.<>2__current = dtdAttributeListInfo;
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x060025AB RID: 9643 RVA: 0x000D3128 File Offset: 0x000D1328
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				((IDisposable)enumerator).Dispose();
			}

			// Token: 0x170007E0 RID: 2016
			// (get) Token: 0x060025AC RID: 9644 RVA: 0x000D3142 File Offset: 0x000D1342
			IDtdAttributeListInfo IEnumerator<IDtdAttributeListInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060025AD RID: 9645 RVA: 0x00010D4A File Offset: 0x0000EF4A
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170007E1 RID: 2017
			// (get) Token: 0x060025AE RID: 9646 RVA: 0x000D3142 File Offset: 0x000D1342
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060025AF RID: 9647 RVA: 0x000D314C File Offset: 0x000D134C
			[DebuggerHidden]
			IEnumerator<IDtdAttributeListInfo> IEnumerable<IDtdAttributeListInfo>.GetEnumerator()
			{
				SchemaInfo.<System-Xml-IDtdInfo-GetAttributeLists>d__60 <System-Xml-IDtdInfo-GetAttributeLists>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<System-Xml-IDtdInfo-GetAttributeLists>d__ = this;
				}
				else
				{
					<System-Xml-IDtdInfo-GetAttributeLists>d__ = new SchemaInfo.<System-Xml-IDtdInfo-GetAttributeLists>d__60(0);
					<System-Xml-IDtdInfo-GetAttributeLists>d__.<>4__this = this;
				}
				return <System-Xml-IDtdInfo-GetAttributeLists>d__;
			}

			// Token: 0x060025B0 RID: 9648 RVA: 0x000D318F File Offset: 0x000D138F
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Xml.IDtdAttributeListInfo>.GetEnumerator();
			}

			// Token: 0x040019F9 RID: 6649
			private int <>1__state;

			// Token: 0x040019FA RID: 6650
			private IDtdAttributeListInfo <>2__current;

			// Token: 0x040019FB RID: 6651
			private int <>l__initialThreadId;

			// Token: 0x040019FC RID: 6652
			public SchemaInfo <>4__this;

			// Token: 0x040019FD RID: 6653
			private Dictionary<XmlQualifiedName, SchemaElementDecl>.ValueCollection.Enumerator <>7__wrap1;
		}
	}
}
