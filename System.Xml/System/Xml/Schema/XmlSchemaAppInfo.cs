﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the World Wide Web Consortium (W3C) <see langword="appinfo" /> element.</summary>
	// Token: 0x02000408 RID: 1032
	public class XmlSchemaAppInfo : XmlSchemaObject
	{
		/// <summary>Gets or sets the source of the application information.</summary>
		/// <returns>A Uniform Resource Identifier (URI) reference. The default is <see langword="String.Empty" />.Optional.</returns>
		// Token: 0x1700082A RID: 2090
		// (get) Token: 0x06002733 RID: 10035 RVA: 0x000DE4DD File Offset: 0x000DC6DD
		// (set) Token: 0x06002734 RID: 10036 RVA: 0x000DE4E5 File Offset: 0x000DC6E5
		[XmlAttribute("source", DataType = "anyURI")]
		public string Source
		{
			get
			{
				return this.source;
			}
			set
			{
				this.source = value;
			}
		}

		/// <summary>Gets or sets an array of <see cref="T:System.Xml.XmlNode" /> objects that represents the <see langword="appinfo" /> child nodes.</summary>
		/// <returns>An array of <see cref="T:System.Xml.XmlNode" /> objects that represents the <see langword="appinfo" /> child nodes.</returns>
		// Token: 0x1700082B RID: 2091
		// (get) Token: 0x06002735 RID: 10037 RVA: 0x000DE4EE File Offset: 0x000DC6EE
		// (set) Token: 0x06002736 RID: 10038 RVA: 0x000DE4F6 File Offset: 0x000DC6F6
		[XmlText]
		[XmlAnyElement]
		public XmlNode[] Markup
		{
			get
			{
				return this.markup;
			}
			set
			{
				this.markup = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAppInfo" /> class.</summary>
		// Token: 0x06002737 RID: 10039 RVA: 0x000DE196 File Offset: 0x000DC396
		public XmlSchemaAppInfo()
		{
		}

		// Token: 0x04001BEC RID: 7148
		private string source;

		// Token: 0x04001BED RID: 7149
		private XmlNode[] markup;
	}
}
