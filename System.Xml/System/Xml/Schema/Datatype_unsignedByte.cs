﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B7 RID: 951
	internal class Datatype_unsignedByte : Datatype_unsignedShort
	{
		// Token: 0x1700075F RID: 1887
		// (get) Token: 0x0600235A RID: 9050 RVA: 0x000BEA3F File Offset: 0x000BCC3F
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_unsignedByte.numeric10FacetsChecker;
			}
		}

		// Token: 0x17000760 RID: 1888
		// (get) Token: 0x0600235B RID: 9051 RVA: 0x000BEA46 File Offset: 0x000BCC46
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.UnsignedByte;
			}
		}

		// Token: 0x0600235C RID: 9052 RVA: 0x000BEA4C File Offset: 0x000BCC4C
		internal override int Compare(object value1, object value2)
		{
			return ((byte)value1).CompareTo(value2);
		}

		// Token: 0x17000761 RID: 1889
		// (get) Token: 0x0600235D RID: 9053 RVA: 0x000BEA68 File Offset: 0x000BCC68
		public override Type ValueType
		{
			get
			{
				return Datatype_unsignedByte.atomicValueType;
			}
		}

		// Token: 0x17000762 RID: 1890
		// (get) Token: 0x0600235E RID: 9054 RVA: 0x000BEA6F File Offset: 0x000BCC6F
		internal override Type ListValueType
		{
			get
			{
				return Datatype_unsignedByte.listValueType;
			}
		}

		// Token: 0x0600235F RID: 9055 RVA: 0x000BEA78 File Offset: 0x000BCC78
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_unsignedByte.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				byte b;
				ex = XmlConvert.TryToByte(s, out b);
				if (ex == null)
				{
					ex = Datatype_unsignedByte.numeric10FacetsChecker.CheckValueFacets((short)b, this);
					if (ex == null)
					{
						typedValue = b;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002360 RID: 9056 RVA: 0x000BEAC2 File Offset: 0x000BCCC2
		public Datatype_unsignedByte()
		{
		}

		// Token: 0x06002361 RID: 9057 RVA: 0x000BEACA File Offset: 0x000BCCCA
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_unsignedByte()
		{
		}

		// Token: 0x040018D6 RID: 6358
		private static readonly Type atomicValueType = typeof(byte);

		// Token: 0x040018D7 RID: 6359
		private static readonly Type listValueType = typeof(byte[]);

		// Token: 0x040018D8 RID: 6360
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(0m, 255m);
	}
}
