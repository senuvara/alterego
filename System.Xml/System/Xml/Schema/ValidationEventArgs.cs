﻿using System;
using Unity;

namespace System.Xml.Schema
{
	/// <summary>Returns detailed information related to the <see langword="ValidationEventHandler" />.</summary>
	// Token: 0x020003EF RID: 1007
	public class ValidationEventArgs : EventArgs
	{
		// Token: 0x0600260C RID: 9740 RVA: 0x000D98AF File Offset: 0x000D7AAF
		internal ValidationEventArgs(XmlSchemaException ex)
		{
			this.ex = ex;
			this.severity = XmlSeverityType.Error;
		}

		// Token: 0x0600260D RID: 9741 RVA: 0x000D98C5 File Offset: 0x000D7AC5
		internal ValidationEventArgs(XmlSchemaException ex, XmlSeverityType severity)
		{
			this.ex = ex;
			this.severity = severity;
		}

		/// <summary>Gets the severity of the validation event.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSeverityType" /> value representing the severity of the validation event.</returns>
		// Token: 0x170007E6 RID: 2022
		// (get) Token: 0x0600260E RID: 9742 RVA: 0x000D98DB File Offset: 0x000D7ADB
		public XmlSeverityType Severity
		{
			get
			{
				return this.severity;
			}
		}

		/// <summary>Gets the <see cref="T:System.Xml.Schema.XmlSchemaException" /> associated with the validation event.</summary>
		/// <returns>The <see langword="XmlSchemaException" /> associated with the validation event.</returns>
		// Token: 0x170007E7 RID: 2023
		// (get) Token: 0x0600260F RID: 9743 RVA: 0x000D98E3 File Offset: 0x000D7AE3
		public XmlSchemaException Exception
		{
			get
			{
				return this.ex;
			}
		}

		/// <summary>Gets the text description corresponding to the validation event.</summary>
		/// <returns>The text description.</returns>
		// Token: 0x170007E8 RID: 2024
		// (get) Token: 0x06002610 RID: 9744 RVA: 0x000D98EB File Offset: 0x000D7AEB
		public string Message
		{
			get
			{
				return this.ex.Message;
			}
		}

		// Token: 0x06002611 RID: 9745 RVA: 0x00072668 File Offset: 0x00070868
		internal ValidationEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001B1F RID: 6943
		private XmlSchemaException ex;

		// Token: 0x04001B20 RID: 6944
		private XmlSeverityType severity;
	}
}
