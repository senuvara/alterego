﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x02000360 RID: 864
	internal class SelectorActiveAxis : ActiveAxis
	{
		// Token: 0x1700067B RID: 1659
		// (get) Token: 0x06002137 RID: 8503 RVA: 0x000B8FA3 File Offset: 0x000B71A3
		public bool EmptyStack
		{
			get
			{
				return this.KSpointer == 0;
			}
		}

		// Token: 0x1700067C RID: 1660
		// (get) Token: 0x06002138 RID: 8504 RVA: 0x000B8FAE File Offset: 0x000B71AE
		public int lastDepth
		{
			get
			{
				if (this.KSpointer != 0)
				{
					return ((KSStruct)this.KSs[this.KSpointer - 1]).depth;
				}
				return -1;
			}
		}

		// Token: 0x06002139 RID: 8505 RVA: 0x000B8FD7 File Offset: 0x000B71D7
		public SelectorActiveAxis(Asttree axisTree, ConstraintStruct cs) : base(axisTree)
		{
			this.KSs = new ArrayList();
			this.cs = cs;
		}

		// Token: 0x0600213A RID: 8506 RVA: 0x000B8FF2 File Offset: 0x000B71F2
		public override bool EndElement(string localname, string URN)
		{
			base.EndElement(localname, URN);
			return this.KSpointer > 0 && base.CurrentDepth == this.lastDepth;
		}

		// Token: 0x0600213B RID: 8507 RVA: 0x000B9018 File Offset: 0x000B7218
		public int PushKS(int errline, int errcol)
		{
			KeySequence ks = new KeySequence(this.cs.TableDim, errline, errcol);
			KSStruct ksstruct;
			if (this.KSpointer < this.KSs.Count)
			{
				ksstruct = (KSStruct)this.KSs[this.KSpointer];
				ksstruct.ks = ks;
				for (int i = 0; i < this.cs.TableDim; i++)
				{
					ksstruct.fields[i].Reactivate(ks);
				}
			}
			else
			{
				ksstruct = new KSStruct(ks, this.cs.TableDim);
				for (int j = 0; j < this.cs.TableDim; j++)
				{
					ksstruct.fields[j] = new LocatedActiveAxis(this.cs.constraint.Fields[j], ks, j);
					this.cs.axisFields.Add(ksstruct.fields[j]);
				}
				this.KSs.Add(ksstruct);
			}
			ksstruct.depth = base.CurrentDepth - 1;
			int kspointer = this.KSpointer;
			this.KSpointer = kspointer + 1;
			return kspointer;
		}

		// Token: 0x0600213C RID: 8508 RVA: 0x000B9120 File Offset: 0x000B7320
		public KeySequence PopKS()
		{
			ArrayList kss = this.KSs;
			int num = this.KSpointer - 1;
			this.KSpointer = num;
			return ((KSStruct)kss[num]).ks;
		}

		// Token: 0x040017CE RID: 6094
		private ConstraintStruct cs;

		// Token: 0x040017CF RID: 6095
		private ArrayList KSs;

		// Token: 0x040017D0 RID: 6096
		private int KSpointer;
	}
}
