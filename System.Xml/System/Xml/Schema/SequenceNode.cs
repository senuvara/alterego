﻿using System;
using System.Collections.Generic;

namespace System.Xml.Schema
{
	// Token: 0x0200036D RID: 877
	internal sealed class SequenceNode : InteriorNode
	{
		// Token: 0x0600218A RID: 8586 RVA: 0x000B9EF4 File Offset: 0x000B80F4
		public override void ConstructPos(BitSet firstpos, BitSet lastpos, BitSet[] followpos)
		{
			Stack<SequenceNode.SequenceConstructPosContext> stack = new Stack<SequenceNode.SequenceConstructPosContext>();
			SequenceNode.SequenceConstructPosContext sequenceConstructPosContext = new SequenceNode.SequenceConstructPosContext(this, firstpos, lastpos);
			SequenceNode this_;
			for (;;)
			{
				this_ = sequenceConstructPosContext.this_;
				sequenceConstructPosContext.lastposLeft = new BitSet(lastpos.Count);
				if (!(this_.LeftChild is SequenceNode))
				{
					break;
				}
				stack.Push(sequenceConstructPosContext);
				sequenceConstructPosContext = new SequenceNode.SequenceConstructPosContext((SequenceNode)this_.LeftChild, sequenceConstructPosContext.firstpos, sequenceConstructPosContext.lastposLeft);
			}
			this_.LeftChild.ConstructPos(sequenceConstructPosContext.firstpos, sequenceConstructPosContext.lastposLeft, followpos);
			for (;;)
			{
				sequenceConstructPosContext.firstposRight = new BitSet(firstpos.Count);
				this_.RightChild.ConstructPos(sequenceConstructPosContext.firstposRight, sequenceConstructPosContext.lastpos, followpos);
				if (this_.LeftChild.IsNullable && !this_.RightChild.IsRangeNode)
				{
					sequenceConstructPosContext.firstpos.Or(sequenceConstructPosContext.firstposRight);
				}
				if (this_.RightChild.IsNullable)
				{
					sequenceConstructPosContext.lastpos.Or(sequenceConstructPosContext.lastposLeft);
				}
				for (int num = sequenceConstructPosContext.lastposLeft.NextSet(-1); num != -1; num = sequenceConstructPosContext.lastposLeft.NextSet(num))
				{
					followpos[num].Or(sequenceConstructPosContext.firstposRight);
				}
				if (this_.RightChild.IsRangeNode)
				{
					((LeafRangeNode)this_.RightChild).NextIteration = sequenceConstructPosContext.firstpos.Clone();
				}
				if (stack.Count == 0)
				{
					break;
				}
				sequenceConstructPosContext = stack.Pop();
				this_ = sequenceConstructPosContext.this_;
			}
		}

		// Token: 0x17000697 RID: 1687
		// (get) Token: 0x0600218B RID: 8587 RVA: 0x000BA05C File Offset: 0x000B825C
		public override bool IsNullable
		{
			get
			{
				SequenceNode sequenceNode = this;
				while (!sequenceNode.RightChild.IsRangeNode || !(((LeafRangeNode)sequenceNode.RightChild).Min == 0m))
				{
					if (!sequenceNode.RightChild.IsNullable && !sequenceNode.RightChild.IsRangeNode)
					{
						return false;
					}
					SyntaxTreeNode leftChild = sequenceNode.LeftChild;
					sequenceNode = (leftChild as SequenceNode);
					if (sequenceNode == null)
					{
						return leftChild.IsNullable;
					}
				}
				return true;
			}
		}

		// Token: 0x0600218C RID: 8588 RVA: 0x000BA0C9 File Offset: 0x000B82C9
		public override void ExpandTree(InteriorNode parent, SymbolsDictionary symbols, Positions positions)
		{
			base.ExpandTreeNoRecursive(parent, symbols, positions);
		}

		// Token: 0x0600218D RID: 8589 RVA: 0x000BA0D4 File Offset: 0x000B82D4
		public SequenceNode()
		{
		}

		// Token: 0x0200036E RID: 878
		private struct SequenceConstructPosContext
		{
			// Token: 0x0600218E RID: 8590 RVA: 0x000BA0DC File Offset: 0x000B82DC
			public SequenceConstructPosContext(SequenceNode node, BitSet firstpos, BitSet lastpos)
			{
				this.this_ = node;
				this.firstpos = firstpos;
				this.lastpos = lastpos;
				this.lastposLeft = null;
				this.firstposRight = null;
			}

			// Token: 0x040017F1 RID: 6129
			public SequenceNode this_;

			// Token: 0x040017F2 RID: 6130
			public BitSet firstpos;

			// Token: 0x040017F3 RID: 6131
			public BitSet lastpos;

			// Token: 0x040017F4 RID: 6132
			public BitSet lastposLeft;

			// Token: 0x040017F5 RID: 6133
			public BitSet firstposRight;
		}
	}
}
