﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="maxLength" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to specify a restriction on the maximum length of the data value of a <see langword="simpleType" /> element. The length must be less than the value of the <see langword="maxLength" /> element.</summary>
	// Token: 0x02000424 RID: 1060
	public class XmlSchemaMaxLengthFacet : XmlSchemaNumericFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaMaxLengthFacet" /> class.</summary>
		// Token: 0x06002878 RID: 10360 RVA: 0x000E05FD File Offset: 0x000DE7FD
		public XmlSchemaMaxLengthFacet()
		{
			base.FacetType = FacetType.MaxLength;
		}
	}
}
