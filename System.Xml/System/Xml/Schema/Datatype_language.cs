﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A4 RID: 932
	internal class Datatype_language : Datatype_token
	{
		// Token: 0x17000727 RID: 1831
		// (get) Token: 0x060022F1 RID: 8945 RVA: 0x000BE248 File Offset: 0x000BC448
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Language;
			}
		}

		// Token: 0x060022F2 RID: 8946 RVA: 0x000BE24C File Offset: 0x000BC44C
		public Datatype_language()
		{
		}
	}
}
