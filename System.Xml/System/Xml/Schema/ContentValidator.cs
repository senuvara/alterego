﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x02000374 RID: 884
	internal class ContentValidator
	{
		// Token: 0x060021A6 RID: 8614 RVA: 0x000BA2E1 File Offset: 0x000B84E1
		public ContentValidator(XmlSchemaContentType contentType)
		{
			this.contentType = contentType;
			this.isEmptiable = true;
		}

		// Token: 0x060021A7 RID: 8615 RVA: 0x000BA2F7 File Offset: 0x000B84F7
		protected ContentValidator(XmlSchemaContentType contentType, bool isOpen, bool isEmptiable)
		{
			this.contentType = contentType;
			this.isOpen = isOpen;
			this.isEmptiable = isEmptiable;
		}

		// Token: 0x170006A0 RID: 1696
		// (get) Token: 0x060021A8 RID: 8616 RVA: 0x000BA314 File Offset: 0x000B8514
		public XmlSchemaContentType ContentType
		{
			get
			{
				return this.contentType;
			}
		}

		// Token: 0x170006A1 RID: 1697
		// (get) Token: 0x060021A9 RID: 8617 RVA: 0x000BA31C File Offset: 0x000B851C
		public bool PreserveWhitespace
		{
			get
			{
				return this.contentType == XmlSchemaContentType.TextOnly || this.contentType == XmlSchemaContentType.Mixed;
			}
		}

		// Token: 0x170006A2 RID: 1698
		// (get) Token: 0x060021AA RID: 8618 RVA: 0x000BA331 File Offset: 0x000B8531
		public virtual bool IsEmptiable
		{
			get
			{
				return this.isEmptiable;
			}
		}

		// Token: 0x170006A3 RID: 1699
		// (get) Token: 0x060021AB RID: 8619 RVA: 0x000BA339 File Offset: 0x000B8539
		// (set) Token: 0x060021AC RID: 8620 RVA: 0x000BA354 File Offset: 0x000B8554
		public bool IsOpen
		{
			get
			{
				return this.contentType != XmlSchemaContentType.TextOnly && this.contentType != XmlSchemaContentType.Empty && this.isOpen;
			}
			set
			{
				this.isOpen = value;
			}
		}

		// Token: 0x060021AD RID: 8621 RVA: 0x000030EC File Offset: 0x000012EC
		public virtual void InitValidation(ValidationState context)
		{
		}

		// Token: 0x060021AE RID: 8622 RVA: 0x000BA35D File Offset: 0x000B855D
		public virtual object ValidateElement(XmlQualifiedName name, ValidationState context, out int errorCode)
		{
			if (this.contentType == XmlSchemaContentType.TextOnly || this.contentType == XmlSchemaContentType.Empty)
			{
				context.NeedValidateChildren = false;
			}
			errorCode = -1;
			return null;
		}

		// Token: 0x060021AF RID: 8623 RVA: 0x000033DE File Offset: 0x000015DE
		public virtual bool CompleteValidation(ValidationState context)
		{
			return true;
		}

		// Token: 0x060021B0 RID: 8624 RVA: 0x000037FB File Offset: 0x000019FB
		public virtual ArrayList ExpectedElements(ValidationState context, bool isRequiredOnly)
		{
			return null;
		}

		// Token: 0x060021B1 RID: 8625 RVA: 0x000037FB File Offset: 0x000019FB
		public virtual ArrayList ExpectedParticles(ValidationState context, bool isRequiredOnly, XmlSchemaSet schemaSet)
		{
			return null;
		}

		// Token: 0x060021B2 RID: 8626 RVA: 0x000BA37B File Offset: 0x000B857B
		public static void AddParticleToExpected(XmlSchemaParticle p, XmlSchemaSet schemaSet, ArrayList particles)
		{
			ContentValidator.AddParticleToExpected(p, schemaSet, particles, false);
		}

		// Token: 0x060021B3 RID: 8627 RVA: 0x000BA388 File Offset: 0x000B8588
		public static void AddParticleToExpected(XmlSchemaParticle p, XmlSchemaSet schemaSet, ArrayList particles, bool global)
		{
			if (!particles.Contains(p))
			{
				particles.Add(p);
			}
			XmlSchemaElement xmlSchemaElement = p as XmlSchemaElement;
			if (xmlSchemaElement != null && (global || !xmlSchemaElement.RefName.IsEmpty))
			{
				XmlSchemaSubstitutionGroup xmlSchemaSubstitutionGroup = (XmlSchemaSubstitutionGroup)schemaSet.SubstitutionGroups[xmlSchemaElement.QualifiedName];
				if (xmlSchemaSubstitutionGroup != null)
				{
					for (int i = 0; i < xmlSchemaSubstitutionGroup.Members.Count; i++)
					{
						XmlSchemaElement xmlSchemaElement2 = (XmlSchemaElement)xmlSchemaSubstitutionGroup.Members[i];
						if (!xmlSchemaElement.QualifiedName.Equals(xmlSchemaElement2.QualifiedName) && !particles.Contains(xmlSchemaElement2))
						{
							particles.Add(xmlSchemaElement2);
						}
					}
				}
			}
		}

		// Token: 0x060021B4 RID: 8628 RVA: 0x000BA426 File Offset: 0x000B8626
		// Note: this type is marked as 'beforefieldinit'.
		static ContentValidator()
		{
		}

		// Token: 0x040017F9 RID: 6137
		private XmlSchemaContentType contentType;

		// Token: 0x040017FA RID: 6138
		private bool isOpen;

		// Token: 0x040017FB RID: 6139
		private bool isEmptiable;

		// Token: 0x040017FC RID: 6140
		public static readonly ContentValidator Empty = new ContentValidator(XmlSchemaContentType.Empty);

		// Token: 0x040017FD RID: 6141
		public static readonly ContentValidator TextOnly = new ContentValidator(XmlSchemaContentType.TextOnly, false, false);

		// Token: 0x040017FE RID: 6142
		public static readonly ContentValidator Mixed = new ContentValidator(XmlSchemaContentType.Mixed);

		// Token: 0x040017FF RID: 6143
		public static readonly ContentValidator Any = new ContentValidator(XmlSchemaContentType.Mixed, true, true);
	}
}
