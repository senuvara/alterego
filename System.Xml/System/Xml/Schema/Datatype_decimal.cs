﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200038B RID: 907
	internal class Datatype_decimal : Datatype_anySimpleType
	{
		// Token: 0x0600227D RID: 8829 RVA: 0x000BDB2E File Offset: 0x000BBD2E
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlNumeric10Converter.Create(schemaType);
		}

		// Token: 0x170006EA RID: 1770
		// (get) Token: 0x0600227E RID: 8830 RVA: 0x000BDB36 File Offset: 0x000BBD36
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_decimal.numeric10FacetsChecker;
			}
		}

		// Token: 0x170006EB RID: 1771
		// (get) Token: 0x0600227F RID: 8831 RVA: 0x0007BAEA File Offset: 0x00079CEA
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Decimal;
			}
		}

		// Token: 0x170006EC RID: 1772
		// (get) Token: 0x06002280 RID: 8832 RVA: 0x000BDB3D File Offset: 0x000BBD3D
		public override Type ValueType
		{
			get
			{
				return Datatype_decimal.atomicValueType;
			}
		}

		// Token: 0x170006ED RID: 1773
		// (get) Token: 0x06002281 RID: 8833 RVA: 0x000BDB44 File Offset: 0x000BBD44
		internal override Type ListValueType
		{
			get
			{
				return Datatype_decimal.listValueType;
			}
		}

		// Token: 0x170006EE RID: 1774
		// (get) Token: 0x06002282 RID: 8834 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x170006EF RID: 1775
		// (get) Token: 0x06002283 RID: 8835 RVA: 0x000BDB4B File Offset: 0x000BBD4B
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace | RestrictionFlags.MaxInclusive | RestrictionFlags.MaxExclusive | RestrictionFlags.MinInclusive | RestrictionFlags.MinExclusive | RestrictionFlags.TotalDigits | RestrictionFlags.FractionDigits;
			}
		}

		// Token: 0x06002284 RID: 8836 RVA: 0x000BDB54 File Offset: 0x000BBD54
		internal override int Compare(object value1, object value2)
		{
			return ((decimal)value1).CompareTo(value2);
		}

		// Token: 0x06002285 RID: 8837 RVA: 0x000BDB70 File Offset: 0x000BBD70
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_decimal.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				decimal num;
				ex = XmlConvert.TryToDecimal(s, out num);
				if (ex == null)
				{
					ex = Datatype_decimal.numeric10FacetsChecker.CheckValueFacets(num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002286 RID: 8838 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_decimal()
		{
		}

		// Token: 0x06002287 RID: 8839 RVA: 0x000BDBBA File Offset: 0x000BBDBA
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_decimal()
		{
		}

		// Token: 0x040018AC RID: 6316
		private static readonly Type atomicValueType = typeof(decimal);

		// Token: 0x040018AD RID: 6317
		private static readonly Type listValueType = typeof(decimal[]);

		// Token: 0x040018AE RID: 6318
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(decimal.MinValue, decimal.MaxValue);
	}
}
