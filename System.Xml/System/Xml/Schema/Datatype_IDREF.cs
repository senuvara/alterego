﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A9 RID: 937
	internal class Datatype_IDREF : Datatype_NCName
	{
		// Token: 0x1700072E RID: 1838
		// (get) Token: 0x060022FE RID: 8958 RVA: 0x000BE2B1 File Offset: 0x000BC4B1
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Idref;
			}
		}

		// Token: 0x1700072F RID: 1839
		// (get) Token: 0x060022FF RID: 8959 RVA: 0x0000284A File Offset: 0x00000A4A
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.IDREF;
			}
		}

		// Token: 0x06002300 RID: 8960 RVA: 0x000BE2A9 File Offset: 0x000BC4A9
		public Datatype_IDREF()
		{
		}
	}
}
