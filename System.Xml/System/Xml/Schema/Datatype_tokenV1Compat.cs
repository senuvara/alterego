﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A3 RID: 931
	internal class Datatype_tokenV1Compat : Datatype_normalizedStringV1Compat
	{
		// Token: 0x17000726 RID: 1830
		// (get) Token: 0x060022EF RID: 8943 RVA: 0x000BE234 File Offset: 0x000BC434
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Token;
			}
		}

		// Token: 0x060022F0 RID: 8944 RVA: 0x000BE240 File Offset: 0x000BC440
		public Datatype_tokenV1Compat()
		{
		}
	}
}
