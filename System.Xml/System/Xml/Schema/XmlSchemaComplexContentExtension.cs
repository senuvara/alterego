﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="extension" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class is for complex types with complex content model derived by extension. It extends the complex type by adding attributes or elements.</summary>
	// Token: 0x02000412 RID: 1042
	public class XmlSchemaComplexContentExtension : XmlSchemaContent
	{
		/// <summary>Gets or sets the name of the complex type from which this type is derived by extension.</summary>
		/// <returns>The name of the complex type from which this type is derived by extension.</returns>
		// Token: 0x1700085A RID: 2138
		// (get) Token: 0x060027A7 RID: 10151 RVA: 0x000DEECC File Offset: 0x000DD0CC
		// (set) Token: 0x060027A8 RID: 10152 RVA: 0x000DEED4 File Offset: 0x000DD0D4
		[XmlAttribute("base")]
		public XmlQualifiedName BaseTypeName
		{
			get
			{
				return this.baseTypeName;
			}
			set
			{
				this.baseTypeName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets or sets one of the <see cref="T:System.Xml.Schema.XmlSchemaGroupRef" />, <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, <see cref="T:System.Xml.Schema.XmlSchemaAll" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" /> classes.</summary>
		/// <returns>One of the <see cref="T:System.Xml.Schema.XmlSchemaGroupRef" />, <see cref="T:System.Xml.Schema.XmlSchemaChoice" />, <see cref="T:System.Xml.Schema.XmlSchemaAll" />, or <see cref="T:System.Xml.Schema.XmlSchemaSequence" /> classes.</returns>
		// Token: 0x1700085B RID: 2139
		// (get) Token: 0x060027A9 RID: 10153 RVA: 0x000DEEED File Offset: 0x000DD0ED
		// (set) Token: 0x060027AA RID: 10154 RVA: 0x000DEEF5 File Offset: 0x000DD0F5
		[XmlElement("group", typeof(XmlSchemaGroupRef))]
		[XmlElement("all", typeof(XmlSchemaAll))]
		[XmlElement("sequence", typeof(XmlSchemaSequence))]
		[XmlElement("choice", typeof(XmlSchemaChoice))]
		public XmlSchemaParticle Particle
		{
			get
			{
				return this.particle;
			}
			set
			{
				this.particle = value;
			}
		}

		/// <summary>Gets the collection of attributes for the complex content. Contains <see cref="T:System.Xml.Schema.XmlSchemaAttribute" /> and <see cref="T:System.Xml.Schema.XmlSchemaAttributeGroupRef" /> elements.</summary>
		/// <returns>The collection of attributes for the complex content.</returns>
		// Token: 0x1700085C RID: 2140
		// (get) Token: 0x060027AB RID: 10155 RVA: 0x000DEEFE File Offset: 0x000DD0FE
		[XmlElement("attributeGroup", typeof(XmlSchemaAttributeGroupRef))]
		[XmlElement("attribute", typeof(XmlSchemaAttribute))]
		public XmlSchemaObjectCollection Attributes
		{
			get
			{
				return this.attributes;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaAnyAttribute" /> component of the complex content model.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaAnyAttribute" /> component of the complex content model.</returns>
		// Token: 0x1700085D RID: 2141
		// (get) Token: 0x060027AC RID: 10156 RVA: 0x000DEF06 File Offset: 0x000DD106
		// (set) Token: 0x060027AD RID: 10157 RVA: 0x000DEF0E File Offset: 0x000DD10E
		[XmlElement("anyAttribute")]
		public XmlSchemaAnyAttribute AnyAttribute
		{
			get
			{
				return this.anyAttribute;
			}
			set
			{
				this.anyAttribute = value;
			}
		}

		// Token: 0x060027AE RID: 10158 RVA: 0x000DEF17 File Offset: 0x000DD117
		internal void SetAttributes(XmlSchemaObjectCollection newAttributes)
		{
			this.attributes = newAttributes;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaComplexContentExtension" /> class.</summary>
		// Token: 0x060027AF RID: 10159 RVA: 0x000DEF20 File Offset: 0x000DD120
		public XmlSchemaComplexContentExtension()
		{
		}

		// Token: 0x04001C13 RID: 7187
		private XmlSchemaParticle particle;

		// Token: 0x04001C14 RID: 7188
		private XmlSchemaObjectCollection attributes = new XmlSchemaObjectCollection();

		// Token: 0x04001C15 RID: 7189
		private XmlSchemaAnyAttribute anyAttribute;

		// Token: 0x04001C16 RID: 7190
		private XmlQualifiedName baseTypeName = XmlQualifiedName.Empty;
	}
}
