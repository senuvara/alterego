﻿using System;
using System.Runtime.InteropServices;

namespace System.Xml.Schema
{
	// Token: 0x020003F1 RID: 1009
	[StructLayout(LayoutKind.Explicit)]
	internal struct StateUnion
	{
		// Token: 0x04001B21 RID: 6945
		[FieldOffset(0)]
		public int State;

		// Token: 0x04001B22 RID: 6946
		[FieldOffset(0)]
		public int AllElementsRequired;

		// Token: 0x04001B23 RID: 6947
		[FieldOffset(0)]
		public int CurPosIndex;

		// Token: 0x04001B24 RID: 6948
		[FieldOffset(0)]
		public int NumberOfRunningPos;
	}
}
