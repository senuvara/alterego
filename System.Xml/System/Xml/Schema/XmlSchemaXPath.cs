﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the World Wide Web Consortium (W3C) <see langword="selector" /> element.</summary>
	// Token: 0x02000433 RID: 1075
	public class XmlSchemaXPath : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the attribute for the XPath expression.</summary>
		/// <returns>The string attribute value for the XPath expression.</returns>
		// Token: 0x170008C0 RID: 2240
		// (get) Token: 0x060028A9 RID: 10409 RVA: 0x000E0846 File Offset: 0x000DEA46
		// (set) Token: 0x060028AA RID: 10410 RVA: 0x000E084E File Offset: 0x000DEA4E
		[XmlAttribute("xpath")]
		[DefaultValue("")]
		public string XPath
		{
			get
			{
				return this.xpath;
			}
			set
			{
				this.xpath = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaXPath" /> class.</summary>
		// Token: 0x060028AB RID: 10411 RVA: 0x000DE4D5 File Offset: 0x000DC6D5
		public XmlSchemaXPath()
		{
		}

		// Token: 0x04001C88 RID: 7304
		private string xpath;
	}
}
