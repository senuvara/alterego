﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the World Wide Web Consortium (W3C) <see langword="whiteSpace" /> facet.</summary>
	// Token: 0x0200042D RID: 1069
	public class XmlSchemaWhiteSpaceFacet : XmlSchemaFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaWhiteSpaceFacet" /> class.</summary>
		// Token: 0x06002881 RID: 10369 RVA: 0x000E0688 File Offset: 0x000DE888
		public XmlSchemaWhiteSpaceFacet()
		{
			base.FacetType = FacetType.Whitespace;
		}
	}
}
