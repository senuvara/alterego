﻿using System;
using System.Collections.Generic;

namespace System.Xml.Schema
{
	// Token: 0x0200036C RID: 876
	internal abstract class InteriorNode : SyntaxTreeNode
	{
		// Token: 0x17000695 RID: 1685
		// (get) Token: 0x06002182 RID: 8578 RVA: 0x000B9DE0 File Offset: 0x000B7FE0
		// (set) Token: 0x06002183 RID: 8579 RVA: 0x000B9DE8 File Offset: 0x000B7FE8
		public SyntaxTreeNode LeftChild
		{
			get
			{
				return this.leftChild;
			}
			set
			{
				this.leftChild = value;
			}
		}

		// Token: 0x17000696 RID: 1686
		// (get) Token: 0x06002184 RID: 8580 RVA: 0x000B9DF1 File Offset: 0x000B7FF1
		// (set) Token: 0x06002185 RID: 8581 RVA: 0x000B9DF9 File Offset: 0x000B7FF9
		public SyntaxTreeNode RightChild
		{
			get
			{
				return this.rightChild;
			}
			set
			{
				this.rightChild = value;
			}
		}

		// Token: 0x06002186 RID: 8582 RVA: 0x000B9E04 File Offset: 0x000B8004
		public override SyntaxTreeNode Clone(Positions positions)
		{
			InteriorNode interiorNode = (InteriorNode)base.MemberwiseClone();
			interiorNode.LeftChild = this.leftChild.Clone(positions);
			if (this.rightChild != null)
			{
				interiorNode.RightChild = this.rightChild.Clone(positions);
			}
			return interiorNode;
		}

		// Token: 0x06002187 RID: 8583 RVA: 0x000B9E4C File Offset: 0x000B804C
		protected void ExpandTreeNoRecursive(InteriorNode parent, SymbolsDictionary symbols, Positions positions)
		{
			Stack<InteriorNode> stack = new Stack<InteriorNode>();
			InteriorNode interiorNode = this;
			while (interiorNode.leftChild is ChoiceNode || interiorNode.leftChild is SequenceNode)
			{
				stack.Push(interiorNode);
				interiorNode = (InteriorNode)interiorNode.leftChild;
			}
			interiorNode.leftChild.ExpandTree(interiorNode, symbols, positions);
			for (;;)
			{
				if (interiorNode.rightChild != null)
				{
					interiorNode.rightChild.ExpandTree(interiorNode, symbols, positions);
				}
				if (stack.Count == 0)
				{
					break;
				}
				interiorNode = stack.Pop();
			}
		}

		// Token: 0x06002188 RID: 8584 RVA: 0x000B9EC5 File Offset: 0x000B80C5
		public override void ExpandTree(InteriorNode parent, SymbolsDictionary symbols, Positions positions)
		{
			this.leftChild.ExpandTree(this, symbols, positions);
			if (this.rightChild != null)
			{
				this.rightChild.ExpandTree(this, symbols, positions);
			}
		}

		// Token: 0x06002189 RID: 8585 RVA: 0x000B9EEB File Offset: 0x000B80EB
		protected InteriorNode()
		{
		}

		// Token: 0x040017EF RID: 6127
		private SyntaxTreeNode leftChild;

		// Token: 0x040017F0 RID: 6128
		private SyntaxTreeNode rightChild;
	}
}
