﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Specifies the order and structure of the child elements of a type.</summary>
	// Token: 0x02000416 RID: 1046
	public abstract class XmlSchemaContentModel : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the content of the type.</summary>
		/// <returns>Provides the content of the type.</returns>
		// Token: 0x17000875 RID: 2165
		// (get) Token: 0x060027E5 RID: 10213
		// (set) Token: 0x060027E6 RID: 10214
		[XmlIgnore]
		public abstract XmlSchemaContent Content { get; set; }

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaContentModel" /> class.</summary>
		// Token: 0x060027E7 RID: 10215 RVA: 0x000DE4D5 File Offset: 0x000DC6D5
		protected XmlSchemaContentModel()
		{
		}
	}
}
