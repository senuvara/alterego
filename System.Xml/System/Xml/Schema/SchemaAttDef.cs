﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003DC RID: 988
	internal sealed class SchemaAttDef : SchemaDeclBase, IDtdDefaultAttributeInfo, IDtdAttributeInfo
	{
		// Token: 0x0600248C RID: 9356 RVA: 0x000CAC00 File Offset: 0x000C8E00
		public SchemaAttDef(XmlQualifiedName name, string prefix) : base(name, prefix)
		{
		}

		// Token: 0x0600248D RID: 9357 RVA: 0x000CAC0A File Offset: 0x000C8E0A
		public SchemaAttDef(XmlQualifiedName name) : base(name, null)
		{
		}

		// Token: 0x0600248E RID: 9358 RVA: 0x000CAC14 File Offset: 0x000C8E14
		private SchemaAttDef()
		{
		}

		// Token: 0x17000785 RID: 1925
		// (get) Token: 0x0600248F RID: 9359 RVA: 0x000CAC1C File Offset: 0x000C8E1C
		string IDtdAttributeInfo.Prefix
		{
			get
			{
				return base.Prefix;
			}
		}

		// Token: 0x17000786 RID: 1926
		// (get) Token: 0x06002490 RID: 9360 RVA: 0x000CAC24 File Offset: 0x000C8E24
		string IDtdAttributeInfo.LocalName
		{
			get
			{
				return base.Name.Name;
			}
		}

		// Token: 0x17000787 RID: 1927
		// (get) Token: 0x06002491 RID: 9361 RVA: 0x000CAC31 File Offset: 0x000C8E31
		int IDtdAttributeInfo.LineNumber
		{
			get
			{
				return this.LineNumber;
			}
		}

		// Token: 0x17000788 RID: 1928
		// (get) Token: 0x06002492 RID: 9362 RVA: 0x000CAC39 File Offset: 0x000C8E39
		int IDtdAttributeInfo.LinePosition
		{
			get
			{
				return this.LinePosition;
			}
		}

		// Token: 0x17000789 RID: 1929
		// (get) Token: 0x06002493 RID: 9363 RVA: 0x000CAC41 File Offset: 0x000C8E41
		bool IDtdAttributeInfo.IsNonCDataType
		{
			get
			{
				return this.TokenizedType > XmlTokenizedType.CDATA;
			}
		}

		// Token: 0x1700078A RID: 1930
		// (get) Token: 0x06002494 RID: 9364 RVA: 0x000CAC4C File Offset: 0x000C8E4C
		bool IDtdAttributeInfo.IsDeclaredInExternal
		{
			get
			{
				return base.IsDeclaredInExternal;
			}
		}

		// Token: 0x1700078B RID: 1931
		// (get) Token: 0x06002495 RID: 9365 RVA: 0x000CAC54 File Offset: 0x000C8E54
		bool IDtdAttributeInfo.IsXmlAttribute
		{
			get
			{
				return this.Reserved > SchemaAttDef.Reserve.None;
			}
		}

		// Token: 0x1700078C RID: 1932
		// (get) Token: 0x06002496 RID: 9366 RVA: 0x000CAC5F File Offset: 0x000C8E5F
		string IDtdDefaultAttributeInfo.DefaultValueExpanded
		{
			get
			{
				return this.DefaultValueExpanded;
			}
		}

		// Token: 0x1700078D RID: 1933
		// (get) Token: 0x06002497 RID: 9367 RVA: 0x000CAC67 File Offset: 0x000C8E67
		object IDtdDefaultAttributeInfo.DefaultValueTyped
		{
			get
			{
				return base.DefaultValueTyped;
			}
		}

		// Token: 0x1700078E RID: 1934
		// (get) Token: 0x06002498 RID: 9368 RVA: 0x000CAC6F File Offset: 0x000C8E6F
		int IDtdDefaultAttributeInfo.ValueLineNumber
		{
			get
			{
				return this.ValueLineNumber;
			}
		}

		// Token: 0x1700078F RID: 1935
		// (get) Token: 0x06002499 RID: 9369 RVA: 0x000CAC77 File Offset: 0x000C8E77
		int IDtdDefaultAttributeInfo.ValueLinePosition
		{
			get
			{
				return this.ValueLinePosition;
			}
		}

		// Token: 0x17000790 RID: 1936
		// (get) Token: 0x0600249A RID: 9370 RVA: 0x000CAC7F File Offset: 0x000C8E7F
		// (set) Token: 0x0600249B RID: 9371 RVA: 0x000CAC87 File Offset: 0x000C8E87
		internal int LinePosition
		{
			get
			{
				return this.linePos;
			}
			set
			{
				this.linePos = value;
			}
		}

		// Token: 0x17000791 RID: 1937
		// (get) Token: 0x0600249C RID: 9372 RVA: 0x000CAC90 File Offset: 0x000C8E90
		// (set) Token: 0x0600249D RID: 9373 RVA: 0x000CAC98 File Offset: 0x000C8E98
		internal int LineNumber
		{
			get
			{
				return this.lineNum;
			}
			set
			{
				this.lineNum = value;
			}
		}

		// Token: 0x17000792 RID: 1938
		// (get) Token: 0x0600249E RID: 9374 RVA: 0x000CACA1 File Offset: 0x000C8EA1
		// (set) Token: 0x0600249F RID: 9375 RVA: 0x000CACA9 File Offset: 0x000C8EA9
		internal int ValueLinePosition
		{
			get
			{
				return this.valueLinePos;
			}
			set
			{
				this.valueLinePos = value;
			}
		}

		// Token: 0x17000793 RID: 1939
		// (get) Token: 0x060024A0 RID: 9376 RVA: 0x000CACB2 File Offset: 0x000C8EB2
		// (set) Token: 0x060024A1 RID: 9377 RVA: 0x000CACBA File Offset: 0x000C8EBA
		internal int ValueLineNumber
		{
			get
			{
				return this.valueLineNum;
			}
			set
			{
				this.valueLineNum = value;
			}
		}

		// Token: 0x17000794 RID: 1940
		// (get) Token: 0x060024A2 RID: 9378 RVA: 0x000CACC3 File Offset: 0x000C8EC3
		// (set) Token: 0x060024A3 RID: 9379 RVA: 0x000CACD9 File Offset: 0x000C8ED9
		internal string DefaultValueExpanded
		{
			get
			{
				if (this.defExpanded == null)
				{
					return string.Empty;
				}
				return this.defExpanded;
			}
			set
			{
				this.defExpanded = value;
			}
		}

		// Token: 0x17000795 RID: 1941
		// (get) Token: 0x060024A4 RID: 9380 RVA: 0x000CACE2 File Offset: 0x000C8EE2
		// (set) Token: 0x060024A5 RID: 9381 RVA: 0x000CACEF File Offset: 0x000C8EEF
		internal XmlTokenizedType TokenizedType
		{
			get
			{
				return base.Datatype.TokenizedType;
			}
			set
			{
				base.Datatype = XmlSchemaDatatype.FromXmlTokenizedType(value);
			}
		}

		// Token: 0x17000796 RID: 1942
		// (get) Token: 0x060024A6 RID: 9382 RVA: 0x000CACFD File Offset: 0x000C8EFD
		// (set) Token: 0x060024A7 RID: 9383 RVA: 0x000CAD05 File Offset: 0x000C8F05
		internal SchemaAttDef.Reserve Reserved
		{
			get
			{
				return this.reserved;
			}
			set
			{
				this.reserved = value;
			}
		}

		// Token: 0x17000797 RID: 1943
		// (get) Token: 0x060024A8 RID: 9384 RVA: 0x000CAD0E File Offset: 0x000C8F0E
		internal bool DefaultValueChecked
		{
			get
			{
				return this.defaultValueChecked;
			}
		}

		// Token: 0x17000798 RID: 1944
		// (get) Token: 0x060024A9 RID: 9385 RVA: 0x000CAD16 File Offset: 0x000C8F16
		// (set) Token: 0x060024AA RID: 9386 RVA: 0x000CAD1E File Offset: 0x000C8F1E
		internal bool HasEntityRef
		{
			get
			{
				return this.hasEntityRef;
			}
			set
			{
				this.hasEntityRef = value;
			}
		}

		// Token: 0x17000799 RID: 1945
		// (get) Token: 0x060024AB RID: 9387 RVA: 0x000CAD27 File Offset: 0x000C8F27
		// (set) Token: 0x060024AC RID: 9388 RVA: 0x000CAD2F File Offset: 0x000C8F2F
		internal XmlSchemaAttribute SchemaAttribute
		{
			get
			{
				return this.schemaAttribute;
			}
			set
			{
				this.schemaAttribute = value;
			}
		}

		// Token: 0x060024AD RID: 9389 RVA: 0x000CAD38 File Offset: 0x000C8F38
		internal void CheckXmlSpace(IValidationEventHandling validationEventHandling)
		{
			if (this.datatype.TokenizedType == XmlTokenizedType.ENUMERATION && this.values != null && this.values.Count <= 2)
			{
				string a = this.values[0].ToString();
				if (this.values.Count == 2)
				{
					string a2 = this.values[1].ToString();
					if ((a == "default" || a2 == "default") && (a == "preserve" || a2 == "preserve"))
					{
						return;
					}
				}
				else if (a == "default" || a == "preserve")
				{
					return;
				}
			}
			validationEventHandling.SendEvent(new XmlSchemaException("Invalid xml:space syntax.", string.Empty), XmlSeverityType.Error);
		}

		// Token: 0x060024AE RID: 9390 RVA: 0x000CAE0B File Offset: 0x000C900B
		internal SchemaAttDef Clone()
		{
			return (SchemaAttDef)base.MemberwiseClone();
		}

		// Token: 0x060024AF RID: 9391 RVA: 0x000CAE18 File Offset: 0x000C9018
		// Note: this type is marked as 'beforefieldinit'.
		static SchemaAttDef()
		{
		}

		// Token: 0x0400198B RID: 6539
		private string defExpanded;

		// Token: 0x0400198C RID: 6540
		private int lineNum;

		// Token: 0x0400198D RID: 6541
		private int linePos;

		// Token: 0x0400198E RID: 6542
		private int valueLineNum;

		// Token: 0x0400198F RID: 6543
		private int valueLinePos;

		// Token: 0x04001990 RID: 6544
		private SchemaAttDef.Reserve reserved;

		// Token: 0x04001991 RID: 6545
		private bool defaultValueChecked;

		// Token: 0x04001992 RID: 6546
		private bool hasEntityRef;

		// Token: 0x04001993 RID: 6547
		private XmlSchemaAttribute schemaAttribute;

		// Token: 0x04001994 RID: 6548
		public static readonly SchemaAttDef Empty = new SchemaAttDef();

		// Token: 0x020003DD RID: 989
		internal enum Reserve
		{
			// Token: 0x04001996 RID: 6550
			None,
			// Token: 0x04001997 RID: 6551
			XmlSpace,
			// Token: 0x04001998 RID: 6552
			XmlLang
		}
	}
}
