﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000361 RID: 865
	internal class KSStruct
	{
		// Token: 0x0600213D RID: 8509 RVA: 0x000B9153 File Offset: 0x000B7353
		public KSStruct(KeySequence ks, int dim)
		{
			this.ks = ks;
			this.fields = new LocatedActiveAxis[dim];
		}

		// Token: 0x040017D1 RID: 6097
		public int depth;

		// Token: 0x040017D2 RID: 6098
		public KeySequence ks;

		// Token: 0x040017D3 RID: 6099
		public LocatedActiveAxis[] fields;
	}
}
