﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="simpleType" /> element for simple content from XML Schema as specified by the World Wide Web Consortium (W3C). This class defines a simple type. Simple types can specify information and constraints for the value of attributes or elements with text-only content.</summary>
	// Token: 0x0200044E RID: 1102
	public class XmlSchemaSimpleType : XmlSchemaType
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaSimpleType" /> class.</summary>
		// Token: 0x06002998 RID: 10648 RVA: 0x000E35D8 File Offset: 0x000E17D8
		public XmlSchemaSimpleType()
		{
		}

		/// <summary>Gets or sets one of <see cref="T:System.Xml.Schema.XmlSchemaSimpleTypeUnion" />, <see cref="T:System.Xml.Schema.XmlSchemaSimpleTypeList" />, or <see cref="T:System.Xml.Schema.XmlSchemaSimpleTypeRestriction" />.</summary>
		/// <returns>One of <see langword="XmlSchemaSimpleTypeUnion" />, <see langword="XmlSchemaSimpleTypeList" />, or <see langword="XmlSchemaSimpleTypeRestriction" />.</returns>
		// Token: 0x17000915 RID: 2325
		// (get) Token: 0x06002999 RID: 10649 RVA: 0x000E35E0 File Offset: 0x000E17E0
		// (set) Token: 0x0600299A RID: 10650 RVA: 0x000E35E8 File Offset: 0x000E17E8
		[XmlElement("restriction", typeof(XmlSchemaSimpleTypeRestriction))]
		[XmlElement("list", typeof(XmlSchemaSimpleTypeList))]
		[XmlElement("union", typeof(XmlSchemaSimpleTypeUnion))]
		public XmlSchemaSimpleTypeContent Content
		{
			get
			{
				return this.content;
			}
			set
			{
				this.content = value;
			}
		}

		// Token: 0x17000916 RID: 2326
		// (get) Token: 0x0600299B RID: 10651 RVA: 0x000E35F1 File Offset: 0x000E17F1
		internal override XmlQualifiedName DerivedFrom
		{
			get
			{
				if (this.content == null)
				{
					return XmlQualifiedName.Empty;
				}
				if (this.content is XmlSchemaSimpleTypeRestriction)
				{
					return ((XmlSchemaSimpleTypeRestriction)this.content).BaseTypeName;
				}
				return XmlQualifiedName.Empty;
			}
		}

		// Token: 0x0600299C RID: 10652 RVA: 0x000E3624 File Offset: 0x000E1824
		internal override XmlSchemaObject Clone()
		{
			XmlSchemaSimpleType xmlSchemaSimpleType = (XmlSchemaSimpleType)base.MemberwiseClone();
			if (this.content != null)
			{
				xmlSchemaSimpleType.Content = (XmlSchemaSimpleTypeContent)this.content.Clone();
			}
			return xmlSchemaSimpleType;
		}

		// Token: 0x04001CDD RID: 7389
		private XmlSchemaSimpleTypeContent content;
	}
}
