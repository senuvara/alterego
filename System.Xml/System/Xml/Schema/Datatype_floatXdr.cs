﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003BA RID: 954
	internal class Datatype_floatXdr : Datatype_float
	{
		// Token: 0x06002368 RID: 9064 RVA: 0x000BEB98 File Offset: 0x000BCD98
		public override object ParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr)
		{
			float num;
			try
			{
				num = XmlConvert.ToSingle(s);
			}
			catch (Exception innerException)
			{
				throw new XmlSchemaException(Res.GetString("The value '{0}' is invalid according to its data type.", new object[]
				{
					s
				}), innerException);
			}
			if (float.IsInfinity(num) || float.IsNaN(num))
			{
				throw new XmlSchemaException("The value '{0}' is invalid according to its data type.", s);
			}
			return num;
		}

		// Token: 0x06002369 RID: 9065 RVA: 0x000BEBFC File Offset: 0x000BCDFC
		public Datatype_floatXdr()
		{
		}
	}
}
