﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x02000366 RID: 870
	internal class SymbolsDictionary
	{
		// Token: 0x0600215D RID: 8541 RVA: 0x000B97DE File Offset: 0x000B79DE
		public SymbolsDictionary()
		{
			this.names = new Hashtable();
			this.particles = new ArrayList();
		}

		// Token: 0x1700068A RID: 1674
		// (get) Token: 0x0600215E RID: 8542 RVA: 0x000B9803 File Offset: 0x000B7A03
		public int Count
		{
			get
			{
				return this.last + 1;
			}
		}

		// Token: 0x1700068B RID: 1675
		// (get) Token: 0x0600215F RID: 8543 RVA: 0x000B980D File Offset: 0x000B7A0D
		public int CountOfNames
		{
			get
			{
				return this.names.Count;
			}
		}

		// Token: 0x1700068C RID: 1676
		// (get) Token: 0x06002160 RID: 8544 RVA: 0x000B981A File Offset: 0x000B7A1A
		// (set) Token: 0x06002161 RID: 8545 RVA: 0x000B9822 File Offset: 0x000B7A22
		public bool IsUpaEnforced
		{
			get
			{
				return this.isUpaEnforced;
			}
			set
			{
				this.isUpaEnforced = value;
			}
		}

		// Token: 0x06002162 RID: 8546 RVA: 0x000B982C File Offset: 0x000B7A2C
		public int AddName(XmlQualifiedName name, object particle)
		{
			object obj = this.names[name];
			if (obj != null)
			{
				int num = (int)obj;
				if (this.particles[num] != particle)
				{
					this.isUpaEnforced = false;
				}
				return num;
			}
			this.names.Add(name, this.last);
			this.particles.Add(particle);
			int num2 = this.last;
			this.last = num2 + 1;
			return num2;
		}

		// Token: 0x06002163 RID: 8547 RVA: 0x000B98A0 File Offset: 0x000B7AA0
		public void AddNamespaceList(NamespaceList list, object particle, bool allowLocal)
		{
			switch (list.Type)
			{
			case NamespaceList.ListType.Any:
				this.particleLast = particle;
				return;
			case NamespaceList.ListType.Other:
				this.AddWildcard(list.Excluded, null);
				if (!allowLocal)
				{
					this.AddWildcard(string.Empty, null);
					return;
				}
				break;
			case NamespaceList.ListType.Set:
				foreach (object obj in list.Enumerate)
				{
					string wildcard = (string)obj;
					this.AddWildcard(wildcard, particle);
				}
				break;
			default:
				return;
			}
		}

		// Token: 0x06002164 RID: 8548 RVA: 0x000B993C File Offset: 0x000B7B3C
		private void AddWildcard(string wildcard, object particle)
		{
			if (this.wildcards == null)
			{
				this.wildcards = new Hashtable();
			}
			object obj = this.wildcards[wildcard];
			if (obj == null)
			{
				this.wildcards.Add(wildcard, this.last);
				this.particles.Add(particle);
				this.last++;
				return;
			}
			if (particle != null)
			{
				this.particles[(int)obj] = particle;
			}
		}

		// Token: 0x06002165 RID: 8549 RVA: 0x000B99B4 File Offset: 0x000B7BB4
		public ICollection GetNamespaceListSymbols(NamespaceList list)
		{
			ArrayList arrayList = new ArrayList();
			foreach (object obj in this.names.Keys)
			{
				XmlQualifiedName xmlQualifiedName = (XmlQualifiedName)obj;
				if (xmlQualifiedName != XmlQualifiedName.Empty && list.Allows(xmlQualifiedName))
				{
					arrayList.Add(this.names[xmlQualifiedName]);
				}
			}
			if (this.wildcards != null)
			{
				foreach (object obj2 in this.wildcards.Keys)
				{
					string text = (string)obj2;
					if (list.Allows(text))
					{
						arrayList.Add(this.wildcards[text]);
					}
				}
			}
			if (list.Type == NamespaceList.ListType.Any || list.Type == NamespaceList.ListType.Other)
			{
				arrayList.Add(this.last);
			}
			return arrayList;
		}

		// Token: 0x1700068D RID: 1677
		public int this[XmlQualifiedName name]
		{
			get
			{
				object obj = this.names[name];
				if (obj != null)
				{
					return (int)obj;
				}
				if (this.wildcards != null)
				{
					obj = this.wildcards[name.Namespace];
					if (obj != null)
					{
						return (int)obj;
					}
				}
				return this.last;
			}
		}

		// Token: 0x06002167 RID: 8551 RVA: 0x000B9B1A File Offset: 0x000B7D1A
		public bool Exists(XmlQualifiedName name)
		{
			return this.names[name] != null;
		}

		// Token: 0x06002168 RID: 8552 RVA: 0x000B9B2D File Offset: 0x000B7D2D
		public object GetParticle(int symbol)
		{
			if (symbol != this.last)
			{
				return this.particles[symbol];
			}
			return this.particleLast;
		}

		// Token: 0x06002169 RID: 8553 RVA: 0x000B9B4C File Offset: 0x000B7D4C
		public string NameOf(int symbol)
		{
			foreach (object obj in this.names)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				if ((int)dictionaryEntry.Value == symbol)
				{
					return ((XmlQualifiedName)dictionaryEntry.Key).ToString();
				}
			}
			if (this.wildcards != null)
			{
				foreach (object obj2 in this.wildcards)
				{
					DictionaryEntry dictionaryEntry2 = (DictionaryEntry)obj2;
					if ((int)dictionaryEntry2.Value == symbol)
					{
						return (string)dictionaryEntry2.Key + ":*";
					}
				}
			}
			return "##other:*";
		}

		// Token: 0x040017E3 RID: 6115
		private int last;

		// Token: 0x040017E4 RID: 6116
		private Hashtable names;

		// Token: 0x040017E5 RID: 6117
		private Hashtable wildcards;

		// Token: 0x040017E6 RID: 6118
		private ArrayList particles;

		// Token: 0x040017E7 RID: 6119
		private object particleLast;

		// Token: 0x040017E8 RID: 6120
		private bool isUpaEnforced = true;
	}
}
