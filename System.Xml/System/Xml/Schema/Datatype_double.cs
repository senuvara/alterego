﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200038A RID: 906
	internal class Datatype_double : Datatype_anySimpleType
	{
		// Token: 0x06002272 RID: 8818 RVA: 0x000BD9EA File Offset: 0x000BBBEA
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlNumeric2Converter.Create(schemaType);
		}

		// Token: 0x170006E4 RID: 1764
		// (get) Token: 0x06002273 RID: 8819 RVA: 0x000BD9F2 File Offset: 0x000BBBF2
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.numeric2FacetsChecker;
			}
		}

		// Token: 0x170006E5 RID: 1765
		// (get) Token: 0x06002274 RID: 8820 RVA: 0x00005EE0 File Offset: 0x000040E0
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Double;
			}
		}

		// Token: 0x170006E6 RID: 1766
		// (get) Token: 0x06002275 RID: 8821 RVA: 0x000BDA9A File Offset: 0x000BBC9A
		public override Type ValueType
		{
			get
			{
				return Datatype_double.atomicValueType;
			}
		}

		// Token: 0x170006E7 RID: 1767
		// (get) Token: 0x06002276 RID: 8822 RVA: 0x000BDAA1 File Offset: 0x000BBCA1
		internal override Type ListValueType
		{
			get
			{
				return Datatype_double.listValueType;
			}
		}

		// Token: 0x170006E8 RID: 1768
		// (get) Token: 0x06002277 RID: 8823 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x170006E9 RID: 1769
		// (get) Token: 0x06002278 RID: 8824 RVA: 0x000BDA0B File Offset: 0x000BBC0B
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace | RestrictionFlags.MaxInclusive | RestrictionFlags.MaxExclusive | RestrictionFlags.MinInclusive | RestrictionFlags.MinExclusive;
			}
		}

		// Token: 0x06002279 RID: 8825 RVA: 0x000BDAA8 File Offset: 0x000BBCA8
		internal override int Compare(object value1, object value2)
		{
			return ((double)value1).CompareTo(value2);
		}

		// Token: 0x0600227A RID: 8826 RVA: 0x000BDAC4 File Offset: 0x000BBCC4
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.numeric2FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				double num;
				ex = XmlConvert.TryToDouble(s, out num);
				if (ex == null)
				{
					ex = DatatypeImplementation.numeric2FacetsChecker.CheckValueFacets(num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x0600227B RID: 8827 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_double()
		{
		}

		// Token: 0x0600227C RID: 8828 RVA: 0x000BDB0E File Offset: 0x000BBD0E
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_double()
		{
		}

		// Token: 0x040018AA RID: 6314
		private static readonly Type atomicValueType = typeof(double);

		// Token: 0x040018AB RID: 6315
		private static readonly Type listValueType = typeof(double[]);
	}
}
