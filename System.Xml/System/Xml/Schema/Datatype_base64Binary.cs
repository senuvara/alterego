﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200039D RID: 925
	internal class Datatype_base64Binary : Datatype_anySimpleType
	{
		// Token: 0x060022C3 RID: 8899 RVA: 0x000BDBF8 File Offset: 0x000BBDF8
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlMiscConverter.Create(schemaType);
		}

		// Token: 0x1700070B RID: 1803
		// (get) Token: 0x060022C4 RID: 8900 RVA: 0x000BDF7A File Offset: 0x000BC17A
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.binaryFacetsChecker;
			}
		}

		// Token: 0x1700070C RID: 1804
		// (get) Token: 0x060022C5 RID: 8901 RVA: 0x000BE030 File Offset: 0x000BC230
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Base64Binary;
			}
		}

		// Token: 0x1700070D RID: 1805
		// (get) Token: 0x060022C6 RID: 8902 RVA: 0x000BE034 File Offset: 0x000BC234
		public override Type ValueType
		{
			get
			{
				return Datatype_base64Binary.atomicValueType;
			}
		}

		// Token: 0x1700070E RID: 1806
		// (get) Token: 0x060022C7 RID: 8903 RVA: 0x000BE03B File Offset: 0x000BC23B
		internal override Type ListValueType
		{
			get
			{
				return Datatype_base64Binary.listValueType;
			}
		}

		// Token: 0x1700070F RID: 1807
		// (get) Token: 0x060022C8 RID: 8904 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x17000710 RID: 1808
		// (get) Token: 0x060022C9 RID: 8905 RVA: 0x000BD2C1 File Offset: 0x000BB4C1
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Length | RestrictionFlags.MinLength | RestrictionFlags.MaxLength | RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace;
			}
		}

		// Token: 0x060022CA RID: 8906 RVA: 0x000BDF93 File Offset: 0x000BC193
		internal override int Compare(object value1, object value2)
		{
			return base.Compare((byte[])value1, (byte[])value2);
		}

		// Token: 0x060022CB RID: 8907 RVA: 0x000BE044 File Offset: 0x000BC244
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.binaryFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				byte[] array = null;
				try
				{
					array = Convert.FromBase64String(s);
				}
				catch (ArgumentException ex)
				{
					return ex;
				}
				catch (FormatException ex)
				{
					return ex;
				}
				ex = DatatypeImplementation.binaryFacetsChecker.CheckValueFacets(array, this);
				if (ex == null)
				{
					typedValue = array;
					return null;
				}
			}
			return ex;
		}

		// Token: 0x060022CC RID: 8908 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_base64Binary()
		{
		}

		// Token: 0x060022CD RID: 8909 RVA: 0x000BE0AC File Offset: 0x000BC2AC
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_base64Binary()
		{
		}

		// Token: 0x040018B6 RID: 6326
		private static readonly Type atomicValueType = typeof(byte[]);

		// Token: 0x040018B7 RID: 6327
		private static readonly Type listValueType = typeof(byte[][]);
	}
}
