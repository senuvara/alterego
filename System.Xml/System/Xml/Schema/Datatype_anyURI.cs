﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200039E RID: 926
	internal class Datatype_anyURI : Datatype_anySimpleType
	{
		// Token: 0x060022CE RID: 8910 RVA: 0x000BDBF8 File Offset: 0x000BBDF8
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlMiscConverter.Create(schemaType);
		}

		// Token: 0x17000711 RID: 1809
		// (get) Token: 0x060022CF RID: 8911 RVA: 0x000BD918 File Offset: 0x000BBB18
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.stringFacetsChecker;
			}
		}

		// Token: 0x17000712 RID: 1810
		// (get) Token: 0x060022D0 RID: 8912 RVA: 0x000BE0CC File Offset: 0x000BC2CC
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.AnyUri;
			}
		}

		// Token: 0x17000713 RID: 1811
		// (get) Token: 0x060022D1 RID: 8913 RVA: 0x000BE0D0 File Offset: 0x000BC2D0
		public override Type ValueType
		{
			get
			{
				return Datatype_anyURI.atomicValueType;
			}
		}

		// Token: 0x17000714 RID: 1812
		// (get) Token: 0x060022D2 RID: 8914 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool HasValueFacets
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000715 RID: 1813
		// (get) Token: 0x060022D3 RID: 8915 RVA: 0x000BE0D7 File Offset: 0x000BC2D7
		internal override Type ListValueType
		{
			get
			{
				return Datatype_anyURI.listValueType;
			}
		}

		// Token: 0x17000716 RID: 1814
		// (get) Token: 0x060022D4 RID: 8916 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x17000717 RID: 1815
		// (get) Token: 0x060022D5 RID: 8917 RVA: 0x000BD2C1 File Offset: 0x000BB4C1
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Length | RestrictionFlags.MinLength | RestrictionFlags.MaxLength | RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace;
			}
		}

		// Token: 0x060022D6 RID: 8918 RVA: 0x000BE0DE File Offset: 0x000BC2DE
		internal override int Compare(object value1, object value2)
		{
			if (!((Uri)value1).Equals((Uri)value2))
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x060022D7 RID: 8919 RVA: 0x000BE0F8 File Offset: 0x000BC2F8
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.stringFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				Uri uri;
				ex = XmlConvert.TryToUri(s, out uri);
				if (ex == null)
				{
					string originalString = uri.OriginalString;
					ex = ((StringFacetsChecker)DatatypeImplementation.stringFacetsChecker).CheckValueFacets(originalString, this, false);
					if (ex == null)
					{
						typedValue = uri;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x060022D8 RID: 8920 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_anyURI()
		{
		}

		// Token: 0x060022D9 RID: 8921 RVA: 0x000BE14A File Offset: 0x000BC34A
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_anyURI()
		{
		}

		// Token: 0x040018B8 RID: 6328
		private static readonly Type atomicValueType = typeof(Uri);

		// Token: 0x040018B9 RID: 6329
		private static readonly Type listValueType = typeof(Uri[]);
	}
}
