﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="notation" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). An XML Schema <see langword="notation" /> declaration is a reconstruction of <see langword="XML 1.0 NOTATION" /> declarations. The purpose of notations is to describe the format of non-XML data within an XML document.</summary>
	// Token: 0x0200043A RID: 1082
	public class XmlSchemaNotation : XmlSchemaAnnotated
	{
		/// <summary>Gets or sets the name of the notation.</summary>
		/// <returns>The name of the notation.</returns>
		// Token: 0x170008D0 RID: 2256
		// (get) Token: 0x060028D1 RID: 10449 RVA: 0x000E0A5A File Offset: 0x000DEC5A
		// (set) Token: 0x060028D2 RID: 10450 RVA: 0x000E0A62 File Offset: 0x000DEC62
		[XmlAttribute("name")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets the <see langword="public" /> identifier.</summary>
		/// <returns>The <see langword="public" /> identifier. The value must be a valid Uniform Resource Identifier (URI).</returns>
		// Token: 0x170008D1 RID: 2257
		// (get) Token: 0x060028D3 RID: 10451 RVA: 0x000E0A6B File Offset: 0x000DEC6B
		// (set) Token: 0x060028D4 RID: 10452 RVA: 0x000E0A73 File Offset: 0x000DEC73
		[XmlAttribute("public")]
		public string Public
		{
			get
			{
				return this.publicId;
			}
			set
			{
				this.publicId = value;
			}
		}

		/// <summary>Gets or sets the <see langword="system" /> identifier.</summary>
		/// <returns>The <see langword="system" /> identifier. The value must be a valid URI.</returns>
		// Token: 0x170008D2 RID: 2258
		// (get) Token: 0x060028D5 RID: 10453 RVA: 0x000E0A7C File Offset: 0x000DEC7C
		// (set) Token: 0x060028D6 RID: 10454 RVA: 0x000E0A84 File Offset: 0x000DEC84
		[XmlAttribute("system")]
		public string System
		{
			get
			{
				return this.systemId;
			}
			set
			{
				this.systemId = value;
			}
		}

		// Token: 0x170008D3 RID: 2259
		// (get) Token: 0x060028D7 RID: 10455 RVA: 0x000E0A8D File Offset: 0x000DEC8D
		// (set) Token: 0x060028D8 RID: 10456 RVA: 0x000E0A95 File Offset: 0x000DEC95
		[XmlIgnore]
		internal XmlQualifiedName QualifiedName
		{
			get
			{
				return this.qname;
			}
			set
			{
				this.qname = value;
			}
		}

		// Token: 0x170008D4 RID: 2260
		// (get) Token: 0x060028D9 RID: 10457 RVA: 0x000E0A9E File Offset: 0x000DEC9E
		// (set) Token: 0x060028DA RID: 10458 RVA: 0x000E0AA6 File Offset: 0x000DECA6
		[XmlIgnore]
		internal override string NameAttribute
		{
			get
			{
				return this.Name;
			}
			set
			{
				this.Name = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaNotation" /> class.</summary>
		// Token: 0x060028DB RID: 10459 RVA: 0x000E0AAF File Offset: 0x000DECAF
		public XmlSchemaNotation()
		{
		}

		// Token: 0x04001C95 RID: 7317
		private string name;

		// Token: 0x04001C96 RID: 7318
		private string publicId;

		// Token: 0x04001C97 RID: 7319
		private string systemId;

		// Token: 0x04001C98 RID: 7320
		private XmlQualifiedName qname = XmlQualifiedName.Empty;
	}
}
