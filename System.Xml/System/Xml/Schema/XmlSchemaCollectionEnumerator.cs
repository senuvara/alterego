﻿using System;
using System.Collections;
using Unity;

namespace System.Xml.Schema
{
	/// <summary>Supports a simple iteration over a collection. This class cannot be inherited. </summary>
	// Token: 0x0200040F RID: 1039
	public sealed class XmlSchemaCollectionEnumerator : IEnumerator
	{
		// Token: 0x06002796 RID: 10134 RVA: 0x000DEE02 File Offset: 0x000DD002
		internal XmlSchemaCollectionEnumerator(Hashtable collection)
		{
			this.enumerator = collection.GetEnumerator();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.Schema.XmlSchemaCollectionEnumerator.System#Collections#IEnumerator#Reset" />.</summary>
		// Token: 0x06002797 RID: 10135 RVA: 0x000DEE16 File Offset: 0x000DD016
		void IEnumerator.Reset()
		{
			this.enumerator.Reset();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.Schema.XmlSchemaCollectionEnumerator.MoveNext" />.</summary>
		/// <returns>Returns the next node.</returns>
		// Token: 0x06002798 RID: 10136 RVA: 0x000DEE23 File Offset: 0x000DD023
		bool IEnumerator.MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		/// <summary>Advances the enumerator to the next schema in the collection.</summary>
		/// <returns>
		///     <see langword="true" /> if the move was successful; <see langword="false" /> if the enumerator has passed the end of the collection.</returns>
		// Token: 0x06002799 RID: 10137 RVA: 0x000DEE23 File Offset: 0x000DD023
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		/// <summary>For a description of this member, see <see cref="P:System.Xml.Schema.XmlSchemaCollectionEnumerator.Current" />.</summary>
		/// <returns>Returns the current node.</returns>
		// Token: 0x17000853 RID: 2131
		// (get) Token: 0x0600279A RID: 10138 RVA: 0x000DEE30 File Offset: 0x000DD030
		object IEnumerator.Current
		{
			get
			{
				return this.Current;
			}
		}

		/// <summary>Gets the current <see cref="T:System.Xml.Schema.XmlSchema" /> in the collection.</summary>
		/// <returns>The current <see langword="XmlSchema" /> in the collection.</returns>
		// Token: 0x17000854 RID: 2132
		// (get) Token: 0x0600279B RID: 10139 RVA: 0x000DEE38 File Offset: 0x000DD038
		public XmlSchema Current
		{
			get
			{
				XmlSchemaCollectionNode xmlSchemaCollectionNode = (XmlSchemaCollectionNode)this.enumerator.Value;
				if (xmlSchemaCollectionNode != null)
				{
					return xmlSchemaCollectionNode.Schema;
				}
				return null;
			}
		}

		// Token: 0x17000855 RID: 2133
		// (get) Token: 0x0600279C RID: 10140 RVA: 0x000DEE61 File Offset: 0x000DD061
		internal XmlSchemaCollectionNode CurrentNode
		{
			get
			{
				return (XmlSchemaCollectionNode)this.enumerator.Value;
			}
		}

		// Token: 0x0600279D RID: 10141 RVA: 0x00072668 File Offset: 0x00070868
		internal XmlSchemaCollectionEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001C0E RID: 7182
		private IDictionaryEnumerator enumerator;
	}
}
