﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000390 RID: 912
	internal class Datatype_dateTimeNoTimeZone : Datatype_dateTimeBase
	{
		// Token: 0x060022A5 RID: 8869 RVA: 0x000BDEF0 File Offset: 0x000BC0F0
		internal Datatype_dateTimeNoTimeZone() : base(XsdDateTimeFlags.XdrDateTimeNoTz)
		{
		}
	}
}
