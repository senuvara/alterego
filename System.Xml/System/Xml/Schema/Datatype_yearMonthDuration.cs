﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200038D RID: 909
	internal class Datatype_yearMonthDuration : Datatype_duration
	{
		// Token: 0x06002293 RID: 8851 RVA: 0x000BDCBC File Offset: 0x000BBEBC
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			if (s == null || s.Length == 0)
			{
				return new XmlSchemaException("The attribute value cannot be empty.", string.Empty);
			}
			Exception ex = DatatypeImplementation.durationFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				XsdDuration xsdDuration;
				ex = XsdDuration.TryParse(s, XsdDuration.DurationType.YearMonthDuration, out xsdDuration);
				if (ex == null)
				{
					TimeSpan timeSpan;
					ex = xsdDuration.TryToTimeSpan(XsdDuration.DurationType.YearMonthDuration, out timeSpan);
					if (ex == null)
					{
						ex = DatatypeImplementation.durationFacetsChecker.CheckValueFacets(timeSpan, this);
						if (ex == null)
						{
							typedValue = timeSpan;
							return null;
						}
					}
				}
			}
			return ex;
		}

		// Token: 0x170006F6 RID: 1782
		// (get) Token: 0x06002294 RID: 8852 RVA: 0x000BDD30 File Offset: 0x000BBF30
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.YearMonthDuration;
			}
		}

		// Token: 0x06002295 RID: 8853 RVA: 0x000BDD34 File Offset: 0x000BBF34
		public Datatype_yearMonthDuration()
		{
		}
	}
}
