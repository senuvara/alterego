﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200038F RID: 911
	internal class Datatype_dateTimeBase : Datatype_anySimpleType
	{
		// Token: 0x06002299 RID: 8857 RVA: 0x000BDDB4 File Offset: 0x000BBFB4
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlDateTimeConverter.Create(schemaType);
		}

		// Token: 0x170006F8 RID: 1784
		// (get) Token: 0x0600229A RID: 8858 RVA: 0x000BDDBC File Offset: 0x000BBFBC
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.dateTimeFacetsChecker;
			}
		}

		// Token: 0x170006F9 RID: 1785
		// (get) Token: 0x0600229B RID: 8859 RVA: 0x000BDDC3 File Offset: 0x000BBFC3
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.DateTime;
			}
		}

		// Token: 0x0600229C RID: 8860 RVA: 0x000BD900 File Offset: 0x000BBB00
		internal Datatype_dateTimeBase()
		{
		}

		// Token: 0x0600229D RID: 8861 RVA: 0x000BDDC7 File Offset: 0x000BBFC7
		internal Datatype_dateTimeBase(XsdDateTimeFlags dateTimeFlags)
		{
			this.dateTimeFlags = dateTimeFlags;
		}

		// Token: 0x170006FA RID: 1786
		// (get) Token: 0x0600229E RID: 8862 RVA: 0x000BDDD6 File Offset: 0x000BBFD6
		public override Type ValueType
		{
			get
			{
				return Datatype_dateTimeBase.atomicValueType;
			}
		}

		// Token: 0x170006FB RID: 1787
		// (get) Token: 0x0600229F RID: 8863 RVA: 0x000BDDDD File Offset: 0x000BBFDD
		internal override Type ListValueType
		{
			get
			{
				return Datatype_dateTimeBase.listValueType;
			}
		}

		// Token: 0x170006FC RID: 1788
		// (get) Token: 0x060022A0 RID: 8864 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x170006FD RID: 1789
		// (get) Token: 0x060022A1 RID: 8865 RVA: 0x000BDA0B File Offset: 0x000BBC0B
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace | RestrictionFlags.MaxInclusive | RestrictionFlags.MaxExclusive | RestrictionFlags.MinInclusive | RestrictionFlags.MinExclusive;
			}
		}

		// Token: 0x060022A2 RID: 8866 RVA: 0x000BDDE4 File Offset: 0x000BBFE4
		internal override int Compare(object value1, object value2)
		{
			DateTime dateTime = (DateTime)value1;
			DateTime value3 = (DateTime)value2;
			if (dateTime.Kind == DateTimeKind.Unspecified || value3.Kind == DateTimeKind.Unspecified)
			{
				return dateTime.CompareTo(value3);
			}
			return dateTime.ToUniversalTime().CompareTo(value3.ToUniversalTime());
		}

		// Token: 0x060022A3 RID: 8867 RVA: 0x000BDE30 File Offset: 0x000BC030
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.dateTimeFacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				XsdDateTime xdt;
				if (!XsdDateTime.TryParse(s, this.dateTimeFlags, out xdt))
				{
					ex = new FormatException(Res.GetString("The string '{0}' is not a valid {1} value.", new object[]
					{
						s,
						this.dateTimeFlags.ToString()
					}));
				}
				else
				{
					DateTime dateTime = DateTime.MinValue;
					try
					{
						dateTime = xdt;
					}
					catch (ArgumentException ex)
					{
						return ex;
					}
					ex = DatatypeImplementation.dateTimeFacetsChecker.CheckValueFacets(dateTime, this);
					if (ex == null)
					{
						typedValue = dateTime;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x060022A4 RID: 8868 RVA: 0x000BDED0 File Offset: 0x000BC0D0
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_dateTimeBase()
		{
		}

		// Token: 0x040018B1 RID: 6321
		private static readonly Type atomicValueType = typeof(DateTime);

		// Token: 0x040018B2 RID: 6322
		private static readonly Type listValueType = typeof(DateTime[]);

		// Token: 0x040018B3 RID: 6323
		private XsdDateTimeFlags dateTimeFlags;
	}
}
