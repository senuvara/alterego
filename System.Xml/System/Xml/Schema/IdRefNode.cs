﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200045B RID: 1115
	internal class IdRefNode
	{
		// Token: 0x060029F3 RID: 10739 RVA: 0x000E3B59 File Offset: 0x000E1D59
		internal IdRefNode(IdRefNode next, string id, int lineNo, int linePos)
		{
			this.Id = id;
			this.LineNo = lineNo;
			this.LinePos = linePos;
			this.Next = next;
		}

		// Token: 0x04001D0E RID: 7438
		internal string Id;

		// Token: 0x04001D0F RID: 7439
		internal int LineNo;

		// Token: 0x04001D10 RID: 7440
		internal int LinePos;

		// Token: 0x04001D11 RID: 7441
		internal IdRefNode Next;
	}
}
