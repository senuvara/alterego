﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the post-schema-validation infoset of a validated XML node.</summary>
	// Token: 0x02000439 RID: 1081
	public class XmlSchemaInfo : IXmlSchemaInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaInfo" /> class.</summary>
		// Token: 0x060028BB RID: 10427 RVA: 0x000E08E4 File Offset: 0x000DEAE4
		public XmlSchemaInfo()
		{
			this.Clear();
		}

		// Token: 0x060028BC RID: 10428 RVA: 0x000E08F2 File Offset: 0x000DEAF2
		internal XmlSchemaInfo(XmlSchemaValidity validity) : this()
		{
			this.validity = validity;
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaValidity" /> value of this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaValidity" /> value.</returns>
		// Token: 0x170008C5 RID: 2245
		// (get) Token: 0x060028BD RID: 10429 RVA: 0x000E0901 File Offset: 0x000DEB01
		// (set) Token: 0x060028BE RID: 10430 RVA: 0x000E0909 File Offset: 0x000DEB09
		public XmlSchemaValidity Validity
		{
			get
			{
				return this.validity;
			}
			set
			{
				this.validity = value;
			}
		}

		/// <summary>Gets or sets a value indicating if this validated XML node was set as the result of a default being applied during XML Schema Definition Language (XSD) schema validation.</summary>
		/// <returns>A <see langword="bool" /> value.</returns>
		// Token: 0x170008C6 RID: 2246
		// (get) Token: 0x060028BF RID: 10431 RVA: 0x000E0912 File Offset: 0x000DEB12
		// (set) Token: 0x060028C0 RID: 10432 RVA: 0x000E091A File Offset: 0x000DEB1A
		public bool IsDefault
		{
			get
			{
				return this.isDefault;
			}
			set
			{
				this.isDefault = value;
			}
		}

		/// <summary>Gets or sets a value indicating if the value for this validated XML node is nil.</summary>
		/// <returns>A <see langword="bool" /> value.</returns>
		// Token: 0x170008C7 RID: 2247
		// (get) Token: 0x060028C1 RID: 10433 RVA: 0x000E0923 File Offset: 0x000DEB23
		// (set) Token: 0x060028C2 RID: 10434 RVA: 0x000E092B File Offset: 0x000DEB2B
		public bool IsNil
		{
			get
			{
				return this.isNil;
			}
			set
			{
				this.isNil = value;
			}
		}

		/// <summary>Gets or sets the dynamic schema type for this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaSimpleType" /> object.</returns>
		// Token: 0x170008C8 RID: 2248
		// (get) Token: 0x060028C3 RID: 10435 RVA: 0x000E0934 File Offset: 0x000DEB34
		// (set) Token: 0x060028C4 RID: 10436 RVA: 0x000E093C File Offset: 0x000DEB3C
		public XmlSchemaSimpleType MemberType
		{
			get
			{
				return this.memberType;
			}
			set
			{
				this.memberType = value;
			}
		}

		/// <summary>Gets or sets the static XML Schema Definition Language (XSD) schema type of this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaType" /> object.</returns>
		// Token: 0x170008C9 RID: 2249
		// (get) Token: 0x060028C5 RID: 10437 RVA: 0x000E0945 File Offset: 0x000DEB45
		// (set) Token: 0x060028C6 RID: 10438 RVA: 0x000E094D File Offset: 0x000DEB4D
		public XmlSchemaType SchemaType
		{
			get
			{
				return this.schemaType;
			}
			set
			{
				this.schemaType = value;
				if (this.schemaType != null)
				{
					this.contentType = this.schemaType.SchemaContentType;
					return;
				}
				this.contentType = XmlSchemaContentType.Empty;
			}
		}

		/// <summary>Gets or sets the compiled <see cref="T:System.Xml.Schema.XmlSchemaElement" /> object that corresponds to this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaElement" /> object.</returns>
		// Token: 0x170008CA RID: 2250
		// (get) Token: 0x060028C7 RID: 10439 RVA: 0x000E0977 File Offset: 0x000DEB77
		// (set) Token: 0x060028C8 RID: 10440 RVA: 0x000E097F File Offset: 0x000DEB7F
		public XmlSchemaElement SchemaElement
		{
			get
			{
				return this.schemaElement;
			}
			set
			{
				this.schemaElement = value;
				if (value != null)
				{
					this.schemaAttribute = null;
				}
			}
		}

		/// <summary>Gets or sets the compiled <see cref="T:System.Xml.Schema.XmlSchemaAttribute" /> object that corresponds to this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaAttribute" /> object.</returns>
		// Token: 0x170008CB RID: 2251
		// (get) Token: 0x060028C9 RID: 10441 RVA: 0x000E0992 File Offset: 0x000DEB92
		// (set) Token: 0x060028CA RID: 10442 RVA: 0x000E099A File Offset: 0x000DEB9A
		public XmlSchemaAttribute SchemaAttribute
		{
			get
			{
				return this.schemaAttribute;
			}
			set
			{
				this.schemaAttribute = value;
				if (value != null)
				{
					this.schemaElement = null;
				}
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaContentType" /> object that corresponds to the content type of this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaContentType" /> object.</returns>
		// Token: 0x170008CC RID: 2252
		// (get) Token: 0x060028CB RID: 10443 RVA: 0x000E09AD File Offset: 0x000DEBAD
		// (set) Token: 0x060028CC RID: 10444 RVA: 0x000E09B5 File Offset: 0x000DEBB5
		public XmlSchemaContentType ContentType
		{
			get
			{
				return this.contentType;
			}
			set
			{
				this.contentType = value;
			}
		}

		// Token: 0x170008CD RID: 2253
		// (get) Token: 0x060028CD RID: 10445 RVA: 0x000E09BE File Offset: 0x000DEBBE
		internal XmlSchemaType XmlType
		{
			get
			{
				if (this.memberType != null)
				{
					return this.memberType;
				}
				return this.schemaType;
			}
		}

		// Token: 0x170008CE RID: 2254
		// (get) Token: 0x060028CE RID: 10446 RVA: 0x000E09D5 File Offset: 0x000DEBD5
		internal bool HasDefaultValue
		{
			get
			{
				return this.schemaElement != null && this.schemaElement.ElementDecl.DefaultValueTyped != null;
			}
		}

		// Token: 0x170008CF RID: 2255
		// (get) Token: 0x060028CF RID: 10447 RVA: 0x000E09F4 File Offset: 0x000DEBF4
		internal bool IsUnionType
		{
			get
			{
				return this.schemaType != null && this.schemaType.Datatype != null && this.schemaType.Datatype.Variety == XmlSchemaDatatypeVariety.Union;
			}
		}

		// Token: 0x060028D0 RID: 10448 RVA: 0x000E0A20 File Offset: 0x000DEC20
		internal void Clear()
		{
			this.isNil = false;
			this.isDefault = false;
			this.schemaType = null;
			this.schemaElement = null;
			this.schemaAttribute = null;
			this.memberType = null;
			this.validity = XmlSchemaValidity.NotKnown;
			this.contentType = XmlSchemaContentType.Empty;
		}

		// Token: 0x04001C8D RID: 7309
		private bool isDefault;

		// Token: 0x04001C8E RID: 7310
		private bool isNil;

		// Token: 0x04001C8F RID: 7311
		private XmlSchemaElement schemaElement;

		// Token: 0x04001C90 RID: 7312
		private XmlSchemaAttribute schemaAttribute;

		// Token: 0x04001C91 RID: 7313
		private XmlSchemaType schemaType;

		// Token: 0x04001C92 RID: 7314
		private XmlSchemaSimpleType memberType;

		// Token: 0x04001C93 RID: 7315
		private XmlSchemaValidity validity;

		// Token: 0x04001C94 RID: 7316
		private XmlSchemaContentType contentType;
	}
}
