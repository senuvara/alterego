﻿using System;

namespace System.Xml.Schema
{
	/// <summary>An abstract class for schema content.</summary>
	// Token: 0x02000415 RID: 1045
	public abstract class XmlSchemaContent : XmlSchemaAnnotated
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaContent" /> class.</summary>
		// Token: 0x060027E4 RID: 10212 RVA: 0x000DE4D5 File Offset: 0x000DC6D5
		protected XmlSchemaContent()
		{
		}
	}
}
