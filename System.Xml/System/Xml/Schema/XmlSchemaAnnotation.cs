﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the World Wide Web Consortium (W3C) <see langword="annotation" /> element.</summary>
	// Token: 0x02000405 RID: 1029
	public class XmlSchemaAnnotation : XmlSchemaObject
	{
		/// <summary>Gets or sets the string id.</summary>
		/// <returns>The string id. The default is <see langword="String.Empty" />.Optional.</returns>
		// Token: 0x1700081C RID: 2076
		// (get) Token: 0x06002711 RID: 10001 RVA: 0x000DE19E File Offset: 0x000DC39E
		// (set) Token: 0x06002712 RID: 10002 RVA: 0x000DE1A6 File Offset: 0x000DC3A6
		[XmlAttribute("id", DataType = "ID")]
		public string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}

		/// <summary>Gets the <see langword="Items" /> collection that is used to store the <see langword="appinfo" /> and <see langword="documentation" /> child elements.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaObjectCollection" /> of <see langword="appinfo" /> and <see langword="documentation" /> child elements.</returns>
		// Token: 0x1700081D RID: 2077
		// (get) Token: 0x06002713 RID: 10003 RVA: 0x000DE1AF File Offset: 0x000DC3AF
		[XmlElement("appinfo", typeof(XmlSchemaAppInfo))]
		[XmlElement("documentation", typeof(XmlSchemaDocumentation))]
		public XmlSchemaObjectCollection Items
		{
			get
			{
				return this.items;
			}
		}

		/// <summary>Gets or sets the qualified attributes that do not belong to the schema's target namespace.</summary>
		/// <returns>An array of <see cref="T:System.Xml.XmlAttribute" /> objects that do not belong to the schema's target namespace.</returns>
		// Token: 0x1700081E RID: 2078
		// (get) Token: 0x06002714 RID: 10004 RVA: 0x000DE1B7 File Offset: 0x000DC3B7
		// (set) Token: 0x06002715 RID: 10005 RVA: 0x000DE1BF File Offset: 0x000DC3BF
		[XmlAnyAttribute]
		public XmlAttribute[] UnhandledAttributes
		{
			get
			{
				return this.moreAttributes;
			}
			set
			{
				this.moreAttributes = value;
			}
		}

		// Token: 0x1700081F RID: 2079
		// (get) Token: 0x06002716 RID: 10006 RVA: 0x000DE1C8 File Offset: 0x000DC3C8
		// (set) Token: 0x06002717 RID: 10007 RVA: 0x000DE1D0 File Offset: 0x000DC3D0
		[XmlIgnore]
		internal override string IdAttribute
		{
			get
			{
				return this.Id;
			}
			set
			{
				this.Id = value;
			}
		}

		// Token: 0x06002718 RID: 10008 RVA: 0x000DE1BF File Offset: 0x000DC3BF
		internal override void SetUnhandledAttributes(XmlAttribute[] moreAttributes)
		{
			this.moreAttributes = moreAttributes;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaAnnotation" /> class.</summary>
		// Token: 0x06002719 RID: 10009 RVA: 0x000DE1D9 File Offset: 0x000DC3D9
		public XmlSchemaAnnotation()
		{
		}

		// Token: 0x04001BE3 RID: 7139
		private string id;

		// Token: 0x04001BE4 RID: 7140
		private XmlSchemaObjectCollection items = new XmlSchemaObjectCollection();

		// Token: 0x04001BE5 RID: 7141
		private XmlAttribute[] moreAttributes;
	}
}
