﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Xml.Schema
{
	/// <summary>Provides the collections for contained elements in the <see cref="T:System.Xml.Schema.XmlSchema" /> class (for example, Attributes, AttributeGroups, Elements, and so on).</summary>
	// Token: 0x0200043E RID: 1086
	public class XmlSchemaObjectTable
	{
		// Token: 0x0600290C RID: 10508 RVA: 0x000E0C49 File Offset: 0x000DEE49
		internal XmlSchemaObjectTable()
		{
		}

		// Token: 0x0600290D RID: 10509 RVA: 0x000E0C67 File Offset: 0x000DEE67
		internal void Add(XmlQualifiedName name, XmlSchemaObject value)
		{
			this.table.Add(name, value);
			this.entries.Add(new XmlSchemaObjectTable.XmlSchemaObjectEntry(name, value));
		}

		// Token: 0x0600290E RID: 10510 RVA: 0x000E0C88 File Offset: 0x000DEE88
		internal void Insert(XmlQualifiedName name, XmlSchemaObject value)
		{
			XmlSchemaObject xso = null;
			if (this.table.TryGetValue(name, out xso))
			{
				this.table[name] = value;
				int index = this.FindIndexByValue(xso);
				this.entries[index] = new XmlSchemaObjectTable.XmlSchemaObjectEntry(name, value);
				return;
			}
			this.Add(name, value);
		}

		// Token: 0x0600290F RID: 10511 RVA: 0x000E0CD8 File Offset: 0x000DEED8
		internal void Replace(XmlQualifiedName name, XmlSchemaObject value)
		{
			XmlSchemaObject xso;
			if (this.table.TryGetValue(name, out xso))
			{
				this.table[name] = value;
				int index = this.FindIndexByValue(xso);
				this.entries[index] = new XmlSchemaObjectTable.XmlSchemaObjectEntry(name, value);
			}
		}

		// Token: 0x06002910 RID: 10512 RVA: 0x000E0D1D File Offset: 0x000DEF1D
		internal void Clear()
		{
			this.table.Clear();
			this.entries.Clear();
		}

		// Token: 0x06002911 RID: 10513 RVA: 0x000E0D38 File Offset: 0x000DEF38
		internal void Remove(XmlQualifiedName name)
		{
			XmlSchemaObject xso;
			if (this.table.TryGetValue(name, out xso))
			{
				this.table.Remove(name);
				int index = this.FindIndexByValue(xso);
				this.entries.RemoveAt(index);
			}
		}

		// Token: 0x06002912 RID: 10514 RVA: 0x000E0D78 File Offset: 0x000DEF78
		private int FindIndexByValue(XmlSchemaObject xso)
		{
			for (int i = 0; i < this.entries.Count; i++)
			{
				if (this.entries[i].xso == xso)
				{
					return i;
				}
			}
			return -1;
		}

		/// <summary>Gets the number of items contained in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</summary>
		/// <returns>The number of items contained in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</returns>
		// Token: 0x170008E0 RID: 2272
		// (get) Token: 0x06002913 RID: 10515 RVA: 0x000E0DB2 File Offset: 0x000DEFB2
		public int Count
		{
			get
			{
				return this.table.Count;
			}
		}

		/// <summary>Determines if the qualified name specified exists in the collection.</summary>
		/// <param name="name">The <see cref="T:System.Xml.XmlQualifiedName" />.</param>
		/// <returns>
		///     <see langword="true" /> if the qualified name specified exists in the collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002914 RID: 10516 RVA: 0x000E0DBF File Offset: 0x000DEFBF
		public bool Contains(XmlQualifiedName name)
		{
			return this.table.ContainsKey(name);
		}

		/// <summary>Returns the element in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> specified by qualified name.</summary>
		/// <param name="name">The <see cref="T:System.Xml.XmlQualifiedName" /> of the element to return.</param>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaObject" /> of the element in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> specified by qualified name.</returns>
		// Token: 0x170008E1 RID: 2273
		public XmlSchemaObject this[XmlQualifiedName name]
		{
			get
			{
				XmlSchemaObject result;
				if (this.table.TryGetValue(name, out result))
				{
					return result;
				}
				return null;
			}
		}

		/// <summary>Returns a collection of all the named elements in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</summary>
		/// <returns>A collection of all the named elements in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</returns>
		// Token: 0x170008E2 RID: 2274
		// (get) Token: 0x06002916 RID: 10518 RVA: 0x000E0DF0 File Offset: 0x000DEFF0
		public ICollection Names
		{
			get
			{
				return new XmlSchemaObjectTable.NamesCollection(this.entries, this.table.Count);
			}
		}

		/// <summary>Returns a collection of all the values for all the elements in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</summary>
		/// <returns>A collection of all the values for all the elements in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</returns>
		// Token: 0x170008E3 RID: 2275
		// (get) Token: 0x06002917 RID: 10519 RVA: 0x000E0E08 File Offset: 0x000DF008
		public ICollection Values
		{
			get
			{
				return new XmlSchemaObjectTable.ValuesCollection(this.entries, this.table.Count);
			}
		}

		/// <summary>Returns an enumerator that can iterate through the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionaryEnumerator" /> that can iterate through <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</returns>
		// Token: 0x06002918 RID: 10520 RVA: 0x000E0E20 File Offset: 0x000DF020
		public IDictionaryEnumerator GetEnumerator()
		{
			return new XmlSchemaObjectTable.XSODictionaryEnumerator(this.entries, this.table.Count, XmlSchemaObjectTable.EnumeratorType.DictionaryEntry);
		}

		// Token: 0x04001CA1 RID: 7329
		private Dictionary<XmlQualifiedName, XmlSchemaObject> table = new Dictionary<XmlQualifiedName, XmlSchemaObject>();

		// Token: 0x04001CA2 RID: 7330
		private List<XmlSchemaObjectTable.XmlSchemaObjectEntry> entries = new List<XmlSchemaObjectTable.XmlSchemaObjectEntry>();

		// Token: 0x0200043F RID: 1087
		internal enum EnumeratorType
		{
			// Token: 0x04001CA4 RID: 7332
			Keys,
			// Token: 0x04001CA5 RID: 7333
			Values,
			// Token: 0x04001CA6 RID: 7334
			DictionaryEntry
		}

		// Token: 0x02000440 RID: 1088
		internal struct XmlSchemaObjectEntry
		{
			// Token: 0x06002919 RID: 10521 RVA: 0x000E0E39 File Offset: 0x000DF039
			public XmlSchemaObjectEntry(XmlQualifiedName name, XmlSchemaObject value)
			{
				this.qname = name;
				this.xso = value;
			}

			// Token: 0x0600291A RID: 10522 RVA: 0x000E0E49 File Offset: 0x000DF049
			public XmlSchemaObject IsMatch(string localName, string ns)
			{
				if (localName == this.qname.Name && ns == this.qname.Namespace)
				{
					return this.xso;
				}
				return null;
			}

			// Token: 0x0600291B RID: 10523 RVA: 0x000E0E79 File Offset: 0x000DF079
			public void Reset()
			{
				this.qname = null;
				this.xso = null;
			}

			// Token: 0x04001CA7 RID: 7335
			internal XmlQualifiedName qname;

			// Token: 0x04001CA8 RID: 7336
			internal XmlSchemaObject xso;
		}

		// Token: 0x02000441 RID: 1089
		internal class NamesCollection : ICollection, IEnumerable
		{
			// Token: 0x0600291C RID: 10524 RVA: 0x000E0E89 File Offset: 0x000DF089
			internal NamesCollection(List<XmlSchemaObjectTable.XmlSchemaObjectEntry> entries, int size)
			{
				this.entries = entries;
				this.size = size;
			}

			// Token: 0x170008E4 RID: 2276
			// (get) Token: 0x0600291D RID: 10525 RVA: 0x000E0E9F File Offset: 0x000DF09F
			public int Count
			{
				get
				{
					return this.size;
				}
			}

			// Token: 0x170008E5 RID: 2277
			// (get) Token: 0x0600291E RID: 10526 RVA: 0x000E0EA7 File Offset: 0x000DF0A7
			public object SyncRoot
			{
				get
				{
					return ((ICollection)this.entries).SyncRoot;
				}
			}

			// Token: 0x170008E6 RID: 2278
			// (get) Token: 0x0600291F RID: 10527 RVA: 0x000E0EB4 File Offset: 0x000DF0B4
			public bool IsSynchronized
			{
				get
				{
					return ((ICollection)this.entries).IsSynchronized;
				}
			}

			// Token: 0x06002920 RID: 10528 RVA: 0x000E0EC4 File Offset: 0x000DF0C4
			public void CopyTo(Array array, int arrayIndex)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (arrayIndex < 0)
				{
					throw new ArgumentOutOfRangeException("arrayIndex");
				}
				for (int i = 0; i < this.size; i++)
				{
					array.SetValue(this.entries[i].qname, arrayIndex++);
				}
			}

			// Token: 0x06002921 RID: 10529 RVA: 0x000E0F1C File Offset: 0x000DF11C
			public IEnumerator GetEnumerator()
			{
				return new XmlSchemaObjectTable.XSOEnumerator(this.entries, this.size, XmlSchemaObjectTable.EnumeratorType.Keys);
			}

			// Token: 0x04001CA9 RID: 7337
			private List<XmlSchemaObjectTable.XmlSchemaObjectEntry> entries;

			// Token: 0x04001CAA RID: 7338
			private int size;
		}

		// Token: 0x02000442 RID: 1090
		internal class ValuesCollection : ICollection, IEnumerable
		{
			// Token: 0x06002922 RID: 10530 RVA: 0x000E0F30 File Offset: 0x000DF130
			internal ValuesCollection(List<XmlSchemaObjectTable.XmlSchemaObjectEntry> entries, int size)
			{
				this.entries = entries;
				this.size = size;
			}

			// Token: 0x170008E7 RID: 2279
			// (get) Token: 0x06002923 RID: 10531 RVA: 0x000E0F46 File Offset: 0x000DF146
			public int Count
			{
				get
				{
					return this.size;
				}
			}

			// Token: 0x170008E8 RID: 2280
			// (get) Token: 0x06002924 RID: 10532 RVA: 0x000E0F4E File Offset: 0x000DF14E
			public object SyncRoot
			{
				get
				{
					return ((ICollection)this.entries).SyncRoot;
				}
			}

			// Token: 0x170008E9 RID: 2281
			// (get) Token: 0x06002925 RID: 10533 RVA: 0x000E0F5B File Offset: 0x000DF15B
			public bool IsSynchronized
			{
				get
				{
					return ((ICollection)this.entries).IsSynchronized;
				}
			}

			// Token: 0x06002926 RID: 10534 RVA: 0x000E0F68 File Offset: 0x000DF168
			public void CopyTo(Array array, int arrayIndex)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (arrayIndex < 0)
				{
					throw new ArgumentOutOfRangeException("arrayIndex");
				}
				for (int i = 0; i < this.size; i++)
				{
					array.SetValue(this.entries[i].xso, arrayIndex++);
				}
			}

			// Token: 0x06002927 RID: 10535 RVA: 0x000E0FC0 File Offset: 0x000DF1C0
			public IEnumerator GetEnumerator()
			{
				return new XmlSchemaObjectTable.XSOEnumerator(this.entries, this.size, XmlSchemaObjectTable.EnumeratorType.Values);
			}

			// Token: 0x04001CAB RID: 7339
			private List<XmlSchemaObjectTable.XmlSchemaObjectEntry> entries;

			// Token: 0x04001CAC RID: 7340
			private int size;
		}

		// Token: 0x02000443 RID: 1091
		internal class XSOEnumerator : IEnumerator
		{
			// Token: 0x06002928 RID: 10536 RVA: 0x000E0FD4 File Offset: 0x000DF1D4
			internal XSOEnumerator(List<XmlSchemaObjectTable.XmlSchemaObjectEntry> entries, int size, XmlSchemaObjectTable.EnumeratorType enumType)
			{
				this.entries = entries;
				this.size = size;
				this.enumType = enumType;
				this.currentIndex = -1;
			}

			// Token: 0x170008EA RID: 2282
			// (get) Token: 0x06002929 RID: 10537 RVA: 0x000E0FF8 File Offset: 0x000DF1F8
			public object Current
			{
				get
				{
					if (this.currentIndex == -1)
					{
						throw new InvalidOperationException(Res.GetString("Enumeration has not started. Call MoveNext.", new object[]
						{
							string.Empty
						}));
					}
					if (this.currentIndex >= this.size)
					{
						throw new InvalidOperationException(Res.GetString("Enumeration has already finished.", new object[]
						{
							string.Empty
						}));
					}
					switch (this.enumType)
					{
					case XmlSchemaObjectTable.EnumeratorType.Keys:
						return this.currentKey;
					case XmlSchemaObjectTable.EnumeratorType.Values:
						return this.currentValue;
					case XmlSchemaObjectTable.EnumeratorType.DictionaryEntry:
						return new DictionaryEntry(this.currentKey, this.currentValue);
					default:
						return null;
					}
				}
			}

			// Token: 0x0600292A RID: 10538 RVA: 0x000E109C File Offset: 0x000DF29C
			public bool MoveNext()
			{
				if (this.currentIndex >= this.size - 1)
				{
					this.currentValue = null;
					this.currentKey = null;
					return false;
				}
				this.currentIndex++;
				this.currentValue = this.entries[this.currentIndex].xso;
				this.currentKey = this.entries[this.currentIndex].qname;
				return true;
			}

			// Token: 0x0600292B RID: 10539 RVA: 0x000E1110 File Offset: 0x000DF310
			public void Reset()
			{
				this.currentIndex = -1;
				this.currentValue = null;
				this.currentKey = null;
			}

			// Token: 0x04001CAD RID: 7341
			private List<XmlSchemaObjectTable.XmlSchemaObjectEntry> entries;

			// Token: 0x04001CAE RID: 7342
			private XmlSchemaObjectTable.EnumeratorType enumType;

			// Token: 0x04001CAF RID: 7343
			protected int currentIndex;

			// Token: 0x04001CB0 RID: 7344
			protected int size;

			// Token: 0x04001CB1 RID: 7345
			protected XmlQualifiedName currentKey;

			// Token: 0x04001CB2 RID: 7346
			protected XmlSchemaObject currentValue;
		}

		// Token: 0x02000444 RID: 1092
		internal class XSODictionaryEnumerator : XmlSchemaObjectTable.XSOEnumerator, IDictionaryEnumerator, IEnumerator
		{
			// Token: 0x0600292C RID: 10540 RVA: 0x000E1127 File Offset: 0x000DF327
			internal XSODictionaryEnumerator(List<XmlSchemaObjectTable.XmlSchemaObjectEntry> entries, int size, XmlSchemaObjectTable.EnumeratorType enumType) : base(entries, size, enumType)
			{
			}

			// Token: 0x170008EB RID: 2283
			// (get) Token: 0x0600292D RID: 10541 RVA: 0x000E1134 File Offset: 0x000DF334
			public DictionaryEntry Entry
			{
				get
				{
					if (this.currentIndex == -1)
					{
						throw new InvalidOperationException(Res.GetString("Enumeration has not started. Call MoveNext.", new object[]
						{
							string.Empty
						}));
					}
					if (this.currentIndex >= this.size)
					{
						throw new InvalidOperationException(Res.GetString("Enumeration has already finished.", new object[]
						{
							string.Empty
						}));
					}
					return new DictionaryEntry(this.currentKey, this.currentValue);
				}
			}

			// Token: 0x170008EC RID: 2284
			// (get) Token: 0x0600292E RID: 10542 RVA: 0x000E11A8 File Offset: 0x000DF3A8
			public object Key
			{
				get
				{
					if (this.currentIndex == -1)
					{
						throw new InvalidOperationException(Res.GetString("Enumeration has not started. Call MoveNext.", new object[]
						{
							string.Empty
						}));
					}
					if (this.currentIndex >= this.size)
					{
						throw new InvalidOperationException(Res.GetString("Enumeration has already finished.", new object[]
						{
							string.Empty
						}));
					}
					return this.currentKey;
				}
			}

			// Token: 0x170008ED RID: 2285
			// (get) Token: 0x0600292F RID: 10543 RVA: 0x000E1210 File Offset: 0x000DF410
			public object Value
			{
				get
				{
					if (this.currentIndex == -1)
					{
						throw new InvalidOperationException(Res.GetString("Enumeration has not started. Call MoveNext.", new object[]
						{
							string.Empty
						}));
					}
					if (this.currentIndex >= this.size)
					{
						throw new InvalidOperationException(Res.GetString("Enumeration has already finished.", new object[]
						{
							string.Empty
						}));
					}
					return this.currentValue;
				}
			}
		}
	}
}
