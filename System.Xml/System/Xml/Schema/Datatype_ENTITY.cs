﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003AA RID: 938
	internal class Datatype_ENTITY : Datatype_NCName
	{
		// Token: 0x17000730 RID: 1840
		// (get) Token: 0x06002301 RID: 8961 RVA: 0x000BE2B5 File Offset: 0x000BC4B5
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Entity;
			}
		}

		// Token: 0x17000731 RID: 1841
		// (get) Token: 0x06002302 RID: 8962 RVA: 0x000042A3 File Offset: 0x000024A3
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.ENTITY;
			}
		}

		// Token: 0x06002303 RID: 8963 RVA: 0x000BE2A9 File Offset: 0x000BC4A9
		public Datatype_ENTITY()
		{
		}
	}
}
