﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200037D RID: 893
	[Flags]
	internal enum RestrictionFlags
	{
		// Token: 0x04001822 RID: 6178
		Length = 1,
		// Token: 0x04001823 RID: 6179
		MinLength = 2,
		// Token: 0x04001824 RID: 6180
		MaxLength = 4,
		// Token: 0x04001825 RID: 6181
		Pattern = 8,
		// Token: 0x04001826 RID: 6182
		Enumeration = 16,
		// Token: 0x04001827 RID: 6183
		WhiteSpace = 32,
		// Token: 0x04001828 RID: 6184
		MaxInclusive = 64,
		// Token: 0x04001829 RID: 6185
		MaxExclusive = 128,
		// Token: 0x0400182A RID: 6186
		MinInclusive = 256,
		// Token: 0x0400182B RID: 6187
		MinExclusive = 512,
		// Token: 0x0400182C RID: 6188
		TotalDigits = 1024,
		// Token: 0x0400182D RID: 6189
		FractionDigits = 2048
	}
}
