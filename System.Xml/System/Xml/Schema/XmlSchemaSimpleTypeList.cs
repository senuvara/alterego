﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="list" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to define a <see langword="simpleType" /> element as a list of values of a specified data type.</summary>
	// Token: 0x02000450 RID: 1104
	public class XmlSchemaSimpleTypeList : XmlSchemaSimpleTypeContent
	{
		/// <summary>Gets or sets the name of a built-in data type or <see langword="simpleType" /> element defined in this schema (or another schema indicated by the specified namespace).</summary>
		/// <returns>The type name of the simple type list.</returns>
		// Token: 0x17000917 RID: 2327
		// (get) Token: 0x0600299E RID: 10654 RVA: 0x000E365C File Offset: 0x000E185C
		// (set) Token: 0x0600299F RID: 10655 RVA: 0x000E3664 File Offset: 0x000E1864
		[XmlAttribute("itemType")]
		public XmlQualifiedName ItemTypeName
		{
			get
			{
				return this.itemTypeName;
			}
			set
			{
				this.itemTypeName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets or sets the <see langword="simpleType" /> element that is derived from the type specified by the base value.</summary>
		/// <returns>The item type for the simple type element.</returns>
		// Token: 0x17000918 RID: 2328
		// (get) Token: 0x060029A0 RID: 10656 RVA: 0x000E367D File Offset: 0x000E187D
		// (set) Token: 0x060029A1 RID: 10657 RVA: 0x000E3685 File Offset: 0x000E1885
		[XmlElement("simpleType", typeof(XmlSchemaSimpleType))]
		public XmlSchemaSimpleType ItemType
		{
			get
			{
				return this.itemType;
			}
			set
			{
				this.itemType = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaSimpleType" /> representing the type of the <see langword="simpleType" /> element based on the <see cref="P:System.Xml.Schema.XmlSchemaSimpleTypeList.ItemType" /> and <see cref="P:System.Xml.Schema.XmlSchemaSimpleTypeList.ItemTypeName" /> values of the simple type.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaSimpleType" /> representing the type of the <see langword="simpleType" /> element.</returns>
		// Token: 0x17000919 RID: 2329
		// (get) Token: 0x060029A2 RID: 10658 RVA: 0x000E368E File Offset: 0x000E188E
		// (set) Token: 0x060029A3 RID: 10659 RVA: 0x000E3696 File Offset: 0x000E1896
		[XmlIgnore]
		public XmlSchemaSimpleType BaseItemType
		{
			get
			{
				return this.baseItemType;
			}
			set
			{
				this.baseItemType = value;
			}
		}

		// Token: 0x060029A4 RID: 10660 RVA: 0x000E369F File Offset: 0x000E189F
		internal override XmlSchemaObject Clone()
		{
			XmlSchemaSimpleTypeList xmlSchemaSimpleTypeList = (XmlSchemaSimpleTypeList)base.MemberwiseClone();
			xmlSchemaSimpleTypeList.ItemTypeName = this.itemTypeName.Clone();
			return xmlSchemaSimpleTypeList;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaSimpleTypeList" /> class.</summary>
		// Token: 0x060029A5 RID: 10661 RVA: 0x000E36BD File Offset: 0x000E18BD
		public XmlSchemaSimpleTypeList()
		{
		}

		// Token: 0x04001CDE RID: 7390
		private XmlQualifiedName itemTypeName = XmlQualifiedName.Empty;

		// Token: 0x04001CDF RID: 7391
		private XmlSchemaSimpleType itemType;

		// Token: 0x04001CE0 RID: 7392
		private XmlSchemaSimpleType baseItemType;
	}
}
