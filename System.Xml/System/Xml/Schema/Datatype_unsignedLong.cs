﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B4 RID: 948
	internal class Datatype_unsignedLong : Datatype_nonNegativeInteger
	{
		// Token: 0x17000753 RID: 1875
		// (get) Token: 0x06002342 RID: 9026 RVA: 0x000BE7F8 File Offset: 0x000BC9F8
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_unsignedLong.numeric10FacetsChecker;
			}
		}

		// Token: 0x17000754 RID: 1876
		// (get) Token: 0x06002343 RID: 9027 RVA: 0x000BE7FF File Offset: 0x000BC9FF
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.UnsignedLong;
			}
		}

		// Token: 0x06002344 RID: 9028 RVA: 0x000BE804 File Offset: 0x000BCA04
		internal override int Compare(object value1, object value2)
		{
			return ((ulong)value1).CompareTo(value2);
		}

		// Token: 0x17000755 RID: 1877
		// (get) Token: 0x06002345 RID: 9029 RVA: 0x000BE820 File Offset: 0x000BCA20
		public override Type ValueType
		{
			get
			{
				return Datatype_unsignedLong.atomicValueType;
			}
		}

		// Token: 0x17000756 RID: 1878
		// (get) Token: 0x06002346 RID: 9030 RVA: 0x000BE827 File Offset: 0x000BCA27
		internal override Type ListValueType
		{
			get
			{
				return Datatype_unsignedLong.listValueType;
			}
		}

		// Token: 0x06002347 RID: 9031 RVA: 0x000BE830 File Offset: 0x000BCA30
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_unsignedLong.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				ulong num;
				ex = XmlConvert.TryToUInt64(s, out num);
				if (ex == null)
				{
					ex = Datatype_unsignedLong.numeric10FacetsChecker.CheckValueFacets(num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002348 RID: 9032 RVA: 0x000BE87F File Offset: 0x000BCA7F
		public Datatype_unsignedLong()
		{
		}

		// Token: 0x06002349 RID: 9033 RVA: 0x000BE887 File Offset: 0x000BCA87
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_unsignedLong()
		{
		}

		// Token: 0x040018CD RID: 6349
		private static readonly Type atomicValueType = typeof(ulong);

		// Token: 0x040018CE RID: 6350
		private static readonly Type listValueType = typeof(ulong[]);

		// Token: 0x040018CF RID: 6351
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(0m, 18446744073709551615m);
	}
}
