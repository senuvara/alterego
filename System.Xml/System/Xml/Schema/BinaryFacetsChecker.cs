﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x020003CC RID: 972
	internal class BinaryFacetsChecker : FacetsChecker
	{
		// Token: 0x060023F6 RID: 9206 RVA: 0x000C20FC File Offset: 0x000C02FC
		internal override Exception CheckValueFacets(object value, XmlSchemaDatatype datatype)
		{
			byte[] value2 = (byte[])value;
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023F7 RID: 9207 RVA: 0x000C2118 File Offset: 0x000C0318
		internal override Exception CheckValueFacets(byte[] value, XmlSchemaDatatype datatype)
		{
			RestrictionFacets restriction = datatype.Restriction;
			int num = value.Length;
			RestrictionFlags restrictionFlags = (restriction != null) ? restriction.Flags : ((RestrictionFlags)0);
			if (restrictionFlags != (RestrictionFlags)0)
			{
				if ((restrictionFlags & RestrictionFlags.Length) != (RestrictionFlags)0 && restriction.Length != num)
				{
					return new XmlSchemaException("The actual length is not equal to the specified length.", string.Empty);
				}
				if ((restrictionFlags & RestrictionFlags.MinLength) != (RestrictionFlags)0 && num < restriction.MinLength)
				{
					return new XmlSchemaException("The actual length is less than the MinLength value.", string.Empty);
				}
				if ((restrictionFlags & RestrictionFlags.MaxLength) != (RestrictionFlags)0 && restriction.MaxLength < num)
				{
					return new XmlSchemaException("The actual length is greater than the MaxLength value.", string.Empty);
				}
				if ((restrictionFlags & RestrictionFlags.Enumeration) != (RestrictionFlags)0 && !this.MatchEnumeration(value, restriction.Enumeration, datatype))
				{
					return new XmlSchemaException("The Enumeration constraint failed.", string.Empty);
				}
			}
			return null;
		}

		// Token: 0x060023F8 RID: 9208 RVA: 0x000C21C4 File Offset: 0x000C03C4
		internal override bool MatchEnumeration(object value, ArrayList enumeration, XmlSchemaDatatype datatype)
		{
			return this.MatchEnumeration((byte[])value, enumeration, datatype);
		}

		// Token: 0x060023F9 RID: 9209 RVA: 0x000C21D4 File Offset: 0x000C03D4
		private bool MatchEnumeration(byte[] value, ArrayList enumeration, XmlSchemaDatatype datatype)
		{
			for (int i = 0; i < enumeration.Count; i++)
			{
				if (datatype.Compare(value, (byte[])enumeration[i]) == 0)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060023FA RID: 9210 RVA: 0x000C1A2D File Offset: 0x000BFC2D
		public BinaryFacetsChecker()
		{
		}
	}
}
