﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000398 RID: 920
	internal class Datatype_year : Datatype_dateTimeBase
	{
		// Token: 0x17000701 RID: 1793
		// (get) Token: 0x060022B0 RID: 8880 RVA: 0x000BDF47 File Offset: 0x000BC147
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.GYear;
			}
		}

		// Token: 0x060022B1 RID: 8881 RVA: 0x000BDF4B File Offset: 0x000BC14B
		internal Datatype_year() : base(XsdDateTimeFlags.GYear)
		{
		}
	}
}
