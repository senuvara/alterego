﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003A5 RID: 933
	internal class Datatype_NMTOKEN : Datatype_token
	{
		// Token: 0x17000728 RID: 1832
		// (get) Token: 0x060022F3 RID: 8947 RVA: 0x000297BA File Offset: 0x000279BA
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.NmToken;
			}
		}

		// Token: 0x17000729 RID: 1833
		// (get) Token: 0x060022F4 RID: 8948 RVA: 0x00006CB1 File Offset: 0x00004EB1
		public override XmlTokenizedType TokenizedType
		{
			get
			{
				return XmlTokenizedType.NMTOKEN;
			}
		}

		// Token: 0x060022F5 RID: 8949 RVA: 0x000BE24C File Offset: 0x000BC44C
		public Datatype_NMTOKEN()
		{
		}
	}
}
