﻿using System;
using System.Collections;
using System.Globalization;

namespace System.Xml.Schema
{
	// Token: 0x020003C5 RID: 965
	internal class Numeric10FacetsChecker : FacetsChecker
	{
		// Token: 0x060023CD RID: 9165 RVA: 0x000C1594 File Offset: 0x000BF794
		internal Numeric10FacetsChecker(decimal minVal, decimal maxVal)
		{
			this.minValue = minVal;
			this.maxValue = maxVal;
		}

		// Token: 0x060023CE RID: 9166 RVA: 0x000C15AC File Offset: 0x000BF7AC
		internal override Exception CheckValueFacets(object value, XmlSchemaDatatype datatype)
		{
			decimal value2 = datatype.ValueConverter.ToDecimal(value);
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023CF RID: 9167 RVA: 0x000C15D0 File Offset: 0x000BF7D0
		internal override Exception CheckValueFacets(decimal value, XmlSchemaDatatype datatype)
		{
			RestrictionFacets restriction = datatype.Restriction;
			RestrictionFlags restrictionFlags = (restriction != null) ? restriction.Flags : ((RestrictionFlags)0);
			XmlValueConverter valueConverter = datatype.ValueConverter;
			if (value > this.maxValue || value < this.minValue)
			{
				return new OverflowException(Res.GetString("Value '{0}' was either too large or too small for {1}.", new object[]
				{
					value.ToString(CultureInfo.InvariantCulture),
					datatype.TypeCodeString
				}));
			}
			if (restrictionFlags == (RestrictionFlags)0)
			{
				return null;
			}
			if ((restrictionFlags & RestrictionFlags.MaxInclusive) != (RestrictionFlags)0 && value > valueConverter.ToDecimal(restriction.MaxInclusive))
			{
				return new XmlSchemaException("The MaxInclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MaxExclusive) != (RestrictionFlags)0 && value >= valueConverter.ToDecimal(restriction.MaxExclusive))
			{
				return new XmlSchemaException("The MaxExclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MinInclusive) != (RestrictionFlags)0 && value < valueConverter.ToDecimal(restriction.MinInclusive))
			{
				return new XmlSchemaException("The MinInclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.MinExclusive) != (RestrictionFlags)0 && value <= valueConverter.ToDecimal(restriction.MinExclusive))
			{
				return new XmlSchemaException("The MinExclusive constraint failed.", string.Empty);
			}
			if ((restrictionFlags & RestrictionFlags.Enumeration) != (RestrictionFlags)0 && !this.MatchEnumeration(value, restriction.Enumeration, valueConverter))
			{
				return new XmlSchemaException("The Enumeration constraint failed.", string.Empty);
			}
			return this.CheckTotalAndFractionDigits(value, restriction.TotalDigits, restriction.FractionDigits, (restrictionFlags & RestrictionFlags.TotalDigits) > (RestrictionFlags)0, (restrictionFlags & RestrictionFlags.FractionDigits) > (RestrictionFlags)0);
		}

		// Token: 0x060023D0 RID: 9168 RVA: 0x000C1748 File Offset: 0x000BF948
		internal override Exception CheckValueFacets(long value, XmlSchemaDatatype datatype)
		{
			decimal value2 = value;
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023D1 RID: 9169 RVA: 0x000C1764 File Offset: 0x000BF964
		internal override Exception CheckValueFacets(int value, XmlSchemaDatatype datatype)
		{
			decimal value2 = value;
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023D2 RID: 9170 RVA: 0x000C1780 File Offset: 0x000BF980
		internal override Exception CheckValueFacets(short value, XmlSchemaDatatype datatype)
		{
			decimal value2 = value;
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023D3 RID: 9171 RVA: 0x000C179C File Offset: 0x000BF99C
		internal override Exception CheckValueFacets(byte value, XmlSchemaDatatype datatype)
		{
			decimal value2 = value;
			return this.CheckValueFacets(value2, datatype);
		}

		// Token: 0x060023D4 RID: 9172 RVA: 0x000C17B8 File Offset: 0x000BF9B8
		internal override bool MatchEnumeration(object value, ArrayList enumeration, XmlSchemaDatatype datatype)
		{
			return this.MatchEnumeration(datatype.ValueConverter.ToDecimal(value), enumeration, datatype.ValueConverter);
		}

		// Token: 0x060023D5 RID: 9173 RVA: 0x000C17D4 File Offset: 0x000BF9D4
		internal bool MatchEnumeration(decimal value, ArrayList enumeration, XmlValueConverter valueConverter)
		{
			for (int i = 0; i < enumeration.Count; i++)
			{
				if (value == valueConverter.ToDecimal(enumeration[i]))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060023D6 RID: 9174 RVA: 0x000C180C File Offset: 0x000BFA0C
		internal Exception CheckTotalAndFractionDigits(decimal value, int totalDigits, int fractionDigits, bool checkTotal, bool checkFraction)
		{
			decimal d = FacetsChecker.Power(10, totalDigits) - 1m;
			int num = 0;
			if (value < 0m)
			{
				value = decimal.Negate(value);
			}
			while (decimal.Truncate(value) != value)
			{
				value *= 10m;
				num++;
			}
			if (checkTotal && (value > d || num > totalDigits))
			{
				return new XmlSchemaException("The TotalDigits constraint failed.", string.Empty);
			}
			if (checkFraction && num > fractionDigits)
			{
				return new XmlSchemaException("The FractionDigits constraint failed.", string.Empty);
			}
			return null;
		}

		// Token: 0x060023D7 RID: 9175 RVA: 0x000C18A0 File Offset: 0x000BFAA0
		// Note: this type is marked as 'beforefieldinit'.
		static Numeric10FacetsChecker()
		{
		}

		// Token: 0x040018F6 RID: 6390
		private static readonly char[] signs = new char[]
		{
			'+',
			'-'
		};

		// Token: 0x040018F7 RID: 6391
		private decimal maxValue;

		// Token: 0x040018F8 RID: 6392
		private decimal minValue;
	}
}
