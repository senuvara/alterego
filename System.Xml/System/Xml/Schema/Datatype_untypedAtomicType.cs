﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000386 RID: 902
	internal class Datatype_untypedAtomicType : Datatype_anyAtomicType
	{
		// Token: 0x06002250 RID: 8784 RVA: 0x000BD89C File Offset: 0x000BBA9C
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlUntypedConverter.Untyped;
		}

		// Token: 0x170006D1 RID: 1745
		// (get) Token: 0x06002251 RID: 8785 RVA: 0x000020CD File Offset: 0x000002CD
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Preserve;
			}
		}

		// Token: 0x170006D2 RID: 1746
		// (get) Token: 0x06002252 RID: 8786 RVA: 0x00074B49 File Offset: 0x00072D49
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.UntypedAtomic;
			}
		}

		// Token: 0x06002253 RID: 8787 RVA: 0x000BD908 File Offset: 0x000BBB08
		public Datatype_untypedAtomicType()
		{
		}
	}
}
