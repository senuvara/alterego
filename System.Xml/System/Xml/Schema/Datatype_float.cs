﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000389 RID: 905
	internal class Datatype_float : Datatype_anySimpleType
	{
		// Token: 0x06002267 RID: 8807 RVA: 0x000BD9EA File Offset: 0x000BBBEA
		internal override XmlValueConverter CreateValueConverter(XmlSchemaType schemaType)
		{
			return XmlNumeric2Converter.Create(schemaType);
		}

		// Token: 0x170006DE RID: 1758
		// (get) Token: 0x06002268 RID: 8808 RVA: 0x000BD9F2 File Offset: 0x000BBBF2
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return DatatypeImplementation.numeric2FacetsChecker;
			}
		}

		// Token: 0x170006DF RID: 1759
		// (get) Token: 0x06002269 RID: 8809 RVA: 0x000BD9F9 File Offset: 0x000BBBF9
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.Float;
			}
		}

		// Token: 0x170006E0 RID: 1760
		// (get) Token: 0x0600226A RID: 8810 RVA: 0x000BD9FD File Offset: 0x000BBBFD
		public override Type ValueType
		{
			get
			{
				return Datatype_float.atomicValueType;
			}
		}

		// Token: 0x170006E1 RID: 1761
		// (get) Token: 0x0600226B RID: 8811 RVA: 0x000BDA04 File Offset: 0x000BBC04
		internal override Type ListValueType
		{
			get
			{
				return Datatype_float.listValueType;
			}
		}

		// Token: 0x170006E2 RID: 1762
		// (get) Token: 0x0600226C RID: 8812 RVA: 0x0000284A File Offset: 0x00000A4A
		internal override XmlSchemaWhiteSpace BuiltInWhitespaceFacet
		{
			get
			{
				return XmlSchemaWhiteSpace.Collapse;
			}
		}

		// Token: 0x170006E3 RID: 1763
		// (get) Token: 0x0600226D RID: 8813 RVA: 0x000BDA0B File Offset: 0x000BBC0B
		internal override RestrictionFlags ValidRestrictionFlags
		{
			get
			{
				return RestrictionFlags.Pattern | RestrictionFlags.Enumeration | RestrictionFlags.WhiteSpace | RestrictionFlags.MaxInclusive | RestrictionFlags.MaxExclusive | RestrictionFlags.MinInclusive | RestrictionFlags.MinExclusive;
			}
		}

		// Token: 0x0600226E RID: 8814 RVA: 0x000BDA14 File Offset: 0x000BBC14
		internal override int Compare(object value1, object value2)
		{
			return ((float)value1).CompareTo(value2);
		}

		// Token: 0x0600226F RID: 8815 RVA: 0x000BDA30 File Offset: 0x000BBC30
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = DatatypeImplementation.numeric2FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				float num;
				ex = XmlConvert.TryToSingle(s, out num);
				if (ex == null)
				{
					ex = DatatypeImplementation.numeric2FacetsChecker.CheckValueFacets(num, this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002270 RID: 8816 RVA: 0x000BD900 File Offset: 0x000BBB00
		public Datatype_float()
		{
		}

		// Token: 0x06002271 RID: 8817 RVA: 0x000BDA7A File Offset: 0x000BBC7A
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_float()
		{
		}

		// Token: 0x040018A8 RID: 6312
		private static readonly Type atomicValueType = typeof(float);

		// Token: 0x040018A9 RID: 6313
		private static readonly Type listValueType = typeof(float[]);
	}
}
