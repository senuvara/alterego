﻿using System;
using System.Collections;
using System.Threading;
using System.Xml.XmlConfiguration;

namespace System.Xml.Schema
{
	/// <summary>Contains a cache of XML Schema definition language (XSD) and XML-Data Reduced (XDR) schemas. The <see cref="T:System.Xml.Schema.XmlSchemaCollection" /> class class is obsolete. Use <see cref="T:System.Xml.Schema.XmlSchemaSet" /> instead.</summary>
	// Token: 0x0200040D RID: 1037
	[Obsolete("Use System.Xml.Schema.XmlSchemaSet for schema compilation and validation. http://go.microsoft.com/fwlink/?linkid=14202")]
	public sealed class XmlSchemaCollection : ICollection, IEnumerable
	{
		/// <summary>Initializes a new instance of the <see langword="XmlSchemaCollection" /> class.</summary>
		// Token: 0x06002770 RID: 10096 RVA: 0x000DE841 File Offset: 0x000DCA41
		public XmlSchemaCollection() : this(new NameTable())
		{
		}

		/// <summary>Initializes a new instance of the <see langword="XmlSchemaCollection" /> class with the specified <see cref="T:System.Xml.XmlNameTable" />. The <see langword="XmlNameTable" /> is used when loading schemas.</summary>
		/// <param name="nametable">The <see langword="XmlNameTable" /> to use. </param>
		// Token: 0x06002771 RID: 10097 RVA: 0x000DE850 File Offset: 0x000DCA50
		public XmlSchemaCollection(XmlNameTable nametable)
		{
			if (nametable == null)
			{
				throw new ArgumentNullException("nametable");
			}
			this.nameTable = nametable;
			this.collection = Hashtable.Synchronized(new Hashtable());
			this.xmlResolver = XmlReaderSection.CreateDefaultResolver();
			this.isThreadSafe = true;
			if (this.isThreadSafe)
			{
				this.wLock = new ReaderWriterLock();
			}
		}

		/// <summary>Gets the number of namespaces defined in this collection.</summary>
		/// <returns>The number of namespaces defined in this collection.</returns>
		// Token: 0x17000848 RID: 2120
		// (get) Token: 0x06002772 RID: 10098 RVA: 0x000DE8BB File Offset: 0x000DCABB
		public int Count
		{
			get
			{
				return this.collection.Count;
			}
		}

		/// <summary>Gets the default <see langword="XmlNameTable" /> used by the <see langword="XmlSchemaCollection" /> when loading new schemas.</summary>
		/// <returns>An <see langword="XmlNameTable" />.</returns>
		// Token: 0x17000849 RID: 2121
		// (get) Token: 0x06002773 RID: 10099 RVA: 0x000DE8C8 File Offset: 0x000DCAC8
		public XmlNameTable NameTable
		{
			get
			{
				return this.nameTable;
			}
		}

		/// <summary>Sets an event handler for receiving information about the XDR and XML schema validation errors.</summary>
		// Token: 0x1400000E RID: 14
		// (add) Token: 0x06002774 RID: 10100 RVA: 0x000DE8D0 File Offset: 0x000DCAD0
		// (remove) Token: 0x06002775 RID: 10101 RVA: 0x000DE8E9 File Offset: 0x000DCAE9
		public event ValidationEventHandler ValidationEventHandler
		{
			add
			{
				this.validationEventHandler = (ValidationEventHandler)Delegate.Combine(this.validationEventHandler, value);
			}
			remove
			{
				this.validationEventHandler = (ValidationEventHandler)Delegate.Remove(this.validationEventHandler, value);
			}
		}

		// Token: 0x1700084A RID: 2122
		// (set) Token: 0x06002776 RID: 10102 RVA: 0x000DE902 File Offset: 0x000DCB02
		internal XmlResolver XmlResolver
		{
			set
			{
				this.xmlResolver = value;
			}
		}

		/// <summary>Adds the schema located by the given URL into the schema collection.</summary>
		/// <param name="ns">The namespace URI associated with the schema. For XML Schemas, this will typically be the <see langword="targetNamespace" />. </param>
		/// <param name="uri">The URL that specifies the schema to load. </param>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchema" /> added to the schema collection; <see langword="null" /> if the schema being added is an XDR schema or if there are compilation errors in the schema. </returns>
		/// <exception cref="T:System.Xml.XmlException">The schema is not a valid schema. </exception>
		// Token: 0x06002777 RID: 10103 RVA: 0x000DE90C File Offset: 0x000DCB0C
		public XmlSchema Add(string ns, string uri)
		{
			if (uri == null || uri.Length == 0)
			{
				throw new ArgumentNullException("uri");
			}
			XmlTextReader xmlTextReader = new XmlTextReader(uri, this.nameTable);
			xmlTextReader.XmlResolver = this.xmlResolver;
			XmlSchema result = null;
			try
			{
				result = this.Add(ns, xmlTextReader, this.xmlResolver);
				while (xmlTextReader.Read())
				{
				}
			}
			finally
			{
				xmlTextReader.Close();
			}
			return result;
		}

		/// <summary>Adds the schema contained in the <see cref="T:System.Xml.XmlReader" /> to the schema collection.</summary>
		/// <param name="ns">The namespace URI associated with the schema. For XML Schemas, this will typically be the <see langword="targetNamespace" />. </param>
		/// <param name="reader">
		///       <see cref="T:System.Xml.XmlReader" /> containing the schema to add. </param>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchema" /> added to the schema collection; <see langword="null" /> if the schema being added is an XDR schema or if there are compilation errors in the schema.</returns>
		/// <exception cref="T:System.Xml.XmlException">The schema is not a valid schema. </exception>
		// Token: 0x06002778 RID: 10104 RVA: 0x000DE97C File Offset: 0x000DCB7C
		public XmlSchema Add(string ns, XmlReader reader)
		{
			return this.Add(ns, reader, this.xmlResolver);
		}

		/// <summary>Adds the schema contained in the <see cref="T:System.Xml.XmlReader" /> to the schema collection. The specified <see cref="T:System.Xml.XmlResolver" /> is used to resolve any external resources.</summary>
		/// <param name="ns">The namespace URI associated with the schema. For XML Schemas, this will typically be the <see langword="targetNamespace" />. </param>
		/// <param name="reader">
		///       <see cref="T:System.Xml.XmlReader" /> containing the schema to add. </param>
		/// <param name="resolver">The <see cref="T:System.Xml.XmlResolver" /> used to resolve namespaces referenced in <see langword="include" /> and <see langword="import" /> elements or <see langword="x-schema" /> attribute (XDR schemas). If this is <see langword="null" />, external references are not resolved. </param>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchema" /> added to the schema collection; <see langword="null" /> if the schema being added is an XDR schema or if there are compilation errors in the schema.</returns>
		/// <exception cref="T:System.Xml.XmlException">The schema is not a valid schema. </exception>
		// Token: 0x06002779 RID: 10105 RVA: 0x000DE98C File Offset: 0x000DCB8C
		public XmlSchema Add(string ns, XmlReader reader, XmlResolver resolver)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			XmlNameTable nt = reader.NameTable;
			SchemaInfo schemaInfo = new SchemaInfo();
			Parser parser = new Parser(SchemaType.None, nt, this.GetSchemaNames(nt), this.validationEventHandler);
			parser.XmlResolver = resolver;
			SchemaType schemaType;
			try
			{
				schemaType = parser.Parse(reader, ns);
			}
			catch (XmlSchemaException e)
			{
				this.SendValidationEvent(e);
				return null;
			}
			if (schemaType == SchemaType.XSD)
			{
				schemaInfo.SchemaType = SchemaType.XSD;
				return this.Add(ns, schemaInfo, parser.XmlSchema, true, resolver);
			}
			SchemaInfo xdrSchema = parser.XdrSchema;
			return this.Add(ns, parser.XdrSchema, null, true, resolver);
		}

		/// <summary>Adds the <see cref="T:System.Xml.Schema.XmlSchema" /> to the collection.</summary>
		/// <param name="schema">The <see langword="XmlSchema" /> to add to the collection. </param>
		/// <returns>The <see langword="XmlSchema" /> object.</returns>
		// Token: 0x0600277A RID: 10106 RVA: 0x000DEA30 File Offset: 0x000DCC30
		public XmlSchema Add(XmlSchema schema)
		{
			return this.Add(schema, this.xmlResolver);
		}

		/// <summary>Adds the <see cref="T:System.Xml.Schema.XmlSchema" /> to the collection. The specified <see cref="T:System.Xml.XmlResolver" /> is used to resolve any external references.</summary>
		/// <param name="schema">The <see langword="XmlSchema" /> to add to the collection. </param>
		/// <param name="resolver">The <see cref="T:System.Xml.XmlResolver" /> used to resolve namespaces referenced in <see langword="include" /> and <see langword="import" /> elements. If this is <see langword="null" />, external references are not resolved. </param>
		/// <returns>The <see langword="XmlSchema" /> added to the schema collection.</returns>
		/// <exception cref="T:System.Xml.XmlException">The schema is not a valid schema. </exception>
		// Token: 0x0600277B RID: 10107 RVA: 0x000DEA40 File Offset: 0x000DCC40
		public XmlSchema Add(XmlSchema schema, XmlResolver resolver)
		{
			if (schema == null)
			{
				throw new ArgumentNullException("schema");
			}
			SchemaInfo schemaInfo = new SchemaInfo();
			schemaInfo.SchemaType = SchemaType.XSD;
			return this.Add(schema.TargetNamespace, schemaInfo, schema, true, resolver);
		}

		/// <summary>Adds all the namespaces defined in the given collection (including their associated schemas) to this collection.</summary>
		/// <param name="schema">The <see langword="XmlSchemaCollection" /> you want to add to this collection. </param>
		// Token: 0x0600277C RID: 10108 RVA: 0x000DEA78 File Offset: 0x000DCC78
		public void Add(XmlSchemaCollection schema)
		{
			if (schema == null)
			{
				throw new ArgumentNullException("schema");
			}
			if (this == schema)
			{
				return;
			}
			IDictionaryEnumerator enumerator = schema.collection.GetEnumerator();
			while (enumerator.MoveNext())
			{
				XmlSchemaCollectionNode xmlSchemaCollectionNode = (XmlSchemaCollectionNode)enumerator.Value;
				this.Add(xmlSchemaCollectionNode.NamespaceURI, xmlSchemaCollectionNode);
			}
		}

		/// <summary>Gets the <see cref="T:System.Xml.Schema.XmlSchema" /> associated with the given namespace URI.</summary>
		/// <param name="ns">The namespace URI associated with the schema you want to return. This will typically be the <see langword="targetNamespace" /> of the schema. </param>
		/// <returns>The <see langword="XmlSchema" /> associated with the namespace URI; <see langword="null" /> if there is no loaded schema associated with the given namespace or if the namespace is associated with an XDR schema.</returns>
		// Token: 0x1700084B RID: 2123
		public XmlSchema this[string ns]
		{
			get
			{
				XmlSchemaCollectionNode xmlSchemaCollectionNode = (XmlSchemaCollectionNode)this.collection[(ns != null) ? ns : string.Empty];
				if (xmlSchemaCollectionNode == null)
				{
					return null;
				}
				return xmlSchemaCollectionNode.Schema;
			}
		}

		/// <summary>Gets a value indicating whether the <see langword="targetNamespace" /> of the specified <see cref="T:System.Xml.Schema.XmlSchema" /> is in the collection.</summary>
		/// <param name="schema">The <see langword="XmlSchema" /> object. </param>
		/// <returns>
		///     <see langword="true" /> if there is a schema in the collection with the same <see langword="targetNamespace" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600277E RID: 10110 RVA: 0x000DEAFC File Offset: 0x000DCCFC
		public bool Contains(XmlSchema schema)
		{
			if (schema == null)
			{
				throw new ArgumentNullException("schema");
			}
			return this[schema.TargetNamespace] != null;
		}

		/// <summary>Gets a value indicating whether a schema with the specified namespace is in the collection.</summary>
		/// <param name="ns">The namespace URI associated with the schema. For XML Schemas, this will typically be the target namespace. </param>
		/// <returns>
		///     <see langword="true" /> if a schema with the specified namespace is in the collection; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600277F RID: 10111 RVA: 0x000DEB1B File Offset: 0x000DCD1B
		public bool Contains(string ns)
		{
			return this.collection[(ns != null) ? ns : string.Empty] != null;
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.Schema.XmlSchemaCollection.GetEnumerator" />.</summary>
		/// <returns>Returns the <see cref="T:System.Collections.IEnumerator" /> for the collection.</returns>
		// Token: 0x06002780 RID: 10112 RVA: 0x000DEB36 File Offset: 0x000DCD36
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new XmlSchemaCollectionEnumerator(this.collection);
		}

		/// <summary>Provides support for the "for each" style iteration over the collection of schemas.</summary>
		/// <returns>An enumerator for iterating over all schemas in the current collection.</returns>
		// Token: 0x06002781 RID: 10113 RVA: 0x000DEB36 File Offset: 0x000DCD36
		public XmlSchemaCollectionEnumerator GetEnumerator()
		{
			return new XmlSchemaCollectionEnumerator(this.collection);
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.Schema.XmlSchemaCollection.CopyTo(System.Xml.Schema.XmlSchema[],System.Int32)" />.</summary>
		/// <param name="array">The array to copy the objects to. </param>
		/// <param name="index">The index in <paramref name="array" /> where copying will begin. </param>
		// Token: 0x06002782 RID: 10114 RVA: 0x000DEB44 File Offset: 0x000DCD44
		void ICollection.CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			XmlSchemaCollectionEnumerator enumerator = this.GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (index == array.Length && array.IsFixedSize)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				array.SetValue(enumerator.Current, index++);
			}
		}

		/// <summary>Copies all the <see langword="XmlSchema" /> objects from this collection into the given array starting at the given index.</summary>
		/// <param name="array">The array to copy the objects to. </param>
		/// <param name="index">The index in <paramref name="array" /> where copying will begin. </param>
		// Token: 0x06002783 RID: 10115 RVA: 0x000DEBB0 File Offset: 0x000DCDB0
		public void CopyTo(XmlSchema[] array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			XmlSchemaCollectionEnumerator enumerator = this.GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (enumerator.Current != null)
				{
					if (index == array.Length)
					{
						throw new ArgumentOutOfRangeException("index");
					}
					array[index++] = enumerator.Current;
				}
			}
		}

		/// <summary>For a description of this member, see <see cref="P:System.Xml.Schema.XmlSchemaCollection.System#Collections#ICollection#IsSynchronized" />.</summary>
		/// <returns>Returns <see langword="true" /> if the collection is synchronized, otherwise <see langword="false" />.</returns>
		// Token: 0x1700084C RID: 2124
		// (get) Token: 0x06002784 RID: 10116 RVA: 0x000033DE File Offset: 0x000015DE
		bool ICollection.IsSynchronized
		{
			get
			{
				return true;
			}
		}

		/// <summary>For a description of this member, see <see cref="P:System.Xml.Schema.XmlSchemaCollection.System#Collections#ICollection#SyncRoot" />.</summary>
		/// <returns>Returns a <see cref="P:System.Collections.ICollection.SyncRoot" /> object that can be used to synchronize access to the collection.</returns>
		// Token: 0x1700084D RID: 2125
		// (get) Token: 0x06002785 RID: 10117 RVA: 0x00002068 File Offset: 0x00000268
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}

		/// <summary>For a description of this member, see <see cref="P:System.Xml.Schema.XmlSchemaCollection.Count" />.</summary>
		/// <returns>Returns the count of the items in the collection.</returns>
		// Token: 0x1700084E RID: 2126
		// (get) Token: 0x06002786 RID: 10118 RVA: 0x000DE8BB File Offset: 0x000DCABB
		int ICollection.Count
		{
			get
			{
				return this.collection.Count;
			}
		}

		// Token: 0x06002787 RID: 10119 RVA: 0x000DEC14 File Offset: 0x000DCE14
		internal SchemaInfo GetSchemaInfo(string ns)
		{
			XmlSchemaCollectionNode xmlSchemaCollectionNode = (XmlSchemaCollectionNode)this.collection[(ns != null) ? ns : string.Empty];
			if (xmlSchemaCollectionNode == null)
			{
				return null;
			}
			return xmlSchemaCollectionNode.SchemaInfo;
		}

		// Token: 0x06002788 RID: 10120 RVA: 0x000DEC48 File Offset: 0x000DCE48
		internal SchemaNames GetSchemaNames(XmlNameTable nt)
		{
			if (this.nameTable != nt)
			{
				return new SchemaNames(nt);
			}
			if (this.schemaNames == null)
			{
				this.schemaNames = new SchemaNames(this.nameTable);
			}
			return this.schemaNames;
		}

		// Token: 0x06002789 RID: 10121 RVA: 0x000DEC79 File Offset: 0x000DCE79
		internal XmlSchema Add(string ns, SchemaInfo schemaInfo, XmlSchema schema, bool compile)
		{
			return this.Add(ns, schemaInfo, schema, compile, this.xmlResolver);
		}

		// Token: 0x0600278A RID: 10122 RVA: 0x000DEC8C File Offset: 0x000DCE8C
		private XmlSchema Add(string ns, SchemaInfo schemaInfo, XmlSchema schema, bool compile, XmlResolver resolver)
		{
			int num = 0;
			if (schema != null)
			{
				if (schema.ErrorCount == 0 && compile)
				{
					if (!schema.CompileSchema(this, resolver, schemaInfo, ns, this.validationEventHandler, this.nameTable, true))
					{
						num = 1;
					}
					ns = ((schema.TargetNamespace == null) ? string.Empty : schema.TargetNamespace);
				}
				num += schema.ErrorCount;
			}
			else
			{
				num += schemaInfo.ErrorCount;
				ns = this.NameTable.Add(ns);
			}
			if (num == 0)
			{
				this.Add(ns, new XmlSchemaCollectionNode
				{
					NamespaceURI = ns,
					SchemaInfo = schemaInfo,
					Schema = schema
				});
				return schema;
			}
			return null;
		}

		// Token: 0x0600278B RID: 10123 RVA: 0x000DED2C File Offset: 0x000DCF2C
		private void Add(string ns, XmlSchemaCollectionNode node)
		{
			if (this.isThreadSafe)
			{
				this.wLock.AcquireWriterLock(this.timeout);
			}
			try
			{
				if (this.collection[ns] != null)
				{
					this.collection.Remove(ns);
				}
				this.collection.Add(ns, node);
			}
			finally
			{
				if (this.isThreadSafe)
				{
					this.wLock.ReleaseWriterLock();
				}
			}
		}

		// Token: 0x0600278C RID: 10124 RVA: 0x000DEDA0 File Offset: 0x000DCFA0
		private void SendValidationEvent(XmlSchemaException e)
		{
			if (this.validationEventHandler != null)
			{
				this.validationEventHandler(this, new ValidationEventArgs(e));
				return;
			}
			throw e;
		}

		// Token: 0x1700084F RID: 2127
		// (get) Token: 0x0600278D RID: 10125 RVA: 0x000DEDBE File Offset: 0x000DCFBE
		// (set) Token: 0x0600278E RID: 10126 RVA: 0x000DEDC6 File Offset: 0x000DCFC6
		internal ValidationEventHandler EventHandler
		{
			get
			{
				return this.validationEventHandler;
			}
			set
			{
				this.validationEventHandler = value;
			}
		}

		// Token: 0x04001C03 RID: 7171
		private Hashtable collection;

		// Token: 0x04001C04 RID: 7172
		private XmlNameTable nameTable;

		// Token: 0x04001C05 RID: 7173
		private SchemaNames schemaNames;

		// Token: 0x04001C06 RID: 7174
		private ReaderWriterLock wLock;

		// Token: 0x04001C07 RID: 7175
		private int timeout = -1;

		// Token: 0x04001C08 RID: 7176
		private bool isThreadSafe = true;

		// Token: 0x04001C09 RID: 7177
		private ValidationEventHandler validationEventHandler;

		// Token: 0x04001C0A RID: 7178
		private XmlResolver xmlResolver;
	}
}
