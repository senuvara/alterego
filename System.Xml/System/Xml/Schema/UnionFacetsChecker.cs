﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x020003CE RID: 974
	internal class UnionFacetsChecker : FacetsChecker
	{
		// Token: 0x060023FE RID: 9214 RVA: 0x000C22F8 File Offset: 0x000C04F8
		internal override Exception CheckValueFacets(object value, XmlSchemaDatatype datatype)
		{
			RestrictionFacets restriction = datatype.Restriction;
			if ((((restriction != null && restriction.Flags != (RestrictionFlags)0) ? 1 : 0) & 16) != 0 && !this.MatchEnumeration(value, restriction.Enumeration, datatype))
			{
				return new XmlSchemaException("The Enumeration constraint failed.", string.Empty);
			}
			return null;
		}

		// Token: 0x060023FF RID: 9215 RVA: 0x000C2340 File Offset: 0x000C0540
		internal override bool MatchEnumeration(object value, ArrayList enumeration, XmlSchemaDatatype datatype)
		{
			for (int i = 0; i < enumeration.Count; i++)
			{
				if (datatype.Compare(value, enumeration[i]) == 0)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002400 RID: 9216 RVA: 0x000C1A2D File Offset: 0x000BFC2D
		public UnionFacetsChecker()
		{
		}
	}
}
