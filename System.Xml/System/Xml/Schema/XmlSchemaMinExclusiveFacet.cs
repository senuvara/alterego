﻿using System;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="minExclusive" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class can be used to specify a restriction on the minimum value of a <see langword="simpleType" /> element. The element value must be greater than the value of the <see langword="minExclusive" /> element.</summary>
	// Token: 0x02000427 RID: 1063
	public class XmlSchemaMinExclusiveFacet : XmlSchemaFacet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaMinExclusiveFacet" /> class.</summary>
		// Token: 0x0600287B RID: 10363 RVA: 0x000E062A File Offset: 0x000DE82A
		public XmlSchemaMinExclusiveFacet()
		{
			base.FacetType = FacetType.MinExclusive;
		}
	}
}
