﻿using System;
using System.Collections.Generic;

namespace System.Xml.Schema
{
	// Token: 0x020003F2 RID: 1010
	internal sealed class ValidationState
	{
		// Token: 0x06002616 RID: 9750 RVA: 0x000D98F8 File Offset: 0x000D7AF8
		public ValidationState()
		{
		}

		// Token: 0x04001B25 RID: 6949
		public bool IsNill;

		// Token: 0x04001B26 RID: 6950
		public bool IsDefault;

		// Token: 0x04001B27 RID: 6951
		public bool NeedValidateChildren;

		// Token: 0x04001B28 RID: 6952
		public bool CheckRequiredAttribute;

		// Token: 0x04001B29 RID: 6953
		public bool ValidationSkipped;

		// Token: 0x04001B2A RID: 6954
		public int Depth;

		// Token: 0x04001B2B RID: 6955
		public XmlSchemaContentProcessing ProcessContents;

		// Token: 0x04001B2C RID: 6956
		public XmlSchemaValidity Validity;

		// Token: 0x04001B2D RID: 6957
		public SchemaElementDecl ElementDecl;

		// Token: 0x04001B2E RID: 6958
		public SchemaElementDecl ElementDeclBeforeXsi;

		// Token: 0x04001B2F RID: 6959
		public string LocalName;

		// Token: 0x04001B30 RID: 6960
		public string Namespace;

		// Token: 0x04001B31 RID: 6961
		public ConstraintStruct[] Constr;

		// Token: 0x04001B32 RID: 6962
		public StateUnion CurrentState;

		// Token: 0x04001B33 RID: 6963
		public bool HasMatched;

		// Token: 0x04001B34 RID: 6964
		public BitSet[] CurPos = new BitSet[2];

		// Token: 0x04001B35 RID: 6965
		public BitSet AllElementsSet;

		// Token: 0x04001B36 RID: 6966
		public List<RangePositionInfo> RunningPositions;

		// Token: 0x04001B37 RID: 6967
		public bool TooComplex;
	}
}
