﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000394 RID: 916
	internal class Datatype_timeTimeZone : Datatype_dateTimeBase
	{
		// Token: 0x060022A9 RID: 8873 RVA: 0x000BDF20 File Offset: 0x000BC120
		internal Datatype_timeTimeZone() : base(XsdDateTimeFlags.Time)
		{
		}
	}
}
