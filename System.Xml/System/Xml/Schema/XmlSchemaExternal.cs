﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>An abstract class. Provides information about the included schema.</summary>
	// Token: 0x0200041E RID: 1054
	public abstract class XmlSchemaExternal : XmlSchemaObject
	{
		/// <summary>Gets or sets the Uniform Resource Identifier (URI) location for the schema, which tells the schema processor where the schema physically resides.</summary>
		/// <returns>The URI location for the schema.Optional for imported schemas.</returns>
		// Token: 0x170008A5 RID: 2213
		// (get) Token: 0x0600285E RID: 10334 RVA: 0x000E051D File Offset: 0x000DE71D
		// (set) Token: 0x0600285F RID: 10335 RVA: 0x000E0525 File Offset: 0x000DE725
		[XmlAttribute("schemaLocation", DataType = "anyURI")]
		public string SchemaLocation
		{
			get
			{
				return this.location;
			}
			set
			{
				this.location = value;
			}
		}

		/// <summary>Gets or sets the <see langword="XmlSchema" /> for the referenced schema.</summary>
		/// <returns>The <see langword="XmlSchema" /> for the referenced schema.</returns>
		// Token: 0x170008A6 RID: 2214
		// (get) Token: 0x06002860 RID: 10336 RVA: 0x000E052E File Offset: 0x000DE72E
		// (set) Token: 0x06002861 RID: 10337 RVA: 0x000E0536 File Offset: 0x000DE736
		[XmlIgnore]
		public XmlSchema Schema
		{
			get
			{
				return this.schema;
			}
			set
			{
				this.schema = value;
			}
		}

		/// <summary>Gets or sets the string id.</summary>
		/// <returns>The string id. The default is <see langword="String.Empty" />.Optional.</returns>
		// Token: 0x170008A7 RID: 2215
		// (get) Token: 0x06002862 RID: 10338 RVA: 0x000E053F File Offset: 0x000DE73F
		// (set) Token: 0x06002863 RID: 10339 RVA: 0x000E0547 File Offset: 0x000DE747
		[XmlAttribute("id", DataType = "ID")]
		public string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}

		/// <summary>Gets and sets the qualified attributes, which do not belong to the schema target namespace.</summary>
		/// <returns>Qualified attributes that belong to another target namespace.</returns>
		// Token: 0x170008A8 RID: 2216
		// (get) Token: 0x06002864 RID: 10340 RVA: 0x000E0550 File Offset: 0x000DE750
		// (set) Token: 0x06002865 RID: 10341 RVA: 0x000E0558 File Offset: 0x000DE758
		[XmlAnyAttribute]
		public XmlAttribute[] UnhandledAttributes
		{
			get
			{
				return this.moreAttributes;
			}
			set
			{
				this.moreAttributes = value;
			}
		}

		// Token: 0x170008A9 RID: 2217
		// (get) Token: 0x06002866 RID: 10342 RVA: 0x000E0561 File Offset: 0x000DE761
		// (set) Token: 0x06002867 RID: 10343 RVA: 0x000E0569 File Offset: 0x000DE769
		[XmlIgnore]
		internal Uri BaseUri
		{
			get
			{
				return this.baseUri;
			}
			set
			{
				this.baseUri = value;
			}
		}

		// Token: 0x170008AA RID: 2218
		// (get) Token: 0x06002868 RID: 10344 RVA: 0x000E0572 File Offset: 0x000DE772
		// (set) Token: 0x06002869 RID: 10345 RVA: 0x000E057A File Offset: 0x000DE77A
		[XmlIgnore]
		internal override string IdAttribute
		{
			get
			{
				return this.Id;
			}
			set
			{
				this.Id = value;
			}
		}

		// Token: 0x0600286A RID: 10346 RVA: 0x000E0558 File Offset: 0x000DE758
		internal override void SetUnhandledAttributes(XmlAttribute[] moreAttributes)
		{
			this.moreAttributes = moreAttributes;
		}

		// Token: 0x170008AB RID: 2219
		// (get) Token: 0x0600286B RID: 10347 RVA: 0x000E0583 File Offset: 0x000DE783
		// (set) Token: 0x0600286C RID: 10348 RVA: 0x000E058B File Offset: 0x000DE78B
		internal Compositor Compositor
		{
			get
			{
				return this.compositor;
			}
			set
			{
				this.compositor = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaExternal" /> class.</summary>
		// Token: 0x0600286D RID: 10349 RVA: 0x000DE196 File Offset: 0x000DC396
		protected XmlSchemaExternal()
		{
		}

		// Token: 0x04001C5F RID: 7263
		private string location;

		// Token: 0x04001C60 RID: 7264
		private Uri baseUri;

		// Token: 0x04001C61 RID: 7265
		private XmlSchema schema;

		// Token: 0x04001C62 RID: 7266
		private string id;

		// Token: 0x04001C63 RID: 7267
		private XmlAttribute[] moreAttributes;

		// Token: 0x04001C64 RID: 7268
		private Compositor compositor;
	}
}
