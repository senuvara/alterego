﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200039A RID: 922
	internal class Datatype_day : Datatype_dateTimeBase
	{
		// Token: 0x17000703 RID: 1795
		// (get) Token: 0x060022B4 RID: 8884 RVA: 0x000BD637 File Offset: 0x000BB837
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.GDay;
			}
		}

		// Token: 0x060022B5 RID: 8885 RVA: 0x000BDF5F File Offset: 0x000BC15F
		internal Datatype_day() : base(XsdDateTimeFlags.GDay)
		{
		}
	}
}
