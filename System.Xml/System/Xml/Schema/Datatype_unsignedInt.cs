﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003B5 RID: 949
	internal class Datatype_unsignedInt : Datatype_unsignedLong
	{
		// Token: 0x17000757 RID: 1879
		// (get) Token: 0x0600234A RID: 9034 RVA: 0x000BE8BD File Offset: 0x000BCABD
		internal override FacetsChecker FacetsChecker
		{
			get
			{
				return Datatype_unsignedInt.numeric10FacetsChecker;
			}
		}

		// Token: 0x17000758 RID: 1880
		// (get) Token: 0x0600234B RID: 9035 RVA: 0x000BE8C4 File Offset: 0x000BCAC4
		public override XmlTypeCode TypeCode
		{
			get
			{
				return XmlTypeCode.UnsignedInt;
			}
		}

		// Token: 0x0600234C RID: 9036 RVA: 0x000BE8C8 File Offset: 0x000BCAC8
		internal override int Compare(object value1, object value2)
		{
			return ((uint)value1).CompareTo(value2);
		}

		// Token: 0x17000759 RID: 1881
		// (get) Token: 0x0600234D RID: 9037 RVA: 0x000BE8E4 File Offset: 0x000BCAE4
		public override Type ValueType
		{
			get
			{
				return Datatype_unsignedInt.atomicValueType;
			}
		}

		// Token: 0x1700075A RID: 1882
		// (get) Token: 0x0600234E RID: 9038 RVA: 0x000BE8EB File Offset: 0x000BCAEB
		internal override Type ListValueType
		{
			get
			{
				return Datatype_unsignedInt.listValueType;
			}
		}

		// Token: 0x0600234F RID: 9039 RVA: 0x000BE8F4 File Offset: 0x000BCAF4
		internal override Exception TryParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr, out object typedValue)
		{
			typedValue = null;
			Exception ex = Datatype_unsignedInt.numeric10FacetsChecker.CheckLexicalFacets(ref s, this);
			if (ex == null)
			{
				uint num;
				ex = XmlConvert.TryToUInt32(s, out num);
				if (ex == null)
				{
					ex = Datatype_unsignedInt.numeric10FacetsChecker.CheckValueFacets((long)((ulong)num), this);
					if (ex == null)
					{
						typedValue = num;
						return null;
					}
				}
			}
			return ex;
		}

		// Token: 0x06002350 RID: 9040 RVA: 0x000BE93F File Offset: 0x000BCB3F
		public Datatype_unsignedInt()
		{
		}

		// Token: 0x06002351 RID: 9041 RVA: 0x000BE947 File Offset: 0x000BCB47
		// Note: this type is marked as 'beforefieldinit'.
		static Datatype_unsignedInt()
		{
		}

		// Token: 0x040018D0 RID: 6352
		private static readonly Type atomicValueType = typeof(uint);

		// Token: 0x040018D1 RID: 6353
		private static readonly Type listValueType = typeof(uint[]);

		// Token: 0x040018D2 RID: 6354
		private static readonly FacetsChecker numeric10FacetsChecker = new Numeric10FacetsChecker(0m, 4294967295m);
	}
}
