﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Represents the <see langword="element" /> element from XML Schema as specified by the World Wide Web Consortium (W3C). This class is the base class for all particle types and is used to describe an element in an XML document.</summary>
	// Token: 0x0200041C RID: 1052
	public class XmlSchemaElement : XmlSchemaParticle
	{
		/// <summary>Gets or sets information to indicate if the element can be used in an instance document.</summary>
		/// <returns>If <see langword="true" />, the element cannot appear in the instance document. The default is <see langword="false" />.Optional.</returns>
		// Token: 0x17000884 RID: 2180
		// (get) Token: 0x06002812 RID: 10258 RVA: 0x000DFEA8 File Offset: 0x000DE0A8
		// (set) Token: 0x06002813 RID: 10259 RVA: 0x000DFEB0 File Offset: 0x000DE0B0
		[XmlAttribute("abstract")]
		[DefaultValue(false)]
		public bool IsAbstract
		{
			get
			{
				return this.isAbstract;
			}
			set
			{
				this.isAbstract = value;
				this.hasAbstractAttribute = true;
			}
		}

		/// <summary>Gets or sets a <see langword="Block" /> derivation.</summary>
		/// <returns>The attribute used to block a type derivation. Default value is <see langword="XmlSchemaDerivationMethod.None" />.Optional.</returns>
		// Token: 0x17000885 RID: 2181
		// (get) Token: 0x06002814 RID: 10260 RVA: 0x000DFEC0 File Offset: 0x000DE0C0
		// (set) Token: 0x06002815 RID: 10261 RVA: 0x000DFEC8 File Offset: 0x000DE0C8
		[DefaultValue(XmlSchemaDerivationMethod.None)]
		[XmlAttribute("block")]
		public XmlSchemaDerivationMethod Block
		{
			get
			{
				return this.block;
			}
			set
			{
				this.block = value;
			}
		}

		/// <summary>Gets or sets the default value of the element if its content is a simple type or content of the element is <see langword="textOnly" />.</summary>
		/// <returns>The default value for the element. The default is a null reference.Optional.</returns>
		// Token: 0x17000886 RID: 2182
		// (get) Token: 0x06002816 RID: 10262 RVA: 0x000DFED1 File Offset: 0x000DE0D1
		// (set) Token: 0x06002817 RID: 10263 RVA: 0x000DFED9 File Offset: 0x000DE0D9
		[DefaultValue(null)]
		[XmlAttribute("default")]
		public string DefaultValue
		{
			get
			{
				return this.defaultValue;
			}
			set
			{
				this.defaultValue = value;
			}
		}

		/// <summary>Gets or sets the <see langword="Final" /> property to indicate that no further derivations are allowed.</summary>
		/// <returns>The <see langword="Final" /> property. The default is <see langword="XmlSchemaDerivationMethod.None" />.Optional.</returns>
		// Token: 0x17000887 RID: 2183
		// (get) Token: 0x06002818 RID: 10264 RVA: 0x000DFEE2 File Offset: 0x000DE0E2
		// (set) Token: 0x06002819 RID: 10265 RVA: 0x000DFEEA File Offset: 0x000DE0EA
		[DefaultValue(XmlSchemaDerivationMethod.None)]
		[XmlAttribute("final")]
		public XmlSchemaDerivationMethod Final
		{
			get
			{
				return this.final;
			}
			set
			{
				this.final = value;
			}
		}

		/// <summary>Gets or sets the fixed value.</summary>
		/// <returns>The fixed value that is predetermined and unchangeable. The default is a null reference.Optional.</returns>
		// Token: 0x17000888 RID: 2184
		// (get) Token: 0x0600281A RID: 10266 RVA: 0x000DFEF3 File Offset: 0x000DE0F3
		// (set) Token: 0x0600281B RID: 10267 RVA: 0x000DFEFB File Offset: 0x000DE0FB
		[DefaultValue(null)]
		[XmlAttribute("fixed")]
		public string FixedValue
		{
			get
			{
				return this.fixedValue;
			}
			set
			{
				this.fixedValue = value;
			}
		}

		/// <summary>Gets or sets the form for the element.</summary>
		/// <returns>The form for the element. The default is the <see cref="P:System.Xml.Schema.XmlSchema.ElementFormDefault" /> value.Optional.</returns>
		// Token: 0x17000889 RID: 2185
		// (get) Token: 0x0600281C RID: 10268 RVA: 0x000DFF04 File Offset: 0x000DE104
		// (set) Token: 0x0600281D RID: 10269 RVA: 0x000DFF0C File Offset: 0x000DE10C
		[XmlAttribute("form")]
		[DefaultValue(XmlSchemaForm.None)]
		public XmlSchemaForm Form
		{
			get
			{
				return this.form;
			}
			set
			{
				this.form = value;
			}
		}

		/// <summary>Gets or sets the name of the element.</summary>
		/// <returns>The name of the element. The default is <see langword="String.Empty" />.</returns>
		// Token: 0x1700088A RID: 2186
		// (get) Token: 0x0600281E RID: 10270 RVA: 0x000DFF15 File Offset: 0x000DE115
		// (set) Token: 0x0600281F RID: 10271 RVA: 0x000DFF1D File Offset: 0x000DE11D
		[XmlAttribute("name")]
		[DefaultValue("")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets information that indicates if <see langword="xsi:nil" /> can occur in the instance data. Indicates if an explicit nil value can be assigned to the element.</summary>
		/// <returns>If nillable is <see langword="true" />, this enables an instance of the element to have the <see langword="nil" /> attribute set to <see langword="true" />. The <see langword="nil" /> attribute is defined as part of the XML Schema namespace for instances. The default is <see langword="false" />.Optional.</returns>
		// Token: 0x1700088B RID: 2187
		// (get) Token: 0x06002820 RID: 10272 RVA: 0x000DFF26 File Offset: 0x000DE126
		// (set) Token: 0x06002821 RID: 10273 RVA: 0x000DFF2E File Offset: 0x000DE12E
		[XmlAttribute("nillable")]
		[DefaultValue(false)]
		public bool IsNillable
		{
			get
			{
				return this.isNillable;
			}
			set
			{
				this.isNillable = value;
				this.hasNillableAttribute = true;
			}
		}

		// Token: 0x1700088C RID: 2188
		// (get) Token: 0x06002822 RID: 10274 RVA: 0x000DFF3E File Offset: 0x000DE13E
		[XmlIgnore]
		internal bool HasNillableAttribute
		{
			get
			{
				return this.hasNillableAttribute;
			}
		}

		// Token: 0x1700088D RID: 2189
		// (get) Token: 0x06002823 RID: 10275 RVA: 0x000DFF46 File Offset: 0x000DE146
		[XmlIgnore]
		internal bool HasAbstractAttribute
		{
			get
			{
				return this.hasAbstractAttribute;
			}
		}

		/// <summary>Gets or sets the reference name of an element declared in this schema (or another schema indicated by the specified namespace).</summary>
		/// <returns>The reference name of the element.</returns>
		// Token: 0x1700088E RID: 2190
		// (get) Token: 0x06002824 RID: 10276 RVA: 0x000DFF4E File Offset: 0x000DE14E
		// (set) Token: 0x06002825 RID: 10277 RVA: 0x000DFF56 File Offset: 0x000DE156
		[XmlAttribute("ref")]
		public XmlQualifiedName RefName
		{
			get
			{
				return this.refName;
			}
			set
			{
				this.refName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets or sets the name of an element that is being substituted by this element.</summary>
		/// <returns>The qualified name of an element that is being substituted by this element.Optional.</returns>
		// Token: 0x1700088F RID: 2191
		// (get) Token: 0x06002826 RID: 10278 RVA: 0x000DFF6F File Offset: 0x000DE16F
		// (set) Token: 0x06002827 RID: 10279 RVA: 0x000DFF77 File Offset: 0x000DE177
		[XmlAttribute("substitutionGroup")]
		public XmlQualifiedName SubstitutionGroup
		{
			get
			{
				return this.substitutionGroup;
			}
			set
			{
				this.substitutionGroup = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets or sets the name of a built-in data type defined in this schema or another schema indicated by the specified namespace.</summary>
		/// <returns>The name of the built-in data type.</returns>
		// Token: 0x17000890 RID: 2192
		// (get) Token: 0x06002828 RID: 10280 RVA: 0x000DFF90 File Offset: 0x000DE190
		// (set) Token: 0x06002829 RID: 10281 RVA: 0x000DFF98 File Offset: 0x000DE198
		[XmlAttribute("type")]
		public XmlQualifiedName SchemaTypeName
		{
			get
			{
				return this.typeName;
			}
			set
			{
				this.typeName = ((value == null) ? XmlQualifiedName.Empty : value);
			}
		}

		/// <summary>Gets or sets the type of the element. This can either be a complex type or a simple type.</summary>
		/// <returns>The type of the element.</returns>
		// Token: 0x17000891 RID: 2193
		// (get) Token: 0x0600282A RID: 10282 RVA: 0x000DFFB1 File Offset: 0x000DE1B1
		// (set) Token: 0x0600282B RID: 10283 RVA: 0x000DFFB9 File Offset: 0x000DE1B9
		[XmlElement("complexType", typeof(XmlSchemaComplexType))]
		[XmlElement("simpleType", typeof(XmlSchemaSimpleType))]
		public XmlSchemaType SchemaType
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		/// <summary>Gets the collection of constraints on the element.</summary>
		/// <returns>The collection of constraints.</returns>
		// Token: 0x17000892 RID: 2194
		// (get) Token: 0x0600282C RID: 10284 RVA: 0x000DFFC2 File Offset: 0x000DE1C2
		[XmlElement("keyref", typeof(XmlSchemaKeyref))]
		[XmlElement("unique", typeof(XmlSchemaUnique))]
		[XmlElement("key", typeof(XmlSchemaKey))]
		public XmlSchemaObjectCollection Constraints
		{
			get
			{
				if (this.constraints == null)
				{
					this.constraints = new XmlSchemaObjectCollection();
				}
				return this.constraints;
			}
		}

		/// <summary>Gets the actual qualified name for the given element. </summary>
		/// <returns>The qualified name of the element. The post-compilation value of the <see langword="QualifiedName" /> property.</returns>
		// Token: 0x17000893 RID: 2195
		// (get) Token: 0x0600282D RID: 10285 RVA: 0x000DFFDD File Offset: 0x000DE1DD
		[XmlIgnore]
		public XmlQualifiedName QualifiedName
		{
			get
			{
				return this.qualifiedName;
			}
		}

		/// <summary>Gets a common language runtime (CLR) object based on the <see cref="T:System.Xml.Schema.XmlSchemaElement" /> or <see cref="T:System.Xml.Schema.XmlSchemaElement" /> of the element, which holds the post-compilation value of the <see langword="ElementType" /> property.</summary>
		/// <returns>The common language runtime object. The post-compilation value of the <see langword="ElementType" /> property.</returns>
		// Token: 0x17000894 RID: 2196
		// (get) Token: 0x0600282E RID: 10286 RVA: 0x000DFFE5 File Offset: 0x000DE1E5
		[XmlIgnore]
		[Obsolete("This property has been deprecated. Please use ElementSchemaType property that returns a strongly typed element type. http://go.microsoft.com/fwlink/?linkid=14202")]
		public object ElementType
		{
			get
			{
				if (this.elementType == null)
				{
					return null;
				}
				if (this.elementType.QualifiedName.Namespace == "http://www.w3.org/2001/XMLSchema")
				{
					return this.elementType.Datatype;
				}
				return this.elementType;
			}
		}

		/// <summary>Gets an <see cref="T:System.Xml.Schema.XmlSchemaType" /> object representing the type of the element based on the <see cref="P:System.Xml.Schema.XmlSchemaElement.SchemaType" /> or <see cref="P:System.Xml.Schema.XmlSchemaElement.SchemaTypeName" /> values of the element.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaType" /> object.</returns>
		// Token: 0x17000895 RID: 2197
		// (get) Token: 0x0600282F RID: 10287 RVA: 0x000E001F File Offset: 0x000DE21F
		[XmlIgnore]
		public XmlSchemaType ElementSchemaType
		{
			get
			{
				return this.elementType;
			}
		}

		/// <summary>Gets the post-compilation value of the <see langword="Block" /> property.</summary>
		/// <returns>The post-compilation value of the <see langword="Block" /> property. The default is the <see langword="BlockDefault" /> value on the <see langword="schema" /> element.</returns>
		// Token: 0x17000896 RID: 2198
		// (get) Token: 0x06002830 RID: 10288 RVA: 0x000E0027 File Offset: 0x000DE227
		[XmlIgnore]
		public XmlSchemaDerivationMethod BlockResolved
		{
			get
			{
				return this.blockResolved;
			}
		}

		/// <summary>Gets the post-compilation value of the <see langword="Final" /> property.</summary>
		/// <returns>The post-compilation value of the <see langword="Final" /> property. Default value is the <see langword="FinalDefault" /> value on the <see langword="schema" /> element.</returns>
		// Token: 0x17000897 RID: 2199
		// (get) Token: 0x06002831 RID: 10289 RVA: 0x000E002F File Offset: 0x000DE22F
		[XmlIgnore]
		public XmlSchemaDerivationMethod FinalResolved
		{
			get
			{
				return this.finalResolved;
			}
		}

		// Token: 0x06002832 RID: 10290 RVA: 0x000E0038 File Offset: 0x000DE238
		internal XmlReader Validate(XmlReader reader, XmlResolver resolver, XmlSchemaSet schemaSet, ValidationEventHandler valEventHandler)
		{
			if (schemaSet != null)
			{
				XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
				xmlReaderSettings.ValidationType = ValidationType.Schema;
				xmlReaderSettings.Schemas = schemaSet;
				xmlReaderSettings.ValidationEventHandler += valEventHandler;
				return new XsdValidatingReader(reader, resolver, xmlReaderSettings, this);
			}
			return null;
		}

		// Token: 0x06002833 RID: 10291 RVA: 0x000E006F File Offset: 0x000DE26F
		internal void SetQualifiedName(XmlQualifiedName value)
		{
			this.qualifiedName = value;
		}

		// Token: 0x06002834 RID: 10292 RVA: 0x000E0078 File Offset: 0x000DE278
		internal void SetElementType(XmlSchemaType value)
		{
			this.elementType = value;
		}

		// Token: 0x06002835 RID: 10293 RVA: 0x000E0081 File Offset: 0x000DE281
		internal void SetBlockResolved(XmlSchemaDerivationMethod value)
		{
			this.blockResolved = value;
		}

		// Token: 0x06002836 RID: 10294 RVA: 0x000E008A File Offset: 0x000DE28A
		internal void SetFinalResolved(XmlSchemaDerivationMethod value)
		{
			this.finalResolved = value;
		}

		// Token: 0x17000898 RID: 2200
		// (get) Token: 0x06002837 RID: 10295 RVA: 0x000E0093 File Offset: 0x000DE293
		[XmlIgnore]
		internal bool HasDefault
		{
			get
			{
				return this.defaultValue != null && this.defaultValue.Length > 0;
			}
		}

		// Token: 0x17000899 RID: 2201
		// (get) Token: 0x06002838 RID: 10296 RVA: 0x000E00AD File Offset: 0x000DE2AD
		internal bool HasConstraints
		{
			get
			{
				return this.constraints != null && this.constraints.Count > 0;
			}
		}

		// Token: 0x1700089A RID: 2202
		// (get) Token: 0x06002839 RID: 10297 RVA: 0x000E00C7 File Offset: 0x000DE2C7
		// (set) Token: 0x0600283A RID: 10298 RVA: 0x000E00CF File Offset: 0x000DE2CF
		internal bool IsLocalTypeDerivationChecked
		{
			get
			{
				return this.isLocalTypeDerivationChecked;
			}
			set
			{
				this.isLocalTypeDerivationChecked = value;
			}
		}

		// Token: 0x1700089B RID: 2203
		// (get) Token: 0x0600283B RID: 10299 RVA: 0x000E00D8 File Offset: 0x000DE2D8
		// (set) Token: 0x0600283C RID: 10300 RVA: 0x000E00E0 File Offset: 0x000DE2E0
		internal SchemaElementDecl ElementDecl
		{
			get
			{
				return this.elementDecl;
			}
			set
			{
				this.elementDecl = value;
			}
		}

		// Token: 0x1700089C RID: 2204
		// (get) Token: 0x0600283D RID: 10301 RVA: 0x000E00E9 File Offset: 0x000DE2E9
		// (set) Token: 0x0600283E RID: 10302 RVA: 0x000E00F1 File Offset: 0x000DE2F1
		[XmlIgnore]
		internal override string NameAttribute
		{
			get
			{
				return this.Name;
			}
			set
			{
				this.Name = value;
			}
		}

		// Token: 0x1700089D RID: 2205
		// (get) Token: 0x0600283F RID: 10303 RVA: 0x000E00FA File Offset: 0x000DE2FA
		[XmlIgnore]
		internal override string NameString
		{
			get
			{
				return this.qualifiedName.ToString();
			}
		}

		// Token: 0x06002840 RID: 10304 RVA: 0x000E0107 File Offset: 0x000DE307
		internal override XmlSchemaObject Clone()
		{
			return this.Clone(null);
		}

		// Token: 0x06002841 RID: 10305 RVA: 0x000E0110 File Offset: 0x000DE310
		internal XmlSchemaObject Clone(XmlSchema parentSchema)
		{
			XmlSchemaElement xmlSchemaElement = (XmlSchemaElement)base.MemberwiseClone();
			xmlSchemaElement.refName = this.refName.Clone();
			xmlSchemaElement.substitutionGroup = this.substitutionGroup.Clone();
			xmlSchemaElement.typeName = this.typeName.Clone();
			xmlSchemaElement.qualifiedName = this.qualifiedName.Clone();
			XmlSchemaComplexType xmlSchemaComplexType = this.type as XmlSchemaComplexType;
			if (xmlSchemaComplexType != null && xmlSchemaComplexType.QualifiedName.IsEmpty)
			{
				xmlSchemaElement.type = (XmlSchemaType)xmlSchemaComplexType.Clone(parentSchema);
			}
			xmlSchemaElement.constraints = null;
			return xmlSchemaElement;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaElement" /> class.</summary>
		// Token: 0x06002842 RID: 10306 RVA: 0x000E01A4 File Offset: 0x000DE3A4
		public XmlSchemaElement()
		{
		}

		// Token: 0x04001C43 RID: 7235
		private bool isAbstract;

		// Token: 0x04001C44 RID: 7236
		private bool hasAbstractAttribute;

		// Token: 0x04001C45 RID: 7237
		private bool isNillable;

		// Token: 0x04001C46 RID: 7238
		private bool hasNillableAttribute;

		// Token: 0x04001C47 RID: 7239
		private bool isLocalTypeDerivationChecked;

		// Token: 0x04001C48 RID: 7240
		private XmlSchemaDerivationMethod block = XmlSchemaDerivationMethod.None;

		// Token: 0x04001C49 RID: 7241
		private XmlSchemaDerivationMethod final = XmlSchemaDerivationMethod.None;

		// Token: 0x04001C4A RID: 7242
		private XmlSchemaForm form;

		// Token: 0x04001C4B RID: 7243
		private string defaultValue;

		// Token: 0x04001C4C RID: 7244
		private string fixedValue;

		// Token: 0x04001C4D RID: 7245
		private string name;

		// Token: 0x04001C4E RID: 7246
		private XmlQualifiedName refName = XmlQualifiedName.Empty;

		// Token: 0x04001C4F RID: 7247
		private XmlQualifiedName substitutionGroup = XmlQualifiedName.Empty;

		// Token: 0x04001C50 RID: 7248
		private XmlQualifiedName typeName = XmlQualifiedName.Empty;

		// Token: 0x04001C51 RID: 7249
		private XmlSchemaType type;

		// Token: 0x04001C52 RID: 7250
		private XmlQualifiedName qualifiedName = XmlQualifiedName.Empty;

		// Token: 0x04001C53 RID: 7251
		private XmlSchemaType elementType;

		// Token: 0x04001C54 RID: 7252
		private XmlSchemaDerivationMethod blockResolved;

		// Token: 0x04001C55 RID: 7253
		private XmlSchemaDerivationMethod finalResolved;

		// Token: 0x04001C56 RID: 7254
		private XmlSchemaObjectCollection constraints;

		// Token: 0x04001C57 RID: 7255
		private SchemaElementDecl elementDecl;
	}
}
