﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003EE RID: 1006
	internal enum SchemaType
	{
		// Token: 0x04001B1B RID: 6939
		None,
		// Token: 0x04001B1C RID: 6940
		DTD,
		// Token: 0x04001B1D RID: 6941
		XDR,
		// Token: 0x04001B1E RID: 6942
		XSD
	}
}
