﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x02000352 RID: 850
	internal class AxisStack
	{
		// Token: 0x17000658 RID: 1624
		// (get) Token: 0x060020B9 RID: 8377 RVA: 0x000B7395 File Offset: 0x000B5595
		internal ForwardAxis Subtree
		{
			get
			{
				return this.subtree;
			}
		}

		// Token: 0x17000659 RID: 1625
		// (get) Token: 0x060020BA RID: 8378 RVA: 0x000B739D File Offset: 0x000B559D
		internal int Length
		{
			get
			{
				return this.stack.Count;
			}
		}

		// Token: 0x060020BB RID: 8379 RVA: 0x000B73AA File Offset: 0x000B55AA
		public AxisStack(ForwardAxis faxis, ActiveAxis parent)
		{
			this.subtree = faxis;
			this.stack = new ArrayList();
			this.parent = parent;
			if (!faxis.IsDss)
			{
				this.Push(1);
			}
		}

		// Token: 0x060020BC RID: 8380 RVA: 0x000B73DC File Offset: 0x000B55DC
		internal void Push(int depth)
		{
			AxisElement value = new AxisElement(this.subtree.RootNode, depth);
			this.stack.Add(value);
		}

		// Token: 0x060020BD RID: 8381 RVA: 0x000B7408 File Offset: 0x000B5608
		internal void Pop()
		{
			this.stack.RemoveAt(this.Length - 1);
		}

		// Token: 0x060020BE RID: 8382 RVA: 0x000B741D File Offset: 0x000B561D
		internal static bool Equal(string thisname, string thisURN, string name, string URN)
		{
			if (thisURN == null)
			{
				if (URN != null && URN.Length != 0)
				{
					return false;
				}
			}
			else if (thisURN.Length != 0 && thisURN != URN)
			{
				return false;
			}
			return thisname.Length == 0 || !(thisname != name);
		}

		// Token: 0x060020BF RID: 8383 RVA: 0x000B7458 File Offset: 0x000B5658
		internal void MoveToParent(string name, string URN, int depth)
		{
			if (this.subtree.IsSelfAxis)
			{
				return;
			}
			for (int i = 0; i < this.stack.Count; i++)
			{
				((AxisElement)this.stack[i]).MoveToParent(depth, this.subtree);
			}
			if (this.subtree.IsDss && AxisStack.Equal(this.subtree.RootNode.Name, this.subtree.RootNode.Urn, name, URN))
			{
				this.Pop();
			}
		}

		// Token: 0x060020C0 RID: 8384 RVA: 0x000B74E4 File Offset: 0x000B56E4
		internal bool MoveToChild(string name, string URN, int depth)
		{
			bool result = false;
			if (this.subtree.IsDss && AxisStack.Equal(this.subtree.RootNode.Name, this.subtree.RootNode.Urn, name, URN))
			{
				this.Push(-1);
			}
			for (int i = 0; i < this.stack.Count; i++)
			{
				if (((AxisElement)this.stack[i]).MoveToChild(name, URN, depth, this.subtree))
				{
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060020C1 RID: 8385 RVA: 0x000B756C File Offset: 0x000B576C
		internal bool MoveToAttribute(string name, string URN, int depth)
		{
			if (!this.subtree.IsAttribute)
			{
				return false;
			}
			if (!AxisStack.Equal(this.subtree.TopNode.Name, this.subtree.TopNode.Urn, name, URN))
			{
				return false;
			}
			bool result = false;
			if (this.subtree.TopNode.Input == null)
			{
				return this.subtree.IsDss || depth == 1;
			}
			for (int i = 0; i < this.stack.Count; i++)
			{
				AxisElement axisElement = (AxisElement)this.stack[i];
				if (axisElement.isMatch && axisElement.CurNode == this.subtree.TopNode.Input)
				{
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0400178C RID: 6028
		private ArrayList stack;

		// Token: 0x0400178D RID: 6029
		private ForwardAxis subtree;

		// Token: 0x0400178E RID: 6030
		private ActiveAxis parent;
	}
}
