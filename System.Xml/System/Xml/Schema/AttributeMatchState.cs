﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020003E6 RID: 998
	internal enum AttributeMatchState
	{
		// Token: 0x040019E1 RID: 6625
		AttributeFound,
		// Token: 0x040019E2 RID: 6626
		AnyIdAttributeFound,
		// Token: 0x040019E3 RID: 6627
		UndeclaredElementAndAttribute,
		// Token: 0x040019E4 RID: 6628
		UndeclaredAttribute,
		// Token: 0x040019E5 RID: 6629
		AnyAttributeLax,
		// Token: 0x040019E6 RID: 6630
		AnyAttributeSkip,
		// Token: 0x040019E7 RID: 6631
		ProhibitedAnyAttribute,
		// Token: 0x040019E8 RID: 6632
		ProhibitedAttribute,
		// Token: 0x040019E9 RID: 6633
		AttributeNameMismatch,
		// Token: 0x040019EA RID: 6634
		ValidateAttributeInvalidCall
	}
}
