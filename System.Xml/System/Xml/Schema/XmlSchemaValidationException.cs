﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Xml.Schema
{
	/// <summary>Represents the exception thrown when XML Schema Definition Language (XSD) schema validation errors and warnings are encountered in an XML document being validated. </summary>
	// Token: 0x02000457 RID: 1111
	[Serializable]
	public class XmlSchemaValidationException : XmlSchemaException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaValidationException" /> class with the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" /> objects specified.</summary>
		/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object.</param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> object.</param>
		// Token: 0x060029DE RID: 10718 RVA: 0x000C5DDF File Offset: 0x000C3FDF
		protected XmlSchemaValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>Constructs a new <see cref="T:System.Xml.Schema.XmlSchemaValidationException" /> object with the given <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" /> information that contains all the properties of the <see cref="T:System.Xml.Schema.XmlSchemaValidationException" />.</summary>
		/// <param name="info">
		///       <see cref="T:System.Runtime.Serialization.SerializationInfo" />
		///     </param>
		/// <param name="context">
		///       <see cref="T:System.Runtime.Serialization.StreamingContext" />
		///     </param>
		// Token: 0x060029DF RID: 10719 RVA: 0x000C5DE9 File Offset: 0x000C3FE9
		[SecurityPermission(SecurityAction.LinkDemand, SerializationFormatter = true)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaValidationException" /> class.</summary>
		// Token: 0x060029E0 RID: 10720 RVA: 0x000C5DF3 File Offset: 0x000C3FF3
		public XmlSchemaValidationException() : base(null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaValidationException" /> class with the exception message specified.</summary>
		/// <param name="message">A <see langword="string" /> description of the error condition.</param>
		// Token: 0x060029E1 RID: 10721 RVA: 0x000C5DFC File Offset: 0x000C3FFC
		public XmlSchemaValidationException(string message) : base(message, null, 0, 0)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaValidationException" /> class with the exception message and original <see cref="T:System.Exception" /> object that caused this exception specified.</summary>
		/// <param name="message">A <see langword="string" /> description of the error condition.</param>
		/// <param name="innerException">The original <see cref="T:System.Exception" /> object that caused this exception.</param>
		// Token: 0x060029E2 RID: 10722 RVA: 0x000C5E08 File Offset: 0x000C4008
		public XmlSchemaValidationException(string message, Exception innerException) : base(message, innerException, 0, 0)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaValidationException" /> class with the exception message specified, and the original <see cref="T:System.Exception" /> object, line number, and line position of the XML that cause this exception specified.</summary>
		/// <param name="message">A <see langword="string" /> description of the error condition.</param>
		/// <param name="innerException">The original <see cref="T:System.Exception" /> object that caused this exception.</param>
		/// <param name="lineNumber">The line number of the XML that caused this exception.</param>
		/// <param name="linePosition">The line position of the XML that caused this exception.</param>
		// Token: 0x060029E3 RID: 10723 RVA: 0x000C5E14 File Offset: 0x000C4014
		public XmlSchemaValidationException(string message, Exception innerException, int lineNumber, int linePosition) : base(message, innerException, lineNumber, linePosition)
		{
		}

		// Token: 0x060029E4 RID: 10724 RVA: 0x000C5E21 File Offset: 0x000C4021
		internal XmlSchemaValidationException(string res, string[] args) : base(res, args, null, null, 0, 0, null)
		{
		}

		// Token: 0x060029E5 RID: 10725 RVA: 0x000C5E30 File Offset: 0x000C4030
		internal XmlSchemaValidationException(string res, string arg) : base(res, new string[]
		{
			arg
		}, null, null, 0, 0, null)
		{
		}

		// Token: 0x060029E6 RID: 10726 RVA: 0x000C5E48 File Offset: 0x000C4048
		internal XmlSchemaValidationException(string res, string arg, string sourceUri, int lineNumber, int linePosition) : base(res, new string[]
		{
			arg
		}, null, sourceUri, lineNumber, linePosition, null)
		{
		}

		// Token: 0x060029E7 RID: 10727 RVA: 0x000C5E62 File Offset: 0x000C4062
		internal XmlSchemaValidationException(string res, string sourceUri, int lineNumber, int linePosition) : base(res, null, null, sourceUri, lineNumber, linePosition, null)
		{
		}

		// Token: 0x060029E8 RID: 10728 RVA: 0x000C5E72 File Offset: 0x000C4072
		internal XmlSchemaValidationException(string res, string[] args, string sourceUri, int lineNumber, int linePosition) : base(res, args, null, sourceUri, lineNumber, linePosition, null)
		{
		}

		// Token: 0x060029E9 RID: 10729 RVA: 0x000E3AEF File Offset: 0x000E1CEF
		internal XmlSchemaValidationException(string res, string[] args, Exception innerException, string sourceUri, int lineNumber, int linePosition) : base(res, args, innerException, sourceUri, lineNumber, linePosition, null)
		{
		}

		// Token: 0x060029EA RID: 10730 RVA: 0x000E3B01 File Offset: 0x000E1D01
		internal XmlSchemaValidationException(string res, string[] args, object sourceNode) : base(res, args, null, null, 0, 0, null)
		{
			this.sourceNodeObject = sourceNode;
		}

		// Token: 0x060029EB RID: 10731 RVA: 0x000E3B17 File Offset: 0x000E1D17
		internal XmlSchemaValidationException(string res, string[] args, string sourceUri, object sourceNode) : base(res, args, null, sourceUri, 0, 0, null)
		{
			this.sourceNodeObject = sourceNode;
		}

		// Token: 0x060029EC RID: 10732 RVA: 0x000E3B2E File Offset: 0x000E1D2E
		internal XmlSchemaValidationException(string res, string[] args, string sourceUri, int lineNumber, int linePosition, XmlSchemaObject source, object sourceNode) : base(res, args, null, sourceUri, lineNumber, linePosition, source)
		{
			this.sourceNodeObject = sourceNode;
		}

		/// <summary>Gets the XML node that caused this <see cref="T:System.Xml.Schema.XmlSchemaValidationException" />.</summary>
		/// <returns>The XML node that caused this <see cref="T:System.Xml.Schema.XmlSchemaValidationException" />.</returns>
		// Token: 0x17000933 RID: 2355
		// (get) Token: 0x060029ED RID: 10733 RVA: 0x000E3B48 File Offset: 0x000E1D48
		public object SourceObject
		{
			get
			{
				return this.sourceNodeObject;
			}
		}

		/// <summary>Sets the XML node that causes the error.</summary>
		/// <param name="sourceObject">The source object.</param>
		// Token: 0x060029EE RID: 10734 RVA: 0x000E3B50 File Offset: 0x000E1D50
		protected internal void SetSourceObject(object sourceObject)
		{
			this.sourceNodeObject = sourceObject;
		}

		// Token: 0x04001CF9 RID: 7417
		private object sourceNodeObject;
	}
}
