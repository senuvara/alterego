﻿using System;
using System.Collections.Generic;

namespace System.Xml
{
	// Token: 0x0200009E RID: 158
	internal interface IDtdAttributeListInfo
	{
		// Token: 0x170000FC RID: 252
		// (get) Token: 0x0600052A RID: 1322
		string Prefix { get; }

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x0600052B RID: 1323
		string LocalName { get; }

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x0600052C RID: 1324
		bool HasNonCDataAttributes { get; }

		// Token: 0x0600052D RID: 1325
		IDtdAttributeInfo LookupAttribute(string prefix, string localName);

		// Token: 0x0600052E RID: 1326
		IEnumerable<IDtdDefaultAttributeInfo> LookupDefaultAttributes();

		// Token: 0x0600052F RID: 1327
		IDtdAttributeInfo LookupIdAttribute();
	}
}
