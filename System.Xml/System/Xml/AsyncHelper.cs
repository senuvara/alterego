﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x02000074 RID: 116
	internal static class AsyncHelper
	{
		// Token: 0x06000382 RID: 898 RVA: 0x0000D43B File Offset: 0x0000B63B
		public static bool IsSuccess(this Task task)
		{
			return task.IsCompleted && task.Exception == null;
		}

		// Token: 0x06000383 RID: 899 RVA: 0x0000D450 File Offset: 0x0000B650
		public static Task CallVoidFuncWhenFinish(this Task task, Action func)
		{
			if (task.IsSuccess())
			{
				func();
				return AsyncHelper.DoneTask;
			}
			return task._CallVoidFuncWhenFinish(func);
		}

		// Token: 0x06000384 RID: 900 RVA: 0x0000D470 File Offset: 0x0000B670
		private static async Task _CallVoidFuncWhenFinish(this Task task, Action func)
		{
			await task.ConfigureAwait(false);
			func();
		}

		// Token: 0x06000385 RID: 901 RVA: 0x0000D4BD File Offset: 0x0000B6BD
		public static Task<bool> ReturnTaskBoolWhenFinish(this Task task, bool ret)
		{
			if (!task.IsSuccess())
			{
				return task._ReturnTaskBoolWhenFinish(ret);
			}
			if (ret)
			{
				return AsyncHelper.DoneTaskTrue;
			}
			return AsyncHelper.DoneTaskFalse;
		}

		// Token: 0x06000386 RID: 902 RVA: 0x0000D4E0 File Offset: 0x0000B6E0
		public static async Task<bool> _ReturnTaskBoolWhenFinish(this Task task, bool ret)
		{
			await task.ConfigureAwait(false);
			return ret;
		}

		// Token: 0x06000387 RID: 903 RVA: 0x0000D52D File Offset: 0x0000B72D
		public static Task CallTaskFuncWhenFinish(this Task task, Func<Task> func)
		{
			if (task.IsSuccess())
			{
				return func();
			}
			return AsyncHelper._CallTaskFuncWhenFinish(task, func);
		}

		// Token: 0x06000388 RID: 904 RVA: 0x0000D548 File Offset: 0x0000B748
		private static async Task _CallTaskFuncWhenFinish(Task task, Func<Task> func)
		{
			await task.ConfigureAwait(false);
			await func().ConfigureAwait(false);
		}

		// Token: 0x06000389 RID: 905 RVA: 0x0000D595 File Offset: 0x0000B795
		public static Task<bool> CallBoolTaskFuncWhenFinish(this Task task, Func<Task<bool>> func)
		{
			if (task.IsSuccess())
			{
				return func();
			}
			return task._CallBoolTaskFuncWhenFinish(func);
		}

		// Token: 0x0600038A RID: 906 RVA: 0x0000D5B0 File Offset: 0x0000B7B0
		private static async Task<bool> _CallBoolTaskFuncWhenFinish(this Task task, Func<Task<bool>> func)
		{
			await task.ConfigureAwait(false);
			return await func().ConfigureAwait(false);
		}

		// Token: 0x0600038B RID: 907 RVA: 0x0000D5FD File Offset: 0x0000B7FD
		public static Task<bool> ContinueBoolTaskFuncWhenFalse(this Task<bool> task, Func<Task<bool>> func)
		{
			if (!task.IsSuccess())
			{
				return AsyncHelper._ContinueBoolTaskFuncWhenFalse(task, func);
			}
			if (task.Result)
			{
				return AsyncHelper.DoneTaskTrue;
			}
			return func();
		}

		// Token: 0x0600038C RID: 908 RVA: 0x0000D624 File Offset: 0x0000B824
		private static async Task<bool> _ContinueBoolTaskFuncWhenFalse(Task<bool> task, Func<Task<bool>> func)
		{
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			bool result;
			if (configuredTaskAwaiter.GetResult())
			{
				result = true;
			}
			else
			{
				result = await func().ConfigureAwait(false);
			}
			return result;
		}

		// Token: 0x0600038D RID: 909 RVA: 0x0000D671 File Offset: 0x0000B871
		// Note: this type is marked as 'beforefieldinit'.
		static AsyncHelper()
		{
		}

		// Token: 0x040001EC RID: 492
		public static readonly Task DoneTask = Task.FromResult<bool>(true);

		// Token: 0x040001ED RID: 493
		public static readonly Task<bool> DoneTaskTrue = Task.FromResult<bool>(true);

		// Token: 0x040001EE RID: 494
		public static readonly Task<bool> DoneTaskFalse = Task.FromResult<bool>(false);

		// Token: 0x040001EF RID: 495
		public static readonly Task<int> DoneTaskZero = Task.FromResult<int>(0);

		// Token: 0x02000075 RID: 117
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_CallVoidFuncWhenFinish>d__6 : IAsyncStateMachine
		{
			// Token: 0x0600038E RID: 910 RVA: 0x0000D6A0 File Offset: 0x0000B8A0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, AsyncHelper.<_CallVoidFuncWhenFinish>d__6>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					func();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600038F RID: 911 RVA: 0x0000D760 File Offset: 0x0000B960
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040001F0 RID: 496
			public int <>1__state;

			// Token: 0x040001F1 RID: 497
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040001F2 RID: 498
			public Task task;

			// Token: 0x040001F3 RID: 499
			public Action func;

			// Token: 0x040001F4 RID: 500
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000076 RID: 118
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ReturnTaskBoolWhenFinish>d__8 : IAsyncStateMachine
		{
			// Token: 0x06000390 RID: 912 RVA: 0x0000D770 File Offset: 0x0000B970
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				bool result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, AsyncHelper.<_ReturnTaskBoolWhenFinish>d__8>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					result = ret;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000391 RID: 913 RVA: 0x0000D830 File Offset: 0x0000BA30
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040001F5 RID: 501
			public int <>1__state;

			// Token: 0x040001F6 RID: 502
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040001F7 RID: 503
			public Task task;

			// Token: 0x040001F8 RID: 504
			public bool ret;

			// Token: 0x040001F9 RID: 505
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000077 RID: 119
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_CallTaskFuncWhenFinish>d__10 : IAsyncStateMachine
		{
			// Token: 0x06000392 RID: 914 RVA: 0x0000D840 File Offset: 0x0000BA40
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_D4;
						}
						configuredTaskAwaiter = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, AsyncHelper.<_CallTaskFuncWhenFinish>d__10>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = func().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, AsyncHelper.<_CallTaskFuncWhenFinish>d__10>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_D4:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000393 RID: 915 RVA: 0x0000D964 File Offset: 0x0000BB64
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040001FA RID: 506
			public int <>1__state;

			// Token: 0x040001FB RID: 507
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040001FC RID: 508
			public Task task;

			// Token: 0x040001FD RID: 509
			public Func<Task> func;

			// Token: 0x040001FE RID: 510
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000078 RID: 120
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_CallBoolTaskFuncWhenFinish>d__12 : IAsyncStateMachine
		{
			// Token: 0x06000394 RID: 916 RVA: 0x0000D974 File Offset: 0x0000BB74
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_D8;
						}
						configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, AsyncHelper.<_CallBoolTaskFuncWhenFinish>d__12>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter3.GetResult();
					configuredTaskAwaiter = func().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, AsyncHelper.<_CallBoolTaskFuncWhenFinish>d__12>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_D8:
					result = configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000395 RID: 917 RVA: 0x0000DAA0 File Offset: 0x0000BCA0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040001FF RID: 511
			public int <>1__state;

			// Token: 0x04000200 RID: 512
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000201 RID: 513
			public Task task;

			// Token: 0x04000202 RID: 514
			public Func<Task<bool>> func;

			// Token: 0x04000203 RID: 515
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000204 RID: 516
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x02000079 RID: 121
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_ContinueBoolTaskFuncWhenFalse>d__14 : IAsyncStateMachine
		{
			// Token: 0x06000396 RID: 918 RVA: 0x0000DAB0 File Offset: 0x0000BCB0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					if (num != 0)
					{
						if (num == 1)
						{
							configuredTaskAwaiter3 = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_DD;
						}
						configuredTaskAwaiter3 = task.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, AsyncHelper.<_ContinueBoolTaskFuncWhenFalse>d__14>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					if (configuredTaskAwaiter3.GetResult())
					{
						result = true;
						goto IL_100;
					}
					configuredTaskAwaiter3 = func().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, AsyncHelper.<_ContinueBoolTaskFuncWhenFalse>d__14>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_DD:
					result = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_100:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000397 RID: 919 RVA: 0x0000DBE4 File Offset: 0x0000BDE4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000205 RID: 517
			public int <>1__state;

			// Token: 0x04000206 RID: 518
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000207 RID: 519
			public Task<bool> task;

			// Token: 0x04000208 RID: 520
			public Func<Task<bool>> func;

			// Token: 0x04000209 RID: 521
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
