﻿using System;
using System.Text;

namespace System.Xml
{
	// Token: 0x02000297 RID: 663
	internal class SafeAsciiDecoder : Decoder
	{
		// Token: 0x06001886 RID: 6278 RVA: 0x0008E735 File Offset: 0x0008C935
		public SafeAsciiDecoder()
		{
		}

		// Token: 0x06001887 RID: 6279 RVA: 0x000183C5 File Offset: 0x000165C5
		public override int GetCharCount(byte[] bytes, int index, int count)
		{
			return count;
		}

		// Token: 0x06001888 RID: 6280 RVA: 0x0008E740 File Offset: 0x0008C940
		public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
		{
			int i = byteIndex;
			int num = charIndex;
			while (i < byteIndex + byteCount)
			{
				chars[num++] = (char)bytes[i++];
			}
			return byteCount;
		}

		// Token: 0x06001889 RID: 6281 RVA: 0x0008E76C File Offset: 0x0008C96C
		public override void Convert(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex, int charCount, bool flush, out int bytesUsed, out int charsUsed, out bool completed)
		{
			if (charCount < byteCount)
			{
				byteCount = charCount;
				completed = false;
			}
			else
			{
				completed = true;
			}
			int i = byteIndex;
			int num = charIndex;
			int num2 = byteIndex + byteCount;
			while (i < num2)
			{
				chars[num++] = (char)bytes[i++];
			}
			charsUsed = byteCount;
			bytesUsed = byteCount;
		}
	}
}
