﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x02000210 RID: 528
	internal class DocumentXPathNodeIterator_ElemChildren_NoLocalName : DocumentXPathNodeIterator_ElemDescendants
	{
		// Token: 0x060012EB RID: 4843 RVA: 0x00070A24 File Offset: 0x0006EC24
		internal DocumentXPathNodeIterator_ElemChildren_NoLocalName(DocumentXPathNavigator nav, string nsAtom) : base(nav)
		{
			this.nsAtom = nsAtom;
		}

		// Token: 0x060012EC RID: 4844 RVA: 0x00070A34 File Offset: 0x0006EC34
		internal DocumentXPathNodeIterator_ElemChildren_NoLocalName(DocumentXPathNodeIterator_ElemChildren_NoLocalName other) : base(other)
		{
			this.nsAtom = other.nsAtom;
		}

		// Token: 0x060012ED RID: 4845 RVA: 0x00070A49 File Offset: 0x0006EC49
		public override XPathNodeIterator Clone()
		{
			return new DocumentXPathNodeIterator_ElemChildren_NoLocalName(this);
		}

		// Token: 0x060012EE RID: 4846 RVA: 0x00070A51 File Offset: 0x0006EC51
		protected override bool Match(XmlNode node)
		{
			return Ref.Equal(node.NamespaceURI, this.nsAtom);
		}

		// Token: 0x04000D40 RID: 3392
		private string nsAtom;
	}
}
