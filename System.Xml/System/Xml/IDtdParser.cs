﻿using System;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020000A2 RID: 162
	internal interface IDtdParser
	{
		// Token: 0x06000547 RID: 1351
		IDtdInfo ParseInternalDtd(IDtdParserAdapter adapter, bool saveInternalSubset);

		// Token: 0x06000548 RID: 1352
		IDtdInfo ParseFreeFloatingDtd(string baseUri, string docTypeName, string publicId, string systemId, string internalSubset, IDtdParserAdapter adapter);

		// Token: 0x06000549 RID: 1353
		Task<IDtdInfo> ParseInternalDtdAsync(IDtdParserAdapter adapter, bool saveInternalSubset);

		// Token: 0x0600054A RID: 1354
		Task<IDtdInfo> ParseFreeFloatingDtdAsync(string baseUri, string docTypeName, string publicId, string systemId, string internalSubset, IDtdParserAdapter adapter);
	}
}
