﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x0200020F RID: 527
	internal sealed class DocumentXPathNodeIterator_AllElemChildren_AndSelf : DocumentXPathNodeIterator_AllElemChildren
	{
		// Token: 0x060012E7 RID: 4839 RVA: 0x000709BB File Offset: 0x0006EBBB
		internal DocumentXPathNodeIterator_AllElemChildren_AndSelf(DocumentXPathNavigator nav) : base(nav)
		{
		}

		// Token: 0x060012E8 RID: 4840 RVA: 0x000709C4 File Offset: 0x0006EBC4
		internal DocumentXPathNodeIterator_AllElemChildren_AndSelf(DocumentXPathNodeIterator_AllElemChildren_AndSelf other) : base(other)
		{
		}

		// Token: 0x060012E9 RID: 4841 RVA: 0x000709CD File Offset: 0x0006EBCD
		public override XPathNodeIterator Clone()
		{
			return new DocumentXPathNodeIterator_AllElemChildren_AndSelf(this);
		}

		// Token: 0x060012EA RID: 4842 RVA: 0x000709D8 File Offset: 0x0006EBD8
		public override bool MoveNext()
		{
			if (this.CurrentPosition == 0)
			{
				XmlNode xmlNode = (XmlNode)((DocumentXPathNavigator)this.Current).UnderlyingObject;
				if (xmlNode.NodeType == XmlNodeType.Element && this.Match(xmlNode))
				{
					base.SetPosition(1);
					return true;
				}
			}
			return base.MoveNext();
		}
	}
}
