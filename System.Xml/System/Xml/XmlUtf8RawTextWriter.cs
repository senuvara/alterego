﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x02000185 RID: 389
	internal class XmlUtf8RawTextWriter : XmlRawWriter
	{
		// Token: 0x06000DDD RID: 3549 RVA: 0x00050344 File Offset: 0x0004E544
		protected XmlUtf8RawTextWriter(XmlWriterSettings settings)
		{
			this.useAsync = settings.Async;
			this.newLineHandling = settings.NewLineHandling;
			this.omitXmlDeclaration = settings.OmitXmlDeclaration;
			this.newLineChars = settings.NewLineChars;
			this.checkCharacters = settings.CheckCharacters;
			this.closeOutput = settings.CloseOutput;
			this.standalone = settings.Standalone;
			this.outputMethod = settings.OutputMethod;
			this.mergeCDataSections = settings.MergeCDataSections;
			if (this.checkCharacters && this.newLineHandling == NewLineHandling.Replace)
			{
				this.ValidateContentChars(this.newLineChars, "NewLineChars", false);
			}
		}

		// Token: 0x06000DDE RID: 3550 RVA: 0x0005040C File Offset: 0x0004E60C
		public XmlUtf8RawTextWriter(Stream stream, XmlWriterSettings settings) : this(settings)
		{
			this.stream = stream;
			this.encoding = settings.Encoding;
			if (settings.Async)
			{
				this.bufLen = 65536;
			}
			this.bufBytes = new byte[this.bufLen + 32];
			if (!stream.CanSeek || stream.Position == 0L)
			{
				byte[] preamble = this.encoding.GetPreamble();
				if (preamble.Length != 0)
				{
					Buffer.BlockCopy(preamble, 0, this.bufBytes, 1, preamble.Length);
					this.bufPos += preamble.Length;
					this.textPos += preamble.Length;
				}
			}
			if (settings.AutoXmlDeclaration)
			{
				this.WriteXmlDeclaration(this.standalone);
				this.autoXmlDeclaration = true;
			}
		}

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06000DDF RID: 3551 RVA: 0x000504C8 File Offset: 0x0004E6C8
		public override XmlWriterSettings Settings
		{
			get
			{
				return new XmlWriterSettings
				{
					Encoding = this.encoding,
					OmitXmlDeclaration = this.omitXmlDeclaration,
					NewLineHandling = this.newLineHandling,
					NewLineChars = this.newLineChars,
					CloseOutput = this.closeOutput,
					ConformanceLevel = ConformanceLevel.Auto,
					CheckCharacters = this.checkCharacters,
					AutoXmlDeclaration = this.autoXmlDeclaration,
					Standalone = this.standalone,
					OutputMethod = this.outputMethod,
					ReadOnly = true
				};
			}
		}

		// Token: 0x06000DE0 RID: 3552 RVA: 0x00050554 File Offset: 0x0004E754
		internal override void WriteXmlDeclaration(XmlStandalone standalone)
		{
			if (!this.omitXmlDeclaration && !this.autoXmlDeclaration)
			{
				this.RawText("<?xml version=\"");
				this.RawText("1.0");
				if (this.encoding != null)
				{
					this.RawText("\" encoding=\"");
					this.RawText(this.encoding.WebName);
				}
				if (standalone != XmlStandalone.Omit)
				{
					this.RawText("\" standalone=\"");
					this.RawText((standalone == XmlStandalone.Yes) ? "yes" : "no");
				}
				this.RawText("\"?>");
			}
		}

		// Token: 0x06000DE1 RID: 3553 RVA: 0x000505DA File Offset: 0x0004E7DA
		internal override void WriteXmlDeclaration(string xmldecl)
		{
			if (!this.omitXmlDeclaration && !this.autoXmlDeclaration)
			{
				this.WriteProcessingInstruction("xml", xmldecl);
			}
		}

		// Token: 0x06000DE2 RID: 3554 RVA: 0x000505F8 File Offset: 0x0004E7F8
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			this.RawText("<!DOCTYPE ");
			this.RawText(name);
			int num;
			if (pubid != null)
			{
				this.RawText(" PUBLIC \"");
				this.RawText(pubid);
				this.RawText("\" \"");
				if (sysid != null)
				{
					this.RawText(sysid);
				}
				byte[] array = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 34;
			}
			else if (sysid != null)
			{
				this.RawText(" SYSTEM \"");
				this.RawText(sysid);
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 34;
			}
			else
			{
				byte[] array3 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 32;
			}
			if (subset != null)
			{
				byte[] array4 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 91;
				this.RawText(subset);
				byte[] array5 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array5[num] = 93;
			}
			byte[] array6 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 62;
		}

		// Token: 0x06000DE3 RID: 3555 RVA: 0x00050704 File Offset: 0x0004E904
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			if (prefix != null && prefix.Length != 0)
			{
				this.RawText(prefix);
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 58;
			}
			this.RawText(localName);
			this.attrEndPos = this.bufPos;
		}

		// Token: 0x06000DE4 RID: 3556 RVA: 0x0005076C File Offset: 0x0004E96C
		internal override void StartElementContent()
		{
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 62;
			this.contentPos = this.bufPos;
		}

		// Token: 0x06000DE5 RID: 3557 RVA: 0x000507A0 File Offset: 0x0004E9A0
		internal override void WriteEndElement(string prefix, string localName, string ns)
		{
			int num;
			if (this.contentPos != this.bufPos)
			{
				byte[] array = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 60;
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 47;
				if (prefix != null && prefix.Length != 0)
				{
					this.RawText(prefix);
					byte[] array3 = this.bufBytes;
					num = this.bufPos;
					this.bufPos = num + 1;
					array3[num] = 58;
				}
				this.RawText(localName);
				byte[] array4 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 62;
				return;
			}
			this.bufPos--;
			byte[] array5 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 32;
			byte[] array6 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 47;
			byte[] array7 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array7[num] = 62;
		}

		// Token: 0x06000DE6 RID: 3558 RVA: 0x0005089C File Offset: 0x0004EA9C
		internal override void WriteFullEndElement(string prefix, string localName, string ns)
		{
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 47;
			if (prefix != null && prefix.Length != 0)
			{
				this.RawText(prefix);
				byte[] array3 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 58;
			}
			this.RawText(localName);
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 62;
		}

		// Token: 0x06000DE7 RID: 3559 RVA: 0x0005092C File Offset: 0x0004EB2C
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			int num;
			if (this.attrEndPos == this.bufPos)
			{
				byte[] array = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 32;
			}
			if (prefix != null && prefix.Length > 0)
			{
				this.RawText(prefix);
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 58;
			}
			this.RawText(localName);
			byte[] array3 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 61;
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 34;
			this.inAttributeValue = true;
		}

		// Token: 0x06000DE8 RID: 3560 RVA: 0x000509D0 File Offset: 0x0004EBD0
		public override void WriteEndAttribute()
		{
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 34;
			this.inAttributeValue = false;
			this.attrEndPos = this.bufPos;
		}

		// Token: 0x06000DE9 RID: 3561 RVA: 0x00020601 File Offset: 0x0001E801
		internal override void WriteNamespaceDeclaration(string prefix, string namespaceName)
		{
			this.WriteStartNamespaceDeclaration(prefix);
			this.WriteString(namespaceName);
			this.WriteEndNamespaceDeclaration();
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06000DEA RID: 3562 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool SupportsNamespaceDeclarationInChunks
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000DEB RID: 3563 RVA: 0x00050A0C File Offset: 0x0004EC0C
		internal override void WriteStartNamespaceDeclaration(string prefix)
		{
			if (prefix.Length == 0)
			{
				this.RawText(" xmlns=\"");
			}
			else
			{
				this.RawText(" xmlns:");
				this.RawText(prefix);
				byte[] array = this.bufBytes;
				int num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 61;
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 34;
			}
			this.inAttributeValue = true;
		}

		// Token: 0x06000DEC RID: 3564 RVA: 0x00050A7C File Offset: 0x0004EC7C
		internal override void WriteEndNamespaceDeclaration()
		{
			this.inAttributeValue = false;
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 34;
			this.attrEndPos = this.bufPos;
		}

		// Token: 0x06000DED RID: 3565 RVA: 0x00050AB8 File Offset: 0x0004ECB8
		public override void WriteCData(string text)
		{
			int num;
			if (this.mergeCDataSections && this.bufPos == this.cdataPos)
			{
				this.bufPos -= 3;
			}
			else
			{
				byte[] array = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 60;
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 33;
				byte[] array3 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 91;
				byte[] array4 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 67;
				byte[] array5 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array5[num] = 68;
				byte[] array6 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array6[num] = 65;
				byte[] array7 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array7[num] = 84;
				byte[] array8 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array8[num] = 65;
				byte[] array9 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array9[num] = 91;
			}
			this.WriteCDataSection(text);
			byte[] array10 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array10[num] = 93;
			byte[] array11 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array11[num] = 93;
			byte[] array12 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array12[num] = 62;
			this.textPos = this.bufPos;
			this.cdataPos = this.bufPos;
		}

		// Token: 0x06000DEE RID: 3566 RVA: 0x00050C48 File Offset: 0x0004EE48
		public override void WriteComment(string text)
		{
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 33;
			byte[] array3 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 45;
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 45;
			this.WriteCommentOrPi(text, 45);
			byte[] array5 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 45;
			byte[] array6 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 45;
			byte[] array7 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array7[num] = 62;
		}

		// Token: 0x06000DEF RID: 3567 RVA: 0x00050D14 File Offset: 0x0004EF14
		public override void WriteProcessingInstruction(string name, string text)
		{
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 63;
			this.RawText(name);
			if (text.Length > 0)
			{
				byte[] array3 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 32;
				this.WriteCommentOrPi(text, 63);
			}
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 63;
			byte[] array5 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 62;
		}

		// Token: 0x06000DF0 RID: 3568 RVA: 0x00050DBC File Offset: 0x0004EFBC
		public override void WriteEntityRef(string name)
		{
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 38;
			this.RawText(name);
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 59;
			if (this.bufPos > this.bufLen)
			{
				this.FlushBuffer();
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x06000DF1 RID: 3569 RVA: 0x00050E24 File Offset: 0x0004F024
		public override void WriteCharEntity(char ch)
		{
			int num = (int)ch;
			string s = num.ToString("X", NumberFormatInfo.InvariantInfo);
			if (this.checkCharacters && !this.xmlCharType.IsCharData(ch))
			{
				throw XmlConvert.CreateInvalidCharException(ch, '\0');
			}
			byte[] array = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 38;
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 35;
			byte[] array3 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 120;
			this.RawText(s);
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 59;
			if (this.bufPos > this.bufLen)
			{
				this.FlushBuffer();
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x06000DF2 RID: 3570 RVA: 0x00050EF4 File Offset: 0x0004F0F4
		public unsafe override void WriteWhitespace(string ws)
		{
			fixed (string text = ws)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char* pSrcEnd = ptr + ws.Length;
				if (this.inAttributeValue)
				{
					this.WriteAttributeTextBlock(ptr, pSrcEnd);
				}
				else
				{
					this.WriteElementTextBlock(ptr, pSrcEnd);
				}
			}
		}

		// Token: 0x06000DF3 RID: 3571 RVA: 0x00050F3C File Offset: 0x0004F13C
		public unsafe override void WriteString(string text)
		{
			fixed (string text2 = text)
			{
				char* ptr = text2;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char* pSrcEnd = ptr + text.Length;
				if (this.inAttributeValue)
				{
					this.WriteAttributeTextBlock(ptr, pSrcEnd);
				}
				else
				{
					this.WriteElementTextBlock(ptr, pSrcEnd);
				}
			}
		}

		// Token: 0x06000DF4 RID: 3572 RVA: 0x00050F84 File Offset: 0x0004F184
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			int num = XmlCharType.CombineSurrogateChar((int)lowChar, (int)highChar);
			byte[] array = this.bufBytes;
			int num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array[num2] = 38;
			byte[] array2 = this.bufBytes;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array2[num2] = 35;
			byte[] array3 = this.bufBytes;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array3[num2] = 120;
			this.RawText(num.ToString("X", NumberFormatInfo.InvariantInfo));
			byte[] array4 = this.bufBytes;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array4[num2] = 59;
			this.textPos = this.bufPos;
		}

		// Token: 0x06000DF5 RID: 3573 RVA: 0x00051024 File Offset: 0x0004F224
		public unsafe override void WriteChars(char[] buffer, int index, int count)
		{
			fixed (char* ptr = &buffer[index])
			{
				char* ptr2 = ptr;
				if (this.inAttributeValue)
				{
					this.WriteAttributeTextBlock(ptr2, ptr2 + count);
				}
				else
				{
					this.WriteElementTextBlock(ptr2, ptr2 + count);
				}
			}
		}

		// Token: 0x06000DF6 RID: 3574 RVA: 0x00051064 File Offset: 0x0004F264
		public unsafe override void WriteRaw(char[] buffer, int index, int count)
		{
			fixed (char* ptr = &buffer[index])
			{
				char* ptr2 = ptr;
				this.WriteRawWithCharChecking(ptr2, ptr2 + count);
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x06000DF7 RID: 3575 RVA: 0x00051098 File Offset: 0x0004F298
		public unsafe override void WriteRaw(string data)
		{
			fixed (string text = data)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				this.WriteRawWithCharChecking(ptr, ptr + data.Length);
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x06000DF8 RID: 3576 RVA: 0x000510D8 File Offset: 0x0004F2D8
		public override void Close()
		{
			try
			{
				this.FlushBuffer();
				this.FlushEncoder();
			}
			finally
			{
				this.writeToNull = true;
				if (this.stream != null)
				{
					try
					{
						this.stream.Flush();
					}
					finally
					{
						try
						{
							if (this.closeOutput)
							{
								this.stream.Close();
							}
						}
						finally
						{
							this.stream = null;
						}
					}
				}
			}
		}

		// Token: 0x06000DF9 RID: 3577 RVA: 0x00051158 File Offset: 0x0004F358
		public override void Flush()
		{
			this.FlushBuffer();
			this.FlushEncoder();
			if (this.stream != null)
			{
				this.stream.Flush();
			}
		}

		// Token: 0x06000DFA RID: 3578 RVA: 0x0005117C File Offset: 0x0004F37C
		protected virtual void FlushBuffer()
		{
			try
			{
				if (!this.writeToNull)
				{
					this.stream.Write(this.bufBytes, 1, this.bufPos - 1);
				}
			}
			catch
			{
				this.writeToNull = true;
				throw;
			}
			finally
			{
				this.bufBytes[0] = this.bufBytes[this.bufPos - 1];
				if (XmlUtf8RawTextWriter.IsSurrogateByte(this.bufBytes[0]))
				{
					this.bufBytes[1] = this.bufBytes[this.bufPos];
					this.bufBytes[2] = this.bufBytes[this.bufPos + 1];
					this.bufBytes[3] = this.bufBytes[this.bufPos + 2];
				}
				this.textPos = ((this.textPos == this.bufPos) ? 1 : 0);
				this.attrEndPos = ((this.attrEndPos == this.bufPos) ? 1 : 0);
				this.contentPos = 0;
				this.cdataPos = 0;
				this.bufPos = 1;
			}
		}

		// Token: 0x06000DFB RID: 3579 RVA: 0x000030EC File Offset: 0x000012EC
		private void FlushEncoder()
		{
		}

		// Token: 0x06000DFC RID: 3580 RVA: 0x00051288 File Offset: 0x0004F488
		protected unsafe void WriteAttributeTextBlock(char* pSrc, char* pSrcEnd)
		{
			byte[] array;
			byte* ptr;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			byte* ptr2 = ptr + this.bufPos;
			int num = 0;
			for (;;)
			{
				byte* ptr3 = ptr2 + (long)(pSrcEnd - pSrc);
				if (ptr3 != ptr + this.bufLen)
				{
					ptr3 = ptr + this.bufLen;
				}
				while (ptr2 < ptr3 && (this.xmlCharType.charProperties[num = (int)(*pSrc)] & 128) != 0 && num <= 127)
				{
					*ptr2 = (byte)num;
					ptr2++;
					pSrc++;
				}
				if (pSrc >= pSrcEnd)
				{
					break;
				}
				if (ptr2 >= ptr3)
				{
					this.bufPos = (int)((long)(ptr2 - ptr));
					this.FlushBuffer();
					ptr2 = ptr + 1;
				}
				else
				{
					if (num <= 38)
					{
						switch (num)
						{
						case 9:
							if (this.newLineHandling == NewLineHandling.None)
							{
								*ptr2 = (byte)num;
								ptr2++;
								goto IL_1CC;
							}
							ptr2 = XmlUtf8RawTextWriter.TabEntity(ptr2);
							goto IL_1CC;
						case 10:
							if (this.newLineHandling == NewLineHandling.None)
							{
								*ptr2 = (byte)num;
								ptr2++;
								goto IL_1CC;
							}
							ptr2 = XmlUtf8RawTextWriter.LineFeedEntity(ptr2);
							goto IL_1CC;
						case 11:
						case 12:
							break;
						case 13:
							if (this.newLineHandling == NewLineHandling.None)
							{
								*ptr2 = (byte)num;
								ptr2++;
								goto IL_1CC;
							}
							ptr2 = XmlUtf8RawTextWriter.CarriageReturnEntity(ptr2);
							goto IL_1CC;
						default:
							if (num == 34)
							{
								ptr2 = XmlUtf8RawTextWriter.QuoteEntity(ptr2);
								goto IL_1CC;
							}
							if (num == 38)
							{
								ptr2 = XmlUtf8RawTextWriter.AmpEntity(ptr2);
								goto IL_1CC;
							}
							break;
						}
					}
					else
					{
						if (num == 39)
						{
							*ptr2 = (byte)num;
							ptr2++;
							goto IL_1CC;
						}
						if (num == 60)
						{
							ptr2 = XmlUtf8RawTextWriter.LtEntity(ptr2);
							goto IL_1CC;
						}
						if (num == 62)
						{
							ptr2 = XmlUtf8RawTextWriter.GtEntity(ptr2);
							goto IL_1CC;
						}
					}
					if (XmlCharType.IsSurrogate(num))
					{
						ptr2 = XmlUtf8RawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, ptr2);
						pSrc += 2;
						continue;
					}
					if (num <= 127 || num >= 65534)
					{
						ptr2 = this.InvalidXmlChar(num, ptr2, true);
						pSrc++;
						continue;
					}
					ptr2 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr2);
					pSrc++;
					continue;
					IL_1CC:
					pSrc++;
				}
			}
			this.bufPos = (int)((long)(ptr2 - ptr));
			array = null;
		}

		// Token: 0x06000DFD RID: 3581 RVA: 0x0005147C File Offset: 0x0004F67C
		protected unsafe void WriteElementTextBlock(char* pSrc, char* pSrcEnd)
		{
			byte[] array;
			byte* ptr;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			byte* ptr2 = ptr + this.bufPos;
			int num = 0;
			for (;;)
			{
				byte* ptr3 = ptr2 + (long)(pSrcEnd - pSrc);
				if (ptr3 != ptr + this.bufLen)
				{
					ptr3 = ptr + this.bufLen;
				}
				while (ptr2 < ptr3 && (this.xmlCharType.charProperties[num = (int)(*pSrc)] & 128) != 0 && num <= 127)
				{
					*ptr2 = (byte)num;
					ptr2++;
					pSrc++;
				}
				if (pSrc >= pSrcEnd)
				{
					break;
				}
				if (ptr2 < ptr3)
				{
					if (num <= 38)
					{
						switch (num)
						{
						case 9:
							goto IL_108;
						case 10:
							if (this.newLineHandling == NewLineHandling.Replace)
							{
								ptr2 = this.WriteNewLine(ptr2);
								goto IL_1CF;
							}
							*ptr2 = (byte)num;
							ptr2++;
							goto IL_1CF;
						case 11:
						case 12:
							break;
						case 13:
							switch (this.newLineHandling)
							{
							case NewLineHandling.Replace:
								if (pSrc[1] == '\n')
								{
									pSrc++;
								}
								ptr2 = this.WriteNewLine(ptr2);
								goto IL_1CF;
							case NewLineHandling.Entitize:
								ptr2 = XmlUtf8RawTextWriter.CarriageReturnEntity(ptr2);
								goto IL_1CF;
							case NewLineHandling.None:
								*ptr2 = (byte)num;
								ptr2++;
								goto IL_1CF;
							default:
								goto IL_1CF;
							}
							break;
						default:
							if (num == 34)
							{
								goto IL_108;
							}
							if (num == 38)
							{
								ptr2 = XmlUtf8RawTextWriter.AmpEntity(ptr2);
								goto IL_1CF;
							}
							break;
						}
					}
					else
					{
						if (num == 39)
						{
							goto IL_108;
						}
						if (num == 60)
						{
							ptr2 = XmlUtf8RawTextWriter.LtEntity(ptr2);
							goto IL_1CF;
						}
						if (num == 62)
						{
							ptr2 = XmlUtf8RawTextWriter.GtEntity(ptr2);
							goto IL_1CF;
						}
					}
					if (XmlCharType.IsSurrogate(num))
					{
						ptr2 = XmlUtf8RawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, ptr2);
						pSrc += 2;
						continue;
					}
					if (num <= 127 || num >= 65534)
					{
						ptr2 = this.InvalidXmlChar(num, ptr2, true);
						pSrc++;
						continue;
					}
					ptr2 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr2);
					pSrc++;
					continue;
					IL_1CF:
					pSrc++;
					continue;
					IL_108:
					*ptr2 = (byte)num;
					ptr2++;
					goto IL_1CF;
				}
				this.bufPos = (int)((long)(ptr2 - ptr));
				this.FlushBuffer();
				ptr2 = ptr + 1;
			}
			this.bufPos = (int)((long)(ptr2 - ptr));
			this.textPos = this.bufPos;
			this.contentPos = 0;
			array = null;
		}

		// Token: 0x06000DFE RID: 3582 RVA: 0x00051684 File Offset: 0x0004F884
		protected unsafe void RawText(string s)
		{
			fixed (string text = s)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				this.RawText(ptr, ptr + s.Length);
			}
		}

		// Token: 0x06000DFF RID: 3583 RVA: 0x000516B8 File Offset: 0x0004F8B8
		protected unsafe void RawText(char* pSrcBegin, char* pSrcEnd)
		{
			byte[] array;
			byte* ptr;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			byte* ptr2 = ptr + this.bufPos;
			char* ptr3 = pSrcBegin;
			int num = 0;
			for (;;)
			{
				byte* ptr4 = ptr2 + (long)(pSrcEnd - ptr3);
				if (ptr4 != ptr + this.bufLen)
				{
					ptr4 = ptr + this.bufLen;
				}
				while (ptr2 < ptr4 && (num = (int)(*ptr3)) <= 127)
				{
					ptr3++;
					*ptr2 = (byte)num;
					ptr2++;
				}
				if (ptr3 >= pSrcEnd)
				{
					break;
				}
				if (ptr2 >= ptr4)
				{
					this.bufPos = (int)((long)(ptr2 - ptr));
					this.FlushBuffer();
					ptr2 = ptr + 1;
				}
				else if (XmlCharType.IsSurrogate(num))
				{
					ptr2 = XmlUtf8RawTextWriter.EncodeSurrogate(ptr3, pSrcEnd, ptr2);
					ptr3 += 2;
				}
				else if (num <= 127 || num >= 65534)
				{
					ptr2 = this.InvalidXmlChar(num, ptr2, false);
					ptr3++;
				}
				else
				{
					ptr2 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr2);
					ptr3++;
				}
			}
			this.bufPos = (int)((long)(ptr2 - ptr));
			array = null;
		}

		// Token: 0x06000E00 RID: 3584 RVA: 0x000517B0 File Offset: 0x0004F9B0
		protected unsafe void WriteRawWithCharChecking(char* pSrcBegin, char* pSrcEnd)
		{
			byte[] array;
			byte* ptr;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			char* ptr2 = pSrcBegin;
			byte* ptr3 = ptr + this.bufPos;
			int num = 0;
			for (;;)
			{
				byte* ptr4 = ptr3 + (long)(pSrcEnd - ptr2);
				if (ptr4 != ptr + this.bufLen)
				{
					ptr4 = ptr + this.bufLen;
				}
				while (ptr3 < ptr4 && (this.xmlCharType.charProperties[num = (int)(*ptr2)] & 64) != 0 && num <= 127)
				{
					*ptr3 = (byte)num;
					ptr3++;
					ptr2++;
				}
				if (ptr2 >= pSrcEnd)
				{
					break;
				}
				if (ptr3 < ptr4)
				{
					if (num <= 38)
					{
						switch (num)
						{
						case 9:
							goto IL_D9;
						case 10:
							if (this.newLineHandling == NewLineHandling.Replace)
							{
								ptr3 = this.WriteNewLine(ptr3);
								goto IL_180;
							}
							*ptr3 = (byte)num;
							ptr3++;
							goto IL_180;
						case 11:
						case 12:
							break;
						case 13:
							if (this.newLineHandling == NewLineHandling.Replace)
							{
								if (ptr2[1] == '\n')
								{
									ptr2++;
								}
								ptr3 = this.WriteNewLine(ptr3);
								goto IL_180;
							}
							*ptr3 = (byte)num;
							ptr3++;
							goto IL_180;
						default:
							if (num == 38)
							{
								goto IL_D9;
							}
							break;
						}
					}
					else if (num == 60 || num == 93)
					{
						goto IL_D9;
					}
					if (XmlCharType.IsSurrogate(num))
					{
						ptr3 = XmlUtf8RawTextWriter.EncodeSurrogate(ptr2, pSrcEnd, ptr3);
						ptr2 += 2;
						continue;
					}
					if (num <= 127 || num >= 65534)
					{
						ptr3 = this.InvalidXmlChar(num, ptr3, false);
						ptr2++;
						continue;
					}
					ptr3 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr3);
					ptr2++;
					continue;
					IL_180:
					ptr2++;
					continue;
					IL_D9:
					*ptr3 = (byte)num;
					ptr3++;
					goto IL_180;
				}
				this.bufPos = (int)((long)(ptr3 - ptr));
				this.FlushBuffer();
				ptr3 = ptr + 1;
			}
			this.bufPos = (int)((long)(ptr3 - ptr));
			array = null;
		}

		// Token: 0x06000E01 RID: 3585 RVA: 0x00051958 File Offset: 0x0004FB58
		protected unsafe void WriteCommentOrPi(string text, int stopChar)
		{
			if (text.Length == 0)
			{
				if (this.bufPos >= this.bufLen)
				{
					this.FlushBuffer();
				}
				return;
			}
			fixed (string text2 = text)
			{
				char* ptr = text2;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				byte[] array;
				byte* ptr2;
				if ((array = this.bufBytes) == null || array.Length == 0)
				{
					ptr2 = null;
				}
				else
				{
					ptr2 = &array[0];
				}
				char* ptr3 = ptr;
				char* ptr4 = ptr + text.Length;
				byte* ptr5 = ptr2 + this.bufPos;
				int num = 0;
				for (;;)
				{
					byte* ptr6 = ptr5 + (long)(ptr4 - ptr3);
					if (ptr6 != ptr2 + this.bufLen)
					{
						ptr6 = ptr2 + this.bufLen;
					}
					while (ptr5 < ptr6 && (this.xmlCharType.charProperties[num = (int)(*ptr3)] & 64) != 0 && num != stopChar && num <= 127)
					{
						*ptr5 = (byte)num;
						ptr5++;
						ptr3++;
					}
					if (ptr3 >= ptr4)
					{
						break;
					}
					if (ptr5 < ptr6)
					{
						if (num <= 45)
						{
							switch (num)
							{
							case 9:
								goto IL_220;
							case 10:
								if (this.newLineHandling == NewLineHandling.Replace)
								{
									ptr5 = this.WriteNewLine(ptr5);
									goto IL_28F;
								}
								*ptr5 = (byte)num;
								ptr5++;
								goto IL_28F;
							case 11:
							case 12:
								break;
							case 13:
								if (this.newLineHandling == NewLineHandling.Replace)
								{
									if (ptr3[1] == '\n')
									{
										ptr3++;
									}
									ptr5 = this.WriteNewLine(ptr5);
									goto IL_28F;
								}
								*ptr5 = (byte)num;
								ptr5++;
								goto IL_28F;
							default:
								if (num == 38)
								{
									goto IL_220;
								}
								if (num == 45)
								{
									*ptr5 = 45;
									ptr5++;
									if (num == stopChar && (ptr3 + 1 == ptr4 || ptr3[1] == '-'))
									{
										*ptr5 = 32;
										ptr5++;
										goto IL_28F;
									}
									goto IL_28F;
								}
								break;
							}
						}
						else
						{
							if (num == 60)
							{
								goto IL_220;
							}
							if (num != 63)
							{
								if (num == 93)
								{
									*ptr5 = 93;
									ptr5++;
									goto IL_28F;
								}
							}
							else
							{
								*ptr5 = 63;
								ptr5++;
								if (num == stopChar && ptr3 + 1 < ptr4 && ptr3[1] == '>')
								{
									*ptr5 = 32;
									ptr5++;
									goto IL_28F;
								}
								goto IL_28F;
							}
						}
						if (XmlCharType.IsSurrogate(num))
						{
							ptr5 = XmlUtf8RawTextWriter.EncodeSurrogate(ptr3, ptr4, ptr5);
							ptr3 += 2;
							continue;
						}
						if (num <= 127 || num >= 65534)
						{
							ptr5 = this.InvalidXmlChar(num, ptr5, false);
							ptr3++;
							continue;
						}
						ptr5 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr5);
						ptr3++;
						continue;
						IL_28F:
						ptr3++;
						continue;
						IL_220:
						*ptr5 = (byte)num;
						ptr5++;
						goto IL_28F;
					}
					this.bufPos = (int)((long)(ptr5 - ptr2));
					this.FlushBuffer();
					ptr5 = ptr2 + 1;
				}
				this.bufPos = (int)((long)(ptr5 - ptr2));
				array = null;
			}
		}

		// Token: 0x06000E02 RID: 3586 RVA: 0x00051C14 File Offset: 0x0004FE14
		protected unsafe void WriteCDataSection(string text)
		{
			if (text.Length == 0)
			{
				if (this.bufPos >= this.bufLen)
				{
					this.FlushBuffer();
				}
				return;
			}
			fixed (string text2 = text)
			{
				char* ptr = text2;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				byte[] array;
				byte* ptr2;
				if ((array = this.bufBytes) == null || array.Length == 0)
				{
					ptr2 = null;
				}
				else
				{
					ptr2 = &array[0];
				}
				char* ptr3 = ptr;
				char* ptr4 = ptr + text.Length;
				byte* ptr5 = ptr2 + this.bufPos;
				int num = 0;
				for (;;)
				{
					byte* ptr6 = ptr5 + (long)(ptr4 - ptr3);
					if (ptr6 != ptr2 + this.bufLen)
					{
						ptr6 = ptr2 + this.bufLen;
					}
					while (ptr5 < ptr6 && (this.xmlCharType.charProperties[num = (int)(*ptr3)] & 128) != 0 && num != 93 && num <= 127)
					{
						*ptr5 = (byte)num;
						ptr5++;
						ptr3++;
					}
					if (ptr3 >= ptr4)
					{
						break;
					}
					if (ptr5 < ptr6)
					{
						if (num <= 39)
						{
							switch (num)
							{
							case 9:
								goto IL_204;
							case 10:
								if (this.newLineHandling == NewLineHandling.Replace)
								{
									ptr5 = this.WriteNewLine(ptr5);
									goto IL_273;
								}
								*ptr5 = (byte)num;
								ptr5++;
								goto IL_273;
							case 11:
							case 12:
								break;
							case 13:
								if (this.newLineHandling == NewLineHandling.Replace)
								{
									if (ptr3[1] == '\n')
									{
										ptr3++;
									}
									ptr5 = this.WriteNewLine(ptr5);
									goto IL_273;
								}
								*ptr5 = (byte)num;
								ptr5++;
								goto IL_273;
							default:
								if (num == 34 || num - 38 <= 1)
								{
									goto IL_204;
								}
								break;
							}
						}
						else
						{
							if (num == 60)
							{
								goto IL_204;
							}
							if (num == 62)
							{
								if (this.hadDoubleBracket && ptr5[-1] == 93)
								{
									ptr5 = XmlUtf8RawTextWriter.RawEndCData(ptr5);
									ptr5 = XmlUtf8RawTextWriter.RawStartCData(ptr5);
								}
								*ptr5 = 62;
								ptr5++;
								goto IL_273;
							}
							if (num == 93)
							{
								if (ptr5[-1] == 93)
								{
									this.hadDoubleBracket = true;
								}
								else
								{
									this.hadDoubleBracket = false;
								}
								*ptr5 = 93;
								ptr5++;
								goto IL_273;
							}
						}
						if (XmlCharType.IsSurrogate(num))
						{
							ptr5 = XmlUtf8RawTextWriter.EncodeSurrogate(ptr3, ptr4, ptr5);
							ptr3 += 2;
							continue;
						}
						if (num <= 127 || num >= 65534)
						{
							ptr5 = this.InvalidXmlChar(num, ptr5, false);
							ptr3++;
							continue;
						}
						ptr5 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr5);
						ptr3++;
						continue;
						IL_273:
						ptr3++;
						continue;
						IL_204:
						*ptr5 = (byte)num;
						ptr5++;
						goto IL_273;
					}
					this.bufPos = (int)((long)(ptr5 - ptr2));
					this.FlushBuffer();
					ptr5 = ptr2 + 1;
				}
				this.bufPos = (int)((long)(ptr5 - ptr2));
				array = null;
			}
		}

		// Token: 0x06000E03 RID: 3587 RVA: 0x00051EB1 File Offset: 0x000500B1
		private static bool IsSurrogateByte(byte b)
		{
			return (b & 248) == 240;
		}

		// Token: 0x06000E04 RID: 3588 RVA: 0x00051EC4 File Offset: 0x000500C4
		private unsafe static byte* EncodeSurrogate(char* pSrc, char* pSrcEnd, byte* pDst)
		{
			int num = (int)(*pSrc);
			if (num > 56319)
			{
				throw XmlConvert.CreateInvalidHighSurrogateCharException((char)num);
			}
			if (pSrc + 1 >= pSrcEnd)
			{
				throw new ArgumentException(Res.GetString("The surrogate pair is invalid. Missing a low surrogate character."));
			}
			int num2 = (int)pSrc[1];
			if (num2 >= 56320 && (LocalAppContextSwitches.DontThrowOnInvalidSurrogatePairs || num2 <= 57343))
			{
				num = XmlCharType.CombineSurrogateChar(num2, num);
				*pDst = (byte)(240 | num >> 18);
				pDst[1] = (byte)(128 | (num >> 12 & 63));
				pDst[2] = (byte)(128 | (num >> 6 & 63));
				pDst[3] = (byte)(128 | (num & 63));
				pDst += 4;
				return pDst;
			}
			throw XmlConvert.CreateInvalidSurrogatePairException((char)num2, (char)num);
		}

		// Token: 0x06000E05 RID: 3589 RVA: 0x00051F70 File Offset: 0x00050170
		private unsafe byte* InvalidXmlChar(int ch, byte* pDst, bool entitize)
		{
			if (this.checkCharacters)
			{
				throw XmlConvert.CreateInvalidCharException((char)ch, '\0');
			}
			if (entitize)
			{
				return XmlUtf8RawTextWriter.CharEntity(pDst, (char)ch);
			}
			if (ch < 128)
			{
				*pDst = (byte)ch;
				pDst++;
			}
			else
			{
				pDst = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(ch, pDst);
			}
			return pDst;
		}

		// Token: 0x06000E06 RID: 3590 RVA: 0x00051FAC File Offset: 0x000501AC
		internal unsafe void EncodeChar(ref char* pSrc, char* pSrcEnd, ref byte* pDst)
		{
			int num = (int)(*pSrc);
			if (XmlCharType.IsSurrogate(num))
			{
				pDst = XmlUtf8RawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, pDst);
				pSrc += (IntPtr)2 * 2;
				return;
			}
			if (num <= 127 || num >= 65534)
			{
				pDst = this.InvalidXmlChar(num, pDst, false);
				pSrc += 2;
				return;
			}
			pDst = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, pDst);
			pSrc += 2;
		}

		// Token: 0x06000E07 RID: 3591 RVA: 0x0005200C File Offset: 0x0005020C
		internal unsafe static byte* EncodeMultibyteUTF8(int ch, byte* pDst)
		{
			if (ch < 2048)
			{
				*pDst = (byte)(-64 | ch >> 6);
			}
			else
			{
				*pDst = (byte)(-32 | ch >> 12);
				pDst++;
				*pDst = (byte)(-128 | (ch >> 6 & 63));
			}
			pDst++;
			*pDst = (byte)(128 | (ch & 63));
			return pDst + 1;
		}

		// Token: 0x06000E08 RID: 3592 RVA: 0x0005205C File Offset: 0x0005025C
		internal unsafe static void CharToUTF8(ref char* pSrc, char* pSrcEnd, ref byte* pDst)
		{
			int num = (int)(*pSrc);
			if (num <= 127)
			{
				*pDst = (byte)num;
				pDst++;
				pSrc += 2;
				return;
			}
			if (XmlCharType.IsSurrogate(num))
			{
				pDst = XmlUtf8RawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, pDst);
				pSrc += (IntPtr)2 * 2;
				return;
			}
			pDst = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, pDst);
			pSrc += 2;
		}

		// Token: 0x06000E09 RID: 3593 RVA: 0x000520B4 File Offset: 0x000502B4
		protected unsafe byte* WriteNewLine(byte* pDst)
		{
			byte[] array;
			byte* ptr;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			this.bufPos = (int)((long)(pDst - ptr));
			this.RawText(this.newLineChars);
			return ptr + this.bufPos;
		}

		// Token: 0x06000E0A RID: 3594 RVA: 0x000520FF File Offset: 0x000502FF
		protected unsafe static byte* LtEntity(byte* pDst)
		{
			*pDst = 38;
			pDst[1] = 108;
			pDst[2] = 116;
			pDst[3] = 59;
			return pDst + 4;
		}

		// Token: 0x06000E0B RID: 3595 RVA: 0x0005211A File Offset: 0x0005031A
		protected unsafe static byte* GtEntity(byte* pDst)
		{
			*pDst = 38;
			pDst[1] = 103;
			pDst[2] = 116;
			pDst[3] = 59;
			return pDst + 4;
		}

		// Token: 0x06000E0C RID: 3596 RVA: 0x00052135 File Offset: 0x00050335
		protected unsafe static byte* AmpEntity(byte* pDst)
		{
			*pDst = 38;
			pDst[1] = 97;
			pDst[2] = 109;
			pDst[3] = 112;
			pDst[4] = 59;
			return pDst + 5;
		}

		// Token: 0x06000E0D RID: 3597 RVA: 0x00052156 File Offset: 0x00050356
		protected unsafe static byte* QuoteEntity(byte* pDst)
		{
			*pDst = 38;
			pDst[1] = 113;
			pDst[2] = 117;
			pDst[3] = 111;
			pDst[4] = 116;
			pDst[5] = 59;
			return pDst + 6;
		}

		// Token: 0x06000E0E RID: 3598 RVA: 0x0005217D File Offset: 0x0005037D
		protected unsafe static byte* TabEntity(byte* pDst)
		{
			*pDst = 38;
			pDst[1] = 35;
			pDst[2] = 120;
			pDst[3] = 57;
			pDst[4] = 59;
			return pDst + 5;
		}

		// Token: 0x06000E0F RID: 3599 RVA: 0x0005219E File Offset: 0x0005039E
		protected unsafe static byte* LineFeedEntity(byte* pDst)
		{
			*pDst = 38;
			pDst[1] = 35;
			pDst[2] = 120;
			pDst[3] = 65;
			pDst[4] = 59;
			return pDst + 5;
		}

		// Token: 0x06000E10 RID: 3600 RVA: 0x000521BF File Offset: 0x000503BF
		protected unsafe static byte* CarriageReturnEntity(byte* pDst)
		{
			*pDst = 38;
			pDst[1] = 35;
			pDst[2] = 120;
			pDst[3] = 68;
			pDst[4] = 59;
			return pDst + 5;
		}

		// Token: 0x06000E11 RID: 3601 RVA: 0x000521E0 File Offset: 0x000503E0
		private unsafe static byte* CharEntity(byte* pDst, char ch)
		{
			int num = (int)ch;
			string text = num.ToString("X", NumberFormatInfo.InvariantInfo);
			*pDst = 38;
			pDst[1] = 35;
			pDst[2] = 120;
			pDst += 3;
			fixed (string text2 = text)
			{
				char* ptr = text2;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char* ptr2 = ptr;
				while ((*(pDst++) = (byte)(*(ptr2++))) != 0)
				{
				}
			}
			pDst[-1] = 59;
			return pDst;
		}

		// Token: 0x06000E12 RID: 3602 RVA: 0x00052244 File Offset: 0x00050444
		protected unsafe static byte* RawStartCData(byte* pDst)
		{
			*pDst = 60;
			pDst[1] = 33;
			pDst[2] = 91;
			pDst[3] = 67;
			pDst[4] = 68;
			pDst[5] = 65;
			pDst[6] = 84;
			pDst[7] = 65;
			pDst[8] = 91;
			return pDst + 9;
		}

		// Token: 0x06000E13 RID: 3603 RVA: 0x0005227E File Offset: 0x0005047E
		protected unsafe static byte* RawEndCData(byte* pDst)
		{
			*pDst = 93;
			pDst[1] = 93;
			pDst[2] = 62;
			return pDst + 3;
		}

		// Token: 0x06000E14 RID: 3604 RVA: 0x00052294 File Offset: 0x00050494
		protected void ValidateContentChars(string chars, string propertyName, bool allowOnlyWhitespace)
		{
			if (!allowOnlyWhitespace)
			{
				for (int i = 0; i < chars.Length; i++)
				{
					if (!this.xmlCharType.IsTextChar(chars[i]))
					{
						char c = chars[i];
						if (c <= '&')
						{
							switch (c)
							{
							case '\t':
							case '\n':
							case '\r':
								goto IL_119;
							case '\v':
							case '\f':
								goto IL_A0;
							default:
								if (c != '&')
								{
									goto IL_A0;
								}
								break;
							}
						}
						else if (c != '<' && c != ']')
						{
							goto IL_A0;
						}
						string @string = Res.GetString("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(chars, i));
						goto IL_12A;
						IL_A0:
						if (XmlCharType.IsHighSurrogate((int)chars[i]))
						{
							if (i + 1 < chars.Length && XmlCharType.IsLowSurrogate((int)chars[i + 1]))
							{
								i++;
								goto IL_119;
							}
							@string = Res.GetString("The surrogate pair is invalid. Missing a low surrogate character.");
						}
						else
						{
							if (!XmlCharType.IsLowSurrogate((int)chars[i]))
							{
								goto IL_119;
							}
							@string = Res.GetString("Invalid high surrogate character (0x{0}). A high surrogate character must have a value from range (0xD800 - 0xDBFF).", new object[]
							{
								((uint)chars[i]).ToString("X", CultureInfo.InvariantCulture)
							});
						}
						IL_12A:
						throw new ArgumentException(Res.GetString("XmlWriterSettings.{0} can contain only valid XML text content characters when XmlWriterSettings.CheckCharacters is true. {1}", new string[]
						{
							propertyName,
							@string
						}));
					}
					IL_119:;
				}
				return;
			}
			if (!this.xmlCharType.IsOnlyWhitespace(chars))
			{
				throw new ArgumentException(Res.GetString("XmlWriterSettings.{0} can contain only valid XML white space characters when XmlWriterSettings.CheckCharacters and XmlWriterSettings.NewLineOnAttributes are true.", new object[]
				{
					propertyName
				}));
			}
		}

		// Token: 0x06000E15 RID: 3605 RVA: 0x000523E9 File Offset: 0x000505E9
		protected void CheckAsyncCall()
		{
			if (!this.useAsync)
			{
				throw new InvalidOperationException(Res.GetString("Set XmlWriterSettings.Async to true if you want to use Async Methods."));
			}
		}

		// Token: 0x06000E16 RID: 3606 RVA: 0x00052404 File Offset: 0x00050604
		internal override async Task WriteXmlDeclarationAsync(XmlStandalone standalone)
		{
			this.CheckAsyncCall();
			if (!this.omitXmlDeclaration && !this.autoXmlDeclaration)
			{
				await this.RawTextAsync("<?xml version=\"").ConfigureAwait(false);
				await this.RawTextAsync("1.0").ConfigureAwait(false);
				if (this.encoding != null)
				{
					await this.RawTextAsync("\" encoding=\"").ConfigureAwait(false);
					await this.RawTextAsync(this.encoding.WebName).ConfigureAwait(false);
				}
				if (standalone != XmlStandalone.Omit)
				{
					await this.RawTextAsync("\" standalone=\"").ConfigureAwait(false);
					await this.RawTextAsync((standalone == XmlStandalone.Yes) ? "yes" : "no").ConfigureAwait(false);
				}
				await this.RawTextAsync("\"?>").ConfigureAwait(false);
			}
		}

		// Token: 0x06000E17 RID: 3607 RVA: 0x00052451 File Offset: 0x00050651
		internal override Task WriteXmlDeclarationAsync(string xmldecl)
		{
			this.CheckAsyncCall();
			if (!this.omitXmlDeclaration && !this.autoXmlDeclaration)
			{
				return this.WriteProcessingInstructionAsync("xml", xmldecl);
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000E18 RID: 3608 RVA: 0x0005247C File Offset: 0x0005067C
		public override async Task WriteDocTypeAsync(string name, string pubid, string sysid, string subset)
		{
			this.CheckAsyncCall();
			await this.RawTextAsync("<!DOCTYPE ").ConfigureAwait(false);
			await this.RawTextAsync(name).ConfigureAwait(false);
			int num;
			if (pubid != null)
			{
				await this.RawTextAsync(" PUBLIC \"").ConfigureAwait(false);
				await this.RawTextAsync(pubid).ConfigureAwait(false);
				await this.RawTextAsync("\" \"").ConfigureAwait(false);
				if (sysid != null)
				{
					await this.RawTextAsync(sysid).ConfigureAwait(false);
				}
				byte[] array = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 34;
			}
			else if (sysid != null)
			{
				await this.RawTextAsync(" SYSTEM \"").ConfigureAwait(false);
				await this.RawTextAsync(sysid).ConfigureAwait(false);
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 34;
			}
			else
			{
				byte[] array3 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 32;
			}
			if (subset != null)
			{
				byte[] array4 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 91;
				await this.RawTextAsync(subset).ConfigureAwait(false);
				byte[] array5 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array5[num] = 93;
			}
			byte[] array6 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 62;
		}

		// Token: 0x06000E19 RID: 3609 RVA: 0x000524E4 File Offset: 0x000506E4
		public override Task WriteStartElementAsync(string prefix, string localName, string ns)
		{
			this.CheckAsyncCall();
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			Task task;
			if (prefix != null && prefix.Length != 0)
			{
				task = this.RawTextAsync(prefix + ":" + localName);
			}
			else
			{
				task = this.RawTextAsync(localName);
			}
			return task.CallVoidFuncWhenFinish(new Action(this.WriteStartElementAsync_SetAttEndPos));
		}

		// Token: 0x06000E1A RID: 3610 RVA: 0x0005254B File Offset: 0x0005074B
		private void WriteStartElementAsync_SetAttEndPos()
		{
			this.attrEndPos = this.bufPos;
		}

		// Token: 0x06000E1B RID: 3611 RVA: 0x0005255C File Offset: 0x0005075C
		internal override Task WriteEndElementAsync(string prefix, string localName, string ns)
		{
			this.CheckAsyncCall();
			int num;
			if (this.contentPos == this.bufPos)
			{
				this.bufPos--;
				byte[] array = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 32;
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 47;
				byte[] array3 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 62;
				return AsyncHelper.DoneTask;
			}
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 60;
			byte[] array5 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 47;
			if (prefix != null && prefix.Length != 0)
			{
				return this.RawTextAsync(prefix + ":" + localName + ">");
			}
			return this.RawTextAsync(localName + ">");
		}

		// Token: 0x06000E1C RID: 3612 RVA: 0x00052648 File Offset: 0x00050848
		internal override Task WriteFullEndElementAsync(string prefix, string localName, string ns)
		{
			this.CheckAsyncCall();
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 47;
			if (prefix != null && prefix.Length != 0)
			{
				return this.RawTextAsync(prefix + ":" + localName + ">");
			}
			return this.RawTextAsync(localName + ">");
		}

		// Token: 0x06000E1D RID: 3613 RVA: 0x000526C4 File Offset: 0x000508C4
		protected internal override Task WriteStartAttributeAsync(string prefix, string localName, string ns)
		{
			this.CheckAsyncCall();
			if (this.attrEndPos == this.bufPos)
			{
				byte[] array = this.bufBytes;
				int num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 32;
			}
			Task task;
			if (prefix != null && prefix.Length > 0)
			{
				task = this.RawTextAsync(prefix + ":" + localName + "=\"");
			}
			else
			{
				task = this.RawTextAsync(localName + "=\"");
			}
			return task.CallVoidFuncWhenFinish(new Action(this.WriteStartAttribute_SetInAttribute));
		}

		// Token: 0x06000E1E RID: 3614 RVA: 0x0001B170 File Offset: 0x00019370
		private void WriteStartAttribute_SetInAttribute()
		{
			this.inAttributeValue = true;
		}

		// Token: 0x06000E1F RID: 3615 RVA: 0x0005274C File Offset: 0x0005094C
		protected internal override Task WriteEndAttributeAsync()
		{
			this.CheckAsyncCall();
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 34;
			this.inAttributeValue = false;
			this.attrEndPos = this.bufPos;
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000E20 RID: 3616 RVA: 0x00052794 File Offset: 0x00050994
		internal override async Task WriteNamespaceDeclarationAsync(string prefix, string namespaceName)
		{
			this.CheckAsyncCall();
			await this.WriteStartNamespaceDeclarationAsync(prefix).ConfigureAwait(false);
			await this.WriteStringAsync(namespaceName).ConfigureAwait(false);
			await this.WriteEndNamespaceDeclarationAsync().ConfigureAwait(false);
		}

		// Token: 0x06000E21 RID: 3617 RVA: 0x000527EC File Offset: 0x000509EC
		internal override async Task WriteStartNamespaceDeclarationAsync(string prefix)
		{
			this.CheckAsyncCall();
			if (prefix.Length == 0)
			{
				await this.RawTextAsync(" xmlns=\"").ConfigureAwait(false);
			}
			else
			{
				await this.RawTextAsync(" xmlns:").ConfigureAwait(false);
				await this.RawTextAsync(prefix).ConfigureAwait(false);
				byte[] array = this.bufBytes;
				int num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 61;
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 34;
			}
			this.inAttributeValue = true;
		}

		// Token: 0x06000E22 RID: 3618 RVA: 0x0005283C File Offset: 0x00050A3C
		internal override Task WriteEndNamespaceDeclarationAsync()
		{
			this.CheckAsyncCall();
			this.inAttributeValue = false;
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 34;
			this.attrEndPos = this.bufPos;
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000E23 RID: 3619 RVA: 0x00052884 File Offset: 0x00050A84
		public override async Task WriteCDataAsync(string text)
		{
			this.CheckAsyncCall();
			int num;
			if (this.mergeCDataSections && this.bufPos == this.cdataPos)
			{
				this.bufPos -= 3;
			}
			else
			{
				byte[] array = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array[num] = 60;
				byte[] array2 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array2[num] = 33;
				byte[] array3 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 91;
				byte[] array4 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array4[num] = 67;
				byte[] array5 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array5[num] = 68;
				byte[] array6 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array6[num] = 65;
				byte[] array7 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array7[num] = 84;
				byte[] array8 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array8[num] = 65;
				byte[] array9 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array9[num] = 91;
			}
			await this.WriteCDataSectionAsync(text).ConfigureAwait(false);
			byte[] array10 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array10[num] = 93;
			byte[] array11 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array11[num] = 93;
			byte[] array12 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array12[num] = 62;
			this.textPos = this.bufPos;
			this.cdataPos = this.bufPos;
		}

		// Token: 0x06000E24 RID: 3620 RVA: 0x000528D4 File Offset: 0x00050AD4
		public override async Task WriteCommentAsync(string text)
		{
			this.CheckAsyncCall();
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 33;
			byte[] array3 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 45;
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 45;
			await this.WriteCommentOrPiAsync(text, 45).ConfigureAwait(false);
			byte[] array5 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 45;
			byte[] array6 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array6[num] = 45;
			byte[] array7 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array7[num] = 62;
		}

		// Token: 0x06000E25 RID: 3621 RVA: 0x00052924 File Offset: 0x00050B24
		public override async Task WriteProcessingInstructionAsync(string name, string text)
		{
			this.CheckAsyncCall();
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 60;
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 63;
			await this.RawTextAsync(name).ConfigureAwait(false);
			if (text.Length > 0)
			{
				byte[] array3 = this.bufBytes;
				num = this.bufPos;
				this.bufPos = num + 1;
				array3[num] = 32;
				await this.WriteCommentOrPiAsync(text, 63).ConfigureAwait(false);
			}
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 63;
			byte[] array5 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array5[num] = 62;
		}

		// Token: 0x06000E26 RID: 3622 RVA: 0x0005297C File Offset: 0x00050B7C
		public override async Task WriteEntityRefAsync(string name)
		{
			this.CheckAsyncCall();
			byte[] array = this.bufBytes;
			int num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 38;
			await this.RawTextAsync(name).ConfigureAwait(false);
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 59;
			if (this.bufPos > this.bufLen)
			{
				await this.FlushBufferAsync().ConfigureAwait(false);
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x06000E27 RID: 3623 RVA: 0x000529CC File Offset: 0x00050BCC
		public override async Task WriteCharEntityAsync(char ch)
		{
			this.CheckAsyncCall();
			int num = (int)ch;
			string text = num.ToString("X", NumberFormatInfo.InvariantInfo);
			if (this.checkCharacters && !this.xmlCharType.IsCharData(ch))
			{
				throw XmlConvert.CreateInvalidCharException(ch, '\0');
			}
			byte[] array = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array[num] = 38;
			byte[] array2 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array2[num] = 35;
			byte[] array3 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array3[num] = 120;
			await this.RawTextAsync(text).ConfigureAwait(false);
			byte[] array4 = this.bufBytes;
			num = this.bufPos;
			this.bufPos = num + 1;
			array4[num] = 59;
			if (this.bufPos > this.bufLen)
			{
				await this.FlushBufferAsync().ConfigureAwait(false);
			}
			this.textPos = this.bufPos;
		}

		// Token: 0x06000E28 RID: 3624 RVA: 0x00052A19 File Offset: 0x00050C19
		public override Task WriteWhitespaceAsync(string ws)
		{
			this.CheckAsyncCall();
			if (this.inAttributeValue)
			{
				return this.WriteAttributeTextBlockAsync(ws);
			}
			return this.WriteElementTextBlockAsync(ws);
		}

		// Token: 0x06000E29 RID: 3625 RVA: 0x00052A19 File Offset: 0x00050C19
		public override Task WriteStringAsync(string text)
		{
			this.CheckAsyncCall();
			if (this.inAttributeValue)
			{
				return this.WriteAttributeTextBlockAsync(text);
			}
			return this.WriteElementTextBlockAsync(text);
		}

		// Token: 0x06000E2A RID: 3626 RVA: 0x00052A38 File Offset: 0x00050C38
		public override async Task WriteSurrogateCharEntityAsync(char lowChar, char highChar)
		{
			this.CheckAsyncCall();
			int num = XmlCharType.CombineSurrogateChar((int)lowChar, (int)highChar);
			byte[] array = this.bufBytes;
			int num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array[num2] = 38;
			byte[] array2 = this.bufBytes;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array2[num2] = 35;
			byte[] array3 = this.bufBytes;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array3[num2] = 120;
			await this.RawTextAsync(num.ToString("X", NumberFormatInfo.InvariantInfo)).ConfigureAwait(false);
			byte[] array4 = this.bufBytes;
			num2 = this.bufPos;
			this.bufPos = num2 + 1;
			array4[num2] = 59;
			this.textPos = this.bufPos;
		}

		// Token: 0x06000E2B RID: 3627 RVA: 0x00052A8D File Offset: 0x00050C8D
		public override Task WriteCharsAsync(char[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			if (this.inAttributeValue)
			{
				return this.WriteAttributeTextBlockAsync(buffer, index, count);
			}
			return this.WriteElementTextBlockAsync(buffer, index, count);
		}

		// Token: 0x06000E2C RID: 3628 RVA: 0x00052AB0 File Offset: 0x00050CB0
		public override async Task WriteRawAsync(char[] buffer, int index, int count)
		{
			this.CheckAsyncCall();
			await this.WriteRawWithCharCheckingAsync(buffer, index, count).ConfigureAwait(false);
			this.textPos = this.bufPos;
		}

		// Token: 0x06000E2D RID: 3629 RVA: 0x00052B10 File Offset: 0x00050D10
		public override async Task WriteRawAsync(string data)
		{
			this.CheckAsyncCall();
			await this.WriteRawWithCharCheckingAsync(data).ConfigureAwait(false);
			this.textPos = this.bufPos;
		}

		// Token: 0x06000E2E RID: 3630 RVA: 0x00052B60 File Offset: 0x00050D60
		public override async Task FlushAsync()
		{
			this.CheckAsyncCall();
			await this.FlushBufferAsync().ConfigureAwait(false);
			await this.FlushEncoderAsync().ConfigureAwait(false);
			if (this.stream != null)
			{
				await this.stream.FlushAsync().ConfigureAwait(false);
			}
		}

		// Token: 0x06000E2F RID: 3631 RVA: 0x00052BA8 File Offset: 0x00050DA8
		protected virtual async Task FlushBufferAsync()
		{
			try
			{
				if (!this.writeToNull)
				{
					await this.stream.WriteAsync(this.bufBytes, 1, this.bufPos - 1).ConfigureAwait(false);
				}
			}
			catch
			{
				this.writeToNull = true;
				throw;
			}
			finally
			{
				this.bufBytes[0] = this.bufBytes[this.bufPos - 1];
				if (XmlUtf8RawTextWriter.IsSurrogateByte(this.bufBytes[0]))
				{
					this.bufBytes[1] = this.bufBytes[this.bufPos];
					this.bufBytes[2] = this.bufBytes[this.bufPos + 1];
					this.bufBytes[3] = this.bufBytes[this.bufPos + 2];
				}
				this.textPos = ((this.textPos == this.bufPos) ? 1 : 0);
				this.attrEndPos = ((this.attrEndPos == this.bufPos) ? 1 : 0);
				this.contentPos = 0;
				this.cdataPos = 0;
				this.bufPos = 1;
			}
		}

		// Token: 0x06000E30 RID: 3632 RVA: 0x000294B6 File Offset: 0x000276B6
		private Task FlushEncoderAsync()
		{
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000E31 RID: 3633 RVA: 0x00052BF0 File Offset: 0x00050DF0
		[SecuritySafeCritical]
		protected unsafe int WriteAttributeTextBlockNoFlush(char* pSrc, char* pSrcEnd)
		{
			char* ptr = pSrc;
			byte[] array;
			byte* ptr2;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr2 = null;
			}
			else
			{
				ptr2 = &array[0];
			}
			byte* ptr3 = ptr2 + this.bufPos;
			int num = 0;
			for (;;)
			{
				byte* ptr4 = ptr3 + (long)(pSrcEnd - pSrc);
				if (ptr4 != ptr2 + this.bufLen)
				{
					ptr4 = ptr2 + this.bufLen;
				}
				while (ptr3 < ptr4 && (this.xmlCharType.charProperties[num = (int)(*pSrc)] & 128) != 0 && num <= 127)
				{
					*ptr3 = (byte)num;
					ptr3++;
					pSrc++;
				}
				if (pSrc >= pSrcEnd)
				{
					goto IL_1E8;
				}
				if (ptr3 >= ptr4)
				{
					break;
				}
				if (num <= 38)
				{
					switch (num)
					{
					case 9:
						if (this.newLineHandling == NewLineHandling.None)
						{
							*ptr3 = (byte)num;
							ptr3++;
							goto IL_1DE;
						}
						ptr3 = XmlUtf8RawTextWriter.TabEntity(ptr3);
						goto IL_1DE;
					case 10:
						if (this.newLineHandling == NewLineHandling.None)
						{
							*ptr3 = (byte)num;
							ptr3++;
							goto IL_1DE;
						}
						ptr3 = XmlUtf8RawTextWriter.LineFeedEntity(ptr3);
						goto IL_1DE;
					case 11:
					case 12:
						break;
					case 13:
						if (this.newLineHandling == NewLineHandling.None)
						{
							*ptr3 = (byte)num;
							ptr3++;
							goto IL_1DE;
						}
						ptr3 = XmlUtf8RawTextWriter.CarriageReturnEntity(ptr3);
						goto IL_1DE;
					default:
						if (num == 34)
						{
							ptr3 = XmlUtf8RawTextWriter.QuoteEntity(ptr3);
							goto IL_1DE;
						}
						if (num == 38)
						{
							ptr3 = XmlUtf8RawTextWriter.AmpEntity(ptr3);
							goto IL_1DE;
						}
						break;
					}
				}
				else
				{
					if (num == 39)
					{
						*ptr3 = (byte)num;
						ptr3++;
						goto IL_1DE;
					}
					if (num == 60)
					{
						ptr3 = XmlUtf8RawTextWriter.LtEntity(ptr3);
						goto IL_1DE;
					}
					if (num == 62)
					{
						ptr3 = XmlUtf8RawTextWriter.GtEntity(ptr3);
						goto IL_1DE;
					}
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr3 = XmlUtf8RawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, ptr3);
					pSrc += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr3 = this.InvalidXmlChar(num, ptr3, true);
					pSrc++;
					continue;
				}
				ptr3 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr3);
				pSrc++;
				continue;
				IL_1DE:
				pSrc++;
			}
			this.bufPos = (int)((long)(ptr3 - ptr2));
			return (int)((long)(pSrc - ptr));
			IL_1E8:
			this.bufPos = (int)((long)(ptr3 - ptr2));
			array = null;
			return -1;
		}

		// Token: 0x06000E32 RID: 3634 RVA: 0x00052DF8 File Offset: 0x00050FF8
		[SecuritySafeCritical]
		protected unsafe int WriteAttributeTextBlockNoFlush(char[] chars, int index, int count)
		{
			if (count == 0)
			{
				return -1;
			}
			fixed (char* ptr = &chars[index])
			{
				char* ptr2 = ptr;
				char* pSrcEnd = ptr2 + count;
				return this.WriteAttributeTextBlockNoFlush(ptr2, pSrcEnd);
			}
		}

		// Token: 0x06000E33 RID: 3635 RVA: 0x00052E24 File Offset: 0x00051024
		[SecuritySafeCritical]
		protected unsafe int WriteAttributeTextBlockNoFlush(string text, int index, int count)
		{
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char* pSrcEnd = ptr2 + count;
			return this.WriteAttributeTextBlockNoFlush(ptr2, pSrcEnd);
		}

		// Token: 0x06000E34 RID: 3636 RVA: 0x00052E5C File Offset: 0x0005105C
		protected async Task WriteAttributeTextBlockAsync(char[] chars, int index, int count)
		{
			int writeLen = 0;
			int curIndex = index;
			int leftCount = count;
			do
			{
				writeLen = this.WriteAttributeTextBlockNoFlush(chars, curIndex, leftCount);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0);
		}

		// Token: 0x06000E35 RID: 3637 RVA: 0x00052EBC File Offset: 0x000510BC
		protected Task WriteAttributeTextBlockAsync(string text)
		{
			int num = 0;
			int num2 = text.Length;
			int num3 = this.WriteAttributeTextBlockNoFlush(text, num, num2);
			num += num3;
			num2 -= num3;
			if (num3 >= 0)
			{
				return this._WriteAttributeTextBlockAsync(text, num, num2);
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000E36 RID: 3638 RVA: 0x00052EFC File Offset: 0x000510FC
		private async Task _WriteAttributeTextBlockAsync(string text, int curIndex, int leftCount)
		{
			await this.FlushBufferAsync().ConfigureAwait(false);
			int writeLen;
			do
			{
				writeLen = this.WriteAttributeTextBlockNoFlush(text, curIndex, leftCount);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0);
		}

		// Token: 0x06000E37 RID: 3639 RVA: 0x00052F5C File Offset: 0x0005115C
		[SecuritySafeCritical]
		protected unsafe int WriteElementTextBlockNoFlush(char* pSrc, char* pSrcEnd, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			char* ptr = pSrc;
			byte[] array;
			byte* ptr2;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr2 = null;
			}
			else
			{
				ptr2 = &array[0];
			}
			byte* ptr3 = ptr2 + this.bufPos;
			int num = 0;
			for (;;)
			{
				byte* ptr4 = ptr3 + (long)(pSrcEnd - pSrc);
				if (ptr4 != ptr2 + this.bufLen)
				{
					ptr4 = ptr2 + this.bufLen;
				}
				while (ptr3 < ptr4 && (this.xmlCharType.charProperties[num = (int)(*pSrc)] & 128) != 0 && num <= 127)
				{
					*ptr3 = (byte)num;
					ptr3++;
					pSrc++;
				}
				if (pSrc >= pSrcEnd)
				{
					goto IL_209;
				}
				if (ptr3 >= ptr4)
				{
					break;
				}
				if (num <= 38)
				{
					switch (num)
					{
					case 9:
						goto IL_114;
					case 10:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_14;
						}
						*ptr3 = (byte)num;
						ptr3++;
						goto IL_1FF;
					case 11:
					case 12:
						break;
					case 13:
						switch (this.newLineHandling)
						{
						case NewLineHandling.Replace:
							goto IL_170;
						case NewLineHandling.Entitize:
							ptr3 = XmlUtf8RawTextWriter.CarriageReturnEntity(ptr3);
							goto IL_1FF;
						case NewLineHandling.None:
							*ptr3 = (byte)num;
							ptr3++;
							goto IL_1FF;
						default:
							goto IL_1FF;
						}
						break;
					default:
						if (num == 34)
						{
							goto IL_114;
						}
						if (num == 38)
						{
							ptr3 = XmlUtf8RawTextWriter.AmpEntity(ptr3);
							goto IL_1FF;
						}
						break;
					}
				}
				else
				{
					if (num == 39)
					{
						goto IL_114;
					}
					if (num == 60)
					{
						ptr3 = XmlUtf8RawTextWriter.LtEntity(ptr3);
						goto IL_1FF;
					}
					if (num == 62)
					{
						ptr3 = XmlUtf8RawTextWriter.GtEntity(ptr3);
						goto IL_1FF;
					}
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr3 = XmlUtf8RawTextWriter.EncodeSurrogate(pSrc, pSrcEnd, ptr3);
					pSrc += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr3 = this.InvalidXmlChar(num, ptr3, true);
					pSrc++;
					continue;
				}
				ptr3 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr3);
				pSrc++;
				continue;
				IL_1FF:
				pSrc++;
				continue;
				IL_114:
				*ptr3 = (byte)num;
				ptr3++;
				goto IL_1FF;
			}
			this.bufPos = (int)((long)(ptr3 - ptr2));
			return (int)((long)(pSrc - ptr));
			Block_14:
			this.bufPos = (int)((long)(ptr3 - ptr2));
			needWriteNewLine = true;
			return (int)((long)(pSrc - ptr));
			IL_170:
			if (pSrc[1] == '\n')
			{
				pSrc++;
			}
			this.bufPos = (int)((long)(ptr3 - ptr2));
			needWriteNewLine = true;
			return (int)((long)(pSrc - ptr));
			IL_209:
			this.bufPos = (int)((long)(ptr3 - ptr2));
			this.textPos = this.bufPos;
			this.contentPos = 0;
			array = null;
			return -1;
		}

		// Token: 0x06000E38 RID: 3640 RVA: 0x00053198 File Offset: 0x00051398
		[SecuritySafeCritical]
		protected unsafe int WriteElementTextBlockNoFlush(char[] chars, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				this.contentPos = 0;
				return -1;
			}
			fixed (char* ptr = &chars[index])
			{
				char* ptr2 = ptr;
				char* pSrcEnd = ptr2 + count;
				return this.WriteElementTextBlockNoFlush(ptr2, pSrcEnd, out needWriteNewLine);
			}
		}

		// Token: 0x06000E39 RID: 3641 RVA: 0x000531D4 File Offset: 0x000513D4
		[SecuritySafeCritical]
		protected unsafe int WriteElementTextBlockNoFlush(string text, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				this.contentPos = 0;
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char* pSrcEnd = ptr2 + count;
			return this.WriteElementTextBlockNoFlush(ptr2, pSrcEnd, out needWriteNewLine);
		}

		// Token: 0x06000E3A RID: 3642 RVA: 0x0005321C File Offset: 0x0005141C
		protected async Task WriteElementTextBlockAsync(char[] chars, int index, int count)
		{
			int writeLen = 0;
			int curIndex = index;
			int leftCount = count;
			bool needWriteNewLine = false;
			do
			{
				writeLen = this.WriteElementTextBlockNoFlush(chars, curIndex, leftCount, out needWriteNewLine);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (needWriteNewLine)
				{
					await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
					curIndex++;
					leftCount--;
				}
				else if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0 || needWriteNewLine);
		}

		// Token: 0x06000E3B RID: 3643 RVA: 0x0005327C File Offset: 0x0005147C
		protected Task WriteElementTextBlockAsync(string text)
		{
			int num = 0;
			int num2 = text.Length;
			bool flag = false;
			int num3 = this.WriteElementTextBlockNoFlush(text, num, num2, out flag);
			num += num3;
			num2 -= num3;
			if (flag)
			{
				return this._WriteElementTextBlockAsync(true, text, num, num2);
			}
			if (num3 >= 0)
			{
				return this._WriteElementTextBlockAsync(false, text, num, num2);
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000E3C RID: 3644 RVA: 0x000532CC File Offset: 0x000514CC
		private async Task _WriteElementTextBlockAsync(bool newLine, string text, int curIndex, int leftCount)
		{
			int writeLen = 0;
			bool needWriteNewLine = false;
			if (newLine)
			{
				await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
				curIndex++;
				leftCount--;
			}
			else
			{
				await this.FlushBufferAsync().ConfigureAwait(false);
			}
			do
			{
				writeLen = this.WriteElementTextBlockNoFlush(text, curIndex, leftCount, out needWriteNewLine);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (needWriteNewLine)
				{
					await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
					curIndex++;
					leftCount--;
				}
				else if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0 || needWriteNewLine);
		}

		// Token: 0x06000E3D RID: 3645 RVA: 0x00053334 File Offset: 0x00051534
		[SecuritySafeCritical]
		protected unsafe int RawTextNoFlush(char* pSrcBegin, char* pSrcEnd)
		{
			byte[] array;
			byte* ptr;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			byte* ptr2 = ptr + this.bufPos;
			char* ptr3 = pSrcBegin;
			int num = 0;
			for (;;)
			{
				byte* ptr4 = ptr2 + (long)(pSrcEnd - ptr3);
				if (ptr4 != ptr + this.bufLen)
				{
					ptr4 = ptr + this.bufLen;
				}
				while (ptr2 < ptr4 && (num = (int)(*ptr3)) <= 127)
				{
					ptr3++;
					*ptr2 = (byte)num;
					ptr2++;
				}
				if (ptr3 >= pSrcEnd)
				{
					goto IL_E7;
				}
				if (ptr2 >= ptr4)
				{
					break;
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr2 = XmlUtf8RawTextWriter.EncodeSurrogate(ptr3, pSrcEnd, ptr2);
					ptr3 += 2;
				}
				else if (num <= 127 || num >= 65534)
				{
					ptr2 = this.InvalidXmlChar(num, ptr2, false);
					ptr3++;
				}
				else
				{
					ptr2 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr2);
					ptr3++;
				}
			}
			this.bufPos = (int)((long)(ptr2 - ptr));
			return (int)((long)(ptr3 - pSrcBegin));
			IL_E7:
			this.bufPos = (int)((long)(ptr2 - ptr));
			array = null;
			return -1;
		}

		// Token: 0x06000E3E RID: 3646 RVA: 0x00053438 File Offset: 0x00051638
		[SecuritySafeCritical]
		protected unsafe int RawTextNoFlush(string text, int index, int count)
		{
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char* pSrcEnd = ptr2 + count;
			return this.RawTextNoFlush(ptr2, pSrcEnd);
		}

		// Token: 0x06000E3F RID: 3647 RVA: 0x00053470 File Offset: 0x00051670
		protected Task RawTextAsync(string text)
		{
			int num = 0;
			int num2 = text.Length;
			int num3 = this.RawTextNoFlush(text, num, num2);
			num += num3;
			num2 -= num3;
			if (num3 >= 0)
			{
				return this._RawTextAsync(text, num, num2);
			}
			return AsyncHelper.DoneTask;
		}

		// Token: 0x06000E40 RID: 3648 RVA: 0x000534B0 File Offset: 0x000516B0
		private async Task _RawTextAsync(string text, int curIndex, int leftCount)
		{
			await this.FlushBufferAsync().ConfigureAwait(false);
			int writeLen = 0;
			do
			{
				writeLen = this.RawTextNoFlush(text, curIndex, leftCount);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0);
		}

		// Token: 0x06000E41 RID: 3649 RVA: 0x00053510 File Offset: 0x00051710
		[SecuritySafeCritical]
		protected unsafe int WriteRawWithCharCheckingNoFlush(char* pSrcBegin, char* pSrcEnd, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			byte[] array;
			byte* ptr;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr = null;
			}
			else
			{
				ptr = &array[0];
			}
			char* ptr2 = pSrcBegin;
			byte* ptr3 = ptr + this.bufPos;
			int num = 0;
			for (;;)
			{
				byte* ptr4 = ptr3 + (long)(pSrcEnd - ptr2);
				if (ptr4 != ptr + this.bufLen)
				{
					ptr4 = ptr + this.bufLen;
				}
				while (ptr3 < ptr4 && (this.xmlCharType.charProperties[num = (int)(*ptr2)] & 64) != 0 && num <= 127)
				{
					*ptr3 = (byte)num;
					ptr3++;
					ptr2++;
				}
				if (ptr2 >= pSrcEnd)
				{
					goto IL_1C5;
				}
				if (ptr3 >= ptr4)
				{
					break;
				}
				if (num <= 38)
				{
					switch (num)
					{
					case 9:
						goto IL_E5;
					case 10:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_13;
						}
						*ptr3 = (byte)num;
						ptr3++;
						goto IL_1BC;
					case 11:
					case 12:
						break;
					case 13:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_11;
						}
						*ptr3 = (byte)num;
						ptr3++;
						goto IL_1BC;
					default:
						if (num == 38)
						{
							goto IL_E5;
						}
						break;
					}
				}
				else if (num == 60 || num == 93)
				{
					goto IL_E5;
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr3 = XmlUtf8RawTextWriter.EncodeSurrogate(ptr2, pSrcEnd, ptr3);
					ptr2 += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr3 = this.InvalidXmlChar(num, ptr3, false);
					ptr2++;
					continue;
				}
				ptr3 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr3);
				ptr2++;
				continue;
				IL_1BC:
				ptr2++;
				continue;
				IL_E5:
				*ptr3 = (byte)num;
				ptr3++;
				goto IL_1BC;
			}
			this.bufPos = (int)((long)(ptr3 - ptr));
			return (int)((long)(ptr2 - pSrcBegin));
			Block_11:
			if (ptr2[1] == '\n')
			{
				ptr2++;
			}
			this.bufPos = (int)((long)(ptr3 - ptr));
			needWriteNewLine = true;
			return (int)((long)(ptr2 - pSrcBegin));
			Block_13:
			this.bufPos = (int)((long)(ptr3 - ptr));
			needWriteNewLine = true;
			return (int)((long)(ptr2 - pSrcBegin));
			IL_1C5:
			this.bufPos = (int)((long)(ptr3 - ptr));
			array = null;
			return -1;
		}

		// Token: 0x06000E42 RID: 3650 RVA: 0x000536F4 File Offset: 0x000518F4
		[SecuritySafeCritical]
		protected unsafe int WriteRawWithCharCheckingNoFlush(char[] chars, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				return -1;
			}
			fixed (char* ptr = &chars[index])
			{
				char* ptr2 = ptr;
				char* pSrcEnd = ptr2 + count;
				return this.WriteRawWithCharCheckingNoFlush(ptr2, pSrcEnd, out needWriteNewLine);
			}
		}

		// Token: 0x06000E43 RID: 3651 RVA: 0x00053728 File Offset: 0x00051928
		[SecuritySafeCritical]
		protected unsafe int WriteRawWithCharCheckingNoFlush(string text, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			char* pSrcEnd = ptr2 + count;
			return this.WriteRawWithCharCheckingNoFlush(ptr2, pSrcEnd, out needWriteNewLine);
		}

		// Token: 0x06000E44 RID: 3652 RVA: 0x00053768 File Offset: 0x00051968
		protected async Task WriteRawWithCharCheckingAsync(char[] chars, int index, int count)
		{
			int writeLen = 0;
			int curIndex = index;
			int leftCount = count;
			bool needWriteNewLine = false;
			do
			{
				writeLen = this.WriteRawWithCharCheckingNoFlush(chars, curIndex, leftCount, out needWriteNewLine);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (needWriteNewLine)
				{
					await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
					curIndex++;
					leftCount--;
				}
				else if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0 || needWriteNewLine);
		}

		// Token: 0x06000E45 RID: 3653 RVA: 0x000537C8 File Offset: 0x000519C8
		protected async Task WriteRawWithCharCheckingAsync(string text)
		{
			int writeLen = 0;
			int curIndex = 0;
			int leftCount = text.Length;
			bool needWriteNewLine = false;
			do
			{
				writeLen = this.WriteRawWithCharCheckingNoFlush(text, curIndex, leftCount, out needWriteNewLine);
				curIndex += writeLen;
				leftCount -= writeLen;
				if (needWriteNewLine)
				{
					await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
					curIndex++;
					leftCount--;
				}
				else if (writeLen >= 0)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			while (writeLen >= 0 || needWriteNewLine);
		}

		// Token: 0x06000E46 RID: 3654 RVA: 0x00053818 File Offset: 0x00051A18
		[SecuritySafeCritical]
		protected unsafe int WriteCommentOrPiNoFlush(string text, int index, int count, int stopChar, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			byte[] array;
			byte* ptr3;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr3 = null;
			}
			else
			{
				ptr3 = &array[0];
			}
			char* ptr4 = ptr2;
			char* ptr5 = ptr4;
			char* ptr6 = ptr2 + count;
			byte* ptr7 = ptr3 + this.bufPos;
			int num = 0;
			for (;;)
			{
				byte* ptr8 = ptr7 + (long)(ptr6 - ptr4);
				if (ptr8 != ptr3 + this.bufLen)
				{
					ptr8 = ptr3 + this.bufLen;
				}
				while (ptr7 < ptr8 && (this.xmlCharType.charProperties[num = (int)(*ptr4)] & 64) != 0 && num != stopChar && num <= 127)
				{
					*ptr7 = (byte)num;
					ptr7++;
					ptr4++;
				}
				if (ptr4 >= ptr6)
				{
					goto IL_2A4;
				}
				if (ptr7 >= ptr8)
				{
					break;
				}
				if (num <= 45)
				{
					switch (num)
					{
					case 9:
						goto IL_22A;
					case 10:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_24;
						}
						*ptr7 = (byte)num;
						ptr7++;
						goto IL_299;
					case 11:
					case 12:
						break;
					case 13:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_22;
						}
						*ptr7 = (byte)num;
						ptr7++;
						goto IL_299;
					default:
						if (num == 38)
						{
							goto IL_22A;
						}
						if (num == 45)
						{
							*ptr7 = 45;
							ptr7++;
							if (num == stopChar && (ptr4 + 1 == ptr6 || ptr4[1] == '-'))
							{
								*ptr7 = 32;
								ptr7++;
								goto IL_299;
							}
							goto IL_299;
						}
						break;
					}
				}
				else
				{
					if (num == 60)
					{
						goto IL_22A;
					}
					if (num != 63)
					{
						if (num == 93)
						{
							*ptr7 = 93;
							ptr7++;
							goto IL_299;
						}
					}
					else
					{
						*ptr7 = 63;
						ptr7++;
						if (num == stopChar && ptr4 + 1 < ptr6 && ptr4[1] == '>')
						{
							*ptr7 = 32;
							ptr7++;
							goto IL_299;
						}
						goto IL_299;
					}
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr7 = XmlUtf8RawTextWriter.EncodeSurrogate(ptr4, ptr6, ptr7);
					ptr4 += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr7 = this.InvalidXmlChar(num, ptr7, false);
					ptr4++;
					continue;
				}
				ptr7 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr7);
				ptr4++;
				continue;
				IL_299:
				ptr4++;
				continue;
				IL_22A:
				*ptr7 = (byte)num;
				ptr7++;
				goto IL_299;
			}
			this.bufPos = (int)((long)(ptr7 - ptr3));
			return (int)((long)(ptr4 - ptr5));
			Block_22:
			if (ptr4[1] == '\n')
			{
				ptr4++;
			}
			this.bufPos = (int)((long)(ptr7 - ptr3));
			needWriteNewLine = true;
			return (int)((long)(ptr4 - ptr5));
			Block_24:
			this.bufPos = (int)((long)(ptr7 - ptr3));
			needWriteNewLine = true;
			return (int)((long)(ptr4 - ptr5));
			IL_2A4:
			this.bufPos = (int)((long)(ptr7 - ptr3));
			array = null;
			return -1;
		}

		// Token: 0x06000E47 RID: 3655 RVA: 0x00053ADC File Offset: 0x00051CDC
		protected async Task WriteCommentOrPiAsync(string text, int stopChar)
		{
			if (text.Length == 0)
			{
				if (this.bufPos >= this.bufLen)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			else
			{
				int writeLen = 0;
				int curIndex = 0;
				int leftCount = text.Length;
				bool needWriteNewLine = false;
				do
				{
					writeLen = this.WriteCommentOrPiNoFlush(text, curIndex, leftCount, stopChar, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
						curIndex++;
						leftCount--;
					}
					else if (writeLen >= 0)
					{
						await this.FlushBufferAsync().ConfigureAwait(false);
					}
				}
				while (writeLen >= 0 || needWriteNewLine);
			}
		}

		// Token: 0x06000E48 RID: 3656 RVA: 0x00053B34 File Offset: 0x00051D34
		[SecuritySafeCritical]
		protected unsafe int WriteCDataSectionNoFlush(string text, int index, int count, out bool needWriteNewLine)
		{
			needWriteNewLine = false;
			if (count == 0)
			{
				return -1;
			}
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			char* ptr2 = ptr + index;
			byte[] array;
			byte* ptr3;
			if ((array = this.bufBytes) == null || array.Length == 0)
			{
				ptr3 = null;
			}
			else
			{
				ptr3 = &array[0];
			}
			char* ptr4 = ptr2;
			char* ptr5 = ptr2 + count;
			char* ptr6 = ptr4;
			byte* ptr7 = ptr3 + this.bufPos;
			int num = 0;
			for (;;)
			{
				byte* ptr8 = ptr7 + (long)(ptr5 - ptr4);
				if (ptr8 != ptr3 + this.bufLen)
				{
					ptr8 = ptr3 + this.bufLen;
				}
				while (ptr7 < ptr8 && (this.xmlCharType.charProperties[num = (int)(*ptr4)] & 128) != 0 && num != 93 && num <= 127)
				{
					*ptr7 = (byte)num;
					ptr7++;
					ptr4++;
				}
				if (ptr4 >= ptr5)
				{
					goto IL_285;
				}
				if (ptr7 >= ptr8)
				{
					break;
				}
				if (num <= 39)
				{
					switch (num)
					{
					case 9:
						goto IL_20B;
					case 10:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_22;
						}
						*ptr7 = (byte)num;
						ptr7++;
						goto IL_27A;
					case 11:
					case 12:
						break;
					case 13:
						if (this.newLineHandling == NewLineHandling.Replace)
						{
							goto Block_20;
						}
						*ptr7 = (byte)num;
						ptr7++;
						goto IL_27A;
					default:
						if (num == 34 || num - 38 <= 1)
						{
							goto IL_20B;
						}
						break;
					}
				}
				else
				{
					if (num == 60)
					{
						goto IL_20B;
					}
					if (num == 62)
					{
						if (this.hadDoubleBracket && ptr7[-1] == 93)
						{
							ptr7 = XmlUtf8RawTextWriter.RawEndCData(ptr7);
							ptr7 = XmlUtf8RawTextWriter.RawStartCData(ptr7);
						}
						*ptr7 = 62;
						ptr7++;
						goto IL_27A;
					}
					if (num == 93)
					{
						if (ptr7[-1] == 93)
						{
							this.hadDoubleBracket = true;
						}
						else
						{
							this.hadDoubleBracket = false;
						}
						*ptr7 = 93;
						ptr7++;
						goto IL_27A;
					}
				}
				if (XmlCharType.IsSurrogate(num))
				{
					ptr7 = XmlUtf8RawTextWriter.EncodeSurrogate(ptr4, ptr5, ptr7);
					ptr4 += 2;
					continue;
				}
				if (num <= 127 || num >= 65534)
				{
					ptr7 = this.InvalidXmlChar(num, ptr7, false);
					ptr4++;
					continue;
				}
				ptr7 = XmlUtf8RawTextWriter.EncodeMultibyteUTF8(num, ptr7);
				ptr4++;
				continue;
				IL_27A:
				ptr4++;
				continue;
				IL_20B:
				*ptr7 = (byte)num;
				ptr7++;
				goto IL_27A;
			}
			this.bufPos = (int)((long)(ptr7 - ptr3));
			return (int)((long)(ptr4 - ptr6));
			Block_20:
			if (ptr4[1] == '\n')
			{
				ptr4++;
			}
			this.bufPos = (int)((long)(ptr7 - ptr3));
			needWriteNewLine = true;
			return (int)((long)(ptr4 - ptr6));
			Block_22:
			this.bufPos = (int)((long)(ptr7 - ptr3));
			needWriteNewLine = true;
			return (int)((long)(ptr4 - ptr6));
			IL_285:
			this.bufPos = (int)((long)(ptr7 - ptr3));
			array = null;
			return -1;
		}

		// Token: 0x06000E49 RID: 3657 RVA: 0x00053DD8 File Offset: 0x00051FD8
		protected async Task WriteCDataSectionAsync(string text)
		{
			if (text.Length == 0)
			{
				if (this.bufPos >= this.bufLen)
				{
					await this.FlushBufferAsync().ConfigureAwait(false);
				}
			}
			else
			{
				int writeLen = 0;
				int curIndex = 0;
				int leftCount = text.Length;
				bool needWriteNewLine = false;
				do
				{
					writeLen = this.WriteCDataSectionNoFlush(text, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						await this.RawTextAsync(this.newLineChars).ConfigureAwait(false);
						curIndex++;
						leftCount--;
					}
					else if (writeLen >= 0)
					{
						await this.FlushBufferAsync().ConfigureAwait(false);
					}
				}
				while (writeLen >= 0 || needWriteNewLine);
			}
		}

		// Token: 0x04000968 RID: 2408
		private readonly bool useAsync;

		// Token: 0x04000969 RID: 2409
		protected byte[] bufBytes;

		// Token: 0x0400096A RID: 2410
		protected Stream stream;

		// Token: 0x0400096B RID: 2411
		protected Encoding encoding;

		// Token: 0x0400096C RID: 2412
		protected XmlCharType xmlCharType = XmlCharType.Instance;

		// Token: 0x0400096D RID: 2413
		protected int bufPos = 1;

		// Token: 0x0400096E RID: 2414
		protected int textPos = 1;

		// Token: 0x0400096F RID: 2415
		protected int contentPos;

		// Token: 0x04000970 RID: 2416
		protected int cdataPos;

		// Token: 0x04000971 RID: 2417
		protected int attrEndPos;

		// Token: 0x04000972 RID: 2418
		protected int bufLen = 6144;

		// Token: 0x04000973 RID: 2419
		protected bool writeToNull;

		// Token: 0x04000974 RID: 2420
		protected bool hadDoubleBracket;

		// Token: 0x04000975 RID: 2421
		protected bool inAttributeValue;

		// Token: 0x04000976 RID: 2422
		protected NewLineHandling newLineHandling;

		// Token: 0x04000977 RID: 2423
		protected bool closeOutput;

		// Token: 0x04000978 RID: 2424
		protected bool omitXmlDeclaration;

		// Token: 0x04000979 RID: 2425
		protected string newLineChars;

		// Token: 0x0400097A RID: 2426
		protected bool checkCharacters;

		// Token: 0x0400097B RID: 2427
		protected XmlStandalone standalone;

		// Token: 0x0400097C RID: 2428
		protected XmlOutputMethod outputMethod;

		// Token: 0x0400097D RID: 2429
		protected bool autoXmlDeclaration;

		// Token: 0x0400097E RID: 2430
		protected bool mergeCDataSections;

		// Token: 0x0400097F RID: 2431
		private const int BUFSIZE = 6144;

		// Token: 0x04000980 RID: 2432
		private const int ASYNCBUFSIZE = 65536;

		// Token: 0x04000981 RID: 2433
		private const int OVERFLOW = 32;

		// Token: 0x04000982 RID: 2434
		private const int INIT_MARKS_COUNT = 64;

		// Token: 0x02000186 RID: 390
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteXmlDeclarationAsync>d__86 : IAsyncStateMachine
		{
			// Token: 0x06000E4A RID: 3658 RVA: 0x00053E28 File Offset: 0x00052028
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_117;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_18B;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1FA;
					}
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_26E;
					}
					case 5:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2E7;
					}
					case 6:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_34D;
					}
					default:
						xmlUtf8RawTextWriter.CheckAsyncCall();
						if (xmlUtf8RawTextWriter.omitXmlDeclaration || xmlUtf8RawTextWriter.autoXmlDeclaration)
						{
							goto IL_354;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync("<?xml version=\"").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteXmlDeclarationAsync>d__86>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync("1.0").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteXmlDeclarationAsync>d__86>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_117:
					configuredTaskAwaiter.GetResult();
					if (xmlUtf8RawTextWriter.encoding == null)
					{
						goto IL_201;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync("\" encoding=\"").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteXmlDeclarationAsync>d__86>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_18B:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(xmlUtf8RawTextWriter.encoding.WebName).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteXmlDeclarationAsync>d__86>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_1FA:
					configuredTaskAwaiter.GetResult();
					IL_201:
					if (standalone == XmlStandalone.Omit)
					{
						goto IL_2EE;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync("\" standalone=\"").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteXmlDeclarationAsync>d__86>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_26E:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync((standalone == XmlStandalone.Yes) ? "yes" : "no").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteXmlDeclarationAsync>d__86>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_2E7:
					configuredTaskAwaiter.GetResult();
					IL_2EE:
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync("\"?>").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 6;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteXmlDeclarationAsync>d__86>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_34D:
					configuredTaskAwaiter.GetResult();
					IL_354:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E4B RID: 3659 RVA: 0x000541D4 File Offset: 0x000523D4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000983 RID: 2435
			public int <>1__state;

			// Token: 0x04000984 RID: 2436
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000985 RID: 2437
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x04000986 RID: 2438
			public XmlStandalone standalone;

			// Token: 0x04000987 RID: 2439
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000187 RID: 391
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteDocTypeAsync>d__88 : IAsyncStateMachine
		{
			// Token: 0x06000E4C RID: 3660 RVA: 0x000541E4 File Offset: 0x000523E4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_10A;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_17E;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1E8;
					}
					case 4:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_251;
					}
					case 5:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2C3;
					}
					case 6:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_359;
					}
					case 7:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3C3;
					}
					case 8:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_491;
					}
					default:
						xmlUtf8RawTextWriter.CheckAsyncCall();
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync("<!DOCTYPE ").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(name).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_10A:
					configuredTaskAwaiter.GetResult();
					int bufPos;
					if (pubid != null)
					{
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(" PUBLIC \"").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (sysid == null)
						{
							byte[] bufBytes = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter2 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter2.bufPos = bufPos + 1;
							bufBytes[bufPos] = 32;
							goto IL_406;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(" SYSTEM \"").ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 6;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_359;
					}
					IL_17E:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(pubid).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 3;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_1E8:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync("\" \"").ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 4;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_251:
					configuredTaskAwaiter.GetResult();
					if (sysid == null)
					{
						goto IL_2CA;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(sysid).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 5;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_2C3:
					configuredTaskAwaiter.GetResult();
					IL_2CA:
					byte[] bufBytes2 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter3 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter3.bufPos = bufPos + 1;
					bufBytes2[bufPos] = 34;
					goto IL_406;
					IL_359:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(sysid).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 7;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_3C3:
					configuredTaskAwaiter.GetResult();
					byte[] bufBytes3 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter4 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter4.bufPos = bufPos + 1;
					bufBytes3[bufPos] = 34;
					IL_406:
					if (subset == null)
					{
						goto IL_4B5;
					}
					byte[] bufBytes4 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter5 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter5.bufPos = bufPos + 1;
					bufBytes4[bufPos] = 91;
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(subset).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 8;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteDocTypeAsync>d__88>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_491:
					configuredTaskAwaiter.GetResult();
					byte[] bufBytes5 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter6 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter6.bufPos = bufPos + 1;
					bufBytes5[bufPos] = 93;
					IL_4B5:
					byte[] bufBytes6 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter7 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter7.bufPos = bufPos + 1;
					bufBytes6[bufPos] = 62;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E4D RID: 3661 RVA: 0x00054710 File Offset: 0x00052910
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000988 RID: 2440
			public int <>1__state;

			// Token: 0x04000989 RID: 2441
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400098A RID: 2442
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x0400098B RID: 2443
			public string name;

			// Token: 0x0400098C RID: 2444
			public string pubid;

			// Token: 0x0400098D RID: 2445
			public string sysid;

			// Token: 0x0400098E RID: 2446
			public string subset;

			// Token: 0x0400098F RID: 2447
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000188 RID: 392
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteNamespaceDeclarationAsync>d__96 : IAsyncStateMachine
		{
			// Token: 0x06000E4E RID: 3662 RVA: 0x00054720 File Offset: 0x00052920
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_F3;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_154;
					}
					default:
						xmlUtf8RawTextWriter.CheckAsyncCall();
						configuredTaskAwaiter = xmlUtf8RawTextWriter.WriteStartNamespaceDeclarationAsync(prefix).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteNamespaceDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.WriteStringAsync(namespaceName).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteNamespaceDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_F3:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.WriteEndNamespaceDeclarationAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteNamespaceDeclarationAsync>d__96>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_154:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E4F RID: 3663 RVA: 0x000548D4 File Offset: 0x00052AD4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000990 RID: 2448
			public int <>1__state;

			// Token: 0x04000991 RID: 2449
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000992 RID: 2450
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x04000993 RID: 2451
			public string prefix;

			// Token: 0x04000994 RID: 2452
			public string namespaceName;

			// Token: 0x04000995 RID: 2453
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000189 RID: 393
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartNamespaceDeclarationAsync>d__97 : IAsyncStateMachine
		{
			// Token: 0x06000E50 RID: 3664 RVA: 0x000548E4 File Offset: 0x00052AE4
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_103;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_16D;
					}
					default:
						xmlUtf8RawTextWriter.CheckAsyncCall();
						if (prefix.Length == 0)
						{
							configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(" xmlns=\"").ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteStartNamespaceDeclarationAsync>d__97>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(" xmlns:").ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteStartNamespaceDeclarationAsync>d__97>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_103;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					goto IL_1AE;
					IL_103:
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(prefix).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteStartNamespaceDeclarationAsync>d__97>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_16D:
					configuredTaskAwaiter.GetResult();
					byte[] bufBytes = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter2 = xmlUtf8RawTextWriter;
					int bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter2.bufPos = bufPos + 1;
					bufBytes[bufPos] = 61;
					byte[] bufBytes2 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter3 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter3.bufPos = bufPos + 1;
					bufBytes2[bufPos] = 34;
					IL_1AE:
					xmlUtf8RawTextWriter.inAttributeValue = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E51 RID: 3665 RVA: 0x00054AF0 File Offset: 0x00052CF0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000996 RID: 2454
			public int <>1__state;

			// Token: 0x04000997 RID: 2455
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000998 RID: 2456
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x04000999 RID: 2457
			public string prefix;

			// Token: 0x0400099A RID: 2458
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200018A RID: 394
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCDataAsync>d__99 : IAsyncStateMachine
		{
			// Token: 0x06000E52 RID: 3666 RVA: 0x00054B00 File Offset: 0x00052D00
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					int bufPos;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlUtf8RawTextWriter.CheckAsyncCall();
						if (xmlUtf8RawTextWriter.mergeCDataSections && xmlUtf8RawTextWriter.bufPos == xmlUtf8RawTextWriter.cdataPos)
						{
							xmlUtf8RawTextWriter.bufPos -= 3;
						}
						else
						{
							byte[] bufBytes = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter2 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter2.bufPos = bufPos + 1;
							bufBytes[bufPos] = 60;
							byte[] bufBytes2 = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter3 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter3.bufPos = bufPos + 1;
							bufBytes2[bufPos] = 33;
							byte[] bufBytes3 = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter4 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter4.bufPos = bufPos + 1;
							bufBytes3[bufPos] = 91;
							byte[] bufBytes4 = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter5 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter5.bufPos = bufPos + 1;
							bufBytes4[bufPos] = 67;
							byte[] bufBytes5 = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter6 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter6.bufPos = bufPos + 1;
							bufBytes5[bufPos] = 68;
							byte[] bufBytes6 = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter7 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter7.bufPos = bufPos + 1;
							bufBytes6[bufPos] = 65;
							byte[] bufBytes7 = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter8 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter8.bufPos = bufPos + 1;
							bufBytes7[bufPos] = 84;
							byte[] bufBytes8 = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter9 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter9.bufPos = bufPos + 1;
							bufBytes8[bufPos] = 65;
							byte[] bufBytes9 = xmlUtf8RawTextWriter.bufBytes;
							XmlUtf8RawTextWriter xmlUtf8RawTextWriter10 = xmlUtf8RawTextWriter;
							bufPos = xmlUtf8RawTextWriter.bufPos;
							xmlUtf8RawTextWriter10.bufPos = bufPos + 1;
							bufBytes9[bufPos] = 91;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.WriteCDataSectionAsync(text).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCDataAsync>d__99>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					byte[] bufBytes10 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter11 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter11.bufPos = bufPos + 1;
					bufBytes10[bufPos] = 93;
					byte[] bufBytes11 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter12 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter12.bufPos = bufPos + 1;
					bufBytes11[bufPos] = 93;
					byte[] bufBytes12 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter13 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter13.bufPos = bufPos + 1;
					bufBytes12[bufPos] = 62;
					xmlUtf8RawTextWriter.textPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter.cdataPos = xmlUtf8RawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E53 RID: 3667 RVA: 0x00054D58 File Offset: 0x00052F58
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400099B RID: 2459
			public int <>1__state;

			// Token: 0x0400099C RID: 2460
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400099D RID: 2461
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x0400099E RID: 2462
			public string text;

			// Token: 0x0400099F RID: 2463
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200018B RID: 395
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCommentAsync>d__100 : IAsyncStateMachine
		{
			// Token: 0x06000E54 RID: 3668 RVA: 0x00054D68 File Offset: 0x00052F68
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					int bufPos;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlUtf8RawTextWriter.CheckAsyncCall();
						byte[] bufBytes = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter2 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter2.bufPos = bufPos + 1;
						bufBytes[bufPos] = 60;
						byte[] bufBytes2 = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter3 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter3.bufPos = bufPos + 1;
						bufBytes2[bufPos] = 33;
						byte[] bufBytes3 = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter4 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter4.bufPos = bufPos + 1;
						bufBytes3[bufPos] = 45;
						byte[] bufBytes4 = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter5 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter5.bufPos = bufPos + 1;
						bufBytes4[bufPos] = 45;
						configuredTaskAwaiter = xmlUtf8RawTextWriter.WriteCommentOrPiAsync(text, 45).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCommentAsync>d__100>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					byte[] bufBytes5 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter6 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter6.bufPos = bufPos + 1;
					bufBytes5[bufPos] = 45;
					byte[] bufBytes6 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter7 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter7.bufPos = bufPos + 1;
					bufBytes6[bufPos] = 45;
					byte[] bufBytes7 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter8 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter8.bufPos = bufPos + 1;
					bufBytes7[bufPos] = 62;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E55 RID: 3669 RVA: 0x00054EFC File Offset: 0x000530FC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009A0 RID: 2464
			public int <>1__state;

			// Token: 0x040009A1 RID: 2465
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009A2 RID: 2466
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009A3 RID: 2467
			public string text;

			// Token: 0x040009A4 RID: 2468
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200018C RID: 396
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteProcessingInstructionAsync>d__101 : IAsyncStateMachine
		{
			// Token: 0x06000E56 RID: 3670 RVA: 0x00054F0C File Offset: 0x0005310C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					int bufPos;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_151;
						}
						xmlUtf8RawTextWriter.CheckAsyncCall();
						byte[] bufBytes = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter2 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter2.bufPos = bufPos + 1;
						bufBytes[bufPos] = 60;
						byte[] bufBytes2 = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter3 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter3.bufPos = bufPos + 1;
						bufBytes2[bufPos] = 63;
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(name).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteProcessingInstructionAsync>d__101>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					if (text.Length <= 0)
					{
						goto IL_158;
					}
					byte[] bufBytes3 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter4 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter4.bufPos = bufPos + 1;
					bufBytes3[bufPos] = 32;
					configuredTaskAwaiter = xmlUtf8RawTextWriter.WriteCommentOrPiAsync(text, 63).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteProcessingInstructionAsync>d__101>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_151:
					configuredTaskAwaiter.GetResult();
					IL_158:
					byte[] bufBytes4 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter5 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter5.bufPos = bufPos + 1;
					bufBytes4[bufPos] = 63;
					byte[] bufBytes5 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter6 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter6.bufPos = bufPos + 1;
					bufBytes5[bufPos] = 62;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E57 RID: 3671 RVA: 0x000550F0 File Offset: 0x000532F0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009A5 RID: 2469
			public int <>1__state;

			// Token: 0x040009A6 RID: 2470
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009A7 RID: 2471
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009A8 RID: 2472
			public string name;

			// Token: 0x040009A9 RID: 2473
			public string text;

			// Token: 0x040009AA RID: 2474
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200018D RID: 397
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteEntityRefAsync>d__102 : IAsyncStateMachine
		{
			// Token: 0x06000E58 RID: 3672 RVA: 0x00055100 File Offset: 0x00053300
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					int bufPos;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_126;
						}
						xmlUtf8RawTextWriter.CheckAsyncCall();
						byte[] bufBytes = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter2 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter2.bufPos = bufPos + 1;
						bufBytes[bufPos] = 38;
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(name).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteEntityRefAsync>d__102>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					byte[] bufBytes2 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter3 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter3.bufPos = bufPos + 1;
					bufBytes2[bufPos] = 59;
					if (xmlUtf8RawTextWriter.bufPos <= xmlUtf8RawTextWriter.bufLen)
					{
						goto IL_12D;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteEntityRefAsync>d__102>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_126:
					configuredTaskAwaiter.GetResult();
					IL_12D:
					xmlUtf8RawTextWriter.textPos = xmlUtf8RawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E59 RID: 3673 RVA: 0x00055290 File Offset: 0x00053490
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009AB RID: 2475
			public int <>1__state;

			// Token: 0x040009AC RID: 2476
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009AD RID: 2477
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009AE RID: 2478
			public string name;

			// Token: 0x040009AF RID: 2479
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200018E RID: 398
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCharEntityAsync>d__103 : IAsyncStateMachine
		{
			// Token: 0x06000E5A RID: 3674 RVA: 0x000552A0 File Offset: 0x000534A0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					int num3;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_19F;
						}
						xmlUtf8RawTextWriter.CheckAsyncCall();
						num3 = (int)ch;
						string text = num3.ToString("X", NumberFormatInfo.InvariantInfo);
						if (xmlUtf8RawTextWriter.checkCharacters && !xmlUtf8RawTextWriter.xmlCharType.IsCharData(ch))
						{
							throw XmlConvert.CreateInvalidCharException(ch, '\0');
						}
						byte[] bufBytes = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter2 = xmlUtf8RawTextWriter;
						num3 = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter2.bufPos = num3 + 1;
						bufBytes[num3] = 38;
						byte[] bufBytes2 = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter3 = xmlUtf8RawTextWriter;
						num3 = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter3.bufPos = num3 + 1;
						bufBytes2[num3] = 35;
						byte[] bufBytes3 = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter4 = xmlUtf8RawTextWriter;
						num3 = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter4.bufPos = num3 + 1;
						bufBytes3[num3] = 120;
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(text).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCharEntityAsync>d__103>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					byte[] bufBytes4 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter5 = xmlUtf8RawTextWriter;
					num3 = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter5.bufPos = num3 + 1;
					bufBytes4[num3] = 59;
					if (xmlUtf8RawTextWriter.bufPos <= xmlUtf8RawTextWriter.bufLen)
					{
						goto IL_1A6;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCharEntityAsync>d__103>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_19F:
					configuredTaskAwaiter.GetResult();
					IL_1A6:
					xmlUtf8RawTextWriter.textPos = xmlUtf8RawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E5B RID: 3675 RVA: 0x000554AC File Offset: 0x000536AC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009B0 RID: 2480
			public int <>1__state;

			// Token: 0x040009B1 RID: 2481
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009B2 RID: 2482
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009B3 RID: 2483
			public char ch;

			// Token: 0x040009B4 RID: 2484
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200018F RID: 399
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteSurrogateCharEntityAsync>d__106 : IAsyncStateMachine
		{
			// Token: 0x06000E5C RID: 3676 RVA: 0x000554BC File Offset: 0x000536BC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					int bufPos;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlUtf8RawTextWriter.CheckAsyncCall();
						int num3 = XmlCharType.CombineSurrogateChar((int)lowChar, (int)highChar);
						byte[] bufBytes = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter2 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter2.bufPos = bufPos + 1;
						bufBytes[bufPos] = 38;
						byte[] bufBytes2 = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter3 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter3.bufPos = bufPos + 1;
						bufBytes2[bufPos] = 35;
						byte[] bufBytes3 = xmlUtf8RawTextWriter.bufBytes;
						XmlUtf8RawTextWriter xmlUtf8RawTextWriter4 = xmlUtf8RawTextWriter;
						bufPos = xmlUtf8RawTextWriter.bufPos;
						xmlUtf8RawTextWriter4.bufPos = bufPos + 1;
						bufBytes3[bufPos] = 120;
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(num3.ToString("X", NumberFormatInfo.InvariantInfo)).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteSurrogateCharEntityAsync>d__106>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					byte[] bufBytes4 = xmlUtf8RawTextWriter.bufBytes;
					XmlUtf8RawTextWriter xmlUtf8RawTextWriter5 = xmlUtf8RawTextWriter;
					bufPos = xmlUtf8RawTextWriter.bufPos;
					xmlUtf8RawTextWriter5.bufPos = bufPos + 1;
					bufBytes4[bufPos] = 59;
					xmlUtf8RawTextWriter.textPos = xmlUtf8RawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E5D RID: 3677 RVA: 0x0005562C File Offset: 0x0005382C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009B5 RID: 2485
			public int <>1__state;

			// Token: 0x040009B6 RID: 2486
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009B7 RID: 2487
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009B8 RID: 2488
			public char lowChar;

			// Token: 0x040009B9 RID: 2489
			public char highChar;

			// Token: 0x040009BA RID: 2490
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000190 RID: 400
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawAsync>d__108 : IAsyncStateMachine
		{
			// Token: 0x06000E5E RID: 3678 RVA: 0x0005563C File Offset: 0x0005383C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlUtf8RawTextWriter.CheckAsyncCall();
						configuredTaskAwaiter = xmlUtf8RawTextWriter.WriteRawWithCharCheckingAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteRawAsync>d__108>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xmlUtf8RawTextWriter.textPos = xmlUtf8RawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E5F RID: 3679 RVA: 0x00055720 File Offset: 0x00053920
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009BB RID: 2491
			public int <>1__state;

			// Token: 0x040009BC RID: 2492
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009BD RID: 2493
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009BE RID: 2494
			public char[] buffer;

			// Token: 0x040009BF RID: 2495
			public int index;

			// Token: 0x040009C0 RID: 2496
			public int count;

			// Token: 0x040009C1 RID: 2497
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000191 RID: 401
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawAsync>d__109 : IAsyncStateMachine
		{
			// Token: 0x06000E60 RID: 3680 RVA: 0x00055730 File Offset: 0x00053930
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlUtf8RawTextWriter.CheckAsyncCall();
						configuredTaskAwaiter = xmlUtf8RawTextWriter.WriteRawWithCharCheckingAsync(data).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteRawAsync>d__109>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					xmlUtf8RawTextWriter.textPos = xmlUtf8RawTextWriter.bufPos;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E61 RID: 3681 RVA: 0x00055808 File Offset: 0x00053A08
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009C2 RID: 2498
			public int <>1__state;

			// Token: 0x040009C3 RID: 2499
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009C4 RID: 2500
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009C5 RID: 2501
			public string data;

			// Token: 0x040009C6 RID: 2502
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000192 RID: 402
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FlushAsync>d__110 : IAsyncStateMachine
		{
			// Token: 0x06000E62 RID: 3682 RVA: 0x00055818 File Offset: 0x00053A18
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_E7;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_155;
					}
					default:
						xmlUtf8RawTextWriter.CheckAsyncCall();
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<FlushAsync>d__110>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushEncoderAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<FlushAsync>d__110>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_E7:
					configuredTaskAwaiter.GetResult();
					if (xmlUtf8RawTextWriter.stream == null)
					{
						goto IL_15C;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.stream.FlushAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 2;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<FlushAsync>d__110>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_155:
					configuredTaskAwaiter.GetResult();
					IL_15C:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E63 RID: 3683 RVA: 0x000559CC File Offset: 0x00053BCC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009C7 RID: 2503
			public int <>1__state;

			// Token: 0x040009C8 RID: 2504
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009C9 RID: 2505
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009CA RID: 2506
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000193 RID: 403
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FlushBufferAsync>d__111 : IAsyncStateMachine
		{
			// Token: 0x06000E64 RID: 3684 RVA: 0x000559DC File Offset: 0x00053BDC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					try
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
						if (num != 0)
						{
							if (xmlUtf8RawTextWriter.writeToNull)
							{
								goto IL_94;
							}
							configuredTaskAwaiter = xmlUtf8RawTextWriter.stream.WriteAsync(xmlUtf8RawTextWriter.bufBytes, 1, xmlUtf8RawTextWriter.bufPos - 1).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 0);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<FlushBufferAsync>d__111>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
						}
						configuredTaskAwaiter.GetResult();
						IL_94:;
					}
					catch
					{
						xmlUtf8RawTextWriter.writeToNull = true;
						throw;
					}
					finally
					{
						if (num < 0)
						{
							xmlUtf8RawTextWriter.bufBytes[0] = xmlUtf8RawTextWriter.bufBytes[xmlUtf8RawTextWriter.bufPos - 1];
							if (XmlUtf8RawTextWriter.IsSurrogateByte(xmlUtf8RawTextWriter.bufBytes[0]))
							{
								xmlUtf8RawTextWriter.bufBytes[1] = xmlUtf8RawTextWriter.bufBytes[xmlUtf8RawTextWriter.bufPos];
								xmlUtf8RawTextWriter.bufBytes[2] = xmlUtf8RawTextWriter.bufBytes[xmlUtf8RawTextWriter.bufPos + 1];
								xmlUtf8RawTextWriter.bufBytes[3] = xmlUtf8RawTextWriter.bufBytes[xmlUtf8RawTextWriter.bufPos + 2];
							}
							xmlUtf8RawTextWriter.textPos = ((xmlUtf8RawTextWriter.textPos == xmlUtf8RawTextWriter.bufPos) ? 1 : 0);
							xmlUtf8RawTextWriter.attrEndPos = ((xmlUtf8RawTextWriter.attrEndPos == xmlUtf8RawTextWriter.bufPos) ? 1 : 0);
							xmlUtf8RawTextWriter.contentPos = 0;
							xmlUtf8RawTextWriter.cdataPos = 0;
							xmlUtf8RawTextWriter.bufPos = 1;
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E65 RID: 3685 RVA: 0x00055BC0 File Offset: 0x00053DC0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009CB RID: 2507
			public int <>1__state;

			// Token: 0x040009CC RID: 2508
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009CD RID: 2509
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009CE RID: 2510
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000194 RID: 404
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteAttributeTextBlockAsync>d__116 : IAsyncStateMachine
		{
			// Token: 0x06000E66 RID: 3686 RVA: 0x00055BD0 File Offset: 0x00053DD0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_DA;
					}
					writeLen = 0;
					curIndex = index;
					leftCount = count;
					IL_33:
					writeLen = xmlUtf8RawTextWriter.WriteAttributeTextBlockNoFlush(chars, curIndex, leftCount);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (writeLen < 0)
					{
						goto IL_E1;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteAttributeTextBlockAsync>d__116>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_DA:
					configuredTaskAwaiter.GetResult();
					IL_E1:
					if (writeLen >= 0)
					{
						goto IL_33;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E67 RID: 3687 RVA: 0x00055D08 File Offset: 0x00053F08
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009CF RID: 2511
			public int <>1__state;

			// Token: 0x040009D0 RID: 2512
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009D1 RID: 2513
			public int index;

			// Token: 0x040009D2 RID: 2514
			public int count;

			// Token: 0x040009D3 RID: 2515
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009D4 RID: 2516
			public char[] chars;

			// Token: 0x040009D5 RID: 2517
			private int <curIndex>5__1;

			// Token: 0x040009D6 RID: 2518
			private int <leftCount>5__2;

			// Token: 0x040009D7 RID: 2519
			private int <writeLen>5__3;

			// Token: 0x040009D8 RID: 2520
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000195 RID: 405
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_WriteAttributeTextBlockAsync>d__118 : IAsyncStateMachine
		{
			// Token: 0x06000E68 RID: 3688 RVA: 0x00055D18 File Offset: 0x00053F18
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_123;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<_WriteAttributeTextBlockAsync>d__118>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_7C:
					writeLen = xmlUtf8RawTextWriter.WriteAttributeTextBlockNoFlush(text, curIndex, leftCount);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (writeLen < 0)
					{
						goto IL_12A;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<_WriteAttributeTextBlockAsync>d__118>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_123:
					configuredTaskAwaiter.GetResult();
					IL_12A:
					if (writeLen >= 0)
					{
						goto IL_7C;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E69 RID: 3689 RVA: 0x00055EA8 File Offset: 0x000540A8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009D9 RID: 2521
			public int <>1__state;

			// Token: 0x040009DA RID: 2522
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009DB RID: 2523
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009DC RID: 2524
			public string text;

			// Token: 0x040009DD RID: 2525
			public int curIndex;

			// Token: 0x040009DE RID: 2526
			public int leftCount;

			// Token: 0x040009DF RID: 2527
			private int <writeLen>5__1;

			// Token: 0x040009E0 RID: 2528
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000196 RID: 406
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteElementTextBlockAsync>d__122 : IAsyncStateMachine
		{
			// Token: 0x06000E6A RID: 3690 RVA: 0x00055EB8 File Offset: 0x000540B8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_F9;
					}
					if (num == 1)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_189;
					}
					writeLen = 0;
					curIndex = index;
					leftCount = count;
					needWriteNewLine = false;
					IL_41:
					writeLen = xmlUtf8RawTextWriter.WriteElementTextBlockNoFlush(chars, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(xmlUtf8RawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteElementTextBlockAsync>d__122>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_190;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteElementTextBlockAsync>d__122>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_189;
					}
					IL_F9:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_190;
					IL_189:
					configuredTaskAwaiter.GetResult();
					IL_190:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_41;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E6B RID: 3691 RVA: 0x000560B8 File Offset: 0x000542B8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009E1 RID: 2529
			public int <>1__state;

			// Token: 0x040009E2 RID: 2530
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009E3 RID: 2531
			public int index;

			// Token: 0x040009E4 RID: 2532
			public int count;

			// Token: 0x040009E5 RID: 2533
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009E6 RID: 2534
			public char[] chars;

			// Token: 0x040009E7 RID: 2535
			private int <curIndex>5__1;

			// Token: 0x040009E8 RID: 2536
			private int <leftCount>5__2;

			// Token: 0x040009E9 RID: 2537
			private int <writeLen>5__3;

			// Token: 0x040009EA RID: 2538
			private bool <needWriteNewLine>5__4;

			// Token: 0x040009EB RID: 2539
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000197 RID: 407
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_WriteElementTextBlockAsync>d__124 : IAsyncStateMachine
		{
			// Token: 0x06000E6C RID: 3692 RVA: 0x000560C8 File Offset: 0x000542C8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_12A;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1E9;
					}
					case 3:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_279;
					}
					default:
						writeLen = 0;
						needWriteNewLine = false;
						if (newLine)
						{
							configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(xmlUtf8RawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<_WriteElementTextBlockAsync>d__124>(ref configuredTaskAwaiter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<_WriteElementTextBlockAsync>d__124>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_12A;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_131;
					IL_12A:
					configuredTaskAwaiter.GetResult();
					IL_131:
					writeLen = xmlUtf8RawTextWriter.WriteElementTextBlockNoFlush(text, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(xmlUtf8RawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<_WriteElementTextBlockAsync>d__124>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_280;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 3;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<_WriteElementTextBlockAsync>d__124>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_279;
					}
					IL_1E9:
					configuredTaskAwaiter.GetResult();
					num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_280;
					IL_279:
					configuredTaskAwaiter.GetResult();
					IL_280:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_131;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E6D RID: 3693 RVA: 0x000563B8 File Offset: 0x000545B8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009EC RID: 2540
			public int <>1__state;

			// Token: 0x040009ED RID: 2541
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009EE RID: 2542
			public bool newLine;

			// Token: 0x040009EF RID: 2543
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009F0 RID: 2544
			public int curIndex;

			// Token: 0x040009F1 RID: 2545
			public int leftCount;

			// Token: 0x040009F2 RID: 2546
			public string text;

			// Token: 0x040009F3 RID: 2547
			private int <writeLen>5__1;

			// Token: 0x040009F4 RID: 2548
			private bool <needWriteNewLine>5__2;

			// Token: 0x040009F5 RID: 2549
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000198 RID: 408
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <_RawTextAsync>d__128 : IAsyncStateMachine
		{
			// Token: 0x06000E6E RID: 3694 RVA: 0x000563C8 File Offset: 0x000545C8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_12A;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<_RawTextAsync>d__128>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					writeLen = 0;
					IL_83:
					writeLen = xmlUtf8RawTextWriter.RawTextNoFlush(text, curIndex, leftCount);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (writeLen < 0)
					{
						goto IL_131;
					}
					configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<_RawTextAsync>d__128>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_12A:
					configuredTaskAwaiter.GetResult();
					IL_131:
					if (writeLen >= 0)
					{
						goto IL_83;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E6F RID: 3695 RVA: 0x0005655C File Offset: 0x0005475C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009F6 RID: 2550
			public int <>1__state;

			// Token: 0x040009F7 RID: 2551
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040009F8 RID: 2552
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x040009F9 RID: 2553
			public string text;

			// Token: 0x040009FA RID: 2554
			public int curIndex;

			// Token: 0x040009FB RID: 2555
			public int leftCount;

			// Token: 0x040009FC RID: 2556
			private int <writeLen>5__1;

			// Token: 0x040009FD RID: 2557
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000199 RID: 409
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawWithCharCheckingAsync>d__132 : IAsyncStateMachine
		{
			// Token: 0x06000E70 RID: 3696 RVA: 0x0005656C File Offset: 0x0005476C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_F9;
					}
					if (num == 1)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_189;
					}
					writeLen = 0;
					curIndex = index;
					leftCount = count;
					needWriteNewLine = false;
					IL_41:
					writeLen = xmlUtf8RawTextWriter.WriteRawWithCharCheckingNoFlush(chars, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(xmlUtf8RawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteRawWithCharCheckingAsync>d__132>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_190;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteRawWithCharCheckingAsync>d__132>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_189;
					}
					IL_F9:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_190;
					IL_189:
					configuredTaskAwaiter.GetResult();
					IL_190:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_41;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E71 RID: 3697 RVA: 0x0005676C File Offset: 0x0005496C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040009FE RID: 2558
			public int <>1__state;

			// Token: 0x040009FF RID: 2559
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A00 RID: 2560
			public int index;

			// Token: 0x04000A01 RID: 2561
			public int count;

			// Token: 0x04000A02 RID: 2562
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x04000A03 RID: 2563
			public char[] chars;

			// Token: 0x04000A04 RID: 2564
			private int <curIndex>5__1;

			// Token: 0x04000A05 RID: 2565
			private int <leftCount>5__2;

			// Token: 0x04000A06 RID: 2566
			private int <writeLen>5__3;

			// Token: 0x04000A07 RID: 2567
			private bool <needWriteNewLine>5__4;

			// Token: 0x04000A08 RID: 2568
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200019A RID: 410
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteRawWithCharCheckingAsync>d__133 : IAsyncStateMachine
		{
			// Token: 0x06000E72 RID: 3698 RVA: 0x0005677C File Offset: 0x0005497C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_F9;
					}
					if (num == 1)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_189;
					}
					writeLen = 0;
					curIndex = 0;
					leftCount = text.Length;
					needWriteNewLine = false;
					IL_41:
					writeLen = xmlUtf8RawTextWriter.WriteRawWithCharCheckingNoFlush(text, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(xmlUtf8RawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteRawWithCharCheckingAsync>d__133>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_190;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteRawWithCharCheckingAsync>d__133>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_189;
					}
					IL_F9:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_190;
					IL_189:
					configuredTaskAwaiter.GetResult();
					IL_190:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_41;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E73 RID: 3699 RVA: 0x0005697C File Offset: 0x00054B7C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A09 RID: 2569
			public int <>1__state;

			// Token: 0x04000A0A RID: 2570
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A0B RID: 2571
			public string text;

			// Token: 0x04000A0C RID: 2572
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x04000A0D RID: 2573
			private int <curIndex>5__1;

			// Token: 0x04000A0E RID: 2574
			private int <leftCount>5__2;

			// Token: 0x04000A0F RID: 2575
			private int <writeLen>5__3;

			// Token: 0x04000A10 RID: 2576
			private bool <needWriteNewLine>5__4;

			// Token: 0x04000A11 RID: 2577
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200019B RID: 411
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCommentOrPiAsync>d__135 : IAsyncStateMachine
		{
			// Token: 0x06000E74 RID: 3700 RVA: 0x0005698C File Offset: 0x00054B8C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_188;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_218;
					}
					default:
						if (text.Length != 0)
						{
							writeLen = 0;
							curIndex = 0;
							leftCount = text.Length;
							needWriteNewLine = false;
							goto IL_CA;
						}
						if (xmlUtf8RawTextWriter.bufPos < xmlUtf8RawTextWriter.bufLen)
						{
							goto IL_9F;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCommentOrPiAsync>d__135>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					IL_9F:
					goto IL_252;
					IL_CA:
					writeLen = xmlUtf8RawTextWriter.WriteCommentOrPiNoFlush(text, curIndex, leftCount, stopChar, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(xmlUtf8RawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCommentOrPiAsync>d__135>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_21F;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCommentOrPiAsync>d__135>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_218;
					}
					IL_188:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_21F;
					IL_218:
					configuredTaskAwaiter.GetResult();
					IL_21F:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_CA;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_252:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E75 RID: 3701 RVA: 0x00056C1C File Offset: 0x00054E1C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A12 RID: 2578
			public int <>1__state;

			// Token: 0x04000A13 RID: 2579
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A14 RID: 2580
			public string text;

			// Token: 0x04000A15 RID: 2581
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x04000A16 RID: 2582
			private int <curIndex>5__1;

			// Token: 0x04000A17 RID: 2583
			private int <leftCount>5__2;

			// Token: 0x04000A18 RID: 2584
			public int stopChar;

			// Token: 0x04000A19 RID: 2585
			private int <writeLen>5__3;

			// Token: 0x04000A1A RID: 2586
			private bool <needWriteNewLine>5__4;

			// Token: 0x04000A1B RID: 2587
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200019C RID: 412
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCDataSectionAsync>d__137 : IAsyncStateMachine
		{
			// Token: 0x06000E76 RID: 3702 RVA: 0x00056C2C File Offset: 0x00054E2C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlUtf8RawTextWriter xmlUtf8RawTextWriter = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_182;
					}
					case 2:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_212;
					}
					default:
						if (text.Length != 0)
						{
							writeLen = 0;
							curIndex = 0;
							leftCount = text.Length;
							needWriteNewLine = false;
							goto IL_CA;
						}
						if (xmlUtf8RawTextWriter.bufPos < xmlUtf8RawTextWriter.bufLen)
						{
							goto IL_9F;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCDataSectionAsync>d__137>(ref configuredTaskAwaiter, ref this);
							return;
						}
						break;
					}
					configuredTaskAwaiter.GetResult();
					IL_9F:
					goto IL_24C;
					IL_CA:
					writeLen = xmlUtf8RawTextWriter.WriteCDataSectionNoFlush(text, curIndex, leftCount, out needWriteNewLine);
					curIndex += writeLen;
					leftCount -= writeLen;
					if (needWriteNewLine)
					{
						configuredTaskAwaiter = xmlUtf8RawTextWriter.RawTextAsync(xmlUtf8RawTextWriter.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCDataSectionAsync>d__137>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						if (writeLen < 0)
						{
							goto IL_219;
						}
						configuredTaskAwaiter = xmlUtf8RawTextWriter.FlushBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlUtf8RawTextWriter.<WriteCDataSectionAsync>d__137>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_212;
					}
					IL_182:
					configuredTaskAwaiter.GetResult();
					int num3 = curIndex;
					curIndex = num3 + 1;
					num3 = leftCount;
					leftCount = num3 - 1;
					goto IL_219;
					IL_212:
					configuredTaskAwaiter.GetResult();
					IL_219:
					if (writeLen >= 0 || needWriteNewLine)
					{
						goto IL_CA;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_24C:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000E77 RID: 3703 RVA: 0x00056EB4 File Offset: 0x000550B4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000A1C RID: 2588
			public int <>1__state;

			// Token: 0x04000A1D RID: 2589
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000A1E RID: 2590
			public string text;

			// Token: 0x04000A1F RID: 2591
			public XmlUtf8RawTextWriter <>4__this;

			// Token: 0x04000A20 RID: 2592
			private int <curIndex>5__1;

			// Token: 0x04000A21 RID: 2593
			private int <leftCount>5__2;

			// Token: 0x04000A22 RID: 2594
			private int <writeLen>5__3;

			// Token: 0x04000A23 RID: 2595
			private bool <needWriteNewLine>5__4;

			// Token: 0x04000A24 RID: 2596
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
