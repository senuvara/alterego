﻿using System;

namespace System.Xml
{
	/// <summary>Specifies the type of validation to perform.</summary>
	// Token: 0x020000C6 RID: 198
	public enum ValidationType
	{
		/// <summary>No validation is performed. This setting creates an XML 1.0 compliant non-validating parser.</summary>
		// Token: 0x040003CE RID: 974
		None,
		/// <summary>Validates if DTD or schema information is found.</summary>
		// Token: 0x040003CF RID: 975
		[Obsolete("Validation type should be specified as DTD or Schema.")]
		Auto,
		/// <summary>Validates according to the DTD.</summary>
		// Token: 0x040003D0 RID: 976
		DTD,
		/// <summary>Validate according to XML-Data Reduced (XDR) schemas, including inline XDR schemas. XDR schemas are recognized using the <see langword="x-schema" /> namespace prefix or the <see cref="P:System.Xml.XmlValidatingReader.Schemas" /> property.</summary>
		// Token: 0x040003D1 RID: 977
		[Obsolete("XDR Validation through XmlValidatingReader is obsoleted")]
		XDR,
		/// <summary>Validate according to XML Schema definition language (XSD) schemas, including inline XML Schemas. XML Schemas are associated with namespace URIs either by using the <see langword="schemaLocation" /> attribute or the provided <see langword="Schemas" /> property.</summary>
		// Token: 0x040003D2 RID: 978
		Schema
	}
}
