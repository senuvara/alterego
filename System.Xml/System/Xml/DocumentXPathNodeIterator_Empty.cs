﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x0200020C RID: 524
	internal sealed class DocumentXPathNodeIterator_Empty : XPathNodeIterator
	{
		// Token: 0x060012D5 RID: 4821 RVA: 0x00070848 File Offset: 0x0006EA48
		internal DocumentXPathNodeIterator_Empty(DocumentXPathNavigator nav)
		{
			this.nav = nav.Clone();
		}

		// Token: 0x060012D6 RID: 4822 RVA: 0x0007085C File Offset: 0x0006EA5C
		internal DocumentXPathNodeIterator_Empty(DocumentXPathNodeIterator_Empty other)
		{
			this.nav = other.nav.Clone();
		}

		// Token: 0x060012D7 RID: 4823 RVA: 0x00070875 File Offset: 0x0006EA75
		public override XPathNodeIterator Clone()
		{
			return new DocumentXPathNodeIterator_Empty(this);
		}

		// Token: 0x060012D8 RID: 4824 RVA: 0x000020CD File Offset: 0x000002CD
		public override bool MoveNext()
		{
			return false;
		}

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x060012D9 RID: 4825 RVA: 0x0007087D File Offset: 0x0006EA7D
		public override XPathNavigator Current
		{
			get
			{
				return this.nav;
			}
		}

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x060012DA RID: 4826 RVA: 0x000020CD File Offset: 0x000002CD
		public override int CurrentPosition
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x060012DB RID: 4827 RVA: 0x000020CD File Offset: 0x000002CD
		public override int Count
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x04000D3C RID: 3388
		private XPathNavigator nav;
	}
}
