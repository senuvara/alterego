﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020000CF RID: 207
	internal class XmlCharCheckingReader : XmlWrappingReader
	{
		// Token: 0x0600075D RID: 1885 RVA: 0x0001CED0 File Offset: 0x0001B0D0
		internal XmlCharCheckingReader(XmlReader reader, bool checkCharacters, bool ignoreWhitespace, bool ignoreComments, bool ignorePis, DtdProcessing dtdProcessing) : base(reader)
		{
			this.state = XmlCharCheckingReader.State.Initial;
			this.checkCharacters = checkCharacters;
			this.ignoreWhitespace = ignoreWhitespace;
			this.ignoreComments = ignoreComments;
			this.ignorePis = ignorePis;
			this.dtdProcessing = dtdProcessing;
			this.lastNodeType = XmlNodeType.None;
			if (checkCharacters)
			{
				this.xmlCharType = XmlCharType.Instance;
			}
		}

		// Token: 0x1700016F RID: 367
		// (get) Token: 0x0600075E RID: 1886 RVA: 0x0001CF28 File Offset: 0x0001B128
		public override XmlReaderSettings Settings
		{
			get
			{
				XmlReaderSettings xmlReaderSettings = this.reader.Settings;
				if (xmlReaderSettings == null)
				{
					xmlReaderSettings = new XmlReaderSettings();
				}
				else
				{
					xmlReaderSettings = xmlReaderSettings.Clone();
				}
				if (this.checkCharacters)
				{
					xmlReaderSettings.CheckCharacters = true;
				}
				if (this.ignoreWhitespace)
				{
					xmlReaderSettings.IgnoreWhitespace = true;
				}
				if (this.ignoreComments)
				{
					xmlReaderSettings.IgnoreComments = true;
				}
				if (this.ignorePis)
				{
					xmlReaderSettings.IgnoreProcessingInstructions = true;
				}
				if (this.dtdProcessing != (DtdProcessing)(-1))
				{
					xmlReaderSettings.DtdProcessing = this.dtdProcessing;
				}
				xmlReaderSettings.ReadOnly = true;
				return xmlReaderSettings;
			}
		}

		// Token: 0x0600075F RID: 1887 RVA: 0x0001CFAC File Offset: 0x0001B1AC
		public override bool MoveToAttribute(string name)
		{
			if (this.state == XmlCharCheckingReader.State.InReadBinary)
			{
				this.FinishReadBinary();
			}
			return this.reader.MoveToAttribute(name);
		}

		// Token: 0x06000760 RID: 1888 RVA: 0x0001CFC9 File Offset: 0x0001B1C9
		public override bool MoveToAttribute(string name, string ns)
		{
			if (this.state == XmlCharCheckingReader.State.InReadBinary)
			{
				this.FinishReadBinary();
			}
			return this.reader.MoveToAttribute(name, ns);
		}

		// Token: 0x06000761 RID: 1889 RVA: 0x0001CFE7 File Offset: 0x0001B1E7
		public override void MoveToAttribute(int i)
		{
			if (this.state == XmlCharCheckingReader.State.InReadBinary)
			{
				this.FinishReadBinary();
			}
			this.reader.MoveToAttribute(i);
		}

		// Token: 0x06000762 RID: 1890 RVA: 0x0001D004 File Offset: 0x0001B204
		public override bool MoveToFirstAttribute()
		{
			if (this.state == XmlCharCheckingReader.State.InReadBinary)
			{
				this.FinishReadBinary();
			}
			return this.reader.MoveToFirstAttribute();
		}

		// Token: 0x06000763 RID: 1891 RVA: 0x0001D020 File Offset: 0x0001B220
		public override bool MoveToNextAttribute()
		{
			if (this.state == XmlCharCheckingReader.State.InReadBinary)
			{
				this.FinishReadBinary();
			}
			return this.reader.MoveToNextAttribute();
		}

		// Token: 0x06000764 RID: 1892 RVA: 0x0001D03C File Offset: 0x0001B23C
		public override bool MoveToElement()
		{
			if (this.state == XmlCharCheckingReader.State.InReadBinary)
			{
				this.FinishReadBinary();
			}
			return this.reader.MoveToElement();
		}

		// Token: 0x06000765 RID: 1893 RVA: 0x0001D058 File Offset: 0x0001B258
		public override bool Read()
		{
			switch (this.state)
			{
			case XmlCharCheckingReader.State.Initial:
				this.state = XmlCharCheckingReader.State.Interactive;
				if (this.reader.ReadState != ReadState.Initial)
				{
					goto IL_55;
				}
				break;
			case XmlCharCheckingReader.State.InReadBinary:
				this.FinishReadBinary();
				this.state = XmlCharCheckingReader.State.Interactive;
				break;
			case XmlCharCheckingReader.State.Error:
				return false;
			case XmlCharCheckingReader.State.Interactive:
				break;
			default:
				return false;
			}
			if (!this.reader.Read())
			{
				return false;
			}
			IL_55:
			XmlNodeType nodeType = this.reader.NodeType;
			if (!this.checkCharacters)
			{
				switch (nodeType)
				{
				case XmlNodeType.ProcessingInstruction:
					if (this.ignorePis)
					{
						return this.Read();
					}
					break;
				case XmlNodeType.Comment:
					if (this.ignoreComments)
					{
						return this.Read();
					}
					break;
				case XmlNodeType.DocumentType:
					if (this.dtdProcessing == DtdProcessing.Prohibit)
					{
						this.Throw("For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.", string.Empty);
					}
					else if (this.dtdProcessing == DtdProcessing.Ignore)
					{
						return this.Read();
					}
					break;
				case XmlNodeType.Whitespace:
					if (this.ignoreWhitespace)
					{
						return this.Read();
					}
					break;
				}
				return true;
			}
			switch (nodeType)
			{
			case XmlNodeType.Element:
				if (this.checkCharacters)
				{
					this.ValidateQName(this.reader.Prefix, this.reader.LocalName);
					if (this.reader.MoveToFirstAttribute())
					{
						do
						{
							this.ValidateQName(this.reader.Prefix, this.reader.LocalName);
							this.CheckCharacters(this.reader.Value);
						}
						while (this.reader.MoveToNextAttribute());
						this.reader.MoveToElement();
					}
				}
				break;
			case XmlNodeType.Text:
			case XmlNodeType.CDATA:
				if (this.checkCharacters)
				{
					this.CheckCharacters(this.reader.Value);
				}
				break;
			case XmlNodeType.EntityReference:
				if (this.checkCharacters)
				{
					this.ValidateQName(this.reader.Name);
				}
				break;
			case XmlNodeType.ProcessingInstruction:
				if (this.ignorePis)
				{
					return this.Read();
				}
				if (this.checkCharacters)
				{
					this.ValidateQName(this.reader.Name);
					this.CheckCharacters(this.reader.Value);
				}
				break;
			case XmlNodeType.Comment:
				if (this.ignoreComments)
				{
					return this.Read();
				}
				if (this.checkCharacters)
				{
					this.CheckCharacters(this.reader.Value);
				}
				break;
			case XmlNodeType.DocumentType:
				if (this.dtdProcessing == DtdProcessing.Prohibit)
				{
					this.Throw("For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.", string.Empty);
				}
				else if (this.dtdProcessing == DtdProcessing.Ignore)
				{
					return this.Read();
				}
				if (this.checkCharacters)
				{
					this.ValidateQName(this.reader.Name);
					this.CheckCharacters(this.reader.Value);
					string attribute = this.reader.GetAttribute("SYSTEM");
					if (attribute != null)
					{
						this.CheckCharacters(attribute);
					}
					attribute = this.reader.GetAttribute("PUBLIC");
					int invCharIndex;
					if (attribute != null && (invCharIndex = this.xmlCharType.IsPublicId(attribute)) >= 0)
					{
						this.Throw("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(attribute, invCharIndex));
					}
				}
				break;
			case XmlNodeType.Whitespace:
				if (this.ignoreWhitespace)
				{
					return this.Read();
				}
				if (this.checkCharacters)
				{
					this.CheckWhitespace(this.reader.Value);
				}
				break;
			case XmlNodeType.SignificantWhitespace:
				if (this.checkCharacters)
				{
					this.CheckWhitespace(this.reader.Value);
				}
				break;
			case XmlNodeType.EndElement:
				if (this.checkCharacters)
				{
					this.ValidateQName(this.reader.Prefix, this.reader.LocalName);
				}
				break;
			}
			this.lastNodeType = nodeType;
			return true;
		}

		// Token: 0x17000170 RID: 368
		// (get) Token: 0x06000766 RID: 1894 RVA: 0x0001D3EC File Offset: 0x0001B5EC
		public override ReadState ReadState
		{
			get
			{
				switch (this.state)
				{
				case XmlCharCheckingReader.State.Initial:
					if (this.reader.ReadState != ReadState.Closed)
					{
						return ReadState.Initial;
					}
					return ReadState.Closed;
				case XmlCharCheckingReader.State.Error:
					return ReadState.Error;
				}
				return this.reader.ReadState;
			}
		}

		// Token: 0x06000767 RID: 1895 RVA: 0x0001D437 File Offset: 0x0001B637
		public override bool ReadAttributeValue()
		{
			if (this.state == XmlCharCheckingReader.State.InReadBinary)
			{
				this.FinishReadBinary();
			}
			return this.reader.ReadAttributeValue();
		}

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x06000768 RID: 1896 RVA: 0x000033DE File Offset: 0x000015DE
		public override bool CanReadBinaryContent
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000769 RID: 1897 RVA: 0x0001D454 File Offset: 0x0001B654
		public override int ReadContentAsBase64(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.state != XmlCharCheckingReader.State.InReadBinary)
			{
				if (base.CanReadBinaryContent && !this.checkCharacters)
				{
					this.readBinaryHelper = null;
					this.state = XmlCharCheckingReader.State.InReadBinary;
					return base.ReadContentAsBase64(buffer, index, count);
				}
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
			}
			else if (this.readBinaryHelper == null)
			{
				return base.ReadContentAsBase64(buffer, index, count);
			}
			this.state = XmlCharCheckingReader.State.Interactive;
			int result = this.readBinaryHelper.ReadContentAsBase64(buffer, index, count);
			this.state = XmlCharCheckingReader.State.InReadBinary;
			return result;
		}

		// Token: 0x0600076A RID: 1898 RVA: 0x0001D4E0 File Offset: 0x0001B6E0
		public override int ReadContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.state != XmlCharCheckingReader.State.InReadBinary)
			{
				if (base.CanReadBinaryContent && !this.checkCharacters)
				{
					this.readBinaryHelper = null;
					this.state = XmlCharCheckingReader.State.InReadBinary;
					return base.ReadContentAsBinHex(buffer, index, count);
				}
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
			}
			else if (this.readBinaryHelper == null)
			{
				return base.ReadContentAsBinHex(buffer, index, count);
			}
			this.state = XmlCharCheckingReader.State.Interactive;
			int result = this.readBinaryHelper.ReadContentAsBinHex(buffer, index, count);
			this.state = XmlCharCheckingReader.State.InReadBinary;
			return result;
		}

		// Token: 0x0600076B RID: 1899 RVA: 0x0001D56C File Offset: 0x0001B76C
		public override int ReadElementContentAsBase64(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.state != XmlCharCheckingReader.State.InReadBinary)
			{
				if (base.CanReadBinaryContent && !this.checkCharacters)
				{
					this.readBinaryHelper = null;
					this.state = XmlCharCheckingReader.State.InReadBinary;
					return base.ReadElementContentAsBase64(buffer, index, count);
				}
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
			}
			else if (this.readBinaryHelper == null)
			{
				return base.ReadElementContentAsBase64(buffer, index, count);
			}
			this.state = XmlCharCheckingReader.State.Interactive;
			int result = this.readBinaryHelper.ReadElementContentAsBase64(buffer, index, count);
			this.state = XmlCharCheckingReader.State.InReadBinary;
			return result;
		}

		// Token: 0x0600076C RID: 1900 RVA: 0x0001D638 File Offset: 0x0001B838
		public override int ReadElementContentAsBinHex(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.ReadState != ReadState.Interactive)
			{
				return 0;
			}
			if (this.state != XmlCharCheckingReader.State.InReadBinary)
			{
				if (base.CanReadBinaryContent && !this.checkCharacters)
				{
					this.readBinaryHelper = null;
					this.state = XmlCharCheckingReader.State.InReadBinary;
					return base.ReadElementContentAsBinHex(buffer, index, count);
				}
				this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
			}
			else if (this.readBinaryHelper == null)
			{
				return base.ReadElementContentAsBinHex(buffer, index, count);
			}
			this.state = XmlCharCheckingReader.State.Interactive;
			int result = this.readBinaryHelper.ReadElementContentAsBinHex(buffer, index, count);
			this.state = XmlCharCheckingReader.State.InReadBinary;
			return result;
		}

		// Token: 0x0600076D RID: 1901 RVA: 0x0001D702 File Offset: 0x0001B902
		private void Throw(string res, string arg)
		{
			this.state = XmlCharCheckingReader.State.Error;
			throw new XmlException(res, arg, null);
		}

		// Token: 0x0600076E RID: 1902 RVA: 0x0001D713 File Offset: 0x0001B913
		private void Throw(string res, string[] args)
		{
			this.state = XmlCharCheckingReader.State.Error;
			throw new XmlException(res, args, null);
		}

		// Token: 0x0600076F RID: 1903 RVA: 0x0001D724 File Offset: 0x0001B924
		private void CheckWhitespace(string value)
		{
			int invCharIndex;
			if ((invCharIndex = this.xmlCharType.IsOnlyWhitespaceWithPos(value)) != -1)
			{
				this.Throw("The Whitespace or SignificantWhitespace node can contain only XML white space characters. '{0}' is not an XML white space character.", XmlException.BuildCharExceptionArgs(value, invCharIndex));
			}
		}

		// Token: 0x06000770 RID: 1904 RVA: 0x0001D754 File Offset: 0x0001B954
		private void ValidateQName(string name)
		{
			string text;
			string text2;
			ValidateNames.ParseQNameThrow(name, out text, out text2);
		}

		// Token: 0x06000771 RID: 1905 RVA: 0x0001D76C File Offset: 0x0001B96C
		private void ValidateQName(string prefix, string localName)
		{
			try
			{
				if (prefix.Length > 0)
				{
					ValidateNames.ParseNCNameThrow(prefix);
				}
				ValidateNames.ParseNCNameThrow(localName);
			}
			catch
			{
				this.state = XmlCharCheckingReader.State.Error;
				throw;
			}
		}

		// Token: 0x06000772 RID: 1906 RVA: 0x0001D7AC File Offset: 0x0001B9AC
		private void CheckCharacters(string value)
		{
			XmlConvert.VerifyCharData(value, ExceptionType.ArgumentException, ExceptionType.XmlException);
		}

		// Token: 0x06000773 RID: 1907 RVA: 0x0001D7B6 File Offset: 0x0001B9B6
		private void FinishReadBinary()
		{
			this.state = XmlCharCheckingReader.State.Interactive;
			if (this.readBinaryHelper != null)
			{
				this.readBinaryHelper.Finish();
			}
		}

		// Token: 0x06000774 RID: 1908 RVA: 0x0001D7D4 File Offset: 0x0001B9D4
		public override async Task<bool> ReadAsync()
		{
			switch (this.state)
			{
			case XmlCharCheckingReader.State.Initial:
				this.state = XmlCharCheckingReader.State.Interactive;
				if (this.reader.ReadState != ReadState.Initial)
				{
					goto IL_176;
				}
				break;
			case XmlCharCheckingReader.State.InReadBinary:
				await this.FinishReadBinaryAsync().ConfigureAwait(false);
				this.state = XmlCharCheckingReader.State.Interactive;
				break;
			case XmlCharCheckingReader.State.Error:
				return false;
			case XmlCharCheckingReader.State.Interactive:
				break;
			default:
				return false;
			}
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			if (!configuredTaskAwaiter.GetResult())
			{
				return false;
			}
			IL_176:
			XmlNodeType nodeType = this.reader.NodeType;
			bool result;
			if (!this.checkCharacters)
			{
				switch (nodeType)
				{
				case XmlNodeType.ProcessingInstruction:
					if (this.ignorePis)
					{
						return await this.ReadAsync().ConfigureAwait(false);
					}
					break;
				case XmlNodeType.Comment:
					if (this.ignoreComments)
					{
						return await this.ReadAsync().ConfigureAwait(false);
					}
					break;
				case XmlNodeType.DocumentType:
					if (this.dtdProcessing == DtdProcessing.Prohibit)
					{
						this.Throw("For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.", string.Empty);
					}
					else if (this.dtdProcessing == DtdProcessing.Ignore)
					{
						return await this.ReadAsync().ConfigureAwait(false);
					}
					break;
				case XmlNodeType.Whitespace:
					if (this.ignoreWhitespace)
					{
						return await this.ReadAsync().ConfigureAwait(false);
					}
					break;
				}
				result = true;
			}
			else
			{
				switch (nodeType)
				{
				case XmlNodeType.Element:
					if (this.checkCharacters)
					{
						this.ValidateQName(this.reader.Prefix, this.reader.LocalName);
						if (this.reader.MoveToFirstAttribute())
						{
							do
							{
								this.ValidateQName(this.reader.Prefix, this.reader.LocalName);
								this.CheckCharacters(this.reader.Value);
							}
							while (this.reader.MoveToNextAttribute());
							this.reader.MoveToElement();
						}
					}
					break;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
					if (this.checkCharacters)
					{
						this.CheckCharacters(await this.reader.GetValueAsync().ConfigureAwait(false));
					}
					break;
				case XmlNodeType.EntityReference:
					if (this.checkCharacters)
					{
						this.ValidateQName(this.reader.Name);
					}
					break;
				case XmlNodeType.ProcessingInstruction:
					if (this.ignorePis)
					{
						return await this.ReadAsync().ConfigureAwait(false);
					}
					if (this.checkCharacters)
					{
						this.ValidateQName(this.reader.Name);
						this.CheckCharacters(this.reader.Value);
					}
					break;
				case XmlNodeType.Comment:
					if (this.ignoreComments)
					{
						return await this.ReadAsync().ConfigureAwait(false);
					}
					if (this.checkCharacters)
					{
						this.CheckCharacters(this.reader.Value);
					}
					break;
				case XmlNodeType.DocumentType:
					if (this.dtdProcessing == DtdProcessing.Prohibit)
					{
						this.Throw("For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.", string.Empty);
					}
					else if (this.dtdProcessing == DtdProcessing.Ignore)
					{
						return await this.ReadAsync().ConfigureAwait(false);
					}
					if (this.checkCharacters)
					{
						this.ValidateQName(this.reader.Name);
						this.CheckCharacters(this.reader.Value);
						string attribute = this.reader.GetAttribute("SYSTEM");
						if (attribute != null)
						{
							this.CheckCharacters(attribute);
						}
						attribute = this.reader.GetAttribute("PUBLIC");
						if (attribute != null)
						{
							int num = this.xmlCharType.IsPublicId(attribute);
							if (num >= 0)
							{
								this.Throw("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(attribute, num));
							}
						}
					}
					break;
				case XmlNodeType.Whitespace:
					if (this.ignoreWhitespace)
					{
						return await this.ReadAsync().ConfigureAwait(false);
					}
					if (this.checkCharacters)
					{
						this.CheckWhitespace(await this.reader.GetValueAsync().ConfigureAwait(false));
					}
					break;
				case XmlNodeType.SignificantWhitespace:
					if (this.checkCharacters)
					{
						this.CheckWhitespace(await this.reader.GetValueAsync().ConfigureAwait(false));
					}
					break;
				case XmlNodeType.EndElement:
					if (this.checkCharacters)
					{
						this.ValidateQName(this.reader.Prefix, this.reader.LocalName);
					}
					break;
				}
				this.lastNodeType = nodeType;
				result = true;
			}
			return result;
		}

		// Token: 0x06000775 RID: 1909 RVA: 0x0001D81C File Offset: 0x0001BA1C
		public override async Task<int> ReadContentAsBase64Async(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.state != XmlCharCheckingReader.State.InReadBinary)
				{
					if (base.CanReadBinaryContent && !this.checkCharacters)
					{
						this.readBinaryHelper = null;
						this.state = XmlCharCheckingReader.State.InReadBinary;
						return await base.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
					}
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
				}
				else if (this.readBinaryHelper == null)
				{
					return await base.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
				}
				this.state = XmlCharCheckingReader.State.Interactive;
				int num = await this.readBinaryHelper.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
				this.state = XmlCharCheckingReader.State.InReadBinary;
				result = num;
			}
			return result;
		}

		// Token: 0x06000776 RID: 1910 RVA: 0x0001D87C File Offset: 0x0001BA7C
		public override async Task<int> ReadContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.state != XmlCharCheckingReader.State.InReadBinary)
				{
					if (base.CanReadBinaryContent && !this.checkCharacters)
					{
						this.readBinaryHelper = null;
						this.state = XmlCharCheckingReader.State.InReadBinary;
						return await base.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
					}
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
				}
				else if (this.readBinaryHelper == null)
				{
					return await base.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
				}
				this.state = XmlCharCheckingReader.State.Interactive;
				int num = await this.readBinaryHelper.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
				this.state = XmlCharCheckingReader.State.InReadBinary;
				result = num;
			}
			return result;
		}

		// Token: 0x06000777 RID: 1911 RVA: 0x0001D8DC File Offset: 0x0001BADC
		public override async Task<int> ReadElementContentAsBase64Async(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.state != XmlCharCheckingReader.State.InReadBinary)
				{
					if (base.CanReadBinaryContent && !this.checkCharacters)
					{
						this.readBinaryHelper = null;
						this.state = XmlCharCheckingReader.State.InReadBinary;
						return await base.ReadElementContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
					}
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
				}
				else if (this.readBinaryHelper == null)
				{
					return await base.ReadElementContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
				}
				this.state = XmlCharCheckingReader.State.Interactive;
				int num = await this.readBinaryHelper.ReadElementContentAsBase64Async(buffer, index, count).ConfigureAwait(false);
				this.state = XmlCharCheckingReader.State.InReadBinary;
				result = num;
			}
			return result;
		}

		// Token: 0x06000778 RID: 1912 RVA: 0x0001D93C File Offset: 0x0001BB3C
		public override async Task<int> ReadElementContentAsBinHexAsync(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			int result;
			if (this.ReadState != ReadState.Interactive)
			{
				result = 0;
			}
			else
			{
				if (this.state != XmlCharCheckingReader.State.InReadBinary)
				{
					if (base.CanReadBinaryContent && !this.checkCharacters)
					{
						this.readBinaryHelper = null;
						this.state = XmlCharCheckingReader.State.InReadBinary;
						return await base.ReadElementContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
					}
					this.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(this.readBinaryHelper, this);
				}
				else if (this.readBinaryHelper == null)
				{
					return await base.ReadElementContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
				}
				this.state = XmlCharCheckingReader.State.Interactive;
				int num = await this.readBinaryHelper.ReadElementContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false);
				this.state = XmlCharCheckingReader.State.InReadBinary;
				result = num;
			}
			return result;
		}

		// Token: 0x06000779 RID: 1913 RVA: 0x0001D99C File Offset: 0x0001BB9C
		private async Task FinishReadBinaryAsync()
		{
			this.state = XmlCharCheckingReader.State.Interactive;
			if (this.readBinaryHelper != null)
			{
				await this.readBinaryHelper.FinishAsync().ConfigureAwait(false);
			}
		}

		// Token: 0x0600077A RID: 1914 RVA: 0x0001D9E1 File Offset: 0x0001BBE1
		[CompilerGenerated]
		[DebuggerHidden]
		private bool <>n__0()
		{
			return base.CanReadBinaryContent;
		}

		// Token: 0x0600077B RID: 1915 RVA: 0x0001D9E9 File Offset: 0x0001BBE9
		[DebuggerHidden]
		[CompilerGenerated]
		private Task<int> <>n__1(byte[] buffer, int index, int count)
		{
			return base.ReadContentAsBase64Async(buffer, index, count);
		}

		// Token: 0x0600077C RID: 1916 RVA: 0x0001D9F4 File Offset: 0x0001BBF4
		[DebuggerHidden]
		[CompilerGenerated]
		private Task<int> <>n__2(byte[] buffer, int index, int count)
		{
			return base.ReadContentAsBinHexAsync(buffer, index, count);
		}

		// Token: 0x0600077D RID: 1917 RVA: 0x0001D9FF File Offset: 0x0001BBFF
		[CompilerGenerated]
		[DebuggerHidden]
		private Task<int> <>n__3(byte[] buffer, int index, int count)
		{
			return base.ReadElementContentAsBase64Async(buffer, index, count);
		}

		// Token: 0x0600077E RID: 1918 RVA: 0x0001DA0A File Offset: 0x0001BC0A
		[DebuggerHidden]
		[CompilerGenerated]
		private Task<int> <>n__4(byte[] buffer, int index, int count)
		{
			return base.ReadElementContentAsBinHexAsync(buffer, index, count);
		}

		// Token: 0x040003E5 RID: 997
		private XmlCharCheckingReader.State state;

		// Token: 0x040003E6 RID: 998
		private bool checkCharacters;

		// Token: 0x040003E7 RID: 999
		private bool ignoreWhitespace;

		// Token: 0x040003E8 RID: 1000
		private bool ignoreComments;

		// Token: 0x040003E9 RID: 1001
		private bool ignorePis;

		// Token: 0x040003EA RID: 1002
		private DtdProcessing dtdProcessing;

		// Token: 0x040003EB RID: 1003
		private XmlNodeType lastNodeType;

		// Token: 0x040003EC RID: 1004
		private XmlCharType xmlCharType;

		// Token: 0x040003ED RID: 1005
		private ReadContentAsBinaryHelper readBinaryHelper;

		// Token: 0x020000D0 RID: 208
		private enum State
		{
			// Token: 0x040003EF RID: 1007
			Initial,
			// Token: 0x040003F0 RID: 1008
			InReadBinary,
			// Token: 0x040003F1 RID: 1009
			Error,
			// Token: 0x040003F2 RID: 1010
			Interactive
		}

		// Token: 0x020000D1 RID: 209
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsync>d__36 : IAsyncStateMachine
		{
			// Token: 0x0600077F RID: 1919 RVA: 0x0001DA18 File Offset: 0x0001BC18
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlCharCheckingReader xmlCharCheckingReader = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
					ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter6;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4;
						configuredTaskAwaiter3 = configuredTaskAwaiter4;
						configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_15F;
					case 2:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_230;
					case 3:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2A9;
					case 4:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_322;
					case 5:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_3B3;
					case 6:
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_50C;
					}
					case 7:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_5AC;
					case 8:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_654;
					case 9:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_707;
					case 10:
						configuredTaskAwaiter5 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_811;
					case 11:
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_890;
					}
					case 12:
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter7;
						configuredTaskAwaiter6 = configuredTaskAwaiter7;
						configuredTaskAwaiter7 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_918;
					}
					default:
						switch (xmlCharCheckingReader.state)
						{
						case XmlCharCheckingReader.State.Initial:
							xmlCharCheckingReader.state = XmlCharCheckingReader.State.Interactive;
							if (xmlCharCheckingReader.reader.ReadState == ReadState.Initial)
							{
								goto IL_F9;
							}
							goto IL_176;
						case XmlCharCheckingReader.State.InReadBinary:
							configuredTaskAwaiter3 = xmlCharCheckingReader.FinishReadBinaryAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 0;
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							break;
						case XmlCharCheckingReader.State.Error:
							result = false;
							goto IL_978;
						case XmlCharCheckingReader.State.Interactive:
							goto IL_F9;
						default:
							result = false;
							goto IL_978;
						}
						break;
					}
					configuredTaskAwaiter3.GetResult();
					xmlCharCheckingReader.state = XmlCharCheckingReader.State.Interactive;
					IL_F9:
					configuredTaskAwaiter5 = xmlCharCheckingReader.reader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter5.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter5;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
						return;
					}
					IL_15F:
					if (!configuredTaskAwaiter5.GetResult())
					{
						result = false;
						goto IL_978;
					}
					IL_176:
					nodeType = xmlCharCheckingReader.reader.NodeType;
					if (!xmlCharCheckingReader.checkCharacters)
					{
						switch (nodeType)
						{
						case XmlNodeType.ProcessingInstruction:
							if (xmlCharCheckingReader.ignorePis)
							{
								configuredTaskAwaiter5 = xmlCharCheckingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter5.IsCompleted)
								{
									num2 = 4;
									configuredTaskAwaiter2 = configuredTaskAwaiter5;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
									return;
								}
								goto IL_322;
							}
							break;
						case XmlNodeType.Comment:
							if (xmlCharCheckingReader.ignoreComments)
							{
								configuredTaskAwaiter5 = xmlCharCheckingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter5.IsCompleted)
								{
									num2 = 2;
									configuredTaskAwaiter2 = configuredTaskAwaiter5;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
									return;
								}
								goto IL_230;
							}
							break;
						case XmlNodeType.DocumentType:
							if (xmlCharCheckingReader.dtdProcessing == DtdProcessing.Prohibit)
							{
								xmlCharCheckingReader.Throw("For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.", string.Empty);
							}
							else if (xmlCharCheckingReader.dtdProcessing == DtdProcessing.Ignore)
							{
								configuredTaskAwaiter5 = xmlCharCheckingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter5.IsCompleted)
								{
									num2 = 5;
									configuredTaskAwaiter2 = configuredTaskAwaiter5;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
									return;
								}
								goto IL_3B3;
							}
							break;
						case XmlNodeType.Whitespace:
							if (xmlCharCheckingReader.ignoreWhitespace)
							{
								configuredTaskAwaiter5 = xmlCharCheckingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter5.IsCompleted)
								{
									num2 = 3;
									configuredTaskAwaiter2 = configuredTaskAwaiter5;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
									return;
								}
								goto IL_2A9;
							}
							break;
						}
						result = true;
						goto IL_978;
					}
					switch (nodeType)
					{
					case XmlNodeType.Element:
						if (!xmlCharCheckingReader.checkCharacters)
						{
							goto IL_94F;
						}
						xmlCharCheckingReader.ValidateQName(xmlCharCheckingReader.reader.Prefix, xmlCharCheckingReader.reader.LocalName);
						if (xmlCharCheckingReader.reader.MoveToFirstAttribute())
						{
							do
							{
								xmlCharCheckingReader.ValidateQName(xmlCharCheckingReader.reader.Prefix, xmlCharCheckingReader.reader.LocalName);
								xmlCharCheckingReader.CheckCharacters(xmlCharCheckingReader.reader.Value);
							}
							while (xmlCharCheckingReader.reader.MoveToNextAttribute());
							xmlCharCheckingReader.reader.MoveToElement();
							goto IL_94F;
						}
						goto IL_94F;
					case XmlNodeType.Attribute:
					case XmlNodeType.Entity:
					case XmlNodeType.Document:
					case XmlNodeType.DocumentFragment:
					case XmlNodeType.Notation:
						goto IL_94F;
					case XmlNodeType.Text:
					case XmlNodeType.CDATA:
						if (!xmlCharCheckingReader.checkCharacters)
						{
							goto IL_94F;
						}
						configuredTaskAwaiter6 = xmlCharCheckingReader.reader.GetValueAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter6.IsCompleted)
						{
							num2 = 6;
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter6, ref this);
							return;
						}
						goto IL_50C;
					case XmlNodeType.EntityReference:
						if (xmlCharCheckingReader.checkCharacters)
						{
							xmlCharCheckingReader.ValidateQName(xmlCharCheckingReader.reader.Name);
							goto IL_94F;
						}
						goto IL_94F;
					case XmlNodeType.ProcessingInstruction:
						if (xmlCharCheckingReader.ignorePis)
						{
							configuredTaskAwaiter5 = xmlCharCheckingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 7;
								configuredTaskAwaiter2 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_5AC;
						}
						else
						{
							if (xmlCharCheckingReader.checkCharacters)
							{
								xmlCharCheckingReader.ValidateQName(xmlCharCheckingReader.reader.Name);
								xmlCharCheckingReader.CheckCharacters(xmlCharCheckingReader.reader.Value);
								goto IL_94F;
							}
							goto IL_94F;
						}
						break;
					case XmlNodeType.Comment:
						if (xmlCharCheckingReader.ignoreComments)
						{
							configuredTaskAwaiter5 = xmlCharCheckingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 8;
								configuredTaskAwaiter2 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_654;
						}
						else
						{
							if (xmlCharCheckingReader.checkCharacters)
							{
								xmlCharCheckingReader.CheckCharacters(xmlCharCheckingReader.reader.Value);
								goto IL_94F;
							}
							goto IL_94F;
						}
						break;
					case XmlNodeType.DocumentType:
					{
						if (xmlCharCheckingReader.dtdProcessing == DtdProcessing.Prohibit)
						{
							xmlCharCheckingReader.Throw("For security reasons DTD is prohibited in this XML document. To enable DTD processing set the DtdProcessing property on XmlReaderSettings to Parse and pass the settings into XmlReader.Create method.", string.Empty);
						}
						else if (xmlCharCheckingReader.dtdProcessing == DtdProcessing.Ignore)
						{
							configuredTaskAwaiter5 = xmlCharCheckingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 9;
								configuredTaskAwaiter2 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_707;
						}
						if (!xmlCharCheckingReader.checkCharacters)
						{
							goto IL_94F;
						}
						xmlCharCheckingReader.ValidateQName(xmlCharCheckingReader.reader.Name);
						xmlCharCheckingReader.CheckCharacters(xmlCharCheckingReader.reader.Value);
						string attribute = xmlCharCheckingReader.reader.GetAttribute("SYSTEM");
						if (attribute != null)
						{
							xmlCharCheckingReader.CheckCharacters(attribute);
						}
						attribute = xmlCharCheckingReader.reader.GetAttribute("PUBLIC");
						int invCharIndex;
						if (attribute != null && (invCharIndex = xmlCharCheckingReader.xmlCharType.IsPublicId(attribute)) >= 0)
						{
							xmlCharCheckingReader.Throw("'{0}', hexadecimal value {1}, is an invalid character.", XmlException.BuildCharExceptionArgs(attribute, invCharIndex));
							goto IL_94F;
						}
						goto IL_94F;
					}
					case XmlNodeType.Whitespace:
						if (xmlCharCheckingReader.ignoreWhitespace)
						{
							configuredTaskAwaiter5 = xmlCharCheckingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter5.IsCompleted)
							{
								num2 = 10;
								configuredTaskAwaiter2 = configuredTaskAwaiter5;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter5, ref this);
								return;
							}
							goto IL_811;
						}
						else
						{
							if (!xmlCharCheckingReader.checkCharacters)
							{
								goto IL_94F;
							}
							configuredTaskAwaiter6 = xmlCharCheckingReader.reader.GetValueAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter6.IsCompleted)
							{
								num2 = 11;
								ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter6, ref this);
								return;
							}
							goto IL_890;
						}
						break;
					case XmlNodeType.SignificantWhitespace:
						if (!xmlCharCheckingReader.checkCharacters)
						{
							goto IL_94F;
						}
						configuredTaskAwaiter6 = xmlCharCheckingReader.reader.GetValueAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter6.IsCompleted)
						{
							num2 = 12;
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter7 = configuredTaskAwaiter6;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadAsync>d__36>(ref configuredTaskAwaiter6, ref this);
							return;
						}
						goto IL_918;
					case XmlNodeType.EndElement:
						if (xmlCharCheckingReader.checkCharacters)
						{
							xmlCharCheckingReader.ValidateQName(xmlCharCheckingReader.reader.Prefix, xmlCharCheckingReader.reader.LocalName);
							goto IL_94F;
						}
						goto IL_94F;
					default:
						goto IL_94F;
					}
					IL_230:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_978;
					IL_2A9:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_978;
					IL_322:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_978;
					IL_3B3:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_978;
					IL_50C:
					string result2 = configuredTaskAwaiter6.GetResult();
					xmlCharCheckingReader.CheckCharacters(result2);
					goto IL_94F;
					IL_5AC:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_978;
					IL_654:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_978;
					IL_707:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_978;
					IL_811:
					result = configuredTaskAwaiter5.GetResult();
					goto IL_978;
					IL_890:
					result2 = configuredTaskAwaiter6.GetResult();
					xmlCharCheckingReader.CheckWhitespace(result2);
					goto IL_94F;
					IL_918:
					result2 = configuredTaskAwaiter6.GetResult();
					xmlCharCheckingReader.CheckWhitespace(result2);
					IL_94F:
					xmlCharCheckingReader.lastNodeType = nodeType;
					result = true;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_978:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000780 RID: 1920 RVA: 0x0001E3D0 File Offset: 0x0001C5D0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040003F3 RID: 1011
			public int <>1__state;

			// Token: 0x040003F4 RID: 1012
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x040003F5 RID: 1013
			public XmlCharCheckingReader <>4__this;

			// Token: 0x040003F6 RID: 1014
			private XmlNodeType <nodeType>5__1;

			// Token: 0x040003F7 RID: 1015
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040003F8 RID: 1016
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x040003F9 RID: 1017
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x020000D2 RID: 210
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBase64Async>d__37 : IAsyncStateMachine
		{
			// Token: 0x06000781 RID: 1921 RVA: 0x0001E3E0 File Offset: 0x0001C5E0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlCharCheckingReader xmlCharCheckingReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_16C;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1F2;
					}
					default:
						if (xmlCharCheckingReader.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_21C;
						}
						if (xmlCharCheckingReader.state != XmlCharCheckingReader.State.InReadBinary)
						{
							if (xmlCharCheckingReader.<>n__0() && !xmlCharCheckingReader.checkCharacters)
							{
								xmlCharCheckingReader.readBinaryHelper = null;
								xmlCharCheckingReader.state = XmlCharCheckingReader.State.InReadBinary;
								configuredTaskAwaiter = xmlCharCheckingReader.<>n__1(buffer, index, count).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadContentAsBase64Async>d__37>(ref configuredTaskAwaiter, ref this);
									return;
								}
								break;
							}
							else
							{
								xmlCharCheckingReader.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xmlCharCheckingReader.readBinaryHelper, xmlCharCheckingReader);
							}
						}
						else if (xmlCharCheckingReader.readBinaryHelper == null)
						{
							configuredTaskAwaiter = xmlCharCheckingReader.<>n__1(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadContentAsBase64Async>d__37>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_16C;
						}
						xmlCharCheckingReader.state = XmlCharCheckingReader.State.Interactive;
						configuredTaskAwaiter = xmlCharCheckingReader.readBinaryHelper.ReadContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadContentAsBase64Async>d__37>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_1F2;
					}
					result = configuredTaskAwaiter.GetResult();
					goto IL_21C;
					IL_16C:
					result = configuredTaskAwaiter.GetResult();
					goto IL_21C;
					IL_1F2:
					int result2 = configuredTaskAwaiter.GetResult();
					xmlCharCheckingReader.state = XmlCharCheckingReader.State.InReadBinary;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_21C:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000782 RID: 1922 RVA: 0x0001E63C File Offset: 0x0001C83C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040003FA RID: 1018
			public int <>1__state;

			// Token: 0x040003FB RID: 1019
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x040003FC RID: 1020
			public XmlCharCheckingReader <>4__this;

			// Token: 0x040003FD RID: 1021
			public byte[] buffer;

			// Token: 0x040003FE RID: 1022
			public int index;

			// Token: 0x040003FF RID: 1023
			public int count;

			// Token: 0x04000400 RID: 1024
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000D3 RID: 211
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadContentAsBinHexAsync>d__38 : IAsyncStateMachine
		{
			// Token: 0x06000783 RID: 1923 RVA: 0x0001E64C File Offset: 0x0001C84C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlCharCheckingReader xmlCharCheckingReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_16C;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1F2;
					}
					default:
						if (xmlCharCheckingReader.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_21C;
						}
						if (xmlCharCheckingReader.state != XmlCharCheckingReader.State.InReadBinary)
						{
							if (xmlCharCheckingReader.<>n__0() && !xmlCharCheckingReader.checkCharacters)
							{
								xmlCharCheckingReader.readBinaryHelper = null;
								xmlCharCheckingReader.state = XmlCharCheckingReader.State.InReadBinary;
								configuredTaskAwaiter = xmlCharCheckingReader.<>n__2(buffer, index, count).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadContentAsBinHexAsync>d__38>(ref configuredTaskAwaiter, ref this);
									return;
								}
								break;
							}
							else
							{
								xmlCharCheckingReader.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xmlCharCheckingReader.readBinaryHelper, xmlCharCheckingReader);
							}
						}
						else if (xmlCharCheckingReader.readBinaryHelper == null)
						{
							configuredTaskAwaiter = xmlCharCheckingReader.<>n__2(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadContentAsBinHexAsync>d__38>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_16C;
						}
						xmlCharCheckingReader.state = XmlCharCheckingReader.State.Interactive;
						configuredTaskAwaiter = xmlCharCheckingReader.readBinaryHelper.ReadContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadContentAsBinHexAsync>d__38>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_1F2;
					}
					result = configuredTaskAwaiter.GetResult();
					goto IL_21C;
					IL_16C:
					result = configuredTaskAwaiter.GetResult();
					goto IL_21C;
					IL_1F2:
					int result2 = configuredTaskAwaiter.GetResult();
					xmlCharCheckingReader.state = XmlCharCheckingReader.State.InReadBinary;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_21C:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000784 RID: 1924 RVA: 0x0001E8A8 File Offset: 0x0001CAA8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000401 RID: 1025
			public int <>1__state;

			// Token: 0x04000402 RID: 1026
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000403 RID: 1027
			public XmlCharCheckingReader <>4__this;

			// Token: 0x04000404 RID: 1028
			public byte[] buffer;

			// Token: 0x04000405 RID: 1029
			public int index;

			// Token: 0x04000406 RID: 1030
			public int count;

			// Token: 0x04000407 RID: 1031
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000D4 RID: 212
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBase64Async>d__39 : IAsyncStateMachine
		{
			// Token: 0x06000785 RID: 1925 RVA: 0x0001E8B8 File Offset: 0x0001CAB8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlCharCheckingReader xmlCharCheckingReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C9;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_24F;
					}
					default:
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (xmlCharCheckingReader.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_279;
						}
						if (xmlCharCheckingReader.state != XmlCharCheckingReader.State.InReadBinary)
						{
							if (xmlCharCheckingReader.<>n__0() && !xmlCharCheckingReader.checkCharacters)
							{
								xmlCharCheckingReader.readBinaryHelper = null;
								xmlCharCheckingReader.state = XmlCharCheckingReader.State.InReadBinary;
								configuredTaskAwaiter = xmlCharCheckingReader.<>n__3(buffer, index, count).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadElementContentAsBase64Async>d__39>(ref configuredTaskAwaiter, ref this);
									return;
								}
								break;
							}
							else
							{
								xmlCharCheckingReader.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xmlCharCheckingReader.readBinaryHelper, xmlCharCheckingReader);
							}
						}
						else if (xmlCharCheckingReader.readBinaryHelper == null)
						{
							configuredTaskAwaiter = xmlCharCheckingReader.<>n__3(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadElementContentAsBase64Async>d__39>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_1C9;
						}
						xmlCharCheckingReader.state = XmlCharCheckingReader.State.Interactive;
						configuredTaskAwaiter = xmlCharCheckingReader.readBinaryHelper.ReadElementContentAsBase64Async(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadElementContentAsBase64Async>d__39>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_24F;
					}
					result = configuredTaskAwaiter.GetResult();
					goto IL_279;
					IL_1C9:
					result = configuredTaskAwaiter.GetResult();
					goto IL_279;
					IL_24F:
					int result2 = configuredTaskAwaiter.GetResult();
					xmlCharCheckingReader.state = XmlCharCheckingReader.State.InReadBinary;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_279:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000786 RID: 1926 RVA: 0x0001EB70 File Offset: 0x0001CD70
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000408 RID: 1032
			public int <>1__state;

			// Token: 0x04000409 RID: 1033
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400040A RID: 1034
			public byte[] buffer;

			// Token: 0x0400040B RID: 1035
			public int count;

			// Token: 0x0400040C RID: 1036
			public int index;

			// Token: 0x0400040D RID: 1037
			public XmlCharCheckingReader <>4__this;

			// Token: 0x0400040E RID: 1038
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000D5 RID: 213
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadElementContentAsBinHexAsync>d__40 : IAsyncStateMachine
		{
			// Token: 0x06000787 RID: 1927 RVA: 0x0001EB80 File Offset: 0x0001CD80
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlCharCheckingReader xmlCharCheckingReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C9;
					}
					case 2:
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_24F;
					}
					default:
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (buffer.Length - index < count)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (xmlCharCheckingReader.ReadState != ReadState.Interactive)
						{
							result = 0;
							goto IL_279;
						}
						if (xmlCharCheckingReader.state != XmlCharCheckingReader.State.InReadBinary)
						{
							if (xmlCharCheckingReader.<>n__0() && !xmlCharCheckingReader.checkCharacters)
							{
								xmlCharCheckingReader.readBinaryHelper = null;
								xmlCharCheckingReader.state = XmlCharCheckingReader.State.InReadBinary;
								configuredTaskAwaiter = xmlCharCheckingReader.<>n__4(buffer, index, count).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num2 = 0;
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadElementContentAsBinHexAsync>d__40>(ref configuredTaskAwaiter, ref this);
									return;
								}
								break;
							}
							else
							{
								xmlCharCheckingReader.readBinaryHelper = ReadContentAsBinaryHelper.CreateOrReset(xmlCharCheckingReader.readBinaryHelper, xmlCharCheckingReader);
							}
						}
						else if (xmlCharCheckingReader.readBinaryHelper == null)
						{
							configuredTaskAwaiter = xmlCharCheckingReader.<>n__4(buffer, index, count).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num2 = 1;
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadElementContentAsBinHexAsync>d__40>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_1C9;
						}
						xmlCharCheckingReader.state = XmlCharCheckingReader.State.Interactive;
						configuredTaskAwaiter = xmlCharCheckingReader.readBinaryHelper.ReadElementContentAsBinHexAsync(buffer, index, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 2;
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, XmlCharCheckingReader.<ReadElementContentAsBinHexAsync>d__40>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_24F;
					}
					result = configuredTaskAwaiter.GetResult();
					goto IL_279;
					IL_1C9:
					result = configuredTaskAwaiter.GetResult();
					goto IL_279;
					IL_24F:
					int result2 = configuredTaskAwaiter.GetResult();
					xmlCharCheckingReader.state = XmlCharCheckingReader.State.InReadBinary;
					result = result2;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_279:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06000788 RID: 1928 RVA: 0x0001EE38 File Offset: 0x0001D038
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400040F RID: 1039
			public int <>1__state;

			// Token: 0x04000410 RID: 1040
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04000411 RID: 1041
			public byte[] buffer;

			// Token: 0x04000412 RID: 1042
			public int count;

			// Token: 0x04000413 RID: 1043
			public int index;

			// Token: 0x04000414 RID: 1044
			public XmlCharCheckingReader <>4__this;

			// Token: 0x04000415 RID: 1045
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000D6 RID: 214
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FinishReadBinaryAsync>d__41 : IAsyncStateMachine
		{
			// Token: 0x06000789 RID: 1929 RVA: 0x0001EE48 File Offset: 0x0001D048
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlCharCheckingReader xmlCharCheckingReader = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						xmlCharCheckingReader.state = XmlCharCheckingReader.State.Interactive;
						if (xmlCharCheckingReader.readBinaryHelper == null)
						{
							goto IL_86;
						}
						configuredTaskAwaiter = xmlCharCheckingReader.readBinaryHelper.FinishAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlCharCheckingReader.<FinishReadBinaryAsync>d__41>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_86:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600078A RID: 1930 RVA: 0x0001EF1C File Offset: 0x0001D11C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000416 RID: 1046
			public int <>1__state;

			// Token: 0x04000417 RID: 1047
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000418 RID: 1048
			public XmlCharCheckingReader <>4__this;

			// Token: 0x04000419 RID: 1049
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
