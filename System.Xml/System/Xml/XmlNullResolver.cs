﻿using System;
using System.Net;

namespace System.Xml
{
	// Token: 0x020002A9 RID: 681
	internal class XmlNullResolver : XmlResolver
	{
		// Token: 0x060018EF RID: 6383 RVA: 0x0008FAEF File Offset: 0x0008DCEF
		private XmlNullResolver()
		{
		}

		// Token: 0x060018F0 RID: 6384 RVA: 0x0008FAF7 File Offset: 0x0008DCF7
		public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
		{
			throw new XmlException("Resolving of external URIs was prohibited.", string.Empty);
		}

		// Token: 0x170004AD RID: 1197
		// (set) Token: 0x060018F1 RID: 6385 RVA: 0x000030EC File Offset: 0x000012EC
		public override ICredentials Credentials
		{
			set
			{
			}
		}

		// Token: 0x060018F2 RID: 6386 RVA: 0x0008FB08 File Offset: 0x0008DD08
		// Note: this type is marked as 'beforefieldinit'.
		static XmlNullResolver()
		{
		}

		// Token: 0x04001049 RID: 4169
		public static readonly XmlNullResolver Singleton = new XmlNullResolver();
	}
}
