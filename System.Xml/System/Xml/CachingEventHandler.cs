﻿using System;

namespace System.Xml
{
	// Token: 0x020001F1 RID: 497
	// (Invoke) Token: 0x060011A4 RID: 4516
	internal delegate void CachingEventHandler(XsdCachingReader cachingReader);
}
