﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020001ED RID: 493
	internal class XsdCachingReader : XmlReader, IXmlLineInfo
	{
		// Token: 0x06001162 RID: 4450 RVA: 0x000669D9 File Offset: 0x00064BD9
		internal XsdCachingReader(XmlReader reader, IXmlLineInfo lineInfo, CachingEventHandler handlerMethod)
		{
			this.coreReader = reader;
			this.lineInfo = lineInfo;
			this.cacheHandler = handlerMethod;
			this.attributeEvents = new ValidatingReaderNodeData[8];
			this.contentEvents = new ValidatingReaderNodeData[4];
			this.Init();
		}

		// Token: 0x06001163 RID: 4451 RVA: 0x00066A14 File Offset: 0x00064C14
		private void Init()
		{
			this.coreReaderNameTable = this.coreReader.NameTable;
			this.cacheState = XsdCachingReader.CachingReaderState.Init;
			this.contentIndex = 0;
			this.currentAttrIndex = -1;
			this.currentContentIndex = -1;
			this.attributeCount = 0;
			this.cachedNode = null;
			this.readAhead = false;
			if (this.coreReader.NodeType == XmlNodeType.Element)
			{
				ValidatingReaderNodeData validatingReaderNodeData = this.AddContent(this.coreReader.NodeType);
				validatingReaderNodeData.SetItemData(this.coreReader.LocalName, this.coreReader.Prefix, this.coreReader.NamespaceURI, this.coreReader.Depth);
				validatingReaderNodeData.SetLineInfo(this.lineInfo);
				this.RecordAttributes();
			}
		}

		// Token: 0x06001164 RID: 4452 RVA: 0x00066AC5 File Offset: 0x00064CC5
		internal void Reset(XmlReader reader)
		{
			this.coreReader = reader;
			this.Init();
		}

		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06001165 RID: 4453 RVA: 0x00066AD4 File Offset: 0x00064CD4
		public override XmlReaderSettings Settings
		{
			get
			{
				return this.coreReader.Settings;
			}
		}

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06001166 RID: 4454 RVA: 0x00066AE1 File Offset: 0x00064CE1
		public override XmlNodeType NodeType
		{
			get
			{
				return this.cachedNode.NodeType;
			}
		}

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06001167 RID: 4455 RVA: 0x00066AEE File Offset: 0x00064CEE
		public override string Name
		{
			get
			{
				return this.cachedNode.GetAtomizedNameWPrefix(this.coreReaderNameTable);
			}
		}

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x06001168 RID: 4456 RVA: 0x00066B01 File Offset: 0x00064D01
		public override string LocalName
		{
			get
			{
				return this.cachedNode.LocalName;
			}
		}

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x06001169 RID: 4457 RVA: 0x00066B0E File Offset: 0x00064D0E
		public override string NamespaceURI
		{
			get
			{
				return this.cachedNode.Namespace;
			}
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x0600116A RID: 4458 RVA: 0x00066B1B File Offset: 0x00064D1B
		public override string Prefix
		{
			get
			{
				return this.cachedNode.Prefix;
			}
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x0600116B RID: 4459 RVA: 0x00066B28 File Offset: 0x00064D28
		public override bool HasValue
		{
			get
			{
				return XmlReader.HasValueInternal(this.cachedNode.NodeType);
			}
		}

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x0600116C RID: 4460 RVA: 0x00066B3A File Offset: 0x00064D3A
		public override string Value
		{
			get
			{
				if (!this.returnOriginalStringValues)
				{
					return this.cachedNode.RawValue;
				}
				return this.cachedNode.OriginalStringValue;
			}
		}

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x0600116D RID: 4461 RVA: 0x00066B5B File Offset: 0x00064D5B
		public override int Depth
		{
			get
			{
				return this.cachedNode.Depth;
			}
		}

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x0600116E RID: 4462 RVA: 0x00066B68 File Offset: 0x00064D68
		public override string BaseURI
		{
			get
			{
				return this.coreReader.BaseURI;
			}
		}

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x0600116F RID: 4463 RVA: 0x000020CD File Offset: 0x000002CD
		public override bool IsEmptyElement
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06001170 RID: 4464 RVA: 0x000020CD File Offset: 0x000002CD
		public override bool IsDefault
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06001171 RID: 4465 RVA: 0x00066B75 File Offset: 0x00064D75
		public override char QuoteChar
		{
			get
			{
				return this.coreReader.QuoteChar;
			}
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06001172 RID: 4466 RVA: 0x00066B82 File Offset: 0x00064D82
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.coreReader.XmlSpace;
			}
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06001173 RID: 4467 RVA: 0x00066B8F File Offset: 0x00064D8F
		public override string XmlLang
		{
			get
			{
				return this.coreReader.XmlLang;
			}
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06001174 RID: 4468 RVA: 0x00066B9C File Offset: 0x00064D9C
		public override int AttributeCount
		{
			get
			{
				return this.attributeCount;
			}
		}

		// Token: 0x06001175 RID: 4469 RVA: 0x00066BA4 File Offset: 0x00064DA4
		public override string GetAttribute(string name)
		{
			int num;
			if (name.IndexOf(':') == -1)
			{
				num = this.GetAttributeIndexWithoutPrefix(name);
			}
			else
			{
				num = this.GetAttributeIndexWithPrefix(name);
			}
			if (num < 0)
			{
				return null;
			}
			return this.attributeEvents[num].RawValue;
		}

		// Token: 0x06001176 RID: 4470 RVA: 0x00066BE4 File Offset: 0x00064DE4
		public override string GetAttribute(string name, string namespaceURI)
		{
			namespaceURI = ((namespaceURI == null) ? string.Empty : this.coreReaderNameTable.Get(namespaceURI));
			name = this.coreReaderNameTable.Get(name);
			for (int i = 0; i < this.attributeCount; i++)
			{
				ValidatingReaderNodeData validatingReaderNodeData = this.attributeEvents[i];
				if (Ref.Equal(validatingReaderNodeData.LocalName, name) && Ref.Equal(validatingReaderNodeData.Namespace, namespaceURI))
				{
					return validatingReaderNodeData.RawValue;
				}
			}
			return null;
		}

		// Token: 0x06001177 RID: 4471 RVA: 0x00066C55 File Offset: 0x00064E55
		public override string GetAttribute(int i)
		{
			if (i < 0 || i >= this.attributeCount)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			return this.attributeEvents[i].RawValue;
		}

		// Token: 0x170002FF RID: 767
		public override string this[int i]
		{
			get
			{
				return this.GetAttribute(i);
			}
		}

		// Token: 0x17000300 RID: 768
		public override string this[string name]
		{
			get
			{
				return this.GetAttribute(name);
			}
		}

		// Token: 0x17000301 RID: 769
		public override string this[string name, string namespaceURI]
		{
			get
			{
				return this.GetAttribute(name, namespaceURI);
			}
		}

		// Token: 0x0600117B RID: 4475 RVA: 0x00066C7C File Offset: 0x00064E7C
		public override bool MoveToAttribute(string name)
		{
			int num;
			if (name.IndexOf(':') == -1)
			{
				num = this.GetAttributeIndexWithoutPrefix(name);
			}
			else
			{
				num = this.GetAttributeIndexWithPrefix(name);
			}
			if (num >= 0)
			{
				this.currentAttrIndex = num;
				this.cachedNode = this.attributeEvents[num];
				return true;
			}
			return false;
		}

		// Token: 0x0600117C RID: 4476 RVA: 0x00066CC4 File Offset: 0x00064EC4
		public override bool MoveToAttribute(string name, string ns)
		{
			ns = ((ns == null) ? string.Empty : this.coreReaderNameTable.Get(ns));
			name = this.coreReaderNameTable.Get(name);
			for (int i = 0; i < this.attributeCount; i++)
			{
				ValidatingReaderNodeData validatingReaderNodeData = this.attributeEvents[i];
				if (Ref.Equal(validatingReaderNodeData.LocalName, name) && Ref.Equal(validatingReaderNodeData.Namespace, ns))
				{
					this.currentAttrIndex = i;
					this.cachedNode = this.attributeEvents[i];
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600117D RID: 4477 RVA: 0x00066D45 File Offset: 0x00064F45
		public override void MoveToAttribute(int i)
		{
			if (i < 0 || i >= this.attributeCount)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			this.currentAttrIndex = i;
			this.cachedNode = this.attributeEvents[i];
		}

		// Token: 0x0600117E RID: 4478 RVA: 0x00066D74 File Offset: 0x00064F74
		public override bool MoveToFirstAttribute()
		{
			if (this.attributeCount == 0)
			{
				return false;
			}
			this.currentAttrIndex = 0;
			this.cachedNode = this.attributeEvents[0];
			return true;
		}

		// Token: 0x0600117F RID: 4479 RVA: 0x00066D98 File Offset: 0x00064F98
		public override bool MoveToNextAttribute()
		{
			if (this.currentAttrIndex + 1 < this.attributeCount)
			{
				ValidatingReaderNodeData[] array = this.attributeEvents;
				int num = this.currentAttrIndex + 1;
				this.currentAttrIndex = num;
				this.cachedNode = array[num];
				return true;
			}
			return false;
		}

		// Token: 0x06001180 RID: 4480 RVA: 0x00066DD6 File Offset: 0x00064FD6
		public override bool MoveToElement()
		{
			if (this.cacheState != XsdCachingReader.CachingReaderState.Replay || this.cachedNode.NodeType != XmlNodeType.Attribute)
			{
				return false;
			}
			this.currentContentIndex = 0;
			this.currentAttrIndex = -1;
			this.Read();
			return true;
		}

		// Token: 0x06001181 RID: 4481 RVA: 0x00066E08 File Offset: 0x00065008
		public override bool Read()
		{
			switch (this.cacheState)
			{
			case XsdCachingReader.CachingReaderState.Init:
				this.cacheState = XsdCachingReader.CachingReaderState.Record;
				break;
			case XsdCachingReader.CachingReaderState.Record:
				break;
			case XsdCachingReader.CachingReaderState.Replay:
				if (this.currentContentIndex >= this.contentIndex)
				{
					this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
					this.cacheHandler(this);
					return (this.coreReader.NodeType == XmlNodeType.Element && !this.readAhead) || this.coreReader.Read();
				}
				this.cachedNode = this.contentEvents[this.currentContentIndex];
				if (this.currentContentIndex > 0)
				{
					this.ClearAttributesInfo();
				}
				this.currentContentIndex++;
				return true;
			default:
				return false;
			}
			ValidatingReaderNodeData validatingReaderNodeData = null;
			if (this.coreReader.Read())
			{
				switch (this.coreReader.NodeType)
				{
				case XmlNodeType.Element:
					this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
					return false;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.ProcessingInstruction:
				case XmlNodeType.Comment:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					validatingReaderNodeData = this.AddContent(this.coreReader.NodeType);
					validatingReaderNodeData.SetItemData(this.coreReader.Value);
					validatingReaderNodeData.SetLineInfo(this.lineInfo);
					validatingReaderNodeData.Depth = this.coreReader.Depth;
					break;
				case XmlNodeType.EndElement:
					validatingReaderNodeData = this.AddContent(this.coreReader.NodeType);
					validatingReaderNodeData.SetItemData(this.coreReader.LocalName, this.coreReader.Prefix, this.coreReader.NamespaceURI, this.coreReader.Depth);
					validatingReaderNodeData.SetLineInfo(this.lineInfo);
					break;
				}
				this.cachedNode = validatingReaderNodeData;
				return true;
			}
			this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
			return false;
		}

		// Token: 0x06001182 RID: 4482 RVA: 0x00066FC8 File Offset: 0x000651C8
		internal ValidatingReaderNodeData RecordTextNode(string textValue, string originalStringValue, int depth, int lineNo, int linePos)
		{
			ValidatingReaderNodeData validatingReaderNodeData = this.AddContent(XmlNodeType.Text);
			validatingReaderNodeData.SetItemData(textValue, originalStringValue);
			validatingReaderNodeData.SetLineInfo(lineNo, linePos);
			validatingReaderNodeData.Depth = depth;
			return validatingReaderNodeData;
		}

		// Token: 0x06001183 RID: 4483 RVA: 0x00066FEC File Offset: 0x000651EC
		internal void SwitchTextNodeAndEndElement(string textValue, string originalStringValue)
		{
			ValidatingReaderNodeData validatingReaderNodeData = this.RecordTextNode(textValue, originalStringValue, this.coreReader.Depth + 1, 0, 0);
			int num = this.contentIndex - 2;
			ValidatingReaderNodeData validatingReaderNodeData2 = this.contentEvents[num];
			this.contentEvents[num] = validatingReaderNodeData;
			this.contentEvents[this.contentIndex - 1] = validatingReaderNodeData2;
		}

		// Token: 0x06001184 RID: 4484 RVA: 0x0006703C File Offset: 0x0006523C
		internal void RecordEndElementNode()
		{
			ValidatingReaderNodeData validatingReaderNodeData = this.AddContent(XmlNodeType.EndElement);
			validatingReaderNodeData.SetItemData(this.coreReader.LocalName, this.coreReader.Prefix, this.coreReader.NamespaceURI, this.coreReader.Depth);
			validatingReaderNodeData.SetLineInfo(this.coreReader as IXmlLineInfo);
			if (this.coreReader.IsEmptyElement)
			{
				this.readAhead = true;
			}
		}

		// Token: 0x06001185 RID: 4485 RVA: 0x000670A7 File Offset: 0x000652A7
		internal string ReadOriginalContentAsString()
		{
			this.returnOriginalStringValues = true;
			string result = base.InternalReadContentAsString();
			this.returnOriginalStringValues = false;
			return result;
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06001186 RID: 4486 RVA: 0x000670BD File Offset: 0x000652BD
		public override bool EOF
		{
			get
			{
				return this.cacheState == XsdCachingReader.CachingReaderState.ReaderClosed && this.coreReader.EOF;
			}
		}

		// Token: 0x06001187 RID: 4487 RVA: 0x000670D5 File Offset: 0x000652D5
		public override void Close()
		{
			this.coreReader.Close();
			this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
		}

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06001188 RID: 4488 RVA: 0x000670E9 File Offset: 0x000652E9
		public override ReadState ReadState
		{
			get
			{
				return this.coreReader.ReadState;
			}
		}

		// Token: 0x06001189 RID: 4489 RVA: 0x000670F8 File Offset: 0x000652F8
		public override void Skip()
		{
			XmlNodeType nodeType = this.cachedNode.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.Attribute)
				{
					this.Read();
					return;
				}
				this.MoveToElement();
			}
			if (this.coreReader.NodeType != XmlNodeType.EndElement && !this.readAhead)
			{
				int num = this.coreReader.Depth - 1;
				while (this.coreReader.Read() && this.coreReader.Depth > num)
				{
				}
			}
			this.coreReader.Read();
			this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
			this.cacheHandler(this);
		}

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x0600118A RID: 4490 RVA: 0x0006718B File Offset: 0x0006538B
		public override XmlNameTable NameTable
		{
			get
			{
				return this.coreReaderNameTable;
			}
		}

		// Token: 0x0600118B RID: 4491 RVA: 0x00067193 File Offset: 0x00065393
		public override string LookupNamespace(string prefix)
		{
			return this.coreReader.LookupNamespace(prefix);
		}

		// Token: 0x0600118C RID: 4492 RVA: 0x00007AE0 File Offset: 0x00005CE0
		public override void ResolveEntity()
		{
			throw new InvalidOperationException();
		}

		// Token: 0x0600118D RID: 4493 RVA: 0x000671A1 File Offset: 0x000653A1
		public override bool ReadAttributeValue()
		{
			if (this.cachedNode.NodeType != XmlNodeType.Attribute)
			{
				return false;
			}
			this.cachedNode = this.CreateDummyTextNode(this.cachedNode.RawValue, this.cachedNode.Depth + 1);
			return true;
		}

		// Token: 0x0600118E RID: 4494 RVA: 0x000033DE File Offset: 0x000015DE
		bool IXmlLineInfo.HasLineInfo()
		{
			return true;
		}

		// Token: 0x17000305 RID: 773
		// (get) Token: 0x0600118F RID: 4495 RVA: 0x000671D8 File Offset: 0x000653D8
		int IXmlLineInfo.LineNumber
		{
			get
			{
				return this.cachedNode.LineNumber;
			}
		}

		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06001190 RID: 4496 RVA: 0x000671E5 File Offset: 0x000653E5
		int IXmlLineInfo.LinePosition
		{
			get
			{
				return this.cachedNode.LinePosition;
			}
		}

		// Token: 0x06001191 RID: 4497 RVA: 0x000671F2 File Offset: 0x000653F2
		internal void SetToReplayMode()
		{
			this.cacheState = XsdCachingReader.CachingReaderState.Replay;
			this.currentContentIndex = 0;
			this.currentAttrIndex = -1;
			this.Read();
		}

		// Token: 0x06001192 RID: 4498 RVA: 0x00067210 File Offset: 0x00065410
		internal XmlReader GetCoreReader()
		{
			return this.coreReader;
		}

		// Token: 0x06001193 RID: 4499 RVA: 0x00067218 File Offset: 0x00065418
		internal IXmlLineInfo GetLineInfo()
		{
			return this.lineInfo;
		}

		// Token: 0x06001194 RID: 4500 RVA: 0x00067220 File Offset: 0x00065420
		private void ClearAttributesInfo()
		{
			this.attributeCount = 0;
			this.currentAttrIndex = -1;
		}

		// Token: 0x06001195 RID: 4501 RVA: 0x00067230 File Offset: 0x00065430
		private ValidatingReaderNodeData AddAttribute(int attIndex)
		{
			ValidatingReaderNodeData validatingReaderNodeData = this.attributeEvents[attIndex];
			if (validatingReaderNodeData != null)
			{
				validatingReaderNodeData.Clear(XmlNodeType.Attribute);
				return validatingReaderNodeData;
			}
			if (attIndex >= this.attributeEvents.Length - 1)
			{
				ValidatingReaderNodeData[] destinationArray = new ValidatingReaderNodeData[this.attributeEvents.Length * 2];
				Array.Copy(this.attributeEvents, 0, destinationArray, 0, this.attributeEvents.Length);
				this.attributeEvents = destinationArray;
			}
			validatingReaderNodeData = this.attributeEvents[attIndex];
			if (validatingReaderNodeData == null)
			{
				validatingReaderNodeData = new ValidatingReaderNodeData(XmlNodeType.Attribute);
				this.attributeEvents[attIndex] = validatingReaderNodeData;
			}
			return validatingReaderNodeData;
		}

		// Token: 0x06001196 RID: 4502 RVA: 0x000672AC File Offset: 0x000654AC
		private ValidatingReaderNodeData AddContent(XmlNodeType nodeType)
		{
			ValidatingReaderNodeData validatingReaderNodeData = this.contentEvents[this.contentIndex];
			if (validatingReaderNodeData != null)
			{
				validatingReaderNodeData.Clear(nodeType);
				this.contentIndex++;
				return validatingReaderNodeData;
			}
			if (this.contentIndex >= this.contentEvents.Length - 1)
			{
				ValidatingReaderNodeData[] destinationArray = new ValidatingReaderNodeData[this.contentEvents.Length * 2];
				Array.Copy(this.contentEvents, 0, destinationArray, 0, this.contentEvents.Length);
				this.contentEvents = destinationArray;
			}
			validatingReaderNodeData = this.contentEvents[this.contentIndex];
			if (validatingReaderNodeData == null)
			{
				validatingReaderNodeData = new ValidatingReaderNodeData(nodeType);
				this.contentEvents[this.contentIndex] = validatingReaderNodeData;
			}
			this.contentIndex++;
			return validatingReaderNodeData;
		}

		// Token: 0x06001197 RID: 4503 RVA: 0x00067358 File Offset: 0x00065558
		private void RecordAttributes()
		{
			this.attributeCount = this.coreReader.AttributeCount;
			if (this.coreReader.MoveToFirstAttribute())
			{
				int num = 0;
				do
				{
					ValidatingReaderNodeData validatingReaderNodeData = this.AddAttribute(num);
					validatingReaderNodeData.SetItemData(this.coreReader.LocalName, this.coreReader.Prefix, this.coreReader.NamespaceURI, this.coreReader.Depth);
					validatingReaderNodeData.SetLineInfo(this.lineInfo);
					validatingReaderNodeData.RawValue = this.coreReader.Value;
					num++;
				}
				while (this.coreReader.MoveToNextAttribute());
				this.coreReader.MoveToElement();
			}
		}

		// Token: 0x06001198 RID: 4504 RVA: 0x000673F8 File Offset: 0x000655F8
		private int GetAttributeIndexWithoutPrefix(string name)
		{
			name = this.coreReaderNameTable.Get(name);
			if (name == null)
			{
				return -1;
			}
			for (int i = 0; i < this.attributeCount; i++)
			{
				ValidatingReaderNodeData validatingReaderNodeData = this.attributeEvents[i];
				if (Ref.Equal(validatingReaderNodeData.LocalName, name) && validatingReaderNodeData.Prefix.Length == 0)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06001199 RID: 4505 RVA: 0x00067450 File Offset: 0x00065650
		private int GetAttributeIndexWithPrefix(string name)
		{
			name = this.coreReaderNameTable.Get(name);
			if (name == null)
			{
				return -1;
			}
			for (int i = 0; i < this.attributeCount; i++)
			{
				if (Ref.Equal(this.attributeEvents[i].GetAtomizedNameWPrefix(this.coreReaderNameTable), name))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x0600119A RID: 4506 RVA: 0x0006749F File Offset: 0x0006569F
		private ValidatingReaderNodeData CreateDummyTextNode(string attributeValue, int depth)
		{
			if (this.textNode == null)
			{
				this.textNode = new ValidatingReaderNodeData(XmlNodeType.Text);
			}
			this.textNode.Depth = depth;
			this.textNode.RawValue = attributeValue;
			return this.textNode;
		}

		// Token: 0x0600119B RID: 4507 RVA: 0x000674D3 File Offset: 0x000656D3
		public override Task<string> GetValueAsync()
		{
			if (this.returnOriginalStringValues)
			{
				return Task.FromResult<string>(this.cachedNode.OriginalStringValue);
			}
			return Task.FromResult<string>(this.cachedNode.RawValue);
		}

		// Token: 0x0600119C RID: 4508 RVA: 0x00067500 File Offset: 0x00065700
		public override async Task<bool> ReadAsync()
		{
			switch (this.cacheState)
			{
			case XsdCachingReader.CachingReaderState.Init:
				this.cacheState = XsdCachingReader.CachingReaderState.Record;
				break;
			case XsdCachingReader.CachingReaderState.Record:
				break;
			case XsdCachingReader.CachingReaderState.Replay:
				if (this.currentContentIndex < this.contentIndex)
				{
					this.cachedNode = this.contentEvents[this.currentContentIndex];
					if (this.currentContentIndex > 0)
					{
						this.ClearAttributesInfo();
					}
					this.currentContentIndex++;
					return true;
				}
				this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
				this.cacheHandler(this);
				if (this.coreReader.NodeType != XmlNodeType.Element || this.readAhead)
				{
					return await this.coreReader.ReadAsync().ConfigureAwait(false);
				}
				return true;
			default:
				return false;
			}
			ValidatingReaderNodeData recordedNode = null;
			ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
			if (!configuredTaskAwaiter.IsCompleted)
			{
				await configuredTaskAwaiter;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
				configuredTaskAwaiter = configuredTaskAwaiter2;
				configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
			}
			bool result;
			if (configuredTaskAwaiter.GetResult())
			{
				switch (this.coreReader.NodeType)
				{
				case XmlNodeType.Element:
					this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
					return false;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.ProcessingInstruction:
				case XmlNodeType.Comment:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
				{
					recordedNode = this.AddContent(this.coreReader.NodeType);
					ValidatingReaderNodeData validatingReaderNodeData = recordedNode;
					validatingReaderNodeData.SetItemData(await this.coreReader.GetValueAsync().ConfigureAwait(false));
					validatingReaderNodeData = null;
					recordedNode.SetLineInfo(this.lineInfo);
					recordedNode.Depth = this.coreReader.Depth;
					break;
				}
				case XmlNodeType.EndElement:
					recordedNode = this.AddContent(this.coreReader.NodeType);
					recordedNode.SetItemData(this.coreReader.LocalName, this.coreReader.Prefix, this.coreReader.NamespaceURI, this.coreReader.Depth);
					recordedNode.SetLineInfo(this.lineInfo);
					break;
				}
				this.cachedNode = recordedNode;
				result = true;
			}
			else
			{
				this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
				result = false;
			}
			return result;
		}

		// Token: 0x0600119D RID: 4509 RVA: 0x00067548 File Offset: 0x00065748
		public override async Task SkipAsync()
		{
			XmlNodeType nodeType = this.cachedNode.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.Attribute)
				{
					await this.ReadAsync().ConfigureAwait(false);
					return;
				}
				this.MoveToElement();
			}
			if (this.coreReader.NodeType != XmlNodeType.EndElement && !this.readAhead)
			{
				int startDepth = this.coreReader.Depth - 1;
				ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter;
				do
				{
					configuredTaskAwaiter = this.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
					}
				}
				while (configuredTaskAwaiter.GetResult() && this.coreReader.Depth > startDepth);
			}
			await this.coreReader.ReadAsync().ConfigureAwait(false);
			this.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
			this.cacheHandler(this);
		}

		// Token: 0x0600119E RID: 4510 RVA: 0x0006758D File Offset: 0x0006578D
		internal Task SetToReplayModeAsync()
		{
			this.cacheState = XsdCachingReader.CachingReaderState.Replay;
			this.currentContentIndex = 0;
			this.currentAttrIndex = -1;
			return this.ReadAsync();
		}

		// Token: 0x04000C4F RID: 3151
		private XmlReader coreReader;

		// Token: 0x04000C50 RID: 3152
		private XmlNameTable coreReaderNameTable;

		// Token: 0x04000C51 RID: 3153
		private ValidatingReaderNodeData[] contentEvents;

		// Token: 0x04000C52 RID: 3154
		private ValidatingReaderNodeData[] attributeEvents;

		// Token: 0x04000C53 RID: 3155
		private ValidatingReaderNodeData cachedNode;

		// Token: 0x04000C54 RID: 3156
		private XsdCachingReader.CachingReaderState cacheState;

		// Token: 0x04000C55 RID: 3157
		private int contentIndex;

		// Token: 0x04000C56 RID: 3158
		private int attributeCount;

		// Token: 0x04000C57 RID: 3159
		private bool returnOriginalStringValues;

		// Token: 0x04000C58 RID: 3160
		private CachingEventHandler cacheHandler;

		// Token: 0x04000C59 RID: 3161
		private int currentAttrIndex;

		// Token: 0x04000C5A RID: 3162
		private int currentContentIndex;

		// Token: 0x04000C5B RID: 3163
		private bool readAhead;

		// Token: 0x04000C5C RID: 3164
		private IXmlLineInfo lineInfo;

		// Token: 0x04000C5D RID: 3165
		private ValidatingReaderNodeData textNode;

		// Token: 0x04000C5E RID: 3166
		private const int InitialAttributeCount = 8;

		// Token: 0x04000C5F RID: 3167
		private const int InitialContentCount = 4;

		// Token: 0x020001EE RID: 494
		private enum CachingReaderState
		{
			// Token: 0x04000C61 RID: 3169
			None,
			// Token: 0x04000C62 RID: 3170
			Init,
			// Token: 0x04000C63 RID: 3171
			Record,
			// Token: 0x04000C64 RID: 3172
			Replay,
			// Token: 0x04000C65 RID: 3173
			ReaderClosed,
			// Token: 0x04000C66 RID: 3174
			Error
		}

		// Token: 0x020001EF RID: 495
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsync>d__100 : IAsyncStateMachine
		{
			// Token: 0x0600119F RID: 4511 RVA: 0x000675AC File Offset: 0x000657AC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdCachingReader xsdCachingReader = this;
				bool result;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
					{
						ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter5;
						configuredTaskAwaiter4 = configuredTaskAwaiter5;
						configuredTaskAwaiter5 = default(ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_212;
					}
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_317;
					default:
						switch (xsdCachingReader.cacheState)
						{
						case XsdCachingReader.CachingReaderState.Init:
							xsdCachingReader.cacheState = XsdCachingReader.CachingReaderState.Record;
							break;
						case XsdCachingReader.CachingReaderState.Record:
							break;
						case XsdCachingReader.CachingReaderState.Replay:
							if (xsdCachingReader.currentContentIndex < xsdCachingReader.contentIndex)
							{
								xsdCachingReader.cachedNode = xsdCachingReader.contentEvents[xsdCachingReader.currentContentIndex];
								if (xsdCachingReader.currentContentIndex > 0)
								{
									xsdCachingReader.ClearAttributesInfo();
								}
								xsdCachingReader.currentContentIndex++;
								result = true;
								goto IL_376;
							}
							xsdCachingReader.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
							xsdCachingReader.cacheHandler(xsdCachingReader);
							if (xsdCachingReader.coreReader.NodeType == XmlNodeType.Element && !xsdCachingReader.readAhead)
							{
								result = true;
								goto IL_376;
							}
							configuredTaskAwaiter3 = xsdCachingReader.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter3.IsCompleted)
							{
								num2 = 2;
								configuredTaskAwaiter2 = configuredTaskAwaiter3;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdCachingReader.<ReadAsync>d__100>(ref configuredTaskAwaiter3, ref this);
								return;
							}
							goto IL_317;
						default:
							result = false;
							goto IL_376;
						}
						recordedNode = null;
						configuredTaskAwaiter3 = xsdCachingReader.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdCachingReader.<ReadAsync>d__100>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					if (!configuredTaskAwaiter3.GetResult())
					{
						xsdCachingReader.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
						result = false;
						goto IL_376;
					}
					switch (xsdCachingReader.coreReader.NodeType)
					{
					case XmlNodeType.Element:
						xsdCachingReader.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
						result = false;
						goto IL_376;
					case XmlNodeType.Attribute:
					case XmlNodeType.EntityReference:
					case XmlNodeType.Entity:
					case XmlNodeType.Document:
					case XmlNodeType.DocumentType:
					case XmlNodeType.DocumentFragment:
					case XmlNodeType.Notation:
						goto IL_256;
					case XmlNodeType.Text:
					case XmlNodeType.CDATA:
					case XmlNodeType.ProcessingInstruction:
					case XmlNodeType.Comment:
					case XmlNodeType.Whitespace:
					case XmlNodeType.SignificantWhitespace:
						recordedNode = xsdCachingReader.AddContent(xsdCachingReader.coreReader.NodeType);
						validatingReaderNodeData = recordedNode;
						configuredTaskAwaiter4 = xsdCachingReader.coreReader.GetValueAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter4.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter configuredTaskAwaiter5 = configuredTaskAwaiter4;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter, XsdCachingReader.<ReadAsync>d__100>(ref configuredTaskAwaiter4, ref this);
							return;
						}
						break;
					case XmlNodeType.EndElement:
						recordedNode = xsdCachingReader.AddContent(xsdCachingReader.coreReader.NodeType);
						recordedNode.SetItemData(xsdCachingReader.coreReader.LocalName, xsdCachingReader.coreReader.Prefix, xsdCachingReader.coreReader.NamespaceURI, xsdCachingReader.coreReader.Depth);
						recordedNode.SetLineInfo(xsdCachingReader.lineInfo);
						goto IL_256;
					default:
						goto IL_256;
					}
					IL_212:
					string result2 = configuredTaskAwaiter4.GetResult();
					validatingReaderNodeData.SetItemData(result2);
					validatingReaderNodeData = null;
					recordedNode.SetLineInfo(xsdCachingReader.lineInfo);
					recordedNode.Depth = xsdCachingReader.coreReader.Depth;
					IL_256:
					xsdCachingReader.cachedNode = recordedNode;
					result = true;
					goto IL_376;
					IL_317:
					result = configuredTaskAwaiter3.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_376:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060011A0 RID: 4512 RVA: 0x00067960 File Offset: 0x00065B60
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000C67 RID: 3175
			public int <>1__state;

			// Token: 0x04000C68 RID: 3176
			public AsyncTaskMethodBuilder<bool> <>t__builder;

			// Token: 0x04000C69 RID: 3177
			public XsdCachingReader <>4__this;

			// Token: 0x04000C6A RID: 3178
			private ValidatingReaderNodeData <recordedNode>5__1;

			// Token: 0x04000C6B RID: 3179
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x04000C6C RID: 3180
			private ValidatingReaderNodeData <>7__wrap1;

			// Token: 0x04000C6D RID: 3181
			private ConfiguredTaskAwaitable<string>.ConfiguredTaskAwaiter <>u__2;
		}

		// Token: 0x020001F0 RID: 496
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <SkipAsync>d__101 : IAsyncStateMachine
		{
			// Token: 0x060011A1 RID: 4513 RVA: 0x00067970 File Offset: 0x00065B70
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XsdCachingReader xsdCachingReader = this;
				try
				{
					ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_CF;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_14E;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1D2;
					default:
					{
						XmlNodeType nodeType = xsdCachingReader.cachedNode.NodeType;
						if (nodeType != XmlNodeType.Element)
						{
							if (nodeType != XmlNodeType.Attribute)
							{
								configuredTaskAwaiter3 = xsdCachingReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num2 = 2;
									configuredTaskAwaiter2 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdCachingReader.<SkipAsync>d__101>(ref configuredTaskAwaiter3, ref this);
									return;
								}
								goto IL_1D2;
							}
							else
							{
								xsdCachingReader.MoveToElement();
							}
						}
						if (xsdCachingReader.coreReader.NodeType == XmlNodeType.EndElement || xsdCachingReader.readAhead)
						{
							goto IL_EB;
						}
						startDepth = xsdCachingReader.coreReader.Depth - 1;
						break;
					}
					}
					IL_6C:
					configuredTaskAwaiter3 = xsdCachingReader.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 0;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdCachingReader.<SkipAsync>d__101>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_CF:
					if (configuredTaskAwaiter3.GetResult() && xsdCachingReader.coreReader.Depth > startDepth)
					{
						goto IL_6C;
					}
					IL_EB:
					configuredTaskAwaiter3 = xsdCachingReader.coreReader.ReadAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter, XsdCachingReader.<SkipAsync>d__101>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					IL_14E:
					configuredTaskAwaiter3.GetResult();
					xsdCachingReader.cacheState = XsdCachingReader.CachingReaderState.ReaderClosed;
					xsdCachingReader.cacheHandler(xsdCachingReader);
					goto IL_1DA;
					IL_1D2:
					configuredTaskAwaiter3.GetResult();
					IL_1DA:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x060011A2 RID: 4514 RVA: 0x00067BA4 File Offset: 0x00065DA4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000C6E RID: 3182
			public int <>1__state;

			// Token: 0x04000C6F RID: 3183
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000C70 RID: 3184
			public XsdCachingReader <>4__this;

			// Token: 0x04000C71 RID: 3185
			private int <startDepth>5__1;

			// Token: 0x04000C72 RID: 3186
			private ConfiguredTaskAwaitable<bool>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
