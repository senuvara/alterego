﻿using System;
using System.Collections;

namespace System.Xml
{
	// Token: 0x0200021A RID: 538
	internal class XmlNodeListEnumerator : IEnumerator
	{
		// Token: 0x06001336 RID: 4918 RVA: 0x000718AE File Offset: 0x0006FAAE
		public XmlNodeListEnumerator(XPathNodeList list)
		{
			this.list = list;
			this.index = -1;
			this.valid = false;
		}

		// Token: 0x06001337 RID: 4919 RVA: 0x000718CB File Offset: 0x0006FACB
		public void Reset()
		{
			this.index = -1;
		}

		// Token: 0x06001338 RID: 4920 RVA: 0x000718D4 File Offset: 0x0006FAD4
		public bool MoveNext()
		{
			this.index++;
			if (this.list.ReadUntil(this.index + 1) - 1 < this.index)
			{
				return false;
			}
			this.valid = (this.list[this.index] != null);
			return this.valid;
		}

		// Token: 0x1700034E RID: 846
		// (get) Token: 0x06001339 RID: 4921 RVA: 0x0007192E File Offset: 0x0006FB2E
		public object Current
		{
			get
			{
				if (this.valid)
				{
					return this.list[this.index];
				}
				return null;
			}
		}

		// Token: 0x04000D78 RID: 3448
		private XPathNodeList list;

		// Token: 0x04000D79 RID: 3449
		private int index;

		// Token: 0x04000D7A RID: 3450
		private bool valid;
	}
}
