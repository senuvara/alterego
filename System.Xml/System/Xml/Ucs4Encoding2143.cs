﻿using System;

namespace System.Xml
{
	// Token: 0x0200029B RID: 667
	internal class Ucs4Encoding2143 : Ucs4Encoding
	{
		// Token: 0x060018A2 RID: 6306 RVA: 0x0008E888 File Offset: 0x0008CA88
		public Ucs4Encoding2143()
		{
			this.ucs4Decoder = new Ucs4Decoder2143();
		}

		// Token: 0x170004A3 RID: 1187
		// (get) Token: 0x060018A3 RID: 6307 RVA: 0x0008E89B File Offset: 0x0008CA9B
		public override string EncodingName
		{
			get
			{
				return "ucs-4 (order 2143)";
			}
		}

		// Token: 0x060018A4 RID: 6308 RVA: 0x0008E8A2 File Offset: 0x0008CAA2
		public override byte[] GetPreamble()
		{
			return new byte[]
			{
				0,
				0,
				byte.MaxValue,
				254
			};
		}
	}
}
