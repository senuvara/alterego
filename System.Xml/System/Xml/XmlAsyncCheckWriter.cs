﻿using System;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x020000CD RID: 205
	internal class XmlAsyncCheckWriter : XmlWriter
	{
		// Token: 0x17000166 RID: 358
		// (get) Token: 0x060006DD RID: 1757 RVA: 0x0001C075 File Offset: 0x0001A275
		internal XmlWriter CoreWriter
		{
			get
			{
				return this.coreWriter;
			}
		}

		// Token: 0x060006DE RID: 1758 RVA: 0x0001C07D File Offset: 0x0001A27D
		public XmlAsyncCheckWriter(XmlWriter writer)
		{
			this.coreWriter = writer;
		}

		// Token: 0x060006DF RID: 1759 RVA: 0x0001C097 File Offset: 0x0001A297
		private void CheckAsync()
		{
			if (!this.lastTask.IsCompleted)
			{
				throw new InvalidOperationException(Res.GetString("An asynchronous operation is already in progress."));
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x060006E0 RID: 1760 RVA: 0x0001C0B8 File Offset: 0x0001A2B8
		public override XmlWriterSettings Settings
		{
			get
			{
				XmlWriterSettings xmlWriterSettings = this.coreWriter.Settings;
				if (xmlWriterSettings != null)
				{
					xmlWriterSettings = xmlWriterSettings.Clone();
				}
				else
				{
					xmlWriterSettings = new XmlWriterSettings();
				}
				xmlWriterSettings.Async = true;
				xmlWriterSettings.ReadOnly = true;
				return xmlWriterSettings;
			}
		}

		// Token: 0x060006E1 RID: 1761 RVA: 0x0001C0F2 File Offset: 0x0001A2F2
		public override void WriteStartDocument()
		{
			this.CheckAsync();
			this.coreWriter.WriteStartDocument();
		}

		// Token: 0x060006E2 RID: 1762 RVA: 0x0001C105 File Offset: 0x0001A305
		public override void WriteStartDocument(bool standalone)
		{
			this.CheckAsync();
			this.coreWriter.WriteStartDocument(standalone);
		}

		// Token: 0x060006E3 RID: 1763 RVA: 0x0001C119 File Offset: 0x0001A319
		public override void WriteEndDocument()
		{
			this.CheckAsync();
			this.coreWriter.WriteEndDocument();
		}

		// Token: 0x060006E4 RID: 1764 RVA: 0x0001C12C File Offset: 0x0001A32C
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			this.CheckAsync();
			this.coreWriter.WriteDocType(name, pubid, sysid, subset);
		}

		// Token: 0x060006E5 RID: 1765 RVA: 0x0001C144 File Offset: 0x0001A344
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			this.CheckAsync();
			this.coreWriter.WriteStartElement(prefix, localName, ns);
		}

		// Token: 0x060006E6 RID: 1766 RVA: 0x0001C15A File Offset: 0x0001A35A
		public override void WriteEndElement()
		{
			this.CheckAsync();
			this.coreWriter.WriteEndElement();
		}

		// Token: 0x060006E7 RID: 1767 RVA: 0x0001C16D File Offset: 0x0001A36D
		public override void WriteFullEndElement()
		{
			this.CheckAsync();
			this.coreWriter.WriteFullEndElement();
		}

		// Token: 0x060006E8 RID: 1768 RVA: 0x0001C180 File Offset: 0x0001A380
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			this.CheckAsync();
			this.coreWriter.WriteStartAttribute(prefix, localName, ns);
		}

		// Token: 0x060006E9 RID: 1769 RVA: 0x0001C196 File Offset: 0x0001A396
		public override void WriteEndAttribute()
		{
			this.CheckAsync();
			this.coreWriter.WriteEndAttribute();
		}

		// Token: 0x060006EA RID: 1770 RVA: 0x0001C1A9 File Offset: 0x0001A3A9
		public override void WriteCData(string text)
		{
			this.CheckAsync();
			this.coreWriter.WriteCData(text);
		}

		// Token: 0x060006EB RID: 1771 RVA: 0x0001C1BD File Offset: 0x0001A3BD
		public override void WriteComment(string text)
		{
			this.CheckAsync();
			this.coreWriter.WriteComment(text);
		}

		// Token: 0x060006EC RID: 1772 RVA: 0x0001C1D1 File Offset: 0x0001A3D1
		public override void WriteProcessingInstruction(string name, string text)
		{
			this.CheckAsync();
			this.coreWriter.WriteProcessingInstruction(name, text);
		}

		// Token: 0x060006ED RID: 1773 RVA: 0x0001C1E6 File Offset: 0x0001A3E6
		public override void WriteEntityRef(string name)
		{
			this.CheckAsync();
			this.coreWriter.WriteEntityRef(name);
		}

		// Token: 0x060006EE RID: 1774 RVA: 0x0001C1FA File Offset: 0x0001A3FA
		public override void WriteCharEntity(char ch)
		{
			this.CheckAsync();
			this.coreWriter.WriteCharEntity(ch);
		}

		// Token: 0x060006EF RID: 1775 RVA: 0x0001C20E File Offset: 0x0001A40E
		public override void WriteWhitespace(string ws)
		{
			this.CheckAsync();
			this.coreWriter.WriteWhitespace(ws);
		}

		// Token: 0x060006F0 RID: 1776 RVA: 0x0001C222 File Offset: 0x0001A422
		public override void WriteString(string text)
		{
			this.CheckAsync();
			this.coreWriter.WriteString(text);
		}

		// Token: 0x060006F1 RID: 1777 RVA: 0x0001C236 File Offset: 0x0001A436
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			this.CheckAsync();
			this.coreWriter.WriteSurrogateCharEntity(lowChar, highChar);
		}

		// Token: 0x060006F2 RID: 1778 RVA: 0x0001C24B File Offset: 0x0001A44B
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.CheckAsync();
			this.coreWriter.WriteChars(buffer, index, count);
		}

		// Token: 0x060006F3 RID: 1779 RVA: 0x0001C261 File Offset: 0x0001A461
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.CheckAsync();
			this.coreWriter.WriteRaw(buffer, index, count);
		}

		// Token: 0x060006F4 RID: 1780 RVA: 0x0001C277 File Offset: 0x0001A477
		public override void WriteRaw(string data)
		{
			this.CheckAsync();
			this.coreWriter.WriteRaw(data);
		}

		// Token: 0x060006F5 RID: 1781 RVA: 0x0001C28B File Offset: 0x0001A48B
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			this.CheckAsync();
			this.coreWriter.WriteBase64(buffer, index, count);
		}

		// Token: 0x060006F6 RID: 1782 RVA: 0x0001C2A1 File Offset: 0x0001A4A1
		public override void WriteBinHex(byte[] buffer, int index, int count)
		{
			this.CheckAsync();
			this.coreWriter.WriteBinHex(buffer, index, count);
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x060006F7 RID: 1783 RVA: 0x0001C2B7 File Offset: 0x0001A4B7
		public override WriteState WriteState
		{
			get
			{
				this.CheckAsync();
				return this.coreWriter.WriteState;
			}
		}

		// Token: 0x060006F8 RID: 1784 RVA: 0x0001C2CA File Offset: 0x0001A4CA
		public override void Close()
		{
			this.CheckAsync();
			this.coreWriter.Close();
		}

		// Token: 0x060006F9 RID: 1785 RVA: 0x0001C2DD File Offset: 0x0001A4DD
		public override void Flush()
		{
			this.CheckAsync();
			this.coreWriter.Flush();
		}

		// Token: 0x060006FA RID: 1786 RVA: 0x0001C2F0 File Offset: 0x0001A4F0
		public override string LookupPrefix(string ns)
		{
			this.CheckAsync();
			return this.coreWriter.LookupPrefix(ns);
		}

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x060006FB RID: 1787 RVA: 0x0001C304 File Offset: 0x0001A504
		public override XmlSpace XmlSpace
		{
			get
			{
				this.CheckAsync();
				return this.coreWriter.XmlSpace;
			}
		}

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x060006FC RID: 1788 RVA: 0x0001C317 File Offset: 0x0001A517
		public override string XmlLang
		{
			get
			{
				this.CheckAsync();
				return this.coreWriter.XmlLang;
			}
		}

		// Token: 0x060006FD RID: 1789 RVA: 0x0001C32A File Offset: 0x0001A52A
		public override void WriteNmToken(string name)
		{
			this.CheckAsync();
			this.coreWriter.WriteNmToken(name);
		}

		// Token: 0x060006FE RID: 1790 RVA: 0x0001C33E File Offset: 0x0001A53E
		public override void WriteName(string name)
		{
			this.CheckAsync();
			this.coreWriter.WriteName(name);
		}

		// Token: 0x060006FF RID: 1791 RVA: 0x0001C352 File Offset: 0x0001A552
		public override void WriteQualifiedName(string localName, string ns)
		{
			this.CheckAsync();
			this.coreWriter.WriteQualifiedName(localName, ns);
		}

		// Token: 0x06000700 RID: 1792 RVA: 0x0001C367 File Offset: 0x0001A567
		public override void WriteValue(object value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000701 RID: 1793 RVA: 0x0001C37B File Offset: 0x0001A57B
		public override void WriteValue(string value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000702 RID: 1794 RVA: 0x0001C38F File Offset: 0x0001A58F
		public override void WriteValue(bool value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000703 RID: 1795 RVA: 0x0001C3A3 File Offset: 0x0001A5A3
		public override void WriteValue(DateTime value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000704 RID: 1796 RVA: 0x0001C3B7 File Offset: 0x0001A5B7
		public override void WriteValue(DateTimeOffset value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000705 RID: 1797 RVA: 0x0001C3CB File Offset: 0x0001A5CB
		public override void WriteValue(double value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000706 RID: 1798 RVA: 0x0001C3DF File Offset: 0x0001A5DF
		public override void WriteValue(float value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000707 RID: 1799 RVA: 0x0001C3F3 File Offset: 0x0001A5F3
		public override void WriteValue(decimal value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000708 RID: 1800 RVA: 0x0001C407 File Offset: 0x0001A607
		public override void WriteValue(int value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x06000709 RID: 1801 RVA: 0x0001C41B File Offset: 0x0001A61B
		public override void WriteValue(long value)
		{
			this.CheckAsync();
			this.coreWriter.WriteValue(value);
		}

		// Token: 0x0600070A RID: 1802 RVA: 0x0001C42F File Offset: 0x0001A62F
		public override void WriteAttributes(XmlReader reader, bool defattr)
		{
			this.CheckAsync();
			this.coreWriter.WriteAttributes(reader, defattr);
		}

		// Token: 0x0600070B RID: 1803 RVA: 0x0001C444 File Offset: 0x0001A644
		public override void WriteNode(XmlReader reader, bool defattr)
		{
			this.CheckAsync();
			this.coreWriter.WriteNode(reader, defattr);
		}

		// Token: 0x0600070C RID: 1804 RVA: 0x0001C459 File Offset: 0x0001A659
		public override void WriteNode(XPathNavigator navigator, bool defattr)
		{
			this.CheckAsync();
			this.coreWriter.WriteNode(navigator, defattr);
		}

		// Token: 0x0600070D RID: 1805 RVA: 0x0001C46E File Offset: 0x0001A66E
		protected override void Dispose(bool disposing)
		{
			this.CheckAsync();
			this.coreWriter.Dispose();
		}

		// Token: 0x0600070E RID: 1806 RVA: 0x0001C484 File Offset: 0x0001A684
		public override Task WriteStartDocumentAsync()
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteStartDocumentAsync();
			this.lastTask = result;
			return result;
		}

		// Token: 0x0600070F RID: 1807 RVA: 0x0001C4AC File Offset: 0x0001A6AC
		public override Task WriteStartDocumentAsync(bool standalone)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteStartDocumentAsync(standalone);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000710 RID: 1808 RVA: 0x0001C4D4 File Offset: 0x0001A6D4
		public override Task WriteEndDocumentAsync()
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteEndDocumentAsync();
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000711 RID: 1809 RVA: 0x0001C4FC File Offset: 0x0001A6FC
		public override Task WriteDocTypeAsync(string name, string pubid, string sysid, string subset)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteDocTypeAsync(name, pubid, sysid, subset);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000712 RID: 1810 RVA: 0x0001C528 File Offset: 0x0001A728
		public override Task WriteStartElementAsync(string prefix, string localName, string ns)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteStartElementAsync(prefix, localName, ns);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000713 RID: 1811 RVA: 0x0001C554 File Offset: 0x0001A754
		public override Task WriteEndElementAsync()
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteEndElementAsync();
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000714 RID: 1812 RVA: 0x0001C57C File Offset: 0x0001A77C
		public override Task WriteFullEndElementAsync()
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteFullEndElementAsync();
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000715 RID: 1813 RVA: 0x0001C5A4 File Offset: 0x0001A7A4
		protected internal override Task WriteStartAttributeAsync(string prefix, string localName, string ns)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteStartAttributeAsync(prefix, localName, ns);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000716 RID: 1814 RVA: 0x0001C5D0 File Offset: 0x0001A7D0
		protected internal override Task WriteEndAttributeAsync()
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteEndAttributeAsync();
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000717 RID: 1815 RVA: 0x0001C5F8 File Offset: 0x0001A7F8
		public override Task WriteCDataAsync(string text)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteCDataAsync(text);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000718 RID: 1816 RVA: 0x0001C620 File Offset: 0x0001A820
		public override Task WriteCommentAsync(string text)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteCommentAsync(text);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000719 RID: 1817 RVA: 0x0001C648 File Offset: 0x0001A848
		public override Task WriteProcessingInstructionAsync(string name, string text)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteProcessingInstructionAsync(name, text);
			this.lastTask = result;
			return result;
		}

		// Token: 0x0600071A RID: 1818 RVA: 0x0001C674 File Offset: 0x0001A874
		public override Task WriteEntityRefAsync(string name)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteEntityRefAsync(name);
			this.lastTask = result;
			return result;
		}

		// Token: 0x0600071B RID: 1819 RVA: 0x0001C69C File Offset: 0x0001A89C
		public override Task WriteCharEntityAsync(char ch)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteCharEntityAsync(ch);
			this.lastTask = result;
			return result;
		}

		// Token: 0x0600071C RID: 1820 RVA: 0x0001C6C4 File Offset: 0x0001A8C4
		public override Task WriteWhitespaceAsync(string ws)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteWhitespaceAsync(ws);
			this.lastTask = result;
			return result;
		}

		// Token: 0x0600071D RID: 1821 RVA: 0x0001C6EC File Offset: 0x0001A8EC
		public override Task WriteStringAsync(string text)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteStringAsync(text);
			this.lastTask = result;
			return result;
		}

		// Token: 0x0600071E RID: 1822 RVA: 0x0001C714 File Offset: 0x0001A914
		public override Task WriteSurrogateCharEntityAsync(char lowChar, char highChar)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteSurrogateCharEntityAsync(lowChar, highChar);
			this.lastTask = result;
			return result;
		}

		// Token: 0x0600071F RID: 1823 RVA: 0x0001C740 File Offset: 0x0001A940
		public override Task WriteCharsAsync(char[] buffer, int index, int count)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteCharsAsync(buffer, index, count);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000720 RID: 1824 RVA: 0x0001C76C File Offset: 0x0001A96C
		public override Task WriteRawAsync(char[] buffer, int index, int count)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteRawAsync(buffer, index, count);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000721 RID: 1825 RVA: 0x0001C798 File Offset: 0x0001A998
		public override Task WriteRawAsync(string data)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteRawAsync(data);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000722 RID: 1826 RVA: 0x0001C7C0 File Offset: 0x0001A9C0
		public override Task WriteBase64Async(byte[] buffer, int index, int count)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteBase64Async(buffer, index, count);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000723 RID: 1827 RVA: 0x0001C7EC File Offset: 0x0001A9EC
		public override Task WriteBinHexAsync(byte[] buffer, int index, int count)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteBinHexAsync(buffer, index, count);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000724 RID: 1828 RVA: 0x0001C818 File Offset: 0x0001AA18
		public override Task FlushAsync()
		{
			this.CheckAsync();
			Task result = this.coreWriter.FlushAsync();
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000725 RID: 1829 RVA: 0x0001C840 File Offset: 0x0001AA40
		public override Task WriteNmTokenAsync(string name)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteNmTokenAsync(name);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000726 RID: 1830 RVA: 0x0001C868 File Offset: 0x0001AA68
		public override Task WriteNameAsync(string name)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteNameAsync(name);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000727 RID: 1831 RVA: 0x0001C890 File Offset: 0x0001AA90
		public override Task WriteQualifiedNameAsync(string localName, string ns)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteQualifiedNameAsync(localName, ns);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000728 RID: 1832 RVA: 0x0001C8BC File Offset: 0x0001AABC
		public override Task WriteAttributesAsync(XmlReader reader, bool defattr)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteAttributesAsync(reader, defattr);
			this.lastTask = result;
			return result;
		}

		// Token: 0x06000729 RID: 1833 RVA: 0x0001C8E8 File Offset: 0x0001AAE8
		public override Task WriteNodeAsync(XmlReader reader, bool defattr)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteNodeAsync(reader, defattr);
			this.lastTask = result;
			return result;
		}

		// Token: 0x0600072A RID: 1834 RVA: 0x0001C914 File Offset: 0x0001AB14
		public override Task WriteNodeAsync(XPathNavigator navigator, bool defattr)
		{
			this.CheckAsync();
			Task result = this.coreWriter.WriteNodeAsync(navigator, defattr);
			this.lastTask = result;
			return result;
		}

		// Token: 0x040003DD RID: 989
		private readonly XmlWriter coreWriter;

		// Token: 0x040003DE RID: 990
		private Task lastTask = AsyncHelper.DoneTask;
	}
}
