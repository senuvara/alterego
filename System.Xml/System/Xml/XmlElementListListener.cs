﻿using System;

namespace System.Xml
{
	// Token: 0x0200022B RID: 555
	internal class XmlElementListListener
	{
		// Token: 0x060014BF RID: 5311 RVA: 0x00075BD8 File Offset: 0x00073DD8
		internal XmlElementListListener(XmlDocument doc, XmlElementList elemList)
		{
			this.doc = doc;
			this.elemList = new WeakReference(elemList);
			this.nodeChangeHandler = new XmlNodeChangedEventHandler(this.OnListChanged);
			doc.NodeInserted += this.nodeChangeHandler;
			doc.NodeRemoved += this.nodeChangeHandler;
		}

		// Token: 0x060014C0 RID: 5312 RVA: 0x00075C28 File Offset: 0x00073E28
		private void OnListChanged(object sender, XmlNodeChangedEventArgs args)
		{
			lock (this)
			{
				if (this.elemList != null)
				{
					XmlElementList xmlElementList = (XmlElementList)this.elemList.Target;
					if (xmlElementList != null)
					{
						xmlElementList.ConcurrencyCheck(args);
					}
					else
					{
						this.doc.NodeInserted -= this.nodeChangeHandler;
						this.doc.NodeRemoved -= this.nodeChangeHandler;
						this.elemList = null;
					}
				}
			}
		}

		// Token: 0x060014C1 RID: 5313 RVA: 0x00075CAC File Offset: 0x00073EAC
		internal void Unregister()
		{
			lock (this)
			{
				if (this.elemList != null)
				{
					this.doc.NodeInserted -= this.nodeChangeHandler;
					this.doc.NodeRemoved -= this.nodeChangeHandler;
					this.elemList = null;
				}
			}
		}

		// Token: 0x04000DCF RID: 3535
		private WeakReference elemList;

		// Token: 0x04000DD0 RID: 3536
		private XmlDocument doc;

		// Token: 0x04000DD1 RID: 3537
		private XmlNodeChangedEventHandler nodeChangeHandler;
	}
}
