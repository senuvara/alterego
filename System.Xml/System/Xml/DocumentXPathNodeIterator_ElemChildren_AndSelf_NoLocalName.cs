﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x02000211 RID: 529
	internal sealed class DocumentXPathNodeIterator_ElemChildren_AndSelf_NoLocalName : DocumentXPathNodeIterator_ElemChildren_NoLocalName
	{
		// Token: 0x060012EF RID: 4847 RVA: 0x00070A64 File Offset: 0x0006EC64
		internal DocumentXPathNodeIterator_ElemChildren_AndSelf_NoLocalName(DocumentXPathNavigator nav, string nsAtom) : base(nav, nsAtom)
		{
		}

		// Token: 0x060012F0 RID: 4848 RVA: 0x00070A6E File Offset: 0x0006EC6E
		internal DocumentXPathNodeIterator_ElemChildren_AndSelf_NoLocalName(DocumentXPathNodeIterator_ElemChildren_AndSelf_NoLocalName other) : base(other)
		{
		}

		// Token: 0x060012F1 RID: 4849 RVA: 0x00070A77 File Offset: 0x0006EC77
		public override XPathNodeIterator Clone()
		{
			return new DocumentXPathNodeIterator_ElemChildren_AndSelf_NoLocalName(this);
		}

		// Token: 0x060012F2 RID: 4850 RVA: 0x00070A80 File Offset: 0x0006EC80
		public override bool MoveNext()
		{
			if (this.CurrentPosition == 0)
			{
				XmlNode xmlNode = (XmlNode)((DocumentXPathNavigator)this.Current).UnderlyingObject;
				if (xmlNode.NodeType == XmlNodeType.Element && this.Match(xmlNode))
				{
					base.SetPosition(1);
					return true;
				}
			}
			return base.MoveNext();
		}
	}
}
