﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	/// <summary>Represents a processing instruction, which XML defines to keep processor-specific information in the text of the document.</summary>
	// Token: 0x02000240 RID: 576
	public class XmlProcessingInstruction : XmlLinkedNode
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlProcessingInstruction" /> class.</summary>
		/// <param name="target">The target of the processing instruction; see the <see cref="P:System.Xml.XmlProcessingInstruction.Target" /> property.</param>
		/// <param name="data">The content of the instruction; see the <see cref="P:System.Xml.XmlProcessingInstruction.Data" /> property.</param>
		/// <param name="doc">The parent XML document.</param>
		// Token: 0x0600162D RID: 5677 RVA: 0x0007B9FE File Offset: 0x00079BFE
		protected internal XmlProcessingInstruction(string target, string data, XmlDocument doc) : base(doc)
		{
			this.target = target;
			this.data = data;
		}

		/// <summary>Gets the qualified name of the node.</summary>
		/// <returns>For processing instruction nodes, this property returns the target of the processing instruction.</returns>
		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x0600162E RID: 5678 RVA: 0x0007BA15 File Offset: 0x00079C15
		public override string Name
		{
			get
			{
				if (this.target != null)
				{
					return this.target;
				}
				return string.Empty;
			}
		}

		/// <summary>Gets the local name of the node.</summary>
		/// <returns>For processing instruction nodes, this property returns the target of the processing instruction.</returns>
		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x0600162F RID: 5679 RVA: 0x00072E6B File Offset: 0x0007106B
		public override string LocalName
		{
			get
			{
				return this.Name;
			}
		}

		/// <summary>Gets or sets the value of the node.</summary>
		/// <returns>The entire content of the processing instruction, excluding the target.</returns>
		/// <exception cref="T:System.ArgumentException">Node is read-only. </exception>
		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x06001630 RID: 5680 RVA: 0x0007BA2B File Offset: 0x00079C2B
		// (set) Token: 0x06001631 RID: 5681 RVA: 0x0007BA33 File Offset: 0x00079C33
		public override string Value
		{
			get
			{
				return this.data;
			}
			set
			{
				this.Data = value;
			}
		}

		/// <summary>Gets the target of the processing instruction.</summary>
		/// <returns>The target of the processing instruction.</returns>
		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x06001632 RID: 5682 RVA: 0x0007BA3C File Offset: 0x00079C3C
		public string Target
		{
			get
			{
				return this.target;
			}
		}

		/// <summary>Gets or sets the content of the processing instruction, excluding the target.</summary>
		/// <returns>The content of the processing instruction, excluding the target.</returns>
		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x06001633 RID: 5683 RVA: 0x0007BA2B File Offset: 0x00079C2B
		// (set) Token: 0x06001634 RID: 5684 RVA: 0x0007BA44 File Offset: 0x00079C44
		public string Data
		{
			get
			{
				return this.data;
			}
			set
			{
				XmlNode parentNode = this.ParentNode;
				XmlNodeChangedEventArgs eventArgs = this.GetEventArgs(this, parentNode, parentNode, this.data, value, XmlNodeChangedAction.Change);
				if (eventArgs != null)
				{
					this.BeforeEvent(eventArgs);
				}
				this.data = value;
				if (eventArgs != null)
				{
					this.AfterEvent(eventArgs);
				}
			}
		}

		/// <summary>Gets or sets the concatenated values of the node and all its children.</summary>
		/// <returns>The concatenated values of the node and all its children.</returns>
		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x06001635 RID: 5685 RVA: 0x0007BA2B File Offset: 0x00079C2B
		// (set) Token: 0x06001636 RID: 5686 RVA: 0x0007BA33 File Offset: 0x00079C33
		public override string InnerText
		{
			get
			{
				return this.data;
			}
			set
			{
				this.Data = value;
			}
		}

		/// <summary>Gets the type of the current node.</summary>
		/// <returns>For <see langword="XmlProcessingInstruction" /> nodes, this value is XmlNodeType.ProcessingInstruction.</returns>
		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x06001637 RID: 5687 RVA: 0x00006EA3 File Offset: 0x000050A3
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.ProcessingInstruction;
			}
		}

		/// <summary>Creates a duplicate of this node.</summary>
		/// <param name="deep">
		///       <see langword="true" /> to recursively clone the subtree under the specified node; <see langword="false" /> to clone only the node itself. </param>
		/// <returns>The duplicate node.</returns>
		// Token: 0x06001638 RID: 5688 RVA: 0x0007BA85 File Offset: 0x00079C85
		public override XmlNode CloneNode(bool deep)
		{
			return this.OwnerDocument.CreateProcessingInstruction(this.target, this.data);
		}

		/// <summary>Saves the node to the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="w">The <see langword="XmlWriter" /> to which you want to save. </param>
		// Token: 0x06001639 RID: 5689 RVA: 0x0007BA9E File Offset: 0x00079C9E
		public override void WriteTo(XmlWriter w)
		{
			w.WriteProcessingInstruction(this.target, this.data);
		}

		/// <summary>Saves all the children of the node to the specified <see cref="T:System.Xml.XmlWriter" />. Because ProcessingInstruction nodes do not have children, this method has no effect.</summary>
		/// <param name="w">The <see langword="XmlWriter" /> to which you want to save. </param>
		// Token: 0x0600163A RID: 5690 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteContentTo(XmlWriter w)
		{
		}

		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x0600163B RID: 5691 RVA: 0x00072E6B File Offset: 0x0007106B
		internal override string XPLocalName
		{
			get
			{
				return this.Name;
			}
		}

		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x0600163C RID: 5692 RVA: 0x00006EA3 File Offset: 0x000050A3
		internal override XPathNodeType XPNodeType
		{
			get
			{
				return XPathNodeType.ProcessingInstruction;
			}
		}

		// Token: 0x04000E24 RID: 3620
		private string target;

		// Token: 0x04000E25 RID: 3621
		private string data;
	}
}
