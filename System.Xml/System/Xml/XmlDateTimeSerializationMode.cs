﻿using System;

namespace System.Xml
{
	/// <summary>Specifies how to treat the time value when converting between string and <see cref="T:System.DateTime" />.</summary>
	// Token: 0x0200028E RID: 654
	public enum XmlDateTimeSerializationMode
	{
		/// <summary>Treat as local time. If the <see cref="T:System.DateTime" /> object represents a Coordinated Universal Time (UTC), it is converted to the local time.</summary>
		// Token: 0x04000FF7 RID: 4087
		Local,
		/// <summary>Treat as a UTC. If the <see cref="T:System.DateTime" /> object represents a local time, it is converted to a UTC.</summary>
		// Token: 0x04000FF8 RID: 4088
		Utc,
		/// <summary>Treat as a local time if a <see cref="T:System.DateTime" /> is being converted to a string.</summary>
		// Token: 0x04000FF9 RID: 4089
		Unspecified,
		/// <summary>Time zone information should be preserved when converting.</summary>
		// Token: 0x04000FFA RID: 4090
		RoundtripKind
	}
}
