﻿using System;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x0200007F RID: 127
	internal class XmlTextWriterBase64Encoder : Base64Encoder
	{
		// Token: 0x060003B0 RID: 944 RVA: 0x0000E601 File Offset: 0x0000C801
		internal XmlTextWriterBase64Encoder(XmlTextEncoder xmlTextEncoder)
		{
			this.xmlTextEncoder = xmlTextEncoder;
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x0000E610 File Offset: 0x0000C810
		internal override void WriteChars(char[] chars, int index, int count)
		{
			this.xmlTextEncoder.WriteRaw(chars, index, count);
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x0000A6CF File Offset: 0x000088CF
		internal override Task WriteCharsAsync(char[] chars, int index, int count)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000227 RID: 551
		private XmlTextEncoder xmlTextEncoder;
	}
}
