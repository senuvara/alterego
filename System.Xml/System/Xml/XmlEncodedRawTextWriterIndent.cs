﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x020000F3 RID: 243
	internal class XmlEncodedRawTextWriterIndent : XmlEncodedRawTextWriter
	{
		// Token: 0x06000855 RID: 2133 RVA: 0x0002741A File Offset: 0x0002561A
		public XmlEncodedRawTextWriterIndent(TextWriter writer, XmlWriterSettings settings) : base(writer, settings)
		{
			this.Init(settings);
		}

		// Token: 0x06000856 RID: 2134 RVA: 0x0002742B File Offset: 0x0002562B
		public XmlEncodedRawTextWriterIndent(Stream stream, XmlWriterSettings settings) : base(stream, settings)
		{
			this.Init(settings);
		}

		// Token: 0x17000175 RID: 373
		// (get) Token: 0x06000857 RID: 2135 RVA: 0x0002743C File Offset: 0x0002563C
		public override XmlWriterSettings Settings
		{
			get
			{
				XmlWriterSettings settings = base.Settings;
				settings.ReadOnly = false;
				settings.Indent = true;
				settings.IndentChars = this.indentChars;
				settings.NewLineOnAttributes = this.newLineOnAttributes;
				settings.ReadOnly = true;
				return settings;
			}
		}

		// Token: 0x06000858 RID: 2136 RVA: 0x00027471 File Offset: 0x00025671
		public override void WriteDocType(string name, string pubid, string sysid, string subset)
		{
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			base.WriteDocType(name, pubid, sysid, subset);
		}

		// Token: 0x06000859 RID: 2137 RVA: 0x0002749C File Offset: 0x0002569C
		public override void WriteStartElement(string prefix, string localName, string ns)
		{
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			this.indentLevel++;
			this.mixedContentStack.PushBit(this.mixedContent);
			base.WriteStartElement(prefix, localName, ns);
		}

		// Token: 0x0600085A RID: 2138 RVA: 0x000274ED File Offset: 0x000256ED
		internal override void StartElementContent()
		{
			if (this.indentLevel == 1 && this.conformanceLevel == ConformanceLevel.Document)
			{
				this.mixedContent = false;
			}
			else
			{
				this.mixedContent = this.mixedContentStack.PeekBit();
			}
			base.StartElementContent();
		}

		// Token: 0x0600085B RID: 2139 RVA: 0x00027521 File Offset: 0x00025721
		internal override void OnRootElement(ConformanceLevel currentConformanceLevel)
		{
			this.conformanceLevel = currentConformanceLevel;
		}

		// Token: 0x0600085C RID: 2140 RVA: 0x0002752C File Offset: 0x0002572C
		internal override void WriteEndElement(string prefix, string localName, string ns)
		{
			this.indentLevel--;
			if (!this.mixedContent && this.contentPos != this.bufPos && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			this.mixedContent = this.mixedContentStack.PopBit();
			base.WriteEndElement(prefix, localName, ns);
		}

		// Token: 0x0600085D RID: 2141 RVA: 0x0002758C File Offset: 0x0002578C
		internal override void WriteFullEndElement(string prefix, string localName, string ns)
		{
			this.indentLevel--;
			if (!this.mixedContent && this.contentPos != this.bufPos && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			this.mixedContent = this.mixedContentStack.PopBit();
			base.WriteFullEndElement(prefix, localName, ns);
		}

		// Token: 0x0600085E RID: 2142 RVA: 0x000275EB File Offset: 0x000257EB
		public override void WriteStartAttribute(string prefix, string localName, string ns)
		{
			if (this.newLineOnAttributes)
			{
				this.WriteIndent();
			}
			base.WriteStartAttribute(prefix, localName, ns);
		}

		// Token: 0x0600085F RID: 2143 RVA: 0x00027604 File Offset: 0x00025804
		public override void WriteCData(string text)
		{
			this.mixedContent = true;
			base.WriteCData(text);
		}

		// Token: 0x06000860 RID: 2144 RVA: 0x00027614 File Offset: 0x00025814
		public override void WriteComment(string text)
		{
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			base.WriteComment(text);
		}

		// Token: 0x06000861 RID: 2145 RVA: 0x00027639 File Offset: 0x00025839
		public override void WriteProcessingInstruction(string target, string text)
		{
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				this.WriteIndent();
			}
			base.WriteProcessingInstruction(target, text);
		}

		// Token: 0x06000862 RID: 2146 RVA: 0x0002765F File Offset: 0x0002585F
		public override void WriteEntityRef(string name)
		{
			this.mixedContent = true;
			base.WriteEntityRef(name);
		}

		// Token: 0x06000863 RID: 2147 RVA: 0x0002766F File Offset: 0x0002586F
		public override void WriteCharEntity(char ch)
		{
			this.mixedContent = true;
			base.WriteCharEntity(ch);
		}

		// Token: 0x06000864 RID: 2148 RVA: 0x0002767F File Offset: 0x0002587F
		public override void WriteSurrogateCharEntity(char lowChar, char highChar)
		{
			this.mixedContent = true;
			base.WriteSurrogateCharEntity(lowChar, highChar);
		}

		// Token: 0x06000865 RID: 2149 RVA: 0x00027690 File Offset: 0x00025890
		public override void WriteWhitespace(string ws)
		{
			this.mixedContent = true;
			base.WriteWhitespace(ws);
		}

		// Token: 0x06000866 RID: 2150 RVA: 0x000276A0 File Offset: 0x000258A0
		public override void WriteString(string text)
		{
			this.mixedContent = true;
			base.WriteString(text);
		}

		// Token: 0x06000867 RID: 2151 RVA: 0x000276B0 File Offset: 0x000258B0
		public override void WriteChars(char[] buffer, int index, int count)
		{
			this.mixedContent = true;
			base.WriteChars(buffer, index, count);
		}

		// Token: 0x06000868 RID: 2152 RVA: 0x000276C2 File Offset: 0x000258C2
		public override void WriteRaw(char[] buffer, int index, int count)
		{
			this.mixedContent = true;
			base.WriteRaw(buffer, index, count);
		}

		// Token: 0x06000869 RID: 2153 RVA: 0x000276D4 File Offset: 0x000258D4
		public override void WriteRaw(string data)
		{
			this.mixedContent = true;
			base.WriteRaw(data);
		}

		// Token: 0x0600086A RID: 2154 RVA: 0x000276E4 File Offset: 0x000258E4
		public override void WriteBase64(byte[] buffer, int index, int count)
		{
			this.mixedContent = true;
			base.WriteBase64(buffer, index, count);
		}

		// Token: 0x0600086B RID: 2155 RVA: 0x000276F8 File Offset: 0x000258F8
		private void Init(XmlWriterSettings settings)
		{
			this.indentLevel = 0;
			this.indentChars = settings.IndentChars;
			this.newLineOnAttributes = settings.NewLineOnAttributes;
			this.mixedContentStack = new BitStack();
			if (this.checkCharacters)
			{
				if (this.newLineOnAttributes)
				{
					base.ValidateContentChars(this.indentChars, "IndentChars", true);
					base.ValidateContentChars(this.newLineChars, "NewLineChars", true);
					return;
				}
				base.ValidateContentChars(this.indentChars, "IndentChars", false);
				if (this.newLineHandling != NewLineHandling.Replace)
				{
					base.ValidateContentChars(this.newLineChars, "NewLineChars", false);
				}
			}
		}

		// Token: 0x0600086C RID: 2156 RVA: 0x00027790 File Offset: 0x00025990
		private void WriteIndent()
		{
			base.RawText(this.newLineChars);
			for (int i = this.indentLevel; i > 0; i--)
			{
				base.RawText(this.indentChars);
			}
		}

		// Token: 0x0600086D RID: 2157 RVA: 0x000277C8 File Offset: 0x000259C8
		public override async Task WriteDocTypeAsync(string name, string pubid, string sysid, string subset)
		{
			base.CheckAsyncCall();
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			await base.WriteDocTypeAsync(name, pubid, sysid, subset).ConfigureAwait(false);
		}

		// Token: 0x0600086E RID: 2158 RVA: 0x00027830 File Offset: 0x00025A30
		public override async Task WriteStartElementAsync(string prefix, string localName, string ns)
		{
			base.CheckAsyncCall();
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			this.indentLevel++;
			this.mixedContentStack.PushBit(this.mixedContent);
			await base.WriteStartElementAsync(prefix, localName, ns).ConfigureAwait(false);
		}

		// Token: 0x0600086F RID: 2159 RVA: 0x00027890 File Offset: 0x00025A90
		internal override async Task WriteEndElementAsync(string prefix, string localName, string ns)
		{
			base.CheckAsyncCall();
			this.indentLevel--;
			if (!this.mixedContent && this.contentPos != this.bufPos && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			this.mixedContent = this.mixedContentStack.PopBit();
			await base.WriteEndElementAsync(prefix, localName, ns).ConfigureAwait(false);
		}

		// Token: 0x06000870 RID: 2160 RVA: 0x000278F0 File Offset: 0x00025AF0
		internal override async Task WriteFullEndElementAsync(string prefix, string localName, string ns)
		{
			base.CheckAsyncCall();
			this.indentLevel--;
			if (!this.mixedContent && this.contentPos != this.bufPos && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			this.mixedContent = this.mixedContentStack.PopBit();
			await base.WriteFullEndElementAsync(prefix, localName, ns).ConfigureAwait(false);
		}

		// Token: 0x06000871 RID: 2161 RVA: 0x00027950 File Offset: 0x00025B50
		protected internal override async Task WriteStartAttributeAsync(string prefix, string localName, string ns)
		{
			base.CheckAsyncCall();
			if (this.newLineOnAttributes)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			await base.WriteStartAttributeAsync(prefix, localName, ns).ConfigureAwait(false);
		}

		// Token: 0x06000872 RID: 2162 RVA: 0x000279AD File Offset: 0x00025BAD
		public override Task WriteCDataAsync(string text)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteCDataAsync(text);
		}

		// Token: 0x06000873 RID: 2163 RVA: 0x000279C4 File Offset: 0x00025BC4
		public override async Task WriteCommentAsync(string text)
		{
			base.CheckAsyncCall();
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			await base.WriteCommentAsync(text).ConfigureAwait(false);
		}

		// Token: 0x06000874 RID: 2164 RVA: 0x00027A14 File Offset: 0x00025C14
		public override async Task WriteProcessingInstructionAsync(string target, string text)
		{
			base.CheckAsyncCall();
			if (!this.mixedContent && this.textPos != this.bufPos)
			{
				await this.WriteIndentAsync().ConfigureAwait(false);
			}
			await base.WriteProcessingInstructionAsync(target, text).ConfigureAwait(false);
		}

		// Token: 0x06000875 RID: 2165 RVA: 0x00027A69 File Offset: 0x00025C69
		public override Task WriteEntityRefAsync(string name)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteEntityRefAsync(name);
		}

		// Token: 0x06000876 RID: 2166 RVA: 0x00027A7F File Offset: 0x00025C7F
		public override Task WriteCharEntityAsync(char ch)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteCharEntityAsync(ch);
		}

		// Token: 0x06000877 RID: 2167 RVA: 0x00027A95 File Offset: 0x00025C95
		public override Task WriteSurrogateCharEntityAsync(char lowChar, char highChar)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteSurrogateCharEntityAsync(lowChar, highChar);
		}

		// Token: 0x06000878 RID: 2168 RVA: 0x00027AAC File Offset: 0x00025CAC
		public override Task WriteWhitespaceAsync(string ws)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteWhitespaceAsync(ws);
		}

		// Token: 0x06000879 RID: 2169 RVA: 0x00027AC2 File Offset: 0x00025CC2
		public override Task WriteStringAsync(string text)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteStringAsync(text);
		}

		// Token: 0x0600087A RID: 2170 RVA: 0x00027AD8 File Offset: 0x00025CD8
		public override Task WriteCharsAsync(char[] buffer, int index, int count)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteCharsAsync(buffer, index, count);
		}

		// Token: 0x0600087B RID: 2171 RVA: 0x00027AF0 File Offset: 0x00025CF0
		public override Task WriteRawAsync(char[] buffer, int index, int count)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteRawAsync(buffer, index, count);
		}

		// Token: 0x0600087C RID: 2172 RVA: 0x00027B08 File Offset: 0x00025D08
		public override Task WriteRawAsync(string data)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteRawAsync(data);
		}

		// Token: 0x0600087D RID: 2173 RVA: 0x00027B1E File Offset: 0x00025D1E
		public override Task WriteBase64Async(byte[] buffer, int index, int count)
		{
			base.CheckAsyncCall();
			this.mixedContent = true;
			return base.WriteBase64Async(buffer, index, count);
		}

		// Token: 0x0600087E RID: 2174 RVA: 0x00027B38 File Offset: 0x00025D38
		private async Task WriteIndentAsync()
		{
			base.CheckAsyncCall();
			await base.RawTextAsync(this.newLineChars).ConfigureAwait(false);
			for (int i = this.indentLevel; i > 0; i--)
			{
				await base.RawTextAsync(this.indentChars).ConfigureAwait(false);
			}
		}

		// Token: 0x0600087F RID: 2175 RVA: 0x00027B7D File Offset: 0x00025D7D
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__0(string name, string pubid, string sysid, string subset)
		{
			return base.WriteDocTypeAsync(name, pubid, sysid, subset);
		}

		// Token: 0x06000880 RID: 2176 RVA: 0x00027B8A File Offset: 0x00025D8A
		[DebuggerHidden]
		[CompilerGenerated]
		private Task <>n__1(string prefix, string localName, string ns)
		{
			return base.WriteStartElementAsync(prefix, localName, ns);
		}

		// Token: 0x06000881 RID: 2177 RVA: 0x00027B95 File Offset: 0x00025D95
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__2(string prefix, string localName, string ns)
		{
			return base.WriteEndElementAsync(prefix, localName, ns);
		}

		// Token: 0x06000882 RID: 2178 RVA: 0x00027BA0 File Offset: 0x00025DA0
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__3(string prefix, string localName, string ns)
		{
			return base.WriteFullEndElementAsync(prefix, localName, ns);
		}

		// Token: 0x06000883 RID: 2179 RVA: 0x00027BAB File Offset: 0x00025DAB
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__4(string prefix, string localName, string ns)
		{
			return base.WriteStartAttributeAsync(prefix, localName, ns);
		}

		// Token: 0x06000884 RID: 2180 RVA: 0x00027BB6 File Offset: 0x00025DB6
		[CompilerGenerated]
		[DebuggerHidden]
		private Task <>n__5(string text)
		{
			return base.WriteCommentAsync(text);
		}

		// Token: 0x06000885 RID: 2181 RVA: 0x00027BBF File Offset: 0x00025DBF
		[DebuggerHidden]
		[CompilerGenerated]
		private Task <>n__6(string name, string text)
		{
			return base.WriteProcessingInstructionAsync(name, text);
		}

		// Token: 0x040004F3 RID: 1267
		protected int indentLevel;

		// Token: 0x040004F4 RID: 1268
		protected bool newLineOnAttributes;

		// Token: 0x040004F5 RID: 1269
		protected string indentChars;

		// Token: 0x040004F6 RID: 1270
		protected bool mixedContent;

		// Token: 0x040004F7 RID: 1271
		private BitStack mixedContentStack;

		// Token: 0x040004F8 RID: 1272
		protected ConformanceLevel conformanceLevel;

		// Token: 0x020000F4 RID: 244
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteDocTypeAsync>d__31 : IAsyncStateMachine
		{
			// Token: 0x06000886 RID: 2182 RVA: 0x00027BCC File Offset: 0x00025DCC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriterIndent xmlEncodedRawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_10A;
						}
						xmlEncodedRawTextWriterIndent.CheckAsyncCall();
						if (xmlEncodedRawTextWriterIndent.mixedContent || xmlEncodedRawTextWriterIndent.textPos == xmlEncodedRawTextWriterIndent.bufPos)
						{
							goto IL_98;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteDocTypeAsync>d__31>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_98:
					configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.<>n__0(name, pubid, sysid, subset).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteDocTypeAsync>d__31>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_10A:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000887 RID: 2183 RVA: 0x00027D34 File Offset: 0x00025F34
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040004F9 RID: 1273
			public int <>1__state;

			// Token: 0x040004FA RID: 1274
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040004FB RID: 1275
			public XmlEncodedRawTextWriterIndent <>4__this;

			// Token: 0x040004FC RID: 1276
			public string name;

			// Token: 0x040004FD RID: 1277
			public string pubid;

			// Token: 0x040004FE RID: 1278
			public string sysid;

			// Token: 0x040004FF RID: 1279
			public string subset;

			// Token: 0x04000500 RID: 1280
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000F5 RID: 245
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartElementAsync>d__32 : IAsyncStateMachine
		{
			// Token: 0x06000888 RID: 2184 RVA: 0x00027D44 File Offset: 0x00025F44
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriterIndent xmlEncodedRawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_123;
						}
						xmlEncodedRawTextWriterIndent.CheckAsyncCall();
						if (xmlEncodedRawTextWriterIndent.mixedContent || xmlEncodedRawTextWriterIndent.textPos == xmlEncodedRawTextWriterIndent.bufPos)
						{
							goto IL_98;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteStartElementAsync>d__32>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_98:
					xmlEncodedRawTextWriterIndent.indentLevel++;
					xmlEncodedRawTextWriterIndent.mixedContentStack.PushBit(xmlEncodedRawTextWriterIndent.mixedContent);
					configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.<>n__1(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteStartElementAsync>d__32>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_123:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000889 RID: 2185 RVA: 0x00027EC8 File Offset: 0x000260C8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000501 RID: 1281
			public int <>1__state;

			// Token: 0x04000502 RID: 1282
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000503 RID: 1283
			public XmlEncodedRawTextWriterIndent <>4__this;

			// Token: 0x04000504 RID: 1284
			public string prefix;

			// Token: 0x04000505 RID: 1285
			public string localName;

			// Token: 0x04000506 RID: 1286
			public string ns;

			// Token: 0x04000507 RID: 1287
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000F6 RID: 246
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteEndElementAsync>d__33 : IAsyncStateMachine
		{
			// Token: 0x0600088A RID: 2186 RVA: 0x00027ED8 File Offset: 0x000260D8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriterIndent xmlEncodedRawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_137;
						}
						xmlEncodedRawTextWriterIndent.CheckAsyncCall();
						xmlEncodedRawTextWriterIndent.indentLevel--;
						if (xmlEncodedRawTextWriterIndent.mixedContent || xmlEncodedRawTextWriterIndent.contentPos == xmlEncodedRawTextWriterIndent.bufPos || xmlEncodedRawTextWriterIndent.textPos == xmlEncodedRawTextWriterIndent.bufPos)
						{
							goto IL_BA;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteEndElementAsync>d__33>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_BA:
					xmlEncodedRawTextWriterIndent.mixedContent = xmlEncodedRawTextWriterIndent.mixedContentStack.PopBit();
					configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.<>n__2(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteEndElementAsync>d__33>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_137:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600088B RID: 2187 RVA: 0x00028070 File Offset: 0x00026270
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000508 RID: 1288
			public int <>1__state;

			// Token: 0x04000509 RID: 1289
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400050A RID: 1290
			public XmlEncodedRawTextWriterIndent <>4__this;

			// Token: 0x0400050B RID: 1291
			public string prefix;

			// Token: 0x0400050C RID: 1292
			public string localName;

			// Token: 0x0400050D RID: 1293
			public string ns;

			// Token: 0x0400050E RID: 1294
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000F7 RID: 247
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteFullEndElementAsync>d__34 : IAsyncStateMachine
		{
			// Token: 0x0600088C RID: 2188 RVA: 0x00028080 File Offset: 0x00026280
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriterIndent xmlEncodedRawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_137;
						}
						xmlEncodedRawTextWriterIndent.CheckAsyncCall();
						xmlEncodedRawTextWriterIndent.indentLevel--;
						if (xmlEncodedRawTextWriterIndent.mixedContent || xmlEncodedRawTextWriterIndent.contentPos == xmlEncodedRawTextWriterIndent.bufPos || xmlEncodedRawTextWriterIndent.textPos == xmlEncodedRawTextWriterIndent.bufPos)
						{
							goto IL_BA;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteFullEndElementAsync>d__34>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_BA:
					xmlEncodedRawTextWriterIndent.mixedContent = xmlEncodedRawTextWriterIndent.mixedContentStack.PopBit();
					configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.<>n__3(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteFullEndElementAsync>d__34>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_137:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600088D RID: 2189 RVA: 0x00028218 File Offset: 0x00026418
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400050F RID: 1295
			public int <>1__state;

			// Token: 0x04000510 RID: 1296
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000511 RID: 1297
			public XmlEncodedRawTextWriterIndent <>4__this;

			// Token: 0x04000512 RID: 1298
			public string prefix;

			// Token: 0x04000513 RID: 1299
			public string localName;

			// Token: 0x04000514 RID: 1300
			public string ns;

			// Token: 0x04000515 RID: 1301
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000F8 RID: 248
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteStartAttributeAsync>d__35 : IAsyncStateMachine
		{
			// Token: 0x0600088E RID: 2190 RVA: 0x00028228 File Offset: 0x00026428
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriterIndent xmlEncodedRawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_F6;
						}
						xmlEncodedRawTextWriterIndent.CheckAsyncCall();
						if (!xmlEncodedRawTextWriterIndent.newLineOnAttributes)
						{
							goto IL_8A;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteStartAttributeAsync>d__35>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_8A:
					configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.<>n__4(prefix, localName, ns).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteStartAttributeAsync>d__35>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_F6:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600088F RID: 2191 RVA: 0x00028370 File Offset: 0x00026570
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000516 RID: 1302
			public int <>1__state;

			// Token: 0x04000517 RID: 1303
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000518 RID: 1304
			public XmlEncodedRawTextWriterIndent <>4__this;

			// Token: 0x04000519 RID: 1305
			public string prefix;

			// Token: 0x0400051A RID: 1306
			public string localName;

			// Token: 0x0400051B RID: 1307
			public string ns;

			// Token: 0x0400051C RID: 1308
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000F9 RID: 249
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteCommentAsync>d__37 : IAsyncStateMachine
		{
			// Token: 0x06000890 RID: 2192 RVA: 0x00028380 File Offset: 0x00026580
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriterIndent xmlEncodedRawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_F8;
						}
						xmlEncodedRawTextWriterIndent.CheckAsyncCall();
						if (xmlEncodedRawTextWriterIndent.mixedContent || xmlEncodedRawTextWriterIndent.textPos == xmlEncodedRawTextWriterIndent.bufPos)
						{
							goto IL_98;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteCommentAsync>d__37>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_98:
					configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.<>n__5(text).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteCommentAsync>d__37>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_F8:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000891 RID: 2193 RVA: 0x000284CC File Offset: 0x000266CC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400051D RID: 1309
			public int <>1__state;

			// Token: 0x0400051E RID: 1310
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400051F RID: 1311
			public XmlEncodedRawTextWriterIndent <>4__this;

			// Token: 0x04000520 RID: 1312
			public string text;

			// Token: 0x04000521 RID: 1313
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000FA RID: 250
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteProcessingInstructionAsync>d__38 : IAsyncStateMachine
		{
			// Token: 0x06000892 RID: 2194 RVA: 0x000284DC File Offset: 0x000266DC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriterIndent xmlEncodedRawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_FE;
						}
						xmlEncodedRawTextWriterIndent.CheckAsyncCall();
						if (xmlEncodedRawTextWriterIndent.mixedContent || xmlEncodedRawTextWriterIndent.textPos == xmlEncodedRawTextWriterIndent.bufPos)
						{
							goto IL_98;
						}
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.WriteIndentAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteProcessingInstructionAsync>d__38>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_98:
					configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.<>n__6(target, text).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteProcessingInstructionAsync>d__38>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_FE:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000893 RID: 2195 RVA: 0x0002862C File Offset: 0x0002682C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000522 RID: 1314
			public int <>1__state;

			// Token: 0x04000523 RID: 1315
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000524 RID: 1316
			public XmlEncodedRawTextWriterIndent <>4__this;

			// Token: 0x04000525 RID: 1317
			public string target;

			// Token: 0x04000526 RID: 1318
			public string text;

			// Token: 0x04000527 RID: 1319
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x020000FB RID: 251
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteIndentAsync>d__48 : IAsyncStateMachine
		{
			// Token: 0x06000894 RID: 2196 RVA: 0x0002863C File Offset: 0x0002683C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				XmlEncodedRawTextWriterIndent xmlEncodedRawTextWriterIndent = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_F6;
						}
						xmlEncodedRawTextWriterIndent.CheckAsyncCall();
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.RawTextAsync(xmlEncodedRawTextWriterIndent.newLineChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteIndentAsync>d__48>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					i = xmlEncodedRawTextWriterIndent.indentLevel;
					goto IL_10F;
					IL_F6:
					configuredTaskAwaiter.GetResult();
					int num3 = i;
					i = num3 - 1;
					IL_10F:
					if (i > 0)
					{
						configuredTaskAwaiter = xmlEncodedRawTextWriterIndent.RawTextAsync(xmlEncodedRawTextWriterIndent.indentChars).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, XmlEncodedRawTextWriterIndent.<WriteIndentAsync>d__48>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_F6;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x06000895 RID: 2197 RVA: 0x000287B0 File Offset: 0x000269B0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000528 RID: 1320
			public int <>1__state;

			// Token: 0x04000529 RID: 1321
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400052A RID: 1322
			public XmlEncodedRawTextWriterIndent <>4__this;

			// Token: 0x0400052B RID: 1323
			private int <i>5__1;

			// Token: 0x0400052C RID: 1324
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
