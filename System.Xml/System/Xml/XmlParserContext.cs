﻿using System;
using System.Text;

namespace System.Xml
{
	/// <summary>Provides all the context information required by the <see cref="T:System.Xml.XmlReader" /> to parse an XML fragment.</summary>
	// Token: 0x020000FF RID: 255
	public class XmlParserContext
	{
		/// <summary>Initializes a new instance of the <see langword="XmlParserContext" /> class with the specified <see cref="T:System.Xml.XmlNameTable" />, <see cref="T:System.Xml.XmlNamespaceManager" />, <see langword="xml:lang" />, and <see langword="xml:space" /> values.</summary>
		/// <param name="nt">The <see cref="T:System.Xml.XmlNameTable" /> to use to atomize strings. If this is <see langword="null" />, the name table used to construct the <paramref name="nsMgr" /> is used instead. For more information about atomized strings, see <see cref="T:System.Xml.XmlNameTable" />. </param>
		/// <param name="nsMgr">The <see cref="T:System.Xml.XmlNamespaceManager" /> to use for looking up namespace information, or <see langword="null" />. </param>
		/// <param name="xmlLang">The <see langword="xml:lang" /> scope. </param>
		/// <param name="xmlSpace">An <see cref="T:System.Xml.XmlSpace" /> value indicating the <see langword="xml:space" /> scope. </param>
		/// <exception cref="T:System.Xml.XmlException">
		///         <paramref name="nt" /> is not the same <see langword="XmlNameTable" /> used to construct <paramref name="nsMgr" />. </exception>
		// Token: 0x060008CD RID: 2253 RVA: 0x0002913C File Offset: 0x0002733C
		public XmlParserContext(XmlNameTable nt, XmlNamespaceManager nsMgr, string xmlLang, XmlSpace xmlSpace) : this(nt, nsMgr, null, null, null, null, string.Empty, xmlLang, xmlSpace)
		{
		}

		/// <summary>Initializes a new instance of the <see langword="XmlParserContext" /> class with the specified <see cref="T:System.Xml.XmlNameTable" />, <see cref="T:System.Xml.XmlNamespaceManager" />, <see langword="xml:lang" />, <see langword="xml:space" />, and encoding.</summary>
		/// <param name="nt">The <see cref="T:System.Xml.XmlNameTable" /> to use to atomize strings. If this is <see langword="null" />, the name table used to construct the <paramref name="nsMgr" /> is used instead. For more information on atomized strings, see <see cref="T:System.Xml.XmlNameTable" />. </param>
		/// <param name="nsMgr">The <see cref="T:System.Xml.XmlNamespaceManager" /> to use for looking up namespace information, or <see langword="null" />. </param>
		/// <param name="xmlLang">The <see langword="xml:lang" /> scope. </param>
		/// <param name="xmlSpace">An <see cref="T:System.Xml.XmlSpace" /> value indicating the <see langword="xml:space" /> scope. </param>
		/// <param name="enc">An <see cref="T:System.Text.Encoding" /> object indicating the encoding setting. </param>
		/// <exception cref="T:System.Xml.XmlException">
		///         <paramref name="nt" /> is not the same <see langword="XmlNameTable" /> used to construct <paramref name="nsMgr" />. </exception>
		// Token: 0x060008CE RID: 2254 RVA: 0x00029160 File Offset: 0x00027360
		public XmlParserContext(XmlNameTable nt, XmlNamespaceManager nsMgr, string xmlLang, XmlSpace xmlSpace, Encoding enc) : this(nt, nsMgr, null, null, null, null, string.Empty, xmlLang, xmlSpace, enc)
		{
		}

		/// <summary>Initializes a new instance of the <see langword="XmlParserContext" /> class with the specified <see cref="T:System.Xml.XmlNameTable" />, <see cref="T:System.Xml.XmlNamespaceManager" />, base URI, <see langword="xml:lang" />, <see langword="xml:space" />, and document type values.</summary>
		/// <param name="nt">The <see cref="T:System.Xml.XmlNameTable" /> to use to atomize strings. If this is <see langword="null" />, the name table used to construct the <paramref name="nsMgr" /> is used instead. For more information about atomized strings, see <see cref="T:System.Xml.XmlNameTable" />. </param>
		/// <param name="nsMgr">The <see cref="T:System.Xml.XmlNamespaceManager" /> to use for looking up namespace information, or <see langword="null" />. </param>
		/// <param name="docTypeName">The name of the document type declaration. </param>
		/// <param name="pubId">The public identifier. </param>
		/// <param name="sysId">The system identifier. </param>
		/// <param name="internalSubset">The internal DTD subset. The DTD subset is used for entity resolution, not for document validation.</param>
		/// <param name="baseURI">The base URI for the XML fragment (the location from which the fragment was loaded). </param>
		/// <param name="xmlLang">The <see langword="xml:lang" /> scope. </param>
		/// <param name="xmlSpace">An <see cref="T:System.Xml.XmlSpace" /> value indicating the <see langword="xml:space" /> scope. </param>
		/// <exception cref="T:System.Xml.XmlException">
		///         <paramref name="nt" /> is not the same <see langword="XmlNameTable" /> used to construct <paramref name="nsMgr" />. </exception>
		// Token: 0x060008CF RID: 2255 RVA: 0x00029184 File Offset: 0x00027384
		public XmlParserContext(XmlNameTable nt, XmlNamespaceManager nsMgr, string docTypeName, string pubId, string sysId, string internalSubset, string baseURI, string xmlLang, XmlSpace xmlSpace) : this(nt, nsMgr, docTypeName, pubId, sysId, internalSubset, baseURI, xmlLang, xmlSpace, null)
		{
		}

		/// <summary>Initializes a new instance of the <see langword="XmlParserContext" /> class with the specified <see cref="T:System.Xml.XmlNameTable" />, <see cref="T:System.Xml.XmlNamespaceManager" />, base URI, <see langword="xml:lang" />, <see langword="xml:space" />, encoding, and document type values.</summary>
		/// <param name="nt">The <see cref="T:System.Xml.XmlNameTable" /> to use to atomize strings. If this is <see langword="null" />, the name table used to construct the <paramref name="nsMgr" /> is used instead. For more information about atomized strings, see <see cref="T:System.Xml.XmlNameTable" />. </param>
		/// <param name="nsMgr">The <see cref="T:System.Xml.XmlNamespaceManager" /> to use for looking up namespace information, or <see langword="null" />. </param>
		/// <param name="docTypeName">The name of the document type declaration. </param>
		/// <param name="pubId">The public identifier. </param>
		/// <param name="sysId">The system identifier. </param>
		/// <param name="internalSubset">The internal DTD subset. The DTD is used for entity resolution, not for document validation.</param>
		/// <param name="baseURI">The base URI for the XML fragment (the location from which the fragment was loaded). </param>
		/// <param name="xmlLang">The <see langword="xml:lang" /> scope. </param>
		/// <param name="xmlSpace">An <see cref="T:System.Xml.XmlSpace" /> value indicating the <see langword="xml:space" /> scope. </param>
		/// <param name="enc">An <see cref="T:System.Text.Encoding" /> object indicating the encoding setting. </param>
		/// <exception cref="T:System.Xml.XmlException">
		///         <paramref name="nt" /> is not the same <see langword="XmlNameTable" /> used to construct <paramref name="nsMgr" />. </exception>
		// Token: 0x060008D0 RID: 2256 RVA: 0x000291A8 File Offset: 0x000273A8
		public XmlParserContext(XmlNameTable nt, XmlNamespaceManager nsMgr, string docTypeName, string pubId, string sysId, string internalSubset, string baseURI, string xmlLang, XmlSpace xmlSpace, Encoding enc)
		{
			if (nsMgr != null)
			{
				if (nt == null)
				{
					this._nt = nsMgr.NameTable;
				}
				else
				{
					if (nt != nsMgr.NameTable)
					{
						throw new XmlException("Not the same name table.", string.Empty);
					}
					this._nt = nt;
				}
			}
			else
			{
				this._nt = nt;
			}
			this._nsMgr = nsMgr;
			this._docTypeName = ((docTypeName == null) ? string.Empty : docTypeName);
			this._pubId = ((pubId == null) ? string.Empty : pubId);
			this._sysId = ((sysId == null) ? string.Empty : sysId);
			this._internalSubset = ((internalSubset == null) ? string.Empty : internalSubset);
			this._baseURI = ((baseURI == null) ? string.Empty : baseURI);
			this._xmlLang = ((xmlLang == null) ? string.Empty : xmlLang);
			this._xmlSpace = xmlSpace;
			this._encoding = enc;
		}

		/// <summary>Gets the <see cref="T:System.Xml.XmlNameTable" /> used to atomize strings. For more information on atomized strings, see <see cref="T:System.Xml.XmlNameTable" />.</summary>
		/// <returns>The <see langword="XmlNameTable" />.</returns>
		// Token: 0x1700017E RID: 382
		// (get) Token: 0x060008D1 RID: 2257 RVA: 0x000292C1 File Offset: 0x000274C1
		// (set) Token: 0x060008D2 RID: 2258 RVA: 0x000292C9 File Offset: 0x000274C9
		public XmlNameTable NameTable
		{
			get
			{
				return this._nt;
			}
			set
			{
				this._nt = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.XmlNamespaceManager" />.</summary>
		/// <returns>The <see langword="XmlNamespaceManager" />.</returns>
		// Token: 0x1700017F RID: 383
		// (get) Token: 0x060008D3 RID: 2259 RVA: 0x000292D2 File Offset: 0x000274D2
		// (set) Token: 0x060008D4 RID: 2260 RVA: 0x000292DA File Offset: 0x000274DA
		public XmlNamespaceManager NamespaceManager
		{
			get
			{
				return this._nsMgr;
			}
			set
			{
				this._nsMgr = value;
			}
		}

		/// <summary>Gets or sets the name of the document type declaration.</summary>
		/// <returns>The name of the document type declaration.</returns>
		// Token: 0x17000180 RID: 384
		// (get) Token: 0x060008D5 RID: 2261 RVA: 0x000292E3 File Offset: 0x000274E3
		// (set) Token: 0x060008D6 RID: 2262 RVA: 0x000292EB File Offset: 0x000274EB
		public string DocTypeName
		{
			get
			{
				return this._docTypeName;
			}
			set
			{
				this._docTypeName = ((value == null) ? string.Empty : value);
			}
		}

		/// <summary>Gets or sets the public identifier.</summary>
		/// <returns>The public identifier.</returns>
		// Token: 0x17000181 RID: 385
		// (get) Token: 0x060008D7 RID: 2263 RVA: 0x000292FE File Offset: 0x000274FE
		// (set) Token: 0x060008D8 RID: 2264 RVA: 0x00029306 File Offset: 0x00027506
		public string PublicId
		{
			get
			{
				return this._pubId;
			}
			set
			{
				this._pubId = ((value == null) ? string.Empty : value);
			}
		}

		/// <summary>Gets or sets the system identifier.</summary>
		/// <returns>The system identifier.</returns>
		// Token: 0x17000182 RID: 386
		// (get) Token: 0x060008D9 RID: 2265 RVA: 0x00029319 File Offset: 0x00027519
		// (set) Token: 0x060008DA RID: 2266 RVA: 0x00029321 File Offset: 0x00027521
		public string SystemId
		{
			get
			{
				return this._sysId;
			}
			set
			{
				this._sysId = ((value == null) ? string.Empty : value);
			}
		}

		/// <summary>Gets or sets the base URI.</summary>
		/// <returns>The base URI to use to resolve the DTD file.</returns>
		// Token: 0x17000183 RID: 387
		// (get) Token: 0x060008DB RID: 2267 RVA: 0x00029334 File Offset: 0x00027534
		// (set) Token: 0x060008DC RID: 2268 RVA: 0x0002933C File Offset: 0x0002753C
		public string BaseURI
		{
			get
			{
				return this._baseURI;
			}
			set
			{
				this._baseURI = ((value == null) ? string.Empty : value);
			}
		}

		/// <summary>Gets or sets the internal DTD subset.</summary>
		/// <returns>The internal DTD subset. For example, this property returns everything between the square brackets &lt;!DOCTYPE doc [...]&gt;.</returns>
		// Token: 0x17000184 RID: 388
		// (get) Token: 0x060008DD RID: 2269 RVA: 0x0002934F File Offset: 0x0002754F
		// (set) Token: 0x060008DE RID: 2270 RVA: 0x00029357 File Offset: 0x00027557
		public string InternalSubset
		{
			get
			{
				return this._internalSubset;
			}
			set
			{
				this._internalSubset = ((value == null) ? string.Empty : value);
			}
		}

		/// <summary>Gets or sets the current <see langword="xml:lang" /> scope.</summary>
		/// <returns>The current <see langword="xml:lang" /> scope. If there is no <see langword="xml:lang" /> in scope, <see langword="String.Empty" /> is returned.</returns>
		// Token: 0x17000185 RID: 389
		// (get) Token: 0x060008DF RID: 2271 RVA: 0x0002936A File Offset: 0x0002756A
		// (set) Token: 0x060008E0 RID: 2272 RVA: 0x00029372 File Offset: 0x00027572
		public string XmlLang
		{
			get
			{
				return this._xmlLang;
			}
			set
			{
				this._xmlLang = ((value == null) ? string.Empty : value);
			}
		}

		/// <summary>Gets or sets the current <see langword="xml:space" /> scope.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlSpace" /> value indicating the <see langword="xml:space" /> scope.</returns>
		// Token: 0x17000186 RID: 390
		// (get) Token: 0x060008E1 RID: 2273 RVA: 0x00029385 File Offset: 0x00027585
		// (set) Token: 0x060008E2 RID: 2274 RVA: 0x0002938D File Offset: 0x0002758D
		public XmlSpace XmlSpace
		{
			get
			{
				return this._xmlSpace;
			}
			set
			{
				this._xmlSpace = value;
			}
		}

		/// <summary>Gets or sets the encoding type.</summary>
		/// <returns>An <see cref="T:System.Text.Encoding" /> object indicating the encoding type.</returns>
		// Token: 0x17000187 RID: 391
		// (get) Token: 0x060008E3 RID: 2275 RVA: 0x00029396 File Offset: 0x00027596
		// (set) Token: 0x060008E4 RID: 2276 RVA: 0x0002939E File Offset: 0x0002759E
		public Encoding Encoding
		{
			get
			{
				return this._encoding;
			}
			set
			{
				this._encoding = value;
			}
		}

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x060008E5 RID: 2277 RVA: 0x000293A7 File Offset: 0x000275A7
		internal bool HasDtdInfo
		{
			get
			{
				return this._internalSubset != string.Empty || this._pubId != string.Empty || this._sysId != string.Empty;
			}
		}

		// Token: 0x04000554 RID: 1364
		private XmlNameTable _nt;

		// Token: 0x04000555 RID: 1365
		private XmlNamespaceManager _nsMgr;

		// Token: 0x04000556 RID: 1366
		private string _docTypeName = string.Empty;

		// Token: 0x04000557 RID: 1367
		private string _pubId = string.Empty;

		// Token: 0x04000558 RID: 1368
		private string _sysId = string.Empty;

		// Token: 0x04000559 RID: 1369
		private string _internalSubset = string.Empty;

		// Token: 0x0400055A RID: 1370
		private string _xmlLang = string.Empty;

		// Token: 0x0400055B RID: 1371
		private XmlSpace _xmlSpace;

		// Token: 0x0400055C RID: 1372
		private string _baseURI = string.Empty;

		// Token: 0x0400055D RID: 1373
		private Encoding _encoding;
	}
}
