﻿using System;

namespace System.Xml
{
	/// <summary>Specifies whether to remove duplicate namespace declarations in the <see cref="T:System.Xml.XmlWriter" />. </summary>
	// Token: 0x020000AC RID: 172
	[Flags]
	public enum NamespaceHandling
	{
		/// <summary>Specifies that duplicate namespace declarations will not be removed.</summary>
		// Token: 0x04000337 RID: 823
		Default = 0,
		/// <summary>Specifies that duplicate namespace declarations will be removed. For the duplicate namespace to be removed, the prefix and the namespace must match.</summary>
		// Token: 0x04000338 RID: 824
		OmitDuplicates = 1
	}
}
