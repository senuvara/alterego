﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	/// <summary>Represents the text content of an element or attribute.</summary>
	// Token: 0x02000242 RID: 578
	public class XmlText : XmlCharacterData
	{
		// Token: 0x0600164A RID: 5706 RVA: 0x0007BBA6 File Offset: 0x00079DA6
		internal XmlText(string strData) : this(strData, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.XmlText" /> class.</summary>
		/// <param name="strData">The content of the node; see the <see cref="P:System.Xml.XmlText.Value" /> property.</param>
		/// <param name="doc">The parent XML document.</param>
		// Token: 0x0600164B RID: 5707 RVA: 0x0007266F File Offset: 0x0007086F
		protected internal XmlText(string strData, XmlDocument doc) : base(strData, doc)
		{
		}

		/// <summary>Gets the qualified name of the node.</summary>
		/// <returns>For text nodes, this property returns <see langword="#text" />.</returns>
		// Token: 0x1700046F RID: 1135
		// (get) Token: 0x0600164C RID: 5708 RVA: 0x0007BBB0 File Offset: 0x00079DB0
		public override string Name
		{
			get
			{
				return this.OwnerDocument.strTextName;
			}
		}

		/// <summary>Gets the local name of the node.</summary>
		/// <returns>For text nodes, this property returns <see langword="#text" />.</returns>
		// Token: 0x17000470 RID: 1136
		// (get) Token: 0x0600164D RID: 5709 RVA: 0x0007BBB0 File Offset: 0x00079DB0
		public override string LocalName
		{
			get
			{
				return this.OwnerDocument.strTextName;
			}
		}

		/// <summary>Gets the type of the current node.</summary>
		/// <returns>For text nodes, this value is XmlNodeType.Text.</returns>
		// Token: 0x17000471 RID: 1137
		// (get) Token: 0x0600164E RID: 5710 RVA: 0x00002408 File Offset: 0x00000608
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.Text;
			}
		}

		/// <summary>Gets the parent of this node (for nodes that can have parents).</summary>
		/// <returns>The <see langword="XmlNode" /> that is the parent of the current node. If a node has just been created and not yet added to the tree, or if it has been removed from the tree, the parent is <see langword="null" />. For all other nodes, the value returned depends on the <see cref="P:System.Xml.XmlNode.NodeType" /> of the node. The following table describes the possible return values for the <see langword="ParentNode" /> property.</returns>
		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x0600164F RID: 5711 RVA: 0x0007BBC0 File Offset: 0x00079DC0
		public override XmlNode ParentNode
		{
			get
			{
				XmlNodeType nodeType = this.parentNode.NodeType;
				if (nodeType - XmlNodeType.Text > 1)
				{
					if (nodeType == XmlNodeType.Document)
					{
						return null;
					}
					if (nodeType - XmlNodeType.Whitespace > 1)
					{
						return this.parentNode;
					}
				}
				XmlNode parentNode = this.parentNode.parentNode;
				while (parentNode.IsText)
				{
					parentNode = parentNode.parentNode;
				}
				return parentNode;
			}
		}

		/// <summary>Creates a duplicate of this node.</summary>
		/// <param name="deep">
		///       <see langword="true" /> to recursively clone the subtree under the specified node; <see langword="false" /> to clone only the node itself. </param>
		/// <returns>The cloned node.</returns>
		// Token: 0x06001650 RID: 5712 RVA: 0x0007BC14 File Offset: 0x00079E14
		public override XmlNode CloneNode(bool deep)
		{
			return this.OwnerDocument.CreateTextNode(this.Data);
		}

		/// <summary>Gets or sets the value of the node.</summary>
		/// <returns>The content of the text node.</returns>
		// Token: 0x17000473 RID: 1139
		// (get) Token: 0x06001651 RID: 5713 RVA: 0x00072724 File Offset: 0x00070924
		// (set) Token: 0x06001652 RID: 5714 RVA: 0x0007BC28 File Offset: 0x00079E28
		public override string Value
		{
			get
			{
				return this.Data;
			}
			set
			{
				this.Data = value;
				XmlNode parentNode = this.parentNode;
				if (parentNode != null && parentNode.NodeType == XmlNodeType.Attribute)
				{
					XmlUnspecifiedAttribute xmlUnspecifiedAttribute = parentNode as XmlUnspecifiedAttribute;
					if (xmlUnspecifiedAttribute != null && !xmlUnspecifiedAttribute.Specified)
					{
						xmlUnspecifiedAttribute.SetSpecified(true);
					}
				}
			}
		}

		/// <summary>Splits the node into two nodes at the specified offset, keeping both in the tree as siblings.</summary>
		/// <param name="offset">The offset at which to split the node. </param>
		/// <returns>The new node.</returns>
		// Token: 0x06001653 RID: 5715 RVA: 0x0007BC68 File Offset: 0x00079E68
		public virtual XmlText SplitText(int offset)
		{
			XmlNode parentNode = this.ParentNode;
			int length = this.Length;
			if (offset > length)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (parentNode == null)
			{
				throw new InvalidOperationException(Res.GetString("The 'Text' node is not connected in the DOM live tree. No 'SplitText' operation could be performed."));
			}
			int count = length - offset;
			string text = this.Substring(offset, count);
			this.DeleteData(offset, count);
			XmlText xmlText = this.OwnerDocument.CreateTextNode(text);
			parentNode.InsertAfter(xmlText, this);
			return xmlText;
		}

		/// <summary>Saves the node to the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="w">The <see langword="XmlWriter" /> to which you want to save. </param>
		// Token: 0x06001654 RID: 5716 RVA: 0x0007BB7E File Offset: 0x00079D7E
		public override void WriteTo(XmlWriter w)
		{
			w.WriteString(this.Data);
		}

		/// <summary>Saves all the children of the node to the specified <see cref="T:System.Xml.XmlWriter" />. <see langword="XmlText" /> nodes do not have children, so this method has no effect.</summary>
		/// <param name="w">The <see langword="XmlWriter" /> to which you want to save. </param>
		// Token: 0x06001655 RID: 5717 RVA: 0x000030EC File Offset: 0x000012EC
		public override void WriteContentTo(XmlWriter w)
		{
		}

		// Token: 0x17000474 RID: 1140
		// (get) Token: 0x06001656 RID: 5718 RVA: 0x000042A3 File Offset: 0x000024A3
		internal override XPathNodeType XPNodeType
		{
			get
			{
				return XPathNodeType.Text;
			}
		}

		// Token: 0x17000475 RID: 1141
		// (get) Token: 0x06001657 RID: 5719 RVA: 0x000033DE File Offset: 0x000015DE
		internal override bool IsText
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets the text node that immediately precedes this node.</summary>
		/// <returns>Returns <see cref="T:System.Xml.XmlNode" />.</returns>
		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x06001658 RID: 5720 RVA: 0x000726FD File Offset: 0x000708FD
		public override XmlNode PreviousText
		{
			get
			{
				if (this.parentNode.IsText)
				{
					return this.parentNode;
				}
				return null;
			}
		}
	}
}
