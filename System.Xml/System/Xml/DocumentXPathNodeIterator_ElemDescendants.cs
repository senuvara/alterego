﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x0200020D RID: 525
	internal abstract class DocumentXPathNodeIterator_ElemDescendants : XPathNodeIterator
	{
		// Token: 0x060012DC RID: 4828 RVA: 0x00070885 File Offset: 0x0006EA85
		internal DocumentXPathNodeIterator_ElemDescendants(DocumentXPathNavigator nav)
		{
			this.nav = (DocumentXPathNavigator)nav.Clone();
			this.level = 0;
			this.position = 0;
		}

		// Token: 0x060012DD RID: 4829 RVA: 0x000708AC File Offset: 0x0006EAAC
		internal DocumentXPathNodeIterator_ElemDescendants(DocumentXPathNodeIterator_ElemDescendants other)
		{
			this.nav = (DocumentXPathNavigator)other.nav.Clone();
			this.level = other.level;
			this.position = other.position;
		}

		// Token: 0x060012DE RID: 4830
		protected abstract bool Match(XmlNode node);

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x060012DF RID: 4831 RVA: 0x000708E2 File Offset: 0x0006EAE2
		public override XPathNavigator Current
		{
			get
			{
				return this.nav;
			}
		}

		// Token: 0x17000347 RID: 839
		// (get) Token: 0x060012E0 RID: 4832 RVA: 0x000708EA File Offset: 0x0006EAEA
		public override int CurrentPosition
		{
			get
			{
				return this.position;
			}
		}

		// Token: 0x060012E1 RID: 4833 RVA: 0x000708F2 File Offset: 0x0006EAF2
		protected void SetPosition(int pos)
		{
			this.position = pos;
		}

		// Token: 0x060012E2 RID: 4834 RVA: 0x000708FC File Offset: 0x0006EAFC
		public override bool MoveNext()
		{
			for (;;)
			{
				if (this.nav.MoveToFirstChild())
				{
					this.level++;
				}
				else
				{
					if (this.level == 0)
					{
						break;
					}
					while (!this.nav.MoveToNext())
					{
						this.level--;
						if (this.level == 0)
						{
							return false;
						}
						if (!this.nav.MoveToParent())
						{
							return false;
						}
					}
				}
				XmlNode xmlNode = (XmlNode)this.nav.UnderlyingObject;
				if (xmlNode.NodeType == XmlNodeType.Element && this.Match(xmlNode))
				{
					goto Block_5;
				}
			}
			return false;
			Block_5:
			this.position++;
			return true;
		}

		// Token: 0x04000D3D RID: 3389
		private DocumentXPathNavigator nav;

		// Token: 0x04000D3E RID: 3390
		private int level;

		// Token: 0x04000D3F RID: 3391
		private int position;
	}
}
