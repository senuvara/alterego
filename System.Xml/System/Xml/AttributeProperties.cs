﻿using System;

namespace System.Xml
{
	// Token: 0x020000BE RID: 190
	internal enum AttributeProperties : uint
	{
		// Token: 0x040003B5 RID: 949
		DEFAULT,
		// Token: 0x040003B6 RID: 950
		URI,
		// Token: 0x040003B7 RID: 951
		BOOLEAN,
		// Token: 0x040003B8 RID: 952
		NAME = 4U
	}
}
