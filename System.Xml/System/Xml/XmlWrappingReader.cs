﻿using System;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace System.Xml
{
	// Token: 0x020001DD RID: 477
	internal class XmlWrappingReader : XmlReader, IXmlLineInfo
	{
		// Token: 0x06001044 RID: 4164 RVA: 0x00062E3A File Offset: 0x0006103A
		internal XmlWrappingReader(XmlReader baseReader)
		{
			this.reader = baseReader;
			this.readerAsIXmlLineInfo = (baseReader as IXmlLineInfo);
		}

		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x06001045 RID: 4165 RVA: 0x00062E55 File Offset: 0x00061055
		public override XmlReaderSettings Settings
		{
			get
			{
				return this.reader.Settings;
			}
		}

		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06001046 RID: 4166 RVA: 0x00062E62 File Offset: 0x00061062
		public override XmlNodeType NodeType
		{
			get
			{
				return this.reader.NodeType;
			}
		}

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06001047 RID: 4167 RVA: 0x00062E6F File Offset: 0x0006106F
		public override string Name
		{
			get
			{
				return this.reader.Name;
			}
		}

		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06001048 RID: 4168 RVA: 0x00062E7C File Offset: 0x0006107C
		public override string LocalName
		{
			get
			{
				return this.reader.LocalName;
			}
		}

		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06001049 RID: 4169 RVA: 0x00062E89 File Offset: 0x00061089
		public override string NamespaceURI
		{
			get
			{
				return this.reader.NamespaceURI;
			}
		}

		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x0600104A RID: 4170 RVA: 0x00062E96 File Offset: 0x00061096
		public override string Prefix
		{
			get
			{
				return this.reader.Prefix;
			}
		}

		// Token: 0x170002BA RID: 698
		// (get) Token: 0x0600104B RID: 4171 RVA: 0x00062EA3 File Offset: 0x000610A3
		public override bool HasValue
		{
			get
			{
				return this.reader.HasValue;
			}
		}

		// Token: 0x170002BB RID: 699
		// (get) Token: 0x0600104C RID: 4172 RVA: 0x00062EB0 File Offset: 0x000610B0
		public override string Value
		{
			get
			{
				return this.reader.Value;
			}
		}

		// Token: 0x170002BC RID: 700
		// (get) Token: 0x0600104D RID: 4173 RVA: 0x00062EBD File Offset: 0x000610BD
		public override int Depth
		{
			get
			{
				return this.reader.Depth;
			}
		}

		// Token: 0x170002BD RID: 701
		// (get) Token: 0x0600104E RID: 4174 RVA: 0x0002D86E File Offset: 0x0002BA6E
		public override string BaseURI
		{
			get
			{
				return this.reader.BaseURI;
			}
		}

		// Token: 0x170002BE RID: 702
		// (get) Token: 0x0600104F RID: 4175 RVA: 0x0002D87B File Offset: 0x0002BA7B
		public override bool IsEmptyElement
		{
			get
			{
				return this.reader.IsEmptyElement;
			}
		}

		// Token: 0x170002BF RID: 703
		// (get) Token: 0x06001050 RID: 4176 RVA: 0x00062ECA File Offset: 0x000610CA
		public override bool IsDefault
		{
			get
			{
				return this.reader.IsDefault;
			}
		}

		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x06001051 RID: 4177 RVA: 0x00062ED7 File Offset: 0x000610D7
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.reader.XmlSpace;
			}
		}

		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06001052 RID: 4178 RVA: 0x00062EE4 File Offset: 0x000610E4
		public override string XmlLang
		{
			get
			{
				return this.reader.XmlLang;
			}
		}

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06001053 RID: 4179 RVA: 0x00062EF1 File Offset: 0x000610F1
		public override Type ValueType
		{
			get
			{
				return this.reader.ValueType;
			}
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06001054 RID: 4180 RVA: 0x00062EFE File Offset: 0x000610FE
		public override int AttributeCount
		{
			get
			{
				return this.reader.AttributeCount;
			}
		}

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06001055 RID: 4181 RVA: 0x00062F0B File Offset: 0x0006110B
		public override bool EOF
		{
			get
			{
				return this.reader.EOF;
			}
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06001056 RID: 4182 RVA: 0x00062F18 File Offset: 0x00061118
		public override ReadState ReadState
		{
			get
			{
				return this.reader.ReadState;
			}
		}

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x06001057 RID: 4183 RVA: 0x00062F25 File Offset: 0x00061125
		public override bool HasAttributes
		{
			get
			{
				return this.reader.HasAttributes;
			}
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06001058 RID: 4184 RVA: 0x0002D8C1 File Offset: 0x0002BAC1
		public override XmlNameTable NameTable
		{
			get
			{
				return this.reader.NameTable;
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06001059 RID: 4185 RVA: 0x00062F32 File Offset: 0x00061132
		public override bool CanResolveEntity
		{
			get
			{
				return this.reader.CanResolveEntity;
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x0600105A RID: 4186 RVA: 0x00062F3F File Offset: 0x0006113F
		public override IXmlSchemaInfo SchemaInfo
		{
			get
			{
				return this.reader.SchemaInfo;
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x0600105B RID: 4187 RVA: 0x00062F4C File Offset: 0x0006114C
		public override char QuoteChar
		{
			get
			{
				return this.reader.QuoteChar;
			}
		}

		// Token: 0x0600105C RID: 4188 RVA: 0x00062F59 File Offset: 0x00061159
		public override string GetAttribute(string name)
		{
			return this.reader.GetAttribute(name);
		}

		// Token: 0x0600105D RID: 4189 RVA: 0x00062F67 File Offset: 0x00061167
		public override string GetAttribute(string name, string namespaceURI)
		{
			return this.reader.GetAttribute(name, namespaceURI);
		}

		// Token: 0x0600105E RID: 4190 RVA: 0x00062F76 File Offset: 0x00061176
		public override string GetAttribute(int i)
		{
			return this.reader.GetAttribute(i);
		}

		// Token: 0x0600105F RID: 4191 RVA: 0x00062F84 File Offset: 0x00061184
		public override bool MoveToAttribute(string name)
		{
			return this.reader.MoveToAttribute(name);
		}

		// Token: 0x06001060 RID: 4192 RVA: 0x00062F92 File Offset: 0x00061192
		public override bool MoveToAttribute(string name, string ns)
		{
			return this.reader.MoveToAttribute(name, ns);
		}

		// Token: 0x06001061 RID: 4193 RVA: 0x00062FA1 File Offset: 0x000611A1
		public override void MoveToAttribute(int i)
		{
			this.reader.MoveToAttribute(i);
		}

		// Token: 0x06001062 RID: 4194 RVA: 0x00062FAF File Offset: 0x000611AF
		public override bool MoveToFirstAttribute()
		{
			return this.reader.MoveToFirstAttribute();
		}

		// Token: 0x06001063 RID: 4195 RVA: 0x00062FBC File Offset: 0x000611BC
		public override bool MoveToNextAttribute()
		{
			return this.reader.MoveToNextAttribute();
		}

		// Token: 0x06001064 RID: 4196 RVA: 0x00062FC9 File Offset: 0x000611C9
		public override bool MoveToElement()
		{
			return this.reader.MoveToElement();
		}

		// Token: 0x06001065 RID: 4197 RVA: 0x00062FD6 File Offset: 0x000611D6
		public override bool Read()
		{
			return this.reader.Read();
		}

		// Token: 0x06001066 RID: 4198 RVA: 0x00062FE3 File Offset: 0x000611E3
		public override void Close()
		{
			this.reader.Close();
		}

		// Token: 0x06001067 RID: 4199 RVA: 0x00062FF0 File Offset: 0x000611F0
		public override void Skip()
		{
			this.reader.Skip();
		}

		// Token: 0x06001068 RID: 4200 RVA: 0x00062FFD File Offset: 0x000611FD
		public override string LookupNamespace(string prefix)
		{
			return this.reader.LookupNamespace(prefix);
		}

		// Token: 0x06001069 RID: 4201 RVA: 0x0006300B File Offset: 0x0006120B
		public override void ResolveEntity()
		{
			this.reader.ResolveEntity();
		}

		// Token: 0x0600106A RID: 4202 RVA: 0x00063018 File Offset: 0x00061218
		public override bool ReadAttributeValue()
		{
			return this.reader.ReadAttributeValue();
		}

		// Token: 0x0600106B RID: 4203 RVA: 0x00063025 File Offset: 0x00061225
		public virtual bool HasLineInfo()
		{
			return this.readerAsIXmlLineInfo != null && this.readerAsIXmlLineInfo.HasLineInfo();
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x0600106C RID: 4204 RVA: 0x0006303C File Offset: 0x0006123C
		public virtual int LineNumber
		{
			get
			{
				if (this.readerAsIXmlLineInfo != null)
				{
					return this.readerAsIXmlLineInfo.LineNumber;
				}
				return 0;
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x0600106D RID: 4205 RVA: 0x00063053 File Offset: 0x00061253
		public virtual int LinePosition
		{
			get
			{
				if (this.readerAsIXmlLineInfo != null)
				{
					return this.readerAsIXmlLineInfo.LinePosition;
				}
				return 0;
			}
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x0600106E RID: 4206 RVA: 0x0006306A File Offset: 0x0006126A
		internal override IDtdInfo DtdInfo
		{
			get
			{
				return this.reader.DtdInfo;
			}
		}

		// Token: 0x0600106F RID: 4207 RVA: 0x00063077 File Offset: 0x00061277
		public override Task<string> GetValueAsync()
		{
			return this.reader.GetValueAsync();
		}

		// Token: 0x06001070 RID: 4208 RVA: 0x00063084 File Offset: 0x00061284
		public override Task<bool> ReadAsync()
		{
			return this.reader.ReadAsync();
		}

		// Token: 0x06001071 RID: 4209 RVA: 0x00063091 File Offset: 0x00061291
		public override Task SkipAsync()
		{
			return this.reader.SkipAsync();
		}

		// Token: 0x04000BE0 RID: 3040
		protected XmlReader reader;

		// Token: 0x04000BE1 RID: 3041
		protected IXmlLineInfo readerAsIXmlLineInfo;
	}
}
