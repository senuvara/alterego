﻿using System;

namespace System.Xml
{
	// Token: 0x02000243 RID: 579
	internal class XmlUnspecifiedAttribute : XmlAttribute
	{
		// Token: 0x06001659 RID: 5721 RVA: 0x0007BCCF File Offset: 0x00079ECF
		protected internal XmlUnspecifiedAttribute(string prefix, string localName, string namespaceURI, XmlDocument doc) : base(prefix, localName, namespaceURI, doc)
		{
		}

		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x0600165A RID: 5722 RVA: 0x0007BCDC File Offset: 0x00079EDC
		public override bool Specified
		{
			get
			{
				return this.fSpecified;
			}
		}

		// Token: 0x0600165B RID: 5723 RVA: 0x0007BCE4 File Offset: 0x00079EE4
		public override XmlNode CloneNode(bool deep)
		{
			XmlDocument ownerDocument = this.OwnerDocument;
			XmlUnspecifiedAttribute xmlUnspecifiedAttribute = (XmlUnspecifiedAttribute)ownerDocument.CreateDefaultAttribute(this.Prefix, this.LocalName, this.NamespaceURI);
			xmlUnspecifiedAttribute.CopyChildren(ownerDocument, this, true);
			xmlUnspecifiedAttribute.fSpecified = true;
			return xmlUnspecifiedAttribute;
		}

		// Token: 0x17000478 RID: 1144
		// (set) Token: 0x0600165C RID: 5724 RVA: 0x0007BD25 File Offset: 0x00079F25
		public override string InnerText
		{
			set
			{
				base.InnerText = value;
				this.fSpecified = true;
			}
		}

		// Token: 0x0600165D RID: 5725 RVA: 0x0007BD35 File Offset: 0x00079F35
		public override XmlNode InsertBefore(XmlNode newChild, XmlNode refChild)
		{
			XmlNode result = base.InsertBefore(newChild, refChild);
			this.fSpecified = true;
			return result;
		}

		// Token: 0x0600165E RID: 5726 RVA: 0x0007BD46 File Offset: 0x00079F46
		public override XmlNode InsertAfter(XmlNode newChild, XmlNode refChild)
		{
			XmlNode result = base.InsertAfter(newChild, refChild);
			this.fSpecified = true;
			return result;
		}

		// Token: 0x0600165F RID: 5727 RVA: 0x0007BD57 File Offset: 0x00079F57
		public override XmlNode ReplaceChild(XmlNode newChild, XmlNode oldChild)
		{
			XmlNode result = base.ReplaceChild(newChild, oldChild);
			this.fSpecified = true;
			return result;
		}

		// Token: 0x06001660 RID: 5728 RVA: 0x0007BD68 File Offset: 0x00079F68
		public override XmlNode RemoveChild(XmlNode oldChild)
		{
			XmlNode result = base.RemoveChild(oldChild);
			this.fSpecified = true;
			return result;
		}

		// Token: 0x06001661 RID: 5729 RVA: 0x0007BD78 File Offset: 0x00079F78
		public override XmlNode AppendChild(XmlNode newChild)
		{
			XmlNode result = base.AppendChild(newChild);
			this.fSpecified = true;
			return result;
		}

		// Token: 0x06001662 RID: 5730 RVA: 0x0007BD88 File Offset: 0x00079F88
		public override void WriteTo(XmlWriter w)
		{
			if (this.fSpecified)
			{
				base.WriteTo(w);
			}
		}

		// Token: 0x06001663 RID: 5731 RVA: 0x0007BD99 File Offset: 0x00079F99
		internal void SetSpecified(bool f)
		{
			this.fSpecified = f;
		}

		// Token: 0x04000E26 RID: 3622
		private bool fSpecified;
	}
}
