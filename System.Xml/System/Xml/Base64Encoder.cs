﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Xml
{
	// Token: 0x0200007B RID: 123
	internal abstract class Base64Encoder
	{
		// Token: 0x060003A2 RID: 930 RVA: 0x0000DF43 File Offset: 0x0000C143
		internal Base64Encoder()
		{
			this.charsLine = new char[76];
		}

		// Token: 0x060003A3 RID: 931
		internal abstract void WriteChars(char[] chars, int index, int count);

		// Token: 0x060003A4 RID: 932 RVA: 0x0000DF58 File Offset: 0x0000C158
		internal void Encode(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (count > buffer.Length - index)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.leftOverBytesCount > 0)
			{
				int num = this.leftOverBytesCount;
				while (num < 3 && count > 0)
				{
					this.leftOverBytes[num++] = buffer[index++];
					count--;
				}
				if (count == 0 && num < 3)
				{
					this.leftOverBytesCount = num;
					return;
				}
				int count2 = Convert.ToBase64CharArray(this.leftOverBytes, 0, 3, this.charsLine, 0);
				this.WriteChars(this.charsLine, 0, count2);
			}
			this.leftOverBytesCount = count % 3;
			if (this.leftOverBytesCount > 0)
			{
				count -= this.leftOverBytesCount;
				if (this.leftOverBytes == null)
				{
					this.leftOverBytes = new byte[3];
				}
				for (int i = 0; i < this.leftOverBytesCount; i++)
				{
					this.leftOverBytes[i] = buffer[index + count + i];
				}
			}
			int num2 = index + count;
			int num3 = 57;
			while (index < num2)
			{
				if (index + num3 > num2)
				{
					num3 = num2 - index;
				}
				int count3 = Convert.ToBase64CharArray(buffer, index, num3, this.charsLine, 0);
				this.WriteChars(this.charsLine, 0, count3);
				index += num3;
			}
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x0000E09C File Offset: 0x0000C29C
		internal void Flush()
		{
			if (this.leftOverBytesCount > 0)
			{
				int count = Convert.ToBase64CharArray(this.leftOverBytes, 0, this.leftOverBytesCount, this.charsLine, 0);
				this.WriteChars(this.charsLine, 0, count);
				this.leftOverBytesCount = 0;
			}
		}

		// Token: 0x060003A6 RID: 934
		internal abstract Task WriteCharsAsync(char[] chars, int index, int count);

		// Token: 0x060003A7 RID: 935 RVA: 0x0000E0E4 File Offset: 0x0000C2E4
		internal async Task EncodeAsync(byte[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (count > buffer.Length - index)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (this.leftOverBytesCount > 0)
			{
				int num = this.leftOverBytesCount;
				while (num < 3 && count > 0)
				{
					byte[] array = this.leftOverBytes;
					int num2 = num++;
					int num3 = index;
					index = num3 + 1;
					array[num2] = buffer[num3];
					num3 = count;
					count = num3 - 1;
				}
				if (count == 0 && num < 3)
				{
					this.leftOverBytesCount = num;
					return;
				}
				int count2 = Convert.ToBase64CharArray(this.leftOverBytes, 0, 3, this.charsLine, 0);
				await this.WriteCharsAsync(this.charsLine, 0, count2).ConfigureAwait(false);
			}
			this.leftOverBytesCount = count % 3;
			if (this.leftOverBytesCount > 0)
			{
				count -= this.leftOverBytesCount;
				if (this.leftOverBytes == null)
				{
					this.leftOverBytes = new byte[3];
				}
				for (int i = 0; i < this.leftOverBytesCount; i++)
				{
					this.leftOverBytes[i] = buffer[index + count + i];
				}
			}
			int endIndex = index + count;
			int chunkSize = 57;
			while (index < endIndex)
			{
				if (index + chunkSize > endIndex)
				{
					chunkSize = endIndex - index;
				}
				int count3 = Convert.ToBase64CharArray(buffer, index, chunkSize, this.charsLine, 0);
				await this.WriteCharsAsync(this.charsLine, 0, count3).ConfigureAwait(false);
				index += chunkSize;
			}
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x0000E144 File Offset: 0x0000C344
		internal async Task FlushAsync()
		{
			if (this.leftOverBytesCount > 0)
			{
				int count = Convert.ToBase64CharArray(this.leftOverBytes, 0, this.leftOverBytesCount, this.charsLine, 0);
				await this.WriteCharsAsync(this.charsLine, 0, count).ConfigureAwait(false);
				this.leftOverBytesCount = 0;
			}
		}

		// Token: 0x04000214 RID: 532
		private byte[] leftOverBytes;

		// Token: 0x04000215 RID: 533
		private int leftOverBytesCount;

		// Token: 0x04000216 RID: 534
		private char[] charsLine;

		// Token: 0x04000217 RID: 535
		internal const int Base64LineSize = 76;

		// Token: 0x04000218 RID: 536
		internal const int LineSizeInBytes = 57;

		// Token: 0x0200007C RID: 124
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <EncodeAsync>d__10 : IAsyncStateMachine
		{
			// Token: 0x060003A9 RID: 937 RVA: 0x0000E18C File Offset: 0x0000C38C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				Base64Encoder base64Encoder = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_2B2;
						}
						if (buffer == null)
						{
							throw new ArgumentNullException("buffer");
						}
						if (index < 0)
						{
							throw new ArgumentOutOfRangeException("index");
						}
						if (count < 0)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (count > buffer.Length - index)
						{
							throw new ArgumentOutOfRangeException("count");
						}
						if (base64Encoder.leftOverBytesCount <= 0)
						{
							goto IL_170;
						}
						int leftOverBytesCount = base64Encoder.leftOverBytesCount;
						while (leftOverBytesCount < 3 && count > 0)
						{
							byte[] leftOverBytes = base64Encoder.leftOverBytes;
							int num3 = leftOverBytesCount++;
							byte[] array = buffer;
							int num4 = index;
							index = num4 + 1;
							leftOverBytes[num3] = array[num4];
							num4 = count;
							count = num4 - 1;
						}
						if (count == 0 && leftOverBytesCount < 3)
						{
							base64Encoder.leftOverBytesCount = leftOverBytesCount;
							goto IL_2F8;
						}
						int num5 = Convert.ToBase64CharArray(base64Encoder.leftOverBytes, 0, 3, base64Encoder.charsLine, 0);
						configuredTaskAwaiter = base64Encoder.WriteCharsAsync(base64Encoder.charsLine, 0, num5).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, Base64Encoder.<EncodeAsync>d__10>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					IL_170:
					base64Encoder.leftOverBytesCount = count % 3;
					if (base64Encoder.leftOverBytesCount > 0)
					{
						count -= base64Encoder.leftOverBytesCount;
						if (base64Encoder.leftOverBytes == null)
						{
							base64Encoder.leftOverBytes = new byte[3];
						}
						for (int i = 0; i < base64Encoder.leftOverBytesCount; i++)
						{
							base64Encoder.leftOverBytes[i] = buffer[index + count + i];
						}
					}
					endIndex = index + count;
					chunkSize = 57;
					goto IL_2CC;
					IL_2B2:
					configuredTaskAwaiter.GetResult();
					index += chunkSize;
					IL_2CC:
					if (index < endIndex)
					{
						if (index + chunkSize > endIndex)
						{
							chunkSize = endIndex - index;
						}
						int num6 = Convert.ToBase64CharArray(buffer, index, chunkSize, base64Encoder.charsLine, 0);
						configuredTaskAwaiter = base64Encoder.WriteCharsAsync(base64Encoder.charsLine, 0, num6).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, Base64Encoder.<EncodeAsync>d__10>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_2B2;
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_2F8:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x060003AA RID: 938 RVA: 0x0000E4C0 File Offset: 0x0000C6C0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000219 RID: 537
			public int <>1__state;

			// Token: 0x0400021A RID: 538
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x0400021B RID: 539
			public byte[] buffer;

			// Token: 0x0400021C RID: 540
			public int index;

			// Token: 0x0400021D RID: 541
			public int count;

			// Token: 0x0400021E RID: 542
			public Base64Encoder <>4__this;

			// Token: 0x0400021F RID: 543
			private int <chunkSize>5__1;

			// Token: 0x04000220 RID: 544
			private int <endIndex>5__2;

			// Token: 0x04000221 RID: 545
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200007D RID: 125
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FlushAsync>d__11 : IAsyncStateMachine
		{
			// Token: 0x060003AB RID: 939 RVA: 0x0000E4D0 File Offset: 0x0000C6D0
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				Base64Encoder base64Encoder = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (base64Encoder.leftOverBytesCount <= 0)
						{
							goto IL_A8;
						}
						int count = Convert.ToBase64CharArray(base64Encoder.leftOverBytes, 0, base64Encoder.leftOverBytesCount, base64Encoder.charsLine, 0);
						configuredTaskAwaiter = base64Encoder.WriteCharsAsync(base64Encoder.charsLine, 0, count).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, Base64Encoder.<FlushAsync>d__11>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					base64Encoder.leftOverBytesCount = 0;
					IL_A8:;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x060003AC RID: 940 RVA: 0x0000E5C4 File Offset: 0x0000C7C4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04000222 RID: 546
			public int <>1__state;

			// Token: 0x04000223 RID: 547
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04000224 RID: 548
			public Base64Encoder <>4__this;

			// Token: 0x04000225 RID: 549
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
