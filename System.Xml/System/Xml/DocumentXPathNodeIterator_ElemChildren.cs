﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x02000212 RID: 530
	internal class DocumentXPathNodeIterator_ElemChildren : DocumentXPathNodeIterator_ElemDescendants
	{
		// Token: 0x060012F3 RID: 4851 RVA: 0x00070ACC File Offset: 0x0006ECCC
		internal DocumentXPathNodeIterator_ElemChildren(DocumentXPathNavigator nav, string localNameAtom, string nsAtom) : base(nav)
		{
			this.localNameAtom = localNameAtom;
			this.nsAtom = nsAtom;
		}

		// Token: 0x060012F4 RID: 4852 RVA: 0x00070AE3 File Offset: 0x0006ECE3
		internal DocumentXPathNodeIterator_ElemChildren(DocumentXPathNodeIterator_ElemChildren other) : base(other)
		{
			this.localNameAtom = other.localNameAtom;
			this.nsAtom = other.nsAtom;
		}

		// Token: 0x060012F5 RID: 4853 RVA: 0x00070B04 File Offset: 0x0006ED04
		public override XPathNodeIterator Clone()
		{
			return new DocumentXPathNodeIterator_ElemChildren(this);
		}

		// Token: 0x060012F6 RID: 4854 RVA: 0x00070B0C File Offset: 0x0006ED0C
		protected override bool Match(XmlNode node)
		{
			return Ref.Equal(node.LocalName, this.localNameAtom) && Ref.Equal(node.NamespaceURI, this.nsAtom);
		}

		// Token: 0x04000D41 RID: 3393
		protected string localNameAtom;

		// Token: 0x04000D42 RID: 3394
		protected string nsAtom;
	}
}
