﻿using System;
using System.Diagnostics;

namespace System.Xml
{
	// Token: 0x02000209 RID: 521
	internal static class DiagnosticsSwitches
	{
		// Token: 0x17000329 RID: 809
		// (get) Token: 0x0600125B RID: 4699 RVA: 0x0006D632 File Offset: 0x0006B832
		public static BooleanSwitch XmlSchemaContentModel
		{
			get
			{
				if (DiagnosticsSwitches.xmlSchemaContentModel == null)
				{
					DiagnosticsSwitches.xmlSchemaContentModel = new BooleanSwitch("XmlSchemaContentModel", "Enable tracing for the XmlSchema content model.");
				}
				return DiagnosticsSwitches.xmlSchemaContentModel;
			}
		}

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x0600125C RID: 4700 RVA: 0x0006D65A File Offset: 0x0006B85A
		public static TraceSwitch XmlSchema
		{
			get
			{
				if (DiagnosticsSwitches.xmlSchema == null)
				{
					DiagnosticsSwitches.xmlSchema = new TraceSwitch("XmlSchema", "Enable tracing for the XmlSchema class.");
				}
				return DiagnosticsSwitches.xmlSchema;
			}
		}

		// Token: 0x1700032B RID: 811
		// (get) Token: 0x0600125D RID: 4701 RVA: 0x0006D682 File Offset: 0x0006B882
		public static BooleanSwitch KeepTempFiles
		{
			get
			{
				if (DiagnosticsSwitches.keepTempFiles == null)
				{
					DiagnosticsSwitches.keepTempFiles = new BooleanSwitch("XmlSerialization.Compilation", "Keep XmlSerialization generated (temp) files.");
				}
				return DiagnosticsSwitches.keepTempFiles;
			}
		}

		// Token: 0x1700032C RID: 812
		// (get) Token: 0x0600125E RID: 4702 RVA: 0x0006D6AA File Offset: 0x0006B8AA
		public static BooleanSwitch PregenEventLog
		{
			get
			{
				if (DiagnosticsSwitches.pregenEventLog == null)
				{
					DiagnosticsSwitches.pregenEventLog = new BooleanSwitch("XmlSerialization.PregenEventLog", "Log failures while loading pre-generated XmlSerialization assembly.");
				}
				return DiagnosticsSwitches.pregenEventLog;
			}
		}

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x0600125F RID: 4703 RVA: 0x0006D6D2 File Offset: 0x0006B8D2
		public static TraceSwitch XmlSerialization
		{
			get
			{
				if (DiagnosticsSwitches.xmlSerialization == null)
				{
					DiagnosticsSwitches.xmlSerialization = new TraceSwitch("XmlSerialization", "Enable tracing for the System.Xml.Serialization component.");
				}
				return DiagnosticsSwitches.xmlSerialization;
			}
		}

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06001260 RID: 4704 RVA: 0x0006D6FA File Offset: 0x0006B8FA
		public static TraceSwitch XslTypeInference
		{
			get
			{
				if (DiagnosticsSwitches.xslTypeInference == null)
				{
					DiagnosticsSwitches.xslTypeInference = new TraceSwitch("XslTypeInference", "Enable tracing for the XSLT type inference algorithm.");
				}
				return DiagnosticsSwitches.xslTypeInference;
			}
		}

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06001261 RID: 4705 RVA: 0x0006D722 File Offset: 0x0006B922
		public static BooleanSwitch NonRecursiveTypeLoading
		{
			get
			{
				if (DiagnosticsSwitches.nonRecursiveTypeLoading == null)
				{
					DiagnosticsSwitches.nonRecursiveTypeLoading = new BooleanSwitch("XmlSerialization.NonRecursiveTypeLoading", "Turn on non-recursive algorithm generating XmlMappings for CLR types.");
				}
				return DiagnosticsSwitches.nonRecursiveTypeLoading;
			}
		}

		// Token: 0x04000D1C RID: 3356
		private static volatile BooleanSwitch xmlSchemaContentModel;

		// Token: 0x04000D1D RID: 3357
		private static volatile TraceSwitch xmlSchema;

		// Token: 0x04000D1E RID: 3358
		private static volatile BooleanSwitch keepTempFiles;

		// Token: 0x04000D1F RID: 3359
		private static volatile BooleanSwitch pregenEventLog;

		// Token: 0x04000D20 RID: 3360
		private static volatile TraceSwitch xmlSerialization;

		// Token: 0x04000D21 RID: 3361
		private static volatile TraceSwitch xslTypeInference;

		// Token: 0x04000D22 RID: 3362
		private static volatile BooleanSwitch nonRecursiveTypeLoading;
	}
}
