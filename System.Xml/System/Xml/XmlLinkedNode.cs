﻿using System;

namespace System.Xml
{
	/// <summary>Gets the node immediately preceding or following this node.</summary>
	// Token: 0x02000230 RID: 560
	public abstract class XmlLinkedNode : XmlNode
	{
		// Token: 0x060014F1 RID: 5361 RVA: 0x000760B7 File Offset: 0x000742B7
		internal XmlLinkedNode()
		{
			this.next = null;
		}

		// Token: 0x060014F2 RID: 5362 RVA: 0x000760C6 File Offset: 0x000742C6
		internal XmlLinkedNode(XmlDocument doc) : base(doc)
		{
			this.next = null;
		}

		/// <summary>Gets the node immediately preceding this node.</summary>
		/// <returns>The preceding <see cref="T:System.Xml.XmlNode" /> or <see langword="null" /> if one does not exist.</returns>
		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x060014F3 RID: 5363 RVA: 0x000760D8 File Offset: 0x000742D8
		public override XmlNode PreviousSibling
		{
			get
			{
				XmlNode parentNode = this.ParentNode;
				if (parentNode != null)
				{
					XmlNode xmlNode;
					XmlNode nextSibling;
					for (xmlNode = parentNode.FirstChild; xmlNode != null; xmlNode = nextSibling)
					{
						nextSibling = xmlNode.NextSibling;
						if (nextSibling == this)
						{
							break;
						}
					}
					return xmlNode;
				}
				return null;
			}
		}

		/// <summary>Gets the node immediately following this node.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> immediately following this node or <see langword="null" /> if one does not exist.</returns>
		// Token: 0x170003EA RID: 1002
		// (get) Token: 0x060014F4 RID: 5364 RVA: 0x0007610C File Offset: 0x0007430C
		public override XmlNode NextSibling
		{
			get
			{
				XmlNode parentNode = this.ParentNode;
				if (parentNode != null && this.next != parentNode.FirstChild)
				{
					return this.next;
				}
				return null;
			}
		}

		// Token: 0x04000DE1 RID: 3553
		internal XmlLinkedNode next;
	}
}
