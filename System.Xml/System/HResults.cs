﻿using System;

namespace System
{
	// Token: 0x02000071 RID: 113
	internal static class HResults
	{
		// Token: 0x040001BD RID: 445
		internal const int Configuration = -2146232062;

		// Token: 0x040001BE RID: 446
		internal const int Xml = -2146232000;

		// Token: 0x040001BF RID: 447
		internal const int XmlSchema = -2146231999;

		// Token: 0x040001C0 RID: 448
		internal const int XmlXslt = -2146231998;

		// Token: 0x040001C1 RID: 449
		internal const int XmlXPath = -2146231997;

		// Token: 0x040001C2 RID: 450
		internal const int Data = -2146232032;

		// Token: 0x040001C3 RID: 451
		internal const int DataDeletedRowInaccessible = -2146232031;

		// Token: 0x040001C4 RID: 452
		internal const int DataDuplicateName = -2146232030;

		// Token: 0x040001C5 RID: 453
		internal const int DataInRowChangingEvent = -2146232029;

		// Token: 0x040001C6 RID: 454
		internal const int DataInvalidConstraint = -2146232028;

		// Token: 0x040001C7 RID: 455
		internal const int DataMissingPrimaryKey = -2146232027;

		// Token: 0x040001C8 RID: 456
		internal const int DataNoNullAllowed = -2146232026;

		// Token: 0x040001C9 RID: 457
		internal const int DataReadOnly = -2146232025;

		// Token: 0x040001CA RID: 458
		internal const int DataRowNotInTable = -2146232024;

		// Token: 0x040001CB RID: 459
		internal const int DataVersionNotFound = -2146232023;

		// Token: 0x040001CC RID: 460
		internal const int DataConstraint = -2146232022;

		// Token: 0x040001CD RID: 461
		internal const int StrongTyping = -2146232021;

		// Token: 0x040001CE RID: 462
		internal const int SqlType = -2146232016;

		// Token: 0x040001CF RID: 463
		internal const int SqlNullValue = -2146232015;

		// Token: 0x040001D0 RID: 464
		internal const int SqlTruncate = -2146232014;

		// Token: 0x040001D1 RID: 465
		internal const int AdapterMapping = -2146232013;

		// Token: 0x040001D2 RID: 466
		internal const int DataAdapter = -2146232012;

		// Token: 0x040001D3 RID: 467
		internal const int DBConcurrency = -2146232011;

		// Token: 0x040001D4 RID: 468
		internal const int OperationAborted = -2146232010;

		// Token: 0x040001D5 RID: 469
		internal const int InvalidUdt = -2146232009;

		// Token: 0x040001D6 RID: 470
		internal const int Metadata = -2146232007;

		// Token: 0x040001D7 RID: 471
		internal const int InvalidQuery = -2146232006;

		// Token: 0x040001D8 RID: 472
		internal const int CommandCompilation = -2146232005;

		// Token: 0x040001D9 RID: 473
		internal const int CommandExecution = -2146232004;

		// Token: 0x040001DA RID: 474
		internal const int SqlException = -2146232060;

		// Token: 0x040001DB RID: 475
		internal const int OdbcException = -2146232009;

		// Token: 0x040001DC RID: 476
		internal const int OracleException = -2146232008;

		// Token: 0x040001DD RID: 477
		internal const int ConnectionPlanException = -2146232003;

		// Token: 0x040001DE RID: 478
		internal const int NteBadKeySet = -2146893802;

		// Token: 0x040001DF RID: 479
		internal const int Win32AccessDenied = -2147024891;

		// Token: 0x040001E0 RID: 480
		internal const int Win32InvalidHandle = -2147024890;

		// Token: 0x040001E1 RID: 481
		internal const int License = -2146232063;

		// Token: 0x040001E2 RID: 482
		internal const int InternalBufferOverflow = -2146232059;

		// Token: 0x040001E3 RID: 483
		internal const int ServiceControllerTimeout = -2146232058;

		// Token: 0x040001E4 RID: 484
		internal const int Install = -2146232057;

		// Token: 0x040001E5 RID: 485
		internal const int EFail = -2147467259;
	}
}
