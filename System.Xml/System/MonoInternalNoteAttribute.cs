﻿using System;

namespace System
{
	// Token: 0x0200006E RID: 110
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoInternalNoteAttribute : MonoTODOAttribute
	{
		// Token: 0x0600037B RID: 891 RVA: 0x0000D3D2 File Offset: 0x0000B5D2
		public MonoInternalNoteAttribute(string comment) : base(comment)
		{
		}
	}
}
