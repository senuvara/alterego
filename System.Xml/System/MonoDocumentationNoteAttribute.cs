﻿using System;

namespace System
{
	// Token: 0x0200006C RID: 108
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoDocumentationNoteAttribute : MonoTODOAttribute
	{
		// Token: 0x06000379 RID: 889 RVA: 0x0000D3D2 File Offset: 0x0000B5D2
		public MonoDocumentationNoteAttribute(string comment) : base(comment)
		{
		}
	}
}
