﻿using System;

namespace System
{
	// Token: 0x0200006D RID: 109
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoExtensionAttribute : MonoTODOAttribute
	{
		// Token: 0x0600037A RID: 890 RVA: 0x0000D3D2 File Offset: 0x0000B5D2
		public MonoExtensionAttribute(string comment) : base(comment)
		{
		}
	}
}
