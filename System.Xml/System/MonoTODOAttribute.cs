﻿using System;

namespace System
{
	// Token: 0x0200006B RID: 107
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoTODOAttribute : Attribute
	{
		// Token: 0x06000376 RID: 886 RVA: 0x0000D3B3 File Offset: 0x0000B5B3
		public MonoTODOAttribute()
		{
		}

		// Token: 0x06000377 RID: 887 RVA: 0x0000D3BB File Offset: 0x0000B5BB
		public MonoTODOAttribute(string comment)
		{
			this.comment = comment;
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x06000378 RID: 888 RVA: 0x0000D3CA File Offset: 0x0000B5CA
		public string Comment
		{
			get
			{
				return this.comment;
			}
		}

		// Token: 0x040001BC RID: 444
		private string comment;
	}
}
