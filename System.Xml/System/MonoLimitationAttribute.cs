﻿using System;

namespace System
{
	// Token: 0x0200006F RID: 111
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoLimitationAttribute : MonoTODOAttribute
	{
		// Token: 0x0600037C RID: 892 RVA: 0x0000D3D2 File Offset: 0x0000B5D2
		public MonoLimitationAttribute(string comment) : base(comment)
		{
		}
	}
}
