﻿using System;

namespace System
{
	// Token: 0x02000073 RID: 115
	internal static class LocalAppContextSwitches
	{
		// Token: 0x06000381 RID: 897 RVA: 0x000030EC File Offset: 0x000012EC
		// Note: this type is marked as 'beforefieldinit'.
		static LocalAppContextSwitches()
		{
		}

		// Token: 0x040001E8 RID: 488
		public static readonly bool IgnoreEmptyKeySequences;

		// Token: 0x040001E9 RID: 489
		public static readonly bool DontThrowOnInvalidSurrogatePairs;

		// Token: 0x040001EA RID: 490
		public static readonly bool IgnoreKindInUtcTimeSerialization;

		// Token: 0x040001EB RID: 491
		public static readonly bool EnableTimeSpanSerialization;
	}
}
