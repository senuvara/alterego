﻿using System;
using System.Collections;
using System.Globalization;

namespace System
{
	// Token: 0x02000072 RID: 114
	[Serializable]
	internal class InvariantComparer : IComparer
	{
		// Token: 0x0600037E RID: 894 RVA: 0x0000D3DB File Offset: 0x0000B5DB
		internal InvariantComparer()
		{
			this.m_compareInfo = CultureInfo.InvariantCulture.CompareInfo;
		}

		// Token: 0x0600037F RID: 895 RVA: 0x0000D3F4 File Offset: 0x0000B5F4
		public int Compare(object a, object b)
		{
			string text = a as string;
			string text2 = b as string;
			if (text != null && text2 != null)
			{
				return this.m_compareInfo.Compare(text, text2);
			}
			return Comparer.Default.Compare(a, b);
		}

		// Token: 0x06000380 RID: 896 RVA: 0x0000D42F File Offset: 0x0000B62F
		// Note: this type is marked as 'beforefieldinit'.
		static InvariantComparer()
		{
		}

		// Token: 0x040001E6 RID: 486
		private CompareInfo m_compareInfo;

		// Token: 0x040001E7 RID: 487
		internal static readonly InvariantComparer Default = new InvariantComparer();
	}
}
