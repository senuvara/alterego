﻿using System;

namespace System
{
	// Token: 0x02000070 RID: 112
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoNotSupportedAttribute : MonoTODOAttribute
	{
		// Token: 0x0600037D RID: 893 RVA: 0x0000D3D2 File Offset: 0x0000B5D2
		public MonoNotSupportedAttribute(string comment) : base(comment)
		{
		}
	}
}
