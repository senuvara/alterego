﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x0200054C RID: 1356
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x0600342D RID: 13357 RVA: 0x0010F388 File Offset: 0x0010D588
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x0400233F RID: 9023 RVA: 0x0010F4C4 File Offset: 0x0010D6C4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 0596273829C0656B9F4D263DA48C807FDF60FD27;

	// Token: 0x04002340 RID: 9024 RVA: 0x0010F4F4 File Offset: 0x0010D6F4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 069E73928E697FB3ED96AD686A80D49E961EC4F9;

	// Token: 0x04002341 RID: 9025 RVA: 0x0010F524 File Offset: 0x0010D724
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 0701435C4E2C38EFE43C51BD22C114AB8B80124D;

	// Token: 0x04002342 RID: 9026 RVA: 0x0010F530 File Offset: 0x0010D730
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=36 0EE6555EB2C89F29655BD23FAB0573D8D684331A;

	// Token: 0x04002343 RID: 9027 RVA: 0x0010F554 File Offset: 0x0010D754
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=68 0F6A1E2CEA2FA691D57F3F3FDCF9B82A3FBF6EE1;

	// Token: 0x04002344 RID: 9028 RVA: 0x0010F598 File Offset: 0x0010D798
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 161F91CE1721D8F16622810CBB39887D21C47031;

	// Token: 0x04002345 RID: 9029 RVA: 0x0010F5A4 File Offset: 0x0010D7A4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 19914D4D7E28E626EA4D28E3B3C6B28E2DCA8012;

	// Token: 0x04002346 RID: 9030 RVA: 0x0010F5D4 File Offset: 0x0010D7D4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=52 1E780CFFC737E02737AC72C1A8B5E40C49FB6F90;

	// Token: 0x04002347 RID: 9031 RVA: 0x0010F608 File Offset: 0x0010D808
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 2051D7520B96DCC12F2E4DE851CB9F203D623805;

	// Token: 0x04002348 RID: 9032 RVA: 0x0010F628 File Offset: 0x0010D828
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=44 221CE291CD044114B4369175B9B91177F5932876;

	// Token: 0x04002349 RID: 9033 RVA: 0x0010F654 File Offset: 0x0010D854
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 2693A561CE39A18562457342E1CA1DA8458F45E4;

	// Token: 0x0400234A RID: 9034 RVA: 0x0010F694 File Offset: 0x0010D894
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=38 2A4F1BD548EC71F652E24985361CD72F0FE1BE7D;

	// Token: 0x0400234B RID: 9035 RVA: 0x0010F6BA File Offset: 0x0010D8BA
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 360487BE4278986419B568EFD887F6145383168A;

	// Token: 0x0400234C RID: 9036 RVA: 0x0010F6E2 File Offset: 0x0010D8E2
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 39822119C0A939F36A5BFFACBC5B9A4B1AABB095;

	// Token: 0x0400234D RID: 9037 RVA: 0x0010F712 File Offset: 0x0010D912
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 39F0147780CED5C87CCBF7E204097B31A73977EF;

	// Token: 0x0400234E RID: 9038 RVA: 0x0010F742 File Offset: 0x0010D942
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=20 42DDBEE388AB59C20A3D7D4D6555E78D74A45AE1;

	// Token: 0x0400234F RID: 9039 RVA: 0x0010F756 File Offset: 0x0010D956
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 485F43E332C2F7530815B17C08DAC169A8F697E0;

	// Token: 0x04002350 RID: 9040 RVA: 0x0010F776 File Offset: 0x0010D976
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=112 49C5BA13401986EC93E4677F52CBE2248184DFBD;

	// Token: 0x04002351 RID: 9041 RVA: 0x0010F7E6 File Offset: 0x0010D9E6
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 51E4CA1C2B009A2876C6E57D8E69E3502BCA3440;

	// Token: 0x04002352 RID: 9042 RVA: 0x0010F7FE File Offset: 0x0010D9FE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 52BCE8E7EFAA4269AA79631DD5F47D15A649B201;

	// Token: 0x04002353 RID: 9043 RVA: 0x0010F82E File Offset: 0x0010DA2E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=960 553E235E202D57C9F1156E7D232E02BBDC920165;

	// Token: 0x04002354 RID: 9044 RVA: 0x0010FBEE File Offset: 0x0010DDEE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 5733FA1CBD2C7E9B865AADCB3505C31FE927380C;

	// Token: 0x04002355 RID: 9045 RVA: 0x0010FC0E File Offset: 0x0010DE0E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=56 574B9D4E4C39F6E8004181E5765B627B75EB1AD1;

	// Token: 0x04002356 RID: 9046 RVA: 0x0010FC46 File Offset: 0x0010DE46
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 5BE9DB9EEB9CBB4D22472CA9734B1FA1D36126BD;

	// Token: 0x04002357 RID: 9047 RVA: 0x0010FC52 File Offset: 0x0010DE52
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 5D100A87B697F3AE2015A5D3B2A7B5419E1BCA98;

	// Token: 0x04002358 RID: 9048 RVA: 0x0010FC58 File Offset: 0x0010DE58
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=664 68D0F86889D5D656483EEE829BCEECDFEC91D8EA;

	// Token: 0x04002359 RID: 9049 RVA: 0x0010FEF0 File Offset: 0x0010E0F0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=416 6A0D50D692745A6663128CD315B71079584F3E59;

	// Token: 0x0400235A RID: 9050 RVA: 0x00110090 File Offset: 0x0010E290
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 6DFC9250492FDC984D6B53377D3836B5FF54D898;

	// Token: 0x0400235B RID: 9051 RVA: 0x001100C0 File Offset: 0x0010E2C0
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 702F6A3276CBE481D247A77C20B5459FB94D07D2;

	// Token: 0x0400235C RID: 9052 RVA: 0x001100D8 File Offset: 0x0010E2D8
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 741A4943DAFD82EB1A19C7D59D33118BE5850140;

	// Token: 0x0400235D RID: 9053 RVA: 0x001100E4 File Offset: 0x0010E2E4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=6 76A37DE9F46FC79071C6D55532C33EF92DC21461;

	// Token: 0x0400235E RID: 9054 RVA: 0x001100EA File Offset: 0x0010E2EA
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=340 7895CD8ED207B9FE3B1484CDA03137DCAE089A18;

	// Token: 0x0400235F RID: 9055 RVA: 0x0011023E File Offset: 0x0010E43E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 7A32E1A19C182315E4263A65A72066492550D8CD;

	// Token: 0x04002360 RID: 9056 RVA: 0x0011025E File Offset: 0x0010E45E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 8B4E5E81A88D29642679AFCE41DCA380F9000462;

	// Token: 0x04002361 RID: 9057 RVA: 0x0011026E File Offset: 0x0010E46E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 8F3BA379120CAB2C6A212F39EB66382C6575697D;

	// Token: 0x04002362 RID: 9058 RVA: 0x0011029E File Offset: 0x0010E49E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=220 925E66261A7A5ECAE0D8764EDDA326CEDBC7A63C;

	// Token: 0x04002363 RID: 9059 RVA: 0x0011037A File Offset: 0x0010E57A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=220 942C3A42D4A40CA2347389D7F2805305CBB4FA55;

	// Token: 0x04002364 RID: 9060 RVA: 0x00110456 File Offset: 0x0010E656
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=960 977375E4E1ED54F588076ACA36CC17E6C2195CB9;

	// Token: 0x04002365 RID: 9061 RVA: 0x00110816 File Offset: 0x0010EA16
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=12 99F0664C2AC8464B51252D92FC24F3834C6FB90C;

	// Token: 0x04002366 RID: 9062 RVA: 0x00110822 File Offset: 0x0010EA22
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 9A45A8272275FEA6F5384242A4EA5B6CA874321C;

	// Token: 0x04002367 RID: 9063 RVA: 0x00110852 File Offset: 0x0010EA52
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=28 9E31F24F64765FCAA589F589324D17C9FCF6A06D;

	// Token: 0x04002368 RID: 9064 RVA: 0x0011086E File Offset: 0x0010EA6E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=144 9E374D7263B2452E25DE3D6E617F6A728D98A439;

	// Token: 0x04002369 RID: 9065 RVA: 0x001108FE File Offset: 0x0010EAFE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=68 A933F173482FF50754B4942AF8DFC584EF14A45B;

	// Token: 0x0400236A RID: 9066 RVA: 0x00110942 File Offset: 0x0010EB42
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=24 AB4EAD1DB94B8220FAAEDAA6A593B2E6DB0A8A9C;

	// Token: 0x0400236B RID: 9067 RVA: 0x0011095A File Offset: 0x0010EB5A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 B113E4157CAB841C570845AE912152A86CD4B232;

	// Token: 0x0400236C RID: 9068 RVA: 0x0011098A File Offset: 0x0010EB8A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=416 B368804F0C6DAB083B253A6B106D0783D5C32E90;

	// Token: 0x0400236D RID: 9069 RVA: 0x00110B2A File Offset: 0x0010ED2A
	internal static readonly long B68FC290AA4E317641D3E37B57B8F12262AA178B;

	// Token: 0x0400236E RID: 9070 RVA: 0x00110B32 File Offset: 0x0010ED32
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 B9F0004E3873FDDCABFDA6174EA18F0859B637B4;

	// Token: 0x0400236F RID: 9071 RVA: 0x00110B5A File Offset: 0x0010ED5A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=20 BAD037B714E1CD1052149B51238A3D4351DD10B5;

	// Token: 0x04002370 RID: 9072 RVA: 0x00110B6E File Offset: 0x0010ED6E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 C2D6E36D84DDA5D661B95D7B32D3F47CD7ACBF6C;

	// Token: 0x04002371 RID: 9073 RVA: 0x00110B7E File Offset: 0x0010ED7E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 C33E4EBBE05CAF97686B45E795EB94C5C28012D8;

	// Token: 0x04002372 RID: 9074 RVA: 0x00110BAE File Offset: 0x0010EDAE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 C888AD892CE77B8CF3660AC17120FD39D516F1B5;

	// Token: 0x04002373 RID: 9075 RVA: 0x00110BDE File Offset: 0x0010EDDE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 CCB1C30EA6E6776FEC1DCFF25CB42F72C6CDFB3D;

	// Token: 0x04002374 RID: 9076 RVA: 0x00110C0E File Offset: 0x0010EE0E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 D1D4FFE97E52ACA7A01B4196375790B40E980B57;

	// Token: 0x04002375 RID: 9077 RVA: 0x00110C36 File Offset: 0x0010EE36
	internal static readonly long DB9F879BE80CC2DA24DEF633D23DBB2B1ADBCC81;

	// Token: 0x04002376 RID: 9078 RVA: 0x00110C3E File Offset: 0x0010EE3E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=16 DCF398750721AA7A27A6BA56E99350329B06E8B1;

	// Token: 0x04002377 RID: 9079 RVA: 0x00110C4E File Offset: 0x0010EE4E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=184 E09FAE0843AE7121A4B493BD6BF6AA1110436B72;

	// Token: 0x04002378 RID: 9080 RVA: 0x00110D06 File Offset: 0x0010EF06
	internal static readonly long EBC658B067B5C785A3F0BB67D73755F6FEE7F70C;

	// Token: 0x04002379 RID: 9081 RVA: 0x00110D0E File Offset: 0x0010EF0E
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=36 ED18A4A1FF7C89B400D7CA156BA8D11BB09E1DE4;

	// Token: 0x0400237A RID: 9082 RVA: 0x00110D32 File Offset: 0x0010EF32
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=40 EDD70F406348AE4E888BF9F18199C00CE9D28ED6;

	// Token: 0x0400237B RID: 9083 RVA: 0x00110D5A File Offset: 0x0010EF5A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 EE3413A2C088FF9432054D6E60A7CB6A498D25F0;

	// Token: 0x0400237C RID: 9084 RVA: 0x00110D9A File Offset: 0x0010EF9A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=36 F64F25EAE9A3D7A356813C4218000185541D7779;

	// Token: 0x0400237D RID: 9085 RVA: 0x00110DBE File Offset: 0x0010EFBE
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=1212 FB0C58D8B3094F018764CC6E3094B9576DB08069;

	// Token: 0x0400237E RID: 9086 RVA: 0x0011127A File Offset: 0x0010F47A
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=20 FFE3F15642234E7FAD6951D432F1134D5AD15922;

	// Token: 0x0200054D RID: 1357
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 6)]
	private struct __StaticArrayInitTypeSize=6
	{
	}

	// Token: 0x0200054E RID: 1358
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 12)]
	private struct __StaticArrayInitTypeSize=12
	{
	}

	// Token: 0x0200054F RID: 1359
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 16)]
	private struct __StaticArrayInitTypeSize=16
	{
	}

	// Token: 0x02000550 RID: 1360
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 20)]
	private struct __StaticArrayInitTypeSize=20
	{
	}

	// Token: 0x02000551 RID: 1361
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 24)]
	private struct __StaticArrayInitTypeSize=24
	{
	}

	// Token: 0x02000552 RID: 1362
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 28)]
	private struct __StaticArrayInitTypeSize=28
	{
	}

	// Token: 0x02000553 RID: 1363
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 32)]
	private struct __StaticArrayInitTypeSize=32
	{
	}

	// Token: 0x02000554 RID: 1364
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 36)]
	private struct __StaticArrayInitTypeSize=36
	{
	}

	// Token: 0x02000555 RID: 1365
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 38)]
	private struct __StaticArrayInitTypeSize=38
	{
	}

	// Token: 0x02000556 RID: 1366
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 40)]
	private struct __StaticArrayInitTypeSize=40
	{
	}

	// Token: 0x02000557 RID: 1367
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 44)]
	private struct __StaticArrayInitTypeSize=44
	{
	}

	// Token: 0x02000558 RID: 1368
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 48)]
	private struct __StaticArrayInitTypeSize=48
	{
	}

	// Token: 0x02000559 RID: 1369
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 52)]
	private struct __StaticArrayInitTypeSize=52
	{
	}

	// Token: 0x0200055A RID: 1370
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 56)]
	private struct __StaticArrayInitTypeSize=56
	{
	}

	// Token: 0x0200055B RID: 1371
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 64)]
	private struct __StaticArrayInitTypeSize=64
	{
	}

	// Token: 0x0200055C RID: 1372
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 68)]
	private struct __StaticArrayInitTypeSize=68
	{
	}

	// Token: 0x0200055D RID: 1373
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 112)]
	private struct __StaticArrayInitTypeSize=112
	{
	}

	// Token: 0x0200055E RID: 1374
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 144)]
	private struct __StaticArrayInitTypeSize=144
	{
	}

	// Token: 0x0200055F RID: 1375
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 184)]
	private struct __StaticArrayInitTypeSize=184
	{
	}

	// Token: 0x02000560 RID: 1376
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 220)]
	private struct __StaticArrayInitTypeSize=220
	{
	}

	// Token: 0x02000561 RID: 1377
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 340)]
	private struct __StaticArrayInitTypeSize=340
	{
	}

	// Token: 0x02000562 RID: 1378
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 416)]
	private struct __StaticArrayInitTypeSize=416
	{
	}

	// Token: 0x02000563 RID: 1379
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 664)]
	private struct __StaticArrayInitTypeSize=664
	{
	}

	// Token: 0x02000564 RID: 1380
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 960)]
	private struct __StaticArrayInitTypeSize=960
	{
	}

	// Token: 0x02000565 RID: 1381
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1212)]
	private struct __StaticArrayInitTypeSize=1212
	{
	}
}
