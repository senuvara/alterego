﻿using System;

namespace Microsoft.CSharp
{
	// Token: 0x02000006 RID: 6
	internal class CodeDomProvider
	{
		// Token: 0x06000009 RID: 9 RVA: 0x000020AE File Offset: 0x000002AE
		public string CreateEscapedIdentifier(string name)
		{
			if (CodeDomProvider.IsKeyword(name) || CodeDomProvider.IsPrefixTwoUnderscore(name))
			{
				return "@" + name;
			}
			return name;
		}

		// Token: 0x0600000A RID: 10 RVA: 0x000020CD File Offset: 0x000002CD
		private static bool IsKeyword(string value)
		{
			return false;
		}

		// Token: 0x0600000B RID: 11 RVA: 0x000020D0 File Offset: 0x000002D0
		private static bool IsPrefixTwoUnderscore(string value)
		{
			return value.Length >= 3 && (value[0] == '_' && value[1] == '_') && value[2] != '_';
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002103 File Offset: 0x00000303
		public CodeDomProvider()
		{
		}
	}
}
