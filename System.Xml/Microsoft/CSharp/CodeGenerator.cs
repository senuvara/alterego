﻿using System;
using System.Globalization;

namespace Microsoft.CSharp
{
	// Token: 0x02000008 RID: 8
	internal class CodeGenerator
	{
		// Token: 0x0600000E RID: 14 RVA: 0x00002113 File Offset: 0x00000313
		public static bool IsValidLanguageIndependentIdentifier(string value)
		{
			return CodeGenerator.IsValidTypeNameOrIdentifier(value, false);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x0000211C File Offset: 0x0000031C
		private static bool IsValidTypeNameOrIdentifier(string value, bool isTypeName)
		{
			bool flag = true;
			if (value.Length == 0)
			{
				return false;
			}
			int i = 0;
			while (i < value.Length)
			{
				char c = value[i];
				switch (char.GetUnicodeCategory(c))
				{
				case UnicodeCategory.UppercaseLetter:
				case UnicodeCategory.LowercaseLetter:
				case UnicodeCategory.TitlecaseLetter:
				case UnicodeCategory.ModifierLetter:
				case UnicodeCategory.OtherLetter:
				case UnicodeCategory.LetterNumber:
					flag = false;
					break;
				case UnicodeCategory.NonSpacingMark:
				case UnicodeCategory.SpacingCombiningMark:
				case UnicodeCategory.DecimalDigitNumber:
				case UnicodeCategory.ConnectorPunctuation:
					if (flag && c != '_')
					{
						return false;
					}
					flag = false;
					break;
				case UnicodeCategory.EnclosingMark:
				case UnicodeCategory.OtherNumber:
				case UnicodeCategory.SpaceSeparator:
				case UnicodeCategory.LineSeparator:
				case UnicodeCategory.ParagraphSeparator:
				case UnicodeCategory.Control:
				case UnicodeCategory.Format:
				case UnicodeCategory.Surrogate:
				case UnicodeCategory.PrivateUse:
					goto IL_88;
				default:
					goto IL_88;
				}
				IL_97:
				i++;
				continue;
				IL_88:
				if (!isTypeName || !CodeGenerator.IsSpecialTypeChar(c, ref flag))
				{
					return false;
				}
				goto IL_97;
			}
			return true;
		}

		// Token: 0x06000010 RID: 16 RVA: 0x000021D4 File Offset: 0x000003D4
		private static bool IsSpecialTypeChar(char ch, ref bool nextMustBeStartChar)
		{
			if (ch <= '>')
			{
				switch (ch)
				{
				case '$':
				case '&':
				case '*':
				case '+':
				case ',':
				case '-':
				case '.':
					break;
				case '%':
				case '\'':
				case '(':
				case ')':
					return false;
				default:
					switch (ch)
					{
					case ':':
					case '<':
					case '>':
						break;
					case ';':
					case '=':
						return false;
					default:
						return false;
					}
					break;
				}
			}
			else if (ch != '[' && ch != ']')
			{
				if (ch != '`')
				{
					return false;
				}
				return true;
			}
			nextMustBeStartChar = true;
			return true;
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002103 File Offset: 0x00000303
		public CodeGenerator()
		{
		}
	}
}
