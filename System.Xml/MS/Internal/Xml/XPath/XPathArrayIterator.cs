﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200004E RID: 78
	[DebuggerDisplay("Position={CurrentPosition}, Current={debuggerDisplayProxy, nq}")]
	internal class XPathArrayIterator : ResetableIterator
	{
		// Token: 0x0600021A RID: 538 RVA: 0x00007EC2 File Offset: 0x000060C2
		public XPathArrayIterator(IList list)
		{
			this.list = list;
		}

		// Token: 0x0600021B RID: 539 RVA: 0x00007ED1 File Offset: 0x000060D1
		public XPathArrayIterator(XPathArrayIterator it)
		{
			this.list = it.list;
			this.index = it.index;
		}

		// Token: 0x0600021C RID: 540 RVA: 0x00007EF1 File Offset: 0x000060F1
		public XPathArrayIterator(XPathNodeIterator nodeIterator)
		{
			this.list = new ArrayList();
			while (nodeIterator.MoveNext())
			{
				XPathNavigator xpathNavigator = nodeIterator.Current;
				this.list.Add(xpathNavigator.Clone());
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600021D RID: 541 RVA: 0x00007F25 File Offset: 0x00006125
		public IList AsList
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x0600021E RID: 542 RVA: 0x00007F2D File Offset: 0x0000612D
		public override XPathNodeIterator Clone()
		{
			return new XPathArrayIterator(this);
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600021F RID: 543 RVA: 0x00007F38 File Offset: 0x00006138
		public override XPathNavigator Current
		{
			get
			{
				if (this.index < 1)
				{
					throw new InvalidOperationException(Res.GetString("Enumeration has not started. Call MoveNext.", new object[]
					{
						string.Empty
					}));
				}
				return (XPathNavigator)this.list[this.index - 1];
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000220 RID: 544 RVA: 0x00007F84 File Offset: 0x00006184
		public override int CurrentPosition
		{
			get
			{
				return this.index;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000221 RID: 545 RVA: 0x00007F8C File Offset: 0x0000618C
		public override int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x06000222 RID: 546 RVA: 0x00007F99 File Offset: 0x00006199
		public override bool MoveNext()
		{
			if (this.index == this.list.Count)
			{
				return false;
			}
			this.index++;
			return true;
		}

		// Token: 0x06000223 RID: 547 RVA: 0x00007FBF File Offset: 0x000061BF
		public override void Reset()
		{
			this.index = 0;
		}

		// Token: 0x06000224 RID: 548 RVA: 0x00007FC8 File Offset: 0x000061C8
		public override IEnumerator GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000225 RID: 549 RVA: 0x00007FD5 File Offset: 0x000061D5
		private object debuggerDisplayProxy
		{
			get
			{
				if (this.index >= 1)
				{
					return new XPathNavigator.DebuggerDisplayProxy(this.Current);
				}
				return null;
			}
		}

		// Token: 0x0400010E RID: 270
		protected IList list;

		// Token: 0x0400010F RID: 271
		protected int index;
	}
}
