﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000050 RID: 80
	internal class XPathChildIterator : XPathAxisIterator
	{
		// Token: 0x0600022D RID: 557 RVA: 0x00008185 File Offset: 0x00006385
		public XPathChildIterator(XPathNavigator nav, XPathNodeType type) : base(nav, type, false)
		{
		}

		// Token: 0x0600022E RID: 558 RVA: 0x00008190 File Offset: 0x00006390
		public XPathChildIterator(XPathNavigator nav, string name, string namespaceURI) : base(nav, name, namespaceURI, false)
		{
		}

		// Token: 0x0600022F RID: 559 RVA: 0x00007CDE File Offset: 0x00005EDE
		public XPathChildIterator(XPathChildIterator it) : base(it)
		{
		}

		// Token: 0x06000230 RID: 560 RVA: 0x0000819C File Offset: 0x0000639C
		public override XPathNodeIterator Clone()
		{
			return new XPathChildIterator(this);
		}

		// Token: 0x06000231 RID: 561 RVA: 0x000081A4 File Offset: 0x000063A4
		public override bool MoveNext()
		{
			while (this.first ? this.nav.MoveToFirstChild() : this.nav.MoveToNext())
			{
				this.first = false;
				if (this.Matches)
				{
					this.position++;
					return true;
				}
			}
			return false;
		}
	}
}
