﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000025 RID: 37
	internal class ForwardPositionQuery : CacheOutputQuery
	{
		// Token: 0x060000DF RID: 223 RVA: 0x0000379B File Offset: 0x0000199B
		public ForwardPositionQuery(Query input) : base(input)
		{
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x000037A4 File Offset: 0x000019A4
		protected ForwardPositionQuery(ForwardPositionQuery other) : base(other)
		{
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x000041E8 File Offset: 0x000023E8
		public override object Evaluate(XPathNodeIterator context)
		{
			base.Evaluate(context);
			XPathNavigator xpathNavigator;
			while ((xpathNavigator = this.input.Advance()) != null)
			{
				this.outputBuffer.Add(xpathNavigator.Clone());
			}
			return this;
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x000037E5 File Offset: 0x000019E5
		public override XPathNavigator MatchNode(XPathNavigator context)
		{
			return this.input.MatchNode(context);
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x00004220 File Offset: 0x00002420
		public override XPathNodeIterator Clone()
		{
			return new ForwardPositionQuery(this);
		}
	}
}
