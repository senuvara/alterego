﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000029 RID: 41
	internal class Group : AstNode
	{
		// Token: 0x060000F7 RID: 247 RVA: 0x00004638 File Offset: 0x00002838
		public Group(AstNode groupNode)
		{
			this.groupNode = groupNode;
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00003A7F File Offset: 0x00001C7F
		public override AstNode.AstType Type
		{
			get
			{
				return AstNode.AstType.Group;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType ReturnType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000FA RID: 250 RVA: 0x00004647 File Offset: 0x00002847
		public AstNode GroupNode
		{
			get
			{
				return this.groupNode;
			}
		}

		// Token: 0x040000AD RID: 173
		private AstNode groupNode;
	}
}
