﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000030 RID: 48
	internal sealed class MergeFilterQuery : CacheOutputQuery
	{
		// Token: 0x0600013D RID: 317 RVA: 0x0000523A File Offset: 0x0000343A
		public MergeFilterQuery(Query input, Query child) : base(input)
		{
			this.child = child;
		}

		// Token: 0x0600013E RID: 318 RVA: 0x0000524A File Offset: 0x0000344A
		private MergeFilterQuery(MergeFilterQuery other) : base(other)
		{
			this.child = Query.Clone(other.child);
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00005264 File Offset: 0x00003464
		public override void SetXsltContext(XsltContext xsltContext)
		{
			base.SetXsltContext(xsltContext);
			this.child.SetXsltContext(xsltContext);
		}

		// Token: 0x06000140 RID: 320 RVA: 0x0000527C File Offset: 0x0000347C
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			base.Evaluate(nodeIterator);
			while (this.input.Advance() != null)
			{
				this.child.Evaluate(this.input);
				XPathNavigator nav;
				while ((nav = this.child.Advance()) != null)
				{
					base.Insert(this.outputBuffer, nav);
				}
			}
			return this;
		}

		// Token: 0x06000141 RID: 321 RVA: 0x000052D4 File Offset: 0x000034D4
		public override XPathNavigator MatchNode(XPathNavigator current)
		{
			XPathNavigator xpathNavigator = this.child.MatchNode(current);
			if (xpathNavigator == null)
			{
				return null;
			}
			xpathNavigator = this.input.MatchNode(xpathNavigator);
			if (xpathNavigator == null)
			{
				return null;
			}
			this.Evaluate(new XPathSingletonIterator(xpathNavigator.Clone(), true));
			for (XPathNavigator xpathNavigator2 = this.Advance(); xpathNavigator2 != null; xpathNavigator2 = this.Advance())
			{
				if (xpathNavigator2.IsSamePosition(current))
				{
					return xpathNavigator;
				}
			}
			return null;
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00005337 File Offset: 0x00003537
		public override XPathNodeIterator Clone()
		{
			return new MergeFilterQuery(this);
		}

		// Token: 0x06000143 RID: 323 RVA: 0x0000533F File Offset: 0x0000353F
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			this.input.PrintQuery(w);
			this.child.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x040000B8 RID: 184
		private Query child;
	}
}
