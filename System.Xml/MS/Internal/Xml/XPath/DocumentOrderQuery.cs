﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200001E RID: 30
	internal sealed class DocumentOrderQuery : CacheOutputQuery
	{
		// Token: 0x060000AA RID: 170 RVA: 0x0000379B File Offset: 0x0000199B
		public DocumentOrderQuery(Query qyParent) : base(qyParent)
		{
		}

		// Token: 0x060000AB RID: 171 RVA: 0x000037A4 File Offset: 0x000019A4
		private DocumentOrderQuery(DocumentOrderQuery other) : base(other)
		{
		}

		// Token: 0x060000AC RID: 172 RVA: 0x000037B0 File Offset: 0x000019B0
		public override object Evaluate(XPathNodeIterator context)
		{
			base.Evaluate(context);
			XPathNavigator nav;
			while ((nav = this.input.Advance()) != null)
			{
				base.Insert(this.outputBuffer, nav);
			}
			return this;
		}

		// Token: 0x060000AD RID: 173 RVA: 0x000037E5 File Offset: 0x000019E5
		public override XPathNavigator MatchNode(XPathNavigator context)
		{
			return this.input.MatchNode(context);
		}

		// Token: 0x060000AE RID: 174 RVA: 0x000037F3 File Offset: 0x000019F3
		public override XPathNodeIterator Clone()
		{
			return new DocumentOrderQuery(this);
		}
	}
}
