﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000023 RID: 35
	internal sealed class FollowingQuery : BaseAxisQuery
	{
		// Token: 0x060000D3 RID: 211 RVA: 0x000022A2 File Offset: 0x000004A2
		public FollowingQuery(Query qyInput, string name, string prefix, XPathNodeType typeTest) : base(qyInput, name, prefix, typeTest)
		{
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x00003E58 File Offset: 0x00002058
		private FollowingQuery(FollowingQuery other) : base(other)
		{
			this.input = Query.Clone(other.input);
			this.iterator = Query.Clone(other.iterator);
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x00003E83 File Offset: 0x00002083
		public override void Reset()
		{
			this.iterator = null;
			base.Reset();
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00003E94 File Offset: 0x00002094
		public override XPathNavigator Advance()
		{
			if (this.iterator == null)
			{
				this.input = this.qyInput.Advance();
				if (this.input == null)
				{
					return null;
				}
				XPathNavigator xpathNavigator;
				do
				{
					xpathNavigator = this.input.Clone();
					this.input = this.qyInput.Advance();
				}
				while (xpathNavigator.IsDescendant(this.input));
				this.input = xpathNavigator;
				this.iterator = XPathEmptyIterator.Instance;
			}
			while (!this.iterator.MoveNext())
			{
				bool matchSelf;
				if (this.input.NodeType == XPathNodeType.Attribute || this.input.NodeType == XPathNodeType.Namespace)
				{
					this.input.MoveToParent();
					matchSelf = false;
				}
				else
				{
					while (!this.input.MoveToNext())
					{
						if (!this.input.MoveToParent())
						{
							return null;
						}
					}
					matchSelf = true;
				}
				if (base.NameTest)
				{
					this.iterator = this.input.SelectDescendants(base.Name, base.Namespace, matchSelf);
				}
				else
				{
					this.iterator = this.input.SelectDescendants(base.TypeTest, matchSelf);
				}
			}
			this.position++;
			this.currentNode = this.iterator.Current;
			return this.currentNode;
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00003FC8 File Offset: 0x000021C8
		public override XPathNodeIterator Clone()
		{
			return new FollowingQuery(this);
		}

		// Token: 0x04000084 RID: 132
		private XPathNavigator input;

		// Token: 0x04000085 RID: 133
		private XPathNodeIterator iterator;
	}
}
