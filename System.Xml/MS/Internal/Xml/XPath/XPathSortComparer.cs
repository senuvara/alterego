﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000046 RID: 70
	internal sealed class XPathSortComparer : IComparer<SortKey>
	{
		// Token: 0x060001D1 RID: 465 RVA: 0x00006F31 File Offset: 0x00005131
		public XPathSortComparer(int size)
		{
			if (size <= 0)
			{
				size = 3;
			}
			this.expressions = new Query[size];
			this.comparers = new IComparer[size];
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x00006F58 File Offset: 0x00005158
		public XPathSortComparer() : this(3)
		{
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x00006F64 File Offset: 0x00005164
		public void AddSort(Query evalQuery, IComparer comparer)
		{
			if (this.numSorts == this.expressions.Length)
			{
				Query[] array = new Query[this.numSorts * 2];
				IComparer[] array2 = new IComparer[this.numSorts * 2];
				for (int i = 0; i < this.numSorts; i++)
				{
					array[i] = this.expressions[i];
					array2[i] = this.comparers[i];
				}
				this.expressions = array;
				this.comparers = array2;
			}
			if (evalQuery.StaticType == XPathResultType.NodeSet || evalQuery.StaticType == XPathResultType.Any)
			{
				evalQuery = new StringFunctions(Function.FunctionType.FuncString, new Query[]
				{
					evalQuery
				});
			}
			this.expressions[this.numSorts] = evalQuery;
			this.comparers[this.numSorts] = comparer;
			this.numSorts++;
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060001D4 RID: 468 RVA: 0x00007020 File Offset: 0x00005220
		public int NumSorts
		{
			get
			{
				return this.numSorts;
			}
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x00007028 File Offset: 0x00005228
		public Query Expression(int i)
		{
			return this.expressions[i];
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x00007034 File Offset: 0x00005234
		int IComparer<SortKey>.Compare(SortKey x, SortKey y)
		{
			for (int i = 0; i < x.NumKeys; i++)
			{
				int num = this.comparers[i].Compare(x[i], y[i]);
				if (num != 0)
				{
					return num;
				}
			}
			return x.OriginalPosition - y.OriginalPosition;
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x00007084 File Offset: 0x00005284
		internal XPathSortComparer Clone()
		{
			XPathSortComparer xpathSortComparer = new XPathSortComparer(this.numSorts);
			for (int i = 0; i < this.numSorts; i++)
			{
				xpathSortComparer.comparers[i] = this.comparers[i];
				xpathSortComparer.expressions[i] = (Query)this.expressions[i].Clone();
			}
			xpathSortComparer.numSorts = this.numSorts;
			return xpathSortComparer;
		}

		// Token: 0x040000FD RID: 253
		private const int minSize = 3;

		// Token: 0x040000FE RID: 254
		private Query[] expressions;

		// Token: 0x040000FF RID: 255
		private IComparer[] comparers;

		// Token: 0x04000100 RID: 256
		private int numSorts;
	}
}
