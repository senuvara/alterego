﻿using System;
using System.Xml;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200002B RID: 43
	internal sealed class IDQuery : CacheOutputQuery
	{
		// Token: 0x06000102 RID: 258 RVA: 0x0000379B File Offset: 0x0000199B
		public IDQuery(Query arg) : base(arg)
		{
		}

		// Token: 0x06000103 RID: 259 RVA: 0x000037A4 File Offset: 0x000019A4
		private IDQuery(IDQuery other) : base(other)
		{
		}

		// Token: 0x06000104 RID: 260 RVA: 0x000046B4 File Offset: 0x000028B4
		public override object Evaluate(XPathNodeIterator context)
		{
			object obj = base.Evaluate(context);
			XPathNavigator contextNode = context.Current.Clone();
			switch (base.GetXPathType(obj))
			{
			case XPathResultType.Number:
				this.ProcessIds(contextNode, StringFunctions.toString((double)obj));
				break;
			case XPathResultType.String:
				this.ProcessIds(contextNode, (string)obj);
				break;
			case XPathResultType.Boolean:
				this.ProcessIds(contextNode, StringFunctions.toString((bool)obj));
				break;
			case XPathResultType.NodeSet:
			{
				XPathNavigator xpathNavigator;
				while ((xpathNavigator = this.input.Advance()) != null)
				{
					this.ProcessIds(contextNode, xpathNavigator.Value);
				}
				break;
			}
			case (XPathResultType)4:
				this.ProcessIds(contextNode, ((XPathNavigator)obj).Value);
				break;
			}
			return this;
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00004764 File Offset: 0x00002964
		private void ProcessIds(XPathNavigator contextNode, string val)
		{
			string[] array = XmlConvert.SplitString(val);
			for (int i = 0; i < array.Length; i++)
			{
				if (contextNode.MoveToId(array[i]))
				{
					base.Insert(this.outputBuffer, contextNode);
				}
			}
		}

		// Token: 0x06000106 RID: 262 RVA: 0x000047A0 File Offset: 0x000029A0
		public override XPathNavigator MatchNode(XPathNavigator context)
		{
			this.Evaluate(new XPathSingletonIterator(context, true));
			XPathNavigator xpathNavigator;
			while ((xpathNavigator = this.Advance()) != null)
			{
				if (xpathNavigator.IsSamePosition(context))
				{
					return context;
				}
			}
			return null;
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000047D3 File Offset: 0x000029D3
		public override XPathNodeIterator Clone()
		{
			return new IDQuery(this);
		}
	}
}
