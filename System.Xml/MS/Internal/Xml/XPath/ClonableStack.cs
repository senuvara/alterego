﻿using System;
using System.Collections.Generic;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000016 RID: 22
	internal sealed class ClonableStack<T> : List<T>
	{
		// Token: 0x06000073 RID: 115 RVA: 0x0000304D File Offset: 0x0000124D
		public ClonableStack()
		{
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00003055 File Offset: 0x00001255
		public ClonableStack(int capacity) : base(capacity)
		{
		}

		// Token: 0x06000075 RID: 117 RVA: 0x0000305E File Offset: 0x0000125E
		private ClonableStack(IEnumerable<T> collection) : base(collection)
		{
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00003067 File Offset: 0x00001267
		public void Push(T value)
		{
			base.Add(value);
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00003070 File Offset: 0x00001270
		public T Pop()
		{
			int index = base.Count - 1;
			T result = base[index];
			base.RemoveAt(index);
			return result;
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00003094 File Offset: 0x00001294
		public T Peek()
		{
			return base[base.Count - 1];
		}

		// Token: 0x06000079 RID: 121 RVA: 0x000030A4 File Offset: 0x000012A4
		public ClonableStack<T> Clone()
		{
			return new ClonableStack<T>(this);
		}
	}
}
