﻿using System;
using System.Globalization;
using System.Xml;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000036 RID: 54
	internal sealed class OperandQuery : ValueQuery
	{
		// Token: 0x0600016D RID: 365 RVA: 0x00005A8C File Offset: 0x00003C8C
		public OperandQuery(object val)
		{
			this.val = val;
		}

		// Token: 0x0600016E RID: 366 RVA: 0x00005A9B File Offset: 0x00003C9B
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			return this.val;
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public override XPathResultType StaticType
		{
			get
			{
				return base.GetXPathType(this.val);
			}
		}

		// Token: 0x06000170 RID: 368 RVA: 0x00002068 File Offset: 0x00000268
		public override XPathNodeIterator Clone()
		{
			return this;
		}

		// Token: 0x06000171 RID: 369 RVA: 0x00005AB1 File Offset: 0x00003CB1
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			w.WriteAttributeString("value", Convert.ToString(this.val, CultureInfo.InvariantCulture));
			w.WriteEndElement();
		}

		// Token: 0x040000C4 RID: 196
		internal object val;
	}
}
