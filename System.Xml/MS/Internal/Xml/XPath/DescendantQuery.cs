﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200001C RID: 28
	internal class DescendantQuery : DescendantBaseQuery
	{
		// Token: 0x0600009E RID: 158 RVA: 0x0000353A File Offset: 0x0000173A
		internal DescendantQuery(Query qyParent, string Name, string Prefix, XPathNodeType Type, bool matchSelf, bool abbrAxis) : base(qyParent, Name, Prefix, Type, matchSelf, abbrAxis)
		{
		}

		// Token: 0x0600009F RID: 159 RVA: 0x0000354B File Offset: 0x0000174B
		public DescendantQuery(DescendantQuery other) : base(other)
		{
			this.nodeIterator = Query.Clone(other.nodeIterator);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00003565 File Offset: 0x00001765
		public override void Reset()
		{
			this.nodeIterator = null;
			base.Reset();
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00003574 File Offset: 0x00001774
		public override XPathNavigator Advance()
		{
			for (;;)
			{
				if (this.nodeIterator == null)
				{
					this.position = 0;
					XPathNavigator xpathNavigator = this.qyInput.Advance();
					if (xpathNavigator == null)
					{
						break;
					}
					if (base.NameTest)
					{
						if (base.TypeTest == XPathNodeType.ProcessingInstruction)
						{
							this.nodeIterator = new IteratorFilter(xpathNavigator.SelectDescendants(base.TypeTest, this.matchSelf), base.Name);
						}
						else
						{
							this.nodeIterator = xpathNavigator.SelectDescendants(base.Name, base.Namespace, this.matchSelf);
						}
					}
					else
					{
						this.nodeIterator = xpathNavigator.SelectDescendants(base.TypeTest, this.matchSelf);
					}
				}
				if (this.nodeIterator.MoveNext())
				{
					goto Block_4;
				}
				this.nodeIterator = null;
			}
			return null;
			Block_4:
			this.position++;
			this.currentNode = this.nodeIterator.Current;
			return this.currentNode;
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00003650 File Offset: 0x00001850
		public override XPathNodeIterator Clone()
		{
			return new DescendantQuery(this);
		}

		// Token: 0x0400007A RID: 122
		private XPathNodeIterator nodeIterator;
	}
}
