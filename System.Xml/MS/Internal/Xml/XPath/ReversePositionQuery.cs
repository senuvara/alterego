﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000042 RID: 66
	internal sealed class ReversePositionQuery : ForwardPositionQuery
	{
		// Token: 0x060001B4 RID: 436 RVA: 0x00006C6F File Offset: 0x00004E6F
		public ReversePositionQuery(Query input) : base(input)
		{
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00006C78 File Offset: 0x00004E78
		private ReversePositionQuery(ReversePositionQuery other) : base(other)
		{
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00006C81 File Offset: 0x00004E81
		public override XPathNodeIterator Clone()
		{
			return new ReversePositionQuery(this);
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060001B7 RID: 439 RVA: 0x00006C89 File Offset: 0x00004E89
		public override int CurrentPosition
		{
			get
			{
				return this.outputBuffer.Count - this.count + 1;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060001B8 RID: 440 RVA: 0x00006C9F File Offset: 0x00004E9F
		public override QueryProps Properties
		{
			get
			{
				return base.Properties | QueryProps.Reverse;
			}
		}
	}
}
