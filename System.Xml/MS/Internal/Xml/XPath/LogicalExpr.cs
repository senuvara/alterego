﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200002D RID: 45
	internal sealed class LogicalExpr : ValueQuery
	{
		// Token: 0x0600010E RID: 270 RVA: 0x0000487E File Offset: 0x00002A7E
		public LogicalExpr(Operator.Op op, Query opnd1, Query opnd2)
		{
			this.op = op;
			this.opnd1 = opnd1;
			this.opnd2 = opnd2;
		}

		// Token: 0x0600010F RID: 271 RVA: 0x0000489B File Offset: 0x00002A9B
		private LogicalExpr(LogicalExpr other) : base(other)
		{
			this.op = other.op;
			this.opnd1 = Query.Clone(other.opnd1);
			this.opnd2 = Query.Clone(other.opnd2);
		}

		// Token: 0x06000110 RID: 272 RVA: 0x000048D2 File Offset: 0x00002AD2
		public override void SetXsltContext(XsltContext context)
		{
			this.opnd1.SetXsltContext(context);
			this.opnd2.SetXsltContext(context);
		}

		// Token: 0x06000111 RID: 273 RVA: 0x000048EC File Offset: 0x00002AEC
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			Operator.Op op = this.op;
			object obj = this.opnd1.Evaluate(nodeIterator);
			object obj2 = this.opnd2.Evaluate(nodeIterator);
			int num = (int)base.GetXPathType(obj);
			int num2 = (int)base.GetXPathType(obj2);
			if (num < num2)
			{
				op = Operator.InvertOperator(op);
				object obj3 = obj;
				obj = obj2;
				obj2 = obj3;
				int num3 = num;
				num = num2;
				num2 = num3;
			}
			if (op == Operator.Op.EQ || op == Operator.Op.NE)
			{
				return LogicalExpr.CompXsltE[num][num2](op, obj, obj2);
			}
			return LogicalExpr.CompXsltO[num][num2](op, obj, obj2);
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00004978 File Offset: 0x00002B78
		private static bool cmpQueryQueryE(Operator.Op op, object val1, object val2)
		{
			bool flag = op == Operator.Op.EQ;
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val1);
			LogicalExpr.NodeSet nodeSet2 = new LogicalExpr.NodeSet(val2);
			IL_15:
			while (nodeSet.MoveNext())
			{
				if (!nodeSet2.MoveNext())
				{
					return false;
				}
				string value = nodeSet.Value;
				while (value == nodeSet2.Value != flag)
				{
					if (!nodeSet2.MoveNext())
					{
						nodeSet2.Reset();
						goto IL_15;
					}
				}
				return true;
			}
			return false;
		}

		// Token: 0x06000113 RID: 275 RVA: 0x000049DC File Offset: 0x00002BDC
		private static bool cmpQueryQueryO(Operator.Op op, object val1, object val2)
		{
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val1);
			LogicalExpr.NodeSet nodeSet2 = new LogicalExpr.NodeSet(val2);
			IL_10:
			while (nodeSet.MoveNext())
			{
				if (!nodeSet2.MoveNext())
				{
					return false;
				}
				double n = NumberFunctions.Number(nodeSet.Value);
				while (!LogicalExpr.cmpNumberNumber(op, n, NumberFunctions.Number(nodeSet2.Value)))
				{
					if (!nodeSet2.MoveNext())
					{
						nodeSet2.Reset();
						goto IL_10;
					}
				}
				return true;
			}
			return false;
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00004A44 File Offset: 0x00002C44
		private static bool cmpQueryNumber(Operator.Op op, object val1, object val2)
		{
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val1);
			double n = (double)val2;
			while (nodeSet.MoveNext())
			{
				if (LogicalExpr.cmpNumberNumber(op, NumberFunctions.Number(nodeSet.Value), n))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00004A84 File Offset: 0x00002C84
		private static bool cmpQueryStringE(Operator.Op op, object val1, object val2)
		{
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val1);
			string n = (string)val2;
			while (nodeSet.MoveNext())
			{
				if (LogicalExpr.cmpStringStringE(op, nodeSet.Value, n))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00004AC0 File Offset: 0x00002CC0
		private static bool cmpQueryStringO(Operator.Op op, object val1, object val2)
		{
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val1);
			double n = NumberFunctions.Number((string)val2);
			while (nodeSet.MoveNext())
			{
				if (LogicalExpr.cmpNumberNumberO(op, NumberFunctions.Number(nodeSet.Value), n))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00004B04 File Offset: 0x00002D04
		private static bool cmpRtfQueryE(Operator.Op op, object val1, object val2)
		{
			string n = LogicalExpr.Rtf(val1);
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val2);
			while (nodeSet.MoveNext())
			{
				if (LogicalExpr.cmpStringStringE(op, n, nodeSet.Value))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000118 RID: 280 RVA: 0x00004B40 File Offset: 0x00002D40
		private static bool cmpRtfQueryO(Operator.Op op, object val1, object val2)
		{
			double n = NumberFunctions.Number(LogicalExpr.Rtf(val1));
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val2);
			while (nodeSet.MoveNext())
			{
				if (LogicalExpr.cmpNumberNumberO(op, n, NumberFunctions.Number(nodeSet.Value)))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000119 RID: 281 RVA: 0x00004B84 File Offset: 0x00002D84
		private static bool cmpQueryBoolE(Operator.Op op, object val1, object val2)
		{
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val1);
			bool n = nodeSet.MoveNext();
			bool n2 = (bool)val2;
			return LogicalExpr.cmpBoolBoolE(op, n, n2);
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00004BB0 File Offset: 0x00002DB0
		private static bool cmpQueryBoolO(Operator.Op op, object val1, object val2)
		{
			LogicalExpr.NodeSet nodeSet = new LogicalExpr.NodeSet(val1);
			double n = nodeSet.MoveNext() ? 1.0 : 0.0;
			double n2 = NumberFunctions.Number((bool)val2);
			return LogicalExpr.cmpNumberNumberO(op, n, n2);
		}

		// Token: 0x0600011B RID: 283 RVA: 0x00004BF7 File Offset: 0x00002DF7
		private static bool cmpBoolBoolE(Operator.Op op, bool n1, bool n2)
		{
			return op == Operator.Op.EQ == (n1 == n2);
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00004C04 File Offset: 0x00002E04
		private static bool cmpBoolBoolE(Operator.Op op, object val1, object val2)
		{
			bool n = (bool)val1;
			bool n2 = (bool)val2;
			return LogicalExpr.cmpBoolBoolE(op, n, n2);
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00004C28 File Offset: 0x00002E28
		private static bool cmpBoolBoolO(Operator.Op op, object val1, object val2)
		{
			double n = NumberFunctions.Number((bool)val1);
			double n2 = NumberFunctions.Number((bool)val2);
			return LogicalExpr.cmpNumberNumberO(op, n, n2);
		}

		// Token: 0x0600011E RID: 286 RVA: 0x00004C58 File Offset: 0x00002E58
		private static bool cmpBoolNumberE(Operator.Op op, object val1, object val2)
		{
			bool n = (bool)val1;
			bool n2 = BooleanFunctions.toBoolean((double)val2);
			return LogicalExpr.cmpBoolBoolE(op, n, n2);
		}

		// Token: 0x0600011F RID: 287 RVA: 0x00004C80 File Offset: 0x00002E80
		private static bool cmpBoolNumberO(Operator.Op op, object val1, object val2)
		{
			double n = NumberFunctions.Number((bool)val1);
			double n2 = (double)val2;
			return LogicalExpr.cmpNumberNumberO(op, n, n2);
		}

		// Token: 0x06000120 RID: 288 RVA: 0x00004CA8 File Offset: 0x00002EA8
		private static bool cmpBoolStringE(Operator.Op op, object val1, object val2)
		{
			bool n = (bool)val1;
			bool n2 = BooleanFunctions.toBoolean((string)val2);
			return LogicalExpr.cmpBoolBoolE(op, n, n2);
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00004CD0 File Offset: 0x00002ED0
		private static bool cmpRtfBoolE(Operator.Op op, object val1, object val2)
		{
			bool n = BooleanFunctions.toBoolean(LogicalExpr.Rtf(val1));
			bool n2 = (bool)val2;
			return LogicalExpr.cmpBoolBoolE(op, n, n2);
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00004CF8 File Offset: 0x00002EF8
		private static bool cmpBoolStringO(Operator.Op op, object val1, object val2)
		{
			return LogicalExpr.cmpNumberNumberO(op, NumberFunctions.Number((bool)val1), NumberFunctions.Number((string)val2));
		}

		// Token: 0x06000123 RID: 291 RVA: 0x00004D16 File Offset: 0x00002F16
		private static bool cmpRtfBoolO(Operator.Op op, object val1, object val2)
		{
			return LogicalExpr.cmpNumberNumberO(op, NumberFunctions.Number(LogicalExpr.Rtf(val1)), NumberFunctions.Number((bool)val2));
		}

		// Token: 0x06000124 RID: 292 RVA: 0x00004D34 File Offset: 0x00002F34
		private static bool cmpNumberNumber(Operator.Op op, double n1, double n2)
		{
			switch (op)
			{
			case Operator.Op.EQ:
				return n1 == n2;
			case Operator.Op.NE:
				return n1 != n2;
			case Operator.Op.LT:
				return n1 < n2;
			case Operator.Op.LE:
				return n1 <= n2;
			case Operator.Op.GT:
				return n1 > n2;
			case Operator.Op.GE:
				return n1 >= n2;
			default:
				return false;
			}
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00004D8B File Offset: 0x00002F8B
		private static bool cmpNumberNumberO(Operator.Op op, double n1, double n2)
		{
			switch (op)
			{
			case Operator.Op.LT:
				return n1 < n2;
			case Operator.Op.LE:
				return n1 <= n2;
			case Operator.Op.GT:
				return n1 > n2;
			case Operator.Op.GE:
				return n1 >= n2;
			default:
				return false;
			}
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00004DC4 File Offset: 0x00002FC4
		private static bool cmpNumberNumber(Operator.Op op, object val1, object val2)
		{
			double n = (double)val1;
			double n2 = (double)val2;
			return LogicalExpr.cmpNumberNumber(op, n, n2);
		}

		// Token: 0x06000127 RID: 295 RVA: 0x00004DE8 File Offset: 0x00002FE8
		private static bool cmpStringNumber(Operator.Op op, object val1, object val2)
		{
			double n = (double)val2;
			double n2 = NumberFunctions.Number((string)val1);
			return LogicalExpr.cmpNumberNumber(op, n2, n);
		}

		// Token: 0x06000128 RID: 296 RVA: 0x00004E10 File Offset: 0x00003010
		private static bool cmpRtfNumber(Operator.Op op, object val1, object val2)
		{
			double n = (double)val2;
			double n2 = NumberFunctions.Number(LogicalExpr.Rtf(val1));
			return LogicalExpr.cmpNumberNumber(op, n2, n);
		}

		// Token: 0x06000129 RID: 297 RVA: 0x00004E38 File Offset: 0x00003038
		private static bool cmpStringStringE(Operator.Op op, string n1, string n2)
		{
			return op == Operator.Op.EQ == (n1 == n2);
		}

		// Token: 0x0600012A RID: 298 RVA: 0x00004E48 File Offset: 0x00003048
		private static bool cmpStringStringE(Operator.Op op, object val1, object val2)
		{
			string n = (string)val1;
			string n2 = (string)val2;
			return LogicalExpr.cmpStringStringE(op, n, n2);
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00004E6C File Offset: 0x0000306C
		private static bool cmpRtfStringE(Operator.Op op, object val1, object val2)
		{
			string n = LogicalExpr.Rtf(val1);
			string n2 = (string)val2;
			return LogicalExpr.cmpStringStringE(op, n, n2);
		}

		// Token: 0x0600012C RID: 300 RVA: 0x00004E90 File Offset: 0x00003090
		private static bool cmpRtfRtfE(Operator.Op op, object val1, object val2)
		{
			string n = LogicalExpr.Rtf(val1);
			string n2 = LogicalExpr.Rtf(val2);
			return LogicalExpr.cmpStringStringE(op, n, n2);
		}

		// Token: 0x0600012D RID: 301 RVA: 0x00004EB4 File Offset: 0x000030B4
		private static bool cmpStringStringO(Operator.Op op, object val1, object val2)
		{
			double n = NumberFunctions.Number((string)val1);
			double n2 = NumberFunctions.Number((string)val2);
			return LogicalExpr.cmpNumberNumberO(op, n, n2);
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00004EE4 File Offset: 0x000030E4
		private static bool cmpRtfStringO(Operator.Op op, object val1, object val2)
		{
			double n = NumberFunctions.Number(LogicalExpr.Rtf(val1));
			double n2 = NumberFunctions.Number((string)val2);
			return LogicalExpr.cmpNumberNumberO(op, n, n2);
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00004F14 File Offset: 0x00003114
		private static bool cmpRtfRtfO(Operator.Op op, object val1, object val2)
		{
			double n = NumberFunctions.Number(LogicalExpr.Rtf(val1));
			double n2 = NumberFunctions.Number(LogicalExpr.Rtf(val2));
			return LogicalExpr.cmpNumberNumberO(op, n, n2);
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00004F41 File Offset: 0x00003141
		public override XPathNodeIterator Clone()
		{
			return new LogicalExpr(this);
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00004F49 File Offset: 0x00003149
		private static string Rtf(object o)
		{
			return ((XPathNavigator)o).Value;
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000132 RID: 306 RVA: 0x0000284A File Offset: 0x00000A4A
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.Boolean;
			}
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00004F58 File Offset: 0x00003158
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			w.WriteAttributeString("op", this.op.ToString());
			this.opnd1.PrintQuery(w);
			this.opnd2.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00004FB0 File Offset: 0x000031B0
		// Note: this type is marked as 'beforefieldinit'.
		static LogicalExpr()
		{
			LogicalExpr.cmpXslt[][] array = new LogicalExpr.cmpXslt[5][];
			int num = 0;
			LogicalExpr.cmpXslt[] array2 = new LogicalExpr.cmpXslt[5];
			array2[0] = new LogicalExpr.cmpXslt(LogicalExpr.cmpNumberNumber);
			array[num] = array2;
			int num2 = 1;
			LogicalExpr.cmpXslt[] array3 = new LogicalExpr.cmpXslt[5];
			array3[0] = new LogicalExpr.cmpXslt(LogicalExpr.cmpStringNumber);
			array3[1] = new LogicalExpr.cmpXslt(LogicalExpr.cmpStringStringE);
			array[num2] = array3;
			int num3 = 2;
			LogicalExpr.cmpXslt[] array4 = new LogicalExpr.cmpXslt[5];
			array4[0] = new LogicalExpr.cmpXslt(LogicalExpr.cmpBoolNumberE);
			array4[1] = new LogicalExpr.cmpXslt(LogicalExpr.cmpBoolStringE);
			array4[2] = new LogicalExpr.cmpXslt(LogicalExpr.cmpBoolBoolE);
			array[num3] = array4;
			int num4 = 3;
			LogicalExpr.cmpXslt[] array5 = new LogicalExpr.cmpXslt[5];
			array5[0] = new LogicalExpr.cmpXslt(LogicalExpr.cmpQueryNumber);
			array5[1] = new LogicalExpr.cmpXslt(LogicalExpr.cmpQueryStringE);
			array5[2] = new LogicalExpr.cmpXslt(LogicalExpr.cmpQueryBoolE);
			array5[3] = new LogicalExpr.cmpXslt(LogicalExpr.cmpQueryQueryE);
			array[num4] = array5;
			array[4] = new LogicalExpr.cmpXslt[]
			{
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfNumber),
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfStringE),
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfBoolE),
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfQueryE),
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfRtfE)
			};
			LogicalExpr.CompXsltE = array;
			LogicalExpr.cmpXslt[][] array6 = new LogicalExpr.cmpXslt[5][];
			int num5 = 0;
			LogicalExpr.cmpXslt[] array7 = new LogicalExpr.cmpXslt[5];
			array7[0] = new LogicalExpr.cmpXslt(LogicalExpr.cmpNumberNumber);
			array6[num5] = array7;
			int num6 = 1;
			LogicalExpr.cmpXslt[] array8 = new LogicalExpr.cmpXslt[5];
			array8[0] = new LogicalExpr.cmpXslt(LogicalExpr.cmpStringNumber);
			array8[1] = new LogicalExpr.cmpXslt(LogicalExpr.cmpStringStringO);
			array6[num6] = array8;
			int num7 = 2;
			LogicalExpr.cmpXslt[] array9 = new LogicalExpr.cmpXslt[5];
			array9[0] = new LogicalExpr.cmpXslt(LogicalExpr.cmpBoolNumberO);
			array9[1] = new LogicalExpr.cmpXslt(LogicalExpr.cmpBoolStringO);
			array9[2] = new LogicalExpr.cmpXslt(LogicalExpr.cmpBoolBoolO);
			array6[num7] = array9;
			int num8 = 3;
			LogicalExpr.cmpXslt[] array10 = new LogicalExpr.cmpXslt[5];
			array10[0] = new LogicalExpr.cmpXslt(LogicalExpr.cmpQueryNumber);
			array10[1] = new LogicalExpr.cmpXslt(LogicalExpr.cmpQueryStringO);
			array10[2] = new LogicalExpr.cmpXslt(LogicalExpr.cmpQueryBoolO);
			array10[3] = new LogicalExpr.cmpXslt(LogicalExpr.cmpQueryQueryO);
			array6[num8] = array10;
			array6[4] = new LogicalExpr.cmpXslt[]
			{
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfNumber),
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfStringO),
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfBoolO),
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfQueryO),
				new LogicalExpr.cmpXslt(LogicalExpr.cmpRtfRtfO)
			};
			LogicalExpr.CompXsltO = array6;
		}

		// Token: 0x040000B1 RID: 177
		private Operator.Op op;

		// Token: 0x040000B2 RID: 178
		private Query opnd1;

		// Token: 0x040000B3 RID: 179
		private Query opnd2;

		// Token: 0x040000B4 RID: 180
		private static readonly LogicalExpr.cmpXslt[][] CompXsltE;

		// Token: 0x040000B5 RID: 181
		private static readonly LogicalExpr.cmpXslt[][] CompXsltO;

		// Token: 0x0200002E RID: 46
		// (Invoke) Token: 0x06000136 RID: 310
		private delegate bool cmpXslt(Operator.Op op, object val1, object val2);

		// Token: 0x0200002F RID: 47
		private struct NodeSet
		{
			// Token: 0x06000139 RID: 313 RVA: 0x000051EF File Offset: 0x000033EF
			public NodeSet(object opnd)
			{
				this.opnd = (Query)opnd;
				this.current = null;
			}

			// Token: 0x0600013A RID: 314 RVA: 0x00005204 File Offset: 0x00003404
			public bool MoveNext()
			{
				this.current = this.opnd.Advance();
				return this.current != null;
			}

			// Token: 0x0600013B RID: 315 RVA: 0x00005220 File Offset: 0x00003420
			public void Reset()
			{
				this.opnd.Reset();
			}

			// Token: 0x17000049 RID: 73
			// (get) Token: 0x0600013C RID: 316 RVA: 0x0000522D File Offset: 0x0000342D
			public string Value
			{
				get
				{
					return this.current.Value;
				}
			}

			// Token: 0x040000B6 RID: 182
			private Query opnd;

			// Token: 0x040000B7 RID: 183
			private XPathNavigator current;
		}
	}
}
