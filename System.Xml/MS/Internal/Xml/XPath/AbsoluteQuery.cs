﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000009 RID: 9
	internal sealed class AbsoluteQuery : ContextQuery
	{
		// Token: 0x06000012 RID: 18 RVA: 0x00002253 File Offset: 0x00000453
		public AbsoluteQuery()
		{
		}

		// Token: 0x06000013 RID: 19 RVA: 0x0000225B File Offset: 0x0000045B
		private AbsoluteQuery(AbsoluteQuery other) : base(other)
		{
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002264 File Offset: 0x00000464
		public override object Evaluate(XPathNodeIterator context)
		{
			this.contextNode = context.Current.Clone();
			this.contextNode.MoveToRoot();
			this.count = 0;
			return this;
		}

		// Token: 0x06000015 RID: 21 RVA: 0x0000228A File Offset: 0x0000048A
		public override XPathNavigator MatchNode(XPathNavigator context)
		{
			if (context != null && context.NodeType == XPathNodeType.Root)
			{
				return context;
			}
			return null;
		}

		// Token: 0x06000016 RID: 22 RVA: 0x0000229A File Offset: 0x0000049A
		public override XPathNodeIterator Clone()
		{
			return new AbsoluteQuery(this);
		}
	}
}
