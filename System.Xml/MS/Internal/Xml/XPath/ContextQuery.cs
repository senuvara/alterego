﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200001A RID: 26
	internal class ContextQuery : Query
	{
		// Token: 0x0600008E RID: 142 RVA: 0x0000337B File Offset: 0x0000157B
		public ContextQuery()
		{
			this.count = 0;
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0000338A File Offset: 0x0000158A
		protected ContextQuery(ContextQuery other) : base(other)
		{
			this.contextNode = other.contextNode;
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00002B15 File Offset: 0x00000D15
		public override void Reset()
		{
			this.count = 0;
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000091 RID: 145 RVA: 0x0000339F File Offset: 0x0000159F
		public override XPathNavigator Current
		{
			get
			{
				return this.contextNode;
			}
		}

		// Token: 0x06000092 RID: 146 RVA: 0x000033A7 File Offset: 0x000015A7
		public override object Evaluate(XPathNodeIterator context)
		{
			this.contextNode = context.Current;
			this.count = 0;
			return this;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x000033BD File Offset: 0x000015BD
		public override XPathNavigator Advance()
		{
			if (this.count == 0)
			{
				this.count = 1;
				return this.contextNode;
			}
			return null;
		}

		// Token: 0x06000094 RID: 148 RVA: 0x0000206B File Offset: 0x0000026B
		public override XPathNavigator MatchNode(XPathNavigator current)
		{
			return current;
		}

		// Token: 0x06000095 RID: 149 RVA: 0x000033D6 File Offset: 0x000015D6
		public override XPathNodeIterator Clone()
		{
			return new ContextQuery(this);
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000096 RID: 150 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000097 RID: 151 RVA: 0x00002B91 File Offset: 0x00000D91
		public override int CurrentPosition
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000098 RID: 152 RVA: 0x000033DE File Offset: 0x000015DE
		public override int Count
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00002BA6 File Offset: 0x00000DA6
		public override QueryProps Properties
		{
			get
			{
				return (QueryProps)23;
			}
		}

		// Token: 0x04000077 RID: 119
		protected XPathNavigator contextNode;
	}
}
