﻿using System;
using System.Collections.Generic;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000012 RID: 18
	internal abstract class CacheAxisQuery : BaseAxisQuery
	{
		// Token: 0x06000051 RID: 81 RVA: 0x00002AD0 File Offset: 0x00000CD0
		public CacheAxisQuery(Query qyInput, string name, string prefix, XPathNodeType typeTest) : base(qyInput, name, prefix, typeTest)
		{
			this.outputBuffer = new List<XPathNavigator>();
			this.count = 0;
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00002AEF File Offset: 0x00000CEF
		protected CacheAxisQuery(CacheAxisQuery other) : base(other)
		{
			this.outputBuffer = new List<XPathNavigator>(other.outputBuffer);
			this.count = other.count;
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00002B15 File Offset: 0x00000D15
		public override void Reset()
		{
			this.count = 0;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00002B1E File Offset: 0x00000D1E
		public override object Evaluate(XPathNodeIterator context)
		{
			base.Evaluate(context);
			this.outputBuffer.Clear();
			return this;
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00002B34 File Offset: 0x00000D34
		public override XPathNavigator Advance()
		{
			if (this.count < this.outputBuffer.Count)
			{
				List<XPathNavigator> list = this.outputBuffer;
				int count = this.count;
				this.count = count + 1;
				return list[count];
			}
			return null;
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000056 RID: 86 RVA: 0x00002B72 File Offset: 0x00000D72
		public override XPathNavigator Current
		{
			get
			{
				if (this.count == 0)
				{
					return null;
				}
				return this.outputBuffer[this.count - 1];
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00002B91 File Offset: 0x00000D91
		public override int CurrentPosition
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000058 RID: 88 RVA: 0x00002B99 File Offset: 0x00000D99
		public override int Count
		{
			get
			{
				return this.outputBuffer.Count;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00002BA6 File Offset: 0x00000DA6
		public override QueryProps Properties
		{
			get
			{
				return (QueryProps)23;
			}
		}

		// Token: 0x04000067 RID: 103
		protected List<XPathNavigator> outputBuffer;
	}
}
