﻿using System;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200003C RID: 60
	internal enum QueryProps
	{
		// Token: 0x040000DC RID: 220
		None,
		// Token: 0x040000DD RID: 221
		Position,
		// Token: 0x040000DE RID: 222
		Count,
		// Token: 0x040000DF RID: 223
		Cached = 4,
		// Token: 0x040000E0 RID: 224
		Reverse = 8,
		// Token: 0x040000E1 RID: 225
		Merge = 16
	}
}
