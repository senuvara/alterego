﻿using System;
using System.Xml;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200001B RID: 27
	internal abstract class DescendantBaseQuery : BaseAxisQuery
	{
		// Token: 0x0600009A RID: 154 RVA: 0x000033E1 File Offset: 0x000015E1
		public DescendantBaseQuery(Query qyParent, string Name, string Prefix, XPathNodeType Type, bool matchSelf, bool abbrAxis) : base(qyParent, Name, Prefix, Type)
		{
			this.matchSelf = matchSelf;
			this.abbrAxis = abbrAxis;
		}

		// Token: 0x0600009B RID: 155 RVA: 0x000033FE File Offset: 0x000015FE
		public DescendantBaseQuery(DescendantBaseQuery other) : base(other)
		{
			this.matchSelf = other.matchSelf;
			this.abbrAxis = other.abbrAxis;
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00003420 File Offset: 0x00001620
		public override XPathNavigator MatchNode(XPathNavigator context)
		{
			if (context != null)
			{
				if (!this.abbrAxis)
				{
					throw XPathException.Create("'{0}' is an invalid XSLT pattern.");
				}
				if (this.matches(context))
				{
					XPathNavigator result;
					if (this.matchSelf && (result = this.qyInput.MatchNode(context)) != null)
					{
						return result;
					}
					XPathNavigator xpathNavigator = context.Clone();
					while (xpathNavigator.MoveToParent())
					{
						if ((result = this.qyInput.MatchNode(xpathNavigator)) != null)
						{
							return result;
						}
					}
				}
			}
			return null;
		}

		// Token: 0x0600009D RID: 157 RVA: 0x0000348C File Offset: 0x0000168C
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			if (this.matchSelf)
			{
				w.WriteAttributeString("self", "yes");
			}
			if (base.NameTest)
			{
				w.WriteAttributeString("name", (base.Prefix.Length != 0) ? (base.Prefix + ":" + base.Name) : base.Name);
			}
			if (base.TypeTest != XPathNodeType.Element)
			{
				w.WriteAttributeString("nodeType", base.TypeTest.ToString());
			}
			this.qyInput.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x04000078 RID: 120
		protected bool matchSelf;

		// Token: 0x04000079 RID: 121
		protected bool abbrAxis;
	}
}
