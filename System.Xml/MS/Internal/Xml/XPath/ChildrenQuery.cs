﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000015 RID: 21
	internal class ChildrenQuery : BaseAxisQuery
	{
		// Token: 0x0600006D RID: 109 RVA: 0x00002EFB File Offset: 0x000010FB
		public ChildrenQuery(Query qyInput, string name, string prefix, XPathNodeType type) : base(qyInput, name, prefix, type)
		{
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00002F13 File Offset: 0x00001113
		protected ChildrenQuery(ChildrenQuery other) : base(other)
		{
			this.iterator = Query.Clone(other.iterator);
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00002F38 File Offset: 0x00001138
		public override void Reset()
		{
			this.iterator = XPathEmptyIterator.Instance;
			base.Reset();
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00002F4C File Offset: 0x0000114C
		public override XPathNavigator Advance()
		{
			while (!this.iterator.MoveNext())
			{
				XPathNavigator xpathNavigator = this.qyInput.Advance();
				if (xpathNavigator == null)
				{
					return null;
				}
				if (base.NameTest)
				{
					if (base.TypeTest == XPathNodeType.ProcessingInstruction)
					{
						this.iterator = new IteratorFilter(xpathNavigator.SelectChildren(base.TypeTest), base.Name);
					}
					else
					{
						this.iterator = xpathNavigator.SelectChildren(base.Name, base.Namespace);
					}
				}
				else
				{
					this.iterator = xpathNavigator.SelectChildren(base.TypeTest);
				}
				this.position = 0;
			}
			this.position++;
			this.currentNode = this.iterator.Current;
			return this.currentNode;
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00003004 File Offset: 0x00001204
		public sealed override XPathNavigator MatchNode(XPathNavigator context)
		{
			if (context == null || !this.matches(context))
			{
				return null;
			}
			XPathNavigator xpathNavigator = context.Clone();
			if (xpathNavigator.NodeType != XPathNodeType.Attribute && xpathNavigator.MoveToParent())
			{
				return this.qyInput.MatchNode(xpathNavigator);
			}
			return null;
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00003045 File Offset: 0x00001245
		public override XPathNodeIterator Clone()
		{
			return new ChildrenQuery(this);
		}

		// Token: 0x0400006E RID: 110
		private XPathNodeIterator iterator = XPathEmptyIterator.Instance;
	}
}
