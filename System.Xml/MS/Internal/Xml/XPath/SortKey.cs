﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000045 RID: 69
	internal sealed class SortKey
	{
		// Token: 0x060001CB RID: 459 RVA: 0x00006EDB File Offset: 0x000050DB
		public SortKey(int numKeys, int originalPosition, XPathNavigator node)
		{
			this.numKeys = numKeys;
			this.keys = new object[numKeys];
			this.originalPosition = originalPosition;
			this.node = node;
		}

		// Token: 0x17000066 RID: 102
		public object this[int index]
		{
			get
			{
				return this.keys[index];
			}
			set
			{
				this.keys[index] = value;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060001CE RID: 462 RVA: 0x00006F19 File Offset: 0x00005119
		public int NumKeys
		{
			get
			{
				return this.numKeys;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001CF RID: 463 RVA: 0x00006F21 File Offset: 0x00005121
		public int OriginalPosition
		{
			get
			{
				return this.originalPosition;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060001D0 RID: 464 RVA: 0x00006F29 File Offset: 0x00005129
		public XPathNavigator Node
		{
			get
			{
				return this.node;
			}
		}

		// Token: 0x040000F9 RID: 249
		private int numKeys;

		// Token: 0x040000FA RID: 250
		private object[] keys;

		// Token: 0x040000FB RID: 251
		private int originalPosition;

		// Token: 0x040000FC RID: 252
		private XPathNavigator node;
	}
}
