﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200000D RID: 13
	internal class Axis : AstNode
	{
		// Token: 0x06000020 RID: 32 RVA: 0x000023B3 File Offset: 0x000005B3
		public Axis(Axis.AxisType axisType, AstNode input, string prefix, string name, XPathNodeType nodetype)
		{
			this.axisType = axisType;
			this.input = input;
			this.prefix = prefix;
			this.name = name;
			this.nodeType = nodetype;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000023EB File Offset: 0x000005EB
		public Axis(Axis.AxisType axisType, AstNode input) : this(axisType, input, string.Empty, string.Empty, XPathNodeType.All)
		{
			this.abbrAxis = true;
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000022 RID: 34 RVA: 0x000020CD File Offset: 0x000002CD
		public override AstNode.AstType Type
		{
			get
			{
				return AstNode.AstType.Axis;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000023 RID: 35 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType ReturnType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000024 RID: 36 RVA: 0x0000240B File Offset: 0x0000060B
		// (set) Token: 0x06000025 RID: 37 RVA: 0x00002413 File Offset: 0x00000613
		public AstNode Input
		{
			get
			{
				return this.input;
			}
			set
			{
				this.input = value;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000026 RID: 38 RVA: 0x0000241C File Offset: 0x0000061C
		public string Prefix
		{
			get
			{
				return this.prefix;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000027 RID: 39 RVA: 0x00002424 File Offset: 0x00000624
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000028 RID: 40 RVA: 0x0000242C File Offset: 0x0000062C
		public XPathNodeType NodeType
		{
			get
			{
				return this.nodeType;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000029 RID: 41 RVA: 0x00002434 File Offset: 0x00000634
		public Axis.AxisType TypeOfAxis
		{
			get
			{
				return this.axisType;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600002A RID: 42 RVA: 0x0000243C File Offset: 0x0000063C
		public bool AbbrAxis
		{
			get
			{
				return this.abbrAxis;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x0600002B RID: 43 RVA: 0x00002444 File Offset: 0x00000644
		// (set) Token: 0x0600002C RID: 44 RVA: 0x0000244C File Offset: 0x0000064C
		public string Urn
		{
			get
			{
				return this.urn;
			}
			set
			{
				this.urn = value;
			}
		}

		// Token: 0x04000044 RID: 68
		private Axis.AxisType axisType;

		// Token: 0x04000045 RID: 69
		private AstNode input;

		// Token: 0x04000046 RID: 70
		private string prefix;

		// Token: 0x04000047 RID: 71
		private string name;

		// Token: 0x04000048 RID: 72
		private XPathNodeType nodeType;

		// Token: 0x04000049 RID: 73
		protected bool abbrAxis;

		// Token: 0x0400004A RID: 74
		private string urn = string.Empty;

		// Token: 0x0200000E RID: 14
		public enum AxisType
		{
			// Token: 0x0400004C RID: 76
			Ancestor,
			// Token: 0x0400004D RID: 77
			AncestorOrSelf,
			// Token: 0x0400004E RID: 78
			Attribute,
			// Token: 0x0400004F RID: 79
			Child,
			// Token: 0x04000050 RID: 80
			Descendant,
			// Token: 0x04000051 RID: 81
			DescendantOrSelf,
			// Token: 0x04000052 RID: 82
			Following,
			// Token: 0x04000053 RID: 83
			FollowingSibling,
			// Token: 0x04000054 RID: 84
			Namespace,
			// Token: 0x04000055 RID: 85
			Parent,
			// Token: 0x04000056 RID: 86
			Preceding,
			// Token: 0x04000057 RID: 87
			PrecedingSibling,
			// Token: 0x04000058 RID: 88
			Self,
			// Token: 0x04000059 RID: 89
			None
		}
	}
}
