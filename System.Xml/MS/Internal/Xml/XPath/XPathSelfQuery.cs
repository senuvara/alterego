﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000059 RID: 89
	internal sealed class XPathSelfQuery : BaseAxisQuery
	{
		// Token: 0x0600028C RID: 652 RVA: 0x000022A2 File Offset: 0x000004A2
		public XPathSelfQuery(Query qyInput, string Name, string Prefix, XPathNodeType Type) : base(qyInput, Name, Prefix, Type)
		{
		}

		// Token: 0x0600028D RID: 653 RVA: 0x00004658 File Offset: 0x00002858
		private XPathSelfQuery(XPathSelfQuery other) : base(other)
		{
		}

		// Token: 0x0600028E RID: 654 RVA: 0x00009FC8 File Offset: 0x000081C8
		public override XPathNavigator Advance()
		{
			while ((this.currentNode = this.qyInput.Advance()) != null)
			{
				if (this.matches(this.currentNode))
				{
					this.position = 1;
					return this.currentNode;
				}
			}
			return null;
		}

		// Token: 0x0600028F RID: 655 RVA: 0x0000A00A File Offset: 0x0000820A
		public override XPathNodeIterator Clone()
		{
			return new XPathSelfQuery(this);
		}
	}
}
