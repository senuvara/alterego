﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000031 RID: 49
	internal sealed class NamespaceQuery : BaseAxisQuery
	{
		// Token: 0x06000144 RID: 324 RVA: 0x000022A2 File Offset: 0x000004A2
		public NamespaceQuery(Query qyParent, string Name, string Prefix, XPathNodeType Type) : base(qyParent, Name, Prefix, Type)
		{
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00005370 File Offset: 0x00003570
		private NamespaceQuery(NamespaceQuery other) : base(other)
		{
			this.onNamespace = other.onNamespace;
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00005385 File Offset: 0x00003585
		public override void Reset()
		{
			this.onNamespace = false;
			base.Reset();
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00005394 File Offset: 0x00003594
		public override XPathNavigator Advance()
		{
			for (;;)
			{
				if (!this.onNamespace)
				{
					this.currentNode = this.qyInput.Advance();
					if (this.currentNode == null)
					{
						break;
					}
					this.position = 0;
					this.currentNode = this.currentNode.Clone();
					this.onNamespace = this.currentNode.MoveToFirstNamespace();
				}
				else
				{
					this.onNamespace = this.currentNode.MoveToNextNamespace();
				}
				if (this.onNamespace && this.matches(this.currentNode))
				{
					goto Block_3;
				}
			}
			return null;
			Block_3:
			this.position++;
			return this.currentNode;
		}

		// Token: 0x06000148 RID: 328 RVA: 0x0000542A File Offset: 0x0000362A
		public override bool matches(XPathNavigator e)
		{
			return e.Value.Length != 0 && (!base.NameTest || base.Name.Equals(e.LocalName));
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00005456 File Offset: 0x00003656
		public override XPathNodeIterator Clone()
		{
			return new NamespaceQuery(this);
		}

		// Token: 0x040000B9 RID: 185
		private bool onNamespace;
	}
}
