﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200003A RID: 58
	internal sealed class PrecedingQuery : BaseAxisQuery
	{
		// Token: 0x0600017E RID: 382 RVA: 0x00005BC1 File Offset: 0x00003DC1
		public PrecedingQuery(Query qyInput, string name, string prefix, XPathNodeType typeTest) : base(qyInput, name, prefix, typeTest)
		{
			this.ancestorStk = new ClonableStack<XPathNavigator>();
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00005BD9 File Offset: 0x00003DD9
		private PrecedingQuery(PrecedingQuery other) : base(other)
		{
			this.workIterator = Query.Clone(other.workIterator);
			this.ancestorStk = other.ancestorStk.Clone();
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00005C04 File Offset: 0x00003E04
		public override void Reset()
		{
			this.workIterator = null;
			this.ancestorStk.Clear();
			base.Reset();
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00005C20 File Offset: 0x00003E20
		public override XPathNavigator Advance()
		{
			if (this.workIterator == null)
			{
				XPathNavigator xpathNavigator = this.qyInput.Advance();
				if (xpathNavigator == null)
				{
					return null;
				}
				XPathNavigator xpathNavigator2 = xpathNavigator.Clone();
				do
				{
					xpathNavigator2.MoveTo(xpathNavigator);
				}
				while ((xpathNavigator = this.qyInput.Advance()) != null);
				if (xpathNavigator2.NodeType == XPathNodeType.Attribute || xpathNavigator2.NodeType == XPathNodeType.Namespace)
				{
					xpathNavigator2.MoveToParent();
				}
				do
				{
					this.ancestorStk.Push(xpathNavigator2.Clone());
				}
				while (xpathNavigator2.MoveToParent());
				this.workIterator = xpathNavigator2.SelectDescendants(XPathNodeType.All, true);
			}
			while (this.workIterator.MoveNext())
			{
				this.currentNode = this.workIterator.Current;
				if (this.currentNode.IsSamePosition(this.ancestorStk.Peek()))
				{
					this.ancestorStk.Pop();
					if (this.ancestorStk.Count == 0)
					{
						this.currentNode = null;
						this.workIterator = null;
						return null;
					}
				}
				else if (this.matches(this.currentNode))
				{
					this.position++;
					return this.currentNode;
				}
			}
			return null;
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00005D30 File Offset: 0x00003F30
		public override XPathNodeIterator Clone()
		{
			return new PrecedingQuery(this);
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00005D38 File Offset: 0x00003F38
		public override QueryProps Properties
		{
			get
			{
				return base.Properties | QueryProps.Reverse;
			}
		}

		// Token: 0x040000D9 RID: 217
		private XPathNodeIterator workIterator;

		// Token: 0x040000DA RID: 218
		private ClonableStack<XPathNavigator> ancestorStk;
	}
}
