﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200004C RID: 76
	internal class XPathAncestorIterator : XPathAxisIterator
	{
		// Token: 0x0600020E RID: 526 RVA: 0x00007CC6 File Offset: 0x00005EC6
		public XPathAncestorIterator(XPathNavigator nav, XPathNodeType type, bool matchSelf) : base(nav, type, matchSelf)
		{
		}

		// Token: 0x0600020F RID: 527 RVA: 0x00007CD1 File Offset: 0x00005ED1
		public XPathAncestorIterator(XPathNavigator nav, string name, string namespaceURI, bool matchSelf) : base(nav, name, namespaceURI, matchSelf)
		{
		}

		// Token: 0x06000210 RID: 528 RVA: 0x00007CDE File Offset: 0x00005EDE
		public XPathAncestorIterator(XPathAncestorIterator other) : base(other)
		{
		}

		// Token: 0x06000211 RID: 529 RVA: 0x00007CE8 File Offset: 0x00005EE8
		public override bool MoveNext()
		{
			if (this.first)
			{
				this.first = false;
				if (this.matchSelf && this.Matches)
				{
					this.position = 1;
					return true;
				}
			}
			while (this.nav.MoveToParent())
			{
				if (this.Matches)
				{
					this.position++;
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000212 RID: 530 RVA: 0x00007D43 File Offset: 0x00005F43
		public override XPathNodeIterator Clone()
		{
			return new XPathAncestorIterator(this);
		}
	}
}
