﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000039 RID: 57
	internal sealed class ParentQuery : CacheAxisQuery
	{
		// Token: 0x0600017A RID: 378 RVA: 0x00005B56 File Offset: 0x00003D56
		public ParentQuery(Query qyInput, string Name, string Prefix, XPathNodeType Type) : base(qyInput, Name, Prefix, Type)
		{
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00005B63 File Offset: 0x00003D63
		private ParentQuery(ParentQuery other) : base(other)
		{
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00005B6C File Offset: 0x00003D6C
		public override object Evaluate(XPathNodeIterator context)
		{
			base.Evaluate(context);
			XPathNavigator xpathNavigator;
			while ((xpathNavigator = this.qyInput.Advance()) != null)
			{
				xpathNavigator = xpathNavigator.Clone();
				if (xpathNavigator.MoveToParent() && this.matches(xpathNavigator))
				{
					base.Insert(this.outputBuffer, xpathNavigator);
				}
			}
			return this;
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00005BB9 File Offset: 0x00003DB9
		public override XPathNodeIterator Clone()
		{
			return new ParentQuery(this);
		}
	}
}
