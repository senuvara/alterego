﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200001D RID: 29
	internal sealed class DescendantOverDescendantQuery : DescendantBaseQuery
	{
		// Token: 0x060000A3 RID: 163 RVA: 0x00003658 File Offset: 0x00001858
		public DescendantOverDescendantQuery(Query qyParent, bool matchSelf, string name, string prefix, XPathNodeType typeTest, bool abbrAxis) : base(qyParent, name, prefix, typeTest, matchSelf, abbrAxis)
		{
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x00003669 File Offset: 0x00001869
		private DescendantOverDescendantQuery(DescendantOverDescendantQuery other) : base(other)
		{
			this.level = other.level;
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x0000367E File Offset: 0x0000187E
		public override void Reset()
		{
			this.level = 0;
			base.Reset();
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x00003690 File Offset: 0x00001890
		public override XPathNavigator Advance()
		{
			for (;;)
			{
				IL_00:
				if (this.level == 0)
				{
					this.currentNode = this.qyInput.Advance();
					this.position = 0;
					if (this.currentNode == null)
					{
						break;
					}
					if (this.matchSelf && this.matches(this.currentNode))
					{
						goto Block_3;
					}
					this.currentNode = this.currentNode.Clone();
					if (!this.MoveToFirstChild())
					{
						continue;
					}
				}
				else if (!this.MoveUpUntillNext())
				{
					continue;
				}
				while (!this.matches(this.currentNode))
				{
					if (!this.MoveToFirstChild())
					{
						goto IL_00;
					}
				}
				goto Block_5;
			}
			return null;
			Block_3:
			this.position = 1;
			return this.currentNode;
			Block_5:
			this.position++;
			return this.currentNode;
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x0000373D File Offset: 0x0000193D
		private bool MoveToFirstChild()
		{
			if (this.currentNode.MoveToFirstChild())
			{
				this.level++;
				return true;
			}
			return false;
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x0000375D File Offset: 0x0000195D
		private bool MoveUpUntillNext()
		{
			while (!this.currentNode.MoveToNext())
			{
				this.level--;
				if (this.level == 0)
				{
					return false;
				}
				this.currentNode.MoveToParent();
			}
			return true;
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00003793 File Offset: 0x00001993
		public override XPathNodeIterator Clone()
		{
			return new DescendantOverDescendantQuery(this);
		}

		// Token: 0x0400007B RID: 123
		private int level;
	}
}
