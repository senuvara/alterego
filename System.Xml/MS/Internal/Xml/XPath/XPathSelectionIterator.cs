﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000058 RID: 88
	internal class XPathSelectionIterator : ResetableIterator
	{
		// Token: 0x06000284 RID: 644 RVA: 0x00009EF9 File Offset: 0x000080F9
		internal XPathSelectionIterator(XPathNavigator nav, Query query)
		{
			this.nav = nav.Clone();
			this.query = query;
		}

		// Token: 0x06000285 RID: 645 RVA: 0x00009F14 File Offset: 0x00008114
		protected XPathSelectionIterator(XPathSelectionIterator it)
		{
			this.nav = it.nav.Clone();
			this.query = (Query)it.query.Clone();
			this.position = it.position;
		}

		// Token: 0x06000286 RID: 646 RVA: 0x00009F4F File Offset: 0x0000814F
		public override void Reset()
		{
			this.query.Reset();
		}

		// Token: 0x06000287 RID: 647 RVA: 0x00009F5C File Offset: 0x0000815C
		public override bool MoveNext()
		{
			XPathNavigator xpathNavigator = this.query.Advance();
			if (xpathNavigator != null)
			{
				this.position++;
				if (!this.nav.MoveTo(xpathNavigator))
				{
					this.nav = xpathNavigator.Clone();
				}
				return true;
			}
			return false;
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000288 RID: 648 RVA: 0x00009FA3 File Offset: 0x000081A3
		public override int Count
		{
			get
			{
				return this.query.Count;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000289 RID: 649 RVA: 0x00009FB0 File Offset: 0x000081B0
		public override XPathNavigator Current
		{
			get
			{
				return this.nav;
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x0600028A RID: 650 RVA: 0x00009FB8 File Offset: 0x000081B8
		public override int CurrentPosition
		{
			get
			{
				return this.position;
			}
		}

		// Token: 0x0600028B RID: 651 RVA: 0x00009FC0 File Offset: 0x000081C0
		public override XPathNodeIterator Clone()
		{
			return new XPathSelectionIterator(this);
		}

		// Token: 0x04000158 RID: 344
		private XPathNavigator nav;

		// Token: 0x04000159 RID: 345
		private Query query;

		// Token: 0x0400015A RID: 346
		private int position;
	}
}
