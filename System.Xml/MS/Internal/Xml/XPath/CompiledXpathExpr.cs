﻿using System;
using System.Collections;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000017 RID: 23
	internal class CompiledXpathExpr : XPathExpression
	{
		// Token: 0x0600007A RID: 122 RVA: 0x000030AC File Offset: 0x000012AC
		internal CompiledXpathExpr(Query query, string expression, bool needContext)
		{
			this.query = query;
			this.expr = expression;
			this.needContext = needContext;
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600007B RID: 123 RVA: 0x000030C9 File Offset: 0x000012C9
		internal Query QueryTree
		{
			get
			{
				if (this.needContext)
				{
					throw XPathException.Create("Namespace Manager or XsltContext needed. This query has a prefix, variable, or user-defined function.");
				}
				return this.query;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600007C RID: 124 RVA: 0x000030E4 File Offset: 0x000012E4
		public override string Expression
		{
			get
			{
				return this.expr;
			}
		}

		// Token: 0x0600007D RID: 125 RVA: 0x000030EC File Offset: 0x000012EC
		public virtual void CheckErrors()
		{
		}

		// Token: 0x0600007E RID: 126 RVA: 0x000030F0 File Offset: 0x000012F0
		public override void AddSort(object expr, IComparer comparer)
		{
			Query evalQuery;
			if (expr is string)
			{
				evalQuery = new QueryBuilder().Build((string)expr, out this.needContext);
			}
			else
			{
				if (!(expr is CompiledXpathExpr))
				{
					throw XPathException.Create("This is an invalid object. Only objects returned from Compile() can be passed as input.");
				}
				evalQuery = ((CompiledXpathExpr)expr).QueryTree;
			}
			SortQuery sortQuery = this.query as SortQuery;
			if (sortQuery == null)
			{
				sortQuery = (this.query = new SortQuery(this.query));
			}
			sortQuery.AddSort(evalQuery, comparer);
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00003169 File Offset: 0x00001369
		public override void AddSort(object expr, XmlSortOrder order, XmlCaseOrder caseOrder, string lang, XmlDataType dataType)
		{
			this.AddSort(expr, new XPathComparerHelper(order, caseOrder, lang, dataType));
		}

		// Token: 0x06000080 RID: 128 RVA: 0x0000317D File Offset: 0x0000137D
		public override XPathExpression Clone()
		{
			return new CompiledXpathExpr(Query.Clone(this.query), this.expr, this.needContext);
		}

		// Token: 0x06000081 RID: 129 RVA: 0x0000319B File Offset: 0x0000139B
		public override void SetContext(XmlNamespaceManager nsManager)
		{
			this.SetContext(nsManager);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x000031A4 File Offset: 0x000013A4
		public override void SetContext(IXmlNamespaceResolver nsResolver)
		{
			XsltContext xsltContext = nsResolver as XsltContext;
			if (xsltContext == null)
			{
				if (nsResolver == null)
				{
					nsResolver = new XmlNamespaceManager(new NameTable());
				}
				xsltContext = new CompiledXpathExpr.UndefinedXsltContext(nsResolver);
			}
			this.query.SetXsltContext(xsltContext);
			this.needContext = false;
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000083 RID: 131 RVA: 0x000031E4 File Offset: 0x000013E4
		public override XPathResultType ReturnType
		{
			get
			{
				return this.query.StaticType;
			}
		}

		// Token: 0x0400006F RID: 111
		private Query query;

		// Token: 0x04000070 RID: 112
		private string expr;

		// Token: 0x04000071 RID: 113
		private bool needContext;

		// Token: 0x02000018 RID: 24
		private class UndefinedXsltContext : XsltContext
		{
			// Token: 0x06000084 RID: 132 RVA: 0x000031F1 File Offset: 0x000013F1
			public UndefinedXsltContext(IXmlNamespaceResolver nsResolver) : base(false)
			{
				this.nsResolver = nsResolver;
			}

			// Token: 0x17000023 RID: 35
			// (get) Token: 0x06000085 RID: 133 RVA: 0x00003201 File Offset: 0x00001401
			public override string DefaultNamespace
			{
				get
				{
					return string.Empty;
				}
			}

			// Token: 0x06000086 RID: 134 RVA: 0x00003208 File Offset: 0x00001408
			public override string LookupNamespace(string prefix)
			{
				if (prefix.Length == 0)
				{
					return string.Empty;
				}
				string text = this.nsResolver.LookupNamespace(prefix);
				if (text == null)
				{
					throw XPathException.Create("Namespace prefix '{0}' is not defined.", prefix);
				}
				return text;
			}

			// Token: 0x06000087 RID: 135 RVA: 0x00003233 File Offset: 0x00001433
			public override IXsltContextVariable ResolveVariable(string prefix, string name)
			{
				throw XPathException.Create("XsltContext is needed for this query because of an unknown function.");
			}

			// Token: 0x06000088 RID: 136 RVA: 0x00003233 File Offset: 0x00001433
			public override IXsltContextFunction ResolveFunction(string prefix, string name, XPathResultType[] ArgTypes)
			{
				throw XPathException.Create("XsltContext is needed for this query because of an unknown function.");
			}

			// Token: 0x17000024 RID: 36
			// (get) Token: 0x06000089 RID: 137 RVA: 0x000020CD File Offset: 0x000002CD
			public override bool Whitespace
			{
				get
				{
					return false;
				}
			}

			// Token: 0x0600008A RID: 138 RVA: 0x000020CD File Offset: 0x000002CD
			public override bool PreserveWhitespace(XPathNavigator node)
			{
				return false;
			}

			// Token: 0x0600008B RID: 139 RVA: 0x0000323F File Offset: 0x0000143F
			public override int CompareDocument(string baseUri, string nextbaseUri)
			{
				return string.CompareOrdinal(baseUri, nextbaseUri);
			}

			// Token: 0x04000072 RID: 114
			private IXmlNamespaceResolver nsResolver;
		}
	}
}
