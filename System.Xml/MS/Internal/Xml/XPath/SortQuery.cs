﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000044 RID: 68
	internal sealed class SortQuery : Query
	{
		// Token: 0x060001BC RID: 444 RVA: 0x00006CB4 File Offset: 0x00004EB4
		public SortQuery(Query qyInput)
		{
			this.results = new List<SortKey>();
			this.comparer = new XPathSortComparer();
			this.qyInput = qyInput;
			this.count = 0;
		}

		// Token: 0x060001BD RID: 445 RVA: 0x00006CE0 File Offset: 0x00004EE0
		private SortQuery(SortQuery other) : base(other)
		{
			this.results = new List<SortKey>(other.results);
			this.comparer = other.comparer.Clone();
			this.qyInput = Query.Clone(other.qyInput);
			this.count = 0;
		}

		// Token: 0x060001BE RID: 446 RVA: 0x00002B15 File Offset: 0x00000D15
		public override void Reset()
		{
			this.count = 0;
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00006D2E File Offset: 0x00004F2E
		public override void SetXsltContext(XsltContext xsltContext)
		{
			this.qyInput.SetXsltContext(xsltContext);
			if (this.qyInput.StaticType != XPathResultType.NodeSet && this.qyInput.StaticType != XPathResultType.Any)
			{
				throw XPathException.Create("Expression must evaluate to a node-set.");
			}
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x00006D64 File Offset: 0x00004F64
		private void BuildResultsList()
		{
			int numSorts = this.comparer.NumSorts;
			XPathNavigator xpathNavigator;
			while ((xpathNavigator = this.qyInput.Advance()) != null)
			{
				SortKey sortKey = new SortKey(numSorts, this.results.Count, xpathNavigator.Clone());
				for (int i = 0; i < numSorts; i++)
				{
					sortKey[i] = this.comparer.Expression(i).Evaluate(this.qyInput);
				}
				this.results.Add(sortKey);
			}
			this.results.Sort(this.comparer);
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x00006DED File Offset: 0x00004FED
		public override object Evaluate(XPathNodeIterator context)
		{
			this.qyInput.Evaluate(context);
			this.results.Clear();
			this.BuildResultsList();
			this.count = 0;
			return this;
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x00006E18 File Offset: 0x00005018
		public override XPathNavigator Advance()
		{
			if (this.count < this.results.Count)
			{
				List<SortKey> list = this.results;
				int count = this.count;
				this.count = count + 1;
				return list[count].Node;
			}
			return null;
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x00006E5B File Offset: 0x0000505B
		public override XPathNavigator Current
		{
			get
			{
				if (this.count == 0)
				{
					return null;
				}
				return this.results[this.count - 1].Node;
			}
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x00006E7F File Offset: 0x0000507F
		internal void AddSort(Query evalQuery, IComparer comparer)
		{
			this.comparer.AddSort(evalQuery, comparer);
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x00006E8E File Offset: 0x0000508E
		public override XPathNodeIterator Clone()
		{
			return new SortQuery(this);
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060001C6 RID: 454 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060001C7 RID: 455 RVA: 0x00002B91 File Offset: 0x00000D91
		public override int CurrentPosition
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060001C8 RID: 456 RVA: 0x00006E96 File Offset: 0x00005096
		public override int Count
		{
			get
			{
				return this.results.Count;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060001C9 RID: 457 RVA: 0x00006EA3 File Offset: 0x000050A3
		public override QueryProps Properties
		{
			get
			{
				return (QueryProps)7;
			}
		}

		// Token: 0x060001CA RID: 458 RVA: 0x00006EA6 File Offset: 0x000050A6
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			this.qyInput.PrintQuery(w);
			w.WriteElementString("XPathSortComparer", "... PrintTree() not implemented ...");
			w.WriteEndElement();
		}

		// Token: 0x040000F6 RID: 246
		private List<SortKey> results;

		// Token: 0x040000F7 RID: 247
		private XPathSortComparer comparer;

		// Token: 0x040000F8 RID: 248
		private Query qyInput;
	}
}
