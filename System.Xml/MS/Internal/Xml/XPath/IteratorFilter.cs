﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200002C RID: 44
	internal class IteratorFilter : XPathNodeIterator
	{
		// Token: 0x06000108 RID: 264 RVA: 0x000047DB File Offset: 0x000029DB
		internal IteratorFilter(XPathNodeIterator innerIterator, string name)
		{
			this.innerIterator = innerIterator;
			this.name = name;
		}

		// Token: 0x06000109 RID: 265 RVA: 0x000047F1 File Offset: 0x000029F1
		private IteratorFilter(IteratorFilter it)
		{
			this.innerIterator = it.innerIterator.Clone();
			this.name = it.name;
			this.position = it.position;
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00004822 File Offset: 0x00002A22
		public override XPathNodeIterator Clone()
		{
			return new IteratorFilter(this);
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600010B RID: 267 RVA: 0x0000482A File Offset: 0x00002A2A
		public override XPathNavigator Current
		{
			get
			{
				return this.innerIterator.Current;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x0600010C RID: 268 RVA: 0x00004837 File Offset: 0x00002A37
		public override int CurrentPosition
		{
			get
			{
				return this.position;
			}
		}

		// Token: 0x0600010D RID: 269 RVA: 0x0000483F File Offset: 0x00002A3F
		public override bool MoveNext()
		{
			while (this.innerIterator.MoveNext())
			{
				if (this.innerIterator.Current.LocalName == this.name)
				{
					this.position++;
					return true;
				}
			}
			return false;
		}

		// Token: 0x040000AE RID: 174
		private XPathNodeIterator innerIterator;

		// Token: 0x040000AF RID: 175
		private string name;

		// Token: 0x040000B0 RID: 176
		private int position;
	}
}
