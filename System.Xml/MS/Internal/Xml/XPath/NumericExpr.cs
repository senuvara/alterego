﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000034 RID: 52
	internal sealed class NumericExpr : ValueQuery
	{
		// Token: 0x0600015F RID: 351 RVA: 0x000058C0 File Offset: 0x00003AC0
		public NumericExpr(Operator.Op op, Query opnd1, Query opnd2)
		{
			if (opnd1.StaticType != XPathResultType.Number)
			{
				opnd1 = new NumberFunctions(Function.FunctionType.FuncNumber, opnd1);
			}
			if (opnd2.StaticType != XPathResultType.Number)
			{
				opnd2 = new NumberFunctions(Function.FunctionType.FuncNumber, opnd2);
			}
			this.op = op;
			this.opnd1 = opnd1;
			this.opnd2 = opnd2;
		}

		// Token: 0x06000160 RID: 352 RVA: 0x0000590C File Offset: 0x00003B0C
		private NumericExpr(NumericExpr other) : base(other)
		{
			this.op = other.op;
			this.opnd1 = Query.Clone(other.opnd1);
			this.opnd2 = Query.Clone(other.opnd2);
		}

		// Token: 0x06000161 RID: 353 RVA: 0x00005943 File Offset: 0x00003B43
		public override void SetXsltContext(XsltContext context)
		{
			this.opnd1.SetXsltContext(context);
			this.opnd2.SetXsltContext(context);
		}

		// Token: 0x06000162 RID: 354 RVA: 0x0000595D File Offset: 0x00003B5D
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			return NumericExpr.GetValue(this.op, XmlConvert.ToXPathDouble(this.opnd1.Evaluate(nodeIterator)), XmlConvert.ToXPathDouble(this.opnd2.Evaluate(nodeIterator)));
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00005991 File Offset: 0x00003B91
		private static double GetValue(Operator.Op op, double n1, double n2)
		{
			switch (op)
			{
			case Operator.Op.PLUS:
				return n1 + n2;
			case Operator.Op.MINUS:
				return n1 - n2;
			case Operator.Op.MUL:
				return n1 * n2;
			case Operator.Op.DIV:
				return n1 / n2;
			case Operator.Op.MOD:
				return n1 % n2;
			default:
				return 0.0;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000164 RID: 356 RVA: 0x000020CD File Offset: 0x000002CD
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.Number;
			}
		}

		// Token: 0x06000165 RID: 357 RVA: 0x000059CF File Offset: 0x00003BCF
		public override XPathNodeIterator Clone()
		{
			return new NumericExpr(this);
		}

		// Token: 0x06000166 RID: 358 RVA: 0x000059D8 File Offset: 0x00003BD8
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			w.WriteAttributeString("op", this.op.ToString());
			this.opnd1.PrintQuery(w);
			this.opnd2.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x040000BF RID: 191
		private Operator.Op op;

		// Token: 0x040000C0 RID: 192
		private Query opnd1;

		// Token: 0x040000C1 RID: 193
		private Query opnd2;
	}
}
