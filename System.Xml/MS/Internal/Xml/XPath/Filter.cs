﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000021 RID: 33
	internal class Filter : AstNode
	{
		// Token: 0x060000C3 RID: 195 RVA: 0x00003A82 File Offset: 0x00001C82
		public Filter(AstNode input, AstNode condition)
		{
			this.input = input;
			this.condition = condition;
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x0000284A File Offset: 0x00000A4A
		public override AstNode.AstType Type
		{
			get
			{
				return AstNode.AstType.Filter;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType ReturnType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x00003A98 File Offset: 0x00001C98
		public AstNode Input
		{
			get
			{
				return this.input;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000C7 RID: 199 RVA: 0x00003AA0 File Offset: 0x00001CA0
		public AstNode Condition
		{
			get
			{
				return this.condition;
			}
		}

		// Token: 0x04000080 RID: 128
		private AstNode input;

		// Token: 0x04000081 RID: 129
		private AstNode condition;
	}
}
