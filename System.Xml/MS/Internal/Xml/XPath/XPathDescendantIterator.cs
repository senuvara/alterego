﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000051 RID: 81
	internal class XPathDescendantIterator : XPathAxisIterator
	{
		// Token: 0x06000232 RID: 562 RVA: 0x00007CC6 File Offset: 0x00005EC6
		public XPathDescendantIterator(XPathNavigator nav, XPathNodeType type, bool matchSelf) : base(nav, type, matchSelf)
		{
		}

		// Token: 0x06000233 RID: 563 RVA: 0x00007CD1 File Offset: 0x00005ED1
		public XPathDescendantIterator(XPathNavigator nav, string name, string namespaceURI, bool matchSelf) : base(nav, name, namespaceURI, matchSelf)
		{
		}

		// Token: 0x06000234 RID: 564 RVA: 0x000081F5 File Offset: 0x000063F5
		public XPathDescendantIterator(XPathDescendantIterator it) : base(it)
		{
			this.level = it.level;
		}

		// Token: 0x06000235 RID: 565 RVA: 0x0000820A File Offset: 0x0000640A
		public override XPathNodeIterator Clone()
		{
			return new XPathDescendantIterator(this);
		}

		// Token: 0x06000236 RID: 566 RVA: 0x00008214 File Offset: 0x00006414
		public override bool MoveNext()
		{
			if (this.first)
			{
				this.first = false;
				if (this.matchSelf && this.Matches)
				{
					this.position = 1;
					return true;
				}
			}
			for (;;)
			{
				if (!this.nav.MoveToFirstChild())
				{
					while (this.level != 0)
					{
						if (this.nav.MoveToNext())
						{
							goto IL_78;
						}
						this.nav.MoveToParent();
						this.level--;
					}
					break;
				}
				this.level++;
				IL_78:
				if (this.Matches)
				{
					goto Block_7;
				}
			}
			return false;
			Block_7:
			this.position++;
			return true;
		}

		// Token: 0x04000117 RID: 279
		private int level;
	}
}
