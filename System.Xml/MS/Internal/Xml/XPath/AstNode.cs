﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200000A RID: 10
	internal abstract class AstNode
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000017 RID: 23
		public abstract AstNode.AstType Type { get; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000018 RID: 24
		public abstract XPathResultType ReturnType { get; }

		// Token: 0x06000019 RID: 25 RVA: 0x00002103 File Offset: 0x00000303
		protected AstNode()
		{
		}

		// Token: 0x0200000B RID: 11
		public enum AstType
		{
			// Token: 0x0400003A RID: 58
			Axis,
			// Token: 0x0400003B RID: 59
			Operator,
			// Token: 0x0400003C RID: 60
			Filter,
			// Token: 0x0400003D RID: 61
			ConstantOperand,
			// Token: 0x0400003E RID: 62
			Function,
			// Token: 0x0400003F RID: 63
			Group,
			// Token: 0x04000040 RID: 64
			Root,
			// Token: 0x04000041 RID: 65
			Variable,
			// Token: 0x04000042 RID: 66
			Error
		}
	}
}
