﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000014 RID: 20
	internal abstract class CacheOutputQuery : Query
	{
		// Token: 0x06000061 RID: 97 RVA: 0x00002DE4 File Offset: 0x00000FE4
		public CacheOutputQuery(Query input)
		{
			this.input = input;
			this.outputBuffer = new List<XPathNavigator>();
			this.count = 0;
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002E05 File Offset: 0x00001005
		protected CacheOutputQuery(CacheOutputQuery other) : base(other)
		{
			this.input = Query.Clone(other.input);
			this.outputBuffer = new List<XPathNavigator>(other.outputBuffer);
			this.count = other.count;
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002B15 File Offset: 0x00000D15
		public override void Reset()
		{
			this.count = 0;
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00002E3C File Offset: 0x0000103C
		public override void SetXsltContext(XsltContext context)
		{
			this.input.SetXsltContext(context);
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002E4A File Offset: 0x0000104A
		public override object Evaluate(XPathNodeIterator context)
		{
			this.outputBuffer.Clear();
			this.count = 0;
			return this.input.Evaluate(context);
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00002E6C File Offset: 0x0000106C
		public override XPathNavigator Advance()
		{
			if (this.count < this.outputBuffer.Count)
			{
				List<XPathNavigator> list = this.outputBuffer;
				int count = this.count;
				this.count = count + 1;
				return list[count];
			}
			return null;
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000067 RID: 103 RVA: 0x00002EAA File Offset: 0x000010AA
		public override XPathNavigator Current
		{
			get
			{
				if (this.count == 0)
				{
					return null;
				}
				return this.outputBuffer[this.count - 1];
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000068 RID: 104 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00002B91 File Offset: 0x00000D91
		public override int CurrentPosition
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600006A RID: 106 RVA: 0x00002EC9 File Offset: 0x000010C9
		public override int Count
		{
			get
			{
				return this.outputBuffer.Count;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600006B RID: 107 RVA: 0x00002BA6 File Offset: 0x00000DA6
		public override QueryProps Properties
		{
			get
			{
				return (QueryProps)23;
			}
		}

		// Token: 0x0600006C RID: 108 RVA: 0x00002ED6 File Offset: 0x000010D6
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			this.input.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x0400006C RID: 108
		internal Query input;

		// Token: 0x0400006D RID: 109
		protected List<XPathNavigator> outputBuffer;
	}
}
