﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000043 RID: 67
	internal class Root : AstNode
	{
		// Token: 0x060001B9 RID: 441 RVA: 0x00006CA9 File Offset: 0x00004EA9
		public Root()
		{
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060001BA RID: 442 RVA: 0x00006CB1 File Offset: 0x00004EB1
		public override AstNode.AstType Type
		{
			get
			{
				return AstNode.AstType.Root;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060001BB RID: 443 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType ReturnType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}
	}
}
