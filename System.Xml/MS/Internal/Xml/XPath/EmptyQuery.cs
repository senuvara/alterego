﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200001F RID: 31
	internal sealed class EmptyQuery : Query
	{
		// Token: 0x060000AF RID: 175 RVA: 0x000037FB File Offset: 0x000019FB
		public override XPathNavigator Advance()
		{
			return null;
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00002068 File Offset: 0x00000268
		public override XPathNodeIterator Clone()
		{
			return this;
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00002068 File Offset: 0x00000268
		public override object Evaluate(XPathNodeIterator context)
		{
			return this;
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x000020CD File Offset: 0x000002CD
		public override int CurrentPosition
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x000020CD File Offset: 0x000002CD
		public override int Count
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x00002BA6 File Offset: 0x00000DA6
		public override QueryProps Properties
		{
			get
			{
				return (QueryProps)23;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x000030EC File Offset: 0x000012EC
		public override void Reset()
		{
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000B7 RID: 183 RVA: 0x000037FB File Offset: 0x000019FB
		public override XPathNavigator Current
		{
			get
			{
				return null;
			}
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x000037FE File Offset: 0x000019FE
		public EmptyQuery()
		{
		}
	}
}
