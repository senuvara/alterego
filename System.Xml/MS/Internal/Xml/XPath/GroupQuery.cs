﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200002A RID: 42
	internal sealed class GroupQuery : BaseAxisQuery
	{
		// Token: 0x060000FB RID: 251 RVA: 0x0000464F File Offset: 0x0000284F
		public GroupQuery(Query qy) : base(qy)
		{
		}

		// Token: 0x060000FC RID: 252 RVA: 0x00004658 File Offset: 0x00002858
		private GroupQuery(GroupQuery other) : base(other)
		{
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00004661 File Offset: 0x00002861
		public override XPathNavigator Advance()
		{
			this.currentNode = this.qyInput.Advance();
			if (this.currentNode != null)
			{
				this.position++;
			}
			return this.currentNode;
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00004690 File Offset: 0x00002890
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			return this.qyInput.Evaluate(nodeIterator);
		}

		// Token: 0x060000FF RID: 255 RVA: 0x0000469E File Offset: 0x0000289E
		public override XPathNodeIterator Clone()
		{
			return new GroupQuery(this);
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000100 RID: 256 RVA: 0x000046A6 File Offset: 0x000028A6
		public override XPathResultType StaticType
		{
			get
			{
				return this.qyInput.StaticType;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000101 RID: 257 RVA: 0x000033DE File Offset: 0x000015DE
		public override QueryProps Properties
		{
			get
			{
				return QueryProps.Position;
			}
		}
	}
}
