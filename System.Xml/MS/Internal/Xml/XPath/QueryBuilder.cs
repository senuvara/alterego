﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200003E RID: 62
	internal sealed class QueryBuilder
	{
		// Token: 0x0600019F RID: 415 RVA: 0x0000613D File Offset: 0x0000433D
		private void Reset()
		{
			this.parseDepth = 0;
			this.needContext = false;
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00006150 File Offset: 0x00004350
		private Query ProcessAxis(Axis root, QueryBuilder.Flags flags, out QueryBuilder.Props props)
		{
			if (root.Prefix.Length > 0)
			{
				this.needContext = true;
			}
			this.firstInput = null;
			Query query;
			Query query2;
			if (root.Input != null)
			{
				QueryBuilder.Flags flags2 = QueryBuilder.Flags.None;
				if ((flags & QueryBuilder.Flags.PosFilter) == QueryBuilder.Flags.None)
				{
					Axis axis = root.Input as Axis;
					if (axis != null && root.TypeOfAxis == Axis.AxisType.Child && axis.TypeOfAxis == Axis.AxisType.DescendantOrSelf && axis.NodeType == XPathNodeType.All)
					{
						Query qyParent;
						if (axis.Input != null)
						{
							qyParent = this.ProcessNode(axis.Input, QueryBuilder.Flags.SmartDesc, out props);
						}
						else
						{
							qyParent = new ContextQuery();
							props = QueryBuilder.Props.None;
						}
						query = new DescendantQuery(qyParent, root.Name, root.Prefix, root.NodeType, false, axis.AbbrAxis);
						if ((props & QueryBuilder.Props.NonFlat) != QueryBuilder.Props.None)
						{
							query = new DocumentOrderQuery(query);
						}
						props |= QueryBuilder.Props.NonFlat;
						return query;
					}
					if (root.TypeOfAxis == Axis.AxisType.Descendant || root.TypeOfAxis == Axis.AxisType.DescendantOrSelf)
					{
						flags2 |= QueryBuilder.Flags.SmartDesc;
					}
				}
				query2 = this.ProcessNode(root.Input, flags2, out props);
			}
			else
			{
				query2 = new ContextQuery();
				props = QueryBuilder.Props.None;
			}
			switch (root.TypeOfAxis)
			{
			case Axis.AxisType.Ancestor:
				query = new XPathAncestorQuery(query2, root.Name, root.Prefix, root.NodeType, false);
				props |= QueryBuilder.Props.NonFlat;
				break;
			case Axis.AxisType.AncestorOrSelf:
				query = new XPathAncestorQuery(query2, root.Name, root.Prefix, root.NodeType, true);
				props |= QueryBuilder.Props.NonFlat;
				break;
			case Axis.AxisType.Attribute:
				query = new AttributeQuery(query2, root.Name, root.Prefix, root.NodeType);
				break;
			case Axis.AxisType.Child:
				if ((props & QueryBuilder.Props.NonFlat) != QueryBuilder.Props.None)
				{
					query = new CacheChildrenQuery(query2, root.Name, root.Prefix, root.NodeType);
				}
				else
				{
					query = new ChildrenQuery(query2, root.Name, root.Prefix, root.NodeType);
				}
				break;
			case Axis.AxisType.Descendant:
				if ((flags & QueryBuilder.Flags.SmartDesc) != QueryBuilder.Flags.None)
				{
					query = new DescendantOverDescendantQuery(query2, false, root.Name, root.Prefix, root.NodeType, false);
				}
				else
				{
					query = new DescendantQuery(query2, root.Name, root.Prefix, root.NodeType, false, false);
					if ((props & QueryBuilder.Props.NonFlat) != QueryBuilder.Props.None)
					{
						query = new DocumentOrderQuery(query);
					}
				}
				props |= QueryBuilder.Props.NonFlat;
				break;
			case Axis.AxisType.DescendantOrSelf:
				if ((flags & QueryBuilder.Flags.SmartDesc) != QueryBuilder.Flags.None)
				{
					query = new DescendantOverDescendantQuery(query2, true, root.Name, root.Prefix, root.NodeType, root.AbbrAxis);
				}
				else
				{
					query = new DescendantQuery(query2, root.Name, root.Prefix, root.NodeType, true, root.AbbrAxis);
					if ((props & QueryBuilder.Props.NonFlat) != QueryBuilder.Props.None)
					{
						query = new DocumentOrderQuery(query);
					}
				}
				props |= QueryBuilder.Props.NonFlat;
				break;
			case Axis.AxisType.Following:
				query = new FollowingQuery(query2, root.Name, root.Prefix, root.NodeType);
				props |= QueryBuilder.Props.NonFlat;
				break;
			case Axis.AxisType.FollowingSibling:
				query = new FollSiblingQuery(query2, root.Name, root.Prefix, root.NodeType);
				if ((props & QueryBuilder.Props.NonFlat) != QueryBuilder.Props.None)
				{
					query = new DocumentOrderQuery(query);
				}
				break;
			case Axis.AxisType.Namespace:
				if ((root.NodeType == XPathNodeType.All || root.NodeType == XPathNodeType.Element || root.NodeType == XPathNodeType.Attribute) && root.Prefix.Length == 0)
				{
					query = new NamespaceQuery(query2, root.Name, root.Prefix, root.NodeType);
				}
				else
				{
					query = new EmptyQuery();
				}
				break;
			case Axis.AxisType.Parent:
				query = new ParentQuery(query2, root.Name, root.Prefix, root.NodeType);
				break;
			case Axis.AxisType.Preceding:
				query = new PrecedingQuery(query2, root.Name, root.Prefix, root.NodeType);
				props |= QueryBuilder.Props.NonFlat;
				break;
			case Axis.AxisType.PrecedingSibling:
				query = new PreSiblingQuery(query2, root.Name, root.Prefix, root.NodeType);
				break;
			case Axis.AxisType.Self:
				query = new XPathSelfQuery(query2, root.Name, root.Prefix, root.NodeType);
				break;
			default:
				throw XPathException.Create("The XPath query '{0}' is not supported.", this.query);
			}
			return query;
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00006513 File Offset: 0x00004713
		private bool CanBeNumber(Query q)
		{
			return q.StaticType == XPathResultType.Any || q.StaticType == XPathResultType.Number;
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x0000652C File Offset: 0x0000472C
		private Query ProcessFilter(Filter root, QueryBuilder.Flags flags, out QueryBuilder.Props props)
		{
			bool flag = (flags & QueryBuilder.Flags.Filter) == QueryBuilder.Flags.None;
			QueryBuilder.Props props2;
			Query query = this.ProcessNode(root.Condition, QueryBuilder.Flags.None, out props2);
			if (this.CanBeNumber(query) || (props2 & (QueryBuilder.Props)6) != QueryBuilder.Props.None)
			{
				props2 |= QueryBuilder.Props.HasPosition;
				flags |= QueryBuilder.Flags.PosFilter;
			}
			flags &= (QueryBuilder.Flags)(-2);
			Query query2 = this.ProcessNode(root.Input, flags | QueryBuilder.Flags.Filter, out props);
			if (root.Input.Type != AstNode.AstType.Filter)
			{
				props &= (QueryBuilder.Props)(-2);
			}
			if ((props2 & QueryBuilder.Props.HasPosition) != QueryBuilder.Props.None)
			{
				props |= QueryBuilder.Props.PosFilter;
			}
			FilterQuery filterQuery = query2 as FilterQuery;
			if (filterQuery != null && (props2 & QueryBuilder.Props.HasPosition) == QueryBuilder.Props.None && filterQuery.Condition.StaticType != XPathResultType.Any)
			{
				Query query3 = filterQuery.Condition;
				if (query3.StaticType == XPathResultType.Number)
				{
					query3 = new LogicalExpr(Operator.Op.EQ, new NodeFunctions(Function.FunctionType.FuncPosition, null), query3);
				}
				query = new BooleanExpr(Operator.Op.AND, query3, query);
				query2 = filterQuery.qyInput;
			}
			if ((props & QueryBuilder.Props.PosFilter) != QueryBuilder.Props.None && query2 is DocumentOrderQuery)
			{
				query2 = ((DocumentOrderQuery)query2).input;
			}
			if (this.firstInput == null)
			{
				this.firstInput = (query2 as BaseAxisQuery);
			}
			bool flag2 = (query2.Properties & QueryProps.Merge) > QueryProps.None;
			bool flag3 = (query2.Properties & QueryProps.Reverse) > QueryProps.None;
			if ((props2 & QueryBuilder.Props.HasPosition) != QueryBuilder.Props.None)
			{
				if (flag3)
				{
					query2 = new ReversePositionQuery(query2);
				}
				else if ((props2 & QueryBuilder.Props.HasLast) != QueryBuilder.Props.None)
				{
					query2 = new ForwardPositionQuery(query2);
				}
			}
			if (flag && this.firstInput != null)
			{
				if (flag2 && (props & QueryBuilder.Props.PosFilter) != QueryBuilder.Props.None)
				{
					query2 = new FilterQuery(query2, query, false);
					Query qyInput = this.firstInput.qyInput;
					if (!(qyInput is ContextQuery))
					{
						this.firstInput.qyInput = new ContextQuery();
						this.firstInput = null;
						return new MergeFilterQuery(qyInput, query2);
					}
					this.firstInput = null;
					return query2;
				}
				else
				{
					this.firstInput = null;
				}
			}
			return new FilterQuery(query2, query, (props2 & QueryBuilder.Props.HasPosition) == QueryBuilder.Props.None);
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x000066C8 File Offset: 0x000048C8
		private Query ProcessOperator(Operator root, out QueryBuilder.Props props)
		{
			QueryBuilder.Props props2;
			Query query = this.ProcessNode(root.Operand1, QueryBuilder.Flags.None, out props2);
			QueryBuilder.Props props3;
			Query query2 = this.ProcessNode(root.Operand2, QueryBuilder.Flags.None, out props3);
			props = (props2 | props3);
			switch (root.OperatorType)
			{
			case Operator.Op.OR:
			case Operator.Op.AND:
				return new BooleanExpr(root.OperatorType, query, query2);
			case Operator.Op.EQ:
			case Operator.Op.NE:
			case Operator.Op.LT:
			case Operator.Op.LE:
			case Operator.Op.GT:
			case Operator.Op.GE:
				return new LogicalExpr(root.OperatorType, query, query2);
			case Operator.Op.PLUS:
			case Operator.Op.MINUS:
			case Operator.Op.MUL:
			case Operator.Op.DIV:
			case Operator.Op.MOD:
				return new NumericExpr(root.OperatorType, query, query2);
			case Operator.Op.UNION:
				props |= QueryBuilder.Props.NonFlat;
				return new UnionExpr(query, query2);
			default:
				return null;
			}
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x0000677E File Offset: 0x0000497E
		private Query ProcessVariable(Variable root)
		{
			this.needContext = true;
			if (!this.allowVar)
			{
				throw XPathException.Create("'{0}' is an invalid key pattern. It either contains a variable reference or 'key()' function.", this.query);
			}
			return new VariableQuery(root.Localname, root.Prefix);
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x000067B4 File Offset: 0x000049B4
		private Query ProcessFunction(Function root, out QueryBuilder.Props props)
		{
			props = QueryBuilder.Props.None;
			switch (root.TypeOfFunction)
			{
			case Function.FunctionType.FuncLast:
			{
				Query result = new NodeFunctions(root.TypeOfFunction, null);
				props |= QueryBuilder.Props.HasLast;
				return result;
			}
			case Function.FunctionType.FuncPosition:
			{
				Query result2 = new NodeFunctions(root.TypeOfFunction, null);
				props |= QueryBuilder.Props.HasPosition;
				return result2;
			}
			case Function.FunctionType.FuncCount:
				return new NodeFunctions(Function.FunctionType.FuncCount, this.ProcessNode((AstNode)root.ArgumentList[0], QueryBuilder.Flags.None, out props));
			case Function.FunctionType.FuncID:
			{
				Query result3 = new IDQuery(this.ProcessNode((AstNode)root.ArgumentList[0], QueryBuilder.Flags.None, out props));
				props |= QueryBuilder.Props.NonFlat;
				return result3;
			}
			case Function.FunctionType.FuncLocalName:
			case Function.FunctionType.FuncNameSpaceUri:
			case Function.FunctionType.FuncName:
				if (root.ArgumentList != null && root.ArgumentList.Count > 0)
				{
					return new NodeFunctions(root.TypeOfFunction, this.ProcessNode((AstNode)root.ArgumentList[0], QueryBuilder.Flags.None, out props));
				}
				return new NodeFunctions(root.TypeOfFunction, null);
			case Function.FunctionType.FuncString:
			case Function.FunctionType.FuncConcat:
			case Function.FunctionType.FuncStartsWith:
			case Function.FunctionType.FuncContains:
			case Function.FunctionType.FuncSubstringBefore:
			case Function.FunctionType.FuncSubstringAfter:
			case Function.FunctionType.FuncSubstring:
			case Function.FunctionType.FuncStringLength:
			case Function.FunctionType.FuncNormalize:
			case Function.FunctionType.FuncTranslate:
				return new StringFunctions(root.TypeOfFunction, this.ProcessArguments(root.ArgumentList, out props));
			case Function.FunctionType.FuncBoolean:
			case Function.FunctionType.FuncNot:
			case Function.FunctionType.FuncLang:
				return new BooleanFunctions(root.TypeOfFunction, this.ProcessNode((AstNode)root.ArgumentList[0], QueryBuilder.Flags.None, out props));
			case Function.FunctionType.FuncNumber:
			case Function.FunctionType.FuncSum:
			case Function.FunctionType.FuncFloor:
			case Function.FunctionType.FuncCeiling:
			case Function.FunctionType.FuncRound:
				if (root.ArgumentList != null && root.ArgumentList.Count > 0)
				{
					return new NumberFunctions(root.TypeOfFunction, this.ProcessNode((AstNode)root.ArgumentList[0], QueryBuilder.Flags.None, out props));
				}
				return new NumberFunctions(Function.FunctionType.FuncNumber, null);
			case Function.FunctionType.FuncTrue:
			case Function.FunctionType.FuncFalse:
				return new BooleanFunctions(root.TypeOfFunction, null);
			case Function.FunctionType.FuncUserDefined:
			{
				this.needContext = true;
				if (!this.allowCurrent && root.Name == "current" && root.Prefix.Length == 0)
				{
					throw XPathException.Create("The 'current()' function cannot be used in a pattern.");
				}
				if (!this.allowKey && root.Name == "key" && root.Prefix.Length == 0)
				{
					throw XPathException.Create("'{0}' is an invalid key pattern. It either contains a variable reference or 'key()' function.", this.query);
				}
				Query result4 = new FunctionQuery(root.Prefix, root.Name, this.ProcessArguments(root.ArgumentList, out props));
				props |= QueryBuilder.Props.NonFlat;
				return result4;
			}
			default:
				throw XPathException.Create("The XPath query '{0}' is not supported.", this.query);
			}
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00006A30 File Offset: 0x00004C30
		private List<Query> ProcessArguments(ArrayList args, out QueryBuilder.Props props)
		{
			int num = (args != null) ? args.Count : 0;
			List<Query> list = new List<Query>(num);
			props = QueryBuilder.Props.None;
			for (int i = 0; i < num; i++)
			{
				QueryBuilder.Props props2;
				list.Add(this.ProcessNode((AstNode)args[i], QueryBuilder.Flags.None, out props2));
				props |= props2;
			}
			return list;
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x00006A84 File Offset: 0x00004C84
		private Query ProcessNode(AstNode root, QueryBuilder.Flags flags, out QueryBuilder.Props props)
		{
			int num = this.parseDepth + 1;
			this.parseDepth = num;
			if (num > 1024)
			{
				throw XPathException.Create("The xpath query is too complex.");
			}
			Query result = null;
			props = QueryBuilder.Props.None;
			switch (root.Type)
			{
			case AstNode.AstType.Axis:
				result = this.ProcessAxis((Axis)root, flags, out props);
				break;
			case AstNode.AstType.Operator:
				result = this.ProcessOperator((Operator)root, out props);
				break;
			case AstNode.AstType.Filter:
				result = this.ProcessFilter((Filter)root, flags, out props);
				break;
			case AstNode.AstType.ConstantOperand:
				result = new OperandQuery(((Operand)root).OperandValue);
				break;
			case AstNode.AstType.Function:
				result = this.ProcessFunction((Function)root, out props);
				break;
			case AstNode.AstType.Group:
				result = new GroupQuery(this.ProcessNode(((Group)root).GroupNode, QueryBuilder.Flags.None, out props));
				break;
			case AstNode.AstType.Root:
				result = new AbsoluteQuery();
				break;
			case AstNode.AstType.Variable:
				result = this.ProcessVariable((Variable)root);
				break;
			}
			this.parseDepth--;
			return result;
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x00006B80 File Offset: 0x00004D80
		private Query Build(AstNode root, string query)
		{
			this.Reset();
			this.query = query;
			QueryBuilder.Props props;
			return this.ProcessNode(root, QueryBuilder.Flags.None, out props);
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x00006BA4 File Offset: 0x00004DA4
		internal Query Build(string query, bool allowVar, bool allowKey)
		{
			this.allowVar = allowVar;
			this.allowKey = allowKey;
			this.allowCurrent = true;
			return this.Build(XPathParser.ParseXPathExpresion(query), query);
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00006BC8 File Offset: 0x00004DC8
		internal Query Build(string query, out bool needContext)
		{
			Query result = this.Build(query, true, true);
			needContext = this.needContext;
			return result;
		}

		// Token: 0x060001AB RID: 427 RVA: 0x00006BDB File Offset: 0x00004DDB
		internal Query BuildPatternQuery(string query, bool allowVar, bool allowKey)
		{
			this.allowVar = allowVar;
			this.allowKey = allowKey;
			this.allowCurrent = false;
			return this.Build(XPathParser.ParseXPathPattern(query), query);
		}

		// Token: 0x060001AC RID: 428 RVA: 0x00006BFF File Offset: 0x00004DFF
		internal Query BuildPatternQuery(string query, out bool needContext)
		{
			Query result = this.BuildPatternQuery(query, true, true);
			needContext = this.needContext;
			return result;
		}

		// Token: 0x060001AD RID: 429 RVA: 0x00002103 File Offset: 0x00000303
		public QueryBuilder()
		{
		}

		// Token: 0x040000E3 RID: 227
		private string query;

		// Token: 0x040000E4 RID: 228
		private bool allowVar;

		// Token: 0x040000E5 RID: 229
		private bool allowKey;

		// Token: 0x040000E6 RID: 230
		private bool allowCurrent;

		// Token: 0x040000E7 RID: 231
		private bool needContext;

		// Token: 0x040000E8 RID: 232
		private BaseAxisQuery firstInput;

		// Token: 0x040000E9 RID: 233
		private int parseDepth;

		// Token: 0x040000EA RID: 234
		private const int MaxParseDepth = 1024;

		// Token: 0x0200003F RID: 63
		private enum Flags
		{
			// Token: 0x040000EC RID: 236
			None,
			// Token: 0x040000ED RID: 237
			SmartDesc,
			// Token: 0x040000EE RID: 238
			PosFilter,
			// Token: 0x040000EF RID: 239
			Filter = 4
		}

		// Token: 0x02000040 RID: 64
		private enum Props
		{
			// Token: 0x040000F1 RID: 241
			None,
			// Token: 0x040000F2 RID: 242
			PosFilter,
			// Token: 0x040000F3 RID: 243
			HasPosition,
			// Token: 0x040000F4 RID: 244
			HasLast = 4,
			// Token: 0x040000F5 RID: 245
			NonFlat = 8
		}
	}
}
