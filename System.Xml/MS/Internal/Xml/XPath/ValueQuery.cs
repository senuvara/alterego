﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000049 RID: 73
	internal abstract class ValueQuery : Query
	{
		// Token: 0x060001FB RID: 507 RVA: 0x000037FE File Offset: 0x000019FE
		public ValueQuery()
		{
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00007B34 File Offset: 0x00005D34
		protected ValueQuery(ValueQuery other) : base(other)
		{
		}

		// Token: 0x060001FD RID: 509 RVA: 0x000030EC File Offset: 0x000012EC
		public sealed override void Reset()
		{
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060001FE RID: 510 RVA: 0x00007B3D File Offset: 0x00005D3D
		public sealed override XPathNavigator Current
		{
			get
			{
				throw XPathException.Create("Expression must evaluate to a node-set.");
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060001FF RID: 511 RVA: 0x00007B3D File Offset: 0x00005D3D
		public sealed override int CurrentPosition
		{
			get
			{
				throw XPathException.Create("Expression must evaluate to a node-set.");
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000200 RID: 512 RVA: 0x00007B3D File Offset: 0x00005D3D
		public sealed override int Count
		{
			get
			{
				throw XPathException.Create("Expression must evaluate to a node-set.");
			}
		}

		// Token: 0x06000201 RID: 513 RVA: 0x00007B3D File Offset: 0x00005D3D
		public sealed override XPathNavigator Advance()
		{
			throw XPathException.Create("Expression must evaluate to a node-set.");
		}
	}
}
