﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200004B RID: 75
	internal sealed class VariableQuery : ExtensionQuery
	{
		// Token: 0x06000207 RID: 519 RVA: 0x00007B6F File Offset: 0x00005D6F
		public VariableQuery(string name, string prefix) : base(prefix, name)
		{
		}

		// Token: 0x06000208 RID: 520 RVA: 0x00007B79 File Offset: 0x00005D79
		private VariableQuery(VariableQuery other) : base(other)
		{
			this.variable = other.variable;
		}

		// Token: 0x06000209 RID: 521 RVA: 0x00007B90 File Offset: 0x00005D90
		public override void SetXsltContext(XsltContext context)
		{
			if (context == null)
			{
				throw XPathException.Create("Namespace Manager or XsltContext needed. This query has a prefix, variable, or user-defined function.");
			}
			if (this.xsltContext != context)
			{
				this.xsltContext = context;
				this.variable = this.xsltContext.ResolveVariable(this.prefix, this.name);
				if (this.variable == null)
				{
					throw XPathException.Create("The variable '{0}' is undefined.", base.QName);
				}
			}
		}

		// Token: 0x0600020A RID: 522 RVA: 0x00007BF1 File Offset: 0x00005DF1
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			if (this.xsltContext == null)
			{
				throw XPathException.Create("Namespace Manager or XsltContext needed. This query has a prefix, variable, or user-defined function.");
			}
			return base.ProcessResult(this.variable.Evaluate(this.xsltContext));
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600020B RID: 523 RVA: 0x00007C20 File Offset: 0x00005E20
		public override XPathResultType StaticType
		{
			get
			{
				if (this.variable != null)
				{
					return base.GetXPathType(this.Evaluate(null));
				}
				XPathResultType xpathResultType = (this.variable != null) ? this.variable.VariableType : XPathResultType.Any;
				if (xpathResultType == XPathResultType.Error)
				{
					xpathResultType = XPathResultType.Any;
				}
				return xpathResultType;
			}
		}

		// Token: 0x0600020C RID: 524 RVA: 0x00007C61 File Offset: 0x00005E61
		public override XPathNodeIterator Clone()
		{
			return new VariableQuery(this);
		}

		// Token: 0x0600020D RID: 525 RVA: 0x00007C6C File Offset: 0x00005E6C
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			w.WriteAttributeString("name", (this.prefix.Length != 0) ? (this.prefix + ":" + this.name) : this.name);
			w.WriteEndElement();
		}

		// Token: 0x0400010C RID: 268
		private IXsltContextVariable variable;
	}
}
