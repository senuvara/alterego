﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200000F RID: 15
	internal abstract class BaseAxisQuery : Query
	{
		// Token: 0x0600002D RID: 45 RVA: 0x00002455 File Offset: 0x00000655
		protected BaseAxisQuery(Query qyInput)
		{
			this.name = string.Empty;
			this.prefix = string.Empty;
			this.nsUri = string.Empty;
			this.qyInput = qyInput;
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002488 File Offset: 0x00000688
		protected BaseAxisQuery(Query qyInput, string name, string prefix, XPathNodeType typeTest)
		{
			this.qyInput = qyInput;
			this.name = name;
			this.prefix = prefix;
			this.typeTest = typeTest;
			this.nameTest = (prefix.Length != 0 || name.Length != 0);
			this.nsUri = string.Empty;
		}

		// Token: 0x0600002F RID: 47 RVA: 0x000024E0 File Offset: 0x000006E0
		protected BaseAxisQuery(BaseAxisQuery other) : base(other)
		{
			this.qyInput = Query.Clone(other.qyInput);
			this.name = other.name;
			this.prefix = other.prefix;
			this.nsUri = other.nsUri;
			this.typeTest = other.typeTest;
			this.nameTest = other.nameTest;
			this.position = other.position;
			this.currentNode = other.currentNode;
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002559 File Offset: 0x00000759
		public override void Reset()
		{
			this.position = 0;
			this.currentNode = null;
			this.qyInput.Reset();
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002574 File Offset: 0x00000774
		public override void SetXsltContext(XsltContext context)
		{
			this.nsUri = context.LookupNamespace(this.prefix);
			this.qyInput.SetXsltContext(context);
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000032 RID: 50 RVA: 0x00002594 File Offset: 0x00000794
		protected string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000033 RID: 51 RVA: 0x0000259C File Offset: 0x0000079C
		protected string Prefix
		{
			get
			{
				return this.prefix;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000034 RID: 52 RVA: 0x000025A4 File Offset: 0x000007A4
		protected string Namespace
		{
			get
			{
				return this.nsUri;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000035 RID: 53 RVA: 0x000025AC File Offset: 0x000007AC
		protected bool NameTest
		{
			get
			{
				return this.nameTest;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000036 RID: 54 RVA: 0x000025B4 File Offset: 0x000007B4
		protected XPathNodeType TypeTest
		{
			get
			{
				return this.typeTest;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000037 RID: 55 RVA: 0x000025BC File Offset: 0x000007BC
		public override int CurrentPosition
		{
			get
			{
				return this.position;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000038 RID: 56 RVA: 0x000025C4 File Offset: 0x000007C4
		public override XPathNavigator Current
		{
			get
			{
				return this.currentNode;
			}
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000025CC File Offset: 0x000007CC
		public virtual bool matches(XPathNavigator e)
		{
			if (this.TypeTest == e.NodeType || this.TypeTest == XPathNodeType.All || (this.TypeTest == XPathNodeType.Text && (e.NodeType == XPathNodeType.Whitespace || e.NodeType == XPathNodeType.SignificantWhitespace)))
			{
				if (!this.NameTest)
				{
					return true;
				}
				if ((this.name.Equals(e.LocalName) || this.name.Length == 0) && this.nsUri.Equals(e.NamespaceURI))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600003A RID: 58 RVA: 0x0000264C File Offset: 0x0000084C
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			base.ResetCount();
			this.Reset();
			this.qyInput.Evaluate(nodeIterator);
			return this;
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600003B RID: 59 RVA: 0x00002668 File Offset: 0x00000868
		public override double XsltDefaultPriority
		{
			get
			{
				if (this.qyInput.GetType() != typeof(ContextQuery))
				{
					return 0.5;
				}
				if (this.name.Length != 0)
				{
					return 0.0;
				}
				if (this.prefix.Length != 0)
				{
					return -0.25;
				}
				return -0.5;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600003C RID: 60 RVA: 0x00002408 File Offset: 0x00000608
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.NodeSet;
			}
		}

		// Token: 0x0600003D RID: 61 RVA: 0x000026D4 File Offset: 0x000008D4
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			if (this.NameTest)
			{
				w.WriteAttributeString("name", (this.Prefix.Length != 0) ? (this.Prefix + ":" + this.Name) : this.Name);
			}
			if (this.TypeTest != XPathNodeType.Element)
			{
				w.WriteAttributeString("nodeType", this.TypeTest.ToString());
			}
			this.qyInput.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x0400005A RID: 90
		internal Query qyInput;

		// Token: 0x0400005B RID: 91
		private bool nameTest;

		// Token: 0x0400005C RID: 92
		private string name;

		// Token: 0x0400005D RID: 93
		private string prefix;

		// Token: 0x0400005E RID: 94
		private string nsUri;

		// Token: 0x0400005F RID: 95
		private XPathNodeType typeTest;

		// Token: 0x04000060 RID: 96
		protected XPathNavigator currentNode;

		// Token: 0x04000061 RID: 97
		protected int position;
	}
}
