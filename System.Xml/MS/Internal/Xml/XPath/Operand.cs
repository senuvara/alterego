﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000035 RID: 53
	internal class Operand : AstNode
	{
		// Token: 0x06000167 RID: 359 RVA: 0x00005A30 File Offset: 0x00003C30
		public Operand(string val)
		{
			this.type = XPathResultType.String;
			this.val = val;
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00005A46 File Offset: 0x00003C46
		public Operand(double val)
		{
			this.type = XPathResultType.Number;
			this.val = val;
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00005A61 File Offset: 0x00003C61
		public Operand(bool val)
		{
			this.type = XPathResultType.Boolean;
			this.val = val;
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600016A RID: 362 RVA: 0x00002408 File Offset: 0x00000608
		public override AstNode.AstType Type
		{
			get
			{
				return AstNode.AstType.ConstantOperand;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600016B RID: 363 RVA: 0x00005A7C File Offset: 0x00003C7C
		public override XPathResultType ReturnType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600016C RID: 364 RVA: 0x00005A84 File Offset: 0x00003C84
		public object OperandValue
		{
			get
			{
				return this.val;
			}
		}

		// Token: 0x040000C2 RID: 194
		private XPathResultType type;

		// Token: 0x040000C3 RID: 195
		private object val;
	}
}
