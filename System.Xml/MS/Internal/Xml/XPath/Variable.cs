﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200004A RID: 74
	internal class Variable : AstNode
	{
		// Token: 0x06000202 RID: 514 RVA: 0x00007B49 File Offset: 0x00005D49
		public Variable(string name, string prefix)
		{
			this.localname = name;
			this.prefix = prefix;
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000203 RID: 515 RVA: 0x00006EA3 File Offset: 0x000050A3
		public override AstNode.AstType Type
		{
			get
			{
				return AstNode.AstType.Variable;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000204 RID: 516 RVA: 0x00003A7F File Offset: 0x00001C7F
		public override XPathResultType ReturnType
		{
			get
			{
				return XPathResultType.Any;
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000205 RID: 517 RVA: 0x00007B5F File Offset: 0x00005D5F
		public string Localname
		{
			get
			{
				return this.localname;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000206 RID: 518 RVA: 0x00007B67 File Offset: 0x00005D67
		public string Prefix
		{
			get
			{
				return this.prefix;
			}
		}

		// Token: 0x0400010A RID: 266
		private string localname;

		// Token: 0x0400010B RID: 267
		private string prefix;
	}
}
