﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200005A RID: 90
	internal class XPathSingletonIterator : ResetableIterator
	{
		// Token: 0x06000290 RID: 656 RVA: 0x0000A012 File Offset: 0x00008212
		public XPathSingletonIterator(XPathNavigator nav)
		{
			this.nav = nav;
		}

		// Token: 0x06000291 RID: 657 RVA: 0x0000A021 File Offset: 0x00008221
		public XPathSingletonIterator(XPathNavigator nav, bool moved) : this(nav)
		{
			if (moved)
			{
				this.position = 1;
			}
		}

		// Token: 0x06000292 RID: 658 RVA: 0x0000A034 File Offset: 0x00008234
		public XPathSingletonIterator(XPathSingletonIterator it)
		{
			this.nav = it.nav.Clone();
			this.position = it.position;
		}

		// Token: 0x06000293 RID: 659 RVA: 0x0000A059 File Offset: 0x00008259
		public override XPathNodeIterator Clone()
		{
			return new XPathSingletonIterator(this);
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x06000294 RID: 660 RVA: 0x0000A061 File Offset: 0x00008261
		public override XPathNavigator Current
		{
			get
			{
				return this.nav;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000295 RID: 661 RVA: 0x0000A069 File Offset: 0x00008269
		public override int CurrentPosition
		{
			get
			{
				return this.position;
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000296 RID: 662 RVA: 0x000033DE File Offset: 0x000015DE
		public override int Count
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06000297 RID: 663 RVA: 0x0000A071 File Offset: 0x00008271
		public override bool MoveNext()
		{
			if (this.position == 0)
			{
				this.position = 1;
				return true;
			}
			return false;
		}

		// Token: 0x06000298 RID: 664 RVA: 0x0000A085 File Offset: 0x00008285
		public override void Reset()
		{
			this.position = 0;
		}

		// Token: 0x0400015B RID: 347
		private XPathNavigator nav;

		// Token: 0x0400015C RID: 348
		private int position;
	}
}
