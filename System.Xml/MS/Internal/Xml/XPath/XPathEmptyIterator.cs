﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000052 RID: 82
	internal sealed class XPathEmptyIterator : ResetableIterator
	{
		// Token: 0x06000237 RID: 567 RVA: 0x00005E5D File Offset: 0x0000405D
		private XPathEmptyIterator()
		{
		}

		// Token: 0x06000238 RID: 568 RVA: 0x00002068 File Offset: 0x00000268
		public override XPathNodeIterator Clone()
		{
			return this;
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000239 RID: 569 RVA: 0x000037FB File Offset: 0x000019FB
		public override XPathNavigator Current
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600023A RID: 570 RVA: 0x000020CD File Offset: 0x000002CD
		public override int CurrentPosition
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x0600023B RID: 571 RVA: 0x000020CD File Offset: 0x000002CD
		public override int Count
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x0600023C RID: 572 RVA: 0x000020CD File Offset: 0x000002CD
		public override bool MoveNext()
		{
			return false;
		}

		// Token: 0x0600023D RID: 573 RVA: 0x000030EC File Offset: 0x000012EC
		public override void Reset()
		{
		}

		// Token: 0x0600023E RID: 574 RVA: 0x000082B0 File Offset: 0x000064B0
		// Note: this type is marked as 'beforefieldinit'.
		static XPathEmptyIterator()
		{
		}

		// Token: 0x04000118 RID: 280
		public static XPathEmptyIterator Instance = new XPathEmptyIterator();
	}
}
