﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200000C RID: 12
	internal sealed class AttributeQuery : BaseAxisQuery
	{
		// Token: 0x0600001A RID: 26 RVA: 0x000022A2 File Offset: 0x000004A2
		public AttributeQuery(Query qyParent, string Name, string Prefix, XPathNodeType Type) : base(qyParent, Name, Prefix, Type)
		{
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000022AF File Offset: 0x000004AF
		private AttributeQuery(AttributeQuery other) : base(other)
		{
			this.onAttribute = other.onAttribute;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000022C4 File Offset: 0x000004C4
		public override void Reset()
		{
			this.onAttribute = false;
			base.Reset();
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000022D4 File Offset: 0x000004D4
		public override XPathNavigator Advance()
		{
			for (;;)
			{
				if (!this.onAttribute)
				{
					this.currentNode = this.qyInput.Advance();
					if (this.currentNode == null)
					{
						break;
					}
					this.position = 0;
					this.currentNode = this.currentNode.Clone();
					this.onAttribute = this.currentNode.MoveToFirstAttribute();
				}
				else
				{
					this.onAttribute = this.currentNode.MoveToNextAttribute();
				}
				if (this.onAttribute && this.matches(this.currentNode))
				{
					goto Block_3;
				}
			}
			return null;
			Block_3:
			this.position++;
			return this.currentNode;
		}

		// Token: 0x0600001E RID: 30 RVA: 0x0000236C File Offset: 0x0000056C
		public override XPathNavigator MatchNode(XPathNavigator context)
		{
			if (context != null && context.NodeType == XPathNodeType.Attribute && this.matches(context))
			{
				XPathNavigator xpathNavigator = context.Clone();
				if (xpathNavigator.MoveToParent())
				{
					return this.qyInput.MatchNode(xpathNavigator);
				}
			}
			return null;
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000023AB File Offset: 0x000005AB
		public override XPathNodeIterator Clone()
		{
			return new AttributeQuery(this);
		}

		// Token: 0x04000043 RID: 67
		private bool onAttribute;
	}
}
