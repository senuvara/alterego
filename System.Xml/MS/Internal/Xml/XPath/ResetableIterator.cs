﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000041 RID: 65
	internal abstract class ResetableIterator : XPathNodeIterator
	{
		// Token: 0x060001AE RID: 430 RVA: 0x00006C12 File Offset: 0x00004E12
		public ResetableIterator()
		{
			this.count = -1;
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00006C21 File Offset: 0x00004E21
		protected ResetableIterator(ResetableIterator other)
		{
			this.count = other.count;
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x00006C35 File Offset: 0x00004E35
		protected void ResetCount()
		{
			this.count = -1;
		}

		// Token: 0x060001B1 RID: 433
		public abstract void Reset();

		// Token: 0x060001B2 RID: 434 RVA: 0x00006C40 File Offset: 0x00004E40
		public virtual bool MoveToPosition(int pos)
		{
			this.Reset();
			for (int i = this.CurrentPosition; i < pos; i++)
			{
				if (!this.MoveNext())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060001B3 RID: 435
		public abstract override int CurrentPosition { get; }
	}
}
