﻿using System;
using System.Xml;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200004D RID: 77
	internal sealed class XPathAncestorQuery : CacheAxisQuery
	{
		// Token: 0x06000213 RID: 531 RVA: 0x00007D4B File Offset: 0x00005F4B
		public XPathAncestorQuery(Query qyInput, string name, string prefix, XPathNodeType typeTest, bool matchSelf) : base(qyInput, name, prefix, typeTest)
		{
			this.matchSelf = matchSelf;
		}

		// Token: 0x06000214 RID: 532 RVA: 0x00007D60 File Offset: 0x00005F60
		private XPathAncestorQuery(XPathAncestorQuery other) : base(other)
		{
			this.matchSelf = other.matchSelf;
		}

		// Token: 0x06000215 RID: 533 RVA: 0x00007D78 File Offset: 0x00005F78
		public override object Evaluate(XPathNodeIterator context)
		{
			base.Evaluate(context);
			XPathNavigator xpathNavigator = null;
			XPathNavigator xpathNavigator2;
			while ((xpathNavigator2 = this.qyInput.Advance()) != null)
			{
				if (!this.matchSelf || !this.matches(xpathNavigator2) || base.Insert(this.outputBuffer, xpathNavigator2))
				{
					if (xpathNavigator == null || !xpathNavigator.MoveTo(xpathNavigator2))
					{
						xpathNavigator = xpathNavigator2.Clone();
					}
					while (xpathNavigator.MoveToParent() && (!this.matches(xpathNavigator) || base.Insert(this.outputBuffer, xpathNavigator)))
					{
					}
				}
			}
			return this;
		}

		// Token: 0x06000216 RID: 534 RVA: 0x00007DF6 File Offset: 0x00005FF6
		public override XPathNodeIterator Clone()
		{
			return new XPathAncestorQuery(this);
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000217 RID: 535 RVA: 0x00007DFE File Offset: 0x00005FFE
		public override int CurrentPosition
		{
			get
			{
				return this.outputBuffer.Count - this.count + 1;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000218 RID: 536 RVA: 0x00005E53 File Offset: 0x00004053
		public override QueryProps Properties
		{
			get
			{
				return base.Properties | QueryProps.Reverse;
			}
		}

		// Token: 0x06000219 RID: 537 RVA: 0x00007E14 File Offset: 0x00006014
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			if (this.matchSelf)
			{
				w.WriteAttributeString("self", "yes");
			}
			if (base.NameTest)
			{
				w.WriteAttributeString("name", (base.Prefix.Length != 0) ? (base.Prefix + ":" + base.Name) : base.Name);
			}
			if (base.TypeTest != XPathNodeType.Element)
			{
				w.WriteAttributeString("nodeType", base.TypeTest.ToString());
			}
			this.qyInput.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x0400010D RID: 269
		private bool matchSelf;
	}
}
