﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000028 RID: 40
	internal sealed class FunctionQuery : ExtensionQuery
	{
		// Token: 0x060000EF RID: 239 RVA: 0x000042ED File Offset: 0x000024ED
		public FunctionQuery(string prefix, string name, List<Query> args) : base(prefix, name)
		{
			this.args = args;
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00004300 File Offset: 0x00002500
		private FunctionQuery(FunctionQuery other) : base(other)
		{
			this.function = other.function;
			Query[] array = new Query[other.args.Count];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = Query.Clone(other.args[i]);
			}
			this.args = array;
			this.args = array;
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x00004364 File Offset: 0x00002564
		public override void SetXsltContext(XsltContext context)
		{
			if (context == null)
			{
				throw XPathException.Create("Namespace Manager or XsltContext needed. This query has a prefix, variable, or user-defined function.");
			}
			if (this.xsltContext != context)
			{
				this.xsltContext = context;
				foreach (Query query in this.args)
				{
					query.SetXsltContext(context);
				}
				XPathResultType[] array = new XPathResultType[this.args.Count];
				for (int i = 0; i < this.args.Count; i++)
				{
					array[i] = this.args[i].StaticType;
				}
				this.function = this.xsltContext.ResolveFunction(this.prefix, this.name, array);
				if (this.function == null)
				{
					throw XPathException.Create("The function '{0}()' is undefined.", base.QName);
				}
			}
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x00004444 File Offset: 0x00002644
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			if (this.xsltContext == null)
			{
				throw XPathException.Create("Namespace Manager or XsltContext needed. This query has a prefix, variable, or user-defined function.");
			}
			object[] array = new object[this.args.Count];
			for (int i = 0; i < this.args.Count; i++)
			{
				array[i] = this.args[i].Evaluate(nodeIterator);
				if (array[i] is XPathNodeIterator)
				{
					array[i] = new XPathSelectionIterator(nodeIterator.Current, this.args[i]);
				}
			}
			object result;
			try
			{
				result = base.ProcessResult(this.function.Invoke(this.xsltContext, array, nodeIterator.Current));
			}
			catch (Exception innerException)
			{
				throw XPathException.Create("Function '{0}()' has failed.", base.QName, innerException);
			}
			return result;
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x0000450C File Offset: 0x0000270C
		public override XPathNavigator MatchNode(XPathNavigator navigator)
		{
			if (this.name != "key" && this.prefix.Length != 0)
			{
				throw XPathException.Create("'{0}' is an invalid XSLT pattern.");
			}
			this.Evaluate(new XPathSingletonIterator(navigator, true));
			XPathNavigator xpathNavigator;
			while ((xpathNavigator = this.Advance()) != null)
			{
				if (xpathNavigator.IsSamePosition(navigator))
				{
					return xpathNavigator;
				}
			}
			return xpathNavigator;
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000F4 RID: 244 RVA: 0x0000456C File Offset: 0x0000276C
		public override XPathResultType StaticType
		{
			get
			{
				XPathResultType xpathResultType = (this.function != null) ? this.function.ReturnType : XPathResultType.Any;
				if (xpathResultType == XPathResultType.Error)
				{
					xpathResultType = XPathResultType.Any;
				}
				return xpathResultType;
			}
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x00004597 File Offset: 0x00002797
		public override XPathNodeIterator Clone()
		{
			return new FunctionQuery(this);
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x000045A0 File Offset: 0x000027A0
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			w.WriteAttributeString("name", (this.prefix.Length != 0) ? (this.prefix + ":" + this.name) : this.name);
			foreach (Query query in this.args)
			{
				query.PrintQuery(w);
			}
			w.WriteEndElement();
		}

		// Token: 0x040000AB RID: 171
		private IList<Query> args;

		// Token: 0x040000AC RID: 172
		private IXsltContextFunction function;
	}
}
