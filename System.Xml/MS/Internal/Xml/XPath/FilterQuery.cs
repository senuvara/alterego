﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000022 RID: 34
	internal sealed class FilterQuery : BaseAxisQuery
	{
		// Token: 0x060000C8 RID: 200 RVA: 0x00003AA8 File Offset: 0x00001CA8
		public FilterQuery(Query qyParent, Query cond, bool noPosition) : base(qyParent)
		{
			this.cond = cond;
			this.noPosition = noPosition;
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00003ABF File Offset: 0x00001CBF
		private FilterQuery(FilterQuery other) : base(other)
		{
			this.cond = Query.Clone(other.cond);
			this.noPosition = other.noPosition;
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00003AE5 File Offset: 0x00001CE5
		public override void Reset()
		{
			this.cond.Reset();
			base.Reset();
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000CB RID: 203 RVA: 0x00003AF8 File Offset: 0x00001CF8
		public Query Condition
		{
			get
			{
				return this.cond;
			}
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00003B00 File Offset: 0x00001D00
		public override void SetXsltContext(XsltContext input)
		{
			base.SetXsltContext(input);
			this.cond.SetXsltContext(input);
			if (this.cond.StaticType != XPathResultType.Number && this.cond.StaticType != XPathResultType.Any && this.noPosition)
			{
				ReversePositionQuery reversePositionQuery = this.qyInput as ReversePositionQuery;
				if (reversePositionQuery != null)
				{
					this.qyInput = reversePositionQuery.input;
				}
			}
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00003B60 File Offset: 0x00001D60
		public override XPathNavigator Advance()
		{
			while ((this.currentNode = this.qyInput.Advance()) != null)
			{
				if (this.EvaluatePredicate())
				{
					this.position++;
					return this.currentNode;
				}
			}
			return null;
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00003BA4 File Offset: 0x00001DA4
		internal bool EvaluatePredicate()
		{
			object obj = this.cond.Evaluate(this.qyInput);
			if (obj is XPathNodeIterator)
			{
				return this.cond.Advance() != null;
			}
			if (obj is string)
			{
				return ((string)obj).Length != 0;
			}
			if (obj is double)
			{
				return (double)obj == (double)this.qyInput.CurrentPosition;
			}
			return !(obj is bool) || (bool)obj;
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00003C20 File Offset: 0x00001E20
		public override XPathNavigator MatchNode(XPathNavigator current)
		{
			if (current == null)
			{
				return null;
			}
			XPathNavigator xpathNavigator = this.qyInput.MatchNode(current);
			if (xpathNavigator != null)
			{
				switch (this.cond.StaticType)
				{
				case XPathResultType.Number:
				{
					OperandQuery operandQuery = this.cond as OperandQuery;
					if (operandQuery != null)
					{
						double num = (double)operandQuery.val;
						ChildrenQuery childrenQuery = this.qyInput as ChildrenQuery;
						if (childrenQuery != null)
						{
							XPathNavigator xpathNavigator2 = current.Clone();
							xpathNavigator2.MoveToParent();
							int num2 = 0;
							xpathNavigator2.MoveToFirstChild();
							for (;;)
							{
								if (childrenQuery.matches(xpathNavigator2))
								{
									num2++;
									if (current.IsSamePosition(xpathNavigator2))
									{
										break;
									}
								}
								if (!xpathNavigator2.MoveToNext())
								{
									goto Block_9;
								}
							}
							if (num != (double)num2)
							{
								return null;
							}
							return xpathNavigator;
							Block_9:
							return null;
						}
						AttributeQuery attributeQuery = this.qyInput as AttributeQuery;
						if (attributeQuery != null)
						{
							XPathNavigator xpathNavigator3 = current.Clone();
							xpathNavigator3.MoveToParent();
							int num3 = 0;
							xpathNavigator3.MoveToFirstAttribute();
							for (;;)
							{
								if (attributeQuery.matches(xpathNavigator3))
								{
									num3++;
									if (current.IsSamePosition(xpathNavigator3))
									{
										break;
									}
								}
								if (!xpathNavigator3.MoveToNextAttribute())
								{
									goto Block_14;
								}
							}
							if (num != (double)num3)
							{
								return null;
							}
							return xpathNavigator;
							Block_14:
							return null;
						}
					}
					break;
				}
				case XPathResultType.String:
					if (this.noPosition)
					{
						if (((string)this.cond.Evaluate(new XPathSingletonIterator(current, true))).Length == 0)
						{
							return null;
						}
						return xpathNavigator;
					}
					break;
				case XPathResultType.Boolean:
					if (this.noPosition)
					{
						if (!(bool)this.cond.Evaluate(new XPathSingletonIterator(current, true)))
						{
							return null;
						}
						return xpathNavigator;
					}
					break;
				case XPathResultType.NodeSet:
					this.cond.Evaluate(new XPathSingletonIterator(current, true));
					if (this.cond.Advance() == null)
					{
						return null;
					}
					return xpathNavigator;
				case (XPathResultType)4:
					return xpathNavigator;
				default:
					return null;
				}
				this.Evaluate(new XPathSingletonIterator(xpathNavigator, true));
				XPathNavigator xpathNavigator4;
				while ((xpathNavigator4 = this.Advance()) != null)
				{
					if (xpathNavigator4.IsSamePosition(current))
					{
						return xpathNavigator;
					}
				}
			}
			return null;
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000D0 RID: 208 RVA: 0x00003DE8 File Offset: 0x00001FE8
		public override QueryProps Properties
		{
			get
			{
				return QueryProps.Position | (this.qyInput.Properties & (QueryProps)24);
			}
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x00003DFA File Offset: 0x00001FFA
		public override XPathNodeIterator Clone()
		{
			return new FilterQuery(this);
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00003E04 File Offset: 0x00002004
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			if (!this.noPosition)
			{
				w.WriteAttributeString("position", "yes");
			}
			this.qyInput.PrintQuery(w);
			this.cond.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x04000082 RID: 130
		private Query cond;

		// Token: 0x04000083 RID: 131
		private bool noPosition;
	}
}
