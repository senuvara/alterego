﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000011 RID: 17
	internal sealed class BooleanFunctions : ValueQuery
	{
		// Token: 0x06000045 RID: 69 RVA: 0x000028B1 File Offset: 0x00000AB1
		public BooleanFunctions(Function.FunctionType funcType, Query arg)
		{
			this.arg = arg;
			this.funcType = funcType;
		}

		// Token: 0x06000046 RID: 70 RVA: 0x000028C7 File Offset: 0x00000AC7
		private BooleanFunctions(BooleanFunctions other) : base(other)
		{
			this.arg = Query.Clone(other.arg);
			this.funcType = other.funcType;
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000028ED File Offset: 0x00000AED
		public override void SetXsltContext(XsltContext context)
		{
			if (this.arg != null)
			{
				this.arg.SetXsltContext(context);
			}
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002904 File Offset: 0x00000B04
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			Function.FunctionType functionType = this.funcType;
			switch (functionType)
			{
			case Function.FunctionType.FuncBoolean:
				return this.toBoolean(nodeIterator);
			case Function.FunctionType.FuncNumber:
				break;
			case Function.FunctionType.FuncTrue:
				return true;
			case Function.FunctionType.FuncFalse:
				return false;
			case Function.FunctionType.FuncNot:
				return this.Not(nodeIterator);
			default:
				if (functionType == Function.FunctionType.FuncLang)
				{
					return this.Lang(nodeIterator);
				}
				break;
			}
			return false;
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002976 File Offset: 0x00000B76
		internal static bool toBoolean(double number)
		{
			return number != 0.0 && !double.IsNaN(number);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x0000298F File Offset: 0x00000B8F
		internal static bool toBoolean(string str)
		{
			return str.Length > 0;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x0000299C File Offset: 0x00000B9C
		internal bool toBoolean(XPathNodeIterator nodeIterator)
		{
			object obj = this.arg.Evaluate(nodeIterator);
			if (obj is XPathNodeIterator)
			{
				return this.arg.Advance() != null;
			}
			if (obj is string)
			{
				return BooleanFunctions.toBoolean((string)obj);
			}
			if (obj is double)
			{
				return BooleanFunctions.toBoolean((double)obj);
			}
			return !(obj is bool) || (bool)obj;
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600004C RID: 76 RVA: 0x0000284A File Offset: 0x00000A4A
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.Boolean;
			}
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002A05 File Offset: 0x00000C05
		private bool Not(XPathNodeIterator nodeIterator)
		{
			return !(bool)this.arg.Evaluate(nodeIterator);
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002A1C File Offset: 0x00000C1C
		private bool Lang(XPathNodeIterator nodeIterator)
		{
			string text = this.arg.Evaluate(nodeIterator).ToString();
			string xmlLang = nodeIterator.Current.XmlLang;
			return xmlLang.StartsWith(text, StringComparison.OrdinalIgnoreCase) && (xmlLang.Length == text.Length || xmlLang[text.Length] == '-');
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002A73 File Offset: 0x00000C73
		public override XPathNodeIterator Clone()
		{
			return new BooleanFunctions(this);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00002A7C File Offset: 0x00000C7C
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			w.WriteAttributeString("name", this.funcType.ToString());
			if (this.arg != null)
			{
				this.arg.PrintQuery(w);
			}
			w.WriteEndElement();
		}

		// Token: 0x04000065 RID: 101
		private Query arg;

		// Token: 0x04000066 RID: 102
		private Function.FunctionType funcType;
	}
}
