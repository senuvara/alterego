﻿using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x02000010 RID: 16
	internal sealed class BooleanExpr : ValueQuery
	{
		// Token: 0x0600003E RID: 62 RVA: 0x0000276C File Offset: 0x0000096C
		public BooleanExpr(Operator.Op op, Query opnd1, Query opnd2)
		{
			if (opnd1.StaticType != XPathResultType.Boolean)
			{
				opnd1 = new BooleanFunctions(Function.FunctionType.FuncBoolean, opnd1);
			}
			if (opnd2.StaticType != XPathResultType.Boolean)
			{
				opnd2 = new BooleanFunctions(Function.FunctionType.FuncBoolean, opnd2);
			}
			this.opnd1 = opnd1;
			this.opnd2 = opnd2;
			this.isOr = (op == Operator.Op.OR);
		}

		// Token: 0x0600003F RID: 63 RVA: 0x000027BB File Offset: 0x000009BB
		private BooleanExpr(BooleanExpr other) : base(other)
		{
			this.opnd1 = Query.Clone(other.opnd1);
			this.opnd2 = Query.Clone(other.opnd2);
			this.isOr = other.isOr;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000027F2 File Offset: 0x000009F2
		public override void SetXsltContext(XsltContext context)
		{
			this.opnd1.SetXsltContext(context);
			this.opnd2.SetXsltContext(context);
		}

		// Token: 0x06000041 RID: 65 RVA: 0x0000280C File Offset: 0x00000A0C
		public override object Evaluate(XPathNodeIterator nodeIterator)
		{
			object obj = this.opnd1.Evaluate(nodeIterator);
			if ((bool)obj == this.isOr)
			{
				return obj;
			}
			return this.opnd2.Evaluate(nodeIterator);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00002842 File Offset: 0x00000A42
		public override XPathNodeIterator Clone()
		{
			return new BooleanExpr(this);
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000043 RID: 67 RVA: 0x0000284A File Offset: 0x00000A4A
		public override XPathResultType StaticType
		{
			get
			{
				return XPathResultType.Boolean;
			}
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00002850 File Offset: 0x00000A50
		public override void PrintQuery(XmlWriter w)
		{
			w.WriteStartElement(base.GetType().Name);
			w.WriteAttributeString("op", (this.isOr ? Operator.Op.OR : Operator.Op.AND).ToString());
			this.opnd1.PrintQuery(w);
			this.opnd2.PrintQuery(w);
			w.WriteEndElement();
		}

		// Token: 0x04000062 RID: 98
		private Query opnd1;

		// Token: 0x04000063 RID: 99
		private Query opnd2;

		// Token: 0x04000064 RID: 100
		private bool isOr;
	}
}
