﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.XPath
{
	// Token: 0x0200004F RID: 79
	internal abstract class XPathAxisIterator : XPathNodeIterator
	{
		// Token: 0x06000226 RID: 550 RVA: 0x00007FF2 File Offset: 0x000061F2
		public XPathAxisIterator(XPathNavigator nav, bool matchSelf)
		{
			this.nav = nav;
			this.matchSelf = matchSelf;
		}

		// Token: 0x06000227 RID: 551 RVA: 0x0000800F File Offset: 0x0000620F
		public XPathAxisIterator(XPathNavigator nav, XPathNodeType type, bool matchSelf) : this(nav, matchSelf)
		{
			this.type = type;
		}

		// Token: 0x06000228 RID: 552 RVA: 0x00008020 File Offset: 0x00006220
		public XPathAxisIterator(XPathNavigator nav, string name, string namespaceURI, bool matchSelf) : this(nav, matchSelf)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (namespaceURI == null)
			{
				throw new ArgumentNullException("namespaceURI");
			}
			this.name = name;
			this.uri = namespaceURI;
		}

		// Token: 0x06000229 RID: 553 RVA: 0x00008058 File Offset: 0x00006258
		public XPathAxisIterator(XPathAxisIterator it)
		{
			this.nav = it.nav.Clone();
			this.type = it.type;
			this.name = it.name;
			this.uri = it.uri;
			this.position = it.position;
			this.matchSelf = it.matchSelf;
			this.first = it.first;
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600022A RID: 554 RVA: 0x000080CB File Offset: 0x000062CB
		public override XPathNavigator Current
		{
			get
			{
				return this.nav;
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600022B RID: 555 RVA: 0x000080D3 File Offset: 0x000062D3
		public override int CurrentPosition
		{
			get
			{
				return this.position;
			}
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600022C RID: 556 RVA: 0x000080DC File Offset: 0x000062DC
		protected virtual bool Matches
		{
			get
			{
				if (this.name == null)
				{
					return this.type == this.nav.NodeType || this.type == XPathNodeType.All || (this.type == XPathNodeType.Text && (this.nav.NodeType == XPathNodeType.Whitespace || this.nav.NodeType == XPathNodeType.SignificantWhitespace));
				}
				return this.nav.NodeType == XPathNodeType.Element && (this.name.Length == 0 || this.name == this.nav.LocalName) && this.uri == this.nav.NamespaceURI;
			}
		}

		// Token: 0x04000110 RID: 272
		internal XPathNavigator nav;

		// Token: 0x04000111 RID: 273
		internal XPathNodeType type;

		// Token: 0x04000112 RID: 274
		internal string name;

		// Token: 0x04000113 RID: 275
		internal string uri;

		// Token: 0x04000114 RID: 276
		internal int position;

		// Token: 0x04000115 RID: 277
		internal bool matchSelf;

		// Token: 0x04000116 RID: 278
		internal bool first = true;
	}
}
