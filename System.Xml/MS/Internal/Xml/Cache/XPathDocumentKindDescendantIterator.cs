﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x02000063 RID: 99
	internal class XPathDocumentKindDescendantIterator : XPathDocumentBaseIterator
	{
		// Token: 0x060002DA RID: 730 RVA: 0x0000B11D File Offset: 0x0000931D
		public XPathDocumentKindDescendantIterator(XPathDocumentNavigator root, XPathNodeType typ, bool matchSelf) : base(root)
		{
			this.typ = typ;
			this.matchSelf = matchSelf;
			if (root.NodeType != XPathNodeType.Root)
			{
				this.end = new XPathDocumentNavigator(root);
				this.end.MoveToNonDescendant();
			}
		}

		// Token: 0x060002DB RID: 731 RVA: 0x0000B154 File Offset: 0x00009354
		public XPathDocumentKindDescendantIterator(XPathDocumentKindDescendantIterator iter) : base(iter)
		{
			this.end = iter.end;
			this.typ = iter.typ;
			this.matchSelf = iter.matchSelf;
		}

		// Token: 0x060002DC RID: 732 RVA: 0x0000B181 File Offset: 0x00009381
		public override XPathNodeIterator Clone()
		{
			return new XPathDocumentKindDescendantIterator(this);
		}

		// Token: 0x060002DD RID: 733 RVA: 0x0000B18C File Offset: 0x0000938C
		public override bool MoveNext()
		{
			if (this.matchSelf)
			{
				this.matchSelf = false;
				if (this.ctxt.IsKindMatch(this.typ))
				{
					this.pos++;
					return true;
				}
			}
			if (!this.ctxt.MoveToFollowing(this.typ, this.end))
			{
				return false;
			}
			this.pos++;
			return true;
		}

		// Token: 0x04000188 RID: 392
		private XPathDocumentNavigator end;

		// Token: 0x04000189 RID: 393
		private XPathNodeType typ;

		// Token: 0x0400018A RID: 394
		private bool matchSelf;
	}
}
