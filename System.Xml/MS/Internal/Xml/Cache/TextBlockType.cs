﻿using System;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x0200005B RID: 91
	internal enum TextBlockType
	{
		// Token: 0x0400015E RID: 350
		None,
		// Token: 0x0400015F RID: 351
		Text = 4,
		// Token: 0x04000160 RID: 352
		SignificantWhitespace,
		// Token: 0x04000161 RID: 353
		Whitespace
	}
}
