﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x02000061 RID: 97
	internal class XPathDocumentKindChildIterator : XPathDocumentBaseIterator
	{
		// Token: 0x060002D2 RID: 722 RVA: 0x0000AF86 File Offset: 0x00009186
		public XPathDocumentKindChildIterator(XPathDocumentNavigator parent, XPathNodeType typ) : base(parent)
		{
			this.typ = typ;
		}

		// Token: 0x060002D3 RID: 723 RVA: 0x0000AF96 File Offset: 0x00009196
		public XPathDocumentKindChildIterator(XPathDocumentKindChildIterator iter) : base(iter)
		{
			this.typ = iter.typ;
		}

		// Token: 0x060002D4 RID: 724 RVA: 0x0000AFAB File Offset: 0x000091AB
		public override XPathNodeIterator Clone()
		{
			return new XPathDocumentKindChildIterator(this);
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x0000AFB4 File Offset: 0x000091B4
		public override bool MoveNext()
		{
			if (this.pos == 0)
			{
				if (!this.ctxt.MoveToChild(this.typ))
				{
					return false;
				}
			}
			else if (!this.ctxt.MoveToNext(this.typ))
			{
				return false;
			}
			this.pos++;
			return true;
		}

		// Token: 0x04000183 RID: 387
		private XPathNodeType typ;
	}
}
