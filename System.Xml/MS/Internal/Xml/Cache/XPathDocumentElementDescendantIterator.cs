﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x02000062 RID: 98
	internal class XPathDocumentElementDescendantIterator : XPathDocumentBaseIterator
	{
		// Token: 0x060002D6 RID: 726 RVA: 0x0000B004 File Offset: 0x00009204
		public XPathDocumentElementDescendantIterator(XPathDocumentNavigator root, string name, string namespaceURI, bool matchSelf) : base(root)
		{
			if (namespaceURI == null)
			{
				throw new ArgumentNullException("namespaceURI");
			}
			this.localName = root.NameTable.Get(name);
			this.namespaceUri = namespaceURI;
			this.matchSelf = matchSelf;
			if (root.NodeType != XPathNodeType.Root)
			{
				this.end = new XPathDocumentNavigator(root);
				this.end.MoveToNonDescendant();
			}
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x0000B067 File Offset: 0x00009267
		public XPathDocumentElementDescendantIterator(XPathDocumentElementDescendantIterator iter) : base(iter)
		{
			this.end = iter.end;
			this.localName = iter.localName;
			this.namespaceUri = iter.namespaceUri;
			this.matchSelf = iter.matchSelf;
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x0000B0A0 File Offset: 0x000092A0
		public override XPathNodeIterator Clone()
		{
			return new XPathDocumentElementDescendantIterator(this);
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x0000B0A8 File Offset: 0x000092A8
		public override bool MoveNext()
		{
			if (this.matchSelf)
			{
				this.matchSelf = false;
				if (this.ctxt.IsElementMatch(this.localName, this.namespaceUri))
				{
					this.pos++;
					return true;
				}
			}
			if (!this.ctxt.MoveToFollowing(this.localName, this.namespaceUri, this.end))
			{
				return false;
			}
			this.pos++;
			return true;
		}

		// Token: 0x04000184 RID: 388
		private XPathDocumentNavigator end;

		// Token: 0x04000185 RID: 389
		private string localName;

		// Token: 0x04000186 RID: 390
		private string namespaceUri;

		// Token: 0x04000187 RID: 391
		private bool matchSelf;
	}
}
