﻿using System;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x02000066 RID: 102
	internal struct XPathNodeRef
	{
		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000339 RID: 825 RVA: 0x0000C4D4 File Offset: 0x0000A6D4
		public static XPathNodeRef Null
		{
			get
			{
				return default(XPathNodeRef);
			}
		}

		// Token: 0x0600033A RID: 826 RVA: 0x0000C4EA File Offset: 0x0000A6EA
		public XPathNodeRef(XPathNode[] page, int idx)
		{
			this.page = page;
			this.idx = idx;
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x0600033B RID: 827 RVA: 0x0000C4FA File Offset: 0x0000A6FA
		public bool IsNull
		{
			get
			{
				return this.page == null;
			}
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x0600033C RID: 828 RVA: 0x0000C505 File Offset: 0x0000A705
		public XPathNode[] Page
		{
			get
			{
				return this.page;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x0600033D RID: 829 RVA: 0x0000C50D File Offset: 0x0000A70D
		public int Index
		{
			get
			{
				return this.idx;
			}
		}

		// Token: 0x0600033E RID: 830 RVA: 0x0000C515 File Offset: 0x0000A715
		public override int GetHashCode()
		{
			return XPathNodeHelper.GetLocation(this.page, this.idx);
		}

		// Token: 0x040001A4 RID: 420
		private XPathNode[] page;

		// Token: 0x040001A5 RID: 421
		private int idx;
	}
}
