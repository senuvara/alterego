﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x02000060 RID: 96
	internal class XPathDocumentElementChildIterator : XPathDocumentBaseIterator
	{
		// Token: 0x060002CE RID: 718 RVA: 0x0000AED2 File Offset: 0x000090D2
		public XPathDocumentElementChildIterator(XPathDocumentNavigator parent, string name, string namespaceURI) : base(parent)
		{
			if (namespaceURI == null)
			{
				throw new ArgumentNullException("namespaceURI");
			}
			this.localName = parent.NameTable.Get(name);
			this.namespaceUri = namespaceURI;
		}

		// Token: 0x060002CF RID: 719 RVA: 0x0000AF02 File Offset: 0x00009102
		public XPathDocumentElementChildIterator(XPathDocumentElementChildIterator iter) : base(iter)
		{
			this.localName = iter.localName;
			this.namespaceUri = iter.namespaceUri;
		}

		// Token: 0x060002D0 RID: 720 RVA: 0x0000AF23 File Offset: 0x00009123
		public override XPathNodeIterator Clone()
		{
			return new XPathDocumentElementChildIterator(this);
		}

		// Token: 0x060002D1 RID: 721 RVA: 0x0000AF2C File Offset: 0x0000912C
		public override bool MoveNext()
		{
			if (this.pos == 0)
			{
				if (!this.ctxt.MoveToChild(this.localName, this.namespaceUri))
				{
					return false;
				}
			}
			else if (!this.ctxt.MoveToNext(this.localName, this.namespaceUri))
			{
				return false;
			}
			this.pos++;
			return true;
		}

		// Token: 0x04000181 RID: 385
		private string localName;

		// Token: 0x04000182 RID: 386
		private string namespaceUri;
	}
}
