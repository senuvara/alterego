﻿using System;
using System.Xml.XPath;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x0200005F RID: 95
	internal abstract class XPathDocumentBaseIterator : XPathNodeIterator
	{
		// Token: 0x060002CA RID: 714 RVA: 0x0000AE89 File Offset: 0x00009089
		protected XPathDocumentBaseIterator(XPathDocumentNavigator ctxt)
		{
			this.ctxt = new XPathDocumentNavigator(ctxt);
		}

		// Token: 0x060002CB RID: 715 RVA: 0x0000AE9D File Offset: 0x0000909D
		protected XPathDocumentBaseIterator(XPathDocumentBaseIterator iter)
		{
			this.ctxt = new XPathDocumentNavigator(iter.ctxt);
			this.pos = iter.pos;
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x060002CC RID: 716 RVA: 0x0000AEC2 File Offset: 0x000090C2
		public override XPathNavigator Current
		{
			get
			{
				return this.ctxt;
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x060002CD RID: 717 RVA: 0x0000AECA File Offset: 0x000090CA
		public override int CurrentPosition
		{
			get
			{
				return this.pos;
			}
		}

		// Token: 0x0400017F RID: 383
		protected XPathDocumentNavigator ctxt;

		// Token: 0x04000180 RID: 384
		protected int pos;
	}
}
