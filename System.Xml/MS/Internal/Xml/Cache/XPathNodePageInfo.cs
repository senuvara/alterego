﻿using System;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x02000068 RID: 104
	internal sealed class XPathNodePageInfo
	{
		// Token: 0x06000356 RID: 854 RVA: 0x0000CD3F File Offset: 0x0000AF3F
		public XPathNodePageInfo(XPathNode[] pagePrev, int pageNum)
		{
			this.pagePrev = pagePrev;
			this.pageNum = pageNum;
			this.nodeCount = 1;
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000357 RID: 855 RVA: 0x0000CD5C File Offset: 0x0000AF5C
		public int PageNumber
		{
			get
			{
				return this.pageNum;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000358 RID: 856 RVA: 0x0000CD64 File Offset: 0x0000AF64
		// (set) Token: 0x06000359 RID: 857 RVA: 0x0000CD6C File Offset: 0x0000AF6C
		public int NodeCount
		{
			get
			{
				return this.nodeCount;
			}
			set
			{
				this.nodeCount = value;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x0600035A RID: 858 RVA: 0x0000CD75 File Offset: 0x0000AF75
		public XPathNode[] PreviousPage
		{
			get
			{
				return this.pagePrev;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x0600035B RID: 859 RVA: 0x0000CD7D File Offset: 0x0000AF7D
		// (set) Token: 0x0600035C RID: 860 RVA: 0x0000CD85 File Offset: 0x0000AF85
		public XPathNode[] NextPage
		{
			get
			{
				return this.pageNext;
			}
			set
			{
				this.pageNext = value;
			}
		}

		// Token: 0x040001A6 RID: 422
		private int pageNum;

		// Token: 0x040001A7 RID: 423
		private int nodeCount;

		// Token: 0x040001A8 RID: 424
		private XPathNode[] pagePrev;

		// Token: 0x040001A9 RID: 425
		private XPathNode[] pageNext;
	}
}
