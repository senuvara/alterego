﻿using System;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace MS.Internal.Xml.Cache
{
	// Token: 0x02000064 RID: 100
	internal sealed class XPathDocumentNavigator : XPathNavigator, IXmlLineInfo
	{
		// Token: 0x060002DE RID: 734 RVA: 0x0000B1F5 File Offset: 0x000093F5
		public XPathDocumentNavigator(XPathNode[] pageCurrent, int idxCurrent, XPathNode[] pageParent, int idxParent)
		{
			this.pageCurrent = pageCurrent;
			this.pageParent = pageParent;
			this.idxCurrent = idxCurrent;
			this.idxParent = idxParent;
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000B21A File Offset: 0x0000941A
		public XPathDocumentNavigator(XPathDocumentNavigator nav) : this(nav.pageCurrent, nav.idxCurrent, nav.pageParent, nav.idxParent)
		{
			this.atomizedLocalName = nav.atomizedLocalName;
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x060002E0 RID: 736 RVA: 0x0000B248 File Offset: 0x00009448
		public override string Value
		{
			get
			{
				string value = this.pageCurrent[this.idxCurrent].Value;
				if (value != null)
				{
					return value;
				}
				if (this.idxParent != 0)
				{
					return this.pageParent[this.idxParent].Value;
				}
				string text = string.Empty;
				StringBuilder stringBuilder = null;
				XPathNode[] array;
				XPathNode[] pageEnd = array = this.pageCurrent;
				int num;
				int idxEnd = num = this.idxCurrent;
				if (!XPathNodeHelper.GetNonDescendant(ref pageEnd, ref idxEnd))
				{
					pageEnd = null;
					idxEnd = 0;
				}
				while (XPathNodeHelper.GetTextFollowing(ref array, ref num, pageEnd, idxEnd))
				{
					if (text.Length == 0)
					{
						text = array[num].Value;
					}
					else
					{
						if (stringBuilder == null)
						{
							stringBuilder = new StringBuilder();
							stringBuilder.Append(text);
						}
						stringBuilder.Append(array[num].Value);
					}
				}
				if (stringBuilder == null)
				{
					return text;
				}
				return stringBuilder.ToString();
			}
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0000B31D File Offset: 0x0000951D
		public override XPathNavigator Clone()
		{
			return new XPathDocumentNavigator(this.pageCurrent, this.idxCurrent, this.pageParent, this.idxParent);
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060002E2 RID: 738 RVA: 0x0000B33C File Offset: 0x0000953C
		public override XPathNodeType NodeType
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].NodeType;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060002E3 RID: 739 RVA: 0x0000B354 File Offset: 0x00009554
		public override string LocalName
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].LocalName;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060002E4 RID: 740 RVA: 0x0000B36C File Offset: 0x0000956C
		public override string NamespaceURI
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].NamespaceUri;
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060002E5 RID: 741 RVA: 0x0000B384 File Offset: 0x00009584
		public override string Name
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].Name;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060002E6 RID: 742 RVA: 0x0000B39C File Offset: 0x0000959C
		public override string Prefix
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].Prefix;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x060002E7 RID: 743 RVA: 0x0000B3B4 File Offset: 0x000095B4
		public override string BaseURI
		{
			get
			{
				XPathNode[] array;
				int parent;
				if (this.idxParent != 0)
				{
					array = this.pageParent;
					parent = this.idxParent;
				}
				else
				{
					array = this.pageCurrent;
					parent = this.idxCurrent;
				}
				for (;;)
				{
					XPathNodeType nodeType = array[parent].NodeType;
					if (nodeType <= XPathNodeType.Element || nodeType == XPathNodeType.ProcessingInstruction)
					{
						break;
					}
					parent = array[parent].GetParent(out array);
					if (parent == 0)
					{
						goto Block_3;
					}
				}
				return array[parent].BaseUri;
				Block_3:
				return string.Empty;
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x060002E8 RID: 744 RVA: 0x0000B420 File Offset: 0x00009620
		public override bool IsEmptyElement
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].AllowShortcutTag;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060002E9 RID: 745 RVA: 0x0000B438 File Offset: 0x00009638
		public override XmlNameTable NameTable
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].Document.NameTable;
			}
		}

		// Token: 0x060002EA RID: 746 RVA: 0x0000B458 File Offset: 0x00009658
		public override bool MoveToFirstAttribute()
		{
			XPathNode[] array = this.pageCurrent;
			int num = this.idxCurrent;
			if (XPathNodeHelper.GetFirstAttribute(ref this.pageCurrent, ref this.idxCurrent))
			{
				this.pageParent = array;
				this.idxParent = num;
				return true;
			}
			return false;
		}

		// Token: 0x060002EB RID: 747 RVA: 0x0000B497 File Offset: 0x00009697
		public override bool MoveToNextAttribute()
		{
			return XPathNodeHelper.GetNextAttribute(ref this.pageCurrent, ref this.idxCurrent);
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x060002EC RID: 748 RVA: 0x0000B4AA File Offset: 0x000096AA
		public override bool HasAttributes
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].HasAttribute;
			}
		}

		// Token: 0x060002ED RID: 749 RVA: 0x0000B4C4 File Offset: 0x000096C4
		public override bool MoveToAttribute(string localName, string namespaceURI)
		{
			XPathNode[] array = this.pageCurrent;
			int num = this.idxCurrent;
			if (localName != this.atomizedLocalName)
			{
				this.atomizedLocalName = ((localName != null) ? this.NameTable.Get(localName) : null);
			}
			if (XPathNodeHelper.GetAttribute(ref this.pageCurrent, ref this.idxCurrent, this.atomizedLocalName, namespaceURI))
			{
				this.pageParent = array;
				this.idxParent = num;
				return true;
			}
			return false;
		}

		// Token: 0x060002EE RID: 750 RVA: 0x0000B52C File Offset: 0x0000972C
		public override bool MoveToFirstNamespace(XPathNamespaceScope namespaceScope)
		{
			XPathNode[] array;
			int num;
			if (namespaceScope == XPathNamespaceScope.Local)
			{
				num = XPathNodeHelper.GetLocalNamespaces(this.pageCurrent, this.idxCurrent, out array);
			}
			else
			{
				num = XPathNodeHelper.GetInScopeNamespaces(this.pageCurrent, this.idxCurrent, out array);
			}
			while (num != 0)
			{
				if (namespaceScope != XPathNamespaceScope.ExcludeXml || !array[num].IsXmlNamespaceNode)
				{
					this.pageParent = this.pageCurrent;
					this.idxParent = this.idxCurrent;
					this.pageCurrent = array;
					this.idxCurrent = num;
					return true;
				}
				num = array[num].GetSibling(out array);
			}
			return false;
		}

		// Token: 0x060002EF RID: 751 RVA: 0x0000B5B8 File Offset: 0x000097B8
		public override bool MoveToNextNamespace(XPathNamespaceScope scope)
		{
			XPathNode[] array = this.pageCurrent;
			int sibling = this.idxCurrent;
			if (array[sibling].NodeType != XPathNodeType.Namespace)
			{
				return false;
			}
			for (;;)
			{
				sibling = array[sibling].GetSibling(out array);
				if (sibling == 0)
				{
					break;
				}
				if (scope != XPathNamespaceScope.ExcludeXml)
				{
					goto Block_3;
				}
				if (!array[sibling].IsXmlNamespaceNode)
				{
					goto IL_6A;
				}
			}
			return false;
			Block_3:
			XPathNode[] array2;
			if (scope == XPathNamespaceScope.Local && (array[sibling].GetParent(out array2) != this.idxParent || array2 != this.pageParent))
			{
				return false;
			}
			IL_6A:
			this.pageCurrent = array;
			this.idxCurrent = sibling;
			return true;
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x0000B63E File Offset: 0x0000983E
		public override bool MoveToNext()
		{
			return XPathNodeHelper.GetContentSibling(ref this.pageCurrent, ref this.idxCurrent);
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x0000B651 File Offset: 0x00009851
		public override bool MoveToPrevious()
		{
			return this.idxParent == 0 && XPathNodeHelper.GetPreviousContentSibling(ref this.pageCurrent, ref this.idxCurrent);
		}

		// Token: 0x060002F2 RID: 754 RVA: 0x0000B670 File Offset: 0x00009870
		public override bool MoveToFirstChild()
		{
			if (this.pageCurrent[this.idxCurrent].HasCollapsedText)
			{
				this.pageParent = this.pageCurrent;
				this.idxParent = this.idxCurrent;
				this.idxCurrent = this.pageCurrent[this.idxCurrent].Document.GetCollapsedTextNode(out this.pageCurrent);
				return true;
			}
			return XPathNodeHelper.GetContentChild(ref this.pageCurrent, ref this.idxCurrent);
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x0000B6E8 File Offset: 0x000098E8
		public override bool MoveToParent()
		{
			if (this.idxParent != 0)
			{
				this.pageCurrent = this.pageParent;
				this.idxCurrent = this.idxParent;
				this.pageParent = null;
				this.idxParent = 0;
				return true;
			}
			return XPathNodeHelper.GetParent(ref this.pageCurrent, ref this.idxCurrent);
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x0000B738 File Offset: 0x00009938
		public override bool MoveTo(XPathNavigator other)
		{
			XPathDocumentNavigator xpathDocumentNavigator = other as XPathDocumentNavigator;
			if (xpathDocumentNavigator != null)
			{
				this.pageCurrent = xpathDocumentNavigator.pageCurrent;
				this.idxCurrent = xpathDocumentNavigator.idxCurrent;
				this.pageParent = xpathDocumentNavigator.pageParent;
				this.idxParent = xpathDocumentNavigator.idxParent;
				return true;
			}
			return false;
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x0000B784 File Offset: 0x00009984
		public override bool MoveToId(string id)
		{
			XPathNode[] array;
			int num = this.pageCurrent[this.idxCurrent].Document.LookupIdElement(id, out array);
			if (num != 0)
			{
				this.pageCurrent = array;
				this.idxCurrent = num;
				this.pageParent = null;
				this.idxParent = 0;
				return true;
			}
			return false;
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x0000B7D4 File Offset: 0x000099D4
		public override bool IsSamePosition(XPathNavigator other)
		{
			XPathDocumentNavigator xpathDocumentNavigator = other as XPathDocumentNavigator;
			return xpathDocumentNavigator != null && (this.idxCurrent == xpathDocumentNavigator.idxCurrent && this.pageCurrent == xpathDocumentNavigator.pageCurrent && this.idxParent == xpathDocumentNavigator.idxParent) && this.pageParent == xpathDocumentNavigator.pageParent;
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060002F7 RID: 759 RVA: 0x0000B827 File Offset: 0x00009A27
		public override bool HasChildren
		{
			get
			{
				return this.pageCurrent[this.idxCurrent].HasContentChild;
			}
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x0000B83F File Offset: 0x00009A3F
		public override void MoveToRoot()
		{
			if (this.idxParent != 0)
			{
				this.pageParent = null;
				this.idxParent = 0;
			}
			this.idxCurrent = this.pageCurrent[this.idxCurrent].GetRoot(out this.pageCurrent);
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x0000B879 File Offset: 0x00009A79
		public override bool MoveToChild(string localName, string namespaceURI)
		{
			if (localName != this.atomizedLocalName)
			{
				this.atomizedLocalName = ((localName != null) ? this.NameTable.Get(localName) : null);
			}
			return XPathNodeHelper.GetElementChild(ref this.pageCurrent, ref this.idxCurrent, this.atomizedLocalName, namespaceURI);
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000B8B4 File Offset: 0x00009AB4
		public override bool MoveToNext(string localName, string namespaceURI)
		{
			if (localName != this.atomizedLocalName)
			{
				this.atomizedLocalName = ((localName != null) ? this.NameTable.Get(localName) : null);
			}
			return XPathNodeHelper.GetElementSibling(ref this.pageCurrent, ref this.idxCurrent, this.atomizedLocalName, namespaceURI);
		}

		// Token: 0x060002FB RID: 763 RVA: 0x0000B8F0 File Offset: 0x00009AF0
		public override bool MoveToChild(XPathNodeType type)
		{
			if (!this.pageCurrent[this.idxCurrent].HasCollapsedText)
			{
				return XPathNodeHelper.GetContentChild(ref this.pageCurrent, ref this.idxCurrent, type);
			}
			if (type != XPathNodeType.Text && type != XPathNodeType.All)
			{
				return false;
			}
			this.pageParent = this.pageCurrent;
			this.idxParent = this.idxCurrent;
			this.idxCurrent = this.pageCurrent[this.idxCurrent].Document.GetCollapsedTextNode(out this.pageCurrent);
			return true;
		}

		// Token: 0x060002FC RID: 764 RVA: 0x0000B973 File Offset: 0x00009B73
		public override bool MoveToNext(XPathNodeType type)
		{
			return XPathNodeHelper.GetContentSibling(ref this.pageCurrent, ref this.idxCurrent, type);
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000B988 File Offset: 0x00009B88
		public override bool MoveToFollowing(string localName, string namespaceURI, XPathNavigator end)
		{
			if (localName != this.atomizedLocalName)
			{
				this.atomizedLocalName = ((localName != null) ? this.NameTable.Get(localName) : null);
			}
			XPathNode[] pageEnd;
			int followingEnd = this.GetFollowingEnd(end as XPathDocumentNavigator, false, out pageEnd);
			if (this.idxParent == 0)
			{
				return XPathNodeHelper.GetElementFollowing(ref this.pageCurrent, ref this.idxCurrent, pageEnd, followingEnd, this.atomizedLocalName, namespaceURI);
			}
			if (!XPathNodeHelper.GetElementFollowing(ref this.pageParent, ref this.idxParent, pageEnd, followingEnd, this.atomizedLocalName, namespaceURI))
			{
				return false;
			}
			this.pageCurrent = this.pageParent;
			this.idxCurrent = this.idxParent;
			this.pageParent = null;
			this.idxParent = 0;
			return true;
		}

		// Token: 0x060002FE RID: 766 RVA: 0x0000BA30 File Offset: 0x00009C30
		public override bool MoveToFollowing(XPathNodeType type, XPathNavigator end)
		{
			XPathDocumentNavigator xpathDocumentNavigator = end as XPathDocumentNavigator;
			XPathNode[] array;
			int followingEnd;
			if (type == XPathNodeType.Text || type == XPathNodeType.All)
			{
				if (this.pageCurrent[this.idxCurrent].HasCollapsedText)
				{
					if (xpathDocumentNavigator != null && this.idxCurrent == xpathDocumentNavigator.idxParent && this.pageCurrent == xpathDocumentNavigator.pageParent)
					{
						return false;
					}
					this.pageParent = this.pageCurrent;
					this.idxParent = this.idxCurrent;
					this.idxCurrent = this.pageCurrent[this.idxCurrent].Document.GetCollapsedTextNode(out this.pageCurrent);
					return true;
				}
				else if (type == XPathNodeType.Text)
				{
					followingEnd = this.GetFollowingEnd(xpathDocumentNavigator, true, out array);
					XPathNode[] array2;
					int num;
					if (this.idxParent != 0)
					{
						array2 = this.pageParent;
						num = this.idxParent;
					}
					else
					{
						array2 = this.pageCurrent;
						num = this.idxCurrent;
					}
					if (xpathDocumentNavigator != null && xpathDocumentNavigator.idxParent != 0 && num == followingEnd && array2 == array)
					{
						return false;
					}
					if (!XPathNodeHelper.GetTextFollowing(ref array2, ref num, array, followingEnd))
					{
						return false;
					}
					if (array2[num].NodeType == XPathNodeType.Element)
					{
						this.idxCurrent = array2[num].Document.GetCollapsedTextNode(out this.pageCurrent);
						this.pageParent = array2;
						this.idxParent = num;
					}
					else
					{
						this.pageCurrent = array2;
						this.idxCurrent = num;
						this.pageParent = null;
						this.idxParent = 0;
					}
					return true;
				}
			}
			followingEnd = this.GetFollowingEnd(xpathDocumentNavigator, false, out array);
			if (this.idxParent == 0)
			{
				return XPathNodeHelper.GetContentFollowing(ref this.pageCurrent, ref this.idxCurrent, array, followingEnd, type);
			}
			if (!XPathNodeHelper.GetContentFollowing(ref this.pageParent, ref this.idxParent, array, followingEnd, type))
			{
				return false;
			}
			this.pageCurrent = this.pageParent;
			this.idxCurrent = this.idxParent;
			this.pageParent = null;
			this.idxParent = 0;
			return true;
		}

		// Token: 0x060002FF RID: 767 RVA: 0x0000BBED File Offset: 0x00009DED
		public override XPathNodeIterator SelectChildren(XPathNodeType type)
		{
			return new XPathDocumentKindChildIterator(this, type);
		}

		// Token: 0x06000300 RID: 768 RVA: 0x0000BBF6 File Offset: 0x00009DF6
		public override XPathNodeIterator SelectChildren(string name, string namespaceURI)
		{
			if (name == null || name.Length == 0)
			{
				return base.SelectChildren(name, namespaceURI);
			}
			return new XPathDocumentElementChildIterator(this, name, namespaceURI);
		}

		// Token: 0x06000301 RID: 769 RVA: 0x0000BC14 File Offset: 0x00009E14
		public override XPathNodeIterator SelectDescendants(XPathNodeType type, bool matchSelf)
		{
			return new XPathDocumentKindDescendantIterator(this, type, matchSelf);
		}

		// Token: 0x06000302 RID: 770 RVA: 0x0000BC1E File Offset: 0x00009E1E
		public override XPathNodeIterator SelectDescendants(string name, string namespaceURI, bool matchSelf)
		{
			if (name == null || name.Length == 0)
			{
				return base.SelectDescendants(name, namespaceURI, matchSelf);
			}
			return new XPathDocumentElementDescendantIterator(this, name, namespaceURI, matchSelf);
		}

		// Token: 0x06000303 RID: 771 RVA: 0x0000BC40 File Offset: 0x00009E40
		public override XmlNodeOrder ComparePosition(XPathNavigator other)
		{
			XPathDocumentNavigator xpathDocumentNavigator = other as XPathDocumentNavigator;
			if (xpathDocumentNavigator != null)
			{
				XPathDocument document = this.pageCurrent[this.idxCurrent].Document;
				XPathDocument document2 = xpathDocumentNavigator.pageCurrent[xpathDocumentNavigator.idxCurrent].Document;
				if (document == document2)
				{
					int num = this.GetPrimaryLocation();
					int num2 = xpathDocumentNavigator.GetPrimaryLocation();
					if (num == num2)
					{
						num = this.GetSecondaryLocation();
						num2 = xpathDocumentNavigator.GetSecondaryLocation();
						if (num == num2)
						{
							return XmlNodeOrder.Same;
						}
					}
					if (num >= num2)
					{
						return XmlNodeOrder.After;
					}
					return XmlNodeOrder.Before;
				}
			}
			return XmlNodeOrder.Unknown;
		}

		// Token: 0x06000304 RID: 772 RVA: 0x0000BCB8 File Offset: 0x00009EB8
		public override bool IsDescendant(XPathNavigator other)
		{
			XPathDocumentNavigator xpathDocumentNavigator = other as XPathDocumentNavigator;
			if (xpathDocumentNavigator != null)
			{
				XPathNode[] array;
				int parent;
				if (xpathDocumentNavigator.idxParent != 0)
				{
					array = xpathDocumentNavigator.pageParent;
					parent = xpathDocumentNavigator.idxParent;
				}
				else
				{
					parent = xpathDocumentNavigator.pageCurrent[xpathDocumentNavigator.idxCurrent].GetParent(out array);
				}
				while (parent != 0)
				{
					if (parent == this.idxCurrent && array == this.pageCurrent)
					{
						return true;
					}
					parent = array[parent].GetParent(out array);
				}
			}
			return false;
		}

		// Token: 0x06000305 RID: 773 RVA: 0x0000BD29 File Offset: 0x00009F29
		private int GetPrimaryLocation()
		{
			if (this.idxParent == 0)
			{
				return XPathNodeHelper.GetLocation(this.pageCurrent, this.idxCurrent);
			}
			return XPathNodeHelper.GetLocation(this.pageParent, this.idxParent);
		}

		// Token: 0x06000306 RID: 774 RVA: 0x0000BD58 File Offset: 0x00009F58
		private int GetSecondaryLocation()
		{
			if (this.idxParent == 0)
			{
				return int.MinValue;
			}
			XPathNodeType nodeType = this.pageCurrent[this.idxCurrent].NodeType;
			if (nodeType == XPathNodeType.Attribute)
			{
				return XPathNodeHelper.GetLocation(this.pageCurrent, this.idxCurrent);
			}
			if (nodeType == XPathNodeType.Namespace)
			{
				return -2147483647 + XPathNodeHelper.GetLocation(this.pageCurrent, this.idxCurrent);
			}
			return int.MaxValue;
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000307 RID: 775 RVA: 0x0000BDC4 File Offset: 0x00009FC4
		internal override string UniqueId
		{
			get
			{
				char[] array = new char[16];
				int length = 0;
				array[length++] = XPathNavigator.NodeTypeLetter[(int)this.pageCurrent[this.idxCurrent].NodeType];
				int num;
				if (this.idxParent != 0)
				{
					num = (this.pageParent[0].PageInfo.PageNumber - 1 << 16 | this.idxParent - 1);
					do
					{
						array[length++] = XPathNavigator.UniqueIdTbl[num & 31];
						num >>= 5;
					}
					while (num != 0);
					array[length++] = '0';
				}
				num = (this.pageCurrent[0].PageInfo.PageNumber - 1 << 16 | this.idxCurrent - 1);
				do
				{
					array[length++] = XPathNavigator.UniqueIdTbl[num & 31];
					num >>= 5;
				}
				while (num != 0);
				return new string(array, 0, length);
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000308 RID: 776 RVA: 0x0000BE91 File Offset: 0x0000A091
		public override object UnderlyingObject
		{
			get
			{
				return this.Clone();
			}
		}

		// Token: 0x06000309 RID: 777 RVA: 0x0000BE99 File Offset: 0x0000A099
		public bool HasLineInfo()
		{
			return this.pageCurrent[this.idxCurrent].Document.HasLineInfo;
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600030A RID: 778 RVA: 0x0000BEB6 File Offset: 0x0000A0B6
		public int LineNumber
		{
			get
			{
				if (this.idxParent != 0 && this.NodeType == XPathNodeType.Text)
				{
					return this.pageParent[this.idxParent].LineNumber;
				}
				return this.pageCurrent[this.idxCurrent].LineNumber;
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600030B RID: 779 RVA: 0x0000BEF6 File Offset: 0x0000A0F6
		public int LinePosition
		{
			get
			{
				if (this.idxParent != 0 && this.NodeType == XPathNodeType.Text)
				{
					return this.pageParent[this.idxParent].CollapsedLinePosition;
				}
				return this.pageCurrent[this.idxCurrent].LinePosition;
			}
		}

		// Token: 0x0600030C RID: 780 RVA: 0x0000BF36 File Offset: 0x0000A136
		public int GetPositionHashCode()
		{
			return this.idxCurrent ^ this.idxParent;
		}

		// Token: 0x0600030D RID: 781 RVA: 0x0000BF48 File Offset: 0x0000A148
		public bool IsElementMatch(string localName, string namespaceURI)
		{
			if (localName != this.atomizedLocalName)
			{
				this.atomizedLocalName = ((localName != null) ? this.NameTable.Get(localName) : null);
			}
			return this.idxParent == 0 && this.pageCurrent[this.idxCurrent].ElementMatch(this.atomizedLocalName, namespaceURI);
		}

		// Token: 0x0600030E RID: 782 RVA: 0x0000BF9D File Offset: 0x0000A19D
		public bool IsContentKindMatch(XPathNodeType typ)
		{
			return (1 << (int)this.pageCurrent[this.idxCurrent].NodeType & XPathNavigator.GetContentKindMask(typ)) != 0;
		}

		// Token: 0x0600030F RID: 783 RVA: 0x0000BFC4 File Offset: 0x0000A1C4
		public bool IsKindMatch(XPathNodeType typ)
		{
			return (1 << (int)this.pageCurrent[this.idxCurrent].NodeType & XPathNavigator.GetKindMask(typ)) != 0;
		}

		// Token: 0x06000310 RID: 784 RVA: 0x0000BFEC File Offset: 0x0000A1EC
		private int GetFollowingEnd(XPathDocumentNavigator end, bool useParentOfVirtual, out XPathNode[] pageEnd)
		{
			if (end == null || this.pageCurrent[this.idxCurrent].Document != end.pageCurrent[end.idxCurrent].Document)
			{
				pageEnd = null;
				return 0;
			}
			if (end.idxParent == 0)
			{
				pageEnd = end.pageCurrent;
				return end.idxCurrent;
			}
			pageEnd = end.pageParent;
			if (!useParentOfVirtual)
			{
				return end.idxParent + 1;
			}
			return end.idxParent;
		}

		// Token: 0x0400018B RID: 395
		private XPathNode[] pageCurrent;

		// Token: 0x0400018C RID: 396
		private XPathNode[] pageParent;

		// Token: 0x0400018D RID: 397
		private int idxCurrent;

		// Token: 0x0400018E RID: 398
		private int idxParent;

		// Token: 0x0400018F RID: 399
		private string atomizedLocalName;
	}
}
