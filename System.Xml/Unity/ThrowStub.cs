﻿using System;

namespace Unity
{
	// Token: 0x02000576 RID: 1398
	internal sealed class ThrowStub : ObjectDisposedException
	{
		// Token: 0x0600348F RID: 13455 RVA: 0x0010F4BB File Offset: 0x0010D6BB
		public static void ThrowNotSupportedException()
		{
			throw new PlatformNotSupportedException();
		}
	}
}
