﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Token: 0x020000B0 RID: 176
[CompilerGenerated]
internal sealed class <PrivateImplementationDetails>
{
	// Token: 0x060006A4 RID: 1700 RVA: 0x0001FD68 File Offset: 0x0001DF68
	internal static uint ComputeStringHash(string s)
	{
		uint num;
		if (s != null)
		{
			num = 2166136261U;
			for (int i = 0; i < s.Length; i++)
			{
				num = ((uint)s[i] ^ num) * 16777619U;
			}
		}
		return num;
	}

	// Token: 0x0400043A RID: 1082 RVA: 0x0004BF6C File Offset: 0x0004A16C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 12D04472A8285260EA12FD3813CDFA9F2D2B548C;

	// Token: 0x0400043B RID: 1083 RVA: 0x0004BF74 File Offset: 0x0004A174
	internal static readonly int 1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C;

	// Token: 0x0400043C RID: 1084 RVA: 0x0004BF7C File Offset: 0x0004A17C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 13A35EF1A549297C70E2AD46045BBD2ECA17852D;

	// Token: 0x0400043D RID: 1085 RVA: 0x0004BF84 File Offset: 0x0004A184
	internal static readonly long 16968835DEF6DD3BB86EABA9DEC53BF41851CD6D;

	// Token: 0x0400043E RID: 1086 RVA: 0x0004BF8C File Offset: 0x0004A18C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 1A84029C80CB5518379F199F53FF08A7B764F8FD;

	// Token: 0x0400043F RID: 1087 RVA: 0x0004BF94 File Offset: 0x0004A194
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=37 1B5753DCEA0F7C708789F946C290D38CC677F4CA;

	// Token: 0x04000440 RID: 1088 RVA: 0x0004BFBC File Offset: 0x0004A1BC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 235D99572263B22ADFEE10FDA0C25E12F4D94FFC;

	// Token: 0x04000441 RID: 1089 RVA: 0x0004BFC4 File Offset: 0x0004A1C4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=14 2D3CF0F15AC2DDEC2956EA1B7BBE43FB8B923130;

	// Token: 0x04000442 RID: 1090 RVA: 0x0004BFD4 File Offset: 0x0004A1D4
	internal static readonly int 31D8729F7377B44017C0A2395A582C9CA4163277;

	// Token: 0x04000443 RID: 1091 RVA: 0x0004BFDC File Offset: 0x0004A1DC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 320B018758ECE3752FFEDBAEB1A6DB67C80B9359;

	// Token: 0x04000444 RID: 1092 RVA: 0x0004C01C File Offset: 0x0004A21C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 3E3442C7396F3F2BB4C7348F4A2074C7DC677D68;

	// Token: 0x04000445 RID: 1093 RVA: 0x0004C024 File Offset: 0x0004A224
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 433175D38B13FFE177FDD661A309F1B528B3F6E2;

	// Token: 0x04000446 RID: 1094 RVA: 0x0004C124 File Offset: 0x0004A324
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=96 4989E5469B40416DC5AFB739C747E32B40CC5C77;

	// Token: 0x04000447 RID: 1095 RVA: 0x0004C184 File Offset: 0x0004A384
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=192 49ECABA9727A1AF0636082C467485A1A9A04B669;

	// Token: 0x04000448 RID: 1096 RVA: 0x0004C244 File Offset: 0x0004A444
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=48 4E3B533C39447AAEB59A8E48FABD7E15B5B5D195;

	// Token: 0x04000449 RID: 1097 RVA: 0x0004C274 File Offset: 0x0004A474
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=10 56DFA5053B3131883637F53219E7D88CCEF35949;

	// Token: 0x0400044A RID: 1098 RVA: 0x0004C284 File Offset: 0x0004A484
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=9 6D49C9D487D7AD3491ECE08732D68A593CC2038D;

	// Token: 0x0400044B RID: 1099 RVA: 0x0004C294 File Offset: 0x0004A494
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3132 6E5DC824F803F8565AF31B42199DAE39FE7F4EA9;

	// Token: 0x0400044C RID: 1100 RVA: 0x0004CED4 File Offset: 0x0004B0D4
	internal static readonly long 6FA00AC9FFFD87F82A38A7F9ECC8134F4A7052AF;

	// Token: 0x0400044D RID: 1101 RVA: 0x0004CEDC File Offset: 0x0004B0DC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 736D39815215889F11249D9958F6ED12D37B9F57;

	// Token: 0x0400044E RID: 1102 RVA: 0x0004CEE4 File Offset: 0x0004B0E4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 86F4F563FA2C61798AE6238D789139739428463A;

	// Token: 0x0400044F RID: 1103 RVA: 0x0004CEEC File Offset: 0x0004B0EC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 97FB30C84FF4A41CD4625B44B2940BFC8DB43003;

	// Token: 0x04000450 RID: 1104 RVA: 0x0004CEF4 File Offset: 0x0004B0F4
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 9A9C3962CD4753376E3507C8CB5FD8FCC4B4EDB5;

	// Token: 0x04000451 RID: 1105 RVA: 0x0004CF34 File Offset: 0x0004B134
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 9BB00D1FCCBAF03165447FC8028E7CA07CA9FE88;

	// Token: 0x04000452 RID: 1106 RVA: 0x0004CF3C File Offset: 0x0004B13C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 A323DB0813C4D072957BA6FDA79D9776674CD06B;

	// Token: 0x04000453 RID: 1107 RVA: 0x0004CF44 File Offset: 0x0004B144
	internal static readonly long AEA5F1CC5CFE1660539EDD691FE017F775F63A0D;

	// Token: 0x04000454 RID: 1108 RVA: 0x0004CF4C File Offset: 0x0004B14C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=20 BE1BDEC0AA74B4DCB079943E70528096CCA985F8;

	// Token: 0x04000455 RID: 1109 RVA: 0x0004CF64 File Offset: 0x0004B164
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 BF477463CE2F5EF38FC4C644BBBF4DF109E7670A;

	// Token: 0x04000456 RID: 1110 RVA: 0x0004CF6C File Offset: 0x0004B16C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=128 C033BD4351FBA3732545EA2E016D52B0FC3E69EC;

	// Token: 0x04000457 RID: 1111 RVA: 0x0004CFEC File Offset: 0x0004B1EC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 CF0B42666EF5E37EDEA0AB8E173E42C196D03814;

	// Token: 0x04000458 RID: 1112 RVA: 0x0004D02C File Offset: 0x0004B22C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=32 D28E8ABDBD777A482CE0EE5C24814ACAE52AABFE;

	// Token: 0x04000459 RID: 1113 RVA: 0x0004D04C File Offset: 0x0004B24C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=256 D2C5BAE967587C6F3D9F2C4551911E0575A1101F;

	// Token: 0x0400045A RID: 1114 RVA: 0x0004D14C File Offset: 0x0004B34C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=524 D693078666967609CB4A2B840921F9E052031847;

	// Token: 0x0400045B RID: 1115 RVA: 0x0004D35C File Offset: 0x0004B55C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=64 E75835D001C843F156FBA01B001DFE1B8029AC17;

	// Token: 0x0400045C RID: 1116 RVA: 0x0004D39C File Offset: 0x0004B59C
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=10 EC5BB4F59D4B9B2E9ECD3904D44A8275F23AFB11;

	// Token: 0x0400045D RID: 1117 RVA: 0x0004D3AC File Offset: 0x0004B5AC
	internal static readonly <PrivateImplementationDetails>.__StaticArrayInitTypeSize=3 EC83FB16C20052BEE2B4025159BC2ED45C9C70C3;

	// Token: 0x020000EB RID: 235
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 3)]
	private struct __StaticArrayInitTypeSize=3
	{
	}

	// Token: 0x020000EC RID: 236
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 9)]
	private struct __StaticArrayInitTypeSize=9
	{
	}

	// Token: 0x020000ED RID: 237
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 10)]
	private struct __StaticArrayInitTypeSize=10
	{
	}

	// Token: 0x020000EE RID: 238
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 14)]
	private struct __StaticArrayInitTypeSize=14
	{
	}

	// Token: 0x020000EF RID: 239
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 20)]
	private struct __StaticArrayInitTypeSize=20
	{
	}

	// Token: 0x020000F0 RID: 240
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 32)]
	private struct __StaticArrayInitTypeSize=32
	{
	}

	// Token: 0x020000F1 RID: 241
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 37)]
	private struct __StaticArrayInitTypeSize=37
	{
	}

	// Token: 0x020000F2 RID: 242
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 48)]
	private struct __StaticArrayInitTypeSize=48
	{
	}

	// Token: 0x020000F3 RID: 243
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 64)]
	private struct __StaticArrayInitTypeSize=64
	{
	}

	// Token: 0x020000F4 RID: 244
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 96)]
	private struct __StaticArrayInitTypeSize=96
	{
	}

	// Token: 0x020000F5 RID: 245
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 128)]
	private struct __StaticArrayInitTypeSize=128
	{
	}

	// Token: 0x020000F6 RID: 246
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 192)]
	private struct __StaticArrayInitTypeSize=192
	{
	}

	// Token: 0x020000F7 RID: 247
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 256)]
	private struct __StaticArrayInitTypeSize=256
	{
	}

	// Token: 0x020000F8 RID: 248
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 524)]
	private struct __StaticArrayInitTypeSize=524
	{
	}

	// Token: 0x020000F9 RID: 249
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 3132)]
	private struct __StaticArrayInitTypeSize=3132
	{
	}
}
