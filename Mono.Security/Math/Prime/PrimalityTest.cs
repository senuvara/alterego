﻿using System;

namespace Mono.Math.Prime
{
	// Token: 0x020000AB RID: 171
	// (Invoke) Token: 0x0600068F RID: 1679
	public delegate bool PrimalityTest(BigInteger bi, ConfidenceFactor confidence);
}
