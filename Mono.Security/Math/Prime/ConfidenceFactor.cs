﻿using System;

namespace Mono.Math.Prime
{
	// Token: 0x020000AA RID: 170
	public enum ConfidenceFactor
	{
		// Token: 0x04000434 RID: 1076
		ExtraLow,
		// Token: 0x04000435 RID: 1077
		Low,
		// Token: 0x04000436 RID: 1078
		Medium,
		// Token: 0x04000437 RID: 1079
		High,
		// Token: 0x04000438 RID: 1080
		ExtraHigh,
		// Token: 0x04000439 RID: 1081
		Provable
	}
}
