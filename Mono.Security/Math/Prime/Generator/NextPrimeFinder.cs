﻿using System;

namespace Mono.Math.Prime.Generator
{
	// Token: 0x020000AD RID: 173
	public class NextPrimeFinder : SequentialSearchPrimeGeneratorBase
	{
		// Token: 0x06000697 RID: 1687 RVA: 0x0001FC1B File Offset: 0x0001DE1B
		protected override BigInteger GenerateSearchBase(int bits, object Context)
		{
			if (Context == null)
			{
				throw new ArgumentNullException("Context");
			}
			BigInteger bigInteger = new BigInteger((BigInteger)Context);
			bigInteger.SetBit(0U);
			return bigInteger;
		}

		// Token: 0x06000698 RID: 1688 RVA: 0x0001FC3D File Offset: 0x0001DE3D
		public NextPrimeFinder()
		{
		}
	}
}
