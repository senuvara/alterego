﻿using System;

namespace Mono.Math.Prime.Generator
{
	// Token: 0x020000AE RID: 174
	public abstract class PrimeGeneratorBase
	{
		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000699 RID: 1689 RVA: 0x0001FC45 File Offset: 0x0001DE45
		public virtual ConfidenceFactor Confidence
		{
			get
			{
				return ConfidenceFactor.Medium;
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x0600069A RID: 1690 RVA: 0x0001FC48 File Offset: 0x0001DE48
		public virtual PrimalityTest PrimalityTest
		{
			get
			{
				return new PrimalityTest(PrimalityTests.RabinMillerTest);
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x0600069B RID: 1691 RVA: 0x0001FC56 File Offset: 0x0001DE56
		public virtual int TrialDivisionBounds
		{
			get
			{
				return 4000;
			}
		}

		// Token: 0x0600069C RID: 1692 RVA: 0x0001FC5D File Offset: 0x0001DE5D
		protected bool PostTrialDivisionTests(BigInteger bi)
		{
			return this.PrimalityTest(bi, this.Confidence);
		}

		// Token: 0x0600069D RID: 1693
		public abstract BigInteger GenerateNewPrime(int bits);

		// Token: 0x0600069E RID: 1694 RVA: 0x0001FC71 File Offset: 0x0001DE71
		protected PrimeGeneratorBase()
		{
		}
	}
}
