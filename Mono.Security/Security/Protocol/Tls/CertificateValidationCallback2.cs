﻿using System;
using Mono.Security.Interface;
using Mono.Security.X509;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000047 RID: 71
	// (Invoke) Token: 0x060002CD RID: 717
	public delegate ValidationResult CertificateValidationCallback2(X509CertificateCollection collection);
}
