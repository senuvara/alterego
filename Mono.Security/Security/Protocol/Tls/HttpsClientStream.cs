﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x0200003C RID: 60
	[Obsolete("This class is obsolete and will be removed shortly.")]
	internal class HttpsClientStream : SslClientStream
	{
		// Token: 0x06000283 RID: 643 RVA: 0x0000ED3C File Offset: 0x0000CF3C
		public HttpsClientStream(Stream stream, X509CertificateCollection clientCertificates, HttpWebRequest request, byte[] buffer) : base(stream, request.Address.Host, false, (SecurityProtocolType)ServicePointManager.SecurityProtocol, clientCertificates)
		{
			this._request = request;
			this._status = 0;
			if (buffer != null)
			{
				base.InputBuffer.Write(buffer, 0, buffer.Length);
			}
			base.CheckCertRevocationStatus = ServicePointManager.CheckCertificateRevocationList;
			base.ClientCertSelection += delegate(X509CertificateCollection clientCerts, X509Certificate serverCertificate, string targetHost, X509CertificateCollection serverRequestedCertificates)
			{
				if (clientCerts != null && clientCerts.Count != 0)
				{
					return clientCerts[0];
				}
				return null;
			};
			base.PrivateKeySelection += delegate(X509Certificate certificate, string targetHost)
			{
				X509Certificate2 x509Certificate = certificate as X509Certificate2;
				if (x509Certificate != null)
				{
					return x509Certificate.PrivateKey;
				}
				return null;
			};
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000284 RID: 644 RVA: 0x0000EDDC File Offset: 0x0000CFDC
		public bool TrustFailure
		{
			get
			{
				int status = this._status;
				return status - -2146762487 <= 1;
			}
		}

		// Token: 0x06000285 RID: 645 RVA: 0x0000EE00 File Offset: 0x0000D000
		internal override bool RaiseServerCertificateValidation(X509Certificate certificate, int[] certificateErrors)
		{
			bool flag = certificateErrors.Length != 0;
			this._status = (flag ? certificateErrors[0] : 0);
			if (ServicePointManager.CertificatePolicy != null)
			{
				ServicePoint servicePoint = this._request.ServicePoint;
				if (!ServicePointManager.CertificatePolicy.CheckValidationResult(servicePoint, certificate, this._request, this._status))
				{
					return false;
				}
				flag = true;
			}
			if (this.HaveRemoteValidation2Callback)
			{
				return flag;
			}
			RemoteCertificateValidationCallback serverCertificateValidationCallback = ServicePointManager.ServerCertificateValidationCallback;
			if (serverCertificateValidationCallback != null)
			{
				SslPolicyErrors sslPolicyErrors = SslPolicyErrors.None;
				foreach (int num in certificateErrors)
				{
					if (num == -2146762490)
					{
						sslPolicyErrors |= SslPolicyErrors.RemoteCertificateNotAvailable;
					}
					else if (num == -2146762481)
					{
						sslPolicyErrors |= SslPolicyErrors.RemoteCertificateNameMismatch;
					}
					else
					{
						sslPolicyErrors |= SslPolicyErrors.RemoteCertificateChainErrors;
					}
				}
				X509Certificate2 certificate2 = new X509Certificate2(certificate.GetRawCertData());
				X509Chain x509Chain = new X509Chain();
				if (!x509Chain.Build(certificate2))
				{
					sslPolicyErrors |= SslPolicyErrors.RemoteCertificateChainErrors;
				}
				return serverCertificateValidationCallback(this._request, certificate2, x509Chain, sslPolicyErrors);
			}
			return flag;
		}

		// Token: 0x04000175 RID: 373
		private HttpWebRequest _request;

		// Token: 0x04000176 RID: 374
		private int _status;

		// Token: 0x020000DD RID: 221
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000779 RID: 1913 RVA: 0x00021BEF File Offset: 0x0001FDEF
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600077A RID: 1914 RVA: 0x00021BFB File Offset: 0x0001FDFB
			public <>c()
			{
			}

			// Token: 0x0600077B RID: 1915 RVA: 0x00021C03 File Offset: 0x0001FE03
			internal X509Certificate <.ctor>b__2_0(X509CertificateCollection clientCerts, X509Certificate serverCertificate, string targetHost, X509CertificateCollection serverRequestedCertificates)
			{
				if (clientCerts != null && clientCerts.Count != 0)
				{
					return clientCerts[0];
				}
				return null;
			}

			// Token: 0x0600077C RID: 1916 RVA: 0x00021C1C File Offset: 0x0001FE1C
			internal AsymmetricAlgorithm <.ctor>b__2_1(X509Certificate certificate, string targetHost)
			{
				X509Certificate2 x509Certificate = certificate as X509Certificate2;
				if (x509Certificate != null)
				{
					return x509Certificate.PrivateKey;
				}
				return null;
			}

			// Token: 0x040004E7 RID: 1255
			public static readonly HttpsClientStream.<>c <>9 = new HttpsClientStream.<>c();

			// Token: 0x040004E8 RID: 1256
			public static CertificateSelectionCallback <>9__2_0;

			// Token: 0x040004E9 RID: 1257
			public static PrivateKeySelectionCallback <>9__2_1;
		}
	}
}
