﻿using System;
using System.Diagnostics;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000038 RID: 56
	internal class DebugHelper
	{
		// Token: 0x0600027C RID: 636 RVA: 0x0000ECC0 File Offset: 0x0000CEC0
		[Conditional("DEBUG")]
		public static void Initialize()
		{
			if (!DebugHelper.isInitialized)
			{
				DebugHelper.isInitialized = true;
			}
		}

		// Token: 0x0600027D RID: 637 RVA: 0x0000ECCF File Offset: 0x0000CECF
		[Conditional("DEBUG")]
		public static void WriteLine(string format, params object[] args)
		{
		}

		// Token: 0x0600027E RID: 638 RVA: 0x0000ECD1 File Offset: 0x0000CED1
		[Conditional("DEBUG")]
		public static void WriteLine(string message)
		{
		}

		// Token: 0x0600027F RID: 639 RVA: 0x0000ECD3 File Offset: 0x0000CED3
		[Conditional("DEBUG")]
		public static void WriteLine(string message, byte[] buffer)
		{
		}

		// Token: 0x06000280 RID: 640 RVA: 0x0000ECD5 File Offset: 0x0000CED5
		[Conditional("DEBUG")]
		public static void WriteBuffer(byte[] buffer)
		{
		}

		// Token: 0x06000281 RID: 641 RVA: 0x0000ECD8 File Offset: 0x0000CED8
		[Conditional("DEBUG")]
		public static void WriteBuffer(byte[] buffer, int index, int length)
		{
			for (int i = index; i < length; i += 16)
			{
				int num = (length - i >= 16) ? 16 : (length - i);
				string str = "";
				for (int j = 0; j < num; j++)
				{
					str = str + buffer[i + j].ToString("x2") + " ";
				}
			}
		}

		// Token: 0x06000282 RID: 642 RVA: 0x0000ED32 File Offset: 0x0000CF32
		public DebugHelper()
		{
		}

		// Token: 0x04000166 RID: 358
		private static bool isInitialized;
	}
}
