﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using Mono.Security.Interface;
using Mono.Security.Protocol.Tls.Handshake;
using Mono.Security.X509;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x0200004C RID: 76
	public class SslServerStream : SslStreamBase
	{
		// Token: 0x14000005 RID: 5
		// (add) Token: 0x06000304 RID: 772 RVA: 0x00011168 File Offset: 0x0000F368
		// (remove) Token: 0x06000305 RID: 773 RVA: 0x000111A0 File Offset: 0x0000F3A0
		internal event CertificateValidationCallback ClientCertValidation
		{
			[CompilerGenerated]
			add
			{
				CertificateValidationCallback certificateValidationCallback = this.ClientCertValidation;
				CertificateValidationCallback certificateValidationCallback2;
				do
				{
					certificateValidationCallback2 = certificateValidationCallback;
					CertificateValidationCallback value2 = (CertificateValidationCallback)Delegate.Combine(certificateValidationCallback2, value);
					certificateValidationCallback = Interlocked.CompareExchange<CertificateValidationCallback>(ref this.ClientCertValidation, value2, certificateValidationCallback2);
				}
				while (certificateValidationCallback != certificateValidationCallback2);
			}
			[CompilerGenerated]
			remove
			{
				CertificateValidationCallback certificateValidationCallback = this.ClientCertValidation;
				CertificateValidationCallback certificateValidationCallback2;
				do
				{
					certificateValidationCallback2 = certificateValidationCallback;
					CertificateValidationCallback value2 = (CertificateValidationCallback)Delegate.Remove(certificateValidationCallback2, value);
					certificateValidationCallback = Interlocked.CompareExchange<CertificateValidationCallback>(ref this.ClientCertValidation, value2, certificateValidationCallback2);
				}
				while (certificateValidationCallback != certificateValidationCallback2);
			}
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x06000306 RID: 774 RVA: 0x000111D8 File Offset: 0x0000F3D8
		// (remove) Token: 0x06000307 RID: 775 RVA: 0x00011210 File Offset: 0x0000F410
		internal event PrivateKeySelectionCallback PrivateKeySelection
		{
			[CompilerGenerated]
			add
			{
				PrivateKeySelectionCallback privateKeySelectionCallback = this.PrivateKeySelection;
				PrivateKeySelectionCallback privateKeySelectionCallback2;
				do
				{
					privateKeySelectionCallback2 = privateKeySelectionCallback;
					PrivateKeySelectionCallback value2 = (PrivateKeySelectionCallback)Delegate.Combine(privateKeySelectionCallback2, value);
					privateKeySelectionCallback = Interlocked.CompareExchange<PrivateKeySelectionCallback>(ref this.PrivateKeySelection, value2, privateKeySelectionCallback2);
				}
				while (privateKeySelectionCallback != privateKeySelectionCallback2);
			}
			[CompilerGenerated]
			remove
			{
				PrivateKeySelectionCallback privateKeySelectionCallback = this.PrivateKeySelection;
				PrivateKeySelectionCallback privateKeySelectionCallback2;
				do
				{
					privateKeySelectionCallback2 = privateKeySelectionCallback;
					PrivateKeySelectionCallback value2 = (PrivateKeySelectionCallback)Delegate.Remove(privateKeySelectionCallback2, value);
					privateKeySelectionCallback = Interlocked.CompareExchange<PrivateKeySelectionCallback>(ref this.PrivateKeySelection, value2, privateKeySelectionCallback2);
				}
				while (privateKeySelectionCallback != privateKeySelectionCallback2);
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x06000308 RID: 776 RVA: 0x00011245 File Offset: 0x0000F445
		public System.Security.Cryptography.X509Certificates.X509Certificate ClientCertificate
		{
			get
			{
				if (this.context.HandshakeState == HandshakeState.Finished)
				{
					return this.context.ClientSettings.ClientCertificate;
				}
				return null;
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x06000309 RID: 777 RVA: 0x00011267 File Offset: 0x0000F467
		// (set) Token: 0x0600030A RID: 778 RVA: 0x0001126F File Offset: 0x0000F46F
		public CertificateValidationCallback ClientCertValidationDelegate
		{
			get
			{
				return this.ClientCertValidation;
			}
			set
			{
				this.ClientCertValidation = value;
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x0600030B RID: 779 RVA: 0x00011278 File Offset: 0x0000F478
		// (set) Token: 0x0600030C RID: 780 RVA: 0x00011280 File Offset: 0x0000F480
		public PrivateKeySelectionCallback PrivateKeyCertSelectionDelegate
		{
			get
			{
				return this.PrivateKeySelection;
			}
			set
			{
				this.PrivateKeySelection = value;
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x0600030D RID: 781 RVA: 0x0001128C File Offset: 0x0000F48C
		// (remove) Token: 0x0600030E RID: 782 RVA: 0x000112C4 File Offset: 0x0000F4C4
		public event CertificateValidationCallback2 ClientCertValidation2
		{
			[CompilerGenerated]
			add
			{
				CertificateValidationCallback2 certificateValidationCallback = this.ClientCertValidation2;
				CertificateValidationCallback2 certificateValidationCallback2;
				do
				{
					certificateValidationCallback2 = certificateValidationCallback;
					CertificateValidationCallback2 value2 = (CertificateValidationCallback2)Delegate.Combine(certificateValidationCallback2, value);
					certificateValidationCallback = Interlocked.CompareExchange<CertificateValidationCallback2>(ref this.ClientCertValidation2, value2, certificateValidationCallback2);
				}
				while (certificateValidationCallback != certificateValidationCallback2);
			}
			[CompilerGenerated]
			remove
			{
				CertificateValidationCallback2 certificateValidationCallback = this.ClientCertValidation2;
				CertificateValidationCallback2 certificateValidationCallback2;
				do
				{
					certificateValidationCallback2 = certificateValidationCallback;
					CertificateValidationCallback2 value2 = (CertificateValidationCallback2)Delegate.Remove(certificateValidationCallback2, value);
					certificateValidationCallback = Interlocked.CompareExchange<CertificateValidationCallback2>(ref this.ClientCertValidation2, value2, certificateValidationCallback2);
				}
				while (certificateValidationCallback != certificateValidationCallback2);
			}
		}

		// Token: 0x0600030F RID: 783 RVA: 0x000112F9 File Offset: 0x0000F4F9
		public SslServerStream(Stream stream, System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate) : this(stream, serverCertificate, false, false, SecurityProtocolType.Default)
		{
		}

		// Token: 0x06000310 RID: 784 RVA: 0x0001130A File Offset: 0x0000F50A
		public SslServerStream(Stream stream, System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, bool clientCertificateRequired, bool ownsStream) : this(stream, serverCertificate, clientCertificateRequired, ownsStream, SecurityProtocolType.Default)
		{
		}

		// Token: 0x06000311 RID: 785 RVA: 0x0001131C File Offset: 0x0000F51C
		public SslServerStream(Stream stream, System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, bool clientCertificateRequired, bool requestClientCertificate, bool ownsStream) : this(stream, serverCertificate, clientCertificateRequired, requestClientCertificate, ownsStream, SecurityProtocolType.Default)
		{
		}

		// Token: 0x06000312 RID: 786 RVA: 0x00011330 File Offset: 0x0000F530
		public SslServerStream(Stream stream, System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, bool clientCertificateRequired, bool ownsStream, SecurityProtocolType securityProtocolType) : this(stream, serverCertificate, clientCertificateRequired, false, ownsStream, securityProtocolType)
		{
		}

		// Token: 0x06000313 RID: 787 RVA: 0x00011340 File Offset: 0x0000F540
		public SslServerStream(Stream stream, System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, bool clientCertificateRequired, bool requestClientCertificate, bool ownsStream, SecurityProtocolType securityProtocolType) : base(stream, ownsStream)
		{
			this.context = new ServerContext(this, securityProtocolType, serverCertificate, clientCertificateRequired, requestClientCertificate);
			this.protocol = new ServerRecordProtocol(this.innerStream, (ServerContext)this.context);
		}

		// Token: 0x06000314 RID: 788 RVA: 0x0001137C File Offset: 0x0000F57C
		~SslServerStream()
		{
			this.Dispose(false);
		}

		// Token: 0x06000315 RID: 789 RVA: 0x000113AC File Offset: 0x0000F5AC
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (disposing)
			{
				this.ClientCertValidation = null;
				this.PrivateKeySelection = null;
			}
		}

		// Token: 0x06000316 RID: 790 RVA: 0x000113C8 File Offset: 0x0000F5C8
		internal override IAsyncResult BeginNegotiateHandshake(AsyncCallback callback, object state)
		{
			if (this.context.HandshakeState != HandshakeState.None)
			{
				this.context.Clear();
			}
			this.context.SupportedCiphers = CipherSuiteFactory.GetSupportedCiphers(true, this.context.SecurityProtocol);
			this.context.HandshakeState = HandshakeState.Started;
			return this.protocol.BeginReceiveRecord(this.innerStream, callback, state);
		}

		// Token: 0x06000317 RID: 791 RVA: 0x00011428 File Offset: 0x0000F628
		internal override void EndNegotiateHandshake(IAsyncResult asyncResult)
		{
			this.protocol.EndReceiveRecord(asyncResult);
			if (this.context.LastHandshakeMsg != HandshakeType.ClientHello)
			{
				this.protocol.SendAlert(AlertDescription.UnexpectedMessage);
			}
			this.protocol.SendRecord(HandshakeType.ServerHello);
			this.protocol.SendRecord(HandshakeType.Certificate);
			if (((ServerContext)this.context).ClientCertificateRequired || ((ServerContext)this.context).RequestClientCertificate)
			{
				this.protocol.SendRecord(HandshakeType.CertificateRequest);
			}
			this.protocol.SendRecord(HandshakeType.ServerHelloDone);
			while (this.context.LastHandshakeMsg != HandshakeType.Finished)
			{
				byte[] array = this.protocol.ReceiveRecord(this.innerStream);
				if (array == null || array.Length == 0)
				{
					throw new TlsException(AlertDescription.HandshakeFailiure, "The client stopped the handshake.");
				}
			}
			this.protocol.SendChangeCipherSpec();
			this.protocol.SendRecord(HandshakeType.Finished);
			this.context.HandshakeState = HandshakeState.Finished;
			this.context.HandshakeMessages.Reset();
			this.context.ClearKeyInfo();
		}

		// Token: 0x06000318 RID: 792 RVA: 0x0001152A File Offset: 0x0000F72A
		internal override System.Security.Cryptography.X509Certificates.X509Certificate OnLocalCertificateSelection(System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCertificates, System.Security.Cryptography.X509Certificates.X509Certificate serverCertificate, string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection serverRequestedCertificates)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000319 RID: 793 RVA: 0x00011531 File Offset: 0x0000F731
		internal override bool OnRemoteCertificateValidation(System.Security.Cryptography.X509Certificates.X509Certificate certificate, int[] errors)
		{
			if (this.ClientCertValidation != null)
			{
				return this.ClientCertValidation(certificate, errors);
			}
			return errors != null && errors.Length == 0;
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x0600031A RID: 794 RVA: 0x00011553 File Offset: 0x0000F753
		internal override bool HaveRemoteValidation2Callback
		{
			get
			{
				return this.ClientCertValidation2 != null;
			}
		}

		// Token: 0x0600031B RID: 795 RVA: 0x00011560 File Offset: 0x0000F760
		internal override ValidationResult OnRemoteCertificateValidation2(Mono.Security.X509.X509CertificateCollection collection)
		{
			CertificateValidationCallback2 clientCertValidation = this.ClientCertValidation2;
			if (clientCertValidation != null)
			{
				return clientCertValidation(collection);
			}
			return null;
		}

		// Token: 0x0600031C RID: 796 RVA: 0x00011580 File Offset: 0x0000F780
		internal bool RaiseClientCertificateValidation(System.Security.Cryptography.X509Certificates.X509Certificate certificate, int[] certificateErrors)
		{
			return base.RaiseRemoteCertificateValidation(certificate, certificateErrors);
		}

		// Token: 0x0600031D RID: 797 RVA: 0x0001158A File Offset: 0x0000F78A
		internal override AsymmetricAlgorithm OnLocalPrivateKeySelection(System.Security.Cryptography.X509Certificates.X509Certificate certificate, string targetHost)
		{
			if (this.PrivateKeySelection != null)
			{
				return this.PrivateKeySelection(certificate, targetHost);
			}
			return null;
		}

		// Token: 0x0600031E RID: 798 RVA: 0x000115A3 File Offset: 0x0000F7A3
		internal AsymmetricAlgorithm RaisePrivateKeySelection(System.Security.Cryptography.X509Certificates.X509Certificate certificate, string targetHost)
		{
			return base.RaiseLocalPrivateKeySelection(certificate, targetHost);
		}

		// Token: 0x0400019F RID: 415
		[CompilerGenerated]
		private CertificateValidationCallback ClientCertValidation;

		// Token: 0x040001A0 RID: 416
		[CompilerGenerated]
		private PrivateKeySelectionCallback PrivateKeySelection;

		// Token: 0x040001A1 RID: 417
		[CompilerGenerated]
		private CertificateValidationCallback2 ClientCertValidation2;
	}
}
