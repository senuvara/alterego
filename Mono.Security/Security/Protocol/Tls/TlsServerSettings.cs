﻿using System;
using System.Security.Cryptography;
using Mono.Security.Cryptography;
using Mono.Security.Protocol.Tls.Handshake;
using Mono.Security.X509;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000051 RID: 81
	internal class TlsServerSettings
	{
		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x0600036D RID: 877 RVA: 0x00012C0E File Offset: 0x00010E0E
		// (set) Token: 0x0600036E RID: 878 RVA: 0x00012C16 File Offset: 0x00010E16
		public bool ServerKeyExchange
		{
			get
			{
				return this.serverKeyExchange;
			}
			set
			{
				this.serverKeyExchange = value;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x0600036F RID: 879 RVA: 0x00012C1F File Offset: 0x00010E1F
		// (set) Token: 0x06000370 RID: 880 RVA: 0x00012C27 File Offset: 0x00010E27
		public X509CertificateCollection Certificates
		{
			get
			{
				return this.certificates;
			}
			set
			{
				this.certificates = value;
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000371 RID: 881 RVA: 0x00012C30 File Offset: 0x00010E30
		public RSA CertificateRSA
		{
			get
			{
				return this.certificateRSA;
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000372 RID: 882 RVA: 0x00012C38 File Offset: 0x00010E38
		// (set) Token: 0x06000373 RID: 883 RVA: 0x00012C40 File Offset: 0x00010E40
		public RSAParameters RsaParameters
		{
			get
			{
				return this.rsaParameters;
			}
			set
			{
				this.rsaParameters = value;
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000374 RID: 884 RVA: 0x00012C49 File Offset: 0x00010E49
		// (set) Token: 0x06000375 RID: 885 RVA: 0x00012C51 File Offset: 0x00010E51
		public byte[] SignedParams
		{
			get
			{
				return this.signedParams;
			}
			set
			{
				this.signedParams = value;
			}
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000376 RID: 886 RVA: 0x00012C5A File Offset: 0x00010E5A
		// (set) Token: 0x06000377 RID: 887 RVA: 0x00012C62 File Offset: 0x00010E62
		public bool CertificateRequest
		{
			get
			{
				return this.certificateRequest;
			}
			set
			{
				this.certificateRequest = value;
			}
		}

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000378 RID: 888 RVA: 0x00012C6B File Offset: 0x00010E6B
		// (set) Token: 0x06000379 RID: 889 RVA: 0x00012C73 File Offset: 0x00010E73
		public ClientCertificateType[] CertificateTypes
		{
			get
			{
				return this.certificateTypes;
			}
			set
			{
				this.certificateTypes = value;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x0600037A RID: 890 RVA: 0x00012C7C File Offset: 0x00010E7C
		// (set) Token: 0x0600037B RID: 891 RVA: 0x00012C84 File Offset: 0x00010E84
		public string[] DistinguisedNames
		{
			get
			{
				return this.distinguisedNames;
			}
			set
			{
				this.distinguisedNames = value;
			}
		}

		// Token: 0x0600037C RID: 892 RVA: 0x00012C8D File Offset: 0x00010E8D
		public TlsServerSettings()
		{
		}

		// Token: 0x0600037D RID: 893 RVA: 0x00012C98 File Offset: 0x00010E98
		public void UpdateCertificateRSA()
		{
			if (this.certificates == null || this.certificates.Count == 0)
			{
				this.certificateRSA = null;
				return;
			}
			this.certificateRSA = new RSAManaged(this.certificates[0].RSA.KeySize);
			this.certificateRSA.ImportParameters(this.certificates[0].RSA.ExportParameters(false));
		}

		// Token: 0x040001B8 RID: 440
		private X509CertificateCollection certificates;

		// Token: 0x040001B9 RID: 441
		private RSA certificateRSA;

		// Token: 0x040001BA RID: 442
		private RSAParameters rsaParameters;

		// Token: 0x040001BB RID: 443
		private byte[] signedParams;

		// Token: 0x040001BC RID: 444
		private string[] distinguisedNames;

		// Token: 0x040001BD RID: 445
		private bool serverKeyExchange;

		// Token: 0x040001BE RID: 446
		private bool certificateRequest;

		// Token: 0x040001BF RID: 447
		private ClientCertificateType[] certificateTypes;
	}
}
