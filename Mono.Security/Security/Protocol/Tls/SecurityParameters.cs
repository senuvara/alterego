﻿using System;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000041 RID: 65
	internal class SecurityParameters
	{
		// Token: 0x060002B1 RID: 689 RVA: 0x0000FADE File Offset: 0x0000DCDE
		public SecurityParameters()
		{
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x060002B2 RID: 690 RVA: 0x0000FAE6 File Offset: 0x0000DCE6
		// (set) Token: 0x060002B3 RID: 691 RVA: 0x0000FAEE File Offset: 0x0000DCEE
		public CipherSuite Cipher
		{
			get
			{
				return this.cipher;
			}
			set
			{
				this.cipher = value;
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x060002B4 RID: 692 RVA: 0x0000FAF7 File Offset: 0x0000DCF7
		// (set) Token: 0x060002B5 RID: 693 RVA: 0x0000FAFF File Offset: 0x0000DCFF
		public byte[] ClientWriteMAC
		{
			get
			{
				return this.clientWriteMAC;
			}
			set
			{
				this.clientWriteMAC = value;
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x060002B6 RID: 694 RVA: 0x0000FB08 File Offset: 0x0000DD08
		// (set) Token: 0x060002B7 RID: 695 RVA: 0x0000FB10 File Offset: 0x0000DD10
		public byte[] ServerWriteMAC
		{
			get
			{
				return this.serverWriteMAC;
			}
			set
			{
				this.serverWriteMAC = value;
			}
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x0000FB19 File Offset: 0x0000DD19
		public void Clear()
		{
			this.cipher = null;
		}

		// Token: 0x04000181 RID: 385
		private CipherSuite cipher;

		// Token: 0x04000182 RID: 386
		private byte[] clientWriteMAC;

		// Token: 0x04000183 RID: 387
		private byte[] serverWriteMAC;
	}
}
