﻿using System;
using System.Security.Cryptography;
using Mono.Security.Cryptography;

namespace Mono.Security.Protocol.Tls.Handshake.Client
{
	// Token: 0x02000063 RID: 99
	internal class TlsClientFinished : HandshakeMessage
	{
		// Token: 0x060003E1 RID: 993 RVA: 0x00014590 File Offset: 0x00012790
		public TlsClientFinished(Context context) : base(context, HandshakeType.Finished)
		{
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x0001459B File Offset: 0x0001279B
		public override void Update()
		{
			base.Update();
			base.Reset();
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x000145AC File Offset: 0x000127AC
		protected override void ProcessAsSsl3()
		{
			HashAlgorithm hashAlgorithm = new SslHandshakeHash(base.Context.MasterSecret);
			byte[] array = base.Context.HandshakeMessages.ToArray();
			hashAlgorithm.TransformBlock(array, 0, array.Length, array, 0);
			hashAlgorithm.TransformBlock(TlsClientFinished.Ssl3Marker, 0, TlsClientFinished.Ssl3Marker.Length, TlsClientFinished.Ssl3Marker, 0);
			hashAlgorithm.TransformFinalBlock(CipherSuite.EmptyArray, 0, 0);
			base.Write(hashAlgorithm.Hash);
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x00014620 File Offset: 0x00012820
		protected override void ProcessAsTls1()
		{
			HashAlgorithm hashAlgorithm = new MD5SHA1();
			byte[] array = base.Context.HandshakeMessages.ToArray();
			byte[] data = hashAlgorithm.ComputeHash(array, 0, array.Length);
			base.Write(base.Context.Write.Cipher.PRF(base.Context.MasterSecret, "client finished", data, 12));
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x0001467C File Offset: 0x0001287C
		// Note: this type is marked as 'beforefieldinit'.
		static TlsClientFinished()
		{
		}

		// Token: 0x040001E5 RID: 485
		private static byte[] Ssl3Marker = new byte[]
		{
			67,
			76,
			78,
			84
		};
	}
}
