﻿using System;
using System.Security.Cryptography;
using Mono.Security.Cryptography;

namespace Mono.Security.Protocol.Tls.Handshake.Client
{
	// Token: 0x02000068 RID: 104
	internal class TlsServerFinished : HandshakeMessage
	{
		// Token: 0x060003FD RID: 1021 RVA: 0x000150AC File Offset: 0x000132AC
		public TlsServerFinished(Context context, byte[] buffer) : base(context, HandshakeType.Finished, buffer)
		{
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x000150B8 File Offset: 0x000132B8
		public override void Update()
		{
			base.Update();
			base.Context.HandshakeState = HandshakeState.Finished;
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x000150CC File Offset: 0x000132CC
		protected override void ProcessAsSsl3()
		{
			SslHandshakeHash sslHandshakeHash = new SslHandshakeHash(base.Context.MasterSecret);
			byte[] array = base.Context.HandshakeMessages.ToArray();
			sslHandshakeHash.TransformBlock(array, 0, array.Length, array, 0);
			sslHandshakeHash.TransformBlock(TlsServerFinished.Ssl3Marker, 0, TlsServerFinished.Ssl3Marker.Length, TlsServerFinished.Ssl3Marker, 0);
			sslHandshakeHash.TransformFinalBlock(CipherSuite.EmptyArray, 0, 0);
			byte[] buffer = base.ReadBytes((int)this.Length);
			if (!HandshakeMessage.Compare(sslHandshakeHash.Hash, buffer))
			{
				throw new TlsException(AlertDescription.InsuficientSecurity, "Invalid ServerFinished message received.");
			}
		}

		// Token: 0x06000400 RID: 1024 RVA: 0x00015158 File Offset: 0x00013358
		protected override void ProcessAsTls1()
		{
			byte[] buffer = base.ReadBytes((int)this.Length);
			HashAlgorithm hashAlgorithm = new MD5SHA1();
			byte[] array = base.Context.HandshakeMessages.ToArray();
			byte[] data = hashAlgorithm.ComputeHash(array, 0, array.Length);
			if (!HandshakeMessage.Compare(base.Context.Current.Cipher.PRF(base.Context.MasterSecret, "server finished", data, 12), buffer))
			{
				throw new TlsException("Invalid ServerFinished message received.");
			}
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x000151CF File Offset: 0x000133CF
		// Note: this type is marked as 'beforefieldinit'.
		static TlsServerFinished()
		{
		}

		// Token: 0x040001EA RID: 490
		private static byte[] Ssl3Marker = new byte[]
		{
			83,
			82,
			86,
			82
		};
	}
}
