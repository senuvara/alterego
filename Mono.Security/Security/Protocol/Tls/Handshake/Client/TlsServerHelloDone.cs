﻿using System;

namespace Mono.Security.Protocol.Tls.Handshake.Client
{
	// Token: 0x0200006A RID: 106
	internal class TlsServerHelloDone : HandshakeMessage
	{
		// Token: 0x06000407 RID: 1031 RVA: 0x0001543A File Offset: 0x0001363A
		public TlsServerHelloDone(Context context, byte[] buffer) : base(context, HandshakeType.ServerHelloDone, buffer)
		{
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x00015446 File Offset: 0x00013646
		protected override void ProcessAsSsl3()
		{
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x00015448 File Offset: 0x00013648
		protected override void ProcessAsTls1()
		{
		}
	}
}
