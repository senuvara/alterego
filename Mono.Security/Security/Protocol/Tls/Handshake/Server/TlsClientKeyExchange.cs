﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Mono.Security.Protocol.Tls.Handshake.Server
{
	// Token: 0x0200005A RID: 90
	internal class TlsClientKeyExchange : HandshakeMessage
	{
		// Token: 0x060003BA RID: 954 RVA: 0x00013AA3 File Offset: 0x00011CA3
		public TlsClientKeyExchange(Context context, byte[] buffer) : base(context, HandshakeType.ClientKeyExchange, buffer)
		{
		}

		// Token: 0x060003BB RID: 955 RVA: 0x00013AB0 File Offset: 0x00011CB0
		protected override void ProcessAsSsl3()
		{
			ServerContext serverContext = (ServerContext)base.Context;
			AsymmetricAlgorithm asymmetricAlgorithm = serverContext.SslStream.RaisePrivateKeySelection(new X509Certificate(serverContext.ServerSettings.Certificates[0].RawData), null);
			if (asymmetricAlgorithm == null)
			{
				throw new TlsException(AlertDescription.UserCancelled, "Server certificate Private Key unavailable.");
			}
			byte[] rgb = base.ReadBytes((int)this.Length);
			byte[] preMasterSecret = new RSAPKCS1KeyExchangeDeformatter(asymmetricAlgorithm).DecryptKeyExchange(rgb);
			base.Context.Negotiating.Cipher.ComputeMasterSecret(preMasterSecret);
			base.Context.Negotiating.Cipher.ComputeKeys();
			base.Context.Negotiating.Cipher.InitializeCipher();
		}

		// Token: 0x060003BC RID: 956 RVA: 0x00013B5C File Offset: 0x00011D5C
		protected override void ProcessAsTls1()
		{
			ServerContext serverContext = (ServerContext)base.Context;
			AsymmetricAlgorithm asymmetricAlgorithm = serverContext.SslStream.RaisePrivateKeySelection(new X509Certificate(serverContext.ServerSettings.Certificates[0].RawData), null);
			if (asymmetricAlgorithm == null)
			{
				throw new TlsException(AlertDescription.UserCancelled, "Server certificate Private Key unavailable.");
			}
			byte[] rgb = base.ReadBytes((int)base.ReadInt16());
			byte[] preMasterSecret = new RSAPKCS1KeyExchangeDeformatter(asymmetricAlgorithm).DecryptKeyExchange(rgb);
			base.Context.Negotiating.Cipher.ComputeMasterSecret(preMasterSecret);
			base.Context.Negotiating.Cipher.ComputeKeys();
			base.Context.Negotiating.Cipher.InitializeCipher();
		}
	}
}
