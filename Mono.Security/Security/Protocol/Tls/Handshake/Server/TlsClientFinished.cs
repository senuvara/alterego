﻿using System;
using System.Security.Cryptography;
using Mono.Security.Cryptography;

namespace Mono.Security.Protocol.Tls.Handshake.Server
{
	// Token: 0x02000058 RID: 88
	internal class TlsClientFinished : HandshakeMessage
	{
		// Token: 0x060003B0 RID: 944 RVA: 0x00013790 File Offset: 0x00011990
		public TlsClientFinished(Context context, byte[] buffer) : base(context, HandshakeType.Finished, buffer)
		{
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x0001379C File Offset: 0x0001199C
		protected override void ProcessAsSsl3()
		{
			HashAlgorithm hashAlgorithm = new SslHandshakeHash(base.Context.MasterSecret);
			TlsStream tlsStream = new TlsStream();
			tlsStream.Write(base.Context.HandshakeMessages.ToArray());
			tlsStream.Write(1129074260);
			hashAlgorithm.TransformFinalBlock(tlsStream.ToArray(), 0, (int)tlsStream.Length);
			tlsStream.Reset();
			byte[] buffer = base.ReadBytes((int)this.Length);
			byte[] hash = hashAlgorithm.Hash;
			if (!HandshakeMessage.Compare(buffer, hash))
			{
				throw new TlsException(AlertDescription.DecryptError, "Decrypt error.");
			}
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x00013828 File Offset: 0x00011A28
		protected override void ProcessAsTls1()
		{
			byte[] buffer = base.ReadBytes((int)this.Length);
			HashAlgorithm hashAlgorithm = new MD5SHA1();
			byte[] array = base.Context.HandshakeMessages.ToArray();
			byte[] data = hashAlgorithm.ComputeHash(array, 0, array.Length);
			byte[] buffer2 = base.Context.Current.Cipher.PRF(base.Context.MasterSecret, "client finished", data, 12);
			if (!HandshakeMessage.Compare(buffer, buffer2))
			{
				throw new TlsException(AlertDescription.DecryptError, "Decrypt error.");
			}
		}
	}
}
