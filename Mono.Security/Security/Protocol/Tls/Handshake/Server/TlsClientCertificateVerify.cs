﻿using System;
using Mono.Security.Cryptography;

namespace Mono.Security.Protocol.Tls.Handshake.Server
{
	// Token: 0x02000057 RID: 87
	internal class TlsClientCertificateVerify : HandshakeMessage
	{
		// Token: 0x060003AD RID: 941 RVA: 0x000136A4 File Offset: 0x000118A4
		public TlsClientCertificateVerify(Context context, byte[] buffer) : base(context, HandshakeType.CertificateVerify, buffer)
		{
		}

		// Token: 0x060003AE RID: 942 RVA: 0x000136B0 File Offset: 0x000118B0
		protected override void ProcessAsSsl3()
		{
			ServerContext serverContext = (ServerContext)base.Context;
			int count = (int)base.ReadInt16();
			byte[] rgbSignature = base.ReadBytes(count);
			SslHandshakeHash sslHandshakeHash = new SslHandshakeHash(serverContext.MasterSecret);
			sslHandshakeHash.TransformFinalBlock(serverContext.HandshakeMessages.ToArray(), 0, (int)serverContext.HandshakeMessages.Length);
			if (!sslHandshakeHash.VerifySignature(serverContext.ClientSettings.CertificateRSA, rgbSignature))
			{
				throw new TlsException(AlertDescription.HandshakeFailiure, "Handshake Failure.");
			}
		}

		// Token: 0x060003AF RID: 943 RVA: 0x00013724 File Offset: 0x00011924
		protected override void ProcessAsTls1()
		{
			ServerContext serverContext = (ServerContext)base.Context;
			int count = (int)base.ReadInt16();
			byte[] rgbSignature = base.ReadBytes(count);
			MD5SHA1 md5SHA = new MD5SHA1();
			md5SHA.ComputeHash(serverContext.HandshakeMessages.ToArray(), 0, (int)serverContext.HandshakeMessages.Length);
			if (!md5SHA.VerifySignature(serverContext.ClientSettings.CertificateRSA, rgbSignature))
			{
				throw new TlsException(AlertDescription.HandshakeFailiure, "Handshake Failure.");
			}
		}
	}
}
