﻿using System;

namespace Mono.Security.Protocol.Tls.Handshake.Server
{
	// Token: 0x0200005F RID: 95
	internal class TlsServerHelloDone : HandshakeMessage
	{
		// Token: 0x060003CB RID: 971 RVA: 0x00013FAA File Offset: 0x000121AA
		public TlsServerHelloDone(Context context) : base(context, HandshakeType.ServerHelloDone)
		{
		}

		// Token: 0x060003CC RID: 972 RVA: 0x00013FB5 File Offset: 0x000121B5
		protected override void ProcessAsSsl3()
		{
		}

		// Token: 0x060003CD RID: 973 RVA: 0x00013FB7 File Offset: 0x000121B7
		protected override void ProcessAsTls1()
		{
		}
	}
}
