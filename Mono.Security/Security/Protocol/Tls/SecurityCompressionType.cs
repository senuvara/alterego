﻿using System;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000040 RID: 64
	public enum SecurityCompressionType
	{
		// Token: 0x0400017F RID: 383
		None,
		// Token: 0x04000180 RID: 384
		Zlib
	}
}
