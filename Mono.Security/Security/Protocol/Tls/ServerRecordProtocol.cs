﻿using System;
using System.Globalization;
using System.IO;
using Mono.Security.Protocol.Tls.Handshake;
using Mono.Security.Protocol.Tls.Handshake.Server;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000044 RID: 68
	internal class ServerRecordProtocol : RecordProtocol
	{
		// Token: 0x060002BD RID: 701 RVA: 0x0000FCB4 File Offset: 0x0000DEB4
		public ServerRecordProtocol(Stream innerStream, ServerContext context) : base(innerStream, context)
		{
		}

		// Token: 0x060002BE RID: 702 RVA: 0x0000FCBE File Offset: 0x0000DEBE
		public override HandshakeMessage GetMessage(HandshakeType type)
		{
			return this.createServerHandshakeMessage(type);
		}

		// Token: 0x060002BF RID: 703 RVA: 0x0000FCC8 File Offset: 0x0000DEC8
		protected override void ProcessHandshakeMessage(TlsStream handMsg)
		{
			HandshakeType handshakeType = (HandshakeType)handMsg.ReadByte();
			int num = handMsg.ReadInt24();
			byte[] array = new byte[num];
			handMsg.Read(array, 0, num);
			HandshakeMessage handshakeMessage = this.createClientHandshakeMessage(handshakeType, array);
			handshakeMessage.Process();
			base.Context.LastHandshakeMsg = handshakeType;
			if (handshakeMessage != null)
			{
				handshakeMessage.Update();
				base.Context.HandshakeMessages.WriteByte((byte)handshakeType);
				base.Context.HandshakeMessages.WriteInt24(num);
				base.Context.HandshakeMessages.Write(array, 0, array.Length);
			}
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x0000FD54 File Offset: 0x0000DF54
		private HandshakeMessage createClientHandshakeMessage(HandshakeType type, byte[] buffer)
		{
			HandshakeType lastHandshakeMsg = this.context.LastHandshakeMsg;
			if (type <= HandshakeType.Certificate)
			{
				if (type == HandshakeType.ClientHello)
				{
					return new TlsClientHello(this.context, buffer);
				}
				if (type == HandshakeType.Certificate)
				{
					if (lastHandshakeMsg == HandshakeType.ClientHello)
					{
						this.cert = new TlsClientCertificate(this.context, buffer);
						return this.cert;
					}
					goto IL_106;
				}
			}
			else if (type != HandshakeType.CertificateVerify)
			{
				if (type != HandshakeType.ClientKeyExchange)
				{
					if (type == HandshakeType.Finished)
					{
						if (((this.cert != null && this.cert.HasCertificate) ? (lastHandshakeMsg == HandshakeType.CertificateVerify) : (lastHandshakeMsg == HandshakeType.ClientKeyExchange)) && this.context.ChangeCipherSpecDone)
						{
							this.context.ChangeCipherSpecDone = false;
							return new TlsClientFinished(this.context, buffer);
						}
						goto IL_106;
					}
				}
				else
				{
					if (lastHandshakeMsg == HandshakeType.ClientHello || lastHandshakeMsg == HandshakeType.Certificate)
					{
						return new TlsClientKeyExchange(this.context, buffer);
					}
					goto IL_106;
				}
			}
			else
			{
				if (lastHandshakeMsg == HandshakeType.ClientKeyExchange && this.cert != null)
				{
					return new TlsClientCertificateVerify(this.context, buffer);
				}
				goto IL_106;
			}
			throw new TlsException(AlertDescription.UnexpectedMessage, string.Format(CultureInfo.CurrentUICulture, "Unknown server handshake message received ({0})", type.ToString()));
			IL_106:
			throw new TlsException(AlertDescription.HandshakeFailiure, string.Format("Protocol error, unexpected protocol transition from {0} to {1}", lastHandshakeMsg, type));
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x0000FE84 File Offset: 0x0000E084
		private HandshakeMessage createServerHandshakeMessage(HandshakeType type)
		{
			if (type <= HandshakeType.ServerHello)
			{
				if (type == HandshakeType.HelloRequest)
				{
					this.SendRecord(HandshakeType.ClientHello);
					return null;
				}
				if (type == HandshakeType.ServerHello)
				{
					return new TlsServerHello(this.context);
				}
			}
			else
			{
				switch (type)
				{
				case HandshakeType.Certificate:
					return new TlsServerCertificate(this.context);
				case HandshakeType.ServerKeyExchange:
					return new TlsServerKeyExchange(this.context);
				case HandshakeType.CertificateRequest:
					return new TlsServerCertificateRequest(this.context);
				case HandshakeType.ServerHelloDone:
					return new TlsServerHelloDone(this.context);
				default:
					if (type == HandshakeType.Finished)
					{
						return new TlsServerFinished(this.context);
					}
					break;
				}
			}
			throw new InvalidOperationException("Unknown server handshake message type: " + type.ToString());
		}

		// Token: 0x0400018E RID: 398
		private TlsClientCertificate cert;
	}
}
