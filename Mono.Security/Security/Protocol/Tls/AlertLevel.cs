﻿using System;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x0200002B RID: 43
	[Serializable]
	internal enum AlertLevel : byte
	{
		// Token: 0x040000F8 RID: 248
		Warning = 1,
		// Token: 0x040000F9 RID: 249
		Fatal
	}
}
