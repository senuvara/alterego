﻿using System;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000031 RID: 49
	internal static class CipherSuiteFactory
	{
		// Token: 0x0600021C RID: 540 RVA: 0x0000DDB0 File Offset: 0x0000BFB0
		public static CipherSuiteCollection GetSupportedCiphers(bool server, SecurityProtocolType protocol)
		{
			if (protocol <= SecurityProtocolType.Ssl2)
			{
				if (protocol != SecurityProtocolType.Default)
				{
					if (protocol != SecurityProtocolType.Ssl2)
					{
						goto IL_2D;
					}
					goto IL_2D;
				}
			}
			else
			{
				if (protocol == SecurityProtocolType.Ssl3)
				{
					return CipherSuiteFactory.GetSsl3SupportedCiphers();
				}
				if (protocol != SecurityProtocolType.Tls)
				{
					goto IL_2D;
				}
			}
			return CipherSuiteFactory.GetTls1SupportedCiphers();
			IL_2D:
			throw new NotSupportedException("Unsupported security protocol type");
		}

		// Token: 0x0600021D RID: 541 RVA: 0x0000DDEC File Offset: 0x0000BFEC
		private static CipherSuiteCollection GetTls1SupportedCiphers()
		{
			return new CipherSuiteCollection(SecurityProtocolType.Tls)
			{
				{
					53,
					"TLS_RSA_WITH_AES_256_CBC_SHA",
					CipherAlgorithmType.Rijndael,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					true,
					32,
					32,
					256,
					16,
					16
				},
				{
					47,
					"TLS_RSA_WITH_AES_128_CBC_SHA",
					CipherAlgorithmType.Rijndael,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					true,
					16,
					16,
					128,
					16,
					16
				},
				{
					10,
					"TLS_RSA_WITH_3DES_EDE_CBC_SHA",
					CipherAlgorithmType.TripleDes,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					true,
					24,
					24,
					168,
					8,
					8
				},
				{
					5,
					"TLS_RSA_WITH_RC4_128_SHA",
					CipherAlgorithmType.Rc4,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					false,
					16,
					16,
					128,
					0,
					0
				},
				{
					4,
					"TLS_RSA_WITH_RC4_128_MD5",
					CipherAlgorithmType.Rc4,
					HashAlgorithmType.Md5,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					false,
					16,
					16,
					128,
					0,
					0
				},
				{
					9,
					"TLS_RSA_WITH_DES_CBC_SHA",
					CipherAlgorithmType.Des,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					true,
					8,
					8,
					56,
					8,
					8
				}
			};
		}

		// Token: 0x0600021E RID: 542 RVA: 0x0000DEB0 File Offset: 0x0000C0B0
		private static CipherSuiteCollection GetSsl3SupportedCiphers()
		{
			return new CipherSuiteCollection(SecurityProtocolType.Ssl3)
			{
				{
					53,
					"SSL_RSA_WITH_AES_256_CBC_SHA",
					CipherAlgorithmType.Rijndael,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					true,
					32,
					32,
					256,
					16,
					16
				},
				{
					47,
					"SSL_RSA_WITH_AES_128_CBC_SHA",
					CipherAlgorithmType.Rijndael,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					true,
					16,
					16,
					128,
					16,
					16
				},
				{
					10,
					"SSL_RSA_WITH_3DES_EDE_CBC_SHA",
					CipherAlgorithmType.TripleDes,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					true,
					24,
					24,
					168,
					8,
					8
				},
				{
					5,
					"SSL_RSA_WITH_RC4_128_SHA",
					CipherAlgorithmType.Rc4,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					false,
					16,
					16,
					128,
					0,
					0
				},
				{
					4,
					"SSL_RSA_WITH_RC4_128_MD5",
					CipherAlgorithmType.Rc4,
					HashAlgorithmType.Md5,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					false,
					16,
					16,
					128,
					0,
					0
				},
				{
					9,
					"SSL_RSA_WITH_DES_CBC_SHA",
					CipherAlgorithmType.Des,
					HashAlgorithmType.Sha1,
					ExchangeAlgorithmType.RsaKeyX,
					false,
					true,
					8,
					8,
					56,
					8,
					8
				}
			};
		}
	}
}
