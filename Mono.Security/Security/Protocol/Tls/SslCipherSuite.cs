﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000045 RID: 69
	internal class SslCipherSuite : CipherSuite
	{
		// Token: 0x060002C2 RID: 706 RVA: 0x0000FF2C File Offset: 0x0000E12C
		public SslCipherSuite(short code, string name, CipherAlgorithmType cipherAlgorithmType, HashAlgorithmType hashAlgorithmType, ExchangeAlgorithmType exchangeAlgorithmType, bool exportable, bool blockMode, byte keyMaterialSize, byte expandedKeyMaterialSize, short effectiveKeyBytes, byte ivSize, byte blockSize) : base(code, name, cipherAlgorithmType, hashAlgorithmType, exchangeAlgorithmType, exportable, blockMode, keyMaterialSize, expandedKeyMaterialSize, effectiveKeyBytes, ivSize, blockSize)
		{
			int num = (hashAlgorithmType == HashAlgorithmType.Md5) ? 48 : 40;
			this.pad1 = new byte[num];
			this.pad2 = new byte[num];
			for (int i = 0; i < num; i++)
			{
				this.pad1[i] = 54;
				this.pad2[i] = 92;
			}
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x0000FF98 File Offset: 0x0000E198
		public override byte[] ComputeServerRecordMAC(ContentType contentType, byte[] fragment)
		{
			HashAlgorithm hashAlgorithm = base.CreateHashAlgorithm();
			byte[] serverWriteMAC = base.Context.Read.ServerWriteMAC;
			hashAlgorithm.TransformBlock(serverWriteMAC, 0, serverWriteMAC.Length, serverWriteMAC, 0);
			hashAlgorithm.TransformBlock(this.pad1, 0, this.pad1.Length, this.pad1, 0);
			if (this.header == null)
			{
				this.header = new byte[11];
			}
			ulong value = (base.Context is ClientContext) ? base.Context.ReadSequenceNumber : base.Context.WriteSequenceNumber;
			base.Write(this.header, 0, value);
			this.header[8] = (byte)contentType;
			base.Write(this.header, 9, (short)fragment.Length);
			hashAlgorithm.TransformBlock(this.header, 0, this.header.Length, this.header, 0);
			hashAlgorithm.TransformBlock(fragment, 0, fragment.Length, fragment, 0);
			hashAlgorithm.TransformFinalBlock(CipherSuite.EmptyArray, 0, 0);
			byte[] hash = hashAlgorithm.Hash;
			hashAlgorithm.Initialize();
			hashAlgorithm.TransformBlock(serverWriteMAC, 0, serverWriteMAC.Length, serverWriteMAC, 0);
			hashAlgorithm.TransformBlock(this.pad2, 0, this.pad2.Length, this.pad2, 0);
			hashAlgorithm.TransformBlock(hash, 0, hash.Length, hash, 0);
			hashAlgorithm.TransformFinalBlock(CipherSuite.EmptyArray, 0, 0);
			return hashAlgorithm.Hash;
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x000100E0 File Offset: 0x0000E2E0
		public override byte[] ComputeClientRecordMAC(ContentType contentType, byte[] fragment)
		{
			HashAlgorithm hashAlgorithm = base.CreateHashAlgorithm();
			byte[] clientWriteMAC = base.Context.Current.ClientWriteMAC;
			hashAlgorithm.TransformBlock(clientWriteMAC, 0, clientWriteMAC.Length, clientWriteMAC, 0);
			hashAlgorithm.TransformBlock(this.pad1, 0, this.pad1.Length, this.pad1, 0);
			if (this.header == null)
			{
				this.header = new byte[11];
			}
			ulong value = (base.Context is ClientContext) ? base.Context.WriteSequenceNumber : base.Context.ReadSequenceNumber;
			base.Write(this.header, 0, value);
			this.header[8] = (byte)contentType;
			base.Write(this.header, 9, (short)fragment.Length);
			hashAlgorithm.TransformBlock(this.header, 0, this.header.Length, this.header, 0);
			hashAlgorithm.TransformBlock(fragment, 0, fragment.Length, fragment, 0);
			hashAlgorithm.TransformFinalBlock(CipherSuite.EmptyArray, 0, 0);
			byte[] hash = hashAlgorithm.Hash;
			hashAlgorithm.Initialize();
			hashAlgorithm.TransformBlock(clientWriteMAC, 0, clientWriteMAC.Length, clientWriteMAC, 0);
			hashAlgorithm.TransformBlock(this.pad2, 0, this.pad2.Length, this.pad2, 0);
			hashAlgorithm.TransformBlock(hash, 0, hash.Length, hash, 0);
			hashAlgorithm.TransformFinalBlock(CipherSuite.EmptyArray, 0, 0);
			return hashAlgorithm.Hash;
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x00010228 File Offset: 0x0000E428
		public override void ComputeMasterSecret(byte[] preMasterSecret)
		{
			TlsStream tlsStream = new TlsStream();
			tlsStream.Write(this.prf(preMasterSecret, "A", base.Context.RandomCS));
			tlsStream.Write(this.prf(preMasterSecret, "BB", base.Context.RandomCS));
			tlsStream.Write(this.prf(preMasterSecret, "CCC", base.Context.RandomCS));
			base.Context.MasterSecret = tlsStream.ToArray();
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x000102A4 File Offset: 0x0000E4A4
		public override void ComputeKeys()
		{
			TlsStream tlsStream = new TlsStream();
			char c = 'A';
			int num = 1;
			while (tlsStream.Length < (long)base.KeyBlockSize)
			{
				string text = string.Empty;
				for (int i = 0; i < num; i++)
				{
					text += c.ToString();
				}
				byte[] array = this.prf(base.Context.MasterSecret, text.ToString(), base.Context.RandomSC);
				int count = (tlsStream.Length + (long)array.Length > (long)base.KeyBlockSize) ? (base.KeyBlockSize - (int)tlsStream.Length) : array.Length;
				tlsStream.Write(array, 0, count);
				c += '\u0001';
				num++;
			}
			TlsStream tlsStream2 = new TlsStream(tlsStream.ToArray());
			base.Context.Negotiating.ClientWriteMAC = tlsStream2.ReadBytes(base.HashSize);
			base.Context.Negotiating.ServerWriteMAC = tlsStream2.ReadBytes(base.HashSize);
			base.Context.ClientWriteKey = tlsStream2.ReadBytes((int)base.KeyMaterialSize);
			base.Context.ServerWriteKey = tlsStream2.ReadBytes((int)base.KeyMaterialSize);
			if (base.IvSize != 0)
			{
				base.Context.ClientWriteIV = tlsStream2.ReadBytes((int)base.IvSize);
				base.Context.ServerWriteIV = tlsStream2.ReadBytes((int)base.IvSize);
			}
			else
			{
				base.Context.ClientWriteIV = CipherSuite.EmptyArray;
				base.Context.ServerWriteIV = CipherSuite.EmptyArray;
			}
			ClientSessionCache.SetContextInCache(base.Context);
			tlsStream2.Reset();
			tlsStream.Reset();
		}

		// Token: 0x060002C7 RID: 711 RVA: 0x00010440 File Offset: 0x0000E640
		private byte[] prf(byte[] secret, string label, byte[] random)
		{
			HashAlgorithm hashAlgorithm = MD5.Create();
			HashAlgorithm hashAlgorithm2 = SHA1.Create();
			TlsStream tlsStream = new TlsStream();
			tlsStream.Write(Encoding.ASCII.GetBytes(label));
			tlsStream.Write(secret);
			tlsStream.Write(random);
			byte[] buffer = hashAlgorithm2.ComputeHash(tlsStream.ToArray(), 0, (int)tlsStream.Length);
			tlsStream.Reset();
			tlsStream.Write(secret);
			tlsStream.Write(buffer);
			byte[] result = hashAlgorithm.ComputeHash(tlsStream.ToArray(), 0, (int)tlsStream.Length);
			tlsStream.Reset();
			return result;
		}

		// Token: 0x0400018F RID: 399
		private byte[] pad1;

		// Token: 0x04000190 RID: 400
		private byte[] pad2;

		// Token: 0x04000191 RID: 401
		private const int MacHeaderLength = 11;

		// Token: 0x04000192 RID: 402
		private byte[] header;
	}
}
