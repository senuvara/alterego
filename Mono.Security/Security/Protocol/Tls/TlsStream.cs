﻿using System;
using System.IO;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000052 RID: 82
	internal class TlsStream : Stream
	{
		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x0600037E RID: 894 RVA: 0x00012D05 File Offset: 0x00010F05
		public bool EOF
		{
			get
			{
				return this.Position >= this.Length;
			}
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x0600037F RID: 895 RVA: 0x00012D18 File Offset: 0x00010F18
		public override bool CanWrite
		{
			get
			{
				return this.canWrite;
			}
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000380 RID: 896 RVA: 0x00012D20 File Offset: 0x00010F20
		public override bool CanRead
		{
			get
			{
				return this.canRead;
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000381 RID: 897 RVA: 0x00012D28 File Offset: 0x00010F28
		public override bool CanSeek
		{
			get
			{
				return this.buffer.CanSeek;
			}
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000382 RID: 898 RVA: 0x00012D35 File Offset: 0x00010F35
		// (set) Token: 0x06000383 RID: 899 RVA: 0x00012D42 File Offset: 0x00010F42
		public override long Position
		{
			get
			{
				return this.buffer.Position;
			}
			set
			{
				this.buffer.Position = value;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000384 RID: 900 RVA: 0x00012D50 File Offset: 0x00010F50
		public override long Length
		{
			get
			{
				return this.buffer.Length;
			}
		}

		// Token: 0x06000385 RID: 901 RVA: 0x00012D5D File Offset: 0x00010F5D
		public TlsStream()
		{
			this.buffer = new MemoryStream(0);
			this.canRead = false;
			this.canWrite = true;
		}

		// Token: 0x06000386 RID: 902 RVA: 0x00012D7F File Offset: 0x00010F7F
		public TlsStream(byte[] data)
		{
			if (data != null)
			{
				this.buffer = new MemoryStream(data);
			}
			else
			{
				this.buffer = new MemoryStream();
			}
			this.canRead = true;
			this.canWrite = false;
		}

		// Token: 0x06000387 RID: 903 RVA: 0x00012DB4 File Offset: 0x00010FB4
		private byte[] ReadSmallValue(int length)
		{
			if (length > 4)
			{
				throw new ArgumentException("8 bytes maximum");
			}
			if (this.temp == null)
			{
				this.temp = new byte[4];
			}
			if (this.Read(this.temp, 0, length) != length)
			{
				throw new TlsException(string.Format("buffer underrun", Array.Empty<object>()));
			}
			return this.temp;
		}

		// Token: 0x06000388 RID: 904 RVA: 0x00012E10 File Offset: 0x00011010
		public new byte ReadByte()
		{
			return this.ReadSmallValue(1)[0];
		}

		// Token: 0x06000389 RID: 905 RVA: 0x00012E1C File Offset: 0x0001101C
		public short ReadInt16()
		{
			byte[] array = this.ReadSmallValue(2);
			return (short)((int)array[0] << 8 | (int)array[1]);
		}

		// Token: 0x0600038A RID: 906 RVA: 0x00012E3C File Offset: 0x0001103C
		public int ReadInt24()
		{
			byte[] array = this.ReadSmallValue(3);
			return (int)array[0] << 16 | (int)array[1] << 8 | (int)array[2];
		}

		// Token: 0x0600038B RID: 907 RVA: 0x00012E64 File Offset: 0x00011064
		public int ReadInt32()
		{
			byte[] array = this.ReadSmallValue(4);
			return (int)array[0] << 24 | (int)array[1] << 16 | (int)array[2] << 8 | (int)array[3];
		}

		// Token: 0x0600038C RID: 908 RVA: 0x00012E90 File Offset: 0x00011090
		public byte[] ReadBytes(int count)
		{
			byte[] result = new byte[count];
			if (this.Read(result, 0, count) != count)
			{
				throw new TlsException("buffer underrun");
			}
			return result;
		}

		// Token: 0x0600038D RID: 909 RVA: 0x00012EBC File Offset: 0x000110BC
		public void Write(byte value)
		{
			if (this.temp == null)
			{
				this.temp = new byte[4];
			}
			this.temp[0] = value;
			this.Write(this.temp, 0, 1);
		}

		// Token: 0x0600038E RID: 910 RVA: 0x00012EE9 File Offset: 0x000110E9
		public void Write(short value)
		{
			if (this.temp == null)
			{
				this.temp = new byte[4];
			}
			this.temp[0] = (byte)(value >> 8);
			this.temp[1] = (byte)value;
			this.Write(this.temp, 0, 2);
		}

		// Token: 0x0600038F RID: 911 RVA: 0x00012F24 File Offset: 0x00011124
		public void WriteInt24(int value)
		{
			if (this.temp == null)
			{
				this.temp = new byte[4];
			}
			this.temp[0] = (byte)(value >> 16);
			this.temp[1] = (byte)(value >> 8);
			this.temp[2] = (byte)value;
			this.Write(this.temp, 0, 3);
		}

		// Token: 0x06000390 RID: 912 RVA: 0x00012F78 File Offset: 0x00011178
		public void Write(int value)
		{
			if (this.temp == null)
			{
				this.temp = new byte[4];
			}
			this.temp[0] = (byte)(value >> 24);
			this.temp[1] = (byte)(value >> 16);
			this.temp[2] = (byte)(value >> 8);
			this.temp[3] = (byte)value;
			this.Write(this.temp, 0, 4);
		}

		// Token: 0x06000391 RID: 913 RVA: 0x00012FD7 File Offset: 0x000111D7
		public void Write(ulong value)
		{
			this.Write((int)(value >> 32));
			this.Write((int)value);
		}

		// Token: 0x06000392 RID: 914 RVA: 0x00012FEC File Offset: 0x000111EC
		public void Write(byte[] buffer)
		{
			this.Write(buffer, 0, buffer.Length);
		}

		// Token: 0x06000393 RID: 915 RVA: 0x00012FF9 File Offset: 0x000111F9
		public void Reset()
		{
			this.buffer.SetLength(0L);
			this.buffer.Position = 0L;
		}

		// Token: 0x06000394 RID: 916 RVA: 0x00013015 File Offset: 0x00011215
		public byte[] ToArray()
		{
			return this.buffer.ToArray();
		}

		// Token: 0x06000395 RID: 917 RVA: 0x00013022 File Offset: 0x00011222
		public override void Flush()
		{
			this.buffer.Flush();
		}

		// Token: 0x06000396 RID: 918 RVA: 0x0001302F File Offset: 0x0001122F
		public override void SetLength(long length)
		{
			this.buffer.SetLength(length);
		}

		// Token: 0x06000397 RID: 919 RVA: 0x0001303D File Offset: 0x0001123D
		public override long Seek(long offset, SeekOrigin loc)
		{
			return this.buffer.Seek(offset, loc);
		}

		// Token: 0x06000398 RID: 920 RVA: 0x0001304C File Offset: 0x0001124C
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (this.canRead)
			{
				return this.buffer.Read(buffer, offset, count);
			}
			throw new InvalidOperationException("Read operations are not allowed by this stream");
		}

		// Token: 0x06000399 RID: 921 RVA: 0x0001306F File Offset: 0x0001126F
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this.canWrite)
			{
				this.buffer.Write(buffer, offset, count);
				return;
			}
			throw new InvalidOperationException("Write operations are not allowed by this stream");
		}

		// Token: 0x040001C0 RID: 448
		private bool canRead;

		// Token: 0x040001C1 RID: 449
		private bool canWrite;

		// Token: 0x040001C2 RID: 450
		private MemoryStream buffer;

		// Token: 0x040001C3 RID: 451
		private byte[] temp;

		// Token: 0x040001C4 RID: 452
		private const int temp_size = 4;
	}
}
