﻿using System;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x0200002D RID: 45
	internal class Alert
	{
		// Token: 0x17000080 RID: 128
		// (get) Token: 0x060001E7 RID: 487 RVA: 0x0000D1FE File Offset: 0x0000B3FE
		public AlertLevel Level
		{
			get
			{
				return this.level;
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x060001E8 RID: 488 RVA: 0x0000D206 File Offset: 0x0000B406
		public AlertDescription Description
		{
			get
			{
				return this.description;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x0000D20E File Offset: 0x0000B40E
		public string Message
		{
			get
			{
				return Alert.GetAlertMessage(this.description);
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x060001EA RID: 490 RVA: 0x0000D21B File Offset: 0x0000B41B
		public bool IsWarning
		{
			get
			{
				return this.level == AlertLevel.Warning;
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x060001EB RID: 491 RVA: 0x0000D229 File Offset: 0x0000B429
		public bool IsCloseNotify
		{
			get
			{
				return this.IsWarning && this.description == AlertDescription.CloseNotify;
			}
		}

		// Token: 0x060001EC RID: 492 RVA: 0x0000D23E File Offset: 0x0000B43E
		public Alert(AlertDescription description)
		{
			this.description = description;
			this.level = Alert.inferAlertLevel(description);
		}

		// Token: 0x060001ED RID: 493 RVA: 0x0000D259 File Offset: 0x0000B459
		public Alert(AlertLevel level, AlertDescription description)
		{
			this.level = level;
			this.description = description;
		}

		// Token: 0x060001EE RID: 494 RVA: 0x0000D270 File Offset: 0x0000B470
		private static AlertLevel inferAlertLevel(AlertDescription description)
		{
			if (description <= AlertDescription.DecryptError)
			{
				if (description <= AlertDescription.UnexpectedMessage)
				{
					if (description != AlertDescription.CloseNotify)
					{
						if (description != AlertDescription.UnexpectedMessage)
						{
							return AlertLevel.Fatal;
						}
						return AlertLevel.Fatal;
					}
				}
				else
				{
					if (description - AlertDescription.BadRecordMAC <= 2)
					{
						return AlertLevel.Fatal;
					}
					switch (description)
					{
					case AlertDescription.DecompressionFailiure:
					case (AlertDescription)31:
					case (AlertDescription)32:
					case (AlertDescription)33:
					case (AlertDescription)34:
					case (AlertDescription)35:
					case (AlertDescription)36:
					case (AlertDescription)37:
					case (AlertDescription)38:
					case (AlertDescription)39:
					case AlertDescription.HandshakeFailiure:
					case AlertDescription.NoCertificate:
					case AlertDescription.BadCertificate:
					case AlertDescription.UnsupportedCertificate:
					case AlertDescription.CertificateRevoked:
					case AlertDescription.CertificateExpired:
					case AlertDescription.CertificateUnknown:
					case AlertDescription.IlegalParameter:
					case AlertDescription.UnknownCA:
					case AlertDescription.AccessDenied:
					case AlertDescription.DecodeError:
					case AlertDescription.DecryptError:
						return AlertLevel.Fatal;
					default:
						return AlertLevel.Fatal;
					}
				}
			}
			else if (description <= AlertDescription.InsuficientSecurity)
			{
				if (description != AlertDescription.ExportRestriction && description - AlertDescription.ProtocolVersion > 1)
				{
					return AlertLevel.Fatal;
				}
				return AlertLevel.Fatal;
			}
			else if (description == AlertDescription.InternalError || (description != AlertDescription.UserCancelled && description != AlertDescription.NoRenegotiation))
			{
				return AlertLevel.Fatal;
			}
			return AlertLevel.Warning;
		}

		// Token: 0x060001EF RID: 495 RVA: 0x0000D32F File Offset: 0x0000B52F
		public static string GetAlertMessage(AlertDescription description)
		{
			return "The authentication or decryption has failed.";
		}

		// Token: 0x04000113 RID: 275
		private AlertLevel level;

		// Token: 0x04000114 RID: 276
		private AlertDescription description;
	}
}
