﻿using System;
using System.Runtime.Serialization;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000050 RID: 80
	[Serializable]
	internal sealed class TlsException : Exception
	{
		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x06000365 RID: 869 RVA: 0x00012B9F File Offset: 0x00010D9F
		public Alert Alert
		{
			get
			{
				return this.alert;
			}
		}

		// Token: 0x06000366 RID: 870 RVA: 0x00012BA7 File Offset: 0x00010DA7
		internal TlsException(string message) : base(message)
		{
		}

		// Token: 0x06000367 RID: 871 RVA: 0x00012BB0 File Offset: 0x00010DB0
		internal TlsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06000368 RID: 872 RVA: 0x00012BBA File Offset: 0x00010DBA
		internal TlsException(string message, Exception ex) : base(message, ex)
		{
		}

		// Token: 0x06000369 RID: 873 RVA: 0x00012BC4 File Offset: 0x00010DC4
		internal TlsException(AlertLevel level, AlertDescription description) : this(level, description, Alert.GetAlertMessage(description))
		{
		}

		// Token: 0x0600036A RID: 874 RVA: 0x00012BD4 File Offset: 0x00010DD4
		internal TlsException(AlertLevel level, AlertDescription description, string message) : base(message)
		{
			this.alert = new Alert(level, description);
		}

		// Token: 0x0600036B RID: 875 RVA: 0x00012BEA File Offset: 0x00010DEA
		internal TlsException(AlertDescription description) : this(description, Alert.GetAlertMessage(description))
		{
		}

		// Token: 0x0600036C RID: 876 RVA: 0x00012BF9 File Offset: 0x00010DF9
		internal TlsException(AlertDescription description, string message) : base(message)
		{
			this.alert = new Alert(description);
		}

		// Token: 0x040001B7 RID: 439
		private Alert alert;
	}
}
