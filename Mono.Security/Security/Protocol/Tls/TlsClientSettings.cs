﻿using System;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.Cryptography;
using Mono.Security.X509;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x0200004F RID: 79
	internal sealed class TlsClientSettings
	{
		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x0600035C RID: 860 RVA: 0x00012AE2 File Offset: 0x00010CE2
		// (set) Token: 0x0600035D RID: 861 RVA: 0x00012AEA File Offset: 0x00010CEA
		public string TargetHost
		{
			get
			{
				return this.targetHost;
			}
			set
			{
				this.targetHost = value;
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x0600035E RID: 862 RVA: 0x00012AF3 File Offset: 0x00010CF3
		// (set) Token: 0x0600035F RID: 863 RVA: 0x00012AFB File Offset: 0x00010CFB
		public System.Security.Cryptography.X509Certificates.X509CertificateCollection Certificates
		{
			get
			{
				return this.certificates;
			}
			set
			{
				this.certificates = value;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000360 RID: 864 RVA: 0x00012B04 File Offset: 0x00010D04
		// (set) Token: 0x06000361 RID: 865 RVA: 0x00012B0C File Offset: 0x00010D0C
		public System.Security.Cryptography.X509Certificates.X509Certificate ClientCertificate
		{
			get
			{
				return this.clientCertificate;
			}
			set
			{
				this.clientCertificate = value;
				this.UpdateCertificateRSA();
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000362 RID: 866 RVA: 0x00012B1B File Offset: 0x00010D1B
		public RSAManaged CertificateRSA
		{
			get
			{
				return this.certificateRSA;
			}
		}

		// Token: 0x06000363 RID: 867 RVA: 0x00012B23 File Offset: 0x00010D23
		public TlsClientSettings()
		{
			this.certificates = new System.Security.Cryptography.X509Certificates.X509CertificateCollection();
			this.targetHost = string.Empty;
		}

		// Token: 0x06000364 RID: 868 RVA: 0x00012B44 File Offset: 0x00010D44
		public void UpdateCertificateRSA()
		{
			if (this.clientCertificate == null)
			{
				this.certificateRSA = null;
				return;
			}
			Mono.Security.X509.X509Certificate x509Certificate = new Mono.Security.X509.X509Certificate(this.clientCertificate.GetRawCertData());
			this.certificateRSA = new RSAManaged(x509Certificate.RSA.KeySize);
			this.certificateRSA.ImportParameters(x509Certificate.RSA.ExportParameters(false));
		}

		// Token: 0x040001B3 RID: 435
		private string targetHost;

		// Token: 0x040001B4 RID: 436
		private System.Security.Cryptography.X509Certificates.X509CertificateCollection certificates;

		// Token: 0x040001B5 RID: 437
		private System.Security.Cryptography.X509Certificates.X509Certificate clientCertificate;

		// Token: 0x040001B6 RID: 438
		private RSAManaged certificateRSA;
	}
}
