﻿using System;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000039 RID: 57
	[Serializable]
	public enum ExchangeAlgorithmType
	{
		// Token: 0x04000168 RID: 360
		DiffieHellman,
		// Token: 0x04000169 RID: 361
		Fortezza,
		// Token: 0x0400016A RID: 362
		None,
		// Token: 0x0400016B RID: 363
		RsaKeyX,
		// Token: 0x0400016C RID: 364
		RsaSign
	}
}
