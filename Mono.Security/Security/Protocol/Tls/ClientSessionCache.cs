﻿using System;
using System.Collections;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000035 RID: 53
	internal class ClientSessionCache
	{
		// Token: 0x06000235 RID: 565 RVA: 0x0000E3F6 File Offset: 0x0000C5F6
		static ClientSessionCache()
		{
		}

		// Token: 0x06000236 RID: 566 RVA: 0x0000E40C File Offset: 0x0000C60C
		public static void Add(string host, byte[] id)
		{
			object obj = ClientSessionCache.locker;
			lock (obj)
			{
				string key = BitConverter.ToString(id);
				ClientSessionInfo clientSessionInfo = (ClientSessionInfo)ClientSessionCache.cache[key];
				if (clientSessionInfo == null)
				{
					ClientSessionCache.cache.Add(key, new ClientSessionInfo(host, id));
				}
				else if (clientSessionInfo.HostName == host)
				{
					clientSessionInfo.KeepAlive();
				}
				else
				{
					clientSessionInfo.Dispose();
					ClientSessionCache.cache.Remove(key);
					ClientSessionCache.cache.Add(key, new ClientSessionInfo(host, id));
				}
			}
		}

		// Token: 0x06000237 RID: 567 RVA: 0x0000E4B0 File Offset: 0x0000C6B0
		public static byte[] FromHost(string host)
		{
			object obj = ClientSessionCache.locker;
			byte[] result;
			lock (obj)
			{
				foreach (object obj2 in ClientSessionCache.cache.Values)
				{
					ClientSessionInfo clientSessionInfo = (ClientSessionInfo)obj2;
					if (clientSessionInfo.HostName == host && clientSessionInfo.Valid)
					{
						clientSessionInfo.KeepAlive();
						return clientSessionInfo.Id;
					}
				}
				result = null;
			}
			return result;
		}

		// Token: 0x06000238 RID: 568 RVA: 0x0000E55C File Offset: 0x0000C75C
		private static ClientSessionInfo FromContext(Context context, bool checkValidity)
		{
			if (context == null)
			{
				return null;
			}
			byte[] sessionId = context.SessionId;
			if (sessionId == null || sessionId.Length == 0)
			{
				return null;
			}
			string key = BitConverter.ToString(sessionId);
			ClientSessionInfo clientSessionInfo = (ClientSessionInfo)ClientSessionCache.cache[key];
			if (clientSessionInfo == null)
			{
				return null;
			}
			if (context.ClientSettings.TargetHost != clientSessionInfo.HostName)
			{
				return null;
			}
			if (checkValidity && !clientSessionInfo.Valid)
			{
				clientSessionInfo.Dispose();
				ClientSessionCache.cache.Remove(key);
				return null;
			}
			return clientSessionInfo;
		}

		// Token: 0x06000239 RID: 569 RVA: 0x0000E5D4 File Offset: 0x0000C7D4
		public static bool SetContextInCache(Context context)
		{
			object obj = ClientSessionCache.locker;
			bool result;
			lock (obj)
			{
				ClientSessionInfo clientSessionInfo = ClientSessionCache.FromContext(context, false);
				if (clientSessionInfo == null)
				{
					result = false;
				}
				else
				{
					clientSessionInfo.GetContext(context);
					clientSessionInfo.KeepAlive();
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600023A RID: 570 RVA: 0x0000E62C File Offset: 0x0000C82C
		public static bool SetContextFromCache(Context context)
		{
			object obj = ClientSessionCache.locker;
			bool result;
			lock (obj)
			{
				ClientSessionInfo clientSessionInfo = ClientSessionCache.FromContext(context, true);
				if (clientSessionInfo == null)
				{
					result = false;
				}
				else
				{
					clientSessionInfo.SetContext(context);
					clientSessionInfo.KeepAlive();
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600023B RID: 571 RVA: 0x0000E684 File Offset: 0x0000C884
		public ClientSessionCache()
		{
		}

		// Token: 0x0400013C RID: 316
		private static Hashtable cache = new Hashtable();

		// Token: 0x0400013D RID: 317
		private static object locker = new object();
	}
}
