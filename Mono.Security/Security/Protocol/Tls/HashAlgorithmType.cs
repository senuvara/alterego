﻿using System;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x0200003B RID: 59
	[Serializable]
	public enum HashAlgorithmType
	{
		// Token: 0x04000172 RID: 370
		Md5,
		// Token: 0x04000173 RID: 371
		None,
		// Token: 0x04000174 RID: 372
		Sha1
	}
}
