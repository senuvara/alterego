﻿using System;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x0200003A RID: 58
	[Serializable]
	internal enum HandshakeState
	{
		// Token: 0x0400016E RID: 366
		None,
		// Token: 0x0400016F RID: 367
		Started,
		// Token: 0x04000170 RID: 368
		Finished
	}
}
