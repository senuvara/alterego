﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using Mono.Security.Cryptography;

namespace Mono.Security.Protocol.Ntlm
{
	// Token: 0x0200006D RID: 109
	public static class ChallengeResponse2
	{
		// Token: 0x0600041E RID: 1054 RVA: 0x000159B4 File Offset: 0x00013BB4
		private static byte[] Compute_LM(string password, byte[] challenge)
		{
			byte[] array = new byte[21];
			DES des = DES.Create();
			des.Mode = CipherMode.ECB;
			if (password == null || password.Length < 1)
			{
				Buffer.BlockCopy(ChallengeResponse2.nullEncMagic, 0, array, 0, 8);
			}
			else
			{
				des.Key = ChallengeResponse2.PasswordToKey(password, 0);
				des.CreateEncryptor().TransformBlock(ChallengeResponse2.magic, 0, 8, array, 0);
			}
			if (password == null || password.Length < 8)
			{
				Buffer.BlockCopy(ChallengeResponse2.nullEncMagic, 0, array, 8, 8);
			}
			else
			{
				des.Key = ChallengeResponse2.PasswordToKey(password, 7);
				des.CreateEncryptor().TransformBlock(ChallengeResponse2.magic, 0, 8, array, 8);
			}
			des.Clear();
			return ChallengeResponse2.GetResponse(challenge, array);
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x00015A60 File Offset: 0x00013C60
		private static byte[] Compute_NTLM_Password(string password)
		{
			byte[] array = new byte[21];
			HashAlgorithm hashAlgorithm = MD4.Create();
			byte[] array2 = (password == null) ? new byte[0] : Encoding.Unicode.GetBytes(password);
			byte[] array3 = hashAlgorithm.ComputeHash(array2);
			Buffer.BlockCopy(array3, 0, array, 0, 16);
			Array.Clear(array2, 0, array2.Length);
			Array.Clear(array3, 0, array3.Length);
			return array;
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x00015AB8 File Offset: 0x00013CB8
		private static byte[] Compute_NTLM(string password, byte[] challenge)
		{
			byte[] pwd = ChallengeResponse2.Compute_NTLM_Password(password);
			return ChallengeResponse2.GetResponse(challenge, pwd);
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x00015AD4 File Offset: 0x00013CD4
		private static void Compute_NTLMv2_Session(string password, byte[] challenge, out byte[] lm, out byte[] ntlm)
		{
			byte[] array = new byte[8];
			RandomNumberGenerator.Create().GetBytes(array);
			byte[] array2 = new byte[challenge.Length + 8];
			challenge.CopyTo(array2, 0);
			array.CopyTo(array2, challenge.Length);
			lm = new byte[24];
			array.CopyTo(lm, 0);
			byte[] array3 = MD5.Create().ComputeHash(array2);
			byte[] array4 = new byte[8];
			Array.Copy(array3, array4, 8);
			ntlm = ChallengeResponse2.Compute_NTLM(password, array4);
			Array.Clear(array, 0, array.Length);
			Array.Clear(array2, 0, array2.Length);
			Array.Clear(array4, 0, array4.Length);
			Array.Clear(array3, 0, array3.Length);
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x00015B70 File Offset: 0x00013D70
		private static byte[] Compute_NTLMv2(Type2Message type2, string username, string password, string domain)
		{
			byte[] array = ChallengeResponse2.Compute_NTLM_Password(password);
			byte[] bytes = Encoding.Unicode.GetBytes(username.ToUpperInvariant());
			byte[] bytes2 = Encoding.Unicode.GetBytes(domain);
			byte[] array2 = new byte[bytes.Length + bytes2.Length];
			bytes.CopyTo(array2, 0);
			Array.Copy(bytes2, 0, array2, bytes.Length, bytes2.Length);
			HMACMD5 hmacmd = new HMACMD5(array);
			byte[] array3 = hmacmd.ComputeHash(array2);
			Array.Clear(array, 0, array.Length);
			hmacmd.Clear();
			HMACMD5 hmacmd2 = new HMACMD5(array3);
			long value = DateTime.Now.Ticks - 504911232000000000L;
			byte[] array4 = new byte[8];
			RandomNumberGenerator.Create().GetBytes(array4);
			byte[] array5 = new byte[28 + type2.TargetInfo.Length];
			array5[0] = 1;
			array5[1] = 1;
			Buffer.BlockCopy(BitConverterLE.GetBytes(value), 0, array5, 8, 8);
			Buffer.BlockCopy(array4, 0, array5, 16, 8);
			Buffer.BlockCopy(type2.TargetInfo, 0, array5, 28, type2.TargetInfo.Length);
			byte[] nonce = type2.Nonce;
			byte[] array6 = new byte[nonce.Length + array5.Length];
			nonce.CopyTo(array6, 0);
			array5.CopyTo(array6, nonce.Length);
			byte[] array7 = hmacmd2.ComputeHash(array6);
			byte[] array8 = new byte[array5.Length + array7.Length];
			array7.CopyTo(array8, 0);
			array5.CopyTo(array8, array7.Length);
			Array.Clear(array3, 0, array3.Length);
			hmacmd2.Clear();
			Array.Clear(array4, 0, array4.Length);
			Array.Clear(array5, 0, array5.Length);
			Array.Clear(array6, 0, array6.Length);
			Array.Clear(array7, 0, array7.Length);
			return array8;
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x00015D08 File Offset: 0x00013F08
		public static void Compute(Type2Message type2, NtlmAuthLevel level, string username, string password, string domain, out byte[] lm, out byte[] ntlm)
		{
			lm = null;
			switch (level)
			{
			case NtlmAuthLevel.LM_and_NTLM:
				break;
			case NtlmAuthLevel.LM_and_NTLM_and_try_NTLMv2_Session:
				if ((type2.Flags & NtlmFlags.NegotiateNtlm2Key) != (NtlmFlags)0)
				{
					ChallengeResponse2.Compute_NTLMv2_Session(password, type2.Nonce, out lm, out ntlm);
					return;
				}
				break;
			case NtlmAuthLevel.NTLM_only:
				if ((type2.Flags & NtlmFlags.NegotiateNtlm2Key) != (NtlmFlags)0)
				{
					ChallengeResponse2.Compute_NTLMv2_Session(password, type2.Nonce, out lm, out ntlm);
					return;
				}
				ntlm = ChallengeResponse2.Compute_NTLM(password, type2.Nonce);
				return;
			case NtlmAuthLevel.NTLMv2_only:
				ntlm = ChallengeResponse2.Compute_NTLMv2(type2, username, password, domain);
				return;
			default:
				throw new InvalidOperationException();
			}
			lm = ChallengeResponse2.Compute_LM(password, type2.Nonce);
			ntlm = ChallengeResponse2.Compute_NTLM(password, type2.Nonce);
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x00015DB4 File Offset: 0x00013FB4
		private static byte[] GetResponse(byte[] challenge, byte[] pwd)
		{
			byte[] array = new byte[24];
			DES des = DES.Create();
			des.Mode = CipherMode.ECB;
			des.Key = ChallengeResponse2.PrepareDESKey(pwd, 0);
			des.CreateEncryptor().TransformBlock(challenge, 0, 8, array, 0);
			des.Key = ChallengeResponse2.PrepareDESKey(pwd, 7);
			des.CreateEncryptor().TransformBlock(challenge, 0, 8, array, 8);
			des.Key = ChallengeResponse2.PrepareDESKey(pwd, 14);
			des.CreateEncryptor().TransformBlock(challenge, 0, 8, array, 16);
			return array;
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x00015E34 File Offset: 0x00014034
		private static byte[] PrepareDESKey(byte[] key56bits, int position)
		{
			return new byte[]
			{
				key56bits[position],
				(byte)((int)key56bits[position] << 7 | key56bits[position + 1] >> 1),
				(byte)((int)key56bits[position + 1] << 6 | key56bits[position + 2] >> 2),
				(byte)((int)key56bits[position + 2] << 5 | key56bits[position + 3] >> 3),
				(byte)((int)key56bits[position + 3] << 4 | key56bits[position + 4] >> 4),
				(byte)((int)key56bits[position + 4] << 3 | key56bits[position + 5] >> 5),
				(byte)((int)key56bits[position + 5] << 2 | key56bits[position + 6] >> 6),
				(byte)(key56bits[position + 6] << 1)
			};
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x00015EC8 File Offset: 0x000140C8
		private static byte[] PasswordToKey(string password, int position)
		{
			byte[] array = new byte[7];
			int charCount = Math.Min(password.Length - position, 7);
			Encoding.ASCII.GetBytes(password.ToUpper(CultureInfo.CurrentCulture), position, charCount, array, 0);
			byte[] result = ChallengeResponse2.PrepareDESKey(array, 0);
			Array.Clear(array, 0, array.Length);
			return result;
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x00015F16 File Offset: 0x00014116
		// Note: this type is marked as 'beforefieldinit'.
		static ChallengeResponse2()
		{
		}

		// Token: 0x040001F7 RID: 503
		private static byte[] magic = new byte[]
		{
			75,
			71,
			83,
			33,
			64,
			35,
			36,
			37
		};

		// Token: 0x040001F8 RID: 504
		private static byte[] nullEncMagic = new byte[]
		{
			170,
			211,
			180,
			53,
			181,
			20,
			4,
			238
		};
	}
}
