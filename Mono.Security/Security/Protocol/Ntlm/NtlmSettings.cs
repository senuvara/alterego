﻿using System;

namespace Mono.Security.Protocol.Ntlm
{
	// Token: 0x02000071 RID: 113
	public static class NtlmSettings
	{
		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000431 RID: 1073 RVA: 0x0001608A File Offset: 0x0001428A
		// (set) Token: 0x06000432 RID: 1074 RVA: 0x00016091 File Offset: 0x00014291
		public static NtlmAuthLevel DefaultAuthLevel
		{
			get
			{
				return NtlmSettings.defaultAuthLevel;
			}
			set
			{
				NtlmSettings.defaultAuthLevel = value;
			}
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x00016099 File Offset: 0x00014299
		// Note: this type is marked as 'beforefieldinit'.
		static NtlmSettings()
		{
		}

		// Token: 0x0400020C RID: 524
		private static NtlmAuthLevel defaultAuthLevel = NtlmAuthLevel.LM_and_NTLM_and_try_NTLMv2_Session;
	}
}
