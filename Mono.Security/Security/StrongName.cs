﻿using System;
using System.Configuration.Assemblies;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using Mono.Security.Cryptography;

namespace Mono.Security
{
	// Token: 0x0200000B RID: 11
	public sealed class StrongName
	{
		// Token: 0x0600004F RID: 79 RVA: 0x000038F3 File Offset: 0x00001AF3
		public StrongName()
		{
		}

		// Token: 0x06000050 RID: 80 RVA: 0x000038FB File Offset: 0x00001AFB
		public StrongName(int keySize)
		{
			this.rsa = new RSAManaged(keySize);
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00003910 File Offset: 0x00001B10
		public StrongName(byte[] data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (data.Length == 16)
			{
				int i = 0;
				int num = 0;
				while (i < data.Length)
				{
					num += (int)data[i++];
				}
				if (num == 4)
				{
					this.publicKey = (byte[])data.Clone();
					return;
				}
			}
			else
			{
				this.RSA = CryptoConvert.FromCapiKeyBlob(data);
				if (this.rsa == null)
				{
					throw new ArgumentException("data isn't a correctly encoded RSA public key");
				}
			}
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00003983 File Offset: 0x00001B83
		public StrongName(RSA rsa)
		{
			if (rsa == null)
			{
				throw new ArgumentNullException("rsa");
			}
			this.RSA = rsa;
		}

		// Token: 0x06000053 RID: 83 RVA: 0x000039A0 File Offset: 0x00001BA0
		private void InvalidateCache()
		{
			this.publicKey = null;
			this.keyToken = null;
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000054 RID: 84 RVA: 0x000039B0 File Offset: 0x00001BB0
		public bool CanSign
		{
			get
			{
				if (this.rsa == null)
				{
					return false;
				}
				if (this.RSA is RSAManaged)
				{
					return !(this.rsa as RSAManaged).PublicOnly;
				}
				bool result;
				try
				{
					RSAParameters rsaparameters = this.rsa.ExportParameters(true);
					result = (rsaparameters.D != null && rsaparameters.P != null && rsaparameters.Q != null);
				}
				catch (CryptographicException)
				{
					result = false;
				}
				return result;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000055 RID: 85 RVA: 0x00003A2C File Offset: 0x00001C2C
		// (set) Token: 0x06000056 RID: 86 RVA: 0x00003A47 File Offset: 0x00001C47
		public RSA RSA
		{
			get
			{
				if (this.rsa == null)
				{
					this.rsa = RSA.Create();
				}
				return this.rsa;
			}
			set
			{
				this.rsa = value;
				this.InvalidateCache();
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00003A58 File Offset: 0x00001C58
		public byte[] PublicKey
		{
			get
			{
				if (this.publicKey == null)
				{
					byte[] array = CryptoConvert.ToCapiKeyBlob(this.rsa, false);
					this.publicKey = new byte[32 + (this.rsa.KeySize >> 3)];
					this.publicKey[0] = array[4];
					this.publicKey[1] = array[5];
					this.publicKey[2] = array[6];
					this.publicKey[3] = array[7];
					this.publicKey[4] = 4;
					this.publicKey[5] = 128;
					this.publicKey[6] = 0;
					this.publicKey[7] = 0;
					byte[] bytes = BitConverterLE.GetBytes(this.publicKey.Length - 12);
					this.publicKey[8] = bytes[0];
					this.publicKey[9] = bytes[1];
					this.publicKey[10] = bytes[2];
					this.publicKey[11] = bytes[3];
					this.publicKey[12] = 6;
					Buffer.BlockCopy(array, 1, this.publicKey, 13, this.publicKey.Length - 13);
					this.publicKey[23] = 49;
				}
				return (byte[])this.publicKey.Clone();
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000058 RID: 88 RVA: 0x00003B6C File Offset: 0x00001D6C
		public byte[] PublicKeyToken
		{
			get
			{
				if (this.keyToken == null)
				{
					byte[] array = this.PublicKey;
					if (array == null)
					{
						return null;
					}
					byte[] array2 = HashAlgorithm.Create(this.TokenAlgorithm).ComputeHash(array);
					this.keyToken = new byte[8];
					Buffer.BlockCopy(array2, array2.Length - 8, this.keyToken, 0, 8);
					Array.Reverse<byte>(this.keyToken, 0, 8);
				}
				return (byte[])this.keyToken.Clone();
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00003BDB File Offset: 0x00001DDB
		// (set) Token: 0x0600005A RID: 90 RVA: 0x00003BF8 File Offset: 0x00001DF8
		public string TokenAlgorithm
		{
			get
			{
				if (this.tokenAlgorithm == null)
				{
					this.tokenAlgorithm = "SHA1";
				}
				return this.tokenAlgorithm;
			}
			set
			{
				string a = value.ToUpper(CultureInfo.InvariantCulture);
				if (a == "SHA1" || a == "MD5")
				{
					this.tokenAlgorithm = value;
					this.InvalidateCache();
					return;
				}
				throw new ArgumentException("Unsupported hash algorithm for token");
			}
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00003C43 File Offset: 0x00001E43
		public byte[] GetBytes()
		{
			return CryptoConvert.ToCapiPrivateKeyBlob(this.RSA);
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00003C50 File Offset: 0x00001E50
		private uint RVAtoPosition(uint r, int sections, byte[] headers)
		{
			for (int i = 0; i < sections; i++)
			{
				uint num = BitConverterLE.ToUInt32(headers, i * 40 + 20);
				uint num2 = BitConverterLE.ToUInt32(headers, i * 40 + 12);
				int num3 = (int)BitConverterLE.ToUInt32(headers, i * 40 + 8);
				if (num2 <= r && (ulong)r < (ulong)num2 + (ulong)((long)num3))
				{
					return num + r - num2;
				}
			}
			return 0U;
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00003CA8 File Offset: 0x00001EA8
		internal StrongName.StrongNameSignature StrongHash(Stream stream, StrongName.StrongNameOptions options)
		{
			StrongName.StrongNameSignature strongNameSignature = new StrongName.StrongNameSignature();
			HashAlgorithm hashAlgorithm = HashAlgorithm.Create(this.TokenAlgorithm);
			CryptoStream cryptoStream = new CryptoStream(Stream.Null, hashAlgorithm, CryptoStreamMode.Write);
			byte[] array = new byte[128];
			stream.Read(array, 0, 128);
			if (BitConverterLE.ToUInt16(array, 0) != 23117)
			{
				return null;
			}
			uint num = BitConverterLE.ToUInt32(array, 60);
			cryptoStream.Write(array, 0, 128);
			if (num != 128U)
			{
				byte[] array2 = new byte[num - 128U];
				stream.Read(array2, 0, array2.Length);
				cryptoStream.Write(array2, 0, array2.Length);
			}
			byte[] array3 = new byte[248];
			stream.Read(array3, 0, 248);
			if (BitConverterLE.ToUInt32(array3, 0) != 17744U)
			{
				return null;
			}
			if (BitConverterLE.ToUInt16(array3, 4) != 332)
			{
				return null;
			}
			byte[] src = new byte[8];
			Buffer.BlockCopy(src, 0, array3, 88, 4);
			Buffer.BlockCopy(src, 0, array3, 152, 8);
			cryptoStream.Write(array3, 0, 248);
			ushort num2 = BitConverterLE.ToUInt16(array3, 6);
			int num3 = (int)(num2 * 40);
			byte[] array4 = new byte[num3];
			stream.Read(array4, 0, num3);
			cryptoStream.Write(array4, 0, num3);
			uint r = BitConverterLE.ToUInt32(array3, 232);
			uint num4 = this.RVAtoPosition(r, (int)num2, array4);
			int num5 = (int)BitConverterLE.ToUInt32(array3, 236);
			byte[] array5 = new byte[num5];
			stream.Position = (long)((ulong)num4);
			stream.Read(array5, 0, num5);
			uint r2 = BitConverterLE.ToUInt32(array5, 32);
			strongNameSignature.SignaturePosition = this.RVAtoPosition(r2, (int)num2, array4);
			strongNameSignature.SignatureLength = BitConverterLE.ToUInt32(array5, 36);
			uint r3 = BitConverterLE.ToUInt32(array5, 8);
			strongNameSignature.MetadataPosition = this.RVAtoPosition(r3, (int)num2, array4);
			strongNameSignature.MetadataLength = BitConverterLE.ToUInt32(array5, 12);
			if (options == StrongName.StrongNameOptions.Metadata)
			{
				cryptoStream.Close();
				hashAlgorithm.Initialize();
				byte[] array6 = new byte[strongNameSignature.MetadataLength];
				stream.Position = (long)((ulong)strongNameSignature.MetadataPosition);
				stream.Read(array6, 0, array6.Length);
				strongNameSignature.Hash = hashAlgorithm.ComputeHash(array6);
				return strongNameSignature;
			}
			for (int i = 0; i < (int)num2; i++)
			{
				uint num6 = BitConverterLE.ToUInt32(array4, i * 40 + 20);
				int num7 = (int)BitConverterLE.ToUInt32(array4, i * 40 + 16);
				byte[] array7 = new byte[num7];
				stream.Position = (long)((ulong)num6);
				stream.Read(array7, 0, num7);
				if (num6 <= strongNameSignature.SignaturePosition && (ulong)strongNameSignature.SignaturePosition < (ulong)num6 + (ulong)((long)num7))
				{
					int num8 = (int)(strongNameSignature.SignaturePosition - num6);
					if (num8 > 0)
					{
						cryptoStream.Write(array7, 0, num8);
					}
					strongNameSignature.Signature = new byte[strongNameSignature.SignatureLength];
					Buffer.BlockCopy(array7, num8, strongNameSignature.Signature, 0, (int)strongNameSignature.SignatureLength);
					Array.Reverse<byte>(strongNameSignature.Signature);
					int num9 = (int)((long)num8 + (long)((ulong)strongNameSignature.SignatureLength));
					int num10 = num7 - num9;
					if (num10 > 0)
					{
						cryptoStream.Write(array7, num9, num10);
					}
				}
				else
				{
					cryptoStream.Write(array7, 0, num7);
				}
			}
			cryptoStream.Close();
			strongNameSignature.Hash = hashAlgorithm.Hash;
			return strongNameSignature;
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00003FD0 File Offset: 0x000021D0
		public byte[] Hash(string fileName)
		{
			FileStream fileStream = File.OpenRead(fileName);
			StrongName.StrongNameSignature strongNameSignature = this.StrongHash(fileStream, StrongName.StrongNameOptions.Metadata);
			fileStream.Close();
			return strongNameSignature.Hash;
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00003FF8 File Offset: 0x000021F8
		public bool Sign(string fileName)
		{
			bool result = false;
			StrongName.StrongNameSignature strongNameSignature;
			using (FileStream fileStream = File.OpenRead(fileName))
			{
				strongNameSignature = this.StrongHash(fileStream, StrongName.StrongNameOptions.Signature);
				fileStream.Close();
			}
			if (strongNameSignature.Hash == null)
			{
				return false;
			}
			byte[] array = null;
			try
			{
				RSAPKCS1SignatureFormatter rsapkcs1SignatureFormatter = new RSAPKCS1SignatureFormatter(this.rsa);
				rsapkcs1SignatureFormatter.SetHashAlgorithm(this.TokenAlgorithm);
				array = rsapkcs1SignatureFormatter.CreateSignature(strongNameSignature.Hash);
				Array.Reverse<byte>(array);
			}
			catch (CryptographicException)
			{
				return false;
			}
			using (FileStream fileStream2 = File.OpenWrite(fileName))
			{
				fileStream2.Position = (long)((ulong)strongNameSignature.SignaturePosition);
				fileStream2.Write(array, 0, array.Length);
				fileStream2.Close();
				result = true;
			}
			return result;
		}

		// Token: 0x06000060 RID: 96 RVA: 0x000040CC File Offset: 0x000022CC
		public bool Verify(string fileName)
		{
			bool result = false;
			using (FileStream fileStream = File.OpenRead(fileName))
			{
				result = this.Verify(fileStream);
				fileStream.Close();
			}
			return result;
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00004110 File Offset: 0x00002310
		public bool Verify(Stream stream)
		{
			StrongName.StrongNameSignature strongNameSignature = this.StrongHash(stream, StrongName.StrongNameOptions.Signature);
			if (strongNameSignature.Hash == null)
			{
				return false;
			}
			bool result;
			try
			{
				AssemblyHashAlgorithm algorithm = AssemblyHashAlgorithm.SHA1;
				if (this.tokenAlgorithm == "MD5")
				{
					algorithm = AssemblyHashAlgorithm.MD5;
				}
				result = StrongName.Verify(this.rsa, algorithm, strongNameSignature.Hash, strongNameSignature.Signature);
			}
			catch (CryptographicException)
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00004180 File Offset: 0x00002380
		private static bool Verify(RSA rsa, AssemblyHashAlgorithm algorithm, byte[] hash, byte[] signature)
		{
			RSAPKCS1SignatureDeformatter rsapkcs1SignatureDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
			if (algorithm != AssemblyHashAlgorithm.None)
			{
				if (algorithm == AssemblyHashAlgorithm.MD5)
				{
					rsapkcs1SignatureDeformatter.SetHashAlgorithm("MD5");
					goto IL_34;
				}
				if (algorithm != AssemblyHashAlgorithm.SHA1)
				{
				}
			}
			rsapkcs1SignatureDeformatter.SetHashAlgorithm("SHA1");
			IL_34:
			return rsapkcs1SignatureDeformatter.VerifySignature(hash, signature);
		}

		// Token: 0x04000047 RID: 71
		private RSA rsa;

		// Token: 0x04000048 RID: 72
		private byte[] publicKey;

		// Token: 0x04000049 RID: 73
		private byte[] keyToken;

		// Token: 0x0400004A RID: 74
		private string tokenAlgorithm;

		// Token: 0x020000C2 RID: 194
		internal class StrongNameSignature
		{
			// Token: 0x170001C2 RID: 450
			// (get) Token: 0x06000728 RID: 1832 RVA: 0x000212B9 File Offset: 0x0001F4B9
			// (set) Token: 0x06000729 RID: 1833 RVA: 0x000212C1 File Offset: 0x0001F4C1
			public byte[] Hash
			{
				get
				{
					return this.hash;
				}
				set
				{
					this.hash = value;
				}
			}

			// Token: 0x170001C3 RID: 451
			// (get) Token: 0x0600072A RID: 1834 RVA: 0x000212CA File Offset: 0x0001F4CA
			// (set) Token: 0x0600072B RID: 1835 RVA: 0x000212D2 File Offset: 0x0001F4D2
			public byte[] Signature
			{
				get
				{
					return this.signature;
				}
				set
				{
					this.signature = value;
				}
			}

			// Token: 0x170001C4 RID: 452
			// (get) Token: 0x0600072C RID: 1836 RVA: 0x000212DB File Offset: 0x0001F4DB
			// (set) Token: 0x0600072D RID: 1837 RVA: 0x000212E3 File Offset: 0x0001F4E3
			public uint MetadataPosition
			{
				get
				{
					return this.metadataPosition;
				}
				set
				{
					this.metadataPosition = value;
				}
			}

			// Token: 0x170001C5 RID: 453
			// (get) Token: 0x0600072E RID: 1838 RVA: 0x000212EC File Offset: 0x0001F4EC
			// (set) Token: 0x0600072F RID: 1839 RVA: 0x000212F4 File Offset: 0x0001F4F4
			public uint MetadataLength
			{
				get
				{
					return this.metadataLength;
				}
				set
				{
					this.metadataLength = value;
				}
			}

			// Token: 0x170001C6 RID: 454
			// (get) Token: 0x06000730 RID: 1840 RVA: 0x000212FD File Offset: 0x0001F4FD
			// (set) Token: 0x06000731 RID: 1841 RVA: 0x00021305 File Offset: 0x0001F505
			public uint SignaturePosition
			{
				get
				{
					return this.signaturePosition;
				}
				set
				{
					this.signaturePosition = value;
				}
			}

			// Token: 0x170001C7 RID: 455
			// (get) Token: 0x06000732 RID: 1842 RVA: 0x0002130E File Offset: 0x0001F50E
			// (set) Token: 0x06000733 RID: 1843 RVA: 0x00021316 File Offset: 0x0001F516
			public uint SignatureLength
			{
				get
				{
					return this.signatureLength;
				}
				set
				{
					this.signatureLength = value;
				}
			}

			// Token: 0x170001C8 RID: 456
			// (get) Token: 0x06000734 RID: 1844 RVA: 0x0002131F File Offset: 0x0001F51F
			// (set) Token: 0x06000735 RID: 1845 RVA: 0x00021327 File Offset: 0x0001F527
			public byte CliFlag
			{
				get
				{
					return this.cliFlag;
				}
				set
				{
					this.cliFlag = value;
				}
			}

			// Token: 0x170001C9 RID: 457
			// (get) Token: 0x06000736 RID: 1846 RVA: 0x00021330 File Offset: 0x0001F530
			// (set) Token: 0x06000737 RID: 1847 RVA: 0x00021338 File Offset: 0x0001F538
			public uint CliFlagPosition
			{
				get
				{
					return this.cliFlagPosition;
				}
				set
				{
					this.cliFlagPosition = value;
				}
			}

			// Token: 0x06000738 RID: 1848 RVA: 0x00021341 File Offset: 0x0001F541
			public StrongNameSignature()
			{
			}

			// Token: 0x040004B3 RID: 1203
			private byte[] hash;

			// Token: 0x040004B4 RID: 1204
			private byte[] signature;

			// Token: 0x040004B5 RID: 1205
			private uint signaturePosition;

			// Token: 0x040004B6 RID: 1206
			private uint signatureLength;

			// Token: 0x040004B7 RID: 1207
			private uint metadataPosition;

			// Token: 0x040004B8 RID: 1208
			private uint metadataLength;

			// Token: 0x040004B9 RID: 1209
			private byte cliFlag;

			// Token: 0x040004BA RID: 1210
			private uint cliFlagPosition;
		}

		// Token: 0x020000C3 RID: 195
		internal enum StrongNameOptions
		{
			// Token: 0x040004BC RID: 1212
			Metadata,
			// Token: 0x040004BD RID: 1213
			Signature
		}
	}
}
