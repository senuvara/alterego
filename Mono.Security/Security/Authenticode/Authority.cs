﻿using System;

namespace Mono.Security.Authenticode
{
	// Token: 0x020000A3 RID: 163
	public enum Authority
	{
		// Token: 0x040003FB RID: 1019
		Individual,
		// Token: 0x040003FC RID: 1020
		Commercial,
		// Token: 0x040003FD RID: 1021
		Maximum
	}
}
