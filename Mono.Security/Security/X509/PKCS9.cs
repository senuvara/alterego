﻿using System;

namespace Mono.Security.X509
{
	// Token: 0x0200000D RID: 13
	public class PKCS9
	{
		// Token: 0x06000064 RID: 100 RVA: 0x000041D1 File Offset: 0x000023D1
		public PKCS9()
		{
		}

		// Token: 0x04000051 RID: 81
		public const string friendlyName = "1.2.840.113549.1.9.20";

		// Token: 0x04000052 RID: 82
		public const string localKeyId = "1.2.840.113549.1.9.21";
	}
}
