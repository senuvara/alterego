﻿using System;

namespace Mono.Security.X509.Extensions
{
	// Token: 0x02000029 RID: 41
	public class SubjectAltNameExtension : X509Extension
	{
		// Token: 0x060001D3 RID: 467 RVA: 0x0000CFC7 File Offset: 0x0000B1C7
		public SubjectAltNameExtension()
		{
			this.extnOid = "2.5.29.17";
			this._names = new GeneralNames();
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x0000CFE5 File Offset: 0x0000B1E5
		public SubjectAltNameExtension(ASN1 asn1) : base(asn1)
		{
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x0000CFEE File Offset: 0x0000B1EE
		public SubjectAltNameExtension(X509Extension extension) : base(extension)
		{
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x0000CFF7 File Offset: 0x0000B1F7
		public SubjectAltNameExtension(string[] rfc822, string[] dnsNames, string[] ipAddresses, string[] uris)
		{
			this._names = new GeneralNames(rfc822, dnsNames, ipAddresses, uris);
			this.extnValue = new ASN1(4, this._names.GetBytes());
			this.extnOid = "2.5.29.17";
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x0000D034 File Offset: 0x0000B234
		protected override void Decode()
		{
			ASN1 asn = new ASN1(this.extnValue.Value);
			if (asn.Tag != 48)
			{
				throw new ArgumentException("Invalid SubjectAltName extension");
			}
			this._names = new GeneralNames(asn);
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x0000D073 File Offset: 0x0000B273
		public override string Name
		{
			get
			{
				return "Subject Alternative Name";
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060001D9 RID: 473 RVA: 0x0000D07A File Offset: 0x0000B27A
		public string[] RFC822
		{
			get
			{
				return this._names.RFC822;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x060001DA RID: 474 RVA: 0x0000D087 File Offset: 0x0000B287
		public string[] DNSNames
		{
			get
			{
				return this._names.DNSNames;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060001DB RID: 475 RVA: 0x0000D094 File Offset: 0x0000B294
		public string[] IPAddresses
		{
			get
			{
				return this._names.IPAddresses;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x060001DC RID: 476 RVA: 0x0000D0A1 File Offset: 0x0000B2A1
		public string[] UniformResourceIdentifiers
		{
			get
			{
				return this._names.UniformResourceIdentifiers;
			}
		}

		// Token: 0x060001DD RID: 477 RVA: 0x0000D0AE File Offset: 0x0000B2AE
		public override string ToString()
		{
			return this._names.ToString();
		}

		// Token: 0x040000F5 RID: 245
		private GeneralNames _names;
	}
}
