﻿using System;
using System.Globalization;
using System.Text;

namespace Mono.Security.X509.Extensions
{
	// Token: 0x0200001F RID: 31
	public class BasicConstraintsExtension : X509Extension
	{
		// Token: 0x06000188 RID: 392 RVA: 0x0000B644 File Offset: 0x00009844
		public BasicConstraintsExtension()
		{
			this.extnOid = "2.5.29.19";
			this.pathLenConstraint = -1;
		}

		// Token: 0x06000189 RID: 393 RVA: 0x0000B65E File Offset: 0x0000985E
		public BasicConstraintsExtension(ASN1 asn1) : base(asn1)
		{
		}

		// Token: 0x0600018A RID: 394 RVA: 0x0000B667 File Offset: 0x00009867
		public BasicConstraintsExtension(X509Extension extension) : base(extension)
		{
		}

		// Token: 0x0600018B RID: 395 RVA: 0x0000B670 File Offset: 0x00009870
		protected override void Decode()
		{
			this.cA = false;
			this.pathLenConstraint = -1;
			ASN1 asn = new ASN1(this.extnValue.Value);
			if (asn.Tag != 48)
			{
				throw new ArgumentException("Invalid BasicConstraints extension");
			}
			int num = 0;
			ASN1 asn2 = asn[num++];
			if (asn2 != null && asn2.Tag == 1)
			{
				this.cA = (asn2.Value[0] == byte.MaxValue);
				asn2 = asn[num++];
			}
			if (asn2 != null && asn2.Tag == 2)
			{
				this.pathLenConstraint = ASN1Convert.ToInt32(asn2);
			}
		}

		// Token: 0x0600018C RID: 396 RVA: 0x0000B704 File Offset: 0x00009904
		protected override void Encode()
		{
			ASN1 asn = new ASN1(48);
			if (this.cA)
			{
				asn.Add(new ASN1(1, new byte[]
				{
					byte.MaxValue
				}));
			}
			if (this.cA && this.pathLenConstraint >= 0)
			{
				asn.Add(ASN1Convert.FromInt32(this.pathLenConstraint));
			}
			this.extnValue = new ASN1(4);
			this.extnValue.Add(asn);
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x0600018D RID: 397 RVA: 0x0000B778 File Offset: 0x00009978
		// (set) Token: 0x0600018E RID: 398 RVA: 0x0000B780 File Offset: 0x00009980
		public bool CertificateAuthority
		{
			get
			{
				return this.cA;
			}
			set
			{
				this.cA = value;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600018F RID: 399 RVA: 0x0000B789 File Offset: 0x00009989
		public override string Name
		{
			get
			{
				return "Basic Constraints";
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000190 RID: 400 RVA: 0x0000B790 File Offset: 0x00009990
		// (set) Token: 0x06000191 RID: 401 RVA: 0x0000B798 File Offset: 0x00009998
		public int PathLenConstraint
		{
			get
			{
				return this.pathLenConstraint;
			}
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException(Locale.GetText("PathLenConstraint must be positive or -1 for none ({0}).", new object[]
					{
						value
					}));
				}
				this.pathLenConstraint = value;
			}
		}

		// Token: 0x06000192 RID: 402 RVA: 0x0000B7C4 File Offset: 0x000099C4
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Subject Type=");
			stringBuilder.Append(this.cA ? "CA" : "End Entity");
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append("Path Length Constraint=");
			if (this.pathLenConstraint == -1)
			{
				stringBuilder.Append("None");
			}
			else
			{
				stringBuilder.Append(this.pathLenConstraint.ToString(CultureInfo.InvariantCulture));
			}
			stringBuilder.Append(Environment.NewLine);
			return stringBuilder.ToString();
		}

		// Token: 0x040000D6 RID: 214
		public const int NoPathLengthConstraint = -1;

		// Token: 0x040000D7 RID: 215
		private bool cA;

		// Token: 0x040000D8 RID: 216
		private int pathLenConstraint;
	}
}
