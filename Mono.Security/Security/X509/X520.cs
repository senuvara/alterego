﻿using System;
using System.Text;

namespace Mono.Security.X509
{
	// Token: 0x0200001D RID: 29
	public class X520
	{
		// Token: 0x0600017E RID: 382 RVA: 0x0000B4B5 File Offset: 0x000096B5
		public X520()
		{
		}

		// Token: 0x020000C8 RID: 200
		public abstract class AttributeTypeAndValue
		{
			// Token: 0x06000756 RID: 1878 RVA: 0x00021844 File Offset: 0x0001FA44
			protected AttributeTypeAndValue(string oid, int upperBound)
			{
				this.oid = oid;
				this.upperBound = upperBound;
				this.encoding = byte.MaxValue;
			}

			// Token: 0x06000757 RID: 1879 RVA: 0x00021865 File Offset: 0x0001FA65
			protected AttributeTypeAndValue(string oid, int upperBound, byte encoding)
			{
				this.oid = oid;
				this.upperBound = upperBound;
				this.encoding = encoding;
			}

			// Token: 0x170001D3 RID: 467
			// (get) Token: 0x06000758 RID: 1880 RVA: 0x00021882 File Offset: 0x0001FA82
			// (set) Token: 0x06000759 RID: 1881 RVA: 0x0002188C File Offset: 0x0001FA8C
			public string Value
			{
				get
				{
					return this.attrValue;
				}
				set
				{
					if (this.attrValue != null && this.attrValue.Length > this.upperBound)
					{
						throw new FormatException(string.Format(Locale.GetText("Value length bigger than upperbound ({0})."), this.upperBound));
					}
					this.attrValue = value;
				}
			}

			// Token: 0x170001D4 RID: 468
			// (get) Token: 0x0600075A RID: 1882 RVA: 0x000218DB File Offset: 0x0001FADB
			public ASN1 ASN1
			{
				get
				{
					return this.GetASN1();
				}
			}

			// Token: 0x0600075B RID: 1883 RVA: 0x000218E4 File Offset: 0x0001FAE4
			internal ASN1 GetASN1(byte encoding)
			{
				byte b = encoding;
				if (b == 255)
				{
					b = this.SelectBestEncoding();
				}
				ASN1 asn = new ASN1(48);
				asn.Add(ASN1Convert.FromOid(this.oid));
				if (b != 19)
				{
					if (b != 22)
					{
						if (b == 30)
						{
							asn.Add(new ASN1(30, Encoding.BigEndianUnicode.GetBytes(this.attrValue)));
						}
					}
					else
					{
						asn.Add(new ASN1(22, Encoding.ASCII.GetBytes(this.attrValue)));
					}
				}
				else
				{
					asn.Add(new ASN1(19, Encoding.ASCII.GetBytes(this.attrValue)));
				}
				return asn;
			}

			// Token: 0x0600075C RID: 1884 RVA: 0x0002198C File Offset: 0x0001FB8C
			internal ASN1 GetASN1()
			{
				return this.GetASN1(this.encoding);
			}

			// Token: 0x0600075D RID: 1885 RVA: 0x0002199A File Offset: 0x0001FB9A
			public byte[] GetBytes(byte encoding)
			{
				return this.GetASN1(encoding).GetBytes();
			}

			// Token: 0x0600075E RID: 1886 RVA: 0x000219A8 File Offset: 0x0001FBA8
			public byte[] GetBytes()
			{
				return this.GetASN1().GetBytes();
			}

			// Token: 0x0600075F RID: 1887 RVA: 0x000219B8 File Offset: 0x0001FBB8
			private byte SelectBestEncoding()
			{
				foreach (char c in this.attrValue)
				{
					if (c == '@' || c == '_')
					{
						return 30;
					}
					if (c > '\u007f')
					{
						return 30;
					}
				}
				return 19;
			}

			// Token: 0x040004CE RID: 1230
			private string oid;

			// Token: 0x040004CF RID: 1231
			private string attrValue;

			// Token: 0x040004D0 RID: 1232
			private int upperBound;

			// Token: 0x040004D1 RID: 1233
			private byte encoding;
		}

		// Token: 0x020000C9 RID: 201
		public class Name : X520.AttributeTypeAndValue
		{
			// Token: 0x06000760 RID: 1888 RVA: 0x000219FC File Offset: 0x0001FBFC
			public Name() : base("2.5.4.41", 32768)
			{
			}
		}

		// Token: 0x020000CA RID: 202
		public class CommonName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000761 RID: 1889 RVA: 0x00021A0E File Offset: 0x0001FC0E
			public CommonName() : base("2.5.4.3", 64)
			{
			}
		}

		// Token: 0x020000CB RID: 203
		public class SerialNumber : X520.AttributeTypeAndValue
		{
			// Token: 0x06000762 RID: 1890 RVA: 0x00021A1D File Offset: 0x0001FC1D
			public SerialNumber() : base("2.5.4.5", 64, 19)
			{
			}
		}

		// Token: 0x020000CC RID: 204
		public class LocalityName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000763 RID: 1891 RVA: 0x00021A2E File Offset: 0x0001FC2E
			public LocalityName() : base("2.5.4.7", 128)
			{
			}
		}

		// Token: 0x020000CD RID: 205
		public class StateOrProvinceName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000764 RID: 1892 RVA: 0x00021A40 File Offset: 0x0001FC40
			public StateOrProvinceName() : base("2.5.4.8", 128)
			{
			}
		}

		// Token: 0x020000CE RID: 206
		public class OrganizationName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000765 RID: 1893 RVA: 0x00021A52 File Offset: 0x0001FC52
			public OrganizationName() : base("2.5.4.10", 64)
			{
			}
		}

		// Token: 0x020000CF RID: 207
		public class OrganizationalUnitName : X520.AttributeTypeAndValue
		{
			// Token: 0x06000766 RID: 1894 RVA: 0x00021A61 File Offset: 0x0001FC61
			public OrganizationalUnitName() : base("2.5.4.11", 64)
			{
			}
		}

		// Token: 0x020000D0 RID: 208
		public class EmailAddress : X520.AttributeTypeAndValue
		{
			// Token: 0x06000767 RID: 1895 RVA: 0x00021A70 File Offset: 0x0001FC70
			public EmailAddress() : base("1.2.840.113549.1.9.1", 128, 22)
			{
			}
		}

		// Token: 0x020000D1 RID: 209
		public class DomainComponent : X520.AttributeTypeAndValue
		{
			// Token: 0x06000768 RID: 1896 RVA: 0x00021A84 File Offset: 0x0001FC84
			public DomainComponent() : base("0.9.2342.19200300.100.1.25", int.MaxValue, 22)
			{
			}
		}

		// Token: 0x020000D2 RID: 210
		public class UserId : X520.AttributeTypeAndValue
		{
			// Token: 0x06000769 RID: 1897 RVA: 0x00021A98 File Offset: 0x0001FC98
			public UserId() : base("0.9.2342.19200300.100.1.1", 256)
			{
			}
		}

		// Token: 0x020000D3 RID: 211
		public class Oid : X520.AttributeTypeAndValue
		{
			// Token: 0x0600076A RID: 1898 RVA: 0x00021AAA File Offset: 0x0001FCAA
			public Oid(string oid) : base(oid, int.MaxValue)
			{
			}
		}

		// Token: 0x020000D4 RID: 212
		public class Title : X520.AttributeTypeAndValue
		{
			// Token: 0x0600076B RID: 1899 RVA: 0x00021AB8 File Offset: 0x0001FCB8
			public Title() : base("2.5.4.12", 64)
			{
			}
		}

		// Token: 0x020000D5 RID: 213
		public class CountryName : X520.AttributeTypeAndValue
		{
			// Token: 0x0600076C RID: 1900 RVA: 0x00021AC7 File Offset: 0x0001FCC7
			public CountryName() : base("2.5.4.6", 2, 19)
			{
			}
		}

		// Token: 0x020000D6 RID: 214
		public class DnQualifier : X520.AttributeTypeAndValue
		{
			// Token: 0x0600076D RID: 1901 RVA: 0x00021AD7 File Offset: 0x0001FCD7
			public DnQualifier() : base("2.5.4.46", 2, 19)
			{
			}
		}

		// Token: 0x020000D7 RID: 215
		public class Surname : X520.AttributeTypeAndValue
		{
			// Token: 0x0600076E RID: 1902 RVA: 0x00021AE7 File Offset: 0x0001FCE7
			public Surname() : base("2.5.4.4", 32768)
			{
			}
		}

		// Token: 0x020000D8 RID: 216
		public class GivenName : X520.AttributeTypeAndValue
		{
			// Token: 0x0600076F RID: 1903 RVA: 0x00021AF9 File Offset: 0x0001FCF9
			public GivenName() : base("2.5.4.42", 16)
			{
			}
		}

		// Token: 0x020000D9 RID: 217
		public class Initial : X520.AttributeTypeAndValue
		{
			// Token: 0x06000770 RID: 1904 RVA: 0x00021B08 File Offset: 0x0001FD08
			public Initial() : base("2.5.4.43", 5)
			{
			}
		}
	}
}
