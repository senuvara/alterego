﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace Mono.Security.Interface
{
	// Token: 0x02000088 RID: 136
	public sealed class MonoTlsSettings
	{
		// Token: 0x17000146 RID: 326
		// (get) Token: 0x060004DC RID: 1244 RVA: 0x00016FE3 File Offset: 0x000151E3
		// (set) Token: 0x060004DD RID: 1245 RVA: 0x00016FEB File Offset: 0x000151EB
		public MonoRemoteCertificateValidationCallback RemoteCertificateValidationCallback
		{
			[CompilerGenerated]
			get
			{
				return this.<RemoteCertificateValidationCallback>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<RemoteCertificateValidationCallback>k__BackingField = value;
			}
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x060004DE RID: 1246 RVA: 0x00016FF4 File Offset: 0x000151F4
		// (set) Token: 0x060004DF RID: 1247 RVA: 0x00016FFC File Offset: 0x000151FC
		public MonoLocalCertificateSelectionCallback ClientCertificateSelectionCallback
		{
			[CompilerGenerated]
			get
			{
				return this.<ClientCertificateSelectionCallback>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ClientCertificateSelectionCallback>k__BackingField = value;
			}
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x060004E0 RID: 1248 RVA: 0x00017005 File Offset: 0x00015205
		// (set) Token: 0x060004E1 RID: 1249 RVA: 0x0001700D File Offset: 0x0001520D
		public bool CheckCertificateName
		{
			get
			{
				return this.checkCertName;
			}
			set
			{
				this.checkCertName = value;
			}
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x060004E2 RID: 1250 RVA: 0x00017016 File Offset: 0x00015216
		// (set) Token: 0x060004E3 RID: 1251 RVA: 0x0001701E File Offset: 0x0001521E
		public bool CheckCertificateRevocationStatus
		{
			get
			{
				return this.checkCertRevocationStatus;
			}
			set
			{
				this.checkCertRevocationStatus = value;
			}
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x060004E4 RID: 1252 RVA: 0x00017027 File Offset: 0x00015227
		// (set) Token: 0x060004E5 RID: 1253 RVA: 0x0001702F File Offset: 0x0001522F
		public bool? UseServicePointManagerCallback
		{
			get
			{
				return this.useServicePointManagerCallback;
			}
			set
			{
				this.useServicePointManagerCallback = value;
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x060004E6 RID: 1254 RVA: 0x00017038 File Offset: 0x00015238
		// (set) Token: 0x060004E7 RID: 1255 RVA: 0x00017040 File Offset: 0x00015240
		public bool SkipSystemValidators
		{
			get
			{
				return this.skipSystemValidators;
			}
			set
			{
				this.skipSystemValidators = value;
			}
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x060004E8 RID: 1256 RVA: 0x00017049 File Offset: 0x00015249
		// (set) Token: 0x060004E9 RID: 1257 RVA: 0x00017051 File Offset: 0x00015251
		public bool CallbackNeedsCertificateChain
		{
			get
			{
				return this.callbackNeedsChain;
			}
			set
			{
				this.callbackNeedsChain = value;
			}
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x060004EA RID: 1258 RVA: 0x0001705A File Offset: 0x0001525A
		// (set) Token: 0x060004EB RID: 1259 RVA: 0x00017062 File Offset: 0x00015262
		public DateTime? CertificateValidationTime
		{
			[CompilerGenerated]
			get
			{
				return this.<CertificateValidationTime>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CertificateValidationTime>k__BackingField = value;
			}
		}

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x060004EC RID: 1260 RVA: 0x0001706B File Offset: 0x0001526B
		// (set) Token: 0x060004ED RID: 1261 RVA: 0x00017073 File Offset: 0x00015273
		public X509CertificateCollection TrustAnchors
		{
			[CompilerGenerated]
			get
			{
				return this.<TrustAnchors>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<TrustAnchors>k__BackingField = value;
			}
		}

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x060004EE RID: 1262 RVA: 0x0001707C File Offset: 0x0001527C
		// (set) Token: 0x060004EF RID: 1263 RVA: 0x00017084 File Offset: 0x00015284
		public object UserSettings
		{
			[CompilerGenerated]
			get
			{
				return this.<UserSettings>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<UserSettings>k__BackingField = value;
			}
		}

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x060004F0 RID: 1264 RVA: 0x0001708D File Offset: 0x0001528D
		// (set) Token: 0x060004F1 RID: 1265 RVA: 0x00017095 File Offset: 0x00015295
		internal string[] CertificateSearchPaths
		{
			[CompilerGenerated]
			get
			{
				return this.<CertificateSearchPaths>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CertificateSearchPaths>k__BackingField = value;
			}
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x060004F2 RID: 1266 RVA: 0x0001709E File Offset: 0x0001529E
		// (set) Token: 0x060004F3 RID: 1267 RVA: 0x000170A6 File Offset: 0x000152A6
		internal bool SendCloseNotify
		{
			[CompilerGenerated]
			get
			{
				return this.<SendCloseNotify>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<SendCloseNotify>k__BackingField = value;
			}
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x060004F4 RID: 1268 RVA: 0x000170AF File Offset: 0x000152AF
		// (set) Token: 0x060004F5 RID: 1269 RVA: 0x000170B7 File Offset: 0x000152B7
		public TlsProtocols? EnabledProtocols
		{
			[CompilerGenerated]
			get
			{
				return this.<EnabledProtocols>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<EnabledProtocols>k__BackingField = value;
			}
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060004F6 RID: 1270 RVA: 0x000170C0 File Offset: 0x000152C0
		// (set) Token: 0x060004F7 RID: 1271 RVA: 0x000170C8 File Offset: 0x000152C8
		[CLSCompliant(false)]
		public CipherSuiteCode[] EnabledCiphers
		{
			[CompilerGenerated]
			get
			{
				return this.<EnabledCiphers>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<EnabledCiphers>k__BackingField = value;
			}
		}

		// Token: 0x060004F8 RID: 1272 RVA: 0x000170D1 File Offset: 0x000152D1
		public MonoTlsSettings()
		{
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060004F9 RID: 1273 RVA: 0x000170E7 File Offset: 0x000152E7
		// (set) Token: 0x060004FA RID: 1274 RVA: 0x00017106 File Offset: 0x00015306
		public static MonoTlsSettings DefaultSettings
		{
			get
			{
				if (MonoTlsSettings.defaultSettings == null)
				{
					Interlocked.CompareExchange<MonoTlsSettings>(ref MonoTlsSettings.defaultSettings, new MonoTlsSettings(), null);
				}
				return MonoTlsSettings.defaultSettings;
			}
			set
			{
				MonoTlsSettings.defaultSettings = (value ?? new MonoTlsSettings());
			}
		}

		// Token: 0x060004FB RID: 1275 RVA: 0x00017117 File Offset: 0x00015317
		public static MonoTlsSettings CopyDefaultSettings()
		{
			return MonoTlsSettings.DefaultSettings.Clone();
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060004FC RID: 1276 RVA: 0x00017123 File Offset: 0x00015323
		[Obsolete("Do not use outside System.dll!")]
		public ICertificateValidator CertificateValidator
		{
			get
			{
				return this.certificateValidator;
			}
		}

		// Token: 0x060004FD RID: 1277 RVA: 0x0001712B File Offset: 0x0001532B
		[Obsolete("Do not use outside System.dll!")]
		public MonoTlsSettings CloneWithValidator(ICertificateValidator validator)
		{
			if (this.cloned)
			{
				this.certificateValidator = validator;
				return this;
			}
			return new MonoTlsSettings(this)
			{
				certificateValidator = validator
			};
		}

		// Token: 0x060004FE RID: 1278 RVA: 0x0001714B File Offset: 0x0001534B
		public MonoTlsSettings Clone()
		{
			return new MonoTlsSettings(this);
		}

		// Token: 0x060004FF RID: 1279 RVA: 0x00017154 File Offset: 0x00015354
		private MonoTlsSettings(MonoTlsSettings other)
		{
			this.RemoteCertificateValidationCallback = other.RemoteCertificateValidationCallback;
			this.ClientCertificateSelectionCallback = other.ClientCertificateSelectionCallback;
			this.checkCertName = other.checkCertName;
			this.checkCertRevocationStatus = other.checkCertRevocationStatus;
			this.UseServicePointManagerCallback = other.useServicePointManagerCallback;
			this.skipSystemValidators = other.skipSystemValidators;
			this.callbackNeedsChain = other.callbackNeedsChain;
			this.UserSettings = other.UserSettings;
			this.EnabledProtocols = other.EnabledProtocols;
			this.EnabledCiphers = other.EnabledCiphers;
			this.CertificateValidationTime = other.CertificateValidationTime;
			this.SendCloseNotify = other.SendCloseNotify;
			if (other.TrustAnchors != null)
			{
				this.TrustAnchors = new X509CertificateCollection(other.TrustAnchors);
			}
			if (other.CertificateSearchPaths != null)
			{
				this.CertificateSearchPaths = new string[other.CertificateSearchPaths.Length];
				other.CertificateSearchPaths.CopyTo(this.CertificateSearchPaths, 0);
			}
			this.cloned = true;
		}

		// Token: 0x04000372 RID: 882
		[CompilerGenerated]
		private MonoRemoteCertificateValidationCallback <RemoteCertificateValidationCallback>k__BackingField;

		// Token: 0x04000373 RID: 883
		[CompilerGenerated]
		private MonoLocalCertificateSelectionCallback <ClientCertificateSelectionCallback>k__BackingField;

		// Token: 0x04000374 RID: 884
		[CompilerGenerated]
		private DateTime? <CertificateValidationTime>k__BackingField;

		// Token: 0x04000375 RID: 885
		[CompilerGenerated]
		private X509CertificateCollection <TrustAnchors>k__BackingField;

		// Token: 0x04000376 RID: 886
		[CompilerGenerated]
		private object <UserSettings>k__BackingField;

		// Token: 0x04000377 RID: 887
		[CompilerGenerated]
		private string[] <CertificateSearchPaths>k__BackingField;

		// Token: 0x04000378 RID: 888
		[CompilerGenerated]
		private bool <SendCloseNotify>k__BackingField;

		// Token: 0x04000379 RID: 889
		[CompilerGenerated]
		private TlsProtocols? <EnabledProtocols>k__BackingField;

		// Token: 0x0400037A RID: 890
		[CompilerGenerated]
		private CipherSuiteCode[] <EnabledCiphers>k__BackingField;

		// Token: 0x0400037B RID: 891
		private bool cloned;

		// Token: 0x0400037C RID: 892
		private bool checkCertName = true;

		// Token: 0x0400037D RID: 893
		private bool checkCertRevocationStatus;

		// Token: 0x0400037E RID: 894
		private bool? useServicePointManagerCallback;

		// Token: 0x0400037F RID: 895
		private bool skipSystemValidators;

		// Token: 0x04000380 RID: 896
		private bool callbackNeedsChain = true;

		// Token: 0x04000381 RID: 897
		private ICertificateValidator certificateValidator;

		// Token: 0x04000382 RID: 898
		private static MonoTlsSettings defaultSettings;
	}
}
