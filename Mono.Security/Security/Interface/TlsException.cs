﻿using System;

namespace Mono.Security.Interface
{
	// Token: 0x02000089 RID: 137
	public sealed class TlsException : Exception
	{
		// Token: 0x17000156 RID: 342
		// (get) Token: 0x06000500 RID: 1280 RVA: 0x00017252 File Offset: 0x00015452
		public Alert Alert
		{
			get
			{
				return this.alert;
			}
		}

		// Token: 0x06000501 RID: 1281 RVA: 0x0001725C File Offset: 0x0001545C
		public TlsException(Alert alert) : this(alert, alert.Description.ToString())
		{
		}

		// Token: 0x06000502 RID: 1282 RVA: 0x00017284 File Offset: 0x00015484
		public TlsException(Alert alert, string message) : base(message)
		{
			this.alert = alert;
		}

		// Token: 0x06000503 RID: 1283 RVA: 0x00017294 File Offset: 0x00015494
		public TlsException(AlertLevel level, AlertDescription description) : this(new Alert(level, description))
		{
		}

		// Token: 0x06000504 RID: 1284 RVA: 0x000172A3 File Offset: 0x000154A3
		public TlsException(AlertDescription description) : this(new Alert(description))
		{
		}

		// Token: 0x06000505 RID: 1285 RVA: 0x000172B1 File Offset: 0x000154B1
		public TlsException(AlertDescription description, string message) : this(new Alert(description), message)
		{
		}

		// Token: 0x06000506 RID: 1286 RVA: 0x000172C0 File Offset: 0x000154C0
		public TlsException(AlertDescription description, string format, params object[] args) : this(new Alert(description), string.Format(format, args))
		{
		}

		// Token: 0x04000383 RID: 899
		private Alert alert;
	}
}
