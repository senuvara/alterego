﻿using System;

namespace Mono.Security.Interface
{
	// Token: 0x02000075 RID: 117
	public enum AlertLevel : byte
	{
		// Token: 0x0400021D RID: 541
		Warning = 1,
		// Token: 0x0400021E RID: 542
		Fatal
	}
}
