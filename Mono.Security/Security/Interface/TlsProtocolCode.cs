﻿using System;

namespace Mono.Security.Interface
{
	// Token: 0x0200008A RID: 138
	public enum TlsProtocolCode : short
	{
		// Token: 0x04000385 RID: 901
		Tls10 = 769,
		// Token: 0x04000386 RID: 902
		Tls11,
		// Token: 0x04000387 RID: 903
		Tls12
	}
}
