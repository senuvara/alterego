﻿using System;

namespace Mono.Security.Interface
{
	// Token: 0x0200007E RID: 126
	public enum ExchangeAlgorithmType
	{
		// Token: 0x04000354 RID: 852
		None,
		// Token: 0x04000355 RID: 853
		Dhe,
		// Token: 0x04000356 RID: 854
		Rsa,
		// Token: 0x04000357 RID: 855
		EcDhe
	}
}
