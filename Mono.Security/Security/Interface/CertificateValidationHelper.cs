﻿using System;
using System.IO;
using Mono.Net.Security;

namespace Mono.Security.Interface
{
	// Token: 0x0200007B RID: 123
	public static class CertificateValidationHelper
	{
		// Token: 0x06000472 RID: 1138 RVA: 0x00016E74 File Offset: 0x00015074
		static CertificateValidationHelper()
		{
			if (File.Exists("/System/Library/Frameworks/Security.framework/Security"))
			{
				CertificateValidationHelper.noX509Chain = true;
				CertificateValidationHelper.supportsTrustAnchors = true;
				return;
			}
			CertificateValidationHelper.noX509Chain = false;
			CertificateValidationHelper.supportsTrustAnchors = false;
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x06000473 RID: 1139 RVA: 0x00016E9B File Offset: 0x0001509B
		public static bool SupportsX509Chain
		{
			get
			{
				return !CertificateValidationHelper.noX509Chain;
			}
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06000474 RID: 1140 RVA: 0x00016EA5 File Offset: 0x000150A5
		public static bool SupportsTrustAnchors
		{
			get
			{
				return CertificateValidationHelper.supportsTrustAnchors;
			}
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x00016EAC File Offset: 0x000150AC
		internal static ICertificateValidator2 GetInternalValidator(MonoTlsSettings settings, MonoTlsProvider provider)
		{
			return (ICertificateValidator2)NoReflectionHelper.GetInternalValidator(provider, settings);
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x00016EBA File Offset: 0x000150BA
		[Obsolete("Use GetInternalValidator")]
		internal static ICertificateValidator2 GetDefaultValidator(MonoTlsSettings settings, MonoTlsProvider provider)
		{
			return CertificateValidationHelper.GetInternalValidator(settings, provider);
		}

		// Token: 0x06000477 RID: 1143 RVA: 0x00016EC3 File Offset: 0x000150C3
		public static ICertificateValidator GetValidator(MonoTlsSettings settings)
		{
			return (ICertificateValidator)NoReflectionHelper.GetDefaultValidator(settings);
		}

		// Token: 0x0400023F RID: 575
		private const string SecurityLibrary = "/System/Library/Frameworks/Security.framework/Security";

		// Token: 0x04000240 RID: 576
		private static readonly bool noX509Chain;

		// Token: 0x04000241 RID: 577
		private static readonly bool supportsTrustAnchors;
	}
}
