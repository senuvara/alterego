﻿using System;

namespace Mono.Security.Interface
{
	// Token: 0x0200007C RID: 124
	public enum CipherAlgorithmType
	{
		// Token: 0x04000243 RID: 579
		None,
		// Token: 0x04000244 RID: 580
		Aes128,
		// Token: 0x04000245 RID: 581
		Aes256,
		// Token: 0x04000246 RID: 582
		AesGcm128,
		// Token: 0x04000247 RID: 583
		AesGcm256
	}
}
