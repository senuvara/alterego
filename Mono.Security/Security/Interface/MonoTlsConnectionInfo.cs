﻿using System;
using System.Runtime.CompilerServices;

namespace Mono.Security.Interface
{
	// Token: 0x02000081 RID: 129
	public class MonoTlsConnectionInfo
	{
		// Token: 0x17000137 RID: 311
		// (get) Token: 0x060004AE RID: 1198 RVA: 0x00016ED0 File Offset: 0x000150D0
		// (set) Token: 0x060004AF RID: 1199 RVA: 0x00016ED8 File Offset: 0x000150D8
		[CLSCompliant(false)]
		public CipherSuiteCode CipherSuiteCode
		{
			[CompilerGenerated]
			get
			{
				return this.<CipherSuiteCode>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CipherSuiteCode>k__BackingField = value;
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x060004B0 RID: 1200 RVA: 0x00016EE1 File Offset: 0x000150E1
		// (set) Token: 0x060004B1 RID: 1201 RVA: 0x00016EE9 File Offset: 0x000150E9
		public TlsProtocols ProtocolVersion
		{
			[CompilerGenerated]
			get
			{
				return this.<ProtocolVersion>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ProtocolVersion>k__BackingField = value;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x060004B2 RID: 1202 RVA: 0x00016EF2 File Offset: 0x000150F2
		// (set) Token: 0x060004B3 RID: 1203 RVA: 0x00016EFA File Offset: 0x000150FA
		public CipherAlgorithmType CipherAlgorithmType
		{
			[CompilerGenerated]
			get
			{
				return this.<CipherAlgorithmType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<CipherAlgorithmType>k__BackingField = value;
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x060004B4 RID: 1204 RVA: 0x00016F03 File Offset: 0x00015103
		// (set) Token: 0x060004B5 RID: 1205 RVA: 0x00016F0B File Offset: 0x0001510B
		public HashAlgorithmType HashAlgorithmType
		{
			[CompilerGenerated]
			get
			{
				return this.<HashAlgorithmType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<HashAlgorithmType>k__BackingField = value;
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x060004B6 RID: 1206 RVA: 0x00016F14 File Offset: 0x00015114
		// (set) Token: 0x060004B7 RID: 1207 RVA: 0x00016F1C File Offset: 0x0001511C
		public ExchangeAlgorithmType ExchangeAlgorithmType
		{
			[CompilerGenerated]
			get
			{
				return this.<ExchangeAlgorithmType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ExchangeAlgorithmType>k__BackingField = value;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x060004B8 RID: 1208 RVA: 0x00016F25 File Offset: 0x00015125
		// (set) Token: 0x060004B9 RID: 1209 RVA: 0x00016F2D File Offset: 0x0001512D
		public string PeerDomainName
		{
			[CompilerGenerated]
			get
			{
				return this.<PeerDomainName>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<PeerDomainName>k__BackingField = value;
			}
		}

		// Token: 0x060004BA RID: 1210 RVA: 0x00016F36 File Offset: 0x00015136
		public override string ToString()
		{
			return string.Format("[MonoTlsConnectionInfo: {0}:{1}]", this.ProtocolVersion, this.CipherSuiteCode);
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x00016F58 File Offset: 0x00015158
		public MonoTlsConnectionInfo()
		{
		}

		// Token: 0x04000362 RID: 866
		[CompilerGenerated]
		private CipherSuiteCode <CipherSuiteCode>k__BackingField;

		// Token: 0x04000363 RID: 867
		[CompilerGenerated]
		private TlsProtocols <ProtocolVersion>k__BackingField;

		// Token: 0x04000364 RID: 868
		[CompilerGenerated]
		private CipherAlgorithmType <CipherAlgorithmType>k__BackingField;

		// Token: 0x04000365 RID: 869
		[CompilerGenerated]
		private HashAlgorithmType <HashAlgorithmType>k__BackingField;

		// Token: 0x04000366 RID: 870
		[CompilerGenerated]
		private ExchangeAlgorithmType <ExchangeAlgorithmType>k__BackingField;

		// Token: 0x04000367 RID: 871
		[CompilerGenerated]
		private string <PeerDomainName>k__BackingField;
	}
}
