﻿using System;

namespace Mono.Security.Interface
{
	// Token: 0x02000083 RID: 131
	public enum MonoEncryptionPolicy
	{
		// Token: 0x0400036E RID: 878
		RequireEncryption,
		// Token: 0x0400036F RID: 879
		AllowNoEncryption,
		// Token: 0x04000370 RID: 880
		NoEncryption
	}
}
