﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x0200008E RID: 142
	public sealed class KeyBuilder
	{
		// Token: 0x06000534 RID: 1332 RVA: 0x000185C9 File Offset: 0x000167C9
		private KeyBuilder()
		{
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x06000535 RID: 1333 RVA: 0x000185D1 File Offset: 0x000167D1
		private static RandomNumberGenerator Rng
		{
			get
			{
				if (KeyBuilder.rng == null)
				{
					KeyBuilder.rng = RandomNumberGenerator.Create();
				}
				return KeyBuilder.rng;
			}
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x000185EC File Offset: 0x000167EC
		public static byte[] Key(int size)
		{
			byte[] array = new byte[size];
			KeyBuilder.Rng.GetBytes(array);
			return array;
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x0001860C File Offset: 0x0001680C
		public static byte[] IV(int size)
		{
			byte[] array = new byte[size];
			KeyBuilder.Rng.GetBytes(array);
			return array;
		}

		// Token: 0x0400039A RID: 922
		private static RandomNumberGenerator rng;
	}
}
