﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x02000095 RID: 149
	public abstract class MD2 : HashAlgorithm
	{
		// Token: 0x06000575 RID: 1397 RVA: 0x00019549 File Offset: 0x00017749
		protected MD2()
		{
			this.HashSizeValue = 128;
		}

		// Token: 0x06000576 RID: 1398 RVA: 0x0001955C File Offset: 0x0001775C
		public new static MD2 Create()
		{
			return new MD2Managed();
		}

		// Token: 0x06000577 RID: 1399 RVA: 0x00019564 File Offset: 0x00017764
		public new static MD2 Create(string hashName)
		{
			object obj = CryptoConfig.CreateFromName(hashName);
			if (obj == null)
			{
				obj = new MD2Managed();
			}
			return (MD2)obj;
		}
	}
}
