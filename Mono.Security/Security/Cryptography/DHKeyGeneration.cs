﻿using System;

namespace Mono.Security.Cryptography
{
	// Token: 0x02000090 RID: 144
	public enum DHKeyGeneration
	{
		// Token: 0x040003A0 RID: 928
		Random,
		// Token: 0x040003A1 RID: 929
		Static
	}
}
