﻿using System;
using System.Security.Cryptography;

namespace Mono.Security.Cryptography
{
	// Token: 0x02000097 RID: 151
	public abstract class MD4 : HashAlgorithm
	{
		// Token: 0x0600057F RID: 1407 RVA: 0x00019832 File Offset: 0x00017A32
		protected MD4()
		{
			this.HashSizeValue = 128;
		}

		// Token: 0x06000580 RID: 1408 RVA: 0x00019845 File Offset: 0x00017A45
		public new static MD4 Create()
		{
			return new MD4Managed();
		}

		// Token: 0x06000581 RID: 1409 RVA: 0x0001984C File Offset: 0x00017A4C
		public new static MD4 Create(string hashName)
		{
			object obj = CryptoConfig.CreateFromName(hashName);
			if (obj == null)
			{
				obj = new MD4Managed();
			}
			return (MD4)obj;
		}
	}
}
