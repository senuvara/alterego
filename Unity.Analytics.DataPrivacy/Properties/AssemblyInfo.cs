﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyVersion("0.0.0.0")]
[assembly: InternalsVisibleTo("Unity.Analytics.DataPrivacy.Tests")]
[assembly: InternalsVisibleTo("Unity.Analytics.DataPrivacy.WebRequest.Tests")]
