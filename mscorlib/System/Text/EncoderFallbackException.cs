﻿using System;
using System.Runtime.Serialization;

namespace System.Text
{
	/// <summary>The exception that is thrown when an encoder fallback operation fails. This class cannot be inherited.</summary>
	// Token: 0x0200026A RID: 618
	[Serializable]
	public sealed class EncoderFallbackException : ArgumentException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Text.EncoderFallbackException" /> class.</summary>
		// Token: 0x06001C51 RID: 7249 RVA: 0x00069955 File Offset: 0x00067B55
		public EncoderFallbackException() : base(Environment.GetResourceString("Value does not fall within the expected range."))
		{
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.EncoderFallbackException" /> class. A parameter specifies the error message.</summary>
		/// <param name="message">An error message.</param>
		// Token: 0x06001C52 RID: 7250 RVA: 0x00069972 File Offset: 0x00067B72
		public EncoderFallbackException(string message) : base(message)
		{
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.EncoderFallbackException" /> class. Parameters specify the error message and the inner exception that is the cause of this exception.</summary>
		/// <param name="message">An error message.</param>
		/// <param name="innerException">The exception that caused this exception.</param>
		// Token: 0x06001C53 RID: 7251 RVA: 0x00069986 File Offset: 0x00067B86
		public EncoderFallbackException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2147024809);
		}

		// Token: 0x06001C54 RID: 7252 RVA: 0x00032429 File Offset: 0x00030629
		internal EncoderFallbackException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001C55 RID: 7253 RVA: 0x0006AC2D File Offset: 0x00068E2D
		internal EncoderFallbackException(string message, char charUnknown, int index) : base(message)
		{
			this.charUnknown = charUnknown;
			this.index = index;
		}

		// Token: 0x06001C56 RID: 7254 RVA: 0x0006AC44 File Offset: 0x00068E44
		internal EncoderFallbackException(string message, char charUnknownHigh, char charUnknownLow, int index) : base(message)
		{
			if (!char.IsHighSurrogate(charUnknownHigh))
			{
				throw new ArgumentOutOfRangeException("charUnknownHigh", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					55296,
					56319
				}));
			}
			if (!char.IsLowSurrogate(charUnknownLow))
			{
				throw new ArgumentOutOfRangeException("charUnknownLow", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					56320,
					57343
				}));
			}
			this.charUnknownHigh = charUnknownHigh;
			this.charUnknownLow = charUnknownLow;
			this.index = index;
		}

		/// <summary>Gets the input character that caused the exception.</summary>
		/// <returns>The character that cannot be encoded.</returns>
		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x06001C57 RID: 7255 RVA: 0x0006ACE8 File Offset: 0x00068EE8
		public char CharUnknown
		{
			get
			{
				return this.charUnknown;
			}
		}

		/// <summary>Gets the high component character of the surrogate pair that caused the exception.</summary>
		/// <returns>The high component character of the surrogate pair that cannot be encoded.</returns>
		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x06001C58 RID: 7256 RVA: 0x0006ACF0 File Offset: 0x00068EF0
		public char CharUnknownHigh
		{
			get
			{
				return this.charUnknownHigh;
			}
		}

		/// <summary>Gets the low component character of the surrogate pair that caused the exception.</summary>
		/// <returns>The low component character of the surrogate pair that cannot be encoded.</returns>
		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x06001C59 RID: 7257 RVA: 0x0006ACF8 File Offset: 0x00068EF8
		public char CharUnknownLow
		{
			get
			{
				return this.charUnknownLow;
			}
		}

		/// <summary>Gets the index position in the input buffer of the character that caused the exception.</summary>
		/// <returns>The index position in the input buffer of the character that cannot be encoded.</returns>
		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x06001C5A RID: 7258 RVA: 0x0006AD00 File Offset: 0x00068F00
		public int Index
		{
			get
			{
				return this.index;
			}
		}

		/// <summary>Indicates whether the input that caused the exception is a surrogate pair.</summary>
		/// <returns>
		///     <see langword="true" /> if the input was a surrogate pair; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001C5B RID: 7259 RVA: 0x0006AD08 File Offset: 0x00068F08
		public bool IsUnknownSurrogate()
		{
			return this.charUnknownHigh > '\0';
		}

		// Token: 0x04000FA8 RID: 4008
		private char charUnknown;

		// Token: 0x04000FA9 RID: 4009
		private char charUnknownHigh;

		// Token: 0x04000FAA RID: 4010
		private char charUnknownLow;

		// Token: 0x04000FAB RID: 4011
		private int index;
	}
}
