﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace System.Text
{
	// Token: 0x0200027E RID: 638
	[Serializable]
	internal sealed class SurrogateEncoder : ISerializable, IObjectReference
	{
		// Token: 0x06001DA6 RID: 7590 RVA: 0x0006FDE3 File Offset: 0x0006DFE3
		internal SurrogateEncoder(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.realEncoding = (Encoding)info.GetValue("m_encoding", typeof(Encoding));
		}

		// Token: 0x06001DA7 RID: 7591 RVA: 0x0006FE19 File Offset: 0x0006E019
		[SecurityCritical]
		public object GetRealObject(StreamingContext context)
		{
			return this.realEncoding.GetEncoder();
		}

		// Token: 0x06001DA8 RID: 7592 RVA: 0x0006913C File Offset: 0x0006733C
		[SecurityCritical]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new ArgumentException(Environment.GetResourceString("Internal error in the runtime."));
		}

		// Token: 0x04001033 RID: 4147
		[NonSerialized]
		private Encoding realEncoding;
	}
}
