﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace System.Text
{
	// Token: 0x02000279 RID: 633
	[Serializable]
	internal sealed class MLangCodePageEncoding : ISerializable, IObjectReference
	{
		// Token: 0x06001D3E RID: 7486 RVA: 0x0006DCDC File Offset: 0x0006BEDC
		internal MLangCodePageEncoding(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.m_codePage = (int)info.GetValue("m_codePage", typeof(int));
			try
			{
				this.m_isReadOnly = (bool)info.GetValue("m_isReadOnly", typeof(bool));
				this.encoderFallback = (EncoderFallback)info.GetValue("encoderFallback", typeof(EncoderFallback));
				this.decoderFallback = (DecoderFallback)info.GetValue("decoderFallback", typeof(DecoderFallback));
			}
			catch (SerializationException)
			{
				this.m_deserializedFromEverett = true;
				this.m_isReadOnly = true;
			}
		}

		// Token: 0x06001D3F RID: 7487 RVA: 0x0006DDA0 File Offset: 0x0006BFA0
		[SecurityCritical]
		public object GetRealObject(StreamingContext context)
		{
			this.realEncoding = Encoding.GetEncoding(this.m_codePage);
			if (!this.m_deserializedFromEverett && !this.m_isReadOnly)
			{
				this.realEncoding = (Encoding)this.realEncoding.Clone();
				this.realEncoding.EncoderFallback = this.encoderFallback;
				this.realEncoding.DecoderFallback = this.decoderFallback;
			}
			return this.realEncoding;
		}

		// Token: 0x06001D40 RID: 7488 RVA: 0x0006913C File Offset: 0x0006733C
		[SecurityCritical]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new ArgumentException(Environment.GetResourceString("Internal error in the runtime."));
		}

		// Token: 0x0400101E RID: 4126
		[NonSerialized]
		private int m_codePage;

		// Token: 0x0400101F RID: 4127
		[NonSerialized]
		private bool m_isReadOnly;

		// Token: 0x04001020 RID: 4128
		[NonSerialized]
		private bool m_deserializedFromEverett;

		// Token: 0x04001021 RID: 4129
		[NonSerialized]
		private EncoderFallback encoderFallback;

		// Token: 0x04001022 RID: 4130
		[NonSerialized]
		private DecoderFallback decoderFallback;

		// Token: 0x04001023 RID: 4131
		[NonSerialized]
		private Encoding realEncoding;

		// Token: 0x0200027A RID: 634
		[Serializable]
		internal sealed class MLangEncoder : ISerializable, IObjectReference
		{
			// Token: 0x06001D41 RID: 7489 RVA: 0x0006DE0C File Offset: 0x0006C00C
			internal MLangEncoder(SerializationInfo info, StreamingContext context)
			{
				if (info == null)
				{
					throw new ArgumentNullException("info");
				}
				this.realEncoding = (Encoding)info.GetValue("m_encoding", typeof(Encoding));
			}

			// Token: 0x06001D42 RID: 7490 RVA: 0x0006DE42 File Offset: 0x0006C042
			[SecurityCritical]
			public object GetRealObject(StreamingContext context)
			{
				return this.realEncoding.GetEncoder();
			}

			// Token: 0x06001D43 RID: 7491 RVA: 0x0006913C File Offset: 0x0006733C
			[SecurityCritical]
			void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
			{
				throw new ArgumentException(Environment.GetResourceString("Internal error in the runtime."));
			}

			// Token: 0x04001024 RID: 4132
			[NonSerialized]
			private Encoding realEncoding;
		}

		// Token: 0x0200027B RID: 635
		[Serializable]
		internal sealed class MLangDecoder : ISerializable, IObjectReference
		{
			// Token: 0x06001D44 RID: 7492 RVA: 0x0006DE4F File Offset: 0x0006C04F
			internal MLangDecoder(SerializationInfo info, StreamingContext context)
			{
				if (info == null)
				{
					throw new ArgumentNullException("info");
				}
				this.realEncoding = (Encoding)info.GetValue("m_encoding", typeof(Encoding));
			}

			// Token: 0x06001D45 RID: 7493 RVA: 0x0006DE85 File Offset: 0x0006C085
			[SecurityCritical]
			public object GetRealObject(StreamingContext context)
			{
				return this.realEncoding.GetDecoder();
			}

			// Token: 0x06001D46 RID: 7494 RVA: 0x0006913C File Offset: 0x0006733C
			[SecurityCritical]
			void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
			{
				throw new ArgumentException(Environment.GetResourceString("Internal error in the runtime."));
			}

			// Token: 0x04001025 RID: 4133
			[NonSerialized]
			private Encoding realEncoding;
		}
	}
}
