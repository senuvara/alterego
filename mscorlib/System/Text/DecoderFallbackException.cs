﻿using System;
using System.Runtime.Serialization;

namespace System.Text
{
	/// <summary>The exception that is thrown when a decoder fallback operation fails. This class cannot be inherited.</summary>
	// Token: 0x0200025F RID: 607
	[Serializable]
	public sealed class DecoderFallbackException : ArgumentException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Text.DecoderFallbackException" /> class. </summary>
		// Token: 0x06001BF4 RID: 7156 RVA: 0x00069955 File Offset: 0x00067B55
		public DecoderFallbackException() : base(Environment.GetResourceString("Value does not fall within the expected range."))
		{
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.DecoderFallbackException" /> class. A parameter specifies the error message.</summary>
		/// <param name="message">An error message.</param>
		// Token: 0x06001BF5 RID: 7157 RVA: 0x00069972 File Offset: 0x00067B72
		public DecoderFallbackException(string message) : base(message)
		{
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.DecoderFallbackException" /> class. Parameters specify the error message and the inner exception that is the cause of this exception.</summary>
		/// <param name="message">An error message.</param>
		/// <param name="innerException">The exception that caused this exception.</param>
		// Token: 0x06001BF6 RID: 7158 RVA: 0x00069986 File Offset: 0x00067B86
		public DecoderFallbackException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2147024809);
		}

		// Token: 0x06001BF7 RID: 7159 RVA: 0x00032429 File Offset: 0x00030629
		internal DecoderFallbackException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.DecoderFallbackException" /> class. Parameters specify the error message, the array of bytes being decoded, and the index of the byte that cannot be decoded.</summary>
		/// <param name="message">An error message.</param>
		/// <param name="bytesUnknown">The input byte array.</param>
		/// <param name="index">The index position in <paramref name="bytesUnknown" /> of the byte that cannot be decoded.</param>
		// Token: 0x06001BF8 RID: 7160 RVA: 0x0006999B File Offset: 0x00067B9B
		public DecoderFallbackException(string message, byte[] bytesUnknown, int index) : base(message)
		{
			this.bytesUnknown = bytesUnknown;
			this.index = index;
		}

		/// <summary>Gets the input byte sequence that caused the exception.</summary>
		/// <returns>The input byte array that cannot be decoded. </returns>
		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x06001BF9 RID: 7161 RVA: 0x000699B2 File Offset: 0x00067BB2
		public byte[] BytesUnknown
		{
			get
			{
				return this.bytesUnknown;
			}
		}

		/// <summary>Gets the index position in the input byte sequence of the byte that caused the exception.</summary>
		/// <returns>The index position in the input byte array of the byte that cannot be decoded. The index position is zero-based. </returns>
		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x06001BFA RID: 7162 RVA: 0x000699BA File Offset: 0x00067BBA
		public int Index
		{
			get
			{
				return this.index;
			}
		}

		// Token: 0x04000F8F RID: 3983
		private byte[] bytesUnknown;

		// Token: 0x04000F90 RID: 3984
		private int index;
	}
}
