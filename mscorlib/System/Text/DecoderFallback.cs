﻿using System;
using System.Threading;

namespace System.Text
{
	/// <summary>Provides a failure-handling mechanism, called a fallback, for an encoded input byte sequence that cannot be converted to an output character. </summary>
	// Token: 0x02000260 RID: 608
	[Serializable]
	public abstract class DecoderFallback
	{
		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x06001BFB RID: 7163 RVA: 0x000699C4 File Offset: 0x00067BC4
		private static object InternalSyncObject
		{
			get
			{
				if (DecoderFallback.s_InternalSyncObject == null)
				{
					object value = new object();
					Interlocked.CompareExchange<object>(ref DecoderFallback.s_InternalSyncObject, value, null);
				}
				return DecoderFallback.s_InternalSyncObject;
			}
		}

		/// <summary>Gets an object that outputs a substitute string in place of an input byte sequence that cannot be decoded.</summary>
		/// <returns>A type derived from the <see cref="T:System.Text.DecoderFallback" /> class. The default value is a <see cref="T:System.Text.DecoderReplacementFallback" /> object that emits the QUESTION MARK character ("?", U+003F) in place of unknown byte sequences. </returns>
		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x06001BFC RID: 7164 RVA: 0x000699F0 File Offset: 0x00067BF0
		public static DecoderFallback ReplacementFallback
		{
			get
			{
				if (DecoderFallback.replacementFallback == null)
				{
					object internalSyncObject = DecoderFallback.InternalSyncObject;
					lock (internalSyncObject)
					{
						if (DecoderFallback.replacementFallback == null)
						{
							DecoderFallback.replacementFallback = new DecoderReplacementFallback();
						}
					}
				}
				return DecoderFallback.replacementFallback;
			}
		}

		/// <summary>Gets an object that throws an exception when an input byte sequence cannot be decoded.</summary>
		/// <returns>A type derived from the <see cref="T:System.Text.DecoderFallback" /> class. The default value is a <see cref="T:System.Text.DecoderExceptionFallback" /> object.</returns>
		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x06001BFD RID: 7165 RVA: 0x00069A50 File Offset: 0x00067C50
		public static DecoderFallback ExceptionFallback
		{
			get
			{
				if (DecoderFallback.exceptionFallback == null)
				{
					object internalSyncObject = DecoderFallback.InternalSyncObject;
					lock (internalSyncObject)
					{
						if (DecoderFallback.exceptionFallback == null)
						{
							DecoderFallback.exceptionFallback = new DecoderExceptionFallback();
						}
					}
				}
				return DecoderFallback.exceptionFallback;
			}
		}

		/// <summary>When overridden in a derived class, initializes a new instance of the <see cref="T:System.Text.DecoderFallbackBuffer" /> class. </summary>
		/// <returns>An object that provides a fallback buffer for a decoder.</returns>
		// Token: 0x06001BFE RID: 7166
		public abstract DecoderFallbackBuffer CreateFallbackBuffer();

		/// <summary>When overridden in a derived class, gets the maximum number of characters the current <see cref="T:System.Text.DecoderFallback" /> object can return.</summary>
		/// <returns>The maximum number of characters the current <see cref="T:System.Text.DecoderFallback" /> object can return.</returns>
		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x06001BFF RID: 7167
		public abstract int MaxCharCount { get; }

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x06001C00 RID: 7168 RVA: 0x00069AB0 File Offset: 0x00067CB0
		internal bool IsMicrosoftBestFitFallback
		{
			get
			{
				return this.bIsMicrosoftBestFitFallback;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.DecoderFallback" /> class. </summary>
		// Token: 0x06001C01 RID: 7169 RVA: 0x00002050 File Offset: 0x00000250
		protected DecoderFallback()
		{
		}

		// Token: 0x04000F91 RID: 3985
		internal bool bIsMicrosoftBestFitFallback;

		// Token: 0x04000F92 RID: 3986
		private static volatile DecoderFallback replacementFallback;

		// Token: 0x04000F93 RID: 3987
		private static volatile DecoderFallback exceptionFallback;

		// Token: 0x04000F94 RID: 3988
		private static object s_InternalSyncObject;
	}
}
