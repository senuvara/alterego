﻿using System;
using Unity;

namespace System.Text
{
	/// <summary>Provides basic information about an encoding.</summary>
	// Token: 0x02000275 RID: 629
	[Serializable]
	public sealed class EncodingInfo
	{
		// Token: 0x06001D10 RID: 7440 RVA: 0x0006CF95 File Offset: 0x0006B195
		internal EncodingInfo(int codePage, string name, string displayName)
		{
			this.iCodePage = codePage;
			this.strEncodingName = name;
			this.strDisplayName = displayName;
		}

		/// <summary>Gets the code page identifier of the encoding.</summary>
		/// <returns>The code page identifier of the encoding.</returns>
		// Token: 0x17000412 RID: 1042
		// (get) Token: 0x06001D11 RID: 7441 RVA: 0x0006CFB2 File Offset: 0x0006B1B2
		public int CodePage
		{
			get
			{
				return this.iCodePage;
			}
		}

		/// <summary>Gets the name registered with the Internet Assigned Numbers Authority (IANA) for the encoding.</summary>
		/// <returns>The IANA name for the encoding. For more information about the IANA, see www.iana.org.</returns>
		// Token: 0x17000413 RID: 1043
		// (get) Token: 0x06001D12 RID: 7442 RVA: 0x0006CFBA File Offset: 0x0006B1BA
		public string Name
		{
			get
			{
				return this.strEncodingName;
			}
		}

		/// <summary>Gets the human-readable description of the encoding.</summary>
		/// <returns>The human-readable description of the encoding.</returns>
		// Token: 0x17000414 RID: 1044
		// (get) Token: 0x06001D13 RID: 7443 RVA: 0x0006CFC2 File Offset: 0x0006B1C2
		public string DisplayName
		{
			get
			{
				return this.strDisplayName;
			}
		}

		/// <summary>Returns a <see cref="T:System.Text.Encoding" /> object that corresponds to the current <see cref="T:System.Text.EncodingInfo" /> object.</summary>
		/// <returns>A <see cref="T:System.Text.Encoding" /> object that corresponds to the current <see cref="T:System.Text.EncodingInfo" /> object.</returns>
		// Token: 0x06001D14 RID: 7444 RVA: 0x0006CFCA File Offset: 0x0006B1CA
		public Encoding GetEncoding()
		{
			return Encoding.GetEncoding(this.iCodePage);
		}

		/// <summary>Gets a value indicating whether the specified object is equal to the current <see cref="T:System.Text.EncodingInfo" /> object.</summary>
		/// <param name="value">An object to compare to the current <see cref="T:System.Text.EncodingInfo" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="value" /> is a <see cref="T:System.Text.EncodingInfo" /> object and is equal to the current <see cref="T:System.Text.EncodingInfo" /> object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001D15 RID: 7445 RVA: 0x0006CFD8 File Offset: 0x0006B1D8
		public override bool Equals(object value)
		{
			EncodingInfo encodingInfo = value as EncodingInfo;
			return encodingInfo != null && this.CodePage == encodingInfo.CodePage;
		}

		/// <summary>Returns the hash code for the current <see cref="T:System.Text.EncodingInfo" /> object.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06001D16 RID: 7446 RVA: 0x0006CFFF File Offset: 0x0006B1FF
		public override int GetHashCode()
		{
			return this.CodePage;
		}

		// Token: 0x06001D17 RID: 7447 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal EncodingInfo()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001018 RID: 4120
		private int iCodePage;

		// Token: 0x04001019 RID: 4121
		private string strEncodingName;

		// Token: 0x0400101A RID: 4122
		private string strDisplayName;
	}
}
