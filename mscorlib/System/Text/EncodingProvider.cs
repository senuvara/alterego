﻿using System;
using System.Runtime.InteropServices;

namespace System.Text
{
	/// <summary>Provides the base class for an encoding provider, which supplies encodings that are unavailable on a particular platform. </summary>
	// Token: 0x02000277 RID: 631
	[ComVisible(true)]
	public abstract class EncodingProvider
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Text.EncodingProvider" /> class. </summary>
		// Token: 0x06001D26 RID: 7462 RVA: 0x00002050 File Offset: 0x00000250
		public EncodingProvider()
		{
		}

		/// <summary>Returns the encoding with the specified name. </summary>
		/// <param name="name">The name of the requested encoding. </param>
		/// <returns>The encoding that is associated with the specified name, or <see langword="null" /> if this <see cref="T:System.Text.EncodingProvider" /> cannot return a valid encoding that corresponds to <paramref name="name" />.</returns>
		// Token: 0x06001D27 RID: 7463
		public abstract Encoding GetEncoding(string name);

		/// <summary>Returns the encoding associated with the specified code page identifier. </summary>
		/// <param name="codepage">The code page identifier of the requested encoding. </param>
		/// <returns>The encoding that is associated with the specified code page, or <see langword="null" /> if this <see cref="T:System.Text.EncodingProvider" /> cannot return a valid encoding that corresponds to <paramref name="codepage" />. </returns>
		// Token: 0x06001D28 RID: 7464
		public abstract Encoding GetEncoding(int codepage);

		/// <summary>Returns the encoding associated with the specified name. Parameters specify an error handler for characters that cannot be encoded and byte sequences that cannot be decoded. </summary>
		/// <param name="name">The name of the preferred encoding. </param>
		/// <param name="encoderFallback">An object that provides an error-handling procedure when a character cannot be encoded with this encoding. </param>
		/// <param name="decoderFallback">An object that provides an error-handling procedure when a byte sequence cannot be decoded with the current encoding. </param>
		/// <returns>The encoding that is associated with the specified name, or <see langword="null" /> if this <see cref="T:System.Text.EncodingProvider" /> cannot return a valid encoding that corresponds to <paramref name="name" />. </returns>
		// Token: 0x06001D29 RID: 7465 RVA: 0x0006D5DC File Offset: 0x0006B7DC
		public virtual Encoding GetEncoding(string name, EncoderFallback encoderFallback, DecoderFallback decoderFallback)
		{
			Encoding encoding = this.GetEncoding(name);
			if (encoding != null)
			{
				encoding = (Encoding)this.GetEncoding(name).Clone();
				encoding.EncoderFallback = encoderFallback;
				encoding.DecoderFallback = decoderFallback;
			}
			return encoding;
		}

		/// <summary>Returns the encoding associated with the specified code page identifier. Parameters specify an error handler for characters that cannot be encoded and byte sequences that cannot be decoded. </summary>
		/// <param name="codepage">The code page identifier of the requested encoding. </param>
		/// <param name="encoderFallback">An object that provides an error-handling procedure when a character cannot be encoded with this encoding. </param>
		/// <param name="decoderFallback">An object that provides an error-handling procedure when a byte sequence cannot be decoded with this encoding. </param>
		/// <returns>The encoding that is associated with the specified code page, or <see langword="null" /> if this <see cref="T:System.Text.EncodingProvider" /> cannot return a valid encoding that corresponds to <paramref name="codepage" />. </returns>
		// Token: 0x06001D2A RID: 7466 RVA: 0x0006D618 File Offset: 0x0006B818
		public virtual Encoding GetEncoding(int codepage, EncoderFallback encoderFallback, DecoderFallback decoderFallback)
		{
			Encoding encoding = this.GetEncoding(codepage);
			if (encoding != null)
			{
				encoding = (Encoding)this.GetEncoding(codepage).Clone();
				encoding.EncoderFallback = encoderFallback;
				encoding.DecoderFallback = decoderFallback;
			}
			return encoding;
		}

		// Token: 0x06001D2B RID: 7467 RVA: 0x0006D654 File Offset: 0x0006B854
		internal static void AddProvider(EncodingProvider provider)
		{
			if (provider == null)
			{
				throw new ArgumentNullException("provider");
			}
			object obj = EncodingProvider.s_InternalSyncObject;
			lock (obj)
			{
				if (EncodingProvider.s_providers == null)
				{
					EncodingProvider.s_providers = new EncodingProvider[]
					{
						provider
					};
				}
				else if (Array.IndexOf<EncodingProvider>(EncodingProvider.s_providers, provider) < 0)
				{
					EncodingProvider[] array = new EncodingProvider[EncodingProvider.s_providers.Length + 1];
					Array.Copy(EncodingProvider.s_providers, array, EncodingProvider.s_providers.Length);
					array[array.Length - 1] = provider;
					EncodingProvider.s_providers = array;
				}
			}
		}

		// Token: 0x06001D2C RID: 7468 RVA: 0x0006D700 File Offset: 0x0006B900
		internal static Encoding GetEncodingFromProvider(int codepage)
		{
			if (EncodingProvider.s_providers == null)
			{
				return null;
			}
			EncodingProvider[] array = EncodingProvider.s_providers;
			for (int i = 0; i < array.Length; i++)
			{
				Encoding encoding = array[i].GetEncoding(codepage);
				if (encoding != null)
				{
					return encoding;
				}
			}
			return null;
		}

		// Token: 0x06001D2D RID: 7469 RVA: 0x0006D740 File Offset: 0x0006B940
		internal static Encoding GetEncodingFromProvider(string encodingName)
		{
			if (EncodingProvider.s_providers == null)
			{
				return null;
			}
			EncodingProvider[] array = EncodingProvider.s_providers;
			for (int i = 0; i < array.Length; i++)
			{
				Encoding encoding = array[i].GetEncoding(encodingName);
				if (encoding != null)
				{
					return encoding;
				}
			}
			return null;
		}

		// Token: 0x06001D2E RID: 7470 RVA: 0x0006D780 File Offset: 0x0006B980
		internal static Encoding GetEncodingFromProvider(int codepage, EncoderFallback enc, DecoderFallback dec)
		{
			if (EncodingProvider.s_providers == null)
			{
				return null;
			}
			EncodingProvider[] array = EncodingProvider.s_providers;
			for (int i = 0; i < array.Length; i++)
			{
				Encoding encoding = array[i].GetEncoding(codepage, enc, dec);
				if (encoding != null)
				{
					return encoding;
				}
			}
			return null;
		}

		// Token: 0x06001D2F RID: 7471 RVA: 0x0006D7C0 File Offset: 0x0006B9C0
		internal static Encoding GetEncodingFromProvider(string encodingName, EncoderFallback enc, DecoderFallback dec)
		{
			if (EncodingProvider.s_providers == null)
			{
				return null;
			}
			EncodingProvider[] array = EncodingProvider.s_providers;
			for (int i = 0; i < array.Length; i++)
			{
				Encoding encoding = array[i].GetEncoding(encodingName, enc, dec);
				if (encoding != null)
				{
					return encoding;
				}
			}
			return null;
		}

		// Token: 0x06001D30 RID: 7472 RVA: 0x0006D800 File Offset: 0x0006BA00
		// Note: this type is marked as 'beforefieldinit'.
		static EncodingProvider()
		{
		}

		// Token: 0x0400101B RID: 4123
		private static object s_InternalSyncObject = new object();

		// Token: 0x0400101C RID: 4124
		private static volatile EncodingProvider[] s_providers;
	}
}
