﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace System.Text
{
	// Token: 0x02000258 RID: 600
	[Serializable]
	internal sealed class CodePageEncoding : ISerializable, IObjectReference
	{
		// Token: 0x06001BC6 RID: 7110 RVA: 0x0006900C File Offset: 0x0006720C
		internal CodePageEncoding(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.m_codePage = (int)info.GetValue("m_codePage", typeof(int));
			try
			{
				this.m_isReadOnly = (bool)info.GetValue("m_isReadOnly", typeof(bool));
				this.encoderFallback = (EncoderFallback)info.GetValue("encoderFallback", typeof(EncoderFallback));
				this.decoderFallback = (DecoderFallback)info.GetValue("decoderFallback", typeof(DecoderFallback));
			}
			catch (SerializationException)
			{
				this.m_deserializedFromEverett = true;
				this.m_isReadOnly = true;
			}
		}

		// Token: 0x06001BC7 RID: 7111 RVA: 0x000690D0 File Offset: 0x000672D0
		[SecurityCritical]
		public object GetRealObject(StreamingContext context)
		{
			this.realEncoding = Encoding.GetEncoding(this.m_codePage);
			if (!this.m_deserializedFromEverett && !this.m_isReadOnly)
			{
				this.realEncoding = (Encoding)this.realEncoding.Clone();
				this.realEncoding.EncoderFallback = this.encoderFallback;
				this.realEncoding.DecoderFallback = this.decoderFallback;
			}
			return this.realEncoding;
		}

		// Token: 0x06001BC8 RID: 7112 RVA: 0x0006913C File Offset: 0x0006733C
		[SecurityCritical]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new ArgumentException(Environment.GetResourceString("Internal error in the runtime."));
		}

		// Token: 0x04000F7E RID: 3966
		[NonSerialized]
		private int m_codePage;

		// Token: 0x04000F7F RID: 3967
		[NonSerialized]
		private bool m_isReadOnly;

		// Token: 0x04000F80 RID: 3968
		[NonSerialized]
		private bool m_deserializedFromEverett;

		// Token: 0x04000F81 RID: 3969
		[NonSerialized]
		private EncoderFallback encoderFallback;

		// Token: 0x04000F82 RID: 3970
		[NonSerialized]
		private DecoderFallback decoderFallback;

		// Token: 0x04000F83 RID: 3971
		[NonSerialized]
		private Encoding realEncoding;

		// Token: 0x02000259 RID: 601
		[Serializable]
		internal sealed class Decoder : ISerializable, IObjectReference
		{
			// Token: 0x06001BC9 RID: 7113 RVA: 0x0006914D File Offset: 0x0006734D
			internal Decoder(SerializationInfo info, StreamingContext context)
			{
				if (info == null)
				{
					throw new ArgumentNullException("info");
				}
				this.realEncoding = (Encoding)info.GetValue("encoding", typeof(Encoding));
			}

			// Token: 0x06001BCA RID: 7114 RVA: 0x00069183 File Offset: 0x00067383
			[SecurityCritical]
			public object GetRealObject(StreamingContext context)
			{
				return this.realEncoding.GetDecoder();
			}

			// Token: 0x06001BCB RID: 7115 RVA: 0x0006913C File Offset: 0x0006733C
			[SecurityCritical]
			void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
			{
				throw new ArgumentException(Environment.GetResourceString("Internal error in the runtime."));
			}

			// Token: 0x04000F84 RID: 3972
			[NonSerialized]
			private Encoding realEncoding;
		}
	}
}
