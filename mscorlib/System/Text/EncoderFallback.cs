﻿using System;
using System.Threading;

namespace System.Text
{
	/// <summary>Provides a failure-handling mechanism, called a fallback, for an input character that cannot be converted to an encoded output byte sequence. </summary>
	// Token: 0x0200026B RID: 619
	[Serializable]
	public abstract class EncoderFallback
	{
		// Token: 0x170003EA RID: 1002
		// (get) Token: 0x06001C5C RID: 7260 RVA: 0x0006AD14 File Offset: 0x00068F14
		private static object InternalSyncObject
		{
			get
			{
				if (EncoderFallback.s_InternalSyncObject == null)
				{
					object value = new object();
					Interlocked.CompareExchange<object>(ref EncoderFallback.s_InternalSyncObject, value, null);
				}
				return EncoderFallback.s_InternalSyncObject;
			}
		}

		/// <summary>Gets an object that outputs a substitute string in place of an input character that cannot be encoded.</summary>
		/// <returns>A type derived from the <see cref="T:System.Text.EncoderFallback" /> class. The default value is a <see cref="T:System.Text.EncoderReplacementFallback" /> object that replaces unknown input characters with the QUESTION MARK character ("?", U+003F).</returns>
		// Token: 0x170003EB RID: 1003
		// (get) Token: 0x06001C5D RID: 7261 RVA: 0x0006AD40 File Offset: 0x00068F40
		public static EncoderFallback ReplacementFallback
		{
			get
			{
				if (EncoderFallback.replacementFallback == null)
				{
					object internalSyncObject = EncoderFallback.InternalSyncObject;
					lock (internalSyncObject)
					{
						if (EncoderFallback.replacementFallback == null)
						{
							EncoderFallback.replacementFallback = new EncoderReplacementFallback();
						}
					}
				}
				return EncoderFallback.replacementFallback;
			}
		}

		/// <summary>Gets an object that throws an exception when an input character cannot be encoded.</summary>
		/// <returns>A type derived from the <see cref="T:System.Text.EncoderFallback" /> class. The default value is a <see cref="T:System.Text.EncoderExceptionFallback" /> object.</returns>
		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x06001C5E RID: 7262 RVA: 0x0006ADA0 File Offset: 0x00068FA0
		public static EncoderFallback ExceptionFallback
		{
			get
			{
				if (EncoderFallback.exceptionFallback == null)
				{
					object internalSyncObject = EncoderFallback.InternalSyncObject;
					lock (internalSyncObject)
					{
						if (EncoderFallback.exceptionFallback == null)
						{
							EncoderFallback.exceptionFallback = new EncoderExceptionFallback();
						}
					}
				}
				return EncoderFallback.exceptionFallback;
			}
		}

		/// <summary>When overridden in a derived class, initializes a new instance of the <see cref="T:System.Text.EncoderFallbackBuffer" /> class. </summary>
		/// <returns>An object that provides a fallback buffer for an encoder.</returns>
		// Token: 0x06001C5F RID: 7263
		public abstract EncoderFallbackBuffer CreateFallbackBuffer();

		/// <summary>When overridden in a derived class, gets the maximum number of characters the current <see cref="T:System.Text.EncoderFallback" /> object can return.</summary>
		/// <returns>The maximum number of characters the current <see cref="T:System.Text.EncoderFallback" /> object can return.</returns>
		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x06001C60 RID: 7264
		public abstract int MaxCharCount { get; }

		/// <summary>Initializes a new instance of the <see cref="T:System.Text.EncoderFallback" /> class.</summary>
		// Token: 0x06001C61 RID: 7265 RVA: 0x00002050 File Offset: 0x00000250
		protected EncoderFallback()
		{
		}

		// Token: 0x04000FAC RID: 4012
		internal bool bIsMicrosoftBestFitFallback;

		// Token: 0x04000FAD RID: 4013
		private static volatile EncoderFallback replacementFallback;

		// Token: 0x04000FAE RID: 4014
		private static volatile EncoderFallback exceptionFallback;

		// Token: 0x04000FAF RID: 4015
		private static object s_InternalSyncObject;
	}
}
