﻿using System;

namespace System.Text
{
	// Token: 0x0200027D RID: 637
	internal static class StringBuilderCache
	{
		// Token: 0x06001DA3 RID: 7587 RVA: 0x0006FD84 File Offset: 0x0006DF84
		public static StringBuilder Acquire(int capacity = 16)
		{
			if (capacity <= 360)
			{
				StringBuilder cachedInstance = StringBuilderCache.CachedInstance;
				if (cachedInstance != null && capacity <= cachedInstance.Capacity)
				{
					StringBuilderCache.CachedInstance = null;
					cachedInstance.Clear();
					return cachedInstance;
				}
			}
			return new StringBuilder(capacity);
		}

		// Token: 0x06001DA4 RID: 7588 RVA: 0x0006FDC0 File Offset: 0x0006DFC0
		public static void Release(StringBuilder sb)
		{
			if (sb.Capacity <= 360)
			{
				StringBuilderCache.CachedInstance = sb;
			}
		}

		// Token: 0x06001DA5 RID: 7589 RVA: 0x0006FDD5 File Offset: 0x0006DFD5
		public static string GetStringAndRelease(StringBuilder sb)
		{
			string result = sb.ToString();
			StringBuilderCache.Release(sb);
			return result;
		}

		// Token: 0x04001031 RID: 4145
		private const int MAX_BUILDER_SIZE = 360;

		// Token: 0x04001032 RID: 4146
		[ThreadStatic]
		private static StringBuilder CachedInstance;
	}
}
