﻿using System;

namespace System.Text
{
	// Token: 0x02000266 RID: 614
	[Serializable]
	internal class InternalEncoderBestFitFallback : EncoderFallback
	{
		// Token: 0x06001C38 RID: 7224 RVA: 0x0006A80B File Offset: 0x00068A0B
		internal InternalEncoderBestFitFallback(Encoding encoding)
		{
			this.encoding = encoding;
			this.bIsMicrosoftBestFitFallback = true;
		}

		// Token: 0x06001C39 RID: 7225 RVA: 0x0006A821 File Offset: 0x00068A21
		public override EncoderFallbackBuffer CreateFallbackBuffer()
		{
			return new InternalEncoderBestFitFallbackBuffer(this);
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x06001C3A RID: 7226 RVA: 0x00004E08 File Offset: 0x00003008
		public override int MaxCharCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001C3B RID: 7227 RVA: 0x0006A82C File Offset: 0x00068A2C
		public override bool Equals(object value)
		{
			InternalEncoderBestFitFallback internalEncoderBestFitFallback = value as InternalEncoderBestFitFallback;
			return internalEncoderBestFitFallback != null && this.encoding.CodePage == internalEncoderBestFitFallback.encoding.CodePage;
		}

		// Token: 0x06001C3C RID: 7228 RVA: 0x0006A85D File Offset: 0x00068A5D
		public override int GetHashCode()
		{
			return this.encoding.CodePage;
		}

		// Token: 0x04000FA1 RID: 4001
		internal Encoding encoding;

		// Token: 0x04000FA2 RID: 4002
		internal char[] arrayBestFit;
	}
}
