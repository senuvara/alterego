﻿using System;

namespace System.Text
{
	// Token: 0x0200025B RID: 603
	[Serializable]
	internal sealed class InternalDecoderBestFitFallback : DecoderFallback
	{
		// Token: 0x06001BDB RID: 7131 RVA: 0x000695C3 File Offset: 0x000677C3
		internal InternalDecoderBestFitFallback(Encoding encoding)
		{
			this.encoding = encoding;
			this.bIsMicrosoftBestFitFallback = true;
		}

		// Token: 0x06001BDC RID: 7132 RVA: 0x000695E1 File Offset: 0x000677E1
		public override DecoderFallbackBuffer CreateFallbackBuffer()
		{
			return new InternalDecoderBestFitFallbackBuffer(this);
		}

		// Token: 0x170003CC RID: 972
		// (get) Token: 0x06001BDD RID: 7133 RVA: 0x00004E08 File Offset: 0x00003008
		public override int MaxCharCount
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001BDE RID: 7134 RVA: 0x000695EC File Offset: 0x000677EC
		public override bool Equals(object value)
		{
			InternalDecoderBestFitFallback internalDecoderBestFitFallback = value as InternalDecoderBestFitFallback;
			return internalDecoderBestFitFallback != null && this.encoding.CodePage == internalDecoderBestFitFallback.encoding.CodePage;
		}

		// Token: 0x06001BDF RID: 7135 RVA: 0x0006961D File Offset: 0x0006781D
		public override int GetHashCode()
		{
			return this.encoding.CodePage;
		}

		// Token: 0x04000F87 RID: 3975
		internal Encoding encoding;

		// Token: 0x04000F88 RID: 3976
		internal char[] arrayBestFit;

		// Token: 0x04000F89 RID: 3977
		internal char cReplacement = '?';
	}
}
