﻿using System;

namespace System.Text
{
	// Token: 0x0200028B RID: 651
	internal enum NormalizationCheck
	{
		// Token: 0x04001059 RID: 4185
		Yes,
		// Token: 0x0400105A RID: 4186
		No,
		// Token: 0x0400105B RID: 4187
		Maybe
	}
}
