﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x020000C6 RID: 198
	internal interface IValueTupleInternal : ITuple
	{
		// Token: 0x0600071A RID: 1818
		int GetHashCode(IEqualityComparer comparer);

		// Token: 0x0600071B RID: 1819
		string ToStringEnd();
	}
}
