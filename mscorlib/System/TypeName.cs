﻿using System;

namespace System
{
	// Token: 0x02000232 RID: 562
	internal interface TypeName : IEquatable<TypeName>
	{
		// Token: 0x1700039A RID: 922
		// (get) Token: 0x06001AD5 RID: 6869
		string DisplayName { get; }

		// Token: 0x06001AD6 RID: 6870
		TypeName NestedName(TypeIdentifier innerName);
	}
}
