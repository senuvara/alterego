﻿using System;

namespace System.Threading
{
	// Token: 0x0200047A RID: 1146
	internal static class TimeoutHelper
	{
		// Token: 0x060033BB RID: 13243 RVA: 0x000B865B File Offset: 0x000B685B
		public static uint GetTime()
		{
			return (uint)Environment.TickCount;
		}

		// Token: 0x060033BC RID: 13244 RVA: 0x000B8664 File Offset: 0x000B6864
		public static int UpdateTimeOut(uint startTime, int originalWaitMillisecondsTimeout)
		{
			uint num = TimeoutHelper.GetTime() - startTime;
			if (num > 2147483647U)
			{
				return 0;
			}
			int num2 = originalWaitMillisecondsTimeout - (int)num;
			if (num2 <= 0)
			{
				return 0;
			}
			return num2;
		}
	}
}
