﻿using System;

namespace System.Threading
{
	// Token: 0x0200049A RID: 1178
	// (Invoke) Token: 0x0600348D RID: 13453
	internal delegate object InternalCrossContextDelegate(object[] args);
}
