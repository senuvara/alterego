﻿using System;
using System.Runtime.Serialization;

namespace System.Threading
{
	/// <summary>The exception that is thrown when a failure occurs in a managed thread after the underlying operating system thread has been started, but before the thread is ready to execute user code.</summary>
	// Token: 0x020004B1 RID: 1201
	[Serializable]
	public sealed class ThreadStartException : SystemException
	{
		// Token: 0x0600359E RID: 13726 RVA: 0x000BC6C3 File Offset: 0x000BA8C3
		private ThreadStartException() : base(Environment.GetResourceString("Thread failed to start."))
		{
			base.SetErrorCode(-2146233051);
		}

		// Token: 0x0600359F RID: 13727 RVA: 0x000BC6E0 File Offset: 0x000BA8E0
		private ThreadStartException(Exception reason) : base(Environment.GetResourceString("Thread failed to start."), reason)
		{
			base.SetErrorCode(-2146233051);
		}

		// Token: 0x060035A0 RID: 13728 RVA: 0x000319C9 File Offset: 0x0002FBC9
		internal ThreadStartException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
