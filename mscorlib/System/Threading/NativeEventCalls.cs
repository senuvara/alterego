﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.Win32.SafeHandles;

namespace System.Threading
{
	// Token: 0x020004C0 RID: 1216
	internal static class NativeEventCalls
	{
		// Token: 0x0600361B RID: 13851
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr CreateEvent_internal(bool manual, bool initial, string name, out int errorCode);

		// Token: 0x0600361C RID: 13852 RVA: 0x000BD29C File Offset: 0x000BB49C
		public static bool SetEvent(SafeWaitHandle handle)
		{
			bool flag = false;
			bool result;
			try
			{
				handle.DangerousAddRef(ref flag);
				result = NativeEventCalls.SetEvent_internal(handle.DangerousGetHandle());
			}
			finally
			{
				if (flag)
				{
					handle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x0600361D RID: 13853
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SetEvent_internal(IntPtr handle);

		// Token: 0x0600361E RID: 13854 RVA: 0x000BD2DC File Offset: 0x000BB4DC
		public static bool ResetEvent(SafeWaitHandle handle)
		{
			bool flag = false;
			bool result;
			try
			{
				handle.DangerousAddRef(ref flag);
				result = NativeEventCalls.ResetEvent_internal(handle.DangerousGetHandle());
			}
			finally
			{
				if (flag)
				{
					handle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x0600361F RID: 13855
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ResetEvent_internal(IntPtr handle);

		// Token: 0x06003620 RID: 13856
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void CloseEvent_internal(IntPtr handle);
	}
}
