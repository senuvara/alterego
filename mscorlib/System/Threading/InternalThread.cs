﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020004C5 RID: 1221
	[StructLayout(LayoutKind.Sequential)]
	internal sealed class InternalThread : CriticalFinalizerObject
	{
		// Token: 0x06003651 RID: 13905
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Thread_free_internal();

		// Token: 0x06003652 RID: 13906 RVA: 0x000BDF34 File Offset: 0x000BC134
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		~InternalThread()
		{
			this.Thread_free_internal();
		}

		// Token: 0x06003653 RID: 13907 RVA: 0x000BDF60 File Offset: 0x000BC160
		public InternalThread()
		{
		}

		// Token: 0x04001C8F RID: 7311
		private int lock_thread_id;

		// Token: 0x04001C90 RID: 7312
		private IntPtr handle;

		// Token: 0x04001C91 RID: 7313
		private IntPtr native_handle;

		// Token: 0x04001C92 RID: 7314
		private IntPtr unused3;

		// Token: 0x04001C93 RID: 7315
		private IntPtr name;

		// Token: 0x04001C94 RID: 7316
		private int name_len;

		// Token: 0x04001C95 RID: 7317
		private ThreadState state;

		// Token: 0x04001C96 RID: 7318
		private object abort_exc;

		// Token: 0x04001C97 RID: 7319
		private int abort_state_handle;

		// Token: 0x04001C98 RID: 7320
		internal long thread_id;

		// Token: 0x04001C99 RID: 7321
		private IntPtr debugger_thread;

		// Token: 0x04001C9A RID: 7322
		private UIntPtr static_data;

		// Token: 0x04001C9B RID: 7323
		private IntPtr runtime_thread_info;

		// Token: 0x04001C9C RID: 7324
		private object current_appcontext;

		// Token: 0x04001C9D RID: 7325
		private object root_domain_thread;

		// Token: 0x04001C9E RID: 7326
		internal byte[] _serialized_principal;

		// Token: 0x04001C9F RID: 7327
		internal int _serialized_principal_version;

		// Token: 0x04001CA0 RID: 7328
		private IntPtr appdomain_refs;

		// Token: 0x04001CA1 RID: 7329
		private int interruption_requested;

		// Token: 0x04001CA2 RID: 7330
		private IntPtr synch_cs;

		// Token: 0x04001CA3 RID: 7331
		internal bool threadpool_thread;

		// Token: 0x04001CA4 RID: 7332
		private bool thread_interrupt_requested;

		// Token: 0x04001CA5 RID: 7333
		internal int stack_size;

		// Token: 0x04001CA6 RID: 7334
		internal byte apartment_state;

		// Token: 0x04001CA7 RID: 7335
		internal volatile int critical_region_level;

		// Token: 0x04001CA8 RID: 7336
		internal int managed_id;

		// Token: 0x04001CA9 RID: 7337
		private int small_id;

		// Token: 0x04001CAA RID: 7338
		private IntPtr manage_callback;

		// Token: 0x04001CAB RID: 7339
		private IntPtr unused4;

		// Token: 0x04001CAC RID: 7340
		private IntPtr flags;

		// Token: 0x04001CAD RID: 7341
		private IntPtr thread_pinning_ref;

		// Token: 0x04001CAE RID: 7342
		private IntPtr abort_protected_block_count;

		// Token: 0x04001CAF RID: 7343
		private int priority = 2;

		// Token: 0x04001CB0 RID: 7344
		private IntPtr owned_mutex;

		// Token: 0x04001CB1 RID: 7345
		private IntPtr suspended_event;

		// Token: 0x04001CB2 RID: 7346
		private int self_suspended;

		// Token: 0x04001CB3 RID: 7347
		private IntPtr unused1;

		// Token: 0x04001CB4 RID: 7348
		private IntPtr unused2;

		// Token: 0x04001CB5 RID: 7349
		private IntPtr last;
	}
}
