﻿using System;

namespace System.Threading
{
	// Token: 0x02000469 RID: 1129
	internal struct CancellationCallbackCoreWorkArguments
	{
		// Token: 0x06003331 RID: 13105 RVA: 0x000B646C File Offset: 0x000B466C
		public CancellationCallbackCoreWorkArguments(SparselyPopulatedArrayFragment<CancellationCallbackInfo> currArrayFragment, int currArrayIndex)
		{
			this.m_currArrayFragment = currArrayFragment;
			this.m_currArrayIndex = currArrayIndex;
		}

		// Token: 0x04001B70 RID: 7024
		internal SparselyPopulatedArrayFragment<CancellationCallbackInfo> m_currArrayFragment;

		// Token: 0x04001B71 RID: 7025
		internal int m_currArrayIndex;
	}
}
