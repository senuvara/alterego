﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x02000484 RID: 1156
	internal interface IAsyncLocal
	{
		// Token: 0x060033ED RID: 13293
		[SecurityCritical]
		void OnValueChanged(object previousValue, object currentValue, bool contextChanged);
	}
}
