﻿using System;

namespace System.Threading
{
	// Token: 0x0200046C RID: 1132
	internal struct SparselyPopulatedArrayAddInfo<T> where T : class
	{
		// Token: 0x06003338 RID: 13112 RVA: 0x000B66AA File Offset: 0x000B48AA
		internal SparselyPopulatedArrayAddInfo(SparselyPopulatedArrayFragment<T> source, int index)
		{
			this.m_source = source;
			this.m_index = index;
		}

		// Token: 0x17000887 RID: 2183
		// (get) Token: 0x06003339 RID: 13113 RVA: 0x000B66BA File Offset: 0x000B48BA
		internal SparselyPopulatedArrayFragment<T> Source
		{
			get
			{
				return this.m_source;
			}
		}

		// Token: 0x17000888 RID: 2184
		// (get) Token: 0x0600333A RID: 13114 RVA: 0x000B66C2 File Offset: 0x000B48C2
		internal int Index
		{
			get
			{
				return this.m_index;
			}
		}

		// Token: 0x04001B79 RID: 7033
		private SparselyPopulatedArrayFragment<T> m_source;

		// Token: 0x04001B7A RID: 7034
		private int m_index;
	}
}
