﻿using System;

namespace System.Threading
{
	// Token: 0x020004BE RID: 1214
	internal class LockQueue
	{
		// Token: 0x06003607 RID: 13831 RVA: 0x000BD10A File Offset: 0x000BB30A
		public LockQueue(ReaderWriterLock rwlock)
		{
			this.rwlock = rwlock;
		}

		// Token: 0x06003608 RID: 13832 RVA: 0x000BD11C File Offset: 0x000BB31C
		public bool Wait(int timeout)
		{
			bool flag = false;
			bool result;
			try
			{
				lock (this)
				{
					this.lockCount++;
					Monitor.Exit(this.rwlock);
					flag = true;
					result = Monitor.Wait(this, timeout);
				}
			}
			finally
			{
				if (flag)
				{
					Monitor.Enter(this.rwlock);
					this.lockCount--;
				}
			}
			return result;
		}

		// Token: 0x170008DB RID: 2267
		// (get) Token: 0x06003609 RID: 13833 RVA: 0x000BD1A0 File Offset: 0x000BB3A0
		public bool IsEmpty
		{
			get
			{
				bool result;
				lock (this)
				{
					result = (this.lockCount == 0);
				}
				return result;
			}
		}

		// Token: 0x0600360A RID: 13834 RVA: 0x000BD1E0 File Offset: 0x000BB3E0
		public void Pulse()
		{
			lock (this)
			{
				Monitor.Pulse(this);
			}
		}

		// Token: 0x04001C74 RID: 7284
		private ReaderWriterLock rwlock;

		// Token: 0x04001C75 RID: 7285
		private int lockCount;
	}
}
