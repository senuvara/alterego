﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	/// <summary>Represents the method that executes on a <see cref="T:System.Threading.Thread" />.</summary>
	/// <param name="obj">An object that contains data for the thread procedure.</param>
	// Token: 0x02000493 RID: 1171
	// (Invoke) Token: 0x0600346A RID: 13418
	[ComVisible(false)]
	public delegate void ParameterizedThreadStart(object obj);
}
