﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x020004A9 RID: 1193
	internal sealed class ThreadPoolWorkQueueThreadLocals
	{
		// Token: 0x06003554 RID: 13652 RVA: 0x000BBC8F File Offset: 0x000B9E8F
		public ThreadPoolWorkQueueThreadLocals(ThreadPoolWorkQueue tpq)
		{
			this.workQueue = tpq;
			this.workStealingQueue = new ThreadPoolWorkQueue.WorkStealingQueue();
			ThreadPoolWorkQueue.allThreadQueues.Add(this.workStealingQueue);
		}

		// Token: 0x06003555 RID: 13653 RVA: 0x000BBCD0 File Offset: 0x000B9ED0
		[SecurityCritical]
		private void CleanUp()
		{
			if (this.workStealingQueue != null)
			{
				if (this.workQueue != null)
				{
					bool flag = false;
					while (!flag)
					{
						try
						{
						}
						finally
						{
							IThreadPoolWorkItem callback = null;
							if (this.workStealingQueue.LocalPop(out callback))
							{
								this.workQueue.Enqueue(callback, true);
							}
							else
							{
								flag = true;
							}
						}
					}
				}
				ThreadPoolWorkQueue.allThreadQueues.Remove(this.workStealingQueue);
			}
		}

		// Token: 0x06003556 RID: 13654 RVA: 0x000BBD3C File Offset: 0x000B9F3C
		[SecuritySafeCritical]
		~ThreadPoolWorkQueueThreadLocals()
		{
			if (!Environment.HasShutdownStarted && !AppDomain.CurrentDomain.IsFinalizingForUnload())
			{
				this.CleanUp();
			}
		}

		// Token: 0x04001C30 RID: 7216
		[SecurityCritical]
		[ThreadStatic]
		public static ThreadPoolWorkQueueThreadLocals threadLocals;

		// Token: 0x04001C31 RID: 7217
		public readonly ThreadPoolWorkQueue workQueue;

		// Token: 0x04001C32 RID: 7218
		public readonly ThreadPoolWorkQueue.WorkStealingQueue workStealingQueue;

		// Token: 0x04001C33 RID: 7219
		public readonly Random random = new Random(Thread.CurrentThread.ManagedThreadId);
	}
}
