﻿using System;

namespace System.Threading
{
	// Token: 0x02000496 RID: 1174
	[Flags]
	internal enum SynchronizationContextProperties
	{
		// Token: 0x04001BF9 RID: 7161
		None = 0,
		// Token: 0x04001BFA RID: 7162
		RequireWaitNotification = 1
	}
}
