﻿using System;

namespace System.Threading
{
	// Token: 0x02000479 RID: 1145
	internal static class PlatformHelper
	{
		// Token: 0x1700089E RID: 2206
		// (get) Token: 0x060033B9 RID: 13241 RVA: 0x000B860C File Offset: 0x000B680C
		internal static int ProcessorCount
		{
			get
			{
				int tickCount = Environment.TickCount;
				int num = PlatformHelper.s_processorCount;
				if (num == 0 || tickCount - PlatformHelper.s_lastProcessorCountRefreshTicks >= 30000)
				{
					num = (PlatformHelper.s_processorCount = Environment.ProcessorCount);
					PlatformHelper.s_lastProcessorCountRefreshTicks = tickCount;
				}
				return num;
			}
		}

		// Token: 0x1700089F RID: 2207
		// (get) Token: 0x060033BA RID: 13242 RVA: 0x000B8651 File Offset: 0x000B6851
		internal static bool IsSingleProcessor
		{
			get
			{
				return PlatformHelper.ProcessorCount == 1;
			}
		}

		// Token: 0x04001BBD RID: 7101
		private const int PROCESSOR_COUNT_REFRESH_INTERVAL_MS = 30000;

		// Token: 0x04001BBE RID: 7102
		private static volatile int s_processorCount;

		// Token: 0x04001BBF RID: 7103
		private static volatile int s_lastProcessorCountRefreshTicks;
	}
}
