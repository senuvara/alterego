﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x020004AB RID: 1195
	internal sealed class QueueUserWorkItemCallback : IThreadPoolWorkItem
	{
		// Token: 0x06003558 RID: 13656 RVA: 0x000BBD83 File Offset: 0x000B9F83
		[SecuritySafeCritical]
		static QueueUserWorkItemCallback()
		{
		}

		// Token: 0x06003559 RID: 13657 RVA: 0x000BBD96 File Offset: 0x000B9F96
		[SecurityCritical]
		internal QueueUserWorkItemCallback(WaitCallback waitCallback, object stateObj, bool compressStack, ref StackCrawlMark stackMark)
		{
			this.callback = waitCallback;
			this.state = stateObj;
			if (compressStack && !ExecutionContext.IsFlowSuppressed())
			{
				this.context = ExecutionContext.Capture(ref stackMark, ExecutionContext.CaptureOptions.IgnoreSyncCtx | ExecutionContext.CaptureOptions.OptimizeDefaultCase);
			}
		}

		// Token: 0x0600355A RID: 13658 RVA: 0x000BBDC4 File Offset: 0x000B9FC4
		internal QueueUserWorkItemCallback(WaitCallback waitCallback, object stateObj, ExecutionContext ec)
		{
			this.callback = waitCallback;
			this.state = stateObj;
			this.context = ec;
		}

		// Token: 0x0600355B RID: 13659 RVA: 0x000BBDE1 File Offset: 0x000B9FE1
		[SecurityCritical]
		void IThreadPoolWorkItem.ExecuteWorkItem()
		{
			if (this.context == null)
			{
				WaitCallback waitCallback = this.callback;
				this.callback = null;
				waitCallback(this.state);
				return;
			}
			ExecutionContext.Run(this.context, QueueUserWorkItemCallback.ccb, this, true);
		}

		// Token: 0x0600355C RID: 13660 RVA: 0x000020D3 File Offset: 0x000002D3
		[SecurityCritical]
		void IThreadPoolWorkItem.MarkAborted(ThreadAbortException tae)
		{
		}

		// Token: 0x0600355D RID: 13661 RVA: 0x000BBE18 File Offset: 0x000BA018
		[SecurityCritical]
		private static void WaitCallback_Context(object state)
		{
			QueueUserWorkItemCallback queueUserWorkItemCallback = (QueueUserWorkItemCallback)state;
			queueUserWorkItemCallback.callback(queueUserWorkItemCallback.state);
		}

		// Token: 0x04001C34 RID: 7220
		private WaitCallback callback;

		// Token: 0x04001C35 RID: 7221
		private ExecutionContext context;

		// Token: 0x04001C36 RID: 7222
		private object state;

		// Token: 0x04001C37 RID: 7223
		[SecurityCritical]
		internal static ContextCallback ccb = new ContextCallback(QueueUserWorkItemCallback.WaitCallback_Context);
	}
}
