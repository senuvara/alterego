﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	/// <summary>Contains constants that specify infinite time-out intervals. This class cannot be inherited.</summary>
	// Token: 0x020004B4 RID: 1204
	[ComVisible(true)]
	public static class Timeout
	{
		// Token: 0x060035A5 RID: 13733 RVA: 0x000BC744 File Offset: 0x000BA944
		// Note: this type is marked as 'beforefieldinit'.
		static Timeout()
		{
		}

		/// <summary>A constant used to specify an infinite waiting period, for methods that accept a <see cref="T:System.TimeSpan" /> parameter.</summary>
		// Token: 0x04001C5C RID: 7260
		[ComVisible(false)]
		public static readonly TimeSpan InfiniteTimeSpan = new TimeSpan(0, 0, 0, 0, -1);

		/// <summary>A constant used to specify an infinite waiting period, for threading methods that accept an <see cref="T:System.Int32" /> parameter. </summary>
		// Token: 0x04001C5D RID: 7261
		public const int Infinite = -1;

		// Token: 0x04001C5E RID: 7262
		internal const uint UnsignedInfinite = 4294967295U;
	}
}
