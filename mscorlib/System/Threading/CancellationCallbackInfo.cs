﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x0200046A RID: 1130
	internal class CancellationCallbackInfo
	{
		// Token: 0x06003332 RID: 13106 RVA: 0x000B647C File Offset: 0x000B467C
		internal CancellationCallbackInfo(Action<object> callback, object stateForCallback, SynchronizationContext targetSyncContext, ExecutionContext targetExecutionContext, CancellationTokenSource cancellationTokenSource)
		{
			this.Callback = callback;
			this.StateForCallback = stateForCallback;
			this.TargetSyncContext = targetSyncContext;
			this.TargetExecutionContext = targetExecutionContext;
			this.CancellationTokenSource = cancellationTokenSource;
		}

		// Token: 0x06003333 RID: 13107 RVA: 0x000B64AC File Offset: 0x000B46AC
		[SecuritySafeCritical]
		internal void ExecuteCallback()
		{
			if (this.TargetExecutionContext != null)
			{
				ContextCallback contextCallback = CancellationCallbackInfo.s_executionContextCallback;
				if (contextCallback == null)
				{
					contextCallback = (CancellationCallbackInfo.s_executionContextCallback = new ContextCallback(CancellationCallbackInfo.ExecutionContextCallback));
				}
				ExecutionContext.Run(this.TargetExecutionContext, contextCallback, this);
				return;
			}
			CancellationCallbackInfo.ExecutionContextCallback(this);
		}

		// Token: 0x06003334 RID: 13108 RVA: 0x000B64F4 File Offset: 0x000B46F4
		[SecurityCritical]
		private static void ExecutionContextCallback(object obj)
		{
			CancellationCallbackInfo cancellationCallbackInfo = obj as CancellationCallbackInfo;
			cancellationCallbackInfo.Callback(cancellationCallbackInfo.StateForCallback);
		}

		// Token: 0x04001B72 RID: 7026
		internal readonly Action<object> Callback;

		// Token: 0x04001B73 RID: 7027
		internal readonly object StateForCallback;

		// Token: 0x04001B74 RID: 7028
		internal readonly SynchronizationContext TargetSyncContext;

		// Token: 0x04001B75 RID: 7029
		internal readonly ExecutionContext TargetExecutionContext;

		// Token: 0x04001B76 RID: 7030
		internal readonly CancellationTokenSource CancellationTokenSource;

		// Token: 0x04001B77 RID: 7031
		[SecurityCritical]
		private static ContextCallback s_executionContextCallback;
	}
}
