﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x020004A0 RID: 1184
	internal interface IThreadPoolWorkItem
	{
		// Token: 0x0600352C RID: 13612
		[SecurityCritical]
		void ExecuteWorkItem();

		// Token: 0x0600352D RID: 13613
		[SecurityCritical]
		void MarkAborted(ThreadAbortException tae);
	}
}
