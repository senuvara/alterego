﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading
{
	/// <summary>The class that provides data change information to <see cref="T:System.Threading.AsyncLocal`1" /> instances that register for change notifications. </summary>
	/// <typeparam name="T">The type of the data. </typeparam>
	// Token: 0x02000485 RID: 1157
	public struct AsyncLocalValueChangedArgs<T>
	{
		/// <summary>Gets the data's previous value.</summary>
		/// <returns>The data's previous value. </returns>
		// Token: 0x170008AC RID: 2220
		// (get) Token: 0x060033EE RID: 13294 RVA: 0x000B912A File Offset: 0x000B732A
		// (set) Token: 0x060033EF RID: 13295 RVA: 0x000B9132 File Offset: 0x000B7332
		public T PreviousValue
		{
			[CompilerGenerated]
			get
			{
				return this.<PreviousValue>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<PreviousValue>k__BackingField = value;
			}
		}

		/// <summary>Gets the data's current value. </summary>
		/// <returns>The data's current value. </returns>
		// Token: 0x170008AD RID: 2221
		// (get) Token: 0x060033F0 RID: 13296 RVA: 0x000B913B File Offset: 0x000B733B
		// (set) Token: 0x060033F1 RID: 13297 RVA: 0x000B9143 File Offset: 0x000B7343
		public T CurrentValue
		{
			[CompilerGenerated]
			get
			{
				return this.<CurrentValue>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<CurrentValue>k__BackingField = value;
			}
		}

		/// <summary>Returns a value that indicates whether the value changes because of a change of execution context. </summary>
		/// <returns>
		///     <see langword="true" /> if the value changed because of a change of execution context; otherwise, <see langword="false" />. </returns>
		// Token: 0x170008AE RID: 2222
		// (get) Token: 0x060033F2 RID: 13298 RVA: 0x000B914C File Offset: 0x000B734C
		// (set) Token: 0x060033F3 RID: 13299 RVA: 0x000B9154 File Offset: 0x000B7354
		public bool ThreadContextChanged
		{
			[CompilerGenerated]
			get
			{
				return this.<ThreadContextChanged>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ThreadContextChanged>k__BackingField = value;
			}
		}

		// Token: 0x060033F4 RID: 13300 RVA: 0x000B915D File Offset: 0x000B735D
		internal AsyncLocalValueChangedArgs(T previousValue, T currentValue, bool contextChanged)
		{
			this = default(AsyncLocalValueChangedArgs<T>);
			this.PreviousValue = previousValue;
			this.CurrentValue = currentValue;
			this.ThreadContextChanged = contextChanged;
		}

		// Token: 0x04001BD9 RID: 7129
		[CompilerGenerated]
		private T <PreviousValue>k__BackingField;

		// Token: 0x04001BDA RID: 7130
		[CompilerGenerated]
		private T <CurrentValue>k__BackingField;

		// Token: 0x04001BDB RID: 7131
		[CompilerGenerated]
		private bool <ThreadContextChanged>k__BackingField;
	}
}
