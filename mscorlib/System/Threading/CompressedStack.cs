﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using Unity;

namespace System.Threading
{
	/// <summary>Provides methods for setting and capturing the compressed stack on the current thread. This class cannot be inherited. </summary>
	// Token: 0x020004B9 RID: 1209
	[Serializable]
	public sealed class CompressedStack : ISerializable
	{
		// Token: 0x060035D3 RID: 13779 RVA: 0x000BCF74 File Offset: 0x000BB174
		internal CompressedStack(int length)
		{
			if (length > 0)
			{
				this._list = new ArrayList(length);
			}
		}

		// Token: 0x060035D4 RID: 13780 RVA: 0x000BCF8C File Offset: 0x000BB18C
		internal CompressedStack(CompressedStack cs)
		{
			if (cs != null && cs._list != null)
			{
				this._list = (ArrayList)cs._list.Clone();
			}
		}

		/// <summary>Creates a copy of the current compressed stack.</summary>
		/// <returns>A <see cref="T:System.Threading.CompressedStack" /> object representing the current compressed stack.</returns>
		// Token: 0x060035D5 RID: 13781 RVA: 0x000BCFB5 File Offset: 0x000BB1B5
		[ComVisible(false)]
		public CompressedStack CreateCopy()
		{
			return new CompressedStack(this);
		}

		/// <summary>Captures the compressed stack from the current thread.</summary>
		/// <returns>A <see cref="T:System.Threading.CompressedStack" /> object.</returns>
		// Token: 0x060035D6 RID: 13782 RVA: 0x000175EA File Offset: 0x000157EA
		public static CompressedStack Capture()
		{
			throw new NotSupportedException();
		}

		/// <summary>Gets the compressed stack for the current thread.</summary>
		/// <returns>A <see cref="T:System.Threading.CompressedStack" /> for the current thread.</returns>
		/// <exception cref="T:System.Security.SecurityException">A caller in the call chain does not have permission to access unmanaged code.-or-The request for <see cref="T:System.Security.Permissions.StrongNameIdentityPermission" /> failed.</exception>
		// Token: 0x060035D7 RID: 13783 RVA: 0x000175EA File Offset: 0x000157EA
		[SecurityCritical]
		public static CompressedStack GetCompressedStack()
		{
			throw new NotSupportedException();
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the logical context information needed to recreate an instance of this execution context.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object to be populated with serialization information. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> structure representing the destination context of the serialization. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />. </exception>
		// Token: 0x060035D8 RID: 13784 RVA: 0x0005BD7C File Offset: 0x00059F7C
		[SecurityCritical]
		[MonoTODO("incomplete")]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
		}

		/// <summary>Runs a method in the specified compressed stack on the current thread.</summary>
		/// <param name="compressedStack">The <see cref="T:System.Threading.CompressedStack" /> to set.</param>
		/// <param name="callback">A <see cref="T:System.Threading.ContextCallback" /> that represents the method to be run in the specified security context.</param>
		/// <param name="state">The object to be passed to the callback method.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="compressedStack" /> is <see langword="null" />.</exception>
		// Token: 0x060035D9 RID: 13785 RVA: 0x000175EA File Offset: 0x000157EA
		[SecurityCritical]
		public static void Run(CompressedStack compressedStack, ContextCallback callback, object state)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060035DA RID: 13786 RVA: 0x000BCFBD File Offset: 0x000BB1BD
		internal bool Equals(CompressedStack cs)
		{
			if (this.IsEmpty())
			{
				return cs.IsEmpty();
			}
			return !cs.IsEmpty() && this._list.Count == cs._list.Count;
		}

		// Token: 0x060035DB RID: 13787 RVA: 0x000BCFF3 File Offset: 0x000BB1F3
		internal bool IsEmpty()
		{
			return this._list == null || this._list.Count == 0;
		}

		// Token: 0x170008D9 RID: 2265
		// (get) Token: 0x060035DC RID: 13788 RVA: 0x000BD00D File Offset: 0x000BB20D
		internal IList List
		{
			get
			{
				return this._list;
			}
		}

		// Token: 0x060035DD RID: 13789 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal CompressedStack()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001C6F RID: 7279
		private ArrayList _list;
	}
}
