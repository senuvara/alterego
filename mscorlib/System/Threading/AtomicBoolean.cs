﻿using System;

namespace System.Threading
{
	// Token: 0x02000465 RID: 1125
	internal class AtomicBoolean
	{
		// Token: 0x060032E2 RID: 13026 RVA: 0x000B5734 File Offset: 0x000B3934
		public bool CompareAndExchange(bool expected, bool newVal)
		{
			int value = newVal ? 1 : 0;
			int num = expected ? 1 : 0;
			return Interlocked.CompareExchange(ref this.flag, value, num) == num;
		}

		// Token: 0x060032E3 RID: 13027 RVA: 0x000B5761 File Offset: 0x000B3961
		public static AtomicBoolean FromValue(bool value)
		{
			return new AtomicBoolean
			{
				Value = value
			};
		}

		// Token: 0x060032E4 RID: 13028 RVA: 0x000B576F File Offset: 0x000B396F
		public bool TrySet()
		{
			return !this.Exchange(true);
		}

		// Token: 0x060032E5 RID: 13029 RVA: 0x000B577B File Offset: 0x000B397B
		public bool TryRelaxedSet()
		{
			return this.flag == 0 && !this.Exchange(true);
		}

		// Token: 0x060032E6 RID: 13030 RVA: 0x000B5794 File Offset: 0x000B3994
		public bool Exchange(bool newVal)
		{
			int value = newVal ? 1 : 0;
			return Interlocked.Exchange(ref this.flag, value) == 1;
		}

		// Token: 0x17000879 RID: 2169
		// (get) Token: 0x060032E7 RID: 13031 RVA: 0x000B57B8 File Offset: 0x000B39B8
		// (set) Token: 0x060032E8 RID: 13032 RVA: 0x000B57C3 File Offset: 0x000B39C3
		public bool Value
		{
			get
			{
				return this.flag == 1;
			}
			set
			{
				this.Exchange(value);
			}
		}

		// Token: 0x060032E9 RID: 13033 RVA: 0x000B57CD File Offset: 0x000B39CD
		public bool Equals(AtomicBoolean rhs)
		{
			return this.flag == rhs.flag;
		}

		// Token: 0x060032EA RID: 13034 RVA: 0x000B57DD File Offset: 0x000B39DD
		public override bool Equals(object rhs)
		{
			return rhs is AtomicBoolean && this.Equals((AtomicBoolean)rhs);
		}

		// Token: 0x060032EB RID: 13035 RVA: 0x000B57F5 File Offset: 0x000B39F5
		public override int GetHashCode()
		{
			return this.flag.GetHashCode();
		}

		// Token: 0x060032EC RID: 13036 RVA: 0x000B5802 File Offset: 0x000B3A02
		public static explicit operator bool(AtomicBoolean rhs)
		{
			return rhs.Value;
		}

		// Token: 0x060032ED RID: 13037 RVA: 0x000B580A File Offset: 0x000B3A0A
		public static implicit operator AtomicBoolean(bool rhs)
		{
			return AtomicBoolean.FromValue(rhs);
		}

		// Token: 0x060032EE RID: 13038 RVA: 0x00002050 File Offset: 0x00000250
		public AtomicBoolean()
		{
		}

		// Token: 0x04001B58 RID: 7000
		private int flag;

		// Token: 0x04001B59 RID: 7001
		private const int UnSet = 0;

		// Token: 0x04001B5A RID: 7002
		private const int Set = 1;
	}
}
