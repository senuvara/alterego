﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x0200049B RID: 1179
	internal class ThreadHelper
	{
		// Token: 0x06003490 RID: 13456 RVA: 0x000BA400 File Offset: 0x000B8600
		[SecuritySafeCritical]
		static ThreadHelper()
		{
		}

		// Token: 0x06003491 RID: 13457 RVA: 0x000BA413 File Offset: 0x000B8613
		internal ThreadHelper(Delegate start)
		{
			this._start = start;
		}

		// Token: 0x06003492 RID: 13458 RVA: 0x000BA422 File Offset: 0x000B8622
		internal void SetExecutionContextHelper(ExecutionContext ec)
		{
			this._executionContext = ec;
		}

		// Token: 0x06003493 RID: 13459 RVA: 0x000BA42C File Offset: 0x000B862C
		[SecurityCritical]
		private static void ThreadStart_Context(object state)
		{
			ThreadHelper threadHelper = (ThreadHelper)state;
			if (threadHelper._start is ThreadStart)
			{
				((ThreadStart)threadHelper._start)();
				return;
			}
			((ParameterizedThreadStart)threadHelper._start)(threadHelper._startArg);
		}

		// Token: 0x06003494 RID: 13460 RVA: 0x000BA474 File Offset: 0x000B8674
		[SecurityCritical]
		internal void ThreadStart(object obj)
		{
			this._startArg = obj;
			if (this._executionContext != null)
			{
				ExecutionContext.Run(this._executionContext, ThreadHelper._ccb, this);
				return;
			}
			((ParameterizedThreadStart)this._start)(obj);
		}

		// Token: 0x06003495 RID: 13461 RVA: 0x000BA4A8 File Offset: 0x000B86A8
		[SecurityCritical]
		internal void ThreadStart()
		{
			if (this._executionContext != null)
			{
				ExecutionContext.Run(this._executionContext, ThreadHelper._ccb, this);
				return;
			}
			((ThreadStart)this._start)();
		}

		// Token: 0x04001C01 RID: 7169
		private Delegate _start;

		// Token: 0x04001C02 RID: 7170
		private object _startArg;

		// Token: 0x04001C03 RID: 7171
		private ExecutionContext _executionContext;

		// Token: 0x04001C04 RID: 7172
		[SecurityCritical]
		internal static ContextCallback _ccb = new ContextCallback(ThreadHelper.ThreadStart_Context);
	}
}
