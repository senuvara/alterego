﻿using System;
using System.Security;
using Microsoft.Win32.SafeHandles;

namespace System.Threading
{
	/// <summary>Provides convenience methods to for working with a safe handle for a wait handle. </summary>
	// Token: 0x020004B7 RID: 1207
	public static class WaitHandleExtensions
	{
		/// <summary>Gets the safe handle for a native operating system wait handle. </summary>
		/// <param name="waitHandle">A native operating system handle. </param>
		/// <returns>The safe wait handle that wraps the native operating system wait handle. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="waitHandle" /> is <see langword="null" />. </exception>
		// Token: 0x060035CD RID: 13773 RVA: 0x000BCF01 File Offset: 0x000BB101
		[SecurityCritical]
		public static SafeWaitHandle GetSafeWaitHandle(this WaitHandle waitHandle)
		{
			if (waitHandle == null)
			{
				throw new ArgumentNullException("waitHandle");
			}
			return waitHandle.SafeWaitHandle;
		}

		/// <summary>Sets a safe handle for a native operating system wait handle. </summary>
		/// <param name="waitHandle">A wait handle that encapsulates an operating system-specific object that waits for exclusive access to a shared resource. </param>
		/// <param name="value">The safe handle to wrap the operating system handle. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="waitHandle" /> is <see langword="null" />. </exception>
		// Token: 0x060035CE RID: 13774 RVA: 0x000BCF17 File Offset: 0x000BB117
		[SecurityCritical]
		public static void SetSafeWaitHandle(this WaitHandle waitHandle, SafeWaitHandle value)
		{
			if (waitHandle == null)
			{
				throw new ArgumentNullException("waitHandle");
			}
			waitHandle.SafeWaitHandle = value;
		}
	}
}
