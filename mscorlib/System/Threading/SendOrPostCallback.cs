﻿using System;

namespace System.Threading
{
	/// <summary>Represents a method to be called when a message is to be dispatched to a synchronization context.  </summary>
	/// <param name="state">The object passed to the delegate.</param>
	// Token: 0x02000495 RID: 1173
	// (Invoke) Token: 0x06003472 RID: 13426
	public delegate void SendOrPostCallback(object state);
}
