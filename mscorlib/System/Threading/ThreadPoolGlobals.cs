﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x020004A4 RID: 1188
	internal static class ThreadPoolGlobals
	{
		// Token: 0x0600353A RID: 13626 RVA: 0x000BB16A File Offset: 0x000B936A
		[SecuritySafeCritical]
		static ThreadPoolGlobals()
		{
		}

		// Token: 0x04001C19 RID: 7193
		public static uint tpQuantum = 30U;

		// Token: 0x04001C1A RID: 7194
		public static int processorCount = Environment.ProcessorCount;

		// Token: 0x04001C1B RID: 7195
		public static bool tpHosted = ThreadPool.IsThreadPoolHosted();

		// Token: 0x04001C1C RID: 7196
		public static volatile bool vmTpInitialized;

		// Token: 0x04001C1D RID: 7197
		public static bool enableWorkerTracking;

		// Token: 0x04001C1E RID: 7198
		[SecurityCritical]
		public static ThreadPoolWorkQueue workQueue = new ThreadPoolWorkQueue();
	}
}
