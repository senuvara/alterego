﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Threading
{
	/// <summary>Provides a mechanism for executing a method on a thread pool thread at specified intervals. This class cannot be inherited.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x020004C6 RID: 1222
	[ComVisible(true)]
	public sealed class Timer : MarshalByRefObject, IDisposable
	{
		/// <summary>Initializes a new instance of the <see langword="Timer" /> class, using a 32-bit signed integer to specify the time interval.</summary>
		/// <param name="callback">A <see cref="T:System.Threading.TimerCallback" /> delegate representing a method to be executed. </param>
		/// <param name="state">An object containing information to be used by the callback method, or <see langword="null" />. </param>
		/// <param name="dueTime">The amount of time to delay before <paramref name="callback" /> is invoked, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to prevent the timer from starting. Specify zero (0) to start the timer immediately. </param>
		/// <param name="period">The time interval between invocations of <paramref name="callback" />, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to disable periodic signaling. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter is negative and is not equal to <see cref="F:System.Threading.Timeout.Infinite" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="callback" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06003654 RID: 13908 RVA: 0x000BDF6F File Offset: 0x000BC16F
		public Timer(TimerCallback callback, object state, int dueTime, int period)
		{
			this.Init(callback, state, (long)dueTime, (long)period);
		}

		/// <summary>Initializes a new instance of the <see langword="Timer" /> class, using 64-bit signed integers to measure time intervals.</summary>
		/// <param name="callback">A <see cref="T:System.Threading.TimerCallback" /> delegate representing a method to be executed. </param>
		/// <param name="state">An object containing information to be used by the callback method, or <see langword="null" />. </param>
		/// <param name="dueTime">The amount of time to delay before <paramref name="callback" /> is invoked, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to prevent the timer from starting. Specify zero (0) to start the timer immediately. </param>
		/// <param name="period">The time interval between invocations of <paramref name="callback" />, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to disable periodic signaling. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter is negative and is not equal to <see cref="F:System.Threading.Timeout.Infinite" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter is greater than 4294967294. </exception>
		// Token: 0x06003655 RID: 13909 RVA: 0x000BDF84 File Offset: 0x000BC184
		public Timer(TimerCallback callback, object state, long dueTime, long period)
		{
			this.Init(callback, state, dueTime, period);
		}

		/// <summary>Initializes a new instance of the <see langword="Timer" /> class, using <see cref="T:System.TimeSpan" /> values to measure time intervals.</summary>
		/// <param name="callback">A delegate representing a method to be executed. </param>
		/// <param name="state">An object containing information to be used by the callback method, or <see langword="null" />. </param>
		/// <param name="dueTime">The amount of time to delay before the <paramref name="callback" /> parameter invokes its methods. Specify negative one (-1) milliseconds to prevent the timer from starting. Specify zero (0) to start the timer immediately. </param>
		/// <param name="period">The time interval between invocations of the methods referenced by <paramref name="callback" />. Specify negative one (-1) milliseconds to disable periodic signaling. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The number of milliseconds in the value of <paramref name="dueTime" /> or <paramref name="period" /> is negative and not equal to <see cref="F:System.Threading.Timeout.Infinite" />, or is greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="callback" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06003656 RID: 13910 RVA: 0x000BDF97 File Offset: 0x000BC197
		public Timer(TimerCallback callback, object state, TimeSpan dueTime, TimeSpan period)
		{
			this.Init(callback, state, (long)dueTime.TotalMilliseconds, (long)period.TotalMilliseconds);
		}

		/// <summary>Initializes a new instance of the <see langword="Timer" /> class, using 32-bit unsigned integers to measure time intervals.</summary>
		/// <param name="callback">A delegate representing a method to be executed. </param>
		/// <param name="state">An object containing information to be used by the callback method, or <see langword="null" />. </param>
		/// <param name="dueTime">The amount of time to delay before <paramref name="callback" /> is invoked, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to prevent the timer from starting. Specify zero (0) to start the timer immediately. </param>
		/// <param name="period">The time interval between invocations of <paramref name="callback" />, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to disable periodic signaling. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter is negative and is not equal to <see cref="F:System.Threading.Timeout.Infinite" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="callback" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06003657 RID: 13911 RVA: 0x000BDFB8 File Offset: 0x000BC1B8
		[CLSCompliant(false)]
		public Timer(TimerCallback callback, object state, uint dueTime, uint period)
		{
			long dueTime2 = (long)((dueTime == uint.MaxValue) ? ulong.MaxValue : ((ulong)dueTime));
			long period2 = (long)((period == uint.MaxValue) ? ulong.MaxValue : ((ulong)period));
			this.Init(callback, state, dueTime2, period2);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Timer" /> class with an infinite period and an infinite due time, using the newly created <see cref="T:System.Threading.Timer" /> object as the state object. </summary>
		/// <param name="callback">A <see cref="T:System.Threading.TimerCallback" /> delegate representing a method to be executed.</param>
		// Token: 0x06003658 RID: 13912 RVA: 0x000BDFED File Offset: 0x000BC1ED
		public Timer(TimerCallback callback)
		{
			this.Init(callback, this, -1L, -1L);
		}

		// Token: 0x06003659 RID: 13913 RVA: 0x000BE001 File Offset: 0x000BC201
		private void Init(TimerCallback callback, object state, long dueTime, long period)
		{
			if (callback == null)
			{
				throw new ArgumentNullException("callback");
			}
			this.callback = callback;
			this.state = state;
			this.Change(dueTime, period, true);
		}

		/// <summary>Changes the start time and the interval between method invocations for a timer, using 32-bit signed integers to measure time intervals.</summary>
		/// <param name="dueTime">The amount of time to delay before the invoking the callback method specified when the <see cref="T:System.Threading.Timer" /> was constructed, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to prevent the timer from restarting. Specify zero (0) to restart the timer immediately. </param>
		/// <param name="period">The time interval between invocations of the callback method specified when the <see cref="T:System.Threading.Timer" /> was constructed, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to disable periodic signaling. </param>
		/// <returns>
		///     <see langword="true" /> if the timer was successfully updated; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.Timer" /> has already been disposed. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter is negative and is not equal to <see cref="F:System.Threading.Timeout.Infinite" />. </exception>
		// Token: 0x0600365A RID: 13914 RVA: 0x000BE02A File Offset: 0x000BC22A
		public bool Change(int dueTime, int period)
		{
			return this.Change((long)dueTime, (long)period, false);
		}

		/// <summary>Changes the start time and the interval between method invocations for a timer, using <see cref="T:System.TimeSpan" /> values to measure time intervals.</summary>
		/// <param name="dueTime">A <see cref="T:System.TimeSpan" /> representing the amount of time to delay before invoking the callback method specified when the <see cref="T:System.Threading.Timer" /> was constructed. Specify negative one (-1) milliseconds to prevent the timer from restarting. Specify zero (0) to restart the timer immediately. </param>
		/// <param name="period">The time interval between invocations of the callback method specified when the <see cref="T:System.Threading.Timer" /> was constructed. Specify negative one (-1) milliseconds to disable periodic signaling. </param>
		/// <returns>
		///     <see langword="true" /> if the timer was successfully updated; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.Timer" /> has already been disposed. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter, in milliseconds, is less than -1. </exception>
		/// <exception cref="T:System.NotSupportedException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter, in milliseconds, is greater than 4294967294. </exception>
		// Token: 0x0600365B RID: 13915 RVA: 0x000BE037 File Offset: 0x000BC237
		public bool Change(TimeSpan dueTime, TimeSpan period)
		{
			return this.Change((long)dueTime.TotalMilliseconds, (long)period.TotalMilliseconds, false);
		}

		/// <summary>Changes the start time and the interval between method invocations for a timer, using 32-bit unsigned integers to measure time intervals.</summary>
		/// <param name="dueTime">The amount of time to delay before the invoking the callback method specified when the <see cref="T:System.Threading.Timer" /> was constructed, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to prevent the timer from restarting. Specify zero (0) to restart the timer immediately. </param>
		/// <param name="period">The time interval between invocations of the callback method specified when the <see cref="T:System.Threading.Timer" /> was constructed, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to disable periodic signaling. </param>
		/// <returns>
		///     <see langword="true" /> if the timer was successfully updated; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.Timer" /> has already been disposed. </exception>
		// Token: 0x0600365C RID: 13916 RVA: 0x000BE050 File Offset: 0x000BC250
		[CLSCompliant(false)]
		public bool Change(uint dueTime, uint period)
		{
			long dueTime2 = (long)((dueTime == uint.MaxValue) ? ulong.MaxValue : ((ulong)dueTime));
			long period2 = (long)((period == uint.MaxValue) ? ulong.MaxValue : ((ulong)period));
			return this.Change(dueTime2, period2, false);
		}

		/// <summary>Releases all resources used by the current instance of <see cref="T:System.Threading.Timer" />.</summary>
		// Token: 0x0600365D RID: 13917 RVA: 0x000BE07C File Offset: 0x000BC27C
		public void Dispose()
		{
			if (this.disposed)
			{
				return;
			}
			this.disposed = true;
			Timer.scheduler.Remove(this);
		}

		/// <summary>Changes the start time and the interval between method invocations for a timer, using 64-bit signed integers to measure time intervals.</summary>
		/// <param name="dueTime">The amount of time to delay before the invoking the callback method specified when the <see cref="T:System.Threading.Timer" /> was constructed, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to prevent the timer from restarting. Specify zero (0) to restart the timer immediately. </param>
		/// <param name="period">The time interval between invocations of the callback method specified when the <see cref="T:System.Threading.Timer" /> was constructed, in milliseconds. Specify <see cref="F:System.Threading.Timeout.Infinite" /> to disable periodic signaling. </param>
		/// <returns>
		///     <see langword="true" /> if the timer was successfully updated; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.Timer" /> has already been disposed. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter is less than -1. </exception>
		/// <exception cref="T:System.NotSupportedException">The <paramref name="dueTime" /> or <paramref name="period" /> parameter is greater than 4294967294. </exception>
		// Token: 0x0600365E RID: 13918 RVA: 0x000BE099 File Offset: 0x000BC299
		public bool Change(long dueTime, long period)
		{
			return this.Change(dueTime, period, false);
		}

		// Token: 0x0600365F RID: 13919 RVA: 0x000BE0A4 File Offset: 0x000BC2A4
		private bool Change(long dueTime, long period, bool first)
		{
			if (dueTime > (long)((ulong)-2))
			{
				throw new ArgumentOutOfRangeException("dueTime", "Due time too large");
			}
			if (period > (long)((ulong)-2))
			{
				throw new ArgumentOutOfRangeException("period", "Period too large");
			}
			if (dueTime < -1L)
			{
				throw new ArgumentOutOfRangeException("dueTime");
			}
			if (period < -1L)
			{
				throw new ArgumentOutOfRangeException("period");
			}
			if (this.disposed)
			{
				throw new ObjectDisposedException(null, Environment.GetResourceString("Cannot access a disposed object."));
			}
			this.due_time_ms = dueTime;
			this.period_ms = period;
			long new_next_run;
			if (dueTime == 0L)
			{
				new_next_run = 0L;
			}
			else if (dueTime < 0L)
			{
				new_next_run = long.MaxValue;
				if (first)
				{
					this.next_run = new_next_run;
					return true;
				}
			}
			else
			{
				new_next_run = dueTime * 10000L + Timer.GetTimeMonotonic();
			}
			Timer.scheduler.Change(this, new_next_run);
			return true;
		}

		/// <summary>Releases all resources used by the current instance of <see cref="T:System.Threading.Timer" /> and signals when the timer has been disposed of.</summary>
		/// <param name="notifyObject">The <see cref="T:System.Threading.WaitHandle" /> to be signaled when the <see langword="Timer" /> has been disposed of. </param>
		/// <returns>
		///     <see langword="true" /> if the function succeeds; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="notifyObject" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06003660 RID: 13920 RVA: 0x000BE163 File Offset: 0x000BC363
		public bool Dispose(WaitHandle notifyObject)
		{
			if (notifyObject == null)
			{
				throw new ArgumentNullException("notifyObject");
			}
			this.Dispose();
			NativeEventCalls.SetEvent(notifyObject.SafeWaitHandle);
			return true;
		}

		// Token: 0x06003661 RID: 13921 RVA: 0x000020D3 File Offset: 0x000002D3
		internal void KeepRootedWhileScheduled()
		{
		}

		// Token: 0x06003662 RID: 13922
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern long GetTimeMonotonic();

		// Token: 0x06003663 RID: 13923 RVA: 0x000BE186 File Offset: 0x000BC386
		// Note: this type is marked as 'beforefieldinit'.
		static Timer()
		{
		}

		// Token: 0x04001CB6 RID: 7350
		private static readonly Timer.Scheduler scheduler = Timer.Scheduler.Instance;

		// Token: 0x04001CB7 RID: 7351
		private TimerCallback callback;

		// Token: 0x04001CB8 RID: 7352
		private object state;

		// Token: 0x04001CB9 RID: 7353
		private long due_time_ms;

		// Token: 0x04001CBA RID: 7354
		private long period_ms;

		// Token: 0x04001CBB RID: 7355
		private long next_run;

		// Token: 0x04001CBC RID: 7356
		private bool disposed;

		// Token: 0x04001CBD RID: 7357
		private const long MaxValue = 4294967294L;

		// Token: 0x020004C7 RID: 1223
		private sealed class TimerComparer : IComparer
		{
			// Token: 0x06003664 RID: 13924 RVA: 0x000BE194 File Offset: 0x000BC394
			public int Compare(object x, object y)
			{
				Timer timer = x as Timer;
				if (timer == null)
				{
					return -1;
				}
				Timer timer2 = y as Timer;
				if (timer2 == null)
				{
					return 1;
				}
				long num = timer.next_run - timer2.next_run;
				if (num == 0L)
				{
					if (x != y)
					{
						return -1;
					}
					return 0;
				}
				else
				{
					if (num <= 0L)
					{
						return -1;
					}
					return 1;
				}
			}

			// Token: 0x06003665 RID: 13925 RVA: 0x00002050 File Offset: 0x00000250
			public TimerComparer()
			{
			}
		}

		// Token: 0x020004C8 RID: 1224
		private sealed class Scheduler
		{
			// Token: 0x06003666 RID: 13926 RVA: 0x000BE1DA File Offset: 0x000BC3DA
			static Scheduler()
			{
			}

			// Token: 0x170008E4 RID: 2276
			// (get) Token: 0x06003667 RID: 13927 RVA: 0x000BE1E6 File Offset: 0x000BC3E6
			public static Timer.Scheduler Instance
			{
				get
				{
					return Timer.Scheduler.instance;
				}
			}

			// Token: 0x06003668 RID: 13928 RVA: 0x000BE1F0 File Offset: 0x000BC3F0
			private Scheduler()
			{
				this.changed = new ManualResetEvent(false);
				this.list = new SortedList(new Timer.TimerComparer(), 1024);
				new Thread(new ThreadStart(this.SchedulerThread))
				{
					IsBackground = true
				}.Start();
			}

			// Token: 0x06003669 RID: 13929 RVA: 0x000BE244 File Offset: 0x000BC444
			public void Remove(Timer timer)
			{
				if (timer.next_run == 0L || timer.next_run == 9223372036854775807L)
				{
					return;
				}
				lock (this)
				{
					this.InternalRemove(timer);
				}
			}

			// Token: 0x0600366A RID: 13930 RVA: 0x000BE29C File Offset: 0x000BC49C
			public void Change(Timer timer, long new_next_run)
			{
				bool flag = false;
				lock (this)
				{
					this.InternalRemove(timer);
					if (new_next_run == 9223372036854775807L)
					{
						timer.next_run = new_next_run;
						return;
					}
					if (!timer.disposed)
					{
						timer.next_run = new_next_run;
						this.Add(timer);
						flag = (this.list.GetByIndex(0) == timer);
					}
				}
				if (flag)
				{
					this.changed.Set();
				}
			}

			// Token: 0x0600366B RID: 13931 RVA: 0x000BE328 File Offset: 0x000BC528
			private int FindByDueTime(long nr)
			{
				int i = 0;
				int num = this.list.Count - 1;
				if (num < 0)
				{
					return -1;
				}
				if (num < 20)
				{
					while (i <= num)
					{
						Timer timer = (Timer)this.list.GetByIndex(i);
						if (timer.next_run == nr)
						{
							return i;
						}
						if (timer.next_run > nr)
						{
							return -1;
						}
						i++;
					}
					return -1;
				}
				while (i <= num)
				{
					int num2 = i + (num - i >> 1);
					Timer timer2 = (Timer)this.list.GetByIndex(num2);
					if (nr == timer2.next_run)
					{
						return num2;
					}
					if (nr > timer2.next_run)
					{
						i = num2 + 1;
					}
					else
					{
						num = num2 - 1;
					}
				}
				return -1;
			}

			// Token: 0x0600366C RID: 13932 RVA: 0x000BE3C4 File Offset: 0x000BC5C4
			private void Add(Timer timer)
			{
				int num = this.FindByDueTime(timer.next_run);
				if (num != -1)
				{
					bool flag = long.MaxValue - timer.next_run > 20000L;
					do
					{
						num++;
						if (flag)
						{
							timer.next_run += 1L;
						}
						else
						{
							timer.next_run -= 1L;
						}
					}
					while (num < this.list.Count && ((Timer)this.list.GetByIndex(num)).next_run == timer.next_run);
				}
				this.list.Add(timer, timer);
			}

			// Token: 0x0600366D RID: 13933 RVA: 0x000BE460 File Offset: 0x000BC660
			private int InternalRemove(Timer timer)
			{
				int num = this.list.IndexOfKey(timer);
				if (num >= 0)
				{
					this.list.RemoveAt(num);
				}
				return num;
			}

			// Token: 0x0600366E RID: 13934 RVA: 0x000BE48C File Offset: 0x000BC68C
			private static void TimerCB(object o)
			{
				Timer timer = (Timer)o;
				timer.callback(timer.state);
			}

			// Token: 0x0600366F RID: 13935 RVA: 0x000BE4B4 File Offset: 0x000BC6B4
			private void SchedulerThread()
			{
				Thread.CurrentThread.Name = "Timer-Scheduler";
				List<Timer> list = new List<Timer>(512);
				for (;;)
				{
					int num = -1;
					long timeMonotonic = Timer.GetTimeMonotonic();
					lock (this)
					{
						this.changed.Reset();
						int num2 = this.list.Count;
						for (int i = 0; i < num2; i++)
						{
							Timer timer = (Timer)this.list.GetByIndex(i);
							if (timer.next_run > timeMonotonic)
							{
								break;
							}
							this.list.RemoveAt(i);
							num2--;
							i--;
							ThreadPool.UnsafeQueueUserWorkItem(new WaitCallback(Timer.Scheduler.TimerCB), timer);
							long period_ms = timer.period_ms;
							long due_time_ms = timer.due_time_ms;
							if (period_ms == -1L || ((period_ms == 0L || period_ms == -1L) && due_time_ms != -1L))
							{
								timer.next_run = long.MaxValue;
							}
							else
							{
								timer.next_run = Timer.GetTimeMonotonic() + 10000L * timer.period_ms;
								list.Add(timer);
							}
						}
						num2 = list.Count;
						for (int i = 0; i < num2; i++)
						{
							Timer timer2 = list[i];
							this.Add(timer2);
						}
						list.Clear();
						this.ShrinkIfNeeded(list, 512);
						int capacity = this.list.Capacity;
						num2 = this.list.Count;
						if (capacity > 1024 && num2 > 0 && capacity / num2 > 3)
						{
							this.list.Capacity = num2 * 2;
						}
						long num3 = long.MaxValue;
						if (this.list.Count > 0)
						{
							num3 = ((Timer)this.list.GetByIndex(0)).next_run;
						}
						num = -1;
						if (num3 != 9223372036854775807L)
						{
							long num4 = (num3 - Timer.GetTimeMonotonic()) / 10000L;
							if (num4 > 2147483647L)
							{
								num = 2147483646;
							}
							else
							{
								num = (int)num4;
								if (num < 0)
								{
									num = 0;
								}
							}
						}
					}
					this.changed.WaitOne(num);
				}
			}

			// Token: 0x06003670 RID: 13936 RVA: 0x000BE6FC File Offset: 0x000BC8FC
			private void ShrinkIfNeeded(List<Timer> list, int initial)
			{
				int capacity = list.Capacity;
				int count = list.Count;
				if (capacity > initial && count > 0 && capacity / count > 3)
				{
					list.Capacity = count * 2;
				}
			}

			// Token: 0x04001CBE RID: 7358
			private static Timer.Scheduler instance = new Timer.Scheduler();

			// Token: 0x04001CBF RID: 7359
			private SortedList list;

			// Token: 0x04001CC0 RID: 7360
			private ManualResetEvent changed;
		}
	}
}
