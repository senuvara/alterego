﻿using System;

namespace System.Threading
{
	// Token: 0x02000471 RID: 1137
	internal static class LazyHelpers<T>
	{
		// Token: 0x0600335D RID: 13149 RVA: 0x000B6C30 File Offset: 0x000B4E30
		private static T ActivatorFactorySelector()
		{
			T result;
			try
			{
				result = (T)((object)Activator.CreateInstance(typeof(T)));
			}
			catch (MissingMethodException)
			{
				throw new MissingMemberException(Environment.GetResourceString("The lazily-initialized type does not have a public, parameterless constructor."));
			}
			return result;
		}

		// Token: 0x0600335E RID: 13150 RVA: 0x000B6C78 File Offset: 0x000B4E78
		// Note: this type is marked as 'beforefieldinit'.
		static LazyHelpers()
		{
		}

		// Token: 0x04001B87 RID: 7047
		internal static Func<T> s_activatorFactorySelector = new Func<T>(LazyHelpers<T>.ActivatorFactorySelector);
	}
}
