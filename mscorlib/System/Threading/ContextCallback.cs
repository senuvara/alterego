﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	/// <summary>Represents a method to be called within a new context.  </summary>
	/// <param name="state">An object containing information to be used by the callback method each time it executes.</param>
	// Token: 0x02000489 RID: 1161
	// (Invoke) Token: 0x06003405 RID: 13317
	[ComVisible(true)]
	public delegate void ContextCallback(object state);
}
