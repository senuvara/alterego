﻿using System;
using System.Collections;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System.Threading
{
	/// <summary>Defines a lock that supports single writers and multiple readers. </summary>
	// Token: 0x020004C3 RID: 1219
	[ComVisible(true)]
	public sealed class ReaderWriterLock : CriticalFinalizerObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.ReaderWriterLock" /> class.</summary>
		// Token: 0x06003634 RID: 13876 RVA: 0x000BD4F0 File Offset: 0x000BB6F0
		public ReaderWriterLock()
		{
			this.writer_queue = new LockQueue(this);
			this.reader_locks = new Hashtable();
			GC.SuppressFinalize(this);
		}

		/// <summary>Ensures that resources are freed and other cleanup operations are performed when the garbage collector reclaims the <see cref="T:System.Threading.ReaderWriterLock" /> object. </summary>
		// Token: 0x06003635 RID: 13877 RVA: 0x000BD51C File Offset: 0x000BB71C
		~ReaderWriterLock()
		{
		}

		/// <summary>Gets a value indicating whether the current thread holds a reader lock.</summary>
		/// <returns>
		///     <see langword="true" /> if the current thread holds a reader lock; otherwise, <see langword="false" />.</returns>
		// Token: 0x170008E1 RID: 2273
		// (get) Token: 0x06003636 RID: 13878 RVA: 0x000BD544 File Offset: 0x000BB744
		public bool IsReaderLockHeld
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				bool result;
				lock (this)
				{
					result = this.reader_locks.ContainsKey(Thread.CurrentThreadId);
				}
				return result;
			}
		}

		/// <summary>Gets a value indicating whether the current thread holds the writer lock.</summary>
		/// <returns>
		///     <see langword="true" /> if the current thread holds the writer lock; otherwise, <see langword="false" />.</returns>
		// Token: 0x170008E2 RID: 2274
		// (get) Token: 0x06003637 RID: 13879 RVA: 0x000BD590 File Offset: 0x000BB790
		public bool IsWriterLockHeld
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				bool result;
				lock (this)
				{
					result = (this.state < 0 && Thread.CurrentThreadId == this.writer_lock_owner);
				}
				return result;
			}
		}

		/// <summary>Gets the current sequence number.</summary>
		/// <returns>The current sequence number.</returns>
		// Token: 0x170008E3 RID: 2275
		// (get) Token: 0x06003638 RID: 13880 RVA: 0x000BD5E0 File Offset: 0x000BB7E0
		public int WriterSeqNum
		{
			get
			{
				int result;
				lock (this)
				{
					result = this.seq_num;
				}
				return result;
			}
		}

		/// <summary>Acquires a reader lock, using an <see cref="T:System.Int32" /> value for the time-out.</summary>
		/// <param name="millisecondsTimeout">The time-out in milliseconds. </param>
		/// <exception cref="T:System.ApplicationException">
		///         <paramref name="millisecondsTimeout" />
		///          expires before the lock request is granted. </exception>
		// Token: 0x06003639 RID: 13881 RVA: 0x000BD620 File Offset: 0x000BB820
		public void AcquireReaderLock(int millisecondsTimeout)
		{
			this.AcquireReaderLock(millisecondsTimeout, 1);
		}

		// Token: 0x0600363A RID: 13882 RVA: 0x000BD62C File Offset: 0x000BB82C
		private void AcquireReaderLock(int millisecondsTimeout, int initialLockCount)
		{
			lock (this)
			{
				if (this.HasWriterLock())
				{
					this.AcquireWriterLock(millisecondsTimeout, initialLockCount);
				}
				else
				{
					object obj = this.reader_locks[Thread.CurrentThreadId];
					if (obj == null)
					{
						this.readers++;
						try
						{
							if (this.state < 0 || !this.writer_queue.IsEmpty)
							{
								while (Monitor.Wait(this, millisecondsTimeout))
								{
									if (this.state >= 0)
									{
										goto IL_7B;
									}
								}
								throw new ApplicationException("Timeout expired");
							}
							IL_7B:;
						}
						finally
						{
							this.readers--;
						}
						this.reader_locks[Thread.CurrentThreadId] = initialLockCount;
						this.state += initialLockCount;
					}
					else
					{
						this.reader_locks[Thread.CurrentThreadId] = (int)obj + 1;
						this.state++;
					}
				}
			}
		}

		/// <summary>Acquires a reader lock, using a <see cref="T:System.TimeSpan" /> value for the time-out.</summary>
		/// <param name="timeout">A <see langword="TimeSpan" /> specifying the time-out period. </param>
		/// <exception cref="T:System.ApplicationException">
		///         <paramref name="timeout" /> expires before the lock request is granted. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="timeout" /> specifies a negative value other than -1 milliseconds. </exception>
		// Token: 0x0600363B RID: 13883 RVA: 0x000BD748 File Offset: 0x000BB948
		public void AcquireReaderLock(TimeSpan timeout)
		{
			int millisecondsTimeout = this.CheckTimeout(timeout);
			this.AcquireReaderLock(millisecondsTimeout, 1);
		}

		/// <summary>Acquires the writer lock, using an <see cref="T:System.Int32" /> value for the time-out.</summary>
		/// <param name="millisecondsTimeout">The time-out in milliseconds. </param>
		/// <exception cref="T:System.ApplicationException">
		///         <paramref name="timeout" />
		///          expires before the lock request is granted. </exception>
		// Token: 0x0600363C RID: 13884 RVA: 0x000BD765 File Offset: 0x000BB965
		public void AcquireWriterLock(int millisecondsTimeout)
		{
			this.AcquireWriterLock(millisecondsTimeout, 1);
		}

		// Token: 0x0600363D RID: 13885 RVA: 0x000BD770 File Offset: 0x000BB970
		private void AcquireWriterLock(int millisecondsTimeout, int initialLockCount)
		{
			lock (this)
			{
				if (this.HasWriterLock())
				{
					this.state--;
				}
				else
				{
					if (this.state != 0 || !this.writer_queue.IsEmpty)
					{
						while (this.writer_queue.Wait(millisecondsTimeout))
						{
							if (this.state == 0)
							{
								goto IL_5A;
							}
						}
						throw new ApplicationException("Timeout expired");
					}
					IL_5A:
					this.state = -initialLockCount;
					this.writer_lock_owner = Thread.CurrentThreadId;
					this.seq_num++;
				}
			}
		}

		/// <summary>Acquires the writer lock, using a <see cref="T:System.TimeSpan" /> value for the time-out.</summary>
		/// <param name="timeout">The <see langword="TimeSpan" /> specifying the time-out period. </param>
		/// <exception cref="T:System.ApplicationException">
		///         <paramref name="timeout" /> expires before the lock request is granted. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="timeout" /> specifies a negative value other than -1 milliseconds. </exception>
		// Token: 0x0600363E RID: 13886 RVA: 0x000BD814 File Offset: 0x000BBA14
		public void AcquireWriterLock(TimeSpan timeout)
		{
			int millisecondsTimeout = this.CheckTimeout(timeout);
			this.AcquireWriterLock(millisecondsTimeout, 1);
		}

		/// <summary>Indicates whether the writer lock has been granted to any thread since the sequence number was obtained.</summary>
		/// <param name="seqNum">The sequence number. </param>
		/// <returns>
		///     <see langword="true" /> if the writer lock has been granted to any thread since the sequence number was obtained; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600363F RID: 13887 RVA: 0x000BD834 File Offset: 0x000BBA34
		public bool AnyWritersSince(int seqNum)
		{
			bool result;
			lock (this)
			{
				result = (this.seq_num > seqNum);
			}
			return result;
		}

		/// <summary>Restores the lock status of the thread to what it was before <see cref="M:System.Threading.ReaderWriterLock.UpgradeToWriterLock(System.Int32)" /> was called.</summary>
		/// <param name="lockCookie">A <see cref="T:System.Threading.LockCookie" /> returned by <see cref="M:System.Threading.ReaderWriterLock.UpgradeToWriterLock(System.Int32)" />. </param>
		/// <exception cref="T:System.ApplicationException">The thread does not have the writer lock. </exception>
		/// <exception cref="T:System.NullReferenceException">The address of <paramref name="lockCookie" /> is a null pointer. </exception>
		// Token: 0x06003640 RID: 13888 RVA: 0x000BD874 File Offset: 0x000BBA74
		public void DowngradeFromWriterLock(ref LockCookie lockCookie)
		{
			lock (this)
			{
				if (!this.HasWriterLock())
				{
					throw new ApplicationException("The thread does not have the writer lock.");
				}
				if (lockCookie.WriterLocks != 0)
				{
					this.state++;
				}
				else
				{
					this.state = lockCookie.ReaderLocks;
					this.reader_locks[Thread.CurrentThreadId] = this.state;
					if (this.readers > 0)
					{
						Monitor.PulseAll(this);
					}
				}
			}
		}

		/// <summary>Releases the lock, regardless of the number of times the thread acquired the lock.</summary>
		/// <returns>A <see cref="T:System.Threading.LockCookie" /> value representing the released lock.</returns>
		// Token: 0x06003641 RID: 13889 RVA: 0x000BD910 File Offset: 0x000BBB10
		public LockCookie ReleaseLock()
		{
			LockCookie lockCookie;
			lock (this)
			{
				lockCookie = this.GetLockCookie();
				if (lockCookie.WriterLocks != 0)
				{
					this.ReleaseWriterLock(lockCookie.WriterLocks);
				}
				else if (lockCookie.ReaderLocks != 0)
				{
					this.ReleaseReaderLock(lockCookie.ReaderLocks, lockCookie.ReaderLocks);
				}
			}
			return lockCookie;
		}

		/// <summary>Decrements the lock count.</summary>
		/// <exception cref="T:System.ApplicationException">The thread does not have any reader or writer locks. </exception>
		// Token: 0x06003642 RID: 13890 RVA: 0x000BD980 File Offset: 0x000BBB80
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void ReleaseReaderLock()
		{
			lock (this)
			{
				if (!this.HasWriterLock())
				{
					if (this.state > 0)
					{
						object obj = this.reader_locks[Thread.CurrentThreadId];
						if (obj != null)
						{
							this.ReleaseReaderLock((int)obj, 1);
							return;
						}
					}
					throw new ApplicationException("The thread does not have any reader or writer locks.");
				}
				this.ReleaseWriterLock();
			}
		}

		// Token: 0x06003643 RID: 13891 RVA: 0x000BDA00 File Offset: 0x000BBC00
		private void ReleaseReaderLock(int currentCount, int releaseCount)
		{
			int num = currentCount - releaseCount;
			if (num == 0)
			{
				this.reader_locks.Remove(Thread.CurrentThreadId);
			}
			else
			{
				this.reader_locks[Thread.CurrentThreadId] = num;
			}
			this.state -= releaseCount;
			if (this.state == 0 && !this.writer_queue.IsEmpty)
			{
				this.writer_queue.Pulse();
			}
		}

		/// <summary>Decrements the lock count on the writer lock.</summary>
		/// <exception cref="T:System.ApplicationException">The thread does not have the writer lock. </exception>
		// Token: 0x06003644 RID: 13892 RVA: 0x000BDA74 File Offset: 0x000BBC74
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void ReleaseWriterLock()
		{
			lock (this)
			{
				if (!this.HasWriterLock())
				{
					throw new ApplicationException("The thread does not have the writer lock.");
				}
				this.ReleaseWriterLock(1);
			}
		}

		// Token: 0x06003645 RID: 13893 RVA: 0x000BDAC4 File Offset: 0x000BBCC4
		private void ReleaseWriterLock(int releaseCount)
		{
			this.state += releaseCount;
			if (this.state == 0)
			{
				if (this.readers > 0)
				{
					Monitor.PulseAll(this);
					return;
				}
				if (!this.writer_queue.IsEmpty)
				{
					this.writer_queue.Pulse();
				}
			}
		}

		/// <summary>Restores the lock status of the thread to what it was before calling <see cref="M:System.Threading.ReaderWriterLock.ReleaseLock" />.</summary>
		/// <param name="lockCookie">A <see cref="T:System.Threading.LockCookie" /> returned by <see cref="M:System.Threading.ReaderWriterLock.ReleaseLock" />. </param>
		/// <exception cref="T:System.NullReferenceException">The address of <paramref name="lockCookie" /> is a null pointer. </exception>
		// Token: 0x06003646 RID: 13894 RVA: 0x000BDB04 File Offset: 0x000BBD04
		public void RestoreLock(ref LockCookie lockCookie)
		{
			lock (this)
			{
				if (lockCookie.WriterLocks != 0)
				{
					this.AcquireWriterLock(-1, lockCookie.WriterLocks);
				}
				else if (lockCookie.ReaderLocks != 0)
				{
					this.AcquireReaderLock(-1, lockCookie.ReaderLocks);
				}
			}
		}

		/// <summary>Upgrades a reader lock to the writer lock, using an <see langword="Int32" /> value for the time-out.</summary>
		/// <param name="millisecondsTimeout">The time-out in milliseconds. </param>
		/// <returns>A <see cref="T:System.Threading.LockCookie" /> value.</returns>
		/// <exception cref="T:System.ApplicationException">
		///         <paramref name="millisecondsTimeout" />
		///          expires before the lock request is granted. </exception>
		// Token: 0x06003647 RID: 13895 RVA: 0x000BDB68 File Offset: 0x000BBD68
		public LockCookie UpgradeToWriterLock(int millisecondsTimeout)
		{
			LockCookie lockCookie;
			lock (this)
			{
				lockCookie = this.GetLockCookie();
				if (lockCookie.WriterLocks != 0)
				{
					this.state--;
					return lockCookie;
				}
				if (lockCookie.ReaderLocks != 0)
				{
					this.ReleaseReaderLock(lockCookie.ReaderLocks, lockCookie.ReaderLocks);
				}
			}
			this.AcquireWriterLock(millisecondsTimeout);
			return lockCookie;
		}

		/// <summary>Upgrades a reader lock to the writer lock, using a <see langword="TimeSpan" /> value for the time-out.</summary>
		/// <param name="timeout">The <see langword="TimeSpan" /> specifying the time-out period. </param>
		/// <returns>A <see cref="T:System.Threading.LockCookie" /> value.</returns>
		/// <exception cref="T:System.ApplicationException">
		///         <paramref name="timeout" /> expires before the lock request is granted. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="timeout" /> specifies a negative value other than -1 milliseconds. </exception>
		// Token: 0x06003648 RID: 13896 RVA: 0x000BDBE4 File Offset: 0x000BBDE4
		public LockCookie UpgradeToWriterLock(TimeSpan timeout)
		{
			int millisecondsTimeout = this.CheckTimeout(timeout);
			return this.UpgradeToWriterLock(millisecondsTimeout);
		}

		// Token: 0x06003649 RID: 13897 RVA: 0x000BDC00 File Offset: 0x000BBE00
		private LockCookie GetLockCookie()
		{
			LockCookie result = new LockCookie(Thread.CurrentThreadId);
			if (this.HasWriterLock())
			{
				result.WriterLocks = -this.state;
			}
			else
			{
				object obj = this.reader_locks[Thread.CurrentThreadId];
				if (obj != null)
				{
					result.ReaderLocks = (int)obj;
				}
			}
			return result;
		}

		// Token: 0x0600364A RID: 13898 RVA: 0x000BDC58 File Offset: 0x000BBE58
		private bool HasWriterLock()
		{
			return this.state < 0 && Thread.CurrentThreadId == this.writer_lock_owner;
		}

		// Token: 0x0600364B RID: 13899 RVA: 0x000BDC72 File Offset: 0x000BBE72
		private int CheckTimeout(TimeSpan timeout)
		{
			int num = (int)timeout.TotalMilliseconds;
			if (num < -1)
			{
				throw new ArgumentOutOfRangeException("timeout", "Number must be either non-negative or -1");
			}
			return num;
		}

		// Token: 0x04001C80 RID: 7296
		private int seq_num = 1;

		// Token: 0x04001C81 RID: 7297
		private int state;

		// Token: 0x04001C82 RID: 7298
		private int readers;

		// Token: 0x04001C83 RID: 7299
		private int writer_lock_owner;

		// Token: 0x04001C84 RID: 7300
		private LockQueue writer_queue;

		// Token: 0x04001C85 RID: 7301
		private Hashtable reader_locks;
	}
}
