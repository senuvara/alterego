﻿using System;

namespace System.Threading
{
	// Token: 0x0200046D RID: 1133
	internal class SparselyPopulatedArrayFragment<T> where T : class
	{
		// Token: 0x0600333B RID: 13115 RVA: 0x000B66CA File Offset: 0x000B48CA
		internal SparselyPopulatedArrayFragment(int size) : this(size, null)
		{
		}

		// Token: 0x0600333C RID: 13116 RVA: 0x000B66D4 File Offset: 0x000B48D4
		internal SparselyPopulatedArrayFragment(int size, SparselyPopulatedArrayFragment<T> prev)
		{
			this.m_elements = new T[size];
			this.m_freeCount = size;
			this.m_prev = prev;
		}

		// Token: 0x17000889 RID: 2185
		internal T this[int index]
		{
			get
			{
				return Volatile.Read<T>(ref this.m_elements[index]);
			}
		}

		// Token: 0x1700088A RID: 2186
		// (get) Token: 0x0600333E RID: 13118 RVA: 0x000B670D File Offset: 0x000B490D
		internal int Length
		{
			get
			{
				return this.m_elements.Length;
			}
		}

		// Token: 0x1700088B RID: 2187
		// (get) Token: 0x0600333F RID: 13119 RVA: 0x000B6717 File Offset: 0x000B4917
		internal SparselyPopulatedArrayFragment<T> Prev
		{
			get
			{
				return this.m_prev;
			}
		}

		// Token: 0x06003340 RID: 13120 RVA: 0x000B6724 File Offset: 0x000B4924
		internal T SafeAtomicRemove(int index, T expectedElement)
		{
			T t = Interlocked.CompareExchange<T>(ref this.m_elements[index], default(T), expectedElement);
			if (t != null)
			{
				this.m_freeCount++;
			}
			return t;
		}

		// Token: 0x04001B7B RID: 7035
		internal readonly T[] m_elements;

		// Token: 0x04001B7C RID: 7036
		internal volatile int m_freeCount;

		// Token: 0x04001B7D RID: 7037
		internal volatile SparselyPopulatedArrayFragment<T> m_next;

		// Token: 0x04001B7E RID: 7038
		internal volatile SparselyPopulatedArrayFragment<T> m_prev;
	}
}
