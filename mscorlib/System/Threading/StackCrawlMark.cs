﻿using System;

namespace System.Threading
{
	// Token: 0x0200049D RID: 1181
	[Serializable]
	internal enum StackCrawlMark
	{
		// Token: 0x04001C15 RID: 7189
		LookForMe,
		// Token: 0x04001C16 RID: 7190
		LookForMyCaller,
		// Token: 0x04001C17 RID: 7191
		LookForMyCallersCaller,
		// Token: 0x04001C18 RID: 7192
		LookForThread
	}
}
