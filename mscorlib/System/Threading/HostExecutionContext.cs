﻿using System;

namespace System.Threading
{
	/// <summary>Encapsulates and propagates the host execution context across threads. </summary>
	// Token: 0x020004BA RID: 1210
	[MonoTODO("Useless until the runtime supports it")]
	public class HostExecutionContext : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.HostExecutionContext" /> class. </summary>
		// Token: 0x060035DE RID: 13790 RVA: 0x000BD015 File Offset: 0x000BB215
		public HostExecutionContext()
		{
			this._state = null;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.HostExecutionContext" /> class using the specified state. </summary>
		/// <param name="state">An object representing the host execution context state.</param>
		// Token: 0x060035DF RID: 13791 RVA: 0x000BD024 File Offset: 0x000BB224
		public HostExecutionContext(object state)
		{
			this._state = state;
		}

		/// <summary>Creates a copy of the current host execution context.</summary>
		/// <returns>A <see cref="T:System.Threading.HostExecutionContext" /> object representing the host context for the current thread.</returns>
		// Token: 0x060035E0 RID: 13792 RVA: 0x000BD033 File Offset: 0x000BB233
		public virtual HostExecutionContext CreateCopy()
		{
			return new HostExecutionContext(this._state);
		}

		/// <summary>Gets or sets the state of the host execution context.</summary>
		/// <returns>An object representing the host execution context state.</returns>
		// Token: 0x170008DA RID: 2266
		// (get) Token: 0x060035E1 RID: 13793 RVA: 0x000BD040 File Offset: 0x000BB240
		// (set) Token: 0x060035E2 RID: 13794 RVA: 0x000BD048 File Offset: 0x000BB248
		protected internal object State
		{
			get
			{
				return this._state;
			}
			set
			{
				this._state = value;
			}
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.Threading.HostExecutionContext" /> class.</summary>
		// Token: 0x060035E3 RID: 13795 RVA: 0x000BD051 File Offset: 0x000BB251
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>When overridden in a derived class, releases the unmanaged resources used by the <see cref="T:System.Threading.WaitHandle" />, and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x060035E4 RID: 13796 RVA: 0x000020D3 File Offset: 0x000002D3
		public virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x04001C70 RID: 7280
		private object _state;
	}
}
