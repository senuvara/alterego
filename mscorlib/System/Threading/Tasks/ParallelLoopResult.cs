﻿using System;

namespace System.Threading.Tasks
{
	/// <summary>Provides completion status on the execution of a <see cref="T:System.Threading.Tasks.Parallel" /> loop.</summary>
	// Token: 0x020004FF RID: 1279
	public struct ParallelLoopResult
	{
		/// <summary>Gets whether the loop ran to completion, such that all iterations of the loop were executed and the loop didn't receive a request to end prematurely.</summary>
		/// <returns>true if the loop ran to completion; otherwise false;</returns>
		// Token: 0x17000928 RID: 2344
		// (get) Token: 0x06003812 RID: 14354 RVA: 0x000C3C09 File Offset: 0x000C1E09
		public bool IsCompleted
		{
			get
			{
				return this.m_completed;
			}
		}

		/// <summary>Gets the index of the lowest iteration from which <see cref="M:System.Threading.Tasks.ParallelLoopState.Break" /> was called.</summary>
		/// <returns>Returns an integer that represents the lowest iteration from which the Break statement was called.</returns>
		// Token: 0x17000929 RID: 2345
		// (get) Token: 0x06003813 RID: 14355 RVA: 0x000C3C11 File Offset: 0x000C1E11
		public long? LowestBreakIteration
		{
			get
			{
				return this.m_lowestBreakIteration;
			}
		}

		// Token: 0x04001D78 RID: 7544
		internal bool m_completed;

		// Token: 0x04001D79 RID: 7545
		internal long? m_lowestBreakIteration;
	}
}
