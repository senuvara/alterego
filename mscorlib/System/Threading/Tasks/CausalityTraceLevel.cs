﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x020004CE RID: 1230
	[FriendAccessAllowed]
	internal enum CausalityTraceLevel
	{
		// Token: 0x04001CC4 RID: 7364
		Required,
		// Token: 0x04001CC5 RID: 7365
		Important,
		// Token: 0x04001CC6 RID: 7366
		Verbose
	}
}
