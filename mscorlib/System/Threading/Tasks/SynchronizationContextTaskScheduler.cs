﻿using System;
using System.Collections.Generic;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x02000538 RID: 1336
	internal sealed class SynchronizationContextTaskScheduler : TaskScheduler
	{
		// Token: 0x06003A1F RID: 14879 RVA: 0x000CABAC File Offset: 0x000C8DAC
		internal SynchronizationContextTaskScheduler()
		{
			SynchronizationContext synchronizationContext = SynchronizationContext.Current;
			if (synchronizationContext == null)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The current SynchronizationContext may not be used as a TaskScheduler."));
			}
			this.m_synchronizationContext = synchronizationContext;
		}

		// Token: 0x06003A20 RID: 14880 RVA: 0x000CABDF File Offset: 0x000C8DDF
		[SecurityCritical]
		protected internal override void QueueTask(Task task)
		{
			this.m_synchronizationContext.Post(SynchronizationContextTaskScheduler.s_postCallback, task);
		}

		// Token: 0x06003A21 RID: 14881 RVA: 0x000CABF2 File Offset: 0x000C8DF2
		[SecurityCritical]
		protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
		{
			return SynchronizationContext.Current == this.m_synchronizationContext && base.TryExecuteTask(task);
		}

		// Token: 0x06003A22 RID: 14882 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		[SecurityCritical]
		protected override IEnumerable<Task> GetScheduledTasks()
		{
			return null;
		}

		// Token: 0x17000977 RID: 2423
		// (get) Token: 0x06003A23 RID: 14883 RVA: 0x00004E08 File Offset: 0x00003008
		public override int MaximumConcurrencyLevel
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06003A24 RID: 14884 RVA: 0x000CAC0A File Offset: 0x000C8E0A
		private static void PostCallback(object obj)
		{
			((Task)obj).ExecuteEntry(true);
		}

		// Token: 0x06003A25 RID: 14885 RVA: 0x000CAC19 File Offset: 0x000C8E19
		// Note: this type is marked as 'beforefieldinit'.
		static SynchronizationContextTaskScheduler()
		{
		}

		// Token: 0x04001E46 RID: 7750
		private SynchronizationContext m_synchronizationContext;

		// Token: 0x04001E47 RID: 7751
		private static SendOrPostCallback s_postCallback = new SendOrPostCallback(SynchronizationContextTaskScheduler.PostCallback);
	}
}
