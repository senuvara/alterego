﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x020004D0 RID: 1232
	internal enum CausalityRelation
	{
		// Token: 0x04001CCD RID: 7373
		AssignDelegate,
		// Token: 0x04001CCE RID: 7374
		Join,
		// Token: 0x04001CCF RID: 7375
		Choice,
		// Token: 0x04001CD0 RID: 7376
		Cancel,
		// Token: 0x04001CD1 RID: 7377
		Error
	}
}
