﻿using System;
using System.Runtime.CompilerServices;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x0200052D RID: 1325
	internal sealed class TaskSchedulerAwaitTaskContinuation : AwaitTaskContinuation
	{
		// Token: 0x0600397A RID: 14714 RVA: 0x000C8E5E File Offset: 0x000C705E
		[SecurityCritical]
		internal TaskSchedulerAwaitTaskContinuation(TaskScheduler scheduler, Action action, bool flowExecutionContext, ref StackCrawlMark stackMark) : base(action, flowExecutionContext, ref stackMark)
		{
			this.m_scheduler = scheduler;
		}

		// Token: 0x0600397B RID: 14715 RVA: 0x000C8E74 File Offset: 0x000C7074
		internal sealed override void Run(Task ignored, bool canInlineContinuationTask)
		{
			if (this.m_scheduler == TaskScheduler.Default)
			{
				base.Run(ignored, canInlineContinuationTask);
				return;
			}
			bool flag = canInlineContinuationTask && (TaskScheduler.InternalCurrent == this.m_scheduler || Thread.CurrentThread.IsThreadPoolThread);
			Task task = base.CreateTask(delegate(object state)
			{
				try
				{
					((Action)state)();
				}
				catch (Exception exc)
				{
					AwaitTaskContinuation.ThrowAsyncIfNecessary(exc);
				}
			}, this.m_action, this.m_scheduler);
			if (flag)
			{
				TaskContinuation.InlineIfPossibleOrElseQueue(task, false);
				return;
			}
			try
			{
				task.ScheduleAndStart(false);
			}
			catch (TaskSchedulerException)
			{
			}
		}

		// Token: 0x04001E26 RID: 7718
		private readonly TaskScheduler m_scheduler;

		// Token: 0x0200052E RID: 1326
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600397C RID: 14716 RVA: 0x000C8F10 File Offset: 0x000C7110
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600397D RID: 14717 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x0600397E RID: 14718 RVA: 0x000C8F1C File Offset: 0x000C711C
			internal void <Run>b__2_0(object state)
			{
				try
				{
					((Action)state)();
				}
				catch (Exception exc)
				{
					AwaitTaskContinuation.ThrowAsyncIfNecessary(exc);
				}
			}

			// Token: 0x04001E27 RID: 7719
			public static readonly TaskSchedulerAwaitTaskContinuation.<>c <>9 = new TaskSchedulerAwaitTaskContinuation.<>c();

			// Token: 0x04001E28 RID: 7720
			public static Action<object> <>9__2_0;
		}
	}
}
