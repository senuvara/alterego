﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x0200053E RID: 1342
	internal sealed class ThreadPoolTaskScheduler : TaskScheduler
	{
		// Token: 0x06003A3A RID: 14906 RVA: 0x000CADEE File Offset: 0x000C8FEE
		internal ThreadPoolTaskScheduler()
		{
			int id = base.Id;
		}

		// Token: 0x06003A3B RID: 14907 RVA: 0x000CADFD File Offset: 0x000C8FFD
		private static void LongRunningThreadWork(object obj)
		{
			(obj as Task).ExecuteEntry(false);
		}

		// Token: 0x06003A3C RID: 14908 RVA: 0x000CAE0C File Offset: 0x000C900C
		[SecurityCritical]
		protected internal override void QueueTask(Task task)
		{
			if ((task.Options & TaskCreationOptions.LongRunning) != TaskCreationOptions.None)
			{
				new Thread(ThreadPoolTaskScheduler.s_longRunningThreadWork)
				{
					IsBackground = true
				}.Start(task);
				return;
			}
			bool forceGlobal = (task.Options & TaskCreationOptions.PreferFairness) > TaskCreationOptions.None;
			ThreadPool.UnsafeQueueCustomWorkItem(task, forceGlobal);
		}

		// Token: 0x06003A3D RID: 14909 RVA: 0x000CAE50 File Offset: 0x000C9050
		[SecurityCritical]
		protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
		{
			if (taskWasPreviouslyQueued && !ThreadPool.TryPopCustomWorkItem(task))
			{
				return false;
			}
			bool result = false;
			try
			{
				result = task.ExecuteEntry(false);
			}
			finally
			{
				if (taskWasPreviouslyQueued)
				{
					this.NotifyWorkItemProgress();
				}
			}
			return result;
		}

		// Token: 0x06003A3E RID: 14910 RVA: 0x000CAE94 File Offset: 0x000C9094
		[SecurityCritical]
		protected internal override bool TryDequeue(Task task)
		{
			return ThreadPool.TryPopCustomWorkItem(task);
		}

		// Token: 0x06003A3F RID: 14911 RVA: 0x000CAE9C File Offset: 0x000C909C
		[SecurityCritical]
		protected override IEnumerable<Task> GetScheduledTasks()
		{
			return this.FilterTasksFromWorkItems(ThreadPool.GetQueuedWorkItems());
		}

		// Token: 0x06003A40 RID: 14912 RVA: 0x000CAEA9 File Offset: 0x000C90A9
		private IEnumerable<Task> FilterTasksFromWorkItems(IEnumerable<IThreadPoolWorkItem> tpwItems)
		{
			foreach (IThreadPoolWorkItem threadPoolWorkItem in tpwItems)
			{
				if (threadPoolWorkItem is Task)
				{
					yield return (Task)threadPoolWorkItem;
				}
			}
			IEnumerator<IThreadPoolWorkItem> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x06003A41 RID: 14913 RVA: 0x000CAEB9 File Offset: 0x000C90B9
		internal override void NotifyWorkItemProgress()
		{
			ThreadPool.NotifyWorkItemProgress();
		}

		// Token: 0x1700097E RID: 2430
		// (get) Token: 0x06003A42 RID: 14914 RVA: 0x00002526 File Offset: 0x00000726
		internal override bool RequiresAtomicStartTransition
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06003A43 RID: 14915 RVA: 0x000CAEC0 File Offset: 0x000C90C0
		// Note: this type is marked as 'beforefieldinit'.
		static ThreadPoolTaskScheduler()
		{
		}

		// Token: 0x04001E4F RID: 7759
		private static readonly ParameterizedThreadStart s_longRunningThreadWork = new ParameterizedThreadStart(ThreadPoolTaskScheduler.LongRunningThreadWork);

		// Token: 0x0200053F RID: 1343
		[CompilerGenerated]
		private sealed class <FilterTasksFromWorkItems>d__7 : IEnumerable<Task>, IEnumerable, IEnumerator<Task>, IDisposable, IEnumerator
		{
			// Token: 0x06003A44 RID: 14916 RVA: 0x000CAED3 File Offset: 0x000C90D3
			[DebuggerHidden]
			public <FilterTasksFromWorkItems>d__7(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06003A45 RID: 14917 RVA: 0x000CAEF0 File Offset: 0x000C90F0
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06003A46 RID: 14918 RVA: 0x000CAF28 File Offset: 0x000C9128
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = tpwItems.GetEnumerator();
						this.<>1__state = -3;
					}
					while (enumerator.MoveNext())
					{
						IThreadPoolWorkItem threadPoolWorkItem = enumerator.Current;
						if (threadPoolWorkItem is Task)
						{
							this.<>2__current = (Task)threadPoolWorkItem;
							this.<>1__state = 1;
							return true;
						}
					}
					this.<>m__Finally1();
					enumerator = null;
					result = false;
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06003A47 RID: 14919 RVA: 0x000CAFD4 File Offset: 0x000C91D4
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x1700097F RID: 2431
			// (get) Token: 0x06003A48 RID: 14920 RVA: 0x000CAFF0 File Offset: 0x000C91F0
			Task IEnumerator<Task>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06003A49 RID: 14921 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000980 RID: 2432
			// (get) Token: 0x06003A4A RID: 14922 RVA: 0x000CAFF0 File Offset: 0x000C91F0
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06003A4B RID: 14923 RVA: 0x000CAFF8 File Offset: 0x000C91F8
			[DebuggerHidden]
			IEnumerator<Task> IEnumerable<Task>.GetEnumerator()
			{
				ThreadPoolTaskScheduler.<FilterTasksFromWorkItems>d__7 <FilterTasksFromWorkItems>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<FilterTasksFromWorkItems>d__ = this;
				}
				else
				{
					<FilterTasksFromWorkItems>d__ = new ThreadPoolTaskScheduler.<FilterTasksFromWorkItems>d__7(0);
				}
				<FilterTasksFromWorkItems>d__.tpwItems = tpwItems;
				return <FilterTasksFromWorkItems>d__;
			}

			// Token: 0x06003A4C RID: 14924 RVA: 0x000CB03B File Offset: 0x000C923B
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Threading.Tasks.Task>.GetEnumerator();
			}

			// Token: 0x04001E50 RID: 7760
			private int <>1__state;

			// Token: 0x04001E51 RID: 7761
			private Task <>2__current;

			// Token: 0x04001E52 RID: 7762
			private int <>l__initialThreadId;

			// Token: 0x04001E53 RID: 7763
			private IEnumerable<IThreadPoolWorkItem> tpwItems;

			// Token: 0x04001E54 RID: 7764
			public IEnumerable<IThreadPoolWorkItem> <>3__tpwItems;

			// Token: 0x04001E55 RID: 7765
			private IEnumerator<IThreadPoolWorkItem> <>7__wrap1;
		}
	}
}
