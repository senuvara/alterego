﻿using System;

namespace System.Threading.Tasks
{
	/// <summary>Provides data for the event that is raised when a faulted <see cref="T:System.Threading.Tasks.Task" />'s exception goes unobserved.</summary>
	// Token: 0x02000539 RID: 1337
	public class UnobservedTaskExceptionEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.UnobservedTaskExceptionEventArgs" /> class with the unobserved exception.</summary>
		/// <param name="exception">The Exception that has gone unobserved.</param>
		// Token: 0x06003A26 RID: 14886 RVA: 0x000CAC2C File Offset: 0x000C8E2C
		public UnobservedTaskExceptionEventArgs(AggregateException exception)
		{
			this.m_exception = exception;
		}

		/// <summary>Marks the <see cref="P:System.Threading.Tasks.UnobservedTaskExceptionEventArgs.Exception" /> as "observed," thus preventing it from triggering exception escalation policy which, by default, terminates the process.</summary>
		// Token: 0x06003A27 RID: 14887 RVA: 0x000CAC3B File Offset: 0x000C8E3B
		public void SetObserved()
		{
			this.m_observed = true;
		}

		/// <summary>Gets whether this exception has been marked as "observed."</summary>
		/// <returns>true if this exception has been marked as "observed"; otherwise false.</returns>
		// Token: 0x17000978 RID: 2424
		// (get) Token: 0x06003A28 RID: 14888 RVA: 0x000CAC44 File Offset: 0x000C8E44
		public bool Observed
		{
			get
			{
				return this.m_observed;
			}
		}

		/// <summary>The Exception that went unobserved.</summary>
		/// <returns>The Exception that went unobserved.</returns>
		// Token: 0x17000979 RID: 2425
		// (get) Token: 0x06003A29 RID: 14889 RVA: 0x000CAC4C File Offset: 0x000C8E4C
		public AggregateException Exception
		{
			get
			{
				return this.m_exception;
			}
		}

		// Token: 0x04001E48 RID: 7752
		private AggregateException m_exception;

		// Token: 0x04001E49 RID: 7753
		internal bool m_observed;
	}
}
