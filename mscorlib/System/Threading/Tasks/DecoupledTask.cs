﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x02000541 RID: 1345
	internal sealed class DecoupledTask : IDecoupledTask
	{
		// Token: 0x06003A4E RID: 14926 RVA: 0x000CB043 File Offset: 0x000C9243
		public DecoupledTask(Task task)
		{
			this.Task = task;
		}

		// Token: 0x17000982 RID: 2434
		// (get) Token: 0x06003A4F RID: 14927 RVA: 0x000CB052 File Offset: 0x000C9252
		public bool IsCompleted
		{
			get
			{
				return this.Task.IsCompleted;
			}
		}

		// Token: 0x17000983 RID: 2435
		// (get) Token: 0x06003A50 RID: 14928 RVA: 0x000CB05F File Offset: 0x000C925F
		// (set) Token: 0x06003A51 RID: 14929 RVA: 0x000CB067 File Offset: 0x000C9267
		public Task Task
		{
			[CompilerGenerated]
			get
			{
				return this.<Task>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Task>k__BackingField = value;
			}
		}

		// Token: 0x04001E56 RID: 7766
		[CompilerGenerated]
		private Task <Task>k__BackingField;
	}
}
