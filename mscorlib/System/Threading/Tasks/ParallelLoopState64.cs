﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x020004FB RID: 1275
	internal class ParallelLoopState64 : ParallelLoopState
	{
		// Token: 0x060037FA RID: 14330 RVA: 0x000C38F0 File Offset: 0x000C1AF0
		internal ParallelLoopState64(ParallelLoopStateFlags64 sharedParallelStateFlags) : base(sharedParallelStateFlags)
		{
			this.m_sharedParallelStateFlags = sharedParallelStateFlags;
		}

		// Token: 0x17000920 RID: 2336
		// (get) Token: 0x060037FB RID: 14331 RVA: 0x000C3900 File Offset: 0x000C1B00
		// (set) Token: 0x060037FC RID: 14332 RVA: 0x000C3908 File Offset: 0x000C1B08
		internal long CurrentIteration
		{
			get
			{
				return this.m_currentIteration;
			}
			set
			{
				this.m_currentIteration = value;
			}
		}

		// Token: 0x17000921 RID: 2337
		// (get) Token: 0x060037FD RID: 14333 RVA: 0x000C3911 File Offset: 0x000C1B11
		internal override bool InternalShouldExitCurrentIteration
		{
			get
			{
				return this.m_sharedParallelStateFlags.ShouldExitLoop(this.CurrentIteration);
			}
		}

		// Token: 0x17000922 RID: 2338
		// (get) Token: 0x060037FE RID: 14334 RVA: 0x000C3924 File Offset: 0x000C1B24
		internal override long? InternalLowestBreakIteration
		{
			get
			{
				return this.m_sharedParallelStateFlags.NullableLowestBreakIteration;
			}
		}

		// Token: 0x060037FF RID: 14335 RVA: 0x000C3931 File Offset: 0x000C1B31
		internal override void InternalBreak()
		{
			ParallelLoopState.Break(this.CurrentIteration, this.m_sharedParallelStateFlags);
		}

		// Token: 0x04001D6E RID: 7534
		private ParallelLoopStateFlags64 m_sharedParallelStateFlags;

		// Token: 0x04001D6F RID: 7535
		private long m_currentIteration;
	}
}
