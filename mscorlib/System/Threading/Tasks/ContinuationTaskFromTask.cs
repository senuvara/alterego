﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x02000525 RID: 1317
	internal sealed class ContinuationTaskFromTask : Task
	{
		// Token: 0x06003963 RID: 14691 RVA: 0x000C8938 File Offset: 0x000C6B38
		public ContinuationTaskFromTask(Task antecedent, Delegate action, object state, TaskCreationOptions creationOptions, InternalTaskOptions internalOptions, ref StackCrawlMark stackMark) : base(action, state, Task.InternalCurrentIfAttached(creationOptions), default(CancellationToken), creationOptions, internalOptions, null)
		{
			this.m_antecedent = antecedent;
			base.PossiblyCaptureContext(ref stackMark);
		}

		// Token: 0x06003964 RID: 14692 RVA: 0x000C8974 File Offset: 0x000C6B74
		internal override void InnerInvoke()
		{
			Task antecedent = this.m_antecedent;
			this.m_antecedent = null;
			antecedent.NotifyDebuggerOfWaitCompletionIfNecessary();
			Action<Task> action = this.m_action as Action<Task>;
			if (action != null)
			{
				action(antecedent);
				return;
			}
			Action<Task, object> action2 = this.m_action as Action<Task, object>;
			if (action2 != null)
			{
				action2(antecedent, this.m_stateObject);
				return;
			}
		}

		// Token: 0x04001E1B RID: 7707
		private Task m_antecedent;
	}
}
