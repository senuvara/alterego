﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x020004FE RID: 1278
	internal class ParallelLoopStateFlags64 : ParallelLoopStateFlags
	{
		// Token: 0x17000926 RID: 2342
		// (get) Token: 0x0600380D RID: 14349 RVA: 0x000C3B0F File Offset: 0x000C1D0F
		internal long LowestBreakIteration
		{
			get
			{
				if (IntPtr.Size >= 8)
				{
					return this.m_lowestBreakIteration;
				}
				return Interlocked.Read(ref this.m_lowestBreakIteration);
			}
		}

		// Token: 0x17000927 RID: 2343
		// (get) Token: 0x0600380E RID: 14350 RVA: 0x000C3B2C File Offset: 0x000C1D2C
		internal long? NullableLowestBreakIteration
		{
			get
			{
				if (this.m_lowestBreakIteration == 9223372036854775807L)
				{
					return null;
				}
				if (IntPtr.Size >= 8)
				{
					return new long?(this.m_lowestBreakIteration);
				}
				return new long?(Interlocked.Read(ref this.m_lowestBreakIteration));
			}
		}

		// Token: 0x0600380F RID: 14351 RVA: 0x000C3B78 File Offset: 0x000C1D78
		internal bool ShouldExitLoop(long CallerIteration)
		{
			int loopStateFlags = base.LoopStateFlags;
			return loopStateFlags != ParallelLoopStateFlags.PLS_NONE && ((loopStateFlags & (ParallelLoopStateFlags.PLS_EXCEPTIONAL | ParallelLoopStateFlags.PLS_STOPPED | ParallelLoopStateFlags.PLS_CANCELED)) != 0 || ((loopStateFlags & ParallelLoopStateFlags.PLS_BROKEN) != 0 && CallerIteration > this.LowestBreakIteration));
		}

		// Token: 0x06003810 RID: 14352 RVA: 0x000C3BC4 File Offset: 0x000C1DC4
		internal bool ShouldExitLoop()
		{
			int loopStateFlags = base.LoopStateFlags;
			return loopStateFlags != ParallelLoopStateFlags.PLS_NONE && (loopStateFlags & (ParallelLoopStateFlags.PLS_EXCEPTIONAL | ParallelLoopStateFlags.PLS_CANCELED)) != 0;
		}

		// Token: 0x06003811 RID: 14353 RVA: 0x000C3BF2 File Offset: 0x000C1DF2
		public ParallelLoopStateFlags64()
		{
		}

		// Token: 0x04001D77 RID: 7543
		internal long m_lowestBreakIteration = long.MaxValue;
	}
}
