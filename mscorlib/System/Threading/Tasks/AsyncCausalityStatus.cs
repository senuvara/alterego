﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x020004CF RID: 1231
	[FriendAccessAllowed]
	internal enum AsyncCausalityStatus
	{
		// Token: 0x04001CC8 RID: 7368
		Started,
		// Token: 0x04001CC9 RID: 7369
		Completed,
		// Token: 0x04001CCA RID: 7370
		Canceled,
		// Token: 0x04001CCB RID: 7371
		Error
	}
}
