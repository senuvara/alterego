﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x020004D2 RID: 1234
	[FriendAccessAllowed]
	internal static class AsyncCausalityTracer
	{
		// Token: 0x060036AC RID: 13996 RVA: 0x000020D3 File Offset: 0x000002D3
		internal static void EnableToETW(bool enabled)
		{
		}

		// Token: 0x170008EB RID: 2283
		// (get) Token: 0x060036AD RID: 13997 RVA: 0x00002526 File Offset: 0x00000726
		[FriendAccessAllowed]
		internal static bool LoggingOn
		{
			[FriendAccessAllowed]
			get
			{
				return false;
			}
		}

		// Token: 0x060036AE RID: 13998 RVA: 0x000020D3 File Offset: 0x000002D3
		[FriendAccessAllowed]
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static void TraceOperationCreation(CausalityTraceLevel traceLevel, int taskId, string operationName, ulong relatedContext)
		{
		}

		// Token: 0x060036AF RID: 13999 RVA: 0x000020D3 File Offset: 0x000002D3
		[FriendAccessAllowed]
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static void TraceOperationCompletion(CausalityTraceLevel traceLevel, int taskId, AsyncCausalityStatus status)
		{
		}

		// Token: 0x060036B0 RID: 14000 RVA: 0x000020D3 File Offset: 0x000002D3
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static void TraceOperationRelation(CausalityTraceLevel traceLevel, int taskId, CausalityRelation relation)
		{
		}

		// Token: 0x060036B1 RID: 14001 RVA: 0x000020D3 File Offset: 0x000002D3
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static void TraceSynchronousWorkStart(CausalityTraceLevel traceLevel, int taskId, CausalitySynchronousWork work)
		{
		}

		// Token: 0x060036B2 RID: 14002 RVA: 0x000020D3 File Offset: 0x000002D3
		[MethodImpl(MethodImplOptions.NoInlining)]
		internal static void TraceSynchronousWorkCompletion(CausalityTraceLevel traceLevel, CausalitySynchronousWork work)
		{
		}

		// Token: 0x060036B3 RID: 14003 RVA: 0x000BE953 File Offset: 0x000BCB53
		private static ulong GetOperationId(uint taskId)
		{
			return (ulong)(((long)AppDomain.CurrentDomain.Id << 32) + (long)((ulong)taskId));
		}
	}
}
