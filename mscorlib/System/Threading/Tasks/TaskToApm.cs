﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x0200053B RID: 1339
	internal static class TaskToApm
	{
		// Token: 0x06003A2F RID: 14895 RVA: 0x000CAC7C File Offset: 0x000C8E7C
		public static IAsyncResult Begin(Task task, AsyncCallback callback, object state)
		{
			IAsyncResult asyncResult;
			if (task.IsCompleted)
			{
				asyncResult = new TaskToApm.TaskWrapperAsyncResult(task, state, true);
				if (callback != null)
				{
					callback(asyncResult);
				}
			}
			else
			{
				IAsyncResult asyncResult3;
				if (task.AsyncState != state)
				{
					IAsyncResult asyncResult2 = new TaskToApm.TaskWrapperAsyncResult(task, state, false);
					asyncResult3 = asyncResult2;
				}
				else
				{
					asyncResult3 = task;
				}
				asyncResult = asyncResult3;
				if (callback != null)
				{
					TaskToApm.InvokeCallbackWhenTaskCompletes(task, callback, asyncResult);
				}
			}
			return asyncResult;
		}

		// Token: 0x06003A30 RID: 14896 RVA: 0x000CACCC File Offset: 0x000C8ECC
		public static void End(IAsyncResult asyncResult)
		{
			TaskToApm.TaskWrapperAsyncResult taskWrapperAsyncResult = asyncResult as TaskToApm.TaskWrapperAsyncResult;
			Task task;
			if (taskWrapperAsyncResult != null)
			{
				task = taskWrapperAsyncResult.Task;
			}
			else
			{
				task = (asyncResult as Task);
			}
			if (task == null)
			{
				__Error.WrongAsyncResult();
			}
			task.GetAwaiter().GetResult();
		}

		// Token: 0x06003A31 RID: 14897 RVA: 0x000CAD0C File Offset: 0x000C8F0C
		public static TResult End<TResult>(IAsyncResult asyncResult)
		{
			TaskToApm.TaskWrapperAsyncResult taskWrapperAsyncResult = asyncResult as TaskToApm.TaskWrapperAsyncResult;
			Task<TResult> task;
			if (taskWrapperAsyncResult != null)
			{
				task = (taskWrapperAsyncResult.Task as Task<TResult>);
			}
			else
			{
				task = (asyncResult as Task<TResult>);
			}
			if (task == null)
			{
				__Error.WrongAsyncResult();
			}
			return task.GetAwaiter().GetResult();
		}

		// Token: 0x06003A32 RID: 14898 RVA: 0x000CAD50 File Offset: 0x000C8F50
		private static void InvokeCallbackWhenTaskCompletes(Task antecedent, AsyncCallback callback, IAsyncResult asyncResult)
		{
			antecedent.ConfigureAwait(false).GetAwaiter().OnCompleted(delegate
			{
				callback(asyncResult);
			});
		}

		// Token: 0x0200053C RID: 1340
		private sealed class TaskWrapperAsyncResult : IAsyncResult
		{
			// Token: 0x06003A33 RID: 14899 RVA: 0x000CAD94 File Offset: 0x000C8F94
			internal TaskWrapperAsyncResult(Task task, object state, bool completedSynchronously)
			{
				this.Task = task;
				this.m_state = state;
				this.m_completedSynchronously = completedSynchronously;
			}

			// Token: 0x1700097A RID: 2426
			// (get) Token: 0x06003A34 RID: 14900 RVA: 0x000CADB1 File Offset: 0x000C8FB1
			object IAsyncResult.AsyncState
			{
				get
				{
					return this.m_state;
				}
			}

			// Token: 0x1700097B RID: 2427
			// (get) Token: 0x06003A35 RID: 14901 RVA: 0x000CADB9 File Offset: 0x000C8FB9
			bool IAsyncResult.CompletedSynchronously
			{
				get
				{
					return this.m_completedSynchronously;
				}
			}

			// Token: 0x1700097C RID: 2428
			// (get) Token: 0x06003A36 RID: 14902 RVA: 0x000CADC1 File Offset: 0x000C8FC1
			bool IAsyncResult.IsCompleted
			{
				get
				{
					return this.Task.IsCompleted;
				}
			}

			// Token: 0x1700097D RID: 2429
			// (get) Token: 0x06003A37 RID: 14903 RVA: 0x000CADCE File Offset: 0x000C8FCE
			WaitHandle IAsyncResult.AsyncWaitHandle
			{
				get
				{
					return ((IAsyncResult)this.Task).AsyncWaitHandle;
				}
			}

			// Token: 0x04001E4A RID: 7754
			internal readonly Task Task;

			// Token: 0x04001E4B RID: 7755
			private readonly object m_state;

			// Token: 0x04001E4C RID: 7756
			private readonly bool m_completedSynchronously;
		}

		// Token: 0x0200053D RID: 1341
		[CompilerGenerated]
		private sealed class <>c__DisplayClass3_0
		{
			// Token: 0x06003A38 RID: 14904 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass3_0()
			{
			}

			// Token: 0x06003A39 RID: 14905 RVA: 0x000CADDB File Offset: 0x000C8FDB
			internal void <InvokeCallbackWhenTaskCompletes>b__0()
			{
				this.callback(this.asyncResult);
			}

			// Token: 0x04001E4D RID: 7757
			public AsyncCallback callback;

			// Token: 0x04001E4E RID: 7758
			public IAsyncResult asyncResult;
		}
	}
}
