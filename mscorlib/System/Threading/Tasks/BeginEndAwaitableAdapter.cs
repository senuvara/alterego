﻿using System;
using System.Runtime.CompilerServices;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x020004D3 RID: 1235
	internal sealed class BeginEndAwaitableAdapter : ICriticalNotifyCompletion, INotifyCompletion
	{
		// Token: 0x060036B4 RID: 14004 RVA: 0x00002058 File Offset: 0x00000258
		public BeginEndAwaitableAdapter GetAwaiter()
		{
			return this;
		}

		// Token: 0x170008EC RID: 2284
		// (get) Token: 0x060036B5 RID: 14005 RVA: 0x000BE966 File Offset: 0x000BCB66
		public bool IsCompleted
		{
			get
			{
				return this._continuation == BeginEndAwaitableAdapter.CALLBACK_RAN;
			}
		}

		// Token: 0x060036B6 RID: 14006 RVA: 0x000BE978 File Offset: 0x000BCB78
		[SecurityCritical]
		public void UnsafeOnCompleted(Action continuation)
		{
			this.OnCompleted(continuation);
		}

		// Token: 0x060036B7 RID: 14007 RVA: 0x000BE981 File Offset: 0x000BCB81
		public void OnCompleted(Action continuation)
		{
			if (this._continuation == BeginEndAwaitableAdapter.CALLBACK_RAN || Interlocked.CompareExchange<Action>(ref this._continuation, continuation, null) == BeginEndAwaitableAdapter.CALLBACK_RAN)
			{
				Task.Run(continuation);
			}
		}

		// Token: 0x060036B8 RID: 14008 RVA: 0x000BE9B5 File Offset: 0x000BCBB5
		public IAsyncResult GetResult()
		{
			IAsyncResult asyncResult = this._asyncResult;
			this._asyncResult = null;
			this._continuation = null;
			return asyncResult;
		}

		// Token: 0x060036B9 RID: 14009 RVA: 0x00002050 File Offset: 0x00000250
		public BeginEndAwaitableAdapter()
		{
		}

		// Token: 0x060036BA RID: 14010 RVA: 0x000BE9CB File Offset: 0x000BCBCB
		// Note: this type is marked as 'beforefieldinit'.
		static BeginEndAwaitableAdapter()
		{
		}

		// Token: 0x04001CD6 RID: 7382
		private static readonly Action CALLBACK_RAN = delegate()
		{
		};

		// Token: 0x04001CD7 RID: 7383
		private IAsyncResult _asyncResult;

		// Token: 0x04001CD8 RID: 7384
		private Action _continuation;

		// Token: 0x04001CD9 RID: 7385
		public static readonly AsyncCallback Callback = delegate(IAsyncResult asyncResult)
		{
			BeginEndAwaitableAdapter beginEndAwaitableAdapter = (BeginEndAwaitableAdapter)asyncResult.AsyncState;
			beginEndAwaitableAdapter._asyncResult = asyncResult;
			Action action = Interlocked.Exchange<Action>(ref beginEndAwaitableAdapter._continuation, BeginEndAwaitableAdapter.CALLBACK_RAN);
			if (action != null)
			{
				action();
			}
		};

		// Token: 0x020004D4 RID: 1236
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060036BB RID: 14011 RVA: 0x000BE9F7 File Offset: 0x000BCBF7
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060036BC RID: 14012 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x060036BD RID: 14013 RVA: 0x000020D3 File Offset: 0x000002D3
			internal void <.cctor>b__11_0()
			{
			}

			// Token: 0x060036BE RID: 14014 RVA: 0x000BEA04 File Offset: 0x000BCC04
			internal void <.cctor>b__11_1(IAsyncResult asyncResult)
			{
				BeginEndAwaitableAdapter beginEndAwaitableAdapter = (BeginEndAwaitableAdapter)asyncResult.AsyncState;
				beginEndAwaitableAdapter._asyncResult = asyncResult;
				Action action = Interlocked.Exchange<Action>(ref beginEndAwaitableAdapter._continuation, BeginEndAwaitableAdapter.CALLBACK_RAN);
				if (action != null)
				{
					action();
				}
			}

			// Token: 0x04001CDA RID: 7386
			public static readonly BeginEndAwaitableAdapter.<>c <>9 = new BeginEndAwaitableAdapter.<>c();
		}
	}
}
