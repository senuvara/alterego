﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Threading.Tasks
{
	// Token: 0x020004CD RID: 1229
	[AsyncMethodBuilder(typeof(AsyncValueTaskMethodBuilder<>))]
	[StructLayout(LayoutKind.Auto)]
	public struct ValueTask<TResult> : IEquatable<ValueTask<TResult>>
	{
		// Token: 0x0600369B RID: 13979 RVA: 0x000BE72E File Offset: 0x000BC92E
		public ValueTask(TResult result)
		{
			this._task = null;
			this._result = result;
		}

		// Token: 0x0600369C RID: 13980 RVA: 0x000BE73E File Offset: 0x000BC93E
		public ValueTask(Task<TResult> task)
		{
			if (task == null)
			{
				throw new ArgumentNullException("task");
			}
			this._task = task;
			this._result = default(TResult);
		}

		// Token: 0x0600369D RID: 13981 RVA: 0x000BE764 File Offset: 0x000BC964
		public override int GetHashCode()
		{
			if (this._task != null)
			{
				return this._task.GetHashCode();
			}
			if (this._result == null)
			{
				return 0;
			}
			TResult result = this._result;
			return result.GetHashCode();
		}

		// Token: 0x0600369E RID: 13982 RVA: 0x000BE7A8 File Offset: 0x000BC9A8
		public override bool Equals(object obj)
		{
			return obj is ValueTask<TResult> && this.Equals((ValueTask<TResult>)obj);
		}

		// Token: 0x0600369F RID: 13983 RVA: 0x000BE7C0 File Offset: 0x000BC9C0
		public bool Equals(ValueTask<TResult> other)
		{
			if (this._task == null && other._task == null)
			{
				return EqualityComparer<TResult>.Default.Equals(this._result, other._result);
			}
			return this._task == other._task;
		}

		// Token: 0x060036A0 RID: 13984 RVA: 0x000BE7F7 File Offset: 0x000BC9F7
		public static bool operator ==(ValueTask<TResult> left, ValueTask<TResult> right)
		{
			return left.Equals(right);
		}

		// Token: 0x060036A1 RID: 13985 RVA: 0x000BE801 File Offset: 0x000BCA01
		public static bool operator !=(ValueTask<TResult> left, ValueTask<TResult> right)
		{
			return !left.Equals(right);
		}

		// Token: 0x060036A2 RID: 13986 RVA: 0x000BE80E File Offset: 0x000BCA0E
		public Task<TResult> AsTask()
		{
			return this._task ?? Task.FromResult<TResult>(this._result);
		}

		// Token: 0x170008E6 RID: 2278
		// (get) Token: 0x060036A3 RID: 13987 RVA: 0x000BE825 File Offset: 0x000BCA25
		public bool IsCompleted
		{
			get
			{
				return this._task == null || this._task.IsCompleted;
			}
		}

		// Token: 0x170008E7 RID: 2279
		// (get) Token: 0x060036A4 RID: 13988 RVA: 0x000BE83C File Offset: 0x000BCA3C
		public bool IsCompletedSuccessfully
		{
			get
			{
				return this._task == null || this._task.Status == TaskStatus.RanToCompletion;
			}
		}

		// Token: 0x170008E8 RID: 2280
		// (get) Token: 0x060036A5 RID: 13989 RVA: 0x000BE856 File Offset: 0x000BCA56
		public bool IsFaulted
		{
			get
			{
				return this._task != null && this._task.IsFaulted;
			}
		}

		// Token: 0x170008E9 RID: 2281
		// (get) Token: 0x060036A6 RID: 13990 RVA: 0x000BE86D File Offset: 0x000BCA6D
		public bool IsCanceled
		{
			get
			{
				return this._task != null && this._task.IsCanceled;
			}
		}

		// Token: 0x170008EA RID: 2282
		// (get) Token: 0x060036A7 RID: 13991 RVA: 0x000BE884 File Offset: 0x000BCA84
		public TResult Result
		{
			get
			{
				if (this._task != null)
				{
					return this._task.GetAwaiter().GetResult();
				}
				return this._result;
			}
		}

		// Token: 0x060036A8 RID: 13992 RVA: 0x000BE8B3 File Offset: 0x000BCAB3
		public ValueTaskAwaiter<TResult> GetAwaiter()
		{
			return new ValueTaskAwaiter<TResult>(this);
		}

		// Token: 0x060036A9 RID: 13993 RVA: 0x000BE8C0 File Offset: 0x000BCAC0
		public ConfiguredValueTaskAwaitable<TResult> ConfigureAwait(bool continueOnCapturedContext)
		{
			return new ConfiguredValueTaskAwaitable<TResult>(this, continueOnCapturedContext);
		}

		// Token: 0x060036AA RID: 13994 RVA: 0x000BE8D0 File Offset: 0x000BCAD0
		public override string ToString()
		{
			if (this._task != null)
			{
				if (this._task.Status != TaskStatus.RanToCompletion || this._task.Result == null)
				{
					return string.Empty;
				}
				TResult result = this._task.Result;
				return result.ToString();
			}
			else
			{
				if (this._result == null)
				{
					return string.Empty;
				}
				TResult result = this._result;
				return result.ToString();
			}
		}

		// Token: 0x060036AB RID: 13995 RVA: 0x000BE94C File Offset: 0x000BCB4C
		public static AsyncValueTaskMethodBuilder<TResult> CreateAsyncMethodBuilder()
		{
			return AsyncValueTaskMethodBuilder<TResult>.Create();
		}

		// Token: 0x04001CC1 RID: 7361
		internal readonly Task<TResult> _task;

		// Token: 0x04001CC2 RID: 7362
		internal readonly TResult _result;
	}
}
