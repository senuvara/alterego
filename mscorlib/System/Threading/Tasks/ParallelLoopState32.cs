﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x020004FA RID: 1274
	internal class ParallelLoopState32 : ParallelLoopState
	{
		// Token: 0x060037F4 RID: 14324 RVA: 0x000C389C File Offset: 0x000C1A9C
		internal ParallelLoopState32(ParallelLoopStateFlags32 sharedParallelStateFlags) : base(sharedParallelStateFlags)
		{
			this.m_sharedParallelStateFlags = sharedParallelStateFlags;
		}

		// Token: 0x1700091D RID: 2333
		// (get) Token: 0x060037F5 RID: 14325 RVA: 0x000C38AC File Offset: 0x000C1AAC
		// (set) Token: 0x060037F6 RID: 14326 RVA: 0x000C38B4 File Offset: 0x000C1AB4
		internal int CurrentIteration
		{
			get
			{
				return this.m_currentIteration;
			}
			set
			{
				this.m_currentIteration = value;
			}
		}

		// Token: 0x1700091E RID: 2334
		// (get) Token: 0x060037F7 RID: 14327 RVA: 0x000C38BD File Offset: 0x000C1ABD
		internal override bool InternalShouldExitCurrentIteration
		{
			get
			{
				return this.m_sharedParallelStateFlags.ShouldExitLoop(this.CurrentIteration);
			}
		}

		// Token: 0x1700091F RID: 2335
		// (get) Token: 0x060037F8 RID: 14328 RVA: 0x000C38D0 File Offset: 0x000C1AD0
		internal override long? InternalLowestBreakIteration
		{
			get
			{
				return this.m_sharedParallelStateFlags.NullableLowestBreakIteration;
			}
		}

		// Token: 0x060037F9 RID: 14329 RVA: 0x000C38DD File Offset: 0x000C1ADD
		internal override void InternalBreak()
		{
			ParallelLoopState.Break(this.CurrentIteration, this.m_sharedParallelStateFlags);
		}

		// Token: 0x04001D6C RID: 7532
		private ParallelLoopStateFlags32 m_sharedParallelStateFlags;

		// Token: 0x04001D6D RID: 7533
		private int m_currentIteration;
	}
}
