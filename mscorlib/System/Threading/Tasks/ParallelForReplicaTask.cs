﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x0200051A RID: 1306
	internal class ParallelForReplicaTask : Task
	{
		// Token: 0x06003937 RID: 14647 RVA: 0x000C82F0 File Offset: 0x000C64F0
		internal ParallelForReplicaTask(Action<object> taskReplicaDelegate, object stateObject, Task parentTask, TaskScheduler taskScheduler, TaskCreationOptions creationOptionsForReplica, InternalTaskOptions internalOptionsForReplica) : base(taskReplicaDelegate, stateObject, parentTask, default(CancellationToken), creationOptionsForReplica, internalOptionsForReplica, taskScheduler)
		{
		}

		// Token: 0x17000960 RID: 2400
		// (get) Token: 0x06003938 RID: 14648 RVA: 0x000C8315 File Offset: 0x000C6515
		// (set) Token: 0x06003939 RID: 14649 RVA: 0x000C831D File Offset: 0x000C651D
		internal override object SavedStateForNextReplica
		{
			get
			{
				return this.m_stateForNextReplica;
			}
			set
			{
				this.m_stateForNextReplica = value;
			}
		}

		// Token: 0x17000961 RID: 2401
		// (get) Token: 0x0600393A RID: 14650 RVA: 0x000C8326 File Offset: 0x000C6526
		// (set) Token: 0x0600393B RID: 14651 RVA: 0x000C832E File Offset: 0x000C652E
		internal override object SavedStateFromPreviousReplica
		{
			get
			{
				return this.m_stateFromPreviousReplica;
			}
			set
			{
				this.m_stateFromPreviousReplica = value;
			}
		}

		// Token: 0x17000962 RID: 2402
		// (get) Token: 0x0600393C RID: 14652 RVA: 0x000C8337 File Offset: 0x000C6537
		// (set) Token: 0x0600393D RID: 14653 RVA: 0x000C833F File Offset: 0x000C653F
		internal override Task HandedOverChildReplica
		{
			get
			{
				return this.m_handedOverChildReplica;
			}
			set
			{
				this.m_handedOverChildReplica = value;
			}
		}

		// Token: 0x04001DEB RID: 7659
		internal object m_stateForNextReplica;

		// Token: 0x04001DEC RID: 7660
		internal object m_stateFromPreviousReplica;

		// Token: 0x04001DED RID: 7661
		internal Task m_handedOverChildReplica;
	}
}
