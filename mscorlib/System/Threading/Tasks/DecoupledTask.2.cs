﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x02000542 RID: 1346
	internal sealed class DecoupledTask<T> : IDecoupledTask
	{
		// Token: 0x06003A52 RID: 14930 RVA: 0x000CB070 File Offset: 0x000C9270
		public DecoupledTask(Task<T> task)
		{
			this.Task = task;
		}

		// Token: 0x17000984 RID: 2436
		// (get) Token: 0x06003A53 RID: 14931 RVA: 0x000CB07F File Offset: 0x000C927F
		public bool IsCompleted
		{
			get
			{
				return this.Task.IsCompleted;
			}
		}

		// Token: 0x17000985 RID: 2437
		// (get) Token: 0x06003A54 RID: 14932 RVA: 0x000CB08C File Offset: 0x000C928C
		// (set) Token: 0x06003A55 RID: 14933 RVA: 0x000CB094 File Offset: 0x000C9294
		public Task<T> Task
		{
			[CompilerGenerated]
			get
			{
				return this.<Task>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Task>k__BackingField = value;
			}
		}

		// Token: 0x04001E57 RID: 7767
		[CompilerGenerated]
		private Task<T> <Task>k__BackingField;
	}
}
