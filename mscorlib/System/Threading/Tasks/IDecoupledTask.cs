﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x02000540 RID: 1344
	internal interface IDecoupledTask
	{
		// Token: 0x17000981 RID: 2433
		// (get) Token: 0x06003A4D RID: 14925
		bool IsCompleted { get; }
	}
}
