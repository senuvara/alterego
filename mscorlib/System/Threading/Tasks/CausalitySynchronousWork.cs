﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x020004D1 RID: 1233
	internal enum CausalitySynchronousWork
	{
		// Token: 0x04001CD3 RID: 7379
		CompletionNotification,
		// Token: 0x04001CD4 RID: 7380
		ProgressNotification,
		// Token: 0x04001CD5 RID: 7381
		Execution
	}
}
