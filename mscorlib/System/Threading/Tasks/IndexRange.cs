﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading.Tasks
{
	// Token: 0x02000500 RID: 1280
	[StructLayout(LayoutKind.Auto)]
	internal struct IndexRange
	{
		// Token: 0x04001D7A RID: 7546
		internal long m_nFromInclusive;

		// Token: 0x04001D7B RID: 7547
		internal long m_nToExclusive;

		// Token: 0x04001D7C RID: 7548
		internal volatile Shared<long> m_nSharedCurrentIndexOffset;

		// Token: 0x04001D7D RID: 7549
		internal int m_bRangeFinished;
	}
}
