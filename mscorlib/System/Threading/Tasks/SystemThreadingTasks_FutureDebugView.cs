﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x020004DE RID: 1246
	internal class SystemThreadingTasks_FutureDebugView<TResult>
	{
		// Token: 0x0600372F RID: 14127 RVA: 0x000BFD64 File Offset: 0x000BDF64
		public SystemThreadingTasks_FutureDebugView(Task<TResult> task)
		{
			this.m_task = task;
		}

		// Token: 0x17000906 RID: 2310
		// (get) Token: 0x06003730 RID: 14128 RVA: 0x000BFD74 File Offset: 0x000BDF74
		public TResult Result
		{
			get
			{
				if (this.m_task.Status != TaskStatus.RanToCompletion)
				{
					return default(TResult);
				}
				return this.m_task.Result;
			}
		}

		// Token: 0x17000907 RID: 2311
		// (get) Token: 0x06003731 RID: 14129 RVA: 0x000BFDA4 File Offset: 0x000BDFA4
		public object AsyncState
		{
			get
			{
				return this.m_task.AsyncState;
			}
		}

		// Token: 0x17000908 RID: 2312
		// (get) Token: 0x06003732 RID: 14130 RVA: 0x000BFDB1 File Offset: 0x000BDFB1
		public TaskCreationOptions CreationOptions
		{
			get
			{
				return this.m_task.CreationOptions;
			}
		}

		// Token: 0x17000909 RID: 2313
		// (get) Token: 0x06003733 RID: 14131 RVA: 0x000BFDBE File Offset: 0x000BDFBE
		public Exception Exception
		{
			get
			{
				return this.m_task.Exception;
			}
		}

		// Token: 0x1700090A RID: 2314
		// (get) Token: 0x06003734 RID: 14132 RVA: 0x000BFDCB File Offset: 0x000BDFCB
		public int Id
		{
			get
			{
				return this.m_task.Id;
			}
		}

		// Token: 0x1700090B RID: 2315
		// (get) Token: 0x06003735 RID: 14133 RVA: 0x000BFDD8 File Offset: 0x000BDFD8
		public bool CancellationPending
		{
			get
			{
				return this.m_task.Status == TaskStatus.WaitingToRun && this.m_task.CancellationToken.IsCancellationRequested;
			}
		}

		// Token: 0x1700090C RID: 2316
		// (get) Token: 0x06003736 RID: 14134 RVA: 0x000BFE08 File Offset: 0x000BE008
		public TaskStatus Status
		{
			get
			{
				return this.m_task.Status;
			}
		}

		// Token: 0x04001CFF RID: 7423
		private Task<TResult> m_task;
	}
}
