﻿using System;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x0200051E RID: 1310
	internal class StackGuard
	{
		// Token: 0x0600393E RID: 14654 RVA: 0x000C8348 File Offset: 0x000C6548
		[SecuritySafeCritical]
		internal bool TryBeginInliningScope()
		{
			if (this.m_inliningDepth < 20 || this.CheckForSufficientStack())
			{
				this.m_inliningDepth++;
				return true;
			}
			return false;
		}

		// Token: 0x0600393F RID: 14655 RVA: 0x000C836D File Offset: 0x000C656D
		internal void EndInliningScope()
		{
			this.m_inliningDepth--;
			if (this.m_inliningDepth < 0)
			{
				this.m_inliningDepth = 0;
			}
		}

		// Token: 0x06003940 RID: 14656 RVA: 0x00004E08 File Offset: 0x00003008
		[SecurityCritical]
		private bool CheckForSufficientStack()
		{
			return true;
		}

		// Token: 0x06003941 RID: 14657 RVA: 0x00002050 File Offset: 0x00000250
		public StackGuard()
		{
		}

		// Token: 0x04001E10 RID: 7696
		private int m_inliningDepth;

		// Token: 0x04001E11 RID: 7697
		private const int MAX_UNCHECKED_INLINING_DEPTH = 20;
	}
}
