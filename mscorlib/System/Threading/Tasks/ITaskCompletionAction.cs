﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x02000520 RID: 1312
	internal interface ITaskCompletionAction
	{
		// Token: 0x06003942 RID: 14658
		void Invoke(Task completingTask);
	}
}
