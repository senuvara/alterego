﻿using System;
using System.Runtime.Serialization;

namespace System.Threading.Tasks
{
	/// <summary>Represents an exception used to communicate task cancellation.</summary>
	// Token: 0x02000523 RID: 1315
	[Serializable]
	public class TaskCanceledException : OperationCanceledException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.TaskCanceledException" /> class with a system-supplied message that describes the error.</summary>
		// Token: 0x0600394D RID: 14669 RVA: 0x000C8669 File Offset: 0x000C6869
		public TaskCanceledException() : base(Environment.GetResourceString("A task was canceled."))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.TaskCanceledException" /> class with a specified message that describes the error.</summary>
		/// <param name="message">The message that describes the exception. The caller of this constructor is required to ensure that this string has been localized for the current system culture.</param>
		// Token: 0x0600394E RID: 14670 RVA: 0x000C867B File Offset: 0x000C687B
		public TaskCanceledException(string message) : base(message)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.TaskCanceledException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The message that describes the exception. The caller of this constructor is required to ensure that this string has been localized for the current system culture. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x0600394F RID: 14671 RVA: 0x000C8684 File Offset: 0x000C6884
		public TaskCanceledException(string message, Exception innerException) : base(message, innerException)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.TaskCanceledException" /> class with a reference to the <see cref="T:System.Threading.Tasks.Task" /> that has been canceled.</summary>
		/// <param name="task">A task that has been canceled.</param>
		// Token: 0x06003950 RID: 14672 RVA: 0x000C8690 File Offset: 0x000C6890
		public TaskCanceledException(Task task) : base(Environment.GetResourceString("A task was canceled."), (task != null) ? task.CancellationToken : default(CancellationToken))
		{
			this.m_canceledTask = task;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.TaskCanceledException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06003951 RID: 14673 RVA: 0x000C86C8 File Offset: 0x000C68C8
		protected TaskCanceledException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>Gets the task associated with this exception.</summary>
		/// <returns>A reference to the <see cref="T:System.Threading.Tasks.Task" /> that is associated with this exception.</returns>
		// Token: 0x17000963 RID: 2403
		// (get) Token: 0x06003952 RID: 14674 RVA: 0x000C86D2 File Offset: 0x000C68D2
		public Task Task
		{
			get
			{
				return this.m_canceledTask;
			}
		}

		// Token: 0x04001E19 RID: 7705
		[NonSerialized]
		private Task m_canceledTask;
	}
}
