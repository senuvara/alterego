﻿using System;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x02000517 RID: 1303
	internal sealed class CompletionActionInvoker : IThreadPoolWorkItem
	{
		// Token: 0x0600392A RID: 14634 RVA: 0x000C81C6 File Offset: 0x000C63C6
		internal CompletionActionInvoker(ITaskCompletionAction action, Task completingTask)
		{
			this.m_action = action;
			this.m_completingTask = completingTask;
		}

		// Token: 0x0600392B RID: 14635 RVA: 0x000C81DC File Offset: 0x000C63DC
		[SecurityCritical]
		public void ExecuteWorkItem()
		{
			this.m_action.Invoke(this.m_completingTask);
		}

		// Token: 0x0600392C RID: 14636 RVA: 0x000020D3 File Offset: 0x000002D3
		[SecurityCritical]
		public void MarkAborted(ThreadAbortException tae)
		{
		}

		// Token: 0x04001DE7 RID: 7655
		private readonly ITaskCompletionAction m_action;

		// Token: 0x04001DE8 RID: 7656
		private readonly Task m_completingTask;
	}
}
