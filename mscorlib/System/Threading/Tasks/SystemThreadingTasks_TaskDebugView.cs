﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x02000518 RID: 1304
	internal class SystemThreadingTasks_TaskDebugView
	{
		// Token: 0x0600392D RID: 14637 RVA: 0x000C81EF File Offset: 0x000C63EF
		public SystemThreadingTasks_TaskDebugView(Task task)
		{
			this.m_task = task;
		}

		// Token: 0x1700095A RID: 2394
		// (get) Token: 0x0600392E RID: 14638 RVA: 0x000C81FE File Offset: 0x000C63FE
		public object AsyncState
		{
			get
			{
				return this.m_task.AsyncState;
			}
		}

		// Token: 0x1700095B RID: 2395
		// (get) Token: 0x0600392F RID: 14639 RVA: 0x000C820B File Offset: 0x000C640B
		public TaskCreationOptions CreationOptions
		{
			get
			{
				return this.m_task.CreationOptions;
			}
		}

		// Token: 0x1700095C RID: 2396
		// (get) Token: 0x06003930 RID: 14640 RVA: 0x000C8218 File Offset: 0x000C6418
		public Exception Exception
		{
			get
			{
				return this.m_task.Exception;
			}
		}

		// Token: 0x1700095D RID: 2397
		// (get) Token: 0x06003931 RID: 14641 RVA: 0x000C8225 File Offset: 0x000C6425
		public int Id
		{
			get
			{
				return this.m_task.Id;
			}
		}

		// Token: 0x1700095E RID: 2398
		// (get) Token: 0x06003932 RID: 14642 RVA: 0x000C8234 File Offset: 0x000C6434
		public bool CancellationPending
		{
			get
			{
				return this.m_task.Status == TaskStatus.WaitingToRun && this.m_task.CancellationToken.IsCancellationRequested;
			}
		}

		// Token: 0x1700095F RID: 2399
		// (get) Token: 0x06003933 RID: 14643 RVA: 0x000C8264 File Offset: 0x000C6464
		public TaskStatus Status
		{
			get
			{
				return this.m_task.Status;
			}
		}

		// Token: 0x04001DE9 RID: 7657
		private Task m_task;
	}
}
