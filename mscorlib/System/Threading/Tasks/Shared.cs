﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x0200050C RID: 1292
	internal class Shared<T>
	{
		// Token: 0x0600383C RID: 14396 RVA: 0x000C47E9 File Offset: 0x000C29E9
		internal Shared(T value)
		{
			this.Value = value;
		}

		// Token: 0x04001D9D RID: 7581
		internal T Value;
	}
}
