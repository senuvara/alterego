﻿using System;
using System.Runtime.CompilerServices;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x0200052B RID: 1323
	internal sealed class SynchronizationContextAwaitTaskContinuation : AwaitTaskContinuation
	{
		// Token: 0x06003972 RID: 14706 RVA: 0x000C8D88 File Offset: 0x000C6F88
		[SecurityCritical]
		internal SynchronizationContextAwaitTaskContinuation(SynchronizationContext context, Action action, bool flowExecutionContext, ref StackCrawlMark stackMark) : base(action, flowExecutionContext, ref stackMark)
		{
			this.m_syncContext = context;
		}

		// Token: 0x06003973 RID: 14707 RVA: 0x000C8D9B File Offset: 0x000C6F9B
		[SecuritySafeCritical]
		internal sealed override void Run(Task task, bool canInlineContinuationTask)
		{
			if (canInlineContinuationTask && this.m_syncContext == SynchronizationContext.CurrentNoFlow)
			{
				base.RunCallback(AwaitTaskContinuation.GetInvokeActionCallback(), this.m_action, ref Task.t_currentTask);
				return;
			}
			base.RunCallback(SynchronizationContextAwaitTaskContinuation.GetPostActionCallback(), this, ref Task.t_currentTask);
		}

		// Token: 0x06003974 RID: 14708 RVA: 0x000C8DD8 File Offset: 0x000C6FD8
		[SecurityCritical]
		private static void PostAction(object state)
		{
			SynchronizationContextAwaitTaskContinuation synchronizationContextAwaitTaskContinuation = (SynchronizationContextAwaitTaskContinuation)state;
			synchronizationContextAwaitTaskContinuation.m_syncContext.Post(SynchronizationContextAwaitTaskContinuation.s_postCallback, synchronizationContextAwaitTaskContinuation.m_action);
		}

		// Token: 0x06003975 RID: 14709 RVA: 0x000C8E04 File Offset: 0x000C7004
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static ContextCallback GetPostActionCallback()
		{
			ContextCallback contextCallback = SynchronizationContextAwaitTaskContinuation.s_postActionCallback;
			if (contextCallback == null)
			{
				contextCallback = (SynchronizationContextAwaitTaskContinuation.s_postActionCallback = new ContextCallback(SynchronizationContextAwaitTaskContinuation.PostAction));
			}
			return contextCallback;
		}

		// Token: 0x06003976 RID: 14710 RVA: 0x000C8E2E File Offset: 0x000C702E
		// Note: this type is marked as 'beforefieldinit'.
		static SynchronizationContextAwaitTaskContinuation()
		{
		}

		// Token: 0x04001E22 RID: 7714
		private static readonly SendOrPostCallback s_postCallback = delegate(object state)
		{
			((Action)state)();
		};

		// Token: 0x04001E23 RID: 7715
		[SecurityCritical]
		private static ContextCallback s_postActionCallback;

		// Token: 0x04001E24 RID: 7716
		private readonly SynchronizationContext m_syncContext;

		// Token: 0x0200052C RID: 1324
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06003977 RID: 14711 RVA: 0x000C8E45 File Offset: 0x000C7045
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06003978 RID: 14712 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x06003979 RID: 14713 RVA: 0x000C8E51 File Offset: 0x000C7051
			internal void <.cctor>b__7_0(object state)
			{
				((Action)state)();
			}

			// Token: 0x04001E25 RID: 7717
			public static readonly SynchronizationContextAwaitTaskContinuation.<>c <>9 = new SynchronizationContextAwaitTaskContinuation.<>c();
		}
	}
}
