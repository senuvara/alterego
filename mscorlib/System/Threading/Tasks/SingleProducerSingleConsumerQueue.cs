﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Threading.Tasks
{
	// Token: 0x02000505 RID: 1285
	[DebuggerDisplay("Count = {Count}")]
	[DebuggerTypeProxy(typeof(SingleProducerSingleConsumerQueue<>.SingleProducerSingleConsumerQueue_DebugView))]
	internal sealed class SingleProducerSingleConsumerQueue<T> : IProducerConsumerQueue<T>, IEnumerable<!0>, IEnumerable
	{
		// Token: 0x06003824 RID: 14372 RVA: 0x000C3F6C File Offset: 0x000C216C
		internal SingleProducerSingleConsumerQueue()
		{
			this.m_head = (this.m_tail = new SingleProducerSingleConsumerQueue<T>.Segment(32));
		}

		// Token: 0x06003825 RID: 14373 RVA: 0x000C3F9C File Offset: 0x000C219C
		public void Enqueue(T item)
		{
			SingleProducerSingleConsumerQueue<T>.Segment tail = this.m_tail;
			T[] array = tail.m_array;
			int last = tail.m_state.m_last;
			int num = last + 1 & array.Length - 1;
			if (num != tail.m_state.m_firstCopy)
			{
				array[last] = item;
				tail.m_state.m_last = num;
				return;
			}
			this.EnqueueSlow(item, ref tail);
		}

		// Token: 0x06003826 RID: 14374 RVA: 0x000C4000 File Offset: 0x000C2200
		private void EnqueueSlow(T item, ref SingleProducerSingleConsumerQueue<T>.Segment segment)
		{
			if (segment.m_state.m_firstCopy != segment.m_state.m_first)
			{
				segment.m_state.m_firstCopy = segment.m_state.m_first;
				this.Enqueue(item);
				return;
			}
			int num = this.m_tail.m_array.Length << 1;
			if (num > 16777216)
			{
				num = 16777216;
			}
			SingleProducerSingleConsumerQueue<T>.Segment segment2 = new SingleProducerSingleConsumerQueue<T>.Segment(num);
			segment2.m_array[0] = item;
			segment2.m_state.m_last = 1;
			segment2.m_state.m_lastCopy = 1;
			try
			{
			}
			finally
			{
				Volatile.Write<SingleProducerSingleConsumerQueue<T>.Segment>(ref this.m_tail.m_next, segment2);
				this.m_tail = segment2;
			}
		}

		// Token: 0x06003827 RID: 14375 RVA: 0x000C40C8 File Offset: 0x000C22C8
		public bool TryDequeue(out T result)
		{
			SingleProducerSingleConsumerQueue<T>.Segment head = this.m_head;
			T[] array = head.m_array;
			int first = head.m_state.m_first;
			if (first != head.m_state.m_lastCopy)
			{
				result = array[first];
				array[first] = default(T);
				head.m_state.m_first = (first + 1 & array.Length - 1);
				return true;
			}
			return this.TryDequeueSlow(ref head, ref array, out result);
		}

		// Token: 0x06003828 RID: 14376 RVA: 0x000C4144 File Offset: 0x000C2344
		private bool TryDequeueSlow(ref SingleProducerSingleConsumerQueue<T>.Segment segment, ref T[] array, out T result)
		{
			if (segment.m_state.m_last != segment.m_state.m_lastCopy)
			{
				segment.m_state.m_lastCopy = segment.m_state.m_last;
				return this.TryDequeue(out result);
			}
			if (segment.m_next != null && segment.m_state.m_first == segment.m_state.m_last)
			{
				segment = segment.m_next;
				array = segment.m_array;
				this.m_head = segment;
			}
			int first = segment.m_state.m_first;
			if (first == segment.m_state.m_last)
			{
				result = default(T);
				return false;
			}
			result = array[first];
			array[first] = default(T);
			segment.m_state.m_first = (first + 1 & segment.m_array.Length - 1);
			segment.m_state.m_lastCopy = segment.m_state.m_last;
			return true;
		}

		// Token: 0x06003829 RID: 14377 RVA: 0x000C4254 File Offset: 0x000C2454
		public bool TryPeek(out T result)
		{
			SingleProducerSingleConsumerQueue<T>.Segment head = this.m_head;
			T[] array = head.m_array;
			int first = head.m_state.m_first;
			if (first != head.m_state.m_lastCopy)
			{
				result = array[first];
				return true;
			}
			return this.TryPeekSlow(ref head, ref array, out result);
		}

		// Token: 0x0600382A RID: 14378 RVA: 0x000C42A8 File Offset: 0x000C24A8
		private bool TryPeekSlow(ref SingleProducerSingleConsumerQueue<T>.Segment segment, ref T[] array, out T result)
		{
			if (segment.m_state.m_last != segment.m_state.m_lastCopy)
			{
				segment.m_state.m_lastCopy = segment.m_state.m_last;
				return this.TryPeek(out result);
			}
			if (segment.m_next != null && segment.m_state.m_first == segment.m_state.m_last)
			{
				segment = segment.m_next;
				array = segment.m_array;
				this.m_head = segment;
			}
			int first = segment.m_state.m_first;
			if (first == segment.m_state.m_last)
			{
				result = default(T);
				return false;
			}
			result = array[first];
			return true;
		}

		// Token: 0x0600382B RID: 14379 RVA: 0x000C4370 File Offset: 0x000C2570
		public bool TryDequeueIf(Predicate<T> predicate, out T result)
		{
			SingleProducerSingleConsumerQueue<T>.Segment head = this.m_head;
			T[] array = head.m_array;
			int first = head.m_state.m_first;
			if (first == head.m_state.m_lastCopy)
			{
				return this.TryDequeueIfSlow(predicate, ref head, ref array, out result);
			}
			result = array[first];
			if (predicate == null || predicate(result))
			{
				array[first] = default(T);
				head.m_state.m_first = (first + 1 & array.Length - 1);
				return true;
			}
			result = default(T);
			return false;
		}

		// Token: 0x0600382C RID: 14380 RVA: 0x000C4404 File Offset: 0x000C2604
		private bool TryDequeueIfSlow(Predicate<T> predicate, ref SingleProducerSingleConsumerQueue<T>.Segment segment, ref T[] array, out T result)
		{
			if (segment.m_state.m_last != segment.m_state.m_lastCopy)
			{
				segment.m_state.m_lastCopy = segment.m_state.m_last;
				return this.TryDequeueIf(predicate, out result);
			}
			if (segment.m_next != null && segment.m_state.m_first == segment.m_state.m_last)
			{
				segment = segment.m_next;
				array = segment.m_array;
				this.m_head = segment;
			}
			int first = segment.m_state.m_first;
			if (first == segment.m_state.m_last)
			{
				result = default(T);
				return false;
			}
			result = array[first];
			if (predicate == null || predicate(result))
			{
				array[first] = default(T);
				segment.m_state.m_first = (first + 1 & segment.m_array.Length - 1);
				segment.m_state.m_lastCopy = segment.m_state.m_last;
				return true;
			}
			result = default(T);
			return false;
		}

		// Token: 0x0600382D RID: 14381 RVA: 0x000C4534 File Offset: 0x000C2734
		public void Clear()
		{
			T t;
			while (this.TryDequeue(out t))
			{
			}
		}

		// Token: 0x1700092E RID: 2350
		// (get) Token: 0x0600382E RID: 14382 RVA: 0x000C454C File Offset: 0x000C274C
		public bool IsEmpty
		{
			get
			{
				SingleProducerSingleConsumerQueue<T>.Segment head = this.m_head;
				return head.m_state.m_first == head.m_state.m_lastCopy && head.m_state.m_first == head.m_state.m_last && head.m_next == null;
			}
		}

		// Token: 0x0600382F RID: 14383 RVA: 0x000C45A5 File Offset: 0x000C27A5
		public IEnumerator<T> GetEnumerator()
		{
			SingleProducerSingleConsumerQueue<T>.Segment segment;
			for (segment = this.m_head; segment != null; segment = segment.m_next)
			{
				for (int pt = segment.m_state.m_first; pt != segment.m_state.m_last; pt = (pt + 1 & segment.m_array.Length - 1))
				{
					yield return segment.m_array[pt];
				}
			}
			segment = null;
			yield break;
		}

		// Token: 0x06003830 RID: 14384 RVA: 0x000C45B4 File Offset: 0x000C27B4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x1700092F RID: 2351
		// (get) Token: 0x06003831 RID: 14385 RVA: 0x000C45BC File Offset: 0x000C27BC
		public int Count
		{
			get
			{
				int num = 0;
				for (SingleProducerSingleConsumerQueue<T>.Segment segment = this.m_head; segment != null; segment = segment.m_next)
				{
					int num2 = segment.m_array.Length;
					int first;
					int last;
					do
					{
						first = segment.m_state.m_first;
						last = segment.m_state.m_last;
					}
					while (first != segment.m_state.m_first);
					num += (last - first & num2 - 1);
				}
				return num;
			}
		}

		// Token: 0x06003832 RID: 14386 RVA: 0x000C4624 File Offset: 0x000C2824
		int IProducerConsumerQueue<!0>.GetCountSafe(object syncObj)
		{
			int count;
			lock (syncObj)
			{
				count = this.Count;
			}
			return count;
		}

		// Token: 0x04001D88 RID: 7560
		private const int INIT_SEGMENT_SIZE = 32;

		// Token: 0x04001D89 RID: 7561
		private const int MAX_SEGMENT_SIZE = 16777216;

		// Token: 0x04001D8A RID: 7562
		private volatile SingleProducerSingleConsumerQueue<T>.Segment m_head;

		// Token: 0x04001D8B RID: 7563
		private volatile SingleProducerSingleConsumerQueue<T>.Segment m_tail;

		// Token: 0x02000506 RID: 1286
		[StructLayout(LayoutKind.Sequential)]
		private sealed class Segment
		{
			// Token: 0x06003833 RID: 14387 RVA: 0x000C4664 File Offset: 0x000C2864
			internal Segment(int size)
			{
				this.m_array = new T[size];
			}

			// Token: 0x04001D8C RID: 7564
			internal SingleProducerSingleConsumerQueue<T>.Segment m_next;

			// Token: 0x04001D8D RID: 7565
			internal readonly T[] m_array;

			// Token: 0x04001D8E RID: 7566
			internal SingleProducerSingleConsumerQueue<T>.SegmentState m_state;
		}

		// Token: 0x02000507 RID: 1287
		private struct SegmentState
		{
			// Token: 0x04001D8F RID: 7567
			internal PaddingFor32 m_pad0;

			// Token: 0x04001D90 RID: 7568
			internal volatile int m_first;

			// Token: 0x04001D91 RID: 7569
			internal int m_lastCopy;

			// Token: 0x04001D92 RID: 7570
			internal PaddingFor32 m_pad1;

			// Token: 0x04001D93 RID: 7571
			internal int m_firstCopy;

			// Token: 0x04001D94 RID: 7572
			internal volatile int m_last;

			// Token: 0x04001D95 RID: 7573
			internal PaddingFor32 m_pad2;
		}

		// Token: 0x02000508 RID: 1288
		private sealed class SingleProducerSingleConsumerQueue_DebugView
		{
			// Token: 0x06003834 RID: 14388 RVA: 0x000C4678 File Offset: 0x000C2878
			public SingleProducerSingleConsumerQueue_DebugView(SingleProducerSingleConsumerQueue<T> queue)
			{
				this.m_queue = queue;
			}

			// Token: 0x17000930 RID: 2352
			// (get) Token: 0x06003835 RID: 14389 RVA: 0x000C4688 File Offset: 0x000C2888
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public T[] Items
			{
				get
				{
					List<T> list = new List<T>();
					foreach (T item in this.m_queue)
					{
						list.Add(item);
					}
					return list.ToArray();
				}
			}

			// Token: 0x04001D96 RID: 7574
			private readonly SingleProducerSingleConsumerQueue<T> m_queue;
		}

		// Token: 0x02000509 RID: 1289
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__16 : IEnumerator<T>, IDisposable, IEnumerator
		{
			// Token: 0x06003836 RID: 14390 RVA: 0x000C46E4 File Offset: 0x000C28E4
			[DebuggerHidden]
			public <GetEnumerator>d__16(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06003837 RID: 14391 RVA: 0x000020D3 File Offset: 0x000002D3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06003838 RID: 14392 RVA: 0x000C46F4 File Offset: 0x000C28F4
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				SingleProducerSingleConsumerQueue<T> singleProducerSingleConsumerQueue = this;
				if (num == 0)
				{
					this.<>1__state = -1;
					segment = singleProducerSingleConsumerQueue.m_head;
					goto IL_C0;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				pt = (pt + 1 & segment.m_array.Length - 1);
				IL_95:
				if (pt != segment.m_state.m_last)
				{
					this.<>2__current = segment.m_array[pt];
					this.<>1__state = 1;
					return true;
				}
				segment = segment.m_next;
				IL_C0:
				if (segment == null)
				{
					segment = null;
					return false;
				}
				pt = segment.m_state.m_first;
				goto IL_95;
			}

			// Token: 0x17000931 RID: 2353
			// (get) Token: 0x06003839 RID: 14393 RVA: 0x000C47D4 File Offset: 0x000C29D4
			T IEnumerator<!0>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x0600383A RID: 14394 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000932 RID: 2354
			// (get) Token: 0x0600383B RID: 14395 RVA: 0x000C47DC File Offset: 0x000C29DC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04001D97 RID: 7575
			private int <>1__state;

			// Token: 0x04001D98 RID: 7576
			private T <>2__current;

			// Token: 0x04001D99 RID: 7577
			public SingleProducerSingleConsumerQueue<T> <>4__this;

			// Token: 0x04001D9A RID: 7578
			private SingleProducerSingleConsumerQueue<T>.Segment <segment>5__1;

			// Token: 0x04001D9B RID: 7579
			private int <pt>5__2;
		}
	}
}
