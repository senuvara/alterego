﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Permissions;

namespace System.Threading.Tasks
{
	/// <summary>Provides support for parallel loops and regions.</summary>
	// Token: 0x020004EE RID: 1262
	[HostProtection(SecurityAction.LinkDemand, Synchronization = true, ExternalThreading = true)]
	public static class Parallel
	{
		/// <summary>Executes each of the provided actions, possibly in parallel.</summary>
		/// <param name="actions">An array of <see cref="T:System.Action" /> to execute.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="actions" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that is thrown when any action in the <paramref name="actions" /> array throws an exception.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="actions" /> array contains a <see langword="null" /> element.</exception>
		// Token: 0x0600379E RID: 14238 RVA: 0x000C1588 File Offset: 0x000BF788
		public static void Invoke(params Action[] actions)
		{
			Parallel.Invoke(Parallel.s_defaultParallelOptions, actions);
		}

		/// <summary>Executes each of the provided actions, possibly in parallel, unless the operation is cancelled by the user.</summary>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="actions">An array of actions to execute.</param>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> is set.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="actions" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that is thrown when any action in the <paramref name="actions" /> array throws an exception.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="actions" /> array contains a <see langword="null" /> element.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x0600379F RID: 14239 RVA: 0x000C1598 File Offset: 0x000BF798
		public static void Invoke(ParallelOptions parallelOptions, params Action[] actions)
		{
			if (actions == null)
			{
				throw new ArgumentNullException("actions");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			if (parallelOptions.CancellationToken.CanBeCanceled && AppContextSwitches.ThrowExceptionIfDisposedCancellationTokenSource)
			{
				parallelOptions.CancellationToken.ThrowIfSourceDisposed();
			}
			if (parallelOptions.CancellationToken.IsCancellationRequested)
			{
				throw new OperationCanceledException(parallelOptions.CancellationToken);
			}
			Action[] actionsCopy = new Action[actions.Length];
			for (int i = 0; i < actionsCopy.Length; i++)
			{
				actionsCopy[i] = actions[i];
				if (actionsCopy[i] == null)
				{
					throw new ArgumentException(Environment.GetResourceString("One of the actions was null."));
				}
			}
			if (actionsCopy.Length < 1)
			{
				return;
			}
			if (actionsCopy.Length > 10 || (parallelOptions.MaxDegreeOfParallelism != -1 && parallelOptions.MaxDegreeOfParallelism < actionsCopy.Length))
			{
				ConcurrentQueue<Exception> exceptionQ = null;
				try
				{
					int actionIndex = 0;
					ParallelForReplicatingTask parallelForReplicatingTask = new ParallelForReplicatingTask(parallelOptions, delegate()
					{
						for (int l = Interlocked.Increment(ref actionIndex); l <= actionsCopy.Length; l = Interlocked.Increment(ref actionIndex))
						{
							try
							{
								actionsCopy[l - 1]();
							}
							catch (Exception item2)
							{
								LazyInitializer.EnsureInitialized<ConcurrentQueue<Exception>>(ref exceptionQ, () => new ConcurrentQueue<Exception>());
								exceptionQ.Enqueue(item2);
							}
							if (parallelOptions.CancellationToken.IsCancellationRequested)
							{
								throw new OperationCanceledException(parallelOptions.CancellationToken);
							}
						}
					}, TaskCreationOptions.None, InternalTaskOptions.SelfReplicating);
					parallelForReplicatingTask.RunSynchronously(parallelOptions.EffectiveTaskScheduler);
					parallelForReplicatingTask.Wait();
				}
				catch (Exception ex)
				{
					LazyInitializer.EnsureInitialized<ConcurrentQueue<Exception>>(ref exceptionQ, () => new ConcurrentQueue<Exception>());
					AggregateException ex2 = ex as AggregateException;
					if (ex2 != null)
					{
						using (IEnumerator<Exception> enumerator = ex2.InnerExceptions.GetEnumerator())
						{
							while (enumerator.MoveNext())
							{
								Exception item = enumerator.Current;
								exceptionQ.Enqueue(item);
							}
							goto IL_208;
						}
					}
					exceptionQ.Enqueue(ex);
					IL_208:;
				}
				if (exceptionQ != null && exceptionQ.Count > 0)
				{
					Parallel.ThrowIfReducableToSingleOCE(exceptionQ, parallelOptions.CancellationToken);
					throw new AggregateException(exceptionQ);
				}
			}
			else
			{
				Task[] array = new Task[actionsCopy.Length];
				if (parallelOptions.CancellationToken.IsCancellationRequested)
				{
					throw new OperationCanceledException(parallelOptions.CancellationToken);
				}
				for (int j = 1; j < array.Length; j++)
				{
					array[j] = Task.Factory.StartNew(actionsCopy[j], parallelOptions.CancellationToken, TaskCreationOptions.None, InternalTaskOptions.None, parallelOptions.EffectiveTaskScheduler);
				}
				array[0] = new Task(actionsCopy[0]);
				array[0].RunSynchronously(parallelOptions.EffectiveTaskScheduler);
				try
				{
					if (array.Length <= 4)
					{
						Task.FastWaitAll(array);
					}
					else
					{
						Task.WaitAll(array);
					}
				}
				catch (AggregateException ex3)
				{
					Parallel.ThrowIfReducableToSingleOCE(ex3.InnerExceptions, parallelOptions.CancellationToken);
					throw;
				}
				finally
				{
					for (int k = 0; k < array.Length; k++)
					{
						if (array[k].IsCompleted)
						{
							array[k].Dispose();
						}
					}
				}
			}
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic) loop in which iterations may run in parallel.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037A0 RID: 14240 RVA: 0x000C1920 File Offset: 0x000BFB20
		public static ParallelLoopResult For(int fromInclusive, int toExclusive, Action<int> body)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.ForWorker<object>(fromInclusive, toExclusive, Parallel.s_defaultParallelOptions, body, null, null, null, null);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic) loop with 64-bit indexes in which iterations may run in parallel.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037A1 RID: 14241 RVA: 0x000C1941 File Offset: 0x000BFB41
		public static ParallelLoopResult For(long fromInclusive, long toExclusive, Action<long> body)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.ForWorker64<object>(fromInclusive, toExclusive, Parallel.s_defaultParallelOptions, body, null, null, null, null);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic) loop in which iterations may run in parallel and loop options can be configured.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <returns>A  structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037A2 RID: 14242 RVA: 0x000C1962 File Offset: 0x000BFB62
		public static ParallelLoopResult For(int fromInclusive, int toExclusive, ParallelOptions parallelOptions, Action<int> body)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForWorker<object>(fromInclusive, toExclusive, parallelOptions, body, null, null, null, null);
		}

		/// <summary>Executes a <see langword="for" />  (<see langword="For" /> in Visual Basic) loop with 64-bit indexes in which iterations may run in parallel and loop options can be configured.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037A3 RID: 14243 RVA: 0x000C198D File Offset: 0x000BFB8D
		public static ParallelLoopResult For(long fromInclusive, long toExclusive, ParallelOptions parallelOptions, Action<long> body)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForWorker64<object>(fromInclusive, toExclusive, parallelOptions, body, null, null, null, null);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic) loop in which iterations may run in parallel and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <returns>A  structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037A4 RID: 14244 RVA: 0x000C19B8 File Offset: 0x000BFBB8
		public static ParallelLoopResult For(int fromInclusive, int toExclusive, Action<int, ParallelLoopState> body)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.ForWorker<object>(fromInclusive, toExclusive, Parallel.s_defaultParallelOptions, null, body, null, null, null);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic) loop with 64-bit indexes in which iterations may run in parallel and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <returns>A <see cref="T:System.Threading.Tasks.ParallelLoopResult" /> structure that contains information on what portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037A5 RID: 14245 RVA: 0x000C19D9 File Offset: 0x000BFBD9
		public static ParallelLoopResult For(long fromInclusive, long toExclusive, Action<long, ParallelLoopState> body)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.ForWorker64<object>(fromInclusive, toExclusive, Parallel.s_defaultParallelOptions, null, body, null, null, null);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic) loop in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037A6 RID: 14246 RVA: 0x000C19FA File Offset: 0x000BFBFA
		public static ParallelLoopResult For(int fromInclusive, int toExclusive, ParallelOptions parallelOptions, Action<int, ParallelLoopState> body)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForWorker<object>(fromInclusive, toExclusive, parallelOptions, null, body, null, null, null);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic)  loop with 64-bit indexes in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037A7 RID: 14247 RVA: 0x000C1A25 File Offset: 0x000BFC25
		public static ParallelLoopResult For(long fromInclusive, long toExclusive, ParallelOptions parallelOptions, Action<long, ParallelLoopState> body)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForWorker64<object>(fromInclusive, toExclusive, parallelOptions, null, body, null, null, null);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic) loop with thread-local data in which iterations may run in parallel, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A  structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037A8 RID: 14248 RVA: 0x000C1A50 File Offset: 0x000BFC50
		public static ParallelLoopResult For<TLocal>(int fromInclusive, int toExclusive, Func<TLocal> localInit, Func<int, ParallelLoopState, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			return Parallel.ForWorker<TLocal>(fromInclusive, toExclusive, Parallel.s_defaultParallelOptions, null, null, body, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic)  loop with 64-bit indexes and thread-local data in which iterations may run in parallel, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037A9 RID: 14249 RVA: 0x000C1A8F File Offset: 0x000BFC8F
		public static ParallelLoopResult For<TLocal>(long fromInclusive, long toExclusive, Func<TLocal> localInit, Func<long, ParallelLoopState, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			return Parallel.ForWorker64<TLocal>(fromInclusive, toExclusive, Parallel.s_defaultParallelOptions, null, null, body, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic)  loop with thread-local data in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037AA RID: 14250 RVA: 0x000C1AD0 File Offset: 0x000BFCD0
		public static ParallelLoopResult For<TLocal>(int fromInclusive, int toExclusive, ParallelOptions parallelOptions, Func<TLocal> localInit, Func<int, ParallelLoopState, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForWorker<TLocal>(fromInclusive, toExclusive, parallelOptions, null, null, body, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="for" /> (<see langword="For" /> in Visual Basic) loop with 64-bit indexes and thread-local data in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="fromInclusive">The start index, inclusive.</param>
		/// <param name="toExclusive">The end index, exclusive.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each thread.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each thread.</param>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037AB RID: 14251 RVA: 0x000C1B28 File Offset: 0x000BFD28
		public static ParallelLoopResult For<TLocal>(long fromInclusive, long toExclusive, ParallelOptions parallelOptions, Func<TLocal> localInit, Func<long, ParallelLoopState, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForWorker64<TLocal>(fromInclusive, toExclusive, parallelOptions, null, null, body, localInit, localFinally);
		}

		// Token: 0x060037AC RID: 14252 RVA: 0x000C1B80 File Offset: 0x000BFD80
		private static ParallelLoopResult ForWorker<TLocal>(int fromInclusive, int toExclusive, ParallelOptions parallelOptions, Action<int> body, Action<int, ParallelLoopState> bodyWithState, Func<int, ParallelLoopState, TLocal, TLocal> bodyWithLocal, Func<TLocal> localInit, Action<TLocal> localFinally)
		{
			ParallelLoopResult result = default(ParallelLoopResult);
			if (toExclusive <= fromInclusive)
			{
				result.m_completed = true;
				return result;
			}
			ParallelLoopStateFlags32 sharedPStateFlags = new ParallelLoopStateFlags32();
			TaskCreationOptions creationOptions = TaskCreationOptions.None;
			InternalTaskOptions internalOptions = InternalTaskOptions.SelfReplicating;
			if (parallelOptions.CancellationToken.IsCancellationRequested)
			{
				throw new OperationCanceledException(parallelOptions.CancellationToken);
			}
			int nNumExpectedWorkers = (parallelOptions.EffectiveMaxConcurrencyLevel == -1) ? PlatformHelper.ProcessorCount : parallelOptions.EffectiveMaxConcurrencyLevel;
			RangeManager rangeManager = new RangeManager((long)fromInclusive, (long)toExclusive, 1L, nNumExpectedWorkers);
			OperationCanceledException oce = null;
			CancellationTokenRegistration cancellationTokenRegistration = default(CancellationTokenRegistration);
			if (parallelOptions.CancellationToken.CanBeCanceled)
			{
				cancellationTokenRegistration = parallelOptions.CancellationToken.InternalRegisterWithoutEC(delegate(object o)
				{
					sharedPStateFlags.Cancel();
					oce = new OperationCanceledException(parallelOptions.CancellationToken);
				}, null);
			}
			ParallelForReplicatingTask rootTask = null;
			try
			{
				rootTask = new ParallelForReplicatingTask(parallelOptions, delegate()
				{
					Task internalCurrent = Task.InternalCurrent;
					bool flag = internalCurrent == rootTask;
					RangeWorker rangeWorker = default(RangeWorker);
					object savedStateFromPreviousReplica = internalCurrent.SavedStateFromPreviousReplica;
					if (savedStateFromPreviousReplica is RangeWorker)
					{
						rangeWorker = (RangeWorker)savedStateFromPreviousReplica;
					}
					else
					{
						rangeWorker = rangeManager.RegisterNewWorker();
					}
					int num;
					int num2;
					if (!rangeWorker.FindNewWork32(out num, out num2) || sharedPStateFlags.ShouldExitLoop(num))
					{
						return;
					}
					TLocal tlocal = default(TLocal);
					bool flag2 = false;
					try
					{
						ParallelLoopState32 parallelLoopState = null;
						if (bodyWithState != null)
						{
							parallelLoopState = new ParallelLoopState32(sharedPStateFlags);
						}
						else if (bodyWithLocal != null)
						{
							parallelLoopState = new ParallelLoopState32(sharedPStateFlags);
							if (localInit != null)
							{
								tlocal = localInit();
								flag2 = true;
							}
						}
						Parallel.LoopTimer loopTimer = new Parallel.LoopTimer(rootTask.ActiveChildCount);
						for (;;)
						{
							if (body != null)
							{
								for (int i = num; i < num2; i++)
								{
									if (sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && sharedPStateFlags.ShouldExitLoop())
									{
										break;
									}
									body(i);
								}
							}
							else if (bodyWithState != null)
							{
								for (int j = num; j < num2; j++)
								{
									if (sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && sharedPStateFlags.ShouldExitLoop(j))
									{
										break;
									}
									parallelLoopState.CurrentIteration = j;
									bodyWithState(j, parallelLoopState);
								}
							}
							else
							{
								int num3 = num;
								while (num3 < num2 && (sharedPStateFlags.LoopStateFlags == ParallelLoopStateFlags.PLS_NONE || !sharedPStateFlags.ShouldExitLoop(num3)))
								{
									parallelLoopState.CurrentIteration = num3;
									tlocal = bodyWithLocal(num3, parallelLoopState, tlocal);
									num3++;
								}
							}
							if (!flag && loopTimer.LimitExceeded())
							{
								break;
							}
							if (!rangeWorker.FindNewWork32(out num, out num2) || (sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && sharedPStateFlags.ShouldExitLoop(num)))
							{
								goto IL_1FD;
							}
						}
						internalCurrent.SavedStateForNextReplica = rangeWorker;
						IL_1FD:;
					}
					catch
					{
						sharedPStateFlags.SetExceptional();
						throw;
					}
					finally
					{
						if (localFinally != null && flag2)
						{
							localFinally(tlocal);
						}
					}
				}, creationOptions, internalOptions);
				rootTask.RunSynchronously(parallelOptions.EffectiveTaskScheduler);
				rootTask.Wait();
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				if (oce != null)
				{
					throw oce;
				}
			}
			catch (AggregateException ex)
			{
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				Parallel.ThrowIfReducableToSingleOCE(ex.InnerExceptions, parallelOptions.CancellationToken);
				throw;
			}
			catch (TaskSchedulerException)
			{
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				throw;
			}
			finally
			{
				int loopStateFlags = sharedPStateFlags.LoopStateFlags;
				result.m_completed = (loopStateFlags == ParallelLoopStateFlags.PLS_NONE);
				if ((loopStateFlags & ParallelLoopStateFlags.PLS_BROKEN) != 0)
				{
					result.m_lowestBreakIteration = new long?((long)sharedPStateFlags.LowestBreakIteration);
				}
				if (rootTask != null && rootTask.IsCompleted)
				{
					rootTask.Dispose();
				}
			}
			return result;
		}

		// Token: 0x060037AD RID: 14253 RVA: 0x000C1DE4 File Offset: 0x000BFFE4
		private static ParallelLoopResult ForWorker64<TLocal>(long fromInclusive, long toExclusive, ParallelOptions parallelOptions, Action<long> body, Action<long, ParallelLoopState> bodyWithState, Func<long, ParallelLoopState, TLocal, TLocal> bodyWithLocal, Func<TLocal> localInit, Action<TLocal> localFinally)
		{
			ParallelLoopResult result = default(ParallelLoopResult);
			if (toExclusive <= fromInclusive)
			{
				result.m_completed = true;
				return result;
			}
			ParallelLoopStateFlags64 sharedPStateFlags = new ParallelLoopStateFlags64();
			TaskCreationOptions creationOptions = TaskCreationOptions.None;
			InternalTaskOptions internalOptions = InternalTaskOptions.SelfReplicating;
			if (parallelOptions.CancellationToken.IsCancellationRequested)
			{
				throw new OperationCanceledException(parallelOptions.CancellationToken);
			}
			int nNumExpectedWorkers = (parallelOptions.EffectiveMaxConcurrencyLevel == -1) ? PlatformHelper.ProcessorCount : parallelOptions.EffectiveMaxConcurrencyLevel;
			RangeManager rangeManager = new RangeManager(fromInclusive, toExclusive, 1L, nNumExpectedWorkers);
			OperationCanceledException oce = null;
			CancellationTokenRegistration cancellationTokenRegistration = default(CancellationTokenRegistration);
			if (parallelOptions.CancellationToken.CanBeCanceled)
			{
				cancellationTokenRegistration = parallelOptions.CancellationToken.InternalRegisterWithoutEC(delegate(object o)
				{
					sharedPStateFlags.Cancel();
					oce = new OperationCanceledException(parallelOptions.CancellationToken);
				}, null);
			}
			ParallelForReplicatingTask rootTask = null;
			try
			{
				rootTask = new ParallelForReplicatingTask(parallelOptions, delegate()
				{
					Task internalCurrent = Task.InternalCurrent;
					bool flag = internalCurrent == rootTask;
					RangeWorker rangeWorker = default(RangeWorker);
					object savedStateFromPreviousReplica = internalCurrent.SavedStateFromPreviousReplica;
					if (savedStateFromPreviousReplica is RangeWorker)
					{
						rangeWorker = (RangeWorker)savedStateFromPreviousReplica;
					}
					else
					{
						rangeWorker = rangeManager.RegisterNewWorker();
					}
					long num;
					long num2;
					if (!rangeWorker.FindNewWork(out num, out num2) || sharedPStateFlags.ShouldExitLoop(num))
					{
						return;
					}
					TLocal tlocal = default(TLocal);
					bool flag2 = false;
					try
					{
						ParallelLoopState64 parallelLoopState = null;
						if (bodyWithState != null)
						{
							parallelLoopState = new ParallelLoopState64(sharedPStateFlags);
						}
						else if (bodyWithLocal != null)
						{
							parallelLoopState = new ParallelLoopState64(sharedPStateFlags);
							if (localInit != null)
							{
								tlocal = localInit();
								flag2 = true;
							}
						}
						Parallel.LoopTimer loopTimer = new Parallel.LoopTimer(rootTask.ActiveChildCount);
						for (;;)
						{
							if (body != null)
							{
								for (long num3 = num; num3 < num2; num3 += 1L)
								{
									if (sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && sharedPStateFlags.ShouldExitLoop())
									{
										break;
									}
									body(num3);
								}
							}
							else if (bodyWithState != null)
							{
								for (long num4 = num; num4 < num2; num4 += 1L)
								{
									if (sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && sharedPStateFlags.ShouldExitLoop(num4))
									{
										break;
									}
									parallelLoopState.CurrentIteration = num4;
									bodyWithState(num4, parallelLoopState);
								}
							}
							else
							{
								long num5 = num;
								while (num5 < num2 && (sharedPStateFlags.LoopStateFlags == ParallelLoopStateFlags.PLS_NONE || !sharedPStateFlags.ShouldExitLoop(num5)))
								{
									parallelLoopState.CurrentIteration = num5;
									tlocal = bodyWithLocal(num5, parallelLoopState, tlocal);
									num5 += 1L;
								}
							}
							if (!flag && loopTimer.LimitExceeded())
							{
								break;
							}
							if (!rangeWorker.FindNewWork(out num, out num2) || (sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && sharedPStateFlags.ShouldExitLoop(num)))
							{
								goto IL_200;
							}
						}
						internalCurrent.SavedStateForNextReplica = rangeWorker;
						IL_200:;
					}
					catch
					{
						sharedPStateFlags.SetExceptional();
						throw;
					}
					finally
					{
						if (localFinally != null && flag2)
						{
							localFinally(tlocal);
						}
					}
				}, creationOptions, internalOptions);
				rootTask.RunSynchronously(parallelOptions.EffectiveTaskScheduler);
				rootTask.Wait();
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				if (oce != null)
				{
					throw oce;
				}
			}
			catch (AggregateException ex)
			{
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				Parallel.ThrowIfReducableToSingleOCE(ex.InnerExceptions, parallelOptions.CancellationToken);
				throw;
			}
			catch (TaskSchedulerException)
			{
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				throw;
			}
			finally
			{
				int loopStateFlags = sharedPStateFlags.LoopStateFlags;
				result.m_completed = (loopStateFlags == ParallelLoopStateFlags.PLS_NONE);
				if ((loopStateFlags & ParallelLoopStateFlags.PLS_BROKEN) != 0)
				{
					result.m_lowestBreakIteration = new long?(sharedPStateFlags.LowestBreakIteration);
				}
				if (rootTask != null && rootTask.IsCompleted)
				{
					rootTask.Dispose();
				}
			}
			return result;
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037AE RID: 14254 RVA: 0x000C2044 File Offset: 0x000C0244
		public static ParallelLoopResult ForEach<TSource>(IEnumerable<TSource> source, Action<TSource> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.ForEachWorker<TSource, object>(source, Parallel.s_defaultParallelOptions, body, null, null, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel and loop options can be configured.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037AF RID: 14255 RVA: 0x000C2080 File Offset: 0x000C0280
		public static ParallelLoopResult ForEach<TSource>(IEnumerable<TSource> source, ParallelOptions parallelOptions, Action<TSource> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForEachWorker<TSource, object>(source, parallelOptions, body, null, null, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037B0 RID: 14256 RVA: 0x000C20C8 File Offset: 0x000C02C8
		public static ParallelLoopResult ForEach<TSource>(IEnumerable<TSource> source, Action<TSource, ParallelLoopState> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.ForEachWorker<TSource, object>(source, Parallel.s_defaultParallelOptions, null, body, null, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037B1 RID: 14257 RVA: 0x000C2104 File Offset: 0x000C0304
		public static ParallelLoopResult ForEach<TSource>(IEnumerable<TSource> source, ParallelOptions parallelOptions, Action<TSource, ParallelLoopState> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForEachWorker<TSource, object>(source, parallelOptions, null, body, null, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with 64-bit indexes on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037B2 RID: 14258 RVA: 0x000C214C File Offset: 0x000C034C
		public static ParallelLoopResult ForEach<TSource>(IEnumerable<TSource> source, Action<TSource, ParallelLoopState, long> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.ForEachWorker<TSource, object>(source, Parallel.s_defaultParallelOptions, null, null, body, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with 64-bit indexes on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037B3 RID: 14259 RVA: 0x000C2188 File Offset: 0x000C0388
		public static ParallelLoopResult ForEach<TSource>(IEnumerable<TSource> source, ParallelOptions parallelOptions, Action<TSource, ParallelLoopState, long> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForEachWorker<TSource, object>(source, parallelOptions, null, null, body, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with thread-local data on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037B4 RID: 14260 RVA: 0x000C21D0 File Offset: 0x000C03D0
		public static ParallelLoopResult ForEach<TSource, TLocal>(IEnumerable<TSource> source, Func<TLocal> localInit, Func<TSource, ParallelLoopState, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			return Parallel.ForEachWorker<TSource, TLocal>(source, Parallel.s_defaultParallelOptions, null, null, null, body, null, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with thread-local data on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated..</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037B5 RID: 14261 RVA: 0x000C2228 File Offset: 0x000C0428
		public static ParallelLoopResult ForEach<TSource, TLocal>(IEnumerable<TSource> source, ParallelOptions parallelOptions, Func<TLocal> localInit, Func<TSource, ParallelLoopState, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForEachWorker<TSource, TLocal>(source, parallelOptions, null, null, null, body, null, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with thread-local data on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037B6 RID: 14262 RVA: 0x000C228C File Offset: 0x000C048C
		public static ParallelLoopResult ForEach<TSource, TLocal>(IEnumerable<TSource> source, Func<TLocal> localInit, Func<TSource, ParallelLoopState, long, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			return Parallel.ForEachWorker<TSource, TLocal>(source, Parallel.s_defaultParallelOptions, null, null, null, null, body, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with thread-local data and 64-bit indexes on an <see cref="T:System.Collections.IEnumerable" /> in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">An enumerable data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TSource">The type of the data in the source.</typeparam>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037B7 RID: 14263 RVA: 0x000C22E4 File Offset: 0x000C04E4
		public static ParallelLoopResult ForEach<TSource, TLocal>(IEnumerable<TSource> source, ParallelOptions parallelOptions, Func<TLocal> localInit, Func<TSource, ParallelLoopState, long, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.ForEachWorker<TSource, TLocal>(source, parallelOptions, null, null, null, null, body, localInit, localFinally);
		}

		// Token: 0x060037B8 RID: 14264 RVA: 0x000C2348 File Offset: 0x000C0548
		private static ParallelLoopResult ForEachWorker<TSource, TLocal>(IEnumerable<TSource> source, ParallelOptions parallelOptions, Action<TSource> body, Action<TSource, ParallelLoopState> bodyWithState, Action<TSource, ParallelLoopState, long> bodyWithStateAndIndex, Func<TSource, ParallelLoopState, TLocal, TLocal> bodyWithStateAndLocal, Func<TSource, ParallelLoopState, long, TLocal, TLocal> bodyWithEverything, Func<TLocal> localInit, Action<TLocal> localFinally)
		{
			if (parallelOptions.CancellationToken.IsCancellationRequested)
			{
				throw new OperationCanceledException(parallelOptions.CancellationToken);
			}
			TSource[] array = source as TSource[];
			if (array != null)
			{
				return Parallel.ForEachWorker<TSource, TLocal>(array, parallelOptions, body, bodyWithState, bodyWithStateAndIndex, bodyWithStateAndLocal, bodyWithEverything, localInit, localFinally);
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				return Parallel.ForEachWorker<TSource, TLocal>(list, parallelOptions, body, bodyWithState, bodyWithStateAndIndex, bodyWithStateAndLocal, bodyWithEverything, localInit, localFinally);
			}
			return Parallel.PartitionerForEachWorker<TSource, TLocal>(Partitioner.Create<TSource>(source), parallelOptions, body, bodyWithState, bodyWithStateAndIndex, bodyWithStateAndLocal, bodyWithEverything, localInit, localFinally);
		}

		// Token: 0x060037B9 RID: 14265 RVA: 0x000C23C8 File Offset: 0x000C05C8
		private static ParallelLoopResult ForEachWorker<TSource, TLocal>(TSource[] array, ParallelOptions parallelOptions, Action<TSource> body, Action<TSource, ParallelLoopState> bodyWithState, Action<TSource, ParallelLoopState, long> bodyWithStateAndIndex, Func<TSource, ParallelLoopState, TLocal, TLocal> bodyWithStateAndLocal, Func<TSource, ParallelLoopState, long, TLocal, TLocal> bodyWithEverything, Func<TLocal> localInit, Action<TLocal> localFinally)
		{
			int lowerBound = array.GetLowerBound(0);
			int toExclusive = array.GetUpperBound(0) + 1;
			if (body != null)
			{
				return Parallel.ForWorker<object>(lowerBound, toExclusive, parallelOptions, delegate(int i)
				{
					body(array[i]);
				}, null, null, null, null);
			}
			if (bodyWithState != null)
			{
				return Parallel.ForWorker<object>(lowerBound, toExclusive, parallelOptions, null, delegate(int i, ParallelLoopState state)
				{
					bodyWithState(array[i], state);
				}, null, null, null);
			}
			if (bodyWithStateAndIndex != null)
			{
				return Parallel.ForWorker<object>(lowerBound, toExclusive, parallelOptions, null, delegate(int i, ParallelLoopState state)
				{
					bodyWithStateAndIndex(array[i], state, (long)i);
				}, null, null, null);
			}
			if (bodyWithStateAndLocal != null)
			{
				return Parallel.ForWorker<TLocal>(lowerBound, toExclusive, parallelOptions, null, null, (int i, ParallelLoopState state, TLocal local) => bodyWithStateAndLocal(array[i], state, local), localInit, localFinally);
			}
			return Parallel.ForWorker<TLocal>(lowerBound, toExclusive, parallelOptions, null, null, (int i, ParallelLoopState state, TLocal local) => bodyWithEverything(array[i], state, (long)i, local), localInit, localFinally);
		}

		// Token: 0x060037BA RID: 14266 RVA: 0x000C24C4 File Offset: 0x000C06C4
		private static ParallelLoopResult ForEachWorker<TSource, TLocal>(IList<TSource> list, ParallelOptions parallelOptions, Action<TSource> body, Action<TSource, ParallelLoopState> bodyWithState, Action<TSource, ParallelLoopState, long> bodyWithStateAndIndex, Func<TSource, ParallelLoopState, TLocal, TLocal> bodyWithStateAndLocal, Func<TSource, ParallelLoopState, long, TLocal, TLocal> bodyWithEverything, Func<TLocal> localInit, Action<TLocal> localFinally)
		{
			if (body != null)
			{
				return Parallel.ForWorker<object>(0, list.Count, parallelOptions, delegate(int i)
				{
					body(list[i]);
				}, null, null, null, null);
			}
			if (bodyWithState != null)
			{
				return Parallel.ForWorker<object>(0, list.Count, parallelOptions, null, delegate(int i, ParallelLoopState state)
				{
					bodyWithState(list[i], state);
				}, null, null, null);
			}
			if (bodyWithStateAndIndex != null)
			{
				return Parallel.ForWorker<object>(0, list.Count, parallelOptions, null, delegate(int i, ParallelLoopState state)
				{
					bodyWithStateAndIndex(list[i], state, (long)i);
				}, null, null, null);
			}
			if (bodyWithStateAndLocal != null)
			{
				return Parallel.ForWorker<TLocal>(0, list.Count, parallelOptions, null, null, (int i, ParallelLoopState state, TLocal local) => bodyWithStateAndLocal(list[i], state, local), localInit, localFinally);
			}
			return Parallel.ForWorker<TLocal>(0, list.Count, parallelOptions, null, null, (int i, ParallelLoopState state, TLocal local) => bodyWithEverything(list[i], state, (long)i, local), localInit, localFinally);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on a <see cref="T:System.Collections.Concurrent.Partitioner" /> in which iterations may run in parallel.</summary>
		/// <param name="source">The partitioner that contains the original data source.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is  <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /> partitioner returns <see langword="false" />.-or-The exception that is thrown when any methods in the <paramref name="source" /> partitioner return <see langword="null" />.-or-The <see cref="M:System.Collections.Concurrent.Partitioner`1.GetPartitions(System.Int32)" /> method in the <paramref name="source" /> partitioner does not return the correct number of partitions.</exception>
		// Token: 0x060037BB RID: 14267 RVA: 0x000C25D8 File Offset: 0x000C07D8
		public static ParallelLoopResult ForEach<TSource>(Partitioner<TSource> source, Action<TSource> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.PartitionerForEachWorker<TSource, object>(source, Parallel.s_defaultParallelOptions, body, null, null, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on a <see cref="T:System.Collections.Concurrent.Partitioner" /> in which iterations may run in parallel, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">The partitioner that contains the original data source.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /> partitioner returns <see langword="false" />.-or-A method in the <paramref name="source" /> partitioner returns <see langword="null" />.-or-The <see cref="M:System.Collections.Concurrent.Partitioner`1.GetPartitions(System.Int32)" /> method in the <paramref name="source" /> partitioner does not return the correct number of partitions.</exception>
		// Token: 0x060037BC RID: 14268 RVA: 0x000C2614 File Offset: 0x000C0814
		public static ParallelLoopResult ForEach<TSource>(Partitioner<TSource> source, Action<TSource, ParallelLoopState> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			return Parallel.PartitionerForEachWorker<TSource, object>(source, Parallel.s_defaultParallelOptions, null, body, null, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on a <see cref="T:System.Collections.Concurrent.OrderablePartitioner`1" /> in which iterations may run in parallel and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">The orderable partitioner that contains the original data source.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /> orderable partitioner returns <see langword="false" />.-or-The <see cref="P:System.Collections.Concurrent.OrderablePartitioner`1.KeysNormalized" /> property in the source orderable partitioner returns <see langword="false" />.-or-Any methods in the source orderable partitioner return <see langword="null" />.</exception>
		// Token: 0x060037BD RID: 14269 RVA: 0x000C2650 File Offset: 0x000C0850
		public static ParallelLoopResult ForEach<TSource>(OrderablePartitioner<TSource> source, Action<TSource, ParallelLoopState, long> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (!source.KeysNormalized)
			{
				throw new InvalidOperationException(Environment.GetResourceString("This method requires the use of an OrderedPartitioner with the KeysNormalized property set to true."));
			}
			return Parallel.PartitionerForEachWorker<TSource, object>(source, Parallel.s_defaultParallelOptions, null, null, body, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with thread-local data on a <see cref="T:System.Collections.Concurrent.Partitioner" /> in which iterations may run in parallel and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">The partitioner that contains the original data source.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /><see cref="T:System.Collections.Concurrent.Partitioner" /> returns <see langword="false" /> or the partitioner returns <see langword="null" /> partitions.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037BE RID: 14270 RVA: 0x000C26A4 File Offset: 0x000C08A4
		public static ParallelLoopResult ForEach<TSource, TLocal>(Partitioner<TSource> source, Func<TLocal> localInit, Func<TSource, ParallelLoopState, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			return Parallel.PartitionerForEachWorker<TSource, TLocal>(source, Parallel.s_defaultParallelOptions, null, null, null, body, null, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with thread-local data on a <see cref="T:System.Collections.Concurrent.OrderablePartitioner`1" /> in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">The orderable partitioner that contains the original data source.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /><see cref="T:System.Collections.Concurrent.Partitioner" /> returns <see langword="false" /> or the partitioner returns <see langword="null" /> partitions.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		// Token: 0x060037BF RID: 14271 RVA: 0x000C26FC File Offset: 0x000C08FC
		public static ParallelLoopResult ForEach<TSource, TLocal>(OrderablePartitioner<TSource> source, Func<TLocal> localInit, Func<TSource, ParallelLoopState, long, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			if (!source.KeysNormalized)
			{
				throw new InvalidOperationException(Environment.GetResourceString("This method requires the use of an OrderedPartitioner with the KeysNormalized property set to true."));
			}
			return Parallel.PartitionerForEachWorker<TSource, TLocal>(source, Parallel.s_defaultParallelOptions, null, null, null, null, body, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on a <see cref="T:System.Collections.Concurrent.Partitioner" /> in which iterations may run in parallel and loop options can be configured.</summary>
		/// <param name="source">The partitioner that contains the original data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /> partitioner returns <see langword="false" />.-or-The exception that is thrown when any methods in the <paramref name="source" /> partitioner return <see langword="null" />.</exception>
		// Token: 0x060037C0 RID: 14272 RVA: 0x000C276C File Offset: 0x000C096C
		public static ParallelLoopResult ForEach<TSource>(Partitioner<TSource> source, ParallelOptions parallelOptions, Action<TSource> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.PartitionerForEachWorker<TSource, object>(source, parallelOptions, body, null, null, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on a <see cref="T:System.Collections.Concurrent.Partitioner" /> in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">The partitioner that contains the original data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <returns>A  structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /> partitioner returns <see langword="false" />.-or-The exception that is thrown when any methods in the <paramref name="source" /> partitioner return <see langword="null" />.</exception>
		// Token: 0x060037C1 RID: 14273 RVA: 0x000C27B4 File Offset: 0x000C09B4
		public static ParallelLoopResult ForEach<TSource>(Partitioner<TSource> source, ParallelOptions parallelOptions, Action<TSource, ParallelLoopState> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.PartitionerForEachWorker<TSource, object>(source, parallelOptions, null, body, null, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation on a <see cref="T:System.Collections.Concurrent.OrderablePartitioner`1" /> in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">The orderable partitioner that contains the original data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is  <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /> orderable partitioner returns <see langword="false" />.-or-The <see cref="P:System.Collections.Concurrent.OrderablePartitioner`1.KeysNormalized" /> property in the <paramref name="source" /> orderable partitioner returns <see langword="false" />.-or-The exception that is thrown when any methods in the <paramref name="source" /> orderable partitioner return <see langword="null" />.</exception>
		// Token: 0x060037C2 RID: 14274 RVA: 0x000C27FC File Offset: 0x000C09FC
		public static ParallelLoopResult ForEach<TSource>(OrderablePartitioner<TSource> source, ParallelOptions parallelOptions, Action<TSource, ParallelLoopState, long> body)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			if (!source.KeysNormalized)
			{
				throw new InvalidOperationException(Environment.GetResourceString("This method requires the use of an OrderedPartitioner with the KeysNormalized property set to true."));
			}
			return Parallel.PartitionerForEachWorker<TSource, object>(source, parallelOptions, null, null, body, null, null, null, null);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation  with thread-local data on a <see cref="T:System.Collections.Concurrent.Partitioner" /> in which iterations may run in parallel, loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">The partitioner that contains the original data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> argument is <see langword="null" />.-or-The <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /><see cref="T:System.Collections.Concurrent.Partitioner" /> returns <see langword="false" /> or the partitioner returns <see langword="null" /> partitions.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037C3 RID: 14275 RVA: 0x000C285C File Offset: 0x000C0A5C
		public static ParallelLoopResult ForEach<TSource, TLocal>(Partitioner<TSource> source, ParallelOptions parallelOptions, Func<TLocal> localInit, Func<TSource, ParallelLoopState, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			return Parallel.PartitionerForEachWorker<TSource, TLocal>(source, parallelOptions, null, null, null, body, null, localInit, localFinally);
		}

		/// <summary>Executes a <see langword="foreach" /> (<see langword="For Each" /> in Visual Basic) operation with 64-bit indexes and  with thread-local data on a <see cref="T:System.Collections.Concurrent.OrderablePartitioner`1" /> in which iterations may run in parallel , loop options can be configured, and the state of the loop can be monitored and manipulated.</summary>
		/// <param name="source">The orderable partitioner that contains the original data source.</param>
		/// <param name="parallelOptions">An object that configures the behavior of this operation.</param>
		/// <param name="localInit">The function delegate that returns the initial state of the local data for each task.</param>
		/// <param name="body">The delegate that is invoked once per iteration.</param>
		/// <param name="localFinally">The delegate that performs a final action on the local state of each task.</param>
		/// <typeparam name="TSource">The type of the elements in <paramref name="source" />.</typeparam>
		/// <typeparam name="TLocal">The type of the thread-local data.</typeparam>
		/// <returns>A structure that contains information about which portion of the loop completed.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="source" /> argument is <see langword="null" />.-or-The <paramref name="parallelOptions" /> argument is <see langword="null" />.-or-The <paramref name="body" /> argument is <see langword="null" />.-or-The <paramref name="localInit" /> or <paramref name="localFinally" /> argument is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Collections.Concurrent.Partitioner`1.SupportsDynamicPartitions" /> property in the <paramref name="source" /><see cref="T:System.Collections.Concurrent.Partitioner" /> returns <see langword="false" /> or the partitioner returns <see langword="null" />  partitions.</exception>
		/// <exception cref="T:System.AggregateException">The exception that contains all the individual exceptions thrown on all threads.</exception>
		/// <exception cref="T:System.OperationCanceledException">The <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> argument is canceled.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.Threading.CancellationTokenSource" /> associated with the <see cref="T:System.Threading.CancellationToken" /> in the <paramref name="parallelOptions" /> has been disposed.</exception>
		// Token: 0x060037C4 RID: 14276 RVA: 0x000C28C0 File Offset: 0x000C0AC0
		public static ParallelLoopResult ForEach<TSource, TLocal>(OrderablePartitioner<TSource> source, ParallelOptions parallelOptions, Func<TLocal> localInit, Func<TSource, ParallelLoopState, long, TLocal, TLocal> body, Action<TLocal> localFinally)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (body == null)
			{
				throw new ArgumentNullException("body");
			}
			if (localInit == null)
			{
				throw new ArgumentNullException("localInit");
			}
			if (localFinally == null)
			{
				throw new ArgumentNullException("localFinally");
			}
			if (parallelOptions == null)
			{
				throw new ArgumentNullException("parallelOptions");
			}
			if (!source.KeysNormalized)
			{
				throw new InvalidOperationException(Environment.GetResourceString("This method requires the use of an OrderedPartitioner with the KeysNormalized property set to true."));
			}
			return Parallel.PartitionerForEachWorker<TSource, TLocal>(source, parallelOptions, null, null, null, null, body, localInit, localFinally);
		}

		// Token: 0x060037C5 RID: 14277 RVA: 0x000C293C File Offset: 0x000C0B3C
		private static ParallelLoopResult PartitionerForEachWorker<TSource, TLocal>(Partitioner<TSource> source, ParallelOptions parallelOptions, Action<TSource> simpleBody, Action<TSource, ParallelLoopState> bodyWithState, Action<TSource, ParallelLoopState, long> bodyWithStateAndIndex, Func<TSource, ParallelLoopState, TLocal, TLocal> bodyWithStateAndLocal, Func<TSource, ParallelLoopState, long, TLocal, TLocal> bodyWithEverything, Func<TLocal> localInit, Action<TLocal> localFinally)
		{
			OrderablePartitioner<TSource> orderedSource = source as OrderablePartitioner<TSource>;
			if (!source.SupportsDynamicPartitions)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The Partitioner used here must support dynamic partitioning."));
			}
			if (parallelOptions.CancellationToken.IsCancellationRequested)
			{
				throw new OperationCanceledException(parallelOptions.CancellationToken);
			}
			ParallelLoopStateFlags64 sharedPStateFlags = new ParallelLoopStateFlags64();
			ParallelLoopResult result = default(ParallelLoopResult);
			OperationCanceledException oce = null;
			CancellationTokenRegistration cancellationTokenRegistration = default(CancellationTokenRegistration);
			if (parallelOptions.CancellationToken.CanBeCanceled)
			{
				cancellationTokenRegistration = parallelOptions.CancellationToken.InternalRegisterWithoutEC(delegate(object o)
				{
					sharedPStateFlags.Cancel();
					oce = new OperationCanceledException(parallelOptions.CancellationToken);
				}, null);
			}
			IEnumerable<TSource> partitionerSource = null;
			IEnumerable<KeyValuePair<long, TSource>> orderablePartitionerSource = null;
			if (orderedSource != null)
			{
				orderablePartitionerSource = orderedSource.GetOrderableDynamicPartitions();
				if (orderablePartitionerSource == null)
				{
					throw new InvalidOperationException(Environment.GetResourceString("The Partitioner used here returned a null partitioner source."));
				}
			}
			else
			{
				partitionerSource = source.GetDynamicPartitions();
				if (partitionerSource == null)
				{
					throw new InvalidOperationException(Environment.GetResourceString("The Partitioner used here returned a null partitioner source."));
				}
			}
			ParallelForReplicatingTask rootTask = null;
			Action action = delegate()
			{
				Task internalCurrent = Task.InternalCurrent;
				TLocal tlocal = default(TLocal);
				bool flag = false;
				IDisposable disposable2 = null;
				try
				{
					ParallelLoopState64 parallelLoopState = null;
					if (bodyWithState != null || bodyWithStateAndIndex != null)
					{
						parallelLoopState = new ParallelLoopState64(sharedPStateFlags);
					}
					else if (bodyWithStateAndLocal != null || bodyWithEverything != null)
					{
						parallelLoopState = new ParallelLoopState64(sharedPStateFlags);
						if (localInit != null)
						{
							tlocal = localInit();
							flag = true;
						}
					}
					bool flag2 = rootTask == internalCurrent;
					Parallel.LoopTimer loopTimer = new Parallel.LoopTimer(rootTask.ActiveChildCount);
					if (orderedSource != null)
					{
						IEnumerator<KeyValuePair<long, TSource>> enumerator = internalCurrent.SavedStateFromPreviousReplica as IEnumerator<KeyValuePair<long, TSource>>;
						if (enumerator == null)
						{
							enumerator = orderablePartitionerSource.GetEnumerator();
							if (enumerator == null)
							{
								throw new InvalidOperationException(Environment.GetResourceString("The Partitioner source returned a null enumerator."));
							}
						}
						disposable2 = enumerator;
						while (enumerator.MoveNext())
						{
							KeyValuePair<long, TSource> keyValuePair = enumerator.Current;
							long key = keyValuePair.Key;
							TSource value = keyValuePair.Value;
							if (parallelLoopState != null)
							{
								parallelLoopState.CurrentIteration = key;
							}
							if (simpleBody != null)
							{
								simpleBody(value);
							}
							else if (bodyWithState != null)
							{
								bodyWithState(value, parallelLoopState);
							}
							else if (bodyWithStateAndIndex != null)
							{
								bodyWithStateAndIndex(value, parallelLoopState, key);
							}
							else if (bodyWithStateAndLocal != null)
							{
								tlocal = bodyWithStateAndLocal(value, parallelLoopState, tlocal);
							}
							else
							{
								tlocal = bodyWithEverything(value, parallelLoopState, key, tlocal);
							}
							if (sharedPStateFlags.ShouldExitLoop(key))
							{
								break;
							}
							if (!flag2 && loopTimer.LimitExceeded())
							{
								internalCurrent.SavedStateForNextReplica = enumerator;
								disposable2 = null;
								break;
							}
						}
					}
					else
					{
						IEnumerator<TSource> enumerator2 = internalCurrent.SavedStateFromPreviousReplica as IEnumerator<TSource>;
						if (enumerator2 == null)
						{
							enumerator2 = partitionerSource.GetEnumerator();
							if (enumerator2 == null)
							{
								throw new InvalidOperationException(Environment.GetResourceString("The Partitioner source returned a null enumerator."));
							}
						}
						disposable2 = enumerator2;
						if (parallelLoopState != null)
						{
							parallelLoopState.CurrentIteration = 0L;
						}
						while (enumerator2.MoveNext())
						{
							TSource tsource = enumerator2.Current;
							if (simpleBody != null)
							{
								simpleBody(tsource);
							}
							else if (bodyWithState != null)
							{
								bodyWithState(tsource, parallelLoopState);
							}
							else if (bodyWithStateAndLocal != null)
							{
								tlocal = bodyWithStateAndLocal(tsource, parallelLoopState, tlocal);
							}
							if (sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE)
							{
								break;
							}
							if (!flag2 && loopTimer.LimitExceeded())
							{
								internalCurrent.SavedStateForNextReplica = enumerator2;
								disposable2 = null;
								break;
							}
						}
					}
				}
				catch
				{
					sharedPStateFlags.SetExceptional();
					throw;
				}
				finally
				{
					if (localFinally != null && flag)
					{
						localFinally(tlocal);
					}
					if (disposable2 != null)
					{
						disposable2.Dispose();
					}
				}
			};
			try
			{
				rootTask = new ParallelForReplicatingTask(parallelOptions, action, TaskCreationOptions.None, InternalTaskOptions.SelfReplicating);
				rootTask.RunSynchronously(parallelOptions.EffectiveTaskScheduler);
				rootTask.Wait();
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				if (oce != null)
				{
					throw oce;
				}
			}
			catch (AggregateException ex)
			{
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				Parallel.ThrowIfReducableToSingleOCE(ex.InnerExceptions, parallelOptions.CancellationToken);
				throw;
			}
			catch (TaskSchedulerException)
			{
				if (parallelOptions.CancellationToken.CanBeCanceled)
				{
					cancellationTokenRegistration.Dispose();
				}
				throw;
			}
			finally
			{
				int loopStateFlags = sharedPStateFlags.LoopStateFlags;
				result.m_completed = (loopStateFlags == ParallelLoopStateFlags.PLS_NONE);
				if ((loopStateFlags & ParallelLoopStateFlags.PLS_BROKEN) != 0)
				{
					result.m_lowestBreakIteration = new long?(sharedPStateFlags.LowestBreakIteration);
				}
				if (rootTask != null && rootTask.IsCompleted)
				{
					rootTask.Dispose();
				}
				IDisposable disposable;
				if (orderablePartitionerSource != null)
				{
					disposable = (orderablePartitionerSource as IDisposable);
				}
				else
				{
					disposable = (partitionerSource as IDisposable);
				}
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
			return result;
		}

		// Token: 0x060037C6 RID: 14278 RVA: 0x000C2C20 File Offset: 0x000C0E20
		internal static void ThrowIfReducableToSingleOCE(IEnumerable<Exception> excCollection, CancellationToken ct)
		{
			bool flag = false;
			if (ct.IsCancellationRequested)
			{
				foreach (Exception ex in excCollection)
				{
					flag = true;
					OperationCanceledException ex2 = ex as OperationCanceledException;
					if (ex2 == null || ex2.CancellationToken != ct)
					{
						return;
					}
				}
				if (flag)
				{
					throw new OperationCanceledException(ct);
				}
			}
		}

		// Token: 0x060037C7 RID: 14279 RVA: 0x000C2C90 File Offset: 0x000C0E90
		// Note: this type is marked as 'beforefieldinit'.
		static Parallel()
		{
		}

		// Token: 0x04001D2E RID: 7470
		internal static int s_forkJoinContextID;

		// Token: 0x04001D2F RID: 7471
		internal const int DEFAULT_LOOP_STRIDE = 16;

		// Token: 0x04001D30 RID: 7472
		internal static ParallelOptions s_defaultParallelOptions = new ParallelOptions();

		// Token: 0x020004EF RID: 1263
		internal struct LoopTimer
		{
			// Token: 0x060037C8 RID: 14280 RVA: 0x000C2C9C File Offset: 0x000C0E9C
			public LoopTimer(int nWorkerTaskIndex)
			{
				int num = 100 + nWorkerTaskIndex % PlatformHelper.ProcessorCount * 50;
				this.m_timeLimit = Environment.TickCount + num;
			}

			// Token: 0x060037C9 RID: 14281 RVA: 0x000C2CC4 File Offset: 0x000C0EC4
			public bool LimitExceeded()
			{
				return Environment.TickCount > this.m_timeLimit;
			}

			// Token: 0x04001D31 RID: 7473
			private const int s_BaseNotifyPeriodMS = 100;

			// Token: 0x04001D32 RID: 7474
			private const int s_NotifyPeriodIncrementMS = 50;

			// Token: 0x04001D33 RID: 7475
			private int m_timeLimit;
		}

		// Token: 0x020004F0 RID: 1264
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x060037CA RID: 14282 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x04001D34 RID: 7476
			public Action[] actionsCopy;

			// Token: 0x04001D35 RID: 7477
			public ParallelOptions parallelOptions;
		}

		// Token: 0x020004F1 RID: 1265
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_1
		{
			// Token: 0x060037CB RID: 14283 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass4_1()
			{
			}

			// Token: 0x04001D36 RID: 7478
			public ConcurrentQueue<Exception> exceptionQ;

			// Token: 0x04001D37 RID: 7479
			public Parallel.<>c__DisplayClass4_0 CS$<>8__locals1;
		}

		// Token: 0x020004F2 RID: 1266
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_2
		{
			// Token: 0x060037CC RID: 14284 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass4_2()
			{
			}

			// Token: 0x060037CD RID: 14285 RVA: 0x000C2CD4 File Offset: 0x000C0ED4
			internal void <Invoke>b__0()
			{
				for (int i = Interlocked.Increment(ref this.actionIndex); i <= this.CS$<>8__locals2.CS$<>8__locals1.actionsCopy.Length; i = Interlocked.Increment(ref this.actionIndex))
				{
					try
					{
						this.CS$<>8__locals2.CS$<>8__locals1.actionsCopy[i - 1]();
					}
					catch (Exception item)
					{
						LazyInitializer.EnsureInitialized<ConcurrentQueue<Exception>>(ref this.CS$<>8__locals2.exceptionQ, new Func<ConcurrentQueue<Exception>>(Parallel.<>c.<>9.<Invoke>b__4_1));
						this.CS$<>8__locals2.exceptionQ.Enqueue(item);
					}
					if (this.CS$<>8__locals2.CS$<>8__locals1.parallelOptions.CancellationToken.IsCancellationRequested)
					{
						throw new OperationCanceledException(this.CS$<>8__locals2.CS$<>8__locals1.parallelOptions.CancellationToken);
					}
				}
			}

			// Token: 0x04001D38 RID: 7480
			public int actionIndex;

			// Token: 0x04001D39 RID: 7481
			public Parallel.<>c__DisplayClass4_1 CS$<>8__locals2;
		}

		// Token: 0x020004F3 RID: 1267
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060037CE RID: 14286 RVA: 0x000C2DC0 File Offset: 0x000C0FC0
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060037CF RID: 14287 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x060037D0 RID: 14288 RVA: 0x000C2DCC File Offset: 0x000C0FCC
			internal ConcurrentQueue<Exception> <Invoke>b__4_1()
			{
				return new ConcurrentQueue<Exception>();
			}

			// Token: 0x060037D1 RID: 14289 RVA: 0x000C2DCC File Offset: 0x000C0FCC
			internal ConcurrentQueue<Exception> <Invoke>b__4_2()
			{
				return new ConcurrentQueue<Exception>();
			}

			// Token: 0x04001D3A RID: 7482
			public static readonly Parallel.<>c <>9 = new Parallel.<>c();

			// Token: 0x04001D3B RID: 7483
			public static Func<ConcurrentQueue<Exception>> <>9__4_1;

			// Token: 0x04001D3C RID: 7484
			public static Func<ConcurrentQueue<Exception>> <>9__4_2;
		}

		// Token: 0x020004F4 RID: 1268
		[CompilerGenerated]
		private sealed class <>c__DisplayClass17_0<TLocal>
		{
			// Token: 0x060037D2 RID: 14290 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass17_0()
			{
			}

			// Token: 0x060037D3 RID: 14291 RVA: 0x000C2DD3 File Offset: 0x000C0FD3
			internal void <ForWorker>b__0(object o)
			{
				this.sharedPStateFlags.Cancel();
				this.oce = new OperationCanceledException(this.parallelOptions.CancellationToken);
			}

			// Token: 0x060037D4 RID: 14292 RVA: 0x000C2DF8 File Offset: 0x000C0FF8
			internal void <ForWorker>b__1()
			{
				Task internalCurrent = Task.InternalCurrent;
				bool flag = internalCurrent == this.rootTask;
				RangeWorker rangeWorker = default(RangeWorker);
				object savedStateFromPreviousReplica = internalCurrent.SavedStateFromPreviousReplica;
				if (savedStateFromPreviousReplica is RangeWorker)
				{
					rangeWorker = (RangeWorker)savedStateFromPreviousReplica;
				}
				else
				{
					rangeWorker = this.rangeManager.RegisterNewWorker();
				}
				int num;
				int num2;
				if (!rangeWorker.FindNewWork32(out num, out num2) || this.sharedPStateFlags.ShouldExitLoop(num))
				{
					return;
				}
				TLocal tlocal = default(TLocal);
				bool flag2 = false;
				try
				{
					ParallelLoopState32 parallelLoopState = null;
					if (this.bodyWithState != null)
					{
						parallelLoopState = new ParallelLoopState32(this.sharedPStateFlags);
					}
					else if (this.bodyWithLocal != null)
					{
						parallelLoopState = new ParallelLoopState32(this.sharedPStateFlags);
						if (this.localInit != null)
						{
							tlocal = this.localInit();
							flag2 = true;
						}
					}
					Parallel.LoopTimer loopTimer = new Parallel.LoopTimer(this.rootTask.ActiveChildCount);
					for (;;)
					{
						if (this.body != null)
						{
							for (int i = num; i < num2; i++)
							{
								if (this.sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && this.sharedPStateFlags.ShouldExitLoop())
								{
									break;
								}
								this.body(i);
							}
						}
						else if (this.bodyWithState != null)
						{
							for (int j = num; j < num2; j++)
							{
								if (this.sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && this.sharedPStateFlags.ShouldExitLoop(j))
								{
									break;
								}
								parallelLoopState.CurrentIteration = j;
								this.bodyWithState(j, parallelLoopState);
							}
						}
						else
						{
							int num3 = num;
							while (num3 < num2 && (this.sharedPStateFlags.LoopStateFlags == ParallelLoopStateFlags.PLS_NONE || !this.sharedPStateFlags.ShouldExitLoop(num3)))
							{
								parallelLoopState.CurrentIteration = num3;
								tlocal = this.bodyWithLocal(num3, parallelLoopState, tlocal);
								num3++;
							}
						}
						if (!flag && loopTimer.LimitExceeded())
						{
							break;
						}
						if (!rangeWorker.FindNewWork32(out num, out num2) || (this.sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && this.sharedPStateFlags.ShouldExitLoop(num)))
						{
							goto IL_1FD;
						}
					}
					internalCurrent.SavedStateForNextReplica = rangeWorker;
					IL_1FD:;
				}
				catch
				{
					this.sharedPStateFlags.SetExceptional();
					throw;
				}
				finally
				{
					if (this.localFinally != null && flag2)
					{
						this.localFinally(tlocal);
					}
				}
			}

			// Token: 0x04001D3D RID: 7485
			public ParallelLoopStateFlags32 sharedPStateFlags;

			// Token: 0x04001D3E RID: 7486
			public OperationCanceledException oce;

			// Token: 0x04001D3F RID: 7487
			public ParallelOptions parallelOptions;

			// Token: 0x04001D40 RID: 7488
			public ParallelForReplicatingTask rootTask;

			// Token: 0x04001D41 RID: 7489
			public RangeManager rangeManager;

			// Token: 0x04001D42 RID: 7490
			public Action<int, ParallelLoopState> bodyWithState;

			// Token: 0x04001D43 RID: 7491
			public Func<int, ParallelLoopState, TLocal, TLocal> bodyWithLocal;

			// Token: 0x04001D44 RID: 7492
			public Func<TLocal> localInit;

			// Token: 0x04001D45 RID: 7493
			public Action<int> body;

			// Token: 0x04001D46 RID: 7494
			public Action<TLocal> localFinally;
		}

		// Token: 0x020004F5 RID: 1269
		[CompilerGenerated]
		private sealed class <>c__DisplayClass18_0<TLocal>
		{
			// Token: 0x060037D5 RID: 14293 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass18_0()
			{
			}

			// Token: 0x060037D6 RID: 14294 RVA: 0x000C3064 File Offset: 0x000C1264
			internal void <ForWorker64>b__0(object o)
			{
				this.sharedPStateFlags.Cancel();
				this.oce = new OperationCanceledException(this.parallelOptions.CancellationToken);
			}

			// Token: 0x060037D7 RID: 14295 RVA: 0x000C3088 File Offset: 0x000C1288
			internal void <ForWorker64>b__1()
			{
				Task internalCurrent = Task.InternalCurrent;
				bool flag = internalCurrent == this.rootTask;
				RangeWorker rangeWorker = default(RangeWorker);
				object savedStateFromPreviousReplica = internalCurrent.SavedStateFromPreviousReplica;
				if (savedStateFromPreviousReplica is RangeWorker)
				{
					rangeWorker = (RangeWorker)savedStateFromPreviousReplica;
				}
				else
				{
					rangeWorker = this.rangeManager.RegisterNewWorker();
				}
				long num;
				long num2;
				if (!rangeWorker.FindNewWork(out num, out num2) || this.sharedPStateFlags.ShouldExitLoop(num))
				{
					return;
				}
				TLocal tlocal = default(TLocal);
				bool flag2 = false;
				try
				{
					ParallelLoopState64 parallelLoopState = null;
					if (this.bodyWithState != null)
					{
						parallelLoopState = new ParallelLoopState64(this.sharedPStateFlags);
					}
					else if (this.bodyWithLocal != null)
					{
						parallelLoopState = new ParallelLoopState64(this.sharedPStateFlags);
						if (this.localInit != null)
						{
							tlocal = this.localInit();
							flag2 = true;
						}
					}
					Parallel.LoopTimer loopTimer = new Parallel.LoopTimer(this.rootTask.ActiveChildCount);
					for (;;)
					{
						if (this.body != null)
						{
							for (long num3 = num; num3 < num2; num3 += 1L)
							{
								if (this.sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && this.sharedPStateFlags.ShouldExitLoop())
								{
									break;
								}
								this.body(num3);
							}
						}
						else if (this.bodyWithState != null)
						{
							for (long num4 = num; num4 < num2; num4 += 1L)
							{
								if (this.sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && this.sharedPStateFlags.ShouldExitLoop(num4))
								{
									break;
								}
								parallelLoopState.CurrentIteration = num4;
								this.bodyWithState(num4, parallelLoopState);
							}
						}
						else
						{
							long num5 = num;
							while (num5 < num2 && (this.sharedPStateFlags.LoopStateFlags == ParallelLoopStateFlags.PLS_NONE || !this.sharedPStateFlags.ShouldExitLoop(num5)))
							{
								parallelLoopState.CurrentIteration = num5;
								tlocal = this.bodyWithLocal(num5, parallelLoopState, tlocal);
								num5 += 1L;
							}
						}
						if (!flag && loopTimer.LimitExceeded())
						{
							break;
						}
						if (!rangeWorker.FindNewWork(out num, out num2) || (this.sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE && this.sharedPStateFlags.ShouldExitLoop(num)))
						{
							goto IL_200;
						}
					}
					internalCurrent.SavedStateForNextReplica = rangeWorker;
					IL_200:;
				}
				catch
				{
					this.sharedPStateFlags.SetExceptional();
					throw;
				}
				finally
				{
					if (this.localFinally != null && flag2)
					{
						this.localFinally(tlocal);
					}
				}
			}

			// Token: 0x04001D47 RID: 7495
			public ParallelLoopStateFlags64 sharedPStateFlags;

			// Token: 0x04001D48 RID: 7496
			public OperationCanceledException oce;

			// Token: 0x04001D49 RID: 7497
			public ParallelOptions parallelOptions;

			// Token: 0x04001D4A RID: 7498
			public ParallelForReplicatingTask rootTask;

			// Token: 0x04001D4B RID: 7499
			public RangeManager rangeManager;

			// Token: 0x04001D4C RID: 7500
			public Action<long, ParallelLoopState> bodyWithState;

			// Token: 0x04001D4D RID: 7501
			public Func<long, ParallelLoopState, TLocal, TLocal> bodyWithLocal;

			// Token: 0x04001D4E RID: 7502
			public Func<TLocal> localInit;

			// Token: 0x04001D4F RID: 7503
			public Action<long> body;

			// Token: 0x04001D50 RID: 7504
			public Action<TLocal> localFinally;
		}

		// Token: 0x020004F6 RID: 1270
		[CompilerGenerated]
		private sealed class <>c__DisplayClass30_0<TSource, TLocal>
		{
			// Token: 0x060037D8 RID: 14296 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass30_0()
			{
			}

			// Token: 0x060037D9 RID: 14297 RVA: 0x000C32F8 File Offset: 0x000C14F8
			internal void <ForEachWorker>b__0(int i)
			{
				this.body(this.array[i]);
			}

			// Token: 0x060037DA RID: 14298 RVA: 0x000C3311 File Offset: 0x000C1511
			internal void <ForEachWorker>b__1(int i, ParallelLoopState state)
			{
				this.bodyWithState(this.array[i], state);
			}

			// Token: 0x060037DB RID: 14299 RVA: 0x000C332B File Offset: 0x000C152B
			internal void <ForEachWorker>b__2(int i, ParallelLoopState state)
			{
				this.bodyWithStateAndIndex(this.array[i], state, (long)i);
			}

			// Token: 0x060037DC RID: 14300 RVA: 0x000C3347 File Offset: 0x000C1547
			internal TLocal <ForEachWorker>b__3(int i, ParallelLoopState state, TLocal local)
			{
				return this.bodyWithStateAndLocal(this.array[i], state, local);
			}

			// Token: 0x060037DD RID: 14301 RVA: 0x000C3362 File Offset: 0x000C1562
			internal TLocal <ForEachWorker>b__4(int i, ParallelLoopState state, TLocal local)
			{
				return this.bodyWithEverything(this.array[i], state, (long)i, local);
			}

			// Token: 0x04001D51 RID: 7505
			public Action<TSource> body;

			// Token: 0x04001D52 RID: 7506
			public TSource[] array;

			// Token: 0x04001D53 RID: 7507
			public Action<TSource, ParallelLoopState> bodyWithState;

			// Token: 0x04001D54 RID: 7508
			public Action<TSource, ParallelLoopState, long> bodyWithStateAndIndex;

			// Token: 0x04001D55 RID: 7509
			public Func<TSource, ParallelLoopState, TLocal, TLocal> bodyWithStateAndLocal;

			// Token: 0x04001D56 RID: 7510
			public Func<TSource, ParallelLoopState, long, TLocal, TLocal> bodyWithEverything;
		}

		// Token: 0x020004F7 RID: 1271
		[CompilerGenerated]
		private sealed class <>c__DisplayClass31_0<TSource, TLocal>
		{
			// Token: 0x060037DE RID: 14302 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass31_0()
			{
			}

			// Token: 0x060037DF RID: 14303 RVA: 0x000C337F File Offset: 0x000C157F
			internal void <ForEachWorker>b__0(int i)
			{
				this.body(this.list[i]);
			}

			// Token: 0x060037E0 RID: 14304 RVA: 0x000C3398 File Offset: 0x000C1598
			internal void <ForEachWorker>b__1(int i, ParallelLoopState state)
			{
				this.bodyWithState(this.list[i], state);
			}

			// Token: 0x060037E1 RID: 14305 RVA: 0x000C33B2 File Offset: 0x000C15B2
			internal void <ForEachWorker>b__2(int i, ParallelLoopState state)
			{
				this.bodyWithStateAndIndex(this.list[i], state, (long)i);
			}

			// Token: 0x060037E2 RID: 14306 RVA: 0x000C33CE File Offset: 0x000C15CE
			internal TLocal <ForEachWorker>b__3(int i, ParallelLoopState state, TLocal local)
			{
				return this.bodyWithStateAndLocal(this.list[i], state, local);
			}

			// Token: 0x060037E3 RID: 14307 RVA: 0x000C33E9 File Offset: 0x000C15E9
			internal TLocal <ForEachWorker>b__4(int i, ParallelLoopState state, TLocal local)
			{
				return this.bodyWithEverything(this.list[i], state, (long)i, local);
			}

			// Token: 0x04001D57 RID: 7511
			public Action<TSource> body;

			// Token: 0x04001D58 RID: 7512
			public IList<TSource> list;

			// Token: 0x04001D59 RID: 7513
			public Action<TSource, ParallelLoopState> bodyWithState;

			// Token: 0x04001D5A RID: 7514
			public Action<TSource, ParallelLoopState, long> bodyWithStateAndIndex;

			// Token: 0x04001D5B RID: 7515
			public Func<TSource, ParallelLoopState, TLocal, TLocal> bodyWithStateAndLocal;

			// Token: 0x04001D5C RID: 7516
			public Func<TSource, ParallelLoopState, long, TLocal, TLocal> bodyWithEverything;
		}

		// Token: 0x020004F8 RID: 1272
		[CompilerGenerated]
		private sealed class <>c__DisplayClass42_0<TSource, TLocal>
		{
			// Token: 0x060037E4 RID: 14308 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass42_0()
			{
			}

			// Token: 0x060037E5 RID: 14309 RVA: 0x000C3406 File Offset: 0x000C1606
			internal void <PartitionerForEachWorker>b__0(object o)
			{
				this.sharedPStateFlags.Cancel();
				this.oce = new OperationCanceledException(this.parallelOptions.CancellationToken);
			}

			// Token: 0x060037E6 RID: 14310 RVA: 0x000C342C File Offset: 0x000C162C
			internal void <PartitionerForEachWorker>b__1()
			{
				Task internalCurrent = Task.InternalCurrent;
				TLocal tlocal = default(TLocal);
				bool flag = false;
				IDisposable disposable = null;
				try
				{
					ParallelLoopState64 parallelLoopState = null;
					if (this.bodyWithState != null || this.bodyWithStateAndIndex != null)
					{
						parallelLoopState = new ParallelLoopState64(this.sharedPStateFlags);
					}
					else if (this.bodyWithStateAndLocal != null || this.bodyWithEverything != null)
					{
						parallelLoopState = new ParallelLoopState64(this.sharedPStateFlags);
						if (this.localInit != null)
						{
							tlocal = this.localInit();
							flag = true;
						}
					}
					bool flag2 = this.rootTask == internalCurrent;
					Parallel.LoopTimer loopTimer = new Parallel.LoopTimer(this.rootTask.ActiveChildCount);
					if (this.orderedSource != null)
					{
						IEnumerator<KeyValuePair<long, TSource>> enumerator = internalCurrent.SavedStateFromPreviousReplica as IEnumerator<KeyValuePair<long, TSource>>;
						if (enumerator == null)
						{
							enumerator = this.orderablePartitionerSource.GetEnumerator();
							if (enumerator == null)
							{
								throw new InvalidOperationException(Environment.GetResourceString("The Partitioner source returned a null enumerator."));
							}
						}
						disposable = enumerator;
						while (enumerator.MoveNext())
						{
							KeyValuePair<long, TSource> keyValuePair = enumerator.Current;
							long key = keyValuePair.Key;
							TSource value = keyValuePair.Value;
							if (parallelLoopState != null)
							{
								parallelLoopState.CurrentIteration = key;
							}
							if (this.simpleBody != null)
							{
								this.simpleBody(value);
							}
							else if (this.bodyWithState != null)
							{
								this.bodyWithState(value, parallelLoopState);
							}
							else if (this.bodyWithStateAndIndex != null)
							{
								this.bodyWithStateAndIndex(value, parallelLoopState, key);
							}
							else if (this.bodyWithStateAndLocal != null)
							{
								tlocal = this.bodyWithStateAndLocal(value, parallelLoopState, tlocal);
							}
							else
							{
								tlocal = this.bodyWithEverything(value, parallelLoopState, key, tlocal);
							}
							if (this.sharedPStateFlags.ShouldExitLoop(key))
							{
								break;
							}
							if (!flag2 && loopTimer.LimitExceeded())
							{
								internalCurrent.SavedStateForNextReplica = enumerator;
								disposable = null;
								break;
							}
						}
					}
					else
					{
						IEnumerator<TSource> enumerator2 = internalCurrent.SavedStateFromPreviousReplica as IEnumerator<TSource>;
						if (enumerator2 == null)
						{
							enumerator2 = this.partitionerSource.GetEnumerator();
							if (enumerator2 == null)
							{
								throw new InvalidOperationException(Environment.GetResourceString("The Partitioner source returned a null enumerator."));
							}
						}
						disposable = enumerator2;
						if (parallelLoopState != null)
						{
							parallelLoopState.CurrentIteration = 0L;
						}
						while (enumerator2.MoveNext())
						{
							TSource tsource = enumerator2.Current;
							if (this.simpleBody != null)
							{
								this.simpleBody(tsource);
							}
							else if (this.bodyWithState != null)
							{
								this.bodyWithState(tsource, parallelLoopState);
							}
							else if (this.bodyWithStateAndLocal != null)
							{
								tlocal = this.bodyWithStateAndLocal(tsource, parallelLoopState, tlocal);
							}
							if (this.sharedPStateFlags.LoopStateFlags != ParallelLoopStateFlags.PLS_NONE)
							{
								break;
							}
							if (!flag2 && loopTimer.LimitExceeded())
							{
								internalCurrent.SavedStateForNextReplica = enumerator2;
								disposable = null;
								break;
							}
						}
					}
				}
				catch
				{
					this.sharedPStateFlags.SetExceptional();
					throw;
				}
				finally
				{
					if (this.localFinally != null && flag)
					{
						this.localFinally(tlocal);
					}
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
			}

			// Token: 0x04001D5D RID: 7517
			public ParallelLoopStateFlags64 sharedPStateFlags;

			// Token: 0x04001D5E RID: 7518
			public OperationCanceledException oce;

			// Token: 0x04001D5F RID: 7519
			public ParallelOptions parallelOptions;

			// Token: 0x04001D60 RID: 7520
			public Action<TSource, ParallelLoopState> bodyWithState;

			// Token: 0x04001D61 RID: 7521
			public Action<TSource, ParallelLoopState, long> bodyWithStateAndIndex;

			// Token: 0x04001D62 RID: 7522
			public Func<TSource, ParallelLoopState, TLocal, TLocal> bodyWithStateAndLocal;

			// Token: 0x04001D63 RID: 7523
			public Func<TSource, ParallelLoopState, long, TLocal, TLocal> bodyWithEverything;

			// Token: 0x04001D64 RID: 7524
			public Func<TLocal> localInit;

			// Token: 0x04001D65 RID: 7525
			public ParallelForReplicatingTask rootTask;

			// Token: 0x04001D66 RID: 7526
			public OrderablePartitioner<TSource> orderedSource;

			// Token: 0x04001D67 RID: 7527
			public IEnumerable<KeyValuePair<long, TSource>> orderablePartitionerSource;

			// Token: 0x04001D68 RID: 7528
			public Action<TSource> simpleBody;

			// Token: 0x04001D69 RID: 7529
			public IEnumerable<TSource> partitionerSource;

			// Token: 0x04001D6A RID: 7530
			public Action<TLocal> localFinally;
		}
	}
}
