﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x020004FD RID: 1277
	internal class ParallelLoopStateFlags32 : ParallelLoopStateFlags
	{
		// Token: 0x17000924 RID: 2340
		// (get) Token: 0x06003808 RID: 14344 RVA: 0x000C3A2A File Offset: 0x000C1C2A
		internal int LowestBreakIteration
		{
			get
			{
				return this.m_lowestBreakIteration;
			}
		}

		// Token: 0x17000925 RID: 2341
		// (get) Token: 0x06003809 RID: 14345 RVA: 0x000C3A34 File Offset: 0x000C1C34
		internal long? NullableLowestBreakIteration
		{
			get
			{
				if (this.m_lowestBreakIteration == 2147483647)
				{
					return null;
				}
				long value = (long)this.m_lowestBreakIteration;
				if (IntPtr.Size >= 8)
				{
					return new long?(value);
				}
				return new long?(Interlocked.Read(ref value));
			}
		}

		// Token: 0x0600380A RID: 14346 RVA: 0x000C3A80 File Offset: 0x000C1C80
		internal bool ShouldExitLoop(int CallerIteration)
		{
			int loopStateFlags = base.LoopStateFlags;
			return loopStateFlags != ParallelLoopStateFlags.PLS_NONE && ((loopStateFlags & (ParallelLoopStateFlags.PLS_EXCEPTIONAL | ParallelLoopStateFlags.PLS_STOPPED | ParallelLoopStateFlags.PLS_CANCELED)) != 0 || ((loopStateFlags & ParallelLoopStateFlags.PLS_BROKEN) != 0 && CallerIteration > this.LowestBreakIteration));
		}

		// Token: 0x0600380B RID: 14347 RVA: 0x000C3ACC File Offset: 0x000C1CCC
		internal bool ShouldExitLoop()
		{
			int loopStateFlags = base.LoopStateFlags;
			return loopStateFlags != ParallelLoopStateFlags.PLS_NONE && (loopStateFlags & (ParallelLoopStateFlags.PLS_EXCEPTIONAL | ParallelLoopStateFlags.PLS_CANCELED)) != 0;
		}

		// Token: 0x0600380C RID: 14348 RVA: 0x000C3AFA File Offset: 0x000C1CFA
		public ParallelLoopStateFlags32()
		{
		}

		// Token: 0x04001D76 RID: 7542
		internal volatile int m_lowestBreakIteration = int.MaxValue;
	}
}
