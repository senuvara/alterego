﻿using System;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x02000529 RID: 1321
	internal abstract class TaskContinuation
	{
		// Token: 0x0600396B RID: 14699
		internal abstract void Run(Task completedTask, bool bCanInlineContinuationTask);

		// Token: 0x0600396C RID: 14700 RVA: 0x000C8BA0 File Offset: 0x000C6DA0
		[SecuritySafeCritical]
		protected static void InlineIfPossibleOrElseQueue(Task task, bool needsProtection)
		{
			if (needsProtection)
			{
				if (!task.MarkStarted())
				{
					return;
				}
			}
			else
			{
				task.m_stateFlags |= 65536;
			}
			try
			{
				if (!task.m_taskScheduler.TryRunInline(task, false))
				{
					task.m_taskScheduler.InternalQueueTask(task);
				}
			}
			catch (Exception ex)
			{
				if (!(ex is ThreadAbortException) || (task.m_stateFlags & 134217728) == 0)
				{
					TaskSchedulerException exceptionObject = new TaskSchedulerException(ex);
					task.AddException(exceptionObject);
					task.Finish(false);
				}
			}
		}

		// Token: 0x0600396D RID: 14701
		internal abstract Delegate[] GetDelegateContinuationsForDebugger();

		// Token: 0x0600396E RID: 14702 RVA: 0x00002050 File Offset: 0x00000250
		protected TaskContinuation()
		{
		}
	}
}
