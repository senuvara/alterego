﻿using System;

namespace System.Threading.Tasks
{
	/// <summary>Stores options that configure the operation of methods on the <see cref="T:System.Threading.Tasks.Parallel" /> class.</summary>
	// Token: 0x020004ED RID: 1261
	public class ParallelOptions
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.ParallelOptions" /> class.</summary>
		// Token: 0x06003795 RID: 14229 RVA: 0x000C14CA File Offset: 0x000BF6CA
		public ParallelOptions()
		{
			this.m_scheduler = TaskScheduler.Default;
			this.m_maxDegreeOfParallelism = -1;
			this.m_cancellationToken = CancellationToken.None;
		}

		/// <summary>Gets or sets the <see cref="T:System.Threading.Tasks.TaskScheduler" /> associated with this <see cref="T:System.Threading.Tasks.ParallelOptions" /> instance. Setting this property to null indicates that the current scheduler should be used.</summary>
		/// <returns>The task scheduler that is associated with this instance.</returns>
		// Token: 0x17000912 RID: 2322
		// (get) Token: 0x06003796 RID: 14230 RVA: 0x000C14EF File Offset: 0x000BF6EF
		// (set) Token: 0x06003797 RID: 14231 RVA: 0x000C14F7 File Offset: 0x000BF6F7
		public TaskScheduler TaskScheduler
		{
			get
			{
				return this.m_scheduler;
			}
			set
			{
				this.m_scheduler = value;
			}
		}

		// Token: 0x17000913 RID: 2323
		// (get) Token: 0x06003798 RID: 14232 RVA: 0x000C1500 File Offset: 0x000BF700
		internal TaskScheduler EffectiveTaskScheduler
		{
			get
			{
				if (this.m_scheduler == null)
				{
					return TaskScheduler.Current;
				}
				return this.m_scheduler;
			}
		}

		/// <summary>Gets or sets the maximum number of concurrent tasks enabled by this <see cref="T:System.Threading.Tasks.ParallelOptions" /> instance.</summary>
		/// <returns>An integer that represents the maximum degree of parallelism.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is being set to zero or to a value that is less than -1.</exception>
		// Token: 0x17000914 RID: 2324
		// (get) Token: 0x06003799 RID: 14233 RVA: 0x000C1516 File Offset: 0x000BF716
		// (set) Token: 0x0600379A RID: 14234 RVA: 0x000C151E File Offset: 0x000BF71E
		public int MaxDegreeOfParallelism
		{
			get
			{
				return this.m_maxDegreeOfParallelism;
			}
			set
			{
				if (value == 0 || value < -1)
				{
					throw new ArgumentOutOfRangeException("MaxDegreeOfParallelism");
				}
				this.m_maxDegreeOfParallelism = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Threading.CancellationToken" /> associated with this <see cref="T:System.Threading.Tasks.ParallelOptions" /> instance.</summary>
		/// <returns>The token that is associated with this instance.</returns>
		// Token: 0x17000915 RID: 2325
		// (get) Token: 0x0600379B RID: 14235 RVA: 0x000C1539 File Offset: 0x000BF739
		// (set) Token: 0x0600379C RID: 14236 RVA: 0x000C1541 File Offset: 0x000BF741
		public CancellationToken CancellationToken
		{
			get
			{
				return this.m_cancellationToken;
			}
			set
			{
				this.m_cancellationToken = value;
			}
		}

		// Token: 0x17000916 RID: 2326
		// (get) Token: 0x0600379D RID: 14237 RVA: 0x000C154C File Offset: 0x000BF74C
		internal int EffectiveMaxConcurrencyLevel
		{
			get
			{
				int num = this.MaxDegreeOfParallelism;
				int maximumConcurrencyLevel = this.EffectiveTaskScheduler.MaximumConcurrencyLevel;
				if (maximumConcurrencyLevel > 0 && maximumConcurrencyLevel != 2147483647)
				{
					num = ((num == -1) ? maximumConcurrencyLevel : Math.Min(maximumConcurrencyLevel, num));
				}
				return num;
			}
		}

		// Token: 0x04001D2B RID: 7467
		private TaskScheduler m_scheduler;

		// Token: 0x04001D2C RID: 7468
		private int m_maxDegreeOfParallelism;

		// Token: 0x04001D2D RID: 7469
		private CancellationToken m_cancellationToken;
	}
}
