﻿using System;
using System.Runtime.CompilerServices;

namespace System.Threading.Tasks
{
	// Token: 0x020004EB RID: 1259
	internal static class GenericDelegateCache<TAntecedentResult, TResult>
	{
		// Token: 0x0600378E RID: 14222 RVA: 0x000C13BC File Offset: 0x000BF5BC
		// Note: this type is marked as 'beforefieldinit'.
		static GenericDelegateCache()
		{
		}

		// Token: 0x04001D26 RID: 7462
		internal static Func<Task<Task>, object, TResult> CWAnyFuncDelegate = delegate(Task<Task> wrappedWinner, object state)
		{
			Func<Task<TAntecedentResult>, TResult> func = (Func<Task<TAntecedentResult>, TResult>)state;
			Task<TAntecedentResult> arg = (Task<TAntecedentResult>)wrappedWinner.Result;
			return func(arg);
		};

		// Token: 0x04001D27 RID: 7463
		internal static Func<Task<Task>, object, TResult> CWAnyActionDelegate = delegate(Task<Task> wrappedWinner, object state)
		{
			Action<Task<TAntecedentResult>> action = (Action<Task<TAntecedentResult>>)state;
			Task<TAntecedentResult> obj = (Task<TAntecedentResult>)wrappedWinner.Result;
			action(obj);
			return default(TResult);
		};

		// Token: 0x04001D28 RID: 7464
		internal static Func<Task<Task<TAntecedentResult>[]>, object, TResult> CWAllFuncDelegate = delegate(Task<Task<TAntecedentResult>[]> wrappedAntecedents, object state)
		{
			wrappedAntecedents.NotifyDebuggerOfWaitCompletionIfNecessary();
			return ((Func<Task<TAntecedentResult>[], TResult>)state)(wrappedAntecedents.Result);
		};

		// Token: 0x04001D29 RID: 7465
		internal static Func<Task<Task<TAntecedentResult>[]>, object, TResult> CWAllActionDelegate = delegate(Task<Task<TAntecedentResult>[]> wrappedAntecedents, object state)
		{
			wrappedAntecedents.NotifyDebuggerOfWaitCompletionIfNecessary();
			((Action<Task<TAntecedentResult>[]>)state)(wrappedAntecedents.Result);
			return default(TResult);
		};

		// Token: 0x020004EC RID: 1260
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600378F RID: 14223 RVA: 0x000C141D File Offset: 0x000BF61D
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06003790 RID: 14224 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x06003791 RID: 14225 RVA: 0x000C142C File Offset: 0x000BF62C
			internal TResult <.cctor>b__4_0(Task<Task> wrappedWinner, object state)
			{
				Func<Task<TAntecedentResult>, TResult> func = (Func<Task<TAntecedentResult>, TResult>)state;
				Task<TAntecedentResult> arg = (Task<TAntecedentResult>)wrappedWinner.Result;
				return func(arg);
			}

			// Token: 0x06003792 RID: 14226 RVA: 0x000C1454 File Offset: 0x000BF654
			internal TResult <.cctor>b__4_1(Task<Task> wrappedWinner, object state)
			{
				Action<Task<TAntecedentResult>> action = (Action<Task<TAntecedentResult>>)state;
				Task<TAntecedentResult> obj = (Task<TAntecedentResult>)wrappedWinner.Result;
				action(obj);
				return default(TResult);
			}

			// Token: 0x06003793 RID: 14227 RVA: 0x000C1482 File Offset: 0x000BF682
			internal TResult <.cctor>b__4_2(Task<Task<TAntecedentResult>[]> wrappedAntecedents, object state)
			{
				wrappedAntecedents.NotifyDebuggerOfWaitCompletionIfNecessary();
				return ((Func<Task<TAntecedentResult>[], TResult>)state)(wrappedAntecedents.Result);
			}

			// Token: 0x06003794 RID: 14228 RVA: 0x000C149C File Offset: 0x000BF69C
			internal TResult <.cctor>b__4_3(Task<Task<TAntecedentResult>[]> wrappedAntecedents, object state)
			{
				wrappedAntecedents.NotifyDebuggerOfWaitCompletionIfNecessary();
				((Action<Task<TAntecedentResult>[]>)state)(wrappedAntecedents.Result);
				return default(TResult);
			}

			// Token: 0x04001D2A RID: 7466
			public static readonly GenericDelegateCache<TAntecedentResult, TResult>.<>c <>9 = new GenericDelegateCache<TAntecedentResult, TResult>.<>c();
		}
	}
}
