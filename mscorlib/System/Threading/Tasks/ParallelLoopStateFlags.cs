﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x020004FC RID: 1276
	internal class ParallelLoopStateFlags
	{
		// Token: 0x17000923 RID: 2339
		// (get) Token: 0x06003800 RID: 14336 RVA: 0x000C3944 File Offset: 0x000C1B44
		internal int LoopStateFlags
		{
			get
			{
				return this.m_LoopStateFlags;
			}
		}

		// Token: 0x06003801 RID: 14337 RVA: 0x000C3950 File Offset: 0x000C1B50
		internal bool AtomicLoopStateUpdate(int newState, int illegalStates)
		{
			int num = 0;
			return this.AtomicLoopStateUpdate(newState, illegalStates, ref num);
		}

		// Token: 0x06003802 RID: 14338 RVA: 0x000C396C File Offset: 0x000C1B6C
		internal bool AtomicLoopStateUpdate(int newState, int illegalStates, ref int oldState)
		{
			SpinWait spinWait = default(SpinWait);
			for (;;)
			{
				oldState = this.m_LoopStateFlags;
				if ((oldState & illegalStates) != 0)
				{
					break;
				}
				if (Interlocked.CompareExchange(ref this.m_LoopStateFlags, oldState | newState, oldState) == oldState)
				{
					return true;
				}
				spinWait.SpinOnce();
			}
			return false;
		}

		// Token: 0x06003803 RID: 14339 RVA: 0x000C39B2 File Offset: 0x000C1BB2
		internal void SetExceptional()
		{
			this.AtomicLoopStateUpdate(ParallelLoopStateFlags.PLS_EXCEPTIONAL, ParallelLoopStateFlags.PLS_NONE);
		}

		// Token: 0x06003804 RID: 14340 RVA: 0x000C39C5 File Offset: 0x000C1BC5
		internal void Stop()
		{
			if (!this.AtomicLoopStateUpdate(ParallelLoopStateFlags.PLS_STOPPED, ParallelLoopStateFlags.PLS_BROKEN))
			{
				throw new InvalidOperationException(Environment.GetResourceString("Stop was called after Break was called."));
			}
		}

		// Token: 0x06003805 RID: 14341 RVA: 0x000C39E9 File Offset: 0x000C1BE9
		internal bool Cancel()
		{
			return this.AtomicLoopStateUpdate(ParallelLoopStateFlags.PLS_CANCELED, ParallelLoopStateFlags.PLS_NONE);
		}

		// Token: 0x06003806 RID: 14342 RVA: 0x000C39FB File Offset: 0x000C1BFB
		public ParallelLoopStateFlags()
		{
		}

		// Token: 0x06003807 RID: 14343 RVA: 0x000C3A10 File Offset: 0x000C1C10
		// Note: this type is marked as 'beforefieldinit'.
		static ParallelLoopStateFlags()
		{
		}

		// Token: 0x04001D70 RID: 7536
		internal static int PLS_NONE;

		// Token: 0x04001D71 RID: 7537
		internal static int PLS_EXCEPTIONAL = 1;

		// Token: 0x04001D72 RID: 7538
		internal static int PLS_BROKEN = 2;

		// Token: 0x04001D73 RID: 7539
		internal static int PLS_STOPPED = 4;

		// Token: 0x04001D74 RID: 7540
		internal static int PLS_CANCELED = 8;

		// Token: 0x04001D75 RID: 7541
		private volatile int m_LoopStateFlags = ParallelLoopStateFlags.PLS_NONE;
	}
}
