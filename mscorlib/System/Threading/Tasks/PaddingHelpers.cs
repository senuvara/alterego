﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x0200050A RID: 1290
	internal static class PaddingHelpers
	{
		// Token: 0x04001D9C RID: 7580
		internal const int CACHE_LINE_SIZE = 128;
	}
}
