﻿using System;

namespace System.Threading.Tasks
{
	// Token: 0x0200051C RID: 1308
	[Flags]
	[Serializable]
	internal enum InternalTaskOptions
	{
		// Token: 0x04001DF7 RID: 7671
		None = 0,
		// Token: 0x04001DF8 RID: 7672
		InternalOptionsMask = 65280,
		// Token: 0x04001DF9 RID: 7673
		ChildReplica = 256,
		// Token: 0x04001DFA RID: 7674
		ContinuationTask = 512,
		// Token: 0x04001DFB RID: 7675
		PromiseTask = 1024,
		// Token: 0x04001DFC RID: 7676
		SelfReplicating = 2048,
		// Token: 0x04001DFD RID: 7677
		LazyCancellation = 4096,
		// Token: 0x04001DFE RID: 7678
		QueuedByRuntime = 8192,
		// Token: 0x04001DFF RID: 7679
		DoNotDispose = 16384
	}
}
