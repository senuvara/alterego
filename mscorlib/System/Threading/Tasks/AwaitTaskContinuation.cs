﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Security;

namespace System.Threading.Tasks
{
	// Token: 0x0200052F RID: 1327
	internal class AwaitTaskContinuation : TaskContinuation, IThreadPoolWorkItem
	{
		// Token: 0x0600397F RID: 14719 RVA: 0x000C8F50 File Offset: 0x000C7150
		[SecurityCritical]
		internal AwaitTaskContinuation(Action action, bool flowExecutionContext, ref StackCrawlMark stackMark)
		{
			this.m_action = action;
			if (flowExecutionContext)
			{
				this.m_capturedContext = ExecutionContext.Capture(ref stackMark, ExecutionContext.CaptureOptions.IgnoreSyncCtx | ExecutionContext.CaptureOptions.OptimizeDefaultCase);
			}
		}

		// Token: 0x06003980 RID: 14720 RVA: 0x000C8F6F File Offset: 0x000C716F
		[SecurityCritical]
		internal AwaitTaskContinuation(Action action, bool flowExecutionContext)
		{
			this.m_action = action;
			if (flowExecutionContext)
			{
				this.m_capturedContext = ExecutionContext.FastCapture();
			}
		}

		// Token: 0x06003981 RID: 14721 RVA: 0x000C8F8C File Offset: 0x000C718C
		protected Task CreateTask(Action<object> action, object state, TaskScheduler scheduler)
		{
			return new Task(action, state, null, default(CancellationToken), TaskCreationOptions.None, InternalTaskOptions.QueuedByRuntime, scheduler)
			{
				CapturedContext = this.m_capturedContext
			};
		}

		// Token: 0x06003982 RID: 14722 RVA: 0x000C8FBD File Offset: 0x000C71BD
		[SecuritySafeCritical]
		internal override void Run(Task task, bool canInlineContinuationTask)
		{
			if (canInlineContinuationTask && AwaitTaskContinuation.IsValidLocationForInlining)
			{
				this.RunCallback(AwaitTaskContinuation.GetInvokeActionCallback(), this.m_action, ref Task.t_currentTask);
				return;
			}
			ThreadPool.UnsafeQueueCustomWorkItem(this, false);
		}

		// Token: 0x17000965 RID: 2405
		// (get) Token: 0x06003983 RID: 14723 RVA: 0x000C8FE8 File Offset: 0x000C71E8
		internal static bool IsValidLocationForInlining
		{
			get
			{
				SynchronizationContext currentNoFlow = SynchronizationContext.CurrentNoFlow;
				if (currentNoFlow != null && currentNoFlow.GetType() != typeof(SynchronizationContext))
				{
					return false;
				}
				TaskScheduler internalCurrent = TaskScheduler.InternalCurrent;
				return internalCurrent == null || internalCurrent == TaskScheduler.Default;
			}
		}

		// Token: 0x06003984 RID: 14724 RVA: 0x000C902C File Offset: 0x000C722C
		[SecurityCritical]
		private void ExecuteWorkItemHelper()
		{
			if (this.m_capturedContext == null)
			{
				this.m_action();
				return;
			}
			try
			{
				ExecutionContext.Run(this.m_capturedContext, AwaitTaskContinuation.GetInvokeActionCallback(), this.m_action, true);
			}
			finally
			{
				this.m_capturedContext.Dispose();
			}
		}

		// Token: 0x06003985 RID: 14725 RVA: 0x000C9084 File Offset: 0x000C7284
		[SecurityCritical]
		void IThreadPoolWorkItem.ExecuteWorkItem()
		{
			if (this.m_capturedContext == null)
			{
				this.m_action();
				return;
			}
			this.ExecuteWorkItemHelper();
		}

		// Token: 0x06003986 RID: 14726 RVA: 0x000020D3 File Offset: 0x000002D3
		[SecurityCritical]
		void IThreadPoolWorkItem.MarkAborted(ThreadAbortException tae)
		{
		}

		// Token: 0x06003987 RID: 14727 RVA: 0x000C90A0 File Offset: 0x000C72A0
		[SecurityCritical]
		private static void InvokeAction(object state)
		{
			((Action)state)();
		}

		// Token: 0x06003988 RID: 14728 RVA: 0x000C90B0 File Offset: 0x000C72B0
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		protected static ContextCallback GetInvokeActionCallback()
		{
			ContextCallback contextCallback = AwaitTaskContinuation.s_invokeActionCallback;
			if (contextCallback == null)
			{
				contextCallback = (AwaitTaskContinuation.s_invokeActionCallback = new ContextCallback(AwaitTaskContinuation.InvokeAction));
			}
			return contextCallback;
		}

		// Token: 0x06003989 RID: 14729 RVA: 0x000C90DC File Offset: 0x000C72DC
		[SecurityCritical]
		protected void RunCallback(ContextCallback callback, object state, ref Task currentTask)
		{
			Task task = currentTask;
			try
			{
				if (task != null)
				{
					currentTask = null;
				}
				if (this.m_capturedContext == null)
				{
					callback(state);
				}
				else
				{
					ExecutionContext.Run(this.m_capturedContext, callback, state, true);
				}
			}
			catch (Exception exc)
			{
				AwaitTaskContinuation.ThrowAsyncIfNecessary(exc);
			}
			finally
			{
				if (task != null)
				{
					currentTask = task;
				}
				if (this.m_capturedContext != null)
				{
					this.m_capturedContext.Dispose();
				}
			}
		}

		// Token: 0x0600398A RID: 14730 RVA: 0x000C9150 File Offset: 0x000C7350
		[SecurityCritical]
		internal static void RunOrScheduleAction(Action action, bool allowInlining, ref Task currentTask)
		{
			if (!allowInlining || !AwaitTaskContinuation.IsValidLocationForInlining)
			{
				AwaitTaskContinuation.UnsafeScheduleAction(action, currentTask);
				return;
			}
			Task task = currentTask;
			try
			{
				if (task != null)
				{
					currentTask = null;
				}
				action();
			}
			catch (Exception exc)
			{
				AwaitTaskContinuation.ThrowAsyncIfNecessary(exc);
			}
			finally
			{
				if (task != null)
				{
					currentTask = task;
				}
			}
		}

		// Token: 0x0600398B RID: 14731 RVA: 0x000C91AC File Offset: 0x000C73AC
		[SecurityCritical]
		internal static void UnsafeScheduleAction(Action action, Task task)
		{
			ThreadPool.UnsafeQueueCustomWorkItem(new AwaitTaskContinuation(action, false), false);
		}

		// Token: 0x0600398C RID: 14732 RVA: 0x000C91BC File Offset: 0x000C73BC
		protected static void ThrowAsyncIfNecessary(Exception exc)
		{
			if (!(exc is ThreadAbortException) && !(exc is AppDomainUnloadedException))
			{
				ExceptionDispatchInfo state = ExceptionDispatchInfo.Capture(exc);
				ThreadPool.QueueUserWorkItem(delegate(object s)
				{
					((ExceptionDispatchInfo)s).Throw();
				}, state);
			}
		}

		// Token: 0x0600398D RID: 14733 RVA: 0x000C9206 File Offset: 0x000C7406
		internal override Delegate[] GetDelegateContinuationsForDebugger()
		{
			return new Delegate[]
			{
				AsyncMethodBuilderCore.TryGetStateMachineForDebugger(this.m_action)
			};
		}

		// Token: 0x04001E29 RID: 7721
		private readonly ExecutionContext m_capturedContext;

		// Token: 0x04001E2A RID: 7722
		protected readonly Action m_action;

		// Token: 0x04001E2B RID: 7723
		[SecurityCritical]
		private static ContextCallback s_invokeActionCallback;

		// Token: 0x02000530 RID: 1328
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x0600398E RID: 14734 RVA: 0x000C921C File Offset: 0x000C741C
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600398F RID: 14735 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x06003990 RID: 14736 RVA: 0x000C9228 File Offset: 0x000C7428
			internal void <ThrowAsyncIfNecessary>b__17_0(object s)
			{
				((ExceptionDispatchInfo)s).Throw();
			}

			// Token: 0x04001E2C RID: 7724
			public static readonly AwaitTaskContinuation.<>c <>9 = new AwaitTaskContinuation.<>c();

			// Token: 0x04001E2D RID: 7725
			public static WaitCallback <>9__17_0;
		}
	}
}
