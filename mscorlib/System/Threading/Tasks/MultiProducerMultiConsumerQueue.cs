﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;

namespace System.Threading.Tasks
{
	// Token: 0x02000504 RID: 1284
	[DebuggerDisplay("Count = {Count}")]
	internal sealed class MultiProducerMultiConsumerQueue<T> : ConcurrentQueue<T>, IProducerConsumerQueue<T>, IEnumerable<!0>, IEnumerable
	{
		// Token: 0x0600381E RID: 14366 RVA: 0x000C3F40 File Offset: 0x000C2140
		void IProducerConsumerQueue<!0>.Enqueue(T item)
		{
			base.Enqueue(item);
		}

		// Token: 0x0600381F RID: 14367 RVA: 0x000C3F49 File Offset: 0x000C2149
		bool IProducerConsumerQueue<!0>.TryDequeue(out T result)
		{
			return base.TryDequeue(out result);
		}

		// Token: 0x1700092C RID: 2348
		// (get) Token: 0x06003820 RID: 14368 RVA: 0x000C3F52 File Offset: 0x000C2152
		bool IProducerConsumerQueue<!0>.IsEmpty
		{
			get
			{
				return base.IsEmpty;
			}
		}

		// Token: 0x1700092D RID: 2349
		// (get) Token: 0x06003821 RID: 14369 RVA: 0x000C3F5A File Offset: 0x000C215A
		int IProducerConsumerQueue<!0>.Count
		{
			get
			{
				return base.Count;
			}
		}

		// Token: 0x06003822 RID: 14370 RVA: 0x000C3F5A File Offset: 0x000C215A
		int IProducerConsumerQueue<!0>.GetCountSafe(object syncObj)
		{
			return base.Count;
		}

		// Token: 0x06003823 RID: 14371 RVA: 0x000C3F62 File Offset: 0x000C2162
		public MultiProducerMultiConsumerQueue()
		{
		}
	}
}
