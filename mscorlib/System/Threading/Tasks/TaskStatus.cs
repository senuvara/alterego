﻿using System;

namespace System.Threading.Tasks
{
	/// <summary>Represents the current stage in the lifecycle of a <see cref="T:System.Threading.Tasks.Task" />.</summary>
	// Token: 0x0200050D RID: 1293
	public enum TaskStatus
	{
		/// <summary>The task has been initialized but has not yet been scheduled.</summary>
		// Token: 0x04001D9F RID: 7583
		Created,
		/// <summary>The task is waiting to be activated and scheduled internally by the .NET Framework infrastructure.</summary>
		// Token: 0x04001DA0 RID: 7584
		WaitingForActivation,
		/// <summary>The task has been scheduled for execution but has not yet begun executing.</summary>
		// Token: 0x04001DA1 RID: 7585
		WaitingToRun,
		/// <summary>The task is running but has not yet completed.</summary>
		// Token: 0x04001DA2 RID: 7586
		Running,
		/// <summary>The task has finished executing and is implicitly waiting for attached child tasks to complete.</summary>
		// Token: 0x04001DA3 RID: 7587
		WaitingForChildrenToComplete,
		/// <summary>The task completed execution successfully.</summary>
		// Token: 0x04001DA4 RID: 7588
		RanToCompletion,
		/// <summary>The task acknowledged cancellation by throwing an OperationCanceledException with its own CancellationToken while the token was in signaled state, or the task's CancellationToken was already signaled before the task started executing. For more information, see Task Cancellation.</summary>
		// Token: 0x04001DA5 RID: 7589
		Canceled,
		/// <summary>The task completed due to an unhandled exception.</summary>
		// Token: 0x04001DA6 RID: 7590
		Faulted
	}
}
