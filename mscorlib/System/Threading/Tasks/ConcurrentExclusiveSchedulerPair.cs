﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Permissions;

namespace System.Threading.Tasks
{
	/// <summary>Provides task schedulers that coordinate to execute tasks while ensuring that concurrent tasks may run concurrently and exclusive tasks never do.</summary>
	// Token: 0x020004D5 RID: 1237
	[DebuggerTypeProxy(typeof(ConcurrentExclusiveSchedulerPair.DebugView))]
	[DebuggerDisplay("Concurrent={ConcurrentTaskCountForDebugger}, Exclusive={ExclusiveTaskCountForDebugger}, Mode={ModeForDebugger}")]
	[HostProtection(SecurityAction.LinkDemand, Synchronization = true, ExternalThreading = true)]
	public class ConcurrentExclusiveSchedulerPair
	{
		// Token: 0x170008ED RID: 2285
		// (get) Token: 0x060036BF RID: 14015 RVA: 0x000BEA3C File Offset: 0x000BCC3C
		private static int DefaultMaxConcurrencyLevel
		{
			get
			{
				return Environment.ProcessorCount;
			}
		}

		// Token: 0x170008EE RID: 2286
		// (get) Token: 0x060036C0 RID: 14016 RVA: 0x000BEA43 File Offset: 0x000BCC43
		private object ValueLock
		{
			get
			{
				return this.m_threadProcessingMapping;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.ConcurrentExclusiveSchedulerPair" /> class.</summary>
		// Token: 0x060036C1 RID: 14017 RVA: 0x000BEA4B File Offset: 0x000BCC4B
		public ConcurrentExclusiveSchedulerPair() : this(TaskScheduler.Default, ConcurrentExclusiveSchedulerPair.DefaultMaxConcurrencyLevel, -1)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.ConcurrentExclusiveSchedulerPair" /> class that targets the specified scheduler.</summary>
		/// <param name="taskScheduler">The target scheduler on which this pair should execute.</param>
		// Token: 0x060036C2 RID: 14018 RVA: 0x000BEA5E File Offset: 0x000BCC5E
		public ConcurrentExclusiveSchedulerPair(TaskScheduler taskScheduler) : this(taskScheduler, ConcurrentExclusiveSchedulerPair.DefaultMaxConcurrencyLevel, -1)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.ConcurrentExclusiveSchedulerPair" /> class that targets the specified scheduler with a maximum concurrency level.</summary>
		/// <param name="taskScheduler">The target scheduler on which this pair should execute.</param>
		/// <param name="maxConcurrencyLevel">The maximum number of tasks to run concurrently.</param>
		// Token: 0x060036C3 RID: 14019 RVA: 0x000BEA6D File Offset: 0x000BCC6D
		public ConcurrentExclusiveSchedulerPair(TaskScheduler taskScheduler, int maxConcurrencyLevel) : this(taskScheduler, maxConcurrencyLevel, -1)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Threading.Tasks.ConcurrentExclusiveSchedulerPair" /> class that targets the specified scheduler with a maximum concurrency level and a maximum number of scheduled tasks that may be processed as a unit.</summary>
		/// <param name="taskScheduler">The target scheduler on which this pair should execute.</param>
		/// <param name="maxConcurrencyLevel">The maximum number of tasks to run concurrently.</param>
		/// <param name="maxItemsPerTask">The maximum number of tasks to process for each underlying scheduled task used by the pair.</param>
		// Token: 0x060036C4 RID: 14020 RVA: 0x000BEA78 File Offset: 0x000BCC78
		public ConcurrentExclusiveSchedulerPair(TaskScheduler taskScheduler, int maxConcurrencyLevel, int maxItemsPerTask)
		{
			if (taskScheduler == null)
			{
				throw new ArgumentNullException("taskScheduler");
			}
			if (maxConcurrencyLevel == 0 || maxConcurrencyLevel < -1)
			{
				throw new ArgumentOutOfRangeException("maxConcurrencyLevel");
			}
			if (maxItemsPerTask == 0 || maxItemsPerTask < -1)
			{
				throw new ArgumentOutOfRangeException("maxItemsPerTask");
			}
			this.m_underlyingTaskScheduler = taskScheduler;
			this.m_maxConcurrencyLevel = maxConcurrencyLevel;
			this.m_maxItemsPerTask = maxItemsPerTask;
			int maximumConcurrencyLevel = taskScheduler.MaximumConcurrencyLevel;
			if (maximumConcurrencyLevel > 0 && maximumConcurrencyLevel < this.m_maxConcurrencyLevel)
			{
				this.m_maxConcurrencyLevel = maximumConcurrencyLevel;
			}
			if (this.m_maxConcurrencyLevel == -1)
			{
				this.m_maxConcurrencyLevel = int.MaxValue;
			}
			if (this.m_maxItemsPerTask == -1)
			{
				this.m_maxItemsPerTask = int.MaxValue;
			}
			this.m_exclusiveTaskScheduler = new ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler(this, 1, ConcurrentExclusiveSchedulerPair.ProcessingMode.ProcessingExclusiveTask);
			this.m_concurrentTaskScheduler = new ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler(this, this.m_maxConcurrencyLevel, ConcurrentExclusiveSchedulerPair.ProcessingMode.ProcessingConcurrentTasks);
		}

		/// <summary>Informs the scheduler pair that it should not accept any more tasks.</summary>
		// Token: 0x060036C5 RID: 14021 RVA: 0x000BEB44 File Offset: 0x000BCD44
		public void Complete()
		{
			object valueLock = this.ValueLock;
			lock (valueLock)
			{
				if (!this.CompletionRequested)
				{
					this.RequestCompletion();
					this.CleanupStateIfCompletingAndQuiesced();
				}
			}
		}

		/// <summary>Gets a <see cref="T:System.Threading.Tasks.Task" /> that will complete when the scheduler has completed processing.</summary>
		/// <returns>The asynchronous operation that will complete when the scheduler finishes processing.</returns>
		// Token: 0x170008EF RID: 2287
		// (get) Token: 0x060036C6 RID: 14022 RVA: 0x000BEB94 File Offset: 0x000BCD94
		public Task Completion
		{
			get
			{
				return this.EnsureCompletionStateInitialized().Task;
			}
		}

		// Token: 0x060036C7 RID: 14023 RVA: 0x000BEBA1 File Offset: 0x000BCDA1
		private ConcurrentExclusiveSchedulerPair.CompletionState EnsureCompletionStateInitialized()
		{
			return LazyInitializer.EnsureInitialized<ConcurrentExclusiveSchedulerPair.CompletionState>(ref this.m_completionState, () => new ConcurrentExclusiveSchedulerPair.CompletionState());
		}

		// Token: 0x170008F0 RID: 2288
		// (get) Token: 0x060036C8 RID: 14024 RVA: 0x000BEBCD File Offset: 0x000BCDCD
		private bool CompletionRequested
		{
			get
			{
				return this.m_completionState != null && Volatile.Read(ref this.m_completionState.m_completionRequested);
			}
		}

		// Token: 0x060036C9 RID: 14025 RVA: 0x000BEBE9 File Offset: 0x000BCDE9
		private void RequestCompletion()
		{
			this.EnsureCompletionStateInitialized().m_completionRequested = true;
		}

		// Token: 0x060036CA RID: 14026 RVA: 0x000BEBF7 File Offset: 0x000BCDF7
		private void CleanupStateIfCompletingAndQuiesced()
		{
			if (this.ReadyToComplete)
			{
				this.CompleteTaskAsync();
			}
		}

		// Token: 0x170008F1 RID: 2289
		// (get) Token: 0x060036CB RID: 14027 RVA: 0x000BEC08 File Offset: 0x000BCE08
		private bool ReadyToComplete
		{
			get
			{
				if (!this.CompletionRequested || this.m_processingCount != 0)
				{
					return false;
				}
				ConcurrentExclusiveSchedulerPair.CompletionState completionState = this.EnsureCompletionStateInitialized();
				return (completionState.m_exceptions != null && completionState.m_exceptions.Count > 0) || (this.m_concurrentTaskScheduler.m_tasks.IsEmpty && this.m_exclusiveTaskScheduler.m_tasks.IsEmpty);
			}
		}

		// Token: 0x060036CC RID: 14028 RVA: 0x000BEC6C File Offset: 0x000BCE6C
		private void CompleteTaskAsync()
		{
			ConcurrentExclusiveSchedulerPair.CompletionState completionState = this.EnsureCompletionStateInitialized();
			if (!completionState.m_completionQueued)
			{
				completionState.m_completionQueued = true;
				ThreadPool.QueueUserWorkItem(delegate(object state)
				{
					ConcurrentExclusiveSchedulerPair.CompletionState completionState2 = (ConcurrentExclusiveSchedulerPair.CompletionState)state;
					List<Exception> exceptions = completionState2.m_exceptions;
					if (exceptions == null || exceptions.Count <= 0)
					{
						completionState2.TrySetResult(default(VoidTaskResult));
						return;
					}
					completionState2.TrySetException(exceptions);
				}, completionState);
			}
		}

		// Token: 0x060036CD RID: 14029 RVA: 0x000BECB8 File Offset: 0x000BCEB8
		private void FaultWithTask(Task faultedTask)
		{
			ConcurrentExclusiveSchedulerPair.CompletionState completionState = this.EnsureCompletionStateInitialized();
			if (completionState.m_exceptions == null)
			{
				completionState.m_exceptions = new List<Exception>();
			}
			completionState.m_exceptions.AddRange(faultedTask.Exception.InnerExceptions);
			this.RequestCompletion();
		}

		/// <summary>Gets a <see cref="T:System.Threading.Tasks.TaskScheduler" /> that can be used to schedule tasks to this pair that may run concurrently with other tasks on this pair.</summary>
		/// <returns>An object that can be used to schedule tasks concurrently.</returns>
		// Token: 0x170008F2 RID: 2290
		// (get) Token: 0x060036CE RID: 14030 RVA: 0x000BECFB File Offset: 0x000BCEFB
		public TaskScheduler ConcurrentScheduler
		{
			get
			{
				return this.m_concurrentTaskScheduler;
			}
		}

		/// <summary>Gets a <see cref="T:System.Threading.Tasks.TaskScheduler" /> that can be used to schedule tasks to this pair that must run exclusively with regards to other tasks on this pair.</summary>
		/// <returns>An object that can be used to schedule tasks that do not run concurrently with other tasks.</returns>
		// Token: 0x170008F3 RID: 2291
		// (get) Token: 0x060036CF RID: 14031 RVA: 0x000BED03 File Offset: 0x000BCF03
		public TaskScheduler ExclusiveScheduler
		{
			get
			{
				return this.m_exclusiveTaskScheduler;
			}
		}

		// Token: 0x170008F4 RID: 2292
		// (get) Token: 0x060036D0 RID: 14032 RVA: 0x000BED0B File Offset: 0x000BCF0B
		private int ConcurrentTaskCountForDebugger
		{
			get
			{
				return this.m_concurrentTaskScheduler.m_tasks.Count;
			}
		}

		// Token: 0x170008F5 RID: 2293
		// (get) Token: 0x060036D1 RID: 14033 RVA: 0x000BED1D File Offset: 0x000BCF1D
		private int ExclusiveTaskCountForDebugger
		{
			get
			{
				return this.m_exclusiveTaskScheduler.m_tasks.Count;
			}
		}

		// Token: 0x060036D2 RID: 14034 RVA: 0x000BED30 File Offset: 0x000BCF30
		private void ProcessAsyncIfNecessary(bool fairly = false)
		{
			if (this.m_processingCount >= 0)
			{
				bool flag = !this.m_exclusiveTaskScheduler.m_tasks.IsEmpty;
				Task task = null;
				if (this.m_processingCount == 0 && flag)
				{
					this.m_processingCount = -1;
					try
					{
						task = new Task(delegate(object thisPair)
						{
							((ConcurrentExclusiveSchedulerPair)thisPair).ProcessExclusiveTasks();
						}, this, default(CancellationToken), ConcurrentExclusiveSchedulerPair.GetCreationOptionsForTask(fairly));
						task.Start(this.m_underlyingTaskScheduler);
						goto IL_149;
					}
					catch
					{
						this.m_processingCount = 0;
						this.FaultWithTask(task);
						goto IL_149;
					}
				}
				int count = this.m_concurrentTaskScheduler.m_tasks.Count;
				if (count > 0 && !flag && this.m_processingCount < this.m_maxConcurrencyLevel)
				{
					int num = 0;
					while (num < count && this.m_processingCount < this.m_maxConcurrencyLevel)
					{
						this.m_processingCount++;
						try
						{
							task = new Task(delegate(object thisPair)
							{
								((ConcurrentExclusiveSchedulerPair)thisPair).ProcessConcurrentTasks();
							}, this, default(CancellationToken), ConcurrentExclusiveSchedulerPair.GetCreationOptionsForTask(fairly));
							task.Start(this.m_underlyingTaskScheduler);
						}
						catch
						{
							this.m_processingCount--;
							this.FaultWithTask(task);
						}
						num++;
					}
				}
				IL_149:
				this.CleanupStateIfCompletingAndQuiesced();
			}
		}

		// Token: 0x060036D3 RID: 14035 RVA: 0x000BEEA8 File Offset: 0x000BD0A8
		private void ProcessExclusiveTasks()
		{
			try
			{
				this.m_threadProcessingMapping[Thread.CurrentThread.ManagedThreadId] = ConcurrentExclusiveSchedulerPair.ProcessingMode.ProcessingExclusiveTask;
				for (int i = 0; i < this.m_maxItemsPerTask; i++)
				{
					Task task;
					if (!this.m_exclusiveTaskScheduler.m_tasks.TryDequeue(out task))
					{
						break;
					}
					if (!task.IsFaulted)
					{
						this.m_exclusiveTaskScheduler.ExecuteTask(task);
					}
				}
			}
			finally
			{
				ConcurrentExclusiveSchedulerPair.ProcessingMode processingMode;
				this.m_threadProcessingMapping.TryRemove(Thread.CurrentThread.ManagedThreadId, out processingMode);
				object valueLock = this.ValueLock;
				lock (valueLock)
				{
					this.m_processingCount = 0;
					this.ProcessAsyncIfNecessary(true);
				}
			}
		}

		// Token: 0x060036D4 RID: 14036 RVA: 0x000BEF6C File Offset: 0x000BD16C
		private void ProcessConcurrentTasks()
		{
			try
			{
				this.m_threadProcessingMapping[Thread.CurrentThread.ManagedThreadId] = ConcurrentExclusiveSchedulerPair.ProcessingMode.ProcessingConcurrentTasks;
				for (int i = 0; i < this.m_maxItemsPerTask; i++)
				{
					Task task;
					if (!this.m_concurrentTaskScheduler.m_tasks.TryDequeue(out task))
					{
						break;
					}
					if (!task.IsFaulted)
					{
						this.m_concurrentTaskScheduler.ExecuteTask(task);
					}
					if (!this.m_exclusiveTaskScheduler.m_tasks.IsEmpty)
					{
						break;
					}
				}
			}
			finally
			{
				ConcurrentExclusiveSchedulerPair.ProcessingMode processingMode;
				this.m_threadProcessingMapping.TryRemove(Thread.CurrentThread.ManagedThreadId, out processingMode);
				object valueLock = this.ValueLock;
				lock (valueLock)
				{
					if (this.m_processingCount > 0)
					{
						this.m_processingCount--;
					}
					this.ProcessAsyncIfNecessary(true);
				}
			}
		}

		// Token: 0x170008F6 RID: 2294
		// (get) Token: 0x060036D5 RID: 14037 RVA: 0x000BF058 File Offset: 0x000BD258
		private ConcurrentExclusiveSchedulerPair.ProcessingMode ModeForDebugger
		{
			get
			{
				if (this.m_completionState != null && this.m_completionState.Task.IsCompleted)
				{
					return ConcurrentExclusiveSchedulerPair.ProcessingMode.Completed;
				}
				ConcurrentExclusiveSchedulerPair.ProcessingMode processingMode = ConcurrentExclusiveSchedulerPair.ProcessingMode.NotCurrentlyProcessing;
				if (this.m_processingCount == -1)
				{
					processingMode |= ConcurrentExclusiveSchedulerPair.ProcessingMode.ProcessingExclusiveTask;
				}
				if (this.m_processingCount >= 1)
				{
					processingMode |= ConcurrentExclusiveSchedulerPair.ProcessingMode.ProcessingConcurrentTasks;
				}
				if (this.CompletionRequested)
				{
					processingMode |= ConcurrentExclusiveSchedulerPair.ProcessingMode.Completing;
				}
				return processingMode;
			}
		}

		// Token: 0x060036D6 RID: 14038 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("DEBUG")]
		internal static void ContractAssertMonitorStatus(object syncObj, bool held)
		{
		}

		// Token: 0x060036D7 RID: 14039 RVA: 0x000BF0AC File Offset: 0x000BD2AC
		internal static TaskCreationOptions GetCreationOptionsForTask(bool isReplacementReplica = false)
		{
			TaskCreationOptions taskCreationOptions = TaskCreationOptions.DenyChildAttach;
			if (isReplacementReplica)
			{
				taskCreationOptions |= TaskCreationOptions.PreferFairness;
			}
			return taskCreationOptions;
		}

		// Token: 0x04001CDB RID: 7387
		private readonly ConcurrentDictionary<int, ConcurrentExclusiveSchedulerPair.ProcessingMode> m_threadProcessingMapping = new ConcurrentDictionary<int, ConcurrentExclusiveSchedulerPair.ProcessingMode>();

		// Token: 0x04001CDC RID: 7388
		private readonly ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler m_concurrentTaskScheduler;

		// Token: 0x04001CDD RID: 7389
		private readonly ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler m_exclusiveTaskScheduler;

		// Token: 0x04001CDE RID: 7390
		private readonly TaskScheduler m_underlyingTaskScheduler;

		// Token: 0x04001CDF RID: 7391
		private readonly int m_maxConcurrencyLevel;

		// Token: 0x04001CE0 RID: 7392
		private readonly int m_maxItemsPerTask;

		// Token: 0x04001CE1 RID: 7393
		private int m_processingCount;

		// Token: 0x04001CE2 RID: 7394
		private ConcurrentExclusiveSchedulerPair.CompletionState m_completionState;

		// Token: 0x04001CE3 RID: 7395
		private const int UNLIMITED_PROCESSING = -1;

		// Token: 0x04001CE4 RID: 7396
		private const int EXCLUSIVE_PROCESSING_SENTINEL = -1;

		// Token: 0x04001CE5 RID: 7397
		private const int DEFAULT_MAXITEMSPERTASK = -1;

		// Token: 0x020004D6 RID: 1238
		private sealed class CompletionState : TaskCompletionSource<VoidTaskResult>
		{
			// Token: 0x060036D8 RID: 14040 RVA: 0x000BF0C3 File Offset: 0x000BD2C3
			public CompletionState()
			{
			}

			// Token: 0x04001CE6 RID: 7398
			internal bool m_completionRequested;

			// Token: 0x04001CE7 RID: 7399
			internal bool m_completionQueued;

			// Token: 0x04001CE8 RID: 7400
			internal List<Exception> m_exceptions;
		}

		// Token: 0x020004D7 RID: 1239
		[DebuggerTypeProxy(typeof(ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler.DebugView))]
		[DebuggerDisplay("Count={CountForDebugger}, MaxConcurrencyLevel={m_maxConcurrencyLevel}, Id={Id}")]
		private sealed class ConcurrentExclusiveTaskScheduler : TaskScheduler
		{
			// Token: 0x060036D9 RID: 14041 RVA: 0x000BF0CC File Offset: 0x000BD2CC
			internal ConcurrentExclusiveTaskScheduler(ConcurrentExclusiveSchedulerPair pair, int maxConcurrencyLevel, ConcurrentExclusiveSchedulerPair.ProcessingMode processingMode)
			{
				this.m_pair = pair;
				this.m_maxConcurrencyLevel = maxConcurrencyLevel;
				this.m_processingMode = processingMode;
				IProducerConsumerQueue<Task> tasks;
				if (processingMode != ConcurrentExclusiveSchedulerPair.ProcessingMode.ProcessingExclusiveTask)
				{
					IProducerConsumerQueue<Task> producerConsumerQueue = new MultiProducerMultiConsumerQueue<Task>();
					tasks = producerConsumerQueue;
				}
				else
				{
					IProducerConsumerQueue<Task> producerConsumerQueue = new SingleProducerSingleConsumerQueue<Task>();
					tasks = producerConsumerQueue;
				}
				this.m_tasks = tasks;
			}

			// Token: 0x170008F7 RID: 2295
			// (get) Token: 0x060036DA RID: 14042 RVA: 0x000BF10E File Offset: 0x000BD30E
			public override int MaximumConcurrencyLevel
			{
				get
				{
					return this.m_maxConcurrencyLevel;
				}
			}

			// Token: 0x060036DB RID: 14043 RVA: 0x000BF118 File Offset: 0x000BD318
			[SecurityCritical]
			protected internal override void QueueTask(Task task)
			{
				object valueLock = this.m_pair.ValueLock;
				lock (valueLock)
				{
					if (this.m_pair.CompletionRequested)
					{
						throw new InvalidOperationException(base.GetType().Name);
					}
					this.m_tasks.Enqueue(task);
					this.m_pair.ProcessAsyncIfNecessary(false);
				}
			}

			// Token: 0x060036DC RID: 14044 RVA: 0x000BF190 File Offset: 0x000BD390
			[SecuritySafeCritical]
			internal void ExecuteTask(Task task)
			{
				base.TryExecuteTask(task);
			}

			// Token: 0x060036DD RID: 14045 RVA: 0x000BF19C File Offset: 0x000BD39C
			[SecurityCritical]
			protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
			{
				if (!taskWasPreviouslyQueued && this.m_pair.CompletionRequested)
				{
					return false;
				}
				bool flag = this.m_pair.m_underlyingTaskScheduler == TaskScheduler.Default;
				if (flag && taskWasPreviouslyQueued && !Thread.CurrentThread.IsThreadPoolThread)
				{
					return false;
				}
				ConcurrentExclusiveSchedulerPair.ProcessingMode processingMode;
				if (!this.m_pair.m_threadProcessingMapping.TryGetValue(Thread.CurrentThread.ManagedThreadId, out processingMode) || processingMode != this.m_processingMode)
				{
					return false;
				}
				if (!flag || taskWasPreviouslyQueued)
				{
					return this.TryExecuteTaskInlineOnTargetScheduler(task);
				}
				return base.TryExecuteTask(task);
			}

			// Token: 0x060036DE RID: 14046 RVA: 0x000BF220 File Offset: 0x000BD420
			private bool TryExecuteTaskInlineOnTargetScheduler(Task task)
			{
				Task<bool> task2 = new Task<bool>(ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler.s_tryExecuteTaskShim, Tuple.Create<ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler, Task>(this, task));
				bool result;
				try
				{
					task2.RunSynchronously(this.m_pair.m_underlyingTaskScheduler);
					result = task2.Result;
				}
				catch
				{
					AggregateException exception = task2.Exception;
					throw;
				}
				finally
				{
					task2.Dispose();
				}
				return result;
			}

			// Token: 0x060036DF RID: 14047 RVA: 0x000BF288 File Offset: 0x000BD488
			[SecuritySafeCritical]
			private static bool TryExecuteTaskShim(object state)
			{
				Tuple<ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler, Task> tuple = (Tuple<ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler, Task>)state;
				return tuple.Item1.TryExecuteTask(tuple.Item2);
			}

			// Token: 0x060036E0 RID: 14048 RVA: 0x000BF2AD File Offset: 0x000BD4AD
			[SecurityCritical]
			protected override IEnumerable<Task> GetScheduledTasks()
			{
				return this.m_tasks;
			}

			// Token: 0x170008F8 RID: 2296
			// (get) Token: 0x060036E1 RID: 14049 RVA: 0x000BF2B5 File Offset: 0x000BD4B5
			private int CountForDebugger
			{
				get
				{
					return this.m_tasks.Count;
				}
			}

			// Token: 0x060036E2 RID: 14050 RVA: 0x000BF2C2 File Offset: 0x000BD4C2
			// Note: this type is marked as 'beforefieldinit'.
			static ConcurrentExclusiveTaskScheduler()
			{
			}

			// Token: 0x04001CE9 RID: 7401
			private static readonly Func<object, bool> s_tryExecuteTaskShim = new Func<object, bool>(ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler.TryExecuteTaskShim);

			// Token: 0x04001CEA RID: 7402
			private readonly ConcurrentExclusiveSchedulerPair m_pair;

			// Token: 0x04001CEB RID: 7403
			private readonly int m_maxConcurrencyLevel;

			// Token: 0x04001CEC RID: 7404
			private readonly ConcurrentExclusiveSchedulerPair.ProcessingMode m_processingMode;

			// Token: 0x04001CED RID: 7405
			internal readonly IProducerConsumerQueue<Task> m_tasks;

			// Token: 0x020004D8 RID: 1240
			private sealed class DebugView
			{
				// Token: 0x060036E3 RID: 14051 RVA: 0x000BF2D5 File Offset: 0x000BD4D5
				public DebugView(ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler scheduler)
				{
					this.m_taskScheduler = scheduler;
				}

				// Token: 0x170008F9 RID: 2297
				// (get) Token: 0x060036E4 RID: 14052 RVA: 0x000BF2E4 File Offset: 0x000BD4E4
				public int MaximumConcurrencyLevel
				{
					get
					{
						return this.m_taskScheduler.m_maxConcurrencyLevel;
					}
				}

				// Token: 0x170008FA RID: 2298
				// (get) Token: 0x060036E5 RID: 14053 RVA: 0x000BF2F1 File Offset: 0x000BD4F1
				public IEnumerable<Task> ScheduledTasks
				{
					get
					{
						return this.m_taskScheduler.m_tasks;
					}
				}

				// Token: 0x170008FB RID: 2299
				// (get) Token: 0x060036E6 RID: 14054 RVA: 0x000BF2FE File Offset: 0x000BD4FE
				public ConcurrentExclusiveSchedulerPair SchedulerPair
				{
					get
					{
						return this.m_taskScheduler.m_pair;
					}
				}

				// Token: 0x04001CEE RID: 7406
				private readonly ConcurrentExclusiveSchedulerPair.ConcurrentExclusiveTaskScheduler m_taskScheduler;
			}
		}

		// Token: 0x020004D9 RID: 1241
		private sealed class DebugView
		{
			// Token: 0x060036E7 RID: 14055 RVA: 0x000BF30B File Offset: 0x000BD50B
			public DebugView(ConcurrentExclusiveSchedulerPair pair)
			{
				this.m_pair = pair;
			}

			// Token: 0x170008FC RID: 2300
			// (get) Token: 0x060036E8 RID: 14056 RVA: 0x000BF31A File Offset: 0x000BD51A
			public ConcurrentExclusiveSchedulerPair.ProcessingMode Mode
			{
				get
				{
					return this.m_pair.ModeForDebugger;
				}
			}

			// Token: 0x170008FD RID: 2301
			// (get) Token: 0x060036E9 RID: 14057 RVA: 0x000BF327 File Offset: 0x000BD527
			public IEnumerable<Task> ScheduledExclusive
			{
				get
				{
					return this.m_pair.m_exclusiveTaskScheduler.m_tasks;
				}
			}

			// Token: 0x170008FE RID: 2302
			// (get) Token: 0x060036EA RID: 14058 RVA: 0x000BF339 File Offset: 0x000BD539
			public IEnumerable<Task> ScheduledConcurrent
			{
				get
				{
					return this.m_pair.m_concurrentTaskScheduler.m_tasks;
				}
			}

			// Token: 0x170008FF RID: 2303
			// (get) Token: 0x060036EB RID: 14059 RVA: 0x000BF34B File Offset: 0x000BD54B
			public int CurrentlyExecutingTaskCount
			{
				get
				{
					if (this.m_pair.m_processingCount != -1)
					{
						return this.m_pair.m_processingCount;
					}
					return 1;
				}
			}

			// Token: 0x17000900 RID: 2304
			// (get) Token: 0x060036EC RID: 14060 RVA: 0x000BF368 File Offset: 0x000BD568
			public TaskScheduler TargetScheduler
			{
				get
				{
					return this.m_pair.m_underlyingTaskScheduler;
				}
			}

			// Token: 0x04001CEF RID: 7407
			private readonly ConcurrentExclusiveSchedulerPair m_pair;
		}

		// Token: 0x020004DA RID: 1242
		[Flags]
		private enum ProcessingMode : byte
		{
			// Token: 0x04001CF1 RID: 7409
			NotCurrentlyProcessing = 0,
			// Token: 0x04001CF2 RID: 7410
			ProcessingExclusiveTask = 1,
			// Token: 0x04001CF3 RID: 7411
			ProcessingConcurrentTasks = 2,
			// Token: 0x04001CF4 RID: 7412
			Completing = 4,
			// Token: 0x04001CF5 RID: 7413
			Completed = 8
		}

		// Token: 0x020004DB RID: 1243
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060036ED RID: 14061 RVA: 0x000BF375 File Offset: 0x000BD575
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060036EE RID: 14062 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x060036EF RID: 14063 RVA: 0x000BF381 File Offset: 0x000BD581
			internal ConcurrentExclusiveSchedulerPair.CompletionState <EnsureCompletionStateInitialized>b__22_0()
			{
				return new ConcurrentExclusiveSchedulerPair.CompletionState();
			}

			// Token: 0x060036F0 RID: 14064 RVA: 0x000BF388 File Offset: 0x000BD588
			internal void <CompleteTaskAsync>b__29_0(object state)
			{
				ConcurrentExclusiveSchedulerPair.CompletionState completionState = (ConcurrentExclusiveSchedulerPair.CompletionState)state;
				List<Exception> exceptions = completionState.m_exceptions;
				if (exceptions == null || exceptions.Count <= 0)
				{
					completionState.TrySetResult(default(VoidTaskResult));
					return;
				}
				completionState.TrySetException(exceptions);
			}

			// Token: 0x060036F1 RID: 14065 RVA: 0x000BF3C8 File Offset: 0x000BD5C8
			internal void <ProcessAsyncIfNecessary>b__39_0(object thisPair)
			{
				((ConcurrentExclusiveSchedulerPair)thisPair).ProcessExclusiveTasks();
			}

			// Token: 0x060036F2 RID: 14066 RVA: 0x000BF3D5 File Offset: 0x000BD5D5
			internal void <ProcessAsyncIfNecessary>b__39_1(object thisPair)
			{
				((ConcurrentExclusiveSchedulerPair)thisPair).ProcessConcurrentTasks();
			}

			// Token: 0x04001CF6 RID: 7414
			public static readonly ConcurrentExclusiveSchedulerPair.<>c <>9 = new ConcurrentExclusiveSchedulerPair.<>c();

			// Token: 0x04001CF7 RID: 7415
			public static Func<ConcurrentExclusiveSchedulerPair.CompletionState> <>9__22_0;

			// Token: 0x04001CF8 RID: 7416
			public static WaitCallback <>9__29_0;

			// Token: 0x04001CF9 RID: 7417
			public static Action<object> <>9__39_0;

			// Token: 0x04001CFA RID: 7418
			public static Action<object> <>9__39_1;
		}
	}
}
