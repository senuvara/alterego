﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Threading.Tasks
{
	// Token: 0x02000503 RID: 1283
	internal interface IProducerConsumerQueue<T> : IEnumerable<!0>, IEnumerable
	{
		// Token: 0x06003819 RID: 14361
		void Enqueue(T item);

		// Token: 0x0600381A RID: 14362
		bool TryDequeue(out T result);

		// Token: 0x1700092A RID: 2346
		// (get) Token: 0x0600381B RID: 14363
		bool IsEmpty { get; }

		// Token: 0x1700092B RID: 2347
		// (get) Token: 0x0600381C RID: 14364
		int Count { get; }

		// Token: 0x0600381D RID: 14365
		int GetCountSafe(object syncObj);
	}
}
