﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Threading
{
	/// <summary>Represents a handle that has been registered when calling <see cref="M:System.Threading.ThreadPool.RegisterWaitForSingleObject(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.UInt32,System.Boolean)" />. This class cannot be inherited.</summary>
	// Token: 0x020004C4 RID: 1220
	[ComVisible(true)]
	public sealed class RegisteredWaitHandle : MarshalByRefObject
	{
		// Token: 0x0600364C RID: 13900 RVA: 0x000BDC90 File Offset: 0x000BBE90
		internal RegisteredWaitHandle(WaitHandle waitObject, WaitOrTimerCallback callback, object state, TimeSpan timeout, bool executeOnlyOnce)
		{
			this._waitObject = waitObject;
			this._callback = callback;
			this._state = state;
			this._timeout = timeout;
			this._executeOnlyOnce = executeOnlyOnce;
			this._finalEvent = null;
			this._cancelEvent = new ManualResetEvent(false);
			this._callsInProcess = 0;
			this._unregistered = false;
		}

		// Token: 0x0600364D RID: 13901 RVA: 0x000BDCEC File Offset: 0x000BBEEC
		internal void Wait(object state)
		{
			bool flag = false;
			try
			{
				this._waitObject.SafeWaitHandle.DangerousAddRef(ref flag);
				RegisteredWaitHandle obj;
				try
				{
					WaitHandle[] waitHandles = new WaitHandle[]
					{
						this._waitObject,
						this._cancelEvent
					};
					do
					{
						int num = WaitHandle.WaitAny(waitHandles, this._timeout, false);
						if (!this._unregistered)
						{
							obj = this;
							lock (obj)
							{
								this._callsInProcess++;
							}
							ThreadPool.QueueUserWorkItem(new WaitCallback(this.DoCallBack), num == 258);
						}
					}
					while (!this._unregistered && !this._executeOnlyOnce);
				}
				catch
				{
				}
				obj = this;
				lock (obj)
				{
					this._unregistered = true;
					if (this._callsInProcess == 0 && this._finalEvent != null)
					{
						NativeEventCalls.SetEvent(this._finalEvent.SafeWaitHandle);
					}
				}
			}
			catch (ObjectDisposedException)
			{
				if (flag)
				{
					throw;
				}
			}
			finally
			{
				if (flag)
				{
					this._waitObject.SafeWaitHandle.DangerousRelease();
				}
			}
		}

		// Token: 0x0600364E RID: 13902 RVA: 0x000BDE38 File Offset: 0x000BC038
		private void DoCallBack(object timedOut)
		{
			try
			{
				if (this._callback != null)
				{
					this._callback(this._state, (bool)timedOut);
				}
			}
			finally
			{
				lock (this)
				{
					this._callsInProcess--;
					if (this._unregistered && this._callsInProcess == 0 && this._finalEvent != null)
					{
						NativeEventCalls.SetEvent(this._finalEvent.SafeWaitHandle);
					}
				}
			}
		}

		/// <summary>Cancels a registered wait operation issued by the <see cref="M:System.Threading.ThreadPool.RegisterWaitForSingleObject(System.Threading.WaitHandle,System.Threading.WaitOrTimerCallback,System.Object,System.UInt32,System.Boolean)" /> method.</summary>
		/// <param name="waitObject">The <see cref="T:System.Threading.WaitHandle" /> to be signaled. </param>
		/// <returns>
		///     <see langword="true" /> if the function succeeds; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600364F RID: 13903 RVA: 0x000BDED4 File Offset: 0x000BC0D4
		[ComVisible(true)]
		public bool Unregister(WaitHandle waitObject)
		{
			bool result;
			lock (this)
			{
				if (this._unregistered)
				{
					result = false;
				}
				else
				{
					this._finalEvent = waitObject;
					this._unregistered = true;
					this._cancelEvent.Set();
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06003650 RID: 13904 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal RegisteredWaitHandle()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001C86 RID: 7302
		private WaitHandle _waitObject;

		// Token: 0x04001C87 RID: 7303
		private WaitOrTimerCallback _callback;

		// Token: 0x04001C88 RID: 7304
		private object _state;

		// Token: 0x04001C89 RID: 7305
		private WaitHandle _finalEvent;

		// Token: 0x04001C8A RID: 7306
		private ManualResetEvent _cancelEvent;

		// Token: 0x04001C8B RID: 7307
		private TimeSpan _timeout;

		// Token: 0x04001C8C RID: 7308
		private int _callsInProcess;

		// Token: 0x04001C8D RID: 7309
		private bool _executeOnlyOnce;

		// Token: 0x04001C8E RID: 7310
		private bool _unregistered;
	}
}
