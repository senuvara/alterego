﻿using System;

namespace System.Threading
{
	// Token: 0x02000462 RID: 1122
	internal interface IDeferredDisposable
	{
		// Token: 0x060032D1 RID: 13009
		void OnFinalRelease(bool disposed);
	}
}
