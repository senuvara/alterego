﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	/// <summary>Represents the method that executes on a <see cref="T:System.Threading.Thread" />.</summary>
	// Token: 0x020004B0 RID: 1200
	// (Invoke) Token: 0x0600359B RID: 13723
	[ComVisible(true)]
	public delegate void ThreadStart();
}
