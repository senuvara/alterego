﻿using System;
using System.Collections.Generic;

namespace System.Threading
{
	// Token: 0x02000480 RID: 1152
	internal sealed class SystemThreading_ThreadLocalDebugView<T>
	{
		// Token: 0x060033DA RID: 13274 RVA: 0x000B8F50 File Offset: 0x000B7150
		public SystemThreading_ThreadLocalDebugView(ThreadLocal<T> tlocal)
		{
			this.m_tlocal = tlocal;
		}

		// Token: 0x170008A6 RID: 2214
		// (get) Token: 0x060033DB RID: 13275 RVA: 0x000B8F5F File Offset: 0x000B715F
		public bool IsValueCreated
		{
			get
			{
				return this.m_tlocal.IsValueCreated;
			}
		}

		// Token: 0x170008A7 RID: 2215
		// (get) Token: 0x060033DC RID: 13276 RVA: 0x000B8F6C File Offset: 0x000B716C
		public T Value
		{
			get
			{
				return this.m_tlocal.ValueForDebugDisplay;
			}
		}

		// Token: 0x170008A8 RID: 2216
		// (get) Token: 0x060033DD RID: 13277 RVA: 0x000B8F79 File Offset: 0x000B7179
		public List<T> Values
		{
			get
			{
				return this.m_tlocal.ValuesForDebugDisplay;
			}
		}

		// Token: 0x04001BD1 RID: 7121
		private readonly ThreadLocal<T> m_tlocal;
	}
}
