﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x020004AA RID: 1194
	internal static class _ThreadPoolWaitCallback
	{
		// Token: 0x06003557 RID: 13655 RVA: 0x000BBD7C File Offset: 0x000B9F7C
		[SecurityCritical]
		internal static bool PerformWaitCallback()
		{
			return ThreadPoolWorkQueue.Dispatch();
		}
	}
}
