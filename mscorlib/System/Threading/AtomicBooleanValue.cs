﻿using System;

namespace System.Threading
{
	// Token: 0x02000464 RID: 1124
	internal struct AtomicBooleanValue
	{
		// Token: 0x060032D6 RID: 13014 RVA: 0x000B5644 File Offset: 0x000B3844
		public bool CompareAndExchange(bool expected, bool newVal)
		{
			int value = newVal ? 1 : 0;
			int num = expected ? 1 : 0;
			return Interlocked.CompareExchange(ref this.flag, value, num) == num;
		}

		// Token: 0x060032D7 RID: 13015 RVA: 0x000B5674 File Offset: 0x000B3874
		public static AtomicBooleanValue FromValue(bool value)
		{
			return new AtomicBooleanValue
			{
				Value = value
			};
		}

		// Token: 0x060032D8 RID: 13016 RVA: 0x000B5692 File Offset: 0x000B3892
		public bool TrySet()
		{
			return !this.Exchange(true);
		}

		// Token: 0x060032D9 RID: 13017 RVA: 0x000B569E File Offset: 0x000B389E
		public bool TryRelaxedSet()
		{
			return this.flag == 0 && !this.Exchange(true);
		}

		// Token: 0x060032DA RID: 13018 RVA: 0x000B56B4 File Offset: 0x000B38B4
		public bool Exchange(bool newVal)
		{
			int value = newVal ? 1 : 0;
			return Interlocked.Exchange(ref this.flag, value) == 1;
		}

		// Token: 0x17000878 RID: 2168
		// (get) Token: 0x060032DB RID: 13019 RVA: 0x000B56D8 File Offset: 0x000B38D8
		// (set) Token: 0x060032DC RID: 13020 RVA: 0x000B56E3 File Offset: 0x000B38E3
		public bool Value
		{
			get
			{
				return this.flag == 1;
			}
			set
			{
				this.Exchange(value);
			}
		}

		// Token: 0x060032DD RID: 13021 RVA: 0x000B56ED File Offset: 0x000B38ED
		public bool Equals(AtomicBooleanValue rhs)
		{
			return this.flag == rhs.flag;
		}

		// Token: 0x060032DE RID: 13022 RVA: 0x000B56FD File Offset: 0x000B38FD
		public override bool Equals(object rhs)
		{
			return rhs is AtomicBooleanValue && this.Equals((AtomicBooleanValue)rhs);
		}

		// Token: 0x060032DF RID: 13023 RVA: 0x000B5715 File Offset: 0x000B3915
		public override int GetHashCode()
		{
			return this.flag.GetHashCode();
		}

		// Token: 0x060032E0 RID: 13024 RVA: 0x000B5722 File Offset: 0x000B3922
		public static explicit operator bool(AtomicBooleanValue rhs)
		{
			return rhs.Value;
		}

		// Token: 0x060032E1 RID: 13025 RVA: 0x000B572B File Offset: 0x000B392B
		public static implicit operator AtomicBooleanValue(bool rhs)
		{
			return AtomicBooleanValue.FromValue(rhs);
		}

		// Token: 0x04001B55 RID: 6997
		private int flag;

		// Token: 0x04001B56 RID: 6998
		private const int UnSet = 0;

		// Token: 0x04001B57 RID: 6999
		private const int Set = 1;
	}
}
