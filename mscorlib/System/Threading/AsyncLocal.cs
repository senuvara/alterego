﻿using System;
using System.Security;

namespace System.Threading
{
	/// <summary>Represents ambient data that is local to a given asynchronous control flow, such as an asynchronous method. </summary>
	/// <typeparam name="T">The type of the ambient data. </typeparam>
	// Token: 0x02000483 RID: 1155
	public sealed class AsyncLocal<T> : IAsyncLocal
	{
		/// <summary>Instantiates an <see cref="T:System.Threading.AsyncLocal`1" /> instance that does not receive change notifications. </summary>
		// Token: 0x060033E8 RID: 13288 RVA: 0x00002050 File Offset: 0x00000250
		public AsyncLocal()
		{
		}

		/// <summary>Instantiates an <see cref="T:System.Threading.AsyncLocal`1" /> local instance that receives change notifications. </summary>
		/// <param name="valueChangedHandler">The delegate that is called whenever the current value changes on any thread. </param>
		// Token: 0x060033E9 RID: 13289 RVA: 0x000B908E File Offset: 0x000B728E
		[SecurityCritical]
		public AsyncLocal(Action<AsyncLocalValueChangedArgs<T>> valueChangedHandler)
		{
			this.m_valueChangedHandler = valueChangedHandler;
		}

		/// <summary>Gets or sets the value of the ambient data. </summary>
		/// <returns>The value of the ambient data. </returns>
		// Token: 0x170008AB RID: 2219
		// (get) Token: 0x060033EA RID: 13290 RVA: 0x000B90A0 File Offset: 0x000B72A0
		// (set) Token: 0x060033EB RID: 13291 RVA: 0x000B90C7 File Offset: 0x000B72C7
		public T Value
		{
			[SecuritySafeCritical]
			get
			{
				object localValue = ExecutionContext.GetLocalValue(this);
				if (localValue != null)
				{
					return (T)((object)localValue);
				}
				return default(T);
			}
			[SecuritySafeCritical]
			set
			{
				ExecutionContext.SetLocalValue(this, value, this.m_valueChangedHandler != null);
			}
		}

		// Token: 0x060033EC RID: 13292 RVA: 0x000B90E0 File Offset: 0x000B72E0
		[SecurityCritical]
		void IAsyncLocal.OnValueChanged(object previousValueObj, object currentValueObj, bool contextChanged)
		{
			T previousValue = (previousValueObj == null) ? default(T) : ((T)((object)previousValueObj));
			T currentValue = (currentValueObj == null) ? default(T) : ((T)((object)currentValueObj));
			this.m_valueChangedHandler(new AsyncLocalValueChangedArgs<T>(previousValue, currentValue, contextChanged));
		}

		// Token: 0x04001BD8 RID: 7128
		[SecurityCritical]
		private readonly Action<AsyncLocalValueChangedArgs<T>> m_valueChangedHandler;
	}
}
