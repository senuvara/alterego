﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.ExceptionServices;
using System.Security;

namespace System.Threading
{
	// Token: 0x0200048A RID: 1162
	internal struct ExecutionContextSwitcher
	{
		// Token: 0x06003408 RID: 13320 RVA: 0x000B9480 File Offset: 0x000B7680
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		[SecurityCritical]
		[HandleProcessCorruptedStateExceptions]
		internal bool UndoNoThrow()
		{
			try
			{
				this.Undo();
			}
			catch
			{
				return false;
			}
			return true;
		}

		// Token: 0x06003409 RID: 13321 RVA: 0x000B94B0 File Offset: 0x000B76B0
		[SecurityCritical]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		internal void Undo()
		{
			if (this.thread == null)
			{
				return;
			}
			Thread thread = this.thread;
			ExecutionContext.Reader executionContextReader = thread.GetExecutionContextReader();
			thread.SetExecutionContext(this.outerEC, this.outerECBelongsToScope);
			this.thread = null;
			ExecutionContext.OnAsyncLocalContextChanged(executionContextReader.DangerousGetRawExecutionContext(), this.outerEC.DangerousGetRawExecutionContext());
		}

		// Token: 0x04001BDF RID: 7135
		internal ExecutionContext.Reader outerEC;

		// Token: 0x04001BE0 RID: 7136
		internal bool outerECBelongsToScope;

		// Token: 0x04001BE1 RID: 7137
		internal object hecsw;

		// Token: 0x04001BE2 RID: 7138
		internal Thread thread;
	}
}
