﻿using System;

namespace System
{
	// Token: 0x0200022D RID: 557
	internal enum TermInfoStrings
	{
		// Token: 0x04000D79 RID: 3449
		BackTab,
		// Token: 0x04000D7A RID: 3450
		Bell,
		// Token: 0x04000D7B RID: 3451
		CarriageReturn,
		// Token: 0x04000D7C RID: 3452
		ChangeScrollRegion,
		// Token: 0x04000D7D RID: 3453
		ClearAllTabs,
		// Token: 0x04000D7E RID: 3454
		ClearScreen,
		// Token: 0x04000D7F RID: 3455
		ClrEol,
		// Token: 0x04000D80 RID: 3456
		ClrEos,
		// Token: 0x04000D81 RID: 3457
		ColumnAddress,
		// Token: 0x04000D82 RID: 3458
		CommandCharacter,
		// Token: 0x04000D83 RID: 3459
		CursorAddress,
		// Token: 0x04000D84 RID: 3460
		CursorDown,
		// Token: 0x04000D85 RID: 3461
		CursorHome,
		// Token: 0x04000D86 RID: 3462
		CursorInvisible,
		// Token: 0x04000D87 RID: 3463
		CursorLeft,
		// Token: 0x04000D88 RID: 3464
		CursorMemAddress,
		// Token: 0x04000D89 RID: 3465
		CursorNormal,
		// Token: 0x04000D8A RID: 3466
		CursorRight,
		// Token: 0x04000D8B RID: 3467
		CursorToLl,
		// Token: 0x04000D8C RID: 3468
		CursorUp,
		// Token: 0x04000D8D RID: 3469
		CursorVisible,
		// Token: 0x04000D8E RID: 3470
		DeleteCharacter,
		// Token: 0x04000D8F RID: 3471
		DeleteLine,
		// Token: 0x04000D90 RID: 3472
		DisStatusLine,
		// Token: 0x04000D91 RID: 3473
		DownHalfLine,
		// Token: 0x04000D92 RID: 3474
		EnterAltCharsetMode,
		// Token: 0x04000D93 RID: 3475
		EnterBlinkMode,
		// Token: 0x04000D94 RID: 3476
		EnterBoldMode,
		// Token: 0x04000D95 RID: 3477
		EnterCaMode,
		// Token: 0x04000D96 RID: 3478
		EnterDeleteMode,
		// Token: 0x04000D97 RID: 3479
		EnterDimMode,
		// Token: 0x04000D98 RID: 3480
		EnterInsertMode,
		// Token: 0x04000D99 RID: 3481
		EnterSecureMode,
		// Token: 0x04000D9A RID: 3482
		EnterProtectedMode,
		// Token: 0x04000D9B RID: 3483
		EnterReverseMode,
		// Token: 0x04000D9C RID: 3484
		EnterStandoutMode,
		// Token: 0x04000D9D RID: 3485
		EnterUnderlineMode,
		// Token: 0x04000D9E RID: 3486
		EraseChars,
		// Token: 0x04000D9F RID: 3487
		ExitAltCharsetMode,
		// Token: 0x04000DA0 RID: 3488
		ExitAttributeMode,
		// Token: 0x04000DA1 RID: 3489
		ExitCaMode,
		// Token: 0x04000DA2 RID: 3490
		ExitDeleteMode,
		// Token: 0x04000DA3 RID: 3491
		ExitInsertMode,
		// Token: 0x04000DA4 RID: 3492
		ExitStandoutMode,
		// Token: 0x04000DA5 RID: 3493
		ExitUnderlineMode,
		// Token: 0x04000DA6 RID: 3494
		FlashScreen,
		// Token: 0x04000DA7 RID: 3495
		FormFeed,
		// Token: 0x04000DA8 RID: 3496
		FromStatusLine,
		// Token: 0x04000DA9 RID: 3497
		Init1string,
		// Token: 0x04000DAA RID: 3498
		Init2string,
		// Token: 0x04000DAB RID: 3499
		Init3string,
		// Token: 0x04000DAC RID: 3500
		InitFile,
		// Token: 0x04000DAD RID: 3501
		InsertCharacter,
		// Token: 0x04000DAE RID: 3502
		InsertLine,
		// Token: 0x04000DAF RID: 3503
		InsertPadding,
		// Token: 0x04000DB0 RID: 3504
		KeyBackspace,
		// Token: 0x04000DB1 RID: 3505
		KeyCatab,
		// Token: 0x04000DB2 RID: 3506
		KeyClear,
		// Token: 0x04000DB3 RID: 3507
		KeyCtab,
		// Token: 0x04000DB4 RID: 3508
		KeyDc,
		// Token: 0x04000DB5 RID: 3509
		KeyDl,
		// Token: 0x04000DB6 RID: 3510
		KeyDown,
		// Token: 0x04000DB7 RID: 3511
		KeyEic,
		// Token: 0x04000DB8 RID: 3512
		KeyEol,
		// Token: 0x04000DB9 RID: 3513
		KeyEos,
		// Token: 0x04000DBA RID: 3514
		KeyF0,
		// Token: 0x04000DBB RID: 3515
		KeyF1,
		// Token: 0x04000DBC RID: 3516
		KeyF10,
		// Token: 0x04000DBD RID: 3517
		KeyF2,
		// Token: 0x04000DBE RID: 3518
		KeyF3,
		// Token: 0x04000DBF RID: 3519
		KeyF4,
		// Token: 0x04000DC0 RID: 3520
		KeyF5,
		// Token: 0x04000DC1 RID: 3521
		KeyF6,
		// Token: 0x04000DC2 RID: 3522
		KeyF7,
		// Token: 0x04000DC3 RID: 3523
		KeyF8,
		// Token: 0x04000DC4 RID: 3524
		KeyF9,
		// Token: 0x04000DC5 RID: 3525
		KeyHome,
		// Token: 0x04000DC6 RID: 3526
		KeyIc,
		// Token: 0x04000DC7 RID: 3527
		KeyIl,
		// Token: 0x04000DC8 RID: 3528
		KeyLeft,
		// Token: 0x04000DC9 RID: 3529
		KeyLl,
		// Token: 0x04000DCA RID: 3530
		KeyNpage,
		// Token: 0x04000DCB RID: 3531
		KeyPpage,
		// Token: 0x04000DCC RID: 3532
		KeyRight,
		// Token: 0x04000DCD RID: 3533
		KeySf,
		// Token: 0x04000DCE RID: 3534
		KeySr,
		// Token: 0x04000DCF RID: 3535
		KeyStab,
		// Token: 0x04000DD0 RID: 3536
		KeyUp,
		// Token: 0x04000DD1 RID: 3537
		KeypadLocal,
		// Token: 0x04000DD2 RID: 3538
		KeypadXmit,
		// Token: 0x04000DD3 RID: 3539
		LabF0,
		// Token: 0x04000DD4 RID: 3540
		LabF1,
		// Token: 0x04000DD5 RID: 3541
		LabF10,
		// Token: 0x04000DD6 RID: 3542
		LabF2,
		// Token: 0x04000DD7 RID: 3543
		LabF3,
		// Token: 0x04000DD8 RID: 3544
		LabF4,
		// Token: 0x04000DD9 RID: 3545
		LabF5,
		// Token: 0x04000DDA RID: 3546
		LabF6,
		// Token: 0x04000DDB RID: 3547
		LabF7,
		// Token: 0x04000DDC RID: 3548
		LabF8,
		// Token: 0x04000DDD RID: 3549
		LabF9,
		// Token: 0x04000DDE RID: 3550
		MetaOff,
		// Token: 0x04000DDF RID: 3551
		MetaOn,
		// Token: 0x04000DE0 RID: 3552
		Newline,
		// Token: 0x04000DE1 RID: 3553
		PadChar,
		// Token: 0x04000DE2 RID: 3554
		ParmDch,
		// Token: 0x04000DE3 RID: 3555
		ParmDeleteLine,
		// Token: 0x04000DE4 RID: 3556
		ParmDownCursor,
		// Token: 0x04000DE5 RID: 3557
		ParmIch,
		// Token: 0x04000DE6 RID: 3558
		ParmIndex,
		// Token: 0x04000DE7 RID: 3559
		ParmInsertLine,
		// Token: 0x04000DE8 RID: 3560
		ParmLeftCursor,
		// Token: 0x04000DE9 RID: 3561
		ParmRightCursor,
		// Token: 0x04000DEA RID: 3562
		ParmRindex,
		// Token: 0x04000DEB RID: 3563
		ParmUpCursor,
		// Token: 0x04000DEC RID: 3564
		PkeyKey,
		// Token: 0x04000DED RID: 3565
		PkeyLocal,
		// Token: 0x04000DEE RID: 3566
		PkeyXmit,
		// Token: 0x04000DEF RID: 3567
		PrintScreen,
		// Token: 0x04000DF0 RID: 3568
		PrtrOff,
		// Token: 0x04000DF1 RID: 3569
		PrtrOn,
		// Token: 0x04000DF2 RID: 3570
		RepeatChar,
		// Token: 0x04000DF3 RID: 3571
		Reset1string,
		// Token: 0x04000DF4 RID: 3572
		Reset2string,
		// Token: 0x04000DF5 RID: 3573
		Reset3string,
		// Token: 0x04000DF6 RID: 3574
		ResetFile,
		// Token: 0x04000DF7 RID: 3575
		RestoreCursor,
		// Token: 0x04000DF8 RID: 3576
		RowAddress,
		// Token: 0x04000DF9 RID: 3577
		SaveCursor,
		// Token: 0x04000DFA RID: 3578
		ScrollForward,
		// Token: 0x04000DFB RID: 3579
		ScrollReverse,
		// Token: 0x04000DFC RID: 3580
		SetAttributes,
		// Token: 0x04000DFD RID: 3581
		SetTab,
		// Token: 0x04000DFE RID: 3582
		SetWindow,
		// Token: 0x04000DFF RID: 3583
		Tab,
		// Token: 0x04000E00 RID: 3584
		ToStatusLine,
		// Token: 0x04000E01 RID: 3585
		UnderlineChar,
		// Token: 0x04000E02 RID: 3586
		UpHalfLine,
		// Token: 0x04000E03 RID: 3587
		InitProg,
		// Token: 0x04000E04 RID: 3588
		KeyA1,
		// Token: 0x04000E05 RID: 3589
		KeyA3,
		// Token: 0x04000E06 RID: 3590
		KeyB2,
		// Token: 0x04000E07 RID: 3591
		KeyC1,
		// Token: 0x04000E08 RID: 3592
		KeyC3,
		// Token: 0x04000E09 RID: 3593
		PrtrNon,
		// Token: 0x04000E0A RID: 3594
		CharPadding,
		// Token: 0x04000E0B RID: 3595
		AcsChars,
		// Token: 0x04000E0C RID: 3596
		PlabNorm,
		// Token: 0x04000E0D RID: 3597
		KeyBtab,
		// Token: 0x04000E0E RID: 3598
		EnterXonMode,
		// Token: 0x04000E0F RID: 3599
		ExitXonMode,
		// Token: 0x04000E10 RID: 3600
		EnterAmMode,
		// Token: 0x04000E11 RID: 3601
		ExitAmMode,
		// Token: 0x04000E12 RID: 3602
		XonCharacter,
		// Token: 0x04000E13 RID: 3603
		XoffCharacter,
		// Token: 0x04000E14 RID: 3604
		EnaAcs,
		// Token: 0x04000E15 RID: 3605
		LabelOn,
		// Token: 0x04000E16 RID: 3606
		LabelOff,
		// Token: 0x04000E17 RID: 3607
		KeyBeg,
		// Token: 0x04000E18 RID: 3608
		KeyCancel,
		// Token: 0x04000E19 RID: 3609
		KeyClose,
		// Token: 0x04000E1A RID: 3610
		KeyCommand,
		// Token: 0x04000E1B RID: 3611
		KeyCopy,
		// Token: 0x04000E1C RID: 3612
		KeyCreate,
		// Token: 0x04000E1D RID: 3613
		KeyEnd,
		// Token: 0x04000E1E RID: 3614
		KeyEnter,
		// Token: 0x04000E1F RID: 3615
		KeyExit,
		// Token: 0x04000E20 RID: 3616
		KeyFind,
		// Token: 0x04000E21 RID: 3617
		KeyHelp,
		// Token: 0x04000E22 RID: 3618
		KeyMark,
		// Token: 0x04000E23 RID: 3619
		KeyMessage,
		// Token: 0x04000E24 RID: 3620
		KeyMove,
		// Token: 0x04000E25 RID: 3621
		KeyNext,
		// Token: 0x04000E26 RID: 3622
		KeyOpen,
		// Token: 0x04000E27 RID: 3623
		KeyOptions,
		// Token: 0x04000E28 RID: 3624
		KeyPrevious,
		// Token: 0x04000E29 RID: 3625
		KeyPrint,
		// Token: 0x04000E2A RID: 3626
		KeyRedo,
		// Token: 0x04000E2B RID: 3627
		KeyReference,
		// Token: 0x04000E2C RID: 3628
		KeyRefresh,
		// Token: 0x04000E2D RID: 3629
		KeyReplace,
		// Token: 0x04000E2E RID: 3630
		KeyRestart,
		// Token: 0x04000E2F RID: 3631
		KeyResume,
		// Token: 0x04000E30 RID: 3632
		KeySave,
		// Token: 0x04000E31 RID: 3633
		KeySuspend,
		// Token: 0x04000E32 RID: 3634
		KeyUndo,
		// Token: 0x04000E33 RID: 3635
		KeySbeg,
		// Token: 0x04000E34 RID: 3636
		KeyScancel,
		// Token: 0x04000E35 RID: 3637
		KeyScommand,
		// Token: 0x04000E36 RID: 3638
		KeyScopy,
		// Token: 0x04000E37 RID: 3639
		KeyScreate,
		// Token: 0x04000E38 RID: 3640
		KeySdc,
		// Token: 0x04000E39 RID: 3641
		KeySdl,
		// Token: 0x04000E3A RID: 3642
		KeySelect,
		// Token: 0x04000E3B RID: 3643
		KeySend,
		// Token: 0x04000E3C RID: 3644
		KeySeol,
		// Token: 0x04000E3D RID: 3645
		KeySexit,
		// Token: 0x04000E3E RID: 3646
		KeySfind,
		// Token: 0x04000E3F RID: 3647
		KeyShelp,
		// Token: 0x04000E40 RID: 3648
		KeyShome,
		// Token: 0x04000E41 RID: 3649
		KeySic,
		// Token: 0x04000E42 RID: 3650
		KeySleft,
		// Token: 0x04000E43 RID: 3651
		KeySmessage,
		// Token: 0x04000E44 RID: 3652
		KeySmove,
		// Token: 0x04000E45 RID: 3653
		KeySnext,
		// Token: 0x04000E46 RID: 3654
		KeySoptions,
		// Token: 0x04000E47 RID: 3655
		KeySprevious,
		// Token: 0x04000E48 RID: 3656
		KeySprint,
		// Token: 0x04000E49 RID: 3657
		KeySredo,
		// Token: 0x04000E4A RID: 3658
		KeySreplace,
		// Token: 0x04000E4B RID: 3659
		KeySright,
		// Token: 0x04000E4C RID: 3660
		KeySrsume,
		// Token: 0x04000E4D RID: 3661
		KeySsave,
		// Token: 0x04000E4E RID: 3662
		KeySsuspend,
		// Token: 0x04000E4F RID: 3663
		KeySundo,
		// Token: 0x04000E50 RID: 3664
		ReqForInput,
		// Token: 0x04000E51 RID: 3665
		KeyF11,
		// Token: 0x04000E52 RID: 3666
		KeyF12,
		// Token: 0x04000E53 RID: 3667
		KeyF13,
		// Token: 0x04000E54 RID: 3668
		KeyF14,
		// Token: 0x04000E55 RID: 3669
		KeyF15,
		// Token: 0x04000E56 RID: 3670
		KeyF16,
		// Token: 0x04000E57 RID: 3671
		KeyF17,
		// Token: 0x04000E58 RID: 3672
		KeyF18,
		// Token: 0x04000E59 RID: 3673
		KeyF19,
		// Token: 0x04000E5A RID: 3674
		KeyF20,
		// Token: 0x04000E5B RID: 3675
		KeyF21,
		// Token: 0x04000E5C RID: 3676
		KeyF22,
		// Token: 0x04000E5D RID: 3677
		KeyF23,
		// Token: 0x04000E5E RID: 3678
		KeyF24,
		// Token: 0x04000E5F RID: 3679
		KeyF25,
		// Token: 0x04000E60 RID: 3680
		KeyF26,
		// Token: 0x04000E61 RID: 3681
		KeyF27,
		// Token: 0x04000E62 RID: 3682
		KeyF28,
		// Token: 0x04000E63 RID: 3683
		KeyF29,
		// Token: 0x04000E64 RID: 3684
		KeyF30,
		// Token: 0x04000E65 RID: 3685
		KeyF31,
		// Token: 0x04000E66 RID: 3686
		KeyF32,
		// Token: 0x04000E67 RID: 3687
		KeyF33,
		// Token: 0x04000E68 RID: 3688
		KeyF34,
		// Token: 0x04000E69 RID: 3689
		KeyF35,
		// Token: 0x04000E6A RID: 3690
		KeyF36,
		// Token: 0x04000E6B RID: 3691
		KeyF37,
		// Token: 0x04000E6C RID: 3692
		KeyF38,
		// Token: 0x04000E6D RID: 3693
		KeyF39,
		// Token: 0x04000E6E RID: 3694
		KeyF40,
		// Token: 0x04000E6F RID: 3695
		KeyF41,
		// Token: 0x04000E70 RID: 3696
		KeyF42,
		// Token: 0x04000E71 RID: 3697
		KeyF43,
		// Token: 0x04000E72 RID: 3698
		KeyF44,
		// Token: 0x04000E73 RID: 3699
		KeyF45,
		// Token: 0x04000E74 RID: 3700
		KeyF46,
		// Token: 0x04000E75 RID: 3701
		KeyF47,
		// Token: 0x04000E76 RID: 3702
		KeyF48,
		// Token: 0x04000E77 RID: 3703
		KeyF49,
		// Token: 0x04000E78 RID: 3704
		KeyF50,
		// Token: 0x04000E79 RID: 3705
		KeyF51,
		// Token: 0x04000E7A RID: 3706
		KeyF52,
		// Token: 0x04000E7B RID: 3707
		KeyF53,
		// Token: 0x04000E7C RID: 3708
		KeyF54,
		// Token: 0x04000E7D RID: 3709
		KeyF55,
		// Token: 0x04000E7E RID: 3710
		KeyF56,
		// Token: 0x04000E7F RID: 3711
		KeyF57,
		// Token: 0x04000E80 RID: 3712
		KeyF58,
		// Token: 0x04000E81 RID: 3713
		KeyF59,
		// Token: 0x04000E82 RID: 3714
		KeyF60,
		// Token: 0x04000E83 RID: 3715
		KeyF61,
		// Token: 0x04000E84 RID: 3716
		KeyF62,
		// Token: 0x04000E85 RID: 3717
		KeyF63,
		// Token: 0x04000E86 RID: 3718
		ClrBol,
		// Token: 0x04000E87 RID: 3719
		ClearMargins,
		// Token: 0x04000E88 RID: 3720
		SetLeftMargin,
		// Token: 0x04000E89 RID: 3721
		SetRightMargin,
		// Token: 0x04000E8A RID: 3722
		LabelFormat,
		// Token: 0x04000E8B RID: 3723
		SetClock,
		// Token: 0x04000E8C RID: 3724
		DisplayClock,
		// Token: 0x04000E8D RID: 3725
		RemoveClock,
		// Token: 0x04000E8E RID: 3726
		CreateWindow,
		// Token: 0x04000E8F RID: 3727
		GotoWindow,
		// Token: 0x04000E90 RID: 3728
		Hangup,
		// Token: 0x04000E91 RID: 3729
		DialPhone,
		// Token: 0x04000E92 RID: 3730
		QuickDial,
		// Token: 0x04000E93 RID: 3731
		Tone,
		// Token: 0x04000E94 RID: 3732
		Pulse,
		// Token: 0x04000E95 RID: 3733
		FlashHook,
		// Token: 0x04000E96 RID: 3734
		FixedPause,
		// Token: 0x04000E97 RID: 3735
		WaitTone,
		// Token: 0x04000E98 RID: 3736
		User0,
		// Token: 0x04000E99 RID: 3737
		User1,
		// Token: 0x04000E9A RID: 3738
		User2,
		// Token: 0x04000E9B RID: 3739
		User3,
		// Token: 0x04000E9C RID: 3740
		User4,
		// Token: 0x04000E9D RID: 3741
		User5,
		// Token: 0x04000E9E RID: 3742
		User6,
		// Token: 0x04000E9F RID: 3743
		User7,
		// Token: 0x04000EA0 RID: 3744
		User8,
		// Token: 0x04000EA1 RID: 3745
		User9,
		// Token: 0x04000EA2 RID: 3746
		OrigPair,
		// Token: 0x04000EA3 RID: 3747
		OrigColors,
		// Token: 0x04000EA4 RID: 3748
		InitializeColor,
		// Token: 0x04000EA5 RID: 3749
		InitializePair,
		// Token: 0x04000EA6 RID: 3750
		SetColorPair,
		// Token: 0x04000EA7 RID: 3751
		SetForeground,
		// Token: 0x04000EA8 RID: 3752
		SetBackground,
		// Token: 0x04000EA9 RID: 3753
		ChangeCharPitch,
		// Token: 0x04000EAA RID: 3754
		ChangeLinePitch,
		// Token: 0x04000EAB RID: 3755
		ChangeResHorz,
		// Token: 0x04000EAC RID: 3756
		ChangeResVert,
		// Token: 0x04000EAD RID: 3757
		DefineChar,
		// Token: 0x04000EAE RID: 3758
		EnterDoublewideMode,
		// Token: 0x04000EAF RID: 3759
		EnterDraftQuality,
		// Token: 0x04000EB0 RID: 3760
		EnterItalicsMode,
		// Token: 0x04000EB1 RID: 3761
		EnterLeftwardMode,
		// Token: 0x04000EB2 RID: 3762
		EnterMicroMode,
		// Token: 0x04000EB3 RID: 3763
		EnterNearLetterQuality,
		// Token: 0x04000EB4 RID: 3764
		EnterNormalQuality,
		// Token: 0x04000EB5 RID: 3765
		EnterShadowMode,
		// Token: 0x04000EB6 RID: 3766
		EnterSubscriptMode,
		// Token: 0x04000EB7 RID: 3767
		EnterSuperscriptMode,
		// Token: 0x04000EB8 RID: 3768
		EnterUpwardMode,
		// Token: 0x04000EB9 RID: 3769
		ExitDoublewideMode,
		// Token: 0x04000EBA RID: 3770
		ExitItalicsMode,
		// Token: 0x04000EBB RID: 3771
		ExitLeftwardMode,
		// Token: 0x04000EBC RID: 3772
		ExitMicroMode,
		// Token: 0x04000EBD RID: 3773
		ExitShadowMode,
		// Token: 0x04000EBE RID: 3774
		ExitSubscriptMode,
		// Token: 0x04000EBF RID: 3775
		ExitSuperscriptMode,
		// Token: 0x04000EC0 RID: 3776
		ExitUpwardMode,
		// Token: 0x04000EC1 RID: 3777
		MicroColumnAddress,
		// Token: 0x04000EC2 RID: 3778
		MicroDown,
		// Token: 0x04000EC3 RID: 3779
		MicroLeft,
		// Token: 0x04000EC4 RID: 3780
		MicroRight,
		// Token: 0x04000EC5 RID: 3781
		MicroRowAddress,
		// Token: 0x04000EC6 RID: 3782
		MicroUp,
		// Token: 0x04000EC7 RID: 3783
		OrderOfPins,
		// Token: 0x04000EC8 RID: 3784
		ParmDownMicro,
		// Token: 0x04000EC9 RID: 3785
		ParmLeftMicro,
		// Token: 0x04000ECA RID: 3786
		ParmRightMicro,
		// Token: 0x04000ECB RID: 3787
		ParmUpMicro,
		// Token: 0x04000ECC RID: 3788
		SelectCharSet,
		// Token: 0x04000ECD RID: 3789
		SetBottomMargin,
		// Token: 0x04000ECE RID: 3790
		SetBottomMarginParm,
		// Token: 0x04000ECF RID: 3791
		SetLeftMarginParm,
		// Token: 0x04000ED0 RID: 3792
		SetRightMarginParm,
		// Token: 0x04000ED1 RID: 3793
		SetTopMargin,
		// Token: 0x04000ED2 RID: 3794
		SetTopMarginParm,
		// Token: 0x04000ED3 RID: 3795
		StartBitImage,
		// Token: 0x04000ED4 RID: 3796
		StartCharSetDef,
		// Token: 0x04000ED5 RID: 3797
		StopBitImage,
		// Token: 0x04000ED6 RID: 3798
		StopCharSetDef,
		// Token: 0x04000ED7 RID: 3799
		SubscriptCharacters,
		// Token: 0x04000ED8 RID: 3800
		SuperscriptCharacters,
		// Token: 0x04000ED9 RID: 3801
		TheseCauseCr,
		// Token: 0x04000EDA RID: 3802
		ZeroMotion,
		// Token: 0x04000EDB RID: 3803
		CharSetNames,
		// Token: 0x04000EDC RID: 3804
		KeyMouse,
		// Token: 0x04000EDD RID: 3805
		MouseInfo,
		// Token: 0x04000EDE RID: 3806
		ReqMousePos,
		// Token: 0x04000EDF RID: 3807
		GetMouse,
		// Token: 0x04000EE0 RID: 3808
		SetAForeground,
		// Token: 0x04000EE1 RID: 3809
		SetABackground,
		// Token: 0x04000EE2 RID: 3810
		PkeyPlab,
		// Token: 0x04000EE3 RID: 3811
		DeviceType,
		// Token: 0x04000EE4 RID: 3812
		CodeSetInit,
		// Token: 0x04000EE5 RID: 3813
		Set0DesSeq,
		// Token: 0x04000EE6 RID: 3814
		Set1DesSeq,
		// Token: 0x04000EE7 RID: 3815
		Set2DesSeq,
		// Token: 0x04000EE8 RID: 3816
		Set3DesSeq,
		// Token: 0x04000EE9 RID: 3817
		SetLrMargin,
		// Token: 0x04000EEA RID: 3818
		SetTbMargin,
		// Token: 0x04000EEB RID: 3819
		BitImageRepeat,
		// Token: 0x04000EEC RID: 3820
		BitImageNewline,
		// Token: 0x04000EED RID: 3821
		BitImageCarriageReturn,
		// Token: 0x04000EEE RID: 3822
		ColorNames,
		// Token: 0x04000EEF RID: 3823
		DefineBitImageRegion,
		// Token: 0x04000EF0 RID: 3824
		EndBitImageRegion,
		// Token: 0x04000EF1 RID: 3825
		SetColorBand,
		// Token: 0x04000EF2 RID: 3826
		SetPageLength,
		// Token: 0x04000EF3 RID: 3827
		DisplayPcChar,
		// Token: 0x04000EF4 RID: 3828
		EnterPcCharsetMode,
		// Token: 0x04000EF5 RID: 3829
		ExitPcCharsetMode,
		// Token: 0x04000EF6 RID: 3830
		EnterScancodeMode,
		// Token: 0x04000EF7 RID: 3831
		ExitScancodeMode,
		// Token: 0x04000EF8 RID: 3832
		PcTermOptions,
		// Token: 0x04000EF9 RID: 3833
		ScancodeEscape,
		// Token: 0x04000EFA RID: 3834
		AltScancodeEsc,
		// Token: 0x04000EFB RID: 3835
		EnterHorizontalHlMode,
		// Token: 0x04000EFC RID: 3836
		EnterLeftHlMode,
		// Token: 0x04000EFD RID: 3837
		EnterLowHlMode,
		// Token: 0x04000EFE RID: 3838
		EnterRightHlMode,
		// Token: 0x04000EFF RID: 3839
		EnterTopHlMode,
		// Token: 0x04000F00 RID: 3840
		EnterVerticalHlMode,
		// Token: 0x04000F01 RID: 3841
		SetAAttributes,
		// Token: 0x04000F02 RID: 3842
		SetPglenInch,
		// Token: 0x04000F03 RID: 3843
		Last
	}
}
