﻿using System;
using System.Runtime.CompilerServices;
using System.Security;
using System.Threading;

namespace System
{
	// Token: 0x02000158 RID: 344
	internal class SizedReference : IDisposable
	{
		// Token: 0x06000F22 RID: 3874
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr CreateSizedRef(object o);

		// Token: 0x06000F23 RID: 3875
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void FreeSizedRef(IntPtr h);

		// Token: 0x06000F24 RID: 3876
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern object GetTargetOfSizedRef(IntPtr h);

		// Token: 0x06000F25 RID: 3877
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern long GetApproximateSizeOfSizedRef(IntPtr h);

		// Token: 0x06000F26 RID: 3878 RVA: 0x0003E748 File Offset: 0x0003C948
		[SecuritySafeCritical]
		private void Free()
		{
			IntPtr handle = this._handle;
			if (handle != IntPtr.Zero && Interlocked.CompareExchange(ref this._handle, IntPtr.Zero, handle) == handle)
			{
				SizedReference.FreeSizedRef(handle);
			}
		}

		// Token: 0x06000F27 RID: 3879 RVA: 0x0003E78C File Offset: 0x0003C98C
		[SecuritySafeCritical]
		public SizedReference(object target)
		{
			IntPtr handle = IntPtr.Zero;
			handle = SizedReference.CreateSizedRef(target);
			this._handle = handle;
		}

		// Token: 0x06000F28 RID: 3880 RVA: 0x0003E7B8 File Offset: 0x0003C9B8
		~SizedReference()
		{
			this.Free();
		}

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06000F29 RID: 3881 RVA: 0x0003E7E4 File Offset: 0x0003C9E4
		public object Target
		{
			[SecuritySafeCritical]
			get
			{
				IntPtr handle = this._handle;
				if (handle == IntPtr.Zero)
				{
					return null;
				}
				object targetOfSizedRef = SizedReference.GetTargetOfSizedRef(handle);
				if (!(this._handle == IntPtr.Zero))
				{
					return targetOfSizedRef;
				}
				return null;
			}
		}

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x06000F2A RID: 3882 RVA: 0x0003E828 File Offset: 0x0003CA28
		public long ApproximateSize
		{
			[SecuritySafeCritical]
			get
			{
				IntPtr handle = this._handle;
				if (handle == IntPtr.Zero)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Handle is not initialized."));
				}
				long approximateSizeOfSizedRef = SizedReference.GetApproximateSizeOfSizedRef(handle);
				if (this._handle == IntPtr.Zero)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Handle is not initialized."));
				}
				return approximateSizeOfSizedRef;
			}
		}

		// Token: 0x06000F2B RID: 3883 RVA: 0x0003E885 File Offset: 0x0003CA85
		public void Dispose()
		{
			this.Free();
			GC.SuppressFinalize(this);
		}

		// Token: 0x040008FA RID: 2298
		internal volatile IntPtr _handle;
	}
}
