﻿using System;

namespace System
{
	// Token: 0x0200020A RID: 522
	internal interface IConsoleDriver
	{
		// Token: 0x1700033B RID: 827
		// (get) Token: 0x060018CF RID: 6351
		// (set) Token: 0x060018D0 RID: 6352
		ConsoleColor BackgroundColor { get; set; }

		// Token: 0x1700033C RID: 828
		// (get) Token: 0x060018D1 RID: 6353
		// (set) Token: 0x060018D2 RID: 6354
		int BufferHeight { get; set; }

		// Token: 0x1700033D RID: 829
		// (get) Token: 0x060018D3 RID: 6355
		// (set) Token: 0x060018D4 RID: 6356
		int BufferWidth { get; set; }

		// Token: 0x1700033E RID: 830
		// (get) Token: 0x060018D5 RID: 6357
		bool CapsLock { get; }

		// Token: 0x1700033F RID: 831
		// (get) Token: 0x060018D6 RID: 6358
		// (set) Token: 0x060018D7 RID: 6359
		int CursorLeft { get; set; }

		// Token: 0x17000340 RID: 832
		// (get) Token: 0x060018D8 RID: 6360
		// (set) Token: 0x060018D9 RID: 6361
		int CursorSize { get; set; }

		// Token: 0x17000341 RID: 833
		// (get) Token: 0x060018DA RID: 6362
		// (set) Token: 0x060018DB RID: 6363
		int CursorTop { get; set; }

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x060018DC RID: 6364
		// (set) Token: 0x060018DD RID: 6365
		bool CursorVisible { get; set; }

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x060018DE RID: 6366
		// (set) Token: 0x060018DF RID: 6367
		ConsoleColor ForegroundColor { get; set; }

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x060018E0 RID: 6368
		bool KeyAvailable { get; }

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x060018E1 RID: 6369
		bool Initialized { get; }

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x060018E2 RID: 6370
		int LargestWindowHeight { get; }

		// Token: 0x17000347 RID: 839
		// (get) Token: 0x060018E3 RID: 6371
		int LargestWindowWidth { get; }

		// Token: 0x17000348 RID: 840
		// (get) Token: 0x060018E4 RID: 6372
		bool NumberLock { get; }

		// Token: 0x17000349 RID: 841
		// (get) Token: 0x060018E5 RID: 6373
		// (set) Token: 0x060018E6 RID: 6374
		string Title { get; set; }

		// Token: 0x1700034A RID: 842
		// (get) Token: 0x060018E7 RID: 6375
		// (set) Token: 0x060018E8 RID: 6376
		bool TreatControlCAsInput { get; set; }

		// Token: 0x1700034B RID: 843
		// (get) Token: 0x060018E9 RID: 6377
		// (set) Token: 0x060018EA RID: 6378
		int WindowHeight { get; set; }

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x060018EB RID: 6379
		// (set) Token: 0x060018EC RID: 6380
		int WindowLeft { get; set; }

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x060018ED RID: 6381
		// (set) Token: 0x060018EE RID: 6382
		int WindowTop { get; set; }

		// Token: 0x1700034E RID: 846
		// (get) Token: 0x060018EF RID: 6383
		// (set) Token: 0x060018F0 RID: 6384
		int WindowWidth { get; set; }

		// Token: 0x060018F1 RID: 6385
		void Init();

		// Token: 0x060018F2 RID: 6386
		void Beep(int frequency, int duration);

		// Token: 0x060018F3 RID: 6387
		void Clear();

		// Token: 0x060018F4 RID: 6388
		void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop, char sourceChar, ConsoleColor sourceForeColor, ConsoleColor sourceBackColor);

		// Token: 0x060018F5 RID: 6389
		ConsoleKeyInfo ReadKey(bool intercept);

		// Token: 0x060018F6 RID: 6390
		void ResetColor();

		// Token: 0x060018F7 RID: 6391
		void SetBufferSize(int width, int height);

		// Token: 0x060018F8 RID: 6392
		void SetCursorPosition(int left, int top);

		// Token: 0x060018F9 RID: 6393
		void SetWindowPosition(int left, int top);

		// Token: 0x060018FA RID: 6394
		void SetWindowSize(int width, int height);

		// Token: 0x060018FB RID: 6395
		string ReadLine();
	}
}
