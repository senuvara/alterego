﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents assembly binding information that can be added to an instance of <see cref="T:System.AppDomain" />.</summary>
	// Token: 0x0200016F RID: 367
	[ComVisible(true)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("27FFF232-A7A8-40dd-8D4A-734AD59FCD41")]
	public interface IAppDomainSetup
	{
		/// <summary>Gets or sets the name of the directory containing the application.</summary>
		/// <returns>A <see cref="T:System.String" /> containg the name of the application base directory.</returns>
		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x06001031 RID: 4145
		// (set) Token: 0x06001032 RID: 4146
		string ApplicationBase { get; set; }

		/// <summary>Gets or sets the name of the application.</summary>
		/// <returns>A <see cref="T:System.String" /> that is the name of the application.</returns>
		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06001033 RID: 4147
		// (set) Token: 0x06001034 RID: 4148
		string ApplicationName { get; set; }

		/// <summary>Gets and sets the name of an area specific to the application where files are shadow copied.</summary>
		/// <returns>A <see cref="T:System.String" /> that is the fully-qualified name of the directory path and file name where files are shadow copied.</returns>
		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x06001035 RID: 4149
		// (set) Token: 0x06001036 RID: 4150
		string CachePath { get; set; }

		/// <summary>Gets and sets the name of the configuration file for an application domain.</summary>
		/// <returns>A <see cref="T:System.String" /> that specifies the name of the configuration file.</returns>
		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x06001037 RID: 4151
		// (set) Token: 0x06001038 RID: 4152
		string ConfigurationFile { get; set; }

		/// <summary>Gets or sets the directory where dynamically generated files are stored and accessed.</summary>
		/// <returns>A <see cref="T:System.String" /> that specifies the directory containing dynamic assemblies.</returns>
		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x06001039 RID: 4153
		// (set) Token: 0x0600103A RID: 4154
		string DynamicBase { get; set; }

		/// <summary>Gets or sets the location of the license file associated with this domain.</summary>
		/// <returns>A <see cref="T:System.String" /> that specifies the name of the license file.</returns>
		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x0600103B RID: 4155
		// (set) Token: 0x0600103C RID: 4156
		string LicenseFile { get; set; }

		/// <summary>Gets or sets the list of directories that is combined with the <see cref="P:System.AppDomainSetup.ApplicationBase" /> directory to probe for private assemblies.</summary>
		/// <returns>A <see cref="T:System.String" /> containing a list of directory names, where each name is separated by a semicolon.</returns>
		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x0600103D RID: 4157
		// (set) Token: 0x0600103E RID: 4158
		string PrivateBinPath { get; set; }

		/// <summary>Gets or sets the private binary directory path used to locate an application.</summary>
		/// <returns>A <see cref="T:System.String" /> containing a list of directory names, where each name is separated by a semicolon.</returns>
		// Token: 0x170001FA RID: 506
		// (get) Token: 0x0600103F RID: 4159
		// (set) Token: 0x06001040 RID: 4160
		string PrivateBinPathProbe { get; set; }

		/// <summary>Gets or sets the names of the directories containing assemblies to be shadow copied.</summary>
		/// <returns>A <see cref="T:System.String" /> containing a list of directory names, where each name is separated by a semicolon.</returns>
		// Token: 0x170001FB RID: 507
		// (get) Token: 0x06001041 RID: 4161
		// (set) Token: 0x06001042 RID: 4162
		string ShadowCopyDirectories { get; set; }

		/// <summary>Gets or sets a string that indicates whether shadow copying is turned on or off.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the value "true" to indicate that shadow copying is turned on; or "false" to indicate that shadow copying is turned off.</returns>
		// Token: 0x170001FC RID: 508
		// (get) Token: 0x06001043 RID: 4163
		// (set) Token: 0x06001044 RID: 4164
		string ShadowCopyFiles { get; set; }
	}
}
