﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Provides a mechanism for releasing unmanaged resources.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x02000176 RID: 374
	[ComVisible(true)]
	public interface IDisposable
	{
		/// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
		// Token: 0x0600105E RID: 4190
		void Dispose();
	}
}
