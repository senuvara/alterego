﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>The exception that is thrown as a wrapper around the exception thrown by the class initializer. This class cannot be inherited.</summary>
	// Token: 0x020001CE RID: 462
	[ComVisible(true)]
	[Serializable]
	public sealed class TypeInitializationException : SystemException
	{
		// Token: 0x060015AB RID: 5547 RVA: 0x00057D2F File Offset: 0x00055F2F
		private TypeInitializationException() : base(Environment.GetResourceString("Type constructor threw an exception."))
		{
			base.SetErrorCode(-2146233036);
		}

		// Token: 0x060015AC RID: 5548 RVA: 0x00057D4C File Offset: 0x00055F4C
		private TypeInitializationException(string message) : base(message)
		{
			base.SetErrorCode(-2146233036);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.TypeInitializationException" /> class with the default error message, the specified type name, and a reference to the inner exception that is the root cause of this exception.</summary>
		/// <param name="fullTypeName">The fully qualified name of the type that fails to initialize. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060015AD RID: 5549 RVA: 0x00057D60 File Offset: 0x00055F60
		public TypeInitializationException(string fullTypeName, Exception innerException) : base(Environment.GetResourceString("The type initializer for '{0}' threw an exception.", new object[]
		{
			fullTypeName
		}), innerException)
		{
			this._typeName = fullTypeName;
			base.SetErrorCode(-2146233036);
		}

		// Token: 0x060015AE RID: 5550 RVA: 0x00057D8F File Offset: 0x00055F8F
		internal TypeInitializationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this._typeName = info.GetString("TypeName");
		}

		/// <summary>Gets the fully qualified name of the type that fails to initialize.</summary>
		/// <returns>The fully qualified name of the type that fails to initialize.</returns>
		// Token: 0x1700029F RID: 671
		// (get) Token: 0x060015AF RID: 5551 RVA: 0x00057DAA File Offset: 0x00055FAA
		public string TypeName
		{
			get
			{
				if (this._typeName == null)
				{
					return string.Empty;
				}
				return this._typeName;
			}
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the type name and additional exception information.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		// Token: 0x060015B0 RID: 5552 RVA: 0x00057DC0 File Offset: 0x00055FC0
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("TypeName", this.TypeName, typeof(string));
		}

		// Token: 0x04000B7B RID: 2939
		private string _typeName;
	}
}
