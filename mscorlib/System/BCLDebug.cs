﻿using System;
using System.Diagnostics;

namespace System
{
	// Token: 0x020001E0 RID: 480
	internal static class BCLDebug
	{
		// Token: 0x0600170B RID: 5899 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("_DEBUG")]
		public static void Assert(bool condition, string message)
		{
		}

		// Token: 0x0600170C RID: 5900 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("_DEBUG")]
		internal static void Correctness(bool expr, string msg)
		{
		}

		// Token: 0x0600170D RID: 5901 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("_DEBUG")]
		public static void Log(string message)
		{
		}

		// Token: 0x0600170E RID: 5902 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("_DEBUG")]
		public static void Log(string switchName, string message)
		{
		}

		// Token: 0x0600170F RID: 5903 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("_DEBUG")]
		public static void Log(string switchName, LogLevel level, params object[] messages)
		{
		}

		// Token: 0x06001710 RID: 5904 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("_DEBUG")]
		internal static void Perf(bool expr, string msg)
		{
		}

		// Token: 0x06001711 RID: 5905 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("_LOGGING")]
		public static void Trace(string switchName, params object[] messages)
		{
		}

		// Token: 0x06001712 RID: 5906 RVA: 0x00002526 File Offset: 0x00000726
		internal static bool CheckEnabled(string switchName)
		{
			return false;
		}
	}
}
