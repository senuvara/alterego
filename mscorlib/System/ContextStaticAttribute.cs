﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates that the value of a static field is unique for a particular context.</summary>
	// Token: 0x02000132 RID: 306
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public class ContextStaticAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ContextStaticAttribute" /> class.</summary>
		// Token: 0x06000B6C RID: 2924 RVA: 0x000020BF File Offset: 0x000002BF
		public ContextStaticAttribute()
		{
		}
	}
}
