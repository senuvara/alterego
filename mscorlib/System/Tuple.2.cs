﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace System
{
	/// <summary>Represents a 1-tuple, or singleton. </summary>
	/// <typeparam name="T1">The type of the tuple's only component.</typeparam>
	// Token: 0x020000D9 RID: 217
	[Serializable]
	public class Tuple<T1> : IStructuralEquatable, IStructuralComparable, IComparable, ITupleInternal, ITuple
	{
		/// <summary>Gets the value of the <see cref="T:System.Tuple`1" /> object's single component. </summary>
		/// <returns>The value of the current <see cref="T:System.Tuple`1" /> object's single component.</returns>
		// Token: 0x1700014B RID: 331
		// (get) Token: 0x06000883 RID: 2179 RVA: 0x0002E8CB File Offset: 0x0002CACB
		public T1 Item1
		{
			get
			{
				return this.m_Item1;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Tuple`1" /> class.</summary>
		/// <param name="item1">The value of the tuple's only component.</param>
		// Token: 0x06000884 RID: 2180 RVA: 0x0002E8D3 File Offset: 0x0002CAD3
		public Tuple(T1 item1)
		{
			this.m_Item1 = item1;
		}

		/// <summary>Returns a value that indicates whether the current <see cref="T:System.Tuple`1" /> object is equal to a specified object.</summary>
		/// <param name="obj">The object to compare with this instance.</param>
		/// <returns>
		///     <see langword="true" /> if the current instance is equal to the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000885 RID: 2181 RVA: 0x0002E8E2 File Offset: 0x0002CAE2
		public override bool Equals(object obj)
		{
			return ((IStructuralEquatable)this).Equals(obj, ObjectEqualityComparer.Default);
		}

		/// <summary>Returns a value that indicates whether the current <see cref="T:System.Tuple`1" /> object is equal to a specified object based on a specified comparison method.</summary>
		/// <param name="other">The object to compare with this instance.</param>
		/// <param name="comparer">An object that defines the method to use to evaluate whether the two objects are equal.</param>
		/// <returns>
		///     <see langword="true" /> if the current instance is equal to the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000886 RID: 2182 RVA: 0x0002E8F0 File Offset: 0x0002CAF0
		bool IStructuralEquatable.Equals(object other, IEqualityComparer comparer)
		{
			if (other == null)
			{
				return false;
			}
			Tuple<T1> tuple = other as Tuple<T1>;
			return tuple != null && comparer.Equals(this.m_Item1, tuple.m_Item1);
		}

		/// <summary>Compares the current <see cref="T:System.Tuple`1" /> object to a specified object, and returns an integer that indicates whether the current object is before, after, or in the same position as the specified object in the sort order.</summary>
		/// <param name="obj">An object to compare with the current instance.</param>
		/// <returns>A signed integer that indicates the relative position of this instance and <paramref name="obj" /> in the sort order, as shown in the following table.ValueDescriptionA negative integerThis instance precedes <paramref name="obj" />.ZeroThis instance and <paramref name="obj" /> have the same position in the sort order.A positive integerThis instance follows <paramref name="obj" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="obj" /> is not a <see cref="T:System.Tuple`1" /> object.</exception>
		// Token: 0x06000887 RID: 2183 RVA: 0x0002E92A File Offset: 0x0002CB2A
		int IComparable.CompareTo(object obj)
		{
			return ((IStructuralComparable)this).CompareTo(obj, LowLevelComparer.Default);
		}

		/// <summary>Compares the current <see cref="T:System.Tuple`1" /> object to a specified object by using a specified comparer, and returns an integer that indicates whether the current object is before, after, or in the same position as the specified object in the sort order.</summary>
		/// <param name="other">An object to compare with the current instance.</param>
		/// <param name="comparer">An object that provides custom rules for comparison.</param>
		/// <returns>A signed integer that indicates the relative position of this instance and <paramref name="other" /> in the sort order, as shown in the following table.ValueDescriptionA negative integerThis instance precedes <paramref name="other" />.ZeroThis instance and <paramref name="other" /> have the same position in the sort order.A positive integerThis instance follows <paramref name="other" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="other" /> is not a <see cref="T:System.Tuple`1" /> object.</exception>
		// Token: 0x06000888 RID: 2184 RVA: 0x0002E938 File Offset: 0x0002CB38
		int IStructuralComparable.CompareTo(object other, IComparer comparer)
		{
			if (other == null)
			{
				return 1;
			}
			Tuple<T1> tuple = other as Tuple<T1>;
			if (tuple == null)
			{
				throw new ArgumentException(SR.Format("Argument must be of type {0}.", base.GetType().ToString()), "other");
			}
			return comparer.Compare(this.m_Item1, tuple.m_Item1);
		}

		/// <summary>Returns the hash code for the current <see cref="T:System.Tuple`1" /> object.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000889 RID: 2185 RVA: 0x0002E990 File Offset: 0x0002CB90
		public override int GetHashCode()
		{
			return ((IStructuralEquatable)this).GetHashCode(ObjectEqualityComparer.Default);
		}

		/// <summary>Calculates the hash code for the current <see cref="T:System.Tuple`1" /> object by using a specified computation method.</summary>
		/// <param name="comparer">An object whose <see cref="M:System.Collections.IEqualityComparer.GetHashCode(System.Object)" />  method calculates the hash code of the current <see cref="T:System.Tuple`1" /> object.</param>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x0600088A RID: 2186 RVA: 0x0002E99D File Offset: 0x0002CB9D
		int IStructuralEquatable.GetHashCode(IEqualityComparer comparer)
		{
			return comparer.GetHashCode(this.m_Item1);
		}

		// Token: 0x0600088B RID: 2187 RVA: 0x0002E9B0 File Offset: 0x0002CBB0
		int ITupleInternal.GetHashCode(IEqualityComparer comparer)
		{
			return ((IStructuralEquatable)this).GetHashCode(comparer);
		}

		/// <summary>Returns a string that represents the value of this <see cref="T:System.Tuple`1" /> instance.</summary>
		/// <returns>The string representation of this <see cref="T:System.Tuple`1" /> object.</returns>
		// Token: 0x0600088C RID: 2188 RVA: 0x0002E9BC File Offset: 0x0002CBBC
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("(");
			return ((ITupleInternal)this).ToString(stringBuilder);
		}

		// Token: 0x0600088D RID: 2189 RVA: 0x0002E9E2 File Offset: 0x0002CBE2
		string ITupleInternal.ToString(StringBuilder sb)
		{
			sb.Append(this.m_Item1);
			sb.Append(')');
			return sb.ToString();
		}

		/// <summary>Gets the number of elements in the <see langword="Tuple" />. </summary>
		/// <returns>1, the number of elements in a <see cref="T:System.Tuple`1" /> object. </returns>
		// Token: 0x1700014C RID: 332
		// (get) Token: 0x0600088E RID: 2190 RVA: 0x00004E08 File Offset: 0x00003008
		int ITuple.Length
		{
			get
			{
				return 1;
			}
		}

		/// <summary>Gets the value of the <see langword="Tuple" /> element. </summary>
		/// <param name="index">The index of the <see langword="Tuple" /> element. <paramref name="index" /> must be 0. </param>
		/// <returns>The value of the <see langword="Tuple" /> element. </returns>
		/// <exception cref="T:System.IndexOutOfRangeException">
		///   <paramref name="index" /> is less than 0 or greater than 0. </exception>
		// Token: 0x1700014D RID: 333
		object ITuple.this[int index]
		{
			get
			{
				if (index != 0)
				{
					throw new IndexOutOfRangeException();
				}
				return this.Item1;
			}
		}

		// Token: 0x040006A8 RID: 1704
		private readonly T1 m_Item1;
	}
}
