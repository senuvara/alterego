﻿using System;

namespace System
{
	// Token: 0x020001A5 RID: 421
	internal enum TypeNameFormatFlags
	{
		// Token: 0x04000A68 RID: 2664
		FormatBasic,
		// Token: 0x04000A69 RID: 2665
		FormatNamespace,
		// Token: 0x04000A6A RID: 2666
		FormatFullInst,
		// Token: 0x04000A6B RID: 2667
		FormatAssembly = 4,
		// Token: 0x04000A6C RID: 2668
		FormatSignature = 8,
		// Token: 0x04000A6D RID: 2669
		FormatNoVersion = 16,
		// Token: 0x04000A6E RID: 2670
		FormatAngleBrackets = 64,
		// Token: 0x04000A6F RID: 2671
		FormatStubInfo = 128,
		// Token: 0x04000A70 RID: 2672
		FormatGenericParam = 256,
		// Token: 0x04000A71 RID: 2673
		FormatSerialization = 259
	}
}
