﻿using System;

namespace System
{
	// Token: 0x020001BA RID: 442
	internal enum ExceptionResource
	{
		// Token: 0x04000AD3 RID: 2771
		Argument_ImplementIComparable,
		// Token: 0x04000AD4 RID: 2772
		Argument_InvalidType,
		// Token: 0x04000AD5 RID: 2773
		Argument_InvalidArgumentForComparison,
		// Token: 0x04000AD6 RID: 2774
		Argument_InvalidRegistryKeyPermissionCheck,
		// Token: 0x04000AD7 RID: 2775
		ArgumentOutOfRange_NeedNonNegNum,
		// Token: 0x04000AD8 RID: 2776
		Arg_ArrayPlusOffTooSmall,
		// Token: 0x04000AD9 RID: 2777
		Arg_NonZeroLowerBound,
		// Token: 0x04000ADA RID: 2778
		Arg_RankMultiDimNotSupported,
		// Token: 0x04000ADB RID: 2779
		Arg_RegKeyDelHive,
		// Token: 0x04000ADC RID: 2780
		Arg_RegKeyStrLenBug,
		// Token: 0x04000ADD RID: 2781
		Arg_RegSetStrArrNull,
		// Token: 0x04000ADE RID: 2782
		Arg_RegSetMismatchedKind,
		// Token: 0x04000ADF RID: 2783
		Arg_RegSubKeyAbsent,
		// Token: 0x04000AE0 RID: 2784
		Arg_RegSubKeyValueAbsent,
		// Token: 0x04000AE1 RID: 2785
		Argument_AddingDuplicate,
		// Token: 0x04000AE2 RID: 2786
		Serialization_InvalidOnDeser,
		// Token: 0x04000AE3 RID: 2787
		Serialization_MissingKeys,
		// Token: 0x04000AE4 RID: 2788
		Serialization_NullKey,
		// Token: 0x04000AE5 RID: 2789
		Argument_InvalidArrayType,
		// Token: 0x04000AE6 RID: 2790
		NotSupported_KeyCollectionSet,
		// Token: 0x04000AE7 RID: 2791
		NotSupported_ValueCollectionSet,
		// Token: 0x04000AE8 RID: 2792
		ArgumentOutOfRange_SmallCapacity,
		// Token: 0x04000AE9 RID: 2793
		ArgumentOutOfRange_Index,
		// Token: 0x04000AEA RID: 2794
		Argument_InvalidOffLen,
		// Token: 0x04000AEB RID: 2795
		Argument_ItemNotExist,
		// Token: 0x04000AEC RID: 2796
		ArgumentOutOfRange_Count,
		// Token: 0x04000AED RID: 2797
		ArgumentOutOfRange_InvalidThreshold,
		// Token: 0x04000AEE RID: 2798
		ArgumentOutOfRange_ListInsert,
		// Token: 0x04000AEF RID: 2799
		NotSupported_ReadOnlyCollection,
		// Token: 0x04000AF0 RID: 2800
		InvalidOperation_CannotRemoveFromStackOrQueue,
		// Token: 0x04000AF1 RID: 2801
		InvalidOperation_EmptyQueue,
		// Token: 0x04000AF2 RID: 2802
		InvalidOperation_EnumOpCantHappen,
		// Token: 0x04000AF3 RID: 2803
		InvalidOperation_EnumFailedVersion,
		// Token: 0x04000AF4 RID: 2804
		InvalidOperation_EmptyStack,
		// Token: 0x04000AF5 RID: 2805
		ArgumentOutOfRange_BiggerThanCollection,
		// Token: 0x04000AF6 RID: 2806
		InvalidOperation_EnumNotStarted,
		// Token: 0x04000AF7 RID: 2807
		InvalidOperation_EnumEnded,
		// Token: 0x04000AF8 RID: 2808
		NotSupported_SortedListNestedWrite,
		// Token: 0x04000AF9 RID: 2809
		InvalidOperation_NoValue,
		// Token: 0x04000AFA RID: 2810
		InvalidOperation_RegRemoveSubKey,
		// Token: 0x04000AFB RID: 2811
		Security_RegistryPermission,
		// Token: 0x04000AFC RID: 2812
		UnauthorizedAccess_RegistryNoWrite,
		// Token: 0x04000AFD RID: 2813
		ObjectDisposed_RegKeyClosed,
		// Token: 0x04000AFE RID: 2814
		NotSupported_InComparableType,
		// Token: 0x04000AFF RID: 2815
		Argument_InvalidRegistryOptionsCheck,
		// Token: 0x04000B00 RID: 2816
		Argument_InvalidRegistryViewCheck
	}
}
