﻿using System;

namespace System
{
	// Token: 0x020001FB RID: 507
	internal sealed class DelegateData
	{
		// Token: 0x0600189E RID: 6302 RVA: 0x00002050 File Offset: 0x00000250
		public DelegateData()
		{
		}

		// Token: 0x04000C60 RID: 3168
		public Type target_type;

		// Token: 0x04000C61 RID: 3169
		public string method_name;

		// Token: 0x04000C62 RID: 3170
		public bool curried_first_arg;
	}
}
