﻿using System;

namespace System
{
	// Token: 0x02000237 RID: 567
	internal class TypeIdentifiers
	{
		// Token: 0x06001AE3 RID: 6883 RVA: 0x00065D9B File Offset: 0x00063F9B
		internal static TypeIdentifier FromDisplay(string displayName)
		{
			return new TypeIdentifiers.Display(displayName);
		}

		// Token: 0x06001AE4 RID: 6884 RVA: 0x00065DA3 File Offset: 0x00063FA3
		internal static TypeIdentifier FromInternal(string internalName)
		{
			return new TypeIdentifiers.Internal(internalName);
		}

		// Token: 0x06001AE5 RID: 6885 RVA: 0x00065DAB File Offset: 0x00063FAB
		internal static TypeIdentifier FromInternal(string internalNameSpace, TypeIdentifier typeName)
		{
			return new TypeIdentifiers.Internal(internalNameSpace, typeName);
		}

		// Token: 0x06001AE6 RID: 6886 RVA: 0x00065DB4 File Offset: 0x00063FB4
		internal static TypeIdentifier WithoutEscape(string simpleName)
		{
			return new TypeIdentifiers.NoEscape(simpleName);
		}

		// Token: 0x06001AE7 RID: 6887 RVA: 0x00002050 File Offset: 0x00000250
		public TypeIdentifiers()
		{
		}

		// Token: 0x02000238 RID: 568
		private class Display : TypeNames.ATypeName, TypeIdentifier, TypeName, IEquatable<TypeName>
		{
			// Token: 0x06001AE8 RID: 6888 RVA: 0x00065DBC File Offset: 0x00063FBC
			internal Display(string displayName)
			{
				this.displayName = displayName;
				this.internal_name = null;
			}

			// Token: 0x1700039E RID: 926
			// (get) Token: 0x06001AE9 RID: 6889 RVA: 0x00065DD2 File Offset: 0x00063FD2
			public override string DisplayName
			{
				get
				{
					return this.displayName;
				}
			}

			// Token: 0x1700039F RID: 927
			// (get) Token: 0x06001AEA RID: 6890 RVA: 0x00065DDA File Offset: 0x00063FDA
			public string InternalName
			{
				get
				{
					if (this.internal_name == null)
					{
						this.internal_name = this.GetInternalName();
					}
					return this.internal_name;
				}
			}

			// Token: 0x06001AEB RID: 6891 RVA: 0x00065DF6 File Offset: 0x00063FF6
			private string GetInternalName()
			{
				return TypeSpec.UnescapeInternalName(this.displayName);
			}

			// Token: 0x06001AEC RID: 6892 RVA: 0x00065E03 File Offset: 0x00064003
			public override TypeName NestedName(TypeIdentifier innerName)
			{
				return TypeNames.FromDisplay(this.DisplayName + "+" + innerName.DisplayName);
			}

			// Token: 0x04000F1F RID: 3871
			private string displayName;

			// Token: 0x04000F20 RID: 3872
			private string internal_name;
		}

		// Token: 0x02000239 RID: 569
		private class Internal : TypeNames.ATypeName, TypeIdentifier, TypeName, IEquatable<TypeName>
		{
			// Token: 0x06001AED RID: 6893 RVA: 0x00065E20 File Offset: 0x00064020
			internal Internal(string internalName)
			{
				this.internalName = internalName;
				this.display_name = null;
			}

			// Token: 0x06001AEE RID: 6894 RVA: 0x00065E36 File Offset: 0x00064036
			internal Internal(string nameSpaceInternal, TypeIdentifier typeName)
			{
				this.internalName = nameSpaceInternal + "." + typeName.InternalName;
				this.display_name = null;
			}

			// Token: 0x170003A0 RID: 928
			// (get) Token: 0x06001AEF RID: 6895 RVA: 0x00065E5C File Offset: 0x0006405C
			public override string DisplayName
			{
				get
				{
					if (this.display_name == null)
					{
						this.display_name = this.GetDisplayName();
					}
					return this.display_name;
				}
			}

			// Token: 0x170003A1 RID: 929
			// (get) Token: 0x06001AF0 RID: 6896 RVA: 0x00065E78 File Offset: 0x00064078
			public string InternalName
			{
				get
				{
					return this.internalName;
				}
			}

			// Token: 0x06001AF1 RID: 6897 RVA: 0x00065E80 File Offset: 0x00064080
			private string GetDisplayName()
			{
				return TypeSpec.EscapeDisplayName(this.internalName);
			}

			// Token: 0x06001AF2 RID: 6898 RVA: 0x00065E03 File Offset: 0x00064003
			public override TypeName NestedName(TypeIdentifier innerName)
			{
				return TypeNames.FromDisplay(this.DisplayName + "+" + innerName.DisplayName);
			}

			// Token: 0x04000F21 RID: 3873
			private string internalName;

			// Token: 0x04000F22 RID: 3874
			private string display_name;
		}

		// Token: 0x0200023A RID: 570
		private class NoEscape : TypeNames.ATypeName, TypeIdentifier, TypeName, IEquatable<TypeName>
		{
			// Token: 0x06001AF3 RID: 6899 RVA: 0x00065E8D File Offset: 0x0006408D
			internal NoEscape(string simpleName)
			{
				this.simpleName = simpleName;
			}

			// Token: 0x170003A2 RID: 930
			// (get) Token: 0x06001AF4 RID: 6900 RVA: 0x00065E9C File Offset: 0x0006409C
			public override string DisplayName
			{
				get
				{
					return this.simpleName;
				}
			}

			// Token: 0x170003A3 RID: 931
			// (get) Token: 0x06001AF5 RID: 6901 RVA: 0x00065E9C File Offset: 0x0006409C
			public string InternalName
			{
				get
				{
					return this.simpleName;
				}
			}

			// Token: 0x06001AF6 RID: 6902 RVA: 0x00065E03 File Offset: 0x00064003
			public override TypeName NestedName(TypeIdentifier innerName)
			{
				return TypeNames.FromDisplay(this.DisplayName + "+" + innerName.DisplayName);
			}

			// Token: 0x04000F23 RID: 3875
			private string simpleName;
		}
	}
}
