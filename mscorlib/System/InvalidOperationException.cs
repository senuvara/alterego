﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when a method call is invalid for the object's current state.</summary>
	// Token: 0x02000181 RID: 385
	[ComVisible(true)]
	[Serializable]
	public class InvalidOperationException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidOperationException" /> class.</summary>
		// Token: 0x060010D3 RID: 4307 RVA: 0x00047414 File Offset: 0x00045614
		public InvalidOperationException() : base(Environment.GetResourceString("Operation is not valid due to the current state of the object."))
		{
			base.SetErrorCode(-2146233079);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidOperationException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x060010D4 RID: 4308 RVA: 0x00047431 File Offset: 0x00045631
		public InvalidOperationException(string message) : base(message)
		{
			base.SetErrorCode(-2146233079);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidOperationException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060010D5 RID: 4309 RVA: 0x00047445 File Offset: 0x00045645
		public InvalidOperationException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233079);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidOperationException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x060010D6 RID: 4310 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected InvalidOperationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
