﻿using System;

namespace System
{
	/// <summary>Specifies combinations of modifier and console keys that can interrupt the current process.</summary>
	// Token: 0x0200012F RID: 303
	[Serializable]
	public enum ConsoleSpecialKey
	{
		/// <summary>The <see cref="F:System.ConsoleModifiers.Control" /> modifier key plus the <see cref="F:System.ConsoleKey.C" /> console key.</summary>
		// Token: 0x0400084B RID: 2123
		ControlC,
		/// <summary>The <see cref="F:System.ConsoleModifiers.Control" /> modifier key plus the BREAK console key.</summary>
		// Token: 0x0400084C RID: 2124
		ControlBreak
	}
}
