﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Supports a value type that can be assigned <see langword="null" />. This class cannot be inherited.</summary>
	// Token: 0x02000217 RID: 535
	[ComVisible(true)]
	public static class Nullable
	{
		/// <summary>Compares the relative values of two <see cref="T:System.Nullable`1" /> objects.</summary>
		/// <param name="n1">A <see cref="T:System.Nullable`1" /> object.</param>
		/// <param name="n2">A <see cref="T:System.Nullable`1" /> object.</param>
		/// <typeparam name="T">The underlying value type of the <paramref name="n1" /> and <paramref name="n2" /> parameters.</typeparam>
		/// <returns>An integer that indicates the relative values of the <paramref name="n1" /> and <paramref name="n2" /> parameters.Return ValueDescriptionLess than zeroThe <see cref="P:System.Nullable`1.HasValue" /> property for <paramref name="n1" /> is <see langword="false" />, and the <see cref="P:System.Nullable`1.HasValue" /> property for <paramref name="n2" /> is <see langword="true" />.-or-The <see cref="P:System.Nullable`1.HasValue" /> properties for <paramref name="n1" /> and <paramref name="n2" /> are <see langword="true" />, and the value of the <see cref="P:System.Nullable`1.Value" /> property for <paramref name="n1" /> is less than the value of the <see cref="P:System.Nullable`1.Value" /> property for <paramref name="n2" />.ZeroThe <see cref="P:System.Nullable`1.HasValue" /> properties for <paramref name="n1" /> and <paramref name="n2" /> are <see langword="false" />.-or-The <see cref="P:System.Nullable`1.HasValue" /> properties for <paramref name="n1" /> and <paramref name="n2" /> are <see langword="true" />, and the value of the <see cref="P:System.Nullable`1.Value" /> property for <paramref name="n1" /> is equal to the value of the <see cref="P:System.Nullable`1.Value" /> property for <paramref name="n2" />.Greater than zeroThe <see cref="P:System.Nullable`1.HasValue" /> property for <paramref name="n1" /> is <see langword="true" />, and the <see cref="P:System.Nullable`1.HasValue" /> property for <paramref name="n2" /> is <see langword="false" />.-or-The <see cref="P:System.Nullable`1.HasValue" /> properties for <paramref name="n1" /> and <paramref name="n2" /> are <see langword="true" />, and the value of the <see cref="P:System.Nullable`1.Value" /> property for <paramref name="n1" /> is greater than the value of the <see cref="P:System.Nullable`1.Value" /> property for <paramref name="n2" />.</returns>
		// Token: 0x06001989 RID: 6537 RVA: 0x0005F26C File Offset: 0x0005D46C
		[ComVisible(false)]
		public static int Compare<T>(T? n1, T? n2) where T : struct
		{
			if (n1.has_value)
			{
				if (!n2.has_value)
				{
					return 1;
				}
				return Comparer<T>.Default.Compare(n1.value, n2.value);
			}
			else
			{
				if (!n2.has_value)
				{
					return 0;
				}
				return -1;
			}
		}

		/// <summary>Indicates whether two specified <see cref="T:System.Nullable`1" /> objects are equal.</summary>
		/// <param name="n1">A <see cref="T:System.Nullable`1" /> object.</param>
		/// <param name="n2">A <see cref="T:System.Nullable`1" /> object.</param>
		/// <typeparam name="T">The underlying value type of the <paramref name="n1" /> and <paramref name="n2" /> parameters.</typeparam>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="n1" /> parameter is equal to the <paramref name="n2" /> parameter; otherwise, <see langword="false" />. The return value depends on the <see cref="P:System.Nullable`1.HasValue" /> and <see cref="P:System.Nullable`1.Value" /> properties of the two parameters that are compared.Return ValueDescription
		///             <see langword="true" />
		///           The <see cref="P:System.Nullable`1.HasValue" /> properties for <paramref name="n1" /> and <paramref name="n2" /> are <see langword="false" />. -or-The <see cref="P:System.Nullable`1.HasValue" /> properties for <paramref name="n1" /> and <paramref name="n2" /> are <see langword="true" />, and the <see cref="P:System.Nullable`1.Value" /> properties of the parameters are equal.
		///             <see langword="false" />
		///           The <see cref="P:System.Nullable`1.HasValue" /> property is <see langword="true" /> for one parameter and <see langword="false" /> for the other parameter.-or-The <see cref="P:System.Nullable`1.HasValue" /> properties for <paramref name="n1" /> and <paramref name="n2" /> are <see langword="true" />, and the <see cref="P:System.Nullable`1.Value" /> properties of the parameters are unequal.</returns>
		// Token: 0x0600198A RID: 6538 RVA: 0x0005F2A2 File Offset: 0x0005D4A2
		[ComVisible(false)]
		public static bool Equals<T>(T? n1, T? n2) where T : struct
		{
			return n1.has_value == n2.has_value && (!n1.has_value || EqualityComparer<T>.Default.Equals(n1.value, n2.value));
		}

		/// <summary>Returns the underlying type argument of the specified nullable type.</summary>
		/// <param name="nullableType">A <see cref="T:System.Type" /> object that describes a closed generic nullable type. </param>
		/// <returns>The type argument of the <paramref name="nullableType" /> parameter, if the <paramref name="nullableType" /> parameter is a closed generic nullable type; otherwise, <see langword="null" />. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="nullableType" /> is <see langword="null" />.</exception>
		// Token: 0x0600198B RID: 6539 RVA: 0x0005F2D4 File Offset: 0x0005D4D4
		public static Type GetUnderlyingType(Type nullableType)
		{
			if (nullableType == null)
			{
				throw new ArgumentNullException("nullableType");
			}
			if (!nullableType.IsGenericType || nullableType.IsGenericTypeDefinition || !(nullableType.GetGenericTypeDefinition() == typeof(Nullable<>)))
			{
				return null;
			}
			return nullableType.GetGenericArguments()[0];
		}
	}
}
