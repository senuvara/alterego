﻿using System;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when a method attempts to use a type that it does not have access to.</summary>
	// Token: 0x020001CC RID: 460
	[Serializable]
	public class TypeAccessException : TypeLoadException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.TypeAccessException" /> class with a system-supplied message that describes the error.</summary>
		// Token: 0x0600159D RID: 5533 RVA: 0x00057B16 File Offset: 0x00055D16
		public TypeAccessException() : base(Environment.GetResourceString("Attempt to access the type failed."))
		{
			base.SetErrorCode(-2146233021);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.TypeAccessException" /> class with a specified message that describes the error.</summary>
		/// <param name="message">The message that describes the exception. The caller of this constructor must ensure that this string has been localized for the current system culture.</param>
		// Token: 0x0600159E RID: 5534 RVA: 0x00057B33 File Offset: 0x00055D33
		public TypeAccessException(string message) : base(message)
		{
			base.SetErrorCode(-2146233021);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.TypeAccessException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The message that describes the exception. The caller of this constructor must ensure that this string has been localized for the current system culture. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x0600159F RID: 5535 RVA: 0x00057B47 File Offset: 0x00055D47
		public TypeAccessException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233021);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.TypeAccessException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x060015A0 RID: 5536 RVA: 0x00057B5C File Offset: 0x00055D5C
		protected TypeAccessException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			base.SetErrorCode(-2146233021);
		}
	}
}
