﻿using System;

namespace System
{
	// Token: 0x02000216 RID: 534
	internal class NullConsoleDriver : IConsoleDriver
	{
		// Token: 0x17000359 RID: 857
		// (get) Token: 0x0600195A RID: 6490 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x0600195B RID: 6491 RVA: 0x000020D3 File Offset: 0x000002D3
		public ConsoleColor BackgroundColor
		{
			get
			{
				return ConsoleColor.Black;
			}
			set
			{
			}
		}

		// Token: 0x1700035A RID: 858
		// (get) Token: 0x0600195C RID: 6492 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x0600195D RID: 6493 RVA: 0x000020D3 File Offset: 0x000002D3
		public int BufferHeight
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x0600195E RID: 6494 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x0600195F RID: 6495 RVA: 0x000020D3 File Offset: 0x000002D3
		public int BufferWidth
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06001960 RID: 6496 RVA: 0x00002526 File Offset: 0x00000726
		public bool CapsLock
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06001961 RID: 6497 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06001962 RID: 6498 RVA: 0x000020D3 File Offset: 0x000002D3
		public int CursorLeft
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06001963 RID: 6499 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06001964 RID: 6500 RVA: 0x000020D3 File Offset: 0x000002D3
		public int CursorSize
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06001965 RID: 6501 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06001966 RID: 6502 RVA: 0x000020D3 File Offset: 0x000002D3
		public int CursorTop
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06001967 RID: 6503 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06001968 RID: 6504 RVA: 0x000020D3 File Offset: 0x000002D3
		public bool CursorVisible
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x17000361 RID: 865
		// (get) Token: 0x06001969 RID: 6505 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x0600196A RID: 6506 RVA: 0x000020D3 File Offset: 0x000002D3
		public ConsoleColor ForegroundColor
		{
			get
			{
				return ConsoleColor.Black;
			}
			set
			{
			}
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x0600196B RID: 6507 RVA: 0x00002526 File Offset: 0x00000726
		public bool KeyAvailable
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x0600196C RID: 6508 RVA: 0x00004E08 File Offset: 0x00003008
		public bool Initialized
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x0600196D RID: 6509 RVA: 0x00002526 File Offset: 0x00000726
		public int LargestWindowHeight
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x0600196E RID: 6510 RVA: 0x00002526 File Offset: 0x00000726
		public int LargestWindowWidth
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x0600196F RID: 6511 RVA: 0x00002526 File Offset: 0x00000726
		public bool NumberLock
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06001970 RID: 6512 RVA: 0x0005F24D File Offset: 0x0005D44D
		// (set) Token: 0x06001971 RID: 6513 RVA: 0x000020D3 File Offset: 0x000002D3
		public string Title
		{
			get
			{
				return "";
			}
			set
			{
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06001972 RID: 6514 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06001973 RID: 6515 RVA: 0x000020D3 File Offset: 0x000002D3
		public bool TreatControlCAsInput
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06001974 RID: 6516 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06001975 RID: 6517 RVA: 0x000020D3 File Offset: 0x000002D3
		public int WindowHeight
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x06001976 RID: 6518 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06001977 RID: 6519 RVA: 0x000020D3 File Offset: 0x000002D3
		public int WindowLeft
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06001978 RID: 6520 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06001979 RID: 6521 RVA: 0x000020D3 File Offset: 0x000002D3
		public int WindowTop
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x0600197A RID: 6522 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x0600197B RID: 6523 RVA: 0x000020D3 File Offset: 0x000002D3
		public int WindowWidth
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x0600197C RID: 6524 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Beep(int frequency, int duration)
		{
		}

		// Token: 0x0600197D RID: 6525 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Clear()
		{
		}

		// Token: 0x0600197E RID: 6526 RVA: 0x000020D3 File Offset: 0x000002D3
		public void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop, char sourceChar, ConsoleColor sourceForeColor, ConsoleColor sourceBackColor)
		{
		}

		// Token: 0x0600197F RID: 6527 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Init()
		{
		}

		// Token: 0x06001980 RID: 6528 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public string ReadLine()
		{
			return null;
		}

		// Token: 0x06001981 RID: 6529 RVA: 0x0005F254 File Offset: 0x0005D454
		public ConsoleKeyInfo ReadKey(bool intercept)
		{
			return NullConsoleDriver.EmptyConsoleKeyInfo;
		}

		// Token: 0x06001982 RID: 6530 RVA: 0x000020D3 File Offset: 0x000002D3
		public void ResetColor()
		{
		}

		// Token: 0x06001983 RID: 6531 RVA: 0x000020D3 File Offset: 0x000002D3
		public void SetBufferSize(int width, int height)
		{
		}

		// Token: 0x06001984 RID: 6532 RVA: 0x000020D3 File Offset: 0x000002D3
		public void SetCursorPosition(int left, int top)
		{
		}

		// Token: 0x06001985 RID: 6533 RVA: 0x000020D3 File Offset: 0x000002D3
		public void SetWindowPosition(int left, int top)
		{
		}

		// Token: 0x06001986 RID: 6534 RVA: 0x000020D3 File Offset: 0x000002D3
		public void SetWindowSize(int width, int height)
		{
		}

		// Token: 0x06001987 RID: 6535 RVA: 0x00002050 File Offset: 0x00000250
		public NullConsoleDriver()
		{
		}

		// Token: 0x06001988 RID: 6536 RVA: 0x0005F25B File Offset: 0x0005D45B
		// Note: this type is marked as 'beforefieldinit'.
		static NullConsoleDriver()
		{
		}

		// Token: 0x04000C9D RID: 3229
		private static readonly ConsoleKeyInfo EmptyConsoleKeyInfo = new ConsoleKeyInfo('\0', (ConsoleKey)0, false, false, false);
	}
}
