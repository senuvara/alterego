﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Text;

namespace System
{
	// Token: 0x020000D7 RID: 215
	internal interface ITupleInternal : ITuple
	{
		// Token: 0x06000872 RID: 2162
		string ToString(StringBuilder sb);

		// Token: 0x06000873 RID: 2163
		int GetHashCode(IEqualityComparer comparer);
	}
}
