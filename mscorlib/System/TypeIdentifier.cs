﻿using System;

namespace System
{
	// Token: 0x02000233 RID: 563
	internal interface TypeIdentifier : TypeName, IEquatable<TypeName>
	{
		// Token: 0x1700039B RID: 923
		// (get) Token: 0x06001AD7 RID: 6871
		string InternalName { get; }
	}
}
