﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	// Token: 0x02000143 RID: 323
	[Serializable]
	internal sealed class Empty : ISerializable
	{
		// Token: 0x06000E6A RID: 3690 RVA: 0x00002050 File Offset: 0x00000250
		private Empty()
		{
		}

		// Token: 0x06000E6B RID: 3691 RVA: 0x00039F28 File Offset: 0x00038128
		public override string ToString()
		{
			return string.Empty;
		}

		// Token: 0x06000E6C RID: 3692 RVA: 0x0003C8F3 File Offset: 0x0003AAF3
		[SecurityCritical]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			UnitySerializationHolder.GetUnitySerializationInfo(info, 1, null, null);
		}

		// Token: 0x06000E6D RID: 3693 RVA: 0x0003C90C File Offset: 0x0003AB0C
		// Note: this type is marked as 'beforefieldinit'.
		static Empty()
		{
		}

		// Token: 0x040008B8 RID: 2232
		public static readonly Empty Value = new Empty();
	}
}
