﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>Serves as the base class for application-defined exceptions.</summary>
	// Token: 0x02000113 RID: 275
	[ComVisible(true)]
	[Serializable]
	public class ApplicationException : Exception
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ApplicationException" /> class.</summary>
		// Token: 0x060009F5 RID: 2549 RVA: 0x00031FAA File Offset: 0x000301AA
		public ApplicationException() : base(Environment.GetResourceString("Error in the application."))
		{
			base.SetErrorCode(-2146232832);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ApplicationException" /> class with a specified error message.</summary>
		/// <param name="message">A message that describes the error. </param>
		// Token: 0x060009F6 RID: 2550 RVA: 0x00031FC7 File Offset: 0x000301C7
		public ApplicationException(string message) : base(message)
		{
			base.SetErrorCode(-2146232832);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ApplicationException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060009F7 RID: 2551 RVA: 0x00031FDB File Offset: 0x000301DB
		public ApplicationException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146232832);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ApplicationException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x060009F8 RID: 2552 RVA: 0x00031FF0 File Offset: 0x000301F0
		protected ApplicationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
