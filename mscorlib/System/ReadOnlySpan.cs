﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x020000BB RID: 187
	[DebuggerTypeProxy(typeof(SpanDebugView<>))]
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	public readonly ref struct ReadOnlySpan<T>
	{
		// Token: 0x0600064D RID: 1613 RVA: 0x000224F7 File Offset: 0x000206F7
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ReadOnlySpan(T[] array)
		{
			if (array == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
			}
			this._length = array.Length;
			this._pinnable = Unsafe.As<Pinnable<T>>(array);
			this._byteOffset = SpanHelpers.PerTypeValues<T>.ArrayAdjustment;
		}

		// Token: 0x0600064E RID: 1614 RVA: 0x00022524 File Offset: 0x00020724
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ReadOnlySpan(T[] array, int start, int length)
		{
			if (array == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
			}
			if (start > array.Length || length > array.Length - start)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			this._length = length;
			this._pinnable = Unsafe.As<Pinnable<T>>(array);
			this._byteOffset = SpanHelpers.PerTypeValues<T>.ArrayAdjustment.Add(start);
		}

		// Token: 0x0600064F RID: 1615 RVA: 0x00022573 File Offset: 0x00020773
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public unsafe ReadOnlySpan(void* pointer, int length)
		{
			if (SpanHelpers.IsReferenceOrContainsReferences<T>())
			{
				ThrowHelper.ThrowArgumentException_InvalidTypeWithPointersNotSupported(typeof(T));
			}
			if (length < 0)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			this._length = length;
			this._pinnable = null;
			this._byteOffset = new IntPtr(pointer);
		}

		// Token: 0x06000650 RID: 1616 RVA: 0x000225B0 File Offset: 0x000207B0
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ReadOnlySpan<T> DangerousCreate(object obj, ref T objectData, int length)
		{
			Pinnable<T> pinnable = Unsafe.As<Pinnable<T>>(obj);
			IntPtr byteOffset = Unsafe.ByteOffset<T>(ref pinnable.Data, ref objectData);
			return new ReadOnlySpan<T>(pinnable, byteOffset, length);
		}

		// Token: 0x06000651 RID: 1617 RVA: 0x000225D7 File Offset: 0x000207D7
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal ReadOnlySpan(Pinnable<T> pinnable, IntPtr byteOffset, int length)
		{
			this._length = length;
			this._pinnable = pinnable;
			this._byteOffset = byteOffset;
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06000652 RID: 1618 RVA: 0x000225EE File Offset: 0x000207EE
		private string DebuggerDisplay
		{
			get
			{
				return string.Format("{{{0}[{1}]}}", typeof(T).Name, this._length);
			}
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x06000653 RID: 1619 RVA: 0x00022614 File Offset: 0x00020814
		public int Length
		{
			get
			{
				return this._length;
			}
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x06000654 RID: 1620 RVA: 0x0002261C File Offset: 0x0002081C
		public bool IsEmpty
		{
			get
			{
				return this._length == 0;
			}
		}

		// Token: 0x1700011E RID: 286
		public unsafe T this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if (index >= this._length)
				{
					ThrowHelper.ThrowIndexOutOfRangeException();
				}
				if (this._pinnable == null)
				{
					return *Unsafe.Add<T>(Unsafe.AsRef<T>(this._byteOffset.ToPointer()), index);
				}
				return *Unsafe.Add<T>(Unsafe.AddByteOffset<T>(ref this._pinnable.Data, this._byteOffset), index);
			}
		}

		// Token: 0x06000656 RID: 1622 RVA: 0x0002268B File Offset: 0x0002088B
		public void CopyTo(Span<T> destination)
		{
			if (!this.TryCopyTo(destination))
			{
				ThrowHelper.ThrowArgumentException_DestinationTooShort();
			}
		}

		// Token: 0x06000657 RID: 1623 RVA: 0x0002269C File Offset: 0x0002089C
		public bool TryCopyTo(Span<T> destination)
		{
			int length = this._length;
			int length2 = destination.Length;
			if (length == 0)
			{
				return true;
			}
			if (length > length2)
			{
				return false;
			}
			ref T src = ref this.DangerousGetPinnableReference();
			SpanHelpers.CopyTo<T>(destination.DangerousGetPinnableReference(), length2, ref src, length);
			return true;
		}

		// Token: 0x06000658 RID: 1624 RVA: 0x000226DA File Offset: 0x000208DA
		public static bool operator ==(ReadOnlySpan<T> left, ReadOnlySpan<T> right)
		{
			return left._length == right._length && Unsafe.AreSame<T>(left.DangerousGetPinnableReference(), right.DangerousGetPinnableReference());
		}

		// Token: 0x06000659 RID: 1625 RVA: 0x000226FF File Offset: 0x000208FF
		public static bool operator !=(ReadOnlySpan<T> left, ReadOnlySpan<T> right)
		{
			return !(left == right);
		}

		// Token: 0x0600065A RID: 1626 RVA: 0x0002270B File Offset: 0x0002090B
		[Obsolete("Equals() on Span will always throw an exception. Use == instead.")]
		public override bool Equals(object obj)
		{
			throw new NotSupportedException("Equals() on Span and ReadOnlySpan is not supported. Use operator== instead.");
		}

		// Token: 0x0600065B RID: 1627 RVA: 0x00022717 File Offset: 0x00020917
		[Obsolete("GetHashCode() on Span will always throw an exception.")]
		public override int GetHashCode()
		{
			throw new NotSupportedException("GetHashCode() on Span and ReadOnlySpan is not supported.");
		}

		// Token: 0x0600065C RID: 1628 RVA: 0x00022723 File Offset: 0x00020923
		public static implicit operator ReadOnlySpan<T>(T[] array)
		{
			return new ReadOnlySpan<T>(array);
		}

		// Token: 0x0600065D RID: 1629 RVA: 0x0002272B File Offset: 0x0002092B
		public static implicit operator ReadOnlySpan<T>(ArraySegment<T> arraySegment)
		{
			return new ReadOnlySpan<T>(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
		}

		// Token: 0x0600065E RID: 1630 RVA: 0x00022748 File Offset: 0x00020948
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ReadOnlySpan<T> Slice(int start)
		{
			if (start > this._length)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			IntPtr byteOffset = this._byteOffset.Add(start);
			int length = this._length - start;
			return new ReadOnlySpan<T>(this._pinnable, byteOffset, length);
		}

		// Token: 0x0600065F RID: 1631 RVA: 0x00022788 File Offset: 0x00020988
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ReadOnlySpan<T> Slice(int start, int length)
		{
			if (start > this._length || length > this._length - start)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			IntPtr byteOffset = this._byteOffset.Add(start);
			return new ReadOnlySpan<T>(this._pinnable, byteOffset, length);
		}

		// Token: 0x06000660 RID: 1632 RVA: 0x000227CC File Offset: 0x000209CC
		public T[] ToArray()
		{
			if (this._length == 0)
			{
				return SpanHelpers.PerTypeValues<T>.EmptyArray;
			}
			T[] array = new T[this._length];
			this.CopyTo(array);
			return array;
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000661 RID: 1633 RVA: 0x00022800 File Offset: 0x00020A00
		public static ReadOnlySpan<T> Empty
		{
			get
			{
				return default(ReadOnlySpan<T>);
			}
		}

		// Token: 0x06000662 RID: 1634 RVA: 0x00022818 File Offset: 0x00020A18
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ref T DangerousGetPinnableReference()
		{
			if (this._pinnable == null)
			{
				return Unsafe.AsRef<T>(this._byteOffset.ToPointer());
			}
			return Unsafe.AddByteOffset<T>(ref this._pinnable.Data, this._byteOffset);
		}

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000663 RID: 1635 RVA: 0x00022857 File Offset: 0x00020A57
		internal Pinnable<T> Pinnable
		{
			get
			{
				return this._pinnable;
			}
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000664 RID: 1636 RVA: 0x0002285F File Offset: 0x00020A5F
		internal IntPtr ByteOffset
		{
			get
			{
				return this._byteOffset;
			}
		}

		// Token: 0x04000668 RID: 1640
		private readonly Pinnable<T> _pinnable;

		// Token: 0x04000669 RID: 1641
		private readonly IntPtr _byteOffset;

		// Token: 0x0400066A RID: 1642
		private readonly int _length;
	}
}
