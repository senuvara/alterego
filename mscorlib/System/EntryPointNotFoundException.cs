﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when an attempt to load a class fails due to the absence of an entry method.</summary>
	// Token: 0x02000144 RID: 324
	[ComVisible(true)]
	[Serializable]
	public class EntryPointNotFoundException : TypeLoadException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EntryPointNotFoundException" /> class.</summary>
		// Token: 0x06000E6E RID: 3694 RVA: 0x0003C918 File Offset: 0x0003AB18
		public EntryPointNotFoundException() : base(Environment.GetResourceString("Entry point was not found."))
		{
			base.SetErrorCode(-2146233053);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EntryPointNotFoundException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x06000E6F RID: 3695 RVA: 0x0003C935 File Offset: 0x0003AB35
		public EntryPointNotFoundException(string message) : base(message)
		{
			base.SetErrorCode(-2146233053);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EntryPointNotFoundException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000E70 RID: 3696 RVA: 0x0003C949 File Offset: 0x0003AB49
		public EntryPointNotFoundException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233053);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.EntryPointNotFoundException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06000E71 RID: 3697 RVA: 0x0003C480 File Offset: 0x0003A680
		protected EntryPointNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
