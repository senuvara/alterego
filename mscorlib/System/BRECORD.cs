﻿using System;

namespace System
{
	// Token: 0x02000245 RID: 581
	internal struct BRECORD
	{
		// Token: 0x04000F4B RID: 3915
		private IntPtr pvRecord;

		// Token: 0x04000F4C RID: 3916
		private IntPtr pRecInfo;
	}
}
