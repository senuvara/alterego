﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000244 RID: 580
	[StructLayout(LayoutKind.Explicit)]
	internal struct Variant
	{
		// Token: 0x06001B3F RID: 6975 RVA: 0x00066E54 File Offset: 0x00065054
		public void SetValue(object obj)
		{
			this.vt = 0;
			if (obj == null)
			{
				return;
			}
			Type type = obj.GetType();
			if (type.IsEnum)
			{
				type = Enum.GetUnderlyingType(type);
			}
			if (type == typeof(sbyte))
			{
				this.vt = 16;
				this.cVal = (sbyte)obj;
				return;
			}
			if (type == typeof(byte))
			{
				this.vt = 17;
				this.bVal = (byte)obj;
				return;
			}
			if (type == typeof(short))
			{
				this.vt = 2;
				this.iVal = (short)obj;
				return;
			}
			if (type == typeof(ushort))
			{
				this.vt = 18;
				this.uiVal = (ushort)obj;
				return;
			}
			if (type == typeof(int))
			{
				this.vt = 3;
				this.lVal = (int)obj;
				return;
			}
			if (type == typeof(uint))
			{
				this.vt = 19;
				this.ulVal = (uint)obj;
				return;
			}
			if (type == typeof(long))
			{
				this.vt = 20;
				this.llVal = (long)obj;
				return;
			}
			if (type == typeof(ulong))
			{
				this.vt = 21;
				this.ullVal = (ulong)obj;
				return;
			}
			if (type == typeof(float))
			{
				this.vt = 4;
				this.fltVal = (float)obj;
				return;
			}
			if (type == typeof(double))
			{
				this.vt = 5;
				this.dblVal = (double)obj;
				return;
			}
			if (type == typeof(string))
			{
				this.vt = 8;
				this.bstrVal = Marshal.StringToBSTR((string)obj);
				return;
			}
			if (type == typeof(bool))
			{
				this.vt = 11;
				this.lVal = (((bool)obj) ? -1 : 0);
				return;
			}
			if (type == typeof(BStrWrapper))
			{
				this.vt = 8;
				this.bstrVal = Marshal.StringToBSTR(((BStrWrapper)obj).WrappedObject);
				return;
			}
			throw new NotImplementedException(string.Format("Variant couldn't handle object of type {0}", obj.GetType()));
		}

		// Token: 0x06001B40 RID: 6976 RVA: 0x000670A4 File Offset: 0x000652A4
		public static object GetValueAt(int vt, IntPtr addr)
		{
			object result = null;
			switch (vt)
			{
			case 2:
				result = Marshal.ReadInt16(addr);
				break;
			case 3:
				result = Marshal.ReadInt32(addr);
				break;
			case 4:
				result = Marshal.PtrToStructure(addr, typeof(float));
				break;
			case 5:
				result = Marshal.PtrToStructure(addr, typeof(double));
				break;
			case 8:
				result = Marshal.PtrToStringBSTR(Marshal.ReadIntPtr(addr));
				break;
			case 9:
			case 13:
			{
				IntPtr intPtr = Marshal.ReadIntPtr(addr);
				if (intPtr != IntPtr.Zero)
				{
					result = Marshal.GetObjectForIUnknown(intPtr);
				}
				break;
			}
			case 11:
				result = (Marshal.ReadInt16(addr) != 0);
				break;
			case 16:
				result = (sbyte)Marshal.ReadByte(addr);
				break;
			case 17:
				result = Marshal.ReadByte(addr);
				break;
			case 18:
				result = (ushort)Marshal.ReadInt16(addr);
				break;
			case 19:
				result = (uint)Marshal.ReadInt32(addr);
				break;
			case 20:
				result = Marshal.ReadInt64(addr);
				break;
			case 21:
				result = (ulong)Marshal.ReadInt64(addr);
				break;
			}
			return result;
		}

		// Token: 0x06001B41 RID: 6977 RVA: 0x000671F4 File Offset: 0x000653F4
		public object GetValue()
		{
			object result = null;
			switch (this.vt)
			{
			case 2:
				return this.iVal;
			case 3:
				return this.lVal;
			case 4:
				return this.fltVal;
			case 5:
				return this.dblVal;
			case 8:
				return Marshal.PtrToStringBSTR(this.bstrVal);
			case 11:
				return this.boolVal != 0;
			case 16:
				return this.cVal;
			case 17:
				return this.bVal;
			case 18:
				return this.uiVal;
			case 19:
				return this.ulVal;
			case 20:
				return this.llVal;
			case 21:
				return this.ullVal;
			}
			if ((this.vt & 16384) == 16384 && this.pdispVal != IntPtr.Zero)
			{
				result = Variant.GetValueAt((int)(this.vt & -16385), this.pdispVal);
			}
			return result;
		}

		// Token: 0x06001B42 RID: 6978 RVA: 0x00067368 File Offset: 0x00065568
		public void Clear()
		{
			if (this.vt == 8)
			{
				Marshal.FreeBSTR(this.bstrVal);
				return;
			}
			if ((this.vt == 9 || this.vt == 13) && this.pdispVal != IntPtr.Zero)
			{
				Marshal.Release(this.pdispVal);
			}
		}

		// Token: 0x04000F37 RID: 3895
		[FieldOffset(0)]
		public short vt;

		// Token: 0x04000F38 RID: 3896
		[FieldOffset(2)]
		public ushort wReserved1;

		// Token: 0x04000F39 RID: 3897
		[FieldOffset(4)]
		public ushort wReserved2;

		// Token: 0x04000F3A RID: 3898
		[FieldOffset(6)]
		public ushort wReserved3;

		// Token: 0x04000F3B RID: 3899
		[FieldOffset(8)]
		public long llVal;

		// Token: 0x04000F3C RID: 3900
		[FieldOffset(8)]
		public int lVal;

		// Token: 0x04000F3D RID: 3901
		[FieldOffset(8)]
		public byte bVal;

		// Token: 0x04000F3E RID: 3902
		[FieldOffset(8)]
		public short iVal;

		// Token: 0x04000F3F RID: 3903
		[FieldOffset(8)]
		public float fltVal;

		// Token: 0x04000F40 RID: 3904
		[FieldOffset(8)]
		public double dblVal;

		// Token: 0x04000F41 RID: 3905
		[FieldOffset(8)]
		public short boolVal;

		// Token: 0x04000F42 RID: 3906
		[FieldOffset(8)]
		public IntPtr bstrVal;

		// Token: 0x04000F43 RID: 3907
		[FieldOffset(8)]
		public sbyte cVal;

		// Token: 0x04000F44 RID: 3908
		[FieldOffset(8)]
		public ushort uiVal;

		// Token: 0x04000F45 RID: 3909
		[FieldOffset(8)]
		public uint ulVal;

		// Token: 0x04000F46 RID: 3910
		[FieldOffset(8)]
		public ulong ullVal;

		// Token: 0x04000F47 RID: 3911
		[FieldOffset(8)]
		public int intVal;

		// Token: 0x04000F48 RID: 3912
		[FieldOffset(8)]
		public uint uintVal;

		// Token: 0x04000F49 RID: 3913
		[FieldOffset(8)]
		public IntPtr pdispVal;

		// Token: 0x04000F4A RID: 3914
		[FieldOffset(8)]
		public BRECORD bRecord;
	}
}
