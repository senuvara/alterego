﻿using System;

namespace System.Globalization
{
	// Token: 0x020003CE RID: 974
	internal enum FORMATFLAGS
	{
		// Token: 0x040017FE RID: 6142
		None,
		// Token: 0x040017FF RID: 6143
		UseGenitiveMonth,
		// Token: 0x04001800 RID: 6144
		UseLeapYearMonth,
		// Token: 0x04001801 RID: 6145
		UseSpacesInMonthNames = 4,
		// Token: 0x04001802 RID: 6146
		UseHebrewParsing = 8,
		// Token: 0x04001803 RID: 6147
		UseSpacesInDayNames = 16,
		// Token: 0x04001804 RID: 6148
		UseDigitPrefixInTokens = 32
	}
}
