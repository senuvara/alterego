﻿using System;

namespace System.Globalization
{
	// Token: 0x0200040F RID: 1039
	internal class Punycode : Bootstring
	{
		// Token: 0x060030E6 RID: 12518 RVA: 0x000B36A4 File Offset: 0x000B18A4
		public Punycode() : base('-', 36, 1, 26, 38, 700, 72, 128)
		{
		}
	}
}
