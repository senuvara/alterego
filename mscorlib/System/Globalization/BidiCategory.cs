﻿using System;

namespace System.Globalization
{
	// Token: 0x020003C0 RID: 960
	[Serializable]
	internal enum BidiCategory
	{
		// Token: 0x04001700 RID: 5888
		LeftToRight,
		// Token: 0x04001701 RID: 5889
		LeftToRightEmbedding,
		// Token: 0x04001702 RID: 5890
		LeftToRightOverride,
		// Token: 0x04001703 RID: 5891
		RightToLeft,
		// Token: 0x04001704 RID: 5892
		RightToLeftArabic,
		// Token: 0x04001705 RID: 5893
		RightToLeftEmbedding,
		// Token: 0x04001706 RID: 5894
		RightToLeftOverride,
		// Token: 0x04001707 RID: 5895
		PopDirectionalFormat,
		// Token: 0x04001708 RID: 5896
		EuropeanNumber,
		// Token: 0x04001709 RID: 5897
		EuropeanNumberSeparator,
		// Token: 0x0400170A RID: 5898
		EuropeanNumberTerminator,
		// Token: 0x0400170B RID: 5899
		ArabicNumber,
		// Token: 0x0400170C RID: 5900
		CommonNumberSeparator,
		// Token: 0x0400170D RID: 5901
		NonSpacingMark,
		// Token: 0x0400170E RID: 5902
		BoundaryNeutral,
		// Token: 0x0400170F RID: 5903
		ParagraphSeparator,
		// Token: 0x04001710 RID: 5904
		SegmentSeparator,
		// Token: 0x04001711 RID: 5905
		Whitespace,
		// Token: 0x04001712 RID: 5906
		OtherNeutrals,
		// Token: 0x04001713 RID: 5907
		LeftToRightIsolate,
		// Token: 0x04001714 RID: 5908
		RightToLeftIsolate,
		// Token: 0x04001715 RID: 5909
		FirstStrongIsolate,
		// Token: 0x04001716 RID: 5910
		PopDirectionIsolate
	}
}
