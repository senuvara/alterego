﻿using System;

namespace System.Globalization
{
	// Token: 0x020003CB RID: 971
	[Flags]
	internal enum DateTimeFormatFlags
	{
		// Token: 0x0400179E RID: 6046
		None = 0,
		// Token: 0x0400179F RID: 6047
		UseGenitiveMonth = 1,
		// Token: 0x040017A0 RID: 6048
		UseLeapYearMonth = 2,
		// Token: 0x040017A1 RID: 6049
		UseSpacesInMonthNames = 4,
		// Token: 0x040017A2 RID: 6050
		UseHebrewRule = 8,
		// Token: 0x040017A3 RID: 6051
		UseSpacesInDayNames = 16,
		// Token: 0x040017A4 RID: 6052
		UseDigitPrefixInTokens = 32,
		// Token: 0x040017A5 RID: 6053
		NotInitialized = -1
	}
}
