﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	/// <summary>Specifies whether a calendar is solar-based, lunar-based, or lunisolar-based.</summary>
	// Token: 0x020003C2 RID: 962
	[ComVisible(true)]
	public enum CalendarAlgorithmType
	{
		/// <summary>An unknown calendar basis.</summary>
		// Token: 0x04001742 RID: 5954
		Unknown,
		/// <summary>A solar-based calendar.</summary>
		// Token: 0x04001743 RID: 5955
		SolarCalendar,
		/// <summary>A lunar-based calendar.</summary>
		// Token: 0x04001744 RID: 5956
		LunarCalendar,
		/// <summary>A lunisolar-based calendar.</summary>
		// Token: 0x04001745 RID: 5957
		LunisolarCalendar
	}
}
