﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Unity;

namespace System.Globalization
{
	/// <summary>Enumerates the text elements of a string. </summary>
	// Token: 0x020003EF RID: 1007
	[ComVisible(true)]
	[Serializable]
	public class TextElementEnumerator : IEnumerator
	{
		// Token: 0x06002F60 RID: 12128 RVA: 0x000A70AD File Offset: 0x000A52AD
		internal TextElementEnumerator(string str, int startIndex, int strLen)
		{
			this.str = str;
			this.startIndex = startIndex;
			this.strLen = strLen;
			this.Reset();
		}

		// Token: 0x06002F61 RID: 12129 RVA: 0x000A70D0 File Offset: 0x000A52D0
		[OnDeserializing]
		private void OnDeserializing(StreamingContext ctx)
		{
			this.charLen = -1;
		}

		// Token: 0x06002F62 RID: 12130 RVA: 0x000A70DC File Offset: 0x000A52DC
		[OnDeserialized]
		private void OnDeserialized(StreamingContext ctx)
		{
			this.strLen = this.endIndex + 1;
			this.currTextElementLen = this.nextTextElementLen;
			if (this.charLen == -1)
			{
				this.uc = CharUnicodeInfo.InternalGetUnicodeCategory(this.str, this.index, out this.charLen);
			}
		}

		// Token: 0x06002F63 RID: 12131 RVA: 0x000A7129 File Offset: 0x000A5329
		[OnSerializing]
		private void OnSerializing(StreamingContext ctx)
		{
			this.endIndex = this.strLen - 1;
			this.nextTextElementLen = this.currTextElementLen;
		}

		/// <summary>Advances the enumerator to the next text element of the string.</summary>
		/// <returns>
		///     <see langword="true" /> if the enumerator was successfully advanced to the next text element; <see langword="false" /> if the enumerator has passed the end of the string.</returns>
		// Token: 0x06002F64 RID: 12132 RVA: 0x000A7148 File Offset: 0x000A5348
		public bool MoveNext()
		{
			if (this.index >= this.strLen)
			{
				this.index = this.strLen + 1;
				return false;
			}
			this.currTextElementLen = StringInfo.GetCurrentTextElementLen(this.str, this.index, this.strLen, ref this.uc, ref this.charLen);
			this.index += this.currTextElementLen;
			return true;
		}

		/// <summary>Gets the current text element in the string.</summary>
		/// <returns>An object containing the current text element in the string.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first text element of the string or after the last text element. </exception>
		// Token: 0x1700078C RID: 1932
		// (get) Token: 0x06002F65 RID: 12133 RVA: 0x000A71B0 File Offset: 0x000A53B0
		public object Current
		{
			get
			{
				return this.GetTextElement();
			}
		}

		/// <summary>Gets the current text element in the string.</summary>
		/// <returns>A new string containing the current text element in the string being read.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first text element of the string or after the last text element. </exception>
		// Token: 0x06002F66 RID: 12134 RVA: 0x000A71B8 File Offset: 0x000A53B8
		public string GetTextElement()
		{
			if (this.index == this.startIndex)
			{
				throw new InvalidOperationException(Environment.GetResourceString("Enumeration has not started. Call MoveNext."));
			}
			if (this.index > this.strLen)
			{
				throw new InvalidOperationException(Environment.GetResourceString("Enumeration already finished."));
			}
			return this.str.Substring(this.index - this.currTextElementLen, this.currTextElementLen);
		}

		/// <summary>Gets the index of the text element that the enumerator is currently positioned over.</summary>
		/// <returns>The index of the text element that the enumerator is currently positioned over.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first text element of the string or after the last text element. </exception>
		// Token: 0x1700078D RID: 1933
		// (get) Token: 0x06002F67 RID: 12135 RVA: 0x000A721F File Offset: 0x000A541F
		public int ElementIndex
		{
			get
			{
				if (this.index == this.startIndex)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has not started. Call MoveNext."));
				}
				return this.index - this.currTextElementLen;
			}
		}

		/// <summary>Sets the enumerator to its initial position, which is before the first text element in the string.</summary>
		// Token: 0x06002F68 RID: 12136 RVA: 0x000A724C File Offset: 0x000A544C
		public void Reset()
		{
			this.index = this.startIndex;
			if (this.index < this.strLen)
			{
				this.uc = CharUnicodeInfo.InternalGetUnicodeCategory(this.str, this.index, out this.charLen);
			}
		}

		// Token: 0x06002F69 RID: 12137 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal TextElementEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001955 RID: 6485
		private string str;

		// Token: 0x04001956 RID: 6486
		private int index;

		// Token: 0x04001957 RID: 6487
		private int startIndex;

		// Token: 0x04001958 RID: 6488
		[NonSerialized]
		private int strLen;

		// Token: 0x04001959 RID: 6489
		[NonSerialized]
		private int currTextElementLen;

		// Token: 0x0400195A RID: 6490
		[OptionalField(VersionAdded = 2)]
		private UnicodeCategory uc;

		// Token: 0x0400195B RID: 6491
		[OptionalField(VersionAdded = 2)]
		private int charLen;

		// Token: 0x0400195C RID: 6492
		private int endIndex;

		// Token: 0x0400195D RID: 6493
		private int nextTextElementLen;
	}
}
