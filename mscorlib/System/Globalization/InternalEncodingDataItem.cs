﻿using System;
using System.Security;

namespace System.Globalization
{
	// Token: 0x02000407 RID: 1031
	internal struct InternalEncodingDataItem
	{
		// Token: 0x04001A1A RID: 6682
		[SecurityCritical]
		internal string webName;

		// Token: 0x04001A1B RID: 6683
		internal ushort codePage;
	}
}
