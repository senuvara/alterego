﻿using System;

namespace System.Globalization
{
	// Token: 0x020003DD RID: 989
	internal struct HebrewNumberParsingContext
	{
		// Token: 0x06002E20 RID: 11808 RVA: 0x000A3EB0 File Offset: 0x000A20B0
		public HebrewNumberParsingContext(int result)
		{
			this.state = HebrewNumber.HS.Start;
			this.result = result;
		}

		// Token: 0x0400189E RID: 6302
		internal HebrewNumber.HS state;

		// Token: 0x0400189F RID: 6303
		internal int result;
	}
}
