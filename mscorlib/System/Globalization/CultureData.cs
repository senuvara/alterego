﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace System.Globalization
{
	// Token: 0x02000404 RID: 1028
	[StructLayout(LayoutKind.Sequential)]
	internal class CultureData
	{
		// Token: 0x06003033 RID: 12339 RVA: 0x000ABA89 File Offset: 0x000A9C89
		private CultureData(string name)
		{
			this.sRealName = name;
		}

		// Token: 0x170007B6 RID: 1974
		// (get) Token: 0x06003034 RID: 12340 RVA: 0x000ABA98 File Offset: 0x000A9C98
		public static CultureData Invariant
		{
			get
			{
				if (CultureData.s_Invariant == null)
				{
					CultureData cultureData = new CultureData("");
					cultureData.sISO639Language = "iv";
					cultureData.sAM1159 = "AM";
					cultureData.sPM2359 = "PM";
					cultureData.sTimeSeparator = ":";
					cultureData.saLongTimes = new string[]
					{
						"HH:mm:ss"
					};
					cultureData.saShortTimes = new string[]
					{
						"HH:mm",
						"hh:mm tt",
						"H:mm",
						"h:mm tt"
					};
					cultureData.iFirstDayOfWeek = 0;
					cultureData.iFirstWeekOfYear = 0;
					cultureData.waCalendars = new int[]
					{
						1
					};
					cultureData.calendars = new CalendarData[23];
					cultureData.calendars[0] = CalendarData.Invariant;
					cultureData.iDefaultAnsiCodePage = 1252;
					cultureData.iDefaultOemCodePage = 437;
					cultureData.iDefaultMacCodePage = 10000;
					cultureData.iDefaultEbcdicCodePage = 37;
					cultureData.sListSeparator = ",";
					Interlocked.CompareExchange<CultureData>(ref CultureData.s_Invariant, cultureData, null);
				}
				return CultureData.s_Invariant;
			}
		}

		// Token: 0x06003035 RID: 12341 RVA: 0x000ABBAC File Offset: 0x000A9DAC
		public static CultureData GetCultureData(string cultureName, bool useUserOverride)
		{
			CultureData result;
			try
			{
				result = new CultureInfo(cultureName, useUserOverride).m_cultureData;
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06003036 RID: 12342 RVA: 0x000ABBE0 File Offset: 0x000A9DE0
		public static CultureData GetCultureData(string cultureName, bool useUserOverride, int datetimeIndex, int calendarId, int numberIndex, string iso2lang, int ansiCodePage, int oemCodePage, int macCodePage, int ebcdicCodePage, bool rightToLeft, string listSeparator)
		{
			if (string.IsNullOrEmpty(cultureName))
			{
				return CultureData.Invariant;
			}
			CultureData cultureData = new CultureData(cultureName);
			cultureData.fill_culture_data(datetimeIndex);
			cultureData.bUseOverrides = useUserOverride;
			cultureData.calendarId = calendarId;
			cultureData.numberIndex = numberIndex;
			cultureData.sISO639Language = iso2lang;
			cultureData.iDefaultAnsiCodePage = ansiCodePage;
			cultureData.iDefaultOemCodePage = oemCodePage;
			cultureData.iDefaultMacCodePage = macCodePage;
			cultureData.iDefaultEbcdicCodePage = ebcdicCodePage;
			cultureData.isRightToLeft = rightToLeft;
			cultureData.sListSeparator = listSeparator;
			return cultureData;
		}

		// Token: 0x06003037 RID: 12343 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		internal static CultureData GetCultureData(int culture, bool bUseUserOverride)
		{
			return null;
		}

		// Token: 0x06003038 RID: 12344
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void fill_culture_data(int datetimeIndex);

		// Token: 0x06003039 RID: 12345 RVA: 0x000ABC58 File Offset: 0x000A9E58
		public CalendarData GetCalendar(int calendarId)
		{
			int num = calendarId - 1;
			if (this.calendars == null)
			{
				this.calendars = new CalendarData[23];
			}
			CalendarData calendarData = this.calendars[num];
			if (calendarData == null)
			{
				calendarData = new CalendarData(this.sRealName, calendarId, this.bUseOverrides);
				this.calendars[num] = calendarData;
			}
			return calendarData;
		}

		// Token: 0x170007B7 RID: 1975
		// (get) Token: 0x0600303A RID: 12346 RVA: 0x000ABCA7 File Offset: 0x000A9EA7
		internal string[] LongTimes
		{
			get
			{
				return this.saLongTimes;
			}
		}

		// Token: 0x170007B8 RID: 1976
		// (get) Token: 0x0600303B RID: 12347 RVA: 0x000ABCB1 File Offset: 0x000A9EB1
		internal string[] ShortTimes
		{
			get
			{
				return this.saShortTimes;
			}
		}

		// Token: 0x170007B9 RID: 1977
		// (get) Token: 0x0600303C RID: 12348 RVA: 0x000ABCBB File Offset: 0x000A9EBB
		internal string SISO639LANGNAME
		{
			get
			{
				return this.sISO639Language;
			}
		}

		// Token: 0x170007BA RID: 1978
		// (get) Token: 0x0600303D RID: 12349 RVA: 0x000ABCC3 File Offset: 0x000A9EC3
		internal int IFIRSTDAYOFWEEK
		{
			get
			{
				return this.iFirstDayOfWeek;
			}
		}

		// Token: 0x170007BB RID: 1979
		// (get) Token: 0x0600303E RID: 12350 RVA: 0x000ABCCB File Offset: 0x000A9ECB
		internal int IFIRSTWEEKOFYEAR
		{
			get
			{
				return this.iFirstWeekOfYear;
			}
		}

		// Token: 0x170007BC RID: 1980
		// (get) Token: 0x0600303F RID: 12351 RVA: 0x000ABCD3 File Offset: 0x000A9ED3
		internal string SAM1159
		{
			get
			{
				return this.sAM1159;
			}
		}

		// Token: 0x170007BD RID: 1981
		// (get) Token: 0x06003040 RID: 12352 RVA: 0x000ABCDB File Offset: 0x000A9EDB
		internal string SPM2359
		{
			get
			{
				return this.sPM2359;
			}
		}

		// Token: 0x170007BE RID: 1982
		// (get) Token: 0x06003041 RID: 12353 RVA: 0x000ABCE3 File Offset: 0x000A9EE3
		internal string TimeSeparator
		{
			get
			{
				return this.sTimeSeparator;
			}
		}

		// Token: 0x170007BF RID: 1983
		// (get) Token: 0x06003042 RID: 12354 RVA: 0x000ABCEC File Offset: 0x000A9EEC
		internal int[] CalendarIds
		{
			get
			{
				if (this.waCalendars == null)
				{
					string a = this.sISO639Language;
					if (!(a == "ja"))
					{
						if (!(a == "zh"))
						{
							this.waCalendars = new int[]
							{
								this.calendarId
							};
						}
						else
						{
							this.waCalendars = new int[]
							{
								this.calendarId,
								4
							};
						}
					}
					else
					{
						this.waCalendars = new int[]
						{
							this.calendarId,
							3
						};
					}
				}
				return this.waCalendars;
			}
		}

		// Token: 0x170007C0 RID: 1984
		// (get) Token: 0x06003043 RID: 12355 RVA: 0x000ABD7F File Offset: 0x000A9F7F
		internal bool IsInvariantCulture
		{
			get
			{
				return string.IsNullOrEmpty(this.sRealName);
			}
		}

		// Token: 0x170007C1 RID: 1985
		// (get) Token: 0x06003044 RID: 12356 RVA: 0x000ABD8C File Offset: 0x000A9F8C
		internal string CultureName
		{
			get
			{
				return this.sRealName;
			}
		}

		// Token: 0x170007C2 RID: 1986
		// (get) Token: 0x06003045 RID: 12357 RVA: 0x0005F24D File Offset: 0x0005D44D
		internal string SCOMPAREINFO
		{
			get
			{
				return "";
			}
		}

		// Token: 0x170007C3 RID: 1987
		// (get) Token: 0x06003046 RID: 12358 RVA: 0x000ABD8C File Offset: 0x000A9F8C
		internal string STEXTINFO
		{
			get
			{
				return this.sRealName;
			}
		}

		// Token: 0x170007C4 RID: 1988
		// (get) Token: 0x06003047 RID: 12359 RVA: 0x00002526 File Offset: 0x00000726
		internal int ILANGUAGE
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x170007C5 RID: 1989
		// (get) Token: 0x06003048 RID: 12360 RVA: 0x000ABD94 File Offset: 0x000A9F94
		internal int IDEFAULTANSICODEPAGE
		{
			get
			{
				return this.iDefaultAnsiCodePage;
			}
		}

		// Token: 0x170007C6 RID: 1990
		// (get) Token: 0x06003049 RID: 12361 RVA: 0x000ABD9C File Offset: 0x000A9F9C
		internal int IDEFAULTOEMCODEPAGE
		{
			get
			{
				return this.iDefaultOemCodePage;
			}
		}

		// Token: 0x170007C7 RID: 1991
		// (get) Token: 0x0600304A RID: 12362 RVA: 0x000ABDA4 File Offset: 0x000A9FA4
		internal int IDEFAULTMACCODEPAGE
		{
			get
			{
				return this.iDefaultMacCodePage;
			}
		}

		// Token: 0x170007C8 RID: 1992
		// (get) Token: 0x0600304B RID: 12363 RVA: 0x000ABDAC File Offset: 0x000A9FAC
		internal int IDEFAULTEBCDICCODEPAGE
		{
			get
			{
				return this.iDefaultEbcdicCodePage;
			}
		}

		// Token: 0x170007C9 RID: 1993
		// (get) Token: 0x0600304C RID: 12364 RVA: 0x000ABDB4 File Offset: 0x000A9FB4
		internal bool IsRightToLeft
		{
			get
			{
				return this.isRightToLeft;
			}
		}

		// Token: 0x170007CA RID: 1994
		// (get) Token: 0x0600304D RID: 12365 RVA: 0x000ABDBC File Offset: 0x000A9FBC
		internal string SLIST
		{
			get
			{
				return this.sListSeparator;
			}
		}

		// Token: 0x170007CB RID: 1995
		// (get) Token: 0x0600304E RID: 12366 RVA: 0x000ABDC4 File Offset: 0x000A9FC4
		internal bool UseUserOverride
		{
			get
			{
				return this.bUseOverrides;
			}
		}

		// Token: 0x0600304F RID: 12367 RVA: 0x000ABDCC File Offset: 0x000A9FCC
		internal string CalendarName(int calendarId)
		{
			return this.GetCalendar(calendarId).sNativeName;
		}

		// Token: 0x06003050 RID: 12368 RVA: 0x000ABDDA File Offset: 0x000A9FDA
		internal string[] EraNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saEraNames;
		}

		// Token: 0x06003051 RID: 12369 RVA: 0x000ABDE8 File Offset: 0x000A9FE8
		internal string[] AbbrevEraNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saAbbrevEraNames;
		}

		// Token: 0x06003052 RID: 12370 RVA: 0x000ABDF6 File Offset: 0x000A9FF6
		internal string[] AbbreviatedEnglishEraNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saAbbrevEnglishEraNames;
		}

		// Token: 0x06003053 RID: 12371 RVA: 0x000ABE04 File Offset: 0x000AA004
		internal string[] ShortDates(int calendarId)
		{
			return this.GetCalendar(calendarId).saShortDates;
		}

		// Token: 0x06003054 RID: 12372 RVA: 0x000ABE12 File Offset: 0x000AA012
		internal string[] LongDates(int calendarId)
		{
			return this.GetCalendar(calendarId).saLongDates;
		}

		// Token: 0x06003055 RID: 12373 RVA: 0x000ABE20 File Offset: 0x000AA020
		internal string[] YearMonths(int calendarId)
		{
			return this.GetCalendar(calendarId).saYearMonths;
		}

		// Token: 0x06003056 RID: 12374 RVA: 0x000ABE2E File Offset: 0x000AA02E
		internal string[] DayNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saDayNames;
		}

		// Token: 0x06003057 RID: 12375 RVA: 0x000ABE3C File Offset: 0x000AA03C
		internal string[] AbbreviatedDayNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saAbbrevDayNames;
		}

		// Token: 0x06003058 RID: 12376 RVA: 0x000ABE4A File Offset: 0x000AA04A
		internal string[] SuperShortDayNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saSuperShortDayNames;
		}

		// Token: 0x06003059 RID: 12377 RVA: 0x000ABE58 File Offset: 0x000AA058
		internal string[] MonthNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saMonthNames;
		}

		// Token: 0x0600305A RID: 12378 RVA: 0x000ABE66 File Offset: 0x000AA066
		internal string[] GenitiveMonthNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saMonthGenitiveNames;
		}

		// Token: 0x0600305B RID: 12379 RVA: 0x000ABE74 File Offset: 0x000AA074
		internal string[] AbbreviatedMonthNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saAbbrevMonthNames;
		}

		// Token: 0x0600305C RID: 12380 RVA: 0x000ABE82 File Offset: 0x000AA082
		internal string[] AbbreviatedGenitiveMonthNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saAbbrevMonthGenitiveNames;
		}

		// Token: 0x0600305D RID: 12381 RVA: 0x000ABE90 File Offset: 0x000AA090
		internal string[] LeapYearMonthNames(int calendarId)
		{
			return this.GetCalendar(calendarId).saLeapYearMonthNames;
		}

		// Token: 0x0600305E RID: 12382 RVA: 0x000ABE9E File Offset: 0x000AA09E
		internal string MonthDay(int calendarId)
		{
			return this.GetCalendar(calendarId).sMonthDay;
		}

		// Token: 0x0600305F RID: 12383 RVA: 0x000ABEAC File Offset: 0x000AA0AC
		internal string DateSeparator(int calendarId)
		{
			return CultureData.GetDateSeparator(this.ShortDates(calendarId)[0]);
		}

		// Token: 0x06003060 RID: 12384 RVA: 0x000ABEBC File Offset: 0x000AA0BC
		private static string GetDateSeparator(string format)
		{
			return CultureData.GetSeparator(format, "dyM");
		}

		// Token: 0x06003061 RID: 12385 RVA: 0x000ABECC File Offset: 0x000AA0CC
		private static string GetSeparator(string format, string timeParts)
		{
			int num = CultureData.IndexOfTimePart(format, 0, timeParts);
			if (num != -1)
			{
				char c = format[num];
				do
				{
					num++;
				}
				while (num < format.Length && format[num] == c);
				int num2 = num;
				if (num2 < format.Length)
				{
					int num3 = CultureData.IndexOfTimePart(format, num2, timeParts);
					if (num3 != -1)
					{
						return CultureData.UnescapeNlsString(format, num2, num3 - 1);
					}
				}
			}
			return string.Empty;
		}

		// Token: 0x06003062 RID: 12386 RVA: 0x000ABF30 File Offset: 0x000AA130
		private static int IndexOfTimePart(string format, int startIndex, string timeParts)
		{
			bool flag = false;
			for (int i = startIndex; i < format.Length; i++)
			{
				if (!flag && timeParts.IndexOf(format[i]) != -1)
				{
					return i;
				}
				char c = format[i];
				if (c != '\'')
				{
					if (c == '\\' && i + 1 < format.Length)
					{
						i++;
						c = format[i];
						if (c != '\'' && c != '\\')
						{
							i--;
						}
					}
				}
				else
				{
					flag = !flag;
				}
			}
			return -1;
		}

		// Token: 0x06003063 RID: 12387 RVA: 0x000ABFA4 File Offset: 0x000AA1A4
		private static string UnescapeNlsString(string str, int start, int end)
		{
			StringBuilder stringBuilder = null;
			int num = start;
			while (num < str.Length && num <= end)
			{
				char c = str[num];
				if (c != '\'')
				{
					if (c != '\\')
					{
						if (stringBuilder != null)
						{
							stringBuilder.Append(str[num]);
						}
					}
					else
					{
						if (stringBuilder == null)
						{
							stringBuilder = new StringBuilder(str, start, num - start, str.Length);
						}
						num++;
						if (num < str.Length)
						{
							stringBuilder.Append(str[num]);
						}
					}
				}
				else if (stringBuilder == null)
				{
					stringBuilder = new StringBuilder(str, start, num - start, str.Length);
				}
				num++;
			}
			if (stringBuilder == null)
			{
				return str.Substring(start, end - start + 1);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06003064 RID: 12388 RVA: 0x00002058 File Offset: 0x00000258
		internal static string[] ReescapeWin32Strings(string[] array)
		{
			return array;
		}

		// Token: 0x06003065 RID: 12389 RVA: 0x00002058 File Offset: 0x00000258
		internal static string ReescapeWin32String(string str)
		{
			return str;
		}

		// Token: 0x06003066 RID: 12390 RVA: 0x00002526 File Offset: 0x00000726
		internal static bool IsCustomCultureId(int cultureId)
		{
			return false;
		}

		// Token: 0x06003067 RID: 12391 RVA: 0x000AC04C File Offset: 0x000AA24C
		internal void GetNFIValues(NumberFormatInfo nfi)
		{
			if (!this.IsInvariantCulture)
			{
				CultureData.fill_number_data(nfi, this.numberIndex);
			}
			nfi.percentDecimalDigits = nfi.numberDecimalDigits;
			nfi.percentDecimalSeparator = nfi.numberDecimalSeparator;
			nfi.percentGroupSizes = nfi.numberGroupSizes;
			nfi.percentGroupSeparator = nfi.numberGroupSeparator;
		}

		// Token: 0x06003068 RID: 12392
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void fill_number_data(NumberFormatInfo nfi, int numberIndex);

		// Token: 0x040019EB RID: 6635
		private string sAM1159;

		// Token: 0x040019EC RID: 6636
		private string sPM2359;

		// Token: 0x040019ED RID: 6637
		private string sTimeSeparator;

		// Token: 0x040019EE RID: 6638
		private volatile string[] saLongTimes;

		// Token: 0x040019EF RID: 6639
		private volatile string[] saShortTimes;

		// Token: 0x040019F0 RID: 6640
		private int iFirstDayOfWeek;

		// Token: 0x040019F1 RID: 6641
		private int iFirstWeekOfYear;

		// Token: 0x040019F2 RID: 6642
		private volatile int[] waCalendars;

		// Token: 0x040019F3 RID: 6643
		private CalendarData[] calendars;

		// Token: 0x040019F4 RID: 6644
		private string sISO639Language;

		// Token: 0x040019F5 RID: 6645
		private readonly string sRealName;

		// Token: 0x040019F6 RID: 6646
		private bool bUseOverrides;

		// Token: 0x040019F7 RID: 6647
		private int calendarId;

		// Token: 0x040019F8 RID: 6648
		private int numberIndex;

		// Token: 0x040019F9 RID: 6649
		private int iDefaultAnsiCodePage;

		// Token: 0x040019FA RID: 6650
		private int iDefaultOemCodePage;

		// Token: 0x040019FB RID: 6651
		private int iDefaultMacCodePage;

		// Token: 0x040019FC RID: 6652
		private int iDefaultEbcdicCodePage;

		// Token: 0x040019FD RID: 6653
		private bool isRightToLeft;

		// Token: 0x040019FE RID: 6654
		private string sListSeparator;

		// Token: 0x040019FF RID: 6655
		private static CultureData s_Invariant;
	}
}
