﻿using System;

namespace System.Globalization
{
	// Token: 0x020003DE RID: 990
	internal enum HebrewNumberParsingState
	{
		// Token: 0x040018A1 RID: 6305
		InvalidHebrewNumber,
		// Token: 0x040018A2 RID: 6306
		NotHebrewDigit,
		// Token: 0x040018A3 RID: 6307
		FoundEndOfHebrewNumber,
		// Token: 0x040018A4 RID: 6308
		ContinueParsing
	}
}
