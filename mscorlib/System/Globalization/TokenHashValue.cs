﻿using System;

namespace System.Globalization
{
	// Token: 0x020003CD RID: 973
	internal class TokenHashValue
	{
		// Token: 0x06002D72 RID: 11634 RVA: 0x000A0C32 File Offset: 0x0009EE32
		internal TokenHashValue(string tokenString, TokenType tokenType, int tokenValue)
		{
			this.tokenString = tokenString;
			this.tokenType = tokenType;
			this.tokenValue = tokenValue;
		}

		// Token: 0x040017FA RID: 6138
		internal string tokenString;

		// Token: 0x040017FB RID: 6139
		internal TokenType tokenType;

		// Token: 0x040017FC RID: 6140
		internal int tokenValue;
	}
}
