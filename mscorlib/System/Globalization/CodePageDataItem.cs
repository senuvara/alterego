﻿using System;
using System.Security;

namespace System.Globalization
{
	// Token: 0x02000405 RID: 1029
	[Serializable]
	internal class CodePageDataItem
	{
		// Token: 0x06003069 RID: 12393 RVA: 0x000AC09D File Offset: 0x000AA29D
		[SecurityCritical]
		internal CodePageDataItem(int dataIndex)
		{
			this.m_dataIndex = dataIndex;
			this.m_uiFamilyCodePage = (int)EncodingTable.codePageDataPtr[dataIndex].uiFamilyCodePage;
			this.m_flags = EncodingTable.codePageDataPtr[dataIndex].flags;
		}

		// Token: 0x0600306A RID: 12394 RVA: 0x000AC0D8 File Offset: 0x000AA2D8
		[SecurityCritical]
		internal static string CreateString(string pStrings, uint index)
		{
			if (pStrings[0] == '|')
			{
				return pStrings.Split(CodePageDataItem.sep, StringSplitOptions.RemoveEmptyEntries)[(int)index];
			}
			return pStrings;
		}

		// Token: 0x170007CC RID: 1996
		// (get) Token: 0x0600306B RID: 12395 RVA: 0x000AC0F5 File Offset: 0x000AA2F5
		public string WebName
		{
			[SecuritySafeCritical]
			get
			{
				if (this.m_webName == null)
				{
					this.m_webName = CodePageDataItem.CreateString(EncodingTable.codePageDataPtr[this.m_dataIndex].Names, 0U);
				}
				return this.m_webName;
			}
		}

		// Token: 0x170007CD RID: 1997
		// (get) Token: 0x0600306C RID: 12396 RVA: 0x000AC126 File Offset: 0x000AA326
		public virtual int UIFamilyCodePage
		{
			get
			{
				return this.m_uiFamilyCodePage;
			}
		}

		// Token: 0x170007CE RID: 1998
		// (get) Token: 0x0600306D RID: 12397 RVA: 0x000AC12E File Offset: 0x000AA32E
		public string HeaderName
		{
			[SecuritySafeCritical]
			get
			{
				if (this.m_headerName == null)
				{
					this.m_headerName = CodePageDataItem.CreateString(EncodingTable.codePageDataPtr[this.m_dataIndex].Names, 1U);
				}
				return this.m_headerName;
			}
		}

		// Token: 0x170007CF RID: 1999
		// (get) Token: 0x0600306E RID: 12398 RVA: 0x000AC15F File Offset: 0x000AA35F
		public string BodyName
		{
			[SecuritySafeCritical]
			get
			{
				if (this.m_bodyName == null)
				{
					this.m_bodyName = CodePageDataItem.CreateString(EncodingTable.codePageDataPtr[this.m_dataIndex].Names, 2U);
				}
				return this.m_bodyName;
			}
		}

		// Token: 0x170007D0 RID: 2000
		// (get) Token: 0x0600306F RID: 12399 RVA: 0x000AC190 File Offset: 0x000AA390
		public uint Flags
		{
			get
			{
				return this.m_flags;
			}
		}

		// Token: 0x06003070 RID: 12400 RVA: 0x000AC198 File Offset: 0x000AA398
		// Note: this type is marked as 'beforefieldinit'.
		static CodePageDataItem()
		{
		}

		// Token: 0x04001A00 RID: 6656
		internal int m_dataIndex;

		// Token: 0x04001A01 RID: 6657
		internal int m_uiFamilyCodePage;

		// Token: 0x04001A02 RID: 6658
		internal string m_webName;

		// Token: 0x04001A03 RID: 6659
		internal string m_headerName;

		// Token: 0x04001A04 RID: 6660
		internal string m_bodyName;

		// Token: 0x04001A05 RID: 6661
		internal uint m_flags;

		// Token: 0x04001A06 RID: 6662
		private static readonly char[] sep = new char[]
		{
			'|'
		};
	}
}
