﻿using System;

namespace System.Globalization
{
	// Token: 0x020003CA RID: 970
	[Flags]
	internal enum MonthNameStyles
	{
		// Token: 0x0400179A RID: 6042
		Regular = 0,
		// Token: 0x0400179B RID: 6043
		Genitive = 1,
		// Token: 0x0400179C RID: 6044
		LeapYear = 2
	}
}
