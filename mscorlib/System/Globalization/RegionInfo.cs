﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Globalization
{
	/// <summary>Contains information about the country/region.</summary>
	// Token: 0x02000410 RID: 1040
	[ComVisible(true)]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class RegionInfo
	{
		/// <summary>Gets the <see cref="T:System.Globalization.RegionInfo" /> that represents the country/region used by the current thread.</summary>
		/// <returns>The <see cref="T:System.Globalization.RegionInfo" /> that represents the country/region used by the current thread.</returns>
		// Token: 0x170007F5 RID: 2037
		// (get) Token: 0x060030E7 RID: 12519 RVA: 0x000B36CC File Offset: 0x000B18CC
		public static RegionInfo CurrentRegion
		{
			get
			{
				RegionInfo regionInfo = RegionInfo.currentRegion;
				if (regionInfo == null)
				{
					CultureInfo currentCulture = CultureInfo.CurrentCulture;
					if (currentCulture != null)
					{
						regionInfo = new RegionInfo(currentCulture);
					}
					if (Interlocked.CompareExchange<RegionInfo>(ref RegionInfo.currentRegion, regionInfo, null) != null)
					{
						regionInfo = RegionInfo.currentRegion;
					}
				}
				return regionInfo;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Globalization.RegionInfo" /> class based on the country/region associated with the specified culture identifier.</summary>
		/// <param name="culture">A culture identifier. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="culture" /> specifies either an invariant, custom, or neutral culture.</exception>
		// Token: 0x060030E8 RID: 12520 RVA: 0x000B3707 File Offset: 0x000B1907
		public RegionInfo(int culture)
		{
			if (!this.GetByTerritory(CultureInfo.GetCultureInfo(culture)))
			{
				throw new ArgumentException(string.Format("Region ID {0} (0x{0:X4}) is not a supported region.", culture), "culture");
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Globalization.RegionInfo" /> class based on the country/region or specific culture, specified by name.</summary>
		/// <param name="name">A string that contains a two-letter code defined in ISO 3166 for country/region.-or-A string that contains the culture name for a specific culture, custom culture, or Windows-only culture. If the culture name is not in RFC 4646 format, your application should specify the entire culture name instead of just the country/region. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is not a valid country/region name or specific culture name.</exception>
		// Token: 0x060030E9 RID: 12521 RVA: 0x000B3738 File Offset: 0x000B1938
		public RegionInfo(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException();
			}
			if (this.construct_internal_region_from_name(name.ToUpperInvariant()))
			{
				return;
			}
			if (!this.GetByTerritory(CultureInfo.GetCultureInfo(name)))
			{
				throw new ArgumentException(string.Format("Region name {0} is not supported.", name), "name");
			}
		}

		// Token: 0x060030EA RID: 12522 RVA: 0x000B3788 File Offset: 0x000B1988
		private RegionInfo(CultureInfo ci)
		{
			if (ci.LCID == 127)
			{
				this.regionId = 244;
				this.iso2Name = "IV";
				this.iso3Name = "ivc";
				this.win3Name = "IVC";
				this.nativeName = (this.englishName = "Invariant Country");
				this.currencySymbol = "¤";
				this.isoCurrencySymbol = "XDR";
				this.currencyEnglishName = (this.currencyNativeName = "International Monetary Fund");
				return;
			}
			if (ci.Territory == null)
			{
				throw new NotImplementedException("Neutral region info");
			}
			this.construct_internal_region_from_name(ci.Territory.ToUpperInvariant());
		}

		// Token: 0x060030EB RID: 12523 RVA: 0x000B3835 File Offset: 0x000B1A35
		private bool GetByTerritory(CultureInfo ci)
		{
			if (ci == null)
			{
				throw new Exception("INTERNAL ERROR: should not happen.");
			}
			return !ci.IsNeutralCulture && ci.Territory != null && this.construct_internal_region_from_name(ci.Territory.ToUpperInvariant());
		}

		// Token: 0x060030EC RID: 12524
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool construct_internal_region_from_name(string name);

		/// <summary>Gets the name, in English, of the currency used in the country/region.</summary>
		/// <returns>The name, in English, of the currency used in the country/region.</returns>
		// Token: 0x170007F6 RID: 2038
		// (get) Token: 0x060030ED RID: 12525 RVA: 0x000B3868 File Offset: 0x000B1A68
		[ComVisible(false)]
		public virtual string CurrencyEnglishName
		{
			get
			{
				return this.currencyEnglishName;
			}
		}

		/// <summary>Gets the currency symbol associated with the country/region.</summary>
		/// <returns>The currency symbol associated with the country/region.</returns>
		// Token: 0x170007F7 RID: 2039
		// (get) Token: 0x060030EE RID: 12526 RVA: 0x000B3870 File Offset: 0x000B1A70
		public virtual string CurrencySymbol
		{
			get
			{
				return this.currencySymbol;
			}
		}

		/// <summary>Gets the full name of the country/region in the language of the localized version of .NET Framework.</summary>
		/// <returns>The full name of the country/region in the language of the localized version of .NET Framework.</returns>
		// Token: 0x170007F8 RID: 2040
		// (get) Token: 0x060030EF RID: 12527 RVA: 0x000B3878 File Offset: 0x000B1A78
		[MonoTODO("DisplayName currently only returns the EnglishName")]
		public virtual string DisplayName
		{
			get
			{
				return this.englishName;
			}
		}

		/// <summary>Gets the full name of the country/region in English.</summary>
		/// <returns>The full name of the country/region in English.</returns>
		// Token: 0x170007F9 RID: 2041
		// (get) Token: 0x060030F0 RID: 12528 RVA: 0x000B3878 File Offset: 0x000B1A78
		public virtual string EnglishName
		{
			get
			{
				return this.englishName;
			}
		}

		/// <summary>Gets a unique identification number for a geographical region, country, city, or location.</summary>
		/// <returns>A 32-bit signed number that uniquely identifies a geographical location.</returns>
		// Token: 0x170007FA RID: 2042
		// (get) Token: 0x060030F1 RID: 12529 RVA: 0x000B3880 File Offset: 0x000B1A80
		[ComVisible(false)]
		public virtual int GeoId
		{
			get
			{
				return this.regionId;
			}
		}

		/// <summary>Gets a value indicating whether the country/region uses the metric system for measurements.</summary>
		/// <returns>
		///     <see langword="true" /> if the country/region uses the metric system for measurements; otherwise, <see langword="false" />.</returns>
		// Token: 0x170007FB RID: 2043
		// (get) Token: 0x060030F2 RID: 12530 RVA: 0x000B3888 File Offset: 0x000B1A88
		public virtual bool IsMetric
		{
			get
			{
				string a = this.iso2Name;
				return !(a == "US") && !(a == "UK");
			}
		}

		/// <summary>Gets the three-character ISO 4217 currency symbol associated with the country/region.</summary>
		/// <returns>The three-character ISO 4217 currency symbol associated with the country/region.</returns>
		// Token: 0x170007FC RID: 2044
		// (get) Token: 0x060030F3 RID: 12531 RVA: 0x000B38B9 File Offset: 0x000B1AB9
		public virtual string ISOCurrencySymbol
		{
			get
			{
				return this.isoCurrencySymbol;
			}
		}

		/// <summary>Gets the name of a country/region formatted in the native language of the country/region.</summary>
		/// <returns>The native name of the country/region formatted in the language associated with the ISO 3166 country/region code. </returns>
		// Token: 0x170007FD RID: 2045
		// (get) Token: 0x060030F4 RID: 12532 RVA: 0x000B38C1 File Offset: 0x000B1AC1
		[ComVisible(false)]
		public virtual string NativeName
		{
			get
			{
				return this.nativeName;
			}
		}

		/// <summary>Gets the name of the currency used in the country/region, formatted in the native language of the country/region. </summary>
		/// <returns>The native name of the currency used in the country/region, formatted in the language associated with the ISO 3166 country/region code. </returns>
		// Token: 0x170007FE RID: 2046
		// (get) Token: 0x060030F5 RID: 12533 RVA: 0x000B38C9 File Offset: 0x000B1AC9
		[ComVisible(false)]
		public virtual string CurrencyNativeName
		{
			get
			{
				return this.currencyNativeName;
			}
		}

		/// <summary>Gets the name or ISO 3166 two-letter country/region code for the current <see cref="T:System.Globalization.RegionInfo" /> object.</summary>
		/// <returns>The value specified by the <paramref name="name" /> parameter of the <see cref="M:System.Globalization.RegionInfo.#ctor(System.String)" /> constructor. The return value is in uppercase.-or-The two-letter code defined in ISO 3166 for the country/region specified by the <paramref name="culture" /> parameter of the <see cref="M:System.Globalization.RegionInfo.#ctor(System.Int32)" /> constructor. The return value is in uppercase.</returns>
		// Token: 0x170007FF RID: 2047
		// (get) Token: 0x060030F6 RID: 12534 RVA: 0x000B38D1 File Offset: 0x000B1AD1
		public virtual string Name
		{
			get
			{
				return this.iso2Name;
			}
		}

		/// <summary>Gets the three-letter code defined in ISO 3166 for the country/region.</summary>
		/// <returns>The three-letter code defined in ISO 3166 for the country/region.</returns>
		// Token: 0x17000800 RID: 2048
		// (get) Token: 0x060030F7 RID: 12535 RVA: 0x000B38D9 File Offset: 0x000B1AD9
		public virtual string ThreeLetterISORegionName
		{
			get
			{
				return this.iso3Name;
			}
		}

		/// <summary>Gets the three-letter code assigned by Windows to the country/region represented by this <see cref="T:System.Globalization.RegionInfo" />.</summary>
		/// <returns>The three-letter code assigned by Windows to the country/region represented by this <see cref="T:System.Globalization.RegionInfo" />.</returns>
		// Token: 0x17000801 RID: 2049
		// (get) Token: 0x060030F8 RID: 12536 RVA: 0x000B38E1 File Offset: 0x000B1AE1
		public virtual string ThreeLetterWindowsRegionName
		{
			get
			{
				return this.win3Name;
			}
		}

		/// <summary>Gets the two-letter code defined in ISO 3166 for the country/region.</summary>
		/// <returns>The two-letter code defined in ISO 3166 for the country/region.</returns>
		// Token: 0x17000802 RID: 2050
		// (get) Token: 0x060030F9 RID: 12537 RVA: 0x000B38D1 File Offset: 0x000B1AD1
		public virtual string TwoLetterISORegionName
		{
			get
			{
				return this.iso2Name;
			}
		}

		/// <summary>Determines whether the specified object is the same instance as the current <see cref="T:System.Globalization.RegionInfo" />.</summary>
		/// <param name="value">The object to compare with the current <see cref="T:System.Globalization.RegionInfo" />. </param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="value" /> parameter is a <see cref="T:System.Globalization.RegionInfo" /> object and its <see cref="P:System.Globalization.RegionInfo.Name" /> property is the same as the <see cref="P:System.Globalization.RegionInfo.Name" /> property of the current <see cref="T:System.Globalization.RegionInfo" /> object; otherwise, <see langword="false" />.</returns>
		// Token: 0x060030FA RID: 12538 RVA: 0x000B38EC File Offset: 0x000B1AEC
		public override bool Equals(object value)
		{
			RegionInfo regionInfo = value as RegionInfo;
			return regionInfo != null && this.Name == regionInfo.Name;
		}

		/// <summary>Serves as a hash function for the current <see cref="T:System.Globalization.RegionInfo" />, suitable for hashing algorithms and data structures, such as a hash table.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Globalization.RegionInfo" />.</returns>
		// Token: 0x060030FB RID: 12539 RVA: 0x000B3916 File Offset: 0x000B1B16
		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}

		/// <summary>Returns a string containing the culture name or ISO 3166 two-letter country/region codes specified for the current <see cref="T:System.Globalization.RegionInfo" />.</summary>
		/// <returns>A string containing the culture name or ISO 3166 two-letter country/region codes defined for the current <see cref="T:System.Globalization.RegionInfo" />.</returns>
		// Token: 0x060030FC RID: 12540 RVA: 0x000B3923 File Offset: 0x000B1B23
		public override string ToString()
		{
			return this.Name;
		}

		// Token: 0x060030FD RID: 12541 RVA: 0x000B392B File Offset: 0x000B1B2B
		internal static void ClearCachedData()
		{
			RegionInfo.currentRegion = null;
		}

		// Token: 0x04001A68 RID: 6760
		private static RegionInfo currentRegion;

		// Token: 0x04001A69 RID: 6761
		private int regionId;

		// Token: 0x04001A6A RID: 6762
		private string iso2Name;

		// Token: 0x04001A6B RID: 6763
		private string iso3Name;

		// Token: 0x04001A6C RID: 6764
		private string win3Name;

		// Token: 0x04001A6D RID: 6765
		private string englishName;

		// Token: 0x04001A6E RID: 6766
		private string nativeName;

		// Token: 0x04001A6F RID: 6767
		private string currencySymbol;

		// Token: 0x04001A70 RID: 6768
		private string isoCurrencySymbol;

		// Token: 0x04001A71 RID: 6769
		private string currencyEnglishName;

		// Token: 0x04001A72 RID: 6770
		private string currencyNativeName;
	}
}
