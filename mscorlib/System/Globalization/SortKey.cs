﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Globalization
{
	/// <summary>Represents the result of mapping a string to its sort key.</summary>
	// Token: 0x02000403 RID: 1027
	[ComVisible(true)]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class SortKey
	{
		/// <summary>Compares two sort keys.</summary>
		/// <param name="sortkey1">The first sort key to compare. </param>
		/// <param name="sortkey2">The second sort key to compare. </param>
		/// <returns>A signed integer that indicates the relationship between <paramref name="sortkey1" /> and <paramref name="sortkey2" />.Value Condition Less than zero 
		///             <paramref name="sortkey1" /> is less than <paramref name="sortkey2" />. Zero 
		///             <paramref name="sortkey1" /> is equal to <paramref name="sortkey2" />. Greater than zero 
		///             <paramref name="sortkey1" /> is greater than <paramref name="sortkey2" />. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="sortkey1" /> or <paramref name="sortkey2" /> is <see langword="null" />.</exception>
		// Token: 0x0600302A RID: 12330 RVA: 0x000AB8C8 File Offset: 0x000A9AC8
		public static int Compare(SortKey sortkey1, SortKey sortkey2)
		{
			if (sortkey1 == null)
			{
				throw new ArgumentNullException("sortkey1");
			}
			if (sortkey2 == null)
			{
				throw new ArgumentNullException("sortkey2");
			}
			if (sortkey1 == sortkey2 || sortkey1.OriginalString == sortkey2.OriginalString)
			{
				return 0;
			}
			byte[] keyData = sortkey1.KeyData;
			byte[] keyData2 = sortkey2.KeyData;
			int num = (keyData.Length > keyData2.Length) ? keyData2.Length : keyData.Length;
			int i = 0;
			while (i < num)
			{
				if (keyData[i] != keyData2[i])
				{
					if (keyData[i] >= keyData2[i])
					{
						return 1;
					}
					return -1;
				}
				else
				{
					i++;
				}
			}
			if (keyData.Length == keyData2.Length)
			{
				return 0;
			}
			if (keyData.Length >= keyData2.Length)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x0600302B RID: 12331 RVA: 0x000AB959 File Offset: 0x000A9B59
		internal SortKey(int lcid, string source, CompareOptions opt)
		{
			this.lcid = lcid;
			this.source = source;
			this.options = opt;
		}

		// Token: 0x0600302C RID: 12332 RVA: 0x000AB976 File Offset: 0x000A9B76
		internal SortKey(int lcid, string source, byte[] buffer, CompareOptions opt, int lv1Length, int lv2Length, int lv3Length, int kanaSmallLength, int markTypeLength, int katakanaLength, int kanaWidthLength, int identLength)
		{
			this.lcid = lcid;
			this.source = source;
			this.key = buffer;
			this.options = opt;
		}

		/// <summary>Gets the original string used to create the current <see cref="T:System.Globalization.SortKey" /> object.</summary>
		/// <returns>The original string used to create the current <see cref="T:System.Globalization.SortKey" /> object.</returns>
		// Token: 0x170007B4 RID: 1972
		// (get) Token: 0x0600302D RID: 12333 RVA: 0x000AB99B File Offset: 0x000A9B9B
		public virtual string OriginalString
		{
			get
			{
				return this.source;
			}
		}

		/// <summary>Gets the byte array representing the current <see cref="T:System.Globalization.SortKey" /> object.</summary>
		/// <returns>A byte array representing the current <see cref="T:System.Globalization.SortKey" /> object. </returns>
		// Token: 0x170007B5 RID: 1973
		// (get) Token: 0x0600302E RID: 12334 RVA: 0x000AB9A3 File Offset: 0x000A9BA3
		public virtual byte[] KeyData
		{
			get
			{
				return this.key;
			}
		}

		/// <summary>Determines whether the specified object is equal to the current <see cref="T:System.Globalization.SortKey" /> object.</summary>
		/// <param name="value">The object to compare with the current <see cref="T:System.Globalization.SortKey" /> object. </param>
		/// <returns>
		///     <see langword="true" /> if the <paramref name="value" /> parameter is equal to the current <see cref="T:System.Globalization.SortKey" /> object; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x0600302F RID: 12335 RVA: 0x000AB9AC File Offset: 0x000A9BAC
		public override bool Equals(object value)
		{
			SortKey sortKey = value as SortKey;
			return sortKey != null && this.lcid == sortKey.lcid && this.options == sortKey.options && SortKey.Compare(this, sortKey) == 0;
		}

		/// <summary>Serves as a hash function for the current <see cref="T:System.Globalization.SortKey" /> object that is suitable for hashing algorithms and data structures such as a hash table.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Globalization.SortKey" /> object.</returns>
		// Token: 0x06003030 RID: 12336 RVA: 0x000AB9EC File Offset: 0x000A9BEC
		public override int GetHashCode()
		{
			if (this.key.Length == 0)
			{
				return 0;
			}
			int num = (int)this.key[0];
			for (int i = 1; i < this.key.Length; i++)
			{
				num ^= (int)this.key[i] << (i & 3);
			}
			return num;
		}

		/// <summary>Returns a string that represents the current <see cref="T:System.Globalization.SortKey" /> object.</summary>
		/// <returns>A string that represents the current <see cref="T:System.Globalization.SortKey" /> object.</returns>
		// Token: 0x06003031 RID: 12337 RVA: 0x000ABA34 File Offset: 0x000A9C34
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"SortKey - ",
				this.lcid,
				", ",
				this.options,
				", ",
				this.source
			});
		}

		// Token: 0x06003032 RID: 12338 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal SortKey()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040019E7 RID: 6631
		private readonly string source;

		// Token: 0x040019E8 RID: 6632
		private readonly byte[] key;

		// Token: 0x040019E9 RID: 6633
		private readonly CompareOptions options;

		// Token: 0x040019EA RID: 6634
		private readonly int lcid;
	}
}
