﻿using System;
using Unity;

namespace System.Globalization
{
	/// <summary>Provides globalization-related extension methods. </summary>
	// Token: 0x02000AB1 RID: 2737
	public static class GlobalizationExtensions
	{
		/// <summary>Returns a <see cref="T:System.StringComparer" /> object based on the culture-sensitive string comparision rules of a specified <see cref="T:System.Globalization.CompareInfo" /> object. </summary>
		/// <param name="compareInfo">An object that supports culture-sensitive string comparison. </param>
		/// <param name="options">A value that defines how strings should be compared. <paramref name="options" /> is either the enumeration value  <see cref="F:System.Globalization.CompareOptions.Ordinal" /> or a bitwise combination of one or more of the following values: <see cref="F:System.Globalization.CompareOptions.IgnoreCase" />, <see cref="F:System.Globalization.CompareOptions.IgnoreSymbols" />, <see cref="F:System.Globalization.CompareOptions.IgnoreNonSpace" />, <see cref="F:System.Globalization.CompareOptions.IgnoreWidth" />, <see cref="F:System.Globalization.CompareOptions.IgnoreKanaType" />, and <see cref="F:System.Globalization.CompareOptions.StringSort" />. </param>
		/// <returns>An object that can be used to perform string comparisons. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="compareInfo" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="options" /> is invalid.</exception>
		// Token: 0x06005F74 RID: 24436 RVA: 0x0005AB11 File Offset: 0x00058D11
		public static StringComparer GetStringComparer(this CompareInfo compareInfo, CompareOptions options)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
