﻿using System;
using System.IO;
using System.Reflection;
using System.Security;

namespace System.Globalization
{
	// Token: 0x020003D6 RID: 982
	internal sealed class GlobalizationAssembly
	{
		// Token: 0x06002DB5 RID: 11701 RVA: 0x000A2010 File Offset: 0x000A0210
		[SecurityCritical]
		internal unsafe static byte* GetGlobalizationResourceBytePtr(Assembly assembly, string tableName)
		{
			UnmanagedMemoryStream unmanagedMemoryStream = assembly.GetManifestResourceStream(tableName) as UnmanagedMemoryStream;
			if (unmanagedMemoryStream != null)
			{
				byte* positionPointer = unmanagedMemoryStream.PositionPointer;
				if (positionPointer != null)
				{
					return positionPointer;
				}
			}
			throw new InvalidOperationException();
		}

		// Token: 0x06002DB6 RID: 11702 RVA: 0x00002050 File Offset: 0x00000250
		public GlobalizationAssembly()
		{
		}
	}
}
