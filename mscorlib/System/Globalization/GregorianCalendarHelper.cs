﻿using System;
using System.Runtime.Serialization;

namespace System.Globalization
{
	// Token: 0x020003D9 RID: 985
	[Serializable]
	internal class GregorianCalendarHelper
	{
		// Token: 0x17000720 RID: 1824
		// (get) Token: 0x06002DDC RID: 11740 RVA: 0x000A2A55 File Offset: 0x000A0C55
		internal int MaxYear
		{
			get
			{
				return this.m_maxYear;
			}
		}

		// Token: 0x06002DDD RID: 11741 RVA: 0x000A2A60 File Offset: 0x000A0C60
		internal GregorianCalendarHelper(Calendar cal, EraInfo[] eraInfo)
		{
			this.m_Cal = cal;
			this.m_EraInfo = eraInfo;
			this.m_minDate = this.m_Cal.MinSupportedDateTime;
			this.m_maxYear = this.m_EraInfo[0].maxEraYear;
			this.m_minYear = this.m_EraInfo[0].minEraYear;
		}

		// Token: 0x06002DDE RID: 11742 RVA: 0x000A2AC4 File Offset: 0x000A0CC4
		internal int GetGregorianYear(int year, int era)
		{
			if (year < 0)
			{
				throw new ArgumentOutOfRangeException("year", Environment.GetResourceString("Non-negative number required."));
			}
			if (era == 0)
			{
				era = this.m_Cal.CurrentEraValue;
			}
			int i = 0;
			while (i < this.m_EraInfo.Length)
			{
				if (era == this.m_EraInfo[i].era)
				{
					if (year < this.m_EraInfo[i].minEraYear || year > this.m_EraInfo[i].maxEraYear)
					{
						throw new ArgumentOutOfRangeException("year", string.Format(CultureInfo.CurrentCulture, Environment.GetResourceString("Valid values are between {0} and {1}, inclusive."), this.m_EraInfo[i].minEraYear, this.m_EraInfo[i].maxEraYear));
					}
					return this.m_EraInfo[i].yearOffset + year;
				}
				else
				{
					i++;
				}
			}
			throw new ArgumentOutOfRangeException("era", Environment.GetResourceString("Era value was not valid."));
		}

		// Token: 0x06002DDF RID: 11743 RVA: 0x000A2BAC File Offset: 0x000A0DAC
		internal bool IsValidYear(int year, int era)
		{
			if (year < 0)
			{
				return false;
			}
			if (era == 0)
			{
				era = this.m_Cal.CurrentEraValue;
			}
			for (int i = 0; i < this.m_EraInfo.Length; i++)
			{
				if (era == this.m_EraInfo[i].era)
				{
					return year >= this.m_EraInfo[i].minEraYear && year <= this.m_EraInfo[i].maxEraYear;
				}
			}
			return false;
		}

		// Token: 0x06002DE0 RID: 11744 RVA: 0x000A2C18 File Offset: 0x000A0E18
		internal virtual int GetDatePart(long ticks, int part)
		{
			this.CheckTicksRange(ticks);
			int i = (int)(ticks / 864000000000L);
			int num = i / 146097;
			i -= num * 146097;
			int num2 = i / 36524;
			if (num2 == 4)
			{
				num2 = 3;
			}
			i -= num2 * 36524;
			int num3 = i / 1461;
			i -= num3 * 1461;
			int num4 = i / 365;
			if (num4 == 4)
			{
				num4 = 3;
			}
			if (part == 0)
			{
				return num * 400 + num2 * 100 + num3 * 4 + num4 + 1;
			}
			i -= num4 * 365;
			if (part == 1)
			{
				return i + 1;
			}
			int[] array = (num4 == 3 && (num3 != 24 || num2 == 3)) ? GregorianCalendarHelper.DaysToMonth366 : GregorianCalendarHelper.DaysToMonth365;
			int num5 = i >> 6;
			while (i >= array[num5])
			{
				num5++;
			}
			if (part == 2)
			{
				return num5;
			}
			return i - array[num5 - 1] + 1;
		}

		// Token: 0x06002DE1 RID: 11745 RVA: 0x000A2D00 File Offset: 0x000A0F00
		internal static long GetAbsoluteDate(int year, int month, int day)
		{
			if (year >= 1 && year <= 9999 && month >= 1 && month <= 12)
			{
				int[] array = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) ? GregorianCalendarHelper.DaysToMonth366 : GregorianCalendarHelper.DaysToMonth365;
				if (day >= 1 && day <= array[month] - array[month - 1])
				{
					int num = year - 1;
					return (long)(num * 365 + num / 4 - num / 100 + num / 400 + array[month - 1] + day - 1);
				}
			}
			throw new ArgumentOutOfRangeException(null, Environment.GetResourceString("Year, Month, and Day parameters describe an un-representable DateTime."));
		}

		// Token: 0x06002DE2 RID: 11746 RVA: 0x000A2D8B File Offset: 0x000A0F8B
		internal static long DateToTicks(int year, int month, int day)
		{
			return GregorianCalendarHelper.GetAbsoluteDate(year, month, day) * 864000000000L;
		}

		// Token: 0x06002DE3 RID: 11747 RVA: 0x000A2DA0 File Offset: 0x000A0FA0
		internal static long TimeToTicks(int hour, int minute, int second, int millisecond)
		{
			if (hour < 0 || hour >= 24 || minute < 0 || minute >= 60 || second < 0 || second >= 60)
			{
				throw new ArgumentOutOfRangeException(null, Environment.GetResourceString("Hour, Minute, and Second parameters describe an un-representable DateTime."));
			}
			if (millisecond < 0 || millisecond >= 1000)
			{
				throw new ArgumentOutOfRangeException("millisecond", string.Format(CultureInfo.CurrentCulture, Environment.GetResourceString("Valid values are between {0} and {1}, inclusive."), 0, 999));
			}
			return TimeSpan.TimeToTicks(hour, minute, second) + (long)millisecond * 10000L;
		}

		// Token: 0x06002DE4 RID: 11748 RVA: 0x000A2E28 File Offset: 0x000A1028
		internal void CheckTicksRange(long ticks)
		{
			if (ticks < this.m_Cal.MinSupportedDateTime.Ticks || ticks > this.m_Cal.MaxSupportedDateTime.Ticks)
			{
				throw new ArgumentOutOfRangeException("time", string.Format(CultureInfo.InvariantCulture, Environment.GetResourceString("Specified time is not supported in this calendar. It should be between {0} (Gregorian date) and {1} (Gregorian date), inclusive."), this.m_Cal.MinSupportedDateTime, this.m_Cal.MaxSupportedDateTime));
			}
		}

		// Token: 0x06002DE5 RID: 11749 RVA: 0x000A2EA0 File Offset: 0x000A10A0
		public DateTime AddMonths(DateTime time, int months)
		{
			if (months < -120000 || months > 120000)
			{
				throw new ArgumentOutOfRangeException("months", string.Format(CultureInfo.CurrentCulture, Environment.GetResourceString("Valid values are between {0} and {1}, inclusive."), -120000, 120000));
			}
			this.CheckTicksRange(time.Ticks);
			int num = this.GetDatePart(time.Ticks, 0);
			int num2 = this.GetDatePart(time.Ticks, 2);
			int num3 = this.GetDatePart(time.Ticks, 3);
			int num4 = num2 - 1 + months;
			if (num4 >= 0)
			{
				num2 = num4 % 12 + 1;
				num += num4 / 12;
			}
			else
			{
				num2 = 12 + (num4 + 1) % 12;
				num += (num4 - 11) / 12;
			}
			int[] array = (num % 4 == 0 && (num % 100 != 0 || num % 400 == 0)) ? GregorianCalendarHelper.DaysToMonth366 : GregorianCalendarHelper.DaysToMonth365;
			int num5 = array[num2] - array[num2 - 1];
			if (num3 > num5)
			{
				num3 = num5;
			}
			long ticks = GregorianCalendarHelper.DateToTicks(num, num2, num3) + time.Ticks % 864000000000L;
			Calendar.CheckAddResult(ticks, this.m_Cal.MinSupportedDateTime, this.m_Cal.MaxSupportedDateTime);
			return new DateTime(ticks);
		}

		// Token: 0x06002DE6 RID: 11750 RVA: 0x000A2FCA File Offset: 0x000A11CA
		public DateTime AddYears(DateTime time, int years)
		{
			return this.AddMonths(time, years * 12);
		}

		// Token: 0x06002DE7 RID: 11751 RVA: 0x000A2FD7 File Offset: 0x000A11D7
		public int GetDayOfMonth(DateTime time)
		{
			return this.GetDatePart(time.Ticks, 3);
		}

		// Token: 0x06002DE8 RID: 11752 RVA: 0x000A2FE7 File Offset: 0x000A11E7
		public DayOfWeek GetDayOfWeek(DateTime time)
		{
			this.CheckTicksRange(time.Ticks);
			return (DayOfWeek)((time.Ticks / 864000000000L + 1L) % 7L);
		}

		// Token: 0x06002DE9 RID: 11753 RVA: 0x000A300E File Offset: 0x000A120E
		public int GetDayOfYear(DateTime time)
		{
			return this.GetDatePart(time.Ticks, 1);
		}

		// Token: 0x06002DEA RID: 11754 RVA: 0x000A3020 File Offset: 0x000A1220
		public int GetDaysInMonth(int year, int month, int era)
		{
			year = this.GetGregorianYear(year, era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", Environment.GetResourceString("Month must be between one and twelve."));
			}
			int[] array = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) ? GregorianCalendarHelper.DaysToMonth366 : GregorianCalendarHelper.DaysToMonth365;
			return array[month] - array[month - 1];
		}

		// Token: 0x06002DEB RID: 11755 RVA: 0x000A307F File Offset: 0x000A127F
		public int GetDaysInYear(int year, int era)
		{
			year = this.GetGregorianYear(year, era);
			if (year % 4 != 0 || (year % 100 == 0 && year % 400 != 0))
			{
				return 365;
			}
			return 366;
		}

		// Token: 0x06002DEC RID: 11756 RVA: 0x000A30AC File Offset: 0x000A12AC
		public int GetEra(DateTime time)
		{
			long ticks = time.Ticks;
			for (int i = 0; i < this.m_EraInfo.Length; i++)
			{
				if (ticks >= this.m_EraInfo[i].ticks)
				{
					return this.m_EraInfo[i].era;
				}
			}
			throw new ArgumentOutOfRangeException(Environment.GetResourceString("Time value was out of era range."));
		}

		// Token: 0x17000721 RID: 1825
		// (get) Token: 0x06002DED RID: 11757 RVA: 0x000A3104 File Offset: 0x000A1304
		public int[] Eras
		{
			get
			{
				if (this.m_eras == null)
				{
					this.m_eras = new int[this.m_EraInfo.Length];
					for (int i = 0; i < this.m_EraInfo.Length; i++)
					{
						this.m_eras[i] = this.m_EraInfo[i].era;
					}
				}
				return (int[])this.m_eras.Clone();
			}
		}

		// Token: 0x06002DEE RID: 11758 RVA: 0x000A3164 File Offset: 0x000A1364
		public int GetMonth(DateTime time)
		{
			return this.GetDatePart(time.Ticks, 2);
		}

		// Token: 0x06002DEF RID: 11759 RVA: 0x000A3174 File Offset: 0x000A1374
		public int GetMonthsInYear(int year, int era)
		{
			year = this.GetGregorianYear(year, era);
			return 12;
		}

		// Token: 0x06002DF0 RID: 11760 RVA: 0x000A3184 File Offset: 0x000A1384
		public int GetYear(DateTime time)
		{
			long ticks = time.Ticks;
			int datePart = this.GetDatePart(ticks, 0);
			for (int i = 0; i < this.m_EraInfo.Length; i++)
			{
				if (ticks >= this.m_EraInfo[i].ticks)
				{
					return datePart - this.m_EraInfo[i].yearOffset;
				}
			}
			throw new ArgumentException(Environment.GetResourceString("No Era was supplied."));
		}

		// Token: 0x06002DF1 RID: 11761 RVA: 0x000A31E4 File Offset: 0x000A13E4
		public int GetYear(int year, DateTime time)
		{
			long ticks = time.Ticks;
			for (int i = 0; i < this.m_EraInfo.Length; i++)
			{
				if (ticks >= this.m_EraInfo[i].ticks)
				{
					return year - this.m_EraInfo[i].yearOffset;
				}
			}
			throw new ArgumentException(Environment.GetResourceString("No Era was supplied."));
		}

		// Token: 0x06002DF2 RID: 11762 RVA: 0x000A323C File Offset: 0x000A143C
		public bool IsLeapDay(int year, int month, int day, int era)
		{
			if (day < 1 || day > this.GetDaysInMonth(year, month, era))
			{
				throw new ArgumentOutOfRangeException("day", string.Format(CultureInfo.CurrentCulture, Environment.GetResourceString("Valid values are between {0} and {1}, inclusive."), 1, this.GetDaysInMonth(year, month, era)));
			}
			return this.IsLeapYear(year, era) && (month == 2 && day == 29);
		}

		// Token: 0x06002DF3 RID: 11763 RVA: 0x000A32A7 File Offset: 0x000A14A7
		public int GetLeapMonth(int year, int era)
		{
			year = this.GetGregorianYear(year, era);
			return 0;
		}

		// Token: 0x06002DF4 RID: 11764 RVA: 0x000A32B4 File Offset: 0x000A14B4
		public bool IsLeapMonth(int year, int month, int era)
		{
			year = this.GetGregorianYear(year, era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", string.Format(CultureInfo.CurrentCulture, Environment.GetResourceString("Valid values are between {0} and {1}, inclusive."), 1, 12));
			}
			return false;
		}

		// Token: 0x06002DF5 RID: 11765 RVA: 0x000A3301 File Offset: 0x000A1501
		public bool IsLeapYear(int year, int era)
		{
			year = this.GetGregorianYear(year, era);
			return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
		}

		// Token: 0x06002DF6 RID: 11766 RVA: 0x000A3328 File Offset: 0x000A1528
		public DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			year = this.GetGregorianYear(year, era);
			long ticks = GregorianCalendarHelper.DateToTicks(year, month, day) + GregorianCalendarHelper.TimeToTicks(hour, minute, second, millisecond);
			this.CheckTicksRange(ticks);
			return new DateTime(ticks);
		}

		// Token: 0x06002DF7 RID: 11767 RVA: 0x000A3364 File Offset: 0x000A1564
		public virtual int GetWeekOfYear(DateTime time, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
		{
			this.CheckTicksRange(time.Ticks);
			return GregorianCalendar.GetDefaultInstance().GetWeekOfYear(time, rule, firstDayOfWeek);
		}

		// Token: 0x06002DF8 RID: 11768 RVA: 0x000A3380 File Offset: 0x000A1580
		public int ToFourDigitYear(int year, int twoDigitYearMax)
		{
			if (year < 0)
			{
				throw new ArgumentOutOfRangeException("year", Environment.GetResourceString("Positive number required."));
			}
			if (year < 100)
			{
				int num = year % 100;
				return (twoDigitYearMax / 100 - ((num > twoDigitYearMax % 100) ? 1 : 0)) * 100 + num;
			}
			if (year < this.m_minYear || year > this.m_maxYear)
			{
				throw new ArgumentOutOfRangeException("year", string.Format(CultureInfo.CurrentCulture, Environment.GetResourceString("Valid values are between {0} and {1}, inclusive."), this.m_minYear, this.m_maxYear));
			}
			return year;
		}

		// Token: 0x06002DF9 RID: 11769 RVA: 0x000A340E File Offset: 0x000A160E
		// Note: this type is marked as 'beforefieldinit'.
		static GregorianCalendarHelper()
		{
		}

		// Token: 0x04001868 RID: 6248
		internal const long TicksPerMillisecond = 10000L;

		// Token: 0x04001869 RID: 6249
		internal const long TicksPerSecond = 10000000L;

		// Token: 0x0400186A RID: 6250
		internal const long TicksPerMinute = 600000000L;

		// Token: 0x0400186B RID: 6251
		internal const long TicksPerHour = 36000000000L;

		// Token: 0x0400186C RID: 6252
		internal const long TicksPerDay = 864000000000L;

		// Token: 0x0400186D RID: 6253
		internal const int MillisPerSecond = 1000;

		// Token: 0x0400186E RID: 6254
		internal const int MillisPerMinute = 60000;

		// Token: 0x0400186F RID: 6255
		internal const int MillisPerHour = 3600000;

		// Token: 0x04001870 RID: 6256
		internal const int MillisPerDay = 86400000;

		// Token: 0x04001871 RID: 6257
		internal const int DaysPerYear = 365;

		// Token: 0x04001872 RID: 6258
		internal const int DaysPer4Years = 1461;

		// Token: 0x04001873 RID: 6259
		internal const int DaysPer100Years = 36524;

		// Token: 0x04001874 RID: 6260
		internal const int DaysPer400Years = 146097;

		// Token: 0x04001875 RID: 6261
		internal const int DaysTo10000 = 3652059;

		// Token: 0x04001876 RID: 6262
		internal const long MaxMillis = 315537897600000L;

		// Token: 0x04001877 RID: 6263
		internal const int DatePartYear = 0;

		// Token: 0x04001878 RID: 6264
		internal const int DatePartDayOfYear = 1;

		// Token: 0x04001879 RID: 6265
		internal const int DatePartMonth = 2;

		// Token: 0x0400187A RID: 6266
		internal const int DatePartDay = 3;

		// Token: 0x0400187B RID: 6267
		internal static readonly int[] DaysToMonth365 = new int[]
		{
			0,
			31,
			59,
			90,
			120,
			151,
			181,
			212,
			243,
			273,
			304,
			334,
			365
		};

		// Token: 0x0400187C RID: 6268
		internal static readonly int[] DaysToMonth366 = new int[]
		{
			0,
			31,
			60,
			91,
			121,
			152,
			182,
			213,
			244,
			274,
			305,
			335,
			366
		};

		// Token: 0x0400187D RID: 6269
		[OptionalField(VersionAdded = 1)]
		internal int m_maxYear = 9999;

		// Token: 0x0400187E RID: 6270
		[OptionalField(VersionAdded = 1)]
		internal int m_minYear;

		// Token: 0x0400187F RID: 6271
		internal Calendar m_Cal;

		// Token: 0x04001880 RID: 6272
		[OptionalField(VersionAdded = 1)]
		internal EraInfo[] m_EraInfo;

		// Token: 0x04001881 RID: 6273
		[OptionalField(VersionAdded = 1)]
		internal int[] m_eras;

		// Token: 0x04001882 RID: 6274
		[OptionalField(VersionAdded = 1)]
		internal DateTime m_minDate;
	}
}
