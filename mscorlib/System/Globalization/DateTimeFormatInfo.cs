﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Versioning;
using System.Security;
using System.Threading;

namespace System.Globalization
{
	/// <summary>Provides culture-specific information about the format of date and time values.</summary>
	// Token: 0x020003CC RID: 972
	[ComVisible(true)]
	[Serializable]
	public sealed class DateTimeFormatInfo : ICloneable, IFormatProvider
	{
		// Token: 0x06002CF8 RID: 11512 RVA: 0x00002526 File Offset: 0x00000726
		[SecuritySafeCritical]
		private static bool InitPreferExistingTokens()
		{
			return false;
		}

		// Token: 0x170006D6 RID: 1750
		// (get) Token: 0x06002CF9 RID: 11513 RVA: 0x0009E1D2 File Offset: 0x0009C3D2
		private string CultureName
		{
			get
			{
				if (this.m_name == null)
				{
					this.m_name = this.m_cultureData.CultureName;
				}
				return this.m_name;
			}
		}

		// Token: 0x170006D7 RID: 1751
		// (get) Token: 0x06002CFA RID: 11514 RVA: 0x0009E1F3 File Offset: 0x0009C3F3
		private CultureInfo Culture
		{
			get
			{
				if (this.m_cultureInfo == null)
				{
					this.m_cultureInfo = CultureInfo.GetCultureInfo(this.CultureName);
				}
				return this.m_cultureInfo;
			}
		}

		// Token: 0x170006D8 RID: 1752
		// (get) Token: 0x06002CFB RID: 11515 RVA: 0x0009E214 File Offset: 0x0009C414
		private string LanguageName
		{
			[SecurityCritical]
			get
			{
				if (this.m_langName == null)
				{
					this.m_langName = this.m_cultureData.SISO639LANGNAME;
				}
				return this.m_langName;
			}
		}

		// Token: 0x06002CFC RID: 11516 RVA: 0x0009E235 File Offset: 0x0009C435
		private string[] internalGetAbbreviatedDayOfWeekNames()
		{
			if (this.abbreviatedDayNames == null)
			{
				this.abbreviatedDayNames = this.m_cultureData.AbbreviatedDayNames(this.Calendar.ID);
			}
			return this.abbreviatedDayNames;
		}

		// Token: 0x06002CFD RID: 11517 RVA: 0x0009E261 File Offset: 0x0009C461
		private string[] internalGetSuperShortDayNames()
		{
			if (this.m_superShortDayNames == null)
			{
				this.m_superShortDayNames = this.m_cultureData.SuperShortDayNames(this.Calendar.ID);
			}
			return this.m_superShortDayNames;
		}

		// Token: 0x06002CFE RID: 11518 RVA: 0x0009E28D File Offset: 0x0009C48D
		private string[] internalGetDayOfWeekNames()
		{
			if (this.dayNames == null)
			{
				this.dayNames = this.m_cultureData.DayNames(this.Calendar.ID);
			}
			return this.dayNames;
		}

		// Token: 0x06002CFF RID: 11519 RVA: 0x0009E2B9 File Offset: 0x0009C4B9
		private string[] internalGetAbbreviatedMonthNames()
		{
			if (this.abbreviatedMonthNames == null)
			{
				this.abbreviatedMonthNames = this.m_cultureData.AbbreviatedMonthNames(this.Calendar.ID);
			}
			return this.abbreviatedMonthNames;
		}

		// Token: 0x06002D00 RID: 11520 RVA: 0x0009E2E5 File Offset: 0x0009C4E5
		private string[] internalGetMonthNames()
		{
			if (this.monthNames == null)
			{
				this.monthNames = this.m_cultureData.MonthNames(this.Calendar.ID);
			}
			return this.monthNames;
		}

		/// <summary>Initializes a new writable instance of the <see cref="T:System.Globalization.DateTimeFormatInfo" /> class that is culture-independent (invariant).</summary>
		// Token: 0x06002D01 RID: 11521 RVA: 0x0009E311 File Offset: 0x0009C511
		public DateTimeFormatInfo() : this(CultureInfo.InvariantCulture.m_cultureData, GregorianCalendar.GetDefaultInstance())
		{
		}

		// Token: 0x06002D02 RID: 11522 RVA: 0x0009E328 File Offset: 0x0009C528
		internal DateTimeFormatInfo(CultureData cultureData, Calendar cal)
		{
			this.m_cultureData = cultureData;
			this.Calendar = cal;
		}

		// Token: 0x06002D03 RID: 11523 RVA: 0x0009E354 File Offset: 0x0009C554
		[SecuritySafeCritical]
		private void InitializeOverridableProperties(CultureData cultureData, int calendarID)
		{
			if (this.firstDayOfWeek == -1)
			{
				this.firstDayOfWeek = cultureData.IFIRSTDAYOFWEEK;
			}
			if (this.calendarWeekRule == -1)
			{
				this.calendarWeekRule = cultureData.IFIRSTWEEKOFYEAR;
			}
			if (this.amDesignator == null)
			{
				this.amDesignator = cultureData.SAM1159;
			}
			if (this.pmDesignator == null)
			{
				this.pmDesignator = cultureData.SPM2359;
			}
			if (this.timeSeparator == null)
			{
				this.timeSeparator = cultureData.TimeSeparator;
			}
			if (this.dateSeparator == null)
			{
				this.dateSeparator = cultureData.DateSeparator(calendarID);
			}
			this.allLongTimePatterns = this.m_cultureData.LongTimes;
			this.allShortTimePatterns = this.m_cultureData.ShortTimes;
			this.allLongDatePatterns = cultureData.LongDates(calendarID);
			this.allShortDatePatterns = cultureData.ShortDates(calendarID);
			this.allYearMonthPatterns = cultureData.YearMonths(calendarID);
		}

		// Token: 0x06002D04 RID: 11524 RVA: 0x0009E428 File Offset: 0x0009C628
		[OnDeserialized]
		private void OnDeserialized(StreamingContext ctx)
		{
			if (this.m_name != null)
			{
				this.m_cultureData = CultureData.GetCultureData(this.m_name, this.m_useUserOverride);
				if (this.m_cultureData == null)
				{
					throw new CultureNotFoundException("m_name", this.m_name, Environment.GetResourceString("Culture is not supported."));
				}
			}
			else
			{
				this.m_cultureData = CultureData.GetCultureData(this.CultureID, this.m_useUserOverride);
			}
			if (this.calendar == null)
			{
				this.calendar = (Calendar)GregorianCalendar.GetDefaultInstance().Clone();
				this.calendar.SetReadOnlyState(this.m_isReadOnly);
			}
			else
			{
				CultureInfo.CheckDomainSafetyObject(this.calendar, this);
			}
			this.InitializeOverridableProperties(this.m_cultureData, this.calendar.ID);
			bool isReadOnly = this.m_isReadOnly;
			this.m_isReadOnly = false;
			if (this.longDatePattern != null)
			{
				this.LongDatePattern = this.longDatePattern;
			}
			if (this.shortDatePattern != null)
			{
				this.ShortDatePattern = this.shortDatePattern;
			}
			if (this.yearMonthPattern != null)
			{
				this.YearMonthPattern = this.yearMonthPattern;
			}
			if (this.longTimePattern != null)
			{
				this.LongTimePattern = this.longTimePattern;
			}
			if (this.shortTimePattern != null)
			{
				this.ShortTimePattern = this.shortTimePattern;
			}
			this.m_isReadOnly = isReadOnly;
		}

		// Token: 0x06002D05 RID: 11525 RVA: 0x0009E55C File Offset: 0x0009C75C
		[OnSerializing]
		private void OnSerializing(StreamingContext ctx)
		{
			this.CultureID = this.m_cultureData.ILANGUAGE;
			this.m_useUserOverride = this.m_cultureData.UseUserOverride;
			this.m_name = this.CultureName;
			if (DateTimeFormatInfo.s_calendarNativeNames == null)
			{
				DateTimeFormatInfo.s_calendarNativeNames = new Hashtable();
			}
			string text = this.LongTimePattern;
			string text2 = this.LongDatePattern;
			string text3 = this.ShortTimePattern;
			string text4 = this.ShortDatePattern;
			string text5 = this.YearMonthPattern;
			string[] array = this.AllLongTimePatterns;
			string[] array2 = this.AllLongDatePatterns;
			string[] array3 = this.AllShortTimePatterns;
			string[] array4 = this.AllShortDatePatterns;
			string[] array5 = this.AllYearMonthPatterns;
		}

		/// <summary>Gets the default read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> object that is culture-independent (invariant).</summary>
		/// <returns>A read-only object that is culture-independent (invariant).</returns>
		// Token: 0x170006D9 RID: 1753
		// (get) Token: 0x06002D06 RID: 11526 RVA: 0x0009E5F2 File Offset: 0x0009C7F2
		public static DateTimeFormatInfo InvariantInfo
		{
			get
			{
				if (DateTimeFormatInfo.invariantInfo == null)
				{
					DateTimeFormatInfo dateTimeFormatInfo = new DateTimeFormatInfo();
					dateTimeFormatInfo.Calendar.SetReadOnlyState(true);
					dateTimeFormatInfo.m_isReadOnly = true;
					DateTimeFormatInfo.invariantInfo = dateTimeFormatInfo;
				}
				return DateTimeFormatInfo.invariantInfo;
			}
		}

		/// <summary>Gets a read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> object that formats values based on the current culture.</summary>
		/// <returns>A read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> object based on the <see cref="T:System.Globalization.CultureInfo" /> object for the current thread.</returns>
		// Token: 0x170006DA RID: 1754
		// (get) Token: 0x06002D07 RID: 11527 RVA: 0x0009E624 File Offset: 0x0009C824
		public static DateTimeFormatInfo CurrentInfo
		{
			get
			{
				CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
				if (!currentCulture.m_isInherited)
				{
					DateTimeFormatInfo dateTimeInfo = currentCulture.dateTimeInfo;
					if (dateTimeInfo != null)
					{
						return dateTimeInfo;
					}
				}
				return (DateTimeFormatInfo)currentCulture.GetFormat(typeof(DateTimeFormatInfo));
			}
		}

		/// <summary>Returns the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object associated with the specified <see cref="T:System.IFormatProvider" />.</summary>
		/// <param name="provider">The <see cref="T:System.IFormatProvider" /> that gets the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.-or- 
		///       <see langword="null" /> to get <see cref="P:System.Globalization.DateTimeFormatInfo.CurrentInfo" />. </param>
		/// <returns>A <see cref="T:System.Globalization.DateTimeFormatInfo" /> object associated with <see cref="T:System.IFormatProvider" />.</returns>
		// Token: 0x06002D08 RID: 11528 RVA: 0x0009E668 File Offset: 0x0009C868
		public static DateTimeFormatInfo GetInstance(IFormatProvider provider)
		{
			CultureInfo cultureInfo = provider as CultureInfo;
			if (cultureInfo != null && !cultureInfo.m_isInherited)
			{
				return cultureInfo.DateTimeFormat;
			}
			DateTimeFormatInfo dateTimeFormatInfo = provider as DateTimeFormatInfo;
			if (dateTimeFormatInfo != null)
			{
				return dateTimeFormatInfo;
			}
			if (provider != null)
			{
				dateTimeFormatInfo = (provider.GetFormat(typeof(DateTimeFormatInfo)) as DateTimeFormatInfo);
				if (dateTimeFormatInfo != null)
				{
					return dateTimeFormatInfo;
				}
			}
			return DateTimeFormatInfo.CurrentInfo;
		}

		/// <summary>Returns an object of the specified type that provides a date and time  formatting service.</summary>
		/// <param name="formatType">The type of the required formatting service. </param>
		/// <returns>The current  object, if <paramref name="formatType" /> is the same as the type of the current <see cref="T:System.Globalization.DateTimeFormatInfo" />; otherwise, <see langword="null" />.</returns>
		// Token: 0x06002D09 RID: 11529 RVA: 0x0009E6BD File Offset: 0x0009C8BD
		public object GetFormat(Type formatType)
		{
			if (!(formatType == typeof(DateTimeFormatInfo)))
			{
				return null;
			}
			return this;
		}

		/// <summary>Creates a shallow copy of the <see cref="T:System.Globalization.DateTimeFormatInfo" />.</summary>
		/// <returns>A new <see cref="T:System.Globalization.DateTimeFormatInfo" /> object copied from the original <see cref="T:System.Globalization.DateTimeFormatInfo" />.</returns>
		// Token: 0x06002D0A RID: 11530 RVA: 0x0009E6D4 File Offset: 0x0009C8D4
		public object Clone()
		{
			DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)base.MemberwiseClone();
			dateTimeFormatInfo.calendar = (Calendar)this.Calendar.Clone();
			dateTimeFormatInfo.m_isReadOnly = false;
			return dateTimeFormatInfo;
		}

		/// <summary>Gets or sets the string designator for hours that are "ante meridiem" (before noon).</summary>
		/// <returns>The string designator for hours that are ante meridiem. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is "AM".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006DB RID: 1755
		// (get) Token: 0x06002D0B RID: 11531 RVA: 0x0009E6FE File Offset: 0x0009C8FE
		// (set) Token: 0x06002D0C RID: 11532 RVA: 0x0009E706 File Offset: 0x0009C906
		public string AMDesignator
		{
			get
			{
				return this.amDesignator;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.ClearTokenHashTable();
				this.amDesignator = value;
			}
		}

		/// <summary>Gets or sets the calendar to use for the current culture.</summary>
		/// <returns>The calendar to use for the current culture. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is a <see cref="T:System.Globalization.GregorianCalendar" /> object.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is being set to a <see cref="T:System.Globalization.Calendar" /> object that is not valid for the current culture. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006DC RID: 1756
		// (get) Token: 0x06002D0D RID: 11533 RVA: 0x0009E745 File Offset: 0x0009C945
		// (set) Token: 0x06002D0E RID: 11534 RVA: 0x0009E750 File Offset: 0x0009C950
		public Calendar Calendar
		{
			get
			{
				return this.calendar;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Object cannot be null."));
				}
				if (value == this.calendar)
				{
					return;
				}
				CultureInfo.CheckDomainSafetyObject(value, this);
				for (int i = 0; i < this.OptionalCalendars.Length; i++)
				{
					if (this.OptionalCalendars[i] == value.ID)
					{
						if (this.calendar != null)
						{
							this.m_eraNames = null;
							this.m_abbrevEraNames = null;
							this.m_abbrevEnglishEraNames = null;
							this.monthDayPattern = null;
							this.dayNames = null;
							this.abbreviatedDayNames = null;
							this.m_superShortDayNames = null;
							this.monthNames = null;
							this.abbreviatedMonthNames = null;
							this.genitiveMonthNames = null;
							this.m_genitiveAbbreviatedMonthNames = null;
							this.leapYearMonthNames = null;
							this.formatFlags = DateTimeFormatFlags.NotInitialized;
							this.allShortDatePatterns = null;
							this.allLongDatePatterns = null;
							this.allYearMonthPatterns = null;
							this.dateTimeOffsetPattern = null;
							this.longDatePattern = null;
							this.shortDatePattern = null;
							this.yearMonthPattern = null;
							this.fullDateTimePattern = null;
							this.generalShortTimePattern = null;
							this.generalLongTimePattern = null;
							this.dateSeparator = null;
							this.ClearTokenHashTable();
						}
						this.calendar = value;
						this.InitializeOverridableProperties(this.m_cultureData, this.calendar.ID);
						return;
					}
				}
				throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("Not a valid calendar for the given culture."));
			}
		}

		// Token: 0x170006DD RID: 1757
		// (get) Token: 0x06002D0F RID: 11535 RVA: 0x0009E8B6 File Offset: 0x0009CAB6
		private int[] OptionalCalendars
		{
			get
			{
				if (this.optionalCalendars == null)
				{
					this.optionalCalendars = this.m_cultureData.CalendarIds;
				}
				return this.optionalCalendars;
			}
		}

		/// <summary>Returns the integer representing the specified era.</summary>
		/// <param name="eraName">The string containing the name of the era. </param>
		/// <returns>The integer representing the era, if <paramref name="eraName" /> is valid; otherwise, -1.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="eraName" /> is <see langword="null" />. </exception>
		// Token: 0x06002D10 RID: 11536 RVA: 0x0009E8D8 File Offset: 0x0009CAD8
		public int GetEra(string eraName)
		{
			if (eraName == null)
			{
				throw new ArgumentNullException("eraName", Environment.GetResourceString("String reference not set to an instance of a String."));
			}
			if (eraName.Length == 0)
			{
				return -1;
			}
			for (int i = 0; i < this.EraNames.Length; i++)
			{
				if (this.m_eraNames[i].Length > 0 && string.Compare(eraName, this.m_eraNames[i], this.Culture, CompareOptions.IgnoreCase) == 0)
				{
					return i + 1;
				}
			}
			for (int j = 0; j < this.AbbreviatedEraNames.Length; j++)
			{
				if (string.Compare(eraName, this.m_abbrevEraNames[j], this.Culture, CompareOptions.IgnoreCase) == 0)
				{
					return j + 1;
				}
			}
			for (int k = 0; k < this.AbbreviatedEnglishEraNames.Length; k++)
			{
				if (string.Compare(eraName, this.m_abbrevEnglishEraNames[k], StringComparison.InvariantCultureIgnoreCase) == 0)
				{
					return k + 1;
				}
			}
			return -1;
		}

		// Token: 0x170006DE RID: 1758
		// (get) Token: 0x06002D11 RID: 11537 RVA: 0x0009E99C File Offset: 0x0009CB9C
		internal string[] EraNames
		{
			get
			{
				if (this.m_eraNames == null)
				{
					this.m_eraNames = this.m_cultureData.EraNames(this.Calendar.ID);
				}
				return this.m_eraNames;
			}
		}

		/// <summary>Returns the string containing the name of the specified era.</summary>
		/// <param name="era">The integer representing the era. </param>
		/// <returns>A string containing the name of the era.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="era" /> does not represent a valid era in the calendar specified in the <see cref="P:System.Globalization.DateTimeFormatInfo.Calendar" /> property. </exception>
		// Token: 0x06002D12 RID: 11538 RVA: 0x0009E9C8 File Offset: 0x0009CBC8
		public string GetEraName(int era)
		{
			if (era == 0)
			{
				era = this.Calendar.CurrentEraValue;
			}
			if (--era < this.EraNames.Length && era >= 0)
			{
				return this.m_eraNames[era];
			}
			throw new ArgumentOutOfRangeException("era", Environment.GetResourceString("Era value was not valid."));
		}

		// Token: 0x170006DF RID: 1759
		// (get) Token: 0x06002D13 RID: 11539 RVA: 0x0009EA16 File Offset: 0x0009CC16
		internal string[] AbbreviatedEraNames
		{
			get
			{
				if (this.m_abbrevEraNames == null)
				{
					this.m_abbrevEraNames = this.m_cultureData.AbbrevEraNames(this.Calendar.ID);
				}
				return this.m_abbrevEraNames;
			}
		}

		/// <summary>Returns the string containing the abbreviated name of the specified era, if an abbreviation exists.</summary>
		/// <param name="era">The integer representing the era. </param>
		/// <returns>A string containing the abbreviated name of the specified era, if an abbreviation exists.-or- A string containing the full name of the era, if an abbreviation does not exist.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="era" /> does not represent a valid era in the calendar specified in the <see cref="P:System.Globalization.DateTimeFormatInfo.Calendar" /> property. </exception>
		// Token: 0x06002D14 RID: 11540 RVA: 0x0009EA44 File Offset: 0x0009CC44
		public string GetAbbreviatedEraName(int era)
		{
			if (this.AbbreviatedEraNames.Length == 0)
			{
				return this.GetEraName(era);
			}
			if (era == 0)
			{
				era = this.Calendar.CurrentEraValue;
			}
			if (--era < this.m_abbrevEraNames.Length && era >= 0)
			{
				return this.m_abbrevEraNames[era];
			}
			throw new ArgumentOutOfRangeException("era", Environment.GetResourceString("Era value was not valid."));
		}

		// Token: 0x170006E0 RID: 1760
		// (get) Token: 0x06002D15 RID: 11541 RVA: 0x0009EAA3 File Offset: 0x0009CCA3
		internal string[] AbbreviatedEnglishEraNames
		{
			get
			{
				if (this.m_abbrevEnglishEraNames == null)
				{
					this.m_abbrevEnglishEraNames = this.m_cultureData.AbbreviatedEnglishEraNames(this.Calendar.ID);
				}
				return this.m_abbrevEnglishEraNames;
			}
		}

		/// <summary>Gets or sets the string that separates the components of a date, that is, the year, month, and day.</summary>
		/// <returns>The string that separates the components of a date, that is, the year, month, and day. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is "/".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006E1 RID: 1761
		// (get) Token: 0x06002D16 RID: 11542 RVA: 0x0009EACF File Offset: 0x0009CCCF
		// (set) Token: 0x06002D17 RID: 11543 RVA: 0x0009EAD7 File Offset: 0x0009CCD7
		public string DateSeparator
		{
			get
			{
				return this.dateSeparator;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.ClearTokenHashTable();
				this.dateSeparator = value;
			}
		}

		/// <summary>Gets or sets the first day of the week.</summary>
		/// <returns>An enumeration value that represents the first day of the week. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is <see cref="F:System.DayOfWeek.Sunday" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is being set to a value that is not a valid <see cref="T:System.DayOfWeek" /> value. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006E2 RID: 1762
		// (get) Token: 0x06002D18 RID: 11544 RVA: 0x0009EB16 File Offset: 0x0009CD16
		// (set) Token: 0x06002D19 RID: 11545 RVA: 0x0009EB20 File Offset: 0x0009CD20
		public DayOfWeek FirstDayOfWeek
		{
			get
			{
				return (DayOfWeek)this.firstDayOfWeek;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value >= DayOfWeek.Sunday && value <= DayOfWeek.Saturday)
				{
					this.firstDayOfWeek = (int)value;
					return;
				}
				throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					DayOfWeek.Sunday,
					DayOfWeek.Saturday
				}));
			}
		}

		/// <summary>Gets or sets a value that specifies which rule is used to determine the first calendar week of the year.</summary>
		/// <returns>A value that determines the first calendar week of the year. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is <see cref="F:System.Globalization.CalendarWeekRule.FirstDay" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is being set to a value that is not a valid <see cref="T:System.Globalization.CalendarWeekRule" /> value. </exception>
		/// <exception cref="T:System.InvalidOperationException">In a set operation, the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only.</exception>
		// Token: 0x170006E3 RID: 1763
		// (get) Token: 0x06002D1A RID: 11546 RVA: 0x0009EB81 File Offset: 0x0009CD81
		// (set) Token: 0x06002D1B RID: 11547 RVA: 0x0009EB8C File Offset: 0x0009CD8C
		public CalendarWeekRule CalendarWeekRule
		{
			get
			{
				return (CalendarWeekRule)this.calendarWeekRule;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value >= CalendarWeekRule.FirstDay && value <= CalendarWeekRule.FirstFourDayWeek)
				{
					this.calendarWeekRule = (int)value;
					return;
				}
				throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					CalendarWeekRule.FirstDay,
					CalendarWeekRule.FirstFourDayWeek
				}));
			}
		}

		/// <summary>Gets or sets the custom format string for a long date and long time value.</summary>
		/// <returns>The custom format string for a long date and long time value.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006E4 RID: 1764
		// (get) Token: 0x06002D1C RID: 11548 RVA: 0x0009EBED File Offset: 0x0009CDED
		// (set) Token: 0x06002D1D RID: 11549 RVA: 0x0009EC19 File Offset: 0x0009CE19
		public string FullDateTimePattern
		{
			get
			{
				if (this.fullDateTimePattern == null)
				{
					this.fullDateTimePattern = this.LongDatePattern + " " + this.LongTimePattern;
				}
				return this.fullDateTimePattern;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.fullDateTimePattern = value;
			}
		}

		/// <summary>Gets or sets the custom format string for a long date value.</summary>
		/// <returns>The custom format string for a long date value.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006E5 RID: 1765
		// (get) Token: 0x06002D1E RID: 11550 RVA: 0x0009EC52 File Offset: 0x0009CE52
		// (set) Token: 0x06002D1F RID: 11551 RVA: 0x0009EC70 File Offset: 0x0009CE70
		public string LongDatePattern
		{
			get
			{
				if (this.longDatePattern == null)
				{
					this.longDatePattern = this.UnclonedLongDatePatterns[0];
				}
				return this.longDatePattern;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.longDatePattern = value;
				this.ClearTokenHashTable();
				this.fullDateTimePattern = null;
			}
		}

		/// <summary>Gets or sets the custom format string for a long time value.</summary>
		/// <returns>The format pattern for a long time value.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006E6 RID: 1766
		// (get) Token: 0x06002D20 RID: 11552 RVA: 0x0009ECC1 File Offset: 0x0009CEC1
		// (set) Token: 0x06002D21 RID: 11553 RVA: 0x0009ECE0 File Offset: 0x0009CEE0
		public string LongTimePattern
		{
			get
			{
				if (this.longTimePattern == null)
				{
					this.longTimePattern = this.UnclonedLongTimePatterns[0];
				}
				return this.longTimePattern;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.longTimePattern = value;
				this.ClearTokenHashTable();
				this.fullDateTimePattern = null;
				this.generalLongTimePattern = null;
				this.dateTimeOffsetPattern = null;
			}
		}

		/// <summary>Gets or sets the custom format string for a month and day value.</summary>
		/// <returns>The custom format string for a month and day value.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006E7 RID: 1767
		// (get) Token: 0x06002D22 RID: 11554 RVA: 0x0009ED3F File Offset: 0x0009CF3F
		// (set) Token: 0x06002D23 RID: 11555 RVA: 0x0009ED6B File Offset: 0x0009CF6B
		public string MonthDayPattern
		{
			get
			{
				if (this.monthDayPattern == null)
				{
					this.monthDayPattern = this.m_cultureData.MonthDay(this.Calendar.ID);
				}
				return this.monthDayPattern;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.monthDayPattern = value;
			}
		}

		/// <summary>Gets or sets the string designator for hours that are "post meridiem" (after noon).</summary>
		/// <returns>The string designator for hours that are "post meridiem" (after noon). The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is "PM".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006E8 RID: 1768
		// (get) Token: 0x06002D24 RID: 11556 RVA: 0x0009EDA4 File Offset: 0x0009CFA4
		// (set) Token: 0x06002D25 RID: 11557 RVA: 0x0009EDAC File Offset: 0x0009CFAC
		public string PMDesignator
		{
			get
			{
				return this.pmDesignator;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.ClearTokenHashTable();
				this.pmDesignator = value;
			}
		}

		/// <summary>Gets the custom format string for a time value that is based on the Internet Engineering Task Force (IETF) Request for Comments (RFC) 1123 specification.</summary>
		/// <returns>The custom format string for a time value that is based on the IETF RFC 1123 specification.</returns>
		// Token: 0x170006E9 RID: 1769
		// (get) Token: 0x06002D26 RID: 11558 RVA: 0x0009EDEB File Offset: 0x0009CFEB
		public string RFC1123Pattern
		{
			get
			{
				return "ddd, dd MMM yyyy HH':'mm':'ss 'GMT'";
			}
		}

		/// <summary>Gets or sets the custom format string for a short date value.</summary>
		/// <returns>The custom format string for a short date value.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006EA RID: 1770
		// (get) Token: 0x06002D27 RID: 11559 RVA: 0x0009EDF2 File Offset: 0x0009CFF2
		// (set) Token: 0x06002D28 RID: 11560 RVA: 0x0009EE10 File Offset: 0x0009D010
		public string ShortDatePattern
		{
			get
			{
				if (this.shortDatePattern == null)
				{
					this.shortDatePattern = this.UnclonedShortDatePatterns[0];
				}
				return this.shortDatePattern;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.shortDatePattern = value;
				this.ClearTokenHashTable();
				this.generalLongTimePattern = null;
				this.generalShortTimePattern = null;
				this.dateTimeOffsetPattern = null;
			}
		}

		/// <summary>Gets or sets the custom format string for a short time value.</summary>
		/// <returns>The custom format string for a short time value.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006EB RID: 1771
		// (get) Token: 0x06002D29 RID: 11561 RVA: 0x0009EE6F File Offset: 0x0009D06F
		// (set) Token: 0x06002D2A RID: 11562 RVA: 0x0009EE90 File Offset: 0x0009D090
		public string ShortTimePattern
		{
			get
			{
				if (this.shortTimePattern == null)
				{
					this.shortTimePattern = this.UnclonedShortTimePatterns[0];
				}
				return this.shortTimePattern;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.shortTimePattern = value;
				this.ClearTokenHashTable();
				this.generalShortTimePattern = null;
			}
		}

		/// <summary>Gets the custom format string for a sortable date and time value.</summary>
		/// <returns>The custom format string for a sortable date and time value.</returns>
		// Token: 0x170006EC RID: 1772
		// (get) Token: 0x06002D2B RID: 11563 RVA: 0x0009EEE1 File Offset: 0x0009D0E1
		public string SortableDateTimePattern
		{
			get
			{
				return "yyyy'-'MM'-'dd'T'HH':'mm':'ss";
			}
		}

		// Token: 0x170006ED RID: 1773
		// (get) Token: 0x06002D2C RID: 11564 RVA: 0x0009EEE8 File Offset: 0x0009D0E8
		internal string GeneralShortTimePattern
		{
			get
			{
				if (this.generalShortTimePattern == null)
				{
					this.generalShortTimePattern = this.ShortDatePattern + " " + this.ShortTimePattern;
				}
				return this.generalShortTimePattern;
			}
		}

		// Token: 0x170006EE RID: 1774
		// (get) Token: 0x06002D2D RID: 11565 RVA: 0x0009EF14 File Offset: 0x0009D114
		internal string GeneralLongTimePattern
		{
			get
			{
				if (this.generalLongTimePattern == null)
				{
					this.generalLongTimePattern = this.ShortDatePattern + " " + this.LongTimePattern;
				}
				return this.generalLongTimePattern;
			}
		}

		// Token: 0x170006EF RID: 1775
		// (get) Token: 0x06002D2E RID: 11566 RVA: 0x0009EF40 File Offset: 0x0009D140
		internal string DateTimeOffsetPattern
		{
			get
			{
				if (this.dateTimeOffsetPattern == null)
				{
					this.dateTimeOffsetPattern = this.ShortDatePattern + " " + this.LongTimePattern;
					bool flag = false;
					bool flag2 = false;
					char c = '\'';
					int num = 0;
					while (!flag && num < this.LongTimePattern.Length)
					{
						char c2 = this.LongTimePattern[num];
						if (c2 <= '%')
						{
							if (c2 == '"')
							{
								goto IL_6D;
							}
							if (c2 == '%')
							{
								goto IL_97;
							}
						}
						else
						{
							if (c2 == '\'')
							{
								goto IL_6D;
							}
							if (c2 == '\\')
							{
								goto IL_97;
							}
							if (c2 == 'z')
							{
								flag = !flag2;
							}
						}
						IL_9B:
						num++;
						continue;
						IL_6D:
						if (flag2 && c == this.LongTimePattern[num])
						{
							flag2 = false;
							goto IL_9B;
						}
						if (!flag2)
						{
							c = this.LongTimePattern[num];
							flag2 = true;
							goto IL_9B;
						}
						goto IL_9B;
						IL_97:
						num++;
						goto IL_9B;
					}
					if (!flag)
					{
						this.dateTimeOffsetPattern += " zzz";
					}
				}
				return this.dateTimeOffsetPattern;
			}
		}

		/// <summary>Gets or sets the string that separates the components of time, that is, the hour, minutes, and seconds.</summary>
		/// <returns>The string that separates the components of time. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is ":".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006F0 RID: 1776
		// (get) Token: 0x06002D2F RID: 11567 RVA: 0x0009F01C File Offset: 0x0009D21C
		// (set) Token: 0x06002D30 RID: 11568 RVA: 0x0009F024 File Offset: 0x0009D224
		public string TimeSeparator
		{
			get
			{
				return this.timeSeparator;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.ClearTokenHashTable();
				this.timeSeparator = value;
			}
		}

		/// <summary>Gets the custom format string for a universal, sortable date and time string.</summary>
		/// <returns>The custom format string for a universal, sortable date and time string.</returns>
		// Token: 0x170006F1 RID: 1777
		// (get) Token: 0x06002D31 RID: 11569 RVA: 0x0009F063 File Offset: 0x0009D263
		public string UniversalSortableDateTimePattern
		{
			get
			{
				return "yyyy'-'MM'-'dd HH':'mm':'ss'Z'";
			}
		}

		/// <summary>Gets or sets the custom format string for a year and month value.</summary>
		/// <returns>The custom format string for a year and month value.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006F2 RID: 1778
		// (get) Token: 0x06002D32 RID: 11570 RVA: 0x0009F06A File Offset: 0x0009D26A
		// (set) Token: 0x06002D33 RID: 11571 RVA: 0x0009F088 File Offset: 0x0009D288
		public string YearMonthPattern
		{
			get
			{
				if (this.yearMonthPattern == null)
				{
					this.yearMonthPattern = this.UnclonedYearMonthPatterns[0];
				}
				return this.yearMonthPattern;
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("String reference not set to an instance of a String."));
				}
				this.yearMonthPattern = value;
				this.ClearTokenHashTable();
			}
		}

		// Token: 0x06002D34 RID: 11572 RVA: 0x0009F0C8 File Offset: 0x0009D2C8
		private static void CheckNullValue(string[] values, int length)
		{
			for (int i = 0; i < length; i++)
			{
				if (values[i] == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Found a null value within an array."));
				}
			}
		}

		/// <summary>Gets or sets a one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific abbreviated names of the days of the week.</summary>
		/// <returns>A one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific abbreviated names of the days of the week. The array for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> contains "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", and "Sat".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to an array that is multidimensional or that has a length that is not exactly 7. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006F3 RID: 1779
		// (get) Token: 0x06002D35 RID: 11573 RVA: 0x0009F0FB File Offset: 0x0009D2FB
		// (set) Token: 0x06002D36 RID: 11574 RVA: 0x0009F110 File Offset: 0x0009D310
		public string[] AbbreviatedDayNames
		{
			get
			{
				return (string[])this.internalGetAbbreviatedDayOfWeekNames().Clone();
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Array cannot be null."));
				}
				if (value.Length != 7)
				{
					throw new ArgumentException(Environment.GetResourceString("Length of the array must be {0}.", new object[]
					{
						7
					}), "value");
				}
				DateTimeFormatInfo.CheckNullValue(value, value.Length);
				this.ClearTokenHashTable();
				this.abbreviatedDayNames = value;
			}
		}

		/// <summary>Gets or sets a string array of the shortest unique abbreviated day names associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>A string array of day names.</returns>
		/// <exception cref="T:System.ArgumentException">In a set operation, the array does not have exactly seven elements.</exception>
		/// <exception cref="T:System.ArgumentNullException">In a set operation, the value array or one of the elements of the value array is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">In a set operation, the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only.</exception>
		// Token: 0x170006F4 RID: 1780
		// (get) Token: 0x06002D37 RID: 11575 RVA: 0x0009F18D File Offset: 0x0009D38D
		// (set) Token: 0x06002D38 RID: 11576 RVA: 0x0009F1A0 File Offset: 0x0009D3A0
		[ComVisible(false)]
		public string[] ShortestDayNames
		{
			get
			{
				return (string[])this.internalGetSuperShortDayNames().Clone();
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Array cannot be null."));
				}
				if (value.Length != 7)
				{
					throw new ArgumentException(Environment.GetResourceString("Length of the array must be {0}.", new object[]
					{
						7
					}), "value");
				}
				DateTimeFormatInfo.CheckNullValue(value, value.Length);
				this.m_superShortDayNames = value;
			}
		}

		/// <summary>Gets or sets a one-dimensional string array that contains the culture-specific full names of the days of the week.</summary>
		/// <returns>A one-dimensional string array that contains the culture-specific full names of the days of the week. The array for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> contains "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", and "Saturday".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to an array that is multidimensional or that has a length that is not exactly 7. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006F5 RID: 1781
		// (get) Token: 0x06002D39 RID: 11577 RVA: 0x0009F217 File Offset: 0x0009D417
		// (set) Token: 0x06002D3A RID: 11578 RVA: 0x0009F22C File Offset: 0x0009D42C
		public string[] DayNames
		{
			get
			{
				return (string[])this.internalGetDayOfWeekNames().Clone();
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Array cannot be null."));
				}
				if (value.Length != 7)
				{
					throw new ArgumentException(Environment.GetResourceString("Length of the array must be {0}.", new object[]
					{
						7
					}), "value");
				}
				DateTimeFormatInfo.CheckNullValue(value, value.Length);
				this.ClearTokenHashTable();
				this.dayNames = value;
			}
		}

		/// <summary>Gets or sets a one-dimensional string array that contains the culture-specific abbreviated names of the months.</summary>
		/// <returns>A one-dimensional string array with 13 elements that contains the culture-specific abbreviated names of the months. For 12-month calendars, the 13th element of the array is an empty string. The array for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> contains "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", and "".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to an array that is multidimensional or that has a length that is not exactly 13. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006F6 RID: 1782
		// (get) Token: 0x06002D3B RID: 11579 RVA: 0x0009F2A9 File Offset: 0x0009D4A9
		// (set) Token: 0x06002D3C RID: 11580 RVA: 0x0009F2BC File Offset: 0x0009D4BC
		public string[] AbbreviatedMonthNames
		{
			get
			{
				return (string[])this.internalGetAbbreviatedMonthNames().Clone();
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Array cannot be null."));
				}
				if (value.Length != 13)
				{
					throw new ArgumentException(Environment.GetResourceString("Length of the array must be {0}.", new object[]
					{
						13
					}), "value");
				}
				DateTimeFormatInfo.CheckNullValue(value, value.Length - 1);
				this.ClearTokenHashTable();
				this.abbreviatedMonthNames = value;
			}
		}

		/// <summary>Gets or sets a one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific full names of the months.</summary>
		/// <returns>A one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific full names of the months. In a 12-month calendar, the 13th element of the array is an empty string. The array for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> contains "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", and "".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to an array that is multidimensional or that has a length that is not exactly 13. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only. </exception>
		// Token: 0x170006F7 RID: 1783
		// (get) Token: 0x06002D3D RID: 11581 RVA: 0x0009F33D File Offset: 0x0009D53D
		// (set) Token: 0x06002D3E RID: 11582 RVA: 0x0009F350 File Offset: 0x0009D550
		public string[] MonthNames
		{
			get
			{
				return (string[])this.internalGetMonthNames().Clone();
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Array cannot be null."));
				}
				if (value.Length != 13)
				{
					throw new ArgumentException(Environment.GetResourceString("Length of the array must be {0}.", new object[]
					{
						13
					}), "value");
				}
				DateTimeFormatInfo.CheckNullValue(value, value.Length - 1);
				this.monthNames = value;
				this.ClearTokenHashTable();
			}
		}

		// Token: 0x170006F8 RID: 1784
		// (get) Token: 0x06002D3F RID: 11583 RVA: 0x0009F3D1 File Offset: 0x0009D5D1
		internal bool HasSpacesInMonthNames
		{
			get
			{
				return (this.FormatFlags & DateTimeFormatFlags.UseSpacesInMonthNames) > DateTimeFormatFlags.None;
			}
		}

		// Token: 0x170006F9 RID: 1785
		// (get) Token: 0x06002D40 RID: 11584 RVA: 0x0009F3DE File Offset: 0x0009D5DE
		internal bool HasSpacesInDayNames
		{
			get
			{
				return (this.FormatFlags & DateTimeFormatFlags.UseSpacesInDayNames) > DateTimeFormatFlags.None;
			}
		}

		// Token: 0x06002D41 RID: 11585 RVA: 0x0009F3EC File Offset: 0x0009D5EC
		internal string internalGetMonthName(int month, MonthNameStyles style, bool abbreviated)
		{
			string[] array;
			if (style != MonthNameStyles.Genitive)
			{
				if (style != MonthNameStyles.LeapYear)
				{
					array = (abbreviated ? this.internalGetAbbreviatedMonthNames() : this.internalGetMonthNames());
				}
				else
				{
					array = this.internalGetLeapYearMonthNames();
				}
			}
			else
			{
				array = this.internalGetGenitiveMonthNames(abbreviated);
			}
			if (month < 1 || month > array.Length)
			{
				throw new ArgumentOutOfRangeException("month", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					1,
					array.Length
				}));
			}
			return array[month - 1];
		}

		// Token: 0x06002D42 RID: 11586 RVA: 0x0009F468 File Offset: 0x0009D668
		private string[] internalGetGenitiveMonthNames(bool abbreviated)
		{
			if (abbreviated)
			{
				if (this.m_genitiveAbbreviatedMonthNames == null)
				{
					this.m_genitiveAbbreviatedMonthNames = this.m_cultureData.AbbreviatedGenitiveMonthNames(this.Calendar.ID);
				}
				return this.m_genitiveAbbreviatedMonthNames;
			}
			if (this.genitiveMonthNames == null)
			{
				this.genitiveMonthNames = this.m_cultureData.GenitiveMonthNames(this.Calendar.ID);
			}
			return this.genitiveMonthNames;
		}

		// Token: 0x06002D43 RID: 11587 RVA: 0x0009F4CD File Offset: 0x0009D6CD
		internal string[] internalGetLeapYearMonthNames()
		{
			if (this.leapYearMonthNames == null)
			{
				this.leapYearMonthNames = this.m_cultureData.LeapYearMonthNames(this.Calendar.ID);
			}
			return this.leapYearMonthNames;
		}

		/// <summary>Returns the culture-specific abbreviated name of the specified day of the week based on the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <param name="dayofweek">A <see cref="T:System.DayOfWeek" /> value. </param>
		/// <returns>The culture-specific abbreviated name of the day of the week represented by <paramref name="dayofweek" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="dayofweek" /> is not a valid <see cref="T:System.DayOfWeek" /> value. </exception>
		// Token: 0x06002D44 RID: 11588 RVA: 0x0009F4F9 File Offset: 0x0009D6F9
		public string GetAbbreviatedDayName(DayOfWeek dayofweek)
		{
			if (dayofweek < DayOfWeek.Sunday || dayofweek > DayOfWeek.Saturday)
			{
				throw new ArgumentOutOfRangeException("dayofweek", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					DayOfWeek.Sunday,
					DayOfWeek.Saturday
				}));
			}
			return this.internalGetAbbreviatedDayOfWeekNames()[(int)dayofweek];
		}

		/// <summary>Obtains the shortest abbreviated day name for a specified day of the week associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <param name="dayOfWeek">One of the <see cref="T:System.DayOfWeek" /> values.</param>
		/// <returns>The abbreviated name of the week that corresponds to the <paramref name="dayOfWeek" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="dayOfWeek" /> is not a value in the <see cref="T:System.DayOfWeek" /> enumeration.</exception>
		// Token: 0x06002D45 RID: 11589 RVA: 0x0009F538 File Offset: 0x0009D738
		[ComVisible(false)]
		public string GetShortestDayName(DayOfWeek dayOfWeek)
		{
			if (dayOfWeek < DayOfWeek.Sunday || dayOfWeek > DayOfWeek.Saturday)
			{
				throw new ArgumentOutOfRangeException("dayOfWeek", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					DayOfWeek.Sunday,
					DayOfWeek.Saturday
				}));
			}
			return this.internalGetSuperShortDayNames()[(int)dayOfWeek];
		}

		// Token: 0x06002D46 RID: 11590 RVA: 0x0009F578 File Offset: 0x0009D778
		private static string[] GetCombinedPatterns(string[] patterns1, string[] patterns2, string connectString)
		{
			string[] array = new string[patterns1.Length * patterns2.Length];
			int num = 0;
			for (int i = 0; i < patterns1.Length; i++)
			{
				for (int j = 0; j < patterns2.Length; j++)
				{
					array[num++] = patterns1[i] + connectString + patterns2[j];
				}
			}
			return array;
		}

		/// <summary>Returns all the standard patterns in which date and time values can be formatted.</summary>
		/// <returns>An array that contains the standard patterns in which date and time values can be formatted.</returns>
		// Token: 0x06002D47 RID: 11591 RVA: 0x0009F5C4 File Offset: 0x0009D7C4
		public string[] GetAllDateTimePatterns()
		{
			List<string> list = new List<string>(132);
			for (int i = 0; i < DateTimeFormat.allStandardFormats.Length; i++)
			{
				string[] allDateTimePatterns = this.GetAllDateTimePatterns(DateTimeFormat.allStandardFormats[i]);
				for (int j = 0; j < allDateTimePatterns.Length; j++)
				{
					list.Add(allDateTimePatterns[j]);
				}
			}
			return list.ToArray();
		}

		/// <summary>Returns all the patterns in which date and time values can be formatted using the specified standard format string.</summary>
		/// <param name="format">A standard format string. </param>
		/// <returns>An array containing the standard patterns in which date and time values can be formatted using the specified format string.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="format" /> is not a valid standard format string. </exception>
		// Token: 0x06002D48 RID: 11592 RVA: 0x0009F61C File Offset: 0x0009D81C
		public string[] GetAllDateTimePatterns(char format)
		{
			if (format <= 'U')
			{
				switch (format)
				{
				case 'D':
					return this.AllLongDatePatterns;
				case 'E':
					goto IL_1AF;
				case 'F':
					break;
				case 'G':
					return DateTimeFormatInfo.GetCombinedPatterns(this.AllShortDatePatterns, this.AllLongTimePatterns, " ");
				default:
					switch (format)
					{
					case 'M':
						goto IL_13D;
					case 'N':
					case 'P':
					case 'Q':
					case 'S':
						goto IL_1AF;
					case 'O':
						goto IL_14F;
					case 'R':
						goto IL_160;
					case 'T':
						return this.AllLongTimePatterns;
					case 'U':
						break;
					default:
						goto IL_1AF;
					}
					break;
				}
				return DateTimeFormatInfo.GetCombinedPatterns(this.AllLongDatePatterns, this.AllLongTimePatterns, " ");
			}
			if (format != 'Y')
			{
				switch (format)
				{
				case 'd':
					return this.AllShortDatePatterns;
				case 'e':
					goto IL_1AF;
				case 'f':
					return DateTimeFormatInfo.GetCombinedPatterns(this.AllLongDatePatterns, this.AllShortTimePatterns, " ");
				case 'g':
					return DateTimeFormatInfo.GetCombinedPatterns(this.AllShortDatePatterns, this.AllShortTimePatterns, " ");
				default:
					switch (format)
					{
					case 'm':
						goto IL_13D;
					case 'n':
					case 'p':
					case 'q':
					case 'v':
					case 'w':
					case 'x':
						goto IL_1AF;
					case 'o':
						goto IL_14F;
					case 'r':
						goto IL_160;
					case 's':
						return new string[]
						{
							"yyyy'-'MM'-'dd'T'HH':'mm':'ss"
						};
					case 't':
						return this.AllShortTimePatterns;
					case 'u':
						return new string[]
						{
							this.UniversalSortableDateTimePattern
						};
					case 'y':
						break;
					default:
						goto IL_1AF;
					}
					break;
				}
			}
			return this.AllYearMonthPatterns;
			IL_13D:
			return new string[]
			{
				this.MonthDayPattern
			};
			IL_14F:
			return new string[]
			{
				"yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK"
			};
			IL_160:
			return new string[]
			{
				"ddd, dd MMM yyyy HH':'mm':'ss 'GMT'"
			};
			IL_1AF:
			throw new ArgumentException(Environment.GetResourceString("Format specifier was invalid."), "format");
		}

		/// <summary>Returns the culture-specific full name of the specified day of the week based on the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <param name="dayofweek">A <see cref="T:System.DayOfWeek" /> value. </param>
		/// <returns>The culture-specific full name of the day of the week represented by <paramref name="dayofweek" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="dayofweek" /> is not a valid <see cref="T:System.DayOfWeek" /> value. </exception>
		// Token: 0x06002D49 RID: 11593 RVA: 0x0009F7EE File Offset: 0x0009D9EE
		public string GetDayName(DayOfWeek dayofweek)
		{
			if (dayofweek < DayOfWeek.Sunday || dayofweek > DayOfWeek.Saturday)
			{
				throw new ArgumentOutOfRangeException("dayofweek", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					DayOfWeek.Sunday,
					DayOfWeek.Saturday
				}));
			}
			return this.internalGetDayOfWeekNames()[(int)dayofweek];
		}

		/// <summary>Returns the culture-specific abbreviated name of the specified month based on the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <param name="month">An integer from 1 through 13 representing the name of the month to retrieve. </param>
		/// <returns>The culture-specific abbreviated name of the month represented by <paramref name="month" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="month" /> is less than 1 or greater than 13. </exception>
		// Token: 0x06002D4A RID: 11594 RVA: 0x0009F830 File Offset: 0x0009DA30
		public string GetAbbreviatedMonthName(int month)
		{
			if (month < 1 || month > 13)
			{
				throw new ArgumentOutOfRangeException("month", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					1,
					13
				}));
			}
			return this.internalGetAbbreviatedMonthNames()[month - 1];
		}

		/// <summary>Returns the culture-specific full name of the specified month based on the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <param name="month">An integer from 1 through 13 representing the name of the month to retrieve. </param>
		/// <returns>The culture-specific full name of the month represented by <paramref name="month" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="month" /> is less than 1 or greater than 13. </exception>
		// Token: 0x06002D4B RID: 11595 RVA: 0x0009F880 File Offset: 0x0009DA80
		public string GetMonthName(int month)
		{
			if (month < 1 || month > 13)
			{
				throw new ArgumentOutOfRangeException("month", Environment.GetResourceString("Valid values are between {0} and {1}, inclusive.", new object[]
				{
					1,
					13
				}));
			}
			return this.internalGetMonthNames()[month - 1];
		}

		// Token: 0x06002D4C RID: 11596 RVA: 0x0009F8D0 File Offset: 0x0009DAD0
		private static string[] GetMergedPatterns(string[] patterns, string defaultPattern)
		{
			if (defaultPattern == patterns[0])
			{
				return (string[])patterns.Clone();
			}
			int num = 0;
			while (num < patterns.Length && !(defaultPattern == patterns[num]))
			{
				num++;
			}
			string[] array;
			if (num < patterns.Length)
			{
				array = (string[])patterns.Clone();
				array[num] = array[0];
			}
			else
			{
				array = new string[patterns.Length + 1];
				Array.Copy(patterns, 0, array, 1, patterns.Length);
			}
			array[0] = defaultPattern;
			return array;
		}

		// Token: 0x170006FA RID: 1786
		// (get) Token: 0x06002D4D RID: 11597 RVA: 0x0009F943 File Offset: 0x0009DB43
		private string[] AllYearMonthPatterns
		{
			get
			{
				return DateTimeFormatInfo.GetMergedPatterns(this.UnclonedYearMonthPatterns, this.YearMonthPattern);
			}
		}

		// Token: 0x170006FB RID: 1787
		// (get) Token: 0x06002D4E RID: 11598 RVA: 0x0009F956 File Offset: 0x0009DB56
		private string[] AllShortDatePatterns
		{
			get
			{
				return DateTimeFormatInfo.GetMergedPatterns(this.UnclonedShortDatePatterns, this.ShortDatePattern);
			}
		}

		// Token: 0x170006FC RID: 1788
		// (get) Token: 0x06002D4F RID: 11599 RVA: 0x0009F969 File Offset: 0x0009DB69
		private string[] AllShortTimePatterns
		{
			get
			{
				return DateTimeFormatInfo.GetMergedPatterns(this.UnclonedShortTimePatterns, this.ShortTimePattern);
			}
		}

		// Token: 0x170006FD RID: 1789
		// (get) Token: 0x06002D50 RID: 11600 RVA: 0x0009F97C File Offset: 0x0009DB7C
		private string[] AllLongDatePatterns
		{
			get
			{
				return DateTimeFormatInfo.GetMergedPatterns(this.UnclonedLongDatePatterns, this.LongDatePattern);
			}
		}

		// Token: 0x170006FE RID: 1790
		// (get) Token: 0x06002D51 RID: 11601 RVA: 0x0009F98F File Offset: 0x0009DB8F
		private string[] AllLongTimePatterns
		{
			get
			{
				return DateTimeFormatInfo.GetMergedPatterns(this.UnclonedLongTimePatterns, this.LongTimePattern);
			}
		}

		// Token: 0x170006FF RID: 1791
		// (get) Token: 0x06002D52 RID: 11602 RVA: 0x0009F9A2 File Offset: 0x0009DBA2
		private string[] UnclonedYearMonthPatterns
		{
			get
			{
				if (this.allYearMonthPatterns == null)
				{
					this.allYearMonthPatterns = this.m_cultureData.YearMonths(this.Calendar.ID);
				}
				return this.allYearMonthPatterns;
			}
		}

		// Token: 0x17000700 RID: 1792
		// (get) Token: 0x06002D53 RID: 11603 RVA: 0x0009F9CE File Offset: 0x0009DBCE
		private string[] UnclonedShortDatePatterns
		{
			get
			{
				if (this.allShortDatePatterns == null)
				{
					this.allShortDatePatterns = this.m_cultureData.ShortDates(this.Calendar.ID);
				}
				return this.allShortDatePatterns;
			}
		}

		// Token: 0x17000701 RID: 1793
		// (get) Token: 0x06002D54 RID: 11604 RVA: 0x0009F9FA File Offset: 0x0009DBFA
		private string[] UnclonedLongDatePatterns
		{
			get
			{
				if (this.allLongDatePatterns == null)
				{
					this.allLongDatePatterns = this.m_cultureData.LongDates(this.Calendar.ID);
				}
				return this.allLongDatePatterns;
			}
		}

		// Token: 0x17000702 RID: 1794
		// (get) Token: 0x06002D55 RID: 11605 RVA: 0x0009FA26 File Offset: 0x0009DC26
		private string[] UnclonedShortTimePatterns
		{
			get
			{
				if (this.allShortTimePatterns == null)
				{
					this.allShortTimePatterns = this.m_cultureData.ShortTimes;
				}
				return this.allShortTimePatterns;
			}
		}

		// Token: 0x17000703 RID: 1795
		// (get) Token: 0x06002D56 RID: 11606 RVA: 0x0009FA47 File Offset: 0x0009DC47
		private string[] UnclonedLongTimePatterns
		{
			get
			{
				if (this.allLongTimePatterns == null)
				{
					this.allLongTimePatterns = this.m_cultureData.LongTimes;
				}
				return this.allLongTimePatterns;
			}
		}

		/// <summary>Returns a read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> wrapper.</summary>
		/// <param name="dtfi">The <see cref="T:System.Globalization.DateTimeFormatInfo" /> object to wrap. </param>
		/// <returns>A read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> wrapper.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="dtfi" /> is <see langword="null" />. </exception>
		// Token: 0x06002D57 RID: 11607 RVA: 0x0009FA68 File Offset: 0x0009DC68
		public static DateTimeFormatInfo ReadOnly(DateTimeFormatInfo dtfi)
		{
			if (dtfi == null)
			{
				throw new ArgumentNullException("dtfi", Environment.GetResourceString("Object cannot be null."));
			}
			if (dtfi.IsReadOnly)
			{
				return dtfi;
			}
			DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)dtfi.MemberwiseClone();
			dateTimeFormatInfo.calendar = Calendar.ReadOnly(dtfi.Calendar);
			dateTimeFormatInfo.m_isReadOnly = true;
			return dateTimeFormatInfo;
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000704 RID: 1796
		// (get) Token: 0x06002D58 RID: 11608 RVA: 0x0009FABA File Offset: 0x0009DCBA
		public bool IsReadOnly
		{
			get
			{
				return this.m_isReadOnly;
			}
		}

		/// <summary>Gets the native name of the calendar associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>The native name of the calendar used in the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object if that name is available, or the empty string ("") if the native calendar name is not available.</returns>
		// Token: 0x17000705 RID: 1797
		// (get) Token: 0x06002D59 RID: 11609 RVA: 0x0009FAC2 File Offset: 0x0009DCC2
		[ComVisible(false)]
		public string NativeCalendarName
		{
			get
			{
				return this.m_cultureData.CalendarName(this.Calendar.ID);
			}
		}

		/// <summary>Sets the custom date and time format strings that correspond to a specified standard format string.</summary>
		/// <param name="patterns">An array of custom format strings.</param>
		/// <param name="format">The standard format string associated with the custom format strings specified in the <paramref name="patterns" /> parameter. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="patterns" /> is <see langword="null" /> or a zero-length array.-or-
		///         <paramref name="format" /> is not a valid standard format string or is a standard format string whose patterns cannot be set.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="patterns" /> has an array element whose value is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">This <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only.</exception>
		// Token: 0x06002D5A RID: 11610 RVA: 0x0009FADC File Offset: 0x0009DCDC
		[ComVisible(false)]
		public void SetAllDateTimePatterns(string[] patterns, char format)
		{
			if (this.IsReadOnly)
			{
				throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
			}
			if (patterns == null)
			{
				throw new ArgumentNullException("patterns", Environment.GetResourceString("Array cannot be null."));
			}
			if (patterns.Length == 0)
			{
				throw new ArgumentException(Environment.GetResourceString("Array must not be of length zero."), "patterns");
			}
			for (int i = 0; i < patterns.Length; i++)
			{
				if (patterns[i] == null)
				{
					throw new ArgumentNullException(Environment.GetResourceString("Found a null value within an array."));
				}
			}
			if (format <= 'Y')
			{
				if (format == 'D')
				{
					this.allLongDatePatterns = patterns;
					this.longDatePattern = this.allLongDatePatterns[0];
					goto IL_11E;
				}
				if (format == 'T')
				{
					this.allLongTimePatterns = patterns;
					this.longTimePattern = this.allLongTimePatterns[0];
					goto IL_11E;
				}
				if (format != 'Y')
				{
					goto IL_109;
				}
			}
			else
			{
				if (format == 'd')
				{
					this.allShortDatePatterns = patterns;
					this.shortDatePattern = this.allShortDatePatterns[0];
					goto IL_11E;
				}
				if (format == 't')
				{
					this.allShortTimePatterns = patterns;
					this.shortTimePattern = this.allShortTimePatterns[0];
					goto IL_11E;
				}
				if (format != 'y')
				{
					goto IL_109;
				}
			}
			this.allYearMonthPatterns = patterns;
			this.yearMonthPattern = this.allYearMonthPatterns[0];
			goto IL_11E;
			IL_109:
			throw new ArgumentException(Environment.GetResourceString("Format specifier was invalid."), "format");
			IL_11E:
			this.ClearTokenHashTable();
		}

		/// <summary>Gets or sets a string array of abbreviated month names associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>An array of abbreviated month names.</returns>
		/// <exception cref="T:System.ArgumentException">In a set operation, the array is multidimensional or has a length that is not exactly 13.</exception>
		/// <exception cref="T:System.ArgumentNullException">In a set operation, the array or one of the elements of the array is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">In a set operation, the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only.</exception>
		// Token: 0x17000706 RID: 1798
		// (get) Token: 0x06002D5B RID: 11611 RVA: 0x0009FC0D File Offset: 0x0009DE0D
		// (set) Token: 0x06002D5C RID: 11612 RVA: 0x0009FC20 File Offset: 0x0009DE20
		[ComVisible(false)]
		public string[] AbbreviatedMonthGenitiveNames
		{
			get
			{
				return (string[])this.internalGetGenitiveMonthNames(true).Clone();
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Array cannot be null."));
				}
				if (value.Length != 13)
				{
					throw new ArgumentException(Environment.GetResourceString("Length of the array must be {0}.", new object[]
					{
						13
					}), "value");
				}
				DateTimeFormatInfo.CheckNullValue(value, value.Length - 1);
				this.ClearTokenHashTable();
				this.m_genitiveAbbreviatedMonthNames = value;
			}
		}

		/// <summary>Gets or sets a string array of month names associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>A string array of month names.</returns>
		/// <exception cref="T:System.ArgumentException">In a set operation, the array is multidimensional or has a length that is not exactly 13.</exception>
		/// <exception cref="T:System.ArgumentNullException">In a set operation, the array or one of its elements is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">In a set operation, the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only.</exception>
		// Token: 0x17000707 RID: 1799
		// (get) Token: 0x06002D5D RID: 11613 RVA: 0x0009FCA1 File Offset: 0x0009DEA1
		// (set) Token: 0x06002D5E RID: 11614 RVA: 0x0009FCB4 File Offset: 0x0009DEB4
		[ComVisible(false)]
		public string[] MonthGenitiveNames
		{
			get
			{
				return (string[])this.internalGetGenitiveMonthNames(false).Clone();
			}
			set
			{
				if (this.IsReadOnly)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Instance is read-only."));
				}
				if (value == null)
				{
					throw new ArgumentNullException("value", Environment.GetResourceString("Array cannot be null."));
				}
				if (value.Length != 13)
				{
					throw new ArgumentException(Environment.GetResourceString("Length of the array must be {0}.", new object[]
					{
						13
					}), "value");
				}
				DateTimeFormatInfo.CheckNullValue(value, value.Length - 1);
				this.genitiveMonthNames = value;
				this.ClearTokenHashTable();
			}
		}

		// Token: 0x17000708 RID: 1800
		// (get) Token: 0x06002D5F RID: 11615 RVA: 0x0009FD38 File Offset: 0x0009DF38
		internal string FullTimeSpanPositivePattern
		{
			get
			{
				if (this.m_fullTimeSpanPositivePattern == null)
				{
					CultureData cultureData;
					if (this.m_cultureData.UseUserOverride)
					{
						cultureData = CultureData.GetCultureData(this.m_cultureData.CultureName, false);
					}
					else
					{
						cultureData = this.m_cultureData;
					}
					string numberDecimalSeparator = new NumberFormatInfo(cultureData).NumberDecimalSeparator;
					this.m_fullTimeSpanPositivePattern = "d':'h':'mm':'ss'" + numberDecimalSeparator + "'FFFFFFF";
				}
				return this.m_fullTimeSpanPositivePattern;
			}
		}

		// Token: 0x17000709 RID: 1801
		// (get) Token: 0x06002D60 RID: 11616 RVA: 0x0009FD9D File Offset: 0x0009DF9D
		internal string FullTimeSpanNegativePattern
		{
			get
			{
				if (this.m_fullTimeSpanNegativePattern == null)
				{
					this.m_fullTimeSpanNegativePattern = "'-'" + this.FullTimeSpanPositivePattern;
				}
				return this.m_fullTimeSpanNegativePattern;
			}
		}

		// Token: 0x1700070A RID: 1802
		// (get) Token: 0x06002D61 RID: 11617 RVA: 0x0009FDC3 File Offset: 0x0009DFC3
		internal CompareInfo CompareInfo
		{
			get
			{
				if (this.m_compareInfo == null)
				{
					this.m_compareInfo = CompareInfo.GetCompareInfo(this.m_cultureData.SCOMPAREINFO);
				}
				return this.m_compareInfo;
			}
		}

		// Token: 0x06002D62 RID: 11618 RVA: 0x0009FDEC File Offset: 0x0009DFEC
		internal static void ValidateStyles(DateTimeStyles style, string parameterName)
		{
			if ((style & ~(DateTimeStyles.AllowLeadingWhite | DateTimeStyles.AllowTrailingWhite | DateTimeStyles.AllowInnerWhite | DateTimeStyles.NoCurrentDateDefault | DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeLocal | DateTimeStyles.AssumeUniversal | DateTimeStyles.RoundtripKind)) != DateTimeStyles.None)
			{
				throw new ArgumentException(Environment.GetResourceString("An undefined DateTimeStyles value is being used."), parameterName);
			}
			if ((style & DateTimeStyles.AssumeLocal) != DateTimeStyles.None && (style & DateTimeStyles.AssumeUniversal) != DateTimeStyles.None)
			{
				throw new ArgumentException(Environment.GetResourceString("The DateTimeStyles values AssumeLocal and AssumeUniversal cannot be used together."), parameterName);
			}
			if ((style & DateTimeStyles.RoundtripKind) != DateTimeStyles.None && (style & (DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeLocal | DateTimeStyles.AssumeUniversal)) != DateTimeStyles.None)
			{
				throw new ArgumentException(Environment.GetResourceString("The DateTimeStyles value RoundtripKind cannot be used with the values AssumeLocal, AssumeUniversal or AdjustToUniversal."), parameterName);
			}
		}

		// Token: 0x1700070B RID: 1803
		// (get) Token: 0x06002D63 RID: 11619 RVA: 0x0009FE50 File Offset: 0x0009E050
		internal DateTimeFormatFlags FormatFlags
		{
			get
			{
				if (this.formatFlags == DateTimeFormatFlags.NotInitialized)
				{
					this.formatFlags = DateTimeFormatFlags.None;
					this.formatFlags |= (DateTimeFormatFlags)DateTimeFormatInfoScanner.GetFormatFlagGenitiveMonth(this.MonthNames, this.internalGetGenitiveMonthNames(false), this.AbbreviatedMonthNames, this.internalGetGenitiveMonthNames(true));
					this.formatFlags |= (DateTimeFormatFlags)DateTimeFormatInfoScanner.GetFormatFlagUseSpaceInMonthNames(this.MonthNames, this.internalGetGenitiveMonthNames(false), this.AbbreviatedMonthNames, this.internalGetGenitiveMonthNames(true));
					this.formatFlags |= (DateTimeFormatFlags)DateTimeFormatInfoScanner.GetFormatFlagUseSpaceInDayNames(this.DayNames, this.AbbreviatedDayNames);
					this.formatFlags |= (DateTimeFormatFlags)DateTimeFormatInfoScanner.GetFormatFlagUseHebrewCalendar(this.Calendar.ID);
				}
				return this.formatFlags;
			}
		}

		// Token: 0x1700070C RID: 1804
		// (get) Token: 0x06002D64 RID: 11620 RVA: 0x0009FF0C File Offset: 0x0009E10C
		internal bool HasForceTwoDigitYears
		{
			get
			{
				int id = this.calendar.ID;
				return id - 3 <= 1;
			}
		}

		// Token: 0x1700070D RID: 1805
		// (get) Token: 0x06002D65 RID: 11621 RVA: 0x0009FF2E File Offset: 0x0009E12E
		internal bool HasYearMonthAdjustment
		{
			get
			{
				return (this.FormatFlags & DateTimeFormatFlags.UseHebrewRule) > DateTimeFormatFlags.None;
			}
		}

		// Token: 0x06002D66 RID: 11622 RVA: 0x0009FF3C File Offset: 0x0009E13C
		internal bool YearMonthAdjustment(ref int year, ref int month, bool parsedMonthName)
		{
			if ((this.FormatFlags & DateTimeFormatFlags.UseHebrewRule) != DateTimeFormatFlags.None)
			{
				if (year < 1000)
				{
					year += 5000;
				}
				if (year < this.Calendar.GetYear(this.Calendar.MinSupportedDateTime) || year > this.Calendar.GetYear(this.Calendar.MaxSupportedDateTime))
				{
					return false;
				}
				if (parsedMonthName && !this.Calendar.IsLeapYear(year))
				{
					if (month >= 8)
					{
						month--;
					}
					else if (month == 7)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06002D67 RID: 11623 RVA: 0x0009FFC4 File Offset: 0x0009E1C4
		internal static DateTimeFormatInfo GetJapaneseCalendarDTFI()
		{
			DateTimeFormatInfo dateTimeFormat = DateTimeFormatInfo.s_jajpDTFI;
			if (dateTimeFormat == null)
			{
				dateTimeFormat = new CultureInfo("ja-JP", false).DateTimeFormat;
				dateTimeFormat.Calendar = JapaneseCalendar.GetDefaultInstance();
				DateTimeFormatInfo.s_jajpDTFI = dateTimeFormat;
			}
			return dateTimeFormat;
		}

		// Token: 0x06002D68 RID: 11624 RVA: 0x000A0004 File Offset: 0x0009E204
		internal static DateTimeFormatInfo GetTaiwanCalendarDTFI()
		{
			DateTimeFormatInfo dateTimeFormat = DateTimeFormatInfo.s_zhtwDTFI;
			if (dateTimeFormat == null)
			{
				dateTimeFormat = new CultureInfo("zh-TW", false).DateTimeFormat;
				dateTimeFormat.Calendar = TaiwanCalendar.GetDefaultInstance();
				DateTimeFormatInfo.s_zhtwDTFI = dateTimeFormat;
			}
			return dateTimeFormat;
		}

		// Token: 0x06002D69 RID: 11625 RVA: 0x000A0041 File Offset: 0x0009E241
		private void ClearTokenHashTable()
		{
			this.m_dtfiTokenHash = null;
			this.formatFlags = DateTimeFormatFlags.NotInitialized;
		}

		// Token: 0x06002D6A RID: 11626 RVA: 0x000A0054 File Offset: 0x0009E254
		[SecurityCritical]
		internal TokenHashValue[] CreateTokenHashTable()
		{
			TokenHashValue[] array = this.m_dtfiTokenHash;
			if (array == null)
			{
				array = new TokenHashValue[199];
				bool flag = this.LanguageName.Equals("ko");
				string b = this.TimeSeparator.Trim();
				if ("," != b)
				{
					this.InsertHash(array, ",", TokenType.IgnorableSymbol, 0);
				}
				if ("." != b)
				{
					this.InsertHash(array, ".", TokenType.IgnorableSymbol, 0);
				}
				if ("시" != b && "時" != b && "时" != b)
				{
					this.InsertHash(array, this.TimeSeparator, TokenType.SEP_Time, 0);
				}
				this.InsertHash(array, this.AMDesignator, (TokenType)1027, 0);
				this.InsertHash(array, this.PMDesignator, (TokenType)1284, 1);
				if (this.LanguageName.Equals("sq"))
				{
					this.InsertHash(array, "." + this.AMDesignator, (TokenType)1027, 0);
					this.InsertHash(array, "." + this.PMDesignator, (TokenType)1284, 1);
				}
				this.InsertHash(array, "年", TokenType.SEP_YearSuff, 0);
				this.InsertHash(array, "년", TokenType.SEP_YearSuff, 0);
				this.InsertHash(array, "月", TokenType.SEP_MonthSuff, 0);
				this.InsertHash(array, "월", TokenType.SEP_MonthSuff, 0);
				this.InsertHash(array, "日", TokenType.SEP_DaySuff, 0);
				this.InsertHash(array, "일", TokenType.SEP_DaySuff, 0);
				this.InsertHash(array, "時", TokenType.SEP_HourSuff, 0);
				this.InsertHash(array, "时", TokenType.SEP_HourSuff, 0);
				this.InsertHash(array, "分", TokenType.SEP_MinuteSuff, 0);
				this.InsertHash(array, "秒", TokenType.SEP_SecondSuff, 0);
				if (flag)
				{
					this.InsertHash(array, "시", TokenType.SEP_HourSuff, 0);
					this.InsertHash(array, "분", TokenType.SEP_MinuteSuff, 0);
					this.InsertHash(array, "초", TokenType.SEP_SecondSuff, 0);
				}
				if (this.LanguageName.Equals("ky"))
				{
					this.InsertHash(array, "-", TokenType.IgnorableSymbol, 0);
				}
				else
				{
					this.InsertHash(array, "-", TokenType.SEP_DateOrOffset, 0);
				}
				DateTimeFormatInfoScanner dateTimeFormatInfoScanner = new DateTimeFormatInfoScanner();
				string[] array2 = this.m_dateWords = dateTimeFormatInfoScanner.GetDateWordsOfDTFI(this);
				DateTimeFormatFlags dateTimeFormatFlags = this.FormatFlags;
				bool flag2 = false;
				if (array2 != null)
				{
					for (int i = 0; i < array2.Length; i++)
					{
						char c = array2[i][0];
						if (c != '')
						{
							if (c != '')
							{
								this.InsertHash(array, array2[i], TokenType.DateWordToken, 0);
								if (this.LanguageName.Equals("eu"))
								{
									this.InsertHash(array, "." + array2[i], TokenType.DateWordToken, 0);
								}
							}
							else
							{
								string text = array2[i].Substring(1);
								this.InsertHash(array, text, TokenType.IgnorableSymbol, 0);
								if (this.DateSeparator.Trim(null).Equals(text))
								{
									flag2 = true;
								}
							}
						}
						else
						{
							string monthPostfix = array2[i].Substring(1);
							this.AddMonthNames(array, monthPostfix);
						}
					}
				}
				if (!flag2)
				{
					this.InsertHash(array, this.DateSeparator, TokenType.SEP_Date, 0);
				}
				this.AddMonthNames(array, null);
				for (int j = 1; j <= 13; j++)
				{
					this.InsertHash(array, this.GetAbbreviatedMonthName(j), TokenType.MonthToken, j);
				}
				if ((this.FormatFlags & DateTimeFormatFlags.UseGenitiveMonth) != DateTimeFormatFlags.None)
				{
					for (int k = 1; k <= 13; k++)
					{
						string str = this.internalGetMonthName(k, MonthNameStyles.Genitive, false);
						this.InsertHash(array, str, TokenType.MonthToken, k);
					}
				}
				if ((this.FormatFlags & DateTimeFormatFlags.UseLeapYearMonth) != DateTimeFormatFlags.None)
				{
					for (int l = 1; l <= 13; l++)
					{
						string str2 = this.internalGetMonthName(l, MonthNameStyles.LeapYear, false);
						this.InsertHash(array, str2, TokenType.MonthToken, l);
					}
				}
				for (int m = 0; m < 7; m++)
				{
					string str3 = this.GetDayName((DayOfWeek)m);
					this.InsertHash(array, str3, TokenType.DayOfWeekToken, m);
					str3 = this.GetAbbreviatedDayName((DayOfWeek)m);
					this.InsertHash(array, str3, TokenType.DayOfWeekToken, m);
				}
				int[] eras = this.calendar.Eras;
				for (int n = 1; n <= eras.Length; n++)
				{
					this.InsertHash(array, this.GetEraName(n), TokenType.EraToken, n);
					this.InsertHash(array, this.GetAbbreviatedEraName(n), TokenType.EraToken, n);
				}
				if (this.LanguageName.Equals("ja"))
				{
					for (int num = 0; num < 7; num++)
					{
						string str4 = "(" + this.GetAbbreviatedDayName((DayOfWeek)num) + ")";
						this.InsertHash(array, str4, TokenType.DayOfWeekToken, num);
					}
					if (this.Calendar.GetType() != typeof(JapaneseCalendar))
					{
						DateTimeFormatInfo japaneseCalendarDTFI = DateTimeFormatInfo.GetJapaneseCalendarDTFI();
						for (int num2 = 1; num2 <= japaneseCalendarDTFI.Calendar.Eras.Length; num2++)
						{
							this.InsertHash(array, japaneseCalendarDTFI.GetEraName(num2), TokenType.JapaneseEraToken, num2);
							this.InsertHash(array, japaneseCalendarDTFI.GetAbbreviatedEraName(num2), TokenType.JapaneseEraToken, num2);
							this.InsertHash(array, japaneseCalendarDTFI.AbbreviatedEnglishEraNames[num2 - 1], TokenType.JapaneseEraToken, num2);
						}
					}
				}
				else if (this.CultureName.Equals("zh-TW"))
				{
					DateTimeFormatInfo taiwanCalendarDTFI = DateTimeFormatInfo.GetTaiwanCalendarDTFI();
					for (int num3 = 1; num3 <= taiwanCalendarDTFI.Calendar.Eras.Length; num3++)
					{
						if (taiwanCalendarDTFI.GetEraName(num3).Length > 0)
						{
							this.InsertHash(array, taiwanCalendarDTFI.GetEraName(num3), TokenType.TEraToken, num3);
						}
					}
				}
				this.InsertHash(array, DateTimeFormatInfo.InvariantInfo.AMDesignator, (TokenType)1027, 0);
				this.InsertHash(array, DateTimeFormatInfo.InvariantInfo.PMDesignator, (TokenType)1284, 1);
				for (int num4 = 1; num4 <= 12; num4++)
				{
					string str5 = DateTimeFormatInfo.InvariantInfo.GetMonthName(num4);
					this.InsertHash(array, str5, TokenType.MonthToken, num4);
					str5 = DateTimeFormatInfo.InvariantInfo.GetAbbreviatedMonthName(num4);
					this.InsertHash(array, str5, TokenType.MonthToken, num4);
				}
				for (int num5 = 0; num5 < 7; num5++)
				{
					string str6 = DateTimeFormatInfo.InvariantInfo.GetDayName((DayOfWeek)num5);
					this.InsertHash(array, str6, TokenType.DayOfWeekToken, num5);
					str6 = DateTimeFormatInfo.InvariantInfo.GetAbbreviatedDayName((DayOfWeek)num5);
					this.InsertHash(array, str6, TokenType.DayOfWeekToken, num5);
				}
				for (int num6 = 0; num6 < this.AbbreviatedEnglishEraNames.Length; num6++)
				{
					this.InsertHash(array, this.AbbreviatedEnglishEraNames[num6], TokenType.EraToken, num6 + 1);
				}
				this.InsertHash(array, "T", TokenType.SEP_LocalTimeMark, 0);
				this.InsertHash(array, "GMT", TokenType.TimeZoneToken, 0);
				this.InsertHash(array, "Z", TokenType.TimeZoneToken, 0);
				this.InsertHash(array, "/", TokenType.SEP_Date, 0);
				this.InsertHash(array, ":", TokenType.SEP_Time, 0);
				this.m_dtfiTokenHash = array;
			}
			return array;
		}

		// Token: 0x06002D6B RID: 11627 RVA: 0x000A0710 File Offset: 0x0009E910
		private void AddMonthNames(TokenHashValue[] temp, string monthPostfix)
		{
			for (int i = 1; i <= 13; i++)
			{
				string text = this.GetMonthName(i);
				if (text.Length > 0)
				{
					if (monthPostfix != null)
					{
						this.InsertHash(temp, text + monthPostfix, TokenType.MonthToken, i);
					}
					else
					{
						this.InsertHash(temp, text, TokenType.MonthToken, i);
					}
				}
				text = this.GetAbbreviatedMonthName(i);
				this.InsertHash(temp, text, TokenType.MonthToken, i);
			}
		}

		// Token: 0x06002D6C RID: 11628 RVA: 0x000A076C File Offset: 0x0009E96C
		private static bool TryParseHebrewNumber(ref __DTString str, out bool badFormat, out int number)
		{
			number = -1;
			badFormat = false;
			int index = str.Index;
			if (!HebrewNumber.IsDigit(str.Value[index]))
			{
				return false;
			}
			HebrewNumberParsingContext hebrewNumberParsingContext = new HebrewNumberParsingContext(0);
			HebrewNumberParsingState hebrewNumberParsingState;
			for (;;)
			{
				hebrewNumberParsingState = HebrewNumber.ParseByChar(str.Value[index++], ref hebrewNumberParsingContext);
				if (hebrewNumberParsingState <= HebrewNumberParsingState.NotHebrewDigit)
				{
					break;
				}
				if (index >= str.Value.Length || hebrewNumberParsingState == HebrewNumberParsingState.FoundEndOfHebrewNumber)
				{
					goto IL_5A;
				}
			}
			return false;
			IL_5A:
			if (hebrewNumberParsingState != HebrewNumberParsingState.FoundEndOfHebrewNumber)
			{
				return false;
			}
			str.Advance(index - str.Index);
			number = hebrewNumberParsingContext.result;
			return true;
		}

		// Token: 0x06002D6D RID: 11629 RVA: 0x000A07F1 File Offset: 0x0009E9F1
		private static bool IsHebrewChar(char ch)
		{
			return ch >= '֐' && ch <= '׿';
		}

		// Token: 0x06002D6E RID: 11630 RVA: 0x000A0808 File Offset: 0x0009EA08
		[SecurityCritical]
		internal bool Tokenize(TokenType TokenMask, out TokenType tokenType, out int tokenValue, ref __DTString str)
		{
			tokenType = TokenType.UnknownToken;
			tokenValue = 0;
			char c = str.m_current;
			bool flag = char.IsLetter(c);
			if (flag)
			{
				c = char.ToLower(c, this.Culture);
				bool flag2;
				if (DateTimeFormatInfo.IsHebrewChar(c) && TokenMask == TokenType.RegularTokenMask && DateTimeFormatInfo.TryParseHebrewNumber(ref str, out flag2, out tokenValue))
				{
					if (flag2)
					{
						tokenType = TokenType.UnknownToken;
						return false;
					}
					tokenType = TokenType.HebrewNumber;
					return true;
				}
			}
			int num = (int)(c % 'Ç');
			int num2 = (int)('\u0001' + c % 'Å');
			int num3 = str.len - str.Index;
			int num4 = 0;
			TokenHashValue[] array = this.m_dtfiTokenHash;
			if (array == null)
			{
				array = this.CreateTokenHashTable();
			}
			TokenHashValue tokenHashValue;
			int count;
			int count2;
			for (;;)
			{
				tokenHashValue = array[num];
				if (tokenHashValue == null)
				{
					return false;
				}
				if ((tokenHashValue.tokenType & TokenMask) > (TokenType)0 && tokenHashValue.tokenString.Length <= num3)
				{
					if (string.Compare(str.Value, str.Index, tokenHashValue.tokenString, 0, tokenHashValue.tokenString.Length, this.Culture, CompareOptions.IgnoreCase) == 0)
					{
						break;
					}
					if (tokenHashValue.tokenType == TokenType.MonthToken && this.HasSpacesInMonthNames)
					{
						count = 0;
						if (str.MatchSpecifiedWords(tokenHashValue.tokenString, true, ref count))
						{
							goto Block_16;
						}
					}
					else if (tokenHashValue.tokenType == TokenType.DayOfWeekToken && this.HasSpacesInDayNames)
					{
						count2 = 0;
						if (str.MatchSpecifiedWords(tokenHashValue.tokenString, true, ref count2))
						{
							goto Block_19;
						}
					}
				}
				num4++;
				num += num2;
				if (num >= 199)
				{
					num -= 199;
				}
				if (num4 >= 199)
				{
					return false;
				}
			}
			int index;
			if (flag && (index = str.Index + tokenHashValue.tokenString.Length) < str.len && char.IsLetter(str.Value[index]))
			{
				return false;
			}
			tokenType = (tokenHashValue.tokenType & TokenMask);
			tokenValue = tokenHashValue.tokenValue;
			str.Advance(tokenHashValue.tokenString.Length);
			return true;
			Block_16:
			tokenType = (tokenHashValue.tokenType & TokenMask);
			tokenValue = tokenHashValue.tokenValue;
			str.Advance(count);
			return true;
			Block_19:
			tokenType = (tokenHashValue.tokenType & TokenMask);
			tokenValue = tokenHashValue.tokenValue;
			str.Advance(count2);
			return true;
		}

		// Token: 0x06002D6F RID: 11631 RVA: 0x000A0A0C File Offset: 0x0009EC0C
		private void InsertAtCurrentHashNode(TokenHashValue[] hashTable, string str, char ch, TokenType tokenType, int tokenValue, int pos, int hashcode, int hashProbe)
		{
			TokenHashValue tokenHashValue = hashTable[hashcode];
			hashTable[hashcode] = new TokenHashValue(str, tokenType, tokenValue);
			while (++pos < 199)
			{
				hashcode += hashProbe;
				if (hashcode >= 199)
				{
					hashcode -= 199;
				}
				TokenHashValue tokenHashValue2 = hashTable[hashcode];
				if (tokenHashValue2 == null || char.ToLower(tokenHashValue2.tokenString[0], this.Culture) == ch)
				{
					hashTable[hashcode] = tokenHashValue;
					if (tokenHashValue2 == null)
					{
						return;
					}
					tokenHashValue = tokenHashValue2;
				}
			}
		}

		// Token: 0x06002D70 RID: 11632 RVA: 0x000A0A84 File Offset: 0x0009EC84
		private void InsertHash(TokenHashValue[] hashTable, string str, TokenType tokenType, int tokenValue)
		{
			if (str == null || str.Length == 0)
			{
				return;
			}
			int num = 0;
			if (char.IsWhiteSpace(str[0]) || char.IsWhiteSpace(str[str.Length - 1]))
			{
				str = str.Trim(null);
				if (str.Length == 0)
				{
					return;
				}
			}
			char c = char.ToLower(str[0], this.Culture);
			int num2 = (int)(c % 'Ç');
			int num3 = (int)('\u0001' + c % 'Å');
			for (;;)
			{
				TokenHashValue tokenHashValue = hashTable[num2];
				if (tokenHashValue == null)
				{
					break;
				}
				if (str.Length >= tokenHashValue.tokenString.Length && string.Compare(str, 0, tokenHashValue.tokenString, 0, tokenHashValue.tokenString.Length, this.Culture, CompareOptions.IgnoreCase) == 0)
				{
					if (str.Length > tokenHashValue.tokenString.Length)
					{
						goto Block_7;
					}
					int tokenType2 = (int)tokenHashValue.tokenType;
					if (DateTimeFormatInfo.preferExistingTokens || BinaryCompatibility.TargetsAtLeast_Desktop_V4_5_1)
					{
						if (((tokenType2 & 255) == 0 && (tokenType & TokenType.RegularTokenMask) != (TokenType)0) || ((tokenType2 & 65280) == 0 && (tokenType & TokenType.SeparatorTokenMask) != (TokenType)0))
						{
							tokenHashValue.tokenType |= tokenType;
							if (tokenValue != 0)
							{
								tokenHashValue.tokenValue = tokenValue;
							}
						}
					}
					else if (((tokenType | (TokenType)tokenType2) & TokenType.RegularTokenMask) == tokenType || ((tokenType | (TokenType)tokenType2) & TokenType.SeparatorTokenMask) == tokenType)
					{
						tokenHashValue.tokenType |= tokenType;
						if (tokenValue != 0)
						{
							tokenHashValue.tokenValue = tokenValue;
						}
					}
				}
				num++;
				num2 += num3;
				if (num2 >= 199)
				{
					num2 -= 199;
				}
				if (num >= 199)
				{
					return;
				}
			}
			hashTable[num2] = new TokenHashValue(str, tokenType, tokenValue);
			return;
			Block_7:
			this.InsertAtCurrentHashNode(hashTable, str, c, tokenType, tokenValue, num, num2, num3);
		}

		// Token: 0x06002D71 RID: 11633 RVA: 0x000A0C26 File Offset: 0x0009EE26
		// Note: this type is marked as 'beforefieldinit'.
		static DateTimeFormatInfo()
		{
		}

		// Token: 0x040017A6 RID: 6054
		private static volatile DateTimeFormatInfo invariantInfo;

		// Token: 0x040017A7 RID: 6055
		[NonSerialized]
		private CultureData m_cultureData;

		// Token: 0x040017A8 RID: 6056
		[OptionalField(VersionAdded = 2)]
		internal string m_name;

		// Token: 0x040017A9 RID: 6057
		[NonSerialized]
		private string m_langName;

		// Token: 0x040017AA RID: 6058
		[NonSerialized]
		private CompareInfo m_compareInfo;

		// Token: 0x040017AB RID: 6059
		[NonSerialized]
		private CultureInfo m_cultureInfo;

		// Token: 0x040017AC RID: 6060
		internal string amDesignator;

		// Token: 0x040017AD RID: 6061
		internal string pmDesignator;

		// Token: 0x040017AE RID: 6062
		[OptionalField(VersionAdded = 1)]
		internal string dateSeparator;

		// Token: 0x040017AF RID: 6063
		[OptionalField(VersionAdded = 1)]
		internal string generalShortTimePattern;

		// Token: 0x040017B0 RID: 6064
		[OptionalField(VersionAdded = 1)]
		internal string generalLongTimePattern;

		// Token: 0x040017B1 RID: 6065
		[OptionalField(VersionAdded = 1)]
		internal string timeSeparator;

		// Token: 0x040017B2 RID: 6066
		internal string monthDayPattern;

		// Token: 0x040017B3 RID: 6067
		[OptionalField(VersionAdded = 2)]
		internal string dateTimeOffsetPattern;

		// Token: 0x040017B4 RID: 6068
		internal const string rfc1123Pattern = "ddd, dd MMM yyyy HH':'mm':'ss 'GMT'";

		// Token: 0x040017B5 RID: 6069
		internal const string sortableDateTimePattern = "yyyy'-'MM'-'dd'T'HH':'mm':'ss";

		// Token: 0x040017B6 RID: 6070
		internal const string universalSortableDateTimePattern = "yyyy'-'MM'-'dd HH':'mm':'ss'Z'";

		// Token: 0x040017B7 RID: 6071
		internal Calendar calendar;

		// Token: 0x040017B8 RID: 6072
		internal int firstDayOfWeek = -1;

		// Token: 0x040017B9 RID: 6073
		internal int calendarWeekRule = -1;

		// Token: 0x040017BA RID: 6074
		[OptionalField(VersionAdded = 1)]
		[NonSerialized]
		internal string fullDateTimePattern;

		// Token: 0x040017BB RID: 6075
		internal string[] abbreviatedDayNames;

		// Token: 0x040017BC RID: 6076
		[OptionalField(VersionAdded = 2)]
		internal string[] m_superShortDayNames;

		// Token: 0x040017BD RID: 6077
		internal string[] dayNames;

		// Token: 0x040017BE RID: 6078
		internal string[] abbreviatedMonthNames;

		// Token: 0x040017BF RID: 6079
		internal string[] monthNames;

		// Token: 0x040017C0 RID: 6080
		[OptionalField(VersionAdded = 2)]
		internal string[] genitiveMonthNames;

		// Token: 0x040017C1 RID: 6081
		[OptionalField(VersionAdded = 2)]
		internal string[] m_genitiveAbbreviatedMonthNames;

		// Token: 0x040017C2 RID: 6082
		[OptionalField(VersionAdded = 2)]
		internal string[] leapYearMonthNames;

		// Token: 0x040017C3 RID: 6083
		internal string longDatePattern;

		// Token: 0x040017C4 RID: 6084
		internal string shortDatePattern;

		// Token: 0x040017C5 RID: 6085
		internal string yearMonthPattern;

		// Token: 0x040017C6 RID: 6086
		internal string longTimePattern;

		// Token: 0x040017C7 RID: 6087
		internal string shortTimePattern;

		// Token: 0x040017C8 RID: 6088
		[OptionalField(VersionAdded = 3)]
		private string[] allYearMonthPatterns;

		// Token: 0x040017C9 RID: 6089
		internal string[] allShortDatePatterns;

		// Token: 0x040017CA RID: 6090
		internal string[] allLongDatePatterns;

		// Token: 0x040017CB RID: 6091
		internal string[] allShortTimePatterns;

		// Token: 0x040017CC RID: 6092
		internal string[] allLongTimePatterns;

		// Token: 0x040017CD RID: 6093
		internal string[] m_eraNames;

		// Token: 0x040017CE RID: 6094
		internal string[] m_abbrevEraNames;

		// Token: 0x040017CF RID: 6095
		internal string[] m_abbrevEnglishEraNames;

		// Token: 0x040017D0 RID: 6096
		internal int[] optionalCalendars;

		// Token: 0x040017D1 RID: 6097
		private const int DEFAULT_ALL_DATETIMES_SIZE = 132;

		// Token: 0x040017D2 RID: 6098
		internal bool m_isReadOnly;

		// Token: 0x040017D3 RID: 6099
		[OptionalField(VersionAdded = 2)]
		internal DateTimeFormatFlags formatFlags = DateTimeFormatFlags.NotInitialized;

		// Token: 0x040017D4 RID: 6100
		internal static bool preferExistingTokens = DateTimeFormatInfo.InitPreferExistingTokens();

		// Token: 0x040017D5 RID: 6101
		[OptionalField(VersionAdded = 1)]
		private int CultureID;

		// Token: 0x040017D6 RID: 6102
		[OptionalField(VersionAdded = 1)]
		private bool m_useUserOverride;

		// Token: 0x040017D7 RID: 6103
		[OptionalField(VersionAdded = 1)]
		private bool bUseCalendarInfo;

		// Token: 0x040017D8 RID: 6104
		[OptionalField(VersionAdded = 1)]
		private int nDataItem;

		// Token: 0x040017D9 RID: 6105
		[OptionalField(VersionAdded = 2)]
		internal bool m_isDefaultCalendar;

		// Token: 0x040017DA RID: 6106
		[OptionalField(VersionAdded = 2)]
		private static volatile Hashtable s_calendarNativeNames;

		// Token: 0x040017DB RID: 6107
		[OptionalField(VersionAdded = 1)]
		internal string[] m_dateWords;

		// Token: 0x040017DC RID: 6108
		[NonSerialized]
		private string m_fullTimeSpanPositivePattern;

		// Token: 0x040017DD RID: 6109
		[NonSerialized]
		private string m_fullTimeSpanNegativePattern;

		// Token: 0x040017DE RID: 6110
		internal const DateTimeStyles InvalidDateTimeStyles = ~(DateTimeStyles.AllowLeadingWhite | DateTimeStyles.AllowTrailingWhite | DateTimeStyles.AllowInnerWhite | DateTimeStyles.NoCurrentDateDefault | DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeLocal | DateTimeStyles.AssumeUniversal | DateTimeStyles.RoundtripKind);

		// Token: 0x040017DF RID: 6111
		[NonSerialized]
		private TokenHashValue[] m_dtfiTokenHash;

		// Token: 0x040017E0 RID: 6112
		private const int TOKEN_HASH_SIZE = 199;

		// Token: 0x040017E1 RID: 6113
		private const int SECOND_PRIME = 197;

		// Token: 0x040017E2 RID: 6114
		private const string dateSeparatorOrTimeZoneOffset = "-";

		// Token: 0x040017E3 RID: 6115
		private const string invariantDateSeparator = "/";

		// Token: 0x040017E4 RID: 6116
		private const string invariantTimeSeparator = ":";

		// Token: 0x040017E5 RID: 6117
		internal const string IgnorablePeriod = ".";

		// Token: 0x040017E6 RID: 6118
		internal const string IgnorableComma = ",";

		// Token: 0x040017E7 RID: 6119
		internal const string CJKYearSuff = "年";

		// Token: 0x040017E8 RID: 6120
		internal const string CJKMonthSuff = "月";

		// Token: 0x040017E9 RID: 6121
		internal const string CJKDaySuff = "日";

		// Token: 0x040017EA RID: 6122
		internal const string KoreanYearSuff = "년";

		// Token: 0x040017EB RID: 6123
		internal const string KoreanMonthSuff = "월";

		// Token: 0x040017EC RID: 6124
		internal const string KoreanDaySuff = "일";

		// Token: 0x040017ED RID: 6125
		internal const string KoreanHourSuff = "시";

		// Token: 0x040017EE RID: 6126
		internal const string KoreanMinuteSuff = "분";

		// Token: 0x040017EF RID: 6127
		internal const string KoreanSecondSuff = "초";

		// Token: 0x040017F0 RID: 6128
		internal const string CJKHourSuff = "時";

		// Token: 0x040017F1 RID: 6129
		internal const string ChineseHourSuff = "时";

		// Token: 0x040017F2 RID: 6130
		internal const string CJKMinuteSuff = "分";

		// Token: 0x040017F3 RID: 6131
		internal const string CJKSecondSuff = "秒";

		// Token: 0x040017F4 RID: 6132
		internal const string LocalTimeMark = "T";

		// Token: 0x040017F5 RID: 6133
		internal const string KoreanLangName = "ko";

		// Token: 0x040017F6 RID: 6134
		internal const string JapaneseLangName = "ja";

		// Token: 0x040017F7 RID: 6135
		internal const string EnglishLangName = "en";

		// Token: 0x040017F8 RID: 6136
		private static volatile DateTimeFormatInfo s_jajpDTFI;

		// Token: 0x040017F9 RID: 6137
		private static volatile DateTimeFormatInfo s_zhtwDTFI;
	}
}
