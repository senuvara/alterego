﻿using System;
using System.Runtime.Serialization;

namespace System.Globalization
{
	// Token: 0x020003D8 RID: 984
	[Serializable]
	internal class EraInfo
	{
		// Token: 0x06002DDA RID: 11738 RVA: 0x000A29A8 File Offset: 0x000A0BA8
		internal EraInfo(int era, int startYear, int startMonth, int startDay, int yearOffset, int minEraYear, int maxEraYear)
		{
			this.era = era;
			this.yearOffset = yearOffset;
			this.minEraYear = minEraYear;
			this.maxEraYear = maxEraYear;
			this.ticks = new DateTime(startYear, startMonth, startDay).Ticks;
		}

		// Token: 0x06002DDB RID: 11739 RVA: 0x000A29F4 File Offset: 0x000A0BF4
		internal EraInfo(int era, int startYear, int startMonth, int startDay, int yearOffset, int minEraYear, int maxEraYear, string eraName, string abbrevEraName, string englishEraName)
		{
			this.era = era;
			this.yearOffset = yearOffset;
			this.minEraYear = minEraYear;
			this.maxEraYear = maxEraYear;
			this.ticks = new DateTime(startYear, startMonth, startDay).Ticks;
			this.eraName = eraName;
			this.abbrevEraName = abbrevEraName;
			this.englishEraName = englishEraName;
		}

		// Token: 0x04001860 RID: 6240
		internal int era;

		// Token: 0x04001861 RID: 6241
		internal long ticks;

		// Token: 0x04001862 RID: 6242
		internal int yearOffset;

		// Token: 0x04001863 RID: 6243
		internal int minEraYear;

		// Token: 0x04001864 RID: 6244
		internal int maxEraYear;

		// Token: 0x04001865 RID: 6245
		[OptionalField(VersionAdded = 4)]
		internal string eraName;

		// Token: 0x04001866 RID: 6246
		[OptionalField(VersionAdded = 4)]
		internal string abbrevEraName;

		// Token: 0x04001867 RID: 6247
		[OptionalField(VersionAdded = 4)]
		internal string englishEraName;
	}
}
