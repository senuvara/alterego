﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	/// <summary>Defines the period of daylight saving time.</summary>
	// Token: 0x020003D3 RID: 979
	[ComVisible(true)]
	[Serializable]
	public class DaylightTime
	{
		// Token: 0x06002D83 RID: 11651 RVA: 0x00002050 File Offset: 0x00000250
		private DaylightTime()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Globalization.DaylightTime" /> class with the specified start, end, and time difference information.</summary>
		/// <param name="start">The object that represents the date and time when daylight saving time begins. The value must be in local time. </param>
		/// <param name="end">The object that represents the date and time when daylight saving time ends. The value must be in local time. </param>
		/// <param name="delta">The object that represents the difference between standard time and daylight saving time, in ticks. </param>
		// Token: 0x06002D84 RID: 11652 RVA: 0x000A1444 File Offset: 0x0009F644
		public DaylightTime(DateTime start, DateTime end, TimeSpan delta)
		{
			this.m_start = start;
			this.m_end = end;
			this.m_delta = delta;
		}

		/// <summary>Gets the object that represents the date and time when the daylight saving period begins.</summary>
		/// <returns>The object that represents the date and time when the daylight saving period begins. The value is in local time.</returns>
		// Token: 0x1700070F RID: 1807
		// (get) Token: 0x06002D85 RID: 11653 RVA: 0x000A1461 File Offset: 0x0009F661
		public DateTime Start
		{
			get
			{
				return this.m_start;
			}
		}

		/// <summary>Gets the object that represents the date and time when the daylight saving period ends.</summary>
		/// <returns>The object that represents the date and time when the daylight saving period ends. The value is in local time.</returns>
		// Token: 0x17000710 RID: 1808
		// (get) Token: 0x06002D86 RID: 11654 RVA: 0x000A1469 File Offset: 0x0009F669
		public DateTime End
		{
			get
			{
				return this.m_end;
			}
		}

		/// <summary>Gets the time interval that represents the difference between standard time and daylight saving time.</summary>
		/// <returns>The time interval that represents the difference between standard time and daylight saving time.</returns>
		// Token: 0x17000711 RID: 1809
		// (get) Token: 0x06002D87 RID: 11655 RVA: 0x000A1471 File Offset: 0x0009F671
		public TimeSpan Delta
		{
			get
			{
				return this.m_delta;
			}
		}

		// Token: 0x04001841 RID: 6209
		internal DateTime m_start;

		// Token: 0x04001842 RID: 6210
		internal DateTime m_end;

		// Token: 0x04001843 RID: 6211
		internal TimeSpan m_delta;
	}
}
