﻿using System;
using System.Security;

namespace System.Globalization
{
	// Token: 0x02000408 RID: 1032
	internal struct InternalCodePageDataItem
	{
		// Token: 0x04001A1C RID: 6684
		internal ushort codePage;

		// Token: 0x04001A1D RID: 6685
		internal ushort uiFamilyCodePage;

		// Token: 0x04001A1E RID: 6686
		internal uint flags;

		// Token: 0x04001A1F RID: 6687
		[SecurityCritical]
		internal string Names;
	}
}
