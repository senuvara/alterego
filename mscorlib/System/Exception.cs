﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>Represents errors that occur during application execution.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x0200014C RID: 332
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_Exception))]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class Exception : ISerializable, _Exception
	{
		// Token: 0x06000EC1 RID: 3777 RVA: 0x0003DB37 File Offset: 0x0003BD37
		private void Init()
		{
			this._message = null;
			this._stackTrace = null;
			this._dynamicMethods = null;
			this.HResult = -2146233088;
			this._safeSerializationManager = new SafeSerializationManager();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class.</summary>
		// Token: 0x06000EC2 RID: 3778 RVA: 0x0003DB64 File Offset: 0x0003BD64
		public Exception()
		{
			this.Init();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06000EC3 RID: 3779 RVA: 0x0003DB72 File Offset: 0x0003BD72
		public Exception(string message)
		{
			this.Init();
			this._message = message;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception, or a null reference (<see langword="Nothing" /> in Visual Basic) if no inner exception is specified. </param>
		// Token: 0x06000EC4 RID: 3780 RVA: 0x0003DB87 File Offset: 0x0003BD87
		public Exception(string message, Exception innerException)
		{
			this.Init();
			this._message = message;
			this._innerException = innerException;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is <see langword="null" /> or <see cref="P:System.Exception.HResult" /> is zero (0). </exception>
		// Token: 0x06000EC5 RID: 3781 RVA: 0x0003DBA4 File Offset: 0x0003BDA4
		[SecuritySafeCritical]
		protected Exception(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this._className = info.GetString("ClassName");
			this._message = info.GetString("Message");
			this._data = (IDictionary)info.GetValueNoThrow("Data", typeof(IDictionary));
			this._innerException = (Exception)info.GetValue("InnerException", typeof(Exception));
			this._helpURL = info.GetString("HelpURL");
			this._stackTraceString = info.GetString("StackTraceString");
			this._remoteStackTraceString = info.GetString("RemoteStackTraceString");
			this._remoteStackIndex = info.GetInt32("RemoteStackIndex");
			this.HResult = info.GetInt32("HResult");
			this._source = info.GetString("Source");
			this._safeSerializationManager = (info.GetValueNoThrow("SafeSerializationManager", typeof(SafeSerializationManager)) as SafeSerializationManager);
			if (this._className == null || this.HResult == 0)
			{
				throw new SerializationException(Environment.GetResourceString("Insufficient state to return the real object."));
			}
			if (context.State == StreamingContextStates.CrossAppDomain)
			{
				this._remoteStackTraceString += this._stackTraceString;
				this._stackTraceString = null;
			}
		}

		/// <summary>Gets a message that describes the current exception.</summary>
		/// <returns>The error message that explains the reason for the exception, or an empty string ("").</returns>
		// Token: 0x170001DE RID: 478
		// (get) Token: 0x06000EC6 RID: 3782 RVA: 0x0003DCF9 File Offset: 0x0003BEF9
		public virtual string Message
		{
			get
			{
				if (this._message == null)
				{
					if (this._className == null)
					{
						this._className = this.GetClassName();
					}
					return Environment.GetResourceString("Exception of type '{0}' was thrown.", new object[]
					{
						this._className
					});
				}
				return this._message;
			}
		}

		/// <summary>Gets a collection of key/value pairs that provide additional user-defined information about the exception.</summary>
		/// <returns>An object that implements the <see cref="T:System.Collections.IDictionary" /> interface and contains a collection of user-defined key/value pairs. The default is an empty collection.</returns>
		// Token: 0x170001DF RID: 479
		// (get) Token: 0x06000EC7 RID: 3783 RVA: 0x0003DD37 File Offset: 0x0003BF37
		public virtual IDictionary Data
		{
			[SecuritySafeCritical]
			get
			{
				if (this._data == null)
				{
					if (Exception.IsImmutableAgileException(this))
					{
						this._data = new EmptyReadOnlyDictionaryInternal();
					}
					else
					{
						this._data = new ListDictionaryInternal();
					}
				}
				return this._data;
			}
		}

		// Token: 0x06000EC8 RID: 3784 RVA: 0x00002526 File Offset: 0x00000726
		private static bool IsImmutableAgileException(Exception e)
		{
			return false;
		}

		// Token: 0x06000EC9 RID: 3785 RVA: 0x0003DD67 File Offset: 0x0003BF67
		private string GetClassName()
		{
			if (this._className == null)
			{
				this._className = this.GetType().ToString();
			}
			return this._className;
		}

		/// <summary>When overridden in a derived class, returns the <see cref="T:System.Exception" /> that is the root cause of one or more subsequent exceptions.</summary>
		/// <returns>The first exception thrown in a chain of exceptions. If the <see cref="P:System.Exception.InnerException" /> property of the current exception is a null reference (<see langword="Nothing" /> in Visual Basic), this property returns the current exception.</returns>
		// Token: 0x06000ECA RID: 3786 RVA: 0x0003DD88 File Offset: 0x0003BF88
		public virtual Exception GetBaseException()
		{
			Exception innerException = this.InnerException;
			Exception result = this;
			while (innerException != null)
			{
				result = innerException;
				innerException = innerException.InnerException;
			}
			return result;
		}

		/// <summary>Gets the <see cref="T:System.Exception" /> instance that caused the current exception.</summary>
		/// <returns>An object that describes the error that caused the current exception. The <see cref="P:System.Exception.InnerException" /> property returns the same value as was passed into the <see cref="M:System.Exception.#ctor(System.String,System.Exception)" /> constructor, or <see langword="null" /> if the inner exception value was not supplied to the constructor. This property is read-only.</returns>
		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x06000ECB RID: 3787 RVA: 0x0003DDAD File Offset: 0x0003BFAD
		public Exception InnerException
		{
			get
			{
				return this._innerException;
			}
		}

		// Token: 0x06000ECC RID: 3788
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IRuntimeMethodInfo GetMethodFromStackTrace(object stackTrace);

		/// <summary>Gets the method that throws the current exception.</summary>
		/// <returns>The <see cref="T:System.Reflection.MethodBase" /> that threw the current exception.</returns>
		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06000ECD RID: 3789 RVA: 0x0003DDB8 File Offset: 0x0003BFB8
		public MethodBase TargetSite
		{
			[SecuritySafeCritical]
			get
			{
				StackTrace stackTrace = new StackTrace(this, true);
				if (stackTrace.FrameCount > 0)
				{
					return stackTrace.GetFrame(0).GetMethod();
				}
				return null;
			}
		}

		/// <summary>Gets a string representation of the immediate frames on the call stack.</summary>
		/// <returns>A string that describes the immediate frames of the call stack.</returns>
		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x06000ECE RID: 3790 RVA: 0x0003DDE4 File Offset: 0x0003BFE4
		public virtual string StackTrace
		{
			get
			{
				return this.GetStackTrace(true);
			}
		}

		// Token: 0x06000ECF RID: 3791 RVA: 0x0003DDF0 File Offset: 0x0003BFF0
		private string GetStackTrace(bool needFileInfo)
		{
			string text = this._stackTraceString;
			string text2 = this._remoteStackTraceString;
			if (!needFileInfo)
			{
				text = this.StripFileInfo(text, false);
				text2 = this.StripFileInfo(text2, true);
			}
			if (text != null)
			{
				return text2 + text;
			}
			if (this._stackTrace == null)
			{
				return text2;
			}
			string stackTrace = Environment.GetStackTrace(this, needFileInfo);
			return text2 + stackTrace;
		}

		// Token: 0x06000ED0 RID: 3792 RVA: 0x0003DE44 File Offset: 0x0003C044
		[FriendAccessAllowed]
		internal void SetErrorCode(int hr)
		{
			this.HResult = hr;
		}

		/// <summary>Gets or sets a link to the help file associated with this exception.</summary>
		/// <returns>The Uniform Resource Name (URN) or Uniform Resource Locator (URL).</returns>
		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x06000ED1 RID: 3793 RVA: 0x0003DE4D File Offset: 0x0003C04D
		// (set) Token: 0x06000ED2 RID: 3794 RVA: 0x0003DE55 File Offset: 0x0003C055
		public virtual string HelpLink
		{
			get
			{
				return this._helpURL;
			}
			set
			{
				this._helpURL = value;
			}
		}

		/// <summary>Gets or sets the name of the application or the object that causes the error.</summary>
		/// <returns>The name of the application or the object that causes the error.</returns>
		/// <exception cref="T:System.ArgumentException">The object must be a runtime <see cref="N:System.Reflection" /> object</exception>
		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x06000ED3 RID: 3795 RVA: 0x0003DE60 File Offset: 0x0003C060
		// (set) Token: 0x06000ED4 RID: 3796 RVA: 0x0003DEBD File Offset: 0x0003C0BD
		public virtual string Source
		{
			get
			{
				if (this._source == null)
				{
					StackTrace stackTrace = new StackTrace(this, true);
					if (stackTrace.FrameCount > 0)
					{
						MethodBase method = stackTrace.GetFrame(0).GetMethod();
						if (method != null)
						{
							this._source = method.DeclaringType.Assembly.GetName().Name;
						}
					}
				}
				return this._source;
			}
			set
			{
				this._source = value;
			}
		}

		/// <summary>Creates and returns a string representation of the current exception.</summary>
		/// <returns>A string representation of the current exception.</returns>
		// Token: 0x06000ED5 RID: 3797 RVA: 0x0003DEC6 File Offset: 0x0003C0C6
		public override string ToString()
		{
			return this.ToString(true, true);
		}

		// Token: 0x06000ED6 RID: 3798 RVA: 0x0003DED0 File Offset: 0x0003C0D0
		private string ToString(bool needFileLineInfo, bool needMessage)
		{
			string text = needMessage ? this.Message : null;
			string text2;
			if (text == null || text.Length <= 0)
			{
				text2 = this.GetClassName();
			}
			else
			{
				text2 = this.GetClassName() + ": " + text;
			}
			if (this._innerException != null)
			{
				text2 = string.Concat(new string[]
				{
					text2,
					" ---> ",
					this._innerException.ToString(needFileLineInfo, needMessage),
					Environment.NewLine,
					"   ",
					Environment.GetResourceString("--- End of inner exception stack trace ---")
				});
			}
			string stackTrace = this.GetStackTrace(needFileLineInfo);
			if (stackTrace != null)
			{
				text2 = text2 + Environment.NewLine + stackTrace;
			}
			return text2;
		}

		/// <summary>Occurs when an exception is serialized to create an exception state object that contains serialized data about the exception.</summary>
		// Token: 0x14000003 RID: 3
		// (add) Token: 0x06000ED7 RID: 3799 RVA: 0x0003DF77 File Offset: 0x0003C177
		// (remove) Token: 0x06000ED8 RID: 3800 RVA: 0x0003DF85 File Offset: 0x0003C185
		protected event EventHandler<SafeSerializationEventArgs> SerializeObjectState
		{
			add
			{
				this._safeSerializationManager.SerializeObjectState += value;
			}
			remove
			{
				this._safeSerializationManager.SerializeObjectState -= value;
			}
		}

		/// <summary>When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with information about the exception.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is a null reference (<see langword="Nothing" /> in Visual Basic). </exception>
		// Token: 0x06000ED9 RID: 3801 RVA: 0x0003DF94 File Offset: 0x0003C194
		[SecurityCritical]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			string text = this._stackTraceString;
			if (this._stackTrace != null && text == null)
			{
				text = Environment.GetStackTrace(this, true);
			}
			if (this._source == null)
			{
				this._source = this.Source;
			}
			info.AddValue("ClassName", this.GetClassName(), typeof(string));
			info.AddValue("Message", this._message, typeof(string));
			info.AddValue("Data", this._data, typeof(IDictionary));
			info.AddValue("InnerException", this._innerException, typeof(Exception));
			info.AddValue("HelpURL", this._helpURL, typeof(string));
			info.AddValue("StackTraceString", text, typeof(string));
			info.AddValue("RemoteStackTraceString", this._remoteStackTraceString, typeof(string));
			info.AddValue("RemoteStackIndex", this._remoteStackIndex, typeof(int));
			info.AddValue("ExceptionMethod", null);
			info.AddValue("HResult", this.HResult);
			info.AddValue("Source", this._source, typeof(string));
			if (this._safeSerializationManager != null && this._safeSerializationManager.IsActive)
			{
				info.AddValue("SafeSerializationManager", this._safeSerializationManager, typeof(SafeSerializationManager));
				this._safeSerializationManager.CompleteSerialization(this, info, context);
			}
		}

		// Token: 0x06000EDA RID: 3802 RVA: 0x0003E12C File Offset: 0x0003C32C
		internal Exception PrepForRemoting()
		{
			string remoteStackTraceString;
			if (this._remoteStackIndex == 0)
			{
				remoteStackTraceString = string.Concat(new object[]
				{
					Environment.NewLine,
					"Server stack trace: ",
					Environment.NewLine,
					this.StackTrace,
					Environment.NewLine,
					Environment.NewLine,
					"Exception rethrown at [",
					this._remoteStackIndex,
					"]: ",
					Environment.NewLine
				});
			}
			else
			{
				remoteStackTraceString = string.Concat(new object[]
				{
					this.StackTrace,
					Environment.NewLine,
					Environment.NewLine,
					"Exception rethrown at [",
					this._remoteStackIndex,
					"]: ",
					Environment.NewLine
				});
			}
			this._remoteStackTraceString = remoteStackTraceString;
			this._remoteStackIndex++;
			return this;
		}

		// Token: 0x06000EDB RID: 3803 RVA: 0x0003E20B File Offset: 0x0003C40B
		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			this._stackTrace = null;
			if (this._safeSerializationManager == null)
			{
				this._safeSerializationManager = new SafeSerializationManager();
				return;
			}
			this._safeSerializationManager.CompleteDeserialization(this);
		}

		// Token: 0x06000EDC RID: 3804 RVA: 0x0003E234 File Offset: 0x0003C434
		internal void InternalPreserveStackTrace()
		{
			string stackTrace = this.StackTrace;
			if (stackTrace != null && stackTrace.Length > 0)
			{
				this._remoteStackTraceString = stackTrace + Environment.NewLine;
			}
			this._stackTrace = null;
			this._stackTraceString = null;
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x06000EDD RID: 3805 RVA: 0x0003E273 File Offset: 0x0003C473
		internal string RemoteStackTrace
		{
			get
			{
				return this._remoteStackTraceString;
			}
		}

		// Token: 0x06000EDE RID: 3806 RVA: 0x0000207C File Offset: 0x0000027C
		private string StripFileInfo(string stackTrace, bool isRemoteStackTrace)
		{
			return stackTrace;
		}

		// Token: 0x06000EDF RID: 3807 RVA: 0x0003E27B File Offset: 0x0003C47B
		[SecuritySafeCritical]
		internal void RestoreExceptionDispatchInfo(ExceptionDispatchInfo exceptionDispatchInfo)
		{
			this.captured_traces = (StackTrace[])exceptionDispatchInfo.BinaryStackTraceArray;
			this._stackTrace = null;
			this._stackTraceString = null;
		}

		/// <summary>Gets or sets HRESULT, a coded numerical value that is assigned to a specific exception.</summary>
		/// <returns>The HRESULT value.</returns>
		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x06000EE0 RID: 3808 RVA: 0x0003E29C File Offset: 0x0003C49C
		// (set) Token: 0x06000EE1 RID: 3809 RVA: 0x0003E2A4 File Offset: 0x0003C4A4
		public int HResult
		{
			get
			{
				return this._HResult;
			}
			protected set
			{
				this._HResult = value;
			}
		}

		// Token: 0x06000EE2 RID: 3810 RVA: 0x0003E2B0 File Offset: 0x0003C4B0
		[SecurityCritical]
		internal virtual string InternalToString()
		{
			bool needFileLineInfo = true;
			return this.ToString(needFileLineInfo, true);
		}

		/// <summary>Gets the runtime type of the current instance.</summary>
		/// <returns>A <see cref="T:System.Type" /> object that represents the exact runtime type of the current instance.</returns>
		// Token: 0x06000EE3 RID: 3811 RVA: 0x0003342D File Offset: 0x0003162D
		public new Type GetType()
		{
			return base.GetType();
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000EE4 RID: 3812 RVA: 0x0003E2C7 File Offset: 0x0003C4C7
		internal bool IsTransient
		{
			[SecuritySafeCritical]
			get
			{
				return Exception.nIsTransient(this._HResult);
			}
		}

		// Token: 0x06000EE5 RID: 3813
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool nIsTransient(int hr);

		// Token: 0x06000EE6 RID: 3814 RVA: 0x0003E2D4 File Offset: 0x0003C4D4
		[SecuritySafeCritical]
		internal static string GetMessageFromNativeResources(Exception.ExceptionMessageKind kind)
		{
			switch (kind)
			{
			case Exception.ExceptionMessageKind.ThreadAbort:
				return "";
			case Exception.ExceptionMessageKind.ThreadInterrupted:
				return "";
			case Exception.ExceptionMessageKind.OutOfMemory:
				return "Out of memory";
			default:
				return "";
			}
		}

		// Token: 0x06000EE7 RID: 3815 RVA: 0x0003E303 File Offset: 0x0003C503
		internal void SetMessage(string s)
		{
			this._message = s;
		}

		// Token: 0x06000EE8 RID: 3816 RVA: 0x0003E30C File Offset: 0x0003C50C
		internal void SetStackTrace(string s)
		{
			this._stackTraceString = s;
		}

		// Token: 0x06000EE9 RID: 3817 RVA: 0x0003E318 File Offset: 0x0003C518
		internal Exception FixRemotingException()
		{
			string remoteStackTraceString = string.Format((this._remoteStackIndex == 0) ? Locale.GetText("{0}{0}Server stack trace: {0}{1}{0}{0}Exception rethrown at [{2}]: {0}") : Locale.GetText("{1}{0}{0}Exception rethrown at [{2}]: {0}"), Environment.NewLine, this.StackTrace, this._remoteStackIndex);
			this._remoteStackTraceString = remoteStackTraceString;
			this._remoteStackIndex++;
			this._stackTraceString = null;
			return this;
		}

		// Token: 0x06000EEA RID: 3818 RVA: 0x0003E37C File Offset: 0x0003C57C
		// Note: this type is marked as 'beforefieldinit'.
		static Exception()
		{
		}

		// Token: 0x040008CB RID: 2251
		[OptionalField]
		private static object s_EDILock = new object();

		// Token: 0x040008CC RID: 2252
		private string _className;

		// Token: 0x040008CD RID: 2253
		internal string _message;

		// Token: 0x040008CE RID: 2254
		private IDictionary _data;

		// Token: 0x040008CF RID: 2255
		private Exception _innerException;

		// Token: 0x040008D0 RID: 2256
		private string _helpURL;

		// Token: 0x040008D1 RID: 2257
		private object _stackTrace;

		// Token: 0x040008D2 RID: 2258
		private string _stackTraceString;

		// Token: 0x040008D3 RID: 2259
		private string _remoteStackTraceString;

		// Token: 0x040008D4 RID: 2260
		private int _remoteStackIndex;

		// Token: 0x040008D5 RID: 2261
		private object _dynamicMethods;

		// Token: 0x040008D6 RID: 2262
		internal int _HResult;

		// Token: 0x040008D7 RID: 2263
		private string _source;

		// Token: 0x040008D8 RID: 2264
		[OptionalField(VersionAdded = 4)]
		private SafeSerializationManager _safeSerializationManager;

		// Token: 0x040008D9 RID: 2265
		internal StackTrace[] captured_traces;

		// Token: 0x040008DA RID: 2266
		private IntPtr[] native_trace_ips;

		// Token: 0x040008DB RID: 2267
		private const int _COMPlusExceptionCode = -532462766;

		// Token: 0x0200014D RID: 333
		internal enum ExceptionMessageKind
		{
			// Token: 0x040008DD RID: 2269
			ThreadAbort = 1,
			// Token: 0x040008DE RID: 2270
			ThreadInterrupted,
			// Token: 0x040008DF RID: 2271
			OutOfMemory
		}
	}
}
