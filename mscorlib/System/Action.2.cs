﻿using System;
using System.Runtime.CompilerServices;

namespace System
{
	/// <summary>Encapsulates a method that has no parameters and does not return a value.</summary>
	// Token: 0x020000FB RID: 251
	// (Invoke) Token: 0x0600097C RID: 2428
	[TypeForwardedFrom("System.Core, Version=2.0.5.0, Culture=Neutral, PublicKeyToken=7cec85d7bea7798e")]
	public delegate void Action();
}
