﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System
{
	/// <summary>The exception that is thrown when time zone information is invalid.</summary>
	// Token: 0x02000183 RID: 387
	[TypeForwardedFrom("System.Core, Version=2.0.5.0, Culture=Neutral, PublicKeyToken=7cec85d7bea7798e")]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[Serializable]
	public class InvalidTimeZoneException : Exception
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidTimeZoneException" /> class with the specified message string.</summary>
		/// <param name="message">A string that describes the exception.</param>
		// Token: 0x060010DB RID: 4315 RVA: 0x000474A0 File Offset: 0x000456A0
		public InvalidTimeZoneException(string message) : base(message)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidTimeZoneException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">A string that describes the exception.    </param>
		/// <param name="innerException">The exception that is the cause of the current exception.  </param>
		// Token: 0x060010DC RID: 4316 RVA: 0x000474A9 File Offset: 0x000456A9
		public InvalidTimeZoneException(string message, Exception innerException) : base(message, innerException)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidTimeZoneException" /> class from serialized data.</summary>
		/// <param name="info">The object that contains the serialized data. </param>
		/// <param name="context">The stream that contains the serialized data.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is <see langword="null" />.-or-The <paramref name="context" /> parameter is <see langword="null" />.</exception>
		// Token: 0x060010DD RID: 4317 RVA: 0x00031FF0 File Offset: 0x000301F0
		protected InvalidTimeZoneException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidTimeZoneException" /> class with a system-supplied message.</summary>
		// Token: 0x060010DE RID: 4318 RVA: 0x000474B3 File Offset: 0x000456B3
		public InvalidTimeZoneException()
		{
		}
	}
}
