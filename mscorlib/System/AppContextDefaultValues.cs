﻿using System;

namespace System
{
	// Token: 0x020001DC RID: 476
	internal static class AppContextDefaultValues
	{
		// Token: 0x06001658 RID: 5720 RVA: 0x000020D3 File Offset: 0x000002D3
		public static void PopulateDefaultValues()
		{
		}

		// Token: 0x06001659 RID: 5721 RVA: 0x0005968C File Offset: 0x0005788C
		public static bool TryGetSwitchOverride(string switchName, out bool overrideValue)
		{
			overrideValue = false;
			return false;
		}

		// Token: 0x04000BB2 RID: 2994
		internal const string SwitchNoAsyncCurrentCulture = "Switch.System.Globalization.NoAsyncCurrentCulture";

		// Token: 0x04000BB3 RID: 2995
		internal const string SwitchThrowExceptionIfDisposedCancellationTokenSource = "Switch.System.Threading.ThrowExceptionIfDisposedCancellationTokenSource";

		// Token: 0x04000BB4 RID: 2996
		internal const string SwitchPreserveEventListnerObjectIdentity = "Switch.System.Diagnostics.EventSource.PreserveEventListnerObjectIdentity";

		// Token: 0x04000BB5 RID: 2997
		internal const string SwitchUseLegacyPathHandling = "Switch.System.IO.UseLegacyPathHandling";

		// Token: 0x04000BB6 RID: 2998
		internal const string SwitchBlockLongPaths = "Switch.System.IO.BlockLongPaths";

		// Token: 0x04000BB7 RID: 2999
		internal const string SwitchDoNotAddrOfCspParentWindowHandle = "Switch.System.Security.Cryptography.DoNotAddrOfCspParentWindowHandle";

		// Token: 0x04000BB8 RID: 3000
		internal const string SwitchSetActorAsReferenceWhenCopyingClaimsIdentity = "Switch.System.Security.ClaimsIdentity.SetActorAsReferenceWhenCopyingClaimsIdentity";
	}
}
