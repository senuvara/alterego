﻿using System;

namespace System
{
	// Token: 0x020000EC RID: 236
	internal static class LazyHelpers
	{
		// Token: 0x06000935 RID: 2357 RVA: 0x00030C57 File Offset: 0x0002EE57
		// Note: this type is marked as 'beforefieldinit'.
		static LazyHelpers()
		{
		}

		// Token: 0x040006D8 RID: 1752
		internal static readonly object PUBLICATION_ONLY_SENTINEL = new object();
	}
}
