﻿using System;
using Unity;

namespace System
{
	/// <summary>Provides data for the <see cref="E:System.Console.CancelKeyPress" /> event. This class cannot be inherited.</summary>
	// Token: 0x0200012A RID: 298
	[Serializable]
	public sealed class ConsoleCancelEventArgs : EventArgs
	{
		// Token: 0x06000B59 RID: 2905 RVA: 0x00035506 File Offset: 0x00033706
		internal ConsoleCancelEventArgs(ConsoleSpecialKey type)
		{
			this._type = type;
			this._cancel = false;
		}

		/// <summary>
		///     Gets or sets a value that indicates whether simultaneously pressing the <see cref="F:System.ConsoleModifiers.Control" /> modifier key and the <see cref="F:System.ConsoleKey.C" /> console key (Ctrl+C) or the Ctrl+Break keys terminates the current process. The default is <see langword="false" />, which terminates the current process. </summary>
		/// <returns>
		///     <see langword="true" /> if the current process should resume when the event handler concludes; <see langword="false" /> if the current process should terminate. The default value is <see langword="false" />; the current process terminates when the event handler returns. If <see langword="true" />, the current process continues. </returns>
		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06000B5A RID: 2906 RVA: 0x0003551C File Offset: 0x0003371C
		// (set) Token: 0x06000B5B RID: 2907 RVA: 0x00035524 File Offset: 0x00033724
		public bool Cancel
		{
			get
			{
				return this._cancel;
			}
			set
			{
				this._cancel = value;
			}
		}

		/// <summary>Gets the combination of modifier and console keys that interrupted the current process.</summary>
		/// <returns>One of the enumeration values that specifies the key combination that interrupted the current process. There is no default value.</returns>
		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06000B5C RID: 2908 RVA: 0x0003552D File Offset: 0x0003372D
		public ConsoleSpecialKey SpecialKey
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x06000B5D RID: 2909 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal ConsoleCancelEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400079F RID: 1951
		private ConsoleSpecialKey _type;

		// Token: 0x040007A0 RID: 1952
		private bool _cancel;
	}
}
