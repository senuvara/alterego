﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when a requested method or operation is not implemented.</summary>
	// Token: 0x02000192 RID: 402
	[ComVisible(true)]
	[Serializable]
	public class NotImplementedException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.NotImplementedException" /> class with default properties.</summary>
		// Token: 0x06001163 RID: 4451 RVA: 0x00047EA8 File Offset: 0x000460A8
		public NotImplementedException() : base(Environment.GetResourceString("The method or operation is not implemented."))
		{
			base.SetErrorCode(-2147467263);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.NotImplementedException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x06001164 RID: 4452 RVA: 0x00047EC5 File Offset: 0x000460C5
		public NotImplementedException(string message) : base(message)
		{
			base.SetErrorCode(-2147467263);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.NotImplementedException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06001165 RID: 4453 RVA: 0x00047ED9 File Offset: 0x000460D9
		public NotImplementedException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2147467263);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.NotImplementedException" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		// Token: 0x06001166 RID: 4454 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected NotImplementedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
