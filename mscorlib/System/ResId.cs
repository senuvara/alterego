﻿using System;

namespace System
{
	// Token: 0x020001A3 RID: 419
	internal static class ResId
	{
		// Token: 0x04000A28 RID: 2600
		internal const string Arg_ArrayLengthsDiffer = "Arg_ArrayLengthsDiffer";

		// Token: 0x04000A29 RID: 2601
		internal const string Argument_InvalidNumberOfMembers = "Argument_InvalidNumberOfMembers";

		// Token: 0x04000A2A RID: 2602
		internal const string Argument_UnequalMembers = "Argument_UnequalMembers";

		// Token: 0x04000A2B RID: 2603
		internal const string Argument_SpecifyValueSize = "Argument_SpecifyValueSize";

		// Token: 0x04000A2C RID: 2604
		internal const string Argument_UnmatchingSymScope = "Argument_UnmatchingSymScope";

		// Token: 0x04000A2D RID: 2605
		internal const string Argument_NotInExceptionBlock = "Argument_NotInExceptionBlock";

		// Token: 0x04000A2E RID: 2606
		internal const string Argument_NotExceptionType = "Argument_NotExceptionType";

		// Token: 0x04000A2F RID: 2607
		internal const string Argument_InvalidLabel = "Argument_InvalidLabel";

		// Token: 0x04000A30 RID: 2608
		internal const string Argument_UnclosedExceptionBlock = "Argument_UnclosedExceptionBlock";

		// Token: 0x04000A31 RID: 2609
		internal const string Argument_MissingDefaultConstructor = "Argument_MissingDefaultConstructor";

		// Token: 0x04000A32 RID: 2610
		internal const string Argument_TooManyFinallyClause = "Argument_TooManyFinallyClause";

		// Token: 0x04000A33 RID: 2611
		internal const string Argument_NotInTheSameModuleBuilder = "Argument_NotInTheSameModuleBuilder";

		// Token: 0x04000A34 RID: 2612
		internal const string Argument_BadCurrentLocalVariable = "Argument_BadCurrentLocalVariable";

		// Token: 0x04000A35 RID: 2613
		internal const string Argument_DuplicateModuleName = "Argument_DuplicateModuleName";

		// Token: 0x04000A36 RID: 2614
		internal const string Argument_BadPersistableModuleInTransientAssembly = "Argument_BadPersistableModuleInTransientAssembly";

		// Token: 0x04000A37 RID: 2615
		internal const string Argument_HasToBeArrayClass = "Argument_HasToBeArrayClass";

		// Token: 0x04000A38 RID: 2616
		internal const string Argument_InvalidDirectory = "Argument_InvalidDirectory";

		// Token: 0x04000A39 RID: 2617
		internal const string MissingType = "MissingType";

		// Token: 0x04000A3A RID: 2618
		internal const string MissingModule = "MissingModule";

		// Token: 0x04000A3B RID: 2619
		internal const string ArgumentOutOfRange_Index = "ArgumentOutOfRange_Index";

		// Token: 0x04000A3C RID: 2620
		internal const string ArgumentOutOfRange_Range = "ArgumentOutOfRange_Range";

		// Token: 0x04000A3D RID: 2621
		internal const string ExecutionEngine_YoureHosed = "ExecutionEngine_YoureHosed";

		// Token: 0x04000A3E RID: 2622
		internal const string Format_NeedSingleChar = "Format_NeedSingleChar";

		// Token: 0x04000A3F RID: 2623
		internal const string Format_StringZeroLength = "Format_StringZeroLength";

		// Token: 0x04000A40 RID: 2624
		internal const string InvalidOperation_EnumEnded = "InvalidOperation_EnumEnded";

		// Token: 0x04000A41 RID: 2625
		internal const string InvalidOperation_EnumFailedVersion = "InvalidOperation_EnumFailedVersion";

		// Token: 0x04000A42 RID: 2626
		internal const string InvalidOperation_EnumNotStarted = "InvalidOperation_EnumNotStarted";

		// Token: 0x04000A43 RID: 2627
		internal const string InvalidOperation_EnumOpCantHappen = "InvalidOperation_EnumOpCantHappen";

		// Token: 0x04000A44 RID: 2628
		internal const string InvalidOperation_InternalState = "InvalidOperation_InternalState";

		// Token: 0x04000A45 RID: 2629
		internal const string InvalidOperation_ModifyRONumFmtInfo = "InvalidOperation_ModifyRONumFmtInfo";

		// Token: 0x04000A46 RID: 2630
		internal const string InvalidOperation_MethodBaked = "InvalidOperation_MethodBaked";

		// Token: 0x04000A47 RID: 2631
		internal const string InvalidOperation_NotADebugModule = "InvalidOperation_NotADebugModule";

		// Token: 0x04000A48 RID: 2632
		internal const string InvalidOperation_MethodHasBody = "InvalidOperation_MethodHasBody";

		// Token: 0x04000A49 RID: 2633
		internal const string InvalidOperation_OpenLocalVariableScope = "InvalidOperation_OpenLocalVariableScope";

		// Token: 0x04000A4A RID: 2634
		internal const string InvalidOperation_TypeHasBeenCreated = "InvalidOperation_TypeHasBeenCreated";

		// Token: 0x04000A4B RID: 2635
		internal const string InvalidOperation_RefedAssemblyNotSaved = "InvalidOperation_RefedAssemblyNotSaved";

		// Token: 0x04000A4C RID: 2636
		internal const string InvalidOperation_AssemblyHasBeenSaved = "InvalidOperation_AssemblyHasBeenSaved";

		// Token: 0x04000A4D RID: 2637
		internal const string InvalidOperation_ModuleHasBeenSaved = "InvalidOperation_ModuleHasBeenSaved";

		// Token: 0x04000A4E RID: 2638
		internal const string InvalidOperation_CannotAlterAssembly = "InvalidOperation_CannotAlterAssembly";

		// Token: 0x04000A4F RID: 2639
		internal const string NotSupported_CannotSaveModuleIndividually = "NotSupported_CannotSaveModuleIndividually";

		// Token: 0x04000A50 RID: 2640
		internal const string NotSupported_Constructor = "NotSupported_Constructor";

		// Token: 0x04000A51 RID: 2641
		internal const string NotSupported_Method = "NotSupported_Method";

		// Token: 0x04000A52 RID: 2642
		internal const string NotSupported_NYI = "NotSupported_NYI";

		// Token: 0x04000A53 RID: 2643
		internal const string NotSupported_DynamicModule = "NotSupported_DynamicModule";

		// Token: 0x04000A54 RID: 2644
		internal const string NotSupported_NotDynamicModule = "NotSupported_NotDynamicModule";

		// Token: 0x04000A55 RID: 2645
		internal const string NotSupported_NotAllTypesAreBaked = "NotSupported_NotAllTypesAreBaked";

		// Token: 0x04000A56 RID: 2646
		internal const string NotSupported_SortedListNestedWrite = "NotSupported_SortedListNestedWrite";

		// Token: 0x04000A57 RID: 2647
		internal const string Serialization_ArrayInvalidLength = "Serialization_ArrayInvalidLength";

		// Token: 0x04000A58 RID: 2648
		internal const string Serialization_ArrayNoLength = "Serialization_ArrayNoLength";

		// Token: 0x04000A59 RID: 2649
		internal const string Serialization_CannotGetType = "Serialization_CannotGetType";

		// Token: 0x04000A5A RID: 2650
		internal const string Serialization_InsufficientState = "Serialization_InsufficientState";

		// Token: 0x04000A5B RID: 2651
		internal const string Serialization_InvalidID = "Serialization_InvalidID";

		// Token: 0x04000A5C RID: 2652
		internal const string Serialization_MalformedArray = "Serialization_MalformedArray";

		// Token: 0x04000A5D RID: 2653
		internal const string Serialization_MultipleMembers = "Serialization_MultipleMembers";

		// Token: 0x04000A5E RID: 2654
		internal const string Serialization_NoID = "Serialization_NoID";

		// Token: 0x04000A5F RID: 2655
		internal const string Serialization_NoType = "Serialization_NoType";

		// Token: 0x04000A60 RID: 2656
		internal const string Serialization_NoBaseType = "Serialization_NoBaseType";

		// Token: 0x04000A61 RID: 2657
		internal const string Serialization_NullSignature = "Serialization_NullSignature";

		// Token: 0x04000A62 RID: 2658
		internal const string Serialization_UnknownMember = "Serialization_UnknownMember";

		// Token: 0x04000A63 RID: 2659
		internal const string Serialization_BadParameterInfo = "Serialization_BadParameterInfo";

		// Token: 0x04000A64 RID: 2660
		internal const string Serialization_NoParameterInfo = "Serialization_NoParameterInfo";

		// Token: 0x04000A65 RID: 2661
		internal const string WeakReference_NoLongerValid = "WeakReference_NoLongerValid";

		// Token: 0x04000A66 RID: 2662
		internal const string Loader_InvalidPath = "Loader_InvalidPath";
	}
}
