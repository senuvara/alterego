﻿using System;

namespace System
{
	/// <summary>Provides a mechanism for receiving push-based notifications.</summary>
	/// <typeparam name="T">The object that provides notification information.</typeparam>
	// Token: 0x02000185 RID: 389
	public interface IObserver<in T>
	{
		/// <summary>Provides the observer with new data.</summary>
		/// <param name="value">The current notification information.</param>
		// Token: 0x060010E0 RID: 4320
		void OnNext(T value);

		/// <summary>Notifies the observer that the provider has experienced an error condition.</summary>
		/// <param name="error">An object that provides additional information about the error.</param>
		// Token: 0x060010E1 RID: 4321
		void OnError(Exception error);

		/// <summary>Notifies the observer that the provider has finished sending push-based notifications.</summary>
		// Token: 0x060010E2 RID: 4322
		void OnCompleted();
	}
}
