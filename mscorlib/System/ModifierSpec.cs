﻿using System;
using System.Text;

namespace System
{
	// Token: 0x0200023B RID: 571
	internal interface ModifierSpec
	{
		// Token: 0x06001AF7 RID: 6903
		Type Resolve(Type type);

		// Token: 0x06001AF8 RID: 6904
		StringBuilder Append(StringBuilder sb);
	}
}
