﻿using System;

namespace System
{
	// Token: 0x020000E1 RID: 225
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoTODOAttribute : Attribute
	{
		// Token: 0x06000907 RID: 2311 RVA: 0x000020BF File Offset: 0x000002BF
		public MonoTODOAttribute()
		{
		}

		// Token: 0x06000908 RID: 2312 RVA: 0x00030525 File Offset: 0x0002E725
		public MonoTODOAttribute(string comment)
		{
			this.comment = comment;
		}

		// Token: 0x1700017F RID: 383
		// (get) Token: 0x06000909 RID: 2313 RVA: 0x00030534 File Offset: 0x0002E734
		public string Comment
		{
			get
			{
				return this.comment;
			}
		}

		// Token: 0x040006CC RID: 1740
		private string comment;
	}
}
