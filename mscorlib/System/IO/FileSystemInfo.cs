﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using Microsoft.Win32;

namespace System.IO
{
	/// <summary>Provides the base class for both <see cref="T:System.IO.FileInfo" /> and <see cref="T:System.IO.DirectoryInfo" /> objects.</summary>
	// Token: 0x02000368 RID: 872
	[ComVisible(true)]
	[Serializable]
	public abstract class FileSystemInfo : MarshalByRefObject, ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.FileSystemInfo" /> class.</summary>
		// Token: 0x06002758 RID: 10072 RVA: 0x0008A232 File Offset: 0x00088432
		protected FileSystemInfo()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.FileSystemInfo" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The specified <see cref="T:System.Runtime.Serialization.SerializationInfo" /> is null.</exception>
		// Token: 0x06002759 RID: 10073 RVA: 0x0008A24C File Offset: 0x0008844C
		protected FileSystemInfo(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.FullPath = Path.GetFullPathInternal(info.GetString("FullPath"));
			this.OriginalPath = info.GetString("OriginalPath");
			this._dataInitialised = -1;
		}

		// Token: 0x0600275A RID: 10074 RVA: 0x000041F3 File Offset: 0x000023F3
		[SecurityCritical]
		internal void InitializeFrom(Win32Native.WIN32_FIND_DATA findData)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the full path of the directory or file.</summary>
		/// <returns>A string containing the full path.</returns>
		/// <exception cref="T:System.IO.PathTooLongException">The fully qualified path and file name is 260 or more characters.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x170005FE RID: 1534
		// (get) Token: 0x0600275B RID: 10075 RVA: 0x0008A2AD File Offset: 0x000884AD
		public virtual string FullName
		{
			[SecuritySafeCritical]
			get
			{
				return this.FullPath;
			}
		}

		// Token: 0x170005FF RID: 1535
		// (get) Token: 0x0600275C RID: 10076 RVA: 0x0008A2AD File Offset: 0x000884AD
		internal virtual string UnsafeGetFullName
		{
			[SecurityCritical]
			get
			{
				return this.FullPath;
			}
		}

		/// <summary>Gets the string representing the extension part of the file.</summary>
		/// <returns>A string containing the <see cref="T:System.IO.FileSystemInfo" /> extension.</returns>
		// Token: 0x17000600 RID: 1536
		// (get) Token: 0x0600275D RID: 10077 RVA: 0x0008A2B8 File Offset: 0x000884B8
		public string Extension
		{
			get
			{
				int length = this.FullPath.Length;
				int num = length;
				while (--num >= 0)
				{
					char c = this.FullPath[num];
					if (c == '.')
					{
						return this.FullPath.Substring(num, length - num);
					}
					if (c == Path.DirectorySeparatorChar || c == Path.AltDirectorySeparatorChar || c == Path.VolumeSeparatorChar)
					{
						break;
					}
				}
				return string.Empty;
			}
		}

		/// <summary>For files, gets the name of the file. For directories, gets the name of the last directory in the hierarchy if a hierarchy exists. Otherwise, the <see langword="Name" /> property gets the name of the directory.</summary>
		/// <returns>A string that is the name of the parent directory, the name of the last directory in the hierarchy, or the name of a file, including the file name extension.</returns>
		// Token: 0x17000601 RID: 1537
		// (get) Token: 0x0600275E RID: 10078
		public abstract string Name { get; }

		/// <summary>Gets a value indicating whether the file or directory exists.</summary>
		/// <returns>
		///     <see langword="true" /> if the file or directory exists; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000602 RID: 1538
		// (get) Token: 0x0600275F RID: 10079
		public abstract bool Exists { get; }

		/// <summary>Deletes a file or directory.</summary>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid; for example, it is on an unmapped drive.</exception>
		/// <exception cref="T:System.IO.IOException">There is an open handle on the file or directory, and the operating system is Windows XP or earlier. This open handle can result from enumerating directories and files. For more information, see How to: Enumerate Directories and Files.</exception>
		// Token: 0x06002760 RID: 10080
		public abstract void Delete();

		/// <summary>Gets or sets the creation time of the current file or directory.</summary>
		/// <returns>The creation date and time of the current <see cref="T:System.IO.FileSystemInfo" /> object.</returns>
		/// <exception cref="T:System.IO.IOException">
		///         <see cref="M:System.IO.FileSystemInfo.Refresh" /> cannot initialize the data. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid; for example, it is on an unmapped drive.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Windows NT or later.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The caller attempts to set an invalid creation time.</exception>
		// Token: 0x17000603 RID: 1539
		// (get) Token: 0x06002761 RID: 10081 RVA: 0x0008A31C File Offset: 0x0008851C
		// (set) Token: 0x06002762 RID: 10082 RVA: 0x0008A337 File Offset: 0x00088537
		public DateTime CreationTime
		{
			get
			{
				return this.CreationTimeUtc.ToLocalTime();
			}
			set
			{
				this.CreationTimeUtc = value.ToUniversalTime();
			}
		}

		/// <summary>Gets or sets the creation time, in coordinated universal time (UTC), of the current file or directory.</summary>
		/// <returns>The creation date and time in UTC format of the current <see cref="T:System.IO.FileSystemInfo" /> object.</returns>
		/// <exception cref="T:System.IO.IOException">
		///         <see cref="M:System.IO.FileSystemInfo.Refresh" /> cannot initialize the data. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid; for example, it is on an unmapped drive.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Windows NT or later.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The caller attempts to set an invalid access time.</exception>
		// Token: 0x17000604 RID: 1540
		// (get) Token: 0x06002763 RID: 10083 RVA: 0x0008A346 File Offset: 0x00088546
		// (set) Token: 0x06002764 RID: 10084 RVA: 0x0008A380 File Offset: 0x00088580
		[ComVisible(false)]
		public DateTime CreationTimeUtc
		{
			[SecuritySafeCritical]
			get
			{
				if (this._dataInitialised == -1)
				{
					this.Refresh();
				}
				if (this._dataInitialised != 0)
				{
					__Error.WinIOError(this._dataInitialised, this.DisplayPath);
				}
				return DateTime.FromFileTimeUtc(this._data.CreationTime);
			}
			set
			{
				if (this is DirectoryInfo)
				{
					Directory.SetCreationTimeUtc(this.FullPath, value);
				}
				else
				{
					File.SetCreationTimeUtc(this.FullPath, value);
				}
				this._dataInitialised = -1;
			}
		}

		/// <summary>Gets or sets the time the current file or directory was last accessed.</summary>
		/// <returns>The time that the current file or directory was last accessed.</returns>
		/// <exception cref="T:System.IO.IOException">
		///         <see cref="M:System.IO.FileSystemInfo.Refresh" /> cannot initialize the data. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Windows NT or later.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The caller attempts to set an invalid access time</exception>
		// Token: 0x17000605 RID: 1541
		// (get) Token: 0x06002765 RID: 10085 RVA: 0x0008A3AC File Offset: 0x000885AC
		// (set) Token: 0x06002766 RID: 10086 RVA: 0x0008A3C7 File Offset: 0x000885C7
		public DateTime LastAccessTime
		{
			get
			{
				return this.LastAccessTimeUtc.ToLocalTime();
			}
			set
			{
				this.LastAccessTimeUtc = value.ToUniversalTime();
			}
		}

		/// <summary>Gets or sets the time, in coordinated universal time (UTC), that the current file or directory was last accessed.</summary>
		/// <returns>The UTC time that the current file or directory was last accessed.</returns>
		/// <exception cref="T:System.IO.IOException">
		///         <see cref="M:System.IO.FileSystemInfo.Refresh" /> cannot initialize the data. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Windows NT or later.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The caller attempts to set an invalid access time.</exception>
		// Token: 0x17000606 RID: 1542
		// (get) Token: 0x06002767 RID: 10087 RVA: 0x0008A3D6 File Offset: 0x000885D6
		// (set) Token: 0x06002768 RID: 10088 RVA: 0x0008A410 File Offset: 0x00088610
		[ComVisible(false)]
		public DateTime LastAccessTimeUtc
		{
			[SecuritySafeCritical]
			get
			{
				if (this._dataInitialised == -1)
				{
					this.Refresh();
				}
				if (this._dataInitialised != 0)
				{
					__Error.WinIOError(this._dataInitialised, this.DisplayPath);
				}
				return DateTime.FromFileTimeUtc(this._data.LastAccessTime);
			}
			set
			{
				if (this is DirectoryInfo)
				{
					Directory.SetLastAccessTimeUtc(this.FullPath, value);
				}
				else
				{
					File.SetLastAccessTimeUtc(this.FullPath, value);
				}
				this._dataInitialised = -1;
			}
		}

		/// <summary>Gets or sets the time when the current file or directory was last written to.</summary>
		/// <returns>The time the current file was last written.</returns>
		/// <exception cref="T:System.IO.IOException">
		///         <see cref="M:System.IO.FileSystemInfo.Refresh" /> cannot initialize the data. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Windows NT or later.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The caller attempts to set an invalid write time.</exception>
		// Token: 0x17000607 RID: 1543
		// (get) Token: 0x06002769 RID: 10089 RVA: 0x0008A43C File Offset: 0x0008863C
		// (set) Token: 0x0600276A RID: 10090 RVA: 0x0008A457 File Offset: 0x00088657
		public DateTime LastWriteTime
		{
			get
			{
				return this.LastWriteTimeUtc.ToLocalTime();
			}
			set
			{
				this.LastWriteTimeUtc = value.ToUniversalTime();
			}
		}

		/// <summary>Gets or sets the time, in coordinated universal time (UTC), when the current file or directory was last written to.</summary>
		/// <returns>The UTC time when the current file was last written to.</returns>
		/// <exception cref="T:System.IO.IOException">
		///         <see cref="M:System.IO.FileSystemInfo.Refresh" /> cannot initialize the data. </exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Windows NT or later.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The caller attempts to set an invalid write time.</exception>
		// Token: 0x17000608 RID: 1544
		// (get) Token: 0x0600276B RID: 10091 RVA: 0x0008A466 File Offset: 0x00088666
		// (set) Token: 0x0600276C RID: 10092 RVA: 0x0008A4A0 File Offset: 0x000886A0
		[ComVisible(false)]
		public DateTime LastWriteTimeUtc
		{
			[SecuritySafeCritical]
			get
			{
				if (this._dataInitialised == -1)
				{
					this.Refresh();
				}
				if (this._dataInitialised != 0)
				{
					__Error.WinIOError(this._dataInitialised, this.DisplayPath);
				}
				return DateTime.FromFileTimeUtc(this._data.LastWriteTime);
			}
			set
			{
				if (this is DirectoryInfo)
				{
					Directory.SetLastWriteTimeUtc(this.FullPath, value);
				}
				else
				{
					File.SetLastWriteTimeUtc(this.FullPath, value);
				}
				this._dataInitialised = -1;
			}
		}

		/// <summary>Refreshes the state of the object.</summary>
		/// <exception cref="T:System.IO.IOException">A device such as a disk drive is not ready. </exception>
		// Token: 0x0600276D RID: 10093 RVA: 0x0008A4CB File Offset: 0x000886CB
		[SecuritySafeCritical]
		public void Refresh()
		{
			this._dataInitialised = File.FillAttributeInfo(this.FullPath, ref this._data, false, false);
		}

		/// <summary>Gets or sets the attributes for the current file or directory.</summary>
		/// <returns>
		///     <see cref="T:System.IO.FileAttributes" /> of the current <see cref="T:System.IO.FileSystemInfo" />.</returns>
		/// <exception cref="T:System.IO.FileNotFoundException">The specified file does not exist. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid; for example, it is on an unmapped drive. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">The caller attempts to set an invalid file attribute. -or-The user attempts to set an attribute value but does not have write permission.</exception>
		/// <exception cref="T:System.IO.IOException">
		///         <see cref="M:System.IO.FileSystemInfo.Refresh" /> cannot initialize the data. </exception>
		// Token: 0x17000609 RID: 1545
		// (get) Token: 0x0600276E RID: 10094 RVA: 0x0008A4E6 File Offset: 0x000886E6
		// (set) Token: 0x0600276F RID: 10095 RVA: 0x0008A51C File Offset: 0x0008871C
		public FileAttributes Attributes
		{
			[SecuritySafeCritical]
			get
			{
				if (this._dataInitialised == -1)
				{
					this.Refresh();
				}
				if (this._dataInitialised != 0)
				{
					__Error.WinIOError(this._dataInitialised, this.DisplayPath);
				}
				return this._data.fileAttributes;
			}
			[SecuritySafeCritical]
			set
			{
				MonoIOError monoIOError;
				if (!MonoIO.SetFileAttributes(this.FullPath, value, out monoIOError))
				{
					MonoIOError monoIOError2 = monoIOError;
					if (monoIOError2 == MonoIOError.ERROR_INVALID_PARAMETER)
					{
						throw new ArgumentException(Environment.GetResourceString("Invalid File or Directory attributes value."));
					}
					if (monoIOError2 == MonoIOError.ERROR_ACCESS_DENIED)
					{
						throw new ArgumentException(Environment.GetResourceString("Access to the path is denied."));
					}
					__Error.WinIOError((int)monoIOError2, this.DisplayPath);
				}
				this._dataInitialised = -1;
			}
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the file name and additional exception information.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		// Token: 0x06002770 RID: 10096 RVA: 0x0008A575 File Offset: 0x00088775
		[SecurityCritical]
		[ComVisible(false)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("OriginalPath", this.OriginalPath, typeof(string));
			info.AddValue("FullPath", this.FullPath, typeof(string));
		}

		// Token: 0x1700060A RID: 1546
		// (get) Token: 0x06002771 RID: 10097 RVA: 0x0008A5AD File Offset: 0x000887AD
		// (set) Token: 0x06002772 RID: 10098 RVA: 0x0008A5B5 File Offset: 0x000887B5
		internal string DisplayPath
		{
			get
			{
				return this._displayPath;
			}
			set
			{
				this._displayPath = value;
			}
		}

		// Token: 0x040014F8 RID: 5368
		internal MonoIOStat _data;

		// Token: 0x040014F9 RID: 5369
		internal int _dataInitialised = -1;

		// Token: 0x040014FA RID: 5370
		private const int ERROR_INVALID_PARAMETER = 87;

		// Token: 0x040014FB RID: 5371
		internal const int ERROR_ACCESS_DENIED = 5;

		/// <summary>Represents the fully qualified path of the directory or file.</summary>
		/// <exception cref="T:System.IO.PathTooLongException">The fully qualified path is 260 or more characters.</exception>
		// Token: 0x040014FC RID: 5372
		protected string FullPath;

		/// <summary>The path originally specified by the user, whether relative or absolute.</summary>
		// Token: 0x040014FD RID: 5373
		protected string OriginalPath;

		// Token: 0x040014FE RID: 5374
		private string _displayPath = "";
	}
}
