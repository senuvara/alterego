﻿using System;
using System.Text;

namespace System.IO
{
	// Token: 0x020003AE RID: 942
	internal class UnexceptionalStreamWriter : StreamWriter
	{
		// Token: 0x06002B54 RID: 11092 RVA: 0x00098D14 File Offset: 0x00096F14
		public UnexceptionalStreamWriter(Stream stream, Encoding encoding) : base(stream, encoding, 1024, true)
		{
		}

		// Token: 0x06002B55 RID: 11093 RVA: 0x00098D24 File Offset: 0x00096F24
		public override void Flush()
		{
			try
			{
				base.Flush();
			}
			catch (Exception)
			{
			}
		}

		// Token: 0x06002B56 RID: 11094 RVA: 0x00098D4C File Offset: 0x00096F4C
		public override void Write(char[] buffer, int index, int count)
		{
			try
			{
				base.Write(buffer, index, count);
			}
			catch (Exception)
			{
			}
		}

		// Token: 0x06002B57 RID: 11095 RVA: 0x00098D78 File Offset: 0x00096F78
		public override void Write(char value)
		{
			try
			{
				base.Write(value);
			}
			catch (Exception)
			{
			}
		}

		// Token: 0x06002B58 RID: 11096 RVA: 0x00098DA4 File Offset: 0x00096FA4
		public override void Write(char[] value)
		{
			try
			{
				base.Write(value);
			}
			catch (Exception)
			{
			}
		}

		// Token: 0x06002B59 RID: 11097 RVA: 0x00098DD0 File Offset: 0x00096FD0
		public override void Write(string value)
		{
			try
			{
				base.Write(value);
			}
			catch (Exception)
			{
			}
		}
	}
}
