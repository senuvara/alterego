﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.IO
{
	// Token: 0x020003AF RID: 943
	internal class CStreamReader : StreamReader
	{
		// Token: 0x06002B5A RID: 11098 RVA: 0x00098DFC File Offset: 0x00096FFC
		public CStreamReader(Stream stream, Encoding encoding) : base(stream, encoding)
		{
			this.driver = (TermInfoDriver)ConsoleDriver.driver;
		}

		// Token: 0x06002B5B RID: 11099 RVA: 0x00098E18 File Offset: 0x00097018
		public override int Peek()
		{
			try
			{
				return base.Peek();
			}
			catch (IOException)
			{
			}
			return -1;
		}

		// Token: 0x06002B5C RID: 11100 RVA: 0x00098E44 File Offset: 0x00097044
		public override int Read()
		{
			try
			{
				return (int)Console.ReadKey().KeyChar;
			}
			catch (IOException)
			{
			}
			return -1;
		}

		// Token: 0x06002B5D RID: 11101 RVA: 0x00098E78 File Offset: 0x00097078
		public override int Read([In] [Out] char[] dest, int index, int count)
		{
			if (dest == null)
			{
				throw new ArgumentNullException("dest");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (index > dest.Length - count)
			{
				throw new ArgumentException("index + count > dest.Length");
			}
			try
			{
				return this.driver.Read(dest, index, count);
			}
			catch (IOException)
			{
			}
			return 0;
		}

		// Token: 0x06002B5E RID: 11102 RVA: 0x00098EF8 File Offset: 0x000970F8
		public override string ReadLine()
		{
			try
			{
				return this.driver.ReadLine();
			}
			catch (IOException)
			{
			}
			return null;
		}

		// Token: 0x06002B5F RID: 11103 RVA: 0x00098F2C File Offset: 0x0009712C
		public override string ReadToEnd()
		{
			try
			{
				return this.driver.ReadToEnd();
			}
			catch (IOException)
			{
			}
			return null;
		}

		// Token: 0x040016AC RID: 5804
		private TermInfoDriver driver;
	}
}
