﻿using System;

namespace System.IO
{
	// Token: 0x020003AB RID: 939
	internal class SearchPattern
	{
		// Token: 0x06002B4A RID: 11082 RVA: 0x00002050 File Offset: 0x00000250
		public SearchPattern()
		{
		}

		// Token: 0x06002B4B RID: 11083 RVA: 0x00098AE6 File Offset: 0x00096CE6
		// Note: this type is marked as 'beforefieldinit'.
		static SearchPattern()
		{
		}

		// Token: 0x040016A5 RID: 5797
		internal static readonly char[] WildcardChars = new char[]
		{
			'*',
			'?'
		};
	}
}
