﻿using System;
using System.Text;

namespace System.IO
{
	// Token: 0x020003B0 RID: 944
	internal class CStreamWriter : StreamWriter
	{
		// Token: 0x06002B60 RID: 11104 RVA: 0x00098F60 File Offset: 0x00097160
		public CStreamWriter(Stream stream, Encoding encoding, bool leaveOpen) : base(stream, encoding, 1024, leaveOpen)
		{
			this.driver = (TermInfoDriver)ConsoleDriver.driver;
		}

		// Token: 0x06002B61 RID: 11105 RVA: 0x00098F80 File Offset: 0x00097180
		public override void Write(char[] buffer, int index, int count)
		{
			if (count <= 0)
			{
				return;
			}
			if (!this.driver.Initialized)
			{
				try
				{
					base.Write(buffer, index, count);
				}
				catch (IOException)
				{
				}
				return;
			}
			lock (this)
			{
				int num = index + count;
				int num2 = index;
				int num3 = 0;
				do
				{
					char c = buffer[num2++];
					if (this.driver.IsSpecialKey(c))
					{
						if (num3 > 0)
						{
							try
							{
								base.Write(buffer, index, num3);
							}
							catch (IOException)
							{
							}
							num3 = 0;
						}
						this.driver.WriteSpecialKey(c);
						index = num2;
					}
					else
					{
						num3++;
					}
				}
				while (num2 < num);
				if (num3 > 0)
				{
					try
					{
						base.Write(buffer, index, num3);
					}
					catch (IOException)
					{
					}
				}
			}
		}

		// Token: 0x06002B62 RID: 11106 RVA: 0x00099064 File Offset: 0x00097264
		public override void Write(char val)
		{
			lock (this)
			{
				try
				{
					if (this.driver.IsSpecialKey(val))
					{
						this.driver.WriteSpecialKey(val);
					}
					else
					{
						this.InternalWriteChar(val);
					}
				}
				catch (IOException)
				{
				}
			}
		}

		// Token: 0x06002B63 RID: 11107 RVA: 0x000990CC File Offset: 0x000972CC
		public void InternalWriteString(string val)
		{
			try
			{
				base.Write(val);
			}
			catch (IOException)
			{
			}
		}

		// Token: 0x06002B64 RID: 11108 RVA: 0x000990F8 File Offset: 0x000972F8
		public void InternalWriteChar(char val)
		{
			try
			{
				base.Write(val);
			}
			catch (IOException)
			{
			}
		}

		// Token: 0x06002B65 RID: 11109 RVA: 0x00099124 File Offset: 0x00097324
		public void InternalWriteChars(char[] buffer, int n)
		{
			try
			{
				base.Write(buffer, 0, n);
			}
			catch (IOException)
			{
			}
		}

		// Token: 0x06002B66 RID: 11110 RVA: 0x00099150 File Offset: 0x00097350
		public override void Write(char[] val)
		{
			this.Write(val, 0, val.Length);
		}

		// Token: 0x06002B67 RID: 11111 RVA: 0x00099160 File Offset: 0x00097360
		public override void Write(string val)
		{
			if (val == null)
			{
				return;
			}
			if (this.driver.Initialized)
			{
				this.Write(val.ToCharArray());
				return;
			}
			try
			{
				base.Write(val);
			}
			catch (IOException)
			{
			}
		}

		// Token: 0x040016AD RID: 5805
		private TermInfoDriver driver;
	}
}
