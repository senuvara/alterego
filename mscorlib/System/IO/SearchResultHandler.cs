﻿using System;
using System.Security;

namespace System.IO
{
	// Token: 0x02000361 RID: 865
	internal abstract class SearchResultHandler<TSource>
	{
		// Token: 0x06002743 RID: 10051
		[SecurityCritical]
		internal abstract bool IsResultIncluded(SearchResult result);

		// Token: 0x06002744 RID: 10052
		[SecurityCritical]
		internal abstract TSource CreateObject(SearchResult result);

		// Token: 0x06002745 RID: 10053 RVA: 0x00002050 File Offset: 0x00000250
		protected SearchResultHandler()
		{
		}
	}
}
