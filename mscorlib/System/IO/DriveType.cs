﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	/// <summary>Defines constants for drive types, including CDRom, Fixed, Network, NoRootDirectory, Ram, Removable, and Unknown.</summary>
	// Token: 0x02000398 RID: 920
	[ComVisible(true)]
	[Serializable]
	public enum DriveType
	{
		/// <summary>The drive is an optical disc device, such as a CD or DVD-ROM.</summary>
		// Token: 0x04001613 RID: 5651
		CDRom = 5,
		/// <summary>The drive is a fixed disk.</summary>
		// Token: 0x04001614 RID: 5652
		Fixed = 3,
		/// <summary>The drive is a network drive.</summary>
		// Token: 0x04001615 RID: 5653
		Network,
		/// <summary>The drive does not have a root directory.</summary>
		// Token: 0x04001616 RID: 5654
		NoRootDirectory = 1,
		/// <summary>The drive is a RAM disk.</summary>
		// Token: 0x04001617 RID: 5655
		Ram = 6,
		/// <summary>The drive is a removable storage device, such as a floppy disk drive or a USB flash drive.</summary>
		// Token: 0x04001618 RID: 5656
		Removable = 2,
		/// <summary>The type of drive is unknown.</summary>
		// Token: 0x04001619 RID: 5657
		Unknown = 0
	}
}
