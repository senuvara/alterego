﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace System.IO
{
	/// <summary>Implements a <see cref="T:System.IO.TextReader" /> that reads characters from a byte stream in a particular encoding.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x02000376 RID: 886
	[ComVisible(true)]
	[Serializable]
	public class StreamReader : TextReader
	{
		// Token: 0x1700062A RID: 1578
		// (get) Token: 0x0600282F RID: 10287 RVA: 0x0008C9BA File Offset: 0x0008ABBA
		internal static int DefaultBufferSize
		{
			get
			{
				return 1024;
			}
		}

		// Token: 0x06002830 RID: 10288 RVA: 0x0008C9C4 File Offset: 0x0008ABC4
		private void CheckAsyncTaskInProgress()
		{
			Task asyncReadTask = this._asyncReadTask;
			if (asyncReadTask != null && !asyncReadTask.IsCompleted)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The stream is currently in use by a previous operation on the stream."));
			}
		}

		// Token: 0x06002831 RID: 10289 RVA: 0x0008C9F5 File Offset: 0x0008ABF5
		internal StreamReader()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified stream.</summary>
		/// <param name="stream">The stream to be read. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> does not support reading. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />. </exception>
		// Token: 0x06002832 RID: 10290 RVA: 0x0008C9FD File Offset: 0x0008ABFD
		public StreamReader(Stream stream) : this(stream, true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified stream, with the specified byte order mark detection option.</summary>
		/// <param name="stream">The stream to be read. </param>
		/// <param name="detectEncodingFromByteOrderMarks">Indicates whether to look for byte order marks at the beginning of the file. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> does not support reading. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />. </exception>
		// Token: 0x06002833 RID: 10291 RVA: 0x0008CA07 File Offset: 0x0008AC07
		public StreamReader(Stream stream, bool detectEncodingFromByteOrderMarks) : this(stream, Encoding.UTF8, detectEncodingFromByteOrderMarks, StreamReader.DefaultBufferSize, false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified stream, with the specified character encoding.</summary>
		/// <param name="stream">The stream to be read. </param>
		/// <param name="encoding">The character encoding to use. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> does not support reading. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> or <paramref name="encoding" /> is <see langword="null" />. </exception>
		// Token: 0x06002834 RID: 10292 RVA: 0x0008CA1C File Offset: 0x0008AC1C
		public StreamReader(Stream stream, Encoding encoding) : this(stream, encoding, true, StreamReader.DefaultBufferSize, false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified stream, with the specified character encoding and byte order mark detection option.</summary>
		/// <param name="stream">The stream to be read. </param>
		/// <param name="encoding">The character encoding to use. </param>
		/// <param name="detectEncodingFromByteOrderMarks">Indicates whether to look for byte order marks at the beginning of the file. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> does not support reading. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> or <paramref name="encoding" /> is <see langword="null" />. </exception>
		// Token: 0x06002835 RID: 10293 RVA: 0x0008CA2D File Offset: 0x0008AC2D
		public StreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks) : this(stream, encoding, detectEncodingFromByteOrderMarks, StreamReader.DefaultBufferSize, false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified stream, with the specified character encoding, byte order mark detection option, and buffer size.</summary>
		/// <param name="stream">The stream to be read. </param>
		/// <param name="encoding">The character encoding to use. </param>
		/// <param name="detectEncodingFromByteOrderMarks">Indicates whether to look for byte order marks at the beginning of the file. </param>
		/// <param name="bufferSize">The minimum buffer size. </param>
		/// <exception cref="T:System.ArgumentException">The stream does not support reading. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> or <paramref name="encoding" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="bufferSize" /> is less than or equal to zero. </exception>
		// Token: 0x06002836 RID: 10294 RVA: 0x0008CA3E File Offset: 0x0008AC3E
		public StreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize) : this(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize, false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified stream based on the specified character encoding, byte order mark detection option, and buffer size, and optionally leaves the stream open.</summary>
		/// <param name="stream">The stream to read.</param>
		/// <param name="encoding">The character encoding to use.</param>
		/// <param name="detectEncodingFromByteOrderMarks">
		///       <see langword="true" /> to look for byte order marks at the beginning of the file; otherwise, <see langword="false" />.</param>
		/// <param name="bufferSize">The minimum buffer size.</param>
		/// <param name="leaveOpen">
		///       <see langword="true" /> to leave the stream open after the <see cref="T:System.IO.StreamReader" /> object is disposed; otherwise, <see langword="false" />.</param>
		// Token: 0x06002837 RID: 10295 RVA: 0x0008CA4C File Offset: 0x0008AC4C
		public StreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize, bool leaveOpen)
		{
			if (stream == null || encoding == null)
			{
				throw new ArgumentNullException((stream == null) ? "stream" : "encoding");
			}
			if (!stream.CanRead)
			{
				throw new ArgumentException(Environment.GetResourceString("Stream was not readable."));
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize", Environment.GetResourceString("Positive number required."));
			}
			this.Init(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize, leaveOpen);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified file name.</summary>
		/// <param name="path">The complete file path to be read. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file cannot be found. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="path" /> includes an incorrect or invalid syntax for file name, directory name, or volume label. </exception>
		// Token: 0x06002838 RID: 10296 RVA: 0x0008CAB9 File Offset: 0x0008ACB9
		public StreamReader(string path) : this(path, true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified file name, with the specified byte order mark detection option.</summary>
		/// <param name="path">The complete file path to be read. </param>
		/// <param name="detectEncodingFromByteOrderMarks">Indicates whether to look for byte order marks at the beginning of the file. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file cannot be found. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.IO.IOException">
		///         <paramref name="path" /> includes an incorrect or invalid syntax for file name, directory name, or volume label. </exception>
		// Token: 0x06002839 RID: 10297 RVA: 0x0008CAC3 File Offset: 0x0008ACC3
		public StreamReader(string path, bool detectEncodingFromByteOrderMarks) : this(path, Encoding.UTF8, detectEncodingFromByteOrderMarks, StreamReader.DefaultBufferSize)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified file name, with the specified character encoding.</summary>
		/// <param name="path">The complete file path to be read. </param>
		/// <param name="encoding">The character encoding to use. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="encoding" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file cannot be found. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <paramref name="path" /> includes an incorrect or invalid syntax for file name, directory name, or volume label. </exception>
		// Token: 0x0600283A RID: 10298 RVA: 0x0008CAD7 File Offset: 0x0008ACD7
		public StreamReader(string path, Encoding encoding) : this(path, encoding, true, StreamReader.DefaultBufferSize)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified file name, with the specified character encoding and byte order mark detection option.</summary>
		/// <param name="path">The complete file path to be read. </param>
		/// <param name="encoding">The character encoding to use. </param>
		/// <param name="detectEncodingFromByteOrderMarks">Indicates whether to look for byte order marks at the beginning of the file. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="encoding" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file cannot be found. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <paramref name="path" /> includes an incorrect or invalid syntax for file name, directory name, or volume label. </exception>
		// Token: 0x0600283B RID: 10299 RVA: 0x0008CAE7 File Offset: 0x0008ACE7
		public StreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks) : this(path, encoding, detectEncodingFromByteOrderMarks, StreamReader.DefaultBufferSize)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.StreamReader" /> class for the specified file name, with the specified character encoding, byte order mark detection option, and buffer size.</summary>
		/// <param name="path">The complete file path to be read. </param>
		/// <param name="encoding">The character encoding to use. </param>
		/// <param name="detectEncodingFromByteOrderMarks">Indicates whether to look for byte order marks at the beginning of the file. </param>
		/// <param name="bufferSize">The minimum buffer size, in number of 16-bit characters. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is an empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> or <paramref name="encoding" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file cannot be found. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <paramref name="path" /> includes an incorrect or invalid syntax for file name, directory name, or volume label. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="buffersize" /> is less than or equal to zero. </exception>
		// Token: 0x0600283C RID: 10300 RVA: 0x0008CAF7 File Offset: 0x0008ACF7
		[SecuritySafeCritical]
		public StreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize) : this(path, encoding, detectEncodingFromByteOrderMarks, bufferSize, true)
		{
		}

		// Token: 0x0600283D RID: 10301 RVA: 0x0008CB08 File Offset: 0x0008AD08
		[SecurityCritical]
		internal StreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize, bool checkHost)
		{
			if (path == null || encoding == null)
			{
				throw new ArgumentNullException((path == null) ? "path" : "encoding");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException(Environment.GetResourceString("Empty path name is not legal."));
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize", Environment.GetResourceString("Positive number required."));
			}
			Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan, Path.GetFileName(path), false, false, checkHost);
			this.Init(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize, false);
		}

		// Token: 0x0600283E RID: 10302 RVA: 0x0008CB94 File Offset: 0x0008AD94
		private void Init(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize, bool leaveOpen)
		{
			this.stream = stream;
			this.encoding = encoding;
			this.decoder = encoding.GetDecoder();
			if (bufferSize < 128)
			{
				bufferSize = 128;
			}
			this.byteBuffer = new byte[bufferSize];
			this._maxCharsPerBuffer = encoding.GetMaxCharCount(bufferSize);
			this.charBuffer = new char[this._maxCharsPerBuffer];
			this.byteLen = 0;
			this.bytePos = 0;
			this._detectEncoding = detectEncodingFromByteOrderMarks;
			this._preamble = encoding.GetPreamble();
			this._checkPreamble = (this._preamble.Length != 0);
			this._isBlocked = false;
			this._closable = !leaveOpen;
		}

		// Token: 0x0600283F RID: 10303 RVA: 0x0008CC3A File Offset: 0x0008AE3A
		internal void Init(Stream stream)
		{
			this.stream = stream;
			this._closable = true;
		}

		/// <summary>Closes the <see cref="T:System.IO.StreamReader" /> object and the underlying stream, and releases any system resources associated with the reader.</summary>
		// Token: 0x06002840 RID: 10304 RVA: 0x0008CC4A File Offset: 0x0008AE4A
		public override void Close()
		{
			this.Dispose(true);
		}

		/// <summary>Closes the underlying stream, releases the unmanaged resources used by the <see cref="T:System.IO.StreamReader" />, and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06002841 RID: 10305 RVA: 0x0008CC54 File Offset: 0x0008AE54
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (!this.LeaveOpen && disposing && this.stream != null)
				{
					this.stream.Close();
				}
			}
			finally
			{
				if (!this.LeaveOpen && this.stream != null)
				{
					this.stream = null;
					this.encoding = null;
					this.decoder = null;
					this.byteBuffer = null;
					this.charBuffer = null;
					this.charPos = 0;
					this.charLen = 0;
					base.Dispose(disposing);
				}
			}
		}

		/// <summary>Gets the current character encoding that the current <see cref="T:System.IO.StreamReader" /> object is using.</summary>
		/// <returns>The current character encoding used by the current reader. The value can be different after the first call to any <see cref="Overload:System.IO.StreamReader.Read" /> method of <see cref="T:System.IO.StreamReader" />, since encoding autodetection is not done until the first call to a <see cref="Overload:System.IO.StreamReader.Read" /> method.</returns>
		// Token: 0x1700062B RID: 1579
		// (get) Token: 0x06002842 RID: 10306 RVA: 0x0008CCDC File Offset: 0x0008AEDC
		public virtual Encoding CurrentEncoding
		{
			get
			{
				return this.encoding;
			}
		}

		/// <summary>Returns the underlying stream.</summary>
		/// <returns>The underlying stream.</returns>
		// Token: 0x1700062C RID: 1580
		// (get) Token: 0x06002843 RID: 10307 RVA: 0x0008CCE4 File Offset: 0x0008AEE4
		public virtual Stream BaseStream
		{
			get
			{
				return this.stream;
			}
		}

		// Token: 0x1700062D RID: 1581
		// (get) Token: 0x06002844 RID: 10308 RVA: 0x0008CCEC File Offset: 0x0008AEEC
		internal bool LeaveOpen
		{
			get
			{
				return !this._closable;
			}
		}

		/// <summary>Clears the internal buffer.</summary>
		// Token: 0x06002845 RID: 10309 RVA: 0x0008CCF7 File Offset: 0x0008AEF7
		public void DiscardBufferedData()
		{
			this.CheckAsyncTaskInProgress();
			this.byteLen = 0;
			this.charLen = 0;
			this.charPos = 0;
			if (this.encoding != null)
			{
				this.decoder = this.encoding.GetDecoder();
			}
			this._isBlocked = false;
		}

		/// <summary>Gets a value that indicates whether the current stream position is at the end of the stream.</summary>
		/// <returns>
		///     <see langword="true" /> if the current stream position is at the end of the stream; otherwise <see langword="false" />.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The underlying stream has been disposed.</exception>
		// Token: 0x1700062E RID: 1582
		// (get) Token: 0x06002846 RID: 10310 RVA: 0x0008CD34 File Offset: 0x0008AF34
		public bool EndOfStream
		{
			get
			{
				if (this.stream == null)
				{
					__Error.ReaderClosed();
				}
				this.CheckAsyncTaskInProgress();
				return this.charPos >= this.charLen && this.ReadBuffer() == 0;
			}
		}

		/// <summary>Returns the next available character but does not consume it.</summary>
		/// <returns>An integer representing the next character to be read, or -1 if there are no characters to be read or if the stream does not support seeking.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		// Token: 0x06002847 RID: 10311 RVA: 0x0008CD64 File Offset: 0x0008AF64
		public override int Peek()
		{
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			if (this.charPos == this.charLen && (this._isBlocked || this.ReadBuffer() == 0))
			{
				return -1;
			}
			return (int)this.charBuffer[this.charPos];
		}

		// Token: 0x06002848 RID: 10312 RVA: 0x0008CDB1 File Offset: 0x0008AFB1
		internal bool DataAvailable()
		{
			return this.charPos < this.charLen;
		}

		/// <summary>Reads the next character from the input stream and advances the character position by one character.</summary>
		/// <returns>The next character from the input stream represented as an <see cref="T:System.Int32" /> object, or -1 if no more characters are available.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		// Token: 0x06002849 RID: 10313 RVA: 0x0008CDC4 File Offset: 0x0008AFC4
		public override int Read()
		{
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			if (this.charPos == this.charLen && this.ReadBuffer() == 0)
			{
				return -1;
			}
			int result = (int)this.charBuffer[this.charPos];
			this.charPos++;
			return result;
		}

		/// <summary>Reads a specified maximum of characters from the current stream into a buffer, beginning at the specified index.</summary>
		/// <param name="buffer">When this method returns, contains the specified character array with the values between <paramref name="index" /> and (<paramref name="index + count - 1" />) replaced by the characters read from the current source. </param>
		/// <param name="index">The index of <paramref name="buffer" /> at which to begin writing. </param>
		/// <param name="count">The maximum number of characters to read. </param>
		/// <returns>The number of characters that have been read, or 0 if at the end of the stream and no data was read. The number will be less than or equal to the <paramref name="count" /> parameter, depending on whether the data is available within the stream.</returns>
		/// <exception cref="T:System.ArgumentException">The buffer length minus <paramref name="index" /> is less than <paramref name="count" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> or <paramref name="count" /> is negative. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs, such as the stream is closed. </exception>
		// Token: 0x0600284A RID: 10314 RVA: 0x0008CE18 File Offset: 0x0008B018
		public override int Read([In] [Out] char[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (index < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			int num = 0;
			bool flag = false;
			while (count > 0)
			{
				int num2 = this.charLen - this.charPos;
				if (num2 == 0)
				{
					num2 = this.ReadBuffer(buffer, index + num, count, out flag);
				}
				if (num2 == 0)
				{
					break;
				}
				if (num2 > count)
				{
					num2 = count;
				}
				if (!flag)
				{
					Buffer.InternalBlockCopy(this.charBuffer, this.charPos * 2, buffer, (index + num) * 2, num2 * 2);
					this.charPos += num2;
				}
				num += num2;
				count -= num2;
				if (this._isBlocked)
				{
					break;
				}
			}
			return num;
		}

		/// <summary>Reads all characters from the current position to the end of the stream.</summary>
		/// <returns>The rest of the stream as a string, from the current position to the end. If the current position is at the end of the stream, returns an empty string ("").</returns>
		/// <exception cref="T:System.OutOfMemoryException">There is insufficient memory to allocate a buffer for the returned string. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		// Token: 0x0600284B RID: 10315 RVA: 0x0008CF04 File Offset: 0x0008B104
		public override string ReadToEnd()
		{
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			StringBuilder stringBuilder = new StringBuilder(this.charLen - this.charPos);
			do
			{
				stringBuilder.Append(this.charBuffer, this.charPos, this.charLen - this.charPos);
				this.charPos = this.charLen;
				this.ReadBuffer();
			}
			while (this.charLen > 0);
			return stringBuilder.ToString();
		}

		/// <summary>Reads a specified maximum number of characters from the current stream and writes the data to a buffer, beginning at the specified index.</summary>
		/// <param name="buffer">When this method returns, contains the specified character array with the values between <paramref name="index" /> and (<paramref name="index + count - 1" />) replaced by the characters read from the current source.</param>
		/// <param name="index">The position in <paramref name="buffer" /> at which to begin writing.</param>
		/// <param name="count">The maximum number of characters to read.</param>
		/// <returns>The number of characters that have been read. The number will be less than or equal to <paramref name="count" />, depending on whether all input characters have been read.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The buffer length minus <paramref name="index" /> is less than <paramref name="count" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> or <paramref name="count" /> is negative. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.StreamReader" /> is closed. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred. </exception>
		// Token: 0x0600284C RID: 10316 RVA: 0x0008CF7C File Offset: 0x0008B17C
		public override int ReadBlock([In] [Out] char[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (index < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			return base.ReadBlock(buffer, index, count);
		}

		// Token: 0x0600284D RID: 10317 RVA: 0x0008CFFD File Offset: 0x0008B1FD
		private void CompressBuffer(int n)
		{
			Buffer.InternalBlockCopy(this.byteBuffer, n, this.byteBuffer, 0, this.byteLen - n);
			this.byteLen -= n;
		}

		// Token: 0x0600284E RID: 10318 RVA: 0x0008D02C File Offset: 0x0008B22C
		private void DetectEncoding()
		{
			if (this.byteLen < 2)
			{
				return;
			}
			this._detectEncoding = false;
			bool flag = false;
			if (this.byteBuffer[0] == 254 && this.byteBuffer[1] == 255)
			{
				this.encoding = new UnicodeEncoding(true, true);
				this.CompressBuffer(2);
				flag = true;
			}
			else if (this.byteBuffer[0] == 255 && this.byteBuffer[1] == 254)
			{
				if (this.byteLen < 4 || this.byteBuffer[2] != 0 || this.byteBuffer[3] != 0)
				{
					this.encoding = new UnicodeEncoding(false, true);
					this.CompressBuffer(2);
					flag = true;
				}
				else
				{
					this.encoding = new UTF32Encoding(false, true);
					this.CompressBuffer(4);
					flag = true;
				}
			}
			else if (this.byteLen >= 3 && this.byteBuffer[0] == 239 && this.byteBuffer[1] == 187 && this.byteBuffer[2] == 191)
			{
				this.encoding = Encoding.UTF8;
				this.CompressBuffer(3);
				flag = true;
			}
			else if (this.byteLen >= 4 && this.byteBuffer[0] == 0 && this.byteBuffer[1] == 0 && this.byteBuffer[2] == 254 && this.byteBuffer[3] == 255)
			{
				this.encoding = new UTF32Encoding(true, true);
				this.CompressBuffer(4);
				flag = true;
			}
			else if (this.byteLen == 2)
			{
				this._detectEncoding = true;
			}
			if (flag)
			{
				this.decoder = this.encoding.GetDecoder();
				this._maxCharsPerBuffer = this.encoding.GetMaxCharCount(this.byteBuffer.Length);
				this.charBuffer = new char[this._maxCharsPerBuffer];
			}
		}

		// Token: 0x0600284F RID: 10319 RVA: 0x0008D1E4 File Offset: 0x0008B3E4
		private bool IsPreamble()
		{
			if (!this._checkPreamble)
			{
				return this._checkPreamble;
			}
			int num = (this.byteLen >= this._preamble.Length) ? (this._preamble.Length - this.bytePos) : (this.byteLen - this.bytePos);
			int i = 0;
			while (i < num)
			{
				if (this.byteBuffer[this.bytePos] != this._preamble[this.bytePos])
				{
					this.bytePos = 0;
					this._checkPreamble = false;
					break;
				}
				i++;
				this.bytePos++;
			}
			if (this._checkPreamble && this.bytePos == this._preamble.Length)
			{
				this.CompressBuffer(this._preamble.Length);
				this.bytePos = 0;
				this._checkPreamble = false;
				this._detectEncoding = false;
			}
			return this._checkPreamble;
		}

		// Token: 0x06002850 RID: 10320 RVA: 0x0008D2B8 File Offset: 0x0008B4B8
		internal virtual int ReadBuffer()
		{
			this.charLen = 0;
			this.charPos = 0;
			if (!this._checkPreamble)
			{
				this.byteLen = 0;
			}
			for (;;)
			{
				if (this._checkPreamble)
				{
					int num = this.stream.Read(this.byteBuffer, this.bytePos, this.byteBuffer.Length - this.bytePos);
					if (num == 0)
					{
						break;
					}
					this.byteLen += num;
				}
				else
				{
					this.byteLen = this.stream.Read(this.byteBuffer, 0, this.byteBuffer.Length);
					if (this.byteLen == 0)
					{
						goto Block_5;
					}
				}
				this._isBlocked = (this.byteLen < this.byteBuffer.Length);
				if (!this.IsPreamble())
				{
					if (this._detectEncoding && this.byteLen >= 2)
					{
						this.DetectEncoding();
					}
					this.charLen += this.decoder.GetChars(this.byteBuffer, 0, this.byteLen, this.charBuffer, this.charLen);
				}
				if (this.charLen != 0)
				{
					goto Block_9;
				}
			}
			if (this.byteLen > 0)
			{
				this.charLen += this.decoder.GetChars(this.byteBuffer, 0, this.byteLen, this.charBuffer, this.charLen);
				this.bytePos = (this.byteLen = 0);
			}
			return this.charLen;
			Block_5:
			return this.charLen;
			Block_9:
			return this.charLen;
		}

		// Token: 0x06002851 RID: 10321 RVA: 0x0008D420 File Offset: 0x0008B620
		private int ReadBuffer(char[] userBuffer, int userOffset, int desiredChars, out bool readToUserBuffer)
		{
			this.charLen = 0;
			this.charPos = 0;
			if (!this._checkPreamble)
			{
				this.byteLen = 0;
			}
			int num = 0;
			readToUserBuffer = (desiredChars >= this._maxCharsPerBuffer);
			for (;;)
			{
				if (this._checkPreamble)
				{
					int num2 = this.stream.Read(this.byteBuffer, this.bytePos, this.byteBuffer.Length - this.bytePos);
					if (num2 == 0)
					{
						break;
					}
					this.byteLen += num2;
				}
				else
				{
					this.byteLen = this.stream.Read(this.byteBuffer, 0, this.byteBuffer.Length);
					if (this.byteLen == 0)
					{
						goto IL_1B1;
					}
				}
				this._isBlocked = (this.byteLen < this.byteBuffer.Length);
				if (!this.IsPreamble())
				{
					if (this._detectEncoding && this.byteLen >= 2)
					{
						this.DetectEncoding();
						readToUserBuffer = (desiredChars >= this._maxCharsPerBuffer);
					}
					this.charPos = 0;
					if (readToUserBuffer)
					{
						num += this.decoder.GetChars(this.byteBuffer, 0, this.byteLen, userBuffer, userOffset + num);
						this.charLen = 0;
					}
					else
					{
						num = this.decoder.GetChars(this.byteBuffer, 0, this.byteLen, this.charBuffer, num);
						this.charLen += num;
					}
				}
				if (num != 0)
				{
					goto IL_1B1;
				}
			}
			if (this.byteLen > 0)
			{
				if (readToUserBuffer)
				{
					num = this.decoder.GetChars(this.byteBuffer, 0, this.byteLen, userBuffer, userOffset + num);
					this.charLen = 0;
				}
				else
				{
					num = this.decoder.GetChars(this.byteBuffer, 0, this.byteLen, this.charBuffer, num);
					this.charLen += num;
				}
			}
			return num;
			IL_1B1:
			this._isBlocked &= (num < desiredChars);
			return num;
		}

		/// <summary>Reads a line of characters from the current stream and returns the data as a string.</summary>
		/// <returns>The next line from the input stream, or <see langword="null" /> if the end of the input stream is reached.</returns>
		/// <exception cref="T:System.OutOfMemoryException">There is insufficient memory to allocate a buffer for the returned string. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		// Token: 0x06002852 RID: 10322 RVA: 0x0008D5F0 File Offset: 0x0008B7F0
		public override string ReadLine()
		{
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			if (this.charPos == this.charLen && this.ReadBuffer() == 0)
			{
				return null;
			}
			StringBuilder stringBuilder = null;
			int num;
			char c;
			for (;;)
			{
				num = this.charPos;
				do
				{
					c = this.charBuffer[num];
					if (c == '\r' || c == '\n')
					{
						goto IL_4A;
					}
					num++;
				}
				while (num < this.charLen);
				num = this.charLen - this.charPos;
				if (stringBuilder == null)
				{
					stringBuilder = new StringBuilder(num + 80);
				}
				stringBuilder.Append(this.charBuffer, this.charPos, num);
				if (this.ReadBuffer() <= 0)
				{
					goto Block_11;
				}
			}
			IL_4A:
			string result;
			if (stringBuilder != null)
			{
				stringBuilder.Append(this.charBuffer, this.charPos, num - this.charPos);
				result = stringBuilder.ToString();
			}
			else
			{
				result = new string(this.charBuffer, this.charPos, num - this.charPos);
			}
			this.charPos = num + 1;
			if (c == '\r' && (this.charPos < this.charLen || this.ReadBuffer() > 0) && this.charBuffer[this.charPos] == '\n')
			{
				this.charPos++;
			}
			return result;
			Block_11:
			return stringBuilder.ToString();
		}

		/// <summary>Reads a line of characters asynchronously from the current stream and returns the data as a string.</summary>
		/// <returns>A task that represents the asynchronous read operation. The value of the <paramref name="TResult" /> parameter contains the next line from the stream, or is <see langword="null" /> if all the characters have been read.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The number of characters in the next line is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The reader is currently in use by a previous read operation. </exception>
		// Token: 0x06002853 RID: 10323 RVA: 0x0008D720 File Offset: 0x0008B920
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public override Task<string> ReadLineAsync()
		{
			if (base.GetType() != typeof(StreamReader))
			{
				return base.ReadLineAsync();
			}
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			Task<string> task = this.ReadLineAsyncInternal();
			this._asyncReadTask = task;
			return task;
		}

		// Token: 0x06002854 RID: 10324 RVA: 0x0008D770 File Offset: 0x0008B970
		private async Task<string> ReadLineAsyncInternal()
		{
			bool flag = this.CharPos_Prop == this.CharLen_Prop;
			ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
			if (flag)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				flag = (configuredTaskAwaiter.GetResult() == 0);
			}
			string result;
			if (flag)
			{
				result = null;
			}
			else
			{
				StringBuilder sb = null;
				char[] tmpCharBuffer;
				int tmpCharLen;
				int tmpCharPos;
				int i;
				char c;
				for (;;)
				{
					tmpCharBuffer = this.CharBuffer_Prop;
					tmpCharLen = this.CharLen_Prop;
					tmpCharPos = this.CharPos_Prop;
					i = tmpCharPos;
					do
					{
						c = tmpCharBuffer[i];
						if (c == '\r' || c == '\n')
						{
							goto IL_FD;
						}
						i++;
					}
					while (i < tmpCharLen);
					i = tmpCharLen - tmpCharPos;
					if (sb == null)
					{
						sb = new StringBuilder(i + 80);
					}
					sb.Append(tmpCharBuffer, tmpCharPos, i);
					tmpCharBuffer = null;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						await configuredTaskAwaiter;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
					}
					if (configuredTaskAwaiter.GetResult() <= 0)
					{
						goto Block_14;
					}
				}
				IL_FD:
				string s;
				if (sb != null)
				{
					sb.Append(tmpCharBuffer, tmpCharPos, i - tmpCharPos);
					s = sb.ToString();
				}
				else
				{
					s = new string(tmpCharBuffer, tmpCharPos, i - tmpCharPos);
				}
				tmpCharPos = (this.CharPos_Prop = i + 1);
				flag = (c == '\r');
				if (flag)
				{
					bool flag2 = tmpCharPos < tmpCharLen;
					if (!flag2)
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							await configuredTaskAwaiter;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						}
						flag2 = (configuredTaskAwaiter.GetResult() > 0);
					}
					flag = flag2;
				}
				if (flag)
				{
					tmpCharPos = this.CharPos_Prop;
					if (this.CharBuffer_Prop[tmpCharPos] == '\n')
					{
						tmpCharPos = (this.CharPos_Prop = tmpCharPos + 1);
					}
				}
				return s;
				Block_14:
				result = sb.ToString();
			}
			return result;
		}

		/// <summary>Reads all characters from the current position to the end of the stream asynchronously and returns them as one string.</summary>
		/// <returns>A task that represents the asynchronous read operation. The value of the <paramref name="TResult" /> parameter contains a string with the characters from the current position to the end of the stream.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The number of characters is larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The reader is currently in use by a previous read operation. </exception>
		// Token: 0x06002855 RID: 10325 RVA: 0x0008D7B8 File Offset: 0x0008B9B8
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public override Task<string> ReadToEndAsync()
		{
			if (base.GetType() != typeof(StreamReader))
			{
				return base.ReadToEndAsync();
			}
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			Task<string> task = this.ReadToEndAsyncInternal();
			this._asyncReadTask = task;
			return task;
		}

		// Token: 0x06002856 RID: 10326 RVA: 0x0008D808 File Offset: 0x0008BA08
		private async Task<string> ReadToEndAsyncInternal()
		{
			StringBuilder sb = new StringBuilder(this.CharLen_Prop - this.CharPos_Prop);
			do
			{
				int charPos_Prop = this.CharPos_Prop;
				sb.Append(this.CharBuffer_Prop, charPos_Prop, this.CharLen_Prop - charPos_Prop);
				this.CharPos_Prop = this.CharLen_Prop;
				await this.ReadBufferAsync().ConfigureAwait(false);
			}
			while (this.CharLen_Prop > 0);
			return sb.ToString();
		}

		/// <summary>Reads a specified maximum number of characters from the current stream asynchronously and writes the data to a buffer, beginning at the specified index. </summary>
		/// <param name="buffer">When this method returns, contains the specified character array with the values between <paramref name="index" /> and (<paramref name="index" /> + <paramref name="count" /> - 1) replaced by the characters read from the current source.</param>
		/// <param name="index">The position in <paramref name="buffer" /> at which to begin writing.</param>
		/// <param name="count">The maximum number of characters to read. If the end of the stream is reached before the specified number of characters is written into the buffer, the current method returns.</param>
		/// <returns>A task that represents the asynchronous read operation. The value of the <paramref name="TResult" /> parameter contains the total number of characters read into the buffer. The result value can be less than the number of characters requested if the number of characters currently available is less than the requested number, or it can be 0 (zero) if the end of the stream has been reached.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> or <paramref name="count" /> is negative.</exception>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="index" /> and <paramref name="count" /> is larger than the buffer length.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The reader is currently in use by a previous read operation. </exception>
		// Token: 0x06002857 RID: 10327 RVA: 0x0008D850 File Offset: 0x0008BA50
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public override Task<int> ReadAsync(char[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (index < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (base.GetType() != typeof(StreamReader))
			{
				return base.ReadAsync(buffer, index, count);
			}
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			Task<int> task = this.ReadAsyncInternal(buffer, index, count);
			this._asyncReadTask = task;
			return task;
		}

		// Token: 0x06002858 RID: 10328 RVA: 0x0008D900 File Offset: 0x0008BB00
		internal override async Task<int> ReadAsyncInternal(char[] buffer, int index, int count)
		{
			bool flag = this.CharPos_Prop == this.CharLen_Prop;
			if (flag)
			{
				ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter = this.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
				if (!configuredTaskAwaiter.IsCompleted)
				{
					await configuredTaskAwaiter;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					configuredTaskAwaiter = configuredTaskAwaiter2;
					configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
				}
				flag = (configuredTaskAwaiter.GetResult() == 0);
			}
			int result;
			if (flag)
			{
				result = 0;
			}
			else
			{
				int charsRead = 0;
				bool readToUserBuffer = false;
				byte[] tmpByteBuffer = this.ByteBuffer_Prop;
				Stream tmpStream = this.Stream_Prop;
				while (count > 0)
				{
					int i = this.CharLen_Prop - this.CharPos_Prop;
					if (i == 0)
					{
						this.CharLen_Prop = 0;
						this.CharPos_Prop = 0;
						if (!this.CheckPreamble_Prop)
						{
							this.ByteLen_Prop = 0;
						}
						readToUserBuffer = (count >= this.MaxCharsPerBuffer_Prop);
						do
						{
							if (this.CheckPreamble_Prop)
							{
								int bytePos_Prop = this.BytePos_Prop;
								int num = await tmpStream.ReadAsync(tmpByteBuffer, bytePos_Prop, tmpByteBuffer.Length - bytePos_Prop).ConfigureAwait(false);
								if (num == 0)
								{
									goto Block_7;
								}
								this.ByteLen_Prop += num;
							}
							else
							{
								this.ByteLen_Prop = await tmpStream.ReadAsync(tmpByteBuffer, 0, tmpByteBuffer.Length).ConfigureAwait(false);
								if (this.ByteLen_Prop == 0)
								{
									goto Block_10;
								}
							}
							this.IsBlocked_Prop = (this.ByteLen_Prop < tmpByteBuffer.Length);
							if (!this.IsPreamble())
							{
								if (this.DetectEncoding_Prop && this.ByteLen_Prop >= 2)
								{
									this.DetectEncoding();
									readToUserBuffer = (count >= this.MaxCharsPerBuffer_Prop);
								}
								this.CharPos_Prop = 0;
								if (readToUserBuffer)
								{
									i += this.Decoder_Prop.GetChars(tmpByteBuffer, 0, this.ByteLen_Prop, buffer, index + charsRead);
									this.CharLen_Prop = 0;
								}
								else
								{
									i = this.Decoder_Prop.GetChars(tmpByteBuffer, 0, this.ByteLen_Prop, this.CharBuffer_Prop, 0);
									this.CharLen_Prop += i;
								}
							}
						}
						while (i == 0);
						IL_3E0:
						if (i != 0)
						{
							goto IL_3EB;
						}
						break;
						Block_10:
						this.IsBlocked_Prop = true;
						goto IL_3E0;
						Block_7:
						if (this.ByteLen_Prop > 0)
						{
							if (readToUserBuffer)
							{
								i = this.Decoder_Prop.GetChars(tmpByteBuffer, 0, this.ByteLen_Prop, buffer, index + charsRead);
								this.CharLen_Prop = 0;
							}
							else
							{
								i = this.Decoder_Prop.GetChars(tmpByteBuffer, 0, this.ByteLen_Prop, this.CharBuffer_Prop, 0);
								this.CharLen_Prop += i;
							}
						}
						this.IsBlocked_Prop = true;
						goto IL_3E0;
					}
					IL_3EB:
					if (i > count)
					{
						i = count;
					}
					if (!readToUserBuffer)
					{
						Buffer.InternalBlockCopy(this.CharBuffer_Prop, this.CharPos_Prop * 2, buffer, (index + charsRead) * 2, i * 2);
						this.CharPos_Prop += i;
					}
					charsRead += i;
					count -= i;
					if (this.IsBlocked_Prop)
					{
						break;
					}
				}
				result = charsRead;
			}
			return result;
		}

		/// <summary>Reads a specified maximum number of characters from the current stream asynchronously and writes the data to a buffer, beginning at the specified index.</summary>
		/// <param name="buffer">When this method returns, contains the specified character array with the values between <paramref name="index" /> and (<paramref name="index" /> + <paramref name="count" /> - 1) replaced by the characters read from the current source.</param>
		/// <param name="index">The position in <paramref name="buffer" /> at which to begin writing.</param>
		/// <param name="count">The maximum number of characters to read. If the end of the stream is reached before the specified number of characters is written into the buffer, the method returns.</param>
		/// <returns>A task that represents the asynchronous read operation. The value of the <paramref name="TResult" /> parameter contains the total number of characters read into the buffer. The result value can be less than the number of characters requested if the number of characters currently available is less than the requested number, or it can be 0 (zero) if the end of the stream has been reached.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> or <paramref name="count" /> is negative.</exception>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="index" /> and <paramref name="count" /> is larger than the buffer length.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The reader is currently in use by a previous read operation. </exception>
		// Token: 0x06002859 RID: 10329 RVA: 0x0008D960 File Offset: 0x0008BB60
		[ComVisible(false)]
		[HostProtection(SecurityAction.LinkDemand, ExternalThreading = true)]
		public override Task<int> ReadBlockAsync(char[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (index < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException((index < 0) ? "index" : "count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (base.GetType() != typeof(StreamReader))
			{
				return base.ReadBlockAsync(buffer, index, count);
			}
			if (this.stream == null)
			{
				__Error.ReaderClosed();
			}
			this.CheckAsyncTaskInProgress();
			Task<int> task = base.ReadBlockAsync(buffer, index, count);
			this._asyncReadTask = task;
			return task;
		}

		// Token: 0x1700062F RID: 1583
		// (get) Token: 0x0600285A RID: 10330 RVA: 0x0008DA0D File Offset: 0x0008BC0D
		// (set) Token: 0x0600285B RID: 10331 RVA: 0x0008DA15 File Offset: 0x0008BC15
		private int CharLen_Prop
		{
			get
			{
				return this.charLen;
			}
			set
			{
				this.charLen = value;
			}
		}

		// Token: 0x17000630 RID: 1584
		// (get) Token: 0x0600285C RID: 10332 RVA: 0x0008DA1E File Offset: 0x0008BC1E
		// (set) Token: 0x0600285D RID: 10333 RVA: 0x0008DA26 File Offset: 0x0008BC26
		private int CharPos_Prop
		{
			get
			{
				return this.charPos;
			}
			set
			{
				this.charPos = value;
			}
		}

		// Token: 0x17000631 RID: 1585
		// (get) Token: 0x0600285E RID: 10334 RVA: 0x0008DA2F File Offset: 0x0008BC2F
		// (set) Token: 0x0600285F RID: 10335 RVA: 0x0008DA37 File Offset: 0x0008BC37
		private int ByteLen_Prop
		{
			get
			{
				return this.byteLen;
			}
			set
			{
				this.byteLen = value;
			}
		}

		// Token: 0x17000632 RID: 1586
		// (get) Token: 0x06002860 RID: 10336 RVA: 0x0008DA40 File Offset: 0x0008BC40
		// (set) Token: 0x06002861 RID: 10337 RVA: 0x0008DA48 File Offset: 0x0008BC48
		private int BytePos_Prop
		{
			get
			{
				return this.bytePos;
			}
			set
			{
				this.bytePos = value;
			}
		}

		// Token: 0x17000633 RID: 1587
		// (get) Token: 0x06002862 RID: 10338 RVA: 0x0008DA51 File Offset: 0x0008BC51
		private byte[] Preamble_Prop
		{
			get
			{
				return this._preamble;
			}
		}

		// Token: 0x17000634 RID: 1588
		// (get) Token: 0x06002863 RID: 10339 RVA: 0x0008DA59 File Offset: 0x0008BC59
		private bool CheckPreamble_Prop
		{
			get
			{
				return this._checkPreamble;
			}
		}

		// Token: 0x17000635 RID: 1589
		// (get) Token: 0x06002864 RID: 10340 RVA: 0x0008DA61 File Offset: 0x0008BC61
		private Decoder Decoder_Prop
		{
			get
			{
				return this.decoder;
			}
		}

		// Token: 0x17000636 RID: 1590
		// (get) Token: 0x06002865 RID: 10341 RVA: 0x0008DA69 File Offset: 0x0008BC69
		private bool DetectEncoding_Prop
		{
			get
			{
				return this._detectEncoding;
			}
		}

		// Token: 0x17000637 RID: 1591
		// (get) Token: 0x06002866 RID: 10342 RVA: 0x0008DA71 File Offset: 0x0008BC71
		private char[] CharBuffer_Prop
		{
			get
			{
				return this.charBuffer;
			}
		}

		// Token: 0x17000638 RID: 1592
		// (get) Token: 0x06002867 RID: 10343 RVA: 0x0008DA79 File Offset: 0x0008BC79
		private byte[] ByteBuffer_Prop
		{
			get
			{
				return this.byteBuffer;
			}
		}

		// Token: 0x17000639 RID: 1593
		// (get) Token: 0x06002868 RID: 10344 RVA: 0x0008DA81 File Offset: 0x0008BC81
		// (set) Token: 0x06002869 RID: 10345 RVA: 0x0008DA89 File Offset: 0x0008BC89
		private bool IsBlocked_Prop
		{
			get
			{
				return this._isBlocked;
			}
			set
			{
				this._isBlocked = value;
			}
		}

		// Token: 0x1700063A RID: 1594
		// (get) Token: 0x0600286A RID: 10346 RVA: 0x0008CCE4 File Offset: 0x0008AEE4
		private Stream Stream_Prop
		{
			get
			{
				return this.stream;
			}
		}

		// Token: 0x1700063B RID: 1595
		// (get) Token: 0x0600286B RID: 10347 RVA: 0x0008DA92 File Offset: 0x0008BC92
		private int MaxCharsPerBuffer_Prop
		{
			get
			{
				return this._maxCharsPerBuffer;
			}
		}

		// Token: 0x0600286C RID: 10348 RVA: 0x0008DA9C File Offset: 0x0008BC9C
		private async Task<int> ReadBufferAsync()
		{
			this.CharLen_Prop = 0;
			this.CharPos_Prop = 0;
			byte[] tmpByteBuffer = this.ByteBuffer_Prop;
			Stream tmpStream = this.Stream_Prop;
			if (!this.CheckPreamble_Prop)
			{
				this.ByteLen_Prop = 0;
			}
			for (;;)
			{
				if (this.CheckPreamble_Prop)
				{
					int bytePos_Prop = this.BytePos_Prop;
					int num = await tmpStream.ReadAsync(tmpByteBuffer, bytePos_Prop, tmpByteBuffer.Length - bytePos_Prop).ConfigureAwait(false);
					if (num == 0)
					{
						break;
					}
					this.ByteLen_Prop += num;
				}
				else
				{
					this.ByteLen_Prop = await tmpStream.ReadAsync(tmpByteBuffer, 0, tmpByteBuffer.Length).ConfigureAwait(false);
					if (this.ByteLen_Prop == 0)
					{
						goto Block_5;
					}
				}
				this.IsBlocked_Prop = (this.ByteLen_Prop < tmpByteBuffer.Length);
				if (!this.IsPreamble())
				{
					if (this.DetectEncoding_Prop && this.ByteLen_Prop >= 2)
					{
						this.DetectEncoding();
					}
					this.CharLen_Prop += this.Decoder_Prop.GetChars(tmpByteBuffer, 0, this.ByteLen_Prop, this.CharBuffer_Prop, this.CharLen_Prop);
				}
				if (this.CharLen_Prop != 0)
				{
					goto Block_9;
				}
			}
			if (this.ByteLen_Prop > 0)
			{
				this.CharLen_Prop += this.Decoder_Prop.GetChars(tmpByteBuffer, 0, this.ByteLen_Prop, this.CharBuffer_Prop, this.CharLen_Prop);
				this.BytePos_Prop = 0;
				this.ByteLen_Prop = 0;
			}
			return this.CharLen_Prop;
			Block_5:
			return this.CharLen_Prop;
			Block_9:
			return this.CharLen_Prop;
		}

		// Token: 0x0600286D RID: 10349 RVA: 0x0008DAE1 File Offset: 0x0008BCE1
		// Note: this type is marked as 'beforefieldinit'.
		static StreamReader()
		{
		}

		/// <summary>A <see cref="T:System.IO.StreamReader" /> object around an empty stream.</summary>
		// Token: 0x0400153C RID: 5436
		public new static readonly StreamReader Null = new StreamReader.NullStreamReader();

		// Token: 0x0400153D RID: 5437
		private const int DefaultFileStreamBufferSize = 4096;

		// Token: 0x0400153E RID: 5438
		private const int MinBufferSize = 128;

		// Token: 0x0400153F RID: 5439
		private Stream stream;

		// Token: 0x04001540 RID: 5440
		private Encoding encoding;

		// Token: 0x04001541 RID: 5441
		private Decoder decoder;

		// Token: 0x04001542 RID: 5442
		private byte[] byteBuffer;

		// Token: 0x04001543 RID: 5443
		private char[] charBuffer;

		// Token: 0x04001544 RID: 5444
		private byte[] _preamble;

		// Token: 0x04001545 RID: 5445
		private int charPos;

		// Token: 0x04001546 RID: 5446
		private int charLen;

		// Token: 0x04001547 RID: 5447
		private int byteLen;

		// Token: 0x04001548 RID: 5448
		private int bytePos;

		// Token: 0x04001549 RID: 5449
		private int _maxCharsPerBuffer;

		// Token: 0x0400154A RID: 5450
		private bool _detectEncoding;

		// Token: 0x0400154B RID: 5451
		private bool _checkPreamble;

		// Token: 0x0400154C RID: 5452
		private bool _isBlocked;

		// Token: 0x0400154D RID: 5453
		private bool _closable;

		// Token: 0x0400154E RID: 5454
		[NonSerialized]
		private volatile Task _asyncReadTask;

		// Token: 0x02000377 RID: 887
		private class NullStreamReader : StreamReader
		{
			// Token: 0x0600286E RID: 10350 RVA: 0x0008DAED File Offset: 0x0008BCED
			internal NullStreamReader()
			{
				base.Init(Stream.Null);
			}

			// Token: 0x1700063C RID: 1596
			// (get) Token: 0x0600286F RID: 10351 RVA: 0x0008DB00 File Offset: 0x0008BD00
			public override Stream BaseStream
			{
				get
				{
					return Stream.Null;
				}
			}

			// Token: 0x1700063D RID: 1597
			// (get) Token: 0x06002870 RID: 10352 RVA: 0x0008DB07 File Offset: 0x0008BD07
			public override Encoding CurrentEncoding
			{
				get
				{
					return Encoding.Unicode;
				}
			}

			// Token: 0x06002871 RID: 10353 RVA: 0x000020D3 File Offset: 0x000002D3
			protected override void Dispose(bool disposing)
			{
			}

			// Token: 0x06002872 RID: 10354 RVA: 0x0008BF5F File Offset: 0x0008A15F
			public override int Peek()
			{
				return -1;
			}

			// Token: 0x06002873 RID: 10355 RVA: 0x0008BF5F File Offset: 0x0008A15F
			public override int Read()
			{
				return -1;
			}

			// Token: 0x06002874 RID: 10356 RVA: 0x00002526 File Offset: 0x00000726
			public override int Read(char[] buffer, int index, int count)
			{
				return 0;
			}

			// Token: 0x06002875 RID: 10357 RVA: 0x0000CEAE File Offset: 0x0000B0AE
			public override string ReadLine()
			{
				return null;
			}

			// Token: 0x06002876 RID: 10358 RVA: 0x00039F28 File Offset: 0x00038128
			public override string ReadToEnd()
			{
				return string.Empty;
			}

			// Token: 0x06002877 RID: 10359 RVA: 0x00002526 File Offset: 0x00000726
			internal override int ReadBuffer()
			{
				return 0;
			}
		}

		// Token: 0x02000378 RID: 888
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadLineAsyncInternal>d__61 : IAsyncStateMachine
		{
			// Token: 0x06002878 RID: 10360 RVA: 0x0008DB10 File Offset: 0x0008BD10
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				StreamReader streamReader = this;
				string result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					bool flag;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1FC;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_326;
					default:
						flag = (streamReader.CharPos_Prop == streamReader.CharLen_Prop);
						if (!flag)
						{
							goto IL_9E;
						}
						configuredTaskAwaiter3 = streamReader.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadLineAsyncInternal>d__61>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					flag = (configuredTaskAwaiter3.GetResult() == 0);
					IL_9E:
					if (flag)
					{
						result = null;
						goto IL_35A;
					}
					sb = null;
					IL_AF:
					tmpCharBuffer = streamReader.CharBuffer_Prop;
					tmpCharLen = streamReader.CharLen_Prop;
					tmpCharPos = streamReader.CharPos_Prop;
					i = tmpCharPos;
					char c;
					for (;;)
					{
						c = tmpCharBuffer[i];
						if (c == '\r' || c == '\n')
						{
							break;
						}
						int num3 = i;
						i = num3 + 1;
						if (i >= tmpCharLen)
						{
							goto Block_14;
						}
					}
					if (sb != null)
					{
						sb.Append(tmpCharBuffer, tmpCharPos, i - tmpCharPos);
						s = sb.ToString();
					}
					else
					{
						s = new string(tmpCharBuffer, tmpCharPos, i - tmpCharPos);
					}
					streamReader.CharPos_Prop = (tmpCharPos = i + 1);
					flag = (c == '\r');
					if (!flag)
					{
						goto IL_20B;
					}
					bool flag2 = tmpCharPos < tmpCharLen;
					if (flag2)
					{
						goto IL_208;
					}
					configuredTaskAwaiter3 = streamReader.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 1;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadLineAsyncInternal>d__61>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_1FC;
					Block_14:
					i = tmpCharLen - tmpCharPos;
					if (sb == null)
					{
						sb = new StringBuilder(i + 80);
					}
					sb.Append(tmpCharBuffer, tmpCharPos, i);
					tmpCharBuffer = null;
					configuredTaskAwaiter3 = streamReader.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter3.IsCompleted)
					{
						num2 = 2;
						configuredTaskAwaiter2 = configuredTaskAwaiter3;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadLineAsyncInternal>d__61>(ref configuredTaskAwaiter3, ref this);
						return;
					}
					goto IL_326;
					IL_1FC:
					flag2 = (configuredTaskAwaiter3.GetResult() > 0);
					IL_208:
					flag = flag2;
					IL_20B:
					if (flag)
					{
						tmpCharPos = streamReader.CharPos_Prop;
						if (streamReader.CharBuffer_Prop[tmpCharPos] == '\n')
						{
							StreamReader streamReader2 = streamReader;
							int num3 = tmpCharPos + 1;
							tmpCharPos = num3;
							streamReader2.CharPos_Prop = num3;
						}
					}
					result = s;
					goto IL_35A;
					IL_326:
					if (configuredTaskAwaiter3.GetResult() > 0)
					{
						goto IL_AF;
					}
					result = sb.ToString();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_35A:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x06002879 RID: 10361 RVA: 0x0008DEA8 File Offset: 0x0008C0A8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400154F RID: 5455
			public int <>1__state;

			// Token: 0x04001550 RID: 5456
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x04001551 RID: 5457
			public StreamReader <>4__this;

			// Token: 0x04001552 RID: 5458
			private char[] <tmpCharBuffer>5__1;

			// Token: 0x04001553 RID: 5459
			private StringBuilder <sb>5__2;

			// Token: 0x04001554 RID: 5460
			private int <tmpCharPos>5__3;

			// Token: 0x04001555 RID: 5461
			private int <tmpCharLen>5__4;

			// Token: 0x04001556 RID: 5462
			private string <s>5__5;

			// Token: 0x04001557 RID: 5463
			private int <i>5__6;

			// Token: 0x04001558 RID: 5464
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000379 RID: 889
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadToEndAsyncInternal>d__63 : IAsyncStateMachine
		{
			// Token: 0x0600287A RID: 10362 RVA: 0x0008DEB8 File Offset: 0x0008C0B8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				StreamReader streamReader = this;
				string result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_B8;
					}
					sb = new StringBuilder(streamReader.CharLen_Prop - streamReader.CharPos_Prop);
					IL_2C:
					int charPos_Prop = streamReader.CharPos_Prop;
					sb.Append(streamReader.CharBuffer_Prop, charPos_Prop, streamReader.CharLen_Prop - charPos_Prop);
					streamReader.CharPos_Prop = streamReader.CharLen_Prop;
					configuredTaskAwaiter = streamReader.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 0;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadToEndAsyncInternal>d__63>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_B8:
					configuredTaskAwaiter.GetResult();
					if (streamReader.CharLen_Prop > 0)
					{
						goto IL_2C;
					}
					result = sb.ToString();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600287B RID: 10363 RVA: 0x0008DFDC File Offset: 0x0008C1DC
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04001559 RID: 5465
			public int <>1__state;

			// Token: 0x0400155A RID: 5466
			public AsyncTaskMethodBuilder<string> <>t__builder;

			// Token: 0x0400155B RID: 5467
			public StreamReader <>4__this;

			// Token: 0x0400155C RID: 5468
			private StringBuilder <sb>5__1;

			// Token: 0x0400155D RID: 5469
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200037A RID: 890
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsyncInternal>d__65 : IAsyncStateMachine
		{
			// Token: 0x0600287C RID: 10364 RVA: 0x0008DFEC File Offset: 0x0008C1EC
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				StreamReader streamReader = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
					bool flag;
					switch (num)
					{
					case 0:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						break;
					case 1:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1B1;
					case 2:
						configuredTaskAwaiter3 = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_2D9;
					default:
						flag = (streamReader.CharPos_Prop == streamReader.CharLen_Prop);
						if (!flag)
						{
							goto IL_9E;
						}
						configuredTaskAwaiter3 = streamReader.ReadBufferAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadAsyncInternal>d__65>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						break;
					}
					flag = (configuredTaskAwaiter3.GetResult() == 0);
					IL_9E:
					if (flag)
					{
						result = 0;
						goto IL_4AD;
					}
					charsRead = 0;
					readToUserBuffer = false;
					tmpByteBuffer = streamReader.ByteBuffer_Prop;
					tmpStream = streamReader.Stream_Prop;
					goto IL_47F;
					IL_125:
					if (streamReader.CheckPreamble_Prop)
					{
						int bytePos_Prop = streamReader.BytePos_Prop;
						configuredTaskAwaiter3 = tmpStream.ReadAsync(tmpByteBuffer, bytePos_Prop, tmpByteBuffer.Length - bytePos_Prop).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadAsyncInternal>d__65>(ref configuredTaskAwaiter3, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter3 = tmpStream.ReadAsync(tmpByteBuffer, 0, tmpByteBuffer.Length).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter3.IsCompleted)
						{
							num2 = 2;
							configuredTaskAwaiter2 = configuredTaskAwaiter3;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadAsyncInternal>d__65>(ref configuredTaskAwaiter3, ref this);
							return;
						}
						goto IL_2D9;
					}
					IL_1B1:
					int result2 = configuredTaskAwaiter3.GetResult();
					if (result2 == 0)
					{
						if (streamReader.ByteLen_Prop > 0)
						{
							if (readToUserBuffer)
							{
								i = streamReader.Decoder_Prop.GetChars(tmpByteBuffer, 0, streamReader.ByteLen_Prop, buffer, index + charsRead);
								streamReader.CharLen_Prop = 0;
							}
							else
							{
								i = streamReader.Decoder_Prop.GetChars(tmpByteBuffer, 0, streamReader.ByteLen_Prop, streamReader.CharBuffer_Prop, 0);
								streamReader.CharLen_Prop += i;
							}
						}
						streamReader.IsBlocked_Prop = true;
						goto IL_3E0;
					}
					streamReader.ByteLen_Prop += result2;
					goto IL_2FE;
					IL_2D9:
					int result3 = configuredTaskAwaiter3.GetResult();
					streamReader.ByteLen_Prop = result3;
					if (streamReader.ByteLen_Prop == 0)
					{
						streamReader.IsBlocked_Prop = true;
						goto IL_3E0;
					}
					IL_2FE:
					streamReader.IsBlocked_Prop = (streamReader.ByteLen_Prop < tmpByteBuffer.Length);
					if (!streamReader.IsPreamble())
					{
						if (streamReader.DetectEncoding_Prop && streamReader.ByteLen_Prop >= 2)
						{
							streamReader.DetectEncoding();
							readToUserBuffer = (count >= streamReader.MaxCharsPerBuffer_Prop);
						}
						streamReader.CharPos_Prop = 0;
						if (readToUserBuffer)
						{
							i += streamReader.Decoder_Prop.GetChars(tmpByteBuffer, 0, streamReader.ByteLen_Prop, buffer, index + charsRead);
							streamReader.CharLen_Prop = 0;
						}
						else
						{
							i = streamReader.Decoder_Prop.GetChars(tmpByteBuffer, 0, streamReader.ByteLen_Prop, streamReader.CharBuffer_Prop, 0);
							streamReader.CharLen_Prop += i;
						}
					}
					if (i == 0)
					{
						goto IL_125;
					}
					IL_3E0:
					if (i == 0)
					{
						goto IL_48B;
					}
					IL_3EB:
					if (i > count)
					{
						i = count;
					}
					if (!readToUserBuffer)
					{
						Buffer.InternalBlockCopy(streamReader.CharBuffer_Prop, streamReader.CharPos_Prop * 2, buffer, (index + charsRead) * 2, i * 2);
						streamReader.CharPos_Prop += i;
					}
					charsRead += i;
					count -= i;
					if (streamReader.IsBlocked_Prop)
					{
						goto IL_48B;
					}
					IL_47F:
					if (count > 0)
					{
						i = streamReader.CharLen_Prop - streamReader.CharPos_Prop;
						if (i == 0)
						{
							streamReader.CharLen_Prop = 0;
							streamReader.CharPos_Prop = 0;
							if (!streamReader.CheckPreamble_Prop)
							{
								streamReader.ByteLen_Prop = 0;
							}
							readToUserBuffer = (count >= streamReader.MaxCharsPerBuffer_Prop);
							goto IL_125;
						}
						goto IL_3EB;
					}
					IL_48B:
					result = charsRead;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_4AD:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600287D RID: 10365 RVA: 0x0008E4D8 File Offset: 0x0008C6D8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400155E RID: 5470
			public int <>1__state;

			// Token: 0x0400155F RID: 5471
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04001560 RID: 5472
			public StreamReader <>4__this;

			// Token: 0x04001561 RID: 5473
			public int count;

			// Token: 0x04001562 RID: 5474
			private Stream <tmpStream>5__1;

			// Token: 0x04001563 RID: 5475
			private byte[] <tmpByteBuffer>5__2;

			// Token: 0x04001564 RID: 5476
			private bool <readToUserBuffer>5__3;

			// Token: 0x04001565 RID: 5477
			public char[] buffer;

			// Token: 0x04001566 RID: 5478
			public int index;

			// Token: 0x04001567 RID: 5479
			private int <charsRead>5__4;

			// Token: 0x04001568 RID: 5480
			private int <n>5__5;

			// Token: 0x04001569 RID: 5481
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x0200037B RID: 891
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadBufferAsync>d__98 : IAsyncStateMachine
		{
			// Token: 0x0600287E RID: 10366 RVA: 0x0008E4E8 File Offset: 0x0008C6E8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				StreamReader streamReader = this;
				int charLen_Prop;
				try
				{
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num == 0)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_D9;
					}
					if (num == 1)
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
						num2 = -1;
						goto IL_1C3;
					}
					streamReader.CharLen_Prop = 0;
					streamReader.CharPos_Prop = 0;
					tmpByteBuffer = streamReader.ByteBuffer_Prop;
					tmpStream = streamReader.Stream_Prop;
					if (!streamReader.CheckPreamble_Prop)
					{
						streamReader.ByteLen_Prop = 0;
					}
					IL_50:
					if (streamReader.CheckPreamble_Prop)
					{
						int bytePos_Prop = streamReader.BytePos_Prop;
						configuredTaskAwaiter = tmpStream.ReadAsync(tmpByteBuffer, bytePos_Prop, tmpByteBuffer.Length - bytePos_Prop).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadBufferAsync>d__98>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter = tmpStream.ReadAsync(tmpByteBuffer, 0, tmpByteBuffer.Length).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 1;
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, StreamReader.<ReadBufferAsync>d__98>(ref configuredTaskAwaiter, ref this);
							return;
						}
						goto IL_1C3;
					}
					IL_D9:
					int result = configuredTaskAwaiter.GetResult();
					if (result == 0)
					{
						if (streamReader.ByteLen_Prop > 0)
						{
							streamReader.CharLen_Prop += streamReader.Decoder_Prop.GetChars(tmpByteBuffer, 0, streamReader.ByteLen_Prop, streamReader.CharBuffer_Prop, streamReader.CharLen_Prop);
							streamReader.BytePos_Prop = 0;
							streamReader.ByteLen_Prop = 0;
						}
						charLen_Prop = streamReader.CharLen_Prop;
						goto IL_27B;
					}
					streamReader.ByteLen_Prop += result;
					goto IL_1E8;
					IL_1C3:
					int result2 = configuredTaskAwaiter.GetResult();
					streamReader.ByteLen_Prop = result2;
					if (streamReader.ByteLen_Prop == 0)
					{
						charLen_Prop = streamReader.CharLen_Prop;
						goto IL_27B;
					}
					IL_1E8:
					streamReader.IsBlocked_Prop = (streamReader.ByteLen_Prop < tmpByteBuffer.Length);
					if (!streamReader.IsPreamble())
					{
						if (streamReader.DetectEncoding_Prop && streamReader.ByteLen_Prop >= 2)
						{
							streamReader.DetectEncoding();
						}
						streamReader.CharLen_Prop += streamReader.Decoder_Prop.GetChars(tmpByteBuffer, 0, streamReader.ByteLen_Prop, streamReader.CharBuffer_Prop, streamReader.CharLen_Prop);
					}
					if (streamReader.CharLen_Prop == 0)
					{
						goto IL_50;
					}
					charLen_Prop = streamReader.CharLen_Prop;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_27B:
				num2 = -2;
				this.<>t__builder.SetResult(charLen_Prop);
			}

			// Token: 0x0600287F RID: 10367 RVA: 0x0008E7A0 File Offset: 0x0008C9A0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x0400156A RID: 5482
			public int <>1__state;

			// Token: 0x0400156B RID: 5483
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x0400156C RID: 5484
			public StreamReader <>4__this;

			// Token: 0x0400156D RID: 5485
			private Stream <tmpStream>5__1;

			// Token: 0x0400156E RID: 5486
			private byte[] <tmpByteBuffer>5__2;

			// Token: 0x0400156F RID: 5487
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__1;
		}
	}
}
