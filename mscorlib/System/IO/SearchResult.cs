﻿using System;
using System.Security;
using Microsoft.Win32;

namespace System.IO
{
	// Token: 0x02000366 RID: 870
	internal sealed class SearchResult
	{
		// Token: 0x06002752 RID: 10066 RVA: 0x0008A1BB File Offset: 0x000883BB
		[SecurityCritical]
		internal SearchResult(string fullPath, string userPath, Win32Native.WIN32_FIND_DATA findData)
		{
			this.fullPath = fullPath;
			this.userPath = userPath;
			this.findData = findData;
		}

		// Token: 0x170005FB RID: 1531
		// (get) Token: 0x06002753 RID: 10067 RVA: 0x0008A1D8 File Offset: 0x000883D8
		internal string FullPath
		{
			get
			{
				return this.fullPath;
			}
		}

		// Token: 0x170005FC RID: 1532
		// (get) Token: 0x06002754 RID: 10068 RVA: 0x0008A1E0 File Offset: 0x000883E0
		internal string UserPath
		{
			get
			{
				return this.userPath;
			}
		}

		// Token: 0x170005FD RID: 1533
		// (get) Token: 0x06002755 RID: 10069 RVA: 0x0008A1E8 File Offset: 0x000883E8
		internal Win32Native.WIN32_FIND_DATA FindData
		{
			[SecurityCritical]
			get
			{
				return this.findData;
			}
		}

		// Token: 0x040014F5 RID: 5365
		private string fullPath;

		// Token: 0x040014F6 RID: 5366
		private string userPath;

		// Token: 0x040014F7 RID: 5367
		[SecurityCritical]
		private Win32Native.WIN32_FIND_DATA findData;
	}
}
