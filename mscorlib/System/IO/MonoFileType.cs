﻿using System;

namespace System.IO
{
	// Token: 0x020003A5 RID: 933
	internal enum MonoFileType
	{
		// Token: 0x0400166E RID: 5742
		Unknown,
		// Token: 0x0400166F RID: 5743
		Disk,
		// Token: 0x04001670 RID: 5744
		Char,
		// Token: 0x04001671 RID: 5745
		Pipe,
		// Token: 0x04001672 RID: 5746
		Remote = 32768
	}
}
