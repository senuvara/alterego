﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.IO
{
	/// <summary>The exception that is thrown when an I/O error occurs.</summary>
	// Token: 0x02000369 RID: 873
	[ComVisible(true)]
	[Serializable]
	public class IOException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.IOException" /> class with its message string set to the empty string (""), its HRESULT set to COR_E_IO, and its inner exception set to a null reference.</summary>
		// Token: 0x06002773 RID: 10099 RVA: 0x0008A5BE File Offset: 0x000887BE
		public IOException() : base(Environment.GetResourceString("I/O error occurred."))
		{
			base.SetErrorCode(-2146232800);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.IOException" /> class with its message string set to <paramref name="message" />, its HRESULT set to COR_E_IO, and its inner exception set to <see langword="null" />.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. The content of <paramref name="message" /> is intended to be understood by humans. The caller of this constructor is required to ensure that this string has been localized for the current system culture. </param>
		// Token: 0x06002774 RID: 10100 RVA: 0x0008A5DB File Offset: 0x000887DB
		public IOException(string message) : base(message)
		{
			base.SetErrorCode(-2146232800);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.IOException" /> class with its message string set to <paramref name="message" /> and its HRESULT user-defined.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. The content of <paramref name="message" /> is intended to be understood by humans. The caller of this constructor is required to ensure that this string has been localized for the current system culture. </param>
		/// <param name="hresult">An integer identifying the error that has occurred. </param>
		// Token: 0x06002775 RID: 10101 RVA: 0x00047404 File Offset: 0x00045604
		public IOException(string message, int hresult) : base(message)
		{
			base.SetErrorCode(hresult);
		}

		// Token: 0x06002776 RID: 10102 RVA: 0x0008A5EF File Offset: 0x000887EF
		internal IOException(string message, int hresult, string maybeFullPath) : base(message)
		{
			base.SetErrorCode(hresult);
			this._maybeFullPath = maybeFullPath;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.IOException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06002777 RID: 10103 RVA: 0x0008A606 File Offset: 0x00088806
		public IOException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146232800);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.IOException" /> class with the specified serialization and context information.</summary>
		/// <param name="info">The data for serializing or deserializing the object. </param>
		/// <param name="context">The source and destination for the object. </param>
		// Token: 0x06002778 RID: 10104 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected IOException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040014FF RID: 5375
		[NonSerialized]
		private string _maybeFullPath;
	}
}
