﻿using System;
using System.Security;

namespace System.IO
{
	// Token: 0x02000365 RID: 869
	internal class FileSystemInfoResultHandler : SearchResultHandler<FileSystemInfo>
	{
		// Token: 0x0600274F RID: 10063 RVA: 0x0008A134 File Offset: 0x00088334
		[SecurityCritical]
		internal override bool IsResultIncluded(SearchResult result)
		{
			bool flag = FileSystemEnumerableHelpers.IsFile(result.FindData);
			return FileSystemEnumerableHelpers.IsDir(result.FindData) || flag;
		}

		// Token: 0x06002750 RID: 10064 RVA: 0x0008A15C File Offset: 0x0008835C
		[SecurityCritical]
		internal override FileSystemInfo CreateObject(SearchResult result)
		{
			FileSystemEnumerableHelpers.IsFile(result.FindData);
			if (FileSystemEnumerableHelpers.IsDir(result.FindData))
			{
				DirectoryInfo directoryInfo = new DirectoryInfo(result.FullPath, false);
				directoryInfo.InitializeFrom(result.FindData);
				return directoryInfo;
			}
			FileInfo fileInfo = new FileInfo(result.FullPath, false);
			fileInfo.InitializeFrom(result.FindData);
			return fileInfo;
		}

		// Token: 0x06002751 RID: 10065 RVA: 0x0008A1B3 File Offset: 0x000883B3
		public FileSystemInfoResultHandler()
		{
		}
	}
}
