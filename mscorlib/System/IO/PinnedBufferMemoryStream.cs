﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace System.IO
{
	// Token: 0x0200036C RID: 876
	internal sealed class PinnedBufferMemoryStream : UnmanagedMemoryStream
	{
		// Token: 0x060027A3 RID: 10147 RVA: 0x0008B42B File Offset: 0x0008962B
		[SecurityCritical]
		private PinnedBufferMemoryStream()
		{
		}

		// Token: 0x060027A4 RID: 10148 RVA: 0x0008B434 File Offset: 0x00089634
		[SecurityCritical]
		internal unsafe PinnedBufferMemoryStream(byte[] array)
		{
			int num = array.Length;
			if (num == 0)
			{
				array = new byte[1];
				num = 0;
			}
			this._array = array;
			this._pinningHandle = new GCHandle(array, GCHandleType.Pinned);
			byte[] array2;
			byte* pointer;
			if ((array2 = this._array) == null || array2.Length == 0)
			{
				pointer = null;
			}
			else
			{
				pointer = &array2[0];
			}
			base.Initialize(pointer, (long)num, (long)num, FileAccess.Read, true);
			array2 = null;
		}

		// Token: 0x060027A5 RID: 10149 RVA: 0x0008B498 File Offset: 0x00089698
		~PinnedBufferMemoryStream()
		{
			this.Dispose(false);
		}

		// Token: 0x060027A6 RID: 10150 RVA: 0x0008B4C8 File Offset: 0x000896C8
		[SecuritySafeCritical]
		protected override void Dispose(bool disposing)
		{
			if (this._isOpen)
			{
				this._pinningHandle.Free();
				this._isOpen = false;
			}
			base.Dispose(disposing);
		}

		// Token: 0x0400150B RID: 5387
		private byte[] _array;

		// Token: 0x0400150C RID: 5388
		private GCHandle _pinningHandle;
	}
}
