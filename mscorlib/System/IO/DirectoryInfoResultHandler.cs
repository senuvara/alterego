﻿using System;
using System.Security;

namespace System.IO
{
	// Token: 0x02000364 RID: 868
	internal class DirectoryInfoResultHandler : SearchResultHandler<DirectoryInfo>
	{
		// Token: 0x0600274C RID: 10060 RVA: 0x0008A103 File Offset: 0x00088303
		[SecurityCritical]
		internal override bool IsResultIncluded(SearchResult result)
		{
			return FileSystemEnumerableHelpers.IsDir(result.FindData);
		}

		// Token: 0x0600274D RID: 10061 RVA: 0x0008A110 File Offset: 0x00088310
		[SecurityCritical]
		internal override DirectoryInfo CreateObject(SearchResult result)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(result.FullPath, false);
			directoryInfo.InitializeFrom(result.FindData);
			return directoryInfo;
		}

		// Token: 0x0600274E RID: 10062 RVA: 0x0008A12A File Offset: 0x0008832A
		public DirectoryInfoResultHandler()
		{
		}
	}
}
