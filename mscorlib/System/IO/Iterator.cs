﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace System.IO
{
	// Token: 0x0200035F RID: 863
	internal abstract class Iterator<TSource> : IEnumerable<!0>, IEnumerable, IEnumerator<TSource>, IDisposable, IEnumerator
	{
		// Token: 0x0600272C RID: 10028 RVA: 0x00089882 File Offset: 0x00087A82
		public Iterator()
		{
			this.threadId = Thread.CurrentThread.ManagedThreadId;
		}

		// Token: 0x170005F9 RID: 1529
		// (get) Token: 0x0600272D RID: 10029 RVA: 0x0008989A File Offset: 0x00087A9A
		public TSource Current
		{
			get
			{
				return this.current;
			}
		}

		// Token: 0x0600272E RID: 10030
		protected abstract Iterator<TSource> Clone();

		// Token: 0x0600272F RID: 10031 RVA: 0x000898A2 File Offset: 0x00087AA2
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06002730 RID: 10032 RVA: 0x000898B1 File Offset: 0x00087AB1
		protected virtual void Dispose(bool disposing)
		{
			this.current = default(TSource);
			this.state = -1;
		}

		// Token: 0x06002731 RID: 10033 RVA: 0x000898C6 File Offset: 0x00087AC6
		public IEnumerator<TSource> GetEnumerator()
		{
			if (this.threadId == Thread.CurrentThread.ManagedThreadId && this.state == 0)
			{
				this.state = 1;
				return this;
			}
			Iterator<TSource> iterator = this.Clone();
			iterator.state = 1;
			return iterator;
		}

		// Token: 0x06002732 RID: 10034
		public abstract bool MoveNext();

		// Token: 0x170005FA RID: 1530
		// (get) Token: 0x06002733 RID: 10035 RVA: 0x000898F8 File Offset: 0x00087AF8
		object IEnumerator.Current
		{
			get
			{
				return this.Current;
			}
		}

		// Token: 0x06002734 RID: 10036 RVA: 0x00089905 File Offset: 0x00087B05
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06002735 RID: 10037 RVA: 0x000175EA File Offset: 0x000157EA
		void IEnumerator.Reset()
		{
			throw new NotSupportedException();
		}

		// Token: 0x040014E0 RID: 5344
		private int threadId;

		// Token: 0x040014E1 RID: 5345
		internal int state;

		// Token: 0x040014E2 RID: 5346
		internal TSource current;
	}
}
