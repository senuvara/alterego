﻿using System;
using System.Threading;

namespace System.IO
{
	// Token: 0x020003A3 RID: 931
	internal class FileStreamAsyncResult : IAsyncResult
	{
		// Token: 0x06002ACE RID: 10958 RVA: 0x00096C1B File Offset: 0x00094E1B
		public FileStreamAsyncResult(AsyncCallback cb, object state)
		{
			this.state = state;
			this.realcb = cb;
			if (this.realcb != null)
			{
				this.cb = new AsyncCallback(FileStreamAsyncResult.CBWrapper);
			}
			this.wh = new ManualResetEvent(false);
		}

		// Token: 0x06002ACF RID: 10959 RVA: 0x00096C57 File Offset: 0x00094E57
		private static void CBWrapper(IAsyncResult ares)
		{
			((FileStreamAsyncResult)ares).realcb.BeginInvoke(ares, null, null);
		}

		// Token: 0x06002AD0 RID: 10960 RVA: 0x00096C6D File Offset: 0x00094E6D
		public void SetComplete(Exception e)
		{
			this.exc = e;
			this.completed = true;
			this.wh.Set();
			if (this.cb != null)
			{
				this.cb(this);
			}
		}

		// Token: 0x06002AD1 RID: 10961 RVA: 0x00096C9D File Offset: 0x00094E9D
		public void SetComplete(Exception e, int nbytes)
		{
			this.BytesRead = nbytes;
			this.SetComplete(e);
		}

		// Token: 0x06002AD2 RID: 10962 RVA: 0x00096CAD File Offset: 0x00094EAD
		public void SetComplete(Exception e, int nbytes, bool synch)
		{
			this.completedSynch = synch;
			this.SetComplete(e, nbytes);
		}

		// Token: 0x17000680 RID: 1664
		// (get) Token: 0x06002AD3 RID: 10963 RVA: 0x00096CBE File Offset: 0x00094EBE
		public object AsyncState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x17000681 RID: 1665
		// (get) Token: 0x06002AD4 RID: 10964 RVA: 0x00096CC6 File Offset: 0x00094EC6
		public bool CompletedSynchronously
		{
			get
			{
				return this.completedSynch;
			}
		}

		// Token: 0x17000682 RID: 1666
		// (get) Token: 0x06002AD5 RID: 10965 RVA: 0x00096CCE File Offset: 0x00094ECE
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				return this.wh;
			}
		}

		// Token: 0x17000683 RID: 1667
		// (get) Token: 0x06002AD6 RID: 10966 RVA: 0x00096CD6 File Offset: 0x00094ED6
		public bool IsCompleted
		{
			get
			{
				return this.completed;
			}
		}

		// Token: 0x17000684 RID: 1668
		// (get) Token: 0x06002AD7 RID: 10967 RVA: 0x00096CDE File Offset: 0x00094EDE
		public Exception Exception
		{
			get
			{
				return this.exc;
			}
		}

		// Token: 0x17000685 RID: 1669
		// (get) Token: 0x06002AD8 RID: 10968 RVA: 0x00096CE6 File Offset: 0x00094EE6
		// (set) Token: 0x06002AD9 RID: 10969 RVA: 0x00096CEE File Offset: 0x00094EEE
		public bool Done
		{
			get
			{
				return this.done;
			}
			set
			{
				this.done = value;
			}
		}

		// Token: 0x0400165F RID: 5727
		private object state;

		// Token: 0x04001660 RID: 5728
		private bool completed;

		// Token: 0x04001661 RID: 5729
		private bool done;

		// Token: 0x04001662 RID: 5730
		private Exception exc;

		// Token: 0x04001663 RID: 5731
		private ManualResetEvent wh;

		// Token: 0x04001664 RID: 5732
		private AsyncCallback cb;

		// Token: 0x04001665 RID: 5733
		private bool completedSynch;

		// Token: 0x04001666 RID: 5734
		public byte[] Buffer;

		// Token: 0x04001667 RID: 5735
		public int Offset;

		// Token: 0x04001668 RID: 5736
		public int Count;

		// Token: 0x04001669 RID: 5737
		public int OriginalCount;

		// Token: 0x0400166A RID: 5738
		public int BytesRead;

		// Token: 0x0400166B RID: 5739
		private AsyncCallback realcb;
	}
}
