﻿using System;
using System.Runtime.InteropServices;

namespace System.IO.IsolatedStorage
{
	/// <summary>Enumerates the levels of isolated storage scope that are supported by <see cref="T:System.IO.IsolatedStorage.IsolatedStorage" />.</summary>
	// Token: 0x020003B7 RID: 951
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum IsolatedStorageScope
	{
		/// <summary>No isolated storage usage.</summary>
		// Token: 0x040016B9 RID: 5817
		None = 0,
		/// <summary>Isolated storage scoped by user identity.</summary>
		// Token: 0x040016BA RID: 5818
		User = 1,
		/// <summary>Isolated storage scoped to the application domain identity.</summary>
		// Token: 0x040016BB RID: 5819
		Domain = 2,
		/// <summary>Isolated storage scoped to the identity of the assembly.</summary>
		// Token: 0x040016BC RID: 5820
		Assembly = 4,
		/// <summary>The isolated store can be placed in a location on the file system that might roam (if roaming user data is enabled on the underlying operating system).</summary>
		// Token: 0x040016BD RID: 5821
		Roaming = 8,
		/// <summary>Isolated storage scoped to the machine.</summary>
		// Token: 0x040016BE RID: 5822
		Machine = 16,
		/// <summary>Isolated storage scoped to the application.</summary>
		// Token: 0x040016BF RID: 5823
		Application = 32
	}
}
