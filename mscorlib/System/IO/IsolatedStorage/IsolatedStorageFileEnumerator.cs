﻿using System;
using System.Collections;

namespace System.IO.IsolatedStorage
{
	// Token: 0x020003B5 RID: 949
	internal class IsolatedStorageFileEnumerator : IEnumerator
	{
		// Token: 0x06002BBB RID: 11195 RVA: 0x0009A123 File Offset: 0x00098323
		public IsolatedStorageFileEnumerator(IsolatedStorageScope scope, string root)
		{
			this._scope = scope;
			if (Directory.Exists(root))
			{
				this._storages = Directory.GetDirectories(root, "d.*");
			}
			this._pos = -1;
		}

		// Token: 0x170006A3 RID: 1699
		// (get) Token: 0x06002BBC RID: 11196 RVA: 0x0009A152 File Offset: 0x00098352
		public object Current
		{
			get
			{
				if (this._pos < 0 || this._storages == null || this._pos >= this._storages.Length)
				{
					return null;
				}
				return new IsolatedStorageFile(this._scope, this._storages[this._pos]);
			}
		}

		// Token: 0x06002BBD RID: 11197 RVA: 0x0009A190 File Offset: 0x00098390
		public bool MoveNext()
		{
			if (this._storages == null)
			{
				return false;
			}
			int num = this._pos + 1;
			this._pos = num;
			return num < this._storages.Length;
		}

		// Token: 0x06002BBE RID: 11198 RVA: 0x0009A1C2 File Offset: 0x000983C2
		public void Reset()
		{
			this._pos = -1;
		}

		// Token: 0x040016B5 RID: 5813
		private IsolatedStorageScope _scope;

		// Token: 0x040016B6 RID: 5814
		private string[] _storages;

		// Token: 0x040016B7 RID: 5815
		private int _pos;
	}
}
