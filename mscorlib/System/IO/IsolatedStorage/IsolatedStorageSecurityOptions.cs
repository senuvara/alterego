﻿using System;

namespace System.IO.IsolatedStorage
{
	/// <summary>Specifies options that affect security in isolated storage.</summary>
	// Token: 0x020003B8 RID: 952
	public enum IsolatedStorageSecurityOptions
	{
		/// <summary>The quota can be increased for isolated storage.</summary>
		// Token: 0x040016C1 RID: 5825
		IncreaseQuotaForApplication = 4
	}
}
