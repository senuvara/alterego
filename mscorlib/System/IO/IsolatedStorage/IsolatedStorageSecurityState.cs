﻿using System;
using System.Security;

namespace System.IO.IsolatedStorage
{
	/// <summary>Provides settings for maintaining the quota size for isolated storage.</summary>
	// Token: 0x020003B9 RID: 953
	public class IsolatedStorageSecurityState : SecurityState
	{
		// Token: 0x06002BDE RID: 11230 RVA: 0x0009A457 File Offset: 0x00098657
		internal IsolatedStorageSecurityState()
		{
		}

		/// <summary>Gets the option for managing isolated storage security.</summary>
		/// <returns>The option to increase the isolated quota storage size.</returns>
		// Token: 0x170006AC RID: 1708
		// (get) Token: 0x06002BDF RID: 11231 RVA: 0x000286CC File Offset: 0x000268CC
		public IsolatedStorageSecurityOptions Options
		{
			get
			{
				return IsolatedStorageSecurityOptions.IncreaseQuotaForApplication;
			}
		}

		/// <summary>Gets or sets the current size of the quota for isolated storage.</summary>
		/// <returns>The current quota size, in bytes.</returns>
		// Token: 0x170006AD RID: 1709
		// (get) Token: 0x06002BE0 RID: 11232 RVA: 0x000041F3 File Offset: 0x000023F3
		// (set) Token: 0x06002BE1 RID: 11233 RVA: 0x000020D3 File Offset: 0x000002D3
		public long Quota
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
			}
		}

		/// <summary>Gets the current usage size in isolated storage.</summary>
		/// <returns>The current usage size, in bytes.</returns>
		// Token: 0x170006AE RID: 1710
		// (get) Token: 0x06002BE2 RID: 11234 RVA: 0x000041F3 File Offset: 0x000023F3
		public long UsedSize
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Ensures that the state that is represented by <see cref="T:System.IO.IsolatedStorage.IsolatedStorageSecurityState" /> is available on the host.</summary>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The state is not available.</exception>
		// Token: 0x06002BE3 RID: 11235 RVA: 0x000041F3 File Offset: 0x000023F3
		public override void EnsureState()
		{
			throw new NotImplementedException();
		}
	}
}
