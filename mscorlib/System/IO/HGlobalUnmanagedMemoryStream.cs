﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x020003A4 RID: 932
	internal class HGlobalUnmanagedMemoryStream : UnmanagedMemoryStream
	{
		// Token: 0x06002ADA RID: 10970 RVA: 0x00096CF7 File Offset: 0x00094EF7
		public unsafe HGlobalUnmanagedMemoryStream(byte* pointer, long length, IntPtr ptr) : base(pointer, length, length, FileAccess.ReadWrite)
		{
			this.ptr = ptr;
		}

		// Token: 0x06002ADB RID: 10971 RVA: 0x00096D0A File Offset: 0x00094F0A
		protected override void Dispose(bool disposing)
		{
			if (this._isOpen)
			{
				Marshal.FreeHGlobal(this.ptr);
			}
			base.Dispose(disposing);
		}

		// Token: 0x0400166C RID: 5740
		private IntPtr ptr;
	}
}
