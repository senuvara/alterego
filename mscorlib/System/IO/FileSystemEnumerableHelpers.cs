﻿using System;
using System.Security;
using Microsoft.Win32;

namespace System.IO
{
	// Token: 0x02000367 RID: 871
	internal static class FileSystemEnumerableHelpers
	{
		// Token: 0x06002756 RID: 10070 RVA: 0x0008A1F0 File Offset: 0x000883F0
		[SecurityCritical]
		internal static bool IsDir(Win32Native.WIN32_FIND_DATA data)
		{
			return (data.dwFileAttributes & 16) != 0 && !data.cFileName.Equals(".") && !data.cFileName.Equals("..");
		}

		// Token: 0x06002757 RID: 10071 RVA: 0x0008A224 File Offset: 0x00088424
		[SecurityCritical]
		internal static bool IsFile(Win32Native.WIN32_FIND_DATA data)
		{
			return (data.dwFileAttributes & 16) == 0;
		}
	}
}
