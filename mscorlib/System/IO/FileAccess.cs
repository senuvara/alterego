﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	/// <summary>Defines constants for read, write, or read/write access to a file.</summary>
	// Token: 0x0200039B RID: 923
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum FileAccess
	{
		/// <summary>Read access to the file. Data can be read from the file. Combine with <see langword="Write" /> for read/write access.</summary>
		// Token: 0x04001623 RID: 5667
		Read = 1,
		/// <summary>Write access to the file. Data can be written to the file. Combine with <see langword="Read" /> for read/write access.</summary>
		// Token: 0x04001624 RID: 5668
		Write = 2,
		/// <summary>Read and write access to the file. Data can be written to and read from the file.</summary>
		// Token: 0x04001625 RID: 5669
		ReadWrite = 3
	}
}
