﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	/// <summary>Specifies the position in a stream to use for seeking.</summary>
	// Token: 0x020003AC RID: 940
	[ComVisible(true)]
	[Serializable]
	public enum SeekOrigin
	{
		/// <summary>Specifies the beginning of a stream.</summary>
		// Token: 0x040016A7 RID: 5799
		Begin,
		/// <summary>Specifies the current position within a stream.</summary>
		// Token: 0x040016A8 RID: 5800
		Current,
		/// <summary>Specifies the end of a stream.</summary>
		// Token: 0x040016A9 RID: 5801
		End
	}
}
