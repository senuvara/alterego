﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Security.AccessControl;
using Microsoft.Win32.SafeHandles;

namespace System.IO
{
	/// <summary>Exposes instance methods for creating, moving, and enumerating through directories and subdirectories. This class cannot be inherited.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x02000392 RID: 914
	[ComVisible(true)]
	[Serializable]
	public sealed class DirectoryInfo : FileSystemInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IO.DirectoryInfo" /> class on the specified path.</summary>
		/// <param name="path">A string specifying the path on which to create the <see langword="DirectoryInfo" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> contains invalid characters such as ", &lt;, &gt;, or |. </exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters. The specified path, file name, or both are too long.</exception>
		// Token: 0x060029E2 RID: 10722 RVA: 0x00093795 File Offset: 0x00091995
		public DirectoryInfo(string path) : this(path, false)
		{
		}

		// Token: 0x060029E3 RID: 10723 RVA: 0x000937A0 File Offset: 0x000919A0
		internal DirectoryInfo(string path, bool simpleOriginalPath)
		{
			this.CheckPath(path);
			SecurityManager.EnsureElevatedPermissions();
			this.FullPath = Path.GetFullPath(path);
			if (simpleOriginalPath)
			{
				this.OriginalPath = Path.GetFileName(this.FullPath);
			}
			else
			{
				this.OriginalPath = path;
			}
			this.Initialize();
		}

		// Token: 0x060029E4 RID: 10724 RVA: 0x000937EE File Offset: 0x000919EE
		private DirectoryInfo(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.Initialize();
		}

		// Token: 0x060029E5 RID: 10725 RVA: 0x00093800 File Offset: 0x00091A00
		private void Initialize()
		{
			int num = this.FullPath.Length - 1;
			if (num > 1 && this.FullPath[num] == Path.DirectorySeparatorChar)
			{
				num--;
			}
			int num2 = this.FullPath.LastIndexOf(Path.DirectorySeparatorChar, num);
			if (num2 == -1 || (num2 == 0 && num == 0))
			{
				this.current = this.FullPath;
				this.parent = null;
				return;
			}
			this.current = this.FullPath.Substring(num2 + 1, num - num2);
			if (num2 == 0 && !Environment.IsRunningOnWindows)
			{
				this.parent = Path.DirectorySeparatorStr;
			}
			else
			{
				this.parent = this.FullPath.Substring(0, num2);
			}
			if (Environment.IsRunningOnWindows && this.parent.Length == 2 && this.parent[1] == ':' && char.IsLetter(this.parent[0]))
			{
				this.parent += Path.DirectorySeparatorChar.ToString();
			}
		}

		/// <summary>Gets a value indicating whether the directory exists.</summary>
		/// <returns>
		///     <see langword="true" /> if the directory exists; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000661 RID: 1633
		// (get) Token: 0x060029E6 RID: 10726 RVA: 0x000938FD File Offset: 0x00091AFD
		public override bool Exists
		{
			get
			{
				if (this._dataInitialised == -1)
				{
					base.Refresh();
				}
				return this._data.fileAttributes != (FileAttributes)(-1) && (this._data.fileAttributes & FileAttributes.Directory) != (FileAttributes)0;
			}
		}

		/// <summary>Gets the name of this <see cref="T:System.IO.DirectoryInfo" /> instance.</summary>
		/// <returns>The directory name.</returns>
		// Token: 0x17000662 RID: 1634
		// (get) Token: 0x060029E7 RID: 10727 RVA: 0x00093931 File Offset: 0x00091B31
		public override string Name
		{
			get
			{
				return this.current;
			}
		}

		/// <summary>Gets the parent directory of a specified subdirectory.</summary>
		/// <returns>The parent directory, or <see langword="null" /> if the path is null or if the file path denotes a root (such as "\", "C:", or * "\\server\share").</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x17000663 RID: 1635
		// (get) Token: 0x060029E8 RID: 10728 RVA: 0x00093939 File Offset: 0x00091B39
		public DirectoryInfo Parent
		{
			get
			{
				if (this.parent == null || this.parent.Length == 0)
				{
					return null;
				}
				return new DirectoryInfo(this.parent);
			}
		}

		/// <summary>Gets the root portion of the directory.</summary>
		/// <returns>An object that represents the root of the directory.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x17000664 RID: 1636
		// (get) Token: 0x060029E9 RID: 10729 RVA: 0x00093960 File Offset: 0x00091B60
		public DirectoryInfo Root
		{
			get
			{
				string pathRoot = Path.GetPathRoot(this.FullPath);
				if (pathRoot == null)
				{
					return null;
				}
				return new DirectoryInfo(pathRoot);
			}
		}

		/// <summary>Creates a directory.</summary>
		/// <exception cref="T:System.IO.IOException">The directory cannot be created. </exception>
		// Token: 0x060029EA RID: 10730 RVA: 0x00093984 File Offset: 0x00091B84
		public void Create()
		{
			Directory.CreateDirectory(this.FullPath);
		}

		/// <summary>Creates a subdirectory or subdirectories on the specified path. The specified path can be relative to this instance of the <see cref="T:System.IO.DirectoryInfo" /> class.</summary>
		/// <param name="path">The specified path. This cannot be a different disk volume or Universal Naming Convention (UNC) name. </param>
		/// <returns>The last directory specified in <paramref name="path" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> does not specify a valid file path or contains invalid <see langword="DirectoryInfo" /> characters. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.IO.IOException">The subdirectory cannot be created.-or- A file or directory already has the name specified by <paramref name="path" />. </exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters. The specified path, file name, or both are too long.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have code access permission to create the directory.-or-The caller does not have code access permission to read the directory described by the returned <see cref="T:System.IO.DirectoryInfo" /> object.  This can occur when the <paramref name="path" /> parameter describes an existing directory.</exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <paramref name="path" /> contains a colon character (:) that is not part of a drive label ("C:\").</exception>
		// Token: 0x060029EB RID: 10731 RVA: 0x00093992 File Offset: 0x00091B92
		public DirectoryInfo CreateSubdirectory(string path)
		{
			this.CheckPath(path);
			path = Path.Combine(this.FullPath, path);
			Directory.CreateDirectory(path);
			return new DirectoryInfo(path);
		}

		/// <summary>Returns a file list from the current directory.</summary>
		/// <returns>An array of type <see cref="T:System.IO.FileInfo" />.</returns>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path is invalid, such as being on an unmapped drive. </exception>
		// Token: 0x060029EC RID: 10732 RVA: 0x000939B6 File Offset: 0x00091BB6
		public FileInfo[] GetFiles()
		{
			return this.GetFiles("*");
		}

		/// <summary>Returns a file list from the current directory matching the given search pattern.</summary>
		/// <param name="searchPattern">The search string to match against the names of files.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <returns>An array of type <see cref="T:System.IO.FileInfo" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="searchPattern " />contains one or more invalid characters defined by the <see cref="M:System.IO.Path.GetInvalidPathChars" /> method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060029ED RID: 10733 RVA: 0x000939C4 File Offset: 0x00091BC4
		public FileInfo[] GetFiles(string searchPattern)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			string[] files = Directory.GetFiles(this.FullPath, searchPattern);
			FileInfo[] array = new FileInfo[files.Length];
			int num = 0;
			foreach (string fileName in files)
			{
				array[num++] = new FileInfo(fileName);
			}
			return array;
		}

		/// <summary>Returns the subdirectories of the current directory.</summary>
		/// <returns>An array of <see cref="T:System.IO.DirectoryInfo" /> objects.</returns>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		// Token: 0x060029EE RID: 10734 RVA: 0x00093A19 File Offset: 0x00091C19
		public DirectoryInfo[] GetDirectories()
		{
			return this.GetDirectories("*");
		}

		/// <summary>Returns an array of directories in the current <see cref="T:System.IO.DirectoryInfo" /> matching the given search criteria.</summary>
		/// <param name="searchPattern">The search string to match against the names of directories.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <returns>An array of type <see langword="DirectoryInfo" /> matching <paramref name="searchPattern" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="searchPattern " />contains one or more invalid characters defined by the <see cref="M:System.IO.Path.GetInvalidPathChars" /> method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see langword="DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		// Token: 0x060029EF RID: 10735 RVA: 0x00093A28 File Offset: 0x00091C28
		public DirectoryInfo[] GetDirectories(string searchPattern)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			string[] directories = Directory.GetDirectories(this.FullPath, searchPattern);
			DirectoryInfo[] array = new DirectoryInfo[directories.Length];
			int num = 0;
			foreach (string path in directories)
			{
				array[num++] = new DirectoryInfo(path);
			}
			return array;
		}

		/// <summary>Returns an array of strongly typed <see cref="T:System.IO.FileSystemInfo" /> entries representing all the files and subdirectories in a directory.</summary>
		/// <returns>An array of strongly typed <see cref="T:System.IO.FileSystemInfo" /> entries.</returns>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path is invalid (for example, it is on an unmapped drive). </exception>
		// Token: 0x060029F0 RID: 10736 RVA: 0x00093A7D File Offset: 0x00091C7D
		public FileSystemInfo[] GetFileSystemInfos()
		{
			return this.GetFileSystemInfos("*");
		}

		/// <summary>Retrieves an array of strongly typed <see cref="T:System.IO.FileSystemInfo" /> objects representing the files and subdirectories that match the specified search criteria.</summary>
		/// <param name="searchPattern">The search string to match against the names of directories and files.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <returns>An array of strongly typed <see langword="FileSystemInfo" /> objects matching the search criteria.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="searchPattern " />contains one or more invalid characters defined by the <see cref="M:System.IO.Path.GetInvalidPathChars" /> method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060029F1 RID: 10737 RVA: 0x00093A8A File Offset: 0x00091C8A
		public FileSystemInfo[] GetFileSystemInfos(string searchPattern)
		{
			return this.GetFileSystemInfos(searchPattern, SearchOption.TopDirectoryOnly);
		}

		/// <summary>Retrieves an array of <see cref="T:System.IO.FileSystemInfo" /> objects that represent the files and subdirectories matching the specified search criteria.</summary>
		/// <param name="searchPattern">The search string to match against the names of directories and filesa.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <param name="searchOption">One of the enumeration values that specifies whether the search operation should include only the current directory or all subdirectories. The default value is <see cref="F:System.IO.SearchOption.TopDirectoryOnly" />.</param>
		/// <returns>An array of file system entries that match the search criteria.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="searchPattern " />contains one or more invalid characters defined by the <see cref="M:System.IO.Path.GetInvalidPathChars" /> method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="searchOption" /> is not a valid <see cref="T:System.IO.SearchOption" /> value.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060029F2 RID: 10738 RVA: 0x00093A94 File Offset: 0x00091C94
		public FileSystemInfo[] GetFileSystemInfos(string searchPattern, SearchOption searchOption)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			if (searchOption != SearchOption.TopDirectoryOnly && searchOption != SearchOption.AllDirectories)
			{
				throw new ArgumentOutOfRangeException("searchOption", "Must be TopDirectoryOnly or AllDirectories");
			}
			if (!Directory.Exists(this.FullPath))
			{
				throw new IOException("Invalid directory");
			}
			List<FileSystemInfo> list = new List<FileSystemInfo>();
			this.InternalGetFileSystemInfos(searchPattern, searchOption, list);
			return list.ToArray();
		}

		// Token: 0x060029F3 RID: 10739 RVA: 0x00093AF4 File Offset: 0x00091CF4
		private void InternalGetFileSystemInfos(string searchPattern, SearchOption searchOption, List<FileSystemInfo> infos)
		{
			string[] directories = Directory.GetDirectories(this.FullPath, searchPattern);
			string[] files = Directory.GetFiles(this.FullPath, searchPattern);
			Array.ForEach<string>(directories, delegate(string dir)
			{
				infos.Add(new DirectoryInfo(dir));
			});
			Array.ForEach<string>(files, delegate(string file)
			{
				infos.Add(new FileInfo(file));
			});
			if (directories.Length == 0 || searchOption == SearchOption.TopDirectoryOnly)
			{
				return;
			}
			string[] array = directories;
			for (int i = 0; i < array.Length; i++)
			{
				new DirectoryInfo(array[i]).InternalGetFileSystemInfos(searchPattern, searchOption, infos);
			}
		}

		/// <summary>Deletes this <see cref="T:System.IO.DirectoryInfo" /> if it is empty.</summary>
		/// <exception cref="T:System.UnauthorizedAccessException">The directory contains a read-only file.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The directory described by this <see cref="T:System.IO.DirectoryInfo" /> object does not exist or could not be found.</exception>
		/// <exception cref="T:System.IO.IOException">The directory is not empty. -or-The directory is the application's current working directory.-or-There is an open handle on the directory, and the operating system is Windows XP or earlier. This open handle can result from enumerating directories. For more information, see How to: Enumerate Directories and Files.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060029F4 RID: 10740 RVA: 0x00093B77 File Offset: 0x00091D77
		public override void Delete()
		{
			this.Delete(false);
		}

		/// <summary>Deletes this instance of a <see cref="T:System.IO.DirectoryInfo" />, specifying whether to delete subdirectories and files.</summary>
		/// <param name="recursive">
		///       <see langword="true" /> to delete this directory, its subdirectories, and all files; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.UnauthorizedAccessException">The directory contains a read-only file.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The directory described by this <see cref="T:System.IO.DirectoryInfo" /> object does not exist or could not be found.</exception>
		/// <exception cref="T:System.IO.IOException">The directory is read-only.-or- The directory contains one or more files or subdirectories and <paramref name="recursive" /> is <see langword="false" />.-or-The directory is the application's current working directory. -or-There is an open handle on the directory or on one of its files, and the operating system is Windows XP or earlier. This open handle can result from enumerating directories and files. For more information, see How to: Enumerate Directories and Files.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060029F5 RID: 10741 RVA: 0x00093B80 File Offset: 0x00091D80
		public void Delete(bool recursive)
		{
			Directory.Delete(this.FullPath, recursive);
		}

		/// <summary>Moves a <see cref="T:System.IO.DirectoryInfo" /> instance and its contents to a new path.</summary>
		/// <param name="destDirName">The name and path to which to move this directory. The destination cannot be another disk volume or a directory with the identical name. It can be an existing directory to which you want to add this directory as a subdirectory. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="destDirName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="destDirName" /> is an empty string (''"). </exception>
		/// <exception cref="T:System.IO.IOException">An attempt was made to move a directory to a different volume. -or-
		///         <paramref name="destDirName" /> already exists.-or-You are not authorized to access this path.-or- The directory being moved and the destination directory have the same name.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The destination directory cannot be found.</exception>
		// Token: 0x060029F6 RID: 10742 RVA: 0x00093B90 File Offset: 0x00091D90
		public void MoveTo(string destDirName)
		{
			if (destDirName == null)
			{
				throw new ArgumentNullException("destDirName");
			}
			if (destDirName.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.", "destDirName");
			}
			Directory.Move(this.FullPath, Path.GetFullPath(destDirName));
			this.OriginalPath = destDirName;
			this.FullPath = destDirName;
			this.Initialize();
		}

		/// <summary>Returns the original path that was passed by the user.</summary>
		/// <returns>Returns the original path that was passed by the user.</returns>
		// Token: 0x060029F7 RID: 10743 RVA: 0x00093BEA File Offset: 0x00091DEA
		public override string ToString()
		{
			return this.OriginalPath;
		}

		/// <summary>Returns an array of directories in the current <see cref="T:System.IO.DirectoryInfo" /> matching the given search criteria and using a value to determine whether to search subdirectories.</summary>
		/// <param name="searchPattern">The search string to match against the names of directories.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <param name="searchOption">One of the enumeration values that specifies whether the search operation should include only the current directory or all subdirectories.</param>
		/// <returns>An array of type <see langword="DirectoryInfo" /> matching <paramref name="searchPattern" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="searchPattern " />contains one or more invalid characters defined by the <see cref="M:System.IO.Path.GetInvalidPathChars" /> method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="searchOption" /> is not a valid <see cref="T:System.IO.SearchOption" /> value.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see langword="DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		// Token: 0x060029F8 RID: 10744 RVA: 0x00093BF4 File Offset: 0x00091DF4
		public DirectoryInfo[] GetDirectories(string searchPattern, SearchOption searchOption)
		{
			string[] directories = Directory.GetDirectories(this.FullPath, searchPattern, searchOption);
			DirectoryInfo[] array = new DirectoryInfo[directories.Length];
			for (int i = 0; i < directories.Length; i++)
			{
				string path = directories[i];
				array[i] = new DirectoryInfo(path);
			}
			return array;
		}

		// Token: 0x060029F9 RID: 10745 RVA: 0x00093C34 File Offset: 0x00091E34
		internal int GetFilesSubdirs(ArrayList l, string pattern)
		{
			FileInfo[] array = null;
			try
			{
				array = this.GetFiles(pattern);
			}
			catch (UnauthorizedAccessException)
			{
				return 0;
			}
			int num = array.Length;
			l.Add(array);
			foreach (DirectoryInfo directoryInfo in this.GetDirectories())
			{
				num += directoryInfo.GetFilesSubdirs(l, pattern);
			}
			return num;
		}

		/// <summary>Returns a file list from the current directory matching the given search pattern and using a value to determine whether to search subdirectories.</summary>
		/// <param name="searchPattern">The search string to match against the names of files.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <param name="searchOption">One of the enumeration values that specifies whether the search operation should include only the current directory or all subdirectories.</param>
		/// <returns>An array of type <see cref="T:System.IO.FileInfo" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="searchPattern " />contains one or more invalid characters defined by the <see cref="M:System.IO.Path.GetInvalidPathChars" /> method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="searchOption" /> is not a valid <see cref="T:System.IO.SearchOption" /> value.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x060029FA RID: 10746 RVA: 0x00093C9C File Offset: 0x00091E9C
		public FileInfo[] GetFiles(string searchPattern, SearchOption searchOption)
		{
			if (searchOption == SearchOption.TopDirectoryOnly)
			{
				return this.GetFiles(searchPattern);
			}
			if (searchOption != SearchOption.AllDirectories)
			{
				string text = Locale.GetText("Invalid enum value '{0}' for '{1}'.", new object[]
				{
					searchOption,
					"SearchOption"
				});
				throw new ArgumentOutOfRangeException("searchOption", text);
			}
			ArrayList arrayList = new ArrayList();
			int filesSubdirs = this.GetFilesSubdirs(arrayList, searchPattern);
			int num = 0;
			FileInfo[] array = new FileInfo[filesSubdirs];
			foreach (object obj in arrayList)
			{
				FileInfo[] array2 = (FileInfo[])obj;
				array2.CopyTo(array, num);
				num += array2.Length;
			}
			return array;
		}

		/// <summary>Creates a directory using a <see cref="T:System.Security.AccessControl.DirectorySecurity" /> object.</summary>
		/// <param name="directorySecurity">The access control to apply to the directory.</param>
		/// <exception cref="T:System.IO.IOException">The directory specified by <paramref name="path" /> is read-only or is not empty. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> is a zero-length string, contains only white space, or contains one or more invalid characters as defined by <see cref="F:System.IO.Path.InvalidPathChars" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.NotSupportedException">Creating a directory with only the colon (:) character was attempted. </exception>
		/// <exception cref="T:System.IO.IOException">The directory specified by <paramref name="path" /> is read-only or is not empty. </exception>
		// Token: 0x060029FB RID: 10747 RVA: 0x00093D58 File Offset: 0x00091F58
		[MonoLimitation("DirectorySecurity isn't implemented")]
		public void Create(DirectorySecurity directorySecurity)
		{
			if (directorySecurity != null)
			{
				throw new UnauthorizedAccessException();
			}
			this.Create();
		}

		/// <summary>Creates a subdirectory or subdirectories on the specified path with the specified security. The specified path can be relative to this instance of the <see cref="T:System.IO.DirectoryInfo" /> class.</summary>
		/// <param name="path">The specified path. This cannot be a different disk volume or Universal Naming Convention (UNC) name.</param>
		/// <param name="directorySecurity">The security to apply.</param>
		/// <returns>The last directory specified in <paramref name="path" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="path" /> does not specify a valid file path or contains invalid <see langword="DirectoryInfo" /> characters. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="path" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive. </exception>
		/// <exception cref="T:System.IO.IOException">The subdirectory cannot be created.-or- A file or directory already has the name specified by <paramref name="path" />. </exception>
		/// <exception cref="T:System.IO.PathTooLongException">The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters. The specified path, file name, or both are too long.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have code access permission to create the directory.-or-The caller does not have code access permission to read the directory described by the returned <see cref="T:System.IO.DirectoryInfo" /> object.  This can occur when the <paramref name="path" /> parameter describes an existing directory.</exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <paramref name="path" /> contains a colon character (:) that is not part of a drive label ("C:\").</exception>
		// Token: 0x060029FC RID: 10748 RVA: 0x00093D69 File Offset: 0x00091F69
		[MonoLimitation("DirectorySecurity isn't implemented")]
		public DirectoryInfo CreateSubdirectory(string path, DirectorySecurity directorySecurity)
		{
			if (directorySecurity != null)
			{
				throw new UnauthorizedAccessException();
			}
			return this.CreateSubdirectory(path);
		}

		/// <summary>Gets a <see cref="T:System.Security.AccessControl.DirectorySecurity" /> object that encapsulates the access control list (ACL) entries for the directory described by the current <see cref="T:System.IO.DirectoryInfo" /> object.</summary>
		/// <returns>A <see cref="T:System.Security.AccessControl.DirectorySecurity" /> object that encapsulates the access control rules for the directory.</returns>
		/// <exception cref="T:System.SystemException">The directory could not be found or modified.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The current process does not have access to open the directory.</exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred while opening the directory.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Microsoft Windows 2000 or later.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The directory is read-only.-or- This operation is not supported on the current platform.-or- The caller does not have the required permission.</exception>
		// Token: 0x060029FD RID: 10749 RVA: 0x00093D7B File Offset: 0x00091F7B
		public DirectorySecurity GetAccessControl()
		{
			return Directory.GetAccessControl(this.FullPath);
		}

		/// <summary>Gets a <see cref="T:System.Security.AccessControl.DirectorySecurity" /> object that encapsulates the specified type of access control list (ACL) entries for the directory described by the current <see cref="T:System.IO.DirectoryInfo" /> object.</summary>
		/// <param name="includeSections">One of the <see cref="T:System.Security.AccessControl.AccessControlSections" /> values that specifies the type of access control list (ACL) information to receive.</param>
		/// <returns>A <see cref="T:System.Security.AccessControl.DirectorySecurity" /> object that encapsulates the access control rules for the file described by the <paramref name="path" /> parameter.ExceptionsException typeCondition
		///             <see cref="T:System.SystemException" />
		///           The directory could not be found or modified.
		///             <see cref="T:System.UnauthorizedAccessException" />
		///           The current process does not have access to open the directory.
		///             <see cref="T:System.IO.IOException" />
		///           An I/O error occurred while opening the directory.
		///             <see cref="T:System.PlatformNotSupportedException" />
		///           The current operating system is not Microsoft Windows 2000 or later.
		///             <see cref="T:System.UnauthorizedAccessException" />
		///           The directory is read-only.-or- This operation is not supported on the current platform.-or- The caller does not have the required permission.</returns>
		// Token: 0x060029FE RID: 10750 RVA: 0x00093D88 File Offset: 0x00091F88
		public DirectorySecurity GetAccessControl(AccessControlSections includeSections)
		{
			return Directory.GetAccessControl(this.FullPath, includeSections);
		}

		/// <summary>Applies access control list (ACL) entries described by a <see cref="T:System.Security.AccessControl.DirectorySecurity" /> object to the directory described by the current <see cref="T:System.IO.DirectoryInfo" /> object.</summary>
		/// <param name="directorySecurity">An object that describes an ACL entry to apply to the directory described by the <paramref name="path" /> parameter.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="directorySecurity" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.SystemException">The file could not be found or modified.</exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The current process does not have access to open the file.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current operating system is not Microsoft Windows 2000 or later.</exception>
		// Token: 0x060029FF RID: 10751 RVA: 0x00093D96 File Offset: 0x00091F96
		public void SetAccessControl(DirectorySecurity directorySecurity)
		{
			Directory.SetAccessControl(this.FullPath, directorySecurity);
		}

		/// <summary>Returns an enumerable collection of directory information in the current directory.</summary>
		/// <returns>An enumerable collection of directories in the current directory.</returns>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A00 RID: 10752 RVA: 0x00093DA4 File Offset: 0x00091FA4
		public IEnumerable<DirectoryInfo> EnumerateDirectories()
		{
			return this.EnumerateDirectories("*", SearchOption.TopDirectoryOnly);
		}

		/// <summary>Returns an enumerable collection of directory information that matches a specified search pattern.</summary>
		/// <param name="searchPattern">The search string to match against the names of directories.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <returns>An enumerable collection of directories that matches <paramref name="searchPattern" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A01 RID: 10753 RVA: 0x00093DB2 File Offset: 0x00091FB2
		public IEnumerable<DirectoryInfo> EnumerateDirectories(string searchPattern)
		{
			return this.EnumerateDirectories(searchPattern, SearchOption.TopDirectoryOnly);
		}

		/// <summary>Returns an enumerable collection of directory information that matches a specified search pattern and search subdirectory option. </summary>
		/// <param name="searchPattern">The search string to match against the names of directories.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <param name="searchOption">One of the enumeration values that specifies whether the search operation should include only the current directory or all subdirectories. The default value is <see cref="F:System.IO.SearchOption.TopDirectoryOnly" />.</param>
		/// <returns>An enumerable collection of directories that matches <paramref name="searchPattern" /> and <paramref name="searchOption" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="searchOption" /> is not a valid <see cref="T:System.IO.SearchOption" /> value.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A02 RID: 10754 RVA: 0x00093DBC File Offset: 0x00091FBC
		public IEnumerable<DirectoryInfo> EnumerateDirectories(string searchPattern, SearchOption searchOption)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			return this.CreateEnumerateDirectoriesIterator(searchPattern, searchOption);
		}

		// Token: 0x06002A03 RID: 10755 RVA: 0x00093DD4 File Offset: 0x00091FD4
		private IEnumerable<DirectoryInfo> CreateEnumerateDirectoriesIterator(string searchPattern, SearchOption searchOption)
		{
			foreach (string path in Directory.EnumerateDirectories(this.FullPath, searchPattern, searchOption))
			{
				yield return new DirectoryInfo(path);
			}
			IEnumerator<string> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Returns an enumerable collection of file information in the current directory.</summary>
		/// <returns>An enumerable collection of the files in the current directory.</returns>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A04 RID: 10756 RVA: 0x00093DF2 File Offset: 0x00091FF2
		public IEnumerable<FileInfo> EnumerateFiles()
		{
			return this.EnumerateFiles("*", SearchOption.TopDirectoryOnly);
		}

		/// <summary>Returns an enumerable collection of file information that matches a search pattern.</summary>
		/// <param name="searchPattern">The search string to match against the names of files.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <returns>An enumerable collection of files that matches <paramref name="searchPattern" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid, (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A05 RID: 10757 RVA: 0x00093E00 File Offset: 0x00092000
		public IEnumerable<FileInfo> EnumerateFiles(string searchPattern)
		{
			return this.EnumerateFiles(searchPattern, SearchOption.TopDirectoryOnly);
		}

		/// <summary>Returns an enumerable collection of file information that matches a specified search pattern and search subdirectory option.</summary>
		/// <param name="searchPattern">The search string to match against the names of files.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <param name="searchOption">One of the enumeration values that specifies whether the search operation should include only the current directory or all subdirectories. The default value is <see cref="F:System.IO.SearchOption.TopDirectoryOnly" />.</param>
		/// <returns>An enumerable collection of files that matches <paramref name="searchPattern" /> and <paramref name="searchOption" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="searchOption" /> is not a valid <see cref="T:System.IO.SearchOption" /> value.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A06 RID: 10758 RVA: 0x00093E0A File Offset: 0x0009200A
		public IEnumerable<FileInfo> EnumerateFiles(string searchPattern, SearchOption searchOption)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			return this.CreateEnumerateFilesIterator(searchPattern, searchOption);
		}

		// Token: 0x06002A07 RID: 10759 RVA: 0x00093E22 File Offset: 0x00092022
		private IEnumerable<FileInfo> CreateEnumerateFilesIterator(string searchPattern, SearchOption searchOption)
		{
			foreach (string fileName in Directory.EnumerateFiles(this.FullPath, searchPattern, searchOption))
			{
				yield return new FileInfo(fileName);
			}
			IEnumerator<string> enumerator = null;
			yield break;
			yield break;
		}

		/// <summary>Returns an enumerable collection of file system information in the current directory.</summary>
		/// <returns>An enumerable collection of file system information in the current directory. </returns>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A08 RID: 10760 RVA: 0x00093E40 File Offset: 0x00092040
		public IEnumerable<FileSystemInfo> EnumerateFileSystemInfos()
		{
			return this.EnumerateFileSystemInfos("*", SearchOption.TopDirectoryOnly);
		}

		/// <summary>Returns an enumerable collection of file system information that matches a specified search pattern.</summary>
		/// <param name="searchPattern">The search string to match against the names of directories.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <returns>An enumerable collection of file system information objects that matches <paramref name="searchPattern" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A09 RID: 10761 RVA: 0x00093E4E File Offset: 0x0009204E
		public IEnumerable<FileSystemInfo> EnumerateFileSystemInfos(string searchPattern)
		{
			return this.EnumerateFileSystemInfos(searchPattern, SearchOption.TopDirectoryOnly);
		}

		/// <summary>Returns an enumerable collection of file system information that matches a specified search pattern and search subdirectory option.</summary>
		/// <param name="searchPattern">The search string to match against the names of directories.  This parameter can contain a combination of valid literal path and wildcard (* and ?) characters (see Remarks), but doesn't support regular expressions. The default pattern is "*", which returns all files.</param>
		/// <param name="searchOption">One of the enumeration values that specifies whether the search operation should include only the current directory or all subdirectories. The default value is <see cref="F:System.IO.SearchOption.TopDirectoryOnly" />.</param>
		/// <returns>An enumerable collection of file system information objects that matches <paramref name="searchPattern" /> and <paramref name="searchOption" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="searchPattern" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="searchOption" /> is not a valid <see cref="T:System.IO.SearchOption" /> value.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">The path encapsulated in the <see cref="T:System.IO.DirectoryInfo" /> object is invalid (for example, it is on an unmapped drive). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002A0A RID: 10762 RVA: 0x00093E58 File Offset: 0x00092058
		public IEnumerable<FileSystemInfo> EnumerateFileSystemInfos(string searchPattern, SearchOption searchOption)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			if (searchOption != SearchOption.TopDirectoryOnly && searchOption != SearchOption.AllDirectories)
			{
				throw new ArgumentOutOfRangeException("searchoption");
			}
			return DirectoryInfo.EnumerateFileSystemInfos(this.FullPath, searchPattern, searchOption);
		}

		// Token: 0x06002A0B RID: 10763 RVA: 0x00093E87 File Offset: 0x00092087
		internal static IEnumerable<FileSystemInfo> EnumerateFileSystemInfos(string basePath, string searchPattern, SearchOption searchOption)
		{
			Path.Validate(basePath);
			SafeFindHandle findHandle = null;
			try
			{
				string text = Path.Combine(basePath, searchPattern);
				string text2;
				int num;
				int num2;
				try
				{
				}
				finally
				{
					findHandle = new SafeFindHandle(MonoIO.FindFirstFile(text, out text2, out num, out num2));
				}
				if (!findHandle.IsInvalid)
				{
					while (text2 != null)
					{
						if (!(text2 == ".") && !(text2 == ".."))
						{
							FileAttributes attrs = (FileAttributes)num;
							string fullPath = Path.Combine(basePath, text2);
							if ((attrs & FileAttributes.ReparsePoint) == (FileAttributes)0)
							{
								if ((attrs & FileAttributes.Directory) != (FileAttributes)0)
								{
									yield return new DirectoryInfo(fullPath);
								}
								else
								{
									yield return new FileInfo(fullPath);
								}
							}
							if ((attrs & FileAttributes.Directory) != (FileAttributes)0 && searchOption == SearchOption.AllDirectories)
							{
								foreach (FileSystemInfo fileSystemInfo in DirectoryInfo.EnumerateFileSystemInfos(fullPath, searchPattern, searchOption))
								{
									yield return fileSystemInfo;
								}
								IEnumerator<FileSystemInfo> enumerator = null;
							}
							fullPath = null;
						}
						int num3;
						if (!MonoIO.FindNextFile(findHandle.DangerousGetHandle(), out text2, out num, out num3))
						{
							goto JumpOutOfTryFinally-3;
						}
					}
					yield break;
				}
				MonoIOError monoIOError = (MonoIOError)num2;
				if (monoIOError != MonoIOError.ERROR_FILE_NOT_FOUND)
				{
					throw MonoIO.GetException(Path.GetDirectoryName(text), monoIOError);
				}
				yield break;
			}
			finally
			{
				if (findHandle != null)
				{
					findHandle.Dispose();
				}
			}
			JumpOutOfTryFinally-3:
			yield break;
			yield break;
		}

		// Token: 0x06002A0C RID: 10764 RVA: 0x00093EA8 File Offset: 0x000920A8
		internal void CheckPath(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.");
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Illegal characters in path.");
			}
			if (Environment.IsRunningOnWindows)
			{
				int num = path.IndexOf(':');
				if (num >= 0 && num != 1)
				{
					throw new ArgumentException("path");
				}
			}
		}

		// Token: 0x040015EE RID: 5614
		private string current;

		// Token: 0x040015EF RID: 5615
		private string parent;

		// Token: 0x02000393 RID: 915
		[CompilerGenerated]
		private sealed class <>c__DisplayClass23_0
		{
			// Token: 0x06002A0D RID: 10765 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass23_0()
			{
			}

			// Token: 0x06002A0E RID: 10766 RVA: 0x00093F12 File Offset: 0x00092112
			internal void <InternalGetFileSystemInfos>b__0(string dir)
			{
				this.infos.Add(new DirectoryInfo(dir));
			}

			// Token: 0x06002A0F RID: 10767 RVA: 0x00093F25 File Offset: 0x00092125
			internal void <InternalGetFileSystemInfos>b__1(string file)
			{
				this.infos.Add(new FileInfo(file));
			}

			// Token: 0x040015F0 RID: 5616
			public List<FileSystemInfo> infos;
		}

		// Token: 0x02000394 RID: 916
		[CompilerGenerated]
		private sealed class <CreateEnumerateDirectoriesIterator>d__39 : IEnumerable<DirectoryInfo>, IEnumerable, IEnumerator<DirectoryInfo>, IDisposable, IEnumerator
		{
			// Token: 0x06002A10 RID: 10768 RVA: 0x00093F38 File Offset: 0x00092138
			[DebuggerHidden]
			public <CreateEnumerateDirectoriesIterator>d__39(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06002A11 RID: 10769 RVA: 0x00093F54 File Offset: 0x00092154
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06002A12 RID: 10770 RVA: 0x00093F8C File Offset: 0x0009218C
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					DirectoryInfo directoryInfo = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = Directory.EnumerateDirectories(directoryInfo.FullPath, searchPattern, searchOption).GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						string path = enumerator.Current;
						this.<>2__current = new DirectoryInfo(path);
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06002A13 RID: 10771 RVA: 0x0009404C File Offset: 0x0009224C
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x17000665 RID: 1637
			// (get) Token: 0x06002A14 RID: 10772 RVA: 0x00094068 File Offset: 0x00092268
			DirectoryInfo IEnumerator<DirectoryInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002A15 RID: 10773 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000666 RID: 1638
			// (get) Token: 0x06002A16 RID: 10774 RVA: 0x00094068 File Offset: 0x00092268
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002A17 RID: 10775 RVA: 0x00094070 File Offset: 0x00092270
			[DebuggerHidden]
			IEnumerator<DirectoryInfo> IEnumerable<DirectoryInfo>.GetEnumerator()
			{
				DirectoryInfo.<CreateEnumerateDirectoriesIterator>d__39 <CreateEnumerateDirectoriesIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<CreateEnumerateDirectoriesIterator>d__ = this;
				}
				else
				{
					<CreateEnumerateDirectoriesIterator>d__ = new DirectoryInfo.<CreateEnumerateDirectoriesIterator>d__39(0);
					<CreateEnumerateDirectoriesIterator>d__.<>4__this = this;
				}
				<CreateEnumerateDirectoriesIterator>d__.searchPattern = searchPattern;
				<CreateEnumerateDirectoriesIterator>d__.searchOption = searchOption;
				return <CreateEnumerateDirectoriesIterator>d__;
			}

			// Token: 0x06002A18 RID: 10776 RVA: 0x000940CB File Offset: 0x000922CB
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.IO.DirectoryInfo>.GetEnumerator();
			}

			// Token: 0x040015F1 RID: 5617
			private int <>1__state;

			// Token: 0x040015F2 RID: 5618
			private DirectoryInfo <>2__current;

			// Token: 0x040015F3 RID: 5619
			private int <>l__initialThreadId;

			// Token: 0x040015F4 RID: 5620
			public DirectoryInfo <>4__this;

			// Token: 0x040015F5 RID: 5621
			private string searchPattern;

			// Token: 0x040015F6 RID: 5622
			public string <>3__searchPattern;

			// Token: 0x040015F7 RID: 5623
			private SearchOption searchOption;

			// Token: 0x040015F8 RID: 5624
			public SearchOption <>3__searchOption;

			// Token: 0x040015F9 RID: 5625
			private IEnumerator<string> <>7__wrap1;
		}

		// Token: 0x02000395 RID: 917
		[CompilerGenerated]
		private sealed class <CreateEnumerateFilesIterator>d__43 : IEnumerable<FileInfo>, IEnumerable, IEnumerator<FileInfo>, IDisposable, IEnumerator
		{
			// Token: 0x06002A19 RID: 10777 RVA: 0x000940D3 File Offset: 0x000922D3
			[DebuggerHidden]
			public <CreateEnumerateFilesIterator>d__43(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06002A1A RID: 10778 RVA: 0x000940F0 File Offset: 0x000922F0
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num == -3 || num == 1)
				{
					try
					{
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06002A1B RID: 10779 RVA: 0x00094128 File Offset: 0x00092328
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					int num = this.<>1__state;
					DirectoryInfo directoryInfo = this;
					if (num != 0)
					{
						if (num != 1)
						{
							return false;
						}
						this.<>1__state = -3;
					}
					else
					{
						this.<>1__state = -1;
						enumerator = Directory.EnumerateFiles(directoryInfo.FullPath, searchPattern, searchOption).GetEnumerator();
						this.<>1__state = -3;
					}
					if (!enumerator.MoveNext())
					{
						this.<>m__Finally1();
						enumerator = null;
						result = false;
					}
					else
					{
						string fileName = enumerator.Current;
						this.<>2__current = new FileInfo(fileName);
						this.<>1__state = 1;
						result = true;
					}
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06002A1C RID: 10780 RVA: 0x000941E8 File Offset: 0x000923E8
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x17000667 RID: 1639
			// (get) Token: 0x06002A1D RID: 10781 RVA: 0x00094204 File Offset: 0x00092404
			FileInfo IEnumerator<FileInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002A1E RID: 10782 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000668 RID: 1640
			// (get) Token: 0x06002A1F RID: 10783 RVA: 0x00094204 File Offset: 0x00092404
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002A20 RID: 10784 RVA: 0x0009420C File Offset: 0x0009240C
			[DebuggerHidden]
			IEnumerator<FileInfo> IEnumerable<FileInfo>.GetEnumerator()
			{
				DirectoryInfo.<CreateEnumerateFilesIterator>d__43 <CreateEnumerateFilesIterator>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<CreateEnumerateFilesIterator>d__ = this;
				}
				else
				{
					<CreateEnumerateFilesIterator>d__ = new DirectoryInfo.<CreateEnumerateFilesIterator>d__43(0);
					<CreateEnumerateFilesIterator>d__.<>4__this = this;
				}
				<CreateEnumerateFilesIterator>d__.searchPattern = searchPattern;
				<CreateEnumerateFilesIterator>d__.searchOption = searchOption;
				return <CreateEnumerateFilesIterator>d__;
			}

			// Token: 0x06002A21 RID: 10785 RVA: 0x00094267 File Offset: 0x00092467
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.IO.FileInfo>.GetEnumerator();
			}

			// Token: 0x040015FA RID: 5626
			private int <>1__state;

			// Token: 0x040015FB RID: 5627
			private FileInfo <>2__current;

			// Token: 0x040015FC RID: 5628
			private int <>l__initialThreadId;

			// Token: 0x040015FD RID: 5629
			public DirectoryInfo <>4__this;

			// Token: 0x040015FE RID: 5630
			private string searchPattern;

			// Token: 0x040015FF RID: 5631
			public string <>3__searchPattern;

			// Token: 0x04001600 RID: 5632
			private SearchOption searchOption;

			// Token: 0x04001601 RID: 5633
			public SearchOption <>3__searchOption;

			// Token: 0x04001602 RID: 5634
			private IEnumerator<string> <>7__wrap1;
		}

		// Token: 0x02000396 RID: 918
		[CompilerGenerated]
		private sealed class <EnumerateFileSystemInfos>d__47 : IEnumerable<FileSystemInfo>, IEnumerable, IEnumerator<FileSystemInfo>, IDisposable, IEnumerator
		{
			// Token: 0x06002A22 RID: 10786 RVA: 0x0009426F File Offset: 0x0009246F
			[DebuggerHidden]
			public <EnumerateFileSystemInfos>d__47(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06002A23 RID: 10787 RVA: 0x0009428C File Offset: 0x0009248C
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
				int num = this.<>1__state;
				if (num - -4 <= 1 || num - 1 <= 2)
				{
					try
					{
						if (num == -4 || num == 3)
						{
							try
							{
							}
							finally
							{
								this.<>m__Finally2();
							}
						}
					}
					finally
					{
						this.<>m__Finally1();
					}
				}
			}

			// Token: 0x06002A24 RID: 10788 RVA: 0x000942E8 File Offset: 0x000924E8
			bool IEnumerator.MoveNext()
			{
				bool result;
				try
				{
					string text2;
					int num;
					switch (this.<>1__state)
					{
					case 0:
					{
						this.<>1__state = -1;
						Path.Validate(basePath);
						findHandle = null;
						this.<>1__state = -3;
						string text = Path.Combine(basePath, searchPattern);
						int num2;
						try
						{
						}
						finally
						{
							findHandle = new SafeFindHandle(MonoIO.FindFirstFile(text, out text2, out num, out num2));
						}
						if (findHandle.IsInvalid)
						{
							MonoIOError monoIOError = (MonoIOError)num2;
							if (monoIOError != MonoIOError.ERROR_FILE_NOT_FOUND)
							{
								throw MonoIO.GetException(Path.GetDirectoryName(text), monoIOError);
							}
							result = false;
							goto IL_1F7;
						}
						break;
					}
					case 1:
						this.<>1__state = -3;
						goto IL_14B;
					case 2:
						this.<>1__state = -3;
						goto IL_14B;
					case 3:
						this.<>1__state = -4;
						goto IL_1B3;
					default:
						return false;
					}
					IL_9F:
					if (text2 == null)
					{
						result = false;
						goto IL_1F7;
					}
					if (text2 == "." || text2 == "..")
					{
						goto IL_1D4;
					}
					attrs = (FileAttributes)num;
					fullPath = Path.Combine(basePath, text2);
					if ((attrs & FileAttributes.ReparsePoint) == (FileAttributes)0)
					{
						if ((attrs & FileAttributes.Directory) != (FileAttributes)0)
						{
							this.<>2__current = new DirectoryInfo(fullPath);
							this.<>1__state = 1;
							return true;
						}
						this.<>2__current = new FileInfo(fullPath);
						this.<>1__state = 2;
						return true;
					}
					IL_14B:
					if ((attrs & FileAttributes.Directory) == (FileAttributes)0 || searchOption != SearchOption.AllDirectories)
					{
						goto IL_1CD;
					}
					enumerator = DirectoryInfo.EnumerateFileSystemInfos(fullPath, searchPattern, searchOption).GetEnumerator();
					this.<>1__state = -4;
					IL_1B3:
					if (enumerator.MoveNext())
					{
						FileSystemInfo fileSystemInfo = enumerator.Current;
						this.<>2__current = fileSystemInfo;
						this.<>1__state = 3;
						return true;
					}
					this.<>m__Finally2();
					enumerator = null;
					IL_1CD:
					fullPath = null;
					IL_1D4:
					int num3;
					if (!MonoIO.FindNextFile(findHandle.DangerousGetHandle(), out text2, out num, out num3))
					{
						this.<>m__Finally1();
						return false;
					}
					goto IL_9F;
					IL_1F7:
					this.<>m__Finally1();
				}
				catch
				{
					this.System.IDisposable.Dispose();
					throw;
				}
				return result;
			}

			// Token: 0x06002A25 RID: 10789 RVA: 0x00094534 File Offset: 0x00092734
			private void <>m__Finally1()
			{
				this.<>1__state = -1;
				if (findHandle != null)
				{
					findHandle.Dispose();
				}
			}

			// Token: 0x06002A26 RID: 10790 RVA: 0x00094550 File Offset: 0x00092750
			private void <>m__Finally2()
			{
				this.<>1__state = -3;
				if (enumerator != null)
				{
					enumerator.Dispose();
				}
			}

			// Token: 0x17000669 RID: 1641
			// (get) Token: 0x06002A27 RID: 10791 RVA: 0x0009456D File Offset: 0x0009276D
			FileSystemInfo IEnumerator<FileSystemInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002A28 RID: 10792 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700066A RID: 1642
			// (get) Token: 0x06002A29 RID: 10793 RVA: 0x0009456D File Offset: 0x0009276D
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002A2A RID: 10794 RVA: 0x00094578 File Offset: 0x00092778
			[DebuggerHidden]
			IEnumerator<FileSystemInfo> IEnumerable<FileSystemInfo>.GetEnumerator()
			{
				DirectoryInfo.<EnumerateFileSystemInfos>d__47 <EnumerateFileSystemInfos>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<EnumerateFileSystemInfos>d__ = this;
				}
				else
				{
					<EnumerateFileSystemInfos>d__ = new DirectoryInfo.<EnumerateFileSystemInfos>d__47(0);
				}
				<EnumerateFileSystemInfos>d__.basePath = basePath;
				<EnumerateFileSystemInfos>d__.searchPattern = searchPattern;
				<EnumerateFileSystemInfos>d__.searchOption = searchOption;
				return <EnumerateFileSystemInfos>d__;
			}

			// Token: 0x06002A2B RID: 10795 RVA: 0x000945D3 File Offset: 0x000927D3
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.IO.FileSystemInfo>.GetEnumerator();
			}

			// Token: 0x04001603 RID: 5635
			private int <>1__state;

			// Token: 0x04001604 RID: 5636
			private FileSystemInfo <>2__current;

			// Token: 0x04001605 RID: 5637
			private int <>l__initialThreadId;

			// Token: 0x04001606 RID: 5638
			private string basePath;

			// Token: 0x04001607 RID: 5639
			public string <>3__basePath;

			// Token: 0x04001608 RID: 5640
			private string searchPattern;

			// Token: 0x04001609 RID: 5641
			public string <>3__searchPattern;

			// Token: 0x0400160A RID: 5642
			private string <fullPath>5__1;

			// Token: 0x0400160B RID: 5643
			private FileAttributes <attrs>5__2;

			// Token: 0x0400160C RID: 5644
			private SearchOption searchOption;

			// Token: 0x0400160D RID: 5645
			public SearchOption <>3__searchOption;

			// Token: 0x0400160E RID: 5646
			private SafeFindHandle <findHandle>5__3;

			// Token: 0x0400160F RID: 5647
			private IEnumerator<FileSystemInfo> <>7__wrap1;
		}
	}
}
