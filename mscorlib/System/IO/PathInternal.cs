﻿using System;

namespace System.IO
{
	// Token: 0x02000390 RID: 912
	internal static class PathInternal
	{
		// Token: 0x060029DF RID: 10719 RVA: 0x00002526 File Offset: 0x00000726
		public static bool IsPartiallyQualified(string path)
		{
			return false;
		}

		// Token: 0x060029E0 RID: 10720 RVA: 0x00093782 File Offset: 0x00091982
		public static bool HasIllegalCharacters(string path, bool checkAdditional)
		{
			return path.IndexOfAny(Path.InvalidPathChars) != -1;
		}
	}
}
