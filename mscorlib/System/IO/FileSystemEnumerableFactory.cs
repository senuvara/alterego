﻿using System;
using System.Collections.Generic;

namespace System.IO
{
	// Token: 0x0200035E RID: 862
	internal static class FileSystemEnumerableFactory
	{
		// Token: 0x06002728 RID: 10024 RVA: 0x00089800 File Offset: 0x00087A00
		internal static IEnumerable<string> CreateFileNameIterator(string path, string originalUserPath, string searchPattern, bool includeFiles, bool includeDirs, SearchOption searchOption, bool checkHost)
		{
			SearchResultHandler<string> resultHandler = new StringResultHandler(includeFiles, includeDirs);
			return new FileSystemEnumerableIterator<string>(path, originalUserPath, searchPattern, searchOption, resultHandler, checkHost);
		}

		// Token: 0x06002729 RID: 10025 RVA: 0x00089824 File Offset: 0x00087A24
		internal static IEnumerable<FileInfo> CreateFileInfoIterator(string path, string originalUserPath, string searchPattern, SearchOption searchOption)
		{
			SearchResultHandler<FileInfo> resultHandler = new FileInfoResultHandler();
			return new FileSystemEnumerableIterator<FileInfo>(path, originalUserPath, searchPattern, searchOption, resultHandler, true);
		}

		// Token: 0x0600272A RID: 10026 RVA: 0x00089844 File Offset: 0x00087A44
		internal static IEnumerable<DirectoryInfo> CreateDirectoryInfoIterator(string path, string originalUserPath, string searchPattern, SearchOption searchOption)
		{
			SearchResultHandler<DirectoryInfo> resultHandler = new DirectoryInfoResultHandler();
			return new FileSystemEnumerableIterator<DirectoryInfo>(path, originalUserPath, searchPattern, searchOption, resultHandler, true);
		}

		// Token: 0x0600272B RID: 10027 RVA: 0x00089864 File Offset: 0x00087A64
		internal static IEnumerable<FileSystemInfo> CreateFileSystemInfoIterator(string path, string originalUserPath, string searchPattern, SearchOption searchOption)
		{
			SearchResultHandler<FileSystemInfo> resultHandler = new FileSystemInfoResultHandler();
			return new FileSystemEnumerableIterator<FileSystemInfo>(path, originalUserPath, searchPattern, searchOption, resultHandler, true);
		}
	}
}
