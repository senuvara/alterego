﻿using System;

namespace System.IO
{
	// Token: 0x0200034E RID: 846
	internal static class __HResults
	{
		// Token: 0x04001488 RID: 5256
		public const int COR_E_ENDOFSTREAM = -2147024858;

		// Token: 0x04001489 RID: 5257
		public const int COR_E_FILELOAD = -2146232799;

		// Token: 0x0400148A RID: 5258
		public const int COR_E_FILENOTFOUND = -2147024894;

		// Token: 0x0400148B RID: 5259
		public const int COR_E_DIRECTORYNOTFOUND = -2147024893;

		// Token: 0x0400148C RID: 5260
		public const int COR_E_PATHTOOLONG = -2147024690;

		// Token: 0x0400148D RID: 5261
		public const int COR_E_IO = -2146232800;
	}
}
