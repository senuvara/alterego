﻿using System;
using System.Security;

namespace System.IO
{
	// Token: 0x02000362 RID: 866
	internal class StringResultHandler : SearchResultHandler<string>
	{
		// Token: 0x06002746 RID: 10054 RVA: 0x0008A079 File Offset: 0x00088279
		internal StringResultHandler(bool includeFiles, bool includeDirs)
		{
			this._includeFiles = includeFiles;
			this._includeDirs = includeDirs;
		}

		// Token: 0x06002747 RID: 10055 RVA: 0x0008A090 File Offset: 0x00088290
		[SecurityCritical]
		internal override bool IsResultIncluded(SearchResult result)
		{
			bool flag = this._includeFiles && FileSystemEnumerableHelpers.IsFile(result.FindData);
			bool flag2 = this._includeDirs && FileSystemEnumerableHelpers.IsDir(result.FindData);
			return flag || flag2;
		}

		// Token: 0x06002748 RID: 10056 RVA: 0x0008A0CC File Offset: 0x000882CC
		[SecurityCritical]
		internal override string CreateObject(SearchResult result)
		{
			return result.UserPath;
		}

		// Token: 0x040014F3 RID: 5363
		private bool _includeFiles;

		// Token: 0x040014F4 RID: 5364
		private bool _includeDirs;
	}
}
