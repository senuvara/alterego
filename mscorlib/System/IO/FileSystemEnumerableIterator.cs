﻿using System;
using System.Collections.Generic;
using System.Security;
using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;

namespace System.IO
{
	// Token: 0x02000360 RID: 864
	internal class FileSystemEnumerableIterator<TSource> : Iterator<TSource>
	{
		// Token: 0x06002736 RID: 10038 RVA: 0x00089910 File Offset: 0x00087B10
		[SecuritySafeCritical]
		internal FileSystemEnumerableIterator(string path, string originalUserPath, string searchPattern, SearchOption searchOption, SearchResultHandler<TSource> resultHandler, bool checkHost)
		{
			this.searchStack = new List<Directory.SearchData>();
			string text = FileSystemEnumerableIterator<TSource>.NormalizeSearchPattern(searchPattern);
			if (text.Length == 0)
			{
				this.empty = true;
				return;
			}
			this._resultHandler = resultHandler;
			this.searchOption = searchOption;
			this.fullPath = Path.GetFullPathInternal(path);
			string fullSearchString = FileSystemEnumerableIterator<TSource>.GetFullSearchString(this.fullPath, text);
			this.normalizedSearchPath = Path.GetDirectoryName(fullSearchString);
			string[] array = new string[]
			{
				Directory.GetDemandDir(this.fullPath, true),
				Directory.GetDemandDir(this.normalizedSearchPath, true)
			};
			this._checkHost = checkHost;
			this.searchCriteria = FileSystemEnumerableIterator<TSource>.GetNormalizedSearchCriteria(fullSearchString, this.normalizedSearchPath);
			string directoryName = Path.GetDirectoryName(text);
			string path2 = originalUserPath;
			if (directoryName != null && directoryName.Length != 0)
			{
				path2 = Path.Combine(path2, directoryName);
			}
			this.userPath = path2;
			this.searchData = new Directory.SearchData(this.normalizedSearchPath, this.userPath, searchOption);
			this.CommonInit();
		}

		// Token: 0x06002737 RID: 10039 RVA: 0x000899FC File Offset: 0x00087BFC
		[SecurityCritical]
		private void CommonInit()
		{
			string pathWithPattern = Path.InternalCombine(this.searchData.fullPath, this.searchCriteria);
			Win32Native.WIN32_FIND_DATA win32_FIND_DATA = new Win32Native.WIN32_FIND_DATA();
			int num;
			this._hnd = new SafeFindHandle(MonoIO.FindFirstFile(pathWithPattern, out win32_FIND_DATA.cFileName, out win32_FIND_DATA.dwFileAttributes, out num));
			if (this._hnd.IsInvalid)
			{
				int num2 = num;
				if (num2 != 2 && num2 != 18)
				{
					this.HandleError(num2, this.searchData.fullPath);
				}
				else
				{
					this.empty = (this.searchData.searchOption == SearchOption.TopDirectoryOnly);
				}
			}
			if (this.searchData.searchOption == SearchOption.TopDirectoryOnly)
			{
				if (this.empty)
				{
					this._hnd.Dispose();
					return;
				}
				SearchResult result = this.CreateSearchResult(this.searchData, win32_FIND_DATA);
				if (this._resultHandler.IsResultIncluded(result))
				{
					this.current = this._resultHandler.CreateObject(result);
					return;
				}
			}
			else
			{
				this._hnd.Dispose();
				this.searchStack.Add(this.searchData);
			}
		}

		// Token: 0x06002738 RID: 10040 RVA: 0x00089AF4 File Offset: 0x00087CF4
		[SecuritySafeCritical]
		private FileSystemEnumerableIterator(string fullPath, string normalizedSearchPath, string searchCriteria, string userPath, SearchOption searchOption, SearchResultHandler<TSource> resultHandler, bool checkHost)
		{
			this.fullPath = fullPath;
			this.normalizedSearchPath = normalizedSearchPath;
			this.searchCriteria = searchCriteria;
			this._resultHandler = resultHandler;
			this.userPath = userPath;
			this.searchOption = searchOption;
			this._checkHost = checkHost;
			this.searchStack = new List<Directory.SearchData>();
			if (searchCriteria != null)
			{
				string[] array = new string[]
				{
					Directory.GetDemandDir(fullPath, true),
					Directory.GetDemandDir(normalizedSearchPath, true)
				};
				this.searchData = new Directory.SearchData(normalizedSearchPath, userPath, searchOption);
				this.CommonInit();
				return;
			}
			this.empty = true;
		}

		// Token: 0x06002739 RID: 10041 RVA: 0x00089B81 File Offset: 0x00087D81
		protected override Iterator<TSource> Clone()
		{
			return new FileSystemEnumerableIterator<TSource>(this.fullPath, this.normalizedSearchPath, this.searchCriteria, this.userPath, this.searchOption, this._resultHandler, this._checkHost);
		}

		// Token: 0x0600273A RID: 10042 RVA: 0x00089BB4 File Offset: 0x00087DB4
		[SecuritySafeCritical]
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (this._hnd != null)
				{
					this._hnd.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x0600273B RID: 10043 RVA: 0x00089BF0 File Offset: 0x00087DF0
		[SecuritySafeCritical]
		public override bool MoveNext()
		{
			Win32Native.WIN32_FIND_DATA win32_FIND_DATA = new Win32Native.WIN32_FIND_DATA();
			switch (this.state)
			{
			case 1:
				if (this.empty)
				{
					this.state = 4;
					goto IL_278;
				}
				if (this.searchData.searchOption == SearchOption.TopDirectoryOnly)
				{
					this.state = 3;
					if (this.current != null)
					{
						return true;
					}
					goto IL_192;
				}
				else
				{
					this.state = 2;
				}
				break;
			case 2:
				break;
			case 3:
				goto IL_192;
			case 4:
				goto IL_278;
			default:
				return false;
			}
			IL_175:
			while (this.searchStack.Count > 0)
			{
				this.searchData = this.searchStack[0];
				this.searchStack.RemoveAt(0);
				this.AddSearchableDirsToStack(this.searchData);
				string pathWithPattern = Path.InternalCombine(this.searchData.fullPath, this.searchCriteria);
				int num;
				this._hnd = new SafeFindHandle(MonoIO.FindFirstFile(pathWithPattern, out win32_FIND_DATA.cFileName, out win32_FIND_DATA.dwFileAttributes, out num));
				if (this._hnd.IsInvalid)
				{
					int num2 = num;
					if (num2 == 2 || num2 == 18 || num2 == 3)
					{
						continue;
					}
					this._hnd.Dispose();
					this.HandleError(num2, this.searchData.fullPath);
				}
				this.state = 3;
				this.needsParentPathDiscoveryDemand = true;
				SearchResult result = this.CreateSearchResult(this.searchData, win32_FIND_DATA);
				if (this._resultHandler.IsResultIncluded(result))
				{
					if (this.needsParentPathDiscoveryDemand)
					{
						this.DoDemand(this.searchData.fullPath);
						this.needsParentPathDiscoveryDemand = false;
					}
					this.current = this._resultHandler.CreateObject(result);
					return true;
				}
				goto IL_192;
			}
			this.state = 4;
			goto IL_278;
			IL_192:
			if (this.searchData != null && this._hnd != null)
			{
				int num3;
				while (MonoIO.FindNextFile(this._hnd.DangerousGetHandle(), out win32_FIND_DATA.cFileName, out win32_FIND_DATA.dwFileAttributes, out num3))
				{
					SearchResult result2 = this.CreateSearchResult(this.searchData, win32_FIND_DATA);
					if (this._resultHandler.IsResultIncluded(result2))
					{
						if (this.needsParentPathDiscoveryDemand)
						{
							this.DoDemand(this.searchData.fullPath);
							this.needsParentPathDiscoveryDemand = false;
						}
						this.current = this._resultHandler.CreateObject(result2);
						return true;
					}
				}
				int num4 = num3;
				if (this._hnd != null)
				{
					this._hnd.Dispose();
				}
				if (num4 != 0 && num4 != 18 && num4 != 2)
				{
					this.HandleError(num4, this.searchData.fullPath);
				}
			}
			if (this.searchData.searchOption != SearchOption.TopDirectoryOnly)
			{
				this.state = 2;
				goto IL_175;
			}
			this.state = 4;
			IL_278:
			base.Dispose();
			return false;
		}

		// Token: 0x0600273C RID: 10044 RVA: 0x00089E7C File Offset: 0x0008807C
		[SecurityCritical]
		private SearchResult CreateSearchResult(Directory.SearchData localSearchData, Win32Native.WIN32_FIND_DATA findData)
		{
			string text = Path.InternalCombine(localSearchData.userPath, findData.cFileName);
			return new SearchResult(Path.InternalCombine(localSearchData.fullPath, findData.cFileName), text, findData);
		}

		// Token: 0x0600273D RID: 10045 RVA: 0x00089EB3 File Offset: 0x000880B3
		[SecurityCritical]
		private void HandleError(int hr, string path)
		{
			base.Dispose();
			__Error.WinIOError(hr, path);
		}

		// Token: 0x0600273E RID: 10046 RVA: 0x00089EC4 File Offset: 0x000880C4
		[SecurityCritical]
		private void AddSearchableDirsToStack(Directory.SearchData localSearchData)
		{
			string pathWithPattern = Path.InternalCombine(localSearchData.fullPath, "*");
			SafeFindHandle safeFindHandle = null;
			Win32Native.WIN32_FIND_DATA win32_FIND_DATA = new Win32Native.WIN32_FIND_DATA();
			try
			{
				int num;
				safeFindHandle = new SafeFindHandle(MonoIO.FindFirstFile(pathWithPattern, out win32_FIND_DATA.cFileName, out win32_FIND_DATA.dwFileAttributes, out num));
				if (safeFindHandle.IsInvalid)
				{
					int num2 = num;
					if (num2 == 2 || num2 == 18 || num2 == 3)
					{
						return;
					}
					this.HandleError(num2, localSearchData.fullPath);
				}
				int num3 = 0;
				do
				{
					if (FileSystemEnumerableHelpers.IsDir(win32_FIND_DATA))
					{
						string text = Path.InternalCombine(localSearchData.fullPath, win32_FIND_DATA.cFileName);
						string text2 = Path.InternalCombine(localSearchData.userPath, win32_FIND_DATA.cFileName);
						SearchOption searchOption = localSearchData.searchOption;
						Directory.SearchData item = new Directory.SearchData(text, text2, searchOption);
						this.searchStack.Insert(num3++, item);
					}
				}
				while (MonoIO.FindNextFile(safeFindHandle.DangerousGetHandle(), out win32_FIND_DATA.cFileName, out win32_FIND_DATA.dwFileAttributes, out num));
			}
			finally
			{
				if (safeFindHandle != null)
				{
					safeFindHandle.Dispose();
				}
			}
		}

		// Token: 0x0600273F RID: 10047 RVA: 0x000020D3 File Offset: 0x000002D3
		[SecurityCritical]
		internal void DoDemand(string fullPathToDemand)
		{
		}

		// Token: 0x06002740 RID: 10048 RVA: 0x00089FC0 File Offset: 0x000881C0
		private static string NormalizeSearchPattern(string searchPattern)
		{
			string text = searchPattern.TrimEnd(Path.TrimEndChars);
			if (text.Equals("."))
			{
				text = "*";
			}
			Path.CheckSearchPattern(text);
			return text;
		}

		// Token: 0x06002741 RID: 10049 RVA: 0x00089FF4 File Offset: 0x000881F4
		private static string GetNormalizedSearchCriteria(string fullSearchString, string fullPathMod)
		{
			string result;
			if (Path.IsDirectorySeparator(fullPathMod[fullPathMod.Length - 1]))
			{
				result = fullSearchString.Substring(fullPathMod.Length);
			}
			else
			{
				result = fullSearchString.Substring(fullPathMod.Length + 1);
			}
			return result;
		}

		// Token: 0x06002742 RID: 10050 RVA: 0x0008A038 File Offset: 0x00088238
		private static string GetFullSearchString(string fullPath, string searchPattern)
		{
			string text = Path.InternalCombine(fullPath, searchPattern);
			char c = text[text.Length - 1];
			if (Path.IsDirectorySeparator(c) || c == Path.VolumeSeparatorChar)
			{
				text += "*";
			}
			return text;
		}

		// Token: 0x040014E3 RID: 5347
		private const int STATE_INIT = 1;

		// Token: 0x040014E4 RID: 5348
		private const int STATE_SEARCH_NEXT_DIR = 2;

		// Token: 0x040014E5 RID: 5349
		private const int STATE_FIND_NEXT_FILE = 3;

		// Token: 0x040014E6 RID: 5350
		private const int STATE_FINISH = 4;

		// Token: 0x040014E7 RID: 5351
		private SearchResultHandler<TSource> _resultHandler;

		// Token: 0x040014E8 RID: 5352
		private List<Directory.SearchData> searchStack;

		// Token: 0x040014E9 RID: 5353
		private Directory.SearchData searchData;

		// Token: 0x040014EA RID: 5354
		private string searchCriteria;

		// Token: 0x040014EB RID: 5355
		[SecurityCritical]
		private SafeFindHandle _hnd;

		// Token: 0x040014EC RID: 5356
		private bool needsParentPathDiscoveryDemand;

		// Token: 0x040014ED RID: 5357
		private bool empty;

		// Token: 0x040014EE RID: 5358
		private string userPath;

		// Token: 0x040014EF RID: 5359
		private SearchOption searchOption;

		// Token: 0x040014F0 RID: 5360
		private string fullPath;

		// Token: 0x040014F1 RID: 5361
		private string normalizedSearchPath;

		// Token: 0x040014F2 RID: 5362
		private bool _checkHost;
	}
}
