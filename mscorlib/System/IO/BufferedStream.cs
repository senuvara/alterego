﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.IO
{
	/// <summary>Adds a buffering layer to read and write operations on another stream. This class cannot be inherited.</summary>
	// Token: 0x02000351 RID: 849
	[ComVisible(true)]
	public sealed class BufferedStream : Stream
	{
		// Token: 0x0600266D RID: 9837 RVA: 0x00086293 File Offset: 0x00084493
		private BufferedStream()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.BufferedStream" /> class with a default buffer size of 4096 bytes.</summary>
		/// <param name="stream">The current stream. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />. </exception>
		// Token: 0x0600266E RID: 9838 RVA: 0x0008629B File Offset: 0x0008449B
		public BufferedStream(Stream stream) : this(stream, 4096)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IO.BufferedStream" /> class with the specified buffer size.</summary>
		/// <param name="stream">The current stream. </param>
		/// <param name="bufferSize">The buffer size in bytes. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stream" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="bufferSize" /> is negative. </exception>
		// Token: 0x0600266F RID: 9839 RVA: 0x000862AC File Offset: 0x000844AC
		public BufferedStream(Stream stream, int bufferSize)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize", Environment.GetResourceString("'{0}' must be greater than zero.", new object[]
				{
					"bufferSize"
				}));
			}
			this._stream = stream;
			this._bufferSize = bufferSize;
			if (!this._stream.CanRead && !this._stream.CanWrite)
			{
				__Error.StreamIsClosed();
			}
		}

		// Token: 0x06002670 RID: 9840 RVA: 0x00086321 File Offset: 0x00084521
		private void EnsureNotClosed()
		{
			if (this._stream == null)
			{
				__Error.StreamIsClosed();
			}
		}

		// Token: 0x06002671 RID: 9841 RVA: 0x00086330 File Offset: 0x00084530
		private void EnsureCanSeek()
		{
			if (!this._stream.CanSeek)
			{
				__Error.SeekNotSupported();
			}
		}

		// Token: 0x06002672 RID: 9842 RVA: 0x00086344 File Offset: 0x00084544
		private void EnsureCanRead()
		{
			if (!this._stream.CanRead)
			{
				__Error.ReadNotSupported();
			}
		}

		// Token: 0x06002673 RID: 9843 RVA: 0x00086358 File Offset: 0x00084558
		private void EnsureCanWrite()
		{
			if (!this._stream.CanWrite)
			{
				__Error.WriteNotSupported();
			}
		}

		// Token: 0x06002674 RID: 9844 RVA: 0x0008636C File Offset: 0x0008456C
		private void EnsureBeginEndAwaitableAllocated()
		{
			if (this._beginEndAwaitable == null)
			{
				this._beginEndAwaitable = new BeginEndAwaitableAdapter();
			}
		}

		// Token: 0x06002675 RID: 9845 RVA: 0x00086384 File Offset: 0x00084584
		private void EnsureShadowBufferAllocated()
		{
			if (this._buffer.Length != this._bufferSize || this._bufferSize >= 81920)
			{
				return;
			}
			byte[] array = new byte[Math.Min(this._bufferSize + this._bufferSize, 81920)];
			Buffer.InternalBlockCopy(this._buffer, 0, array, 0, this._writePos);
			this._buffer = array;
		}

		// Token: 0x06002676 RID: 9846 RVA: 0x000863E8 File Offset: 0x000845E8
		private void EnsureBufferAllocated()
		{
			if (this._buffer == null)
			{
				this._buffer = new byte[this._bufferSize];
			}
		}

		// Token: 0x170005E6 RID: 1510
		// (get) Token: 0x06002677 RID: 9847 RVA: 0x00086403 File Offset: 0x00084603
		internal Stream UnderlyingStream
		{
			[FriendAccessAllowed]
			get
			{
				return this._stream;
			}
		}

		// Token: 0x170005E7 RID: 1511
		// (get) Token: 0x06002678 RID: 9848 RVA: 0x0008640B File Offset: 0x0008460B
		internal int BufferSize
		{
			[FriendAccessAllowed]
			get
			{
				return this._bufferSize;
			}
		}

		/// <summary>Gets a value indicating whether the current stream supports reading.</summary>
		/// <returns>
		///     <see langword="true" /> if the stream supports reading; <see langword="false" /> if the stream is closed or was opened with write-only access.</returns>
		// Token: 0x170005E8 RID: 1512
		// (get) Token: 0x06002679 RID: 9849 RVA: 0x00086413 File Offset: 0x00084613
		public override bool CanRead
		{
			get
			{
				return this._stream != null && this._stream.CanRead;
			}
		}

		/// <summary>Gets a value indicating whether the current stream supports writing.</summary>
		/// <returns>
		///     <see langword="true" /> if the stream supports writing; <see langword="false" /> if the stream is closed or was opened with read-only access.</returns>
		// Token: 0x170005E9 RID: 1513
		// (get) Token: 0x0600267A RID: 9850 RVA: 0x0008642A File Offset: 0x0008462A
		public override bool CanWrite
		{
			get
			{
				return this._stream != null && this._stream.CanWrite;
			}
		}

		/// <summary>Gets a value indicating whether the current stream supports seeking.</summary>
		/// <returns>
		///     <see langword="true" /> if the stream supports seeking; <see langword="false" /> if the stream is closed or if the stream was constructed from an operating system handle such as a pipe or output to the console.</returns>
		// Token: 0x170005EA RID: 1514
		// (get) Token: 0x0600267B RID: 9851 RVA: 0x00086441 File Offset: 0x00084641
		public override bool CanSeek
		{
			get
			{
				return this._stream != null && this._stream.CanSeek;
			}
		}

		/// <summary>Gets the stream length in bytes.</summary>
		/// <returns>The stream length in bytes.</returns>
		/// <exception cref="T:System.IO.IOException">The underlying stream is <see langword="null" /> or closed. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support seeking. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		// Token: 0x170005EB RID: 1515
		// (get) Token: 0x0600267C RID: 9852 RVA: 0x00086458 File Offset: 0x00084658
		public override long Length
		{
			get
			{
				this.EnsureNotClosed();
				if (this._writePos > 0)
				{
					this.FlushWrite();
				}
				return this._stream.Length;
			}
		}

		/// <summary>Gets the position within the current stream.</summary>
		/// <returns>The position within the current stream.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value passed to <see cref="M:System.IO.BufferedStream.Seek(System.Int64,System.IO.SeekOrigin)" /> is negative. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs, such as the stream being closed. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support seeking. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		// Token: 0x170005EC RID: 1516
		// (get) Token: 0x0600267D RID: 9853 RVA: 0x0008647A File Offset: 0x0008467A
		// (set) Token: 0x0600267E RID: 9854 RVA: 0x000864AC File Offset: 0x000846AC
		public override long Position
		{
			get
			{
				this.EnsureNotClosed();
				this.EnsureCanSeek();
				return this._stream.Position + (long)(this._readPos - this._readLen + this._writePos);
			}
			set
			{
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("Non-negative number required."));
				}
				this.EnsureNotClosed();
				this.EnsureCanSeek();
				if (this._writePos > 0)
				{
					this.FlushWrite();
				}
				this._readPos = 0;
				this._readLen = 0;
				this._stream.Seek(value, SeekOrigin.Begin);
			}
		}

		// Token: 0x0600267F RID: 9855 RVA: 0x0008650C File Offset: 0x0008470C
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing && this._stream != null)
				{
					try
					{
						this.Flush();
					}
					finally
					{
						this._stream.Close();
					}
				}
			}
			finally
			{
				this._stream = null;
				this._buffer = null;
				this._lastSyncCompletedReadTask = null;
				base.Dispose(disposing);
			}
		}

		/// <summary>Clears all buffers for this stream and causes any buffered data to be written to the underlying device.</summary>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.IO.IOException">The data source or repository is not open. </exception>
		// Token: 0x06002680 RID: 9856 RVA: 0x00086574 File Offset: 0x00084774
		public override void Flush()
		{
			this.EnsureNotClosed();
			if (this._writePos > 0)
			{
				this.FlushWrite();
				return;
			}
			if (this._readPos >= this._readLen)
			{
				if (this._stream.CanWrite || this._stream is BufferedStream)
				{
					this._stream.Flush();
				}
				this._writePos = (this._readPos = (this._readLen = 0));
				return;
			}
			if (!this._stream.CanSeek)
			{
				return;
			}
			this.FlushRead();
			if (this._stream.CanWrite || this._stream is BufferedStream)
			{
				this._stream.Flush();
			}
		}

		/// <summary>Asynchronously clears all buffers for this stream, causes any buffered data to be written to the underlying device, and monitors cancellation requests.</summary>
		/// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
		/// <returns>A task that represents the asynchronous flush operation.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		// Token: 0x06002681 RID: 9857 RVA: 0x0008661D File Offset: 0x0008481D
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			if (cancellationToken.IsCancellationRequested)
			{
				return Task.FromCancellation<int>(cancellationToken);
			}
			this.EnsureNotClosed();
			return BufferedStream.FlushAsyncInternal(cancellationToken, this, this._stream, this._writePos, this._readPos, this._readLen);
		}

		// Token: 0x06002682 RID: 9858 RVA: 0x00086654 File Offset: 0x00084854
		private static async Task FlushAsyncInternal(CancellationToken cancellationToken, BufferedStream _this, Stream stream, int writePos, int readPos, int readLen)
		{
			SemaphoreSlim sem = _this.EnsureAsyncActiveSemaphoreInitialized();
			await sem.WaitAsync().ConfigureAwait(false);
			try
			{
				if (writePos > 0)
				{
					await _this.FlushWriteAsync(cancellationToken).ConfigureAwait(false);
				}
				else if (readPos < readLen)
				{
					if (stream.CanSeek)
					{
						_this.FlushRead();
						if (stream.CanRead || stream is BufferedStream)
						{
							await stream.FlushAsync(cancellationToken).ConfigureAwait(false);
						}
					}
				}
				else if (stream.CanWrite || stream is BufferedStream)
				{
					await stream.FlushAsync(cancellationToken).ConfigureAwait(false);
				}
			}
			finally
			{
				sem.Release();
			}
		}

		// Token: 0x06002683 RID: 9859 RVA: 0x000866C3 File Offset: 0x000848C3
		private void FlushRead()
		{
			if (this._readPos - this._readLen != 0)
			{
				this._stream.Seek((long)(this._readPos - this._readLen), SeekOrigin.Current);
			}
			this._readPos = 0;
			this._readLen = 0;
		}

		// Token: 0x06002684 RID: 9860 RVA: 0x00086700 File Offset: 0x00084900
		private void ClearReadBufferBeforeWrite()
		{
			if (this._readPos == this._readLen)
			{
				this._readPos = (this._readLen = 0);
				return;
			}
			if (!this._stream.CanSeek)
			{
				throw new NotSupportedException(Environment.GetResourceString("Cannot write to a BufferedStream while the read buffer is not empty if the underlying stream is not seekable. Ensure that the stream underlying this BufferedStream can seek or avoid interleaving read and write operations on this BufferedStream."));
			}
			this.FlushRead();
		}

		// Token: 0x06002685 RID: 9861 RVA: 0x0008674F File Offset: 0x0008494F
		private void FlushWrite()
		{
			this._stream.Write(this._buffer, 0, this._writePos);
			this._writePos = 0;
			this._stream.Flush();
		}

		// Token: 0x06002686 RID: 9862 RVA: 0x0008677C File Offset: 0x0008497C
		private async Task FlushWriteAsync(CancellationToken cancellationToken)
		{
			await this._stream.WriteAsync(this._buffer, 0, this._writePos, cancellationToken).ConfigureAwait(false);
			this._writePos = 0;
			await this._stream.FlushAsync(cancellationToken).ConfigureAwait(false);
		}

		// Token: 0x06002687 RID: 9863 RVA: 0x000867CC File Offset: 0x000849CC
		private int ReadFromBuffer(byte[] array, int offset, int count)
		{
			int num = this._readLen - this._readPos;
			if (num == 0)
			{
				return 0;
			}
			if (num > count)
			{
				num = count;
			}
			Buffer.InternalBlockCopy(this._buffer, this._readPos, array, offset, num);
			this._readPos += num;
			return num;
		}

		// Token: 0x06002688 RID: 9864 RVA: 0x00086818 File Offset: 0x00084A18
		private int ReadFromBuffer(byte[] array, int offset, int count, out Exception error)
		{
			int result;
			try
			{
				error = null;
				result = this.ReadFromBuffer(array, offset, count);
			}
			catch (Exception ex)
			{
				error = ex;
				result = 0;
			}
			return result;
		}

		/// <summary>Copies bytes from the current buffered stream to an array.</summary>
		/// <param name="array">The buffer to which bytes are to be copied. </param>
		/// <param name="offset">The byte offset in the buffer at which to begin reading bytes. </param>
		/// <param name="count">The number of bytes to be read. </param>
		/// <returns>The total number of bytes read into <paramref name="array" />. This can be less than the number of bytes requested if that many bytes are not currently available, or 0 if the end of the stream has been reached before any data can be read.</returns>
		/// <exception cref="T:System.ArgumentException">Length of <paramref name="array" /> minus <paramref name="offset" /> is less than <paramref name="count" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is negative. </exception>
		/// <exception cref="T:System.IO.IOException">The stream is not open or is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support reading. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		// Token: 0x06002689 RID: 9865 RVA: 0x00086850 File Offset: 0x00084A50
		public override int Read([In] [Out] byte[] array, int offset, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (array.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			this.EnsureNotClosed();
			this.EnsureCanRead();
			int num = this.ReadFromBuffer(array, offset, count);
			if (num == count)
			{
				return num;
			}
			int num2 = num;
			if (num > 0)
			{
				count -= num;
				offset += num;
			}
			this._readPos = (this._readLen = 0);
			if (this._writePos > 0)
			{
				this.FlushWrite();
			}
			if (count >= this._bufferSize)
			{
				return this._stream.Read(array, offset, count) + num2;
			}
			this.EnsureBufferAllocated();
			this._readLen = this._stream.Read(this._buffer, 0, this._bufferSize);
			num = this.ReadFromBuffer(array, offset, count);
			return num + num2;
		}

		/// <summary>Begins an asynchronous read operation. (Consider using <see cref="M:System.IO.BufferedStream.ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)" /> instead; see the Remarks section.)</summary>
		/// <param name="buffer">The buffer to read the data into.</param>
		/// <param name="offset">The byte offset in <paramref name="buffer" /> at which to begin writing data read from the stream.</param>
		/// <param name="count">The maximum number of bytes to read.</param>
		/// <param name="callback">An optional asynchronous callback, to be called when the read is complete.</param>
		/// <param name="state">A user-provided object that distinguishes this particular asynchronous read request from other requests.</param>
		/// <returns>An object that represents the asynchronous read, which could still be pending.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is negative.</exception>
		/// <exception cref="T:System.IO.IOException">Attempted an asynchronous read past the end of the stream.</exception>
		/// <exception cref="T:System.ArgumentException">The buffer length minus <paramref name="offset" /> is less than <paramref name="count" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The current stream does not support the read operation. </exception>
		// Token: 0x0600268A RID: 9866 RVA: 0x00086958 File Offset: 0x00084B58
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (this._stream == null)
			{
				__Error.ReadNotSupported();
			}
			this.EnsureCanRead();
			int num = 0;
			SemaphoreSlim semaphoreSlim = base.EnsureAsyncActiveSemaphoreInitialized();
			Task task = semaphoreSlim.WaitAsync();
			if (task.Status == TaskStatus.RanToCompletion)
			{
				bool flag = true;
				try
				{
					Exception ex;
					num = this.ReadFromBuffer(buffer, offset, count, out ex);
					flag = (num == count || ex != null);
					if (flag)
					{
						Stream.SynchronousAsyncResult synchronousAsyncResult = (ex == null) ? new Stream.SynchronousAsyncResult(num, state) : new Stream.SynchronousAsyncResult(ex, state, false);
						if (callback != null)
						{
							callback(synchronousAsyncResult);
						}
						return synchronousAsyncResult;
					}
				}
				finally
				{
					if (flag)
					{
						semaphoreSlim.Release();
					}
				}
			}
			return this.BeginReadFromUnderlyingStream(buffer, offset + num, count - num, callback, state, num, task);
		}

		// Token: 0x0600268B RID: 9867 RVA: 0x00086A74 File Offset: 0x00084C74
		private IAsyncResult BeginReadFromUnderlyingStream(byte[] buffer, int offset, int count, AsyncCallback callback, object state, int bytesAlreadySatisfied, Task semaphoreLockTask)
		{
			return TaskToApm.Begin(this.ReadFromUnderlyingStreamAsync(buffer, offset, count, CancellationToken.None, bytesAlreadySatisfied, semaphoreLockTask, true), callback, state);
		}

		/// <summary>Waits for the pending asynchronous read operation to complete. (Consider using <see cref="M:System.IO.BufferedStream.ReadAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)" /> instead; see the Remarks section.)</summary>
		/// <param name="asyncResult">The reference to the pending asynchronous request to wait for.</param>
		/// <returns>The number of bytes read from the stream, between 0 (zero) and the number of bytes you requested. Streams only return 0 only at the end of the stream, otherwise, they should block until at least 1 byte is available.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">This <see cref="T:System.IAsyncResult" /> object was not created by calling <see cref="M:System.IO.BufferedStream.BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)" /> on this class. </exception>
		// Token: 0x0600268C RID: 9868 RVA: 0x00086A92 File Offset: 0x00084C92
		public override int EndRead(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			if (asyncResult is Stream.SynchronousAsyncResult)
			{
				return Stream.SynchronousAsyncResult.EndRead(asyncResult);
			}
			return TaskToApm.End<int>(asyncResult);
		}

		// Token: 0x0600268D RID: 9869 RVA: 0x00086AB8 File Offset: 0x00084CB8
		private Task<int> LastSyncCompletedReadTask(int val)
		{
			Task<int> task = this._lastSyncCompletedReadTask;
			if (task != null && task.Result == val)
			{
				return task;
			}
			task = Task.FromResult<int>(val);
			this._lastSyncCompletedReadTask = task;
			return task;
		}

		/// <summary>Asynchronously reads a sequence of bytes from the current stream, advances the position within the stream by the number of bytes read, and monitors cancellation requests.</summary>
		/// <param name="buffer">The buffer to write the data into.</param>
		/// <param name="offset">The byte offset in <paramref name="buffer" /> at which to begin writing data from the stream.</param>
		/// <param name="count">The maximum number of bytes to read.</param>
		/// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
		/// <returns>A task that represents the asynchronous read operation. The value of the <paramref name="TResult" /> parameter contains the total number of bytes read into the buffer. The result value can be less than the number of bytes requested if the number of bytes currently available is less than the requested number, or it can be 0 (zero) if the end of the stream has been reached. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is negative.</exception>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset" /> and <paramref name="count" /> is larger than the buffer length.</exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is currently in use by a previous read operation. </exception>
		// Token: 0x0600268E RID: 9870 RVA: 0x00086AEC File Offset: 0x00084CEC
		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (cancellationToken.IsCancellationRequested)
			{
				return Task.FromCancellation<int>(cancellationToken);
			}
			this.EnsureNotClosed();
			this.EnsureCanRead();
			int num = 0;
			SemaphoreSlim semaphoreSlim = base.EnsureAsyncActiveSemaphoreInitialized();
			Task task = semaphoreSlim.WaitAsync();
			if (task.Status == TaskStatus.RanToCompletion)
			{
				bool flag = true;
				try
				{
					Exception ex;
					num = this.ReadFromBuffer(buffer, offset, count, out ex);
					flag = (num == count || ex != null);
					if (flag)
					{
						return (ex == null) ? this.LastSyncCompletedReadTask(num) : Task.FromException<int>(ex);
					}
				}
				finally
				{
					if (flag)
					{
						semaphoreSlim.Release();
					}
				}
			}
			return this.ReadFromUnderlyingStreamAsync(buffer, offset + num, count - num, cancellationToken, num, task, false);
		}

		// Token: 0x0600268F RID: 9871 RVA: 0x00086BFC File Offset: 0x00084DFC
		private async Task<int> ReadFromUnderlyingStreamAsync(byte[] array, int offset, int count, CancellationToken cancellationToken, int bytesAlreadySatisfied, Task semaphoreLockTask, bool useApmPattern)
		{
			await semaphoreLockTask.ConfigureAwait(false);
			int result;
			try
			{
				int num = this.ReadFromBuffer(array, offset, count);
				if (num == count)
				{
					result = bytesAlreadySatisfied + num;
				}
				else
				{
					if (num > 0)
					{
						count -= num;
						offset += num;
						bytesAlreadySatisfied += num;
					}
					int num2 = 0;
					this._readLen = num2;
					this._readPos = num2;
					if (this._writePos > 0)
					{
						await this.FlushWriteAsync(cancellationToken).ConfigureAwait(false);
					}
					if (count >= this._bufferSize)
					{
						if (useApmPattern)
						{
							this.EnsureBeginEndAwaitableAllocated();
							this._stream.BeginRead(array, offset, count, BeginEndAwaitableAdapter.Callback, this._beginEndAwaitable);
							int num3 = bytesAlreadySatisfied;
							Stream stream = this._stream;
							result = num3 + stream.EndRead(await this._beginEndAwaitable);
						}
						else
						{
							int num3 = bytesAlreadySatisfied;
							result = num3 + await this._stream.ReadAsync(array, offset, count, cancellationToken).ConfigureAwait(false);
						}
					}
					else
					{
						this.EnsureBufferAllocated();
						if (useApmPattern)
						{
							this.EnsureBeginEndAwaitableAllocated();
							this._stream.BeginRead(this._buffer, 0, this._bufferSize, BeginEndAwaitableAdapter.Callback, this._beginEndAwaitable);
							Stream stream = this._stream;
							this._readLen = stream.EndRead(await this._beginEndAwaitable);
							stream = null;
						}
						else
						{
							this._readLen = await this._stream.ReadAsync(this._buffer, 0, this._bufferSize, cancellationToken).ConfigureAwait(false);
						}
						result = bytesAlreadySatisfied + this.ReadFromBuffer(array, offset, count);
					}
				}
			}
			finally
			{
				base.EnsureAsyncActiveSemaphoreInitialized().Release();
			}
			return result;
		}

		/// <summary>Reads a byte from the underlying stream and returns the byte cast to an <see langword="int" />, or returns -1 if reading from the end of the stream.</summary>
		/// <returns>The byte cast to an <see langword="int" />, or -1 if reading from the end of the stream.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs, such as the stream being closed. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support reading. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		// Token: 0x06002690 RID: 9872 RVA: 0x00086C80 File Offset: 0x00084E80
		public override int ReadByte()
		{
			this.EnsureNotClosed();
			this.EnsureCanRead();
			if (this._readPos == this._readLen)
			{
				if (this._writePos > 0)
				{
					this.FlushWrite();
				}
				this.EnsureBufferAllocated();
				this._readLen = this._stream.Read(this._buffer, 0, this._bufferSize);
				this._readPos = 0;
			}
			if (this._readPos == this._readLen)
			{
				return -1;
			}
			byte[] buffer = this._buffer;
			int readPos = this._readPos;
			this._readPos = readPos + 1;
			return buffer[readPos];
		}

		// Token: 0x06002691 RID: 9873 RVA: 0x00086D0C File Offset: 0x00084F0C
		private void WriteToBuffer(byte[] array, ref int offset, ref int count)
		{
			int num = Math.Min(this._bufferSize - this._writePos, count);
			if (num <= 0)
			{
				return;
			}
			this.EnsureBufferAllocated();
			Buffer.InternalBlockCopy(array, offset, this._buffer, this._writePos, num);
			this._writePos += num;
			count -= num;
			offset += num;
		}

		// Token: 0x06002692 RID: 9874 RVA: 0x00086D6C File Offset: 0x00084F6C
		private void WriteToBuffer(byte[] array, ref int offset, ref int count, out Exception error)
		{
			try
			{
				error = null;
				this.WriteToBuffer(array, ref offset, ref count);
			}
			catch (Exception ex)
			{
				error = ex;
			}
		}

		/// <summary>Copies bytes to the buffered stream and advances the current position within the buffered stream by the number of bytes written.</summary>
		/// <param name="array">The byte array from which to copy <paramref name="count" /> bytes to the current buffered stream. </param>
		/// <param name="offset">The offset in the buffer at which to begin copying bytes to the current buffered stream. </param>
		/// <param name="count">The number of bytes to be written to the current buffered stream. </param>
		/// <exception cref="T:System.ArgumentException">Length of <paramref name="array" /> minus <paramref name="offset" /> is less than <paramref name="count" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is negative. </exception>
		/// <exception cref="T:System.IO.IOException">The stream is closed or <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support writing. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		// Token: 0x06002693 RID: 9875 RVA: 0x00086DA0 File Offset: 0x00084FA0
		public override void Write(byte[] array, int offset, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (array.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			this.EnsureNotClosed();
			this.EnsureCanWrite();
			if (this._writePos == 0)
			{
				this.ClearReadBufferBeforeWrite();
			}
			int num = checked(this._writePos + count);
			if (checked(num + count >= this._bufferSize + this._bufferSize))
			{
				if (this._writePos > 0)
				{
					if (num <= this._bufferSize + this._bufferSize && num <= 81920)
					{
						this.EnsureShadowBufferAllocated();
						Buffer.InternalBlockCopy(array, offset, this._buffer, this._writePos, count);
						this._stream.Write(this._buffer, 0, num);
						this._writePos = 0;
						return;
					}
					this._stream.Write(this._buffer, 0, this._writePos);
					this._writePos = 0;
				}
				this._stream.Write(array, offset, count);
				return;
			}
			this.WriteToBuffer(array, ref offset, ref count);
			if (this._writePos < this._bufferSize)
			{
				return;
			}
			this._stream.Write(this._buffer, 0, this._writePos);
			this._writePos = 0;
			this.WriteToBuffer(array, ref offset, ref count);
		}

		/// <summary>Begins an asynchronous write operation. (Consider using <see cref="M:System.IO.BufferedStream.WriteAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)" /> instead; see the Remarks section.)</summary>
		/// <param name="buffer">The buffer containing data to write to the current stream.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> at which to begin copying bytes to the current stream.</param>
		/// <param name="count">The maximum number of bytes to write.</param>
		/// <param name="callback">The method to be called when the asynchronous write operation is completed.</param>
		/// <param name="state">A user-provided object that distinguishes this particular asynchronous write request from other requests.</param>
		/// <returns>An object that references the asynchronous write which could still be pending.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="buffer" /> length minus <paramref name="offset" /> is less than <paramref name="count" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is negative. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support writing. </exception>
		// Token: 0x06002694 RID: 9876 RVA: 0x00086F10 File Offset: 0x00085110
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (this._stream == null)
			{
				__Error.ReadNotSupported();
			}
			this.EnsureCanWrite();
			SemaphoreSlim semaphoreSlim = base.EnsureAsyncActiveSemaphoreInitialized();
			Task task = semaphoreSlim.WaitAsync();
			if (task.Status == TaskStatus.RanToCompletion)
			{
				bool flag = true;
				try
				{
					if (this._writePos == 0)
					{
						this.ClearReadBufferBeforeWrite();
					}
					flag = (count < this._bufferSize - this._writePos);
					if (flag)
					{
						Exception ex;
						this.WriteToBuffer(buffer, ref offset, ref count, out ex);
						Stream.SynchronousAsyncResult synchronousAsyncResult = (ex == null) ? new Stream.SynchronousAsyncResult(state) : new Stream.SynchronousAsyncResult(ex, state, true);
						if (callback != null)
						{
							callback(synchronousAsyncResult);
						}
						return synchronousAsyncResult;
					}
				}
				finally
				{
					if (flag)
					{
						semaphoreSlim.Release();
					}
				}
			}
			return this.BeginWriteToUnderlyingStream(buffer, offset, count, callback, state, task);
		}

		// Token: 0x06002695 RID: 9877 RVA: 0x00087034 File Offset: 0x00085234
		private IAsyncResult BeginWriteToUnderlyingStream(byte[] buffer, int offset, int count, AsyncCallback callback, object state, Task semaphoreLockTask)
		{
			return TaskToApm.Begin(this.WriteToUnderlyingStreamAsync(buffer, offset, count, CancellationToken.None, semaphoreLockTask, true), callback, state);
		}

		/// <summary>Ends an asynchronous write operation and blocks until the I/O operation is complete. (Consider using <see cref="M:System.IO.BufferedStream.WriteAsync(System.Byte[],System.Int32,System.Int32,System.Threading.CancellationToken)" /> instead; see the Remarks section.)</summary>
		/// <param name="asyncResult">The pending asynchronous request.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="asyncResult" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">This <see cref="T:System.IAsyncResult" /> object was not created by calling <see cref="M:System.IO.BufferedStream.BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)" /> on this class. </exception>
		// Token: 0x06002696 RID: 9878 RVA: 0x00087050 File Offset: 0x00085250
		public override void EndWrite(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			if (asyncResult is Stream.SynchronousAsyncResult)
			{
				Stream.SynchronousAsyncResult.EndWrite(asyncResult);
				return;
			}
			TaskToApm.End(asyncResult);
		}

		/// <summary>Asynchronously writes a sequence of bytes to the current stream, advances the current position within this stream by the number of bytes written, and monitors cancellation requests.</summary>
		/// <param name="buffer">The buffer to write data from.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> from which to begin copying bytes to the stream.</param>
		/// <param name="count">The maximum number of bytes to write.</param>
		/// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
		/// <returns>A task that represents the asynchronous write operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is negative.</exception>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset" /> and <paramref name="count" /> is larger than the buffer length.</exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is currently in use by a previous write operation. </exception>
		// Token: 0x06002697 RID: 9879 RVA: 0x00087078 File Offset: 0x00085278
		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer", Environment.GetResourceString("Buffer cannot be null."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (cancellationToken.IsCancellationRequested)
			{
				return Task.FromCancellation<int>(cancellationToken);
			}
			this.EnsureNotClosed();
			this.EnsureCanWrite();
			SemaphoreSlim semaphoreSlim = base.EnsureAsyncActiveSemaphoreInitialized();
			Task task = semaphoreSlim.WaitAsync();
			if (task.Status == TaskStatus.RanToCompletion)
			{
				bool flag = true;
				try
				{
					if (this._writePos == 0)
					{
						this.ClearReadBufferBeforeWrite();
					}
					flag = (count < this._bufferSize - this._writePos);
					if (flag)
					{
						Exception ex;
						this.WriteToBuffer(buffer, ref offset, ref count, out ex);
						return (ex == null) ? Task.CompletedTask : Task.FromException(ex);
					}
				}
				finally
				{
					if (flag)
					{
						semaphoreSlim.Release();
					}
				}
			}
			return this.WriteToUnderlyingStreamAsync(buffer, offset, count, cancellationToken, task, false);
		}

		// Token: 0x06002698 RID: 9880 RVA: 0x00087190 File Offset: 0x00085390
		private async Task WriteToUnderlyingStreamAsync(byte[] array, int offset, int count, CancellationToken cancellationToken, Task semaphoreLockTask, bool useApmPattern)
		{
			await semaphoreLockTask.ConfigureAwait(false);
			try
			{
				if (this._writePos == 0)
				{
					this.ClearReadBufferBeforeWrite();
				}
				int totalUserBytes = checked(this._writePos + count);
				if (checked(totalUserBytes + count < this._bufferSize + this._bufferSize))
				{
					this.WriteToBuffer(array, ref offset, ref count);
					if (this._writePos >= this._bufferSize)
					{
						if (useApmPattern)
						{
							this.EnsureBeginEndAwaitableAllocated();
							this._stream.BeginWrite(this._buffer, 0, this._writePos, BeginEndAwaitableAdapter.Callback, this._beginEndAwaitable);
							Stream stream = this._stream;
							stream.EndWrite(await this._beginEndAwaitable);
							stream = null;
						}
						else
						{
							await this._stream.WriteAsync(this._buffer, 0, this._writePos, cancellationToken).ConfigureAwait(false);
						}
						this._writePos = 0;
						this.WriteToBuffer(array, ref offset, ref count);
					}
				}
				else
				{
					if (this._writePos > 0)
					{
						if (totalUserBytes <= this._bufferSize + this._bufferSize && totalUserBytes <= 81920)
						{
							this.EnsureShadowBufferAllocated();
							Buffer.InternalBlockCopy(array, offset, this._buffer, this._writePos, count);
							if (useApmPattern)
							{
								this.EnsureBeginEndAwaitableAllocated();
								this._stream.BeginWrite(this._buffer, 0, totalUserBytes, BeginEndAwaitableAdapter.Callback, this._beginEndAwaitable);
								Stream stream = this._stream;
								stream.EndWrite(await this._beginEndAwaitable);
								stream = null;
							}
							else
							{
								await this._stream.WriteAsync(this._buffer, 0, totalUserBytes, cancellationToken).ConfigureAwait(false);
							}
							this._writePos = 0;
							return;
						}
						if (useApmPattern)
						{
							this.EnsureBeginEndAwaitableAllocated();
							this._stream.BeginWrite(this._buffer, 0, this._writePos, BeginEndAwaitableAdapter.Callback, this._beginEndAwaitable);
							Stream stream = this._stream;
							stream.EndWrite(await this._beginEndAwaitable);
							stream = null;
						}
						else
						{
							await this._stream.WriteAsync(this._buffer, 0, this._writePos, cancellationToken).ConfigureAwait(false);
						}
						this._writePos = 0;
					}
					if (useApmPattern)
					{
						this.EnsureBeginEndAwaitableAllocated();
						this._stream.BeginWrite(array, offset, count, BeginEndAwaitableAdapter.Callback, this._beginEndAwaitable);
						Stream stream = this._stream;
						stream.EndWrite(await this._beginEndAwaitable);
						stream = null;
					}
					else
					{
						await this._stream.WriteAsync(array, offset, count, cancellationToken).ConfigureAwait(false);
					}
				}
			}
			finally
			{
				base.EnsureAsyncActiveSemaphoreInitialized().Release();
			}
		}

		/// <summary>Writes a byte to the current position in the buffered stream.</summary>
		/// <param name="value">A byte to write to the stream. </param>
		/// <exception cref="T:System.NotSupportedException">The stream does not support writing. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		// Token: 0x06002699 RID: 9881 RVA: 0x00087208 File Offset: 0x00085408
		public override void WriteByte(byte value)
		{
			this.EnsureNotClosed();
			if (this._writePos == 0)
			{
				this.EnsureCanWrite();
				this.ClearReadBufferBeforeWrite();
				this.EnsureBufferAllocated();
			}
			if (this._writePos >= this._bufferSize - 1)
			{
				this.FlushWrite();
			}
			byte[] buffer = this._buffer;
			int writePos = this._writePos;
			this._writePos = writePos + 1;
			buffer[writePos] = value;
		}

		/// <summary>Sets the position within the current buffered stream.</summary>
		/// <param name="offset">A byte offset relative to <paramref name="origin" />. </param>
		/// <param name="origin">A value of type <see cref="T:System.IO.SeekOrigin" /> indicating the reference point from which to obtain the new position. </param>
		/// <returns>The new position within the current buffered stream.</returns>
		/// <exception cref="T:System.IO.IOException">The stream is not open or is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support seeking. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		// Token: 0x0600269A RID: 9882 RVA: 0x00087264 File Offset: 0x00085464
		public override long Seek(long offset, SeekOrigin origin)
		{
			this.EnsureNotClosed();
			this.EnsureCanSeek();
			if (this._writePos > 0)
			{
				this.FlushWrite();
				return this._stream.Seek(offset, origin);
			}
			if (this._readLen - this._readPos > 0 && origin == SeekOrigin.Current)
			{
				offset -= (long)(this._readLen - this._readPos);
			}
			long position = this.Position;
			long num = this._stream.Seek(offset, origin);
			this._readPos = (int)(num - (position - (long)this._readPos));
			if (0 <= this._readPos && this._readPos < this._readLen)
			{
				this._stream.Seek((long)(this._readLen - this._readPos), SeekOrigin.Current);
			}
			else
			{
				this._readPos = (this._readLen = 0);
			}
			return num;
		}

		/// <summary>Sets the length of the buffered stream.</summary>
		/// <param name="value">An integer indicating the desired length of the current buffered stream in bytes. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="value" /> is negative. </exception>
		/// <exception cref="T:System.IO.IOException">The stream is not open or is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support both writing and seeking. </exception>
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
		// Token: 0x0600269B RID: 9883 RVA: 0x0008732C File Offset: 0x0008552C
		public override void SetLength(long value)
		{
			if (value < 0L)
			{
				throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("Length must be non-negative."));
			}
			this.EnsureNotClosed();
			this.EnsureCanSeek();
			this.EnsureCanWrite();
			this.Flush();
			this._stream.SetLength(value);
		}

		// Token: 0x040014A3 RID: 5283
		private const int _DefaultBufferSize = 4096;

		// Token: 0x040014A4 RID: 5284
		private Stream _stream;

		// Token: 0x040014A5 RID: 5285
		private byte[] _buffer;

		// Token: 0x040014A6 RID: 5286
		private readonly int _bufferSize;

		// Token: 0x040014A7 RID: 5287
		private int _readPos;

		// Token: 0x040014A8 RID: 5288
		private int _readLen;

		// Token: 0x040014A9 RID: 5289
		private int _writePos;

		// Token: 0x040014AA RID: 5290
		private BeginEndAwaitableAdapter _beginEndAwaitable;

		// Token: 0x040014AB RID: 5291
		private Task<int> _lastSyncCompletedReadTask;

		// Token: 0x040014AC RID: 5292
		private const int MaxShadowBufferSize = 81920;

		// Token: 0x02000352 RID: 850
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FlushAsyncInternal>d__38 : IAsyncStateMachine
		{
			// Token: 0x0600269C RID: 9884 RVA: 0x0008736C File Offset: 0x0008556C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num - 1 <= 2)
						{
							goto IL_8A;
						}
						sem = _this.EnsureAsyncActiveSemaphoreInitialized();
						configuredTaskAwaiter = sem.WaitAsync().ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<FlushAsyncInternal>d__38>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					configuredTaskAwaiter.GetResult();
					IL_8A:
					try
					{
						switch (num)
						{
						case 1:
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							break;
						case 2:
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_1CC;
						case 3:
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_257;
						default:
							if (writePos > 0)
							{
								configuredTaskAwaiter = _this.FlushWriteAsync(cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 1);
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<FlushAsyncInternal>d__38>(ref configuredTaskAwaiter, ref this);
									return;
								}
							}
							else if (readPos < readLen)
							{
								if (!stream.CanSeek)
								{
									goto IL_28A;
								}
								_this.FlushRead();
								if (!stream.CanRead && !(stream is BufferedStream))
								{
									goto IL_1D3;
								}
								configuredTaskAwaiter = stream.FlushAsync(cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 2);
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<FlushAsyncInternal>d__38>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_1CC;
							}
							else
							{
								if (!stream.CanWrite && !(stream is BufferedStream))
								{
									goto IL_25E;
								}
								configuredTaskAwaiter = stream.FlushAsync(cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 3);
									configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<FlushAsyncInternal>d__38>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_257;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						goto IL_28A;
						IL_1CC:
						configuredTaskAwaiter.GetResult();
						IL_1D3:
						goto IL_28A;
						IL_257:
						configuredTaskAwaiter.GetResult();
						IL_25E:;
					}
					finally
					{
						if (num < 0)
						{
							sem.Release();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_28A:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600269D RID: 9885 RVA: 0x0008764C File Offset: 0x0008584C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040014AD RID: 5293
			public int <>1__state;

			// Token: 0x040014AE RID: 5294
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040014AF RID: 5295
			public BufferedStream _this;

			// Token: 0x040014B0 RID: 5296
			public int writePos;

			// Token: 0x040014B1 RID: 5297
			public CancellationToken cancellationToken;

			// Token: 0x040014B2 RID: 5298
			public int readPos;

			// Token: 0x040014B3 RID: 5299
			public int readLen;

			// Token: 0x040014B4 RID: 5300
			public Stream stream;

			// Token: 0x040014B5 RID: 5301
			private SemaphoreSlim <sem>5__1;

			// Token: 0x040014B6 RID: 5302
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000353 RID: 851
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <FlushWriteAsync>d__42 : IAsyncStateMachine
		{
			// Token: 0x0600269E RID: 9886 RVA: 0x0008765C File Offset: 0x0008585C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				BufferedStream bufferedStream = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					if (num != 0)
					{
						if (num == 1)
						{
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num2 = -1;
							goto IL_100;
						}
						configuredTaskAwaiter = bufferedStream._stream.WriteAsync(bufferedStream._buffer, 0, bufferedStream._writePos, cancellationToken).ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num2 = 0;
							ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<FlushWriteAsync>d__42>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num2 = -1;
					}
					configuredTaskAwaiter.GetResult();
					bufferedStream._writePos = 0;
					configuredTaskAwaiter = bufferedStream._stream.FlushAsync(cancellationToken).ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num2 = 1;
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<FlushWriteAsync>d__42>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_100:
					configuredTaskAwaiter.GetResult();
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600269F RID: 9887 RVA: 0x000877B0 File Offset: 0x000859B0
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040014B7 RID: 5303
			public int <>1__state;

			// Token: 0x040014B8 RID: 5304
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040014B9 RID: 5305
			public BufferedStream <>4__this;

			// Token: 0x040014BA RID: 5306
			public CancellationToken cancellationToken;

			// Token: 0x040014BB RID: 5307
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;
		}

		// Token: 0x02000354 RID: 852
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadFromUnderlyingStreamAsync>d__51 : IAsyncStateMachine
		{
			// Token: 0x060026A0 RID: 9888 RVA: 0x000877C0 File Offset: 0x000859C0
			void IAsyncStateMachine.MoveNext()
			{
				int num5;
				int num4 = num5;
				BufferedStream bufferedStream = this;
				int result;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num4 != 0)
					{
						if (num4 - 1 <= 4)
						{
							goto IL_7C;
						}
						configuredTaskAwaiter = semaphoreLockTask.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num4 = (num5 = 0);
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<ReadFromUnderlyingStreamAsync>d__51>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num4 = (num5 = -1);
					}
					configuredTaskAwaiter.GetResult();
					IL_7C:
					try
					{
						BeginEndAwaitableAdapter beginEndAwaitableAdapter;
						ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
						int num6;
						switch (num4)
						{
						case 1:
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num4 = (num5 = -1);
							break;
						case 2:
						{
							object obj;
							beginEndAwaitableAdapter = (BeginEndAwaitableAdapter)obj;
							obj = null;
							num4 = (num5 = -1);
							goto IL_23E;
						}
						case 3:
						{
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num4 = (num5 = -1);
							goto IL_2EB;
						}
						case 4:
						{
							object obj;
							beginEndAwaitableAdapter = (BeginEndAwaitableAdapter)obj;
							obj = null;
							num4 = (num5 = -1);
							goto IL_3A1;
						}
						case 5:
						{
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
							configuredTaskAwaiter3 = configuredTaskAwaiter4;
							configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
							num4 = (num5 = -1);
							goto IL_442;
						}
						default:
							num6 = bufferedStream.ReadFromBuffer(array, offset, count);
							if (num6 == count)
							{
								result = bytesAlreadySatisfied + num6;
								goto IL_4A3;
							}
							if (num6 > 0)
							{
								count -= num6;
								offset += num6;
								bytesAlreadySatisfied += num6;
							}
							bufferedStream._readPos = (bufferedStream._readLen = 0);
							if (bufferedStream._writePos <= 0)
							{
								goto IL_184;
							}
							configuredTaskAwaiter = bufferedStream.FlushWriteAsync(cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num4 = (num5 = 1);
								configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<ReadFromUnderlyingStreamAsync>d__51>(ref configuredTaskAwaiter, ref this);
								return;
							}
							break;
						}
						configuredTaskAwaiter.GetResult();
						IL_184:
						if (count >= bufferedStream._bufferSize)
						{
							if (useApmPattern)
							{
								bufferedStream.EnsureBeginEndAwaitableAllocated();
								bufferedStream._stream.BeginRead(array, offset, count, BeginEndAwaitableAdapter.Callback, bufferedStream._beginEndAwaitable);
								num3 = bytesAlreadySatisfied;
								stream = bufferedStream._stream;
								beginEndAwaitableAdapter = bufferedStream._beginEndAwaitable.GetAwaiter();
								if (!beginEndAwaitableAdapter.IsCompleted)
								{
									num4 = (num5 = 2);
									object obj = beginEndAwaitableAdapter;
									this.<>t__builder.AwaitUnsafeOnCompleted<BeginEndAwaitableAdapter, BufferedStream.<ReadFromUnderlyingStreamAsync>d__51>(ref beginEndAwaitableAdapter, ref this);
									return;
								}
							}
							else
							{
								num3 = bytesAlreadySatisfied;
								configuredTaskAwaiter3 = bufferedStream._stream.ReadAsync(array, offset, count, cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num4 = (num5 = 3);
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, BufferedStream.<ReadFromUnderlyingStreamAsync>d__51>(ref configuredTaskAwaiter3, ref this);
									return;
								}
								goto IL_2EB;
							}
						}
						else
						{
							bufferedStream.EnsureBufferAllocated();
							if (useApmPattern)
							{
								bufferedStream.EnsureBeginEndAwaitableAllocated();
								bufferedStream._stream.BeginRead(bufferedStream._buffer, 0, bufferedStream._bufferSize, BeginEndAwaitableAdapter.Callback, bufferedStream._beginEndAwaitable);
								stream = bufferedStream._stream;
								beginEndAwaitableAdapter = bufferedStream._beginEndAwaitable.GetAwaiter();
								if (!beginEndAwaitableAdapter.IsCompleted)
								{
									num4 = (num5 = 4);
									object obj = beginEndAwaitableAdapter;
									this.<>t__builder.AwaitUnsafeOnCompleted<BeginEndAwaitableAdapter, BufferedStream.<ReadFromUnderlyingStreamAsync>d__51>(ref beginEndAwaitableAdapter, ref this);
									return;
								}
								goto IL_3A1;
							}
							else
							{
								configuredTaskAwaiter3 = bufferedStream._stream.ReadAsync(bufferedStream._buffer, 0, bufferedStream._bufferSize, cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num4 = (num5 = 5);
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, BufferedStream.<ReadFromUnderlyingStreamAsync>d__51>(ref configuredTaskAwaiter3, ref this);
									return;
								}
								goto IL_442;
							}
						}
						IL_23E:
						IAsyncResult result2 = beginEndAwaitableAdapter.GetResult();
						result = num3 + stream.EndRead(result2);
						goto IL_4A3;
						IL_2EB:
						int result3 = configuredTaskAwaiter3.GetResult();
						result = num3 + result3;
						goto IL_4A3;
						IL_3A1:
						result2 = beginEndAwaitableAdapter.GetResult();
						bufferedStream._readLen = stream.EndRead(result2);
						stream = null;
						goto IL_453;
						IL_442:
						result3 = configuredTaskAwaiter3.GetResult();
						bufferedStream._readLen = result3;
						IL_453:
						num6 = bufferedStream.ReadFromBuffer(array, offset, count);
						result = bytesAlreadySatisfied + num6;
					}
					finally
					{
						if (num4 < 0)
						{
							bufferedStream.EnsureAsyncActiveSemaphoreInitialized().Release();
						}
					}
				}
				catch (Exception exception)
				{
					num5 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_4A3:
				num5 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x060026A1 RID: 9889 RVA: 0x00087CB8 File Offset: 0x00085EB8
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040014BC RID: 5308
			public int <>1__state;

			// Token: 0x040014BD RID: 5309
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x040014BE RID: 5310
			public Task semaphoreLockTask;

			// Token: 0x040014BF RID: 5311
			public BufferedStream <>4__this;

			// Token: 0x040014C0 RID: 5312
			public byte[] array;

			// Token: 0x040014C1 RID: 5313
			public int offset;

			// Token: 0x040014C2 RID: 5314
			public int count;

			// Token: 0x040014C3 RID: 5315
			public int bytesAlreadySatisfied;

			// Token: 0x040014C4 RID: 5316
			public CancellationToken cancellationToken;

			// Token: 0x040014C5 RID: 5317
			public bool useApmPattern;

			// Token: 0x040014C6 RID: 5318
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040014C7 RID: 5319
			private int <>7__wrap1;

			// Token: 0x040014C8 RID: 5320
			private Stream <>7__wrap2;

			// Token: 0x040014C9 RID: 5321
			private object <>u__2;

			// Token: 0x040014CA RID: 5322
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x02000355 RID: 853
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteToUnderlyingStreamAsync>d__60 : IAsyncStateMachine
		{
			// Token: 0x060026A2 RID: 9890 RVA: 0x00087CC8 File Offset: 0x00085EC8
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				BufferedStream bufferedStream = this;
				try
				{
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
					if (num != 0)
					{
						if (num - 1 <= 7)
						{
							goto IL_7B;
						}
						configuredTaskAwaiter = semaphoreLockTask.ConfigureAwait(false).GetAwaiter();
						if (!configuredTaskAwaiter.IsCompleted)
						{
							num = (num2 = 0);
							configuredTaskAwaiter2 = configuredTaskAwaiter;
							this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref configuredTaskAwaiter, ref this);
							return;
						}
					}
					else
					{
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
					}
					configuredTaskAwaiter.GetResult();
					IL_7B:
					try
					{
						BeginEndAwaitableAdapter beginEndAwaitableAdapter;
						switch (num)
						{
						case 1:
						{
							object obj;
							beginEndAwaitableAdapter = (BeginEndAwaitableAdapter)obj;
							obj = null;
							num = (num2 = -1);
							break;
						}
						case 2:
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_23D;
						case 3:
						{
							object obj;
							beginEndAwaitableAdapter = (BeginEndAwaitableAdapter)obj;
							obj = null;
							num = (num2 = -1);
							goto IL_35E;
						}
						case 4:
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_3F2;
						case 5:
						{
							object obj;
							beginEndAwaitableAdapter = (BeginEndAwaitableAdapter)obj;
							obj = null;
							num = (num2 = -1);
							goto IL_49D;
						}
						case 6:
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_531;
						case 7:
						{
							object obj;
							beginEndAwaitableAdapter = (BeginEndAwaitableAdapter)obj;
							obj = null;
							num = (num2 = -1);
							goto IL_5DC;
						}
						case 8:
							configuredTaskAwaiter = configuredTaskAwaiter2;
							configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
							num = (num2 = -1);
							goto IL_672;
						default:
							if (bufferedStream._writePos == 0)
							{
								bufferedStream.ClearReadBufferBeforeWrite();
							}
							totalUserBytes = checked(bufferedStream._writePos + count);
							if (checked(totalUserBytes + count < bufferedStream._bufferSize + bufferedStream._bufferSize))
							{
								bufferedStream.WriteToBuffer(array, ref offset, ref count);
								if (bufferedStream._writePos < bufferedStream._bufferSize)
								{
									goto IL_6A7;
								}
								if (useApmPattern)
								{
									bufferedStream.EnsureBeginEndAwaitableAllocated();
									bufferedStream._stream.BeginWrite(bufferedStream._buffer, 0, bufferedStream._writePos, BeginEndAwaitableAdapter.Callback, bufferedStream._beginEndAwaitable);
									stream = bufferedStream._stream;
									beginEndAwaitableAdapter = bufferedStream._beginEndAwaitable.GetAwaiter();
									if (!beginEndAwaitableAdapter.IsCompleted)
									{
										num = (num2 = 1);
										object obj = beginEndAwaitableAdapter;
										this.<>t__builder.AwaitUnsafeOnCompleted<BeginEndAwaitableAdapter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref beginEndAwaitableAdapter, ref this);
										return;
									}
								}
								else
								{
									configuredTaskAwaiter = bufferedStream._stream.WriteAsync(bufferedStream._buffer, 0, bufferedStream._writePos, cancellationToken).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num = (num2 = 2);
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref configuredTaskAwaiter, ref this);
										return;
									}
									goto IL_23D;
								}
							}
							else
							{
								if (bufferedStream._writePos <= 0)
								{
									goto IL_53F;
								}
								if (totalUserBytes <= bufferedStream._bufferSize + bufferedStream._bufferSize && totalUserBytes <= 81920)
								{
									bufferedStream.EnsureShadowBufferAllocated();
									Buffer.InternalBlockCopy(array, offset, bufferedStream._buffer, bufferedStream._writePos, count);
									if (useApmPattern)
									{
										bufferedStream.EnsureBeginEndAwaitableAllocated();
										bufferedStream._stream.BeginWrite(bufferedStream._buffer, 0, totalUserBytes, BeginEndAwaitableAdapter.Callback, bufferedStream._beginEndAwaitable);
										stream = bufferedStream._stream;
										beginEndAwaitableAdapter = bufferedStream._beginEndAwaitable.GetAwaiter();
										if (!beginEndAwaitableAdapter.IsCompleted)
										{
											num = (num2 = 3);
											object obj = beginEndAwaitableAdapter;
											this.<>t__builder.AwaitUnsafeOnCompleted<BeginEndAwaitableAdapter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref beginEndAwaitableAdapter, ref this);
											return;
										}
										goto IL_35E;
									}
									else
									{
										configuredTaskAwaiter = bufferedStream._stream.WriteAsync(bufferedStream._buffer, 0, totalUserBytes, cancellationToken).ConfigureAwait(false).GetAwaiter();
										if (!configuredTaskAwaiter.IsCompleted)
										{
											num = (num2 = 4);
											configuredTaskAwaiter2 = configuredTaskAwaiter;
											this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref configuredTaskAwaiter, ref this);
											return;
										}
										goto IL_3F2;
									}
								}
								else if (useApmPattern)
								{
									bufferedStream.EnsureBeginEndAwaitableAllocated();
									bufferedStream._stream.BeginWrite(bufferedStream._buffer, 0, bufferedStream._writePos, BeginEndAwaitableAdapter.Callback, bufferedStream._beginEndAwaitable);
									stream = bufferedStream._stream;
									beginEndAwaitableAdapter = bufferedStream._beginEndAwaitable.GetAwaiter();
									if (!beginEndAwaitableAdapter.IsCompleted)
									{
										num = (num2 = 5);
										object obj = beginEndAwaitableAdapter;
										this.<>t__builder.AwaitUnsafeOnCompleted<BeginEndAwaitableAdapter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref beginEndAwaitableAdapter, ref this);
										return;
									}
									goto IL_49D;
								}
								else
								{
									configuredTaskAwaiter = bufferedStream._stream.WriteAsync(bufferedStream._buffer, 0, bufferedStream._writePos, cancellationToken).ConfigureAwait(false).GetAwaiter();
									if (!configuredTaskAwaiter.IsCompleted)
									{
										num = (num2 = 6);
										configuredTaskAwaiter2 = configuredTaskAwaiter;
										this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref configuredTaskAwaiter, ref this);
										return;
									}
									goto IL_531;
								}
							}
							break;
						}
						IAsyncResult result = beginEndAwaitableAdapter.GetResult();
						stream.EndWrite(result);
						stream = null;
						goto IL_244;
						IL_23D:
						configuredTaskAwaiter.GetResult();
						IL_244:
						bufferedStream._writePos = 0;
						bufferedStream.WriteToBuffer(array, ref offset, ref count);
						goto IL_679;
						IL_35E:
						result = beginEndAwaitableAdapter.GetResult();
						stream.EndWrite(result);
						stream = null;
						goto IL_3F9;
						IL_3F2:
						configuredTaskAwaiter.GetResult();
						IL_3F9:
						bufferedStream._writePos = 0;
						goto IL_6A7;
						IL_49D:
						result = beginEndAwaitableAdapter.GetResult();
						stream.EndWrite(result);
						stream = null;
						goto IL_538;
						IL_531:
						configuredTaskAwaiter.GetResult();
						IL_538:
						bufferedStream._writePos = 0;
						IL_53F:
						if (useApmPattern)
						{
							bufferedStream.EnsureBeginEndAwaitableAllocated();
							bufferedStream._stream.BeginWrite(array, offset, count, BeginEndAwaitableAdapter.Callback, bufferedStream._beginEndAwaitable);
							stream = bufferedStream._stream;
							beginEndAwaitableAdapter = bufferedStream._beginEndAwaitable.GetAwaiter();
							if (!beginEndAwaitableAdapter.IsCompleted)
							{
								num = (num2 = 7);
								object obj = beginEndAwaitableAdapter;
								this.<>t__builder.AwaitUnsafeOnCompleted<BeginEndAwaitableAdapter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref beginEndAwaitableAdapter, ref this);
								return;
							}
						}
						else
						{
							configuredTaskAwaiter = bufferedStream._stream.WriteAsync(array, offset, count, cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 8);
								configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, BufferedStream.<WriteToUnderlyingStreamAsync>d__60>(ref configuredTaskAwaiter, ref this);
								return;
							}
							goto IL_672;
						}
						IL_5DC:
						result = beginEndAwaitableAdapter.GetResult();
						stream.EndWrite(result);
						stream = null;
						goto IL_679;
						IL_672:
						configuredTaskAwaiter.GetResult();
						IL_679:;
					}
					finally
					{
						if (num < 0)
						{
							bufferedStream.EnsureAsyncActiveSemaphoreInitialized().Release();
						}
					}
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_6A7:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x060026A3 RID: 9891 RVA: 0x000883C4 File Offset: 0x000865C4
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x040014CB RID: 5323
			public int <>1__state;

			// Token: 0x040014CC RID: 5324
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x040014CD RID: 5325
			public Task semaphoreLockTask;

			// Token: 0x040014CE RID: 5326
			public BufferedStream <>4__this;

			// Token: 0x040014CF RID: 5327
			public int count;

			// Token: 0x040014D0 RID: 5328
			public byte[] array;

			// Token: 0x040014D1 RID: 5329
			public int offset;

			// Token: 0x040014D2 RID: 5330
			public bool useApmPattern;

			// Token: 0x040014D3 RID: 5331
			public CancellationToken cancellationToken;

			// Token: 0x040014D4 RID: 5332
			private int <totalUserBytes>5__1;

			// Token: 0x040014D5 RID: 5333
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__1;

			// Token: 0x040014D6 RID: 5334
			private Stream <>7__wrap1;

			// Token: 0x040014D7 RID: 5335
			private object <>u__2;
		}
	}
}
