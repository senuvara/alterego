﻿using System;

namespace System.IO
{
	// Token: 0x020003A8 RID: 936
	internal struct MonoIOStat
	{
		// Token: 0x04001692 RID: 5778
		public FileAttributes fileAttributes;

		// Token: 0x04001693 RID: 5779
		public long Length;

		// Token: 0x04001694 RID: 5780
		public long CreationTime;

		// Token: 0x04001695 RID: 5781
		public long LastAccessTime;

		// Token: 0x04001696 RID: 5782
		public long LastWriteTime;
	}
}
