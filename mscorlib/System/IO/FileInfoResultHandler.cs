﻿using System;
using System.Security;

namespace System.IO
{
	// Token: 0x02000363 RID: 867
	internal class FileInfoResultHandler : SearchResultHandler<FileInfo>
	{
		// Token: 0x06002749 RID: 10057 RVA: 0x0008A0D4 File Offset: 0x000882D4
		[SecurityCritical]
		internal override bool IsResultIncluded(SearchResult result)
		{
			return FileSystemEnumerableHelpers.IsFile(result.FindData);
		}

		// Token: 0x0600274A RID: 10058 RVA: 0x0008A0E1 File Offset: 0x000882E1
		[SecurityCritical]
		internal override FileInfo CreateObject(SearchResult result)
		{
			FileInfo fileInfo = new FileInfo(result.FullPath, false);
			fileInfo.InitializeFrom(result.FindData);
			return fileInfo;
		}

		// Token: 0x0600274B RID: 10059 RVA: 0x0008A0FB File Offset: 0x000882FB
		public FileInfoResultHandler()
		{
		}
	}
}
