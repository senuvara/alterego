﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Threading.Tasks;

namespace System.IO
{
	// Token: 0x0200038F RID: 911
	internal sealed class UnmanagedMemoryStreamWrapper : MemoryStream
	{
		// Token: 0x060029C6 RID: 10694 RVA: 0x000934AC File Offset: 0x000916AC
		internal UnmanagedMemoryStreamWrapper(UnmanagedMemoryStream stream)
		{
			this._unmanagedStream = stream;
		}

		// Token: 0x1700065B RID: 1627
		// (get) Token: 0x060029C7 RID: 10695 RVA: 0x000934BB File Offset: 0x000916BB
		public override bool CanRead
		{
			get
			{
				return this._unmanagedStream.CanRead;
			}
		}

		// Token: 0x1700065C RID: 1628
		// (get) Token: 0x060029C8 RID: 10696 RVA: 0x000934C8 File Offset: 0x000916C8
		public override bool CanSeek
		{
			get
			{
				return this._unmanagedStream.CanSeek;
			}
		}

		// Token: 0x1700065D RID: 1629
		// (get) Token: 0x060029C9 RID: 10697 RVA: 0x000934D5 File Offset: 0x000916D5
		public override bool CanWrite
		{
			get
			{
				return this._unmanagedStream.CanWrite;
			}
		}

		// Token: 0x060029CA RID: 10698 RVA: 0x000934E4 File Offset: 0x000916E4
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					this._unmanagedStream.Close();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x060029CB RID: 10699 RVA: 0x0009351C File Offset: 0x0009171C
		public override void Flush()
		{
			this._unmanagedStream.Flush();
		}

		// Token: 0x060029CC RID: 10700 RVA: 0x00093529 File Offset: 0x00091729
		public override byte[] GetBuffer()
		{
			throw new UnauthorizedAccessException(Environment.GetResourceString("MemoryStream's internal buffer cannot be accessed."));
		}

		// Token: 0x060029CD RID: 10701 RVA: 0x0009353A File Offset: 0x0009173A
		public override bool TryGetBuffer(out ArraySegment<byte> buffer)
		{
			buffer = default(ArraySegment<byte>);
			return false;
		}

		// Token: 0x1700065E RID: 1630
		// (get) Token: 0x060029CE RID: 10702 RVA: 0x00093544 File Offset: 0x00091744
		// (set) Token: 0x060029CF RID: 10703 RVA: 0x00093552 File Offset: 0x00091752
		public override int Capacity
		{
			get
			{
				return (int)this._unmanagedStream.Capacity;
			}
			set
			{
				throw new IOException(Environment.GetResourceString("Unable to expand length of this stream beyond its capacity."));
			}
		}

		// Token: 0x1700065F RID: 1631
		// (get) Token: 0x060029D0 RID: 10704 RVA: 0x00093563 File Offset: 0x00091763
		public override long Length
		{
			get
			{
				return this._unmanagedStream.Length;
			}
		}

		// Token: 0x17000660 RID: 1632
		// (get) Token: 0x060029D1 RID: 10705 RVA: 0x00093570 File Offset: 0x00091770
		// (set) Token: 0x060029D2 RID: 10706 RVA: 0x0009357D File Offset: 0x0009177D
		public override long Position
		{
			get
			{
				return this._unmanagedStream.Position;
			}
			set
			{
				this._unmanagedStream.Position = value;
			}
		}

		// Token: 0x060029D3 RID: 10707 RVA: 0x0009358B File Offset: 0x0009178B
		public override int Read([In] [Out] byte[] buffer, int offset, int count)
		{
			return this._unmanagedStream.Read(buffer, offset, count);
		}

		// Token: 0x060029D4 RID: 10708 RVA: 0x0009359B File Offset: 0x0009179B
		public override int ReadByte()
		{
			return this._unmanagedStream.ReadByte();
		}

		// Token: 0x060029D5 RID: 10709 RVA: 0x000935A8 File Offset: 0x000917A8
		public override long Seek(long offset, SeekOrigin loc)
		{
			return this._unmanagedStream.Seek(offset, loc);
		}

		// Token: 0x060029D6 RID: 10710 RVA: 0x000935B8 File Offset: 0x000917B8
		[SecuritySafeCritical]
		public override byte[] ToArray()
		{
			if (!this._unmanagedStream._isOpen)
			{
				__Error.StreamIsClosed();
			}
			if (!this._unmanagedStream.CanRead)
			{
				__Error.ReadNotSupported();
			}
			byte[] array = new byte[this._unmanagedStream.Length];
			Buffer.Memcpy(array, 0, this._unmanagedStream.Pointer, 0, (int)this._unmanagedStream.Length);
			return array;
		}

		// Token: 0x060029D7 RID: 10711 RVA: 0x00093619 File Offset: 0x00091819
		public override void Write(byte[] buffer, int offset, int count)
		{
			this._unmanagedStream.Write(buffer, offset, count);
		}

		// Token: 0x060029D8 RID: 10712 RVA: 0x00093629 File Offset: 0x00091829
		public override void WriteByte(byte value)
		{
			this._unmanagedStream.WriteByte(value);
		}

		// Token: 0x060029D9 RID: 10713 RVA: 0x00093638 File Offset: 0x00091838
		public override void WriteTo(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream", Environment.GetResourceString("Stream cannot be null."));
			}
			if (!this._unmanagedStream._isOpen)
			{
				__Error.StreamIsClosed();
			}
			if (!this.CanRead)
			{
				__Error.ReadNotSupported();
			}
			byte[] array = this.ToArray();
			stream.Write(array, 0, array.Length);
		}

		// Token: 0x060029DA RID: 10714 RVA: 0x0009368E File Offset: 0x0009188E
		public override void SetLength(long value)
		{
			base.SetLength(value);
		}

		// Token: 0x060029DB RID: 10715 RVA: 0x00093698 File Offset: 0x00091898
		public override Task CopyToAsync(Stream destination, int bufferSize, CancellationToken cancellationToken)
		{
			if (destination == null)
			{
				throw new ArgumentNullException("destination");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize", Environment.GetResourceString("Positive number required."));
			}
			if (!this.CanRead && !this.CanWrite)
			{
				throw new ObjectDisposedException(null, Environment.GetResourceString("Cannot access a closed Stream."));
			}
			if (!destination.CanRead && !destination.CanWrite)
			{
				throw new ObjectDisposedException("destination", Environment.GetResourceString("Cannot access a closed Stream."));
			}
			if (!this.CanRead)
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support reading."));
			}
			if (!destination.CanWrite)
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support writing."));
			}
			return this._unmanagedStream.CopyToAsync(destination, bufferSize, cancellationToken);
		}

		// Token: 0x060029DC RID: 10716 RVA: 0x00093750 File Offset: 0x00091950
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return this._unmanagedStream.FlushAsync(cancellationToken);
		}

		// Token: 0x060029DD RID: 10717 RVA: 0x0009375E File Offset: 0x0009195E
		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			return this._unmanagedStream.ReadAsync(buffer, offset, count, cancellationToken);
		}

		// Token: 0x060029DE RID: 10718 RVA: 0x00093770 File Offset: 0x00091970
		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			return this._unmanagedStream.WriteAsync(buffer, offset, count, cancellationToken);
		}

		// Token: 0x040015ED RID: 5613
		private UnmanagedMemoryStream _unmanagedStream;
	}
}
