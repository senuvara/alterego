﻿using System;
using System.Threading;

namespace System
{
	// Token: 0x020001A0 RID: 416
	internal static class ProgressStatics
	{
		// Token: 0x060011CD RID: 4557 RVA: 0x000494BB File Offset: 0x000476BB
		// Note: this type is marked as 'beforefieldinit'.
		static ProgressStatics()
		{
		}

		// Token: 0x04000A21 RID: 2593
		internal static readonly SynchronizationContext DefaultContext = new SynchronizationContext();
	}
}
