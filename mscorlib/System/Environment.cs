﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using Mono;

namespace System
{
	/// <summary>Provides information about, and means to manipulate, the current environment and platform. This class cannot be inherited.</summary>
	// Token: 0x020001E3 RID: 483
	[ComVisible(true)]
	public static class Environment
	{
		// Token: 0x06001717 RID: 5911 RVA: 0x00002058 File Offset: 0x00000258
		internal static string GetResourceString(string key)
		{
			return key;
		}

		// Token: 0x06001718 RID: 5912 RVA: 0x00002058 File Offset: 0x00000258
		internal static string GetResourceString(string key, CultureInfo culture)
		{
			return key;
		}

		// Token: 0x06001719 RID: 5913 RVA: 0x0005AB19 File Offset: 0x00058D19
		internal static string GetResourceString(string key, params object[] values)
		{
			return string.Format(CultureInfo.InvariantCulture, key, values);
		}

		// Token: 0x0600171A RID: 5914 RVA: 0x00002058 File Offset: 0x00000258
		internal static string GetRuntimeResourceString(string key)
		{
			return key;
		}

		// Token: 0x0600171B RID: 5915 RVA: 0x0005AB19 File Offset: 0x00058D19
		internal static string GetRuntimeResourceString(string key, params object[] values)
		{
			return string.Format(CultureInfo.InvariantCulture, key, values);
		}

		// Token: 0x0600171C RID: 5916 RVA: 0x0005AB28 File Offset: 0x00058D28
		internal static string GetResourceStringEncodingName(int codePage)
		{
			if (codePage <= 12000)
			{
				if (codePage == 1200)
				{
					return Environment.GetResourceString("Unicode");
				}
				if (codePage == 1201)
				{
					return Environment.GetResourceString("Unicode (Big-Endian)");
				}
				if (codePage == 12000)
				{
					return Environment.GetResourceString("Unicode (UTF-32)");
				}
			}
			else if (codePage <= 20127)
			{
				if (codePage == 12001)
				{
					return Environment.GetResourceString("Unicode (UTF-32 Big-Endian)");
				}
				if (codePage == 20127)
				{
					return Environment.GetResourceString("US-ASCII");
				}
			}
			else
			{
				if (codePage == 65000)
				{
					return Environment.GetResourceString("Unicode (UTF-7)");
				}
				if (codePage == 65001)
				{
					return Environment.GetResourceString("Unicode (UTF-8)");
				}
			}
			return codePage.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x0600171D RID: 5917 RVA: 0x00002526 File Offset: 0x00000726
		internal static bool IsWindows8OrAbove
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets the command line for this process.</summary>
		/// <returns>A string containing command-line arguments.</returns>
		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x0600171E RID: 5918 RVA: 0x0005ABDC File Offset: 0x00058DDC
		public static string CommandLine
		{
			get
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (string text in Environment.GetCommandLineArgs())
				{
					bool flag = false;
					string text2 = "";
					string text3 = text;
					for (int j = 0; j < text3.Length; j++)
					{
						if (text2.Length == 0 && char.IsWhiteSpace(text3[j]))
						{
							text2 = "\"";
						}
						else if (text3[j] == '"')
						{
							flag = true;
						}
					}
					if (flag && text2.Length != 0)
					{
						text3 = text3.Replace("\"", "\\\"");
					}
					stringBuilder.AppendFormat("{0}{1}{0} ", text2, text3);
				}
				if (stringBuilder.Length > 0)
				{
					StringBuilder stringBuilder2 = stringBuilder;
					int i = stringBuilder2.Length;
					stringBuilder2.Length = i - 1;
				}
				return stringBuilder.ToString();
			}
		}

		/// <summary>Gets or sets the fully qualified path of the current working directory.</summary>
		/// <returns>A string containing a directory path.</returns>
		/// <exception cref="T:System.ArgumentException">Attempted to set to an empty string ("").</exception>
		/// <exception cref="T:System.ArgumentNullException">Attempted to set to <see langword="null." /></exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred.</exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">Attempted to set a local path that cannot be found.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the appropriate permission.</exception>
		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x0600171F RID: 5919 RVA: 0x0005ACAC File Offset: 0x00058EAC
		// (set) Token: 0x06001720 RID: 5920 RVA: 0x0005ACB3 File Offset: 0x00058EB3
		public static string CurrentDirectory
		{
			get
			{
				return Directory.GetCurrentDirectory();
			}
			set
			{
				Directory.SetCurrentDirectory(value);
			}
		}

		/// <summary>Gets a unique identifier for the current managed thread.</summary>
		/// <returns>An integer that represents a unique identifier for this managed thread.</returns>
		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06001721 RID: 5921 RVA: 0x0005ACBB File Offset: 0x00058EBB
		public static int CurrentManagedThreadId
		{
			get
			{
				return Thread.CurrentThread.ManagedThreadId;
			}
		}

		/// <summary>Gets or sets the exit code of the process.</summary>
		/// <returns>A 32-bit signed integer containing the exit code. The default value is 0 (zero), which indicates that the process completed successfully.</returns>
		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06001722 RID: 5922
		// (set) Token: 0x06001723 RID: 5923
		public static extern int ExitCode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		/// <summary>Gets a value that indicates whether the current application domain is being unloaded or the common language runtime (CLR) is shutting down. </summary>
		/// <returns>
		///     <see langword="true" /> if the current application domain is being unloaded or the CLR is shutting down; otherwise, <see langword="false." /></returns>
		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06001724 RID: 5924
		public static extern bool HasShutdownStarted { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		/// <summary>Gets the NetBIOS name of this local computer.</summary>
		/// <returns>A string containing the name of this computer.</returns>
		/// <exception cref="T:System.InvalidOperationException">The name of this computer cannot be obtained.</exception>
		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06001725 RID: 5925
		public static extern string MachineName { [EnvironmentPermission(SecurityAction.Demand, Read = "COMPUTERNAME")] [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001726 RID: 5926
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetNewLine();

		/// <summary>Gets the newline string defined for this environment.</summary>
		/// <returns>A string containing "\r\n" for non-Unix platforms, or a string containing "\n" for Unix platforms.</returns>
		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06001727 RID: 5927 RVA: 0x0005ACC7 File Offset: 0x00058EC7
		public static string NewLine
		{
			get
			{
				if (Environment.nl != null)
				{
					return Environment.nl;
				}
				Environment.nl = Environment.GetNewLine();
				return Environment.nl;
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06001728 RID: 5928
		private static extern PlatformID Platform { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001729 RID: 5929
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetOSVersionString();

		/// <summary>Gets an <see cref="T:System.OperatingSystem" /> object that contains the current platform identifier and version number.</summary>
		/// <returns>An object that contains the platform identifier and version number.</returns>
		/// <exception cref="T:System.InvalidOperationException">This property was unable to obtain the system version.-or- The obtained platform identifier is not a member of <see cref="T:System.PlatformID" /></exception>
		// Token: 0x170002CD RID: 717
		// (get) Token: 0x0600172A RID: 5930 RVA: 0x0005ACE8 File Offset: 0x00058EE8
		public static OperatingSystem OSVersion
		{
			get
			{
				if (Environment.os == null)
				{
					Version version = Environment.CreateVersionFromString(Environment.GetOSVersionString());
					PlatformID platformID = Environment.Platform;
					if (platformID == PlatformID.MacOSX)
					{
						platformID = PlatformID.Unix;
					}
					Environment.os = new OperatingSystem(platformID, version);
				}
				return Environment.os;
			}
		}

		// Token: 0x0600172B RID: 5931 RVA: 0x0005AD24 File Offset: 0x00058F24
		internal static Version CreateVersionFromString(string info)
		{
			int major = 0;
			int minor = 0;
			int build = 0;
			int revision = 0;
			int num = 1;
			int num2 = -1;
			if (info == null)
			{
				return new Version(0, 0, 0, 0);
			}
			foreach (char c in info)
			{
				if (char.IsDigit(c))
				{
					if (num2 < 0)
					{
						num2 = (int)(c - '0');
					}
					else
					{
						num2 = num2 * 10 + (int)(c - '0');
					}
				}
				else if (num2 >= 0)
				{
					switch (num)
					{
					case 1:
						major = num2;
						break;
					case 2:
						minor = num2;
						break;
					case 3:
						build = num2;
						break;
					case 4:
						revision = num2;
						break;
					}
					num2 = -1;
					num++;
				}
				if (num == 5)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				switch (num)
				{
				case 1:
					major = num2;
					break;
				case 2:
					minor = num2;
					break;
				case 3:
					build = num2;
					break;
				case 4:
					revision = num2;
					break;
				}
			}
			return new Version(major, minor, build, revision);
		}

		/// <summary>Gets current stack trace information.</summary>
		/// <returns>A string containing stack trace information. This value can be <see cref="F:System.String.Empty" />.</returns>
		// Token: 0x170002CE RID: 718
		// (get) Token: 0x0600172C RID: 5932 RVA: 0x0005AE0C File Offset: 0x0005900C
		public static string StackTrace
		{
			[EnvironmentPermission(SecurityAction.Demand, Unrestricted = true)]
			get
			{
				return new StackTrace(0, true).ToString();
			}
		}

		/// <summary>Gets the fully qualified path of the system directory.</summary>
		/// <returns>A string containing a directory path.</returns>
		// Token: 0x170002CF RID: 719
		// (get) Token: 0x0600172D RID: 5933 RVA: 0x0005AE1A File Offset: 0x0005901A
		public static string SystemDirectory
		{
			get
			{
				return Environment.GetFolderPath(Environment.SpecialFolder.System);
			}
		}

		/// <summary>Gets the number of milliseconds elapsed since the system started.</summary>
		/// <returns>A 32-bit signed integer containing the amount of time in milliseconds that has passed since the last time the computer was started. </returns>
		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x0600172E RID: 5934
		public static extern int TickCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		/// <summary>Gets the network domain name associated with the current user.</summary>
		/// <returns>The network domain name associated with the current user.</returns>
		/// <exception cref="T:System.PlatformNotSupportedException">The operating system does not support retrieving the network domain name.</exception>
		/// <exception cref="T:System.InvalidOperationException">The network domain name cannot be retrieved.</exception>
		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x0600172F RID: 5935 RVA: 0x0005AE23 File Offset: 0x00059023
		public static string UserDomainName
		{
			[EnvironmentPermission(SecurityAction.Demand, Read = "USERDOMAINNAME")]
			get
			{
				return Environment.MachineName;
			}
		}

		/// <summary>Gets a value indicating whether the current process is running in user interactive mode.</summary>
		/// <returns>
		///     <see langword="true" /> if the current process is running in user interactive mode; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x06001730 RID: 5936 RVA: 0x00002526 File Offset: 0x00000726
		[MonoTODO("Currently always returns false, regardless of interactive state")]
		public static bool UserInteractive
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets the user name of the person who is currently logged on to the Windows operating system.</summary>
		/// <returns>The user name of the person who is logged on to Windows.</returns>
		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x06001731 RID: 5937
		public static extern string UserName { [EnvironmentPermission(SecurityAction.Demand, Read = "USERNAME;USER")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		/// <summary>Gets a <see cref="T:System.Version" /> object that describes the major, minor, build, and revision numbers of the common language runtime.</summary>
		/// <returns>An object that displays the version of the common language runtime.</returns>
		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x06001732 RID: 5938 RVA: 0x0005AE2A File Offset: 0x0005902A
		public static Version Version
		{
			get
			{
				return new Version("4.0.30319.17020");
			}
		}

		/// <summary>Gets the amount of physical memory mapped to the process context.</summary>
		/// <returns>A 64-bit signed integer containing the number of bytes of physical memory mapped to the process context.</returns>
		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x06001733 RID: 5939 RVA: 0x0005AE36 File Offset: 0x00059036
		[MonoTODO("Currently always returns zero")]
		public static long WorkingSet
		{
			[EnvironmentPermission(SecurityAction.Demand, Unrestricted = true)]
			get
			{
				return 0L;
			}
		}

		/// <summary>Terminates this process and returns an exit code to the operating system.</summary>
		/// <param name="exitCode">The exit code to return to the operating system. Use 0 (zero) to indicate that the process completed successfully.</param>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have sufficient security permission to perform this function. </exception>
		// Token: 0x06001734 RID: 5940
		[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Exit(int exitCode);

		// Token: 0x06001735 RID: 5941 RVA: 0x0005AE3A File Offset: 0x0005903A
		internal static void _Exit(int exitCode)
		{
			Environment.Exit(exitCode);
		}

		/// <summary>Replaces the name of each environment variable embedded in the specified string with the string equivalent of the value of the variable, then returns the resulting string.</summary>
		/// <param name="name">A string containing the names of zero or more environment variables. Each environment variable is quoted with the percent sign character (%).</param>
		/// <returns>A string with each environment variable replaced by its value.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />.</exception>
		// Token: 0x06001736 RID: 5942 RVA: 0x0005AE44 File Offset: 0x00059044
		public static string ExpandEnvironmentVariables(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			int num = name.IndexOf('%');
			if (num == -1)
			{
				return name;
			}
			int length = name.Length;
			int num2;
			if (num == length - 1 || (num2 = name.IndexOf('%', num + 1)) == -1)
			{
				return name;
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(name, 0, num);
			Hashtable hashtable = null;
			do
			{
				string text = name.Substring(num + 1, num2 - num - 1);
				string text2 = Environment.GetEnvironmentVariable(text);
				if (text2 == null && Environment.IsRunningOnWindows)
				{
					if (hashtable == null)
					{
						hashtable = Environment.GetEnvironmentVariablesNoCase();
					}
					text2 = (hashtable[text] as string);
				}
				int num3 = num2;
				if (text2 == null)
				{
					stringBuilder.Append('%');
					stringBuilder.Append(text);
					num2--;
				}
				else
				{
					stringBuilder.Append(text2);
				}
				int num4 = num2;
				num = name.IndexOf('%', num2 + 1);
				num2 = ((num == -1 || num2 > length - 1) ? -1 : name.IndexOf('%', num + 1));
				int count;
				if (num == -1 || num2 == -1)
				{
					count = length - num4 - 1;
				}
				else if (text2 != null)
				{
					count = num - num4 - 1;
				}
				else
				{
					count = num - num3;
				}
				if (num >= num4 || num == -1)
				{
					stringBuilder.Append(name, num4 + 1, count);
				}
			}
			while (num2 > -1 && num2 < length);
			return stringBuilder.ToString();
		}

		/// <summary>Returns a string array containing the command-line arguments for the current process.</summary>
		/// <returns>An array of string where each element contains a command-line argument. The first element is the executable file name, and the following zero or more elements contain the remaining command-line arguments.</returns>
		/// <exception cref="T:System.NotSupportedException">The system does not support command-line arguments.</exception>
		// Token: 0x06001737 RID: 5943
		[EnvironmentPermission(SecurityAction.Demand, Read = "PATH")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetCommandLineArgs();

		// Token: 0x06001738 RID: 5944
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string internalGetEnvironmentVariable_native(IntPtr variable);

		// Token: 0x06001739 RID: 5945 RVA: 0x0005AF80 File Offset: 0x00059180
		internal static string internalGetEnvironmentVariable(string variable)
		{
			if (variable == null)
			{
				return null;
			}
			string result;
			using (SafeStringMarshal safeStringMarshal = RuntimeMarshal.MarshalString(variable))
			{
				result = Environment.internalGetEnvironmentVariable_native(safeStringMarshal.Value);
			}
			return result;
		}

		/// <summary>Retrieves the value of an environment variable from the current process. </summary>
		/// <param name="variable">The name of the environment variable.</param>
		/// <returns>The value of the environment variable specified by <paramref name="variable" />, or <see langword="null" /> if the environment variable is not found.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="variable" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation. </exception>
		// Token: 0x0600173A RID: 5946 RVA: 0x0005AFC8 File Offset: 0x000591C8
		public static string GetEnvironmentVariable(string variable)
		{
			return Environment.internalGetEnvironmentVariable(variable);
		}

		// Token: 0x0600173B RID: 5947 RVA: 0x0005AFD0 File Offset: 0x000591D0
		private static Hashtable GetEnvironmentVariablesNoCase()
		{
			Hashtable hashtable = new Hashtable(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default);
			foreach (string text in Environment.GetEnvironmentVariableNames())
			{
				hashtable[text] = Environment.internalGetEnvironmentVariable(text);
			}
			return hashtable;
		}

		/// <summary>Retrieves all environment variable names and their values from the current process.</summary>
		/// <returns>A dictionary that contains all environment variable names and their values; otherwise, an empty dictionary if no environment variables are found.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation.</exception>
		/// <exception cref="T:System.OutOfMemoryException">The buffer is out of memory.</exception>
		// Token: 0x0600173C RID: 5948 RVA: 0x0005B014 File Offset: 0x00059214
		[EnvironmentPermission(SecurityAction.Demand, Unrestricted = true)]
		public static IDictionary GetEnvironmentVariables()
		{
			Hashtable hashtable = new Hashtable();
			foreach (string text in Environment.GetEnvironmentVariableNames())
			{
				hashtable[text] = Environment.internalGetEnvironmentVariable(text);
			}
			return hashtable;
		}

		/// <summary>Gets the path to the system special folder that is identified by the specified enumeration.</summary>
		/// <param name="folder">An enumerated constant that identifies a system special folder.</param>
		/// <returns>The path to the specified system special folder, if that folder physically exists on your computer; otherwise, an empty string ("").A folder will not physically exist if the operating system did not create it, the existing folder was deleted, or the folder is a virtual directory, such as My Computer, which does not correspond to a physical path.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="folder" /> is not a member of <see cref="T:System.Environment.SpecialFolder" />.</exception>
		/// <exception cref="T:System.PlatformNotSupportedException">The current platform is not supported.</exception>
		// Token: 0x0600173D RID: 5949 RVA: 0x0005B04D File Offset: 0x0005924D
		public static string GetFolderPath(Environment.SpecialFolder folder)
		{
			return Environment.GetFolderPath(folder, Environment.SpecialFolderOption.None);
		}

		// Token: 0x0600173E RID: 5950
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetWindowsFolderPath(int folder);

		/// <summary>Gets the path to the system special folder that is identified by the specified enumeration, and uses a specified option for accessing special folders.</summary>
		/// <param name="folder">An enumerated constant that identifies a system special folder.</param>
		/// <param name="option">Specifies options to use for accessing a special folder.</param>
		/// <returns>The path to the specified system special folder, if that folder physically exists on your computer; otherwise, an empty string ("").A folder will not physically exist if the operating system did not create it, the existing folder was deleted, or the folder is a virtual directory, such as My Computer, which does not correspond to a physical path.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="folder" /> is not a member of <see cref="T:System.Environment.SpecialFolder" /></exception>
		/// <exception cref="T:System.PlatformNotSupportedException">
		///         <see cref="T:System.PlatformNotSupportedException" />
		///       </exception>
		// Token: 0x0600173F RID: 5951 RVA: 0x0005B058 File Offset: 0x00059258
		public static string GetFolderPath(Environment.SpecialFolder folder, Environment.SpecialFolderOption option)
		{
			SecurityManager.EnsureElevatedPermissions();
			string result;
			if (Environment.IsRunningOnWindows)
			{
				result = Environment.GetWindowsFolderPath((int)folder);
			}
			else
			{
				result = Environment.UnixGetFolderPath(folder, option);
			}
			return result;
		}

		// Token: 0x06001740 RID: 5952 RVA: 0x0005B088 File Offset: 0x00059288
		private static string ReadXdgUserDir(string config_dir, string home_dir, string key, string fallback)
		{
			string text = Environment.internalGetEnvironmentVariable(key);
			if (text != null && text != string.Empty)
			{
				return text;
			}
			string path = Path.Combine(config_dir, "user-dirs.dirs");
			if (!File.Exists(path))
			{
				return Path.Combine(home_dir, fallback);
			}
			try
			{
				using (StreamReader streamReader = new StreamReader(path))
				{
					string text2;
					while ((text2 = streamReader.ReadLine()) != null)
					{
						text2 = text2.Trim();
						int num = text2.IndexOf('=');
						if (num > 8 && text2.Substring(0, num) == key)
						{
							string text3 = text2.Substring(num + 1).Trim(new char[]
							{
								'"'
							});
							bool flag = false;
							if (text3.StartsWithOrdinalUnchecked("$HOME/"))
							{
								flag = true;
								text3 = text3.Substring(6);
							}
							else if (!text3.StartsWithOrdinalUnchecked("/"))
							{
								flag = true;
							}
							return flag ? Path.Combine(home_dir, text3) : text3;
						}
					}
				}
			}
			catch (FileNotFoundException)
			{
			}
			return Path.Combine(home_dir, fallback);
		}

		// Token: 0x06001741 RID: 5953 RVA: 0x0005B1A4 File Offset: 0x000593A4
		internal static string UnixGetFolderPath(Environment.SpecialFolder folder, Environment.SpecialFolderOption option)
		{
			string text = Environment.internalGetHome();
			string text2 = Environment.internalGetEnvironmentVariable("XDG_DATA_HOME");
			if (text2 == null || text2 == string.Empty)
			{
				text2 = Path.Combine(text, ".local");
				text2 = Path.Combine(text2, "share");
			}
			string text3 = Environment.internalGetEnvironmentVariable("XDG_CONFIG_HOME");
			if (text3 == null || text3 == string.Empty)
			{
				text3 = Path.Combine(text, ".config");
			}
			switch (folder)
			{
			case Environment.SpecialFolder.Desktop:
			case Environment.SpecialFolder.DesktopDirectory:
				return Environment.ReadXdgUserDir(text3, text, "XDG_DESKTOP_DIR", "Desktop");
			case Environment.SpecialFolder.Programs:
			case Environment.SpecialFolder.Startup:
			case Environment.SpecialFolder.Recent:
			case Environment.SpecialFolder.SendTo:
			case Environment.SpecialFolder.StartMenu:
			case Environment.SpecialFolder.NetworkShortcuts:
			case Environment.SpecialFolder.CommonStartMenu:
			case Environment.SpecialFolder.CommonPrograms:
			case Environment.SpecialFolder.CommonStartup:
			case Environment.SpecialFolder.CommonDesktopDirectory:
			case Environment.SpecialFolder.PrinterShortcuts:
			case Environment.SpecialFolder.Cookies:
			case Environment.SpecialFolder.History:
			case Environment.SpecialFolder.Windows:
			case Environment.SpecialFolder.System:
			case Environment.SpecialFolder.SystemX86:
			case Environment.SpecialFolder.ProgramFilesX86:
			case Environment.SpecialFolder.CommonProgramFiles:
			case Environment.SpecialFolder.CommonProgramFilesX86:
			case Environment.SpecialFolder.CommonDocuments:
			case Environment.SpecialFolder.CommonAdminTools:
			case Environment.SpecialFolder.AdminTools:
			case Environment.SpecialFolder.CommonMusic:
			case Environment.SpecialFolder.CommonPictures:
			case Environment.SpecialFolder.CommonVideos:
			case Environment.SpecialFolder.Resources:
			case Environment.SpecialFolder.LocalizedResources:
			case Environment.SpecialFolder.CommonOemLinks:
			case Environment.SpecialFolder.CDBurning:
				return string.Empty;
			case Environment.SpecialFolder.MyDocuments:
				return text;
			case Environment.SpecialFolder.Favorites:
				if (Environment.Platform == PlatformID.MacOSX)
				{
					return Path.Combine(text, "Library", "Favorites");
				}
				return string.Empty;
			case Environment.SpecialFolder.MyMusic:
				if (Environment.Platform == PlatformID.MacOSX)
				{
					return Path.Combine(text, "Music");
				}
				return Environment.ReadXdgUserDir(text3, text, "XDG_MUSIC_DIR", "Music");
			case Environment.SpecialFolder.MyVideos:
				return Environment.ReadXdgUserDir(text3, text, "XDG_VIDEOS_DIR", "Videos");
			case Environment.SpecialFolder.MyComputer:
				return string.Empty;
			case Environment.SpecialFolder.Fonts:
				if (Environment.Platform == PlatformID.MacOSX)
				{
					return Path.Combine(text, "Library", "Fonts");
				}
				return Path.Combine(text, ".fonts");
			case Environment.SpecialFolder.Templates:
				return Environment.ReadXdgUserDir(text3, text, "XDG_TEMPLATES_DIR", "Templates");
			case Environment.SpecialFolder.ApplicationData:
				return text3;
			case Environment.SpecialFolder.LocalApplicationData:
				return text2;
			case Environment.SpecialFolder.InternetCache:
				if (Environment.Platform == PlatformID.MacOSX)
				{
					return Path.Combine(text, "Library", "Caches");
				}
				return string.Empty;
			case Environment.SpecialFolder.CommonApplicationData:
				return "/usr/share";
			case Environment.SpecialFolder.ProgramFiles:
				if (Environment.Platform == PlatformID.MacOSX)
				{
					return "/Applications";
				}
				return string.Empty;
			case Environment.SpecialFolder.MyPictures:
				if (Environment.Platform == PlatformID.MacOSX)
				{
					return Path.Combine(text, "Pictures");
				}
				return Environment.ReadXdgUserDir(text3, text, "XDG_PICTURES_DIR", "Pictures");
			case Environment.SpecialFolder.UserProfile:
				return text;
			case Environment.SpecialFolder.CommonTemplates:
				return "/usr/share/templates";
			}
			throw new ArgumentException("Invalid SpecialFolder");
		}

		/// <summary>Returns an array of string containing the names of the logical drives on the current computer.</summary>
		/// <returns>An array of strings where each element contains the name of a logical drive. For example, if the computer's hard drive is the first logical drive, the first element returned is "C:\".</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permissions.</exception>
		// Token: 0x06001742 RID: 5954 RVA: 0x0005B42F File Offset: 0x0005962F
		[EnvironmentPermission(SecurityAction.Demand, Unrestricted = true)]
		public static string[] GetLogicalDrives()
		{
			return Environment.GetLogicalDrivesInternal();
		}

		/// <summary>Retrieves the value of an environment variable from the current process or from the Windows operating system registry key for the current user or local machine.</summary>
		/// <param name="variable">The name of an environment variable.</param>
		/// <param name="target">One of the <see cref="T:System.EnvironmentVariableTarget" /> values.</param>
		/// <returns>The value of the environment variable specified by the <paramref name="variable" /> and <paramref name="target" /> parameters, or <see langword="null" /> if the environment variable is not found.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="variable" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> is not a valid <see cref="T:System.EnvironmentVariableTarget" /> value.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation.</exception>
		// Token: 0x06001743 RID: 5955 RVA: 0x0005B436 File Offset: 0x00059636
		public static string GetEnvironmentVariable(string variable, EnvironmentVariableTarget target)
		{
			if (target == EnvironmentVariableTarget.Process)
			{
				return Environment.GetEnvironmentVariable(variable);
			}
			return null;
		}

		/// <summary>Retrieves all environment variable names and their values from the current process, or from the Windows operating system registry key for the current user or local machine.</summary>
		/// <param name="target">One of the <see cref="T:System.EnvironmentVariableTarget" /> values.</param>
		/// <returns>A dictionary that contains all environment variable names and their values from the source specified by the <paramref name="target" /> parameter; otherwise, an empty dictionary if no environment variables are found.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation for the specified value of <paramref name="target" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="target" /> contains an illegal value.</exception>
		// Token: 0x06001744 RID: 5956 RVA: 0x0005B443 File Offset: 0x00059643
		public static IDictionary GetEnvironmentVariables(EnvironmentVariableTarget target)
		{
			if (target == EnvironmentVariableTarget.Process)
			{
				return Environment.GetEnvironmentVariables();
			}
			return new Hashtable();
		}

		/// <summary>Creates, modifies, or deletes an environment variable stored in the current process.</summary>
		/// <param name="variable">The name of an environment variable.</param>
		/// <param name="value">A value to assign to <paramref name="variable" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="variable" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="variable" /> contains a zero-length string, an initial hexadecimal zero character (0x00), or an equal sign ("="). -or-The length of <paramref name="variable" /> or <paramref name="value" /> is greater than or equal to 32,767 characters.-or-An error occurred during the execution of this operation.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation.</exception>
		// Token: 0x06001745 RID: 5957 RVA: 0x0005B454 File Offset: 0x00059654
		public static void SetEnvironmentVariable(string variable, string value)
		{
			if (variable == null)
			{
				throw new ArgumentNullException("variable");
			}
			if (variable == string.Empty)
			{
				throw new ArgumentException("String cannot be of zero length.", "variable");
			}
			if (variable.IndexOf('=') != -1)
			{
				throw new ArgumentException("Environment variable name cannot contain an equal character.", "variable");
			}
			if (variable[0] == '\0')
			{
				throw new ArgumentException("The first char in the string is the null character.", "variable");
			}
			Environment.InternalSetEnvironmentVariable(variable, value);
		}

		/// <summary>Creates, modifies, or deletes an environment variable stored in the current process or in the Windows operating system registry key reserved for the current user or local machine.</summary>
		/// <param name="variable">The name of an environment variable.</param>
		/// <param name="value">A value to assign to <paramref name="variable" />.</param>
		/// <param name="target">One of the enumeration values that specifies the location of the environment variable.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="variable" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="variable" /> contains a zero-length string, an initial hexadecimal zero character (0x00), or an equal sign ("="). -or-The length of <paramref name="variable" /> is greater than or equal to 32,767 characters.-or-
		///         <paramref name="target" /> is not a member of the <see cref="T:System.EnvironmentVariableTarget" /> enumeration. -or-
		///         <paramref name="target" /> is <see cref="F:System.EnvironmentVariableTarget.Machine" /> or <see cref="F:System.EnvironmentVariableTarget.User" />, and the length of <paramref name="variable" /> is greater than or equal to 255.-or-
		///         <paramref name="target" /> is <see cref="F:System.EnvironmentVariableTarget.Process" /> and the length of <paramref name="value" /> is greater than or equal to 32,767 characters. -or-An error occurred during the execution of this operation.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation.</exception>
		// Token: 0x06001746 RID: 5958 RVA: 0x0005B4C7 File Offset: 0x000596C7
		public static void SetEnvironmentVariable(string variable, string value, EnvironmentVariableTarget target)
		{
			if (target == EnvironmentVariableTarget.Process)
			{
				Environment.SetEnvironmentVariable(variable, value);
			}
		}

		// Token: 0x06001747 RID: 5959
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalSetEnvironmentVariable(string variable, string value);

		/// <summary>Immediately terminates a process after writing a message to the Windows Application event log, and then includes the message in error reporting to Microsoft.</summary>
		/// <param name="message">A message that explains why the process was terminated, or <see langword="null" /> if no explanation is provided.</param>
		// Token: 0x06001748 RID: 5960 RVA: 0x000041F3 File Offset: 0x000023F3
		[SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode = true)]
		public static void FailFast(string message)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001749 RID: 5961 RVA: 0x000041F3 File Offset: 0x000023F3
		internal static void FailFast(string message, uint exitCode)
		{
			throw new NotImplementedException();
		}

		/// <summary>Immediately terminates a process after writing a message to the Windows Application event log, and then includes the message and exception information in error reporting to Microsoft.</summary>
		/// <param name="message">A message that explains why the process was terminated, or <see langword="null" /> if no explanation is provided.</param>
		/// <param name="exception">An exception that represents the error that caused the termination. This is typically the exception in a <see langword="catch" /> block.</param>
		// Token: 0x0600174A RID: 5962 RVA: 0x0005B4D3 File Offset: 0x000596D3
		[SecurityCritical]
		public static void FailFast(string message, Exception exception)
		{
			throw new ExecutionEngineException(message, exception);
		}

		// Token: 0x0600174B RID: 5963
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetIs64BitOperatingSystem();

		/// <summary>Determines whether the current operating system is a 64-bit operating system.</summary>
		/// <returns>
		///     <see langword="true" /> if the operating system is 64-bit; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x0600174C RID: 5964 RVA: 0x0005B4DC File Offset: 0x000596DC
		public static bool Is64BitOperatingSystem
		{
			get
			{
				return Environment.GetIs64BitOperatingSystem();
			}
		}

		/// <summary>Gets the number of bytes in the operating system's memory page.</summary>
		/// <returns>The number of bytes in the system memory page.</returns>
		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x0600174D RID: 5965 RVA: 0x0005B4E3 File Offset: 0x000596E3
		public static int SystemPageSize
		{
			get
			{
				return Environment.GetPageSize();
			}
		}

		/// <summary>Determines whether the current process is a 64-bit process.</summary>
		/// <returns>
		///     <see langword="true" /> if the process is 64-bit; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x0600174E RID: 5966 RVA: 0x0005B4EA File Offset: 0x000596EA
		public static bool Is64BitProcess
		{
			get
			{
				return IntPtr.Size == 8;
			}
		}

		/// <summary>Gets the number of processors on the current machine.</summary>
		/// <returns>The 32-bit signed integer that specifies the number of processors on the current machine. There is no default. If the current machine contains multiple processor groups, this property returns the number of logical processors that are available for use by the common language runtime (CLR).</returns>
		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x0600174F RID: 5967
		public static extern int ProcessorCount { [EnvironmentPermission(SecurityAction.Demand, Read = "NUMBER_OF_PROCESSORS")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06001750 RID: 5968 RVA: 0x0005B4F4 File Offset: 0x000596F4
		internal static bool IsRunningOnWindows
		{
			get
			{
				return Environment.Platform < PlatformID.Unix;
			}
		}

		// Token: 0x06001751 RID: 5969
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetLogicalDrivesInternal();

		// Token: 0x06001752 RID: 5970
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetEnvironmentVariableNames();

		// Token: 0x06001753 RID: 5971
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetMachineConfigPath();

		// Token: 0x06001754 RID: 5972
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string internalGetHome();

		// Token: 0x06001755 RID: 5973
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetPageSize();

		// Token: 0x06001756 RID: 5974
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string get_bundled_machine_config();

		// Token: 0x06001757 RID: 5975 RVA: 0x0005B4FE File Offset: 0x000596FE
		internal static string GetBundledMachineConfig()
		{
			return Environment.get_bundled_machine_config();
		}

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06001758 RID: 5976 RVA: 0x0005B508 File Offset: 0x00059708
		internal static bool IsUnix
		{
			get
			{
				int platform = (int)Environment.Platform;
				return platform == 4 || platform == 128 || platform == 6;
			}
		}

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06001759 RID: 5977 RVA: 0x0005B52D File Offset: 0x0005972D
		internal static bool IsMacOS
		{
			get
			{
				return Environment.Platform == PlatformID.MacOSX;
			}
		}

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x0600175A RID: 5978 RVA: 0x00002526 File Offset: 0x00000726
		internal static bool IsCLRHosted
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0600175B RID: 5979 RVA: 0x000020D3 File Offset: 0x000002D3
		internal static void TriggerCodeContractFailure(ContractFailureKind failureKind, string message, string condition, string exceptionAsString)
		{
		}

		// Token: 0x0600175C RID: 5980 RVA: 0x0005B538 File Offset: 0x00059738
		internal static string GetStackTrace(Exception e, bool needFileInfo)
		{
			StackTrace stackTrace;
			if (e == null)
			{
				stackTrace = new StackTrace(needFileInfo);
			}
			else
			{
				stackTrace = new StackTrace(e, needFileInfo);
			}
			return stackTrace.ToString(System.Diagnostics.StackTrace.TraceFormat.Normal);
		}

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x0600175D RID: 5981 RVA: 0x00004E08 File Offset: 0x00003008
		internal static bool IsWinRTSupported
		{
			get
			{
				return true;
			}
		}

		// Token: 0x04000BDA RID: 3034
		private const int mono_corlib_version = 1051100001;

		// Token: 0x04000BDB RID: 3035
		private static string nl;

		// Token: 0x04000BDC RID: 3036
		private static OperatingSystem os;

		/// <summary>Specifies enumerated constants used to retrieve directory paths to system special folders.</summary>
		// Token: 0x020001E4 RID: 484
		[ComVisible(true)]
		public enum SpecialFolder
		{
			/// <summary>The My Documents folder.</summary>
			// Token: 0x04000BDE RID: 3038
			MyDocuments = 5,
			/// <summary>The logical Desktop rather than the physical file system location.</summary>
			// Token: 0x04000BDF RID: 3039
			Desktop = 0,
			/// <summary>The My Computer folder. </summary>
			// Token: 0x04000BE0 RID: 3040
			MyComputer = 17,
			/// <summary>The directory that contains the user's program groups.</summary>
			// Token: 0x04000BE1 RID: 3041
			Programs = 2,
			/// <summary>The directory that serves as a common repository for documents.</summary>
			// Token: 0x04000BE2 RID: 3042
			Personal = 5,
			/// <summary>The directory that serves as a common repository for the user's favorite items.</summary>
			// Token: 0x04000BE3 RID: 3043
			Favorites,
			/// <summary>The directory that corresponds to the user's Startup program group.</summary>
			// Token: 0x04000BE4 RID: 3044
			Startup,
			/// <summary>The directory that contains the user's most recently used documents.</summary>
			// Token: 0x04000BE5 RID: 3045
			Recent,
			/// <summary>The directory that contains the Send To menu items.</summary>
			// Token: 0x04000BE6 RID: 3046
			SendTo,
			/// <summary>The directory that contains the Start menu items.</summary>
			// Token: 0x04000BE7 RID: 3047
			StartMenu = 11,
			/// <summary>The My Music folder.</summary>
			// Token: 0x04000BE8 RID: 3048
			MyMusic = 13,
			/// <summary>The directory used to physically store file objects on the desktop.</summary>
			// Token: 0x04000BE9 RID: 3049
			DesktopDirectory = 16,
			/// <summary>The directory that serves as a common repository for document templates.</summary>
			// Token: 0x04000BEA RID: 3050
			Templates = 21,
			/// <summary>The directory that serves as a common repository for application-specific data for the current roaming user.</summary>
			// Token: 0x04000BEB RID: 3051
			ApplicationData = 26,
			/// <summary>The directory that serves as a common repository for application-specific data that is used by the current, non-roaming user.</summary>
			// Token: 0x04000BEC RID: 3052
			LocalApplicationData = 28,
			/// <summary>The directory that serves as a common repository for temporary Internet files.</summary>
			// Token: 0x04000BED RID: 3053
			InternetCache = 32,
			/// <summary>The directory that serves as a common repository for Internet cookies.</summary>
			// Token: 0x04000BEE RID: 3054
			Cookies,
			/// <summary>The directory that serves as a common repository for Internet history items.</summary>
			// Token: 0x04000BEF RID: 3055
			History,
			/// <summary>The directory that serves as a common repository for application-specific data that is used by all users.</summary>
			// Token: 0x04000BF0 RID: 3056
			CommonApplicationData,
			/// <summary>The System directory.</summary>
			// Token: 0x04000BF1 RID: 3057
			System = 37,
			/// <summary>The program files directory.On a non-x86 system, passing <see cref="F:System.Environment.SpecialFolder.ProgramFiles" /> to the <see cref="M:System.Environment.GetFolderPath(System.Environment.SpecialFolder)" /> method returns the path for non-x86 programs. To get the x86 program files directory on a non-x86 system, use the <see cref="F:System.Environment.SpecialFolder.ProgramFilesX86" /> member. </summary>
			// Token: 0x04000BF2 RID: 3058
			ProgramFiles,
			/// <summary>The My Pictures folder.</summary>
			// Token: 0x04000BF3 RID: 3059
			MyPictures,
			/// <summary>The directory for components that are shared across applications.To get the x86 common program files directory on a non-x86 system, use the <see cref="F:System.Environment.SpecialFolder.ProgramFilesX86" /> member. </summary>
			// Token: 0x04000BF4 RID: 3060
			CommonProgramFiles = 43,
			/// <summary>The file system directory that serves as a repository for videos that belong to a user.  Added in the .NET Framework 4. </summary>
			// Token: 0x04000BF5 RID: 3061
			MyVideos = 14,
			/// <summary>A file system directory that contains the link objects that may exist in the My Network Places virtual folder. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BF6 RID: 3062
			NetworkShortcuts = 19,
			/// <summary>A virtual folder that contains fonts. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BF7 RID: 3063
			Fonts,
			/// <summary>The file system directory that contains the programs and folders that appear on the Start menu for all users. This special folder is valid only for Windows NT systems. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BF8 RID: 3064
			CommonStartMenu = 22,
			/// <summary>A folder for components that are shared across applications. This special folder is valid only for Windows NT, Windows 2000, and Windows XP systems. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BF9 RID: 3065
			CommonPrograms,
			/// <summary>The file system directory that contains the programs that appear in the Startup folder for all users. This special folder is valid only for Windows NT systems. Added in the .NET Framework 4.</summary>
			// Token: 0x04000BFA RID: 3066
			CommonStartup,
			/// <summary>The file system directory that contains files and folders that appear on the desktop for all users. This special folder is valid only for Windows NT systems. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BFB RID: 3067
			CommonDesktopDirectory,
			/// <summary>The file system directory that contains the link objects that can exist in the Printers virtual folder. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BFC RID: 3068
			PrinterShortcuts = 27,
			/// <summary>The Windows directory or SYSROOT. This corresponds to the %windir% or %SYSTEMROOT% environment variables. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BFD RID: 3069
			Windows = 36,
			/// <summary>The user's profile folder. Applications should not create files or folders at this level; they should put their data under the locations referred to by <see cref="F:System.Environment.SpecialFolder.ApplicationData" />. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BFE RID: 3070
			UserProfile = 40,
			/// <summary>The Windows System folder. Added in the .NET Framework 4. </summary>
			// Token: 0x04000BFF RID: 3071
			SystemX86,
			/// <summary>The x86 Program Files folder. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C00 RID: 3072
			ProgramFilesX86,
			/// <summary>The Program Files folder. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C01 RID: 3073
			CommonProgramFilesX86 = 44,
			/// <summary>The file system directory that contains the templates that are available to all users. This special folder is valid only for Windows NT systems.  Added in the .NET Framework 4. </summary>
			// Token: 0x04000C02 RID: 3074
			CommonTemplates,
			/// <summary>The file system directory that contains documents that are common to all users. This special folder is valid for Windows NT systems, Windows 95, and Windows 98 systems with Shfolder.dll installed. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C03 RID: 3075
			CommonDocuments,
			/// <summary>The file system directory that contains administrative tools for all users of the computer. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C04 RID: 3076
			CommonAdminTools,
			/// <summary>The file system directory that is used to store administrative tools for an individual user. The Microsoft Management Console (MMC) will save customized consoles to this directory, and it will roam with the user. Added in the .NET Framework 4.</summary>
			// Token: 0x04000C05 RID: 3077
			AdminTools,
			/// <summary>The file system directory that serves as a repository for music files common to all users. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C06 RID: 3078
			CommonMusic = 53,
			/// <summary>The file system directory that serves as a repository for image files common to all users. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C07 RID: 3079
			CommonPictures,
			/// <summary>The file system directory that serves as a repository for video files common to all users. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C08 RID: 3080
			CommonVideos,
			/// <summary>The file system directory that contains resource data. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C09 RID: 3081
			Resources,
			/// <summary>The file system directory that contains localized resource data. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C0A RID: 3082
			LocalizedResources,
			/// <summary>This value is recognized in Windows Vista for backward compatibility, but the special folder itself is no longer used. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C0B RID: 3083
			CommonOemLinks,
			/// <summary>The file system directory that acts as a staging area for files waiting to be written to a CD. Added in the .NET Framework 4. </summary>
			// Token: 0x04000C0C RID: 3084
			CDBurning
		}

		/// <summary>Specifies options to use for getting the path to a special folder. </summary>
		// Token: 0x020001E5 RID: 485
		public enum SpecialFolderOption
		{
			/// <summary>The path to the folder is verified. If the folder exists, the path is returned. If the folder does not exist, an empty string is returned. This is the default behavior. </summary>
			// Token: 0x04000C0E RID: 3086
			None,
			/// <summary>The path to the folder is returned without verifying whether the path exists. If the folder is located on a network, specifying this option can reduce lag time. </summary>
			// Token: 0x04000C0F RID: 3087
			DoNotVerify = 16384,
			/// <summary>The path to the folder is created if it does not already exist.</summary>
			// Token: 0x04000C10 RID: 3088
			Create = 32768
		}
	}
}
