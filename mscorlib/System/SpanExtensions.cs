﻿using System;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x020000BE RID: 190
	public static class SpanExtensions
	{
		// Token: 0x06000683 RID: 1667 RVA: 0x00022F94 File Offset: 0x00021194
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Span<byte> AsBytes<T>(this Span<T> source) where T : struct
		{
			if (SpanHelpers.IsReferenceOrContainsReferences<T>())
			{
				ThrowHelper.ThrowArgumentException_InvalidTypeWithPointersNotSupported(typeof(T));
			}
			int length = checked(source.Length * Unsafe.SizeOf<T>());
			return new Span<byte>(Unsafe.As<Pinnable<byte>>(source.Pinnable), source.ByteOffset, length);
		}

		// Token: 0x06000684 RID: 1668 RVA: 0x00022FE0 File Offset: 0x000211E0
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ReadOnlySpan<byte> AsBytes<T>(this ReadOnlySpan<T> source) where T : struct
		{
			if (SpanHelpers.IsReferenceOrContainsReferences<T>())
			{
				ThrowHelper.ThrowArgumentException_InvalidTypeWithPointersNotSupported(typeof(T));
			}
			int length = checked(source.Length * Unsafe.SizeOf<T>());
			return new ReadOnlySpan<byte>(Unsafe.As<Pinnable<byte>>(source.Pinnable), source.ByteOffset, length);
		}

		// Token: 0x06000685 RID: 1669 RVA: 0x0002302A File Offset: 0x0002122A
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ReadOnlySpan<char> AsReadOnlySpan(this string text)
		{
			if (text == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.text);
			}
			return new ReadOnlySpan<char>(Unsafe.As<Pinnable<char>>(text), SpanExtensions.StringAdjustment, text.Length);
		}

		// Token: 0x06000686 RID: 1670 RVA: 0x0002304C File Offset: 0x0002124C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Span<TTo> NonPortableCast<TFrom, TTo>(this Span<TFrom> source) where TFrom : struct where TTo : struct
		{
			if (SpanHelpers.IsReferenceOrContainsReferences<TFrom>())
			{
				ThrowHelper.ThrowArgumentException_InvalidTypeWithPointersNotSupported(typeof(TFrom));
			}
			if (SpanHelpers.IsReferenceOrContainsReferences<TTo>())
			{
				ThrowHelper.ThrowArgumentException_InvalidTypeWithPointersNotSupported(typeof(TTo));
			}
			int length = checked((int)(unchecked((long)source.Length) * unchecked((long)Unsafe.SizeOf<TFrom>()) / unchecked((long)Unsafe.SizeOf<TTo>())));
			return new Span<TTo>(Unsafe.As<Pinnable<TTo>>(source.Pinnable), source.ByteOffset, length);
		}

		// Token: 0x06000687 RID: 1671 RVA: 0x000230B8 File Offset: 0x000212B8
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ReadOnlySpan<TTo> NonPortableCast<TFrom, TTo>(this ReadOnlySpan<TFrom> source) where TFrom : struct where TTo : struct
		{
			if (SpanHelpers.IsReferenceOrContainsReferences<TFrom>())
			{
				ThrowHelper.ThrowArgumentException_InvalidTypeWithPointersNotSupported(typeof(TFrom));
			}
			if (SpanHelpers.IsReferenceOrContainsReferences<TTo>())
			{
				ThrowHelper.ThrowArgumentException_InvalidTypeWithPointersNotSupported(typeof(TTo));
			}
			int length = checked((int)(unchecked((long)source.Length) * unchecked((long)Unsafe.SizeOf<TFrom>()) / unchecked((long)Unsafe.SizeOf<TTo>())));
			return new ReadOnlySpan<TTo>(Unsafe.As<Pinnable<TTo>>(source.Pinnable), source.ByteOffset, length);
		}

		// Token: 0x06000688 RID: 1672 RVA: 0x00023124 File Offset: 0x00021324
		private unsafe static IntPtr MeasureStringAdjustment()
		{
			string text;
			object o = text = "a";
			char* ptr = text;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			return Unsafe.ByteOffset<char>(ref Unsafe.As<Pinnable<char>>(o).Data, Unsafe.AsRef<char>((void*)ptr));
		}

		// Token: 0x06000689 RID: 1673 RVA: 0x0002315B File Offset: 0x0002135B
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOf<T>(this Span<T> span, T value) where T : struct, IEquatable<T>
		{
			return SpanHelpers.IndexOf<T>(span.DangerousGetPinnableReference(), value, span.Length);
		}

		// Token: 0x0600068A RID: 1674 RVA: 0x00023171 File Offset: 0x00021371
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOf(this Span<byte> span, byte value)
		{
			return SpanHelpers.IndexOf(span.DangerousGetPinnableReference(), value, span.Length);
		}

		// Token: 0x0600068B RID: 1675 RVA: 0x00023187 File Offset: 0x00021387
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOf<T>(this Span<T> span, ReadOnlySpan<T> value) where T : struct, IEquatable<T>
		{
			return SpanHelpers.IndexOf<T>(span.DangerousGetPinnableReference(), span.Length, value.DangerousGetPinnableReference(), value.Length);
		}

		// Token: 0x0600068C RID: 1676 RVA: 0x000231AA File Offset: 0x000213AA
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOf(this Span<byte> span, ReadOnlySpan<byte> value)
		{
			return SpanHelpers.IndexOf(span.DangerousGetPinnableReference(), span.Length, value.DangerousGetPinnableReference(), value.Length);
		}

		// Token: 0x0600068D RID: 1677 RVA: 0x000231D0 File Offset: 0x000213D0
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool SequenceEqual<T>(this Span<T> first, ReadOnlySpan<T> second) where T : struct, IEquatable<T>
		{
			int length = first.Length;
			return length == second.Length && SpanHelpers.SequenceEqual<T>(first.DangerousGetPinnableReference(), second.DangerousGetPinnableReference(), length);
		}

		// Token: 0x0600068E RID: 1678 RVA: 0x00023208 File Offset: 0x00021408
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool SequenceEqual(this Span<byte> first, ReadOnlySpan<byte> second)
		{
			int length = first.Length;
			return length == second.Length && SpanHelpers.SequenceEqual(first.DangerousGetPinnableReference(), second.DangerousGetPinnableReference(), length);
		}

		// Token: 0x0600068F RID: 1679 RVA: 0x0002323D File Offset: 0x0002143D
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOf<T>(this ReadOnlySpan<T> span, T value) where T : struct, IEquatable<T>
		{
			return SpanHelpers.IndexOf<T>(span.DangerousGetPinnableReference(), value, span.Length);
		}

		// Token: 0x06000690 RID: 1680 RVA: 0x00023253 File Offset: 0x00021453
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOf(this ReadOnlySpan<byte> span, byte value)
		{
			return SpanHelpers.IndexOf(span.DangerousGetPinnableReference(), value, span.Length);
		}

		// Token: 0x06000691 RID: 1681 RVA: 0x00023269 File Offset: 0x00021469
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOf<T>(this ReadOnlySpan<T> span, ReadOnlySpan<T> value) where T : struct, IEquatable<T>
		{
			return SpanHelpers.IndexOf<T>(span.DangerousGetPinnableReference(), span.Length, value.DangerousGetPinnableReference(), value.Length);
		}

		// Token: 0x06000692 RID: 1682 RVA: 0x0002328C File Offset: 0x0002148C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOf(this ReadOnlySpan<byte> span, ReadOnlySpan<byte> value)
		{
			return SpanHelpers.IndexOf(span.DangerousGetPinnableReference(), span.Length, value.DangerousGetPinnableReference(), value.Length);
		}

		// Token: 0x06000693 RID: 1683 RVA: 0x000232AF File Offset: 0x000214AF
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOfAny(this Span<byte> span, byte value0, byte value1)
		{
			return SpanHelpers.IndexOfAny(span.DangerousGetPinnableReference(), value0, value1, span.Length);
		}

		// Token: 0x06000694 RID: 1684 RVA: 0x000232C6 File Offset: 0x000214C6
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOfAny(this Span<byte> span, byte value0, byte value1, byte value2)
		{
			return SpanHelpers.IndexOfAny(span.DangerousGetPinnableReference(), value0, value1, value2, span.Length);
		}

		// Token: 0x06000695 RID: 1685 RVA: 0x000232DE File Offset: 0x000214DE
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOfAny(this Span<byte> span, ReadOnlySpan<byte> values)
		{
			return SpanHelpers.IndexOfAny(span.DangerousGetPinnableReference(), span.Length, values.DangerousGetPinnableReference(), values.Length);
		}

		// Token: 0x06000696 RID: 1686 RVA: 0x00023301 File Offset: 0x00021501
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOfAny(this ReadOnlySpan<byte> span, byte value0, byte value1)
		{
			return SpanHelpers.IndexOfAny(span.DangerousGetPinnableReference(), value0, value1, span.Length);
		}

		// Token: 0x06000697 RID: 1687 RVA: 0x00023318 File Offset: 0x00021518
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOfAny(this ReadOnlySpan<byte> span, byte value0, byte value1, byte value2)
		{
			return SpanHelpers.IndexOfAny(span.DangerousGetPinnableReference(), value0, value1, value2, span.Length);
		}

		// Token: 0x06000698 RID: 1688 RVA: 0x00023330 File Offset: 0x00021530
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int IndexOfAny(this ReadOnlySpan<byte> span, ReadOnlySpan<byte> values)
		{
			return SpanHelpers.IndexOfAny(span.DangerousGetPinnableReference(), span.Length, values.DangerousGetPinnableReference(), values.Length);
		}

		// Token: 0x06000699 RID: 1689 RVA: 0x00023354 File Offset: 0x00021554
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool SequenceEqual<T>(this ReadOnlySpan<T> first, ReadOnlySpan<T> second) where T : struct, IEquatable<T>
		{
			int length = first.Length;
			return length == second.Length && SpanHelpers.SequenceEqual<T>(first.DangerousGetPinnableReference(), second.DangerousGetPinnableReference(), length);
		}

		// Token: 0x0600069A RID: 1690 RVA: 0x0002338C File Offset: 0x0002158C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool SequenceEqual(this ReadOnlySpan<byte> first, ReadOnlySpan<byte> second)
		{
			int length = first.Length;
			return length == second.Length && SpanHelpers.SequenceEqual(first.DangerousGetPinnableReference(), second.DangerousGetPinnableReference(), length);
		}

		// Token: 0x0600069B RID: 1691 RVA: 0x000233C4 File Offset: 0x000215C4
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool StartsWith(this Span<byte> span, ReadOnlySpan<byte> value)
		{
			int length = value.Length;
			return length <= span.Length && SpanHelpers.SequenceEqual(span.DangerousGetPinnableReference(), value.DangerousGetPinnableReference(), length);
		}

		// Token: 0x0600069C RID: 1692 RVA: 0x000233FC File Offset: 0x000215FC
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool StartsWith<T>(this Span<T> span, ReadOnlySpan<T> value) where T : struct, IEquatable<T>
		{
			int length = value.Length;
			return length <= span.Length && SpanHelpers.SequenceEqual<T>(span.DangerousGetPinnableReference(), value.DangerousGetPinnableReference(), length);
		}

		// Token: 0x0600069D RID: 1693 RVA: 0x00023434 File Offset: 0x00021634
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool StartsWith(this ReadOnlySpan<byte> span, ReadOnlySpan<byte> value)
		{
			int length = value.Length;
			return length <= span.Length && SpanHelpers.SequenceEqual(span.DangerousGetPinnableReference(), value.DangerousGetPinnableReference(), length);
		}

		// Token: 0x0600069E RID: 1694 RVA: 0x0002346C File Offset: 0x0002166C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool StartsWith<T>(this ReadOnlySpan<T> span, ReadOnlySpan<T> value) where T : struct, IEquatable<T>
		{
			int length = value.Length;
			return length <= span.Length && SpanHelpers.SequenceEqual<T>(span.DangerousGetPinnableReference(), value.DangerousGetPinnableReference(), length);
		}

		// Token: 0x0600069F RID: 1695 RVA: 0x000234A1 File Offset: 0x000216A1
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Span<T> AsSpan<T>(this T[] array)
		{
			return new Span<T>(array);
		}

		// Token: 0x060006A0 RID: 1696 RVA: 0x000234A9 File Offset: 0x000216A9
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Span<T> AsSpan<T>(this ArraySegment<T> arraySegment)
		{
			return new Span<T>(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
		}

		// Token: 0x060006A1 RID: 1697 RVA: 0x000234C5 File Offset: 0x000216C5
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ReadOnlySpan<T> AsReadOnlySpan<T>(this T[] array)
		{
			return new ReadOnlySpan<T>(array);
		}

		// Token: 0x060006A2 RID: 1698 RVA: 0x000234CD File Offset: 0x000216CD
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ReadOnlySpan<T> AsReadOnlySpan<T>(this ArraySegment<T> arraySegment)
		{
			return new ReadOnlySpan<T>(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
		}

		// Token: 0x060006A3 RID: 1699 RVA: 0x000234EC File Offset: 0x000216EC
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void CopyTo<T>(this T[] array, Span<T> destination)
		{
			new ReadOnlySpan<T>(array).CopyTo(destination);
		}

		// Token: 0x060006A4 RID: 1700 RVA: 0x00023508 File Offset: 0x00021708
		// Note: this type is marked as 'beforefieldinit'.
		static SpanExtensions()
		{
		}

		// Token: 0x04000671 RID: 1649
		private static readonly IntPtr StringAdjustment = SpanExtensions.MeasureStringAdjustment();
	}
}
