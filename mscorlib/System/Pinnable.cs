﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020000B9 RID: 185
	[StructLayout(LayoutKind.Sequential)]
	internal sealed class Pinnable<T>
	{
		// Token: 0x06000637 RID: 1591 RVA: 0x00002050 File Offset: 0x00000250
		public Pinnable()
		{
		}

		// Token: 0x04000662 RID: 1634
		public T Data;
	}
}
