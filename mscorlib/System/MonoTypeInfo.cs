﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020001E9 RID: 489
	[StructLayout(LayoutKind.Sequential)]
	internal class MonoTypeInfo
	{
		// Token: 0x0600176D RID: 5997 RVA: 0x00002050 File Offset: 0x00000250
		public MonoTypeInfo()
		{
		}

		// Token: 0x04000C1B RID: 3099
		public string full_name;

		// Token: 0x04000C1C RID: 3100
		public MonoCMethod default_ctor;
	}
}
