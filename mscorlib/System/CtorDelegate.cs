﻿using System;

namespace System
{
	// Token: 0x020001A4 RID: 420
	// (Invoke) Token: 0x060011DD RID: 4573
	internal delegate void CtorDelegate(object instance);
}
