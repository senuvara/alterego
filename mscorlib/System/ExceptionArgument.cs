﻿using System;

namespace System
{
	// Token: 0x020001B9 RID: 441
	internal enum ExceptionArgument
	{
		// Token: 0x04000AB6 RID: 2742
		obj,
		// Token: 0x04000AB7 RID: 2743
		dictionary,
		// Token: 0x04000AB8 RID: 2744
		dictionaryCreationThreshold,
		// Token: 0x04000AB9 RID: 2745
		array,
		// Token: 0x04000ABA RID: 2746
		info,
		// Token: 0x04000ABB RID: 2747
		key,
		// Token: 0x04000ABC RID: 2748
		collection,
		// Token: 0x04000ABD RID: 2749
		list,
		// Token: 0x04000ABE RID: 2750
		match,
		// Token: 0x04000ABF RID: 2751
		converter,
		// Token: 0x04000AC0 RID: 2752
		queue,
		// Token: 0x04000AC1 RID: 2753
		stack,
		// Token: 0x04000AC2 RID: 2754
		capacity,
		// Token: 0x04000AC3 RID: 2755
		index,
		// Token: 0x04000AC4 RID: 2756
		startIndex,
		// Token: 0x04000AC5 RID: 2757
		value,
		// Token: 0x04000AC6 RID: 2758
		count,
		// Token: 0x04000AC7 RID: 2759
		arrayIndex,
		// Token: 0x04000AC8 RID: 2760
		name,
		// Token: 0x04000AC9 RID: 2761
		mode,
		// Token: 0x04000ACA RID: 2762
		item,
		// Token: 0x04000ACB RID: 2763
		options,
		// Token: 0x04000ACC RID: 2764
		view,
		// Token: 0x04000ACD RID: 2765
		sourceBytesToCopy,
		// Token: 0x04000ACE RID: 2766
		start,
		// Token: 0x04000ACF RID: 2767
		pointer,
		// Token: 0x04000AD0 RID: 2768
		ownedMemory,
		// Token: 0x04000AD1 RID: 2769
		text
	}
}
