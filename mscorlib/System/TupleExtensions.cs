﻿using System;
using System.Runtime.CompilerServices;

namespace System
{
	/// <summary>Provides extension methods for tuples to interoperate with language support for tuples in C#. </summary>
	// Token: 0x020000C5 RID: 197
	public static class TupleExtensions
	{
		/// <summary>Deconstructs a tuple with 1 element into a separate variable.</summary>
		/// <param name="value">The 1-element tuple to deconstruct into a separate variable.</param>
		/// <param name="item1">The value of the single element.</param>
		/// <typeparam name="T1">The type of the single element.</typeparam>
		// Token: 0x060006D9 RID: 1753 RVA: 0x000250FB File Offset: 0x000232FB
		public static void Deconstruct<T1>(this Tuple<T1> value, out T1 item1)
		{
			item1 = value.Item1;
		}

		/// <summary>Deconstructs a tuple with 2 elements into separate variables.</summary>
		/// <param name="value">The 2-element tuple to deconstruct into 2 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		// Token: 0x060006DA RID: 1754 RVA: 0x00025109 File Offset: 0x00023309
		public static void Deconstruct<T1, T2>(this Tuple<T1, T2> value, out T1 item1, out T2 item2)
		{
			item1 = value.Item1;
			item2 = value.Item2;
		}

		/// <summary>Deconstructs a tuple with 3 elements into separate variables.
		/// </summary>
		/// <param name="value">The 3-element tuple to deconstruct into 3 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		// Token: 0x060006DB RID: 1755 RVA: 0x00025123 File Offset: 0x00023323
		public static void Deconstruct<T1, T2, T3>(this Tuple<T1, T2, T3> value, out T1 item1, out T2 item2, out T3 item3)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
		}

		/// <summary>Deconstructs a tuple with 4 elements into separate variables.</summary>
		/// <param name="value">The 4-element tuple to deconstruct into 4 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		// Token: 0x060006DC RID: 1756 RVA: 0x00025149 File Offset: 0x00023349
		public static void Deconstruct<T1, T2, T3, T4>(this Tuple<T1, T2, T3, T4> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
		}

		/// <summary>Deconstructs a tuple with 5 elements into separate variables.</summary>
		/// <param name="value">The 5-element tuple to deconstruct into 5 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		// Token: 0x060006DD RID: 1757 RVA: 0x0002517C File Offset: 0x0002337C
		public static void Deconstruct<T1, T2, T3, T4, T5>(this Tuple<T1, T2, T3, T4, T5> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
		}

		/// <summary>Deconstructs a tuple with 6 elements into separate variables.</summary>
		/// <param name="value">The 6-element tuple to deconstruct into 6 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		// Token: 0x060006DE RID: 1758 RVA: 0x000251BC File Offset: 0x000233BC
		public static void Deconstruct<T1, T2, T3, T4, T5, T6>(this Tuple<T1, T2, T3, T4, T5, T6> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
		}

		/// <summary>Deconstructs a tuple with 7 elements into separate variables.</summary>
		/// <param name="value">The 7-element tuple to deconstruct into 7 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		// Token: 0x060006DF RID: 1759 RVA: 0x00025214 File Offset: 0x00023414
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7>(this Tuple<T1, T2, T3, T4, T5, T6, T7> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
		}

		/// <summary>Deconstructs a tuple with 8 elements into separate variables.</summary>
		/// <param name="value">The 8-element tuple to deconstruct into 8 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		// Token: 0x060006E0 RID: 1760 RVA: 0x0002527C File Offset: 0x0002347C
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
		}

		/// <summary>Deconstructs a tuple with 9 elements into separate variables.</summary>
		/// <param name="value">The 9-element tuple to deconstruct into 9 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		// Token: 0x060006E1 RID: 1761 RVA: 0x000252F4 File Offset: 0x000234F4
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
		}

		/// <summary>Deconstructs a tuple with 10 elements into separate variables.</summary>
		/// <param name="value">The 10-element tuple to deconstruct into 10 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		// Token: 0x060006E2 RID: 1762 RVA: 0x00025380 File Offset: 0x00023580
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
		}

		/// <summary>Deconstructs a tuple with 11 elements into separate variables.</summary>
		/// <param name="value">The 11-element tuple to deconstruct into 11 separate variables. </param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		// Token: 0x060006E3 RID: 1763 RVA: 0x0002541C File Offset: 0x0002361C
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
		}

		/// <summary>Deconstructs a tuple with 12 elements into separate variables.</summary>
		/// <param name="value">The 12-element tuple to deconstruct into 12 separate variables. </param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		// Token: 0x060006E4 RID: 1764 RVA: 0x000254CC File Offset: 0x000236CC
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
		}

		/// <summary>Deconstructs a tuple with 13 elements into separate variables. </summary>
		/// <param name="value">The 13-element tuple to deconstruct into 13 separate variables. </param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		// Token: 0x060006E5 RID: 1765 RVA: 0x0002558C File Offset: 0x0002378C
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
		}

		/// <summary>Deconstructs a tuple with 14 elements into separate variables.</summary>
		/// <param name="value">The 14-element tuple to deconstruct into 14 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <param name="item14">The value of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element.</typeparam>
		// Token: 0x060006E6 RID: 1766 RVA: 0x00025660 File Offset: 0x00023860
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13, out T14 item14)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
			item14 = value.Rest.Item7;
		}

		/// <summary>Deconstructs a tuple with 15 elements into separate variables.</summary>
		/// <param name="value">The 15-element tuple to deconstruct into 15 separate variables. </param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <param name="item14">The value of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </param>
		/// <param name="item15">The value of the fifteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item1 " />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element.</typeparam>
		// Token: 0x060006E7 RID: 1767 RVA: 0x00025744 File Offset: 0x00023944
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15>>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13, out T14 item14, out T15 item15)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
			item14 = value.Rest.Item7;
			item15 = value.Rest.Rest.Item1;
		}

		/// <summary>Deconstructs a tuple with 16 elements into separate variables.</summary>
		/// <param name="value">The 16-element tuple to deconstruct into 16 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <param name="item14">The value of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </param>
		/// <param name="item15">The value of the fifteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item1 " />. </param>
		/// <param name="item16">The value of the sixteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element.</typeparam>
		// Token: 0x060006E8 RID: 1768 RVA: 0x00025840 File Offset: 0x00023A40
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16>>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13, out T14 item14, out T15 item15, out T16 item16)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
			item14 = value.Rest.Item7;
			item15 = value.Rest.Rest.Item1;
			item16 = value.Rest.Rest.Item2;
		}

		/// <summary>Deconstructs a tuple with 17 elements into separate variables.</summary>
		/// <param name="value">The 17-element tuple to deconstruct into 17 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <param name="item14">The value of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </param>
		/// <param name="item15">The value of the fifteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item1 " />. </param>
		/// <param name="item16">The value of the sixteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </param>
		/// <param name="item17">The value of the seventeenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element.</typeparam>
		// Token: 0x060006E9 RID: 1769 RVA: 0x00025954 File Offset: 0x00023B54
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17>>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13, out T14 item14, out T15 item15, out T16 item16, out T17 item17)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
			item14 = value.Rest.Item7;
			item15 = value.Rest.Rest.Item1;
			item16 = value.Rest.Rest.Item2;
			item17 = value.Rest.Rest.Item3;
		}

		/// <summary>Deconstructs a tuple with 18 elements into separate variables.</summary>
		/// <param name="value">The 18-element tuple to deconstruct into 18 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <param name="item14">The value of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </param>
		/// <param name="item15">The value of the fifteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item1 " />. </param>
		/// <param name="item16">The value of the sixteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </param>
		/// <param name="item17">The value of the seventeenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </param>
		/// <param name="item18">The value of the eighteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element.</typeparam>
		/// <typeparam name="T18">The type of the eighteenth element.</typeparam>
		// Token: 0x060006EA RID: 1770 RVA: 0x00025A7C File Offset: 0x00023C7C
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18>>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13, out T14 item14, out T15 item15, out T16 item16, out T17 item17, out T18 item18)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
			item14 = value.Rest.Item7;
			item15 = value.Rest.Rest.Item1;
			item16 = value.Rest.Rest.Item2;
			item17 = value.Rest.Rest.Item3;
			item18 = value.Rest.Rest.Item4;
		}

		/// <summary>Deconstructs a tuple with 19 elements into separate variables.</summary>
		/// <param name="value">The 19-element tuple to deconstruct into 19 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <param name="item14">The value of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </param>
		/// <param name="item15">The value of the fifteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item1 " />. </param>
		/// <param name="item16">The value of the sixteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </param>
		/// <param name="item17">The value of the seventeenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </param>
		/// <param name="item18">The value of the eighteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </param>
		/// <param name="item19">The value of the nineteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item5" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element.</typeparam>
		/// <typeparam name="T18">The type of the eighteenth element.</typeparam>
		/// <typeparam name="T19">The type of the nineteenth element.</typeparam>
		// Token: 0x060006EB RID: 1771 RVA: 0x00025BBC File Offset: 0x00023DBC
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19>>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13, out T14 item14, out T15 item15, out T16 item16, out T17 item17, out T18 item18, out T19 item19)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
			item14 = value.Rest.Item7;
			item15 = value.Rest.Rest.Item1;
			item16 = value.Rest.Rest.Item2;
			item17 = value.Rest.Rest.Item3;
			item18 = value.Rest.Rest.Item4;
			item19 = value.Rest.Rest.Item5;
		}

		/// <summary>Deconstructs a tuple with 20 elements into separate variables.</summary>
		/// <param name="value">The 20-element tuple to deconstruct into 20 separate variables.</param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <param name="item14">The value of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </param>
		/// <param name="item15">The value of the fifteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item1 " />. </param>
		/// <param name="item16">The value of the sixteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </param>
		/// <param name="item17">The value of the seventeenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </param>
		/// <param name="item18">The value of the eighteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </param>
		/// <param name="item19">The value of the nineteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item5" />. </param>
		/// <param name="item20">The value of the twentieth element, or <paramref name="value" /><see langword=".Rest.Rest.Item6" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element.</typeparam>
		/// <typeparam name="T18">The type of the eighteenth element.</typeparam>
		/// <typeparam name="T19">The type of the nineteenth element.</typeparam>
		/// <typeparam name="T20">The type of the twentieth element.</typeparam>
		// Token: 0x060006EC RID: 1772 RVA: 0x00025D14 File Offset: 0x00023F14
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20>>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13, out T14 item14, out T15 item15, out T16 item16, out T17 item17, out T18 item18, out T19 item19, out T20 item20)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
			item14 = value.Rest.Item7;
			item15 = value.Rest.Rest.Item1;
			item16 = value.Rest.Rest.Item2;
			item17 = value.Rest.Rest.Item3;
			item18 = value.Rest.Rest.Item4;
			item19 = value.Rest.Rest.Item5;
			item20 = value.Rest.Rest.Item6;
		}

		/// <summary>Deconstructs a tuple with 21 elements into separate variables. </summary>
		/// <param name="value">The 21-element tuple to deconstruct into 21 separate variables. </param>
		/// <param name="item1">The value of the first element. </param>
		/// <param name="item2">The value of the second element. </param>
		/// <param name="item3">The value of the third element. </param>
		/// <param name="item4">The value of the fourth element. </param>
		/// <param name="item5">The value of the fifth element. </param>
		/// <param name="item6">The value of the sixth element. </param>
		/// <param name="item7">The value of the seventh element. </param>
		/// <param name="item8">The value of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </param>
		/// <param name="item9">The value of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </param>
		/// <param name="item10">The value of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </param>
		/// <param name="item11">The value of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </param>
		/// <param name="item12">The value of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </param>
		/// <param name="item13">The value of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </param>
		/// <param name="item14">The value of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </param>
		/// <param name="item15">The value of the fifteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item1 " />. </param>
		/// <param name="item16">The value of the sixteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </param>
		/// <param name="item17">The value of the seventeenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </param>
		/// <param name="item18">The value of the eighteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </param>
		/// <param name="item19">The value of the nineteenth element, or <paramref name="value" /><see langword=".Rest.Rest.Item5" />. </param>
		/// <param name="item20">The value of the twentieth element, or <paramref name="value" /><see langword=".Rest.Rest.Item6" />. </param>
		/// <param name="item21">The value of the twenty-first element, or <paramref name="value" /><see langword=".Rest.Rest.Item7" />. </param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element.</typeparam>
		/// <typeparam name="T9">The type of the ninth element.</typeparam>
		/// <typeparam name="T10">The type of the tenth element.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element.</typeparam>
		/// <typeparam name="T18">The type of the eighteenth element.</typeparam>
		/// <typeparam name="T19">The type of the nineteenth element.</typeparam>
		/// <typeparam name="T20">The type of the twentieth element.</typeparam>
		/// <typeparam name="T21">The type of the twenty-first element.</typeparam>
		// Token: 0x060006ED RID: 1773 RVA: 0x00025E84 File Offset: 0x00024084
		public static void Deconstruct<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20, T21>>> value, out T1 item1, out T2 item2, out T3 item3, out T4 item4, out T5 item5, out T6 item6, out T7 item7, out T8 item8, out T9 item9, out T10 item10, out T11 item11, out T12 item12, out T13 item13, out T14 item14, out T15 item15, out T16 item16, out T17 item17, out T18 item18, out T19 item19, out T20 item20, out T21 item21)
		{
			item1 = value.Item1;
			item2 = value.Item2;
			item3 = value.Item3;
			item4 = value.Item4;
			item5 = value.Item5;
			item6 = value.Item6;
			item7 = value.Item7;
			item8 = value.Rest.Item1;
			item9 = value.Rest.Item2;
			item10 = value.Rest.Item3;
			item11 = value.Rest.Item4;
			item12 = value.Rest.Item5;
			item13 = value.Rest.Item6;
			item14 = value.Rest.Item7;
			item15 = value.Rest.Rest.Item1;
			item16 = value.Rest.Rest.Item2;
			item17 = value.Rest.Rest.Item3;
			item18 = value.Rest.Rest.Item4;
			item19 = value.Rest.Rest.Item5;
			item20 = value.Rest.Rest.Item6;
			item21 = value.Rest.Rest.Item7;
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006EE RID: 1774 RVA: 0x00026008 File Offset: 0x00024208
		public static ValueTuple<T1> ToValueTuple<T1>(this Tuple<T1> value)
		{
			return ValueTuple.Create<T1>(value.Item1);
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006EF RID: 1775 RVA: 0x00026015 File Offset: 0x00024215
		public static ValueTuple<T1, T2> ToValueTuple<T1, T2>(this Tuple<T1, T2> value)
		{
			return ValueTuple.Create<T1, T2>(value.Item1, value.Item2);
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F0 RID: 1776 RVA: 0x00026028 File Offset: 0x00024228
		public static ValueTuple<T1, T2, T3> ToValueTuple<T1, T2, T3>(this Tuple<T1, T2, T3> value)
		{
			return ValueTuple.Create<T1, T2, T3>(value.Item1, value.Item2, value.Item3);
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F1 RID: 1777 RVA: 0x00026041 File Offset: 0x00024241
		public static ValueTuple<T1, T2, T3, T4> ToValueTuple<T1, T2, T3, T4>(this Tuple<T1, T2, T3, T4> value)
		{
			return ValueTuple.Create<T1, T2, T3, T4>(value.Item1, value.Item2, value.Item3, value.Item4);
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F2 RID: 1778 RVA: 0x00026060 File Offset: 0x00024260
		public static ValueTuple<T1, T2, T3, T4, T5> ToValueTuple<T1, T2, T3, T4, T5>(this Tuple<T1, T2, T3, T4, T5> value)
		{
			return ValueTuple.Create<T1, T2, T3, T4, T5>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5);
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F3 RID: 1779 RVA: 0x00026085 File Offset: 0x00024285
		public static ValueTuple<T1, T2, T3, T4, T5, T6> ToValueTuple<T1, T2, T3, T4, T5, T6>(this Tuple<T1, T2, T3, T4, T5, T6> value)
		{
			return ValueTuple.Create<T1, T2, T3, T4, T5, T6>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6);
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F4 RID: 1780 RVA: 0x000260B0 File Offset: 0x000242B0
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7> ToValueTuple<T1, T2, T3, T4, T5, T6, T7>(this Tuple<T1, T2, T3, T4, T5, T6, T7> value)
		{
			return ValueTuple.Create<T1, T2, T3, T4, T5, T6, T7>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7);
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F5 RID: 1781 RVA: 0x000260E4 File Offset: 0x000242E4
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, ValueTuple.Create<T8>(value.Rest.Item1));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F6 RID: 1782 RVA: 0x00026130 File Offset: 0x00024330
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, ValueTuple.Create<T8, T9>(value.Rest.Item1, value.Rest.Item2));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F7 RID: 1783 RVA: 0x00026188 File Offset: 0x00024388
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, ValueTuple.Create<T8, T9, T10>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F8 RID: 1784 RVA: 0x000261EC File Offset: 0x000243EC
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, ValueTuple.Create<T8, T9, T10, T11>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006F9 RID: 1785 RVA: 0x0002625C File Offset: 0x0002445C
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, ValueTuple.Create<T8, T9, T10, T11, T12>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006FA RID: 1786 RVA: 0x000262D4 File Offset: 0x000244D4
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, ValueTuple.Create<T8, T9, T10, T11, T12, T13>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006FB RID: 1787 RVA: 0x00026358 File Offset: 0x00024558
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, ValueTuple.Create<T8, T9, T10, T11, T12, T13, T14>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006FC RID: 1788 RVA: 0x000263E8 File Offset: 0x000245E8
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15>>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15>>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLong<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, ValueTuple.Create<T15>(value.Rest.Rest.Item1)));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />. </typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006FD RID: 1789 RVA: 0x0002648C File Offset: 0x0002468C
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16>>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16>>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLong<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, ValueTuple.Create<T15, T16>(value.Rest.Rest.Item1, value.Rest.Rest.Item2)));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />. </typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006FE RID: 1790 RVA: 0x00026540 File Offset: 0x00024740
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17>>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17>>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLong<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, ValueTuple.Create<T15, T16, T17>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3)));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />. </typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </typeparam>
		/// <typeparam name="T18">The type of the eighteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x060006FF RID: 1791 RVA: 0x00026604 File Offset: 0x00024804
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18>>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18>>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLong<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, ValueTuple.Create<T15, T16, T17, T18>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3, value.Rest.Rest.Item4)));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />. </typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </typeparam>
		/// <typeparam name="T18">The type of the eighteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </typeparam>
		/// <typeparam name="T19">The type of the nineteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item5" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x06000700 RID: 1792 RVA: 0x000266D8 File Offset: 0x000248D8
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19>>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19>>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLong<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, ValueTuple.Create<T15, T16, T17, T18, T19>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3, value.Rest.Rest.Item4, value.Rest.Rest.Item5)));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />. </typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </typeparam>
		/// <typeparam name="T18">The type of the eighteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </typeparam>
		/// <typeparam name="T19">The type of the nineteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item5" />. </typeparam>
		/// <typeparam name="T20">The type of the twentieth element., or <paramref name="value" /><see langword=".Rest.Rest.Item6" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x06000701 RID: 1793 RVA: 0x000267BC File Offset: 0x000249BC
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19, T20>>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20>>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19, T20>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLong<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19, T20>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, ValueTuple.Create<T15, T16, T17, T18, T19, T20>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3, value.Rest.Rest.Item4, value.Rest.Rest.Item5, value.Rest.Rest.Item6)));
		}

		/// <summary>Converts an instance of the <see langword="Tuple" /> class to an instance of the  <see langword="ValueTuple" /> structure. </summary>
		/// <param name="value">The tuple object to convert to a value tuple</param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />. </typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </typeparam>
		/// <typeparam name="T18">The type of the eighteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </typeparam>
		/// <typeparam name="T19">The type of the nineteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item5" />. </typeparam>
		/// <typeparam name="T20">The type of the twentieth element., or <paramref name="value" /><see langword=".Rest.Rest.Item6" />. </typeparam>
		/// <typeparam name="T21">The type of the twenty-first element., or <paramref name="value" /><see langword=".Rest.Rest.Item7" />. </typeparam>
		/// <returns>The converted value tuple instance. </returns>
		// Token: 0x06000702 RID: 1794 RVA: 0x000268B0 File Offset: 0x00024AB0
		public static ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19, T20, T21>>> ToValueTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21>(this Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20, T21>>> value)
		{
			return TupleExtensions.CreateLong<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19, T20, T21>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLong<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19, T20, T21>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, ValueTuple.Create<T15, T16, T17, T18, T19, T20, T21>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3, value.Rest.Rest.Item4, value.Rest.Rest.Item5, value.Rest.Rest.Item6, value.Rest.Rest.Item7)));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class. </summary>
		/// <param name="value">The value tuple instance to convert to a tuple. </param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <returns>The converted tuple. </returns>
		// Token: 0x06000703 RID: 1795 RVA: 0x000269B3 File Offset: 0x00024BB3
		public static Tuple<T1> ToTuple<T1>(this ValueTuple<T1> value)
		{
			return Tuple.Create<T1>(value.Item1);
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000704 RID: 1796 RVA: 0x000269C0 File Offset: 0x00024BC0
		public static Tuple<T1, T2> ToTuple<T1, T2>(this ValueTuple<T1, T2> value)
		{
			return Tuple.Create<T1, T2>(value.Item1, value.Item2);
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000705 RID: 1797 RVA: 0x000269D3 File Offset: 0x00024BD3
		public static Tuple<T1, T2, T3> ToTuple<T1, T2, T3>(this ValueTuple<T1, T2, T3> value)
		{
			return Tuple.Create<T1, T2, T3>(value.Item1, value.Item2, value.Item3);
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000706 RID: 1798 RVA: 0x000269EC File Offset: 0x00024BEC
		public static Tuple<T1, T2, T3, T4> ToTuple<T1, T2, T3, T4>(this ValueTuple<T1, T2, T3, T4> value)
		{
			return Tuple.Create<T1, T2, T3, T4>(value.Item1, value.Item2, value.Item3, value.Item4);
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000707 RID: 1799 RVA: 0x00026A0B File Offset: 0x00024C0B
		public static Tuple<T1, T2, T3, T4, T5> ToTuple<T1, T2, T3, T4, T5>(this ValueTuple<T1, T2, T3, T4, T5> value)
		{
			return Tuple.Create<T1, T2, T3, T4, T5>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5);
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000708 RID: 1800 RVA: 0x00026A30 File Offset: 0x00024C30
		public static Tuple<T1, T2, T3, T4, T5, T6> ToTuple<T1, T2, T3, T4, T5, T6>(this ValueTuple<T1, T2, T3, T4, T5, T6> value)
		{
			return Tuple.Create<T1, T2, T3, T4, T5, T6>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6);
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000709 RID: 1801 RVA: 0x00026A5B File Offset: 0x00024C5B
		public static Tuple<T1, T2, T3, T4, T5, T6, T7> ToTuple<T1, T2, T3, T4, T5, T6, T7>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7> value)
		{
			return Tuple.Create<T1, T2, T3, T4, T5, T6, T7>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7);
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x0600070A RID: 1802 RVA: 0x00026A8C File Offset: 0x00024C8C
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, Tuple.Create<T8>(value.Rest.Item1));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x0600070B RID: 1803 RVA: 0x00026AD8 File Offset: 0x00024CD8
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, Tuple.Create<T8, T9>(value.Rest.Item1, value.Rest.Item2));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class. </summary>
		/// <param name="value">The value tuple instance to convert to a tuple. </param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <returns>The converted tuple. </returns>
		// Token: 0x0600070C RID: 1804 RVA: 0x00026B30 File Offset: 0x00024D30
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, Tuple.Create<T8, T9, T10>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x0600070D RID: 1805 RVA: 0x00026B94 File Offset: 0x00024D94
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, Tuple.Create<T8, T9, T10, T11>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x0600070E RID: 1806 RVA: 0x00026C04 File Offset: 0x00024E04
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, Tuple.Create<T8, T9, T10, T11, T12>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x0600070F RID: 1807 RVA: 0x00026C7C File Offset: 0x00024E7C
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, Tuple.Create<T8, T9, T10, T11, T12, T13>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000710 RID: 1808 RVA: 0x00026D00 File Offset: 0x00024F00
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, Tuple.Create<T8, T9, T10, T11, T12, T13, T14>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000711 RID: 1809 RVA: 0x00026D90 File Offset: 0x00024F90
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15>>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15>>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLongRef<T8, T9, T10, T11, T12, T13, T14, Tuple<T15>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, Tuple.Create<T15>(value.Rest.Rest.Item1)));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000712 RID: 1810 RVA: 0x00026E34 File Offset: 0x00025034
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16>>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16>>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLongRef<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, Tuple.Create<T15, T16>(value.Rest.Rest.Item1, value.Rest.Rest.Item2)));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000713 RID: 1811 RVA: 0x00026EE8 File Offset: 0x000250E8
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17>>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17>>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLongRef<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, Tuple.Create<T15, T16, T17>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3)));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />.</typeparam>
		/// <typeparam name="T18">The type of the eighteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item4" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000714 RID: 1812 RVA: 0x00026FAC File Offset: 0x000251AC
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18>>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18>>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLongRef<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, Tuple.Create<T15, T16, T17, T18>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3, value.Rest.Rest.Item4)));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />.</typeparam>
		/// <typeparam name="T18">The type of the eighteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item4" />.</typeparam>
		/// <typeparam name="T19">The type of the nineteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item5" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000715 RID: 1813 RVA: 0x00027080 File Offset: 0x00025280
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19>>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19>>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLongRef<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, Tuple.Create<T15, T16, T17, T18, T19>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3, value.Rest.Rest.Item4, value.Rest.Rest.Item5)));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class.</summary>
		/// <param name="value">The value tuple instance to convert to a tuple.</param>
		/// <typeparam name="T1">The type of the first element.</typeparam>
		/// <typeparam name="T2">The type of the second element.</typeparam>
		/// <typeparam name="T3">The type of the third element.</typeparam>
		/// <typeparam name="T4">The type of the fourth element.</typeparam>
		/// <typeparam name="T5">The type of the fifth element.</typeparam>
		/// <typeparam name="T6">The type of the sixth element.</typeparam>
		/// <typeparam name="T7">The type of the seventh element.</typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />.</typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />.</typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />.</typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />.</typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />.</typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />.</typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />.</typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />.</typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />.</typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />.</typeparam>
		/// <typeparam name="T18">The type of the eighteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item4" />.</typeparam>
		/// <typeparam name="T19">The type of the nineteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item5" />.</typeparam>
		/// <typeparam name="T20">The type of the twentieth element., or <paramref name="value" /><see langword=".Rest.Rest.Item6" />.</typeparam>
		/// <returns>The converted tuple.</returns>
		// Token: 0x06000716 RID: 1814 RVA: 0x00027164 File Offset: 0x00025364
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20>>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19, T20>>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLongRef<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, Tuple.Create<T15, T16, T17, T18, T19, T20>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3, value.Rest.Rest.Item4, value.Rest.Rest.Item5, value.Rest.Rest.Item6)));
		}

		/// <summary>Converts an instance of the <see langword="ValueTuple" /> structure to an instance of the  <see langword="Tuple" /> class. </summary>
		/// <param name="value">The value tuple instance to convert to a tuple. </param>
		/// <typeparam name="T1">The type of the first element. </typeparam>
		/// <typeparam name="T2">The type of the second element. </typeparam>
		/// <typeparam name="T3">The type of the third element. </typeparam>
		/// <typeparam name="T4">The type of the fourth element. </typeparam>
		/// <typeparam name="T5">The type of the fifth element. </typeparam>
		/// <typeparam name="T6">The type of the sixth element. </typeparam>
		/// <typeparam name="T7">The type of the seventh element. </typeparam>
		/// <typeparam name="T8">The type of the eighth element, or <paramref name="value" /><see langword=".Rest.Item1" />. </typeparam>
		/// <typeparam name="T9">The type of the ninth element, or <paramref name="value" /><see langword=".Rest.Item2" />. </typeparam>
		/// <typeparam name="T10">The type of the tenth element, or <paramref name="value" /><see langword=".Rest.Item3" />. </typeparam>
		/// <typeparam name="T11">The type of the eleventh element, or <paramref name="value" /><see langword=".Rest.Item4" />. </typeparam>
		/// <typeparam name="T12">The type of the twelfth element, or <paramref name="value" /><see langword=".Rest.Item5" />. </typeparam>
		/// <typeparam name="T13">The type of the thirteenth element, or <paramref name="value" /><see langword=".Rest.Item6" />. </typeparam>
		/// <typeparam name="T14">The type of the fourteenth element, or <paramref name="value" /><see langword=".Rest.Item7" />. </typeparam>
		/// <typeparam name="T15">The type of the fifteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item1" />. </typeparam>
		/// <typeparam name="T16">The type of the sixteenth element, ., or <paramref name="value" /><see langword=".Rest.Rest.Item2" />. </typeparam>
		/// <typeparam name="T17">The type of the seventeenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item3" />. </typeparam>
		/// <typeparam name="T18">The type of the eighteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item4" />. </typeparam>
		/// <typeparam name="T19">The type of the nineteenth element., or <paramref name="value" /><see langword=".Rest.Rest.Item5" />. </typeparam>
		/// <typeparam name="T20">The type of the twentieth element., or <paramref name="value" /><see langword=".Rest.Rest.Item6" />. </typeparam>
		/// <typeparam name="T21">The type of the twenty-first element., or <paramref name="value" /><see langword=".Rest.Rest.Item7" />. </typeparam>
		/// <returns>The converted tuple. </returns>
		// Token: 0x06000717 RID: 1815 RVA: 0x00027258 File Offset: 0x00025458
		public static Tuple<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20, T21>>> ToTuple<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21>(this ValueTuple<T1, T2, T3, T4, T5, T6, T7, ValueTuple<T8, T9, T10, T11, T12, T13, T14, ValueTuple<T15, T16, T17, T18, T19, T20, T21>>> value)
		{
			return TupleExtensions.CreateLongRef<T1, T2, T3, T4, T5, T6, T7, Tuple<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20, T21>>>(value.Item1, value.Item2, value.Item3, value.Item4, value.Item5, value.Item6, value.Item7, TupleExtensions.CreateLongRef<T8, T9, T10, T11, T12, T13, T14, Tuple<T15, T16, T17, T18, T19, T20, T21>>(value.Rest.Item1, value.Rest.Item2, value.Rest.Item3, value.Rest.Item4, value.Rest.Item5, value.Rest.Item6, value.Rest.Item7, Tuple.Create<T15, T16, T17, T18, T19, T20, T21>(value.Rest.Rest.Item1, value.Rest.Rest.Item2, value.Rest.Rest.Item3, value.Rest.Rest.Item4, value.Rest.Rest.Item5, value.Rest.Rest.Item6, value.Rest.Rest.Item7)));
		}

		// Token: 0x06000718 RID: 1816 RVA: 0x0002735B File Offset: 0x0002555B
		private static ValueTuple<T1, T2, T3, T4, T5, T6, T7, TRest> CreateLong<T1, T2, T3, T4, T5, T6, T7, TRest>(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, TRest rest) where TRest : struct, ITuple
		{
			return new ValueTuple<T1, T2, T3, T4, T5, T6, T7, TRest>(item1, item2, item3, item4, item5, item6, item7, rest);
		}

		// Token: 0x06000719 RID: 1817 RVA: 0x0002736E File Offset: 0x0002556E
		private static Tuple<T1, T2, T3, T4, T5, T6, T7, TRest> CreateLongRef<T1, T2, T3, T4, T5, T6, T7, TRest>(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7, TRest rest) where TRest : ITuple
		{
			return new Tuple<T1, T2, T3, T4, T5, T6, T7, TRest>(item1, item2, item3, item4, item5, item6, item7, rest);
		}
	}
}
