﻿using System;

namespace System
{
	// Token: 0x020000F3 RID: 243
	internal static class __HResults
	{
		// Token: 0x040006E2 RID: 1762
		internal const int RO_E_CLOSED = -2147483629;

		// Token: 0x040006E3 RID: 1763
		internal const int E_BOUNDS = -2147483637;

		// Token: 0x040006E4 RID: 1764
		internal const int E_CHANGED_STATE = -2147483636;

		// Token: 0x040006E5 RID: 1765
		internal const int E_FAIL = -2147467259;

		// Token: 0x040006E6 RID: 1766
		internal const int E_POINTER = -2147467261;

		// Token: 0x040006E7 RID: 1767
		internal const int E_NOTIMPL = -2147467263;

		// Token: 0x040006E8 RID: 1768
		internal const int REGDB_E_CLASSNOTREG = -2147221164;

		// Token: 0x040006E9 RID: 1769
		internal const int COR_E_AMBIGUOUSMATCH = -2147475171;

		// Token: 0x040006EA RID: 1770
		internal const int COR_E_APPDOMAINUNLOADED = -2146234348;

		// Token: 0x040006EB RID: 1771
		internal const int COR_E_APPLICATION = -2146232832;

		// Token: 0x040006EC RID: 1772
		internal const int COR_E_ARGUMENT = -2147024809;

		// Token: 0x040006ED RID: 1773
		internal const int COR_E_ARGUMENTOUTOFRANGE = -2146233086;

		// Token: 0x040006EE RID: 1774
		internal const int COR_E_ARITHMETIC = -2147024362;

		// Token: 0x040006EF RID: 1775
		internal const int COR_E_ARRAYTYPEMISMATCH = -2146233085;

		// Token: 0x040006F0 RID: 1776
		internal const int COR_E_BADIMAGEFORMAT = -2147024885;

		// Token: 0x040006F1 RID: 1777
		internal const int COR_E_TYPEUNLOADED = -2146234349;

		// Token: 0x040006F2 RID: 1778
		internal const int COR_E_CANNOTUNLOADAPPDOMAIN = -2146234347;

		// Token: 0x040006F3 RID: 1779
		internal const int COR_E_COMEMULATE = -2146233035;

		// Token: 0x040006F4 RID: 1780
		internal const int COR_E_CONTEXTMARSHAL = -2146233084;

		// Token: 0x040006F5 RID: 1781
		internal const int COR_E_DATAMISALIGNED = -2146233023;

		// Token: 0x040006F6 RID: 1782
		internal const int COR_E_TIMEOUT = -2146233083;

		// Token: 0x040006F7 RID: 1783
		internal const int COR_E_CUSTOMATTRIBUTEFORMAT = -2146232827;

		// Token: 0x040006F8 RID: 1784
		internal const int COR_E_DIVIDEBYZERO = -2147352558;

		// Token: 0x040006F9 RID: 1785
		internal const int COR_E_DUPLICATEWAITOBJECT = -2146233047;

		// Token: 0x040006FA RID: 1786
		internal const int COR_E_EXCEPTION = -2146233088;

		// Token: 0x040006FB RID: 1787
		internal const int COR_E_EXECUTIONENGINE = -2146233082;

		// Token: 0x040006FC RID: 1788
		internal const int COR_E_FIELDACCESS = -2146233081;

		// Token: 0x040006FD RID: 1789
		internal const int COR_E_FORMAT = -2146233033;

		// Token: 0x040006FE RID: 1790
		internal const int COR_E_INDEXOUTOFRANGE = -2146233080;

		// Token: 0x040006FF RID: 1791
		internal const int COR_E_INSUFFICIENTMEMORY = -2146233027;

		// Token: 0x04000700 RID: 1792
		internal const int COR_E_INSUFFICIENTEXECUTIONSTACK = -2146232968;

		// Token: 0x04000701 RID: 1793
		internal const int COR_E_INVALIDCAST = -2147467262;

		// Token: 0x04000702 RID: 1794
		internal const int COR_E_INVALIDCOMOBJECT = -2146233049;

		// Token: 0x04000703 RID: 1795
		internal const int COR_E_INVALIDFILTERCRITERIA = -2146232831;

		// Token: 0x04000704 RID: 1796
		internal const int COR_E_INVALIDOLEVARIANTTYPE = -2146233039;

		// Token: 0x04000705 RID: 1797
		internal const int COR_E_INVALIDOPERATION = -2146233079;

		// Token: 0x04000706 RID: 1798
		internal const int COR_E_INVALIDPROGRAM = -2146233030;

		// Token: 0x04000707 RID: 1799
		internal const int COR_E_KEYNOTFOUND = -2146232969;

		// Token: 0x04000708 RID: 1800
		internal const int COR_E_MARSHALDIRECTIVE = -2146233035;

		// Token: 0x04000709 RID: 1801
		internal const int COR_E_MEMBERACCESS = -2146233062;

		// Token: 0x0400070A RID: 1802
		internal const int COR_E_METHODACCESS = -2146233072;

		// Token: 0x0400070B RID: 1803
		internal const int COR_E_MISSINGFIELD = -2146233071;

		// Token: 0x0400070C RID: 1804
		internal const int COR_E_MISSINGMANIFESTRESOURCE = -2146233038;

		// Token: 0x0400070D RID: 1805
		internal const int COR_E_MISSINGMEMBER = -2146233070;

		// Token: 0x0400070E RID: 1806
		internal const int COR_E_MISSINGMETHOD = -2146233069;

		// Token: 0x0400070F RID: 1807
		internal const int COR_E_MISSINGSATELLITEASSEMBLY = -2146233034;

		// Token: 0x04000710 RID: 1808
		internal const int COR_E_MULTICASTNOTSUPPORTED = -2146233068;

		// Token: 0x04000711 RID: 1809
		internal const int COR_E_NOTFINITENUMBER = -2146233048;

		// Token: 0x04000712 RID: 1810
		internal const int COR_E_PLATFORMNOTSUPPORTED = -2146233031;

		// Token: 0x04000713 RID: 1811
		internal const int COR_E_NOTSUPPORTED = -2146233067;

		// Token: 0x04000714 RID: 1812
		internal const int COR_E_NULLREFERENCE = -2147467261;

		// Token: 0x04000715 RID: 1813
		internal const int COR_E_OBJECTDISPOSED = -2146232798;

		// Token: 0x04000716 RID: 1814
		internal const int COR_E_OPERATIONCANCELED = -2146233029;

		// Token: 0x04000717 RID: 1815
		internal const int COR_E_OUTOFMEMORY = -2147024882;

		// Token: 0x04000718 RID: 1816
		internal const int COR_E_OVERFLOW = -2146233066;

		// Token: 0x04000719 RID: 1817
		internal const int COR_E_RANK = -2146233065;

		// Token: 0x0400071A RID: 1818
		internal const int COR_E_REFLECTIONTYPELOAD = -2146232830;

		// Token: 0x0400071B RID: 1819
		internal const int COR_E_RUNTIMEWRAPPED = -2146233026;

		// Token: 0x0400071C RID: 1820
		internal const int COR_E_SAFEARRAYRANKMISMATCH = -2146233032;

		// Token: 0x0400071D RID: 1821
		internal const int COR_E_SAFEARRAYTYPEMISMATCH = -2146233037;

		// Token: 0x0400071E RID: 1822
		internal const int COR_E_SAFEHANDLEMISSINGATTRIBUTE = -2146232797;

		// Token: 0x0400071F RID: 1823
		internal const int COR_E_SECURITY = -2146233078;

		// Token: 0x04000720 RID: 1824
		internal const int COR_E_SERIALIZATION = -2146233076;

		// Token: 0x04000721 RID: 1825
		internal const int COR_E_SEMAPHOREFULL = -2146233045;

		// Token: 0x04000722 RID: 1826
		internal const int COR_E_WAITHANDLECANNOTBEOPENED = -2146233044;

		// Token: 0x04000723 RID: 1827
		internal const int COR_E_ABANDONEDMUTEX = -2146233043;

		// Token: 0x04000724 RID: 1828
		internal const int COR_E_STACKOVERFLOW = -2147023895;

		// Token: 0x04000725 RID: 1829
		internal const int COR_E_SYNCHRONIZATIONLOCK = -2146233064;

		// Token: 0x04000726 RID: 1830
		internal const int COR_E_SYSTEM = -2146233087;

		// Token: 0x04000727 RID: 1831
		internal const int COR_E_TARGET = -2146232829;

		// Token: 0x04000728 RID: 1832
		internal const int COR_E_TARGETINVOCATION = -2146232828;

		// Token: 0x04000729 RID: 1833
		internal const int COR_E_TARGETPARAMCOUNT = -2147352562;

		// Token: 0x0400072A RID: 1834
		internal const int COR_E_THREADABORTED = -2146233040;

		// Token: 0x0400072B RID: 1835
		internal const int COR_E_THREADINTERRUPTED = -2146233063;

		// Token: 0x0400072C RID: 1836
		internal const int COR_E_THREADSTATE = -2146233056;

		// Token: 0x0400072D RID: 1837
		internal const int COR_E_THREADSTOP = -2146233055;

		// Token: 0x0400072E RID: 1838
		internal const int COR_E_THREADSTART = -2146233051;

		// Token: 0x0400072F RID: 1839
		internal const int COR_E_TYPEACCESS = -2146233021;

		// Token: 0x04000730 RID: 1840
		internal const int COR_E_TYPEINITIALIZATION = -2146233036;

		// Token: 0x04000731 RID: 1841
		internal const int COR_E_TYPELOAD = -2146233054;

		// Token: 0x04000732 RID: 1842
		internal const int COR_E_ENTRYPOINTNOTFOUND = -2146233053;

		// Token: 0x04000733 RID: 1843
		internal const int COR_E_DLLNOTFOUND = -2146233052;

		// Token: 0x04000734 RID: 1844
		internal const int COR_E_UNAUTHORIZEDACCESS = -2147024891;

		// Token: 0x04000735 RID: 1845
		internal const int COR_E_UNSUPPORTEDFORMAT = -2146233053;

		// Token: 0x04000736 RID: 1846
		internal const int COR_E_VERIFICATION = -2146233075;

		// Token: 0x04000737 RID: 1847
		internal const int COR_E_HOSTPROTECTION = -2146232768;

		// Token: 0x04000738 RID: 1848
		internal const int CORSEC_E_MIN_GRANT_FAIL = -2146233321;

		// Token: 0x04000739 RID: 1849
		internal const int CORSEC_E_NO_EXEC_PERM = -2146233320;

		// Token: 0x0400073A RID: 1850
		internal const int CORSEC_E_POLICY_EXCEPTION = -2146233322;

		// Token: 0x0400073B RID: 1851
		internal const int CORSEC_E_XMLSYNTAX = -2146233320;

		// Token: 0x0400073C RID: 1852
		internal const int NTE_FAIL = -2146893792;

		// Token: 0x0400073D RID: 1853
		internal const int CORSEC_E_CRYPTO = -2146233296;

		// Token: 0x0400073E RID: 1854
		internal const int CORSEC_E_CRYPTO_UNEX_OPER = -2146233295;

		// Token: 0x0400073F RID: 1855
		internal const int DISP_E_OVERFLOW = -2147352566;

		// Token: 0x04000740 RID: 1856
		internal const int FUSION_E_REF_DEF_MISMATCH = -2146234304;

		// Token: 0x04000741 RID: 1857
		internal const int FUSION_E_INVALID_NAME = -2146234297;

		// Token: 0x04000742 RID: 1858
		internal const int TYPE_E_TYPEMISMATCH = -2147316576;
	}
}
