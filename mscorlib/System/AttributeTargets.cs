﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies the application elements on which it is valid to apply an attribute.</summary>
	// Token: 0x0200011E RID: 286
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum AttributeTargets
	{
		/// <summary>Attribute can be applied to an assembly.</summary>
		// Token: 0x04000771 RID: 1905
		Assembly = 1,
		/// <summary>Attribute can be applied to a module.</summary>
		// Token: 0x04000772 RID: 1906
		Module = 2,
		/// <summary>Attribute can be applied to a class.</summary>
		// Token: 0x04000773 RID: 1907
		Class = 4,
		/// <summary>Attribute can be applied to a structure; that is, a value type.</summary>
		// Token: 0x04000774 RID: 1908
		Struct = 8,
		/// <summary>Attribute can be applied to an enumeration.</summary>
		// Token: 0x04000775 RID: 1909
		Enum = 16,
		/// <summary>Attribute can be applied to a constructor.</summary>
		// Token: 0x04000776 RID: 1910
		Constructor = 32,
		/// <summary>Attribute can be applied to a method.</summary>
		// Token: 0x04000777 RID: 1911
		Method = 64,
		/// <summary>Attribute can be applied to a property.</summary>
		// Token: 0x04000778 RID: 1912
		Property = 128,
		/// <summary>Attribute can be applied to a field.</summary>
		// Token: 0x04000779 RID: 1913
		Field = 256,
		/// <summary>Attribute can be applied to an event.</summary>
		// Token: 0x0400077A RID: 1914
		Event = 512,
		/// <summary>Attribute can be applied to an interface.</summary>
		// Token: 0x0400077B RID: 1915
		Interface = 1024,
		/// <summary>Attribute can be applied to a parameter.</summary>
		// Token: 0x0400077C RID: 1916
		Parameter = 2048,
		/// <summary>Attribute can be applied to a delegate.</summary>
		// Token: 0x0400077D RID: 1917
		Delegate = 4096,
		/// <summary>Attribute can be applied to a return value.</summary>
		// Token: 0x0400077E RID: 1918
		ReturnValue = 8192,
		/// <summary>Attribute can be applied to a generic parameter.</summary>
		// Token: 0x0400077F RID: 1919
		GenericParameter = 16384,
		/// <summary>Attribute can be applied to any application element.</summary>
		// Token: 0x04000780 RID: 1920
		All = 32767
	}
}
