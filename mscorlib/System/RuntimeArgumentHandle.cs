﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>References a variable-length argument list.</summary>
	// Token: 0x02000220 RID: 544
	[ComVisible(true)]
	public struct RuntimeArgumentHandle
	{
		// Token: 0x04000CE8 RID: 3304
		internal IntPtr args;
	}
}
