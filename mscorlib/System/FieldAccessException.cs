﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when there is an invalid attempt to access a private or protected field inside a class.</summary>
	// Token: 0x0200014F RID: 335
	[ComVisible(true)]
	[Serializable]
	public class FieldAccessException : MemberAccessException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.FieldAccessException" /> class.</summary>
		// Token: 0x06000EEF RID: 3823 RVA: 0x0003E3CE File Offset: 0x0003C5CE
		public FieldAccessException() : base(Environment.GetResourceString("Attempted to access a field that is not accessible by the caller."))
		{
			base.SetErrorCode(-2146233081);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.FieldAccessException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		// Token: 0x06000EF0 RID: 3824 RVA: 0x0003E3EB File Offset: 0x0003C5EB
		public FieldAccessException(string message) : base(message)
		{
			base.SetErrorCode(-2146233081);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.FieldAccessException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception.</param>
		// Token: 0x06000EF1 RID: 3825 RVA: 0x0003E3FF File Offset: 0x0003C5FF
		public FieldAccessException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233081);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.FieldAccessException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06000EF2 RID: 3826 RVA: 0x0003E414 File Offset: 0x0003C614
		protected FieldAccessException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
