﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when the format of an argument is invalid, or when a composite format string is not well formed. </summary>
	// Token: 0x02000151 RID: 337
	[ComVisible(true)]
	[Serializable]
	public class FormatException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.FormatException" /> class.</summary>
		// Token: 0x06000EF4 RID: 3828 RVA: 0x0003E41E File Offset: 0x0003C61E
		public FormatException() : base(Environment.GetResourceString("One of the identified items was in an invalid format."))
		{
			base.SetErrorCode(-2146233033);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.FormatException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06000EF5 RID: 3829 RVA: 0x0003E43B File Offset: 0x0003C63B
		public FormatException(string message) : base(message)
		{
			base.SetErrorCode(-2146233033);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.FormatException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000EF6 RID: 3830 RVA: 0x0003E44F File Offset: 0x0003C64F
		public FormatException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233033);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.FormatException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06000EF7 RID: 3831 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected FormatException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
