﻿using System;

namespace System
{
	// Token: 0x0200024A RID: 586
	internal struct InputRecord
	{
		// Token: 0x04000F53 RID: 3923
		public short EventType;

		// Token: 0x04000F54 RID: 3924
		public bool KeyDown;

		// Token: 0x04000F55 RID: 3925
		public short RepeatCount;

		// Token: 0x04000F56 RID: 3926
		public short VirtualKeyCode;

		// Token: 0x04000F57 RID: 3927
		public short VirtualScanCode;

		// Token: 0x04000F58 RID: 3928
		public char Character;

		// Token: 0x04000F59 RID: 3929
		public int ControlKeyState;

		// Token: 0x04000F5A RID: 3930
		private int pad1;

		// Token: 0x04000F5B RID: 3931
		private bool pad2;
	}
}
