﻿using System;

namespace System
{
	// Token: 0x02000225 RID: 549
	internal enum TermInfoBooleans
	{
		// Token: 0x04000CF4 RID: 3316
		AutoLeftMargin,
		// Token: 0x04000CF5 RID: 3317
		AutoRightMargin,
		// Token: 0x04000CF6 RID: 3318
		NoEscCtlc,
		// Token: 0x04000CF7 RID: 3319
		CeolStandoutGlitch,
		// Token: 0x04000CF8 RID: 3320
		EatNewlineGlitch,
		// Token: 0x04000CF9 RID: 3321
		EraseOverstrike,
		// Token: 0x04000CFA RID: 3322
		GenericType,
		// Token: 0x04000CFB RID: 3323
		HardCopy,
		// Token: 0x04000CFC RID: 3324
		HasMetaKey,
		// Token: 0x04000CFD RID: 3325
		HasStatusLine,
		// Token: 0x04000CFE RID: 3326
		InsertNullGlitch,
		// Token: 0x04000CFF RID: 3327
		MemoryAbove,
		// Token: 0x04000D00 RID: 3328
		MemoryBelow,
		// Token: 0x04000D01 RID: 3329
		MoveInsertMode,
		// Token: 0x04000D02 RID: 3330
		MoveStandoutMode,
		// Token: 0x04000D03 RID: 3331
		OverStrike,
		// Token: 0x04000D04 RID: 3332
		StatusLineEscOk,
		// Token: 0x04000D05 RID: 3333
		DestTabsMagicSmso,
		// Token: 0x04000D06 RID: 3334
		TildeGlitch,
		// Token: 0x04000D07 RID: 3335
		TransparentUnderline,
		// Token: 0x04000D08 RID: 3336
		XonXoff,
		// Token: 0x04000D09 RID: 3337
		NeedsXonXoff,
		// Token: 0x04000D0A RID: 3338
		PrtrSilent,
		// Token: 0x04000D0B RID: 3339
		HardCursor,
		// Token: 0x04000D0C RID: 3340
		NonRevRmcup,
		// Token: 0x04000D0D RID: 3341
		NoPadChar,
		// Token: 0x04000D0E RID: 3342
		NonDestScrollRegion,
		// Token: 0x04000D0F RID: 3343
		CanChange,
		// Token: 0x04000D10 RID: 3344
		BackColorErase,
		// Token: 0x04000D11 RID: 3345
		HueLightnessSaturation,
		// Token: 0x04000D12 RID: 3346
		ColAddrGlitch,
		// Token: 0x04000D13 RID: 3347
		CrCancelsMicroMode,
		// Token: 0x04000D14 RID: 3348
		HasPrintWheel,
		// Token: 0x04000D15 RID: 3349
		RowAddrGlitch,
		// Token: 0x04000D16 RID: 3350
		SemiAutoRightMargin,
		// Token: 0x04000D17 RID: 3351
		CpiChangesRes,
		// Token: 0x04000D18 RID: 3352
		LpiChangesRes,
		// Token: 0x04000D19 RID: 3353
		Last
	}
}
