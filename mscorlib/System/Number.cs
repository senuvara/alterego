﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;

namespace System
{
	// Token: 0x02000195 RID: 405
	[FriendAccessAllowed]
	internal class Number
	{
		// Token: 0x0600116F RID: 4463 RVA: 0x00002050 File Offset: 0x00000250
		private Number()
		{
		}

		// Token: 0x06001170 RID: 4464
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern bool NumberBufferToDecimal(byte* number, ref decimal value);

		// Token: 0x06001171 RID: 4465
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal unsafe static extern bool NumberBufferToDouble(byte* number, ref double value);

		// Token: 0x06001172 RID: 4466 RVA: 0x00047F51 File Offset: 0x00046151
		public static string FormatDecimal(decimal value, string format, NumberFormatInfo info)
		{
			return NumberFormatter.NumberToString(format, value, info);
		}

		// Token: 0x06001173 RID: 4467 RVA: 0x00047F5B File Offset: 0x0004615B
		public static string FormatDouble(double value, string format, NumberFormatInfo info)
		{
			return NumberFormatter.NumberToString(format, value, info);
		}

		// Token: 0x06001174 RID: 4468 RVA: 0x00047F65 File Offset: 0x00046165
		public static string FormatInt32(int value, string format, NumberFormatInfo info)
		{
			return NumberFormatter.NumberToString(format, value, info);
		}

		// Token: 0x06001175 RID: 4469 RVA: 0x00047F6F File Offset: 0x0004616F
		public static string FormatUInt32(uint value, string format, NumberFormatInfo info)
		{
			return NumberFormatter.NumberToString(format, value, info);
		}

		// Token: 0x06001176 RID: 4470 RVA: 0x00047F79 File Offset: 0x00046179
		public static string FormatInt64(long value, string format, NumberFormatInfo info)
		{
			return NumberFormatter.NumberToString(format, value, info);
		}

		// Token: 0x06001177 RID: 4471 RVA: 0x00047F83 File Offset: 0x00046183
		public static string FormatUInt64(ulong value, string format, NumberFormatInfo info)
		{
			return NumberFormatter.NumberToString(format, value, info);
		}

		// Token: 0x06001178 RID: 4472 RVA: 0x00047F8D File Offset: 0x0004618D
		public static string FormatSingle(float value, string format, NumberFormatInfo info)
		{
			return NumberFormatter.NumberToString(format, value, info);
		}

		// Token: 0x06001179 RID: 4473 RVA: 0x000041F3 File Offset: 0x000023F3
		internal unsafe static string FormatNumberBuffer(byte* number, string format, NumberFormatInfo info, char* allDigits)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600117A RID: 4474 RVA: 0x00047F98 File Offset: 0x00046198
		private static bool HexNumberToInt32(ref Number.NumberBuffer number, ref int value)
		{
			uint num = 0U;
			bool result = Number.HexNumberToUInt32(ref number, ref num);
			value = (int)num;
			return result;
		}

		// Token: 0x0600117B RID: 4475 RVA: 0x00047FB4 File Offset: 0x000461B4
		private static bool HexNumberToInt64(ref Number.NumberBuffer number, ref long value)
		{
			ulong num = 0UL;
			bool result = Number.HexNumberToUInt64(ref number, ref num);
			value = (long)num;
			return result;
		}

		// Token: 0x0600117C RID: 4476 RVA: 0x00047FD0 File Offset: 0x000461D0
		[SecuritySafeCritical]
		private unsafe static bool HexNumberToUInt32(ref Number.NumberBuffer number, ref uint value)
		{
			int num = number.scale;
			if (num > 10 || num < number.precision)
			{
				return false;
			}
			char* ptr = number.digits;
			uint num2 = 0U;
			while (--num >= 0)
			{
				if (num2 > 268435455U)
				{
					return false;
				}
				num2 *= 16U;
				if (*ptr != '\0')
				{
					uint num3 = num2;
					if (*ptr != '\0')
					{
						if (*ptr >= '0' && *ptr <= '9')
						{
							num3 += (uint)(*ptr - '0');
						}
						else if (*ptr >= 'A' && *ptr <= 'F')
						{
							num3 += (uint)(*ptr - 'A' + '\n');
						}
						else
						{
							num3 += (uint)(*ptr - 'a' + '\n');
						}
						ptr++;
					}
					if (num3 < num2)
					{
						return false;
					}
					num2 = num3;
				}
			}
			value = num2;
			return true;
		}

		// Token: 0x0600117D RID: 4477 RVA: 0x0004806C File Offset: 0x0004626C
		[SecuritySafeCritical]
		private unsafe static bool HexNumberToUInt64(ref Number.NumberBuffer number, ref ulong value)
		{
			int num = number.scale;
			if (num > 20 || num < number.precision)
			{
				return false;
			}
			char* ptr = number.digits;
			ulong num2 = 0UL;
			while (--num >= 0)
			{
				if (num2 > 1152921504606846975UL)
				{
					return false;
				}
				num2 *= 16UL;
				if (*ptr != '\0')
				{
					ulong num3 = num2;
					if (*ptr != '\0')
					{
						if (*ptr >= '0' && *ptr <= '9')
						{
							num3 += (ulong)((long)(*ptr - '0'));
						}
						else if (*ptr >= 'A' && *ptr <= 'F')
						{
							num3 += (ulong)((long)(*ptr - 'A' + '\n'));
						}
						else
						{
							num3 += (ulong)((long)(*ptr - 'a' + '\n'));
						}
						ptr++;
					}
					if (num3 < num2)
					{
						return false;
					}
					num2 = num3;
				}
			}
			value = num2;
			return true;
		}

		// Token: 0x0600117E RID: 4478 RVA: 0x0004810F File Offset: 0x0004630F
		private static bool IsWhite(char ch)
		{
			return ch == ' ' || (ch >= '\t' && ch <= '\r');
		}

		// Token: 0x0600117F RID: 4479 RVA: 0x00048128 File Offset: 0x00046328
		[SecuritySafeCritical]
		private unsafe static bool NumberToInt32(ref Number.NumberBuffer number, ref int value)
		{
			int num = number.scale;
			if (num > 10 || num < number.precision)
			{
				return false;
			}
			char* digits = number.digits;
			int num2 = 0;
			while (--num >= 0)
			{
				if (num2 > 214748364)
				{
					return false;
				}
				num2 *= 10;
				if (*digits != '\0')
				{
					num2 += (int)(*(digits++) - '0');
				}
			}
			if (number.sign)
			{
				num2 = -num2;
				if (num2 > 0)
				{
					return false;
				}
			}
			else if (num2 < 0)
			{
				return false;
			}
			value = num2;
			return true;
		}

		// Token: 0x06001180 RID: 4480 RVA: 0x0004819C File Offset: 0x0004639C
		[SecuritySafeCritical]
		private unsafe static bool NumberToInt64(ref Number.NumberBuffer number, ref long value)
		{
			int num = number.scale;
			if (num > 19 || num < number.precision)
			{
				return false;
			}
			char* digits = number.digits;
			long num2 = 0L;
			while (--num >= 0)
			{
				if (num2 > 922337203685477580L)
				{
					return false;
				}
				num2 *= 10L;
				if (*digits != '\0')
				{
					num2 += (long)(*(digits++) - '0');
				}
			}
			if (number.sign)
			{
				num2 = -num2;
				if (num2 > 0L)
				{
					return false;
				}
			}
			else if (num2 < 0L)
			{
				return false;
			}
			value = num2;
			return true;
		}

		// Token: 0x06001181 RID: 4481 RVA: 0x00048218 File Offset: 0x00046418
		[SecuritySafeCritical]
		private unsafe static bool NumberToUInt32(ref Number.NumberBuffer number, ref uint value)
		{
			int num = number.scale;
			if (num > 10 || num < number.precision || number.sign)
			{
				return false;
			}
			char* digits = number.digits;
			uint num2 = 0U;
			while (--num >= 0)
			{
				if (num2 > 429496729U)
				{
					return false;
				}
				num2 *= 10U;
				if (*digits != '\0')
				{
					uint num3 = num2 + (uint)(*(digits++) - '0');
					if (num3 < num2)
					{
						return false;
					}
					num2 = num3;
				}
			}
			value = num2;
			return true;
		}

		// Token: 0x06001182 RID: 4482 RVA: 0x00048284 File Offset: 0x00046484
		[SecuritySafeCritical]
		private unsafe static bool NumberToUInt64(ref Number.NumberBuffer number, ref ulong value)
		{
			int num = number.scale;
			if (num > 20 || num < number.precision || number.sign)
			{
				return false;
			}
			char* digits = number.digits;
			ulong num2 = 0UL;
			while (--num >= 0)
			{
				if (num2 > 1844674407370955161UL)
				{
					return false;
				}
				num2 *= 10UL;
				if (*digits != '\0')
				{
					ulong num3 = num2 + (ulong)((long)(*(digits++) - '0'));
					if (num3 < num2)
					{
						return false;
					}
					num2 = num3;
				}
			}
			value = num2;
			return true;
		}

		// Token: 0x06001183 RID: 4483 RVA: 0x000482F8 File Offset: 0x000464F8
		[SecurityCritical]
		private unsafe static char* MatchChars(char* p, string str)
		{
			char* ptr = str;
			if (ptr != null)
			{
				ptr += RuntimeHelpers.OffsetToStringData / 2;
			}
			return Number.MatchChars(p, ptr);
		}

		// Token: 0x06001184 RID: 4484 RVA: 0x0004831C File Offset: 0x0004651C
		[SecurityCritical]
		private unsafe static char* MatchChars(char* p, char* str)
		{
			if (*str == '\0')
			{
				return null;
			}
			while (*str != '\0')
			{
				if (*p != *str && (*str != '\u00a0' || *p != ' '))
				{
					return null;
				}
				p++;
				str++;
			}
			return p;
		}

		// Token: 0x06001185 RID: 4485 RVA: 0x0004834C File Offset: 0x0004654C
		[SecuritySafeCritical]
		internal unsafe static decimal ParseDecimal(string value, NumberStyles options, NumberFormatInfo numfmt)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			decimal result = 0m;
			Number.StringToNumber(value, options, ref numberBuffer, numfmt, true);
			if (!Number.NumberBufferToDecimal(numberBuffer.PackForNative(), ref result))
			{
				throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for a Decimal."));
			}
			return result;
		}

		// Token: 0x06001186 RID: 4486 RVA: 0x000483A0 File Offset: 0x000465A0
		[SecuritySafeCritical]
		internal unsafe static double ParseDouble(string value, NumberStyles options, NumberFormatInfo numfmt)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			double result = 0.0;
			if (!Number.TryStringToNumber(value, options, ref numberBuffer, numfmt, false))
			{
				string text = value.Trim();
				if (text.Equals(numfmt.PositiveInfinitySymbol))
				{
					return double.PositiveInfinity;
				}
				if (text.Equals(numfmt.NegativeInfinitySymbol))
				{
					return double.NegativeInfinity;
				}
				if (text.Equals(numfmt.NaNSymbol))
				{
					return double.NaN;
				}
				throw new FormatException(Environment.GetResourceString("Input string was not in a correct format."));
			}
			else
			{
				if (!Number.NumberBufferToDouble(numberBuffer.PackForNative(), ref result))
				{
					throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for a Double."));
				}
				return result;
			}
		}

		// Token: 0x06001187 RID: 4487 RVA: 0x00048464 File Offset: 0x00046664
		[SecuritySafeCritical]
		internal unsafe static int ParseInt32(string s, NumberStyles style, NumberFormatInfo info)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			int result = 0;
			Number.StringToNumber(s, style, ref numberBuffer, info, false);
			if ((style & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				if (!Number.HexNumberToInt32(ref numberBuffer, ref result))
				{
					throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for an Int32."));
				}
			}
			else if (!Number.NumberToInt32(ref numberBuffer, ref result))
			{
				throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for an Int32."));
			}
			return result;
		}

		// Token: 0x06001188 RID: 4488 RVA: 0x000484D0 File Offset: 0x000466D0
		[SecuritySafeCritical]
		internal unsafe static long ParseInt64(string value, NumberStyles options, NumberFormatInfo numfmt)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			long result = 0L;
			Number.StringToNumber(value, options, ref numberBuffer, numfmt, false);
			if ((options & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				if (!Number.HexNumberToInt64(ref numberBuffer, ref result))
				{
					throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for an Int64."));
				}
			}
			else if (!Number.NumberToInt64(ref numberBuffer, ref result))
			{
				throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for an Int64."));
			}
			return result;
		}

		// Token: 0x06001189 RID: 4489 RVA: 0x0004853C File Offset: 0x0004673C
		[SecurityCritical]
		private unsafe static bool ParseNumber(ref char* str, NumberStyles options, ref Number.NumberBuffer number, StringBuilder sb, NumberFormatInfo numfmt, bool parseDecimal)
		{
			number.scale = 0;
			number.sign = false;
			string text = null;
			string text2 = null;
			string str2 = null;
			string str3 = null;
			bool flag = false;
			string str4;
			string str5;
			if ((options & NumberStyles.AllowCurrencySymbol) != NumberStyles.None)
			{
				text = numfmt.CurrencySymbol;
				if (numfmt.ansiCurrencySymbol != null)
				{
					text2 = numfmt.ansiCurrencySymbol;
				}
				str2 = numfmt.NumberDecimalSeparator;
				str3 = numfmt.NumberGroupSeparator;
				str4 = numfmt.CurrencyDecimalSeparator;
				str5 = numfmt.CurrencyGroupSeparator;
				flag = true;
			}
			else
			{
				str4 = numfmt.NumberDecimalSeparator;
				str5 = numfmt.NumberGroupSeparator;
			}
			int num = 0;
			bool flag2 = sb != null;
			bool flag3 = flag2 && (options & NumberStyles.AllowHexSpecifier) > NumberStyles.None;
			int num2 = flag2 ? int.MaxValue : 50;
			char* ptr = str;
			char c = *ptr;
			for (;;)
			{
				if (!Number.IsWhite(c) || (options & NumberStyles.AllowLeadingWhite) == NumberStyles.None || ((num & 1) != 0 && ((num & 1) == 0 || ((num & 32) == 0 && numfmt.NumberNegativePattern != 2))))
				{
					bool flag4;
					char* ptr2;
					if ((flag4 = ((options & NumberStyles.AllowLeadingSign) != NumberStyles.None && (num & 1) == 0)) && (ptr2 = Number.MatchChars(ptr, numfmt.PositiveSign)) != null)
					{
						num |= 1;
						ptr = ptr2 - 1;
					}
					else if (flag4 && (ptr2 = Number.MatchChars(ptr, numfmt.NegativeSign)) != null)
					{
						num |= 1;
						number.sign = true;
						ptr = ptr2 - 1;
					}
					else if (c == '(' && (options & NumberStyles.AllowParentheses) != NumberStyles.None && (num & 1) == 0)
					{
						num |= 3;
						number.sign = true;
					}
					else
					{
						if ((text == null || (ptr2 = Number.MatchChars(ptr, text)) == null) && (text2 == null || (ptr2 = Number.MatchChars(ptr, text2)) == null))
						{
							break;
						}
						num |= 32;
						text = null;
						text2 = null;
						ptr = ptr2 - 1;
					}
				}
				c = *(++ptr);
			}
			int num3 = 0;
			int num4 = 0;
			for (;;)
			{
				char* ptr2;
				if ((c >= '0' && c <= '9') || ((options & NumberStyles.AllowHexSpecifier) != NumberStyles.None && ((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))))
				{
					num |= 4;
					if (c != '0' || (num & 8) != 0 || flag3)
					{
						if (num3 < num2)
						{
							if (flag2)
							{
								sb.Append(c);
							}
							else
							{
								number.digits[(IntPtr)(num3++) * 2] = c;
							}
							if (c != '0' || parseDecimal)
							{
								num4 = num3;
							}
						}
						if ((num & 16) == 0)
						{
							number.scale++;
						}
						num |= 8;
					}
					else if ((num & 16) != 0)
					{
						number.scale--;
					}
				}
				else if ((options & NumberStyles.AllowDecimalPoint) != NumberStyles.None && (num & 16) == 0 && ((ptr2 = Number.MatchChars(ptr, str4)) != null || (flag && (num & 32) == 0 && (ptr2 = Number.MatchChars(ptr, str2)) != null)))
				{
					num |= 16;
					ptr = ptr2 - 1;
				}
				else
				{
					if ((options & NumberStyles.AllowThousands) == NumberStyles.None || (num & 4) == 0 || (num & 16) != 0 || ((ptr2 = Number.MatchChars(ptr, str5)) == null && (!flag || (num & 32) != 0 || (ptr2 = Number.MatchChars(ptr, str3)) == null)))
					{
						break;
					}
					ptr = ptr2 - 1;
				}
				c = *(++ptr);
			}
			bool flag5 = false;
			number.precision = num4;
			if (flag2)
			{
				sb.Append('\0');
			}
			else
			{
				number.digits[num4] = '\0';
			}
			if ((num & 4) != 0)
			{
				if ((c == 'E' || c == 'e') && (options & NumberStyles.AllowExponent) != NumberStyles.None)
				{
					char* ptr3 = ptr;
					c = *(++ptr);
					char* ptr2;
					if ((ptr2 = Number.MatchChars(ptr, numfmt.PositiveSign)) != null)
					{
						c = *(ptr = ptr2);
					}
					else if ((ptr2 = Number.MatchChars(ptr, numfmt.NegativeSign)) != null)
					{
						c = *(ptr = ptr2);
						flag5 = true;
					}
					if (c >= '0' && c <= '9')
					{
						int num5 = 0;
						do
						{
							num5 = num5 * 10 + (int)(c - '0');
							c = *(++ptr);
							if (num5 > 1000)
							{
								num5 = 9999;
								while (c >= '0' && c <= '9')
								{
									c = *(++ptr);
								}
							}
						}
						while (c >= '0' && c <= '9');
						if (flag5)
						{
							num5 = -num5;
						}
						number.scale += num5;
					}
					else
					{
						ptr = ptr3;
						c = *ptr;
					}
				}
				for (;;)
				{
					if (!Number.IsWhite(c) || (options & NumberStyles.AllowTrailingWhite) == NumberStyles.None)
					{
						bool flag4;
						char* ptr2;
						if ((flag4 = ((options & NumberStyles.AllowTrailingSign) != NumberStyles.None && (num & 1) == 0)) && (ptr2 = Number.MatchChars(ptr, numfmt.PositiveSign)) != null)
						{
							num |= 1;
							ptr = ptr2 - 1;
						}
						else if (flag4 && (ptr2 = Number.MatchChars(ptr, numfmt.NegativeSign)) != null)
						{
							num |= 1;
							number.sign = true;
							ptr = ptr2 - 1;
						}
						else if (c == ')' && (num & 2) != 0)
						{
							num &= -3;
						}
						else
						{
							if ((text == null || (ptr2 = Number.MatchChars(ptr, text)) == null) && (text2 == null || (ptr2 = Number.MatchChars(ptr, text2)) == null))
							{
								break;
							}
							text = null;
							text2 = null;
							ptr = ptr2 - 1;
						}
					}
					c = *(++ptr);
				}
				if ((num & 2) == 0)
				{
					if ((num & 8) == 0)
					{
						if (!parseDecimal)
						{
							number.scale = 0;
						}
						if ((num & 16) == 0)
						{
							number.sign = false;
						}
					}
					str = ptr;
					return true;
				}
			}
			str = ptr;
			return false;
		}

		// Token: 0x0600118A RID: 4490 RVA: 0x00048A60 File Offset: 0x00046C60
		[SecuritySafeCritical]
		internal unsafe static float ParseSingle(string value, NumberStyles options, NumberFormatInfo numfmt)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			double num = 0.0;
			if (!Number.TryStringToNumber(value, options, ref numberBuffer, numfmt, false))
			{
				string text = value.Trim();
				if (text.Equals(numfmt.PositiveInfinitySymbol))
				{
					return float.PositiveInfinity;
				}
				if (text.Equals(numfmt.NegativeInfinitySymbol))
				{
					return float.NegativeInfinity;
				}
				if (text.Equals(numfmt.NaNSymbol))
				{
					return float.NaN;
				}
				throw new FormatException(Environment.GetResourceString("Input string was not in a correct format."));
			}
			else
			{
				if (!Number.NumberBufferToDouble(numberBuffer.PackForNative(), ref num))
				{
					throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for a Single."));
				}
				float num2 = (float)num;
				if (float.IsInfinity(num2))
				{
					throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for a Single."));
				}
				return num2;
			}
		}

		// Token: 0x0600118B RID: 4491 RVA: 0x00048B30 File Offset: 0x00046D30
		[SecuritySafeCritical]
		internal unsafe static uint ParseUInt32(string value, NumberStyles options, NumberFormatInfo numfmt)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			uint result = 0U;
			Number.StringToNumber(value, options, ref numberBuffer, numfmt, false);
			if ((options & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				if (!Number.HexNumberToUInt32(ref numberBuffer, ref result))
				{
					throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for a UInt32."));
				}
			}
			else if (!Number.NumberToUInt32(ref numberBuffer, ref result))
			{
				throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for a UInt32."));
			}
			return result;
		}

		// Token: 0x0600118C RID: 4492 RVA: 0x00048B9C File Offset: 0x00046D9C
		[SecuritySafeCritical]
		internal unsafe static ulong ParseUInt64(string value, NumberStyles options, NumberFormatInfo numfmt)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			ulong result = 0UL;
			Number.StringToNumber(value, options, ref numberBuffer, numfmt, false);
			if ((options & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				if (!Number.HexNumberToUInt64(ref numberBuffer, ref result))
				{
					throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for a UInt64."));
				}
			}
			else if (!Number.NumberToUInt64(ref numberBuffer, ref result))
			{
				throw new OverflowException(Environment.GetResourceString("Value was either too large or too small for a UInt64."));
			}
			return result;
		}

		// Token: 0x0600118D RID: 4493 RVA: 0x00048C08 File Offset: 0x00046E08
		[SecuritySafeCritical]
		private unsafe static void StringToNumber(string str, NumberStyles options, ref Number.NumberBuffer number, NumberFormatInfo info, bool parseDecimal)
		{
			if (str == null)
			{
				throw new ArgumentNullException("String");
			}
			fixed (string text = str)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char* ptr2 = ptr;
				if (!Number.ParseNumber(ref ptr2, options, ref number, null, info, parseDecimal) || ((long)(ptr2 - ptr) < (long)str.Length && !Number.TrailingZeros(str, (int)((long)(ptr2 - ptr)))))
				{
					throw new FormatException(Environment.GetResourceString("Input string was not in a correct format."));
				}
			}
		}

		// Token: 0x0600118E RID: 4494 RVA: 0x00048C74 File Offset: 0x00046E74
		private static bool TrailingZeros(string s, int index)
		{
			for (int i = index; i < s.Length; i++)
			{
				if (s[i] != '\0')
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600118F RID: 4495 RVA: 0x00048CA0 File Offset: 0x00046EA0
		[SecuritySafeCritical]
		internal unsafe static bool TryParseDecimal(string value, NumberStyles options, NumberFormatInfo numfmt, out decimal result)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			result = 0m;
			return Number.TryStringToNumber(value, options, ref numberBuffer, numfmt, true) && Number.NumberBufferToDecimal(numberBuffer.PackForNative(), ref result);
		}

		// Token: 0x06001190 RID: 4496 RVA: 0x00048CE8 File Offset: 0x00046EE8
		[SecuritySafeCritical]
		internal unsafe static bool TryParseDouble(string value, NumberStyles options, NumberFormatInfo numfmt, out double result)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			result = 0.0;
			return Number.TryStringToNumber(value, options, ref numberBuffer, numfmt, false) && Number.NumberBufferToDouble(numberBuffer.PackForNative(), ref result);
		}

		// Token: 0x06001191 RID: 4497 RVA: 0x00048D34 File Offset: 0x00046F34
		[SecuritySafeCritical]
		internal unsafe static bool TryParseInt32(string s, NumberStyles style, NumberFormatInfo info, out int result)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			result = 0;
			if (!Number.TryStringToNumber(s, style, ref numberBuffer, info, false))
			{
				return false;
			}
			if ((style & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				if (!Number.HexNumberToInt32(ref numberBuffer, ref result))
				{
					return false;
				}
			}
			else if (!Number.NumberToInt32(ref numberBuffer, ref result))
			{
				return false;
			}
			return true;
		}

		// Token: 0x06001192 RID: 4498 RVA: 0x00048D88 File Offset: 0x00046F88
		[SecuritySafeCritical]
		internal unsafe static bool TryParseInt64(string s, NumberStyles style, NumberFormatInfo info, out long result)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			result = 0L;
			if (!Number.TryStringToNumber(s, style, ref numberBuffer, info, false))
			{
				return false;
			}
			if ((style & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				if (!Number.HexNumberToInt64(ref numberBuffer, ref result))
				{
					return false;
				}
			}
			else if (!Number.NumberToInt64(ref numberBuffer, ref result))
			{
				return false;
			}
			return true;
		}

		// Token: 0x06001193 RID: 4499 RVA: 0x00048DDC File Offset: 0x00046FDC
		[SecuritySafeCritical]
		internal unsafe static bool TryParseSingle(string value, NumberStyles options, NumberFormatInfo numfmt, out float result)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			result = 0f;
			double num = 0.0;
			if (!Number.TryStringToNumber(value, options, ref numberBuffer, numfmt, false))
			{
				return false;
			}
			if (!Number.NumberBufferToDouble(numberBuffer.PackForNative(), ref num))
			{
				return false;
			}
			float num2 = (float)num;
			if (float.IsInfinity(num2))
			{
				return false;
			}
			result = num2;
			return true;
		}

		// Token: 0x06001194 RID: 4500 RVA: 0x00048E40 File Offset: 0x00047040
		[SecuritySafeCritical]
		internal unsafe static bool TryParseUInt32(string s, NumberStyles style, NumberFormatInfo info, out uint result)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			result = 0U;
			if (!Number.TryStringToNumber(s, style, ref numberBuffer, info, false))
			{
				return false;
			}
			if ((style & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				if (!Number.HexNumberToUInt32(ref numberBuffer, ref result))
				{
					return false;
				}
			}
			else if (!Number.NumberToUInt32(ref numberBuffer, ref result))
			{
				return false;
			}
			return true;
		}

		// Token: 0x06001195 RID: 4501 RVA: 0x00048E94 File Offset: 0x00047094
		[SecuritySafeCritical]
		internal unsafe static bool TryParseUInt64(string s, NumberStyles style, NumberFormatInfo info, out ulong result)
		{
			byte* stackBuffer = stackalloc byte[(UIntPtr)Number.NumberBuffer.NumberBufferBytes];
			Number.NumberBuffer numberBuffer = new Number.NumberBuffer(stackBuffer);
			result = 0UL;
			if (!Number.TryStringToNumber(s, style, ref numberBuffer, info, false))
			{
				return false;
			}
			if ((style & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				if (!Number.HexNumberToUInt64(ref numberBuffer, ref result))
				{
					return false;
				}
			}
			else if (!Number.NumberToUInt64(ref numberBuffer, ref result))
			{
				return false;
			}
			return true;
		}

		// Token: 0x06001196 RID: 4502 RVA: 0x00048EE7 File Offset: 0x000470E7
		internal static bool TryStringToNumber(string str, NumberStyles options, ref Number.NumberBuffer number, NumberFormatInfo numfmt, bool parseDecimal)
		{
			return Number.TryStringToNumber(str, options, ref number, null, numfmt, parseDecimal);
		}

		// Token: 0x06001197 RID: 4503 RVA: 0x00048EF8 File Offset: 0x000470F8
		[SecuritySafeCritical]
		[FriendAccessAllowed]
		internal unsafe static bool TryStringToNumber(string str, NumberStyles options, ref Number.NumberBuffer number, StringBuilder sb, NumberFormatInfo numfmt, bool parseDecimal)
		{
			if (str == null)
			{
				return false;
			}
			fixed (string text = str)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				char* ptr2 = ptr;
				if (!Number.ParseNumber(ref ptr2, options, ref number, sb, numfmt, parseDecimal) || ((long)(ptr2 - ptr) < (long)str.Length && !Number.TrailingZeros(str, (int)((long)(ptr2 - ptr)))))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x04000A07 RID: 2567
		private const int NumberMaxDigits = 50;

		// Token: 0x04000A08 RID: 2568
		private const int Int32Precision = 10;

		// Token: 0x04000A09 RID: 2569
		private const int UInt32Precision = 10;

		// Token: 0x04000A0A RID: 2570
		private const int Int64Precision = 19;

		// Token: 0x04000A0B RID: 2571
		private const int UInt64Precision = 20;

		// Token: 0x02000196 RID: 406
		[FriendAccessAllowed]
		internal struct NumberBuffer
		{
			// Token: 0x06001198 RID: 4504 RVA: 0x00048F4F File Offset: 0x0004714F
			[SecurityCritical]
			public unsafe NumberBuffer(byte* stackBuffer)
			{
				this.baseAddress = stackBuffer;
				this.digits = (char*)(stackBuffer + (IntPtr)6 * 2);
				this.precision = 0;
				this.scale = 0;
				this.sign = false;
			}

			// Token: 0x06001199 RID: 4505 RVA: 0x00048F7C File Offset: 0x0004717C
			[SecurityCritical]
			public unsafe byte* PackForNative()
			{
				int* ptr = (int*)this.baseAddress;
				*ptr = this.precision;
				ptr[1] = this.scale;
				ptr[2] = (this.sign ? 1 : 0);
				return this.baseAddress;
			}

			// Token: 0x0600119A RID: 4506 RVA: 0x00048FBB File Offset: 0x000471BB
			// Note: this type is marked as 'beforefieldinit'.
			static NumberBuffer()
			{
			}

			// Token: 0x04000A0C RID: 2572
			public static readonly int NumberBufferBytes = 114 + IntPtr.Size;

			// Token: 0x04000A0D RID: 2573
			[SecurityCritical]
			private unsafe byte* baseAddress;

			// Token: 0x04000A0E RID: 2574
			[SecurityCritical]
			public unsafe char* digits;

			// Token: 0x04000A0F RID: 2575
			public int precision;

			// Token: 0x04000A10 RID: 2576
			public int scale;

			// Token: 0x04000A11 RID: 2577
			public bool sign;
		}
	}
}
