﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Security;

namespace System
{
	/// <summary>Controls the system garbage collector, a service that automatically reclaims unused memory.</summary>
	// Token: 0x02000155 RID: 341
	public static class GC
	{
		// Token: 0x06000EF8 RID: 3832
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetCollectionCount(int generation);

		// Token: 0x06000EF9 RID: 3833
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetMaxGeneration();

		// Token: 0x06000EFA RID: 3834
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalCollect(int generation);

		// Token: 0x06000EFB RID: 3835
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void RecordPressure(long bytesAllocated);

		// Token: 0x06000EFC RID: 3836
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void register_ephemeron_array(Ephemeron[] array);

		// Token: 0x06000EFD RID: 3837
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern object get_ephemeron_tombstone();

		/// <summary>Informs the runtime of a large allocation of unmanaged memory that should be taken into account when scheduling garbage collection.</summary>
		/// <param name="bytesAllocated">The incremental amount of unmanaged memory that has been allocated. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="bytesAllocated" /> is less than or equal to 0.-or-On a 32-bit computer, <paramref name="bytesAllocated" /> is larger than <see cref="F:System.Int32.MaxValue" />. </exception>
		// Token: 0x06000EFE RID: 3838 RVA: 0x0003E464 File Offset: 0x0003C664
		[SecurityCritical]
		public static void AddMemoryPressure(long bytesAllocated)
		{
			if (bytesAllocated <= 0L)
			{
				throw new ArgumentOutOfRangeException("bytesAllocated", Environment.GetResourceString("Positive number required."));
			}
			if (4 == IntPtr.Size && bytesAllocated > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("pressure", Environment.GetResourceString("Value must be non-negative and less than or equal to Int32.MaxValue."));
			}
			GC.RecordPressure(bytesAllocated);
		}

		/// <summary>Informs the runtime that unmanaged memory has been released and no longer needs to be taken into account when scheduling garbage collection.</summary>
		/// <param name="bytesAllocated">The amount of unmanaged memory that has been released. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="bytesAllocated" /> is less than or equal to 0. -or- On a 32-bit computer, <paramref name="bytesAllocated" /> is larger than <see cref="F:System.Int32.MaxValue" />. </exception>
		// Token: 0x06000EFF RID: 3839 RVA: 0x0003E4B8 File Offset: 0x0003C6B8
		[SecurityCritical]
		public static void RemoveMemoryPressure(long bytesAllocated)
		{
			if (bytesAllocated <= 0L)
			{
				throw new ArgumentOutOfRangeException("bytesAllocated", Environment.GetResourceString("Positive number required."));
			}
			if (4 == IntPtr.Size && bytesAllocated > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("bytesAllocated", Environment.GetResourceString("Value must be non-negative and less than or equal to Int32.MaxValue."));
			}
			GC.RecordPressure(-bytesAllocated);
		}

		/// <summary>Returns the current generation number of the specified object.</summary>
		/// <param name="obj">The object that generation information is retrieved for. </param>
		/// <returns>The current generation number of <paramref name="obj" />.</returns>
		// Token: 0x06000F00 RID: 3840
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetGeneration(object obj);

		/// <summary>Forces an immediate garbage collection from generation 0 through a specified generation.</summary>
		/// <param name="generation">The number of the oldest generation to be garbage collected. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="generation" /> is not valid. </exception>
		// Token: 0x06000F01 RID: 3841 RVA: 0x0003E50C File Offset: 0x0003C70C
		public static void Collect(int generation)
		{
			GC.Collect(generation, GCCollectionMode.Default);
		}

		/// <summary>Forces an immediate garbage collection of all generations. </summary>
		// Token: 0x06000F02 RID: 3842 RVA: 0x0003E515 File Offset: 0x0003C715
		[SecuritySafeCritical]
		public static void Collect()
		{
			GC.InternalCollect(GC.MaxGeneration);
		}

		/// <summary>Forces a garbage collection from generation 0 through a specified generation, at a time specified by a <see cref="T:System.GCCollectionMode" /> value.</summary>
		/// <param name="generation">The number of the oldest generation to be garbage collected. </param>
		/// <param name="mode">An enumeration value that specifies whether the garbage collection is forced (<see cref="F:System.GCCollectionMode.Default" /> or <see cref="F:System.GCCollectionMode.Forced" />) or optimized (<see cref="F:System.GCCollectionMode.Optimized" />). </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="generation" /> is not valid.-or-
		///         <paramref name="mode" /> is not one of the <see cref="T:System.GCCollectionMode" /> values.</exception>
		// Token: 0x06000F03 RID: 3843 RVA: 0x0003E521 File Offset: 0x0003C721
		[SecuritySafeCritical]
		public static void Collect(int generation, GCCollectionMode mode)
		{
			GC.Collect(generation, mode, true);
		}

		/// <summary>Forces a garbage collection from generation 0 through a specified generation, at a time specified by a <see cref="T:System.GCCollectionMode" /> value, with a value specifying whether the collection should be blocking.</summary>
		/// <param name="generation">The number of the oldest generation to be garbage collected. </param>
		/// <param name="mode">An enumeration value that specifies whether the garbage collection is forced (<see cref="F:System.GCCollectionMode.Default" /> or <see cref="F:System.GCCollectionMode.Forced" />) or optimized (<see cref="F:System.GCCollectionMode.Optimized" />).</param>
		/// <param name="blocking">
		///       <see langword="true" /> to perform a blocking garbage collection; <see langword="false" /> to perform a background garbage collection where possible.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="generation" /> is not valid.-or-
		///         <paramref name="mode" /> is not one of the <see cref="T:System.GCCollectionMode" /> values.</exception>
		// Token: 0x06000F04 RID: 3844 RVA: 0x0003E52B File Offset: 0x0003C72B
		[SecuritySafeCritical]
		public static void Collect(int generation, GCCollectionMode mode, bool blocking)
		{
			GC.Collect(generation, mode, blocking, false);
		}

		/// <summary>Forces a garbage collection from generation 0 through a specified generation, at a time specified by a <see cref="T:System.GCCollectionMode" /> value, with values that specify whether the collection should be blocking and compacting. </summary>
		/// <param name="generation">The number of the oldest generation to be garbage collected. </param>
		/// <param name="mode">An enumeration value that specifies whether the garbage collection is forced (<see cref="F:System.GCCollectionMode.Default" /> or <see cref="F:System.GCCollectionMode.Forced" />) or optimized (<see cref="F:System.GCCollectionMode.Optimized" />).</param>
		/// <param name="blocking">
		///       <see langword="true" /> to perform a blocking garbage collection; <see langword="false" /> to perform a background garbage collection where possible. See the Remarks section for more information. </param>
		/// <param name="compacting">
		///       <see langword="true" /> to compact the small object heap; <see langword="false" /> to sweep only. See the Remarks section for more information. </param>
		// Token: 0x06000F05 RID: 3845 RVA: 0x0003E538 File Offset: 0x0003C738
		[SecuritySafeCritical]
		public static void Collect(int generation, GCCollectionMode mode, bool blocking, bool compacting)
		{
			if (generation < 0)
			{
				throw new ArgumentOutOfRangeException("generation", Environment.GetResourceString("Value must be positive."));
			}
			if (mode < GCCollectionMode.Default || mode > GCCollectionMode.Optimized)
			{
				throw new ArgumentOutOfRangeException(Environment.GetResourceString("Enum value was out of legal range."));
			}
			int num = 0;
			if (mode == GCCollectionMode.Optimized)
			{
				num |= 4;
			}
			if (compacting)
			{
				num |= 8;
			}
			if (blocking)
			{
				num |= 2;
			}
			else if (!compacting)
			{
				num |= 1;
			}
			GC.InternalCollect(generation);
		}

		/// <summary>Returns the number of times garbage collection has occurred for the specified generation of objects.</summary>
		/// <param name="generation">The generation of objects for which the garbage collection count is to be determined. </param>
		/// <returns>The number of times garbage collection has occurred for the specified generation since the process was started.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="generation" /> is less than 0. </exception>
		// Token: 0x06000F06 RID: 3846 RVA: 0x0003E59D File Offset: 0x0003C79D
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[SecuritySafeCritical]
		public static int CollectionCount(int generation)
		{
			if (generation < 0)
			{
				throw new ArgumentOutOfRangeException("generation", Environment.GetResourceString("Value must be positive."));
			}
			return GC.GetCollectionCount(generation);
		}

		/// <summary>References the specified object, which makes it ineligible for garbage collection from the start of the current routine to the point where this method is called.</summary>
		/// <param name="obj">The object to reference. </param>
		// Token: 0x06000F07 RID: 3847 RVA: 0x000020D3 File Offset: 0x000002D3
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.NoInlining)]
		public static void KeepAlive(object obj)
		{
		}

		/// <summary>Returns the current generation number of the target of a specified weak reference.</summary>
		/// <param name="wo">A <see cref="T:System.WeakReference" /> that refers to the target object whose generation number is to be determined. </param>
		/// <returns>The current generation number of the target of <paramref name="wo" />.</returns>
		/// <exception cref="T:System.ArgumentException">Garbage collection has already been performed on <paramref name="wo" />. </exception>
		// Token: 0x06000F08 RID: 3848 RVA: 0x0003E5BE File Offset: 0x0003C7BE
		[SecuritySafeCritical]
		public static int GetGeneration(WeakReference wo)
		{
			object target = wo.Target;
			if (target == null)
			{
				throw new ArgumentException();
			}
			return GC.GetGeneration(target);
		}

		/// <summary>Gets the maximum number of generations that the system currently supports.</summary>
		/// <returns>A value that ranges from zero to the maximum number of supported generations.</returns>
		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06000F09 RID: 3849 RVA: 0x0003E5D4 File Offset: 0x0003C7D4
		public static int MaxGeneration
		{
			[SecuritySafeCritical]
			get
			{
				return GC.GetMaxGeneration();
			}
		}

		/// <summary>Suspends the current thread until the thread that is processing the queue of finalizers has emptied that queue.</summary>
		// Token: 0x06000F0A RID: 3850
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WaitForPendingFinalizers();

		// Token: 0x06000F0B RID: 3851
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void _SuppressFinalize(object o);

		/// <summary>Requests that the common language runtime not call the finalizer for the specified object. </summary>
		/// <param name="obj">The object whose finalizer must not be executed. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="obj" /> is <see langword="null" />. </exception>
		// Token: 0x06000F0C RID: 3852 RVA: 0x0003E5DB File Offset: 0x0003C7DB
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[SecuritySafeCritical]
		public static void SuppressFinalize(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			GC._SuppressFinalize(obj);
		}

		// Token: 0x06000F0D RID: 3853
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void _ReRegisterForFinalize(object o);

		/// <summary>Requests that the system call the finalizer for the specified object for which <see cref="M:System.GC.SuppressFinalize(System.Object)" /> has previously been called.</summary>
		/// <param name="obj">The object that a finalizer must be called for. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="obj" /> is <see langword="null" />. </exception>
		// Token: 0x06000F0E RID: 3854 RVA: 0x0003E5F1 File Offset: 0x0003C7F1
		[SecuritySafeCritical]
		public static void ReRegisterForFinalize(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			GC._ReRegisterForFinalize(obj);
		}

		/// <summary>Retrieves the number of bytes currently thought to be allocated. A parameter indicates whether this method can wait a short interval before returning, to allow the system to collect garbage and finalize objects.</summary>
		/// <param name="forceFullCollection">
		///       <see langword="true" /> to indicate that this method can wait for garbage collection to occur before returning; otherwise, <see langword="false" />.</param>
		/// <returns>A number that is the best available approximation of the number of bytes currently allocated in managed memory.</returns>
		// Token: 0x06000F0F RID: 3855
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetTotalMemory(bool forceFullCollection);

		// Token: 0x06000F10 RID: 3856 RVA: 0x000041F3 File Offset: 0x000023F3
		private static bool _RegisterForFullGCNotification(int maxGenerationPercentage, int largeObjectHeapPercentage)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000F11 RID: 3857 RVA: 0x000041F3 File Offset: 0x000023F3
		private static bool _CancelFullGCNotification()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000F12 RID: 3858 RVA: 0x000041F3 File Offset: 0x000023F3
		private static int _WaitForFullGCApproach(int millisecondsTimeout)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000F13 RID: 3859 RVA: 0x000041F3 File Offset: 0x000023F3
		private static int _WaitForFullGCComplete(int millisecondsTimeout)
		{
			throw new NotImplementedException();
		}

		/// <summary>Specifies that a garbage collection notification should be raised when conditions favor full garbage collection and when the collection has been completed.</summary>
		/// <param name="maxGenerationThreshold">A number between 1 and 99 that specifies when the notification should be raised based on the objects allocated in generation 2. </param>
		/// <param name="largeObjectHeapThreshold">A number between 1 and 99 that specifies when the notification should be raised based on objects allocated in the large object heap. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="maxGenerationThreshold " />or <paramref name="largeObjectHeapThreshold " />is not between 1 and 99.</exception>
		// Token: 0x06000F14 RID: 3860 RVA: 0x0003E608 File Offset: 0x0003C808
		[SecurityCritical]
		public static void RegisterForFullGCNotification(int maxGenerationThreshold, int largeObjectHeapThreshold)
		{
			if (maxGenerationThreshold <= 0 || maxGenerationThreshold >= 100)
			{
				throw new ArgumentOutOfRangeException("maxGenerationThreshold", string.Format(CultureInfo.CurrentCulture, Environment.GetResourceString("Argument must be between {0} and {1}."), 1, 99));
			}
			if (largeObjectHeapThreshold <= 0 || largeObjectHeapThreshold >= 100)
			{
				throw new ArgumentOutOfRangeException("largeObjectHeapThreshold", string.Format(CultureInfo.CurrentCulture, Environment.GetResourceString("Argument must be between {0} and {1}."), 1, 99));
			}
			if (!GC._RegisterForFullGCNotification(maxGenerationThreshold, largeObjectHeapThreshold))
			{
				throw new InvalidOperationException(Environment.GetResourceString("This API is not available when the concurrent GC is enabled."));
			}
		}

		/// <summary>Cancels the registration of a garbage collection notification.</summary>
		/// <exception cref="T:System.InvalidOperationException">This member is not available when concurrent garbage collection is enabled. See the &lt;gcConcurrent&gt; runtime setting for information about how to disable concurrent garbage collection.</exception>
		// Token: 0x06000F15 RID: 3861 RVA: 0x0003E698 File Offset: 0x0003C898
		[SecurityCritical]
		public static void CancelFullGCNotification()
		{
			if (!GC._CancelFullGCNotification())
			{
				throw new InvalidOperationException(Environment.GetResourceString("This API is not available when the concurrent GC is enabled."));
			}
		}

		/// <summary>Returns the status of a registered notification for determining whether a full, blocking garbage collection by the common language runtime is imminent.</summary>
		/// <returns>The status of the registered garbage collection notification.</returns>
		// Token: 0x06000F16 RID: 3862 RVA: 0x0003E6B1 File Offset: 0x0003C8B1
		[SecurityCritical]
		public static GCNotificationStatus WaitForFullGCApproach()
		{
			return (GCNotificationStatus)GC._WaitForFullGCApproach(-1);
		}

		/// <summary>Returns, in a specified time-out period, the status of a registered notification for determining whether a full, blocking garbage collection by the common language runtime is imminent.</summary>
		/// <param name="millisecondsTimeout">The length of time to wait before a notification status can be obtained. Specify -1 to wait indefinitely.</param>
		/// <returns>The status of the registered garbage collection notification.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="millisecondsTimeout" /> must be either non-negative or less than or equal to <see cref="F:System.Int32.MaxValue" /> or -1.</exception>
		// Token: 0x06000F17 RID: 3863 RVA: 0x0003E6B9 File Offset: 0x0003C8B9
		[SecurityCritical]
		public static GCNotificationStatus WaitForFullGCApproach(int millisecondsTimeout)
		{
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout", Environment.GetResourceString("Number must be either non-negative and less than or equal to Int32.MaxValue or -1."));
			}
			return (GCNotificationStatus)GC._WaitForFullGCApproach(millisecondsTimeout);
		}

		/// <summary>Returns the status of a registered notification for determining whether a full, blocking garbage collection by the common language runtime has completed.</summary>
		/// <returns>The status of the registered garbage collection notification.</returns>
		// Token: 0x06000F18 RID: 3864 RVA: 0x0003E6DA File Offset: 0x0003C8DA
		[SecurityCritical]
		public static GCNotificationStatus WaitForFullGCComplete()
		{
			return (GCNotificationStatus)GC._WaitForFullGCComplete(-1);
		}

		/// <summary>Returns, in a specified time-out period, the status of a registered notification for determining whether a full, blocking garbage collection by common language the runtime has completed.</summary>
		/// <param name="millisecondsTimeout">The length of time to wait before a notification status can be obtained. Specify -1 to wait indefinitely.</param>
		/// <returns>The status of the registered garbage collection notification.</returns>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="millisecondsTimeout" /> must be either non-negative or less than or equal to <see cref="F:System.Int32.MaxValue" /> or -1.</exception>
		// Token: 0x06000F19 RID: 3865 RVA: 0x0003E6E2 File Offset: 0x0003C8E2
		[SecurityCritical]
		public static GCNotificationStatus WaitForFullGCComplete(int millisecondsTimeout)
		{
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout", Environment.GetResourceString("Number must be either non-negative and less than or equal to Int32.MaxValue or -1."));
			}
			return (GCNotificationStatus)GC._WaitForFullGCComplete(millisecondsTimeout);
		}

		// Token: 0x06000F1A RID: 3866 RVA: 0x000041F3 File Offset: 0x000023F3
		[SecurityCritical]
		private static bool StartNoGCRegionWorker(long totalSize, bool hasLohSize, long lohSize, bool disallowFullBlockingGC)
		{
			throw new NotImplementedException();
		}

		/// <summary>Attempts to disallow garbage collection during the execution of a critical path if a specified amount of memory is available. </summary>
		/// <param name="totalSize">The amount of memory in bytes to allocate without triggering a garbage collection. It must be less than or equal to the size of an ephemeral segment. For information on the size of an ephemeral segement, see the "Ephemeral generations and segments" section in the Fundamentals of Garbage Collection article. </param>
		/// <returns>
		///     <see langword="true" /> if the runtime was able to commit the required amount of memory and the garbage collector is able to enter no GC region latency mode; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="totalSize" /> exceeds the ephemeral segment size. </exception>
		/// <exception cref="T:System.InvalidOperationException">The process is already in no GC region latency mode. </exception>
		// Token: 0x06000F1B RID: 3867 RVA: 0x0003E703 File Offset: 0x0003C903
		[SecurityCritical]
		public static bool TryStartNoGCRegion(long totalSize)
		{
			return GC.StartNoGCRegionWorker(totalSize, false, 0L, false);
		}

		/// <summary>Attempts to disallow garbage collection during the execution of a critical path if a specified amount of memory is available for the large object heap and the small object heap. </summary>
		/// <param name="totalSize">The amount of memory in bytes to allocate without triggering a garbage collection. <paramref name="totalSize" /> –<paramref name="lohSize" /> must be less than or equal to the size of an ephemeral segment. For information on the size of an ephemeral segement, see the "Ephemeral generations and segments" section in the Fundamentals of Garbage Collection article. </param>
		/// <param name="lohSize">The number of bytes in <paramref name="totalSize" /> to use for large object heap (LOH) allocations. </param>
		/// <returns>
		///     <see langword="true" /> if the runtime was able to commit the required amount of memory and the garbage collector is able to enter no GC region latency mode; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="totalSize" /> – <paramref name="lohSize" />  exceeds the ephemeral segment size. </exception>
		/// <exception cref="T:System.InvalidOperationException">The process is already in no GC region latency mode. </exception>
		// Token: 0x06000F1C RID: 3868 RVA: 0x0003E70F File Offset: 0x0003C90F
		[SecurityCritical]
		public static bool TryStartNoGCRegion(long totalSize, long lohSize)
		{
			return GC.StartNoGCRegionWorker(totalSize, true, lohSize, false);
		}

		/// <summary>Attempts to disallow garbage collection during the execution of a critical path if a specified amount of memory is available, and controls whether the garbage collector does a full blocking garbage collection if not enough memory is initially available. </summary>
		/// <param name="totalSize">The amount of memory in bytes to allocate without triggering a garbage collection. It must be less than or equal to the size of an ephemeral segment. For information on the size of an ephemeral segement, see the "Ephemeral generations and segments" section in the Fundamentals of Garbage Collection article. </param>
		/// <param name="disallowFullBlockingGC">
		///       <see langword="true" /> to omit a full blocking garbage collection if the garbage collector is initially unable to allocate <paramref name="totalSize" /> bytes; otherwise, <see langword="false" />. </param>
		/// <returns>
		///     <see langword="true" /> if the runtime was able to commit the required amount of memory and the garbage collector is able to enter no GC region latency mode; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="totalSize" /> exceeds the ephemeral segment size. </exception>
		/// <exception cref="T:System.InvalidOperationException">The process is already in no GC region latency mode. </exception>
		// Token: 0x06000F1D RID: 3869 RVA: 0x0003E71A File Offset: 0x0003C91A
		[SecurityCritical]
		public static bool TryStartNoGCRegion(long totalSize, bool disallowFullBlockingGC)
		{
			return GC.StartNoGCRegionWorker(totalSize, false, 0L, disallowFullBlockingGC);
		}

		/// <summary>Attempts to disallow garbage collection during the execution of a critical path if a specified amount of memory is available for the large object heap and the small object heap, and controls whether the garbage collector does a full blocking garbage collection if not enough memory is initially available. </summary>
		/// <param name="totalSize">The amount of memory in bytes to allocate without triggering a garbage collection. <paramref name="totalSize" /> –<paramref name="lohSize" /> must be less than or equal to the size of an ephemeral segment. For information on the size of an ephemeral segement, see the "Ephemeral generations and segments" section in the Fundamentals of Garbage Collection article. </param>
		/// <param name="lohSize">The number of bytes in <paramref name="totalSize" /> to use for large object heap (LOH) allocations. </param>
		/// <param name="disallowFullBlockingGC">
		///       <see langword="true" /> to omit a full blocking garbage collection if the garbage collector is initially unable to allocate the specified memory on the small object heap (SOH) and LOH; otherwise, <see langword="false" />.</param>
		/// <returns>
		///     <see langword="true" /> if the runtime was able to commit the required amount of memory and the garbage collector is able to enter no GC region latency mode; otherwise, <see langword="false" />. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="totalSize" /> – <paramref name="lohSize" />  exceeds the ephemeral segment size. </exception>
		/// <exception cref="T:System.InvalidOperationException">The process is already in no GC region latency mode. </exception>
		// Token: 0x06000F1E RID: 3870 RVA: 0x0003E726 File Offset: 0x0003C926
		[SecurityCritical]
		public static bool TryStartNoGCRegion(long totalSize, long lohSize, bool disallowFullBlockingGC)
		{
			return GC.StartNoGCRegionWorker(totalSize, true, lohSize, disallowFullBlockingGC);
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x000041F3 File Offset: 0x000023F3
		[SecurityCritical]
		private static GC.EndNoGCRegionStatus EndNoGCRegionWorker()
		{
			throw new NotImplementedException();
		}

		/// <summary>Ends the no GC region latency mode. </summary>
		/// <exception cref="T:System.InvalidOperationException">The garbage collector is not in no GC region latency mode. See the Remarks section for more information. -or-The no GC region latency mode was ended previously because a garbage collection was induced. -or-A memory allocation exceeded the amount specified in the call to the <see cref="M:System.GC.TryStartNoGCRegion(System.Int64)" /> method. </exception>
		// Token: 0x06000F20 RID: 3872 RVA: 0x0003E731 File Offset: 0x0003C931
		[SecurityCritical]
		public static void EndNoGCRegion()
		{
			GC.EndNoGCRegionWorker();
		}

		// Token: 0x06000F21 RID: 3873 RVA: 0x0003E739 File Offset: 0x0003C939
		// Note: this type is marked as 'beforefieldinit'.
		static GC()
		{
		}

		// Token: 0x040008EF RID: 2287
		internal static readonly object EPHEMERON_TOMBSTONE = GC.get_ephemeron_tombstone();

		// Token: 0x02000156 RID: 342
		private enum StartNoGCRegionStatus
		{
			// Token: 0x040008F1 RID: 2289
			Succeeded,
			// Token: 0x040008F2 RID: 2290
			NotEnoughMemory,
			// Token: 0x040008F3 RID: 2291
			AmountTooLarge,
			// Token: 0x040008F4 RID: 2292
			AlreadyInProgress
		}

		// Token: 0x02000157 RID: 343
		private enum EndNoGCRegionStatus
		{
			// Token: 0x040008F6 RID: 2294
			Succeeded,
			// Token: 0x040008F7 RID: 2295
			NotInProgress,
			// Token: 0x040008F8 RID: 2296
			GCInduced,
			// Token: 0x040008F9 RID: 2297
			AllocationExceeded
		}
	}
}
