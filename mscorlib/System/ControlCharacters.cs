﻿using System;

namespace System
{
	// Token: 0x020001F9 RID: 505
	internal class ControlCharacters
	{
		// Token: 0x06001899 RID: 6297 RVA: 0x00002050 File Offset: 0x00000250
		public ControlCharacters()
		{
		}

		// Token: 0x04000C4F RID: 3151
		public const int Intr = 0;

		// Token: 0x04000C50 RID: 3152
		public const int Quit = 1;

		// Token: 0x04000C51 RID: 3153
		public const int Erase = 2;

		// Token: 0x04000C52 RID: 3154
		public const int Kill = 3;

		// Token: 0x04000C53 RID: 3155
		public const int EOF = 4;

		// Token: 0x04000C54 RID: 3156
		public const int Time = 5;

		// Token: 0x04000C55 RID: 3157
		public const int Min = 6;

		// Token: 0x04000C56 RID: 3158
		public const int SWTC = 7;

		// Token: 0x04000C57 RID: 3159
		public const int Start = 8;

		// Token: 0x04000C58 RID: 3160
		public const int Stop = 9;

		// Token: 0x04000C59 RID: 3161
		public const int Susp = 10;

		// Token: 0x04000C5A RID: 3162
		public const int EOL = 11;

		// Token: 0x04000C5B RID: 3163
		public const int Reprint = 12;

		// Token: 0x04000C5C RID: 3164
		public const int Discard = 13;

		// Token: 0x04000C5D RID: 3165
		public const int WErase = 14;

		// Token: 0x04000C5E RID: 3166
		public const int LNext = 15;

		// Token: 0x04000C5F RID: 3167
		public const int EOL2 = 16;
	}
}
