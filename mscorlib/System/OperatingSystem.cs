﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>Represents information about an operating system, such as the version and platform identifier. This class cannot be inherited.</summary>
	// Token: 0x0200021C RID: 540
	[ComVisible(true)]
	[Serializable]
	public sealed class OperatingSystem : ICloneable, ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.OperatingSystem" /> class, using the specified platform identifier value and version object.</summary>
		/// <param name="platform">One of the <see cref="T:System.PlatformID" /> values that indicates the operating system platform. </param>
		/// <param name="version">A <see cref="T:System.Version" /> object that indicates the version of the operating system. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="version" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="platform" /> is not a <see cref="T:System.PlatformID" /> enumeration value.</exception>
		// Token: 0x060019FA RID: 6650 RVA: 0x00062480 File Offset: 0x00060680
		public OperatingSystem(PlatformID platform, Version version)
		{
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			this._platform = platform;
			this._version = version;
			if (platform == PlatformID.Win32NT && version.Revision != 0)
			{
				this._servicePack = "Service Pack " + (version.Revision >> 16);
			}
		}

		// Token: 0x060019FB RID: 6651 RVA: 0x000624EC File Offset: 0x000606EC
		private OperatingSystem(SerializationInfo information, StreamingContext context)
		{
			this._platform = (PlatformID)information.GetValue("_platform", typeof(PlatformID));
			this._version = (Version)information.GetValue("_version", typeof(Version));
			this._servicePack = information.GetString("_servicePack");
		}

		/// <summary>Gets a <see cref="T:System.PlatformID" /> enumeration value that identifies the operating system platform.</summary>
		/// <returns>One of the <see cref="T:System.PlatformID" /> values.</returns>
		// Token: 0x17000375 RID: 885
		// (get) Token: 0x060019FC RID: 6652 RVA: 0x0006255B File Offset: 0x0006075B
		public PlatformID Platform
		{
			get
			{
				return this._platform;
			}
		}

		/// <summary>Gets a <see cref="T:System.Version" /> object that identifies the operating system.</summary>
		/// <returns>A <see cref="T:System.Version" /> object that describes the major version, minor version, build, and revision numbers for the operating system.</returns>
		// Token: 0x17000376 RID: 886
		// (get) Token: 0x060019FD RID: 6653 RVA: 0x00062563 File Offset: 0x00060763
		public Version Version
		{
			get
			{
				return this._version;
			}
		}

		/// <summary>Gets the service pack version represented by this <see cref="T:System.OperatingSystem" /> object.</summary>
		/// <returns>The service pack version, if service packs are supported and at least one is installed; otherwise, an empty string (""). </returns>
		// Token: 0x17000377 RID: 887
		// (get) Token: 0x060019FE RID: 6654 RVA: 0x0006256B File Offset: 0x0006076B
		public string ServicePack
		{
			get
			{
				return this._servicePack;
			}
		}

		/// <summary>Gets the concatenated string representation of the platform identifier, version, and service pack that are currently installed on the operating system. </summary>
		/// <returns>The string representation of the values returned by the <see cref="P:System.OperatingSystem.Platform" />, <see cref="P:System.OperatingSystem.Version" />, and <see cref="P:System.OperatingSystem.ServicePack" /> properties.</returns>
		// Token: 0x17000378 RID: 888
		// (get) Token: 0x060019FF RID: 6655 RVA: 0x0003D36C File Offset: 0x0003B56C
		public string VersionString
		{
			get
			{
				return this.ToString();
			}
		}

		/// <summary>Creates an <see cref="T:System.OperatingSystem" /> object that is identical to this instance.</summary>
		/// <returns>An <see cref="T:System.OperatingSystem" /> object that is a copy of this instance.</returns>
		// Token: 0x06001A00 RID: 6656 RVA: 0x00062573 File Offset: 0x00060773
		public object Clone()
		{
			return new OperatingSystem(this._platform, this._version);
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the data necessary to deserialize this instance.</summary>
		/// <param name="info">The object to populate with serialization information.</param>
		/// <param name="context">The place to store and retrieve serialized data. Reserved for future use.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />. </exception>
		// Token: 0x06001A01 RID: 6657 RVA: 0x00062586 File Offset: 0x00060786
		[SecurityCritical]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_platform", this._platform);
			info.AddValue("_version", this._version);
			info.AddValue("_servicePack", this._servicePack);
		}

		/// <summary>Converts the value of this <see cref="T:System.OperatingSystem" /> object to its equivalent string representation.</summary>
		/// <returns>The string representation of the values returned by the <see cref="P:System.OperatingSystem.Platform" />, <see cref="P:System.OperatingSystem.Version" />, and <see cref="P:System.OperatingSystem.ServicePack" /> properties.</returns>
		// Token: 0x06001A02 RID: 6658 RVA: 0x000625C0 File Offset: 0x000607C0
		public override string ToString()
		{
			int platform = (int)this._platform;
			string str;
			switch (platform)
			{
			case 0:
				str = "Microsoft Win32S";
				goto IL_76;
			case 1:
				str = "Microsoft Windows 98";
				goto IL_76;
			case 2:
				str = "Microsoft Windows NT";
				goto IL_76;
			case 3:
				str = "Microsoft Windows CE";
				goto IL_76;
			case 4:
				break;
			case 5:
				str = "XBox";
				goto IL_76;
			case 6:
				str = "OSX";
				goto IL_76;
			default:
				if (platform != 128)
				{
					str = Locale.GetText("<unknown>");
					goto IL_76;
				}
				break;
			}
			str = "Unix";
			IL_76:
			string str2 = "";
			if (this.ServicePack != string.Empty)
			{
				str2 = " " + this.ServicePack;
			}
			return str + " " + this._version.ToString() + str2;
		}

		// Token: 0x04000CDB RID: 3291
		private PlatformID _platform;

		// Token: 0x04000CDC RID: 3292
		private Version _version;

		// Token: 0x04000CDD RID: 3293
		private string _servicePack = string.Empty;
	}
}
