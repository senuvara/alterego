﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace System
{
	// Token: 0x0200023E RID: 574
	internal class TypeSpec
	{
		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x06001B03 RID: 6915 RVA: 0x00065F99 File Offset: 0x00064199
		internal bool HasModifiers
		{
			get
			{
				return this.modifier_spec != null;
			}
		}

		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x06001B04 RID: 6916 RVA: 0x00065FA4 File Offset: 0x000641A4
		internal bool IsNested
		{
			get
			{
				return this.nested != null && this.nested.Count > 0;
			}
		}

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x06001B05 RID: 6917 RVA: 0x00065FBE File Offset: 0x000641BE
		internal bool IsByRef
		{
			get
			{
				return this.is_byref;
			}
		}

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06001B06 RID: 6918 RVA: 0x00065FC6 File Offset: 0x000641C6
		internal TypeName Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06001B07 RID: 6919 RVA: 0x00065FCE File Offset: 0x000641CE
		internal IEnumerable<TypeName> Nested
		{
			get
			{
				if (this.nested != null)
				{
					return this.nested;
				}
				return EmptyArray<TypeName>.Value;
			}
		}

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x06001B08 RID: 6920 RVA: 0x00065FE4 File Offset: 0x000641E4
		internal IEnumerable<ModifierSpec> Modifiers
		{
			get
			{
				if (this.modifier_spec != null)
				{
					return this.modifier_spec;
				}
				return EmptyArray<ModifierSpec>.Value;
			}
		}

		// Token: 0x06001B09 RID: 6921 RVA: 0x00065FFC File Offset: 0x000641FC
		private string GetDisplayFullName(TypeSpec.DisplayNameFormat flags)
		{
			bool flag = (flags & TypeSpec.DisplayNameFormat.WANT_ASSEMBLY) > TypeSpec.DisplayNameFormat.Default;
			bool flag2 = (flags & TypeSpec.DisplayNameFormat.NO_MODIFIERS) == TypeSpec.DisplayNameFormat.Default;
			StringBuilder stringBuilder = new StringBuilder(this.name.DisplayName);
			if (this.nested != null)
			{
				foreach (TypeIdentifier typeIdentifier in this.nested)
				{
					stringBuilder.Append('+').Append(typeIdentifier.DisplayName);
				}
			}
			if (this.generic_params != null)
			{
				stringBuilder.Append('[');
				for (int i = 0; i < this.generic_params.Count; i++)
				{
					if (i > 0)
					{
						stringBuilder.Append(", ");
					}
					if (this.generic_params[i].assembly_name != null)
					{
						stringBuilder.Append('[').Append(this.generic_params[i].DisplayFullName).Append(']');
					}
					else
					{
						stringBuilder.Append(this.generic_params[i].DisplayFullName);
					}
				}
				stringBuilder.Append(']');
			}
			if (flag2)
			{
				this.GetModifierString(stringBuilder);
			}
			if (this.assembly_name != null && flag)
			{
				stringBuilder.Append(", ").Append(this.assembly_name);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001B0A RID: 6922 RVA: 0x00066158 File Offset: 0x00064358
		internal string ModifierString()
		{
			return this.GetModifierString(new StringBuilder()).ToString();
		}

		// Token: 0x06001B0B RID: 6923 RVA: 0x0006616C File Offset: 0x0006436C
		private StringBuilder GetModifierString(StringBuilder sb)
		{
			if (this.modifier_spec != null)
			{
				foreach (ModifierSpec modifierSpec in this.modifier_spec)
				{
					modifierSpec.Append(sb);
				}
			}
			if (this.is_byref)
			{
				sb.Append('&');
			}
			return sb;
		}

		// Token: 0x170003AC RID: 940
		// (get) Token: 0x06001B0C RID: 6924 RVA: 0x000661D8 File Offset: 0x000643D8
		internal string DisplayFullName
		{
			get
			{
				if (this.display_fullname == null)
				{
					this.display_fullname = this.GetDisplayFullName(TypeSpec.DisplayNameFormat.Default);
				}
				return this.display_fullname;
			}
		}

		// Token: 0x06001B0D RID: 6925 RVA: 0x000661F8 File Offset: 0x000643F8
		internal static TypeSpec Parse(string typeName)
		{
			int num = 0;
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			TypeSpec result = TypeSpec.Parse(typeName, ref num, false, true);
			if (num < typeName.Length)
			{
				throw new ArgumentException("Count not parse the whole type name", "typeName");
			}
			return result;
		}

		// Token: 0x06001B0E RID: 6926 RVA: 0x00066238 File Offset: 0x00064438
		internal static string EscapeDisplayName(string internalName)
		{
			StringBuilder stringBuilder = new StringBuilder(internalName.Length);
			int i = 0;
			while (i < internalName.Length)
			{
				char value = internalName[i];
				switch (value)
				{
				case '&':
				case '*':
				case '+':
				case ',':
					goto IL_56;
				case '\'':
				case '(':
				case ')':
					goto IL_67;
				default:
					switch (value)
					{
					case '[':
					case '\\':
					case ']':
						goto IL_56;
					default:
						goto IL_67;
					}
					break;
				}
				IL_6F:
				i++;
				continue;
				IL_56:
				stringBuilder.Append('\\').Append(value);
				goto IL_6F;
				IL_67:
				stringBuilder.Append(value);
				goto IL_6F;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001B0F RID: 6927 RVA: 0x000662C8 File Offset: 0x000644C8
		internal static string UnescapeInternalName(string displayName)
		{
			StringBuilder stringBuilder = new StringBuilder(displayName.Length);
			for (int i = 0; i < displayName.Length; i++)
			{
				char c = displayName[i];
				if (c == '\\' && ++i < displayName.Length)
				{
					c = displayName[i];
				}
				stringBuilder.Append(c);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001B10 RID: 6928 RVA: 0x00066324 File Offset: 0x00064524
		internal static bool NeedsEscaping(string internalName)
		{
			foreach (char c in internalName)
			{
				switch (c)
				{
				case '&':
				case '*':
				case '+':
				case ',':
					return true;
				case '\'':
				case '(':
				case ')':
					break;
				default:
					switch (c)
					{
					case '[':
					case '\\':
					case ']':
						return true;
					}
					break;
				}
			}
			return false;
		}

		// Token: 0x06001B11 RID: 6929 RVA: 0x0006638C File Offset: 0x0006458C
		internal Type Resolve(Func<AssemblyName, Assembly> assemblyResolver, Func<Assembly, string, bool, Type> typeResolver, bool throwOnError, bool ignoreCase)
		{
			Assembly assembly = null;
			if (assemblyResolver == null && typeResolver == null)
			{
				return Type.GetType(this.DisplayFullName, throwOnError, ignoreCase);
			}
			if (this.assembly_name != null)
			{
				if (assemblyResolver != null)
				{
					assembly = assemblyResolver(new AssemblyName(this.assembly_name));
				}
				else
				{
					assembly = Assembly.Load(this.assembly_name);
				}
				if (assembly == null)
				{
					if (throwOnError)
					{
						throw new FileNotFoundException("Could not resolve assembly '" + this.assembly_name + "'");
					}
					return null;
				}
			}
			Type type = null;
			if (typeResolver != null)
			{
				type = typeResolver(assembly, this.name.DisplayName, ignoreCase);
			}
			else
			{
				type = assembly.GetType(this.name.DisplayName, false, ignoreCase);
			}
			if (!(type == null))
			{
				if (this.nested != null)
				{
					foreach (TypeIdentifier typeIdentifier in this.nested)
					{
						Type nestedType = type.GetNestedType(typeIdentifier.DisplayName, BindingFlags.Public | BindingFlags.NonPublic);
						if (nestedType == null)
						{
							if (throwOnError)
							{
								throw new TypeLoadException("Could not resolve type '" + typeIdentifier + "'");
							}
							return null;
						}
						else
						{
							type = nestedType;
						}
					}
				}
				if (this.generic_params != null)
				{
					Type[] array = new Type[this.generic_params.Count];
					int i = 0;
					while (i < array.Length)
					{
						Type type2 = this.generic_params[i].Resolve(assemblyResolver, typeResolver, throwOnError, ignoreCase);
						if (type2 == null)
						{
							if (throwOnError)
							{
								throw new TypeLoadException("Could not resolve type '" + this.generic_params[i].name + "'");
							}
							return null;
						}
						else
						{
							array[i] = type2;
							i++;
						}
					}
					type = type.MakeGenericType(array);
				}
				if (this.modifier_spec != null)
				{
					foreach (ModifierSpec modifierSpec in this.modifier_spec)
					{
						type = modifierSpec.Resolve(type);
					}
				}
				if (this.is_byref)
				{
					type = type.MakeByRefType();
				}
				return type;
			}
			if (throwOnError)
			{
				throw new TypeLoadException("Could not resolve type '" + this.name + "'");
			}
			return null;
		}

		// Token: 0x06001B12 RID: 6930 RVA: 0x000665D0 File Offset: 0x000647D0
		private void AddName(string type_name)
		{
			if (this.name == null)
			{
				this.name = TypeSpec.ParsedTypeIdentifier(type_name);
				return;
			}
			if (this.nested == null)
			{
				this.nested = new List<TypeIdentifier>();
			}
			this.nested.Add(TypeSpec.ParsedTypeIdentifier(type_name));
		}

		// Token: 0x06001B13 RID: 6931 RVA: 0x0006660B File Offset: 0x0006480B
		private void AddModifier(ModifierSpec md)
		{
			if (this.modifier_spec == null)
			{
				this.modifier_spec = new List<ModifierSpec>();
			}
			this.modifier_spec.Add(md);
		}

		// Token: 0x06001B14 RID: 6932 RVA: 0x0006662C File Offset: 0x0006482C
		private static void SkipSpace(string name, ref int pos)
		{
			int num = pos;
			while (num < name.Length && char.IsWhiteSpace(name[num]))
			{
				num++;
			}
			pos = num;
		}

		// Token: 0x06001B15 RID: 6933 RVA: 0x0006665C File Offset: 0x0006485C
		private static void BoundCheck(int idx, string s)
		{
			if (idx >= s.Length)
			{
				throw new ArgumentException("Invalid generic arguments spec", "typeName");
			}
		}

		// Token: 0x06001B16 RID: 6934 RVA: 0x00066677 File Offset: 0x00064877
		private static TypeIdentifier ParsedTypeIdentifier(string displayName)
		{
			return TypeIdentifiers.FromDisplay(displayName);
		}

		// Token: 0x06001B17 RID: 6935 RVA: 0x00066680 File Offset: 0x00064880
		private static TypeSpec Parse(string name, ref int p, bool is_recurse, bool allow_aqn)
		{
			int i = p;
			bool flag = false;
			TypeSpec typeSpec = new TypeSpec();
			TypeSpec.SkipSpace(name, ref i);
			int num = i;
			while (i < name.Length)
			{
				char c = name[i];
				switch (c)
				{
				case '&':
				case '*':
					goto IL_98;
				case '\'':
				case '(':
				case ')':
					break;
				case '+':
					typeSpec.AddName(name.Substring(num, i - num));
					num = i + 1;
					break;
				case ',':
					goto IL_77;
				default:
					switch (c)
					{
					case '[':
						goto IL_98;
					case '\\':
						i++;
						break;
					case ']':
						goto IL_77;
					}
					break;
				}
				IL_D6:
				if (!flag)
				{
					i++;
					continue;
				}
				break;
				IL_77:
				typeSpec.AddName(name.Substring(num, i - num));
				num = i + 1;
				flag = true;
				if (is_recurse && !allow_aqn)
				{
					p = i;
					return typeSpec;
				}
				goto IL_D6;
				IL_98:
				if (name[i] != '[' && is_recurse)
				{
					throw new ArgumentException("Generic argument can't be byref or pointer type", "typeName");
				}
				typeSpec.AddName(name.Substring(num, i - num));
				num = i + 1;
				flag = true;
				goto IL_D6;
			}
			if (num < i)
			{
				typeSpec.AddName(name.Substring(num, i - num));
			}
			else if (num == i)
			{
				typeSpec.AddName(string.Empty);
			}
			if (flag)
			{
				while (i < name.Length)
				{
					char c = name[i];
					if (c <= '*')
					{
						if (c != '&')
						{
							if (c != '*')
							{
								goto IL_4BE;
							}
							if (typeSpec.is_byref)
							{
								throw new ArgumentException("Can't have a pointer to a byref type", "typeName");
							}
							int num2 = 1;
							while (i + 1 < name.Length && name[i + 1] == '*')
							{
								i++;
								num2++;
							}
							typeSpec.AddModifier(new PointerSpec(num2));
						}
						else
						{
							if (typeSpec.is_byref)
							{
								throw new ArgumentException("Can't have a byref of a byref", "typeName");
							}
							typeSpec.is_byref = true;
						}
					}
					else if (c != ',')
					{
						if (c != '[')
						{
							if (c != ']')
							{
								goto IL_4BE;
							}
							if (is_recurse)
							{
								p = i;
								return typeSpec;
							}
							throw new ArgumentException("Unmatched ']'", "typeName");
						}
						else
						{
							if (typeSpec.is_byref)
							{
								throw new ArgumentException("Byref qualifier must be the last one of a type", "typeName");
							}
							i++;
							if (i >= name.Length)
							{
								throw new ArgumentException("Invalid array/generic spec", "typeName");
							}
							TypeSpec.SkipSpace(name, ref i);
							if (name[i] != ',' && name[i] != '*' && name[i] != ']')
							{
								List<TypeSpec> list = new List<TypeSpec>();
								if (typeSpec.HasModifiers)
								{
									throw new ArgumentException("generic args after array spec or pointer type", "typeName");
								}
								while (i < name.Length)
								{
									TypeSpec.SkipSpace(name, ref i);
									bool flag2 = name[i] == '[';
									if (flag2)
									{
										i++;
									}
									list.Add(TypeSpec.Parse(name, ref i, true, flag2));
									TypeSpec.BoundCheck(i, name);
									if (flag2)
									{
										if (name[i] != ']')
										{
											throw new ArgumentException("Unclosed assembly-qualified type name at " + name[i].ToString(), "typeName");
										}
										i++;
										TypeSpec.BoundCheck(i, name);
									}
									if (name[i] == ']')
									{
										break;
									}
									if (name[i] != ',')
									{
										throw new ArgumentException("Invalid generic arguments separator " + name[i].ToString(), "typeName");
									}
									i++;
								}
								if (i >= name.Length || name[i] != ']')
								{
									throw new ArgumentException("Error parsing generic params spec", "typeName");
								}
								typeSpec.generic_params = list;
							}
							else
							{
								int num3 = 1;
								bool flag3 = false;
								while (i < name.Length && name[i] != ']')
								{
									if (name[i] == '*')
									{
										if (flag3)
										{
											throw new ArgumentException("Array spec cannot have 2 bound dimensions", "typeName");
										}
										flag3 = true;
									}
									else
									{
										if (name[i] != ',')
										{
											throw new ArgumentException("Invalid character in array spec " + name[i].ToString(), "typeName");
										}
										num3++;
									}
									i++;
									TypeSpec.SkipSpace(name, ref i);
								}
								if (i >= name.Length || name[i] != ']')
								{
									throw new ArgumentException("Error parsing array spec", "typeName");
								}
								if (num3 > 1 && flag3)
								{
									throw new ArgumentException("Invalid array spec, multi-dimensional array cannot be bound", "typeName");
								}
								typeSpec.AddModifier(new ArraySpec(num3, flag3));
							}
						}
					}
					else if (is_recurse && allow_aqn)
					{
						int num4 = i;
						while (num4 < name.Length && name[num4] != ']')
						{
							num4++;
						}
						if (num4 >= name.Length)
						{
							throw new ArgumentException("Unmatched ']' while parsing generic argument assembly name");
						}
						typeSpec.assembly_name = name.Substring(i + 1, num4 - i - 1).Trim();
						p = num4;
						return typeSpec;
					}
					else
					{
						if (is_recurse)
						{
							p = i;
							return typeSpec;
						}
						if (allow_aqn)
						{
							typeSpec.assembly_name = name.Substring(i + 1).Trim();
							i = name.Length;
						}
					}
					i++;
					continue;
					IL_4BE:
					throw new ArgumentException(string.Concat(new object[]
					{
						"Bad type def, can't handle '",
						name[i].ToString(),
						"' at ",
						i
					}), "typeName");
				}
			}
			p = i;
			return typeSpec;
		}

		// Token: 0x06001B18 RID: 6936 RVA: 0x00066BA1 File Offset: 0x00064DA1
		internal TypeName TypeNameWithoutModifiers()
		{
			return new TypeSpec.TypeSpecTypeName(this, false);
		}

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x06001B19 RID: 6937 RVA: 0x00066BAA File Offset: 0x00064DAA
		internal TypeName TypeName
		{
			get
			{
				return new TypeSpec.TypeSpecTypeName(this, true);
			}
		}

		// Token: 0x06001B1A RID: 6938 RVA: 0x00002050 File Offset: 0x00000250
		public TypeSpec()
		{
		}

		// Token: 0x04000F27 RID: 3879
		private TypeIdentifier name;

		// Token: 0x04000F28 RID: 3880
		private string assembly_name;

		// Token: 0x04000F29 RID: 3881
		private List<TypeIdentifier> nested;

		// Token: 0x04000F2A RID: 3882
		private List<TypeSpec> generic_params;

		// Token: 0x04000F2B RID: 3883
		private List<ModifierSpec> modifier_spec;

		// Token: 0x04000F2C RID: 3884
		private bool is_byref;

		// Token: 0x04000F2D RID: 3885
		private string display_fullname;

		// Token: 0x0200023F RID: 575
		[Flags]
		internal enum DisplayNameFormat
		{
			// Token: 0x04000F2F RID: 3887
			Default = 0,
			// Token: 0x04000F30 RID: 3888
			WANT_ASSEMBLY = 1,
			// Token: 0x04000F31 RID: 3889
			NO_MODIFIERS = 2
		}

		// Token: 0x02000240 RID: 576
		private class TypeSpecTypeName : TypeNames.ATypeName, TypeName, IEquatable<TypeName>
		{
			// Token: 0x06001B1B RID: 6939 RVA: 0x00066BB3 File Offset: 0x00064DB3
			internal TypeSpecTypeName(TypeSpec ts, bool wantModifiers)
			{
				this.ts = ts;
				this.want_modifiers = wantModifiers;
			}

			// Token: 0x170003AE RID: 942
			// (get) Token: 0x06001B1C RID: 6940 RVA: 0x00066BC9 File Offset: 0x00064DC9
			public override string DisplayName
			{
				get
				{
					if (this.want_modifiers)
					{
						return this.ts.DisplayFullName;
					}
					return this.ts.GetDisplayFullName(TypeSpec.DisplayNameFormat.NO_MODIFIERS);
				}
			}

			// Token: 0x06001B1D RID: 6941 RVA: 0x00065E03 File Offset: 0x00064003
			public override TypeName NestedName(TypeIdentifier innerName)
			{
				return TypeNames.FromDisplay(this.DisplayName + "+" + innerName.DisplayName);
			}

			// Token: 0x04000F32 RID: 3890
			private TypeSpec ts;

			// Token: 0x04000F33 RID: 3891
			private bool want_modifiers;
		}
	}
}
