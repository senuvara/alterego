﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200020F RID: 527
	[StructLayout(LayoutKind.Sequential)]
	internal class MonoAsyncCall
	{
		// Token: 0x06001934 RID: 6452 RVA: 0x00002050 File Offset: 0x00000250
		public MonoAsyncCall()
		{
		}

		// Token: 0x04000C8B RID: 3211
		private object msg;

		// Token: 0x04000C8C RID: 3212
		private IntPtr cb_method;

		// Token: 0x04000C8D RID: 3213
		private object cb_target;

		// Token: 0x04000C8E RID: 3214
		private object state;

		// Token: 0x04000C8F RID: 3215
		private object res;

		// Token: 0x04000C90 RID: 3216
		private object out_args;
	}
}
