﻿using System;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when there is insufficient execution stack available to allow most methods to execute.</summary>
	// Token: 0x0200017B RID: 379
	[Serializable]
	public sealed class InsufficientExecutionStackException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.InsufficientExecutionStackException" /> class. </summary>
		// Token: 0x06001066 RID: 4198 RVA: 0x00046CC5 File Offset: 0x00044EC5
		public InsufficientExecutionStackException() : base(Environment.GetResourceString("Insufficient stack to continue executing the program safely. This can happen from having too many functions on the call stack or function on the stack using too much stack space."))
		{
			base.SetErrorCode(-2146232968);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InsufficientExecutionStackException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x06001067 RID: 4199 RVA: 0x00046CE2 File Offset: 0x00044EE2
		public InsufficientExecutionStackException(string message) : base(message)
		{
			base.SetErrorCode(-2146232968);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InsufficientExecutionStackException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06001068 RID: 4200 RVA: 0x00046CF6 File Offset: 0x00044EF6
		public InsufficientExecutionStackException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146232968);
		}

		// Token: 0x06001069 RID: 4201 RVA: 0x000319C9 File Offset: 0x0002FBC9
		private InsufficientExecutionStackException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
