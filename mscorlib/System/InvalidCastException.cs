﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown for invalid casting or explicit conversion.</summary>
	// Token: 0x02000180 RID: 384
	[ComVisible(true)]
	[Serializable]
	public class InvalidCastException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidCastException" /> class.</summary>
		// Token: 0x060010CE RID: 4302 RVA: 0x000473BE File Offset: 0x000455BE
		public InvalidCastException() : base(Environment.GetResourceString("Specified cast is not valid."))
		{
			base.SetErrorCode(-2147467262);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidCastException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x060010CF RID: 4303 RVA: 0x000473DB File Offset: 0x000455DB
		public InvalidCastException(string message) : base(message)
		{
			base.SetErrorCode(-2147467262);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidCastException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060010D0 RID: 4304 RVA: 0x000473EF File Offset: 0x000455EF
		public InvalidCastException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2147467262);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidCastException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x060010D1 RID: 4305 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected InvalidCastException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidCastException" /> class with a specified message and error code.</summary>
		/// <param name="message">The message that indicates the reason the exception occurred.</param>
		/// <param name="errorCode">The error code (HRESULT) value associated with the exception.</param>
		// Token: 0x060010D2 RID: 4306 RVA: 0x00047404 File Offset: 0x00045604
		public InvalidCastException(string message, int errorCode) : base(message)
		{
			base.SetErrorCode(errorCode);
		}
	}
}
