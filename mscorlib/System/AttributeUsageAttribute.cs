﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies the usage of another attribute class. This class cannot be inherited.</summary>
	// Token: 0x0200011F RID: 287
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	[ComVisible(true)]
	[Serializable]
	public sealed class AttributeUsageAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.AttributeUsageAttribute" /> class with the specified list of <see cref="T:System.AttributeTargets" />, the <see cref="P:System.AttributeUsageAttribute.AllowMultiple" /> value, and the <see cref="P:System.AttributeUsageAttribute.Inherited" /> value.</summary>
		/// <param name="validOn">The set of values combined using a bitwise OR operation to indicate which program elements are valid. </param>
		// Token: 0x06000A77 RID: 2679 RVA: 0x0003343E File Offset: 0x0003163E
		public AttributeUsageAttribute(AttributeTargets validOn)
		{
			this.m_attributeTarget = validOn;
		}

		// Token: 0x06000A78 RID: 2680 RVA: 0x0003345F File Offset: 0x0003165F
		internal AttributeUsageAttribute(AttributeTargets validOn, bool allowMultiple, bool inherited)
		{
			this.m_attributeTarget = validOn;
			this.m_allowMultiple = allowMultiple;
			this.m_inherited = inherited;
		}

		/// <summary>Gets a set of values identifying which program elements that the indicated attribute can be applied to.</summary>
		/// <returns>One or several <see cref="T:System.AttributeTargets" /> values. The default is <see langword="All" />.</returns>
		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x06000A79 RID: 2681 RVA: 0x0003348E File Offset: 0x0003168E
		public AttributeTargets ValidOn
		{
			get
			{
				return this.m_attributeTarget;
			}
		}

		/// <summary>Gets or sets a Boolean value indicating whether more than one instance of the indicated attribute can be specified for a single program element.</summary>
		/// <returns>
		///     <see langword="true" /> if more than one instance is allowed to be specified; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170001AA RID: 426
		// (get) Token: 0x06000A7A RID: 2682 RVA: 0x00033496 File Offset: 0x00031696
		// (set) Token: 0x06000A7B RID: 2683 RVA: 0x0003349E File Offset: 0x0003169E
		public bool AllowMultiple
		{
			get
			{
				return this.m_allowMultiple;
			}
			set
			{
				this.m_allowMultiple = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value that determines whether the indicated attribute is inherited by derived classes and overriding members.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute can be inherited by derived classes and overriding members; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x170001AB RID: 427
		// (get) Token: 0x06000A7C RID: 2684 RVA: 0x000334A7 File Offset: 0x000316A7
		// (set) Token: 0x06000A7D RID: 2685 RVA: 0x000334AF File Offset: 0x000316AF
		public bool Inherited
		{
			get
			{
				return this.m_inherited;
			}
			set
			{
				this.m_inherited = value;
			}
		}

		// Token: 0x06000A7E RID: 2686 RVA: 0x000334B8 File Offset: 0x000316B8
		// Note: this type is marked as 'beforefieldinit'.
		static AttributeUsageAttribute()
		{
		}

		// Token: 0x04000781 RID: 1921
		internal AttributeTargets m_attributeTarget = AttributeTargets.All;

		// Token: 0x04000782 RID: 1922
		internal bool m_allowMultiple;

		// Token: 0x04000783 RID: 1923
		internal bool m_inherited = true;

		// Token: 0x04000784 RID: 1924
		internal static AttributeUsageAttribute Default = new AttributeUsageAttribute(AttributeTargets.All);
	}
}
