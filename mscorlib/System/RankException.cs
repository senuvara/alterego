﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when an array with the wrong number of dimensions is passed to a method.</summary>
	// Token: 0x020001A2 RID: 418
	[ComVisible(true)]
	[Serializable]
	public class RankException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.RankException" /> class.</summary>
		// Token: 0x060011D8 RID: 4568 RVA: 0x0004977D File Offset: 0x0004797D
		public RankException() : base(Environment.GetResourceString("Attempted to operate on an array with the incorrect number of dimensions."))
		{
			base.SetErrorCode(-2146233065);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.RankException" /> class with a specified error message.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. </param>
		// Token: 0x060011D9 RID: 4569 RVA: 0x0004979A File Offset: 0x0004799A
		public RankException(string message) : base(message)
		{
			base.SetErrorCode(-2146233065);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.RankException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060011DA RID: 4570 RVA: 0x000497AE File Offset: 0x000479AE
		public RankException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233065);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.RankException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x060011DB RID: 4571 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected RankException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
