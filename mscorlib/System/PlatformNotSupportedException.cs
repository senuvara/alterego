﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when a feature does not run on a particular platform.</summary>
	// Token: 0x0200019E RID: 414
	[ComVisible(true)]
	[Serializable]
	public class PlatformNotSupportedException : NotSupportedException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.PlatformNotSupportedException" /> class with default properties.</summary>
		// Token: 0x060011C2 RID: 4546 RVA: 0x00049338 File Offset: 0x00047538
		public PlatformNotSupportedException() : base(Environment.GetResourceString("Operation is not supported on this platform."))
		{
			base.SetErrorCode(-2146233031);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.PlatformNotSupportedException" /> class with a specified error message.</summary>
		/// <param name="message">The text message that explains the reason for the exception. </param>
		// Token: 0x060011C3 RID: 4547 RVA: 0x00049355 File Offset: 0x00047555
		public PlatformNotSupportedException(string message) : base(message)
		{
			base.SetErrorCode(-2146233031);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.PlatformNotSupportedException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060011C4 RID: 4548 RVA: 0x00049369 File Offset: 0x00047569
		public PlatformNotSupportedException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233031);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.PlatformNotSupportedException" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		// Token: 0x060011C5 RID: 4549 RVA: 0x0004937E File Offset: 0x0004757E
		protected PlatformNotSupportedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
