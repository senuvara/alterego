﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Security;

namespace System.Runtime
{
	/// <summary>Checks for sufficient memory resources before executing an operation. This class cannot be inherited.</summary>
	// Token: 0x020006CA RID: 1738
	public sealed class MemoryFailPoint : CriticalFinalizerObject, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.MemoryFailPoint" /> class, specifying the amount of memory required for successful execution. </summary>
		/// <param name="sizeInMegabytes">The required memory size, in megabytes. This must be a positive value.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The specified memory size is negative.</exception>
		/// <exception cref="T:System.InsufficientMemoryException">There is insufficient memory to begin execution of the code protected by the gate.</exception>
		// Token: 0x0600475E RID: 18270 RVA: 0x000F8AC5 File Offset: 0x000F6CC5
		[MonoTODO]
		public MemoryFailPoint(int sizeInMegabytes)
		{
			throw new NotImplementedException();
		}

		/// <summary>Ensures that resources are freed and other cleanup operations are performed when the garbage collector reclaims the <see cref="T:System.Runtime.MemoryFailPoint" /> object. </summary>
		// Token: 0x0600475F RID: 18271 RVA: 0x000F8AD4 File Offset: 0x000F6CD4
		~MemoryFailPoint()
		{
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Runtime.MemoryFailPoint" />. </summary>
		// Token: 0x06004760 RID: 18272 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		[SecuritySafeCritical]
		public void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}
