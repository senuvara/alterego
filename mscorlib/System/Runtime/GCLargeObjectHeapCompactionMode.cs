﻿using System;

namespace System.Runtime
{
	/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Indicates whether the next blocking garbage collection compacts the large object heap (LOH). </summary>
	// Token: 0x020006C7 RID: 1735
	public enum GCLargeObjectHeapCompactionMode
	{
		/// <summary>The large object heap (LOH) is not compacted.</summary>
		// Token: 0x040024CE RID: 9422
		Default = 1,
		/// <summary>The large object heap (LOH) will be compacted during the next blocking generation 2 garbage collection. </summary>
		// Token: 0x040024CF RID: 9423
		CompactOnce
	}
}
