﻿using System;
using System.Runtime.InteropServices;
using System.Security.Policy;

namespace System.Runtime.Hosting
{
	/// <summary>Provides data for manifest-based activation of an application. This class cannot be inherited. </summary>
	// Token: 0x020006CB RID: 1739
	[ComVisible(true)]
	[Serializable]
	public sealed class ActivationArguments : EvidenceBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Hosting.ActivationArguments" /> class with the specified activation context. </summary>
		/// <param name="activationData">An object that identifies the manifest-based activation application.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="activationData" /> is <see langword="null" />.</exception>
		// Token: 0x06004761 RID: 18273 RVA: 0x000F8AFC File Offset: 0x000F6CFC
		public ActivationArguments(ActivationContext activationData)
		{
			if (activationData == null)
			{
				throw new ArgumentNullException("activationData");
			}
			this._context = activationData;
			this._identity = activationData.Identity;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Hosting.ActivationArguments" /> class with the specified application identity.</summary>
		/// <param name="applicationIdentity">An object that identifies the manifest-based activation application.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="applicationIdentity" /> is <see langword="null" />.</exception>
		// Token: 0x06004762 RID: 18274 RVA: 0x000F8B25 File Offset: 0x000F6D25
		public ActivationArguments(ApplicationIdentity applicationIdentity)
		{
			if (applicationIdentity == null)
			{
				throw new ArgumentNullException("applicationIdentity");
			}
			this._identity = applicationIdentity;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Hosting.ActivationArguments" /> class with the specified activation context and activation data.</summary>
		/// <param name="activationContext">An object that identifies the manifest-based activation application.</param>
		/// <param name="activationData">An array of strings containing host-provided activation data.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="activationContext" /> is <see langword="null" />.</exception>
		// Token: 0x06004763 RID: 18275 RVA: 0x000F8B42 File Offset: 0x000F6D42
		public ActivationArguments(ActivationContext activationContext, string[] activationData)
		{
			if (activationContext == null)
			{
				throw new ArgumentNullException("activationContext");
			}
			this._context = activationContext;
			this._identity = activationContext.Identity;
			this._data = activationData;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Hosting.ActivationArguments" /> class with the specified application identity and activation data.</summary>
		/// <param name="applicationIdentity">An object that identifies the manifest-based activation application.</param>
		/// <param name="activationData">An array of strings containing host-provided activation data.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="applicationIdentity" /> is <see langword="null" />.</exception>
		// Token: 0x06004764 RID: 18276 RVA: 0x000F8B72 File Offset: 0x000F6D72
		public ActivationArguments(ApplicationIdentity applicationIdentity, string[] activationData)
		{
			if (applicationIdentity == null)
			{
				throw new ArgumentNullException("applicationIdentity");
			}
			this._identity = applicationIdentity;
			this._data = activationData;
		}

		/// <summary>Gets the activation context for manifest-based activation of an application.</summary>
		/// <returns>An object that identifies a manifest-based activation application.</returns>
		// Token: 0x17000BF9 RID: 3065
		// (get) Token: 0x06004765 RID: 18277 RVA: 0x000F8B96 File Offset: 0x000F6D96
		public ActivationContext ActivationContext
		{
			get
			{
				return this._context;
			}
		}

		/// <summary>Gets activation data from the host.</summary>
		/// <returns>An array of strings containing host-provided activation data.</returns>
		// Token: 0x17000BFA RID: 3066
		// (get) Token: 0x06004766 RID: 18278 RVA: 0x000F8B9E File Offset: 0x000F6D9E
		public string[] ActivationData
		{
			get
			{
				return this._data;
			}
		}

		/// <summary>Gets the application identity for a manifest-activated application.</summary>
		/// <returns>An object that identifies an application for manifest-based activation.</returns>
		// Token: 0x17000BFB RID: 3067
		// (get) Token: 0x06004767 RID: 18279 RVA: 0x000F8BA6 File Offset: 0x000F6DA6
		public ApplicationIdentity ApplicationIdentity
		{
			get
			{
				return this._identity;
			}
		}

		// Token: 0x040024D7 RID: 9431
		private ActivationContext _context;

		// Token: 0x040024D8 RID: 9432
		private ApplicationIdentity _identity;

		// Token: 0x040024D9 RID: 9433
		private string[] _data;
	}
}
