﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a modified instance is pinned in memory. This class cannot be inherited.</summary>
	// Token: 0x02000894 RID: 2196
	public static class IsPinned
	{
	}
}
