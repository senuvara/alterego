﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Allows you to obtain the full path of the source file that contains the caller. This is the file path at the time of compile.</summary>
	// Token: 0x02000855 RID: 2133
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	public sealed class CallerFilePathAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CallerFilePathAttribute" /> class.</summary>
		// Token: 0x060051C2 RID: 20930 RVA: 0x000020BF File Offset: 0x000002BF
		public CallerFilePathAttribute()
		{
		}
	}
}
