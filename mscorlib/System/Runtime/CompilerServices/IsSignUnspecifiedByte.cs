﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a modifier is neither signed nor unsigned. This class cannot be inherited.</summary>
	// Token: 0x02000895 RID: 2197
	public static class IsSignUnspecifiedByte
	{
	}
}
