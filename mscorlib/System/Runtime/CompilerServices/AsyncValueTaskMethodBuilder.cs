﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading.Tasks;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000841 RID: 2113
	[StructLayout(LayoutKind.Auto)]
	public struct AsyncValueTaskMethodBuilder<TResult>
	{
		// Token: 0x06005164 RID: 20836 RVA: 0x00115E24 File Offset: 0x00114024
		public static AsyncValueTaskMethodBuilder<TResult> Create()
		{
			return new AsyncValueTaskMethodBuilder<TResult>
			{
				_methodBuilder = AsyncTaskMethodBuilder<TResult>.Create()
			};
		}

		// Token: 0x06005165 RID: 20837 RVA: 0x00115E46 File Offset: 0x00114046
		public void Start<TStateMachine>(ref TStateMachine stateMachine) where TStateMachine : IAsyncStateMachine
		{
			this._methodBuilder.Start<TStateMachine>(ref stateMachine);
		}

		// Token: 0x06005166 RID: 20838 RVA: 0x00115E54 File Offset: 0x00114054
		public void SetStateMachine(IAsyncStateMachine stateMachine)
		{
			this._methodBuilder.SetStateMachine(stateMachine);
		}

		// Token: 0x06005167 RID: 20839 RVA: 0x00115E62 File Offset: 0x00114062
		public void SetResult(TResult result)
		{
			if (this._useBuilder)
			{
				this._methodBuilder.SetResult(result);
				return;
			}
			this._result = result;
			this._haveResult = true;
		}

		// Token: 0x06005168 RID: 20840 RVA: 0x00115E87 File Offset: 0x00114087
		public void SetException(Exception exception)
		{
			this._methodBuilder.SetException(exception);
		}

		// Token: 0x17000E35 RID: 3637
		// (get) Token: 0x06005169 RID: 20841 RVA: 0x00115E95 File Offset: 0x00114095
		public ValueTask<TResult> Task
		{
			get
			{
				if (this._haveResult)
				{
					return new ValueTask<TResult>(this._result);
				}
				this._useBuilder = true;
				return new ValueTask<TResult>(this._methodBuilder.Task);
			}
		}

		// Token: 0x0600516A RID: 20842 RVA: 0x00115EC2 File Offset: 0x001140C2
		public void AwaitOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine) where TAwaiter : INotifyCompletion where TStateMachine : IAsyncStateMachine
		{
			this._useBuilder = true;
			this._methodBuilder.AwaitOnCompleted<TAwaiter, TStateMachine>(ref awaiter, ref stateMachine);
		}

		// Token: 0x0600516B RID: 20843 RVA: 0x00115ED8 File Offset: 0x001140D8
		[SecuritySafeCritical]
		public void AwaitUnsafeOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine) where TAwaiter : ICriticalNotifyCompletion where TStateMachine : IAsyncStateMachine
		{
			this._useBuilder = true;
			this._methodBuilder.AwaitUnsafeOnCompleted<TAwaiter, TStateMachine>(ref awaiter, ref stateMachine);
		}

		// Token: 0x040029B1 RID: 10673
		private AsyncTaskMethodBuilder<TResult> _methodBuilder;

		// Token: 0x040029B2 RID: 10674
		private TResult _result;

		// Token: 0x040029B3 RID: 10675
		private bool _haveResult;

		// Token: 0x040029B4 RID: 10676
		private bool _useBuilder;
	}
}
