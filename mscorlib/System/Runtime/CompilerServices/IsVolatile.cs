﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Marks a field as volatile. This class cannot be inherited.</summary>
	// Token: 0x02000897 RID: 2199
	[ComVisible(true)]
	public static class IsVolatile
	{
	}
}
