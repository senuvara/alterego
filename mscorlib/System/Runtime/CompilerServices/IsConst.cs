﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that the modified type has a <see langword="const" /> modifier. This class cannot be inherited.</summary>
	// Token: 0x0200088E RID: 2190
	public static class IsConst
	{
	}
}
