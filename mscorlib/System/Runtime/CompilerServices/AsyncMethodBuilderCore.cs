﻿using System;
using System.Diagnostics;
using System.Runtime.ExceptionServices;
using System.Security;
using System.Threading;
using System.Threading.Tasks;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200084F RID: 2127
	internal struct AsyncMethodBuilderCore
	{
		// Token: 0x060051AE RID: 20910 RVA: 0x00116E38 File Offset: 0x00115038
		public void SetStateMachine(IAsyncStateMachine stateMachine)
		{
			if (stateMachine == null)
			{
				throw new ArgumentNullException("stateMachine");
			}
			if (this.m_stateMachine != null)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The builder was not properly initialized."));
			}
			this.m_stateMachine = stateMachine;
		}

		// Token: 0x060051AF RID: 20911 RVA: 0x00116E68 File Offset: 0x00115068
		[SecuritySafeCritical]
		internal Action GetCompletionAction(Task taskForTracing, ref AsyncMethodBuilderCore.MoveNextRunner runnerToInitialize)
		{
			Debugger.NotifyOfCrossThreadDependency();
			ExecutionContext executionContext = ExecutionContext.FastCapture();
			Action action;
			AsyncMethodBuilderCore.MoveNextRunner moveNextRunner;
			if (executionContext != null && executionContext.IsPreAllocatedDefault)
			{
				action = this.m_defaultContextAction;
				if (action != null)
				{
					return action;
				}
				moveNextRunner = new AsyncMethodBuilderCore.MoveNextRunner(executionContext, this.m_stateMachine);
				action = new Action(moveNextRunner.Run);
				if (taskForTracing != null)
				{
					action = (this.m_defaultContextAction = this.OutputAsyncCausalityEvents(taskForTracing, action));
				}
				else
				{
					this.m_defaultContextAction = action;
				}
			}
			else
			{
				moveNextRunner = new AsyncMethodBuilderCore.MoveNextRunner(executionContext, this.m_stateMachine);
				action = new Action(moveNextRunner.Run);
				if (taskForTracing != null)
				{
					action = this.OutputAsyncCausalityEvents(taskForTracing, action);
				}
			}
			if (this.m_stateMachine == null)
			{
				runnerToInitialize = moveNextRunner;
			}
			return action;
		}

		// Token: 0x060051B0 RID: 20912 RVA: 0x00116F04 File Offset: 0x00115104
		private Action OutputAsyncCausalityEvents(Task innerTask, Action continuation)
		{
			return AsyncMethodBuilderCore.CreateContinuationWrapper(continuation, delegate
			{
				AsyncCausalityTracer.TraceSynchronousWorkStart(CausalityTraceLevel.Required, innerTask.Id, CausalitySynchronousWork.Execution);
				continuation();
				AsyncCausalityTracer.TraceSynchronousWorkCompletion(CausalityTraceLevel.Required, CausalitySynchronousWork.Execution);
			}, innerTask);
		}

		// Token: 0x060051B1 RID: 20913 RVA: 0x00116F44 File Offset: 0x00115144
		internal void PostBoxInitialization(IAsyncStateMachine stateMachine, AsyncMethodBuilderCore.MoveNextRunner runner, Task builtTask)
		{
			if (builtTask != null)
			{
				if (AsyncCausalityTracer.LoggingOn)
				{
					AsyncCausalityTracer.TraceOperationCreation(CausalityTraceLevel.Required, builtTask.Id, "Async: " + stateMachine.GetType().Name, 0UL);
				}
				if (Task.s_asyncDebuggingEnabled)
				{
					Task.AddToActiveTasks(builtTask);
				}
			}
			this.m_stateMachine = stateMachine;
			this.m_stateMachine.SetStateMachine(this.m_stateMachine);
			runner.m_stateMachine = this.m_stateMachine;
		}

		// Token: 0x060051B2 RID: 20914 RVA: 0x00116FB0 File Offset: 0x001151B0
		internal static void ThrowAsync(Exception exception, SynchronizationContext targetContext)
		{
			ExceptionDispatchInfo state2 = ExceptionDispatchInfo.Capture(exception);
			if (targetContext != null)
			{
				try
				{
					targetContext.Post(delegate(object state)
					{
						((ExceptionDispatchInfo)state).Throw();
					}, state2);
					return;
				}
				catch (Exception ex)
				{
					state2 = ExceptionDispatchInfo.Capture(new AggregateException(new Exception[]
					{
						exception,
						ex
					}));
				}
			}
			ThreadPool.QueueUserWorkItem(delegate(object state)
			{
				((ExceptionDispatchInfo)state).Throw();
			}, state2);
		}

		// Token: 0x060051B3 RID: 20915 RVA: 0x00117044 File Offset: 0x00115244
		internal static Action CreateContinuationWrapper(Action continuation, Action invokeAction, Task innerTask = null)
		{
			return new Action(new AsyncMethodBuilderCore.ContinuationWrapper(continuation, invokeAction, innerTask).Invoke);
		}

		// Token: 0x060051B4 RID: 20916 RVA: 0x0011705C File Offset: 0x0011525C
		internal static Action TryGetStateMachineForDebugger(Action action)
		{
			object target = action.Target;
			AsyncMethodBuilderCore.MoveNextRunner moveNextRunner = target as AsyncMethodBuilderCore.MoveNextRunner;
			if (moveNextRunner != null)
			{
				return new Action(moveNextRunner.m_stateMachine.MoveNext);
			}
			AsyncMethodBuilderCore.ContinuationWrapper continuationWrapper = target as AsyncMethodBuilderCore.ContinuationWrapper;
			if (continuationWrapper != null)
			{
				return AsyncMethodBuilderCore.TryGetStateMachineForDebugger(continuationWrapper.m_continuation);
			}
			return action;
		}

		// Token: 0x060051B5 RID: 20917 RVA: 0x001170A4 File Offset: 0x001152A4
		internal static Task TryGetContinuationTask(Action action)
		{
			if (action != null)
			{
				AsyncMethodBuilderCore.ContinuationWrapper continuationWrapper = action.Target as AsyncMethodBuilderCore.ContinuationWrapper;
				if (continuationWrapper != null)
				{
					return continuationWrapper.m_innerTask;
				}
			}
			return null;
		}

		// Token: 0x040029CC RID: 10700
		internal IAsyncStateMachine m_stateMachine;

		// Token: 0x040029CD RID: 10701
		internal Action m_defaultContextAction;

		// Token: 0x02000850 RID: 2128
		internal sealed class MoveNextRunner
		{
			// Token: 0x060051B6 RID: 20918 RVA: 0x001170CB File Offset: 0x001152CB
			[SecurityCritical]
			internal MoveNextRunner(ExecutionContext context, IAsyncStateMachine stateMachine)
			{
				this.m_context = context;
				this.m_stateMachine = stateMachine;
			}

			// Token: 0x060051B7 RID: 20919 RVA: 0x001170E4 File Offset: 0x001152E4
			[SecuritySafeCritical]
			internal void Run()
			{
				if (this.m_context != null)
				{
					try
					{
						ContextCallback contextCallback = AsyncMethodBuilderCore.MoveNextRunner.s_invokeMoveNext;
						if (contextCallback == null)
						{
							contextCallback = (AsyncMethodBuilderCore.MoveNextRunner.s_invokeMoveNext = new ContextCallback(AsyncMethodBuilderCore.MoveNextRunner.InvokeMoveNext));
						}
						ExecutionContext.Run(this.m_context, contextCallback, this.m_stateMachine, true);
						return;
					}
					finally
					{
						this.m_context.Dispose();
					}
				}
				this.m_stateMachine.MoveNext();
			}

			// Token: 0x060051B8 RID: 20920 RVA: 0x00117154 File Offset: 0x00115354
			[SecurityCritical]
			private static void InvokeMoveNext(object stateMachine)
			{
				((IAsyncStateMachine)stateMachine).MoveNext();
			}

			// Token: 0x040029CE RID: 10702
			private readonly ExecutionContext m_context;

			// Token: 0x040029CF RID: 10703
			internal IAsyncStateMachine m_stateMachine;

			// Token: 0x040029D0 RID: 10704
			[SecurityCritical]
			private static ContextCallback s_invokeMoveNext;
		}

		// Token: 0x02000851 RID: 2129
		private class ContinuationWrapper
		{
			// Token: 0x060051B9 RID: 20921 RVA: 0x00117161 File Offset: 0x00115361
			internal ContinuationWrapper(Action continuation, Action invokeAction, Task innerTask)
			{
				if (innerTask == null)
				{
					innerTask = AsyncMethodBuilderCore.TryGetContinuationTask(continuation);
				}
				this.m_continuation = continuation;
				this.m_innerTask = innerTask;
				this.m_invokeAction = invokeAction;
			}

			// Token: 0x060051BA RID: 20922 RVA: 0x00117189 File Offset: 0x00115389
			internal void Invoke()
			{
				this.m_invokeAction();
			}

			// Token: 0x040029D1 RID: 10705
			internal readonly Action m_continuation;

			// Token: 0x040029D2 RID: 10706
			private readonly Action m_invokeAction;

			// Token: 0x040029D3 RID: 10707
			internal readonly Task m_innerTask;
		}

		// Token: 0x02000852 RID: 2130
		[CompilerGenerated]
		private sealed class <>c__DisplayClass4_0
		{
			// Token: 0x060051BB RID: 20923 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass4_0()
			{
			}

			// Token: 0x060051BC RID: 20924 RVA: 0x00117196 File Offset: 0x00115396
			internal void <OutputAsyncCausalityEvents>b__0()
			{
				AsyncCausalityTracer.TraceSynchronousWorkStart(CausalityTraceLevel.Required, this.innerTask.Id, CausalitySynchronousWork.Execution);
				this.continuation();
				AsyncCausalityTracer.TraceSynchronousWorkCompletion(CausalityTraceLevel.Required, CausalitySynchronousWork.Execution);
			}

			// Token: 0x040029D4 RID: 10708
			public Task innerTask;

			// Token: 0x040029D5 RID: 10709
			public Action continuation;
		}

		// Token: 0x02000853 RID: 2131
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x060051BD RID: 20925 RVA: 0x001171BC File Offset: 0x001153BC
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x060051BE RID: 20926 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x060051BF RID: 20927 RVA: 0x000C9228 File Offset: 0x000C7428
			internal void <ThrowAsync>b__6_0(object state)
			{
				((ExceptionDispatchInfo)state).Throw();
			}

			// Token: 0x060051C0 RID: 20928 RVA: 0x000C9228 File Offset: 0x000C7428
			internal void <ThrowAsync>b__6_1(object state)
			{
				((ExceptionDispatchInfo)state).Throw();
			}

			// Token: 0x040029D6 RID: 10710
			public static readonly AsyncMethodBuilderCore.<>c <>9 = new AsyncMethodBuilderCore.<>c();

			// Token: 0x040029D7 RID: 10711
			public static SendOrPostCallback <>9__6_0;

			// Token: 0x040029D8 RID: 10712
			public static WaitCallback <>9__6_1;
		}
	}
}
