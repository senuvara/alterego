﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Distinguishes a compiler-generated element from a user-generated element. This class cannot be inherited.</summary>
	// Token: 0x0200087B RID: 2171
	[AttributeUsage(AttributeTargets.All, Inherited = true)]
	[Serializable]
	public sealed class CompilerGeneratedAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CompilerGeneratedAttribute" /> class.</summary>
		// Token: 0x0600521D RID: 21021 RVA: 0x000020BF File Offset: 0x000002BF
		public CompilerGeneratedAttribute()
		{
		}
	}
}
