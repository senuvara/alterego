﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000871 RID: 2161
	[FriendAccessAllowed]
	internal sealed class AssemblyAttributesGoHereSM
	{
		// Token: 0x06005210 RID: 21008 RVA: 0x00002050 File Offset: 0x00000250
		internal AssemblyAttributesGoHereSM()
		{
		}
	}
}
