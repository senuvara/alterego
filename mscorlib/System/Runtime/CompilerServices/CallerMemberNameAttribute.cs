﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Allows you to obtain the method or property name of the caller to the method.</summary>
	// Token: 0x02000857 RID: 2135
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	public sealed class CallerMemberNameAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CallerMemberNameAttribute" /> class.</summary>
		// Token: 0x060051C4 RID: 20932 RVA: 0x000020BF File Offset: 0x000002BF
		public CallerMemberNameAttribute()
		{
		}
	}
}
