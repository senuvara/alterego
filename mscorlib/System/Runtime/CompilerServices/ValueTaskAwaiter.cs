﻿using System;
using System.Threading.Tasks;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000844 RID: 2116
	public struct ValueTaskAwaiter<TResult> : ICriticalNotifyCompletion, INotifyCompletion
	{
		// Token: 0x06005173 RID: 20851 RVA: 0x00115FEF File Offset: 0x001141EF
		internal ValueTaskAwaiter(ValueTask<TResult> value)
		{
			this._value = value;
		}

		// Token: 0x17000E37 RID: 3639
		// (get) Token: 0x06005174 RID: 20852 RVA: 0x00115FF8 File Offset: 0x001141F8
		public bool IsCompleted
		{
			get
			{
				return this._value.IsCompleted;
			}
		}

		// Token: 0x06005175 RID: 20853 RVA: 0x00116014 File Offset: 0x00114214
		public TResult GetResult()
		{
			if (this._value._task != null)
			{
				return this._value._task.GetAwaiter().GetResult();
			}
			return this._value._result;
		}

		// Token: 0x06005176 RID: 20854 RVA: 0x00116054 File Offset: 0x00114254
		public void OnCompleted(Action continuation)
		{
			this._value.AsTask().ConfigureAwait(true).GetAwaiter().OnCompleted(continuation);
		}

		// Token: 0x06005177 RID: 20855 RVA: 0x00116088 File Offset: 0x00114288
		public void UnsafeOnCompleted(Action continuation)
		{
			this._value.AsTask().ConfigureAwait(true).GetAwaiter().UnsafeOnCompleted(continuation);
		}

		// Token: 0x040029B9 RID: 10681
		private readonly ValueTask<TResult> _value;
	}
}
