﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200086F RID: 2159
	[FriendAccessAllowed]
	internal sealed class AssemblyAttributesGoHereS
	{
		// Token: 0x0600520E RID: 21006 RVA: 0x00002050 File Offset: 0x00000250
		internal AssemblyAttributesGoHereS()
		{
		}
	}
}
