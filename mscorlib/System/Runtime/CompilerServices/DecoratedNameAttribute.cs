﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000881 RID: 2177
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.All)]
	internal sealed class DecoratedNameAttribute : Attribute
	{
		// Token: 0x06005229 RID: 21033 RVA: 0x000020BF File Offset: 0x000002BF
		public DecoratedNameAttribute(string decoratedName)
		{
		}
	}
}
