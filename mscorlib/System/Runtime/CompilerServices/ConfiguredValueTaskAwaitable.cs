﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000842 RID: 2114
	[StructLayout(LayoutKind.Auto)]
	public struct ConfiguredValueTaskAwaitable<TResult>
	{
		// Token: 0x0600516C RID: 20844 RVA: 0x00115EEE File Offset: 0x001140EE
		internal ConfiguredValueTaskAwaitable(ValueTask<TResult> value, bool continueOnCapturedContext)
		{
			this._value = value;
			this._continueOnCapturedContext = continueOnCapturedContext;
		}

		// Token: 0x0600516D RID: 20845 RVA: 0x00115EFE File Offset: 0x001140FE
		public ConfiguredValueTaskAwaitable<TResult>.ConfiguredValueTaskAwaiter GetAwaiter()
		{
			return new ConfiguredValueTaskAwaitable<TResult>.ConfiguredValueTaskAwaiter(this._value, this._continueOnCapturedContext);
		}

		// Token: 0x040029B5 RID: 10677
		private readonly ValueTask<TResult> _value;

		// Token: 0x040029B6 RID: 10678
		private readonly bool _continueOnCapturedContext;

		// Token: 0x02000843 RID: 2115
		[StructLayout(LayoutKind.Auto)]
		public struct ConfiguredValueTaskAwaiter : ICriticalNotifyCompletion, INotifyCompletion
		{
			// Token: 0x0600516E RID: 20846 RVA: 0x00115F11 File Offset: 0x00114111
			internal ConfiguredValueTaskAwaiter(ValueTask<TResult> value, bool continueOnCapturedContext)
			{
				this._value = value;
				this._continueOnCapturedContext = continueOnCapturedContext;
			}

			// Token: 0x17000E36 RID: 3638
			// (get) Token: 0x0600516F RID: 20847 RVA: 0x00115F24 File Offset: 0x00114124
			public bool IsCompleted
			{
				get
				{
					return this._value.IsCompleted;
				}
			}

			// Token: 0x06005170 RID: 20848 RVA: 0x00115F40 File Offset: 0x00114140
			public TResult GetResult()
			{
				if (this._value._task != null)
				{
					return this._value._task.GetAwaiter().GetResult();
				}
				return this._value._result;
			}

			// Token: 0x06005171 RID: 20849 RVA: 0x00115F80 File Offset: 0x00114180
			public void OnCompleted(Action continuation)
			{
				this._value.AsTask().ConfigureAwait(this._continueOnCapturedContext).GetAwaiter().OnCompleted(continuation);
			}

			// Token: 0x06005172 RID: 20850 RVA: 0x00115FB8 File Offset: 0x001141B8
			public void UnsafeOnCompleted(Action continuation)
			{
				this._value.AsTask().ConfigureAwait(this._continueOnCapturedContext).GetAwaiter().UnsafeOnCompleted(continuation);
			}

			// Token: 0x040029B7 RID: 10679
			private readonly ValueTask<TResult> _value;

			// Token: 0x040029B8 RID: 10680
			private readonly bool _continueOnCapturedContext;
		}
	}
}
