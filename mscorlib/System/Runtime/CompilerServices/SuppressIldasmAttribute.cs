﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Prevents the Ildasm.exe (IL Disassembler) from disassembling an assembly. This class cannot be inherited.</summary>
	// Token: 0x020008A0 RID: 2208
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Module)]
	public sealed class SuppressIldasmAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.SuppressIldasmAttribute" /> class. </summary>
		// Token: 0x06005246 RID: 21062 RVA: 0x000020BF File Offset: 0x000002BF
		public SuppressIldasmAttribute()
		{
		}
	}
}
