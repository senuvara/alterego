﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Provides a static method to create a <see cref="T:System.FormattableString" /> object from a composite format string and its arguments. </summary>
	// Token: 0x02000858 RID: 2136
	public static class FormattableStringFactory
	{
		/// <summary>Creates a <see cref="T:System.FormattableString" /> instance from a composite format string and its arguments. </summary>
		/// <param name="format">A composite format string. </param>
		/// <param name="arguments">The arguments whose string representations are to be inserted in the result string. </param>
		/// <returns>The object that represents the composite format string and its arguments. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="format" /> is <see langword="null" />. -or-
		///         <paramref name="arguments" /> is <see langword="null" />. </exception>
		// Token: 0x060051C5 RID: 20933 RVA: 0x001171D1 File Offset: 0x001153D1
		public static FormattableString Create(string format, params object[] arguments)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}
			if (arguments == null)
			{
				throw new ArgumentNullException("arguments");
			}
			return new FormattableStringFactory.ConcreteFormattableString(format, arguments);
		}

		// Token: 0x02000859 RID: 2137
		private sealed class ConcreteFormattableString : FormattableString
		{
			// Token: 0x060051C6 RID: 20934 RVA: 0x001171F6 File Offset: 0x001153F6
			internal ConcreteFormattableString(string format, object[] arguments)
			{
				this._format = format;
				this._arguments = arguments;
			}

			// Token: 0x17000E41 RID: 3649
			// (get) Token: 0x060051C7 RID: 20935 RVA: 0x0011720C File Offset: 0x0011540C
			public override string Format
			{
				get
				{
					return this._format;
				}
			}

			// Token: 0x060051C8 RID: 20936 RVA: 0x00117214 File Offset: 0x00115414
			public override object[] GetArguments()
			{
				return this._arguments;
			}

			// Token: 0x17000E42 RID: 3650
			// (get) Token: 0x060051C9 RID: 20937 RVA: 0x0011721C File Offset: 0x0011541C
			public override int ArgumentCount
			{
				get
				{
					return this._arguments.Length;
				}
			}

			// Token: 0x060051CA RID: 20938 RVA: 0x00117226 File Offset: 0x00115426
			public override object GetArgument(int index)
			{
				return this._arguments[index];
			}

			// Token: 0x060051CB RID: 20939 RVA: 0x00117230 File Offset: 0x00115430
			public override string ToString(IFormatProvider formatProvider)
			{
				return string.Format(formatProvider, this._format, this._arguments);
			}

			// Token: 0x040029D9 RID: 10713
			private readonly string _format;

			// Token: 0x040029DA RID: 10714
			private readonly object[] _arguments;
		}
	}
}
