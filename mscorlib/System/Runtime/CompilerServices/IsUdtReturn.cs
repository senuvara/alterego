﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a return type is a user-defined type. This class cannot be inherited.</summary>
	// Token: 0x02000896 RID: 2198
	public static class IsUdtReturn
	{
	}
}
