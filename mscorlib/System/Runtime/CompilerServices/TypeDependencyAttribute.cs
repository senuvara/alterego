﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x020008A2 RID: 2210
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface, AllowMultiple = true, Inherited = false)]
	internal sealed class TypeDependencyAttribute : Attribute
	{
		// Token: 0x06005248 RID: 21064 RVA: 0x00117B10 File Offset: 0x00115D10
		public TypeDependencyAttribute(string typeName)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			this.typeName = typeName;
		}

		// Token: 0x04002A0F RID: 10767
		private string typeName;
	}
}
