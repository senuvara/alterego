﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200086E RID: 2158
	[FriendAccessAllowed]
	internal sealed class AssemblyAttributesGoHere
	{
		// Token: 0x0600520D RID: 21005 RVA: 0x00002050 File Offset: 0x00000250
		internal AssemblyAttributesGoHere()
		{
		}
	}
}
