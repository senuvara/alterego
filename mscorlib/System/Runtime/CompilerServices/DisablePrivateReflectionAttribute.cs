﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that any private members contained in an assembly's types are not available to reflection. </summary>
	// Token: 0x02000882 RID: 2178
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	public sealed class DisablePrivateReflectionAttribute : Attribute
	{
		/// <summary>Initializes a new instances of the <see cref="T:System.Runtime.CompilerServices.DisablePrivateReflectionAttribute" /> class. </summary>
		// Token: 0x0600522A RID: 21034 RVA: 0x000020BF File Offset: 0x000002BF
		public DisablePrivateReflectionAttribute()
		{
		}
	}
}
