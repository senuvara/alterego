﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000840 RID: 2112
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false, AllowMultiple = false)]
	public sealed class AsyncMethodBuilderAttribute : Attribute
	{
		// Token: 0x06005162 RID: 20834 RVA: 0x00115E0A File Offset: 0x0011400A
		public AsyncMethodBuilderAttribute(Type builderType)
		{
			this.BuilderType = builderType;
		}

		// Token: 0x17000E34 RID: 3636
		// (get) Token: 0x06005163 RID: 20835 RVA: 0x00115E19 File Offset: 0x00114019
		public Type BuilderType
		{
			[CompilerGenerated]
			get
			{
				return this.<BuilderType>k__BackingField;
			}
		}

		// Token: 0x040029B0 RID: 10672
		[CompilerGenerated]
		private readonly Type <BuilderType>k__BackingField;
	}
}
