﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a method should use the <see langword="Cdecl" /> calling convention.</summary>
	// Token: 0x02000875 RID: 2165
	[ComVisible(true)]
	public class CallConvCdecl
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CallConvCdecl" /> class. </summary>
		// Token: 0x06005216 RID: 21014 RVA: 0x00002050 File Offset: 0x00000250
		public CallConvCdecl()
		{
		}
	}
}
