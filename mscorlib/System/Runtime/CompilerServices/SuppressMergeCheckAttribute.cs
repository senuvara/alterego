﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x020008A1 RID: 2209
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event)]
	internal sealed class SuppressMergeCheckAttribute : Attribute
	{
		// Token: 0x06005247 RID: 21063 RVA: 0x000020BF File Offset: 0x000002BF
		public SuppressMergeCheckAttribute()
		{
		}
	}
}
