﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Identifies an assembly as a reference assembly, which contains metadata but no executable code.</summary>
	// Token: 0x0200085E RID: 2142
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
	[Serializable]
	public sealed class ReferenceAssemblyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.ReferenceAssemblyAttribute" /> class. </summary>
		// Token: 0x060051D1 RID: 20945 RVA: 0x000020BF File Offset: 0x000002BF
		public ReferenceAssemblyAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.ReferenceAssemblyAttribute" /> class by using the specified description. </summary>
		/// <param name="description">The description of the reference assembly. </param>
		// Token: 0x060051D2 RID: 20946 RVA: 0x00117244 File Offset: 0x00115444
		public ReferenceAssemblyAttribute(string description)
		{
			this._description = description;
		}

		/// <summary>Gets the description of the reference assembly.</summary>
		/// <returns>The description of the reference assembly.</returns>
		// Token: 0x17000E43 RID: 3651
		// (get) Token: 0x060051D3 RID: 20947 RVA: 0x00117253 File Offset: 0x00115453
		public string Description
		{
			get
			{
				return this._description;
			}
		}

		// Token: 0x040029DB RID: 10715
		private string _description;
	}
}
