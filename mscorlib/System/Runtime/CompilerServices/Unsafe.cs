﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x020008AD RID: 2221
	internal static class Unsafe
	{
		// Token: 0x06005281 RID: 21121 RVA: 0x00118524 File Offset: 0x00116724
		public static ref T Add<T>(ref T source, int elementOffset)
		{
			return ref source + (IntPtr)elementOffset * (IntPtr)sizeof(T);
		}

		// Token: 0x06005282 RID: 21122 RVA: 0x00118531 File Offset: 0x00116731
		public static ref T Add<T>(ref T source, IntPtr elementOffset)
		{
			return ref source + elementOffset * (IntPtr)sizeof(T);
		}

		// Token: 0x06005283 RID: 21123 RVA: 0x00118524 File Offset: 0x00116724
		public unsafe static void* Add<T>(void* source, int elementOffset)
		{
			return (void*)((byte*)source + (IntPtr)elementOffset * (IntPtr)sizeof(T));
		}

		// Token: 0x06005284 RID: 21124 RVA: 0x0011853D File Offset: 0x0011673D
		public static ref T AddByteOffset<T>(ref T source, IntPtr byteOffset)
		{
			return ref source + byteOffset;
		}

		// Token: 0x06005285 RID: 21125 RVA: 0x0003C5DE File Offset: 0x0003A7DE
		public static bool AreSame<T>(ref T left, ref T right)
		{
			return ref left == ref right;
		}

		// Token: 0x06005286 RID: 21126 RVA: 0x00002058 File Offset: 0x00000258
		public static T As<T>(object o) where T : class
		{
			return o;
		}

		// Token: 0x06005287 RID: 21127 RVA: 0x00002058 File Offset: 0x00000258
		public static ref TTo As<TFrom, TTo>(ref TFrom source)
		{
			return ref source;
		}

		// Token: 0x06005288 RID: 21128 RVA: 0x00002058 File Offset: 0x00000258
		public unsafe static ref T AsRef<T>(void* source)
		{
			return ref *(T*)source;
		}

		// Token: 0x06005289 RID: 21129 RVA: 0x00118542 File Offset: 0x00116742
		public static IntPtr ByteOffset<T>(ref T origin, ref T target)
		{
			return ref target - ref origin;
		}

		// Token: 0x0600528A RID: 21130 RVA: 0x00118547 File Offset: 0x00116747
		public static void CopyBlock(ref byte destination, ref byte source, uint byteCount)
		{
			cpblk(ref destination, ref source, byteCount);
		}

		// Token: 0x0600528B RID: 21131 RVA: 0x0011854E File Offset: 0x0011674E
		public static void InitBlockUnaligned(ref byte startAddress, byte value, uint byteCount)
		{
			initblk(ref startAddress, value, byteCount);
		}

		// Token: 0x0600528C RID: 21132 RVA: 0x0011854E File Offset: 0x0011674E
		public unsafe static void InitBlockUnaligned(void* startAddress, byte value, uint byteCount)
		{
			initblk(startAddress, value, byteCount);
		}

		// Token: 0x0600528D RID: 21133 RVA: 0x00118558 File Offset: 0x00116758
		public unsafe static T Read<T>(void* source)
		{
			return *(T*)source;
		}

		// Token: 0x0600528E RID: 21134 RVA: 0x00118560 File Offset: 0x00116760
		public static T ReadUnaligned<T>(ref byte source)
		{
			return source;
		}

		// Token: 0x0600528F RID: 21135 RVA: 0x0011856B File Offset: 0x0011676B
		public static int SizeOf<T>()
		{
			return sizeof(T);
		}

		// Token: 0x06005290 RID: 21136 RVA: 0x00118573 File Offset: 0x00116773
		public static ref T Subtract<T>(ref T source, int elementOffset)
		{
			return ref source - (IntPtr)elementOffset * (IntPtr)sizeof(T);
		}

		// Token: 0x06005291 RID: 21137 RVA: 0x00118580 File Offset: 0x00116780
		public static void WriteUnaligned<T>(ref byte destination, T value)
		{
			destination = value;
		}
	}
}
