﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Specifies a destination <see cref="T:System.Type" /> in another assembly. </summary>
	// Token: 0x0200086A RID: 2154
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
	public sealed class TypeForwardedToAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.TypeForwardedToAttribute" /> class specifying a destination <see cref="T:System.Type" />. </summary>
		/// <param name="destination">The destination <see cref="T:System.Type" /> in another assembly.</param>
		// Token: 0x06005201 RID: 20993 RVA: 0x001175E9 File Offset: 0x001157E9
		public TypeForwardedToAttribute(Type destination)
		{
			this._destination = destination;
		}

		/// <summary>Gets the destination <see cref="T:System.Type" /> in another assembly.</summary>
		/// <returns>The destination <see cref="T:System.Type" /> in another assembly.</returns>
		// Token: 0x17000E4C RID: 3660
		// (get) Token: 0x06005202 RID: 20994 RVA: 0x001175F8 File Offset: 0x001157F8
		public Type Destination
		{
			get
			{
				return this._destination;
			}
		}

		// Token: 0x040029EA RID: 10730
		private Type _destination;
	}
}
