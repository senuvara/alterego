﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Marks a program element as read-only. </summary>
	// Token: 0x02000847 RID: 2119
	[AttributeUsage(AttributeTargets.All, Inherited = false)]
	public sealed class IsReadOnlyAttribute : Attribute
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Runtime.CompilerServices.IsReadOnlyAttribute" /> class. </summary>
		// Token: 0x0600517B RID: 20859 RVA: 0x000020BF File Offset: 0x000002BF
		public IsReadOnlyAttribute()
		{
		}
	}
}
