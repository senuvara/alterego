﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x020008A5 RID: 2213
	[FriendAccessAllowed]
	internal static class JitHelpers
	{
		// Token: 0x0600524B RID: 21067 RVA: 0x00117B2D File Offset: 0x00115D2D
		internal static T UnsafeCast<T>(object o) where T : class
		{
			return Array.UnsafeMov<object, T>(o);
		}

		// Token: 0x0600524C RID: 21068 RVA: 0x00117B35 File Offset: 0x00115D35
		internal static int UnsafeEnumCast<T>(T val) where T : struct
		{
			return Array.UnsafeMov<T, int>(val);
		}

		// Token: 0x0600524D RID: 21069 RVA: 0x00117B3D File Offset: 0x00115D3D
		internal static long UnsafeEnumCastLong<T>(T val) where T : struct
		{
			return Array.UnsafeMov<T, long>(val);
		}
	}
}
