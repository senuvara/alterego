﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a method should use the <see langword="StdCall" /> calling convention.</summary>
	// Token: 0x02000876 RID: 2166
	[ComVisible(true)]
	public class CallConvStdcall
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CallConvStdcall" /> class. </summary>
		// Token: 0x06005217 RID: 21015 RVA: 0x00002050 File Offset: 0x00000250
		public CallConvStdcall()
		{
		}
	}
}
