﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Specifies the preferred default binding for a dependent assembly.</summary>
	// Token: 0x02000872 RID: 2162
	[Serializable]
	public enum LoadHint
	{
		/// <summary>No preference specified.</summary>
		// Token: 0x040029EF RID: 10735
		Default,
		/// <summary>The dependency is always loaded.</summary>
		// Token: 0x040029F0 RID: 10736
		Always,
		/// <summary>The dependency is sometimes loaded.</summary>
		// Token: 0x040029F1 RID: 10737
		Sometimes
	}
}
