﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000870 RID: 2160
	[FriendAccessAllowed]
	internal sealed class AssemblyAttributesGoHereM
	{
		// Token: 0x0600520F RID: 21007 RVA: 0x00002050 File Offset: 0x00000250
		internal AssemblyAttributesGoHereM()
		{
		}
	}
}
