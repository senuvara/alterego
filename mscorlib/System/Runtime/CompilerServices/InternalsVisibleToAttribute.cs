﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Specifies that types that are ordinarily visible only within the current assembly are visible to a specified assembly.</summary>
	// Token: 0x0200088A RID: 2186
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
	public sealed class InternalsVisibleToAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.InternalsVisibleToAttribute" /> class with the name of the specified friend assembly. </summary>
		/// <param name="assemblyName">The name of a friend assembly.</param>
		// Token: 0x06005235 RID: 21045 RVA: 0x00117A86 File Offset: 0x00115C86
		public InternalsVisibleToAttribute(string assemblyName)
		{
			this._assemblyName = assemblyName;
		}

		/// <summary>Gets the name of the friend assembly to which all types and type members that are marked with the <see langword="internal" /> keyword are to be made visible. </summary>
		/// <returns>A string that represents the name of the friend assembly.</returns>
		// Token: 0x17000E59 RID: 3673
		// (get) Token: 0x06005236 RID: 21046 RVA: 0x00117A9C File Offset: 0x00115C9C
		public string AssemblyName
		{
			get
			{
				return this._assemblyName;
			}
		}

		/// <summary>This property is not implemented.</summary>
		/// <returns>This property does not return a value.</returns>
		// Token: 0x17000E5A RID: 3674
		// (get) Token: 0x06005237 RID: 21047 RVA: 0x00117AA4 File Offset: 0x00115CA4
		// (set) Token: 0x06005238 RID: 21048 RVA: 0x00117AAC File Offset: 0x00115CAC
		public bool AllInternalsVisible
		{
			get
			{
				return this._allInternalsVisible;
			}
			set
			{
				this._allInternalsVisible = value;
			}
		}

		// Token: 0x040029FC RID: 10748
		private string _assemblyName;

		// Token: 0x040029FD RID: 10749
		private bool _allInternalsVisible = true;
	}
}
