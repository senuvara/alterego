﻿using System;
using System.Runtime.Serialization;
using System.Security;
using Unity;

namespace System.Runtime.CompilerServices
{
	/// <summary>Wraps an exception that does not derive from the <see cref="T:System.Exception" /> class. This class cannot be inherited.</summary>
	// Token: 0x02000860 RID: 2144
	[Serializable]
	public sealed class RuntimeWrappedException : Exception
	{
		// Token: 0x060051D7 RID: 20951 RVA: 0x0011726C File Offset: 0x0011546C
		private RuntimeWrappedException(object thrownObject) : base(Environment.GetResourceString("An object that does not derive from System.Exception has been wrapped in a RuntimeWrappedException."))
		{
			base.SetErrorCode(-2146233026);
			this.m_wrappedException = thrownObject;
		}

		/// <summary>Gets the object that was wrapped by the <see cref="T:System.Runtime.CompilerServices.RuntimeWrappedException" /> object.</summary>
		/// <returns>The object that was wrapped by the <see cref="T:System.Runtime.CompilerServices.RuntimeWrappedException" /> object.</returns>
		// Token: 0x17000E45 RID: 3653
		// (get) Token: 0x060051D8 RID: 20952 RVA: 0x00117290 File Offset: 0x00115490
		public object WrappedException
		{
			get
			{
				return this.m_wrappedException;
			}
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with information about the exception.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> object that contains contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is <see langword="null" />.</exception>
		// Token: 0x060051D9 RID: 20953 RVA: 0x00117298 File Offset: 0x00115498
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
			info.AddValue("WrappedException", this.m_wrappedException, typeof(object));
		}

		// Token: 0x060051DA RID: 20954 RVA: 0x001172CB File Offset: 0x001154CB
		internal RuntimeWrappedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.m_wrappedException = info.GetValue("WrappedException", typeof(object));
		}

		// Token: 0x060051DB RID: 20955 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal RuntimeWrappedException()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040029DD RID: 10717
		private object m_wrappedException;
	}
}
