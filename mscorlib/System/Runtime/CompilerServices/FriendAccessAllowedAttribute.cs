﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200088B RID: 2187
	[FriendAccessAllowed]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
	internal sealed class FriendAccessAllowedAttribute : Attribute
	{
		// Token: 0x06005239 RID: 21049 RVA: 0x000020BF File Offset: 0x000002BF
		public FriendAccessAllowedAttribute()
		{
		}
	}
}
