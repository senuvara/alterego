﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Specifies the name of the property that accesses the attributed field.</summary>
	// Token: 0x0200086D RID: 2157
	[AttributeUsage(AttributeTargets.Field)]
	[ComVisible(true)]
	public sealed class AccessedThroughPropertyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see langword="AccessedThroughPropertyAttribute" /> class with the name of the property used to access the attributed field.</summary>
		/// <param name="propertyName">The name of the property used to access the attributed field. </param>
		// Token: 0x0600520B RID: 21003 RVA: 0x001176D6 File Offset: 0x001158D6
		public AccessedThroughPropertyAttribute(string propertyName)
		{
			this.propertyName = propertyName;
		}

		/// <summary>Gets the name of the property used to access the attributed field.</summary>
		/// <returns>The name of the property used to access the attributed field.</returns>
		// Token: 0x17000E4E RID: 3662
		// (get) Token: 0x0600520C RID: 21004 RVA: 0x001176E5 File Offset: 0x001158E5
		public string PropertyName
		{
			get
			{
				return this.propertyName;
			}
		}

		// Token: 0x040029ED RID: 10733
		private readonly string propertyName;
	}
}
