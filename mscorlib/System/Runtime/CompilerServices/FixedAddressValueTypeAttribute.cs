﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Fixes the address of a static value type field throughout its lifetime. This class cannot be inherited.</summary>
	// Token: 0x02000885 RID: 2181
	[AttributeUsage(AttributeTargets.Field)]
	[Serializable]
	public sealed class FixedAddressValueTypeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.FixedAddressValueTypeAttribute" /> class. </summary>
		// Token: 0x0600522D RID: 21037 RVA: 0x000020BF File Offset: 0x000002BF
		public FixedAddressValueTypeAttribute()
		{
		}
	}
}
