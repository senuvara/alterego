﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that the modified garbage collection reference represents a reference parameter within a method signature. This class cannot be inherited.</summary>
	// Token: 0x02000891 RID: 2193
	public static class IsImplicitlyDereferenced
	{
	}
}
