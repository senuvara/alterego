﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a method is an extension method, or that a class or assembly contains extension methods.</summary>
	// Token: 0x02000884 RID: 2180
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Method)]
	public sealed class ExtensionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.ExtensionAttribute" /> class. </summary>
		// Token: 0x0600522C RID: 21036 RVA: 0x000020BF File Offset: 0x000002BF
		public ExtensionAttribute()
		{
		}
	}
}
