﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that the default value for the attributed field or parameter is an instance of <see cref="T:System.Runtime.InteropServices.DispatchWrapper" />, where the <see cref="P:System.Runtime.InteropServices.DispatchWrapper.WrappedObject" /> is <see langword="null" />.</summary>
	// Token: 0x02000888 RID: 2184
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter, Inherited = false)]
	[Serializable]
	public sealed class IDispatchConstantAttribute : CustomConstantAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.IDispatchConstantAttribute" /> class.</summary>
		// Token: 0x06005232 RID: 21042 RVA: 0x00117A76 File Offset: 0x00115C76
		public IDispatchConstantAttribute()
		{
		}

		/// <summary>Gets the <see langword="IDispatch" /> constant stored in this attribute.</summary>
		/// <returns>The <see langword="IDispatch" /> constant stored in this attribute. Only <see langword="null" /> is allowed for an <see langword="IDispatch" /> constant value.</returns>
		// Token: 0x17000E58 RID: 3672
		// (get) Token: 0x06005233 RID: 21043 RVA: 0x00117A7E File Offset: 0x00115C7E
		public override object Value
		{
			get
			{
				return new DispatchWrapper(null);
			}
		}
	}
}
