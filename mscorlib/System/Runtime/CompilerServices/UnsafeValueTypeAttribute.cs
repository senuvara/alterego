﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Specifies that a type contains an unmanaged array that might potentially overflow. This class cannot be inherited.</summary>
	// Token: 0x020008A3 RID: 2211
	[AttributeUsage(AttributeTargets.Struct)]
	[Serializable]
	public sealed class UnsafeValueTypeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.UnsafeValueTypeAttribute" /> class.</summary>
		// Token: 0x06005249 RID: 21065 RVA: 0x000020BF File Offset: 0x000002BF
		public UnsafeValueTypeAttribute()
		{
		}
	}
}
