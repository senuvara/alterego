﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>This class is not used in the .NET Framework version 2.0 and is reserved for future use. This class cannot be inherited.</summary>
	// Token: 0x02000887 RID: 2183
	[AttributeUsage(AttributeTargets.Struct)]
	[Serializable]
	public sealed class HasCopySemanticsAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.HasCopySemanticsAttribute" /> class. </summary>
		// Token: 0x06005231 RID: 21041 RVA: 0x000020BF File Offset: 0x000002BF
		public HasCopySemanticsAttribute()
		{
		}
	}
}
