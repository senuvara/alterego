﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a structure is byref-like. </summary>
	// Token: 0x02000846 RID: 2118
	[AttributeUsage(AttributeTargets.Struct)]
	public sealed class IsByRefLikeAttribute : Attribute
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Runtime.CompilerServices.IsByRefLikeAttribute" /> class. </summary>
		// Token: 0x0600517A RID: 20858 RVA: 0x000020BF File Offset: 0x000002BF
		public IsByRefLikeAttribute()
		{
		}
	}
}
