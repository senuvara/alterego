﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Specifies a source <see cref="T:System.Type" /> in another assembly. </summary>
	// Token: 0x02000869 RID: 2153
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false, AllowMultiple = false)]
	public sealed class TypeForwardedFromAttribute : Attribute
	{
		// Token: 0x060051FE RID: 20990 RVA: 0x000020BF File Offset: 0x000002BF
		private TypeForwardedFromAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.TypeForwardedFromAttribute" /> class. </summary>
		/// <param name="assemblyFullName">The source <see cref="T:System.Type" /> in another assembly. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="assemblyFullName" /> is <see langword="null" /> or empty.</exception>
		// Token: 0x060051FF RID: 20991 RVA: 0x001175BF File Offset: 0x001157BF
		public TypeForwardedFromAttribute(string assemblyFullName)
		{
			if (string.IsNullOrEmpty(assemblyFullName))
			{
				throw new ArgumentNullException("assemblyFullName");
			}
			this.assemblyFullName = assemblyFullName;
		}

		/// <summary>Gets the assembly-qualified name of the source type.</summary>
		/// <returns>The assembly-qualified name of the source type.</returns>
		// Token: 0x17000E4B RID: 3659
		// (get) Token: 0x06005200 RID: 20992 RVA: 0x001175E1 File Offset: 0x001157E1
		public string AssemblyFullName
		{
			get
			{
				return this.assemblyFullName;
			}
		}

		// Token: 0x040029E9 RID: 10729
		private string assemblyFullName;
	}
}
