﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that the modified reference type is a boxed value type. This class cannot be inherited.</summary>
	// Token: 0x0200088C RID: 2188
	public static class IsBoxed
	{
	}
}
