﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Marks a type definition as discardable.</summary>
	// Token: 0x02000883 RID: 2179
	[ComVisible(true)]
	public class DiscardableAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.DiscardableAttribute" /> class with default values.</summary>
		// Token: 0x0600522B RID: 21035 RVA: 0x000020BF File Offset: 0x000002BF
		public DiscardableAttribute()
		{
		}
	}
}
