﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a field should be treated as containing a fixed number of elements of the specified primitive type. This class cannot be inherited. </summary>
	// Token: 0x02000886 RID: 2182
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	public sealed class FixedBufferAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.FixedBufferAttribute" /> class. </summary>
		/// <param name="elementType">The type of the elements contained in the buffer.</param>
		/// <param name="length">The number of elements in the buffer.</param>
		// Token: 0x0600522E RID: 21038 RVA: 0x00117A50 File Offset: 0x00115C50
		public FixedBufferAttribute(Type elementType, int length)
		{
			this.elementType = elementType;
			this.length = length;
		}

		/// <summary>Gets the type of the elements contained in the fixed buffer. </summary>
		/// <returns>The type of the elements.</returns>
		// Token: 0x17000E56 RID: 3670
		// (get) Token: 0x0600522F RID: 21039 RVA: 0x00117A66 File Offset: 0x00115C66
		public Type ElementType
		{
			get
			{
				return this.elementType;
			}
		}

		/// <summary>Gets the number of elements in the fixed buffer. </summary>
		/// <returns>The number of elements in the fixed buffer.</returns>
		// Token: 0x17000E57 RID: 3671
		// (get) Token: 0x06005230 RID: 21040 RVA: 0x00117A6E File Offset: 0x00115C6E
		public int Length
		{
			get
			{
				return this.length;
			}
		}

		// Token: 0x040029FA RID: 10746
		private Type elementType;

		// Token: 0x040029FB RID: 10747
		private int length;
	}
}
