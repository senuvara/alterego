﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x020008A6 RID: 2214
	internal struct Ephemeron
	{
		// Token: 0x04002A10 RID: 10768
		internal object key;

		// Token: 0x04002A11 RID: 10769
		internal object value;
	}
}
