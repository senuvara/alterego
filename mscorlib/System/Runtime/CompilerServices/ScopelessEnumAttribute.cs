﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a native enumeration is not qualified by the enumeration type name. This class cannot be inherited.</summary>
	// Token: 0x0200089E RID: 2206
	[AttributeUsage(AttributeTargets.Enum)]
	[Serializable]
	public sealed class ScopelessEnumAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.ScopelessEnumAttribute" /> class. </summary>
		// Token: 0x06005244 RID: 21060 RVA: 0x000020BF File Offset: 0x000002BF
		public ScopelessEnumAttribute()
		{
		}
	}
}
