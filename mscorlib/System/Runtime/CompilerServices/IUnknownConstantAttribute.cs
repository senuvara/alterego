﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that the default value for the attributed field or parameter is an instance of <see cref="T:System.Runtime.InteropServices.UnknownWrapper" />, where the <see cref="P:System.Runtime.InteropServices.UnknownWrapper.WrappedObject" /> is <see langword="null" />. This class cannot be inherited. </summary>
	// Token: 0x02000898 RID: 2200
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter, Inherited = false)]
	[Serializable]
	public sealed class IUnknownConstantAttribute : CustomConstantAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.IUnknownConstantAttribute" /> class.</summary>
		// Token: 0x0600523A RID: 21050 RVA: 0x00117A76 File Offset: 0x00115C76
		public IUnknownConstantAttribute()
		{
		}

		/// <summary>Gets the <see langword="IUnknown" /> constant stored in this attribute.</summary>
		/// <returns>The <see langword="IUnknown" /> constant stored in this attribute. Only <see langword="null" /> is allowed for an <see langword="IUnknown" /> constant value.</returns>
		// Token: 0x17000E5B RID: 3675
		// (get) Token: 0x0600523B RID: 21051 RVA: 0x00117AB5 File Offset: 0x00115CB5
		public override object Value
		{
			get
			{
				return new UnknownWrapper(null);
			}
		}
	}
}
