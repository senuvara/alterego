﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Allows you to obtain the line number in the source file at which the method is called.</summary>
	// Token: 0x02000856 RID: 2134
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	public sealed class CallerLineNumberAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CallerLineNumberAttribute" /> class.</summary>
		// Token: 0x060051C3 RID: 20931 RVA: 0x000020BF File Offset: 0x000002BF
		public CallerLineNumberAttribute()
		{
		}
	}
}
