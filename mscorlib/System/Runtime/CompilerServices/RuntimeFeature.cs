﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>A class whose static <see cref="M:System.Runtime.CompilerServices.RuntimeFeature.IsSupported(System.String)" /> method checks whether a specified feature is supported by the common language runtime. </summary>
	// Token: 0x02000848 RID: 2120
	public static class RuntimeFeature
	{
		/// <summary>Determines whether a specified feature is supported by the common language runtime. </summary>
		/// <param name="feature">The name of the feature. </param>
		/// <returns>
		///   <see langword="true" /> if <paramref name="feature" /> is supported; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600517C RID: 20860 RVA: 0x001160BA File Offset: 0x001142BA
		public static bool IsSupported(string feature)
		{
			return feature == "PortablePdb";
		}

		/// <summary>Gets the name of the portable PDB feature. </summary>
		/// <returns>The name of the portable PDB feature. This method always returns the string "PortablePdb". </returns>
		// Token: 0x040029BA RID: 10682
		public const string PortablePdb = "PortablePdb";
	}
}
