﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a modified integer is a standard C++ <see langword="long" /> value. This class cannot be inherited.</summary>
	// Token: 0x02000893 RID: 2195
	public static class IsLong
	{
	}
}
