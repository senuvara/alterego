﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a method should use the <see langword="ThisCall" /> calling convention.</summary>
	// Token: 0x02000877 RID: 2167
	[ComVisible(true)]
	public class CallConvThiscall
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CallConvThiscall" /> class. </summary>
		// Token: 0x06005218 RID: 21016 RVA: 0x00002050 File Offset: 0x00000250
		public CallConvThiscall()
		{
		}
	}
}
