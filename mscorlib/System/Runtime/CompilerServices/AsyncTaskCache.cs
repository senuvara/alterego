﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200084E RID: 2126
	internal static class AsyncTaskCache
	{
		// Token: 0x060051AB RID: 20907 RVA: 0x00116DC4 File Offset: 0x00114FC4
		private static Task<int>[] CreateInt32Tasks()
		{
			Task<int>[] array = new Task<int>[10];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = AsyncTaskCache.CreateCacheableTask<int>(i + -1);
			}
			return array;
		}

		// Token: 0x060051AC RID: 20908 RVA: 0x00116DF4 File Offset: 0x00114FF4
		internal static Task<TResult> CreateCacheableTask<TResult>(TResult result)
		{
			return new Task<TResult>(false, result, (TaskCreationOptions)16384, default(CancellationToken));
		}

		// Token: 0x060051AD RID: 20909 RVA: 0x00116E16 File Offset: 0x00115016
		// Note: this type is marked as 'beforefieldinit'.
		static AsyncTaskCache()
		{
		}

		// Token: 0x040029C7 RID: 10695
		internal static readonly Task<bool> TrueTask = AsyncTaskCache.CreateCacheableTask<bool>(true);

		// Token: 0x040029C8 RID: 10696
		internal static readonly Task<bool> FalseTask = AsyncTaskCache.CreateCacheableTask<bool>(false);

		// Token: 0x040029C9 RID: 10697
		internal static readonly Task<int>[] Int32Tasks = AsyncTaskCache.CreateInt32Tasks();

		// Token: 0x040029CA RID: 10698
		internal const int INCLUSIVE_INT32_MIN = -1;

		// Token: 0x040029CB RID: 10699
		internal const int EXCLUSIVE_INT32_MAX = 9;
	}
}
