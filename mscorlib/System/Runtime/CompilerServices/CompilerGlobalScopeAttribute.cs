﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a class should be treated as if it has global scope.</summary>
	// Token: 0x0200087C RID: 2172
	[AttributeUsage(AttributeTargets.Class)]
	[ComVisible(true)]
	[Serializable]
	public class CompilerGlobalScopeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CompilerGlobalScopeAttribute" /> class.</summary>
		// Token: 0x0600521E RID: 21022 RVA: 0x000020BF File Offset: 0x000002BF
		public CompilerGlobalScopeAttribute()
		{
		}
	}
}
