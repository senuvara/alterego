﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a managed pointer represents a pointer parameter within a method signature. This class cannot be inherited.</summary>
	// Token: 0x02000890 RID: 2192
	public static class IsExplicitlyDereferenced
	{
	}
}
