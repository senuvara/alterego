﻿using System;

namespace System.Runtime.CompilerServices
{
	/// <summary>Indicates that a modified method is an intrinsic value for which the just-in-time (JIT) compiler  can perform special code generation. This class cannot be inherited.</summary>
	// Token: 0x02000892 RID: 2194
	public static class IsJitIntrinsic
	{
	}
}
