﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>This calling convention is not supported in this version of the .NET Framework.</summary>
	// Token: 0x02000878 RID: 2168
	[ComVisible(true)]
	public class CallConvFastcall
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.CompilerServices.CallConvFastcall" /> class. </summary>
		// Token: 0x06005219 RID: 21017 RVA: 0x00002050 File Offset: 0x00000250
		public CallConvFastcall()
		{
		}
	}
}
