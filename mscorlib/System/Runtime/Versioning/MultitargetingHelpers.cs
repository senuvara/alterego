﻿using System;
using System.Runtime.CompilerServices;
using System.Security;
using System.Threading;

namespace System.Runtime.Versioning
{
	// Token: 0x020006D0 RID: 1744
	internal static class MultitargetingHelpers
	{
		// Token: 0x0600476F RID: 18287 RVA: 0x000F8CAC File Offset: 0x000F6EAC
		internal static string GetAssemblyQualifiedName(Type type, Func<Type, string> converter)
		{
			string text = null;
			if (type != null)
			{
				if (converter != null)
				{
					try
					{
						text = converter(type);
					}
					catch (Exception ex)
					{
						if (MultitargetingHelpers.IsSecurityOrCriticalException(ex))
						{
							throw;
						}
					}
				}
				if (text == null)
				{
					text = MultitargetingHelpers.defaultConverter(type);
				}
			}
			return text;
		}

		// Token: 0x06004770 RID: 18288 RVA: 0x000F8CFC File Offset: 0x000F6EFC
		private static bool IsCriticalException(Exception ex)
		{
			return ex is NullReferenceException || ex is StackOverflowException || ex is OutOfMemoryException || ex is ThreadAbortException || ex is IndexOutOfRangeException || ex is AccessViolationException;
		}

		// Token: 0x06004771 RID: 18289 RVA: 0x000F8D31 File Offset: 0x000F6F31
		private static bool IsSecurityOrCriticalException(Exception ex)
		{
			return ex is SecurityException || MultitargetingHelpers.IsCriticalException(ex);
		}

		// Token: 0x06004772 RID: 18290 RVA: 0x000F8D43 File Offset: 0x000F6F43
		// Note: this type is marked as 'beforefieldinit'.
		static MultitargetingHelpers()
		{
		}

		// Token: 0x040024E0 RID: 9440
		private static Func<Type, string> defaultConverter = (Type t) => t.AssemblyQualifiedName;

		// Token: 0x020006D1 RID: 1745
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06004773 RID: 18291 RVA: 0x000F8D5A File Offset: 0x000F6F5A
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06004774 RID: 18292 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x06004775 RID: 18293 RVA: 0x000F8D66 File Offset: 0x000F6F66
			internal string <.cctor>b__4_0(Type t)
			{
				return t.AssemblyQualifiedName;
			}

			// Token: 0x040024E1 RID: 9441
			public static readonly MultitargetingHelpers.<>c <>9 = new MultitargetingHelpers.<>c();
		}
	}
}
