﻿using System;

namespace System.Runtime.Versioning
{
	// Token: 0x020006DA RID: 1754
	public static class CompatibilitySwitch
	{
		// Token: 0x06004787 RID: 18311 RVA: 0x00002526 File Offset: 0x00000726
		public static bool IsEnabled(string compatibilitySwitchName)
		{
			return false;
		}

		// Token: 0x06004788 RID: 18312 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public static string GetValue(string compatibilitySwitchName)
		{
			return null;
		}

		// Token: 0x06004789 RID: 18313 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		internal static string GetValueInternal(string compatibilitySwitchName)
		{
			return null;
		}
	}
}
