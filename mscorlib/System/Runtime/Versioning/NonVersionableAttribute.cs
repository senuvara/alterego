﻿using System;
using System.Diagnostics;

namespace System.Runtime.Versioning
{
	// Token: 0x020006CD RID: 1741
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	[Conditional("FEATURE_READYTORUN")]
	internal sealed class NonVersionableAttribute : Attribute
	{
		// Token: 0x0600476C RID: 18284 RVA: 0x000020BF File Offset: 0x000002BF
		public NonVersionableAttribute()
		{
		}
	}
}
