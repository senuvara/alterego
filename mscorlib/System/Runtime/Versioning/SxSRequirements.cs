﻿using System;

namespace System.Runtime.Versioning
{
	// Token: 0x020006D5 RID: 1749
	[Flags]
	internal enum SxSRequirements
	{
		// Token: 0x040024EE RID: 9454
		None = 0,
		// Token: 0x040024EF RID: 9455
		AppDomainID = 1,
		// Token: 0x040024F0 RID: 9456
		ProcessID = 2,
		// Token: 0x040024F1 RID: 9457
		CLRInstanceID = 4,
		// Token: 0x040024F2 RID: 9458
		AssemblyName = 8,
		// Token: 0x040024F3 RID: 9459
		TypeName = 16
	}
}
