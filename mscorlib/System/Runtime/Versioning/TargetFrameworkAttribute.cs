﻿using System;

namespace System.Runtime.Versioning
{
	/// <summary>Identifies the version of the .NET Framework that a particular assembly was compiled against.</summary>
	// Token: 0x020006D7 RID: 1751
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	public sealed class TargetFrameworkAttribute : Attribute
	{
		/// <summary>Initializes an instance of the <see cref="T:System.Runtime.Versioning.TargetFrameworkAttribute" /> class by specifying the .NET Framework version against which an assembly was built.</summary>
		/// <param name="frameworkName">The version of the .NET Framework against which the assembly was built.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="frameworkName" /> is <see langword="null" />.</exception>
		// Token: 0x06004781 RID: 18305 RVA: 0x000F905D File Offset: 0x000F725D
		public TargetFrameworkAttribute(string frameworkName)
		{
			if (frameworkName == null)
			{
				throw new ArgumentNullException("frameworkName");
			}
			this._frameworkName = frameworkName;
		}

		/// <summary>Gets the name of the .NET Framework version against which a particular assembly was compiled.</summary>
		/// <returns>The name of the .NET Framework version with which the assembly was compiled.</returns>
		// Token: 0x17000C00 RID: 3072
		// (get) Token: 0x06004782 RID: 18306 RVA: 0x000F907A File Offset: 0x000F727A
		public string FrameworkName
		{
			get
			{
				return this._frameworkName;
			}
		}

		/// <summary>Gets the display name of the .NET Framework version against which an assembly was built.</summary>
		/// <returns>The display name of the .NET Framework version.</returns>
		// Token: 0x17000C01 RID: 3073
		// (get) Token: 0x06004783 RID: 18307 RVA: 0x000F9082 File Offset: 0x000F7282
		// (set) Token: 0x06004784 RID: 18308 RVA: 0x000F908A File Offset: 0x000F728A
		public string FrameworkDisplayName
		{
			get
			{
				return this._frameworkDisplayName;
			}
			set
			{
				this._frameworkDisplayName = value;
			}
		}

		// Token: 0x040024F6 RID: 9462
		private string _frameworkName;

		// Token: 0x040024F7 RID: 9463
		private string _frameworkDisplayName;
	}
}
