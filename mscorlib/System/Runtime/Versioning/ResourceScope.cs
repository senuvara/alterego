﻿using System;

namespace System.Runtime.Versioning
{
	/// <summary>Identifies the scope of a sharable resource.</summary>
	// Token: 0x020006D4 RID: 1748
	[Flags]
	public enum ResourceScope
	{
		/// <summary>There is no shared state.</summary>
		// Token: 0x040024E6 RID: 9446
		None = 0,
		/// <summary>The state is shared by objects within the machine.</summary>
		// Token: 0x040024E7 RID: 9447
		Machine = 1,
		/// <summary>The state is shared within a process.</summary>
		// Token: 0x040024E8 RID: 9448
		Process = 2,
		/// <summary>The state is shared by objects within an <see cref="T:System.AppDomain" />.</summary>
		// Token: 0x040024E9 RID: 9449
		AppDomain = 4,
		/// <summary>The state is shared by objects within a library.</summary>
		// Token: 0x040024EA RID: 9450
		Library = 8,
		/// <summary>The resource is visible to only the type.</summary>
		// Token: 0x040024EB RID: 9451
		Private = 16,
		/// <summary>The resource is visible at an assembly scope.</summary>
		// Token: 0x040024EC RID: 9452
		Assembly = 32
	}
}
