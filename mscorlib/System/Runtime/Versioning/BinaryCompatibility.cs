﻿using System;

namespace System.Runtime.Versioning
{
	// Token: 0x020006D9 RID: 1753
	internal static class BinaryCompatibility
	{
		// Token: 0x17000C02 RID: 3074
		// (get) Token: 0x06004785 RID: 18309 RVA: 0x00004E08 File Offset: 0x00003008
		public static bool TargetsAtLeast_Desktop_V4_5_2
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06004786 RID: 18310 RVA: 0x000F9093 File Offset: 0x000F7293
		// Note: this type is marked as 'beforefieldinit'.
		static BinaryCompatibility()
		{
		}

		// Token: 0x04002501 RID: 9473
		public static readonly bool TargetsAtLeast_Desktop_V4_5 = true;

		// Token: 0x04002502 RID: 9474
		public static readonly bool TargetsAtLeast_Desktop_V4_5_1 = true;
	}
}
