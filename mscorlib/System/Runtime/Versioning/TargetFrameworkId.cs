﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime.Versioning
{
	// Token: 0x020006D8 RID: 1752
	[FriendAccessAllowed]
	internal enum TargetFrameworkId
	{
		// Token: 0x040024F9 RID: 9465
		NotYetChecked,
		// Token: 0x040024FA RID: 9466
		Unrecognized,
		// Token: 0x040024FB RID: 9467
		Unspecified,
		// Token: 0x040024FC RID: 9468
		NetFramework,
		// Token: 0x040024FD RID: 9469
		Portable,
		// Token: 0x040024FE RID: 9470
		NetCore,
		// Token: 0x040024FF RID: 9471
		Silverlight,
		// Token: 0x04002500 RID: 9472
		Phone
	}
}
