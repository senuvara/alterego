﻿using System;

namespace System.Runtime
{
	/// <summary>Specifies patch band information for targeted patching of the .NET Framework.</summary>
	// Token: 0x020006C5 RID: 1733
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyTargetedPatchBandAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.AssemblyTargetedPatchBandAttribute" /> class.</summary>
		/// <param name="targetedPatchBand">The patch band.</param>
		// Token: 0x06004754 RID: 18260 RVA: 0x000F8A88 File Offset: 0x000F6C88
		public AssemblyTargetedPatchBandAttribute(string targetedPatchBand)
		{
			this.m_targetedPatchBand = targetedPatchBand;
		}

		/// <summary>Gets the patch band. </summary>
		/// <returns>The patch band information.</returns>
		// Token: 0x17000BF4 RID: 3060
		// (get) Token: 0x06004755 RID: 18261 RVA: 0x000F8A97 File Offset: 0x000F6C97
		public string TargetedPatchBand
		{
			get
			{
				return this.m_targetedPatchBand;
			}
		}

		// Token: 0x040024CB RID: 9419
		private string m_targetedPatchBand;
	}
}
