﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Threading;

namespace System.Runtime.Serialization
{
	// Token: 0x020006F5 RID: 1781
	[Serializable]
	internal sealed class SafeSerializationManager : IObjectReference, ISerializable
	{
		// Token: 0x1400001D RID: 29
		// (add) Token: 0x06004879 RID: 18553 RVA: 0x000FBC20 File Offset: 0x000F9E20
		// (remove) Token: 0x0600487A RID: 18554 RVA: 0x000FBC58 File Offset: 0x000F9E58
		internal event EventHandler<SafeSerializationEventArgs> SerializeObjectState
		{
			[CompilerGenerated]
			add
			{
				EventHandler<SafeSerializationEventArgs> eventHandler = this.SerializeObjectState;
				EventHandler<SafeSerializationEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<SafeSerializationEventArgs> value2 = (EventHandler<SafeSerializationEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<SafeSerializationEventArgs>>(ref this.SerializeObjectState, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<SafeSerializationEventArgs> eventHandler = this.SerializeObjectState;
				EventHandler<SafeSerializationEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<SafeSerializationEventArgs> value2 = (EventHandler<SafeSerializationEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<SafeSerializationEventArgs>>(ref this.SerializeObjectState, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x0600487B RID: 18555 RVA: 0x00002050 File Offset: 0x00000250
		internal SafeSerializationManager()
		{
		}

		// Token: 0x0600487C RID: 18556 RVA: 0x000FBC90 File Offset: 0x000F9E90
		[SecurityCritical]
		private SafeSerializationManager(SerializationInfo info, StreamingContext context)
		{
			RuntimeType runtimeType = info.GetValueNoThrow("CLR_SafeSerializationManager_RealType", typeof(RuntimeType)) as RuntimeType;
			if (runtimeType == null)
			{
				this.m_serializedStates = (info.GetValue("m_serializedStates", typeof(List<object>)) as List<object>);
				return;
			}
			this.m_realType = runtimeType;
			this.m_savedSerializationInfo = info;
		}

		// Token: 0x17000C28 RID: 3112
		// (get) Token: 0x0600487D RID: 18557 RVA: 0x000FBCF6 File Offset: 0x000F9EF6
		internal bool IsActive
		{
			get
			{
				return this.SerializeObjectState != null;
			}
		}

		// Token: 0x0600487E RID: 18558 RVA: 0x000FBD04 File Offset: 0x000F9F04
		[SecurityCritical]
		internal void CompleteSerialization(object serializedObject, SerializationInfo info, StreamingContext context)
		{
			this.m_serializedStates = null;
			EventHandler<SafeSerializationEventArgs> serializeObjectState = this.SerializeObjectState;
			if (serializeObjectState != null)
			{
				SafeSerializationEventArgs safeSerializationEventArgs = new SafeSerializationEventArgs(context);
				serializeObjectState(serializedObject, safeSerializationEventArgs);
				this.m_serializedStates = safeSerializationEventArgs.SerializedStates;
				info.AddValue("CLR_SafeSerializationManager_RealType", serializedObject.GetType(), typeof(RuntimeType));
				info.SetType(typeof(SafeSerializationManager));
			}
		}

		// Token: 0x0600487F RID: 18559 RVA: 0x000FBD68 File Offset: 0x000F9F68
		internal void CompleteDeserialization(object deserializedObject)
		{
			if (this.m_serializedStates != null)
			{
				foreach (object obj in this.m_serializedStates)
				{
					((ISafeSerializationData)obj).CompleteDeserialization(deserializedObject);
				}
			}
		}

		// Token: 0x06004880 RID: 18560 RVA: 0x000FBDC0 File Offset: 0x000F9FC0
		[SecurityCritical]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("m_serializedStates", this.m_serializedStates, typeof(List<IDeserializationCallback>));
		}

		// Token: 0x06004881 RID: 18561 RVA: 0x000FBDE0 File Offset: 0x000F9FE0
		[SecurityCritical]
		object IObjectReference.GetRealObject(StreamingContext context)
		{
			if (this.m_realObject != null)
			{
				return this.m_realObject;
			}
			if (this.m_realType == null)
			{
				return this;
			}
			Stack stack = new Stack();
			RuntimeType runtimeType = this.m_realType;
			do
			{
				stack.Push(runtimeType);
				runtimeType = (runtimeType.BaseType as RuntimeType);
			}
			while (runtimeType != typeof(object));
			RuntimeType t;
			RuntimeConstructorInfo runtimeConstructorInfo;
			do
			{
				t = runtimeType;
				runtimeType = (stack.Pop() as RuntimeType);
				runtimeConstructorInfo = runtimeType.GetSerializationCtor();
			}
			while (runtimeConstructorInfo != null && runtimeConstructorInfo.IsSecurityCritical);
			runtimeConstructorInfo = ObjectManager.GetConstructor(t);
			object uninitializedObject = FormatterServices.GetUninitializedObject(this.m_realType);
			runtimeConstructorInfo.SerializationInvoke(uninitializedObject, this.m_savedSerializationInfo, context);
			this.m_savedSerializationInfo = null;
			this.m_realType = null;
			this.m_realObject = uninitializedObject;
			return uninitializedObject;
		}

		// Token: 0x06004882 RID: 18562 RVA: 0x000FBEA3 File Offset: 0x000FA0A3
		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			if (this.m_realObject != null)
			{
				SerializationEventsCache.GetSerializationEventsForType(this.m_realObject.GetType()).InvokeOnDeserialized(this.m_realObject, context);
				this.m_realObject = null;
			}
		}

		// Token: 0x0400254E RID: 9550
		private IList<object> m_serializedStates;

		// Token: 0x0400254F RID: 9551
		private SerializationInfo m_savedSerializationInfo;

		// Token: 0x04002550 RID: 9552
		private object m_realObject;

		// Token: 0x04002551 RID: 9553
		private RuntimeType m_realType;

		// Token: 0x04002552 RID: 9554
		[CompilerGenerated]
		private EventHandler<SafeSerializationEventArgs> SerializeObjectState;

		// Token: 0x04002553 RID: 9555
		private const string RealTypeSerializationName = "CLR_SafeSerializationManager_RealType";
	}
}
