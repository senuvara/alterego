﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	/// <summary>Provides functionality for formatting serialized objects.</summary>
	// Token: 0x020006E3 RID: 1763
	[ComVisible(true)]
	public interface IFormatter
	{
		/// <summary>Deserializes the data on the provided stream and reconstitutes the graph of objects.</summary>
		/// <param name="serializationStream">The stream that contains the data to deserialize. </param>
		/// <returns>The top object of the deserialized graph.</returns>
		// Token: 0x060047E1 RID: 18401
		object Deserialize(Stream serializationStream);

		/// <summary>Serializes an object, or graph of objects with the given root to the provided stream.</summary>
		/// <param name="serializationStream">The stream where the formatter puts the serialized data. This stream can reference a variety of backing stores (such as files, network, memory, and so on). </param>
		/// <param name="graph">The object, or root of the object graph, to serialize. All child objects of this root object are automatically serialized. </param>
		// Token: 0x060047E2 RID: 18402
		void Serialize(Stream serializationStream, object graph);

		/// <summary>Gets or sets the <see cref="T:System.Runtime.Serialization.SurrogateSelector" /> used by the current formatter.</summary>
		/// <returns>The <see cref="T:System.Runtime.Serialization.SurrogateSelector" /> used by this formatter.</returns>
		// Token: 0x17000C06 RID: 3078
		// (get) Token: 0x060047E3 RID: 18403
		// (set) Token: 0x060047E4 RID: 18404
		ISurrogateSelector SurrogateSelector { get; set; }

		/// <summary>Gets or sets the <see cref="T:System.Runtime.Serialization.SerializationBinder" /> that performs type lookups during deserialization.</summary>
		/// <returns>The <see cref="T:System.Runtime.Serialization.SerializationBinder" /> that performs type lookups during deserialization.</returns>
		// Token: 0x17000C07 RID: 3079
		// (get) Token: 0x060047E5 RID: 18405
		// (set) Token: 0x060047E6 RID: 18406
		SerializationBinder Binder { get; set; }

		/// <summary>Gets or sets the <see cref="T:System.Runtime.Serialization.StreamingContext" /> used for serialization and deserialization.</summary>
		/// <returns>The <see cref="T:System.Runtime.Serialization.StreamingContext" /> used for serialization and deserialization.</returns>
		// Token: 0x17000C08 RID: 3080
		// (get) Token: 0x060047E7 RID: 18407
		// (set) Token: 0x060047E8 RID: 18408
		StreamingContext Context { get; set; }
	}
}
