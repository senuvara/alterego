﻿using System;
using System.Diagnostics;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000711 RID: 1809
	internal static class SerTrace
	{
		// Token: 0x06004925 RID: 18725 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("_LOGGING")]
		internal static void InfoLog(params object[] messages)
		{
		}

		// Token: 0x06004926 RID: 18726 RVA: 0x000FD59D File Offset: 0x000FB79D
		[Conditional("SER_LOGGING")]
		internal static void Log(params object[] messages)
		{
			if (!(messages[0] is string))
			{
				messages[0] = messages[0].GetType().Name + " ";
				return;
			}
			messages[0] = messages[0] + " ";
		}
	}
}
