﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Metadata;

namespace System.Runtime.Serialization.Formatters
{
	/// <summary>Contains information for a server fault. This class cannot be inherited.</summary>
	// Token: 0x02000713 RID: 1811
	[ComVisible(true)]
	[SoapType(Embedded = true)]
	[Serializable]
	public sealed class ServerFault
	{
		// Token: 0x06004933 RID: 18739 RVA: 0x000FD7CD File Offset: 0x000FB9CD
		internal ServerFault(Exception exception)
		{
			this.exception = exception;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Formatters.ServerFault" /> class.</summary>
		/// <param name="exceptionType">The type of the exception that occurred on the server. </param>
		/// <param name="message">The message that accompanied the exception. </param>
		/// <param name="stackTrace">The stack trace of the thread that threw the exception on the server. </param>
		// Token: 0x06004934 RID: 18740 RVA: 0x000FD7DC File Offset: 0x000FB9DC
		public ServerFault(string exceptionType, string message, string stackTrace)
		{
			this.exceptionType = exceptionType;
			this.message = message;
			this.stackTrace = stackTrace;
		}

		/// <summary>Gets or sets the type of exception that was thrown by the server.</summary>
		/// <returns>The type of exception that was thrown by the server.</returns>
		// Token: 0x17000C55 RID: 3157
		// (get) Token: 0x06004935 RID: 18741 RVA: 0x000FD7F9 File Offset: 0x000FB9F9
		// (set) Token: 0x06004936 RID: 18742 RVA: 0x000FD801 File Offset: 0x000FBA01
		public string ExceptionType
		{
			get
			{
				return this.exceptionType;
			}
			set
			{
				this.exceptionType = value;
			}
		}

		/// <summary>Gets or sets the exception message that accompanied the exception thrown on the server.</summary>
		/// <returns>The exception message that accompanied the exception thrown on the server.</returns>
		// Token: 0x17000C56 RID: 3158
		// (get) Token: 0x06004937 RID: 18743 RVA: 0x000FD80A File Offset: 0x000FBA0A
		// (set) Token: 0x06004938 RID: 18744 RVA: 0x000FD812 File Offset: 0x000FBA12
		public string ExceptionMessage
		{
			get
			{
				return this.message;
			}
			set
			{
				this.message = value;
			}
		}

		/// <summary>Gets or sets the stack trace of the thread that threw the exception on the server.</summary>
		/// <returns>The stack trace of the thread that threw the exception on the server.</returns>
		// Token: 0x17000C57 RID: 3159
		// (get) Token: 0x06004939 RID: 18745 RVA: 0x000FD81B File Offset: 0x000FBA1B
		// (set) Token: 0x0600493A RID: 18746 RVA: 0x000FD823 File Offset: 0x000FBA23
		public string StackTrace
		{
			get
			{
				return this.stackTrace;
			}
			set
			{
				this.stackTrace = value;
			}
		}

		// Token: 0x17000C58 RID: 3160
		// (get) Token: 0x0600493B RID: 18747 RVA: 0x000FD82C File Offset: 0x000FBA2C
		internal Exception Exception
		{
			get
			{
				return this.exception;
			}
		}

		// Token: 0x0400259A RID: 9626
		private string exceptionType;

		// Token: 0x0400259B RID: 9627
		private string message;

		// Token: 0x0400259C RID: 9628
		private string stackTrace;

		// Token: 0x0400259D RID: 9629
		private Exception exception;
	}
}
