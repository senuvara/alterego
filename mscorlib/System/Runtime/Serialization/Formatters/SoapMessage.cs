﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Serialization.Formatters
{
	/// <summary>Holds the names and types of parameters required during serialization of a SOAP RPC (Remote Procedure Call).</summary>
	// Token: 0x02000714 RID: 1812
	[ComVisible(true)]
	[Serializable]
	public class SoapMessage : ISoapMessage
	{
		/// <summary>Gets or sets the parameter names for the called method.</summary>
		/// <returns>The parameter names for the called method.</returns>
		// Token: 0x17000C59 RID: 3161
		// (get) Token: 0x0600493C RID: 18748 RVA: 0x000FD834 File Offset: 0x000FBA34
		// (set) Token: 0x0600493D RID: 18749 RVA: 0x000FD83C File Offset: 0x000FBA3C
		public string[] ParamNames
		{
			get
			{
				return this.paramNames;
			}
			set
			{
				this.paramNames = value;
			}
		}

		/// <summary>Gets or sets the parameter values for the called method.</summary>
		/// <returns>Parameter values for the called method.</returns>
		// Token: 0x17000C5A RID: 3162
		// (get) Token: 0x0600493E RID: 18750 RVA: 0x000FD845 File Offset: 0x000FBA45
		// (set) Token: 0x0600493F RID: 18751 RVA: 0x000FD84D File Offset: 0x000FBA4D
		public object[] ParamValues
		{
			get
			{
				return this.paramValues;
			}
			set
			{
				this.paramValues = value;
			}
		}

		/// <summary>This property is reserved. Use the <see cref="P:System.Runtime.Serialization.Formatters.SoapMessage.ParamNames" /> and/or <see cref="P:System.Runtime.Serialization.Formatters.SoapMessage.ParamValues" /> properties instead.</summary>
		/// <returns>Parameter types for the called method.</returns>
		// Token: 0x17000C5B RID: 3163
		// (get) Token: 0x06004940 RID: 18752 RVA: 0x000FD856 File Offset: 0x000FBA56
		// (set) Token: 0x06004941 RID: 18753 RVA: 0x000FD85E File Offset: 0x000FBA5E
		public Type[] ParamTypes
		{
			get
			{
				return this.paramTypes;
			}
			set
			{
				this.paramTypes = value;
			}
		}

		/// <summary>Gets or sets the name of the called method.</summary>
		/// <returns>The name of the called method.</returns>
		// Token: 0x17000C5C RID: 3164
		// (get) Token: 0x06004942 RID: 18754 RVA: 0x000FD867 File Offset: 0x000FBA67
		// (set) Token: 0x06004943 RID: 18755 RVA: 0x000FD86F File Offset: 0x000FBA6F
		public string MethodName
		{
			get
			{
				return this.methodName;
			}
			set
			{
				this.methodName = value;
			}
		}

		/// <summary>Gets or sets the XML namespace name where the object that contains the called method is located.</summary>
		/// <returns>The XML namespace name where the object that contains the called method is located.</returns>
		// Token: 0x17000C5D RID: 3165
		// (get) Token: 0x06004944 RID: 18756 RVA: 0x000FD878 File Offset: 0x000FBA78
		// (set) Token: 0x06004945 RID: 18757 RVA: 0x000FD880 File Offset: 0x000FBA80
		public string XmlNameSpace
		{
			get
			{
				return this.xmlNameSpace;
			}
			set
			{
				this.xmlNameSpace = value;
			}
		}

		/// <summary>Gets or sets the out-of-band data of the called method.</summary>
		/// <returns>The out-of-band data of the called method.</returns>
		// Token: 0x17000C5E RID: 3166
		// (get) Token: 0x06004946 RID: 18758 RVA: 0x000FD889 File Offset: 0x000FBA89
		// (set) Token: 0x06004947 RID: 18759 RVA: 0x000FD891 File Offset: 0x000FBA91
		public Header[] Headers
		{
			get
			{
				return this.headers;
			}
			set
			{
				this.headers = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.Formatters.SoapMessage" /> class. </summary>
		// Token: 0x06004948 RID: 18760 RVA: 0x00002050 File Offset: 0x00000250
		public SoapMessage()
		{
		}

		// Token: 0x0400259E RID: 9630
		internal string[] paramNames;

		// Token: 0x0400259F RID: 9631
		internal object[] paramValues;

		// Token: 0x040025A0 RID: 9632
		internal Type[] paramTypes;

		// Token: 0x040025A1 RID: 9633
		internal string methodName;

		// Token: 0x040025A2 RID: 9634
		internal string xmlNameSpace;

		// Token: 0x040025A3 RID: 9635
		internal Header[] headers;
	}
}
