﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Serialization.Formatters
{
	/// <summary>Provides an interface for an object that contains the names and types of parameters required during serialization of a SOAP RPC (Remote Procedure Call).</summary>
	// Token: 0x0200070E RID: 1806
	[ComVisible(true)]
	public interface ISoapMessage
	{
		/// <summary>Gets or sets the parameter names of the method call.</summary>
		/// <returns>The parameter names of the method call.</returns>
		// Token: 0x17000C4B RID: 3147
		// (get) Token: 0x0600490F RID: 18703
		// (set) Token: 0x06004910 RID: 18704
		string[] ParamNames { get; set; }

		/// <summary>Gets or sets the parameter values of a method call.</summary>
		/// <returns>The parameter values of a method call.</returns>
		// Token: 0x17000C4C RID: 3148
		// (get) Token: 0x06004911 RID: 18705
		// (set) Token: 0x06004912 RID: 18706
		object[] ParamValues { get; set; }

		/// <summary>Gets or sets the parameter types of a method call.</summary>
		/// <returns>The parameter types of a method call.</returns>
		// Token: 0x17000C4D RID: 3149
		// (get) Token: 0x06004913 RID: 18707
		// (set) Token: 0x06004914 RID: 18708
		Type[] ParamTypes { get; set; }

		/// <summary>Gets or sets the name of the called method.</summary>
		/// <returns>The name of the called method.</returns>
		// Token: 0x17000C4E RID: 3150
		// (get) Token: 0x06004915 RID: 18709
		// (set) Token: 0x06004916 RID: 18710
		string MethodName { get; set; }

		/// <summary>Gets or sets the XML namespace of the SOAP RPC (Remote Procedure Call) <see cref="P:System.Runtime.Serialization.Formatters.ISoapMessage.MethodName" /> element.</summary>
		/// <returns>The XML namespace name where the object that contains the called method is located.</returns>
		// Token: 0x17000C4F RID: 3151
		// (get) Token: 0x06004917 RID: 18711
		// (set) Token: 0x06004918 RID: 18712
		string XmlNameSpace { get; set; }

		/// <summary>Gets or sets the out-of-band data of the method call.</summary>
		/// <returns>The out-of-band data of the method call.</returns>
		// Token: 0x17000C50 RID: 3152
		// (get) Token: 0x06004919 RID: 18713
		// (set) Token: 0x0600491A RID: 18714
		Header[] Headers { get; set; }
	}
}
