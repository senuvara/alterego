﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000733 RID: 1843
	[Serializable]
	internal enum InternalParseTypeE
	{
		// Token: 0x04002666 RID: 9830
		Empty,
		// Token: 0x04002667 RID: 9831
		SerializedStreamHeader,
		// Token: 0x04002668 RID: 9832
		Object,
		// Token: 0x04002669 RID: 9833
		Member,
		// Token: 0x0400266A RID: 9834
		ObjectEnd,
		// Token: 0x0400266B RID: 9835
		MemberEnd,
		// Token: 0x0400266C RID: 9836
		Headers,
		// Token: 0x0400266D RID: 9837
		HeadersEnd,
		// Token: 0x0400266E RID: 9838
		SerializedStreamHeaderEnd,
		// Token: 0x0400266F RID: 9839
		Envelope,
		// Token: 0x04002670 RID: 9840
		EnvelopeEnd,
		// Token: 0x04002671 RID: 9841
		Body,
		// Token: 0x04002672 RID: 9842
		BodyEnd
	}
}
