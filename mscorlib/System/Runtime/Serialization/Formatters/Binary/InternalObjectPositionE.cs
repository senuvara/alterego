﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000735 RID: 1845
	[Serializable]
	internal enum InternalObjectPositionE
	{
		// Token: 0x04002678 RID: 9848
		Empty,
		// Token: 0x04002679 RID: 9849
		Top,
		// Token: 0x0400267A RID: 9850
		Child,
		// Token: 0x0400267B RID: 9851
		Headers
	}
}
