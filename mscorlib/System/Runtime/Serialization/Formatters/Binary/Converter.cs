﻿using System;
using System.Globalization;
using System.Reflection;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200072D RID: 1837
	internal sealed class Converter
	{
		// Token: 0x060049CB RID: 18891 RVA: 0x00002050 File Offset: 0x00000250
		private Converter()
		{
		}

		// Token: 0x060049CC RID: 18892 RVA: 0x000FF628 File Offset: 0x000FD828
		internal static InternalPrimitiveTypeE ToCode(Type type)
		{
			InternalPrimitiveTypeE result;
			if (type != null && !type.IsPrimitive)
			{
				if (type == Converter.typeofDateTime)
				{
					result = InternalPrimitiveTypeE.DateTime;
				}
				else if (type == Converter.typeofTimeSpan)
				{
					result = InternalPrimitiveTypeE.TimeSpan;
				}
				else if (type == Converter.typeofDecimal)
				{
					result = InternalPrimitiveTypeE.Decimal;
				}
				else
				{
					result = InternalPrimitiveTypeE.Invalid;
				}
			}
			else
			{
				result = Converter.ToPrimitiveTypeEnum(Type.GetTypeCode(type));
			}
			return result;
		}

		// Token: 0x060049CD RID: 18893 RVA: 0x000FF678 File Offset: 0x000FD878
		internal static bool IsWriteAsByteArray(InternalPrimitiveTypeE code)
		{
			bool result = false;
			switch (code)
			{
			case InternalPrimitiveTypeE.Boolean:
			case InternalPrimitiveTypeE.Byte:
			case InternalPrimitiveTypeE.Char:
			case InternalPrimitiveTypeE.Double:
			case InternalPrimitiveTypeE.Int16:
			case InternalPrimitiveTypeE.Int32:
			case InternalPrimitiveTypeE.Int64:
			case InternalPrimitiveTypeE.SByte:
			case InternalPrimitiveTypeE.Single:
			case InternalPrimitiveTypeE.UInt16:
			case InternalPrimitiveTypeE.UInt32:
			case InternalPrimitiveTypeE.UInt64:
				result = true;
				break;
			}
			return result;
		}

		// Token: 0x060049CE RID: 18894 RVA: 0x000FF6D4 File Offset: 0x000FD8D4
		internal static int TypeLength(InternalPrimitiveTypeE code)
		{
			int result = 0;
			switch (code)
			{
			case InternalPrimitiveTypeE.Boolean:
				result = 1;
				break;
			case InternalPrimitiveTypeE.Byte:
				result = 1;
				break;
			case InternalPrimitiveTypeE.Char:
				result = 2;
				break;
			case InternalPrimitiveTypeE.Double:
				result = 8;
				break;
			case InternalPrimitiveTypeE.Int16:
				result = 2;
				break;
			case InternalPrimitiveTypeE.Int32:
				result = 4;
				break;
			case InternalPrimitiveTypeE.Int64:
				result = 8;
				break;
			case InternalPrimitiveTypeE.SByte:
				result = 1;
				break;
			case InternalPrimitiveTypeE.Single:
				result = 4;
				break;
			case InternalPrimitiveTypeE.UInt16:
				result = 2;
				break;
			case InternalPrimitiveTypeE.UInt32:
				result = 4;
				break;
			case InternalPrimitiveTypeE.UInt64:
				result = 8;
				break;
			}
			return result;
		}

		// Token: 0x060049CF RID: 18895 RVA: 0x000FF75C File Offset: 0x000FD95C
		internal static InternalNameSpaceE GetNameSpaceEnum(InternalPrimitiveTypeE code, Type type, WriteObjectInfo objectInfo, out string typeName)
		{
			InternalNameSpaceE internalNameSpaceE = InternalNameSpaceE.None;
			typeName = null;
			if (code != InternalPrimitiveTypeE.Invalid)
			{
				switch (code)
				{
				case InternalPrimitiveTypeE.Boolean:
				case InternalPrimitiveTypeE.Byte:
				case InternalPrimitiveTypeE.Char:
				case InternalPrimitiveTypeE.Double:
				case InternalPrimitiveTypeE.Int16:
				case InternalPrimitiveTypeE.Int32:
				case InternalPrimitiveTypeE.Int64:
				case InternalPrimitiveTypeE.SByte:
				case InternalPrimitiveTypeE.Single:
				case InternalPrimitiveTypeE.TimeSpan:
				case InternalPrimitiveTypeE.DateTime:
				case InternalPrimitiveTypeE.UInt16:
				case InternalPrimitiveTypeE.UInt32:
				case InternalPrimitiveTypeE.UInt64:
					internalNameSpaceE = InternalNameSpaceE.XdrPrimitive;
					typeName = "System." + Converter.ToComType(code);
					break;
				case InternalPrimitiveTypeE.Decimal:
					internalNameSpaceE = InternalNameSpaceE.UrtSystem;
					typeName = "System." + Converter.ToComType(code);
					break;
				}
			}
			if (internalNameSpaceE == InternalNameSpaceE.None && type != null)
			{
				if (type == Converter.typeofString)
				{
					internalNameSpaceE = InternalNameSpaceE.XdrString;
				}
				else if (objectInfo == null)
				{
					typeName = type.FullName;
					if (type.Assembly == Converter.urtAssembly)
					{
						internalNameSpaceE = InternalNameSpaceE.UrtSystem;
					}
					else
					{
						internalNameSpaceE = InternalNameSpaceE.UrtUser;
					}
				}
				else
				{
					typeName = objectInfo.GetTypeFullName();
					if (objectInfo.GetAssemblyString().Equals(Converter.urtAssemblyString))
					{
						internalNameSpaceE = InternalNameSpaceE.UrtSystem;
					}
					else
					{
						internalNameSpaceE = InternalNameSpaceE.UrtUser;
					}
				}
			}
			return internalNameSpaceE;
		}

		// Token: 0x060049D0 RID: 18896 RVA: 0x000FF83D File Offset: 0x000FDA3D
		internal static Type ToArrayType(InternalPrimitiveTypeE code)
		{
			if (Converter.arrayTypeA == null)
			{
				Converter.InitArrayTypeA();
			}
			return Converter.arrayTypeA[(int)code];
		}

		// Token: 0x060049D1 RID: 18897 RVA: 0x000FF858 File Offset: 0x000FDA58
		private static void InitTypeA()
		{
			Type[] array = new Type[Converter.primitiveTypeEnumLength];
			array[0] = null;
			array[1] = Converter.typeofBoolean;
			array[2] = Converter.typeofByte;
			array[3] = Converter.typeofChar;
			array[5] = Converter.typeofDecimal;
			array[6] = Converter.typeofDouble;
			array[7] = Converter.typeofInt16;
			array[8] = Converter.typeofInt32;
			array[9] = Converter.typeofInt64;
			array[10] = Converter.typeofSByte;
			array[11] = Converter.typeofSingle;
			array[12] = Converter.typeofTimeSpan;
			array[13] = Converter.typeofDateTime;
			array[14] = Converter.typeofUInt16;
			array[15] = Converter.typeofUInt32;
			array[16] = Converter.typeofUInt64;
			Converter.typeA = array;
		}

		// Token: 0x060049D2 RID: 18898 RVA: 0x000FF8FC File Offset: 0x000FDAFC
		private static void InitArrayTypeA()
		{
			Type[] array = new Type[Converter.primitiveTypeEnumLength];
			array[0] = null;
			array[1] = Converter.typeofBooleanArray;
			array[2] = Converter.typeofByteArray;
			array[3] = Converter.typeofCharArray;
			array[5] = Converter.typeofDecimalArray;
			array[6] = Converter.typeofDoubleArray;
			array[7] = Converter.typeofInt16Array;
			array[8] = Converter.typeofInt32Array;
			array[9] = Converter.typeofInt64Array;
			array[10] = Converter.typeofSByteArray;
			array[11] = Converter.typeofSingleArray;
			array[12] = Converter.typeofTimeSpanArray;
			array[13] = Converter.typeofDateTimeArray;
			array[14] = Converter.typeofUInt16Array;
			array[15] = Converter.typeofUInt32Array;
			array[16] = Converter.typeofUInt64Array;
			Converter.arrayTypeA = array;
		}

		// Token: 0x060049D3 RID: 18899 RVA: 0x000FF99E File Offset: 0x000FDB9E
		internal static Type ToType(InternalPrimitiveTypeE code)
		{
			if (Converter.typeA == null)
			{
				Converter.InitTypeA();
			}
			return Converter.typeA[(int)code];
		}

		// Token: 0x060049D4 RID: 18900 RVA: 0x000FF9B8 File Offset: 0x000FDBB8
		internal static Array CreatePrimitiveArray(InternalPrimitiveTypeE code, int length)
		{
			Array result = null;
			switch (code)
			{
			case InternalPrimitiveTypeE.Boolean:
				result = new bool[length];
				break;
			case InternalPrimitiveTypeE.Byte:
				result = new byte[length];
				break;
			case InternalPrimitiveTypeE.Char:
				result = new char[length];
				break;
			case InternalPrimitiveTypeE.Decimal:
				result = new decimal[length];
				break;
			case InternalPrimitiveTypeE.Double:
				result = new double[length];
				break;
			case InternalPrimitiveTypeE.Int16:
				result = new short[length];
				break;
			case InternalPrimitiveTypeE.Int32:
				result = new int[length];
				break;
			case InternalPrimitiveTypeE.Int64:
				result = new long[length];
				break;
			case InternalPrimitiveTypeE.SByte:
				result = new sbyte[length];
				break;
			case InternalPrimitiveTypeE.Single:
				result = new float[length];
				break;
			case InternalPrimitiveTypeE.TimeSpan:
				result = new TimeSpan[length];
				break;
			case InternalPrimitiveTypeE.DateTime:
				result = new DateTime[length];
				break;
			case InternalPrimitiveTypeE.UInt16:
				result = new ushort[length];
				break;
			case InternalPrimitiveTypeE.UInt32:
				result = new uint[length];
				break;
			case InternalPrimitiveTypeE.UInt64:
				result = new ulong[length];
				break;
			}
			return result;
		}

		// Token: 0x060049D5 RID: 18901 RVA: 0x000FFA9C File Offset: 0x000FDC9C
		internal static bool IsPrimitiveArray(Type type, out object typeInformation)
		{
			typeInformation = null;
			bool result = true;
			if (type == Converter.typeofBooleanArray)
			{
				typeInformation = InternalPrimitiveTypeE.Boolean;
			}
			else if (type == Converter.typeofByteArray)
			{
				typeInformation = InternalPrimitiveTypeE.Byte;
			}
			else if (type == Converter.typeofCharArray)
			{
				typeInformation = InternalPrimitiveTypeE.Char;
			}
			else if (type == Converter.typeofDoubleArray)
			{
				typeInformation = InternalPrimitiveTypeE.Double;
			}
			else if (type == Converter.typeofInt16Array)
			{
				typeInformation = InternalPrimitiveTypeE.Int16;
			}
			else if (type == Converter.typeofInt32Array)
			{
				typeInformation = InternalPrimitiveTypeE.Int32;
			}
			else if (type == Converter.typeofInt64Array)
			{
				typeInformation = InternalPrimitiveTypeE.Int64;
			}
			else if (type == Converter.typeofSByteArray)
			{
				typeInformation = InternalPrimitiveTypeE.SByte;
			}
			else if (type == Converter.typeofSingleArray)
			{
				typeInformation = InternalPrimitiveTypeE.Single;
			}
			else if (type == Converter.typeofUInt16Array)
			{
				typeInformation = InternalPrimitiveTypeE.UInt16;
			}
			else if (type == Converter.typeofUInt32Array)
			{
				typeInformation = InternalPrimitiveTypeE.UInt32;
			}
			else if (type == Converter.typeofUInt64Array)
			{
				typeInformation = InternalPrimitiveTypeE.UInt64;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x060049D6 RID: 18902 RVA: 0x000FFBA0 File Offset: 0x000FDDA0
		private static void InitValueA()
		{
			string[] array = new string[Converter.primitiveTypeEnumLength];
			array[0] = null;
			array[1] = "Boolean";
			array[2] = "Byte";
			array[3] = "Char";
			array[5] = "Decimal";
			array[6] = "Double";
			array[7] = "Int16";
			array[8] = "Int32";
			array[9] = "Int64";
			array[10] = "SByte";
			array[11] = "Single";
			array[12] = "TimeSpan";
			array[13] = "DateTime";
			array[14] = "UInt16";
			array[15] = "UInt32";
			array[16] = "UInt64";
			Converter.valueA = array;
		}

		// Token: 0x060049D7 RID: 18903 RVA: 0x000FFC42 File Offset: 0x000FDE42
		internal static string ToComType(InternalPrimitiveTypeE code)
		{
			if (Converter.valueA == null)
			{
				Converter.InitValueA();
			}
			return Converter.valueA[(int)code];
		}

		// Token: 0x060049D8 RID: 18904 RVA: 0x000FFC5C File Offset: 0x000FDE5C
		private static void InitTypeCodeA()
		{
			TypeCode[] array = new TypeCode[Converter.primitiveTypeEnumLength];
			array[0] = TypeCode.Object;
			array[1] = TypeCode.Boolean;
			array[2] = TypeCode.Byte;
			array[3] = TypeCode.Char;
			array[5] = TypeCode.Decimal;
			array[6] = TypeCode.Double;
			array[7] = TypeCode.Int16;
			array[8] = TypeCode.Int32;
			array[9] = TypeCode.Int64;
			array[10] = TypeCode.SByte;
			array[11] = TypeCode.Single;
			array[12] = TypeCode.Object;
			array[13] = TypeCode.DateTime;
			array[14] = TypeCode.UInt16;
			array[15] = TypeCode.UInt32;
			array[16] = TypeCode.UInt64;
			Converter.typeCodeA = array;
		}

		// Token: 0x060049D9 RID: 18905 RVA: 0x000FFCCA File Offset: 0x000FDECA
		internal static TypeCode ToTypeCode(InternalPrimitiveTypeE code)
		{
			if (Converter.typeCodeA == null)
			{
				Converter.InitTypeCodeA();
			}
			return Converter.typeCodeA[(int)code];
		}

		// Token: 0x060049DA RID: 18906 RVA: 0x000FFCE4 File Offset: 0x000FDEE4
		private static void InitCodeA()
		{
			Converter.codeA = new InternalPrimitiveTypeE[]
			{
				InternalPrimitiveTypeE.Invalid,
				InternalPrimitiveTypeE.Invalid,
				InternalPrimitiveTypeE.Invalid,
				InternalPrimitiveTypeE.Boolean,
				InternalPrimitiveTypeE.Char,
				InternalPrimitiveTypeE.SByte,
				InternalPrimitiveTypeE.Byte,
				InternalPrimitiveTypeE.Int16,
				InternalPrimitiveTypeE.UInt16,
				InternalPrimitiveTypeE.Int32,
				InternalPrimitiveTypeE.UInt32,
				InternalPrimitiveTypeE.Int64,
				InternalPrimitiveTypeE.UInt64,
				InternalPrimitiveTypeE.Single,
				InternalPrimitiveTypeE.Double,
				InternalPrimitiveTypeE.Decimal,
				InternalPrimitiveTypeE.DateTime,
				InternalPrimitiveTypeE.Invalid,
				InternalPrimitiveTypeE.Invalid
			};
		}

		// Token: 0x060049DB RID: 18907 RVA: 0x000FFD5C File Offset: 0x000FDF5C
		internal static InternalPrimitiveTypeE ToPrimitiveTypeEnum(TypeCode typeCode)
		{
			if (Converter.codeA == null)
			{
				Converter.InitCodeA();
			}
			return Converter.codeA[(int)typeCode];
		}

		// Token: 0x060049DC RID: 18908 RVA: 0x000FFD78 File Offset: 0x000FDF78
		internal static object FromString(string value, InternalPrimitiveTypeE code)
		{
			object result;
			if (code != InternalPrimitiveTypeE.Invalid)
			{
				result = Convert.ChangeType(value, Converter.ToTypeCode(code), CultureInfo.InvariantCulture);
			}
			else
			{
				result = value;
			}
			return result;
		}

		// Token: 0x060049DD RID: 18909 RVA: 0x000FFDA0 File Offset: 0x000FDFA0
		// Note: this type is marked as 'beforefieldinit'.
		static Converter()
		{
		}

		// Token: 0x04002607 RID: 9735
		private static int primitiveTypeEnumLength = 17;

		// Token: 0x04002608 RID: 9736
		private static volatile Type[] typeA;

		// Token: 0x04002609 RID: 9737
		private static volatile Type[] arrayTypeA;

		// Token: 0x0400260A RID: 9738
		private static volatile string[] valueA;

		// Token: 0x0400260B RID: 9739
		private static volatile TypeCode[] typeCodeA;

		// Token: 0x0400260C RID: 9740
		private static volatile InternalPrimitiveTypeE[] codeA;

		// Token: 0x0400260D RID: 9741
		internal static Type typeofISerializable = typeof(ISerializable);

		// Token: 0x0400260E RID: 9742
		internal static Type typeofString = typeof(string);

		// Token: 0x0400260F RID: 9743
		internal static Type typeofConverter = typeof(Converter);

		// Token: 0x04002610 RID: 9744
		internal static Type typeofBoolean = typeof(bool);

		// Token: 0x04002611 RID: 9745
		internal static Type typeofByte = typeof(byte);

		// Token: 0x04002612 RID: 9746
		internal static Type typeofChar = typeof(char);

		// Token: 0x04002613 RID: 9747
		internal static Type typeofDecimal = typeof(decimal);

		// Token: 0x04002614 RID: 9748
		internal static Type typeofDouble = typeof(double);

		// Token: 0x04002615 RID: 9749
		internal static Type typeofInt16 = typeof(short);

		// Token: 0x04002616 RID: 9750
		internal static Type typeofInt32 = typeof(int);

		// Token: 0x04002617 RID: 9751
		internal static Type typeofInt64 = typeof(long);

		// Token: 0x04002618 RID: 9752
		internal static Type typeofSByte = typeof(sbyte);

		// Token: 0x04002619 RID: 9753
		internal static Type typeofSingle = typeof(float);

		// Token: 0x0400261A RID: 9754
		internal static Type typeofTimeSpan = typeof(TimeSpan);

		// Token: 0x0400261B RID: 9755
		internal static Type typeofDateTime = typeof(DateTime);

		// Token: 0x0400261C RID: 9756
		internal static Type typeofUInt16 = typeof(ushort);

		// Token: 0x0400261D RID: 9757
		internal static Type typeofUInt32 = typeof(uint);

		// Token: 0x0400261E RID: 9758
		internal static Type typeofUInt64 = typeof(ulong);

		// Token: 0x0400261F RID: 9759
		internal static Type typeofObject = typeof(object);

		// Token: 0x04002620 RID: 9760
		internal static Type typeofSystemVoid = typeof(void);

		// Token: 0x04002621 RID: 9761
		internal static Assembly urtAssembly = Assembly.GetAssembly(Converter.typeofString);

		// Token: 0x04002622 RID: 9762
		internal static string urtAssemblyString = Converter.urtAssembly.FullName;

		// Token: 0x04002623 RID: 9763
		internal static Type typeofTypeArray = typeof(Type[]);

		// Token: 0x04002624 RID: 9764
		internal static Type typeofObjectArray = typeof(object[]);

		// Token: 0x04002625 RID: 9765
		internal static Type typeofStringArray = typeof(string[]);

		// Token: 0x04002626 RID: 9766
		internal static Type typeofBooleanArray = typeof(bool[]);

		// Token: 0x04002627 RID: 9767
		internal static Type typeofByteArray = typeof(byte[]);

		// Token: 0x04002628 RID: 9768
		internal static Type typeofCharArray = typeof(char[]);

		// Token: 0x04002629 RID: 9769
		internal static Type typeofDecimalArray = typeof(decimal[]);

		// Token: 0x0400262A RID: 9770
		internal static Type typeofDoubleArray = typeof(double[]);

		// Token: 0x0400262B RID: 9771
		internal static Type typeofInt16Array = typeof(short[]);

		// Token: 0x0400262C RID: 9772
		internal static Type typeofInt32Array = typeof(int[]);

		// Token: 0x0400262D RID: 9773
		internal static Type typeofInt64Array = typeof(long[]);

		// Token: 0x0400262E RID: 9774
		internal static Type typeofSByteArray = typeof(sbyte[]);

		// Token: 0x0400262F RID: 9775
		internal static Type typeofSingleArray = typeof(float[]);

		// Token: 0x04002630 RID: 9776
		internal static Type typeofTimeSpanArray = typeof(TimeSpan[]);

		// Token: 0x04002631 RID: 9777
		internal static Type typeofDateTimeArray = typeof(DateTime[]);

		// Token: 0x04002632 RID: 9778
		internal static Type typeofUInt16Array = typeof(ushort[]);

		// Token: 0x04002633 RID: 9779
		internal static Type typeofUInt32Array = typeof(uint[]);

		// Token: 0x04002634 RID: 9780
		internal static Type typeofUInt64Array = typeof(ulong[]);

		// Token: 0x04002635 RID: 9781
		internal static Type typeofMarshalByRefObject = typeof(MarshalByRefObject);
	}
}
