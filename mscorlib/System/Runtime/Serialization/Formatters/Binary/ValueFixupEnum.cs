﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200073C RID: 1852
	[Serializable]
	internal enum ValueFixupEnum
	{
		// Token: 0x040026B7 RID: 9911
		Empty,
		// Token: 0x040026B8 RID: 9912
		Array,
		// Token: 0x040026B9 RID: 9913
		Header,
		// Token: 0x040026BA RID: 9914
		Member
	}
}
