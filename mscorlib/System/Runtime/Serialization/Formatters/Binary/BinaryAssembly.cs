﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200071B RID: 1819
	internal sealed class BinaryAssembly : IStreamable
	{
		// Token: 0x06004961 RID: 18785 RVA: 0x00002050 File Offset: 0x00000250
		internal BinaryAssembly()
		{
		}

		// Token: 0x06004962 RID: 18786 RVA: 0x000FDEB6 File Offset: 0x000FC0B6
		internal void Set(int assemId, string assemblyString)
		{
			this.assemId = assemId;
			this.assemblyString = assemblyString;
		}

		// Token: 0x06004963 RID: 18787 RVA: 0x000FDEC6 File Offset: 0x000FC0C6
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(12);
			sout.WriteInt32(this.assemId);
			sout.WriteString(this.assemblyString);
		}

		// Token: 0x06004964 RID: 18788 RVA: 0x000FDEE8 File Offset: 0x000FC0E8
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.assemId = input.ReadInt32();
			this.assemblyString = input.ReadString();
		}

		// Token: 0x06004965 RID: 18789 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x06004966 RID: 18790 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025AD RID: 9645
		internal int assemId;

		// Token: 0x040025AE RID: 9646
		internal string assemblyString;
	}
}
