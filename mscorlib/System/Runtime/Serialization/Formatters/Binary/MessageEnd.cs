﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200072A RID: 1834
	internal sealed class MessageEnd : IStreamable
	{
		// Token: 0x060049BA RID: 18874 RVA: 0x00002050 File Offset: 0x00000250
		internal MessageEnd()
		{
		}

		// Token: 0x060049BB RID: 18875 RVA: 0x000FF1B4 File Offset: 0x000FD3B4
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(11);
		}

		// Token: 0x060049BC RID: 18876 RVA: 0x000020D3 File Offset: 0x000002D3
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
		}

		// Token: 0x060049BD RID: 18877 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x060049BE RID: 18878 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump(Stream sout)
		{
		}

		// Token: 0x060049BF RID: 18879 RVA: 0x000FF1BE File Offset: 0x000FD3BE
		[Conditional("_LOGGING")]
		private void DumpInternal(Stream sout)
		{
			if (BCLDebug.CheckEnabled("BINARY") && sout != null && sout.CanSeek)
			{
				long length = sout.Length;
			}
		}
	}
}
