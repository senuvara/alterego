﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000737 RID: 1847
	[Serializable]
	internal enum InternalMemberTypeE
	{
		// Token: 0x04002683 RID: 9859
		Empty,
		// Token: 0x04002684 RID: 9860
		Header,
		// Token: 0x04002685 RID: 9861
		Field,
		// Token: 0x04002686 RID: 9862
		Item
	}
}
