﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200073A RID: 1850
	[Serializable]
	internal enum InternalPrimitiveTypeE
	{
		// Token: 0x04002693 RID: 9875
		Invalid,
		// Token: 0x04002694 RID: 9876
		Boolean,
		// Token: 0x04002695 RID: 9877
		Byte,
		// Token: 0x04002696 RID: 9878
		Char,
		// Token: 0x04002697 RID: 9879
		Currency,
		// Token: 0x04002698 RID: 9880
		Decimal,
		// Token: 0x04002699 RID: 9881
		Double,
		// Token: 0x0400269A RID: 9882
		Int16,
		// Token: 0x0400269B RID: 9883
		Int32,
		// Token: 0x0400269C RID: 9884
		Int64,
		// Token: 0x0400269D RID: 9885
		SByte,
		// Token: 0x0400269E RID: 9886
		Single,
		// Token: 0x0400269F RID: 9887
		TimeSpan,
		// Token: 0x040026A0 RID: 9888
		DateTime,
		// Token: 0x040026A1 RID: 9889
		UInt16,
		// Token: 0x040026A2 RID: 9890
		UInt32,
		// Token: 0x040026A3 RID: 9891
		UInt64,
		// Token: 0x040026A4 RID: 9892
		Null,
		// Token: 0x040026A5 RID: 9893
		String
	}
}
