﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000746 RID: 1862
	internal sealed class TypeInformation
	{
		// Token: 0x17000C65 RID: 3173
		// (get) Token: 0x06004A59 RID: 19033 RVA: 0x00101F72 File Offset: 0x00100172
		internal string FullTypeName
		{
			get
			{
				return this.fullTypeName;
			}
		}

		// Token: 0x17000C66 RID: 3174
		// (get) Token: 0x06004A5A RID: 19034 RVA: 0x00101F7A File Offset: 0x0010017A
		internal string AssemblyString
		{
			get
			{
				return this.assemblyString;
			}
		}

		// Token: 0x17000C67 RID: 3175
		// (get) Token: 0x06004A5B RID: 19035 RVA: 0x00101F82 File Offset: 0x00100182
		internal bool HasTypeForwardedFrom
		{
			get
			{
				return this.hasTypeForwardedFrom;
			}
		}

		// Token: 0x06004A5C RID: 19036 RVA: 0x00101F8A File Offset: 0x0010018A
		internal TypeInformation(string fullTypeName, string assemblyString, bool hasTypeForwardedFrom)
		{
			this.fullTypeName = fullTypeName;
			this.assemblyString = assemblyString;
			this.hasTypeForwardedFrom = hasTypeForwardedFrom;
		}

		// Token: 0x0400271A RID: 10010
		private string fullTypeName;

		// Token: 0x0400271B RID: 10011
		private string assemblyString;

		// Token: 0x0400271C RID: 10012
		private bool hasTypeForwardedFrom;
	}
}
