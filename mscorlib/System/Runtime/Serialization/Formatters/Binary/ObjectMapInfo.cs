﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000741 RID: 1857
	internal sealed class ObjectMapInfo
	{
		// Token: 0x06004A2A RID: 18986 RVA: 0x00100F01 File Offset: 0x000FF101
		internal ObjectMapInfo(int objectId, int numMembers, string[] memberNames, Type[] memberTypes)
		{
			this.objectId = objectId;
			this.numMembers = numMembers;
			this.memberNames = memberNames;
			this.memberTypes = memberTypes;
		}

		// Token: 0x06004A2B RID: 18987 RVA: 0x00100F28 File Offset: 0x000FF128
		internal bool isCompatible(int numMembers, string[] memberNames, Type[] memberTypes)
		{
			bool result = true;
			if (this.numMembers == numMembers)
			{
				for (int i = 0; i < numMembers; i++)
				{
					if (!this.memberNames[i].Equals(memberNames[i]))
					{
						result = false;
						break;
					}
					if (memberTypes != null && this.memberTypes[i] != memberTypes[i])
					{
						result = false;
						break;
					}
				}
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x040026E9 RID: 9961
		internal int objectId;

		// Token: 0x040026EA RID: 9962
		private int numMembers;

		// Token: 0x040026EB RID: 9963
		private string[] memberNames;

		// Token: 0x040026EC RID: 9964
		private Type[] memberTypes;
	}
}
