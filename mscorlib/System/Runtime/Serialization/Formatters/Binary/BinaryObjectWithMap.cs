﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000724 RID: 1828
	internal sealed class BinaryObjectWithMap : IStreamable
	{
		// Token: 0x06004995 RID: 18837 RVA: 0x00002050 File Offset: 0x00000250
		internal BinaryObjectWithMap()
		{
		}

		// Token: 0x06004996 RID: 18838 RVA: 0x000FE889 File Offset: 0x000FCA89
		internal BinaryObjectWithMap(BinaryHeaderEnum binaryHeaderEnum)
		{
			this.binaryHeaderEnum = binaryHeaderEnum;
		}

		// Token: 0x06004997 RID: 18839 RVA: 0x000FE898 File Offset: 0x000FCA98
		internal void Set(int objectId, string name, int numMembers, string[] memberNames, int assemId)
		{
			this.objectId = objectId;
			this.name = name;
			this.numMembers = numMembers;
			this.memberNames = memberNames;
			this.assemId = assemId;
			if (assemId > 0)
			{
				this.binaryHeaderEnum = BinaryHeaderEnum.ObjectWithMapAssemId;
				return;
			}
			this.binaryHeaderEnum = BinaryHeaderEnum.ObjectWithMap;
		}

		// Token: 0x06004998 RID: 18840 RVA: 0x000FE8D4 File Offset: 0x000FCAD4
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte((byte)this.binaryHeaderEnum);
			sout.WriteInt32(this.objectId);
			sout.WriteString(this.name);
			sout.WriteInt32(this.numMembers);
			for (int i = 0; i < this.numMembers; i++)
			{
				sout.WriteString(this.memberNames[i]);
			}
			if (this.assemId > 0)
			{
				sout.WriteInt32(this.assemId);
			}
		}

		// Token: 0x06004999 RID: 18841 RVA: 0x000FE948 File Offset: 0x000FCB48
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.objectId = input.ReadInt32();
			this.name = input.ReadString();
			this.numMembers = input.ReadInt32();
			this.memberNames = new string[this.numMembers];
			for (int i = 0; i < this.numMembers; i++)
			{
				this.memberNames[i] = input.ReadString();
			}
			if (this.binaryHeaderEnum == BinaryHeaderEnum.ObjectWithMapAssemId)
			{
				this.assemId = input.ReadInt32();
			}
		}

		// Token: 0x0600499A RID: 18842 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x0600499B RID: 18843 RVA: 0x000FE9C0 File Offset: 0x000FCBC0
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			if (BCLDebug.CheckEnabled("BINARY"))
			{
				for (int i = 0; i < this.numMembers; i++)
				{
				}
				BinaryHeaderEnum binaryHeaderEnum = this.binaryHeaderEnum;
			}
		}

		// Token: 0x040025CB RID: 9675
		internal BinaryHeaderEnum binaryHeaderEnum;

		// Token: 0x040025CC RID: 9676
		internal int objectId;

		// Token: 0x040025CD RID: 9677
		internal string name;

		// Token: 0x040025CE RID: 9678
		internal int numMembers;

		// Token: 0x040025CF RID: 9679
		internal string[] memberNames;

		// Token: 0x040025D0 RID: 9680
		internal int assemId;
	}
}
