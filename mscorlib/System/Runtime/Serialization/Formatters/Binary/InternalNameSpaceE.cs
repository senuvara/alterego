﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200073D RID: 1853
	[Serializable]
	internal enum InternalNameSpaceE
	{
		// Token: 0x040026BC RID: 9916
		None,
		// Token: 0x040026BD RID: 9917
		Soap,
		// Token: 0x040026BE RID: 9918
		XdrPrimitive,
		// Token: 0x040026BF RID: 9919
		XdrString,
		// Token: 0x040026C0 RID: 9920
		UrtSystem,
		// Token: 0x040026C1 RID: 9921
		UrtUser,
		// Token: 0x040026C2 RID: 9922
		UserNameSpace,
		// Token: 0x040026C3 RID: 9923
		MemberName,
		// Token: 0x040026C4 RID: 9924
		Interop,
		// Token: 0x040026C5 RID: 9925
		CallElement
	}
}
