﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000728 RID: 1832
	internal sealed class MemberReference : IStreamable
	{
		// Token: 0x060049AD RID: 18861 RVA: 0x00002050 File Offset: 0x00000250
		internal MemberReference()
		{
		}

		// Token: 0x060049AE RID: 18862 RVA: 0x000FF0AA File Offset: 0x000FD2AA
		internal void Set(int idRef)
		{
			this.idRef = idRef;
		}

		// Token: 0x060049AF RID: 18863 RVA: 0x000FF0B3 File Offset: 0x000FD2B3
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(9);
			sout.WriteInt32(this.idRef);
		}

		// Token: 0x060049B0 RID: 18864 RVA: 0x000FF0C9 File Offset: 0x000FD2C9
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.idRef = input.ReadInt32();
		}

		// Token: 0x060049B1 RID: 18865 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x060049B2 RID: 18866 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025E5 RID: 9701
		internal int idRef;
	}
}
