﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200074C RID: 1868
	internal sealed class ParseRecord
	{
		// Token: 0x06004AD2 RID: 19154 RVA: 0x00002050 File Offset: 0x00000250
		internal ParseRecord()
		{
		}

		// Token: 0x06004AD3 RID: 19155 RVA: 0x001065A8 File Offset: 0x001047A8
		internal void Init()
		{
			this.PRparseTypeEnum = InternalParseTypeE.Empty;
			this.PRobjectTypeEnum = InternalObjectTypeE.Empty;
			this.PRarrayTypeEnum = InternalArrayTypeE.Empty;
			this.PRmemberTypeEnum = InternalMemberTypeE.Empty;
			this.PRmemberValueEnum = InternalMemberValueE.Empty;
			this.PRobjectPositionEnum = InternalObjectPositionE.Empty;
			this.PRname = null;
			this.PRvalue = null;
			this.PRkeyDt = null;
			this.PRdtType = null;
			this.PRdtTypeCode = InternalPrimitiveTypeE.Invalid;
			this.PRisEnum = false;
			this.PRobjectId = 0L;
			this.PRidRef = 0L;
			this.PRarrayElementTypeString = null;
			this.PRarrayElementType = null;
			this.PRisArrayVariant = false;
			this.PRarrayElementTypeCode = InternalPrimitiveTypeE.Invalid;
			this.PRrank = 0;
			this.PRlengthA = null;
			this.PRpositionA = null;
			this.PRlowerBoundA = null;
			this.PRupperBoundA = null;
			this.PRindexMap = null;
			this.PRmemberIndex = 0;
			this.PRlinearlength = 0;
			this.PRrectangularMap = null;
			this.PRisLowerBound = false;
			this.PRtopId = 0L;
			this.PRheaderId = 0L;
			this.PRisValueTypeFixup = false;
			this.PRnewObj = null;
			this.PRobjectA = null;
			this.PRprimitiveArray = null;
			this.PRobjectInfo = null;
			this.PRisRegistered = false;
			this.PRmemberData = null;
			this.PRsi = null;
			this.PRnullCount = 0;
		}

		// Token: 0x06004AD4 RID: 19156 RVA: 0x001066CA File Offset: 0x001048CA
		// Note: this type is marked as 'beforefieldinit'.
		static ParseRecord()
		{
		}

		// Token: 0x04002769 RID: 10089
		internal static int parseRecordIdCount = 1;

		// Token: 0x0400276A RID: 10090
		internal int PRparseRecordId;

		// Token: 0x0400276B RID: 10091
		internal InternalParseTypeE PRparseTypeEnum;

		// Token: 0x0400276C RID: 10092
		internal InternalObjectTypeE PRobjectTypeEnum;

		// Token: 0x0400276D RID: 10093
		internal InternalArrayTypeE PRarrayTypeEnum;

		// Token: 0x0400276E RID: 10094
		internal InternalMemberTypeE PRmemberTypeEnum;

		// Token: 0x0400276F RID: 10095
		internal InternalMemberValueE PRmemberValueEnum;

		// Token: 0x04002770 RID: 10096
		internal InternalObjectPositionE PRobjectPositionEnum;

		// Token: 0x04002771 RID: 10097
		internal string PRname;

		// Token: 0x04002772 RID: 10098
		internal string PRvalue;

		// Token: 0x04002773 RID: 10099
		internal object PRvarValue;

		// Token: 0x04002774 RID: 10100
		internal string PRkeyDt;

		// Token: 0x04002775 RID: 10101
		internal Type PRdtType;

		// Token: 0x04002776 RID: 10102
		internal InternalPrimitiveTypeE PRdtTypeCode;

		// Token: 0x04002777 RID: 10103
		internal bool PRisVariant;

		// Token: 0x04002778 RID: 10104
		internal bool PRisEnum;

		// Token: 0x04002779 RID: 10105
		internal long PRobjectId;

		// Token: 0x0400277A RID: 10106
		internal long PRidRef;

		// Token: 0x0400277B RID: 10107
		internal string PRarrayElementTypeString;

		// Token: 0x0400277C RID: 10108
		internal Type PRarrayElementType;

		// Token: 0x0400277D RID: 10109
		internal bool PRisArrayVariant;

		// Token: 0x0400277E RID: 10110
		internal InternalPrimitiveTypeE PRarrayElementTypeCode;

		// Token: 0x0400277F RID: 10111
		internal int PRrank;

		// Token: 0x04002780 RID: 10112
		internal int[] PRlengthA;

		// Token: 0x04002781 RID: 10113
		internal int[] PRpositionA;

		// Token: 0x04002782 RID: 10114
		internal int[] PRlowerBoundA;

		// Token: 0x04002783 RID: 10115
		internal int[] PRupperBoundA;

		// Token: 0x04002784 RID: 10116
		internal int[] PRindexMap;

		// Token: 0x04002785 RID: 10117
		internal int PRmemberIndex;

		// Token: 0x04002786 RID: 10118
		internal int PRlinearlength;

		// Token: 0x04002787 RID: 10119
		internal int[] PRrectangularMap;

		// Token: 0x04002788 RID: 10120
		internal bool PRisLowerBound;

		// Token: 0x04002789 RID: 10121
		internal long PRtopId;

		// Token: 0x0400278A RID: 10122
		internal long PRheaderId;

		// Token: 0x0400278B RID: 10123
		internal ReadObjectInfo PRobjectInfo;

		// Token: 0x0400278C RID: 10124
		internal bool PRisValueTypeFixup;

		// Token: 0x0400278D RID: 10125
		internal object PRnewObj;

		// Token: 0x0400278E RID: 10126
		internal object[] PRobjectA;

		// Token: 0x0400278F RID: 10127
		internal PrimitiveArray PRprimitiveArray;

		// Token: 0x04002790 RID: 10128
		internal bool PRisRegistered;

		// Token: 0x04002791 RID: 10129
		internal object[] PRmemberData;

		// Token: 0x04002792 RID: 10130
		internal SerializationInfo PRsi;

		// Token: 0x04002793 RID: 10131
		internal int PRnullCount;
	}
}
