﻿using System;
using System.Diagnostics;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200072C RID: 1836
	internal sealed class ObjectProgress
	{
		// Token: 0x060049C5 RID: 18885 RVA: 0x000FF42C File Offset: 0x000FD62C
		internal ObjectProgress()
		{
		}

		// Token: 0x060049C6 RID: 18886 RVA: 0x000FF448 File Offset: 0x000FD648
		[Conditional("SER_LOGGING")]
		private void Counter()
		{
			lock (this)
			{
				this.opRecordId = ObjectProgress.opRecordIdCount++;
				if (ObjectProgress.opRecordIdCount > 1000)
				{
					ObjectProgress.opRecordIdCount = 1;
				}
			}
		}

		// Token: 0x060049C7 RID: 18887 RVA: 0x000FF4A4 File Offset: 0x000FD6A4
		internal void Init()
		{
			this.isInitial = false;
			this.count = 0;
			this.expectedType = BinaryTypeEnum.ObjectUrt;
			this.expectedTypeInformation = null;
			this.name = null;
			this.objectTypeEnum = InternalObjectTypeE.Empty;
			this.memberTypeEnum = InternalMemberTypeE.Empty;
			this.memberValueEnum = InternalMemberValueE.Empty;
			this.dtType = null;
			this.numItems = 0;
			this.nullCount = 0;
			this.typeInformation = null;
			this.memberLength = 0;
			this.binaryTypeEnumA = null;
			this.typeInformationA = null;
			this.memberNames = null;
			this.memberTypes = null;
			this.pr.Init();
		}

		// Token: 0x060049C8 RID: 18888 RVA: 0x000FF533 File Offset: 0x000FD733
		internal void ArrayCountIncrement(int value)
		{
			this.count += value;
		}

		// Token: 0x060049C9 RID: 18889 RVA: 0x000FF544 File Offset: 0x000FD744
		internal bool GetNext(out BinaryTypeEnum outBinaryTypeEnum, out object outTypeInformation)
		{
			outBinaryTypeEnum = BinaryTypeEnum.Primitive;
			outTypeInformation = null;
			if (this.objectTypeEnum == InternalObjectTypeE.Array)
			{
				if (this.count == this.numItems)
				{
					return false;
				}
				outBinaryTypeEnum = this.binaryTypeEnum;
				outTypeInformation = this.typeInformation;
				if (this.count == 0)
				{
					this.isInitial = false;
				}
				this.count++;
				return true;
			}
			else
			{
				if (this.count == this.memberLength && !this.isInitial)
				{
					return false;
				}
				outBinaryTypeEnum = this.binaryTypeEnumA[this.count];
				outTypeInformation = this.typeInformationA[this.count];
				if (this.count == 0)
				{
					this.isInitial = false;
				}
				this.name = this.memberNames[this.count];
				Type[] array = this.memberTypes;
				this.dtType = this.memberTypes[this.count];
				this.count++;
				return true;
			}
		}

		// Token: 0x060049CA RID: 18890 RVA: 0x000FF620 File Offset: 0x000FD820
		// Note: this type is marked as 'beforefieldinit'.
		static ObjectProgress()
		{
		}

		// Token: 0x040025F2 RID: 9714
		internal static int opRecordIdCount = 1;

		// Token: 0x040025F3 RID: 9715
		internal int opRecordId;

		// Token: 0x040025F4 RID: 9716
		internal bool isInitial;

		// Token: 0x040025F5 RID: 9717
		internal int count;

		// Token: 0x040025F6 RID: 9718
		internal BinaryTypeEnum expectedType = BinaryTypeEnum.ObjectUrt;

		// Token: 0x040025F7 RID: 9719
		internal object expectedTypeInformation;

		// Token: 0x040025F8 RID: 9720
		internal string name;

		// Token: 0x040025F9 RID: 9721
		internal InternalObjectTypeE objectTypeEnum;

		// Token: 0x040025FA RID: 9722
		internal InternalMemberTypeE memberTypeEnum;

		// Token: 0x040025FB RID: 9723
		internal InternalMemberValueE memberValueEnum;

		// Token: 0x040025FC RID: 9724
		internal Type dtType;

		// Token: 0x040025FD RID: 9725
		internal int numItems;

		// Token: 0x040025FE RID: 9726
		internal BinaryTypeEnum binaryTypeEnum;

		// Token: 0x040025FF RID: 9727
		internal object typeInformation;

		// Token: 0x04002600 RID: 9728
		internal int nullCount;

		// Token: 0x04002601 RID: 9729
		internal int memberLength;

		// Token: 0x04002602 RID: 9730
		internal BinaryTypeEnum[] binaryTypeEnumA;

		// Token: 0x04002603 RID: 9731
		internal object[] typeInformationA;

		// Token: 0x04002604 RID: 9732
		internal string[] memberNames;

		// Token: 0x04002605 RID: 9733
		internal Type[] memberTypes;

		// Token: 0x04002606 RID: 9734
		internal ParseRecord pr = new ParseRecord();
	}
}
