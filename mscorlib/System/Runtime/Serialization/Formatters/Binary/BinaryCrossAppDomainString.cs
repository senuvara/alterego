﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000721 RID: 1825
	internal sealed class BinaryCrossAppDomainString : IStreamable
	{
		// Token: 0x06004985 RID: 18821 RVA: 0x00002050 File Offset: 0x00000250
		internal BinaryCrossAppDomainString()
		{
		}

		// Token: 0x06004986 RID: 18822 RVA: 0x000FE7D1 File Offset: 0x000FC9D1
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(19);
			sout.WriteInt32(this.objectId);
			sout.WriteInt32(this.value);
		}

		// Token: 0x06004987 RID: 18823 RVA: 0x000FE7F3 File Offset: 0x000FC9F3
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.objectId = input.ReadInt32();
			this.value = input.ReadInt32();
		}

		// Token: 0x06004988 RID: 18824 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x06004989 RID: 18825 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025C6 RID: 9670
		internal int objectId;

		// Token: 0x040025C7 RID: 9671
		internal int value;
	}
}
