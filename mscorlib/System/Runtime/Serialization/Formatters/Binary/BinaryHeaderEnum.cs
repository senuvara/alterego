﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200072E RID: 1838
	[Serializable]
	internal enum BinaryHeaderEnum
	{
		// Token: 0x04002637 RID: 9783
		SerializedStreamHeader,
		// Token: 0x04002638 RID: 9784
		Object,
		// Token: 0x04002639 RID: 9785
		ObjectWithMap,
		// Token: 0x0400263A RID: 9786
		ObjectWithMapAssemId,
		// Token: 0x0400263B RID: 9787
		ObjectWithMapTyped,
		// Token: 0x0400263C RID: 9788
		ObjectWithMapTypedAssemId,
		// Token: 0x0400263D RID: 9789
		ObjectString,
		// Token: 0x0400263E RID: 9790
		Array,
		// Token: 0x0400263F RID: 9791
		MemberPrimitiveTyped,
		// Token: 0x04002640 RID: 9792
		MemberReference,
		// Token: 0x04002641 RID: 9793
		ObjectNull,
		// Token: 0x04002642 RID: 9794
		MessageEnd,
		// Token: 0x04002643 RID: 9795
		Assembly,
		// Token: 0x04002644 RID: 9796
		ObjectNullMultiple256,
		// Token: 0x04002645 RID: 9797
		ObjectNullMultiple,
		// Token: 0x04002646 RID: 9798
		ArraySinglePrimitive,
		// Token: 0x04002647 RID: 9799
		ArraySingleObject,
		// Token: 0x04002648 RID: 9800
		ArraySingleString,
		// Token: 0x04002649 RID: 9801
		CrossAppDomainMap,
		// Token: 0x0400264A RID: 9802
		CrossAppDomainString,
		// Token: 0x0400264B RID: 9803
		CrossAppDomainAssembly,
		// Token: 0x0400264C RID: 9804
		MethodCall,
		// Token: 0x0400264D RID: 9805
		MethodReturn
	}
}
