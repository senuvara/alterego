﻿using System;
using System.Collections;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000744 RID: 1860
	internal sealed class SerObjectInfoInit
	{
		// Token: 0x06004A56 RID: 19030 RVA: 0x00101EEA File Offset: 0x001000EA
		public SerObjectInfoInit()
		{
		}

		// Token: 0x04002711 RID: 10001
		internal Hashtable seenBeforeTable = new Hashtable();

		// Token: 0x04002712 RID: 10002
		internal int objectInfoIdCount = 1;

		// Token: 0x04002713 RID: 10003
		internal SerStack oiPool = new SerStack("SerObjectInfo Pool");
	}
}
