﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000732 RID: 1842
	[Serializable]
	internal enum InternalElementTypeE
	{
		// Token: 0x04002662 RID: 9826
		ObjectBegin,
		// Token: 0x04002663 RID: 9827
		ObjectEnd,
		// Token: 0x04002664 RID: 9828
		Member
	}
}
