﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000734 RID: 1844
	[Serializable]
	internal enum InternalObjectTypeE
	{
		// Token: 0x04002674 RID: 9844
		Empty,
		// Token: 0x04002675 RID: 9845
		Object,
		// Token: 0x04002676 RID: 9846
		Array
	}
}
