﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200071A RID: 1818
	internal sealed class SerializationHeaderRecord : IStreamable
	{
		// Token: 0x0600495A RID: 18778 RVA: 0x000FDD7F File Offset: 0x000FBF7F
		internal SerializationHeaderRecord()
		{
		}

		// Token: 0x0600495B RID: 18779 RVA: 0x000FDD8E File Offset: 0x000FBF8E
		internal SerializationHeaderRecord(BinaryHeaderEnum binaryHeaderEnum, int topId, int headerId, int majorVersion, int minorVersion)
		{
			this.binaryHeaderEnum = binaryHeaderEnum;
			this.topId = topId;
			this.headerId = headerId;
			this.majorVersion = majorVersion;
			this.minorVersion = minorVersion;
		}

		// Token: 0x0600495C RID: 18780 RVA: 0x000FDDC4 File Offset: 0x000FBFC4
		public void Write(__BinaryWriter sout)
		{
			this.majorVersion = this.binaryFormatterMajorVersion;
			this.minorVersion = this.binaryFormatterMinorVersion;
			sout.WriteByte((byte)this.binaryHeaderEnum);
			sout.WriteInt32(this.topId);
			sout.WriteInt32(this.headerId);
			sout.WriteInt32(this.binaryFormatterMajorVersion);
			sout.WriteInt32(this.binaryFormatterMinorVersion);
		}

		// Token: 0x0600495D RID: 18781 RVA: 0x000DCCC8 File Offset: 0x000DAEC8
		private static int GetInt32(byte[] buffer, int index)
		{
			return (int)buffer[index] | (int)buffer[index + 1] << 8 | (int)buffer[index + 2] << 16 | (int)buffer[index + 3] << 24;
		}

		// Token: 0x0600495E RID: 18782 RVA: 0x000FDE28 File Offset: 0x000FC028
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			byte[] array = input.ReadBytes(17);
			if (array.Length < 17)
			{
				__Error.EndOfFile();
			}
			this.majorVersion = SerializationHeaderRecord.GetInt32(array, 9);
			if (this.majorVersion > this.binaryFormatterMajorVersion)
			{
				throw new SerializationException(Environment.GetResourceString("The input stream is not a valid binary format. The starting contents (in bytes) are: {0} ...", new object[]
				{
					BitConverter.ToString(array)
				}));
			}
			this.binaryHeaderEnum = (BinaryHeaderEnum)array[0];
			this.topId = SerializationHeaderRecord.GetInt32(array, 1);
			this.headerId = SerializationHeaderRecord.GetInt32(array, 5);
			this.minorVersion = SerializationHeaderRecord.GetInt32(array, 13);
		}

		// Token: 0x0600495F RID: 18783 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x06004960 RID: 18784 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025A6 RID: 9638
		internal int binaryFormatterMajorVersion = 1;

		// Token: 0x040025A7 RID: 9639
		internal int binaryFormatterMinorVersion;

		// Token: 0x040025A8 RID: 9640
		internal BinaryHeaderEnum binaryHeaderEnum;

		// Token: 0x040025A9 RID: 9641
		internal int topId;

		// Token: 0x040025AA RID: 9642
		internal int headerId;

		// Token: 0x040025AB RID: 9643
		internal int majorVersion;

		// Token: 0x040025AC RID: 9644
		internal int minorVersion;
	}
}
