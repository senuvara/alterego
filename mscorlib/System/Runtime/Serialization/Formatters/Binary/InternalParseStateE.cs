﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000739 RID: 1849
	[Serializable]
	internal enum InternalParseStateE
	{
		// Token: 0x0400268E RID: 9870
		Initial,
		// Token: 0x0400268F RID: 9871
		Object,
		// Token: 0x04002690 RID: 9872
		Member,
		// Token: 0x04002691 RID: 9873
		MemberChild
	}
}
