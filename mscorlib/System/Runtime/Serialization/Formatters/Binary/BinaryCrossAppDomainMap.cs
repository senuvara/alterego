﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000722 RID: 1826
	internal sealed class BinaryCrossAppDomainMap : IStreamable
	{
		// Token: 0x0600498A RID: 18826 RVA: 0x00002050 File Offset: 0x00000250
		internal BinaryCrossAppDomainMap()
		{
		}

		// Token: 0x0600498B RID: 18827 RVA: 0x000FE80D File Offset: 0x000FCA0D
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(18);
			sout.WriteInt32(this.crossAppDomainArrayIndex);
		}

		// Token: 0x0600498C RID: 18828 RVA: 0x000FE823 File Offset: 0x000FCA23
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.crossAppDomainArrayIndex = input.ReadInt32();
		}

		// Token: 0x0600498D RID: 18829 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x0600498E RID: 18830 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025C8 RID: 9672
		internal int crossAppDomainArrayIndex;
	}
}
