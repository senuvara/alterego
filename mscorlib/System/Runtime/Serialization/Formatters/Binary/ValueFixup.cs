﻿using System;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000751 RID: 1873
	internal sealed class ValueFixup
	{
		// Token: 0x06004AF0 RID: 19184 RVA: 0x00106C31 File Offset: 0x00104E31
		internal ValueFixup(Array arrayObj, int[] indexMap)
		{
			this.valueFixupEnum = ValueFixupEnum.Array;
			this.arrayObj = arrayObj;
			this.indexMap = indexMap;
		}

		// Token: 0x06004AF1 RID: 19185 RVA: 0x00106C4E File Offset: 0x00104E4E
		internal ValueFixup(object memberObject, string memberName, ReadObjectInfo objectInfo)
		{
			this.valueFixupEnum = ValueFixupEnum.Member;
			this.memberObject = memberObject;
			this.memberName = memberName;
			this.objectInfo = objectInfo;
		}

		// Token: 0x06004AF2 RID: 19186 RVA: 0x00106C74 File Offset: 0x00104E74
		[SecurityCritical]
		internal void Fixup(ParseRecord record, ParseRecord parent)
		{
			object prnewObj = record.PRnewObj;
			switch (this.valueFixupEnum)
			{
			case ValueFixupEnum.Array:
				this.arrayObj.SetValue(prnewObj, this.indexMap);
				return;
			case ValueFixupEnum.Header:
			{
				Type typeFromHandle = typeof(Header);
				if (ValueFixup.valueInfo == null)
				{
					MemberInfo[] member = typeFromHandle.GetMember("Value");
					if (member.Length != 1)
					{
						throw new SerializationException(Environment.GetResourceString("Header reflection error: number of value members: {0}.", new object[]
						{
							member.Length
						}));
					}
					ValueFixup.valueInfo = member[0];
				}
				FormatterServices.SerializationSetValue(ValueFixup.valueInfo, this.header, prnewObj);
				return;
			}
			case ValueFixupEnum.Member:
			{
				if (this.objectInfo.isSi)
				{
					this.objectInfo.objectManager.RecordDelayedFixup(parent.PRobjectId, this.memberName, record.PRobjectId);
					return;
				}
				MemberInfo memberInfo = this.objectInfo.GetMemberInfo(this.memberName);
				if (memberInfo != null)
				{
					this.objectInfo.objectManager.RecordFixup(parent.PRobjectId, memberInfo, record.PRobjectId);
				}
				return;
			}
			default:
				return;
			}
		}

		// Token: 0x0400279E RID: 10142
		internal ValueFixupEnum valueFixupEnum;

		// Token: 0x0400279F RID: 10143
		internal Array arrayObj;

		// Token: 0x040027A0 RID: 10144
		internal int[] indexMap;

		// Token: 0x040027A1 RID: 10145
		internal object header;

		// Token: 0x040027A2 RID: 10146
		internal object memberObject;

		// Token: 0x040027A3 RID: 10147
		internal static volatile MemberInfo valueInfo;

		// Token: 0x040027A4 RID: 10148
		internal ReadObjectInfo objectInfo;

		// Token: 0x040027A5 RID: 10149
		internal string memberName;
	}
}
