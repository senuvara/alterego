﻿using System;
using System.Reflection;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000745 RID: 1861
	internal sealed class SerObjectInfoCache
	{
		// Token: 0x06004A57 RID: 19031 RVA: 0x00101F14 File Offset: 0x00100114
		internal SerObjectInfoCache(string typeName, string assemblyName, bool hasTypeForwardedFrom)
		{
			this.fullTypeName = typeName;
			this.assemblyString = assemblyName;
			this.hasTypeForwardedFrom = hasTypeForwardedFrom;
		}

		// Token: 0x06004A58 RID: 19032 RVA: 0x00101F34 File Offset: 0x00100134
		internal SerObjectInfoCache(Type type)
		{
			TypeInformation typeInformation = BinaryFormatter.GetTypeInformation(type);
			this.fullTypeName = typeInformation.FullTypeName;
			this.assemblyString = typeInformation.AssemblyString;
			this.hasTypeForwardedFrom = typeInformation.HasTypeForwardedFrom;
		}

		// Token: 0x04002714 RID: 10004
		internal string fullTypeName;

		// Token: 0x04002715 RID: 10005
		internal string assemblyString;

		// Token: 0x04002716 RID: 10006
		internal bool hasTypeForwardedFrom;

		// Token: 0x04002717 RID: 10007
		internal MemberInfo[] memberInfos;

		// Token: 0x04002718 RID: 10008
		internal string[] memberNames;

		// Token: 0x04002719 RID: 10009
		internal Type[] memberTypes;
	}
}
