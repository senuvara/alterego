﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200074E RID: 1870
	[Serializable]
	internal sealed class SizedArray : ICloneable
	{
		// Token: 0x06004ADF RID: 19167 RVA: 0x00106842 File Offset: 0x00104A42
		internal SizedArray()
		{
			this.objects = new object[16];
			this.negObjects = new object[4];
		}

		// Token: 0x06004AE0 RID: 19168 RVA: 0x00106863 File Offset: 0x00104A63
		internal SizedArray(int length)
		{
			this.objects = new object[length];
			this.negObjects = new object[length];
		}

		// Token: 0x06004AE1 RID: 19169 RVA: 0x00106884 File Offset: 0x00104A84
		private SizedArray(SizedArray sizedArray)
		{
			this.objects = new object[sizedArray.objects.Length];
			sizedArray.objects.CopyTo(this.objects, 0);
			this.negObjects = new object[sizedArray.negObjects.Length];
			sizedArray.negObjects.CopyTo(this.negObjects, 0);
		}

		// Token: 0x06004AE2 RID: 19170 RVA: 0x001068E1 File Offset: 0x00104AE1
		public object Clone()
		{
			return new SizedArray(this);
		}

		// Token: 0x17000C6F RID: 3183
		internal object this[int index]
		{
			get
			{
				if (index < 0)
				{
					if (-index > this.negObjects.Length - 1)
					{
						return null;
					}
					return this.negObjects[-index];
				}
				else
				{
					if (index > this.objects.Length - 1)
					{
						return null;
					}
					return this.objects[index];
				}
			}
			set
			{
				if (index < 0)
				{
					if (-index > this.negObjects.Length - 1)
					{
						this.IncreaseCapacity(index);
					}
					this.negObjects[-index] = value;
					return;
				}
				if (index > this.objects.Length - 1)
				{
					this.IncreaseCapacity(index);
				}
				object obj = this.objects[index];
				this.objects[index] = value;
			}
		}

		// Token: 0x06004AE5 RID: 19173 RVA: 0x00106978 File Offset: 0x00104B78
		internal void IncreaseCapacity(int index)
		{
			try
			{
				if (index < 0)
				{
					object[] destinationArray = new object[Math.Max(this.negObjects.Length * 2, -index + 1)];
					Array.Copy(this.negObjects, 0, destinationArray, 0, this.negObjects.Length);
					this.negObjects = destinationArray;
				}
				else
				{
					object[] destinationArray2 = new object[Math.Max(this.objects.Length * 2, index + 1)];
					Array.Copy(this.objects, 0, destinationArray2, 0, this.objects.Length);
					this.objects = destinationArray2;
				}
			}
			catch (Exception)
			{
				throw new SerializationException(Environment.GetResourceString("Invalid BinaryFormatter stream."));
			}
		}

		// Token: 0x04002798 RID: 10136
		internal object[] objects;

		// Token: 0x04002799 RID: 10137
		internal object[] negObjects;
	}
}
