﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000738 RID: 1848
	[Serializable]
	internal enum InternalMemberValueE
	{
		// Token: 0x04002688 RID: 9864
		Empty,
		// Token: 0x04002689 RID: 9865
		InlineValue,
		// Token: 0x0400268A RID: 9866
		Nested,
		// Token: 0x0400268B RID: 9867
		Reference,
		// Token: 0x0400268C RID: 9868
		Null
	}
}
