﻿using System;
using System.Reflection;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000719 RID: 1817
	internal sealed class BinaryAssemblyInfo
	{
		// Token: 0x06004957 RID: 18775 RVA: 0x000FDCFA File Offset: 0x000FBEFA
		internal BinaryAssemblyInfo(string assemblyString)
		{
			this.assemblyString = assemblyString;
		}

		// Token: 0x06004958 RID: 18776 RVA: 0x000FDD09 File Offset: 0x000FBF09
		internal BinaryAssemblyInfo(string assemblyString, Assembly assembly)
		{
			this.assemblyString = assemblyString;
			this.assembly = assembly;
		}

		// Token: 0x06004959 RID: 18777 RVA: 0x000FDD20 File Offset: 0x000FBF20
		internal Assembly GetAssembly()
		{
			if (this.assembly == null)
			{
				this.assembly = FormatterServices.LoadAssemblyFromStringNoThrow(this.assemblyString);
				if (this.assembly == null)
				{
					throw new SerializationException(Environment.GetResourceString("Unable to find assembly '{0}'.", new object[]
					{
						this.assemblyString
					}));
				}
			}
			return this.assembly;
		}

		// Token: 0x040025A4 RID: 9636
		internal string assemblyString;

		// Token: 0x040025A5 RID: 9637
		private Assembly assembly;
	}
}
