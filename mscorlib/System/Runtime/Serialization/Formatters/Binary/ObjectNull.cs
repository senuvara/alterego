﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000729 RID: 1833
	internal sealed class ObjectNull : IStreamable
	{
		// Token: 0x060049B3 RID: 18867 RVA: 0x00002050 File Offset: 0x00000250
		internal ObjectNull()
		{
		}

		// Token: 0x060049B4 RID: 18868 RVA: 0x000FF0D7 File Offset: 0x000FD2D7
		internal void SetNullCount(int nullCount)
		{
			this.nullCount = nullCount;
		}

		// Token: 0x060049B5 RID: 18869 RVA: 0x000FF0E0 File Offset: 0x000FD2E0
		public void Write(__BinaryWriter sout)
		{
			if (this.nullCount == 1)
			{
				sout.WriteByte(10);
				return;
			}
			if (this.nullCount < 256)
			{
				sout.WriteByte(13);
				sout.WriteByte((byte)this.nullCount);
				return;
			}
			sout.WriteByte(14);
			sout.WriteInt32(this.nullCount);
		}

		// Token: 0x060049B6 RID: 18870 RVA: 0x000FF136 File Offset: 0x000FD336
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.Read(input, BinaryHeaderEnum.ObjectNull);
		}

		// Token: 0x060049B7 RID: 18871 RVA: 0x000FF144 File Offset: 0x000FD344
		public void Read(__BinaryParser input, BinaryHeaderEnum binaryHeaderEnum)
		{
			switch (binaryHeaderEnum)
			{
			case BinaryHeaderEnum.ObjectNull:
				this.nullCount = 1;
				return;
			case BinaryHeaderEnum.MessageEnd:
			case BinaryHeaderEnum.Assembly:
				break;
			case BinaryHeaderEnum.ObjectNullMultiple256:
				this.nullCount = (int)input.ReadByte();
				return;
			case BinaryHeaderEnum.ObjectNullMultiple:
				this.nullCount = input.ReadInt32();
				break;
			default:
				return;
			}
		}

		// Token: 0x060049B8 RID: 18872 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x060049B9 RID: 18873 RVA: 0x000FF190 File Offset: 0x000FD390
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			if (BCLDebug.CheckEnabled("BINARY") && this.nullCount != 1)
			{
				int num = this.nullCount;
			}
		}

		// Token: 0x040025E6 RID: 9702
		internal int nullCount;
	}
}
