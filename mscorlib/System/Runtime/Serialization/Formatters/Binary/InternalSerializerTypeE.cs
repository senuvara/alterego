﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000731 RID: 1841
	[Serializable]
	internal enum InternalSerializerTypeE
	{
		// Token: 0x0400265F RID: 9823
		Soap = 1,
		// Token: 0x04002660 RID: 9824
		Binary
	}
}
