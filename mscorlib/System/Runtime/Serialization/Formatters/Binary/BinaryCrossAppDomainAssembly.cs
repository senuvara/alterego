﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200071C RID: 1820
	internal sealed class BinaryCrossAppDomainAssembly : IStreamable
	{
		// Token: 0x06004967 RID: 18791 RVA: 0x00002050 File Offset: 0x00000250
		internal BinaryCrossAppDomainAssembly()
		{
		}

		// Token: 0x06004968 RID: 18792 RVA: 0x000FDF02 File Offset: 0x000FC102
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(20);
			sout.WriteInt32(this.assemId);
			sout.WriteInt32(this.assemblyIndex);
		}

		// Token: 0x06004969 RID: 18793 RVA: 0x000FDF24 File Offset: 0x000FC124
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.assemId = input.ReadInt32();
			this.assemblyIndex = input.ReadInt32();
		}

		// Token: 0x0600496A RID: 18794 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x0600496B RID: 18795 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025AF RID: 9647
		internal int assemId;

		// Token: 0x040025B0 RID: 9648
		internal int assemblyIndex;
	}
}
