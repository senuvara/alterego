﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000730 RID: 1840
	[Serializable]
	internal enum BinaryArrayTypeEnum
	{
		// Token: 0x04002658 RID: 9816
		Single,
		// Token: 0x04002659 RID: 9817
		Jagged,
		// Token: 0x0400265A RID: 9818
		Rectangular,
		// Token: 0x0400265B RID: 9819
		SingleOffset,
		// Token: 0x0400265C RID: 9820
		JaggedOffset,
		// Token: 0x0400265D RID: 9821
		RectangularOffset
	}
}
