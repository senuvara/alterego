﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000752 RID: 1874
	internal sealed class InternalFE
	{
		// Token: 0x06004AF3 RID: 19187 RVA: 0x00002050 File Offset: 0x00000250
		public InternalFE()
		{
		}

		// Token: 0x040027A6 RID: 10150
		internal FormatterTypeStyle FEtypeFormat;

		// Token: 0x040027A7 RID: 10151
		internal FormatterAssemblyStyle FEassemblyFormat;

		// Token: 0x040027A8 RID: 10152
		internal TypeFilterLevel FEsecurityLevel;

		// Token: 0x040027A9 RID: 10153
		internal InternalSerializerTypeE FEserializerTypeEnum;
	}
}
