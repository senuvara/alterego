﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000753 RID: 1875
	internal sealed class NameInfo
	{
		// Token: 0x06004AF4 RID: 19188 RVA: 0x00002050 File Offset: 0x00000250
		internal NameInfo()
		{
		}

		// Token: 0x06004AF5 RID: 19189 RVA: 0x00106D90 File Offset: 0x00104F90
		internal void Init()
		{
			this.NIFullName = null;
			this.NIobjectId = 0L;
			this.NIassemId = 0L;
			this.NIprimitiveTypeEnum = InternalPrimitiveTypeE.Invalid;
			this.NItype = null;
			this.NIisSealed = false;
			this.NItransmitTypeOnObject = false;
			this.NItransmitTypeOnMember = false;
			this.NIisParentTypeOnObject = false;
			this.NIisArray = false;
			this.NIisArrayItem = false;
			this.NIarrayEnum = InternalArrayTypeE.Empty;
			this.NIsealedStatusChecked = false;
		}

		// Token: 0x17000C71 RID: 3185
		// (get) Token: 0x06004AF6 RID: 19190 RVA: 0x00106DFA File Offset: 0x00104FFA
		public bool IsSealed
		{
			get
			{
				if (!this.NIsealedStatusChecked)
				{
					this.NIisSealed = this.NItype.IsSealed;
					this.NIsealedStatusChecked = true;
				}
				return this.NIisSealed;
			}
		}

		// Token: 0x17000C72 RID: 3186
		// (get) Token: 0x06004AF7 RID: 19191 RVA: 0x00106E22 File Offset: 0x00105022
		// (set) Token: 0x06004AF8 RID: 19192 RVA: 0x00106E43 File Offset: 0x00105043
		public string NIname
		{
			get
			{
				if (this.NIFullName == null)
				{
					this.NIFullName = this.NItype.FullName;
				}
				return this.NIFullName;
			}
			set
			{
				this.NIFullName = value;
			}
		}

		// Token: 0x040027AA RID: 10154
		internal string NIFullName;

		// Token: 0x040027AB RID: 10155
		internal long NIobjectId;

		// Token: 0x040027AC RID: 10156
		internal long NIassemId;

		// Token: 0x040027AD RID: 10157
		internal InternalPrimitiveTypeE NIprimitiveTypeEnum;

		// Token: 0x040027AE RID: 10158
		internal Type NItype;

		// Token: 0x040027AF RID: 10159
		internal bool NIisSealed;

		// Token: 0x040027B0 RID: 10160
		internal bool NIisArray;

		// Token: 0x040027B1 RID: 10161
		internal bool NIisArrayItem;

		// Token: 0x040027B2 RID: 10162
		internal bool NItransmitTypeOnObject;

		// Token: 0x040027B3 RID: 10163
		internal bool NItransmitTypeOnMember;

		// Token: 0x040027B4 RID: 10164
		internal bool NIisParentTypeOnObject;

		// Token: 0x040027B5 RID: 10165
		internal InternalArrayTypeE NIarrayEnum;

		// Token: 0x040027B6 RID: 10166
		private bool NIsealedStatusChecked;
	}
}
