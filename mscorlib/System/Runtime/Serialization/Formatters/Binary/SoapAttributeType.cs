﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200073E RID: 1854
	[Serializable]
	internal enum SoapAttributeType
	{
		// Token: 0x040026C7 RID: 9927
		None,
		// Token: 0x040026C8 RID: 9928
		SchemaType,
		// Token: 0x040026C9 RID: 9929
		Embedded,
		// Token: 0x040026CA RID: 9930
		XmlElement = 4,
		// Token: 0x040026CB RID: 9931
		XmlAttribute = 8
	}
}
