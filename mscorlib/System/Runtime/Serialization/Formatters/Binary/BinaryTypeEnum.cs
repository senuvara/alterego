﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200072F RID: 1839
	[Serializable]
	internal enum BinaryTypeEnum
	{
		// Token: 0x0400264F RID: 9807
		Primitive,
		// Token: 0x04002650 RID: 9808
		String,
		// Token: 0x04002651 RID: 9809
		Object,
		// Token: 0x04002652 RID: 9810
		ObjectUrt,
		// Token: 0x04002653 RID: 9811
		ObjectUser,
		// Token: 0x04002654 RID: 9812
		ObjectArray,
		// Token: 0x04002655 RID: 9813
		StringArray,
		// Token: 0x04002656 RID: 9814
		PrimitiveArray
	}
}
