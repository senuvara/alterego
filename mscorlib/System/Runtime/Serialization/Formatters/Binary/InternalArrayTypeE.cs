﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000736 RID: 1846
	[Serializable]
	internal enum InternalArrayTypeE
	{
		// Token: 0x0400267D RID: 9853
		Empty,
		// Token: 0x0400267E RID: 9854
		Single,
		// Token: 0x0400267F RID: 9855
		Jagged,
		// Token: 0x04002680 RID: 9856
		Rectangular,
		// Token: 0x04002681 RID: 9857
		Base64
	}
}
