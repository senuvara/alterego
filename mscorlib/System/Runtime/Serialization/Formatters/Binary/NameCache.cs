﻿using System;
using System.Collections.Concurrent;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000750 RID: 1872
	internal sealed class NameCache
	{
		// Token: 0x06004AEC RID: 19180 RVA: 0x00106BEC File Offset: 0x00104DEC
		internal object GetCachedValue(string name)
		{
			this.name = name;
			object result;
			if (!NameCache.ht.TryGetValue(name, out result))
			{
				return null;
			}
			return result;
		}

		// Token: 0x06004AED RID: 19181 RVA: 0x00106C12 File Offset: 0x00104E12
		internal void SetCachedValue(object value)
		{
			NameCache.ht[this.name] = value;
		}

		// Token: 0x06004AEE RID: 19182 RVA: 0x00002050 File Offset: 0x00000250
		public NameCache()
		{
		}

		// Token: 0x06004AEF RID: 19183 RVA: 0x00106C25 File Offset: 0x00104E25
		// Note: this type is marked as 'beforefieldinit'.
		static NameCache()
		{
		}

		// Token: 0x0400279C RID: 10140
		private static ConcurrentDictionary<string, object> ht = new ConcurrentDictionary<string, object>();

		// Token: 0x0400279D RID: 10141
		private string name;
	}
}
