﻿using System;
using System.Diagnostics;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000717 RID: 1815
	internal static class BinaryUtil
	{
		// Token: 0x06004953 RID: 18771 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		public static void NVTraceI(string name, string value)
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x06004954 RID: 18772 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		public static void NVTraceI(string name, object value)
		{
			BCLDebug.CheckEnabled("BINARY");
		}
	}
}
