﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000723 RID: 1827
	internal sealed class MemberPrimitiveTyped : IStreamable
	{
		// Token: 0x0600498F RID: 18831 RVA: 0x00002050 File Offset: 0x00000250
		internal MemberPrimitiveTyped()
		{
		}

		// Token: 0x06004990 RID: 18832 RVA: 0x000FE831 File Offset: 0x000FCA31
		internal void Set(InternalPrimitiveTypeE primitiveTypeEnum, object value)
		{
			this.primitiveTypeEnum = primitiveTypeEnum;
			this.value = value;
		}

		// Token: 0x06004991 RID: 18833 RVA: 0x000FE841 File Offset: 0x000FCA41
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(8);
			sout.WriteByte((byte)this.primitiveTypeEnum);
			sout.WriteValue(this.primitiveTypeEnum, this.value);
		}

		// Token: 0x06004992 RID: 18834 RVA: 0x000FE869 File Offset: 0x000FCA69
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.primitiveTypeEnum = (InternalPrimitiveTypeE)input.ReadByte();
			this.value = input.ReadValue(this.primitiveTypeEnum);
		}

		// Token: 0x06004993 RID: 18835 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x06004994 RID: 18836 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025C9 RID: 9673
		internal InternalPrimitiveTypeE primitiveTypeEnum;

		// Token: 0x040025CA RID: 9674
		internal object value;
	}
}
