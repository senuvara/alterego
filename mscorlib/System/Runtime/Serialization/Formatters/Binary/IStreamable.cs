﻿using System;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000718 RID: 1816
	internal interface IStreamable
	{
		// Token: 0x06004955 RID: 18773
		[SecurityCritical]
		void Read(__BinaryParser input);

		// Token: 0x06004956 RID: 18774
		void Write(__BinaryWriter sout);
	}
}
