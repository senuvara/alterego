﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000720 RID: 1824
	internal sealed class BinaryObjectString : IStreamable
	{
		// Token: 0x0600497F RID: 18815 RVA: 0x00002050 File Offset: 0x00000250
		internal BinaryObjectString()
		{
		}

		// Token: 0x06004980 RID: 18816 RVA: 0x000FE786 File Offset: 0x000FC986
		internal void Set(int objectId, string value)
		{
			this.objectId = objectId;
			this.value = value;
		}

		// Token: 0x06004981 RID: 18817 RVA: 0x000FE796 File Offset: 0x000FC996
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(6);
			sout.WriteInt32(this.objectId);
			sout.WriteString(this.value);
		}

		// Token: 0x06004982 RID: 18818 RVA: 0x000FE7B7 File Offset: 0x000FC9B7
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.objectId = input.ReadInt32();
			this.value = input.ReadString();
		}

		// Token: 0x06004983 RID: 18819 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x06004984 RID: 18820 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025C4 RID: 9668
		internal int objectId;

		// Token: 0x040025C5 RID: 9669
		internal string value;
	}
}
