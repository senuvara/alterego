﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200071D RID: 1821
	internal sealed class BinaryObject : IStreamable
	{
		// Token: 0x0600496C RID: 18796 RVA: 0x00002050 File Offset: 0x00000250
		internal BinaryObject()
		{
		}

		// Token: 0x0600496D RID: 18797 RVA: 0x000FDF3E File Offset: 0x000FC13E
		internal void Set(int objectId, int mapId)
		{
			this.objectId = objectId;
			this.mapId = mapId;
		}

		// Token: 0x0600496E RID: 18798 RVA: 0x000FDF4E File Offset: 0x000FC14E
		public void Write(__BinaryWriter sout)
		{
			sout.WriteByte(1);
			sout.WriteInt32(this.objectId);
			sout.WriteInt32(this.mapId);
		}

		// Token: 0x0600496F RID: 18799 RVA: 0x000FDF6F File Offset: 0x000FC16F
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.objectId = input.ReadInt32();
			this.mapId = input.ReadInt32();
		}

		// Token: 0x06004970 RID: 18800 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x06004971 RID: 18801 RVA: 0x000FDCED File Offset: 0x000FBEED
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			BCLDebug.CheckEnabled("BINARY");
		}

		// Token: 0x040025B1 RID: 9649
		internal int objectId;

		// Token: 0x040025B2 RID: 9650
		internal int mapId;
	}
}
