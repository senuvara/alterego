﻿using System;
using System.Diagnostics;
using System.Security;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000727 RID: 1831
	internal sealed class MemberPrimitiveUnTyped : IStreamable
	{
		// Token: 0x060049A6 RID: 18854 RVA: 0x00002050 File Offset: 0x00000250
		internal MemberPrimitiveUnTyped()
		{
		}

		// Token: 0x060049A7 RID: 18855 RVA: 0x000FF04F File Offset: 0x000FD24F
		internal void Set(InternalPrimitiveTypeE typeInformation, object value)
		{
			this.typeInformation = typeInformation;
			this.value = value;
		}

		// Token: 0x060049A8 RID: 18856 RVA: 0x000FF05F File Offset: 0x000FD25F
		internal void Set(InternalPrimitiveTypeE typeInformation)
		{
			this.typeInformation = typeInformation;
		}

		// Token: 0x060049A9 RID: 18857 RVA: 0x000FF068 File Offset: 0x000FD268
		public void Write(__BinaryWriter sout)
		{
			sout.WriteValue(this.typeInformation, this.value);
		}

		// Token: 0x060049AA RID: 18858 RVA: 0x000FF07C File Offset: 0x000FD27C
		[SecurityCritical]
		public void Read(__BinaryParser input)
		{
			this.value = input.ReadValue(this.typeInformation);
		}

		// Token: 0x060049AB RID: 18859 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Dump()
		{
		}

		// Token: 0x060049AC RID: 18860 RVA: 0x000FF090 File Offset: 0x000FD290
		[Conditional("_LOGGING")]
		private void DumpInternal()
		{
			if (BCLDebug.CheckEnabled("BINARY"))
			{
				Converter.ToComType(this.typeInformation);
			}
		}

		// Token: 0x040025E3 RID: 9699
		internal InternalPrimitiveTypeE typeInformation;

		// Token: 0x040025E4 RID: 9700
		internal object value;
	}
}
