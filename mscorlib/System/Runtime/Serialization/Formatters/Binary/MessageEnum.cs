﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200073B RID: 1851
	[Flags]
	[Serializable]
	internal enum MessageEnum
	{
		// Token: 0x040026A7 RID: 9895
		NoArgs = 1,
		// Token: 0x040026A8 RID: 9896
		ArgsInline = 2,
		// Token: 0x040026A9 RID: 9897
		ArgsIsArray = 4,
		// Token: 0x040026AA RID: 9898
		ArgsInArray = 8,
		// Token: 0x040026AB RID: 9899
		NoContext = 16,
		// Token: 0x040026AC RID: 9900
		ContextInline = 32,
		// Token: 0x040026AD RID: 9901
		ContextInArray = 64,
		// Token: 0x040026AE RID: 9902
		MethodSignatureInArray = 128,
		// Token: 0x040026AF RID: 9903
		PropertyInArray = 256,
		// Token: 0x040026B0 RID: 9904
		NoReturnValue = 512,
		// Token: 0x040026B1 RID: 9905
		ReturnValueVoid = 1024,
		// Token: 0x040026B2 RID: 9906
		ReturnValueInline = 2048,
		// Token: 0x040026B3 RID: 9907
		ReturnValueInArray = 4096,
		// Token: 0x040026B4 RID: 9908
		ExceptionInArray = 8192,
		// Token: 0x040026B5 RID: 9909
		GenericMethod = 32768
	}
}
