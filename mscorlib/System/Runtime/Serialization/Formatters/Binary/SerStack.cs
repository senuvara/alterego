﻿using System;
using System.Diagnostics;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200074D RID: 1869
	internal sealed class SerStack
	{
		// Token: 0x06004AD5 RID: 19157 RVA: 0x001066D2 File Offset: 0x001048D2
		internal SerStack()
		{
			this.stackId = "System";
		}

		// Token: 0x06004AD6 RID: 19158 RVA: 0x001066F8 File Offset: 0x001048F8
		internal SerStack(string stackId)
		{
			this.stackId = stackId;
		}

		// Token: 0x06004AD7 RID: 19159 RVA: 0x0010671C File Offset: 0x0010491C
		internal void Push(object obj)
		{
			if (this.top == this.objects.Length - 1)
			{
				this.IncreaseCapacity();
			}
			object[] array = this.objects;
			int num = this.top + 1;
			this.top = num;
			array[num] = obj;
		}

		// Token: 0x06004AD8 RID: 19160 RVA: 0x0010675C File Offset: 0x0010495C
		internal object Pop()
		{
			if (this.top < 0)
			{
				return null;
			}
			object result = this.objects[this.top];
			object[] array = this.objects;
			int num = this.top;
			this.top = num - 1;
			array[num] = null;
			return result;
		}

		// Token: 0x06004AD9 RID: 19161 RVA: 0x0010679C File Offset: 0x0010499C
		internal void IncreaseCapacity()
		{
			object[] destinationArray = new object[this.objects.Length * 2];
			Array.Copy(this.objects, 0, destinationArray, 0, this.objects.Length);
			this.objects = destinationArray;
		}

		// Token: 0x06004ADA RID: 19162 RVA: 0x001067D6 File Offset: 0x001049D6
		internal object Peek()
		{
			if (this.top < 0)
			{
				return null;
			}
			return this.objects[this.top];
		}

		// Token: 0x06004ADB RID: 19163 RVA: 0x001067F0 File Offset: 0x001049F0
		internal object PeekPeek()
		{
			if (this.top < 1)
			{
				return null;
			}
			return this.objects[this.top - 1];
		}

		// Token: 0x06004ADC RID: 19164 RVA: 0x0010680C File Offset: 0x00104A0C
		internal int Count()
		{
			return this.top + 1;
		}

		// Token: 0x06004ADD RID: 19165 RVA: 0x00106816 File Offset: 0x00104A16
		internal bool IsEmpty()
		{
			return this.top <= 0;
		}

		// Token: 0x06004ADE RID: 19166 RVA: 0x00106824 File Offset: 0x00104A24
		[Conditional("SER_LOGGING")]
		internal void Dump()
		{
			for (int i = 0; i < this.Count(); i++)
			{
			}
		}

		// Token: 0x04002794 RID: 10132
		internal object[] objects = new object[5];

		// Token: 0x04002795 RID: 10133
		internal string stackId;

		// Token: 0x04002796 RID: 10134
		internal int top = -1;

		// Token: 0x04002797 RID: 10135
		internal int next;
	}
}
