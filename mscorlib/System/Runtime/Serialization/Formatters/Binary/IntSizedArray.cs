﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200074F RID: 1871
	[Serializable]
	internal sealed class IntSizedArray : ICloneable
	{
		// Token: 0x06004AE6 RID: 19174 RVA: 0x00106A1C File Offset: 0x00104C1C
		public IntSizedArray()
		{
		}

		// Token: 0x06004AE7 RID: 19175 RVA: 0x00106A40 File Offset: 0x00104C40
		private IntSizedArray(IntSizedArray sizedArray)
		{
			this.objects = new int[sizedArray.objects.Length];
			sizedArray.objects.CopyTo(this.objects, 0);
			this.negObjects = new int[sizedArray.negObjects.Length];
			sizedArray.negObjects.CopyTo(this.negObjects, 0);
		}

		// Token: 0x06004AE8 RID: 19176 RVA: 0x00106AB6 File Offset: 0x00104CB6
		public object Clone()
		{
			return new IntSizedArray(this);
		}

		// Token: 0x17000C70 RID: 3184
		internal int this[int index]
		{
			get
			{
				if (index < 0)
				{
					if (-index > this.negObjects.Length - 1)
					{
						return 0;
					}
					return this.negObjects[-index];
				}
				else
				{
					if (index > this.objects.Length - 1)
					{
						return 0;
					}
					return this.objects[index];
				}
			}
			set
			{
				if (index < 0)
				{
					if (-index > this.negObjects.Length - 1)
					{
						this.IncreaseCapacity(index);
					}
					this.negObjects[-index] = value;
					return;
				}
				if (index > this.objects.Length - 1)
				{
					this.IncreaseCapacity(index);
				}
				this.objects[index] = value;
			}
		}

		// Token: 0x06004AEB RID: 19179 RVA: 0x00106B48 File Offset: 0x00104D48
		internal void IncreaseCapacity(int index)
		{
			try
			{
				if (index < 0)
				{
					int[] destinationArray = new int[Math.Max(this.negObjects.Length * 2, -index + 1)];
					Array.Copy(this.negObjects, 0, destinationArray, 0, this.negObjects.Length);
					this.negObjects = destinationArray;
				}
				else
				{
					int[] destinationArray2 = new int[Math.Max(this.objects.Length * 2, index + 1)];
					Array.Copy(this.objects, 0, destinationArray2, 0, this.objects.Length);
					this.objects = destinationArray2;
				}
			}
			catch (Exception)
			{
				throw new SerializationException(Environment.GetResourceString("Invalid BinaryFormatter stream."));
			}
		}

		// Token: 0x0400279A RID: 10138
		internal int[] objects = new int[16];

		// Token: 0x0400279B RID: 10139
		internal int[] negObjects = new int[4];
	}
}
