﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Runtime.Serialization.Formatters
{
	/// <summary>Allows access to field names and field types of objects that support the <see cref="T:System.Runtime.Serialization.ISerializable" /> interface.</summary>
	// Token: 0x0200070D RID: 1805
	[ComVisible(true)]
	public interface IFieldInfo
	{
		/// <summary>Gets or sets the field names of serialized objects.</summary>
		/// <returns>The field names of serialized objects.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x17000C49 RID: 3145
		// (get) Token: 0x0600490B RID: 18699
		// (set) Token: 0x0600490C RID: 18700
		string[] FieldNames { [SecurityCritical] get; [SecurityCritical] set; }

		/// <summary>Gets or sets the field types of the serialized objects.</summary>
		/// <returns>The field types of the serialized objects.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x17000C4A RID: 3146
		// (get) Token: 0x0600490D RID: 18701
		// (set) Token: 0x0600490E RID: 18702
		Type[] FieldTypes { [SecurityCritical] get; [SecurityCritical] set; }
	}
}
