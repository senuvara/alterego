﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	/// <summary>Specifies that a field can be missing from a serialization stream so that the <see cref="T:System.Runtime.Serialization.Formatters.Binary.BinaryFormatter" /> and the <see cref="T:System.Runtime.Serialization.Formatters.Soap.SoapFormatter" /> does not throw an exception. </summary>
	// Token: 0x020006F6 RID: 1782
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	public sealed class OptionalFieldAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Serialization.OptionalFieldAttribute" /> class. </summary>
		// Token: 0x06004883 RID: 18563 RVA: 0x000FBED0 File Offset: 0x000FA0D0
		public OptionalFieldAttribute()
		{
		}

		/// <summary>This property is unused and is reserved.</summary>
		/// <returns>This property is reserved.</returns>
		// Token: 0x17000C29 RID: 3113
		// (get) Token: 0x06004884 RID: 18564 RVA: 0x000FBEDF File Offset: 0x000FA0DF
		// (set) Token: 0x06004885 RID: 18565 RVA: 0x000FBEE7 File Offset: 0x000FA0E7
		public int VersionAdded
		{
			get
			{
				return this.versionAdded;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentException(Environment.GetResourceString("Version value must be positive."));
				}
				this.versionAdded = value;
			}
		}

		// Token: 0x04002554 RID: 9556
		private int versionAdded = 1;
	}
}
