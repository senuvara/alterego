﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020006EF RID: 1775
	[Serializable]
	internal class LongList
	{
		// Token: 0x0600485E RID: 18526 RVA: 0x000FB862 File Offset: 0x000F9A62
		internal LongList() : this(2)
		{
		}

		// Token: 0x0600485F RID: 18527 RVA: 0x000FB86B File Offset: 0x000F9A6B
		internal LongList(int startingSize)
		{
			this.m_count = 0;
			this.m_totalItems = 0;
			this.m_values = new long[startingSize];
		}

		// Token: 0x06004860 RID: 18528 RVA: 0x000FB890 File Offset: 0x000F9A90
		internal void Add(long value)
		{
			if (this.m_totalItems == this.m_values.Length)
			{
				this.EnlargeArray();
			}
			long[] values = this.m_values;
			int totalItems = this.m_totalItems;
			this.m_totalItems = totalItems + 1;
			values[totalItems] = value;
			this.m_count++;
		}

		// Token: 0x17000C20 RID: 3104
		// (get) Token: 0x06004861 RID: 18529 RVA: 0x000FB8DA File Offset: 0x000F9ADA
		internal int Count
		{
			get
			{
				return this.m_count;
			}
		}

		// Token: 0x06004862 RID: 18530 RVA: 0x000FB8E2 File Offset: 0x000F9AE2
		internal void StartEnumeration()
		{
			this.m_currentItem = -1;
		}

		// Token: 0x06004863 RID: 18531 RVA: 0x000FB8EC File Offset: 0x000F9AEC
		internal bool MoveNext()
		{
			int num;
			do
			{
				num = this.m_currentItem + 1;
				this.m_currentItem = num;
			}
			while (num < this.m_totalItems && this.m_values[this.m_currentItem] == -1L);
			return this.m_currentItem != this.m_totalItems;
		}

		// Token: 0x17000C21 RID: 3105
		// (get) Token: 0x06004864 RID: 18532 RVA: 0x000FB934 File Offset: 0x000F9B34
		internal long Current
		{
			get
			{
				return this.m_values[this.m_currentItem];
			}
		}

		// Token: 0x06004865 RID: 18533 RVA: 0x000FB944 File Offset: 0x000F9B44
		internal bool RemoveElement(long value)
		{
			int num = 0;
			while (num < this.m_totalItems && this.m_values[num] != value)
			{
				num++;
			}
			if (num == this.m_totalItems)
			{
				return false;
			}
			this.m_values[num] = -1L;
			return true;
		}

		// Token: 0x06004866 RID: 18534 RVA: 0x000FB984 File Offset: 0x000F9B84
		private void EnlargeArray()
		{
			int num = this.m_values.Length * 2;
			if (num < 0)
			{
				if (num == 2147483647)
				{
					throw new SerializationException(Environment.GetResourceString("The internal array cannot expand to greater than Int32.MaxValue elements."));
				}
				num = int.MaxValue;
			}
			long[] array = new long[num];
			Array.Copy(this.m_values, array, this.m_count);
			this.m_values = array;
		}

		// Token: 0x0400253F RID: 9535
		private const int InitialSize = 2;

		// Token: 0x04002540 RID: 9536
		private long[] m_values;

		// Token: 0x04002541 RID: 9537
		private int m_count;

		// Token: 0x04002542 RID: 9538
		private int m_totalItems;

		// Token: 0x04002543 RID: 9539
		private int m_currentItem;
	}
}
