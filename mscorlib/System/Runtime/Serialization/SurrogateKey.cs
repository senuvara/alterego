﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x02000707 RID: 1799
	[Serializable]
	internal class SurrogateKey
	{
		// Token: 0x06004903 RID: 18691 RVA: 0x000FD431 File Offset: 0x000FB631
		internal SurrogateKey(Type type, StreamingContext context)
		{
			this.m_type = type;
			this.m_context = context;
		}

		// Token: 0x06004904 RID: 18692 RVA: 0x000FD447 File Offset: 0x000FB647
		public override int GetHashCode()
		{
			return this.m_type.GetHashCode();
		}

		// Token: 0x04002587 RID: 9607
		internal Type m_type;

		// Token: 0x04002588 RID: 9608
		internal StreamingContext m_context;
	}
}
