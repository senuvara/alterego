﻿using System;
using System.Reflection;

namespace System.Runtime.Serialization
{
	// Token: 0x020006E9 RID: 1769
	[Serializable]
	internal class MemberHolder
	{
		// Token: 0x06004801 RID: 18433 RVA: 0x000F9EEC File Offset: 0x000F80EC
		internal MemberHolder(Type type, StreamingContext ctx)
		{
			this.memberType = type;
			this.context = ctx;
		}

		// Token: 0x06004802 RID: 18434 RVA: 0x000F9F02 File Offset: 0x000F8102
		public override int GetHashCode()
		{
			return this.memberType.GetHashCode();
		}

		// Token: 0x06004803 RID: 18435 RVA: 0x000F9F10 File Offset: 0x000F8110
		public override bool Equals(object obj)
		{
			if (!(obj is MemberHolder))
			{
				return false;
			}
			MemberHolder memberHolder = (MemberHolder)obj;
			return memberHolder.memberType == this.memberType && memberHolder.context.State == this.context.State;
		}

		// Token: 0x0400250C RID: 9484
		internal MemberInfo[] members;

		// Token: 0x0400250D RID: 9485
		internal Type memberType;

		// Token: 0x0400250E RID: 9486
		internal StreamingContext context;
	}
}
