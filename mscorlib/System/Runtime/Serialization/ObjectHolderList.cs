﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020006F0 RID: 1776
	internal class ObjectHolderList
	{
		// Token: 0x06004867 RID: 18535 RVA: 0x000FB9DE File Offset: 0x000F9BDE
		internal ObjectHolderList() : this(8)
		{
		}

		// Token: 0x06004868 RID: 18536 RVA: 0x000FB9E7 File Offset: 0x000F9BE7
		internal ObjectHolderList(int startingSize)
		{
			this.m_count = 0;
			this.m_values = new ObjectHolder[startingSize];
		}

		// Token: 0x06004869 RID: 18537 RVA: 0x000FBA04 File Offset: 0x000F9C04
		internal virtual void Add(ObjectHolder value)
		{
			if (this.m_count == this.m_values.Length)
			{
				this.EnlargeArray();
			}
			ObjectHolder[] values = this.m_values;
			int count = this.m_count;
			this.m_count = count + 1;
			values[count] = value;
		}

		// Token: 0x0600486A RID: 18538 RVA: 0x000FBA40 File Offset: 0x000F9C40
		internal ObjectHolderListEnumerator GetFixupEnumerator()
		{
			return new ObjectHolderListEnumerator(this, true);
		}

		// Token: 0x0600486B RID: 18539 RVA: 0x000FBA4C File Offset: 0x000F9C4C
		private void EnlargeArray()
		{
			int num = this.m_values.Length * 2;
			if (num < 0)
			{
				if (num == 2147483647)
				{
					throw new SerializationException(Environment.GetResourceString("The internal array cannot expand to greater than Int32.MaxValue elements."));
				}
				num = int.MaxValue;
			}
			ObjectHolder[] array = new ObjectHolder[num];
			Array.Copy(this.m_values, array, this.m_count);
			this.m_values = array;
		}

		// Token: 0x17000C22 RID: 3106
		// (get) Token: 0x0600486C RID: 18540 RVA: 0x000FBAA6 File Offset: 0x000F9CA6
		internal int Version
		{
			get
			{
				return this.m_count;
			}
		}

		// Token: 0x17000C23 RID: 3107
		// (get) Token: 0x0600486D RID: 18541 RVA: 0x000FBAA6 File Offset: 0x000F9CA6
		internal int Count
		{
			get
			{
				return this.m_count;
			}
		}

		// Token: 0x04002544 RID: 9540
		internal const int DefaultInitialSize = 8;

		// Token: 0x04002545 RID: 9541
		internal ObjectHolder[] m_values;

		// Token: 0x04002546 RID: 9542
		internal int m_count;
	}
}
