﻿using System;

namespace System.Runtime.Serialization
{
	/// <summary>Enables serialization of custom exception data in security-transparent code.</summary>
	// Token: 0x020006F4 RID: 1780
	public interface ISafeSerializationData
	{
		/// <summary>This method is called when the instance is deserialized. </summary>
		/// <param name="deserialized">An object that contains the state of the instance.</param>
		// Token: 0x06004878 RID: 18552
		void CompleteDeserialization(object deserialized);
	}
}
