﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020006EE RID: 1774
	[Serializable]
	internal class FixupHolderList
	{
		// Token: 0x06004859 RID: 18521 RVA: 0x000FB753 File Offset: 0x000F9953
		internal FixupHolderList() : this(2)
		{
		}

		// Token: 0x0600485A RID: 18522 RVA: 0x000FB75C File Offset: 0x000F995C
		internal FixupHolderList(int startingSize)
		{
			this.m_count = 0;
			this.m_values = new FixupHolder[startingSize];
		}

		// Token: 0x0600485B RID: 18523 RVA: 0x000FB778 File Offset: 0x000F9978
		internal virtual void Add(long id, object fixupInfo)
		{
			if (this.m_count == this.m_values.Length)
			{
				this.EnlargeArray();
			}
			this.m_values[this.m_count].m_id = id;
			FixupHolder[] values = this.m_values;
			int count = this.m_count;
			this.m_count = count + 1;
			values[count].m_fixupInfo = fixupInfo;
		}

		// Token: 0x0600485C RID: 18524 RVA: 0x000FB7CC File Offset: 0x000F99CC
		internal virtual void Add(FixupHolder fixup)
		{
			if (this.m_count == this.m_values.Length)
			{
				this.EnlargeArray();
			}
			FixupHolder[] values = this.m_values;
			int count = this.m_count;
			this.m_count = count + 1;
			values[count] = fixup;
		}

		// Token: 0x0600485D RID: 18525 RVA: 0x000FB808 File Offset: 0x000F9A08
		private void EnlargeArray()
		{
			int num = this.m_values.Length * 2;
			if (num < 0)
			{
				if (num == 2147483647)
				{
					throw new SerializationException(Environment.GetResourceString("The internal array cannot expand to greater than Int32.MaxValue elements."));
				}
				num = int.MaxValue;
			}
			FixupHolder[] array = new FixupHolder[num];
			Array.Copy(this.m_values, array, this.m_count);
			this.m_values = array;
		}

		// Token: 0x0400253C RID: 9532
		internal const int InitialSize = 2;

		// Token: 0x0400253D RID: 9533
		internal FixupHolder[] m_values;

		// Token: 0x0400253E RID: 9534
		internal int m_count;
	}
}
