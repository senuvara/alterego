﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Unity;

namespace System.Runtime.Serialization
{
	/// <summary>Provides a formatter-friendly mechanism for parsing the data in <see cref="T:System.Runtime.Serialization.SerializationInfo" />. This class cannot be inherited.</summary>
	// Token: 0x02000702 RID: 1794
	[ComVisible(true)]
	public sealed class SerializationInfoEnumerator : IEnumerator
	{
		// Token: 0x060048E9 RID: 18665 RVA: 0x000FCF31 File Offset: 0x000FB131
		internal SerializationInfoEnumerator(string[] members, object[] info, Type[] types, int numItems)
		{
			this.m_members = members;
			this.m_data = info;
			this.m_types = types;
			this.m_numItems = numItems - 1;
			this.m_currItem = -1;
			this.m_current = false;
		}

		/// <summary>Updates the enumerator to the next item.</summary>
		/// <returns>
		///     <see langword="true" /> if a new element is found; otherwise, <see langword="false" />.</returns>
		// Token: 0x060048EA RID: 18666 RVA: 0x000FCF66 File Offset: 0x000FB166
		public bool MoveNext()
		{
			if (this.m_currItem < this.m_numItems)
			{
				this.m_currItem++;
				this.m_current = true;
			}
			else
			{
				this.m_current = false;
			}
			return this.m_current;
		}

		/// <summary>Gets the current item in the collection.</summary>
		/// <returns>A <see cref="T:System.Runtime.Serialization.SerializationEntry" /> that contains the current serialization data.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumeration has not started or has already ended. </exception>
		// Token: 0x17000C3F RID: 3135
		// (get) Token: 0x060048EB RID: 18667 RVA: 0x000FCF9C File Offset: 0x000FB19C
		object IEnumerator.Current
		{
			get
			{
				if (!this.m_current)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
				return new SerializationEntry(this.m_members[this.m_currItem], this.m_data[this.m_currItem], this.m_types[this.m_currItem]);
			}
		}

		/// <summary>Gets the item currently being examined.</summary>
		/// <returns>The item currently being examined.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator has not started enumerating items or has reached the end of the enumeration. </exception>
		// Token: 0x17000C40 RID: 3136
		// (get) Token: 0x060048EC RID: 18668 RVA: 0x000FCFF4 File Offset: 0x000FB1F4
		public SerializationEntry Current
		{
			get
			{
				if (!this.m_current)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
				return new SerializationEntry(this.m_members[this.m_currItem], this.m_data[this.m_currItem], this.m_types[this.m_currItem]);
			}
		}

		/// <summary>Resets the enumerator to the first item.</summary>
		// Token: 0x060048ED RID: 18669 RVA: 0x000FD045 File Offset: 0x000FB245
		public void Reset()
		{
			this.m_currItem = -1;
			this.m_current = false;
		}

		/// <summary>Gets the name for the item currently being examined.</summary>
		/// <returns>The item name.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator has not started enumerating items or has reached the end of the enumeration. </exception>
		// Token: 0x17000C41 RID: 3137
		// (get) Token: 0x060048EE RID: 18670 RVA: 0x000FD055 File Offset: 0x000FB255
		public string Name
		{
			get
			{
				if (!this.m_current)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
				return this.m_members[this.m_currItem];
			}
		}

		/// <summary>Gets the value of the item currently being examined.</summary>
		/// <returns>The value of the item currently being examined.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator has not started enumerating items or has reached the end of the enumeration. </exception>
		// Token: 0x17000C42 RID: 3138
		// (get) Token: 0x060048EF RID: 18671 RVA: 0x000FD07C File Offset: 0x000FB27C
		public object Value
		{
			get
			{
				if (!this.m_current)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
				return this.m_data[this.m_currItem];
			}
		}

		/// <summary>Gets the type of the item currently being examined.</summary>
		/// <returns>The type of the item currently being examined.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumerator has not started enumerating items or has reached the end of the enumeration. </exception>
		// Token: 0x17000C43 RID: 3139
		// (get) Token: 0x060048F0 RID: 18672 RVA: 0x000FD0A3 File Offset: 0x000FB2A3
		public Type ObjectType
		{
			get
			{
				if (!this.m_current)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration has either not started or has already finished."));
				}
				return this.m_types[this.m_currItem];
			}
		}

		// Token: 0x060048F1 RID: 18673 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal SerializationInfoEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04002570 RID: 9584
		private string[] m_members;

		// Token: 0x04002571 RID: 9585
		private object[] m_data;

		// Token: 0x04002572 RID: 9586
		private Type[] m_types;

		// Token: 0x04002573 RID: 9587
		private int m_numItems;

		// Token: 0x04002574 RID: 9588
		private int m_currItem;

		// Token: 0x04002575 RID: 9589
		private bool m_current;
	}
}
