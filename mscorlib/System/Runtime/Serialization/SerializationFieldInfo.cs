﻿using System;
using System.Globalization;
using System.Reflection;
using System.Security;

namespace System.Runtime.Serialization
{
	// Token: 0x020006FF RID: 1791
	internal sealed class SerializationFieldInfo : FieldInfo
	{
		// Token: 0x17000C2B RID: 3115
		// (get) Token: 0x0600489C RID: 18588 RVA: 0x000FC37E File Offset: 0x000FA57E
		public override Module Module
		{
			get
			{
				return this.m_field.Module;
			}
		}

		// Token: 0x17000C2C RID: 3116
		// (get) Token: 0x0600489D RID: 18589 RVA: 0x000FC38B File Offset: 0x000FA58B
		public override int MetadataToken
		{
			get
			{
				return this.m_field.MetadataToken;
			}
		}

		// Token: 0x0600489E RID: 18590 RVA: 0x000FC398 File Offset: 0x000FA598
		internal SerializationFieldInfo(RuntimeFieldInfo field, string namePrefix)
		{
			this.m_field = field;
			this.m_serializationName = namePrefix + "+" + this.m_field.Name;
		}

		// Token: 0x17000C2D RID: 3117
		// (get) Token: 0x0600489F RID: 18591 RVA: 0x000FC3C3 File Offset: 0x000FA5C3
		public override string Name
		{
			get
			{
				return this.m_serializationName;
			}
		}

		// Token: 0x17000C2E RID: 3118
		// (get) Token: 0x060048A0 RID: 18592 RVA: 0x000FC3CB File Offset: 0x000FA5CB
		public override Type DeclaringType
		{
			get
			{
				return this.m_field.DeclaringType;
			}
		}

		// Token: 0x17000C2F RID: 3119
		// (get) Token: 0x060048A1 RID: 18593 RVA: 0x000FC3D8 File Offset: 0x000FA5D8
		public override Type ReflectedType
		{
			get
			{
				return this.m_field.ReflectedType;
			}
		}

		// Token: 0x060048A2 RID: 18594 RVA: 0x000FC3E5 File Offset: 0x000FA5E5
		public override object[] GetCustomAttributes(bool inherit)
		{
			return this.m_field.GetCustomAttributes(inherit);
		}

		// Token: 0x060048A3 RID: 18595 RVA: 0x000FC3F3 File Offset: 0x000FA5F3
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return this.m_field.GetCustomAttributes(attributeType, inherit);
		}

		// Token: 0x060048A4 RID: 18596 RVA: 0x000FC402 File Offset: 0x000FA602
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return this.m_field.IsDefined(attributeType, inherit);
		}

		// Token: 0x17000C30 RID: 3120
		// (get) Token: 0x060048A5 RID: 18597 RVA: 0x000FC411 File Offset: 0x000FA611
		public override Type FieldType
		{
			get
			{
				return this.m_field.FieldType;
			}
		}

		// Token: 0x060048A6 RID: 18598 RVA: 0x000FC41E File Offset: 0x000FA61E
		public override object GetValue(object obj)
		{
			return this.m_field.GetValue(obj);
		}

		// Token: 0x060048A7 RID: 18599 RVA: 0x000FC42C File Offset: 0x000FA62C
		[SecurityCritical]
		internal object InternalGetValue(object obj)
		{
			RtFieldInfo rtFieldInfo = this.m_field as RtFieldInfo;
			if (rtFieldInfo != null)
			{
				rtFieldInfo.CheckConsistency(obj);
				return rtFieldInfo.UnsafeGetValue(obj);
			}
			return this.m_field.GetValue(obj);
		}

		// Token: 0x060048A8 RID: 18600 RVA: 0x000FC469 File Offset: 0x000FA669
		public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, CultureInfo culture)
		{
			this.m_field.SetValue(obj, value, invokeAttr, binder, culture);
		}

		// Token: 0x060048A9 RID: 18601 RVA: 0x000FC480 File Offset: 0x000FA680
		[SecurityCritical]
		internal void InternalSetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, CultureInfo culture)
		{
			RtFieldInfo rtFieldInfo = this.m_field as RtFieldInfo;
			if (rtFieldInfo != null)
			{
				rtFieldInfo.CheckConsistency(obj);
				rtFieldInfo.UnsafeSetValue(obj, value, invokeAttr, binder, culture);
				return;
			}
			this.m_field.SetValue(obj, value, invokeAttr, binder, culture);
		}

		// Token: 0x17000C31 RID: 3121
		// (get) Token: 0x060048AA RID: 18602 RVA: 0x000FC4C9 File Offset: 0x000FA6C9
		internal RuntimeFieldInfo FieldInfo
		{
			get
			{
				return this.m_field;
			}
		}

		// Token: 0x17000C32 RID: 3122
		// (get) Token: 0x060048AB RID: 18603 RVA: 0x000FC4D1 File Offset: 0x000FA6D1
		public override RuntimeFieldHandle FieldHandle
		{
			get
			{
				return this.m_field.FieldHandle;
			}
		}

		// Token: 0x17000C33 RID: 3123
		// (get) Token: 0x060048AC RID: 18604 RVA: 0x000FC4DE File Offset: 0x000FA6DE
		public override FieldAttributes Attributes
		{
			get
			{
				return this.m_field.Attributes;
			}
		}

		// Token: 0x0400255B RID: 9563
		internal const string FakeNameSeparatorString = "+";

		// Token: 0x0400255C RID: 9564
		private RuntimeFieldInfo m_field;

		// Token: 0x0400255D RID: 9565
		private string m_serializationName;
	}
}
