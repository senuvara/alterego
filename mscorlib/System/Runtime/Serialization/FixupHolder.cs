﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020006ED RID: 1773
	[Serializable]
	internal class FixupHolder
	{
		// Token: 0x06004858 RID: 18520 RVA: 0x000FB736 File Offset: 0x000F9936
		internal FixupHolder(long id, object fixupInfo, int fixupType)
		{
			this.m_id = id;
			this.m_fixupInfo = fixupInfo;
			this.m_fixupType = fixupType;
		}

		// Token: 0x04002536 RID: 9526
		internal const int ArrayFixup = 1;

		// Token: 0x04002537 RID: 9527
		internal const int MemberFixup = 2;

		// Token: 0x04002538 RID: 9528
		internal const int DelayedFixup = 4;

		// Token: 0x04002539 RID: 9529
		internal long m_id;

		// Token: 0x0400253A RID: 9530
		internal object m_fixupInfo;

		// Token: 0x0400253B RID: 9531
		internal int m_fixupType;
	}
}
