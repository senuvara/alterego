﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020006DB RID: 1755
	// (Invoke) Token: 0x0600478B RID: 18315
	[Serializable]
	internal delegate void DeserializationEventHandler(object sender);
}
