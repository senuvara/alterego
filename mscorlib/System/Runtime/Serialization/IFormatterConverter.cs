﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	/// <summary>Provides the connection between an instance of <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and the formatter-provided class best suited to parse the data inside the <see cref="T:System.Runtime.Serialization.SerializationInfo" />.</summary>
	// Token: 0x020006E4 RID: 1764
	[CLSCompliant(false)]
	[ComVisible(true)]
	public interface IFormatterConverter
	{
		/// <summary>Converts a value to the given <see cref="T:System.Type" />.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <param name="type">The <see cref="T:System.Type" /> into which <paramref name="value" /> is to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047E9 RID: 18409
		object Convert(object value, Type type);

		/// <summary>Converts a value to the given <see cref="T:System.TypeCode" />.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <param name="typeCode">The <see cref="T:System.TypeCode" /> into which <paramref name="value" /> is to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047EA RID: 18410
		object Convert(object value, TypeCode typeCode);

		/// <summary>Converts a value to a <see cref="T:System.Boolean" />.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047EB RID: 18411
		bool ToBoolean(object value);

		/// <summary>Converts a value to a Unicode character.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047EC RID: 18412
		char ToChar(object value);

		/// <summary>Converts a value to a <see cref="T:System.SByte" />.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047ED RID: 18413
		sbyte ToSByte(object value);

		/// <summary>Converts a value to an 8-bit unsigned integer.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047EE RID: 18414
		byte ToByte(object value);

		/// <summary>Converts a value to a 16-bit signed integer.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047EF RID: 18415
		short ToInt16(object value);

		/// <summary>Converts a value to a 16-bit unsigned integer.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F0 RID: 18416
		ushort ToUInt16(object value);

		/// <summary>Converts a value to a 32-bit signed integer.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F1 RID: 18417
		int ToInt32(object value);

		/// <summary>Converts a value to a 32-bit unsigned integer.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F2 RID: 18418
		uint ToUInt32(object value);

		/// <summary>Converts a value to a 64-bit signed integer.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F3 RID: 18419
		long ToInt64(object value);

		/// <summary>Converts a value to a 64-bit unsigned integer.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F4 RID: 18420
		ulong ToUInt64(object value);

		/// <summary>Converts a value to a single-precision floating-point number.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F5 RID: 18421
		float ToSingle(object value);

		/// <summary>Converts a value to a double-precision floating-point number.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F6 RID: 18422
		double ToDouble(object value);

		/// <summary>Converts a value to a <see cref="T:System.Decimal" />.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F7 RID: 18423
		decimal ToDecimal(object value);

		/// <summary>Converts a value to a <see cref="T:System.DateTime" />.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F8 RID: 18424
		DateTime ToDateTime(object value);

		/// <summary>Converts a value to a <see cref="T:System.String" />.</summary>
		/// <param name="value">The object to be converted. </param>
		/// <returns>The converted <paramref name="value" />.</returns>
		// Token: 0x060047F9 RID: 18425
		string ToString(object value);
	}
}
