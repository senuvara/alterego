﻿using System;
using System.Reflection;

namespace System.Runtime.Serialization
{
	// Token: 0x02000709 RID: 1801
	internal class ValueTypeFixupInfo
	{
		// Token: 0x06004907 RID: 18695 RVA: 0x000FD4CC File Offset: 0x000FB6CC
		public ValueTypeFixupInfo(long containerID, FieldInfo member, int[] parentIndex)
		{
			if (member == null && parentIndex == null)
			{
				throw new ArgumentException(Environment.GetResourceString("When supplying the ID of a containing object, the FieldInfo that identifies the current field within that object must also be supplied."));
			}
			if (containerID == 0L && member == null)
			{
				this.m_containerID = containerID;
				this.m_parentField = member;
				this.m_parentIndex = parentIndex;
			}
			if (member != null)
			{
				if (parentIndex != null)
				{
					throw new ArgumentException(Environment.GetResourceString("Cannot supply both a MemberInfo and an Array to indicate the parent of a value type."));
				}
				if (member.FieldType.IsValueType && containerID == 0L)
				{
					throw new ArgumentException(Environment.GetResourceString("When supplying a FieldInfo for fixing up a nested type, a valid ID for that containing object must also be supplied."));
				}
			}
			this.m_containerID = containerID;
			this.m_parentField = member;
			this.m_parentIndex = parentIndex;
		}

		// Token: 0x17000C46 RID: 3142
		// (get) Token: 0x06004908 RID: 18696 RVA: 0x000FD56D File Offset: 0x000FB76D
		public long ContainerID
		{
			get
			{
				return this.m_containerID;
			}
		}

		// Token: 0x17000C47 RID: 3143
		// (get) Token: 0x06004909 RID: 18697 RVA: 0x000FD575 File Offset: 0x000FB775
		public FieldInfo ParentField
		{
			get
			{
				return this.m_parentField;
			}
		}

		// Token: 0x17000C48 RID: 3144
		// (get) Token: 0x0600490A RID: 18698 RVA: 0x000FD57D File Offset: 0x000FB77D
		public int[] ParentIndex
		{
			get
			{
				return this.m_parentIndex;
			}
		}

		// Token: 0x04002589 RID: 9609
		private long m_containerID;

		// Token: 0x0400258A RID: 9610
		private FieldInfo m_parentField;

		// Token: 0x0400258B RID: 9611
		private int[] m_parentIndex;
	}
}
