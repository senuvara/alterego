﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	/// <summary>Holds the value, <see cref="T:System.Type" />, and name of a serialized object. </summary>
	// Token: 0x02000701 RID: 1793
	[ComVisible(true)]
	public struct SerializationEntry
	{
		/// <summary>Gets the value contained in the object.</summary>
		/// <returns>The value contained in the object.</returns>
		// Token: 0x17000C3C RID: 3132
		// (get) Token: 0x060048E5 RID: 18661 RVA: 0x000FCF02 File Offset: 0x000FB102
		public object Value
		{
			get
			{
				return this.m_value;
			}
		}

		/// <summary>Gets the name of the object.</summary>
		/// <returns>The name of the object.</returns>
		// Token: 0x17000C3D RID: 3133
		// (get) Token: 0x060048E6 RID: 18662 RVA: 0x000FCF0A File Offset: 0x000FB10A
		public string Name
		{
			get
			{
				return this.m_name;
			}
		}

		/// <summary>Gets the <see cref="T:System.Type" /> of the object.</summary>
		/// <returns>The <see cref="T:System.Type" /> of the object.</returns>
		// Token: 0x17000C3E RID: 3134
		// (get) Token: 0x060048E7 RID: 18663 RVA: 0x000FCF12 File Offset: 0x000FB112
		public Type ObjectType
		{
			get
			{
				return this.m_type;
			}
		}

		// Token: 0x060048E8 RID: 18664 RVA: 0x000FCF1A File Offset: 0x000FB11A
		internal SerializationEntry(string entryName, object entryValue, Type entryType)
		{
			this.m_value = entryValue;
			this.m_name = entryName;
			this.m_type = entryType;
		}

		// Token: 0x0400256D RID: 9581
		private Type m_type;

		// Token: 0x0400256E RID: 9582
		private object m_value;

		// Token: 0x0400256F RID: 9583
		private string m_name;
	}
}
