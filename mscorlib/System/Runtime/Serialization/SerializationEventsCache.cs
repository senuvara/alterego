﻿using System;
using System.Collections;

namespace System.Runtime.Serialization
{
	// Token: 0x020006FD RID: 1789
	internal static class SerializationEventsCache
	{
		// Token: 0x06004895 RID: 18581 RVA: 0x000FC2A4 File Offset: 0x000FA4A4
		internal static SerializationEvents GetSerializationEventsForType(Type t)
		{
			SerializationEvents serializationEvents;
			if ((serializationEvents = (SerializationEvents)SerializationEventsCache.cache[t]) == null)
			{
				object syncRoot = SerializationEventsCache.cache.SyncRoot;
				lock (syncRoot)
				{
					if ((serializationEvents = (SerializationEvents)SerializationEventsCache.cache[t]) == null)
					{
						serializationEvents = new SerializationEvents(t);
						SerializationEventsCache.cache[t] = serializationEvents;
					}
				}
			}
			return serializationEvents;
		}

		// Token: 0x06004896 RID: 18582 RVA: 0x000FC320 File Offset: 0x000FA520
		// Note: this type is marked as 'beforefieldinit'.
		static SerializationEventsCache()
		{
		}

		// Token: 0x04002559 RID: 9561
		private static Hashtable cache = new Hashtable();
	}
}
