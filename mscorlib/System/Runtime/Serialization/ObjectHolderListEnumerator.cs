﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020006F1 RID: 1777
	internal class ObjectHolderListEnumerator
	{
		// Token: 0x0600486E RID: 18542 RVA: 0x000FBAAE File Offset: 0x000F9CAE
		internal ObjectHolderListEnumerator(ObjectHolderList list, bool isFixupEnumerator)
		{
			this.m_list = list;
			this.m_startingVersion = this.m_list.Version;
			this.m_currPos = -1;
			this.m_isFixupEnumerator = isFixupEnumerator;
		}

		// Token: 0x0600486F RID: 18543 RVA: 0x000FBADC File Offset: 0x000F9CDC
		internal bool MoveNext()
		{
			if (this.m_isFixupEnumerator)
			{
				int num;
				do
				{
					num = this.m_currPos + 1;
					this.m_currPos = num;
				}
				while (num < this.m_list.Count && this.m_list.m_values[this.m_currPos].CompletelyFixed);
				return this.m_currPos != this.m_list.Count;
			}
			this.m_currPos++;
			return this.m_currPos != this.m_list.Count;
		}

		// Token: 0x17000C24 RID: 3108
		// (get) Token: 0x06004870 RID: 18544 RVA: 0x000FBB63 File Offset: 0x000F9D63
		internal ObjectHolder Current
		{
			get
			{
				return this.m_list.m_values[this.m_currPos];
			}
		}

		// Token: 0x04002547 RID: 9543
		private bool m_isFixupEnumerator;

		// Token: 0x04002548 RID: 9544
		private ObjectHolderList m_list;

		// Token: 0x04002549 RID: 9545
		private int m_startingVersion;

		// Token: 0x0400254A RID: 9546
		private int m_currPos;
	}
}
