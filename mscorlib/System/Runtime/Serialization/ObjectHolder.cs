﻿using System;
using System.Reflection;
using System.Security;

namespace System.Runtime.Serialization
{
	// Token: 0x020006EC RID: 1772
	internal sealed class ObjectHolder
	{
		// Token: 0x0600482F RID: 18479 RVA: 0x000FB1D6 File Offset: 0x000F93D6
		internal ObjectHolder(long objID) : this(null, objID, null, null, 0L, null, null)
		{
		}

		// Token: 0x06004830 RID: 18480 RVA: 0x000FB1E8 File Offset: 0x000F93E8
		internal ObjectHolder(object obj, long objID, SerializationInfo info, ISerializationSurrogate surrogate, long idOfContainingObj, FieldInfo field, int[] arrayIndex)
		{
			this.m_object = obj;
			this.m_id = objID;
			this.m_flags = 0;
			this.m_missingElementsRemaining = 0;
			this.m_missingDecendents = 0;
			this.m_dependentObjects = null;
			this.m_next = null;
			this.m_serInfo = info;
			this.m_surrogate = surrogate;
			this.m_markForFixupWhenAvailable = false;
			if (obj is TypeLoadExceptionHolder)
			{
				this.m_typeLoad = (TypeLoadExceptionHolder)obj;
			}
			if (idOfContainingObj != 0L && ((field != null && field.FieldType.IsValueType) || arrayIndex != null))
			{
				if (idOfContainingObj == objID)
				{
					throw new SerializationException(Environment.GetResourceString("The ID of the containing object cannot be the same as the object ID."));
				}
				this.m_valueFixup = new ValueTypeFixupInfo(idOfContainingObj, field, arrayIndex);
			}
			this.SetFlags();
		}

		// Token: 0x06004831 RID: 18481 RVA: 0x000FB2A4 File Offset: 0x000F94A4
		internal ObjectHolder(string obj, long objID, SerializationInfo info, ISerializationSurrogate surrogate, long idOfContainingObj, FieldInfo field, int[] arrayIndex)
		{
			this.m_object = obj;
			this.m_id = objID;
			this.m_flags = 0;
			this.m_missingElementsRemaining = 0;
			this.m_missingDecendents = 0;
			this.m_dependentObjects = null;
			this.m_next = null;
			this.m_serInfo = info;
			this.m_surrogate = surrogate;
			this.m_markForFixupWhenAvailable = false;
			if (idOfContainingObj != 0L && arrayIndex != null)
			{
				this.m_valueFixup = new ValueTypeFixupInfo(idOfContainingObj, field, arrayIndex);
			}
			if (this.m_valueFixup != null)
			{
				this.m_flags |= 8;
			}
		}

		// Token: 0x06004832 RID: 18482 RVA: 0x000FB32D File Offset: 0x000F952D
		private void IncrementDescendentFixups(int amount)
		{
			this.m_missingDecendents += amount;
		}

		// Token: 0x06004833 RID: 18483 RVA: 0x000FB33D File Offset: 0x000F953D
		internal void DecrementFixupsRemaining(ObjectManager manager)
		{
			this.m_missingElementsRemaining--;
			if (this.RequiresValueTypeFixup)
			{
				this.UpdateDescendentDependencyChain(-1, manager);
			}
		}

		// Token: 0x06004834 RID: 18484 RVA: 0x000FB35D File Offset: 0x000F955D
		internal void RemoveDependency(long id)
		{
			this.m_dependentObjects.RemoveElement(id);
		}

		// Token: 0x06004835 RID: 18485 RVA: 0x000FB36C File Offset: 0x000F956C
		internal void AddFixup(FixupHolder fixup, ObjectManager manager)
		{
			if (this.m_missingElements == null)
			{
				this.m_missingElements = new FixupHolderList();
			}
			this.m_missingElements.Add(fixup);
			this.m_missingElementsRemaining++;
			if (this.RequiresValueTypeFixup)
			{
				this.UpdateDescendentDependencyChain(1, manager);
			}
		}

		// Token: 0x06004836 RID: 18486 RVA: 0x000FB3AC File Offset: 0x000F95AC
		private void UpdateDescendentDependencyChain(int amount, ObjectManager manager)
		{
			ObjectHolder objectHolder = this;
			do
			{
				objectHolder = manager.FindOrCreateObjectHolder(objectHolder.ContainerID);
				objectHolder.IncrementDescendentFixups(amount);
			}
			while (objectHolder.RequiresValueTypeFixup);
		}

		// Token: 0x06004837 RID: 18487 RVA: 0x000FB3D7 File Offset: 0x000F95D7
		internal void AddDependency(long dependentObject)
		{
			if (this.m_dependentObjects == null)
			{
				this.m_dependentObjects = new LongList();
			}
			this.m_dependentObjects.Add(dependentObject);
		}

		// Token: 0x06004838 RID: 18488 RVA: 0x000FB3F8 File Offset: 0x000F95F8
		[SecurityCritical]
		internal void UpdateData(object obj, SerializationInfo info, ISerializationSurrogate surrogate, long idOfContainer, FieldInfo field, int[] arrayIndex, ObjectManager manager)
		{
			this.SetObjectValue(obj, manager);
			this.m_serInfo = info;
			this.m_surrogate = surrogate;
			if (idOfContainer != 0L && ((field != null && field.FieldType.IsValueType) || arrayIndex != null))
			{
				if (idOfContainer == this.m_id)
				{
					throw new SerializationException(Environment.GetResourceString("The ID of the containing object cannot be the same as the object ID."));
				}
				this.m_valueFixup = new ValueTypeFixupInfo(idOfContainer, field, arrayIndex);
			}
			this.SetFlags();
			if (this.RequiresValueTypeFixup)
			{
				this.UpdateDescendentDependencyChain(this.m_missingElementsRemaining, manager);
			}
		}

		// Token: 0x06004839 RID: 18489 RVA: 0x000FB483 File Offset: 0x000F9683
		internal void MarkForCompletionWhenAvailable()
		{
			this.m_markForFixupWhenAvailable = true;
		}

		// Token: 0x0600483A RID: 18490 RVA: 0x000FB48C File Offset: 0x000F968C
		internal void SetFlags()
		{
			if (this.m_object is IObjectReference)
			{
				this.m_flags |= 1;
			}
			this.m_flags &= -7;
			if (this.m_surrogate != null)
			{
				this.m_flags |= 4;
			}
			else if (this.m_object is ISerializable)
			{
				this.m_flags |= 2;
			}
			if (this.m_valueFixup != null)
			{
				this.m_flags |= 8;
			}
		}

		// Token: 0x17000C0B RID: 3083
		// (get) Token: 0x0600483B RID: 18491 RVA: 0x000FB50C File Offset: 0x000F970C
		// (set) Token: 0x0600483C RID: 18492 RVA: 0x000FB519 File Offset: 0x000F9719
		internal bool IsIncompleteObjectReference
		{
			get
			{
				return (this.m_flags & 1) != 0;
			}
			set
			{
				if (value)
				{
					this.m_flags |= 1;
					return;
				}
				this.m_flags &= -2;
			}
		}

		// Token: 0x17000C0C RID: 3084
		// (get) Token: 0x0600483D RID: 18493 RVA: 0x000FB53C File Offset: 0x000F973C
		internal bool RequiresDelayedFixup
		{
			get
			{
				return (this.m_flags & 7) != 0;
			}
		}

		// Token: 0x17000C0D RID: 3085
		// (get) Token: 0x0600483E RID: 18494 RVA: 0x000FB549 File Offset: 0x000F9749
		internal bool RequiresValueTypeFixup
		{
			get
			{
				return (this.m_flags & 8) != 0;
			}
		}

		// Token: 0x17000C0E RID: 3086
		// (get) Token: 0x0600483F RID: 18495 RVA: 0x000FB556 File Offset: 0x000F9756
		// (set) Token: 0x06004840 RID: 18496 RVA: 0x000FB58A File Offset: 0x000F978A
		internal bool ValueTypeFixupPerformed
		{
			get
			{
				return (this.m_flags & 32768) != 0 || (this.m_object != null && (this.m_dependentObjects == null || this.m_dependentObjects.Count == 0));
			}
			set
			{
				if (value)
				{
					this.m_flags |= 32768;
				}
			}
		}

		// Token: 0x17000C0F RID: 3087
		// (get) Token: 0x06004841 RID: 18497 RVA: 0x000FB5A1 File Offset: 0x000F97A1
		internal bool HasISerializable
		{
			get
			{
				return (this.m_flags & 2) != 0;
			}
		}

		// Token: 0x17000C10 RID: 3088
		// (get) Token: 0x06004842 RID: 18498 RVA: 0x000FB5AE File Offset: 0x000F97AE
		internal bool HasSurrogate
		{
			get
			{
				return (this.m_flags & 4) != 0;
			}
		}

		// Token: 0x17000C11 RID: 3089
		// (get) Token: 0x06004843 RID: 18499 RVA: 0x000FB5BB File Offset: 0x000F97BB
		internal bool CanSurrogatedObjectValueChange
		{
			get
			{
				return this.m_surrogate == null || this.m_surrogate.GetType() != typeof(SurrogateForCyclicalReference);
			}
		}

		// Token: 0x17000C12 RID: 3090
		// (get) Token: 0x06004844 RID: 18500 RVA: 0x000FB5E1 File Offset: 0x000F97E1
		internal bool CanObjectValueChange
		{
			get
			{
				return this.IsIncompleteObjectReference || (this.HasSurrogate && this.CanSurrogatedObjectValueChange);
			}
		}

		// Token: 0x17000C13 RID: 3091
		// (get) Token: 0x06004845 RID: 18501 RVA: 0x000FB5FD File Offset: 0x000F97FD
		internal int DirectlyDependentObjects
		{
			get
			{
				return this.m_missingElementsRemaining;
			}
		}

		// Token: 0x17000C14 RID: 3092
		// (get) Token: 0x06004846 RID: 18502 RVA: 0x000FB605 File Offset: 0x000F9805
		internal int TotalDependentObjects
		{
			get
			{
				return this.m_missingElementsRemaining + this.m_missingDecendents;
			}
		}

		// Token: 0x17000C15 RID: 3093
		// (get) Token: 0x06004847 RID: 18503 RVA: 0x000FB614 File Offset: 0x000F9814
		// (set) Token: 0x06004848 RID: 18504 RVA: 0x000FB61C File Offset: 0x000F981C
		internal bool Reachable
		{
			get
			{
				return this.m_reachable;
			}
			set
			{
				this.m_reachable = value;
			}
		}

		// Token: 0x17000C16 RID: 3094
		// (get) Token: 0x06004849 RID: 18505 RVA: 0x000FB625 File Offset: 0x000F9825
		internal bool TypeLoadExceptionReachable
		{
			get
			{
				return this.m_typeLoad != null;
			}
		}

		// Token: 0x17000C17 RID: 3095
		// (get) Token: 0x0600484A RID: 18506 RVA: 0x000FB630 File Offset: 0x000F9830
		// (set) Token: 0x0600484B RID: 18507 RVA: 0x000FB638 File Offset: 0x000F9838
		internal TypeLoadExceptionHolder TypeLoadException
		{
			get
			{
				return this.m_typeLoad;
			}
			set
			{
				this.m_typeLoad = value;
			}
		}

		// Token: 0x17000C18 RID: 3096
		// (get) Token: 0x0600484C RID: 18508 RVA: 0x000FB641 File Offset: 0x000F9841
		internal object ObjectValue
		{
			get
			{
				return this.m_object;
			}
		}

		// Token: 0x0600484D RID: 18509 RVA: 0x000FB649 File Offset: 0x000F9849
		[SecurityCritical]
		internal void SetObjectValue(object obj, ObjectManager manager)
		{
			this.m_object = obj;
			if (obj == manager.TopObject)
			{
				this.m_reachable = true;
			}
			if (obj is TypeLoadExceptionHolder)
			{
				this.m_typeLoad = (TypeLoadExceptionHolder)obj;
			}
			if (this.m_markForFixupWhenAvailable)
			{
				manager.CompleteObject(this, true);
			}
		}

		// Token: 0x17000C19 RID: 3097
		// (get) Token: 0x0600484E RID: 18510 RVA: 0x000FB686 File Offset: 0x000F9886
		// (set) Token: 0x0600484F RID: 18511 RVA: 0x000FB68E File Offset: 0x000F988E
		internal SerializationInfo SerializationInfo
		{
			get
			{
				return this.m_serInfo;
			}
			set
			{
				this.m_serInfo = value;
			}
		}

		// Token: 0x17000C1A RID: 3098
		// (get) Token: 0x06004850 RID: 18512 RVA: 0x000FB697 File Offset: 0x000F9897
		internal ISerializationSurrogate Surrogate
		{
			get
			{
				return this.m_surrogate;
			}
		}

		// Token: 0x17000C1B RID: 3099
		// (get) Token: 0x06004851 RID: 18513 RVA: 0x000FB69F File Offset: 0x000F989F
		// (set) Token: 0x06004852 RID: 18514 RVA: 0x000FB6A7 File Offset: 0x000F98A7
		internal LongList DependentObjects
		{
			get
			{
				return this.m_dependentObjects;
			}
			set
			{
				this.m_dependentObjects = value;
			}
		}

		// Token: 0x17000C1C RID: 3100
		// (get) Token: 0x06004853 RID: 18515 RVA: 0x000FB6B0 File Offset: 0x000F98B0
		// (set) Token: 0x06004854 RID: 18516 RVA: 0x000FB6D7 File Offset: 0x000F98D7
		internal bool RequiresSerInfoFixup
		{
			get
			{
				return ((this.m_flags & 4) != 0 || (this.m_flags & 2) != 0) && (this.m_flags & 16384) == 0;
			}
			set
			{
				if (!value)
				{
					this.m_flags |= 16384;
					return;
				}
				this.m_flags &= -16385;
			}
		}

		// Token: 0x17000C1D RID: 3101
		// (get) Token: 0x06004855 RID: 18517 RVA: 0x000FB701 File Offset: 0x000F9901
		internal ValueTypeFixupInfo ValueFixup
		{
			get
			{
				return this.m_valueFixup;
			}
		}

		// Token: 0x17000C1E RID: 3102
		// (get) Token: 0x06004856 RID: 18518 RVA: 0x000FB709 File Offset: 0x000F9909
		internal bool CompletelyFixed
		{
			get
			{
				return !this.RequiresSerInfoFixup && !this.IsIncompleteObjectReference;
			}
		}

		// Token: 0x17000C1F RID: 3103
		// (get) Token: 0x06004857 RID: 18519 RVA: 0x000FB71E File Offset: 0x000F991E
		internal long ContainerID
		{
			get
			{
				if (this.m_valueFixup != null)
				{
					return this.m_valueFixup.ContainerID;
				}
				return 0L;
			}
		}

		// Token: 0x04002521 RID: 9505
		internal const int INCOMPLETE_OBJECT_REFERENCE = 1;

		// Token: 0x04002522 RID: 9506
		internal const int HAS_ISERIALIZABLE = 2;

		// Token: 0x04002523 RID: 9507
		internal const int HAS_SURROGATE = 4;

		// Token: 0x04002524 RID: 9508
		internal const int REQUIRES_VALUETYPE_FIXUP = 8;

		// Token: 0x04002525 RID: 9509
		internal const int REQUIRES_DELAYED_FIXUP = 7;

		// Token: 0x04002526 RID: 9510
		internal const int SER_INFO_FIXED = 16384;

		// Token: 0x04002527 RID: 9511
		internal const int VALUETYPE_FIXUP_PERFORMED = 32768;

		// Token: 0x04002528 RID: 9512
		private object m_object;

		// Token: 0x04002529 RID: 9513
		internal long m_id;

		// Token: 0x0400252A RID: 9514
		private int m_missingElementsRemaining;

		// Token: 0x0400252B RID: 9515
		private int m_missingDecendents;

		// Token: 0x0400252C RID: 9516
		internal SerializationInfo m_serInfo;

		// Token: 0x0400252D RID: 9517
		internal ISerializationSurrogate m_surrogate;

		// Token: 0x0400252E RID: 9518
		internal FixupHolderList m_missingElements;

		// Token: 0x0400252F RID: 9519
		internal LongList m_dependentObjects;

		// Token: 0x04002530 RID: 9520
		internal ObjectHolder m_next;

		// Token: 0x04002531 RID: 9521
		internal int m_flags;

		// Token: 0x04002532 RID: 9522
		private bool m_markForFixupWhenAvailable;

		// Token: 0x04002533 RID: 9523
		private ValueTypeFixupInfo m_valueFixup;

		// Token: 0x04002534 RID: 9524
		private TypeLoadExceptionHolder m_typeLoad;

		// Token: 0x04002535 RID: 9525
		private bool m_reachable;
	}
}
