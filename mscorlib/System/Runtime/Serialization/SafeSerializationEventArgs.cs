﻿using System;
using System.Collections.Generic;
using Unity;

namespace System.Runtime.Serialization
{
	/// <summary>Provides data for the <see cref="E:System.Exception.SerializeObjectState" /> event.</summary>
	// Token: 0x020006F3 RID: 1779
	public sealed class SafeSerializationEventArgs : EventArgs
	{
		// Token: 0x06004873 RID: 18547 RVA: 0x000FBB8E File Offset: 0x000F9D8E
		internal SafeSerializationEventArgs(StreamingContext streamingContext)
		{
			this.m_serializedStates = new List<object>();
			base..ctor();
			this.m_streamingContext = streamingContext;
		}

		/// <summary>Stores the state of the exception.</summary>
		/// <param name="serializedState">A state object that is serialized with the instance.</param>
		// Token: 0x06004874 RID: 18548 RVA: 0x000FBBA8 File Offset: 0x000F9DA8
		public void AddSerializedState(ISafeSerializationData serializedState)
		{
			if (serializedState == null)
			{
				throw new ArgumentNullException("serializedState");
			}
			if (!serializedState.GetType().IsSerializable)
			{
				throw new ArgumentException(Environment.GetResourceString("Type '{0}' in Assembly '{1}' is not marked as serializable.", new object[]
				{
					serializedState.GetType(),
					serializedState.GetType().Assembly.FullName
				}));
			}
			this.m_serializedStates.Add(serializedState);
		}

		// Token: 0x17000C26 RID: 3110
		// (get) Token: 0x06004875 RID: 18549 RVA: 0x000FBC0E File Offset: 0x000F9E0E
		internal IList<object> SerializedStates
		{
			get
			{
				return this.m_serializedStates;
			}
		}

		/// <summary>Gets or sets an object that describes the source and destination of a serialized stream.</summary>
		/// <returns>An object that describes the source and destination of a serialized stream.</returns>
		// Token: 0x17000C27 RID: 3111
		// (get) Token: 0x06004876 RID: 18550 RVA: 0x000FBC16 File Offset: 0x000F9E16
		public StreamingContext StreamingContext
		{
			get
			{
				return this.m_streamingContext;
			}
		}

		// Token: 0x06004877 RID: 18551 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal SafeSerializationEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400254C RID: 9548
		private StreamingContext m_streamingContext;

		// Token: 0x0400254D RID: 9549
		private List<object> m_serializedStates;
	}
}
