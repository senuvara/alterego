﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020006DC RID: 1756
	// (Invoke) Token: 0x0600478F RID: 18319
	[Serializable]
	internal delegate void SerializationEventHandler(StreamingContext context);
}
