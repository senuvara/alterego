﻿using System;
using System.Security;

namespace System.Runtime.Serialization
{
	// Token: 0x020006E1 RID: 1761
	internal sealed class SurrogateForCyclicalReference : ISerializationSurrogate
	{
		// Token: 0x060047DD RID: 18397 RVA: 0x000F9EAD File Offset: 0x000F80AD
		internal SurrogateForCyclicalReference(ISerializationSurrogate innerSurrogate)
		{
			if (innerSurrogate == null)
			{
				throw new ArgumentNullException("innerSurrogate");
			}
			this.innerSurrogate = innerSurrogate;
		}

		// Token: 0x060047DE RID: 18398 RVA: 0x000F9ECA File Offset: 0x000F80CA
		[SecurityCritical]
		public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
		{
			this.innerSurrogate.GetObjectData(obj, info, context);
		}

		// Token: 0x060047DF RID: 18399 RVA: 0x000F9EDA File Offset: 0x000F80DA
		[SecurityCritical]
		public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
		{
			return this.innerSurrogate.SetObjectData(obj, info, context, selector);
		}

		// Token: 0x0400250B RID: 9483
		private ISerializationSurrogate innerSurrogate;
	}
}
