﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020006F2 RID: 1778
	internal class TypeLoadExceptionHolder
	{
		// Token: 0x06004871 RID: 18545 RVA: 0x000FBB77 File Offset: 0x000F9D77
		internal TypeLoadExceptionHolder(string typeName)
		{
			this.m_typeName = typeName;
		}

		// Token: 0x17000C25 RID: 3109
		// (get) Token: 0x06004872 RID: 18546 RVA: 0x000FBB86 File Offset: 0x000F9D86
		internal string TypeName
		{
			get
			{
				return this.m_typeName;
			}
		}

		// Token: 0x0400254B RID: 9547
		private string m_typeName;
	}
}
