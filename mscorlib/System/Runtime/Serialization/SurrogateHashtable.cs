﻿using System;
using System.Collections;

namespace System.Runtime.Serialization
{
	// Token: 0x02000708 RID: 1800
	internal class SurrogateHashtable : Hashtable
	{
		// Token: 0x06004905 RID: 18693 RVA: 0x000FD454 File Offset: 0x000FB654
		internal SurrogateHashtable(int size) : base(size)
		{
		}

		// Token: 0x06004906 RID: 18694 RVA: 0x000FD460 File Offset: 0x000FB660
		protected override bool KeyEquals(object key, object item)
		{
			SurrogateKey surrogateKey = (SurrogateKey)item;
			SurrogateKey surrogateKey2 = (SurrogateKey)key;
			return surrogateKey2.m_type == surrogateKey.m_type && (surrogateKey2.m_context.m_state & surrogateKey.m_context.m_state) == surrogateKey.m_context.m_state && surrogateKey2.m_context.Context == surrogateKey.m_context.Context;
		}
	}
}
