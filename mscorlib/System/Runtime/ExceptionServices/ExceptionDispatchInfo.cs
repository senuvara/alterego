﻿using System;
using System.Diagnostics;
using Unity;

namespace System.Runtime.ExceptionServices
{
	/// <summary>Represents an exception whose state is captured at a certain point in code. </summary>
	// Token: 0x0200083A RID: 2106
	public sealed class ExceptionDispatchInfo
	{
		// Token: 0x06005155 RID: 20821 RVA: 0x00115D18 File Offset: 0x00113F18
		private ExceptionDispatchInfo(Exception exception)
		{
			this.m_Exception = exception;
			StackTrace[] captured_traces = exception.captured_traces;
			int num = (captured_traces == null) ? 0 : captured_traces.Length;
			StackTrace[] array = new StackTrace[num + 1];
			if (num != 0)
			{
				Array.Copy(captured_traces, 0, array, 0, num);
			}
			array[num] = new StackTrace(exception, 0, true);
			this.m_stackTrace = array;
		}

		// Token: 0x17000E30 RID: 3632
		// (get) Token: 0x06005156 RID: 20822 RVA: 0x00115D6B File Offset: 0x00113F6B
		internal object BinaryStackTraceArray
		{
			get
			{
				return this.m_stackTrace;
			}
		}

		/// <summary>Creates an <see cref="T:System.Runtime.ExceptionServices.ExceptionDispatchInfo" /> object that represents the specified exception at the current point in code. </summary>
		/// <param name="source">The exception whose state is captured, and which is represented by the returned object. </param>
		/// <returns>An object that represents the specified exception at the current point in code. </returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="source" /> is <see langword="null" />. </exception>
		// Token: 0x06005157 RID: 20823 RVA: 0x00115D73 File Offset: 0x00113F73
		public static ExceptionDispatchInfo Capture(Exception source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source", Environment.GetResourceString("Object cannot be null."));
			}
			return new ExceptionDispatchInfo(source);
		}

		/// <summary>Gets the exception that is represented by the current instance. </summary>
		/// <returns>The exception that is represented by the current instance. </returns>
		// Token: 0x17000E31 RID: 3633
		// (get) Token: 0x06005158 RID: 20824 RVA: 0x00115D93 File Offset: 0x00113F93
		public Exception SourceException
		{
			get
			{
				return this.m_Exception;
			}
		}

		/// <summary>Throws the exception that is represented by the current <see cref="T:System.Runtime.ExceptionServices.ExceptionDispatchInfo" /> object, after restoring the state that was saved when the exception was captured. </summary>
		// Token: 0x06005159 RID: 20825 RVA: 0x00115D9B File Offset: 0x00113F9B
		public void Throw()
		{
			this.m_Exception.RestoreExceptionDispatchInfo(this);
			throw this.m_Exception;
		}

		// Token: 0x0600515A RID: 20826 RVA: 0x00115DAF File Offset: 0x00113FAF
		public static void Throw(Exception source)
		{
			ExceptionDispatchInfo.Capture(source).Throw();
		}

		// Token: 0x0600515B RID: 20827 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal ExceptionDispatchInfo()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040029A3 RID: 10659
		private Exception m_Exception;

		// Token: 0x040029A4 RID: 10660
		private object m_stackTrace;
	}
}
