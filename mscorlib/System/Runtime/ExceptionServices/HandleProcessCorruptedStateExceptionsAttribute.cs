﻿using System;

namespace System.Runtime.ExceptionServices
{
	/// <summary>Enables managed code to handle exceptions that indicate a corrupted process state.</summary>
	// Token: 0x02000838 RID: 2104
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public sealed class HandleProcessCorruptedStateExceptionsAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptionsAttribute" /> class. </summary>
		// Token: 0x06005152 RID: 20818 RVA: 0x000020BF File Offset: 0x000002BF
		public HandleProcessCorruptedStateExceptionsAttribute()
		{
		}
	}
}
