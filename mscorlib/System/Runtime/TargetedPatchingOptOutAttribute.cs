﻿using System;

namespace System.Runtime
{
	/// <summary>Indicates that the .NET Framework class library method to which this attribute is applied is unlikely to be affected by servicing releases, and therefore is eligible to be inlined across Native Image Generator (NGen) images.</summary>
	// Token: 0x020006C6 RID: 1734
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public sealed class TargetedPatchingOptOutAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.TargetedPatchingOptOutAttribute" /> class.</summary>
		/// <param name="reason">The reason why the method to which the <see cref="T:System.Runtime.TargetedPatchingOptOutAttribute" /> attribute is applied is considered to be eligible for inlining across Native Image Generator (NGen) images.</param>
		// Token: 0x06004756 RID: 18262 RVA: 0x000F8A9F File Offset: 0x000F6C9F
		public TargetedPatchingOptOutAttribute(string reason)
		{
			this.m_reason = reason;
		}

		/// <summary>Gets the reason why the method to which this attribute is applied is considered to be eligible for inlining across Native Image Generator (NGen) images.</summary>
		/// <returns>The reason why the method is considered to be eligible for inlining across NGen images.</returns>
		// Token: 0x17000BF5 RID: 3061
		// (get) Token: 0x06004757 RID: 18263 RVA: 0x000F8AAE File Offset: 0x000F6CAE
		public string Reason
		{
			get
			{
				return this.m_reason;
			}
		}

		// Token: 0x06004758 RID: 18264 RVA: 0x000020BF File Offset: 0x000002BF
		private TargetedPatchingOptOutAttribute()
		{
		}

		// Token: 0x040024CC RID: 9420
		private string m_reason;
	}
}
