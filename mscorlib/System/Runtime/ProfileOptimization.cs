﻿using System;
using System.Security;
using Unity;

namespace System.Runtime
{
	/// <summary>
	///     Improves the startup performance of application domains in applications that require the just-in-time (JIT) compiler by performing background compilation of methods that are likely to be executed, based on profiles created during previous compilations.</summary>
	// Token: 0x02000A98 RID: 2712
	public static class ProfileOptimization
	{
		/// <summary>Enables optimization profiling for the current application domain, and sets the folder where the optimization profile files are stored. On a single-core computer, the method is ignored. </summary>
		/// <param name="directoryPath">The full path to the folder where profile files are stored for the current application domain. </param>
		// Token: 0x06005ED1 RID: 24273 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecurityCritical]
		public static void SetProfileRoot(string directoryPath)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Starts just-in-time (JIT) compilation of the methods that were previously recorded in the specified profile file, on a background thread. Starts the process of recording current method use, which later overwrites the specified profile file. </summary>
		/// <param name="profile">The file name of the profile to use. </param>
		// Token: 0x06005ED2 RID: 24274 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecurityCritical]
		public static void StartProfile(string profile)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
