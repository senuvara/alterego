﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;

namespace System.Runtime
{
	/// <summary>Specifies the garbage collection settings for the current process. </summary>
	// Token: 0x020006C9 RID: 1737
	public static class GCSettings
	{
		/// <summary>Gets a value that indicates whether server garbage collection is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if server garbage collection is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BF6 RID: 3062
		// (get) Token: 0x06004759 RID: 18265 RVA: 0x00002526 File Offset: 0x00000726
		[MonoTODO("Always returns false")]
		public static bool IsServerGC
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets or sets the current latency mode for garbage collection.</summary>
		/// <returns>One of the enumeration values that specifies the latency mode. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.Runtime.GCSettings.LatencyMode" /> property is being set to an invalid value. -or-The <see cref="P:System.Runtime.GCSettings.LatencyMode" /> property cannot be set to <see cref="F:System.Runtime.GCLatencyMode.NoGCRegion" />. </exception>
		// Token: 0x17000BF7 RID: 3063
		// (get) Token: 0x0600475A RID: 18266 RVA: 0x00004E08 File Offset: 0x00003008
		// (set) Token: 0x0600475B RID: 18267 RVA: 0x000020D3 File Offset: 0x000002D3
		[MonoTODO("Always returns GCLatencyMode.Interactive and ignores set")]
		public static GCLatencyMode LatencyMode
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return GCLatencyMode.Interactive;
			}
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			set
			{
			}
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Gets or sets a value that indicates whether a full blocking garbage collection compacts the large object heap (LOH). </summary>
		/// <returns>One of the enumeration values that indicates whether a full blocking garbage collection compacts the LOH. </returns>
		// Token: 0x17000BF8 RID: 3064
		// (get) Token: 0x0600475C RID: 18268 RVA: 0x000F8AB6 File Offset: 0x000F6CB6
		// (set) Token: 0x0600475D RID: 18269 RVA: 0x000F8ABD File Offset: 0x000F6CBD
		public static GCLargeObjectHeapCompactionMode LargeObjectHeapCompactionMode
		{
			[CompilerGenerated]
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return GCSettings.<LargeObjectHeapCompactionMode>k__BackingField;
			}
			[CompilerGenerated]
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			set
			{
				GCSettings.<LargeObjectHeapCompactionMode>k__BackingField = value;
			}
		}

		// Token: 0x040024D6 RID: 9430
		[CompilerGenerated]
		private static GCLargeObjectHeapCompactionMode <LargeObjectHeapCompactionMode>k__BackingField;
	}
}
