﻿using System;

namespace System.Runtime.ConstrainedExecution
{
	/// <summary>Instructs the native image generation service to prepare a method for inclusion in a constrained execution region (CER).</summary>
	// Token: 0x0200083C RID: 2108
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, Inherited = false)]
	public sealed class PrePrepareMethodAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.ConstrainedExecution.PrePrepareMethodAttribute" /> class. </summary>
		// Token: 0x0600515E RID: 20830 RVA: 0x000020BF File Offset: 0x000002BF
		public PrePrepareMethodAttribute()
		{
		}
	}
}
