﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting
{
	// Token: 0x0200076E RID: 1902
	internal class ClientActivatedIdentity : ServerIdentity
	{
		// Token: 0x06004BEA RID: 19434 RVA: 0x0010A90A File Offset: 0x00108B0A
		public ClientActivatedIdentity(string objectUri, Type objectType) : base(objectUri, null, objectType)
		{
		}

		// Token: 0x06004BEB RID: 19435 RVA: 0x0010A915 File Offset: 0x00108B15
		public MarshalByRefObject GetServerObject()
		{
			return this._serverObject;
		}

		// Token: 0x06004BEC RID: 19436 RVA: 0x0010A91D File Offset: 0x00108B1D
		public MarshalByRefObject GetClientProxy()
		{
			return this._targetThis;
		}

		// Token: 0x06004BED RID: 19437 RVA: 0x0010A925 File Offset: 0x00108B25
		public void SetClientProxy(MarshalByRefObject obj)
		{
			this._targetThis = obj;
		}

		// Token: 0x06004BEE RID: 19438 RVA: 0x0010A92E File Offset: 0x00108B2E
		public override void OnLifetimeExpired()
		{
			base.OnLifetimeExpired();
			RemotingServices.DisposeIdentity(this);
		}

		// Token: 0x06004BEF RID: 19439 RVA: 0x0010A93C File Offset: 0x00108B3C
		public override IMessage SyncObjectProcessMessage(IMessage msg)
		{
			if (this._serverSink == null)
			{
				bool flag = this._targetThis != null;
				this._serverSink = this._context.CreateServerObjectSinkChain(flag ? this._targetThis : this._serverObject, flag);
			}
			return this._serverSink.SyncProcessMessage(msg);
		}

		// Token: 0x06004BF0 RID: 19440 RVA: 0x0010A98C File Offset: 0x00108B8C
		public override IMessageCtrl AsyncObjectProcessMessage(IMessage msg, IMessageSink replySink)
		{
			if (this._serverSink == null)
			{
				bool flag = this._targetThis != null;
				this._serverSink = this._context.CreateServerObjectSinkChain(flag ? this._targetThis : this._serverObject, flag);
			}
			return this._serverSink.AsyncProcessMessage(msg, replySink);
		}

		// Token: 0x0400280F RID: 10255
		private MarshalByRefObject _targetThis;
	}
}
