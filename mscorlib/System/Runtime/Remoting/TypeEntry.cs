﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	/// <summary>Implements a base class that holds the configuration information used to activate an instance of a remote type.</summary>
	// Token: 0x02000774 RID: 1908
	[ComVisible(true)]
	public class TypeEntry
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.TypeEntry" /> class.</summary>
		// Token: 0x06004C1D RID: 19485 RVA: 0x00002050 File Offset: 0x00000250
		protected TypeEntry()
		{
		}

		/// <summary>Gets the assembly name of the object type configured to be a remote-activated type.</summary>
		/// <returns>The assembly name of the object type configured to be a remote-activated type.</returns>
		// Token: 0x17000C9E RID: 3230
		// (get) Token: 0x06004C1E RID: 19486 RVA: 0x0010B408 File Offset: 0x00109608
		// (set) Token: 0x06004C1F RID: 19487 RVA: 0x0010B410 File Offset: 0x00109610
		public string AssemblyName
		{
			get
			{
				return this.assembly_name;
			}
			set
			{
				this.assembly_name = value;
			}
		}

		/// <summary>Gets the full type name of the object type configured to be a remote-activated type.</summary>
		/// <returns>The full type name of the object type configured to be a remote-activated type.</returns>
		// Token: 0x17000C9F RID: 3231
		// (get) Token: 0x06004C20 RID: 19488 RVA: 0x0010B419 File Offset: 0x00109619
		// (set) Token: 0x06004C21 RID: 19489 RVA: 0x0010B421 File Offset: 0x00109621
		public string TypeName
		{
			get
			{
				return this.type_name;
			}
			set
			{
				this.type_name = value;
			}
		}

		// Token: 0x04002819 RID: 10265
		private string assembly_name;

		// Token: 0x0400281A RID: 10266
		private string type_name;
	}
}
