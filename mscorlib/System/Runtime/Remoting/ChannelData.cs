﻿using System;
using System.Collections;

namespace System.Runtime.Remoting
{
	// Token: 0x02000765 RID: 1893
	internal class ChannelData
	{
		// Token: 0x17000C93 RID: 3219
		// (get) Token: 0x06004B8E RID: 19342 RVA: 0x00109321 File Offset: 0x00107521
		internal ArrayList ServerProviders
		{
			get
			{
				if (this._serverProviders == null)
				{
					this._serverProviders = new ArrayList();
				}
				return this._serverProviders;
			}
		}

		// Token: 0x17000C94 RID: 3220
		// (get) Token: 0x06004B8F RID: 19343 RVA: 0x0010933C File Offset: 0x0010753C
		public ArrayList ClientProviders
		{
			get
			{
				if (this._clientProviders == null)
				{
					this._clientProviders = new ArrayList();
				}
				return this._clientProviders;
			}
		}

		// Token: 0x17000C95 RID: 3221
		// (get) Token: 0x06004B90 RID: 19344 RVA: 0x00109357 File Offset: 0x00107557
		public Hashtable CustomProperties
		{
			get
			{
				if (this._customProperties == null)
				{
					this._customProperties = new Hashtable();
				}
				return this._customProperties;
			}
		}

		// Token: 0x06004B91 RID: 19345 RVA: 0x00109374 File Offset: 0x00107574
		public void CopyFrom(ChannelData other)
		{
			if (this.Ref == null)
			{
				this.Ref = other.Ref;
			}
			if (this.Id == null)
			{
				this.Id = other.Id;
			}
			if (this.Type == null)
			{
				this.Type = other.Type;
			}
			if (this.DelayLoadAsClientChannel == null)
			{
				this.DelayLoadAsClientChannel = other.DelayLoadAsClientChannel;
			}
			if (other._customProperties != null)
			{
				foreach (object obj in other._customProperties)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					if (!this.CustomProperties.ContainsKey(dictionaryEntry.Key))
					{
						this.CustomProperties[dictionaryEntry.Key] = dictionaryEntry.Value;
					}
				}
			}
			if (this._serverProviders == null && other._serverProviders != null)
			{
				foreach (object obj2 in other._serverProviders)
				{
					ProviderData other2 = (ProviderData)obj2;
					ProviderData providerData = new ProviderData();
					providerData.CopyFrom(other2);
					this.ServerProviders.Add(providerData);
				}
			}
			if (this._clientProviders == null && other._clientProviders != null)
			{
				foreach (object obj3 in other._clientProviders)
				{
					ProviderData other3 = (ProviderData)obj3;
					ProviderData providerData2 = new ProviderData();
					providerData2.CopyFrom(other3);
					this.ClientProviders.Add(providerData2);
				}
			}
		}

		// Token: 0x06004B92 RID: 19346 RVA: 0x0010952C File Offset: 0x0010772C
		public ChannelData()
		{
		}

		// Token: 0x040027F3 RID: 10227
		internal string Ref;

		// Token: 0x040027F4 RID: 10228
		internal string Type;

		// Token: 0x040027F5 RID: 10229
		internal string Id;

		// Token: 0x040027F6 RID: 10230
		internal string DelayLoadAsClientChannel;

		// Token: 0x040027F7 RID: 10231
		private ArrayList _serverProviders = new ArrayList();

		// Token: 0x040027F8 RID: 10232
		private ArrayList _clientProviders = new ArrayList();

		// Token: 0x040027F9 RID: 10233
		private Hashtable _customProperties = new Hashtable();
	}
}
