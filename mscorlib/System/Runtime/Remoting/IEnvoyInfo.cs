﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting
{
	/// <summary>Provides envoy information.</summary>
	// Token: 0x0200075B RID: 1883
	[ComVisible(true)]
	public interface IEnvoyInfo
	{
		/// <summary>Gets or sets the list of envoys that were contributed by the server context and object chains when the object was marshaled.</summary>
		/// <returns>A chain of envoy sinks.</returns>
		// Token: 0x17000C7B RID: 3195
		// (get) Token: 0x06004B12 RID: 19218
		// (set) Token: 0x06004B13 RID: 19219
		IMessageSink EnvoySinks { get; set; }
	}
}
