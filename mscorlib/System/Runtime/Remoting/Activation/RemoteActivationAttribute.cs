﻿using System;
using System.Collections;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x020007D1 RID: 2001
	internal class RemoteActivationAttribute : Attribute, IContextAttribute
	{
		// Token: 0x06004E0F RID: 19983 RVA: 0x000020BF File Offset: 0x000002BF
		public RemoteActivationAttribute()
		{
		}

		// Token: 0x06004E10 RID: 19984 RVA: 0x0010FB28 File Offset: 0x0010DD28
		public RemoteActivationAttribute(IList contextProperties)
		{
			this._contextProperties = contextProperties;
		}

		// Token: 0x06004E11 RID: 19985 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsContextOK(Context ctx, IConstructionCallMessage ctor)
		{
			return false;
		}

		// Token: 0x06004E12 RID: 19986 RVA: 0x0010FB38 File Offset: 0x0010DD38
		public void GetPropertiesForNewContext(IConstructionCallMessage ctor)
		{
			if (this._contextProperties != null)
			{
				foreach (object value in this._contextProperties)
				{
					ctor.ContextProperties.Add(value);
				}
			}
		}

		// Token: 0x040028AD RID: 10413
		private IList _contextProperties;
	}
}
