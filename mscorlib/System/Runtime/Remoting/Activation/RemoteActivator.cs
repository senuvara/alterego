﻿using System;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x020007D2 RID: 2002
	internal class RemoteActivator : MarshalByRefObject, IActivator
	{
		// Token: 0x06004E13 RID: 19987 RVA: 0x0010FB9C File Offset: 0x0010DD9C
		public IConstructionReturnMessage Activate(IConstructionCallMessage msg)
		{
			if (!RemotingConfiguration.IsActivationAllowed(msg.ActivationType))
			{
				throw new RemotingException("The type " + msg.ActivationTypeName + " is not allowed to be client activated");
			}
			object[] activationAttributes = null;
			if (msg.ActivationType.IsContextful)
			{
				activationAttributes = new object[]
				{
					new RemoteActivationAttribute(msg.ContextProperties)
				};
			}
			return new ConstructionResponse(RemotingServices.Marshal((MarshalByRefObject)Activator.CreateInstance(msg.ActivationType, msg.Args, activationAttributes)), null, msg);
		}

		// Token: 0x06004E14 RID: 19988 RVA: 0x0010FC18 File Offset: 0x0010DE18
		public override object InitializeLifetimeService()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();
			if (lease.CurrentState == LeaseState.Initial)
			{
				lease.InitialLeaseTime = TimeSpan.FromMinutes(30.0);
				lease.SponsorshipTimeout = TimeSpan.FromMinutes(1.0);
				lease.RenewOnCallTime = TimeSpan.FromMinutes(10.0);
			}
			return lease;
		}

		// Token: 0x17000D19 RID: 3353
		// (get) Token: 0x06004E15 RID: 19989 RVA: 0x000175EA File Offset: 0x000157EA
		public ActivatorLevel Level
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000D1A RID: 3354
		// (get) Token: 0x06004E16 RID: 19990 RVA: 0x000175EA File Offset: 0x000157EA
		// (set) Token: 0x06004E17 RID: 19991 RVA: 0x000175EA File Offset: 0x000157EA
		public IActivator NextActivator
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x06004E18 RID: 19992 RVA: 0x00035633 File Offset: 0x00033833
		public RemoteActivator()
		{
		}
	}
}
