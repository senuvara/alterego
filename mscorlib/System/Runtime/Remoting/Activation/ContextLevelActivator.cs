﻿using System;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x020007CD RID: 1997
	[Serializable]
	internal class ContextLevelActivator : IActivator
	{
		// Token: 0x06004E00 RID: 19968 RVA: 0x0010FA87 File Offset: 0x0010DC87
		public ContextLevelActivator(IActivator next)
		{
			this.m_NextActivator = next;
		}

		// Token: 0x17000D10 RID: 3344
		// (get) Token: 0x06004E01 RID: 19969 RVA: 0x000581CD File Offset: 0x000563CD
		public ActivatorLevel Level
		{
			get
			{
				return ActivatorLevel.Context;
			}
		}

		// Token: 0x17000D11 RID: 3345
		// (get) Token: 0x06004E02 RID: 19970 RVA: 0x0010FA96 File Offset: 0x0010DC96
		// (set) Token: 0x06004E03 RID: 19971 RVA: 0x0010FA9E File Offset: 0x0010DC9E
		public IActivator NextActivator
		{
			get
			{
				return this.m_NextActivator;
			}
			set
			{
				this.m_NextActivator = value;
			}
		}

		// Token: 0x06004E04 RID: 19972 RVA: 0x0010FAA8 File Offset: 0x0010DCA8
		public IConstructionReturnMessage Activate(IConstructionCallMessage ctorCall)
		{
			ServerIdentity serverIdentity = RemotingServices.CreateContextBoundObjectIdentity(ctorCall.ActivationType);
			RemotingServices.SetMessageTargetIdentity(ctorCall, serverIdentity);
			ConstructionCall constructionCall = ctorCall as ConstructionCall;
			if (constructionCall == null || !constructionCall.IsContextOk)
			{
				serverIdentity.Context = Context.CreateNewContext(ctorCall);
				Context newContext = Context.SwitchToContext(serverIdentity.Context);
				try
				{
					return this.m_NextActivator.Activate(ctorCall);
				}
				finally
				{
					Context.SwitchToContext(newContext);
				}
			}
			return this.m_NextActivator.Activate(ctorCall);
		}

		// Token: 0x040028AC RID: 10412
		private IActivator m_NextActivator;
	}
}
