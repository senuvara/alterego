﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Activation
{
	/// <summary>Defines the appropriate position for a <see cref="T:System.Activator" /> in the chain of activators.</summary>
	// Token: 0x020007CA RID: 1994
	[ComVisible(true)]
	[Serializable]
	public enum ActivatorLevel
	{
		/// <summary>Constructs a blank object and runs the constructor.</summary>
		// Token: 0x040028A5 RID: 10405
		Construction = 4,
		/// <summary>Finds or creates a suitable context.</summary>
		// Token: 0x040028A6 RID: 10406
		Context = 8,
		/// <summary>Finds or creates a <see cref="T:System.AppDomain" />.</summary>
		// Token: 0x040028A7 RID: 10407
		AppDomain = 12,
		/// <summary>Starts a process.</summary>
		// Token: 0x040028A8 RID: 10408
		Process = 16,
		/// <summary>Finds a suitable computer.</summary>
		// Token: 0x040028A9 RID: 10409
		Machine = 20
	}
}
