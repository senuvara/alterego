﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x020007CB RID: 1995
	internal class AppDomainLevelActivator : IActivator
	{
		// Token: 0x06004DF6 RID: 19958 RVA: 0x0010F9B5 File Offset: 0x0010DBB5
		public AppDomainLevelActivator(string activationUrl, IActivator next)
		{
			this._activationUrl = activationUrl;
			this._next = next;
		}

		// Token: 0x17000D0C RID: 3340
		// (get) Token: 0x06004DF7 RID: 19959 RVA: 0x00058554 File Offset: 0x00056754
		public ActivatorLevel Level
		{
			get
			{
				return ActivatorLevel.AppDomain;
			}
		}

		// Token: 0x17000D0D RID: 3341
		// (get) Token: 0x06004DF8 RID: 19960 RVA: 0x0010F9CB File Offset: 0x0010DBCB
		// (set) Token: 0x06004DF9 RID: 19961 RVA: 0x0010F9D3 File Offset: 0x0010DBD3
		public IActivator NextActivator
		{
			get
			{
				return this._next;
			}
			set
			{
				this._next = value;
			}
		}

		// Token: 0x06004DFA RID: 19962 RVA: 0x0010F9DC File Offset: 0x0010DBDC
		public IConstructionReturnMessage Activate(IConstructionCallMessage ctorCall)
		{
			IActivator activator = (IActivator)RemotingServices.Connect(typeof(IActivator), this._activationUrl);
			ctorCall.Activator = ctorCall.Activator.NextActivator;
			IConstructionReturnMessage constructionReturnMessage;
			try
			{
				constructionReturnMessage = activator.Activate(ctorCall);
			}
			catch (Exception e)
			{
				return new ConstructionResponse(e, ctorCall);
			}
			ObjRef objRef = (ObjRef)constructionReturnMessage.ReturnValue;
			if (RemotingServices.GetIdentityForUri(objRef.URI) != null)
			{
				throw new RemotingException("Inconsistent state during activation; there may be two proxies for the same object");
			}
			object obj;
			Identity orCreateClientIdentity = RemotingServices.GetOrCreateClientIdentity(objRef, null, out obj);
			RemotingServices.SetMessageTargetIdentity(ctorCall, orCreateClientIdentity);
			return constructionReturnMessage;
		}

		// Token: 0x040028AA RID: 10410
		private string _activationUrl;

		// Token: 0x040028AB RID: 10411
		private IActivator _next;
	}
}
