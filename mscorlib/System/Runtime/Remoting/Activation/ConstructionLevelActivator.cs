﻿using System;
using System.Threading;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x020007CC RID: 1996
	[Serializable]
	internal class ConstructionLevelActivator : IActivator
	{
		// Token: 0x17000D0E RID: 3342
		// (get) Token: 0x06004DFB RID: 19963 RVA: 0x000286CC File Offset: 0x000268CC
		public ActivatorLevel Level
		{
			get
			{
				return ActivatorLevel.Construction;
			}
		}

		// Token: 0x17000D0F RID: 3343
		// (get) Token: 0x06004DFC RID: 19964 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		// (set) Token: 0x06004DFD RID: 19965 RVA: 0x000020D3 File Offset: 0x000002D3
		public IActivator NextActivator
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x06004DFE RID: 19966 RVA: 0x0010FA70 File Offset: 0x0010DC70
		public IConstructionReturnMessage Activate(IConstructionCallMessage msg)
		{
			return (IConstructionReturnMessage)Thread.CurrentContext.GetServerContextSinkChain().SyncProcessMessage(msg);
		}

		// Token: 0x06004DFF RID: 19967 RVA: 0x00002050 File Offset: 0x00000250
		public ConstructionLevelActivator()
		{
		}
	}
}
