﻿using System;
using System.Runtime.Remoting.Channels;

namespace System.Runtime.Remoting
{
	// Token: 0x02000755 RID: 1877
	[Serializable]
	internal class ChannelInfo : IChannelInfo
	{
		// Token: 0x06004AFC RID: 19196 RVA: 0x00107098 File Offset: 0x00105298
		public ChannelInfo()
		{
			this.channelData = ChannelServices.GetCurrentChannelInfo();
		}

		// Token: 0x06004AFD RID: 19197 RVA: 0x001070AB File Offset: 0x001052AB
		public ChannelInfo(object remoteChannelData)
		{
			this.channelData = new object[]
			{
				remoteChannelData
			};
		}

		// Token: 0x17000C73 RID: 3187
		// (get) Token: 0x06004AFE RID: 19198 RVA: 0x001070C3 File Offset: 0x001052C3
		// (set) Token: 0x06004AFF RID: 19199 RVA: 0x001070CB File Offset: 0x001052CB
		public object[] ChannelData
		{
			get
			{
				return this.channelData;
			}
			set
			{
				this.channelData = value;
			}
		}

		// Token: 0x040027C3 RID: 10179
		private object[] channelData;
	}
}
