﻿using System;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007AA RID: 1962
	[Serializable]
	internal class CrossAppDomainData
	{
		// Token: 0x06004D7E RID: 19838 RVA: 0x0010F036 File Offset: 0x0010D236
		internal CrossAppDomainData(int domainId)
		{
			this._ContextID = 0;
			this._DomainID = domainId;
			this._processGuid = RemotingConfiguration.ProcessId;
		}

		// Token: 0x17000CEE RID: 3310
		// (get) Token: 0x06004D7F RID: 19839 RVA: 0x0010F05C File Offset: 0x0010D25C
		internal int DomainID
		{
			get
			{
				return this._DomainID;
			}
		}

		// Token: 0x17000CEF RID: 3311
		// (get) Token: 0x06004D80 RID: 19840 RVA: 0x0010F064 File Offset: 0x0010D264
		internal string ProcessID
		{
			get
			{
				return this._processGuid;
			}
		}

		// Token: 0x0400288E RID: 10382
		private object _ContextID;

		// Token: 0x0400288F RID: 10383
		private int _DomainID;

		// Token: 0x04002890 RID: 10384
		private string _processGuid;
	}
}
