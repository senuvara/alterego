﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007C4 RID: 1988
	internal class ServerDispatchSink : IServerChannelSink, IChannelSinkBase
	{
		// Token: 0x06004DD6 RID: 19926 RVA: 0x00002050 File Offset: 0x00000250
		public ServerDispatchSink()
		{
		}

		// Token: 0x17000D04 RID: 3332
		// (get) Token: 0x06004DD7 RID: 19927 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public IServerChannelSink NextChannelSink
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000D05 RID: 3333
		// (get) Token: 0x06004DD8 RID: 19928 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public IDictionary Properties
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06004DD9 RID: 19929 RVA: 0x000175EA File Offset: 0x000157EA
		public void AsyncProcessResponse(IServerResponseChannelSinkStack sinkStack, object state, IMessage msg, ITransportHeaders headers, Stream stream)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004DDA RID: 19930 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public Stream GetResponseStream(IServerResponseChannelSinkStack sinkStack, object state, IMessage msg, ITransportHeaders headers)
		{
			return null;
		}

		// Token: 0x06004DDB RID: 19931 RVA: 0x0010F57A File Offset: 0x0010D77A
		public ServerProcessing ProcessMessage(IServerChannelSinkStack sinkStack, IMessage requestMsg, ITransportHeaders requestHeaders, Stream requestStream, out IMessage responseMsg, out ITransportHeaders responseHeaders, out Stream responseStream)
		{
			responseHeaders = null;
			responseStream = null;
			return ChannelServices.DispatchMessage(sinkStack, requestMsg, out responseMsg);
		}
	}
}
