﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007AC RID: 1964
	[MonoTODO("Handle domain unloading?")]
	internal class CrossAppDomainSink : IMessageSink
	{
		// Token: 0x06004D8C RID: 19852 RVA: 0x0010F13F File Offset: 0x0010D33F
		internal CrossAppDomainSink(int domainID)
		{
			this._domainID = domainID;
		}

		// Token: 0x06004D8D RID: 19853 RVA: 0x0010F150 File Offset: 0x0010D350
		internal static CrossAppDomainSink GetSink(int domainID)
		{
			object syncRoot = CrossAppDomainSink.s_sinks.SyncRoot;
			CrossAppDomainSink result;
			lock (syncRoot)
			{
				if (CrossAppDomainSink.s_sinks.ContainsKey(domainID))
				{
					result = (CrossAppDomainSink)CrossAppDomainSink.s_sinks[domainID];
				}
				else
				{
					CrossAppDomainSink crossAppDomainSink = new CrossAppDomainSink(domainID);
					CrossAppDomainSink.s_sinks[domainID] = crossAppDomainSink;
					result = crossAppDomainSink;
				}
			}
			return result;
		}

		// Token: 0x17000CF3 RID: 3315
		// (get) Token: 0x06004D8E RID: 19854 RVA: 0x0010F1D4 File Offset: 0x0010D3D4
		internal int TargetDomainId
		{
			get
			{
				return this._domainID;
			}
		}

		// Token: 0x06004D8F RID: 19855 RVA: 0x0010F1DC File Offset: 0x0010D3DC
		private static CrossAppDomainSink.ProcessMessageRes ProcessMessageInDomain(byte[] arrRequest, CADMethodCallMessage cadMsg)
		{
			CrossAppDomainSink.ProcessMessageRes result = default(CrossAppDomainSink.ProcessMessageRes);
			try
			{
				AppDomain.CurrentDomain.ProcessMessageInDomain(arrRequest, cadMsg, out result.arrResponse, out result.cadMrm);
			}
			catch (Exception e)
			{
				IMessage msg = new MethodResponse(e, new ErrorMessage());
				result.arrResponse = CADSerializer.SerializeMessage(msg).GetBuffer();
			}
			return result;
		}

		// Token: 0x06004D90 RID: 19856 RVA: 0x0010F240 File Offset: 0x0010D440
		public virtual IMessage SyncProcessMessage(IMessage msgRequest)
		{
			IMessage result = null;
			try
			{
				byte[] array = null;
				byte[] array2 = null;
				CADMethodReturnMessage retmsg = null;
				CADMethodCallMessage cadmethodCallMessage = CADMethodCallMessage.Create(msgRequest);
				if (cadmethodCallMessage == null)
				{
					array2 = CADSerializer.SerializeMessage(msgRequest).GetBuffer();
				}
				Context currentContext = Thread.CurrentContext;
				try
				{
					CrossAppDomainSink.ProcessMessageRes processMessageRes = (CrossAppDomainSink.ProcessMessageRes)AppDomain.InvokeInDomainByID(this._domainID, CrossAppDomainSink.processMessageMethod, null, new object[]
					{
						array2,
						cadmethodCallMessage
					});
					array = processMessageRes.arrResponse;
					retmsg = processMessageRes.cadMrm;
				}
				finally
				{
					AppDomain.InternalSetContext(currentContext);
				}
				if (array != null)
				{
					result = CADSerializer.DeserializeMessage(new MemoryStream(array), msgRequest as IMethodCallMessage);
				}
				else
				{
					result = new MethodResponse(msgRequest as IMethodCallMessage, retmsg);
				}
			}
			catch (Exception e)
			{
				try
				{
					result = new ReturnMessage(e, msgRequest as IMethodCallMessage);
				}
				catch (Exception)
				{
				}
			}
			return result;
		}

		// Token: 0x06004D91 RID: 19857 RVA: 0x0010F318 File Offset: 0x0010D518
		public virtual IMessageCtrl AsyncProcessMessage(IMessage reqMsg, IMessageSink replySink)
		{
			AsyncRequest state = new AsyncRequest(reqMsg, replySink);
			ThreadPool.QueueUserWorkItem(delegate(object data)
			{
				try
				{
					this.SendAsyncMessage(data);
				}
				catch
				{
				}
			}, state);
			return null;
		}

		// Token: 0x06004D92 RID: 19858 RVA: 0x0010F344 File Offset: 0x0010D544
		public void SendAsyncMessage(object data)
		{
			AsyncRequest asyncRequest = (AsyncRequest)data;
			IMessage msg = this.SyncProcessMessage(asyncRequest.MsgRequest);
			asyncRequest.ReplySink.SyncProcessMessage(msg);
		}

		// Token: 0x17000CF4 RID: 3316
		// (get) Token: 0x06004D93 RID: 19859 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public IMessageSink NextSink
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06004D94 RID: 19860 RVA: 0x0010F372 File Offset: 0x0010D572
		// Note: this type is marked as 'beforefieldinit'.
		static CrossAppDomainSink()
		{
		}

		// Token: 0x06004D95 RID: 19861 RVA: 0x0010F39C File Offset: 0x0010D59C
		[CompilerGenerated]
		private void <AsyncProcessMessage>b__10_0(object data)
		{
			try
			{
				this.SendAsyncMessage(data);
			}
			catch
			{
			}
		}

		// Token: 0x04002893 RID: 10387
		private static Hashtable s_sinks = new Hashtable();

		// Token: 0x04002894 RID: 10388
		private static MethodInfo processMessageMethod = typeof(CrossAppDomainSink).GetMethod("ProcessMessageInDomain", BindingFlags.Static | BindingFlags.NonPublic);

		// Token: 0x04002895 RID: 10389
		private int _domainID;

		// Token: 0x020007AD RID: 1965
		private struct ProcessMessageRes
		{
			// Token: 0x04002896 RID: 10390
			public byte[] arrResponse;

			// Token: 0x04002897 RID: 10391
			public CADMethodReturnMessage cadMrm;
		}
	}
}
