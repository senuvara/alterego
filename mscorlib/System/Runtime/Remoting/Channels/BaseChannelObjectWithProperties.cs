﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Runtime.Remoting.Channels
{
	/// <summary>Provides a base implementation of a channel object that exposes a dictionary interface to its properties.</summary>
	// Token: 0x020007A2 RID: 1954
	[ComVisible(true)]
	public abstract class BaseChannelObjectWithProperties : IDictionary, ICollection, IEnumerable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties" /> class.</summary>
		// Token: 0x06004D42 RID: 19778 RVA: 0x0010E219 File Offset: 0x0010C419
		protected BaseChannelObjectWithProperties()
		{
			this.table = new Hashtable();
		}

		/// <summary>Gets the number of properties associated with the channel object.</summary>
		/// <returns>The number of properties associated with the channel object.</returns>
		// Token: 0x17000CDF RID: 3295
		// (get) Token: 0x06004D43 RID: 19779 RVA: 0x0010E22C File Offset: 0x0010C42C
		public virtual int Count
		{
			[SecuritySafeCritical]
			get
			{
				return this.table.Count;
			}
		}

		/// <summary>Gets a value that indicates whether the number of properties that can be entered into the channel object is fixed.</summary>
		/// <returns>
		///     <see langword="true" /> if the number of properties that can be entered into the channel object is fixed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CE0 RID: 3296
		// (get) Token: 0x06004D44 RID: 19780 RVA: 0x00004E08 File Offset: 0x00003008
		public virtual bool IsFixedSize
		{
			[SecuritySafeCritical]
			get
			{
				return true;
			}
		}

		/// <summary>Gets a value that indicates whether the collection of properties in the channel object is read-only.</summary>
		/// <returns>
		///     <see langword="true" /> if the collection of properties in the channel object is read-only; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CE1 RID: 3297
		// (get) Token: 0x06004D45 RID: 19781 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsReadOnly
		{
			[SecuritySafeCritical]
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value that indicates whether the dictionary of channel object properties is synchronized.</summary>
		/// <returns>
		///     <see langword="true" /> if the dictionary of channel object properties is synchronized; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000CE2 RID: 3298
		// (get) Token: 0x06004D46 RID: 19782 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsSynchronized
		{
			[SecuritySafeCritical]
			get
			{
				return false;
			}
		}

		/// <summary>When overridden in a derived class, gets or sets the property that is associated with the specified key.</summary>
		/// <param name="key">The key of the property to get or set. </param>
		/// <returns>The property that is associated with the specified key.</returns>
		/// <exception cref="T:System.NotImplementedException">The property is accessed. </exception>
		// Token: 0x17000CE3 RID: 3299
		public virtual object this[object key]
		{
			[SecuritySafeCritical]
			get
			{
				throw new NotImplementedException();
			}
			[SecuritySafeCritical]
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>When overridden in a derived class, gets a <see cref="T:System.Collections.ICollection" /> of keys that the channel object properties are associated with.</summary>
		/// <returns>A <see cref="T:System.Collections.ICollection" /> of keys that the channel object properties are associated with.</returns>
		// Token: 0x17000CE4 RID: 3300
		// (get) Token: 0x06004D49 RID: 19785 RVA: 0x0010E239 File Offset: 0x0010C439
		public virtual ICollection Keys
		{
			[SecuritySafeCritical]
			get
			{
				return this.table.Keys;
			}
		}

		/// <summary>Gets a <see cref="T:System.Collections.IDictionary" /> of the channel properties associated with the channel object.</summary>
		/// <returns>A <see cref="T:System.Collections.IDictionary" /> of the channel properties associated with the channel object.</returns>
		/// <exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		// Token: 0x17000CE5 RID: 3301
		// (get) Token: 0x06004D4A RID: 19786 RVA: 0x00002058 File Offset: 0x00000258
		public virtual IDictionary Properties
		{
			get
			{
				return this;
			}
		}

		/// <summary>Gets an object that is used to synchronize access to the <see cref="T:System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties" />.</summary>
		/// <returns>An object that is used to synchronize access to the <see cref="T:System.Runtime.Remoting.Channels.BaseChannelObjectWithProperties" />.</returns>
		// Token: 0x17000CE6 RID: 3302
		// (get) Token: 0x06004D4B RID: 19787 RVA: 0x00002058 File Offset: 0x00000258
		public virtual object SyncRoot
		{
			[SecuritySafeCritical]
			get
			{
				return this;
			}
		}

		/// <summary>Gets a <see cref="T:System.Collections.ICollection" /> of the values of the properties associated with the channel object.</summary>
		/// <returns>A <see cref="T:System.Collections.ICollection" /> of the values of the properties associated with the channel object.</returns>
		// Token: 0x17000CE7 RID: 3303
		// (get) Token: 0x06004D4C RID: 19788 RVA: 0x0010E246 File Offset: 0x0010C446
		public virtual ICollection Values
		{
			[SecuritySafeCritical]
			get
			{
				return this.table.Values;
			}
		}

		/// <summary>Throws a <see cref="T:System.NotSupportedException" />.</summary>
		/// <param name="key">The key that is associated with the object in the <paramref name="value" /> parameter. </param>
		/// <param name="value">The value to add. </param>
		/// <exception cref="T:System.NotSupportedException">The method is called. </exception>
		// Token: 0x06004D4D RID: 19789 RVA: 0x000175EA File Offset: 0x000157EA
		[SecuritySafeCritical]
		public virtual void Add(object key, object value)
		{
			throw new NotSupportedException();
		}

		/// <summary>Throws a <see cref="T:System.NotSupportedException" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The method is called. </exception>
		// Token: 0x06004D4E RID: 19790 RVA: 0x000175EA File Offset: 0x000157EA
		[SecuritySafeCritical]
		public virtual void Clear()
		{
			throw new NotSupportedException();
		}

		/// <summary>Returns a value that indicates whether the channel object contains a property that is associated with the specified key.</summary>
		/// <param name="key">The key of the property to look for. </param>
		/// <returns>
		///     <see langword="true" /> if the channel object contains a property associated with the specified key; otherwise, <see langword="false" />.</returns>
		// Token: 0x06004D4F RID: 19791 RVA: 0x0010E253 File Offset: 0x0010C453
		[SecuritySafeCritical]
		public virtual bool Contains(object key)
		{
			return this.table.Contains(key);
		}

		/// <summary>Throws a <see cref="T:System.NotSupportedException" />.</summary>
		/// <param name="array">The array to copy the properties to. </param>
		/// <param name="index">The index at which to begin copying. </param>
		/// <exception cref="T:System.NotSupportedException">The method is called. </exception>
		// Token: 0x06004D50 RID: 19792 RVA: 0x000175EA File Offset: 0x000157EA
		[SecuritySafeCritical]
		public virtual void CopyTo(Array array, int index)
		{
			throw new NotSupportedException();
		}

		/// <summary>Returns a <see cref="T:System.Collections.IDictionaryEnumerator" /> that enumerates over all the properties associated with the channel object.</summary>
		/// <returns>A <see cref="T:System.Collections.IDictionaryEnumerator" /> that enumerates over all the properties associated with the channel object.</returns>
		// Token: 0x06004D51 RID: 19793 RVA: 0x0010E261 File Offset: 0x0010C461
		[SecuritySafeCritical]
		public virtual IDictionaryEnumerator GetEnumerator()
		{
			return this.table.GetEnumerator();
		}

		/// <summary>Returns a <see cref="T:System.Collections.IEnumerator" /> that enumerates over all the properties that are associated with the channel object.</summary>
		/// <returns>A <see cref="T:System.Collections.IEnumerator" /> that enumerates over all the properties that are associated with the channel object.</returns>
		// Token: 0x06004D52 RID: 19794 RVA: 0x0010E261 File Offset: 0x0010C461
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.table.GetEnumerator();
		}

		/// <summary>Throws a <see cref="T:System.NotSupportedException" />.</summary>
		/// <param name="key">The key of the object to be removed. </param>
		/// <exception cref="T:System.NotSupportedException">The method is called. </exception>
		// Token: 0x06004D53 RID: 19795 RVA: 0x000175EA File Offset: 0x000157EA
		[SecuritySafeCritical]
		public virtual void Remove(object key)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0400287E RID: 10366
		private Hashtable table;
	}
}
