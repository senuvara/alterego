﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	/// <summary>Indicates the status of the server message processing.</summary>
	// Token: 0x020007C6 RID: 1990
	[ComVisible(true)]
	[Serializable]
	public enum ServerProcessing
	{
		/// <summary>The server synchronously processed the message.</summary>
		// Token: 0x0400289C RID: 10396
		Complete,
		/// <summary>The message was dispatched and no response can be sent.</summary>
		// Token: 0x0400289D RID: 10397
		OneWay,
		/// <summary>The call was dispatched asynchronously, which indicates that the sink must store response data on the stack for later processing.</summary>
		// Token: 0x0400289E RID: 10398
		Async
	}
}
