﻿using System;
using System.Collections;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007A1 RID: 1953
	internal class AggregateEnumerator : IDictionaryEnumerator, IEnumerator
	{
		// Token: 0x06004D3B RID: 19771 RVA: 0x0010E13E File Offset: 0x0010C33E
		public AggregateEnumerator(IDictionary[] dics)
		{
			this.dictionaries = dics;
			this.Reset();
		}

		// Token: 0x17000CDB RID: 3291
		// (get) Token: 0x06004D3C RID: 19772 RVA: 0x0010E153 File Offset: 0x0010C353
		public DictionaryEntry Entry
		{
			get
			{
				return this.currente.Entry;
			}
		}

		// Token: 0x17000CDC RID: 3292
		// (get) Token: 0x06004D3D RID: 19773 RVA: 0x0010E160 File Offset: 0x0010C360
		public object Key
		{
			get
			{
				return this.currente.Key;
			}
		}

		// Token: 0x17000CDD RID: 3293
		// (get) Token: 0x06004D3E RID: 19774 RVA: 0x0010E16D File Offset: 0x0010C36D
		public object Value
		{
			get
			{
				return this.currente.Value;
			}
		}

		// Token: 0x17000CDE RID: 3294
		// (get) Token: 0x06004D3F RID: 19775 RVA: 0x0010E17A File Offset: 0x0010C37A
		public object Current
		{
			get
			{
				return this.currente.Current;
			}
		}

		// Token: 0x06004D40 RID: 19776 RVA: 0x0010E188 File Offset: 0x0010C388
		public bool MoveNext()
		{
			if (this.pos >= this.dictionaries.Length)
			{
				return false;
			}
			if (this.currente.MoveNext())
			{
				return true;
			}
			this.pos++;
			if (this.pos >= this.dictionaries.Length)
			{
				return false;
			}
			this.currente = this.dictionaries[this.pos].GetEnumerator();
			return this.MoveNext();
		}

		// Token: 0x06004D41 RID: 19777 RVA: 0x0010E1F4 File Offset: 0x0010C3F4
		public void Reset()
		{
			this.pos = 0;
			if (this.dictionaries.Length != 0)
			{
				this.currente = this.dictionaries[0].GetEnumerator();
			}
		}

		// Token: 0x0400287B RID: 10363
		private IDictionary[] dictionaries;

		// Token: 0x0400287C RID: 10364
		private int pos;

		// Token: 0x0400287D RID: 10365
		private IDictionaryEnumerator currente;
	}
}
