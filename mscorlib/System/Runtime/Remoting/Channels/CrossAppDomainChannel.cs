﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007AB RID: 1963
	[Serializable]
	internal class CrossAppDomainChannel : IChannel, IChannelSender, IChannelReceiver
	{
		// Token: 0x06004D81 RID: 19841 RVA: 0x0010F06C File Offset: 0x0010D26C
		internal static void RegisterCrossAppDomainChannel()
		{
			object obj = CrossAppDomainChannel.s_lock;
			lock (obj)
			{
				ChannelServices.RegisterChannel(new CrossAppDomainChannel());
			}
		}

		// Token: 0x17000CF0 RID: 3312
		// (get) Token: 0x06004D82 RID: 19842 RVA: 0x0010F0B0 File Offset: 0x0010D2B0
		public virtual string ChannelName
		{
			get
			{
				return "MONOCAD";
			}
		}

		// Token: 0x17000CF1 RID: 3313
		// (get) Token: 0x06004D83 RID: 19843 RVA: 0x0010F0B7 File Offset: 0x0010D2B7
		public virtual int ChannelPriority
		{
			get
			{
				return 100;
			}
		}

		// Token: 0x06004D84 RID: 19844 RVA: 0x0010F0BB File Offset: 0x0010D2BB
		public string Parse(string url, out string objectURI)
		{
			objectURI = url;
			return null;
		}

		// Token: 0x17000CF2 RID: 3314
		// (get) Token: 0x06004D85 RID: 19845 RVA: 0x0010F0C1 File Offset: 0x0010D2C1
		public virtual object ChannelData
		{
			get
			{
				return new CrossAppDomainData(Thread.GetDomainID());
			}
		}

		// Token: 0x06004D86 RID: 19846 RVA: 0x0010F0CD File Offset: 0x0010D2CD
		public virtual string[] GetUrlsForUri(string objectURI)
		{
			throw new NotSupportedException("CrossAppdomain channel dont support UrlsForUri");
		}

		// Token: 0x06004D87 RID: 19847 RVA: 0x000020D3 File Offset: 0x000002D3
		public virtual void StartListening(object data)
		{
		}

		// Token: 0x06004D88 RID: 19848 RVA: 0x000020D3 File Offset: 0x000002D3
		public virtual void StopListening(object data)
		{
		}

		// Token: 0x06004D89 RID: 19849 RVA: 0x0010F0DC File Offset: 0x0010D2DC
		public virtual IMessageSink CreateMessageSink(string url, object data, out string uri)
		{
			uri = null;
			if (data != null)
			{
				CrossAppDomainData crossAppDomainData = data as CrossAppDomainData;
				if (crossAppDomainData != null && crossAppDomainData.ProcessID == RemotingConfiguration.ProcessId)
				{
					return CrossAppDomainSink.GetSink(crossAppDomainData.DomainID);
				}
			}
			if (url != null && url.StartsWith("MONOCAD"))
			{
				throw new NotSupportedException("Can't create a named channel via crossappdomain");
			}
			return null;
		}

		// Token: 0x06004D8A RID: 19850 RVA: 0x00002050 File Offset: 0x00000250
		public CrossAppDomainChannel()
		{
		}

		// Token: 0x06004D8B RID: 19851 RVA: 0x0010F133 File Offset: 0x0010D333
		// Note: this type is marked as 'beforefieldinit'.
		static CrossAppDomainChannel()
		{
		}

		// Token: 0x04002891 RID: 10385
		private const string _strName = "MONOCAD";

		// Token: 0x04002892 RID: 10386
		private static object s_lock = new object();
	}
}
