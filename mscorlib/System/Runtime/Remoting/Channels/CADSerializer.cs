﻿using System;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters.Binary;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007AE RID: 1966
	internal class CADSerializer
	{
		// Token: 0x06004D96 RID: 19862 RVA: 0x000175EA File Offset: 0x000157EA
		internal static IMessage DeserializeMessage(MemoryStream mem, IMethodCallMessage msg)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004D97 RID: 19863 RVA: 0x0010F3C8 File Offset: 0x0010D5C8
		internal static MemoryStream SerializeMessage(IMessage msg)
		{
			MemoryStream memoryStream = new MemoryStream();
			new BinaryFormatter
			{
				SurrogateSelector = new RemotingSurrogateSelector()
			}.Serialize(memoryStream, msg);
			memoryStream.Position = 0L;
			return memoryStream;
		}

		// Token: 0x06004D98 RID: 19864 RVA: 0x0010F3FC File Offset: 0x0010D5FC
		internal static object DeserializeObjectSafe(byte[] mem)
		{
			byte[] array = new byte[mem.Length];
			Array.Copy(mem, array, mem.Length);
			return CADSerializer.DeserializeObject(new MemoryStream(array));
		}

		// Token: 0x06004D99 RID: 19865 RVA: 0x0010F428 File Offset: 0x0010D628
		internal static MemoryStream SerializeObject(object obj)
		{
			MemoryStream memoryStream = new MemoryStream();
			new BinaryFormatter
			{
				SurrogateSelector = new RemotingSurrogateSelector()
			}.Serialize(memoryStream, obj);
			memoryStream.Position = 0L;
			return memoryStream;
		}

		// Token: 0x06004D9A RID: 19866 RVA: 0x0010F45B File Offset: 0x0010D65B
		internal static object DeserializeObject(MemoryStream mem)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.SurrogateSelector = null;
			mem.Position = 0L;
			return binaryFormatter.Deserialize(mem);
		}

		// Token: 0x06004D9B RID: 19867 RVA: 0x00002050 File Offset: 0x00000250
		public CADSerializer()
		{
		}
	}
}
