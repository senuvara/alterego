﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	/// <summary>Stores sink provider data for sink providers.</summary>
	// Token: 0x020007C7 RID: 1991
	[ComVisible(true)]
	public class SinkProviderData
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Channels.SinkProviderData" /> class.</summary>
		/// <param name="name">The name of the sink provider that the data in the current <see cref="T:System.Runtime.Remoting.Channels.SinkProviderData" /> object is associated with. </param>
		// Token: 0x06004DE2 RID: 19938 RVA: 0x0010F594 File Offset: 0x0010D794
		public SinkProviderData(string name)
		{
			this.sinkName = name;
			this.children = new ArrayList();
			this.properties = new Hashtable();
		}

		/// <summary>Gets a list of the child <see cref="T:System.Runtime.Remoting.Channels.SinkProviderData" /> nodes.</summary>
		/// <returns>A <see cref="T:System.Collections.IList" /> of the child <see cref="T:System.Runtime.Remoting.Channels.SinkProviderData" /> nodes.</returns>
		// Token: 0x17000D07 RID: 3335
		// (get) Token: 0x06004DE3 RID: 19939 RVA: 0x0010F5B9 File Offset: 0x0010D7B9
		public IList Children
		{
			get
			{
				return this.children;
			}
		}

		/// <summary>Gets the name of the sink provider that the data in the current <see cref="T:System.Runtime.Remoting.Channels.SinkProviderData" /> object is associated with.</summary>
		/// <returns>A <see cref="T:System.String" /> with the name of the XML node that the data in the current <see cref="T:System.Runtime.Remoting.Channels.SinkProviderData" /> object is associated with.</returns>
		// Token: 0x17000D08 RID: 3336
		// (get) Token: 0x06004DE4 RID: 19940 RVA: 0x0010F5C1 File Offset: 0x0010D7C1
		public string Name
		{
			get
			{
				return this.sinkName;
			}
		}

		/// <summary>Gets a dictionary through which properties on the sink provider can be accessed.</summary>
		/// <returns>A dictionary through which properties on the sink provider can be accessed.</returns>
		// Token: 0x17000D09 RID: 3337
		// (get) Token: 0x06004DE5 RID: 19941 RVA: 0x0010F5C9 File Offset: 0x0010D7C9
		public IDictionary Properties
		{
			get
			{
				return this.properties;
			}
		}

		// Token: 0x0400289F RID: 10399
		private string sinkName;

		// Token: 0x040028A0 RID: 10400
		private ArrayList children;

		// Token: 0x040028A1 RID: 10401
		private Hashtable properties;
	}
}
