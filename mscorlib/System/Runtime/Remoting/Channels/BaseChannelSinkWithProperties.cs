﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	/// <summary>Provides a base implementation for channel sinks that want to expose a dictionary interface to their properties.</summary>
	// Token: 0x020007A3 RID: 1955
	[ComVisible(true)]
	public abstract class BaseChannelSinkWithProperties : BaseChannelObjectWithProperties
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Channels.BaseChannelSinkWithProperties" /> class.</summary>
		// Token: 0x06004D54 RID: 19796 RVA: 0x0010E26E File Offset: 0x0010C46E
		protected BaseChannelSinkWithProperties()
		{
		}
	}
}
