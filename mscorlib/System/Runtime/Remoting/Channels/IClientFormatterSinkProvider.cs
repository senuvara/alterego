﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	/// <summary>Marks a client channel sink provider as a client formatter sink provider.</summary>
	// Token: 0x020007BA RID: 1978
	[ComVisible(true)]
	public interface IClientFormatterSinkProvider : IClientChannelSinkProvider
	{
	}
}
