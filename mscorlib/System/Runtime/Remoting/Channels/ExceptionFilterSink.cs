﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007A7 RID: 1959
	internal class ExceptionFilterSink : IMessageSink
	{
		// Token: 0x06004D72 RID: 19826 RVA: 0x0010EEFC File Offset: 0x0010D0FC
		public ExceptionFilterSink(IMessage call, IMessageSink next)
		{
			this._call = call;
			this._next = next;
		}

		// Token: 0x06004D73 RID: 19827 RVA: 0x0010EF12 File Offset: 0x0010D112
		public IMessage SyncProcessMessage(IMessage msg)
		{
			return this._next.SyncProcessMessage(ChannelServices.CheckReturnMessage(this._call, msg));
		}

		// Token: 0x06004D74 RID: 19828 RVA: 0x0007D3EE File Offset: 0x0007B5EE
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x17000CED RID: 3309
		// (get) Token: 0x06004D75 RID: 19829 RVA: 0x0010EF2B File Offset: 0x0010D12B
		public IMessageSink NextSink
		{
			get
			{
				return this._next;
			}
		}

		// Token: 0x04002887 RID: 10375
		private IMessageSink _next;

		// Token: 0x04002888 RID: 10376
		private IMessage _call;
	}
}
