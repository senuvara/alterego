﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007AF RID: 1967
	internal class AsyncRequest
	{
		// Token: 0x06004D9C RID: 19868 RVA: 0x0010F477 File Offset: 0x0010D677
		public AsyncRequest(IMessage msgRequest, IMessageSink replySink)
		{
			this.ReplySink = replySink;
			this.MsgRequest = msgRequest;
		}

		// Token: 0x04002898 RID: 10392
		internal IMessageSink ReplySink;

		// Token: 0x04002899 RID: 10393
		internal IMessage MsgRequest;
	}
}
