﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007A0 RID: 1952
	[ComVisible(true)]
	internal class AggregateDictionary : IDictionary, ICollection, IEnumerable
	{
		// Token: 0x06004D2A RID: 19754 RVA: 0x0010DF7C File Offset: 0x0010C17C
		public AggregateDictionary(IDictionary[] dics)
		{
			this.dictionaries = dics;
		}

		// Token: 0x17000CD3 RID: 3283
		// (get) Token: 0x06004D2B RID: 19755 RVA: 0x00004E08 File Offset: 0x00003008
		public bool IsFixedSize
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000CD4 RID: 3284
		// (get) Token: 0x06004D2C RID: 19756 RVA: 0x00004E08 File Offset: 0x00003008
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000CD5 RID: 3285
		public object this[object key]
		{
			get
			{
				foreach (IDictionary dictionary in this.dictionaries)
				{
					if (dictionary.Contains(key))
					{
						return dictionary[key];
					}
				}
				return null;
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000CD6 RID: 3286
		// (get) Token: 0x06004D2F RID: 19759 RVA: 0x0010DFC4 File Offset: 0x0010C1C4
		public ICollection Keys
		{
			get
			{
				if (this._keys != null)
				{
					return this._keys;
				}
				this._keys = new ArrayList();
				foreach (IDictionary dictionary in this.dictionaries)
				{
					this._keys.AddRange(dictionary.Keys);
				}
				return this._keys;
			}
		}

		// Token: 0x17000CD7 RID: 3287
		// (get) Token: 0x06004D30 RID: 19760 RVA: 0x0010E01C File Offset: 0x0010C21C
		public ICollection Values
		{
			get
			{
				if (this._values != null)
				{
					return this._values;
				}
				this._values = new ArrayList();
				foreach (IDictionary dictionary in this.dictionaries)
				{
					this._values.AddRange(dictionary.Values);
				}
				return this._values;
			}
		}

		// Token: 0x06004D31 RID: 19761 RVA: 0x000175EA File Offset: 0x000157EA
		public void Add(object key, object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004D32 RID: 19762 RVA: 0x000175EA File Offset: 0x000157EA
		public void Clear()
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004D33 RID: 19763 RVA: 0x0010E074 File Offset: 0x0010C274
		public bool Contains(object ob)
		{
			IDictionary[] array = this.dictionaries;
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i].Contains(ob))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06004D34 RID: 19764 RVA: 0x0010E0A4 File Offset: 0x0010C2A4
		public IDictionaryEnumerator GetEnumerator()
		{
			return new AggregateEnumerator(this.dictionaries);
		}

		// Token: 0x06004D35 RID: 19765 RVA: 0x0010E0A4 File Offset: 0x0010C2A4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new AggregateEnumerator(this.dictionaries);
		}

		// Token: 0x06004D36 RID: 19766 RVA: 0x000175EA File Offset: 0x000157EA
		public void Remove(object ob)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004D37 RID: 19767 RVA: 0x0010E0B4 File Offset: 0x0010C2B4
		public void CopyTo(Array array, int index)
		{
			foreach (object value in this)
			{
				array.SetValue(value, index++);
			}
		}

		// Token: 0x17000CD8 RID: 3288
		// (get) Token: 0x06004D38 RID: 19768 RVA: 0x0010E10C File Offset: 0x0010C30C
		public int Count
		{
			get
			{
				int num = 0;
				foreach (IDictionary dictionary in this.dictionaries)
				{
					num += dictionary.Count;
				}
				return num;
			}
		}

		// Token: 0x17000CD9 RID: 3289
		// (get) Token: 0x06004D39 RID: 19769 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000CDA RID: 3290
		// (get) Token: 0x06004D3A RID: 19770 RVA: 0x00002058 File Offset: 0x00000258
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x04002878 RID: 10360
		private IDictionary[] dictionaries;

		// Token: 0x04002879 RID: 10361
		private ArrayList _values;

		// Token: 0x0400287A RID: 10362
		private ArrayList _keys;
	}
}
