﻿using System;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007A8 RID: 1960
	internal class ChanelSinkStackEntry
	{
		// Token: 0x06004D76 RID: 19830 RVA: 0x0010EF33 File Offset: 0x0010D133
		public ChanelSinkStackEntry(IChannelSinkBase sink, object state, ChanelSinkStackEntry next)
		{
			this.Sink = sink;
			this.State = state;
			this.Next = next;
		}

		// Token: 0x04002889 RID: 10377
		public IChannelSinkBase Sink;

		// Token: 0x0400288A RID: 10378
		public object State;

		// Token: 0x0400288B RID: 10379
		public ChanelSinkStackEntry Next;
	}
}
