﻿using System;
using System.Collections;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x020007C5 RID: 1989
	internal class ServerDispatchSinkProvider : IServerFormatterSinkProvider, IServerChannelSinkProvider
	{
		// Token: 0x06004DDC RID: 19932 RVA: 0x00002050 File Offset: 0x00000250
		public ServerDispatchSinkProvider()
		{
		}

		// Token: 0x06004DDD RID: 19933 RVA: 0x00002050 File Offset: 0x00000250
		public ServerDispatchSinkProvider(IDictionary properties, ICollection providerData)
		{
		}

		// Token: 0x17000D06 RID: 3334
		// (get) Token: 0x06004DDE RID: 19934 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		// (set) Token: 0x06004DDF RID: 19935 RVA: 0x000175EA File Offset: 0x000157EA
		public IServerChannelSinkProvider Next
		{
			get
			{
				return null;
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x06004DE0 RID: 19936 RVA: 0x0010F58D File Offset: 0x0010D78D
		public IServerChannelSink CreateSink(IChannelReceiver channel)
		{
			return new ServerDispatchSink();
		}

		// Token: 0x06004DE1 RID: 19937 RVA: 0x000020D3 File Offset: 0x000002D3
		public void GetChannelData(IChannelDataStore channelData)
		{
		}
	}
}
