﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	/// <summary>Specifies how custom errors are handled.</summary>
	// Token: 0x02000758 RID: 1880
	[ComVisible(true)]
	public enum CustomErrorsModes
	{
		/// <summary>All callers receive filtered exception information.</summary>
		// Token: 0x040027C8 RID: 10184
		On,
		/// <summary>All callers receive complete exception information.</summary>
		// Token: 0x040027C9 RID: 10185
		Off,
		/// <summary>Local callers receive complete exception information; remote callers receive filtered exception information.</summary>
		// Token: 0x040027CA RID: 10186
		RemoteOnly
	}
}
