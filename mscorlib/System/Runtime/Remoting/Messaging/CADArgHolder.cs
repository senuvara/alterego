﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000809 RID: 2057
	internal class CADArgHolder
	{
		// Token: 0x06004FC3 RID: 20419 RVA: 0x00111F65 File Offset: 0x00110165
		public CADArgHolder(int i)
		{
			this.index = i;
		}

		// Token: 0x04002921 RID: 10529
		public int index;
	}
}
