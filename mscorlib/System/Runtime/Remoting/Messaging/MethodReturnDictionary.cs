﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200082A RID: 2090
	internal class MethodReturnDictionary : MessageDictionary
	{
		// Token: 0x060050D0 RID: 20688 RVA: 0x00114CBC File Offset: 0x00112EBC
		public MethodReturnDictionary(IMethodReturnMessage message) : base(message)
		{
			if (message.Exception == null)
			{
				base.MethodKeys = MethodReturnDictionary.InternalReturnKeys;
				return;
			}
			base.MethodKeys = MethodReturnDictionary.InternalExceptionKeys;
		}

		// Token: 0x060050D1 RID: 20689 RVA: 0x00114CE4 File Offset: 0x00112EE4
		// Note: this type is marked as 'beforefieldinit'.
		static MethodReturnDictionary()
		{
		}

		// Token: 0x0400296D RID: 10605
		public static string[] InternalReturnKeys = new string[]
		{
			"__Uri",
			"__MethodName",
			"__TypeName",
			"__MethodSignature",
			"__OutArgs",
			"__Return",
			"__CallContext"
		};

		// Token: 0x0400296E RID: 10606
		public static string[] InternalExceptionKeys = new string[]
		{
			"__CallContext"
		};
	}
}
