﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000820 RID: 2080
	internal interface ISerializationRootObject
	{
		// Token: 0x06005046 RID: 20550
		void RootSetObjectData(SerializationInfo info, StreamingContext context);
	}
}
