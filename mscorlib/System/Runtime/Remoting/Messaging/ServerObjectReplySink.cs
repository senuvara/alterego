﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000836 RID: 2102
	internal class ServerObjectReplySink : IMessageSink
	{
		// Token: 0x06005147 RID: 20807 RVA: 0x00115AED File Offset: 0x00113CED
		public ServerObjectReplySink(ServerIdentity identity, IMessageSink replySink)
		{
			this._replySink = replySink;
			this._identity = identity;
		}

		// Token: 0x06005148 RID: 20808 RVA: 0x00115B03 File Offset: 0x00113D03
		public IMessage SyncProcessMessage(IMessage msg)
		{
			this._identity.NotifyServerDynamicSinks(false, msg, true, true);
			return this._replySink.SyncProcessMessage(msg);
		}

		// Token: 0x06005149 RID: 20809 RVA: 0x000175EA File Offset: 0x000157EA
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000E2D RID: 3629
		// (get) Token: 0x0600514A RID: 20810 RVA: 0x00115B20 File Offset: 0x00113D20
		public IMessageSink NextSink
		{
			get
			{
				return this._replySink;
			}
		}

		// Token: 0x0400299E RID: 10654
		private IMessageSink _replySink;

		// Token: 0x0400299F RID: 10655
		private ServerIdentity _identity;
	}
}
