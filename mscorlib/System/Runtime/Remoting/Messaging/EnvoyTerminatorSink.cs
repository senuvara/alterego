﻿using System;
using System.Threading;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000814 RID: 2068
	[Serializable]
	internal class EnvoyTerminatorSink : IMessageSink
	{
		// Token: 0x06005007 RID: 20487 RVA: 0x0011302D File Offset: 0x0011122D
		public IMessage SyncProcessMessage(IMessage msg)
		{
			return Thread.CurrentContext.GetClientContextSinkChain().SyncProcessMessage(msg);
		}

		// Token: 0x06005008 RID: 20488 RVA: 0x0011303F File Offset: 0x0011123F
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			return Thread.CurrentContext.GetClientContextSinkChain().AsyncProcessMessage(msg, replySink);
		}

		// Token: 0x17000D9F RID: 3487
		// (get) Token: 0x06005009 RID: 20489 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public IMessageSink NextSink
		{
			get
			{
				return null;
			}
		}

		// Token: 0x0600500A RID: 20490 RVA: 0x00002050 File Offset: 0x00000250
		public EnvoyTerminatorSink()
		{
		}

		// Token: 0x0600500B RID: 20491 RVA: 0x00113052 File Offset: 0x00111252
		// Note: this type is marked as 'beforefieldinit'.
		static EnvoyTerminatorSink()
		{
		}

		// Token: 0x0400293E RID: 10558
		public static EnvoyTerminatorSink Instance = new EnvoyTerminatorSink();
	}
}
