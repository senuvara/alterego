﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Serialization;
using System.Security;

namespace System.Runtime.Remoting.Messaging
{
	/// <summary>Implements the <see cref="T:System.Runtime.Remoting.Activation.IConstructionCallMessage" /> interface to create a request message that constitutes a constructor call on a remote object.</summary>
	// Token: 0x02000811 RID: 2065
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Serializable]
	public class ConstructionCall : MethodCall, IConstructionCallMessage, IMessage, IMethodCallMessage, IMethodMessage
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Messaging.ConstructionCall" /> class by copying an existing message. </summary>
		/// <param name="m">A remoting message.</param>
		// Token: 0x06004FEB RID: 20459 RVA: 0x00112C2B File Offset: 0x00110E2B
		public ConstructionCall(IMessage m) : base(m)
		{
			this._activationTypeName = base.TypeName;
			this._isContextOk = true;
		}

		// Token: 0x06004FEC RID: 20460 RVA: 0x00112C47 File Offset: 0x00110E47
		internal ConstructionCall(Type type)
		{
			this._activationType = type;
			this._activationTypeName = type.AssemblyQualifiedName;
			this._isContextOk = true;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Messaging.ConstructionCall" /> class from an array of remoting headers. </summary>
		/// <param name="headers">An array of remoting headers that contain key-value pairs. This array is used to initialize <see cref="T:System.Runtime.Remoting.Messaging.ConstructionCall" /> fields for those headers that belong to the namespace "http://schemas.microsoft.com/clr/soap/messageProperties".</param>
		// Token: 0x06004FED RID: 20461 RVA: 0x00112C69 File Offset: 0x00110E69
		public ConstructionCall(Header[] headers) : base(headers)
		{
		}

		// Token: 0x06004FEE RID: 20462 RVA: 0x00112C72 File Offset: 0x00110E72
		internal ConstructionCall(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06004FEF RID: 20463 RVA: 0x00112C7C File Offset: 0x00110E7C
		internal override void InitDictionary()
		{
			ConstructionCallDictionary constructionCallDictionary = new ConstructionCallDictionary(this);
			this.ExternalProperties = constructionCallDictionary;
			this.InternalProperties = constructionCallDictionary.GetInternalProperties();
		}

		// Token: 0x17000D96 RID: 3478
		// (get) Token: 0x06004FF0 RID: 20464 RVA: 0x00112CA3 File Offset: 0x00110EA3
		// (set) Token: 0x06004FF1 RID: 20465 RVA: 0x00112CAB File Offset: 0x00110EAB
		internal bool IsContextOk
		{
			get
			{
				return this._isContextOk;
			}
			set
			{
				this._isContextOk = value;
			}
		}

		/// <summary>Gets the type of the remote object to activate. </summary>
		/// <returns>The <see cref="T:System.Type" /> of the remote object to activate.</returns>
		// Token: 0x17000D97 RID: 3479
		// (get) Token: 0x06004FF2 RID: 20466 RVA: 0x00112CB4 File Offset: 0x00110EB4
		public Type ActivationType
		{
			[SecurityCritical]
			get
			{
				if (this._activationType == null)
				{
					this._activationType = Type.GetType(this._activationTypeName);
				}
				return this._activationType;
			}
		}

		/// <summary>Gets the full type name of the remote object to activate. </summary>
		/// <returns>A <see cref="T:System.String" /> containing the full type name of the remote object to activate.</returns>
		// Token: 0x17000D98 RID: 3480
		// (get) Token: 0x06004FF3 RID: 20467 RVA: 0x00112CDB File Offset: 0x00110EDB
		public string ActivationTypeName
		{
			[SecurityCritical]
			get
			{
				return this._activationTypeName;
			}
		}

		/// <summary>Gets or sets the activator that activates the remote object. </summary>
		/// <returns>The <see cref="T:System.Runtime.Remoting.Activation.IActivator" /> that activates the remote object.</returns>
		// Token: 0x17000D99 RID: 3481
		// (get) Token: 0x06004FF4 RID: 20468 RVA: 0x00112CE3 File Offset: 0x00110EE3
		// (set) Token: 0x06004FF5 RID: 20469 RVA: 0x00112CEB File Offset: 0x00110EEB
		public IActivator Activator
		{
			[SecurityCritical]
			get
			{
				return this._activator;
			}
			[SecurityCritical]
			set
			{
				this._activator = value;
			}
		}

		/// <summary>Gets the call site activation attributes for the remote object. </summary>
		/// <returns>An array of type <see cref="T:System.Object" /> containing the call site activation attributes for the remote object.</returns>
		// Token: 0x17000D9A RID: 3482
		// (get) Token: 0x06004FF6 RID: 20470 RVA: 0x00112CF4 File Offset: 0x00110EF4
		public object[] CallSiteActivationAttributes
		{
			[SecurityCritical]
			get
			{
				return this._activationAttributes;
			}
		}

		// Token: 0x06004FF7 RID: 20471 RVA: 0x00112CFC File Offset: 0x00110EFC
		internal void SetActivationAttributes(object[] attributes)
		{
			this._activationAttributes = attributes;
		}

		/// <summary>Gets a list of properties that define the context in which the remote object is to be created. </summary>
		/// <returns>A <see cref="T:System.Collections.IList" /> that contains a list of properties that define the context in which the remote object is to be created.</returns>
		// Token: 0x17000D9B RID: 3483
		// (get) Token: 0x06004FF8 RID: 20472 RVA: 0x00112D05 File Offset: 0x00110F05
		public IList ContextProperties
		{
			[SecurityCritical]
			get
			{
				if (this._contextProperties == null)
				{
					this._contextProperties = new ArrayList();
				}
				return this._contextProperties;
			}
		}

		// Token: 0x06004FF9 RID: 20473 RVA: 0x00112D20 File Offset: 0x00110F20
		internal override void InitMethodProperty(string key, object value)
		{
			if (key == "__Activator")
			{
				this._activator = (IActivator)value;
				return;
			}
			if (key == "__CallSiteActivationAttributes")
			{
				this._activationAttributes = (object[])value;
				return;
			}
			if (key == "__ActivationType")
			{
				this._activationType = (Type)value;
				return;
			}
			if (key == "__ContextProperties")
			{
				this._contextProperties = (IList)value;
				return;
			}
			if (!(key == "__ActivationTypeName"))
			{
				base.InitMethodProperty(key, value);
				return;
			}
			this._activationTypeName = (string)value;
		}

		// Token: 0x06004FFA RID: 20474 RVA: 0x00112DBC File Offset: 0x00110FBC
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			IList list = this._contextProperties;
			if (list != null && list.Count == 0)
			{
				list = null;
			}
			info.AddValue("__Activator", this._activator);
			info.AddValue("__CallSiteActivationAttributes", this._activationAttributes);
			info.AddValue("__ActivationType", null);
			info.AddValue("__ContextProperties", list);
			info.AddValue("__ActivationTypeName", this._activationTypeName);
		}

		/// <summary>Gets an <see cref="T:System.Collections.IDictionary" /> interface that represents a collection of the remoting message's properties. </summary>
		/// <returns>An <see cref="T:System.Collections.IDictionary" /> interface that represents a collection of the remoting message's properties.</returns>
		// Token: 0x17000D9C RID: 3484
		// (get) Token: 0x06004FFB RID: 20475 RVA: 0x00112E30 File Offset: 0x00111030
		public override IDictionary Properties
		{
			[SecurityCritical]
			get
			{
				return base.Properties;
			}
		}

		// Token: 0x17000D9D RID: 3485
		// (get) Token: 0x06004FFC RID: 20476 RVA: 0x00112E38 File Offset: 0x00111038
		// (set) Token: 0x06004FFD RID: 20477 RVA: 0x00112E40 File Offset: 0x00111040
		internal RemotingProxy SourceProxy
		{
			get
			{
				return this._sourceProxy;
			}
			set
			{
				this._sourceProxy = value;
			}
		}

		// Token: 0x04002936 RID: 10550
		private IActivator _activator;

		// Token: 0x04002937 RID: 10551
		private object[] _activationAttributes;

		// Token: 0x04002938 RID: 10552
		private IList _contextProperties;

		// Token: 0x04002939 RID: 10553
		private Type _activationType;

		// Token: 0x0400293A RID: 10554
		private string _activationTypeName;

		// Token: 0x0400293B RID: 10555
		private bool _isContextOk;

		// Token: 0x0400293C RID: 10556
		[NonSerialized]
		private RemotingProxy _sourceProxy;
	}
}
