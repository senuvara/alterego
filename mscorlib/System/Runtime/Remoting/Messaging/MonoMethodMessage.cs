﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200082D RID: 2093
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	internal class MonoMethodMessage : IMethodCallMessage, IMethodMessage, IMessage, IMethodReturnMessage, IInternalMessage
	{
		// Token: 0x060050EE RID: 20718 RVA: 0x00115010 File Offset: 0x00113210
		internal void InitMessage(MonoMethod method, object[] out_args)
		{
			this.method = method;
			ParameterInfo[] parametersInternal = method.GetParametersInternal();
			int num = parametersInternal.Length;
			this.args = new object[num];
			this.arg_types = new byte[num];
			this.asyncResult = null;
			this.call_type = CallType.Sync;
			this.names = new string[num];
			for (int i = 0; i < num; i++)
			{
				this.names[i] = parametersInternal[i].Name;
			}
			bool flag = out_args != null;
			int num2 = 0;
			for (int j = 0; j < num; j++)
			{
				bool isOut = parametersInternal[j].IsOut;
				byte b;
				if (parametersInternal[j].ParameterType.IsByRef)
				{
					if (flag)
					{
						this.args[j] = out_args[num2++];
					}
					b = 2;
					if (!isOut)
					{
						b |= 1;
					}
				}
				else
				{
					b = 1;
					if (isOut)
					{
						b |= 4;
					}
				}
				this.arg_types[j] = b;
			}
		}

		// Token: 0x060050EF RID: 20719 RVA: 0x001150F1 File Offset: 0x001132F1
		public MonoMethodMessage(MethodBase method, object[] out_args)
		{
			if (method != null)
			{
				this.InitMessage((MonoMethod)method, out_args);
				return;
			}
			this.args = null;
		}

		// Token: 0x060050F0 RID: 20720 RVA: 0x00115118 File Offset: 0x00113318
		internal MonoMethodMessage(MethodInfo minfo, object[] in_args, object[] out_args)
		{
			this.InitMessage((MonoMethod)minfo, out_args);
			int num = in_args.Length;
			for (int i = 0; i < num; i++)
			{
				this.args[i] = in_args[i];
			}
		}

		// Token: 0x060050F1 RID: 20721 RVA: 0x00115153 File Offset: 0x00113353
		private static MethodInfo GetMethodInfo(Type type, string methodName)
		{
			MethodInfo methodInfo = type.GetMethod(methodName);
			if (methodInfo == null)
			{
				throw new ArgumentException(string.Format("Could not find '{0}' in {1}", methodName, type), "methodName");
			}
			return methodInfo;
		}

		// Token: 0x060050F2 RID: 20722 RVA: 0x0011517C File Offset: 0x0011337C
		public MonoMethodMessage(Type type, string methodName, object[] in_args) : this(MonoMethodMessage.GetMethodInfo(type, methodName), in_args, null)
		{
		}

		// Token: 0x17000E06 RID: 3590
		// (get) Token: 0x060050F3 RID: 20723 RVA: 0x0011518D File Offset: 0x0011338D
		public IDictionary Properties
		{
			get
			{
				if (this.properties == null)
				{
					this.properties = new MCMDictionary(this);
				}
				return this.properties;
			}
		}

		// Token: 0x17000E07 RID: 3591
		// (get) Token: 0x060050F4 RID: 20724 RVA: 0x001151A9 File Offset: 0x001133A9
		public int ArgCount
		{
			get
			{
				if (this.CallType == CallType.EndInvoke)
				{
					return -1;
				}
				if (this.args == null)
				{
					return 0;
				}
				return this.args.Length;
			}
		}

		// Token: 0x17000E08 RID: 3592
		// (get) Token: 0x060050F5 RID: 20725 RVA: 0x001151C8 File Offset: 0x001133C8
		public object[] Args
		{
			get
			{
				return this.args;
			}
		}

		// Token: 0x17000E09 RID: 3593
		// (get) Token: 0x060050F6 RID: 20726 RVA: 0x00002526 File Offset: 0x00000726
		public bool HasVarArgs
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000E0A RID: 3594
		// (get) Token: 0x060050F7 RID: 20727 RVA: 0x001151D0 File Offset: 0x001133D0
		// (set) Token: 0x060050F8 RID: 20728 RVA: 0x001151D8 File Offset: 0x001133D8
		public LogicalCallContext LogicalCallContext
		{
			get
			{
				return this.ctx;
			}
			set
			{
				this.ctx = value;
			}
		}

		// Token: 0x17000E0B RID: 3595
		// (get) Token: 0x060050F9 RID: 20729 RVA: 0x001151E1 File Offset: 0x001133E1
		public MethodBase MethodBase
		{
			get
			{
				return this.method;
			}
		}

		// Token: 0x17000E0C RID: 3596
		// (get) Token: 0x060050FA RID: 20730 RVA: 0x001151E9 File Offset: 0x001133E9
		public string MethodName
		{
			get
			{
				if (null == this.method)
				{
					return string.Empty;
				}
				return this.method.Name;
			}
		}

		// Token: 0x17000E0D RID: 3597
		// (get) Token: 0x060050FB RID: 20731 RVA: 0x0011520C File Offset: 0x0011340C
		public object MethodSignature
		{
			get
			{
				if (this.methodSignature == null)
				{
					ParameterInfo[] parameters = this.method.GetParameters();
					this.methodSignature = new Type[parameters.Length];
					for (int i = 0; i < parameters.Length; i++)
					{
						this.methodSignature[i] = parameters[i].ParameterType;
					}
				}
				return this.methodSignature;
			}
		}

		// Token: 0x17000E0E RID: 3598
		// (get) Token: 0x060050FC RID: 20732 RVA: 0x0011525F File Offset: 0x0011345F
		public string TypeName
		{
			get
			{
				if (null == this.method)
				{
					return string.Empty;
				}
				return this.method.DeclaringType.AssemblyQualifiedName;
			}
		}

		// Token: 0x17000E0F RID: 3599
		// (get) Token: 0x060050FD RID: 20733 RVA: 0x00115285 File Offset: 0x00113485
		// (set) Token: 0x060050FE RID: 20734 RVA: 0x0011528D File Offset: 0x0011348D
		public string Uri
		{
			get
			{
				return this.uri;
			}
			set
			{
				this.uri = value;
			}
		}

		// Token: 0x060050FF RID: 20735 RVA: 0x00115296 File Offset: 0x00113496
		public object GetArg(int arg_num)
		{
			if (this.args == null)
			{
				return null;
			}
			return this.args[arg_num];
		}

		// Token: 0x06005100 RID: 20736 RVA: 0x001152AA File Offset: 0x001134AA
		public string GetArgName(int arg_num)
		{
			if (this.args == null)
			{
				return string.Empty;
			}
			return this.names[arg_num];
		}

		// Token: 0x17000E10 RID: 3600
		// (get) Token: 0x06005101 RID: 20737 RVA: 0x001152C4 File Offset: 0x001134C4
		public int InArgCount
		{
			get
			{
				if (this.CallType == CallType.EndInvoke)
				{
					return -1;
				}
				if (this.args == null)
				{
					return 0;
				}
				int num = 0;
				byte[] array = this.arg_types;
				for (int i = 0; i < array.Length; i++)
				{
					if ((array[i] & 1) != 0)
					{
						num++;
					}
				}
				return num;
			}
		}

		// Token: 0x17000E11 RID: 3601
		// (get) Token: 0x06005102 RID: 20738 RVA: 0x0011530C File Offset: 0x0011350C
		public object[] InArgs
		{
			get
			{
				object[] array = new object[this.InArgCount];
				int num2;
				int num = num2 = 0;
				byte[] array2 = this.arg_types;
				for (int i = 0; i < array2.Length; i++)
				{
					if ((array2[i] & 1) != 0)
					{
						array[num++] = this.args[num2];
					}
					num2++;
				}
				return array;
			}
		}

		// Token: 0x06005103 RID: 20739 RVA: 0x00115360 File Offset: 0x00113560
		public object GetInArg(int arg_num)
		{
			int num = 0;
			int num2 = 0;
			byte[] array = this.arg_types;
			for (int i = 0; i < array.Length; i++)
			{
				if ((array[i] & 1) != 0 && num2++ == arg_num)
				{
					return this.args[num];
				}
				num++;
			}
			return null;
		}

		// Token: 0x06005104 RID: 20740 RVA: 0x001153A4 File Offset: 0x001135A4
		public string GetInArgName(int arg_num)
		{
			int num = 0;
			int num2 = 0;
			byte[] array = this.arg_types;
			for (int i = 0; i < array.Length; i++)
			{
				if ((array[i] & 1) != 0 && num2++ == arg_num)
				{
					return this.names[num];
				}
				num++;
			}
			return null;
		}

		// Token: 0x17000E12 RID: 3602
		// (get) Token: 0x06005105 RID: 20741 RVA: 0x001153E7 File Offset: 0x001135E7
		public Exception Exception
		{
			get
			{
				return this.exc;
			}
		}

		// Token: 0x17000E13 RID: 3603
		// (get) Token: 0x06005106 RID: 20742 RVA: 0x001153F0 File Offset: 0x001135F0
		public int OutArgCount
		{
			get
			{
				if (this.args == null)
				{
					return 0;
				}
				int num = 0;
				byte[] array = this.arg_types;
				for (int i = 0; i < array.Length; i++)
				{
					if ((array[i] & 2) != 0)
					{
						num++;
					}
				}
				return num;
			}
		}

		// Token: 0x17000E14 RID: 3604
		// (get) Token: 0x06005107 RID: 20743 RVA: 0x0011542C File Offset: 0x0011362C
		public object[] OutArgs
		{
			get
			{
				if (this.args == null)
				{
					return null;
				}
				object[] array = new object[this.OutArgCount];
				int num2;
				int num = num2 = 0;
				byte[] array2 = this.arg_types;
				for (int i = 0; i < array2.Length; i++)
				{
					if ((array2[i] & 2) != 0)
					{
						array[num++] = this.args[num2];
					}
					num2++;
				}
				return array;
			}
		}

		// Token: 0x17000E15 RID: 3605
		// (get) Token: 0x06005108 RID: 20744 RVA: 0x00115488 File Offset: 0x00113688
		public object ReturnValue
		{
			get
			{
				return this.rval;
			}
		}

		// Token: 0x06005109 RID: 20745 RVA: 0x00115490 File Offset: 0x00113690
		public object GetOutArg(int arg_num)
		{
			int num = 0;
			int num2 = 0;
			byte[] array = this.arg_types;
			for (int i = 0; i < array.Length; i++)
			{
				if ((array[i] & 2) != 0 && num2++ == arg_num)
				{
					return this.args[num];
				}
				num++;
			}
			return null;
		}

		// Token: 0x0600510A RID: 20746 RVA: 0x001154D4 File Offset: 0x001136D4
		public string GetOutArgName(int arg_num)
		{
			int num = 0;
			int num2 = 0;
			byte[] array = this.arg_types;
			for (int i = 0; i < array.Length; i++)
			{
				if ((array[i] & 2) != 0 && num2++ == arg_num)
				{
					return this.names[num];
				}
				num++;
			}
			return null;
		}

		// Token: 0x17000E16 RID: 3606
		// (get) Token: 0x0600510B RID: 20747 RVA: 0x00115517 File Offset: 0x00113717
		// (set) Token: 0x0600510C RID: 20748 RVA: 0x0011551F File Offset: 0x0011371F
		Identity IInternalMessage.TargetIdentity
		{
			get
			{
				return this.identity;
			}
			set
			{
				this.identity = value;
			}
		}

		// Token: 0x0600510D RID: 20749 RVA: 0x00115528 File Offset: 0x00113728
		bool IInternalMessage.HasProperties()
		{
			return this.properties != null;
		}

		// Token: 0x17000E17 RID: 3607
		// (get) Token: 0x0600510E RID: 20750 RVA: 0x00115533 File Offset: 0x00113733
		public bool IsAsync
		{
			get
			{
				return this.asyncResult != null;
			}
		}

		// Token: 0x17000E18 RID: 3608
		// (get) Token: 0x0600510F RID: 20751 RVA: 0x0011553E File Offset: 0x0011373E
		public AsyncResult AsyncResult
		{
			get
			{
				return this.asyncResult;
			}
		}

		// Token: 0x17000E19 RID: 3609
		// (get) Token: 0x06005110 RID: 20752 RVA: 0x00115546 File Offset: 0x00113746
		internal CallType CallType
		{
			get
			{
				if (this.call_type == CallType.Sync && RemotingServices.IsOneWay(this.method))
				{
					this.call_type = CallType.OneWay;
				}
				return this.call_type;
			}
		}

		// Token: 0x06005111 RID: 20753 RVA: 0x0011556C File Offset: 0x0011376C
		public bool NeedsOutProcessing(out int outCount)
		{
			bool flag = false;
			outCount = 0;
			foreach (byte b in this.arg_types)
			{
				if ((b & 2) != 0)
				{
					outCount++;
				}
				else if ((b & 4) != 0)
				{
					flag = true;
				}
			}
			return outCount > 0 || flag;
		}

		// Token: 0x06005112 RID: 20754 RVA: 0x001155B2 File Offset: 0x001137B2
		// Note: this type is marked as 'beforefieldinit'.
		static MonoMethodMessage()
		{
		}

		// Token: 0x04002976 RID: 10614
		private MonoMethod method;

		// Token: 0x04002977 RID: 10615
		private object[] args;

		// Token: 0x04002978 RID: 10616
		private string[] names;

		// Token: 0x04002979 RID: 10617
		private byte[] arg_types;

		// Token: 0x0400297A RID: 10618
		public LogicalCallContext ctx;

		// Token: 0x0400297B RID: 10619
		public object rval;

		// Token: 0x0400297C RID: 10620
		public Exception exc;

		// Token: 0x0400297D RID: 10621
		private AsyncResult asyncResult;

		// Token: 0x0400297E RID: 10622
		private CallType call_type;

		// Token: 0x0400297F RID: 10623
		private string uri;

		// Token: 0x04002980 RID: 10624
		private MCMDictionary properties;

		// Token: 0x04002981 RID: 10625
		private Type[] methodSignature;

		// Token: 0x04002982 RID: 10626
		private Identity identity;

		// Token: 0x04002983 RID: 10627
		internal static string CallContextKey = "__CallContext";

		// Token: 0x04002984 RID: 10628
		internal static string UriKey = "__Uri";
	}
}
