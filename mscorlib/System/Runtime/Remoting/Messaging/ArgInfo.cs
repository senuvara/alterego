﻿using System;
using System.Reflection;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000807 RID: 2055
	internal class ArgInfo
	{
		// Token: 0x06004FA6 RID: 20390 RVA: 0x00111C1C File Offset: 0x0010FE1C
		public ArgInfo(MethodBase method, ArgInfoType type)
		{
			this._method = method;
			ParameterInfo[] parameters = this._method.GetParameters();
			this._paramMap = new int[parameters.Length];
			this._inoutArgCount = 0;
			if (type == ArgInfoType.In)
			{
				for (int i = 0; i < parameters.Length; i++)
				{
					if (!parameters[i].ParameterType.IsByRef)
					{
						int[] paramMap = this._paramMap;
						int inoutArgCount = this._inoutArgCount;
						this._inoutArgCount = inoutArgCount + 1;
						paramMap[inoutArgCount] = i;
					}
				}
				return;
			}
			for (int j = 0; j < parameters.Length; j++)
			{
				if (parameters[j].ParameterType.IsByRef || parameters[j].IsOut)
				{
					int[] paramMap2 = this._paramMap;
					int inoutArgCount = this._inoutArgCount;
					this._inoutArgCount = inoutArgCount + 1;
					paramMap2[inoutArgCount] = j;
				}
			}
		}

		// Token: 0x06004FA7 RID: 20391 RVA: 0x00111CD1 File Offset: 0x0010FED1
		public int GetInOutArgIndex(int inoutArgNum)
		{
			return this._paramMap[inoutArgNum];
		}

		// Token: 0x06004FA8 RID: 20392 RVA: 0x00111CDB File Offset: 0x0010FEDB
		public virtual string GetInOutArgName(int index)
		{
			return this._method.GetParameters()[this._paramMap[index]].Name;
		}

		// Token: 0x06004FA9 RID: 20393 RVA: 0x00111CF6 File Offset: 0x0010FEF6
		public int GetInOutArgCount()
		{
			return this._inoutArgCount;
		}

		// Token: 0x06004FAA RID: 20394 RVA: 0x00111D00 File Offset: 0x0010FF00
		public object[] GetInOutArgs(object[] args)
		{
			object[] array = new object[this._inoutArgCount];
			for (int i = 0; i < this._inoutArgCount; i++)
			{
				array[i] = args[this._paramMap[i]];
			}
			return array;
		}

		// Token: 0x0400290D RID: 10509
		private int[] _paramMap;

		// Token: 0x0400290E RID: 10510
		private int _inoutArgCount;

		// Token: 0x0400290F RID: 10511
		private MethodBase _method;
	}
}
