﻿using System;
using System.Collections;
using System.Reflection;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000815 RID: 2069
	[Serializable]
	internal class ErrorMessage : IMethodCallMessage, IMethodMessage, IMessage
	{
		// Token: 0x0600500C RID: 20492 RVA: 0x0011305E File Offset: 0x0011125E
		public ErrorMessage()
		{
		}

		// Token: 0x17000DA0 RID: 3488
		// (get) Token: 0x0600500D RID: 20493 RVA: 0x00002526 File Offset: 0x00000726
		public int ArgCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000DA1 RID: 3489
		// (get) Token: 0x0600500E RID: 20494 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public object[] Args
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000DA2 RID: 3490
		// (get) Token: 0x0600500F RID: 20495 RVA: 0x00002526 File Offset: 0x00000726
		public bool HasVarArgs
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000DA3 RID: 3491
		// (get) Token: 0x06005010 RID: 20496 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public MethodBase MethodBase
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000DA4 RID: 3492
		// (get) Token: 0x06005011 RID: 20497 RVA: 0x00113071 File Offset: 0x00111271
		public string MethodName
		{
			get
			{
				return "unknown";
			}
		}

		// Token: 0x17000DA5 RID: 3493
		// (get) Token: 0x06005012 RID: 20498 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public object MethodSignature
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000DA6 RID: 3494
		// (get) Token: 0x06005013 RID: 20499 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public virtual IDictionary Properties
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000DA7 RID: 3495
		// (get) Token: 0x06005014 RID: 20500 RVA: 0x00113071 File Offset: 0x00111271
		public string TypeName
		{
			get
			{
				return "unknown";
			}
		}

		// Token: 0x17000DA8 RID: 3496
		// (get) Token: 0x06005015 RID: 20501 RVA: 0x00113078 File Offset: 0x00111278
		// (set) Token: 0x06005016 RID: 20502 RVA: 0x00113080 File Offset: 0x00111280
		public string Uri
		{
			get
			{
				return this._uri;
			}
			set
			{
				this._uri = value;
			}
		}

		// Token: 0x06005017 RID: 20503 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public object GetArg(int arg_num)
		{
			return null;
		}

		// Token: 0x06005018 RID: 20504 RVA: 0x00113071 File Offset: 0x00111271
		public string GetArgName(int arg_num)
		{
			return "unknown";
		}

		// Token: 0x17000DA9 RID: 3497
		// (get) Token: 0x06005019 RID: 20505 RVA: 0x00002526 File Offset: 0x00000726
		public int InArgCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x0600501A RID: 20506 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public string GetInArgName(int index)
		{
			return null;
		}

		// Token: 0x0600501B RID: 20507 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public object GetInArg(int argNum)
		{
			return null;
		}

		// Token: 0x17000DAA RID: 3498
		// (get) Token: 0x0600501C RID: 20508 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public object[] InArgs
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000DAB RID: 3499
		// (get) Token: 0x0600501D RID: 20509 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public LogicalCallContext LogicalCallContext
		{
			get
			{
				return null;
			}
		}

		// Token: 0x0400293F RID: 10559
		private string _uri = "Exception";
	}
}
