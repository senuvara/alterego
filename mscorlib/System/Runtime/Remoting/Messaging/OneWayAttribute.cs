﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	/// <summary>Marks a method as one way, without a return value and <see langword="out" /> or <see langword="ref" /> parameters.</summary>
	// Token: 0x0200082F RID: 2095
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Method)]
	public class OneWayAttribute : Attribute
	{
		/// <summary>Creates an instance of <see cref="T:System.Runtime.Remoting.Messaging.OneWayAttribute" />.</summary>
		// Token: 0x06005113 RID: 20755 RVA: 0x000020BF File Offset: 0x000002BF
		public OneWayAttribute()
		{
		}
	}
}
