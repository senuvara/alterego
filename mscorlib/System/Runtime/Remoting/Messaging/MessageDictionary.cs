﻿using System;
using System.Collections;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000827 RID: 2087
	[Serializable]
	internal class MessageDictionary : IDictionary, ICollection, IEnumerable
	{
		// Token: 0x0600508D RID: 20621 RVA: 0x00113D12 File Offset: 0x00111F12
		public MessageDictionary(IMethodMessage message)
		{
			this._message = message;
		}

		// Token: 0x0600508E RID: 20622 RVA: 0x00113D21 File Offset: 0x00111F21
		internal bool HasUserData()
		{
			if (this._internalProperties == null)
			{
				return false;
			}
			if (this._internalProperties is MessageDictionary)
			{
				return ((MessageDictionary)this._internalProperties).HasUserData();
			}
			return this._internalProperties.Count > 0;
		}

		// Token: 0x17000DDA RID: 3546
		// (get) Token: 0x0600508F RID: 20623 RVA: 0x00113D59 File Offset: 0x00111F59
		internal IDictionary InternalDictionary
		{
			get
			{
				if (this._internalProperties != null && this._internalProperties is MessageDictionary)
				{
					return ((MessageDictionary)this._internalProperties).InternalDictionary;
				}
				return this._internalProperties;
			}
		}

		// Token: 0x17000DDB RID: 3547
		// (get) Token: 0x06005090 RID: 20624 RVA: 0x00113D87 File Offset: 0x00111F87
		// (set) Token: 0x06005091 RID: 20625 RVA: 0x00113D8F File Offset: 0x00111F8F
		public string[] MethodKeys
		{
			get
			{
				return this._methodKeys;
			}
			set
			{
				this._methodKeys = value;
			}
		}

		// Token: 0x06005092 RID: 20626 RVA: 0x00113D98 File Offset: 0x00111F98
		protected virtual IDictionary AllocInternalProperties()
		{
			this._ownProperties = true;
			return new Hashtable();
		}

		// Token: 0x06005093 RID: 20627 RVA: 0x00113DA6 File Offset: 0x00111FA6
		public IDictionary GetInternalProperties()
		{
			if (this._internalProperties == null)
			{
				this._internalProperties = this.AllocInternalProperties();
			}
			return this._internalProperties;
		}

		// Token: 0x06005094 RID: 20628 RVA: 0x00113DC4 File Offset: 0x00111FC4
		private bool IsOverridenKey(string key)
		{
			if (this._ownProperties)
			{
				return false;
			}
			foreach (string b in this._methodKeys)
			{
				if (key == b)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06005095 RID: 20629 RVA: 0x00113E00 File Offset: 0x00112000
		public MessageDictionary(string[] keys)
		{
			this._methodKeys = keys;
		}

		// Token: 0x17000DDC RID: 3548
		// (get) Token: 0x06005096 RID: 20630 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000DDD RID: 3549
		// (get) Token: 0x06005097 RID: 20631 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000DDE RID: 3550
		public object this[object key]
		{
			get
			{
				string text = (string)key;
				for (int i = 0; i < this._methodKeys.Length; i++)
				{
					if (this._methodKeys[i] == text)
					{
						return this.GetMethodProperty(text);
					}
				}
				if (this._internalProperties != null)
				{
					return this._internalProperties[key];
				}
				return null;
			}
			set
			{
				this.Add(key, value);
			}
		}

		// Token: 0x0600509A RID: 20634 RVA: 0x00113E70 File Offset: 0x00112070
		protected virtual object GetMethodProperty(string key)
		{
			uint num = <PrivateImplementationDetails>.ComputeStringHash(key);
			if (num <= 1637783905U)
			{
				if (num <= 1201911322U)
				{
					if (num != 990701179U)
					{
						if (num == 1201911322U)
						{
							if (key == "__CallContext")
							{
								return this._message.LogicalCallContext;
							}
						}
					}
					else if (key == "__Uri")
					{
						return this._message.Uri;
					}
				}
				else if (num != 1619225942U)
				{
					if (num == 1637783905U)
					{
						if (key == "__Return")
						{
							return ((IMethodReturnMessage)this._message).ReturnValue;
						}
					}
				}
				else if (key == "__Args")
				{
					return this._message.Args;
				}
			}
			else if (num <= 2010141056U)
			{
				if (num != 1960967436U)
				{
					if (num == 2010141056U)
					{
						if (key == "__TypeName")
						{
							return this._message.TypeName;
						}
					}
				}
				else if (key == "__OutArgs")
				{
					return ((IMethodReturnMessage)this._message).OutArgs;
				}
			}
			else if (num != 3166241401U)
			{
				if (num == 3679129400U)
				{
					if (key == "__MethodSignature")
					{
						return this._message.MethodSignature;
					}
				}
			}
			else if (key == "__MethodName")
			{
				return this._message.MethodName;
			}
			return null;
		}

		// Token: 0x0600509B RID: 20635 RVA: 0x00113FF4 File Offset: 0x001121F4
		protected virtual void SetMethodProperty(string key, object value)
		{
			uint num = <PrivateImplementationDetails>.ComputeStringHash(key);
			if (num <= 1637783905U)
			{
				if (num <= 1201911322U)
				{
					if (num != 990701179U)
					{
						if (num != 1201911322U)
						{
							return;
						}
						key == "__CallContext";
						return;
					}
					else
					{
						if (!(key == "__Uri"))
						{
							return;
						}
						((IInternalMessage)this._message).Uri = (string)value;
						return;
					}
				}
				else
				{
					if (num == 1619225942U)
					{
						key == "__Args";
						return;
					}
					if (num != 1637783905U)
					{
						return;
					}
					key == "__Return";
					return;
				}
			}
			else if (num <= 2010141056U)
			{
				if (num == 1960967436U)
				{
					key == "__OutArgs";
					return;
				}
				if (num != 2010141056U)
				{
					return;
				}
				key == "__TypeName";
				return;
			}
			else
			{
				if (num == 3166241401U)
				{
					key == "__MethodName";
					return;
				}
				if (num != 3679129400U)
				{
					return;
				}
				key == "__MethodSignature";
				return;
			}
		}

		// Token: 0x17000DDF RID: 3551
		// (get) Token: 0x0600509C RID: 20636 RVA: 0x001140EC File Offset: 0x001122EC
		public ICollection Keys
		{
			get
			{
				ArrayList arrayList = new ArrayList();
				for (int i = 0; i < this._methodKeys.Length; i++)
				{
					arrayList.Add(this._methodKeys[i]);
				}
				if (this._internalProperties != null)
				{
					foreach (object obj in this._internalProperties.Keys)
					{
						string text = (string)obj;
						if (!this.IsOverridenKey(text))
						{
							arrayList.Add(text);
						}
					}
				}
				return arrayList;
			}
		}

		// Token: 0x17000DE0 RID: 3552
		// (get) Token: 0x0600509D RID: 20637 RVA: 0x00114188 File Offset: 0x00112388
		public ICollection Values
		{
			get
			{
				ArrayList arrayList = new ArrayList();
				for (int i = 0; i < this._methodKeys.Length; i++)
				{
					arrayList.Add(this.GetMethodProperty(this._methodKeys[i]));
				}
				if (this._internalProperties != null)
				{
					foreach (object obj in this._internalProperties)
					{
						DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
						if (!this.IsOverridenKey((string)dictionaryEntry.Key))
						{
							arrayList.Add(dictionaryEntry.Value);
						}
					}
				}
				return arrayList;
			}
		}

		// Token: 0x0600509E RID: 20638 RVA: 0x00114238 File Offset: 0x00112438
		public void Add(object key, object value)
		{
			string text = (string)key;
			for (int i = 0; i < this._methodKeys.Length; i++)
			{
				if (this._methodKeys[i] == text)
				{
					this.SetMethodProperty(text, value);
					return;
				}
			}
			if (this._internalProperties == null)
			{
				this._internalProperties = this.AllocInternalProperties();
			}
			this._internalProperties[key] = value;
		}

		// Token: 0x0600509F RID: 20639 RVA: 0x00114299 File Offset: 0x00112499
		public void Clear()
		{
			if (this._internalProperties != null)
			{
				this._internalProperties.Clear();
			}
		}

		// Token: 0x060050A0 RID: 20640 RVA: 0x001142B0 File Offset: 0x001124B0
		public bool Contains(object key)
		{
			string b = (string)key;
			for (int i = 0; i < this._methodKeys.Length; i++)
			{
				if (this._methodKeys[i] == b)
				{
					return true;
				}
			}
			return this._internalProperties != null && this._internalProperties.Contains(key);
		}

		// Token: 0x060050A1 RID: 20641 RVA: 0x00114300 File Offset: 0x00112500
		public void Remove(object key)
		{
			string b = (string)key;
			for (int i = 0; i < this._methodKeys.Length; i++)
			{
				if (this._methodKeys[i] == b)
				{
					throw new ArgumentException("key was invalid");
				}
			}
			if (this._internalProperties != null)
			{
				this._internalProperties.Remove(key);
			}
		}

		// Token: 0x17000DE1 RID: 3553
		// (get) Token: 0x060050A2 RID: 20642 RVA: 0x00114356 File Offset: 0x00112556
		public int Count
		{
			get
			{
				if (this._internalProperties != null)
				{
					return this._internalProperties.Count + this._methodKeys.Length;
				}
				return this._methodKeys.Length;
			}
		}

		// Token: 0x17000DE2 RID: 3554
		// (get) Token: 0x060050A3 RID: 20643 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000DE3 RID: 3555
		// (get) Token: 0x060050A4 RID: 20644 RVA: 0x00002058 File Offset: 0x00000258
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x060050A5 RID: 20645 RVA: 0x0011437D File Offset: 0x0011257D
		public void CopyTo(Array array, int index)
		{
			this.Values.CopyTo(array, index);
		}

		// Token: 0x060050A6 RID: 20646 RVA: 0x0011438C File Offset: 0x0011258C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new MessageDictionary.DictionaryEnumerator(this);
		}

		// Token: 0x060050A7 RID: 20647 RVA: 0x0011438C File Offset: 0x0011258C
		public IDictionaryEnumerator GetEnumerator()
		{
			return new MessageDictionary.DictionaryEnumerator(this);
		}

		// Token: 0x04002957 RID: 10583
		private IDictionary _internalProperties;

		// Token: 0x04002958 RID: 10584
		protected IMethodMessage _message;

		// Token: 0x04002959 RID: 10585
		private string[] _methodKeys;

		// Token: 0x0400295A RID: 10586
		private bool _ownProperties;

		// Token: 0x02000828 RID: 2088
		private class DictionaryEnumerator : IDictionaryEnumerator, IEnumerator
		{
			// Token: 0x060050A8 RID: 20648 RVA: 0x00114394 File Offset: 0x00112594
			public DictionaryEnumerator(MessageDictionary methodDictionary)
			{
				this._methodDictionary = methodDictionary;
				this._hashtableEnum = ((this._methodDictionary._internalProperties != null) ? this._methodDictionary._internalProperties.GetEnumerator() : null);
				this._posMethod = -1;
			}

			// Token: 0x17000DE4 RID: 3556
			// (get) Token: 0x060050A9 RID: 20649 RVA: 0x001143D0 File Offset: 0x001125D0
			public object Current
			{
				get
				{
					return this.Entry;
				}
			}

			// Token: 0x060050AA RID: 20650 RVA: 0x001143E0 File Offset: 0x001125E0
			public bool MoveNext()
			{
				if (this._posMethod != -2)
				{
					this._posMethod++;
					if (this._posMethod < this._methodDictionary._methodKeys.Length)
					{
						return true;
					}
					this._posMethod = -2;
				}
				if (this._hashtableEnum == null)
				{
					return false;
				}
				while (this._hashtableEnum.MoveNext())
				{
					if (!this._methodDictionary.IsOverridenKey((string)this._hashtableEnum.Key))
					{
						return true;
					}
				}
				return false;
			}

			// Token: 0x060050AB RID: 20651 RVA: 0x0011445B File Offset: 0x0011265B
			public void Reset()
			{
				this._posMethod = -1;
				this._hashtableEnum.Reset();
			}

			// Token: 0x17000DE5 RID: 3557
			// (get) Token: 0x060050AC RID: 20652 RVA: 0x00114470 File Offset: 0x00112670
			public DictionaryEntry Entry
			{
				get
				{
					if (this._posMethod >= 0)
					{
						return new DictionaryEntry(this._methodDictionary._methodKeys[this._posMethod], this._methodDictionary.GetMethodProperty(this._methodDictionary._methodKeys[this._posMethod]));
					}
					if (this._posMethod == -1 || this._hashtableEnum == null)
					{
						throw new InvalidOperationException("The enumerator is positioned before the first element of the collection or after the last element");
					}
					return this._hashtableEnum.Entry;
				}
			}

			// Token: 0x17000DE6 RID: 3558
			// (get) Token: 0x060050AD RID: 20653 RVA: 0x001144E4 File Offset: 0x001126E4
			public object Key
			{
				get
				{
					return this.Entry.Key;
				}
			}

			// Token: 0x17000DE7 RID: 3559
			// (get) Token: 0x060050AE RID: 20654 RVA: 0x00114500 File Offset: 0x00112700
			public object Value
			{
				get
				{
					return this.Entry.Value;
				}
			}

			// Token: 0x0400295B RID: 10587
			private MessageDictionary _methodDictionary;

			// Token: 0x0400295C RID: 10588
			private IDictionaryEnumerator _hashtableEnum;

			// Token: 0x0400295D RID: 10589
			private int _posMethod;
		}
	}
}
