﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000805 RID: 2053
	[Serializable]
	internal class CallContextRemotingData : ICloneable
	{
		// Token: 0x17000D85 RID: 3461
		// (get) Token: 0x06004FA1 RID: 20385 RVA: 0x00111BEB File Offset: 0x0010FDEB
		// (set) Token: 0x06004FA2 RID: 20386 RVA: 0x00111BF3 File Offset: 0x0010FDF3
		internal string LogicalCallID
		{
			get
			{
				return this._logicalCallID;
			}
			set
			{
				this._logicalCallID = value;
			}
		}

		// Token: 0x17000D86 RID: 3462
		// (get) Token: 0x06004FA3 RID: 20387 RVA: 0x00111BFC File Offset: 0x0010FDFC
		internal bool HasInfo
		{
			get
			{
				return this._logicalCallID != null;
			}
		}

		// Token: 0x06004FA4 RID: 20388 RVA: 0x00111C07 File Offset: 0x0010FE07
		public object Clone()
		{
			return new CallContextRemotingData
			{
				LogicalCallID = this.LogicalCallID
			};
		}

		// Token: 0x06004FA5 RID: 20389 RVA: 0x00002050 File Offset: 0x00000250
		public CallContextRemotingData()
		{
		}

		// Token: 0x04002909 RID: 10505
		private string _logicalCallID;
	}
}
