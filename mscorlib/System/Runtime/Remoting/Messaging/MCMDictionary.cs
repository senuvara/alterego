﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000824 RID: 2084
	internal class MCMDictionary : MessageDictionary
	{
		// Token: 0x06005073 RID: 20595 RVA: 0x00113AA4 File Offset: 0x00111CA4
		public MCMDictionary(IMethodMessage message) : base(message)
		{
			base.MethodKeys = MCMDictionary.InternalKeys;
		}

		// Token: 0x06005074 RID: 20596 RVA: 0x00113AB8 File Offset: 0x00111CB8
		// Note: this type is marked as 'beforefieldinit'.
		static MCMDictionary()
		{
		}

		// Token: 0x04002951 RID: 10577
		public static string[] InternalKeys = new string[]
		{
			"__Uri",
			"__MethodName",
			"__TypeName",
			"__MethodSignature",
			"__Args",
			"__CallContext"
		};
	}
}
