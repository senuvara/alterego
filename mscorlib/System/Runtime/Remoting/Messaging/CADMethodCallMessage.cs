﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Remoting.Channels;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200080D RID: 2061
	internal class CADMethodCallMessage : CADMessageBase
	{
		// Token: 0x17000D91 RID: 3473
		// (get) Token: 0x06004FD6 RID: 20438 RVA: 0x001128B3 File Offset: 0x00110AB3
		internal string Uri
		{
			get
			{
				return this._uri;
			}
		}

		// Token: 0x06004FD7 RID: 20439 RVA: 0x001128BC File Offset: 0x00110ABC
		internal static CADMethodCallMessage Create(IMessage callMsg)
		{
			IMethodCallMessage methodCallMessage = callMsg as IMethodCallMessage;
			if (methodCallMessage == null)
			{
				return null;
			}
			return new CADMethodCallMessage(methodCallMessage);
		}

		// Token: 0x06004FD8 RID: 20440 RVA: 0x001128DC File Offset: 0x00110ADC
		internal CADMethodCallMessage(IMethodCallMessage callMsg) : base(callMsg)
		{
			this._uri = callMsg.Uri;
			ArrayList arrayList = null;
			this._propertyCount = CADMessageBase.MarshalProperties(callMsg.Properties, ref arrayList);
			this._args = base.MarshalArguments(callMsg.Args, ref arrayList);
			base.SaveLogicalCallContext(callMsg, ref arrayList);
			if (arrayList != null)
			{
				MemoryStream memoryStream = CADSerializer.SerializeObject(arrayList.ToArray());
				this._serializedArgs = memoryStream.GetBuffer();
			}
		}

		// Token: 0x06004FD9 RID: 20441 RVA: 0x0011294C File Offset: 0x00110B4C
		internal ArrayList GetArguments()
		{
			ArrayList result = null;
			if (this._serializedArgs != null)
			{
				result = new ArrayList((object[])CADSerializer.DeserializeObject(new MemoryStream(this._serializedArgs)));
				this._serializedArgs = null;
			}
			return result;
		}

		// Token: 0x06004FDA RID: 20442 RVA: 0x00112986 File Offset: 0x00110B86
		internal object[] GetArgs(ArrayList args)
		{
			return base.UnmarshalArguments(this._args, args);
		}

		// Token: 0x17000D92 RID: 3474
		// (get) Token: 0x06004FDB RID: 20443 RVA: 0x00112995 File Offset: 0x00110B95
		internal int PropertiesCount
		{
			get
			{
				return this._propertyCount;
			}
		}

		// Token: 0x0400292F RID: 10543
		private string _uri;
	}
}
