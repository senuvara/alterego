﻿using System;
using System.Collections;
using System.Security;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000800 RID: 2048
	internal class IllogicalCallContext
	{
		// Token: 0x17000D73 RID: 3443
		// (get) Token: 0x06004F6F RID: 20335 RVA: 0x00111438 File Offset: 0x0010F638
		private Hashtable Datastore
		{
			get
			{
				if (this.m_Datastore == null)
				{
					this.m_Datastore = new Hashtable();
				}
				return this.m_Datastore;
			}
		}

		// Token: 0x17000D74 RID: 3444
		// (get) Token: 0x06004F70 RID: 20336 RVA: 0x00111453 File Offset: 0x0010F653
		// (set) Token: 0x06004F71 RID: 20337 RVA: 0x0011145B File Offset: 0x0010F65B
		internal object HostContext
		{
			get
			{
				return this.m_HostContext;
			}
			set
			{
				this.m_HostContext = value;
			}
		}

		// Token: 0x17000D75 RID: 3445
		// (get) Token: 0x06004F72 RID: 20338 RVA: 0x00111464 File Offset: 0x0010F664
		internal bool HasUserData
		{
			get
			{
				return this.m_Datastore != null && this.m_Datastore.Count > 0;
			}
		}

		// Token: 0x06004F73 RID: 20339 RVA: 0x0011147E File Offset: 0x0010F67E
		public void FreeNamedDataSlot(string name)
		{
			this.Datastore.Remove(name);
		}

		// Token: 0x06004F74 RID: 20340 RVA: 0x0011148C File Offset: 0x0010F68C
		public object GetData(string name)
		{
			return this.Datastore[name];
		}

		// Token: 0x06004F75 RID: 20341 RVA: 0x0011149A File Offset: 0x0010F69A
		public void SetData(string name, object data)
		{
			this.Datastore[name] = data;
		}

		// Token: 0x06004F76 RID: 20342 RVA: 0x001114AC File Offset: 0x0010F6AC
		public IllogicalCallContext CreateCopy()
		{
			IllogicalCallContext illogicalCallContext = new IllogicalCallContext();
			illogicalCallContext.HostContext = this.HostContext;
			if (this.HasUserData)
			{
				IDictionaryEnumerator enumerator = this.m_Datastore.GetEnumerator();
				while (enumerator.MoveNext())
				{
					illogicalCallContext.Datastore[(string)enumerator.Key] = enumerator.Value;
				}
			}
			return illogicalCallContext;
		}

		// Token: 0x06004F77 RID: 20343 RVA: 0x00002050 File Offset: 0x00000250
		public IllogicalCallContext()
		{
		}

		// Token: 0x040028FB RID: 10491
		private Hashtable m_Datastore;

		// Token: 0x040028FC RID: 10492
		private object m_HostContext;

		// Token: 0x02000801 RID: 2049
		internal struct Reader
		{
			// Token: 0x06004F78 RID: 20344 RVA: 0x00111506 File Offset: 0x0010F706
			public Reader(IllogicalCallContext ctx)
			{
				this.m_ctx = ctx;
			}

			// Token: 0x17000D76 RID: 3446
			// (get) Token: 0x06004F79 RID: 20345 RVA: 0x0011150F File Offset: 0x0010F70F
			public bool IsNull
			{
				get
				{
					return this.m_ctx == null;
				}
			}

			// Token: 0x06004F7A RID: 20346 RVA: 0x0011151A File Offset: 0x0010F71A
			[SecurityCritical]
			public object GetData(string name)
			{
				if (!this.IsNull)
				{
					return this.m_ctx.GetData(name);
				}
				return null;
			}

			// Token: 0x17000D77 RID: 3447
			// (get) Token: 0x06004F7B RID: 20347 RVA: 0x00111532 File Offset: 0x0010F732
			public object HostContext
			{
				get
				{
					if (!this.IsNull)
					{
						return this.m_ctx.HostContext;
					}
					return null;
				}
			}

			// Token: 0x040028FD RID: 10493
			private IllogicalCallContext m_ctx;
		}
	}
}
