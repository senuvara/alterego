﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Runtime.Remoting.Messaging
{
	/// <summary>Holds a message returned in response to a method call on a remote object.</summary>
	// Token: 0x02000833 RID: 2099
	[ComVisible(true)]
	public class ReturnMessage : IMethodReturnMessage, IMethodMessage, IMessage, IInternalMessage
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Messaging.ReturnMessage" /> class with all the information returning to the caller after the method call.</summary>
		/// <param name="ret">The object returned by the invoked method from which the current <see cref="T:System.Runtime.Remoting.Messaging.ReturnMessage" /> instance originated. </param>
		/// <param name="outArgs">The objects returned from the invoked method as <see langword="out" /> parameters. </param>
		/// <param name="outArgsCount">The number of <see langword="out" /> parameters returned from the invoked method. </param>
		/// <param name="callCtx">The <see cref="T:System.Runtime.Remoting.Messaging.LogicalCallContext" /> of the method call. </param>
		/// <param name="mcm">The original method call to the invoked method. </param>
		// Token: 0x06005124 RID: 20772 RVA: 0x001156F0 File Offset: 0x001138F0
		public ReturnMessage(object ret, object[] outArgs, int outArgsCount, LogicalCallContext callCtx, IMethodCallMessage mcm)
		{
			this._returnValue = ret;
			this._args = outArgs;
			this._callCtx = callCtx;
			if (mcm != null)
			{
				this._uri = mcm.Uri;
				this._methodBase = mcm.MethodBase;
			}
			if (this._args == null)
			{
				this._args = new object[outArgsCount];
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Messaging.ReturnMessage" /> class.</summary>
		/// <param name="e">The exception that was thrown during execution of the remotely called method. </param>
		/// <param name="mcm">An <see cref="T:System.Runtime.Remoting.Messaging.IMethodCallMessage" /> with which to create an instance of the <see cref="T:System.Runtime.Remoting.Messaging.ReturnMessage" /> class. </param>
		// Token: 0x06005125 RID: 20773 RVA: 0x0011574B File Offset: 0x0011394B
		public ReturnMessage(Exception e, IMethodCallMessage mcm)
		{
			this._exception = e;
			if (mcm != null)
			{
				this._methodBase = mcm.MethodBase;
				this._callCtx = mcm.LogicalCallContext;
			}
			this._args = new object[0];
		}

		/// <summary>Gets the number of arguments of the called method.</summary>
		/// <returns>The number of arguments of the called method.</returns>
		// Token: 0x17000E1B RID: 3611
		// (get) Token: 0x06005126 RID: 20774 RVA: 0x00115781 File Offset: 0x00113981
		public int ArgCount
		{
			[SecurityCritical]
			get
			{
				return this._args.Length;
			}
		}

		/// <summary>Gets a specified argument passed to the method called on the remote object.</summary>
		/// <returns>An argument passed to the method called on the remote object.</returns>
		// Token: 0x17000E1C RID: 3612
		// (get) Token: 0x06005127 RID: 20775 RVA: 0x0011578B File Offset: 0x0011398B
		public object[] Args
		{
			[SecurityCritical]
			get
			{
				return this._args;
			}
		}

		/// <summary>Gets a value indicating whether the called method accepts a variable number of arguments.</summary>
		/// <returns>
		///     <see langword="true" /> if the called method accepts a variable number of arguments; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E1D RID: 3613
		// (get) Token: 0x06005128 RID: 20776 RVA: 0x00115793 File Offset: 0x00113993
		public bool HasVarArgs
		{
			[SecurityCritical]
			get
			{
				return !(this._methodBase == null) && (this._methodBase.CallingConvention | CallingConventions.VarArgs) > (CallingConventions)0;
			}
		}

		/// <summary>Gets the <see cref="T:System.Runtime.Remoting.Messaging.LogicalCallContext" /> of the called method.</summary>
		/// <returns>The <see cref="T:System.Runtime.Remoting.Messaging.LogicalCallContext" /> of the called method.</returns>
		// Token: 0x17000E1E RID: 3614
		// (get) Token: 0x06005129 RID: 20777 RVA: 0x001157B5 File Offset: 0x001139B5
		public LogicalCallContext LogicalCallContext
		{
			[SecurityCritical]
			get
			{
				if (this._callCtx == null)
				{
					this._callCtx = new LogicalCallContext();
				}
				return this._callCtx;
			}
		}

		/// <summary>Gets the <see cref="T:System.Reflection.MethodBase" /> of the called method.</summary>
		/// <returns>The <see cref="T:System.Reflection.MethodBase" /> of the called method.</returns>
		// Token: 0x17000E1F RID: 3615
		// (get) Token: 0x0600512A RID: 20778 RVA: 0x001157D0 File Offset: 0x001139D0
		public MethodBase MethodBase
		{
			[SecurityCritical]
			get
			{
				return this._methodBase;
			}
		}

		/// <summary>Gets the name of the called method.</summary>
		/// <returns>The name of the method that the current <see cref="T:System.Runtime.Remoting.Messaging.ReturnMessage" /> originated from.</returns>
		// Token: 0x17000E20 RID: 3616
		// (get) Token: 0x0600512B RID: 20779 RVA: 0x001157D8 File Offset: 0x001139D8
		public string MethodName
		{
			[SecurityCritical]
			get
			{
				if (this._methodBase != null && this._methodName == null)
				{
					this._methodName = this._methodBase.Name;
				}
				return this._methodName;
			}
		}

		/// <summary>Gets an array of <see cref="T:System.Type" /> objects containing the method signature.</summary>
		/// <returns>An array of <see cref="T:System.Type" /> objects containing the method signature.</returns>
		// Token: 0x17000E21 RID: 3617
		// (get) Token: 0x0600512C RID: 20780 RVA: 0x00115808 File Offset: 0x00113A08
		public object MethodSignature
		{
			[SecurityCritical]
			get
			{
				if (this._methodBase != null && this._methodSignature == null)
				{
					ParameterInfo[] parameters = this._methodBase.GetParameters();
					this._methodSignature = new Type[parameters.Length];
					for (int i = 0; i < parameters.Length; i++)
					{
						this._methodSignature[i] = parameters[i].ParameterType;
					}
				}
				return this._methodSignature;
			}
		}

		/// <summary>Gets an <see cref="T:System.Collections.IDictionary" /> of properties contained in the current <see cref="T:System.Runtime.Remoting.Messaging.ReturnMessage" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionary" /> of properties contained in the current <see cref="T:System.Runtime.Remoting.Messaging.ReturnMessage" />.</returns>
		// Token: 0x17000E22 RID: 3618
		// (get) Token: 0x0600512D RID: 20781 RVA: 0x00115869 File Offset: 0x00113A69
		public virtual IDictionary Properties
		{
			[SecurityCritical]
			get
			{
				if (this._properties == null)
				{
					this._properties = new MethodReturnDictionary(this);
				}
				return this._properties;
			}
		}

		/// <summary>Gets the name of the type on which the remote method was called.</summary>
		/// <returns>The type name of the remote object on which the remote method was called.</returns>
		// Token: 0x17000E23 RID: 3619
		// (get) Token: 0x0600512E RID: 20782 RVA: 0x00115885 File Offset: 0x00113A85
		public string TypeName
		{
			[SecurityCritical]
			get
			{
				if (this._methodBase != null && this._typeName == null)
				{
					this._typeName = this._methodBase.DeclaringType.AssemblyQualifiedName;
				}
				return this._typeName;
			}
		}

		/// <summary>Gets or sets the URI of the remote object on which the remote method was called.</summary>
		/// <returns>The URI of the remote object on which the remote method was called.</returns>
		// Token: 0x17000E24 RID: 3620
		// (get) Token: 0x0600512F RID: 20783 RVA: 0x001158B9 File Offset: 0x00113AB9
		// (set) Token: 0x06005130 RID: 20784 RVA: 0x001158C1 File Offset: 0x00113AC1
		public string Uri
		{
			[SecurityCritical]
			get
			{
				return this._uri;
			}
			set
			{
				this._uri = value;
			}
		}

		// Token: 0x17000E25 RID: 3621
		// (get) Token: 0x06005131 RID: 20785 RVA: 0x001158CA File Offset: 0x00113ACA
		// (set) Token: 0x06005132 RID: 20786 RVA: 0x001158D2 File Offset: 0x00113AD2
		string IInternalMessage.Uri
		{
			get
			{
				return this.Uri;
			}
			set
			{
				this.Uri = value;
			}
		}

		/// <summary>Returns a specified argument passed to the remote method during the method call.</summary>
		/// <param name="argNum">The zero-based index of the requested argument. </param>
		/// <returns>An argument passed to the remote method during the method call.</returns>
		// Token: 0x06005133 RID: 20787 RVA: 0x001158DB File Offset: 0x00113ADB
		[SecurityCritical]
		public object GetArg(int argNum)
		{
			return this._args[argNum];
		}

		/// <summary>Returns the name of a specified method argument.</summary>
		/// <param name="index">The zero-based index of the requested argument name. </param>
		/// <returns>The name of a specified method argument.</returns>
		// Token: 0x06005134 RID: 20788 RVA: 0x001158E5 File Offset: 0x00113AE5
		[SecurityCritical]
		public string GetArgName(int index)
		{
			return this._methodBase.GetParameters()[index].Name;
		}

		/// <summary>Gets the exception that was thrown during the remote method call.</summary>
		/// <returns>The exception thrown during the method call, or <see langword="null" /> if an exception did not occur during the call.</returns>
		// Token: 0x17000E26 RID: 3622
		// (get) Token: 0x06005135 RID: 20789 RVA: 0x001158F9 File Offset: 0x00113AF9
		public Exception Exception
		{
			[SecurityCritical]
			get
			{
				return this._exception;
			}
		}

		/// <summary>Gets the number of <see langword="out" /> or <see langword="ref" /> arguments on the called method.</summary>
		/// <returns>The number of <see langword="out" /> or <see langword="ref" /> arguments on the called method.</returns>
		// Token: 0x17000E27 RID: 3623
		// (get) Token: 0x06005136 RID: 20790 RVA: 0x00115901 File Offset: 0x00113B01
		public int OutArgCount
		{
			[SecurityCritical]
			get
			{
				if (this._args == null || this._args.Length == 0)
				{
					return 0;
				}
				if (this._inArgInfo == null)
				{
					this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
				}
				return this._inArgInfo.GetInOutArgCount();
			}
		}

		/// <summary>Gets a specified object passed as an <see langword="out" /> or <see langword="ref" /> parameter to the called method.</summary>
		/// <returns>An object passed as an <see langword="out" /> or <see langword="ref" /> parameter to the called method.</returns>
		// Token: 0x17000E28 RID: 3624
		// (get) Token: 0x06005137 RID: 20791 RVA: 0x0011593C File Offset: 0x00113B3C
		public object[] OutArgs
		{
			[SecurityCritical]
			get
			{
				if (this._outArgs == null && this._args != null)
				{
					if (this._inArgInfo == null)
					{
						this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
					}
					this._outArgs = this._inArgInfo.GetInOutArgs(this._args);
				}
				return this._outArgs;
			}
		}

		/// <summary>Gets the object returned by the called method.</summary>
		/// <returns>The object returned by the called method.</returns>
		// Token: 0x17000E29 RID: 3625
		// (get) Token: 0x06005138 RID: 20792 RVA: 0x00115990 File Offset: 0x00113B90
		public virtual object ReturnValue
		{
			[SecurityCritical]
			get
			{
				return this._returnValue;
			}
		}

		/// <summary>Returns the object passed as an <see langword="out" /> or <see langword="ref" /> parameter during the remote method call.</summary>
		/// <param name="argNum">The zero-based index of the requested <see langword="out" /> or <see langword="ref" /> parameter. </param>
		/// <returns>The object passed as an <see langword="out" /> or <see langword="ref" /> parameter during the remote method call.</returns>
		// Token: 0x06005139 RID: 20793 RVA: 0x00115998 File Offset: 0x00113B98
		[SecurityCritical]
		public object GetOutArg(int argNum)
		{
			if (this._inArgInfo == null)
			{
				this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
			}
			return this._args[this._inArgInfo.GetInOutArgIndex(argNum)];
		}

		/// <summary>Returns the name of a specified <see langword="out" /> or <see langword="ref" /> parameter passed to the remote method.</summary>
		/// <param name="index">The zero-based index of the requested argument. </param>
		/// <returns>A string representing the name of the specified <see langword="out" /> or <see langword="ref" /> parameter, or <see langword="null" /> if the current method is not implemented.</returns>
		// Token: 0x0600513A RID: 20794 RVA: 0x001159C7 File Offset: 0x00113BC7
		[SecurityCritical]
		public string GetOutArgName(int index)
		{
			if (this._inArgInfo == null)
			{
				this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
			}
			return this._inArgInfo.GetInOutArgName(index);
		}

		// Token: 0x17000E2A RID: 3626
		// (get) Token: 0x0600513B RID: 20795 RVA: 0x001159EF File Offset: 0x00113BEF
		// (set) Token: 0x0600513C RID: 20796 RVA: 0x001159F7 File Offset: 0x00113BF7
		Identity IInternalMessage.TargetIdentity
		{
			get
			{
				return this._targetIdentity;
			}
			set
			{
				this._targetIdentity = value;
			}
		}

		// Token: 0x0600513D RID: 20797 RVA: 0x00115A00 File Offset: 0x00113C00
		bool IInternalMessage.HasProperties()
		{
			return this._properties != null;
		}

		// Token: 0x0600513E RID: 20798 RVA: 0x00115A00 File Offset: 0x00113C00
		internal bool HasProperties()
		{
			return this._properties != null;
		}

		// Token: 0x04002990 RID: 10640
		private object[] _outArgs;

		// Token: 0x04002991 RID: 10641
		private object[] _args;

		// Token: 0x04002992 RID: 10642
		private LogicalCallContext _callCtx;

		// Token: 0x04002993 RID: 10643
		private object _returnValue;

		// Token: 0x04002994 RID: 10644
		private string _uri;

		// Token: 0x04002995 RID: 10645
		private Exception _exception;

		// Token: 0x04002996 RID: 10646
		private MethodBase _methodBase;

		// Token: 0x04002997 RID: 10647
		private string _methodName;

		// Token: 0x04002998 RID: 10648
		private Type[] _methodSignature;

		// Token: 0x04002999 RID: 10649
		private string _typeName;

		// Token: 0x0400299A RID: 10650
		private MethodReturnDictionary _properties;

		// Token: 0x0400299B RID: 10651
		private Identity _targetIdentity;

		// Token: 0x0400299C RID: 10652
		private ArgInfo _inArgInfo;
	}
}
