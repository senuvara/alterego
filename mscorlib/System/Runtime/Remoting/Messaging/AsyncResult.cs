﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;

namespace System.Runtime.Remoting.Messaging
{
	/// <summary>Encapsulates the results of an asynchronous operation on a delegate.</summary>
	// Token: 0x02000808 RID: 2056
	[ComVisible(true)]
	[StructLayout(LayoutKind.Sequential)]
	public class AsyncResult : IAsyncResult, IMessageSink, IThreadPoolWorkItem
	{
		// Token: 0x06004FAB RID: 20395 RVA: 0x00002050 File Offset: 0x00000250
		internal AsyncResult()
		{
		}

		// Token: 0x06004FAC RID: 20396 RVA: 0x00111D38 File Offset: 0x0010FF38
		internal AsyncResult(WaitCallback cb, object state, bool capture_context)
		{
			this.orig_cb = cb;
			if (capture_context)
			{
				StackCrawlMark stackCrawlMark = StackCrawlMark.LookForMe;
				this.current = ExecutionContext.Capture(ref stackCrawlMark, ExecutionContext.CaptureOptions.IgnoreSyncCtx | ExecutionContext.CaptureOptions.OptimizeDefaultCase);
				cb = delegate(object <p0>)
				{
					ExecutionContext.Run(this.current, AsyncResult.ccb, this, true);
				};
			}
			this.async_state = state;
			this.async_delegate = cb;
		}

		// Token: 0x06004FAD RID: 20397 RVA: 0x00111D84 File Offset: 0x0010FF84
		private static void WaitCallback_Context(object state)
		{
			AsyncResult asyncResult = (AsyncResult)state;
			asyncResult.orig_cb(asyncResult.async_state);
		}

		/// <summary>Gets the object provided as the last parameter of a <see langword="BeginInvoke" /> method call.</summary>
		/// <returns>The object provided as the last parameter of a <see langword="BeginInvoke" /> method call.</returns>
		// Token: 0x17000D87 RID: 3463
		// (get) Token: 0x06004FAE RID: 20398 RVA: 0x00111DA9 File Offset: 0x0010FFA9
		public virtual object AsyncState
		{
			get
			{
				return this.async_state;
			}
		}

		/// <summary>Gets a <see cref="T:System.Threading.WaitHandle" /> that encapsulates Win32 synchronization handles, and allows the implementation of various synchronization schemes.</summary>
		/// <returns>A <see cref="T:System.Threading.WaitHandle" /> that encapsulates Win32 synchronization handles, and allows the implementation of various synchronization schemes.</returns>
		// Token: 0x17000D88 RID: 3464
		// (get) Token: 0x06004FAF RID: 20399 RVA: 0x00111DB4 File Offset: 0x0010FFB4
		public virtual WaitHandle AsyncWaitHandle
		{
			get
			{
				WaitHandle result;
				lock (this)
				{
					if (this.handle == null)
					{
						this.handle = new ManualResetEvent(this.completed);
					}
					result = this.handle;
				}
				return result;
			}
		}

		/// <summary>Gets a value indicating whether the <see langword="BeginInvoke" /> call completed synchronously.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see langword="BeginInvoke" /> call completed synchronously; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D89 RID: 3465
		// (get) Token: 0x06004FB0 RID: 20400 RVA: 0x00111E0C File Offset: 0x0011000C
		public virtual bool CompletedSynchronously
		{
			get
			{
				return this.sync_completed;
			}
		}

		/// <summary>Gets a value indicating whether the server has completed the call.</summary>
		/// <returns>
		///     <see langword="true" /> after the server has completed the call; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D8A RID: 3466
		// (get) Token: 0x06004FB1 RID: 20401 RVA: 0x00111E14 File Offset: 0x00110014
		public virtual bool IsCompleted
		{
			get
			{
				return this.completed;
			}
		}

		/// <summary>Gets or sets a value indicating whether <see langword="EndInvoke" /> has been called on the current <see cref="T:System.Runtime.Remoting.Messaging.AsyncResult" />.</summary>
		/// <returns>
		///     <see langword="true" /> if <see langword="EndInvoke" /> has been called on the current <see cref="T:System.Runtime.Remoting.Messaging.AsyncResult" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D8B RID: 3467
		// (get) Token: 0x06004FB2 RID: 20402 RVA: 0x00111E1C File Offset: 0x0011001C
		// (set) Token: 0x06004FB3 RID: 20403 RVA: 0x00111E24 File Offset: 0x00110024
		public bool EndInvokeCalled
		{
			get
			{
				return this.endinvoke_called;
			}
			set
			{
				this.endinvoke_called = value;
			}
		}

		/// <summary>Gets the delegate object on which the asynchronous call was invoked.</summary>
		/// <returns>The delegate object on which the asynchronous call was invoked.</returns>
		// Token: 0x17000D8C RID: 3468
		// (get) Token: 0x06004FB4 RID: 20404 RVA: 0x00111E2D File Offset: 0x0011002D
		public virtual object AsyncDelegate
		{
			get
			{
				return this.async_delegate;
			}
		}

		/// <summary>Gets the next message sink in the sink chain.</summary>
		/// <returns>An <see cref="T:System.Runtime.Remoting.Messaging.IMessageSink" /> interface that represents the next message sink in the sink chain.</returns>
		// Token: 0x17000D8D RID: 3469
		// (get) Token: 0x06004FB5 RID: 20405 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public IMessageSink NextSink
		{
			[SecurityCritical]
			get
			{
				return null;
			}
		}

		/// <summary>Implements the <see cref="T:System.Runtime.Remoting.Messaging.IMessageSink" /> interface.</summary>
		/// <param name="msg">The request <see cref="T:System.Runtime.Remoting.Messaging.IMessage" /> interface. </param>
		/// <param name="replySink">The response <see cref="T:System.Runtime.Remoting.Messaging.IMessageSink" /> interface. </param>
		/// <returns>No value is returned.</returns>
		// Token: 0x06004FB6 RID: 20406 RVA: 0x000175EA File Offset: 0x000157EA
		[SecurityCritical]
		public virtual IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new NotSupportedException();
		}

		/// <summary>Gets the response message for the asynchronous call.</summary>
		/// <returns>A remoting message that should represent a response to a method call on a remote object.</returns>
		// Token: 0x06004FB7 RID: 20407 RVA: 0x00111E35 File Offset: 0x00110035
		public virtual IMessage GetReplyMessage()
		{
			return this.reply_message;
		}

		/// <summary>Sets an <see cref="T:System.Runtime.Remoting.Messaging.IMessageCtrl" /> for the current remote method call, which provides a way to control asynchronous messages after they have been dispatched.</summary>
		/// <param name="mc">The <see cref="T:System.Runtime.Remoting.Messaging.IMessageCtrl" /> for the current remote method call. </param>
		// Token: 0x06004FB8 RID: 20408 RVA: 0x00111E3D File Offset: 0x0011003D
		public virtual void SetMessageCtrl(IMessageCtrl mc)
		{
			this.message_ctrl = mc;
		}

		// Token: 0x06004FB9 RID: 20409 RVA: 0x00111E46 File Offset: 0x00110046
		internal void SetCompletedSynchronously(bool completed)
		{
			this.sync_completed = completed;
		}

		// Token: 0x06004FBA RID: 20410 RVA: 0x00111E50 File Offset: 0x00110050
		internal IMessage EndInvoke()
		{
			lock (this)
			{
				if (this.completed)
				{
					return this.reply_message;
				}
			}
			this.AsyncWaitHandle.WaitOne();
			return this.reply_message;
		}

		/// <summary>Synchronously processes a response message returned by a method call on a remote object.</summary>
		/// <param name="msg">A response message to a method call on a remote object.</param>
		/// <returns>Returns <see langword="null" />.</returns>
		// Token: 0x06004FBB RID: 20411 RVA: 0x00111EAC File Offset: 0x001100AC
		[SecurityCritical]
		public virtual IMessage SyncProcessMessage(IMessage msg)
		{
			this.reply_message = msg;
			lock (this)
			{
				this.completed = true;
				if (this.handle != null)
				{
					((ManualResetEvent)this.AsyncWaitHandle).Set();
				}
			}
			if (this.async_callback != null)
			{
				((AsyncCallback)this.async_callback)(this);
			}
			return null;
		}

		// Token: 0x17000D8E RID: 3470
		// (get) Token: 0x06004FBC RID: 20412 RVA: 0x00111F24 File Offset: 0x00110124
		// (set) Token: 0x06004FBD RID: 20413 RVA: 0x00111F2C File Offset: 0x0011012C
		internal MonoMethodMessage CallMessage
		{
			get
			{
				return this.call_message;
			}
			set
			{
				this.call_message = value;
			}
		}

		// Token: 0x06004FBE RID: 20414 RVA: 0x00111F35 File Offset: 0x00110135
		void IThreadPoolWorkItem.ExecuteWorkItem()
		{
			this.Invoke();
		}

		// Token: 0x06004FBF RID: 20415 RVA: 0x000020D3 File Offset: 0x000002D3
		void IThreadPoolWorkItem.MarkAborted(ThreadAbortException tae)
		{
		}

		// Token: 0x06004FC0 RID: 20416
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern object Invoke();

		// Token: 0x06004FC1 RID: 20417 RVA: 0x00111F3E File Offset: 0x0011013E
		// Note: this type is marked as 'beforefieldinit'.
		static AsyncResult()
		{
		}

		// Token: 0x06004FC2 RID: 20418 RVA: 0x00111F51 File Offset: 0x00110151
		[CompilerGenerated]
		private void <.ctor>b__17_0(object <p0>)
		{
			ExecutionContext.Run(this.current, AsyncResult.ccb, this, true);
		}

		// Token: 0x04002910 RID: 10512
		private object async_state;

		// Token: 0x04002911 RID: 10513
		private WaitHandle handle;

		// Token: 0x04002912 RID: 10514
		private object async_delegate;

		// Token: 0x04002913 RID: 10515
		private IntPtr data;

		// Token: 0x04002914 RID: 10516
		private object object_data;

		// Token: 0x04002915 RID: 10517
		private bool sync_completed;

		// Token: 0x04002916 RID: 10518
		private bool completed;

		// Token: 0x04002917 RID: 10519
		private bool endinvoke_called;

		// Token: 0x04002918 RID: 10520
		private object async_callback;

		// Token: 0x04002919 RID: 10521
		private ExecutionContext current;

		// Token: 0x0400291A RID: 10522
		private ExecutionContext original;

		// Token: 0x0400291B RID: 10523
		private long add_time;

		// Token: 0x0400291C RID: 10524
		private MonoMethodMessage call_message;

		// Token: 0x0400291D RID: 10525
		private IMessageCtrl message_ctrl;

		// Token: 0x0400291E RID: 10526
		private IMessage reply_message;

		// Token: 0x0400291F RID: 10527
		private WaitCallback orig_cb;

		// Token: 0x04002920 RID: 10528
		internal static ContextCallback ccb = new ContextCallback(AsyncResult.WaitCallback_Context);
	}
}
