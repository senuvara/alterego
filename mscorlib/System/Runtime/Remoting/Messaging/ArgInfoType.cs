﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000806 RID: 2054
	internal enum ArgInfoType : byte
	{
		// Token: 0x0400290B RID: 10507
		In,
		// Token: 0x0400290C RID: 10508
		Out
	}
}
