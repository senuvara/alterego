﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000818 RID: 2072
	internal interface IInternalMessage
	{
		// Token: 0x17000DAC RID: 3500
		// (get) Token: 0x06005025 RID: 20517
		// (set) Token: 0x06005026 RID: 20518
		Identity TargetIdentity { get; set; }

		// Token: 0x17000DAD RID: 3501
		// (get) Token: 0x06005027 RID: 20519
		// (set) Token: 0x06005028 RID: 20520
		string Uri { get; set; }

		// Token: 0x06005029 RID: 20521
		bool HasProperties();
	}
}
