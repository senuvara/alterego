﻿using System;
using System.Runtime.Remoting.Activation;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000834 RID: 2100
	internal class ServerContextTerminatorSink : IMessageSink
	{
		// Token: 0x0600513F RID: 20799 RVA: 0x00115A0B File Offset: 0x00113C0B
		public IMessage SyncProcessMessage(IMessage msg)
		{
			if (msg is IConstructionCallMessage)
			{
				return ActivationServices.CreateInstanceFromMessage((IConstructionCallMessage)msg);
			}
			return ((ServerIdentity)RemotingServices.GetMessageTargetIdentity(msg)).SyncObjectProcessMessage(msg);
		}

		// Token: 0x06005140 RID: 20800 RVA: 0x00115A32 File Offset: 0x00113C32
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			return ((ServerIdentity)RemotingServices.GetMessageTargetIdentity(msg)).AsyncObjectProcessMessage(msg, replySink);
		}

		// Token: 0x17000E2B RID: 3627
		// (get) Token: 0x06005141 RID: 20801 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public IMessageSink NextSink
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06005142 RID: 20802 RVA: 0x00002050 File Offset: 0x00000250
		public ServerContextTerminatorSink()
		{
		}
	}
}
