﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000830 RID: 2096
	internal class RemotingSurrogate : ISerializationSurrogate
	{
		// Token: 0x06005114 RID: 20756 RVA: 0x001155C8 File Offset: 0x001137C8
		public virtual void GetObjectData(object obj, SerializationInfo si, StreamingContext sc)
		{
			if (obj == null || si == null)
			{
				throw new ArgumentNullException();
			}
			if (RemotingServices.IsTransparentProxy(obj))
			{
				RemotingServices.GetRealProxy(obj).GetObjectData(si, sc);
				return;
			}
			RemotingServices.GetObjectData(obj, si, sc);
		}

		// Token: 0x06005115 RID: 20757 RVA: 0x000175EA File Offset: 0x000157EA
		public virtual object SetObjectData(object obj, SerializationInfo si, StreamingContext sc, ISurrogateSelector selector)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06005116 RID: 20758 RVA: 0x00002050 File Offset: 0x00000250
		public RemotingSurrogate()
		{
		}
	}
}
