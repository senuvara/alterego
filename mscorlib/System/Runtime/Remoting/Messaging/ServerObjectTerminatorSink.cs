﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000835 RID: 2101
	internal class ServerObjectTerminatorSink : IMessageSink
	{
		// Token: 0x06005143 RID: 20803 RVA: 0x00115A46 File Offset: 0x00113C46
		public ServerObjectTerminatorSink(IMessageSink nextSink)
		{
			this._nextSink = nextSink;
		}

		// Token: 0x06005144 RID: 20804 RVA: 0x00115A58 File Offset: 0x00113C58
		public IMessage SyncProcessMessage(IMessage msg)
		{
			ServerIdentity serverIdentity = (ServerIdentity)RemotingServices.GetMessageTargetIdentity(msg);
			serverIdentity.NotifyServerDynamicSinks(true, msg, false, false);
			IMessage result = this._nextSink.SyncProcessMessage(msg);
			serverIdentity.NotifyServerDynamicSinks(false, msg, false, false);
			return result;
		}

		// Token: 0x06005145 RID: 20805 RVA: 0x00115A94 File Offset: 0x00113C94
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			ServerIdentity serverIdentity = (ServerIdentity)RemotingServices.GetMessageTargetIdentity(msg);
			if (serverIdentity.HasServerDynamicSinks)
			{
				serverIdentity.NotifyServerDynamicSinks(true, msg, false, true);
				if (replySink != null)
				{
					replySink = new ServerObjectReplySink(serverIdentity, replySink);
				}
			}
			IMessageCtrl result = this._nextSink.AsyncProcessMessage(msg, replySink);
			if (replySink == null)
			{
				serverIdentity.NotifyServerDynamicSinks(false, msg, true, true);
			}
			return result;
		}

		// Token: 0x17000E2C RID: 3628
		// (get) Token: 0x06005146 RID: 20806 RVA: 0x00115AE5 File Offset: 0x00113CE5
		public IMessageSink NextSink
		{
			get
			{
				return this._nextSink;
			}
		}

		// Token: 0x0400299D RID: 10653
		private IMessageSink _nextSink;
	}
}
