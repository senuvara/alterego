﻿using System;
using System.Security.Principal;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000804 RID: 2052
	[Serializable]
	internal class CallContextSecurityData : ICloneable
	{
		// Token: 0x17000D83 RID: 3459
		// (get) Token: 0x06004F9C RID: 20380 RVA: 0x00111BBC File Offset: 0x0010FDBC
		// (set) Token: 0x06004F9D RID: 20381 RVA: 0x00111BC4 File Offset: 0x0010FDC4
		internal IPrincipal Principal
		{
			get
			{
				return this._principal;
			}
			set
			{
				this._principal = value;
			}
		}

		// Token: 0x17000D84 RID: 3460
		// (get) Token: 0x06004F9E RID: 20382 RVA: 0x00111BCD File Offset: 0x0010FDCD
		internal bool HasInfo
		{
			get
			{
				return this._principal != null;
			}
		}

		// Token: 0x06004F9F RID: 20383 RVA: 0x00111BD8 File Offset: 0x0010FDD8
		public object Clone()
		{
			return new CallContextSecurityData
			{
				_principal = this._principal
			};
		}

		// Token: 0x06004FA0 RID: 20384 RVA: 0x00002050 File Offset: 0x00000250
		public CallContextSecurityData()
		{
		}

		// Token: 0x04002908 RID: 10504
		private IPrincipal _principal;
	}
}
