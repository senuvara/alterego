﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	/// <summary>Wraps remoting data for passing between message sinks, either for requests from client to server or for the subsequent responses.</summary>
	// Token: 0x02000821 RID: 2081
	[ComVisible(true)]
	public class InternalMessageWrapper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Messaging.InternalMessageWrapper" /> class. </summary>
		/// <param name="msg">A message that acts either as an outgoing method call on a remote object, or as the subsequent response.</param>
		// Token: 0x06005047 RID: 20551 RVA: 0x001130C5 File Offset: 0x001112C5
		public InternalMessageWrapper(IMessage msg)
		{
			this.WrappedMessage = msg;
		}

		/// <summary>Represents the request or response <see cref="T:System.Runtime.Remoting.Messaging.IMethodMessage" /> interface that is wrapped by the message wrapper. </summary>
		// Token: 0x04002944 RID: 10564
		protected IMessage WrappedMessage;
	}
}
