﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200080A RID: 2058
	internal class CADObjRef
	{
		// Token: 0x06004FC4 RID: 20420 RVA: 0x00111F74 File Offset: 0x00110174
		public CADObjRef(ObjRef o, int sourceDomain)
		{
			this.objref = o;
			this.TypeInfo = o.SerializeType();
			this.SourceDomain = sourceDomain;
		}

		// Token: 0x17000D8F RID: 3471
		// (get) Token: 0x06004FC5 RID: 20421 RVA: 0x00111F96 File Offset: 0x00110196
		public string TypeName
		{
			get
			{
				return this.objref.TypeInfo.TypeName;
			}
		}

		// Token: 0x17000D90 RID: 3472
		// (get) Token: 0x06004FC6 RID: 20422 RVA: 0x00111FA8 File Offset: 0x001101A8
		public string URI
		{
			get
			{
				return this.objref.URI;
			}
		}

		// Token: 0x04002922 RID: 10530
		internal ObjRef objref;

		// Token: 0x04002923 RID: 10531
		internal int SourceDomain;

		// Token: 0x04002924 RID: 10532
		internal byte[] TypeInfo;
	}
}
