﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000831 RID: 2097
	internal class ObjRefSurrogate : ISerializationSurrogate
	{
		// Token: 0x06005117 RID: 20759 RVA: 0x001155F4 File Offset: 0x001137F4
		public virtual void GetObjectData(object obj, SerializationInfo si, StreamingContext sc)
		{
			if (obj == null || si == null)
			{
				throw new ArgumentNullException();
			}
			((ObjRef)obj).GetObjectData(si, sc);
			si.AddValue("fIsMarshalled", 0);
		}

		// Token: 0x06005118 RID: 20760 RVA: 0x0011561B File Offset: 0x0011381B
		public virtual object SetObjectData(object obj, SerializationInfo si, StreamingContext sc, ISurrogateSelector selector)
		{
			throw new NotSupportedException("Do not use RemotingSurrogateSelector when deserializating");
		}

		// Token: 0x06005119 RID: 20761 RVA: 0x00002050 File Offset: 0x00000250
		public ObjRefSurrogate()
		{
		}
	}
}
