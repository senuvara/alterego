﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200082E RID: 2094
	internal enum CallType
	{
		// Token: 0x04002986 RID: 10630
		Sync,
		// Token: 0x04002987 RID: 10631
		BeginInvoke,
		// Token: 0x04002988 RID: 10632
		EndInvoke,
		// Token: 0x04002989 RID: 10633
		OneWay
	}
}
