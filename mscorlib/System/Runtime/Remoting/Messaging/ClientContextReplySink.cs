﻿using System;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000810 RID: 2064
	internal class ClientContextReplySink : IMessageSink
	{
		// Token: 0x06004FE7 RID: 20455 RVA: 0x00112BE7 File Offset: 0x00110DE7
		public ClientContextReplySink(Context ctx, IMessageSink replySink)
		{
			this._replySink = replySink;
			this._context = ctx;
		}

		// Token: 0x06004FE8 RID: 20456 RVA: 0x00112BFD File Offset: 0x00110DFD
		public IMessage SyncProcessMessage(IMessage msg)
		{
			Context.NotifyGlobalDynamicSinks(false, msg, true, true);
			this._context.NotifyDynamicSinks(false, msg, true, true);
			return this._replySink.SyncProcessMessage(msg);
		}

		// Token: 0x06004FE9 RID: 20457 RVA: 0x000175EA File Offset: 0x000157EA
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000D95 RID: 3477
		// (get) Token: 0x06004FEA RID: 20458 RVA: 0x00112C23 File Offset: 0x00110E23
		public IMessageSink NextSink
		{
			get
			{
				return this._replySink;
			}
		}

		// Token: 0x04002934 RID: 10548
		private IMessageSink _replySink;

		// Token: 0x04002935 RID: 10549
		private Context _context;
	}
}
