﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Runtime.Remoting.Messaging
{
	/// <summary>Implements the <see cref="T:System.Runtime.Remoting.Messaging.IMethodReturnMessage" /> interface to create a message that acts as a response to a method call on a remote object.</summary>
	// Token: 0x0200082B RID: 2091
	[ComVisible(true)]
	public class MethodReturnMessageWrapper : InternalMessageWrapper, IMethodReturnMessage, IMethodMessage, IMessage
	{
		/// <summary>Wraps an <see cref="T:System.Runtime.Remoting.Messaging.IMethodReturnMessage" /> to create a <see cref="T:System.Runtime.Remoting.Messaging.MethodReturnMessageWrapper" />. </summary>
		/// <param name="msg">A message that acts as an outgoing method call on a remote object.</param>
		// Token: 0x060050D2 RID: 20690 RVA: 0x00114D48 File Offset: 0x00112F48
		public MethodReturnMessageWrapper(IMethodReturnMessage msg) : base(msg)
		{
			if (msg.Exception != null)
			{
				this._exception = msg.Exception;
				this._args = new object[0];
				return;
			}
			this._args = msg.Args;
			this._return = msg.ReturnValue;
			if (msg.MethodBase != null)
			{
				this._outArgInfo = new ArgInfo(msg.MethodBase, ArgInfoType.Out);
			}
		}

		/// <summary>Gets the number of arguments passed to the method. </summary>
		/// <returns>A <see cref="T:System.Int32" /> that represents the number of arguments passed to a method.</returns>
		// Token: 0x17000DF8 RID: 3576
		// (get) Token: 0x060050D3 RID: 20691 RVA: 0x00114DB5 File Offset: 0x00112FB5
		public virtual int ArgCount
		{
			[SecurityCritical]
			get
			{
				return this._args.Length;
			}
		}

		/// <summary>Gets an array of arguments passed to the method. </summary>
		/// <returns>An array of type <see cref="T:System.Object" /> that represents the arguments passed to a method.</returns>
		// Token: 0x17000DF9 RID: 3577
		// (get) Token: 0x060050D4 RID: 20692 RVA: 0x00114DBF File Offset: 0x00112FBF
		// (set) Token: 0x060050D5 RID: 20693 RVA: 0x00114DC7 File Offset: 0x00112FC7
		public virtual object[] Args
		{
			[SecurityCritical]
			get
			{
				return this._args;
			}
			set
			{
				this._args = value;
			}
		}

		/// <summary>Gets the exception thrown during the method call, or <see langword="null" /> if the method did not throw an exception. </summary>
		/// <returns>The <see cref="T:System.Exception" /> thrown during the method call, or <see langword="null" /> if the method did not throw an exception.</returns>
		// Token: 0x17000DFA RID: 3578
		// (get) Token: 0x060050D6 RID: 20694 RVA: 0x00114DD0 File Offset: 0x00112FD0
		// (set) Token: 0x060050D7 RID: 20695 RVA: 0x00114DD8 File Offset: 0x00112FD8
		public virtual Exception Exception
		{
			[SecurityCritical]
			get
			{
				return this._exception;
			}
			set
			{
				this._exception = value;
			}
		}

		/// <summary>Gets a flag that indicates whether the method can accept a variable number of arguments. </summary>
		/// <returns>
		///     <see langword="true" /> if the method can accept a variable number of arguments; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000DFB RID: 3579
		// (get) Token: 0x060050D8 RID: 20696 RVA: 0x00114DE1 File Offset: 0x00112FE1
		public virtual bool HasVarArgs
		{
			[SecurityCritical]
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).HasVarArgs;
			}
		}

		/// <summary>Gets the <see cref="T:System.Runtime.Remoting.Messaging.LogicalCallContext" /> for the current method call. </summary>
		/// <returns>The <see cref="T:System.Runtime.Remoting.Messaging.LogicalCallContext" /> for the current method call.</returns>
		// Token: 0x17000DFC RID: 3580
		// (get) Token: 0x060050D9 RID: 20697 RVA: 0x00114DF3 File Offset: 0x00112FF3
		public virtual LogicalCallContext LogicalCallContext
		{
			[SecurityCritical]
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).LogicalCallContext;
			}
		}

		/// <summary>Gets the <see cref="T:System.Reflection.MethodBase" /> of the called method. </summary>
		/// <returns>The <see cref="T:System.Reflection.MethodBase" /> of the called method.</returns>
		// Token: 0x17000DFD RID: 3581
		// (get) Token: 0x060050DA RID: 20698 RVA: 0x00114E05 File Offset: 0x00113005
		public virtual MethodBase MethodBase
		{
			[SecurityCritical]
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).MethodBase;
			}
		}

		/// <summary>Gets the name of the invoked method. </summary>
		/// <returns>A <see cref="T:System.String" /> that contains the name of the invoked method.</returns>
		// Token: 0x17000DFE RID: 3582
		// (get) Token: 0x060050DB RID: 20699 RVA: 0x00114E17 File Offset: 0x00113017
		public virtual string MethodName
		{
			[SecurityCritical]
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).MethodName;
			}
		}

		/// <summary>Gets an object that contains the method signature. </summary>
		/// <returns>A <see cref="T:System.Object" /> that contains the method signature.</returns>
		// Token: 0x17000DFF RID: 3583
		// (get) Token: 0x060050DC RID: 20700 RVA: 0x00114E29 File Offset: 0x00113029
		public virtual object MethodSignature
		{
			[SecurityCritical]
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).MethodSignature;
			}
		}

		/// <summary>Gets the number of arguments in the method call that are marked as <see langword="ref" /> parameters or <see langword="out" /> parameters. </summary>
		/// <returns>A <see cref="T:System.Int32" /> that represents the number of arguments in the method call that are marked as <see langword="ref" /> parameters or <see langword="out" /> parameters.</returns>
		// Token: 0x17000E00 RID: 3584
		// (get) Token: 0x060050DD RID: 20701 RVA: 0x00114E3B File Offset: 0x0011303B
		public virtual int OutArgCount
		{
			[SecurityCritical]
			get
			{
				if (this._outArgInfo == null)
				{
					return 0;
				}
				return this._outArgInfo.GetInOutArgCount();
			}
		}

		/// <summary>Gets an array of arguments in the method call that are marked as <see langword="ref" /> parameters or <see langword="out" /> parameters. </summary>
		/// <returns>An array of type <see cref="T:System.Object" /> that represents the arguments in the method call that are marked as <see langword="ref" /> parameters or <see langword="out" /> parameters.</returns>
		// Token: 0x17000E01 RID: 3585
		// (get) Token: 0x060050DE RID: 20702 RVA: 0x00114E52 File Offset: 0x00113052
		public virtual object[] OutArgs
		{
			[SecurityCritical]
			get
			{
				if (this._outArgInfo == null)
				{
					return this._args;
				}
				return this._outArgInfo.GetInOutArgs(this._args);
			}
		}

		/// <summary>An <see cref="T:System.Collections.IDictionary" /> interface that represents a collection of the remoting message's properties. </summary>
		/// <returns>An <see cref="T:System.Collections.IDictionary" /> interface that represents a collection of the remoting message's properties.</returns>
		// Token: 0x17000E02 RID: 3586
		// (get) Token: 0x060050DF RID: 20703 RVA: 0x00114E74 File Offset: 0x00113074
		public virtual IDictionary Properties
		{
			[SecurityCritical]
			get
			{
				if (this._properties == null)
				{
					this._properties = new MethodReturnMessageWrapper.DictionaryWrapper(this, this.WrappedMessage.Properties);
				}
				return this._properties;
			}
		}

		/// <summary>Gets the return value of the method call. </summary>
		/// <returns>A <see cref="T:System.Object" /> that represents the return value of the method call.</returns>
		// Token: 0x17000E03 RID: 3587
		// (get) Token: 0x060050E0 RID: 20704 RVA: 0x00114E9B File Offset: 0x0011309B
		// (set) Token: 0x060050E1 RID: 20705 RVA: 0x00114EA3 File Offset: 0x001130A3
		public virtual object ReturnValue
		{
			[SecurityCritical]
			get
			{
				return this._return;
			}
			set
			{
				this._return = value;
			}
		}

		/// <summary>Gets the full type name of the remote object on which the method call is being made. </summary>
		/// <returns>A <see cref="T:System.String" /> that contains the full type name of the remote object on which the method call is being made.</returns>
		// Token: 0x17000E04 RID: 3588
		// (get) Token: 0x060050E2 RID: 20706 RVA: 0x00114EAC File Offset: 0x001130AC
		public virtual string TypeName
		{
			[SecurityCritical]
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).TypeName;
			}
		}

		/// <summary>Gets the Uniform Resource Identifier (URI) of the remote object on which the method call is being made. </summary>
		/// <returns>The URI of a remote object.</returns>
		// Token: 0x17000E05 RID: 3589
		// (get) Token: 0x060050E3 RID: 20707 RVA: 0x00114EBE File Offset: 0x001130BE
		// (set) Token: 0x060050E4 RID: 20708 RVA: 0x00114ED0 File Offset: 0x001130D0
		public string Uri
		{
			[SecurityCritical]
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).Uri;
			}
			set
			{
				this.Properties["__Uri"] = value;
			}
		}

		/// <summary>Gets a method argument, as an object, at a specified index. </summary>
		/// <param name="argNum">The index of the requested argument.</param>
		/// <returns>The method argument as an object.</returns>
		// Token: 0x060050E5 RID: 20709 RVA: 0x00114EE3 File Offset: 0x001130E3
		[SecurityCritical]
		public virtual object GetArg(int argNum)
		{
			return this._args[argNum];
		}

		/// <summary>Gets the name of a method argument at a specified index. </summary>
		/// <param name="index">The index of the requested argument.</param>
		/// <returns>The name of the method argument.</returns>
		// Token: 0x060050E6 RID: 20710 RVA: 0x00114EED File Offset: 0x001130ED
		[SecurityCritical]
		public virtual string GetArgName(int index)
		{
			return ((IMethodReturnMessage)this.WrappedMessage).GetArgName(index);
		}

		/// <summary>Returns the specified argument marked as a <see langword="ref" /> parameter or an <see langword="out" /> parameter. </summary>
		/// <param name="argNum">The index of the requested argument.</param>
		/// <returns>The specified argument marked as a <see langword="ref" /> parameter or an <see langword="out" /> parameter.</returns>
		// Token: 0x060050E7 RID: 20711 RVA: 0x00114F00 File Offset: 0x00113100
		[SecurityCritical]
		public virtual object GetOutArg(int argNum)
		{
			return this._args[this._outArgInfo.GetInOutArgIndex(argNum)];
		}

		/// <summary>Returns the name of the specified argument marked as a <see langword="ref" /> parameter or an <see langword="out" /> parameter. </summary>
		/// <param name="index">The index of the requested argument.</param>
		/// <returns>The argument name, or <see langword="null" /> if the current method is not implemented.</returns>
		// Token: 0x060050E8 RID: 20712 RVA: 0x00114F15 File Offset: 0x00113115
		[SecurityCritical]
		public virtual string GetOutArgName(int index)
		{
			return this._outArgInfo.GetInOutArgName(index);
		}

		// Token: 0x0400296F RID: 10607
		private object[] _args;

		// Token: 0x04002970 RID: 10608
		private ArgInfo _outArgInfo;

		// Token: 0x04002971 RID: 10609
		private MethodReturnMessageWrapper.DictionaryWrapper _properties;

		// Token: 0x04002972 RID: 10610
		private Exception _exception;

		// Token: 0x04002973 RID: 10611
		private object _return;

		// Token: 0x0200082C RID: 2092
		private class DictionaryWrapper : MethodReturnDictionary
		{
			// Token: 0x060050E9 RID: 20713 RVA: 0x00114F23 File Offset: 0x00113123
			public DictionaryWrapper(IMethodReturnMessage message, IDictionary wrappedDictionary) : base(message)
			{
				this._wrappedDictionary = wrappedDictionary;
				base.MethodKeys = MethodReturnMessageWrapper.DictionaryWrapper._keys;
			}

			// Token: 0x060050EA RID: 20714 RVA: 0x00114F3E File Offset: 0x0011313E
			protected override IDictionary AllocInternalProperties()
			{
				return this._wrappedDictionary;
			}

			// Token: 0x060050EB RID: 20715 RVA: 0x00114F48 File Offset: 0x00113148
			protected override void SetMethodProperty(string key, object value)
			{
				if (key == "__Args")
				{
					((MethodReturnMessageWrapper)this._message)._args = (object[])value;
					return;
				}
				if (key == "__Return")
				{
					((MethodReturnMessageWrapper)this._message)._return = value;
					return;
				}
				base.SetMethodProperty(key, value);
			}

			// Token: 0x060050EC RID: 20716 RVA: 0x00114FA0 File Offset: 0x001131A0
			protected override object GetMethodProperty(string key)
			{
				if (key == "__Args")
				{
					return ((MethodReturnMessageWrapper)this._message)._args;
				}
				if (key == "__Return")
				{
					return ((MethodReturnMessageWrapper)this._message)._return;
				}
				return base.GetMethodProperty(key);
			}

			// Token: 0x060050ED RID: 20717 RVA: 0x00114FF0 File Offset: 0x001131F0
			// Note: this type is marked as 'beforefieldinit'.
			static DictionaryWrapper()
			{
			}

			// Token: 0x04002974 RID: 10612
			private IDictionary _wrappedDictionary;

			// Token: 0x04002975 RID: 10613
			private static string[] _keys = new string[]
			{
				"__Args",
				"__Return"
			};
		}
	}
}
