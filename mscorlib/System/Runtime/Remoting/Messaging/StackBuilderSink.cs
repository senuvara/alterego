﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Proxies;
using System.Threading;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000837 RID: 2103
	internal class StackBuilderSink : IMessageSink
	{
		// Token: 0x0600514B RID: 20811 RVA: 0x00115B28 File Offset: 0x00113D28
		public StackBuilderSink(MarshalByRefObject obj, bool forceInternalExecute)
		{
			this._target = obj;
			if (!forceInternalExecute && RemotingServices.IsTransparentProxy(obj))
			{
				this._rp = RemotingServices.GetRealProxy(obj);
			}
		}

		// Token: 0x0600514C RID: 20812 RVA: 0x00115B4E File Offset: 0x00113D4E
		public IMessage SyncProcessMessage(IMessage msg)
		{
			this.CheckParameters(msg);
			if (this._rp != null)
			{
				return this._rp.Invoke(msg);
			}
			return RemotingServices.InternalExecuteMessage(this._target, (IMethodCallMessage)msg);
		}

		// Token: 0x0600514D RID: 20813 RVA: 0x00115B80 File Offset: 0x00113D80
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			object[] state = new object[]
			{
				msg,
				replySink
			};
			ThreadPool.QueueUserWorkItem(delegate(object data)
			{
				try
				{
					this.ExecuteAsyncMessage(data);
				}
				catch
				{
				}
			}, state);
			return null;
		}

		// Token: 0x0600514E RID: 20814 RVA: 0x00115BB0 File Offset: 0x00113DB0
		private void ExecuteAsyncMessage(object ob)
		{
			object[] array = (object[])ob;
			IMethodCallMessage methodCallMessage = (IMethodCallMessage)array[0];
			IMessageSink messageSink = (IMessageSink)array[1];
			this.CheckParameters(methodCallMessage);
			IMessage msg;
			if (this._rp != null)
			{
				msg = this._rp.Invoke(methodCallMessage);
			}
			else
			{
				msg = RemotingServices.InternalExecuteMessage(this._target, methodCallMessage);
			}
			messageSink.SyncProcessMessage(msg);
		}

		// Token: 0x17000E2E RID: 3630
		// (get) Token: 0x0600514F RID: 20815 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public IMessageSink NextSink
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06005150 RID: 20816 RVA: 0x00115C08 File Offset: 0x00113E08
		private void CheckParameters(IMessage msg)
		{
			IMethodCallMessage methodCallMessage = (IMethodCallMessage)msg;
			ParameterInfo[] parameters = methodCallMessage.MethodBase.GetParameters();
			int num = 0;
			foreach (ParameterInfo parameterInfo in parameters)
			{
				object arg = methodCallMessage.GetArg(num++);
				Type type = parameterInfo.ParameterType;
				if (type.IsByRef)
				{
					type = type.GetElementType();
				}
				if (arg != null && !type.IsInstanceOfType(arg))
				{
					throw new RemotingException(string.Concat(new object[]
					{
						"Cannot cast argument ",
						parameterInfo.Position,
						" of type '",
						arg.GetType().AssemblyQualifiedName,
						"' to type '",
						type.AssemblyQualifiedName,
						"'"
					}));
				}
			}
		}

		// Token: 0x06005151 RID: 20817 RVA: 0x00115CD4 File Offset: 0x00113ED4
		[CompilerGenerated]
		private void <AsyncProcessMessage>b__4_0(object data)
		{
			try
			{
				this.ExecuteAsyncMessage(data);
			}
			catch
			{
			}
		}

		// Token: 0x040029A0 RID: 10656
		private MarshalByRefObject _target;

		// Token: 0x040029A1 RID: 10657
		private RealProxy _rp;
	}
}
