﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Remoting.Channels;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200080E RID: 2062
	internal class CADMethodReturnMessage : CADMessageBase
	{
		// Token: 0x06004FDC RID: 20444 RVA: 0x001129A0 File Offset: 0x00110BA0
		internal static CADMethodReturnMessage Create(IMessage callMsg)
		{
			IMethodReturnMessage methodReturnMessage = callMsg as IMethodReturnMessage;
			if (methodReturnMessage == null)
			{
				return null;
			}
			return new CADMethodReturnMessage(methodReturnMessage);
		}

		// Token: 0x06004FDD RID: 20445 RVA: 0x001129C0 File Offset: 0x00110BC0
		internal CADMethodReturnMessage(IMethodReturnMessage retMsg) : base(retMsg)
		{
			ArrayList arrayList = null;
			this._propertyCount = CADMessageBase.MarshalProperties(retMsg.Properties, ref arrayList);
			this._returnValue = base.MarshalArgument(retMsg.ReturnValue, ref arrayList);
			this._args = base.MarshalArguments(retMsg.Args, ref arrayList);
			this._sig = CADMessageBase.GetSignature(base.GetMethod(), true);
			if (retMsg.Exception != null)
			{
				if (arrayList == null)
				{
					arrayList = new ArrayList();
				}
				this._exception = new CADArgHolder(arrayList.Count);
				arrayList.Add(retMsg.Exception);
			}
			base.SaveLogicalCallContext(retMsg, ref arrayList);
			if (arrayList != null)
			{
				MemoryStream memoryStream = CADSerializer.SerializeObject(arrayList.ToArray());
				this._serializedArgs = memoryStream.GetBuffer();
			}
		}

		// Token: 0x06004FDE RID: 20446 RVA: 0x00112A78 File Offset: 0x00110C78
		internal ArrayList GetArguments()
		{
			ArrayList result = null;
			if (this._serializedArgs != null)
			{
				result = new ArrayList((object[])CADSerializer.DeserializeObject(new MemoryStream(this._serializedArgs)));
				this._serializedArgs = null;
			}
			return result;
		}

		// Token: 0x06004FDF RID: 20447 RVA: 0x00112986 File Offset: 0x00110B86
		internal object[] GetArgs(ArrayList args)
		{
			return base.UnmarshalArguments(this._args, args);
		}

		// Token: 0x06004FE0 RID: 20448 RVA: 0x00112AB2 File Offset: 0x00110CB2
		internal object GetReturnValue(ArrayList args)
		{
			return base.UnmarshalArgument(this._returnValue, args);
		}

		// Token: 0x06004FE1 RID: 20449 RVA: 0x00112AC1 File Offset: 0x00110CC1
		internal Exception GetException(ArrayList args)
		{
			if (this._exception == null)
			{
				return null;
			}
			return (Exception)args[this._exception.index];
		}

		// Token: 0x17000D93 RID: 3475
		// (get) Token: 0x06004FE2 RID: 20450 RVA: 0x00112995 File Offset: 0x00110B95
		internal int PropertiesCount
		{
			get
			{
				return this._propertyCount;
			}
		}

		// Token: 0x04002930 RID: 10544
		private object _returnValue;

		// Token: 0x04002931 RID: 10545
		private CADArgHolder _exception;

		// Token: 0x04002932 RID: 10546
		private Type[] _sig;
	}
}
