﻿using System;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting
{
	// Token: 0x0200075E RID: 1886
	internal abstract class Identity
	{
		// Token: 0x06004B18 RID: 19224 RVA: 0x00107255 File Offset: 0x00105455
		public Identity(string objectUri)
		{
			this._objectUri = objectUri;
		}

		// Token: 0x06004B19 RID: 19225
		public abstract ObjRef CreateObjRef(Type requestedType);

		// Token: 0x17000C7D RID: 3197
		// (get) Token: 0x06004B1A RID: 19226 RVA: 0x00107264 File Offset: 0x00105464
		public bool IsFromThisAppDomain
		{
			get
			{
				return this._channelSink == null;
			}
		}

		// Token: 0x17000C7E RID: 3198
		// (get) Token: 0x06004B1B RID: 19227 RVA: 0x0010726F File Offset: 0x0010546F
		// (set) Token: 0x06004B1C RID: 19228 RVA: 0x00107277 File Offset: 0x00105477
		public IMessageSink ChannelSink
		{
			get
			{
				return this._channelSink;
			}
			set
			{
				this._channelSink = value;
			}
		}

		// Token: 0x17000C7F RID: 3199
		// (get) Token: 0x06004B1D RID: 19229 RVA: 0x00107280 File Offset: 0x00105480
		public IMessageSink EnvoySink
		{
			get
			{
				return this._envoySink;
			}
		}

		// Token: 0x17000C80 RID: 3200
		// (get) Token: 0x06004B1E RID: 19230 RVA: 0x00107288 File Offset: 0x00105488
		// (set) Token: 0x06004B1F RID: 19231 RVA: 0x00107290 File Offset: 0x00105490
		public string ObjectUri
		{
			get
			{
				return this._objectUri;
			}
			set
			{
				this._objectUri = value;
			}
		}

		// Token: 0x17000C81 RID: 3201
		// (get) Token: 0x06004B20 RID: 19232 RVA: 0x00107299 File Offset: 0x00105499
		public bool IsConnected
		{
			get
			{
				return this._objectUri != null;
			}
		}

		// Token: 0x17000C82 RID: 3202
		// (get) Token: 0x06004B21 RID: 19233 RVA: 0x001072A4 File Offset: 0x001054A4
		// (set) Token: 0x06004B22 RID: 19234 RVA: 0x001072AC File Offset: 0x001054AC
		public bool Disposed
		{
			get
			{
				return this._disposed;
			}
			set
			{
				this._disposed = value;
			}
		}

		// Token: 0x17000C83 RID: 3203
		// (get) Token: 0x06004B23 RID: 19235 RVA: 0x001072B5 File Offset: 0x001054B5
		public DynamicPropertyCollection ClientDynamicProperties
		{
			get
			{
				if (this._clientDynamicProperties == null)
				{
					this._clientDynamicProperties = new DynamicPropertyCollection();
				}
				return this._clientDynamicProperties;
			}
		}

		// Token: 0x17000C84 RID: 3204
		// (get) Token: 0x06004B24 RID: 19236 RVA: 0x001072D0 File Offset: 0x001054D0
		public DynamicPropertyCollection ServerDynamicProperties
		{
			get
			{
				if (this._serverDynamicProperties == null)
				{
					this._serverDynamicProperties = new DynamicPropertyCollection();
				}
				return this._serverDynamicProperties;
			}
		}

		// Token: 0x17000C85 RID: 3205
		// (get) Token: 0x06004B25 RID: 19237 RVA: 0x001072EB File Offset: 0x001054EB
		public bool HasClientDynamicSinks
		{
			get
			{
				return this._clientDynamicProperties != null && this._clientDynamicProperties.HasProperties;
			}
		}

		// Token: 0x17000C86 RID: 3206
		// (get) Token: 0x06004B26 RID: 19238 RVA: 0x00107302 File Offset: 0x00105502
		public bool HasServerDynamicSinks
		{
			get
			{
				return this._serverDynamicProperties != null && this._serverDynamicProperties.HasProperties;
			}
		}

		// Token: 0x06004B27 RID: 19239 RVA: 0x00107319 File Offset: 0x00105519
		public void NotifyClientDynamicSinks(bool start, IMessage req_msg, bool client_site, bool async)
		{
			if (this._clientDynamicProperties != null && this._clientDynamicProperties.HasProperties)
			{
				this._clientDynamicProperties.NotifyMessage(start, req_msg, client_site, async);
			}
		}

		// Token: 0x06004B28 RID: 19240 RVA: 0x00107340 File Offset: 0x00105540
		public void NotifyServerDynamicSinks(bool start, IMessage req_msg, bool client_site, bool async)
		{
			if (this._serverDynamicProperties != null && this._serverDynamicProperties.HasProperties)
			{
				this._serverDynamicProperties.NotifyMessage(start, req_msg, client_site, async);
			}
		}

		// Token: 0x040027CC RID: 10188
		protected string _objectUri;

		// Token: 0x040027CD RID: 10189
		protected IMessageSink _channelSink;

		// Token: 0x040027CE RID: 10190
		protected IMessageSink _envoySink;

		// Token: 0x040027CF RID: 10191
		private DynamicPropertyCollection _clientDynamicProperties;

		// Token: 0x040027D0 RID: 10192
		private DynamicPropertyCollection _serverDynamicProperties;

		// Token: 0x040027D1 RID: 10193
		protected ObjRef _objRef;

		// Token: 0x040027D2 RID: 10194
		private bool _disposed;
	}
}
