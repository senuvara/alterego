﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	/// <summary>You should not use this enumeration; it is not used by the .NET Framework remoting infrastructure.</summary>
	// Token: 0x020007DC RID: 2012
	[ComVisible(true)]
	[Serializable]
	public enum XmlFieldOrderOption
	{
		/// <summary>You should not use the XmlFieldOrderOption enumeration; it is not used by the .NET Framework's remoting infrastructure.</summary>
		// Token: 0x040028CD RID: 10445
		All,
		/// <summary>You should not use the XmlFieldOrderOption enumeration; it is not used by the .NET Framework's remoting infrastructure.</summary>
		// Token: 0x040028CE RID: 10446
		Sequence,
		/// <summary>You should not use the XmlFieldOrderOption enumeration; it is not used by the .NET Framework's remoting infrastructure.</summary>
		// Token: 0x040028CF RID: 10447
		Choice
	}
}
