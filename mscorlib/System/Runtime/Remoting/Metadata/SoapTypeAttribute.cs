﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	/// <summary>Customizes SOAP generation and processing for target types. This class cannot be inherited.</summary>
	// Token: 0x020007DB RID: 2011
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface)]
	[ComVisible(true)]
	public sealed class SoapTypeAttribute : SoapAttribute
	{
		/// <summary>Creates an instance of <see cref="T:System.Runtime.Remoting.Metadata.SoapTypeAttribute" />.</summary>
		// Token: 0x06004E40 RID: 20032 RVA: 0x0010FCFA File Offset: 0x0010DEFA
		public SoapTypeAttribute()
		{
		}

		/// <summary>Gets or sets a <see cref="T:System.Runtime.Remoting.Metadata.SoapOption" /> configuration value.</summary>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.SoapOption" /> configuration value.</returns>
		// Token: 0x17000D27 RID: 3367
		// (get) Token: 0x06004E41 RID: 20033 RVA: 0x0010FE59 File Offset: 0x0010E059
		// (set) Token: 0x06004E42 RID: 20034 RVA: 0x0010FE61 File Offset: 0x0010E061
		public SoapOption SoapOptions
		{
			get
			{
				return this._soapOption;
			}
			set
			{
				this._soapOption = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the target of the current attribute will be serialized as an XML attribute instead of an XML field.</summary>
		/// <returns>The current implementation always returns <see langword="false" />.</returns>
		/// <exception cref="T:System.Runtime.Remoting.RemotingException">An attempt was made to set the current property. </exception>
		// Token: 0x17000D28 RID: 3368
		// (get) Token: 0x06004E43 RID: 20035 RVA: 0x0010FE6A File Offset: 0x0010E06A
		// (set) Token: 0x06004E44 RID: 20036 RVA: 0x0010FE72 File Offset: 0x0010E072
		public override bool UseAttribute
		{
			get
			{
				return this._useAttribute;
			}
			set
			{
				this._useAttribute = value;
			}
		}

		/// <summary>Gets or sets the XML element name.</summary>
		/// <returns>The XML element name.</returns>
		// Token: 0x17000D29 RID: 3369
		// (get) Token: 0x06004E45 RID: 20037 RVA: 0x0010FE7B File Offset: 0x0010E07B
		// (set) Token: 0x06004E46 RID: 20038 RVA: 0x0010FE83 File Offset: 0x0010E083
		public string XmlElementName
		{
			get
			{
				return this._xmlElementName;
			}
			set
			{
				this._isElement = (value != null);
				this._xmlElementName = value;
			}
		}

		/// <summary>Gets or sets the XML field order for the target object type.</summary>
		/// <returns>The XML field order for the target object type.</returns>
		// Token: 0x17000D2A RID: 3370
		// (get) Token: 0x06004E47 RID: 20039 RVA: 0x0010FE96 File Offset: 0x0010E096
		// (set) Token: 0x06004E48 RID: 20040 RVA: 0x0010FE9E File Offset: 0x0010E09E
		public XmlFieldOrderOption XmlFieldOrder
		{
			get
			{
				return this._xmlFieldOrder;
			}
			set
			{
				this._xmlFieldOrder = value;
			}
		}

		/// <summary>Gets or sets the XML namespace that is used during serialization of the target object type.</summary>
		/// <returns>The XML namespace that is used during serialization of the target object type.</returns>
		// Token: 0x17000D2B RID: 3371
		// (get) Token: 0x06004E49 RID: 20041 RVA: 0x0010FEA7 File Offset: 0x0010E0A7
		// (set) Token: 0x06004E4A RID: 20042 RVA: 0x0010FEAF File Offset: 0x0010E0AF
		public override string XmlNamespace
		{
			get
			{
				return this._xmlNamespace;
			}
			set
			{
				this._isElement = (value != null);
				this._xmlNamespace = value;
			}
		}

		/// <summary>Gets or sets the XML type name for the target object type.</summary>
		/// <returns>The XML type name for the target object type.</returns>
		// Token: 0x17000D2C RID: 3372
		// (get) Token: 0x06004E4B RID: 20043 RVA: 0x0010FEC2 File Offset: 0x0010E0C2
		// (set) Token: 0x06004E4C RID: 20044 RVA: 0x0010FECA File Offset: 0x0010E0CA
		public string XmlTypeName
		{
			get
			{
				return this._xmlTypeName;
			}
			set
			{
				this._isType = (value != null);
				this._xmlTypeName = value;
			}
		}

		/// <summary>Gets or sets the XML type namespace for the current object type.</summary>
		/// <returns>The XML type namespace for the current object type.</returns>
		// Token: 0x17000D2D RID: 3373
		// (get) Token: 0x06004E4D RID: 20045 RVA: 0x0010FEDD File Offset: 0x0010E0DD
		// (set) Token: 0x06004E4E RID: 20046 RVA: 0x0010FEE5 File Offset: 0x0010E0E5
		public string XmlTypeNamespace
		{
			get
			{
				return this._xmlTypeNamespace;
			}
			set
			{
				this._isType = (value != null);
				this._xmlTypeNamespace = value;
			}
		}

		// Token: 0x17000D2E RID: 3374
		// (get) Token: 0x06004E4F RID: 20047 RVA: 0x0010FEF8 File Offset: 0x0010E0F8
		internal bool IsInteropXmlElement
		{
			get
			{
				return this._isElement;
			}
		}

		// Token: 0x17000D2F RID: 3375
		// (get) Token: 0x06004E50 RID: 20048 RVA: 0x0010FF00 File Offset: 0x0010E100
		internal bool IsInteropXmlType
		{
			get
			{
				return this._isType;
			}
		}

		// Token: 0x06004E51 RID: 20049 RVA: 0x0010FF08 File Offset: 0x0010E108
		internal override void SetReflectionObject(object reflectionObject)
		{
			Type type = (Type)reflectionObject;
			if (this._xmlElementName == null)
			{
				this._xmlElementName = type.Name;
			}
			if (this._xmlTypeName == null)
			{
				this._xmlTypeName = type.Name;
			}
			if (this._xmlTypeNamespace == null)
			{
				string assemblyName;
				if (type.Assembly == typeof(object).Assembly)
				{
					assemblyName = string.Empty;
				}
				else
				{
					assemblyName = type.Assembly.GetName().Name;
				}
				this._xmlTypeNamespace = SoapServices.CodeXmlNamespaceForClrTypeNamespace(type.Namespace, assemblyName);
			}
			if (this._xmlNamespace == null)
			{
				this._xmlNamespace = this._xmlTypeNamespace;
			}
		}

		// Token: 0x040028C3 RID: 10435
		private SoapOption _soapOption;

		// Token: 0x040028C4 RID: 10436
		private bool _useAttribute;

		// Token: 0x040028C5 RID: 10437
		private string _xmlElementName;

		// Token: 0x040028C6 RID: 10438
		private XmlFieldOrderOption _xmlFieldOrder;

		// Token: 0x040028C7 RID: 10439
		private string _xmlNamespace;

		// Token: 0x040028C8 RID: 10440
		private string _xmlTypeName;

		// Token: 0x040028C9 RID: 10441
		private string _xmlTypeNamespace;

		// Token: 0x040028CA RID: 10442
		private bool _isType;

		// Token: 0x040028CB RID: 10443
		private bool _isElement;
	}
}
