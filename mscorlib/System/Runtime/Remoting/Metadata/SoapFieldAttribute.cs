﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	/// <summary>Customizes SOAP generation and processing for a field. This class cannot be inherited.</summary>
	// Token: 0x020007D7 RID: 2007
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field)]
	public sealed class SoapFieldAttribute : SoapAttribute
	{
		/// <summary>Creates an instance of <see cref="T:System.Runtime.Remoting.Metadata.SoapFieldAttribute" /> class.</summary>
		// Token: 0x06004E2A RID: 20010 RVA: 0x0010FCFA File Offset: 0x0010DEFA
		public SoapFieldAttribute()
		{
		}

		/// <summary>Gets or sets the order of the current field attribute.</summary>
		/// <returns>The order of the current field attribute.</returns>
		// Token: 0x17000D1F RID: 3359
		// (get) Token: 0x06004E2B RID: 20011 RVA: 0x0010FD02 File Offset: 0x0010DF02
		// (set) Token: 0x06004E2C RID: 20012 RVA: 0x0010FD0A File Offset: 0x0010DF0A
		public int Order
		{
			get
			{
				return this._order;
			}
			set
			{
				this._order = value;
			}
		}

		/// <summary>Gets or sets the XML element name of the field contained in the <see cref="T:System.Runtime.Remoting.Metadata.SoapFieldAttribute" /> attribute.</summary>
		/// <returns>The XML element name of the field contained in this attribute.</returns>
		// Token: 0x17000D20 RID: 3360
		// (get) Token: 0x06004E2D RID: 20013 RVA: 0x0010FD13 File Offset: 0x0010DF13
		// (set) Token: 0x06004E2E RID: 20014 RVA: 0x0010FD1B File Offset: 0x0010DF1B
		public string XmlElementName
		{
			get
			{
				return this._elementName;
			}
			set
			{
				this._isElement = (value != null);
				this._elementName = value;
			}
		}

		/// <summary>Returns a value indicating whether the current attribute contains interop XML element values.</summary>
		/// <returns>
		///     <see langword="true" /> if the current attribute contains interop XML element values; otherwise, <see langword="false" />.</returns>
		// Token: 0x06004E2F RID: 20015 RVA: 0x0010FD2E File Offset: 0x0010DF2E
		public bool IsInteropXmlElement()
		{
			return this._isElement;
		}

		// Token: 0x06004E30 RID: 20016 RVA: 0x0010FD38 File Offset: 0x0010DF38
		internal override void SetReflectionObject(object reflectionObject)
		{
			FieldInfo fieldInfo = (FieldInfo)reflectionObject;
			if (this._elementName == null)
			{
				this._elementName = fieldInfo.Name;
			}
		}

		// Token: 0x040028B3 RID: 10419
		private int _order;

		// Token: 0x040028B4 RID: 10420
		private string _elementName;

		// Token: 0x040028B5 RID: 10421
		private bool _isElement;
	}
}
