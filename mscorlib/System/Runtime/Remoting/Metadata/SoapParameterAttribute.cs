﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	/// <summary>Customizes SOAP generation and processing for a parameter. This class cannot be inherited.</summary>
	// Token: 0x020007DA RID: 2010
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Parameter)]
	public sealed class SoapParameterAttribute : SoapAttribute
	{
		/// <summary>Creates an instance of <see cref="T:System.Runtime.Remoting.Metadata.SoapParameterAttribute" />.</summary>
		// Token: 0x06004E3F RID: 20031 RVA: 0x0010FCFA File Offset: 0x0010DEFA
		public SoapParameterAttribute()
		{
		}
	}
}
