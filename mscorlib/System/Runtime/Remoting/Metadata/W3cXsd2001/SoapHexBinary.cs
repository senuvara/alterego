﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XSD <see langword="hexBinary" /> type.</summary>
	// Token: 0x020007E7 RID: 2023
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapHexBinary : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary" /> class.</summary>
		// Token: 0x06004E94 RID: 20116 RVA: 0x001106DC File Offset: 0x0010E8DC
		public SoapHexBinary()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary" /> class.</summary>
		/// <param name="value">A <see cref="T:System.Byte" /> array that contains a hexadecimal number. </param>
		// Token: 0x06004E95 RID: 20117 RVA: 0x001106EF File Offset: 0x0010E8EF
		public SoapHexBinary(byte[] value)
		{
			this._value = value;
		}

		/// <summary>Gets or sets the hexadecimal representation of a number.</summary>
		/// <returns>A <see cref="T:System.Byte" /> array containing the hexadecimal representation of a number.</returns>
		// Token: 0x17000D3F RID: 3391
		// (get) Token: 0x06004E96 RID: 20118 RVA: 0x00110709 File Offset: 0x0010E909
		// (set) Token: 0x06004E97 RID: 20119 RVA: 0x00110711 File Offset: 0x0010E911
		public byte[] Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> indicating the XSD of the current SOAP type.</returns>
		// Token: 0x17000D40 RID: 3392
		// (get) Token: 0x06004E98 RID: 20120 RVA: 0x0011071A File Offset: 0x0010E91A
		public static string XsdType
		{
			get
			{
				return "hexBinary";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004E99 RID: 20121 RVA: 0x00110721 File Offset: 0x0010E921
		public string GetXsdType()
		{
			return SoapHexBinary.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary" /> object that is obtained from <paramref name="value" />.</returns>
		// Token: 0x06004E9A RID: 20122 RVA: 0x00110728 File Offset: 0x0010E928
		public static SoapHexBinary Parse(string value)
		{
			return new SoapHexBinary(SoapHexBinary.FromBinHexString(value));
		}

		// Token: 0x06004E9B RID: 20123 RVA: 0x00110738 File Offset: 0x0010E938
		internal static byte[] FromBinHexString(string value)
		{
			char[] array = value.ToCharArray();
			byte[] array2 = new byte[array.Length / 2 + array.Length % 2];
			int num = array.Length;
			if (num % 2 != 0)
			{
				throw SoapHexBinary.CreateInvalidValueException(value);
			}
			int num2 = 0;
			for (int i = 0; i < num - 1; i += 2)
			{
				array2[num2] = SoapHexBinary.FromHex(array[i], value);
				byte[] array3 = array2;
				int num3 = num2;
				array3[num3] = (byte)(array3[num3] << 4);
				byte[] array4 = array2;
				int num4 = num2;
				array4[num4] += SoapHexBinary.FromHex(array[i + 1], value);
				num2++;
			}
			return array2;
		}

		// Token: 0x06004E9C RID: 20124 RVA: 0x001107B8 File Offset: 0x0010E9B8
		private static byte FromHex(char hexDigit, string value)
		{
			byte result;
			try
			{
				result = byte.Parse(hexDigit.ToString(), NumberStyles.HexNumber, CultureInfo.InvariantCulture);
			}
			catch (FormatException)
			{
				throw SoapHexBinary.CreateInvalidValueException(value);
			}
			return result;
		}

		// Token: 0x06004E9D RID: 20125 RVA: 0x001107F8 File Offset: 0x0010E9F8
		private static Exception CreateInvalidValueException(string value)
		{
			return new RemotingException(string.Format(CultureInfo.InvariantCulture, "Invalid value '{0}' for xsd:{1}.", value, SoapHexBinary.XsdType));
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapHexBinary.Value" />.</returns>
		// Token: 0x06004E9E RID: 20126 RVA: 0x00110814 File Offset: 0x0010EA14
		public override string ToString()
		{
			this.sb.Length = 0;
			foreach (byte b in this._value)
			{
				this.sb.Append(b.ToString("X2"));
			}
			return this.sb.ToString();
		}

		// Token: 0x040028DA RID: 10458
		private byte[] _value;

		// Token: 0x040028DB RID: 10459
		private StringBuilder sb = new StringBuilder();
	}
}
