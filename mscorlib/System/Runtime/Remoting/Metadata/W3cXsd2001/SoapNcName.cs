﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XML <see langword="NcName" /> type.</summary>
	// Token: 0x020007F0 RID: 2032
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNcName : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName" /> class.</summary>
		// Token: 0x06004EE1 RID: 20193 RVA: 0x00002050 File Offset: 0x00000250
		public SoapNcName()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName" /> class with an XML <see langword="NcName" /> type.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that contains an XML <see langword="NcName" /> type. </param>
		// Token: 0x06004EE2 RID: 20194 RVA: 0x00110AC4 File Offset: 0x0010ECC4
		public SoapNcName(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		/// <summary>Gets or sets an XML <see langword="NcName" /> type.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains an XML <see langword="NcName" /> type.</returns>
		// Token: 0x17000D51 RID: 3409
		// (get) Token: 0x06004EE3 RID: 20195 RVA: 0x00110AD8 File Offset: 0x0010ECD8
		// (set) Token: 0x06004EE4 RID: 20196 RVA: 0x00110AE0 File Offset: 0x0010ECE0
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D52 RID: 3410
		// (get) Token: 0x06004EE5 RID: 20197 RVA: 0x00110AE9 File Offset: 0x0010ECE9
		public static string XsdType
		{
			get
			{
				return "NCName";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004EE6 RID: 20198 RVA: 0x00110AF0 File Offset: 0x0010ECF0
		public string GetXsdType()
		{
			return SoapNcName.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName" /> object that is obtained from <paramref name="value" />.</returns>
		// Token: 0x06004EE7 RID: 20199 RVA: 0x00110AF7 File Offset: 0x0010ECF7
		public static SoapNcName Parse(string value)
		{
			return new SoapNcName(value);
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNcName.Value" />.</returns>
		// Token: 0x06004EE8 RID: 20200 RVA: 0x00110AD8 File Offset: 0x0010ECD8
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040028E6 RID: 10470
		private string _value;
	}
}
