﻿using System;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020007E6 RID: 2022
	internal class SoapHelper
	{
		// Token: 0x06004E91 RID: 20113 RVA: 0x001106BF File Offset: 0x0010E8BF
		public static Exception GetException(ISoapXsd type, string msg)
		{
			return new RemotingException("Soap Parse error, xsd:type xsd:" + type.GetXsdType() + " " + msg);
		}

		// Token: 0x06004E92 RID: 20114 RVA: 0x00002058 File Offset: 0x00000258
		public static string Normalize(string s)
		{
			return s;
		}

		// Token: 0x06004E93 RID: 20115 RVA: 0x00002050 File Offset: 0x00000250
		public SoapHelper()
		{
		}
	}
}
