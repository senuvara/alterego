﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XML <see langword="NOTATION" /> attribute type.</summary>
	// Token: 0x020007F7 RID: 2039
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNotation : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation" /> class.</summary>
		// Token: 0x06004F19 RID: 20249 RVA: 0x00002050 File Offset: 0x00000250
		public SoapNotation()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation" /> class with an XML <see langword="NOTATION" /> attribute.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that contains an XML <see langword="NOTATION" /> attribute. </param>
		// Token: 0x06004F1A RID: 20250 RVA: 0x00110CF4 File Offset: 0x0010EEF4
		public SoapNotation(string value)
		{
			this._value = value;
		}

		/// <summary>Gets or sets an XML <see langword="NOTATION" /> attribute.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains an XML <see langword="NOTATION" /> attribute.</returns>
		// Token: 0x17000D5F RID: 3423
		// (get) Token: 0x06004F1B RID: 20251 RVA: 0x00110D03 File Offset: 0x0010EF03
		// (set) Token: 0x06004F1C RID: 20252 RVA: 0x00110D0B File Offset: 0x0010EF0B
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D60 RID: 3424
		// (get) Token: 0x06004F1D RID: 20253 RVA: 0x00110D14 File Offset: 0x0010EF14
		public static string XsdType
		{
			get
			{
				return "NOTATION";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004F1E RID: 20254 RVA: 0x00110D1B File Offset: 0x0010EF1B
		public string GetXsdType()
		{
			return SoapNotation.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation" /> object that is obtained from <paramref name="value" />.</returns>
		// Token: 0x06004F1F RID: 20255 RVA: 0x00110D22 File Offset: 0x0010EF22
		public static SoapNotation Parse(string value)
		{
			return new SoapNotation(value);
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapNotation.Value" />.</returns>
		// Token: 0x06004F20 RID: 20256 RVA: 0x00110D03 File Offset: 0x0010EF03
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040028ED RID: 10477
		private string _value;
	}
}
