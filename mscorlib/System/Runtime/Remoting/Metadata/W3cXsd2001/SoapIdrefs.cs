﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XML <see langword="IDREFS" /> attribute.</summary>
	// Token: 0x020007EA RID: 2026
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapIdrefs : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs" /> class.</summary>
		// Token: 0x06004EAF RID: 20143 RVA: 0x00002050 File Offset: 0x00000250
		public SoapIdrefs()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs" /> class with an XML <see langword="IDREFS" /> attribute.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that contains an XML <see langword="IDREFS" /> attribute. </param>
		// Token: 0x06004EB0 RID: 20144 RVA: 0x001108DF File Offset: 0x0010EADF
		public SoapIdrefs(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		/// <summary>Gets or sets an XML <see langword="IDREFS" /> attribute.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains an XML <see langword="IDREFS" /> attribute.</returns>
		// Token: 0x17000D45 RID: 3397
		// (get) Token: 0x06004EB1 RID: 20145 RVA: 0x001108F3 File Offset: 0x0010EAF3
		// (set) Token: 0x06004EB2 RID: 20146 RVA: 0x001108FB File Offset: 0x0010EAFB
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D46 RID: 3398
		// (get) Token: 0x06004EB3 RID: 20147 RVA: 0x00110904 File Offset: 0x0010EB04
		public static string XsdType
		{
			get
			{
				return "IDREFS";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004EB4 RID: 20148 RVA: 0x0011090B File Offset: 0x0010EB0B
		public string GetXsdType()
		{
			return SoapIdrefs.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <paramref name="value" />.</returns>
		// Token: 0x06004EB5 RID: 20149 RVA: 0x00110912 File Offset: 0x0010EB12
		public static SoapIdrefs Parse(string value)
		{
			return new SoapIdrefs(value);
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs.Value" />.</returns>
		// Token: 0x06004EB6 RID: 20150 RVA: 0x001108F3 File Offset: 0x0010EAF3
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040028DE RID: 10462
		private string _value;
	}
}
