﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XML <see langword="token" /> type.</summary>
	// Token: 0x020007FB RID: 2043
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapToken : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken" /> class.</summary>
		// Token: 0x06004F40 RID: 20288 RVA: 0x00002050 File Offset: 0x00000250
		public SoapToken()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken" /> class with an XML <see langword="token" />.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that contains an XML <see langword="token" />. </param>
		/// <exception cref="T:System.Runtime.Remoting.RemotingException">One of the following: 
		///             <paramref name="value" /> contains invalid characters (0xD or 0x9).
		///             <paramref name="value" /> [0] or <paramref name="value" /> [ <paramref name="value" />.Length - 1] contains white space.
		///             <paramref name="value" /> contains any spaces. </exception>
		// Token: 0x06004F41 RID: 20289 RVA: 0x00110FE0 File Offset: 0x0010F1E0
		public SoapToken(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		/// <summary>Gets or sets an XML <see langword="token" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains an XML <see langword="token" />.</returns>
		/// <exception cref="T:System.Runtime.Remoting.RemotingException">One of the following: <paramref name="value" /> contains invalid characters (0xD or 0x9).
		///             <paramref name="value" /> [0] or <paramref name="value" /> [ <paramref name="value" />.Length - 1] contains white space.
		///             <paramref name="value" /> contains any spaces. </exception>
		// Token: 0x17000D69 RID: 3433
		// (get) Token: 0x06004F42 RID: 20290 RVA: 0x00110FF4 File Offset: 0x0010F1F4
		// (set) Token: 0x06004F43 RID: 20291 RVA: 0x00110FFC File Offset: 0x0010F1FC
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D6A RID: 3434
		// (get) Token: 0x06004F44 RID: 20292 RVA: 0x00111005 File Offset: 0x0010F205
		public static string XsdType
		{
			get
			{
				return "token";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004F45 RID: 20293 RVA: 0x0011100C File Offset: 0x0010F20C
		public string GetXsdType()
		{
			return SoapToken.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken" /> object that is obtained from <paramref name="value" />.</returns>
		// Token: 0x06004F46 RID: 20294 RVA: 0x00111013 File Offset: 0x0010F213
		public static SoapToken Parse(string value)
		{
			return new SoapToken(value);
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapToken.Value" />.</returns>
		// Token: 0x06004F47 RID: 20295 RVA: 0x00110FF4 File Offset: 0x0010F1F4
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040028F4 RID: 10484
		private string _value;
	}
}
