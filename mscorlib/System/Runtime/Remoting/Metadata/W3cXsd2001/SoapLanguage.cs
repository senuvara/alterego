﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XML <see langword="language" /> type.</summary>
	// Token: 0x020007EC RID: 2028
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapLanguage : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage" /> class.</summary>
		// Token: 0x06004EBF RID: 20159 RVA: 0x00002050 File Offset: 0x00000250
		public SoapLanguage()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage" /> class with the language identifier value of <see langword="language" /> attribute.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that contains the language identifier value of a <see langword="language" /> attribute. </param>
		// Token: 0x06004EC0 RID: 20160 RVA: 0x00110962 File Offset: 0x0010EB62
		public SoapLanguage(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		/// <summary>Gets or sets the language identifier of a <see langword="language" /> attribute.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the language identifier of a <see langword="language" /> attribute.</returns>
		// Token: 0x17000D49 RID: 3401
		// (get) Token: 0x06004EC1 RID: 20161 RVA: 0x00110976 File Offset: 0x0010EB76
		// (set) Token: 0x06004EC2 RID: 20162 RVA: 0x0011097E File Offset: 0x0010EB7E
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D4A RID: 3402
		// (get) Token: 0x06004EC3 RID: 20163 RVA: 0x00110987 File Offset: 0x0010EB87
		public static string XsdType
		{
			get
			{
				return "language";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004EC4 RID: 20164 RVA: 0x0011098E File Offset: 0x0010EB8E
		public string GetXsdType()
		{
			return SoapLanguage.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage" /> object that is obtained from <paramref name="value" />.</returns>
		// Token: 0x06004EC5 RID: 20165 RVA: 0x00110995 File Offset: 0x0010EB95
		public static SoapLanguage Parse(string value)
		{
			return new SoapLanguage(value);
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage" /> object that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapLanguage.Value" />.</returns>
		// Token: 0x06004EC6 RID: 20166 RVA: 0x00110976 File Offset: 0x0010EB76
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040028E0 RID: 10464
		private string _value;
	}
}
