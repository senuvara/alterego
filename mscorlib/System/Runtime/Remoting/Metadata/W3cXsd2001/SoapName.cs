﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XML <see langword="Name" /> type.</summary>
	// Token: 0x020007EF RID: 2031
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapName : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName" /> class.</summary>
		// Token: 0x06004ED9 RID: 20185 RVA: 0x00002050 File Offset: 0x00000250
		public SoapName()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName" /> class with an XML <see langword="Name" /> type.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that contains an XML <see langword="Name" /> type. </param>
		// Token: 0x06004EDA RID: 20186 RVA: 0x00110A89 File Offset: 0x0010EC89
		public SoapName(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		/// <summary>Gets or sets an XML <see langword="Name" /> type.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains an XML <see langword="Name" /> type.</returns>
		// Token: 0x17000D4F RID: 3407
		// (get) Token: 0x06004EDB RID: 20187 RVA: 0x00110A9D File Offset: 0x0010EC9D
		// (set) Token: 0x06004EDC RID: 20188 RVA: 0x00110AA5 File Offset: 0x0010ECA5
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D50 RID: 3408
		// (get) Token: 0x06004EDD RID: 20189 RVA: 0x00110AAE File Offset: 0x0010ECAE
		public static string XsdType
		{
			get
			{
				return "Name";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004EDE RID: 20190 RVA: 0x00110AB5 File Offset: 0x0010ECB5
		public string GetXsdType()
		{
			return SoapName.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName" /> object that is obtained from <paramref name="value" />.</returns>
		// Token: 0x06004EDF RID: 20191 RVA: 0x00110ABC File Offset: 0x0010ECBC
		public static SoapName Parse(string value)
		{
			return new SoapName(value);
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapName.Value" />.</returns>
		// Token: 0x06004EE0 RID: 20192 RVA: 0x00110A9D File Offset: 0x0010EC9D
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040028E5 RID: 10469
		private string _value;
	}
}
