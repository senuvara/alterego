﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XML <see langword="IDREFS" /> attribute.</summary>
	// Token: 0x020007E9 RID: 2025
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapIdref : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref" /> class.</summary>
		// Token: 0x06004EA7 RID: 20135 RVA: 0x00002050 File Offset: 0x00000250
		public SoapIdref()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref" /> class with an XML <see langword="IDREF" /> attribute.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that contains an XML <see langword="IDREF" /> attribute. </param>
		// Token: 0x06004EA8 RID: 20136 RVA: 0x001108A4 File Offset: 0x0010EAA4
		public SoapIdref(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		/// <summary>Gets or sets an XML <see langword="IDREF" /> attribute.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains an XML <see langword="IDREF" /> attribute.</returns>
		// Token: 0x17000D43 RID: 3395
		// (get) Token: 0x06004EA9 RID: 20137 RVA: 0x001108B8 File Offset: 0x0010EAB8
		// (set) Token: 0x06004EAA RID: 20138 RVA: 0x001108C0 File Offset: 0x0010EAC0
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D44 RID: 3396
		// (get) Token: 0x06004EAB RID: 20139 RVA: 0x001108C9 File Offset: 0x0010EAC9
		public static string XsdType
		{
			get
			{
				return "IDREF";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004EAC RID: 20140 RVA: 0x001108D0 File Offset: 0x0010EAD0
		public string GetXsdType()
		{
			return SoapIdref.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdrefs" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.String" /> obtained from <paramref name="value" />.</returns>
		// Token: 0x06004EAD RID: 20141 RVA: 0x001108D7 File Offset: 0x0010EAD7
		public static SoapIdref Parse(string value)
		{
			return new SoapIdref(value);
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapIdref.Value" />.</returns>
		// Token: 0x06004EAE RID: 20142 RVA: 0x001108B8 File Offset: 0x0010EAB8
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040028DD RID: 10461
		private string _value;
	}
}
