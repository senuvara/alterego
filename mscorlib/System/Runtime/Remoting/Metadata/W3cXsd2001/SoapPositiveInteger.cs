﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XSD <see langword="positiveInteger" /> type.</summary>
	// Token: 0x020007F8 RID: 2040
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapPositiveInteger : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger" /> class.</summary>
		// Token: 0x06004F21 RID: 20257 RVA: 0x00002050 File Offset: 0x00000250
		public SoapPositiveInteger()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapInteger" /> class with a <see cref="T:System.Decimal" /> value.</summary>
		/// <param name="value">A <see cref="T:System.Decimal" /> value to initialize the current instance. </param>
		/// <exception cref="T:System.Runtime.Remoting.RemotingException">
		///         <paramref name="value" /> is less than 1. </exception>
		// Token: 0x06004F22 RID: 20258 RVA: 0x00110D2A File Offset: 0x0010EF2A
		public SoapPositiveInteger(decimal value)
		{
			if (value <= 0m)
			{
				throw SoapHelper.GetException(this, "invalid " + value);
			}
			this._value = value;
		}

		/// <summary>Gets or sets the numeric value of the current instance.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> indicating the numeric value of the current instance.</returns>
		/// <exception cref="T:System.Runtime.Remoting.RemotingException">
		///         <paramref name="value" /> is less than 1. </exception>
		// Token: 0x17000D61 RID: 3425
		// (get) Token: 0x06004F23 RID: 20259 RVA: 0x00110D5D File Offset: 0x0010EF5D
		// (set) Token: 0x06004F24 RID: 20260 RVA: 0x00110D65 File Offset: 0x0010EF65
		public decimal Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D62 RID: 3426
		// (get) Token: 0x06004F25 RID: 20261 RVA: 0x00110D6E File Offset: 0x0010EF6E
		public static string XsdType
		{
			get
			{
				return "positiveInteger";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004F26 RID: 20262 RVA: 0x00110D75 File Offset: 0x0010EF75
		public string GetXsdType()
		{
			return SoapPositiveInteger.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger" /> object that is obtained from <paramref name="value" />. </returns>
		// Token: 0x06004F27 RID: 20263 RVA: 0x00110D7C File Offset: 0x0010EF7C
		public static SoapPositiveInteger Parse(string value)
		{
			return new SoapPositiveInteger(decimal.Parse(value));
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapPositiveInteger.Value" />.</returns>
		// Token: 0x06004F28 RID: 20264 RVA: 0x00110D89 File Offset: 0x0010EF89
		public override string ToString()
		{
			return this._value.ToString();
		}

		// Token: 0x040028EE RID: 10478
		private decimal _value;
	}
}
