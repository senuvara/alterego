﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	/// <summary>Wraps an XML <see langword="ID" /> attribute.</summary>
	// Token: 0x020007E8 RID: 2024
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapId : ISoapXsd
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId" /> class.</summary>
		// Token: 0x06004E9F RID: 20127 RVA: 0x00002050 File Offset: 0x00000250
		public SoapId()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId" /> class with an XML <see langword="ID" /> attribute.</summary>
		/// <param name="value">A <see cref="T:System.String" /> that contains an XML <see langword="ID" /> attribute. </param>
		// Token: 0x06004EA0 RID: 20128 RVA: 0x00110869 File Offset: 0x0010EA69
		public SoapId(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		/// <summary>Gets or sets an XML <see langword="ID" /> attribute.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains an XML <see langword="ID" /> attribute.</returns>
		// Token: 0x17000D41 RID: 3393
		// (get) Token: 0x06004EA1 RID: 20129 RVA: 0x0011087D File Offset: 0x0010EA7D
		// (set) Token: 0x06004EA2 RID: 20130 RVA: 0x00110885 File Offset: 0x0010EA85
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		/// <summary>Gets the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x17000D42 RID: 3394
		// (get) Token: 0x06004EA3 RID: 20131 RVA: 0x0011088E File Offset: 0x0010EA8E
		public static string XsdType
		{
			get
			{
				return "ID";
			}
		}

		/// <summary>Returns the XML Schema definition language (XSD) of the current SOAP type.</summary>
		/// <returns>A <see cref="T:System.String" /> that indicates the XSD of the current SOAP type.</returns>
		// Token: 0x06004EA4 RID: 20132 RVA: 0x00110895 File Offset: 0x0010EA95
		public string GetXsdType()
		{
			return SoapId.XsdType;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> into a <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId" /> object.</summary>
		/// <param name="value">The <see langword="String" /> to convert. </param>
		/// <returns>A <see cref="T:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId" /> object that is obtained from <paramref name="value" />.</returns>
		// Token: 0x06004EA5 RID: 20133 RVA: 0x0011089C File Offset: 0x0010EA9C
		public static SoapId Parse(string value)
		{
			return new SoapId(value);
		}

		/// <summary>Returns <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId.Value" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that is obtained from <see cref="P:System.Runtime.Remoting.Metadata.W3cXsd2001.SoapId.Value" />.</returns>
		// Token: 0x06004EA6 RID: 20134 RVA: 0x0011087D File Offset: 0x0010EA7D
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040028DC RID: 10460
		private string _value;
	}
}
