﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Metadata
{
	// Token: 0x020007D5 RID: 2005
	internal class RemotingFieldCachedData
	{
		// Token: 0x06004E20 RID: 20000 RVA: 0x00002050 File Offset: 0x00000250
		internal RemotingFieldCachedData(RuntimeFieldInfo ri)
		{
		}

		// Token: 0x06004E21 RID: 20001 RVA: 0x00002050 File Offset: 0x00000250
		internal RemotingFieldCachedData(SerializationFieldInfo ri)
		{
		}
	}
}
