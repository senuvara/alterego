﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	/// <summary>Customizes SOAP generation and processing for a method. This class cannot be inherited.</summary>
	// Token: 0x020007D8 RID: 2008
	[AttributeUsage(AttributeTargets.Method)]
	[ComVisible(true)]
	public sealed class SoapMethodAttribute : SoapAttribute
	{
		/// <summary>Creates an instance of <see cref="T:System.Runtime.Remoting.Metadata.SoapMethodAttribute" />.</summary>
		// Token: 0x06004E31 RID: 20017 RVA: 0x0010FCFA File Offset: 0x0010DEFA
		public SoapMethodAttribute()
		{
		}

		/// <summary>Gets or sets the XML element name to use for the method response to the target method.</summary>
		/// <returns>The XML element name to use for the method response to the target method.</returns>
		// Token: 0x17000D21 RID: 3361
		// (get) Token: 0x06004E32 RID: 20018 RVA: 0x0010FD60 File Offset: 0x0010DF60
		// (set) Token: 0x06004E33 RID: 20019 RVA: 0x0010FD68 File Offset: 0x0010DF68
		public string ResponseXmlElementName
		{
			get
			{
				return this._responseElement;
			}
			set
			{
				this._responseElement = value;
			}
		}

		/// <summary>Gets or sets the XML element namesapce used for method response to the target method.</summary>
		/// <returns>The XML element namesapce used for method response to the target method.</returns>
		// Token: 0x17000D22 RID: 3362
		// (get) Token: 0x06004E34 RID: 20020 RVA: 0x0010FD71 File Offset: 0x0010DF71
		// (set) Token: 0x06004E35 RID: 20021 RVA: 0x0010FD79 File Offset: 0x0010DF79
		public string ResponseXmlNamespace
		{
			get
			{
				return this._responseNamespace;
			}
			set
			{
				this._responseNamespace = value;
			}
		}

		/// <summary>Gets or sets the XML element name used for the return value from the target method.</summary>
		/// <returns>The XML element name used for the return value from the target method.</returns>
		// Token: 0x17000D23 RID: 3363
		// (get) Token: 0x06004E36 RID: 20022 RVA: 0x0010FD82 File Offset: 0x0010DF82
		// (set) Token: 0x06004E37 RID: 20023 RVA: 0x0010FD8A File Offset: 0x0010DF8A
		public string ReturnXmlElementName
		{
			get
			{
				return this._returnElement;
			}
			set
			{
				this._returnElement = value;
			}
		}

		/// <summary>Gets or sets the SOAPAction header field used with HTTP requests sent with this method. This property is currently not implemented.</summary>
		/// <returns>The SOAPAction header field used with HTTP requests sent with this method.</returns>
		// Token: 0x17000D24 RID: 3364
		// (get) Token: 0x06004E38 RID: 20024 RVA: 0x0010FD93 File Offset: 0x0010DF93
		// (set) Token: 0x06004E39 RID: 20025 RVA: 0x0010FD9B File Offset: 0x0010DF9B
		public string SoapAction
		{
			get
			{
				return this._soapAction;
			}
			set
			{
				this._soapAction = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the target of the current attribute will be serialized as an XML attribute instead of an XML field.</summary>
		/// <returns>The current implementation always returns <see langword="false" />.</returns>
		/// <exception cref="T:System.Runtime.Remoting.RemotingException">An attempt was made to set the current property. </exception>
		// Token: 0x17000D25 RID: 3365
		// (get) Token: 0x06004E3A RID: 20026 RVA: 0x0010FDA4 File Offset: 0x0010DFA4
		// (set) Token: 0x06004E3B RID: 20027 RVA: 0x0010FDAC File Offset: 0x0010DFAC
		public override bool UseAttribute
		{
			get
			{
				return this._useAttribute;
			}
			set
			{
				this._useAttribute = value;
			}
		}

		/// <summary>Gets or sets the XML namespace that is used during serialization of remote method calls of the target method.</summary>
		/// <returns>The XML namespace that is used during serialization of remote method calls of the target method.</returns>
		// Token: 0x17000D26 RID: 3366
		// (get) Token: 0x06004E3C RID: 20028 RVA: 0x0010FDB5 File Offset: 0x0010DFB5
		// (set) Token: 0x06004E3D RID: 20029 RVA: 0x0010FDBD File Offset: 0x0010DFBD
		public override string XmlNamespace
		{
			get
			{
				return this._namespace;
			}
			set
			{
				this._namespace = value;
			}
		}

		// Token: 0x06004E3E RID: 20030 RVA: 0x0010FDC8 File Offset: 0x0010DFC8
		internal override void SetReflectionObject(object reflectionObject)
		{
			MethodBase methodBase = (MethodBase)reflectionObject;
			if (this._responseElement == null)
			{
				this._responseElement = methodBase.Name + "Response";
			}
			if (this._responseNamespace == null)
			{
				this._responseNamespace = SoapServices.GetXmlNamespaceForMethodResponse(methodBase);
			}
			if (this._returnElement == null)
			{
				this._returnElement = "return";
			}
			if (this._soapAction == null)
			{
				this._soapAction = SoapServices.GetXmlNamespaceForMethodCall(methodBase) + "#" + methodBase.Name;
			}
			if (this._namespace == null)
			{
				this._namespace = SoapServices.GetXmlNamespaceForMethodCall(methodBase);
			}
		}

		// Token: 0x040028B6 RID: 10422
		private string _responseElement;

		// Token: 0x040028B7 RID: 10423
		private string _responseNamespace;

		// Token: 0x040028B8 RID: 10424
		private string _returnElement;

		// Token: 0x040028B9 RID: 10425
		private string _soapAction;

		// Token: 0x040028BA RID: 10426
		private bool _useAttribute;

		// Token: 0x040028BB RID: 10427
		private string _namespace;
	}
}
