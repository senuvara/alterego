﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	/// <summary>Provides default functionality for all SOAP attributes.</summary>
	// Token: 0x020007D6 RID: 2006
	[ComVisible(true)]
	public class SoapAttribute : Attribute
	{
		/// <summary>Creates an instance of <see cref="T:System.Runtime.Remoting.Metadata.SoapAttribute" />.</summary>
		// Token: 0x06004E22 RID: 20002 RVA: 0x000020BF File Offset: 0x000002BF
		public SoapAttribute()
		{
		}

		/// <summary>Gets or sets a value indicating whether the type must be nested during SOAP serialization.</summary>
		/// <returns>
		///     <see langword="true" /> if the target object must be nested during SOAP serialization; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000D1C RID: 3356
		// (get) Token: 0x06004E23 RID: 20003 RVA: 0x0010FCBE File Offset: 0x0010DEBE
		// (set) Token: 0x06004E24 RID: 20004 RVA: 0x0010FCC6 File Offset: 0x0010DEC6
		public virtual bool Embedded
		{
			get
			{
				return this._nested;
			}
			set
			{
				this._nested = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the target of the current attribute will be serialized as an XML attribute instead of an XML field.</summary>
		/// <returns>
		///     <see langword="true" /> if the target object of the current attribute must be serialized as an XML attribute; <see langword="false" /> if the target object must be serialized as a subelement.</returns>
		// Token: 0x17000D1D RID: 3357
		// (get) Token: 0x06004E25 RID: 20005 RVA: 0x0010FCCF File Offset: 0x0010DECF
		// (set) Token: 0x06004E26 RID: 20006 RVA: 0x0010FCD7 File Offset: 0x0010DED7
		public virtual bool UseAttribute
		{
			get
			{
				return this._useAttribute;
			}
			set
			{
				this._useAttribute = value;
			}
		}

		/// <summary>Gets or sets the XML namespace name.</summary>
		/// <returns>The XML namespace name under which the target of the current attribute is serialized.</returns>
		// Token: 0x17000D1E RID: 3358
		// (get) Token: 0x06004E27 RID: 20007 RVA: 0x0010FCE0 File Offset: 0x0010DEE0
		// (set) Token: 0x06004E28 RID: 20008 RVA: 0x0010FCE8 File Offset: 0x0010DEE8
		public virtual string XmlNamespace
		{
			get
			{
				return this.ProtXmlNamespace;
			}
			set
			{
				this.ProtXmlNamespace = value;
			}
		}

		// Token: 0x06004E29 RID: 20009 RVA: 0x0010FCF1 File Offset: 0x0010DEF1
		internal virtual void SetReflectionObject(object reflectionObject)
		{
			this.ReflectInfo = reflectionObject;
		}

		// Token: 0x040028AF RID: 10415
		private bool _nested;

		// Token: 0x040028B0 RID: 10416
		private bool _useAttribute;

		/// <summary>The XML namespace to which the target of the current SOAP attribute is serialized.</summary>
		// Token: 0x040028B1 RID: 10417
		protected string ProtXmlNamespace;

		/// <summary>A reflection object used by attribute classes derived from the <see cref="T:System.Runtime.Remoting.Metadata.SoapAttribute" /> class to set XML serialization information.</summary>
		// Token: 0x040028B2 RID: 10418
		protected object ReflectInfo;
	}
}
