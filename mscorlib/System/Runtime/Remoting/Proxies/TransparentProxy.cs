﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using Mono;

namespace System.Runtime.Remoting.Proxies
{
	// Token: 0x0200077D RID: 1917
	[StructLayout(LayoutKind.Sequential)]
	internal class TransparentProxy
	{
		// Token: 0x06004C49 RID: 19529 RVA: 0x0010BB30 File Offset: 0x00109D30
		internal RuntimeType GetProxyType()
		{
			return (RuntimeType)Type.GetTypeFromHandle(this._class.ProxyClass.GetTypeHandle());
		}

		// Token: 0x17000CA9 RID: 3241
		// (get) Token: 0x06004C4A RID: 19530 RVA: 0x0010BB5A File Offset: 0x00109D5A
		private bool IsContextBoundObject
		{
			get
			{
				return this.GetProxyType().IsContextful;
			}
		}

		// Token: 0x17000CAA RID: 3242
		// (get) Token: 0x06004C4B RID: 19531 RVA: 0x0010BB67 File Offset: 0x00109D67
		private Context TargetContext
		{
			get
			{
				return this._rp._targetContext;
			}
		}

		// Token: 0x06004C4C RID: 19532 RVA: 0x0010BB74 File Offset: 0x00109D74
		private bool InCurrentContext()
		{
			return this.IsContextBoundObject && this.TargetContext == Thread.CurrentContext;
		}

		// Token: 0x06004C4D RID: 19533 RVA: 0x0010BB90 File Offset: 0x00109D90
		internal object LoadRemoteFieldNew(IntPtr classPtr, IntPtr fieldPtr)
		{
			RuntimeClassHandle runtimeClassHandle = new RuntimeClassHandle(classPtr);
			RuntimeFieldHandle handle = new RuntimeFieldHandle(fieldPtr);
			RuntimeTypeHandle typeHandle = runtimeClassHandle.GetTypeHandle();
			FieldInfo fieldFromHandle = FieldInfo.GetFieldFromHandle(handle);
			if (this.InCurrentContext())
			{
				object server = this._rp._server;
				return fieldFromHandle.GetValue(server);
			}
			string fullName = Type.GetTypeFromHandle(typeHandle).FullName;
			string name = fieldFromHandle.Name;
			object[] in_args = new object[]
			{
				fullName,
				name
			};
			object[] out_args = new object[1];
			MethodInfo method = typeof(object).GetMethod("FieldGetter", BindingFlags.Instance | BindingFlags.NonPublic);
			if (method == null)
			{
				throw new MissingMethodException("System.Object", "FieldGetter");
			}
			MonoMethodMessage msg = new MonoMethodMessage(method, in_args, out_args);
			Exception ex;
			object[] array;
			RealProxy.PrivateInvoke(this._rp, msg, out ex, out array);
			if (ex != null)
			{
				throw ex;
			}
			return array[0];
		}

		// Token: 0x06004C4E RID: 19534 RVA: 0x0010BC5C File Offset: 0x00109E5C
		internal void StoreRemoteField(IntPtr classPtr, IntPtr fieldPtr, object arg)
		{
			RuntimeClassHandle runtimeClassHandle = new RuntimeClassHandle(classPtr);
			RuntimeFieldHandle handle = new RuntimeFieldHandle(fieldPtr);
			RuntimeTypeHandle typeHandle = runtimeClassHandle.GetTypeHandle();
			FieldInfo fieldFromHandle = FieldInfo.GetFieldFromHandle(handle);
			if (this.InCurrentContext())
			{
				object server = this._rp._server;
				fieldFromHandle.SetValue(server, arg);
				return;
			}
			string fullName = Type.GetTypeFromHandle(typeHandle).FullName;
			string name = fieldFromHandle.Name;
			object[] in_args = new object[]
			{
				fullName,
				name,
				arg
			};
			MethodInfo method = typeof(object).GetMethod("FieldSetter", BindingFlags.Instance | BindingFlags.NonPublic);
			if (method == null)
			{
				throw new MissingMethodException("System.Object", "FieldSetter");
			}
			MonoMethodMessage msg = new MonoMethodMessage(method, in_args, null);
			Exception ex;
			object[] array;
			RealProxy.PrivateInvoke(this._rp, msg, out ex, out array);
			if (ex != null)
			{
				throw ex;
			}
		}

		// Token: 0x06004C4F RID: 19535 RVA: 0x00002050 File Offset: 0x00000250
		public TransparentProxy()
		{
		}

		// Token: 0x04002828 RID: 10280
		public RealProxy _rp;

		// Token: 0x04002829 RID: 10281
		private RuntimeRemoteClassHandle _class;

		// Token: 0x0400282A RID: 10282
		private bool _custom_type_info;
	}
}
