﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting
{
	// Token: 0x02000759 RID: 1881
	[Serializable]
	internal class EnvoyInfo : IEnvoyInfo
	{
		// Token: 0x06004B0D RID: 19213 RVA: 0x00107235 File Offset: 0x00105435
		public EnvoyInfo(IMessageSink sinks)
		{
			this.envoySinks = sinks;
		}

		// Token: 0x17000C79 RID: 3193
		// (get) Token: 0x06004B0E RID: 19214 RVA: 0x00107244 File Offset: 0x00105444
		// (set) Token: 0x06004B0F RID: 19215 RVA: 0x0010724C File Offset: 0x0010544C
		public IMessageSink EnvoySinks
		{
			get
			{
				return this.envoySinks;
			}
			set
			{
				this.envoySinks = value;
			}
		}

		// Token: 0x040027CB RID: 10187
		private IMessageSink envoySinks;
	}
}
