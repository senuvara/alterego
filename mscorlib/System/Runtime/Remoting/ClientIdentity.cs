﻿using System;

namespace System.Runtime.Remoting
{
	// Token: 0x0200075F RID: 1887
	internal class ClientIdentity : Identity
	{
		// Token: 0x06004B29 RID: 19241 RVA: 0x00107367 File Offset: 0x00105567
		public ClientIdentity(string objectUri, ObjRef objRef) : base(objectUri)
		{
			this._objRef = objRef;
			this._envoySink = ((this._objRef.EnvoyInfo != null) ? this._objRef.EnvoyInfo.EnvoySinks : null);
		}

		// Token: 0x17000C87 RID: 3207
		// (get) Token: 0x06004B2A RID: 19242 RVA: 0x0010739D File Offset: 0x0010559D
		// (set) Token: 0x06004B2B RID: 19243 RVA: 0x001073AF File Offset: 0x001055AF
		public MarshalByRefObject ClientProxy
		{
			get
			{
				return (MarshalByRefObject)this._proxyReference.Target;
			}
			set
			{
				this._proxyReference = new WeakReference(value);
			}
		}

		// Token: 0x06004B2C RID: 19244 RVA: 0x001073BD File Offset: 0x001055BD
		public override ObjRef CreateObjRef(Type requestedType)
		{
			return this._objRef;
		}

		// Token: 0x17000C88 RID: 3208
		// (get) Token: 0x06004B2D RID: 19245 RVA: 0x001073C5 File Offset: 0x001055C5
		public string TargetUri
		{
			get
			{
				return this._objRef.URI;
			}
		}

		// Token: 0x040027D3 RID: 10195
		private WeakReference _proxyReference;
	}
}
