﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting
{
	/// <summary>Holds values for an object type registered on the client end as a type that can be activated on the server.</summary>
	// Token: 0x02000756 RID: 1878
	[ComVisible(true)]
	public class ActivatedClientTypeEntry : TypeEntry
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.ActivatedClientTypeEntry" /> class with the given <see cref="T:System.Type" /> and application URL.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of the client activated type. </param>
		/// <param name="appUrl">The URL of the application to activate the type in. </param>
		// Token: 0x06004B00 RID: 19200 RVA: 0x001070D4 File Offset: 0x001052D4
		public ActivatedClientTypeEntry(Type type, string appUrl)
		{
			base.AssemblyName = type.Assembly.FullName;
			base.TypeName = type.FullName;
			this.applicationUrl = appUrl;
			this.obj_type = type;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.ActivatedClientTypeEntry" /> class with the given type name, assembly name, and application URL.</summary>
		/// <param name="typeName">The type name of the client activated type. </param>
		/// <param name="assemblyName">The assembly name of the client activated type. </param>
		/// <param name="appUrl">The URL of the application to activate the type in. </param>
		// Token: 0x06004B01 RID: 19201 RVA: 0x00107108 File Offset: 0x00105308
		public ActivatedClientTypeEntry(string typeName, string assemblyName, string appUrl)
		{
			base.AssemblyName = assemblyName;
			base.TypeName = typeName;
			this.applicationUrl = appUrl;
			Assembly assembly = Assembly.Load(assemblyName);
			this.obj_type = assembly.GetType(typeName);
			if (this.obj_type == null)
			{
				throw new RemotingException("Type not found: " + typeName + ", " + assemblyName);
			}
		}

		/// <summary>Gets the URL of the application to activate the type in.</summary>
		/// <returns>The URL of the application to activate the type in.</returns>
		// Token: 0x17000C74 RID: 3188
		// (get) Token: 0x06004B02 RID: 19202 RVA: 0x00107169 File Offset: 0x00105369
		public string ApplicationUrl
		{
			get
			{
				return this.applicationUrl;
			}
		}

		/// <summary>Gets or sets the context attributes for the client-activated type.</summary>
		/// <returns>The context attributes for the client activated type.</returns>
		// Token: 0x17000C75 RID: 3189
		// (get) Token: 0x06004B03 RID: 19203 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		// (set) Token: 0x06004B04 RID: 19204 RVA: 0x000020D3 File Offset: 0x000002D3
		public IContextAttribute[] ContextAttributes
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		/// <summary>Gets the <see cref="T:System.Type" /> of the client-activated type.</summary>
		/// <returns>Gets the <see cref="T:System.Type" /> of the client-activated type.</returns>
		// Token: 0x17000C76 RID: 3190
		// (get) Token: 0x06004B05 RID: 19205 RVA: 0x00107171 File Offset: 0x00105371
		public Type ObjectType
		{
			get
			{
				return this.obj_type;
			}
		}

		/// <summary>Returns the type name, assembly name, and application URL of the client-activated type as a <see cref="T:System.String" />.</summary>
		/// <returns>The type name, assembly name, and application URL of the client-activated type as a <see cref="T:System.String" />.</returns>
		// Token: 0x06004B06 RID: 19206 RVA: 0x00107179 File Offset: 0x00105379
		public override string ToString()
		{
			return base.TypeName + base.AssemblyName + this.ApplicationUrl;
		}

		// Token: 0x040027C4 RID: 10180
		private string applicationUrl;

		// Token: 0x040027C5 RID: 10181
		private Type obj_type;
	}
}
