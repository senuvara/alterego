﻿using System;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Remoting.Services;

namespace System.Runtime.Remoting
{
	// Token: 0x0200076D RID: 1901
	internal abstract class ServerIdentity : Identity
	{
		// Token: 0x06004BDE RID: 19422 RVA: 0x0010A766 File Offset: 0x00108966
		public ServerIdentity(string objectUri, Context context, Type objectType) : base(objectUri)
		{
			this._objectType = objectType;
			this._context = context;
		}

		// Token: 0x17000C96 RID: 3222
		// (get) Token: 0x06004BDF RID: 19423 RVA: 0x0010A77D File Offset: 0x0010897D
		public Type ObjectType
		{
			get
			{
				return this._objectType;
			}
		}

		// Token: 0x06004BE0 RID: 19424 RVA: 0x0010A785 File Offset: 0x00108985
		public void StartTrackingLifetime(ILease lease)
		{
			if (lease != null && lease.CurrentState == LeaseState.Null)
			{
				lease = null;
			}
			if (lease != null)
			{
				if (!(lease is Lease))
				{
					lease = new Lease();
				}
				this._lease = (Lease)lease;
				LifetimeServices.TrackLifetime(this);
			}
		}

		// Token: 0x06004BE1 RID: 19425 RVA: 0x0010A7B9 File Offset: 0x001089B9
		public virtual void OnLifetimeExpired()
		{
			this.DisposeServerObject();
		}

		// Token: 0x06004BE2 RID: 19426 RVA: 0x0010A7C4 File Offset: 0x001089C4
		public override ObjRef CreateObjRef(Type requestedType)
		{
			if (this._objRef != null)
			{
				this._objRef.UpdateChannelInfo();
				return this._objRef;
			}
			if (requestedType == null)
			{
				requestedType = this._objectType;
			}
			this._objRef = new ObjRef();
			this._objRef.TypeInfo = new TypeInfo(requestedType);
			this._objRef.URI = this._objectUri;
			if (this._envoySink != null && !(this._envoySink is EnvoyTerminatorSink))
			{
				this._objRef.EnvoyInfo = new EnvoyInfo(this._envoySink);
			}
			return this._objRef;
		}

		// Token: 0x06004BE3 RID: 19427 RVA: 0x0010A85C File Offset: 0x00108A5C
		public void AttachServerObject(MarshalByRefObject serverObject, Context context)
		{
			this.DisposeServerObject();
			this._context = context;
			this._serverObject = serverObject;
			if (RemotingServices.IsTransparentProxy(serverObject))
			{
				RealProxy realProxy = RemotingServices.GetRealProxy(serverObject);
				if (realProxy.ObjectIdentity == null)
				{
					realProxy.ObjectIdentity = this;
					return;
				}
			}
			else
			{
				if (this._objectType.IsContextful)
				{
					this._envoySink = context.CreateEnvoySink(serverObject);
				}
				this._serverObject.ObjectIdentity = this;
			}
		}

		// Token: 0x17000C97 RID: 3223
		// (get) Token: 0x06004BE4 RID: 19428 RVA: 0x0010A8C2 File Offset: 0x00108AC2
		public Lease Lease
		{
			get
			{
				return this._lease;
			}
		}

		// Token: 0x17000C98 RID: 3224
		// (get) Token: 0x06004BE5 RID: 19429 RVA: 0x0010A8CA File Offset: 0x00108ACA
		// (set) Token: 0x06004BE6 RID: 19430 RVA: 0x0010A8D2 File Offset: 0x00108AD2
		public Context Context
		{
			get
			{
				return this._context;
			}
			set
			{
				this._context = value;
			}
		}

		// Token: 0x06004BE7 RID: 19431
		public abstract IMessage SyncObjectProcessMessage(IMessage msg);

		// Token: 0x06004BE8 RID: 19432
		public abstract IMessageCtrl AsyncObjectProcessMessage(IMessage msg, IMessageSink replySink);

		// Token: 0x06004BE9 RID: 19433 RVA: 0x0010A8DB File Offset: 0x00108ADB
		protected void DisposeServerObject()
		{
			if (this._serverObject != null)
			{
				object serverObject = this._serverObject;
				this._serverObject.ObjectIdentity = null;
				this._serverObject = null;
				this._serverSink = null;
				TrackingServices.NotifyDisconnectedObject(serverObject);
			}
		}

		// Token: 0x0400280A RID: 10250
		protected Type _objectType;

		// Token: 0x0400280B RID: 10251
		protected MarshalByRefObject _serverObject;

		// Token: 0x0400280C RID: 10252
		protected IMessageSink _serverSink;

		// Token: 0x0400280D RID: 10253
		protected Context _context;

		// Token: 0x0400280E RID: 10254
		protected Lease _lease;
	}
}
