﻿using System;
using System.Collections;
using System.Runtime.Remoting.Channels;

namespace System.Runtime.Remoting
{
	// Token: 0x02000766 RID: 1894
	internal class ProviderData
	{
		// Token: 0x06004B93 RID: 19347 RVA: 0x00109558 File Offset: 0x00107758
		public void CopyFrom(ProviderData other)
		{
			if (this.Ref == null)
			{
				this.Ref = other.Ref;
			}
			if (this.Id == null)
			{
				this.Id = other.Id;
			}
			if (this.Type == null)
			{
				this.Type = other.Type;
			}
			foreach (object obj in other.CustomProperties)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				if (!this.CustomProperties.ContainsKey(dictionaryEntry.Key))
				{
					this.CustomProperties[dictionaryEntry.Key] = dictionaryEntry.Value;
				}
			}
			if (other.CustomData != null)
			{
				if (this.CustomData == null)
				{
					this.CustomData = new ArrayList();
				}
				foreach (object obj2 in other.CustomData)
				{
					SinkProviderData value = (SinkProviderData)obj2;
					this.CustomData.Add(value);
				}
			}
		}

		// Token: 0x06004B94 RID: 19348 RVA: 0x00109680 File Offset: 0x00107880
		public ProviderData()
		{
		}

		// Token: 0x040027FA RID: 10234
		internal string Ref;

		// Token: 0x040027FB RID: 10235
		internal string Type;

		// Token: 0x040027FC RID: 10236
		internal string Id;

		// Token: 0x040027FD RID: 10237
		internal Hashtable CustomProperties = new Hashtable();

		// Token: 0x040027FE RID: 10238
		internal IList CustomData;
	}
}
