﻿using System;

namespace System.Runtime.Remoting
{
	// Token: 0x02000775 RID: 1909
	[Serializable]
	internal class TypeInfo : IRemotingTypeInfo
	{
		// Token: 0x06004C22 RID: 19490 RVA: 0x0010B42C File Offset: 0x0010962C
		public TypeInfo(Type type)
		{
			if (type.IsInterface)
			{
				this.serverType = typeof(MarshalByRefObject).AssemblyQualifiedName;
				this.serverHierarchy = new string[0];
				this.interfacesImplemented = new string[]
				{
					type.AssemblyQualifiedName
				};
				return;
			}
			this.serverType = type.AssemblyQualifiedName;
			int num = 0;
			Type baseType = type.BaseType;
			while (baseType != typeof(MarshalByRefObject) && baseType != null)
			{
				baseType = baseType.BaseType;
				num++;
			}
			this.serverHierarchy = new string[num];
			baseType = type.BaseType;
			for (int i = 0; i < num; i++)
			{
				this.serverHierarchy[i] = baseType.AssemblyQualifiedName;
				baseType = baseType.BaseType;
			}
			Type[] interfaces = type.GetInterfaces();
			this.interfacesImplemented = new string[interfaces.Length];
			for (int j = 0; j < interfaces.Length; j++)
			{
				this.interfacesImplemented[j] = interfaces[j].AssemblyQualifiedName;
			}
		}

		// Token: 0x17000CA0 RID: 3232
		// (get) Token: 0x06004C23 RID: 19491 RVA: 0x0010B528 File Offset: 0x00109728
		// (set) Token: 0x06004C24 RID: 19492 RVA: 0x0010B530 File Offset: 0x00109730
		public string TypeName
		{
			get
			{
				return this.serverType;
			}
			set
			{
				this.serverType = value;
			}
		}

		// Token: 0x06004C25 RID: 19493 RVA: 0x0010B53C File Offset: 0x0010973C
		public bool CanCastTo(Type fromType, object o)
		{
			if (fromType == typeof(object))
			{
				return true;
			}
			if (fromType == typeof(MarshalByRefObject))
			{
				return true;
			}
			string text = fromType.AssemblyQualifiedName;
			int num = text.IndexOf(',');
			if (num != -1)
			{
				num = text.IndexOf(',', num + 1);
			}
			if (num != -1)
			{
				text = text.Substring(0, num + 1);
			}
			else
			{
				text += ",";
			}
			if ((this.serverType + ",").StartsWith(text))
			{
				return true;
			}
			if (this.serverHierarchy != null)
			{
				string[] array = this.serverHierarchy;
				for (int i = 0; i < array.Length; i++)
				{
					if ((array[i] + ",").StartsWith(text))
					{
						return true;
					}
				}
			}
			if (this.interfacesImplemented != null)
			{
				string[] array = this.interfacesImplemented;
				for (int i = 0; i < array.Length; i++)
				{
					if ((array[i] + ",").StartsWith(text))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x0400281B RID: 10267
		private string serverType;

		// Token: 0x0400281C RID: 10268
		private string[] serverHierarchy;

		// Token: 0x0400281D RID: 10269
		private string[] interfacesImplemented;
	}
}
