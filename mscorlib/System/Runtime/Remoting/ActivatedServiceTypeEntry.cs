﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting
{
	/// <summary>Holds values for an object type registered on the service end as one that can be activated on request from a client.</summary>
	// Token: 0x02000757 RID: 1879
	[ComVisible(true)]
	public class ActivatedServiceTypeEntry : TypeEntry
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.ActivatedServiceTypeEntry" /> class with the given <see cref="T:System.Type" />.</summary>
		/// <param name="type">The <see cref="T:System.Type" /> of the client-activated service type. </param>
		// Token: 0x06004B07 RID: 19207 RVA: 0x00107192 File Offset: 0x00105392
		public ActivatedServiceTypeEntry(Type type)
		{
			base.AssemblyName = type.Assembly.FullName;
			base.TypeName = type.FullName;
			this.obj_type = type;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.ActivatedServiceTypeEntry" /> class with the given type name and assembly name.</summary>
		/// <param name="typeName">The type name of the client-activated service type. </param>
		/// <param name="assemblyName">The assembly name of the client-activated service type. </param>
		// Token: 0x06004B08 RID: 19208 RVA: 0x001071C0 File Offset: 0x001053C0
		public ActivatedServiceTypeEntry(string typeName, string assemblyName)
		{
			base.AssemblyName = assemblyName;
			base.TypeName = typeName;
			Assembly assembly = Assembly.Load(assemblyName);
			this.obj_type = assembly.GetType(typeName);
			if (this.obj_type == null)
			{
				throw new RemotingException("Type not found: " + typeName + ", " + assemblyName);
			}
		}

		/// <summary>Gets or sets the context attributes for the client-activated service type.</summary>
		/// <returns>The context attributes for the client-activated service type.</returns>
		// Token: 0x17000C77 RID: 3191
		// (get) Token: 0x06004B09 RID: 19209 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		// (set) Token: 0x06004B0A RID: 19210 RVA: 0x000020D3 File Offset: 0x000002D3
		public IContextAttribute[] ContextAttributes
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		/// <summary>Gets the <see cref="T:System.Type" /> of the client-activated service type.</summary>
		/// <returns>The <see cref="T:System.Type" /> of the client-activated service type.</returns>
		// Token: 0x17000C78 RID: 3192
		// (get) Token: 0x06004B0B RID: 19211 RVA: 0x0010721A File Offset: 0x0010541A
		public Type ObjectType
		{
			get
			{
				return this.obj_type;
			}
		}

		/// <summary>Returns the type and assembly name of the client-activated service type as a <see cref="T:System.String" />.</summary>
		/// <returns>The type and assembly name of the client-activated service type as a <see cref="T:System.String" />.</returns>
		// Token: 0x06004B0C RID: 19212 RVA: 0x00107222 File Offset: 0x00105422
		public override string ToString()
		{
			return base.AssemblyName + base.TypeName;
		}

		// Token: 0x040027C6 RID: 10182
		private Type obj_type;
	}
}
