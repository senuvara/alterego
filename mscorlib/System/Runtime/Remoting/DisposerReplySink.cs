﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting
{
	// Token: 0x02000771 RID: 1905
	internal class DisposerReplySink : IMessageSink
	{
		// Token: 0x06004BF8 RID: 19448 RVA: 0x0010AB81 File Offset: 0x00108D81
		public DisposerReplySink(IMessageSink next, IDisposable disposable)
		{
			this._next = next;
			this._disposable = disposable;
		}

		// Token: 0x06004BF9 RID: 19449 RVA: 0x0010AB97 File Offset: 0x00108D97
		public IMessage SyncProcessMessage(IMessage msg)
		{
			this._disposable.Dispose();
			return this._next.SyncProcessMessage(msg);
		}

		// Token: 0x06004BFA RID: 19450 RVA: 0x000175EA File Offset: 0x000157EA
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000C99 RID: 3225
		// (get) Token: 0x06004BFB RID: 19451 RVA: 0x0010ABB0 File Offset: 0x00108DB0
		public IMessageSink NextSink
		{
			get
			{
				return this._next;
			}
		}

		// Token: 0x04002810 RID: 10256
		private IMessageSink _next;

		// Token: 0x04002811 RID: 10257
		private IDisposable _disposable;
	}
}
