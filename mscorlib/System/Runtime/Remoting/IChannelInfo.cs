﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	/// <summary>Provides custom channel information that is carried along with the <see cref="T:System.Runtime.Remoting.ObjRef" />.</summary>
	// Token: 0x0200075A RID: 1882
	[ComVisible(true)]
	public interface IChannelInfo
	{
		/// <summary>Gets and sets the channel data for each channel.</summary>
		/// <returns>The channel data for each channel.</returns>
		/// <exception cref="T:System.Security.SecurityException">The immediate caller makes the call through a reference to the interface and does not have infrastructure permission. </exception>
		// Token: 0x17000C7A RID: 3194
		// (get) Token: 0x06004B10 RID: 19216
		// (set) Token: 0x06004B11 RID: 19217
		object[] ChannelData { get; set; }
	}
}
