﻿using System;
using System.Collections;
using System.Threading;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x02000783 RID: 1923
	internal class Lease : MarshalByRefObject, ILease
	{
		// Token: 0x06004C8B RID: 19595 RVA: 0x0010C754 File Offset: 0x0010A954
		public Lease()
		{
			this._currentState = LeaseState.Initial;
			this._initialLeaseTime = LifetimeServices.LeaseTime;
			this._renewOnCallTime = LifetimeServices.RenewOnCallTime;
			this._sponsorshipTimeout = LifetimeServices.SponsorshipTimeout;
			this._leaseExpireTime = DateTime.UtcNow + this._initialLeaseTime;
		}

		// Token: 0x17000CB3 RID: 3251
		// (get) Token: 0x06004C8C RID: 19596 RVA: 0x0010C7A5 File Offset: 0x0010A9A5
		public TimeSpan CurrentLeaseTime
		{
			get
			{
				return this._leaseExpireTime - DateTime.UtcNow;
			}
		}

		// Token: 0x17000CB4 RID: 3252
		// (get) Token: 0x06004C8D RID: 19597 RVA: 0x0010C7B7 File Offset: 0x0010A9B7
		public LeaseState CurrentState
		{
			get
			{
				return this._currentState;
			}
		}

		// Token: 0x06004C8E RID: 19598 RVA: 0x0010C7BF File Offset: 0x0010A9BF
		public void Activate()
		{
			this._currentState = LeaseState.Active;
		}

		// Token: 0x17000CB5 RID: 3253
		// (get) Token: 0x06004C8F RID: 19599 RVA: 0x0010C7C8 File Offset: 0x0010A9C8
		// (set) Token: 0x06004C90 RID: 19600 RVA: 0x0010C7D0 File Offset: 0x0010A9D0
		public TimeSpan InitialLeaseTime
		{
			get
			{
				return this._initialLeaseTime;
			}
			set
			{
				if (this._currentState != LeaseState.Initial)
				{
					throw new RemotingException("InitialLeaseTime property can only be set when the lease is in initial state; state is " + this._currentState + ".");
				}
				this._initialLeaseTime = value;
				this._leaseExpireTime = DateTime.UtcNow + this._initialLeaseTime;
				if (value == TimeSpan.Zero)
				{
					this._currentState = LeaseState.Null;
				}
			}
		}

		// Token: 0x17000CB6 RID: 3254
		// (get) Token: 0x06004C91 RID: 19601 RVA: 0x0010C837 File Offset: 0x0010AA37
		// (set) Token: 0x06004C92 RID: 19602 RVA: 0x0010C83F File Offset: 0x0010AA3F
		public TimeSpan RenewOnCallTime
		{
			get
			{
				return this._renewOnCallTime;
			}
			set
			{
				if (this._currentState != LeaseState.Initial)
				{
					throw new RemotingException("RenewOnCallTime property can only be set when the lease is in initial state; state is " + this._currentState + ".");
				}
				this._renewOnCallTime = value;
			}
		}

		// Token: 0x17000CB7 RID: 3255
		// (get) Token: 0x06004C93 RID: 19603 RVA: 0x0010C871 File Offset: 0x0010AA71
		// (set) Token: 0x06004C94 RID: 19604 RVA: 0x0010C879 File Offset: 0x0010AA79
		public TimeSpan SponsorshipTimeout
		{
			get
			{
				return this._sponsorshipTimeout;
			}
			set
			{
				if (this._currentState != LeaseState.Initial)
				{
					throw new RemotingException("SponsorshipTimeout property can only be set when the lease is in initial state; state is " + this._currentState + ".");
				}
				this._sponsorshipTimeout = value;
			}
		}

		// Token: 0x06004C95 RID: 19605 RVA: 0x0010C8AB File Offset: 0x0010AAAB
		public void Register(ISponsor obj)
		{
			this.Register(obj, TimeSpan.Zero);
		}

		// Token: 0x06004C96 RID: 19606 RVA: 0x0010C8BC File Offset: 0x0010AABC
		public void Register(ISponsor obj, TimeSpan renewalTime)
		{
			lock (this)
			{
				if (this._sponsors == null)
				{
					this._sponsors = new ArrayList();
				}
				this._sponsors.Add(obj);
			}
			if (renewalTime != TimeSpan.Zero)
			{
				this.Renew(renewalTime);
			}
		}

		// Token: 0x06004C97 RID: 19607 RVA: 0x0010C928 File Offset: 0x0010AB28
		public TimeSpan Renew(TimeSpan renewalTime)
		{
			DateTime dateTime = DateTime.UtcNow + renewalTime;
			if (dateTime > this._leaseExpireTime)
			{
				this._leaseExpireTime = dateTime;
			}
			return this.CurrentLeaseTime;
		}

		// Token: 0x06004C98 RID: 19608 RVA: 0x0010C95C File Offset: 0x0010AB5C
		public void Unregister(ISponsor obj)
		{
			lock (this)
			{
				if (this._sponsors != null)
				{
					for (int i = 0; i < this._sponsors.Count; i++)
					{
						if (this._sponsors[i] == obj)
						{
							this._sponsors.RemoveAt(i);
							break;
						}
					}
				}
			}
		}

		// Token: 0x06004C99 RID: 19609 RVA: 0x0010C9D0 File Offset: 0x0010ABD0
		internal void UpdateState()
		{
			if (this._currentState != LeaseState.Active)
			{
				return;
			}
			if (this.CurrentLeaseTime > TimeSpan.Zero)
			{
				return;
			}
			if (this._sponsors != null)
			{
				this._currentState = LeaseState.Renewing;
				lock (this)
				{
					this._renewingSponsors = new Queue(this._sponsors);
				}
				this.CheckNextSponsor();
				return;
			}
			this._currentState = LeaseState.Expired;
		}

		// Token: 0x06004C9A RID: 19610 RVA: 0x0010CA50 File Offset: 0x0010AC50
		private void CheckNextSponsor()
		{
			if (this._renewingSponsors.Count == 0)
			{
				this._currentState = LeaseState.Expired;
				this._renewingSponsors = null;
				return;
			}
			ISponsor @object = (ISponsor)this._renewingSponsors.Peek();
			this._renewalDelegate = new Lease.RenewalDelegate(@object.Renewal);
			IAsyncResult asyncResult = this._renewalDelegate.BeginInvoke(this, null, null);
			ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, new WaitOrTimerCallback(this.ProcessSponsorResponse), asyncResult, this._sponsorshipTimeout, true);
		}

		// Token: 0x06004C9B RID: 19611 RVA: 0x0010CACC File Offset: 0x0010ACCC
		private void ProcessSponsorResponse(object state, bool timedOut)
		{
			if (!timedOut)
			{
				try
				{
					IAsyncResult result = (IAsyncResult)state;
					TimeSpan timeSpan = this._renewalDelegate.EndInvoke(result);
					if (timeSpan != TimeSpan.Zero)
					{
						this.Renew(timeSpan);
						this._currentState = LeaseState.Active;
						this._renewingSponsors = null;
						return;
					}
				}
				catch
				{
				}
			}
			this.Unregister((ISponsor)this._renewingSponsors.Dequeue());
			this.CheckNextSponsor();
		}

		// Token: 0x0400283A RID: 10298
		private DateTime _leaseExpireTime;

		// Token: 0x0400283B RID: 10299
		private LeaseState _currentState;

		// Token: 0x0400283C RID: 10300
		private TimeSpan _initialLeaseTime;

		// Token: 0x0400283D RID: 10301
		private TimeSpan _renewOnCallTime;

		// Token: 0x0400283E RID: 10302
		private TimeSpan _sponsorshipTimeout;

		// Token: 0x0400283F RID: 10303
		private ArrayList _sponsors;

		// Token: 0x04002840 RID: 10304
		private Queue _renewingSponsors;

		// Token: 0x04002841 RID: 10305
		private Lease.RenewalDelegate _renewalDelegate;

		// Token: 0x02000784 RID: 1924
		// (Invoke) Token: 0x06004C9D RID: 19613
		private delegate TimeSpan RenewalDelegate(ILease lease);
	}
}
