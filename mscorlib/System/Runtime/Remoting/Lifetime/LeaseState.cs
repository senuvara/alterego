﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Lifetime
{
	/// <summary>Indicates the possible lease states of a lifetime lease.</summary>
	// Token: 0x02000787 RID: 1927
	[ComVisible(true)]
	[Serializable]
	public enum LeaseState
	{
		/// <summary>The lease is not initialized.</summary>
		// Token: 0x04002846 RID: 10310
		Null,
		/// <summary>The lease has been created, but is not yet active.</summary>
		// Token: 0x04002847 RID: 10311
		Initial,
		/// <summary>The lease is active and has not expired.</summary>
		// Token: 0x04002848 RID: 10312
		Active,
		/// <summary>The lease has expired and is seeking sponsorship.</summary>
		// Token: 0x04002849 RID: 10313
		Renewing,
		/// <summary>The lease has expired and cannot be renewed.</summary>
		// Token: 0x0400284A RID: 10314
		Expired
	}
}
