﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Lifetime
{
	/// <summary>Controls the.NET remoting lifetime services.</summary>
	// Token: 0x02000788 RID: 1928
	[ComVisible(true)]
	public sealed class LifetimeServices
	{
		// Token: 0x06004CAC RID: 19628 RVA: 0x0010CDE0 File Offset: 0x0010AFE0
		static LifetimeServices()
		{
			LifetimeServices._leaseManagerPollTime = TimeSpan.FromSeconds(10.0);
			LifetimeServices._leaseTime = TimeSpan.FromMinutes(5.0);
			LifetimeServices._renewOnCallTime = TimeSpan.FromMinutes(2.0);
			LifetimeServices._sponsorshipTimeout = TimeSpan.FromMinutes(2.0);
		}

		/// <summary>Creates an instance of <see cref="T:System.Runtime.Remoting.Lifetime.LifetimeServices" />.</summary>
		// Token: 0x06004CAD RID: 19629 RVA: 0x00002050 File Offset: 0x00000250
		[Obsolete("Call the static methods directly on this type instead", true)]
		public LifetimeServices()
		{
		}

		/// <summary>Gets or sets the time interval between each activation of the lease manager to clean up expired leases.</summary>
		/// <returns>The default amount of time the lease manager sleeps after checking for expired leases.</returns>
		/// <exception cref="T:System.Security.SecurityException">At least one of the callers higher in the callstack does not have permission to configure remoting types and channels. This exception is thrown only when setting the property value. </exception>
		// Token: 0x17000CB9 RID: 3257
		// (get) Token: 0x06004CAE RID: 19630 RVA: 0x0010CE43 File Offset: 0x0010B043
		// (set) Token: 0x06004CAF RID: 19631 RVA: 0x0010CE4A File Offset: 0x0010B04A
		public static TimeSpan LeaseManagerPollTime
		{
			get
			{
				return LifetimeServices._leaseManagerPollTime;
			}
			set
			{
				LifetimeServices._leaseManagerPollTime = value;
				LifetimeServices._leaseManager.SetPollTime(value);
			}
		}

		/// <summary>Gets or sets the initial lease time span for an <see cref="T:System.AppDomain" />.</summary>
		/// <returns>The initial lease <see cref="T:System.TimeSpan" /> for objects that can have leases in the <see cref="T:System.AppDomain" />.</returns>
		/// <exception cref="T:System.Security.SecurityException">At least one of the callers higher in the callstack does not have permission to configure remoting types and channels. This exception is thrown only when setting the property value. </exception>
		// Token: 0x17000CBA RID: 3258
		// (get) Token: 0x06004CB0 RID: 19632 RVA: 0x0010CE5D File Offset: 0x0010B05D
		// (set) Token: 0x06004CB1 RID: 19633 RVA: 0x0010CE64 File Offset: 0x0010B064
		public static TimeSpan LeaseTime
		{
			get
			{
				return LifetimeServices._leaseTime;
			}
			set
			{
				LifetimeServices._leaseTime = value;
			}
		}

		/// <summary>Gets or sets the amount of time by which the lease is extended every time a call comes in on the server object.</summary>
		/// <returns>The <see cref="T:System.TimeSpan" /> by which a lifetime lease in the current <see cref="T:System.AppDomain" /> is extended after each call.</returns>
		/// <exception cref="T:System.Security.SecurityException">At least one of the callers higher in the callstack does not have permission to configure remoting types and channels. This exception is thrown only when setting the property value. </exception>
		// Token: 0x17000CBB RID: 3259
		// (get) Token: 0x06004CB2 RID: 19634 RVA: 0x0010CE6C File Offset: 0x0010B06C
		// (set) Token: 0x06004CB3 RID: 19635 RVA: 0x0010CE73 File Offset: 0x0010B073
		public static TimeSpan RenewOnCallTime
		{
			get
			{
				return LifetimeServices._renewOnCallTime;
			}
			set
			{
				LifetimeServices._renewOnCallTime = value;
			}
		}

		/// <summary>Gets or sets the amount of time the lease manager waits for a sponsor to return with a lease renewal time.</summary>
		/// <returns>The initial sponsorship time-out.</returns>
		/// <exception cref="T:System.Security.SecurityException">At least one of the callers higher in the callstack does not have permission to configure remoting types and channels. This exception is thrown only when setting the property value. </exception>
		// Token: 0x17000CBC RID: 3260
		// (get) Token: 0x06004CB4 RID: 19636 RVA: 0x0010CE7B File Offset: 0x0010B07B
		// (set) Token: 0x06004CB5 RID: 19637 RVA: 0x0010CE82 File Offset: 0x0010B082
		public static TimeSpan SponsorshipTimeout
		{
			get
			{
				return LifetimeServices._sponsorshipTimeout;
			}
			set
			{
				LifetimeServices._sponsorshipTimeout = value;
			}
		}

		// Token: 0x06004CB6 RID: 19638 RVA: 0x0010CE8A File Offset: 0x0010B08A
		internal static void TrackLifetime(ServerIdentity identity)
		{
			LifetimeServices._leaseManager.TrackLifetime(identity);
		}

		// Token: 0x06004CB7 RID: 19639 RVA: 0x0010CE97 File Offset: 0x0010B097
		internal static void StopTrackingLifetime(ServerIdentity identity)
		{
			LifetimeServices._leaseManager.StopTrackingLifetime(identity);
		}

		// Token: 0x0400284B RID: 10315
		private static TimeSpan _leaseManagerPollTime;

		// Token: 0x0400284C RID: 10316
		private static TimeSpan _leaseTime;

		// Token: 0x0400284D RID: 10317
		private static TimeSpan _renewOnCallTime;

		// Token: 0x0400284E RID: 10318
		private static TimeSpan _sponsorshipTimeout;

		// Token: 0x0400284F RID: 10319
		private static LeaseManager _leaseManager = new LeaseManager();
	}
}
