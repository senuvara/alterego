﻿using System;
using System.Collections;
using System.Threading;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x02000785 RID: 1925
	internal class LeaseManager
	{
		// Token: 0x06004CA0 RID: 19616 RVA: 0x0010CB48 File Offset: 0x0010AD48
		public void SetPollTime(TimeSpan timeSpan)
		{
			object syncRoot = this._objects.SyncRoot;
			lock (syncRoot)
			{
				if (this._timer != null)
				{
					this._timer.Change(timeSpan, timeSpan);
				}
			}
		}

		// Token: 0x06004CA1 RID: 19617 RVA: 0x0010CBA0 File Offset: 0x0010ADA0
		public void TrackLifetime(ServerIdentity identity)
		{
			object syncRoot = this._objects.SyncRoot;
			lock (syncRoot)
			{
				identity.Lease.Activate();
				this._objects.Add(identity);
				if (this._timer == null)
				{
					this.StartManager();
				}
			}
		}

		// Token: 0x06004CA2 RID: 19618 RVA: 0x0010CC08 File Offset: 0x0010AE08
		public void StopTrackingLifetime(ServerIdentity identity)
		{
			object syncRoot = this._objects.SyncRoot;
			lock (syncRoot)
			{
				this._objects.Remove(identity);
			}
		}

		// Token: 0x06004CA3 RID: 19619 RVA: 0x0010CC54 File Offset: 0x0010AE54
		public void StartManager()
		{
			this._timer = new Timer(new TimerCallback(this.ManageLeases), null, LifetimeServices.LeaseManagerPollTime, LifetimeServices.LeaseManagerPollTime);
		}

		// Token: 0x06004CA4 RID: 19620 RVA: 0x0010CC78 File Offset: 0x0010AE78
		public void StopManager()
		{
			Timer timer = this._timer;
			this._timer = null;
			if (timer != null)
			{
				timer.Dispose();
			}
		}

		// Token: 0x06004CA5 RID: 19621 RVA: 0x0010CC9C File Offset: 0x0010AE9C
		public void ManageLeases(object state)
		{
			object syncRoot = this._objects.SyncRoot;
			lock (syncRoot)
			{
				int i = 0;
				while (i < this._objects.Count)
				{
					ServerIdentity serverIdentity = (ServerIdentity)this._objects[i];
					serverIdentity.Lease.UpdateState();
					if (serverIdentity.Lease.CurrentState == LeaseState.Expired)
					{
						this._objects.RemoveAt(i);
						serverIdentity.OnLifetimeExpired();
					}
					else
					{
						i++;
					}
				}
				if (this._objects.Count == 0)
				{
					this.StopManager();
				}
			}
		}

		// Token: 0x06004CA6 RID: 19622 RVA: 0x0010CD44 File Offset: 0x0010AF44
		public LeaseManager()
		{
		}

		// Token: 0x04002842 RID: 10306
		private ArrayList _objects = new ArrayList();

		// Token: 0x04002843 RID: 10307
		private Timer _timer;
	}
}
