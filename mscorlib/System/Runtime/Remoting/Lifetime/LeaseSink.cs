﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x02000786 RID: 1926
	internal class LeaseSink : IMessageSink
	{
		// Token: 0x06004CA7 RID: 19623 RVA: 0x0010CD57 File Offset: 0x0010AF57
		public LeaseSink(IMessageSink nextSink)
		{
			this._nextSink = nextSink;
		}

		// Token: 0x06004CA8 RID: 19624 RVA: 0x0010CD66 File Offset: 0x0010AF66
		public IMessage SyncProcessMessage(IMessage msg)
		{
			this.RenewLease(msg);
			return this._nextSink.SyncProcessMessage(msg);
		}

		// Token: 0x06004CA9 RID: 19625 RVA: 0x0010CD7B File Offset: 0x0010AF7B
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			this.RenewLease(msg);
			return this._nextSink.AsyncProcessMessage(msg, replySink);
		}

		// Token: 0x06004CAA RID: 19626 RVA: 0x0010CD94 File Offset: 0x0010AF94
		private void RenewLease(IMessage msg)
		{
			ILease lease = ((ServerIdentity)RemotingServices.GetMessageTargetIdentity(msg)).Lease;
			if (lease != null && lease.CurrentLeaseTime < lease.RenewOnCallTime)
			{
				lease.Renew(lease.RenewOnCallTime);
			}
		}

		// Token: 0x17000CB8 RID: 3256
		// (get) Token: 0x06004CAB RID: 19627 RVA: 0x0010CDD5 File Offset: 0x0010AFD5
		public IMessageSink NextSink
		{
			get
			{
				return this._nextSink;
			}
		}

		// Token: 0x04002844 RID: 10308
		private IMessageSink _nextSink;
	}
}
