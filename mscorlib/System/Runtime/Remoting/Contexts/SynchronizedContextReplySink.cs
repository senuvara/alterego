﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200079F RID: 1951
	internal class SynchronizedContextReplySink : IMessageSink
	{
		// Token: 0x06004D26 RID: 19750 RVA: 0x0010DEF0 File Offset: 0x0010C0F0
		public SynchronizedContextReplySink(IMessageSink next, SynchronizationAttribute att, bool newLock)
		{
			this._newLock = newLock;
			this._next = next;
			this._att = att;
		}

		// Token: 0x17000CD2 RID: 3282
		// (get) Token: 0x06004D27 RID: 19751 RVA: 0x0010DF0D File Offset: 0x0010C10D
		public IMessageSink NextSink
		{
			get
			{
				return this._next;
			}
		}

		// Token: 0x06004D28 RID: 19752 RVA: 0x000175EA File Offset: 0x000157EA
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004D29 RID: 19753 RVA: 0x0010DF18 File Offset: 0x0010C118
		public IMessage SyncProcessMessage(IMessage msg)
		{
			if (this._newLock)
			{
				this._att.AcquireLock();
			}
			else
			{
				this._att.ReleaseLock();
			}
			IMessage result;
			try
			{
				result = this._next.SyncProcessMessage(msg);
			}
			finally
			{
				if (this._newLock)
				{
					this._att.ReleaseLock();
				}
			}
			return result;
		}

		// Token: 0x04002875 RID: 10357
		private IMessageSink _next;

		// Token: 0x04002876 RID: 10358
		private bool _newLock;

		// Token: 0x04002877 RID: 10359
		private SynchronizationAttribute _att;
	}
}
