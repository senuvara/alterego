﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Runtime.Remoting.Contexts
{
	/// <summary>Holds the name/value pair of the property name and the object representing the property of a context.</summary>
	// Token: 0x0200078E RID: 1934
	[ComVisible(true)]
	public class ContextProperty
	{
		// Token: 0x06004CED RID: 19693 RVA: 0x0010D89D File Offset: 0x0010BA9D
		private ContextProperty(string name, object prop)
		{
			this.name = name;
			this.prop = prop;
		}

		/// <summary>Gets the name of the T:System.Runtime.Remoting.Contexts.ContextProperty class.</summary>
		/// <returns>The name of the <see cref="T:System.Runtime.Remoting.Contexts.ContextProperty" /> class.</returns>
		// Token: 0x17000CC8 RID: 3272
		// (get) Token: 0x06004CEE RID: 19694 RVA: 0x0010D8B3 File Offset: 0x0010BAB3
		public virtual string Name
		{
			get
			{
				return this.name;
			}
		}

		/// <summary>Gets the object representing the property of a context.</summary>
		/// <returns>The object representing the property of a context.</returns>
		// Token: 0x17000CC9 RID: 3273
		// (get) Token: 0x06004CEF RID: 19695 RVA: 0x0010D8BB File Offset: 0x0010BABB
		public virtual object Property
		{
			get
			{
				return this.prop;
			}
		}

		// Token: 0x06004CF0 RID: 19696 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal ContextProperty()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04002863 RID: 10339
		private string name;

		// Token: 0x04002864 RID: 10340
		private object prop;
	}
}
