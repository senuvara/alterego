﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200078F RID: 1935
	internal class CrossContextChannel : IMessageSink
	{
		// Token: 0x06004CF1 RID: 19697 RVA: 0x0010D8C4 File Offset: 0x0010BAC4
		public IMessage SyncProcessMessage(IMessage msg)
		{
			ServerIdentity serverIdentity = (ServerIdentity)RemotingServices.GetMessageTargetIdentity(msg);
			Context context = null;
			if (Thread.CurrentContext != serverIdentity.Context)
			{
				context = Context.SwitchToContext(serverIdentity.Context);
			}
			IMessage result;
			try
			{
				Context.NotifyGlobalDynamicSinks(true, msg, false, false);
				Thread.CurrentContext.NotifyDynamicSinks(true, msg, false, false);
				result = serverIdentity.Context.GetServerContextSinkChain().SyncProcessMessage(msg);
				Context.NotifyGlobalDynamicSinks(false, msg, false, false);
				Thread.CurrentContext.NotifyDynamicSinks(false, msg, false, false);
			}
			catch (Exception e)
			{
				result = new ReturnMessage(e, (IMethodCallMessage)msg);
			}
			finally
			{
				if (context != null)
				{
					Context.SwitchToContext(context);
				}
			}
			return result;
		}

		// Token: 0x06004CF2 RID: 19698 RVA: 0x0010D974 File Offset: 0x0010BB74
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			ServerIdentity serverIdentity = (ServerIdentity)RemotingServices.GetMessageTargetIdentity(msg);
			Context context = null;
			if (Thread.CurrentContext != serverIdentity.Context)
			{
				context = Context.SwitchToContext(serverIdentity.Context);
			}
			IMessageCtrl result;
			try
			{
				Context.NotifyGlobalDynamicSinks(true, msg, false, true);
				Thread.CurrentContext.NotifyDynamicSinks(true, msg, false, false);
				if (replySink != null)
				{
					replySink = new CrossContextChannel.ContextRestoreSink(replySink, context, msg);
				}
				IMessageCtrl messageCtrl = serverIdentity.AsyncObjectProcessMessage(msg, replySink);
				if (replySink == null)
				{
					Context.NotifyGlobalDynamicSinks(false, msg, false, false);
					Thread.CurrentContext.NotifyDynamicSinks(false, msg, false, false);
				}
				result = messageCtrl;
			}
			catch (Exception e)
			{
				if (replySink != null)
				{
					replySink.SyncProcessMessage(new ReturnMessage(e, (IMethodCallMessage)msg));
				}
				result = null;
			}
			finally
			{
				if (context != null)
				{
					Context.SwitchToContext(context);
				}
			}
			return result;
		}

		// Token: 0x17000CCA RID: 3274
		// (get) Token: 0x06004CF3 RID: 19699 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public IMessageSink NextSink
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06004CF4 RID: 19700 RVA: 0x00002050 File Offset: 0x00000250
		public CrossContextChannel()
		{
		}

		// Token: 0x02000790 RID: 1936
		private class ContextRestoreSink : IMessageSink
		{
			// Token: 0x06004CF5 RID: 19701 RVA: 0x0010DA38 File Offset: 0x0010BC38
			public ContextRestoreSink(IMessageSink next, Context context, IMessage call)
			{
				this._next = next;
				this._context = context;
				this._call = call;
			}

			// Token: 0x06004CF6 RID: 19702 RVA: 0x0010DA58 File Offset: 0x0010BC58
			public IMessage SyncProcessMessage(IMessage msg)
			{
				IMessage result;
				try
				{
					Context.NotifyGlobalDynamicSinks(false, msg, false, false);
					Thread.CurrentContext.NotifyDynamicSinks(false, msg, false, false);
					result = this._next.SyncProcessMessage(msg);
				}
				catch (Exception e)
				{
					result = new ReturnMessage(e, (IMethodCallMessage)this._call);
				}
				finally
				{
					if (this._context != null)
					{
						Context.SwitchToContext(this._context);
					}
				}
				return result;
			}

			// Token: 0x06004CF7 RID: 19703 RVA: 0x000175EA File Offset: 0x000157EA
			public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000CCB RID: 3275
			// (get) Token: 0x06004CF8 RID: 19704 RVA: 0x0010DAD0 File Offset: 0x0010BCD0
			public IMessageSink NextSink
			{
				get
				{
					return this._next;
				}
			}

			// Token: 0x04002865 RID: 10341
			private IMessageSink _next;

			// Token: 0x04002866 RID: 10342
			private Context _context;

			// Token: 0x04002867 RID: 10343
			private IMessage _call;
		}
	}
}
