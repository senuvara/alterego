﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Contexts
{
	/// <summary>Represents the method that will handle the requests of execution of some code in another context.</summary>
	// Token: 0x02000791 RID: 1937
	// (Invoke) Token: 0x06004CFA RID: 19706
	[ComVisible(true)]
	public delegate void CrossContextDelegate();
}
