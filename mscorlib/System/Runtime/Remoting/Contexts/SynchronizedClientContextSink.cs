﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200079D RID: 1949
	internal class SynchronizedClientContextSink : IMessageSink
	{
		// Token: 0x06004D1E RID: 19742 RVA: 0x0010DDAE File Offset: 0x0010BFAE
		public SynchronizedClientContextSink(IMessageSink next, SynchronizationAttribute att)
		{
			this._att = att;
			this._next = next;
		}

		// Token: 0x17000CD0 RID: 3280
		// (get) Token: 0x06004D1F RID: 19743 RVA: 0x0010DDC4 File Offset: 0x0010BFC4
		public IMessageSink NextSink
		{
			get
			{
				return this._next;
			}
		}

		// Token: 0x06004D20 RID: 19744 RVA: 0x0010DDCC File Offset: 0x0010BFCC
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			if (this._att.IsReEntrant)
			{
				this._att.ReleaseLock();
				replySink = new SynchronizedContextReplySink(replySink, this._att, true);
			}
			return this._next.AsyncProcessMessage(msg, replySink);
		}

		// Token: 0x06004D21 RID: 19745 RVA: 0x0010DE04 File Offset: 0x0010C004
		public IMessage SyncProcessMessage(IMessage msg)
		{
			if (this._att.IsReEntrant)
			{
				this._att.ReleaseLock();
			}
			IMessage result;
			try
			{
				result = this._next.SyncProcessMessage(msg);
			}
			finally
			{
				if (this._att.IsReEntrant)
				{
					this._att.AcquireLock();
				}
			}
			return result;
		}

		// Token: 0x04002871 RID: 10353
		private IMessageSink _next;

		// Token: 0x04002872 RID: 10354
		private SynchronizationAttribute _att;
	}
}
