﻿using System;
using System.Collections;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200078A RID: 1930
	internal class DynamicPropertyCollection
	{
		// Token: 0x17000CC6 RID: 3270
		// (get) Token: 0x06004CDC RID: 19676 RVA: 0x0010D59C File Offset: 0x0010B79C
		public bool HasProperties
		{
			get
			{
				return this._properties.Count > 0;
			}
		}

		// Token: 0x06004CDD RID: 19677 RVA: 0x0010D5AC File Offset: 0x0010B7AC
		public bool RegisterDynamicProperty(IDynamicProperty prop)
		{
			bool result;
			lock (this)
			{
				if (this.FindProperty(prop.Name) != -1)
				{
					throw new InvalidOperationException("Another property by this name already exists");
				}
				ArrayList arrayList = new ArrayList(this._properties);
				DynamicPropertyCollection.DynamicPropertyReg dynamicPropertyReg = new DynamicPropertyCollection.DynamicPropertyReg();
				dynamicPropertyReg.Property = prop;
				IContributeDynamicSink contributeDynamicSink = prop as IContributeDynamicSink;
				if (contributeDynamicSink != null)
				{
					dynamicPropertyReg.Sink = contributeDynamicSink.GetDynamicSink();
				}
				arrayList.Add(dynamicPropertyReg);
				this._properties = arrayList;
				result = true;
			}
			return result;
		}

		// Token: 0x06004CDE RID: 19678 RVA: 0x0010D644 File Offset: 0x0010B844
		public bool UnregisterDynamicProperty(string name)
		{
			bool result;
			lock (this)
			{
				int num = this.FindProperty(name);
				if (num == -1)
				{
					throw new RemotingException("A property with the name " + name + " was not found");
				}
				this._properties.RemoveAt(num);
				result = true;
			}
			return result;
		}

		// Token: 0x06004CDF RID: 19679 RVA: 0x0010D6AC File Offset: 0x0010B8AC
		public void NotifyMessage(bool start, IMessage msg, bool client_site, bool async)
		{
			ArrayList properties = this._properties;
			if (start)
			{
				using (IEnumerator enumerator = properties.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						DynamicPropertyCollection.DynamicPropertyReg dynamicPropertyReg = (DynamicPropertyCollection.DynamicPropertyReg)obj;
						if (dynamicPropertyReg.Sink != null)
						{
							dynamicPropertyReg.Sink.ProcessMessageStart(msg, client_site, async);
						}
					}
					return;
				}
			}
			foreach (object obj2 in properties)
			{
				DynamicPropertyCollection.DynamicPropertyReg dynamicPropertyReg2 = (DynamicPropertyCollection.DynamicPropertyReg)obj2;
				if (dynamicPropertyReg2.Sink != null)
				{
					dynamicPropertyReg2.Sink.ProcessMessageFinish(msg, client_site, async);
				}
			}
		}

		// Token: 0x06004CE0 RID: 19680 RVA: 0x0010D770 File Offset: 0x0010B970
		private int FindProperty(string name)
		{
			for (int i = 0; i < this._properties.Count; i++)
			{
				if (((DynamicPropertyCollection.DynamicPropertyReg)this._properties[i]).Property.Name == name)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06004CE1 RID: 19681 RVA: 0x0010D7B9 File Offset: 0x0010B9B9
		public DynamicPropertyCollection()
		{
		}

		// Token: 0x0400285F RID: 10335
		private ArrayList _properties = new ArrayList();

		// Token: 0x0200078B RID: 1931
		private class DynamicPropertyReg
		{
			// Token: 0x06004CE2 RID: 19682 RVA: 0x00002050 File Offset: 0x00000250
			public DynamicPropertyReg()
			{
			}

			// Token: 0x04002860 RID: 10336
			public IDynamicProperty Property;

			// Token: 0x04002861 RID: 10337
			public IDynamicMessageSink Sink;
		}
	}
}
