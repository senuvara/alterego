﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200079E RID: 1950
	internal class SynchronizedServerContextSink : IMessageSink
	{
		// Token: 0x06004D22 RID: 19746 RVA: 0x0010DE64 File Offset: 0x0010C064
		public SynchronizedServerContextSink(IMessageSink next, SynchronizationAttribute att)
		{
			this._att = att;
			this._next = next;
		}

		// Token: 0x17000CD1 RID: 3281
		// (get) Token: 0x06004D23 RID: 19747 RVA: 0x0010DE7A File Offset: 0x0010C07A
		public IMessageSink NextSink
		{
			get
			{
				return this._next;
			}
		}

		// Token: 0x06004D24 RID: 19748 RVA: 0x0010DE82 File Offset: 0x0010C082
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			this._att.AcquireLock();
			replySink = new SynchronizedContextReplySink(replySink, this._att, false);
			return this._next.AsyncProcessMessage(msg, replySink);
		}

		// Token: 0x06004D25 RID: 19749 RVA: 0x0010DEAC File Offset: 0x0010C0AC
		public IMessage SyncProcessMessage(IMessage msg)
		{
			this._att.AcquireLock();
			IMessage result;
			try
			{
				result = this._next.SyncProcessMessage(msg);
			}
			finally
			{
				this._att.ReleaseLock();
			}
			return result;
		}

		// Token: 0x04002873 RID: 10355
		private IMessageSink _next;

		// Token: 0x04002874 RID: 10356
		private SynchronizationAttribute _att;
	}
}
