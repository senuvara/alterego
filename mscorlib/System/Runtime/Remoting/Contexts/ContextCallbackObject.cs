﻿using System;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200078C RID: 1932
	internal class ContextCallbackObject : ContextBoundObject
	{
		// Token: 0x06004CE3 RID: 19683 RVA: 0x000020D3 File Offset: 0x000002D3
		public void DoCallBack(CrossContextDelegate deleg)
		{
		}

		// Token: 0x06004CE4 RID: 19684 RVA: 0x0010D7CC File Offset: 0x0010B9CC
		public ContextCallbackObject()
		{
		}
	}
}
