﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Messaging;
using System.Security;
using System.Threading;

namespace System.Runtime.Remoting.Contexts
{
	/// <summary>Enforces a synchronization domain for the current context and all contexts that share the same instance.</summary>
	// Token: 0x0200079C RID: 1948
	[AttributeUsage(AttributeTargets.Class)]
	[ComVisible(true)]
	[Serializable]
	public class SynchronizationAttribute : ContextAttribute, IContributeClientContextSink, IContributeServerContextSink
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Contexts.SynchronizationAttribute" /> class with default values.</summary>
		// Token: 0x06004D0F RID: 19727 RVA: 0x0010DAD8 File Offset: 0x0010BCD8
		public SynchronizationAttribute() : this(8, false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Contexts.SynchronizationAttribute" /> class with a Boolean value indicating whether reentry is required.</summary>
		/// <param name="reEntrant">A Boolean value indicating whether reentry is required. </param>
		// Token: 0x06004D10 RID: 19728 RVA: 0x0010DAE2 File Offset: 0x0010BCE2
		public SynchronizationAttribute(bool reEntrant) : this(8, reEntrant)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Contexts.SynchronizationAttribute" /> class with a flag indicating the behavior of the object to which this attribute is applied.</summary>
		/// <param name="flag">An integer value indicating the behavior of the object to which this attribute is applied. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="flag" /> parameter was not one of the defined flags. </exception>
		// Token: 0x06004D11 RID: 19729 RVA: 0x0010DAEC File Offset: 0x0010BCEC
		public SynchronizationAttribute(int flag) : this(flag, false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.Remoting.Contexts.SynchronizationAttribute" /> class with a flag indicating the behavior of the object to which this attribute is applied, and a Boolean value indicating whether reentry is required.</summary>
		/// <param name="flag">An integer value indicating the behavior of the object to which this attribute is applied. </param>
		/// <param name="reEntrant">
		///       <see langword="true" /> if reentry is required, and callouts must be intercepted and serialized; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="flag" /> parameter was not one of the defined flags. </exception>
		// Token: 0x06004D12 RID: 19730 RVA: 0x0010DAF8 File Offset: 0x0010BCF8
		public SynchronizationAttribute(int flag, bool reEntrant) : base("Synchronization")
		{
			if (flag != 1 && flag != 4 && flag != 8 && flag != 2)
			{
				throw new ArgumentException("flag");
			}
			this._bReEntrant = reEntrant;
			this._flavor = flag;
		}

		/// <summary>Gets or sets a Boolean value indicating whether reentry is required.</summary>
		/// <returns>A Boolean value indicating whether reentry is required.</returns>
		// Token: 0x17000CCE RID: 3278
		// (get) Token: 0x06004D13 RID: 19731 RVA: 0x0010DB45 File Offset: 0x0010BD45
		public virtual bool IsReEntrant
		{
			get
			{
				return this._bReEntrant;
			}
		}

		/// <summary>Gets or sets a Boolean value indicating whether the <see cref="T:System.Runtime.Remoting.Contexts.Context" /> implementing this instance of <see cref="T:System.Runtime.Remoting.Contexts.SynchronizationAttribute" /> is locked.</summary>
		/// <returns>A Boolean value indicating whether the <see cref="T:System.Runtime.Remoting.Contexts.Context" /> implementing this instance of <see cref="T:System.Runtime.Remoting.Contexts.SynchronizationAttribute" /> is locked.</returns>
		// Token: 0x17000CCF RID: 3279
		// (get) Token: 0x06004D14 RID: 19732 RVA: 0x0010DB4D File Offset: 0x0010BD4D
		// (set) Token: 0x06004D15 RID: 19733 RVA: 0x0010DB58 File Offset: 0x0010BD58
		public virtual bool Locked
		{
			get
			{
				return this._lockCount > 0;
			}
			set
			{
				SynchronizationAttribute obj;
				if (value)
				{
					this.AcquireLock();
					obj = this;
					lock (obj)
					{
						if (this._lockCount > 1)
						{
							this.ReleaseLock();
						}
						return;
					}
				}
				obj = this;
				lock (obj)
				{
					while (this._lockCount > 0 && this._ownerThread == Thread.CurrentThread)
					{
						this.ReleaseLock();
					}
				}
			}
		}

		// Token: 0x06004D16 RID: 19734 RVA: 0x0010DBE8 File Offset: 0x0010BDE8
		internal void AcquireLock()
		{
			this._mutex.WaitOne();
			lock (this)
			{
				this._ownerThread = Thread.CurrentThread;
				this._lockCount++;
			}
		}

		// Token: 0x06004D17 RID: 19735 RVA: 0x0010DC44 File Offset: 0x0010BE44
		internal void ReleaseLock()
		{
			lock (this)
			{
				if (this._lockCount > 0 && this._ownerThread == Thread.CurrentThread)
				{
					this._lockCount--;
					this._mutex.ReleaseMutex();
					if (this._lockCount == 0)
					{
						this._ownerThread = null;
					}
				}
			}
		}

		/// <summary>Adds the <see langword="Synchronized" /> context property to the specified <see cref="T:System.Runtime.Remoting.Activation.IConstructionCallMessage" />.</summary>
		/// <param name="ctorMsg">The <see cref="T:System.Runtime.Remoting.Activation.IConstructionCallMessage" /> to which to add the property. </param>
		// Token: 0x06004D18 RID: 19736 RVA: 0x0010DCB8 File Offset: 0x0010BEB8
		[ComVisible(true)]
		[SecurityCritical]
		public override void GetPropertiesForNewContext(IConstructionCallMessage ctorMsg)
		{
			if (this._flavor != 1)
			{
				ctorMsg.ContextProperties.Add(this);
			}
		}

		/// <summary>Creates a CallOut sink and chains it in front of the provided chain of sinks at the context boundary on the client end of a remoting call.</summary>
		/// <param name="nextSink">The chain of sinks composed so far. </param>
		/// <returns>The composite sink chain with the new CallOut sink.</returns>
		// Token: 0x06004D19 RID: 19737 RVA: 0x0010DCD0 File Offset: 0x0010BED0
		[SecurityCritical]
		public virtual IMessageSink GetClientContextSink(IMessageSink nextSink)
		{
			return new SynchronizedClientContextSink(nextSink, this);
		}

		/// <summary>Creates a synchronized dispatch sink and chains it in front of the provided chain of sinks at the context boundary on the server end of a remoting call.</summary>
		/// <param name="nextSink">The chain of sinks composed so far. </param>
		/// <returns>The composite sink chain with the new synchronized dispatch sink.</returns>
		// Token: 0x06004D1A RID: 19738 RVA: 0x0010DCD9 File Offset: 0x0010BED9
		[SecurityCritical]
		public virtual IMessageSink GetServerContextSink(IMessageSink nextSink)
		{
			return new SynchronizedServerContextSink(nextSink, this);
		}

		/// <summary>Returns a Boolean value indicating whether the context parameter meets the context attribute's requirements.</summary>
		/// <param name="ctx">The context to check. </param>
		/// <param name="msg">Information gathered at construction time of the context bound object marked by this attribute. The <see cref="T:System.Runtime.Remoting.Contexts.SynchronizationAttribute" /> can inspect, add to, and remove properties from the context while determining if the context is acceptable to it. </param>
		/// <returns>
		///     <see langword="true" /> if the passed in context is OK; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="ctx" /> or <paramref name="msg" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06004D1B RID: 19739 RVA: 0x0010DCE4 File Offset: 0x0010BEE4
		[SecurityCritical]
		[ComVisible(true)]
		public override bool IsContextOK(Context ctx, IConstructionCallMessage msg)
		{
			SynchronizationAttribute synchronizationAttribute = ctx.GetProperty("Synchronization") as SynchronizationAttribute;
			int flavor = this._flavor;
			switch (flavor)
			{
			case 1:
				return synchronizationAttribute == null;
			case 2:
				return true;
			case 3:
				break;
			case 4:
				return synchronizationAttribute != null;
			default:
				if (flavor == 8)
				{
					return false;
				}
				break;
			}
			return false;
		}

		// Token: 0x06004D1C RID: 19740 RVA: 0x0010DD38 File Offset: 0x0010BF38
		internal static void ExitContext()
		{
			if (Thread.CurrentContext.IsDefaultContext)
			{
				return;
			}
			SynchronizationAttribute synchronizationAttribute = Thread.CurrentContext.GetProperty("Synchronization") as SynchronizationAttribute;
			if (synchronizationAttribute == null)
			{
				return;
			}
			synchronizationAttribute.Locked = false;
		}

		// Token: 0x06004D1D RID: 19741 RVA: 0x0010DD74 File Offset: 0x0010BF74
		internal static void EnterContext()
		{
			if (Thread.CurrentContext.IsDefaultContext)
			{
				return;
			}
			SynchronizationAttribute synchronizationAttribute = Thread.CurrentContext.GetProperty("Synchronization") as SynchronizationAttribute;
			if (synchronizationAttribute == null)
			{
				return;
			}
			synchronizationAttribute.Locked = true;
		}

		/// <summary>Indicates that the class to which this attribute is applied cannot be created in a context that has synchronization. This field is constant.</summary>
		// Token: 0x04002868 RID: 10344
		public const int NOT_SUPPORTED = 1;

		/// <summary>Indicates that the class to which this attribute is applied is not dependent on whether the context has synchronization. This field is constant.</summary>
		// Token: 0x04002869 RID: 10345
		public const int SUPPORTED = 2;

		/// <summary>Indicates that the class to which this attribute is applied must be created in a context that has synchronization. This field is constant.</summary>
		// Token: 0x0400286A RID: 10346
		public const int REQUIRED = 4;

		/// <summary>Indicates that the class to which this attribute is applied must be created in a context with a new instance of the synchronization property each time. This field is constant.</summary>
		// Token: 0x0400286B RID: 10347
		public const int REQUIRES_NEW = 8;

		// Token: 0x0400286C RID: 10348
		private bool _bReEntrant;

		// Token: 0x0400286D RID: 10349
		private int _flavor;

		// Token: 0x0400286E RID: 10350
		[NonSerialized]
		private int _lockCount;

		// Token: 0x0400286F RID: 10351
		[NonSerialized]
		private Mutex _mutex = new Mutex(false);

		// Token: 0x04002870 RID: 10352
		[NonSerialized]
		private Thread _ownerThread;
	}
}
