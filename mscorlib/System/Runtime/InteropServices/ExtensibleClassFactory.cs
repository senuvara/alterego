﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	/// <summary>Enables customization of managed objects that extend from unmanaged objects during creation.</summary>
	// Token: 0x02000920 RID: 2336
	[ComVisible(true)]
	public sealed class ExtensibleClassFactory
	{
		// Token: 0x06005395 RID: 21397 RVA: 0x00002050 File Offset: 0x00000250
		private ExtensibleClassFactory()
		{
		}

		// Token: 0x06005396 RID: 21398 RVA: 0x0011976F File Offset: 0x0011796F
		internal static ObjectCreationDelegate GetObjectCreationCallback(Type t)
		{
			return ExtensibleClassFactory.hashtable[t] as ObjectCreationDelegate;
		}

		/// <summary>Registers a <see langword="delegate" /> that is called when an instance of a managed type, that extends from an unmanaged type, needs to allocate the aggregated unmanaged object.</summary>
		/// <param name="callback">A <see langword="delegate" /> that is called in place of <see langword="CoCreateInstance" />. </param>
		// Token: 0x06005397 RID: 21399 RVA: 0x00119784 File Offset: 0x00117984
		public static void RegisterObjectCreationCallback(ObjectCreationDelegate callback)
		{
			int i = 1;
			StackTrace stackTrace = new StackTrace(false);
			while (i < stackTrace.FrameCount)
			{
				MethodBase method = stackTrace.GetFrame(i).GetMethod();
				if (method.MemberType == MemberTypes.Constructor && method.IsStatic)
				{
					ExtensibleClassFactory.hashtable.Add(method.DeclaringType, callback);
					return;
				}
				i++;
			}
			throw new InvalidOperationException("RegisterObjectCreationCallback must be called from .cctor of class derived from ComImport type.");
		}

		// Token: 0x06005398 RID: 21400 RVA: 0x001197E3 File Offset: 0x001179E3
		// Note: this type is marked as 'beforefieldinit'.
		static ExtensibleClassFactory()
		{
		}

		// Token: 0x04002BD5 RID: 11221
		private static readonly Hashtable hashtable = new Hashtable();
	}
}
