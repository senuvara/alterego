﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	/// <summary>Provides data for the <see cref="E:System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMetadata.DesignerNamespaceResolve" /> event. </summary>
	// Token: 0x02000956 RID: 2390
	[ComVisible(false)]
	public class DesignerNamespaceResolveEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.WindowsRuntime.DesignerNamespaceResolveEventArgs" /> class. </summary>
		/// <param name="namespaceName">The name of the namespace to resolve. </param>
		// Token: 0x0600568A RID: 22154 RVA: 0x0011C18C File Offset: 0x0011A38C
		public DesignerNamespaceResolveEventArgs(string namespaceName)
		{
			this.NamespaceName = namespaceName;
			this.ResolvedAssemblyFiles = new Collection<string>();
		}

		/// <summary>
		///     Gets the name of the namespace to resolve. </summary>
		/// <returns>The name of the namespace to resolve. </returns>
		// Token: 0x17000F4C RID: 3916
		// (get) Token: 0x0600568B RID: 22155 RVA: 0x0011C1A6 File Offset: 0x0011A3A6
		// (set) Token: 0x0600568C RID: 22156 RVA: 0x0011C1AE File Offset: 0x0011A3AE
		public string NamespaceName
		{
			[CompilerGenerated]
			get
			{
				return this.<NamespaceName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<NamespaceName>k__BackingField = value;
			}
		}

		/// <summary>
		///     Gets a collection of assembly file paths; when the event handler for the <see cref="E:System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMetadata.DesignerNamespaceResolve" /> event is invoked, the collection is empty, and the event handler is responsible for adding the necessary assembly files. </summary>
		/// <returns>A collection of assembly files that define the requested namespace. </returns>
		// Token: 0x17000F4D RID: 3917
		// (get) Token: 0x0600568D RID: 22157 RVA: 0x0011C1B7 File Offset: 0x0011A3B7
		// (set) Token: 0x0600568E RID: 22158 RVA: 0x0011C1BF File Offset: 0x0011A3BF
		public Collection<string> ResolvedAssemblyFiles
		{
			[CompilerGenerated]
			get
			{
				return this.<ResolvedAssemblyFiles>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ResolvedAssemblyFiles>k__BackingField = value;
			}
		}

		// Token: 0x04002C43 RID: 11331
		[CompilerGenerated]
		private string <NamespaceName>k__BackingField;

		// Token: 0x04002C44 RID: 11332
		[CompilerGenerated]
		private Collection<string> <ResolvedAssemblyFiles>k__BackingField;
	}
}
