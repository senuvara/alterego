﻿using System;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	/// <summary>Specifies the name of the return value of a method in a Windows Runtime component.</summary>
	// Token: 0x02000946 RID: 2374
	[AttributeUsage(AttributeTargets.Delegate | AttributeTargets.ReturnValue, AllowMultiple = false, Inherited = false)]
	public sealed class ReturnValueNameAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.WindowsRuntime.ReturnValueNameAttribute" /> class, and specifies the name of the return value.</summary>
		/// <param name="name">The name of the return value. </param>
		// Token: 0x06005638 RID: 22072 RVA: 0x0011ADEB File Offset: 0x00118FEB
		public ReturnValueNameAttribute(string name)
		{
			this.m_Name = name;
		}

		/// <summary>Gets the name that was specified for the return value of a method in a Windows Runtime component.</summary>
		/// <returns>The name of the method's return value. </returns>
		// Token: 0x17000F48 RID: 3912
		// (get) Token: 0x06005639 RID: 22073 RVA: 0x0011ADFA File Offset: 0x00118FFA
		public string Name
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x04002C2A RID: 11306
		private string m_Name;
	}
}
