﻿using System;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	/// <summary>Specifies the default interface of a managed Windows Runtime class.</summary>
	// Token: 0x02000941 RID: 2369
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
	public sealed class DefaultInterfaceAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.WindowsRuntime.DefaultInterfaceAttribute" /> class. </summary>
		/// <param name="defaultInterface">The interface type that is specified as the default interface for the class the attribute is applied to. </param>
		// Token: 0x0600562D RID: 22061 RVA: 0x0011AD7F File Offset: 0x00118F7F
		public DefaultInterfaceAttribute(Type defaultInterface)
		{
			this.m_defaultInterface = defaultInterface;
		}

		/// <summary>Gets the type of the default interface. </summary>
		/// <returns>The type of the default interface. </returns>
		// Token: 0x17000F42 RID: 3906
		// (get) Token: 0x0600562E RID: 22062 RVA: 0x0011AD8E File Offset: 0x00118F8E
		public Type DefaultInterface
		{
			get
			{
				return this.m_defaultInterface;
			}
		}

		// Token: 0x04002C24 RID: 11300
		private Type m_defaultInterface;
	}
}
