﻿using System;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	/// <summary>Specifies the version of the target type that first implemented the specified interface.</summary>
	// Token: 0x02000943 RID: 2371
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = false, AllowMultiple = true)]
	public sealed class InterfaceImplementedInVersionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.WindowsRuntime.InterfaceImplementedInVersionAttribute" /> class, specifying the interface that the target type implements and the version in which that interface was first implemented. </summary>
		/// <param name="interfaceType">The interface that was first implemented in the specified version of the target type. </param>
		/// <param name="majorVersion">The major component of the version of the target type that first implemented <paramref name="interfaceType" />.</param>
		/// <param name="minorVersion">The minor component of the version of the target type that first implemented <paramref name="interfaceType" />.</param>
		/// <param name="buildVersion">The build component of the version of the target type that first implemented <paramref name="interfaceType" />.</param>
		/// <param name="revisionVersion">The revision component of the version of the target type that first implemented <paramref name="interfaceType" />.</param>
		// Token: 0x06005630 RID: 22064 RVA: 0x0011AD96 File Offset: 0x00118F96
		public InterfaceImplementedInVersionAttribute(Type interfaceType, byte majorVersion, byte minorVersion, byte buildVersion, byte revisionVersion)
		{
			this.m_interfaceType = interfaceType;
			this.m_majorVersion = majorVersion;
			this.m_minorVersion = minorVersion;
			this.m_buildVersion = buildVersion;
			this.m_revisionVersion = revisionVersion;
		}

		/// <summary>Gets the type of the interface that the target type implements. </summary>
		/// <returns>The type of the interface. </returns>
		// Token: 0x17000F43 RID: 3907
		// (get) Token: 0x06005631 RID: 22065 RVA: 0x0011ADC3 File Offset: 0x00118FC3
		public Type InterfaceType
		{
			get
			{
				return this.m_interfaceType;
			}
		}

		/// <summary>Gets the major component of the version of the target type that first implemented the interface. </summary>
		/// <returns>The major component of the version.</returns>
		// Token: 0x17000F44 RID: 3908
		// (get) Token: 0x06005632 RID: 22066 RVA: 0x0011ADCB File Offset: 0x00118FCB
		public byte MajorVersion
		{
			get
			{
				return this.m_majorVersion;
			}
		}

		/// <summary>Gets the minor component of the version of the target type that first implemented the interface. </summary>
		/// <returns>The minor component of the version. </returns>
		// Token: 0x17000F45 RID: 3909
		// (get) Token: 0x06005633 RID: 22067 RVA: 0x0011ADD3 File Offset: 0x00118FD3
		public byte MinorVersion
		{
			get
			{
				return this.m_minorVersion;
			}
		}

		/// <summary>Gets the build component of the version of the target type that first implemented the interface. </summary>
		/// <returns>The build component of the version.</returns>
		// Token: 0x17000F46 RID: 3910
		// (get) Token: 0x06005634 RID: 22068 RVA: 0x0011ADDB File Offset: 0x00118FDB
		public byte BuildVersion
		{
			get
			{
				return this.m_buildVersion;
			}
		}

		/// <summary>Gets the revision component of the version of the target type that first implemented the interface. </summary>
		/// <returns>The revision component of the version.</returns>
		// Token: 0x17000F47 RID: 3911
		// (get) Token: 0x06005635 RID: 22069 RVA: 0x0011ADE3 File Offset: 0x00118FE3
		public byte RevisionVersion
		{
			get
			{
				return this.m_revisionVersion;
			}
		}

		// Token: 0x04002C25 RID: 11301
		private Type m_interfaceType;

		// Token: 0x04002C26 RID: 11302
		private byte m_majorVersion;

		// Token: 0x04002C27 RID: 11303
		private byte m_minorVersion;

		// Token: 0x04002C28 RID: 11304
		private byte m_buildVersion;

		// Token: 0x04002C29 RID: 11305
		private byte m_revisionVersion;
	}
}
