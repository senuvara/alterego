﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	// Token: 0x02000958 RID: 2392
	internal static class UnsafeNativeMethods
	{
		// Token: 0x06005696 RID: 22166
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern int WindowsCreateString(string sourceString, int length, IntPtr* hstring);

		// Token: 0x06005697 RID: 22167
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int WindowsDeleteString(IntPtr hstring);

		// Token: 0x06005698 RID: 22168
		[MethodImpl(MethodImplOptions.InternalCall)]
		public unsafe static extern char* WindowsGetStringRawBuffer(IntPtr hstring, uint* length);

		// Token: 0x06005699 RID: 22169
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool RoOriginateLanguageException(int error, string message, IntPtr languageException);

		// Token: 0x0600569A RID: 22170
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void RoReportUnhandledError(IRestrictedErrorInfo error);

		// Token: 0x0600569B RID: 22171
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IRestrictedErrorInfo GetRestrictedErrorInfo();
	}
}
