﻿using System;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	/// <summary>A token that is returned when an event handler is added to a Windows Runtime event. The token is used to remove the event handler from the event at a later time. </summary>
	// Token: 0x02000947 RID: 2375
	public struct EventRegistrationToken
	{
		// Token: 0x0600563A RID: 22074 RVA: 0x0011AE02 File Offset: 0x00119002
		internal EventRegistrationToken(ulong value)
		{
			this.m_value = value;
		}

		// Token: 0x17000F49 RID: 3913
		// (get) Token: 0x0600563B RID: 22075 RVA: 0x0011AE0B File Offset: 0x0011900B
		internal ulong Value
		{
			get
			{
				return this.m_value;
			}
		}

		/// <summary>Indicates whether two <see cref="T:System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken" /> instances are equal. </summary>
		/// <param name="left">The first instance to compare. </param>
		/// <param name="right">The second instance to compare. </param>
		/// <returns>
		///     <see langword="true" /> if the two objects are equal; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600563C RID: 22076 RVA: 0x0011AE13 File Offset: 0x00119013
		public static bool operator ==(EventRegistrationToken left, EventRegistrationToken right)
		{
			return left.Equals(right);
		}

		/// <summary>Indicates whether two <see cref="T:System.Runtime.InteropServices.WindowsRuntime.EventRegistrationToken" /> instances are not equal.</summary>
		/// <param name="left">The first instance to compare. </param>
		/// <param name="right">The second instance to compare. </param>
		/// <returns>
		///     <see langword="true" /> if the two instances are not equal; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600563D RID: 22077 RVA: 0x0011AE28 File Offset: 0x00119028
		public static bool operator !=(EventRegistrationToken left, EventRegistrationToken right)
		{
			return !left.Equals(right);
		}

		/// <summary>Returns a value that indicates whether the current object is equal to the specified object. </summary>
		/// <param name="obj">The object to compare.</param>
		/// <returns>
		///     <see langword="true" />  if the current object is equal to <paramref name="obj" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600563E RID: 22078 RVA: 0x0011AE40 File Offset: 0x00119040
		public override bool Equals(object obj)
		{
			return obj is EventRegistrationToken && ((EventRegistrationToken)obj).Value == this.Value;
		}

		/// <summary>Returns the hash code for this instance. </summary>
		/// <returns>The hash code for this instance. </returns>
		// Token: 0x0600563F RID: 22079 RVA: 0x0011AE6D File Offset: 0x0011906D
		public override int GetHashCode()
		{
			return this.m_value.GetHashCode();
		}

		// Token: 0x04002C2B RID: 11307
		internal ulong m_value;
	}
}
