﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	/// <summary>Provides data for the <see cref="E:System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMetadata.ReflectionOnlyNamespaceResolve" /> event.</summary>
	// Token: 0x02000957 RID: 2391
	[ComVisible(false)]
	public class NamespaceResolveEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.WindowsRuntime.NamespaceResolveEventArgs" /> class, specifying the namespace to resolve and the assembly whose dependency is being resolved. </summary>
		/// <param name="namespaceName">The namespace to resolve. </param>
		/// <param name="requestingAssembly">The assembly whose dependency is being resolved. </param>
		// Token: 0x0600568F RID: 22159 RVA: 0x0011C1C8 File Offset: 0x0011A3C8
		public NamespaceResolveEventArgs(string namespaceName, Assembly requestingAssembly)
		{
			this.NamespaceName = namespaceName;
			this.RequestingAssembly = requestingAssembly;
			this.ResolvedAssemblies = new Collection<Assembly>();
		}

		/// <summary>Gets the name of the namespace to resolve. </summary>
		/// <returns>The name of the namespace to resolve. </returns>
		// Token: 0x17000F4E RID: 3918
		// (get) Token: 0x06005690 RID: 22160 RVA: 0x0011C1E9 File Offset: 0x0011A3E9
		// (set) Token: 0x06005691 RID: 22161 RVA: 0x0011C1F1 File Offset: 0x0011A3F1
		public string NamespaceName
		{
			[CompilerGenerated]
			get
			{
				return this.<NamespaceName>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<NamespaceName>k__BackingField = value;
			}
		}

		/// <summary>Gets the name of the assembly whose dependency is being resolved. </summary>
		/// <returns>The name of the assembly whose dependency is being resolved. </returns>
		// Token: 0x17000F4F RID: 3919
		// (get) Token: 0x06005692 RID: 22162 RVA: 0x0011C1FA File Offset: 0x0011A3FA
		// (set) Token: 0x06005693 RID: 22163 RVA: 0x0011C202 File Offset: 0x0011A402
		public Assembly RequestingAssembly
		{
			[CompilerGenerated]
			get
			{
				return this.<RequestingAssembly>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<RequestingAssembly>k__BackingField = value;
			}
		}

		/// <summary>Gets a collection of assemblies; when the event handler for the <see cref="E:System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeMetadata.ReflectionOnlyNamespaceResolve" /> event is invoked, the collection is empty, and the event handler is responsible for adding the necessary assemblies. </summary>
		/// <returns>A collection of assemblies that define the requested namespace. </returns>
		// Token: 0x17000F50 RID: 3920
		// (get) Token: 0x06005694 RID: 22164 RVA: 0x0011C20B File Offset: 0x0011A40B
		// (set) Token: 0x06005695 RID: 22165 RVA: 0x0011C213 File Offset: 0x0011A413
		public Collection<Assembly> ResolvedAssemblies
		{
			[CompilerGenerated]
			get
			{
				return this.<ResolvedAssemblies>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<ResolvedAssemblies>k__BackingField = value;
			}
		}

		// Token: 0x04002C45 RID: 11333
		[CompilerGenerated]
		private string <NamespaceName>k__BackingField;

		// Token: 0x04002C46 RID: 11334
		[CompilerGenerated]
		private Assembly <RequestingAssembly>k__BackingField;

		// Token: 0x04002C47 RID: 11335
		[CompilerGenerated]
		private Collection<Assembly> <ResolvedAssemblies>k__BackingField;
	}
}
