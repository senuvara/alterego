﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	/// <summary>Provides an event for resolving reflection-only type requests for types that are provided by Windows Metadata files, and methods for performing the resolution. </summary>
	// Token: 0x02000959 RID: 2393
	[MonoTODO]
	public static class WindowsRuntimeMetadata
	{
		/// <summary>Locates the Windows Metadata files for the specified namespace, given the specified locations to search. </summary>
		/// <param name="namespaceName">The namespace to resolve. </param>
		/// <param name="packageGraphFilePaths">The application paths to search for Windows Metadata files, or <see langword="null" /> to search only for Windows Metadata files from the operating system installation. </param>
		/// <returns>An enumerable list of strings that represent the Windows Metadata files that define <paramref name="namespaceName" />. </returns>
		/// <exception cref="T:System.PlatformNotSupportedException">The operating system version does not support the Windows Runtime. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="namespaceName" /> is <see langword="null" />.</exception>
		// Token: 0x0600569C RID: 22172 RVA: 0x000041F3 File Offset: 0x000023F3
		public static IEnumerable<string> ResolveNamespace(string namespaceName, IEnumerable<string> packageGraphFilePaths)
		{
			throw new NotImplementedException();
		}

		/// <summary>Locates the Windows Metadata files for the specified namespace, given the specified locations to search.</summary>
		/// <param name="namespaceName">The namespace to resolve. </param>
		/// <param name="windowsSdkFilePath">The path to search for Windows Metadata files provided by the SDK, or <see langword="null" /> to search for Windows Metadata files from the operating system installation. </param>
		/// <param name="packageGraphFilePaths">The application paths to search for Windows Metadata files. </param>
		/// <returns>An enumerable list of strings that represent the Windows Metadata files that define <paramref name="namespaceName" />. </returns>
		/// <exception cref="T:System.PlatformNotSupportedException">The operating system version does not support the Windows Runtime. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="namespaceName" /> is <see langword="null" />.</exception>
		// Token: 0x0600569D RID: 22173 RVA: 0x000041F3 File Offset: 0x000023F3
		public static IEnumerable<string> ResolveNamespace(string namespaceName, string windowsSdkFilePath, IEnumerable<string> packageGraphFilePaths)
		{
			throw new NotImplementedException();
		}

		/// <summary>Occurs when the resolution of a Windows Metadata file fails in the design environment. </summary>
		// Token: 0x14000020 RID: 32
		// (add) Token: 0x0600569E RID: 22174 RVA: 0x0011C21C File Offset: 0x0011A41C
		// (remove) Token: 0x0600569F RID: 22175 RVA: 0x0011C250 File Offset: 0x0011A450
		public static event EventHandler<DesignerNamespaceResolveEventArgs> DesignerNamespaceResolve
		{
			[CompilerGenerated]
			add
			{
				EventHandler<DesignerNamespaceResolveEventArgs> eventHandler = WindowsRuntimeMetadata.DesignerNamespaceResolve;
				EventHandler<DesignerNamespaceResolveEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<DesignerNamespaceResolveEventArgs> value2 = (EventHandler<DesignerNamespaceResolveEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<DesignerNamespaceResolveEventArgs>>(ref WindowsRuntimeMetadata.DesignerNamespaceResolve, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<DesignerNamespaceResolveEventArgs> eventHandler = WindowsRuntimeMetadata.DesignerNamespaceResolve;
				EventHandler<DesignerNamespaceResolveEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<DesignerNamespaceResolveEventArgs> value2 = (EventHandler<DesignerNamespaceResolveEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<DesignerNamespaceResolveEventArgs>>(ref WindowsRuntimeMetadata.DesignerNamespaceResolve, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		/// <summary>Occurs when the resolution of a Windows Metadata file fails in the reflection-only context. </summary>
		// Token: 0x14000021 RID: 33
		// (add) Token: 0x060056A0 RID: 22176 RVA: 0x0011C284 File Offset: 0x0011A484
		// (remove) Token: 0x060056A1 RID: 22177 RVA: 0x0011C2B8 File Offset: 0x0011A4B8
		public static event EventHandler<NamespaceResolveEventArgs> ReflectionOnlyNamespaceResolve
		{
			[CompilerGenerated]
			add
			{
				EventHandler<NamespaceResolveEventArgs> eventHandler = WindowsRuntimeMetadata.ReflectionOnlyNamespaceResolve;
				EventHandler<NamespaceResolveEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<NamespaceResolveEventArgs> value2 = (EventHandler<NamespaceResolveEventArgs>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<NamespaceResolveEventArgs>>(ref WindowsRuntimeMetadata.ReflectionOnlyNamespaceResolve, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<NamespaceResolveEventArgs> eventHandler = WindowsRuntimeMetadata.ReflectionOnlyNamespaceResolve;
				EventHandler<NamespaceResolveEventArgs> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<NamespaceResolveEventArgs> value2 = (EventHandler<NamespaceResolveEventArgs>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<NamespaceResolveEventArgs>>(ref WindowsRuntimeMetadata.ReflectionOnlyNamespaceResolve, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		// Token: 0x04002C48 RID: 11336
		[CompilerGenerated]
		private static EventHandler<DesignerNamespaceResolveEventArgs> DesignerNamespaceResolve;

		// Token: 0x04002C49 RID: 11337
		[CompilerGenerated]
		private static EventHandler<NamespaceResolveEventArgs> ReflectionOnlyNamespaceResolve;
	}
}
