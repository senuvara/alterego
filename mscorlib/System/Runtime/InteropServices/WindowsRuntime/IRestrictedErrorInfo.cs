﻿using System;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	// Token: 0x0200094A RID: 2378
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("82BA7092-4C88-427D-A7BC-16DD93FEB67E")]
	[ComImport]
	internal interface IRestrictedErrorInfo
	{
		// Token: 0x0600564C RID: 22092
		void GetErrorDetails([MarshalAs(UnmanagedType.BStr)] out string description, out int error, [MarshalAs(UnmanagedType.BStr)] out string restrictedDescription, [MarshalAs(UnmanagedType.BStr)] out string capabilitySid);

		// Token: 0x0600564D RID: 22093
		void GetReference([MarshalAs(UnmanagedType.BStr)] out string reference);
	}
}
