﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime.InteropServices.WindowsRuntime
{
	// Token: 0x02000942 RID: 2370
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false)]
	[FriendAccessAllowed]
	internal sealed class WindowsRuntimeImportAttribute : Attribute
	{
		// Token: 0x0600562F RID: 22063 RVA: 0x000020BF File Offset: 0x000002BF
		public WindowsRuntimeImportAttribute()
		{
		}
	}
}
