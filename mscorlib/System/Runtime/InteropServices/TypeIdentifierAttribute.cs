﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Provides support for type equivalence.</summary>
	// Token: 0x020008B3 RID: 2227
	[AttributeUsage(AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate, AllowMultiple = false, Inherited = false)]
	[ComVisible(false)]
	public sealed class TypeIdentifierAttribute : Attribute
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Runtime.InteropServices.TypeIdentifierAttribute" /> class. </summary>
		// Token: 0x060052AB RID: 21163 RVA: 0x000020BF File Offset: 0x000002BF
		public TypeIdentifierAttribute()
		{
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Runtime.InteropServices.TypeIdentifierAttribute" /> class with the specified scope and identifier. </summary>
		/// <param name="scope">The first type equivalence string.</param>
		/// <param name="identifier">The second type equivalence string.</param>
		// Token: 0x060052AC RID: 21164 RVA: 0x00118765 File Offset: 0x00116965
		public TypeIdentifierAttribute(string scope, string identifier)
		{
			this.Scope_ = scope;
			this.Identifier_ = identifier;
		}

		/// <summary>Gets the value of the <paramref name="scope" /> parameter that was passed to the <see cref="M:System.Runtime.InteropServices.TypeIdentifierAttribute.#ctor(System.String,System.String)" /> constructor.</summary>
		/// <returns>The value of the constructor's <paramref name="scope" /> parameter.</returns>
		// Token: 0x17000E65 RID: 3685
		// (get) Token: 0x060052AD RID: 21165 RVA: 0x0011877B File Offset: 0x0011697B
		public string Scope
		{
			get
			{
				return this.Scope_;
			}
		}

		/// <summary>Gets the value of the <paramref name="identifier" /> parameter that was passed to the <see cref="M:System.Runtime.InteropServices.TypeIdentifierAttribute.#ctor(System.String,System.String)" /> constructor.</summary>
		/// <returns>The value of the constructor's <paramref name="identifier" /> parameter.</returns>
		// Token: 0x17000E66 RID: 3686
		// (get) Token: 0x060052AE RID: 21166 RVA: 0x00118783 File Offset: 0x00116983
		public string Identifier
		{
			get
			{
				return this.Identifier_;
			}
		}

		// Token: 0x04002A2E RID: 10798
		internal string Scope_;

		// Token: 0x04002A2F RID: 10799
		internal string Identifier_;
	}
}
