﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates that a parameter is optional.</summary>
	// Token: 0x020008D3 RID: 2259
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	public sealed class OptionalAttribute : Attribute
	{
		// Token: 0x060052E7 RID: 21223 RVA: 0x00118A57 File Offset: 0x00116C57
		internal static Attribute GetCustomAttribute(RuntimeParameterInfo parameter)
		{
			if (!parameter.IsOptional)
			{
				return null;
			}
			return new OptionalAttribute();
		}

		// Token: 0x060052E8 RID: 21224 RVA: 0x00118A68 File Offset: 0x00116C68
		internal static bool IsDefined(RuntimeParameterInfo parameter)
		{
			return parameter.IsOptional;
		}

		/// <summary>Initializes a new instance of the <see langword="OptionalAttribute" /> class with default values.</summary>
		// Token: 0x060052E9 RID: 21225 RVA: 0x000020BF File Offset: 0x000002BF
		public OptionalAttribute()
		{
		}
	}
}
