﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Security;
using System.Security.Permissions;
using System.Threading;

namespace System.Runtime.InteropServices
{
	/// <summary>Represents a wrapper class for operating system handles. This class must be inherited.</summary>
	// Token: 0x020008FE RID: 2302
	[SecurityCritical]
	[SecurityPermission(SecurityAction.InheritanceDemand, UnmanagedCode = true)]
	[StructLayout(LayoutKind.Sequential)]
	public abstract class SafeHandle : CriticalFinalizerObject, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.SafeHandle" /> class with the specified invalid handle value.</summary>
		/// <param name="invalidHandleValue">The value of an invalid handle (usually 0 or -1).  Your implementation of <see cref="P:System.Runtime.InteropServices.SafeHandle.IsInvalid" /> should return <see langword="true" /> for this value.</param>
		/// <param name="ownsHandle">
		///       <see langword="true" /> to reliably let <see cref="T:System.Runtime.InteropServices.SafeHandle" /> release the handle during the finalization phase; otherwise, <see langword="false" /> (not recommended). </param>
		/// <exception cref="T:System.TypeLoadException">The derived class resides in an assembly without unmanaged code access permission. </exception>
		// Token: 0x06005374 RID: 21364 RVA: 0x00119464 File Offset: 0x00117664
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		protected SafeHandle(IntPtr invalidHandleValue, bool ownsHandle)
		{
			this.handle = invalidHandleValue;
			this._state = 4;
			this._ownsHandle = ownsHandle;
			if (!ownsHandle)
			{
				GC.SuppressFinalize(this);
			}
			this._fullyInitialized = true;
		}

		// Token: 0x06005375 RID: 21365 RVA: 0x000F8AC5 File Offset: 0x000F6CC5
		protected SafeHandle()
		{
			throw new NotImplementedException();
		}

		/// <summary>Frees all resources associated with the handle.</summary>
		// Token: 0x06005376 RID: 21366 RVA: 0x00119494 File Offset: 0x00117694
		[SecuritySafeCritical]
		~SafeHandle()
		{
			this.Dispose(false);
		}

		/// <summary>Sets the handle to the specified pre-existing handle.</summary>
		/// <param name="handle">The pre-existing handle to use. </param>
		// Token: 0x06005377 RID: 21367 RVA: 0x001194C4 File Offset: 0x001176C4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected void SetHandle(IntPtr handle)
		{
			this.handle = handle;
		}

		/// <summary>Returns the value of the <see cref="F:System.Runtime.InteropServices.SafeHandle.handle" /> field.</summary>
		/// <returns>An <see langword="IntPtr" /> representing the value of the <see cref="F:System.Runtime.InteropServices.SafeHandle.handle" /> field. If the handle has been marked invalid with <see cref="M:System.Runtime.InteropServices.SafeHandle.SetHandleAsInvalid" />, this method still returns the original handle value, which can be a stale value.</returns>
		// Token: 0x06005378 RID: 21368 RVA: 0x001194CD File Offset: 0x001176CD
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public IntPtr DangerousGetHandle()
		{
			return this.handle;
		}

		/// <summary>Gets a value indicating whether the handle is closed.</summary>
		/// <returns>
		///     <see langword="true" /> if the handle is closed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E95 RID: 3733
		// (get) Token: 0x06005379 RID: 21369 RVA: 0x001194D5 File Offset: 0x001176D5
		public bool IsClosed
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return (this._state & 1) == 1;
			}
		}

		/// <summary>When overridden in a derived class, gets a value indicating whether the handle value is invalid.</summary>
		/// <returns>
		///     <see langword="true" /> if the handle value is invalid; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000E96 RID: 3734
		// (get) Token: 0x0600537A RID: 21370
		public abstract bool IsInvalid { [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)] get; }

		/// <summary>Marks the handle for releasing and freeing resources.</summary>
		// Token: 0x0600537B RID: 21371 RVA: 0x001194E2 File Offset: 0x001176E2
		[SecurityCritical]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void Close()
		{
			this.Dispose(true);
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Runtime.InteropServices.SafeHandle" /> class.</summary>
		// Token: 0x0600537C RID: 21372 RVA: 0x001194E2 File Offset: 0x001176E2
		[SecuritySafeCritical]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Runtime.InteropServices.SafeHandle" /> class specifying whether to perform a normal dispose operation.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> for a normal dispose operation; <see langword="false" /> to finalize the handle.</param>
		// Token: 0x0600537D RID: 21373 RVA: 0x001194EB File Offset: 0x001176EB
		[SecurityCritical]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.InternalDispose();
				return;
			}
			this.InternalFinalize();
		}

		/// <summary>When overridden in a derived class, executes the code required to free the handle.</summary>
		/// <returns>
		///     <see langword="true" /> if the handle is released successfully; otherwise, in the event of a catastrophic failure,<see langword=" false" />. In this case, it generates a releaseHandleFailed MDA Managed Debugging Assistant.</returns>
		// Token: 0x0600537E RID: 21374
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected abstract bool ReleaseHandle();

		/// <summary>Marks a handle as no longer used.</summary>
		// Token: 0x0600537F RID: 21375 RVA: 0x00119500 File Offset: 0x00117700
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void SetHandleAsInvalid()
		{
			try
			{
			}
			finally
			{
				int state;
				int value;
				do
				{
					state = this._state;
					value = (state | 1);
				}
				while (Interlocked.CompareExchange(ref this._state, value, state) != state);
				GC.SuppressFinalize(this);
			}
		}

		/// <summary>Manually increments the reference counter on <see cref="T:System.Runtime.InteropServices.SafeHandle" /> instances.</summary>
		/// <param name="success">
		///       <see langword="true" /> if the reference counter was successfully incremented; otherwise, <see langword="false" />.</param>
		// Token: 0x06005380 RID: 21376 RVA: 0x00119544 File Offset: 0x00117744
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public void DangerousAddRef(ref bool success)
		{
			try
			{
			}
			finally
			{
				if (!this._fullyInitialized)
				{
					throw new InvalidOperationException();
				}
				for (;;)
				{
					int state = this._state;
					if ((state & 1) != 0)
					{
						break;
					}
					int value = state + 4;
					if (Interlocked.CompareExchange(ref this._state, value, state) == state)
					{
						goto Block_5;
					}
				}
				throw new ObjectDisposedException(null, "Safe handle has been closed");
				Block_5:
				success = true;
			}
		}

		/// <summary>Manually decrements the reference counter on a <see cref="T:System.Runtime.InteropServices.SafeHandle" /> instance.</summary>
		// Token: 0x06005381 RID: 21377 RVA: 0x001195A4 File Offset: 0x001177A4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void DangerousRelease()
		{
			this.DangerousReleaseInternal(false);
		}

		// Token: 0x06005382 RID: 21378 RVA: 0x001195AD File Offset: 0x001177AD
		private void InternalDispose()
		{
			if (!this._fullyInitialized)
			{
				throw new InvalidOperationException();
			}
			this.DangerousReleaseInternal(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06005383 RID: 21379 RVA: 0x001195CA File Offset: 0x001177CA
		private void InternalFinalize()
		{
			if (this._fullyInitialized)
			{
				this.DangerousReleaseInternal(true);
			}
		}

		// Token: 0x06005384 RID: 21380 RVA: 0x001195DC File Offset: 0x001177DC
		private void DangerousReleaseInternal(bool dispose)
		{
			try
			{
			}
			finally
			{
				if (!this._fullyInitialized)
				{
					throw new InvalidOperationException();
				}
				bool flag;
				for (;;)
				{
					int state = this._state;
					if (dispose && (state & 2) != 0)
					{
						break;
					}
					if ((state & 2147483644) == 0)
					{
						goto Block_6;
					}
					flag = ((state & 2147483644) == 4 && (state & 1) == 0 && this._ownsHandle && !this.IsInvalid);
					int num = (state & 2147483644) - 4;
					if ((state & 2147483644) == 4)
					{
						num |= 1;
					}
					if (dispose)
					{
						num |= 2;
					}
					if (Interlocked.CompareExchange(ref this._state, num, state) == state)
					{
						goto IL_A0;
					}
				}
				flag = false;
				goto IL_A0;
				Block_6:
				throw new ObjectDisposedException(null, "Safe handle has been closed");
				IL_A0:
				if (flag)
				{
					this.ReleaseHandle();
				}
			}
		}

		/// <summary>Specifies the handle to be wrapped.</summary>
		// Token: 0x04002B17 RID: 11031
		protected IntPtr handle;

		// Token: 0x04002B18 RID: 11032
		private int _state;

		// Token: 0x04002B19 RID: 11033
		private bool _ownsHandle;

		// Token: 0x04002B1A RID: 11034
		private bool _fullyInitialized;

		// Token: 0x04002B1B RID: 11035
		private const int RefCount_Mask = 2147483644;

		// Token: 0x04002B1C RID: 11036
		private const int RefCount_One = 4;

		// Token: 0x020008FF RID: 2303
		private enum State
		{
			// Token: 0x04002B1E RID: 11038
			Closed = 1,
			// Token: 0x04002B1F RID: 11039
			Disposed
		}
	}
}
