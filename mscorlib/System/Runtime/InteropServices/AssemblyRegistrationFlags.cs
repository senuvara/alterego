﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Defines a set of flags used when registering assemblies.</summary>
	// Token: 0x020008F6 RID: 2294
	[ComVisible(true)]
	[Flags]
	public enum AssemblyRegistrationFlags
	{
		/// <summary>Indicates no special settings.</summary>
		// Token: 0x04002B11 RID: 11025
		None = 0,
		/// <summary>Indicates that the code base key for the assembly should be set in the registry.</summary>
		// Token: 0x04002B12 RID: 11026
		SetCodeBase = 1
	}
}
