﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>This attribute has been deprecated. </summary>
	// Token: 0x020008E2 RID: 2274
	[Obsolete("This attribute has been deprecated.  Application Domains no longer respect Activation Context boundaries in IDispatch calls.", false)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class SetWin32ContextInIDispatchAttribute : Attribute
	{
		/// <summary>This attribute has been deprecated.  </summary>
		// Token: 0x06005313 RID: 21267 RVA: 0x000020BF File Offset: 0x000002BF
		public SetWin32ContextInIDispatchAttribute()
		{
		}
	}
}
