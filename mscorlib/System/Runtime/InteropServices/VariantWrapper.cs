﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Marshals data of type <see langword="VT_VARIANT | VT_BYREF" /> from managed to unmanaged code. This class cannot be inherited.</summary>
	// Token: 0x02000918 RID: 2328
	[Serializable]
	public sealed class VariantWrapper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.VariantWrapper" /> class for the specified <see cref="T:System.Object" /> parameter.</summary>
		/// <param name="obj">The object to marshal. </param>
		// Token: 0x0600538C RID: 21388 RVA: 0x001196CE File Offset: 0x001178CE
		public VariantWrapper(object obj)
		{
			this.m_WrappedObject = obj;
		}

		/// <summary>Gets the object wrapped by the <see cref="T:System.Runtime.InteropServices.VariantWrapper" /> object.</summary>
		/// <returns>The object wrapped by the <see cref="T:System.Runtime.InteropServices.VariantWrapper" /> object.</returns>
		// Token: 0x17000E98 RID: 3736
		// (get) Token: 0x0600538D RID: 21389 RVA: 0x001196DD File Offset: 0x001178DD
		public object WrappedObject
		{
			get
			{
				return this.m_WrappedObject;
			}
		}

		// Token: 0x04002BBF RID: 11199
		private object m_WrappedObject;
	}
}
