﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000940 RID: 2368
	internal static class AddrofIntrinsics
	{
		// Token: 0x0600562C RID: 22060 RVA: 0x0011AD77 File Offset: 0x00118F77
		internal static IntPtr AddrOf<T>(T ftn)
		{
			return Marshal.GetFunctionPointerForDelegate<T>(ftn);
		}
	}
}
