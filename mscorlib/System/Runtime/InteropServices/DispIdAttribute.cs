﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Specifies the COM dispatch identifier (DISPID) of a method, field, or property.</summary>
	// Token: 0x020008B5 RID: 2229
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event, Inherited = false)]
	public sealed class DispIdAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see langword="DispIdAttribute" /> class with the specified DISPID.</summary>
		/// <param name="dispId">The DISPID for the member. </param>
		// Token: 0x060052B0 RID: 21168 RVA: 0x0011878B File Offset: 0x0011698B
		public DispIdAttribute(int dispId)
		{
			this._val = dispId;
		}

		/// <summary>Gets the DISPID for the member.</summary>
		/// <returns>The DISPID for the member.</returns>
		// Token: 0x17000E67 RID: 3687
		// (get) Token: 0x060052B1 RID: 21169 RVA: 0x0011879A File Offset: 0x0011699A
		public int Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002A30 RID: 10800
		internal int _val;
	}
}
