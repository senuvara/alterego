﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Identifies the source interface and the class that implements the methods of the event interface that is generated when a coclass is imported from a COM type library.</summary>
	// Token: 0x020008DD RID: 2269
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Interface, Inherited = false)]
	public sealed class ComEventInterfaceAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComEventInterfaceAttribute" /> class with the source interface and event provider class.</summary>
		/// <param name="SourceInterface">A <see cref="T:System.Type" /> that contains the original source interface from the type library. COM uses this interface to call back to the managed class. </param>
		/// <param name="EventProvider">A <see cref="T:System.Type" /> that contains the class that implements the methods of the event interface. </param>
		// Token: 0x06005304 RID: 21252 RVA: 0x00118DCB File Offset: 0x00116FCB
		public ComEventInterfaceAttribute(Type SourceInterface, Type EventProvider)
		{
			this._SourceInterface = SourceInterface;
			this._EventProvider = EventProvider;
		}

		/// <summary>Gets the original source interface from the type library.</summary>
		/// <returns>A <see cref="T:System.Type" /> containing the source interface.</returns>
		// Token: 0x17000E7F RID: 3711
		// (get) Token: 0x06005305 RID: 21253 RVA: 0x00118DE1 File Offset: 0x00116FE1
		public Type SourceInterface
		{
			get
			{
				return this._SourceInterface;
			}
		}

		/// <summary>Gets the class that implements the methods of the event interface.</summary>
		/// <returns>A <see cref="T:System.Type" /> that contains the class that implements the methods of the event interface.</returns>
		// Token: 0x17000E80 RID: 3712
		// (get) Token: 0x06005306 RID: 21254 RVA: 0x00118DE9 File Offset: 0x00116FE9
		public Type EventProvider
		{
			get
			{
				return this._EventProvider;
			}
		}

		// Token: 0x04002AE8 RID: 10984
		internal Type _SourceInterface;

		// Token: 0x04002AE9 RID: 10985
		internal Type _EventProvider;
	}
}
