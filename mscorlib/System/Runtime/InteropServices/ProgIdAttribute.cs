﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Allows the user to specify the ProgID of a class.</summary>
	// Token: 0x020008C0 RID: 2240
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public sealed class ProgIdAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see langword="ProgIdAttribute" /> with the specified ProgID.</summary>
		/// <param name="progId">The ProgID to be assigned to the class. </param>
		// Token: 0x060052C2 RID: 21186 RVA: 0x00118831 File Offset: 0x00116A31
		public ProgIdAttribute(string progId)
		{
			this._val = progId;
		}

		/// <summary>Gets the ProgID of the class.</summary>
		/// <returns>The ProgID of the class.</returns>
		// Token: 0x17000E6E RID: 3694
		// (get) Token: 0x060052C3 RID: 21187 RVA: 0x00118840 File Offset: 0x00116A40
		public string Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002A40 RID: 10816
		internal string _val;
	}
}
