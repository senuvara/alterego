﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Provides support for user customization of interop stubs in managed-to-COM interop scenarios.</summary>
	// Token: 0x020008E3 RID: 2275
	[AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
	[ComVisible(false)]
	public sealed class ManagedToNativeComInteropStubAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ManagedToNativeComInteropStubAttribute" /> class with the specified class type and method name.</summary>
		/// <param name="classType">The class that contains the required stub method. </param>
		/// <param name="methodName">The name of the stub method.</param>
		/// <exception cref="T:System.ArgumentException">The stub method is not in the same assembly as the interface that contains the managed interop method.-or-
		///         <paramref name="classType" /> is a generic type.-or-
		///         <paramref name="classType" /> is an interface. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="methodName" /> cannot be found.-or-The method is not static or non-generic.-or-The method's parameter list does not match the expected parameter list for the stub.</exception>
		/// <exception cref="T:System.MethodAccessException">The interface that contains the managed interop method has no access to the stub method, because the stub method has private or protected accessibility, or because of a security issue.</exception>
		// Token: 0x06005314 RID: 21268 RVA: 0x00118E8A File Offset: 0x0011708A
		public ManagedToNativeComInteropStubAttribute(Type classType, string methodName)
		{
			this._classType = classType;
			this._methodName = methodName;
		}

		/// <summary>Gets the class that contains the required stub method.</summary>
		/// <returns>The class that contains the customized interop stub.</returns>
		// Token: 0x17000E89 RID: 3721
		// (get) Token: 0x06005315 RID: 21269 RVA: 0x00118EA0 File Offset: 0x001170A0
		public Type ClassType
		{
			get
			{
				return this._classType;
			}
		}

		/// <summary>Gets the name of the stub method.</summary>
		/// <returns>The name of a customized interop stub.</returns>
		// Token: 0x17000E8A RID: 3722
		// (get) Token: 0x06005316 RID: 21270 RVA: 0x00118EA8 File Offset: 0x001170A8
		public string MethodName
		{
			get
			{
				return this._methodName;
			}
		}

		// Token: 0x04002AF3 RID: 10995
		internal Type _classType;

		// Token: 0x04002AF4 RID: 10996
		internal string _methodName;
	}
}
