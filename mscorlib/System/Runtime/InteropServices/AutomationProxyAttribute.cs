﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Specifies whether the type should be marshaled using the Automation marshaler or a custom proxy and stub.</summary>
	// Token: 0x020008DA RID: 2266
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Interface, Inherited = false)]
	[ComVisible(true)]
	public sealed class AutomationProxyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.AutomationProxyAttribute" /> class.</summary>
		/// <param name="val">
		///       <see langword="true" /> if the class should be marshaled using the Automation Marshaler; <see langword="false" /> if a proxy stub marshaler should be used. </param>
		// Token: 0x060052FD RID: 21245 RVA: 0x00118D77 File Offset: 0x00116F77
		public AutomationProxyAttribute(bool val)
		{
			this._val = val;
		}

		/// <summary>Gets a value indicating the type of marshaler to use.</summary>
		/// <returns>
		///     <see langword="true" /> if the class should be marshaled using the Automation Marshaler; <see langword="false" /> if a proxy stub marshaler should be used.</returns>
		// Token: 0x17000E7B RID: 3707
		// (get) Token: 0x060052FE RID: 21246 RVA: 0x00118D86 File Offset: 0x00116F86
		public bool Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002AE4 RID: 10980
		internal bool _val;
	}
}
