﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Specifies the method to call when you register an assembly for use from COM; this enables the execution of user-written code during the registration process.</summary>
	// Token: 0x020008BE RID: 2238
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	[ComVisible(true)]
	public sealed class ComRegisterFunctionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComRegisterFunctionAttribute" /> class.</summary>
		// Token: 0x060052C0 RID: 21184 RVA: 0x000020BF File Offset: 0x000002BF
		public ComRegisterFunctionAttribute()
		{
		}
	}
}
