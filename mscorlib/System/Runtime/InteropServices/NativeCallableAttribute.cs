﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020008B0 RID: 2224
	[AttributeUsage(AttributeTargets.Method)]
	internal sealed class NativeCallableAttribute : Attribute
	{
		// Token: 0x0600529F RID: 21151 RVA: 0x000020BF File Offset: 0x000002BF
		public NativeCallableAttribute()
		{
		}

		// Token: 0x04002A24 RID: 10788
		public string EntryPoint;

		// Token: 0x04002A25 RID: 10789
		public CallingConvention CallingConvention;
	}
}
