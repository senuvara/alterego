﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates how an assembly should be produced.</summary>
	// Token: 0x02000A9D RID: 2717
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum TypeLibImporterFlags
	{
		/// <summary>Imports a type library for any platform.</summary>
		// Token: 0x04002F72 RID: 12146
		ImportAsAgnostic = 2048,
		/// <summary>Imports a library for the ARM platform.</summary>
		// Token: 0x04002F73 RID: 12147
		ImportAsArm = 16384,
		/// <summary>Imports a type library for the Itanium platform.</summary>
		// Token: 0x04002F74 RID: 12148
		ImportAsItanium = 1024,
		/// <summary>Imports a type library for the x86 64-bit platform.</summary>
		// Token: 0x04002F75 RID: 12149
		ImportAsX64 = 512,
		/// <summary>Imports a type library for the x86 platform.</summary>
		// Token: 0x04002F76 RID: 12150
		ImportAsX86 = 256,
		/// <summary>Prevents inclusion of a version resource in the interop assembly. For more information, see the <see cref="M:System.Reflection.Emit.AssemblyBuilder.DefineVersionInfoResource" /> method.</summary>
		// Token: 0x04002F77 RID: 12151
		NoDefineVersionResource = 8192,
		/// <summary>No special settings. This is the default.</summary>
		// Token: 0x04002F78 RID: 12152
		None = 0,
		/// <summary>Not used.</summary>
		// Token: 0x04002F79 RID: 12153
		PreventClassMembers = 16,
		/// <summary>Generates a primary interop assembly. For more information, see the <see cref="T:System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute" /> attribute. A keyfile must be specified.</summary>
		// Token: 0x04002F7A RID: 12154
		PrimaryInteropAssembly = 1,
		/// <summary>Uses reflection-only loading.</summary>
		// Token: 0x04002F7B RID: 12155
		ReflectionOnlyLoading = 4096,
		/// <summary>Imports all <see langword="SAFEARRAY" /> instances as <see cref="T:System.Array" /> instead of typed, single-dimensional, zero-based managed arrays. This option is useful when dealing with multi-dimensional, non-zero-based <see langword="SAFEARRAY" /> instances, which otherwise cannot be accessed unless you edit the resulting assembly by using the MSIL Disassembler (Ildasm.exe) and MSIL Assembler (Ilasm.exe) tools.</summary>
		// Token: 0x04002F7C RID: 12156
		SafeArrayAsSystemArray = 4,
		/// <summary>Uses serializable classes.</summary>
		// Token: 0x04002F7D RID: 12157
		SerializableValueClasses = 32,
		/// <summary>Transforms <see langword="[out, retval]" /> parameters of methods on dispatch-only interfaces (dispinterface) into return values.</summary>
		// Token: 0x04002F7E RID: 12158
		TransformDispRetVals = 8,
		/// <summary>Imports all interfaces as interfaces that suppress the common language runtime's stack crawl for <see cref="F:System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode" /> permission. Be sure you understand the responsibilities associated with suppressing this security check.</summary>
		// Token: 0x04002F7F RID: 12159
		UnsafeInterfaces = 2
	}
}
