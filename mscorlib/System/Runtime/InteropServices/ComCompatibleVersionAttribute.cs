﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates to a COM client that all classes in the current version of an assembly are compatible with classes in an earlier version of the assembly.</summary>
	// Token: 0x020008DF RID: 2271
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class ComCompatibleVersionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComCompatibleVersionAttribute" /> class with the major version, minor version, build, and revision numbers of the assembly.</summary>
		/// <param name="major">The major version number of the assembly. </param>
		/// <param name="minor">The minor version number of the assembly. </param>
		/// <param name="build">The build number of the assembly. </param>
		/// <param name="revision">The revision number of the assembly. </param>
		// Token: 0x0600530A RID: 21258 RVA: 0x00118E17 File Offset: 0x00117017
		public ComCompatibleVersionAttribute(int major, int minor, int build, int revision)
		{
			this._major = major;
			this._minor = minor;
			this._build = build;
			this._revision = revision;
		}

		/// <summary>Gets the major version number of the assembly.</summary>
		/// <returns>The major version number of the assembly.</returns>
		// Token: 0x17000E83 RID: 3715
		// (get) Token: 0x0600530B RID: 21259 RVA: 0x00118E3C File Offset: 0x0011703C
		public int MajorVersion
		{
			get
			{
				return this._major;
			}
		}

		/// <summary>Gets the minor version number of the assembly.</summary>
		/// <returns>The minor version number of the assembly.</returns>
		// Token: 0x17000E84 RID: 3716
		// (get) Token: 0x0600530C RID: 21260 RVA: 0x00118E44 File Offset: 0x00117044
		public int MinorVersion
		{
			get
			{
				return this._minor;
			}
		}

		/// <summary>Gets the build number of the assembly.</summary>
		/// <returns>The build number of the assembly.</returns>
		// Token: 0x17000E85 RID: 3717
		// (get) Token: 0x0600530D RID: 21261 RVA: 0x00118E4C File Offset: 0x0011704C
		public int BuildNumber
		{
			get
			{
				return this._build;
			}
		}

		/// <summary>Gets the revision number of the assembly.</summary>
		/// <returns>The revision number of the assembly.</returns>
		// Token: 0x17000E86 RID: 3718
		// (get) Token: 0x0600530E RID: 21262 RVA: 0x00118E54 File Offset: 0x00117054
		public int RevisionNumber
		{
			get
			{
				return this._revision;
			}
		}

		// Token: 0x04002AEC RID: 10988
		internal int _major;

		// Token: 0x04002AED RID: 10989
		internal int _minor;

		// Token: 0x04002AEE RID: 10990
		internal int _build;

		// Token: 0x04002AEF RID: 10991
		internal int _revision;
	}
}
