﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates the COM alias for a parameter or field type.</summary>
	// Token: 0x020008D9 RID: 2265
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue, Inherited = false)]
	[ComVisible(true)]
	public sealed class ComAliasNameAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComAliasNameAttribute" /> class with the alias for the attributed field or parameter.</summary>
		/// <param name="alias">The alias for the field or parameter as found in the type library when it was imported. </param>
		// Token: 0x060052FB RID: 21243 RVA: 0x00118D60 File Offset: 0x00116F60
		public ComAliasNameAttribute(string alias)
		{
			this._val = alias;
		}

		/// <summary>Gets the alias for the field or parameter as found in the type library when it was imported.</summary>
		/// <returns>The alias for the field or parameter as found in the type library when it was imported.</returns>
		// Token: 0x17000E7A RID: 3706
		// (get) Token: 0x060052FC RID: 21244 RVA: 0x00118D6F File Offset: 0x00116F6F
		public string Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002AE3 RID: 10979
		internal string _val;
	}
}
