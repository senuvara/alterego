﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates that a method's unmanaged signature expects a locale identifier (LCID) parameter.</summary>
	// Token: 0x020008BD RID: 2237
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	[ComVisible(true)]
	public sealed class LCIDConversionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see langword="LCIDConversionAttribute" /> class with the position of the LCID in the unmanaged signature.</summary>
		/// <param name="lcid">Indicates the position of the LCID argument in the unmanaged signature, where 0 is the first argument. </param>
		// Token: 0x060052BE RID: 21182 RVA: 0x0011881A File Offset: 0x00116A1A
		public LCIDConversionAttribute(int lcid)
		{
			this._val = lcid;
		}

		/// <summary>Gets the position of the LCID argument in the unmanaged signature.</summary>
		/// <returns>The position of the LCID argument in the unmanaged signature, where 0 is the first argument.</returns>
		// Token: 0x17000E6D RID: 3693
		// (get) Token: 0x060052BF RID: 21183 RVA: 0x00118829 File Offset: 0x00116A29
		public int Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002A3F RID: 10815
		internal int _val;
	}
}
