﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates that the types defined within an assembly were originally defined in a type library.</summary>
	// Token: 0x020008C1 RID: 2241
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class ImportedFromTypeLibAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ImportedFromTypeLibAttribute" /> class with the name of the original type library file.</summary>
		/// <param name="tlbFile">The location of the original type library file. </param>
		// Token: 0x060052C4 RID: 21188 RVA: 0x00118848 File Offset: 0x00116A48
		public ImportedFromTypeLibAttribute(string tlbFile)
		{
			this._val = tlbFile;
		}

		/// <summary>Gets the name of the original type library file.</summary>
		/// <returns>The name of the original type library file.</returns>
		// Token: 0x17000E6F RID: 3695
		// (get) Token: 0x060052C5 RID: 21189 RVA: 0x00118857 File Offset: 0x00116A57
		public string Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002A41 RID: 10817
		internal string _val;
	}
}
