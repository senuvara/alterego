﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Use <see cref="T:System.Runtime.InteropServices.ComTypes.BIND_OPTS" /> instead.</summary>
	// Token: 0x0200091B RID: 2331
	[Obsolete]
	public struct BIND_OPTS
	{
		/// <summary>Specifies the size of the <see langword="BIND_OPTS" /> structure in bytes.</summary>
		// Token: 0x04002BC3 RID: 11203
		public int cbStruct;

		/// <summary>Controls aspects of moniker binding operations.</summary>
		// Token: 0x04002BC4 RID: 11204
		public int grfFlags;

		/// <summary>Flags that should be used when opening the file that contains the object identified by the moniker.</summary>
		// Token: 0x04002BC5 RID: 11205
		public int grfMode;

		/// <summary>Indicates the amount of time (clock time in milliseconds, as returned by the <see langword="GetTickCount" /> function) the caller specified to complete the binding operation.</summary>
		// Token: 0x04002BC6 RID: 11206
		public int dwTickCountDeadline;
	}
}
