﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	/// <summary>The exception thrown when an invalid COM object is used.</summary>
	// Token: 0x020008F4 RID: 2292
	[ComVisible(true)]
	[Serializable]
	public class InvalidComObjectException : SystemException
	{
		/// <summary>Initializes an instance of the <see langword="InvalidComObjectException" /> with default properties.</summary>
		// Token: 0x0600534B RID: 21323 RVA: 0x00119273 File Offset: 0x00117473
		public InvalidComObjectException() : base(Environment.GetResourceString("Attempt has been made to use a COM object that does not have a backing class factory."))
		{
			base.SetErrorCode(-2146233049);
		}

		/// <summary>Initializes an instance of the <see langword="InvalidComObjectException" /> with a message.</summary>
		/// <param name="message">The message that indicates the reason for the exception. </param>
		// Token: 0x0600534C RID: 21324 RVA: 0x00119290 File Offset: 0x00117490
		public InvalidComObjectException(string message) : base(message)
		{
			base.SetErrorCode(-2146233049);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.InvalidComObjectException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x0600534D RID: 21325 RVA: 0x001192A4 File Offset: 0x001174A4
		public InvalidComObjectException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233049);
		}

		/// <summary>Initializes a new instance of the <see langword="COMException" /> class from serialization data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info" /> is <see langword="null" />. </exception>
		// Token: 0x0600534E RID: 21326 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected InvalidComObjectException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
