﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Use <see cref="T:System.Runtime.InteropServices.ComTypes.STATSTG" /> instead.</summary>
	// Token: 0x0200092A RID: 2346
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct STATSTG
	{
		/// <summary>Pointer to a null-terminated string containing the name of the object described by this structure.</summary>
		// Token: 0x04002C0C RID: 11276
		public string pwcsName;

		/// <summary>Indicates the type of storage object which is one of the values from the <see langword="STGTY" /> enumeration.</summary>
		// Token: 0x04002C0D RID: 11277
		public int type;

		/// <summary>Specifies the size in bytes of the stream or byte array.</summary>
		// Token: 0x04002C0E RID: 11278
		public long cbSize;

		/// <summary>Indicates the last modification time for this storage, stream, or byte array.</summary>
		// Token: 0x04002C0F RID: 11279
		public FILETIME mtime;

		/// <summary>Indicates the creation time for this storage, stream, or byte array.</summary>
		// Token: 0x04002C10 RID: 11280
		public FILETIME ctime;

		/// <summary>Indicates the last access time for this storage, stream or byte array </summary>
		// Token: 0x04002C11 RID: 11281
		public FILETIME atime;

		/// <summary>Indicates the access mode that was specified when the object was opened.</summary>
		// Token: 0x04002C12 RID: 11282
		public int grfMode;

		/// <summary>Indicates the types of region locking supported by the stream or byte array.</summary>
		// Token: 0x04002C13 RID: 11283
		public int grfLocksSupported;

		/// <summary>Indicates the class identifier for the storage object.</summary>
		// Token: 0x04002C14 RID: 11284
		public Guid clsid;

		/// <summary>Indicates the current state bits of the storage object (the value most recently set by the <see langword="IStorage::SetStateBits" /> method).</summary>
		// Token: 0x04002C15 RID: 11285
		public int grfStateBits;

		/// <summary>Reserved for future use.</summary>
		// Token: 0x04002C16 RID: 11286
		public int reserved;
	}
}
