﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates that the attributed assembly is a primary interop assembly.</summary>
	// Token: 0x020008DB RID: 2267
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = true)]
	[ComVisible(true)]
	public sealed class PrimaryInteropAssemblyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.PrimaryInteropAssemblyAttribute" /> class with the major and minor version numbers of the type library for which this assembly is the primary interop assembly.</summary>
		/// <param name="major">The major version of the type library for which this assembly is the primary interop assembly. </param>
		/// <param name="minor">The minor version of the type library for which this assembly is the primary interop assembly. </param>
		// Token: 0x060052FF RID: 21247 RVA: 0x00118D8E File Offset: 0x00116F8E
		public PrimaryInteropAssemblyAttribute(int major, int minor)
		{
			this._major = major;
			this._minor = minor;
		}

		/// <summary>Gets the major version number of the type library for which this assembly is the primary interop assembly.</summary>
		/// <returns>The major version number of the type library for which this assembly is the primary interop assembly.</returns>
		// Token: 0x17000E7C RID: 3708
		// (get) Token: 0x06005300 RID: 21248 RVA: 0x00118DA4 File Offset: 0x00116FA4
		public int MajorVersion
		{
			get
			{
				return this._major;
			}
		}

		/// <summary>Gets the minor version number of the type library for which this assembly is the primary interop assembly.</summary>
		/// <returns>The minor version number of the type library for which this assembly is the primary interop assembly.</returns>
		// Token: 0x17000E7D RID: 3709
		// (get) Token: 0x06005301 RID: 21249 RVA: 0x00118DAC File Offset: 0x00116FAC
		public int MinorVersion
		{
			get
			{
				return this._minor;
			}
		}

		// Token: 0x04002AE5 RID: 10981
		internal int _major;

		// Token: 0x04002AE6 RID: 10982
		internal int _minor;
	}
}
