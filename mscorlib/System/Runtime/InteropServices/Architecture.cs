﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates the processor architecture. </summary>
	// Token: 0x020008AE RID: 2222
	public enum Architecture
	{
		/// <summary>An Intel-based 32-bit processor architecture. </summary>
		// Token: 0x04002A1C RID: 10780
		X86,
		/// <summary>An Intel-based 64-bit processor architecture. </summary>
		// Token: 0x04002A1D RID: 10781
		X64,
		/// <summary>A 32-bit ARM processor architecture. </summary>
		// Token: 0x04002A1E RID: 10782
		Arm,
		/// <summary>A 64-bit ARM processor architecture. </summary>
		// Token: 0x04002A1F RID: 10783
		Arm64
	}
}
