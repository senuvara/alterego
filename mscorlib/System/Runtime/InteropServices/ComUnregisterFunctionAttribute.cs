﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Specifies the method to call when you unregister an assembly for use from COM; this allows for the execution of user-written code during the unregistration process.</summary>
	// Token: 0x020008BF RID: 2239
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	public sealed class ComUnregisterFunctionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComUnregisterFunctionAttribute" /> class.</summary>
		// Token: 0x060052C1 RID: 21185 RVA: 0x000020BF File Offset: 0x000002BF
		public ComUnregisterFunctionAttribute()
		{
		}
	}
}
