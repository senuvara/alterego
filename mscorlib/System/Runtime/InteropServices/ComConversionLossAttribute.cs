﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates that information was lost about a class or interface when it was imported from a type library to an assembly.</summary>
	// Token: 0x020008C5 RID: 2245
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.All, Inherited = false)]
	public sealed class ComConversionLossAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see langword="ComConversionLossAttribute" /> class.</summary>
		// Token: 0x060052CF RID: 21199 RVA: 0x000020BF File Offset: 0x000002BF
		public ComConversionLossAttribute()
		{
		}
	}
}
