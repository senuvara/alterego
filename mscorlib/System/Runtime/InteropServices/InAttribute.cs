﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates that data should be marshaled from the caller to the callee, but not back to the caller.</summary>
	// Token: 0x020008D1 RID: 2257
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	public sealed class InAttribute : Attribute
	{
		// Token: 0x060052E1 RID: 21217 RVA: 0x00118A25 File Offset: 0x00116C25
		internal static Attribute GetCustomAttribute(RuntimeParameterInfo parameter)
		{
			if (!parameter.IsIn)
			{
				return null;
			}
			return new InAttribute();
		}

		// Token: 0x060052E2 RID: 21218 RVA: 0x00118A36 File Offset: 0x00116C36
		internal static bool IsDefined(RuntimeParameterInfo parameter)
		{
			return parameter.IsIn;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.InAttribute" /> class.</summary>
		// Token: 0x060052E3 RID: 21219 RVA: 0x000020BF File Offset: 0x000002BF
		public InAttribute()
		{
		}
	}
}
