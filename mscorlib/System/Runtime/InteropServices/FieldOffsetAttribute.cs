﻿using System;
using System.Reflection;
using System.Security;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates the physical position of fields within the unmanaged representation of a class or structure.</summary>
	// Token: 0x020008D8 RID: 2264
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	[ComVisible(true)]
	public sealed class FieldOffsetAttribute : Attribute
	{
		// Token: 0x060052F7 RID: 21239 RVA: 0x00118D10 File Offset: 0x00116F10
		[SecurityCritical]
		internal static Attribute GetCustomAttribute(RuntimeFieldInfo field)
		{
			int fieldOffset;
			if (field.DeclaringType != null && (fieldOffset = field.GetFieldOffset()) >= 0)
			{
				return new FieldOffsetAttribute(fieldOffset);
			}
			return null;
		}

		// Token: 0x060052F8 RID: 21240 RVA: 0x00118D3E File Offset: 0x00116F3E
		[SecurityCritical]
		internal static bool IsDefined(RuntimeFieldInfo field)
		{
			return FieldOffsetAttribute.GetCustomAttribute(field) != null;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.FieldOffsetAttribute" /> class with the offset in the structure to the beginning of the field.</summary>
		/// <param name="offset">The offset in bytes from the beginning of the structure to the beginning of the field. </param>
		// Token: 0x060052F9 RID: 21241 RVA: 0x00118D49 File Offset: 0x00116F49
		public FieldOffsetAttribute(int offset)
		{
			this._val = offset;
		}

		/// <summary>Gets the offset from the beginning of the structure to the beginning of the field.</summary>
		/// <returns>The offset from the beginning of the structure to the beginning of the field.</returns>
		// Token: 0x17000E79 RID: 3705
		// (get) Token: 0x060052FA RID: 21242 RVA: 0x00118D58 File Offset: 0x00116F58
		public int Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002AE2 RID: 10978
		internal int _val;
	}
}
