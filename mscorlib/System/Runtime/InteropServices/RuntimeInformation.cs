﻿using System;
using System.IO;
using Mono;

namespace System.Runtime.InteropServices
{
	/// <summary>Provides information about the .NET runtime installation. </summary>
	// Token: 0x02000919 RID: 2329
	public static class RuntimeInformation
	{
		/// <summary>Returns a string that indicates the name of the .NET installation on which an app is running. </summary>
		/// <returns>The name of the .NET installation on which the app is running. </returns>
		// Token: 0x17000E99 RID: 3737
		// (get) Token: 0x0600538E RID: 21390 RVA: 0x001196E5 File Offset: 0x001178E5
		public static string FrameworkDescription
		{
			get
			{
				return "Mono " + Runtime.GetDisplayName();
			}
		}

		/// <summary>Indicates whether the current application is running on the specified platform. </summary>
		/// <param name="osPlatform">A platform. </param>
		/// <returns>
		///   <see langword="true" /> if the current app is running on the specified platform; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600538F RID: 21391 RVA: 0x001196F8 File Offset: 0x001178F8
		public static bool IsOSPlatform(OSPlatform osPlatform)
		{
			PlatformID platform = Environment.OSVersion.Platform;
			if (platform == PlatformID.Win32NT)
			{
				return osPlatform == OSPlatform.Windows;
			}
			if (platform != PlatformID.Unix)
			{
				return false;
			}
			if (File.Exists("/usr/lib/libc.dylib"))
			{
				return osPlatform == OSPlatform.OSX;
			}
			return osPlatform == OSPlatform.Linux;
		}

		/// <summary>Gets a string that indicates the name of the operating system on which the app is running. </summary>
		/// <returns>The name of the operating system on which the app is running. </returns>
		// Token: 0x17000E9A RID: 3738
		// (get) Token: 0x06005390 RID: 21392 RVA: 0x0011974B File Offset: 0x0011794B
		public static string OSDescription
		{
			get
			{
				return Environment.OSVersion.VersionString;
			}
		}

		/// <summary>Gets the platform architecture on which the current app is running. </summary>
		/// <returns>The platform architecture on which the current app is running. </returns>
		// Token: 0x17000E9B RID: 3739
		// (get) Token: 0x06005391 RID: 21393 RVA: 0x00119757 File Offset: 0x00117957
		public static Architecture OSArchitecture
		{
			get
			{
				if (!Environment.Is64BitOperatingSystem)
				{
					return Architecture.X86;
				}
				return Architecture.X64;
			}
		}

		/// <summary>Gets the process architecture of the currently running app. </summary>
		/// <returns>The process architecture of the currently running app.</returns>
		// Token: 0x17000E9C RID: 3740
		// (get) Token: 0x06005392 RID: 21394 RVA: 0x00119763 File Offset: 0x00117963
		public static Architecture ProcessArchitecture
		{
			get
			{
				if (!Environment.Is64BitProcess)
				{
					return Architecture.X86;
				}
				return Architecture.X64;
			}
		}
	}
}
