﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	/// <summary>Indicates that data should be marshaled from callee back to caller.</summary>
	// Token: 0x020008D2 RID: 2258
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	[ComVisible(true)]
	public sealed class OutAttribute : Attribute
	{
		// Token: 0x060052E4 RID: 21220 RVA: 0x00118A3E File Offset: 0x00116C3E
		internal static Attribute GetCustomAttribute(RuntimeParameterInfo parameter)
		{
			if (!parameter.IsOut)
			{
				return null;
			}
			return new OutAttribute();
		}

		// Token: 0x060052E5 RID: 21221 RVA: 0x00118A4F File Offset: 0x00116C4F
		internal static bool IsDefined(RuntimeParameterInfo parameter)
		{
			return parameter.IsOut;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.OutAttribute" /> class.</summary>
		// Token: 0x060052E6 RID: 21222 RVA: 0x000020BF File Offset: 0x000002BF
		public OutAttribute()
		{
		}
	}
}
