﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Identifies a list of interfaces that are exposed as COM event sources for the attributed class.</summary>
	// Token: 0x020008C4 RID: 2244
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	[ComVisible(true)]
	public sealed class ComSourceInterfacesAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComSourceInterfacesAttribute" /> class with the name of the event source interface.</summary>
		/// <param name="sourceInterfaces">A null-delimited list of fully qualified event source interface names. </param>
		// Token: 0x060052C9 RID: 21193 RVA: 0x00118876 File Offset: 0x00116A76
		public ComSourceInterfacesAttribute(string sourceInterfaces)
		{
			this._val = sourceInterfaces;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComSourceInterfacesAttribute" /> class with the type to use as a source interface.</summary>
		/// <param name="sourceInterface">The <see cref="T:System.Type" /> of the source interface. </param>
		// Token: 0x060052CA RID: 21194 RVA: 0x00118885 File Offset: 0x00116A85
		public ComSourceInterfacesAttribute(Type sourceInterface)
		{
			this._val = sourceInterface.FullName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComSourceInterfacesAttribute" /> class with the types to use as source interfaces.</summary>
		/// <param name="sourceInterface1">The <see cref="T:System.Type" /> of the default source interface. </param>
		/// <param name="sourceInterface2">The <see cref="T:System.Type" /> of a source interface. </param>
		// Token: 0x060052CB RID: 21195 RVA: 0x00118899 File Offset: 0x00116A99
		public ComSourceInterfacesAttribute(Type sourceInterface1, Type sourceInterface2)
		{
			this._val = sourceInterface1.FullName + "\0" + sourceInterface2.FullName;
		}

		/// <summary>Initializes a new instance of the <see langword="ComSourceInterfacesAttribute" /> class with the types to use as source interfaces.</summary>
		/// <param name="sourceInterface1">The <see cref="T:System.Type" /> of the default source interface. </param>
		/// <param name="sourceInterface2">The <see cref="T:System.Type" /> of a source interface. </param>
		/// <param name="sourceInterface3">The <see cref="T:System.Type" /> of a source interface. </param>
		// Token: 0x060052CC RID: 21196 RVA: 0x001188C0 File Offset: 0x00116AC0
		public ComSourceInterfacesAttribute(Type sourceInterface1, Type sourceInterface2, Type sourceInterface3)
		{
			this._val = string.Concat(new string[]
			{
				sourceInterface1.FullName,
				"\0",
				sourceInterface2.FullName,
				"\0",
				sourceInterface3.FullName
			});
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.ComSourceInterfacesAttribute" /> class with the types to use as source interfaces.</summary>
		/// <param name="sourceInterface1">The <see cref="T:System.Type" /> of the default source interface. </param>
		/// <param name="sourceInterface2">The <see cref="T:System.Type" /> of a source interface. </param>
		/// <param name="sourceInterface3">The <see cref="T:System.Type" /> of a source interface. </param>
		/// <param name="sourceInterface4">The <see cref="T:System.Type" /> of a source interface. </param>
		// Token: 0x060052CD RID: 21197 RVA: 0x00118910 File Offset: 0x00116B10
		public ComSourceInterfacesAttribute(Type sourceInterface1, Type sourceInterface2, Type sourceInterface3, Type sourceInterface4)
		{
			this._val = string.Concat(new string[]
			{
				sourceInterface1.FullName,
				"\0",
				sourceInterface2.FullName,
				"\0",
				sourceInterface3.FullName,
				"\0",
				sourceInterface4.FullName
			});
		}

		/// <summary>Gets the fully qualified name of the event source interface.</summary>
		/// <returns>The fully qualified name of the event source interface.</returns>
		// Token: 0x17000E71 RID: 3697
		// (get) Token: 0x060052CE RID: 21198 RVA: 0x00118971 File Offset: 0x00116B71
		public string Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002A47 RID: 10823
		internal string _val;
	}
}
