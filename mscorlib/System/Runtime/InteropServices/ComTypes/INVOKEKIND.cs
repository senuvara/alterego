﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	/// <summary>Specifies how to invoke a function by <see langword="IDispatch::Invoke" />.</summary>
	// Token: 0x02000980 RID: 2432
	[Flags]
	[Serializable]
	public enum INVOKEKIND
	{
		/// <summary>The member is called using a normal function invocation syntax.</summary>
		// Token: 0x04002CDF RID: 11487
		INVOKE_FUNC = 1,
		/// <summary>The function is invoked using a normal property access syntax.</summary>
		// Token: 0x04002CE0 RID: 11488
		INVOKE_PROPERTYGET = 2,
		/// <summary>The function is invoked using a property value assignment syntax.</summary>
		// Token: 0x04002CE1 RID: 11489
		INVOKE_PROPERTYPUT = 4,
		/// <summary>The function is invoked using a property reference assignment syntax.</summary>
		// Token: 0x04002CE2 RID: 11490
		INVOKE_PROPERTYPUTREF = 8
	}
}
