﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	/// <summary>Defines a function description.</summary>
	// Token: 0x02000972 RID: 2418
	public struct FUNCDESC
	{
		/// <summary>Identifies the function member ID.</summary>
		// Token: 0x04002C99 RID: 11417
		public int memid;

		/// <summary>Stores the count of errors a function can return on a 16-bit system.</summary>
		// Token: 0x04002C9A RID: 11418
		public IntPtr lprgscode;

		/// <summary>Indicates the size of <see cref="F:System.Runtime.InteropServices.FUNCDESC.cParams" />.</summary>
		// Token: 0x04002C9B RID: 11419
		public IntPtr lprgelemdescParam;

		/// <summary>Specifies whether the function is virtual, static, or dispatch-only.</summary>
		// Token: 0x04002C9C RID: 11420
		public FUNCKIND funckind;

		/// <summary>Specifies the type of a property function.</summary>
		// Token: 0x04002C9D RID: 11421
		public INVOKEKIND invkind;

		/// <summary>Specifies the calling convention of a function.</summary>
		// Token: 0x04002C9E RID: 11422
		public CALLCONV callconv;

		/// <summary>Counts the total number of parameters.</summary>
		// Token: 0x04002C9F RID: 11423
		public short cParams;

		/// <summary>Counts the optional parameters.</summary>
		// Token: 0x04002CA0 RID: 11424
		public short cParamsOpt;

		/// <summary>Specifies the offset in the VTBL for <see cref="F:System.Runtime.InteropServices.FUNCKIND.FUNC_VIRTUAL" />.</summary>
		// Token: 0x04002CA1 RID: 11425
		public short oVft;

		/// <summary>Counts the permitted return values.</summary>
		// Token: 0x04002CA2 RID: 11426
		public short cScodes;

		/// <summary>Contains the return type of the function.</summary>
		// Token: 0x04002CA3 RID: 11427
		public ELEMDESC elemdescFunc;

		/// <summary>Indicates the <see cref="T:System.Runtime.InteropServices.FUNCFLAGS" /> of a function.</summary>
		// Token: 0x04002CA4 RID: 11428
		public short wFuncFlags;
	}
}
