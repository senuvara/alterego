﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	/// <summary>Describes a variable, constant, or data member.</summary>
	// Token: 0x0200097B RID: 2427
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct VARDESC
	{
		/// <summary>Indicates the member ID of a variable.</summary>
		// Token: 0x04002CC3 RID: 11459
		public int memid;

		/// <summary>This field is reserved for future use.</summary>
		// Token: 0x04002CC4 RID: 11460
		public string lpstrSchema;

		/// <summary>Contains information about a variable.</summary>
		// Token: 0x04002CC5 RID: 11461
		public VARDESC.DESCUNION desc;

		/// <summary>Contains the variable type.</summary>
		// Token: 0x04002CC6 RID: 11462
		public ELEMDESC elemdescVar;

		/// <summary>Defines the properties of a variable.</summary>
		// Token: 0x04002CC7 RID: 11463
		public short wVarFlags;

		/// <summary>Defines how to marshal a variable.</summary>
		// Token: 0x04002CC8 RID: 11464
		public VARKIND varkind;

		/// <summary>Contains information about a variable.</summary>
		// Token: 0x0200097C RID: 2428
		[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode)]
		public struct DESCUNION
		{
			/// <summary>Indicates the offset of this variable within the instance.</summary>
			// Token: 0x04002CC9 RID: 11465
			[FieldOffset(0)]
			public int oInst;

			/// <summary>Describes a symbolic constant.</summary>
			// Token: 0x04002CCA RID: 11466
			[FieldOffset(0)]
			public IntPtr lpvarValue;
		}
	}
}
