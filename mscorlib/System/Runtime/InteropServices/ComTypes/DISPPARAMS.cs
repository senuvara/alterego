﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	/// <summary>Contains the arguments passed to a method or property by <see langword="IDispatch::Invoke" />.</summary>
	// Token: 0x0200097D RID: 2429
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct DISPPARAMS
	{
		/// <summary>Represents a reference to the array of arguments.</summary>
		// Token: 0x04002CCB RID: 11467
		public IntPtr rgvarg;

		/// <summary>Represents the dispatch IDs of named arguments.</summary>
		// Token: 0x04002CCC RID: 11468
		public IntPtr rgdispidNamedArgs;

		/// <summary>Represents the count of arguments.</summary>
		// Token: 0x04002CCD RID: 11469
		public int cArgs;

		/// <summary>Represents the count of named arguments </summary>
		// Token: 0x04002CCE RID: 11470
		public int cNamedArgs;
	}
}
