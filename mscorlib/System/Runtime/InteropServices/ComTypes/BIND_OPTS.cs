﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	/// <summary>Stores the parameters that are used during a moniker binding operation.</summary>
	// Token: 0x0200095B RID: 2395
	public struct BIND_OPTS
	{
		/// <summary>Specifies the size, in bytes, of the <see langword="BIND_OPTS" /> structure.</summary>
		// Token: 0x04002C4A RID: 11338
		public int cbStruct;

		/// <summary>Controls aspects of moniker binding operations.</summary>
		// Token: 0x04002C4B RID: 11339
		public int grfFlags;

		/// <summary>Represents flags that should be used when opening the file that contains the object identified by the moniker.</summary>
		// Token: 0x04002C4C RID: 11340
		public int grfMode;

		/// <summary>Indicates the amount of time (clock time in milliseconds, as returned by the <see langword="GetTickCount" /> function) that the caller specified to complete the binding operation.</summary>
		// Token: 0x04002C4D RID: 11341
		public int dwTickCountDeadline;
	}
}
