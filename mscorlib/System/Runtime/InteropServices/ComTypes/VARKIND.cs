﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	/// <summary>Defines the kind of variable.</summary>
	// Token: 0x0200097A RID: 2426
	[Serializable]
	public enum VARKIND
	{
		/// <summary>The variable is a field or member of the type. It exists at a fixed offset within each instance of the type.</summary>
		// Token: 0x04002CBF RID: 11455
		VAR_PERINSTANCE,
		/// <summary>There is only one instance of the variable.</summary>
		// Token: 0x04002CC0 RID: 11456
		VAR_STATIC,
		/// <summary>The <see langword="VARDESC" /> structure describes a symbolic constant. There is no memory associated with it.</summary>
		// Token: 0x04002CC1 RID: 11457
		VAR_CONST,
		/// <summary>The variable can be accessed only through <see langword="IDispatch::Invoke" />.</summary>
		// Token: 0x04002CC2 RID: 11458
		VAR_DISPATCH
	}
}
