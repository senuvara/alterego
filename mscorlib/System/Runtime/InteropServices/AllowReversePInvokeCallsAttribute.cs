﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Allows an unmanaged method to call a managed method.</summary>
	// Token: 0x020008B4 RID: 2228
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public sealed class AllowReversePInvokeCallsAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.AllowReversePInvokeCallsAttribute" /> class.</summary>
		// Token: 0x060052AF RID: 21167 RVA: 0x000020BF File Offset: 0x000002BF
		public AllowReversePInvokeCallsAttribute()
		{
		}
	}
}
