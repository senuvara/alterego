﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Supplies an explicit <see cref="T:System.Guid" /> when an automatic GUID is undesirable.</summary>
	// Token: 0x020008CF RID: 2255
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false)]
	[ComVisible(true)]
	public sealed class GuidAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.GuidAttribute" /> class with the specified GUID.</summary>
		/// <param name="guid">The <see cref="T:System.Guid" /> to be assigned. </param>
		// Token: 0x060052DC RID: 21212 RVA: 0x001189E6 File Offset: 0x00116BE6
		public GuidAttribute(string guid)
		{
			this._val = guid;
		}

		/// <summary>Gets the <see cref="T:System.Guid" /> of the class.</summary>
		/// <returns>The <see cref="T:System.Guid" /> of the class.</returns>
		// Token: 0x17000E75 RID: 3701
		// (get) Token: 0x060052DD RID: 21213 RVA: 0x001189F5 File Offset: 0x00116BF5
		public string Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04002ACA RID: 10954
		internal string _val;
	}
}
