﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Specifies the paths that are used to search for DLLs that provide functions for platform invokes. </summary>
	// Token: 0x020008D5 RID: 2261
	[ComVisible(false)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Method, AllowMultiple = false)]
	public sealed class DefaultDllImportSearchPathsAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.DefaultDllImportSearchPathsAttribute" /> class, specifying the paths to use when searching for the targets of platform invokes. </summary>
		/// <param name="paths">A bitwise combination of enumeration values that specify the paths that the LoadLibraryEx function searches during platform invokes. </param>
		// Token: 0x060052EA RID: 21226 RVA: 0x00118A70 File Offset: 0x00116C70
		public DefaultDllImportSearchPathsAttribute(DllImportSearchPath paths)
		{
			this._paths = paths;
		}

		/// <summary>Gets a bitwise combination of enumeration values that specify the paths that the LoadLibraryEx function searches during platform invokes. </summary>
		/// <returns>A bitwise combination of enumeration values that specify search paths for platform invokes. </returns>
		// Token: 0x17000E76 RID: 3702
		// (get) Token: 0x060052EB RID: 21227 RVA: 0x00118A7F File Offset: 0x00116C7F
		public DllImportSearchPath Paths
		{
			get
			{
				return this._paths;
			}
		}

		// Token: 0x04002AD3 RID: 10963
		internal DllImportSearchPath _paths;
	}
}
