﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Describes the type of a COM member.</summary>
	// Token: 0x020008E8 RID: 2280
	[ComVisible(true)]
	[Serializable]
	public enum ComMemberType
	{
		/// <summary>The member is a normal method.</summary>
		// Token: 0x04002B02 RID: 11010
		Method,
		/// <summary>The member gets properties.</summary>
		// Token: 0x04002B03 RID: 11011
		PropGet,
		/// <summary>The member sets properties.</summary>
		// Token: 0x04002B04 RID: 11012
		PropSet
	}
}
