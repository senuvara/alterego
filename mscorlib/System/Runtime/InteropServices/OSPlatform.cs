﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime.InteropServices
{
	/// <summary>Represents an operating system platform.</summary>
	// Token: 0x020008AF RID: 2223
	public struct OSPlatform : IEquatable<OSPlatform>
	{
		/// <summary>Gets an object that represents the Linux operating system. </summary>
		/// <returns>An object that represents the Linux operating system. </returns>
		// Token: 0x17000E61 RID: 3681
		// (get) Token: 0x06005292 RID: 21138 RVA: 0x0011858C File Offset: 0x0011678C
		public static OSPlatform Linux
		{
			[CompilerGenerated]
			get
			{
				return OSPlatform.<Linux>k__BackingField;
			}
		} = new OSPlatform("LINUX");

		/// <summary>Gets an object that represents the OSX operating system. </summary>
		/// <returns>An object that represents the OSX operating system. </returns>
		// Token: 0x17000E62 RID: 3682
		// (get) Token: 0x06005293 RID: 21139 RVA: 0x00118593 File Offset: 0x00116793
		public static OSPlatform OSX
		{
			[CompilerGenerated]
			get
			{
				return OSPlatform.<OSX>k__BackingField;
			}
		} = new OSPlatform("OSX");

		/// <summary>Gets an object that represents the Windows operating system.</summary>
		/// <returns>An object that represents the Windows operating system. </returns>
		// Token: 0x17000E63 RID: 3683
		// (get) Token: 0x06005294 RID: 21140 RVA: 0x0011859A File Offset: 0x0011679A
		public static OSPlatform Windows
		{
			[CompilerGenerated]
			get
			{
				return OSPlatform.<Windows>k__BackingField;
			}
		} = new OSPlatform("WINDOWS");

		// Token: 0x06005295 RID: 21141 RVA: 0x001185A1 File Offset: 0x001167A1
		private OSPlatform(string osPlatform)
		{
			if (osPlatform == null)
			{
				throw new ArgumentNullException("osPlatform");
			}
			if (osPlatform.Length == 0)
			{
				throw new ArgumentException("Value cannot be empty.", "osPlatform");
			}
			this._osPlatform = osPlatform;
		}

		/// <summary>Creates a new <see cref="T:System.Runtime.InteropServices.OSPlatform" /> instance. </summary>
		/// <param name="osPlatform">The name of the platform that this instance represents. </param>
		/// <returns>An object that represents the <paramref name="osPlatform" /> operating system. </returns>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="osPlatform" /> is an empty string. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="osPlatform" /> is <see langword="null" />. </exception>
		// Token: 0x06005296 RID: 21142 RVA: 0x001185D0 File Offset: 0x001167D0
		public static OSPlatform Create(string osPlatform)
		{
			return new OSPlatform(osPlatform);
		}

		/// <summary>Determines whether the current instance and the specified <see cref="T:System.Runtime.InteropServices.OSPlatform" /> instance are equal. </summary>
		/// <param name="other">The object to compare with the current instance. </param>
		/// <returns>
		///   <see langword="true" /> if the current instance and <paramref name="other" /> are equal; otherwise, <see langword="false" />. </returns>
		// Token: 0x06005297 RID: 21143 RVA: 0x001185D8 File Offset: 0x001167D8
		public bool Equals(OSPlatform other)
		{
			return this.Equals(other._osPlatform);
		}

		// Token: 0x06005298 RID: 21144 RVA: 0x001185E6 File Offset: 0x001167E6
		internal bool Equals(string other)
		{
			return string.Equals(this._osPlatform, other, StringComparison.Ordinal);
		}

		/// <summary>Determines whether the current <see cref="T:System.Runtime.InteropServices.OSPlatform" /> instance is equal to the specified object. </summary>
		/// <param name="obj">
		///   <see langword="true" /> if <paramref name="obj" /> is a <see cref="T:System.Runtime.InteropServices.OSPlatform" /> instance and its name is the same as the current object; otherwise, <paramref name="false" />. </param>
		/// <returns>
		///   <see langword="true" /> if <paramref name="obj" /> is a <see cref="T:System.Runtime.InteropServices.OSPlatform" /> instance and its name is the same as the current object.</returns>
		// Token: 0x06005299 RID: 21145 RVA: 0x001185F5 File Offset: 0x001167F5
		public override bool Equals(object obj)
		{
			return obj is OSPlatform && this.Equals((OSPlatform)obj);
		}

		/// <summary>Returns the hash code for this instance. </summary>
		/// <returns>The hash code for this instance. </returns>
		// Token: 0x0600529A RID: 21146 RVA: 0x0011860D File Offset: 0x0011680D
		public override int GetHashCode()
		{
			if (this._osPlatform != null)
			{
				return this._osPlatform.GetHashCode();
			}
			return 0;
		}

		/// <summary>Returns the string representation of this <see cref="T:System.Runtime.InteropServices.OSPlatform" /> instance. </summary>
		/// <returns>A string that represents this <see cref="T:System.Runtime.InteropServices.OSPlatform" /> instance.  </returns>
		// Token: 0x0600529B RID: 21147 RVA: 0x00118624 File Offset: 0x00116824
		public override string ToString()
		{
			return this._osPlatform ?? string.Empty;
		}

		/// <summary>Determines whether two <see cref="T:System.Runtime.InteropServices.OSPlatform" /> objects are equal. </summary>
		/// <param name="left">The first object to compare. </param>
		/// <param name="right">The second object to compare. </param>
		/// <returns>
		///   <see langword="true" /> if <paramref name="left" /> and <paramref name="right" /> are equal; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600529C RID: 21148 RVA: 0x00118635 File Offset: 0x00116835
		public static bool operator ==(OSPlatform left, OSPlatform right)
		{
			return left.Equals(right);
		}

		/// <summary>Determines whether two <see cref="T:System.Runtime.InteropServices.OSPlatform" /> instances are unequal. </summary>
		/// <param name="left">The first object to compare. </param>
		/// <param name="right">The second object to compare. </param>
		/// <returns>
		///   <see langword="true" /> if <paramref name="left" /> and <paramref name="right" /> are unequal; otherwise, <see langword="false" />. </returns>
		// Token: 0x0600529D RID: 21149 RVA: 0x0011863F File Offset: 0x0011683F
		public static bool operator !=(OSPlatform left, OSPlatform right)
		{
			return !(left == right);
		}

		// Token: 0x0600529E RID: 21150 RVA: 0x0011864B File Offset: 0x0011684B
		// Note: this type is marked as 'beforefieldinit'.
		static OSPlatform()
		{
		}

		// Token: 0x04002A20 RID: 10784
		private readonly string _osPlatform;

		// Token: 0x04002A21 RID: 10785
		[CompilerGenerated]
		private static readonly OSPlatform <Linux>k__BackingField;

		// Token: 0x04002A22 RID: 10786
		[CompilerGenerated]
		private static readonly OSPlatform <OSX>k__BackingField;

		// Token: 0x04002A23 RID: 10787
		[CompilerGenerated]
		private static readonly OSPlatform <Windows>k__BackingField;
	}
}
