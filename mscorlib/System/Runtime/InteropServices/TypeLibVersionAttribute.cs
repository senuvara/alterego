﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Specifies the version number of an exported type library.</summary>
	// Token: 0x020008DE RID: 2270
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class TypeLibVersionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.TypeLibVersionAttribute" /> class with the major and minor version numbers of the type library.</summary>
		/// <param name="major">The major version number of the type library. </param>
		/// <param name="minor">The minor version number of the type library. </param>
		// Token: 0x06005307 RID: 21255 RVA: 0x00118DF1 File Offset: 0x00116FF1
		public TypeLibVersionAttribute(int major, int minor)
		{
			this._major = major;
			this._minor = minor;
		}

		/// <summary>Gets the major version number of the type library.</summary>
		/// <returns>The major version number of the type library.</returns>
		// Token: 0x17000E81 RID: 3713
		// (get) Token: 0x06005308 RID: 21256 RVA: 0x00118E07 File Offset: 0x00117007
		public int MajorVersion
		{
			get
			{
				return this._major;
			}
		}

		/// <summary>Gets the minor version number of the type library.</summary>
		/// <returns>The minor version number of the type library.</returns>
		// Token: 0x17000E82 RID: 3714
		// (get) Token: 0x06005309 RID: 21257 RVA: 0x00118E0F File Offset: 0x0011700F
		public int MinorVersion
		{
			get
			{
				return this._minor;
			}
		}

		// Token: 0x04002AEA RID: 10986
		internal int _major;

		// Token: 0x04002AEB RID: 10987
		internal int _minor;
	}
}
