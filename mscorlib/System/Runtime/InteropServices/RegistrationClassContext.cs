﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Specifies the set of execution contexts in which a class object will be made available for requests to construct instances.</summary>
	// Token: 0x02000928 RID: 2344
	[Flags]
	public enum RegistrationClassContext
	{
		/// <summary>Disables activate-as-activator (AAA) activations for this activation only.</summary>
		// Token: 0x04002BF4 RID: 11252
		DisableActivateAsActivator = 32768,
		/// <summary>Enables activate-as-activator (AAA) activations for this activation only.</summary>
		// Token: 0x04002BF5 RID: 11253
		EnableActivateAsActivator = 65536,
		/// <summary>Allows the downloading of code from the Directory Service or the Internet.</summary>
		// Token: 0x04002BF6 RID: 11254
		EnableCodeDownload = 8192,
		/// <summary>Begin this activation from the default context of the current apartment.</summary>
		// Token: 0x04002BF7 RID: 11255
		FromDefaultContext = 131072,
		/// <summary>The code that manages objects of this class is an in-process handler.</summary>
		// Token: 0x04002BF8 RID: 11256
		InProcessHandler = 2,
		/// <summary>Not used.</summary>
		// Token: 0x04002BF9 RID: 11257
		InProcessHandler16 = 32,
		/// <summary>The code that creates and manages objects of this class is a DLL that runs in the same process as the caller of the function specifying the class context.</summary>
		// Token: 0x04002BFA RID: 11258
		InProcessServer = 1,
		/// <summary>Not used.</summary>
		// Token: 0x04002BFB RID: 11259
		InProcessServer16 = 8,
		/// <summary>The EXE code that creates and manages objects of this class runs on same machine but is loaded in a separate process space.</summary>
		// Token: 0x04002BFC RID: 11260
		LocalServer = 4,
		/// <summary>Disallows the downloading of code from the Directory Service or the Internet.</summary>
		// Token: 0x04002BFD RID: 11261
		NoCodeDownload = 1024,
		/// <summary>Specifies whether activation fails if it uses custom marshaling.</summary>
		// Token: 0x04002BFE RID: 11262
		NoCustomMarshal = 4096,
		/// <summary>Overrides the logging of failures.</summary>
		// Token: 0x04002BFF RID: 11263
		NoFailureLog = 16384,
		/// <summary>A remote machine context.</summary>
		// Token: 0x04002C00 RID: 11264
		RemoteServer = 16,
		/// <summary>Not used.</summary>
		// Token: 0x04002C01 RID: 11265
		Reserved1 = 64,
		/// <summary>Not used.</summary>
		// Token: 0x04002C02 RID: 11266
		Reserved2 = 128,
		/// <summary>Not used.</summary>
		// Token: 0x04002C03 RID: 11267
		Reserved3 = 256,
		/// <summary>Not used.</summary>
		// Token: 0x04002C04 RID: 11268
		Reserved4 = 512,
		/// <summary>Not used.</summary>
		// Token: 0x04002C05 RID: 11269
		Reserved5 = 2048
	}
}
