﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Wraps objects the marshaler should marshal as a <see langword="VT_UNKNOWN" />.</summary>
	// Token: 0x02000917 RID: 2327
	[ComVisible(true)]
	[Serializable]
	public sealed class UnknownWrapper
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.UnknownWrapper" /> class with the object to be wrapped.</summary>
		/// <param name="obj">The object being wrapped. </param>
		// Token: 0x0600538A RID: 21386 RVA: 0x001196B7 File Offset: 0x001178B7
		public UnknownWrapper(object obj)
		{
			this.m_WrappedObject = obj;
		}

		/// <summary>Gets the object contained by this wrapper.</summary>
		/// <returns>The wrapped object.</returns>
		// Token: 0x17000E97 RID: 3735
		// (get) Token: 0x0600538B RID: 21387 RVA: 0x001196C6 File Offset: 0x001178C6
		public object WrappedObject
		{
			get
			{
				return this.m_WrappedObject;
			}
		}

		// Token: 0x04002BBE RID: 11198
		private object m_WrappedObject;
	}
}
