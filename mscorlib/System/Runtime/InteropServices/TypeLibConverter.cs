﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Runtime.InteropServices
{
	/// <summary>Provides a set of services that convert a managed assembly to a COM type library and vice versa.</summary>
	// Token: 0x02000AA0 RID: 2720
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[Guid("F1C3BF79-C3E4-11d3-88E7-00902754C43A")]
	public sealed class TypeLibConverter : ITypeLibConverter
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.TypeLibConverter" /> class. </summary>
		// Token: 0x06005EE7 RID: 24295 RVA: 0x00002ABD File Offset: 0x00000CBD
		public TypeLibConverter()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Converts an assembly to a COM type library.</summary>
		/// <param name="assembly">The assembly to convert. </param>
		/// <param name="strTypeLibName">The file name of the resulting type library. </param>
		/// <param name="flags">A <see cref="T:System.Runtime.InteropServices.TypeLibExporterFlags" /> value indicating any special settings. </param>
		/// <param name="notifySink">The <see cref="T:System.Runtime.InteropServices.ITypeLibExporterNotifySink" /> interface implemented by the caller. </param>
		/// <returns>An object that implements the <see langword="ITypeLib" /> interface.</returns>
		// Token: 0x06005EE8 RID: 24296 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		[return: MarshalAs(UnmanagedType.Interface)]
		public object ConvertAssemblyToTypeLib(Assembly assembly, string strTypeLibName, TypeLibExporterFlags flags, ITypeLibExporterNotifySink notifySink)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Converts a COM type library to an assembly.</summary>
		/// <param name="typeLib">The object that implements the <see langword="ITypeLib" /> interface. </param>
		/// <param name="asmFileName">The file name of the resulting assembly. </param>
		/// <param name="flags">A <see cref="T:System.Runtime.InteropServices.TypeLibImporterFlags" /> value indicating any special settings. </param>
		/// <param name="notifySink">
		///       <see cref="T:System.Runtime.InteropServices.ITypeLibImporterNotifySink" /> interface implemented by the caller. </param>
		/// <param name="publicKey">A <see langword="byte" /> array containing the public key. </param>
		/// <param name="keyPair">A <see cref="T:System.Reflection.StrongNameKeyPair" /> object containing the public and private cryptographic key pair. </param>
		/// <param name="unsafeInterfaces">If <see langword="true" />, the interfaces require link time checks for <see cref="F:System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode" /> permission. If <see langword="false" />, the interfaces require run time checks that require a stack walk and are more expensive, but help provide greater protection. </param>
		/// <returns>An <see cref="T:System.Reflection.Emit.AssemblyBuilder" /> object containing the converted type library.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="typeLib" /> is <see langword="null" />.-or- 
		///         <paramref name="asmFileName" /> is <see langword="null" />.-or- 
		///         <paramref name="notifySink" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asmFileName" /> is an empty string.-or- 
		///         <paramref name="asmFileName" /> is longer than MAX_PATH. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="flags" /> is not <see cref="F:System.Runtime.InteropServices.TypeLibImporterFlags.PrimaryInteropAssembly" />.-or- 
		///         <paramref name="publicKey" /> and <paramref name="keyPair" /> are <see langword="null" />. </exception>
		/// <exception cref="T:System.Reflection.ReflectionTypeLoadException">The metadata produced has errors preventing any types from loading. </exception>
		// Token: 0x06005EE9 RID: 24297 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public AssemblyBuilder ConvertTypeLibToAssembly([MarshalAs(UnmanagedType.Interface)] object typeLib, string asmFileName, int flags, ITypeLibImporterNotifySink notifySink, byte[] publicKey, StrongNameKeyPair keyPair, bool unsafeInterfaces)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Converts a COM type library to an assembly.</summary>
		/// <param name="typeLib">The object that implements the <see langword="ITypeLib" /> interface. </param>
		/// <param name="asmFileName">The file name of the resulting assembly. </param>
		/// <param name="flags">A <see cref="T:System.Runtime.InteropServices.TypeLibImporterFlags" /> value indicating any special settings. </param>
		/// <param name="notifySink">
		///       <see cref="T:System.Runtime.InteropServices.ITypeLibImporterNotifySink" /> interface implemented by the caller. </param>
		/// <param name="publicKey">A <see langword="byte" /> array containing the public key. </param>
		/// <param name="keyPair">A <see cref="T:System.Reflection.StrongNameKeyPair" /> object containing the public and private cryptographic key pair. </param>
		/// <param name="asmNamespace">The namespace for the resulting assembly. </param>
		/// <param name="asmVersion">The version of the resulting assembly. If <see langword="null" />, the version of the type library is used. </param>
		/// <returns>An <see cref="T:System.Reflection.Emit.AssemblyBuilder" /> object containing the converted type library.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="typeLib" /> is <see langword="null" />.-or- 
		///         <paramref name="asmFileName" /> is <see langword="null" />.-or- 
		///         <paramref name="notifySink" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="asmFileName" /> is an empty string.-or- 
		///         <paramref name="asmFileName" /> is longer than MAX_PATH. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="flags" /> is not <see cref="F:System.Runtime.InteropServices.TypeLibImporterFlags.PrimaryInteropAssembly" />.-or- 
		///         <paramref name="publicKey" /> and <paramref name="keyPair" /> are <see langword="null" />. </exception>
		/// <exception cref="T:System.Reflection.ReflectionTypeLoadException">The metadata produced has errors preventing any types from loading. </exception>
		// Token: 0x06005EEA RID: 24298 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public AssemblyBuilder ConvertTypeLibToAssembly([MarshalAs(UnmanagedType.Interface)] object typeLib, string asmFileName, TypeLibImporterFlags flags, ITypeLibImporterNotifySink notifySink, byte[] publicKey, StrongNameKeyPair keyPair, string asmNamespace, Version asmVersion)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the name and code base of a primary interop assembly for a specified type library.</summary>
		/// <param name="g">The GUID of the type library. </param>
		/// <param name="major">The major version number of the type library. </param>
		/// <param name="minor">The minor version number of the type library. </param>
		/// <param name="lcid">The LCID of the type library. </param>
		/// <param name="asmName">On successful return, the name of the primary interop assembly associated with <paramref name="g" />. </param>
		/// <param name="asmCodeBase">On successful return, the code base of the primary interop assembly associated with <paramref name="g" />. </param>
		/// <returns>
		///     <see langword="true" /> if the primary interop assembly was found in the registry; otherwise <see langword="false" />.</returns>
		// Token: 0x06005EEB RID: 24299 RVA: 0x00130554 File Offset: 0x0012E754
		public bool GetPrimaryInteropAssembly(Guid g, int major, int minor, int lcid, out string asmName, out string asmCodeBase)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
