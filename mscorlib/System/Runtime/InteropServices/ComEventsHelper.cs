﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Provides methods that enable .NET Framework delegates that handle events to be added and removed from COM objects.</summary>
	// Token: 0x0200091C RID: 2332
	public static class ComEventsHelper
	{
		/// <summary>Adds a delegate to the invocation list of events originating from a COM object.</summary>
		/// <param name="rcw">The COM object that triggers the events the caller would like to respond to.</param>
		/// <param name="iid">The identifier of the source interface used by the COM object to trigger events. </param>
		/// <param name="dispid">The dispatch identifier of the method on the source interface.</param>
		/// <param name="d">The delegate to invoke when the COM event is fired.</param>
		// Token: 0x06005393 RID: 21395 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static void Combine(object rcw, Guid iid, int dispid, Delegate d)
		{
			throw new NotImplementedException();
		}

		/// <summary>Removes a delegate from the invocation list of events originating from a COM object.</summary>
		/// <param name="rcw">The COM object the delegate is attached to.</param>
		/// <param name="iid">The identifier of the source interface used by the COM object to trigger events. </param>
		/// <param name="dispid">The dispatch identifier of the method on the source interface.</param>
		/// <param name="d">The delegate to remove from the invocation list.</param>
		/// <returns>The delegate that was removed from the invocation list.</returns>
		// Token: 0x06005394 RID: 21396 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static Delegate Remove(object rcw, Guid iid, int dispid, Delegate d)
		{
			throw new NotImplementedException();
		}
	}
}
