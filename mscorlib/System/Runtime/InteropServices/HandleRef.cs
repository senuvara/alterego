﻿using System;

namespace System.Runtime.InteropServices
{
	/// <summary>Wraps a managed object holding a handle to a resource that is passed to unmanaged code using platform invoke.</summary>
	// Token: 0x020008EE RID: 2286
	[ComVisible(true)]
	public struct HandleRef
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Runtime.InteropServices.HandleRef" /> class with the object to wrap and a handle to the resource used by unmanaged code.</summary>
		/// <param name="wrapper">A managed object that should not be finalized until the platform invoke call returns. </param>
		/// <param name="handle">An <see cref="T:System.IntPtr" /> that indicates a handle to a resource. </param>
		// Token: 0x0600533E RID: 21310 RVA: 0x00119253 File Offset: 0x00117453
		public HandleRef(object wrapper, IntPtr handle)
		{
			this.m_wrapper = wrapper;
			this.m_handle = handle;
		}

		/// <summary>Gets the object holding the handle to a resource.</summary>
		/// <returns>The object holding the handle to a resource.</returns>
		// Token: 0x17000E92 RID: 3730
		// (get) Token: 0x0600533F RID: 21311 RVA: 0x00119263 File Offset: 0x00117463
		public object Wrapper
		{
			get
			{
				return this.m_wrapper;
			}
		}

		/// <summary>Gets the handle to a resource.</summary>
		/// <returns>The handle to a resource.</returns>
		// Token: 0x17000E93 RID: 3731
		// (get) Token: 0x06005340 RID: 21312 RVA: 0x0011926B File Offset: 0x0011746B
		public IntPtr Handle
		{
			get
			{
				return this.m_handle;
			}
		}

		/// <summary>Returns the handle to a resource of the specified <see cref="T:System.Runtime.InteropServices.HandleRef" /> object.</summary>
		/// <param name="value">The object that needs a handle. </param>
		/// <returns>The handle to a resource of the specified <see cref="T:System.Runtime.InteropServices.HandleRef" /> object.</returns>
		// Token: 0x06005341 RID: 21313 RVA: 0x0011926B File Offset: 0x0011746B
		public static explicit operator IntPtr(HandleRef value)
		{
			return value.m_handle;
		}

		/// <summary>Returns the internal integer representation of a <see cref="T:System.Runtime.InteropServices.HandleRef" /> object.</summary>
		/// <param name="value">A <see cref="T:System.Runtime.InteropServices.HandleRef" /> object to retrieve an internal integer representation from.</param>
		/// <returns>An <see cref="T:System.IntPtr" /> object that represents a <see cref="T:System.Runtime.InteropServices.HandleRef" /> object.</returns>
		// Token: 0x06005342 RID: 21314 RVA: 0x0011926B File Offset: 0x0011746B
		public static IntPtr ToIntPtr(HandleRef value)
		{
			return value.m_handle;
		}

		// Token: 0x04002B0A RID: 11018
		internal object m_wrapper;

		// Token: 0x04002B0B RID: 11019
		internal IntPtr m_handle;
	}
}
