﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>The exception that is thrown when there is an attempt to dynamically access a field that does not exist. If a field in a class library has been removed or renamed, recompile any assemblies that reference that library.</summary>
	// Token: 0x0200018C RID: 396
	[ComVisible(true)]
	[Serializable]
	public class MissingFieldException : MissingMemberException, ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.MissingFieldException" /> class.</summary>
		// Token: 0x0600113B RID: 4411 RVA: 0x000479B3 File Offset: 0x00045BB3
		public MissingFieldException() : base(Environment.GetResourceString("Attempted to access a non-existing field."))
		{
			base.SetErrorCode(-2146233071);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingFieldException" /> class with a specified error message.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. </param>
		// Token: 0x0600113C RID: 4412 RVA: 0x000479D0 File Offset: 0x00045BD0
		public MissingFieldException(string message) : base(message)
		{
			base.SetErrorCode(-2146233071);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingFieldException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x0600113D RID: 4413 RVA: 0x000479E4 File Offset: 0x00045BE4
		public MissingFieldException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233071);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingFieldException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x0600113E RID: 4414 RVA: 0x000479F9 File Offset: 0x00045BF9
		protected MissingFieldException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>Gets the text string showing the signature of the missing field, the class name, and the field name. This property is read-only.</summary>
		/// <returns>The error message string.</returns>
		// Token: 0x17000201 RID: 513
		// (get) Token: 0x0600113F RID: 4415 RVA: 0x00047A04 File Offset: 0x00045C04
		public override string Message
		{
			[SecuritySafeCritical]
			get
			{
				if (this.ClassName == null)
				{
					return base.Message;
				}
				return Environment.GetResourceString("Field '{0}' not found.", new object[]
				{
					((this.Signature != null) ? (MissingMemberException.FormatSignature(this.Signature) + " ") : "") + this.ClassName + "." + this.MemberName
				});
			}
		}

		// Token: 0x06001140 RID: 4416 RVA: 0x00047A6D File Offset: 0x00045C6D
		private MissingFieldException(string className, string fieldName, byte[] signature)
		{
			this.ClassName = className;
			this.MemberName = fieldName;
			this.Signature = signature;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingFieldException" /> class with the specified class name and field name.</summary>
		/// <param name="className">The name of the class in which access to a nonexistent field was attempted. </param>
		/// <param name="fieldName">The name of the field that cannot be accessed. </param>
		// Token: 0x06001141 RID: 4417 RVA: 0x00047A8A File Offset: 0x00045C8A
		public MissingFieldException(string className, string fieldName)
		{
			this.ClassName = className;
			this.MemberName = fieldName;
		}
	}
}
