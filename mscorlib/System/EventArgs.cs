﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents the base class for classes that contain event data, and provides a value to use for events that do not include event data. </summary>
	// Token: 0x02000149 RID: 329
	[ComVisible(true)]
	[Serializable]
	public class EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.EventArgs" /> class.</summary>
		// Token: 0x06000EB7 RID: 3767 RVA: 0x00002050 File Offset: 0x00000250
		public EventArgs()
		{
		}

		// Token: 0x06000EB8 RID: 3768 RVA: 0x0003DB2B File Offset: 0x0003BD2B
		// Note: this type is marked as 'beforefieldinit'.
		static EventArgs()
		{
		}

		/// <summary>Provides a value to use with events that do not have event data.</summary>
		// Token: 0x040008CA RID: 2250
		public static readonly EventArgs Empty = new EventArgs();
	}
}
