﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Used by <see cref="M:System.AppDomain.DoCallBack(System.CrossAppDomainDelegate)" /> for cross-application domain calls.</summary>
	// Token: 0x020001FA RID: 506
	// (Invoke) Token: 0x0600189B RID: 6299
	[ComVisible(true)]
	public delegate void CrossAppDomainDelegate();
}
