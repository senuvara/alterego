﻿using System;

namespace System
{
	// Token: 0x02000162 RID: 354
	internal struct DateTimeToken
	{
		// Token: 0x04000963 RID: 2403
		internal DateTimeParse.DTT dtt;

		// Token: 0x04000964 RID: 2404
		internal TokenType suffix;

		// Token: 0x04000965 RID: 2405
		internal int num;
	}
}
