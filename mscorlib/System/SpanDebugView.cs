﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x020000BD RID: 189
	internal sealed class SpanDebugView<T>
	{
		// Token: 0x06000680 RID: 1664 RVA: 0x00022E73 File Offset: 0x00021073
		public SpanDebugView(Span<T> collection)
		{
			this._pinnable = (T[])collection.Pinnable;
			this._byteOffset = collection.ByteOffset;
			this._length = collection.Length;
		}

		// Token: 0x06000681 RID: 1665 RVA: 0x00022EA7 File Offset: 0x000210A7
		public SpanDebugView(ReadOnlySpan<T> collection)
		{
			this._pinnable = (T[])collection.Pinnable;
			this._byteOffset = collection.ByteOffset;
			this._length = collection.Length;
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000682 RID: 1666 RVA: 0x00022EDC File Offset: 0x000210DC
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public unsafe T[] Items
		{
			get
			{
				int num = typeof(T).GetTypeInfo().IsValueType ? Unsafe.SizeOf<T>() : IntPtr.Size;
				T[] array = new T[this._length];
				if (this._pinnable == null)
				{
					byte* ptr = (byte*)this._byteOffset.ToPointer();
					for (int i = 0; i < array.Length; i++)
					{
						array[i] = Unsafe.Read<T>((void*)ptr);
						ptr += num;
					}
				}
				else
				{
					long num2 = this._byteOffset.ToInt64();
					long num3 = SpanHelpers.PerTypeValues<T>.ArrayAdjustment.ToInt64();
					int sourceIndex = (int)((num2 - num3) / (long)num);
					Array.Copy(this._pinnable, sourceIndex, array, 0, this._length);
				}
				return array;
			}
		}

		// Token: 0x0400066E RID: 1646
		private readonly T[] _pinnable;

		// Token: 0x0400066F RID: 1647
		private readonly IntPtr _byteOffset;

		// Token: 0x04000670 RID: 1648
		private readonly int _length;
	}
}
