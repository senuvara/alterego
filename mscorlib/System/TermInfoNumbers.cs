﻿using System;

namespace System
{
	// Token: 0x0200022B RID: 555
	internal enum TermInfoNumbers
	{
		// Token: 0x04000D51 RID: 3409
		Columns,
		// Token: 0x04000D52 RID: 3410
		InitTabs,
		// Token: 0x04000D53 RID: 3411
		Lines,
		// Token: 0x04000D54 RID: 3412
		LinesOfMemory,
		// Token: 0x04000D55 RID: 3413
		MagicCookieGlitch,
		// Token: 0x04000D56 RID: 3414
		PaddingBaudRate,
		// Token: 0x04000D57 RID: 3415
		VirtualTerminal,
		// Token: 0x04000D58 RID: 3416
		WidthStatusLine,
		// Token: 0x04000D59 RID: 3417
		NumLabels,
		// Token: 0x04000D5A RID: 3418
		LabelHeight,
		// Token: 0x04000D5B RID: 3419
		LabelWidth,
		// Token: 0x04000D5C RID: 3420
		MaxAttributes,
		// Token: 0x04000D5D RID: 3421
		MaximumWindows,
		// Token: 0x04000D5E RID: 3422
		MaxColors,
		// Token: 0x04000D5F RID: 3423
		MaxPairs,
		// Token: 0x04000D60 RID: 3424
		NoColorVideo,
		// Token: 0x04000D61 RID: 3425
		BufferCapacity,
		// Token: 0x04000D62 RID: 3426
		DotVertSpacing,
		// Token: 0x04000D63 RID: 3427
		DotHorzSpacing,
		// Token: 0x04000D64 RID: 3428
		MaxMicroAddress,
		// Token: 0x04000D65 RID: 3429
		MaxMicroJump,
		// Token: 0x04000D66 RID: 3430
		MicroColSize,
		// Token: 0x04000D67 RID: 3431
		MicroLineSize,
		// Token: 0x04000D68 RID: 3432
		NumberOfPins,
		// Token: 0x04000D69 RID: 3433
		OutputResChar,
		// Token: 0x04000D6A RID: 3434
		OutputResLine,
		// Token: 0x04000D6B RID: 3435
		OutputResHorzInch,
		// Token: 0x04000D6C RID: 3436
		OutputResVertInch,
		// Token: 0x04000D6D RID: 3437
		PrintRate,
		// Token: 0x04000D6E RID: 3438
		WideCharSize,
		// Token: 0x04000D6F RID: 3439
		Buttons,
		// Token: 0x04000D70 RID: 3440
		BitImageEntwining,
		// Token: 0x04000D71 RID: 3441
		BitImageType,
		// Token: 0x04000D72 RID: 3442
		Last
	}
}
