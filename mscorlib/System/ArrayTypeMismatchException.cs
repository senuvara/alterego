﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when an attempt is made to store an element of the wrong type within an array. </summary>
	// Token: 0x0200011B RID: 283
	[ComVisible(true)]
	[Serializable]
	public class ArrayTypeMismatchException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ArrayTypeMismatchException" /> class.</summary>
		// Token: 0x06000A3E RID: 2622 RVA: 0x000329C3 File Offset: 0x00030BC3
		public ArrayTypeMismatchException() : base(Environment.GetResourceString("Attempted to access an element as a type incompatible with the array."))
		{
			base.SetErrorCode(-2146233085);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArrayTypeMismatchException" /> class with a specified error message.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. </param>
		// Token: 0x06000A3F RID: 2623 RVA: 0x000329E0 File Offset: 0x00030BE0
		public ArrayTypeMismatchException(string message) : base(message)
		{
			base.SetErrorCode(-2146233085);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArrayTypeMismatchException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000A40 RID: 2624 RVA: 0x000329F4 File Offset: 0x00030BF4
		public ArrayTypeMismatchException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233085);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArrayTypeMismatchException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06000A41 RID: 2625 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected ArrayTypeMismatchException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
