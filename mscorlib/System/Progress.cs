﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System
{
	/// <summary>Provides an <see cref="T:System.IProgress`1" /> that invokes callbacks for each reported progress value.</summary>
	/// <typeparam name="T">Specifies the type of the progress report value.</typeparam>
	// Token: 0x0200019F RID: 415
	public class Progress<T> : IProgress<T>
	{
		/// <summary>Initializes the <see cref="T:System.Progress`1" /> object.</summary>
		// Token: 0x060011C6 RID: 4550 RVA: 0x00049388 File Offset: 0x00047588
		public Progress()
		{
			this.m_synchronizationContext = (SynchronizationContext.CurrentNoFlow ?? ProgressStatics.DefaultContext);
			this.m_invokeHandlers = new SendOrPostCallback(this.InvokeHandlers);
		}

		/// <summary>Initializes the <see cref="T:System.Progress`1" /> object with the specified callback.</summary>
		/// <param name="handler">A handler to invoke for each reported progress value. This handler will be invoked in addition to any delegates registered with the <see cref="E:System.Progress`1.ProgressChanged" /> event. Depending on the <see cref="T:System.Threading.SynchronizationContext" /> instance captured by the <see cref="T:System.Progress`1" /> at construction, it is possible that this handler instance could be invoked concurrently with itself.</param>
		// Token: 0x060011C7 RID: 4551 RVA: 0x000493B6 File Offset: 0x000475B6
		public Progress(Action<T> handler) : this()
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			this.m_handler = handler;
		}

		/// <summary>Raised for each reported progress value.</summary>
		// Token: 0x1400000B RID: 11
		// (add) Token: 0x060011C8 RID: 4552 RVA: 0x000493D4 File Offset: 0x000475D4
		// (remove) Token: 0x060011C9 RID: 4553 RVA: 0x0004940C File Offset: 0x0004760C
		public event EventHandler<T> ProgressChanged
		{
			[CompilerGenerated]
			add
			{
				EventHandler<T> eventHandler = this.ProgressChanged;
				EventHandler<T> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<T> value2 = (EventHandler<T>)Delegate.Combine(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<T>>(ref this.ProgressChanged, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
			[CompilerGenerated]
			remove
			{
				EventHandler<T> eventHandler = this.ProgressChanged;
				EventHandler<T> eventHandler2;
				do
				{
					eventHandler2 = eventHandler;
					EventHandler<T> value2 = (EventHandler<T>)Delegate.Remove(eventHandler2, value);
					eventHandler = Interlocked.CompareExchange<EventHandler<T>>(ref this.ProgressChanged, value2, eventHandler2);
				}
				while (eventHandler != eventHandler2);
			}
		}

		/// <summary>Reports a progress change.</summary>
		/// <param name="value">The value of the updated progress.</param>
		// Token: 0x060011CA RID: 4554 RVA: 0x00049444 File Offset: 0x00047644
		protected virtual void OnReport(T value)
		{
			bool handler = this.m_handler != null;
			EventHandler<T> progressChanged = this.ProgressChanged;
			if (handler || progressChanged != null)
			{
				this.m_synchronizationContext.Post(this.m_invokeHandlers, value);
			}
		}

		// Token: 0x060011CB RID: 4555 RVA: 0x0004947A File Offset: 0x0004767A
		void IProgress<!0>.Report(T value)
		{
			this.OnReport(value);
		}

		// Token: 0x060011CC RID: 4556 RVA: 0x00049484 File Offset: 0x00047684
		private void InvokeHandlers(object state)
		{
			T t = (T)((object)state);
			Action<T> handler = this.m_handler;
			EventHandler<T> progressChanged = this.ProgressChanged;
			if (handler != null)
			{
				handler(t);
			}
			if (progressChanged != null)
			{
				progressChanged(this, t);
			}
		}

		// Token: 0x04000A1D RID: 2589
		private readonly SynchronizationContext m_synchronizationContext;

		// Token: 0x04000A1E RID: 2590
		private readonly Action<T> m_handler;

		// Token: 0x04000A1F RID: 2591
		private readonly SendOrPostCallback m_invokeHandlers;

		// Token: 0x04000A20 RID: 2592
		[CompilerGenerated]
		private EventHandler<T> ProgressChanged;
	}
}
