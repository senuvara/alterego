﻿using System;

namespace System
{
	// Token: 0x0200024D RID: 589
	internal struct SmallRect
	{
		// Token: 0x06001B56 RID: 6998 RVA: 0x000676BA File Offset: 0x000658BA
		public SmallRect(int left, int top, int right, int bottom)
		{
			this.Left = (short)left;
			this.Top = (short)top;
			this.Right = (short)right;
			this.Bottom = (short)bottom;
		}

		// Token: 0x04000F60 RID: 3936
		public short Left;

		// Token: 0x04000F61 RID: 3937
		public short Top;

		// Token: 0x04000F62 RID: 3938
		public short Right;

		// Token: 0x04000F63 RID: 3939
		public short Bottom;
	}
}
