﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when a program contains invalid Microsoft intermediate language (MSIL) or metadata. Generally this indicates a bug in the compiler that generated the program.</summary>
	// Token: 0x02000182 RID: 386
	[ComVisible(true)]
	[Serializable]
	public sealed class InvalidProgramException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidProgramException" /> class with default properties.</summary>
		// Token: 0x060010D7 RID: 4311 RVA: 0x0004745A File Offset: 0x0004565A
		public InvalidProgramException() : base(Environment.GetResourceString("Common Language Runtime detected an invalid program."))
		{
			base.SetErrorCode(-2146233030);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidProgramException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x060010D8 RID: 4312 RVA: 0x00047477 File Offset: 0x00045677
		public InvalidProgramException(string message) : base(message)
		{
			base.SetErrorCode(-2146233030);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.InvalidProgramException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060010D9 RID: 4313 RVA: 0x0004748B File Offset: 0x0004568B
		public InvalidProgramException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233030);
		}

		// Token: 0x060010DA RID: 4314 RVA: 0x000319C9 File Offset: 0x0002FBC9
		internal InvalidProgramException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
