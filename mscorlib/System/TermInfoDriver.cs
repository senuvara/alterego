﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace System
{
	// Token: 0x02000226 RID: 550
	internal class TermInfoDriver : IConsoleDriver
	{
		// Token: 0x06001A50 RID: 6736 RVA: 0x00062CA4 File Offset: 0x00060EA4
		private static string TryTermInfoDir(string dir, string term)
		{
			string text = string.Format("{0}/{1:x}/{2}", dir, (int)term[0], term);
			if (File.Exists(text))
			{
				return text;
			}
			text = Path.Combine(dir, term.Substring(0, 1), term);
			if (File.Exists(text))
			{
				return text;
			}
			return null;
		}

		// Token: 0x06001A51 RID: 6737 RVA: 0x00062CF0 File Offset: 0x00060EF0
		private static string SearchTerminfo(string term)
		{
			if (term == null || term == string.Empty)
			{
				return null;
			}
			string environmentVariable = Environment.GetEnvironmentVariable("TERMINFO");
			if (environmentVariable != null && Directory.Exists(environmentVariable))
			{
				string text = TermInfoDriver.TryTermInfoDir(environmentVariable, term);
				if (text != null)
				{
					return text;
				}
			}
			foreach (string text2 in TermInfoDriver.locations)
			{
				if (Directory.Exists(text2))
				{
					string text = TermInfoDriver.TryTermInfoDir(text2, term);
					if (text != null)
					{
						return text;
					}
				}
			}
			return null;
		}

		// Token: 0x06001A52 RID: 6738 RVA: 0x00062D63 File Offset: 0x00060F63
		private void WriteConsole(string str)
		{
			if (str == null)
			{
				return;
			}
			this.stdout.InternalWriteString(str);
		}

		// Token: 0x06001A53 RID: 6739 RVA: 0x00062D75 File Offset: 0x00060F75
		public TermInfoDriver() : this(Environment.GetEnvironmentVariable("TERM"))
		{
		}

		// Token: 0x06001A54 RID: 6740 RVA: 0x00062D88 File Offset: 0x00060F88
		public TermInfoDriver(string term)
		{
			this.term = term;
			string text = TermInfoDriver.SearchTerminfo(term);
			if (text != null)
			{
				this.reader = new TermInfoReader(term, text);
			}
			else if (term == "xterm")
			{
				this.reader = new TermInfoReader(term, KnownTerminals.xterm);
			}
			else if (term == "linux")
			{
				this.reader = new TermInfoReader(term, KnownTerminals.linux);
			}
			if (this.reader == null)
			{
				this.reader = new TermInfoReader(term, KnownTerminals.ansi);
			}
			if (!(Console.stdout is CStreamWriter))
			{
				this.stdout = new CStreamWriter(Console.OpenStandardOutput(0), Console.OutputEncoding, false);
				this.stdout.AutoFlush = true;
				return;
			}
			this.stdout = (CStreamWriter)Console.stdout;
		}

		// Token: 0x1700037E RID: 894
		// (get) Token: 0x06001A55 RID: 6741 RVA: 0x00062E90 File Offset: 0x00061090
		public bool Initialized
		{
			get
			{
				return this.inited;
			}
		}

		// Token: 0x06001A56 RID: 6742 RVA: 0x00062E98 File Offset: 0x00061098
		public void Init()
		{
			if (this.inited)
			{
				return;
			}
			object obj = this.initLock;
			lock (obj)
			{
				if (!this.inited)
				{
					this.inited = true;
					if (!ConsoleDriver.IsConsole)
					{
						throw new IOException("Not a tty.");
					}
					ConsoleDriver.SetEcho(false);
					string text = null;
					this.keypadXmit = this.reader.Get(TermInfoStrings.KeypadXmit);
					this.keypadLocal = this.reader.Get(TermInfoStrings.KeypadLocal);
					if (this.keypadXmit != null)
					{
						this.WriteConsole(this.keypadXmit);
						if (this.keypadLocal != null)
						{
							text += this.keypadLocal;
						}
					}
					this.origPair = this.reader.Get(TermInfoStrings.OrigPair);
					this.origColors = this.reader.Get(TermInfoStrings.OrigColors);
					this.setfgcolor = this.reader.Get(TermInfoStrings.SetAForeground);
					this.setbgcolor = this.reader.Get(TermInfoStrings.SetABackground);
					this.maxColors = this.reader.Get(TermInfoNumbers.MaxColors);
					this.maxColors = Math.Max(Math.Min(this.maxColors, 16), 1);
					string text2 = (this.origColors == null) ? this.origPair : this.origColors;
					if (text2 != null)
					{
						text += text2;
					}
					if (!ConsoleDriver.TtySetup(this.keypadXmit, text, out this.control_characters, out TermInfoDriver.native_terminal_size))
					{
						this.control_characters = new byte[17];
						TermInfoDriver.native_terminal_size = null;
					}
					this.stdin = new StreamReader(Console.OpenStandardInput(0), Console.InputEncoding);
					this.clear = this.reader.Get(TermInfoStrings.ClearScreen);
					this.bell = this.reader.Get(TermInfoStrings.Bell);
					if (this.clear == null)
					{
						this.clear = this.reader.Get(TermInfoStrings.CursorHome);
						this.clear += this.reader.Get(TermInfoStrings.ClrEos);
					}
					this.csrVisible = this.reader.Get(TermInfoStrings.CursorNormal);
					if (this.csrVisible == null)
					{
						this.csrVisible = this.reader.Get(TermInfoStrings.CursorVisible);
					}
					this.csrInvisible = this.reader.Get(TermInfoStrings.CursorInvisible);
					if (this.term == "cygwin" || this.term == "linux" || (this.term != null && this.term.StartsWith("xterm")) || this.term == "rxvt" || this.term == "dtterm")
					{
						this.titleFormat = "\u001b]0;{0}\a";
					}
					else if (this.term == "iris-ansi")
					{
						this.titleFormat = "\u001bP1.y{0}\u001b\\";
					}
					else if (this.term == "sun-cmd")
					{
						this.titleFormat = "\u001b]l{0}\u001b\\";
					}
					this.cursorAddress = this.reader.Get(TermInfoStrings.CursorAddress);
					this.GetCursorPosition();
					if (this.noGetPosition)
					{
						this.WriteConsole(this.clear);
						this.cursorLeft = 0;
						this.cursorTop = 0;
					}
				}
			}
		}

		// Token: 0x06001A57 RID: 6743 RVA: 0x000631D4 File Offset: 0x000613D4
		private void IncrementX()
		{
			this.cursorLeft++;
			if (this.cursorLeft >= this.WindowWidth)
			{
				this.cursorTop++;
				this.cursorLeft = 0;
				if (this.cursorTop >= this.WindowHeight)
				{
					if (this.rl_starty != -1)
					{
						this.rl_starty--;
					}
					this.cursorTop--;
				}
			}
		}

		// Token: 0x06001A58 RID: 6744 RVA: 0x00063248 File Offset: 0x00061448
		public void WriteSpecialKey(ConsoleKeyInfo key)
		{
			switch (key.Key)
			{
			case ConsoleKey.Backspace:
				if (this.cursorLeft > 0 && (this.cursorLeft > this.rl_startx || this.cursorTop != this.rl_starty))
				{
					this.cursorLeft--;
					this.SetCursorPosition(this.cursorLeft, this.cursorTop);
					this.WriteConsole(" ");
					this.SetCursorPosition(this.cursorLeft, this.cursorTop);
					return;
				}
				break;
			case ConsoleKey.Tab:
			{
				int num = 8 - this.cursorLeft % 8;
				for (int i = 0; i < num; i++)
				{
					this.IncrementX();
				}
				this.WriteConsole("\t");
				return;
			}
			case (ConsoleKey)10:
			case (ConsoleKey)11:
			case ConsoleKey.Enter:
				break;
			case ConsoleKey.Clear:
				this.WriteConsole(this.clear);
				this.cursorLeft = 0;
				this.cursorTop = 0;
				break;
			default:
				return;
			}
		}

		// Token: 0x06001A59 RID: 6745 RVA: 0x0006332A File Offset: 0x0006152A
		public void WriteSpecialKey(char c)
		{
			this.WriteSpecialKey(this.CreateKeyInfoFromInt((int)c, false));
		}

		// Token: 0x06001A5A RID: 6746 RVA: 0x0006333C File Offset: 0x0006153C
		public bool IsSpecialKey(ConsoleKeyInfo key)
		{
			if (!this.inited)
			{
				return false;
			}
			switch (key.Key)
			{
			case ConsoleKey.Backspace:
				return true;
			case ConsoleKey.Tab:
				return true;
			case ConsoleKey.Clear:
				return true;
			case ConsoleKey.Enter:
				this.cursorLeft = 0;
				this.cursorTop++;
				if (this.cursorTop >= this.WindowHeight)
				{
					this.cursorTop--;
				}
				return false;
			}
			this.IncrementX();
			return false;
		}

		// Token: 0x06001A5B RID: 6747 RVA: 0x000633BD File Offset: 0x000615BD
		public bool IsSpecialKey(char c)
		{
			return this.IsSpecialKey(this.CreateKeyInfoFromInt((int)c, false));
		}

		// Token: 0x06001A5C RID: 6748 RVA: 0x000633D0 File Offset: 0x000615D0
		private void ChangeColor(string format, ConsoleColor color)
		{
			if ((color & (ConsoleColor)(-16)) != ConsoleColor.Black)
			{
				throw new ArgumentException("Invalid Console Color");
			}
			int value = TermInfoDriver._consoleColorToAnsiCode[(int)color] % this.maxColors;
			this.WriteConsole(ParameterizedStrings.Evaluate(format, new ParameterizedStrings.FormatParam[]
			{
				value
			}));
		}

		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06001A5D RID: 6749 RVA: 0x0006341E File Offset: 0x0006161E
		// (set) Token: 0x06001A5E RID: 6750 RVA: 0x00063434 File Offset: 0x00061634
		public ConsoleColor BackgroundColor
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return this.bgcolor;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.ChangeColor(this.setbgcolor, value);
				this.bgcolor = value;
			}
		}

		// Token: 0x17000380 RID: 896
		// (get) Token: 0x06001A5F RID: 6751 RVA: 0x00063458 File Offset: 0x00061658
		// (set) Token: 0x06001A60 RID: 6752 RVA: 0x0006346E File Offset: 0x0006166E
		public ConsoleColor ForegroundColor
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return this.fgcolor;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.ChangeColor(this.setfgcolor, value);
				this.fgcolor = value;
			}
		}

		// Token: 0x06001A61 RID: 6753 RVA: 0x00063494 File Offset: 0x00061694
		private void GetCursorPosition()
		{
			int num = 0;
			int num2 = 0;
			int num3 = ConsoleDriver.InternalKeyAvailable(0);
			int num4;
			while (num3-- > 0)
			{
				num4 = this.stdin.Read();
				this.AddToBuffer(num4);
			}
			this.WriteConsole("\u001b[6n");
			if (ConsoleDriver.InternalKeyAvailable(1000) <= 0)
			{
				this.noGetPosition = true;
				return;
			}
			for (num4 = this.stdin.Read(); num4 != 27; num4 = this.stdin.Read())
			{
				this.AddToBuffer(num4);
				if (ConsoleDriver.InternalKeyAvailable(100) <= 0)
				{
					return;
				}
			}
			num4 = this.stdin.Read();
			if (num4 != 91)
			{
				this.AddToBuffer(27);
				this.AddToBuffer(num4);
				return;
			}
			num4 = this.stdin.Read();
			if (num4 != 59)
			{
				num = num4 - 48;
				num4 = this.stdin.Read();
				while (num4 >= 48 && num4 <= 57)
				{
					num = num * 10 + num4 - 48;
					num4 = this.stdin.Read();
				}
				num--;
			}
			num4 = this.stdin.Read();
			if (num4 != 82)
			{
				num2 = num4 - 48;
				num4 = this.stdin.Read();
				while (num4 >= 48 && num4 <= 57)
				{
					num2 = num2 * 10 + num4 - 48;
					num4 = this.stdin.Read();
				}
				num2--;
			}
			this.cursorLeft = num2;
			this.cursorTop = num;
		}

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x06001A62 RID: 6754 RVA: 0x000635D9 File Offset: 0x000617D9
		// (set) Token: 0x06001A63 RID: 6755 RVA: 0x000635F5 File Offset: 0x000617F5
		public int BufferHeight
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.CheckWindowDimensions();
				return this.bufferHeight;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000382 RID: 898
		// (get) Token: 0x06001A64 RID: 6756 RVA: 0x0006360A File Offset: 0x0006180A
		// (set) Token: 0x06001A65 RID: 6757 RVA: 0x000635F5 File Offset: 0x000617F5
		public int BufferWidth
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.CheckWindowDimensions();
				return this.bufferWidth;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000383 RID: 899
		// (get) Token: 0x06001A66 RID: 6758 RVA: 0x00063626 File Offset: 0x00061826
		public bool CapsLock
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return false;
			}
		}

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x06001A67 RID: 6759 RVA: 0x00063637 File Offset: 0x00061837
		// (set) Token: 0x06001A68 RID: 6760 RVA: 0x0006364D File Offset: 0x0006184D
		public int CursorLeft
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return this.cursorLeft;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.SetCursorPosition(value, this.CursorTop);
			}
		}

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x06001A69 RID: 6761 RVA: 0x0006366A File Offset: 0x0006186A
		// (set) Token: 0x06001A6A RID: 6762 RVA: 0x00063680 File Offset: 0x00061880
		public int CursorTop
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return this.cursorTop;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.SetCursorPosition(this.CursorLeft, value);
			}
		}

		// Token: 0x17000386 RID: 902
		// (get) Token: 0x06001A6B RID: 6763 RVA: 0x0006369D File Offset: 0x0006189D
		// (set) Token: 0x06001A6C RID: 6764 RVA: 0x000636B3 File Offset: 0x000618B3
		public bool CursorVisible
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return this.cursorVisible;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.cursorVisible = value;
				this.WriteConsole(value ? this.csrVisible : this.csrInvisible);
			}
		}

		// Token: 0x17000387 RID: 903
		// (get) Token: 0x06001A6D RID: 6765 RVA: 0x000636E1 File Offset: 0x000618E1
		// (set) Token: 0x06001A6E RID: 6766 RVA: 0x000636F2 File Offset: 0x000618F2
		[MonoTODO]
		public int CursorSize
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return 1;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
			}
		}

		// Token: 0x17000388 RID: 904
		// (get) Token: 0x06001A6F RID: 6767 RVA: 0x00063702 File Offset: 0x00061902
		public bool KeyAvailable
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return this.writepos > this.readpos || ConsoleDriver.InternalKeyAvailable(0) > 0;
			}
		}

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x06001A70 RID: 6768 RVA: 0x0006372B File Offset: 0x0006192B
		public int LargestWindowHeight
		{
			get
			{
				return this.WindowHeight;
			}
		}

		// Token: 0x1700038A RID: 906
		// (get) Token: 0x06001A71 RID: 6769 RVA: 0x00063733 File Offset: 0x00061933
		public int LargestWindowWidth
		{
			get
			{
				return this.WindowWidth;
			}
		}

		// Token: 0x1700038B RID: 907
		// (get) Token: 0x06001A72 RID: 6770 RVA: 0x00063626 File Offset: 0x00061826
		public bool NumberLock
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return false;
			}
		}

		// Token: 0x1700038C RID: 908
		// (get) Token: 0x06001A73 RID: 6771 RVA: 0x0006373B File Offset: 0x0006193B
		// (set) Token: 0x06001A74 RID: 6772 RVA: 0x00063751 File Offset: 0x00061951
		public string Title
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return this.title;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.title = value;
				this.WriteConsole(string.Format(this.titleFormat, value));
			}
		}

		// Token: 0x1700038D RID: 909
		// (get) Token: 0x06001A75 RID: 6773 RVA: 0x0006377A File Offset: 0x0006197A
		// (set) Token: 0x06001A76 RID: 6774 RVA: 0x00063790 File Offset: 0x00061990
		public bool TreatControlCAsInput
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return this.controlCAsInput;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				if (this.controlCAsInput == value)
				{
					return;
				}
				ConsoleDriver.SetBreak(value);
				this.controlCAsInput = value;
			}
		}

		// Token: 0x06001A77 RID: 6775 RVA: 0x000637B8 File Offset: 0x000619B8
		private unsafe void CheckWindowDimensions()
		{
			if (TermInfoDriver.native_terminal_size == null || TermInfoDriver.terminal_size == *TermInfoDriver.native_terminal_size)
			{
				return;
			}
			if (*TermInfoDriver.native_terminal_size == -1)
			{
				int num = this.reader.Get(TermInfoNumbers.Columns);
				if (num != 0)
				{
					this.windowWidth = num;
				}
				num = this.reader.Get(TermInfoNumbers.Lines);
				if (num != 0)
				{
					this.windowHeight = num;
				}
			}
			else
			{
				TermInfoDriver.terminal_size = *TermInfoDriver.native_terminal_size;
				this.windowWidth = TermInfoDriver.terminal_size >> 16;
				this.windowHeight = (TermInfoDriver.terminal_size & 65535);
			}
			this.bufferHeight = this.windowHeight;
			this.bufferWidth = this.windowWidth;
		}

		// Token: 0x1700038E RID: 910
		// (get) Token: 0x06001A78 RID: 6776 RVA: 0x00063857 File Offset: 0x00061A57
		// (set) Token: 0x06001A79 RID: 6777 RVA: 0x000635F5 File Offset: 0x000617F5
		public int WindowHeight
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.CheckWindowDimensions();
				return this.windowHeight;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				throw new NotSupportedException();
			}
		}

		// Token: 0x1700038F RID: 911
		// (get) Token: 0x06001A7A RID: 6778 RVA: 0x00063626 File Offset: 0x00061826
		// (set) Token: 0x06001A7B RID: 6779 RVA: 0x000635F5 File Offset: 0x000617F5
		public int WindowLeft
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return 0;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000390 RID: 912
		// (get) Token: 0x06001A7C RID: 6780 RVA: 0x00063626 File Offset: 0x00061826
		// (set) Token: 0x06001A7D RID: 6781 RVA: 0x000635F5 File Offset: 0x000617F5
		public int WindowTop
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				return 0;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000391 RID: 913
		// (get) Token: 0x06001A7E RID: 6782 RVA: 0x00063873 File Offset: 0x00061A73
		// (set) Token: 0x06001A7F RID: 6783 RVA: 0x000635F5 File Offset: 0x000617F5
		public int WindowWidth
		{
			get
			{
				if (!this.inited)
				{
					this.Init();
				}
				this.CheckWindowDimensions();
				return this.windowWidth;
			}
			set
			{
				if (!this.inited)
				{
					this.Init();
				}
				throw new NotSupportedException();
			}
		}

		// Token: 0x06001A80 RID: 6784 RVA: 0x0006388F File Offset: 0x00061A8F
		public void Clear()
		{
			if (!this.inited)
			{
				this.Init();
			}
			this.WriteConsole(this.clear);
			this.cursorLeft = 0;
			this.cursorTop = 0;
		}

		// Token: 0x06001A81 RID: 6785 RVA: 0x000638B9 File Offset: 0x00061AB9
		public void Beep(int frequency, int duration)
		{
			if (!this.inited)
			{
				this.Init();
			}
			this.WriteConsole(this.bell);
		}

		// Token: 0x06001A82 RID: 6786 RVA: 0x000638D5 File Offset: 0x00061AD5
		public void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop, char sourceChar, ConsoleColor sourceForeColor, ConsoleColor sourceBackColor)
		{
			if (!this.inited)
			{
				this.Init();
			}
			throw new NotImplementedException();
		}

		// Token: 0x06001A83 RID: 6787 RVA: 0x000638EC File Offset: 0x00061AEC
		private void AddToBuffer(int b)
		{
			if (this.buffer == null)
			{
				this.buffer = new char[1024];
			}
			else if (this.writepos >= this.buffer.Length)
			{
				char[] dst = new char[this.buffer.Length * 2];
				Buffer.BlockCopy(this.buffer, 0, dst, 0, this.buffer.Length);
				this.buffer = dst;
			}
			char[] array = this.buffer;
			int num = this.writepos;
			this.writepos = num + 1;
			array[num] = (ushort)b;
		}

		// Token: 0x06001A84 RID: 6788 RVA: 0x0006396C File Offset: 0x00061B6C
		private void AdjustBuffer()
		{
			if (this.readpos >= this.writepos)
			{
				this.readpos = (this.writepos = 0);
			}
		}

		// Token: 0x06001A85 RID: 6789 RVA: 0x00063998 File Offset: 0x00061B98
		private ConsoleKeyInfo CreateKeyInfoFromInt(int n, bool alt)
		{
			char keyChar = (char)n;
			ConsoleKey key = (ConsoleKey)n;
			bool shift = false;
			bool control = false;
			if (n <= 19)
			{
				switch (n)
				{
				case 8:
				case 9:
				case 12:
				case 13:
					goto IL_C7;
				case 10:
					key = ConsoleKey.Enter;
					goto IL_C7;
				case 11:
					break;
				default:
					if (n == 19)
					{
						goto IL_C7;
					}
					break;
				}
			}
			else
			{
				if (n == 27)
				{
					key = ConsoleKey.Escape;
					goto IL_C7;
				}
				if (n == 32)
				{
					key = ConsoleKey.Spacebar;
					goto IL_C7;
				}
				switch (n)
				{
				case 42:
					key = ConsoleKey.Multiply;
					goto IL_C7;
				case 43:
					key = ConsoleKey.Add;
					goto IL_C7;
				case 45:
					key = ConsoleKey.Subtract;
					goto IL_C7;
				case 47:
					key = ConsoleKey.Divide;
					goto IL_C7;
				}
			}
			if (n >= 1 && n <= 26)
			{
				control = true;
				key = ConsoleKey.A + n - 1;
			}
			else if (n >= 97 && n <= 122)
			{
				key = (ConsoleKey)(-32) + n;
			}
			else if (n >= 65 && n <= 90)
			{
				shift = true;
			}
			else if (n < 48 || n > 57)
			{
				key = (ConsoleKey)0;
			}
			IL_C7:
			return new ConsoleKeyInfo(keyChar, key, shift, alt, control);
		}

		// Token: 0x06001A86 RID: 6790 RVA: 0x00063A78 File Offset: 0x00061C78
		private object GetKeyFromBuffer(bool cooked)
		{
			if (this.readpos >= this.writepos)
			{
				return null;
			}
			int num = (int)this.buffer[this.readpos];
			if (!cooked || !this.rootmap.StartsWith(num))
			{
				this.readpos++;
				this.AdjustBuffer();
				return this.CreateKeyInfoFromInt(num, false);
			}
			int num2;
			TermInfoStrings termInfoStrings = this.rootmap.Match(this.buffer, this.readpos, this.writepos - this.readpos, out num2);
			if (termInfoStrings == (TermInfoStrings)(-1))
			{
				if (this.buffer[this.readpos] != '\u001b' || this.writepos - this.readpos < 2)
				{
					return null;
				}
				this.readpos += 2;
				this.AdjustBuffer();
				if (this.buffer[this.readpos + 1] == '\u007f')
				{
					return new ConsoleKeyInfo('\b', ConsoleKey.Backspace, false, true, false);
				}
				return this.CreateKeyInfoFromInt((int)this.buffer[this.readpos + 1], true);
			}
			else
			{
				if (this.keymap[termInfoStrings] != null)
				{
					ConsoleKeyInfo consoleKeyInfo = (ConsoleKeyInfo)this.keymap[termInfoStrings];
					this.readpos += num2;
					this.AdjustBuffer();
					return consoleKeyInfo;
				}
				this.readpos++;
				this.AdjustBuffer();
				return this.CreateKeyInfoFromInt(num, false);
			}
		}

		// Token: 0x06001A87 RID: 6791 RVA: 0x00063BE0 File Offset: 0x00061DE0
		private ConsoleKeyInfo ReadKeyInternal(out bool fresh)
		{
			if (!this.inited)
			{
				this.Init();
			}
			this.InitKeys();
			object keyFromBuffer;
			if ((keyFromBuffer = this.GetKeyFromBuffer(true)) == null)
			{
				do
				{
					if (ConsoleDriver.InternalKeyAvailable(150) > 0)
					{
						do
						{
							this.AddToBuffer(this.stdin.Read());
						}
						while (ConsoleDriver.InternalKeyAvailable(0) > 0);
					}
					else if (this.stdin.DataAvailable())
					{
						do
						{
							this.AddToBuffer(this.stdin.Read());
						}
						while (this.stdin.DataAvailable());
					}
					else
					{
						if ((keyFromBuffer = this.GetKeyFromBuffer(false)) != null)
						{
							break;
						}
						this.AddToBuffer(this.stdin.Read());
					}
					keyFromBuffer = this.GetKeyFromBuffer(true);
				}
				while (keyFromBuffer == null);
				fresh = true;
			}
			else
			{
				fresh = false;
			}
			return (ConsoleKeyInfo)keyFromBuffer;
		}

		// Token: 0x06001A88 RID: 6792 RVA: 0x00063C9A File Offset: 0x00061E9A
		private bool InputPending()
		{
			return this.readpos < this.writepos || this.stdin.DataAvailable();
		}

		// Token: 0x06001A89 RID: 6793 RVA: 0x00063CB8 File Offset: 0x00061EB8
		private void QueueEcho(char c)
		{
			if (this.echobuf == null)
			{
				this.echobuf = new char[1024];
			}
			char[] array = this.echobuf;
			int num = this.echon;
			this.echon = num + 1;
			array[num] = c;
			if (this.echon == this.echobuf.Length || !this.InputPending())
			{
				this.stdout.InternalWriteChars(this.echobuf, this.echon);
				this.echon = 0;
			}
		}

		// Token: 0x06001A8A RID: 6794 RVA: 0x00063D2C File Offset: 0x00061F2C
		private void Echo(ConsoleKeyInfo key)
		{
			if (!this.IsSpecialKey(key))
			{
				this.QueueEcho(key.KeyChar);
				return;
			}
			this.EchoFlush();
			this.WriteSpecialKey(key);
		}

		// Token: 0x06001A8B RID: 6795 RVA: 0x00063D52 File Offset: 0x00061F52
		private void EchoFlush()
		{
			if (this.echon == 0)
			{
				return;
			}
			this.stdout.InternalWriteChars(this.echobuf, this.echon);
			this.echon = 0;
		}

		// Token: 0x06001A8C RID: 6796 RVA: 0x00063D7C File Offset: 0x00061F7C
		public int Read([In] [Out] char[] dest, int index, int count)
		{
			bool flag = false;
			int num = 0;
			StringBuilder stringBuilder = new StringBuilder();
			object keyFromBuffer;
			while ((keyFromBuffer = this.GetKeyFromBuffer(true)) != null)
			{
				ConsoleKeyInfo key = (ConsoleKeyInfo)keyFromBuffer;
				char keyChar = key.KeyChar;
				if (key.Key != ConsoleKey.Backspace)
				{
					if (key.Key == ConsoleKey.Enter)
					{
						num = stringBuilder.Length;
					}
					stringBuilder.Append(keyChar);
				}
				else if (stringBuilder.Length > num)
				{
					StringBuilder stringBuilder2 = stringBuilder;
					int length = stringBuilder2.Length;
					stringBuilder2.Length = length - 1;
				}
			}
			this.rl_startx = this.cursorLeft;
			this.rl_starty = this.cursorTop;
			for (;;)
			{
				bool flag2;
				ConsoleKeyInfo key = this.ReadKeyInternal(out flag2);
				flag = (flag || flag2);
				char keyChar = key.KeyChar;
				if (key.Key != ConsoleKey.Backspace)
				{
					if (key.Key == ConsoleKey.Enter)
					{
						num = stringBuilder.Length;
					}
					stringBuilder.Append(keyChar);
					goto IL_E0;
				}
				if (stringBuilder.Length > num)
				{
					StringBuilder stringBuilder3 = stringBuilder;
					int length = stringBuilder3.Length;
					stringBuilder3.Length = length - 1;
					goto IL_E0;
				}
				IL_EA:
				if (key.Key == ConsoleKey.Enter)
				{
					break;
				}
				continue;
				IL_E0:
				if (flag)
				{
					this.Echo(key);
					goto IL_EA;
				}
				goto IL_EA;
			}
			this.EchoFlush();
			this.rl_startx = -1;
			this.rl_starty = -1;
			int num2 = 0;
			while (count > 0 && num2 < stringBuilder.Length)
			{
				dest[index + num2] = stringBuilder[num2];
				num2++;
				count--;
			}
			for (int i = num2; i < stringBuilder.Length; i++)
			{
				this.AddToBuffer((int)stringBuilder[i]);
			}
			return num2;
		}

		// Token: 0x06001A8D RID: 6797 RVA: 0x00063EE4 File Offset: 0x000620E4
		public ConsoleKeyInfo ReadKey(bool intercept)
		{
			bool flag;
			ConsoleKeyInfo consoleKeyInfo = this.ReadKeyInternal(out flag);
			if (!intercept && flag)
			{
				this.Echo(consoleKeyInfo);
				this.EchoFlush();
			}
			return consoleKeyInfo;
		}

		// Token: 0x06001A8E RID: 6798 RVA: 0x00063F10 File Offset: 0x00062110
		public string ReadLine()
		{
			return this.ReadUntilConditionInternal(true);
		}

		// Token: 0x06001A8F RID: 6799 RVA: 0x00063F19 File Offset: 0x00062119
		public string ReadToEnd()
		{
			return this.ReadUntilConditionInternal(false);
		}

		// Token: 0x06001A90 RID: 6800 RVA: 0x00063F24 File Offset: 0x00062124
		private string ReadUntilConditionInternal(bool haltOnNewLine)
		{
			if (!this.inited)
			{
				this.Init();
			}
			this.GetCursorPosition();
			StringBuilder stringBuilder = new StringBuilder();
			bool flag = false;
			this.rl_startx = this.cursorLeft;
			this.rl_starty = this.cursorTop;
			char c = (char)this.control_characters[4];
			for (;;)
			{
				bool flag2;
				ConsoleKeyInfo key = this.ReadKeyInternal(out flag2);
				flag = (flag || flag2);
				char keyChar = key.KeyChar;
				if (keyChar == c && keyChar != '\0' && stringBuilder.Length == 0)
				{
					break;
				}
				bool flag3 = haltOnNewLine && key.Key == ConsoleKey.Enter;
				if (flag3)
				{
					goto IL_AC;
				}
				if (key.Key != ConsoleKey.Backspace)
				{
					stringBuilder.Append(keyChar);
					goto IL_AC;
				}
				if (stringBuilder.Length > 0)
				{
					StringBuilder stringBuilder2 = stringBuilder;
					int length = stringBuilder2.Length;
					stringBuilder2.Length = length - 1;
					goto IL_AC;
				}
				IL_B6:
				if (flag3)
				{
					goto Block_10;
				}
				continue;
				IL_AC:
				if (flag)
				{
					this.Echo(key);
					goto IL_B6;
				}
				goto IL_B6;
			}
			return null;
			Block_10:
			this.EchoFlush();
			this.rl_startx = -1;
			this.rl_starty = -1;
			return stringBuilder.ToString();
		}

		// Token: 0x06001A91 RID: 6801 RVA: 0x00064004 File Offset: 0x00062204
		public void ResetColor()
		{
			if (!this.inited)
			{
				this.Init();
			}
			string str = (this.origPair != null) ? this.origPair : this.origColors;
			this.WriteConsole(str);
		}

		// Token: 0x06001A92 RID: 6802 RVA: 0x0006403D File Offset: 0x0006223D
		public void SetBufferSize(int width, int height)
		{
			if (!this.inited)
			{
				this.Init();
			}
			throw new NotImplementedException(string.Empty);
		}

		// Token: 0x06001A93 RID: 6803 RVA: 0x00064058 File Offset: 0x00062258
		public void SetCursorPosition(int left, int top)
		{
			if (!this.inited)
			{
				this.Init();
			}
			this.CheckWindowDimensions();
			if (left < 0 || left >= this.bufferWidth)
			{
				throw new ArgumentOutOfRangeException("left", "Value must be positive and below the buffer width.");
			}
			if (top < 0 || top >= this.bufferHeight)
			{
				throw new ArgumentOutOfRangeException("top", "Value must be positive and below the buffer height.");
			}
			if (this.cursorAddress == null)
			{
				throw new NotSupportedException("This terminal does not suport setting the cursor position.");
			}
			this.WriteConsole(ParameterizedStrings.Evaluate(this.cursorAddress, new ParameterizedStrings.FormatParam[]
			{
				top,
				left
			}));
			this.cursorLeft = left;
			this.cursorTop = top;
		}

		// Token: 0x06001A94 RID: 6804 RVA: 0x000636F2 File Offset: 0x000618F2
		public void SetWindowPosition(int left, int top)
		{
			if (!this.inited)
			{
				this.Init();
			}
		}

		// Token: 0x06001A95 RID: 6805 RVA: 0x000636F2 File Offset: 0x000618F2
		public void SetWindowSize(int width, int height)
		{
			if (!this.inited)
			{
				this.Init();
			}
		}

		// Token: 0x06001A96 RID: 6806 RVA: 0x00064108 File Offset: 0x00062308
		private void CreateKeyMap()
		{
			this.keymap = new Hashtable();
			this.keymap[TermInfoStrings.KeyBackspace] = new ConsoleKeyInfo('\0', ConsoleKey.Backspace, false, false, false);
			this.keymap[TermInfoStrings.KeyClear] = new ConsoleKeyInfo('\0', ConsoleKey.Clear, false, false, false);
			this.keymap[TermInfoStrings.KeyDown] = new ConsoleKeyInfo('\0', ConsoleKey.DownArrow, false, false, false);
			this.keymap[TermInfoStrings.KeyF1] = new ConsoleKeyInfo('\0', ConsoleKey.F1, false, false, false);
			this.keymap[TermInfoStrings.KeyF10] = new ConsoleKeyInfo('\0', ConsoleKey.F10, false, false, false);
			this.keymap[TermInfoStrings.KeyF2] = new ConsoleKeyInfo('\0', ConsoleKey.F2, false, false, false);
			this.keymap[TermInfoStrings.KeyF3] = new ConsoleKeyInfo('\0', ConsoleKey.F3, false, false, false);
			this.keymap[TermInfoStrings.KeyF4] = new ConsoleKeyInfo('\0', ConsoleKey.F4, false, false, false);
			this.keymap[TermInfoStrings.KeyF5] = new ConsoleKeyInfo('\0', ConsoleKey.F5, false, false, false);
			this.keymap[TermInfoStrings.KeyF6] = new ConsoleKeyInfo('\0', ConsoleKey.F6, false, false, false);
			this.keymap[TermInfoStrings.KeyF7] = new ConsoleKeyInfo('\0', ConsoleKey.F7, false, false, false);
			this.keymap[TermInfoStrings.KeyF8] = new ConsoleKeyInfo('\0', ConsoleKey.F8, false, false, false);
			this.keymap[TermInfoStrings.KeyF9] = new ConsoleKeyInfo('\0', ConsoleKey.F9, false, false, false);
			this.keymap[TermInfoStrings.KeyHome] = new ConsoleKeyInfo('\0', ConsoleKey.Home, false, false, false);
			this.keymap[TermInfoStrings.KeyLeft] = new ConsoleKeyInfo('\0', ConsoleKey.LeftArrow, false, false, false);
			this.keymap[TermInfoStrings.KeyLl] = new ConsoleKeyInfo('\0', ConsoleKey.NumPad1, false, false, false);
			this.keymap[TermInfoStrings.KeyNpage] = new ConsoleKeyInfo('\0', ConsoleKey.PageDown, false, false, false);
			this.keymap[TermInfoStrings.KeyPpage] = new ConsoleKeyInfo('\0', ConsoleKey.PageUp, false, false, false);
			this.keymap[TermInfoStrings.KeyRight] = new ConsoleKeyInfo('\0', ConsoleKey.RightArrow, false, false, false);
			this.keymap[TermInfoStrings.KeySf] = new ConsoleKeyInfo('\0', ConsoleKey.PageDown, false, false, false);
			this.keymap[TermInfoStrings.KeySr] = new ConsoleKeyInfo('\0', ConsoleKey.PageUp, false, false, false);
			this.keymap[TermInfoStrings.KeyUp] = new ConsoleKeyInfo('\0', ConsoleKey.UpArrow, false, false, false);
			this.keymap[TermInfoStrings.KeyA1] = new ConsoleKeyInfo('\0', ConsoleKey.NumPad7, false, false, false);
			this.keymap[TermInfoStrings.KeyA3] = new ConsoleKeyInfo('\0', ConsoleKey.NumPad9, false, false, false);
			this.keymap[TermInfoStrings.KeyB2] = new ConsoleKeyInfo('\0', ConsoleKey.NumPad5, false, false, false);
			this.keymap[TermInfoStrings.KeyC1] = new ConsoleKeyInfo('\0', ConsoleKey.NumPad1, false, false, false);
			this.keymap[TermInfoStrings.KeyC3] = new ConsoleKeyInfo('\0', ConsoleKey.NumPad3, false, false, false);
			this.keymap[TermInfoStrings.KeyBtab] = new ConsoleKeyInfo('\0', ConsoleKey.Tab, true, false, false);
			this.keymap[TermInfoStrings.KeyBeg] = new ConsoleKeyInfo('\0', ConsoleKey.Home, false, false, false);
			this.keymap[TermInfoStrings.KeyCopy] = new ConsoleKeyInfo('C', ConsoleKey.C, false, true, false);
			this.keymap[TermInfoStrings.KeyEnd] = new ConsoleKeyInfo('\0', ConsoleKey.End, false, false, false);
			this.keymap[TermInfoStrings.KeyEnter] = new ConsoleKeyInfo('\n', ConsoleKey.Enter, false, false, false);
			this.keymap[TermInfoStrings.KeyHelp] = new ConsoleKeyInfo('\0', ConsoleKey.Help, false, false, false);
			this.keymap[TermInfoStrings.KeyPrint] = new ConsoleKeyInfo('\0', ConsoleKey.Print, false, false, false);
			this.keymap[TermInfoStrings.KeyUndo] = new ConsoleKeyInfo('Z', ConsoleKey.Z, false, true, false);
			this.keymap[TermInfoStrings.KeySbeg] = new ConsoleKeyInfo('\0', ConsoleKey.Home, true, false, false);
			this.keymap[TermInfoStrings.KeyScopy] = new ConsoleKeyInfo('C', ConsoleKey.C, true, true, false);
			this.keymap[TermInfoStrings.KeySdc] = new ConsoleKeyInfo('\t', ConsoleKey.Delete, true, false, false);
			this.keymap[TermInfoStrings.KeyShelp] = new ConsoleKeyInfo('\0', ConsoleKey.Help, true, false, false);
			this.keymap[TermInfoStrings.KeyShome] = new ConsoleKeyInfo('\0', ConsoleKey.Home, true, false, false);
			this.keymap[TermInfoStrings.KeySleft] = new ConsoleKeyInfo('\0', ConsoleKey.LeftArrow, true, false, false);
			this.keymap[TermInfoStrings.KeySprint] = new ConsoleKeyInfo('\0', ConsoleKey.Print, true, false, false);
			this.keymap[TermInfoStrings.KeySright] = new ConsoleKeyInfo('\0', ConsoleKey.RightArrow, true, false, false);
			this.keymap[TermInfoStrings.KeySundo] = new ConsoleKeyInfo('Z', ConsoleKey.Z, true, false, false);
			this.keymap[TermInfoStrings.KeyF11] = new ConsoleKeyInfo('\0', ConsoleKey.F11, false, false, false);
			this.keymap[TermInfoStrings.KeyF12] = new ConsoleKeyInfo('\0', ConsoleKey.F12, false, false, false);
			this.keymap[TermInfoStrings.KeyF13] = new ConsoleKeyInfo('\0', ConsoleKey.F13, false, false, false);
			this.keymap[TermInfoStrings.KeyF14] = new ConsoleKeyInfo('\0', ConsoleKey.F14, false, false, false);
			this.keymap[TermInfoStrings.KeyF15] = new ConsoleKeyInfo('\0', ConsoleKey.F15, false, false, false);
			this.keymap[TermInfoStrings.KeyF16] = new ConsoleKeyInfo('\0', ConsoleKey.F16, false, false, false);
			this.keymap[TermInfoStrings.KeyF17] = new ConsoleKeyInfo('\0', ConsoleKey.F17, false, false, false);
			this.keymap[TermInfoStrings.KeyF18] = new ConsoleKeyInfo('\0', ConsoleKey.F18, false, false, false);
			this.keymap[TermInfoStrings.KeyF19] = new ConsoleKeyInfo('\0', ConsoleKey.F19, false, false, false);
			this.keymap[TermInfoStrings.KeyF20] = new ConsoleKeyInfo('\0', ConsoleKey.F20, false, false, false);
			this.keymap[TermInfoStrings.KeyF21] = new ConsoleKeyInfo('\0', ConsoleKey.F21, false, false, false);
			this.keymap[TermInfoStrings.KeyF22] = new ConsoleKeyInfo('\0', ConsoleKey.F22, false, false, false);
			this.keymap[TermInfoStrings.KeyF23] = new ConsoleKeyInfo('\0', ConsoleKey.F23, false, false, false);
			this.keymap[TermInfoStrings.KeyF24] = new ConsoleKeyInfo('\0', ConsoleKey.F24, false, false, false);
			this.keymap[TermInfoStrings.KeyDc] = new ConsoleKeyInfo('\0', ConsoleKey.Delete, false, false, false);
			this.keymap[TermInfoStrings.KeyIc] = new ConsoleKeyInfo('\0', ConsoleKey.Insert, false, false, false);
		}

		// Token: 0x06001A97 RID: 6807 RVA: 0x000649A4 File Offset: 0x00062BA4
		private void InitKeys()
		{
			if (this.initKeys)
			{
				return;
			}
			this.CreateKeyMap();
			this.rootmap = new ByteMatcher();
			foreach (TermInfoStrings s in new TermInfoStrings[]
			{
				TermInfoStrings.KeyBackspace,
				TermInfoStrings.KeyClear,
				TermInfoStrings.KeyDown,
				TermInfoStrings.KeyF1,
				TermInfoStrings.KeyF10,
				TermInfoStrings.KeyF2,
				TermInfoStrings.KeyF3,
				TermInfoStrings.KeyF4,
				TermInfoStrings.KeyF5,
				TermInfoStrings.KeyF6,
				TermInfoStrings.KeyF7,
				TermInfoStrings.KeyF8,
				TermInfoStrings.KeyF9,
				TermInfoStrings.KeyHome,
				TermInfoStrings.KeyLeft,
				TermInfoStrings.KeyLl,
				TermInfoStrings.KeyNpage,
				TermInfoStrings.KeyPpage,
				TermInfoStrings.KeyRight,
				TermInfoStrings.KeySf,
				TermInfoStrings.KeySr,
				TermInfoStrings.KeyUp,
				TermInfoStrings.KeyA1,
				TermInfoStrings.KeyA3,
				TermInfoStrings.KeyB2,
				TermInfoStrings.KeyC1,
				TermInfoStrings.KeyC3,
				TermInfoStrings.KeyBtab,
				TermInfoStrings.KeyBeg,
				TermInfoStrings.KeyCopy,
				TermInfoStrings.KeyEnd,
				TermInfoStrings.KeyEnter,
				TermInfoStrings.KeyHelp,
				TermInfoStrings.KeyPrint,
				TermInfoStrings.KeyUndo,
				TermInfoStrings.KeySbeg,
				TermInfoStrings.KeyScopy,
				TermInfoStrings.KeySdc,
				TermInfoStrings.KeyShelp,
				TermInfoStrings.KeyShome,
				TermInfoStrings.KeySleft,
				TermInfoStrings.KeySprint,
				TermInfoStrings.KeySright,
				TermInfoStrings.KeySundo,
				TermInfoStrings.KeyF11,
				TermInfoStrings.KeyF12,
				TermInfoStrings.KeyF13,
				TermInfoStrings.KeyF14,
				TermInfoStrings.KeyF15,
				TermInfoStrings.KeyF16,
				TermInfoStrings.KeyF17,
				TermInfoStrings.KeyF18,
				TermInfoStrings.KeyF19,
				TermInfoStrings.KeyF20,
				TermInfoStrings.KeyF21,
				TermInfoStrings.KeyF22,
				TermInfoStrings.KeyF23,
				TermInfoStrings.KeyF24,
				TermInfoStrings.KeyDc,
				TermInfoStrings.KeyIc
			})
			{
				this.AddStringMapping(s);
			}
			this.rootmap.AddMapping(TermInfoStrings.KeyBackspace, new byte[]
			{
				this.control_characters[2]
			});
			this.rootmap.Sort();
			this.initKeys = true;
		}

		// Token: 0x06001A98 RID: 6808 RVA: 0x00064A28 File Offset: 0x00062C28
		private void AddStringMapping(TermInfoStrings s)
		{
			byte[] stringBytes = this.reader.GetStringBytes(s);
			if (stringBytes == null)
			{
				return;
			}
			this.rootmap.AddMapping(s, stringBytes);
		}

		// Token: 0x06001A99 RID: 6809 RVA: 0x00064A54 File Offset: 0x00062C54
		// Note: this type is marked as 'beforefieldinit'.
		static TermInfoDriver()
		{
		}

		// Token: 0x04000D1A RID: 3354
		private unsafe static int* native_terminal_size;

		// Token: 0x04000D1B RID: 3355
		private static int terminal_size;

		// Token: 0x04000D1C RID: 3356
		private static readonly string[] locations = new string[]
		{
			"/usr/share/terminfo",
			"/etc/terminfo",
			"/usr/lib/terminfo",
			"/lib/terminfo"
		};

		// Token: 0x04000D1D RID: 3357
		private TermInfoReader reader;

		// Token: 0x04000D1E RID: 3358
		private int cursorLeft;

		// Token: 0x04000D1F RID: 3359
		private int cursorTop;

		// Token: 0x04000D20 RID: 3360
		private string title = string.Empty;

		// Token: 0x04000D21 RID: 3361
		private string titleFormat = string.Empty;

		// Token: 0x04000D22 RID: 3362
		private bool cursorVisible = true;

		// Token: 0x04000D23 RID: 3363
		private string csrVisible;

		// Token: 0x04000D24 RID: 3364
		private string csrInvisible;

		// Token: 0x04000D25 RID: 3365
		private string clear;

		// Token: 0x04000D26 RID: 3366
		private string bell;

		// Token: 0x04000D27 RID: 3367
		private string term;

		// Token: 0x04000D28 RID: 3368
		private StreamReader stdin;

		// Token: 0x04000D29 RID: 3369
		private CStreamWriter stdout;

		// Token: 0x04000D2A RID: 3370
		private int windowWidth;

		// Token: 0x04000D2B RID: 3371
		private int windowHeight;

		// Token: 0x04000D2C RID: 3372
		private int bufferHeight;

		// Token: 0x04000D2D RID: 3373
		private int bufferWidth;

		// Token: 0x04000D2E RID: 3374
		private char[] buffer;

		// Token: 0x04000D2F RID: 3375
		private int readpos;

		// Token: 0x04000D30 RID: 3376
		private int writepos;

		// Token: 0x04000D31 RID: 3377
		private string keypadXmit;

		// Token: 0x04000D32 RID: 3378
		private string keypadLocal;

		// Token: 0x04000D33 RID: 3379
		private bool controlCAsInput;

		// Token: 0x04000D34 RID: 3380
		private bool inited;

		// Token: 0x04000D35 RID: 3381
		private object initLock = new object();

		// Token: 0x04000D36 RID: 3382
		private bool initKeys;

		// Token: 0x04000D37 RID: 3383
		private string origPair;

		// Token: 0x04000D38 RID: 3384
		private string origColors;

		// Token: 0x04000D39 RID: 3385
		private string cursorAddress;

		// Token: 0x04000D3A RID: 3386
		private ConsoleColor fgcolor = ConsoleColor.White;

		// Token: 0x04000D3B RID: 3387
		private ConsoleColor bgcolor;

		// Token: 0x04000D3C RID: 3388
		private string setfgcolor;

		// Token: 0x04000D3D RID: 3389
		private string setbgcolor;

		// Token: 0x04000D3E RID: 3390
		private int maxColors;

		// Token: 0x04000D3F RID: 3391
		private bool noGetPosition;

		// Token: 0x04000D40 RID: 3392
		private Hashtable keymap;

		// Token: 0x04000D41 RID: 3393
		private ByteMatcher rootmap;

		// Token: 0x04000D42 RID: 3394
		private int rl_startx = -1;

		// Token: 0x04000D43 RID: 3395
		private int rl_starty = -1;

		// Token: 0x04000D44 RID: 3396
		private byte[] control_characters;

		// Token: 0x04000D45 RID: 3397
		private static readonly int[] _consoleColorToAnsiCode = new int[]
		{
			0,
			4,
			2,
			6,
			1,
			5,
			3,
			7,
			8,
			12,
			10,
			14,
			9,
			13,
			11,
			15
		};

		// Token: 0x04000D46 RID: 3398
		private char[] echobuf;

		// Token: 0x04000D47 RID: 3399
		private int echon;
	}
}
