﻿using System;
using System.IO;
using System.Text;

namespace System
{
	// Token: 0x0200022C RID: 556
	internal class TermInfoReader
	{
		// Token: 0x06001AB4 RID: 6836 RVA: 0x00065570 File Offset: 0x00063770
		public TermInfoReader(string term, string filename)
		{
			using (FileStream fileStream = File.OpenRead(filename))
			{
				long length = fileStream.Length;
				if (length > 4096L)
				{
					throw new Exception("File must be smaller than 4K");
				}
				this.buffer = new byte[(int)length];
				if (fileStream.Read(this.buffer, 0, this.buffer.Length) != this.buffer.Length)
				{
					throw new Exception("Short read");
				}
				this.ReadHeader(this.buffer, ref this.booleansOffset);
				this.ReadNames(this.buffer, ref this.booleansOffset);
			}
		}

		// Token: 0x06001AB5 RID: 6837 RVA: 0x0006561C File Offset: 0x0006381C
		public TermInfoReader(string term, byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			this.buffer = buffer;
			this.ReadHeader(buffer, ref this.booleansOffset);
			this.ReadNames(buffer, ref this.booleansOffset);
		}

		// Token: 0x06001AB6 RID: 6838 RVA: 0x00065654 File Offset: 0x00063854
		private void ReadHeader(byte[] buffer, ref int position)
		{
			short @int = this.GetInt16(buffer, position);
			position += 2;
			if (@int != 282)
			{
				throw new Exception(string.Format("Magic number is wrong: {0}", @int));
			}
			this.GetInt16(buffer, position);
			position += 2;
			this.boolSize = this.GetInt16(buffer, position);
			position += 2;
			this.numSize = this.GetInt16(buffer, position);
			position += 2;
			this.strOffsets = this.GetInt16(buffer, position);
			position += 2;
			this.GetInt16(buffer, position);
			position += 2;
		}

		// Token: 0x06001AB7 RID: 6839 RVA: 0x000656F0 File Offset: 0x000638F0
		private void ReadNames(byte[] buffer, ref int position)
		{
			string @string = this.GetString(buffer, position);
			position += @string.Length + 1;
		}

		// Token: 0x06001AB8 RID: 6840 RVA: 0x00065714 File Offset: 0x00063914
		public bool Get(TermInfoBooleans boolean)
		{
			if (boolean < TermInfoBooleans.AutoLeftMargin || boolean >= TermInfoBooleans.Last || boolean >= (TermInfoBooleans)this.boolSize)
			{
				return false;
			}
			int num = this.booleansOffset;
			num = (int)(num + boolean);
			return this.buffer[num] > 0;
		}

		// Token: 0x06001AB9 RID: 6841 RVA: 0x00065750 File Offset: 0x00063950
		public int Get(TermInfoNumbers number)
		{
			if (number < TermInfoNumbers.Columns || number >= TermInfoNumbers.Last || number > (TermInfoNumbers)this.numSize)
			{
				return -1;
			}
			int num = this.booleansOffset + (int)this.boolSize;
			if (num % 2 == 1)
			{
				num++;
			}
			num = (int)(num + number * TermInfoNumbers.Lines);
			return (int)this.GetInt16(this.buffer, num);
		}

		// Token: 0x06001ABA RID: 6842 RVA: 0x000657A0 File Offset: 0x000639A0
		public string Get(TermInfoStrings tstr)
		{
			if (tstr < TermInfoStrings.BackTab || tstr >= TermInfoStrings.Last || tstr > (TermInfoStrings)this.strOffsets)
			{
				return null;
			}
			int num = this.booleansOffset + (int)this.boolSize;
			if (num % 2 == 1)
			{
				num++;
			}
			num += (int)(this.numSize * 2);
			int @int = (int)this.GetInt16(this.buffer, (int)(num + tstr * TermInfoStrings.CarriageReturn));
			if (@int == -1)
			{
				return null;
			}
			return this.GetString(this.buffer, num + (int)(this.strOffsets * 2) + @int);
		}

		// Token: 0x06001ABB RID: 6843 RVA: 0x0006581C File Offset: 0x00063A1C
		public byte[] GetStringBytes(TermInfoStrings tstr)
		{
			if (tstr < TermInfoStrings.BackTab || tstr >= TermInfoStrings.Last || tstr > (TermInfoStrings)this.strOffsets)
			{
				return null;
			}
			int num = this.booleansOffset + (int)this.boolSize;
			if (num % 2 == 1)
			{
				num++;
			}
			num += (int)(this.numSize * 2);
			int @int = (int)this.GetInt16(this.buffer, (int)(num + tstr * TermInfoStrings.CarriageReturn));
			if (@int == -1)
			{
				return null;
			}
			return this.GetStringBytes(this.buffer, num + (int)(this.strOffsets * 2) + @int);
		}

		// Token: 0x06001ABC RID: 6844 RVA: 0x00065898 File Offset: 0x00063A98
		private short GetInt16(byte[] buffer, int offset)
		{
			int num = (int)buffer[offset];
			int num2 = (int)buffer[offset + 1];
			if (num == 255 && num2 == 255)
			{
				return -1;
			}
			return (short)(num + num2 * 256);
		}

		// Token: 0x06001ABD RID: 6845 RVA: 0x000658CC File Offset: 0x00063ACC
		private string GetString(byte[] buffer, int offset)
		{
			int num = 0;
			int num2 = offset;
			while (buffer[num2++] != 0)
			{
				num++;
			}
			return Encoding.ASCII.GetString(buffer, offset, num);
		}

		// Token: 0x06001ABE RID: 6846 RVA: 0x000658FC File Offset: 0x00063AFC
		private byte[] GetStringBytes(byte[] buffer, int offset)
		{
			int num = 0;
			int num2 = offset;
			while (buffer[num2++] != 0)
			{
				num++;
			}
			byte[] array = new byte[num];
			Buffer.InternalBlockCopy(buffer, offset, array, 0, num);
			return array;
		}

		// Token: 0x06001ABF RID: 6847 RVA: 0x00065930 File Offset: 0x00063B30
		internal static string Escape(string s)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (char c in s)
			{
				if (char.IsControl(c))
				{
					stringBuilder.AppendFormat("\\x{0:X2}", (int)c);
				}
				else
				{
					stringBuilder.Append(c);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04000D73 RID: 3443
		private short boolSize;

		// Token: 0x04000D74 RID: 3444
		private short numSize;

		// Token: 0x04000D75 RID: 3445
		private short strOffsets;

		// Token: 0x04000D76 RID: 3446
		private byte[] buffer;

		// Token: 0x04000D77 RID: 3447
		private int booleansOffset;
	}
}
