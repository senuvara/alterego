﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies how mathematical rounding methods should process a number that is midway between two numbers.</summary>
	// Token: 0x0200018B RID: 395
	[ComVisible(true)]
	public enum MidpointRounding
	{
		/// <summary>When a number is halfway between two others, it is rounded toward the nearest even number.</summary>
		// Token: 0x04000A00 RID: 2560
		ToEven,
		/// <summary>When a number is halfway between two others, it is rounded toward the nearest number that is away from zero.</summary>
		// Token: 0x04000A01 RID: 2561
		AwayFromZero
	}
}
