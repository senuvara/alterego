﻿using System;
using System.Runtime.CompilerServices;
using System.Security;

namespace System
{
	// Token: 0x020001E1 RID: 481
	[FriendAccessAllowed]
	internal class CLRConfig
	{
		// Token: 0x06001713 RID: 5907 RVA: 0x00002526 File Offset: 0x00000726
		[SecurityCritical]
		[FriendAccessAllowed]
		[SuppressUnmanagedCodeSecurity]
		internal static bool CheckLegacyManagedDeflateStream()
		{
			return false;
		}

		// Token: 0x06001714 RID: 5908
		[SecurityCritical]
		[SuppressUnmanagedCodeSecurity]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool CheckThrowUnobservedTaskExceptions();

		// Token: 0x06001715 RID: 5909 RVA: 0x00002050 File Offset: 0x00000250
		public CLRConfig()
		{
		}
	}
}
