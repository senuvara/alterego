﻿using System;

namespace System
{
	// Token: 0x02000160 RID: 352
	internal enum DTSubStringType
	{
		// Token: 0x04000959 RID: 2393
		Unknown,
		// Token: 0x0400095A RID: 2394
		Invalid,
		// Token: 0x0400095B RID: 2395
		Number,
		// Token: 0x0400095C RID: 2396
		End,
		// Token: 0x0400095D RID: 2397
		Other
	}
}
