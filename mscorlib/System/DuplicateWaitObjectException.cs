﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when an object appears more than once in an array of synchronization objects.</summary>
	// Token: 0x02000142 RID: 322
	[ComVisible(true)]
	[Serializable]
	public class DuplicateWaitObjectException : ArgumentException
	{
		// Token: 0x170001DD RID: 477
		// (get) Token: 0x06000E63 RID: 3683 RVA: 0x0003C875 File Offset: 0x0003AA75
		private static string DuplicateWaitObjectMessage
		{
			get
			{
				if (DuplicateWaitObjectException._duplicateWaitObjectMessage == null)
				{
					DuplicateWaitObjectException._duplicateWaitObjectMessage = Environment.GetResourceString("Duplicate objects in argument.");
				}
				return DuplicateWaitObjectException._duplicateWaitObjectMessage;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DuplicateWaitObjectException" /> class.</summary>
		// Token: 0x06000E64 RID: 3684 RVA: 0x0003C898 File Offset: 0x0003AA98
		public DuplicateWaitObjectException() : base(DuplicateWaitObjectException.DuplicateWaitObjectMessage)
		{
			base.SetErrorCode(-2146233047);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DuplicateWaitObjectException" /> class with the name of the parameter that causes this exception.</summary>
		/// <param name="parameterName">The name of the parameter that caused the exception. </param>
		// Token: 0x06000E65 RID: 3685 RVA: 0x0003C8B0 File Offset: 0x0003AAB0
		public DuplicateWaitObjectException(string parameterName) : base(DuplicateWaitObjectException.DuplicateWaitObjectMessage, parameterName)
		{
			base.SetErrorCode(-2146233047);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DuplicateWaitObjectException" /> class with a specified error message and the name of the parameter that causes this exception.</summary>
		/// <param name="parameterName">The name of the parameter that caused the exception. </param>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06000E66 RID: 3686 RVA: 0x0003C8C9 File Offset: 0x0003AAC9
		public DuplicateWaitObjectException(string parameterName, string message) : base(message, parameterName)
		{
			base.SetErrorCode(-2146233047);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DuplicateWaitObjectException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception.</param>
		// Token: 0x06000E67 RID: 3687 RVA: 0x0003C8DE File Offset: 0x0003AADE
		public DuplicateWaitObjectException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233047);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DuplicateWaitObjectException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06000E68 RID: 3688 RVA: 0x00032429 File Offset: 0x00030629
		protected DuplicateWaitObjectException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06000E69 RID: 3689 RVA: 0x000020D3 File Offset: 0x000002D3
		// Note: this type is marked as 'beforefieldinit'.
		static DuplicateWaitObjectException()
		{
		}

		// Token: 0x040008B7 RID: 2231
		private static volatile string _duplicateWaitObjectMessage;
	}
}
