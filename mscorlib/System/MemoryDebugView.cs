﻿using System;
using System.Diagnostics;

namespace System
{
	// Token: 0x020000B8 RID: 184
	internal sealed class MemoryDebugView<T>
	{
		// Token: 0x06000634 RID: 1588 RVA: 0x00021FEC File Offset: 0x000201EC
		public MemoryDebugView(Memory<T> memory)
		{
			this._memory = memory;
		}

		// Token: 0x06000635 RID: 1589 RVA: 0x00022000 File Offset: 0x00020200
		public MemoryDebugView(ReadOnlyMemory<T> memory)
		{
			this._memory = memory;
		}

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000636 RID: 1590 RVA: 0x00022010 File Offset: 0x00020210
		[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
		public T[] Items
		{
			get
			{
				ArraySegment<T> arraySegment;
				if (this._memory.DangerousTryGetArray(out arraySegment))
				{
					T[] array = new T[this._memory.Length];
					Array.Copy(arraySegment.Array, arraySegment.Offset, array, 0, array.Length);
					return array;
				}
				return SpanHelpers.PerTypeValues<T>.EmptyArray;
			}
		}

		// Token: 0x04000661 RID: 1633
		private readonly ReadOnlyMemory<T> _memory;
	}
}
