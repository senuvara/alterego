﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>Serves as the base class for system exceptions namespace.</summary>
	// Token: 0x020001B5 RID: 437
	[ComVisible(true)]
	[Serializable]
	public class SystemException : Exception
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.SystemException" /> class.</summary>
		// Token: 0x060013DD RID: 5085 RVA: 0x00051204 File Offset: 0x0004F404
		public SystemException() : base(Environment.GetResourceString("System error."))
		{
			base.SetErrorCode(-2146233087);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.SystemException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x060013DE RID: 5086 RVA: 0x00051221 File Offset: 0x0004F421
		public SystemException(string message) : base(message)
		{
			base.SetErrorCode(-2146233087);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.SystemException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060013DF RID: 5087 RVA: 0x00051235 File Offset: 0x0004F435
		public SystemException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233087);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.SystemException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x060013E0 RID: 5088 RVA: 0x00031FF0 File Offset: 0x000301F0
		protected SystemException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
