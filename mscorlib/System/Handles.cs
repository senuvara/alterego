﻿using System;

namespace System
{
	// Token: 0x0200024F RID: 591
	internal enum Handles
	{
		// Token: 0x04000F6A RID: 3946
		STD_INPUT = -10,
		// Token: 0x04000F6B RID: 3947
		STD_OUTPUT = -11,
		// Token: 0x04000F6C RID: 3948
		STD_ERROR = -12
	}
}
