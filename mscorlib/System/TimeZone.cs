﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents a time zone.</summary>
	// Token: 0x0200022E RID: 558
	[ComVisible(true)]
	[Serializable]
	public abstract class TimeZone
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.TimeZone" /> class.</summary>
		// Token: 0x06001AC0 RID: 6848 RVA: 0x00002050 File Offset: 0x00000250
		protected TimeZone()
		{
		}

		/// <summary>Gets the time zone of the current computer.</summary>
		/// <returns>A <see cref="T:System.TimeZone" /> object that represents the current local time zone.</returns>
		// Token: 0x17000395 RID: 917
		// (get) Token: 0x06001AC1 RID: 6849 RVA: 0x00065988 File Offset: 0x00063B88
		public static TimeZone CurrentTimeZone
		{
			get
			{
				long ticks = DateTime.UtcNow.Ticks;
				TimeZone timeZone = TimeZone.currentTimeZone;
				object obj = TimeZone.tz_lock;
				lock (obj)
				{
					if (timeZone == null || Math.Abs(ticks - TimeZone.timezone_check) > 600000000L)
					{
						timeZone = new CurrentSystemTimeZone();
						TimeZone.timezone_check = ticks;
						TimeZone.currentTimeZone = timeZone;
					}
				}
				return timeZone;
			}
		}

		/// <summary>Gets the daylight saving time zone name.</summary>
		/// <returns>The daylight saving time zone name.</returns>
		// Token: 0x17000396 RID: 918
		// (get) Token: 0x06001AC2 RID: 6850
		public abstract string DaylightName { get; }

		/// <summary>Gets the standard time zone name.</summary>
		/// <returns>The standard time zone name.</returns>
		/// <exception cref="T:System.ArgumentNullException">An attempt was made to set this property to <see langword="null" />. </exception>
		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06001AC3 RID: 6851
		public abstract string StandardName { get; }

		/// <summary>Returns the daylight saving time period for a particular year.</summary>
		/// <param name="year">The year that the daylight saving time period applies to. </param>
		/// <returns>A <see cref="T:System.Globalization.DaylightTime" /> object that contains the start and end date for daylight saving time in <paramref name="year" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="year" /> is less than 1 or greater than 9999. </exception>
		// Token: 0x06001AC4 RID: 6852
		public abstract DaylightTime GetDaylightChanges(int year);

		/// <summary>Returns the Coordinated Universal Time (UTC) offset for the specified local time.</summary>
		/// <param name="time">A date and time value.</param>
		/// <returns>The Coordinated Universal Time (UTC) offset from <paramref name="time" />.</returns>
		// Token: 0x06001AC5 RID: 6853
		public abstract TimeSpan GetUtcOffset(DateTime time);

		/// <summary>Returns a value indicating whether the specified date and time is within a daylight saving time period.</summary>
		/// <param name="time">A date and time. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="time" /> is in a daylight saving time period; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001AC6 RID: 6854 RVA: 0x00065A04 File Offset: 0x00063C04
		public virtual bool IsDaylightSavingTime(DateTime time)
		{
			return TimeZone.IsDaylightSavingTime(time, this.GetDaylightChanges(time.Year));
		}

		/// <summary>Returns a value indicating whether the specified date and time is within the specified daylight saving time period.</summary>
		/// <param name="time">A date and time. </param>
		/// <param name="daylightTimes">A daylight saving time period. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="time" /> is in <paramref name="daylightTimes" />; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="daylightTimes" /> is <see langword="null" />. </exception>
		// Token: 0x06001AC7 RID: 6855 RVA: 0x00065A1C File Offset: 0x00063C1C
		public static bool IsDaylightSavingTime(DateTime time, DaylightTime daylightTimes)
		{
			if (daylightTimes == null)
			{
				throw new ArgumentNullException("daylightTimes");
			}
			if (daylightTimes.Start.Ticks == daylightTimes.End.Ticks)
			{
				return false;
			}
			if (daylightTimes.Start.Ticks < daylightTimes.End.Ticks)
			{
				if (daylightTimes.Start.Ticks < time.Ticks && daylightTimes.End.Ticks > time.Ticks)
				{
					return true;
				}
			}
			else if (time.Year == daylightTimes.Start.Year && time.Year == daylightTimes.End.Year && (time.Ticks < daylightTimes.End.Ticks || time.Ticks > daylightTimes.Start.Ticks))
			{
				return true;
			}
			return false;
		}

		/// <summary>Returns the local time that corresponds to a specified date and time value.</summary>
		/// <param name="time">A Coordinated Universal Time (UTC) time. </param>
		/// <returns>A <see cref="T:System.DateTime" /> object whose value is the local time that corresponds to <paramref name="time" />.</returns>
		// Token: 0x06001AC8 RID: 6856 RVA: 0x00065B04 File Offset: 0x00063D04
		public virtual DateTime ToLocalTime(DateTime time)
		{
			if (time.Kind == DateTimeKind.Local)
			{
				return time;
			}
			TimeSpan utcOffset = this.GetUtcOffset(new DateTime(time.Ticks));
			if (utcOffset.Ticks > 0L)
			{
				if (DateTime.MaxValue - utcOffset < time)
				{
					return DateTime.SpecifyKind(DateTime.MaxValue, DateTimeKind.Local);
				}
			}
			else if (utcOffset.Ticks < 0L && time.Ticks + utcOffset.Ticks < DateTime.MinValue.Ticks)
			{
				return DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
			}
			return DateTime.SpecifyKind(time.Add(utcOffset), DateTimeKind.Local);
		}

		/// <summary>Returns the Coordinated Universal Time (UTC) that corresponds to a specified time.</summary>
		/// <param name="time">A date and time. </param>
		/// <returns>A <see cref="T:System.DateTime" /> object whose value is the Coordinated Universal Time (UTC) that corresponds to <paramref name="time" />.</returns>
		// Token: 0x06001AC9 RID: 6857 RVA: 0x00065BA0 File Offset: 0x00063DA0
		public virtual DateTime ToUniversalTime(DateTime time)
		{
			if (time.Kind == DateTimeKind.Utc)
			{
				return time;
			}
			TimeSpan utcOffset = this.GetUtcOffset(time);
			if (utcOffset.Ticks < 0L)
			{
				if (DateTime.MaxValue + utcOffset < time)
				{
					return DateTime.SpecifyKind(DateTime.MaxValue, DateTimeKind.Utc);
				}
			}
			else if (utcOffset.Ticks > 0L && DateTime.MinValue + utcOffset > time)
			{
				return DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
			}
			return DateTime.SpecifyKind(new DateTime(time.Ticks - utcOffset.Ticks), DateTimeKind.Utc);
		}

		// Token: 0x06001ACA RID: 6858 RVA: 0x00065C2F File Offset: 0x00063E2F
		internal static void ClearCachedData()
		{
			TimeZone.currentTimeZone = null;
		}

		// Token: 0x06001ACB RID: 6859 RVA: 0x00065C37 File Offset: 0x00063E37
		// Note: this type is marked as 'beforefieldinit'.
		static TimeZone()
		{
		}

		// Token: 0x04000F04 RID: 3844
		private static TimeZone currentTimeZone;

		// Token: 0x04000F05 RID: 3845
		[NonSerialized]
		private static object tz_lock = new object();

		// Token: 0x04000F06 RID: 3846
		[NonSerialized]
		private static long timezone_check;
	}
}
