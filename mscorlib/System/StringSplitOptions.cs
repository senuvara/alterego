﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies whether applicable <see cref="Overload:System.String.Split" /> method overloads include or omit empty substrings from the return value.</summary>
	// Token: 0x020001B1 RID: 433
	[ComVisible(false)]
	[Flags]
	public enum StringSplitOptions
	{
		/// <summary>The return value includes array elements that contain an empty string</summary>
		// Token: 0x04000AAB RID: 2731
		None = 0,
		/// <summary>The return value does not include array elements that contain an empty string</summary>
		// Token: 0x04000AAC RID: 2732
		RemoveEmptyEntries = 1
	}
}
