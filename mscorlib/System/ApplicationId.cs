﻿using System;
using System.Runtime.InteropServices;
using System.Security.Util;
using System.Text;

namespace System
{
	/// <summary>Contains information used to uniquely identify a manifest-based application. This class cannot be inherited.</summary>
	// Token: 0x02000114 RID: 276
	[ComVisible(true)]
	[Serializable]
	public sealed class ApplicationId
	{
		// Token: 0x060009F9 RID: 2553 RVA: 0x00002050 File Offset: 0x00000250
		internal ApplicationId()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ApplicationId" /> class.</summary>
		/// <param name="publicKeyToken">The array of bytes representing the raw public key data. </param>
		/// <param name="name">The name of the application. </param>
		/// <param name="version">A <see cref="T:System.Version" /> object that specifies the version of the application. </param>
		/// <param name="processorArchitecture">The processor architecture of the application. </param>
		/// <param name="culture">The culture of the application. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name " />is <see langword="null" />.-or-
		///         <paramref name="version " />is <see langword="null" />.-or-
		///         <paramref name="publicKeyToken " />is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name " />is an empty string.</exception>
		// Token: 0x060009FA RID: 2554 RVA: 0x00031FFC File Offset: 0x000301FC
		public ApplicationId(byte[] publicKeyToken, string name, Version version, string processorArchitecture, string culture)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException(Environment.GetResourceString("ApplicationId cannot have an empty string for the name."));
			}
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			if (publicKeyToken == null)
			{
				throw new ArgumentNullException("publicKeyToken");
			}
			this.m_publicKeyToken = new byte[publicKeyToken.Length];
			Array.Copy(publicKeyToken, 0, this.m_publicKeyToken, 0, publicKeyToken.Length);
			this.m_name = name;
			this.m_version = version;
			this.m_processorArchitecture = processorArchitecture;
			this.m_culture = culture;
		}

		/// <summary>Gets the public key token for the application.</summary>
		/// <returns>A byte array containing the public key token for the application.</returns>
		// Token: 0x17000196 RID: 406
		// (get) Token: 0x060009FB RID: 2555 RVA: 0x00032094 File Offset: 0x00030294
		public byte[] PublicKeyToken
		{
			get
			{
				byte[] array = new byte[this.m_publicKeyToken.Length];
				Array.Copy(this.m_publicKeyToken, 0, array, 0, this.m_publicKeyToken.Length);
				return array;
			}
		}

		/// <summary>Gets the name of the application.</summary>
		/// <returns>The name of the application.</returns>
		// Token: 0x17000197 RID: 407
		// (get) Token: 0x060009FC RID: 2556 RVA: 0x000320C6 File Offset: 0x000302C6
		public string Name
		{
			get
			{
				return this.m_name;
			}
		}

		/// <summary>Gets the version of the application.</summary>
		/// <returns>A <see cref="T:System.Version" /> that specifies the version of the application.</returns>
		// Token: 0x17000198 RID: 408
		// (get) Token: 0x060009FD RID: 2557 RVA: 0x000320CE File Offset: 0x000302CE
		public Version Version
		{
			get
			{
				return this.m_version;
			}
		}

		/// <summary>Gets the target processor architecture for the application.</summary>
		/// <returns>The processor architecture of the application.</returns>
		// Token: 0x17000199 RID: 409
		// (get) Token: 0x060009FE RID: 2558 RVA: 0x000320D6 File Offset: 0x000302D6
		public string ProcessorArchitecture
		{
			get
			{
				return this.m_processorArchitecture;
			}
		}

		/// <summary>Gets a string representing the culture information for the application.</summary>
		/// <returns>The culture information for the application.</returns>
		// Token: 0x1700019A RID: 410
		// (get) Token: 0x060009FF RID: 2559 RVA: 0x000320DE File Offset: 0x000302DE
		public string Culture
		{
			get
			{
				return this.m_culture;
			}
		}

		/// <summary>Creates and returns an identical copy of the current application identity.</summary>
		/// <returns>An <see cref="T:System.ApplicationId" /> object that represents an exact copy of the original.</returns>
		// Token: 0x06000A00 RID: 2560 RVA: 0x000320E6 File Offset: 0x000302E6
		public ApplicationId Copy()
		{
			return new ApplicationId(this.m_publicKeyToken, this.m_name, this.m_version, this.m_processorArchitecture, this.m_culture);
		}

		/// <summary>Creates and returns a string representation of the application identity.</summary>
		/// <returns>A string representation of the application identity.</returns>
		// Token: 0x06000A01 RID: 2561 RVA: 0x0003210C File Offset: 0x0003030C
		public override string ToString()
		{
			StringBuilder stringBuilder = StringBuilderCache.Acquire(16);
			stringBuilder.Append(this.m_name);
			if (this.m_culture != null)
			{
				stringBuilder.Append(", culture=\"");
				stringBuilder.Append(this.m_culture);
				stringBuilder.Append("\"");
			}
			stringBuilder.Append(", version=\"");
			stringBuilder.Append(this.m_version.ToString());
			stringBuilder.Append("\"");
			if (this.m_publicKeyToken != null)
			{
				stringBuilder.Append(", publicKeyToken=\"");
				stringBuilder.Append(Hex.EncodeHexString(this.m_publicKeyToken));
				stringBuilder.Append("\"");
			}
			if (this.m_processorArchitecture != null)
			{
				stringBuilder.Append(", processorArchitecture =\"");
				stringBuilder.Append(this.m_processorArchitecture);
				stringBuilder.Append("\"");
			}
			return StringBuilderCache.GetStringAndRelease(stringBuilder);
		}

		/// <summary>Determines whether the specified <see cref="T:System.ApplicationId" /> object is equivalent to the current <see cref="T:System.ApplicationId" />.</summary>
		/// <param name="o">The <see cref="T:System.ApplicationId" /> object to compare to the current <see cref="T:System.ApplicationId" />. </param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.ApplicationId" /> object is equivalent to the current <see cref="T:System.ApplicationId" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000A02 RID: 2562 RVA: 0x000321EC File Offset: 0x000303EC
		public override bool Equals(object o)
		{
			ApplicationId applicationId = o as ApplicationId;
			if (applicationId == null)
			{
				return false;
			}
			if (!object.Equals(this.m_name, applicationId.m_name) || !object.Equals(this.m_version, applicationId.m_version) || !object.Equals(this.m_processorArchitecture, applicationId.m_processorArchitecture) || !object.Equals(this.m_culture, applicationId.m_culture))
			{
				return false;
			}
			if (this.m_publicKeyToken.Length != applicationId.m_publicKeyToken.Length)
			{
				return false;
			}
			for (int i = 0; i < this.m_publicKeyToken.Length; i++)
			{
				if (this.m_publicKeyToken[i] != applicationId.m_publicKeyToken[i])
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>Gets the hash code for the current application identity.</summary>
		/// <returns>The hash code for the current application identity.</returns>
		// Token: 0x06000A03 RID: 2563 RVA: 0x0003228F File Offset: 0x0003048F
		public override int GetHashCode()
		{
			return this.m_name.GetHashCode() ^ this.m_version.GetHashCode();
		}

		// Token: 0x04000761 RID: 1889
		private string m_name;

		// Token: 0x04000762 RID: 1890
		private Version m_version;

		// Token: 0x04000763 RID: 1891
		private string m_processorArchitecture;

		// Token: 0x04000764 RID: 1892
		private string m_culture;

		// Token: 0x04000765 RID: 1893
		internal byte[] m_publicKeyToken;
	}
}
