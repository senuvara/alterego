﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	// Token: 0x020001D7 RID: 471
	[Serializable]
	internal class UnitySerializationHolder : ISerializable, IObjectReference
	{
		// Token: 0x06001629 RID: 5673 RVA: 0x00058667 File Offset: 0x00056867
		internal static void GetUnitySerializationInfo(SerializationInfo info, Missing missing)
		{
			info.SetType(typeof(UnitySerializationHolder));
			info.AddValue("UnityType", 3);
		}

		// Token: 0x0600162A RID: 5674 RVA: 0x00058688 File Offset: 0x00056888
		internal static RuntimeType AddElementTypes(SerializationInfo info, RuntimeType type)
		{
			List<int> list = new List<int>();
			while (type.HasElementType)
			{
				if (type.IsSzArray)
				{
					list.Add(3);
				}
				else if (type.IsArray)
				{
					list.Add(type.GetArrayRank());
					list.Add(2);
				}
				else if (type.IsPointer)
				{
					list.Add(1);
				}
				else if (type.IsByRef)
				{
					list.Add(4);
				}
				type = (RuntimeType)type.GetElementType();
			}
			info.AddValue("ElementTypes", list.ToArray(), typeof(int[]));
			return type;
		}

		// Token: 0x0600162B RID: 5675 RVA: 0x0005871C File Offset: 0x0005691C
		internal Type MakeElementTypes(Type type)
		{
			for (int i = this.m_elementTypes.Length - 1; i >= 0; i--)
			{
				if (this.m_elementTypes[i] == 3)
				{
					type = type.MakeArrayType();
				}
				else if (this.m_elementTypes[i] == 2)
				{
					type = type.MakeArrayType(this.m_elementTypes[--i]);
				}
				else if (this.m_elementTypes[i] == 1)
				{
					type = type.MakePointerType();
				}
				else if (this.m_elementTypes[i] == 4)
				{
					type = type.MakeByRefType();
				}
			}
			return type;
		}

		// Token: 0x0600162C RID: 5676 RVA: 0x000587A0 File Offset: 0x000569A0
		internal static void GetUnitySerializationInfo(SerializationInfo info, RuntimeType type)
		{
			if (type.GetRootElementType().IsGenericParameter)
			{
				type = UnitySerializationHolder.AddElementTypes(info, type);
				info.SetType(typeof(UnitySerializationHolder));
				info.AddValue("UnityType", 7);
				info.AddValue("GenericParameterPosition", type.GenericParameterPosition);
				info.AddValue("DeclaringMethod", type.DeclaringMethod, typeof(MethodBase));
				info.AddValue("DeclaringType", type.DeclaringType, typeof(Type));
				return;
			}
			int unityType = 4;
			if (!type.IsGenericTypeDefinition && type.ContainsGenericParameters)
			{
				unityType = 8;
				type = UnitySerializationHolder.AddElementTypes(info, type);
				info.AddValue("GenericArguments", type.GetGenericArguments(), typeof(Type[]));
				type = (RuntimeType)type.GetGenericTypeDefinition();
			}
			UnitySerializationHolder.GetUnitySerializationInfo(info, unityType, type.FullName, type.GetRuntimeAssembly());
		}

		// Token: 0x0600162D RID: 5677 RVA: 0x00058880 File Offset: 0x00056A80
		internal static void GetUnitySerializationInfo(SerializationInfo info, int unityType, string data, RuntimeAssembly assembly)
		{
			info.SetType(typeof(UnitySerializationHolder));
			info.AddValue("Data", data, typeof(string));
			info.AddValue("UnityType", unityType);
			string value;
			if (assembly == null)
			{
				value = string.Empty;
			}
			else
			{
				value = assembly.FullName;
			}
			info.AddValue("AssemblyName", value);
		}

		// Token: 0x0600162E RID: 5678 RVA: 0x000588E4 File Offset: 0x00056AE4
		internal UnitySerializationHolder(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.m_unityType = info.GetInt32("UnityType");
			if (this.m_unityType == 3)
			{
				return;
			}
			if (this.m_unityType == 7)
			{
				this.m_declaringMethod = (info.GetValue("DeclaringMethod", typeof(MethodBase)) as MethodBase);
				this.m_declaringType = (info.GetValue("DeclaringType", typeof(Type)) as Type);
				this.m_genericParameterPosition = info.GetInt32("GenericParameterPosition");
				this.m_elementTypes = (info.GetValue("ElementTypes", typeof(int[])) as int[]);
				return;
			}
			if (this.m_unityType == 8)
			{
				this.m_instantiation = (info.GetValue("GenericArguments", typeof(Type[])) as Type[]);
				this.m_elementTypes = (info.GetValue("ElementTypes", typeof(int[])) as int[]);
			}
			this.m_data = info.GetString("Data");
			this.m_assemblyName = info.GetString("AssemblyName");
		}

		// Token: 0x0600162F RID: 5679 RVA: 0x00058A06 File Offset: 0x00056C06
		private void ThrowInsufficientInformation(string field)
		{
			throw new SerializationException(Environment.GetResourceString("Insufficient state to deserialize the object. Missing field '{0}'. More information is needed.", new object[]
			{
				field
			}));
		}

		// Token: 0x06001630 RID: 5680 RVA: 0x00058A21 File Offset: 0x00056C21
		[SecurityCritical]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotSupportedException(Environment.GetResourceString("The UnitySerializationHolder object is designed to transmit information about other types and is not serializable itself."));
		}

		// Token: 0x06001631 RID: 5681 RVA: 0x00058A34 File Offset: 0x00056C34
		[SecurityCritical]
		public virtual object GetRealObject(StreamingContext context)
		{
			switch (this.m_unityType)
			{
			case 1:
				return Empty.Value;
			case 2:
				return DBNull.Value;
			case 3:
				return Missing.Value;
			case 4:
				if (this.m_data == null || this.m_data.Length == 0)
				{
					this.ThrowInsufficientInformation("Data");
				}
				if (this.m_assemblyName == null)
				{
					this.ThrowInsufficientInformation("AssemblyName");
				}
				if (this.m_assemblyName.Length == 0)
				{
					return Type.GetType(this.m_data, true, false);
				}
				return Assembly.Load(this.m_assemblyName).GetType(this.m_data, true, false);
			case 5:
			{
				if (this.m_data == null || this.m_data.Length == 0)
				{
					this.ThrowInsufficientInformation("Data");
				}
				if (this.m_assemblyName == null)
				{
					this.ThrowInsufficientInformation("AssemblyName");
				}
				Module module = Assembly.Load(this.m_assemblyName).GetModule(this.m_data);
				if (module == null)
				{
					throw new SerializationException(Environment.GetResourceString("The given module {0} cannot be found within the assembly {1}.", new object[]
					{
						this.m_data,
						this.m_assemblyName
					}));
				}
				return module;
			}
			case 6:
				if (this.m_data == null || this.m_data.Length == 0)
				{
					this.ThrowInsufficientInformation("Data");
				}
				if (this.m_assemblyName == null)
				{
					this.ThrowInsufficientInformation("AssemblyName");
				}
				return Assembly.Load(this.m_assemblyName);
			case 7:
				if (this.m_declaringMethod == null && this.m_declaringType == null)
				{
					this.ThrowInsufficientInformation("DeclaringMember");
				}
				if (this.m_declaringMethod != null)
				{
					return this.m_declaringMethod.GetGenericArguments()[this.m_genericParameterPosition];
				}
				return this.MakeElementTypes(this.m_declaringType.GetGenericArguments()[this.m_genericParameterPosition]);
			case 8:
			{
				this.m_unityType = 4;
				Type type = this.GetRealObject(context) as Type;
				this.m_unityType = 8;
				if (this.m_instantiation[0] == null)
				{
					return null;
				}
				return this.MakeElementTypes(type.MakeGenericType(this.m_instantiation));
			}
			default:
				throw new ArgumentException(Environment.GetResourceString("Invalid Unity type."));
			}
		}

		// Token: 0x04000B8B RID: 2955
		internal const int EmptyUnity = 1;

		// Token: 0x04000B8C RID: 2956
		internal const int NullUnity = 2;

		// Token: 0x04000B8D RID: 2957
		internal const int MissingUnity = 3;

		// Token: 0x04000B8E RID: 2958
		internal const int RuntimeTypeUnity = 4;

		// Token: 0x04000B8F RID: 2959
		internal const int ModuleUnity = 5;

		// Token: 0x04000B90 RID: 2960
		internal const int AssemblyUnity = 6;

		// Token: 0x04000B91 RID: 2961
		internal const int GenericParameterTypeUnity = 7;

		// Token: 0x04000B92 RID: 2962
		internal const int PartialInstantiationTypeUnity = 8;

		// Token: 0x04000B93 RID: 2963
		internal const int Pointer = 1;

		// Token: 0x04000B94 RID: 2964
		internal const int Array = 2;

		// Token: 0x04000B95 RID: 2965
		internal const int SzArray = 3;

		// Token: 0x04000B96 RID: 2966
		internal const int ByRef = 4;

		// Token: 0x04000B97 RID: 2967
		private Type[] m_instantiation;

		// Token: 0x04000B98 RID: 2968
		private int[] m_elementTypes;

		// Token: 0x04000B99 RID: 2969
		private int m_genericParameterPosition;

		// Token: 0x04000B9A RID: 2970
		private Type m_declaringType;

		// Token: 0x04000B9B RID: 2971
		private MethodBase m_declaringMethod;

		// Token: 0x04000B9C RID: 2972
		private string m_data;

		// Token: 0x04000B9D RID: 2973
		private string m_assemblyName;

		// Token: 0x04000B9E RID: 2974
		private int m_unityType;
	}
}
