﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when there is an invalid attempt to access a method, such as accessing a private method from partially trusted code.</summary>
	// Token: 0x0200018A RID: 394
	[ComVisible(true)]
	[Serializable]
	public class MethodAccessException : MemberAccessException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.MethodAccessException" /> class, setting the <see cref="P:System.Exception.Message" /> property of the new instance to a system-supplied message that describes the error, such as "Attempt to access the method failed." This message takes into account the current system culture.</summary>
		// Token: 0x06001137 RID: 4407 RVA: 0x0004796D File Offset: 0x00045B6D
		public MethodAccessException() : base(Environment.GetResourceString("Attempt to access the method failed."))
		{
			base.SetErrorCode(-2146233072);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MethodAccessException" /> class with a specified error message.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. </param>
		// Token: 0x06001138 RID: 4408 RVA: 0x0004798A File Offset: 0x00045B8A
		public MethodAccessException(string message) : base(message)
		{
			base.SetErrorCode(-2146233072);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MethodAccessException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06001139 RID: 4409 RVA: 0x0004799E File Offset: 0x00045B9E
		public MethodAccessException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233072);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MethodAccessException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x0600113A RID: 4410 RVA: 0x0003E414 File Offset: 0x0003C614
		protected MethodAccessException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
