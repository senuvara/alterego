﻿using System;
using System.Buffers;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020000BA RID: 186
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	[DebuggerTypeProxy(typeof(MemoryDebugView<>))]
	public readonly struct ReadOnlyMemory<T>
	{
		// Token: 0x06000638 RID: 1592 RVA: 0x00022062 File Offset: 0x00020262
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ReadOnlyMemory(T[] array)
		{
			if (array == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
			}
			this._arrayOrOwnedMemory = array;
			this._index = 0;
			this._length = array.Length;
		}

		// Token: 0x06000639 RID: 1593 RVA: 0x00022084 File Offset: 0x00020284
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ReadOnlyMemory(T[] array, int start, int length)
		{
			if (array == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
			}
			if (start > array.Length || length > array.Length - start)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			this._arrayOrOwnedMemory = array;
			this._index = start;
			this._length = length;
		}

		// Token: 0x0600063A RID: 1594 RVA: 0x000220B9 File Offset: 0x000202B9
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal ReadOnlyMemory(OwnedMemory<T> owner, int index, int length)
		{
			if (owner == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.ownedMemory);
			}
			if (index < 0 || length < 0)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			this._arrayOrOwnedMemory = owner;
			this._index = (index | int.MinValue);
			this._length = length;
		}

		// Token: 0x17000116 RID: 278
		// (get) Token: 0x0600063B RID: 1595 RVA: 0x000220EF File Offset: 0x000202EF
		private string DebuggerDisplay
		{
			get
			{
				return string.Format("{{{0}[{1}]}}", typeof(T).Name, this._length);
			}
		}

		// Token: 0x0600063C RID: 1596 RVA: 0x00022115 File Offset: 0x00020315
		public static implicit operator ReadOnlyMemory<T>(T[] array)
		{
			return new ReadOnlyMemory<T>(array);
		}

		// Token: 0x0600063D RID: 1597 RVA: 0x0002211D File Offset: 0x0002031D
		public static implicit operator ReadOnlyMemory<T>(ArraySegment<T> arraySegment)
		{
			return new ReadOnlyMemory<T>(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
		}

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x0600063E RID: 1598 RVA: 0x00022139 File Offset: 0x00020339
		public static ReadOnlyMemory<T> Empty
		{
			[CompilerGenerated]
			get
			{
				return ReadOnlyMemory<T>.<Empty>k__BackingField;
			}
		} = SpanHelpers.PerTypeValues<T>.EmptyArray;

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x0600063F RID: 1599 RVA: 0x00022140 File Offset: 0x00020340
		public int Length
		{
			get
			{
				return this._length;
			}
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x06000640 RID: 1600 RVA: 0x00022148 File Offset: 0x00020348
		public bool IsEmpty
		{
			get
			{
				return this._length == 0;
			}
		}

		// Token: 0x06000641 RID: 1601 RVA: 0x00022154 File Offset: 0x00020354
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ReadOnlyMemory<T> Slice(int start)
		{
			if (start > this._length)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			if (this._index < 0)
			{
				return new ReadOnlyMemory<T>((OwnedMemory<T>)this._arrayOrOwnedMemory, (this._index & int.MaxValue) + start, this._length - start);
			}
			return new ReadOnlyMemory<T>((T[])this._arrayOrOwnedMemory, this._index + start, this._length - start);
		}

		// Token: 0x06000642 RID: 1602 RVA: 0x000221C4 File Offset: 0x000203C4
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ReadOnlyMemory<T> Slice(int start, int length)
		{
			if (start > this._length || length > this._length - start)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			if (this._index < 0)
			{
				return new ReadOnlyMemory<T>((OwnedMemory<T>)this._arrayOrOwnedMemory, (this._index & int.MaxValue) + start, length);
			}
			return new ReadOnlyMemory<T>((T[])this._arrayOrOwnedMemory, this._index + start, length);
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x06000643 RID: 1603 RVA: 0x00022230 File Offset: 0x00020430
		public ReadOnlySpan<T> Span
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if (this._index < 0)
				{
					return ((OwnedMemory<T>)this._arrayOrOwnedMemory).Span.Slice(this._index & int.MaxValue, this._length);
				}
				return new ReadOnlySpan<T>((T[])this._arrayOrOwnedMemory, this._index, this._length);
			}
		}

		// Token: 0x06000644 RID: 1604 RVA: 0x00022294 File Offset: 0x00020494
		public unsafe MemoryHandle Retain(bool pin = false)
		{
			MemoryHandle result;
			if (pin)
			{
				if (this._index < 0)
				{
					result = ((OwnedMemory<T>)this._arrayOrOwnedMemory).Pin();
					result.AddOffset((this._index & int.MaxValue) * Unsafe.SizeOf<T>());
				}
				else
				{
					GCHandle handle = GCHandle.Alloc((T[])this._arrayOrOwnedMemory, GCHandleType.Pinned);
					void* pinnedPointer = Unsafe.Add<T>((void*)handle.AddrOfPinnedObject(), this._index);
					result = new MemoryHandle(null, pinnedPointer, handle);
				}
			}
			else if (this._index < 0)
			{
				((OwnedMemory<T>)this._arrayOrOwnedMemory).Retain();
				result = new MemoryHandle((OwnedMemory<T>)this._arrayOrOwnedMemory, null, default(GCHandle));
			}
			else
			{
				result = new MemoryHandle(null, null, default(GCHandle));
			}
			return result;
		}

		// Token: 0x06000645 RID: 1605 RVA: 0x00022360 File Offset: 0x00020560
		public bool DangerousTryGetArray(out ArraySegment<T> arraySegment)
		{
			if (this._index >= 0)
			{
				arraySegment = new ArraySegment<T>((T[])this._arrayOrOwnedMemory, this._index, this._length);
				return true;
			}
			ArraySegment<T> arraySegment2;
			if (((OwnedMemory<T>)this._arrayOrOwnedMemory).TryGetArray(out arraySegment2))
			{
				arraySegment = new ArraySegment<T>(arraySegment2.Array, arraySegment2.Offset + (this._index & int.MaxValue), this._length);
				return true;
			}
			arraySegment = default(ArraySegment<T>);
			return false;
		}

		// Token: 0x06000646 RID: 1606 RVA: 0x000223E4 File Offset: 0x000205E4
		public T[] ToArray()
		{
			return this.Span.ToArray();
		}

		// Token: 0x06000647 RID: 1607 RVA: 0x00022400 File Offset: 0x00020600
		public override bool Equals(object obj)
		{
			bool flag = obj is ReadOnlyMemory<T>;
			ReadOnlyMemory<T> other = flag ? ((ReadOnlyMemory<T>)obj) : default(ReadOnlyMemory<T>);
			if (flag)
			{
				return this.Equals(other);
			}
			bool flag2 = obj is Memory<T>;
			Memory<T> memory = flag2 ? ((Memory<T>)obj) : default(Memory<T>);
			return flag2 && this.Equals(memory);
		}

		// Token: 0x06000648 RID: 1608 RVA: 0x00022468 File Offset: 0x00020668
		public bool Equals(ReadOnlyMemory<T> other)
		{
			return this._arrayOrOwnedMemory == other._arrayOrOwnedMemory && this._index == other._index && this._length == other._length;
		}

		// Token: 0x06000649 RID: 1609 RVA: 0x00022498 File Offset: 0x00020698
		public override int GetHashCode()
		{
			return ReadOnlyMemory<T>.CombineHashCodes(this._arrayOrOwnedMemory.GetHashCode(), (this._index & int.MaxValue).GetHashCode(), this._length.GetHashCode());
		}

		// Token: 0x0600064A RID: 1610 RVA: 0x00021FC3 File Offset: 0x000201C3
		private static int CombineHashCodes(int left, int right)
		{
			return (left << 5) + left ^ right;
		}

		// Token: 0x0600064B RID: 1611 RVA: 0x000224D7 File Offset: 0x000206D7
		private static int CombineHashCodes(int h1, int h2, int h3)
		{
			return ReadOnlyMemory<T>.CombineHashCodes(ReadOnlyMemory<T>.CombineHashCodes(h1, h2), h3);
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x000224E6 File Offset: 0x000206E6
		// Note: this type is marked as 'beforefieldinit'.
		static ReadOnlyMemory()
		{
		}

		// Token: 0x04000663 RID: 1635
		private readonly object _arrayOrOwnedMemory;

		// Token: 0x04000664 RID: 1636
		private readonly int _index;

		// Token: 0x04000665 RID: 1637
		private readonly int _length;

		// Token: 0x04000666 RID: 1638
		private const int RemoveOwnedFlagBitMask = 2147483647;

		// Token: 0x04000667 RID: 1639
		[CompilerGenerated]
		private static readonly ReadOnlyMemory<T> <Empty>k__BackingField;
	}
}
