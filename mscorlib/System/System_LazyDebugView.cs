﻿using System;
using System.Threading;

namespace System
{
	// Token: 0x020000F1 RID: 241
	internal sealed class System_LazyDebugView<T>
	{
		// Token: 0x0600094C RID: 2380 RVA: 0x0003104E File Offset: 0x0002F24E
		public System_LazyDebugView(Lazy<T> lazy)
		{
			this.m_lazy = lazy;
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x0600094D RID: 2381 RVA: 0x0003105D File Offset: 0x0002F25D
		public bool IsValueCreated
		{
			get
			{
				return this.m_lazy.IsValueCreated;
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x0600094E RID: 2382 RVA: 0x0003106A File Offset: 0x0002F26A
		public T Value
		{
			get
			{
				return this.m_lazy.ValueForDebugDisplay;
			}
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x0600094F RID: 2383 RVA: 0x00031077 File Offset: 0x0002F277
		public LazyThreadSafetyMode Mode
		{
			get
			{
				return this.m_lazy.Mode;
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000950 RID: 2384 RVA: 0x00031084 File Offset: 0x0002F284
		public bool IsValueFaulted
		{
			get
			{
				return this.m_lazy.IsValueFaulted;
			}
		}

		// Token: 0x040006E0 RID: 1760
		private readonly Lazy<T> m_lazy;
	}
}
