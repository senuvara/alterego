﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Security;
using System.Security.Permissions;

namespace System
{
	/// <summary>Enables access to objects across application domain boundaries in applications that support remoting.</summary>
	// Token: 0x0200020D RID: 525
	[ComVisible(true)]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public abstract class MarshalByRefObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.MarshalByRefObject" /> class. </summary>
		// Token: 0x06001919 RID: 6425 RVA: 0x00002050 File Offset: 0x00000250
		protected MarshalByRefObject()
		{
		}

		// Token: 0x17000353 RID: 851
		// (get) Token: 0x0600191A RID: 6426 RVA: 0x000175EA File Offset: 0x000157EA
		// (set) Token: 0x0600191B RID: 6427 RVA: 0x000175EA File Offset: 0x000157EA
		internal ServerIdentity ObjectIdentity
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Creates an object that contains all the relevant information required to generate a proxy used to communicate with a remote object.</summary>
		/// <param name="requestedType">The <see cref="T:System.Type" /> of the object that the new <see cref="T:System.Runtime.Remoting.ObjRef" /> will reference. </param>
		/// <returns>Information required to generate a proxy.</returns>
		/// <exception cref="T:System.Runtime.Remoting.RemotingException">This instance is not a valid remoting object. </exception>
		/// <exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		// Token: 0x0600191C RID: 6428 RVA: 0x000175EA File Offset: 0x000157EA
		[SecurityPermission(SecurityAction.LinkDemand, Infrastructure = true)]
		public virtual ObjRef CreateObjRef(Type requestedType)
		{
			throw new NotSupportedException();
		}

		/// <summary>Retrieves the current lifetime service object that controls the lifetime policy for this instance.</summary>
		/// <returns>An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease" /> used to control the lifetime policy for this instance.</returns>
		/// <exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		// Token: 0x0600191D RID: 6429 RVA: 0x000175EA File Offset: 0x000157EA
		[SecurityCritical]
		[SecurityPermission(SecurityAction.LinkDemand, Infrastructure = true)]
		public object GetLifetimeService()
		{
			throw new NotSupportedException();
		}

		/// <summary>Obtains a lifetime service object to control the lifetime policy for this instance.</summary>
		/// <returns>An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease" /> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime" /> property.</returns>
		/// <exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		// Token: 0x0600191E RID: 6430 RVA: 0x000175EA File Offset: 0x000157EA
		[SecurityPermission(SecurityAction.LinkDemand, Infrastructure = true)]
		public virtual object InitializeLifetimeService()
		{
			throw new NotSupportedException();
		}

		/// <summary>Creates a shallow copy of the current <see cref="T:System.MarshalByRefObject" /> object.</summary>
		/// <param name="cloneIdentity">
		///       <see langword="false" /> to delete the current <see cref="T:System.MarshalByRefObject" /> object's identity, which will cause the object to be assigned a new identity when it is marshaled across a remoting boundary. A value of <see langword="false" /> is usually appropriate. <see langword="true" /> to copy the current <see cref="T:System.MarshalByRefObject" /> object's identity to its clone, which will cause remoting client calls to be routed to the remote server object. </param>
		/// <returns>A shallow copy of the current <see cref="T:System.MarshalByRefObject" /> object.</returns>
		// Token: 0x0600191F RID: 6431 RVA: 0x000175EA File Offset: 0x000157EA
		protected MarshalByRefObject MemberwiseClone(bool cloneIdentity)
		{
			throw new NotSupportedException();
		}

		// Token: 0x04000C88 RID: 3208
		[NonSerialized]
		private object _identity;
	}
}
