﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace System
{
	// Token: 0x02000219 RID: 537
	internal sealed class NumberFormatter
	{
		// Token: 0x06001999 RID: 6553
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void GetFormatterTables(out ulong* MantissaBitsTable, out int* TensExponentTable, out char* DigitLowerTable, out char* DigitUpperTable, out long* TenPowersList, out int* DecHexDigits);

		// Token: 0x0600199A RID: 6554 RVA: 0x0005F45D File Offset: 0x0005D65D
		static NumberFormatter()
		{
			NumberFormatter.GetFormatterTables(out NumberFormatter.MantissaBitsTable, out NumberFormatter.TensExponentTable, out NumberFormatter.DigitLowerTable, out NumberFormatter.DigitUpperTable, out NumberFormatter.TenPowersList, out NumberFormatter.DecHexDigits);
		}

		// Token: 0x0600199B RID: 6555 RVA: 0x0005F482 File Offset: 0x0005D682
		private unsafe static long GetTenPowerOf(int i)
		{
			return NumberFormatter.TenPowersList[i];
		}

		// Token: 0x0600199C RID: 6556 RVA: 0x0005F490 File Offset: 0x0005D690
		private void InitDecHexDigits(uint value)
		{
			if (value >= 100000000U)
			{
				int num = (int)(value / 100000000U);
				value -= (uint)(100000000 * num);
				this._val2 = NumberFormatter.FastToDecHex(num);
			}
			this._val1 = NumberFormatter.ToDecHex((int)value);
		}

		// Token: 0x0600199D RID: 6557 RVA: 0x0005F4D0 File Offset: 0x0005D6D0
		private void InitDecHexDigits(ulong value)
		{
			if (value >= 100000000UL)
			{
				long num = (long)(value / 100000000UL);
				value -= (ulong)(100000000L * num);
				if (num >= 100000000L)
				{
					int num2 = (int)(num / 100000000L);
					num -= (long)num2 * 100000000L;
					this._val3 = NumberFormatter.ToDecHex(num2);
				}
				if (num != 0L)
				{
					this._val2 = NumberFormatter.ToDecHex((int)num);
				}
			}
			if (value != 0UL)
			{
				this._val1 = NumberFormatter.ToDecHex((int)value);
			}
		}

		// Token: 0x0600199E RID: 6558 RVA: 0x0005F548 File Offset: 0x0005D748
		private void InitDecHexDigits(uint hi, ulong lo)
		{
			if (hi == 0U)
			{
				this.InitDecHexDigits(lo);
				return;
			}
			uint num = hi / 100000000U;
			ulong num2 = (ulong)(hi - num * 100000000U);
			ulong num3 = lo / 100000000UL;
			ulong num4 = lo - num3 * 100000000UL + num2 * 9551616UL;
			hi = num;
			lo = num3 + num2 * 184467440737UL;
			num3 = num4 / 100000000UL;
			num4 -= num3 * 100000000UL;
			lo += num3;
			this._val1 = NumberFormatter.ToDecHex((int)num4);
			num3 = lo / 100000000UL;
			num4 = lo - num3 * 100000000UL;
			lo = num3;
			if (hi != 0U)
			{
				lo += (ulong)hi * 184467440737UL;
				num4 += (ulong)hi * 9551616UL;
				num3 = num4 / 100000000UL;
				lo += num3;
				num4 -= num3 * 100000000UL;
			}
			this._val2 = NumberFormatter.ToDecHex((int)num4);
			if (lo >= 100000000UL)
			{
				num3 = lo / 100000000UL;
				lo -= num3 * 100000000UL;
				this._val4 = NumberFormatter.ToDecHex((int)num3);
			}
			this._val3 = NumberFormatter.ToDecHex((int)lo);
		}

		// Token: 0x0600199F RID: 6559 RVA: 0x0005F65C File Offset: 0x0005D85C
		private unsafe static uint FastToDecHex(int val)
		{
			if (val < 100)
			{
				return (uint)NumberFormatter.DecHexDigits[val];
			}
			int num = val * 5243 >> 19;
			return (uint)(NumberFormatter.DecHexDigits[num] << 8 | NumberFormatter.DecHexDigits[val - num * 100]);
		}

		// Token: 0x060019A0 RID: 6560 RVA: 0x0005F6A4 File Offset: 0x0005D8A4
		private static uint ToDecHex(int val)
		{
			uint num = 0U;
			if (val >= 10000)
			{
				int num2 = val / 10000;
				val -= num2 * 10000;
				num = NumberFormatter.FastToDecHex(num2) << 16;
			}
			return num | NumberFormatter.FastToDecHex(val);
		}

		// Token: 0x060019A1 RID: 6561 RVA: 0x0005F6E0 File Offset: 0x0005D8E0
		private static int FastDecHexLen(int val)
		{
			if (val < 256)
			{
				if (val < 16)
				{
					return 1;
				}
				return 2;
			}
			else
			{
				if (val < 4096)
				{
					return 3;
				}
				return 4;
			}
		}

		// Token: 0x060019A2 RID: 6562 RVA: 0x0005F6FE File Offset: 0x0005D8FE
		private static int DecHexLen(uint val)
		{
			if (val < 65536U)
			{
				return NumberFormatter.FastDecHexLen((int)val);
			}
			return 4 + NumberFormatter.FastDecHexLen((int)(val >> 16));
		}

		// Token: 0x060019A3 RID: 6563 RVA: 0x0005F71C File Offset: 0x0005D91C
		private int DecHexLen()
		{
			if (this._val4 != 0U)
			{
				return NumberFormatter.DecHexLen(this._val4) + 24;
			}
			if (this._val3 != 0U)
			{
				return NumberFormatter.DecHexLen(this._val3) + 16;
			}
			if (this._val2 != 0U)
			{
				return NumberFormatter.DecHexLen(this._val2) + 8;
			}
			if (this._val1 != 0U)
			{
				return NumberFormatter.DecHexLen(this._val1);
			}
			return 0;
		}

		// Token: 0x060019A4 RID: 6564 RVA: 0x0005F784 File Offset: 0x0005D984
		private static int ScaleOrder(long hi)
		{
			for (int i = 18; i >= 0; i--)
			{
				if (hi >= NumberFormatter.GetTenPowerOf(i))
				{
					return i + 1;
				}
			}
			return 1;
		}

		// Token: 0x060019A5 RID: 6565 RVA: 0x0005F7AC File Offset: 0x0005D9AC
		private int InitialFloatingPrecision()
		{
			if (this._specifier == 'R')
			{
				return this._defPrecision + 2;
			}
			if (this._precision < this._defPrecision)
			{
				return this._defPrecision;
			}
			if (this._specifier == 'G')
			{
				return Math.Min(this._defPrecision + 2, this._precision);
			}
			if (this._specifier == 'E')
			{
				return Math.Min(this._defPrecision + 2, this._precision + 1);
			}
			return this._defPrecision;
		}

		// Token: 0x060019A6 RID: 6566 RVA: 0x0005F828 File Offset: 0x0005DA28
		private static int ParsePrecision(string format)
		{
			int num = 0;
			for (int i = 1; i < format.Length; i++)
			{
				int num2 = (int)(format[i] - '0');
				num = num * 10 + num2;
				if (num2 < 0 || num2 > 9 || num > 99)
				{
					return -2;
				}
			}
			return num;
		}

		// Token: 0x060019A7 RID: 6567 RVA: 0x0005F86C File Offset: 0x0005DA6C
		private NumberFormatter(Thread current)
		{
			this._cbuf = EmptyArray<char>.Value;
			if (current == null)
			{
				return;
			}
			this.CurrentCulture = current.CurrentCulture;
		}

		// Token: 0x060019A8 RID: 6568 RVA: 0x0005F890 File Offset: 0x0005DA90
		private void Init(string format)
		{
			this._val1 = (this._val2 = (this._val3 = (this._val4 = 0U)));
			this._offset = 0;
			this._NaN = (this._infinity = false);
			this._isCustomFormat = false;
			this._specifierIsUpper = true;
			this._precision = -1;
			if (format == null || format.Length == 0)
			{
				this._specifier = 'G';
				return;
			}
			char c = format[0];
			if (c >= 'a' && c <= 'z')
			{
				c = c - 'a' + 'A';
				this._specifierIsUpper = false;
			}
			else if (c < 'A' || c > 'Z')
			{
				this._isCustomFormat = true;
				this._specifier = '0';
				return;
			}
			this._specifier = c;
			if (format.Length > 1)
			{
				this._precision = NumberFormatter.ParsePrecision(format);
				if (this._precision == -2)
				{
					this._isCustomFormat = true;
					this._specifier = '0';
					this._precision = -1;
				}
			}
		}

		// Token: 0x060019A9 RID: 6569 RVA: 0x0005F97C File Offset: 0x0005DB7C
		private void InitHex(ulong value)
		{
			int defPrecision = this._defPrecision;
			if (defPrecision == 10)
			{
				value = (ulong)((uint)value);
			}
			this._val1 = (uint)value;
			this._val2 = (uint)(value >> 32);
			this._decPointPos = (this._digitsLen = this.DecHexLen());
			if (value == 0UL)
			{
				this._decPointPos = 1;
			}
		}

		// Token: 0x060019AA RID: 6570 RVA: 0x0005F9CC File Offset: 0x0005DBCC
		private void Init(string format, int value, int defPrecision)
		{
			this.Init(format);
			this._defPrecision = defPrecision;
			this._positive = (value >= 0);
			if (value == 0 || this._specifier == 'X')
			{
				this.InitHex((ulong)((long)value));
				return;
			}
			if (value < 0)
			{
				value = -value;
			}
			this.InitDecHexDigits((uint)value);
			this._decPointPos = (this._digitsLen = this.DecHexLen());
		}

		// Token: 0x060019AB RID: 6571 RVA: 0x0005FA30 File Offset: 0x0005DC30
		private void Init(string format, uint value, int defPrecision)
		{
			this.Init(format);
			this._defPrecision = defPrecision;
			this._positive = true;
			if (value == 0U || this._specifier == 'X')
			{
				this.InitHex((ulong)value);
				return;
			}
			this.InitDecHexDigits(value);
			this._decPointPos = (this._digitsLen = this.DecHexLen());
		}

		// Token: 0x060019AC RID: 6572 RVA: 0x0005FA84 File Offset: 0x0005DC84
		private void Init(string format, long value)
		{
			this.Init(format);
			this._defPrecision = 19;
			this._positive = (value >= 0L);
			if (value == 0L || this._specifier == 'X')
			{
				this.InitHex((ulong)value);
				return;
			}
			if (value < 0L)
			{
				value = -value;
			}
			this.InitDecHexDigits((ulong)value);
			this._decPointPos = (this._digitsLen = this.DecHexLen());
		}

		// Token: 0x060019AD RID: 6573 RVA: 0x0005FAE8 File Offset: 0x0005DCE8
		private void Init(string format, ulong value)
		{
			this.Init(format);
			this._defPrecision = 20;
			this._positive = true;
			if (value == 0UL || this._specifier == 'X')
			{
				this.InitHex(value);
				return;
			}
			this.InitDecHexDigits(value);
			this._decPointPos = (this._digitsLen = this.DecHexLen());
		}

		// Token: 0x060019AE RID: 6574 RVA: 0x0005FB3C File Offset: 0x0005DD3C
		private unsafe void Init(string format, double value, int defPrecision)
		{
			this.Init(format);
			this._defPrecision = defPrecision;
			long num = BitConverter.DoubleToInt64Bits(value);
			this._positive = (num >= 0L);
			num &= long.MaxValue;
			if (num == 0L)
			{
				this._decPointPos = 1;
				this._digitsLen = 0;
				this._positive = true;
				return;
			}
			int num2 = (int)(num >> 52);
			long num3 = num & 4503599627370495L;
			if (num2 == 2047)
			{
				this._NaN = (num3 != 0L);
				this._infinity = (num3 == 0L);
				return;
			}
			int num4 = 0;
			if (num2 == 0)
			{
				num2 = 1;
				int num5 = NumberFormatter.ScaleOrder(num3);
				if (num5 < 15)
				{
					num4 = num5 - 15;
					num3 *= NumberFormatter.GetTenPowerOf(-num4);
				}
			}
			else
			{
				num3 = (num3 + 4503599627370495L + 1L) * 10L;
				num4 = -1;
			}
			ulong num6 = (ulong)((uint)num3);
			ulong num7 = (ulong)num3 >> 32;
			ulong num8 = NumberFormatter.MantissaBitsTable[num2];
			ulong num9 = num8 >> 32;
			num8 = (ulong)((uint)num8);
			ulong num10 = num7 * num8 + num6 * num9 + (num6 * num8 >> 32);
			long num11 = (long)(num7 * num9 + (num10 >> 32));
			while (num11 < 10000000000000000L)
			{
				num10 = (num10 & (ulong)-1) * 10UL;
				num11 = num11 * 10L + (long)(num10 >> 32);
				num4--;
			}
			if ((num10 & (ulong)-2147483648) != 0UL)
			{
				num11 += 1L;
			}
			int num12 = 17;
			this._decPointPos = NumberFormatter.TensExponentTable[num2] + num4 + num12;
			int num13 = this.InitialFloatingPrecision();
			if (num12 > num13)
			{
				long tenPowerOf = NumberFormatter.GetTenPowerOf(num12 - num13);
				num11 = (num11 + (tenPowerOf >> 1)) / tenPowerOf;
				num12 = num13;
			}
			if (num11 >= NumberFormatter.GetTenPowerOf(num12))
			{
				num12++;
				this._decPointPos++;
			}
			this.InitDecHexDigits((ulong)num11);
			this._offset = this.CountTrailingZeros();
			this._digitsLen = num12 - this._offset;
		}

		// Token: 0x060019AF RID: 6575 RVA: 0x0005FD08 File Offset: 0x0005DF08
		private void Init(string format, decimal value)
		{
			this.Init(format);
			this._defPrecision = 100;
			int[] bits = decimal.GetBits(value);
			int num = (bits[3] & 2031616) >> 16;
			this._positive = (bits[3] >= 0);
			if (bits[0] == 0 && bits[1] == 0 && bits[2] == 0)
			{
				this._decPointPos = -num;
				this._positive = true;
				this._digitsLen = 0;
				return;
			}
			this.InitDecHexDigits((uint)bits[2], (ulong)((long)bits[1] << 32 | (long)((ulong)bits[0])));
			this._digitsLen = this.DecHexLen();
			this._decPointPos = this._digitsLen - num;
			if (this._precision != -1 || this._specifier != 'G')
			{
				this._offset = this.CountTrailingZeros();
				this._digitsLen -= this._offset;
			}
		}

		// Token: 0x060019B0 RID: 6576 RVA: 0x0005FDCE File Offset: 0x0005DFCE
		private void ResetCharBuf(int size)
		{
			this._ind = 0;
			if (this._cbuf.Length < size)
			{
				this._cbuf = new char[size];
			}
		}

		// Token: 0x060019B1 RID: 6577 RVA: 0x0005FDEE File Offset: 0x0005DFEE
		private void Resize(int len)
		{
			Array.Resize<char>(ref this._cbuf, len);
		}

		// Token: 0x060019B2 RID: 6578 RVA: 0x0005FDFC File Offset: 0x0005DFFC
		private void Append(char c)
		{
			if (this._ind == this._cbuf.Length)
			{
				this.Resize(this._ind + 10);
			}
			char[] cbuf = this._cbuf;
			int ind = this._ind;
			this._ind = ind + 1;
			cbuf[ind] = c;
		}

		// Token: 0x060019B3 RID: 6579 RVA: 0x0005FE44 File Offset: 0x0005E044
		private void Append(char c, int cnt)
		{
			if (this._ind + cnt > this._cbuf.Length)
			{
				this.Resize(this._ind + cnt + 10);
			}
			while (cnt-- > 0)
			{
				char[] cbuf = this._cbuf;
				int ind = this._ind;
				this._ind = ind + 1;
				cbuf[ind] = c;
			}
		}

		// Token: 0x060019B4 RID: 6580 RVA: 0x0005FE98 File Offset: 0x0005E098
		private void Append(string s)
		{
			int length = s.Length;
			if (this._ind + length > this._cbuf.Length)
			{
				this.Resize(this._ind + length + 10);
			}
			for (int i = 0; i < length; i++)
			{
				char[] cbuf = this._cbuf;
				int ind = this._ind;
				this._ind = ind + 1;
				cbuf[ind] = s[i];
			}
		}

		// Token: 0x060019B5 RID: 6581 RVA: 0x0005FEFA File Offset: 0x0005E0FA
		private NumberFormatInfo GetNumberFormatInstance(IFormatProvider fp)
		{
			if (this._nfi != null && fp == null)
			{
				return this._nfi;
			}
			return NumberFormatInfo.GetInstance(fp);
		}

		// Token: 0x1700036F RID: 879
		// (set) Token: 0x060019B6 RID: 6582 RVA: 0x0005FF14 File Offset: 0x0005E114
		private CultureInfo CurrentCulture
		{
			set
			{
				if (value != null && value.IsReadOnly)
				{
					this._nfi = value.NumberFormat;
					return;
				}
				this._nfi = null;
			}
		}

		// Token: 0x17000370 RID: 880
		// (get) Token: 0x060019B7 RID: 6583 RVA: 0x0005FF35 File Offset: 0x0005E135
		private int IntegerDigits
		{
			get
			{
				if (this._decPointPos <= 0)
				{
					return 1;
				}
				return this._decPointPos;
			}
		}

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x060019B8 RID: 6584 RVA: 0x0005FF48 File Offset: 0x0005E148
		private int DecimalDigits
		{
			get
			{
				if (this._digitsLen <= this._decPointPos)
				{
					return 0;
				}
				return this._digitsLen - this._decPointPos;
			}
		}

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x060019B9 RID: 6585 RVA: 0x0005FF67 File Offset: 0x0005E167
		private bool IsFloatingSource
		{
			get
			{
				return this._defPrecision == 15 || this._defPrecision == 7;
			}
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x060019BA RID: 6586 RVA: 0x0005FF7E File Offset: 0x0005E17E
		private bool IsZero
		{
			get
			{
				return this._digitsLen == 0;
			}
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x060019BB RID: 6587 RVA: 0x0005FF89 File Offset: 0x0005E189
		private bool IsZeroInteger
		{
			get
			{
				return this._digitsLen == 0 || this._decPointPos <= 0;
			}
		}

		// Token: 0x060019BC RID: 6588 RVA: 0x0005FFA1 File Offset: 0x0005E1A1
		private void RoundPos(int pos)
		{
			this.RoundBits(this._digitsLen - pos);
		}

		// Token: 0x060019BD RID: 6589 RVA: 0x0005FFB2 File Offset: 0x0005E1B2
		private bool RoundDecimal(int decimals)
		{
			return this.RoundBits(this._digitsLen - this._decPointPos - decimals);
		}

		// Token: 0x060019BE RID: 6590 RVA: 0x0005FFCC File Offset: 0x0005E1CC
		private bool RoundBits(int shift)
		{
			if (shift <= 0)
			{
				return false;
			}
			if (shift > this._digitsLen)
			{
				this._digitsLen = 0;
				this._decPointPos = 1;
				this._val1 = (this._val2 = (this._val3 = (this._val4 = 0U)));
				this._positive = true;
				return false;
			}
			shift += this._offset;
			this._digitsLen += this._offset;
			while (shift > 8)
			{
				this._val1 = this._val2;
				this._val2 = this._val3;
				this._val3 = this._val4;
				this._val4 = 0U;
				this._digitsLen -= 8;
				shift -= 8;
			}
			shift = shift - 1 << 2;
			uint num = this._val1 >> shift;
			uint num2 = num & 15U;
			this._val1 = (num ^ num2) << shift;
			bool result = false;
			if (num2 >= 5U)
			{
				this._val1 |= 2576980377U >> 28 - shift;
				this.AddOneToDecHex();
				int num3 = this.DecHexLen();
				result = (num3 != this._digitsLen);
				this._decPointPos = this._decPointPos + num3 - this._digitsLen;
				this._digitsLen = num3;
			}
			this.RemoveTrailingZeros();
			return result;
		}

		// Token: 0x060019BF RID: 6591 RVA: 0x00060109 File Offset: 0x0005E309
		private void RemoveTrailingZeros()
		{
			this._offset = this.CountTrailingZeros();
			this._digitsLen -= this._offset;
			if (this._digitsLen == 0)
			{
				this._offset = 0;
				this._decPointPos = 1;
				this._positive = true;
			}
		}

		// Token: 0x060019C0 RID: 6592 RVA: 0x00060148 File Offset: 0x0005E348
		private void AddOneToDecHex()
		{
			if (this._val1 != 2576980377U)
			{
				this._val1 = NumberFormatter.AddOneToDecHex(this._val1);
				return;
			}
			this._val1 = 0U;
			if (this._val2 != 2576980377U)
			{
				this._val2 = NumberFormatter.AddOneToDecHex(this._val2);
				return;
			}
			this._val2 = 0U;
			if (this._val3 == 2576980377U)
			{
				this._val3 = 0U;
				this._val4 = NumberFormatter.AddOneToDecHex(this._val4);
				return;
			}
			this._val3 = NumberFormatter.AddOneToDecHex(this._val3);
		}

		// Token: 0x060019C1 RID: 6593 RVA: 0x000601D8 File Offset: 0x0005E3D8
		private static uint AddOneToDecHex(uint val)
		{
			if ((val & 65535U) == 39321U)
			{
				if ((val & 16777215U) == 10066329U)
				{
					if ((val & 268435455U) == 161061273U)
					{
						return val + 107374183U;
					}
					return val + 6710887U;
				}
				else
				{
					if ((val & 1048575U) == 629145U)
					{
						return val + 419431U;
					}
					return val + 26215U;
				}
			}
			else if ((val & 255U) == 153U)
			{
				if ((val & 4095U) == 2457U)
				{
					return val + 1639U;
				}
				return val + 103U;
			}
			else
			{
				if ((val & 15U) == 9U)
				{
					return val + 7U;
				}
				return val + 1U;
			}
		}

		// Token: 0x060019C2 RID: 6594 RVA: 0x00060278 File Offset: 0x0005E478
		private int CountTrailingZeros()
		{
			if (this._val1 != 0U)
			{
				return NumberFormatter.CountTrailingZeros(this._val1);
			}
			if (this._val2 != 0U)
			{
				return NumberFormatter.CountTrailingZeros(this._val2) + 8;
			}
			if (this._val3 != 0U)
			{
				return NumberFormatter.CountTrailingZeros(this._val3) + 16;
			}
			if (this._val4 != 0U)
			{
				return NumberFormatter.CountTrailingZeros(this._val4) + 24;
			}
			return this._digitsLen;
		}

		// Token: 0x060019C3 RID: 6595 RVA: 0x000602E4 File Offset: 0x0005E4E4
		private static int CountTrailingZeros(uint val)
		{
			if ((val & 65535U) == 0U)
			{
				if ((val & 16777215U) == 0U)
				{
					if ((val & 268435455U) == 0U)
					{
						return 7;
					}
					return 6;
				}
				else
				{
					if ((val & 1048575U) == 0U)
					{
						return 5;
					}
					return 4;
				}
			}
			else if ((val & 255U) == 0U)
			{
				if ((val & 4095U) == 0U)
				{
					return 3;
				}
				return 2;
			}
			else
			{
				if ((val & 15U) == 0U)
				{
					return 1;
				}
				return 0;
			}
		}

		// Token: 0x060019C4 RID: 6596 RVA: 0x0006033C File Offset: 0x0005E53C
		private static NumberFormatter GetInstance(IFormatProvider fp)
		{
			if (fp != null)
			{
				if (NumberFormatter.userFormatProvider == null)
				{
					Interlocked.CompareExchange<NumberFormatter>(ref NumberFormatter.userFormatProvider, new NumberFormatter(null), null);
				}
				return NumberFormatter.userFormatProvider;
			}
			NumberFormatter numberFormatter = NumberFormatter.threadNumberFormatter;
			NumberFormatter.threadNumberFormatter = null;
			if (numberFormatter == null)
			{
				return new NumberFormatter(Thread.CurrentThread);
			}
			numberFormatter.CurrentCulture = Thread.CurrentThread.CurrentCulture;
			return numberFormatter;
		}

		// Token: 0x060019C5 RID: 6597 RVA: 0x00060396 File Offset: 0x0005E596
		private void Release()
		{
			if (this != NumberFormatter.userFormatProvider)
			{
				NumberFormatter.threadNumberFormatter = this;
			}
		}

		// Token: 0x060019C6 RID: 6598 RVA: 0x000603A8 File Offset: 0x0005E5A8
		public static string NumberToString(string format, uint value, IFormatProvider fp)
		{
			NumberFormatter instance = NumberFormatter.GetInstance(fp);
			instance.Init(format, value, 10);
			string result = instance.IntegerToString(format, fp);
			instance.Release();
			return result;
		}

		// Token: 0x060019C7 RID: 6599 RVA: 0x000603D4 File Offset: 0x0005E5D4
		public static string NumberToString(string format, int value, IFormatProvider fp)
		{
			NumberFormatter instance = NumberFormatter.GetInstance(fp);
			instance.Init(format, value, 10);
			string result = instance.IntegerToString(format, fp);
			instance.Release();
			return result;
		}

		// Token: 0x060019C8 RID: 6600 RVA: 0x00060400 File Offset: 0x0005E600
		public static string NumberToString(string format, ulong value, IFormatProvider fp)
		{
			NumberFormatter instance = NumberFormatter.GetInstance(fp);
			instance.Init(format, value);
			string result = instance.IntegerToString(format, fp);
			instance.Release();
			return result;
		}

		// Token: 0x060019C9 RID: 6601 RVA: 0x0006042C File Offset: 0x0005E62C
		public static string NumberToString(string format, long value, IFormatProvider fp)
		{
			NumberFormatter instance = NumberFormatter.GetInstance(fp);
			instance.Init(format, value);
			string result = instance.IntegerToString(format, fp);
			instance.Release();
			return result;
		}

		// Token: 0x060019CA RID: 6602 RVA: 0x00060458 File Offset: 0x0005E658
		public static string NumberToString(string format, float value, IFormatProvider fp)
		{
			NumberFormatter instance = NumberFormatter.GetInstance(fp);
			instance.Init(format, (double)value, 7);
			NumberFormatInfo numberFormatInstance = instance.GetNumberFormatInstance(fp);
			string result;
			if (instance._NaN)
			{
				result = numberFormatInstance.NaNSymbol;
			}
			else if (instance._infinity)
			{
				if (instance._positive)
				{
					result = numberFormatInstance.PositiveInfinitySymbol;
				}
				else
				{
					result = numberFormatInstance.NegativeInfinitySymbol;
				}
			}
			else if (instance._specifier == 'R')
			{
				result = instance.FormatRoundtrip(value, numberFormatInstance);
			}
			else
			{
				result = instance.NumberToString(format, numberFormatInstance);
			}
			instance.Release();
			return result;
		}

		// Token: 0x060019CB RID: 6603 RVA: 0x000604D8 File Offset: 0x0005E6D8
		public static string NumberToString(string format, double value, IFormatProvider fp)
		{
			NumberFormatter instance = NumberFormatter.GetInstance(fp);
			instance.Init(format, value, 15);
			NumberFormatInfo numberFormatInstance = instance.GetNumberFormatInstance(fp);
			string result;
			if (instance._NaN)
			{
				result = numberFormatInstance.NaNSymbol;
			}
			else if (instance._infinity)
			{
				if (instance._positive)
				{
					result = numberFormatInstance.PositiveInfinitySymbol;
				}
				else
				{
					result = numberFormatInstance.NegativeInfinitySymbol;
				}
			}
			else if (instance._specifier == 'R')
			{
				result = instance.FormatRoundtrip(value, numberFormatInstance);
			}
			else
			{
				result = instance.NumberToString(format, numberFormatInstance);
			}
			instance.Release();
			return result;
		}

		// Token: 0x060019CC RID: 6604 RVA: 0x00060558 File Offset: 0x0005E758
		public static string NumberToString(string format, decimal value, IFormatProvider fp)
		{
			NumberFormatter instance = NumberFormatter.GetInstance(fp);
			instance.Init(format, value);
			string result = instance.NumberToString(format, instance.GetNumberFormatInstance(fp));
			instance.Release();
			return result;
		}

		// Token: 0x060019CD RID: 6605 RVA: 0x00060588 File Offset: 0x0005E788
		private string IntegerToString(string format, IFormatProvider fp)
		{
			NumberFormatInfo numberFormatInstance = this.GetNumberFormatInstance(fp);
			char specifier = this._specifier;
			if (specifier <= 'N')
			{
				switch (specifier)
				{
				case 'C':
					return this.FormatCurrency(this._precision, numberFormatInstance);
				case 'D':
					return this.FormatDecimal(this._precision, numberFormatInstance);
				case 'E':
					return this.FormatExponential(this._precision, numberFormatInstance);
				case 'F':
					return this.FormatFixedPoint(this._precision, numberFormatInstance);
				case 'G':
					if (this._precision <= 0)
					{
						return this.FormatDecimal(-1, numberFormatInstance);
					}
					return this.FormatGeneral(this._precision, numberFormatInstance);
				default:
					if (specifier == 'N')
					{
						return this.FormatNumber(this._precision, numberFormatInstance);
					}
					break;
				}
			}
			else
			{
				if (specifier == 'P')
				{
					return this.FormatPercent(this._precision, numberFormatInstance);
				}
				if (specifier == 'X')
				{
					return this.FormatHexadecimal(this._precision);
				}
			}
			if (this._isCustomFormat)
			{
				return this.FormatCustom(format, numberFormatInstance);
			}
			throw new FormatException("The specified format '" + format + "' is invalid");
		}

		// Token: 0x060019CE RID: 6606 RVA: 0x00060688 File Offset: 0x0005E888
		private string NumberToString(string format, NumberFormatInfo nfi)
		{
			char specifier = this._specifier;
			if (specifier <= 'N')
			{
				switch (specifier)
				{
				case 'C':
					return this.FormatCurrency(this._precision, nfi);
				case 'D':
					break;
				case 'E':
					return this.FormatExponential(this._precision, nfi);
				case 'F':
					return this.FormatFixedPoint(this._precision, nfi);
				case 'G':
					return this.FormatGeneral(this._precision, nfi);
				default:
					if (specifier == 'N')
					{
						return this.FormatNumber(this._precision, nfi);
					}
					break;
				}
			}
			else
			{
				if (specifier == 'P')
				{
					return this.FormatPercent(this._precision, nfi);
				}
				if (specifier != 'X')
				{
				}
			}
			if (this._isCustomFormat)
			{
				return this.FormatCustom(format, nfi);
			}
			throw new FormatException("The specified format '" + format + "' is invalid");
		}

		// Token: 0x060019CF RID: 6607 RVA: 0x0006074C File Offset: 0x0005E94C
		private string FormatCurrency(int precision, NumberFormatInfo nfi)
		{
			precision = ((precision >= 0) ? precision : nfi.CurrencyDecimalDigits);
			this.RoundDecimal(precision);
			this.ResetCharBuf(this.IntegerDigits * 2 + precision * 2 + 16);
			if (this._positive)
			{
				int currencyPositivePattern = nfi.CurrencyPositivePattern;
				if (currencyPositivePattern != 0)
				{
					if (currencyPositivePattern == 2)
					{
						this.Append(nfi.CurrencySymbol);
						this.Append(' ');
					}
				}
				else
				{
					this.Append(nfi.CurrencySymbol);
				}
			}
			else
			{
				switch (nfi.CurrencyNegativePattern)
				{
				case 0:
					this.Append('(');
					this.Append(nfi.CurrencySymbol);
					break;
				case 1:
					this.Append(nfi.NegativeSign);
					this.Append(nfi.CurrencySymbol);
					break;
				case 2:
					this.Append(nfi.CurrencySymbol);
					this.Append(nfi.NegativeSign);
					break;
				case 3:
					this.Append(nfi.CurrencySymbol);
					break;
				case 4:
					this.Append('(');
					break;
				case 5:
					this.Append(nfi.NegativeSign);
					break;
				case 8:
					this.Append(nfi.NegativeSign);
					break;
				case 9:
					this.Append(nfi.NegativeSign);
					this.Append(nfi.CurrencySymbol);
					this.Append(' ');
					break;
				case 11:
					this.Append(nfi.CurrencySymbol);
					this.Append(' ');
					break;
				case 12:
					this.Append(nfi.CurrencySymbol);
					this.Append(' ');
					this.Append(nfi.NegativeSign);
					break;
				case 14:
					this.Append('(');
					this.Append(nfi.CurrencySymbol);
					this.Append(' ');
					break;
				case 15:
					this.Append('(');
					break;
				}
			}
			this.AppendIntegerStringWithGroupSeparator(nfi.CurrencyGroupSizes, nfi.CurrencyGroupSeparator);
			if (precision > 0)
			{
				this.Append(nfi.CurrencyDecimalSeparator);
				this.AppendDecimalString(precision);
			}
			if (this._positive)
			{
				int currencyPositivePattern = nfi.CurrencyPositivePattern;
				if (currencyPositivePattern != 1)
				{
					if (currencyPositivePattern == 3)
					{
						this.Append(' ');
						this.Append(nfi.CurrencySymbol);
					}
				}
				else
				{
					this.Append(nfi.CurrencySymbol);
				}
			}
			else
			{
				switch (nfi.CurrencyNegativePattern)
				{
				case 0:
					this.Append(')');
					break;
				case 3:
					this.Append(nfi.NegativeSign);
					break;
				case 4:
					this.Append(nfi.CurrencySymbol);
					this.Append(')');
					break;
				case 5:
					this.Append(nfi.CurrencySymbol);
					break;
				case 6:
					this.Append(nfi.NegativeSign);
					this.Append(nfi.CurrencySymbol);
					break;
				case 7:
					this.Append(nfi.CurrencySymbol);
					this.Append(nfi.NegativeSign);
					break;
				case 8:
					this.Append(' ');
					this.Append(nfi.CurrencySymbol);
					break;
				case 10:
					this.Append(' ');
					this.Append(nfi.CurrencySymbol);
					this.Append(nfi.NegativeSign);
					break;
				case 11:
					this.Append(nfi.NegativeSign);
					break;
				case 13:
					this.Append(nfi.NegativeSign);
					this.Append(' ');
					this.Append(nfi.CurrencySymbol);
					break;
				case 14:
					this.Append(')');
					break;
				case 15:
					this.Append(' ');
					this.Append(nfi.CurrencySymbol);
					this.Append(')');
					break;
				}
			}
			return new string(this._cbuf, 0, this._ind);
		}

		// Token: 0x060019D0 RID: 6608 RVA: 0x00060B14 File Offset: 0x0005ED14
		private string FormatDecimal(int precision, NumberFormatInfo nfi)
		{
			if (precision < this._digitsLen)
			{
				precision = this._digitsLen;
			}
			if (precision == 0)
			{
				return "0";
			}
			this.ResetCharBuf(precision + 1);
			if (!this._positive)
			{
				this.Append(nfi.NegativeSign);
			}
			this.AppendDigits(0, precision);
			return new string(this._cbuf, 0, this._ind);
		}

		// Token: 0x060019D1 RID: 6609 RVA: 0x00060B74 File Offset: 0x0005ED74
		private unsafe string FormatHexadecimal(int precision)
		{
			int i = Math.Max(precision, this._decPointPos);
			char* ptr = this._specifierIsUpper ? NumberFormatter.DigitUpperTable : NumberFormatter.DigitLowerTable;
			this.ResetCharBuf(i);
			this._ind = i;
			ulong num = (ulong)this._val1 | (ulong)this._val2 << 32;
			while (i > 0)
			{
				this._cbuf[--i] = ptr[(num & 15UL) * 2UL / 2UL];
				num >>= 4;
			}
			return new string(this._cbuf, 0, this._ind);
		}

		// Token: 0x060019D2 RID: 6610 RVA: 0x00060BF8 File Offset: 0x0005EDF8
		private string FormatFixedPoint(int precision, NumberFormatInfo nfi)
		{
			if (precision == -1)
			{
				precision = nfi.NumberDecimalDigits;
			}
			this.RoundDecimal(precision);
			this.ResetCharBuf(this.IntegerDigits + precision + 2);
			if (!this._positive)
			{
				this.Append(nfi.NegativeSign);
			}
			this.AppendIntegerString(this.IntegerDigits);
			if (precision > 0)
			{
				this.Append(nfi.NumberDecimalSeparator);
				this.AppendDecimalString(precision);
			}
			return new string(this._cbuf, 0, this._ind);
		}

		// Token: 0x060019D3 RID: 6611 RVA: 0x00060C74 File Offset: 0x0005EE74
		private string FormatRoundtrip(double origval, NumberFormatInfo nfi)
		{
			NumberFormatter clone = this.GetClone();
			if (origval >= -1.79769313486231E+308 && origval <= 1.79769313486231E+308)
			{
				string text = this.FormatGeneral(this._defPrecision, nfi);
				if (origval == double.Parse(text, nfi))
				{
					return text;
				}
			}
			return clone.FormatGeneral(this._defPrecision + 2, nfi);
		}

		// Token: 0x060019D4 RID: 6612 RVA: 0x00060CCC File Offset: 0x0005EECC
		private string FormatRoundtrip(float origval, NumberFormatInfo nfi)
		{
			NumberFormatter clone = this.GetClone();
			string text = this.FormatGeneral(this._defPrecision, nfi);
			if (origval == float.Parse(text, nfi))
			{
				return text;
			}
			return clone.FormatGeneral(this._defPrecision + 2, nfi);
		}

		// Token: 0x060019D5 RID: 6613 RVA: 0x00060D0C File Offset: 0x0005EF0C
		private string FormatGeneral(int precision, NumberFormatInfo nfi)
		{
			bool flag;
			if (precision == -1)
			{
				flag = this.IsFloatingSource;
				precision = this._defPrecision;
			}
			else
			{
				flag = true;
				if (precision == 0)
				{
					precision = this._defPrecision;
				}
				this.RoundPos(precision);
			}
			int num = this._decPointPos;
			int digitsLen = this._digitsLen;
			int num2 = digitsLen - num;
			if ((num > precision || num <= -4) && flag)
			{
				return this.FormatExponential(digitsLen - 1, nfi, 2);
			}
			if (num2 < 0)
			{
				num2 = 0;
			}
			if (num < 0)
			{
				num = 0;
			}
			this.ResetCharBuf(num2 + num + 3);
			if (!this._positive)
			{
				this.Append(nfi.NegativeSign);
			}
			if (num == 0)
			{
				this.Append('0');
			}
			else
			{
				this.AppendDigits(digitsLen - num, digitsLen);
			}
			if (num2 > 0)
			{
				this.Append(nfi.NumberDecimalSeparator);
				this.AppendDigits(0, num2);
			}
			return new string(this._cbuf, 0, this._ind);
		}

		// Token: 0x060019D6 RID: 6614 RVA: 0x00060DE0 File Offset: 0x0005EFE0
		private string FormatNumber(int precision, NumberFormatInfo nfi)
		{
			precision = ((precision >= 0) ? precision : nfi.NumberDecimalDigits);
			this.ResetCharBuf(this.IntegerDigits * 3 + precision);
			this.RoundDecimal(precision);
			if (!this._positive)
			{
				switch (nfi.NumberNegativePattern)
				{
				case 0:
					this.Append('(');
					break;
				case 1:
					this.Append(nfi.NegativeSign);
					break;
				case 2:
					this.Append(nfi.NegativeSign);
					this.Append(' ');
					break;
				}
			}
			this.AppendIntegerStringWithGroupSeparator(nfi.NumberGroupSizes, nfi.NumberGroupSeparator);
			if (precision > 0)
			{
				this.Append(nfi.NumberDecimalSeparator);
				this.AppendDecimalString(precision);
			}
			if (!this._positive)
			{
				switch (nfi.NumberNegativePattern)
				{
				case 0:
					this.Append(')');
					break;
				case 3:
					this.Append(nfi.NegativeSign);
					break;
				case 4:
					this.Append(' ');
					this.Append(nfi.NegativeSign);
					break;
				}
			}
			return new string(this._cbuf, 0, this._ind);
		}

		// Token: 0x060019D7 RID: 6615 RVA: 0x00060EF8 File Offset: 0x0005F0F8
		private string FormatPercent(int precision, NumberFormatInfo nfi)
		{
			precision = ((precision >= 0) ? precision : nfi.PercentDecimalDigits);
			this.Multiply10(2);
			this.RoundDecimal(precision);
			this.ResetCharBuf(this.IntegerDigits * 2 + precision + 16);
			if (this._positive)
			{
				if (nfi.PercentPositivePattern == 2)
				{
					this.Append(nfi.PercentSymbol);
				}
			}
			else
			{
				switch (nfi.PercentNegativePattern)
				{
				case 0:
					this.Append(nfi.NegativeSign);
					break;
				case 1:
					this.Append(nfi.NegativeSign);
					break;
				case 2:
					this.Append(nfi.NegativeSign);
					this.Append(nfi.PercentSymbol);
					break;
				}
			}
			this.AppendIntegerStringWithGroupSeparator(nfi.PercentGroupSizes, nfi.PercentGroupSeparator);
			if (precision > 0)
			{
				this.Append(nfi.PercentDecimalSeparator);
				this.AppendDecimalString(precision);
			}
			if (this._positive)
			{
				int num = nfi.PercentPositivePattern;
				if (num != 0)
				{
					if (num == 1)
					{
						this.Append(nfi.PercentSymbol);
					}
				}
				else
				{
					this.Append(' ');
					this.Append(nfi.PercentSymbol);
				}
			}
			else
			{
				int num = nfi.PercentNegativePattern;
				if (num != 0)
				{
					if (num == 1)
					{
						this.Append(nfi.PercentSymbol);
					}
				}
				else
				{
					this.Append(' ');
					this.Append(nfi.PercentSymbol);
				}
			}
			return new string(this._cbuf, 0, this._ind);
		}

		// Token: 0x060019D8 RID: 6616 RVA: 0x0006104D File Offset: 0x0005F24D
		private string FormatExponential(int precision, NumberFormatInfo nfi)
		{
			if (precision == -1)
			{
				precision = 6;
			}
			this.RoundPos(precision + 1);
			return this.FormatExponential(precision, nfi, 3);
		}

		// Token: 0x060019D9 RID: 6617 RVA: 0x00061068 File Offset: 0x0005F268
		private string FormatExponential(int precision, NumberFormatInfo nfi, int expDigits)
		{
			int decPointPos = this._decPointPos;
			int digitsLen = this._digitsLen;
			int exponent = decPointPos - 1;
			this._decPointPos = 1;
			this.ResetCharBuf(precision + 8);
			if (!this._positive)
			{
				this.Append(nfi.NegativeSign);
			}
			this.AppendOneDigit(digitsLen - 1);
			if (precision > 0)
			{
				this.Append(nfi.NumberDecimalSeparator);
				this.AppendDigits(digitsLen - precision - 1, digitsLen - this._decPointPos);
			}
			this.AppendExponent(nfi, exponent, expDigits);
			return new string(this._cbuf, 0, this._ind);
		}

		// Token: 0x060019DA RID: 6618 RVA: 0x000610F0 File Offset: 0x0005F2F0
		private string FormatCustom(string format, NumberFormatInfo nfi)
		{
			bool positive = this._positive;
			int offset = 0;
			int num = 0;
			NumberFormatter.CustomInfo.GetActiveSection(format, ref positive, this.IsZero, ref offset, ref num);
			if (num != 0)
			{
				this._positive = positive;
				NumberFormatter.CustomInfo customInfo = NumberFormatter.CustomInfo.Parse(format, offset, num, nfi);
				StringBuilder stringBuilder = new StringBuilder(customInfo.IntegerDigits * 2);
				StringBuilder stringBuilder2 = new StringBuilder(customInfo.DecimalDigits * 2);
				StringBuilder stringBuilder3 = customInfo.UseExponent ? new StringBuilder(customInfo.ExponentDigits * 2) : null;
				int num2 = 0;
				if (customInfo.Percents > 0)
				{
					this.Multiply10(2 * customInfo.Percents);
				}
				if (customInfo.Permilles > 0)
				{
					this.Multiply10(3 * customInfo.Permilles);
				}
				if (customInfo.DividePlaces > 0)
				{
					this.Divide10(customInfo.DividePlaces);
				}
				bool flag = true;
				if (customInfo.UseExponent && (customInfo.DecimalDigits > 0 || customInfo.IntegerDigits > 0))
				{
					if (!this.IsZero)
					{
						this.RoundPos(customInfo.DecimalDigits + customInfo.IntegerDigits);
						num2 -= this._decPointPos - customInfo.IntegerDigits;
						this._decPointPos = customInfo.IntegerDigits;
					}
					flag = (num2 <= 0);
					NumberFormatter.AppendNonNegativeNumber(stringBuilder3, (num2 < 0) ? (-num2) : num2);
				}
				else
				{
					this.RoundDecimal(customInfo.DecimalDigits);
				}
				if (customInfo.IntegerDigits != 0 || !this.IsZeroInteger)
				{
					this.AppendIntegerString(this.IntegerDigits, stringBuilder);
				}
				this.AppendDecimalString(this.DecimalDigits, stringBuilder2);
				if (customInfo.UseExponent)
				{
					if (customInfo.DecimalDigits <= 0 && customInfo.IntegerDigits <= 0)
					{
						this._positive = true;
					}
					if (stringBuilder.Length < customInfo.IntegerDigits)
					{
						stringBuilder.Insert(0, "0", customInfo.IntegerDigits - stringBuilder.Length);
					}
					while (stringBuilder3.Length < customInfo.ExponentDigits - customInfo.ExponentTailSharpDigits)
					{
						stringBuilder3.Insert(0, '0');
					}
					if (flag && !customInfo.ExponentNegativeSignOnly)
					{
						stringBuilder3.Insert(0, nfi.PositiveSign);
					}
					else if (!flag)
					{
						stringBuilder3.Insert(0, nfi.NegativeSign);
					}
				}
				else
				{
					if (stringBuilder.Length < customInfo.IntegerDigits - customInfo.IntegerHeadSharpDigits)
					{
						stringBuilder.Insert(0, "0", customInfo.IntegerDigits - customInfo.IntegerHeadSharpDigits - stringBuilder.Length);
					}
					if (customInfo.IntegerDigits == customInfo.IntegerHeadSharpDigits && NumberFormatter.IsZeroOnly(stringBuilder))
					{
						stringBuilder.Remove(0, stringBuilder.Length);
					}
				}
				NumberFormatter.ZeroTrimEnd(stringBuilder2, true);
				while (stringBuilder2.Length < customInfo.DecimalDigits - customInfo.DecimalTailSharpDigits)
				{
					stringBuilder2.Append('0');
				}
				if (stringBuilder2.Length > customInfo.DecimalDigits)
				{
					stringBuilder2.Remove(customInfo.DecimalDigits, stringBuilder2.Length - customInfo.DecimalDigits);
				}
				return customInfo.Format(format, offset, num, nfi, this._positive, stringBuilder, stringBuilder2, stringBuilder3);
			}
			if (!this._positive)
			{
				return nfi.NegativeSign;
			}
			return string.Empty;
		}

		// Token: 0x060019DB RID: 6619 RVA: 0x000613E0 File Offset: 0x0005F5E0
		private static void ZeroTrimEnd(StringBuilder sb, bool canEmpty)
		{
			int num = 0;
			int num2 = sb.Length - 1;
			while ((canEmpty ? (num2 >= 0) : (num2 > 0)) && sb[num2] == '0')
			{
				num++;
				num2--;
			}
			if (num > 0)
			{
				sb.Remove(sb.Length - num, num);
			}
		}

		// Token: 0x060019DC RID: 6620 RVA: 0x00061434 File Offset: 0x0005F634
		private static bool IsZeroOnly(StringBuilder sb)
		{
			for (int i = 0; i < sb.Length; i++)
			{
				if (char.IsDigit(sb[i]) && sb[i] != '0')
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060019DD RID: 6621 RVA: 0x00061470 File Offset: 0x0005F670
		private static void AppendNonNegativeNumber(StringBuilder sb, int v)
		{
			if (v < 0)
			{
				throw new ArgumentException();
			}
			int num = NumberFormatter.ScaleOrder((long)v) - 1;
			do
			{
				int num2 = v / (int)NumberFormatter.GetTenPowerOf(num);
				sb.Append((char)(48 | num2));
				v -= (int)NumberFormatter.GetTenPowerOf(num--) * num2;
			}
			while (num >= 0);
		}

		// Token: 0x060019DE RID: 6622 RVA: 0x000614BC File Offset: 0x0005F6BC
		private void AppendIntegerString(int minLength, StringBuilder sb)
		{
			if (this._decPointPos <= 0)
			{
				sb.Append('0', minLength);
				return;
			}
			if (this._decPointPos < minLength)
			{
				sb.Append('0', minLength - this._decPointPos);
			}
			this.AppendDigits(this._digitsLen - this._decPointPos, this._digitsLen, sb);
		}

		// Token: 0x060019DF RID: 6623 RVA: 0x00061514 File Offset: 0x0005F714
		private void AppendIntegerString(int minLength)
		{
			if (this._decPointPos <= 0)
			{
				this.Append('0', minLength);
				return;
			}
			if (this._decPointPos < minLength)
			{
				this.Append('0', minLength - this._decPointPos);
			}
			this.AppendDigits(this._digitsLen - this._decPointPos, this._digitsLen);
		}

		// Token: 0x060019E0 RID: 6624 RVA: 0x00061566 File Offset: 0x0005F766
		private void AppendDecimalString(int precision, StringBuilder sb)
		{
			this.AppendDigits(this._digitsLen - precision - this._decPointPos, this._digitsLen - this._decPointPos, sb);
		}

		// Token: 0x060019E1 RID: 6625 RVA: 0x0006158B File Offset: 0x0005F78B
		private void AppendDecimalString(int precision)
		{
			this.AppendDigits(this._digitsLen - precision - this._decPointPos, this._digitsLen - this._decPointPos);
		}

		// Token: 0x060019E2 RID: 6626 RVA: 0x000615B0 File Offset: 0x0005F7B0
		private void AppendIntegerStringWithGroupSeparator(int[] groups, string groupSeparator)
		{
			if (this.IsZeroInteger)
			{
				this.Append('0');
				return;
			}
			int num = 0;
			int num2 = 0;
			for (int i = 0; i < groups.Length; i++)
			{
				num += groups[i];
				if (num > this._decPointPos)
				{
					break;
				}
				num2 = i;
			}
			if (groups.Length != 0 && num > 0)
			{
				int num3 = groups[num2];
				int num4 = (this._decPointPos > num) ? (this._decPointPos - num) : 0;
				if (num3 == 0)
				{
					while (num2 >= 0 && groups[num2] == 0)
					{
						num2--;
					}
					num3 = ((num4 > 0) ? num4 : groups[num2]);
				}
				int num5;
				if (num4 == 0)
				{
					num5 = num3;
				}
				else
				{
					num2 += num4 / num3;
					num5 = num4 % num3;
					if (num5 == 0)
					{
						num5 = num3;
					}
					else
					{
						num2++;
					}
				}
				if (num >= this._decPointPos)
				{
					int num6 = groups[0];
					if (num > num6)
					{
						int num7 = -(num6 - this._decPointPos);
						int num8;
						if (num7 < num6)
						{
							num5 = num7;
						}
						else if (num6 > 0 && (num8 = this._decPointPos % num6) > 0)
						{
							num5 = num8;
						}
					}
				}
				int num9 = 0;
				while (this._decPointPos - num9 > num5 && num5 != 0)
				{
					this.AppendDigits(this._digitsLen - num9 - num5, this._digitsLen - num9);
					num9 += num5;
					this.Append(groupSeparator);
					if (--num2 < groups.Length && num2 >= 0)
					{
						num3 = groups[num2];
					}
					num5 = num3;
				}
				this.AppendDigits(this._digitsLen - this._decPointPos, this._digitsLen - num9);
				return;
			}
			this.AppendDigits(this._digitsLen - this._decPointPos, this._digitsLen);
		}

		// Token: 0x060019E3 RID: 6627 RVA: 0x00061728 File Offset: 0x0005F928
		private void AppendExponent(NumberFormatInfo nfi, int exponent, int minDigits)
		{
			if (this._specifierIsUpper || this._specifier == 'R')
			{
				this.Append('E');
			}
			else
			{
				this.Append('e');
			}
			if (exponent >= 0)
			{
				this.Append(nfi.PositiveSign);
			}
			else
			{
				this.Append(nfi.NegativeSign);
				exponent = -exponent;
			}
			if (exponent == 0)
			{
				this.Append('0', minDigits);
				return;
			}
			if (exponent < 10)
			{
				this.Append('0', minDigits - 1);
				this.Append((char)(48 | exponent));
				return;
			}
			uint num = NumberFormatter.FastToDecHex(exponent);
			if (exponent >= 100 || minDigits == 3)
			{
				this.Append((char)(48U | num >> 8));
			}
			this.Append((char)(48U | (num >> 4 & 15U)));
			this.Append((char)(48U | (num & 15U)));
		}

		// Token: 0x060019E4 RID: 6628 RVA: 0x000617E0 File Offset: 0x0005F9E0
		private void AppendOneDigit(int start)
		{
			if (this._ind == this._cbuf.Length)
			{
				this.Resize(this._ind + 10);
			}
			start += this._offset;
			uint num;
			if (start < 0)
			{
				num = 0U;
			}
			else if (start < 8)
			{
				num = this._val1;
			}
			else if (start < 16)
			{
				num = this._val2;
			}
			else if (start < 24)
			{
				num = this._val3;
			}
			else if (start < 32)
			{
				num = this._val4;
			}
			else
			{
				num = 0U;
			}
			num >>= (start & 7) << 2;
			char[] cbuf = this._cbuf;
			int ind = this._ind;
			this._ind = ind + 1;
			cbuf[ind] = (ushort)(48U | (num & 15U));
		}

		// Token: 0x060019E5 RID: 6629 RVA: 0x00061884 File Offset: 0x0005FA84
		private void AppendDigits(int start, int end)
		{
			if (start >= end)
			{
				return;
			}
			int num = this._ind + (end - start);
			if (num > this._cbuf.Length)
			{
				this.Resize(num + 10);
			}
			this._ind = num;
			end += this._offset;
			start += this._offset;
			int num2 = start + 8 - (start & 7);
			for (;;)
			{
				uint num3;
				if (num2 == 8)
				{
					num3 = this._val1;
				}
				else if (num2 == 16)
				{
					num3 = this._val2;
				}
				else if (num2 == 24)
				{
					num3 = this._val3;
				}
				else if (num2 == 32)
				{
					num3 = this._val4;
				}
				else
				{
					num3 = 0U;
				}
				num3 >>= (start & 7) << 2;
				if (num2 > end)
				{
					num2 = end;
				}
				this._cbuf[--num] = (char)(48U | (num3 & 15U));
				switch (num2 - start)
				{
				case 1:
					goto IL_17F;
				case 2:
					goto IL_167;
				case 3:
					goto IL_14F;
				case 4:
					goto IL_137;
				case 5:
					goto IL_11F;
				case 6:
					goto IL_107;
				case 7:
					goto IL_EF;
				case 8:
					this._cbuf[--num] = (char)(48U | ((num3 >>= 4) & 15U));
					goto IL_EF;
				}
				IL_184:
				start = num2;
				num2 += 8;
				continue;
				IL_17F:
				if (num2 == end)
				{
					break;
				}
				goto IL_184;
				IL_167:
				this._cbuf[--num] = (char)(48U | (num3 >> 4 & 15U));
				goto IL_17F;
				IL_14F:
				this._cbuf[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_167;
				IL_137:
				this._cbuf[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_14F;
				IL_11F:
				this._cbuf[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_137;
				IL_107:
				this._cbuf[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_11F;
				IL_EF:
				this._cbuf[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_107;
			}
		}

		// Token: 0x060019E6 RID: 6630 RVA: 0x00061A20 File Offset: 0x0005FC20
		private void AppendDigits(int start, int end, StringBuilder sb)
		{
			if (start >= end)
			{
				return;
			}
			int num = sb.Length + (end - start);
			sb.Length = num;
			end += this._offset;
			start += this._offset;
			int num2 = start + 8 - (start & 7);
			for (;;)
			{
				uint num3;
				if (num2 == 8)
				{
					num3 = this._val1;
				}
				else if (num2 == 16)
				{
					num3 = this._val2;
				}
				else if (num2 == 24)
				{
					num3 = this._val3;
				}
				else if (num2 == 32)
				{
					num3 = this._val4;
				}
				else
				{
					num3 = 0U;
				}
				num3 >>= (start & 7) << 2;
				if (num2 > end)
				{
					num2 = end;
				}
				sb[--num] = (char)(48U | (num3 & 15U));
				switch (num2 - start)
				{
				case 1:
					goto IL_162;
				case 2:
					goto IL_14B;
				case 3:
					goto IL_134;
				case 4:
					goto IL_11D;
				case 5:
					goto IL_106;
				case 6:
					goto IL_EF;
				case 7:
					goto IL_D8;
				case 8:
					sb[--num] = (char)(48U | ((num3 >>= 4) & 15U));
					goto IL_D8;
				}
				IL_167:
				start = num2;
				num2 += 8;
				continue;
				IL_162:
				if (num2 == end)
				{
					break;
				}
				goto IL_167;
				IL_14B:
				sb[--num] = (char)(48U | (num3 >> 4 & 15U));
				goto IL_162;
				IL_134:
				sb[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_14B;
				IL_11D:
				sb[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_134;
				IL_106:
				sb[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_11D;
				IL_EF:
				sb[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_106;
				IL_D8:
				sb[--num] = (char)(48U | ((num3 >>= 4) & 15U));
				goto IL_EF;
			}
		}

		// Token: 0x060019E7 RID: 6631 RVA: 0x00061B9F File Offset: 0x0005FD9F
		private void Multiply10(int count)
		{
			if (count <= 0 || this._digitsLen == 0)
			{
				return;
			}
			this._decPointPos += count;
		}

		// Token: 0x060019E8 RID: 6632 RVA: 0x00061BBC File Offset: 0x0005FDBC
		private void Divide10(int count)
		{
			if (count <= 0 || this._digitsLen == 0)
			{
				return;
			}
			this._decPointPos -= count;
		}

		// Token: 0x060019E9 RID: 6633 RVA: 0x00061BD9 File Offset: 0x0005FDD9
		private NumberFormatter GetClone()
		{
			return (NumberFormatter)base.MemberwiseClone();
		}

		// Token: 0x04000CA0 RID: 3232
		private const int DefaultExpPrecision = 6;

		// Token: 0x04000CA1 RID: 3233
		private const int HundredMillion = 100000000;

		// Token: 0x04000CA2 RID: 3234
		private const long SeventeenDigitsThreshold = 10000000000000000L;

		// Token: 0x04000CA3 RID: 3235
		private const ulong ULongDivHundredMillion = 184467440737UL;

		// Token: 0x04000CA4 RID: 3236
		private const ulong ULongModHundredMillion = 9551616UL;

		// Token: 0x04000CA5 RID: 3237
		private const int DoubleBitsExponentShift = 52;

		// Token: 0x04000CA6 RID: 3238
		private const int DoubleBitsExponentMask = 2047;

		// Token: 0x04000CA7 RID: 3239
		private const long DoubleBitsMantissaMask = 4503599627370495L;

		// Token: 0x04000CA8 RID: 3240
		private const int DecimalBitsScaleMask = 2031616;

		// Token: 0x04000CA9 RID: 3241
		private const int SingleDefPrecision = 7;

		// Token: 0x04000CAA RID: 3242
		private const int DoubleDefPrecision = 15;

		// Token: 0x04000CAB RID: 3243
		private const int Int32DefPrecision = 10;

		// Token: 0x04000CAC RID: 3244
		private const int UInt32DefPrecision = 10;

		// Token: 0x04000CAD RID: 3245
		private const int Int64DefPrecision = 19;

		// Token: 0x04000CAE RID: 3246
		private const int UInt64DefPrecision = 20;

		// Token: 0x04000CAF RID: 3247
		private const int DecimalDefPrecision = 100;

		// Token: 0x04000CB0 RID: 3248
		private const int TenPowersListLength = 19;

		// Token: 0x04000CB1 RID: 3249
		private const double MinRoundtripVal = -1.79769313486231E+308;

		// Token: 0x04000CB2 RID: 3250
		private const double MaxRoundtripVal = 1.79769313486231E+308;

		// Token: 0x04000CB3 RID: 3251
		private unsafe static readonly ulong* MantissaBitsTable;

		// Token: 0x04000CB4 RID: 3252
		private unsafe static readonly int* TensExponentTable;

		// Token: 0x04000CB5 RID: 3253
		private unsafe static readonly char* DigitLowerTable;

		// Token: 0x04000CB6 RID: 3254
		private unsafe static readonly char* DigitUpperTable;

		// Token: 0x04000CB7 RID: 3255
		private unsafe static readonly long* TenPowersList;

		// Token: 0x04000CB8 RID: 3256
		private unsafe static readonly int* DecHexDigits;

		// Token: 0x04000CB9 RID: 3257
		private NumberFormatInfo _nfi;

		// Token: 0x04000CBA RID: 3258
		private char[] _cbuf;

		// Token: 0x04000CBB RID: 3259
		private bool _NaN;

		// Token: 0x04000CBC RID: 3260
		private bool _infinity;

		// Token: 0x04000CBD RID: 3261
		private bool _isCustomFormat;

		// Token: 0x04000CBE RID: 3262
		private bool _specifierIsUpper;

		// Token: 0x04000CBF RID: 3263
		private bool _positive;

		// Token: 0x04000CC0 RID: 3264
		private char _specifier;

		// Token: 0x04000CC1 RID: 3265
		private int _precision;

		// Token: 0x04000CC2 RID: 3266
		private int _defPrecision;

		// Token: 0x04000CC3 RID: 3267
		private int _digitsLen;

		// Token: 0x04000CC4 RID: 3268
		private int _offset;

		// Token: 0x04000CC5 RID: 3269
		private int _decPointPos;

		// Token: 0x04000CC6 RID: 3270
		private uint _val1;

		// Token: 0x04000CC7 RID: 3271
		private uint _val2;

		// Token: 0x04000CC8 RID: 3272
		private uint _val3;

		// Token: 0x04000CC9 RID: 3273
		private uint _val4;

		// Token: 0x04000CCA RID: 3274
		private int _ind;

		// Token: 0x04000CCB RID: 3275
		[ThreadStatic]
		private static NumberFormatter threadNumberFormatter;

		// Token: 0x04000CCC RID: 3276
		[ThreadStatic]
		private static NumberFormatter userFormatProvider;

		// Token: 0x0200021A RID: 538
		private class CustomInfo
		{
			// Token: 0x060019EA RID: 6634 RVA: 0x00061BE8 File Offset: 0x0005FDE8
			public static void GetActiveSection(string format, ref bool positive, bool zero, ref int offset, ref int length)
			{
				int[] array = new int[3];
				int num = 0;
				int num2 = 0;
				bool flag = false;
				for (int i = 0; i < format.Length; i++)
				{
					char c = format[i];
					if (c == '"' || c == '\'')
					{
						if (i == 0 || format[i - 1] != '\\')
						{
							flag = !flag;
						}
					}
					else if (c == ';' && !flag && (i == 0 || format[i - 1] != '\\'))
					{
						array[num++] = i - num2;
						num2 = i + 1;
						if (num == 3)
						{
							break;
						}
					}
				}
				if (num == 0)
				{
					offset = 0;
					length = format.Length;
					return;
				}
				if (num == 1)
				{
					if (positive || zero)
					{
						offset = 0;
						length = array[0];
						return;
					}
					if (array[0] + 1 < format.Length)
					{
						positive = true;
						offset = array[0] + 1;
						length = format.Length - offset;
						return;
					}
					offset = 0;
					length = array[0];
					return;
				}
				else if (zero)
				{
					if (num == 2)
					{
						if (format.Length - num2 == 0)
						{
							offset = 0;
							length = array[0];
							return;
						}
						offset = array[0] + array[1] + 2;
						length = format.Length - offset;
						return;
					}
					else
					{
						if (array[2] == 0)
						{
							offset = 0;
							length = array[0];
							return;
						}
						offset = array[0] + array[1] + 2;
						length = array[2];
						return;
					}
				}
				else
				{
					if (positive)
					{
						offset = 0;
						length = array[0];
						return;
					}
					if (array[1] > 0)
					{
						positive = true;
						offset = array[0] + 1;
						length = array[1];
						return;
					}
					offset = 0;
					length = array[0];
					return;
				}
			}

			// Token: 0x060019EB RID: 6635 RVA: 0x00061D4C File Offset: 0x0005FF4C
			public static NumberFormatter.CustomInfo Parse(string format, int offset, int length, NumberFormatInfo nfi)
			{
				char c = '\0';
				bool flag = true;
				bool flag2 = false;
				bool flag3 = false;
				bool flag4 = true;
				NumberFormatter.CustomInfo customInfo = new NumberFormatter.CustomInfo();
				int num = 0;
				int num2 = offset;
				while (num2 - offset < length)
				{
					char c2 = format[num2];
					if (c2 == c && c2 != '\0')
					{
						c = '\0';
					}
					else if (c == '\0')
					{
						if (flag3 && c2 != '\0' && c2 != '0' && c2 != '#')
						{
							flag3 = false;
							flag = (customInfo.DecimalPointPos < 0);
							flag2 = !flag;
							num2--;
						}
						else
						{
							if (c2 <= 'E')
							{
								switch (c2)
								{
								case '"':
								case '\'':
									if (c2 == '"' || c2 == '\'')
									{
										c = c2;
										goto IL_292;
									}
									goto IL_292;
								case '#':
									if (flag4 && flag)
									{
										customInfo.IntegerHeadSharpDigits++;
									}
									else if (flag2)
									{
										customInfo.DecimalTailSharpDigits++;
									}
									else if (flag3)
									{
										customInfo.ExponentTailSharpDigits++;
									}
									break;
								case '$':
								case '&':
									goto IL_292;
								case '%':
									customInfo.Percents++;
									goto IL_292;
								default:
									switch (c2)
									{
									case ',':
										if (flag && customInfo.IntegerDigits > 0)
										{
											num++;
											goto IL_292;
										}
										goto IL_292;
									case '-':
									case '/':
										goto IL_292;
									case '.':
										flag = false;
										flag2 = true;
										flag3 = false;
										if (customInfo.DecimalPointPos == -1)
										{
											customInfo.DecimalPointPos = num2;
											goto IL_292;
										}
										goto IL_292;
									case '0':
										break;
									default:
										if (c2 != 'E')
										{
											goto IL_292;
										}
										goto IL_1CC;
									}
									break;
								}
								if (c2 != '#')
								{
									flag4 = false;
									if (flag2)
									{
										customInfo.DecimalTailSharpDigits = 0;
									}
									else if (flag3)
									{
										customInfo.ExponentTailSharpDigits = 0;
									}
								}
								if (customInfo.IntegerHeadPos == -1)
								{
									customInfo.IntegerHeadPos = num2;
								}
								if (flag)
								{
									customInfo.IntegerDigits++;
									if (num > 0)
									{
										customInfo.UseGroup = true;
									}
									num = 0;
									goto IL_292;
								}
								if (flag2)
								{
									customInfo.DecimalDigits++;
									goto IL_292;
								}
								if (flag3)
								{
									customInfo.ExponentDigits++;
									goto IL_292;
								}
								goto IL_292;
							}
							else
							{
								if (c2 == '\\')
								{
									num2++;
									goto IL_292;
								}
								if (c2 != 'e')
								{
									if (c2 != '‰')
									{
										goto IL_292;
									}
									customInfo.Permilles++;
									goto IL_292;
								}
							}
							IL_1CC:
							if (!customInfo.UseExponent)
							{
								customInfo.UseExponent = true;
								flag = false;
								flag2 = false;
								flag3 = true;
								if (num2 + 1 - offset < length)
								{
									char c3 = format[num2 + 1];
									if (c3 == '+')
									{
										customInfo.ExponentNegativeSignOnly = false;
									}
									if (c3 == '+' || c3 == '-')
									{
										num2++;
									}
									else if (c3 != '0' && c3 != '#')
									{
										customInfo.UseExponent = false;
										if (customInfo.DecimalPointPos < 0)
										{
											flag = true;
										}
									}
								}
							}
						}
					}
					IL_292:
					num2++;
				}
				if (customInfo.ExponentDigits == 0)
				{
					customInfo.UseExponent = false;
				}
				else
				{
					customInfo.IntegerHeadSharpDigits = 0;
				}
				if (customInfo.DecimalDigits == 0)
				{
					customInfo.DecimalPointPos = -1;
				}
				customInfo.DividePlaces += num * 3;
				return customInfo;
			}

			// Token: 0x060019EC RID: 6636 RVA: 0x0006203C File Offset: 0x0006023C
			public string Format(string format, int offset, int length, NumberFormatInfo nfi, bool positive, StringBuilder sb_int, StringBuilder sb_dec, StringBuilder sb_exp)
			{
				StringBuilder stringBuilder = new StringBuilder();
				char c = '\0';
				bool flag = true;
				bool flag2 = false;
				int num = 0;
				int i = 0;
				int num2 = 0;
				int[] numberGroupSizes = nfi.NumberGroupSizes;
				string numberGroupSeparator = nfi.NumberGroupSeparator;
				int num3 = 0;
				int num4 = 0;
				int num5 = 0;
				int num6 = 0;
				int num7 = 0;
				if (this.UseGroup && numberGroupSizes.Length != 0)
				{
					num3 = sb_int.Length;
					for (int j = 0; j < numberGroupSizes.Length; j++)
					{
						num4 += numberGroupSizes[j];
						if (num4 <= num3)
						{
							num5 = j;
						}
					}
					num7 = numberGroupSizes[num5];
					int num8 = (num3 > num4) ? (num3 - num4) : 0;
					if (num7 == 0)
					{
						while (num5 >= 0 && numberGroupSizes[num5] == 0)
						{
							num5--;
						}
						num7 = ((num8 > 0) ? num8 : numberGroupSizes[num5]);
					}
					if (num8 == 0)
					{
						num6 = num7;
					}
					else
					{
						num5 += num8 / num7;
						num6 = num8 % num7;
						if (num6 == 0)
						{
							num6 = num7;
						}
						else
						{
							num5++;
						}
					}
				}
				else
				{
					this.UseGroup = false;
				}
				int num9 = offset;
				while (num9 - offset < length)
				{
					char c2 = format[num9];
					if (c2 == c && c2 != '\0')
					{
						c = '\0';
					}
					else if (c != '\0')
					{
						stringBuilder.Append(c2);
					}
					else
					{
						if (c2 <= 'E')
						{
							switch (c2)
							{
							case '"':
							case '\'':
								if (c2 == '"' || c2 == '\'')
								{
									c = c2;
									goto IL_3CC;
								}
								goto IL_3CC;
							case '#':
								break;
							case '$':
							case '&':
								goto IL_3C3;
							case '%':
								stringBuilder.Append(nfi.PercentSymbol);
								goto IL_3CC;
							default:
								switch (c2)
								{
								case ',':
									goto IL_3CC;
								case '-':
								case '/':
									goto IL_3C3;
								case '.':
									if (this.DecimalPointPos == num9)
									{
										if (this.DecimalDigits > 0)
										{
											while (i < sb_int.Length)
											{
												stringBuilder.Append(sb_int[i++]);
											}
										}
										if (sb_dec.Length > 0)
										{
											stringBuilder.Append(nfi.NumberDecimalSeparator);
										}
									}
									flag = false;
									flag2 = true;
									goto IL_3CC;
								case '0':
									break;
								default:
									if (c2 != 'E')
									{
										goto IL_3C3;
									}
									goto IL_2A3;
								}
								break;
							}
							if (flag)
							{
								num++;
								if (this.IntegerDigits - num >= sb_int.Length + i)
								{
									if (c2 != '0')
									{
										goto IL_3CC;
									}
								}
								while (this.IntegerDigits - num + i < sb_int.Length)
								{
									stringBuilder.Append(sb_int[i++]);
									if (this.UseGroup && --num3 > 0 && --num6 == 0)
									{
										stringBuilder.Append(numberGroupSeparator);
										if (--num5 < numberGroupSizes.Length && num5 >= 0)
										{
											num7 = numberGroupSizes[num5];
										}
										num6 = num7;
									}
								}
								goto IL_3CC;
							}
							if (!flag2)
							{
								stringBuilder.Append(c2);
								goto IL_3CC;
							}
							if (num2 < sb_dec.Length)
							{
								stringBuilder.Append(sb_dec[num2++]);
								goto IL_3CC;
							}
							goto IL_3CC;
						}
						else if (c2 != '\\')
						{
							if (c2 != 'e')
							{
								if (c2 != '‰')
								{
									goto IL_3C3;
								}
								stringBuilder.Append(nfi.PerMilleSymbol);
								goto IL_3CC;
							}
						}
						else
						{
							num9++;
							if (num9 - offset < length)
							{
								stringBuilder.Append(format[num9]);
								goto IL_3CC;
							}
							goto IL_3CC;
						}
						IL_2A3:
						if (sb_exp == null || !this.UseExponent)
						{
							stringBuilder.Append(c2);
							goto IL_3CC;
						}
						bool flag3 = true;
						bool flag4 = false;
						int num10 = num9 + 1;
						while (num10 - offset < length)
						{
							if (format[num10] == '0')
							{
								flag4 = true;
							}
							else if (num10 != num9 + 1 || (format[num10] != '+' && format[num10] != '-'))
							{
								if (!flag4)
								{
									flag3 = false;
									break;
								}
								break;
							}
							num10++;
						}
						if (flag3)
						{
							num9 = num10 - 1;
							flag = (this.DecimalPointPos < 0);
							flag2 = !flag;
							stringBuilder.Append(c2);
							stringBuilder.Append(sb_exp);
							sb_exp = null;
							goto IL_3CC;
						}
						stringBuilder.Append(c2);
						goto IL_3CC;
						IL_3C3:
						stringBuilder.Append(c2);
					}
					IL_3CC:
					num9++;
				}
				if (!positive)
				{
					stringBuilder.Insert(0, nfi.NegativeSign);
				}
				return stringBuilder.ToString();
			}

			// Token: 0x060019ED RID: 6637 RVA: 0x0006243E File Offset: 0x0006063E
			public CustomInfo()
			{
			}

			// Token: 0x04000CCD RID: 3277
			public bool UseGroup;

			// Token: 0x04000CCE RID: 3278
			public int DecimalDigits;

			// Token: 0x04000CCF RID: 3279
			public int DecimalPointPos = -1;

			// Token: 0x04000CD0 RID: 3280
			public int DecimalTailSharpDigits;

			// Token: 0x04000CD1 RID: 3281
			public int IntegerDigits;

			// Token: 0x04000CD2 RID: 3282
			public int IntegerHeadSharpDigits;

			// Token: 0x04000CD3 RID: 3283
			public int IntegerHeadPos;

			// Token: 0x04000CD4 RID: 3284
			public bool UseExponent;

			// Token: 0x04000CD5 RID: 3285
			public int ExponentDigits;

			// Token: 0x04000CD6 RID: 3286
			public int ExponentTailSharpDigits;

			// Token: 0x04000CD7 RID: 3287
			public bool ExponentNegativeSignOnly = true;

			// Token: 0x04000CD8 RID: 3288
			public int DividePlaces;

			// Token: 0x04000CD9 RID: 3289
			public int Percents;

			// Token: 0x04000CDA RID: 3290
			public int Permilles;
		}
	}
}
