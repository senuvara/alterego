﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates that the COM threading model for an application is single-threaded apartment (STA). </summary>
	// Token: 0x020001B6 RID: 438
	[AttributeUsage(AttributeTargets.Method)]
	[ComVisible(true)]
	public sealed class STAThreadAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.STAThreadAttribute" /> class.</summary>
		// Token: 0x060013E1 RID: 5089 RVA: 0x000020BF File Offset: 0x000002BF
		public STAThreadAttribute()
		{
		}
	}
}
