﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace System.Diagnostics
{
	/// <summary>Provides information about a <see cref="T:System.Diagnostics.StackFrame" />, which represents a function call on the call stack for the current thread.</summary>
	// Token: 0x02000422 RID: 1058
	[ComVisible(true)]
	[MonoTODO("Serialized objects are not compatible with MS.NET")]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class StackFrame
	{
		// Token: 0x0600313F RID: 12607
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool get_frame_info(int skip, bool needFileInfo, out MethodBase method, out int iloffset, out int native_offset, out string file, out int line, out int column);

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackFrame" /> class.</summary>
		// Token: 0x06003140 RID: 12608 RVA: 0x000B3D4C File Offset: 0x000B1F4C
		public StackFrame()
		{
			bool flag = StackFrame.get_frame_info(2, false, out this.methodBase, out this.ilOffset, out this.nativeOffset, out this.fileName, out this.lineNumber, out this.columnNumber);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackFrame" /> class, optionally capturing source information.</summary>
		/// <param name="fNeedFileInfo">
		///       <see langword="true" /> to capture the file name, line number, and column number of the stack frame; otherwise, <see langword="false" />. </param>
		// Token: 0x06003141 RID: 12609 RVA: 0x000B3D9C File Offset: 0x000B1F9C
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackFrame(bool fNeedFileInfo)
		{
			bool flag = StackFrame.get_frame_info(2, fNeedFileInfo, out this.methodBase, out this.ilOffset, out this.nativeOffset, out this.fileName, out this.lineNumber, out this.columnNumber);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackFrame" /> class that corresponds to a frame above the current stack frame.</summary>
		/// <param name="skipFrames">The number of frames up the stack to skip. </param>
		// Token: 0x06003142 RID: 12610 RVA: 0x000B3DEC File Offset: 0x000B1FEC
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackFrame(int skipFrames)
		{
			bool flag = StackFrame.get_frame_info(skipFrames + 2, false, out this.methodBase, out this.ilOffset, out this.nativeOffset, out this.fileName, out this.lineNumber, out this.columnNumber);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackFrame" /> class that corresponds to a frame above the current stack frame, optionally capturing source information.</summary>
		/// <param name="skipFrames">The number of frames up the stack to skip. </param>
		/// <param name="fNeedFileInfo">
		///       <see langword="true" /> to capture the file name, line number, and column number of the stack frame; otherwise, <see langword="false" />. </param>
		// Token: 0x06003143 RID: 12611 RVA: 0x000B3E3C File Offset: 0x000B203C
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackFrame(int skipFrames, bool fNeedFileInfo)
		{
			bool flag = StackFrame.get_frame_info(skipFrames + 2, fNeedFileInfo, out this.methodBase, out this.ilOffset, out this.nativeOffset, out this.fileName, out this.lineNumber, out this.columnNumber);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackFrame" /> class that contains only the given file name and line number.</summary>
		/// <param name="fileName">The file name. </param>
		/// <param name="lineNumber">The line number in the specified file. </param>
		// Token: 0x06003144 RID: 12612 RVA: 0x000B3E8C File Offset: 0x000B208C
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackFrame(string fileName, int lineNumber)
		{
			bool flag = StackFrame.get_frame_info(2, false, out this.methodBase, out this.ilOffset, out this.nativeOffset, out fileName, out lineNumber, out this.columnNumber);
			this.fileName = fileName;
			this.lineNumber = lineNumber;
			this.columnNumber = 0;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackFrame" /> class that contains only the given file name, line number, and column number.</summary>
		/// <param name="fileName">The file name. </param>
		/// <param name="lineNumber">The line number in the specified file. </param>
		/// <param name="colNumber">The column number in the specified file. </param>
		// Token: 0x06003145 RID: 12613 RVA: 0x000B3EE8 File Offset: 0x000B20E8
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackFrame(string fileName, int lineNumber, int colNumber)
		{
			bool flag = StackFrame.get_frame_info(2, false, out this.methodBase, out this.ilOffset, out this.nativeOffset, out fileName, out lineNumber, out this.columnNumber);
			this.fileName = fileName;
			this.lineNumber = lineNumber;
			this.columnNumber = colNumber;
		}

		/// <summary>Gets the line number in the file that contains the code that is executing. This information is typically extracted from the debugging symbols for the executable.</summary>
		/// <returns>The file line number, or 0 (zero) if the file line number cannot be determined.</returns>
		// Token: 0x06003146 RID: 12614 RVA: 0x000B3F42 File Offset: 0x000B2142
		public virtual int GetFileLineNumber()
		{
			return this.lineNumber;
		}

		/// <summary>Gets the column number in the file that contains the code that is executing. This information is typically extracted from the debugging symbols for the executable.</summary>
		/// <returns>The file column number, or 0 (zero) if the file column number cannot be determined.</returns>
		// Token: 0x06003147 RID: 12615 RVA: 0x000B3F4A File Offset: 0x000B214A
		public virtual int GetFileColumnNumber()
		{
			return this.columnNumber;
		}

		/// <summary>Gets the file name that contains the code that is executing. This information is typically extracted from the debugging symbols for the executable.</summary>
		/// <returns>The file name, or <see langword="null" /> if the file name cannot be determined.</returns>
		// Token: 0x06003148 RID: 12616 RVA: 0x000B3F52 File Offset: 0x000B2152
		public virtual string GetFileName()
		{
			return this.fileName;
		}

		// Token: 0x06003149 RID: 12617 RVA: 0x000B3F5C File Offset: 0x000B215C
		internal string GetSecureFileName()
		{
			string result = "<filename unknown>";
			if (this.fileName == null)
			{
				return result;
			}
			try
			{
				result = this.GetFileName();
			}
			catch (SecurityException)
			{
			}
			return result;
		}

		/// <summary>Gets the offset from the start of the Microsoft intermediate language (MSIL) code for the method that is executing. This offset might be an approximation depending on whether or not the just-in-time (JIT) compiler is generating debugging code. The generation of this debugging information is controlled by the <see cref="T:System.Diagnostics.DebuggableAttribute" />.</summary>
		/// <returns>The offset from the start of the MSIL code for the method that is executing.</returns>
		// Token: 0x0600314A RID: 12618 RVA: 0x000B3F98 File Offset: 0x000B2198
		public virtual int GetILOffset()
		{
			return this.ilOffset;
		}

		/// <summary>Gets the method in which the frame is executing.</summary>
		/// <returns>The method in which the frame is executing.</returns>
		// Token: 0x0600314B RID: 12619 RVA: 0x000B3FA0 File Offset: 0x000B21A0
		public virtual MethodBase GetMethod()
		{
			return this.methodBase;
		}

		/// <summary>Gets the offset from the start of the native just-in-time (JIT)-compiled code for the method that is being executed. The generation of this debugging information is controlled by the <see cref="T:System.Diagnostics.DebuggableAttribute" /> class.</summary>
		/// <returns>The offset from the start of the JIT-compiled code for the method that is being executed.</returns>
		// Token: 0x0600314C RID: 12620 RVA: 0x000B3FA8 File Offset: 0x000B21A8
		public virtual int GetNativeOffset()
		{
			return this.nativeOffset;
		}

		// Token: 0x0600314D RID: 12621 RVA: 0x000B3FB0 File Offset: 0x000B21B0
		internal long GetMethodAddress()
		{
			return this.methodAddress;
		}

		// Token: 0x0600314E RID: 12622 RVA: 0x000B3FB8 File Offset: 0x000B21B8
		internal uint GetMethodIndex()
		{
			return this.methodIndex;
		}

		// Token: 0x0600314F RID: 12623 RVA: 0x000B3FC0 File Offset: 0x000B21C0
		internal string GetInternalMethodName()
		{
			return this.internalMethodName;
		}

		/// <summary>Builds a readable representation of the stack trace.</summary>
		/// <returns>A readable representation of the stack trace.</returns>
		// Token: 0x06003150 RID: 12624 RVA: 0x000B3FC8 File Offset: 0x000B21C8
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (this.methodBase == null)
			{
				stringBuilder.Append(Locale.GetText("<unknown method>"));
			}
			else
			{
				stringBuilder.Append(this.methodBase.Name);
			}
			stringBuilder.Append(Locale.GetText(" at "));
			if (this.ilOffset == -1)
			{
				stringBuilder.Append(Locale.GetText("<unknown offset>"));
			}
			else
			{
				stringBuilder.Append(Locale.GetText("offset "));
				stringBuilder.Append(this.ilOffset);
			}
			stringBuilder.Append(Locale.GetText(" in file:line:column "));
			stringBuilder.Append(this.GetSecureFileName());
			stringBuilder.AppendFormat(":{0}:{1}", this.lineNumber, this.columnNumber);
			return stringBuilder.ToString();
		}

		/// <summary>Defines the value that is returned from the <see cref="M:System.Diagnostics.StackFrame.GetNativeOffset" /> or <see cref="M:System.Diagnostics.StackFrame.GetILOffset" /> method when the native or Microsoft intermediate language (MSIL) offset is unknown. This field is constant.</summary>
		// Token: 0x04001A95 RID: 6805
		public const int OFFSET_UNKNOWN = -1;

		// Token: 0x04001A96 RID: 6806
		private int ilOffset = -1;

		// Token: 0x04001A97 RID: 6807
		private int nativeOffset = -1;

		// Token: 0x04001A98 RID: 6808
		private long methodAddress;

		// Token: 0x04001A99 RID: 6809
		private uint methodIndex;

		// Token: 0x04001A9A RID: 6810
		private MethodBase methodBase;

		// Token: 0x04001A9B RID: 6811
		private string fileName;

		// Token: 0x04001A9C RID: 6812
		private int lineNumber;

		// Token: 0x04001A9D RID: 6813
		private int columnNumber;

		// Token: 0x04001A9E RID: 6814
		private string internalMethodName;
	}
}
