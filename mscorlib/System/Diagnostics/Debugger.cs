﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	/// <summary>Enables communication with a debugger. This class cannot be inherited.</summary>
	// Token: 0x02000421 RID: 1057
	[ComVisible(true)]
	[MonoTODO("The Debugger class is not functional")]
	public sealed class Debugger
	{
		/// <summary>Gets a value that indicates whether a debugger is attached to the process.</summary>
		/// <returns>
		///     <see langword="true" /> if a debugger is attached; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000815 RID: 2069
		// (get) Token: 0x06003136 RID: 12598 RVA: 0x000B3D39 File Offset: 0x000B1F39
		public static bool IsAttached
		{
			get
			{
				return Debugger.IsAttached_internal();
			}
		}

		// Token: 0x06003137 RID: 12599
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsAttached_internal();

		/// <summary>Signals a breakpoint to an attached debugger.</summary>
		/// <exception cref="T:System.Security.SecurityException">The <see cref="T:System.Security.Permissions.UIPermission" /> is not set to break into the debugger. </exception>
		// Token: 0x06003138 RID: 12600 RVA: 0x000020D3 File Offset: 0x000002D3
		public static void Break()
		{
		}

		/// <summary>Checks to see if logging is enabled by an attached debugger.</summary>
		/// <returns>
		///     <see langword="true" /> if a debugger is attached and logging is enabled; otherwise, <see langword="false" />. The attached debugger is the registered managed debugger in the <see langword="DbgManagedDebugger" /> registry key. For more information on this key, see Enabling JIT-Attach Debugging.</returns>
		// Token: 0x06003139 RID: 12601
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsLogging();

		/// <summary>Launches and attaches a debugger to the process.</summary>
		/// <returns>
		///     <see langword="true" /> if the startup is successful or if the debugger is already attached; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Security.SecurityException">The <see cref="T:System.Security.Permissions.UIPermission" /> is not set to start the debugger. </exception>
		// Token: 0x0600313A RID: 12602 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO("Not implemented")]
		public static bool Launch()
		{
			throw new NotImplementedException();
		}

		/// <summary>Posts a message for the attached debugger.</summary>
		/// <param name="level">A description of the importance of the message. </param>
		/// <param name="category">The category of the message. </param>
		/// <param name="message">The message to show. </param>
		// Token: 0x0600313B RID: 12603
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Log(int level, string category, string message);

		/// <summary>Notifies a debugger that execution is about to enter a path that involves a cross-thread dependency.</summary>
		// Token: 0x0600313C RID: 12604 RVA: 0x000020D3 File Offset: 0x000002D3
		public static void NotifyOfCrossThreadDependency()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Debugger" /> class. </summary>
		// Token: 0x0600313D RID: 12605 RVA: 0x00002050 File Offset: 0x00000250
		[Obsolete("Call the static methods directly on this type", true)]
		public Debugger()
		{
		}

		// Token: 0x0600313E RID: 12606 RVA: 0x000B3D40 File Offset: 0x000B1F40
		// Note: this type is marked as 'beforefieldinit'.
		static Debugger()
		{
		}

		/// <summary>Represents the default category of message with a constant.</summary>
		// Token: 0x04001A94 RID: 6804
		public static readonly string DefaultCategory = "";
	}
}
