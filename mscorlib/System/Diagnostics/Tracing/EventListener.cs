﻿using System;
using System.Collections.Generic;
using Unity;

namespace System.Diagnostics.Tracing
{
	/// <summary>Provides methods for enabling and disabling events from event sources.</summary>
	// Token: 0x02000446 RID: 1094
	public class EventListener : IDisposable
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Diagnostics.Tracing.EventListener" /> class.</summary>
		// Token: 0x060031EE RID: 12782 RVA: 0x00002050 File Offset: 0x00000250
		public EventListener()
		{
		}

		/// <summary>Gets a small non-negative number that represents the specified event source.</summary>
		/// <param name="eventSource">The event source to find the index for.</param>
		/// <returns>A small non-negative number that represents the specified event source.</returns>
		// Token: 0x060031EF RID: 12783 RVA: 0x00002526 File Offset: 0x00000726
		public static int EventSourceIndex(EventSource eventSource)
		{
			return 0;
		}

		/// <summary>Enables events for the specified event source that has the specified verbosity level or lower.</summary>
		/// <param name="eventSource">The event source to enable events for.</param>
		/// <param name="level">The level of events to enable.</param>
		// Token: 0x060031F0 RID: 12784 RVA: 0x000020D3 File Offset: 0x000002D3
		public void EnableEvents(EventSource eventSource, EventLevel level)
		{
		}

		/// <summary>Enables events for the specified event source that has the specified verbosity level or lower, and matching keyword flags.</summary>
		/// <param name="eventSource">The event source to enable events for.</param>
		/// <param name="level">The level of events to enable.</param>
		/// <param name="matchAnyKeyword">The keyword flags necessary to enable the events.</param>
		// Token: 0x060031F1 RID: 12785 RVA: 0x000020D3 File Offset: 0x000002D3
		public void EnableEvents(EventSource eventSource, EventLevel level, EventKeywords matchAnyKeyword)
		{
		}

		/// <summary>Enables events for the specified event source that has the specified verbosity level or lower, matching event keyword flag, and matching arguments.</summary>
		/// <param name="eventSource">The event source to enable events for.</param>
		/// <param name="level">The level of events to enable.</param>
		/// <param name="matchAnyKeyword">The keyword flags necessary to enable the events.</param>
		/// <param name="arguments">The arguments to be matched to enable the events.</param>
		// Token: 0x060031F2 RID: 12786 RVA: 0x000020D3 File Offset: 0x000002D3
		public void EnableEvents(EventSource eventSource, EventLevel level, EventKeywords matchAnyKeyword, IDictionary<string, string> arguments)
		{
		}

		/// <summary>Disables all events for the specified event source.</summary>
		/// <param name="eventSource">The event source to disable events for.</param>
		// Token: 0x060031F3 RID: 12787 RVA: 0x000020D3 File Offset: 0x000002D3
		public void DisableEvents(EventSource eventSource)
		{
		}

		/// <summary>Called for all existing event sources when the event listener is created and when a new event source is attached to the listener.</summary>
		/// <param name="eventSource">The event source.</param>
		// Token: 0x060031F4 RID: 12788 RVA: 0x000020D3 File Offset: 0x000002D3
		protected internal virtual void OnEventSourceCreated(EventSource eventSource)
		{
		}

		/// <summary>Called whenever an event has been written by an event source for which the event listener has enabled events.</summary>
		/// <param name="eventData">The event arguments that describe the event.</param>
		// Token: 0x060031F5 RID: 12789 RVA: 0x000020D3 File Offset: 0x000002D3
		protected internal virtual void OnEventWritten(EventWrittenEventArgs eventData)
		{
		}

		/// <summary>Releases the resources used by the current instance of the <see cref="T:System.Diagnostics.Tracing.EventListener" /> class.</summary>
		// Token: 0x060031F6 RID: 12790 RVA: 0x000020D3 File Offset: 0x000002D3
		public virtual void Dispose()
		{
		}

		/// <summary>Occurs when an event source (<see cref="T:System.Diagnostics.Tracing.EventSource" /> object) is attached to the dispatcher.</summary>
		// Token: 0x14000018 RID: 24
		// (add) Token: 0x060031F7 RID: 12791 RVA: 0x00002ABD File Offset: 0x00000CBD
		// (remove) Token: 0x060031F8 RID: 12792 RVA: 0x00002ABD File Offset: 0x00000CBD
		public event EventHandler<EventSourceCreatedEventArgs> EventSourceCreated
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}

		/// <summary>Occurs when an event has been written by an event source (<see cref="T:System.Diagnostics.Tracing.EventSource" /> object) for which 
		///   the event listener has enabled events.</summary>
		// Token: 0x14000019 RID: 25
		// (add) Token: 0x060031F9 RID: 12793 RVA: 0x00002ABD File Offset: 0x00000CBD
		// (remove) Token: 0x060031FA RID: 12794 RVA: 0x00002ABD File Offset: 0x00000CBD
		public event EventHandler<EventWrittenEventArgs> EventWritten
		{
			add
			{
				ThrowStub.ThrowNotSupportedException();
			}
			remove
			{
				ThrowStub.ThrowNotSupportedException();
			}
		}
	}
}
