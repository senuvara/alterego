﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Specifies a type to be passed to the <see cref="M:System.Diagnostics.Tracing.EventSource.Write``1(System.String,System.Diagnostics.Tracing.EventSourceOptions,``0)" /> method.</summary>
	// Token: 0x02000441 RID: 1089
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false)]
	public class EventDataAttribute : Attribute
	{
		/// <summary>Gets or set the name to apply to an event if the event type or property is not explicitly named.</summary>
		/// <returns>The name to apply to the event or property.</returns>
		// Token: 0x17000841 RID: 2113
		// (get) Token: 0x060031E5 RID: 12773 RVA: 0x000041F3 File Offset: 0x000023F3
		// (set) Token: 0x060031E6 RID: 12774 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public string Name
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Tracing.EventDataAttribute" /> class. </summary>
		// Token: 0x060031E7 RID: 12775 RVA: 0x000020BF File Offset: 0x000002BF
		public EventDataAttribute()
		{
		}
	}
}
