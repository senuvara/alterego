﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Identifies the level of an event.</summary>
	// Token: 0x02000438 RID: 1080
	public enum EventLevel
	{
		/// <summary>No level filtering is done on the event.</summary>
		// Token: 0x04001AD5 RID: 6869
		LogAlways,
		/// <summary>This level corresponds to a critical error, which is a serious error that has caused a major failure.</summary>
		// Token: 0x04001AD6 RID: 6870
		Critical,
		/// <summary>This level adds standard errors that signify a problem.</summary>
		// Token: 0x04001AD7 RID: 6871
		Error,
		/// <summary>This level adds warning events (for example, events that are published because a disk is nearing full capacity).</summary>
		// Token: 0x04001AD8 RID: 6872
		Warning,
		/// <summary>This level adds informational events or messages that are not errors. These events can help trace the progress or state of an application.</summary>
		// Token: 0x04001AD9 RID: 6873
		Informational,
		/// <summary>This level adds lengthy events or messages. It causes all events to be logged.</summary>
		// Token: 0x04001ADA RID: 6874
		Verbose
	}
}
