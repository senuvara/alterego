﻿using System;
using System.Runtime.CompilerServices;

namespace System.Diagnostics.Tracing
{
	/// <summary>Defines the standard operation codes that the event source attaches to events.</summary>
	// Token: 0x0200043A RID: 1082
	[FriendAccessAllowed]
	public enum EventOpcode
	{
		/// <summary>An informational event.</summary>
		// Token: 0x04001ADE RID: 6878
		Info,
		/// <summary>An event that is published when an application starts a new transaction or activity. This operation code can be embedded within another transaction or activity when multiple events that have the <see cref="F:System.Diagnostics.Tracing.EventOpcode.Start" /> code follow each other without an intervening event that has a <see cref="F:System.Diagnostics.Tracing.EventOpcode.Stop" /> code.</summary>
		// Token: 0x04001ADF RID: 6879
		Start,
		/// <summary>An event that is published when an activity or a transaction in an application ends. The event corresponds to the last unpaired event that has a <see cref="F:System.Diagnostics.Tracing.EventOpcode.Start" /> operation code.</summary>
		// Token: 0x04001AE0 RID: 6880
		Stop,
		/// <summary>A trace collection start event.</summary>
		// Token: 0x04001AE1 RID: 6881
		DataCollectionStart,
		/// <summary>A trace collection stop event.</summary>
		// Token: 0x04001AE2 RID: 6882
		DataCollectionStop,
		/// <summary>An extension event.</summary>
		// Token: 0x04001AE3 RID: 6883
		Extension,
		/// <summary>An event that is published after an activity in an application replies to an event.</summary>
		// Token: 0x04001AE4 RID: 6884
		Reply,
		/// <summary>An event that is published after an activity in an application resumes from a suspended state. The event should follow an event that has the <see cref="F:System.Diagnostics.Tracing.EventOpcode.Suspend" /> operation code.</summary>
		// Token: 0x04001AE5 RID: 6885
		Resume,
		/// <summary>An event that is published when an activity in an application is suspended.</summary>
		// Token: 0x04001AE6 RID: 6886
		Suspend,
		/// <summary>An event that is published when one activity in an application transfers data or system resources to another activity.</summary>
		// Token: 0x04001AE7 RID: 6887
		Send,
		/// <summary>An event that is published when one activity in an application receives data.</summary>
		// Token: 0x04001AE8 RID: 6888
		Receive = 240
	}
}
