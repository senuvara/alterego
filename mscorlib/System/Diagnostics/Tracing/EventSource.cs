﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Diagnostics.Tracing
{
	/// <summary>Provides the ability to create events for event tracing for Windows (ETW).</summary>
	// Token: 0x02000448 RID: 1096
	public class EventSource : IDisposable
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSource" /> class.</summary>
		// Token: 0x060031FB RID: 12795 RVA: 0x000B485C File Offset: 0x000B2A5C
		protected EventSource()
		{
			this.Name = base.GetType().Name;
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSource" /> class and specifies whether to throw an exception when an error occurs in the underlying Windows code.</summary>
		/// <param name="throwOnEventWriteErrors">
		///       <see langword="true" /> to throw an exception when an error occurs in the underlying Windows code; otherwise, <see langword="false" />.</param>
		// Token: 0x060031FC RID: 12796 RVA: 0x000B4875 File Offset: 0x000B2A75
		protected EventSource(bool throwOnEventWriteErrors) : this()
		{
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSource" /> class with the specified configuration settings.</summary>
		/// <param name="settings">A bitwise combination of the enumeration values that specify the configuration settings to apply to the event source.</param>
		// Token: 0x060031FD RID: 12797 RVA: 0x000B487D File Offset: 0x000B2A7D
		protected EventSource(EventSourceSettings settings) : this()
		{
			this.Settings = settings;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSource" /> to be used with non-contract events that contains the specified settings and traits.</summary>
		/// <param name="settings">A bitwise combination of the enumeration values that specify the configuration settings to apply to the event source.</param>
		/// <param name="traits">The key-value pairs that specify traits for the event source.</param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="traits" /> is not specified in key-value pairs.</exception>
		// Token: 0x060031FE RID: 12798 RVA: 0x000B488C File Offset: 0x000B2A8C
		protected EventSource(EventSourceSettings settings, params string[] traits) : this(settings)
		{
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSource" /> class with the specified name.</summary>
		/// <param name="eventSourceName">The name to apply to the event source. Must not be <see langword="null" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="eventSourceName" /> is <see langword="null" />.</exception>
		// Token: 0x060031FF RID: 12799 RVA: 0x000B4895 File Offset: 0x000B2A95
		public EventSource(string eventSourceName)
		{
			this.Name = eventSourceName;
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSource" /> class with the specified name and settings.</summary>
		/// <param name="eventSourceName">The name to apply to the event source. Must not be <see langword="null" />.</param>
		/// <param name="config">A bitwise combination of the enumeration values that specify the configuration settings to apply to the event source.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="eventSourceName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="eventSourceName" /> is <see langword="null" />.</exception>
		// Token: 0x06003200 RID: 12800 RVA: 0x000B48A4 File Offset: 0x000B2AA4
		public EventSource(string eventSourceName, EventSourceSettings config) : this(eventSourceName)
		{
			this.Settings = config;
		}

		/// <summary>Creates a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSource" /> class with the specified configuration settings.</summary>
		/// <param name="eventSourceName">The name to apply to the event source. Must not be <see langword="null" />.</param>
		/// <param name="config">A bitwise combination of the enumeration values that specify the configuration settings to apply to the event source.</param>
		/// <param name="traits">The key-value pairs that specify traits for the event source.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="eventSourceName" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="traits" /> is not specified in key-value pairs.</exception>
		// Token: 0x06003201 RID: 12801 RVA: 0x000B48B4 File Offset: 0x000B2AB4
		public EventSource(string eventSourceName, EventSourceSettings config, params string[] traits) : this(eventSourceName, config)
		{
		}

		/// <summary>Allows the <see cref="T:System.Diagnostics.Tracing.EventSource" /> object to attempt to free resources and perform other cleanup operations before the  object is reclaimed by garbage collection.</summary>
		// Token: 0x06003202 RID: 12802 RVA: 0x000B48C0 File Offset: 0x000B2AC0
		~EventSource()
		{
			this.Dispose(false);
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Gets any exception that was thrown during the construction of the event source.</summary>
		/// <returns>The exception that was thrown during the construction of the event source, or <see langword="null" /> if no exception was thrown. </returns>
		// Token: 0x17000844 RID: 2116
		// (get) Token: 0x06003203 RID: 12803 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public Exception ConstructionException
		{
			get
			{
				return null;
			}
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Gets the activity ID of the current thread. </summary>
		/// <returns>The activity ID of the current thread. </returns>
		// Token: 0x17000845 RID: 2117
		// (get) Token: 0x06003204 RID: 12804 RVA: 0x000B48F0 File Offset: 0x000B2AF0
		public static Guid CurrentThreadActivityId
		{
			get
			{
				return Guid.Empty;
			}
		}

		/// <summary>The unique identifier for the event source.</summary>
		/// <returns>A unique identifier for the event source.</returns>
		// Token: 0x17000846 RID: 2118
		// (get) Token: 0x06003205 RID: 12805 RVA: 0x000B48F0 File Offset: 0x000B2AF0
		public Guid Guid
		{
			get
			{
				return Guid.Empty;
			}
		}

		/// <summary>The friendly name of the class that is derived from the event source.</summary>
		/// <returns>The friendly name of the derived class.  The default is the simple name of the class.</returns>
		// Token: 0x17000847 RID: 2119
		// (get) Token: 0x06003206 RID: 12806 RVA: 0x000B48F7 File Offset: 0x000B2AF7
		// (set) Token: 0x06003207 RID: 12807 RVA: 0x000B48FF File Offset: 0x000B2AFF
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Name>k__BackingField = value;
			}
		}

		/// <summary>Gets the settings applied to this event source.</summary>
		/// <returns>The settings applied to this event source.</returns>
		// Token: 0x17000848 RID: 2120
		// (get) Token: 0x06003208 RID: 12808 RVA: 0x000B4908 File Offset: 0x000B2B08
		// (set) Token: 0x06003209 RID: 12809 RVA: 0x000B4910 File Offset: 0x000B2B10
		public EventSourceSettings Settings
		{
			[CompilerGenerated]
			get
			{
				return this.<Settings>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<Settings>k__BackingField = value;
			}
		}

		/// <summary>Determines whether the current event source is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if the current event source is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600320A RID: 12810 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsEnabled()
		{
			return false;
		}

		/// <summary>Determines whether the current event source that has the specified level and keyword is enabled.</summary>
		/// <param name="level">The level of the event source.</param>
		/// <param name="keywords">The keyword of the event source.</param>
		/// <returns>
		///     <see langword="true" /> if the event source is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600320B RID: 12811 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsEnabled(EventLevel level, EventKeywords keywords)
		{
			return false;
		}

		/// <summary>Determines whether the current event source is enabled for events with the specified level, keywords and channel.</summary>
		/// <param name="level">The event level to check. An event source will be considered enabled when its level is greater than or equal to <paramref name="level" />.</param>
		/// <param name="keywords">The event keywords to check.</param>
		/// <param name="channel">The event channel to check.</param>
		/// <returns>
		///     <see langword="true" /> if the event source is enabled for the specified event level, keywords and channel; otherwise, <see langword="false" />.The result of this method is only an approximation of whether a particular event is active.  Use it to avoid expensive computation for logging when logging is disabled.   Event sources may have additional filtering that determines their activity..</returns>
		// Token: 0x0600320C RID: 12812 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsEnabled(EventLevel level, EventKeywords keywords, EventChannel channel)
		{
			return false;
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.Diagnostics.Tracing.EventSource" /> class.</summary>
		// Token: 0x0600320D RID: 12813 RVA: 0x000B4919 File Offset: 0x000B2B19
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Gets the trait value associated with the specified key.</summary>
		/// <param name="key">The key of the trait to get.</param>
		/// <returns>The trait value associated with the specified key. If the key is not found, returns <see langword="null" />.</returns>
		// Token: 0x0600320E RID: 12814 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public string GetTrait(string key)
		{
			return null;
		}

		/// <summary>Writes an event without fields, but with the specified name and default options.</summary>
		/// <param name="eventName">The name of the event to write.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="eventName" /> is <see langword="null" />.</exception>
		// Token: 0x0600320F RID: 12815 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Write(string eventName)
		{
		}

		/// <summary>Writes an event without fields, but with the specified name and options.</summary>
		/// <param name="eventName">The name of the event to write.</param>
		/// <param name="options">The options such as level, keywords and operation code for the event.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="eventName" /> is <see langword="null" />.</exception>
		// Token: 0x06003210 RID: 12816 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Write(string eventName, EventSourceOptions options)
		{
		}

		/// <summary>Writes an event with the specified name and data.</summary>
		/// <param name="eventName">The name of the event.</param>
		/// <param name="data">The event data. This type must be an anonymous type or marked with the <see cref="T:System.Diagnostics.Tracing.EventDataAttribute" /> attribute.</param>
		/// <typeparam name="T">The type that defines the event and its associated data. This type must be an anonymous type or marked with the <see cref="T:System.Diagnostics.Tracing.EventSourceAttribute" /> attribute.</typeparam>
		// Token: 0x06003211 RID: 12817 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Write<T>(string eventName, T data)
		{
		}

		/// <summary>Writes an event with the specified name, event data and options.</summary>
		/// <param name="eventName">The name of the event.</param>
		/// <param name="options">The event options.</param>
		/// <param name="data">The event data. This type must be an anonymous type or marked with the <see cref="T:System.Diagnostics.Tracing.EventDataAttribute" /> attribute.</param>
		/// <typeparam name="T">The type that defines the event and its associated data. This type must be an anonymous type or marked with the <see cref="T:System.Diagnostics.Tracing.EventSourceAttribute" /> attribute.</typeparam>
		// Token: 0x06003212 RID: 12818 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Write<T>(string eventName, EventSourceOptions options, T data)
		{
		}

		/// <summary>Writes an event with the specified name, options and event data.</summary>
		/// <param name="eventName">The name of the event.</param>
		/// <param name="options">The event options.</param>
		/// <param name="data">The event data. This type must be an anonymous type or marked with the <see cref="T:System.Diagnostics.Tracing.EventDataAttribute" /> attribute.</param>
		/// <typeparam name="T">The type that defines the event and its associated data. This type must be an anonymous type or marked with the <see cref="T:System.Diagnostics.Tracing.EventSourceAttribute" /> attribute.</typeparam>
		// Token: 0x06003213 RID: 12819 RVA: 0x000020D3 File Offset: 0x000002D3
		[CLSCompliant(false)]
		public void Write<T>(string eventName, ref EventSourceOptions options, ref T data)
		{
		}

		/// <summary>Writes an event with the specified name, options, related activity and event data.</summary>
		/// <param name="eventName">The name of the event.</param>
		/// <param name="options">The event options.</param>
		/// <param name="activityId">The ID of the activity associated with the event.</param>
		/// <param name="relatedActivityId">The ID of an associated activity, or <see cref="F:System.Guid.Empty" /> if there is no associated activity.</param>
		/// <param name="data">The event data. This type must be an anonymous type or marked with the <see cref="T:System.Diagnostics.Tracing.EventDataAttribute" /> attribute.</param>
		/// <typeparam name="T">The type that defines the event and its associated data. This type must be an anonymous type or marked with the <see cref="T:System.Diagnostics.Tracing.EventSourceAttribute" /> attribute.</typeparam>
		// Token: 0x06003214 RID: 12820 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Write<T>(string eventName, ref EventSourceOptions options, ref Guid activityId, ref Guid relatedActivityId, ref T data)
		{
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Diagnostics.Tracing.EventSource" /> class and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06003215 RID: 12821 RVA: 0x000020D3 File Offset: 0x000002D3
		protected virtual void Dispose(bool disposing)
		{
		}

		/// <summary>Called when the current event source is updated by the controller.</summary>
		/// <param name="command">The arguments for the event.</param>
		// Token: 0x06003216 RID: 12822 RVA: 0x000020D3 File Offset: 0x000002D3
		protected virtual void OnEventCommand(EventCommandEventArgs command)
		{
		}

		/// <summary>Writes an event by using the provided event identifier.</summary>
		/// <param name="eventId">The event identifier. This value should be between 0 and 65535.</param>
		// Token: 0x06003217 RID: 12823 RVA: 0x000B4928 File Offset: 0x000B2B28
		protected void WriteEvent(int eventId)
		{
			this.WriteEvent(eventId, new object[0]);
		}

		/// <summary>Writes an event by using the provided event identifier and byte array argument.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A byte array argument.</param>
		// Token: 0x06003218 RID: 12824 RVA: 0x000B4937 File Offset: 0x000B2B37
		protected void WriteEvent(int eventId, byte[] arg1)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1
			});
		}

		/// <summary>Writes an event by using the provided event identifier and 32-bit integer argument.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">
		///       An integer argument.</param>
		// Token: 0x06003219 RID: 12825 RVA: 0x000B494A File Offset: 0x000B2B4A
		protected void WriteEvent(int eventId, int arg1)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1
			});
		}

		/// <summary>Writes an event by using the provided event identifier and string argument.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A string argument.</param>
		// Token: 0x0600321A RID: 12826 RVA: 0x000B4937 File Offset: 0x000B2B37
		protected void WriteEvent(int eventId, string arg1)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1
			});
		}

		/// <summary>Writes an event by using the provided event identifier and 32-bit integer arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">An integer argument.</param>
		/// <param name="arg2">An integer argument.</param>
		// Token: 0x0600321B RID: 12827 RVA: 0x000B4962 File Offset: 0x000B2B62
		protected void WriteEvent(int eventId, int arg1, int arg2)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2
			});
		}

		/// <summary>Writes an event by using the provided event identifier and 32-bit integer arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">An integer argument.</param>
		/// <param name="arg2">An integer argument.</param>
		/// <param name="arg3">An integer argument.</param>
		// Token: 0x0600321C RID: 12828 RVA: 0x000B4983 File Offset: 0x000B2B83
		protected void WriteEvent(int eventId, int arg1, int arg2, int arg3)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2,
				arg3
			});
		}

		/// <summary>Writes an event by using the provided event identifier and 32-bit integer and string arguments.</summary>
		/// <param name="eventId">The event identifier. This value should be between 0 and 65535.</param>
		/// <param name="arg1">A 32-bit integer argument.</param>
		/// <param name="arg2">A string argument.</param>
		// Token: 0x0600321D RID: 12829 RVA: 0x000B49AE File Offset: 0x000B2BAE
		protected void WriteEvent(int eventId, int arg1, string arg2)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2
			});
		}

		/// <summary>Writes an event by using the provided event identifier and 64-bit integer argument.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A 64 bit integer argument.</param>
		// Token: 0x0600321E RID: 12830 RVA: 0x000B49CA File Offset: 0x000B2BCA
		protected void WriteEvent(int eventId, long arg1)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1
			});
		}

		/// <summary>Writes the event data using the specified indentifier and 64-bit integer and byte array arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A 64-bit integer argument.</param>
		/// <param name="arg2">A byte array argument.</param>
		// Token: 0x0600321F RID: 12831 RVA: 0x000B49E2 File Offset: 0x000B2BE2
		protected void WriteEvent(int eventId, long arg1, byte[] arg2)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2
			});
		}

		/// <summary>Writes an event by using the provided event identifier and 64-bit arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A 64 bit integer argument.</param>
		/// <param name="arg2">A 64 bit integer argument.</param>
		// Token: 0x06003220 RID: 12832 RVA: 0x000B49FE File Offset: 0x000B2BFE
		protected void WriteEvent(int eventId, long arg1, long arg2)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2
			});
		}

		/// <summary>Writes an event by using the provided event identifier and 64-bit arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A 64 bit integer argument.</param>
		/// <param name="arg2">A 64 bit integer argument.</param>
		/// <param name="arg3">A 64 bit integer argument.</param>
		// Token: 0x06003221 RID: 12833 RVA: 0x000B4A1F File Offset: 0x000B2C1F
		protected void WriteEvent(int eventId, long arg1, long arg2, long arg3)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2,
				arg3
			});
		}

		/// <summary>Writes an event by using the provided event identifier and 64-bit integer, and string arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A 64-bit integer argument.</param>
		/// <param name="arg2">A string argument.</param>
		// Token: 0x06003222 RID: 12834 RVA: 0x000B49E2 File Offset: 0x000B2BE2
		protected void WriteEvent(int eventId, long arg1, string arg2)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2
			});
		}

		/// <summary>Writes an event by using the provided event identifier and array of arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="args">An array of objects.</param>
		// Token: 0x06003223 RID: 12835 RVA: 0x000020D3 File Offset: 0x000002D3
		protected void WriteEvent(int eventId, params object[] args)
		{
		}

		/// <summary>Writes an event by using the provided event identifier and arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A string argument.</param>
		/// <param name="arg2">A 32 bit integer argument.</param>
		// Token: 0x06003224 RID: 12836 RVA: 0x000B4A4A File Offset: 0x000B2C4A
		protected void WriteEvent(int eventId, string arg1, int arg2)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2
			});
		}

		/// <summary>Writes an event by using the provided event identifier and arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A string argument.</param>
		/// <param name="arg2">A 32 bit integer argument.</param>
		/// <param name="arg3">A 32 bit integer argument.</param>
		// Token: 0x06003225 RID: 12837 RVA: 0x000B4A66 File Offset: 0x000B2C66
		protected void WriteEvent(int eventId, string arg1, int arg2, int arg3)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2,
				arg3
			});
		}

		/// <summary>Writes an event by using the provided event identifier and arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A string argument.</param>
		/// <param name="arg2">A 64 bit integer argument.</param>
		// Token: 0x06003226 RID: 12838 RVA: 0x000B4A8C File Offset: 0x000B2C8C
		protected void WriteEvent(int eventId, string arg1, long arg2)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2
			});
		}

		/// <summary>Writes an event by using the provided event identifier and string arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A string argument.</param>
		/// <param name="arg2">A string argument.</param>
		// Token: 0x06003227 RID: 12839 RVA: 0x000B4AA8 File Offset: 0x000B2CA8
		protected void WriteEvent(int eventId, string arg1, string arg2)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2
			});
		}

		/// <summary>Writes an event by using the provided event identifier and string arguments.</summary>
		/// <param name="eventId">The event identifier.  This value should be between 0 and 65535.</param>
		/// <param name="arg1">A string argument.</param>
		/// <param name="arg2">A string argument.</param>
		/// <param name="arg3">A string argument.</param>
		// Token: 0x06003228 RID: 12840 RVA: 0x000B4ABF File Offset: 0x000B2CBF
		protected void WriteEvent(int eventId, string arg1, string arg2, string arg3)
		{
			this.WriteEvent(eventId, new object[]
			{
				arg1,
				arg2,
				arg3
			});
		}

		/// <summary>Creates a new <see cref="Overload:System.Diagnostics.Tracing.EventSource.WriteEvent" /> overload by using the provided event identifier and event data.</summary>
		/// <param name="eventId">The event identifier.</param>
		/// <param name="eventDataCount">The number of event data items.</param>
		/// <param name="data">The structure that contains the event data.</param>
		// Token: 0x06003229 RID: 12841 RVA: 0x000020D3 File Offset: 0x000002D3
		[CLSCompliant(false)]
		protected unsafe void WriteEventCore(int eventId, int eventDataCount, EventSource.EventData* data)
		{
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Writes an event that indicates that the current activity is related to another activity. </summary>
		/// <param name="eventId">An identifier that uniquely identifies this event within the <see cref="T:System.Diagnostics.Tracing.EventSource" />. </param>
		/// <param name="relatedActivityId">The related activity identifier. </param>
		/// <param name="args">An array of objects that contain data about the event. </param>
		// Token: 0x0600322A RID: 12842 RVA: 0x000020D3 File Offset: 0x000002D3
		protected void WriteEventWithRelatedActivityId(int eventId, Guid relatedActivityId, params object[] args)
		{
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Writes an event that indicates that the current activity is related to another activity.</summary>
		/// <param name="eventId">An identifier that uniquely identifies this event within the <see cref="T:System.Diagnostics.Tracing.EventSource" />.</param>
		/// <param name="relatedActivityId">A pointer to the GUID of the related activity ID. </param>
		/// <param name="eventDataCount">The number of items in the <paramref name="data" /> field. </param>
		/// <param name="data">A pointer to the first item in the event data field. </param>
		// Token: 0x0600322B RID: 12843 RVA: 0x000020D3 File Offset: 0x000002D3
		[CLSCompliant(false)]
		protected unsafe void WriteEventWithRelatedActivityIdCore(int eventId, Guid* relatedActivityId, int eventDataCount, EventSource.EventData* data)
		{
		}

		/// <summary>Occurs when a command comes from an event listener.</summary>
		// Token: 0x1400001A RID: 26
		// (add) Token: 0x0600322C RID: 12844 RVA: 0x000041F3 File Offset: 0x000023F3
		// (remove) Token: 0x0600322D RID: 12845 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public event EventHandler<EventCommandEventArgs> EventCommandExecuted
		{
			add
			{
				throw new NotImplementedException();
			}
			remove
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Returns a string of the XML manifest that is associated with the current event source.</summary>
		/// <param name="eventSourceType">The type of the event source.</param>
		/// <param name="assemblyPathToIncludeInManifest">The path to the assembly file (.dll) to include in the provider element of the manifest. </param>
		/// <returns>The XML data string.</returns>
		// Token: 0x0600322E RID: 12846 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static string GenerateManifest(Type eventSourceType, string assemblyPathToIncludeInManifest)
		{
			throw new NotImplementedException();
		}

		/// <summary>Returns a string of the XML manifest that is associated with the current event source.</summary>
		/// <param name="eventSourceType">The type of the event source.</param>
		/// <param name="assemblyPathToIncludeInManifest">The path to the assembly file (.dll) file to include in the provider element of the manifest. </param>
		/// <param name="flags">A bitwise combination of the enumeration values that specify how the manifest is generated.</param>
		/// <returns>The XML data string or <see langword="null" /> (see remarks).</returns>
		// Token: 0x0600322F RID: 12847 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static string GenerateManifest(Type eventSourceType, string assemblyPathToIncludeInManifest, EventManifestOptions flags)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the unique identifier for this implementation of the event source.</summary>
		/// <param name="eventSourceType">The type of the event source.</param>
		/// <returns>A unique identifier for this event source type.</returns>
		// Token: 0x06003230 RID: 12848 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static Guid GetGuid(Type eventSourceType)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the friendly name of the event source.</summary>
		/// <param name="eventSourceType">The type of the event source.</param>
		/// <returns>The friendly name of the event source. The default is the simple name of the class.</returns>
		// Token: 0x06003231 RID: 12849 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static string GetName(Type eventSourceType)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a snapshot of all the event sources for the application domain.</summary>
		/// <returns>An enumeration of all the event sources in the application domain.</returns>
		// Token: 0x06003232 RID: 12850 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static IEnumerable<EventSource> GetSources()
		{
			throw new NotImplementedException();
		}

		/// <summary>Sends a command to a specified event source.</summary>
		/// <param name="eventSource">The event source to send the command to.</param>
		/// <param name="command">The event command to send.</param>
		/// <param name="commandArguments">The arguments for the event command.</param>
		// Token: 0x06003233 RID: 12851 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static void SendCommand(EventSource eventSource, EventCommand command, IDictionary<string, string> commandArguments)
		{
			throw new NotImplementedException();
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Sets the activity ID on the current thread.</summary>
		/// <param name="activityId">The current thread's new activity ID, or <see cref="F:System.Guid.Empty" /> to indicate that work on the current thread is not associated with any activity. </param>
		// Token: 0x06003234 RID: 12852 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static void SetCurrentThreadActivityId(Guid activityId)
		{
			throw new NotImplementedException();
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Sets the activity ID on the current thread, and returns the previous activity ID.</summary>
		/// <param name="activityId">The current thread's new activity ID, or <see cref="F:System.Guid.Empty" /> to indicate that work on the current thread is not associated with any activity.</param>
		/// <param name="oldActivityThatWillContinue">When this method returns, contains the previous activity ID on the current thread. </param>
		// Token: 0x06003235 RID: 12853 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static void SetCurrentThreadActivityId(Guid activityId, out Guid oldActivityThatWillContinue)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04001B19 RID: 6937
		[CompilerGenerated]
		private string <Name>k__BackingField;

		// Token: 0x04001B1A RID: 6938
		[CompilerGenerated]
		private EventSourceSettings <Settings>k__BackingField;

		/// <summary>Provides the event data for creating fast <see cref="Overload:System.Diagnostics.Tracing.EventSource.WriteEvent" /> overloads by using the <see cref="M:System.Diagnostics.Tracing.EventSource.WriteEventCore(System.Int32,System.Int32,System.Diagnostics.Tracing.EventSource.EventData*)" /> method.</summary>
		// Token: 0x02000449 RID: 1097
		protected internal struct EventData
		{
			/// <summary>Gets or sets the pointer to the data for the new <see cref="Overload:System.Diagnostics.Tracing.EventSource.WriteEvent" /> overload.</summary>
			/// <returns>The pointer to the data.</returns>
			// Token: 0x17000849 RID: 2121
			// (get) Token: 0x06003236 RID: 12854 RVA: 0x000B4ADB File Offset: 0x000B2CDB
			// (set) Token: 0x06003237 RID: 12855 RVA: 0x000B4AE3 File Offset: 0x000B2CE3
			public IntPtr DataPointer
			{
				[CompilerGenerated]
				get
				{
					return this.<DataPointer>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<DataPointer>k__BackingField = value;
				}
			}

			/// <summary>Gets or sets the number of payload items in the new <see cref="Overload:System.Diagnostics.Tracing.EventSource.WriteEvent" /> overload.</summary>
			/// <returns>The number of payload items in the new overload.</returns>
			// Token: 0x1700084A RID: 2122
			// (get) Token: 0x06003238 RID: 12856 RVA: 0x000B4AEC File Offset: 0x000B2CEC
			// (set) Token: 0x06003239 RID: 12857 RVA: 0x000B4AF4 File Offset: 0x000B2CF4
			public int Size
			{
				[CompilerGenerated]
				get
				{
					return this.<Size>k__BackingField;
				}
				[CompilerGenerated]
				set
				{
					this.<Size>k__BackingField = value;
				}
			}

			// Token: 0x04001B1B RID: 6939
			[CompilerGenerated]
			private IntPtr <DataPointer>k__BackingField;

			// Token: 0x04001B1C RID: 6940
			[CompilerGenerated]
			private int <Size>k__BackingField;
		}
	}
}
