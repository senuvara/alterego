﻿using System;
using System.Runtime.CompilerServices;

namespace System.Diagnostics.Tracing
{
	/// <summary>Allows the event tracing for Windows (ETW) name to be defined independently of the name of the event source class.   </summary>
	// Token: 0x0200044A RID: 1098
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class EventSourceAttribute : Attribute
	{
		/// <summary>Gets or sets the event source identifier.</summary>
		/// <returns>The event source identifier.</returns>
		// Token: 0x1700084B RID: 2123
		// (get) Token: 0x0600323A RID: 12858 RVA: 0x000B4AFD File Offset: 0x000B2CFD
		// (set) Token: 0x0600323B RID: 12859 RVA: 0x000B4B05 File Offset: 0x000B2D05
		public string Guid
		{
			[CompilerGenerated]
			get
			{
				return this.<Guid>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Guid>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the name of the localization resource file.</summary>
		/// <returns>The name of the localization resource file, or <see langword="null" /> if the localization resource file does not exist.</returns>
		// Token: 0x1700084C RID: 2124
		// (get) Token: 0x0600323C RID: 12860 RVA: 0x000B4B0E File Offset: 0x000B2D0E
		// (set) Token: 0x0600323D RID: 12861 RVA: 0x000B4B16 File Offset: 0x000B2D16
		public string LocalizationResources
		{
			[CompilerGenerated]
			get
			{
				return this.<LocalizationResources>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<LocalizationResources>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the name of the event source.</summary>
		/// <returns>The name of the event source.</returns>
		// Token: 0x1700084D RID: 2125
		// (get) Token: 0x0600323E RID: 12862 RVA: 0x000B4B1F File Offset: 0x000B2D1F
		// (set) Token: 0x0600323F RID: 12863 RVA: 0x000B4B27 File Offset: 0x000B2D27
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Name>k__BackingField = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSourceAttribute" /> class.</summary>
		// Token: 0x06003240 RID: 12864 RVA: 0x000020BF File Offset: 0x000002BF
		public EventSourceAttribute()
		{
		}

		// Token: 0x04001B1D RID: 6941
		[CompilerGenerated]
		private string <Guid>k__BackingField;

		// Token: 0x04001B1E RID: 6942
		[CompilerGenerated]
		private string <LocalizationResources>k__BackingField;

		// Token: 0x04001B1F RID: 6943
		[CompilerGenerated]
		private string <Name>k__BackingField;
	}
}
