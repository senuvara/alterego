﻿using System;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Diagnostics.Tracing
{
	/// <summary>Provides data for the <see cref="E:System.Diagnostics.Tracing.EventListener.EventSourceCreated" /> event.</summary>
	// Token: 0x02000A96 RID: 2710
	public class EventSourceCreatedEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Tracing.EventSourceCreatedEventArgs" /> class.</summary>
		// Token: 0x06005EC9 RID: 24265 RVA: 0x00002ABD File Offset: 0x00000CBD
		public EventSourceCreatedEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Get the event source that is attaching to the listener.</summary>
		/// <returns>The event source that is attaching to the listener.</returns>
		// Token: 0x1700110B RID: 4363
		// (get) Token: 0x06005ECA RID: 24266 RVA: 0x0005AB11 File Offset: 0x00058D11
		public EventSource EventSource
		{
			[CompilerGenerated]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}
	}
}
