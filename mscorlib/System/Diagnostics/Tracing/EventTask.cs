﻿using System;
using System.Runtime.CompilerServices;

namespace System.Diagnostics.Tracing
{
	/// <summary>Defines the tasks that apply to events.</summary>
	// Token: 0x02000439 RID: 1081
	[FriendAccessAllowed]
	public enum EventTask
	{
		/// <summary>Undefined task.</summary>
		// Token: 0x04001ADC RID: 6876
		None
	}
}
