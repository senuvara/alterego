﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Describes the command (<see cref="P:System.Diagnostics.Tracing.EventCommandEventArgs.Command" /> property) that is passed to the <see cref="M:System.Diagnostics.Tracing.EventSource.OnEventCommand(System.Diagnostics.Tracing.EventCommandEventArgs)" /> callback.</summary>
	// Token: 0x0200043E RID: 1086
	public enum EventCommand
	{
		/// <summary>Update the event.</summary>
		// Token: 0x04001B05 RID: 6917
		Update,
		/// <summary>Send the manifest.</summary>
		// Token: 0x04001B06 RID: 6918
		SendManifest = -1,
		/// <summary>Enable the event.</summary>
		// Token: 0x04001B07 RID: 6919
		Enable = -2,
		/// <summary>Disable the event.</summary>
		// Token: 0x04001B08 RID: 6920
		Disable = -3
	}
}
