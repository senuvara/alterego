﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Identifies a method that is not generating an event.</summary>
	// Token: 0x0200044D RID: 1101
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class NonEventAttribute : Attribute
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Diagnostics.Tracing.NonEventAttribute" /> class.</summary>
		// Token: 0x06003259 RID: 12889 RVA: 0x000020BF File Offset: 0x000002BF
		public NonEventAttribute()
		{
		}
	}
}
