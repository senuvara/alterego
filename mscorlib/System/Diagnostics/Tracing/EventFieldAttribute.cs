﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>The <see cref="T:System.Diagnostics.Tracing.EventFieldAttribute" /> is placed on fields of user-defined types that are passed as <see cref="T:System.Diagnostics.Tracing.EventSource" /> payloads. </summary>
	// Token: 0x02000442 RID: 1090
	[AttributeUsage(AttributeTargets.Property)]
	public class EventFieldAttribute : Attribute
	{
		/// <summary>Gets and sets the value that specifies how to format the value of a user-defined type.</summary>
		/// <returns>Returns a<see cref="T:System.Diagnostics.Tracing.EventFieldFormat" /> value.</returns>
		// Token: 0x17000842 RID: 2114
		// (get) Token: 0x060031E8 RID: 12776 RVA: 0x000041F3 File Offset: 0x000023F3
		// (set) Token: 0x060031E9 RID: 12777 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public EventFieldFormat Format
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets and sets the user-defined <see cref="T:System.Diagnostics.Tracing.EventFieldTags" /> value that is required for fields that contain data that isn't one of the supported types. </summary>
		/// <returns>Returns <see cref="T:System.Diagnostics.Tracing.EventFieldTags" />.</returns>
		// Token: 0x17000843 RID: 2115
		// (get) Token: 0x060031EA RID: 12778 RVA: 0x000041F3 File Offset: 0x000023F3
		// (set) Token: 0x060031EB RID: 12779 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public EventFieldTags Tags
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Tracing.EventFieldAttribute" /> class.</summary>
		// Token: 0x060031EC RID: 12780 RVA: 0x000020BF File Offset: 0x000002BF
		public EventFieldAttribute()
		{
		}
	}
}
