﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Specifies the tracking of activity start and stop events. You should only use the lower 24 bits. For more information, see <see cref="T:System.Diagnostics.Tracing.EventSourceOptions" /> and <see cref="M:System.Diagnostics.Tracing.EventSource.Write(System.String,System.Diagnostics.Tracing.EventSourceOptions)" />.</summary>
	// Token: 0x02000435 RID: 1077
	[Flags]
	public enum EventTags
	{
		/// <summary>Specifies no tag and is equal to zero.</summary>
		// Token: 0x04001ACE RID: 6862
		None = 0
	}
}
