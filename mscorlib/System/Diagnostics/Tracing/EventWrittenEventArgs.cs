﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using Unity;

namespace System.Diagnostics.Tracing
{
	/// <summary>Provides data for the <see cref="M:System.Diagnostics.Tracing.EventListener.OnEventWritten(System.Diagnostics.Tracing.EventWrittenEventArgs)" /> callback.</summary>
	// Token: 0x0200044C RID: 1100
	public class EventWrittenEventArgs : EventArgs
	{
		// Token: 0x06003241 RID: 12865 RVA: 0x000B4B30 File Offset: 0x000B2D30
		internal EventWrittenEventArgs(EventSource eventSource)
		{
			this.EventSource = eventSource;
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Gets the activity ID on the thread that the event was written to. </summary>
		/// <returns>The activity ID on the thread that the event was written to. </returns>
		// Token: 0x1700084E RID: 2126
		// (get) Token: 0x06003242 RID: 12866 RVA: 0x000B4B3F File Offset: 0x000B2D3F
		public Guid ActivityId
		{
			get
			{
				return EventSource.CurrentThreadActivityId;
			}
		}

		/// <summary>Gets the channel for the event.</summary>
		/// <returns>The channel for the event.</returns>
		// Token: 0x1700084F RID: 2127
		// (get) Token: 0x06003243 RID: 12867 RVA: 0x00002526 File Offset: 0x00000726
		public EventChannel Channel
		{
			get
			{
				return EventChannel.None;
			}
		}

		/// <summary>Gets the event identifier.</summary>
		/// <returns>The event identifier.</returns>
		// Token: 0x17000850 RID: 2128
		// (get) Token: 0x06003244 RID: 12868 RVA: 0x000B4B46 File Offset: 0x000B2D46
		// (set) Token: 0x06003245 RID: 12869 RVA: 0x000B4B4E File Offset: 0x000B2D4E
		public int EventId
		{
			[CompilerGenerated]
			get
			{
				return this.<EventId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<EventId>k__BackingField = value;
			}
		}

		/// <summary>Gets the name of the event.</summary>
		/// <returns>The name of the event.</returns>
		// Token: 0x17000851 RID: 2129
		// (get) Token: 0x06003246 RID: 12870 RVA: 0x000B4B57 File Offset: 0x000B2D57
		// (set) Token: 0x06003247 RID: 12871 RVA: 0x000B4B5F File Offset: 0x000B2D5F
		public string EventName
		{
			[CompilerGenerated]
			get
			{
				return this.<EventName>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<EventName>k__BackingField = value;
			}
		}

		/// <summary>Gets the event source object.</summary>
		/// <returns>The event source object.</returns>
		// Token: 0x17000852 RID: 2130
		// (get) Token: 0x06003248 RID: 12872 RVA: 0x000B4B68 File Offset: 0x000B2D68
		// (set) Token: 0x06003249 RID: 12873 RVA: 0x000B4B70 File Offset: 0x000B2D70
		public EventSource EventSource
		{
			[CompilerGenerated]
			get
			{
				return this.<EventSource>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<EventSource>k__BackingField = value;
			}
		}

		/// <summary>Gets the keywords for the event.</summary>
		/// <returns>The keywords for the event.</returns>
		// Token: 0x17000853 RID: 2131
		// (get) Token: 0x0600324A RID: 12874 RVA: 0x0005AE36 File Offset: 0x00059036
		public EventKeywords Keywords
		{
			get
			{
				return EventKeywords.None;
			}
		}

		/// <summary>Gets the level of the event.</summary>
		/// <returns>The level of the event.</returns>
		// Token: 0x17000854 RID: 2132
		// (get) Token: 0x0600324B RID: 12875 RVA: 0x00002526 File Offset: 0x00000726
		public EventLevel Level
		{
			get
			{
				return EventLevel.LogAlways;
			}
		}

		/// <summary>Gets the message for the event.</summary>
		/// <returns>The message for the event.</returns>
		// Token: 0x17000855 RID: 2133
		// (get) Token: 0x0600324C RID: 12876 RVA: 0x000B4B79 File Offset: 0x000B2D79
		// (set) Token: 0x0600324D RID: 12877 RVA: 0x000B4B81 File Offset: 0x000B2D81
		public string Message
		{
			[CompilerGenerated]
			get
			{
				return this.<Message>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Message>k__BackingField = value;
			}
		}

		/// <summary>Gets the operation code for the event.</summary>
		/// <returns>The operation code for the event.</returns>
		// Token: 0x17000856 RID: 2134
		// (get) Token: 0x0600324E RID: 12878 RVA: 0x00002526 File Offset: 0x00000726
		public EventOpcode Opcode
		{
			get
			{
				return EventOpcode.Info;
			}
		}

		/// <summary>Gets the payload for the event.</summary>
		/// <returns>The payload for the event.</returns>
		// Token: 0x17000857 RID: 2135
		// (get) Token: 0x0600324F RID: 12879 RVA: 0x000B4B8A File Offset: 0x000B2D8A
		// (set) Token: 0x06003250 RID: 12880 RVA: 0x000B4B92 File Offset: 0x000B2D92
		public ReadOnlyCollection<object> Payload
		{
			[CompilerGenerated]
			get
			{
				return this.<Payload>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<Payload>k__BackingField = value;
			}
		}

		/// <summary>Returns a list of strings that represent the property names of the event.</summary>
		/// <returns>Returns <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" />.</returns>
		// Token: 0x17000858 RID: 2136
		// (get) Token: 0x06003251 RID: 12881 RVA: 0x000B4B9B File Offset: 0x000B2D9B
		// (set) Token: 0x06003252 RID: 12882 RVA: 0x000B4BA3 File Offset: 0x000B2DA3
		public ReadOnlyCollection<string> PayloadNames
		{
			[CompilerGenerated]
			get
			{
				return this.<PayloadNames>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<PayloadNames>k__BackingField = value;
			}
		}

		/// <summary>[Supported in the .NET Framework 4.5.1 and later versions] Gets the identifier of an activity that is related to the activity represented by the current instance. </summary>
		/// <returns>The identifier of the related activity, or <see cref="F:System.Guid.Empty" /> if there is no related activity.</returns>
		// Token: 0x17000859 RID: 2137
		// (get) Token: 0x06003253 RID: 12883 RVA: 0x000B4BAC File Offset: 0x000B2DAC
		// (set) Token: 0x06003254 RID: 12884 RVA: 0x000B4BB4 File Offset: 0x000B2DB4
		public Guid RelatedActivityId
		{
			[CompilerGenerated]
			get
			{
				return this.<RelatedActivityId>k__BackingField;
			}
			[CompilerGenerated]
			internal set
			{
				this.<RelatedActivityId>k__BackingField = value;
			}
		}

		/// <summary>Returns the tags specified in the call to the <see cref="M:System.Diagnostics.Tracing.EventSource.Write(System.String,System.Diagnostics.Tracing.EventSourceOptions)" /> method.</summary>
		/// <returns>Returns <see cref="T:System.Diagnostics.Tracing.EventTags" />.</returns>
		// Token: 0x1700085A RID: 2138
		// (get) Token: 0x06003255 RID: 12885 RVA: 0x00002526 File Offset: 0x00000726
		public EventTags Tags
		{
			get
			{
				return EventTags.None;
			}
		}

		/// <summary>Gets the task for the event.</summary>
		/// <returns>The task for the event.</returns>
		// Token: 0x1700085B RID: 2139
		// (get) Token: 0x06003256 RID: 12886 RVA: 0x00002526 File Offset: 0x00000726
		public EventTask Task
		{
			get
			{
				return EventTask.None;
			}
		}

		/// <summary>Gets the version of the event.</summary>
		/// <returns>The version of the event.</returns>
		// Token: 0x1700085C RID: 2140
		// (get) Token: 0x06003257 RID: 12887 RVA: 0x00002526 File Offset: 0x00000726
		public byte Version
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06003258 RID: 12888 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal EventWrittenEventArgs()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001B25 RID: 6949
		[CompilerGenerated]
		private int <EventId>k__BackingField;

		// Token: 0x04001B26 RID: 6950
		[CompilerGenerated]
		private string <EventName>k__BackingField;

		// Token: 0x04001B27 RID: 6951
		[CompilerGenerated]
		private EventSource <EventSource>k__BackingField;

		// Token: 0x04001B28 RID: 6952
		[CompilerGenerated]
		private string <Message>k__BackingField;

		// Token: 0x04001B29 RID: 6953
		[CompilerGenerated]
		private ReadOnlyCollection<object> <Payload>k__BackingField;

		// Token: 0x04001B2A RID: 6954
		[CompilerGenerated]
		private ReadOnlyCollection<string> <PayloadNames>k__BackingField;

		// Token: 0x04001B2B RID: 6955
		[CompilerGenerated]
		private Guid <RelatedActivityId>k__BackingField;
	}
}
