﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Specifies a property should be ignored when writing an event type with the <see cref="M:System.Diagnostics.Tracing.EventSource.Write``1(System.String,System.Diagnostics.Tracing.EventSourceOptions@,``0@)" /> method.</summary>
	// Token: 0x02000445 RID: 1093
	[AttributeUsage(AttributeTargets.Property)]
	public class EventIgnoreAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Tracing.EventIgnoreAttribute" /> class.</summary>
		// Token: 0x060031ED RID: 12781 RVA: 0x000020BF File Offset: 0x000002BF
		public EventIgnoreAttribute()
		{
		}
	}
}
