﻿using System;
using System.Runtime.CompilerServices;

namespace System.Diagnostics.Tracing
{
	/// <summary>Specifies additional event schema information for an event.</summary>
	// Token: 0x0200043D RID: 1085
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class EventAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Tracing.EventAttribute" /> class with the specified event identifier.</summary>
		/// <param name="eventId">The event identifier for the event.</param>
		// Token: 0x060031C9 RID: 12745 RVA: 0x000B479B File Offset: 0x000B299B
		public EventAttribute(int eventId)
		{
			this.EventId = eventId;
		}

		/// <summary>Gets or sets the identifier for the event.</summary>
		/// <returns>The event identifier. This value should be between 0 and 65535.</returns>
		// Token: 0x17000835 RID: 2101
		// (get) Token: 0x060031CA RID: 12746 RVA: 0x000B47AA File Offset: 0x000B29AA
		// (set) Token: 0x060031CB RID: 12747 RVA: 0x000B47B2 File Offset: 0x000B29B2
		public int EventId
		{
			[CompilerGenerated]
			get
			{
				return this.<EventId>k__BackingField;
			}
			[CompilerGenerated]
			private set
			{
				this.<EventId>k__BackingField = value;
			}
		}

		/// <summary>Specifies the behavior of the start and stop events of an activity. An activity is the region of time in an app between the start and the stop.</summary>
		/// <returns>Returns <see cref="T:System.Diagnostics.Tracing.EventActivityOptions" />.</returns>
		// Token: 0x17000836 RID: 2102
		// (get) Token: 0x060031CC RID: 12748 RVA: 0x000B47BB File Offset: 0x000B29BB
		// (set) Token: 0x060031CD RID: 12749 RVA: 0x000B47C3 File Offset: 0x000B29C3
		public EventActivityOptions ActivityOptions
		{
			[CompilerGenerated]
			get
			{
				return this.<ActivityOptions>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ActivityOptions>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the level for the event.</summary>
		/// <returns>One of the enumeration values that specifies the level for the event.</returns>
		// Token: 0x17000837 RID: 2103
		// (get) Token: 0x060031CE RID: 12750 RVA: 0x000B47CC File Offset: 0x000B29CC
		// (set) Token: 0x060031CF RID: 12751 RVA: 0x000B47D4 File Offset: 0x000B29D4
		public EventLevel Level
		{
			[CompilerGenerated]
			get
			{
				return this.<Level>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Level>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the keywords for the event.</summary>
		/// <returns>A bitwise combination of the enumeration values.</returns>
		// Token: 0x17000838 RID: 2104
		// (get) Token: 0x060031D0 RID: 12752 RVA: 0x000B47DD File Offset: 0x000B29DD
		// (set) Token: 0x060031D1 RID: 12753 RVA: 0x000B47E5 File Offset: 0x000B29E5
		public EventKeywords Keywords
		{
			[CompilerGenerated]
			get
			{
				return this.<Keywords>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Keywords>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the operation code for the event.</summary>
		/// <returns>One of the enumeration values that specifies the operation code.</returns>
		// Token: 0x17000839 RID: 2105
		// (get) Token: 0x060031D2 RID: 12754 RVA: 0x000B47EE File Offset: 0x000B29EE
		// (set) Token: 0x060031D3 RID: 12755 RVA: 0x000B47F6 File Offset: 0x000B29F6
		public EventOpcode Opcode
		{
			[CompilerGenerated]
			get
			{
				return this.<Opcode>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Opcode>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets an additional event log where the event should be written.</summary>
		/// <returns>An additional event log where the event should be written.</returns>
		// Token: 0x1700083A RID: 2106
		// (get) Token: 0x060031D4 RID: 12756 RVA: 0x000B47FF File Offset: 0x000B29FF
		// (set) Token: 0x060031D5 RID: 12757 RVA: 0x000B4807 File Offset: 0x000B2A07
		public EventChannel Channel
		{
			[CompilerGenerated]
			get
			{
				return this.<Channel>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Channel>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the message for the event.</summary>
		/// <returns>The message for the event.</returns>
		// Token: 0x1700083B RID: 2107
		// (get) Token: 0x060031D6 RID: 12758 RVA: 0x000B4810 File Offset: 0x000B2A10
		// (set) Token: 0x060031D7 RID: 12759 RVA: 0x000B4818 File Offset: 0x000B2A18
		public string Message
		{
			[CompilerGenerated]
			get
			{
				return this.<Message>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Message>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the task for the event.</summary>
		/// <returns>The task for the event.</returns>
		// Token: 0x1700083C RID: 2108
		// (get) Token: 0x060031D8 RID: 12760 RVA: 0x000B4821 File Offset: 0x000B2A21
		// (set) Token: 0x060031D9 RID: 12761 RVA: 0x000B4829 File Offset: 0x000B2A29
		public EventTask Task
		{
			[CompilerGenerated]
			get
			{
				return this.<Task>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Task>k__BackingField = value;
			}
		}

		/// <summary>Gets and sets the <see cref="T:System.Diagnostics.Tracing.EventTags" /> value for this <see cref="T:System.Diagnostics.Tracing.EventAttribute" /> object. An event tag is a user-defined value that is passed through when the event is logged. </summary>
		/// <returns>Returns the <see cref="T:System.Diagnostics.Tracing.EventTags" /> value.</returns>
		// Token: 0x1700083D RID: 2109
		// (get) Token: 0x060031DA RID: 12762 RVA: 0x000B4832 File Offset: 0x000B2A32
		// (set) Token: 0x060031DB RID: 12763 RVA: 0x000B483A File Offset: 0x000B2A3A
		public EventTags Tags
		{
			[CompilerGenerated]
			get
			{
				return this.<Tags>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Tags>k__BackingField = value;
			}
		}

		/// <summary>Gets or sets the version of the event.</summary>
		/// <returns>The version of the event.</returns>
		// Token: 0x1700083E RID: 2110
		// (get) Token: 0x060031DC RID: 12764 RVA: 0x000B4843 File Offset: 0x000B2A43
		// (set) Token: 0x060031DD RID: 12765 RVA: 0x000B484B File Offset: 0x000B2A4B
		public byte Version
		{
			[CompilerGenerated]
			get
			{
				return this.<Version>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Version>k__BackingField = value;
			}
		}

		// Token: 0x04001AFA RID: 6906
		[CompilerGenerated]
		private int <EventId>k__BackingField;

		// Token: 0x04001AFB RID: 6907
		[CompilerGenerated]
		private EventActivityOptions <ActivityOptions>k__BackingField;

		// Token: 0x04001AFC RID: 6908
		[CompilerGenerated]
		private EventLevel <Level>k__BackingField;

		// Token: 0x04001AFD RID: 6909
		[CompilerGenerated]
		private EventKeywords <Keywords>k__BackingField;

		// Token: 0x04001AFE RID: 6910
		[CompilerGenerated]
		private EventOpcode <Opcode>k__BackingField;

		// Token: 0x04001AFF RID: 6911
		[CompilerGenerated]
		private EventChannel <Channel>k__BackingField;

		// Token: 0x04001B00 RID: 6912
		[CompilerGenerated]
		private string <Message>k__BackingField;

		// Token: 0x04001B01 RID: 6913
		[CompilerGenerated]
		private EventTask <Task>k__BackingField;

		// Token: 0x04001B02 RID: 6914
		[CompilerGenerated]
		private EventTags <Tags>k__BackingField;

		// Token: 0x04001B03 RID: 6915
		[CompilerGenerated]
		private byte <Version>k__BackingField;
	}
}
