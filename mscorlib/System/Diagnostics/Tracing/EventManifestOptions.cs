﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Specifies how the ETW manifest for the event source is generated.</summary>
	// Token: 0x02000447 RID: 1095
	[Flags]
	public enum EventManifestOptions
	{
		/// <summary>Generates a resources node under the localization folder for every satellite assembly provided.</summary>
		// Token: 0x04001B14 RID: 6932
		AllCultures = 2,
		/// <summary>Overrides the default behavior that the current <see cref="T:System.Diagnostics.Tracing.EventSource" /> must be the base class of the user-defined type passed to the write method. This enables the validation of .NET event sources.</summary>
		// Token: 0x04001B15 RID: 6933
		AllowEventSourceOverride = 8,
		/// <summary>No options are specified.</summary>
		// Token: 0x04001B16 RID: 6934
		None = 0,
		/// <summary>A manifest is generated only the event source must be registered on the host computer.</summary>
		// Token: 0x04001B17 RID: 6935
		OnlyIfNeededForRegistration = 4,
		/// <summary>Causes an exception to be raised if any inconsistencies occur when writing the manifest file.</summary>
		// Token: 0x04001B18 RID: 6936
		Strict = 1
	}
}
