﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Specifies overrides of default event settings such as the log level, keywords and operation code when the <see cref="M:System.Diagnostics.Tracing.EventSource.Write``1(System.String,System.Diagnostics.Tracing.EventSourceOptions,``0)" /> method is called.</summary>
	// Token: 0x02000434 RID: 1076
	public struct EventSourceOptions
	{
		/// <summary>Gets or sets the event level applied to the event. </summary>
		/// <returns>The event level for the event. If not set, the default is Verbose (5).</returns>
		// Token: 0x1700082F RID: 2095
		// (get) Token: 0x060031B9 RID: 12729 RVA: 0x000B46C6 File Offset: 0x000B28C6
		// (set) Token: 0x060031BA RID: 12730 RVA: 0x000B46CE File Offset: 0x000B28CE
		public EventLevel Level
		{
			get
			{
				return (EventLevel)this.level;
			}
			set
			{
				this.level = checked((byte)value);
				this.valuesSet |= 4;
			}
		}

		/// <summary>Gets or sets the operation code to use for the specified event. </summary>
		/// <returns>The operation code to use for the specified event. If not set, the default is <see langword="Info" /> (0).</returns>
		// Token: 0x17000830 RID: 2096
		// (get) Token: 0x060031BB RID: 12731 RVA: 0x000B46E7 File Offset: 0x000B28E7
		// (set) Token: 0x060031BC RID: 12732 RVA: 0x000B46EF File Offset: 0x000B28EF
		public EventOpcode Opcode
		{
			get
			{
				return (EventOpcode)this.opcode;
			}
			set
			{
				this.opcode = checked((byte)value);
				this.valuesSet |= 8;
			}
		}

		// Token: 0x17000831 RID: 2097
		// (get) Token: 0x060031BD RID: 12733 RVA: 0x000B4708 File Offset: 0x000B2908
		internal bool IsOpcodeSet
		{
			get
			{
				return (this.valuesSet & 8) > 0;
			}
		}

		/// <summary>Gets or sets the keywords applied to the event. If this property is not set, the event’s keywords will be <see langword="None" />.</summary>
		/// <returns>The keywords applied to the event, or <see langword="None" /> if no keywords are set.</returns>
		// Token: 0x17000832 RID: 2098
		// (get) Token: 0x060031BE RID: 12734 RVA: 0x000B4715 File Offset: 0x000B2915
		// (set) Token: 0x060031BF RID: 12735 RVA: 0x000B471D File Offset: 0x000B291D
		public EventKeywords Keywords
		{
			get
			{
				return this.keywords;
			}
			set
			{
				this.keywords = value;
				this.valuesSet |= 1;
			}
		}

		/// <summary>The event tags defined for this event source.</summary>
		/// <returns>Returns <see cref="T:System.Diagnostics.Tracing.EventTags" />.</returns>
		// Token: 0x17000833 RID: 2099
		// (get) Token: 0x060031C0 RID: 12736 RVA: 0x000B4735 File Offset: 0x000B2935
		// (set) Token: 0x060031C1 RID: 12737 RVA: 0x000B473D File Offset: 0x000B293D
		public EventTags Tags
		{
			get
			{
				return this.tags;
			}
			set
			{
				this.tags = value;
				this.valuesSet |= 2;
			}
		}

		/// <summary>The activity options defined for this event source.</summary>
		/// <returns>Returns <see cref="T:System.Diagnostics.Tracing.EventActivityOptions" />.</returns>
		// Token: 0x17000834 RID: 2100
		// (get) Token: 0x060031C2 RID: 12738 RVA: 0x000B4755 File Offset: 0x000B2955
		// (set) Token: 0x060031C3 RID: 12739 RVA: 0x000B475D File Offset: 0x000B295D
		public EventActivityOptions ActivityOptions
		{
			get
			{
				return this.activityOptions;
			}
			set
			{
				this.activityOptions = value;
				this.valuesSet |= 16;
			}
		}

		// Token: 0x04001AC2 RID: 6850
		internal EventKeywords keywords;

		// Token: 0x04001AC3 RID: 6851
		internal EventTags tags;

		// Token: 0x04001AC4 RID: 6852
		internal EventActivityOptions activityOptions;

		// Token: 0x04001AC5 RID: 6853
		internal byte level;

		// Token: 0x04001AC6 RID: 6854
		internal byte opcode;

		// Token: 0x04001AC7 RID: 6855
		internal byte valuesSet;

		// Token: 0x04001AC8 RID: 6856
		internal const byte keywordsSet = 1;

		// Token: 0x04001AC9 RID: 6857
		internal const byte tagsSet = 2;

		// Token: 0x04001ACA RID: 6858
		internal const byte levelSet = 4;

		// Token: 0x04001ACB RID: 6859
		internal const byte opcodeSet = 8;

		// Token: 0x04001ACC RID: 6860
		internal const byte activityOptionsSet = 16;
	}
}
