﻿using System;
using System.Collections.Generic;

namespace System.Diagnostics.Tracing
{
	/// <summary>Provides the arguments for the <see cref="M:System.Diagnostics.Tracing.EventSource.OnEventCommand(System.Diagnostics.Tracing.EventCommandEventArgs)" /> callback.</summary>
	// Token: 0x0200043F RID: 1087
	public class EventCommandEventArgs : EventArgs
	{
		// Token: 0x060031DE RID: 12766 RVA: 0x000B4854 File Offset: 0x000B2A54
		private EventCommandEventArgs()
		{
		}

		/// <summary>Gets the array of arguments for the callback.</summary>
		/// <returns>An array of callback arguments.</returns>
		// Token: 0x1700083F RID: 2111
		// (get) Token: 0x060031DF RID: 12767 RVA: 0x000041F3 File Offset: 0x000023F3
		public IDictionary<string, string> Arguments
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the command for the callback.</summary>
		/// <returns>The callback command.</returns>
		// Token: 0x17000840 RID: 2112
		// (get) Token: 0x060031E0 RID: 12768 RVA: 0x000041F3 File Offset: 0x000023F3
		public EventCommand Command
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Disables the event that have the specified identifier.</summary>
		/// <param name="eventId">The identifier of the event to disable.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="eventId" /> is in range; otherwise, <see langword="false" />.</returns>
		// Token: 0x060031E1 RID: 12769 RVA: 0x00004E08 File Offset: 0x00003008
		public bool DisableEvent(int eventId)
		{
			return true;
		}

		/// <summary>Enables the event that has the specified identifier.</summary>
		/// <param name="eventId">The identifier of the event to enable.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="eventId" /> is in range; otherwise, <see langword="false" />.</returns>
		// Token: 0x060031E2 RID: 12770 RVA: 0x00004E08 File Offset: 0x00003008
		public bool EnableEvent(int eventId)
		{
			return true;
		}
	}
}
