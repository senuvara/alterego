﻿using System;
using System.Runtime.CompilerServices;

namespace System.Diagnostics.Tracing
{
	/// <summary>Specifies the event log channel for the event.</summary>
	// Token: 0x0200043B RID: 1083
	[FriendAccessAllowed]
	public enum EventChannel : byte
	{
		/// <summary>No channel specified.</summary>
		// Token: 0x04001AEA RID: 6890
		None,
		/// <summary>The administrator log channel.</summary>
		// Token: 0x04001AEB RID: 6891
		Admin = 16,
		/// <summary>The operational channel. </summary>
		// Token: 0x04001AEC RID: 6892
		Operational,
		/// <summary>The analytic channel.</summary>
		// Token: 0x04001AED RID: 6893
		Analytic,
		/// <summary>The debug channel.</summary>
		// Token: 0x04001AEE RID: 6894
		Debug
	}
}
