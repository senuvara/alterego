﻿using System;

namespace System.Diagnostics.Tracing
{
	/// <summary>Specifies how to format the value of a user-defined type and can be used to override the default formatting for a field.</summary>
	// Token: 0x02000443 RID: 1091
	public enum EventFieldFormat
	{
		/// <summary>Boolean</summary>
		// Token: 0x04001B0A RID: 6922
		Boolean = 3,
		/// <summary>Default.</summary>
		// Token: 0x04001B0B RID: 6923
		Default = 0,
		/// <summary>Hexadecimal.</summary>
		// Token: 0x04001B0C RID: 6924
		Hexadecimal = 4,
		/// <summary>HResult.</summary>
		// Token: 0x04001B0D RID: 6925
		HResult = 15,
		/// <summary>JSON.</summary>
		// Token: 0x04001B0E RID: 6926
		Json = 12,
		/// <summary>String.</summary>
		// Token: 0x04001B0F RID: 6927
		String = 2,
		/// <summary>XML.</summary>
		// Token: 0x04001B10 RID: 6928
		Xml = 11
	}
}
