﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	/// <summary>Specifies that the type has a visualizer. This class cannot be inherited. </summary>
	// Token: 0x02000420 RID: 1056
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
	public sealed class DebuggerVisualizerAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerVisualizerAttribute" /> class, specifying the type name of the visualizer.</summary>
		/// <param name="visualizerTypeName">The fully qualified type name of the visualizer.</param>
		// Token: 0x06003128 RID: 12584 RVA: 0x000B3BD7 File Offset: 0x000B1DD7
		public DebuggerVisualizerAttribute(string visualizerTypeName)
		{
			this.visualizerName = visualizerTypeName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerVisualizerAttribute" /> class, specifying the type name of the visualizer and the type name of the visualizer object source.</summary>
		/// <param name="visualizerTypeName">The fully qualified type name of the visualizer.</param>
		/// <param name="visualizerObjectSourceTypeName">The fully qualified type name of the visualizer object source.</param>
		// Token: 0x06003129 RID: 12585 RVA: 0x000B3BE6 File Offset: 0x000B1DE6
		public DebuggerVisualizerAttribute(string visualizerTypeName, string visualizerObjectSourceTypeName)
		{
			this.visualizerName = visualizerTypeName;
			this.visualizerObjectSourceName = visualizerObjectSourceTypeName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerVisualizerAttribute" /> class, specifying the type name of the visualizer and the type of the visualizer object source.</summary>
		/// <param name="visualizerTypeName">The fully qualified type name of the visualizer.</param>
		/// <param name="visualizerObjectSource">The type of the visualizer object source.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="visualizerObjectSource" /> is <see langword="null" />.</exception>
		// Token: 0x0600312A RID: 12586 RVA: 0x000B3BFC File Offset: 0x000B1DFC
		public DebuggerVisualizerAttribute(string visualizerTypeName, Type visualizerObjectSource)
		{
			if (visualizerObjectSource == null)
			{
				throw new ArgumentNullException("visualizerObjectSource");
			}
			this.visualizerName = visualizerTypeName;
			this.visualizerObjectSourceName = visualizerObjectSource.AssemblyQualifiedName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerVisualizerAttribute" /> class, specifying the type of the visualizer.</summary>
		/// <param name="visualizer">The type of the visualizer.</param>
		/// <exception cref="T:System.ArgumentNullException">v<paramref name="isualizer" /> is <see langword="null" />.</exception>
		// Token: 0x0600312B RID: 12587 RVA: 0x000B3C2B File Offset: 0x000B1E2B
		public DebuggerVisualizerAttribute(Type visualizer)
		{
			if (visualizer == null)
			{
				throw new ArgumentNullException("visualizer");
			}
			this.visualizerName = visualizer.AssemblyQualifiedName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerVisualizerAttribute" /> class, specifying the type of the visualizer and the type of the visualizer object source.</summary>
		/// <param name="visualizer">The type of the visualizer.</param>
		/// <param name="visualizerObjectSource">The type of the visualizer object source.</param>
		/// <exception cref="T:System.ArgumentNullException">v<paramref name="isualizer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="visualizerObjectSource" /> is <see langword="null" />.</exception>
		// Token: 0x0600312C RID: 12588 RVA: 0x000B3C54 File Offset: 0x000B1E54
		public DebuggerVisualizerAttribute(Type visualizer, Type visualizerObjectSource)
		{
			if (visualizer == null)
			{
				throw new ArgumentNullException("visualizer");
			}
			if (visualizerObjectSource == null)
			{
				throw new ArgumentNullException("visualizerObjectSource");
			}
			this.visualizerName = visualizer.AssemblyQualifiedName;
			this.visualizerObjectSourceName = visualizerObjectSource.AssemblyQualifiedName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerVisualizerAttribute" /> class, specifying the type of the visualizer and the type name of the visualizer object source.</summary>
		/// <param name="visualizer">The type of the visualizer.</param>
		/// <param name="visualizerObjectSourceTypeName">The fully qualified type name of the visualizer object source.</param>
		/// <exception cref="T:System.ArgumentNullException">v<paramref name="isualizer" /> is <see langword="null" />.</exception>
		// Token: 0x0600312D RID: 12589 RVA: 0x000B3CA7 File Offset: 0x000B1EA7
		public DebuggerVisualizerAttribute(Type visualizer, string visualizerObjectSourceTypeName)
		{
			if (visualizer == null)
			{
				throw new ArgumentNullException("visualizer");
			}
			this.visualizerName = visualizer.AssemblyQualifiedName;
			this.visualizerObjectSourceName = visualizerObjectSourceTypeName;
		}

		/// <summary>Gets the fully qualified type name of the visualizer object source.</summary>
		/// <returns>The fully qualified type name of the visualizer object source.</returns>
		// Token: 0x17000810 RID: 2064
		// (get) Token: 0x0600312E RID: 12590 RVA: 0x000B3CD6 File Offset: 0x000B1ED6
		public string VisualizerObjectSourceTypeName
		{
			get
			{
				return this.visualizerObjectSourceName;
			}
		}

		/// <summary>Gets the fully qualified type name of the visualizer.</summary>
		/// <returns>The fully qualified visualizer type name.</returns>
		// Token: 0x17000811 RID: 2065
		// (get) Token: 0x0600312F RID: 12591 RVA: 0x000B3CDE File Offset: 0x000B1EDE
		public string VisualizerTypeName
		{
			get
			{
				return this.visualizerName;
			}
		}

		/// <summary>Gets or sets the description of the visualizer.</summary>
		/// <returns>The description of the visualizer.</returns>
		// Token: 0x17000812 RID: 2066
		// (get) Token: 0x06003130 RID: 12592 RVA: 0x000B3CE6 File Offset: 0x000B1EE6
		// (set) Token: 0x06003131 RID: 12593 RVA: 0x000B3CEE File Offset: 0x000B1EEE
		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		/// <summary>Gets or sets the target type when the attribute is applied at the assembly level.</summary>
		/// <returns>The type that is the target of the visualizer.</returns>
		/// <exception cref="T:System.ArgumentNullException">The value cannot be set because it is <see langword="null" />.</exception>
		// Token: 0x17000813 RID: 2067
		// (get) Token: 0x06003133 RID: 12595 RVA: 0x000B3D20 File Offset: 0x000B1F20
		// (set) Token: 0x06003132 RID: 12594 RVA: 0x000B3CF7 File Offset: 0x000B1EF7
		public Type Target
		{
			get
			{
				return this.target;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.targetName = value.AssemblyQualifiedName;
				this.target = value;
			}
		}

		/// <summary>Gets or sets the fully qualified type name when the attribute is applied at the assembly level.</summary>
		/// <returns>The fully qualified type name of the target type.</returns>
		// Token: 0x17000814 RID: 2068
		// (get) Token: 0x06003135 RID: 12597 RVA: 0x000B3D31 File Offset: 0x000B1F31
		// (set) Token: 0x06003134 RID: 12596 RVA: 0x000B3D28 File Offset: 0x000B1F28
		public string TargetTypeName
		{
			get
			{
				return this.targetName;
			}
			set
			{
				this.targetName = value;
			}
		}

		// Token: 0x04001A8F RID: 6799
		private string visualizerObjectSourceName;

		// Token: 0x04001A90 RID: 6800
		private string visualizerName;

		// Token: 0x04001A91 RID: 6801
		private string description;

		// Token: 0x04001A92 RID: 6802
		private string targetName;

		// Token: 0x04001A93 RID: 6803
		private Type target;
	}
}
