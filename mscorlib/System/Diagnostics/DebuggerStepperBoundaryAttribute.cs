﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	/// <summary>Indicates the code following the attribute is to be executed in run, not step, mode.</summary>
	// Token: 0x02000417 RID: 1047
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, Inherited = false)]
	[Serializable]
	public sealed class DebuggerStepperBoundaryAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerStepperBoundaryAttribute" /> class. </summary>
		// Token: 0x0600310D RID: 12557 RVA: 0x000020BF File Offset: 0x000002BF
		public DebuggerStepperBoundaryAttribute()
		{
		}
	}
}
