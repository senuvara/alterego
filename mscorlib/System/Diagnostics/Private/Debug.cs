﻿using System;
using Internal.Runtime.Augments;

namespace System.Diagnostics.Private
{
	// Token: 0x0200045F RID: 1119
	internal static class Debug
	{
		// Token: 0x17000875 RID: 2165
		// (get) Token: 0x060032A6 RID: 12966 RVA: 0x00004E08 File Offset: 0x00003008
		// (set) Token: 0x060032A7 RID: 12967 RVA: 0x000020D3 File Offset: 0x000002D3
		public static bool AutoFlush
		{
			get
			{
				return true;
			}
			set
			{
			}
		}

		// Token: 0x17000876 RID: 2166
		// (get) Token: 0x060032A8 RID: 12968 RVA: 0x000B5187 File Offset: 0x000B3387
		// (set) Token: 0x060032A9 RID: 12969 RVA: 0x000B518E File Offset: 0x000B338E
		public static int IndentLevel
		{
			get
			{
				return Debug.s_indentLevel;
			}
			set
			{
				Debug.s_indentLevel = ((value < 0) ? 0 : value);
			}
		}

		// Token: 0x17000877 RID: 2167
		// (get) Token: 0x060032AA RID: 12970 RVA: 0x000B519D File Offset: 0x000B339D
		// (set) Token: 0x060032AB RID: 12971 RVA: 0x000B51A4 File Offset: 0x000B33A4
		public static int IndentSize
		{
			get
			{
				return Debug.s_indentSize;
			}
			set
			{
				Debug.s_indentSize = ((value < 0) ? 0 : value);
			}
		}

		// Token: 0x060032AC RID: 12972 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("DEBUG")]
		public static void Close()
		{
		}

		// Token: 0x060032AD RID: 12973 RVA: 0x000020D3 File Offset: 0x000002D3
		[Conditional("DEBUG")]
		public static void Flush()
		{
		}

		// Token: 0x060032AE RID: 12974 RVA: 0x000B51B3 File Offset: 0x000B33B3
		[Conditional("DEBUG")]
		public static void Indent()
		{
			Debug.IndentLevel++;
		}

		// Token: 0x060032AF RID: 12975 RVA: 0x000B51C1 File Offset: 0x000B33C1
		[Conditional("DEBUG")]
		public static void Unindent()
		{
			Debug.IndentLevel--;
		}

		// Token: 0x060032B0 RID: 12976 RVA: 0x000B51CF File Offset: 0x000B33CF
		[Conditional("DEBUG")]
		public static void Print(string message)
		{
			Debug.Write(message);
		}

		// Token: 0x060032B1 RID: 12977 RVA: 0x000B51D7 File Offset: 0x000B33D7
		[Conditional("DEBUG")]
		public static void Print(string format, params object[] args)
		{
			Debug.Write(string.Format(null, format, args));
		}

		// Token: 0x060032B2 RID: 12978 RVA: 0x000B51E6 File Offset: 0x000B33E6
		[Conditional("DEBUG")]
		public static void Assert(bool condition)
		{
			Debug.Assert(condition, string.Empty, string.Empty);
		}

		// Token: 0x060032B3 RID: 12979 RVA: 0x000B51F8 File Offset: 0x000B33F8
		[Conditional("DEBUG")]
		public static void Assert(bool condition, string message)
		{
			Debug.Assert(condition, message, string.Empty);
		}

		// Token: 0x060032B4 RID: 12980 RVA: 0x000B5208 File Offset: 0x000B3408
		[Conditional("DEBUG")]
		public static void Assert(bool condition, string message, string detailMessage)
		{
			if (!condition)
			{
				string text;
				try
				{
					text = EnvironmentAugments.StackTrace;
				}
				catch
				{
					text = "";
				}
				Debug.WriteLine(Debug.FormatAssert(text, message, detailMessage));
				Debug.s_ShowAssertDialog(text, message, detailMessage);
			}
		}

		// Token: 0x060032B5 RID: 12981 RVA: 0x000B5254 File Offset: 0x000B3454
		[Conditional("DEBUG")]
		public static void Fail(string message)
		{
			Debug.Assert(false, message, string.Empty);
		}

		// Token: 0x060032B6 RID: 12982 RVA: 0x000B5262 File Offset: 0x000B3462
		[Conditional("DEBUG")]
		public static void Fail(string message, string detailMessage)
		{
			Debug.Assert(false, message, detailMessage);
		}

		// Token: 0x060032B7 RID: 12983 RVA: 0x000B526C File Offset: 0x000B346C
		private static string FormatAssert(string stackTrace, string message, string detailMessage)
		{
			string text = Debug.GetIndentString() + Environment.NewLine;
			return string.Concat(new string[]
			{
				"---- DEBUG ASSERTION FAILED ----",
				text,
				"---- Assert Short Message ----",
				text,
				message,
				text,
				"---- Assert Long Message ----",
				text,
				detailMessage,
				text,
				stackTrace
			});
		}

		// Token: 0x060032B8 RID: 12984 RVA: 0x000B52CF File Offset: 0x000B34CF
		[Conditional("DEBUG")]
		public static void Assert(bool condition, string message, string detailMessageFormat, params object[] args)
		{
			Debug.Assert(condition, message, string.Format(detailMessageFormat, args));
		}

		// Token: 0x060032B9 RID: 12985 RVA: 0x000B52DF File Offset: 0x000B34DF
		[Conditional("DEBUG")]
		public static void WriteLine(string message)
		{
			Debug.Write(message + Environment.NewLine);
		}

		// Token: 0x060032BA RID: 12986 RVA: 0x000B52F4 File Offset: 0x000B34F4
		[Conditional("DEBUG")]
		public static void Write(string message)
		{
			object obj = Debug.s_lock;
			lock (obj)
			{
				if (message == null)
				{
					Debug.s_WriteCore(string.Empty);
				}
				else
				{
					if (Debug.s_needIndent)
					{
						message = Debug.GetIndentString() + message;
						Debug.s_needIndent = false;
					}
					Debug.s_WriteCore(message);
					if (message.EndsWith(Environment.NewLine))
					{
						Debug.s_needIndent = true;
					}
				}
			}
		}

		// Token: 0x060032BB RID: 12987 RVA: 0x000B537C File Offset: 0x000B357C
		[Conditional("DEBUG")]
		public static void WriteLine(object value)
		{
			Debug.WriteLine((value != null) ? value.ToString() : null);
		}

		// Token: 0x060032BC RID: 12988 RVA: 0x000B538F File Offset: 0x000B358F
		[Conditional("DEBUG")]
		public static void WriteLine(object value, string category)
		{
			Debug.WriteLine((value != null) ? value.ToString() : null, category);
		}

		// Token: 0x060032BD RID: 12989 RVA: 0x000B53A3 File Offset: 0x000B35A3
		[Conditional("DEBUG")]
		public static void WriteLine(string format, params object[] args)
		{
			Debug.WriteLine(string.Format(null, format, args));
		}

		// Token: 0x060032BE RID: 12990 RVA: 0x000B53B2 File Offset: 0x000B35B2
		[Conditional("DEBUG")]
		public static void WriteLine(string message, string category)
		{
			if (category == null)
			{
				Debug.WriteLine(message);
				return;
			}
			Debug.WriteLine(category + ":" + message);
		}

		// Token: 0x060032BF RID: 12991 RVA: 0x000B53CF File Offset: 0x000B35CF
		[Conditional("DEBUG")]
		public static void Write(object value)
		{
			Debug.Write((value != null) ? value.ToString() : null);
		}

		// Token: 0x060032C0 RID: 12992 RVA: 0x000B53E2 File Offset: 0x000B35E2
		[Conditional("DEBUG")]
		public static void Write(string message, string category)
		{
			if (category == null)
			{
				Debug.Write(message);
				return;
			}
			Debug.Write(category + ":" + message);
		}

		// Token: 0x060032C1 RID: 12993 RVA: 0x000B53FF File Offset: 0x000B35FF
		[Conditional("DEBUG")]
		public static void Write(object value, string category)
		{
			Debug.Write((value != null) ? value.ToString() : null, category);
		}

		// Token: 0x060032C2 RID: 12994 RVA: 0x000B5413 File Offset: 0x000B3613
		[Conditional("DEBUG")]
		public static void WriteIf(bool condition, string message)
		{
			if (condition)
			{
				Debug.Write(message);
			}
		}

		// Token: 0x060032C3 RID: 12995 RVA: 0x000B541E File Offset: 0x000B361E
		[Conditional("DEBUG")]
		public static void WriteIf(bool condition, object value)
		{
			if (condition)
			{
				Debug.Write(value);
			}
		}

		// Token: 0x060032C4 RID: 12996 RVA: 0x000B5429 File Offset: 0x000B3629
		[Conditional("DEBUG")]
		public static void WriteIf(bool condition, string message, string category)
		{
			if (condition)
			{
				Debug.Write(message, category);
			}
		}

		// Token: 0x060032C5 RID: 12997 RVA: 0x000B5435 File Offset: 0x000B3635
		[Conditional("DEBUG")]
		public static void WriteIf(bool condition, object value, string category)
		{
			if (condition)
			{
				Debug.Write(value, category);
			}
		}

		// Token: 0x060032C6 RID: 12998 RVA: 0x000B5441 File Offset: 0x000B3641
		[Conditional("DEBUG")]
		public static void WriteLineIf(bool condition, object value)
		{
			if (condition)
			{
				Debug.WriteLine(value);
			}
		}

		// Token: 0x060032C7 RID: 12999 RVA: 0x000B544C File Offset: 0x000B364C
		[Conditional("DEBUG")]
		public static void WriteLineIf(bool condition, object value, string category)
		{
			if (condition)
			{
				Debug.WriteLine(value, category);
			}
		}

		// Token: 0x060032C8 RID: 13000 RVA: 0x000B5458 File Offset: 0x000B3658
		[Conditional("DEBUG")]
		public static void WriteLineIf(bool condition, string message)
		{
			if (condition)
			{
				Debug.WriteLine(message);
			}
		}

		// Token: 0x060032C9 RID: 13001 RVA: 0x000B5463 File Offset: 0x000B3663
		[Conditional("DEBUG")]
		public static void WriteLineIf(bool condition, string message, string category)
		{
			if (condition)
			{
				Debug.WriteLine(message, category);
			}
		}

		// Token: 0x060032CA RID: 13002 RVA: 0x000B5470 File Offset: 0x000B3670
		private static string GetIndentString()
		{
			int num = Debug.IndentSize * Debug.IndentLevel;
			string text = Debug.s_indentString;
			if (text != null && text.Length == num)
			{
				return Debug.s_indentString;
			}
			return Debug.s_indentString = new string(' ', num);
		}

		// Token: 0x060032CB RID: 13003 RVA: 0x000020D3 File Offset: 0x000002D3
		private static void ShowAssertDialog(string stackTrace, string message, string detailMessage)
		{
		}

		// Token: 0x060032CC RID: 13004 RVA: 0x000020D3 File Offset: 0x000002D3
		private static void WriteCore(string message)
		{
		}

		// Token: 0x060032CD RID: 13005 RVA: 0x000B54B3 File Offset: 0x000B36B3
		// Note: this type is marked as 'beforefieldinit'.
		static Debug()
		{
		}

		// Token: 0x04001B4C RID: 6988
		private static readonly object s_lock = new object();

		// Token: 0x04001B4D RID: 6989
		[ThreadStatic]
		private static int s_indentLevel;

		// Token: 0x04001B4E RID: 6990
		private static int s_indentSize = 4;

		// Token: 0x04001B4F RID: 6991
		private static bool s_needIndent;

		// Token: 0x04001B50 RID: 6992
		private static string s_indentString;

		// Token: 0x04001B51 RID: 6993
		internal static Action<string, string, string> s_ShowAssertDialog = new Action<string, string, string>(Debug.ShowAssertDialog);

		// Token: 0x04001B52 RID: 6994
		internal static Action<string> s_WriteCore = new Action<string>(Debug.WriteCore);

		// Token: 0x02000460 RID: 1120
		private sealed class DebugAssertException : Exception
		{
			// Token: 0x060032CE RID: 13006 RVA: 0x000B54E7 File Offset: 0x000B36E7
			internal DebugAssertException(string message, string detailMessage, string stackTrace) : base(string.Concat(new string[]
			{
				message,
				Environment.NewLine,
				detailMessage,
				Environment.NewLine,
				stackTrace
			}))
			{
			}
		}
	}
}
