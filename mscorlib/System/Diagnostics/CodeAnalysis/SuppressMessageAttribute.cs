﻿using System;

namespace System.Diagnostics.CodeAnalysis
{
	/// <summary>Suppresses reporting of a specific static analysis tool rule violation, allowing multiple suppressions on a single code artifact.</summary>
	// Token: 0x0200045E RID: 1118
	[Conditional("CODE_ANALYSIS")]
	[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
	public sealed class SuppressMessageAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.CodeAnalysis.SuppressMessageAttribute" /> class, specifying the category of the static analysis tool and the identifier for an analysis rule. </summary>
		/// <param name="category">The category for the attribute.</param>
		/// <param name="checkId">The identifier of the analysis tool rule the attribute applies to.</param>
		// Token: 0x0600329B RID: 12955 RVA: 0x000B511D File Offset: 0x000B331D
		public SuppressMessageAttribute(string category, string checkId)
		{
			this.category = category;
			this.checkId = checkId;
		}

		/// <summary>Gets the category identifying the classification of the attribute.</summary>
		/// <returns>The category identifying the attribute.</returns>
		// Token: 0x1700086F RID: 2159
		// (get) Token: 0x0600329C RID: 12956 RVA: 0x000B5133 File Offset: 0x000B3333
		public string Category
		{
			get
			{
				return this.category;
			}
		}

		/// <summary>Gets the identifier of the static analysis tool rule to be suppressed.</summary>
		/// <returns>The identifier of the static analysis tool rule to be suppressed.</returns>
		// Token: 0x17000870 RID: 2160
		// (get) Token: 0x0600329D RID: 12957 RVA: 0x000B513B File Offset: 0x000B333B
		public string CheckId
		{
			get
			{
				return this.checkId;
			}
		}

		/// <summary>Gets or sets the scope of the code that is relevant for the attribute.</summary>
		/// <returns>The scope of the code that is relevant for the attribute.</returns>
		// Token: 0x17000871 RID: 2161
		// (get) Token: 0x0600329E RID: 12958 RVA: 0x000B5143 File Offset: 0x000B3343
		// (set) Token: 0x0600329F RID: 12959 RVA: 0x000B514B File Offset: 0x000B334B
		public string Scope
		{
			get
			{
				return this.scope;
			}
			set
			{
				this.scope = value;
			}
		}

		/// <summary>Gets or sets a fully qualified path that represents the target of the attribute.</summary>
		/// <returns>A fully qualified path that represents the target of the attribute.</returns>
		// Token: 0x17000872 RID: 2162
		// (get) Token: 0x060032A0 RID: 12960 RVA: 0x000B5154 File Offset: 0x000B3354
		// (set) Token: 0x060032A1 RID: 12961 RVA: 0x000B515C File Offset: 0x000B335C
		public string Target
		{
			get
			{
				return this.target;
			}
			set
			{
				this.target = value;
			}
		}

		/// <summary>Gets or sets an optional argument expanding on exclusion criteria.</summary>
		/// <returns>A string containing the expanded exclusion criteria.</returns>
		// Token: 0x17000873 RID: 2163
		// (get) Token: 0x060032A2 RID: 12962 RVA: 0x000B5165 File Offset: 0x000B3365
		// (set) Token: 0x060032A3 RID: 12963 RVA: 0x000B516D File Offset: 0x000B336D
		public string MessageId
		{
			get
			{
				return this.messageId;
			}
			set
			{
				this.messageId = value;
			}
		}

		/// <summary>Gets or sets the justification for suppressing the code analysis message.</summary>
		/// <returns>The justification for suppressing the message.</returns>
		// Token: 0x17000874 RID: 2164
		// (get) Token: 0x060032A4 RID: 12964 RVA: 0x000B5176 File Offset: 0x000B3376
		// (set) Token: 0x060032A5 RID: 12965 RVA: 0x000B517E File Offset: 0x000B337E
		public string Justification
		{
			get
			{
				return this.justification;
			}
			set
			{
				this.justification = value;
			}
		}

		// Token: 0x04001B46 RID: 6982
		private string category;

		// Token: 0x04001B47 RID: 6983
		private string justification;

		// Token: 0x04001B48 RID: 6984
		private string checkId;

		// Token: 0x04001B49 RID: 6985
		private string scope;

		// Token: 0x04001B4A RID: 6986
		private string target;

		// Token: 0x04001B4B RID: 6987
		private string messageId;
	}
}
