﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	/// <summary>Provides display instructions for the debugger.</summary>
	// Token: 0x0200041C RID: 1052
	[ComVisible(true)]
	public enum DebuggerBrowsableState
	{
		/// <summary>Never show the element.</summary>
		// Token: 0x04001A83 RID: 6787
		Never,
		/// <summary>Show the element as collapsed.</summary>
		// Token: 0x04001A84 RID: 6788
		Collapsed = 2,
		/// <summary>Do not display the root element; display the child elements if the element is a collection or array of items.</summary>
		// Token: 0x04001A85 RID: 6789
		RootHidden
	}
}
