﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace System.Diagnostics
{
	/// <summary>Represents a stack trace, which is an ordered collection of one or more stack frames.</summary>
	// Token: 0x02000423 RID: 1059
	[MonoTODO("Serialized objects are not compatible with .NET")]
	[ComVisible(true)]
	[Serializable]
	public class StackTrace
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class from the caller's frame.</summary>
		// Token: 0x06003151 RID: 12625 RVA: 0x000B409F File Offset: 0x000B229F
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackTrace()
		{
			this.init_frames(0, false);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class from the caller's frame, optionally capturing source information.</summary>
		/// <param name="fNeedFileInfo">
		///       <see langword="true" /> to capture the file name, line number, and column number; otherwise, <see langword="false" />. </param>
		// Token: 0x06003152 RID: 12626 RVA: 0x000B40AF File Offset: 0x000B22AF
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackTrace(bool fNeedFileInfo)
		{
			this.init_frames(0, fNeedFileInfo);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class from the caller's frame, skipping the specified number of frames.</summary>
		/// <param name="skipFrames">The number of frames up the stack from which to start the trace. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="skipFrames" /> parameter is negative. </exception>
		// Token: 0x06003153 RID: 12627 RVA: 0x000B40BF File Offset: 0x000B22BF
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackTrace(int skipFrames)
		{
			this.init_frames(skipFrames, false);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class from the caller's frame, skipping the specified number of frames and optionally capturing source information.</summary>
		/// <param name="skipFrames">The number of frames up the stack from which to start the trace. </param>
		/// <param name="fNeedFileInfo">
		///       <see langword="true" /> to capture the file name, line number, and column number; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="skipFrames" /> parameter is negative. </exception>
		// Token: 0x06003154 RID: 12628 RVA: 0x000B40CF File Offset: 0x000B22CF
		[MethodImpl(MethodImplOptions.NoInlining)]
		public StackTrace(int skipFrames, bool fNeedFileInfo)
		{
			this.init_frames(skipFrames, fNeedFileInfo);
		}

		// Token: 0x06003155 RID: 12629 RVA: 0x000B40E0 File Offset: 0x000B22E0
		[MethodImpl(MethodImplOptions.NoInlining)]
		private void init_frames(int skipFrames, bool fNeedFileInfo)
		{
			if (skipFrames < 0)
			{
				throw new ArgumentOutOfRangeException("< 0", "skipFrames");
			}
			List<StackFrame> list = new List<StackFrame>();
			skipFrames += 2;
			StackFrame stackFrame;
			while ((stackFrame = new StackFrame(skipFrames, fNeedFileInfo)) != null && stackFrame.GetMethod() != null)
			{
				list.Add(stackFrame);
				skipFrames++;
			}
			this.debug_info = fNeedFileInfo;
			this.frames = list.ToArray();
		}

		// Token: 0x06003156 RID: 12630
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern StackFrame[] get_trace(Exception e, int skipFrames, bool fNeedFileInfo);

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class using the provided exception object.</summary>
		/// <param name="e">The exception object from which to construct the stack trace. </param>
		/// <exception cref="T:System.ArgumentNullException">The parameter <paramref name="e" /> is <see langword="null" />. </exception>
		// Token: 0x06003157 RID: 12631 RVA: 0x000B4146 File Offset: 0x000B2346
		public StackTrace(Exception e) : this(e, 0, false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class, using the provided exception object and optionally capturing source information.</summary>
		/// <param name="e">The exception object from which to construct the stack trace. </param>
		/// <param name="fNeedFileInfo">
		///       <see langword="true" /> to capture the file name, line number, and column number; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentNullException">The parameter <paramref name="e" /> is <see langword="null" />. </exception>
		// Token: 0x06003158 RID: 12632 RVA: 0x000B4151 File Offset: 0x000B2351
		public StackTrace(Exception e, bool fNeedFileInfo) : this(e, 0, fNeedFileInfo)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class using the provided exception object and skipping the specified number of frames.</summary>
		/// <param name="e">The exception object from which to construct the stack trace. </param>
		/// <param name="skipFrames">The number of frames up the stack from which to start the trace. </param>
		/// <exception cref="T:System.ArgumentNullException">The parameter <paramref name="e" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="skipFrames" /> parameter is negative. </exception>
		// Token: 0x06003159 RID: 12633 RVA: 0x000B415C File Offset: 0x000B235C
		public StackTrace(Exception e, int skipFrames) : this(e, skipFrames, false)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class using the provided exception object, skipping the specified number of frames and optionally capturing source information.</summary>
		/// <param name="e">The exception object from which to construct the stack trace. </param>
		/// <param name="skipFrames">The number of frames up the stack from which to start the trace. </param>
		/// <param name="fNeedFileInfo">
		///       <see langword="true" /> to capture the file name, line number, and column number; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.ArgumentNullException">The parameter <paramref name="e" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="skipFrames" /> parameter is negative. </exception>
		// Token: 0x0600315A RID: 12634 RVA: 0x000B4168 File Offset: 0x000B2368
		public StackTrace(Exception e, int skipFrames, bool fNeedFileInfo)
		{
			if (e == null)
			{
				throw new ArgumentNullException("e");
			}
			if (skipFrames < 0)
			{
				throw new ArgumentOutOfRangeException("< 0", "skipFrames");
			}
			this.frames = StackTrace.get_trace(e, skipFrames, fNeedFileInfo);
			this.captured_traces = e.captured_traces;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class that contains a single frame.</summary>
		/// <param name="frame">The frame that the <see cref="T:System.Diagnostics.StackTrace" /> object should contain. </param>
		// Token: 0x0600315B RID: 12635 RVA: 0x000B41B7 File Offset: 0x000B23B7
		public StackTrace(StackFrame frame)
		{
			this.frames = new StackFrame[1];
			this.frames[0] = frame;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.StackTrace" /> class for a specific thread, optionally capturing source information. Do not use this constructor overload.</summary>
		/// <param name="targetThread">The thread whose stack trace is requested. </param>
		/// <param name="needFileInfo">
		///       <see langword="true" /> to capture the file name, line number, and column number; otherwise, <see langword="false" />. </param>
		/// <exception cref="T:System.Threading.ThreadStateException">The thread <paramref name="targetThread" /> is not suspended. </exception>
		// Token: 0x0600315C RID: 12636 RVA: 0x000B41D4 File Offset: 0x000B23D4
		[MonoLimitation("Not possible to create StackTraces from other threads")]
		[Obsolete]
		public StackTrace(Thread targetThread, bool needFileInfo)
		{
			if (targetThread == Thread.CurrentThread)
			{
				this.init_frames(0, needFileInfo);
				return;
			}
			throw new NotImplementedException();
		}

		// Token: 0x0600315D RID: 12637 RVA: 0x000B41F2 File Offset: 0x000B23F2
		internal StackTrace(StackFrame[] frames)
		{
			this.frames = frames;
		}

		/// <summary>Gets the number of frames in the stack trace.</summary>
		/// <returns>The number of frames in the stack trace. </returns>
		// Token: 0x17000816 RID: 2070
		// (get) Token: 0x0600315E RID: 12638 RVA: 0x000B4201 File Offset: 0x000B2401
		public virtual int FrameCount
		{
			get
			{
				if (this.frames != null)
				{
					return this.frames.Length;
				}
				return 0;
			}
		}

		/// <summary>Gets the specified stack frame.</summary>
		/// <param name="index">The index of the stack frame requested. </param>
		/// <returns>The specified stack frame.</returns>
		// Token: 0x0600315F RID: 12639 RVA: 0x000B4215 File Offset: 0x000B2415
		public virtual StackFrame GetFrame(int index)
		{
			if (index < 0 || index >= this.FrameCount)
			{
				return null;
			}
			return this.frames[index];
		}

		/// <summary>Returns a copy of all stack frames in the current stack trace.</summary>
		/// <returns>An array of type <see cref="T:System.Diagnostics.StackFrame" /> representing the function calls in the stack trace.</returns>
		// Token: 0x06003160 RID: 12640 RVA: 0x000B422E File Offset: 0x000B242E
		[ComVisible(false)]
		public virtual StackFrame[] GetFrames()
		{
			return this.frames;
		}

		// Token: 0x06003161 RID: 12641 RVA: 0x000B4238 File Offset: 0x000B2438
		private static string GetAotId()
		{
			if (!StackTrace.isAotidSet)
			{
				StackTrace.aotid = Assembly.GetAotId();
				if (StackTrace.aotid != null)
				{
					StackTrace.aotid = new Guid(StackTrace.aotid).ToString("N");
				}
				StackTrace.isAotidSet = true;
			}
			return StackTrace.aotid;
		}

		// Token: 0x06003162 RID: 12642 RVA: 0x000B4284 File Offset: 0x000B2484
		private bool AddFrames(StringBuilder sb)
		{
			string text = Locale.GetText("<unknown method>");
			string text2 = "  ";
			string text3 = Locale.GetText(" in {0}:{1} ");
			string value = string.Format("{0}{1}{2} ", Environment.NewLine, text2, Locale.GetText("at"));
			int i;
			for (i = 0; i < this.FrameCount; i++)
			{
				StackFrame frame = this.GetFrame(i);
				if (i == 0)
				{
					sb.AppendFormat("{0}{1} ", text2, Locale.GetText("at"));
				}
				else
				{
					sb.Append(value);
				}
				if (frame.GetMethod() == null)
				{
					string internalMethodName = frame.GetInternalMethodName();
					if (internalMethodName != null)
					{
						sb.Append(internalMethodName);
					}
					else
					{
						sb.AppendFormat("<0x{0:x5} + 0x{1:x5}> {2}", frame.GetMethodAddress(), frame.GetNativeOffset(), text);
					}
				}
				else
				{
					this.GetFullNameForStackTrace(sb, frame.GetMethod());
					if (frame.GetILOffset() == -1)
					{
						sb.AppendFormat(" <0x{0:x5} + 0x{1:x5}>", frame.GetMethodAddress(), frame.GetNativeOffset());
						if (frame.GetMethodIndex() != 16777215U)
						{
							sb.AppendFormat(" {0}", frame.GetMethodIndex());
						}
					}
					else
					{
						sb.AppendFormat(" [0x{0:x5}]", frame.GetILOffset());
					}
					string text4 = frame.GetSecureFileName();
					if (text4[0] == '<')
					{
						string arg = frame.GetMethod().Module.ModuleVersionId.ToString("N");
						string aotId = StackTrace.GetAotId();
						if (frame.GetILOffset() != -1 || aotId == null)
						{
							text4 = string.Format("<{0}>", arg);
						}
						else
						{
							text4 = string.Format("<{0}#{1}>", arg, aotId);
						}
					}
					sb.AppendFormat(text3, text4, frame.GetFileLineNumber());
				}
			}
			return i != 0;
		}

		// Token: 0x06003163 RID: 12643 RVA: 0x000B4468 File Offset: 0x000B2668
		internal void GetFullNameForStackTrace(StringBuilder sb, MethodBase mi)
		{
			Type type = mi.DeclaringType;
			if (type.IsGenericType && !type.IsGenericTypeDefinition)
			{
				type = type.GetGenericTypeDefinition();
			}
			foreach (MethodInfo methodInfo in type.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
			{
				if (methodInfo.MetadataToken == mi.MetadataToken)
				{
					mi = methodInfo;
					break;
				}
			}
			sb.Append(type.ToString());
			sb.Append(".");
			sb.Append(mi.Name);
			if (mi.IsGenericMethod)
			{
				Type[] genericArguments = mi.GetGenericArguments();
				sb.Append("[");
				for (int j = 0; j < genericArguments.Length; j++)
				{
					if (j > 0)
					{
						sb.Append(",");
					}
					sb.Append(genericArguments[j].Name);
				}
				sb.Append("]");
			}
			ParameterInfo[] parameters = mi.GetParameters();
			sb.Append(" (");
			for (int k = 0; k < parameters.Length; k++)
			{
				if (k > 0)
				{
					sb.Append(", ");
				}
				Type type2 = parameters[k].ParameterType;
				if (type2.IsGenericType && !type2.IsGenericTypeDefinition)
				{
					type2 = type2.GetGenericTypeDefinition();
				}
				sb.Append(type2.ToString());
				if (parameters[k].Name != null)
				{
					sb.Append(" ");
					sb.Append(parameters[k].Name);
				}
			}
			sb.Append(")");
		}

		/// <summary>Builds a readable representation of the stack trace.</summary>
		/// <returns>A readable representation of the stack trace.</returns>
		// Token: 0x06003164 RID: 12644 RVA: 0x000B45E4 File Offset: 0x000B27E4
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (this.captured_traces != null)
			{
				StackTrace[] array = this.captured_traces;
				for (int i = 0; i < array.Length; i++)
				{
					if (array[i].AddFrames(stringBuilder))
					{
						stringBuilder.Append(Environment.NewLine);
						stringBuilder.Append("--- End of stack trace from previous location where exception was thrown ---");
						stringBuilder.Append(Environment.NewLine);
					}
				}
			}
			this.AddFrames(stringBuilder);
			return stringBuilder.ToString();
		}

		// Token: 0x06003165 RID: 12645 RVA: 0x0003D36C File Offset: 0x0003B56C
		internal string ToString(StackTrace.TraceFormat traceFormat)
		{
			return this.ToString();
		}

		/// <summary>Defines the default for the number of methods to omit from the stack trace. This field is constant.</summary>
		// Token: 0x04001A9F RID: 6815
		public const int METHODS_TO_SKIP = 0;

		// Token: 0x04001AA0 RID: 6816
		private StackFrame[] frames;

		// Token: 0x04001AA1 RID: 6817
		private readonly StackTrace[] captured_traces;

		// Token: 0x04001AA2 RID: 6818
		private bool debug_info;

		// Token: 0x04001AA3 RID: 6819
		private static bool isAotidSet;

		// Token: 0x04001AA4 RID: 6820
		private static string aotid;

		// Token: 0x02000424 RID: 1060
		internal enum TraceFormat
		{
			// Token: 0x04001AA6 RID: 6822
			Normal,
			// Token: 0x04001AA7 RID: 6823
			TrailingNewLine,
			// Token: 0x04001AA8 RID: 6824
			NoResourceLookup
		}
	}
}
