﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	/// <summary>Represents a document referenced by a symbol store.</summary>
	// Token: 0x02000427 RID: 1063
	[ComVisible(true)]
	public interface ISymbolDocument
	{
		/// <summary>Gets the checksum algorithm identifier.</summary>
		/// <returns>A GUID identifying the checksum algorithm. The value is all zeros, if there is no checksum.</returns>
		// Token: 0x17000817 RID: 2071
		// (get) Token: 0x06003168 RID: 12648
		Guid CheckSumAlgorithmId { get; }

		/// <summary>Gets the type of the current document.</summary>
		/// <returns>The type of the current document.</returns>
		// Token: 0x17000818 RID: 2072
		// (get) Token: 0x06003169 RID: 12649
		Guid DocumentType { get; }

		/// <summary>Checks whether the current document is stored in the symbol store.</summary>
		/// <returns>
		///     <see langword="true" /> if the current document is stored in the symbol store; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000819 RID: 2073
		// (get) Token: 0x0600316A RID: 12650
		bool HasEmbeddedSource { get; }

		/// <summary>Gets the language of the current document.</summary>
		/// <returns>The language of the current document.</returns>
		// Token: 0x1700081A RID: 2074
		// (get) Token: 0x0600316B RID: 12651
		Guid Language { get; }

		/// <summary>Gets the language vendor of the current document.</summary>
		/// <returns>The language vendor of the current document.</returns>
		// Token: 0x1700081B RID: 2075
		// (get) Token: 0x0600316C RID: 12652
		Guid LanguageVendor { get; }

		/// <summary>Gets the length, in bytes, of the embedded source.</summary>
		/// <returns>The source length of the current document.</returns>
		// Token: 0x1700081C RID: 2076
		// (get) Token: 0x0600316D RID: 12653
		int SourceLength { get; }

		/// <summary>Gets the URL of the current document.</summary>
		/// <returns>The URL of the current document.</returns>
		// Token: 0x1700081D RID: 2077
		// (get) Token: 0x0600316E RID: 12654
		string URL { get; }

		/// <summary>Returns the closest line that is a sequence point, given a line in the current document that might or might not be a sequence point.</summary>
		/// <param name="line">The specified line in the document. </param>
		/// <returns>The closest line that is a sequence point.</returns>
		// Token: 0x0600316F RID: 12655
		int FindClosestLine(int line);

		/// <summary>Gets the checksum.</summary>
		/// <returns>The checksum.</returns>
		// Token: 0x06003170 RID: 12656
		byte[] GetCheckSum();

		/// <summary>Gets the embedded document source for the specified range.</summary>
		/// <param name="startLine">The starting line in the current document. </param>
		/// <param name="startColumn">The starting column in the current document. </param>
		/// <param name="endLine">The ending line in the current document. </param>
		/// <param name="endColumn">The ending column in the current document. </param>
		/// <returns>The document source for the specified range.</returns>
		// Token: 0x06003171 RID: 12657
		byte[] GetSourceRange(int startLine, int startColumn, int endLine, int endColumn);
	}
}
