﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	/// <summary>The <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> structure is an object representation of a token that represents symbolic information.</summary>
	// Token: 0x02000433 RID: 1075
	[ComVisible(true)]
	public struct SymbolToken
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> structure when given a value.</summary>
		/// <param name="val">The value to be used for the token. </param>
		// Token: 0x060031B2 RID: 12722 RVA: 0x000B4651 File Offset: 0x000B2851
		public SymbolToken(int val)
		{
			this._val = val;
		}

		/// <summary>Determines whether <paramref name="obj" /> is an instance of <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> and is equal to this instance.</summary>
		/// <param name="obj">The object to check. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is an instance of <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> and is equal to this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x060031B3 RID: 12723 RVA: 0x000B465C File Offset: 0x000B285C
		public override bool Equals(object obj)
		{
			return obj is SymbolToken && ((SymbolToken)obj).GetToken() == this._val;
		}

		/// <summary>Determines whether <paramref name="obj" /> is equal to this instance.</summary>
		/// <param name="obj">The <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> to check.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is equal to this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x060031B4 RID: 12724 RVA: 0x000B4689 File Offset: 0x000B2889
		public bool Equals(SymbolToken obj)
		{
			return obj.GetToken() == this._val;
		}

		/// <summary>Returns a value indicating whether two <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> objects are equal.</summary>
		/// <param name="a">A <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> structure.</param>
		/// <param name="b">A <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> structure.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> and <paramref name="b" /> are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060031B5 RID: 12725 RVA: 0x000B469A File Offset: 0x000B289A
		public static bool operator ==(SymbolToken a, SymbolToken b)
		{
			return a.Equals(b);
		}

		/// <summary>Returns a value indicating whether two <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> objects are not equal.</summary>
		/// <param name="a">A <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> structure.</param>
		/// <param name="b">A <see cref="T:System.Diagnostics.SymbolStore.SymbolToken" /> structure.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> and <paramref name="b" /> are not equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060031B6 RID: 12726 RVA: 0x000B46A4 File Offset: 0x000B28A4
		public static bool operator !=(SymbolToken a, SymbolToken b)
		{
			return !a.Equals(b);
		}

		/// <summary>Generates the hash code for the current token.</summary>
		/// <returns>The hash code for the current token.</returns>
		// Token: 0x060031B7 RID: 12727 RVA: 0x000B46B1 File Offset: 0x000B28B1
		public override int GetHashCode()
		{
			return this._val.GetHashCode();
		}

		/// <summary>Gets the value of the current token.</summary>
		/// <returns>The value of the current token.</returns>
		// Token: 0x060031B8 RID: 12728 RVA: 0x000B46BE File Offset: 0x000B28BE
		public int GetToken()
		{
			return this._val;
		}

		// Token: 0x04001AC1 RID: 6849
		private int _val;
	}
}
