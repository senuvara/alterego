﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	/// <summary>Represents a variable within a symbol store.</summary>
	// Token: 0x0200042D RID: 1069
	[ComVisible(true)]
	public interface ISymbolVariable
	{
		/// <summary>Gets the first address of a variable.</summary>
		/// <returns>The first address of the variable.</returns>
		// Token: 0x17000827 RID: 2087
		// (get) Token: 0x06003192 RID: 12690
		int AddressField1 { get; }

		/// <summary>Gets the second address of a variable.</summary>
		/// <returns>The second address of the variable.</returns>
		// Token: 0x17000828 RID: 2088
		// (get) Token: 0x06003193 RID: 12691
		int AddressField2 { get; }

		/// <summary>Gets the third address of a variable.</summary>
		/// <returns>The third address of the variable.</returns>
		// Token: 0x17000829 RID: 2089
		// (get) Token: 0x06003194 RID: 12692
		int AddressField3 { get; }

		/// <summary>Gets the <see cref="T:System.Diagnostics.SymbolStore.SymAddressKind" /> value describing the type of the address.</summary>
		/// <returns>The type of the address. One of the <see cref="T:System.Diagnostics.SymbolStore.SymAddressKind" /> values.</returns>
		// Token: 0x1700082A RID: 2090
		// (get) Token: 0x06003195 RID: 12693
		SymAddressKind AddressKind { get; }

		/// <summary>Gets the attributes of the variable.</summary>
		/// <returns>The variable attributes.</returns>
		// Token: 0x1700082B RID: 2091
		// (get) Token: 0x06003196 RID: 12694
		object Attributes { get; }

		/// <summary>Gets the end offset of a variable within the scope of the variable.</summary>
		/// <returns>The end offset of the variable.</returns>
		// Token: 0x1700082C RID: 2092
		// (get) Token: 0x06003197 RID: 12695
		int EndOffset { get; }

		/// <summary>Gets the name of the variable.</summary>
		/// <returns>The name of the variable.</returns>
		// Token: 0x1700082D RID: 2093
		// (get) Token: 0x06003198 RID: 12696
		string Name { get; }

		/// <summary>Gets the start offset of the variable within the scope of the variable.</summary>
		/// <returns>The start offset of the variable.</returns>
		// Token: 0x1700082E RID: 2094
		// (get) Token: 0x06003199 RID: 12697
		int StartOffset { get; }

		/// <summary>Gets the variable signature.</summary>
		/// <returns>The variable signature as an opaque blob.</returns>
		// Token: 0x0600319A RID: 12698
		byte[] GetSignature();
	}
}
