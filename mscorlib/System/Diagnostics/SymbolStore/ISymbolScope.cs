﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	/// <summary>Represents a lexical scope within <see cref="T:System.Diagnostics.SymbolStore.ISymbolMethod" />, providing access to the start and end offsets of the scope, as well as its child and parent scopes.</summary>
	// Token: 0x0200042C RID: 1068
	[ComVisible(true)]
	public interface ISymbolScope
	{
		/// <summary>Gets the end offset of the current lexical scope.</summary>
		/// <returns>The end offset of the current lexical scope.</returns>
		// Token: 0x17000823 RID: 2083
		// (get) Token: 0x0600318B RID: 12683
		int EndOffset { get; }

		/// <summary>Gets the method that contains the current lexical scope.</summary>
		/// <returns>The method that contains the current lexical scope.</returns>
		// Token: 0x17000824 RID: 2084
		// (get) Token: 0x0600318C RID: 12684
		ISymbolMethod Method { get; }

		/// <summary>Gets the parent lexical scope of the current scope.</summary>
		/// <returns>The parent lexical scope of the current scope.</returns>
		// Token: 0x17000825 RID: 2085
		// (get) Token: 0x0600318D RID: 12685
		ISymbolScope Parent { get; }

		/// <summary>Gets the start offset of the current lexical scope.</summary>
		/// <returns>The start offset of the current lexical scope.</returns>
		// Token: 0x17000826 RID: 2086
		// (get) Token: 0x0600318E RID: 12686
		int StartOffset { get; }

		/// <summary>Gets the child lexical scopes of the current lexical scope.</summary>
		/// <returns>The child lexical scopes that of the current lexical scope.</returns>
		// Token: 0x0600318F RID: 12687
		ISymbolScope[] GetChildren();

		/// <summary>Gets the local variables within the current lexical scope.</summary>
		/// <returns>The local variables within the current lexical scope.</returns>
		// Token: 0x06003190 RID: 12688
		ISymbolVariable[] GetLocals();

		/// <summary>Gets the namespaces that are used within the current scope.</summary>
		/// <returns>The namespaces that are used within the current scope.</returns>
		// Token: 0x06003191 RID: 12689
		ISymbolNamespace[] GetNamespaces();
	}
}
