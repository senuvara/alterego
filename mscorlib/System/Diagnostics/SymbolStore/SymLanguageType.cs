﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	/// <summary>Holds the public GUIDs for language types to be used with the symbol store.</summary>
	// Token: 0x02000431 RID: 1073
	[ComVisible(true)]
	public class SymLanguageType
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.SymbolStore.SymLanguageType" /> class.</summary>
		// Token: 0x060031B0 RID: 12720 RVA: 0x00002050 File Offset: 0x00000250
		public SymLanguageType()
		{
		}

		/// <summary>Specifies the GUID of the Basic language type to be used with the symbol store.</summary>
		// Token: 0x04001AB5 RID: 6837
		public static readonly Guid Basic;

		/// <summary>Specifies the GUID of the C language type to be used with the symbol store.</summary>
		// Token: 0x04001AB6 RID: 6838
		public static readonly Guid C;

		/// <summary>Specifies the GUID of the Cobol language type to be used with the symbol store.</summary>
		// Token: 0x04001AB7 RID: 6839
		public static readonly Guid Cobol;

		/// <summary>Specifies the GUID of the C++ language type to be used with the symbol store.</summary>
		// Token: 0x04001AB8 RID: 6840
		public static readonly Guid CPlusPlus;

		/// <summary>Specifies the GUID of the C# language type to be used with the symbol store.</summary>
		// Token: 0x04001AB9 RID: 6841
		public static readonly Guid CSharp;

		/// <summary>Specifies the GUID of the ILAssembly language type to be used with the symbol store.</summary>
		// Token: 0x04001ABA RID: 6842
		public static readonly Guid ILAssembly;

		/// <summary>Specifies the GUID of the Java language type to be used with the symbol store.</summary>
		// Token: 0x04001ABB RID: 6843
		public static readonly Guid Java;

		/// <summary>Specifies the GUID of the JScript language type to be used with the symbol store.</summary>
		// Token: 0x04001ABC RID: 6844
		public static readonly Guid JScript;

		/// <summary>Specifies the GUID of the C++ language type to be used with the symbol store.</summary>
		// Token: 0x04001ABD RID: 6845
		public static readonly Guid MCPlusPlus;

		/// <summary>Specifies the GUID of the Pascal language type to be used with the symbol store.</summary>
		// Token: 0x04001ABE RID: 6846
		public static readonly Guid Pascal;

		/// <summary>Specifies the GUID of the SMC language type to be used with the symbol store.</summary>
		// Token: 0x04001ABF RID: 6847
		public static readonly Guid SMC;
	}
}
