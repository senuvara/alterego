﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	/// <summary>Holds the public GUIDs for document types to be used with the symbol store.</summary>
	// Token: 0x02000430 RID: 1072
	[ComVisible(true)]
	public class SymDocumentType
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.SymbolStore.SymDocumentType" /> class.</summary>
		// Token: 0x060031AF RID: 12719 RVA: 0x00002050 File Offset: 0x00000250
		public SymDocumentType()
		{
		}

		/// <summary>Specifies the GUID of the document type to be used with the symbol store.</summary>
		// Token: 0x04001AB4 RID: 6836
		public static readonly Guid Text;
	}
}
