﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	/// <summary>Holds the public GUIDs for language vendors to be used with the symbol store.</summary>
	// Token: 0x02000432 RID: 1074
	[ComVisible(true)]
	public class SymLanguageVendor
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.SymbolStore.SymLanguageVendor" /> class. </summary>
		// Token: 0x060031B1 RID: 12721 RVA: 0x00002050 File Offset: 0x00000250
		public SymLanguageVendor()
		{
		}

		/// <summary>Specifies the GUID of the Microsoft language vendor.</summary>
		// Token: 0x04001AC0 RID: 6848
		public static readonly Guid Microsoft;
	}
}
