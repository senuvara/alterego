﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000414 RID: 1044
	[Serializable]
	internal enum AssertFilters
	{
		// Token: 0x04001A76 RID: 6774
		FailDebug,
		// Token: 0x04001A77 RID: 6775
		FailIgnore,
		// Token: 0x04001A78 RID: 6776
		FailTerminate,
		// Token: 0x04001A79 RID: 6777
		FailContinueFilter
	}
}
