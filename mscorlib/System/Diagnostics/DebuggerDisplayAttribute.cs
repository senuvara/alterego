﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	/// <summary>Determines how a class or field is displayed in the debugger variable windows.</summary>
	// Token: 0x0200041F RID: 1055
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Delegate, AllowMultiple = true)]
	public sealed class DebuggerDisplayAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerDisplayAttribute" /> class. </summary>
		/// <param name="value">The string to be displayed in the value column for instances of the type; an empty string ("") causes the value column to be hidden.</param>
		// Token: 0x0600311E RID: 12574 RVA: 0x000B3B36 File Offset: 0x000B1D36
		public DebuggerDisplayAttribute(string value)
		{
			if (value == null)
			{
				this.value = "";
			}
			else
			{
				this.value = value;
			}
			this.name = "";
			this.type = "";
		}

		/// <summary>Gets the string to display in the value column of the debugger variable windows.</summary>
		/// <returns>The string to display in the value column of the debugger variable.</returns>
		// Token: 0x1700080B RID: 2059
		// (get) Token: 0x0600311F RID: 12575 RVA: 0x000B3B6B File Offset: 0x000B1D6B
		public string Value
		{
			get
			{
				return this.value;
			}
		}

		/// <summary>Gets or sets the name to display in the debugger variable windows.</summary>
		/// <returns>The name to display in the debugger variable windows.</returns>
		// Token: 0x1700080C RID: 2060
		// (get) Token: 0x06003120 RID: 12576 RVA: 0x000B3B73 File Offset: 0x000B1D73
		// (set) Token: 0x06003121 RID: 12577 RVA: 0x000B3B7B File Offset: 0x000B1D7B
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets the string to display in the type column of the debugger variable windows.</summary>
		/// <returns>The string to display in the type column of the debugger variable windows.</returns>
		// Token: 0x1700080D RID: 2061
		// (get) Token: 0x06003122 RID: 12578 RVA: 0x000B3B84 File Offset: 0x000B1D84
		// (set) Token: 0x06003123 RID: 12579 RVA: 0x000B3B8C File Offset: 0x000B1D8C
		public string Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		/// <summary>Gets or sets the type of the attribute's target.</summary>
		/// <returns>The attribute's target type.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Diagnostics.DebuggerDisplayAttribute.Target" /> is set to <see langword="null" />.</exception>
		// Token: 0x1700080E RID: 2062
		// (get) Token: 0x06003125 RID: 12581 RVA: 0x000B3BBE File Offset: 0x000B1DBE
		// (set) Token: 0x06003124 RID: 12580 RVA: 0x000B3B95 File Offset: 0x000B1D95
		public Type Target
		{
			get
			{
				return this.target;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.targetName = value.AssemblyQualifiedName;
				this.target = value;
			}
		}

		/// <summary>Gets or sets the type name of the attribute's target.</summary>
		/// <returns>The name of the attribute's target type.</returns>
		// Token: 0x1700080F RID: 2063
		// (get) Token: 0x06003126 RID: 12582 RVA: 0x000B3BC6 File Offset: 0x000B1DC6
		// (set) Token: 0x06003127 RID: 12583 RVA: 0x000B3BCE File Offset: 0x000B1DCE
		public string TargetTypeName
		{
			get
			{
				return this.targetName;
			}
			set
			{
				this.targetName = value;
			}
		}

		// Token: 0x04001A8A RID: 6794
		private string name;

		// Token: 0x04001A8B RID: 6795
		private string value;

		// Token: 0x04001A8C RID: 6796
		private string type;

		// Token: 0x04001A8D RID: 6797
		private string targetName;

		// Token: 0x04001A8E RID: 6798
		private Type target;
	}
}
