﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000412 RID: 1042
	[Serializable]
	internal abstract class AssertFilter
	{
		// Token: 0x06003106 RID: 12550
		public abstract AssertFilters AssertFailure(string condition, string message, StackTrace location, StackTrace.TraceFormat stackTraceFormat, string windowTitle);

		// Token: 0x06003107 RID: 12551 RVA: 0x00002050 File Offset: 0x00000250
		protected AssertFilter()
		{
		}
	}
}
