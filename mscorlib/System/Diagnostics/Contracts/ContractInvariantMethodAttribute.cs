﻿using System;

namespace System.Diagnostics.Contracts
{
	/// <summary>Marks a method as being the invariant method for a class.</summary>
	// Token: 0x02000451 RID: 1105
	[Conditional("CONTRACTS_FULL")]
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public sealed class ContractInvariantMethodAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractInvariantMethodAttribute" /> class. </summary>
		// Token: 0x0600325F RID: 12895 RVA: 0x000020BF File Offset: 0x000002BF
		public ContractInvariantMethodAttribute()
		{
		}
	}
}
