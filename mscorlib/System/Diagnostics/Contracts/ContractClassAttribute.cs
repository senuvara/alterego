﻿using System;

namespace System.Diagnostics.Contracts
{
	/// <summary>Specifies that a separate type contains the code contracts for this type.</summary>
	// Token: 0x0200044F RID: 1103
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Delegate, AllowMultiple = false, Inherited = false)]
	[Conditional("DEBUG")]
	[Conditional("CONTRACTS_FULL")]
	public sealed class ContractClassAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractClassAttribute" /> class. </summary>
		/// <param name="typeContainingContracts">The type that contains the code contracts for this type.</param>
		// Token: 0x0600325B RID: 12891 RVA: 0x000B4BBD File Offset: 0x000B2DBD
		public ContractClassAttribute(Type typeContainingContracts)
		{
			this._typeWithContracts = typeContainingContracts;
		}

		/// <summary>Gets the type that contains the code contracts for this type.</summary>
		/// <returns>The type that contains the code contracts for this type. </returns>
		// Token: 0x1700085D RID: 2141
		// (get) Token: 0x0600325C RID: 12892 RVA: 0x000B4BCC File Offset: 0x000B2DCC
		public Type TypeContainingContracts
		{
			get
			{
				return this._typeWithContracts;
			}
		}

		// Token: 0x04001B2C RID: 6956
		private Type _typeWithContracts;
	}
}
