﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Security;

namespace System.Diagnostics.Contracts
{
	/// <summary>Provides methods and data for the <see cref="E:System.Diagnostics.Contracts.Contract.ContractFailed" /> event.</summary>
	// Token: 0x0200045B RID: 1115
	public sealed class ContractFailedEventArgs : EventArgs
	{
		/// <summary>Provides data for the <see cref="E:System.Diagnostics.Contracts.Contract.ContractFailed" /> event.</summary>
		/// <param name="failureKind">One of the enumeration values that specifies the contract that failed.</param>
		/// <param name="message">The message for the event.</param>
		/// <param name="condition">The condition for the event.</param>
		/// <param name="originalException">The exception that caused the event.</param>
		// Token: 0x06003288 RID: 12936 RVA: 0x000B4FB4 File Offset: 0x000B31B4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public ContractFailedEventArgs(ContractFailureKind failureKind, string message, string condition, Exception originalException)
		{
			this._failureKind = failureKind;
			this._message = message;
			this._condition = condition;
			this._originalException = originalException;
		}

		/// <summary>Gets the message that describes the <see cref="E:System.Diagnostics.Contracts.Contract.ContractFailed" /> event.</summary>
		/// <returns>The message that describes the event.</returns>
		// Token: 0x17000865 RID: 2149
		// (get) Token: 0x06003289 RID: 12937 RVA: 0x000B4FD9 File Offset: 0x000B31D9
		public string Message
		{
			get
			{
				return this._message;
			}
		}

		/// <summary>Gets the condition for the failure of the contract.</summary>
		/// <returns>The condition for the failure.</returns>
		// Token: 0x17000866 RID: 2150
		// (get) Token: 0x0600328A RID: 12938 RVA: 0x000B4FE1 File Offset: 0x000B31E1
		public string Condition
		{
			get
			{
				return this._condition;
			}
		}

		/// <summary>Gets the type of contract that failed.</summary>
		/// <returns>One of the enumeration values that specifies the type of contract that failed.</returns>
		// Token: 0x17000867 RID: 2151
		// (get) Token: 0x0600328B RID: 12939 RVA: 0x000B4FE9 File Offset: 0x000B31E9
		public ContractFailureKind FailureKind
		{
			get
			{
				return this._failureKind;
			}
		}

		/// <summary>Gets the original exception that caused the <see cref="E:System.Diagnostics.Contracts.Contract.ContractFailed" /> event.</summary>
		/// <returns>The exception that caused the event.</returns>
		// Token: 0x17000868 RID: 2152
		// (get) Token: 0x0600328C RID: 12940 RVA: 0x000B4FF1 File Offset: 0x000B31F1
		public Exception OriginalException
		{
			get
			{
				return this._originalException;
			}
		}

		/// <summary>Indicates whether the <see cref="E:System.Diagnostics.Contracts.Contract.ContractFailed" /> event has been handled.</summary>
		/// <returns>
		///     <see langword="true" /> if the event has been handled; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000869 RID: 2153
		// (get) Token: 0x0600328D RID: 12941 RVA: 0x000B4FF9 File Offset: 0x000B31F9
		public bool Handled
		{
			get
			{
				return this._handled;
			}
		}

		/// <summary>Sets the <see cref="P:System.Diagnostics.Contracts.ContractFailedEventArgs.Handled" /> property to <see langword="true" />.</summary>
		// Token: 0x0600328E RID: 12942 RVA: 0x000B5001 File Offset: 0x000B3201
		[SecurityCritical]
		public void SetHandled()
		{
			this._handled = true;
		}

		/// <summary>Indicates whether the code contract escalation policy should be applied.</summary>
		/// <returns>
		///     <see langword="true" /> to apply the escalation policy; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x1700086A RID: 2154
		// (get) Token: 0x0600328F RID: 12943 RVA: 0x000B500A File Offset: 0x000B320A
		public bool Unwind
		{
			get
			{
				return this._unwind;
			}
		}

		/// <summary>Sets the <see cref="P:System.Diagnostics.Contracts.ContractFailedEventArgs.Unwind" /> property to <see langword="true" />.</summary>
		// Token: 0x06003290 RID: 12944 RVA: 0x000B5012 File Offset: 0x000B3212
		[SecurityCritical]
		public void SetUnwind()
		{
			this._unwind = true;
		}

		// Token: 0x04001B3C RID: 6972
		private ContractFailureKind _failureKind;

		// Token: 0x04001B3D RID: 6973
		private string _message;

		// Token: 0x04001B3E RID: 6974
		private string _condition;

		// Token: 0x04001B3F RID: 6975
		private Exception _originalException;

		// Token: 0x04001B40 RID: 6976
		private bool _handled;

		// Token: 0x04001B41 RID: 6977
		private bool _unwind;

		// Token: 0x04001B42 RID: 6978
		internal Exception thrownDuringHandler;
	}
}
