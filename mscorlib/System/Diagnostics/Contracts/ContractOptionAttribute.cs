﻿using System;

namespace System.Diagnostics.Contracts
{
	/// <summary>Enables you to set contract and tool options at assembly, type, or method granularity.</summary>
	// Token: 0x02000458 RID: 1112
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = false)]
	[Conditional("CONTRACTS_FULL")]
	public sealed class ContractOptionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractOptionAttribute" /> class by using the provided category, setting, and enable/disable value.</summary>
		/// <param name="category">The category for the option to be set.</param>
		/// <param name="setting">The option setting.</param>
		/// <param name="enabled">
		///       <see langword="true" /> to enable the option; <see langword="false" /> to disable the option.</param>
		// Token: 0x06003268 RID: 12904 RVA: 0x000B4C19 File Offset: 0x000B2E19
		public ContractOptionAttribute(string category, string setting, bool enabled)
		{
			this._category = category;
			this._setting = setting;
			this._enabled = enabled;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractOptionAttribute" /> class by using the provided category, setting, and value.</summary>
		/// <param name="category">The category of the option to be set.</param>
		/// <param name="setting">The option setting.</param>
		/// <param name="value">The value for the setting.</param>
		// Token: 0x06003269 RID: 12905 RVA: 0x000B4C36 File Offset: 0x000B2E36
		public ContractOptionAttribute(string category, string setting, string value)
		{
			this._category = category;
			this._setting = setting;
			this._value = value;
		}

		/// <summary>Gets the category of the option.</summary>
		/// <returns>The category of the option.</returns>
		// Token: 0x17000861 RID: 2145
		// (get) Token: 0x0600326A RID: 12906 RVA: 0x000B4C53 File Offset: 0x000B2E53
		public string Category
		{
			get
			{
				return this._category;
			}
		}

		/// <summary>Gets the setting for the option.</summary>
		/// <returns>The setting for the option.</returns>
		// Token: 0x17000862 RID: 2146
		// (get) Token: 0x0600326B RID: 12907 RVA: 0x000B4C5B File Offset: 0x000B2E5B
		public string Setting
		{
			get
			{
				return this._setting;
			}
		}

		/// <summary>Determines if an option is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if the option is enabled; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000863 RID: 2147
		// (get) Token: 0x0600326C RID: 12908 RVA: 0x000B4C63 File Offset: 0x000B2E63
		public bool Enabled
		{
			get
			{
				return this._enabled;
			}
		}

		/// <summary>Gets the value for the option.</summary>
		/// <returns>The value for the option.</returns>
		// Token: 0x17000864 RID: 2148
		// (get) Token: 0x0600326D RID: 12909 RVA: 0x000B4C6B File Offset: 0x000B2E6B
		public string Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x04001B30 RID: 6960
		private string _category;

		// Token: 0x04001B31 RID: 6961
		private string _setting;

		// Token: 0x04001B32 RID: 6962
		private bool _enabled;

		// Token: 0x04001B33 RID: 6963
		private string _value;
	}
}
