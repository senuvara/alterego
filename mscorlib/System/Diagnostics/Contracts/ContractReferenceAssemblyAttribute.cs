﻿using System;

namespace System.Diagnostics.Contracts
{
	/// <summary>Specifies that an assembly is a reference assembly that contains contracts.</summary>
	// Token: 0x02000452 RID: 1106
	[AttributeUsage(AttributeTargets.Assembly)]
	public sealed class ContractReferenceAssemblyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractReferenceAssemblyAttribute" /> class. </summary>
		// Token: 0x06003260 RID: 12896 RVA: 0x000020BF File Offset: 0x000002BF
		public ContractReferenceAssemblyAttribute()
		{
		}
	}
}
