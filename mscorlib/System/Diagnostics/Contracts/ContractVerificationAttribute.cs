﻿using System;

namespace System.Diagnostics.Contracts
{
	/// <summary>Instructs analysis tools to assume the correctness of an assembly, type, or member without performing static verification.</summary>
	// Token: 0x02000454 RID: 1108
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property)]
	[Conditional("CONTRACTS_FULL")]
	public sealed class ContractVerificationAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractVerificationAttribute" /> class. </summary>
		/// <param name="value">
		///       <see langword="true" /> to require verification; otherwise, <see langword="false" />. </param>
		// Token: 0x06003262 RID: 12898 RVA: 0x000B4BEB File Offset: 0x000B2DEB
		public ContractVerificationAttribute(bool value)
		{
			this._value = value;
		}

		/// <summary>Gets the value that indicates whether to verify the contract of the target. </summary>
		/// <returns>
		///     <see langword="true" /> if verification is required; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700085F RID: 2143
		// (get) Token: 0x06003263 RID: 12899 RVA: 0x000B4BFA File Offset: 0x000B2DFA
		public bool Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x04001B2E RID: 6958
		private bool _value;
	}
}
