﻿using System;

namespace System.Diagnostics.Contracts
{
	/// <summary>Specifies that a field can be used in method contracts when the field has less visibility than the method. </summary>
	// Token: 0x02000455 RID: 1109
	[AttributeUsage(AttributeTargets.Field)]
	[Conditional("CONTRACTS_FULL")]
	public sealed class ContractPublicPropertyNameAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractPublicPropertyNameAttribute" /> class. </summary>
		/// <param name="name">The property name to apply to the field.</param>
		// Token: 0x06003264 RID: 12900 RVA: 0x000B4C02 File Offset: 0x000B2E02
		public ContractPublicPropertyNameAttribute(string name)
		{
			this._publicName = name;
		}

		/// <summary>Gets the property name to be applied to the field.</summary>
		/// <returns>The property name to be applied to the field.</returns>
		// Token: 0x17000860 RID: 2144
		// (get) Token: 0x06003265 RID: 12901 RVA: 0x000B4C11 File Offset: 0x000B2E11
		public string Name
		{
			get
			{
				return this._publicName;
			}
		}

		// Token: 0x04001B2F RID: 6959
		private string _publicName;
	}
}
