﻿using System;

namespace System.Diagnostics.Contracts
{
	/// <summary>Enables the factoring of legacy <see langword="if-then-throw" /> code into separate methods for reuse, and provides full control over thrown exceptions and arguments.</summary>
	// Token: 0x02000456 RID: 1110
	[Conditional("CONTRACTS_FULL")]
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public sealed class ContractArgumentValidatorAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractArgumentValidatorAttribute" /> class.</summary>
		// Token: 0x06003266 RID: 12902 RVA: 0x000020BF File Offset: 0x000002BF
		public ContractArgumentValidatorAttribute()
		{
		}
	}
}
