﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace System.Diagnostics.Contracts
{
	// Token: 0x0200045C RID: 1116
	[Serializable]
	internal sealed class ContractException : Exception
	{
		// Token: 0x1700086B RID: 2155
		// (get) Token: 0x06003291 RID: 12945 RVA: 0x000B501B File Offset: 0x000B321B
		public ContractFailureKind Kind
		{
			get
			{
				return this._Kind;
			}
		}

		// Token: 0x1700086C RID: 2156
		// (get) Token: 0x06003292 RID: 12946 RVA: 0x000B5023 File Offset: 0x000B3223
		public string Failure
		{
			get
			{
				return this.Message;
			}
		}

		// Token: 0x1700086D RID: 2157
		// (get) Token: 0x06003293 RID: 12947 RVA: 0x000B502B File Offset: 0x000B322B
		public string UserMessage
		{
			get
			{
				return this._UserMessage;
			}
		}

		// Token: 0x1700086E RID: 2158
		// (get) Token: 0x06003294 RID: 12948 RVA: 0x000B5033 File Offset: 0x000B3233
		public string Condition
		{
			get
			{
				return this._Condition;
			}
		}

		// Token: 0x06003295 RID: 12949 RVA: 0x000B503B File Offset: 0x000B323B
		private ContractException()
		{
			base.HResult = -2146233022;
		}

		// Token: 0x06003296 RID: 12950 RVA: 0x000B504E File Offset: 0x000B324E
		public ContractException(ContractFailureKind kind, string failure, string userMessage, string condition, Exception innerException) : base(failure, innerException)
		{
			base.HResult = -2146233022;
			this._Kind = kind;
			this._UserMessage = userMessage;
			this._Condition = condition;
		}

		// Token: 0x06003297 RID: 12951 RVA: 0x000B507A File Offset: 0x000B327A
		private ContractException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this._Kind = (ContractFailureKind)info.GetInt32("Kind");
			this._UserMessage = info.GetString("UserMessage");
			this._Condition = info.GetString("Condition");
		}

		// Token: 0x06003298 RID: 12952 RVA: 0x000B50B8 File Offset: 0x000B32B8
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("Kind", this._Kind);
			info.AddValue("UserMessage", this._UserMessage);
			info.AddValue("Condition", this._Condition);
		}

		// Token: 0x04001B43 RID: 6979
		private readonly ContractFailureKind _Kind;

		// Token: 0x04001B44 RID: 6980
		private readonly string _UserMessage;

		// Token: 0x04001B45 RID: 6981
		private readonly string _Condition;
	}
}
