﻿using System;

namespace System.Diagnostics.Contracts
{
	/// <summary>Specifies that a class is a contract for a type.</summary>
	// Token: 0x02000450 RID: 1104
	[Conditional("CONTRACTS_FULL")]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public sealed class ContractClassForAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.Contracts.ContractClassForAttribute" /> class, specifying the type the current class is a contract for. </summary>
		/// <param name="typeContractsAreFor">The type the current class is a contract for.</param>
		// Token: 0x0600325D RID: 12893 RVA: 0x000B4BD4 File Offset: 0x000B2DD4
		public ContractClassForAttribute(Type typeContractsAreFor)
		{
			this._typeIAmAContractFor = typeContractsAreFor;
		}

		/// <summary>Gets the type that this code contract applies to. </summary>
		/// <returns>The type that this contract applies to.</returns>
		// Token: 0x1700085E RID: 2142
		// (get) Token: 0x0600325E RID: 12894 RVA: 0x000B4BE3 File Offset: 0x000B2DE3
		public Type TypeContractsAreFor
		{
			get
			{
				return this._typeIAmAContractFor;
			}
		}

		// Token: 0x04001B2D RID: 6957
		private Type _typeIAmAContractFor;
	}
}
