﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	/// <summary>Indicates to compilers that a method call or attribute should be ignored unless a specified conditional compilation symbol is defined.</summary>
	// Token: 0x02000415 RID: 1045
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
	[ComVisible(true)]
	[Serializable]
	public sealed class ConditionalAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.ConditionalAttribute" /> class.</summary>
		/// <param name="conditionString">A string that specifies the case-sensitive conditional compilation symbol that is associated with the attribute. </param>
		// Token: 0x0600310A RID: 12554 RVA: 0x000B3A0A File Offset: 0x000B1C0A
		public ConditionalAttribute(string conditionString)
		{
			this.m_conditionString = conditionString;
		}

		/// <summary>Gets the conditional compilation symbol that is associated with the <see cref="T:System.Diagnostics.ConditionalAttribute" /> attribute.</summary>
		/// <returns>A string that specifies the case-sensitive conditional compilation symbol that is associated with the <see cref="T:System.Diagnostics.ConditionalAttribute" /> attribute.</returns>
		// Token: 0x17000803 RID: 2051
		// (get) Token: 0x0600310B RID: 12555 RVA: 0x000B3A19 File Offset: 0x000B1C19
		public string ConditionString
		{
			get
			{
				return this.m_conditionString;
			}
		}

		// Token: 0x04001A7A RID: 6778
		private string m_conditionString;
	}
}
