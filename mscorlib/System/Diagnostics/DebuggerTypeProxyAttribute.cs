﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	/// <summary>Specifies the display proxy for a type.</summary>
	// Token: 0x0200041E RID: 1054
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
	[ComVisible(true)]
	public sealed class DebuggerTypeProxyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerTypeProxyAttribute" /> class using the type of the proxy. </summary>
		/// <param name="type">The proxy type.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> is <see langword="null" />.</exception>
		// Token: 0x06003117 RID: 12567 RVA: 0x000B3AB5 File Offset: 0x000B1CB5
		public DebuggerTypeProxyAttribute(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			this.typeName = type.AssemblyQualifiedName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Diagnostics.DebuggerTypeProxyAttribute" /> class using the type name of the proxy. </summary>
		/// <param name="typeName">The type name of the proxy type.</param>
		// Token: 0x06003118 RID: 12568 RVA: 0x000B3ADD File Offset: 0x000B1CDD
		public DebuggerTypeProxyAttribute(string typeName)
		{
			this.typeName = typeName;
		}

		/// <summary>Gets the type name of the proxy type. </summary>
		/// <returns>The type name of the proxy type.</returns>
		// Token: 0x17000808 RID: 2056
		// (get) Token: 0x06003119 RID: 12569 RVA: 0x000B3AEC File Offset: 0x000B1CEC
		public string ProxyTypeName
		{
			get
			{
				return this.typeName;
			}
		}

		/// <summary>Gets or sets the target type for the attribute.</summary>
		/// <returns>The target type for the attribute.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Diagnostics.DebuggerTypeProxyAttribute.Target" /> is set to <see langword="null" />.</exception>
		// Token: 0x17000809 RID: 2057
		// (get) Token: 0x0600311B RID: 12571 RVA: 0x000B3B1D File Offset: 0x000B1D1D
		// (set) Token: 0x0600311A RID: 12570 RVA: 0x000B3AF4 File Offset: 0x000B1CF4
		public Type Target
		{
			get
			{
				return this.target;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.targetName = value.AssemblyQualifiedName;
				this.target = value;
			}
		}

		/// <summary>Gets or sets the name of the target type.</summary>
		/// <returns>The name of the target type.</returns>
		// Token: 0x1700080A RID: 2058
		// (get) Token: 0x0600311C RID: 12572 RVA: 0x000B3B25 File Offset: 0x000B1D25
		// (set) Token: 0x0600311D RID: 12573 RVA: 0x000B3B2D File Offset: 0x000B1D2D
		public string TargetTypeName
		{
			get
			{
				return this.targetName;
			}
			set
			{
				this.targetName = value;
			}
		}

		// Token: 0x04001A87 RID: 6791
		private string typeName;

		// Token: 0x04001A88 RID: 6792
		private string targetName;

		// Token: 0x04001A89 RID: 6793
		private Type target;
	}
}
