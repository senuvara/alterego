﻿using System;
using System.Security;

namespace System.Diagnostics
{
	// Token: 0x02000413 RID: 1043
	internal class DefaultFilter : AssertFilter
	{
		// Token: 0x06003108 RID: 12552 RVA: 0x000B39EF File Offset: 0x000B1BEF
		internal DefaultFilter()
		{
		}

		// Token: 0x06003109 RID: 12553 RVA: 0x000B39F7 File Offset: 0x000B1BF7
		[SecuritySafeCritical]
		public override AssertFilters AssertFailure(string condition, string message, StackTrace location, StackTrace.TraceFormat stackTraceFormat, string windowTitle)
		{
			return (AssertFilters)Assert.ShowDefaultAssertDialog(condition, message, location.ToString(stackTraceFormat), windowTitle);
		}
	}
}
