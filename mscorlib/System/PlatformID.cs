﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Identifies the operating system, or platform, supported by an assembly.</summary>
	// Token: 0x0200021D RID: 541
	[ComVisible(true)]
	[Serializable]
	public enum PlatformID
	{
		/// <summary>The operating system is Win32s. Win32s is a layer that runs on 16-bit versions of Windows to provide access to 32-bit applications.</summary>
		// Token: 0x04000CDF RID: 3295
		Win32S,
		/// <summary>The operating system is Windows 95 or Windows 98.</summary>
		// Token: 0x04000CE0 RID: 3296
		Win32Windows,
		/// <summary>The operating system is Windows NT or later.</summary>
		// Token: 0x04000CE1 RID: 3297
		Win32NT,
		/// <summary>The operating system is Windows CE.</summary>
		// Token: 0x04000CE2 RID: 3298
		WinCE,
		/// <summary>The operating system is Unix.</summary>
		// Token: 0x04000CE3 RID: 3299
		Unix,
		/// <summary>The development platform is Xbox 360.</summary>
		// Token: 0x04000CE4 RID: 3300
		Xbox,
		/// <summary>The operating system is Macintosh.</summary>
		// Token: 0x04000CE5 RID: 3301
		MacOSX
	}
}
