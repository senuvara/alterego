﻿using System;

namespace System
{
	/// <summary>Specifies the behavior for a forced garbage collection.</summary>
	// Token: 0x02000152 RID: 338
	[Serializable]
	public enum GCCollectionMode
	{
		/// <summary>The default setting for this enumeration, which is currently <see cref="F:System.GCCollectionMode.Forced" />. </summary>
		// Token: 0x040008E1 RID: 2273
		Default,
		/// <summary>Forces the garbage collection to occur immediately.</summary>
		// Token: 0x040008E2 RID: 2274
		Forced,
		/// <summary>Allows the garbage collector to determine whether the current time is optimal to reclaim objects. </summary>
		// Token: 0x040008E3 RID: 2275
		Optimized
	}
}
