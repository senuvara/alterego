﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>The exception that is thrown when there is an attempt to dynamically access a class member that does not exist or that is not declared as public. If a member in a class library has been removed or renamed, recompile any assemblies that reference that library.</summary>
	// Token: 0x0200018D RID: 397
	[ComVisible(true)]
	[Serializable]
	public class MissingMemberException : MemberAccessException, ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMemberException" /> class.</summary>
		// Token: 0x06001142 RID: 4418 RVA: 0x00047AA0 File Offset: 0x00045CA0
		public MissingMemberException() : base(Environment.GetResourceString("Attempted to access a missing member."))
		{
			base.SetErrorCode(-2146233070);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMemberException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06001143 RID: 4419 RVA: 0x00047ABD File Offset: 0x00045CBD
		public MissingMemberException(string message) : base(message)
		{
			base.SetErrorCode(-2146233070);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMemberException" /> class with a specified error message and a reference to the inner exception that is the root cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">An instance of <see cref="T:System.Exception" /> that is the cause of the current <see langword="Exception" />. If <paramref name="inner" /> is not a null reference (<see langword="Nothing" /> in Visual Basic), then the current <see langword="Exception" /> is raised in a catch block handling <paramref name="inner" />. </param>
		// Token: 0x06001144 RID: 4420 RVA: 0x00047AD1 File Offset: 0x00045CD1
		public MissingMemberException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233070);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMemberException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06001145 RID: 4421 RVA: 0x00047AE8 File Offset: 0x00045CE8
		protected MissingMemberException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.ClassName = info.GetString("MMClassName");
			this.MemberName = info.GetString("MMMemberName");
			this.Signature = (byte[])info.GetValue("MMSignature", typeof(byte[]));
		}

		/// <summary>Gets the text string showing the class name, the member name, and the signature of the missing member.</summary>
		/// <returns>The error message string.</returns>
		// Token: 0x17000202 RID: 514
		// (get) Token: 0x06001146 RID: 4422 RVA: 0x00047B40 File Offset: 0x00045D40
		public override string Message
		{
			[SecuritySafeCritical]
			get
			{
				if (this.ClassName == null)
				{
					return base.Message;
				}
				return Environment.GetResourceString("Member '{0}' not found.", new object[]
				{
					this.ClassName + "." + this.MemberName + ((this.Signature != null) ? (" " + MissingMemberException.FormatSignature(this.Signature)) : "")
				});
			}
		}

		// Token: 0x06001147 RID: 4423
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string FormatSignature(byte[] signature);

		// Token: 0x06001148 RID: 4424 RVA: 0x00047BA9 File Offset: 0x00045DA9
		private MissingMemberException(string className, string memberName, byte[] signature)
		{
			this.ClassName = className;
			this.MemberName = memberName;
			this.Signature = signature;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMemberException" /> class with the specified class name and member name.</summary>
		/// <param name="className">The name of the class in which access to a nonexistent member was attempted. </param>
		/// <param name="memberName">The name of the member that cannot be accessed. </param>
		// Token: 0x06001149 RID: 4425 RVA: 0x00047BC6 File Offset: 0x00045DC6
		public MissingMemberException(string className, string memberName)
		{
			this.ClassName = className;
			this.MemberName = memberName;
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the class name, the member name, the signature of the missing member, and additional exception information.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> object is <see langword="null" />. </exception>
		// Token: 0x0600114A RID: 4426 RVA: 0x00047BDC File Offset: 0x00045DDC
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
			info.AddValue("MMClassName", this.ClassName, typeof(string));
			info.AddValue("MMMemberName", this.MemberName, typeof(string));
			info.AddValue("MMSignature", this.Signature, typeof(byte[]));
		}

		/// <summary>Holds the class name of the missing member.</summary>
		// Token: 0x04000A02 RID: 2562
		protected string ClassName;

		/// <summary>Holds the name of the missing member.</summary>
		// Token: 0x04000A03 RID: 2563
		protected string MemberName;

		/// <summary>Holds the signature of the missing member.</summary>
		// Token: 0x04000A04 RID: 2564
		protected byte[] Signature;
	}
}
