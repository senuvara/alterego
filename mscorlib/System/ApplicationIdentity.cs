﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>Provides the ability to uniquely identify a manifest-activated application. This class cannot be inherited. </summary>
	// Token: 0x020001F0 RID: 496
	[ComVisible(false)]
	[Serializable]
	public sealed class ApplicationIdentity : ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ApplicationIdentity" /> class. </summary>
		/// <param name="applicationIdentityFullName">The full name of the application.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="applicationIdentityFullName" /> is <see langword="null" />.</exception>
		// Token: 0x060017C6 RID: 6086 RVA: 0x0005C40B File Offset: 0x0005A60B
		public ApplicationIdentity(string applicationIdentityFullName)
		{
			if (applicationIdentityFullName == null)
			{
				throw new ArgumentNullException("applicationIdentityFullName");
			}
			if (applicationIdentityFullName.IndexOf(", Culture=") == -1)
			{
				this._fullName = applicationIdentityFullName + ", Culture=neutral";
				return;
			}
			this._fullName = applicationIdentityFullName;
		}

		/// <summary>Gets the location of the deployment manifest as a URL.</summary>
		/// <returns>The URL of the deployment manifest.</returns>
		// Token: 0x17000303 RID: 771
		// (get) Token: 0x060017C7 RID: 6087 RVA: 0x0005C448 File Offset: 0x0005A648
		public string CodeBase
		{
			get
			{
				return this._codeBase;
			}
		}

		/// <summary>Gets the full name of the application.</summary>
		/// <returns>The full name of the application, also known as the display name.</returns>
		// Token: 0x17000304 RID: 772
		// (get) Token: 0x060017C8 RID: 6088 RVA: 0x0005C450 File Offset: 0x0005A650
		public string FullName
		{
			get
			{
				return this._fullName;
			}
		}

		/// <summary>Returns the full name of the manifest-activated application.</summary>
		/// <returns>The full name of the manifest-activated application.</returns>
		// Token: 0x060017C9 RID: 6089 RVA: 0x0005C450 File Offset: 0x0005A650
		public override string ToString()
		{
			return this._fullName;
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the data needed to serialize the target object.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" />) structure for the serialization.</param>
		// Token: 0x060017CA RID: 6090 RVA: 0x0005BD7C File Offset: 0x00059F7C
		[MonoTODO("Missing serialization")]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
		}

		// Token: 0x04000C3C RID: 3132
		private string _fullName;

		// Token: 0x04000C3D RID: 3133
		private string _codeBase;
	}
}
