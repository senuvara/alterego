﻿using System;

namespace System
{
	// Token: 0x020000E6 RID: 230
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoNotSupportedAttribute : MonoTODOAttribute
	{
		// Token: 0x0600090E RID: 2318 RVA: 0x0003053C File Offset: 0x0002E73C
		public MonoNotSupportedAttribute(string comment) : base(comment)
		{
		}
	}
}
