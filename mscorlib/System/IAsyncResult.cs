﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace System
{
	/// <summary>Represents the status of an asynchronous operation. </summary>
	// Token: 0x02000170 RID: 368
	[ComVisible(true)]
	public interface IAsyncResult
	{
		/// <summary>Gets a value that indicates whether the asynchronous operation has completed.</summary>
		/// <returns>
		///     <see langword="true" /> if the operation is complete; otherwise, <see langword="false" />.</returns>
		// Token: 0x170001FD RID: 509
		// (get) Token: 0x06001045 RID: 4165
		bool IsCompleted { get; }

		/// <summary>Gets a <see cref="T:System.Threading.WaitHandle" /> that is used to wait for an asynchronous operation to complete.</summary>
		/// <returns>A <see cref="T:System.Threading.WaitHandle" /> that is used to wait for an asynchronous operation to complete.</returns>
		// Token: 0x170001FE RID: 510
		// (get) Token: 0x06001046 RID: 4166
		WaitHandle AsyncWaitHandle { get; }

		/// <summary>Gets a user-defined object that qualifies or contains information about an asynchronous operation.</summary>
		/// <returns>A user-defined object that qualifies or contains information about an asynchronous operation.</returns>
		// Token: 0x170001FF RID: 511
		// (get) Token: 0x06001047 RID: 4167
		object AsyncState { get; }

		/// <summary>Gets a value that indicates whether the asynchronous operation completed synchronously.</summary>
		/// <returns>
		///     <see langword="true" /> if the asynchronous operation completed synchronously; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000200 RID: 512
		// (get) Token: 0x06001048 RID: 4168
		bool CompletedSynchronously { get; }
	}
}
