﻿using System;
using System.IO;

namespace System.Resources
{
	// Token: 0x020002AA RID: 682
	internal abstract class Win32Resource
	{
		// Token: 0x06001F46 RID: 8006 RVA: 0x0007B510 File Offset: 0x00079710
		internal Win32Resource(NameOrId type, NameOrId name, int language)
		{
			this.type = type;
			this.name = name;
			this.language = language;
		}

		// Token: 0x06001F47 RID: 8007 RVA: 0x0007B52D File Offset: 0x0007972D
		internal Win32Resource(Win32ResourceType type, int name, int language)
		{
			this.type = new NameOrId((int)type);
			this.name = new NameOrId(name);
			this.language = language;
		}

		// Token: 0x17000446 RID: 1094
		// (get) Token: 0x06001F48 RID: 8008 RVA: 0x0007B554 File Offset: 0x00079754
		public Win32ResourceType ResourceType
		{
			get
			{
				if (this.type.IsName)
				{
					return (Win32ResourceType)(-1);
				}
				return (Win32ResourceType)this.type.Id;
			}
		}

		// Token: 0x17000447 RID: 1095
		// (get) Token: 0x06001F49 RID: 8009 RVA: 0x0007B570 File Offset: 0x00079770
		public NameOrId Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000448 RID: 1096
		// (get) Token: 0x06001F4A RID: 8010 RVA: 0x0007B578 File Offset: 0x00079778
		public NameOrId Type
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x17000449 RID: 1097
		// (get) Token: 0x06001F4B RID: 8011 RVA: 0x0007B580 File Offset: 0x00079780
		public int Language
		{
			get
			{
				return this.language;
			}
		}

		// Token: 0x06001F4C RID: 8012
		public abstract void WriteTo(Stream s);

		// Token: 0x06001F4D RID: 8013 RVA: 0x0007B588 File Offset: 0x00079788
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"Win32Resource (Kind=",
				this.ResourceType,
				", Name=",
				this.name,
				")"
			});
		}

		// Token: 0x04001109 RID: 4361
		private NameOrId type;

		// Token: 0x0400110A RID: 4362
		private NameOrId name;

		// Token: 0x0400110B RID: 4363
		private int language;
	}
}
