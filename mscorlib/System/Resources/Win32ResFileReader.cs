﻿using System;
using System.Collections;
using System.IO;
using System.Text;

namespace System.Resources
{
	// Token: 0x020002AF RID: 687
	internal class Win32ResFileReader
	{
		// Token: 0x06001F73 RID: 8051 RVA: 0x0007C074 File Offset: 0x0007A274
		public Win32ResFileReader(Stream s)
		{
			this.res_file = s;
		}

		// Token: 0x06001F74 RID: 8052 RVA: 0x0007C084 File Offset: 0x0007A284
		private int read_int16()
		{
			int num = this.res_file.ReadByte();
			if (num == -1)
			{
				return -1;
			}
			int num2 = this.res_file.ReadByte();
			if (num2 == -1)
			{
				return -1;
			}
			return num | num2 << 8;
		}

		// Token: 0x06001F75 RID: 8053 RVA: 0x0007C0BC File Offset: 0x0007A2BC
		private int read_int32()
		{
			int num = this.read_int16();
			if (num == -1)
			{
				return -1;
			}
			int num2 = this.read_int16();
			if (num2 == -1)
			{
				return -1;
			}
			return num | num2 << 16;
		}

		// Token: 0x06001F76 RID: 8054 RVA: 0x0007C0E9 File Offset: 0x0007A2E9
		private bool read_padding()
		{
			while (this.res_file.Position % 4L != 0L)
			{
				if (this.read_int16() == -1)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06001F77 RID: 8055 RVA: 0x0007C10C File Offset: 0x0007A30C
		private NameOrId read_ordinal()
		{
			if ((this.read_int16() & 65535) != 0)
			{
				return new NameOrId(this.read_int16());
			}
			byte[] array = new byte[16];
			int num = 0;
			for (;;)
			{
				int num2 = this.read_int16();
				if (num2 == 0)
				{
					break;
				}
				if (num == array.Length)
				{
					byte[] array2 = new byte[array.Length * 2];
					Array.Copy(array, array2, array.Length);
					array = array2;
				}
				array[num] = (byte)(num2 >> 8);
				array[num + 1] = (byte)(num2 & 255);
				num += 2;
			}
			return new NameOrId(new string(Encoding.Unicode.GetChars(array, 0, num)));
		}

		// Token: 0x06001F78 RID: 8056 RVA: 0x0007C198 File Offset: 0x0007A398
		public ICollection ReadResources()
		{
			ArrayList arrayList = new ArrayList();
			while (this.read_padding())
			{
				int num = this.read_int32();
				if (num == -1)
				{
					break;
				}
				this.read_int32();
				NameOrId type = this.read_ordinal();
				NameOrId name = this.read_ordinal();
				if (!this.read_padding())
				{
					break;
				}
				this.read_int32();
				this.read_int16();
				int language = this.read_int16();
				this.read_int32();
				this.read_int32();
				if (num != 0)
				{
					byte[] array = new byte[num];
					if (this.res_file.Read(array, 0, num) != num)
					{
						break;
					}
					arrayList.Add(new Win32EncodedResource(type, name, language, array));
				}
			}
			return arrayList;
		}

		// Token: 0x0400111D RID: 4381
		private Stream res_file;
	}
}
