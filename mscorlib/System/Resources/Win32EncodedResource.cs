﻿using System;
using System.IO;

namespace System.Resources
{
	// Token: 0x020002AB RID: 683
	internal class Win32EncodedResource : Win32Resource
	{
		// Token: 0x06001F4E RID: 8014 RVA: 0x0007B5C4 File Offset: 0x000797C4
		internal Win32EncodedResource(NameOrId type, NameOrId name, int language, byte[] data) : base(type, name, language)
		{
			this.data = data;
		}

		// Token: 0x1700044A RID: 1098
		// (get) Token: 0x06001F4F RID: 8015 RVA: 0x0007B5D7 File Offset: 0x000797D7
		public byte[] Data
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x06001F50 RID: 8016 RVA: 0x0007B5DF File Offset: 0x000797DF
		public override void WriteTo(Stream s)
		{
			s.Write(this.data, 0, this.data.Length);
		}

		// Token: 0x0400110C RID: 4364
		private byte[] data;
	}
}
