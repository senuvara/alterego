﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security;
using System.Threading;

namespace System.Resources
{
	// Token: 0x02000290 RID: 656
	internal class FileBasedResourceGroveler : IResourceGroveler
	{
		// Token: 0x06001E73 RID: 7795 RVA: 0x000766E4 File Offset: 0x000748E4
		public FileBasedResourceGroveler(ResourceManager.ResourceManagerMediator mediator)
		{
			this._mediator = mediator;
		}

		// Token: 0x06001E74 RID: 7796 RVA: 0x000766F4 File Offset: 0x000748F4
		[SecuritySafeCritical]
		public ResourceSet GrovelForResourceSet(CultureInfo culture, Dictionary<string, ResourceSet> localResourceSets, bool tryParents, bool createIfNotExists, ref StackCrawlMark stackMark)
		{
			ResourceSet result = null;
			string resourceFileName = this._mediator.GetResourceFileName(culture);
			string text = this.FindResourceFile(culture, resourceFileName);
			if (text == null)
			{
				if (tryParents && culture.HasInvariantCultureName)
				{
					throw new MissingManifestResourceException(string.Concat(new string[]
					{
						Environment.GetResourceString("Could not find any resources appropriate for the specified culture (or the neutral culture) on disk."),
						Environment.NewLine,
						"baseName: ",
						this._mediator.BaseNameField,
						"  locationInfo: ",
						(this._mediator.LocationInfo == null) ? "<null>" : this._mediator.LocationInfo.FullName,
						"  fileName: ",
						this._mediator.GetResourceFileName(culture)
					}));
				}
			}
			else
			{
				result = this.CreateResourceSet(text);
			}
			return result;
		}

		// Token: 0x06001E75 RID: 7797 RVA: 0x000767C4 File Offset: 0x000749C4
		public bool HasNeutralResources(CultureInfo culture, string defaultResName)
		{
			string text = this.FindResourceFile(culture, defaultResName);
			if (text == null || !File.Exists(text))
			{
				string moduleDir = this._mediator.ModuleDir;
				if (text != null)
				{
					Path.GetDirectoryName(text);
				}
				return false;
			}
			return true;
		}

		// Token: 0x06001E76 RID: 7798 RVA: 0x00076800 File Offset: 0x00074A00
		private string FindResourceFile(CultureInfo culture, string fileName)
		{
			if (this._mediator.ModuleDir != null)
			{
				string text = Path.Combine(this._mediator.ModuleDir, fileName);
				if (File.Exists(text))
				{
					return text;
				}
			}
			if (File.Exists(fileName))
			{
				return fileName;
			}
			return null;
		}

		// Token: 0x06001E77 RID: 7799 RVA: 0x00076844 File Offset: 0x00074A44
		[SecurityCritical]
		private ResourceSet CreateResourceSet(string file)
		{
			if (this._mediator.UserResourceSet == null)
			{
				return new RuntimeResourceSet(file);
			}
			object[] args = new object[]
			{
				file
			};
			ResourceSet result;
			try
			{
				result = (ResourceSet)Activator.CreateInstance(this._mediator.UserResourceSet, args);
			}
			catch (MissingMethodException innerException)
			{
				throw new InvalidOperationException(Environment.GetResourceString("'{0}': ResourceSet derived classes must provide a constructor that takes a String file name and a constructor that takes a Stream.", new object[]
				{
					this._mediator.UserResourceSet.AssemblyQualifiedName
				}), innerException);
			}
			return result;
		}

		// Token: 0x04001081 RID: 4225
		private ResourceManager.ResourceManagerMediator _mediator;
	}
}
