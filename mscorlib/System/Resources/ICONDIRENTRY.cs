﻿using System;

namespace System.Resources
{
	// Token: 0x020002B0 RID: 688
	internal class ICONDIRENTRY
	{
		// Token: 0x06001F79 RID: 8057 RVA: 0x0007C238 File Offset: 0x0007A438
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"ICONDIRENTRY (",
				this.bWidth,
				"x",
				this.bHeight,
				" ",
				this.wBitCount,
				" bpp)"
			});
		}

		// Token: 0x06001F7A RID: 8058 RVA: 0x00002050 File Offset: 0x00000250
		public ICONDIRENTRY()
		{
		}

		// Token: 0x0400111E RID: 4382
		public byte bWidth;

		// Token: 0x0400111F RID: 4383
		public byte bHeight;

		// Token: 0x04001120 RID: 4384
		public byte bColorCount;

		// Token: 0x04001121 RID: 4385
		public byte bReserved;

		// Token: 0x04001122 RID: 4386
		public short wPlanes;

		// Token: 0x04001123 RID: 4387
		public short wBitCount;

		// Token: 0x04001124 RID: 4388
		public int dwBytesInRes;

		// Token: 0x04001125 RID: 4389
		public int dwImageOffset;

		// Token: 0x04001126 RID: 4390
		public byte[] image;
	}
}
