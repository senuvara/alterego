﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace System.Resources
{
	// Token: 0x02000291 RID: 657
	internal interface IResourceGroveler
	{
		// Token: 0x06001E78 RID: 7800
		ResourceSet GrovelForResourceSet(CultureInfo culture, Dictionary<string, ResourceSet> localResourceSets, bool tryParents, bool createIfNotExists, ref StackCrawlMark stackMark);

		// Token: 0x06001E79 RID: 7801
		bool HasNeutralResources(CultureInfo culture, string defaultResName);
	}
}
