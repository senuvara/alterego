﻿using System;
using System.Runtime.InteropServices;

namespace System.Resources
{
	/// <summary>Specifies whether a <see cref="T:System.Resources.ResourceManager" /> object looks for the resources of the app's default culture in the main assembly or in a satellite assembly. </summary>
	// Token: 0x020002A7 RID: 679
	[ComVisible(true)]
	[Serializable]
	public enum UltimateResourceFallbackLocation
	{
		/// <summary>Fallback resources are located in the main assembly.</summary>
		// Token: 0x040010F0 RID: 4336
		MainAssembly,
		/// <summary>Fallback resources are located in a satellite assembly. </summary>
		// Token: 0x040010F1 RID: 4337
		Satellite
	}
}
