﻿using System;
using System.IO;

namespace System.Resources
{
	// Token: 0x020002AC RID: 684
	internal class Win32IconResource : Win32Resource
	{
		// Token: 0x06001F51 RID: 8017 RVA: 0x0007B5F6 File Offset: 0x000797F6
		public Win32IconResource(int id, int language, ICONDIRENTRY icon) : base(Win32ResourceType.RT_ICON, id, language)
		{
			this.icon = icon;
		}

		// Token: 0x1700044B RID: 1099
		// (get) Token: 0x06001F52 RID: 8018 RVA: 0x0007B608 File Offset: 0x00079808
		public ICONDIRENTRY Icon
		{
			get
			{
				return this.icon;
			}
		}

		// Token: 0x06001F53 RID: 8019 RVA: 0x0007B610 File Offset: 0x00079810
		public override void WriteTo(Stream s)
		{
			s.Write(this.icon.image, 0, this.icon.image.Length);
		}

		// Token: 0x0400110D RID: 4365
		private ICONDIRENTRY icon;
	}
}
