﻿using System;
using System.IO;

namespace System.Resources
{
	// Token: 0x020002AD RID: 685
	internal class Win32GroupIconResource : Win32Resource
	{
		// Token: 0x06001F54 RID: 8020 RVA: 0x0007B631 File Offset: 0x00079831
		public Win32GroupIconResource(int id, int language, Win32IconResource[] icons) : base(Win32ResourceType.RT_GROUP_ICON, id, language)
		{
			this.icons = icons;
		}

		// Token: 0x06001F55 RID: 8021 RVA: 0x0007B644 File Offset: 0x00079844
		public override void WriteTo(Stream s)
		{
			using (BinaryWriter binaryWriter = new BinaryWriter(s))
			{
				binaryWriter.Write(0);
				binaryWriter.Write(1);
				binaryWriter.Write((short)this.icons.Length);
				for (int i = 0; i < this.icons.Length; i++)
				{
					Win32IconResource win32IconResource = this.icons[i];
					ICONDIRENTRY icon = win32IconResource.Icon;
					binaryWriter.Write(icon.bWidth);
					binaryWriter.Write(icon.bHeight);
					binaryWriter.Write(icon.bColorCount);
					binaryWriter.Write(0);
					binaryWriter.Write(icon.wPlanes);
					binaryWriter.Write(icon.wBitCount);
					binaryWriter.Write(icon.image.Length);
					binaryWriter.Write((short)win32IconResource.Name.Id);
				}
			}
		}

		// Token: 0x0400110E RID: 4366
		private Win32IconResource[] icons;
	}
}
