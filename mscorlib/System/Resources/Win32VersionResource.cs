﻿using System;
using System.Collections;
using System.IO;
using System.Text;

namespace System.Resources
{
	// Token: 0x020002AE RID: 686
	internal class Win32VersionResource : Win32Resource
	{
		// Token: 0x06001F56 RID: 8022 RVA: 0x0007B71C File Offset: 0x0007991C
		public Win32VersionResource(int id, int language, bool compilercontext) : base(Win32ResourceType.RT_VERSION, id, language)
		{
			this.signature = (long)((ulong)-17890115);
			this.struct_version = 65536;
			this.file_flags_mask = 63;
			this.file_flags = 0;
			this.file_os = 4;
			this.file_type = 2;
			this.file_subtype = 0;
			this.file_date = 0L;
			this.file_lang = (compilercontext ? 0 : 127);
			this.file_codepage = 1200;
			this.properties = new Hashtable();
			string value = compilercontext ? string.Empty : " ";
			foreach (string key in this.WellKnownProperties)
			{
				this.properties[key] = value;
			}
			this.LegalCopyright = " ";
			this.FileDescription = " ";
		}

		// Token: 0x1700044C RID: 1100
		// (get) Token: 0x06001F57 RID: 8023 RVA: 0x0007B834 File Offset: 0x00079A34
		// (set) Token: 0x06001F58 RID: 8024 RVA: 0x0007B8BC File Offset: 0x00079ABC
		public string Version
		{
			get
			{
				return string.Concat(new object[]
				{
					this.file_version >> 48,
					".",
					this.file_version >> 32 & 65535L,
					".",
					this.file_version >> 16 & 65535L,
					".",
					this.file_version & 65535L
				});
			}
			set
			{
				long[] array = new long[4];
				if (value != null)
				{
					string[] array2 = value.Split(new char[]
					{
						'.'
					});
					try
					{
						for (int i = 0; i < array2.Length; i++)
						{
							if (i < array.Length)
							{
								array[i] = (long)int.Parse(array2[i]);
							}
						}
					}
					catch (FormatException)
					{
					}
				}
				this.file_version = (array[0] << 48 | array[1] << 32 | (array[2] << 16) + array[3]);
				this.properties["FileVersion"] = this.Version;
			}
		}

		// Token: 0x1700044D RID: 1101
		public virtual string this[string key]
		{
			set
			{
				this.properties[key] = value;
			}
		}

		// Token: 0x1700044E RID: 1102
		// (get) Token: 0x06001F5A RID: 8026 RVA: 0x0007B95F File Offset: 0x00079B5F
		// (set) Token: 0x06001F5B RID: 8027 RVA: 0x0007B976 File Offset: 0x00079B76
		public virtual string Comments
		{
			get
			{
				return (string)this.properties["Comments"];
			}
			set
			{
				this.properties["Comments"] = ((value == string.Empty) ? " " : value);
			}
		}

		// Token: 0x1700044F RID: 1103
		// (get) Token: 0x06001F5C RID: 8028 RVA: 0x0007B99D File Offset: 0x00079B9D
		// (set) Token: 0x06001F5D RID: 8029 RVA: 0x0007B9B4 File Offset: 0x00079BB4
		public virtual string CompanyName
		{
			get
			{
				return (string)this.properties["CompanyName"];
			}
			set
			{
				this.properties["CompanyName"] = ((value == string.Empty) ? " " : value);
			}
		}

		// Token: 0x17000450 RID: 1104
		// (get) Token: 0x06001F5E RID: 8030 RVA: 0x0007B9DB File Offset: 0x00079BDB
		// (set) Token: 0x06001F5F RID: 8031 RVA: 0x0007B9F2 File Offset: 0x00079BF2
		public virtual string LegalCopyright
		{
			get
			{
				return (string)this.properties["LegalCopyright"];
			}
			set
			{
				this.properties["LegalCopyright"] = ((value == string.Empty) ? " " : value);
			}
		}

		// Token: 0x17000451 RID: 1105
		// (get) Token: 0x06001F60 RID: 8032 RVA: 0x0007BA19 File Offset: 0x00079C19
		// (set) Token: 0x06001F61 RID: 8033 RVA: 0x0007BA30 File Offset: 0x00079C30
		public virtual string LegalTrademarks
		{
			get
			{
				return (string)this.properties["LegalTrademarks"];
			}
			set
			{
				this.properties["LegalTrademarks"] = ((value == string.Empty) ? " " : value);
			}
		}

		// Token: 0x17000452 RID: 1106
		// (get) Token: 0x06001F62 RID: 8034 RVA: 0x0007BA57 File Offset: 0x00079C57
		// (set) Token: 0x06001F63 RID: 8035 RVA: 0x0007BA6E File Offset: 0x00079C6E
		public virtual string OriginalFilename
		{
			get
			{
				return (string)this.properties["OriginalFilename"];
			}
			set
			{
				this.properties["OriginalFilename"] = ((value == string.Empty) ? " " : value);
			}
		}

		// Token: 0x17000453 RID: 1107
		// (get) Token: 0x06001F64 RID: 8036 RVA: 0x0007BA95 File Offset: 0x00079C95
		// (set) Token: 0x06001F65 RID: 8037 RVA: 0x0007BAAC File Offset: 0x00079CAC
		public virtual string ProductName
		{
			get
			{
				return (string)this.properties["ProductName"];
			}
			set
			{
				this.properties["ProductName"] = ((value == string.Empty) ? " " : value);
			}
		}

		// Token: 0x17000454 RID: 1108
		// (get) Token: 0x06001F66 RID: 8038 RVA: 0x0007BAD3 File Offset: 0x00079CD3
		// (set) Token: 0x06001F67 RID: 8039 RVA: 0x0007BAEC File Offset: 0x00079CEC
		public virtual string ProductVersion
		{
			get
			{
				return (string)this.properties["ProductVersion"];
			}
			set
			{
				if (value == null || value.Length == 0)
				{
					value = " ";
				}
				long[] array = new long[4];
				string[] array2 = value.Split(new char[]
				{
					'.'
				});
				try
				{
					for (int i = 0; i < array2.Length; i++)
					{
						if (i < array.Length)
						{
							array[i] = (long)int.Parse(array2[i]);
						}
					}
				}
				catch (FormatException)
				{
				}
				this.properties["ProductVersion"] = value;
				this.product_version = (array[0] << 48 | array[1] << 32 | (array[2] << 16) + array[3]);
			}
		}

		// Token: 0x17000455 RID: 1109
		// (get) Token: 0x06001F68 RID: 8040 RVA: 0x0007BB88 File Offset: 0x00079D88
		// (set) Token: 0x06001F69 RID: 8041 RVA: 0x0007BB9F File Offset: 0x00079D9F
		public virtual string InternalName
		{
			get
			{
				return (string)this.properties["InternalName"];
			}
			set
			{
				this.properties["InternalName"] = ((value == string.Empty) ? " " : value);
			}
		}

		// Token: 0x17000456 RID: 1110
		// (get) Token: 0x06001F6A RID: 8042 RVA: 0x0007BBC6 File Offset: 0x00079DC6
		// (set) Token: 0x06001F6B RID: 8043 RVA: 0x0007BBDD File Offset: 0x00079DDD
		public virtual string FileDescription
		{
			get
			{
				return (string)this.properties["FileDescription"];
			}
			set
			{
				this.properties["FileDescription"] = ((value == string.Empty) ? " " : value);
			}
		}

		// Token: 0x17000457 RID: 1111
		// (get) Token: 0x06001F6C RID: 8044 RVA: 0x0007BC04 File Offset: 0x00079E04
		// (set) Token: 0x06001F6D RID: 8045 RVA: 0x0007BC0C File Offset: 0x00079E0C
		public virtual int FileLanguage
		{
			get
			{
				return this.file_lang;
			}
			set
			{
				this.file_lang = value;
			}
		}

		// Token: 0x17000458 RID: 1112
		// (get) Token: 0x06001F6E RID: 8046 RVA: 0x0007BC15 File Offset: 0x00079E15
		// (set) Token: 0x06001F6F RID: 8047 RVA: 0x0007BC2C File Offset: 0x00079E2C
		public virtual string FileVersion
		{
			get
			{
				return (string)this.properties["FileVersion"];
			}
			set
			{
				if (value == null || value.Length == 0)
				{
					value = " ";
				}
				long[] array = new long[4];
				string[] array2 = value.Split(new char[]
				{
					'.'
				});
				try
				{
					for (int i = 0; i < array2.Length; i++)
					{
						if (i < array.Length)
						{
							array[i] = (long)int.Parse(array2[i]);
						}
					}
				}
				catch (FormatException)
				{
				}
				this.properties["FileVersion"] = value;
				this.file_version = (array[0] << 48 | array[1] << 32 | (array[2] << 16) + array[3]);
			}
		}

		// Token: 0x06001F70 RID: 8048 RVA: 0x0007BCC8 File Offset: 0x00079EC8
		private void emit_padding(BinaryWriter w)
		{
			if (w.BaseStream.Position % 4L != 0L)
			{
				w.Write(0);
			}
		}

		// Token: 0x06001F71 RID: 8049 RVA: 0x0007BCE4 File Offset: 0x00079EE4
		private void patch_length(BinaryWriter w, long len_pos)
		{
			Stream baseStream = w.BaseStream;
			long position = baseStream.Position;
			baseStream.Position = len_pos;
			w.Write((short)(position - len_pos));
			baseStream.Position = position;
		}

		// Token: 0x06001F72 RID: 8050 RVA: 0x0007BD18 File Offset: 0x00079F18
		public override void WriteTo(Stream ms)
		{
			using (BinaryWriter binaryWriter = new BinaryWriter(ms, Encoding.Unicode))
			{
				binaryWriter.Write(0);
				binaryWriter.Write(52);
				binaryWriter.Write(0);
				binaryWriter.Write("VS_VERSION_INFO".ToCharArray());
				binaryWriter.Write(0);
				this.emit_padding(binaryWriter);
				binaryWriter.Write((uint)this.signature);
				binaryWriter.Write(this.struct_version);
				binaryWriter.Write((int)(this.file_version >> 32));
				binaryWriter.Write((int)(this.file_version & (long)((ulong)-1)));
				binaryWriter.Write((int)(this.product_version >> 32));
				binaryWriter.Write((int)(this.product_version & (long)((ulong)-1)));
				binaryWriter.Write(this.file_flags_mask);
				binaryWriter.Write(this.file_flags);
				binaryWriter.Write(this.file_os);
				binaryWriter.Write(this.file_type);
				binaryWriter.Write(this.file_subtype);
				binaryWriter.Write((int)(this.file_date >> 32));
				binaryWriter.Write((int)(this.file_date & (long)((ulong)-1)));
				this.emit_padding(binaryWriter);
				long position = ms.Position;
				binaryWriter.Write(0);
				binaryWriter.Write(0);
				binaryWriter.Write(1);
				binaryWriter.Write("VarFileInfo".ToCharArray());
				binaryWriter.Write(0);
				if (ms.Position % 4L != 0L)
				{
					binaryWriter.Write(0);
				}
				long position2 = ms.Position;
				binaryWriter.Write(0);
				binaryWriter.Write(4);
				binaryWriter.Write(0);
				binaryWriter.Write("Translation".ToCharArray());
				binaryWriter.Write(0);
				if (ms.Position % 4L != 0L)
				{
					binaryWriter.Write(0);
				}
				binaryWriter.Write((short)this.file_lang);
				binaryWriter.Write((short)this.file_codepage);
				this.patch_length(binaryWriter, position2);
				this.patch_length(binaryWriter, position);
				long position3 = ms.Position;
				binaryWriter.Write(0);
				binaryWriter.Write(0);
				binaryWriter.Write(1);
				binaryWriter.Write("StringFileInfo".ToCharArray());
				this.emit_padding(binaryWriter);
				long position4 = ms.Position;
				binaryWriter.Write(0);
				binaryWriter.Write(0);
				binaryWriter.Write(1);
				binaryWriter.Write(string.Format("{0:x4}{1:x4}", this.file_lang, this.file_codepage).ToCharArray());
				this.emit_padding(binaryWriter);
				foreach (object obj in this.properties.Keys)
				{
					string text = (string)obj;
					string text2 = (string)this.properties[text];
					long position5 = ms.Position;
					binaryWriter.Write(0);
					binaryWriter.Write((short)(text2.ToCharArray().Length + 1));
					binaryWriter.Write(1);
					binaryWriter.Write(text.ToCharArray());
					binaryWriter.Write(0);
					this.emit_padding(binaryWriter);
					binaryWriter.Write(text2.ToCharArray());
					binaryWriter.Write(0);
					this.emit_padding(binaryWriter);
					this.patch_length(binaryWriter, position5);
				}
				this.patch_length(binaryWriter, position4);
				this.patch_length(binaryWriter, position3);
				this.patch_length(binaryWriter, 0L);
			}
		}

		// Token: 0x0400110F RID: 4367
		public string[] WellKnownProperties = new string[]
		{
			"Comments",
			"CompanyName",
			"FileVersion",
			"InternalName",
			"LegalTrademarks",
			"OriginalFilename",
			"ProductName",
			"ProductVersion"
		};

		// Token: 0x04001110 RID: 4368
		private long signature;

		// Token: 0x04001111 RID: 4369
		private int struct_version;

		// Token: 0x04001112 RID: 4370
		private long file_version;

		// Token: 0x04001113 RID: 4371
		private long product_version;

		// Token: 0x04001114 RID: 4372
		private int file_flags_mask;

		// Token: 0x04001115 RID: 4373
		private int file_flags;

		// Token: 0x04001116 RID: 4374
		private int file_os;

		// Token: 0x04001117 RID: 4375
		private int file_type;

		// Token: 0x04001118 RID: 4376
		private int file_subtype;

		// Token: 0x04001119 RID: 4377
		private long file_date;

		// Token: 0x0400111A RID: 4378
		private int file_lang;

		// Token: 0x0400111B RID: 4379
		private int file_codepage;

		// Token: 0x0400111C RID: 4380
		private Hashtable properties;
	}
}
