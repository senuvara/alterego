﻿using System;

namespace System.Resources
{
	// Token: 0x020002A9 RID: 681
	internal class NameOrId
	{
		// Token: 0x06001F40 RID: 8000 RVA: 0x0007B49D File Offset: 0x0007969D
		public NameOrId(string name)
		{
			this.name = name;
		}

		// Token: 0x06001F41 RID: 8001 RVA: 0x0007B4AC File Offset: 0x000796AC
		public NameOrId(int id)
		{
			this.id = id;
		}

		// Token: 0x17000443 RID: 1091
		// (get) Token: 0x06001F42 RID: 8002 RVA: 0x0007B4BB File Offset: 0x000796BB
		public bool IsName
		{
			get
			{
				return this.name != null;
			}
		}

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x06001F43 RID: 8003 RVA: 0x0007B4C6 File Offset: 0x000796C6
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000445 RID: 1093
		// (get) Token: 0x06001F44 RID: 8004 RVA: 0x0007B4CE File Offset: 0x000796CE
		public int Id
		{
			get
			{
				return this.id;
			}
		}

		// Token: 0x06001F45 RID: 8005 RVA: 0x0007B4D6 File Offset: 0x000796D6
		public override string ToString()
		{
			if (this.name != null)
			{
				return "Name(" + this.name + ")";
			}
			return "Id(" + this.id + ")";
		}

		// Token: 0x04001107 RID: 4359
		private string name;

		// Token: 0x04001108 RID: 4360
		private int id;
	}
}
