﻿using System;
using System.Runtime.InteropServices;

namespace System.Resources
{
	/// <summary>Instructs a <see cref="T:System.Resources.ResourceManager" /> object to ask for a particular version of a satellite assembly.</summary>
	// Token: 0x020002A6 RID: 678
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
	public sealed class SatelliteContractVersionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Resources.SatelliteContractVersionAttribute" /> class.</summary>
		/// <param name="version">A string that specifies the version of the satellite assemblies to load. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="version" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06001F3E RID: 7998 RVA: 0x0007B478 File Offset: 0x00079678
		public SatelliteContractVersionAttribute(string version)
		{
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			this._version = version;
		}

		/// <summary>Gets the version of the satellite assemblies with the required resources.</summary>
		/// <returns>A string that contains the version of the satellite assemblies with the required resources.</returns>
		// Token: 0x17000442 RID: 1090
		// (get) Token: 0x06001F3F RID: 7999 RVA: 0x0007B495 File Offset: 0x00079695
		public string Version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x040010EE RID: 4334
		private string _version;
	}
}
