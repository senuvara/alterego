﻿using System;

namespace System.Resources
{
	// Token: 0x020002A1 RID: 673
	[Serializable]
	internal enum ResourceTypeCode
	{
		// Token: 0x040010C7 RID: 4295
		Null,
		// Token: 0x040010C8 RID: 4296
		String,
		// Token: 0x040010C9 RID: 4297
		Boolean,
		// Token: 0x040010CA RID: 4298
		Char,
		// Token: 0x040010CB RID: 4299
		Byte,
		// Token: 0x040010CC RID: 4300
		SByte,
		// Token: 0x040010CD RID: 4301
		Int16,
		// Token: 0x040010CE RID: 4302
		UInt16,
		// Token: 0x040010CF RID: 4303
		Int32,
		// Token: 0x040010D0 RID: 4304
		UInt32,
		// Token: 0x040010D1 RID: 4305
		Int64,
		// Token: 0x040010D2 RID: 4306
		UInt64,
		// Token: 0x040010D3 RID: 4307
		Single,
		// Token: 0x040010D4 RID: 4308
		Double,
		// Token: 0x040010D5 RID: 4309
		Decimal,
		// Token: 0x040010D6 RID: 4310
		DateTime,
		// Token: 0x040010D7 RID: 4311
		TimeSpan,
		// Token: 0x040010D8 RID: 4312
		LastPrimitive = 16,
		// Token: 0x040010D9 RID: 4313
		ByteArray = 32,
		// Token: 0x040010DA RID: 4314
		Stream,
		// Token: 0x040010DB RID: 4315
		StartOfUserTypes = 64
	}
}
