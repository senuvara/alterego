﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;

namespace System.Resources
{
	// Token: 0x0200028F RID: 655
	internal sealed class FastResourceComparer : IComparer, IEqualityComparer, IComparer<string>, IEqualityComparer<string>
	{
		// Token: 0x06001E67 RID: 7783 RVA: 0x0007653C File Offset: 0x0007473C
		public int GetHashCode(object key)
		{
			return FastResourceComparer.HashFunction((string)key);
		}

		// Token: 0x06001E68 RID: 7784 RVA: 0x00076549 File Offset: 0x00074749
		public int GetHashCode(string key)
		{
			return FastResourceComparer.HashFunction(key);
		}

		// Token: 0x06001E69 RID: 7785 RVA: 0x00076554 File Offset: 0x00074754
		internal static int HashFunction(string key)
		{
			uint num = 5381U;
			for (int i = 0; i < key.Length; i++)
			{
				num = ((num << 5) + num ^ (uint)key[i]);
			}
			return (int)num;
		}

		// Token: 0x06001E6A RID: 7786 RVA: 0x00076588 File Offset: 0x00074788
		public int Compare(object a, object b)
		{
			if (a == b)
			{
				return 0;
			}
			string strA = (string)a;
			string strB = (string)b;
			return string.CompareOrdinal(strA, strB);
		}

		// Token: 0x06001E6B RID: 7787 RVA: 0x000765AE File Offset: 0x000747AE
		public int Compare(string a, string b)
		{
			return string.CompareOrdinal(a, b);
		}

		// Token: 0x06001E6C RID: 7788 RVA: 0x000765B7 File Offset: 0x000747B7
		public bool Equals(string a, string b)
		{
			return string.Equals(a, b);
		}

		// Token: 0x06001E6D RID: 7789 RVA: 0x000765C0 File Offset: 0x000747C0
		public bool Equals(object a, object b)
		{
			if (a == b)
			{
				return true;
			}
			string a2 = (string)a;
			string b2 = (string)b;
			return string.Equals(a2, b2);
		}

		// Token: 0x06001E6E RID: 7790 RVA: 0x000765E8 File Offset: 0x000747E8
		[SecurityCritical]
		public unsafe static int CompareOrdinal(string a, byte[] bytes, int bCharLength)
		{
			int num = 0;
			int num2 = 0;
			int num3 = a.Length;
			if (num3 > bCharLength)
			{
				num3 = bCharLength;
			}
			if (bCharLength == 0)
			{
				if (a.Length != 0)
				{
					return -1;
				}
				return 0;
			}
			else
			{
				fixed (byte[] array = bytes)
				{
					byte* ptr;
					if (bytes == null || array.Length == 0)
					{
						ptr = null;
					}
					else
					{
						ptr = &array[0];
					}
					byte* ptr2 = ptr;
					while (num < num3 && num2 == 0)
					{
						int num4 = (int)(*ptr2) | (int)ptr2[1] << 8;
						num2 = (int)a[num++] - num4;
						ptr2 += 2;
					}
				}
				if (num2 != 0)
				{
					return num2;
				}
				return a.Length - bCharLength;
			}
		}

		// Token: 0x06001E6F RID: 7791 RVA: 0x0007666E File Offset: 0x0007486E
		[SecurityCritical]
		public static int CompareOrdinal(byte[] bytes, int aCharLength, string b)
		{
			return -FastResourceComparer.CompareOrdinal(b, bytes, aCharLength);
		}

		// Token: 0x06001E70 RID: 7792 RVA: 0x0007667C File Offset: 0x0007487C
		[SecurityCritical]
		internal unsafe static int CompareOrdinal(byte* a, int byteLen, string b)
		{
			int num = 0;
			int num2 = 0;
			int num3 = byteLen >> 1;
			if (num3 > b.Length)
			{
				num3 = b.Length;
			}
			while (num2 < num3 && num == 0)
			{
				num = (int)((char)((int)(*(a++)) | (int)(*(a++)) << 8) - b[num2++]);
			}
			if (num != 0)
			{
				return num;
			}
			return byteLen - b.Length * 2;
		}

		// Token: 0x06001E71 RID: 7793 RVA: 0x00002050 File Offset: 0x00000250
		public FastResourceComparer()
		{
		}

		// Token: 0x06001E72 RID: 7794 RVA: 0x000766D8 File Offset: 0x000748D8
		// Note: this type is marked as 'beforefieldinit'.
		static FastResourceComparer()
		{
		}

		// Token: 0x04001080 RID: 4224
		internal static readonly FastResourceComparer Default = new FastResourceComparer();
	}
}
