﻿using System;

namespace System.Resources
{
	// Token: 0x020002A8 RID: 680
	internal enum Win32ResourceType
	{
		// Token: 0x040010F3 RID: 4339
		RT_CURSOR = 1,
		// Token: 0x040010F4 RID: 4340
		RT_FONT = 8,
		// Token: 0x040010F5 RID: 4341
		RT_BITMAP = 2,
		// Token: 0x040010F6 RID: 4342
		RT_ICON,
		// Token: 0x040010F7 RID: 4343
		RT_MENU,
		// Token: 0x040010F8 RID: 4344
		RT_DIALOG,
		// Token: 0x040010F9 RID: 4345
		RT_STRING,
		// Token: 0x040010FA RID: 4346
		RT_FONTDIR,
		// Token: 0x040010FB RID: 4347
		RT_ACCELERATOR = 9,
		// Token: 0x040010FC RID: 4348
		RT_RCDATA,
		// Token: 0x040010FD RID: 4349
		RT_MESSAGETABLE,
		// Token: 0x040010FE RID: 4350
		RT_GROUP_CURSOR,
		// Token: 0x040010FF RID: 4351
		RT_GROUP_ICON = 14,
		// Token: 0x04001100 RID: 4352
		RT_VERSION = 16,
		// Token: 0x04001101 RID: 4353
		RT_DLGINCLUDE,
		// Token: 0x04001102 RID: 4354
		RT_PLUGPLAY = 19,
		// Token: 0x04001103 RID: 4355
		RT_VXD,
		// Token: 0x04001104 RID: 4356
		RT_ANICURSOR,
		// Token: 0x04001105 RID: 4357
		RT_ANIICON,
		// Token: 0x04001106 RID: 4358
		RT_HTML
	}
}
