﻿using System;

namespace System.Resources
{
	// Token: 0x0200029D RID: 669
	internal struct ResourceLocator
	{
		// Token: 0x06001EDD RID: 7901 RVA: 0x00078526 File Offset: 0x00076726
		internal ResourceLocator(int dataPos, object value)
		{
			this._dataPos = dataPos;
			this._value = value;
		}

		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x06001EDE RID: 7902 RVA: 0x00078536 File Offset: 0x00076736
		internal int DataPosition
		{
			get
			{
				return this._dataPos;
			}
		}

		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x06001EDF RID: 7903 RVA: 0x0007853E File Offset: 0x0007673E
		// (set) Token: 0x06001EE0 RID: 7904 RVA: 0x00078546 File Offset: 0x00076746
		internal object Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x06001EE1 RID: 7905 RVA: 0x0007854F File Offset: 0x0007674F
		internal static bool CanCache(ResourceTypeCode value)
		{
			return value <= ResourceTypeCode.TimeSpan;
		}

		// Token: 0x040010AC RID: 4268
		internal object _value;

		// Token: 0x040010AD RID: 4269
		internal int _dataPos;
	}
}
