﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Resources
{
	/// <summary>The exception that is thrown when the satellite assembly for the resources of the default culture is missing.</summary>
	// Token: 0x02000296 RID: 662
	[ComVisible(true)]
	[Serializable]
	public class MissingSatelliteAssemblyException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Resources.MissingSatelliteAssemblyException" /> class with default properties.</summary>
		// Token: 0x06001E93 RID: 7827 RVA: 0x00077275 File Offset: 0x00075475
		public MissingSatelliteAssemblyException() : base(Environment.GetResourceString("Resource lookup fell back to the ultimate fallback resources in a satellite assembly, but that satellite either was not found or could not be loaded. Please consider reinstalling or repairing the application."))
		{
			base.SetErrorCode(-2146233034);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Resources.MissingSatelliteAssemblyException" /> class with the specified error message. </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		// Token: 0x06001E94 RID: 7828 RVA: 0x00077292 File Offset: 0x00075492
		public MissingSatelliteAssemblyException(string message) : base(message)
		{
			base.SetErrorCode(-2146233034);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Resources.MissingSatelliteAssemblyException" /> class with a specified error message and the name of a neutral culture. </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="cultureName">The name of the neutral culture.</param>
		// Token: 0x06001E95 RID: 7829 RVA: 0x000772A6 File Offset: 0x000754A6
		public MissingSatelliteAssemblyException(string message, string cultureName) : base(message)
		{
			base.SetErrorCode(-2146233034);
			this._cultureName = cultureName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Resources.MissingSatelliteAssemblyException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception. </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception.</param>
		// Token: 0x06001E96 RID: 7830 RVA: 0x000772C1 File Offset: 0x000754C1
		public MissingSatelliteAssemblyException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233034);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Resources.MissingSatelliteAssemblyException" /> class from serialized data. </summary>
		/// <param name="info">The object that holds the serialized object data.</param>
		/// <param name="context">The contextual information about the source or destination of the exception.</param>
		// Token: 0x06001E97 RID: 7831 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected MissingSatelliteAssemblyException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>Gets the name of the default culture. </summary>
		/// <returns>The name of the default culture.</returns>
		// Token: 0x17000426 RID: 1062
		// (get) Token: 0x06001E98 RID: 7832 RVA: 0x000772D6 File Offset: 0x000754D6
		public string CultureName
		{
			get
			{
				return this._cultureName;
			}
		}

		// Token: 0x04001083 RID: 4227
		private string _cultureName;
	}
}
