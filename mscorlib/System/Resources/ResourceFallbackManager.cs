﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;

namespace System.Resources
{
	// Token: 0x02000298 RID: 664
	internal class ResourceFallbackManager : IEnumerable<CultureInfo>, IEnumerable
	{
		// Token: 0x06001E9D RID: 7837 RVA: 0x00077379 File Offset: 0x00075579
		internal ResourceFallbackManager(CultureInfo startingCulture, CultureInfo neutralResourcesCulture, bool useParents)
		{
			if (startingCulture != null)
			{
				this.m_startingCulture = startingCulture;
			}
			else
			{
				this.m_startingCulture = CultureInfo.CurrentUICulture;
			}
			this.m_neutralResourcesCulture = neutralResourcesCulture;
			this.m_useParents = useParents;
		}

		// Token: 0x06001E9E RID: 7838 RVA: 0x000773A6 File Offset: 0x000755A6
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06001E9F RID: 7839 RVA: 0x000773AE File Offset: 0x000755AE
		public IEnumerator<CultureInfo> GetEnumerator()
		{
			bool reachedNeutralResourcesCulture = false;
			CultureInfo currentCulture = this.m_startingCulture;
			while (this.m_neutralResourcesCulture == null || !(currentCulture.Name == this.m_neutralResourcesCulture.Name))
			{
				yield return currentCulture;
				currentCulture = currentCulture.Parent;
				if (!this.m_useParents || currentCulture.HasInvariantCultureName)
				{
					IL_CE:
					if (!this.m_useParents || this.m_startingCulture.HasInvariantCultureName)
					{
						yield break;
					}
					if (reachedNeutralResourcesCulture)
					{
						yield break;
					}
					yield return CultureInfo.InvariantCulture;
					yield break;
				}
			}
			yield return CultureInfo.InvariantCulture;
			reachedNeutralResourcesCulture = true;
			goto IL_CE;
		}

		// Token: 0x04001086 RID: 4230
		private CultureInfo m_startingCulture;

		// Token: 0x04001087 RID: 4231
		private CultureInfo m_neutralResourcesCulture;

		// Token: 0x04001088 RID: 4232
		private bool m_useParents;

		// Token: 0x02000299 RID: 665
		[CompilerGenerated]
		private sealed class <GetEnumerator>d__5 : IEnumerator<CultureInfo>, IDisposable, IEnumerator
		{
			// Token: 0x06001EA0 RID: 7840 RVA: 0x000773BD File Offset: 0x000755BD
			[DebuggerHidden]
			public <GetEnumerator>d__5(int <>1__state)
			{
				this.<>1__state = <>1__state;
			}

			// Token: 0x06001EA1 RID: 7841 RVA: 0x000020D3 File Offset: 0x000002D3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06001EA2 RID: 7842 RVA: 0x000773CC File Offset: 0x000755CC
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				ResourceFallbackManager resourceFallbackManager = this;
				switch (num)
				{
				case 0:
					this.<>1__state = -1;
					reachedNeutralResourcesCulture = false;
					currentCulture = resourceFallbackManager.m_startingCulture;
					break;
				case 1:
					this.<>1__state = -1;
					reachedNeutralResourcesCulture = true;
					goto IL_CE;
				case 2:
					this.<>1__state = -1;
					currentCulture = currentCulture.Parent;
					if (!resourceFallbackManager.m_useParents || currentCulture.HasInvariantCultureName)
					{
						goto IL_CE;
					}
					break;
				case 3:
					this.<>1__state = -1;
					return false;
				default:
					return false;
				}
				if (resourceFallbackManager.m_neutralResourcesCulture != null && currentCulture.Name == resourceFallbackManager.m_neutralResourcesCulture.Name)
				{
					this.<>2__current = CultureInfo.InvariantCulture;
					this.<>1__state = 1;
					return true;
				}
				this.<>2__current = currentCulture;
				this.<>1__state = 2;
				return true;
				IL_CE:
				if (!resourceFallbackManager.m_useParents || resourceFallbackManager.m_startingCulture.HasInvariantCultureName)
				{
					return false;
				}
				if (reachedNeutralResourcesCulture)
				{
					return false;
				}
				this.<>2__current = CultureInfo.InvariantCulture;
				this.<>1__state = 3;
				return true;
			}

			// Token: 0x17000429 RID: 1065
			// (get) Token: 0x06001EA3 RID: 7843 RVA: 0x000774E4 File Offset: 0x000756E4
			CultureInfo IEnumerator<CultureInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06001EA4 RID: 7844 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x1700042A RID: 1066
			// (get) Token: 0x06001EA5 RID: 7845 RVA: 0x000774E4 File Offset: 0x000756E4
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x04001089 RID: 4233
			private int <>1__state;

			// Token: 0x0400108A RID: 4234
			private CultureInfo <>2__current;

			// Token: 0x0400108B RID: 4235
			public ResourceFallbackManager <>4__this;

			// Token: 0x0400108C RID: 4236
			private CultureInfo <currentCulture>5__1;

			// Token: 0x0400108D RID: 4237
			private bool <reachedNeutralResourcesCulture>5__2;
		}
	}
}
