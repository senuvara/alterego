﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies the culture, case, and sort rules to be used by certain overloads of the <see cref="M:System.String.Compare(System.String,System.String)" /> and <see cref="M:System.String.Equals(System.Object)" /> methods.</summary>
	// Token: 0x02000224 RID: 548
	[ComVisible(true)]
	[Serializable]
	public enum StringComparison
	{
		/// <summary>Compare strings using culture-sensitive sort rules and the current culture.</summary>
		// Token: 0x04000CED RID: 3309
		CurrentCulture,
		/// <summary>Compare strings using culture-sensitive sort rules, the current culture, and ignoring the case of the strings being compared.</summary>
		// Token: 0x04000CEE RID: 3310
		CurrentCultureIgnoreCase,
		/// <summary>Compare strings using culture-sensitive sort rules and the invariant culture.</summary>
		// Token: 0x04000CEF RID: 3311
		InvariantCulture,
		/// <summary>Compare strings using culture-sensitive sort rules, the invariant culture, and ignoring the case of the strings being compared.</summary>
		// Token: 0x04000CF0 RID: 3312
		InvariantCultureIgnoreCase,
		/// <summary>Compare strings using ordinal (binary) sort rules.</summary>
		// Token: 0x04000CF1 RID: 3313
		Ordinal,
		/// <summary>Compare strings using ordinal (binary) sort rules and ignoring the case of the strings being compared.</summary>
		// Token: 0x04000CF2 RID: 3314
		OrdinalIgnoreCase
	}
}
