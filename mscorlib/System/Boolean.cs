﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents a Boolean (<see langword="true" /> or <see langword="false" />) value.</summary>
	// Token: 0x02000122 RID: 290
	[ComVisible(true)]
	[Serializable]
	public struct Boolean : IComparable, IConvertible, IComparable<bool>, IEquatable<bool>
	{
		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Boolean" />.</returns>
		// Token: 0x06000AA8 RID: 2728 RVA: 0x00033CF1 File Offset: 0x00031EF1
		public override int GetHashCode()
		{
			if (!this)
			{
				return 0;
			}
			return 1;
		}

		/// <summary>Converts the value of this instance to its equivalent string representation (either "True" or "False").</summary>
		/// <returns>"True" (the value of the <see cref="F:System.Boolean.TrueString" /> property) if the value of this instance is <see langword="true" />, or "False" (the value of the <see cref="F:System.Boolean.FalseString" /> property) if the value of this instance is <see langword="false" />.</returns>
		// Token: 0x06000AA9 RID: 2729 RVA: 0x00033CFA File Offset: 0x00031EFA
		public override string ToString()
		{
			if (!this)
			{
				return "False";
			}
			return "True";
		}

		/// <summary>Converts the value of this instance to its equivalent string representation (either "True" or "False").</summary>
		/// <param name="provider">(Reserved) An <see cref="T:System.IFormatProvider" /> object. </param>
		/// <returns>
		///     <see cref="F:System.Boolean.TrueString" /> if the value of this instance is <see langword="true" />, or <see cref="F:System.Boolean.FalseString" /> if the value of this instance is <see langword="false" />.</returns>
		// Token: 0x06000AAA RID: 2730 RVA: 0x00033CFA File Offset: 0x00031EFA
		public string ToString(IFormatProvider provider)
		{
			if (!this)
			{
				return "False";
			}
			return "True";
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified object.</summary>
		/// <param name="obj">An object to compare to this instance. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is a <see cref="T:System.Boolean" /> and has the same value as this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AAB RID: 2731 RVA: 0x00033D0B File Offset: 0x00031F0B
		public override bool Equals(object obj)
		{
			return obj is bool && this == (bool)obj;
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified <see cref="T:System.Boolean" /> object.</summary>
		/// <param name="obj">A <see cref="T:System.Boolean" /> value to compare to this instance.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> has the same value as this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AAC RID: 2732 RVA: 0x00033D21 File Offset: 0x00031F21
		public bool Equals(bool obj)
		{
			return this == obj;
		}

		/// <summary>Compares this instance to a specified object and returns an integer that indicates their relationship to one another.</summary>
		/// <param name="obj">An object to compare to this instance, or <see langword="null" />. </param>
		/// <returns>A signed integer that indicates the relative order of this instance and <paramref name="obj" />.Return Value Condition Less than zero This instance is <see langword="false" /> and <paramref name="obj" /> is <see langword="true" />. Zero This instance and <paramref name="obj" /> are equal (either both are <see langword="true" /> or both are <see langword="false" />). Greater than zero This instance is <see langword="true" /> and <paramref name="obj" /> is <see langword="false" />.-or- 
		///             <paramref name="obj" /> is <see langword="null" />. </returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="obj" /> is not a <see cref="T:System.Boolean" />. </exception>
		// Token: 0x06000AAD RID: 2733 RVA: 0x00033D28 File Offset: 0x00031F28
		public int CompareTo(object obj)
		{
			if (obj == null)
			{
				return 1;
			}
			if (!(obj is bool))
			{
				throw new ArgumentException(Environment.GetResourceString("Object must be of type Boolean."));
			}
			if (this == (bool)obj)
			{
				return 0;
			}
			if (!this)
			{
				return -1;
			}
			return 1;
		}

		/// <summary>Compares this instance to a specified <see cref="T:System.Boolean" /> object and returns an integer that indicates their relationship to one another.</summary>
		/// <param name="value">A <see cref="T:System.Boolean" /> object to compare to this instance. </param>
		/// <returns>A signed integer that indicates the relative values of this instance and <paramref name="value" />.Return Value Condition Less than zero This instance is <see langword="false" /> and <paramref name="value" /> is <see langword="true" />. Zero This instance and <paramref name="value" /> are equal (either both are <see langword="true" /> or both are <see langword="false" />). Greater than zero This instance is <see langword="true" /> and <paramref name="value" /> is <see langword="false" />. </returns>
		// Token: 0x06000AAE RID: 2734 RVA: 0x00033D5A File Offset: 0x00031F5A
		public int CompareTo(bool value)
		{
			if (this == value)
			{
				return 0;
			}
			if (!this)
			{
				return -1;
			}
			return 1;
		}

		/// <summary>Converts the specified string representation of a logical value to its <see cref="T:System.Boolean" /> equivalent.</summary>
		/// <param name="value">A string containing the value to convert. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="value" /> is equivalent to <see cref="F:System.Boolean.TrueString" />; <see langword="false" /> if <paramref name="value" /> is equivalent to <see cref="F:System.Boolean.FalseString" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="value" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.FormatException">
		///         <paramref name="value" /> is not equivalent to <see cref="F:System.Boolean.TrueString" /> or <see cref="F:System.Boolean.FalseString" />. </exception>
		// Token: 0x06000AAF RID: 2735 RVA: 0x00033D6C File Offset: 0x00031F6C
		public static bool Parse(string value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			bool result = false;
			if (!bool.TryParse(value, out result))
			{
				throw new FormatException(Environment.GetResourceString("String was not recognized as a valid Boolean."));
			}
			return result;
		}

		/// <summary>Tries to convert the specified string representation of a logical value to its <see cref="T:System.Boolean" /> equivalent. A return value indicates whether the conversion succeeded or failed.</summary>
		/// <param name="value">A string containing the value to convert. </param>
		/// <param name="result">When this method returns, if the conversion succeeded, contains <see langword="true" /> if <paramref name="value" /> is equal to <see cref="F:System.Boolean.TrueString" /> or <see langword="false" /> if <paramref name="value" /> is equal to <see cref="F:System.Boolean.FalseString" />. If the conversion failed, contains <see langword="false" />. The conversion fails if <paramref name="value" /> is <see langword="null" /> or is not equal to the value of either the <see cref="F:System.Boolean.TrueString" /> or <see cref="F:System.Boolean.FalseString" /> field.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="value" /> was converted successfully; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000AB0 RID: 2736 RVA: 0x00033DA4 File Offset: 0x00031FA4
		public static bool TryParse(string value, out bool result)
		{
			result = false;
			if (value == null)
			{
				return false;
			}
			if ("True".Equals(value, StringComparison.OrdinalIgnoreCase))
			{
				result = true;
				return true;
			}
			if ("False".Equals(value, StringComparison.OrdinalIgnoreCase))
			{
				result = false;
				return true;
			}
			value = bool.TrimWhiteSpaceAndNull(value);
			if ("True".Equals(value, StringComparison.OrdinalIgnoreCase))
			{
				result = true;
				return true;
			}
			if ("False".Equals(value, StringComparison.OrdinalIgnoreCase))
			{
				result = false;
				return true;
			}
			return false;
		}

		// Token: 0x06000AB1 RID: 2737 RVA: 0x00033E10 File Offset: 0x00032010
		private static string TrimWhiteSpaceAndNull(string value)
		{
			int i = 0;
			int num = value.Length - 1;
			char c = '\0';
			while (i < value.Length)
			{
				if (!char.IsWhiteSpace(value[i]) && value[i] != c)
				{
					IL_52:
					while (num >= i && (char.IsWhiteSpace(value[num]) || value[num] == c))
					{
						num--;
					}
					return value.Substring(i, num - i + 1);
				}
				i++;
			}
			goto IL_52;
		}

		/// <summary>Returns the type code for the <see cref="T:System.Boolean" /> value type. </summary>
		/// <returns>The enumerated constant <see cref="F:System.TypeCode.Boolean" />.</returns>
		// Token: 0x06000AB2 RID: 2738 RVA: 0x00028028 File Offset: 0x00026228
		public TypeCode GetTypeCode()
		{
			return TypeCode.Boolean;
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToBoolean(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>
		///     <see langword="true" /> or <see langword="false" />.</returns>
		// Token: 0x06000AB3 RID: 2739 RVA: 0x00033E7F File Offset: 0x0003207F
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return this;
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <exception cref="T:System.InvalidCastException">You attempt to convert a <see cref="T:System.Boolean" /> value to a <see cref="T:System.Char" /> value. This conversion is not supported.</exception>
		// Token: 0x06000AB4 RID: 2740 RVA: 0x00033E83 File Offset: 0x00032083
		char IConvertible.ToChar(IFormatProvider provider)
		{
			throw new InvalidCastException(Environment.GetResourceString("Invalid cast from '{0}' to '{1}'.", new object[]
			{
				"Boolean",
				"Char"
			}));
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToSByte(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000AB5 RID: 2741 RVA: 0x00033EAA File Offset: 0x000320AA
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToByte(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if the value of this instance is <see langword="true" />; otherwise, 0. </returns>
		// Token: 0x06000AB6 RID: 2742 RVA: 0x00033EB3 File Offset: 0x000320B3
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToInt16(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000AB7 RID: 2743 RVA: 0x00033EBC File Offset: 0x000320BC
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToUInt16(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000AB8 RID: 2744 RVA: 0x00033EC5 File Offset: 0x000320C5
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToInt32(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000AB9 RID: 2745 RVA: 0x00033ECE File Offset: 0x000320CE
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToUInt32(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000ABA RID: 2746 RVA: 0x00033ED7 File Offset: 0x000320D7
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToInt64(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000ABB RID: 2747 RVA: 0x00033EE0 File Offset: 0x000320E0
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToUInt64(System.IFormatProvider)" />. </summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000ABC RID: 2748 RVA: 0x00033EE9 File Offset: 0x000320E9
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToSingle(System.IFormatProvider)" />..</summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000ABD RID: 2749 RVA: 0x00033EF2 File Offset: 0x000320F2
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToDouble(System.IFormatProvider)" />..</summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000ABE RID: 2750 RVA: 0x00033EFB File Offset: 0x000320FB
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToDecimal(System.IFormatProvider)" />..</summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>1 if this instance is <see langword="true" />; otherwise, 0.</returns>
		// Token: 0x06000ABF RID: 2751 RVA: 0x00033F04 File Offset: 0x00032104
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <param name="provider">This parameter is ignored.</param>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <exception cref="T:System.InvalidCastException">You attempt to convert a <see cref="T:System.Boolean" /> value to a <see cref="T:System.DateTime" /> value. This conversion is not supported.</exception>
		// Token: 0x06000AC0 RID: 2752 RVA: 0x00033F0D File Offset: 0x0003210D
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException(Environment.GetResourceString("Invalid cast from '{0}' to '{1}'.", new object[]
			{
				"Boolean",
				"DateTime"
			}));
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToType(System.Type,System.IFormatProvider)" />. </summary>
		/// <param name="type">The desired type. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> implementation that supplies culture-specific information about the format of the returned value.</param>
		/// <returns>An object of the specified type, with a value that is equivalent to the value of this <see langword="Boolean" /> object.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The requested type conversion is not supported. </exception>
		// Token: 0x06000AC1 RID: 2753 RVA: 0x00033F34 File Offset: 0x00032134
		object IConvertible.ToType(Type type, IFormatProvider provider)
		{
			return Convert.DefaultToType(this, type, provider);
		}

		// Token: 0x06000AC2 RID: 2754 RVA: 0x00033F44 File Offset: 0x00032144
		// Note: this type is marked as 'beforefieldinit'.
		static Boolean()
		{
		}

		// Token: 0x04000788 RID: 1928
		private bool m_value;

		// Token: 0x04000789 RID: 1929
		internal const int True = 1;

		// Token: 0x0400078A RID: 1930
		internal const int False = 0;

		// Token: 0x0400078B RID: 1931
		internal const string TrueLiteral = "True";

		// Token: 0x0400078C RID: 1932
		internal const string FalseLiteral = "False";

		/// <summary>Represents the Boolean value <see langword="true" /> as a string. This field is read-only.</summary>
		// Token: 0x0400078D RID: 1933
		public static readonly string TrueString = "True";

		/// <summary>Represents the Boolean value <see langword="false" /> as a string. This field is read-only.</summary>
		// Token: 0x0400078E RID: 1934
		public static readonly string FalseString = "False";
	}
}
