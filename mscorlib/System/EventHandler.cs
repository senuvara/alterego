﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents the method that will handle an event that has no event data.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">An object that contains no event data. </param>
	// Token: 0x0200014A RID: 330
	// (Invoke) Token: 0x06000EBA RID: 3770
	[ComVisible(true)]
	[Serializable]
	public delegate void EventHandler(object sender, EventArgs e);
}
