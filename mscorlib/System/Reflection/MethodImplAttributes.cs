﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies flags for the attributes of a method implementation.</summary>
	// Token: 0x020002E8 RID: 744
	[ComVisible(true)]
	[Serializable]
	public enum MethodImplAttributes
	{
		/// <summary>Specifies flags about code type.</summary>
		// Token: 0x04001242 RID: 4674
		CodeTypeMask = 3,
		/// <summary>Specifies that the method implementation is in Microsoft intermediate language (MSIL).</summary>
		// Token: 0x04001243 RID: 4675
		IL = 0,
		/// <summary>Specifies that the method implementation is native.</summary>
		// Token: 0x04001244 RID: 4676
		Native,
		/// <summary>Specifies that the method implementation is in Optimized Intermediate Language (OPTIL).</summary>
		// Token: 0x04001245 RID: 4677
		OPTIL,
		/// <summary>Specifies that the method implementation is provided by the runtime.</summary>
		// Token: 0x04001246 RID: 4678
		Runtime,
		/// <summary>Specifies whether the method is implemented in managed or unmanaged code.</summary>
		// Token: 0x04001247 RID: 4679
		ManagedMask,
		/// <summary>Specifies that the method is implemented in unmanaged code.</summary>
		// Token: 0x04001248 RID: 4680
		Unmanaged = 4,
		/// <summary>Specifies that the method is implemented in managed code.</summary>
		// Token: 0x04001249 RID: 4681
		Managed = 0,
		/// <summary>Specifies that the method is not defined.</summary>
		// Token: 0x0400124A RID: 4682
		ForwardRef = 16,
		/// <summary>Specifies that the method signature is exported exactly as declared.</summary>
		// Token: 0x0400124B RID: 4683
		PreserveSig = 128,
		/// <summary>Specifies an internal call.</summary>
		// Token: 0x0400124C RID: 4684
		InternalCall = 4096,
		/// <summary>Specifies that the method is single-threaded through the body. Static methods (<see langword="Shared" /> in Visual Basic) lock on the type, whereas instance methods lock on the instance. You can also use the C# lock statement or the Visual Basic SyncLock statement for this purpose. </summary>
		// Token: 0x0400124D RID: 4685
		Synchronized = 32,
		/// <summary>Specifies that the method cannot be inlined.</summary>
		// Token: 0x0400124E RID: 4686
		NoInlining = 8,
		/// <summary>Specifies that the method should be inlined wherever possible.</summary>
		// Token: 0x0400124F RID: 4687
		[ComVisible(false)]
		AggressiveInlining = 256,
		/// <summary>Specifies that the method is not optimized by the just-in-time (JIT) compiler or by native code generation (see Ngen.exe) when debugging possible code generation problems.</summary>
		// Token: 0x04001250 RID: 4688
		NoOptimization = 64,
		/// <summary>Specifies a range check value.</summary>
		// Token: 0x04001251 RID: 4689
		MaxMethodImplVal = 65535
	}
}
