﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies the resource location.</summary>
	// Token: 0x020002DA RID: 730
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum ResourceLocation
	{
		/// <summary>Specifies an embedded (that is, non-linked) resource.</summary>
		// Token: 0x04001194 RID: 4500
		Embedded = 1,
		/// <summary>Specifies that the resource is contained in another assembly.</summary>
		// Token: 0x04001195 RID: 4501
		ContainedInAnotherAssembly = 2,
		/// <summary>Specifies that the resource is contained in the manifest file.</summary>
		// Token: 0x04001196 RID: 4502
		ContainedInManifestFile = 4
	}
}
