﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Represents the class that describes how to marshal a field from managed to unmanaged code. This class cannot be inherited.</summary>
	// Token: 0x02000A89 RID: 2697
	[Obsolete("An alternate API is available: Emit the MarshalAs custom attribute instead. http://go.microsoft.com/fwlink/?linkid=14202")]
	[ComVisible(true)]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	[Serializable]
	public sealed class UnmanagedMarshal
	{
		// Token: 0x06005E8F RID: 24207 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal UnmanagedMarshal()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets an unmanaged base type. This property is read-only.</summary>
		/// <returns>An <see langword="UnmanagedType" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The unmanaged type is not an <see langword="LPArray" /> or a <see langword="SafeArray" />. </exception>
		// Token: 0x17001103 RID: 4355
		// (get) Token: 0x06005E90 RID: 24208 RVA: 0x0013027C File Offset: 0x0012E47C
		public UnmanagedType BaseType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (UnmanagedType)0;
			}
		}

		/// <summary>Gets a number element. This property is read-only.</summary>
		/// <returns>An integer indicating the element count.</returns>
		/// <exception cref="T:System.ArgumentException">The argument is not an unmanaged element count. </exception>
		// Token: 0x17001104 RID: 4356
		// (get) Token: 0x06005E91 RID: 24209 RVA: 0x00130298 File Offset: 0x0012E498
		public int ElementCount
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Indicates an unmanaged type. This property is read-only.</summary>
		/// <returns>An <see cref="T:System.Runtime.InteropServices.UnmanagedType" /> object.</returns>
		// Token: 0x17001105 RID: 4357
		// (get) Token: 0x06005E92 RID: 24210 RVA: 0x001302B4 File Offset: 0x0012E4B4
		public UnmanagedType GetUnmanagedType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return (UnmanagedType)0;
			}
		}

		/// <summary>Gets a GUID. This property is read-only.</summary>
		/// <returns>A <see cref="T:System.Guid" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The argument is not a custom marshaler. </exception>
		// Token: 0x17001106 RID: 4358
		// (get) Token: 0x06005E93 RID: 24211 RVA: 0x001302D0 File Offset: 0x0012E4D0
		public Guid IIDGuid
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(Guid);
			}
		}

		/// <summary>Specifies a fixed-length array (ByValArray) to marshal to unmanaged code.</summary>
		/// <param name="elemCount">The number of elements in the fixed-length array. </param>
		/// <returns>An <see cref="T:System.Reflection.Emit.UnmanagedMarshal" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The argument is not a simple native type. </exception>
		// Token: 0x06005E94 RID: 24212 RVA: 0x0005AB11 File Offset: 0x00058D11
		public static UnmanagedMarshal DefineByValArray(int elemCount)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Specifies a string in a fixed array buffer (ByValTStr) to marshal to unmanaged code.</summary>
		/// <param name="elemCount">The number of elements in the fixed array buffer. </param>
		/// <returns>An <see cref="T:System.Reflection.Emit.UnmanagedMarshal" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The argument is not a simple native type. </exception>
		// Token: 0x06005E95 RID: 24213 RVA: 0x0005AB11 File Offset: 0x00058D11
		public static UnmanagedMarshal DefineByValTStr(int elemCount)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Specifies an <see langword="LPArray" /> to marshal to unmanaged code. The length of an <see langword="LPArray" /> is determined at runtime by the size of the actual marshaled array.</summary>
		/// <param name="elemType">The unmanaged type to which to marshal the array. </param>
		/// <returns>An <see cref="T:System.Reflection.Emit.UnmanagedMarshal" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The argument is not a simple native type. </exception>
		// Token: 0x06005E96 RID: 24214 RVA: 0x0005AB11 File Offset: 0x00058D11
		public static UnmanagedMarshal DefineLPArray(UnmanagedType elemType)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Specifies a <see langword="SafeArray" /> to marshal to unmanaged code.</summary>
		/// <param name="elemType">The base type or the <see langword="UnmanagedType" /> of each element of the array. </param>
		/// <returns>An <see cref="T:System.Reflection.Emit.UnmanagedMarshal" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The argument is not a simple native type. </exception>
		// Token: 0x06005E97 RID: 24215 RVA: 0x0005AB11 File Offset: 0x00058D11
		public static UnmanagedMarshal DefineSafeArray(UnmanagedType elemType)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Specifies a given type that is to be marshaled to unmanaged code.</summary>
		/// <param name="unmanagedType">The unmanaged type to which the type is to be marshaled. </param>
		/// <returns>An <see cref="T:System.Reflection.Emit.UnmanagedMarshal" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">The argument is not a simple native type. </exception>
		// Token: 0x06005E98 RID: 24216 RVA: 0x0005AB11 File Offset: 0x00058D11
		public static UnmanagedMarshal DefineUnmanagedMarshal(UnmanagedType unmanagedType)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
