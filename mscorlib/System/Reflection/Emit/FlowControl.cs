﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	/// <summary>Describes how an instruction alters the flow of control.</summary>
	// Token: 0x02000339 RID: 825
	[ComVisible(true)]
	[Serializable]
	public enum FlowControl
	{
		/// <summary>Branch instruction.</summary>
		// Token: 0x04001347 RID: 4935
		Branch,
		/// <summary>Break instruction.</summary>
		// Token: 0x04001348 RID: 4936
		Break,
		/// <summary>Call instruction.</summary>
		// Token: 0x04001349 RID: 4937
		Call,
		/// <summary>Conditional branch instruction.</summary>
		// Token: 0x0400134A RID: 4938
		Cond_Branch,
		/// <summary>Provides information about a subsequent instruction. For example, the <see langword="Unaligned" /> instruction of <see langword="Reflection.Emit.Opcodes" /> has <see langword="FlowControl.Meta" /> and specifies that the subsequent pointer instruction might be unaligned.</summary>
		// Token: 0x0400134B RID: 4939
		Meta,
		/// <summary>Normal flow of control.</summary>
		// Token: 0x0400134C RID: 4940
		Next,
		/// <summary>This enumerator value is reserved and should not be used.</summary>
		// Token: 0x0400134D RID: 4941
		[Obsolete("This API has been deprecated.")]
		Phi,
		/// <summary>Return instruction.</summary>
		// Token: 0x0400134E RID: 4942
		Return,
		/// <summary>Exception throw instruction.</summary>
		// Token: 0x0400134F RID: 4943
		Throw
	}
}
