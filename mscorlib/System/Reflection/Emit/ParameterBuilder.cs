﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Creates or associates parameter information.</summary>
	// Token: 0x02000347 RID: 839
	public class ParameterBuilder : _ParameterBuilder
	{
		// Token: 0x0600258B RID: 9611 RVA: 0x00002050 File Offset: 0x00000250
		private ParameterBuilder()
		{
		}

		/// <summary>Retrieves the attributes for this parameter.</summary>
		/// <returns>Read-only. Retrieves the attributes for this parameter.</returns>
		// Token: 0x170005CA RID: 1482
		// (get) Token: 0x0600258C RID: 9612 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public int Attributes
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves whether this is an input parameter.</summary>
		/// <returns>Read-only. Retrieves whether this is an input parameter.</returns>
		// Token: 0x170005CB RID: 1483
		// (get) Token: 0x0600258D RID: 9613 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public bool IsIn
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves whether this parameter is optional.</summary>
		/// <returns>Read-only. Specifies whether this parameter is optional.</returns>
		// Token: 0x170005CC RID: 1484
		// (get) Token: 0x0600258E RID: 9614 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public bool IsOptional
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves whether this parameter is an output parameter.</summary>
		/// <returns>Read-only. Retrieves whether this parameter is an output parameter.</returns>
		// Token: 0x170005CD RID: 1485
		// (get) Token: 0x0600258F RID: 9615 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public bool IsOut
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the name of this parameter.</summary>
		/// <returns>Read-only. Retrieves the name of this parameter.</returns>
		// Token: 0x170005CE RID: 1486
		// (get) Token: 0x06002590 RID: 9616 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public string Name
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the signature position for this parameter.</summary>
		/// <returns>Read-only. Retrieves the signature position for this parameter.</returns>
		// Token: 0x170005CF RID: 1487
		// (get) Token: 0x06002591 RID: 9617 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public int Position
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Sets the default value of the parameter.</summary>
		/// <param name="defaultValue">The default value of this parameter. </param>
		/// <exception cref="T:System.ArgumentException">The parameter is not one of the supported types.-or-The type of <paramref name="defaultValue" /> does not match the type of the parameter.-or-The parameter is of type <see cref="T:System.Object" /> or other reference type, <paramref name="defaultValue" /> is not <see langword="null" />, and the value cannot be assigned to the reference type.</exception>
		// Token: 0x06002592 RID: 9618 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void SetConstant(object defaultValue)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class to define the custom attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> is <see langword="null" />. </exception>
		// Token: 0x06002593 RID: 9619 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute using a specified custom attribute blob.</summary>
		/// <param name="con">The constructor for the custom attribute. </param>
		/// <param name="binaryAttribute">A byte blob representing the attributes. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> or <paramref name="binaryAttribute" /> is <see langword="null" />. </exception>
		// Token: 0x06002594 RID: 9620 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Retrieves the token for this parameter.</summary>
		/// <returns>Returns the token for this parameter.</returns>
		// Token: 0x06002595 RID: 9621 RVA: 0x00084CC0 File Offset: 0x00082EC0
		public virtual ParameterToken GetToken()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(ParameterToken);
		}

		/// <summary>Specifies the marshaling for this parameter.</summary>
		/// <param name="unmanagedMarshal">The marshaling information for this parameter. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="unmanagedMarshal" /> is <see langword="null" />. </exception>
		// Token: 0x06002596 RID: 9622 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecuritySafeCritical]
		[Obsolete("An alternate API is available: Emit the MarshalAs custom attribute instead. http://go.microsoft.com/fwlink/?linkid=14202")]
		public virtual void SetMarshal(UnmanagedMarshal unmanagedMarshal)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002597 RID: 9623 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ParameterBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002598 RID: 9624 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ParameterBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002599 RID: 9625 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ParameterBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x0600259A RID: 9626 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ParameterBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
