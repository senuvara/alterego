﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Defines the properties for a type.</summary>
	// Token: 0x02000348 RID: 840
	public abstract class PropertyBuilder : PropertyInfo, _PropertyBuilder
	{
		/// <summary>Gets the attributes for this property.</summary>
		/// <returns>Attributes of this property.</returns>
		// Token: 0x170005D0 RID: 1488
		// (get) Token: 0x0600259B RID: 9627 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override PropertyAttributes Attributes
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether the property can be read.</summary>
		/// <returns>
		///     <see langword="true" /> if this property can be read; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005D1 RID: 1489
		// (get) Token: 0x0600259C RID: 9628 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override bool CanRead
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets a value indicating whether the property can be written to.</summary>
		/// <returns>
		///     <see langword="true" /> if this property can be written to; otherwise, <see langword="false" />.</returns>
		// Token: 0x170005D2 RID: 1490
		// (get) Token: 0x0600259D RID: 9629 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override bool CanWrite
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets the class that declares this member.</summary>
		/// <returns>The <see langword="Type" /> object for the class that declares this member.</returns>
		// Token: 0x170005D3 RID: 1491
		// (get) Token: 0x0600259E RID: 9630 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Type DeclaringType
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets the name of this member.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the name of this member.</returns>
		// Token: 0x170005D4 RID: 1492
		// (get) Token: 0x0600259F RID: 9631 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string Name
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets the type of the field of this property.</summary>
		/// <returns>The type of this property.</returns>
		// Token: 0x170005D5 RID: 1493
		// (get) Token: 0x060025A0 RID: 9632 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Type PropertyType
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Adds one of the other methods associated with this property.</summary>
		/// <param name="mdBuilder">A <see langword="MethodBuilder" /> object that represents the other method. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mdBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x060025A1 RID: 9633 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void AddOtherMethod(MethodBuilder mdBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the default value of this property.</summary>
		/// <param name="defaultValue">The default value of this property. </param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		/// <exception cref="T:System.ArgumentException">The property is not one of the supported types.-or-The type of <paramref name="defaultValue" /> does not match the type of the property.-or-The property is of type <see cref="T:System.Object" /> or other reference type, <paramref name="defaultValue" /> is not <see langword="null" />, and the value cannot be assigned to the reference type.</exception>
		// Token: 0x060025A2 RID: 9634 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetConstant(object defaultValue)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class to define the custom attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="customBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">if <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x060025A3 RID: 9635 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute using a specified custom attribute blob.</summary>
		/// <param name="con">The constructor for the custom attribute. </param>
		/// <param name="binaryAttribute">A byte blob representing the attributes. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> or <paramref name="binaryAttribute" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x060025A4 RID: 9636 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the method that gets the property value.</summary>
		/// <param name="mdBuilder">A <see langword="MethodBuilder" /> object that represents the method that gets the property value. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mdBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x060025A5 RID: 9637 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetGetMethod(MethodBuilder mdBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the method that sets the property value.</summary>
		/// <param name="mdBuilder">A <see langword="MethodBuilder" /> object that represents the method that sets the property value. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mdBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x060025A6 RID: 9638 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetSetMethod(MethodBuilder mdBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns an array of all the index parameters for the property.</summary>
		/// <returns>An array of type <see langword="ParameterInfo" /> containing the parameters for the indexes.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x060025A7 RID: 9639 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override ParameterInfo[] GetIndexParameters()
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x060025A8 RID: 9640 RVA: 0x00081BD0 File Offset: 0x0007FDD0
		protected PropertyBuilder()
		{
		}

		/// <summary>Retrieves the token for this property.</summary>
		/// <returns>Read-only. Retrieves the token for this property.</returns>
		// Token: 0x170005D6 RID: 1494
		// (get) Token: 0x060025A9 RID: 9641 RVA: 0x00084CDC File Offset: 0x00082EDC
		public PropertyToken PropertyToken
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(PropertyToken);
			}
		}

		/// <summary>Gets the class object that was used to obtain this instance of <see langword="MemberInfo" />.</summary>
		/// <returns>The <see langword="Type" /> object through which this <see langword="MemberInfo" /> object was obtained.</returns>
		// Token: 0x170005D7 RID: 1495
		// (get) Token: 0x060025AA RID: 9642 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type ReflectedType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Returns an array of the public and non-public <see langword="get" /> and <see langword="set" /> accessors on this property.</summary>
		/// <param name="nonPublic">Indicates whether non-public methods should be returned in the <see langword="MethodInfo" /> array. <see langword="true" /> if non-public methods are to be included; otherwise, <see langword="false" />. </param>
		/// <returns>An array of type <see langword="MethodInfo" /> containing the matching public or non-public accessors, or an empty array if matching accessors do not exist on this property.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x060025AB RID: 9643 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MethodInfo[] GetAccessors(bool nonPublic)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of all the custom attributes for this property.</summary>
		/// <param name="inherit">If <see langword="true" />, walks up this property's inheritance chain to find the custom attributes </param>
		/// <returns>An array of all the custom attributes.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x060025AC RID: 9644 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object[] GetCustomAttributes(bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of custom attributes identified by <see cref="T:System.Type" />.</summary>
		/// <param name="attributeType">An array of custom attributes identified by type. </param>
		/// <param name="inherit">If <see langword="true" />, walks up this property's inheritance chain to find the custom attributes. </param>
		/// <returns>An array of custom attributes defined on this reflected member, or <see langword="null" /> if no attributes are defined on this member.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x060025AD RID: 9645 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the public and non-public get accessor for this property.</summary>
		/// <param name="nonPublic">Indicates whether non-public get accessors should be returned. <see langword="true" /> if non-public methods are to be included; otherwise, <see langword="false" />. </param>
		/// <returns>A <see langword="MethodInfo" /> object representing the get accessor for this property, if <paramref name="nonPublic" /> is <see langword="true" />. Returns <see langword="null" /> if <paramref name="nonPublic" /> is <see langword="false" /> and the get accessor is non-public, or if <paramref name="nonPublic" /> is <see langword="true" /> but no get accessors exist.</returns>
		// Token: 0x060025AE RID: 9646 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MethodInfo GetGetMethod(bool nonPublic)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the set accessor for this property.</summary>
		/// <param name="nonPublic">Indicates whether the accessor should be returned if it is non-public. <see langword="true" /> if non-public methods are to be included; otherwise, <see langword="false" />. </param>
		/// <returns>The property's <see langword="Set" /> method, or <see langword="null" />, as shown in the following table. Value Condition A <see cref="T:System.Reflection.MethodInfo" /> object representing the Set method for this property. The set accessor is public.
		///             <paramref name="nonPublic" /> is true and non-public methods can be returned. null 
		///             <paramref name="nonPublic" /> is true, but the property is read-only.
		///             <paramref name="nonPublic" /> is false and the set accessor is non-public. </returns>
		// Token: 0x060025AF RID: 9647 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MethodInfo GetSetMethod(bool nonPublic)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Gets the value of a property having the specified binding, index, and <see langword="CultureInfo" />.</summary>
		/// <param name="obj">The object whose property value will be returned. </param>
		/// <param name="invokeAttr">The invocation attribute. This must be a bit flag from <see langword="BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="CreateInstance" />, <see langword="Static" />, <see langword="GetField" />, <see langword="SetField" />, <see langword="GetProperty" />, or <see langword="SetProperty" />. A suitable invocation attribute must be specified. If a static member is to be invoked, the <see langword="Static" /> flag of <see langword="BindingFlags" /> must be set. </param>
		/// <param name="binder">An object that enables the binding, coercion of argument types, invocation of members, and retrieval of <see langword="MemberInfo" /> objects using reflection. If <paramref name="binder" /> is <see langword="null" />, the default binder is used. </param>
		/// <param name="index">Optional index values for indexed properties. This value should be <see langword="null" /> for non-indexed properties. </param>
		/// <param name="culture">The <see langword="CultureInfo" /> object that represents the culture for which the resource is to be localized. Note that if the resource is not localized for this culture, the <see langword="CultureInfo.Parent" /> method will be called successively in search of a match. If this value is <see langword="null" />, the <see langword="CultureInfo" /> is obtained from the <see langword="CultureInfo.CurrentUICulture" /> property. </param>
		/// <returns>The property value for <paramref name="obj" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x060025B0 RID: 9648 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Indicates whether one or more instance of <paramref name="attributeType" /> is defined on this property.</summary>
		/// <param name="attributeType">The <see langword="Type" /> object to which the custom attributes are applied. </param>
		/// <param name="inherit">Specifies whether to walk up this property's inheritance chain to find the custom attributes. </param>
		/// <returns>
		///     <see langword="true" /> if one or more instance of <paramref name="attributeType" /> is defined on this property; otherwise <see langword="false" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x060025B1 RID: 9649 RVA: 0x00084CF8 File Offset: 0x00082EF8
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Sets the property value for the given object to the given value.</summary>
		/// <param name="obj">The object whose property value will be returned. </param>
		/// <param name="value">The new value for this property. </param>
		/// <param name="invokeAttr">The invocation attribute. This must be a bit flag from <see langword="BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="CreateInstance" />, <see langword="Static" />, <see langword="GetField" />, <see langword="SetField" />, <see langword="GetProperty" />, or <see langword="SetProperty" />. A suitable invocation attribute must be specified. If a static member is to be invoked, the <see langword="Static" /> flag of <see langword="BindingFlags" /> must be set. </param>
		/// <param name="binder">An object that enables the binding, coercion of argument types, invocation of members, and retrieval of <see langword="MemberInfo" /> objects using reflection. If <paramref name="binder" /> is <see langword="null" />, the default binder is used. </param>
		/// <param name="index">Optional index values for indexed properties. This value should be <see langword="null" /> for non-indexed properties. </param>
		/// <param name="culture">The <see langword="CultureInfo" /> object that represents the culture for which the resource is to be localized. Note that if the resource is not localized for this culture, the <see langword="CultureInfo.Parent" /> method will be called successively in search of a match. If this value is <see langword="null" />, the <see langword="CultureInfo" /> is obtained from the <see langword="CultureInfo.CurrentUICulture" /> property. </param>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x060025B2 RID: 9650 RVA: 0x00002ABD File Offset: 0x00000CBD
		public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x060025B3 RID: 9651 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _PropertyBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x060025B4 RID: 9652 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _PropertyBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x060025B5 RID: 9653 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _PropertyBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x060025B6 RID: 9654 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _PropertyBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
