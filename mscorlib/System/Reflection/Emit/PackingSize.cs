﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	/// <summary>Specifies one of two factors that determine the memory alignment of fields when a type is marshaled.</summary>
	// Token: 0x02000346 RID: 838
	[ComVisible(true)]
	[Serializable]
	public enum PackingSize
	{
		/// <summary>The packing size is not specified.</summary>
		// Token: 0x04001460 RID: 5216
		Unspecified,
		/// <summary>The packing size is 1 byte.</summary>
		// Token: 0x04001461 RID: 5217
		Size1,
		/// <summary>The packing size is 2 bytes.</summary>
		// Token: 0x04001462 RID: 5218
		Size2,
		/// <summary>The packing size is 4 bytes.</summary>
		// Token: 0x04001463 RID: 5219
		Size4 = 4,
		/// <summary>The packing size is 8 bytes.</summary>
		// Token: 0x04001464 RID: 5220
		Size8 = 8,
		/// <summary>The packing size is 16 bytes.</summary>
		// Token: 0x04001465 RID: 5221
		Size16 = 16,
		/// <summary>The packing size is 32 bytes.</summary>
		// Token: 0x04001466 RID: 5222
		Size32 = 32,
		/// <summary>The packing size is 64 bytes.</summary>
		// Token: 0x04001467 RID: 5223
		Size64 = 64,
		/// <summary>The packing size is 128 bytes.</summary>
		// Token: 0x04001468 RID: 5224
		Size128 = 128
	}
}
