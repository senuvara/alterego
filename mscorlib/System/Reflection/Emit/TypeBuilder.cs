﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Defines and creates new instances of classes during run time.</summary>
	// Token: 0x0200034B RID: 843
	public abstract class TypeBuilder : TypeInfo, _TypeBuilder
	{
		/// <summary>Retrieves the packing size of this type.</summary>
		/// <returns>Read-only. Retrieves the packing size of this type.</returns>
		// Token: 0x170005D8 RID: 1496
		// (get) Token: 0x060025CB RID: 9675 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public PackingSize PackingSize
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the total size of a type.</summary>
		/// <returns>Read-only. Retrieves this type’s total size.</returns>
		// Token: 0x170005D9 RID: 1497
		// (get) Token: 0x060025CC RID: 9676 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public int Size
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the dynamic assembly that contains this type definition.</summary>
		/// <returns>Read-only. Retrieves the dynamic assembly that contains this type definition.</returns>
		// Token: 0x170005DA RID: 1498
		// (get) Token: 0x060025CD RID: 9677 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Assembly Assembly
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Returns the full name of this type qualified by the display name of the assembly.</summary>
		/// <returns>Read-only. The full name of this type qualified by the display name of the assembly.</returns>
		// Token: 0x170005DB RID: 1499
		// (get) Token: 0x060025CE RID: 9678 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string AssemblyQualifiedName
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the base type of this type.</summary>
		/// <returns>Read-only. Retrieves the base type of this type.</returns>
		// Token: 0x170005DC RID: 1500
		// (get) Token: 0x060025CF RID: 9679 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Type BaseType
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the full path of this type.</summary>
		/// <returns>Read-only. Retrieves the full path of this type.</returns>
		// Token: 0x170005DD RID: 1501
		// (get) Token: 0x060025D0 RID: 9680 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string FullName
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the GUID of this type.</summary>
		/// <returns>Read-only. Retrieves the GUID of this type </returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported for incomplete types. </exception>
		// Token: 0x170005DE RID: 1502
		// (get) Token: 0x060025D1 RID: 9681 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Guid GUID
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the dynamic module that contains this type definition.</summary>
		/// <returns>Read-only. Retrieves the dynamic module that contains this type definition.</returns>
		// Token: 0x170005DF RID: 1503
		// (get) Token: 0x060025D2 RID: 9682 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Module Module
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the name of this type.</summary>
		/// <returns>Read-only. Retrieves the <see cref="T:System.String" /> name of this type.</returns>
		// Token: 0x170005E0 RID: 1504
		// (get) Token: 0x060025D3 RID: 9683 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string Name
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the namespace where this <see langword="TypeBuilder" /> is defined.</summary>
		/// <returns>Read-only. Retrieves the namespace where this <see langword="TypeBuilder" /> is defined.</returns>
		// Token: 0x170005E1 RID: 1505
		// (get) Token: 0x060025D4 RID: 9684 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string Namespace
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Adds an interface that this type implements.</summary>
		/// <param name="interfaceType">The interface that this type implements. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="interfaceType" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025D5 RID: 9685 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void AddInterfaceImplementation(Type interfaceType)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Gets a <see cref="T:System.Reflection.TypeInfo" /> object that represents this type.</summary>
		/// <returns>An object that represents this type.</returns>
		// Token: 0x060025D6 RID: 9686 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeInfo CreateTypeInfo()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new constructor to the type, with the given attributes and signature.</summary>
		/// <param name="attributes">The attributes of the constructor. </param>
		/// <param name="callingConvention">The calling convention of the constructor. </param>
		/// <param name="parameterTypes">The parameter types of the constructor. </param>
		/// <returns>The defined constructor.</returns>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025D7 RID: 9687 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public ConstructorBuilder DefineConstructor(MethodAttributes attributes, CallingConventions callingConvention, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new constructor to the type, with the given attributes, signature, and custom modifiers.</summary>
		/// <param name="attributes">The attributes of the constructor. </param>
		/// <param name="callingConvention">The calling convention of the constructor. </param>
		/// <param name="parameterTypes">The parameter types of the constructor. </param>
		/// <param name="requiredCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="optionalCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <returns>The defined constructor.</returns>
		/// <exception cref="T:System.ArgumentException">The size of <paramref name="requiredCustomModifiers" /> or <paramref name="optionalCustomModifiers" /> does not equal the size of <paramref name="parameterTypes" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />.</exception>
		// Token: 0x060025D8 RID: 9688 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public ConstructorBuilder DefineConstructor(MethodAttributes attributes, CallingConventions callingConvention, Type[] parameterTypes, Type[][] requiredCustomModifiers, Type[][] optionalCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines the default constructor. The constructor defined here will simply call the default constructor of the parent.</summary>
		/// <param name="attributes">A <see langword="MethodAttributes" /> object representing the attributes to be applied to the constructor. </param>
		/// <returns>Returns the constructor.</returns>
		/// <exception cref="T:System.NotSupportedException">The parent type (base type) does not have a default constructor. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />.</exception>
		// Token: 0x060025D9 RID: 9689 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public ConstructorBuilder DefineDefaultConstructor(MethodAttributes attributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new event to the type, with the given name, attributes and event type.</summary>
		/// <param name="name">The name of the event. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the event. </param>
		/// <param name="eventtype">The type of the event. </param>
		/// <returns>The defined event.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />.-or- 
		///         <paramref name="eventtype" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025DA RID: 9690 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public EventBuilder DefineEvent(string name, EventAttributes attributes, Type eventtype)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new field to the type, with the given name, attributes, and field type.</summary>
		/// <param name="fieldName">The name of the field. <paramref name="fieldName" /> cannot contain embedded nulls. </param>
		/// <param name="type">The type of the field </param>
		/// <param name="attributes">The attributes of the field. </param>
		/// <returns>The defined field.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="fieldName" /> is zero.-or- 
		///         <paramref name="type" /> is System.Void.-or- A total size was specified for the parent class of this field. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="fieldName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.</exception>
		// Token: 0x060025DB RID: 9691 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public FieldBuilder DefineField(string fieldName, Type type, FieldAttributes attributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new field to the type, with the given name, attributes, field type, and custom modifiers.</summary>
		/// <param name="fieldName">The name of the field. <paramref name="fieldName" /> cannot contain embedded nulls. </param>
		/// <param name="type">The type of the field </param>
		/// <param name="requiredCustomModifiers">An array of types representing the required custom modifiers for the field, such as <see cref="T:Microsoft.VisualC.IsConstModifier" />.</param>
		/// <param name="optionalCustomModifiers">An array of types representing the optional custom modifiers for the field, such as <see cref="T:Microsoft.VisualC.IsConstModifier" />.</param>
		/// <param name="attributes">The attributes of the field. </param>
		/// <returns>The defined field.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="fieldName" /> is zero.-or- 
		///         <paramref name="type" /> is System.Void.-or- A total size was specified for the parent class of this field. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="fieldName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025DC RID: 9692 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public FieldBuilder DefineField(string fieldName, Type type, Type[] requiredCustomModifiers, Type[] optionalCustomModifiers, FieldAttributes attributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines the generic type parameters for the current type, specifying their number and their names, and returns an array of <see cref="T:System.Reflection.Emit.GenericTypeParameterBuilder" /> objects that can be used to set their constraints.</summary>
		/// <param name="names">An array of names for the generic type parameters.</param>
		/// <returns>An array of <see cref="T:System.Reflection.Emit.GenericTypeParameterBuilder" /> objects that can be used to define the constraints of the generic type parameters for the current type.</returns>
		/// <exception cref="T:System.InvalidOperationException">Generic type parameters have already been defined for this type.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="names" /> is <see langword="null" />.-or-An element of <paramref name="names" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="names" /> is an empty array.</exception>
		// Token: 0x060025DD RID: 9693 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public GenericTypeParameterBuilder[] DefineGenericParameters(string[] names)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines initialized data field in the .sdata section of the portable executable (PE) file.</summary>
		/// <param name="name">The name used to refer to the data. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="data">The blob of data. </param>
		/// <param name="attributes">The attributes for the field. </param>
		/// <returns>A field to reference the data.</returns>
		/// <exception cref="T:System.ArgumentException">Length of <paramref name="name" /> is zero.-or- The size of the data is less than or equal to zero, or greater than or equal to 0x3f0000. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="data" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been previously called. </exception>
		// Token: 0x060025DE RID: 9694 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public FieldBuilder DefineInitializedData(string name, byte[] data, FieldAttributes attributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new method to the type, with the specified name and method attributes.</summary>
		/// <param name="name">The name of the method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <returns>A <see cref="T:System.Reflection.Emit.MethodBuilder" /> representing the newly defined method.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero.-or- The type of the parent of this method is an interface, and this method is not virtual (<see langword="Overridable" /> in Visual Basic). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />. </exception>
		// Token: 0x060025DF RID: 9695 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new method to the type, with the specified name, method attributes, and calling convention.</summary>
		/// <param name="name">The name of the method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="callingConvention">The calling convention of the method. </param>
		/// <returns>A <see cref="T:System.Reflection.Emit.MethodBuilder" /> representing the newly defined method.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero.-or- The type of the parent of this method is an interface and this method is not virtual (<see langword="Overridable" /> in Visual Basic). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />. </exception>
		// Token: 0x060025E0 RID: 9696 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes, CallingConventions callingConvention)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new method to the type, with the specified name, method attributes, and method signature.</summary>
		/// <param name="name">The name of the method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="returnType">The return type of the method. </param>
		/// <param name="parameterTypes">The types of the parameters of the method. </param>
		/// <returns>The defined method.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero.-or- The type of the parent of this method is an interface, and this method is not virtual (<see langword="Overridable" /> in Visual Basic). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />. </exception>
		// Token: 0x060025E1 RID: 9697 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new method to the type, with the specified name, method attributes, calling convention, and method signature.</summary>
		/// <param name="name">The name of the method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="callingConvention">The calling convention of the method. </param>
		/// <param name="returnType">The return type of the method. </param>
		/// <param name="parameterTypes">The types of the parameters of the method. </param>
		/// <returns>A <see cref="T:System.Reflection.Emit.MethodBuilder" /> representing the newly defined method.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero.-or- The type of the parent of this method is an interface, and this method is not virtual (<see langword="Overridable" /> in Visual Basic). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />. </exception>
		// Token: 0x060025E2 RID: 9698 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new method to the type, with the specified name, method attributes, calling convention, method signature, and custom modifiers.</summary>
		/// <param name="name">The name of the method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="callingConvention">The calling convention of the method. </param>
		/// <param name="returnType">The return type of the method. </param>
		/// <param name="returnTypeRequiredCustomModifiers">An array of types representing the required custom modifiers, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />, for the return type of the method. If the return type has no required custom modifiers, specify <see langword="null" />.</param>
		/// <param name="returnTypeOptionalCustomModifiers">An array of types representing the optional custom modifiers, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />, for the return type of the method. If the return type has no optional custom modifiers, specify <see langword="null" />.</param>
		/// <param name="parameterTypes">The types of the parameters of the method.</param>
		/// <param name="parameterTypeRequiredCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="parameterTypeOptionalCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <returns>A <see cref="T:System.Reflection.Emit.MethodBuilder" /> object representing the newly added method.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero.-or- The type of the parent of this method is an interface, and this method is not virtual (<see langword="Overridable" /> in Visual Basic). -or-The size of <paramref name="parameterTypeRequiredCustomModifiers" /> or <paramref name="parameterTypeOptionalCustomModifiers" /> does not equal the size of <paramref name="parameterTypes" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />.</exception>
		// Token: 0x060025E3 RID: 9699 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] returnTypeRequiredCustomModifiers, Type[] returnTypeOptionalCustomModifiers, Type[] parameterTypes, Type[][] parameterTypeRequiredCustomModifiers, Type[][] parameterTypeOptionalCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Specifies a given method body that implements a given method declaration, potentially with a different name.</summary>
		/// <param name="methodInfoBody">The method body to be used. This should be a <see langword="MethodBuilder" /> object. </param>
		/// <param name="methodInfoDeclaration">The method whose declaration is to be used. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="methodInfoBody" /> does not belong to this class. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="methodInfoBody" /> or <paramref name="methodInfoDeclaration" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or- 
		///         The declaring type of <paramref name="methodInfoBody" /> is not the type represented by this <see cref="T:System.Reflection.Emit.TypeBuilder" />. </exception>
		// Token: 0x060025E4 RID: 9700 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void DefineMethodOverride(MethodInfo methodInfoBody, MethodInfo methodInfoDeclaration)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a nested type, given its name.</summary>
		/// <param name="name">The short name of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <returns>The defined nested type.</returns>
		/// <exception cref="T:System.ArgumentException">Length of <paramref name="name" /> is zero or greater than 1023. -or-This operation would create a type with a duplicate <see cref="P:System.Reflection.Emit.TypeBuilder.FullName" /> in the current assembly.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060025E5 RID: 9701 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineNestedType(string name)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a nested type, given its name and attributes.</summary>
		/// <param name="name">The short name of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the type. </param>
		/// <returns>The defined nested type.</returns>
		/// <exception cref="T:System.ArgumentException">The nested attribute is not specified.-or- This type is sealed.-or- This type is an array.-or- This type is an interface, but the nested type is not an interface.-or- The length of <paramref name="name" /> is zero or greater than 1023. -or-This operation would create a type with a duplicate <see cref="P:System.Reflection.Emit.TypeBuilder.FullName" /> in the current assembly.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060025E6 RID: 9702 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a nested type, given its name, attributes, and the type that it extends.</summary>
		/// <param name="name">The short name of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the type. </param>
		/// <param name="parent">The type that the nested type extends. </param>
		/// <returns>The defined nested type.</returns>
		/// <exception cref="T:System.ArgumentException">The nested attribute is not specified.-or- This type is sealed.-or- This type is an array.-or- This type is an interface, but the nested type is not an interface.-or- The length of <paramref name="name" /> is zero or greater than 1023. -or-This operation would create a type with a duplicate <see cref="P:System.Reflection.Emit.TypeBuilder.FullName" /> in the current assembly.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060025E7 RID: 9703 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a nested type, given its name, attributes, the total size of the type, and the type that it extends.</summary>
		/// <param name="name">The short name of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the type. </param>
		/// <param name="parent">The type that the nested type extends. </param>
		/// <param name="typeSize">The total size of the type. </param>
		/// <returns>The defined nested type.</returns>
		/// <exception cref="T:System.ArgumentException">The nested attribute is not specified.-or- This type is sealed.-or- This type is an array.-or- This type is an interface, but the nested type is not an interface.-or- The length of <paramref name="name" /> is zero or greater than 1023. -or-This operation would create a type with a duplicate <see cref="P:System.Reflection.Emit.TypeBuilder.FullName" /> in the current assembly.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060025E8 RID: 9704 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent, int typeSize)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a nested type, given its name, attributes, the type that it extends, and the packing size.</summary>
		/// <param name="name">The short name of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the type. </param>
		/// <param name="parent">The type that the nested type extends. </param>
		/// <param name="packSize">The packing size of the type. </param>
		/// <returns>The defined nested type.</returns>
		/// <exception cref="T:System.ArgumentException">The nested attribute is not specified.-or- This type is sealed.-or- This type is an array.-or- This type is an interface, but the nested type is not an interface.-or- The length of <paramref name="name" /> is zero or greater than 1023. -or-This operation would create a type with a duplicate <see cref="P:System.Reflection.Emit.TypeBuilder.FullName" /> in the current assembly.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060025E9 RID: 9705 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent, PackingSize packSize)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a nested type, given its name, attributes, the type that it extends, and the interfaces that it implements.</summary>
		/// <param name="name">The short name of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the type. </param>
		/// <param name="parent">The type that the nested type extends. </param>
		/// <param name="interfaces">The interfaces that the nested type implements. </param>
		/// <returns>The defined nested type.</returns>
		/// <exception cref="T:System.ArgumentException">The nested attribute is not specified.-or- This type is sealed.-or- This type is an array.-or- This type is an interface, but the nested type is not an interface.-or- The length of <paramref name="name" /> is zero or greater than 1023. -or-This operation would create a type with a duplicate <see cref="P:System.Reflection.Emit.TypeBuilder.FullName" /> in the current assembly.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />.-or-An element of the <paramref name="interfaces" /> array is <see langword="null" />.</exception>
		// Token: 0x060025EA RID: 9706 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent, Type[] interfaces)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a nested type, given its name, attributes, size, and the type that it extends.</summary>
		/// <param name="name">The short name of the type. <paramref name="name" /> cannot contain embedded null values.</param>
		/// <param name="attr">The attributes of the type.</param>
		/// <param name="parent">The type that the nested type extends.</param>
		/// <param name="packSize">The packing size of the type.</param>
		/// <param name="typeSize">The total size of the type.</param>
		/// <returns>The defined nested type.</returns>
		// Token: 0x060025EB RID: 9707 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent, PackingSize packSize, int typeSize)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new property to the type, with the given name and property signature.</summary>
		/// <param name="name">The name of the property. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the property. </param>
		/// <param name="returnType">The return type of the property. </param>
		/// <param name="parameterTypes">The types of the parameters of the property. </param>
		/// <returns>The defined property.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. -or- Any of the elements of the <paramref name="parameterTypes" /> array is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025EC RID: 9708 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public PropertyBuilder DefineProperty(string name, PropertyAttributes attributes, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new property to the type, with the given name, attributes, calling convention, and property signature.</summary>
		/// <param name="name">The name of the property. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the property. </param>
		/// <param name="callingConvention">The calling convention of the property accessors. </param>
		/// <param name="returnType">The return type of the property. </param>
		/// <param name="parameterTypes">The types of the parameters of the property.</param>
		/// <returns>The defined property.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. -or- Any of the elements of the <paramref name="parameterTypes" /> array is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025ED RID: 9709 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public PropertyBuilder DefineProperty(string name, PropertyAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new property to the type, with the given name, property signature, and custom modifiers.</summary>
		/// <param name="name">The name of the property. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the property. </param>
		/// <param name="returnType">The return type of the property. </param>
		/// <param name="returnTypeRequiredCustomModifiers">An array of types representing the required custom modifiers, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />, for the return type of the property. If the return type has no required custom modifiers, specify <see langword="null" />.</param>
		/// <param name="returnTypeOptionalCustomModifiers">An array of types representing the optional custom modifiers, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />, for the return type of the property. If the return type has no optional custom modifiers, specify <see langword="null" />.</param>
		/// <param name="parameterTypes">The types of the parameters of the property. </param>
		/// <param name="parameterTypeRequiredCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="parameterTypeOptionalCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <returns>The defined property.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />-or- Any of the elements of the <paramref name="parameterTypes" /> array is <see langword="null" /></exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025EE RID: 9710 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public PropertyBuilder DefineProperty(string name, PropertyAttributes attributes, Type returnType, Type[] returnTypeRequiredCustomModifiers, Type[] returnTypeOptionalCustomModifiers, Type[] parameterTypes, Type[][] parameterTypeRequiredCustomModifiers, Type[][] parameterTypeOptionalCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a new property to the type, with the given name, calling convention, property signature, and custom modifiers.</summary>
		/// <param name="name">The name of the property. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the property. </param>
		/// <param name="callingConvention">The calling convention of the property accessors. </param>
		/// <param name="returnType">The return type of the property. </param>
		/// <param name="returnTypeRequiredCustomModifiers">An array of types representing the required custom modifiers, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />, for the return type of the property. If the return type has no required custom modifiers, specify <see langword="null" />.</param>
		/// <param name="returnTypeOptionalCustomModifiers">An array of types representing the optional custom modifiers, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />, for the return type of the property. If the return type has no optional custom modifiers, specify <see langword="null" />.</param>
		/// <param name="parameterTypes">The types of the parameters of the property. </param>
		/// <param name="parameterTypeRequiredCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="parameterTypeOptionalCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <returns>The defined property.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. -or- Any of the elements of the <paramref name="parameterTypes" /> array is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025EF RID: 9711 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public PropertyBuilder DefineProperty(string name, PropertyAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] returnTypeRequiredCustomModifiers, Type[] returnTypeOptionalCustomModifiers, Type[] parameterTypes, Type[][] parameterTypeRequiredCustomModifiers, Type[][] parameterTypeOptionalCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines the initializer for this type.</summary>
		/// <returns>Returns a type initializer.</returns>
		/// <exception cref="T:System.InvalidOperationException">The containing type has been previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025F0 RID: 9712 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public ConstructorBuilder DefineTypeInitializer()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines an uninitialized data field in the <see langword=".sdata" /> section of the portable executable (PE) file.</summary>
		/// <param name="name">The name used to refer to the data. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="size">The size of the data field. </param>
		/// <param name="attributes">The attributes for the field. </param>
		/// <returns>A field to reference the data.</returns>
		/// <exception cref="T:System.ArgumentException">Length of <paramref name="name" /> is zero.-or- 
		///         <paramref name="size" /> is less than or equal to zero, or greater than or equal to 0x003f0000. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025F1 RID: 9713 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public FieldBuilder DefineUninitializedData(string name, int size, FieldAttributes attributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns the constructor of the specified constructed generic type that corresponds to the specified constructor of the generic type definition. </summary>
		/// <param name="type">The constructed generic type whose constructor is returned.</param>
		/// <param name="constructor">A constructor on the generic type definition of <paramref name="type" />, which specifies which constructor of <paramref name="type" /> to return.</param>
		/// <returns>A <see cref="T:System.Reflection.ConstructorInfo" /> object that represents the constructor of <paramref name="type" /> corresponding to <paramref name="constructor" />, which specifies a constructor belonging to the generic type definition of <paramref name="type" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="type" /> does not represent a generic type. -or-
		///         <paramref name="type" /> is not of type <see cref="T:System.Reflection.Emit.TypeBuilder" />.-or-The declaring type of <paramref name="constructor" /> is not a generic type definition. -or-The declaring type of <paramref name="constructor" /> is not the generic type definition of <paramref name="type" />.</exception>
		// Token: 0x060025F2 RID: 9714 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static ConstructorInfo GetConstructor(Type type, ConstructorInfo constructor)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns the field of the specified constructed generic type that corresponds to the specified field of the generic type definition. </summary>
		/// <param name="type">The constructed generic type whose field is returned.</param>
		/// <param name="field">A field on the generic type definition of <paramref name="type" />, which specifies which field of <paramref name="type" /> to return.</param>
		/// <returns>A <see cref="T:System.Reflection.FieldInfo" /> object that represents the field of <paramref name="type" /> corresponding to <paramref name="field" />, which specifies a field belonging to the generic type definition of <paramref name="type" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="type" /> does not represent a generic type. -or-
		///         <paramref name="type" /> is not of type <see cref="T:System.Reflection.Emit.TypeBuilder" />.-or-The declaring type of <paramref name="field" /> is not a generic type definition. -or-The declaring type of <paramref name="field" /> is not the generic type definition of <paramref name="type" />.</exception>
		// Token: 0x060025F3 RID: 9715 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static FieldInfo GetField(Type type, FieldInfo field)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns the method of the specified constructed generic type that corresponds to the specified method of the generic type definition. </summary>
		/// <param name="type">The constructed generic type whose method is returned.</param>
		/// <param name="method">A method on the generic type definition of <paramref name="type" />, which specifies which method of <paramref name="type" /> to return.</param>
		/// <returns>A <see cref="T:System.Reflection.MethodInfo" /> object that represents the method of <paramref name="type" /> corresponding to <paramref name="method" />, which specifies a method belonging to the generic type definition of <paramref name="type" />.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="method" /> is a generic method that is not a generic method definition.-or-
		///         <paramref name="type" /> does not represent a generic type.-or-
		///         <paramref name="type" /> is not of type <see cref="T:System.Reflection.Emit.TypeBuilder" />.-or-The declaring type of <paramref name="method" /> is not a generic type definition. -or-The declaring type of <paramref name="method" /> is not the generic type definition of <paramref name="type" />.</exception>
		// Token: 0x060025F4 RID: 9716 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static MethodInfo GetMethod(Type type, MethodInfo method)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a value that indicates whether the current dynamic type has been created.</summary>
		/// <returns>
		///     <see langword="true" /> if the <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> method has been called; otherwise, <see langword="false" />. </returns>
		// Token: 0x060025F5 RID: 9717 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public bool IsCreated()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class to define the custom attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="customBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />.</exception>
		// Token: 0x060025F6 RID: 9718 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets a custom attribute using a specified custom attribute blob.</summary>
		/// <param name="con">The constructor for the custom attribute. </param>
		/// <param name="binaryAttribute">A byte blob representing the attributes. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> or <paramref name="binaryAttribute" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />.</exception>
		// Token: 0x060025F7 RID: 9719 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the base type of the type currently under construction.</summary>
		/// <param name="parent">The new base type. </param>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-
		///         <paramref name="parent" /> is <see langword="null" />, and the current instance represents an interface whose attributes do not include <see cref="F:System.Reflection.TypeAttributes.Abstract" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="parent" /> is an interface. This exception condition is new in the .NET Framework version 2.0. </exception>
		// Token: 0x060025F8 RID: 9720 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetParent(Type parent)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Calling this method always throws <see cref="T:System.NotSupportedException" />.</summary>
		/// <returns>This method is not supported. No value is returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x060025F9 RID: 9721 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Type GetElementType()
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x060025FA RID: 9722 RVA: 0x0007DAC7 File Offset: 0x0007BCC7
		protected TypeBuilder()
		{
		}

		/// <summary>Returns the type token of this type.</summary>
		/// <returns>Read-only. Returns the <see langword="TypeToken" /> of this type.</returns>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.</exception>
		// Token: 0x170005E2 RID: 1506
		// (get) Token: 0x060025FB RID: 9723 RVA: 0x00084D14 File Offset: 0x00082F14
		public TypeToken TypeToken
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TypeToken);
			}
		}

		/// <summary>Returns the underlying system type for this <see langword="TypeBuilder" />.</summary>
		/// <returns>Read-only. Returns the underlying system type.</returns>
		/// <exception cref="T:System.InvalidOperationException">This type is an enumeration, but there is no underlying system type. </exception>
		// Token: 0x170005E3 RID: 1507
		// (get) Token: 0x060025FC RID: 9724 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type UnderlyingSystemType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds declarative security to this type.</summary>
		/// <param name="action">The security action to be taken such as Demand, Assert, and so on. </param>
		/// <param name="pset">The set of permissions the action applies to. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="action" /> is invalid (<see langword="RequestMinimum" />, <see langword="RequestOptional" />, and <see langword="RequestRefuse" /> are invalid). </exception>
		/// <exception cref="T:System.InvalidOperationException">The containing type has been created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or- The permission set <paramref name="pset" /> contains an action that was added earlier by <see langword="AddDeclarativeSecurity" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="pset" /> is <see langword="null" />. </exception>
		// Token: 0x060025FD RID: 9725 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecuritySafeCritical]
		public void AddDeclarativeSecurity(SecurityAction action, PermissionSet pset)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Creates a <see cref="T:System.Type" /> object for the class. After defining fields and methods on the class, <see langword="CreateType" /> is called in order to load its <see langword="Type" /> object.</summary>
		/// <returns>Returns the new <see cref="T:System.Type" /> object for this class.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enclosing type has not been created.-or- This type is non-abstract and contains an abstract method.-or- 
		///         This type is not an abstract class or an interface and has a method without a method body. </exception>
		/// <exception cref="T:System.NotSupportedException">The type contains invalid Microsoft intermediate language (MSIL) code.-or- The branch target is specified using a 1-byte offset, but the target is at a distance greater than 127 bytes from the branch. </exception>
		/// <exception cref="T:System.TypeLoadException">The type cannot be loaded. For example, it contains a <see langword="static" /> method that has the calling convention <see cref="F:System.Reflection.CallingConventions.HasThis" />.</exception>
		// Token: 0x060025FE RID: 9726 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public Type CreateType()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a <see langword="PInvoke" /> method given its name, the name of the DLL in which the method is defined, the attributes of the method, the calling convention of the method, the return type of the method, the types of the parameters of the method, and the <see langword="PInvoke" /> flags.</summary>
		/// <param name="name">The name of the <see langword="PInvoke" /> method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="dllName">The name of the DLL in which the <see langword="PInvoke" /> method is defined. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="callingConvention">The method's calling convention. </param>
		/// <param name="returnType">The method's return type. </param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <param name="nativeCallConv">The native calling convention. </param>
		/// <param name="nativeCharSet">The method's native character set. </param>
		/// <returns>The defined <see langword="PInvoke" /> method.</returns>
		/// <exception cref="T:System.ArgumentException">The method is not static.-or- The parent type is an interface.-or- The method is abstract.-or- The method was previously defined.-or- The length of <paramref name="name" /> or <paramref name="dllName" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="dllName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The containing type has been previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x060025FF RID: 9727 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a <see langword="PInvoke" /> method given its name, the name of the DLL in which the method is defined, the name of the entry point, the attributes of the method, the calling convention of the method, the return type of the method, the types of the parameters of the method, and the <see langword="PInvoke" /> flags.</summary>
		/// <param name="name">The name of the <see langword="PInvoke" /> method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="dllName">The name of the DLL in which the <see langword="PInvoke" /> method is defined. </param>
		/// <param name="entryName">The name of the entry point in the DLL. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="callingConvention">The method's calling convention. </param>
		/// <param name="returnType">The method's return type. </param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <param name="nativeCallConv">The native calling convention. </param>
		/// <param name="nativeCharSet">The method's native character set. </param>
		/// <returns>The defined <see langword="PInvoke" /> method.</returns>
		/// <exception cref="T:System.ArgumentException">The method is not static.-or- The parent type is an interface.-or- The method is abstract.-or- The method was previously defined.-or- The length of <paramref name="name" />, <paramref name="dllName" />, or <paramref name="entryName" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" />, <paramref name="dllName" />, or <paramref name="entryName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The containing type has been previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />. </exception>
		// Token: 0x06002600 RID: 9728 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, string entryName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a <see langword="PInvoke" /> method given its name, the name of the DLL in which the method is defined, the name of the entry point, the attributes of the method, the calling convention of the method, the return type of the method, the types of the parameters of the method, the <see langword="PInvoke" /> flags, and custom modifiers for the parameters and return type.</summary>
		/// <param name="name">The name of the <see langword="PInvoke" /> method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="dllName">The name of the DLL in which the <see langword="PInvoke" /> method is defined. </param>
		/// <param name="entryName">The name of the entry point in the DLL. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="callingConvention">The method's calling convention. </param>
		/// <param name="returnType">The method's return type. </param>
		/// <param name="returnTypeRequiredCustomModifiers">An array of types representing the required custom modifiers, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />, for the return type of the method. If the return type has no required custom modifiers, specify <see langword="null" />.</param>
		/// <param name="returnTypeOptionalCustomModifiers">An array of types representing the optional custom modifiers, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />, for the return type of the method. If the return type has no optional custom modifiers, specify <see langword="null" />.</param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <param name="parameterTypeRequiredCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="parameterTypeOptionalCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" />. If a particular parameter has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the parameters have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="nativeCallConv">The native calling convention. </param>
		/// <param name="nativeCharSet">The method's native character set. </param>
		/// <returns>A <see cref="T:System.Reflection.Emit.MethodBuilder" /> representing the defined <see langword="PInvoke" /> method.</returns>
		/// <exception cref="T:System.ArgumentException">The method is not static.-or- The parent type is an interface.-or- The method is abstract.-or- The method was previously defined.-or- The length of <paramref name="name" />, <paramref name="dllName" />, or <paramref name="entryName" /> is zero. -or-The size of <paramref name="parameterTypeRequiredCustomModifiers" /> or <paramref name="parameterTypeOptionalCustomModifiers" /> does not equal the size of <paramref name="parameterTypes" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" />, <paramref name="dllName" />, or <paramref name="entryName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The type was previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" />.-or-For the current dynamic type, the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericType" /> property is <see langword="true" />, but the <see cref="P:System.Reflection.Emit.TypeBuilder.IsGenericTypeDefinition" /> property is <see langword="false" />.</exception>
		// Token: 0x06002601 RID: 9729 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, string entryName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] returnTypeRequiredCustomModifiers, Type[] returnTypeOptionalCustomModifiers, Type[] parameterTypes, Type[][] parameterTypeRequiredCustomModifiers, Type[][] parameterTypeOptionalCustomModifiers, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002602 RID: 9730 RVA: 0x00084D30 File Offset: 0x00082F30
		protected override TypeAttributes GetAttributeFlagsImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return TypeAttributes.NotPublic;
		}

		// Token: 0x06002603 RID: 9731 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of <see cref="T:System.Reflection.ConstructorInfo" /> objects representing the public and non-public constructors defined for this class, as specified.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> as in <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.ConstructorInfo" /> objects representing the specified constructors defined for this class. If no constructors are defined, an empty array is returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x06002604 RID: 9732 RVA: 0x0005AB11 File Offset: 0x00058D11
		[ComVisible(true)]
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns all the custom attributes defined for this type.</summary>
		/// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes. </param>
		/// <returns>Returns an array of objects representing all the custom attributes of this type.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported for incomplete types. Retrieve the type using <see cref="M:System.Type.GetType" /> and call <see cref="M:System.Reflection.MemberInfo.GetCustomAttributes(System.Boolean)" /> on the returned <see cref="T:System.Type" />. </exception>
		// Token: 0x06002605 RID: 9733 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public override object[] GetCustomAttributes(bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns all the custom attributes of the current type that are assignable to a specified type.</summary>
		/// <param name="attributeType">The type of attribute to search for. Only attributes that are assignable to this type are returned.</param>
		/// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes. </param>
		/// <returns>An array of custom attributes defined on the current type.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported for incomplete types. Retrieve the type using <see cref="M:System.Type.GetType" /> and call <see cref="M:System.Reflection.MemberInfo.GetCustomAttributes(System.Boolean)" /> on the returned <see cref="T:System.Type" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="attributeType" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The type must be a type provided by the underlying runtime system.</exception>
		// Token: 0x06002606 RID: 9734 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the event with the specified name.</summary>
		/// <param name="name">The name of the event to search for. </param>
		/// <param name="bindingAttr">A bitwise combination of <see cref="T:System.Reflection.BindingFlags" /> values that limits the search. </param>
		/// <returns>An <see cref="T:System.Reflection.EventInfo" /> object representing the event declared or inherited by this type with the specified name, or <see langword="null" /> if there are no matches.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x06002607 RID: 9735 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the public and non-public events that are declared by this type.</summary>
		/// <param name="bindingAttr">A bitwise combination of <see cref="T:System.Reflection.BindingFlags" /> values that limits the search.</param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.EventInfo" /> objects representing the events declared or inherited by this type that match the specified binding flags. An empty array is returned if there are no matching events.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x06002608 RID: 9736 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override EventInfo[] GetEvents(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the field specified by the given name.</summary>
		/// <param name="name">The name of the field to get. </param>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> as in <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns the <see cref="T:System.Reflection.FieldInfo" /> object representing the field declared or inherited by this type with the specified name and public or non-public modifier. If there are no matches then <see langword="null" /> is returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x06002609 RID: 9737 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the public and non-public fields that are declared by this type.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.FieldInfo" /> objects representing the public and non-public fields declared or inherited by this type. An empty array is returned if there are no fields, as specified.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x0600260A RID: 9738 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override FieldInfo[] GetFields(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the interface implemented (directly or indirectly) by this class with the fully qualified name matching the given interface name.</summary>
		/// <param name="name">The name of the interface. </param>
		/// <param name="ignoreCase">If <see langword="true" />, the search is case-insensitive. If <see langword="false" />, the search is case-sensitive. </param>
		/// <returns>Returns a <see cref="T:System.Type" /> object representing the implemented interface. Returns null if no interface matching name is found.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x0600260B RID: 9739 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type GetInterface(string name, bool ignoreCase)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of all the interfaces implemented on this type and its base types.</summary>
		/// <returns>Returns an array of <see cref="T:System.Type" /> objects representing the implemented interfaces. If none are defined, an empty array is returned.</returns>
		// Token: 0x0600260C RID: 9740 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type[] GetInterfaces()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the members for the public and non-public members declared or inherited by this type.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" />, such as <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.MemberInfo" /> objects representing the public and non-public members declared or inherited by this type. An empty array is returned if there are no matching members.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x0600260D RID: 9741 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600260E RID: 9742 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns all the public and non-public methods declared or inherited by this type, as specified.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> as in <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.MethodInfo" /> objects representing the public and non-public methods defined on this type if <paramref name="nonPublic" /> is used; otherwise, only the public methods are returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x0600260F RID: 9743 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the public and non-public nested types that are declared by this type.</summary>
		/// <param name="name">The <see cref="T:System.String" /> containing the name of the nested type to get. </param>
		/// <param name="bindingAttr">A bitmask comprised of one or more <see cref="T:System.Reflection.BindingFlags" /> that specify how the search is conducted.-or- Zero, to conduct a case-sensitive search for public methods. </param>
		/// <returns>A <see cref="T:System.Type" /> object representing the nested type that matches the specified requirements, if found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x06002610 RID: 9744 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type GetNestedType(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the public and non-public nested types that are declared or inherited by this type.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" />, as in <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>An array of <see cref="T:System.Type" /> objects representing all the types nested within the current <see cref="T:System.Type" /> that match the specified binding constraints.An empty array of type <see cref="T:System.Type" />, if no types are nested within the current <see cref="T:System.Type" />, or if none of the nested types match the binding constraints.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x06002611 RID: 9745 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type[] GetNestedTypes(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns all the public and non-public properties declared or inherited by this type, as specified.</summary>
		/// <param name="bindingAttr">This invocation attribute. This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see langword="PropertyInfo" /> objects representing the public and non-public properties defined on this type if <paramref name="nonPublic" /> is used; otherwise, only the public properties are returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented for incomplete types. </exception>
		// Token: 0x06002612 RID: 9746 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002613 RID: 9747 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002614 RID: 9748 RVA: 0x00084D4C File Offset: 0x00082F4C
		protected override bool HasElementTypeImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Invokes the specified member. The method that is to be invoked must be accessible and provide the most specific match with the specified argument list, under the constraints of the specified binder and invocation attributes.</summary>
		/// <param name="name">The name of the member to invoke. This can be a constructor, method, property, or field. A suitable invocation attribute must be specified. Note that it is possible to invoke the default member of a class by passing an empty string as the name of the member. </param>
		/// <param name="invokeAttr">The invocation attribute. This must be a bit flag from <see langword="BindingFlags" />. </param>
		/// <param name="binder">An object that enables the binding, coercion of argument types, invocation of members, and retrieval of <see langword="MemberInfo" /> objects using reflection. If binder is <see langword="null" />, the default binder is used. See <see cref="T:System.Reflection.Binder" />. </param>
		/// <param name="target">The object on which to invoke the specified member. If the member is static, this parameter is ignored. </param>
		/// <param name="args">An argument list. This is an array of Objects that contains the number, order, and type of the parameters of the member to be invoked. If there are no parameters this should be null. </param>
		/// <param name="modifiers">An array of the same length as <paramref name="args" /> with elements that represent the attributes associated with the arguments of the member to be invoked. A parameter has attributes associated with it in the metadata. They are used by various interoperability services. See the metadata specs for more details. </param>
		/// <param name="culture">An instance of <see langword="CultureInfo" /> used to govern the coercion of types. If this is null, the <see langword="CultureInfo" /> for the current thread is used. (Note that this is necessary to, for example, convert a String that represents 1000 to a Double value, since 1000 is represented differently by different cultures.) </param>
		/// <param name="namedParameters">Each parameter in the <paramref name="namedParameters" /> array gets the value in the corresponding element in the <paramref name="args" /> array. If the length of <paramref name="args" /> is greater than the length of <paramref name="namedParameters" />, the remaining argument values are passed in order. </param>
		/// <returns>Returns the return value of the invoked member.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported for incomplete types. </exception>
		// Token: 0x06002615 RID: 9749 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002616 RID: 9750 RVA: 0x00084D68 File Offset: 0x00082F68
		protected override bool IsArrayImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06002617 RID: 9751 RVA: 0x00084D84 File Offset: 0x00082F84
		protected override bool IsByRefImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06002618 RID: 9752 RVA: 0x00084DA0 File Offset: 0x00082FA0
		protected override bool IsCOMObjectImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Determines whether a custom attribute is applied to the current type.</summary>
		/// <param name="attributeType">The type of attribute to search for. Only attributes that are assignable to this type are returned. </param>
		/// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes. </param>
		/// <returns>
		///     <see langword="true" /> if one or more instances of <paramref name="attributeType" />, or an attribute derived from <paramref name="attributeType" />, is defined on this type; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported for incomplete types. Retrieve the type using <see cref="M:System.Type.GetType" /> and call <see cref="M:System.Reflection.MemberInfo.IsDefined(System.Type,System.Boolean)" /> on the returned <see cref="T:System.Type" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="attributeType" /> is not defined.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="attributeType" /> is <see langword="null" />.</exception>
		// Token: 0x06002619 RID: 9753 RVA: 0x00084DBC File Offset: 0x00082FBC
		[SecuritySafeCritical]
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x0600261A RID: 9754 RVA: 0x00084DD8 File Offset: 0x00082FD8
		protected override bool IsPointerImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x0600261B RID: 9755 RVA: 0x00084DF4 File Offset: 0x00082FF4
		protected override bool IsPrimitiveImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x0600261C RID: 9756 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _TypeBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x0600261D RID: 9757 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _TypeBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x0600261E RID: 9758 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _TypeBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x0600261F RID: 9759 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _TypeBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Represents that total size for the type is not specified.</summary>
		// Token: 0x04001487 RID: 5255
		public const int UnspecifiedTypeSize = 0;
	}
}
