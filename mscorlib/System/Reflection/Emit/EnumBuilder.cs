﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Describes and represents an enumeration type.</summary>
	// Token: 0x02000336 RID: 822
	public abstract class EnumBuilder : TypeInfo, _EnumBuilder
	{
		/// <summary>Returns the underlying field for this enum.</summary>
		/// <returns>Read-only. The underlying field for this enum.</returns>
		// Token: 0x17000599 RID: 1433
		// (get) Token: 0x06002469 RID: 9321 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public FieldBuilder UnderlyingField
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the dynamic assembly that contains this enum definition.</summary>
		/// <returns>Read-only. The dynamic assembly that contains this enum definition.</returns>
		// Token: 0x1700059A RID: 1434
		// (get) Token: 0x0600246A RID: 9322 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Assembly Assembly
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Returns the full path of this enum qualified by the display name of the parent assembly.</summary>
		/// <returns>Read-only. The full path of this enum qualified by the display name of the parent assembly.</returns>
		// Token: 0x1700059B RID: 1435
		// (get) Token: 0x0600246B RID: 9323 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string AssemblyQualifiedName
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Returns the parent <see cref="T:System.Type" /> of this type which is always <see cref="T:System.Enum" />.</summary>
		/// <returns>Read-only. The parent <see cref="T:System.Type" /> of this type.</returns>
		// Token: 0x1700059C RID: 1436
		// (get) Token: 0x0600246C RID: 9324 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Type BaseType
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Returns the full path of this enum.</summary>
		/// <returns>Read-only. The full path of this enum.</returns>
		// Token: 0x1700059D RID: 1437
		// (get) Token: 0x0600246D RID: 9325 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string FullName
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Returns the GUID of this enum.</summary>
		/// <returns>Read-only. The GUID of this enum.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x1700059E RID: 1438
		// (get) Token: 0x0600246E RID: 9326 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Guid GUID
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Retrieves the dynamic module that contains this <see cref="T:System.Reflection.Emit.EnumBuilder" /> definition.</summary>
		/// <returns>Read-only. The dynamic module that contains this <see cref="T:System.Reflection.Emit.EnumBuilder" /> definition.</returns>
		// Token: 0x1700059F RID: 1439
		// (get) Token: 0x0600246F RID: 9327 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Module Module
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Returns the name of this enum.</summary>
		/// <returns>Read-only. The name of this enum.</returns>
		// Token: 0x170005A0 RID: 1440
		// (get) Token: 0x06002470 RID: 9328 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string Name
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Returns the namespace of this enum.</summary>
		/// <returns>Read-only. The namespace of this enum.</returns>
		// Token: 0x170005A1 RID: 1441
		// (get) Token: 0x06002471 RID: 9329 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string Namespace
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets a <see cref="T:System.Reflection.TypeInfo" /> object that represents this enumeration.</summary>
		/// <returns>An object that represents this enumeration.</returns>
		// Token: 0x06002472 RID: 9330 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeInfo CreateTypeInfo()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines the named static field in an enumeration type with the specified constant value.</summary>
		/// <param name="literalName">The name of the static field. </param>
		/// <param name="literalValue">The constant value of the literal. </param>
		/// <returns>The defined field.</returns>
		// Token: 0x06002473 RID: 9331 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public FieldBuilder DefineLiteral(string literalName, object literalValue)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Calling this method always throws <see cref="T:System.NotSupportedException" />.</summary>
		/// <returns>This method is not supported. No value is returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported. </exception>
		// Token: 0x06002474 RID: 9332 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Type GetElementType()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets a custom attribute using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class to define the custom attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> is <see langword="null" />. </exception>
		// Token: 0x06002475 RID: 9333 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets a custom attribute using a specified custom attribute blob.</summary>
		/// <param name="con">The constructor for the custom attribute. </param>
		/// <param name="binaryAttribute">A byte blob representing the attributes. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> or <paramref name="binaryAttribute" /> is <see langword="null" />. </exception>
		// Token: 0x06002476 RID: 9334 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x06002477 RID: 9335 RVA: 0x0007DAC7 File Offset: 0x0007BCC7
		protected EnumBuilder()
		{
		}

		/// <summary>Returns the internal metadata type token of this enum.</summary>
		/// <returns>Read-only. The type token of this enum.</returns>
		// Token: 0x170005A2 RID: 1442
		// (get) Token: 0x06002478 RID: 9336 RVA: 0x00082B04 File Offset: 0x00080D04
		public TypeToken TypeToken
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return default(TypeToken);
			}
		}

		/// <summary>Returns the underlying system type for this enum.</summary>
		/// <returns>Read-only. Returns the underlying system type.</returns>
		// Token: 0x170005A3 RID: 1443
		// (get) Token: 0x06002479 RID: 9337 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type UnderlyingSystemType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Creates a <see cref="T:System.Type" /> object for this enum.</summary>
		/// <returns>A <see cref="T:System.Type" /> object for this enum.</returns>
		/// <exception cref="T:System.InvalidOperationException">This type has been previously created.-or- The enclosing type has not been created. </exception>
		// Token: 0x0600247A RID: 9338 RVA: 0x0005AB11 File Offset: 0x00058D11
		public Type CreateType()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600247B RID: 9339 RVA: 0x00082B20 File Offset: 0x00080D20
		protected override TypeAttributes GetAttributeFlagsImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return TypeAttributes.NotPublic;
		}

		// Token: 0x0600247C RID: 9340 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of <see cref="T:System.Reflection.ConstructorInfo" /> objects representing the public and non-public constructors defined for this class, as specified.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.ConstructorInfo" /> objects representing the specified constructors defined for this class. If no constructors are defined, an empty array is returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x0600247D RID: 9341 RVA: 0x0005AB11 File Offset: 0x00058D11
		[ComVisible(true)]
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns all the custom attributes defined for this constructor.</summary>
		/// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes. </param>
		/// <returns>Returns an array of objects representing all the custom attributes of the constructor represented by this <see cref="T:System.Reflection.Emit.ConstructorBuilder" /> instance.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x0600247E RID: 9342 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object[] GetCustomAttributes(bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the custom attributes identified by the given type.</summary>
		/// <param name="attributeType">The <see langword="Type" /> object to which the custom attributes are applied. </param>
		/// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes. </param>
		/// <returns>Returns an array of objects representing the attributes of this constructor that are of <see cref="T:System.Type" /><paramref name="attributeType" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x0600247F RID: 9343 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the event with the specified name.</summary>
		/// <param name="name">The name of the event to get. </param>
		/// <param name="bindingAttr">This invocation attribute. This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an <see cref="T:System.Reflection.EventInfo" /> object representing the event declared or inherited by this type with the specified name. If there are no matches, <see langword="null" /> is returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002480 RID: 9344 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the public and non-public events that are declared by this type.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" />, such as <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.EventInfo" /> objects representing the public and non-public events declared or inherited by this type. An empty array is returned if there are no events, as specified.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002481 RID: 9345 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override EventInfo[] GetEvents(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the field specified by the given name.</summary>
		/// <param name="name">The name of the field to get. </param>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns the <see cref="T:System.Reflection.FieldInfo" /> object representing the field declared or inherited by this type with the specified name and public or non-public modifier. If there are no matches, then null is returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002482 RID: 9346 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the public and non-public fields that are declared by this type.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" />, such as InvokeMethod, NonPublic, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.FieldInfo" /> objects representing the public and non-public fields declared or inherited by this type. An empty array is returned if there are no fields, as specified.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002483 RID: 9347 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override FieldInfo[] GetFields(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the interface implemented (directly or indirectly) by this type, with the specified fully-qualified name.</summary>
		/// <param name="name">The name of the interface. </param>
		/// <param name="ignoreCase">If <see langword="true" />, the search is case-insensitive. If <see langword="false" />, the search is case-sensitive. </param>
		/// <returns>Returns a <see cref="T:System.Type" /> object representing the implemented interface. Returns null if no interface matching name is found.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002484 RID: 9348 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type GetInterface(string name, bool ignoreCase)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns an array of all the interfaces implemented on this a class and its base classes.</summary>
		/// <returns>Returns an array of <see cref="T:System.Type" /> objects representing the implemented interfaces. If none are defined, an empty array is returned.</returns>
		// Token: 0x06002485 RID: 9349 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type[] GetInterfaces()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the specified members declared or inherited by this type,.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.MemberInfo" /> objects representing the public and non-public members declared or inherited by this type. An empty array is returned if there are no matching members.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002486 RID: 9350 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06002487 RID: 9351 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns all the public and non-public methods declared or inherited by this type, as specified.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" />, such as <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.MethodInfo" /> objects representing the public and non-public methods defined on this type if <paramref name="nonPublic" /> is used; otherwise, only the public methods are returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002488 RID: 9352 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the specified nested type that is declared by this type.</summary>
		/// <param name="name">The <see cref="T:System.String" /> containing the name of the nested type to get. </param>
		/// <param name="bindingAttr">A bitmask comprised of one or more <see cref="T:System.Reflection.BindingFlags" /> that specify how the search is conducted.-or- Zero, to conduct a case-sensitive search for public methods. </param>
		/// <returns>A <see cref="T:System.Type" /> object representing the nested type that matches the specified requirements, if found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002489 RID: 9353 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type GetNestedType(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the public and non-public nested types that are declared or inherited by this type.</summary>
		/// <param name="bindingAttr">This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" />, such as <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>An array of <see cref="T:System.Type" /> objects representing all the types nested within the current <see cref="T:System.Type" /> that match the specified binding constraints.An empty array of type <see cref="T:System.Type" />, if no types are nested within the current <see cref="T:System.Type" />, or if none of the nested types match the binding constraints.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x0600248A RID: 9354 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type[] GetNestedTypes(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns all the public and non-public properties declared or inherited by this type, as specified.</summary>
		/// <param name="bindingAttr">This invocation attribute. This must be a bit flag from <see cref="T:System.Reflection.BindingFlags" /> : <see langword="InvokeMethod" />, <see langword="NonPublic" />, and so on. </param>
		/// <returns>Returns an array of <see cref="T:System.Reflection.PropertyInfo" /> objects representing the public and non-public properties defined on this type if <paramref name="nonPublic" /> is used; otherwise, only the public properties are returned.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x0600248B RID: 9355 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600248C RID: 9356 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600248D RID: 9357 RVA: 0x00082B3C File Offset: 0x00080D3C
		protected override bool HasElementTypeImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Invokes the specified member. The method that is to be invoked must be accessible and provide the most specific match with the specified argument list, under the contraints of the specified binder and invocation attributes.</summary>
		/// <param name="name">The name of the member to invoke. This can be a constructor, method, property, or field. A suitable invocation attribute must be specified. Note that it is possible to invoke the default member of a class by passing an empty string as the name of the member. </param>
		/// <param name="invokeAttr">The invocation attribute. This must be a bit flag from <see langword="BindingFlags" />. </param>
		/// <param name="binder">An object that enables the binding, coercion of argument types, invocation of members, and retrieval of <see langword="MemberInfo" /> objects using reflection. If binder is <see langword="null" />, the default binder is used. See <see cref="T:System.Reflection.Binder" />. </param>
		/// <param name="target">The object on which to invoke the specified member. If the member is static, this parameter is ignored. </param>
		/// <param name="args">An argument list. This is an array of objects that contains the number, order, and type of the parameters of the member to be invoked. If there are no parameters this should be null. </param>
		/// <param name="modifiers">An array of the same length as <paramref name="args" /> with elements that represent the attributes associated with the arguments of the member to be invoked. A parameter has attributes associated with it in the metadata. They are used by various interoperability services. See the metadata specs for details such as this. </param>
		/// <param name="culture">An instance of <see langword="CultureInfo" /> used to govern the coercion of types. If this is null, the <see langword="CultureInfo" /> for the current thread is used. (Note that this is necessary to, for example, convert a string that represents 1000 to a double value, since 1000 is represented differently by different cultures.) </param>
		/// <param name="namedParameters">Each parameter in the <paramref name="namedParameters" /> array gets the value in the corresponding element in the <paramref name="args" /> array. If the length of <paramref name="args" /> is greater than the length of <paramref name="namedParameters" />, the remaining argument values are passed in order. </param>
		/// <returns>Returns the return value of the invoked member.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x0600248E RID: 9358 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x0600248F RID: 9359 RVA: 0x00082B58 File Offset: 0x00080D58
		protected override bool IsArrayImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06002490 RID: 9360 RVA: 0x00082B74 File Offset: 0x00080D74
		protected override bool IsByRefImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06002491 RID: 9361 RVA: 0x00082B90 File Offset: 0x00080D90
		protected override bool IsCOMObjectImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Checks if the specified custom attribute type is defined.</summary>
		/// <param name="attributeType">The <see langword="Type" /> object to which the custom attributes are applied. </param>
		/// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes. </param>
		/// <returns>
		///     <see langword="true" /> if one or more instance of <paramref name="attributeType" /> is defined on this member; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported in types that are not complete. </exception>
		// Token: 0x06002492 RID: 9362 RVA: 0x00082BAC File Offset: 0x00080DAC
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06002493 RID: 9363 RVA: 0x00082BC8 File Offset: 0x00080DC8
		protected override bool IsPointerImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x06002494 RID: 9364 RVA: 0x00082BE4 File Offset: 0x00080DE4
		protected override bool IsPrimitiveImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002495 RID: 9365 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EnumBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002496 RID: 9366 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EnumBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002497 RID: 9367 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EnumBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002498 RID: 9368 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EnumBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
