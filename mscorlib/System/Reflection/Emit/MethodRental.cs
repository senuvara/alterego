﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Provides a fast way to swap method body implementation given a method of a class.</summary>
	// Token: 0x02000AB0 RID: 2736
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_MethodRental))]
	[HostProtection(SecurityAction.LinkDemand, MayLeakOnAbort = true)]
	public sealed class MethodRental : _MethodRental
	{
		// Token: 0x06005F6E RID: 24430 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal MethodRental()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Swaps the body of a method.</summary>
		/// <param name="cls">The class containing the method. </param>
		/// <param name="methodtoken">The token for the method. </param>
		/// <param name="rgIL">A pointer to the method. This should include the method header. </param>
		/// <param name="methodSize">The size of the new method body in bytes. </param>
		/// <param name="flags">Flags that control the swapping. See the definitions of the constants. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="cls" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The type <paramref name="cls" /> is not complete. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="methodSize" /> is less than one or greater than 4128767 (3effff hex).</exception>
		// Token: 0x06005F6F RID: 24431 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
		public static void SwapMethodBody(Type cls, int methodtoken, IntPtr rgIL, int methodSize, int flags)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06005F70 RID: 24432 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _MethodRental.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06005F71 RID: 24433 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _MethodRental.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06005F72 RID: 24434 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _MethodRental.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06005F73 RID: 24435 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _MethodRental.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Specifies that the method should be just-in-time (JIT) compiled immediately.</summary>
		// Token: 0x04002F80 RID: 12160
		public const int JitImmediate = 1;

		/// <summary>Specifies that the method should be just-in-time (JIT) compiled when needed.</summary>
		// Token: 0x04002F81 RID: 12161
		public const int JitOnDemand = 0;
	}
}
