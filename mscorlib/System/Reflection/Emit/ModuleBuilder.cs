﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Resources;
using System.Runtime.InteropServices;
using System.Security;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Defines and represents a module in a dynamic assembly.</summary>
	// Token: 0x0200033F RID: 831
	public abstract class ModuleBuilder : Module, _ModuleBuilder
	{
		/// <summary>Completes the global function definitions and global data definitions for this dynamic module.</summary>
		/// <exception cref="T:System.InvalidOperationException">This method was called previously. </exception>
		// Token: 0x0600254B RID: 9547 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void CreateGlobalFunctions()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines an enumeration type that is a value type with a single non-static field called <paramref name="value__" /> of the specified type.</summary>
		/// <param name="name">The full path of the enumeration type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="visibility">The type attributes for the enumeration. The attributes are any bits defined by <see cref="F:System.Reflection.TypeAttributes.VisibilityMask" />. </param>
		/// <param name="underlyingType">The underlying type for the enumeration. This must be a built-in integer type. </param>
		/// <returns>The defined enumeration.</returns>
		/// <exception cref="T:System.ArgumentException">Attributes other than visibility attributes are provided.-or- An enumeration with the given name exists in the parent assembly of this module.-or- The visibility attributes do not match the scope of the enumeration. For example, <see cref="F:System.Reflection.TypeAttributes.NestedPublic" /> is specified for <paramref name="visibility" />, but the enumeration is not a nested type. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x0600254C RID: 9548 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public EnumBuilder DefineEnum(string name, TypeAttributes visibility, Type underlyingType)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a global method with the specified name, attributes, return type, and parameter types.</summary>
		/// <param name="name">The name of the method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the method. <paramref name="attributes" /> must include <see cref="F:System.Reflection.MethodAttributes.Static" />. </param>
		/// <param name="returnType">The return type of the method. </param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <returns>The defined global method.</returns>
		/// <exception cref="T:System.ArgumentException">The method is not static. That is, <paramref name="attributes" /> does not include <see cref="F:System.Reflection.MethodAttributes.Static" />.-or- The length of <paramref name="name" /> is zero -or-An element in the <see cref="T:System.Type" /> array is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.ModuleBuilder.CreateGlobalFunctions" /> has been previously called. </exception>
		// Token: 0x0600254D RID: 9549 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodBuilder DefineGlobalMethod(string name, MethodAttributes attributes, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a global method with the specified name, attributes, calling convention, return type, and parameter types.</summary>
		/// <param name="name">The name of the method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attributes">The attributes of the method. <paramref name="attributes" /> must include <see cref="F:System.Reflection.MethodAttributes.Static" />.</param>
		/// <param name="callingConvention">The calling convention for the method. </param>
		/// <param name="returnType">The return type of the method. </param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <returns>The defined global method.</returns>
		/// <exception cref="T:System.ArgumentException">The method is not static. That is, <paramref name="attributes" /> does not include <see cref="F:System.Reflection.MethodAttributes.Static" />.-or-An element in the <see cref="T:System.Type" /> array is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.ModuleBuilder.CreateGlobalFunctions" /> has been previously called. </exception>
		// Token: 0x0600254E RID: 9550 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodBuilder DefineGlobalMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a global method with the specified name, attributes, calling convention, return type, custom modifiers for the return type, parameter types, and custom modifiers for the parameter types.</summary>
		/// <param name="name">The name of the method. <paramref name="name" /> cannot contain embedded null characters. </param>
		/// <param name="attributes">The attributes of the method. <paramref name="attributes" /> must include <see cref="F:System.Reflection.MethodAttributes.Static" />.</param>
		/// <param name="callingConvention">The calling convention for the method. </param>
		/// <param name="returnType">The return type of the method. </param>
		/// <param name="requiredReturnTypeCustomModifiers">An array of types representing the required custom modifiers for the return type, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If the return type has no required custom modifiers, specify <see langword="null" />. </param>
		/// <param name="optionalReturnTypeCustomModifiers">An array of types representing the optional custom modifiers for the return type, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If the return type has no optional custom modifiers, specify <see langword="null" />. </param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <param name="requiredParameterTypeCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding parameter of the global method. If a particular argument has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If the global method has no arguments, or if none of the arguments have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="optionalParameterTypeCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding parameter. If a particular argument has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If the global method has no arguments, or if none of the arguments have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <returns>The defined global method.</returns>
		/// <exception cref="T:System.ArgumentException">The method is not static. That is, <paramref name="attributes" /> does not include <see cref="F:System.Reflection.MethodAttributes.Static" />.-or-An element in the <see cref="T:System.Type" /> array is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="M:System.Reflection.Emit.ModuleBuilder.CreateGlobalFunctions" /> method has been previously called. </exception>
		// Token: 0x0600254F RID: 9551 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodBuilder DefineGlobalMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] requiredReturnTypeCustomModifiers, Type[] optionalReturnTypeCustomModifiers, Type[] parameterTypes, Type[][] requiredParameterTypeCustomModifiers, Type[][] optionalParameterTypeCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines an initialized data field in the .sdata section of the portable executable (PE) file.</summary>
		/// <param name="name">The name used to refer to the data. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="data">The binary large object (BLOB) of data. </param>
		/// <param name="attributes">The attributes for the field. The default is <see langword="Static" />. </param>
		/// <returns>A field to reference the data.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero.-or- The size of <paramref name="data" /> is less than or equal to zero or greater than or equal to 0x3f0000. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="data" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.ModuleBuilder.CreateGlobalFunctions" /> has been previously called. </exception>
		// Token: 0x06002550 RID: 9552 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public FieldBuilder DefineInitializedData(string name, byte[] data, FieldAttributes attributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Constructs a <see langword="TypeBuilder" /> for a private type with the specified name in this module.</summary>
		/// <param name="name">The full path of the type, including the namespace. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <returns>A private type with the specified name.</returns>
		/// <exception cref="T:System.ArgumentException">A type with the given name exists in the parent assembly of this module.-or- Nested type attributes are set on a type that is not nested. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x06002551 RID: 9553 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineType(string name)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Constructs a <see langword="TypeBuilder" /> given the type name and the type attributes.</summary>
		/// <param name="name">The full path of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the defined type. </param>
		/// <returns>A <see langword="TypeBuilder" /> created with all of the requested attributes.</returns>
		/// <exception cref="T:System.ArgumentException">A type with the given name exists in the parent assembly of this module.-or- Nested type attributes are set on a type that is not nested. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x06002552 RID: 9554 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineType(string name, TypeAttributes attr)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Constructs a <see langword="TypeBuilder" /> given type name, its attributes, and the type that the defined type extends.</summary>
		/// <param name="name">The full path of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attribute to be associated with the type. </param>
		/// <param name="parent">The type that the defined type extends. </param>
		/// <returns>A <see langword="TypeBuilder" /> created with all of the requested attributes.</returns>
		/// <exception cref="T:System.ArgumentException">A type with the given name exists in the parent assembly of this module.-or- Nested type attributes are set on a type that is not nested. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x06002553 RID: 9555 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Constructs a <see langword="TypeBuilder" /> given the type name, the attributes, the type that the defined type extends, and the total size of the type.</summary>
		/// <param name="name">The full path of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the defined type. </param>
		/// <param name="parent">The type that the defined type extends. </param>
		/// <param name="typesize">The total size of the type. </param>
		/// <returns>A <see langword="TypeBuilder" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">A type with the given name exists in the parent assembly of this module.-or- Nested type attributes are set on a type that is not nested. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x06002554 RID: 9556 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, int typesize)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Constructs a <see langword="TypeBuilder" /> given the type name, the attributes, the type that the defined type extends, and the packing size of the type.</summary>
		/// <param name="name">The full path of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the defined type. </param>
		/// <param name="parent">The type that the defined type extends. </param>
		/// <param name="packsize">The packing size of the type. </param>
		/// <returns>A <see langword="TypeBuilder" /> object.</returns>
		/// <exception cref="T:System.ArgumentException">A type with the given name exists in the parent assembly of this module.-or- Nested type attributes are set on a type that is not nested. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x06002555 RID: 9557 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, PackingSize packsize)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Constructs a <see langword="TypeBuilder" /> given the type name, attributes, the type that the defined type extends, and the interfaces that the defined type implements.</summary>
		/// <param name="name">The full path of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes to be associated with the type. </param>
		/// <param name="parent">The type that the defined type extends. </param>
		/// <param name="interfaces">The list of interfaces that the type implements. </param>
		/// <returns>A <see langword="TypeBuilder" /> created with all of the requested attributes.</returns>
		/// <exception cref="T:System.ArgumentException">A type with the given name exists in the parent assembly of this module.-or- Nested type attributes are set on a type that is not nested. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x06002556 RID: 9558 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, Type[] interfaces)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Constructs a <see langword="TypeBuilder" /> given the type name, attributes, the type that the defined type extends, the packing size of the defined type, and the total size of the defined type.</summary>
		/// <param name="name">The full path of the type. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="attr">The attributes of the defined type. </param>
		/// <param name="parent">The type that the defined type extends. </param>
		/// <param name="packingSize">The packing size of the type. </param>
		/// <param name="typesize">The total size of the type. </param>
		/// <returns>A <see langword="TypeBuilder" /> created with all of the requested attributes.</returns>
		/// <exception cref="T:System.ArgumentException">A type with the given name exists in the parent assembly of this module.-or- Nested type attributes are set on a type that is not nested. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x06002557 RID: 9559 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, PackingSize packingSize, int typesize)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines an uninitialized data field in the .sdata section of the portable executable (PE) file.</summary>
		/// <param name="name">The name used to refer to the data. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="size">The size of the data field. </param>
		/// <param name="attributes">The attributes for the field. </param>
		/// <returns>A field to reference the data.</returns>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero.-or- 
		///         <paramref name="size" /> is less than or equal to zero, or greater than or equal to 0x003f0000. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.ModuleBuilder.CreateGlobalFunctions" /> has been previously called. </exception>
		// Token: 0x06002558 RID: 9560 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public FieldBuilder DefineUninitializedData(string name, int size, FieldAttributes attributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns the named method on an array class.</summary>
		/// <param name="arrayClass">An array class. </param>
		/// <param name="methodName">The name of a method on the array class. </param>
		/// <param name="callingConvention">The method's calling convention. </param>
		/// <param name="returnType">The return type of the method. </param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <returns>The named method on an array class.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="arrayClass" /> is not an array. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="arrayClass" /> or <paramref name="methodName" /> is <see langword="null" />. </exception>
		// Token: 0x06002559 RID: 9561 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public MethodInfo GetArrayMethod(Type arrayClass, string methodName, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Applies a custom attribute to this module by using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class that specifies the custom attribute to apply. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="customBuilder" /> is <see langword="null" />. </exception>
		// Token: 0x0600255A RID: 9562 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Applies a custom attribute to this module by using a specified binary large object (BLOB) that represents the attribute.</summary>
		/// <param name="con">The constructor for the custom attribute. </param>
		/// <param name="binaryAttribute">A byte BLOB representing the attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> or <paramref name="binaryAttribute" /> is <see langword="null" />. </exception>
		// Token: 0x0600255B RID: 9563 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x0600255C RID: 9564 RVA: 0x000814A6 File Offset: 0x0007F6A6
		protected ModuleBuilder()
		{
		}

		/// <summary>Defines a document for source.</summary>
		/// <param name="url">The URL for the document. </param>
		/// <param name="language">The GUID that identifies the document language. This can be <see cref="F:System.Guid.Empty" />. </param>
		/// <param name="languageVendor">The GUID that identifies the document language vendor. This can be <see cref="F:System.Guid.Empty" />. </param>
		/// <param name="documentType">The GUID that identifies the document type. This can be <see cref="F:System.Guid.Empty" />. </param>
		/// <returns>The defined document.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="url" /> is <see langword="null" />. This is a change from earlier versions of the .NET Framework.</exception>
		/// <exception cref="T:System.InvalidOperationException">This method is called on a dynamic module that is not a debug module. </exception>
		// Token: 0x0600255D RID: 9565 RVA: 0x0005AB11 File Offset: 0x00058D11
		public ISymbolDocumentWriter DefineDocument(string url, Guid language, Guid languageVendor, Guid documentType)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a binary large object (BLOB) that represents a manifest resource to be embedded in the dynamic assembly.</summary>
		/// <param name="name">The case-sensitive name for the resource.</param>
		/// <param name="stream">A stream that contains the bytes for the resource.</param>
		/// <param name="attribute">An enumeration value that specifies whether the resource is public or private.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />.-or-
		///         <paramref name="stream" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is a zero-length string.</exception>
		/// <exception cref="T:System.InvalidOperationException">The dynamic assembly that contains the current module is transient; that is, no file name was specified when <see cref="M:System.Reflection.Emit.AssemblyBuilder.DefineDynamicModule(System.String,System.String)" /> was called.</exception>
		// Token: 0x0600255E RID: 9566 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void DefineManifestResource(string name, Stream stream, ResourceAttributes attribute)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Defines a <see langword="PInvoke" /> method with the specified name, the name of the DLL in which the method is defined, the attributes of the method, the calling convention of the method, the return type of the method, the types of the parameters of the method, and the <see langword="PInvoke" /> flags.</summary>
		/// <param name="name">The name of the <see langword="PInvoke" /> method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="dllName">The name of the DLL in which the <see langword="PInvoke" /> method is defined. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="callingConvention">The method's calling convention. </param>
		/// <param name="returnType">The method's return type. </param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <param name="nativeCallConv">The native calling convention. </param>
		/// <param name="nativeCharSet">The method's native character set. </param>
		/// <returns>The defined <see langword="PInvoke" /> method.</returns>
		/// <exception cref="T:System.ArgumentException">The method is not static or if the containing type is an interface.-or- The method is abstract.-or- The method was previously defined. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="dllName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The containing type has been previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /></exception>
		// Token: 0x0600255F RID: 9567 RVA: 0x0005AB11 File Offset: 0x00058D11
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a <see langword="PInvoke" /> method with the specified name, the name of the DLL in which the method is defined, the attributes of the method, the calling convention of the method, the return type of the method, the types of the parameters of the method, and the <see langword="PInvoke" /> flags.</summary>
		/// <param name="name">The name of the <see langword="PInvoke" /> method. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="dllName">The name of the DLL in which the <see langword="PInvoke" /> method is defined. </param>
		/// <param name="entryName">The name of the entry point in the DLL. </param>
		/// <param name="attributes">The attributes of the method. </param>
		/// <param name="callingConvention">The method's calling convention. </param>
		/// <param name="returnType">The method's return type. </param>
		/// <param name="parameterTypes">The types of the method's parameters. </param>
		/// <param name="nativeCallConv">The native calling convention. </param>
		/// <param name="nativeCharSet">The method's native character set. </param>
		/// <returns>The defined <see langword="PInvoke" /> method.</returns>
		/// <exception cref="T:System.ArgumentException">The method is not static or if the containing type is an interface or if the method is abstract of if the method was previously defined. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="dllName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The containing type has been previously created using <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /></exception>
		// Token: 0x06002560 RID: 9568 RVA: 0x0005AB11 File Offset: 0x00058D11
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, string entryName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines the named managed embedded resource to be stored in this module.</summary>
		/// <param name="name">The name of the resource. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="description">The description of the resource. </param>
		/// <returns>A resource writer for the defined resource.</returns>
		/// <exception cref="T:System.ArgumentException">Length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">This module is transient.-or- The containing assembly is not persistable. </exception>
		// Token: 0x06002561 RID: 9569 RVA: 0x0005AB11 File Offset: 0x00058D11
		public IResourceWriter DefineResource(string name, string description)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines the named managed embedded resource with the given attributes that is to be stored in this module.</summary>
		/// <param name="name">The name of the resource. <paramref name="name" /> cannot contain embedded nulls. </param>
		/// <param name="description">The description of the resource. </param>
		/// <param name="attribute">The resource attributes. </param>
		/// <returns>A resource writer for the defined resource.</returns>
		/// <exception cref="T:System.ArgumentException">Length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">This module is transient.-or- The containing assembly is not persistable. </exception>
		// Token: 0x06002562 RID: 9570 RVA: 0x0005AB11 File Offset: 0x00058D11
		public IResourceWriter DefineResource(string name, string description, ResourceAttributes attribute)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines an unmanaged embedded resource given an opaque binary large object (BLOB) of bytes.</summary>
		/// <param name="resource">An opaque BLOB that represents an unmanaged resource </param>
		/// <exception cref="T:System.ArgumentException">An unmanaged resource has already been defined in the module's assembly. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="resource" /> is <see langword="null" />. </exception>
		// Token: 0x06002563 RID: 9571 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void DefineUnmanagedResource(byte[] resource)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Defines an unmanaged resource given the name of Win32 resource file.</summary>
		/// <param name="resourceFileName">The name of the unmanaged resource file. </param>
		/// <exception cref="T:System.ArgumentException">An unmanaged resource has already been defined in the module's assembly.-or- 
		///         <paramref name="resourceFileName" /> is the empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="resourceFileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">
		///         <paramref name="resourceFileName" /> is not found. -or- 
		///         <paramref name="resourceFileName" /> is a directory. </exception>
		// Token: 0x06002564 RID: 9572 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecuritySafeCritical]
		public void DefineUnmanagedResource(string resourceFileName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns the token for the named method on an array class.</summary>
		/// <param name="arrayClass">The object for the array. </param>
		/// <param name="methodName">A string that contains the name of the method. </param>
		/// <param name="callingConvention">The calling convention for the method. </param>
		/// <param name="returnType">The return type of the method. </param>
		/// <param name="parameterTypes">The types of the parameters of the method. </param>
		/// <returns>The token for the named method on an array class.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="arrayClass" /> is not an array.-or- The length of <paramref name="methodName" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="arrayClass" /> or <paramref name="methodName" /> is <see langword="null" />. </exception>
		// Token: 0x06002565 RID: 9573 RVA: 0x00082E90 File Offset: 0x00081090
		[SecuritySafeCritical]
		public MethodToken GetArrayMethodToken(Type arrayClass, string methodName, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(MethodToken);
		}

		/// <summary>Returns the token used to identify the specified constructor within this module.</summary>
		/// <param name="con">The constructor to get a token for. </param>
		/// <returns>The token used to identify the specified constructor within this module.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> is <see langword="null" />. </exception>
		// Token: 0x06002566 RID: 9574 RVA: 0x00082EAC File Offset: 0x000810AC
		[ComVisible(true)]
		[SecuritySafeCritical]
		public MethodToken GetConstructorToken(ConstructorInfo con)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(MethodToken);
		}

		/// <summary>Returns the token used to identify the constructor that has the specified attributes and parameter types within this module.</summary>
		/// <param name="constructor">The constructor to get a token for.</param>
		/// <param name="optionalParameterTypes">A collection of the types of the optional parameters to the constructor.</param>
		/// <returns>The token used to identify the specified constructor within this module.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="constructor" /> is <see langword="null" />. </exception>
		// Token: 0x06002567 RID: 9575 RVA: 0x00082EC8 File Offset: 0x000810C8
		[SecuritySafeCritical]
		public MethodToken GetConstructorToken(ConstructorInfo constructor, IEnumerable<Type> optionalParameterTypes)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(MethodToken);
		}

		/// <summary>Returns the token used to identify the specified field within this module.</summary>
		/// <param name="field">The field to get a token for. </param>
		/// <returns>The token used to identify the specified field within this module.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="field" /> is <see langword="null" />. </exception>
		// Token: 0x06002568 RID: 9576 RVA: 0x00082EE4 File Offset: 0x000810E4
		[SecuritySafeCritical]
		public FieldToken GetFieldToken(FieldInfo field)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(FieldToken);
		}

		/// <summary>Returns the token used to identify the specified method within this module.</summary>
		/// <param name="method">The method to get a token for. </param>
		/// <returns>The token used to identify the specified method within this module.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="method" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The declaring type for the method is not in this module. </exception>
		// Token: 0x06002569 RID: 9577 RVA: 0x00082F00 File Offset: 0x00081100
		[SecuritySafeCritical]
		public MethodToken GetMethodToken(MethodInfo method)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(MethodToken);
		}

		/// <summary>Returns the token used to identify the method that has the specified attributes and parameter types within this module.</summary>
		/// <param name="method">The method to get a token for.</param>
		/// <param name="optionalParameterTypes">A collection of the types of the optional parameters to the method.</param>
		/// <returns>The token used to identify the specified method within this module.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="method" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The declaring type for the method is not in this module. </exception>
		// Token: 0x0600256A RID: 9578 RVA: 0x00082F1C File Offset: 0x0008111C
		[SecuritySafeCritical]
		public MethodToken GetMethodToken(MethodInfo method, IEnumerable<Type> optionalParameterTypes)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(MethodToken);
		}

		/// <summary>Defines a token for the signature that has the specified character array and signature length.</summary>
		/// <param name="sigBytes">The signature binary large object (BLOB). </param>
		/// <param name="sigLength">The length of the signature BLOB. </param>
		/// <returns>A token for the specified signature.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="sigBytes" /> is <see langword="null" />. </exception>
		// Token: 0x0600256B RID: 9579 RVA: 0x00082F38 File Offset: 0x00081138
		[SecuritySafeCritical]
		public SignatureToken GetSignatureToken(byte[] sigBytes, int sigLength)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SignatureToken);
		}

		/// <summary>Defines a token for the signature that is defined by the specified <see cref="T:System.Reflection.Emit.SignatureHelper" />.</summary>
		/// <param name="sigHelper">The signature. </param>
		/// <returns>A token for the defined signature.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="sigHelper" /> is <see langword="null" />. </exception>
		// Token: 0x0600256C RID: 9580 RVA: 0x00082F54 File Offset: 0x00081154
		[SecuritySafeCritical]
		public SignatureToken GetSignatureToken(SignatureHelper sigHelper)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(SignatureToken);
		}

		/// <summary>Returns the token of the given string in the module’s constant pool.</summary>
		/// <param name="str">The string to add to the module's constant pool. </param>
		/// <returns>The token of the string in the constant pool.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="str" /> is <see langword="null" />. </exception>
		// Token: 0x0600256D RID: 9581 RVA: 0x00082F70 File Offset: 0x00081170
		[SecuritySafeCritical]
		public StringToken GetStringConstant(string str)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(StringToken);
		}

		/// <summary>Returns the symbol writer associated with this dynamic module.</summary>
		/// <returns>The symbol writer associated with this dynamic module.</returns>
		// Token: 0x0600256E RID: 9582 RVA: 0x0005AB11 File Offset: 0x00058D11
		public ISymbolWriter GetSymWriter()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Returns the token used to identify the type with the specified name.</summary>
		/// <param name="name">The name of the class, including the namespace. </param>
		/// <returns>The token used to identify the type with the specified name within this module.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> is the empty string ("").-or-
		///         <paramref name="name" /> represents a <see langword="ByRef" /> type.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. -or-The type specified by <paramref name="name" /> could not be found.</exception>
		/// <exception cref="T:System.InvalidOperationException">This is a non-transient module that references a transient module. </exception>
		// Token: 0x0600256F RID: 9583 RVA: 0x00082F8C File Offset: 0x0008118C
		public TypeToken GetTypeToken(string name)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(TypeToken);
		}

		/// <summary>Returns the token used to identify the specified type within this module.</summary>
		/// <param name="type">The type object that represents the class type. </param>
		/// <returns>The token used to identify the given type within this module.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="type" /> is a <see langword="ByRef" /> type.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This is a non-transient module that references a transient module. </exception>
		// Token: 0x06002570 RID: 9584 RVA: 0x00082FA8 File Offset: 0x000811A8
		[SecuritySafeCritical]
		public TypeToken GetTypeToken(Type type)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(TypeToken);
		}

		/// <summary>Returns a value that indicates whether this dynamic module is transient.</summary>
		/// <returns>
		///     <see langword="true" /> if this dynamic module is transient; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002571 RID: 9585 RVA: 0x00082FC4 File Offset: 0x000811C4
		public bool IsTransient()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>This method does nothing.</summary>
		/// <param name="name">The name of the custom attribute </param>
		/// <param name="data">An opaque binary large object (BLOB) of bytes that represents the value of the custom attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="url" />
		///          is <see langword="null" />. </exception>
		// Token: 0x06002572 RID: 9586 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void SetSymCustomAttribute(string name, byte[] data)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the user entry point.</summary>
		/// <param name="entryPoint">The user entry point. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="entryPoint" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This method is called on a dynamic module that is not a debug module.-or- 
		///         <paramref name="entryPoint" /> is not contained in this dynamic module. </exception>
		// Token: 0x06002573 RID: 9587 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecuritySafeCritical]
		public void SetUserEntryPoint(MethodInfo entryPoint)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Runtime.InteropServices._ModuleBuilder.GetIDsOfNames(System.Guid@,System.IntPtr,System.UInt32,System.UInt32,System.IntPtr)" />.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002574 RID: 9588 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ModuleBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Runtime.InteropServices._ModuleBuilder.GetTypeInfo(System.UInt32,System.UInt32,System.IntPtr)" />.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">A pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002575 RID: 9589 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ModuleBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Runtime.InteropServices._ModuleBuilder.GetTypeInfoCount(System.UInt32@)" />.</summary>
		/// <param name="pcTInfo">The location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002576 RID: 9590 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ModuleBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Runtime.InteropServices._ModuleBuilder.Invoke(System.UInt32,System.Guid@,System.UInt32,System.Int16,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)" />.</summary>
		/// <param name="dispIdMember">The member ID.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002577 RID: 9591 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ModuleBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
