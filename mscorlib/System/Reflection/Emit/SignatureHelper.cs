﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Provides methods for building signatures.</summary>
	// Token: 0x02000349 RID: 841
	public class SignatureHelper : _SignatureHelper
	{
		// Token: 0x060025B7 RID: 9655 RVA: 0x00002050 File Offset: 0x00000250
		private SignatureHelper()
		{
		}

		/// <summary>Adds an argument to the signature.</summary>
		/// <param name="clsArgument">The type of the argument. </param>
		/// <exception cref="T:System.ArgumentException">The signature has already been finished. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="clsArgument" /> is <see langword="null" />.</exception>
		// Token: 0x060025B8 RID: 9656 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void AddArgument(Type clsArgument)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds an argument of the specified type to the signature, specifying whether the argument is pinned.</summary>
		/// <param name="argument">The argument type.</param>
		/// <param name="pinned">
		///       <see langword="true" /> if the argument is pinned; otherwise, <see langword="false" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="argument" /> is <see langword="null" />.</exception>
		// Token: 0x060025B9 RID: 9657 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void AddArgument(Type argument, bool pinned)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds an argument to the signature, with the specified custom modifiers.</summary>
		/// <param name="argument">The argument type.</param>
		/// <param name="requiredCustomModifiers">An array of types representing the required custom modifiers for the argument, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If the argument has no required custom modifiers, specify <see langword="null" />.</param>
		/// <param name="optionalCustomModifiers">An array of types representing the optional custom modifiers for the argument, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If the argument has no optional custom modifiers, specify <see langword="null" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="argument" /> is <see langword="null" />. -or-An element of <paramref name="requiredCustomModifiers" /> or <paramref name="optionalCustomModifiers" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">The signature has already been finished. -or-One of the specified custom modifiers is an array type.-or-One of the specified custom modifiers is an open generic type. That is, the <see cref="P:System.Type.ContainsGenericParameters" /> property is <see langword="true" /> for the custom modifier.</exception>
		// Token: 0x060025BA RID: 9658 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void AddArgument(Type argument, Type[] requiredCustomModifiers, Type[] optionalCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a set of arguments to the signature, with the specified custom modifiers.</summary>
		/// <param name="arguments">The types of the arguments to be added.</param>
		/// <param name="requiredCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding argument, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If a particular argument has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the arguments have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="optionalCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding argument, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If a particular argument has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If none of the arguments have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <exception cref="T:System.ArgumentNullException">An element of <paramref name="arguments" /> is <see langword="null" />. -or-One of the specified custom modifiers is <see langword="null" />. (However, <see langword="null" /> can be specified for the array of custom modifiers for any argument.)</exception>
		/// <exception cref="T:System.ArgumentException">The signature has already been finished. -or-One of the specified custom modifiers is an array type.-or-One of the specified custom modifiers is an open generic type. That is, the <see cref="P:System.Type.ContainsGenericParameters" /> property is <see langword="true" /> for the custom modifier. -or-The size of <paramref name="requiredCustomModifiers" /> or <paramref name="optionalCustomModifiers" /> does not equal the size of <paramref name="arguments" />.</exception>
		// Token: 0x060025BB RID: 9659 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void AddArguments(Type[] arguments, Type[][] requiredCustomModifiers, Type[][] optionalCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Marks the end of a vararg fixed part. This is only used if the caller is creating a vararg signature call site.</summary>
		// Token: 0x060025BC RID: 9660 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void AddSentinel()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a field.</summary>
		/// <param name="mod">The dynamic module that contains the field for which the <see langword="SignatureHelper" /> is requested. </param>
		/// <returns>The <see langword="SignatureHelper" /> object for a field.</returns>
		// Token: 0x060025BD RID: 9661 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetFieldSigHelper(Module mod)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a local variable.</summary>
		/// <returns>A <see cref="T:System.Reflection.Emit.SignatureHelper" /> for a local variable.</returns>
		// Token: 0x060025BE RID: 9662 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetLocalVarSigHelper()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a local variable.</summary>
		/// <param name="mod">The dynamic module that contains the local variable for which the <see langword="SignatureHelper" /> is requested. </param>
		/// <returns>The <see langword="SignatureHelper" /> object for a local variable.</returns>
		// Token: 0x060025BF RID: 9663 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetLocalVarSigHelper(Module mod)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a method given the method's calling convention and return type.</summary>
		/// <param name="callingConvention">The calling convention of the method. </param>
		/// <param name="returnType">The return type of the method, or <see langword="null" /> for a void return type (<see langword="Sub" /> procedure in Visual Basic). </param>
		/// <returns>The <see langword="SignatureHelper" /> object for a method.</returns>
		// Token: 0x060025C0 RID: 9664 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetMethodSigHelper(CallingConventions callingConvention, Type returnType)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a method given the method's module, calling convention, and return type.</summary>
		/// <param name="mod">The <see cref="T:System.Reflection.Emit.ModuleBuilder" /> that contains the method for which the <see langword="SignatureHelper" /> is requested. </param>
		/// <param name="callingConvention">The calling convention of the method. </param>
		/// <param name="returnType">The return type of the method, or <see langword="null" /> for a void return type (<see langword="Sub" /> procedure in Visual Basic). </param>
		/// <returns>The <see langword="SignatureHelper" /> object for a method.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mod" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mod" /> is not a <see cref="T:System.Reflection.Emit.ModuleBuilder" />.</exception>
		// Token: 0x060025C1 RID: 9665 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetMethodSigHelper(Module mod, CallingConventions callingConvention, Type returnType)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a method with a standard calling convention, given the method's module, return type, and argument types.</summary>
		/// <param name="mod">The <see cref="T:System.Reflection.Emit.ModuleBuilder" /> that contains the method for which the <see langword="SignatureHelper" /> is requested. </param>
		/// <param name="returnType">The return type of the method, or <see langword="null" /> for a void return type (<see langword="Sub" /> procedure in Visual Basic). </param>
		/// <param name="parameterTypes">The types of the arguments of the method, or <see langword="null" /> if the method has no arguments. </param>
		/// <returns>The <see langword="SignatureHelper" /> object for a method.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mod" /> is <see langword="null" />.-or-An element of <paramref name="parameterTypes" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mod" /> is not a <see cref="T:System.Reflection.Emit.ModuleBuilder" />.</exception>
		// Token: 0x060025C2 RID: 9666 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetMethodSigHelper(Module mod, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a property, given the dynamic module that contains the property, the property type, and the property arguments.</summary>
		/// <param name="mod">The <see cref="T:System.Reflection.Emit.ModuleBuilder" /> that contains the property for which the <see cref="T:System.Reflection.Emit.SignatureHelper" /> is requested.</param>
		/// <param name="returnType">The property type.</param>
		/// <param name="parameterTypes">The argument types, or <see langword="null" /> if the property has no arguments.</param>
		/// <returns>A <see cref="T:System.Reflection.Emit.SignatureHelper" /> object for a property.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mod" /> is <see langword="null" />.-or-An element of <paramref name="parameterTypes" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mod" /> is not a <see cref="T:System.Reflection.Emit.ModuleBuilder" />.</exception>
		// Token: 0x060025C3 RID: 9667 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetPropertySigHelper(Module mod, Type returnType, Type[] parameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a property, given the dynamic module that contains the property, the property type, the property arguments, and custom modifiers for the return type and arguments.</summary>
		/// <param name="mod">The <see cref="T:System.Reflection.Emit.ModuleBuilder" /> that contains the property for which the <see cref="T:System.Reflection.Emit.SignatureHelper" /> is requested.</param>
		/// <param name="returnType">The property type.</param>
		/// <param name="requiredReturnTypeCustomModifiers">An array of types representing the required custom modifiers for the return type, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If the return type has no required custom modifiers, specify <see langword="null" />.</param>
		/// <param name="optionalReturnTypeCustomModifiers">An array of types representing the optional custom modifiers for the return type, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If the return type has no optional custom modifiers, specify <see langword="null" />.</param>
		/// <param name="parameterTypes">The types of the property's arguments, or <see langword="null" /> if the property has no arguments.</param>
		/// <param name="requiredParameterTypeCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding argument of the property. If a particular argument has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If the property has no arguments, or if none of the arguments have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="optionalParameterTypeCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding argument of the property. If a particular argument has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If the property has no arguments, or if none of the arguments have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <returns>A <see cref="T:System.Reflection.Emit.SignatureHelper" /> object for a property.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mod" /> is <see langword="null" />.-or-An element of <paramref name="parameterTypes" /> is <see langword="null" />. -or-One of the specified custom modifiers is <see langword="null" />. (However, <see langword="null" /> can be specified for the array of custom modifiers for any argument.)</exception>
		/// <exception cref="T:System.ArgumentException">The signature has already been finished. -or-
		///         <paramref name="mod" /> is not a <see cref="T:System.Reflection.Emit.ModuleBuilder" />.-or-One of the specified custom modifiers is an array type.-or-One of the specified custom modifiers is an open generic type. That is, the <see cref="P:System.Type.ContainsGenericParameters" /> property is <see langword="true" /> for the custom modifier.-or-The size of <paramref name="requiredParameterTypeCustomModifiers" /> or <paramref name="optionalParameterTypeCustomModifiers" /> does not equal the size of <paramref name="parameterTypes" />.</exception>
		// Token: 0x060025C4 RID: 9668 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetPropertySigHelper(Module mod, Type returnType, Type[] requiredReturnTypeCustomModifiers, Type[] optionalReturnTypeCustomModifiers, Type[] parameterTypes, Type[][] requiredParameterTypeCustomModifiers, Type[][] optionalParameterTypeCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns a signature helper for a property, given the dynamic module that contains the property, the calling convention, the property type, the property arguments, and custom modifiers for the return type and arguments.</summary>
		/// <param name="mod">The <see cref="T:System.Reflection.Emit.ModuleBuilder" /> that contains the property for which the <see cref="T:System.Reflection.Emit.SignatureHelper" /> is requested.</param>
		/// <param name="callingConvention">The calling convention of the property accessors.</param>
		/// <param name="returnType">The property type.</param>
		/// <param name="requiredReturnTypeCustomModifiers">An array of types representing the required custom modifiers for the return type, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If the return type has no required custom modifiers, specify <see langword="null" />.</param>
		/// <param name="optionalReturnTypeCustomModifiers">An array of types representing the optional custom modifiers for the return type, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsBoxed" />. If the return type has no optional custom modifiers, specify <see langword="null" />.</param>
		/// <param name="parameterTypes">The types of the property's arguments, or <see langword="null" /> if the property has no arguments.</param>
		/// <param name="requiredParameterTypeCustomModifiers">An array of arrays of types. Each array of types represents the required custom modifiers for the corresponding argument of the property. If a particular argument has no required custom modifiers, specify <see langword="null" /> instead of an array of types. If the property has no arguments, or if none of the arguments have required custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <param name="optionalParameterTypeCustomModifiers">An array of arrays of types. Each array of types represents the optional custom modifiers for the corresponding argument of the property. If a particular argument has no optional custom modifiers, specify <see langword="null" /> instead of an array of types. If the property has no arguments, or if none of the arguments have optional custom modifiers, specify <see langword="null" /> instead of an array of arrays.</param>
		/// <returns>A <see cref="T:System.Reflection.Emit.SignatureHelper" /> object for a property.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mod" /> is <see langword="null" />.-or-An element of <paramref name="parameterTypes" /> is <see langword="null" />. -or-One of the specified custom modifiers is <see langword="null" />. (However, <see langword="null" /> can be specified for the array of custom modifiers for any argument.)</exception>
		/// <exception cref="T:System.ArgumentException">The signature has already been finished. -or-
		///         <paramref name="mod" /> is not a <see cref="T:System.Reflection.Emit.ModuleBuilder" />.-or-One of the specified custom modifiers is an array type.-or-One of the specified custom modifiers is an open generic type. That is, the <see cref="P:System.Type.ContainsGenericParameters" /> property is <see langword="true" /> for the custom modifier.-or-The size of <paramref name="requiredParameterTypeCustomModifiers" /> or <paramref name="optionalParameterTypeCustomModifiers" /> does not equal the size of <paramref name="parameterTypes" />.</exception>
		// Token: 0x060025C5 RID: 9669 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static SignatureHelper GetPropertySigHelper(Module mod, CallingConventions callingConvention, Type returnType, Type[] requiredReturnTypeCustomModifiers, Type[] optionalReturnTypeCustomModifiers, Type[] parameterTypes, Type[][] requiredParameterTypeCustomModifiers, Type[][] optionalParameterTypeCustomModifiers)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds the end token to the signature and marks the signature as finished, so no further tokens can be added.</summary>
		/// <returns>Returns a byte array made up of the full signature.</returns>
		// Token: 0x060025C6 RID: 9670 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public byte[] GetSignature()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x060025C7 RID: 9671 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _SignatureHelper.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x060025C8 RID: 9672 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _SignatureHelper.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x060025C9 RID: 9673 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _SignatureHelper.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x060025CA RID: 9674 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _SignatureHelper.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
