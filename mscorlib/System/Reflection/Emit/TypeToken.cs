﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Represents the <see langword="Token" /> returned by the metadata to represent a type.</summary>
	// Token: 0x02000A84 RID: 2692
	[ComVisible(true)]
	[Serializable]
	public struct TypeToken
	{
		/// <summary>Retrieves the metadata token for this class.</summary>
		/// <returns>Read-only. Retrieves the metadata token of this type.</returns>
		// Token: 0x17001100 RID: 4352
		// (get) Token: 0x06005E7B RID: 24187 RVA: 0x0013012C File Offset: 0x0012E32C
		public int Token
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return 0;
			}
		}

		/// <summary>Indicates whether the current instance is equal to the specified <see cref="T:System.Reflection.Emit.TypeToken" />.</summary>
		/// <param name="obj">The <see cref="T:System.Reflection.Emit.TypeToken" /> to compare to the current instance.</param>
		/// <returns>
		///     <see langword="true" /> if the value of <paramref name="obj" /> is equal to the value of the current instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06005E7C RID: 24188 RVA: 0x00130148 File Offset: 0x0012E348
		public bool Equals(TypeToken obj)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether two <see cref="T:System.Reflection.Emit.TypeToken" /> structures are equal.</summary>
		/// <param name="a">The <see cref="T:System.Reflection.Emit.TypeToken" /> to compare to <paramref name="b" />.</param>
		/// <param name="b">The <see cref="T:System.Reflection.Emit.TypeToken" /> to compare to <paramref name="a" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> is equal to <paramref name="b" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06005E7D RID: 24189 RVA: 0x00130164 File Offset: 0x0012E364
		public static bool operator ==(TypeToken a, TypeToken b)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Indicates whether two <see cref="T:System.Reflection.Emit.TypeToken" /> structures are not equal.</summary>
		/// <param name="a">The <see cref="T:System.Reflection.Emit.TypeToken" /> to compare to <paramref name="b" />.</param>
		/// <param name="b">The <see cref="T:System.Reflection.Emit.TypeToken" /> to compare to <paramref name="a" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> is not equal to <paramref name="b" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06005E7E RID: 24190 RVA: 0x00130180 File Offset: 0x0012E380
		public static bool operator !=(TypeToken a, TypeToken b)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>The default <see langword="TypeToken" /> with <see cref="P:System.Reflection.Emit.TypeToken.Token" /> value 0.</summary>
		// Token: 0x04002F63 RID: 12131
		public static readonly TypeToken Empty;
	}
}
