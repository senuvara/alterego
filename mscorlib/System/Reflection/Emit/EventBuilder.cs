﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Defines events for a class.</summary>
	// Token: 0x02000337 RID: 823
	public class EventBuilder : _EventBuilder
	{
		/// <summary>Adds one of the "other" methods associated with this event. "Other" methods are methods other than the "on" and "raise" methods associated with an event. This function can be called many times to add as many "other" methods.</summary>
		/// <param name="mdBuilder">A <see langword="MethodBuilder" /> object that represents the other method. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mdBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x06002499 RID: 9369 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void AddOtherMethod(MethodBuilder mdBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the method used to subscribe to this event.</summary>
		/// <param name="mdBuilder">A <see langword="MethodBuilder" /> object that represents the method used to subscribe to this event. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mdBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x0600249A RID: 9370 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetAddOnMethod(MethodBuilder mdBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets a custom attribute using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class to describe the custom attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x0600249B RID: 9371 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute using a specified custom attribute blob.</summary>
		/// <param name="con">The constructor for the custom attribute. </param>
		/// <param name="binaryAttribute">A byte blob representing the attributes. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> or <paramref name="binaryAttribute" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x0600249C RID: 9372 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the method used to raise this event.</summary>
		/// <param name="mdBuilder">A <see langword="MethodBuilder" /> object that represents the method used to raise this event. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mdBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x0600249D RID: 9373 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetRaiseMethod(MethodBuilder mdBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the method used to unsubscribe to this event.</summary>
		/// <param name="mdBuilder">A <see langword="MethodBuilder" /> object that represents the method used to unsubscribe to this event. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="mdBuilder" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has been called on the enclosing type. </exception>
		// Token: 0x0600249E RID: 9374 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetRemoveOnMethod(MethodBuilder mdBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x0600249F RID: 9375 RVA: 0x00002050 File Offset: 0x00000250
		public EventBuilder()
		{
		}

		/// <summary>Returns the token for this event.</summary>
		/// <returns>Returns the <see langword="EventToken" /> for this event.</returns>
		// Token: 0x060024A0 RID: 9376 RVA: 0x00082C00 File Offset: 0x00080E00
		public EventToken GetEventToken()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(EventToken);
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x060024A1 RID: 9377 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EventBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x060024A2 RID: 9378 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EventBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x060024A3 RID: 9379 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EventBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x060024A4 RID: 9380 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EventBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
