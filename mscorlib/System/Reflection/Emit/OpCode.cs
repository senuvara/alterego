﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	/// <summary>Describes an intermediate language (IL) instruction.</summary>
	// Token: 0x02000340 RID: 832
	[ComVisible(true)]
	public struct OpCode
	{
		// Token: 0x06002578 RID: 9592 RVA: 0x00082FE0 File Offset: 0x000811E0
		internal OpCode(int p, int q)
		{
			this.op1 = (byte)(p & 255);
			this.op2 = (byte)(p >> 8 & 255);
			this.push = (byte)(p >> 16 & 255);
			this.pop = (byte)(p >> 24 & 255);
			this.size = (byte)(q & 255);
			this.type = (byte)(q >> 8 & 255);
			this.args = (byte)(q >> 16 & 255);
			this.flow = (byte)(q >> 24 & 255);
		}

		/// <summary>Returns the generated hash code for this <see langword="Opcode" />.</summary>
		/// <returns>Returns the hash code for this instance.</returns>
		// Token: 0x06002579 RID: 9593 RVA: 0x0008306D File Offset: 0x0008126D
		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}

		/// <summary>Tests whether the given object is equal to this <see langword="Opcode" />.</summary>
		/// <param name="obj">The object to compare to this object. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is an instance of <see langword="Opcode" /> and is equal to this object; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600257A RID: 9594 RVA: 0x0008307C File Offset: 0x0008127C
		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is OpCode))
			{
				return false;
			}
			OpCode opCode = (OpCode)obj;
			return opCode.op1 == this.op1 && opCode.op2 == this.op2;
		}

		/// <summary>Indicates whether the current instance is equal to the specified <see cref="T:System.Reflection.Emit.OpCode" />.</summary>
		/// <param name="obj">The <see cref="T:System.Reflection.Emit.OpCode" /> to compare to the current instance.</param>
		/// <returns>
		///     <see langword="true" /> if the value of <paramref name="obj" /> is equal to the value of the current instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600257B RID: 9595 RVA: 0x000830BB File Offset: 0x000812BB
		public bool Equals(OpCode obj)
		{
			return obj.op1 == this.op1 && obj.op2 == this.op2;
		}

		/// <summary>Returns this <see langword="Opcode" /> as a <see cref="T:System.String" />.</summary>
		/// <returns>Returns a <see cref="T:System.String" /> containing the name of this <see langword="Opcode" />.</returns>
		// Token: 0x0600257C RID: 9596 RVA: 0x000830DB File Offset: 0x000812DB
		public override string ToString()
		{
			return this.Name;
		}

		/// <summary>The name of the intermediate language (IL) instruction.</summary>
		/// <returns>Read-only. The name of the IL instruction.</returns>
		// Token: 0x170005C2 RID: 1474
		// (get) Token: 0x0600257D RID: 9597 RVA: 0x000830E3 File Offset: 0x000812E3
		public string Name
		{
			get
			{
				if (this.op1 == 255)
				{
					return OpCodeNames.names[(int)this.op2];
				}
				return OpCodeNames.names[256 + (int)this.op2];
			}
		}

		/// <summary>The size of the intermediate language (IL) instruction.</summary>
		/// <returns>Read-only. The size of the IL instruction.</returns>
		// Token: 0x170005C3 RID: 1475
		// (get) Token: 0x0600257E RID: 9598 RVA: 0x00083111 File Offset: 0x00081311
		public int Size
		{
			get
			{
				return (int)this.size;
			}
		}

		/// <summary>The type of intermediate language (IL) instruction.</summary>
		/// <returns>Read-only. The type of intermediate language (IL) instruction.</returns>
		// Token: 0x170005C4 RID: 1476
		// (get) Token: 0x0600257F RID: 9599 RVA: 0x00083119 File Offset: 0x00081319
		public OpCodeType OpCodeType
		{
			get
			{
				return (OpCodeType)this.type;
			}
		}

		/// <summary>The operand type of an intermediate language (IL) instruction.</summary>
		/// <returns>Read-only. The operand type of an IL instruction.</returns>
		// Token: 0x170005C5 RID: 1477
		// (get) Token: 0x06002580 RID: 9600 RVA: 0x00083121 File Offset: 0x00081321
		public OperandType OperandType
		{
			get
			{
				return (OperandType)this.args;
			}
		}

		/// <summary>The flow control characteristics of the intermediate language (IL) instruction.</summary>
		/// <returns>Read-only. The type of flow control.</returns>
		// Token: 0x170005C6 RID: 1478
		// (get) Token: 0x06002581 RID: 9601 RVA: 0x00083129 File Offset: 0x00081329
		public FlowControl FlowControl
		{
			get
			{
				return (FlowControl)this.flow;
			}
		}

		/// <summary>How the intermediate language (IL) instruction pops the stack.</summary>
		/// <returns>Read-only. The way the IL instruction pops the stack.</returns>
		// Token: 0x170005C7 RID: 1479
		// (get) Token: 0x06002582 RID: 9602 RVA: 0x00083131 File Offset: 0x00081331
		public StackBehaviour StackBehaviourPop
		{
			get
			{
				return (StackBehaviour)this.pop;
			}
		}

		/// <summary>How the intermediate language (IL) instruction pushes operand onto the stack.</summary>
		/// <returns>Read-only. The way the IL instruction pushes operand onto the stack.</returns>
		// Token: 0x170005C8 RID: 1480
		// (get) Token: 0x06002583 RID: 9603 RVA: 0x00083139 File Offset: 0x00081339
		public StackBehaviour StackBehaviourPush
		{
			get
			{
				return (StackBehaviour)this.push;
			}
		}

		/// <summary>Gets the numeric value of the intermediate language (IL) instruction.</summary>
		/// <returns>Read-only. The numeric value of the IL instruction.</returns>
		// Token: 0x170005C9 RID: 1481
		// (get) Token: 0x06002584 RID: 9604 RVA: 0x00083141 File Offset: 0x00081341
		public short Value
		{
			get
			{
				if (this.size == 1)
				{
					return (short)this.op2;
				}
				return (short)((int)this.op1 << 8 | (int)this.op2);
			}
		}

		/// <summary>Indicates whether two <see cref="T:System.Reflection.Emit.OpCode" /> structures are equal.</summary>
		/// <param name="a">The <see cref="T:System.Reflection.Emit.OpCode" /> to compare to <paramref name="b" />.</param>
		/// <param name="b">The <see cref="T:System.Reflection.Emit.OpCode" /> to compare to <paramref name="a" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> is equal to <paramref name="b" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002585 RID: 9605 RVA: 0x00083163 File Offset: 0x00081363
		public static bool operator ==(OpCode a, OpCode b)
		{
			return a.op1 == b.op1 && a.op2 == b.op2;
		}

		/// <summary>Indicates whether two <see cref="T:System.Reflection.Emit.OpCode" /> structures are not equal.</summary>
		/// <param name="a">The <see cref="T:System.Reflection.Emit.OpCode" /> to compare to <paramref name="b" />.</param>
		/// <param name="b">The <see cref="T:System.Reflection.Emit.OpCode" /> to compare to <paramref name="a" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> is not equal to <paramref name="b" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002586 RID: 9606 RVA: 0x00083183 File Offset: 0x00081383
		public static bool operator !=(OpCode a, OpCode b)
		{
			return a.op1 != b.op1 || a.op2 != b.op2;
		}

		// Token: 0x04001356 RID: 4950
		internal byte op1;

		// Token: 0x04001357 RID: 4951
		internal byte op2;

		// Token: 0x04001358 RID: 4952
		private byte push;

		// Token: 0x04001359 RID: 4953
		private byte pop;

		// Token: 0x0400135A RID: 4954
		private byte size;

		// Token: 0x0400135B RID: 4955
		private byte type;

		// Token: 0x0400135C RID: 4956
		private byte args;

		// Token: 0x0400135D RID: 4957
		private byte flow;
	}
}
