﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	/// <summary>Defines the access modes for a dynamic assembly.</summary>
	// Token: 0x02000333 RID: 819
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum AssemblyBuilderAccess
	{
		/// <summary>The dynamic assembly can be executed, but not saved.</summary>
		// Token: 0x04001340 RID: 4928
		Run = 1,
		/// <summary>The dynamic assembly can be saved, but not executed.</summary>
		// Token: 0x04001341 RID: 4929
		Save = 2,
		/// <summary>The dynamic assembly can be executed and saved.</summary>
		// Token: 0x04001342 RID: 4930
		RunAndSave = 3,
		/// <summary>
		///
		///     The dynamic assembly is loaded into the reflection-only context, and cannot be executed.</summary>
		// Token: 0x04001343 RID: 4931
		ReflectionOnly = 6,
		/// <summary>The dynamic assembly can be unloaded and its memory reclaimed, subject to the restrictions described in Collectible Assemblies for Dynamic Type Generation.</summary>
		// Token: 0x04001344 RID: 4932
		RunAndCollect = 9
	}
}
