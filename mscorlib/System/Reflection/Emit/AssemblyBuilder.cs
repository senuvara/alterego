﻿using System;
using System.Collections.Generic;
using System.Resources;
using System.Runtime.InteropServices;
using System.Security;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Defines and represents a dynamic assembly.</summary>
	// Token: 0x02000332 RID: 818
	public class AssemblyBuilder : Assembly, _AssemblyBuilder
	{
		/// <summary>Defines a dynamic assembly that has the specified name and access rights.</summary>
		/// <param name="name">The name of the assembly.</param>
		/// <param name="access">The access rights of the assembly.</param>
		/// <returns>An object that represents the new assembly.</returns>
		// Token: 0x06002426 RID: 9254 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a new assembly that has the specified name, access rights, and attributes.</summary>
		/// <param name="name">The name of the assembly.</param>
		/// <param name="access">The access rights of the assembly.</param>
		/// <param name="assemblyAttributes">A collection that contains the attributes of the assembly.</param>
		/// <returns>An object that represents the new assembly.</returns>
		// Token: 0x06002427 RID: 9255 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, IEnumerable<CustomAttributeBuilder> assemblyAttributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Defines a named transient dynamic module in this assembly.</summary>
		/// <param name="name">The name of the dynamic module. Must be less than 260 characters in length. </param>
		/// <returns>A <see cref="T:System.Reflection.Emit.ModuleBuilder" /> representing the defined dynamic module.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> begins with white space.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="name" /> is greater than or equal to 260. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ExecutionEngineException">The assembly for default symbol writer cannot be loaded.-or- The type that implements the default symbol writer interface cannot be found. </exception>
		// Token: 0x06002428 RID: 9256 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public ModuleBuilder DefineDynamicModule(string name)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Returns the dynamic module with the specified name.</summary>
		/// <param name="name">The name of the requested dynamic module. </param>
		/// <returns>A ModuleBuilder object representing the requested dynamic module.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002429 RID: 9257 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public ModuleBuilder GetDynamicModule(string name)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute on this assembly using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class to define the custom attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x0600242A RID: 9258 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute on this assembly using a specified custom attribute blob.</summary>
		/// <param name="con">The constructor for the custom attribute. </param>
		/// <param name="binaryAttribute">A byte blob representing the attributes. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> or <paramref name="binaryAttribute" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="con" /> is not a <see langword="RuntimeConstructorInfo" /> object. </exception>
		// Token: 0x0600242B RID: 9259 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x0600242C RID: 9260 RVA: 0x000805AC File Offset: 0x0007E7AC
		public AssemblyBuilder()
		{
		}

		/// <summary>Gets the grant set of the current dynamic assembly.</summary>
		/// <returns>The grant set of the current dynamic assembly.</returns>
		// Token: 0x17000590 RID: 1424
		// (get) Token: 0x0600242D RID: 9261 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override PermissionSet PermissionSet
		{
			[SecurityCritical]
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		/// <summary>Adds an existing resource file to this assembly.</summary>
		/// <param name="name">The logical name of the resource. </param>
		/// <param name="fileName">The physical file name (.resources file) to which the logical name is mapped. This should not include a path; the file must be in the same directory as the assembly to which it is added. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> has been previously defined.-or- There is another file in the assembly named <paramref name="fileName" />.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="fileName" /> is zero, or if <paramref name="fileName" /> includes a path. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="fileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file <paramref name="fileName" /> is not found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x0600242E RID: 9262 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void AddResourceFile(string name, string fileName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds an existing resource file to this assembly.</summary>
		/// <param name="name">The logical name of the resource. </param>
		/// <param name="fileName">The physical file name (.resources file) to which the logical name is mapped. This should not include a path; the file must be in the same directory as the assembly to which it is added. </param>
		/// <param name="attribute">The resource attributes. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> has been previously defined.-or- There is another file in the assembly named <paramref name="fileName" />.-or- The length of <paramref name="name" /> is zero or if the length of <paramref name="fileName" /> is zero.-or- 
		///         <paramref name="fileName" /> includes a path. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="fileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">If the file <paramref name="fileName" /> is not found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x0600242F RID: 9263 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void AddResourceFile(string name, string fileName, ResourceAttributes attribute)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Defines a named transient dynamic module in this assembly and specifies whether symbol information should be emitted.</summary>
		/// <param name="name">The name of the dynamic module. Must be less than 260 characters in length. </param>
		/// <param name="emitSymbolInfo">
		///       <see langword="true" /> if symbol information is to be emitted; otherwise, <see langword="false" />. </param>
		/// <returns>A <see cref="T:System.Reflection.Emit.ModuleBuilder" /> representing the defined dynamic module.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> begins with white space.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="name" /> is greater than or equal to 260. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ExecutionEngineException">The assembly for default symbol writer cannot be loaded.-or- The type that implements the default symbol writer interface cannot be found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002430 RID: 9264 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public ModuleBuilder DefineDynamicModule(string name, bool emitSymbolInfo)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a persistable dynamic module with the given name that will be saved to the specified file. No symbol information is emitted.</summary>
		/// <param name="name">The name of the dynamic module. Must be less than 260 characters in length. </param>
		/// <param name="fileName">The name of the file to which the dynamic module should be saved. </param>
		/// <returns>A <see cref="T:System.Reflection.Emit.ModuleBuilder" /> object representing the defined dynamic module.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="fileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> or <paramref name="fileName" /> is zero.-or- The length of <paramref name="name" /> is greater than or equal to 260.-or- 
		///         <paramref name="fileName" /> contains a path specification (a directory component, for example).-or- There is a conflict with the name of another file that belongs to this assembly. </exception>
		/// <exception cref="T:System.InvalidOperationException">This assembly has been previously saved. </exception>
		/// <exception cref="T:System.NotSupportedException">This assembly was called on a dynamic assembly with <see cref="F:System.Reflection.Emit.AssemblyBuilderAccess.Run" /> attribute. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ExecutionEngineException">The assembly for default symbol writer cannot be loaded.-or- The type that implements the default symbol writer interface cannot be found. </exception>
		// Token: 0x06002431 RID: 9265 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public ModuleBuilder DefineDynamicModule(string name, string fileName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a persistable dynamic module, specifying the module name, the name of the file to which the module will be saved, and whether symbol information should be emitted using the default symbol writer.</summary>
		/// <param name="name">The name of the dynamic module. Must be less than 260 characters in length. </param>
		/// <param name="fileName">The name of the file to which the dynamic module should be saved. </param>
		/// <param name="emitSymbolInfo">If <see langword="true" />, symbolic information is written using the default symbol writer. </param>
		/// <returns>A <see cref="T:System.Reflection.Emit.ModuleBuilder" /> object representing the defined dynamic module.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="fileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> or <paramref name="fileName" /> is zero.-or- The length of <paramref name="name" /> is greater than or equal to 260.-or- 
		///         <paramref name="fileName" /> contains a path specification (a directory component, for example).-or- There is a conflict with the name of another file that belongs to this assembly. </exception>
		/// <exception cref="T:System.InvalidOperationException">This assembly has been previously saved. </exception>
		/// <exception cref="T:System.NotSupportedException">This assembly was called on a dynamic assembly with the <see cref="F:System.Reflection.Emit.AssemblyBuilderAccess.Run" /> attribute. </exception>
		/// <exception cref="T:System.ExecutionEngineException">The assembly for default symbol writer cannot be loaded.-or- The type that implements the default symbol writer interface cannot be found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002432 RID: 9266 RVA: 0x0005AB11 File Offset: 0x00058D11
		[SecuritySafeCritical]
		public ModuleBuilder DefineDynamicModule(string name, string fileName, bool emitSymbolInfo)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a standalone managed resource for this assembly with the default public resource attribute.</summary>
		/// <param name="name">The logical name of the resource. </param>
		/// <param name="description">A textual description of the resource. </param>
		/// <param name="fileName">The physical file name (.resources file) to which the logical name is mapped. This should not include a path. </param>
		/// <returns>A <see cref="T:System.Resources.ResourceWriter" /> object for the specified resource.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> has been previously defined.-or- There is another file in the assembly named <paramref name="fileName" />.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="fileName" /> is zero.-or- 
		///         <paramref name="fileName" /> includes a path. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="fileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002433 RID: 9267 RVA: 0x0005AB11 File Offset: 0x00058D11
		public IResourceWriter DefineResource(string name, string description, string fileName)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines a standalone managed resource for this assembly. Attributes can be specified for the managed resource.</summary>
		/// <param name="name">The logical name of the resource. </param>
		/// <param name="description">A textual description of the resource. </param>
		/// <param name="fileName">The physical file name (.resources file) to which the logical name is mapped. This should not include a path. </param>
		/// <param name="attribute">The resource attributes. </param>
		/// <returns>A <see cref="T:System.Resources.ResourceWriter" /> object for the specified resource.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="name" /> has been previously defined or if there is another file in the assembly named <paramref name="fileName" />.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="fileName" /> is zero.-or- 
		///         <paramref name="fileName" /> includes a path. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> or <paramref name="fileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002434 RID: 9268 RVA: 0x0005AB11 File Offset: 0x00058D11
		public IResourceWriter DefineResource(string name, string description, string fileName, ResourceAttributes attribute)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Defines an unmanaged resource for this assembly as an opaque blob of bytes.</summary>
		/// <param name="resource">The opaque blob of bytes representing the unmanaged resource. </param>
		/// <exception cref="T:System.ArgumentException">An unmanaged resource was previously defined. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="resource" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002435 RID: 9269 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void DefineUnmanagedResource(byte[] resource)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Defines an unmanaged resource file for this assembly given the name of the resource file.</summary>
		/// <param name="resourceFileName">The name of the resource file. </param>
		/// <exception cref="T:System.ArgumentException">An unmanaged resource was previously defined.-or- The file <paramref name="resourceFileName" /> is not readable.-or- 
		///         <paramref name="resourceFileName" /> is the empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="resourceFileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">
		///         <paramref name="resourceFileName" /> is not found.-or- 
		///         <paramref name="resourceFileName" /> is a directory. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002436 RID: 9270 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecuritySafeCritical]
		public void DefineUnmanagedResource(string resourceFileName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Defines an unmanaged version information resource using the information specified in the assembly's AssemblyName object and the assembly's custom attributes.</summary>
		/// <exception cref="T:System.ArgumentException">An unmanaged version information resource was previously defined.-or- The unmanaged version information is too large to persist. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002437 RID: 9271 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void DefineVersionInfoResource()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Defines an unmanaged version information resource for this assembly with the given specifications.</summary>
		/// <param name="product">The name of the product with which this assembly is distributed. </param>
		/// <param name="productVersion">The version of the product with which this assembly is distributed. </param>
		/// <param name="company">The name of the company that produced this assembly. </param>
		/// <param name="copyright">Describes all copyright notices, trademarks, and registered trademarks that apply to this assembly. This should include the full text of all notices, legal symbols, copyright dates, trademark numbers, and so on. In English, this string should be in the format "Copyright Microsoft Corp. 1990-2001". </param>
		/// <param name="trademark">Describes all trademarks and registered trademarks that apply to this assembly. This should include the full text of all notices, legal symbols, trademark numbers, and so on. In English, this string should be in the format "Windows is a trademark of Microsoft Corporation". </param>
		/// <exception cref="T:System.ArgumentException">An unmanaged version information resource was previously defined.-or- The unmanaged version information is too large to persist. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x06002438 RID: 9272 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void DefineVersionInfoResource(string product, string productVersion, string company, string copyright, string trademark)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves this dynamic assembly to disk.</summary>
		/// <param name="assemblyFileName">The file name of the assembly. </param>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="assemblyFileName" /> is 0.-or- There are two or more modules resource files in the assembly with the same name.-or- The target directory of the assembly is invalid.-or- 
		///         <paramref name="assemblyFileName" /> is not a simple file name (for example, has a directory or drive component), or more than one unmanaged resource, including a version information resource, was defined in this assembly.-or- The <see langword="CultureInfo" /> string in <see cref="T:System.Reflection.AssemblyCultureAttribute" /> is not a valid string and <see cref="M:System.Reflection.Emit.AssemblyBuilder.DefineVersionInfoResource(System.String,System.String,System.String,System.String,System.String)" /> was called prior to calling this method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="assemblyFileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This assembly has been saved before.-or- This assembly has access <see langword="Run" /><see cref="T:System.Reflection.Emit.AssemblyBuilderAccess" /></exception>
		/// <exception cref="T:System.IO.IOException">An output error occurs during the save. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has not been called for any of the types in the modules of the assembly to be written to disk. </exception>
		// Token: 0x06002439 RID: 9273 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void Save(string assemblyFileName)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Saves this dynamic assembly to disk, specifying the nature of code in the assembly's executables and the target platform.</summary>
		/// <param name="assemblyFileName">The file name of the assembly.</param>
		/// <param name="portableExecutableKind">A bitwise combination of the <see cref="T:System.Reflection.PortableExecutableKinds" /> values that specifies the nature of the code.</param>
		/// <param name="imageFileMachine">One of the <see cref="T:System.Reflection.ImageFileMachine" /> values that specifies the target platform.</param>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="assemblyFileName" /> is 0.-or- There are two or more modules resource files in the assembly with the same name.-or- The target directory of the assembly is invalid.-or- 
		///         <paramref name="assemblyFileName" /> is not a simple file name (for example, has a directory or drive component), or more than one unmanaged resource, including a version information resources, was defined in this assembly.-or- The <see langword="CultureInfo" /> string in <see cref="T:System.Reflection.AssemblyCultureAttribute" /> is not a valid string and <see cref="M:System.Reflection.Emit.AssemblyBuilder.DefineVersionInfoResource(System.String,System.String,System.String,System.String,System.String)" /> was called prior to calling this method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="assemblyFileName" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">This assembly has been saved before.-or- This assembly has access <see langword="Run" /><see cref="T:System.Reflection.Emit.AssemblyBuilderAccess" /></exception>
		/// <exception cref="T:System.IO.IOException">An output error occurs during the save. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has not been called for any of the types in the modules of the assembly to be written to disk. </exception>
		// Token: 0x0600243A RID: 9274 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecuritySafeCritical]
		public void Save(string assemblyFileName, PortableExecutableKinds portableExecutableKind, ImageFileMachine imageFileMachine)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the entry point for this dynamic assembly, assuming that a console application is being built.</summary>
		/// <param name="entryMethod">A reference to the method that represents the entry point for this dynamic assembly. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="entryMethod" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="entryMethod" /> is not contained within this assembly. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x0600243B RID: 9275 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void SetEntryPoint(MethodInfo entryMethod)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Sets the entry point for this assembly and defines the type of the portable executable (PE file) being built.</summary>
		/// <param name="entryMethod">A reference to the method that represents the entry point for this dynamic assembly. </param>
		/// <param name="fileKind">The type of the assembly executable being built. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="entryMethod" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="entryMethod" /> is not contained within this assembly. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x0600243C RID: 9276 RVA: 0x00002ABD File Offset: 0x00000CBD
		public void SetEntryPoint(MethodInfo entryMethod, PEFileKinds fileKind)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x0600243D RID: 9277 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _AssemblyBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x0600243E RID: 9278 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _AssemblyBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x0600243F RID: 9279 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _AssemblyBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002440 RID: 9280 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _AssemblyBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
