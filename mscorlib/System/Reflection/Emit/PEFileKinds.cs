﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	/// <summary>Specifies the type of the portable executable (PE) file.</summary>
	// Token: 0x02000345 RID: 837
	[ComVisible(true)]
	[Serializable]
	public enum PEFileKinds
	{
		/// <summary>The portable executable (PE) file is a DLL.</summary>
		// Token: 0x0400145C RID: 5212
		Dll = 1,
		/// <summary>The application is a console (not a Windows-based) application.</summary>
		// Token: 0x0400145D RID: 5213
		ConsoleApplication,
		/// <summary>The application is a Windows-based application.</summary>
		// Token: 0x0400145E RID: 5214
		WindowApplication
	}
}
