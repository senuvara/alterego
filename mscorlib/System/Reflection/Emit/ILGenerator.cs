﻿using System;
using System.Diagnostics.SymbolStore;
using System.Runtime.InteropServices;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Generates Microsoft intermediate language (MSIL) instructions.</summary>
	// Token: 0x0200033B RID: 827
	public class ILGenerator : _ILGenerator
	{
		// Token: 0x060024E5 RID: 9445 RVA: 0x00002050 File Offset: 0x00000250
		private ILGenerator()
		{
		}

		/// <summary>Gets the current offset, in bytes, in the Microsoft intermediate language (MSIL) stream that is being emitted by the <see cref="T:System.Reflection.Emit.ILGenerator" />.</summary>
		/// <returns>The offset in the MSIL stream at which the next instruction will be emitted. </returns>
		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x060024E6 RID: 9446 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public int ILOffset
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Begins a catch block.</summary>
		/// <param name="exceptionType">The <see cref="T:System.Type" /> object that represents the exception. </param>
		/// <exception cref="T:System.ArgumentException">The catch block is within a filtered exception. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="exceptionType" /> is <see langword="null" />, and the exception filter block has not returned a value that indicates that finally blocks should be run until this catch block is located. </exception>
		/// <exception cref="T:System.NotSupportedException">The Microsoft intermediate language (MSIL) being generated is not currently in an exception block. </exception>
		// Token: 0x060024E7 RID: 9447 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void BeginCatchBlock(Type exceptionType)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Begins an exception block for a filtered exception.</summary>
		/// <exception cref="T:System.NotSupportedException">The Microsoft intermediate language (MSIL) being generated is not currently in an exception block. -or-This <see cref="T:System.Reflection.Emit.ILGenerator" /> belongs to a <see cref="T:System.Reflection.Emit.DynamicMethod" />.</exception>
		// Token: 0x060024E8 RID: 9448 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void BeginExceptFilterBlock()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Begins an exception block for a non-filtered exception.</summary>
		/// <returns>The label for the end of the block. This will leave you in the correct place to execute finally blocks or to finish the try.</returns>
		// Token: 0x060024E9 RID: 9449 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual Label BeginExceptionBlock()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Begins an exception fault block in the Microsoft intermediate language (MSIL) stream.</summary>
		/// <exception cref="T:System.NotSupportedException">The MSIL being generated is not currently in an exception block. -or-This <see cref="T:System.Reflection.Emit.ILGenerator" /> belongs to a <see cref="T:System.Reflection.Emit.DynamicMethod" />.</exception>
		// Token: 0x060024EA RID: 9450 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void BeginFaultBlock()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Begins a finally block in the Microsoft intermediate language (MSIL) instruction stream.</summary>
		/// <exception cref="T:System.NotSupportedException">The MSIL being generated is not currently in an exception block. </exception>
		// Token: 0x060024EB RID: 9451 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void BeginFinallyBlock()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Begins a lexical scope.</summary>
		/// <exception cref="T:System.NotSupportedException">This <see cref="T:System.Reflection.Emit.ILGenerator" /> belongs to a <see cref="T:System.Reflection.Emit.DynamicMethod" />.</exception>
		// Token: 0x060024EC RID: 9452 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void BeginScope()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Declares a local variable of the specified type.</summary>
		/// <param name="localType">A <see cref="T:System.Type" /> object that represents the type of the local variable. </param>
		/// <returns>The declared local variable.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="localType" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The containing type has been created by the <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> method. </exception>
		// Token: 0x060024ED RID: 9453 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual LocalBuilder DeclareLocal(Type localType)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Declares a local variable of the specified type, optionally pinning the object referred to by the variable.</summary>
		/// <param name="localType">A <see cref="T:System.Type" /> object that represents the type of the local variable.</param>
		/// <param name="pinned">
		///       <see langword="true" /> to pin the object in memory; otherwise, <see langword="false" />.</param>
		/// <returns>A <see cref="T:System.Reflection.Emit.LocalBuilder" /> object that represents the local variable.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="localType" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The containing type has been created by the <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> method.-or-The method body of the enclosing method has been created by the <see cref="M:System.Reflection.Emit.MethodBuilder.CreateMethodBody(System.Byte[],System.Int32)" /> method. </exception>
		/// <exception cref="T:System.NotSupportedException">The method with which this <see cref="T:System.Reflection.Emit.ILGenerator" /> is associated is not represented by a <see cref="T:System.Reflection.Emit.MethodBuilder" />.</exception>
		// Token: 0x060024EE RID: 9454 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual LocalBuilder DeclareLocal(Type localType, bool pinned)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Declares a new label.</summary>
		/// <returns>Returns a new label that can be used as a token for branching.</returns>
		// Token: 0x060024EF RID: 9455 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual Label DefineLabel()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction onto the stream of instructions.</summary>
		/// <param name="opcode">The Microsoft Intermediate Language (MSIL) instruction to be put onto the stream. </param>
		// Token: 0x060024F0 RID: 9456 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and character argument onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be put onto the stream. </param>
		/// <param name="arg">The character argument pushed onto the stream immediately after the instruction. </param>
		// Token: 0x060024F1 RID: 9457 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, byte arg)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and numerical argument onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be put onto the stream. Defined in the <see langword="OpCodes" /> enumeration. </param>
		/// <param name="arg">The numerical argument pushed onto the stream immediately after the instruction. </param>
		// Token: 0x060024F2 RID: 9458 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, double arg)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and numerical argument onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="arg">The <see langword="Int" /> argument pushed onto the stream immediately after the instruction. </param>
		// Token: 0x060024F3 RID: 9459 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, short arg)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and numerical argument onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be put onto the stream. </param>
		/// <param name="arg">The numerical argument pushed onto the stream immediately after the instruction. </param>
		// Token: 0x060024F4 RID: 9460 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, int arg)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and numerical argument onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be put onto the stream. </param>
		/// <param name="arg">The numerical argument pushed onto the stream immediately after the instruction. </param>
		// Token: 0x060024F5 RID: 9461 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, long arg)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and metadata token for the specified constructor onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="con">A <see langword="ConstructorInfo" /> representing a constructor. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> is <see langword="null" />. This exception is new in the .NET Framework 4.</exception>
		// Token: 0x060024F6 RID: 9462 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, ConstructorInfo con)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction onto the Microsoft intermediate language (MSIL) stream and leaves space to include a label when fixes are done.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="label">The label to which to branch from this location. </param>
		// Token: 0x060024F7 RID: 9463 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, Label label)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction onto the Microsoft intermediate language (MSIL) stream and leaves space to include a label when fixes are done.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="labels">The array of label objects to which to branch from this location. All of the labels will be used. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> is <see langword="null" />. This exception is new in the .NET Framework 4.</exception>
		// Token: 0x060024F8 RID: 9464 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, Label[] labels)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction onto the Microsoft intermediate language (MSIL) stream followed by the index of the given local variable.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="local">A local variable. </param>
		/// <exception cref="T:System.ArgumentException">The parent method of the <paramref name="local" /> parameter does not match the method associated with this <see cref="T:System.Reflection.Emit.ILGenerator" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="local" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="opcode" />
		///
		///          is a single-byte instruction, and <paramref name="local" /> represents a local variable with an index greater than <see langword="Byte.MaxValue" />. </exception>
		// Token: 0x060024F9 RID: 9465 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, LocalBuilder local)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and a signature token onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="signature">A helper for constructing a signature token. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="signature" /> is <see langword="null" />. </exception>
		// Token: 0x060024FA RID: 9466 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, SignatureHelper signature)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and metadata token for the specified field onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="field">A <see langword="FieldInfo" /> representing a field. </param>
		// Token: 0x060024FB RID: 9467 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, FieldInfo field)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction onto the Microsoft intermediate language (MSIL) stream followed by the metadata token for the given method.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="meth">A <see langword="MethodInfo" /> representing a method. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="meth" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///         <paramref name="meth" /> is a generic method for which the <see cref="P:System.Reflection.MethodInfo.IsGenericMethodDefinition" /> property is <see langword="false" />.</exception>
		// Token: 0x060024FC RID: 9468 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, MethodInfo meth)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and character argument onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be put onto the stream. </param>
		/// <param name="arg">The character argument pushed onto the stream immediately after the instruction. </param>
		// Token: 0x060024FD RID: 9469 RVA: 0x0005A2B6 File Offset: 0x000584B6
		[CLSCompliant(false)]
		public void Emit(OpCode opcode, sbyte arg)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction and numerical argument onto the Microsoft intermediate language (MSIL) stream of instructions.</summary>
		/// <param name="opcode">The MSIL instruction to be put onto the stream. </param>
		/// <param name="arg">The <see langword="Single" /> argument pushed onto the stream immediately after the instruction. </param>
		// Token: 0x060024FE RID: 9470 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, float arg)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction onto the Microsoft intermediate language (MSIL) stream followed by the metadata token for the given string.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. </param>
		/// <param name="str">The <see langword="String" /> to be emitted. </param>
		// Token: 0x060024FF RID: 9471 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, string str)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts the specified instruction onto the Microsoft intermediate language (MSIL) stream followed by the metadata token for the given type.</summary>
		/// <param name="opcode">The MSIL instruction to be put onto the stream. </param>
		/// <param name="cls">A <see langword="Type" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="cls" /> is <see langword="null" />. </exception>
		// Token: 0x06002500 RID: 9472 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void Emit(OpCode opcode, Type cls)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts a <see langword="call" /> or <see langword="callvirt" /> instruction onto the Microsoft intermediate language (MSIL) stream to call a <see langword="varargs" /> method.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. Must be <see cref="F:System.Reflection.Emit.OpCodes.Call" />, <see cref="F:System.Reflection.Emit.OpCodes.Callvirt" />, or <see cref="F:System.Reflection.Emit.OpCodes.Newobj" />.</param>
		/// <param name="methodInfo">The <see langword="varargs" /> method to be called. </param>
		/// <param name="optionalParameterTypes">The types of the optional arguments if the method is a <see langword="varargs" /> method; otherwise, <see langword="null" />. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="opcode" /> does not specify a method call.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="methodInfo" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">The calling convention for the method is not <see langword="varargs" />, but optional parameter types are supplied. This exception is thrown in the .NET Framework versions 1.0 and 1.1, In subsequent versions, no exception is thrown.</exception>
		// Token: 0x06002501 RID: 9473 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void EmitCall(OpCode opcode, MethodInfo methodInfo, Type[] optionalParameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts a <see cref="F:System.Reflection.Emit.OpCodes.Calli" /> instruction onto the Microsoft intermediate language (MSIL) stream, specifying a managed calling convention for the indirect call.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. Must be <see cref="F:System.Reflection.Emit.OpCodes.Calli" />. </param>
		/// <param name="callingConvention">The managed calling convention to be used. </param>
		/// <param name="returnType">The <see cref="T:System.Type" /> of the result. </param>
		/// <param name="parameterTypes">The types of the required arguments to the instruction. </param>
		/// <param name="optionalParameterTypes">The types of the optional arguments for <see langword="varargs" /> calls. </param>
		/// <exception cref="T:System.InvalidOperationException">
		///         <paramref name="optionalParameterTypes" /> is not <see langword="null" />, but <paramref name="callingConvention" /> does not include the <see cref="F:System.Reflection.CallingConventions.VarArgs" /> flag.</exception>
		// Token: 0x06002502 RID: 9474 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void EmitCalli(OpCode opcode, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Emits the Microsoft intermediate language (MSIL) necessary to call <see cref="Overload:System.Console.WriteLine" /> with the given local variable.</summary>
		/// <param name="localBuilder">The local variable whose value is to be written to the console. </param>
		/// <exception cref="T:System.ArgumentException">The type of <paramref name="localBuilder" /> is <see cref="T:System.Reflection.Emit.TypeBuilder" /> or <see cref="T:System.Reflection.Emit.EnumBuilder" />, which are not supported. -or-There is no overload of <see cref="Overload:System.Console.WriteLine" /> that accepts the type of <paramref name="localBuilder" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="localBuilder" /> is <see langword="null" />. </exception>
		// Token: 0x06002503 RID: 9475 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void EmitWriteLine(LocalBuilder localBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Emits the Microsoft intermediate language (MSIL) necessary to call <see cref="Overload:System.Console.WriteLine" /> with the given field.</summary>
		/// <param name="fld">The field whose value is to be written to the console. </param>
		/// <exception cref="T:System.ArgumentException">There is no overload of the <see cref="Overload:System.Console.WriteLine" /> method that accepts the type of the specified field. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="fld" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The type of the field is <see cref="T:System.Reflection.Emit.TypeBuilder" /> or <see cref="T:System.Reflection.Emit.EnumBuilder" />, which are not supported. </exception>
		// Token: 0x06002504 RID: 9476 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void EmitWriteLine(FieldInfo fld)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Emits the Microsoft intermediate language (MSIL) to call <see cref="Overload:System.Console.WriteLine" /> with a string.</summary>
		/// <param name="value">The string to be printed. </param>
		// Token: 0x06002505 RID: 9477 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void EmitWriteLine(string value)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Ends an exception block.</summary>
		/// <exception cref="T:System.InvalidOperationException">The end exception block occurs in an unexpected place in the code stream. </exception>
		/// <exception cref="T:System.NotSupportedException">The Microsoft intermediate language (MSIL) being generated is not currently in an exception block. </exception>
		// Token: 0x06002506 RID: 9478 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void EndExceptionBlock()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Ends a lexical scope.</summary>
		/// <exception cref="T:System.NotSupportedException">This <see cref="T:System.Reflection.Emit.ILGenerator" /> belongs to a <see cref="T:System.Reflection.Emit.DynamicMethod" />.</exception>
		// Token: 0x06002507 RID: 9479 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void EndScope()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Marks the Microsoft intermediate language (MSIL) stream's current position with the given label.</summary>
		/// <param name="loc">The label for which to set an index. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="loc" /> represents an invalid index into the label array.-or- An index for <paramref name="loc" /> has already been defined. </exception>
		// Token: 0x06002508 RID: 9480 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void MarkLabel(Label loc)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Emits an instruction to throw an exception.</summary>
		/// <param name="excType">The class of the type of exception to throw. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="excType" /> is not the <see cref="T:System.Exception" /> class or a derived class of <see cref="T:System.Exception" />.-or- The type does not have a default constructor. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="excType" /> is <see langword="null" />. </exception>
		// Token: 0x06002509 RID: 9481 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void ThrowException(Type excType)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Specifies the namespace to be used in evaluating locals and watches for the current active lexical scope.</summary>
		/// <param name="usingNamespace">The namespace to be used in evaluating locals and watches for the current active lexical scope </param>
		/// <exception cref="T:System.ArgumentException">Length of <paramref name="usingNamespace" /> is zero. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="usingNamespace" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.NotSupportedException">This <see cref="T:System.Reflection.Emit.ILGenerator" /> belongs to a <see cref="T:System.Reflection.Emit.DynamicMethod" />.</exception>
		// Token: 0x0600250A RID: 9482 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public virtual void UsingNamespace(string usingNamespace)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Puts a <see cref="F:System.Reflection.Emit.OpCodes.Calli" /> instruction onto the Microsoft intermediate language (MSIL) stream, specifying an unmanaged calling convention for the indirect call.</summary>
		/// <param name="opcode">The MSIL instruction to be emitted onto the stream. Must be <see cref="F:System.Reflection.Emit.OpCodes.Calli" />.</param>
		/// <param name="unmanagedCallConv">The unmanaged calling convention to be used. </param>
		/// <param name="returnType">The <see cref="T:System.Type" /> of the result. </param>
		/// <param name="parameterTypes">The types of the required arguments to the instruction. </param>
		// Token: 0x0600250B RID: 9483 RVA: 0x00002ABD File Offset: 0x00000CBD
		public virtual void EmitCalli(OpCode opcode, CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Marks a sequence point in the Microsoft intermediate language (MSIL) stream.</summary>
		/// <param name="document">The document for which the sequence point is being defined. </param>
		/// <param name="startLine">The line where the sequence point begins. </param>
		/// <param name="startColumn">The column in the line where the sequence point begins. </param>
		/// <param name="endLine">The line where the sequence point ends. </param>
		/// <param name="endColumn">The column in the line where the sequence point ends. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="startLine" /> or <paramref name="endLine" /> is &lt;= 0. </exception>
		/// <exception cref="T:System.NotSupportedException">This <see cref="T:System.Reflection.Emit.ILGenerator" /> belongs to a <see cref="T:System.Reflection.Emit.DynamicMethod" />.</exception>
		// Token: 0x0600250C RID: 9484 RVA: 0x00002ABD File Offset: 0x00000CBD
		public virtual void MarkSequencePoint(ISymbolDocumentWriter document, int startLine, int startColumn, int endLine, int endColumn)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array that receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x0600250D RID: 9485 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ILGenerator.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x0600250E RID: 9486 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ILGenerator.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x0600250F RID: 9487 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ILGenerator.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		// Token: 0x06002510 RID: 9488 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ILGenerator.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}
	}
}
