﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using Unity;

namespace System.Reflection.Emit
{
	/// <summary>Defines and creates generic type parameters for dynamically defined generic types and methods. This class cannot be inherited. </summary>
	// Token: 0x0200033A RID: 826
	public abstract class GenericTypeParameterBuilder : TypeInfo
	{
		/// <summary>Gets an <see cref="T:System.Reflection.Assembly" /> object representing the dynamic assembly that contains the generic type definition the current type parameter belongs to.</summary>
		/// <returns>An <see cref="T:System.Reflection.Assembly" /> object representing the dynamic assembly that contains the generic type definition the current type parameter belongs to.</returns>
		// Token: 0x170005AA RID: 1450
		// (get) Token: 0x060024BB RID: 9403 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Assembly Assembly
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets <see langword="null" /> in all cases.</summary>
		/// <returns>A null reference (<see langword="Nothing" /> in Visual Basic) in all cases.</returns>
		// Token: 0x170005AB RID: 1451
		// (get) Token: 0x060024BC RID: 9404 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string AssemblyQualifiedName
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets the base type constraint of the current generic type parameter.</summary>
		/// <returns>A <see cref="T:System.Type" /> object that represents the base type constraint of the generic type parameter, or <see langword="null" /> if the type parameter has no base type constraint.</returns>
		// Token: 0x170005AC RID: 1452
		// (get) Token: 0x060024BD RID: 9405 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Type BaseType
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets <see langword="null" /> in all cases.</summary>
		/// <returns>A null reference (<see langword="Nothing" /> in Visual Basic) in all cases.</returns>
		// Token: 0x170005AD RID: 1453
		// (get) Token: 0x060024BE RID: 9406 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string FullName
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases. </exception>
		// Token: 0x170005AE RID: 1454
		// (get) Token: 0x060024BF RID: 9407 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Guid GUID
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets the dynamic module that contains the generic type parameter.</summary>
		/// <returns>A <see cref="T:System.Reflection.Module" /> object that represents the dynamic module that contains the generic type parameter.</returns>
		// Token: 0x170005AF RID: 1455
		// (get) Token: 0x060024C0 RID: 9408 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Module Module
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets the name of the generic type parameter.</summary>
		/// <returns>The name of the generic type parameter.</returns>
		// Token: 0x170005B0 RID: 1456
		// (get) Token: 0x060024C1 RID: 9409 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string Name
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Gets <see langword="null" /> in all cases.</summary>
		/// <returns>A null reference (<see langword="Nothing" /> in Visual Basic) in all cases.</returns>
		// Token: 0x170005B1 RID: 1457
		// (get) Token: 0x060024C2 RID: 9410 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override string Namespace
		{
			get
			{
				throw new PlatformNotSupportedException();
			}
		}

		/// <summary>Throws a <see cref="T:System.NotSupportedException" /> in all cases. </summary>
		/// <returns>The type referred to by the current array type, pointer type, or <see langword="ByRef" /> type; or <see langword="null" /> if the current type is not an array type, is not a pointer type, and is not passed by reference.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024C3 RID: 9411 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public override Type GetElementType()
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the base type that a type must inherit in order to be substituted for the type parameter.</summary>
		/// <param name="baseTypeConstraint">The <see cref="T:System.Type" /> that must be inherited by any type that is to be substituted for the type parameter.</param>
		// Token: 0x060024C4 RID: 9412 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetBaseTypeConstraint(Type baseTypeConstraint)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Set a custom attribute using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class that defines the custom attribute.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="customBuilder" /> is <see langword="null" />.</exception>
		// Token: 0x060024C5 RID: 9413 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets a custom attribute using a specified custom attribute blob.</summary>
		/// <param name="con">The constructor for the custom attribute.</param>
		/// <param name="binaryAttribute">A byte blob representing the attribute.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="con" /> is <see langword="null" />.-or-
		///         <paramref name="binaryAttribute" /> is a null reference.</exception>
		// Token: 0x060024C6 RID: 9414 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the variance characteristics and special constraints of the generic parameter, such as the parameterless constructor constraint.</summary>
		/// <param name="genericParameterAttributes">A bitwise combination of <see cref="T:System.Reflection.GenericParameterAttributes" /> values that represent the variance characteristics and special constraints of the generic type parameter.</param>
		// Token: 0x060024C7 RID: 9415 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetGenericParameterAttributes(GenericParameterAttributes genericParameterAttributes)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Sets the interfaces a type must implement in order to be substituted for the type parameter. </summary>
		/// <param name="interfaceConstraints">An array of <see cref="T:System.Type" /> objects that represent the interfaces a type must implement in order to be substituted for the type parameter.</param>
		// Token: 0x060024C8 RID: 9416 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public void SetInterfaceConstraints(Type[] interfaceConstraints)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x060024C9 RID: 9417 RVA: 0x0007DAC7 File Offset: 0x0007BCC7
		protected GenericTypeParameterBuilder()
		{
		}

		/// <summary>Gets the current generic type parameter.</summary>
		/// <returns>The current <see cref="T:System.Reflection.Emit.GenericTypeParameterBuilder" /> object.</returns>
		// Token: 0x170005B2 RID: 1458
		// (get) Token: 0x060024CA RID: 9418 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type UnderlyingSystemType
		{
			get
			{
				ThrowStub.ThrowNotSupportedException();
				return null;
			}
		}

		// Token: 0x060024CB RID: 9419 RVA: 0x00082C70 File Offset: 0x00080E70
		protected override TypeAttributes GetAttributeFlagsImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return TypeAttributes.NotPublic;
		}

		// Token: 0x060024CC RID: 9420 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases. </exception>
		// Token: 0x060024CD RID: 9421 RVA: 0x0005AB11 File Offset: 0x00058D11
		[ComVisible(true)]
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024CE RID: 9422 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object[] GetCustomAttributes(bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="attributeType">The type of attribute to search for. Only attributes that are assignable to this type are returned.</param>
		/// <param name="inherit">Specifies whether to search this member's inheritance chain to find the attributes.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024CF RID: 9423 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="name">Not supported.</param>
		/// <param name="bindingAttr">Not supported. </param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D0 RID: 9424 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D1 RID: 9425 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override EventInfo[] GetEvents(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="name">Not supported.</param>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D2 RID: 9426 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D3 RID: 9427 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override FieldInfo[] GetFields(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="name">The name of the interface.</param>
		/// <param name="ignoreCase">
		///       <see langword="true" /> to search without regard for case; <see langword="false" /> to make a case-sensitive search.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D4 RID: 9428 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type GetInterface(string name, bool ignoreCase)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D5 RID: 9429 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type[] GetInterfaces()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D6 RID: 9430 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060024D7 RID: 9431 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D8 RID: 9432 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="name">Not supported.</param>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024D9 RID: 9433 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type GetNestedType(string name, BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024DA RID: 9434 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override Type[] GetNestedTypes(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="bindingAttr">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024DB RID: 9435 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060024DC RID: 9436 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060024DD RID: 9437 RVA: 0x00082C8C File Offset: 0x00080E8C
		protected override bool HasElementTypeImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="name">Not supported. </param>
		/// <param name="invokeAttr">Not supported.</param>
		/// <param name="binder">Not supported.</param>
		/// <param name="target">Not supported.</param>
		/// <param name="args">Not supported.</param>
		/// <param name="modifiers">Not supported.</param>
		/// <param name="culture">Not supported.</param>
		/// <param name="namedParameters">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024DE RID: 9438 RVA: 0x0005AB11 File Offset: 0x00058D11
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x060024DF RID: 9439 RVA: 0x00082CA8 File Offset: 0x00080EA8
		protected override bool IsArrayImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x060024E0 RID: 9440 RVA: 0x00082CC4 File Offset: 0x00080EC4
		protected override bool IsByRefImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x060024E1 RID: 9441 RVA: 0x00082CE0 File Offset: 0x00080EE0
		protected override bool IsCOMObjectImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		/// <summary>Not supported for incomplete generic type parameters.</summary>
		/// <param name="attributeType">Not supported.</param>
		/// <param name="inherit">Not supported.</param>
		/// <returns>Not supported for incomplete generic type parameters.</returns>
		/// <exception cref="T:System.NotSupportedException">In all cases.</exception>
		// Token: 0x060024E2 RID: 9442 RVA: 0x00082CFC File Offset: 0x00080EFC
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x060024E3 RID: 9443 RVA: 0x00082D18 File Offset: 0x00080F18
		protected override bool IsPointerImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}

		// Token: 0x060024E4 RID: 9444 RVA: 0x00082D34 File Offset: 0x00080F34
		protected override bool IsPrimitiveImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(bool);
		}
	}
}
