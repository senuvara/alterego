﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	/// <summary>Describes the operand type of Microsoft intermediate language (MSIL) instruction.</summary>
	// Token: 0x02000344 RID: 836
	[ComVisible(true)]
	[Serializable]
	public enum OperandType
	{
		/// <summary>The operand is a 32-bit integer branch target.</summary>
		// Token: 0x04001449 RID: 5193
		InlineBrTarget,
		/// <summary>The operand is a 32-bit metadata token.</summary>
		// Token: 0x0400144A RID: 5194
		InlineField,
		/// <summary>The operand is a 32-bit integer.</summary>
		// Token: 0x0400144B RID: 5195
		InlineI,
		/// <summary>The operand is a 64-bit integer.</summary>
		// Token: 0x0400144C RID: 5196
		InlineI8,
		/// <summary>The operand is a 32-bit metadata token.</summary>
		// Token: 0x0400144D RID: 5197
		InlineMethod,
		/// <summary>No operand.</summary>
		// Token: 0x0400144E RID: 5198
		InlineNone,
		/// <summary>The operand is reserved and should not be used.</summary>
		// Token: 0x0400144F RID: 5199
		[Obsolete("This API has been deprecated.")]
		InlinePhi,
		/// <summary>The operand is a 64-bit IEEE floating point number.</summary>
		// Token: 0x04001450 RID: 5200
		InlineR,
		/// <summary>The operand is a 32-bit metadata signature token.</summary>
		// Token: 0x04001451 RID: 5201
		InlineSig = 9,
		/// <summary>The operand is a 32-bit metadata string token.</summary>
		// Token: 0x04001452 RID: 5202
		InlineString,
		/// <summary>The operand is the 32-bit integer argument to a switch instruction.</summary>
		// Token: 0x04001453 RID: 5203
		InlineSwitch,
		/// <summary>The operand is a <see langword="FieldRef" />, <see langword="MethodRef" />, or <see langword="TypeRef" /> token.</summary>
		// Token: 0x04001454 RID: 5204
		InlineTok,
		/// <summary>The operand is a 32-bit metadata token.</summary>
		// Token: 0x04001455 RID: 5205
		InlineType,
		/// <summary>The operand is 16-bit integer containing the ordinal of a local variable or an argument.</summary>
		// Token: 0x04001456 RID: 5206
		InlineVar,
		/// <summary>The operand is an 8-bit integer branch target.</summary>
		// Token: 0x04001457 RID: 5207
		ShortInlineBrTarget,
		/// <summary>The operand is an 8-bit integer.</summary>
		// Token: 0x04001458 RID: 5208
		ShortInlineI,
		/// <summary>The operand is a 32-bit IEEE floating point number.</summary>
		// Token: 0x04001459 RID: 5209
		ShortInlineR,
		/// <summary>The operand is an 8-bit integer containing the ordinal of a local variable or an argumenta.</summary>
		// Token: 0x0400145A RID: 5210
		ShortInlineVar
	}
}
