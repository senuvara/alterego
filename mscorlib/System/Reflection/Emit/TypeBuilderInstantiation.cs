﻿using System;

namespace System.Reflection.Emit
{
	// Token: 0x0200034C RID: 844
	internal abstract class TypeBuilderInstantiation : TypeInfo
	{
		// Token: 0x06002620 RID: 9760 RVA: 0x00084E0F File Offset: 0x0008300F
		internal static Type MakeGenericType(Type type, Type[] typeArguments)
		{
			throw new NotSupportedException("User types are not supported under full aot");
		}

		// Token: 0x06002621 RID: 9761 RVA: 0x0007DAC7 File Offset: 0x0007BCC7
		protected TypeBuilderInstantiation()
		{
		}
	}
}
