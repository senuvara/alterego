﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	/// <summary>The exception that is thrown by methods invoked through reflection. This class cannot be inherited.</summary>
	// Token: 0x020002F5 RID: 757
	[ComVisible(true)]
	[Serializable]
	public sealed class TargetInvocationException : ApplicationException
	{
		// Token: 0x060020A9 RID: 8361 RVA: 0x0007DA1D File Offset: 0x0007BC1D
		private TargetInvocationException() : base(Environment.GetResourceString("Exception has been thrown by the target of an invocation."))
		{
			base.SetErrorCode(-2146232828);
		}

		// Token: 0x060020AA RID: 8362 RVA: 0x0007DA3A File Offset: 0x0007BC3A
		private TargetInvocationException(string message) : base(message)
		{
			base.SetErrorCode(-2146232828);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.TargetInvocationException" /> class with a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060020AB RID: 8363 RVA: 0x0007DA4E File Offset: 0x0007BC4E
		public TargetInvocationException(Exception inner) : base(Environment.GetResourceString("Exception has been thrown by the target of an invocation."), inner)
		{
			base.SetErrorCode(-2146232828);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.TargetInvocationException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060020AC RID: 8364 RVA: 0x0007DA6C File Offset: 0x0007BC6C
		public TargetInvocationException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146232828);
		}

		// Token: 0x060020AD RID: 8365 RVA: 0x0007CA25 File Offset: 0x0007AC25
		internal TargetInvocationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
