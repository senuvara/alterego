﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Provides access to manifest resources, which are XML files that describe application dependencies.  </summary>
	// Token: 0x020002D9 RID: 729
	[ComVisible(true)]
	public class ManifestResourceInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.ManifestResourceInfo" /> class for a resource that is contained by the specified assembly and file, and that has the specified location.</summary>
		/// <param name="containingAssembly">The assembly that contains the manifest resource.</param>
		/// <param name="containingFileName">The name of the file that contains the manifest resource, if the file is not the same as the manifest file.</param>
		/// <param name="resourceLocation">A bitwise combination of enumeration values that provides information about the location of the manifest resource.</param>
		// Token: 0x06001FFE RID: 8190 RVA: 0x0007CA2F File Offset: 0x0007AC2F
		public ManifestResourceInfo(Assembly containingAssembly, string containingFileName, ResourceLocation resourceLocation)
		{
			this._containingAssembly = containingAssembly;
			this._containingFileName = containingFileName;
			this._resourceLocation = resourceLocation;
		}

		/// <summary>Gets the containing assembly for the manifest resource. </summary>
		/// <returns>The manifest resource's containing assembly.</returns>
		// Token: 0x17000471 RID: 1137
		// (get) Token: 0x06001FFF RID: 8191 RVA: 0x0007CA4C File Offset: 0x0007AC4C
		public virtual Assembly ReferencedAssembly
		{
			get
			{
				return this._containingAssembly;
			}
		}

		/// <summary>Gets the name of the file that contains the manifest resource, if it is not the same as the manifest file. </summary>
		/// <returns>The manifest resource's file name.</returns>
		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x06002000 RID: 8192 RVA: 0x0007CA54 File Offset: 0x0007AC54
		public virtual string FileName
		{
			get
			{
				return this._containingFileName;
			}
		}

		/// <summary>Gets the manifest resource's location. </summary>
		/// <returns>A bitwise combination of <see cref="T:System.Reflection.ResourceLocation" /> flags that indicates the location of the manifest resource.</returns>
		// Token: 0x17000473 RID: 1139
		// (get) Token: 0x06002001 RID: 8193 RVA: 0x0007CA5C File Offset: 0x0007AC5C
		public virtual ResourceLocation ResourceLocation
		{
			get
			{
				return this._resourceLocation;
			}
		}

		// Token: 0x04001190 RID: 4496
		private Assembly _containingAssembly;

		// Token: 0x04001191 RID: 4497
		private string _containingFileName;

		// Token: 0x04001192 RID: 4498
		private ResourceLocation _resourceLocation;
	}
}
