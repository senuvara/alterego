﻿using System;

namespace System.Reflection
{
	// Token: 0x02000324 RID: 804
	[Flags]
	internal enum PInfo
	{
		// Token: 0x0400131A RID: 4890
		Attributes = 1,
		// Token: 0x0400131B RID: 4891
		GetMethod = 2,
		// Token: 0x0400131C RID: 4892
		SetMethod = 4,
		// Token: 0x0400131D RID: 4893
		ReflectedType = 8,
		// Token: 0x0400131E RID: 4894
		DeclaringType = 16,
		// Token: 0x0400131F RID: 4895
		Name = 32
	}
}
