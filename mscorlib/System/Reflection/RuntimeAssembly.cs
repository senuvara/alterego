﻿using System;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Policy;
using System.Threading;

namespace System.Reflection
{
	// Token: 0x02000312 RID: 786
	internal abstract class RuntimeAssembly : Assembly
	{
		// Token: 0x060022AA RID: 8874 RVA: 0x000804DA File Offset: 0x0007E6DA
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			UnitySerializationHolder.GetUnitySerializationInfo(info, 6, this.FullName, this);
		}

		// Token: 0x060022AB RID: 8875 RVA: 0x000175EA File Offset: 0x000157EA
		internal static RuntimeAssembly GetExecutingAssembly(ref StackCrawlMark stackMark)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060022AC RID: 8876 RVA: 0x000804F8 File Offset: 0x0007E6F8
		[SecurityCritical]
		internal static AssemblyName CreateAssemblyName(string assemblyString, bool forIntrospection, out RuntimeAssembly assemblyFromResolveEvent)
		{
			if (assemblyString == null)
			{
				throw new ArgumentNullException("assemblyString");
			}
			if (assemblyString.Length == 0 || assemblyString[0] == '\0')
			{
				throw new ArgumentException(Environment.GetResourceString("String cannot have zero length."));
			}
			if (forIntrospection)
			{
				AppDomain.CheckReflectionOnlyLoadSupported();
			}
			AssemblyName assemblyName = new AssemblyName();
			assemblyName.Name = assemblyString;
			assemblyFromResolveEvent = null;
			return assemblyName;
		}

		// Token: 0x060022AD RID: 8877 RVA: 0x0008054B File Offset: 0x0007E74B
		internal static RuntimeAssembly InternalLoadAssemblyName(AssemblyName assemblyRef, Evidence assemblySecurity, RuntimeAssembly reqAssembly, ref StackCrawlMark stackMark, bool throwOnFileNotFound, bool forIntrospection, bool suppressSecurityChecks)
		{
			if (assemblyRef == null)
			{
				throw new ArgumentNullException("assemblyRef");
			}
			if (assemblyRef.CodeBase != null)
			{
				AppDomain.CheckLoadFromSupported();
			}
			assemblyRef = (AssemblyName)assemblyRef.Clone();
			if (assemblySecurity != null)
			{
			}
			return (RuntimeAssembly)Assembly.Load(assemblyRef);
		}

		// Token: 0x060022AE RID: 8878 RVA: 0x00080586 File Offset: 0x0007E786
		internal static RuntimeAssembly LoadWithPartialNameInternal(string partialName, Evidence securityEvidence, ref StackCrawlMark stackMark)
		{
			return (RuntimeAssembly)Assembly.LoadWithPartialName(partialName, securityEvidence);
		}

		// Token: 0x060022AF RID: 8879 RVA: 0x00080594 File Offset: 0x0007E794
		internal static RuntimeAssembly LoadWithPartialNameInternal(AssemblyName an, Evidence securityEvidence, ref StackCrawlMark stackMark)
		{
			return RuntimeAssembly.LoadWithPartialNameInternal(an.ToString(), securityEvidence, ref stackMark);
		}

		// Token: 0x060022B0 RID: 8880 RVA: 0x000805A3 File Offset: 0x0007E7A3
		public override AssemblyName GetName(bool copiedName)
		{
			return AssemblyName.Create(this, true);
		}

		// Token: 0x060022B1 RID: 8881 RVA: 0x000805AC File Offset: 0x0007E7AC
		protected RuntimeAssembly()
		{
		}
	}
}
