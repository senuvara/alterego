﻿using System;

namespace System.Reflection
{
	// Token: 0x020002DF RID: 735
	[Serializable]
	internal enum MetadataTokenType
	{
		// Token: 0x040011EB RID: 4587
		Module,
		// Token: 0x040011EC RID: 4588
		TypeRef = 16777216,
		// Token: 0x040011ED RID: 4589
		TypeDef = 33554432,
		// Token: 0x040011EE RID: 4590
		FieldDef = 67108864,
		// Token: 0x040011EF RID: 4591
		MethodDef = 100663296,
		// Token: 0x040011F0 RID: 4592
		ParamDef = 134217728,
		// Token: 0x040011F1 RID: 4593
		InterfaceImpl = 150994944,
		// Token: 0x040011F2 RID: 4594
		MemberRef = 167772160,
		// Token: 0x040011F3 RID: 4595
		CustomAttribute = 201326592,
		// Token: 0x040011F4 RID: 4596
		Permission = 234881024,
		// Token: 0x040011F5 RID: 4597
		Signature = 285212672,
		// Token: 0x040011F6 RID: 4598
		Event = 335544320,
		// Token: 0x040011F7 RID: 4599
		Property = 385875968,
		// Token: 0x040011F8 RID: 4600
		ModuleRef = 436207616,
		// Token: 0x040011F9 RID: 4601
		TypeSpec = 452984832,
		// Token: 0x040011FA RID: 4602
		Assembly = 536870912,
		// Token: 0x040011FB RID: 4603
		AssemblyRef = 587202560,
		// Token: 0x040011FC RID: 4604
		File = 637534208,
		// Token: 0x040011FD RID: 4605
		ExportedType = 654311424,
		// Token: 0x040011FE RID: 4606
		ManifestResource = 671088640,
		// Token: 0x040011FF RID: 4607
		GenericPar = 704643072,
		// Token: 0x04001200 RID: 4608
		MethodSpec = 721420288,
		// Token: 0x04001201 RID: 4609
		String = 1879048192,
		// Token: 0x04001202 RID: 4610
		Name = 1895825408,
		// Token: 0x04001203 RID: 4611
		BaseType = 1912602624,
		// Token: 0x04001204 RID: 4612
		Invalid = 2147483647
	}
}
