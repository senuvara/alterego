﻿using System;
using System.Collections.Generic;

namespace System.Reflection
{
	/// <summary>Provides methods that retrieve information about types at run time.</summary>
	// Token: 0x020002B3 RID: 691
	public static class RuntimeReflectionExtensions
	{
		// Token: 0x06001FA1 RID: 8097 RVA: 0x0007C632 File Offset: 0x0007A832
		private static void CheckAndThrow(Type t)
		{
			if (t == null)
			{
				throw new ArgumentNullException("type");
			}
			if (!(t is RuntimeType))
			{
				throw new ArgumentException(Environment.GetResourceString("Type must be a runtime Type object."));
			}
		}

		// Token: 0x06001FA2 RID: 8098 RVA: 0x0007C660 File Offset: 0x0007A860
		private static void CheckAndThrow(MethodInfo m)
		{
			if (m == null)
			{
				throw new ArgumentNullException("method");
			}
			if (!(m is RuntimeMethodInfo))
			{
				throw new ArgumentException(Environment.GetResourceString("MethodInfo must be a runtime MethodInfo object."));
			}
		}

		/// <summary>Retrieves a collection that represents all the properties defined on a specified type.</summary>
		/// <param name="type">The type that contains the properties.</param>
		/// <returns>A collection of properties for the specified type.</returns>
		// Token: 0x06001FA3 RID: 8099 RVA: 0x0007C68E File Offset: 0x0007A88E
		public static IEnumerable<PropertyInfo> GetRuntimeProperties(this Type type)
		{
			RuntimeReflectionExtensions.CheckAndThrow(type);
			return type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>Retrieves a collection that represents all the events defined on a specified type.</summary>
		/// <param name="type">The type that contains the events.</param>
		/// <returns>A collection of events for the specified type.</returns>
		// Token: 0x06001FA4 RID: 8100 RVA: 0x0007C69E File Offset: 0x0007A89E
		public static IEnumerable<EventInfo> GetRuntimeEvents(this Type type)
		{
			RuntimeReflectionExtensions.CheckAndThrow(type);
			return type.GetEvents(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>Retrieves a collection that represents all methods defined on a specified type.</summary>
		/// <param name="type">The type that contains the methods.</param>
		/// <returns>A collection of methods for the specified type.</returns>
		// Token: 0x06001FA5 RID: 8101 RVA: 0x0007C6AE File Offset: 0x0007A8AE
		public static IEnumerable<MethodInfo> GetRuntimeMethods(this Type type)
		{
			RuntimeReflectionExtensions.CheckAndThrow(type);
			return type.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>Retrieves a collection that represents all the fields defined on a specified type.</summary>
		/// <param name="type">The type that contains the fields.</param>
		/// <returns>A collection of fields for the specified type.</returns>
		// Token: 0x06001FA6 RID: 8102 RVA: 0x0007C6BE File Offset: 0x0007A8BE
		public static IEnumerable<FieldInfo> GetRuntimeFields(this Type type)
		{
			RuntimeReflectionExtensions.CheckAndThrow(type);
			return type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>Retrieves an object that represents a specified property.</summary>
		/// <param name="type">The type that contains the property.</param>
		/// <param name="name">The name of the property.</param>
		/// <returns>An object that represents the specified property, or <see langword="null" /> if the property is not found.</returns>
		// Token: 0x06001FA7 RID: 8103 RVA: 0x0007C6CE File Offset: 0x0007A8CE
		public static PropertyInfo GetRuntimeProperty(this Type type, string name)
		{
			RuntimeReflectionExtensions.CheckAndThrow(type);
			return type.GetProperty(name);
		}

		/// <summary>Retrieves an object that represents the specified event.</summary>
		/// <param name="type">The type that contains the event.</param>
		/// <param name="name">The name of the event.</param>
		/// <returns>An object that represents the specified event, or <see langword="null" /> if the event is not found.</returns>
		// Token: 0x06001FA8 RID: 8104 RVA: 0x0007C6DD File Offset: 0x0007A8DD
		public static EventInfo GetRuntimeEvent(this Type type, string name)
		{
			RuntimeReflectionExtensions.CheckAndThrow(type);
			return type.GetEvent(name);
		}

		/// <summary>Retrieves an object that represents a specified method.</summary>
		/// <param name="type">The type that contains the method.</param>
		/// <param name="name">The name of the method.</param>
		/// <param name="parameters">An array that contains the method's parameters.</param>
		/// <returns>An object that represents the specified method, or <see langword="null" /> if the method is not found.</returns>
		// Token: 0x06001FA9 RID: 8105 RVA: 0x0007C6EC File Offset: 0x0007A8EC
		public static MethodInfo GetRuntimeMethod(this Type type, string name, Type[] parameters)
		{
			RuntimeReflectionExtensions.CheckAndThrow(type);
			return type.GetMethod(name, parameters);
		}

		/// <summary>Retrieves an object that represents a specified field.</summary>
		/// <param name="type">The type that contains the field.</param>
		/// <param name="name">The name of the field.</param>
		/// <returns>An object that represents the specified field, or <see langword="null" /> if the field is not found.</returns>
		// Token: 0x06001FAA RID: 8106 RVA: 0x0007C6FC File Offset: 0x0007A8FC
		public static FieldInfo GetRuntimeField(this Type type, string name)
		{
			RuntimeReflectionExtensions.CheckAndThrow(type);
			return type.GetField(name);
		}

		/// <summary>Retrieves an object that represents the specified method on the direct or indirect base class where the method was first declared.</summary>
		/// <param name="method">The method to retrieve information about.</param>
		/// <returns>An object that represents the specified method's initial declaration on a base class.</returns>
		// Token: 0x06001FAB RID: 8107 RVA: 0x0007C70B File Offset: 0x0007A90B
		public static MethodInfo GetRuntimeBaseDefinition(this MethodInfo method)
		{
			RuntimeReflectionExtensions.CheckAndThrow(method);
			return method.GetBaseDefinition();
		}

		/// <summary>Returns an interface mapping for the specified type and the specified interface.</summary>
		/// <param name="typeInfo">The type to retrieve a mapping for.</param>
		/// <param name="interfaceType">The interface to retrieve a mapping for.</param>
		/// <returns>An object that represents the interface mapping for the specified interface and type.</returns>
		// Token: 0x06001FAC RID: 8108 RVA: 0x0007C719 File Offset: 0x0007A919
		public static InterfaceMapping GetRuntimeInterfaceMap(this TypeInfo typeInfo, Type interfaceType)
		{
			if (typeInfo == null)
			{
				throw new ArgumentNullException("typeInfo");
			}
			if (!(typeInfo is RuntimeType))
			{
				throw new ArgumentException(Environment.GetResourceString("Type must be a runtime Type object."));
			}
			return typeInfo.GetInterfaceMap(interfaceType);
		}

		/// <summary>Gets an object that represents the method represented by the specified delegate.</summary>
		/// <param name="del">The delegate to examine.</param>
		/// <returns>An object that represents the method.</returns>
		// Token: 0x06001FAD RID: 8109 RVA: 0x0007C74E File Offset: 0x0007A94E
		public static MethodInfo GetMethodInfo(this Delegate del)
		{
			if (del == null)
			{
				throw new ArgumentNullException("del");
			}
			return del.Method;
		}

		// Token: 0x04001128 RID: 4392
		private const BindingFlags everything = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
	}
}
