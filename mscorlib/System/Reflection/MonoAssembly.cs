﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000313 RID: 787
	[ComDefaultInterface(typeof(_Assembly))]
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[Serializable]
	internal class MonoAssembly : RuntimeAssembly
	{
		// Token: 0x060022B2 RID: 8882 RVA: 0x000805B4 File Offset: 0x0007E7B4
		public override Type GetType(string name, bool throwOnError, bool ignoreCase)
		{
			if (name == null)
			{
				throw new ArgumentNullException(name);
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("name", "Name cannot be empty");
			}
			return base.InternalGetType(null, name, throwOnError, ignoreCase);
		}

		// Token: 0x060022B3 RID: 8883 RVA: 0x000805E4 File Offset: 0x0007E7E4
		public override Module GetModule(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Name can't be empty");
			}
			foreach (Module module in this.GetModules(true))
			{
				if (module.ScopeName == name)
				{
					return module;
				}
			}
			return null;
		}

		// Token: 0x060022B4 RID: 8884 RVA: 0x0008063D File Offset: 0x0007E83D
		public override AssemblyName[] GetReferencedAssemblies()
		{
			return Assembly.GetReferencedAssemblies(this);
		}

		// Token: 0x060022B5 RID: 8885 RVA: 0x00080648 File Offset: 0x0007E848
		public override Module[] GetModules(bool getResourceModules)
		{
			Module[] modulesInternal = this.GetModulesInternal();
			if (!getResourceModules)
			{
				List<Module> list = new List<Module>(modulesInternal.Length);
				foreach (Module module in modulesInternal)
				{
					if (!module.IsResource())
					{
						list.Add(module);
					}
				}
				return list.ToArray();
			}
			return modulesInternal;
		}

		// Token: 0x060022B6 RID: 8886 RVA: 0x00080696 File Offset: 0x0007E896
		[MonoTODO("Always returns the same as GetModules")]
		public override Module[] GetLoadedModules(bool getResourceModules)
		{
			return this.GetModules(getResourceModules);
		}

		// Token: 0x060022B7 RID: 8887 RVA: 0x0008069F File Offset: 0x0007E89F
		public override Assembly GetSatelliteAssembly(CultureInfo culture)
		{
			return base.GetSatelliteAssembly(culture, null, true);
		}

		// Token: 0x060022B8 RID: 8888 RVA: 0x000806AA File Offset: 0x0007E8AA
		public override Assembly GetSatelliteAssembly(CultureInfo culture, Version version)
		{
			return base.GetSatelliteAssembly(culture, version, true);
		}

		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x060022B9 RID: 8889 RVA: 0x000806B5 File Offset: 0x0007E8B5
		[ComVisible(false)]
		public override Module ManifestModule
		{
			get
			{
				return this.GetManifestModule();
			}
		}

		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x060022BA RID: 8890 RVA: 0x000806BD File Offset: 0x0007E8BD
		public override bool GlobalAssemblyCache
		{
			get
			{
				return base.get_global_assembly_cache();
			}
		}

		// Token: 0x060022BB RID: 8891 RVA: 0x000806C5 File Offset: 0x0007E8C5
		public MonoAssembly()
		{
		}
	}
}
