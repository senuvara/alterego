﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines the attributes that can be associated with a property. These attribute values are defined in corhdr.h.</summary>
	// Token: 0x020002F0 RID: 752
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum PropertyAttributes
	{
		/// <summary>Specifies that no attributes are associated with a property.</summary>
		// Token: 0x04001269 RID: 4713
		None = 0,
		/// <summary>Specifies that the property is special, with the name describing how the property is special.</summary>
		// Token: 0x0400126A RID: 4714
		SpecialName = 512,
		/// <summary>Specifies a flag reserved for runtime use only.</summary>
		// Token: 0x0400126B RID: 4715
		ReservedMask = 62464,
		/// <summary>Specifies that the metadata internal APIs check the name encoding.</summary>
		// Token: 0x0400126C RID: 4716
		RTSpecialName = 1024,
		/// <summary>Specifies that the property has a default value.</summary>
		// Token: 0x0400126D RID: 4717
		HasDefault = 4096,
		/// <summary>Reserved.</summary>
		// Token: 0x0400126E RID: 4718
		Reserved2 = 8192,
		/// <summary>Reserved.</summary>
		// Token: 0x0400126F RID: 4719
		Reserved3 = 16384,
		/// <summary>Reserved.</summary>
		// Token: 0x04001270 RID: 4720
		Reserved4 = 32768
	}
}
