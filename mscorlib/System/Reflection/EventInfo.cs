﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Mono;
using Unity;

namespace System.Reflection
{
	/// <summary>Discovers the attributes of an event and provides access to event metadata.</summary>
	// Token: 0x02000308 RID: 776
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_EventInfo))]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public abstract class EventInfo : MemberInfo, _EventInfo
	{
		/// <summary>Gets the attributes for this event.</summary>
		/// <returns>The read-only attributes for this event.</returns>
		// Token: 0x170004F7 RID: 1271
		// (get) Token: 0x060021F7 RID: 8695
		public abstract EventAttributes Attributes { get; }

		/// <summary>Gets the <see langword="Type" /> object of the underlying event-handler delegate associated with this event.</summary>
		/// <returns>A read-only <see langword="Type" /> object representing the delegate event handler.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x170004F8 RID: 1272
		// (get) Token: 0x060021F8 RID: 8696 RVA: 0x0007FD04 File Offset: 0x0007DF04
		public virtual Type EventHandlerType
		{
			get
			{
				ParameterInfo[] parametersInternal = this.GetAddMethod(true).GetParametersInternal();
				if (parametersInternal.Length != 0)
				{
					return parametersInternal[0].ParameterType;
				}
				return null;
			}
		}

		/// <summary>Gets a value indicating whether the event is multicast.</summary>
		/// <returns>
		///     <see langword="true" /> if the delegate is an instance of a multicast delegate; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		// Token: 0x170004F9 RID: 1273
		// (get) Token: 0x060021F9 RID: 8697 RVA: 0x00004E08 File Offset: 0x00003008
		public virtual bool IsMulticast
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets a value indicating whether the <see langword="EventInfo" /> has a name with a special meaning.</summary>
		/// <returns>
		///     <see langword="true" /> if this event has a special name; otherwise, <see langword="false" />.</returns>
		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x060021FA RID: 8698 RVA: 0x0007FD2C File Offset: 0x0007DF2C
		public bool IsSpecialName
		{
			get
			{
				return (this.Attributes & EventAttributes.SpecialName) > EventAttributes.None;
			}
		}

		/// <summary>Gets a <see cref="T:System.Reflection.MemberTypes" /> value indicating that this member is an event.</summary>
		/// <returns>A <see cref="T:System.Reflection.MemberTypes" /> value indicating that this member is an event.</returns>
		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x060021FB RID: 8699 RVA: 0x00021971 File Offset: 0x0001FB71
		public override MemberTypes MemberType
		{
			get
			{
				return MemberTypes.Event;
			}
		}

		/// <summary>Initializes a new instance of the <see langword="EventInfo" /> class.</summary>
		// Token: 0x060021FC RID: 8700 RVA: 0x00056575 File Offset: 0x00054775
		protected EventInfo()
		{
		}

		/// <summary>Adds an event handler to an event source.</summary>
		/// <param name="target">The event source. </param>
		/// <param name="handler">Encapsulates a method or methods to be invoked when the event is raised by the target. </param>
		/// <exception cref="T:System.InvalidOperationException">The event does not have a public <see langword="add" /> accessor.</exception>
		/// <exception cref="T:System.ArgumentException">The handler that was passed in cannot be used. </exception>
		/// <exception cref="T:System.MethodAccessException">
		///           In the .NET for Windows Store apps or the Portable Class Library, catch the base class exception, <see cref="T:System.MemberAccessException" />, instead.The caller does not have access permission to the member. </exception>
		/// <exception cref="T:System.Reflection.TargetException">
		///           In the .NET for Windows Store apps or the Portable Class Library, catch <see cref="T:System.Exception" /> instead.The <paramref name="target" /> parameter is <see langword="null" /> and the event is not static.-or- The <see cref="T:System.Reflection.EventInfo" /> is not declared on the target. </exception>
		// Token: 0x060021FD RID: 8701 RVA: 0x0007FD40 File Offset: 0x0007DF40
		[DebuggerStepThrough]
		[DebuggerHidden]
		public virtual void AddEventHandler(object target, Delegate handler)
		{
			MethodInfo addMethod = this.GetAddMethod();
			if (addMethod == null)
			{
				throw new InvalidOperationException("Cannot add a handler to an event that doesn't have a visible add method");
			}
			if (target == null && !addMethod.IsStatic)
			{
				throw new TargetException("Cannot add a handler to a non static event with a null target");
			}
			addMethod.Invoke(target, new object[]
			{
				handler
			});
		}

		/// <summary>Returns the method used to add an event handler delegate to the event source.</summary>
		/// <returns>A <see cref="T:System.Reflection.MethodInfo" /> object representing the method used to add an event handler delegate to the event source.</returns>
		// Token: 0x060021FE RID: 8702 RVA: 0x0007FD90 File Offset: 0x0007DF90
		public MethodInfo GetAddMethod()
		{
			return this.GetAddMethod(false);
		}

		/// <summary>When overridden in a derived class, retrieves the <see langword="MethodInfo" /> object for the <see cref="M:System.Reflection.EventInfo.AddEventHandler(System.Object,System.Delegate)" /> method of the event, specifying whether to return non-public methods.</summary>
		/// <param name="nonPublic">
		///       <see langword="true" /> if non-public methods can be returned; otherwise, <see langword="false" />. </param>
		/// <returns>A <see cref="T:System.Reflection.MethodInfo" /> object representing the method used to add an event handler delegate to the event source.</returns>
		/// <exception cref="T:System.MethodAccessException">
		///         <paramref name="nonPublic" /> is <see langword="true" />, the method used to add an event handler delegate is non-public, and the caller does not have permission to reflect on non-public methods. </exception>
		// Token: 0x060021FF RID: 8703
		public abstract MethodInfo GetAddMethod(bool nonPublic);

		/// <summary>Returns the method that is called when the event is raised.</summary>
		/// <returns>The method that is called when the event is raised.</returns>
		// Token: 0x06002200 RID: 8704 RVA: 0x0007FD99 File Offset: 0x0007DF99
		public MethodInfo GetRaiseMethod()
		{
			return this.GetRaiseMethod(false);
		}

		/// <summary>When overridden in a derived class, returns the method that is called when the event is raised, specifying whether to return non-public methods.</summary>
		/// <param name="nonPublic">
		///       <see langword="true" /> if non-public methods can be returned; otherwise, <see langword="false" />. </param>
		/// <returns>A <see langword="MethodInfo" /> object that was called when the event was raised.</returns>
		/// <exception cref="T:System.MethodAccessException">
		///         <paramref name="nonPublic" /> is <see langword="true" />, the method used to add an event handler delegate is non-public, and the caller does not have permission to reflect on non-public methods. </exception>
		// Token: 0x06002201 RID: 8705
		public abstract MethodInfo GetRaiseMethod(bool nonPublic);

		/// <summary>Returns the method used to remove an event handler delegate from the event source.</summary>
		/// <returns>A <see cref="T:System.Reflection.MethodInfo" /> object representing the method used to remove an event handler delegate from the event source.</returns>
		// Token: 0x06002202 RID: 8706 RVA: 0x0007FDA2 File Offset: 0x0007DFA2
		public MethodInfo GetRemoveMethod()
		{
			return this.GetRemoveMethod(false);
		}

		/// <summary>When overridden in a derived class, retrieves the <see langword="MethodInfo" /> object for removing a method of the event, specifying whether to return non-public methods.</summary>
		/// <param name="nonPublic">
		///       <see langword="true" /> if non-public methods can be returned; otherwise, <see langword="false" />. </param>
		/// <returns>A <see cref="T:System.Reflection.MethodInfo" /> object representing the method used to remove an event handler delegate from the event source.</returns>
		/// <exception cref="T:System.MethodAccessException">
		///         <paramref name="nonPublic" /> is <see langword="true" />, the method used to add an event handler delegate is non-public, and the caller does not have permission to reflect on non-public methods. </exception>
		// Token: 0x06002203 RID: 8707
		public abstract MethodInfo GetRemoveMethod(bool nonPublic);

		/// <summary>Returns the methods that have been associated with the event in metadata using the <see langword=".other" /> directive, specifying whether to include non-public methods.</summary>
		/// <param name="nonPublic">
		///       <see langword="true" /> to include non-public methods; otherwise, <see langword="false" />.</param>
		/// <returns>An array of <see cref="T:System.Reflection.EventInfo" /> objects representing methods that have been associated with an event in metadata by using the <see langword=".other" /> directive. If there are no methods matching the specification, an empty array is returned.</returns>
		/// <exception cref="T:System.NotImplementedException">This method is not implemented.</exception>
		// Token: 0x06002204 RID: 8708 RVA: 0x0007FDAB File Offset: 0x0007DFAB
		public virtual MethodInfo[] GetOtherMethods(bool nonPublic)
		{
			return EmptyArray<MethodInfo>.Value;
		}

		/// <summary>Returns the public methods that have been associated with an event in metadata using the <see langword=".other" /> directive.</summary>
		/// <returns>An array of <see cref="T:System.Reflection.EventInfo" /> objects representing the public methods that have been associated with the event in metadata by using the <see langword=".other" /> directive. If there are no such public methods, an empty array is returned.</returns>
		// Token: 0x06002205 RID: 8709 RVA: 0x0007FDB2 File Offset: 0x0007DFB2
		public MethodInfo[] GetOtherMethods()
		{
			return this.GetOtherMethods(false);
		}

		/// <summary>Removes an event handler from an event source.</summary>
		/// <param name="target">The event source. </param>
		/// <param name="handler">The delegate to be disassociated from the events raised by target. </param>
		/// <exception cref="T:System.InvalidOperationException">The event does not have a public <see langword="remove" /> accessor. </exception>
		/// <exception cref="T:System.ArgumentException">The handler that was passed in cannot be used. </exception>
		/// <exception cref="T:System.Reflection.TargetException">
		///           In the .NET for Windows Store apps or the Portable Class Library, catch <see cref="T:System.Exception" /> instead.The <paramref name="target" /> parameter is <see langword="null" /> and the event is not static.-or- The <see cref="T:System.Reflection.EventInfo" /> is not declared on the target. </exception>
		/// <exception cref="T:System.MethodAccessException">
		///           In the .NET for Windows Store apps or the Portable Class Library, catch the base class exception, <see cref="T:System.MemberAccessException" />, instead.The caller does not have access permission to the member. </exception>
		// Token: 0x06002206 RID: 8710 RVA: 0x0007FDBB File Offset: 0x0007DFBB
		[DebuggerHidden]
		[DebuggerStepThrough]
		public virtual void RemoveEventHandler(object target, Delegate handler)
		{
			MethodInfo removeMethod = this.GetRemoveMethod();
			if (removeMethod == null)
			{
				throw new InvalidOperationException("Cannot remove a handler to an event that doesn't have a visible remove method");
			}
			removeMethod.Invoke(target, new object[]
			{
				handler
			});
		}

		/// <summary>Returns a value that indicates whether this instance is equal to a specified object.</summary>
		/// <param name="obj">An object to compare with this instance, or <see langword="null" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> equals the type and value of this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002207 RID: 8711 RVA: 0x0004B4DC File Offset: 0x000496DC
		public override bool Equals(object obj)
		{
			return obj == this;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06002208 RID: 8712 RVA: 0x0007D2D9 File Offset: 0x0007B4D9
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>Indicates whether two <see cref="T:System.Reflection.EventInfo" /> objects are equal.</summary>
		/// <param name="left">The first object to compare.</param>
		/// <param name="right">The second object to compare.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="left" /> is equal to <paramref name="right" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06002209 RID: 8713 RVA: 0x0007E902 File Offset: 0x0007CB02
		public static bool operator ==(EventInfo left, EventInfo right)
		{
			return left == right || (!(left == null ^ right == null) && left.Equals(right));
		}

		/// <summary>Indicates whether two <see cref="T:System.Reflection.EventInfo" /> objects are not equal.</summary>
		/// <param name="left">The first object to compare.</param>
		/// <param name="right">The second object to compare.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="left" /> is not equal to <paramref name="right" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600220A RID: 8714 RVA: 0x0007E91E File Offset: 0x0007CB1E
		public static bool operator !=(EventInfo left, EventInfo right)
		{
			return left != right && ((left == null ^ right == null) || !left.Equals(right));
		}

		/// <summary>Gets the <see cref="T:System.Reflection.MethodInfo" /> object for the <see cref="M:System.Reflection.EventInfo.AddEventHandler(System.Object,System.Delegate)" /> method of the event, including non-public methods.</summary>
		/// <returns>The <see cref="T:System.Reflection.MethodInfo" /> object for the <see cref="M:System.Reflection.EventInfo.AddEventHandler(System.Object,System.Delegate)" /> method.</returns>
		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x0600220B RID: 8715 RVA: 0x0007FDE8 File Offset: 0x0007DFE8
		public virtual MethodInfo AddMethod
		{
			get
			{
				return this.GetAddMethod(true);
			}
		}

		/// <summary>Gets the method that is called when the event is raised, including non-public methods.</summary>
		/// <returns>The method that is called when the event is raised.</returns>
		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x0600220C RID: 8716 RVA: 0x0007FDF1 File Offset: 0x0007DFF1
		public virtual MethodInfo RaiseMethod
		{
			get
			{
				return this.GetRaiseMethod(true);
			}
		}

		/// <summary>Gets the <see langword="MethodInfo" /> object for removing a method of the event, including non-public methods.</summary>
		/// <returns>The <see langword="MethodInfo" /> object for removing a method of the event.</returns>
		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x0600220D RID: 8717 RVA: 0x0007FDFA File Offset: 0x0007DFFA
		public virtual MethodInfo RemoveMethod
		{
			get
			{
				return this.GetRemoveMethod(true);
			}
		}

		// Token: 0x0600220E RID: 8718
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern EventInfo internal_from_handle_type(IntPtr event_handle, IntPtr type_handle);

		// Token: 0x0600220F RID: 8719 RVA: 0x0007FE03 File Offset: 0x0007E003
		internal static EventInfo GetEventFromHandle(RuntimeEventHandle handle)
		{
			if (handle.Value == IntPtr.Zero)
			{
				throw new ArgumentException("The handle is invalid.");
			}
			return EventInfo.internal_from_handle_type(handle.Value, IntPtr.Zero);
		}

		// Token: 0x06002210 RID: 8720 RVA: 0x0007FE34 File Offset: 0x0007E034
		internal static EventInfo GetEventFromHandle(RuntimeEventHandle handle, RuntimeTypeHandle reflectedType)
		{
			if (handle.Value == IntPtr.Zero)
			{
				throw new ArgumentException("The handle is invalid.");
			}
			EventInfo eventInfo = EventInfo.internal_from_handle_type(handle.Value, reflectedType.Value);
			if (eventInfo == null)
			{
				throw new ArgumentException("The event handle and the type handle are incompatible.");
			}
			return eventInfo;
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x06002211 RID: 8721 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EventInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Returns a T:System.Type object representing the <see cref="T:System.Reflection.EventInfo" /> type.</summary>
		/// <returns>A T:System.Type object representing the <see cref="T:System.Reflection.EventInfo" /> type.</returns>
		// Token: 0x06002212 RID: 8722 RVA: 0x0005AB11 File Offset: 0x00058D11
		Type _EventInfo.GetType()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x06002213 RID: 8723 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EventInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x06002214 RID: 8724 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EventInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x06002215 RID: 8725 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _EventInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040012D5 RID: 4821
		private EventInfo.AddEventAdapter cached_add_event;

		// Token: 0x02000309 RID: 777
		// (Invoke) Token: 0x06002217 RID: 8727
		private delegate void AddEventAdapter(object _this, Delegate dele);
	}
}
