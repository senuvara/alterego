﻿using System;

namespace System.Reflection
{
	// Token: 0x02000321 RID: 801
	internal abstract class RuntimeParameterInfo : ParameterInfo
	{
		// Token: 0x06002378 RID: 9080 RVA: 0x0008186A File Offset: 0x0007FA6A
		protected RuntimeParameterInfo()
		{
		}
	}
}
