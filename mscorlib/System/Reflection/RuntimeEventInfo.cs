﻿using System;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x02000315 RID: 789
	internal abstract class RuntimeEventInfo : EventInfo, ISerializable
	{
		// Token: 0x1700052D RID: 1325
		// (get) Token: 0x060022BE RID: 8894 RVA: 0x00002526 File Offset: 0x00000726
		internal BindingFlags BindingFlags
		{
			get
			{
				return BindingFlags.Default;
			}
		}

		// Token: 0x1700052E RID: 1326
		// (get) Token: 0x060022BF RID: 8895 RVA: 0x000806E6 File Offset: 0x0007E8E6
		public override Module Module
		{
			get
			{
				return this.GetRuntimeModule();
			}
		}

		// Token: 0x060022C0 RID: 8896 RVA: 0x000806EE File Offset: 0x0007E8EE
		internal RuntimeType GetDeclaringTypeInternal()
		{
			return (RuntimeType)this.DeclaringType;
		}

		// Token: 0x1700052F RID: 1327
		// (get) Token: 0x060022C1 RID: 8897 RVA: 0x000806FB File Offset: 0x0007E8FB
		private RuntimeType ReflectedTypeInternal
		{
			get
			{
				return (RuntimeType)this.ReflectedType;
			}
		}

		// Token: 0x060022C2 RID: 8898 RVA: 0x00080708 File Offset: 0x0007E908
		internal RuntimeModule GetRuntimeModule()
		{
			return this.GetDeclaringTypeInternal().GetRuntimeModule();
		}

		// Token: 0x060022C3 RID: 8899 RVA: 0x00080715 File Offset: 0x0007E915
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			MemberInfoSerializationHolder.GetSerializationInfo(info, this.Name, this.ReflectedTypeInternal, null, MemberTypes.Event);
		}

		// Token: 0x060022C4 RID: 8900 RVA: 0x00080739 File Offset: 0x0007E939
		protected RuntimeEventInfo()
		{
		}
	}
}
