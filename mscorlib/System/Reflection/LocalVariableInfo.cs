﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Discovers the attributes of a local variable and provides access to local variable metadata.</summary>
	// Token: 0x0200030D RID: 781
	[ComVisible(true)]
	[StructLayout(LayoutKind.Sequential)]
	public class LocalVariableInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.LocalVariableInfo" /> class.</summary>
		// Token: 0x0600224F RID: 8783 RVA: 0x00002050 File Offset: 0x00000250
		protected LocalVariableInfo()
		{
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value that indicates whether the object referred to by the local variable is pinned in memory.</summary>
		/// <returns>
		///     <see langword="true" /> if the object referred to by the variable is pinned in memory; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x06002250 RID: 8784 RVA: 0x0008019F File Offset: 0x0007E39F
		public virtual bool IsPinned
		{
			get
			{
				return this.is_pinned;
			}
		}

		/// <summary>Gets the index of the local variable within the method body.</summary>
		/// <returns>An integer value that represents the order of declaration of the local variable within the method body.</returns>
		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x06002251 RID: 8785 RVA: 0x000801A7 File Offset: 0x0007E3A7
		public virtual int LocalIndex
		{
			get
			{
				return (int)this.position;
			}
		}

		/// <summary>Gets the type of the local variable.</summary>
		/// <returns>The type of the local variable.</returns>
		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x06002252 RID: 8786 RVA: 0x000801AF File Offset: 0x0007E3AF
		public virtual Type LocalType
		{
			get
			{
				return this.type;
			}
		}

		/// <summary>Returns a user-readable string that describes the local variable.</summary>
		/// <returns>A string that displays information about the local variable, including the type name, index, and pinned status.</returns>
		// Token: 0x06002253 RID: 8787 RVA: 0x000801B8 File Offset: 0x0007E3B8
		public override string ToString()
		{
			if (this.is_pinned)
			{
				return string.Format("{0} ({1}) (pinned)", this.type, this.position);
			}
			return string.Format("{0} ({1})", this.type, this.position);
		}

		// Token: 0x040012E2 RID: 4834
		internal Type type;

		// Token: 0x040012E3 RID: 4835
		internal bool is_pinned;

		// Token: 0x040012E4 RID: 4836
		internal ushort position;
	}
}
