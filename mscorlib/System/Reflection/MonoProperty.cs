﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Reflection
{
	// Token: 0x02000328 RID: 808
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	internal class MonoProperty : RuntimePropertyInfo
	{
		// Token: 0x0600239B RID: 9115 RVA: 0x00081BD8 File Offset: 0x0007FDD8
		private void CachePropertyInfo(PInfo flags)
		{
			if ((this.cached & flags) != flags)
			{
				MonoPropertyInfo.get_property_info(this, ref this.info, flags);
				this.cached |= flags;
			}
		}

		// Token: 0x1700056A RID: 1386
		// (get) Token: 0x0600239C RID: 9116 RVA: 0x00081C00 File Offset: 0x0007FE00
		public override PropertyAttributes Attributes
		{
			get
			{
				this.CachePropertyInfo(PInfo.Attributes);
				return this.info.attrs;
			}
		}

		// Token: 0x1700056B RID: 1387
		// (get) Token: 0x0600239D RID: 9117 RVA: 0x00081C14 File Offset: 0x0007FE14
		public override bool CanRead
		{
			get
			{
				this.CachePropertyInfo(PInfo.GetMethod);
				return this.info.get_method != null;
			}
		}

		// Token: 0x1700056C RID: 1388
		// (get) Token: 0x0600239E RID: 9118 RVA: 0x00081C2E File Offset: 0x0007FE2E
		public override bool CanWrite
		{
			get
			{
				this.CachePropertyInfo(PInfo.SetMethod);
				return this.info.set_method != null;
			}
		}

		// Token: 0x1700056D RID: 1389
		// (get) Token: 0x0600239F RID: 9119 RVA: 0x00081C48 File Offset: 0x0007FE48
		public override Type PropertyType
		{
			get
			{
				this.CachePropertyInfo(PInfo.GetMethod | PInfo.SetMethod);
				if (this.info.get_method != null)
				{
					return this.info.get_method.ReturnType;
				}
				ParameterInfo[] parametersInternal = this.info.set_method.GetParametersInternal();
				return parametersInternal[parametersInternal.Length - 1].ParameterType;
			}
		}

		// Token: 0x1700056E RID: 1390
		// (get) Token: 0x060023A0 RID: 9120 RVA: 0x00081C9B File Offset: 0x0007FE9B
		public override Type ReflectedType
		{
			get
			{
				this.CachePropertyInfo(PInfo.ReflectedType);
				return this.info.parent;
			}
		}

		// Token: 0x1700056F RID: 1391
		// (get) Token: 0x060023A1 RID: 9121 RVA: 0x00081CAF File Offset: 0x0007FEAF
		public override Type DeclaringType
		{
			get
			{
				this.CachePropertyInfo(PInfo.DeclaringType);
				return this.info.declaring_type;
			}
		}

		// Token: 0x17000570 RID: 1392
		// (get) Token: 0x060023A2 RID: 9122 RVA: 0x00081CC4 File Offset: 0x0007FEC4
		public override string Name
		{
			get
			{
				this.CachePropertyInfo(PInfo.Name);
				return this.info.name;
			}
		}

		// Token: 0x060023A3 RID: 9123 RVA: 0x00081CDC File Offset: 0x0007FEDC
		public override MethodInfo[] GetAccessors(bool nonPublic)
		{
			int num = 0;
			int num2 = 0;
			this.CachePropertyInfo(PInfo.GetMethod | PInfo.SetMethod);
			if (this.info.set_method != null && (nonPublic || this.info.set_method.IsPublic))
			{
				num2 = 1;
			}
			if (this.info.get_method != null && (nonPublic || this.info.get_method.IsPublic))
			{
				num = 1;
			}
			MethodInfo[] array = new MethodInfo[num + num2];
			int num3 = 0;
			if (num2 != 0)
			{
				array[num3++] = this.info.set_method;
			}
			if (num != 0)
			{
				array[num3++] = this.info.get_method;
			}
			return array;
		}

		// Token: 0x060023A4 RID: 9124 RVA: 0x00081D7E File Offset: 0x0007FF7E
		public override MethodInfo GetGetMethod(bool nonPublic)
		{
			this.CachePropertyInfo(PInfo.GetMethod);
			if (this.info.get_method != null && (nonPublic || this.info.get_method.IsPublic))
			{
				return this.info.get_method;
			}
			return null;
		}

		// Token: 0x060023A5 RID: 9125 RVA: 0x00081DBC File Offset: 0x0007FFBC
		public override ParameterInfo[] GetIndexParameters()
		{
			this.CachePropertyInfo(PInfo.GetMethod | PInfo.SetMethod);
			ParameterInfo[] parametersInternal;
			int num;
			if (this.info.get_method != null)
			{
				parametersInternal = this.info.get_method.GetParametersInternal();
				num = parametersInternal.Length;
			}
			else
			{
				if (!(this.info.set_method != null))
				{
					return EmptyArray<ParameterInfo>.Value;
				}
				parametersInternal = this.info.set_method.GetParametersInternal();
				num = parametersInternal.Length - 1;
			}
			ParameterInfo[] array = new ParameterInfo[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = ParameterInfo.New(parametersInternal[i], this);
			}
			return array;
		}

		// Token: 0x060023A6 RID: 9126 RVA: 0x00081E4C File Offset: 0x0008004C
		public override MethodInfo GetSetMethod(bool nonPublic)
		{
			this.CachePropertyInfo(PInfo.SetMethod);
			if (this.info.set_method != null && (nonPublic || this.info.set_method.IsPublic))
			{
				return this.info.set_method;
			}
			return null;
		}

		// Token: 0x060023A7 RID: 9127 RVA: 0x00081E8A File Offset: 0x0008008A
		public override object GetConstantValue()
		{
			return MonoPropertyInfo.get_default_value(this);
		}

		// Token: 0x060023A8 RID: 9128 RVA: 0x00081E8A File Offset: 0x0008008A
		public override object GetRawConstantValue()
		{
			return MonoPropertyInfo.get_default_value(this);
		}

		// Token: 0x060023A9 RID: 9129 RVA: 0x00081E92 File Offset: 0x00080092
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, false);
		}

		// Token: 0x060023AA RID: 9130 RVA: 0x00081E9C File Offset: 0x0008009C
		public override object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, false);
		}

		// Token: 0x060023AB RID: 9131 RVA: 0x00081EA5 File Offset: 0x000800A5
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, false);
		}

		// Token: 0x060023AC RID: 9132 RVA: 0x00081EAF File Offset: 0x000800AF
		private static object GetterAdapterFrame<T, R>(MonoProperty.Getter<T, R> getter, object obj)
		{
			return getter((T)((object)obj));
		}

		// Token: 0x060023AD RID: 9133 RVA: 0x00081EC2 File Offset: 0x000800C2
		private static object StaticGetterAdapterFrame<R>(MonoProperty.StaticGetter<R> getter, object obj)
		{
			return getter();
		}

		// Token: 0x060023AE RID: 9134 RVA: 0x00081ED0 File Offset: 0x000800D0
		private static MonoProperty.GetterAdapter CreateGetterDelegate(MethodInfo method)
		{
			Type[] typeArguments;
			Type typeFromHandle;
			string name;
			if (method.IsStatic)
			{
				typeArguments = new Type[]
				{
					method.ReturnType
				};
				typeFromHandle = typeof(MonoProperty.StaticGetter<>);
				name = "StaticGetterAdapterFrame";
			}
			else
			{
				typeArguments = new Type[]
				{
					method.DeclaringType,
					method.ReturnType
				};
				typeFromHandle = typeof(MonoProperty.Getter<, >);
				name = "GetterAdapterFrame";
			}
			object obj = Delegate.CreateDelegate(typeFromHandle.MakeGenericType(typeArguments), method, false);
			if (obj == null)
			{
				throw new MethodAccessException();
			}
			MethodInfo methodInfo = typeof(MonoProperty).GetMethod(name, BindingFlags.Static | BindingFlags.NonPublic);
			methodInfo = methodInfo.MakeGenericMethod(typeArguments);
			return (MonoProperty.GetterAdapter)Delegate.CreateDelegate(typeof(MonoProperty.GetterAdapter), obj, methodInfo, true);
		}

		// Token: 0x060023AF RID: 9135 RVA: 0x00081F7F File Offset: 0x0008017F
		public override object GetValue(object obj, object[] index)
		{
			if (index != null)
			{
				int num = index.Length;
			}
			return this.GetValue(obj, BindingFlags.Default, null, index, null);
		}

		// Token: 0x060023B0 RID: 9136 RVA: 0x00081F94 File Offset: 0x00080194
		public override object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
		{
			object result = null;
			MethodInfo getMethod = this.GetGetMethod(true);
			if (getMethod == null)
			{
				throw new ArgumentException("Get Method not found for '" + this.Name + "'");
			}
			try
			{
				if (index == null || index.Length == 0)
				{
					result = getMethod.Invoke(obj, invokeAttr, binder, null, culture);
				}
				else
				{
					result = getMethod.Invoke(obj, invokeAttr, binder, index, culture);
				}
			}
			catch (SecurityException inner)
			{
				throw new TargetInvocationException(inner);
			}
			return result;
		}

		// Token: 0x060023B1 RID: 9137 RVA: 0x00082010 File Offset: 0x00080210
		public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
		{
			MethodInfo setMethod = this.GetSetMethod(true);
			if (setMethod == null)
			{
				throw new ArgumentException("Set Method not found for '" + this.Name + "'");
			}
			object[] array;
			if (index == null || index.Length == 0)
			{
				array = new object[]
				{
					value
				};
			}
			else
			{
				int num = index.Length;
				array = new object[num + 1];
				index.CopyTo(array, 0);
				array[num] = value;
			}
			setMethod.Invoke(obj, invokeAttr, binder, array, culture);
		}

		// Token: 0x060023B2 RID: 9138 RVA: 0x00082088 File Offset: 0x00080288
		public override Type[] GetOptionalCustomModifiers()
		{
			Type[] typeModifiers = MonoPropertyInfo.GetTypeModifiers(this, true);
			if (typeModifiers == null)
			{
				return Type.EmptyTypes;
			}
			return typeModifiers;
		}

		// Token: 0x060023B3 RID: 9139 RVA: 0x000820A8 File Offset: 0x000802A8
		public override Type[] GetRequiredCustomModifiers()
		{
			Type[] typeModifiers = MonoPropertyInfo.GetTypeModifiers(this, false);
			if (typeModifiers == null)
			{
				return Type.EmptyTypes;
			}
			return typeModifiers;
		}

		// Token: 0x060023B4 RID: 9140 RVA: 0x000808D8 File Offset: 0x0007EAD8
		public override IList<CustomAttributeData> GetCustomAttributesData()
		{
			return CustomAttributeData.GetCustomAttributes(this);
		}

		// Token: 0x060023B5 RID: 9141 RVA: 0x000820C7 File Offset: 0x000802C7
		public MonoProperty()
		{
		}

		// Token: 0x04001320 RID: 4896
		internal IntPtr klass;

		// Token: 0x04001321 RID: 4897
		internal IntPtr prop;

		// Token: 0x04001322 RID: 4898
		private MonoPropertyInfo info;

		// Token: 0x04001323 RID: 4899
		private PInfo cached;

		// Token: 0x04001324 RID: 4900
		private MonoProperty.GetterAdapter cached_getter;

		// Token: 0x02000329 RID: 809
		// (Invoke) Token: 0x060023B7 RID: 9143
		private delegate object GetterAdapter(object _this);

		// Token: 0x0200032A RID: 810
		// (Invoke) Token: 0x060023BB RID: 9147
		private delegate R Getter<T, R>(T _this);

		// Token: 0x0200032B RID: 811
		// (Invoke) Token: 0x060023BF RID: 9151
		private delegate R StaticGetter<R>();
	}
}
