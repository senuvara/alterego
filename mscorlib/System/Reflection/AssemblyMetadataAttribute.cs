﻿using System;

namespace System.Reflection
{
	/// <summary>Defines a key/value metadata pair for the decorated assembly.</summary>
	// Token: 0x020002C5 RID: 709
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
	public sealed class AssemblyMetadataAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyMetadataAttribute" /> class by using the specified metadata key and value.</summary>
		/// <param name="key">The metadata key.</param>
		/// <param name="value">The metadata value.</param>
		// Token: 0x06001FD6 RID: 8150 RVA: 0x0007C928 File Offset: 0x0007AB28
		public AssemblyMetadataAttribute(string key, string value)
		{
			this.m_key = key;
			this.m_value = value;
		}

		/// <summary>Gets the metadata key.</summary>
		/// <returns>The metadata key.</returns>
		// Token: 0x1700046A RID: 1130
		// (get) Token: 0x06001FD7 RID: 8151 RVA: 0x0007C93E File Offset: 0x0007AB3E
		public string Key
		{
			get
			{
				return this.m_key;
			}
		}

		/// <summary>Gets the metadata value.</summary>
		/// <returns>The metadata value.</returns>
		// Token: 0x1700046B RID: 1131
		// (get) Token: 0x06001FD8 RID: 8152 RVA: 0x0007C946 File Offset: 0x0007AB46
		public string Value
		{
			get
			{
				return this.m_value;
			}
		}

		// Token: 0x04001139 RID: 4409
		private string m_key;

		// Token: 0x0400113A RID: 4410
		private string m_value;
	}
}
