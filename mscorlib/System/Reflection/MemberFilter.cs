﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Represents a delegate that is used to filter a list of members represented in an array of <see cref="T:System.Reflection.MemberInfo" /> objects.</summary>
	/// <param name="m">The <see cref="T:System.Reflection.MemberInfo" /> object to which the filter is applied. </param>
	/// <param name="filterCriteria">An arbitrary object used to filter the list. </param>
	/// <returns>
	///     <see langword="true" /> to include the member in the filtered list; otherwise <see langword="false" />.</returns>
	// Token: 0x020002E0 RID: 736
	// (Invoke) Token: 0x06002003 RID: 8195
	[ComVisible(true)]
	[Serializable]
	public delegate bool MemberFilter(MemberInfo m, object filterCriteria);
}
