﻿using System;

namespace System.Reflection
{
	// Token: 0x020002DB RID: 731
	[Serializable]
	internal enum CorElementType : byte
	{
		// Token: 0x04001198 RID: 4504
		End,
		// Token: 0x04001199 RID: 4505
		Void,
		// Token: 0x0400119A RID: 4506
		Boolean,
		// Token: 0x0400119B RID: 4507
		Char,
		// Token: 0x0400119C RID: 4508
		I1,
		// Token: 0x0400119D RID: 4509
		U1,
		// Token: 0x0400119E RID: 4510
		I2,
		// Token: 0x0400119F RID: 4511
		U2,
		// Token: 0x040011A0 RID: 4512
		I4,
		// Token: 0x040011A1 RID: 4513
		U4,
		// Token: 0x040011A2 RID: 4514
		I8,
		// Token: 0x040011A3 RID: 4515
		U8,
		// Token: 0x040011A4 RID: 4516
		R4,
		// Token: 0x040011A5 RID: 4517
		R8,
		// Token: 0x040011A6 RID: 4518
		String,
		// Token: 0x040011A7 RID: 4519
		Ptr,
		// Token: 0x040011A8 RID: 4520
		ByRef,
		// Token: 0x040011A9 RID: 4521
		ValueType,
		// Token: 0x040011AA RID: 4522
		Class,
		// Token: 0x040011AB RID: 4523
		Var,
		// Token: 0x040011AC RID: 4524
		Array,
		// Token: 0x040011AD RID: 4525
		GenericInst,
		// Token: 0x040011AE RID: 4526
		TypedByRef,
		// Token: 0x040011AF RID: 4527
		I = 24,
		// Token: 0x040011B0 RID: 4528
		U,
		// Token: 0x040011B1 RID: 4529
		FnPtr = 27,
		// Token: 0x040011B2 RID: 4530
		Object,
		// Token: 0x040011B3 RID: 4531
		SzArray,
		// Token: 0x040011B4 RID: 4532
		MVar,
		// Token: 0x040011B5 RID: 4533
		CModReqd,
		// Token: 0x040011B6 RID: 4534
		CModOpt,
		// Token: 0x040011B7 RID: 4535
		Internal,
		// Token: 0x040011B8 RID: 4536
		Max,
		// Token: 0x040011B9 RID: 4537
		Modifier = 64,
		// Token: 0x040011BA RID: 4538
		Sentinel,
		// Token: 0x040011BB RID: 4539
		Pinned = 69
	}
}
