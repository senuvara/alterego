﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies the version of the assembly being attributed.</summary>
	// Token: 0x020002C0 RID: 704
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyVersionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see langword="AssemblyVersionAttribute" /> class with the version number of the assembly being attributed.</summary>
		/// <param name="version">The version number of the attributed assembly. </param>
		// Token: 0x06001FC8 RID: 8136 RVA: 0x0007C8B5 File Offset: 0x0007AAB5
		public AssemblyVersionAttribute(string version)
		{
			this.m_version = version;
		}

		/// <summary>Gets the version number of the attributed assembly.</summary>
		/// <returns>A string containing the assembly version number.</returns>
		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x06001FC9 RID: 8137 RVA: 0x0007C8C4 File Offset: 0x0007AAC4
		public string Version
		{
			get
			{
				return this.m_version;
			}
		}

		// Token: 0x04001134 RID: 4404
		private string m_version;
	}
}
