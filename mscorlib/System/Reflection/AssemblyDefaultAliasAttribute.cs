﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines a friendly default alias for an assembly manifest.</summary>
	// Token: 0x020002BC RID: 700
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyDefaultAliasAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyDefaultAliasAttribute" /> class.</summary>
		/// <param name="defaultAlias">The assembly default alias information. </param>
		// Token: 0x06001FC0 RID: 8128 RVA: 0x0007C84B File Offset: 0x0007AA4B
		public AssemblyDefaultAliasAttribute(string defaultAlias)
		{
			this.m_defaultAlias = defaultAlias;
		}

		/// <summary>Gets default alias information.</summary>
		/// <returns>A string containing the default alias information.</returns>
		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x06001FC1 RID: 8129 RVA: 0x0007C85A File Offset: 0x0007AA5A
		public string DefaultAlias
		{
			get
			{
				return this.m_defaultAlias;
			}
		}

		// Token: 0x04001130 RID: 4400
		private string m_defaultAlias;
	}
}
