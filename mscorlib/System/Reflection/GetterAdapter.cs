﻿using System;

namespace System.Reflection
{
	// Token: 0x02000325 RID: 805
	// (Invoke) Token: 0x0600238A RID: 9098
	internal delegate object GetterAdapter(object _this);
}
