﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines the attributes that can be associated with a parameter. These are defined in CorHdr.h.</summary>
	// Token: 0x020002ED RID: 749
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum ParameterAttributes
	{
		/// <summary>Specifies that there is no parameter attribute.</summary>
		// Token: 0x0400125A RID: 4698
		None = 0,
		/// <summary>Specifies that the parameter is an input parameter.</summary>
		// Token: 0x0400125B RID: 4699
		In = 1,
		/// <summary>Specifies that the parameter is an output parameter.</summary>
		// Token: 0x0400125C RID: 4700
		Out = 2,
		/// <summary>Specifies that the parameter is a locale identifier (lcid).</summary>
		// Token: 0x0400125D RID: 4701
		Lcid = 4,
		/// <summary>Specifies that the parameter is a return value.</summary>
		// Token: 0x0400125E RID: 4702
		Retval = 8,
		/// <summary>Specifies that the parameter is optional.</summary>
		// Token: 0x0400125F RID: 4703
		Optional = 16,
		/// <summary>Specifies that the parameter is reserved.</summary>
		// Token: 0x04001260 RID: 4704
		ReservedMask = 61440,
		/// <summary>Specifies that the parameter has a default value.</summary>
		// Token: 0x04001261 RID: 4705
		HasDefault = 4096,
		/// <summary>Specifies that the parameter has field marshaling information.</summary>
		// Token: 0x04001262 RID: 4706
		HasFieldMarshal = 8192,
		/// <summary>Reserved.</summary>
		// Token: 0x04001263 RID: 4707
		Reserved3 = 16384,
		/// <summary>Reserved.</summary>
		// Token: 0x04001264 RID: 4708
		Reserved4 = 32768
	}
}
