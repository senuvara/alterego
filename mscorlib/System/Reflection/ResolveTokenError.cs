﻿using System;

namespace System.Reflection
{
	// Token: 0x0200030F RID: 783
	internal enum ResolveTokenError
	{
		// Token: 0x040012EC RID: 4844
		OutOfRange,
		// Token: 0x040012ED RID: 4845
		BadTable,
		// Token: 0x040012EE RID: 4846
		Other
	}
}
