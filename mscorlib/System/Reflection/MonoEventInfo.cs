﻿using System;
using System.Runtime.CompilerServices;

namespace System.Reflection
{
	// Token: 0x02000314 RID: 788
	internal struct MonoEventInfo
	{
		// Token: 0x060022BC RID: 8892
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_event_info(MonoEvent ev, out MonoEventInfo info);

		// Token: 0x060022BD RID: 8893 RVA: 0x000806D0 File Offset: 0x0007E8D0
		internal static MonoEventInfo GetEventInfo(MonoEvent ev)
		{
			MonoEventInfo result;
			MonoEventInfo.get_event_info(ev, out result);
			return result;
		}

		// Token: 0x040012F9 RID: 4857
		public Type declaring_type;

		// Token: 0x040012FA RID: 4858
		public Type reflected_type;

		// Token: 0x040012FB RID: 4859
		public string name;

		// Token: 0x040012FC RID: 4860
		public MethodInfo add_method;

		// Token: 0x040012FD RID: 4861
		public MethodInfo remove_method;

		// Token: 0x040012FE RID: 4862
		public MethodInfo raise_method;

		// Token: 0x040012FF RID: 4863
		public EventAttributes attrs;

		// Token: 0x04001300 RID: 4864
		public MethodInfo[] other_methods;
	}
}
