﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines the member of a type that is the default member used by <see cref="M:System.Type.InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])" />. </summary>
	// Token: 0x020002CF RID: 719
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface)]
	[ComVisible(true)]
	[Serializable]
	public sealed class DefaultMemberAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.DefaultMemberAttribute" /> class.</summary>
		/// <param name="memberName">A <see langword="String" /> containing the name of the member to invoke. This may be a constructor, method, property, or field. A suitable invocation attribute must be specified when the member is invoked. The default member of a class can be specified by passing an empty <see langword="String" /> as the name of the member.The default member of a type is marked with the <see langword="DefaultMemberAttribute" /> custom attribute or marked in COM in the usual way. </param>
		// Token: 0x06001FE7 RID: 8167 RVA: 0x0007C993 File Offset: 0x0007AB93
		public DefaultMemberAttribute(string memberName)
		{
			this.m_memberName = memberName;
		}

		/// <summary>Gets the name from the attribute.</summary>
		/// <returns>A string representing the member name.</returns>
		// Token: 0x1700046F RID: 1135
		// (get) Token: 0x06001FE8 RID: 8168 RVA: 0x0007C9A2 File Offset: 0x0007ABA2
		public string MemberName
		{
			get
			{
				return this.m_memberName;
			}
		}

		// Token: 0x04001169 RID: 4457
		private string m_memberName;
	}
}
