﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000316 RID: 790
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	internal sealed class MonoEvent : RuntimeEventInfo
	{
		// Token: 0x17000530 RID: 1328
		// (get) Token: 0x060022C5 RID: 8901 RVA: 0x00080741 File Offset: 0x0007E941
		public override EventAttributes Attributes
		{
			get
			{
				return MonoEventInfo.GetEventInfo(this).attrs;
			}
		}

		// Token: 0x060022C6 RID: 8902 RVA: 0x00080750 File Offset: 0x0007E950
		public override MethodInfo GetAddMethod(bool nonPublic)
		{
			MonoEventInfo eventInfo = MonoEventInfo.GetEventInfo(this);
			if (nonPublic || (eventInfo.add_method != null && eventInfo.add_method.IsPublic))
			{
				return eventInfo.add_method;
			}
			return null;
		}

		// Token: 0x060022C7 RID: 8903 RVA: 0x0008078C File Offset: 0x0007E98C
		public override MethodInfo GetRaiseMethod(bool nonPublic)
		{
			MonoEventInfo eventInfo = MonoEventInfo.GetEventInfo(this);
			if (nonPublic || (eventInfo.raise_method != null && eventInfo.raise_method.IsPublic))
			{
				return eventInfo.raise_method;
			}
			return null;
		}

		// Token: 0x060022C8 RID: 8904 RVA: 0x000807C8 File Offset: 0x0007E9C8
		public override MethodInfo GetRemoveMethod(bool nonPublic)
		{
			MonoEventInfo eventInfo = MonoEventInfo.GetEventInfo(this);
			if (nonPublic || (eventInfo.remove_method != null && eventInfo.remove_method.IsPublic))
			{
				return eventInfo.remove_method;
			}
			return null;
		}

		// Token: 0x060022C9 RID: 8905 RVA: 0x00080804 File Offset: 0x0007EA04
		public override MethodInfo[] GetOtherMethods(bool nonPublic)
		{
			MonoEventInfo eventInfo = MonoEventInfo.GetEventInfo(this);
			if (nonPublic)
			{
				return eventInfo.other_methods;
			}
			int num = 0;
			MethodInfo[] other_methods = eventInfo.other_methods;
			for (int i = 0; i < other_methods.Length; i++)
			{
				if (other_methods[i].IsPublic)
				{
					num++;
				}
			}
			if (num == eventInfo.other_methods.Length)
			{
				return eventInfo.other_methods;
			}
			MethodInfo[] array = new MethodInfo[num];
			num = 0;
			foreach (MethodInfo methodInfo in eventInfo.other_methods)
			{
				if (methodInfo.IsPublic)
				{
					array[num++] = methodInfo;
				}
			}
			return array;
		}

		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x060022CA RID: 8906 RVA: 0x00080899 File Offset: 0x0007EA99
		public override Type DeclaringType
		{
			get
			{
				return MonoEventInfo.GetEventInfo(this).declaring_type;
			}
		}

		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x060022CB RID: 8907 RVA: 0x000808A6 File Offset: 0x0007EAA6
		public override Type ReflectedType
		{
			get
			{
				return MonoEventInfo.GetEventInfo(this).reflected_type;
			}
		}

		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x060022CC RID: 8908 RVA: 0x000808B3 File Offset: 0x0007EAB3
		public override string Name
		{
			get
			{
				return MonoEventInfo.GetEventInfo(this).name;
			}
		}

		// Token: 0x060022CD RID: 8909 RVA: 0x000808C0 File Offset: 0x0007EAC0
		public override string ToString()
		{
			return this.EventHandlerType + " " + this.Name;
		}

		// Token: 0x060022CE RID: 8910 RVA: 0x00032B0D File Offset: 0x00030D0D
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x060022CF RID: 8911 RVA: 0x0007E188 File Offset: 0x0007C388
		public override object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x060022D0 RID: 8912 RVA: 0x0007E191 File Offset: 0x0007C391
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x060022D1 RID: 8913 RVA: 0x000808D8 File Offset: 0x0007EAD8
		public override IList<CustomAttributeData> GetCustomAttributesData()
		{
			return CustomAttributeData.GetCustomAttributes(this);
		}

		// Token: 0x060022D2 RID: 8914 RVA: 0x000808E0 File Offset: 0x0007EAE0
		public MonoEvent()
		{
		}

		// Token: 0x04001301 RID: 4865
		private IntPtr klass;

		// Token: 0x04001302 RID: 4866
		private IntPtr handle;
	}
}
