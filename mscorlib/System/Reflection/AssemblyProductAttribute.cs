﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines a product name custom attribute for an assembly manifest.</summary>
	// Token: 0x020002B7 RID: 695
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyProductAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyProductAttribute" /> class.</summary>
		/// <param name="product">The product name information. </param>
		// Token: 0x06001FB6 RID: 8118 RVA: 0x0007C7D8 File Offset: 0x0007A9D8
		public AssemblyProductAttribute(string product)
		{
			this.m_product = product;
		}

		/// <summary>Gets product name information.</summary>
		/// <returns>A string containing the product name.</returns>
		// Token: 0x1700045B RID: 1115
		// (get) Token: 0x06001FB7 RID: 8119 RVA: 0x0007C7E7 File Offset: 0x0007A9E7
		public string Product
		{
			get
			{
				return this.m_product;
			}
		}

		// Token: 0x0400112B RID: 4395
		private string m_product;
	}
}
