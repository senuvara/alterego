﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies which culture the assembly supports.</summary>
	// Token: 0x020002BF RID: 703
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyCultureAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyCultureAttribute" /> class with the culture supported by the assembly being attributed.</summary>
		/// <param name="culture">The culture supported by the attributed assembly. </param>
		// Token: 0x06001FC6 RID: 8134 RVA: 0x0007C89E File Offset: 0x0007AA9E
		public AssemblyCultureAttribute(string culture)
		{
			this.m_culture = culture;
		}

		/// <summary>Gets the supported culture of the attributed assembly.</summary>
		/// <returns>A string containing the name of the supported culture.</returns>
		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x06001FC7 RID: 8135 RVA: 0x0007C8AD File Offset: 0x0007AAAD
		public string Culture
		{
			get
			{
				return this.m_culture;
			}
		}

		// Token: 0x04001133 RID: 4403
		private string m_culture;
	}
}
