﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies the name of a file containing the key pair used to generate a strong name.</summary>
	// Token: 0x020002C1 RID: 705
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyKeyFileAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see langword="AssemblyKeyFileAttribute" /> class with the name of the file containing the key pair to generate a strong name for the assembly being attributed.</summary>
		/// <param name="keyFile">The name of the file containing the key pair. </param>
		// Token: 0x06001FCA RID: 8138 RVA: 0x0007C8CC File Offset: 0x0007AACC
		public AssemblyKeyFileAttribute(string keyFile)
		{
			this.m_keyFile = keyFile;
		}

		/// <summary>Gets the name of the file containing the key pair used to generate a strong name for the attributed assembly.</summary>
		/// <returns>A string containing the name of the file that contains the key pair.</returns>
		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x06001FCB RID: 8139 RVA: 0x0007C8DB File Offset: 0x0007AADB
		public string KeyFile
		{
			get
			{
				return this.m_keyFile;
			}
		}

		// Token: 0x04001135 RID: 4405
		private string m_keyFile;
	}
}
