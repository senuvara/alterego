﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies a description for an assembly.</summary>
	// Token: 0x020002BA RID: 698
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyTitleAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyTitleAttribute" /> class.</summary>
		/// <param name="title">The assembly title. </param>
		// Token: 0x06001FBC RID: 8124 RVA: 0x0007C81D File Offset: 0x0007AA1D
		public AssemblyTitleAttribute(string title)
		{
			this.m_title = title;
		}

		/// <summary>Gets assembly title information.</summary>
		/// <returns>The assembly title.</returns>
		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x06001FBD RID: 8125 RVA: 0x0007C82C File Offset: 0x0007AA2C
		public string Title
		{
			get
			{
				return this.m_title;
			}
		}

		// Token: 0x0400112E RID: 4398
		private string m_title;
	}
}
