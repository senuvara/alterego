﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Reflection
{
	// Token: 0x0200031E RID: 798
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	internal class MonoCMethod : RuntimeConstructorInfo
	{
		// Token: 0x0600233F RID: 9023 RVA: 0x00081214 File Offset: 0x0007F414
		public override MethodImplAttributes GetMethodImplementationFlags()
		{
			return MonoMethodInfo.GetMethodImplementationFlags(this.mhandle);
		}

		// Token: 0x06002340 RID: 9024 RVA: 0x00081221 File Offset: 0x0007F421
		public override ParameterInfo[] GetParameters()
		{
			return MonoMethodInfo.GetParametersInfo(this.mhandle, this);
		}

		// Token: 0x06002341 RID: 9025 RVA: 0x00081221 File Offset: 0x0007F421
		internal override ParameterInfo[] GetParametersInternal()
		{
			return MonoMethodInfo.GetParametersInfo(this.mhandle, this);
		}

		// Token: 0x06002342 RID: 9026 RVA: 0x00081230 File Offset: 0x0007F430
		internal override int GetParametersCount()
		{
			ParameterInfo[] parametersInfo = MonoMethodInfo.GetParametersInfo(this.mhandle, this);
			if (parametersInfo != null)
			{
				return parametersInfo.Length;
			}
			return 0;
		}

		// Token: 0x06002343 RID: 9027
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern object InternalInvoke(object obj, object[] parameters, out Exception exc);

		// Token: 0x06002344 RID: 9028 RVA: 0x00081252 File Offset: 0x0007F452
		[DebuggerHidden]
		[DebuggerStepThrough]
		public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			if (obj == null)
			{
				if (!base.IsStatic)
				{
					throw new TargetException("Instance constructor requires a target");
				}
			}
			else if (!this.DeclaringType.IsInstanceOfType(obj))
			{
				throw new TargetException("Constructor does not match target type");
			}
			return this.DoInvoke(obj, invokeAttr, binder, parameters, culture);
		}

		// Token: 0x06002345 RID: 9029 RVA: 0x00081290 File Offset: 0x0007F490
		private object DoInvoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			if (binder == null)
			{
				binder = Type.DefaultBinder;
			}
			ParameterInfo[] parametersInfo = MonoMethodInfo.GetParametersInfo(this.mhandle, this);
			MonoMethod.ConvertValues(binder, parameters, parametersInfo, culture, invokeAttr);
			if (obj == null && this.DeclaringType.ContainsGenericParameters)
			{
				throw new MemberAccessException("Cannot create an instance of " + this.DeclaringType + " because Type.ContainsGenericParameters is true.");
			}
			if ((invokeAttr & BindingFlags.CreateInstance) != BindingFlags.Default && this.DeclaringType.IsAbstract)
			{
				throw new MemberAccessException(string.Format("Cannot create an instance of {0} because it is an abstract class", this.DeclaringType));
			}
			return this.InternalInvoke(obj, parameters);
		}

		// Token: 0x06002346 RID: 9030 RVA: 0x00081320 File Offset: 0x0007F520
		public object InternalInvoke(object obj, object[] parameters)
		{
			object result = null;
			Exception ex;
			try
			{
				result = this.InternalInvoke(obj, parameters, out ex);
			}
			catch (MethodAccessException)
			{
				throw;
			}
			catch (Exception inner)
			{
				throw new TargetInvocationException(inner);
			}
			if (ex != null)
			{
				throw ex;
			}
			if (obj != null)
			{
				return null;
			}
			return result;
		}

		// Token: 0x06002347 RID: 9031 RVA: 0x0008136C File Offset: 0x0007F56C
		[DebuggerHidden]
		[DebuggerStepThrough]
		public override object Invoke(BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			return this.DoInvoke(null, invokeAttr, binder, parameters, culture);
		}

		// Token: 0x17000552 RID: 1362
		// (get) Token: 0x06002348 RID: 9032 RVA: 0x0008137A File Offset: 0x0007F57A
		public override RuntimeMethodHandle MethodHandle
		{
			get
			{
				return new RuntimeMethodHandle(this.mhandle);
			}
		}

		// Token: 0x17000553 RID: 1363
		// (get) Token: 0x06002349 RID: 9033 RVA: 0x00081387 File Offset: 0x0007F587
		public override MethodAttributes Attributes
		{
			get
			{
				return MonoMethodInfo.GetAttributes(this.mhandle);
			}
		}

		// Token: 0x17000554 RID: 1364
		// (get) Token: 0x0600234A RID: 9034 RVA: 0x00081394 File Offset: 0x0007F594
		public override CallingConventions CallingConvention
		{
			get
			{
				return MonoMethodInfo.GetCallingConvention(this.mhandle);
			}
		}

		// Token: 0x17000555 RID: 1365
		// (get) Token: 0x0600234B RID: 9035 RVA: 0x000813A1 File Offset: 0x0007F5A1
		public override bool ContainsGenericParameters
		{
			get
			{
				return this.DeclaringType.ContainsGenericParameters;
			}
		}

		// Token: 0x17000556 RID: 1366
		// (get) Token: 0x0600234C RID: 9036 RVA: 0x000813AE File Offset: 0x0007F5AE
		public override Type ReflectedType
		{
			get
			{
				return this.reftype;
			}
		}

		// Token: 0x17000557 RID: 1367
		// (get) Token: 0x0600234D RID: 9037 RVA: 0x000813B6 File Offset: 0x0007F5B6
		public override Type DeclaringType
		{
			get
			{
				return MonoMethodInfo.GetDeclaringType(this.mhandle);
			}
		}

		// Token: 0x17000558 RID: 1368
		// (get) Token: 0x0600234E RID: 9038 RVA: 0x000813C3 File Offset: 0x0007F5C3
		public override string Name
		{
			get
			{
				if (this.name != null)
				{
					return this.name;
				}
				return MonoMethod.get_name(this);
			}
		}

		// Token: 0x0600234F RID: 9039 RVA: 0x00032B0D File Offset: 0x00030D0D
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x06002350 RID: 9040 RVA: 0x0007E188 File Offset: 0x0007C388
		public override object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x06002351 RID: 9041 RVA: 0x0007E191 File Offset: 0x0007C391
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x06002352 RID: 9042 RVA: 0x000813DA File Offset: 0x0007F5DA
		public override MethodBody GetMethodBody()
		{
			return MethodBase.GetMethodBody(this.mhandle);
		}

		// Token: 0x06002353 RID: 9043 RVA: 0x000813E8 File Offset: 0x0007F5E8
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Void ");
			stringBuilder.Append(this.Name);
			stringBuilder.Append("(");
			ParameterInfo[] parameters = this.GetParameters();
			for (int i = 0; i < parameters.Length; i++)
			{
				if (i > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append(parameters[i].ParameterType.Name);
			}
			if (this.CallingConvention == CallingConventions.Any)
			{
				stringBuilder.Append(", ...");
			}
			stringBuilder.Append(")");
			return stringBuilder.ToString();
		}

		// Token: 0x06002354 RID: 9044 RVA: 0x000808D8 File Offset: 0x0007EAD8
		public override IList<CustomAttributeData> GetCustomAttributesData()
		{
			return CustomAttributeData.GetCustomAttributes(this);
		}

		// Token: 0x06002355 RID: 9045 RVA: 0x00004E08 File Offset: 0x00003008
		private static int get_core_clr_security_level()
		{
			return 1;
		}

		// Token: 0x17000559 RID: 1369
		// (get) Token: 0x06002356 RID: 9046 RVA: 0x00081480 File Offset: 0x0007F680
		public override bool IsSecurityTransparent
		{
			get
			{
				return MonoCMethod.get_core_clr_security_level() == 0;
			}
		}

		// Token: 0x1700055A RID: 1370
		// (get) Token: 0x06002357 RID: 9047 RVA: 0x0008148A File Offset: 0x0007F68A
		public override bool IsSecurityCritical
		{
			get
			{
				return MonoCMethod.get_core_clr_security_level() > 0;
			}
		}

		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x06002358 RID: 9048 RVA: 0x00081494 File Offset: 0x0007F694
		public override bool IsSecuritySafeCritical
		{
			get
			{
				return MonoCMethod.get_core_clr_security_level() == 1;
			}
		}

		// Token: 0x06002359 RID: 9049 RVA: 0x0008149E File Offset: 0x0007F69E
		public MonoCMethod()
		{
		}

		// Token: 0x04001310 RID: 4880
		internal IntPtr mhandle;

		// Token: 0x04001311 RID: 4881
		private string name;

		// Token: 0x04001312 RID: 4882
		private Type reftype;
	}
}
