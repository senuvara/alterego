﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Represents a clause in a structured exception-handling block.</summary>
	// Token: 0x0200030A RID: 778
	[ComVisible(true)]
	[StructLayout(LayoutKind.Sequential)]
	public class ExceptionHandlingClause
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.ExceptionHandlingClause" /> class.</summary>
		// Token: 0x0600221A RID: 8730 RVA: 0x00002050 File Offset: 0x00000250
		protected ExceptionHandlingClause()
		{
		}

		/// <summary>Gets the type of exception handled by this clause.</summary>
		/// <returns>A <see cref="T:System.Type" /> object that represents that type of exception handled by this clause, or <see langword="null" /> if the <see cref="P:System.Reflection.ExceptionHandlingClause.Flags" /> property is <see cref="F:System.Reflection.ExceptionHandlingClauseOptions.Filter" /> or <see cref="F:System.Reflection.ExceptionHandlingClauseOptions.Finally" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">Invalid use of property for the object's current state.</exception>
		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x0600221B RID: 8731 RVA: 0x0007FE86 File Offset: 0x0007E086
		public virtual Type CatchType
		{
			get
			{
				return this.catch_type;
			}
		}

		/// <summary>Gets the offset within the method body, in bytes, of the user-supplied filter code.</summary>
		/// <returns>The offset within the method body, in bytes, of the user-supplied filter code. The value of this property has no meaning if the <see cref="P:System.Reflection.ExceptionHandlingClause.Flags" /> property has any value other than <see cref="F:System.Reflection.ExceptionHandlingClauseOptions.Filter" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">Cannot get the offset because the exception handling clause is not a filter.</exception>
		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x0600221C RID: 8732 RVA: 0x0007FE8E File Offset: 0x0007E08E
		public virtual int FilterOffset
		{
			get
			{
				return this.filter_offset;
			}
		}

		/// <summary>Gets a value indicating whether this exception-handling clause is a finally clause, a type-filtered clause, or a user-filtered clause.</summary>
		/// <returns>An <see cref="T:System.Reflection.ExceptionHandlingClauseOptions" /> value that indicates what kind of action this clause performs.</returns>
		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x0600221D RID: 8733 RVA: 0x0007FE96 File Offset: 0x0007E096
		public virtual ExceptionHandlingClauseOptions Flags
		{
			get
			{
				return this.flags;
			}
		}

		/// <summary>Gets the length, in bytes, of the body of this exception-handling clause.</summary>
		/// <returns>An integer that represents the length, in bytes, of the MSIL that forms the body of this exception-handling clause.</returns>
		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x0600221E RID: 8734 RVA: 0x0007FE9E File Offset: 0x0007E09E
		public virtual int HandlerLength
		{
			get
			{
				return this.handler_length;
			}
		}

		/// <summary>Gets the offset within the method body, in bytes, of this exception-handling clause.</summary>
		/// <returns>An integer that represents the offset within the method body, in bytes, of this exception-handling clause.</returns>
		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x0600221F RID: 8735 RVA: 0x0007FEA6 File Offset: 0x0007E0A6
		public virtual int HandlerOffset
		{
			get
			{
				return this.handler_offset;
			}
		}

		/// <summary>The total length, in bytes, of the try block that includes this exception-handling clause.</summary>
		/// <returns>The total length, in bytes, of the try block that includes this exception-handling clause.</returns>
		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x06002220 RID: 8736 RVA: 0x0007FEAE File Offset: 0x0007E0AE
		public virtual int TryLength
		{
			get
			{
				return this.try_length;
			}
		}

		/// <summary>The offset within the method, in bytes, of the try block that includes this exception-handling clause.</summary>
		/// <returns>An integer that represents the offset within the method, in bytes, of the try block that includes this exception-handling clause.</returns>
		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x06002221 RID: 8737 RVA: 0x0007FEB6 File Offset: 0x0007E0B6
		public virtual int TryOffset
		{
			get
			{
				return this.try_offset;
			}
		}

		/// <summary>A string representation of the exception-handling clause.</summary>
		/// <returns>A string that lists appropriate property values for the filter clause type.</returns>
		// Token: 0x06002222 RID: 8738 RVA: 0x0007FEC0 File Offset: 0x0007E0C0
		public override string ToString()
		{
			string text = string.Format("Flags={0}, TryOffset={1}, TryLength={2}, HandlerOffset={3}, HandlerLength={4}", new object[]
			{
				this.flags,
				this.try_offset,
				this.try_length,
				this.handler_offset,
				this.handler_length
			});
			if (this.catch_type != null)
			{
				text = string.Format("{0}, CatchType={1}", text, this.catch_type);
			}
			if (this.flags == ExceptionHandlingClauseOptions.Filter)
			{
				text = string.Format("{0}, FilterOffset={1}", text, this.filter_offset);
			}
			return text;
		}

		// Token: 0x040012D6 RID: 4822
		internal Type catch_type;

		// Token: 0x040012D7 RID: 4823
		internal int filter_offset;

		// Token: 0x040012D8 RID: 4824
		internal ExceptionHandlingClauseOptions flags;

		// Token: 0x040012D9 RID: 4825
		internal int try_offset;

		// Token: 0x040012DA RID: 4826
		internal int try_length;

		// Token: 0x040012DB RID: 4827
		internal int handler_offset;

		// Token: 0x040012DC RID: 4828
		internal int handler_length;
	}
}
