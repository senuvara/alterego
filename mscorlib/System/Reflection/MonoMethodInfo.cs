﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200031A RID: 794
	internal struct MonoMethodInfo
	{
		// Token: 0x060022F7 RID: 8951
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_method_info(IntPtr handle, out MonoMethodInfo info);

		// Token: 0x060022F8 RID: 8952
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int get_method_attributes(IntPtr handle);

		// Token: 0x060022F9 RID: 8953 RVA: 0x00080C14 File Offset: 0x0007EE14
		internal static MonoMethodInfo GetMethodInfo(IntPtr handle)
		{
			MonoMethodInfo result;
			MonoMethodInfo.get_method_info(handle, out result);
			return result;
		}

		// Token: 0x060022FA RID: 8954 RVA: 0x00080C2A File Offset: 0x0007EE2A
		internal static Type GetDeclaringType(IntPtr handle)
		{
			return MonoMethodInfo.GetMethodInfo(handle).parent;
		}

		// Token: 0x060022FB RID: 8955 RVA: 0x00080C37 File Offset: 0x0007EE37
		internal static Type GetReturnType(IntPtr handle)
		{
			return MonoMethodInfo.GetMethodInfo(handle).ret;
		}

		// Token: 0x060022FC RID: 8956 RVA: 0x00080C44 File Offset: 0x0007EE44
		internal static MethodAttributes GetAttributes(IntPtr handle)
		{
			return (MethodAttributes)MonoMethodInfo.get_method_attributes(handle);
		}

		// Token: 0x060022FD RID: 8957 RVA: 0x00080C4C File Offset: 0x0007EE4C
		internal static CallingConventions GetCallingConvention(IntPtr handle)
		{
			return MonoMethodInfo.GetMethodInfo(handle).callconv;
		}

		// Token: 0x060022FE RID: 8958 RVA: 0x00080C59 File Offset: 0x0007EE59
		internal static MethodImplAttributes GetMethodImplementationFlags(IntPtr handle)
		{
			return MonoMethodInfo.GetMethodInfo(handle).iattrs;
		}

		// Token: 0x060022FF RID: 8959
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern ParameterInfo[] get_parameter_info(IntPtr handle, MemberInfo member);

		// Token: 0x06002300 RID: 8960 RVA: 0x00080C66 File Offset: 0x0007EE66
		internal static ParameterInfo[] GetParametersInfo(IntPtr handle, MemberInfo member)
		{
			return MonoMethodInfo.get_parameter_info(handle, member);
		}

		// Token: 0x06002301 RID: 8961
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern MarshalAsAttribute get_retval_marshal(IntPtr handle);

		// Token: 0x06002302 RID: 8962 RVA: 0x00080C6F File Offset: 0x0007EE6F
		internal static ParameterInfo GetReturnParameterInfo(MonoMethod method)
		{
			return ParameterInfo.New(MonoMethodInfo.GetReturnType(method.mhandle), method, MonoMethodInfo.get_retval_marshal(method.mhandle));
		}

		// Token: 0x04001308 RID: 4872
		private Type parent;

		// Token: 0x04001309 RID: 4873
		private Type ret;

		// Token: 0x0400130A RID: 4874
		internal MethodAttributes attrs;

		// Token: 0x0400130B RID: 4875
		internal MethodImplAttributes iattrs;

		// Token: 0x0400130C RID: 4876
		private CallingConventions callconv;
	}
}
