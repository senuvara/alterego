﻿using System;
using System.Runtime.CompilerServices;

namespace System.Reflection
{
	// Token: 0x02000323 RID: 803
	internal struct MonoPropertyInfo
	{
		// Token: 0x06002386 RID: 9094
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void get_property_info(MonoProperty prop, ref MonoPropertyInfo info, PInfo req_info);

		// Token: 0x06002387 RID: 9095
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Type[] GetTypeModifiers(MonoProperty prop, bool optional);

		// Token: 0x06002388 RID: 9096
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern object get_default_value(MonoProperty prop);

		// Token: 0x04001313 RID: 4883
		public Type parent;

		// Token: 0x04001314 RID: 4884
		public Type declaring_type;

		// Token: 0x04001315 RID: 4885
		public string name;

		// Token: 0x04001316 RID: 4886
		public MethodInfo get_method;

		// Token: 0x04001317 RID: 4887
		public MethodInfo set_method;

		// Token: 0x04001318 RID: 4888
		public PropertyAttributes attrs;
	}
}
