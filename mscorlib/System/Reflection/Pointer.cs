﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System.Reflection
{
	/// <summary>Provides a wrapper class for pointers.</summary>
	// Token: 0x020002EF RID: 751
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class Pointer : ISerializable
	{
		// Token: 0x06002092 RID: 8338 RVA: 0x00002050 File Offset: 0x00000250
		private Pointer()
		{
		}

		// Token: 0x06002093 RID: 8339 RVA: 0x0007D744 File Offset: 0x0007B944
		[SecurityCritical]
		private Pointer(SerializationInfo info, StreamingContext context)
		{
			this._ptr = ((IntPtr)info.GetValue("_ptr", typeof(IntPtr))).ToPointer();
			this._ptrType = (RuntimeType)info.GetValue("_ptrType", typeof(RuntimeType));
		}

		/// <summary>Boxes the supplied unmanaged memory pointer and the type associated with that pointer into a managed <see cref="T:System.Reflection.Pointer" /> wrapper object. The value and the type are saved so they can be accessed from the native code during an invocation.</summary>
		/// <param name="ptr">The supplied unmanaged memory pointer. </param>
		/// <param name="type">The type associated with the <paramref name="ptr" /> parameter. </param>
		/// <returns>A pointer object.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="type" /> is not a pointer. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> is <see langword="null" />. </exception>
		// Token: 0x06002094 RID: 8340 RVA: 0x0007D7A0 File Offset: 0x0007B9A0
		[SecurityCritical]
		public unsafe static object Box(void* ptr, Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (!type.IsPointer)
			{
				throw new ArgumentException(Environment.GetResourceString("Type must be a Pointer."), "ptr");
			}
			RuntimeType runtimeType = type as RuntimeType;
			if (runtimeType == null)
			{
				throw new ArgumentException(Environment.GetResourceString("Type must be a Pointer."), "ptr");
			}
			return new Pointer
			{
				_ptr = ptr,
				_ptrType = runtimeType
			};
		}

		/// <summary>Returns the stored pointer.</summary>
		/// <param name="ptr">The stored pointer. </param>
		/// <returns>This method returns void.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="ptr" /> is not a pointer. </exception>
		// Token: 0x06002095 RID: 8341 RVA: 0x0007D816 File Offset: 0x0007BA16
		[SecurityCritical]
		public unsafe static void* Unbox(object ptr)
		{
			if (!(ptr is Pointer))
			{
				throw new ArgumentException(Environment.GetResourceString("Type must be a Pointer."), "ptr");
			}
			return ((Pointer)ptr)._ptr;
		}

		// Token: 0x06002096 RID: 8342 RVA: 0x0007D840 File Offset: 0x0007BA40
		internal RuntimeType GetPointerType()
		{
			return this._ptrType;
		}

		// Token: 0x06002097 RID: 8343 RVA: 0x0007D848 File Offset: 0x0007BA48
		[SecurityCritical]
		internal object GetPointerValue()
		{
			return (IntPtr)this._ptr;
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the file name, fusion log, and additional exception information.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		// Token: 0x06002098 RID: 8344 RVA: 0x0007D85A File Offset: 0x0007BA5A
		[SecurityCritical]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_ptr", new IntPtr(this._ptr));
			info.AddValue("_ptrType", this._ptrType);
		}

		// Token: 0x04001266 RID: 4710
		[SecurityCritical]
		private unsafe void* _ptr;

		// Token: 0x04001267 RID: 4711
		private RuntimeType _ptrType;
	}
}
