﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies the build configuration, such as retail or debug, for an assembly.</summary>
	// Token: 0x020002BB RID: 699
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyConfigurationAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyConfigurationAttribute" /> class.</summary>
		/// <param name="configuration">The assembly configuration. </param>
		// Token: 0x06001FBE RID: 8126 RVA: 0x0007C834 File Offset: 0x0007AA34
		public AssemblyConfigurationAttribute(string configuration)
		{
			this.m_configuration = configuration;
		}

		/// <summary>Gets assembly configuration information.</summary>
		/// <returns>A string containing the assembly configuration information.</returns>
		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x06001FBF RID: 8127 RVA: 0x0007C843 File Offset: 0x0007AA43
		public string Configuration
		{
			get
			{
				return this.m_configuration;
			}
		}

		// Token: 0x0400112F RID: 4399
		private string m_configuration;
	}
}
