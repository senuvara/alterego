﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines a company name custom attribute for an assembly manifest.</summary>
	// Token: 0x020002B8 RID: 696
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyCompanyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyCompanyAttribute" /> class.</summary>
		/// <param name="company">The company name information. </param>
		// Token: 0x06001FB8 RID: 8120 RVA: 0x0007C7EF File Offset: 0x0007A9EF
		public AssemblyCompanyAttribute(string company)
		{
			this.m_company = company;
		}

		/// <summary>Gets company name information.</summary>
		/// <returns>A string containing the company name.</returns>
		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x06001FB9 RID: 8121 RVA: 0x0007C7FE File Offset: 0x0007A9FE
		public string Company
		{
			get
			{
				return this.m_company;
			}
		}

		// Token: 0x0400112C RID: 4396
		private string m_company;
	}
}
