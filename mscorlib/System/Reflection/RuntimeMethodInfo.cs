﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace System.Reflection
{
	// Token: 0x0200031B RID: 795
	internal abstract class RuntimeMethodInfo : MethodInfo, ISerializable
	{
		// Token: 0x1700053D RID: 1341
		// (get) Token: 0x06002303 RID: 8963 RVA: 0x00002526 File Offset: 0x00000726
		internal BindingFlags BindingFlags
		{
			get
			{
				return BindingFlags.Default;
			}
		}

		// Token: 0x1700053E RID: 1342
		// (get) Token: 0x06002304 RID: 8964 RVA: 0x00080C8D File Offset: 0x0007EE8D
		public override Module Module
		{
			get
			{
				return this.GetRuntimeModule();
			}
		}

		// Token: 0x1700053F RID: 1343
		// (get) Token: 0x06002305 RID: 8965 RVA: 0x000806FB File Offset: 0x0007E8FB
		private RuntimeType ReflectedTypeInternal
		{
			get
			{
				return (RuntimeType)this.ReflectedType;
			}
		}

		// Token: 0x06002306 RID: 8966 RVA: 0x00080C98 File Offset: 0x0007EE98
		internal override string FormatNameAndSig(bool serialization)
		{
			StringBuilder stringBuilder = new StringBuilder(this.Name);
			TypeNameFormatFlags format = serialization ? TypeNameFormatFlags.FormatSerialization : TypeNameFormatFlags.FormatBasic;
			if (this.IsGenericMethod)
			{
				stringBuilder.Append(RuntimeMethodHandle.ConstructInstantiation(this, format));
			}
			stringBuilder.Append("(");
			ParameterInfo.FormatParameters(stringBuilder, this.GetParametersNoCopy(), this.CallingConvention, serialization);
			stringBuilder.Append(")");
			return stringBuilder.ToString();
		}

		// Token: 0x06002307 RID: 8967 RVA: 0x00080D04 File Offset: 0x0007EF04
		public override Delegate CreateDelegate(Type delegateType)
		{
			return Delegate.CreateDelegate(delegateType, this);
		}

		// Token: 0x06002308 RID: 8968 RVA: 0x00080D0D File Offset: 0x0007EF0D
		public override Delegate CreateDelegate(Type delegateType, object target)
		{
			return Delegate.CreateDelegate(delegateType, target, this);
		}

		// Token: 0x06002309 RID: 8969 RVA: 0x00080D17 File Offset: 0x0007EF17
		public override string ToString()
		{
			return this.ReturnType.FormatTypeName() + " " + this.FormatNameAndSig(false);
		}

		// Token: 0x0600230A RID: 8970 RVA: 0x00080D35 File Offset: 0x0007EF35
		internal RuntimeModule GetRuntimeModule()
		{
			return ((RuntimeType)this.DeclaringType).GetRuntimeModule();
		}

		// Token: 0x0600230B RID: 8971 RVA: 0x00080D48 File Offset: 0x0007EF48
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			MemberInfoSerializationHolder.GetSerializationInfo(info, this.Name, this.ReflectedTypeInternal, this.ToString(), this.SerializationToString(), MemberTypes.Method, (this.IsGenericMethod & !this.IsGenericMethodDefinition) ? this.GetGenericArguments() : null);
		}

		// Token: 0x0600230C RID: 8972 RVA: 0x00080D9D File Offset: 0x0007EF9D
		internal string SerializationToString()
		{
			return this.ReturnType.FormatTypeName(true) + " " + this.FormatNameAndSig(true);
		}

		// Token: 0x0600230D RID: 8973 RVA: 0x00080DBC File Offset: 0x0007EFBC
		protected RuntimeMethodInfo()
		{
		}
	}
}
