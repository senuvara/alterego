﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Represents a named argument of a custom attribute in the reflection-only context.</summary>
	// Token: 0x02000306 RID: 774
	[ComVisible(true)]
	[Serializable]
	public struct CustomAttributeNamedArgument
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.CustomAttributeNamedArgument" /> class, which represents the specified field or property of the custom attribute, and specifies the value of the field or property.</summary>
		/// <param name="memberInfo">A field or property of the custom attribute. The new <see cref="T:System.Reflection.CustomAttributeNamedArgument" /> object represents this member and its value.</param>
		/// <param name="value">The value of the field or property of the custom attribute.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="memberInfo" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="memberInfo" /> is not a field or property of the custom attribute.</exception>
		// Token: 0x060021E3 RID: 8675 RVA: 0x0007F9E4 File Offset: 0x0007DBE4
		public CustomAttributeNamedArgument(MemberInfo memberInfo, object value)
		{
			this.memberInfo = memberInfo;
			this.typedArgument = (CustomAttributeTypedArgument)value;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.CustomAttributeNamedArgument" /> class, which represents the specified field or property of the custom attribute, and specifies a <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> object that describes the type and value of the field or property.</summary>
		/// <param name="memberInfo">A field or property of the custom attribute. The new <see cref="T:System.Reflection.CustomAttributeNamedArgument" /> object represents this member and its value.</param>
		/// <param name="typedArgument">An object that describes the type and value of the field or property.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="memberInfo" /> is <see langword="null" />.</exception>
		// Token: 0x060021E4 RID: 8676 RVA: 0x0007F9F9 File Offset: 0x0007DBF9
		public CustomAttributeNamedArgument(MemberInfo memberInfo, CustomAttributeTypedArgument typedArgument)
		{
			this.memberInfo = memberInfo;
			this.typedArgument = typedArgument;
		}

		/// <summary>Gets the attribute member that would be used to set the named argument.</summary>
		/// <returns>The attribute member that would be used to set the named argument.</returns>
		// Token: 0x170004F1 RID: 1265
		// (get) Token: 0x060021E5 RID: 8677 RVA: 0x0007FA09 File Offset: 0x0007DC09
		public MemberInfo MemberInfo
		{
			get
			{
				return this.memberInfo;
			}
		}

		/// <summary>Gets a <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structure that can be used to obtain the type and value of the current named argument.</summary>
		/// <returns>A structure that can be used to obtain the type and value of the current named argument.</returns>
		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x060021E6 RID: 8678 RVA: 0x0007FA11 File Offset: 0x0007DC11
		public CustomAttributeTypedArgument TypedValue
		{
			get
			{
				return this.typedArgument;
			}
		}

		/// <summary>Gets a value that indicates whether the named argument is a field.</summary>
		/// <returns>
		///     <see langword="true" /> if the named argument is a field; otherwise, <see langword="false" />.</returns>
		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x060021E7 RID: 8679 RVA: 0x0007FA19 File Offset: 0x0007DC19
		public bool IsField
		{
			get
			{
				return this.memberInfo.MemberType == MemberTypes.Field;
			}
		}

		/// <summary>Gets the name of the attribute member that would be used to set the named argument.</summary>
		/// <returns>The name of the attribute member that would be used to set the named argument.</returns>
		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x060021E8 RID: 8680 RVA: 0x0007FA29 File Offset: 0x0007DC29
		public string MemberName
		{
			get
			{
				return this.memberInfo.Name;
			}
		}

		/// <summary>Returns a string that consists of the argument name, the equal sign, and a string representation of the argument value.</summary>
		/// <returns>A string that consists of the argument name, the equal sign, and a string representation of the argument value.</returns>
		// Token: 0x060021E9 RID: 8681 RVA: 0x0007FA36 File Offset: 0x0007DC36
		public override string ToString()
		{
			return this.memberInfo.Name + " = " + this.typedArgument.ToString();
		}

		/// <summary>Returns a value that indicates whether this instance is equal to a specified object.</summary>
		/// <param name="obj">An object to compare with this instance, or <see langword="null" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> equals the type and value of this instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x060021EA RID: 8682 RVA: 0x0007FA60 File Offset: 0x0007DC60
		public override bool Equals(object obj)
		{
			if (!(obj is CustomAttributeNamedArgument))
			{
				return false;
			}
			CustomAttributeNamedArgument customAttributeNamedArgument = (CustomAttributeNamedArgument)obj;
			return customAttributeNamedArgument.memberInfo == this.memberInfo && this.typedArgument.Equals(customAttributeNamedArgument.typedArgument);
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x060021EB RID: 8683 RVA: 0x0007FAAF File Offset: 0x0007DCAF
		public override int GetHashCode()
		{
			return (this.memberInfo.GetHashCode() << 16) + this.typedArgument.GetHashCode();
		}

		/// <summary>Tests whether two <see cref="T:System.Reflection.CustomAttributeNamedArgument" /> structures are equivalent.</summary>
		/// <param name="left">The structure to the left of the equality operator.</param>
		/// <param name="right">The structure to the right of the equality operator.</param>
		/// <returns>
		///     <see langword="true" /> if the two <see cref="T:System.Reflection.CustomAttributeNamedArgument" /> structures are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060021EC RID: 8684 RVA: 0x0007FAD1 File Offset: 0x0007DCD1
		public static bool operator ==(CustomAttributeNamedArgument left, CustomAttributeNamedArgument right)
		{
			return left.Equals(right);
		}

		/// <summary>Tests whether two <see cref="T:System.Reflection.CustomAttributeNamedArgument" /> structures are different.</summary>
		/// <param name="left">The structure to the left of the inequality operator.</param>
		/// <param name="right">The structure to the right of the inequality operator.</param>
		/// <returns>
		///     <see langword="true" /> if the two <see cref="T:System.Reflection.CustomAttributeNamedArgument" /> structures are different; otherwise, <see langword="false" />.</returns>
		// Token: 0x060021ED RID: 8685 RVA: 0x0007FAE6 File Offset: 0x0007DCE6
		public static bool operator !=(CustomAttributeNamedArgument left, CustomAttributeNamedArgument right)
		{
			return !left.Equals(right);
		}

		// Token: 0x040012D1 RID: 4817
		private CustomAttributeTypedArgument typedArgument;

		// Token: 0x040012D2 RID: 4818
		private MemberInfo memberInfo;
	}
}
