﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000319 RID: 793
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	internal class MonoField : RtFieldInfo
	{
		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x060022E0 RID: 8928 RVA: 0x00080A36 File Offset: 0x0007EC36
		public override FieldAttributes Attributes
		{
			get
			{
				return this.attrs;
			}
		}

		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x060022E1 RID: 8929 RVA: 0x00080A3E File Offset: 0x0007EC3E
		public override RuntimeFieldHandle FieldHandle
		{
			get
			{
				return this.fhandle;
			}
		}

		// Token: 0x060022E2 RID: 8930
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type ResolveType();

		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x060022E3 RID: 8931 RVA: 0x00080A46 File Offset: 0x0007EC46
		public override Type FieldType
		{
			get
			{
				if (this.type == null)
				{
					this.type = this.ResolveType();
				}
				return this.type;
			}
		}

		// Token: 0x060022E4 RID: 8932
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type GetParentType(bool declaring);

		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x060022E5 RID: 8933 RVA: 0x00080A68 File Offset: 0x0007EC68
		public override Type ReflectedType
		{
			get
			{
				return this.GetParentType(false);
			}
		}

		// Token: 0x1700053B RID: 1339
		// (get) Token: 0x060022E6 RID: 8934 RVA: 0x00080A71 File Offset: 0x0007EC71
		public override Type DeclaringType
		{
			get
			{
				return this.GetParentType(true);
			}
		}

		// Token: 0x1700053C RID: 1340
		// (get) Token: 0x060022E7 RID: 8935 RVA: 0x00080A7A File Offset: 0x0007EC7A
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x060022E8 RID: 8936 RVA: 0x00032B0D File Offset: 0x00030D0D
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x060022E9 RID: 8937 RVA: 0x0007E188 File Offset: 0x0007C388
		public override object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x060022EA RID: 8938 RVA: 0x0007E191 File Offset: 0x0007C391
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x060022EB RID: 8939
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal override extern int GetFieldOffset();

		// Token: 0x060022EC RID: 8940
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern object GetValueInternal(object obj);

		// Token: 0x060022ED RID: 8941 RVA: 0x00080A84 File Offset: 0x0007EC84
		public override object GetValue(object obj)
		{
			if (!base.IsStatic)
			{
				if (obj == null)
				{
					throw new TargetException("Non-static field requires a target");
				}
				if (!this.DeclaringType.IsAssignableFrom(obj.GetType()))
				{
					throw new ArgumentException(string.Format("Field {0} defined on type {1} is not a field on the target object which is of type {2}.", this.Name, this.DeclaringType, obj.GetType()), "obj");
				}
			}
			if (!base.IsLiteral)
			{
				this.CheckGeneric();
			}
			return this.GetValueInternal(obj);
		}

		// Token: 0x060022EE RID: 8942 RVA: 0x00080AF6 File Offset: 0x0007ECF6
		public override string ToString()
		{
			return string.Format("{0} {1}", this.FieldType, this.name);
		}

		// Token: 0x060022EF RID: 8943
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetValueInternal(FieldInfo fi, object obj, object value);

		// Token: 0x060022F0 RID: 8944 RVA: 0x00080B10 File Offset: 0x0007ED10
		public override void SetValue(object obj, object val, BindingFlags invokeAttr, Binder binder, CultureInfo culture)
		{
			if (!base.IsStatic)
			{
				if (obj == null)
				{
					throw new TargetException("Non-static field requires a target");
				}
				if (!this.DeclaringType.IsAssignableFrom(obj.GetType()))
				{
					throw new ArgumentException(string.Format("Field {0} defined on type {1} is not a field on the target object which is of type {2}.", this.Name, this.DeclaringType, obj.GetType()), "obj");
				}
			}
			if (base.IsLiteral)
			{
				throw new FieldAccessException("Cannot set a constant field");
			}
			if (binder == null)
			{
				binder = Type.DefaultBinder;
			}
			this.CheckGeneric();
			if (val != null)
			{
				val = ((RuntimeType)this.FieldType).CheckValue(val, binder, culture, invokeAttr);
			}
			MonoField.SetValueInternal(this, obj, val);
		}

		// Token: 0x060022F1 RID: 8945 RVA: 0x00080BB4 File Offset: 0x0007EDB4
		internal MonoField Clone(string newName)
		{
			return new MonoField
			{
				name = newName,
				type = this.type,
				attrs = this.attrs,
				klass = this.klass,
				fhandle = this.fhandle
			};
		}

		// Token: 0x060022F2 RID: 8946
		[MethodImpl(MethodImplOptions.InternalCall)]
		public override extern object GetRawConstantValue();

		// Token: 0x060022F3 RID: 8947 RVA: 0x000808D8 File Offset: 0x0007EAD8
		public override IList<CustomAttributeData> GetCustomAttributesData()
		{
			return CustomAttributeData.GetCustomAttributes(this);
		}

		// Token: 0x060022F4 RID: 8948 RVA: 0x00080BF2 File Offset: 0x0007EDF2
		private void CheckGeneric()
		{
			if (this.DeclaringType.ContainsGenericParameters)
			{
				throw new InvalidOperationException("Late bound operations cannot be performed on fields with types for which Type.ContainsGenericParameters is true.");
			}
		}

		// Token: 0x060022F5 RID: 8949 RVA: 0x00004E08 File Offset: 0x00003008
		private static int get_core_clr_security_level()
		{
			return 1;
		}

		// Token: 0x060022F6 RID: 8950 RVA: 0x00080C0C File Offset: 0x0007EE0C
		public MonoField()
		{
		}

		// Token: 0x04001303 RID: 4867
		internal IntPtr klass;

		// Token: 0x04001304 RID: 4868
		internal RuntimeFieldHandle fhandle;

		// Token: 0x04001305 RID: 4869
		private string name;

		// Token: 0x04001306 RID: 4870
		private Type type;

		// Token: 0x04001307 RID: 4871
		private FieldAttributes attrs;
	}
}
