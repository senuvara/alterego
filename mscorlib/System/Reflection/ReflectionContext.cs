﻿using System;

namespace System.Reflection
{
	/// <summary>Represents a context that can provide reflection objects.</summary>
	// Token: 0x020002F1 RID: 753
	public abstract class ReflectionContext
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.ReflectionContext" /> class.</summary>
		// Token: 0x06002099 RID: 8345 RVA: 0x00002050 File Offset: 0x00000250
		protected ReflectionContext()
		{
		}

		/// <summary>Gets the representation, in this reflection context, of an assembly that is represented by an object from another reflection context.</summary>
		/// <param name="assembly">The external representation of the assembly to represent in this context.</param>
		/// <returns>The representation of the assembly in this reflection context.</returns>
		// Token: 0x0600209A RID: 8346
		public abstract Assembly MapAssembly(Assembly assembly);

		/// <summary>Gets the representation, in this reflection context, of a type represented by an object from another reflection context.</summary>
		/// <param name="type">The external representation of the type to represent in this context.</param>
		/// <returns>The representation of the type in this reflection context..</returns>
		// Token: 0x0600209B RID: 8347
		public abstract TypeInfo MapType(TypeInfo type);

		/// <summary>Gets the representation of the type of the specified object in this reflection context.</summary>
		/// <param name="value">The object to represent.</param>
		/// <returns>An object that represents the type of the specified object.</returns>
		// Token: 0x0600209C RID: 8348 RVA: 0x0007D888 File Offset: 0x0007BA88
		public virtual TypeInfo GetTypeForObject(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return this.MapType(value.GetType().GetTypeInfo());
		}
	}
}
