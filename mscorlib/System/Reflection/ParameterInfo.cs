﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using Unity;

namespace System.Reflection
{
	/// <summary>Discovers the attributes of a parameter and provides access to parameter metadata.</summary>
	// Token: 0x0200032C RID: 812
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_ParameterInfo))]
	[ComVisible(true)]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class ParameterInfo : ICustomAttributeProvider, IObjectReference, _ParameterInfo
	{
		/// <summary>Initializes a new instance of the <see langword="ParameterInfo" /> class.</summary>
		// Token: 0x060023C2 RID: 9154 RVA: 0x00002050 File Offset: 0x00000250
		protected ParameterInfo()
		{
		}

		/// <summary>Gets the parameter type and name represented as a string.</summary>
		/// <returns>A string containing the type and the name of the parameter.</returns>
		// Token: 0x060023C3 RID: 9155 RVA: 0x000820D0 File Offset: 0x000802D0
		public override string ToString()
		{
			Type type = this.ClassImpl;
			while (type.HasElementType)
			{
				type = type.GetElementType();
			}
			string text = (type.IsPrimitive || this.ClassImpl == typeof(void) || this.ClassImpl.Namespace == this.MemberImpl.DeclaringType.Namespace) ? this.ClassImpl.Name : this.ClassImpl.FullName;
			if (!this.IsRetval)
			{
				text += " ";
				text += this.NameImpl;
			}
			return text;
		}

		// Token: 0x060023C4 RID: 9156 RVA: 0x00082174 File Offset: 0x00080374
		internal static void FormatParameters(StringBuilder sb, ParameterInfo[] p, CallingConventions callingConvention, bool serialization)
		{
			for (int i = 0; i < p.Length; i++)
			{
				if (i > 0)
				{
					sb.Append(", ");
				}
				Type parameterType = p[i].ParameterType;
				string text = parameterType.FormatTypeName(serialization);
				if (parameterType.IsByRef && !serialization)
				{
					sb.Append(text.TrimEnd(new char[]
					{
						'&'
					}));
					sb.Append(" ByRef");
				}
				else
				{
					sb.Append(text);
				}
			}
			if ((callingConvention & CallingConventions.VarArgs) != (CallingConventions)0)
			{
				if (p.Length != 0)
				{
					sb.Append(", ");
				}
				sb.Append("...");
			}
		}

		/// <summary>Gets the <see langword="Type" /> of this parameter.</summary>
		/// <returns>The <see langword="Type" /> object that represents the <see langword="Type" /> of this parameter.</returns>
		// Token: 0x17000571 RID: 1393
		// (get) Token: 0x060023C5 RID: 9157 RVA: 0x00082208 File Offset: 0x00080408
		public virtual Type ParameterType
		{
			get
			{
				return this.ClassImpl;
			}
		}

		/// <summary>Gets the attributes for this parameter.</summary>
		/// <returns>A <see langword="ParameterAttributes" /> object representing the attributes for this parameter.</returns>
		// Token: 0x17000572 RID: 1394
		// (get) Token: 0x060023C6 RID: 9158 RVA: 0x00082210 File Offset: 0x00080410
		public virtual ParameterAttributes Attributes
		{
			get
			{
				return this.AttrsImpl;
			}
		}

		/// <summary>Gets a value indicating whether this is an input parameter.</summary>
		/// <returns>
		///     <see langword="true" /> if the parameter is an input parameter; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000573 RID: 1395
		// (get) Token: 0x060023C7 RID: 9159 RVA: 0x00082218 File Offset: 0x00080418
		public bool IsIn
		{
			get
			{
				return (this.Attributes & ParameterAttributes.In) > ParameterAttributes.None;
			}
		}

		/// <summary>Gets a value indicating whether this parameter is a locale identifier (lcid).</summary>
		/// <returns>
		///     <see langword="true" /> if the parameter is a locale identifier; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000574 RID: 1396
		// (get) Token: 0x060023C8 RID: 9160 RVA: 0x00082225 File Offset: 0x00080425
		public bool IsLcid
		{
			get
			{
				return (this.Attributes & ParameterAttributes.Lcid) > ParameterAttributes.None;
			}
		}

		/// <summary>Gets a value indicating whether this parameter is optional.</summary>
		/// <returns>
		///     <see langword="true" /> if the parameter is optional; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000575 RID: 1397
		// (get) Token: 0x060023C9 RID: 9161 RVA: 0x00082232 File Offset: 0x00080432
		public bool IsOptional
		{
			get
			{
				return (this.Attributes & ParameterAttributes.Optional) > ParameterAttributes.None;
			}
		}

		/// <summary>Gets a value indicating whether this is an output parameter.</summary>
		/// <returns>
		///     <see langword="true" /> if the parameter is an output parameter; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000576 RID: 1398
		// (get) Token: 0x060023CA RID: 9162 RVA: 0x00082240 File Offset: 0x00080440
		public bool IsOut
		{
			get
			{
				return (this.Attributes & ParameterAttributes.Out) > ParameterAttributes.None;
			}
		}

		/// <summary>Gets a value indicating whether this is a <see langword="Retval" /> parameter.</summary>
		/// <returns>
		///     <see langword="true" /> if the parameter is a <see langword="Retval" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000577 RID: 1399
		// (get) Token: 0x060023CB RID: 9163 RVA: 0x0008224D File Offset: 0x0008044D
		public bool IsRetval
		{
			get
			{
				return (this.Attributes & ParameterAttributes.Retval) > ParameterAttributes.None;
			}
		}

		/// <summary>Gets a value indicating the member in which the parameter is implemented.</summary>
		/// <returns>The member which implanted the parameter represented by this <see cref="T:System.Reflection.ParameterInfo" />.</returns>
		// Token: 0x17000578 RID: 1400
		// (get) Token: 0x060023CC RID: 9164 RVA: 0x0008225A File Offset: 0x0008045A
		public virtual MemberInfo Member
		{
			get
			{
				return this.MemberImpl;
			}
		}

		/// <summary>Gets the name of the parameter.</summary>
		/// <returns>The simple name of this parameter.</returns>
		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x060023CD RID: 9165 RVA: 0x00082262 File Offset: 0x00080462
		public virtual string Name
		{
			get
			{
				return this.NameImpl;
			}
		}

		/// <summary>Gets the zero-based position of the parameter in the formal parameter list.</summary>
		/// <returns>An integer representing the position this parameter occupies in the parameter list.</returns>
		// Token: 0x1700057A RID: 1402
		// (get) Token: 0x060023CE RID: 9166 RVA: 0x0008226A File Offset: 0x0008046A
		public virtual int Position
		{
			get
			{
				return this.PositionImpl;
			}
		}

		// Token: 0x060023CF RID: 9167
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetMetadataToken();

		// Token: 0x060023D0 RID: 9168 RVA: 0x00082274 File Offset: 0x00080474
		internal object[] GetPseudoCustomAttributes()
		{
			int num = 0;
			if (this.IsIn)
			{
				num++;
			}
			if (this.IsOut)
			{
				num++;
			}
			if (this.IsOptional)
			{
				num++;
			}
			if (this.marshalAs != null)
			{
				num++;
			}
			if (num == 0)
			{
				return null;
			}
			object[] array = new object[num];
			num = 0;
			if (this.IsIn)
			{
				array[num++] = new InAttribute();
			}
			if (this.IsOptional)
			{
				array[num++] = new OptionalAttribute();
			}
			if (this.IsOut)
			{
				array[num++] = new OutAttribute();
			}
			if (this.marshalAs != null)
			{
				array[num++] = this.marshalAs.Copy();
			}
			return array;
		}

		// Token: 0x060023D1 RID: 9169
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Type[] GetTypeModifiers(bool optional);

		// Token: 0x060023D2 RID: 9170 RVA: 0x00082318 File Offset: 0x00080518
		internal object GetDefaultValueImpl()
		{
			return this.DefaultValueImpl;
		}

		/// <summary>Gets a collection that contains this parameter's custom attributes.</summary>
		/// <returns>A collection that contains this parameter's custom attributes.</returns>
		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x060023D3 RID: 9171 RVA: 0x00082320 File Offset: 0x00080520
		public virtual IEnumerable<CustomAttributeData> CustomAttributes
		{
			get
			{
				return this.GetCustomAttributesData();
			}
		}

		/// <summary>Gets a value that indicates whether this parameter has a default value.</summary>
		/// <returns>
		///     <see langword="true" /> if this parameter has a default value; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x060023D4 RID: 9172 RVA: 0x000041F3 File Offset: 0x000023F3
		public virtual bool HasDefaultValue
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a value indicating the default value if the parameter has a default value.</summary>
		/// <returns>The default value of the parameter, or <see cref="F:System.DBNull.Value" /> if the parameter has no default value.</returns>
		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x060023D5 RID: 9173 RVA: 0x000041F3 File Offset: 0x000023F3
		public virtual object DefaultValue
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a value indicating the default value if the parameter has a default value.</summary>
		/// <returns>The default value of the parameter, or <see cref="F:System.DBNull.Value" /> if the parameter has no default value.</returns>
		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x060023D6 RID: 9174 RVA: 0x000041F3 File Offset: 0x000023F3
		public virtual object RawDefaultValue
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets a value that identifies this parameter in metadata.</summary>
		/// <returns>A value which, in combination with the module, uniquely identifies this parameter in metadata.</returns>
		// Token: 0x1700057F RID: 1407
		// (get) Token: 0x060023D7 RID: 9175 RVA: 0x00082328 File Offset: 0x00080528
		public virtual int MetadataToken
		{
			get
			{
				return 134217728;
			}
		}

		/// <summary>Gets all the custom attributes defined on this parameter.</summary>
		/// <param name="inherit">This argument is ignored for objects of this type. See Remarks.</param>
		/// <returns>An array that contains all the custom attributes applied to this parameter.</returns>
		/// <exception cref="T:System.TypeLoadException">A custom attribute type could not be loaded. </exception>
		// Token: 0x060023D8 RID: 9176 RVA: 0x0008232F File Offset: 0x0008052F
		public virtual object[] GetCustomAttributes(bool inherit)
		{
			return new object[0];
		}

		/// <summary>Gets the custom attributes of the specified type or its derived types that are applied to this parameter.</summary>
		/// <param name="attributeType">The custom attributes identified by type. </param>
		/// <param name="inherit">This argument is ignored for objects of this type. See Remarks.</param>
		/// <returns>An array that contains the custom attributes of the specified type or its derived types.</returns>
		/// <exception cref="T:System.ArgumentException">The type must be a type provided by the underlying runtime system.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="attributeType" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.TypeLoadException">A custom attribute type could not be loaded. </exception>
		// Token: 0x060023D9 RID: 9177 RVA: 0x0008232F File Offset: 0x0008052F
		public virtual object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return new object[0];
		}

		/// <summary>Returns the real object that should be deserialized instead of the object that the serialized stream specifies.</summary>
		/// <param name="context">The serialized stream from which the current object is deserialized.</param>
		/// <returns>The actual object that is put into the graph.</returns>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The parameter's position in the parameter list of its associated member is not valid for that member's type.</exception>
		// Token: 0x060023DA RID: 9178 RVA: 0x000041F3 File Offset: 0x000023F3
		[SecurityCritical]
		public object GetRealObject(StreamingContext context)
		{
			throw new NotImplementedException();
		}

		/// <summary>Determines whether the custom attribute of the specified type or its derived types is applied to this parameter.</summary>
		/// <param name="attributeType">The <see langword="Type" /> object to search for. </param>
		/// <param name="inherit">This argument is ignored for objects of this type. See Remarks.</param>
		/// <returns>
		///     <see langword="true" /> if one or more instances of <paramref name="attributeType" /> or its derived types are applied to this parameter; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="attributeType" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="attributeType" /> is not a <see cref="T:System.Type" /> object supplied by the common language runtime.</exception>
		// Token: 0x060023DB RID: 9179 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsDefined(Type attributeType, bool inherit)
		{
			return false;
		}

		/// <summary>Gets the required custom modifiers of the parameter.</summary>
		/// <returns>An array of <see cref="T:System.Type" /> objects that identify the required custom modifiers of the current parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsImplicitlyDereferenced" />.</returns>
		// Token: 0x060023DC RID: 9180 RVA: 0x00082337 File Offset: 0x00080537
		public virtual Type[] GetRequiredCustomModifiers()
		{
			return new Type[0];
		}

		/// <summary>Gets the optional custom modifiers of the parameter.</summary>
		/// <returns>An array of <see cref="T:System.Type" /> objects that identify the optional custom modifiers of the current parameter, such as <see cref="T:System.Runtime.CompilerServices.IsConst" /> or <see cref="T:System.Runtime.CompilerServices.IsImplicitlyDereferenced" />.</returns>
		// Token: 0x060023DD RID: 9181 RVA: 0x00082337 File Offset: 0x00080537
		public virtual Type[] GetOptionalCustomModifiers()
		{
			return new Type[0];
		}

		/// <summary>Returns a list of <see cref="T:System.Reflection.CustomAttributeData" /> objects for the current parameter, which can be used in the reflection-only context.</summary>
		/// <returns>A generic list of <see cref="T:System.Reflection.CustomAttributeData" /> objects representing data about the attributes that have been applied to the current parameter.</returns>
		// Token: 0x060023DE RID: 9182 RVA: 0x000041F3 File Offset: 0x000023F3
		public virtual IList<CustomAttributeData> GetCustomAttributesData()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060023DF RID: 9183 RVA: 0x0008233F File Offset: 0x0008053F
		internal static ParameterInfo New(ParameterInfo pinfo, Type type, MemberInfo member, int position)
		{
			return new MonoParameterInfo(pinfo, type, member, position);
		}

		// Token: 0x060023E0 RID: 9184 RVA: 0x0008234A File Offset: 0x0008054A
		internal static ParameterInfo New(ParameterInfo pinfo, MemberInfo member)
		{
			return new MonoParameterInfo(pinfo, member);
		}

		// Token: 0x060023E1 RID: 9185 RVA: 0x00082353 File Offset: 0x00080553
		internal static ParameterInfo New(Type type, MemberInfo member, MarshalAsAttribute marshalAs)
		{
			return new MonoParameterInfo(type, member, marshalAs);
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x060023E2 RID: 9186 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ParameterInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x060023E3 RID: 9187 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ParameterInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x060023E4 RID: 9188 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ParameterInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">Late-bound access using the COM IDispatch interface is not supported.</exception>
		// Token: 0x060023E5 RID: 9189 RVA: 0x00002ABD File Offset: 0x00000CBD
		void _ParameterInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>The <see langword="Type" /> of the parameter.</summary>
		// Token: 0x04001325 RID: 4901
		protected Type ClassImpl;

		/// <summary>The default value of the parameter.</summary>
		// Token: 0x04001326 RID: 4902
		protected object DefaultValueImpl;

		/// <summary>The member in which the field is implemented.</summary>
		// Token: 0x04001327 RID: 4903
		protected MemberInfo MemberImpl;

		/// <summary>The name of the parameter.</summary>
		// Token: 0x04001328 RID: 4904
		protected string NameImpl;

		/// <summary>The zero-based position of the parameter in the parameter list.</summary>
		// Token: 0x04001329 RID: 4905
		protected int PositionImpl;

		/// <summary>The attributes of the parameter.</summary>
		// Token: 0x0400132A RID: 4906
		protected ParameterAttributes AttrsImpl;

		// Token: 0x0400132B RID: 4907
		internal MarshalAsAttribute marshalAs;
	}
}
