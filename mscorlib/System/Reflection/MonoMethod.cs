﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Reflection
{
	// Token: 0x0200031C RID: 796
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	internal class MonoMethod : RuntimeMethodInfo
	{
		// Token: 0x0600230E RID: 8974 RVA: 0x00080DC4 File Offset: 0x0007EFC4
		internal MonoMethod()
		{
		}

		// Token: 0x0600230F RID: 8975 RVA: 0x00080DCC File Offset: 0x0007EFCC
		internal MonoMethod(RuntimeMethodHandle mhandle)
		{
			this.mhandle = mhandle.Value;
		}

		// Token: 0x06002310 RID: 8976
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string get_name(MethodBase method);

		// Token: 0x06002311 RID: 8977
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern MonoMethod get_base_method(MonoMethod method, bool definition);

		// Token: 0x06002312 RID: 8978 RVA: 0x00080DE1 File Offset: 0x0007EFE1
		public override MethodInfo GetBaseDefinition()
		{
			return MonoMethod.get_base_method(this, true);
		}

		// Token: 0x06002313 RID: 8979 RVA: 0x00080DEA File Offset: 0x0007EFEA
		internal override MethodInfo GetBaseMethod()
		{
			return MonoMethod.get_base_method(this, false);
		}

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x06002314 RID: 8980 RVA: 0x00080DF3 File Offset: 0x0007EFF3
		public override ParameterInfo ReturnParameter
		{
			get
			{
				return MonoMethodInfo.GetReturnParameterInfo(this);
			}
		}

		// Token: 0x17000541 RID: 1345
		// (get) Token: 0x06002315 RID: 8981 RVA: 0x00080DFB File Offset: 0x0007EFFB
		public override Type ReturnType
		{
			get
			{
				return MonoMethodInfo.GetReturnType(this.mhandle);
			}
		}

		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06002316 RID: 8982 RVA: 0x00080DF3 File Offset: 0x0007EFF3
		public override ICustomAttributeProvider ReturnTypeCustomAttributes
		{
			get
			{
				return MonoMethodInfo.GetReturnParameterInfo(this);
			}
		}

		// Token: 0x06002317 RID: 8983 RVA: 0x00080E08 File Offset: 0x0007F008
		public override MethodImplAttributes GetMethodImplementationFlags()
		{
			return MonoMethodInfo.GetMethodImplementationFlags(this.mhandle);
		}

		// Token: 0x06002318 RID: 8984 RVA: 0x00080E18 File Offset: 0x0007F018
		public override ParameterInfo[] GetParameters()
		{
			ParameterInfo[] parametersInfo = MonoMethodInfo.GetParametersInfo(this.mhandle, this);
			if (parametersInfo.Length == 0)
			{
				return parametersInfo;
			}
			ParameterInfo[] array = new ParameterInfo[parametersInfo.Length];
			Array.FastCopy(parametersInfo, 0, array, 0, parametersInfo.Length);
			return array;
		}

		// Token: 0x06002319 RID: 8985 RVA: 0x00080E4F File Offset: 0x0007F04F
		internal override ParameterInfo[] GetParametersInternal()
		{
			return MonoMethodInfo.GetParametersInfo(this.mhandle, this);
		}

		// Token: 0x0600231A RID: 8986 RVA: 0x00080E5D File Offset: 0x0007F05D
		internal override int GetParametersCount()
		{
			return MonoMethodInfo.GetParametersInfo(this.mhandle, this).Length;
		}

		// Token: 0x0600231B RID: 8987
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern object InternalInvoke(object obj, object[] parameters, out Exception exc);

		// Token: 0x0600231C RID: 8988 RVA: 0x00080E70 File Offset: 0x0007F070
		[DebuggerStepThrough]
		[DebuggerHidden]
		public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			if (binder == null)
			{
				binder = Type.DefaultBinder;
			}
			ParameterInfo[] parametersInternal = this.GetParametersInternal();
			MonoMethod.ConvertValues(binder, parameters, parametersInternal, culture, invokeAttr);
			if (this.ContainsGenericParameters)
			{
				throw new InvalidOperationException("Late bound operations cannot be performed on types or methods for which ContainsGenericParameters is true.");
			}
			object result = null;
			Exception ex;
			try
			{
				result = this.InternalInvoke(obj, parameters, out ex);
			}
			catch (ThreadAbortException)
			{
				throw;
			}
			catch (MethodAccessException)
			{
				throw;
			}
			catch (Exception inner)
			{
				throw new TargetInvocationException(inner);
			}
			if (ex != null)
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x0600231D RID: 8989 RVA: 0x00080EF8 File Offset: 0x0007F0F8
		internal static void ConvertValues(Binder binder, object[] args, ParameterInfo[] pinfo, CultureInfo culture, BindingFlags invokeAttr)
		{
			if (args == null)
			{
				if (pinfo.Length == 0)
				{
					return;
				}
				throw new TargetParameterCountException();
			}
			else
			{
				if (pinfo.Length != args.Length)
				{
					throw new TargetParameterCountException();
				}
				for (int i = 0; i < args.Length; i++)
				{
					object obj = args[i];
					ParameterInfo parameterInfo = pinfo[i];
					if (obj == Type.Missing)
					{
						if (parameterInfo.DefaultValue == DBNull.Value)
						{
							throw new ArgumentException(Environment.GetResourceString("Missing parameter does not have a default value."), "parameters");
						}
						args[i] = parameterInfo.DefaultValue;
					}
					else
					{
						RuntimeType runtimeType = (RuntimeType)parameterInfo.ParameterType;
						args[i] = runtimeType.CheckValue(obj, binder, culture, invokeAttr);
					}
				}
				return;
			}
		}

		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x0600231E RID: 8990 RVA: 0x00080F86 File Offset: 0x0007F186
		public override RuntimeMethodHandle MethodHandle
		{
			get
			{
				return new RuntimeMethodHandle(this.mhandle);
			}
		}

		// Token: 0x17000544 RID: 1348
		// (get) Token: 0x0600231F RID: 8991 RVA: 0x00080F93 File Offset: 0x0007F193
		public override MethodAttributes Attributes
		{
			get
			{
				return MonoMethodInfo.GetAttributes(this.mhandle);
			}
		}

		// Token: 0x17000545 RID: 1349
		// (get) Token: 0x06002320 RID: 8992 RVA: 0x00080FA0 File Offset: 0x0007F1A0
		public override CallingConventions CallingConvention
		{
			get
			{
				return MonoMethodInfo.GetCallingConvention(this.mhandle);
			}
		}

		// Token: 0x17000546 RID: 1350
		// (get) Token: 0x06002321 RID: 8993 RVA: 0x00080FAD File Offset: 0x0007F1AD
		public override Type ReflectedType
		{
			get
			{
				return this.reftype;
			}
		}

		// Token: 0x17000547 RID: 1351
		// (get) Token: 0x06002322 RID: 8994 RVA: 0x00080FB5 File Offset: 0x0007F1B5
		public override Type DeclaringType
		{
			get
			{
				return MonoMethodInfo.GetDeclaringType(this.mhandle);
			}
		}

		// Token: 0x17000548 RID: 1352
		// (get) Token: 0x06002323 RID: 8995 RVA: 0x00080FC2 File Offset: 0x0007F1C2
		public override string Name
		{
			get
			{
				if (this.name != null)
				{
					return this.name;
				}
				return MonoMethod.get_name(this);
			}
		}

		// Token: 0x06002324 RID: 8996 RVA: 0x00032B0D File Offset: 0x00030D0D
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x06002325 RID: 8997 RVA: 0x0007E188 File Offset: 0x0007C388
		public override object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x06002326 RID: 8998 RVA: 0x0007E191 File Offset: 0x0007C391
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x06002327 RID: 8999
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetPInvoke(out PInvokeAttributes flags, out string entryPoint, out string dllName);

		// Token: 0x06002328 RID: 9000 RVA: 0x00080FDC File Offset: 0x0007F1DC
		internal object[] GetPseudoCustomAttributes()
		{
			int num = 0;
			MonoMethodInfo methodInfo = MonoMethodInfo.GetMethodInfo(this.mhandle);
			if ((methodInfo.iattrs & MethodImplAttributes.PreserveSig) != MethodImplAttributes.IL)
			{
				num++;
			}
			if ((methodInfo.attrs & MethodAttributes.PinvokeImpl) != MethodAttributes.PrivateScope)
			{
				num++;
			}
			if (num == 0)
			{
				return null;
			}
			object[] array = new object[num];
			num = 0;
			if ((methodInfo.iattrs & MethodImplAttributes.PreserveSig) != MethodImplAttributes.IL)
			{
				array[num++] = new PreserveSigAttribute();
			}
			if ((methodInfo.attrs & MethodAttributes.PinvokeImpl) != MethodAttributes.PrivateScope)
			{
				array[num++] = DllImportAttribute.GetCustomAttribute(this);
			}
			return array;
		}

		// Token: 0x06002329 RID: 9001 RVA: 0x00081060 File Offset: 0x0007F260
		public override MethodInfo MakeGenericMethod(params Type[] methodInstantiation)
		{
			if (methodInstantiation == null)
			{
				throw new ArgumentNullException("methodInstantiation");
			}
			if (!this.IsGenericMethodDefinition)
			{
				throw new InvalidOperationException("not a generic method definition");
			}
			if (this.GetGenericArguments().Length != methodInstantiation.Length)
			{
				throw new ArgumentException("Incorrect length");
			}
			bool flag = false;
			foreach (Type type in methodInstantiation)
			{
				if (type == null)
				{
					throw new ArgumentNullException();
				}
				if (!(type is RuntimeType))
				{
					flag = true;
				}
			}
			if (flag)
			{
				throw new NotSupportedException("User types are not supported under full aot");
			}
			MethodInfo methodInfo = this.MakeGenericMethod_impl(methodInstantiation);
			if (methodInfo == null)
			{
				throw new ArgumentException(string.Format("The method has {0} generic parameter(s) but {1} generic argument(s) were provided.", this.GetGenericArguments().Length, methodInstantiation.Length));
			}
			return methodInfo;
		}

		// Token: 0x0600232A RID: 9002
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern MethodInfo MakeGenericMethod_impl(Type[] types);

		// Token: 0x0600232B RID: 9003
		[MethodImpl(MethodImplOptions.InternalCall)]
		public override extern Type[] GetGenericArguments();

		// Token: 0x0600232C RID: 9004
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern MethodInfo GetGenericMethodDefinition_impl();

		// Token: 0x0600232D RID: 9005 RVA: 0x00081116 File Offset: 0x0007F316
		public override MethodInfo GetGenericMethodDefinition()
		{
			MethodInfo genericMethodDefinition_impl = this.GetGenericMethodDefinition_impl();
			if (genericMethodDefinition_impl == null)
			{
				throw new InvalidOperationException();
			}
			return genericMethodDefinition_impl;
		}

		// Token: 0x17000549 RID: 1353
		// (get) Token: 0x0600232E RID: 9006
		public override extern bool IsGenericMethodDefinition { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700054A RID: 1354
		// (get) Token: 0x0600232F RID: 9007
		public override extern bool IsGenericMethod { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700054B RID: 1355
		// (get) Token: 0x06002330 RID: 9008 RVA: 0x00081130 File Offset: 0x0007F330
		public override bool ContainsGenericParameters
		{
			get
			{
				if (this.IsGenericMethod)
				{
					Type[] genericArguments = this.GetGenericArguments();
					for (int i = 0; i < genericArguments.Length; i++)
					{
						if (genericArguments[i].ContainsGenericParameters)
						{
							return true;
						}
					}
				}
				return this.DeclaringType.ContainsGenericParameters;
			}
		}

		// Token: 0x06002331 RID: 9009 RVA: 0x00081171 File Offset: 0x0007F371
		public override MethodBody GetMethodBody()
		{
			return MethodBase.GetMethodBody(this.mhandle);
		}

		// Token: 0x06002332 RID: 9010 RVA: 0x000808D8 File Offset: 0x0007EAD8
		public override IList<CustomAttributeData> GetCustomAttributesData()
		{
			return CustomAttributeData.GetCustomAttributes(this);
		}

		// Token: 0x06002333 RID: 9011 RVA: 0x00004E08 File Offset: 0x00003008
		private static int get_core_clr_security_level()
		{
			return 1;
		}

		// Token: 0x1700054C RID: 1356
		// (get) Token: 0x06002334 RID: 9012 RVA: 0x0008117E File Offset: 0x0007F37E
		public override bool IsSecurityTransparent
		{
			get
			{
				return MonoMethod.get_core_clr_security_level() == 0;
			}
		}

		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x06002335 RID: 9013 RVA: 0x00081188 File Offset: 0x0007F388
		public override bool IsSecurityCritical
		{
			get
			{
				return MonoMethod.get_core_clr_security_level() > 0;
			}
		}

		// Token: 0x1700054E RID: 1358
		// (get) Token: 0x06002336 RID: 9014 RVA: 0x00081192 File Offset: 0x0007F392
		public override bool IsSecuritySafeCritical
		{
			get
			{
				return MonoMethod.get_core_clr_security_level() == 1;
			}
		}

		// Token: 0x0400130D RID: 4877
		internal IntPtr mhandle;

		// Token: 0x0400130E RID: 4878
		private string name;

		// Token: 0x0400130F RID: 4879
		private Type reftype;
	}
}
