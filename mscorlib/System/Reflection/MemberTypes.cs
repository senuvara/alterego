﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Marks each type of member that is defined as a derived class of <see cref="T:System.Reflection.MemberInfo" />.</summary>
	// Token: 0x020002E3 RID: 739
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum MemberTypes
	{
		/// <summary>Specifies that the member is a constructor</summary>
		// Token: 0x0400120C RID: 4620
		Constructor = 1,
		/// <summary>Specifies that the member is an event.</summary>
		// Token: 0x0400120D RID: 4621
		Event = 2,
		/// <summary>Specifies that the member is a field.</summary>
		// Token: 0x0400120E RID: 4622
		Field = 4,
		/// <summary>Specifies that the member is a method.</summary>
		// Token: 0x0400120F RID: 4623
		Method = 8,
		/// <summary>Specifies that the member is a property.</summary>
		// Token: 0x04001210 RID: 4624
		Property = 16,
		/// <summary>Specifies that the member is a type.</summary>
		// Token: 0x04001211 RID: 4625
		TypeInfo = 32,
		/// <summary>Specifies that the member is a custom member type. </summary>
		// Token: 0x04001212 RID: 4626
		Custom = 64,
		/// <summary>Specifies that the member is a nested type.</summary>
		// Token: 0x04001213 RID: 4627
		NestedType = 128,
		/// <summary>Specifies all member types.</summary>
		// Token: 0x04001214 RID: 4628
		All = 191
	}
}
