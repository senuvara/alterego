﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Attaches a modifier to parameters so that binding can work with parameter signatures in which the types have been modified.</summary>
	// Token: 0x020002EE RID: 750
	[ComVisible(true)]
	[Serializable]
	public struct ParameterModifier
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.ParameterModifier" /> structure representing the specified number of parameters.</summary>
		/// <param name="parameterCount">The number of parameters. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="parameterCount" /> is negative. </exception>
		// Token: 0x0600208E RID: 8334 RVA: 0x0007D703 File Offset: 0x0007B903
		public ParameterModifier(int parameterCount)
		{
			if (parameterCount <= 0)
			{
				throw new ArgumentException(Environment.GetResourceString("Must specify one or more parameters."));
			}
			this._byRef = new bool[parameterCount];
		}

		// Token: 0x170004AA RID: 1194
		// (get) Token: 0x0600208F RID: 8335 RVA: 0x0007D725 File Offset: 0x0007B925
		internal bool[] IsByRefArray
		{
			get
			{
				return this._byRef;
			}
		}

		/// <summary>Gets or sets a value that specifies whether the parameter at the specified index position is to be modified by the current <see cref="T:System.Reflection.ParameterModifier" />.</summary>
		/// <param name="index">The index position of the parameter whose modification status is being examined or set. </param>
		/// <returns>
		///     <see langword="true" /> if the parameter at this index position is to be modified by this <see cref="T:System.Reflection.ParameterModifier" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x170004AB RID: 1195
		public bool this[int index]
		{
			get
			{
				return this._byRef[index];
			}
			set
			{
				this._byRef[index] = value;
			}
		}

		// Token: 0x04001265 RID: 4709
		private bool[] _byRef;
	}
}
