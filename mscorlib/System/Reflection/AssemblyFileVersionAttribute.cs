﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Instructs a compiler to use a specific version number for the Win32 file version resource. The Win32 file version is not required to be the same as the assembly's version number.</summary>
	// Token: 0x020002BE RID: 702
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyFileVersionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyFileVersionAttribute" /> class, specifying the file version.</summary>
		/// <param name="version">The file version. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="version" /> is <see langword="null" />. </exception>
		// Token: 0x06001FC4 RID: 8132 RVA: 0x0007C879 File Offset: 0x0007AA79
		public AssemblyFileVersionAttribute(string version)
		{
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			this._version = version;
		}

		/// <summary>Gets the Win32 file version resource name.</summary>
		/// <returns>A string containing the file version resource name.</returns>
		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x06001FC5 RID: 8133 RVA: 0x0007C896 File Offset: 0x0007AA96
		public string Version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x04001132 RID: 4402
		private string _version;
	}
}
