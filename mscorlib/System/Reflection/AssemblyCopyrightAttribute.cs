﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines a copyright custom attribute for an assembly manifest.</summary>
	// Token: 0x020002B5 RID: 693
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyCopyrightAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyCopyrightAttribute" /> class.</summary>
		/// <param name="copyright">The copyright information. </param>
		// Token: 0x06001FB2 RID: 8114 RVA: 0x0007C7AA File Offset: 0x0007A9AA
		public AssemblyCopyrightAttribute(string copyright)
		{
			this.m_copyright = copyright;
		}

		/// <summary>Gets copyright information.</summary>
		/// <returns>A string containing the copyright information.</returns>
		// Token: 0x17000459 RID: 1113
		// (get) Token: 0x06001FB3 RID: 8115 RVA: 0x0007C7B9 File Offset: 0x0007A9B9
		public string Copyright
		{
			get
			{
				return this.m_copyright;
			}
		}

		// Token: 0x04001129 RID: 4393
		private string m_copyright;
	}
}
