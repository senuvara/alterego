﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines a trademark custom attribute for an assembly manifest.</summary>
	// Token: 0x020002B6 RID: 694
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyTrademarkAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyTrademarkAttribute" /> class.</summary>
		/// <param name="trademark">The trademark information. </param>
		// Token: 0x06001FB4 RID: 8116 RVA: 0x0007C7C1 File Offset: 0x0007A9C1
		public AssemblyTrademarkAttribute(string trademark)
		{
			this.m_trademark = trademark;
		}

		/// <summary>Gets trademark information.</summary>
		/// <returns>A <see langword="String" /> containing trademark information.</returns>
		// Token: 0x1700045A RID: 1114
		// (get) Token: 0x06001FB5 RID: 8117 RVA: 0x0007C7D0 File Offset: 0x0007A9D0
		public string Trademark
		{
			get
			{
				return this.m_trademark;
			}
		}

		// Token: 0x0400112A RID: 4394
		private string m_trademark;
	}
}
