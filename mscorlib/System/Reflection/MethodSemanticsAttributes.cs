﻿using System;

namespace System.Reflection
{
	// Token: 0x020002DE RID: 734
	[Flags]
	[Serializable]
	internal enum MethodSemanticsAttributes
	{
		// Token: 0x040011E4 RID: 4580
		Setter = 1,
		// Token: 0x040011E5 RID: 4581
		Getter = 2,
		// Token: 0x040011E6 RID: 4582
		Other = 4,
		// Token: 0x040011E7 RID: 4583
		AddOn = 8,
		// Token: 0x040011E8 RID: 4584
		RemoveOn = 16,
		// Token: 0x040011E9 RID: 4585
		Fire = 32
	}
}
