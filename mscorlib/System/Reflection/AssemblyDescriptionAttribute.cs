﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Provides a text description for an assembly.</summary>
	// Token: 0x020002B9 RID: 697
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyDescriptionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyDescriptionAttribute" /> class.</summary>
		/// <param name="description">The assembly description. </param>
		// Token: 0x06001FBA RID: 8122 RVA: 0x0007C806 File Offset: 0x0007AA06
		public AssemblyDescriptionAttribute(string description)
		{
			this.m_description = description;
		}

		/// <summary>Gets assembly description information.</summary>
		/// <returns>A string containing the assembly description.</returns>
		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x06001FBB RID: 8123 RVA: 0x0007C815 File Offset: 0x0007AA15
		public string Description
		{
			get
			{
				return this.m_description;
			}
		}

		// Token: 0x0400112D RID: 4397
		private string m_description;
	}
}
