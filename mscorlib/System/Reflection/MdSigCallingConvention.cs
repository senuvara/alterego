﻿using System;

namespace System.Reflection
{
	// Token: 0x020002DC RID: 732
	[Flags]
	[Serializable]
	internal enum MdSigCallingConvention : byte
	{
		// Token: 0x040011BD RID: 4541
		CallConvMask = 15,
		// Token: 0x040011BE RID: 4542
		Default = 0,
		// Token: 0x040011BF RID: 4543
		C = 1,
		// Token: 0x040011C0 RID: 4544
		StdCall = 2,
		// Token: 0x040011C1 RID: 4545
		ThisCall = 3,
		// Token: 0x040011C2 RID: 4546
		FastCall = 4,
		// Token: 0x040011C3 RID: 4547
		Vararg = 5,
		// Token: 0x040011C4 RID: 4548
		Field = 6,
		// Token: 0x040011C5 RID: 4549
		LocalSig = 7,
		// Token: 0x040011C6 RID: 4550
		Property = 8,
		// Token: 0x040011C7 RID: 4551
		Unmgd = 9,
		// Token: 0x040011C8 RID: 4552
		GenericInst = 10,
		// Token: 0x040011C9 RID: 4553
		Generic = 16,
		// Token: 0x040011CA RID: 4554
		HasThis = 32,
		// Token: 0x040011CB RID: 4555
		ExplicitThis = 64
	}
}
