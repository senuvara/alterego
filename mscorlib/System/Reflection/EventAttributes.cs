﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies the attributes of an event.</summary>
	// Token: 0x020002D0 RID: 720
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum EventAttributes
	{
		/// <summary>Specifies that the event has no attributes.</summary>
		// Token: 0x0400116B RID: 4459
		None = 0,
		/// <summary>Specifies that the event is special in a way described by the name.</summary>
		// Token: 0x0400116C RID: 4460
		SpecialName = 512,
		/// <summary>Specifies a reserved flag for common language runtime use only.</summary>
		// Token: 0x0400116D RID: 4461
		ReservedMask = 1024,
		/// <summary>Specifies that the common language runtime should check name encoding.</summary>
		// Token: 0x0400116E RID: 4462
		RTSpecialName = 1024
	}
}
