﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Identifies the processor and bits-per-word of the platform targeted by an executable.</summary>
	// Token: 0x020002CA RID: 714
	[ComVisible(true)]
	[Serializable]
	public enum ProcessorArchitecture
	{
		/// <summary>An unknown or unspecified combination of processor and bits-per-word.</summary>
		// Token: 0x04001148 RID: 4424
		None,
		/// <summary>Neutral with respect to processor and bits-per-word.</summary>
		// Token: 0x04001149 RID: 4425
		MSIL,
		/// <summary>A 32-bit Intel processor, either native or in the Windows on Windows environment on a 64-bit platform (WOW64).</summary>
		// Token: 0x0400114A RID: 4426
		X86,
		/// <summary>A 64-bit Intel processor only.</summary>
		// Token: 0x0400114B RID: 4427
		IA64,
		/// <summary>A 64-bit AMD processor only.</summary>
		// Token: 0x0400114C RID: 4428
		Amd64,
		/// <summary>An ARM processor.</summary>
		// Token: 0x0400114D RID: 4429
		Arm
	}
}
