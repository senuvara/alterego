﻿using System;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x0200031D RID: 797
	internal abstract class RuntimeConstructorInfo : ConstructorInfo, ISerializable
	{
		// Token: 0x1700054F RID: 1359
		// (get) Token: 0x06002337 RID: 9015 RVA: 0x0008119C File Offset: 0x0007F39C
		public override Module Module
		{
			get
			{
				return this.GetRuntimeModule();
			}
		}

		// Token: 0x06002338 RID: 9016 RVA: 0x000811A4 File Offset: 0x0007F3A4
		internal RuntimeModule GetRuntimeModule()
		{
			return RuntimeTypeHandle.GetModule((RuntimeType)this.DeclaringType);
		}

		// Token: 0x17000550 RID: 1360
		// (get) Token: 0x06002339 RID: 9017 RVA: 0x00002526 File Offset: 0x00000726
		internal BindingFlags BindingFlags
		{
			get
			{
				return BindingFlags.Default;
			}
		}

		// Token: 0x17000551 RID: 1361
		// (get) Token: 0x0600233A RID: 9018 RVA: 0x000806FB File Offset: 0x0007E8FB
		private RuntimeType ReflectedTypeInternal
		{
			get
			{
				return (RuntimeType)this.ReflectedType;
			}
		}

		// Token: 0x0600233B RID: 9019 RVA: 0x000811B6 File Offset: 0x0007F3B6
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			MemberInfoSerializationHolder.GetSerializationInfo(info, this.Name, this.ReflectedTypeInternal, this.ToString(), this.SerializationToString(), MemberTypes.Constructor, null);
		}

		// Token: 0x0600233C RID: 9020 RVA: 0x000811E6 File Offset: 0x0007F3E6
		internal string SerializationToString()
		{
			return this.FormatNameAndSig(true);
		}

		// Token: 0x0600233D RID: 9021 RVA: 0x000811EF File Offset: 0x0007F3EF
		internal void SerializationInvoke(object target, SerializationInfo info, StreamingContext context)
		{
			base.Invoke(target, new object[]
			{
				info,
				context
			});
		}

		// Token: 0x0600233E RID: 9022 RVA: 0x0008120C File Offset: 0x0007F40C
		protected RuntimeConstructorInfo()
		{
		}
	}
}
