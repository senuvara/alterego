﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Instructs obfuscation tools to take the specified actions for an assembly, type, or member.</summary>
	// Token: 0x020002EC RID: 748
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Parameter | AttributeTargets.Delegate, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	public sealed class ObfuscationAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.ObfuscationAttribute" /> class.</summary>
		// Token: 0x06002085 RID: 8325 RVA: 0x0007D697 File Offset: 0x0007B897
		public ObfuscationAttribute()
		{
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value indicating whether the obfuscation tool should remove this attribute after processing.</summary>
		/// <returns>
		///     <see langword="true" /> if an obfuscation tool should remove the attribute after processing; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x170004A6 RID: 1190
		// (get) Token: 0x06002086 RID: 8326 RVA: 0x0007D6BF File Offset: 0x0007B8BF
		// (set) Token: 0x06002087 RID: 8327 RVA: 0x0007D6C7 File Offset: 0x0007B8C7
		public bool StripAfterObfuscation
		{
			get
			{
				return this.m_strip;
			}
			set
			{
				this.m_strip = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value indicating whether the obfuscation tool should exclude the type or member from obfuscation.</summary>
		/// <returns>
		///     <see langword="true" /> if the type or member to which this attribute is applied should be excluded from obfuscation; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x170004A7 RID: 1191
		// (get) Token: 0x06002088 RID: 8328 RVA: 0x0007D6D0 File Offset: 0x0007B8D0
		// (set) Token: 0x06002089 RID: 8329 RVA: 0x0007D6D8 File Offset: 0x0007B8D8
		public bool Exclude
		{
			get
			{
				return this.m_exclude;
			}
			set
			{
				this.m_exclude = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value indicating whether the attribute of a type is to apply to the members of the type.</summary>
		/// <returns>
		///     <see langword="true" /> if the attribute is to apply to the members of the type; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x170004A8 RID: 1192
		// (get) Token: 0x0600208A RID: 8330 RVA: 0x0007D6E1 File Offset: 0x0007B8E1
		// (set) Token: 0x0600208B RID: 8331 RVA: 0x0007D6E9 File Offset: 0x0007B8E9
		public bool ApplyToMembers
		{
			get
			{
				return this.m_applyToMembers;
			}
			set
			{
				this.m_applyToMembers = value;
			}
		}

		/// <summary>Gets or sets a string value that is recognized by the obfuscation tool, and which specifies processing options. </summary>
		/// <returns>A string value that is recognized by the obfuscation tool, and which specifies processing options. The default is "all".</returns>
		// Token: 0x170004A9 RID: 1193
		// (get) Token: 0x0600208C RID: 8332 RVA: 0x0007D6F2 File Offset: 0x0007B8F2
		// (set) Token: 0x0600208D RID: 8333 RVA: 0x0007D6FA File Offset: 0x0007B8FA
		public string Feature
		{
			get
			{
				return this.m_feature;
			}
			set
			{
				this.m_feature = value;
			}
		}

		// Token: 0x04001255 RID: 4693
		private bool m_strip = true;

		// Token: 0x04001256 RID: 4694
		private bool m_exclude = true;

		// Token: 0x04001257 RID: 4695
		private bool m_applyToMembers = true;

		// Token: 0x04001258 RID: 4696
		private string m_feature = "all";
	}
}
