﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x02000320 RID: 800
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_Module))]
	[ClassInterface(ClassInterfaceType.None)]
	[Serializable]
	internal class MonoModule : RuntimeModule
	{
		// Token: 0x1700055C RID: 1372
		// (get) Token: 0x0600235B RID: 9051 RVA: 0x000814AE File Offset: 0x0007F6AE
		public override Assembly Assembly
		{
			get
			{
				return this.assembly;
			}
		}

		// Token: 0x1700055D RID: 1373
		// (get) Token: 0x0600235C RID: 9052 RVA: 0x000802CC File Offset: 0x0007E4CC
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x0600235D RID: 9053 RVA: 0x000814B6 File Offset: 0x0007F6B6
		public override string ScopeName
		{
			get
			{
				return this.scopename;
			}
		}

		// Token: 0x1700055F RID: 1375
		// (get) Token: 0x0600235E RID: 9054 RVA: 0x000814BE File Offset: 0x0007F6BE
		public override int MDStreamVersion
		{
			get
			{
				if (this._impl == IntPtr.Zero)
				{
					throw new NotSupportedException();
				}
				return Module.GetMDStreamVersion(this._impl);
			}
		}

		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x0600235F RID: 9055 RVA: 0x000802D4 File Offset: 0x0007E4D4
		public override Guid ModuleVersionId
		{
			get
			{
				return this.GetModuleVersionId();
			}
		}

		// Token: 0x17000561 RID: 1377
		// (get) Token: 0x06002360 RID: 9056 RVA: 0x000814E3 File Offset: 0x0007F6E3
		public override string FullyQualifiedName
		{
			get
			{
				return this.fqname;
			}
		}

		// Token: 0x06002361 RID: 9057 RVA: 0x000814EB File Offset: 0x0007F6EB
		public override bool IsResource()
		{
			return this.is_resource;
		}

		// Token: 0x06002362 RID: 9058 RVA: 0x000814F4 File Offset: 0x0007F6F4
		public override Type[] FindTypes(TypeFilter filter, object filterCriteria)
		{
			List<Type> list = new List<Type>();
			foreach (Type type in this.GetTypes())
			{
				if (filter(type, filterCriteria))
				{
					list.Add(type);
				}
			}
			return list.ToArray();
		}

		// Token: 0x06002363 RID: 9059 RVA: 0x0007E188 File Offset: 0x0007C388
		public override object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x06002364 RID: 9060 RVA: 0x0007E191 File Offset: 0x0007C391
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x06002365 RID: 9061 RVA: 0x00081538 File Offset: 0x0007F738
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.IsResource())
			{
				return null;
			}
			Type globalType = base.GetGlobalType();
			if (!(globalType != null))
			{
				return null;
			}
			return globalType.GetField(name, bindingAttr);
		}

		// Token: 0x06002366 RID: 9062 RVA: 0x00081578 File Offset: 0x0007F778
		public override FieldInfo[] GetFields(BindingFlags bindingFlags)
		{
			if (this.IsResource())
			{
				return new FieldInfo[0];
			}
			Type globalType = base.GetGlobalType();
			if (!(globalType != null))
			{
				return new FieldInfo[0];
			}
			return globalType.GetFields(bindingFlags);
		}

		// Token: 0x17000562 RID: 1378
		// (get) Token: 0x06002367 RID: 9063 RVA: 0x000815B2 File Offset: 0x0007F7B2
		public override int MetadataToken
		{
			get
			{
				return Module.get_MetadataToken(this);
			}
		}

		// Token: 0x06002368 RID: 9064 RVA: 0x000815BC File Offset: 0x0007F7BC
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			if (this.IsResource())
			{
				return null;
			}
			Type globalType = base.GetGlobalType();
			if (globalType == null)
			{
				return null;
			}
			if (types == null)
			{
				return globalType.GetMethod(name);
			}
			return globalType.GetMethod(name, bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x06002369 RID: 9065 RVA: 0x00081600 File Offset: 0x0007F800
		public override MethodInfo[] GetMethods(BindingFlags bindingFlags)
		{
			if (this.IsResource())
			{
				return new MethodInfo[0];
			}
			Type globalType = base.GetGlobalType();
			if (!(globalType != null))
			{
				return new MethodInfo[0];
			}
			return globalType.GetMethods(bindingFlags);
		}

		// Token: 0x0600236A RID: 9066 RVA: 0x0008163C File Offset: 0x0007F83C
		public override void GetPEKind(out PortableExecutableKinds peKind, out ImageFileMachine machine)
		{
			base.ModuleHandle.GetPEKind(out peKind, out machine);
		}

		// Token: 0x0600236B RID: 9067 RVA: 0x00081659 File Offset: 0x0007F859
		public override Type GetType(string className, bool throwOnError, bool ignoreCase)
		{
			if (className == null)
			{
				throw new ArgumentNullException("className");
			}
			if (className == string.Empty)
			{
				throw new ArgumentException("Type name can't be empty");
			}
			return this.assembly.InternalGetType(this, className, throwOnError, ignoreCase);
		}

		// Token: 0x0600236C RID: 9068 RVA: 0x00032B0D File Offset: 0x00030D0D
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x0600236D RID: 9069 RVA: 0x00081690 File Offset: 0x0007F890
		public override FieldInfo ResolveField(int metadataToken, Type[] genericTypeArguments, Type[] genericMethodArguments)
		{
			ResolveTokenError error;
			IntPtr intPtr = Module.ResolveFieldToken(this._impl, metadataToken, base.ptrs_from_types(genericTypeArguments), base.ptrs_from_types(genericMethodArguments), out error);
			if (intPtr == IntPtr.Zero)
			{
				throw base.resolve_token_exception(metadataToken, error, "Field");
			}
			return FieldInfo.GetFieldFromHandle(new RuntimeFieldHandle(intPtr));
		}

		// Token: 0x0600236E RID: 9070 RVA: 0x000816E0 File Offset: 0x0007F8E0
		public override MemberInfo ResolveMember(int metadataToken, Type[] genericTypeArguments, Type[] genericMethodArguments)
		{
			ResolveTokenError error;
			MemberInfo memberInfo = Module.ResolveMemberToken(this._impl, metadataToken, base.ptrs_from_types(genericTypeArguments), base.ptrs_from_types(genericMethodArguments), out error);
			if (memberInfo == null)
			{
				throw base.resolve_token_exception(metadataToken, error, "MemberInfo");
			}
			return memberInfo;
		}

		// Token: 0x0600236F RID: 9071 RVA: 0x00081724 File Offset: 0x0007F924
		public override MethodBase ResolveMethod(int metadataToken, Type[] genericTypeArguments, Type[] genericMethodArguments)
		{
			ResolveTokenError error;
			IntPtr intPtr = Module.ResolveMethodToken(this._impl, metadataToken, base.ptrs_from_types(genericTypeArguments), base.ptrs_from_types(genericMethodArguments), out error);
			if (intPtr == IntPtr.Zero)
			{
				throw base.resolve_token_exception(metadataToken, error, "MethodBase");
			}
			return MethodBase.GetMethodFromHandleNoGenericCheck(new RuntimeMethodHandle(intPtr));
		}

		// Token: 0x06002370 RID: 9072 RVA: 0x00081774 File Offset: 0x0007F974
		public override string ResolveString(int metadataToken)
		{
			ResolveTokenError error;
			string text = Module.ResolveStringToken(this._impl, metadataToken, out error);
			if (text == null)
			{
				throw base.resolve_token_exception(metadataToken, error, "string");
			}
			return text;
		}

		// Token: 0x06002371 RID: 9073 RVA: 0x000817A4 File Offset: 0x0007F9A4
		public override Type ResolveType(int metadataToken, Type[] genericTypeArguments, Type[] genericMethodArguments)
		{
			ResolveTokenError error;
			IntPtr intPtr = Module.ResolveTypeToken(this._impl, metadataToken, base.ptrs_from_types(genericTypeArguments), base.ptrs_from_types(genericMethodArguments), out error);
			if (intPtr == IntPtr.Zero)
			{
				throw base.resolve_token_exception(metadataToken, error, "Type");
			}
			return Type.GetTypeFromHandle(new RuntimeTypeHandle(intPtr));
		}

		// Token: 0x06002372 RID: 9074 RVA: 0x000817F4 File Offset: 0x0007F9F4
		public override byte[] ResolveSignature(int metadataToken)
		{
			ResolveTokenError error;
			byte[] array = Module.ResolveSignature(this._impl, metadataToken, out error);
			if (array == null)
			{
				throw base.resolve_token_exception(metadataToken, error, "signature");
			}
			return array;
		}

		// Token: 0x06002373 RID: 9075 RVA: 0x00081822 File Offset: 0x0007FA22
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			UnitySerializationHolder.GetUnitySerializationInfo(info, 5, this.ScopeName, this.GetRuntimeAssembly());
		}

		// Token: 0x06002374 RID: 9076 RVA: 0x00081845 File Offset: 0x0007FA45
		public override Type[] GetTypes()
		{
			return base.InternalGetTypes();
		}

		// Token: 0x06002375 RID: 9077 RVA: 0x0008184D File Offset: 0x0007FA4D
		public override IList<CustomAttributeData> GetCustomAttributesData()
		{
			return CustomAttributeData.GetCustomAttributes(this);
		}

		// Token: 0x06002376 RID: 9078 RVA: 0x00081855 File Offset: 0x0007FA55
		internal RuntimeAssembly GetRuntimeAssembly()
		{
			return (RuntimeAssembly)this.assembly;
		}

		// Token: 0x06002377 RID: 9079 RVA: 0x00081862 File Offset: 0x0007FA62
		public MonoModule()
		{
		}
	}
}
