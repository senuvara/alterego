﻿using System;

namespace System.Reflection
{
	/// <summary>Provides migration from an older, simpler strong name key to a larger key with a stronger hashing algorithm.</summary>
	// Token: 0x020002C6 RID: 710
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
	public sealed class AssemblySignatureKeyAttribute : Attribute
	{
		/// <summary>Creates a new instance of the <see cref="T:System.Reflection.AssemblySignatureKeyAttribute" /> class by using the specified public key and countersignature.</summary>
		/// <param name="publicKey">The public or identity key.</param>
		/// <param name="countersignature">The countersignature, which is the signature key portion of the strong-name key.</param>
		// Token: 0x06001FD9 RID: 8153 RVA: 0x0007C94E File Offset: 0x0007AB4E
		public AssemblySignatureKeyAttribute(string publicKey, string countersignature)
		{
			this._publicKey = publicKey;
			this._countersignature = countersignature;
		}

		/// <summary>Gets the public key for the strong name used to sign the assembly.</summary>
		/// <returns>The public key for this assembly.</returns>
		// Token: 0x1700046C RID: 1132
		// (get) Token: 0x06001FDA RID: 8154 RVA: 0x0007C964 File Offset: 0x0007AB64
		public string PublicKey
		{
			get
			{
				return this._publicKey;
			}
		}

		/// <summary>Gets the countersignature for the strong name for this assembly.</summary>
		/// <returns>The countersignature for this signature key.</returns>
		// Token: 0x1700046D RID: 1133
		// (get) Token: 0x06001FDB RID: 8155 RVA: 0x0007C96C File Offset: 0x0007AB6C
		public string Countersignature
		{
			get
			{
				return this._countersignature;
			}
		}

		// Token: 0x0400113B RID: 4411
		private string _publicKey;

		// Token: 0x0400113C RID: 4412
		private string _countersignature;
	}
}
