﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Provides information about the type of code contained in an assembly.</summary>
	// Token: 0x020002C9 RID: 713
	[ComVisible(false)]
	[Serializable]
	public enum AssemblyContentType
	{
		/// <summary>The assembly contains .NET Framework code.</summary>
		// Token: 0x04001145 RID: 4421
		Default,
		/// <summary>The assembly contains Windows Runtime code.</summary>
		// Token: 0x04001146 RID: 4422
		WindowsRuntime
	}
}
