﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Represents type declarations for class types, interface types, array types, value types, enumeration types, type parameters, generic type definitions, and open or closed constructed generic types. </summary>
	// Token: 0x020002FA RID: 762
	[ComVisible(true)]
	[Serializable]
	public abstract class TypeInfo : Type, IReflectableType
	{
		// Token: 0x060020E5 RID: 8421 RVA: 0x0007DDA5 File Offset: 0x0007BFA5
		[FriendAccessAllowed]
		internal TypeInfo()
		{
		}

		/// <summary>Returns a representation of the current type as a <see cref="T:System.Reflection.TypeInfo" /> object.</summary>
		/// <returns>A reference to the current type.</returns>
		// Token: 0x060020E6 RID: 8422 RVA: 0x00002058 File Offset: 0x00000258
		TypeInfo IReflectableType.GetTypeInfo()
		{
			return this;
		}

		/// <summary>Returns the current type as a <see cref="T:System.Type" /> object.</summary>
		/// <returns>The current type.</returns>
		// Token: 0x060020E7 RID: 8423 RVA: 0x00002058 File Offset: 0x00000258
		public virtual Type AsType()
		{
			return this;
		}

		/// <summary>Gets an array of the generic type parameters of the current instance. </summary>
		/// <returns>An array that contains the current instance's generic type parameters, or an array of <see cref="P:System.Array.Length" /> zero if the current instance has no generic type parameters. </returns>
		// Token: 0x170004BB RID: 1211
		// (get) Token: 0x060020E8 RID: 8424 RVA: 0x0007DDAD File Offset: 0x0007BFAD
		public virtual Type[] GenericTypeParameters
		{
			get
			{
				if (this.IsGenericTypeDefinition)
				{
					return this.GetGenericArguments();
				}
				return Type.EmptyTypes;
			}
		}

		/// <summary>Returns a value that indicates whether the specified type can be assigned to the current type.</summary>
		/// <param name="typeInfo">The type to check.</param>
		/// <returns>
		///     <see langword="true" /> if the specified type can be assigned to this type; otherwise, <see langword="false" />.</returns>
		// Token: 0x060020E9 RID: 8425 RVA: 0x0007DDC4 File Offset: 0x0007BFC4
		public virtual bool IsAssignableFrom(TypeInfo typeInfo)
		{
			if (typeInfo == null)
			{
				return false;
			}
			if (this == typeInfo)
			{
				return true;
			}
			if (typeInfo.IsSubclassOf(this))
			{
				return true;
			}
			if (base.IsInterface)
			{
				return typeInfo.ImplementInterface(this);
			}
			if (this.IsGenericParameter)
			{
				Type[] genericParameterConstraints = this.GetGenericParameterConstraints();
				for (int i = 0; i < genericParameterConstraints.Length; i++)
				{
					if (!genericParameterConstraints[i].IsAssignableFrom(typeInfo))
					{
						return false;
					}
				}
				return true;
			}
			return false;
		}

		/// <summary>Returns an object that represents the specified public event declared by the current type.</summary>
		/// <param name="name">The name of the event.</param>
		/// <returns>An object that represents the specified event, if found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060020EA RID: 8426 RVA: 0x0007DE2F File Offset: 0x0007C02F
		public virtual EventInfo GetDeclaredEvent(string name)
		{
			return this.GetEvent(name, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>Returns an object that represents the specified public field declared by the current type.</summary>
		/// <param name="name">The name of the field.</param>
		/// <returns>An object that represents the specified field, if found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060020EB RID: 8427 RVA: 0x0007DE3A File Offset: 0x0007C03A
		public virtual FieldInfo GetDeclaredField(string name)
		{
			return this.GetField(name, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>Returns an object that represents the specified public method declared by the current type.</summary>
		/// <param name="name">The name of the method.</param>
		/// <returns>An object that represents the specified method, if found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060020EC RID: 8428 RVA: 0x0007DE45 File Offset: 0x0007C045
		public virtual MethodInfo GetDeclaredMethod(string name)
		{
			return base.GetMethod(name, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>Returns a collection that contains all public methods declared on the current type that match the specified name.</summary>
		/// <param name="name">The method name to search for.</param>
		/// <returns>A collection that contains methods that match <paramref name="name" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060020ED RID: 8429 RVA: 0x0007DE50 File Offset: 0x0007C050
		public virtual IEnumerable<MethodInfo> GetDeclaredMethods(string name)
		{
			foreach (MethodInfo methodInfo in this.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
			{
				if (methodInfo.Name == name)
				{
					yield return methodInfo;
				}
			}
			MethodInfo[] array = null;
			yield break;
		}

		/// <summary>Returns an object that represents the specified public nested type declared by the current type.</summary>
		/// <param name="name">The name of the nested type.</param>
		/// <returns>An object that represents the specified nested type, if found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060020EE RID: 8430 RVA: 0x0007DE68 File Offset: 0x0007C068
		public virtual TypeInfo GetDeclaredNestedType(string name)
		{
			Type nestedType = this.GetNestedType(name, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			if (nestedType == null)
			{
				return null;
			}
			return nestedType.GetTypeInfo();
		}

		/// <summary>Returns an object that represents the specified public property declared by the current type.</summary>
		/// <param name="name">The name of the property.</param>
		/// <returns>An object that represents the specified property, if found; otherwise, <see langword="null" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="name" /> is <see langword="null" />. </exception>
		// Token: 0x060020EF RID: 8431 RVA: 0x0007DE90 File Offset: 0x0007C090
		public virtual PropertyInfo GetDeclaredProperty(string name)
		{
			return base.GetProperty(name, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		/// <summary>Gets a collection of the constructors declared by the current type.</summary>
		/// <returns>A collection of the constructors declared by the current type.</returns>
		// Token: 0x170004BC RID: 1212
		// (get) Token: 0x060020F0 RID: 8432 RVA: 0x0007DE9B File Offset: 0x0007C09B
		public virtual IEnumerable<ConstructorInfo> DeclaredConstructors
		{
			get
			{
				return this.GetConstructors(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			}
		}

		/// <summary>Gets a collection of the events defined by the current type.</summary>
		/// <returns>A collection of the events defined by the current type.</returns>
		// Token: 0x170004BD RID: 1213
		// (get) Token: 0x060020F1 RID: 8433 RVA: 0x0007DEA5 File Offset: 0x0007C0A5
		public virtual IEnumerable<EventInfo> DeclaredEvents
		{
			get
			{
				return this.GetEvents(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			}
		}

		/// <summary>Gets a collection of the fields defined by the current type.</summary>
		/// <returns>A collection of the fields defined by the current type.</returns>
		// Token: 0x170004BE RID: 1214
		// (get) Token: 0x060020F2 RID: 8434 RVA: 0x0007DEAF File Offset: 0x0007C0AF
		public virtual IEnumerable<FieldInfo> DeclaredFields
		{
			get
			{
				return this.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			}
		}

		/// <summary>Gets a collection of the members defined by the current type.</summary>
		/// <returns>A collection of the members defined by the current type.</returns>
		// Token: 0x170004BF RID: 1215
		// (get) Token: 0x060020F3 RID: 8435 RVA: 0x0007DEB9 File Offset: 0x0007C0B9
		public virtual IEnumerable<MemberInfo> DeclaredMembers
		{
			get
			{
				return this.GetMembers(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			}
		}

		/// <summary>Gets a collection of the methods defined by the current type.</summary>
		/// <returns>A collection of the methods defined by the current type.</returns>
		// Token: 0x170004C0 RID: 1216
		// (get) Token: 0x060020F4 RID: 8436 RVA: 0x0007DEC3 File Offset: 0x0007C0C3
		public virtual IEnumerable<MethodInfo> DeclaredMethods
		{
			get
			{
				return this.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			}
		}

		/// <summary>Gets a collection of the nested types defined by the current type.</summary>
		/// <returns>A collection of nested types defined by the current type.</returns>
		// Token: 0x170004C1 RID: 1217
		// (get) Token: 0x060020F5 RID: 8437 RVA: 0x0007DECD File Offset: 0x0007C0CD
		public virtual IEnumerable<TypeInfo> DeclaredNestedTypes
		{
			get
			{
				foreach (Type type in this.GetNestedTypes(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
				{
					yield return type.GetTypeInfo();
				}
				Type[] array = null;
				yield break;
			}
		}

		/// <summary>Gets a collection of the properties defined by the current type. </summary>
		/// <returns>A collection of the properties defined by the current type.</returns>
		// Token: 0x170004C2 RID: 1218
		// (get) Token: 0x060020F6 RID: 8438 RVA: 0x0007DEDD File Offset: 0x0007C0DD
		public virtual IEnumerable<PropertyInfo> DeclaredProperties
		{
			get
			{
				return this.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			}
		}

		/// <summary>Gets a collection of the interfaces implemented by the current type.</summary>
		/// <returns>A collection of the interfaces implemented by the current type.</returns>
		// Token: 0x170004C3 RID: 1219
		// (get) Token: 0x060020F7 RID: 8439 RVA: 0x0007DEE7 File Offset: 0x0007C0E7
		public virtual IEnumerable<Type> ImplementedInterfaces
		{
			get
			{
				return this.GetInterfaces();
			}
		}

		// Token: 0x020002FB RID: 763
		[CompilerGenerated]
		private sealed class <GetDeclaredMethods>d__9 : IEnumerable<MethodInfo>, IEnumerable, IEnumerator<MethodInfo>, IDisposable, IEnumerator
		{
			// Token: 0x060020F8 RID: 8440 RVA: 0x0007DEEF File Offset: 0x0007C0EF
			[DebuggerHidden]
			public <GetDeclaredMethods>d__9(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x060020F9 RID: 8441 RVA: 0x000020D3 File Offset: 0x000002D3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x060020FA RID: 8442 RVA: 0x0007DF0C File Offset: 0x0007C10C
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				TypeInfo typeInfo = this;
				if (num == 0)
				{
					this.<>1__state = -1;
					array = typeInfo.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
					i = 0;
					goto IL_7B;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				IL_6D:
				i++;
				IL_7B:
				if (i >= array.Length)
				{
					array = null;
					return false;
				}
				MethodInfo methodInfo = array[i];
				if (methodInfo.Name == name)
				{
					this.<>2__current = methodInfo;
					this.<>1__state = 1;
					return true;
				}
				goto IL_6D;
			}

			// Token: 0x170004C4 RID: 1220
			// (get) Token: 0x060020FB RID: 8443 RVA: 0x0007DFAC File Offset: 0x0007C1AC
			MethodInfo IEnumerator<MethodInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060020FC RID: 8444 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170004C5 RID: 1221
			// (get) Token: 0x060020FD RID: 8445 RVA: 0x0007DFAC File Offset: 0x0007C1AC
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x060020FE RID: 8446 RVA: 0x0007DFB4 File Offset: 0x0007C1B4
			[DebuggerHidden]
			IEnumerator<MethodInfo> IEnumerable<MethodInfo>.GetEnumerator()
			{
				TypeInfo.<GetDeclaredMethods>d__9 <GetDeclaredMethods>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<GetDeclaredMethods>d__ = this;
				}
				else
				{
					<GetDeclaredMethods>d__ = new TypeInfo.<GetDeclaredMethods>d__9(0);
					<GetDeclaredMethods>d__.<>4__this = this;
				}
				<GetDeclaredMethods>d__.name = name;
				return <GetDeclaredMethods>d__;
			}

			// Token: 0x060020FF RID: 8447 RVA: 0x0007E003 File Offset: 0x0007C203
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Reflection.MethodInfo>.GetEnumerator();
			}

			// Token: 0x04001298 RID: 4760
			private int <>1__state;

			// Token: 0x04001299 RID: 4761
			private MethodInfo <>2__current;

			// Token: 0x0400129A RID: 4762
			private int <>l__initialThreadId;

			// Token: 0x0400129B RID: 4763
			public TypeInfo <>4__this;

			// Token: 0x0400129C RID: 4764
			private string name;

			// Token: 0x0400129D RID: 4765
			public string <>3__name;

			// Token: 0x0400129E RID: 4766
			private MethodInfo[] <>7__wrap1;

			// Token: 0x0400129F RID: 4767
			private int <>7__wrap2;
		}

		// Token: 0x020002FC RID: 764
		[CompilerGenerated]
		private sealed class <get_DeclaredNestedTypes>d__23 : IEnumerable<TypeInfo>, IEnumerable, IEnumerator<TypeInfo>, IDisposable, IEnumerator
		{
			// Token: 0x06002100 RID: 8448 RVA: 0x0007E00B File Offset: 0x0007C20B
			[DebuggerHidden]
			public <get_DeclaredNestedTypes>d__23(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x06002101 RID: 8449 RVA: 0x000020D3 File Offset: 0x000002D3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06002102 RID: 8450 RVA: 0x0007E028 File Offset: 0x0007C228
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				TypeInfo typeInfo = this;
				if (num != 0)
				{
					if (num != 1)
					{
						return false;
					}
					this.<>1__state = -1;
					i++;
				}
				else
				{
					this.<>1__state = -1;
					array = typeInfo.GetNestedTypes(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
					i = 0;
				}
				if (i >= array.Length)
				{
					array = null;
					return false;
				}
				Type type = array[i];
				this.<>2__current = type.GetTypeInfo();
				this.<>1__state = 1;
				return true;
			}

			// Token: 0x170004C6 RID: 1222
			// (get) Token: 0x06002103 RID: 8451 RVA: 0x0007E0BA File Offset: 0x0007C2BA
			TypeInfo IEnumerator<TypeInfo>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002104 RID: 8452 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x170004C7 RID: 1223
			// (get) Token: 0x06002105 RID: 8453 RVA: 0x0007E0BA File Offset: 0x0007C2BA
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06002106 RID: 8454 RVA: 0x0007E0C4 File Offset: 0x0007C2C4
			[DebuggerHidden]
			IEnumerator<TypeInfo> IEnumerable<TypeInfo>.GetEnumerator()
			{
				TypeInfo.<get_DeclaredNestedTypes>d__23 <get_DeclaredNestedTypes>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<get_DeclaredNestedTypes>d__ = this;
				}
				else
				{
					<get_DeclaredNestedTypes>d__ = new TypeInfo.<get_DeclaredNestedTypes>d__23(0);
					<get_DeclaredNestedTypes>d__.<>4__this = this;
				}
				return <get_DeclaredNestedTypes>d__;
			}

			// Token: 0x06002107 RID: 8455 RVA: 0x0007E107 File Offset: 0x0007C307
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Reflection.TypeInfo>.GetEnumerator();
			}

			// Token: 0x040012A0 RID: 4768
			private int <>1__state;

			// Token: 0x040012A1 RID: 4769
			private TypeInfo <>2__current;

			// Token: 0x040012A2 RID: 4770
			private int <>l__initialThreadId;

			// Token: 0x040012A3 RID: 4771
			public TypeInfo <>4__this;

			// Token: 0x040012A4 RID: 4772
			private Type[] <>7__wrap1;

			// Token: 0x040012A5 RID: 4773
			private int <>7__wrap2;
		}
	}
}
