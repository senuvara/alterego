﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines additional version information for an assembly manifest.</summary>
	// Token: 0x020002BD RID: 701
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyInformationalVersionAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyInformationalVersionAttribute" /> class.</summary>
		/// <param name="informationalVersion">The assembly version information. </param>
		// Token: 0x06001FC2 RID: 8130 RVA: 0x0007C862 File Offset: 0x0007AA62
		public AssemblyInformationalVersionAttribute(string informationalVersion)
		{
			this.m_informationalVersion = informationalVersion;
		}

		/// <summary>Gets version information.</summary>
		/// <returns>A string containing the version information.</returns>
		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x06001FC3 RID: 8131 RVA: 0x0007C871 File Offset: 0x0007AA71
		public string InformationalVersion
		{
			get
			{
				return this.m_informationalVersion;
			}
		}

		// Token: 0x04001131 RID: 4401
		private string m_informationalVersion;
	}
}
