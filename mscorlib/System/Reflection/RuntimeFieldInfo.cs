﻿using System;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x02000317 RID: 791
	internal abstract class RuntimeFieldInfo : FieldInfo, ISerializable
	{
		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x060022D3 RID: 8915 RVA: 0x00002526 File Offset: 0x00000726
		internal BindingFlags BindingFlags
		{
			get
			{
				return BindingFlags.Default;
			}
		}

		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x060022D4 RID: 8916 RVA: 0x000808E8 File Offset: 0x0007EAE8
		public override Module Module
		{
			get
			{
				return this.GetRuntimeModule();
			}
		}

		// Token: 0x060022D5 RID: 8917 RVA: 0x000806EE File Offset: 0x0007E8EE
		internal RuntimeType GetDeclaringTypeInternal()
		{
			return (RuntimeType)this.DeclaringType;
		}

		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x060022D6 RID: 8918 RVA: 0x000806FB File Offset: 0x0007E8FB
		private RuntimeType ReflectedTypeInternal
		{
			get
			{
				return (RuntimeType)this.ReflectedType;
			}
		}

		// Token: 0x060022D7 RID: 8919 RVA: 0x000808F0 File Offset: 0x0007EAF0
		internal RuntimeModule GetRuntimeModule()
		{
			return this.GetDeclaringTypeInternal().GetRuntimeModule();
		}

		// Token: 0x060022D8 RID: 8920 RVA: 0x000808FD File Offset: 0x0007EAFD
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			MemberInfoSerializationHolder.GetSerializationInfo(info, this.Name, this.ReflectedTypeInternal, this.ToString(), MemberTypes.Field);
		}

		// Token: 0x060022D9 RID: 8921 RVA: 0x00080926 File Offset: 0x0007EB26
		protected RuntimeFieldInfo()
		{
		}
	}
}
