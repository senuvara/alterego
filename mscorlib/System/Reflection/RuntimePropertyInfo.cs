﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace System.Reflection
{
	// Token: 0x02000327 RID: 807
	internal abstract class RuntimePropertyInfo : PropertyInfo, ISerializable
	{
		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x06002391 RID: 9105 RVA: 0x00002526 File Offset: 0x00000726
		internal BindingFlags BindingFlags
		{
			get
			{
				return BindingFlags.Default;
			}
		}

		// Token: 0x17000568 RID: 1384
		// (get) Token: 0x06002392 RID: 9106 RVA: 0x00081B0E File Offset: 0x0007FD0E
		public override Module Module
		{
			get
			{
				return this.GetRuntimeModule();
			}
		}

		// Token: 0x06002393 RID: 9107 RVA: 0x000806EE File Offset: 0x0007E8EE
		internal RuntimeType GetDeclaringTypeInternal()
		{
			return (RuntimeType)this.DeclaringType;
		}

		// Token: 0x17000569 RID: 1385
		// (get) Token: 0x06002394 RID: 9108 RVA: 0x000806FB File Offset: 0x0007E8FB
		private RuntimeType ReflectedTypeInternal
		{
			get
			{
				return (RuntimeType)this.ReflectedType;
			}
		}

		// Token: 0x06002395 RID: 9109 RVA: 0x00081B16 File Offset: 0x0007FD16
		internal RuntimeModule GetRuntimeModule()
		{
			return this.GetDeclaringTypeInternal().GetRuntimeModule();
		}

		// Token: 0x06002396 RID: 9110 RVA: 0x00081B23 File Offset: 0x0007FD23
		public override string ToString()
		{
			return this.FormatNameAndSig(false);
		}

		// Token: 0x06002397 RID: 9111 RVA: 0x00081B2C File Offset: 0x0007FD2C
		private string FormatNameAndSig(bool serialization)
		{
			StringBuilder stringBuilder = new StringBuilder(this.PropertyType.FormatTypeName(serialization));
			stringBuilder.Append(" ");
			stringBuilder.Append(this.Name);
			ParameterInfo[] indexParameters = this.GetIndexParameters();
			if (indexParameters.Length != 0)
			{
				stringBuilder.Append(" [");
				ParameterInfo.FormatParameters(stringBuilder, indexParameters, (CallingConventions)0, serialization);
				stringBuilder.Append("]");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06002398 RID: 9112 RVA: 0x00081B96 File Offset: 0x0007FD96
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			MemberInfoSerializationHolder.GetSerializationInfo(info, this.Name, this.ReflectedTypeInternal, this.ToString(), this.SerializationToString(), MemberTypes.Property, null);
		}

		// Token: 0x06002399 RID: 9113 RVA: 0x00081BC7 File Offset: 0x0007FDC7
		internal string SerializationToString()
		{
			return this.FormatNameAndSig(true);
		}

		// Token: 0x0600239A RID: 9114 RVA: 0x00081BD0 File Offset: 0x0007FDD0
		protected RuntimePropertyInfo()
		{
		}
	}
}
