﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Identifies the platform targeted by an executable.</summary>
	// Token: 0x0200030C RID: 780
	[ComVisible(true)]
	[Serializable]
	public enum ImageFileMachine
	{
		/// <summary>Targets a 32-bit Intel processor.</summary>
		// Token: 0x040012DE RID: 4830
		I386 = 332,
		/// <summary>Targets a 64-bit Intel processor.</summary>
		// Token: 0x040012DF RID: 4831
		IA64 = 512,
		/// <summary>Targets a 64-bit AMD processor.</summary>
		// Token: 0x040012E0 RID: 4832
		AMD64 = 34404,
		/// <summary>Targets an ARM processor.</summary>
		// Token: 0x040012E1 RID: 4833
		ARM = 452
	}
}
