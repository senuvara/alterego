﻿using System;

namespace System.Reflection
{
	// Token: 0x0200031F RID: 799
	internal abstract class RuntimeModule : Module
	{
		// Token: 0x0600235A RID: 9050 RVA: 0x000814A6 File Offset: 0x0007F6A6
		protected RuntimeModule()
		{
		}
	}
}
