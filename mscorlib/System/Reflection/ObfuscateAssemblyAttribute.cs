﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Instructs obfuscation tools to use their standard obfuscation rules for the appropriate assembly type.</summary>
	// Token: 0x020002EB RID: 747
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	[ComVisible(true)]
	public sealed class ObfuscateAssemblyAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.ObfuscateAssemblyAttribute" /> class, specifying whether the assembly to be obfuscated is public or private.</summary>
		/// <param name="assemblyIsPrivate">
		///       <see langword="true" /> if the assembly is used within the scope of one application; otherwise, <see langword="false" />.</param>
		// Token: 0x06002081 RID: 8321 RVA: 0x0007D668 File Offset: 0x0007B868
		public ObfuscateAssemblyAttribute(bool assemblyIsPrivate)
		{
			this.m_assemblyIsPrivate = assemblyIsPrivate;
		}

		/// <summary>Gets a <see cref="T:System.Boolean" /> value indicating whether the assembly was marked private.</summary>
		/// <returns>
		///     <see langword="true" /> if the assembly was marked private; otherwise, <see langword="false" />. </returns>
		// Token: 0x170004A4 RID: 1188
		// (get) Token: 0x06002082 RID: 8322 RVA: 0x0007D67E File Offset: 0x0007B87E
		public bool AssemblyIsPrivate
		{
			get
			{
				return this.m_assemblyIsPrivate;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Boolean" /> value indicating whether the obfuscation tool should remove the attribute after processing.</summary>
		/// <returns>
		///     <see langword="true" /> if the obfuscation tool should remove the attribute after processing; otherwise, <see langword="false" />. The default value for this property is <see langword="true" />.</returns>
		// Token: 0x170004A5 RID: 1189
		// (get) Token: 0x06002083 RID: 8323 RVA: 0x0007D686 File Offset: 0x0007B886
		// (set) Token: 0x06002084 RID: 8324 RVA: 0x0007D68E File Offset: 0x0007B88E
		public bool StripAfterObfuscation
		{
			get
			{
				return this.m_strip;
			}
			set
			{
				this.m_strip = value;
			}
		}

		// Token: 0x04001253 RID: 4691
		private bool m_assemblyIsPrivate;

		// Token: 0x04001254 RID: 4692
		private bool m_strip = true;
	}
}
