﻿using System;

namespace System.Reflection
{
	// Token: 0x020002DD RID: 733
	[Flags]
	[Serializable]
	internal enum PInvokeAttributes
	{
		// Token: 0x040011CD RID: 4557
		NoMangle = 1,
		// Token: 0x040011CE RID: 4558
		CharSetMask = 6,
		// Token: 0x040011CF RID: 4559
		CharSetNotSpec = 0,
		// Token: 0x040011D0 RID: 4560
		CharSetAnsi = 2,
		// Token: 0x040011D1 RID: 4561
		CharSetUnicode = 4,
		// Token: 0x040011D2 RID: 4562
		CharSetAuto = 6,
		// Token: 0x040011D3 RID: 4563
		BestFitUseAssem = 0,
		// Token: 0x040011D4 RID: 4564
		BestFitEnabled = 16,
		// Token: 0x040011D5 RID: 4565
		BestFitDisabled = 32,
		// Token: 0x040011D6 RID: 4566
		BestFitMask = 48,
		// Token: 0x040011D7 RID: 4567
		ThrowOnUnmappableCharUseAssem = 0,
		// Token: 0x040011D8 RID: 4568
		ThrowOnUnmappableCharEnabled = 4096,
		// Token: 0x040011D9 RID: 4569
		ThrowOnUnmappableCharDisabled = 8192,
		// Token: 0x040011DA RID: 4570
		ThrowOnUnmappableCharMask = 12288,
		// Token: 0x040011DB RID: 4571
		SupportsLastError = 64,
		// Token: 0x040011DC RID: 4572
		CallConvMask = 1792,
		// Token: 0x040011DD RID: 4573
		CallConvWinapi = 256,
		// Token: 0x040011DE RID: 4574
		CallConvCdecl = 512,
		// Token: 0x040011DF RID: 4575
		CallConvStdcall = 768,
		// Token: 0x040011E0 RID: 4576
		CallConvThiscall = 1024,
		// Token: 0x040011E1 RID: 4577
		CallConvFastcall = 1280,
		// Token: 0x040011E2 RID: 4578
		MaxValue = 65535
	}
}
