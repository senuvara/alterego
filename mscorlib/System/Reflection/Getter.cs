﻿using System;

namespace System.Reflection
{
	// Token: 0x02000326 RID: 806
	// (Invoke) Token: 0x0600238E RID: 9102
	internal delegate R Getter<T, R>(T _this);
}
