﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies that the assembly is not fully signed when created.</summary>
	// Token: 0x020002C2 RID: 706
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyDelaySignAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyDelaySignAttribute" /> class.</summary>
		/// <param name="delaySign">
		///       <see langword="true" /> if the feature this attribute represents is activated; otherwise, <see langword="false" />. </param>
		// Token: 0x06001FCC RID: 8140 RVA: 0x0007C8E3 File Offset: 0x0007AAE3
		public AssemblyDelaySignAttribute(bool delaySign)
		{
			this.m_delaySign = delaySign;
		}

		/// <summary>Gets a value indicating the state of the attribute.</summary>
		/// <returns>
		///     <see langword="true" /> if this assembly has been built as delay-signed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x06001FCD RID: 8141 RVA: 0x0007C8F2 File Offset: 0x0007AAF2
		public bool DelaySign
		{
			get
			{
				return this.m_delaySign;
			}
		}

		// Token: 0x04001136 RID: 4406
		private bool m_delaySign;
	}
}
