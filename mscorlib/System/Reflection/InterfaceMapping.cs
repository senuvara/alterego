﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Retrieves the mapping of an interface into the actual methods on a class that implements that interface.</summary>
	// Token: 0x020002D4 RID: 724
	[ComVisible(true)]
	public struct InterfaceMapping
	{
		/// <summary>Represents the type that was used to create the interface mapping.</summary>
		// Token: 0x0400118C RID: 4492
		[ComVisible(true)]
		public Type TargetType;

		/// <summary>Shows the type that represents the interface.</summary>
		// Token: 0x0400118D RID: 4493
		[ComVisible(true)]
		public Type InterfaceType;

		/// <summary>Shows the methods that implement the interface.</summary>
		// Token: 0x0400118E RID: 4494
		[ComVisible(true)]
		public MethodInfo[] TargetMethods;

		/// <summary>Shows the methods that are defined on the interface.</summary>
		// Token: 0x0400118F RID: 4495
		[ComVisible(true)]
		public MethodInfo[] InterfaceMethods;
	}
}
