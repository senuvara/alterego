﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Provides access to the metadata and MSIL for the body of a method.</summary>
	// Token: 0x0200030E RID: 782
	[ComVisible(true)]
	[StructLayout(LayoutKind.Sequential)]
	public class MethodBody
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.MethodBody" /> class.</summary>
		// Token: 0x06002254 RID: 8788 RVA: 0x00002050 File Offset: 0x00000250
		protected MethodBody()
		{
		}

		/// <summary>Gets a list that includes all the exception-handling clauses in the method body.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IList`1" /> of <see cref="T:System.Reflection.ExceptionHandlingClause" /> objects representing the exception-handling clauses in the body of the method.</returns>
		// Token: 0x1700051C RID: 1308
		// (get) Token: 0x06002255 RID: 8789 RVA: 0x00080204 File Offset: 0x0007E404
		public virtual IList<ExceptionHandlingClause> ExceptionHandlingClauses
		{
			get
			{
				return Array.AsReadOnly<ExceptionHandlingClause>(this.clauses);
			}
		}

		/// <summary>Gets the list of local variables declared in the method body.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IList`1" /> of <see cref="T:System.Reflection.LocalVariableInfo" /> objects that describe the local variables declared in the method body.</returns>
		// Token: 0x1700051D RID: 1309
		// (get) Token: 0x06002256 RID: 8790 RVA: 0x00080211 File Offset: 0x0007E411
		public virtual IList<LocalVariableInfo> LocalVariables
		{
			get
			{
				return Array.AsReadOnly<LocalVariableInfo>(this.locals);
			}
		}

		/// <summary>Gets a value indicating whether local variables in the method body are initialized to the default values for their types.</summary>
		/// <returns>
		///     <see langword="true" /> if the method body contains code to initialize local variables to <see langword="null" /> for reference types, or to the zero-initialized value for value types; otherwise, <see langword="false" />.</returns>
		// Token: 0x1700051E RID: 1310
		// (get) Token: 0x06002257 RID: 8791 RVA: 0x0008021E File Offset: 0x0007E41E
		public virtual bool InitLocals
		{
			get
			{
				return this.init_locals;
			}
		}

		/// <summary>Gets a metadata token for the signature that describes the local variables for the method in metadata.</summary>
		/// <returns>An integer that represents the metadata token.</returns>
		// Token: 0x1700051F RID: 1311
		// (get) Token: 0x06002258 RID: 8792 RVA: 0x00080226 File Offset: 0x0007E426
		public virtual int LocalSignatureMetadataToken
		{
			get
			{
				return this.sig_token;
			}
		}

		/// <summary>Gets the maximum number of items on the operand stack when the method is executing.</summary>
		/// <returns>The maximum number of items on the operand stack when the method is executing.</returns>
		// Token: 0x17000520 RID: 1312
		// (get) Token: 0x06002259 RID: 8793 RVA: 0x0008022E File Offset: 0x0007E42E
		public virtual int MaxStackSize
		{
			get
			{
				return this.max_stack;
			}
		}

		/// <summary>Returns the MSIL for the method body, as an array of bytes.</summary>
		/// <returns>An array of type <see cref="T:System.Byte" /> that contains the MSIL for the method body. </returns>
		// Token: 0x0600225A RID: 8794 RVA: 0x00080236 File Offset: 0x0007E436
		public virtual byte[] GetILAsByteArray()
		{
			return this.il;
		}

		// Token: 0x040012E5 RID: 4837
		private ExceptionHandlingClause[] clauses;

		// Token: 0x040012E6 RID: 4838
		private LocalVariableInfo[] locals;

		// Token: 0x040012E7 RID: 4839
		private byte[] il;

		// Token: 0x040012E8 RID: 4840
		private bool init_locals;

		// Token: 0x040012E9 RID: 4841
		private int sig_token;

		// Token: 0x040012EA RID: 4842
		private int max_stack;
	}
}
