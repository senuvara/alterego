﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates that the value of a static field is unique for each thread.</summary>
	// Token: 0x020001B8 RID: 440
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public class ThreadStaticAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ThreadStaticAttribute" /> class.</summary>
		// Token: 0x060013E3 RID: 5091 RVA: 0x000020BF File Offset: 0x000002BF
		public ThreadStaticAttribute()
		{
		}
	}
}
