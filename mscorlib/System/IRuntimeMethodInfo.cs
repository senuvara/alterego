﻿using System;

namespace System
{
	// Token: 0x020001E7 RID: 487
	internal interface IRuntimeMethodInfo
	{
		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06001768 RID: 5992
		RuntimeMethodHandleInternal Value { get; }
	}
}
