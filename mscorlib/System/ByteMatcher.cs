﻿using System;
using System.Collections;

namespace System
{
	// Token: 0x0200022A RID: 554
	internal class ByteMatcher
	{
		// Token: 0x06001AAF RID: 6831 RVA: 0x00065466 File Offset: 0x00063666
		public void AddMapping(TermInfoStrings key, byte[] val)
		{
			if (val.Length == 0)
			{
				return;
			}
			this.map[val] = key;
			this.starts[(int)val[0]] = true;
		}

		// Token: 0x06001AB0 RID: 6832 RVA: 0x000020D3 File Offset: 0x000002D3
		public void Sort()
		{
		}

		// Token: 0x06001AB1 RID: 6833 RVA: 0x00065498 File Offset: 0x00063698
		public bool StartsWith(int c)
		{
			return this.starts[c] != null;
		}

		// Token: 0x06001AB2 RID: 6834 RVA: 0x000654B0 File Offset: 0x000636B0
		public TermInfoStrings Match(char[] buffer, int offset, int length, out int used)
		{
			foreach (object obj in this.map.Keys)
			{
				byte[] array = (byte[])obj;
				int num = 0;
				while (num < array.Length && num < length && (char)array[num] == buffer[offset + num])
				{
					if (array.Length - 1 == num)
					{
						used = array.Length;
						return (TermInfoStrings)this.map[array];
					}
					num++;
				}
			}
			used = 0;
			return (TermInfoStrings)(-1);
		}

		// Token: 0x06001AB3 RID: 6835 RVA: 0x00065550 File Offset: 0x00063750
		public ByteMatcher()
		{
		}

		// Token: 0x04000D4E RID: 3406
		private Hashtable map = new Hashtable();

		// Token: 0x04000D4F RID: 3407
		private Hashtable starts = new Hashtable();
	}
}
