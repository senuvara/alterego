﻿using System;

namespace System
{
	// Token: 0x0200024C RID: 588
	internal struct Coord
	{
		// Token: 0x06001B55 RID: 6997 RVA: 0x000676A8 File Offset: 0x000658A8
		public Coord(int x, int y)
		{
			this.X = (short)x;
			this.Y = (short)y;
		}

		// Token: 0x04000F5E RID: 3934
		public short X;

		// Token: 0x04000F5F RID: 3935
		public short Y;
	}
}
