﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>The exception that is thrown when there is an attempt to dynamically access a method that does not exist.</summary>
	// Token: 0x0200018E RID: 398
	[ComVisible(true)]
	[Serializable]
	public class MissingMethodException : MissingMemberException, ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMethodException" /> class.</summary>
		// Token: 0x0600114B RID: 4427 RVA: 0x00047C50 File Offset: 0x00045E50
		public MissingMethodException() : base(Environment.GetResourceString("Attempted to access a missing method."))
		{
			base.SetErrorCode(-2146233069);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMethodException" /> class with a specified error message.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. </param>
		// Token: 0x0600114C RID: 4428 RVA: 0x00047C6D File Offset: 0x00045E6D
		public MissingMethodException(string message) : base(message)
		{
			base.SetErrorCode(-2146233069);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMethodException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x0600114D RID: 4429 RVA: 0x00047C81 File Offset: 0x00045E81
		public MissingMethodException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233069);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMethodException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x0600114E RID: 4430 RVA: 0x000479F9 File Offset: 0x00045BF9
		protected MissingMethodException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>Gets the text string showing the class name, the method name, and the signature of the missing method. This property is read-only.</summary>
		/// <returns>The error message string.</returns>
		// Token: 0x17000203 RID: 515
		// (get) Token: 0x0600114F RID: 4431 RVA: 0x00047C98 File Offset: 0x00045E98
		public override string Message
		{
			[SecuritySafeCritical]
			get
			{
				if (this.ClassName == null)
				{
					return base.Message;
				}
				string text = this.ClassName + "." + this.MemberName;
				if (!string.IsNullOrEmpty(this.signature))
				{
					text = string.Format(CultureInfo.InvariantCulture, this.signature, text);
				}
				if (!string.IsNullOrEmpty(this._message))
				{
					text = text + " Due to: " + this._message;
				}
				return text;
			}
		}

		// Token: 0x06001150 RID: 4432 RVA: 0x00047A6D File Offset: 0x00045C6D
		private MissingMethodException(string className, string methodName, byte[] signature)
		{
			this.ClassName = className;
			this.MemberName = methodName;
			this.Signature = signature;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MissingMethodException" /> class with the specified class name and method name.</summary>
		/// <param name="className">The name of the class in which access to a nonexistent method was attempted. </param>
		/// <param name="methodName">The name of the method that cannot be accessed. </param>
		// Token: 0x06001151 RID: 4433 RVA: 0x00047A8A File Offset: 0x00045C8A
		public MissingMethodException(string className, string methodName)
		{
			this.ClassName = className;
			this.MemberName = methodName;
		}

		// Token: 0x06001152 RID: 4434 RVA: 0x00047D0A File Offset: 0x00045F0A
		private MissingMethodException(string className, string methodName, string signature, string message) : base(message)
		{
			this.ClassName = className;
			this.MemberName = methodName;
			this.signature = signature;
		}

		// Token: 0x04000A05 RID: 2565
		[NonSerialized]
		private string signature;
	}
}
