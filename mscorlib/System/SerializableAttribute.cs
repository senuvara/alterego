﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates that a class can be serialized. This class cannot be inherited.</summary>
	// Token: 0x020001AC RID: 428
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Delegate, Inherited = false)]
	[ComVisible(true)]
	public sealed class SerializableAttribute : Attribute
	{
		// Token: 0x060012AD RID: 4781 RVA: 0x0004CC6E File Offset: 0x0004AE6E
		internal static Attribute GetCustomAttribute(RuntimeType type)
		{
			if ((type.Attributes & TypeAttributes.Serializable) != TypeAttributes.Serializable)
			{
				return null;
			}
			return new SerializableAttribute();
		}

		// Token: 0x060012AE RID: 4782 RVA: 0x0004CC8A File Offset: 0x0004AE8A
		internal static bool IsDefined(RuntimeType type)
		{
			return type.IsSerializable;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.SerializableAttribute" /> class.</summary>
		// Token: 0x060012AF RID: 4783 RVA: 0x000020BF File Offset: 0x000002BF
		public SerializableAttribute()
		{
		}
	}
}
