﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x020001F8 RID: 504
	internal static class ConsoleDriver
	{
		// Token: 0x0600185D RID: 6237 RVA: 0x0005CE7C File Offset: 0x0005B07C
		static ConsoleDriver()
		{
			if (!ConsoleDriver.IsConsole)
			{
				ConsoleDriver.driver = ConsoleDriver.CreateNullConsoleDriver();
				return;
			}
			if (Environment.IsRunningOnWindows)
			{
				ConsoleDriver.driver = ConsoleDriver.CreateWindowsConsoleDriver();
				return;
			}
			string environmentVariable = Environment.GetEnvironmentVariable("TERM");
			if (environmentVariable == "dumb")
			{
				ConsoleDriver.is_console = false;
				ConsoleDriver.driver = ConsoleDriver.CreateNullConsoleDriver();
				return;
			}
			ConsoleDriver.driver = ConsoleDriver.CreateTermInfoDriver(environmentVariable);
		}

		// Token: 0x0600185E RID: 6238 RVA: 0x0005CEE1 File Offset: 0x0005B0E1
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static IConsoleDriver CreateNullConsoleDriver()
		{
			return new NullConsoleDriver();
		}

		// Token: 0x0600185F RID: 6239 RVA: 0x0005CEE8 File Offset: 0x0005B0E8
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static IConsoleDriver CreateWindowsConsoleDriver()
		{
			return new WindowsConsoleDriver();
		}

		// Token: 0x06001860 RID: 6240 RVA: 0x0005CEEF File Offset: 0x0005B0EF
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static IConsoleDriver CreateTermInfoDriver(string term)
		{
			return new TermInfoDriver(term);
		}

		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06001861 RID: 6241 RVA: 0x0005CEF7 File Offset: 0x0005B0F7
		public static bool Initialized
		{
			get
			{
				return ConsoleDriver.driver.Initialized;
			}
		}

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06001862 RID: 6242 RVA: 0x0005CF03 File Offset: 0x0005B103
		// (set) Token: 0x06001863 RID: 6243 RVA: 0x0005CF0F File Offset: 0x0005B10F
		public static ConsoleColor BackgroundColor
		{
			get
			{
				return ConsoleDriver.driver.BackgroundColor;
			}
			set
			{
				if (value < ConsoleColor.Black || value > ConsoleColor.White)
				{
					throw new ArgumentOutOfRangeException("value", "Not a ConsoleColor value.");
				}
				ConsoleDriver.driver.BackgroundColor = value;
			}
		}

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x06001864 RID: 6244 RVA: 0x0005CF35 File Offset: 0x0005B135
		// (set) Token: 0x06001865 RID: 6245 RVA: 0x0005CF41 File Offset: 0x0005B141
		public static int BufferHeight
		{
			get
			{
				return ConsoleDriver.driver.BufferHeight;
			}
			set
			{
				ConsoleDriver.driver.BufferHeight = value;
			}
		}

		// Token: 0x17000324 RID: 804
		// (get) Token: 0x06001866 RID: 6246 RVA: 0x0005CF4E File Offset: 0x0005B14E
		// (set) Token: 0x06001867 RID: 6247 RVA: 0x0005CF5A File Offset: 0x0005B15A
		public static int BufferWidth
		{
			get
			{
				return ConsoleDriver.driver.BufferWidth;
			}
			set
			{
				ConsoleDriver.driver.BufferWidth = value;
			}
		}

		// Token: 0x17000325 RID: 805
		// (get) Token: 0x06001868 RID: 6248 RVA: 0x0005CF67 File Offset: 0x0005B167
		public static bool CapsLock
		{
			get
			{
				return ConsoleDriver.driver.CapsLock;
			}
		}

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x06001869 RID: 6249 RVA: 0x0005CF73 File Offset: 0x0005B173
		// (set) Token: 0x0600186A RID: 6250 RVA: 0x0005CF7F File Offset: 0x0005B17F
		public static int CursorLeft
		{
			get
			{
				return ConsoleDriver.driver.CursorLeft;
			}
			set
			{
				ConsoleDriver.driver.CursorLeft = value;
			}
		}

		// Token: 0x17000327 RID: 807
		// (get) Token: 0x0600186B RID: 6251 RVA: 0x0005CF8C File Offset: 0x0005B18C
		// (set) Token: 0x0600186C RID: 6252 RVA: 0x0005CF98 File Offset: 0x0005B198
		public static int CursorSize
		{
			get
			{
				return ConsoleDriver.driver.CursorSize;
			}
			set
			{
				ConsoleDriver.driver.CursorSize = value;
			}
		}

		// Token: 0x17000328 RID: 808
		// (get) Token: 0x0600186D RID: 6253 RVA: 0x0005CFA5 File Offset: 0x0005B1A5
		// (set) Token: 0x0600186E RID: 6254 RVA: 0x0005CFB1 File Offset: 0x0005B1B1
		public static int CursorTop
		{
			get
			{
				return ConsoleDriver.driver.CursorTop;
			}
			set
			{
				ConsoleDriver.driver.CursorTop = value;
			}
		}

		// Token: 0x17000329 RID: 809
		// (get) Token: 0x0600186F RID: 6255 RVA: 0x0005CFBE File Offset: 0x0005B1BE
		// (set) Token: 0x06001870 RID: 6256 RVA: 0x0005CFCA File Offset: 0x0005B1CA
		public static bool CursorVisible
		{
			get
			{
				return ConsoleDriver.driver.CursorVisible;
			}
			set
			{
				ConsoleDriver.driver.CursorVisible = value;
			}
		}

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x06001871 RID: 6257 RVA: 0x0005CFD7 File Offset: 0x0005B1D7
		public static bool KeyAvailable
		{
			get
			{
				return ConsoleDriver.driver.KeyAvailable;
			}
		}

		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06001872 RID: 6258 RVA: 0x0005CFE3 File Offset: 0x0005B1E3
		// (set) Token: 0x06001873 RID: 6259 RVA: 0x0005CFEF File Offset: 0x0005B1EF
		public static ConsoleColor ForegroundColor
		{
			get
			{
				return ConsoleDriver.driver.ForegroundColor;
			}
			set
			{
				if (value < ConsoleColor.Black || value > ConsoleColor.White)
				{
					throw new ArgumentOutOfRangeException("value", "Not a ConsoleColor value.");
				}
				ConsoleDriver.driver.ForegroundColor = value;
			}
		}

		// Token: 0x1700032C RID: 812
		// (get) Token: 0x06001874 RID: 6260 RVA: 0x0005D015 File Offset: 0x0005B215
		public static int LargestWindowHeight
		{
			get
			{
				return ConsoleDriver.driver.LargestWindowHeight;
			}
		}

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06001875 RID: 6261 RVA: 0x0005D021 File Offset: 0x0005B221
		public static int LargestWindowWidth
		{
			get
			{
				return ConsoleDriver.driver.LargestWindowWidth;
			}
		}

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06001876 RID: 6262 RVA: 0x0005D02D File Offset: 0x0005B22D
		public static bool NumberLock
		{
			get
			{
				return ConsoleDriver.driver.NumberLock;
			}
		}

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06001877 RID: 6263 RVA: 0x0005D039 File Offset: 0x0005B239
		// (set) Token: 0x06001878 RID: 6264 RVA: 0x0005D045 File Offset: 0x0005B245
		public static string Title
		{
			get
			{
				return ConsoleDriver.driver.Title;
			}
			set
			{
				ConsoleDriver.driver.Title = value;
			}
		}

		// Token: 0x17000330 RID: 816
		// (get) Token: 0x06001879 RID: 6265 RVA: 0x0005D052 File Offset: 0x0005B252
		// (set) Token: 0x0600187A RID: 6266 RVA: 0x0005D05E File Offset: 0x0005B25E
		public static bool TreatControlCAsInput
		{
			get
			{
				return ConsoleDriver.driver.TreatControlCAsInput;
			}
			set
			{
				ConsoleDriver.driver.TreatControlCAsInput = value;
			}
		}

		// Token: 0x17000331 RID: 817
		// (get) Token: 0x0600187B RID: 6267 RVA: 0x0005D06B File Offset: 0x0005B26B
		// (set) Token: 0x0600187C RID: 6268 RVA: 0x0005D077 File Offset: 0x0005B277
		public static int WindowHeight
		{
			get
			{
				return ConsoleDriver.driver.WindowHeight;
			}
			set
			{
				ConsoleDriver.driver.WindowHeight = value;
			}
		}

		// Token: 0x17000332 RID: 818
		// (get) Token: 0x0600187D RID: 6269 RVA: 0x0005D084 File Offset: 0x0005B284
		// (set) Token: 0x0600187E RID: 6270 RVA: 0x0005D090 File Offset: 0x0005B290
		public static int WindowLeft
		{
			get
			{
				return ConsoleDriver.driver.WindowLeft;
			}
			set
			{
				ConsoleDriver.driver.WindowLeft = value;
			}
		}

		// Token: 0x17000333 RID: 819
		// (get) Token: 0x0600187F RID: 6271 RVA: 0x0005D09D File Offset: 0x0005B29D
		// (set) Token: 0x06001880 RID: 6272 RVA: 0x0005D0A9 File Offset: 0x0005B2A9
		public static int WindowTop
		{
			get
			{
				return ConsoleDriver.driver.WindowTop;
			}
			set
			{
				ConsoleDriver.driver.WindowTop = value;
			}
		}

		// Token: 0x17000334 RID: 820
		// (get) Token: 0x06001881 RID: 6273 RVA: 0x0005D0B6 File Offset: 0x0005B2B6
		// (set) Token: 0x06001882 RID: 6274 RVA: 0x0005D0C2 File Offset: 0x0005B2C2
		public static int WindowWidth
		{
			get
			{
				return ConsoleDriver.driver.WindowWidth;
			}
			set
			{
				ConsoleDriver.driver.WindowWidth = value;
			}
		}

		// Token: 0x17000335 RID: 821
		// (get) Token: 0x06001883 RID: 6275 RVA: 0x0005D0CF File Offset: 0x0005B2CF
		public static bool IsErrorRedirected
		{
			get
			{
				return !ConsoleDriver.Isatty(MonoIO.ConsoleError);
			}
		}

		// Token: 0x17000336 RID: 822
		// (get) Token: 0x06001884 RID: 6276 RVA: 0x0005D0DE File Offset: 0x0005B2DE
		public static bool IsOutputRedirected
		{
			get
			{
				return !ConsoleDriver.Isatty(MonoIO.ConsoleOutput);
			}
		}

		// Token: 0x17000337 RID: 823
		// (get) Token: 0x06001885 RID: 6277 RVA: 0x0005D0ED File Offset: 0x0005B2ED
		public static bool IsInputRedirected
		{
			get
			{
				return !ConsoleDriver.Isatty(MonoIO.ConsoleInput);
			}
		}

		// Token: 0x06001886 RID: 6278 RVA: 0x0005D0FC File Offset: 0x0005B2FC
		public static void Beep(int frequency, int duration)
		{
			ConsoleDriver.driver.Beep(frequency, duration);
		}

		// Token: 0x06001887 RID: 6279 RVA: 0x0005D10A File Offset: 0x0005B30A
		public static void Clear()
		{
			ConsoleDriver.driver.Clear();
		}

		// Token: 0x06001888 RID: 6280 RVA: 0x0005D118 File Offset: 0x0005B318
		public static void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop)
		{
			ConsoleDriver.MoveBufferArea(sourceLeft, sourceTop, sourceWidth, sourceHeight, targetLeft, targetTop, ' ', ConsoleColor.Black, ConsoleColor.Black);
		}

		// Token: 0x06001889 RID: 6281 RVA: 0x0005D138 File Offset: 0x0005B338
		public static void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop, char sourceChar, ConsoleColor sourceForeColor, ConsoleColor sourceBackColor)
		{
			ConsoleDriver.driver.MoveBufferArea(sourceLeft, sourceTop, sourceWidth, sourceHeight, targetLeft, targetTop, sourceChar, sourceForeColor, sourceBackColor);
		}

		// Token: 0x0600188A RID: 6282 RVA: 0x0005D15D File Offset: 0x0005B35D
		public static void Init()
		{
			ConsoleDriver.driver.Init();
		}

		// Token: 0x0600188B RID: 6283 RVA: 0x0005D16C File Offset: 0x0005B36C
		public static int Read()
		{
			return (int)ConsoleDriver.ReadKey(false).KeyChar;
		}

		// Token: 0x0600188C RID: 6284 RVA: 0x0005D187 File Offset: 0x0005B387
		public static string ReadLine()
		{
			return ConsoleDriver.driver.ReadLine();
		}

		// Token: 0x0600188D RID: 6285 RVA: 0x0005D193 File Offset: 0x0005B393
		public static ConsoleKeyInfo ReadKey(bool intercept)
		{
			return ConsoleDriver.driver.ReadKey(intercept);
		}

		// Token: 0x0600188E RID: 6286 RVA: 0x0005D1A0 File Offset: 0x0005B3A0
		public static void ResetColor()
		{
			ConsoleDriver.driver.ResetColor();
		}

		// Token: 0x0600188F RID: 6287 RVA: 0x0005D1AC File Offset: 0x0005B3AC
		public static void SetBufferSize(int width, int height)
		{
			ConsoleDriver.driver.SetBufferSize(width, height);
		}

		// Token: 0x06001890 RID: 6288 RVA: 0x0005D1BA File Offset: 0x0005B3BA
		public static void SetCursorPosition(int left, int top)
		{
			ConsoleDriver.driver.SetCursorPosition(left, top);
		}

		// Token: 0x06001891 RID: 6289 RVA: 0x0005D1C8 File Offset: 0x0005B3C8
		public static void SetWindowPosition(int left, int top)
		{
			ConsoleDriver.driver.SetWindowPosition(left, top);
		}

		// Token: 0x06001892 RID: 6290 RVA: 0x0005D1D6 File Offset: 0x0005B3D6
		public static void SetWindowSize(int width, int height)
		{
			ConsoleDriver.driver.SetWindowSize(width, height);
		}

		// Token: 0x17000338 RID: 824
		// (get) Token: 0x06001893 RID: 6291 RVA: 0x0005D1E4 File Offset: 0x0005B3E4
		public static bool IsConsole
		{
			get
			{
				if (ConsoleDriver.called_isatty)
				{
					return ConsoleDriver.is_console;
				}
				ConsoleDriver.is_console = (ConsoleDriver.Isatty(MonoIO.ConsoleOutput) && ConsoleDriver.Isatty(MonoIO.ConsoleInput));
				ConsoleDriver.called_isatty = true;
				return ConsoleDriver.is_console;
			}
		}

		// Token: 0x06001894 RID: 6292
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Isatty(IntPtr handle);

		// Token: 0x06001895 RID: 6293
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int InternalKeyAvailable(int ms_timeout);

		// Token: 0x06001896 RID: 6294
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal unsafe static extern bool TtySetup(string keypadXmit, string teardown, out byte[] control_characters, out int* address);

		// Token: 0x06001897 RID: 6295
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool SetEcho(bool wantEcho);

		// Token: 0x06001898 RID: 6296
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool SetBreak(bool wantBreak);

		// Token: 0x04000C4C RID: 3148
		internal static IConsoleDriver driver;

		// Token: 0x04000C4D RID: 3149
		private static bool is_console;

		// Token: 0x04000C4E RID: 3150
		private static bool called_isatty;
	}
}
