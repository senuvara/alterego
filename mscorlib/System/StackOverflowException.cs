﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when the execution stack overflows because it contains too many nested method calls. This class cannot be inherited.</summary>
	// Token: 0x020001AF RID: 431
	[ComVisible(true)]
	[Serializable]
	public sealed class StackOverflowException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.StackOverflowException" /> class, setting the <see cref="P:System.Exception.Message" /> property of the new instance to a system-supplied message that describes the error, such as "The requested operation caused a stack overflow." This message takes into account the current system culture.</summary>
		// Token: 0x060012E4 RID: 4836 RVA: 0x0004D170 File Offset: 0x0004B370
		public StackOverflowException() : base(Environment.GetResourceString("Operation caused a stack overflow."))
		{
			base.SetErrorCode(-2147023895);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.StackOverflowException" /> class with a specified error message.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. The content of message is intended to be understood by humans. The caller of this constructor is required to ensure that this string has been localized for the current system culture. </param>
		// Token: 0x060012E5 RID: 4837 RVA: 0x0004D18D File Offset: 0x0004B38D
		public StackOverflowException(string message) : base(message)
		{
			base.SetErrorCode(-2147023895);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.StackOverflowException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060012E6 RID: 4838 RVA: 0x0004D1A1 File Offset: 0x0004B3A1
		public StackOverflowException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2147023895);
		}

		// Token: 0x060012E7 RID: 4839 RVA: 0x000319C9 File Offset: 0x0002FBC9
		internal StackOverflowException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
