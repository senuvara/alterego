﻿using System;
using System.Buffers;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020000B7 RID: 183
	[DebuggerTypeProxy(typeof(MemoryDebugView<>))]
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	public readonly struct Memory<T>
	{
		// Token: 0x0600061E RID: 1566 RVA: 0x00021A88 File Offset: 0x0001FC88
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Memory(T[] array)
		{
			if (array == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
			}
			if (default(T) == null && array.GetType() != typeof(T[]))
			{
				ThrowHelper.ThrowArrayTypeMismatchException_ArrayTypeMustBeExactMatch(typeof(T));
			}
			this._arrayOrOwnedMemory = array;
			this._index = 0;
			this._length = array.Length;
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x00021AEC File Offset: 0x0001FCEC
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Memory(T[] array, int start, int length)
		{
			if (array == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
			}
			if (default(T) == null && array.GetType() != typeof(T[]))
			{
				ThrowHelper.ThrowArrayTypeMismatchException_ArrayTypeMustBeExactMatch(typeof(T));
			}
			if (start > array.Length || length > array.Length - start)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			this._arrayOrOwnedMemory = array;
			this._index = start;
			this._length = length;
		}

		// Token: 0x06000620 RID: 1568 RVA: 0x00021B62 File Offset: 0x0001FD62
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal Memory(OwnedMemory<T> owner, int index, int length)
		{
			if (owner == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.ownedMemory);
			}
			if (index < 0 || length < 0)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			this._arrayOrOwnedMemory = owner;
			this._index = (index | int.MinValue);
			this._length = length;
		}

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x06000621 RID: 1569 RVA: 0x00021B98 File Offset: 0x0001FD98
		private string DebuggerDisplay
		{
			get
			{
				return string.Format("{{{0}[{1}]}}", typeof(T).Name, this._length);
			}
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x00021BBE File Offset: 0x0001FDBE
		public static implicit operator Memory<T>(T[] array)
		{
			return new Memory<T>(array);
		}

		// Token: 0x06000623 RID: 1571 RVA: 0x00021BC6 File Offset: 0x0001FDC6
		public static implicit operator Memory<T>(ArraySegment<T> arraySegment)
		{
			return new Memory<T>(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
		}

		// Token: 0x06000624 RID: 1572 RVA: 0x00021BE4 File Offset: 0x0001FDE4
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator ReadOnlyMemory<T>(Memory<T> memory)
		{
			if (memory._index < 0)
			{
				return new ReadOnlyMemory<T>((OwnedMemory<T>)memory._arrayOrOwnedMemory, memory._index & int.MaxValue, memory._length);
			}
			return new ReadOnlyMemory<T>((T[])memory._arrayOrOwnedMemory, memory._index, memory._length);
		}

		// Token: 0x17000111 RID: 273
		// (get) Token: 0x06000625 RID: 1573 RVA: 0x00021C39 File Offset: 0x0001FE39
		public static Memory<T> Empty
		{
			[CompilerGenerated]
			get
			{
				return Memory<T>.<Empty>k__BackingField;
			}
		} = SpanHelpers.PerTypeValues<T>.EmptyArray;

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x06000626 RID: 1574 RVA: 0x00021C40 File Offset: 0x0001FE40
		public int Length
		{
			get
			{
				return this._length;
			}
		}

		// Token: 0x17000113 RID: 275
		// (get) Token: 0x06000627 RID: 1575 RVA: 0x00021C48 File Offset: 0x0001FE48
		public bool IsEmpty
		{
			get
			{
				return this._length == 0;
			}
		}

		// Token: 0x06000628 RID: 1576 RVA: 0x00021C54 File Offset: 0x0001FE54
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Memory<T> Slice(int start)
		{
			if (start > this._length)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			if (this._index < 0)
			{
				return new Memory<T>((OwnedMemory<T>)this._arrayOrOwnedMemory, (this._index & int.MaxValue) + start, this._length - start);
			}
			return new Memory<T>((T[])this._arrayOrOwnedMemory, this._index + start, this._length - start);
		}

		// Token: 0x06000629 RID: 1577 RVA: 0x00021CC4 File Offset: 0x0001FEC4
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Memory<T> Slice(int start, int length)
		{
			if (start > this._length || length > this._length - start)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			if (this._index < 0)
			{
				return new Memory<T>((OwnedMemory<T>)this._arrayOrOwnedMemory, (this._index & int.MaxValue) + start, length);
			}
			return new Memory<T>((T[])this._arrayOrOwnedMemory, this._index + start, length);
		}

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x0600062A RID: 1578 RVA: 0x00021D30 File Offset: 0x0001FF30
		public Span<T> Span
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if (this._index < 0)
				{
					return ((OwnedMemory<T>)this._arrayOrOwnedMemory).Span.Slice(this._index & int.MaxValue, this._length);
				}
				return new Span<T>((T[])this._arrayOrOwnedMemory, this._index, this._length);
			}
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x00021D90 File Offset: 0x0001FF90
		public unsafe MemoryHandle Retain(bool pin = false)
		{
			MemoryHandle result;
			if (pin)
			{
				if (this._index < 0)
				{
					result = ((OwnedMemory<T>)this._arrayOrOwnedMemory).Pin();
					result.AddOffset((this._index & int.MaxValue) * Unsafe.SizeOf<T>());
				}
				else
				{
					GCHandle handle = GCHandle.Alloc((T[])this._arrayOrOwnedMemory, GCHandleType.Pinned);
					void* pinnedPointer = Unsafe.Add<T>((void*)handle.AddrOfPinnedObject(), this._index);
					result = new MemoryHandle(null, pinnedPointer, handle);
				}
			}
			else if (this._index < 0)
			{
				((OwnedMemory<T>)this._arrayOrOwnedMemory).Retain();
				result = new MemoryHandle((OwnedMemory<T>)this._arrayOrOwnedMemory, null, default(GCHandle));
			}
			else
			{
				result = new MemoryHandle(null, null, default(GCHandle));
			}
			return result;
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x00021E5C File Offset: 0x0002005C
		public bool TryGetArray(out ArraySegment<T> arraySegment)
		{
			if (this._index >= 0)
			{
				arraySegment = new ArraySegment<T>((T[])this._arrayOrOwnedMemory, this._index, this._length);
				return true;
			}
			ArraySegment<T> arraySegment2;
			if (((OwnedMemory<T>)this._arrayOrOwnedMemory).TryGetArray(out arraySegment2))
			{
				arraySegment = new ArraySegment<T>(arraySegment2.Array, arraySegment2.Offset + (this._index & int.MaxValue), this._length);
				return true;
			}
			arraySegment = default(ArraySegment<T>);
			return false;
		}

		// Token: 0x0600062D RID: 1581 RVA: 0x00021EE0 File Offset: 0x000200E0
		public T[] ToArray()
		{
			return this.Span.ToArray();
		}

		// Token: 0x0600062E RID: 1582 RVA: 0x00021EFC File Offset: 0x000200FC
		public override bool Equals(object obj)
		{
			if (obj is ReadOnlyMemory<T>)
			{
				return ((ReadOnlyMemory<T>)obj).Equals(this);
			}
			bool flag = obj is Memory<T>;
			Memory<T> other = flag ? ((Memory<T>)obj) : default(Memory<T>);
			return flag && this.Equals(other);
		}

		// Token: 0x0600062F RID: 1583 RVA: 0x00021F56 File Offset: 0x00020156
		public bool Equals(Memory<T> other)
		{
			return this._arrayOrOwnedMemory == other._arrayOrOwnedMemory && this._index == other._index && this._length == other._length;
		}

		// Token: 0x06000630 RID: 1584 RVA: 0x00021F84 File Offset: 0x00020184
		public override int GetHashCode()
		{
			return Memory<T>.CombineHashCodes(this._arrayOrOwnedMemory.GetHashCode(), (this._index & int.MaxValue).GetHashCode(), this._length.GetHashCode());
		}

		// Token: 0x06000631 RID: 1585 RVA: 0x00021FC3 File Offset: 0x000201C3
		private static int CombineHashCodes(int left, int right)
		{
			return (left << 5) + left ^ right;
		}

		// Token: 0x06000632 RID: 1586 RVA: 0x00021FCC File Offset: 0x000201CC
		private static int CombineHashCodes(int h1, int h2, int h3)
		{
			return Memory<T>.CombineHashCodes(Memory<T>.CombineHashCodes(h1, h2), h3);
		}

		// Token: 0x06000633 RID: 1587 RVA: 0x00021FDB File Offset: 0x000201DB
		// Note: this type is marked as 'beforefieldinit'.
		static Memory()
		{
		}

		// Token: 0x0400065C RID: 1628
		private readonly object _arrayOrOwnedMemory;

		// Token: 0x0400065D RID: 1629
		private readonly int _index;

		// Token: 0x0400065E RID: 1630
		private readonly int _length;

		// Token: 0x0400065F RID: 1631
		private const int RemoveOwnedFlagBitMask = 2147483647;

		// Token: 0x04000660 RID: 1632
		[CompilerGenerated]
		private static readonly Memory<T> <Empty>k__BackingField;
	}
}
