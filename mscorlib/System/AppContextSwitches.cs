﻿using System;

namespace System
{
	// Token: 0x020000EA RID: 234
	internal static class AppContextSwitches
	{
		// Token: 0x0600092B RID: 2347 RVA: 0x000020D3 File Offset: 0x000002D3
		// Note: this type is marked as 'beforefieldinit'.
		static AppContextSwitches()
		{
		}

		// Token: 0x040006D5 RID: 1749
		public static readonly bool ThrowExceptionIfDisposedCancellationTokenSource;

		// Token: 0x040006D6 RID: 1750
		public static readonly bool SetActorAsReferenceWhenCopyingClaimsIdentity;

		// Token: 0x040006D7 RID: 1751
		public static readonly bool NoAsyncCurrentCulture;
	}
}
