﻿using System;
using System.Text;

namespace System
{
	// Token: 0x0200023D RID: 573
	internal class PointerSpec : ModifierSpec
	{
		// Token: 0x06001AFF RID: 6911 RVA: 0x00065F3E File Offset: 0x0006413E
		internal PointerSpec(int pointer_level)
		{
			this.pointer_level = pointer_level;
		}

		// Token: 0x06001B00 RID: 6912 RVA: 0x00065F50 File Offset: 0x00064150
		public Type Resolve(Type type)
		{
			for (int i = 0; i < this.pointer_level; i++)
			{
				type = type.MakePointerType();
			}
			return type;
		}

		// Token: 0x06001B01 RID: 6913 RVA: 0x00065F77 File Offset: 0x00064177
		public StringBuilder Append(StringBuilder sb)
		{
			return sb.Append('*', this.pointer_level);
		}

		// Token: 0x06001B02 RID: 6914 RVA: 0x00065F87 File Offset: 0x00064187
		public override string ToString()
		{
			return this.Append(new StringBuilder()).ToString();
		}

		// Token: 0x04000F26 RID: 3878
		private int pointer_level;
	}
}
