﻿using System;

namespace System
{
	// Token: 0x0200024E RID: 590
	internal struct ConsoleScreenBufferInfo
	{
		// Token: 0x04000F64 RID: 3940
		public Coord Size;

		// Token: 0x04000F65 RID: 3941
		public Coord CursorPosition;

		// Token: 0x04000F66 RID: 3942
		public short Attribute;

		// Token: 0x04000F67 RID: 3943
		public SmallRect Window;

		// Token: 0x04000F68 RID: 3944
		public Coord MaxWindowSize;
	}
}
