﻿using System;

namespace System
{
	// Token: 0x020001A6 RID: 422
	internal enum TypeNameKind
	{
		// Token: 0x04000A73 RID: 2675
		Name,
		// Token: 0x04000A74 RID: 2676
		ToString,
		// Token: 0x04000A75 RID: 2677
		SerializationName,
		// Token: 0x04000A76 RID: 2678
		FullName
	}
}
