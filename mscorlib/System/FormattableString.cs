﻿using System;
using System.Globalization;

namespace System
{
	/// <summary>Represents a composite format string, along with the arguments to be formatted. </summary>
	// Token: 0x020000EB RID: 235
	public abstract class FormattableString : IFormattable
	{
		/// <summary>Returns the composite format string. </summary>
		/// <returns>The composite format string. </returns>
		// Token: 0x17000184 RID: 388
		// (get) Token: 0x0600092C RID: 2348
		public abstract string Format { get; }

		/// <summary>Returns an object array that contains one or more objects to format. </summary>
		/// <returns>An object array that contains one or more objects to format. </returns>
		// Token: 0x0600092D RID: 2349
		public abstract object[] GetArguments();

		/// <summary>Gets the number of arguments to be formatted. </summary>
		/// <returns>The number of arguments to be formatted. </returns>
		// Token: 0x17000185 RID: 389
		// (get) Token: 0x0600092E RID: 2350
		public abstract int ArgumentCount { get; }

		/// <summary>Returns the argument at the specified index position. </summary>
		/// <param name="index">The index of the argument. Its value can range from zero to one less than the value of <see cref="P:System.FormattableString.ArgumentCount" />. </param>
		/// <returns>The argument. </returns>
		// Token: 0x0600092F RID: 2351
		public abstract object GetArgument(int index);

		/// <summary>Returns the string that results from formatting the composite format string along with its arguments by using the formatting conventions of a specified culture. </summary>
		/// <param name="formatProvider">An object that provides culture-specific formatting information. </param>
		/// <returns>A result string formatted by using the conventions of <paramref name="formatProvider" />. </returns>
		// Token: 0x06000930 RID: 2352
		public abstract string ToString(IFormatProvider formatProvider);

		/// <summary>Returns the string that results from formatting the format string along with its arguments by using the formatting conventions of a specified culture. </summary>
		/// <param name="ignored">A string. This argument is ignored. </param>
		/// <param name="formatProvider">An object that provides culture-specific formatting information. </param>
		/// <returns>A string formatted using the conventions of the <paramref name="formatProvider" /> parameter. </returns>
		// Token: 0x06000931 RID: 2353 RVA: 0x00030C26 File Offset: 0x0002EE26
		string IFormattable.ToString(string ignored, IFormatProvider formatProvider)
		{
			return this.ToString(formatProvider);
		}

		/// <summary>Returns a result string in which arguments are formatted by using the conventions of the invariant culture. </summary>
		/// <param name="formattable">The object to convert to a result string. </param>
		/// <returns>The string that results from formatting the current instance by using the conventions of the invariant culture. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="formattable" /> is <see langword="null" />. </exception>
		// Token: 0x06000932 RID: 2354 RVA: 0x00030C2F File Offset: 0x0002EE2F
		public static string Invariant(FormattableString formattable)
		{
			if (formattable == null)
			{
				throw new ArgumentNullException("formattable");
			}
			return formattable.ToString(CultureInfo.InvariantCulture);
		}

		/// <summary>Returns the string that results from formatting the composite format string along with its arguments by using the formatting conventions of the current culture. </summary>
		/// <returns>A result string formatted by using the conventions of the current culture. </returns>
		// Token: 0x06000933 RID: 2355 RVA: 0x00030C4A File Offset: 0x0002EE4A
		public override string ToString()
		{
			return this.ToString(CultureInfo.CurrentCulture);
		}

		/// <summary>Instantiates a new instance of the <see cref="T:System.FormattableString" /> class. </summary>
		// Token: 0x06000934 RID: 2356 RVA: 0x00002050 File Offset: 0x00000250
		protected FormattableString()
		{
		}
	}
}
