﻿using System;
using System.Runtime.CompilerServices;
using System.Security;

namespace System
{
	// Token: 0x020001D8 RID: 472
	internal struct UnSafeCharBuffer
	{
		// Token: 0x06001632 RID: 5682 RVA: 0x00058C5A File Offset: 0x00056E5A
		[SecurityCritical]
		public unsafe UnSafeCharBuffer(char* buffer, int bufferSize)
		{
			this.m_buffer = buffer;
			this.m_totalSize = bufferSize;
			this.m_length = 0;
		}

		// Token: 0x06001633 RID: 5683 RVA: 0x00058C74 File Offset: 0x00056E74
		[SecuritySafeCritical]
		public unsafe void AppendString(string stringToAppend)
		{
			if (string.IsNullOrEmpty(stringToAppend))
			{
				return;
			}
			if (this.m_totalSize - this.m_length < stringToAppend.Length)
			{
				throw new IndexOutOfRangeException();
			}
			fixed (string text = stringToAppend)
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				Buffer.Memcpy((byte*)(this.m_buffer + this.m_length), (byte*)ptr, stringToAppend.Length * 2);
			}
			this.m_length += stringToAppend.Length;
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06001634 RID: 5684 RVA: 0x00058CE8 File Offset: 0x00056EE8
		public int Length
		{
			get
			{
				return this.m_length;
			}
		}

		// Token: 0x04000B9F RID: 2975
		[SecurityCritical]
		private unsafe char* m_buffer;

		// Token: 0x04000BA0 RID: 2976
		private int m_totalSize;

		// Token: 0x04000BA1 RID: 2977
		private int m_length;
	}
}
