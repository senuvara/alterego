﻿using System;

namespace System
{
	// Token: 0x020000E3 RID: 227
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoExtensionAttribute : MonoTODOAttribute
	{
		// Token: 0x0600090B RID: 2315 RVA: 0x0003053C File Offset: 0x0002E73C
		public MonoExtensionAttribute(string comment) : base(comment)
		{
		}
	}
}
