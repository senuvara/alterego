﻿using System;

namespace System.Numerics.Hashing
{
	// Token: 0x02000461 RID: 1121
	internal static class HashHelpers
	{
		// Token: 0x060032CF RID: 13007 RVA: 0x000B5516 File Offset: 0x000B3716
		public static int Combine(int h1, int h2)
		{
			return (h1 << 5 | (int)((uint)h1 >> 27)) + h1 ^ h2;
		}

		// Token: 0x060032D0 RID: 13008 RVA: 0x000B5524 File Offset: 0x000B3724
		// Note: this type is marked as 'beforefieldinit'.
		static HashHelpers()
		{
		}

		// Token: 0x04001B53 RID: 6995
		public static readonly int RandomSeed = new Random().Next(int.MinValue, int.MaxValue);
	}
}
