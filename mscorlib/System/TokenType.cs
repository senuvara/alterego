﻿using System;

namespace System
{
	// Token: 0x02000168 RID: 360
	internal enum TokenType
	{
		// Token: 0x040009A0 RID: 2464
		NumberToken = 1,
		// Token: 0x040009A1 RID: 2465
		YearNumberToken,
		// Token: 0x040009A2 RID: 2466
		Am,
		// Token: 0x040009A3 RID: 2467
		Pm,
		// Token: 0x040009A4 RID: 2468
		MonthToken,
		// Token: 0x040009A5 RID: 2469
		EndOfString,
		// Token: 0x040009A6 RID: 2470
		DayOfWeekToken,
		// Token: 0x040009A7 RID: 2471
		TimeZoneToken,
		// Token: 0x040009A8 RID: 2472
		EraToken,
		// Token: 0x040009A9 RID: 2473
		DateWordToken,
		// Token: 0x040009AA RID: 2474
		UnknownToken,
		// Token: 0x040009AB RID: 2475
		HebrewNumber,
		// Token: 0x040009AC RID: 2476
		JapaneseEraToken,
		// Token: 0x040009AD RID: 2477
		TEraToken,
		// Token: 0x040009AE RID: 2478
		IgnorableSymbol,
		// Token: 0x040009AF RID: 2479
		SEP_Unk = 256,
		// Token: 0x040009B0 RID: 2480
		SEP_End = 512,
		// Token: 0x040009B1 RID: 2481
		SEP_Space = 768,
		// Token: 0x040009B2 RID: 2482
		SEP_Am = 1024,
		// Token: 0x040009B3 RID: 2483
		SEP_Pm = 1280,
		// Token: 0x040009B4 RID: 2484
		SEP_Date = 1536,
		// Token: 0x040009B5 RID: 2485
		SEP_Time = 1792,
		// Token: 0x040009B6 RID: 2486
		SEP_YearSuff = 2048,
		// Token: 0x040009B7 RID: 2487
		SEP_MonthSuff = 2304,
		// Token: 0x040009B8 RID: 2488
		SEP_DaySuff = 2560,
		// Token: 0x040009B9 RID: 2489
		SEP_HourSuff = 2816,
		// Token: 0x040009BA RID: 2490
		SEP_MinuteSuff = 3072,
		// Token: 0x040009BB RID: 2491
		SEP_SecondSuff = 3328,
		// Token: 0x040009BC RID: 2492
		SEP_LocalTimeMark = 3584,
		// Token: 0x040009BD RID: 2493
		SEP_DateOrOffset = 3840,
		// Token: 0x040009BE RID: 2494
		RegularTokenMask = 255,
		// Token: 0x040009BF RID: 2495
		SeparatorTokenMask = 65280
	}
}
