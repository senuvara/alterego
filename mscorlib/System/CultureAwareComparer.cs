﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x020001B3 RID: 435
	[Serializable]
	internal sealed class CultureAwareComparer : StringComparer
	{
		// Token: 0x060013CF RID: 5071 RVA: 0x00050FBF File Offset: 0x0004F1BF
		internal CultureAwareComparer(CultureInfo culture, bool ignoreCase)
		{
			this._compareInfo = culture.CompareInfo;
			this._ignoreCase = ignoreCase;
			this._options = (ignoreCase ? CompareOptions.IgnoreCase : CompareOptions.None);
		}

		// Token: 0x060013D0 RID: 5072 RVA: 0x00050FE7 File Offset: 0x0004F1E7
		internal CultureAwareComparer(CompareInfo compareInfo, bool ignoreCase)
		{
			this._compareInfo = compareInfo;
			this._ignoreCase = ignoreCase;
			this._options = (ignoreCase ? CompareOptions.IgnoreCase : CompareOptions.None);
		}

		// Token: 0x060013D1 RID: 5073 RVA: 0x0005100A File Offset: 0x0004F20A
		internal CultureAwareComparer(CompareInfo compareInfo, CompareOptions options)
		{
			this._compareInfo = compareInfo;
			this._options = options;
			this._ignoreCase = ((options & CompareOptions.IgnoreCase) == CompareOptions.IgnoreCase || (options & CompareOptions.OrdinalIgnoreCase) == CompareOptions.OrdinalIgnoreCase);
		}

		// Token: 0x060013D2 RID: 5074 RVA: 0x0005103D File Offset: 0x0004F23D
		public override int Compare(string x, string y)
		{
			if (x == y)
			{
				return 0;
			}
			if (x == null)
			{
				return -1;
			}
			if (y == null)
			{
				return 1;
			}
			return this._compareInfo.Compare(x, y, this._options);
		}

		// Token: 0x060013D3 RID: 5075 RVA: 0x00051062 File Offset: 0x0004F262
		public override bool Equals(string x, string y)
		{
			return x == y || (x != null && y != null && this._compareInfo.Compare(x, y, this._options) == 0);
		}

		// Token: 0x060013D4 RID: 5076 RVA: 0x00051088 File Offset: 0x0004F288
		public override int GetHashCode(string obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			return this._compareInfo.GetHashCodeOfString(obj, this._options);
		}

		// Token: 0x060013D5 RID: 5077 RVA: 0x000510AC File Offset: 0x0004F2AC
		public override bool Equals(object obj)
		{
			CultureAwareComparer cultureAwareComparer = obj as CultureAwareComparer;
			return cultureAwareComparer != null && this._ignoreCase == cultureAwareComparer._ignoreCase && this._compareInfo.Equals(cultureAwareComparer._compareInfo) && this._options == cultureAwareComparer._options;
		}

		// Token: 0x060013D6 RID: 5078 RVA: 0x000510F8 File Offset: 0x0004F2F8
		public override int GetHashCode()
		{
			int hashCode = this._compareInfo.GetHashCode();
			if (!this._ignoreCase)
			{
				return hashCode;
			}
			return ~hashCode;
		}

		// Token: 0x04000AB1 RID: 2737
		private CompareInfo _compareInfo;

		// Token: 0x04000AB2 RID: 2738
		private bool _ignoreCase;

		// Token: 0x04000AB3 RID: 2739
		[OptionalField]
		private CompareOptions _options;
	}
}
