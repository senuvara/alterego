﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates that an enumeration can be treated as a bit field; that is, a set of flags.</summary>
	// Token: 0x02000150 RID: 336
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Enum, Inherited = false)]
	[Serializable]
	public class FlagsAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.FlagsAttribute" /> class.</summary>
		// Token: 0x06000EF3 RID: 3827 RVA: 0x000020BF File Offset: 0x000002BF
		public FlagsAttribute()
		{
		}
	}
}
