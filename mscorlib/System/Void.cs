﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies a return value type for a method that does not return a value.</summary>
	// Token: 0x02000246 RID: 582
	[ComVisible(true)]
	[Serializable]
	public struct Void
	{
	}
}
