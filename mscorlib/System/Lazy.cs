﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Threading;

namespace System
{
	/// <summary>Provides support for lazy initialization.</summary>
	/// <typeparam name="T">The type of object that is being lazily initialized.</typeparam>
	// Token: 0x020000ED RID: 237
	[DebuggerTypeProxy(typeof(System_LazyDebugView<>))]
	[ComVisible(false)]
	[DebuggerDisplay("ThreadSafetyMode={Mode}, IsValueCreated={IsValueCreated}, IsValueFaulted={IsValueFaulted}, Value={ValueForDebugDisplay}")]
	[HostProtection(SecurityAction.LinkDemand, Synchronization = true, ExternalThreading = true)]
	[Serializable]
	public class Lazy<T>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`1" /> class. When lazy initialization occurs, the default constructor of the target type is used.</summary>
		// Token: 0x06000936 RID: 2358 RVA: 0x00030C63 File Offset: 0x0002EE63
		public Lazy() : this(LazyThreadSafetyMode.ExecutionAndPublication)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`1" /> class. When lazy initialization occurs, the specified initialization function is used.</summary>
		/// <param name="valueFactory">The delegate that is invoked to produce the lazily initialized value when it is needed.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="valueFactory" /> is <see langword="null" />. </exception>
		// Token: 0x06000937 RID: 2359 RVA: 0x00030C6C File Offset: 0x0002EE6C
		public Lazy(Func<T> valueFactory) : this(valueFactory, LazyThreadSafetyMode.ExecutionAndPublication)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`1" /> class. When lazy initialization occurs, the default constructor of the target type and the specified initialization mode are used.</summary>
		/// <param name="isThreadSafe">
		///       <see langword="true" /> to make this instance usable concurrently by multiple threads; <see langword="false" /> to make the instance usable by only one thread at a time. </param>
		// Token: 0x06000938 RID: 2360 RVA: 0x00030C76 File Offset: 0x0002EE76
		public Lazy(bool isThreadSafe) : this(isThreadSafe ? LazyThreadSafetyMode.ExecutionAndPublication : LazyThreadSafetyMode.None)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`1" /> class that uses the default constructor of <paramref name="T" /> and the specified thread-safety mode.</summary>
		/// <param name="mode">One of the enumeration values that specifies the thread safety mode. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="mode" /> contains an invalid value. </exception>
		// Token: 0x06000939 RID: 2361 RVA: 0x00030C85 File Offset: 0x0002EE85
		public Lazy(LazyThreadSafetyMode mode)
		{
			this.m_threadSafeObj = Lazy<T>.GetObjectFromMode(mode);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`1" /> class. When lazy initialization occurs, the specified initialization function and initialization mode are used.</summary>
		/// <param name="valueFactory">The delegate that is invoked to produce the lazily initialized value when it is needed.</param>
		/// <param name="isThreadSafe">
		///       <see langword="true" /> to make this instance usable concurrently by multiple threads; <see langword="false" /> to make this instance usable by only one thread at a time.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="valueFactory" /> is <see langword="null" />. </exception>
		// Token: 0x0600093A RID: 2362 RVA: 0x00030C99 File Offset: 0x0002EE99
		public Lazy(Func<T> valueFactory, bool isThreadSafe) : this(valueFactory, isThreadSafe ? LazyThreadSafetyMode.ExecutionAndPublication : LazyThreadSafetyMode.None)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Lazy`1" /> class that uses the specified initialization function and thread-safety mode.</summary>
		/// <param name="valueFactory">The delegate that is invoked to produce the lazily initialized value when it is needed.</param>
		/// <param name="mode">One of the enumeration values that specifies the thread safety mode. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="mode" /> contains an invalid value. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="valueFactory" /> is <see langword="null" />. </exception>
		// Token: 0x0600093B RID: 2363 RVA: 0x00030CA9 File Offset: 0x0002EEA9
		public Lazy(Func<T> valueFactory, LazyThreadSafetyMode mode)
		{
			if (valueFactory == null)
			{
				throw new ArgumentNullException("valueFactory");
			}
			this.m_threadSafeObj = Lazy<T>.GetObjectFromMode(mode);
			this.m_valueFactory = valueFactory;
		}

		// Token: 0x0600093C RID: 2364 RVA: 0x00030CD2 File Offset: 0x0002EED2
		private static object GetObjectFromMode(LazyThreadSafetyMode mode)
		{
			if (mode == LazyThreadSafetyMode.ExecutionAndPublication)
			{
				return new object();
			}
			if (mode == LazyThreadSafetyMode.PublicationOnly)
			{
				return LazyHelpers.PUBLICATION_ONLY_SENTINEL;
			}
			if (mode != LazyThreadSafetyMode.None)
			{
				throw new ArgumentOutOfRangeException("mode", Environment.GetResourceString("The mode argument specifies an invalid value."));
			}
			return null;
		}

		// Token: 0x0600093D RID: 2365 RVA: 0x00030D01 File Offset: 0x0002EF01
		[OnSerializing]
		private void OnSerializing(StreamingContext context)
		{
			T value = this.Value;
		}

		/// <summary>Creates and returns a string representation of the <see cref="P:System.Lazy`1.Value" /> property for this instance.</summary>
		/// <returns>The result of calling the <see cref="M:System.Object.ToString" /> method on the <see cref="P:System.Lazy`1.Value" /> property for this instance, if the value has been created (that is, if the <see cref="P:System.Lazy`1.IsValueCreated" /> property returns <see langword="true" />). Otherwise, a string indicating that the value has not been created. </returns>
		/// <exception cref="T:System.NullReferenceException">The <see cref="P:System.Lazy`1.Value" /> property is <see langword="null" />.</exception>
		// Token: 0x0600093E RID: 2366 RVA: 0x00030D0C File Offset: 0x0002EF0C
		public override string ToString()
		{
			if (!this.IsValueCreated)
			{
				return Environment.GetResourceString("Value is not created.");
			}
			T value = this.Value;
			return value.ToString();
		}

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x0600093F RID: 2367 RVA: 0x00030D40 File Offset: 0x0002EF40
		internal T ValueForDebugDisplay
		{
			get
			{
				if (!this.IsValueCreated)
				{
					return default(T);
				}
				return ((Lazy<T>.Boxed)this.m_boxed).m_value;
			}
		}

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x06000940 RID: 2368 RVA: 0x00030D6F File Offset: 0x0002EF6F
		internal LazyThreadSafetyMode Mode
		{
			get
			{
				if (this.m_threadSafeObj == null)
				{
					return LazyThreadSafetyMode.None;
				}
				if (this.m_threadSafeObj == LazyHelpers.PUBLICATION_ONLY_SENTINEL)
				{
					return LazyThreadSafetyMode.PublicationOnly;
				}
				return LazyThreadSafetyMode.ExecutionAndPublication;
			}
		}

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x06000941 RID: 2369 RVA: 0x00030D8B File Offset: 0x0002EF8B
		internal bool IsValueFaulted
		{
			get
			{
				return this.m_boxed is Lazy<T>.LazyInternalExceptionHolder;
			}
		}

		/// <summary>Gets a value that indicates whether a value has been created for this <see cref="T:System.Lazy`1" /> instance.</summary>
		/// <returns>
		///     <see langword="true" /> if a value has been created for this <see cref="T:System.Lazy`1" /> instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000942 RID: 2370 RVA: 0x00030D9B File Offset: 0x0002EF9B
		public bool IsValueCreated
		{
			get
			{
				return this.m_boxed != null && this.m_boxed is Lazy<T>.Boxed;
			}
		}

		/// <summary>Gets the lazily initialized value of the current <see cref="T:System.Lazy`1" /> instance.</summary>
		/// <returns>The lazily initialized value of the current <see cref="T:System.Lazy`1" /> instance.</returns>
		/// <exception cref="T:System.MemberAccessException">The <see cref="T:System.Lazy`1" /> instance is initialized to use the default constructor of the type that is being lazily initialized, and permissions to access the constructor are missing. </exception>
		/// <exception cref="T:System.MissingMemberException">The <see cref="T:System.Lazy`1" /> instance is initialized to use the default constructor of the type that is being lazily initialized, and that type does not have a public, parameterless constructor. </exception>
		/// <exception cref="T:System.InvalidOperationException">The initialization function tries to access <see cref="P:System.Lazy`1.Value" /> on this instance. </exception>
		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000943 RID: 2371 RVA: 0x00030DB8 File Offset: 0x0002EFB8
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public T Value
		{
			get
			{
				if (this.m_boxed != null)
				{
					Lazy<T>.Boxed boxed = this.m_boxed as Lazy<T>.Boxed;
					if (boxed != null)
					{
						return boxed.m_value;
					}
					(this.m_boxed as Lazy<T>.LazyInternalExceptionHolder).m_edi.Throw();
				}
				Debugger.NotifyOfCrossThreadDependency();
				return this.LazyInitValue();
			}
		}

		// Token: 0x06000944 RID: 2372 RVA: 0x00030E08 File Offset: 0x0002F008
		private T LazyInitValue()
		{
			Lazy<T>.Boxed boxed = null;
			LazyThreadSafetyMode mode = this.Mode;
			if (mode == LazyThreadSafetyMode.None)
			{
				boxed = this.CreateValue();
				this.m_boxed = boxed;
			}
			else if (mode == LazyThreadSafetyMode.PublicationOnly)
			{
				boxed = this.CreateValue();
				if (boxed == null || Interlocked.CompareExchange(ref this.m_boxed, boxed, null) != null)
				{
					boxed = (Lazy<T>.Boxed)this.m_boxed;
				}
				else
				{
					this.m_valueFactory = Lazy<T>.ALREADY_INVOKED_SENTINEL;
				}
			}
			else
			{
				object obj = Volatile.Read<object>(ref this.m_threadSafeObj);
				bool flag = false;
				try
				{
					if (obj != Lazy<T>.ALREADY_INVOKED_SENTINEL)
					{
						Monitor.Enter(obj, ref flag);
					}
					if (this.m_boxed == null)
					{
						boxed = this.CreateValue();
						this.m_boxed = boxed;
						Volatile.Write<object>(ref this.m_threadSafeObj, Lazy<T>.ALREADY_INVOKED_SENTINEL);
					}
					else
					{
						boxed = (this.m_boxed as Lazy<T>.Boxed);
						if (boxed == null)
						{
							(this.m_boxed as Lazy<T>.LazyInternalExceptionHolder).m_edi.Throw();
						}
					}
				}
				finally
				{
					if (flag)
					{
						Monitor.Exit(obj);
					}
				}
			}
			return boxed.m_value;
		}

		// Token: 0x06000945 RID: 2373 RVA: 0x00030EFC File Offset: 0x0002F0FC
		private Lazy<T>.Boxed CreateValue()
		{
			Lazy<T>.Boxed result = null;
			LazyThreadSafetyMode mode = this.Mode;
			if (this.m_valueFactory != null)
			{
				try
				{
					if (mode != LazyThreadSafetyMode.PublicationOnly && this.m_valueFactory == Lazy<T>.ALREADY_INVOKED_SENTINEL)
					{
						throw new InvalidOperationException(Environment.GetResourceString("ValueFactory attempted to access the Value property of this instance."));
					}
					Func<T> valueFactory = this.m_valueFactory;
					if (mode != LazyThreadSafetyMode.PublicationOnly)
					{
						this.m_valueFactory = Lazy<T>.ALREADY_INVOKED_SENTINEL;
					}
					else if (valueFactory == Lazy<T>.ALREADY_INVOKED_SENTINEL)
					{
						return null;
					}
					return new Lazy<T>.Boxed(valueFactory());
				}
				catch (Exception ex)
				{
					if (mode != LazyThreadSafetyMode.PublicationOnly)
					{
						this.m_boxed = new Lazy<T>.LazyInternalExceptionHolder(ex);
					}
					throw;
				}
			}
			try
			{
				result = new Lazy<T>.Boxed((T)((object)Activator.CreateInstance(typeof(T))));
			}
			catch (MissingMethodException)
			{
				Exception ex2 = new MissingMemberException(Environment.GetResourceString("The lazily-initialized type does not have a public, parameterless constructor."));
				if (mode != LazyThreadSafetyMode.PublicationOnly)
				{
					this.m_boxed = new Lazy<T>.LazyInternalExceptionHolder(ex2);
				}
				throw ex2;
			}
			return result;
		}

		// Token: 0x06000946 RID: 2374 RVA: 0x00030FF0 File Offset: 0x0002F1F0
		// Note: this type is marked as 'beforefieldinit'.
		static Lazy()
		{
		}

		// Token: 0x040006D9 RID: 1753
		private static readonly Func<T> ALREADY_INVOKED_SENTINEL = () => default(T);

		// Token: 0x040006DA RID: 1754
		private object m_boxed;

		// Token: 0x040006DB RID: 1755
		[NonSerialized]
		private Func<T> m_valueFactory;

		// Token: 0x040006DC RID: 1756
		[NonSerialized]
		private object m_threadSafeObj;

		// Token: 0x020000EE RID: 238
		[Serializable]
		private class Boxed
		{
			// Token: 0x06000947 RID: 2375 RVA: 0x00031007 File Offset: 0x0002F207
			internal Boxed(T value)
			{
				this.m_value = value;
			}

			// Token: 0x040006DD RID: 1757
			internal T m_value;
		}

		// Token: 0x020000EF RID: 239
		private class LazyInternalExceptionHolder
		{
			// Token: 0x06000948 RID: 2376 RVA: 0x00031016 File Offset: 0x0002F216
			internal LazyInternalExceptionHolder(Exception ex)
			{
				this.m_edi = ExceptionDispatchInfo.Capture(ex);
			}

			// Token: 0x040006DE RID: 1758
			internal ExceptionDispatchInfo m_edi;
		}

		// Token: 0x020000F0 RID: 240
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06000949 RID: 2377 RVA: 0x0003102A File Offset: 0x0002F22A
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x0600094A RID: 2378 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x0600094B RID: 2379 RVA: 0x00031038 File Offset: 0x0002F238
			internal T <.cctor>b__27_0()
			{
				return default(T);
			}

			// Token: 0x040006DF RID: 1759
			public static readonly Lazy<T>.<>c <>9 = new Lazy<T>.<>c();
		}
	}
}
