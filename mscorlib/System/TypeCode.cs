﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies the type of an object.</summary>
	// Token: 0x02000231 RID: 561
	[ComVisible(true)]
	[Serializable]
	public enum TypeCode
	{
		/// <summary>A null reference.</summary>
		// Token: 0x04000F0C RID: 3852
		Empty,
		/// <summary>A general type representing any reference or value type not explicitly represented by another <see langword="TypeCode" />.</summary>
		// Token: 0x04000F0D RID: 3853
		Object,
		/// <summary>A database null (column) value.</summary>
		// Token: 0x04000F0E RID: 3854
		DBNull,
		/// <summary>A simple type representing Boolean values of <see langword="true" /> or <see langword="false" />.</summary>
		// Token: 0x04000F0F RID: 3855
		Boolean,
		/// <summary>An integral type representing unsigned 16-bit integers with values between 0 and 65535. The set of possible values for the <see cref="F:System.TypeCode.Char" /> type corresponds to the Unicode character set.</summary>
		// Token: 0x04000F10 RID: 3856
		Char,
		/// <summary>An integral type representing signed 8-bit integers with values between -128 and 127.</summary>
		// Token: 0x04000F11 RID: 3857
		SByte,
		/// <summary>An integral type representing unsigned 8-bit integers with values between 0 and 255.</summary>
		// Token: 0x04000F12 RID: 3858
		Byte,
		/// <summary>An integral type representing signed 16-bit integers with values between -32768 and 32767.</summary>
		// Token: 0x04000F13 RID: 3859
		Int16,
		/// <summary>An integral type representing unsigned 16-bit integers with values between 0 and 65535.</summary>
		// Token: 0x04000F14 RID: 3860
		UInt16,
		/// <summary>An integral type representing signed 32-bit integers with values between -2147483648 and 2147483647.</summary>
		// Token: 0x04000F15 RID: 3861
		Int32,
		/// <summary>An integral type representing unsigned 32-bit integers with values between 0 and 4294967295.</summary>
		// Token: 0x04000F16 RID: 3862
		UInt32,
		/// <summary>An integral type representing signed 64-bit integers with values between -9223372036854775808 and 9223372036854775807.</summary>
		// Token: 0x04000F17 RID: 3863
		Int64,
		/// <summary>An integral type representing unsigned 64-bit integers with values between 0 and 18446744073709551615.</summary>
		// Token: 0x04000F18 RID: 3864
		UInt64,
		/// <summary>A floating point type representing values ranging from approximately 1.5 x 10 -45 to 3.4 x 10 38 with a precision of 7 digits.</summary>
		// Token: 0x04000F19 RID: 3865
		Single,
		/// <summary>A floating point type representing values ranging from approximately 5.0 x 10 -324 to 1.7 x 10 308 with a precision of 15-16 digits.</summary>
		// Token: 0x04000F1A RID: 3866
		Double,
		/// <summary>A simple type representing values ranging from 1.0 x 10 -28 to approximately 7.9 x 10 28 with 28-29 significant digits.</summary>
		// Token: 0x04000F1B RID: 3867
		Decimal,
		/// <summary>A type representing a date and time value.</summary>
		// Token: 0x04000F1C RID: 3868
		DateTime,
		/// <summary>A sealed class type representing Unicode character strings.</summary>
		// Token: 0x04000F1D RID: 3869
		String = 18
	}
}
