﻿using System;

namespace System
{
	/// <summary>Represents the SHIFT, ALT, and CTRL modifier keys on a keyboard.</summary>
	// Token: 0x0200012E RID: 302
	[Flags]
	[Serializable]
	public enum ConsoleModifiers
	{
		/// <summary>The left or right ALT modifier key.</summary>
		// Token: 0x04000847 RID: 2119
		Alt = 1,
		/// <summary>The left or right SHIFT modifier key.</summary>
		// Token: 0x04000848 RID: 2120
		Shift = 2,
		/// <summary>The left or right CTRL modifier key.</summary>
		// Token: 0x04000849 RID: 2121
		Control = 4
	}
}
