﻿using System;

namespace System
{
	// Token: 0x0200024B RID: 587
	internal struct CharInfo
	{
		// Token: 0x04000F5C RID: 3932
		public char Character;

		// Token: 0x04000F5D RID: 3933
		public short Attributes;
	}
}
