﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Provides data for the event that is raised when there is an exception that is not handled in any application domain.</summary>
	// Token: 0x020001D5 RID: 469
	[ComVisible(true)]
	[Serializable]
	public class UnhandledExceptionEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.UnhandledExceptionEventArgs" /> class with the exception object and a common language runtime termination flag.</summary>
		/// <param name="exception">The exception that is not handled. </param>
		/// <param name="isTerminating">
		///       <see langword="true" /> if the runtime is terminating; otherwise, <see langword="false" />. </param>
		// Token: 0x06001622 RID: 5666 RVA: 0x00058641 File Offset: 0x00056841
		public UnhandledExceptionEventArgs(object exception, bool isTerminating)
		{
			this._Exception = exception;
			this._IsTerminating = isTerminating;
		}

		/// <summary>Gets the unhandled exception object.</summary>
		/// <returns>The unhandled exception object.</returns>
		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06001623 RID: 5667 RVA: 0x00058657 File Offset: 0x00056857
		public object ExceptionObject
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this._Exception;
			}
		}

		/// <summary>Indicates whether the common language runtime is terminating.</summary>
		/// <returns>
		///     <see langword="true" /> if the runtime is terminating; otherwise, <see langword="false" />.</returns>
		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06001624 RID: 5668 RVA: 0x0005865F File Offset: 0x0005685F
		public bool IsTerminating
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this._IsTerminating;
			}
		}

		// Token: 0x04000B89 RID: 2953
		private object _Exception;

		// Token: 0x04000B8A RID: 2954
		private bool _IsTerminating;
	}
}
