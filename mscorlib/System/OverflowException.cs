﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when an arithmetic, casting, or conversion operation in a checked context results in an overflow.</summary>
	// Token: 0x0200019B RID: 411
	[ComVisible(true)]
	[Serializable]
	public class OverflowException : ArithmeticException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.OverflowException" /> class.</summary>
		// Token: 0x060011B5 RID: 4533 RVA: 0x000491E4 File Offset: 0x000473E4
		public OverflowException() : base(Environment.GetResourceString("Arithmetic operation resulted in an overflow."))
		{
			base.SetErrorCode(-2146233066);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.OverflowException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x060011B6 RID: 4534 RVA: 0x00049201 File Offset: 0x00047401
		public OverflowException(string message) : base(message)
		{
			base.SetErrorCode(-2146233066);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.OverflowException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x060011B7 RID: 4535 RVA: 0x00049215 File Offset: 0x00047415
		public OverflowException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233066);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.OverflowException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x060011B8 RID: 4536 RVA: 0x0003C430 File Offset: 0x0003A630
		protected OverflowException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
