﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when an attempt to unload an application domain fails.</summary>
	// Token: 0x02000125 RID: 293
	[ComVisible(true)]
	[Serializable]
	public class CannotUnloadAppDomainException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.CannotUnloadAppDomainException" /> class.</summary>
		// Token: 0x06000AF6 RID: 2806 RVA: 0x00034696 File Offset: 0x00032896
		public CannotUnloadAppDomainException() : base(Environment.GetResourceString("Attempt to unload the AppDomain failed."))
		{
			base.SetErrorCode(-2146234347);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CannotUnloadAppDomainException" /> class with a specified error message.</summary>
		/// <param name="message">A <see cref="T:System.String" /> that describes the error. </param>
		// Token: 0x06000AF7 RID: 2807 RVA: 0x000346B3 File Offset: 0x000328B3
		public CannotUnloadAppDomainException(string message) : base(message)
		{
			base.SetErrorCode(-2146234347);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CannotUnloadAppDomainException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000AF8 RID: 2808 RVA: 0x000346C7 File Offset: 0x000328C7
		public CannotUnloadAppDomainException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146234347);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.CannotUnloadAppDomainException" /> class from serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06000AF9 RID: 2809 RVA: 0x000319C9 File Offset: 0x0002FBC9
		protected CannotUnloadAppDomainException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
