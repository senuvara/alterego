﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>The exception that is thrown when one of the arguments provided to a method is not valid.</summary>
	// Token: 0x02000115 RID: 277
	[ComVisible(true)]
	[Serializable]
	public class ArgumentException : SystemException, ISerializable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentException" /> class.</summary>
		// Token: 0x06000A04 RID: 2564 RVA: 0x000322A8 File Offset: 0x000304A8
		public ArgumentException() : base(Environment.GetResourceString("Value does not fall within the expected range."))
		{
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x06000A05 RID: 2565 RVA: 0x000322C5 File Offset: 0x000304C5
		public ArgumentException(string message) : base(message)
		{
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000A06 RID: 2566 RVA: 0x000322D9 File Offset: 0x000304D9
		public ArgumentException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentException" /> class with a specified error message, the parameter name, and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="paramName">The name of the parameter that caused the current exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000A07 RID: 2567 RVA: 0x000322EE File Offset: 0x000304EE
		public ArgumentException(string message, string paramName, Exception innerException) : base(message, innerException)
		{
			this.m_paramName = paramName;
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentException" /> class with a specified error message and the name of the parameter that causes this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="paramName">The name of the parameter that caused the current exception. </param>
		// Token: 0x06000A08 RID: 2568 RVA: 0x0003230A File Offset: 0x0003050A
		public ArgumentException(string message, string paramName) : base(message)
		{
			this.m_paramName = paramName;
			base.SetErrorCode(-2147024809);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		// Token: 0x06000A09 RID: 2569 RVA: 0x00032325 File Offset: 0x00030525
		protected ArgumentException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.m_paramName = info.GetString("ParamName");
		}

		/// <summary>Gets the error message and the parameter name, or only the error message if no parameter name is set.</summary>
		/// <returns>A text string describing the details of the exception. The value of this property takes one of two forms: Condition Value The <paramref name="paramName" /> is a null reference (<see langword="Nothing" /> in Visual Basic) or of zero length. The <paramref name="message" /> string passed to the constructor. The <paramref name="paramName" /> is not null reference (<see langword="Nothing" /> in Visual Basic) and it has a length greater than zero. The <paramref name="message" /> string appended with the name of the invalid parameter. </returns>
		// Token: 0x1700019B RID: 411
		// (get) Token: 0x06000A0A RID: 2570 RVA: 0x00032340 File Offset: 0x00030540
		public override string Message
		{
			get
			{
				string message = base.Message;
				if (!string.IsNullOrEmpty(this.m_paramName))
				{
					string resourceString = Environment.GetResourceString("Parameter name: {0}", new object[]
					{
						this.m_paramName
					});
					return message + Environment.NewLine + resourceString;
				}
				return message;
			}
		}

		/// <summary>Gets the name of the parameter that causes this exception.</summary>
		/// <returns>The parameter name.</returns>
		// Token: 0x1700019C RID: 412
		// (get) Token: 0x06000A0B RID: 2571 RVA: 0x00032389 File Offset: 0x00030589
		public virtual string ParamName
		{
			get
			{
				return this.m_paramName;
			}
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the parameter name and additional exception information.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> object is a null reference (<see langword="Nothing" /> in Visual Basic). </exception>
		// Token: 0x06000A0C RID: 2572 RVA: 0x00032391 File Offset: 0x00030591
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
			info.AddValue("ParamName", this.m_paramName, typeof(string));
		}

		// Token: 0x04000766 RID: 1894
		private string m_paramName;
	}
}
