﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Assemblies
{
	/// <summary>Represents a hash of an assembly manifest's contents.</summary>
	// Token: 0x02000254 RID: 596
	[Obsolete]
	[ComVisible(true)]
	[Serializable]
	public struct AssemblyHash : ICloneable
	{
		/// <summary>Gets or sets the hash algorithm.</summary>
		/// <returns>An assembly hash algorithm.</returns>
		// Token: 0x170003C7 RID: 967
		// (get) Token: 0x06001BA8 RID: 7080 RVA: 0x00068299 File Offset: 0x00066499
		// (set) Token: 0x06001BA9 RID: 7081 RVA: 0x000682A1 File Offset: 0x000664A1
		[Obsolete]
		public AssemblyHashAlgorithm Algorithm
		{
			get
			{
				return this._algorithm;
			}
			set
			{
				this._algorithm = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.Assemblies.AssemblyHash" /> structure with the specified hash algorithm and the hash value.</summary>
		/// <param name="algorithm">The algorithm used to generate the hash. Values for this parameter come from the <see cref="T:System.Configuration.Assemblies.AssemblyHashAlgorithm" /> enumeration. </param>
		/// <param name="value">The hash value. </param>
		// Token: 0x06001BAA RID: 7082 RVA: 0x000682AA File Offset: 0x000664AA
		[Obsolete]
		public AssemblyHash(AssemblyHashAlgorithm algorithm, byte[] value)
		{
			this._algorithm = algorithm;
			if (value != null)
			{
				this._value = (byte[])value.Clone();
				return;
			}
			this._value = null;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Configuration.Assemblies.AssemblyHash" /> structure with the specified hash value. The hash algorithm defaults to <see cref="F:System.Configuration.Assemblies.AssemblyHashAlgorithm.SHA1" />.</summary>
		/// <param name="value">The hash value. </param>
		// Token: 0x06001BAB RID: 7083 RVA: 0x000682CF File Offset: 0x000664CF
		[Obsolete]
		public AssemblyHash(byte[] value)
		{
			this = new AssemblyHash(AssemblyHashAlgorithm.SHA1, value);
		}

		/// <summary>Clones this object.</summary>
		/// <returns>An exact copy of this object.</returns>
		// Token: 0x06001BAC RID: 7084 RVA: 0x000682DD File Offset: 0x000664DD
		[Obsolete]
		public object Clone()
		{
			return new AssemblyHash(this._algorithm, this._value);
		}

		/// <summary>Gets the hash value.</summary>
		/// <returns>The hash value.</returns>
		// Token: 0x06001BAD RID: 7085 RVA: 0x000682F5 File Offset: 0x000664F5
		[Obsolete]
		public byte[] GetValue()
		{
			return this._value;
		}

		/// <summary>Sets the hash value.</summary>
		/// <param name="value">The hash value. </param>
		// Token: 0x06001BAE RID: 7086 RVA: 0x000682FD File Offset: 0x000664FD
		[Obsolete]
		public void SetValue(byte[] value)
		{
			this._value = value;
		}

		// Token: 0x06001BAF RID: 7087 RVA: 0x00068306 File Offset: 0x00066506
		// Note: this type is marked as 'beforefieldinit'.
		static AssemblyHash()
		{
		}

		// Token: 0x04000F70 RID: 3952
		private AssemblyHashAlgorithm _algorithm;

		// Token: 0x04000F71 RID: 3953
		private byte[] _value;

		/// <summary>An empty <see cref="T:System.Configuration.Assemblies.AssemblyHash" /> object.</summary>
		// Token: 0x04000F72 RID: 3954
		[Obsolete]
		public static readonly AssemblyHash Empty = new AssemblyHash(AssemblyHashAlgorithm.None, null);
	}
}
