﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace System
{
	/// <summary>Describes objects that contain both a managed pointer to a location and a runtime representation of the type that may be stored at that location.</summary>
	// Token: 0x020001CD RID: 461
	[ComVisible(true)]
	[CLSCompliant(false)]
	public struct TypedReference
	{
		/// <summary>Makes a <see langword="TypedReference" /> for a field identified by a specified object and list of field descriptions.</summary>
		/// <param name="target">An object that contains the field described by the first element of <paramref name="flds" />. </param>
		/// <param name="flds">A list of field descriptions where each element describes a field that contains the field described by the succeeding element. Each described field must be a value type. The field descriptions must be <see langword="RuntimeFieldInfo" /> objects supplied by the type system.</param>
		/// <returns>A <see cref="T:System.TypedReference" /> for the field described by the last element of <paramref name="flds" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="target" /> or <paramref name="flds" /> is <see langword="null" />.-or- An element of <paramref name="flds" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="flds" /> array has no elements.-or- An element of <paramref name="flds" /> is not a <see langword="RuntimeFieldInfo" /> object. -or- The <see cref="P:System.Reflection.FieldInfo.IsInitOnly" /> or <see cref="P:System.Reflection.FieldInfo.IsStatic" /> property of an element of <paramref name="flds" /> is <see langword="true" />. </exception>
		/// <exception cref="T:System.MissingMemberException">Parameter <paramref name="target" /> does not contain the field described by the first element of <paramref name="flds" />, or an element of <paramref name="flds" /> describes a field that is not contained in the field described by the succeeding element of <paramref name="flds" />.-or- The field described by an element of <paramref name="flds" /> is not a value type. </exception>
		// Token: 0x060015A1 RID: 5537 RVA: 0x00057B74 File Offset: 0x00055D74
		[CLSCompliant(false)]
		[SecurityCritical]
		public static TypedReference MakeTypedReference(object target, FieldInfo[] flds)
		{
			if (target == null)
			{
				throw new ArgumentNullException("target");
			}
			if (flds == null)
			{
				throw new ArgumentNullException("flds");
			}
			if (flds.Length == 0)
			{
				throw new ArgumentException(Environment.GetResourceString("Array must not be of length zero."));
			}
			IntPtr[] array = new IntPtr[flds.Length];
			RuntimeType runtimeType = (RuntimeType)target.GetType();
			for (int i = 0; i < flds.Length; i++)
			{
				RuntimeFieldInfo runtimeFieldInfo = flds[i] as RuntimeFieldInfo;
				if (runtimeFieldInfo == null)
				{
					throw new ArgumentException(Environment.GetResourceString("FieldInfo must be a runtime FieldInfo object."));
				}
				if (runtimeFieldInfo.IsInitOnly || runtimeFieldInfo.IsStatic)
				{
					throw new ArgumentException(Environment.GetResourceString("Field in TypedReferences cannot be static or init only."));
				}
				if (runtimeType != runtimeFieldInfo.GetDeclaringTypeInternal() && !runtimeType.IsSubclassOf(runtimeFieldInfo.GetDeclaringTypeInternal()))
				{
					throw new MissingMemberException(Environment.GetResourceString("FieldInfo does not match the target Type."));
				}
				RuntimeType runtimeType2 = (RuntimeType)runtimeFieldInfo.FieldType;
				if (runtimeType2.IsPrimitive)
				{
					throw new ArgumentException(Environment.GetResourceString("TypedReferences cannot be redefined as primitives."));
				}
				if (i < flds.Length - 1 && !runtimeType2.IsValueType)
				{
					throw new MissingMemberException(Environment.GetResourceString("TypedReference can only be made on nested value Types."));
				}
				array[i] = runtimeFieldInfo.FieldHandle.Value;
				runtimeType = runtimeType2;
			}
			return TypedReference.MakeTypedReferenceInternal(target, flds);
		}

		// Token: 0x060015A2 RID: 5538
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern TypedReference MakeTypedReferenceInternal(object target, FieldInfo[] fields);

		/// <summary>Returns the hash code of this object.</summary>
		/// <returns>The hash code of this object.</returns>
		// Token: 0x060015A3 RID: 5539 RVA: 0x00057CAB File Offset: 0x00055EAB
		public override int GetHashCode()
		{
			if (this.Type == IntPtr.Zero)
			{
				return 0;
			}
			return __reftype(this).GetHashCode();
		}

		/// <summary>Checks if this object is equal to the specified object.</summary>
		/// <param name="o">The object with which to compare the current object. </param>
		/// <returns>
		///     <see langword="true" /> if this object is equal to the specified object; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented. </exception>
		// Token: 0x060015A4 RID: 5540 RVA: 0x00057CD3 File Offset: 0x00055ED3
		public override bool Equals(object o)
		{
			throw new NotSupportedException(Environment.GetResourceString("This feature is not currently implemented."));
		}

		/// <summary>Converts the specified <see langword="TypedReference" /> to an <see langword="Object" />.</summary>
		/// <param name="value">The <see langword="TypedReference" /> to be converted. </param>
		/// <returns>An <see cref="T:System.Object" /> converted from a <see langword="TypedReference" />.</returns>
		// Token: 0x060015A5 RID: 5541 RVA: 0x00057CE4 File Offset: 0x00055EE4
		[SecuritySafeCritical]
		public unsafe static object ToObject(TypedReference value)
		{
			return TypedReference.InternalToObject((void*)(&value));
		}

		// Token: 0x060015A6 RID: 5542
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal unsafe static extern object InternalToObject(void* value);

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x060015A7 RID: 5543 RVA: 0x00057CEE File Offset: 0x00055EEE
		internal bool IsNull
		{
			get
			{
				return this.Value.IsNull() && this.Type.IsNull();
			}
		}

		/// <summary>Returns the type of the target of the specified <see langword="TypedReference" />.</summary>
		/// <param name="value">The value whose target's type is to be returned. </param>
		/// <returns>The type of the target of the specified <see langword="TypedReference" />.</returns>
		// Token: 0x060015A8 RID: 5544 RVA: 0x00057D0A File Offset: 0x00055F0A
		public static Type GetTargetType(TypedReference value)
		{
			return __reftype(value);
		}

		/// <summary>Returns the internal metadata type handle for the specified <see langword="TypedReference" />.</summary>
		/// <param name="value">The <see langword="TypedReference" /> for which the type handle is requested. </param>
		/// <returns>The internal metadata type handle for the specified <see langword="TypedReference" />.</returns>
		// Token: 0x060015A9 RID: 5545 RVA: 0x00057D14 File Offset: 0x00055F14
		public static RuntimeTypeHandle TargetTypeToken(TypedReference value)
		{
			return __reftype(value).TypeHandle;
		}

		/// <summary>Converts the specified value to a <see langword="TypedReference" />. This method is not supported.</summary>
		/// <param name="target">The target of the conversion. </param>
		/// <param name="value">The value to be converted. </param>
		/// <exception cref="T:System.NotSupportedException">In all cases. </exception>
		// Token: 0x060015AA RID: 5546 RVA: 0x00057D23 File Offset: 0x00055F23
		[CLSCompliant(false)]
		[SecuritySafeCritical]
		public static void SetTypedReference(TypedReference target, object value)
		{
			throw new NotImplementedException("SetTypedReference");
		}

		// Token: 0x04000B78 RID: 2936
		private RuntimeTypeHandle type;

		// Token: 0x04000B79 RID: 2937
		private IntPtr Value;

		// Token: 0x04000B7A RID: 2938
		private IntPtr Type;
	}
}
