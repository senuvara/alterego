﻿using System;
using System.Security;

namespace System
{
	// Token: 0x02000163 RID: 355
	internal struct DateTimeRawInfo
	{
		// Token: 0x06000FB6 RID: 4022 RVA: 0x00044E8F File Offset: 0x0004308F
		[SecurityCritical]
		internal unsafe void Init(int* numberBuffer)
		{
			this.month = -1;
			this.year = -1;
			this.dayOfWeek = -1;
			this.era = -1;
			this.timeMark = DateTimeParse.TM.NotSet;
			this.fraction = -1.0;
			this.num = numberBuffer;
		}

		// Token: 0x06000FB7 RID: 4023 RVA: 0x00044ECC File Offset: 0x000430CC
		[SecuritySafeCritical]
		internal unsafe void AddNumber(int value)
		{
			ref int ptr = ref *this.num;
			int num = this.numCount;
			this.numCount = num + 1;
			*(ref ptr + (IntPtr)num * 4) = value;
		}

		// Token: 0x06000FB8 RID: 4024 RVA: 0x00044EF6 File Offset: 0x000430F6
		[SecuritySafeCritical]
		internal unsafe int GetNumber(int index)
		{
			return this.num[index];
		}

		// Token: 0x04000966 RID: 2406
		[SecurityCritical]
		private unsafe int* num;

		// Token: 0x04000967 RID: 2407
		internal int numCount;

		// Token: 0x04000968 RID: 2408
		internal int month;

		// Token: 0x04000969 RID: 2409
		internal int year;

		// Token: 0x0400096A RID: 2410
		internal int dayOfWeek;

		// Token: 0x0400096B RID: 2411
		internal int era;

		// Token: 0x0400096C RID: 2412
		internal DateTimeParse.TM timeMark;

		// Token: 0x0400096D RID: 2413
		internal double fraction;

		// Token: 0x0400096E RID: 2414
		internal bool hasSameDateAndTimeSeparators;

		// Token: 0x0400096F RID: 2415
		internal bool timeZone;
	}
}
