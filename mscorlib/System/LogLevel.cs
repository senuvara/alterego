﻿using System;

namespace System
{
	// Token: 0x020001DF RID: 479
	internal enum LogLevel
	{
		// Token: 0x04000BD3 RID: 3027
		Trace,
		// Token: 0x04000BD4 RID: 3028
		Status = 20,
		// Token: 0x04000BD5 RID: 3029
		Warning = 40,
		// Token: 0x04000BD6 RID: 3030
		Error = 50,
		// Token: 0x04000BD7 RID: 3031
		Panic = 100
	}
}
