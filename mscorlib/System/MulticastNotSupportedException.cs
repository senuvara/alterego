﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when there is an attempt to combine two delegates based on the <see cref="T:System.Delegate" /> type instead of the <see cref="T:System.MulticastDelegate" /> type. This class cannot be inherited. </summary>
	// Token: 0x0200018F RID: 399
	[ComVisible(true)]
	[Serializable]
	public sealed class MulticastNotSupportedException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.MulticastNotSupportedException" /> class.</summary>
		// Token: 0x06001153 RID: 4435 RVA: 0x00047D29 File Offset: 0x00045F29
		public MulticastNotSupportedException() : base(Environment.GetResourceString("Attempted to add multiple callbacks to a delegate that does not support multicast."))
		{
			base.SetErrorCode(-2146233068);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MulticastNotSupportedException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06001154 RID: 4436 RVA: 0x00047D46 File Offset: 0x00045F46
		public MulticastNotSupportedException(string message) : base(message)
		{
			base.SetErrorCode(-2146233068);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.MulticastNotSupportedException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06001155 RID: 4437 RVA: 0x00047D5A File Offset: 0x00045F5A
		public MulticastNotSupportedException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233068);
		}

		// Token: 0x06001156 RID: 4438 RVA: 0x000319C9 File Offset: 0x0002FBC9
		internal MulticastNotSupportedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
