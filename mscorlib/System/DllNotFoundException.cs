﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when a DLL specified in a DLL import cannot be found.</summary>
	// Token: 0x02000140 RID: 320
	[ComVisible(true)]
	[Serializable]
	public class DllNotFoundException : TypeLoadException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.DllNotFoundException" /> class with default properties.</summary>
		// Token: 0x06000E31 RID: 3633 RVA: 0x0003C43A File Offset: 0x0003A63A
		public DllNotFoundException() : base(Environment.GetResourceString("Dll was not found."))
		{
			base.SetErrorCode(-2146233052);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DllNotFoundException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x06000E32 RID: 3634 RVA: 0x0003C457 File Offset: 0x0003A657
		public DllNotFoundException(string message) : base(message)
		{
			base.SetErrorCode(-2146233052);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DllNotFoundException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06000E33 RID: 3635 RVA: 0x0003C46B File Offset: 0x0003A66B
		public DllNotFoundException(string message, Exception inner) : base(message, inner)
		{
			base.SetErrorCode(-2146233052);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DllNotFoundException" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		// Token: 0x06000E34 RID: 3636 RVA: 0x0003C480 File Offset: 0x0003A680
		protected DllNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
