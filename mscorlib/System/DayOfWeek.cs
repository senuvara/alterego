﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies the day of the week.</summary>
	// Token: 0x02000139 RID: 313
	[ComVisible(true)]
	[Serializable]
	public enum DayOfWeek
	{
		/// <summary>Indicates Sunday.</summary>
		// Token: 0x04000890 RID: 2192
		Sunday,
		/// <summary>Indicates Monday.</summary>
		// Token: 0x04000891 RID: 2193
		Monday,
		/// <summary>Indicates Tuesday.</summary>
		// Token: 0x04000892 RID: 2194
		Tuesday,
		/// <summary>Indicates Wednesday.</summary>
		// Token: 0x04000893 RID: 2195
		Wednesday,
		/// <summary>Indicates Thursday.</summary>
		// Token: 0x04000894 RID: 2196
		Thursday,
		/// <summary>Indicates Friday.</summary>
		// Token: 0x04000895 RID: 2197
		Friday,
		/// <summary>Indicates Saturday.</summary>
		// Token: 0x04000896 RID: 2198
		Saturday
	}
}
