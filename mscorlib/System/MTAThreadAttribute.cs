﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates that the COM threading model for an application is multithreaded apartment (MTA). </summary>
	// Token: 0x020001B7 RID: 439
	[AttributeUsage(AttributeTargets.Method)]
	[ComVisible(true)]
	public sealed class MTAThreadAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.MTAThreadAttribute" /> class.</summary>
		// Token: 0x060013E2 RID: 5090 RVA: 0x000020BF File Offset: 0x000002BF
		public MTAThreadAttribute()
		{
		}
	}
}
