﻿using System;

namespace System
{
	// Token: 0x02000164 RID: 356
	internal enum ParseFailureKind
	{
		// Token: 0x04000971 RID: 2417
		None,
		// Token: 0x04000972 RID: 2418
		ArgumentNull,
		// Token: 0x04000973 RID: 2419
		Format,
		// Token: 0x04000974 RID: 2420
		FormatWithParameter,
		// Token: 0x04000975 RID: 2421
		FormatBadDateTimeCalendar
	}
}
