﻿using System;

namespace System
{
	// Token: 0x02000234 RID: 564
	internal class TypeNames
	{
		// Token: 0x06001AD8 RID: 6872 RVA: 0x00065D2C File Offset: 0x00063F2C
		internal static TypeName FromDisplay(string displayName)
		{
			return new TypeNames.Display(displayName);
		}

		// Token: 0x06001AD9 RID: 6873 RVA: 0x00002050 File Offset: 0x00000250
		public TypeNames()
		{
		}

		// Token: 0x02000235 RID: 565
		internal abstract class ATypeName : TypeName, IEquatable<TypeName>
		{
			// Token: 0x1700039C RID: 924
			// (get) Token: 0x06001ADA RID: 6874
			public abstract string DisplayName { get; }

			// Token: 0x06001ADB RID: 6875
			public abstract TypeName NestedName(TypeIdentifier innerName);

			// Token: 0x06001ADC RID: 6876 RVA: 0x00065D34 File Offset: 0x00063F34
			public bool Equals(TypeName other)
			{
				return other != null && this.DisplayName == other.DisplayName;
			}

			// Token: 0x06001ADD RID: 6877 RVA: 0x00065D4C File Offset: 0x00063F4C
			public override int GetHashCode()
			{
				return this.DisplayName.GetHashCode();
			}

			// Token: 0x06001ADE RID: 6878 RVA: 0x00065D59 File Offset: 0x00063F59
			public override bool Equals(object other)
			{
				return this.Equals(other as TypeName);
			}

			// Token: 0x06001ADF RID: 6879 RVA: 0x00002050 File Offset: 0x00000250
			protected ATypeName()
			{
			}
		}

		// Token: 0x02000236 RID: 566
		private class Display : TypeNames.ATypeName
		{
			// Token: 0x06001AE0 RID: 6880 RVA: 0x00065D67 File Offset: 0x00063F67
			internal Display(string displayName)
			{
				this.displayName = displayName;
			}

			// Token: 0x1700039D RID: 925
			// (get) Token: 0x06001AE1 RID: 6881 RVA: 0x00065D76 File Offset: 0x00063F76
			public override string DisplayName
			{
				get
				{
					return this.displayName;
				}
			}

			// Token: 0x06001AE2 RID: 6882 RVA: 0x00065D7E File Offset: 0x00063F7E
			public override TypeName NestedName(TypeIdentifier innerName)
			{
				return new TypeNames.Display(this.DisplayName + "+" + innerName.DisplayName);
			}

			// Token: 0x04000F1E RID: 3870
			private string displayName;
		}
	}
}
