﻿using System;
using System.Reflection;
using System.Runtime.Hosting;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Threading;

namespace System
{
	/// <summary>Provides a managed equivalent of an unmanaged host.</summary>
	/// <exception cref="T:System.Security.SecurityException">The caller does not have the correct permissions. See the Requirements section.</exception>
	// Token: 0x020001EE RID: 494
	[ComVisible(true)]
	[SecurityPermission(SecurityAction.LinkDemand, Infrastructure = true)]
	[SecurityPermission(SecurityAction.InheritanceDemand, Infrastructure = true)]
	public class AppDomainManager : MarshalByRefObject
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.AppDomainManager" /> class. </summary>
		// Token: 0x06001780 RID: 6016 RVA: 0x0005BD8C File Offset: 0x00059F8C
		public AppDomainManager()
		{
			this._flags = AppDomainManagerInitializationOptions.None;
		}

		/// <summary>Gets the application activator that handles the activation of add-ins and manifest-based applications for the domain.</summary>
		/// <returns>The application activator.</returns>
		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06001781 RID: 6017 RVA: 0x0005BD9B File Offset: 0x00059F9B
		public virtual ApplicationActivator ApplicationActivator
		{
			get
			{
				if (this._activator == null)
				{
					this._activator = new ApplicationActivator();
				}
				return this._activator;
			}
		}

		/// <summary>Gets the entry assembly for an application.</summary>
		/// <returns>The entry assembly for the application.</returns>
		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x06001782 RID: 6018 RVA: 0x0005BDB6 File Offset: 0x00059FB6
		public virtual Assembly EntryAssembly
		{
			get
			{
				return Assembly.GetEntryAssembly();
			}
		}

		/// <summary>Gets the host execution context manager that manages the flow of the execution context.</summary>
		/// <returns>The host execution context manager.</returns>
		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06001783 RID: 6019 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public virtual HostExecutionContextManager HostExecutionContextManager
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the host security manager that participates in security decisions for the application domain.</summary>
		/// <returns>The host security manager.</returns>
		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06001784 RID: 6020 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public virtual HostSecurityManager HostSecurityManager
		{
			get
			{
				return null;
			}
		}

		/// <summary>Gets the initialization flags for custom application domain managers.</summary>
		/// <returns>A bitwise combination of the enumeration values that describe the initialization action to perform. The default is <see cref="F:System.AppDomainManagerInitializationOptions.None" />.</returns>
		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06001785 RID: 6021 RVA: 0x0005BDBD File Offset: 0x00059FBD
		// (set) Token: 0x06001786 RID: 6022 RVA: 0x0005BDC5 File Offset: 0x00059FC5
		public AppDomainManagerInitializationOptions InitializationFlags
		{
			get
			{
				return this._flags;
			}
			set
			{
				this._flags = value;
			}
		}

		/// <summary>Returns a new or existing application domain.</summary>
		/// <param name="friendlyName">The friendly name of the domain. </param>
		/// <param name="securityInfo">An object that contains evidence mapped through the security policy to establish a top-of-stack permission set.</param>
		/// <param name="appDomainInfo">An object that contains application domain initialization information.</param>
		/// <returns>A new or existing application domain.</returns>
		// Token: 0x06001787 RID: 6023 RVA: 0x0005BDD0 File Offset: 0x00059FD0
		public virtual AppDomain CreateDomain(string friendlyName, Evidence securityInfo, AppDomainSetup appDomainInfo)
		{
			this.InitializeNewDomain(appDomainInfo);
			AppDomain appDomain = AppDomainManager.CreateDomainHelper(friendlyName, securityInfo, appDomainInfo);
			if ((this.HostSecurityManager.Flags & HostSecurityManagerOptions.HostPolicyLevel) == HostSecurityManagerOptions.HostPolicyLevel)
			{
				PolicyLevel domainPolicy = this.HostSecurityManager.DomainPolicy;
				if (domainPolicy != null)
				{
					appDomain.SetAppDomainPolicy(domainPolicy);
				}
			}
			return appDomain;
		}

		/// <summary>Initializes the new application domain.</summary>
		/// <param name="appDomainInfo">An object that contains application domain initialization information.</param>
		// Token: 0x06001788 RID: 6024 RVA: 0x000020D3 File Offset: 0x000002D3
		public virtual void InitializeNewDomain(AppDomainSetup appDomainInfo)
		{
		}

		/// <summary>Indicates whether the specified operation is allowed in the application domain.</summary>
		/// <param name="state">A subclass of <see cref="T:System.Security.SecurityState" /> that identifies the operation whose security status is requested.</param>
		/// <returns>
		///     <see langword="true" /> if the host allows the operation specified by <paramref name="state" /> to be performed in the application domain; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001789 RID: 6025 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool CheckSecuritySettings(SecurityState state)
		{
			return false;
		}

		/// <summary>Provides a helper method to create an application domain.</summary>
		/// <param name="friendlyName">The friendly name of the domain. </param>
		/// <param name="securityInfo">An object that contains evidence mapped through the security policy to establish a top-of-stack permission set.</param>
		/// <param name="appDomainInfo">An object that contains application domain initialization information.</param>
		/// <returns>A newly created application domain.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="friendlyName" /> is <see langword="null" />. </exception>
		// Token: 0x0600178A RID: 6026 RVA: 0x0005BE14 File Offset: 0x0005A014
		protected static AppDomain CreateDomainHelper(string friendlyName, Evidence securityInfo, AppDomainSetup appDomainInfo)
		{
			return AppDomain.CreateDomain(friendlyName, securityInfo, appDomainInfo);
		}

		// Token: 0x04000C23 RID: 3107
		private ApplicationActivator _activator;

		// Token: 0x04000C24 RID: 3108
		private AppDomainManagerInitializationOptions _flags;
	}
}
