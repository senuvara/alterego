﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents a value tuple with 3 components. </summary>
	/// <typeparam name="T1">The type of the value tuple's first element. </typeparam>
	/// <typeparam name="T2">The type of the value tuple's second element. </typeparam>
	/// <typeparam name="T3">The type of the value tuple's third element. </typeparam>
	// Token: 0x020000CA RID: 202
	[Serializable]
	[StructLayout(LayoutKind.Auto)]
	public struct ValueTuple<T1, T2, T3> : IEquatable<ValueTuple<T1, T2, T3>>, IStructuralEquatable, IStructuralComparable, IComparable, IComparable<ValueTuple<T1, T2, T3>>, IValueTupleInternal, ITuple
	{
		/// <summary>Initializes a new <see cref="T:System.ValueTuple`3" /> instance. </summary>
		/// <param name="item1">The value tuple's first element. </param>
		/// <param name="item2">The value tuple's second element. </param>
		/// <param name="item3">The value tuple's third element. </param>
		// Token: 0x06000756 RID: 1878 RVA: 0x00027B1E File Offset: 0x00025D1E
		public ValueTuple(T1 item1, T2 item2, T3 item3)
		{
			this.Item1 = item1;
			this.Item2 = item2;
			this.Item3 = item3;
		}

		/// <summary>Returns a value that indicates whether the current <see cref="T:System.ValueTuple`3" /> instance is equal to a specified object.</summary>
		/// <param name="obj">The object to compare with this instance.</param>
		/// <returns>
		///     <see langword="true" /> if the current instance is equal to the specified object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000757 RID: 1879 RVA: 0x00027B35 File Offset: 0x00025D35
		public override bool Equals(object obj)
		{
			return obj is ValueTuple<T1, T2, T3> && this.Equals((ValueTuple<T1, T2, T3>)obj);
		}

		/// <summary>Returns a value that indicates whether the current <see cref="T:System.ValueTuple`3" /> instance is equal to a specified <see cref="T:System.ValueTuple`3" /> instance.</summary>
		/// <param name="other">The value tuple to compare with this instance.</param>
		/// <returns>
		///     <see langword="true" /> if the current instance is equal to the specified tuple; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000758 RID: 1880 RVA: 0x00027B50 File Offset: 0x00025D50
		public bool Equals(ValueTuple<T1, T2, T3> other)
		{
			return EqualityComparer<T1>.Default.Equals(this.Item1, other.Item1) && EqualityComparer<T2>.Default.Equals(this.Item2, other.Item2) && EqualityComparer<T3>.Default.Equals(this.Item3, other.Item3);
		}

		/// <summary>Returns a value that indicates whether the current <see cref="T:System.ValueTuple`3" /> instance is equal to a specified object based on a specified comparison method. </summary>
		/// <param name="other">The object to compare with this instance. </param>
		/// <param name="comparer">An object that defines the method to use to evaluate whether the two objects are equal. </param>
		/// <returns>
		///     <see langword="true" /> if the current instance is equal to the specified objects; otherwise, <see langword="false" />. </returns>
		// Token: 0x06000759 RID: 1881 RVA: 0x00027BA8 File Offset: 0x00025DA8
		bool IStructuralEquatable.Equals(object other, IEqualityComparer comparer)
		{
			if (other == null || !(other is ValueTuple<T1, T2, T3>))
			{
				return false;
			}
			ValueTuple<T1, T2, T3> valueTuple = (ValueTuple<T1, T2, T3>)other;
			return comparer.Equals(this.Item1, valueTuple.Item1) && comparer.Equals(this.Item2, valueTuple.Item2) && comparer.Equals(this.Item3, valueTuple.Item3);
		}

		/// <summary>Compares the current <see cref="T:System.ValueTuple`3" /> instance to a specified object by using a specified comparer and returns an integer that indicates whether the current object is before, after, or in the same position as the specified object in the sort order. </summary>
		/// <param name="other">The object to compare with the current instance. </param>
		/// <returns>A signed integer that indicates the relative position of this instance and <paramref name="obj" /> in the sort order, as shown in the following table.ValueDescriptionA negative integerThis instance precedes <paramref name="other" />.ZeroThis instance and <paramref name="other" /> have the same position in the sort order.A positive integerThis instance follows <paramref name="other" />.</returns>
		// Token: 0x0600075A RID: 1882 RVA: 0x00027C24 File Offset: 0x00025E24
		int IComparable.CompareTo(object other)
		{
			if (other == null)
			{
				return 1;
			}
			if (!(other is ValueTuple<T1, T2, T3>))
			{
				throw new ArgumentException(SR.Format("Argument must be of type {0}.", base.GetType().ToString()), "other");
			}
			return this.CompareTo((ValueTuple<T1, T2, T3>)other);
		}

		/// <summary>Compares the current <see cref="T:System.ValueTuple`3" /> instance to a specified <see cref="T:System.ValueTuple`3" /> instance.</summary>
		/// <param name="other">The tuple to compare with this instance.</param>
		/// <returns>A signed integer that indicates the relative position of this instance and 
		///               <paramref name="other" /> in the sort order, as shown in the following able. 
		///           VaueDescriptionA negative integerThis instance precedes 
		///                               <paramref name="other" />. 
		///                           ZeroThis instance and 
		///                               <paramref name="other" /> have the same position in the sort order.
		///                           A positive integerThis instance follows 
		///                               <paramref name="other" />. 
		///                           </returns>
		// Token: 0x0600075B RID: 1883 RVA: 0x00027C74 File Offset: 0x00025E74
		public int CompareTo(ValueTuple<T1, T2, T3> other)
		{
			int num = Comparer<T1>.Default.Compare(this.Item1, other.Item1);
			if (num != 0)
			{
				return num;
			}
			num = Comparer<T2>.Default.Compare(this.Item2, other.Item2);
			if (num != 0)
			{
				return num;
			}
			return Comparer<T3>.Default.Compare(this.Item3, other.Item3);
		}

		/// <summary>Compares the current <see cref="T:System.ValueTuple`3" /> instance to a specified object by using a specified comparer and returns an integer that indicates whether the current object is before, after, or in the same position as the specified object in the sort order. </summary>
		/// <param name="other">The object to compare with the current instance. </param>
		/// <param name="comparer">An object that provides custom rules for comparison. </param>
		/// <returns>A signed integer that indicates the relative position of this instance and <paramref name="other" /> in the sort order, as shown in the following able. VaueDescriptionA negative integerThis instance precedes <paramref name="other" />. ZeroThis instance and <paramref name="other" /> have the same position in the sort order.A positive integerThis instance follows <paramref name="other" />. </returns>
		// Token: 0x0600075C RID: 1884 RVA: 0x00027CD0 File Offset: 0x00025ED0
		int IStructuralComparable.CompareTo(object other, IComparer comparer)
		{
			if (other == null)
			{
				return 1;
			}
			if (!(other is ValueTuple<T1, T2, T3>))
			{
				throw new ArgumentException(SR.Format("Argument must be of type {0}.", base.GetType().ToString()), "other");
			}
			ValueTuple<T1, T2, T3> valueTuple = (ValueTuple<T1, T2, T3>)other;
			int num = comparer.Compare(this.Item1, valueTuple.Item1);
			if (num != 0)
			{
				return num;
			}
			num = comparer.Compare(this.Item2, valueTuple.Item2);
			if (num != 0)
			{
				return num;
			}
			return comparer.Compare(this.Item3, valueTuple.Item3);
		}

		/// <summary>Calculates the hash code for the current <see cref="T:System.ValueTuple`3" /> instance.</summary>
		/// <returns>The hash code for the current <see cref="T:System.ValueTuple`3" /> instance. </returns>
		// Token: 0x0600075D RID: 1885 RVA: 0x00027D7C File Offset: 0x00025F7C
		public override int GetHashCode()
		{
			ref T1 ptr = ref this.Item1;
			T1 t = default(T1);
			int h;
			if (t == null)
			{
				t = this.Item1;
				ptr = ref t;
				if (t == null)
				{
					h = 0;
					goto IL_35;
				}
			}
			h = ptr.GetHashCode();
			IL_35:
			ref T2 ptr2 = ref this.Item2;
			T2 t2 = default(T2);
			int h2;
			if (t2 == null)
			{
				t2 = this.Item2;
				ptr2 = ref t2;
				if (t2 == null)
				{
					h2 = 0;
					goto IL_6A;
				}
			}
			h2 = ptr2.GetHashCode();
			IL_6A:
			ref T3 ptr3 = ref this.Item3;
			T3 t3 = default(T3);
			int h3;
			if (t3 == null)
			{
				t3 = this.Item3;
				ptr3 = ref t3;
				if (t3 == null)
				{
					h3 = 0;
					goto IL_9F;
				}
			}
			h3 = ptr3.GetHashCode();
			IL_9F:
			return ValueTuple.CombineHashCodes(h, h2, h3);
		}

		/// <summary>Calculates the hash code for the current <see cref="T:System.ValueTuple`3" /> instance by using a specified computation method. </summary>
		/// <param name="comparer">An object whose <see cref="M:System.Collections.IEqualityComparer.GetHashCode(System.Object)" /> method calculates the hash code of the current <see cref="T:System.ValueTuple`3" /> instance. </param>
		/// <returns>A 32-bit signed integer hash code. </returns>
		// Token: 0x0600075E RID: 1886 RVA: 0x00027E2D File Offset: 0x0002602D
		int IStructuralEquatable.GetHashCode(IEqualityComparer comparer)
		{
			return this.GetHashCodeCore(comparer);
		}

		// Token: 0x0600075F RID: 1887 RVA: 0x00027E36 File Offset: 0x00026036
		private int GetHashCodeCore(IEqualityComparer comparer)
		{
			return ValueTuple.CombineHashCodes(comparer.GetHashCode(this.Item1), comparer.GetHashCode(this.Item2), comparer.GetHashCode(this.Item3));
		}

		// Token: 0x06000760 RID: 1888 RVA: 0x00027E2D File Offset: 0x0002602D
		int IValueTupleInternal.GetHashCode(IEqualityComparer comparer)
		{
			return this.GetHashCodeCore(comparer);
		}

		/// <summary>Returns a string that represents the value of this <see cref="T:System.ValueTuple`3" /> instance. </summary>
		/// <returns>The string representation of this <see cref="T:System.ValueTuple`2" /> instance. </returns>
		// Token: 0x06000761 RID: 1889 RVA: 0x00027E70 File Offset: 0x00026070
		public override string ToString()
		{
			string[] array = new string[7];
			array[0] = "(";
			int num = 1;
			ref T1 ptr = ref this.Item1;
			T1 t = default(T1);
			string text;
			if (t == null)
			{
				t = this.Item1;
				ptr = ref t;
				if (t == null)
				{
					text = null;
					goto IL_45;
				}
			}
			text = ptr.ToString();
			IL_45:
			array[num] = text;
			array[2] = ", ";
			int num2 = 3;
			ref T2 ptr2 = ref this.Item2;
			T2 t2 = default(T2);
			string text2;
			if (t2 == null)
			{
				t2 = this.Item2;
				ptr2 = ref t2;
				if (t2 == null)
				{
					text2 = null;
					goto IL_85;
				}
			}
			text2 = ptr2.ToString();
			IL_85:
			array[num2] = text2;
			array[4] = ", ";
			int num3 = 5;
			ref T3 ptr3 = ref this.Item3;
			T3 t3 = default(T3);
			string text3;
			if (t3 == null)
			{
				t3 = this.Item3;
				ptr3 = ref t3;
				if (t3 == null)
				{
					text3 = null;
					goto IL_C5;
				}
			}
			text3 = ptr3.ToString();
			IL_C5:
			array[num3] = text3;
			array[6] = ")";
			return string.Concat(array);
		}

		// Token: 0x06000762 RID: 1890 RVA: 0x00027F50 File Offset: 0x00026150
		string IValueTupleInternal.ToStringEnd()
		{
			string[] array = new string[6];
			int num = 0;
			ref T1 ptr = ref this.Item1;
			T1 t = default(T1);
			string text;
			if (t == null)
			{
				t = this.Item1;
				ptr = ref t;
				if (t == null)
				{
					text = null;
					goto IL_3D;
				}
			}
			text = ptr.ToString();
			IL_3D:
			array[num] = text;
			array[1] = ", ";
			int num2 = 2;
			ref T2 ptr2 = ref this.Item2;
			T2 t2 = default(T2);
			string text2;
			if (t2 == null)
			{
				t2 = this.Item2;
				ptr2 = ref t2;
				if (t2 == null)
				{
					text2 = null;
					goto IL_7D;
				}
			}
			text2 = ptr2.ToString();
			IL_7D:
			array[num2] = text2;
			array[3] = ", ";
			int num3 = 4;
			ref T3 ptr3 = ref this.Item3;
			T3 t3 = default(T3);
			string text3;
			if (t3 == null)
			{
				t3 = this.Item3;
				ptr3 = ref t3;
				if (t3 == null)
				{
					text3 = null;
					goto IL_BD;
				}
			}
			text3 = ptr3.ToString();
			IL_BD:
			array[num3] = text3;
			array[5] = ")";
			return string.Concat(array);
		}

		/// <summary>Gets the number of elements in the <see langword="ValueTuple" />.</summary>
		/// <returns>3, the number of elements in a <see cref="T:System.ValueTuple`3" /> object. </returns>
		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06000763 RID: 1891 RVA: 0x00028028 File Offset: 0x00026228
		int ITuple.Length
		{
			get
			{
				return 3;
			}
		}

		/// <summary>Gets the value of the specified <see langword="ValueTuple" /> element. </summary>
		/// <param name="index">The index of the specified <see langword="ValueTuple" /> element. <paramref name="index" /> can range from 0 to 2. </param>
		/// <returns>The value of the <see langword="ValueTuple" /> element at the specified position. </returns>
		/// <exception cref="T:System.IndexOutOfRangeException">
		///   <paramref name="index" /> is less than 0 or greater than 2. </exception>
		// Token: 0x17000131 RID: 305
		object ITuple.this[int index]
		{
			get
			{
				switch (index)
				{
				case 0:
					return this.Item1;
				case 1:
					return this.Item2;
				case 2:
					return this.Item3;
				default:
					throw new IndexOutOfRangeException();
				}
			}
		}

		/// <summary>Gets the value of the current <see cref="T:System.ValueTuple`3" /> instance's first element. </summary>
		/// <returns>The value of the current <see cref="T:System.ValueTuple`3" /> instance's first element. </returns>
		// Token: 0x04000678 RID: 1656
		public T1 Item1;

		/// <summary>Gets the value of the current <see cref="T:System.ValueTuple`3" /> instance's second element. </summary>
		/// <returns>The value of the current <see cref="T:System.ValueTuple`3" /> instance's second element. </returns>
		// Token: 0x04000679 RID: 1657
		public T2 Item2;

		/// <summary>Gets the value of the current <see cref="T:System.ValueTuple`3" /> instance's third element. </summary>
		/// <returns>The value of the current <see cref="T:System.ValueTuple`3" /> instance's third element. </returns>
		// Token: 0x0400067A RID: 1658
		public T3 Item3;
	}
}
