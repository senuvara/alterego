﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Provides data for the <see cref="E:System.AppDomain.AssemblyLoad" /> event.</summary>
	// Token: 0x020001F2 RID: 498
	[ComVisible(true)]
	public class AssemblyLoadEventArgs : EventArgs
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.AssemblyLoadEventArgs" /> class using the specified <see cref="T:System.Reflection.Assembly" />.</summary>
		/// <param name="loadedAssembly">An instance that represents the currently loaded assembly. </param>
		// Token: 0x060017D8 RID: 6104 RVA: 0x0005C59F File Offset: 0x0005A79F
		public AssemblyLoadEventArgs(Assembly loadedAssembly)
		{
			this.m_loadedAssembly = loadedAssembly;
		}

		/// <summary>Gets an <see cref="T:System.Reflection.Assembly" /> that represents the currently loaded assembly.</summary>
		/// <returns>An instance of <see cref="T:System.Reflection.Assembly" /> that represents the currently loaded assembly.</returns>
		// Token: 0x17000305 RID: 773
		// (get) Token: 0x060017D9 RID: 6105 RVA: 0x0005C5AE File Offset: 0x0005A7AE
		public Assembly LoadedAssembly
		{
			get
			{
				return this.m_loadedAssembly;
			}
		}

		// Token: 0x04000C42 RID: 3138
		private Assembly m_loadedAssembly;
	}
}
