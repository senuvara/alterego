﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates that a field of a serializable class should not be serialized. This class cannot be inherited.</summary>
	// Token: 0x02000190 RID: 400
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	[ComVisible(true)]
	public sealed class NonSerializedAttribute : Attribute
	{
		// Token: 0x06001157 RID: 4439 RVA: 0x00047D6F File Offset: 0x00045F6F
		internal static Attribute GetCustomAttribute(RuntimeFieldInfo field)
		{
			if ((field.Attributes & FieldAttributes.NotSerialized) == FieldAttributes.PrivateScope)
			{
				return null;
			}
			return new NonSerializedAttribute();
		}

		// Token: 0x06001158 RID: 4440 RVA: 0x00047D86 File Offset: 0x00045F86
		internal static bool IsDefined(RuntimeFieldInfo field)
		{
			return (field.Attributes & FieldAttributes.NotSerialized) > FieldAttributes.PrivateScope;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.NonSerializedAttribute" /> class.</summary>
		// Token: 0x06001159 RID: 4441 RVA: 0x000020BF File Offset: 0x000002BF
		public NonSerializedAttribute()
		{
		}
	}
}
