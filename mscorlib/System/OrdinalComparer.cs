﻿using System;
using System.Globalization;

namespace System
{
	// Token: 0x020001B4 RID: 436
	[Serializable]
	internal sealed class OrdinalComparer : StringComparer
	{
		// Token: 0x060013D7 RID: 5079 RVA: 0x0005111D File Offset: 0x0004F31D
		internal OrdinalComparer(bool ignoreCase)
		{
			this._ignoreCase = ignoreCase;
		}

		// Token: 0x060013D8 RID: 5080 RVA: 0x0005112C File Offset: 0x0004F32C
		public override int Compare(string x, string y)
		{
			if (x == y)
			{
				return 0;
			}
			if (x == null)
			{
				return -1;
			}
			if (y == null)
			{
				return 1;
			}
			if (this._ignoreCase)
			{
				return string.Compare(x, y, StringComparison.OrdinalIgnoreCase);
			}
			return string.CompareOrdinal(x, y);
		}

		// Token: 0x060013D9 RID: 5081 RVA: 0x00051156 File Offset: 0x0004F356
		public override bool Equals(string x, string y)
		{
			if (x == y)
			{
				return true;
			}
			if (x == null || y == null)
			{
				return false;
			}
			if (this._ignoreCase)
			{
				return x.Length == y.Length && string.Compare(x, y, StringComparison.OrdinalIgnoreCase) == 0;
			}
			return x.Equals(y);
		}

		// Token: 0x060013DA RID: 5082 RVA: 0x00051191 File Offset: 0x0004F391
		public override int GetHashCode(string obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (this._ignoreCase)
			{
				return TextInfo.GetHashCodeOrdinalIgnoreCase(obj);
			}
			return obj.GetHashCode();
		}

		// Token: 0x060013DB RID: 5083 RVA: 0x000511B8 File Offset: 0x0004F3B8
		public override bool Equals(object obj)
		{
			OrdinalComparer ordinalComparer = obj as OrdinalComparer;
			return ordinalComparer != null && this._ignoreCase == ordinalComparer._ignoreCase;
		}

		// Token: 0x060013DC RID: 5084 RVA: 0x000511E0 File Offset: 0x0004F3E0
		public override int GetHashCode()
		{
			int hashCode = "OrdinalComparer".GetHashCode();
			if (!this._ignoreCase)
			{
				return hashCode;
			}
			return ~hashCode;
		}

		// Token: 0x04000AB4 RID: 2740
		private bool _ignoreCase;
	}
}
