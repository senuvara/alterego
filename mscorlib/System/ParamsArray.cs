﻿using System;

namespace System
{
	// Token: 0x0200019D RID: 413
	internal struct ParamsArray
	{
		// Token: 0x060011BA RID: 4538 RVA: 0x0004922A File Offset: 0x0004742A
		public ParamsArray(object arg0)
		{
			this.arg0 = arg0;
			this.arg1 = null;
			this.arg2 = null;
			this.args = ParamsArray.oneArgArray;
		}

		// Token: 0x060011BB RID: 4539 RVA: 0x0004924C File Offset: 0x0004744C
		public ParamsArray(object arg0, object arg1)
		{
			this.arg0 = arg0;
			this.arg1 = arg1;
			this.arg2 = null;
			this.args = ParamsArray.twoArgArray;
		}

		// Token: 0x060011BC RID: 4540 RVA: 0x0004926E File Offset: 0x0004746E
		public ParamsArray(object arg0, object arg1, object arg2)
		{
			this.arg0 = arg0;
			this.arg1 = arg1;
			this.arg2 = arg2;
			this.args = ParamsArray.threeArgArray;
		}

		// Token: 0x060011BD RID: 4541 RVA: 0x00049290 File Offset: 0x00047490
		public ParamsArray(object[] args)
		{
			int num = args.Length;
			this.arg0 = ((num > 0) ? args[0] : null);
			this.arg1 = ((num > 1) ? args[1] : null);
			this.arg2 = ((num > 2) ? args[2] : null);
			this.args = args;
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x060011BE RID: 4542 RVA: 0x000492D8 File Offset: 0x000474D8
		public int Length
		{
			get
			{
				return this.args.Length;
			}
		}

		// Token: 0x1700020B RID: 523
		public object this[int index]
		{
			get
			{
				if (index != 0)
				{
					return this.GetAtSlow(index);
				}
				return this.arg0;
			}
		}

		// Token: 0x060011C0 RID: 4544 RVA: 0x000492F5 File Offset: 0x000474F5
		private object GetAtSlow(int index)
		{
			if (index == 1)
			{
				return this.arg1;
			}
			if (index == 2)
			{
				return this.arg2;
			}
			return this.args[index];
		}

		// Token: 0x060011C1 RID: 4545 RVA: 0x00049315 File Offset: 0x00047515
		// Note: this type is marked as 'beforefieldinit'.
		static ParamsArray()
		{
		}

		// Token: 0x04000A16 RID: 2582
		private static readonly object[] oneArgArray = new object[1];

		// Token: 0x04000A17 RID: 2583
		private static readonly object[] twoArgArray = new object[2];

		// Token: 0x04000A18 RID: 2584
		private static readonly object[] threeArgArray = new object[3];

		// Token: 0x04000A19 RID: 2585
		private readonly object arg0;

		// Token: 0x04000A1A RID: 2586
		private readonly object arg1;

		// Token: 0x04000A1B RID: 2587
		private readonly object arg2;

		// Token: 0x04000A1C RID: 2588
		private readonly object[] args;
	}
}
