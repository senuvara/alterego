﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000210 RID: 528
	[StructLayout(LayoutKind.Sequential)]
	internal sealed class MonoCQItem
	{
		// Token: 0x06001935 RID: 6453 RVA: 0x00002050 File Offset: 0x00000250
		public MonoCQItem()
		{
		}

		// Token: 0x04000C91 RID: 3217
		private object[] array;

		// Token: 0x04000C92 RID: 3218
		private byte[] array_state;

		// Token: 0x04000C93 RID: 3219
		private int head;

		// Token: 0x04000C94 RID: 3220
		private int tail;
	}
}
