﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Security.Policy;

namespace System.Security
{
	/// <summary>Provides the main access point for classes interacting with the security system. This class cannot be inherited.</summary>
	// Token: 0x02000567 RID: 1383
	[ComVisible(true)]
	public static class SecurityManager
	{
		/// <summary>Gets or sets a value indicating whether code must have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.Execution" /> in order to execute.</summary>
		/// <returns>
		///     <see langword="true" /> if code must have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.Execution" /> in order to execute; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Security.SecurityException">The code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlPolicy" />. </exception>
		// Token: 0x170009AF RID: 2479
		// (get) Token: 0x06003B64 RID: 15204 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06003B65 RID: 15205 RVA: 0x000020D3 File Offset: 0x000002D3
		[Obsolete]
		public static bool CheckExecutionRights
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		/// <summary>Gets or sets a value indicating whether security is enabled.</summary>
		/// <returns>
		///     <see langword="true" /> if security is enabled; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Security.SecurityException">The code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlPolicy" />. </exception>
		// Token: 0x170009B0 RID: 2480
		// (get) Token: 0x06003B66 RID: 15206 RVA: 0x00002526 File Offset: 0x00000726
		// (set) Token: 0x06003B67 RID: 15207 RVA: 0x000020D3 File Offset: 0x000002D3
		[Obsolete("The security manager cannot be turned off on MS runtime")]
		public static bool SecurityEnabled
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x170009B1 RID: 2481
		// (get) Token: 0x06003B68 RID: 15208 RVA: 0x00004E08 File Offset: 0x00003008
		internal static bool HasElevatedPermissions
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06003B69 RID: 15209 RVA: 0x00004E08 File Offset: 0x00003008
		internal static bool CheckElevatedPermissions()
		{
			return true;
		}

		// Token: 0x06003B6A RID: 15210 RVA: 0x000020D3 File Offset: 0x000002D3
		internal static void EnsureElevatedPermissions()
		{
		}

		/// <summary>Gets the granted zone identity and URL identity permission sets for the current assembly.</summary>
		/// <param name="zone">An output parameter that contains an <see cref="T:System.Collections.ArrayList" /> of granted <see cref="P:System.Security.Permissions.ZoneIdentityPermissionAttribute.Zone" /> objects.</param>
		/// <param name="origin">An output parameter that contains an <see cref="T:System.Collections.ArrayList" /> of granted <see cref="T:System.Security.Permissions.UrlIdentityPermission" /> objects.</param>
		/// <exception cref="T:System.Security.SecurityException">The request for <see cref="T:System.Security.Permissions.StrongNameIdentityPermission" /> failed.</exception>
		// Token: 0x06003B6B RID: 15211 RVA: 0x000CE338 File Offset: 0x000CC538
		[StrongNameIdentityPermission(SecurityAction.LinkDemand, PublicKey = "0x00000000000000000400000000000000")]
		public static void GetZoneAndOrigin(out ArrayList zone, out ArrayList origin)
		{
			zone = new ArrayList();
			origin = new ArrayList();
		}

		/// <summary>Determines whether a permission is granted to the caller.</summary>
		/// <param name="perm">The permission to test against the grant of the caller. </param>
		/// <returns>
		///     <see langword="true" /> if the permissions granted to the caller include the permission <paramref name="perm" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003B6C RID: 15212 RVA: 0x00004E08 File Offset: 0x00003008
		[Obsolete]
		public static bool IsGranted(IPermission perm)
		{
			return true;
		}

		/// <summary>Loads a <see cref="T:System.Security.Policy.PolicyLevel" /> from the specified file.</summary>
		/// <param name="path">The physical file path to a file containing the security policy information. </param>
		/// <param name="type">One of the enumeration values that specifies the type of the policy level to be loaded. </param>
		/// <returns>The loaded policy level.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="path" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The file indicated by the <paramref name="path" /> parameter does not exist. </exception>
		/// <exception cref="T:System.Security.SecurityException">The code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlPolicy" />.-or- The code that calls this method does not have <see cref="F:System.Security.Permissions.FileIOPermissionAccess.Read" />.-or- The code that calls this method does not have <see cref="F:System.Security.Permissions.FileIOPermissionAccess.Write" />.-or- The code that calls this method does not have <see cref="F:System.Security.Permissions.FileIOPermissionAccess.PathDiscovery" />. </exception>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		// Token: 0x06003B6D RID: 15213 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		[SecurityPermission(SecurityAction.Demand, ControlPolicy = true)]
		public static PolicyLevel LoadPolicyLevelFromFile(string path, PolicyLevelType type)
		{
			throw new NotSupportedException();
		}

		/// <summary>Loads a <see cref="T:System.Security.Policy.PolicyLevel" /> from the specified string.</summary>
		/// <param name="str">The XML representation of a security policy level in the same form in which it appears in a configuration file. </param>
		/// <param name="type">One of the enumeration values that specifies the type of the policy level to be loaded. </param>
		/// <returns>The loaded policy level.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="str" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="str" /> parameter is not valid. </exception>
		/// <exception cref="T:System.Security.SecurityException">The code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlPolicy" />. </exception>
		// Token: 0x06003B6E RID: 15214 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		[SecurityPermission(SecurityAction.Demand, ControlPolicy = true)]
		public static PolicyLevel LoadPolicyLevelFromString(string str, PolicyLevelType type)
		{
			throw new NotSupportedException();
		}

		/// <summary>Provides an enumerator to access the security policy hierarchy by levels, such as computer policy and user policy.</summary>
		/// <returns>An enumerator for <see cref="T:System.Security.Policy.PolicyLevel" /> objects that compose the security policy hierarchy.</returns>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		/// <exception cref="T:System.Security.SecurityException">The code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlPolicy" />. </exception>
		// Token: 0x06003B6F RID: 15215 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		[SecurityPermission(SecurityAction.Demand, ControlPolicy = true)]
		public static IEnumerator PolicyHierarchy()
		{
			throw new NotSupportedException();
		}

		/// <summary>Determines what permissions to grant to code based on the specified evidence.</summary>
		/// <param name="evidence">The evidence set used to evaluate policy. </param>
		/// <returns>The set of permissions that can be granted by the security system.</returns>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		// Token: 0x06003B70 RID: 15216 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		public static PermissionSet ResolvePolicy(Evidence evidence)
		{
			throw new NotSupportedException();
		}

		/// <summary>Determines what permissions to grant to code based on the specified evidence.</summary>
		/// <param name="evidences">An array of evidence objects used to evaluate policy. </param>
		/// <returns>The set of permissions that is appropriate for all of the provided evidence.</returns>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		// Token: 0x06003B71 RID: 15217 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		public static PermissionSet ResolvePolicy(Evidence[] evidences)
		{
			throw new NotSupportedException();
		}

		/// <summary>Determines which permissions to grant to code based on the specified evidence, excluding the policy for the <see cref="T:System.AppDomain" /> level.</summary>
		/// <param name="evidence">The evidence set used to evaluate policy.</param>
		/// <returns>The set of permissions that can be granted by the security system.</returns>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		// Token: 0x06003B72 RID: 15218 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		public static PermissionSet ResolveSystemPolicy(Evidence evidence)
		{
			throw new NotSupportedException();
		}

		/// <summary>Determines what permissions to grant to code based on the specified evidence and requests.</summary>
		/// <param name="evidence">The evidence set used to evaluate policy. </param>
		/// <param name="reqdPset">The required permissions the code needs to run. </param>
		/// <param name="optPset">The optional permissions that will be used if granted, but aren't required for the code to run. </param>
		/// <param name="denyPset">The denied permissions that must never be granted to the code even if policy otherwise permits it. </param>
		/// <param name="denied">An output parameter that contains the set of permissions not granted. </param>
		/// <returns>The set of permissions that would be granted by the security system.</returns>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		/// <exception cref="T:System.Security.Policy.PolicyException">Policy fails to grant the minimum required permissions specified by the <paramref name="reqdPset" /> parameter. </exception>
		// Token: 0x06003B73 RID: 15219 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		public static PermissionSet ResolvePolicy(Evidence evidence, PermissionSet reqdPset, PermissionSet optPset, PermissionSet denyPset, out PermissionSet denied)
		{
			throw new NotSupportedException();
		}

		/// <summary>Gets a collection of code groups matching the specified evidence.</summary>
		/// <param name="evidence">The evidence set against which the policy is evaluated. </param>
		/// <returns>An enumeration of the set of code groups matching the evidence.</returns>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		// Token: 0x06003B74 RID: 15220 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		public static IEnumerator ResolvePolicyGroups(Evidence evidence)
		{
			throw new NotSupportedException();
		}

		/// <summary>Saves the modified security policy state.</summary>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		/// <exception cref="T:System.Security.SecurityException">The code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlPolicy" />. </exception>
		// Token: 0x06003B75 RID: 15221 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		[SecurityPermission(SecurityAction.Demand, ControlPolicy = true)]
		public static void SavePolicy()
		{
			throw new NotSupportedException();
		}

		/// <summary>Saves a modified security policy level loaded with <see cref="M:System.Security.SecurityManager.LoadPolicyLevelFromFile(System.String,System.Security.PolicyLevelType)" />.</summary>
		/// <param name="level">The policy level object to be saved. </param>
		/// <exception cref="T:System.Security.SecurityException">The code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlPolicy" />. </exception>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		// Token: 0x06003B76 RID: 15222 RVA: 0x000175EA File Offset: 0x000157EA
		[Obsolete]
		[SecurityPermission(SecurityAction.Demand, ControlPolicy = true)]
		public static void SavePolicyLevel(PolicyLevel level)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06003B77 RID: 15223 RVA: 0x000175EA File Offset: 0x000157EA
		internal static bool ResolvePolicyLevel(ref PermissionSet ps, PolicyLevel pl, Evidence evidence)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06003B78 RID: 15224 RVA: 0x000175EA File Offset: 0x000157EA
		internal static void ResolveIdentityPermissions(PermissionSet ps, Evidence evidence)
		{
			throw new NotSupportedException();
		}

		// Token: 0x170009B2 RID: 2482
		// (get) Token: 0x06003B79 RID: 15225 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		// (set) Token: 0x06003B7A RID: 15226 RVA: 0x000020D3 File Offset: 0x000002D3
		internal static PolicyLevel ResolvingPolicyLevel
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x06003B7B RID: 15227 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		internal static IPermission CheckPermissionSet(Assembly a, PermissionSet ps, bool noncas)
		{
			return null;
		}

		// Token: 0x06003B7C RID: 15228 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		internal static IPermission CheckPermissionSet(AppDomain ad, PermissionSet ps)
		{
			return null;
		}

		// Token: 0x06003B7D RID: 15229 RVA: 0x000175EA File Offset: 0x000157EA
		internal static PermissionSet Decode(IntPtr permissions, int length)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06003B7E RID: 15230 RVA: 0x000175EA File Offset: 0x000157EA
		internal static PermissionSet Decode(byte[] encodedPermissions)
		{
			throw new NotSupportedException();
		}

		/// <summary>Gets a permission set that is safe to grant to an application that has the provided evidence.</summary>
		/// <param name="evidence">The host evidence to match to a permission set.</param>
		/// <returns>A permission set that can be used as a grant set for the application that has the provided evidence.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="evidence" /> is <see langword="null" />.</exception>
		// Token: 0x06003B7F RID: 15231 RVA: 0x000CE348 File Offset: 0x000CC548
		public static PermissionSet GetStandardSandbox(Evidence evidence)
		{
			if (evidence == null)
			{
				throw new ArgumentNullException("evidence");
			}
			throw new NotImplementedException();
		}

		/// <summary>Determines whether the current thread requires a security context capture if its security state has to be re-created at a later point in time.</summary>
		/// <returns>
		///     <see langword="false" /> if the stack contains no partially trusted application domains, no partially trusted assemblies, and no currently active <see cref="M:System.Security.CodeAccessPermission.PermitOnly" /> or <see cref="M:System.Security.CodeAccessPermission.Deny" /> modifiers; <see langword="true" /> if the common language runtime cannot guarantee that the stack contains none of these. </returns>
		// Token: 0x06003B80 RID: 15232 RVA: 0x000041F3 File Offset: 0x000023F3
		public static bool CurrentThreadRequiresSecurityContextCapture()
		{
			throw new NotImplementedException();
		}
	}
}
