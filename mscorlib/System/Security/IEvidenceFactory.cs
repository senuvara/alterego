﻿using System;
using System.Runtime.InteropServices;
using System.Security.Policy;

namespace System.Security
{
	/// <summary>Gets an object's <see cref="T:System.Security.Policy.Evidence" />.</summary>
	// Token: 0x02000557 RID: 1367
	[ComVisible(true)]
	public interface IEvidenceFactory
	{
		/// <summary>Gets <see cref="T:System.Security.Policy.Evidence" /> that verifies the current object's identity.</summary>
		/// <returns>
		///     <see cref="T:System.Security.Policy.Evidence" /> of the current object's identity.</returns>
		// Token: 0x1700098E RID: 2446
		// (get) Token: 0x06003AB1 RID: 15025
		Evidence Evidence { get; }
	}
}
