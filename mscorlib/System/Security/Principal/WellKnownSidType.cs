﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	/// <summary>Defines a set of commonly used security identifiers (SIDs).</summary>
	// Token: 0x02000643 RID: 1603
	[ComVisible(false)]
	public enum WellKnownSidType
	{
		/// <summary>Indicates a null SID.</summary>
		// Token: 0x040021BB RID: 8635
		NullSid,
		/// <summary>Indicates a SID that matches everyone.</summary>
		// Token: 0x040021BC RID: 8636
		WorldSid,
		/// <summary>Indicates a local SID.</summary>
		// Token: 0x040021BD RID: 8637
		LocalSid,
		/// <summary>Indicates a SID that matches the owner or creator of an object.</summary>
		// Token: 0x040021BE RID: 8638
		CreatorOwnerSid,
		/// <summary>Indicates a SID that matches the creator group of an object.</summary>
		// Token: 0x040021BF RID: 8639
		CreatorGroupSid,
		/// <summary>Indicates a creator owner server SID.</summary>
		// Token: 0x040021C0 RID: 8640
		CreatorOwnerServerSid,
		/// <summary>Indicates a creator group server SID.</summary>
		// Token: 0x040021C1 RID: 8641
		CreatorGroupServerSid,
		/// <summary>Indicates a SID for the Windows NT authority.</summary>
		// Token: 0x040021C2 RID: 8642
		NTAuthoritySid,
		/// <summary>Indicates a SID for a dial-up account.</summary>
		// Token: 0x040021C3 RID: 8643
		DialupSid,
		/// <summary>Indicates a SID for a network account. This SID is added to the process of a token when it logs on across a network.</summary>
		// Token: 0x040021C4 RID: 8644
		NetworkSid,
		/// <summary>Indicates a SID for a batch process. This SID is added to the process of a token when it logs on as a batch job.</summary>
		// Token: 0x040021C5 RID: 8645
		BatchSid,
		/// <summary>Indicates a SID for an interactive account. This SID is added to the process of a token when it logs on interactively.</summary>
		// Token: 0x040021C6 RID: 8646
		InteractiveSid,
		/// <summary>Indicates a SID for a service. This SID is added to the process of a token when it logs on as a service.</summary>
		// Token: 0x040021C7 RID: 8647
		ServiceSid,
		/// <summary>Indicates a SID for the anonymous account.</summary>
		// Token: 0x040021C8 RID: 8648
		AnonymousSid,
		/// <summary>Indicates a proxy SID.</summary>
		// Token: 0x040021C9 RID: 8649
		ProxySid,
		/// <summary>Indicates a SID for an enterprise controller.</summary>
		// Token: 0x040021CA RID: 8650
		EnterpriseControllersSid,
		/// <summary>Indicates a SID for self.</summary>
		// Token: 0x040021CB RID: 8651
		SelfSid,
		/// <summary>Indicates a SID for an authenticated user.</summary>
		// Token: 0x040021CC RID: 8652
		AuthenticatedUserSid,
		/// <summary>Indicates a SID for restricted code.</summary>
		// Token: 0x040021CD RID: 8653
		RestrictedCodeSid,
		/// <summary>Indicates a SID that matches a terminal server account.</summary>
		// Token: 0x040021CE RID: 8654
		TerminalServerSid,
		/// <summary>Indicates a SID that matches remote logons.</summary>
		// Token: 0x040021CF RID: 8655
		RemoteLogonIdSid,
		/// <summary>Indicates a SID that matches logon IDs.</summary>
		// Token: 0x040021D0 RID: 8656
		LogonIdsSid,
		/// <summary>Indicates a SID that matches the local system.</summary>
		// Token: 0x040021D1 RID: 8657
		LocalSystemSid,
		/// <summary>Indicates a SID that matches a local service.</summary>
		// Token: 0x040021D2 RID: 8658
		LocalServiceSid,
		/// <summary>Indicates a SID that matches a network service.</summary>
		// Token: 0x040021D3 RID: 8659
		NetworkServiceSid,
		/// <summary>Indicates a SID that matches the domain account.</summary>
		// Token: 0x040021D4 RID: 8660
		BuiltinDomainSid,
		/// <summary>Indicates a SID that matches the administrator account.</summary>
		// Token: 0x040021D5 RID: 8661
		BuiltinAdministratorsSid,
		/// <summary>Indicates a SID that matches built-in user accounts.</summary>
		// Token: 0x040021D6 RID: 8662
		BuiltinUsersSid,
		/// <summary>Indicates a SID that matches the guest account.</summary>
		// Token: 0x040021D7 RID: 8663
		BuiltinGuestsSid,
		/// <summary>Indicates a SID that matches the power users group.</summary>
		// Token: 0x040021D8 RID: 8664
		BuiltinPowerUsersSid,
		/// <summary>Indicates a SID that matches the account operators account.</summary>
		// Token: 0x040021D9 RID: 8665
		BuiltinAccountOperatorsSid,
		/// <summary>Indicates a SID that matches the system operators group.</summary>
		// Token: 0x040021DA RID: 8666
		BuiltinSystemOperatorsSid,
		/// <summary>Indicates a SID that matches the print operators group.</summary>
		// Token: 0x040021DB RID: 8667
		BuiltinPrintOperatorsSid,
		/// <summary>Indicates a SID that matches the backup operators group.</summary>
		// Token: 0x040021DC RID: 8668
		BuiltinBackupOperatorsSid,
		/// <summary>Indicates a SID that matches the replicator account.</summary>
		// Token: 0x040021DD RID: 8669
		BuiltinReplicatorSid,
		/// <summary>Indicates a SID that matches pre-Windows 2000 compatible accounts.</summary>
		// Token: 0x040021DE RID: 8670
		BuiltinPreWindows2000CompatibleAccessSid,
		/// <summary>Indicates a SID that matches remote desktop users.</summary>
		// Token: 0x040021DF RID: 8671
		BuiltinRemoteDesktopUsersSid,
		/// <summary>Indicates a SID that matches the network operators group.</summary>
		// Token: 0x040021E0 RID: 8672
		BuiltinNetworkConfigurationOperatorsSid,
		/// <summary>Indicates a SID that matches the account administrators group.</summary>
		// Token: 0x040021E1 RID: 8673
		AccountAdministratorSid,
		/// <summary>Indicates a SID that matches the account guest group.</summary>
		// Token: 0x040021E2 RID: 8674
		AccountGuestSid,
		/// <summary>Indicates a SID that matches the account Kerberos target group.</summary>
		// Token: 0x040021E3 RID: 8675
		AccountKrbtgtSid,
		/// <summary>Indicates a SID that matches the account domain administrator group.</summary>
		// Token: 0x040021E4 RID: 8676
		AccountDomainAdminsSid,
		/// <summary>Indicates a SID that matches the account domain users group.</summary>
		// Token: 0x040021E5 RID: 8677
		AccountDomainUsersSid,
		/// <summary>Indicates a SID that matches the account domain guests group.</summary>
		// Token: 0x040021E6 RID: 8678
		AccountDomainGuestsSid,
		/// <summary>Indicates a SID that matches the account computer group.</summary>
		// Token: 0x040021E7 RID: 8679
		AccountComputersSid,
		/// <summary>Indicates a SID that matches the account controller group.</summary>
		// Token: 0x040021E8 RID: 8680
		AccountControllersSid,
		/// <summary>Indicates a SID that matches the certificate administrators group.</summary>
		// Token: 0x040021E9 RID: 8681
		AccountCertAdminsSid,
		/// <summary>Indicates a SID that matches the schema administrators group.</summary>
		// Token: 0x040021EA RID: 8682
		AccountSchemaAdminsSid,
		/// <summary>Indicates a SID that matches the enterprise administrators group.</summary>
		// Token: 0x040021EB RID: 8683
		AccountEnterpriseAdminsSid,
		/// <summary>Indicates a SID that matches the policy administrators group.</summary>
		// Token: 0x040021EC RID: 8684
		AccountPolicyAdminsSid,
		/// <summary>Indicates a SID that matches the RAS and IAS server account.</summary>
		// Token: 0x040021ED RID: 8685
		AccountRasAndIasServersSid,
		/// <summary>Indicates a SID present when the Microsoft NTLM authentication package authenticated the client.</summary>
		// Token: 0x040021EE RID: 8686
		NtlmAuthenticationSid,
		/// <summary>Indicates a SID present when the Microsoft Digest authentication package authenticated the client.</summary>
		// Token: 0x040021EF RID: 8687
		DigestAuthenticationSid,
		/// <summary>Indicates a SID present when the Secure Channel (SSL/TLS) authentication package authenticated the client.</summary>
		// Token: 0x040021F0 RID: 8688
		SChannelAuthenticationSid,
		/// <summary>Indicates a SID present when the user authenticated from within the forest or across a trust that does not have the selective authentication option enabled. If this SID is present, then <see cref="F:System.Security.Principal.WellKnownSidType.OtherOrganizationSid" /> cannot be present.</summary>
		// Token: 0x040021F1 RID: 8689
		ThisOrganizationSid,
		/// <summary>Indicates a SID present when the user authenticated across a forest with the selective authentication option enabled. If this SID is present, then <see cref="F:System.Security.Principal.WellKnownSidType.ThisOrganizationSid" /> cannot be present.</summary>
		// Token: 0x040021F2 RID: 8690
		OtherOrganizationSid,
		/// <summary>Indicates a SID that allows a user to create incoming forest trusts. It is added to the token of users who are a member of the Incoming Forest Trust Builders built-in group in the root domain of the forest.</summary>
		// Token: 0x040021F3 RID: 8691
		BuiltinIncomingForestTrustBuildersSid,
		/// <summary>Indicates a SID that matches the group of users that have remote access to schedule logging of performance counters on this computer.</summary>
		// Token: 0x040021F4 RID: 8692
		BuiltinPerformanceMonitoringUsersSid,
		/// <summary>Indicates a SID that matches the group of users that have remote access to monitor the computer.</summary>
		// Token: 0x040021F5 RID: 8693
		BuiltinPerformanceLoggingUsersSid,
		/// <summary>Indicates a SID that matches the Windows Authorization Access group.</summary>
		// Token: 0x040021F6 RID: 8694
		BuiltinAuthorizationAccessSid,
		/// <summary>Indicates a SID is present in a server that can issue Terminal Server licenses.</summary>
		// Token: 0x040021F7 RID: 8695
		WinBuiltinTerminalServerLicenseServersSid,
		/// <summary>Indicates the maximum defined SID in the <see cref="T:System.Security.Principal.WellKnownSidType" /> enumeration.</summary>
		// Token: 0x040021F8 RID: 8696
		MaxDefined = 60,
		// Token: 0x040021F9 RID: 8697
		WinBuiltinDCOMUsersSid,
		// Token: 0x040021FA RID: 8698
		WinBuiltinIUsersSid,
		// Token: 0x040021FB RID: 8699
		WinIUserSid,
		// Token: 0x040021FC RID: 8700
		WinBuiltinCryptoOperatorsSid,
		// Token: 0x040021FD RID: 8701
		WinUntrustedLabelSid,
		// Token: 0x040021FE RID: 8702
		WinLowLabelSid,
		// Token: 0x040021FF RID: 8703
		WinMediumLabelSid,
		// Token: 0x04002200 RID: 8704
		WinHighLabelSid,
		// Token: 0x04002201 RID: 8705
		WinSystemLabelSid,
		// Token: 0x04002202 RID: 8706
		WinWriteRestrictedCodeSid,
		// Token: 0x04002203 RID: 8707
		WinCreatorOwnerRightsSid,
		// Token: 0x04002204 RID: 8708
		WinCacheablePrincipalsGroupSid,
		// Token: 0x04002205 RID: 8709
		WinNonCacheablePrincipalsGroupSid,
		// Token: 0x04002206 RID: 8710
		WinEnterpriseReadonlyControllersSid,
		// Token: 0x04002207 RID: 8711
		WinAccountReadonlyControllersSid,
		// Token: 0x04002208 RID: 8712
		WinBuiltinEventLogReadersGroup,
		// Token: 0x04002209 RID: 8713
		WinNewEnterpriseReadonlyControllersSid,
		// Token: 0x0400220A RID: 8714
		WinBuiltinCertSvcDComAccessGroup,
		// Token: 0x0400220B RID: 8715
		WinMediumPlusLabelSid,
		// Token: 0x0400220C RID: 8716
		WinLocalLogonSid,
		// Token: 0x0400220D RID: 8717
		WinConsoleLogonSid,
		// Token: 0x0400220E RID: 8718
		WinThisOrganizationCertificateSid,
		// Token: 0x0400220F RID: 8719
		WinApplicationPackageAuthoritySid,
		// Token: 0x04002210 RID: 8720
		WinBuiltinAnyPackageSid,
		// Token: 0x04002211 RID: 8721
		WinCapabilityInternetClientSid,
		// Token: 0x04002212 RID: 8722
		WinCapabilityInternetClientServerSid,
		// Token: 0x04002213 RID: 8723
		WinCapabilityPrivateNetworkClientServerSid,
		// Token: 0x04002214 RID: 8724
		WinCapabilityPicturesLibrarySid,
		// Token: 0x04002215 RID: 8725
		WinCapabilityVideosLibrarySid,
		// Token: 0x04002216 RID: 8726
		WinCapabilityMusicLibrarySid,
		// Token: 0x04002217 RID: 8727
		WinCapabilityDocumentsLibrarySid,
		// Token: 0x04002218 RID: 8728
		WinCapabilitySharedUserCertificatesSid,
		// Token: 0x04002219 RID: 8729
		WinCapabilityEnterpriseAuthenticationSid,
		// Token: 0x0400221A RID: 8730
		WinCapabilityRemovableStorageSid
	}
}
