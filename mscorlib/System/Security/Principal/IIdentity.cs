﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	/// <summary>Defines the basic functionality of an identity object.</summary>
	// Token: 0x02000638 RID: 1592
	[ComVisible(true)]
	public interface IIdentity
	{
		/// <summary>Gets the type of authentication used.</summary>
		/// <returns>The type of authentication used to identify the user.</returns>
		// Token: 0x17000B11 RID: 2833
		// (get) Token: 0x0600426A RID: 17002
		string AuthenticationType { get; }

		/// <summary>Gets a value that indicates whether the user has been authenticated.</summary>
		/// <returns>
		///     <see langword="true" /> if the user was authenticated; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000B12 RID: 2834
		// (get) Token: 0x0600426B RID: 17003
		bool IsAuthenticated { get; }

		/// <summary>Gets the name of the current user.</summary>
		/// <returns>The name of the user on whose behalf the code is running.</returns>
		// Token: 0x17000B13 RID: 2835
		// (get) Token: 0x0600426C RID: 17004
		string Name { get; }
	}
}
