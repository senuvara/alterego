﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	/// <summary>Specifies common roles to be used with <see cref="M:System.Security.Principal.WindowsPrincipal.IsInRole(System.String)" />.</summary>
	// Token: 0x02000645 RID: 1605
	[ComVisible(true)]
	[Serializable]
	public enum WindowsBuiltInRole
	{
		/// <summary>Administrators have complete and unrestricted access to the computer or domain.</summary>
		// Token: 0x04002221 RID: 8737
		Administrator = 544,
		/// <summary>Users are prevented from making accidental or intentional system-wide changes. Thus, users can run certified applications, but not most legacy applications.</summary>
		// Token: 0x04002222 RID: 8738
		User,
		/// <summary>Guests are more restricted than users.</summary>
		// Token: 0x04002223 RID: 8739
		Guest,
		/// <summary>Power users possess most administrative permissions with some restrictions. Thus, power users can run legacy applications, in addition to certified applications.</summary>
		// Token: 0x04002224 RID: 8740
		PowerUser,
		/// <summary>Account operators manage the user accounts on a computer or domain.</summary>
		// Token: 0x04002225 RID: 8741
		AccountOperator,
		/// <summary>System operators manage a particular computer.</summary>
		// Token: 0x04002226 RID: 8742
		SystemOperator,
		/// <summary>Print operators can take control of a printer.</summary>
		// Token: 0x04002227 RID: 8743
		PrintOperator,
		/// <summary>Backup operators can override security restrictions for the sole purpose of backing up or restoring files.</summary>
		// Token: 0x04002228 RID: 8744
		BackupOperator,
		/// <summary>Replicators support file replication in a domain.</summary>
		// Token: 0x04002229 RID: 8745
		Replicator
	}
}
