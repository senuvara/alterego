﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Security.Principal
{
	/// <summary>Represents an exception for a principal whose identity could not be mapped to a known identity.</summary>
	// Token: 0x0200063A RID: 1594
	[ComVisible(false)]
	[Serializable]
	public sealed class IdentityNotMappedException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Principal.IdentityNotMappedException" /> class.</summary>
		// Token: 0x0600426F RID: 17007 RVA: 0x000E1277 File Offset: 0x000DF477
		public IdentityNotMappedException() : base(Locale.GetText("Couldn't translate some identities."))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Principal.IdentityNotMappedException" /> class by using the specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		// Token: 0x06004270 RID: 17008 RVA: 0x000BA200 File Offset: 0x000B8400
		public IdentityNotMappedException(string message) : base(message)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Principal.IdentityNotMappedException" /> class by using the specified error message and inner exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="inner">The exception that is the cause of the current exception. If <paramref name="inner" /> is not null, the current exception is raised in a <see langword="catch" /> block that handles the inner exception.</param>
		// Token: 0x06004271 RID: 17009 RVA: 0x000BA209 File Offset: 0x000B8409
		public IdentityNotMappedException(string message, Exception inner) : base(message, inner)
		{
		}

		/// <summary>Represents the collection of unmapped identities for an <see cref="T:System.Security.Principal.IdentityNotMappedException" /> exception.</summary>
		/// <returns>The collection of unmapped identities.</returns>
		// Token: 0x17000B15 RID: 2837
		// (get) Token: 0x06004272 RID: 17010 RVA: 0x000E1289 File Offset: 0x000DF489
		public IdentityReferenceCollection UnmappedIdentities
		{
			get
			{
				if (this._coll == null)
				{
					this._coll = new IdentityReferenceCollection();
				}
				return this._coll;
			}
		}

		/// <summary>Gets serialization information with the data needed to create an instance of this <see cref="T:System.Security.Principal.IdentityNotMappedException" /> object. </summary>
		/// <param name="serializationInfo">The object that holds the serialized object data about the exception being thrown.</param>
		/// <param name="streamingContext">The object that contains contextual information about the source or destination.</param>
		// Token: 0x06004273 RID: 17011 RVA: 0x000020D3 File Offset: 0x000002D3
		[MonoTODO("not implemented")]
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
		}

		// Token: 0x04002195 RID: 8597
		private IdentityReferenceCollection _coll;
	}
}
