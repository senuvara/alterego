﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	/// <summary>Defines the privileges of the user account associated with the access token. </summary>
	// Token: 0x02000640 RID: 1600
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum TokenAccessLevels
	{
		/// <summary>The user can attach a primary token to a process.</summary>
		// Token: 0x040021A0 RID: 8608
		AssignPrimary = 1,
		/// <summary>The user can duplicate the token.</summary>
		// Token: 0x040021A1 RID: 8609
		Duplicate = 2,
		/// <summary>The user can impersonate a client.</summary>
		// Token: 0x040021A2 RID: 8610
		Impersonate = 4,
		/// <summary>The user can query the token.</summary>
		// Token: 0x040021A3 RID: 8611
		Query = 8,
		/// <summary>The user can query the source of the token.</summary>
		// Token: 0x040021A4 RID: 8612
		QuerySource = 16,
		/// <summary>The user can enable or disable privileges in the token.</summary>
		// Token: 0x040021A5 RID: 8613
		AdjustPrivileges = 32,
		/// <summary>The user can change the attributes of the groups in the token.</summary>
		// Token: 0x040021A6 RID: 8614
		AdjustGroups = 64,
		/// <summary>The user can change the default owner, primary group, or discretionary access control list (DACL) of the token.</summary>
		// Token: 0x040021A7 RID: 8615
		AdjustDefault = 128,
		/// <summary>The user can adjust the session identifier of the token.</summary>
		// Token: 0x040021A8 RID: 8616
		AdjustSessionId = 256,
		/// <summary>The user has standard read rights and the <see cref="F:System.Security.Principal.TokenAccessLevels.Query" /> privilege for the token.</summary>
		// Token: 0x040021A9 RID: 8617
		Read = 131080,
		/// <summary>The user has standard write rights and the <see cref="F:System.Security.Principal.TokenAccessLevels.AdjustPrivileges" />, <see cref="F:System.Security.Principal.TokenAccessLevels.AdjustGroups" /> and <see cref="F:System.Security.Principal.TokenAccessLevels.AdjustDefault" /> privileges for the token.</summary>
		// Token: 0x040021AA RID: 8618
		Write = 131296,
		/// <summary>The user has all possible access to the token.</summary>
		// Token: 0x040021AB RID: 8619
		AllAccess = 983551,
		/// <summary>The maximum value that can be assigned for the <see cref="T:System.Security.Principal.TokenAccessLevels" /> enumeration.</summary>
		// Token: 0x040021AC RID: 8620
		MaximumAllowed = 33554432
	}
}
