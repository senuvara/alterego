﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	/// <summary>Specifies the type of Windows account used.</summary>
	// Token: 0x02000644 RID: 1604
	[ComVisible(true)]
	[Serializable]
	public enum WindowsAccountType
	{
		/// <summary>A standard user account.</summary>
		// Token: 0x0400221C RID: 8732
		Normal,
		/// <summary>A Windows guest account.</summary>
		// Token: 0x0400221D RID: 8733
		Guest,
		/// <summary>A Windows system account.</summary>
		// Token: 0x0400221E RID: 8734
		System,
		/// <summary>An anonymous account.</summary>
		// Token: 0x0400221F RID: 8735
		Anonymous
	}
}
