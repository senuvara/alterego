﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	/// <summary>Defines the basic functionality of a principal object.</summary>
	// Token: 0x02000639 RID: 1593
	[ComVisible(true)]
	public interface IPrincipal
	{
		/// <summary>Gets the identity of the current principal.</summary>
		/// <returns>The <see cref="T:System.Security.Principal.IIdentity" /> object associated with the current principal.</returns>
		// Token: 0x17000B14 RID: 2836
		// (get) Token: 0x0600426D RID: 17005
		IIdentity Identity { get; }

		/// <summary>Determines whether the current principal belongs to the specified role.</summary>
		/// <param name="role">The name of the role for which to check membership. </param>
		/// <returns>
		///     <see langword="true" /> if the current principal is a member of the specified role; otherwise, <see langword="false" />.</returns>
		// Token: 0x0600426E RID: 17006
		bool IsInRole(string role);
	}
}
