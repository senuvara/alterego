﻿using System;
using System.Reflection;
using System.Runtime.Hosting;
using System.Runtime.InteropServices;
using System.Security.Policy;
using Unity;

namespace System.Security
{
	/// <summary>Allows the control and customization of security behavior for application domains.</summary>
	// Token: 0x02000555 RID: 1365
	[ComVisible(true)]
	[Serializable]
	public class HostSecurityManager
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.HostSecurityManager" /> class.</summary>
		// Token: 0x06003AA6 RID: 15014 RVA: 0x00002050 File Offset: 0x00000250
		public HostSecurityManager()
		{
		}

		/// <summary>When overridden in a derived class, gets the security policy for the current application domain.</summary>
		/// <returns>The security policy for the current application domain. The default is <see langword="null" />.</returns>
		/// <exception cref="T:System.NotSupportedException">This method uses code access security (CAS) policy, which is obsolete in the .NET Framework 4. To enable CAS policy for compatibility with earlier versions of the .NET Framework, use the &lt;legacyCasPolicy&gt; element.</exception>
		// Token: 0x1700098C RID: 2444
		// (get) Token: 0x06003AA7 RID: 15015 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public virtual PolicyLevel DomainPolicy
		{
			get
			{
				return null;
			}
		}

		/// <summary>Gets the flag representing the security policy components of concern to the host.</summary>
		/// <returns>One of the enumeration values that specifies security policy components. The default is <see cref="F:System.Security.HostSecurityManagerOptions.AllFlags" />.</returns>
		// Token: 0x1700098D RID: 2445
		// (get) Token: 0x06003AA8 RID: 15016 RVA: 0x000CB94F File Offset: 0x000C9B4F
		public virtual HostSecurityManagerOptions Flags
		{
			get
			{
				return HostSecurityManagerOptions.AllFlags;
			}
		}

		/// <summary>Determines whether an application should be executed.</summary>
		/// <param name="applicationEvidence">The evidence for the application to be activated.</param>
		/// <param name="activatorEvidence">Optionally, the evidence for the activating application domain. </param>
		/// <param name="context">The trust context. </param>
		/// <returns>An object that contains trust information about the application.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="applicationEvidence" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">An <see cref="T:System.Runtime.Hosting.ActivationArguments" /> object could not be found in the application evidence.-or-The <see cref="P:System.Runtime.Hosting.ActivationArguments.ActivationContext" /> property in the activation arguments is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Security.Policy.ApplicationTrust" /> grant set does not contain the minimum request set specified by the <see cref="T:System.ActivationContext" />.</exception>
		// Token: 0x06003AA9 RID: 15017 RVA: 0x000CB954 File Offset: 0x000C9B54
		public virtual ApplicationTrust DetermineApplicationTrust(Evidence applicationEvidence, Evidence activatorEvidence, TrustManagerContext context)
		{
			if (applicationEvidence == null)
			{
				throw new ArgumentNullException("applicationEvidence");
			}
			ActivationArguments activationArguments = null;
			foreach (object obj in applicationEvidence)
			{
				activationArguments = (obj as ActivationArguments);
				if (activationArguments != null)
				{
					break;
				}
			}
			if (activationArguments == null)
			{
				throw new ArgumentException(string.Format(Locale.GetText("No {0} found in {1}."), "ActivationArguments", "Evidence"), "applicationEvidence");
			}
			if (activationArguments.ActivationContext == null)
			{
				throw new ArgumentException(string.Format(Locale.GetText("No {0} found in {1}."), "ActivationContext", "ActivationArguments"), "applicationEvidence");
			}
			if (!ApplicationSecurityManager.DetermineApplicationTrust(activationArguments.ActivationContext, context))
			{
				return null;
			}
			if (activationArguments.ApplicationIdentity == null)
			{
				return new ApplicationTrust();
			}
			return new ApplicationTrust(activationArguments.ApplicationIdentity);
		}

		/// <summary>Provides the application domain evidence for an assembly being loaded.</summary>
		/// <param name="inputEvidence">Additional evidence to add to the <see cref="T:System.AppDomain" /> evidence.</param>
		/// <returns>The evidence to be used for the <see cref="T:System.AppDomain" />.</returns>
		// Token: 0x06003AAA RID: 15018 RVA: 0x0000207C File Offset: 0x0000027C
		public virtual Evidence ProvideAppDomainEvidence(Evidence inputEvidence)
		{
			return inputEvidence;
		}

		/// <summary>Provides the assembly evidence for an assembly being loaded.</summary>
		/// <param name="loadedAssembly">The loaded assembly. </param>
		/// <param name="inputEvidence">Additional evidence to add to the assembly evidence.</param>
		/// <returns>The evidence to be used for the assembly.</returns>
		// Token: 0x06003AAB RID: 15019 RVA: 0x0006DB95 File Offset: 0x0006BD95
		public virtual Evidence ProvideAssemblyEvidence(Assembly loadedAssembly, Evidence inputEvidence)
		{
			return inputEvidence;
		}

		/// <summary>Determines what permissions to grant to code based on the specified evidence.</summary>
		/// <param name="evidence">The evidence set used to evaluate policy.</param>
		/// <returns>The permission set that can be granted by the security system.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="evidence" /> is <see langword="null" />.</exception>
		// Token: 0x06003AAC RID: 15020 RVA: 0x000CBA34 File Offset: 0x000C9C34
		public virtual PermissionSet ResolvePolicy(Evidence evidence)
		{
			if (evidence == null)
			{
				throw new NullReferenceException("evidence");
			}
			return SecurityManager.ResolvePolicy(evidence);
		}

		/// <summary>Requests a specific evidence type for the application domain.</summary>
		/// <param name="evidenceType">The evidence type.</param>
		/// <returns>The requested application domain evidence.</returns>
		// Token: 0x06003AAD RID: 15021 RVA: 0x0005AB11 File Offset: 0x00058D11
		public virtual EvidenceBase GenerateAppDomainEvidence(Type evidenceType)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Requests a specific evidence type for the assembly.</summary>
		/// <param name="evidenceType">The evidence type.</param>
		/// <param name="assembly">The target assembly.</param>
		/// <returns>The requested assembly evidence.</returns>
		// Token: 0x06003AAE RID: 15022 RVA: 0x0005AB11 File Offset: 0x00058D11
		public virtual EvidenceBase GenerateAssemblyEvidence(Type evidenceType, Assembly assembly)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Determines which evidence types the host can supply for the application domain, if requested.</summary>
		/// <returns>An array of evidence types.</returns>
		// Token: 0x06003AAF RID: 15023 RVA: 0x0005AB11 File Offset: 0x00058D11
		public virtual Type[] GetHostSuppliedAppDomainEvidenceTypes()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		/// <summary>Determines which evidence types the host can supply for the assembly, if requested.</summary>
		/// <param name="assembly">The target assembly.</param>
		/// <returns>An array of evidence types.</returns>
		// Token: 0x06003AB0 RID: 15024 RVA: 0x0005AB11 File Offset: 0x00058D11
		public virtual Type[] GetHostSuppliedAssemblyEvidenceTypes(Assembly assembly)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
