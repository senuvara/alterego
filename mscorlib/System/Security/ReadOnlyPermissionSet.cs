﻿using System;
using System.Collections;
using Unity;

namespace System.Security
{
	/// <summary>Represents a read-only collection that can contain many different types of permissions.</summary>
	// Token: 0x02000A97 RID: 2711
	[Serializable]
	public sealed class ReadOnlyPermissionSet : PermissionSet
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.ReadOnlyPermissionSet" /> class. </summary>
		/// <param name="permissionSetXml">The XML element from which to take the value of the new <see cref="T:System.Security.ReadOnlyPermissionSet" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="permissionSetXml" /> is <see langword="null" />.</exception>
		// Token: 0x06005ECB RID: 24267 RVA: 0x00002ABD File Offset: 0x00000CBD
		public ReadOnlyPermissionSet(SecurityElement permissionSetXml)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x06005ECC RID: 24268 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override IPermission AddPermissionImpl(IPermission perm)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06005ECD RID: 24269 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override IEnumerator GetEnumeratorImpl()
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06005ECE RID: 24270 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override IPermission GetPermissionImpl(Type permClass)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06005ECF RID: 24271 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override IPermission RemovePermissionImpl(Type permClass)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}

		// Token: 0x06005ED0 RID: 24272 RVA: 0x0005AB11 File Offset: 0x00058D11
		protected override IPermission SetPermissionImpl(IPermission perm)
		{
			ThrowStub.ThrowNotSupportedException();
			return null;
		}
	}
}
