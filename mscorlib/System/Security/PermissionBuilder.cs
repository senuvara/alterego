﻿using System;
using System.Security.Permissions;

namespace System.Security
{
	// Token: 0x0200055D RID: 1373
	internal static class PermissionBuilder
	{
		// Token: 0x06003ACE RID: 15054 RVA: 0x000CBBE4 File Offset: 0x000C9DE4
		public static IPermission Create(string fullname, PermissionState state)
		{
			if (fullname == null)
			{
				throw new ArgumentNullException("fullname");
			}
			SecurityElement securityElement = new SecurityElement("IPermission");
			securityElement.AddAttribute("class", fullname);
			securityElement.AddAttribute("version", "1");
			if (state == PermissionState.Unrestricted)
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			return PermissionBuilder.CreatePermission(fullname, securityElement);
		}

		// Token: 0x06003ACF RID: 15055 RVA: 0x000CBC44 File Offset: 0x000C9E44
		public static IPermission Create(SecurityElement se)
		{
			if (se == null)
			{
				throw new ArgumentNullException("se");
			}
			string text = se.Attribute("class");
			if (text == null || text.Length == 0)
			{
				throw new ArgumentException("class");
			}
			return PermissionBuilder.CreatePermission(text, se);
		}

		// Token: 0x06003AD0 RID: 15056 RVA: 0x000CBC88 File Offset: 0x000C9E88
		public static IPermission Create(string fullname, SecurityElement se)
		{
			if (fullname == null)
			{
				throw new ArgumentNullException("fullname");
			}
			if (se == null)
			{
				throw new ArgumentNullException("se");
			}
			return PermissionBuilder.CreatePermission(fullname, se);
		}

		// Token: 0x06003AD1 RID: 15057 RVA: 0x000CBCAD File Offset: 0x000C9EAD
		public static IPermission Create(Type type)
		{
			return (IPermission)Activator.CreateInstance(type, PermissionBuilder.psNone);
		}

		// Token: 0x06003AD2 RID: 15058 RVA: 0x000CBCBF File Offset: 0x000C9EBF
		internal static IPermission CreatePermission(string fullname, SecurityElement se)
		{
			Type type = Type.GetType(fullname);
			if (type == null)
			{
				throw new TypeLoadException(string.Format(Locale.GetText("Can't create an instance of permission class {0}."), fullname));
			}
			IPermission permission = PermissionBuilder.Create(type);
			permission.FromXml(se);
			return permission;
		}

		// Token: 0x06003AD3 RID: 15059 RVA: 0x000CBCF2 File Offset: 0x000C9EF2
		// Note: this type is marked as 'beforefieldinit'.
		static PermissionBuilder()
		{
		}

		// Token: 0x04001E7F RID: 7807
		private static object[] psNone = new object[]
		{
			PermissionState.None
		};
	}
}
