﻿using System;

namespace System.Security
{
	// Token: 0x02000543 RID: 1347
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	internal sealed class DynamicSecurityMethodAttribute : Attribute
	{
		// Token: 0x06003A56 RID: 14934 RVA: 0x000020BF File Offset: 0x000002BF
		public DynamicSecurityMethodAttribute()
		{
		}
	}
}
