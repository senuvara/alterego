﻿using System;

namespace System.Security
{
	/// <summary>Provides a base class for requesting the security status of an action from the <see cref="T:System.AppDomainManager" /> object.</summary>
	// Token: 0x02000568 RID: 1384
	public abstract class SecurityState
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityState" /> class. </summary>
		// Token: 0x06003B81 RID: 15233 RVA: 0x00002050 File Offset: 0x00000250
		protected SecurityState()
		{
		}

		/// <summary>When overridden in a derived class, ensures that the state that is represented by <see cref="T:System.Security.SecurityState" /> is available on the host.</summary>
		// Token: 0x06003B82 RID: 15234
		public abstract void EnsureState();

		/// <summary>Gets a value that indicates whether the state for this implementation of the <see cref="T:System.Security.SecurityState" /> class is available on the current host. </summary>
		/// <returns>
		///     <see langword="true" /> if the state is available; otherwise, <see langword="false" />. </returns>
		// Token: 0x06003B83 RID: 15235 RVA: 0x000CE360 File Offset: 0x000CC560
		public bool IsStateAvailable()
		{
			AppDomainManager domainManager = AppDomain.CurrentDomain.DomainManager;
			return domainManager != null && domainManager.CheckSecuritySettings(this);
		}
	}
}
