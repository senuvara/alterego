﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	/// <summary>Allows managed code to call into unmanaged code without a stack walk. This class cannot be inherited.</summary>
	// Token: 0x02000544 RID: 1348
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Interface | AttributeTargets.Delegate, AllowMultiple = true, Inherited = false)]
	public sealed class SuppressUnmanagedCodeSecurityAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SuppressUnmanagedCodeSecurityAttribute" /> class. </summary>
		// Token: 0x06003A57 RID: 14935 RVA: 0x000020BF File Offset: 0x000002BF
		public SuppressUnmanagedCodeSecurityAttribute()
		{
		}
	}
}
