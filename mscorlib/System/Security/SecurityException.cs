﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Security.Policy;
using Unity;

namespace System.Security
{
	/// <summary>The exception that is thrown when a security error is detected.</summary>
	// Token: 0x02000564 RID: 1380
	[ComVisible(true)]
	[Serializable]
	public class SecurityException : SystemException
	{
		/// <summary>Gets or sets the security action that caused the exception.</summary>
		/// <returns>One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values.</returns>
		// Token: 0x170009A2 RID: 2466
		// (get) Token: 0x06003B3F RID: 15167 RVA: 0x000CE0E8 File Offset: 0x000CC2E8
		// (set) Token: 0x06003B40 RID: 15168 RVA: 0x000CE0F0 File Offset: 0x000CC2F0
		[ComVisible(false)]
		public SecurityAction Action
		{
			get
			{
				return this._action;
			}
			set
			{
				this._action = value;
			}
		}

		/// <summary>Gets or sets the denied security permission, permission set, or permission set collection that caused a demand to fail.</summary>
		/// <returns>A permission, permission set, or permission set collection object.</returns>
		// Token: 0x170009A3 RID: 2467
		// (get) Token: 0x06003B41 RID: 15169 RVA: 0x000CE0F9 File Offset: 0x000CC2F9
		// (set) Token: 0x06003B42 RID: 15170 RVA: 0x000CE101 File Offset: 0x000CC301
		[ComVisible(false)]
		public object DenySetInstance
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._denyset;
			}
			set
			{
				this._denyset = value;
			}
		}

		/// <summary>Gets or sets information about the failed assembly.</summary>
		/// <returns>An <see cref="T:System.Reflection.AssemblyName" /> that identifies the failed assembly.</returns>
		// Token: 0x170009A4 RID: 2468
		// (get) Token: 0x06003B43 RID: 15171 RVA: 0x000CE10A File Offset: 0x000CC30A
		// (set) Token: 0x06003B44 RID: 15172 RVA: 0x000CE112 File Offset: 0x000CC312
		[ComVisible(false)]
		public AssemblyName FailedAssemblyInfo
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._assembly;
			}
			set
			{
				this._assembly = value;
			}
		}

		/// <summary>Gets or sets the information about the method associated with the exception.</summary>
		/// <returns>A <see cref="T:System.Reflection.MethodInfo" /> object describing the method.</returns>
		// Token: 0x170009A5 RID: 2469
		// (get) Token: 0x06003B45 RID: 15173 RVA: 0x000CE11B File Offset: 0x000CC31B
		// (set) Token: 0x06003B46 RID: 15174 RVA: 0x000CE123 File Offset: 0x000CC323
		[ComVisible(false)]
		public MethodInfo Method
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._method;
			}
			set
			{
				this._method = value;
			}
		}

		/// <summary>Gets or sets the permission, permission set, or permission set collection that is part of the permit-only stack frame that caused a security check to fail.</summary>
		/// <returns>A permission, permission set, or permission set collection object.</returns>
		// Token: 0x170009A6 RID: 2470
		// (get) Token: 0x06003B47 RID: 15175 RVA: 0x000CE12C File Offset: 0x000CC32C
		// (set) Token: 0x06003B48 RID: 15176 RVA: 0x000CE134 File Offset: 0x000CC334
		[ComVisible(false)]
		public object PermitOnlySetInstance
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._permitset;
			}
			set
			{
				this._permitset = value;
			}
		}

		/// <summary>Gets or sets the URL of the assembly that caused the exception.</summary>
		/// <returns>A URL that identifies the location of the assembly.</returns>
		// Token: 0x170009A7 RID: 2471
		// (get) Token: 0x06003B49 RID: 15177 RVA: 0x000CE13D File Offset: 0x000CC33D
		// (set) Token: 0x06003B4A RID: 15178 RVA: 0x000CE145 File Offset: 0x000CC345
		public string Url
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._url;
			}
			set
			{
				this._url = value;
			}
		}

		/// <summary>Gets or sets the zone of the assembly that caused the exception.</summary>
		/// <returns>One of the <see cref="T:System.Security.SecurityZone" /> values that identifies the zone of the assembly that caused the exception.</returns>
		// Token: 0x170009A8 RID: 2472
		// (get) Token: 0x06003B4B RID: 15179 RVA: 0x000CE14E File Offset: 0x000CC34E
		// (set) Token: 0x06003B4C RID: 15180 RVA: 0x000CE156 File Offset: 0x000CC356
		public SecurityZone Zone
		{
			get
			{
				return this._zone;
			}
			set
			{
				this._zone = value;
			}
		}

		/// <summary>Gets or sets the demanded security permission, permission set, or permission set collection that failed.</summary>
		/// <returns>A permission, permission set, or permission set collection object.</returns>
		// Token: 0x170009A9 RID: 2473
		// (get) Token: 0x06003B4D RID: 15181 RVA: 0x000CE15F File Offset: 0x000CC35F
		// (set) Token: 0x06003B4E RID: 15182 RVA: 0x000CE167 File Offset: 0x000CC367
		[ComVisible(false)]
		public object Demanded
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._demanded;
			}
			set
			{
				this._demanded = value;
			}
		}

		/// <summary>Gets or sets the first permission in a permission set or permission set collection that failed the demand.</summary>
		/// <returns>An <see cref="T:System.Security.IPermission" /> object representing the first permission that failed.</returns>
		// Token: 0x170009AA RID: 2474
		// (get) Token: 0x06003B4F RID: 15183 RVA: 0x000CE170 File Offset: 0x000CC370
		// (set) Token: 0x06003B50 RID: 15184 RVA: 0x000CE178 File Offset: 0x000CC378
		public IPermission FirstPermissionThatFailed
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._firstperm;
			}
			set
			{
				this._firstperm = value;
			}
		}

		/// <summary>Gets or sets the state of the permission that threw the exception.</summary>
		/// <returns>The state of the permission at the time the exception was thrown.</returns>
		// Token: 0x170009AB RID: 2475
		// (get) Token: 0x06003B51 RID: 15185 RVA: 0x000CE181 File Offset: 0x000CC381
		// (set) Token: 0x06003B52 RID: 15186 RVA: 0x000CE189 File Offset: 0x000CC389
		public string PermissionState
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this.permissionState;
			}
			set
			{
				this.permissionState = value;
			}
		}

		/// <summary>Gets or sets the type of the permission that failed.</summary>
		/// <returns>The type of the permission that failed.</returns>
		// Token: 0x170009AC RID: 2476
		// (get) Token: 0x06003B53 RID: 15187 RVA: 0x000CE192 File Offset: 0x000CC392
		// (set) Token: 0x06003B54 RID: 15188 RVA: 0x000CE19A File Offset: 0x000CC39A
		public Type PermissionType
		{
			get
			{
				return this.permissionType;
			}
			set
			{
				this.permissionType = value;
			}
		}

		/// <summary>Gets or sets the granted permission set of the assembly that caused the <see cref="T:System.Security.SecurityException" />.</summary>
		/// <returns>The XML representation of the granted set of the assembly.</returns>
		// Token: 0x170009AD RID: 2477
		// (get) Token: 0x06003B55 RID: 15189 RVA: 0x000CE1A3 File Offset: 0x000CC3A3
		// (set) Token: 0x06003B56 RID: 15190 RVA: 0x000CE1AB File Offset: 0x000CC3AB
		public string GrantedSet
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._granted;
			}
			set
			{
				this._granted = value;
			}
		}

		/// <summary>Gets or sets the refused permission set of the assembly that caused the <see cref="T:System.Security.SecurityException" />.</summary>
		/// <returns>The XML representation of the refused permission set of the assembly.</returns>
		// Token: 0x170009AE RID: 2478
		// (get) Token: 0x06003B57 RID: 15191 RVA: 0x000CE1B4 File Offset: 0x000CC3B4
		// (set) Token: 0x06003B58 RID: 15192 RVA: 0x000CE1BC File Offset: 0x000CC3BC
		public string RefusedSet
		{
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true, ControlPolicy = true)]
			get
			{
				return this._refused;
			}
			set
			{
				this._refused = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityException" /> class with default properties.</summary>
		// Token: 0x06003B59 RID: 15193 RVA: 0x000CE1C5 File Offset: 0x000CC3C5
		public SecurityException() : this(Locale.GetText("A security error has been detected."))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x06003B5A RID: 15194 RVA: 0x000CE1D7 File Offset: 0x000CC3D7
		public SecurityException(string message) : base(message)
		{
			base.HResult = -2146233078;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">The contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="info " />is<see langword=" null" />.</exception>
		// Token: 0x06003B5B RID: 15195 RVA: 0x000CE1EC File Offset: 0x000CC3EC
		protected SecurityException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			base.HResult = -2146233078;
			SerializationInfoEnumerator enumerator = info.GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (enumerator.Name == "PermissionState")
				{
					this.permissionState = (string)enumerator.Value;
					return;
				}
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06003B5C RID: 15196 RVA: 0x000CE241 File Offset: 0x000CC441
		public SecurityException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233078;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityException" /> class with a specified error message and the permission type that caused the exception to be thrown.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="type">The type of the permission that caused the exception to be thrown. </param>
		// Token: 0x06003B5D RID: 15197 RVA: 0x000CE256 File Offset: 0x000CC456
		public SecurityException(string message, Type type) : base(message)
		{
			base.HResult = -2146233078;
			this.permissionType = type;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityException" /> class with a specified error message, the permission type that caused the exception to be thrown, and the permission state.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="type">The type of the permission that caused the exception to be thrown. </param>
		/// <param name="state">The state of the permission that caused the exception to be thrown. </param>
		// Token: 0x06003B5E RID: 15198 RVA: 0x000CE271 File Offset: 0x000CC471
		public SecurityException(string message, Type type, string state) : base(message)
		{
			base.HResult = -2146233078;
			this.permissionType = type;
			this.permissionState = state;
		}

		// Token: 0x06003B5F RID: 15199 RVA: 0x000CE293 File Offset: 0x000CC493
		internal SecurityException(string message, PermissionSet granted, PermissionSet refused) : base(message)
		{
			base.HResult = -2146233078;
			this._granted = granted.ToString();
			this._refused = refused.ToString();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityException" /> class for an exception caused by a Deny on the stack.  </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="deny">The denied permission or permission set.</param>
		/// <param name="permitOnly">The permit-only permission or permission set.</param>
		/// <param name="method">A <see cref="T:System.Reflection.MethodInfo" /> that identifies the method that encountered the exception.</param>
		/// <param name="demanded">The demanded permission, permission set, or permission set collection.</param>
		/// <param name="permThatFailed">An <see cref="T:System.Security.IPermission" /> that identifies the permission that failed.</param>
		// Token: 0x06003B60 RID: 15200 RVA: 0x000CE2BF File Offset: 0x000CC4BF
		public SecurityException(string message, object deny, object permitOnly, MethodInfo method, object demanded, IPermission permThatFailed) : base(message)
		{
			base.HResult = -2146233078;
			this._denyset = deny;
			this._permitset = permitOnly;
			this._method = method;
			this._demanded = demanded;
			this._firstperm = permThatFailed;
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with information about the <see cref="T:System.Security.SecurityException" />.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06003B61 RID: 15201 RVA: 0x000CE2FC File Offset: 0x000CC4FC
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			try
			{
				info.AddValue("PermissionState", this.permissionState);
			}
			catch (SecurityException)
			{
			}
		}

		/// <summary>Returns a representation of the current <see cref="T:System.Security.SecurityException" />.</summary>
		/// <returns>A string representation of the current <see cref="T:System.Security.SecurityException" />.</returns>
		// Token: 0x06003B62 RID: 15202 RVA: 0x000CB947 File Offset: 0x000C9B47
		[SecuritySafeCritical]
		public override string ToString()
		{
			return base.ToString();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityException" /> class for an exception caused by an insufficient grant set. </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="assemblyName">An <see cref="T:System.Reflection.AssemblyName" /> that specifies the name of the assembly that caused the exception.</param>
		/// <param name="grant">A <see cref="T:System.Security.PermissionSet" /> that represents the permissions granted the assembly.</param>
		/// <param name="refused">A <see cref="T:System.Security.PermissionSet" /> that represents the refused permission or permission set.</param>
		/// <param name="method">A <see cref="T:System.Reflection.MethodInfo" /> that represents the method that encountered the exception.</param>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values.</param>
		/// <param name="demanded">The demanded permission, permission set, or permission set collection.</param>
		/// <param name="permThatFailed">An <see cref="T:System.Security.IPermission" /> that represents the permission that failed.</param>
		/// <param name="evidence">The <see cref="T:System.Security.Policy.Evidence" /> for the assembly that caused the exception.</param>
		// Token: 0x06003B63 RID: 15203 RVA: 0x00002ABD File Offset: 0x00000CBD
		[SecuritySafeCritical]
		public SecurityException(string message, AssemblyName assemblyName, PermissionSet grant, PermissionSet refused, MethodInfo method, SecurityAction action, object demanded, IPermission permThatFailed, Evidence evidence)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04001EA4 RID: 7844
		private string permissionState;

		// Token: 0x04001EA5 RID: 7845
		private Type permissionType;

		// Token: 0x04001EA6 RID: 7846
		private string _granted;

		// Token: 0x04001EA7 RID: 7847
		private string _refused;

		// Token: 0x04001EA8 RID: 7848
		private object _demanded;

		// Token: 0x04001EA9 RID: 7849
		private IPermission _firstperm;

		// Token: 0x04001EAA RID: 7850
		private MethodInfo _method;

		// Token: 0x04001EAB RID: 7851
		private SecurityAction _action;

		// Token: 0x04001EAC RID: 7852
		private object _denyset;

		// Token: 0x04001EAD RID: 7853
		private object _permitset;

		// Token: 0x04001EAE RID: 7854
		private AssemblyName _assembly;

		// Token: 0x04001EAF RID: 7855
		private string _url;

		// Token: 0x04001EB0 RID: 7856
		private SecurityZone _zone;
	}
}
