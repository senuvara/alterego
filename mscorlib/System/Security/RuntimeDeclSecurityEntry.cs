﻿using System;

namespace System.Security
{
	// Token: 0x02000565 RID: 1381
	internal struct RuntimeDeclSecurityEntry
	{
		// Token: 0x04001EB1 RID: 7857
		public IntPtr blob;

		// Token: 0x04001EB2 RID: 7858
		public int size;

		// Token: 0x04001EB3 RID: 7859
		public int index;
	}
}
