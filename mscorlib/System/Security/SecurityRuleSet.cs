﻿using System;

namespace System.Security
{
	/// <summary>Identifies the set of security rules the common language runtime should enforce for an assembly.  </summary>
	// Token: 0x0200054D RID: 1357
	public enum SecurityRuleSet : byte
	{
		/// <summary>Unsupported. Using this value results in a <see cref="T:System.IO.FileLoadException" /> being thrown.</summary>
		// Token: 0x04001E61 RID: 7777
		None,
		/// <summary>Indicates that the runtime will enforce level 1 (.NET Framework version 2.0) transparency rules.</summary>
		// Token: 0x04001E62 RID: 7778
		Level1,
		/// <summary>Indicates that the runtime will enforce level 2 transparency rules.</summary>
		// Token: 0x04001E63 RID: 7779
		Level2
	}
}
