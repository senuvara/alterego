﻿using System;

namespace System.Security.Util
{
	// Token: 0x02000633 RID: 1587
	internal sealed class TokenizerShortBlock
	{
		// Token: 0x0600424E RID: 16974 RVA: 0x000E0D7C File Offset: 0x000DEF7C
		public TokenizerShortBlock()
		{
		}

		// Token: 0x04002185 RID: 8581
		internal short[] m_block = new short[16];

		// Token: 0x04002186 RID: 8582
		internal TokenizerShortBlock m_next;
	}
}
