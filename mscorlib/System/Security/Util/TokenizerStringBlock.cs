﻿using System;

namespace System.Security.Util
{
	// Token: 0x02000634 RID: 1588
	internal sealed class TokenizerStringBlock
	{
		// Token: 0x0600424F RID: 16975 RVA: 0x000E0D91 File Offset: 0x000DEF91
		public TokenizerStringBlock()
		{
		}

		// Token: 0x04002187 RID: 8583
		internal string[] m_block = new string[16];

		// Token: 0x04002188 RID: 8584
		internal TokenizerStringBlock m_next;
	}
}
