﻿using System;

namespace System.Security.Util
{
	// Token: 0x02000635 RID: 1589
	internal sealed class TokenizerStream
	{
		// Token: 0x06004250 RID: 16976 RVA: 0x000E0DA6 File Offset: 0x000DEFA6
		internal TokenizerStream()
		{
			this.m_countTokens = 0;
			this.m_headTokens = new TokenizerShortBlock();
			this.m_headStrings = new TokenizerStringBlock();
			this.Reset();
		}

		// Token: 0x06004251 RID: 16977 RVA: 0x000E0DD4 File Offset: 0x000DEFD4
		internal void AddToken(short token)
		{
			if (this.m_currentTokens.m_block.Length <= this.m_indexTokens)
			{
				this.m_currentTokens.m_next = new TokenizerShortBlock();
				this.m_currentTokens = this.m_currentTokens.m_next;
				this.m_indexTokens = 0;
			}
			this.m_countTokens++;
			short[] block = this.m_currentTokens.m_block;
			int indexTokens = this.m_indexTokens;
			this.m_indexTokens = indexTokens + 1;
			block[indexTokens] = token;
		}

		// Token: 0x06004252 RID: 16978 RVA: 0x000E0E4C File Offset: 0x000DF04C
		internal void AddString(string str)
		{
			if (this.m_currentStrings.m_block.Length <= this.m_indexStrings)
			{
				this.m_currentStrings.m_next = new TokenizerStringBlock();
				this.m_currentStrings = this.m_currentStrings.m_next;
				this.m_indexStrings = 0;
			}
			string[] block = this.m_currentStrings.m_block;
			int indexStrings = this.m_indexStrings;
			this.m_indexStrings = indexStrings + 1;
			block[indexStrings] = str;
		}

		// Token: 0x06004253 RID: 16979 RVA: 0x000E0EB4 File Offset: 0x000DF0B4
		internal void Reset()
		{
			this.m_lastTokens = null;
			this.m_currentTokens = this.m_headTokens;
			this.m_currentStrings = this.m_headStrings;
			this.m_indexTokens = 0;
			this.m_indexStrings = 0;
		}

		// Token: 0x06004254 RID: 16980 RVA: 0x000E0EE4 File Offset: 0x000DF0E4
		internal short GetNextFullToken()
		{
			if (this.m_currentTokens.m_block.Length <= this.m_indexTokens)
			{
				this.m_lastTokens = this.m_currentTokens;
				this.m_currentTokens = this.m_currentTokens.m_next;
				this.m_indexTokens = 0;
			}
			short[] block = this.m_currentTokens.m_block;
			int indexTokens = this.m_indexTokens;
			this.m_indexTokens = indexTokens + 1;
			return block[indexTokens];
		}

		// Token: 0x06004255 RID: 16981 RVA: 0x000E0F47 File Offset: 0x000DF147
		internal short GetNextToken()
		{
			return this.GetNextFullToken() & 255;
		}

		// Token: 0x06004256 RID: 16982 RVA: 0x000E0F58 File Offset: 0x000DF158
		internal string GetNextString()
		{
			if (this.m_currentStrings.m_block.Length <= this.m_indexStrings)
			{
				this.m_currentStrings = this.m_currentStrings.m_next;
				this.m_indexStrings = 0;
			}
			string[] block = this.m_currentStrings.m_block;
			int indexStrings = this.m_indexStrings;
			this.m_indexStrings = indexStrings + 1;
			return block[indexStrings];
		}

		// Token: 0x06004257 RID: 16983 RVA: 0x000E0FAF File Offset: 0x000DF1AF
		internal void ThrowAwayNextString()
		{
			this.GetNextString();
		}

		// Token: 0x06004258 RID: 16984 RVA: 0x000E0FB8 File Offset: 0x000DF1B8
		internal void TagLastToken(short tag)
		{
			if (this.m_indexTokens == 0)
			{
				this.m_lastTokens.m_block[this.m_lastTokens.m_block.Length - 1] = (short)((ushort)this.m_lastTokens.m_block[this.m_lastTokens.m_block.Length - 1] | (ushort)tag);
				return;
			}
			this.m_currentTokens.m_block[this.m_indexTokens - 1] = (short)((ushort)this.m_currentTokens.m_block[this.m_indexTokens - 1] | (ushort)tag);
		}

		// Token: 0x06004259 RID: 16985 RVA: 0x000E1036 File Offset: 0x000DF236
		internal int GetTokenCount()
		{
			return this.m_countTokens;
		}

		// Token: 0x0600425A RID: 16986 RVA: 0x000E1040 File Offset: 0x000DF240
		internal void GoToPosition(int position)
		{
			this.Reset();
			for (int i = 0; i < position; i++)
			{
				if (this.GetNextToken() == 3)
				{
					this.ThrowAwayNextString();
				}
			}
		}

		// Token: 0x04002189 RID: 8585
		private int m_countTokens;

		// Token: 0x0400218A RID: 8586
		private TokenizerShortBlock m_headTokens;

		// Token: 0x0400218B RID: 8587
		private TokenizerShortBlock m_lastTokens;

		// Token: 0x0400218C RID: 8588
		private TokenizerShortBlock m_currentTokens;

		// Token: 0x0400218D RID: 8589
		private int m_indexTokens;

		// Token: 0x0400218E RID: 8590
		private TokenizerStringBlock m_headStrings;

		// Token: 0x0400218F RID: 8591
		private TokenizerStringBlock m_currentStrings;

		// Token: 0x04002190 RID: 8592
		private int m_indexStrings;
	}
}
