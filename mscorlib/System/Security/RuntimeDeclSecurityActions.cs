﻿using System;

namespace System.Security
{
	// Token: 0x02000566 RID: 1382
	internal struct RuntimeDeclSecurityActions
	{
		// Token: 0x04001EB4 RID: 7860
		public RuntimeDeclSecurityEntry cas;

		// Token: 0x04001EB5 RID: 7861
		public RuntimeDeclSecurityEntry noncas;

		// Token: 0x04001EB6 RID: 7862
		public RuntimeDeclSecurityEntry choice;
	}
}
