﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Represents the base class from which all asymmetric key exchange deformatters derive.</summary>
	// Token: 0x0200065C RID: 1628
	[ComVisible(true)]
	public abstract class AsymmetricKeyExchangeDeformatter
	{
		/// <summary>Initializes a new instance of <see cref="T:System.Security.Cryptography.AsymmetricKeyExchangeDeformatter" />.</summary>
		// Token: 0x060043D2 RID: 17362 RVA: 0x00002050 File Offset: 0x00000250
		protected AsymmetricKeyExchangeDeformatter()
		{
		}

		/// <summary>When overridden in a derived class, gets or sets the parameters for the asymmetric key exchange.</summary>
		/// <returns>A string in XML format containing the parameters of the asymmetric key exchange operation.</returns>
		// Token: 0x17000B6B RID: 2923
		// (get) Token: 0x060043D3 RID: 17363
		// (set) Token: 0x060043D4 RID: 17364
		public abstract string Parameters { get; set; }

		/// <summary>When overridden in a derived class, sets the private key to use for decrypting the secret information.</summary>
		/// <param name="key">The instance of the implementation of <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> that holds the private key. </param>
		// Token: 0x060043D5 RID: 17365
		public abstract void SetKey(AsymmetricAlgorithm key);

		/// <summary>When overridden in a derived class, extracts secret information from the encrypted key exchange data.</summary>
		/// <param name="rgb">The key exchange data within which the secret information is hidden. </param>
		/// <returns>The secret information derived from the key exchange data.</returns>
		// Token: 0x060043D6 RID: 17366
		public abstract byte[] DecryptKeyExchange(byte[] rgb);
	}
}
