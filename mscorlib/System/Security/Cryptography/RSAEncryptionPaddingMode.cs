﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the padding mode to use with RSA encryption or decryption operations. </summary>
	// Token: 0x02000657 RID: 1623
	public enum RSAEncryptionPaddingMode
	{
		/// <summary>PKCS #1 v1.5. </summary>
		// Token: 0x040022F0 RID: 8944
		Pkcs1,
		/// <summary>Optimal Asymmetric Encryption Padding. </summary>
		// Token: 0x040022F1 RID: 8945
		Oaep
	}
}
