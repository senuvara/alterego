﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the mode of a cryptographic stream.</summary>
	// Token: 0x0200066A RID: 1642
	[ComVisible(true)]
	[Serializable]
	public enum CryptoStreamMode
	{
		/// <summary>Read access to a cryptographic stream.</summary>
		// Token: 0x04002327 RID: 8999
		Read,
		/// <summary>Write access to a cryptographic stream.</summary>
		// Token: 0x04002328 RID: 9000
		Write
	}
}
