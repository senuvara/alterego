﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x020006A5 RID: 1701
	internal class DSASignatureDescription : SignatureDescription
	{
		// Token: 0x0600460C RID: 17932 RVA: 0x000F209E File Offset: 0x000F029E
		public DSASignatureDescription()
		{
			base.KeyAlgorithm = "System.Security.Cryptography.DSACryptoServiceProvider";
			base.DigestAlgorithm = "System.Security.Cryptography.SHA1CryptoServiceProvider";
			base.FormatterAlgorithm = "System.Security.Cryptography.DSASignatureFormatter";
			base.DeformatterAlgorithm = "System.Security.Cryptography.DSASignatureDeformatter";
		}
	}
}
