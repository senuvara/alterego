﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Specifies whether white space should be ignored in the base 64 transformation.</summary>
	// Token: 0x02000660 RID: 1632
	[ComVisible(true)]
	[Serializable]
	public enum FromBase64TransformMode
	{
		/// <summary>White space should be ignored.</summary>
		// Token: 0x040022FD RID: 8957
		IgnoreWhiteSpaces,
		/// <summary>White space should not be ignored.</summary>
		// Token: 0x040022FE RID: 8958
		DoNotIgnoreWhiteSpaces
	}
}
