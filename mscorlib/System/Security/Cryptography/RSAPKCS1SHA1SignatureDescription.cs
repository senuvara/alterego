﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x020006A1 RID: 1697
	internal class RSAPKCS1SHA1SignatureDescription : RSAPKCS1SignatureDescription
	{
		// Token: 0x06004608 RID: 17928 RVA: 0x000F2056 File Offset: 0x000F0256
		public RSAPKCS1SHA1SignatureDescription() : base("SHA1", "System.Security.Cryptography.SHA1Cng")
		{
		}
	}
}
