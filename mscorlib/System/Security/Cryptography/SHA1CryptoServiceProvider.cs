﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Computes the <see cref="T:System.Security.Cryptography.SHA1" /> hash value for the input data using the implementation provided by the cryptographic service provider (CSP). This class cannot be inherited. </summary>
	// Token: 0x020006B9 RID: 1721
	[ComVisible(true)]
	public sealed class SHA1CryptoServiceProvider : SHA1
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.SHA1CryptoServiceProvider" /> class.</summary>
		// Token: 0x060046CF RID: 18127 RVA: 0x000F7A58 File Offset: 0x000F5C58
		public SHA1CryptoServiceProvider()
		{
			this.sha = new SHA1Internal();
		}

		// Token: 0x060046D0 RID: 18128 RVA: 0x000F7A6C File Offset: 0x000F5C6C
		~SHA1CryptoServiceProvider()
		{
			this.Dispose(false);
		}

		// Token: 0x060046D1 RID: 18129 RVA: 0x000F7A9C File Offset: 0x000F5C9C
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		// Token: 0x060046D2 RID: 18130 RVA: 0x000F7AA5 File Offset: 0x000F5CA5
		protected override void HashCore(byte[] rgb, int ibStart, int cbSize)
		{
			this.State = 1;
			this.sha.HashCore(rgb, ibStart, cbSize);
		}

		// Token: 0x060046D3 RID: 18131 RVA: 0x000F7ABC File Offset: 0x000F5CBC
		protected override byte[] HashFinal()
		{
			this.State = 0;
			return this.sha.HashFinal();
		}

		/// <summary>Initializes an instance of <see cref="T:System.Security.Cryptography.SHA1CryptoServiceProvider" />.</summary>
		// Token: 0x060046D4 RID: 18132 RVA: 0x000F7AD0 File Offset: 0x000F5CD0
		public override void Initialize()
		{
			this.sha.Initialize();
		}

		// Token: 0x04002470 RID: 9328
		private SHA1Internal sha;
	}
}
