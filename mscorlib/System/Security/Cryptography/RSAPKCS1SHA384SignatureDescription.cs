﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x020006A3 RID: 1699
	internal class RSAPKCS1SHA384SignatureDescription : RSAPKCS1SignatureDescription
	{
		// Token: 0x0600460A RID: 17930 RVA: 0x000F207A File Offset: 0x000F027A
		public RSAPKCS1SHA384SignatureDescription() : base("SHA384", "System.Security.Cryptography.SHA384Cng")
		{
		}
	}
}
