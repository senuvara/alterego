﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x020006A4 RID: 1700
	internal class RSAPKCS1SHA512SignatureDescription : RSAPKCS1SignatureDescription
	{
		// Token: 0x0600460B RID: 17931 RVA: 0x000F208C File Offset: 0x000F028C
		public RSAPKCS1SHA512SignatureDescription() : base("SHA512", "System.Security.Cryptography.SHA512Cng")
		{
		}
	}
}
