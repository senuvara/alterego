﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200068C RID: 1676
	[Serializable]
	internal enum RijndaelManagedTransformMode
	{
		// Token: 0x04002392 RID: 9106
		Encrypt,
		// Token: 0x04002393 RID: 9107
		Decrypt
	}
}
