﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x020006A2 RID: 1698
	internal class RSAPKCS1SHA256SignatureDescription : RSAPKCS1SignatureDescription
	{
		// Token: 0x06004609 RID: 17929 RVA: 0x000F2068 File Offset: 0x000F0268
		public RSAPKCS1SHA256SignatureDescription() : base("SHA256", "System.Security.Cryptography.SHA256Cng")
		{
		}
	}
}
