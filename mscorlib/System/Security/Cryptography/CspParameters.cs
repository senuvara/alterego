﻿using System;
using System.Runtime.InteropServices;
using System.Security.AccessControl;

namespace System.Security.Cryptography
{
	/// <summary>Contains parameters that are passed to the cryptographic service provider (CSP) that performs cryptographic computations. This class cannot be inherited.</summary>
	// Token: 0x02000669 RID: 1641
	[ComVisible(true)]
	public sealed class CspParameters
	{
		/// <summary>Represents the flags for <see cref="T:System.Security.Cryptography.CspParameters" /> that modify the behavior of the cryptographic service provider (CSP).</summary>
		/// <returns>An enumeration value, or a bitwise combination of enumeration values.</returns>
		/// <exception cref="T:System.ArgumentException">Value is not a valid enumeration value.</exception>
		// Token: 0x17000B78 RID: 2936
		// (get) Token: 0x06004411 RID: 17425 RVA: 0x000E676E File Offset: 0x000E496E
		// (set) Token: 0x06004412 RID: 17426 RVA: 0x000E6778 File Offset: 0x000E4978
		public CspProviderFlags Flags
		{
			get
			{
				return (CspProviderFlags)this.m_flags;
			}
			set
			{
				int num = 255;
				if ((value & (CspProviderFlags)(~(CspProviderFlags)num)) != CspProviderFlags.NoFlags)
				{
					throw new ArgumentException(Environment.GetResourceString("Illegal enum value: {0}.", new object[]
					{
						(int)value
					}), "value");
				}
				this.m_flags = (int)value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Security.AccessControl.CryptoKeySecurity" /> object that represents access rights and audit rules for a container. </summary>
		/// <returns>A <see cref="T:System.Security.AccessControl.CryptoKeySecurity" /> object that represents access rights and audit rules for a container.</returns>
		// Token: 0x17000B79 RID: 2937
		// (get) Token: 0x06004413 RID: 17427 RVA: 0x000E67BE File Offset: 0x000E49BE
		// (set) Token: 0x06004414 RID: 17428 RVA: 0x000E67C6 File Offset: 0x000E49C6
		public CryptoKeySecurity CryptoKeySecurity
		{
			get
			{
				return this.m_cryptoKeySecurity;
			}
			set
			{
				this.m_cryptoKeySecurity = value;
			}
		}

		/// <summary>Gets or sets a password associated with a smart card key. </summary>
		/// <returns>A password associated with a smart card key.</returns>
		// Token: 0x17000B7A RID: 2938
		// (get) Token: 0x06004415 RID: 17429 RVA: 0x000E67CF File Offset: 0x000E49CF
		// (set) Token: 0x06004416 RID: 17430 RVA: 0x000E67D7 File Offset: 0x000E49D7
		public SecureString KeyPassword
		{
			get
			{
				return this.m_keyPassword;
			}
			set
			{
				this.m_keyPassword = value;
				this.m_parentWindowHandle = IntPtr.Zero;
			}
		}

		/// <summary>Gets or sets a handle to the unmanaged parent window for a smart card password dialog box.</summary>
		/// <returns>A handle to the parent window for a smart card password dialog box.</returns>
		// Token: 0x17000B7B RID: 2939
		// (get) Token: 0x06004417 RID: 17431 RVA: 0x000E67EB File Offset: 0x000E49EB
		// (set) Token: 0x06004418 RID: 17432 RVA: 0x000E67F3 File Offset: 0x000E49F3
		public IntPtr ParentWindowHandle
		{
			get
			{
				return this.m_parentWindowHandle;
			}
			set
			{
				this.m_parentWindowHandle = value;
				this.m_keyPassword = null;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CspParameters" /> class.</summary>
		// Token: 0x06004419 RID: 17433 RVA: 0x000E6803 File Offset: 0x000E4A03
		public CspParameters() : this(1, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CspParameters" /> class with the specified provider type code.</summary>
		/// <param name="dwTypeIn">A provider type code that specifies the kind of provider to create. </param>
		// Token: 0x0600441A RID: 17434 RVA: 0x000E680E File Offset: 0x000E4A0E
		public CspParameters(int dwTypeIn) : this(dwTypeIn, null, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CspParameters" /> class with the specified provider type code and name.</summary>
		/// <param name="dwTypeIn">A provider type code that specifies the kind of provider to create.</param>
		/// <param name="strProviderNameIn">A provider name. </param>
		// Token: 0x0600441B RID: 17435 RVA: 0x000E6819 File Offset: 0x000E4A19
		public CspParameters(int dwTypeIn, string strProviderNameIn) : this(dwTypeIn, strProviderNameIn, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CspParameters" /> class with the specified provider type code and name, and the specified container name.</summary>
		/// <param name="dwTypeIn">The provider type code that specifies the kind of provider to create.</param>
		/// <param name="strProviderNameIn">A provider name. </param>
		/// <param name="strContainerNameIn">A container name. </param>
		// Token: 0x0600441C RID: 17436 RVA: 0x000E6824 File Offset: 0x000E4A24
		public CspParameters(int dwTypeIn, string strProviderNameIn, string strContainerNameIn) : this(dwTypeIn, strProviderNameIn, strContainerNameIn, CspProviderFlags.NoFlags)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CspParameters" /> class using a provider type, a provider name, a container name, access information, and a password associated with a smart card key.</summary>
		/// <param name="providerType">The provider type code that specifies the kind of provider to create.</param>
		/// <param name="providerName">A provider name. </param>
		/// <param name="keyContainerName">A container name. </param>
		/// <param name="cryptoKeySecurity">An object that represents access rights and audit rules for a container. </param>
		/// <param name="keyPassword">A password associated with a smart card key.</param>
		// Token: 0x0600441D RID: 17437 RVA: 0x000E6830 File Offset: 0x000E4A30
		public CspParameters(int providerType, string providerName, string keyContainerName, CryptoKeySecurity cryptoKeySecurity, SecureString keyPassword) : this(providerType, providerName, keyContainerName)
		{
			this.m_cryptoKeySecurity = cryptoKeySecurity;
			this.m_keyPassword = keyPassword;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CspParameters" /> class using a provider type, a provider name, a container name, access information, and a handle to an unmanaged smart card password dialog. </summary>
		/// <param name="providerType">The provider type code that specifies the kind of provider to create.</param>
		/// <param name="providerName">A provider name. </param>
		/// <param name="keyContainerName">A container name. </param>
		/// <param name="cryptoKeySecurity">An object that represents access rights and audit rules for the container.</param>
		/// <param name="parentWindowHandle">A handle to the parent window for a smart card password dialog.</param>
		// Token: 0x0600441E RID: 17438 RVA: 0x000E684B File Offset: 0x000E4A4B
		public CspParameters(int providerType, string providerName, string keyContainerName, CryptoKeySecurity cryptoKeySecurity, IntPtr parentWindowHandle) : this(providerType, providerName, keyContainerName)
		{
			this.m_cryptoKeySecurity = cryptoKeySecurity;
			this.m_parentWindowHandle = parentWindowHandle;
		}

		// Token: 0x0600441F RID: 17439 RVA: 0x000E6866 File Offset: 0x000E4A66
		internal CspParameters(int providerType, string providerName, string keyContainerName, CspProviderFlags flags)
		{
			this.ProviderType = providerType;
			this.ProviderName = providerName;
			this.KeyContainerName = keyContainerName;
			this.KeyNumber = -1;
			this.Flags = flags;
		}

		// Token: 0x06004420 RID: 17440 RVA: 0x000E6894 File Offset: 0x000E4A94
		internal CspParameters(CspParameters parameters)
		{
			this.ProviderType = parameters.ProviderType;
			this.ProviderName = parameters.ProviderName;
			this.KeyContainerName = parameters.KeyContainerName;
			this.KeyNumber = parameters.KeyNumber;
			this.Flags = parameters.Flags;
			this.m_cryptoKeySecurity = parameters.m_cryptoKeySecurity;
			this.m_keyPassword = parameters.m_keyPassword;
			this.m_parentWindowHandle = parameters.m_parentWindowHandle;
		}

		/// <summary>Represents the provider type code for <see cref="T:System.Security.Cryptography.CspParameters" />.</summary>
		// Token: 0x0400231E RID: 8990
		public int ProviderType;

		/// <summary>Represents the provider name for <see cref="T:System.Security.Cryptography.CspParameters" />.</summary>
		// Token: 0x0400231F RID: 8991
		public string ProviderName;

		/// <summary>Represents the key container name for <see cref="T:System.Security.Cryptography.CspParameters" />.</summary>
		// Token: 0x04002320 RID: 8992
		public string KeyContainerName;

		/// <summary>Specifies whether an asymmetric key is created as a signature key or an exchange key.</summary>
		// Token: 0x04002321 RID: 8993
		public int KeyNumber;

		// Token: 0x04002322 RID: 8994
		private int m_flags;

		// Token: 0x04002323 RID: 8995
		private CryptoKeySecurity m_cryptoKeySecurity;

		// Token: 0x04002324 RID: 8996
		private SecureString m_keyPassword;

		// Token: 0x04002325 RID: 8997
		private IntPtr m_parentWindowHandle;
	}
}
