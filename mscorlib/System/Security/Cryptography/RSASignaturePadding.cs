﻿using System;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the padding mode and parameters to use with RSA signature creation or verification operations. </summary>
	// Token: 0x02000658 RID: 1624
	public sealed class RSASignaturePadding : IEquatable<RSASignaturePadding>
	{
		// Token: 0x060043B5 RID: 17333 RVA: 0x000E5DCD File Offset: 0x000E3FCD
		private RSASignaturePadding(RSASignaturePaddingMode mode)
		{
			this._mode = mode;
		}

		/// <summary>Gets an object that uses the PKCS #1 v1.5 padding mode. </summary>
		/// <returns>An object that uses the <see cref="F:System.Security.Cryptography.RSASignaturePaddingMode.Pkcs1" /> padding mode. </returns>
		// Token: 0x17000B64 RID: 2916
		// (get) Token: 0x060043B6 RID: 17334 RVA: 0x000E5DDC File Offset: 0x000E3FDC
		public static RSASignaturePadding Pkcs1
		{
			get
			{
				return RSASignaturePadding.s_pkcs1;
			}
		}

		/// <summary>Gets an object that uses PSS padding mode. </summary>
		/// <returns>An object that uses the <see cref="F:System.Security.Cryptography.RSASignaturePaddingMode.Pss" /> padding mode with the number of salt bytes equal to the size of the hash. </returns>
		// Token: 0x17000B65 RID: 2917
		// (get) Token: 0x060043B7 RID: 17335 RVA: 0x000E5DE3 File Offset: 0x000E3FE3
		public static RSASignaturePadding Pss
		{
			get
			{
				return RSASignaturePadding.s_pss;
			}
		}

		/// <summary>Gets the padding mode of this <see cref="T:System.Security.Cryptography.RSASignaturePadding" /> instance.</summary>
		/// <returns>The padding mode (either <see cref="F:System.Security.Cryptography.RSASignaturePaddingMode.Pkcs1" /> or <see cref="F:System.Security.Cryptography.RSASignaturePaddingMode.Pss" />) of this instance. </returns>
		// Token: 0x17000B66 RID: 2918
		// (get) Token: 0x060043B8 RID: 17336 RVA: 0x000E5DEA File Offset: 0x000E3FEA
		public RSASignaturePaddingMode Mode
		{
			get
			{
				return this._mode;
			}
		}

		/// <summary>Returns the hash code for this <see cref="T:System.Security.Cryptography.RSASignaturePadding" />  instance.</summary>
		/// <returns>The hash code for this <see cref="T:System.Security.Cryptography.RSASignaturePadding" /> instance. </returns>
		// Token: 0x060043B9 RID: 17337 RVA: 0x000E5DF4 File Offset: 0x000E3FF4
		public override int GetHashCode()
		{
			return this._mode.GetHashCode();
		}

		/// <summary>Returns a value that indicates whether this instance is equal to a specified object. </summary>
		/// <param name="obj">The object to compare with the current instance. </param>
		/// <returns>
		///     <see langword="true" /> if the specified object is equal to the current object; otherwise, <see langword="false" />. </returns>
		// Token: 0x060043BA RID: 17338 RVA: 0x000E5E15 File Offset: 0x000E4015
		public override bool Equals(object obj)
		{
			return this.Equals(obj as RSASignaturePadding);
		}

		/// <summary>Returns a value that indicates whether this instance is equal to a specified <see cref="T:System.Security.Cryptography.RSASignaturePadding" /> object.</summary>
		/// <param name="other">The object to compare with the current instance. </param>
		/// <returns>
		///     <see langword="true" /> if the specified object is equal to the current object; otherwise, <see langword="false" />. </returns>
		// Token: 0x060043BB RID: 17339 RVA: 0x000E5E23 File Offset: 0x000E4023
		public bool Equals(RSASignaturePadding other)
		{
			return other != null && this._mode == other._mode;
		}

		/// <summary>Indicates whether two specified <see cref="T:System.Security.Cryptography.RSASignaturePadding" /> objects are equal. </summary>
		/// <param name="left">The first object to compare. </param>
		/// <param name="right">The second object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if <see langword="left" /> and <see langword="right" /> are equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060043BC RID: 17340 RVA: 0x000E5E3E File Offset: 0x000E403E
		public static bool operator ==(RSASignaturePadding left, RSASignaturePadding right)
		{
			if (left == null)
			{
				return right == null;
			}
			return left.Equals(right);
		}

		/// <summary>Indicates whether two specified <see cref="T:System.Security.Cryptography.RSASignaturePadding" /> objects are unequal. </summary>
		/// <param name="left">The first object to compare. </param>
		/// <param name="right">The second object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if <see langword="left" /> and <see langword="right" /> are unequal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060043BD RID: 17341 RVA: 0x000E5E4F File Offset: 0x000E404F
		public static bool operator !=(RSASignaturePadding left, RSASignaturePadding right)
		{
			return !(left == right);
		}

		/// <summary>Returns the string representation of the current <see cref="T:System.Security.Cryptography.RSASignaturePadding" /> instance. </summary>
		/// <returns>The string representation of the current object. </returns>
		// Token: 0x060043BE RID: 17342 RVA: 0x000E5E5C File Offset: 0x000E405C
		public override string ToString()
		{
			return this._mode.ToString();
		}

		// Token: 0x060043BF RID: 17343 RVA: 0x000E5E7D File Offset: 0x000E407D
		// Note: this type is marked as 'beforefieldinit'.
		static RSASignaturePadding()
		{
		}

		// Token: 0x060043C0 RID: 17344 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal RSASignaturePadding()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040022F2 RID: 8946
		private static readonly RSASignaturePadding s_pkcs1 = new RSASignaturePadding(RSASignaturePaddingMode.Pkcs1);

		// Token: 0x040022F3 RID: 8947
		private static readonly RSASignaturePadding s_pss = new RSASignaturePadding(RSASignaturePaddingMode.Pss);

		// Token: 0x040022F4 RID: 8948
		private readonly RSASignaturePaddingMode _mode;
	}
}
