﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x020006A9 RID: 1705
	[Serializable]
	internal enum CspAlgorithmType
	{
		// Token: 0x040023F2 RID: 9202
		Rsa,
		// Token: 0x040023F3 RID: 9203
		Dss
	}
}
