﻿using System;
using System.IO;

namespace System.Security.Cryptography
{
	// Token: 0x02000681 RID: 1665
	internal sealed class TailStream : Stream
	{
		// Token: 0x060044BC RID: 17596 RVA: 0x000E9933 File Offset: 0x000E7B33
		public TailStream(int bufferSize)
		{
			this._Buffer = new byte[bufferSize];
			this._BufferSize = bufferSize;
		}

		// Token: 0x060044BD RID: 17597 RVA: 0x0008B7ED File Offset: 0x000899ED
		public void Clear()
		{
			this.Close();
		}

		// Token: 0x060044BE RID: 17598 RVA: 0x000E9950 File Offset: 0x000E7B50
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					if (this._Buffer != null)
					{
						Array.Clear(this._Buffer, 0, this._Buffer.Length);
					}
					this._Buffer = null;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x17000B97 RID: 2967
		// (get) Token: 0x060044BF RID: 17599 RVA: 0x000E99A0 File Offset: 0x000E7BA0
		public byte[] Buffer
		{
			get
			{
				return (byte[])this._Buffer.Clone();
			}
		}

		// Token: 0x17000B98 RID: 2968
		// (get) Token: 0x060044C0 RID: 17600 RVA: 0x00002526 File Offset: 0x00000726
		public override bool CanRead
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B99 RID: 2969
		// (get) Token: 0x060044C1 RID: 17601 RVA: 0x00002526 File Offset: 0x00000726
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B9A RID: 2970
		// (get) Token: 0x060044C2 RID: 17602 RVA: 0x000E99B2 File Offset: 0x000E7BB2
		public override bool CanWrite
		{
			get
			{
				return this._Buffer != null;
			}
		}

		// Token: 0x17000B9B RID: 2971
		// (get) Token: 0x060044C3 RID: 17603 RVA: 0x00084E84 File Offset: 0x00083084
		public override long Length
		{
			get
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
			}
		}

		// Token: 0x17000B9C RID: 2972
		// (get) Token: 0x060044C4 RID: 17604 RVA: 0x00084E84 File Offset: 0x00083084
		// (set) Token: 0x060044C5 RID: 17605 RVA: 0x00084E84 File Offset: 0x00083084
		public override long Position
		{
			get
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
			}
			set
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
			}
		}

		// Token: 0x060044C6 RID: 17606 RVA: 0x000020D3 File Offset: 0x000002D3
		public override void Flush()
		{
		}

		// Token: 0x060044C7 RID: 17607 RVA: 0x00084E84 File Offset: 0x00083084
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
		}

		// Token: 0x060044C8 RID: 17608 RVA: 0x00084E84 File Offset: 0x00083084
		public override void SetLength(long value)
		{
			throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
		}

		// Token: 0x060044C9 RID: 17609 RVA: 0x00084E73 File Offset: 0x00083073
		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException(Environment.GetResourceString("Stream does not support reading."));
		}

		// Token: 0x060044CA RID: 17610 RVA: 0x000E99C0 File Offset: 0x000E7BC0
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this._Buffer == null)
			{
				throw new ObjectDisposedException("TailStream");
			}
			if (count == 0)
			{
				return;
			}
			if (this._BufferFull)
			{
				if (count > this._BufferSize)
				{
					System.Buffer.InternalBlockCopy(buffer, offset + count - this._BufferSize, this._Buffer, 0, this._BufferSize);
					return;
				}
				System.Buffer.InternalBlockCopy(this._Buffer, this._BufferSize - count, this._Buffer, 0, this._BufferSize - count);
				System.Buffer.InternalBlockCopy(buffer, offset, this._Buffer, this._BufferSize - count, count);
				return;
			}
			else
			{
				if (count > this._BufferSize)
				{
					System.Buffer.InternalBlockCopy(buffer, offset + count - this._BufferSize, this._Buffer, 0, this._BufferSize);
					this._BufferFull = true;
					return;
				}
				if (count + this._BufferIndex >= this._BufferSize)
				{
					System.Buffer.InternalBlockCopy(this._Buffer, this._BufferIndex + count - this._BufferSize, this._Buffer, 0, this._BufferSize - count);
					System.Buffer.InternalBlockCopy(buffer, offset, this._Buffer, this._BufferIndex, count);
					this._BufferFull = true;
					return;
				}
				System.Buffer.InternalBlockCopy(buffer, offset, this._Buffer, this._BufferIndex, count);
				this._BufferIndex += count;
				return;
			}
		}

		// Token: 0x04002373 RID: 9075
		private byte[] _Buffer;

		// Token: 0x04002374 RID: 9076
		private int _BufferSize;

		// Token: 0x04002375 RID: 9077
		private int _BufferIndex;

		// Token: 0x04002376 RID: 9078
		private bool _BufferFull;
	}
}
