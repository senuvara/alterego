﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020006BD RID: 1725
	internal static class X509Constants
	{
		// Token: 0x04002488 RID: 9352
		internal const uint CRYPT_EXPORTABLE = 1U;

		// Token: 0x04002489 RID: 9353
		internal const uint CRYPT_USER_PROTECTED = 2U;

		// Token: 0x0400248A RID: 9354
		internal const uint CRYPT_MACHINE_KEYSET = 32U;

		// Token: 0x0400248B RID: 9355
		internal const uint CRYPT_USER_KEYSET = 4096U;

		// Token: 0x0400248C RID: 9356
		internal const uint CERT_QUERY_CONTENT_CERT = 1U;

		// Token: 0x0400248D RID: 9357
		internal const uint CERT_QUERY_CONTENT_CTL = 2U;

		// Token: 0x0400248E RID: 9358
		internal const uint CERT_QUERY_CONTENT_CRL = 3U;

		// Token: 0x0400248F RID: 9359
		internal const uint CERT_QUERY_CONTENT_SERIALIZED_STORE = 4U;

		// Token: 0x04002490 RID: 9360
		internal const uint CERT_QUERY_CONTENT_SERIALIZED_CERT = 5U;

		// Token: 0x04002491 RID: 9361
		internal const uint CERT_QUERY_CONTENT_SERIALIZED_CTL = 6U;

		// Token: 0x04002492 RID: 9362
		internal const uint CERT_QUERY_CONTENT_SERIALIZED_CRL = 7U;

		// Token: 0x04002493 RID: 9363
		internal const uint CERT_QUERY_CONTENT_PKCS7_SIGNED = 8U;

		// Token: 0x04002494 RID: 9364
		internal const uint CERT_QUERY_CONTENT_PKCS7_UNSIGNED = 9U;

		// Token: 0x04002495 RID: 9365
		internal const uint CERT_QUERY_CONTENT_PKCS7_SIGNED_EMBED = 10U;

		// Token: 0x04002496 RID: 9366
		internal const uint CERT_QUERY_CONTENT_PKCS10 = 11U;

		// Token: 0x04002497 RID: 9367
		internal const uint CERT_QUERY_CONTENT_PFX = 12U;

		// Token: 0x04002498 RID: 9368
		internal const uint CERT_QUERY_CONTENT_CERT_PAIR = 13U;

		// Token: 0x04002499 RID: 9369
		internal const uint CERT_STORE_PROV_MEMORY = 2U;

		// Token: 0x0400249A RID: 9370
		internal const uint CERT_STORE_PROV_SYSTEM = 10U;

		// Token: 0x0400249B RID: 9371
		internal const uint CERT_STORE_NO_CRYPT_RELEASE_FLAG = 1U;

		// Token: 0x0400249C RID: 9372
		internal const uint CERT_STORE_SET_LOCALIZED_NAME_FLAG = 2U;

		// Token: 0x0400249D RID: 9373
		internal const uint CERT_STORE_DEFER_CLOSE_UNTIL_LAST_FREE_FLAG = 4U;

		// Token: 0x0400249E RID: 9374
		internal const uint CERT_STORE_DELETE_FLAG = 16U;

		// Token: 0x0400249F RID: 9375
		internal const uint CERT_STORE_SHARE_STORE_FLAG = 64U;

		// Token: 0x040024A0 RID: 9376
		internal const uint CERT_STORE_SHARE_CONTEXT_FLAG = 128U;

		// Token: 0x040024A1 RID: 9377
		internal const uint CERT_STORE_MANIFOLD_FLAG = 256U;

		// Token: 0x040024A2 RID: 9378
		internal const uint CERT_STORE_ENUM_ARCHIVED_FLAG = 512U;

		// Token: 0x040024A3 RID: 9379
		internal const uint CERT_STORE_UPDATE_KEYID_FLAG = 1024U;

		// Token: 0x040024A4 RID: 9380
		internal const uint CERT_STORE_BACKUP_RESTORE_FLAG = 2048U;

		// Token: 0x040024A5 RID: 9381
		internal const uint CERT_STORE_READONLY_FLAG = 32768U;

		// Token: 0x040024A6 RID: 9382
		internal const uint CERT_STORE_OPEN_EXISTING_FLAG = 16384U;

		// Token: 0x040024A7 RID: 9383
		internal const uint CERT_STORE_CREATE_NEW_FLAG = 8192U;

		// Token: 0x040024A8 RID: 9384
		internal const uint CERT_STORE_MAXIMUM_ALLOWED_FLAG = 4096U;

		// Token: 0x040024A9 RID: 9385
		internal const uint CERT_NAME_EMAIL_TYPE = 1U;

		// Token: 0x040024AA RID: 9386
		internal const uint CERT_NAME_RDN_TYPE = 2U;

		// Token: 0x040024AB RID: 9387
		internal const uint CERT_NAME_SIMPLE_DISPLAY_TYPE = 4U;

		// Token: 0x040024AC RID: 9388
		internal const uint CERT_NAME_FRIENDLY_DISPLAY_TYPE = 5U;

		// Token: 0x040024AD RID: 9389
		internal const uint CERT_NAME_DNS_TYPE = 6U;

		// Token: 0x040024AE RID: 9390
		internal const uint CERT_NAME_URL_TYPE = 7U;

		// Token: 0x040024AF RID: 9391
		internal const uint CERT_NAME_UPN_TYPE = 8U;
	}
}
