﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using Mono.Security.Authenticode;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Provides methods that help you use X.509 v.3 certificates.</summary>
	// Token: 0x020006C1 RID: 1729
	[ComVisible(true)]
	[MonoTODO("X509ContentType.SerializedCert isn't supported (anywhere in the class)")]
	[Serializable]
	public class X509Certificate : IDeserializationCallback, ISerializable, IDisposable
	{
		/// <summary>Creates an X.509v3 certificate from the specified PKCS7 signed file.</summary>
		/// <param name="filename">The path of the PKCS7 signed file from which to create the X.509 certificate. </param>
		/// <returns>The newly created X.509 certificate.</returns>
		/// <exception cref="T:System.ArgumentException">The <paramref name="filename" /> parameter is <see langword="null" />. </exception>
		// Token: 0x060046DA RID: 18138 RVA: 0x000F7C82 File Offset: 0x000F5E82
		public static X509Certificate CreateFromCertFile(string filename)
		{
			return new X509Certificate(File.ReadAllBytes(filename));
		}

		/// <summary>Creates an X.509v3 certificate from the specified signed file.</summary>
		/// <param name="filename">The path of the signed file from which to create the X.509 certificate. </param>
		/// <returns>The newly created X.509 certificate.</returns>
		// Token: 0x060046DB RID: 18139 RVA: 0x000F7C90 File Offset: 0x000F5E90
		[MonoTODO("Incomplete - minimal validation in this version")]
		public static X509Certificate CreateFromSignedFile(string filename)
		{
			try
			{
				AuthenticodeDeformatter authenticodeDeformatter = new AuthenticodeDeformatter(filename);
				if (authenticodeDeformatter.SigningCertificate != null)
				{
					return new X509Certificate(authenticodeDeformatter.SigningCertificate.RawData);
				}
			}
			catch (SecurityException)
			{
				throw;
			}
			catch (Exception inner)
			{
				throw new COMException(Locale.GetText("Couldn't extract digital signature from {0}.", new object[]
				{
					filename
				}), inner);
			}
			throw new CryptographicException(Locale.GetText("{0} isn't signed.", new object[]
			{
				filename
			}));
		}

		// Token: 0x060046DC RID: 18140 RVA: 0x000F7D18 File Offset: 0x000F5F18
		internal X509Certificate(byte[] data, bool dates)
		{
			if (data != null)
			{
				this.Import(data, null, X509KeyStorageFlags.DefaultKeySet);
				this.hideDates = !dates;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class defined from a sequence of bytes representing an X.509v3 certificate.</summary>
		/// <param name="data">A byte array containing data from an X.509 certificate.</param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="rawData" /> parameter is <see langword="null" />.-or-The length of the <paramref name="rawData" /> parameter is 0.</exception>
		// Token: 0x060046DD RID: 18141 RVA: 0x000F7D36 File Offset: 0x000F5F36
		public X509Certificate(byte[] data) : this(data, true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using a handle to an unmanaged <see langword="PCCERT_CONTEXT" /> structure.</summary>
		/// <param name="handle">A handle to an unmanaged <see langword="PCCERT_CONTEXT" /> structure.</param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The handle parameter does not represent a valid <see langword="PCCERT_CONTEXT" /> structure.</exception>
		// Token: 0x060046DE RID: 18142 RVA: 0x000F7D40 File Offset: 0x000F5F40
		public X509Certificate(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				throw new ArgumentException("Invalid handle.");
			}
			this.impl = X509Helper.InitFromHandle(handle);
		}

		// Token: 0x060046DF RID: 18143 RVA: 0x000F7D6C File Offset: 0x000F5F6C
		internal X509Certificate(X509CertificateImpl impl)
		{
			if (impl == null)
			{
				throw new ArgumentNullException("impl");
			}
			this.impl = X509Helper.InitFromCertificate(impl);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using another <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class.</summary>
		/// <param name="cert">A <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class from which to initialize this class. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentNullException">The value of the <paramref name="cert" /> parameter is <see langword="null" />.</exception>
		// Token: 0x060046E0 RID: 18144 RVA: 0x000F7D8E File Offset: 0x000F5F8E
		public X509Certificate(X509Certificate cert)
		{
			if (cert == null)
			{
				throw new ArgumentNullException("cert");
			}
			this.impl = X509Helper.InitFromCertificate(cert);
			this.hideDates = false;
		}

		// Token: 0x060046E1 RID: 18145 RVA: 0x000F7DB7 File Offset: 0x000F5FB7
		internal void ImportHandle(X509CertificateImpl impl)
		{
			this.Reset();
			this.impl = impl;
		}

		// Token: 0x17000BEB RID: 3051
		// (get) Token: 0x060046E2 RID: 18146 RVA: 0x000F7DC6 File Offset: 0x000F5FC6
		internal X509CertificateImpl Impl
		{
			get
			{
				X509Helper.ThrowIfContextInvalid(this.impl);
				return this.impl;
			}
		}

		// Token: 0x17000BEC RID: 3052
		// (get) Token: 0x060046E3 RID: 18147 RVA: 0x000F7DD9 File Offset: 0x000F5FD9
		internal bool IsValid
		{
			get
			{
				return X509Helper.IsValid(this.impl);
			}
		}

		// Token: 0x060046E4 RID: 18148 RVA: 0x000F7DE6 File Offset: 0x000F5FE6
		internal void ThrowIfContextInvalid()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
		}

		/// <summary>Compares two <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> objects for equality.</summary>
		/// <param name="other">An <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object to compare to the current object.</param>
		/// <returns>
		///     <see langword="true" /> if the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object is equal to the object specified by the <paramref name="other" /> parameter; otherwise, <see langword="false" />.</returns>
		// Token: 0x060046E5 RID: 18149 RVA: 0x000F7DF4 File Offset: 0x000F5FF4
		public virtual bool Equals(X509Certificate other)
		{
			if (other == null)
			{
				return false;
			}
			if (X509Helper.IsValid(other.impl))
			{
				return object.Equals(this.impl, other.impl);
			}
			if (!X509Helper.IsValid(this.impl))
			{
				return true;
			}
			throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
		}

		/// <summary>Returns the hash value for the X.509v3 certificate as an array of bytes.</summary>
		/// <returns>The hash value for the X.509 certificate.</returns>
		// Token: 0x060046E6 RID: 18150 RVA: 0x000F7E43 File Offset: 0x000F6043
		public virtual byte[] GetCertHash()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetCertHash();
		}

		/// <summary>Returns the SHA1 hash value for the X.509v3 certificate as a hexadecimal string.</summary>
		/// <returns>The hexadecimal string representation of the X.509 certificate hash value.</returns>
		// Token: 0x060046E7 RID: 18151 RVA: 0x000F7E5B File Offset: 0x000F605B
		public virtual string GetCertHashString()
		{
			return X509Helper.ToHexString(this.GetCertHash());
		}

		/// <summary>Returns the effective date of this X.509v3 certificate.</summary>
		/// <returns>The effective date for this X.509 certificate.</returns>
		// Token: 0x060046E8 RID: 18152 RVA: 0x000F7E68 File Offset: 0x000F6068
		public virtual string GetEffectiveDateString()
		{
			if (this.hideDates)
			{
				return null;
			}
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetValidFrom().ToLocalTime().ToString();
		}

		/// <summary>Returns the expiration date of this X.509v3 certificate.</summary>
		/// <returns>The expiration date for this X.509 certificate.</returns>
		// Token: 0x060046E9 RID: 18153 RVA: 0x000F7EA8 File Offset: 0x000F60A8
		public virtual string GetExpirationDateString()
		{
			if (this.hideDates)
			{
				return null;
			}
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetValidUntil().ToLocalTime().ToString();
		}

		/// <summary>Returns the name of the format of this X.509v3 certificate.</summary>
		/// <returns>The format of this X.509 certificate.</returns>
		// Token: 0x060046EA RID: 18154 RVA: 0x000F7EE5 File Offset: 0x000F60E5
		public virtual string GetFormat()
		{
			return "X509";
		}

		/// <summary>Returns the hash code for the X.509v3 certificate as an integer.</summary>
		/// <returns>The hash code for the X.509 certificate as an integer.</returns>
		// Token: 0x060046EB RID: 18155 RVA: 0x000F7EEC File Offset: 0x000F60EC
		public override int GetHashCode()
		{
			if (!X509Helper.IsValid(this.impl))
			{
				return 0;
			}
			return this.impl.GetHashCode();
		}

		/// <summary>Returns the name of the certification authority that issued the X.509v3 certificate.</summary>
		/// <returns>The name of the certification authority that issued the X.509 certificate.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		// Token: 0x060046EC RID: 18156 RVA: 0x000F7F08 File Offset: 0x000F6108
		[Obsolete("Use the Issuer property.")]
		public virtual string GetIssuerName()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetIssuerName(true);
		}

		/// <summary>Returns the key algorithm information for this X.509v3 certificate as a string.</summary>
		/// <returns>The key algorithm information for this X.509 certificate as a string.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The certificate context is invalid.</exception>
		// Token: 0x060046ED RID: 18157 RVA: 0x000F7F21 File Offset: 0x000F6121
		public virtual string GetKeyAlgorithm()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetKeyAlgorithm();
		}

		/// <summary>Returns the key algorithm parameters for the X.509v3 certificate as an array of bytes.</summary>
		/// <returns>The key algorithm parameters for the X.509 certificate as an array of bytes.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The certificate context is invalid.</exception>
		// Token: 0x060046EE RID: 18158 RVA: 0x000F7F39 File Offset: 0x000F6139
		public virtual byte[] GetKeyAlgorithmParameters()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			byte[] keyAlgorithmParameters = this.impl.GetKeyAlgorithmParameters();
			if (keyAlgorithmParameters == null)
			{
				throw new CryptographicException(Locale.GetText("Parameters not part of the certificate"));
			}
			return keyAlgorithmParameters;
		}

		/// <summary>Returns the key algorithm parameters for the X.509v3 certificate as a hexadecimal string.</summary>
		/// <returns>The key algorithm parameters for the X.509 certificate as a hexadecimal string.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The certificate context is invalid.</exception>
		// Token: 0x060046EF RID: 18159 RVA: 0x000F7F64 File Offset: 0x000F6164
		public virtual string GetKeyAlgorithmParametersString()
		{
			return X509Helper.ToHexString(this.GetKeyAlgorithmParameters());
		}

		/// <summary>Returns the name of the principal to which the certificate was issued.</summary>
		/// <returns>The name of the principal to which the certificate was issued.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The certificate context is invalid.</exception>
		// Token: 0x060046F0 RID: 18160 RVA: 0x000F7F71 File Offset: 0x000F6171
		[Obsolete("Use the Subject property.")]
		public virtual string GetName()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetSubjectName(true);
		}

		/// <summary>Returns the public key for the X.509v3 certificate as an array of bytes.</summary>
		/// <returns>The public key for the X.509 certificate as an array of bytes.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The certificate context is invalid.</exception>
		// Token: 0x060046F1 RID: 18161 RVA: 0x000F7F8A File Offset: 0x000F618A
		public virtual byte[] GetPublicKey()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetPublicKey();
		}

		/// <summary>Returns the public key for the X.509v3 certificate as a hexadecimal string.</summary>
		/// <returns>The public key for the X.509 certificate as a hexadecimal string.</returns>
		// Token: 0x060046F2 RID: 18162 RVA: 0x000F7FA2 File Offset: 0x000F61A2
		public virtual string GetPublicKeyString()
		{
			return X509Helper.ToHexString(this.GetPublicKey());
		}

		/// <summary>Returns the raw data for the entire X.509v3 certificate as an array of bytes.</summary>
		/// <returns>A byte array containing the X.509 certificate data.</returns>
		// Token: 0x060046F3 RID: 18163 RVA: 0x000F7FAF File Offset: 0x000F61AF
		public virtual byte[] GetRawCertData()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetRawCertData();
		}

		/// <summary>Returns the raw data for the entire X.509v3 certificate as a hexadecimal string.</summary>
		/// <returns>The X.509 certificate data as a hexadecimal string.</returns>
		// Token: 0x060046F4 RID: 18164 RVA: 0x000F7FC7 File Offset: 0x000F61C7
		public virtual string GetRawCertDataString()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			return X509Helper.ToHexString(this.impl.GetRawCertData());
		}

		/// <summary>Returns the serial number of the X.509v3 certificate as an array of bytes.</summary>
		/// <returns>The serial number of the X.509 certificate as an array of bytes.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The certificate context is invalid.</exception>
		// Token: 0x060046F5 RID: 18165 RVA: 0x000F7FE4 File Offset: 0x000F61E4
		public virtual byte[] GetSerialNumber()
		{
			X509Helper.ThrowIfContextInvalid(this.impl);
			return this.impl.GetSerialNumber();
		}

		/// <summary>Returns the serial number of the X.509v3 certificate as a hexadecimal string.</summary>
		/// <returns>The serial number of the X.509 certificate as a hexadecimal string.</returns>
		// Token: 0x060046F6 RID: 18166 RVA: 0x000F7FFC File Offset: 0x000F61FC
		public virtual string GetSerialNumberString()
		{
			byte[] serialNumber = this.GetSerialNumber();
			Array.Reverse<byte>(serialNumber);
			return X509Helper.ToHexString(serialNumber);
		}

		/// <summary>Returns a string representation of the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object.</summary>
		/// <returns>A string representation of the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object.</returns>
		// Token: 0x060046F7 RID: 18167 RVA: 0x000F800F File Offset: 0x000F620F
		public override string ToString()
		{
			return base.ToString();
		}

		/// <summary>Returns a string representation of the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object, with extra information, if specified.</summary>
		/// <param name="fVerbose">
		///       <see langword="true" /> to produce the verbose form of the string representation; otherwise, <see langword="false" />. </param>
		/// <returns>A string representation of the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object.</returns>
		// Token: 0x060046F8 RID: 18168 RVA: 0x000F8017 File Offset: 0x000F6217
		public virtual string ToString(bool fVerbose)
		{
			if (!fVerbose || !X509Helper.IsValid(this.impl))
			{
				return base.ToString();
			}
			return this.impl.ToString(true);
		}

		/// <summary>Converts the specified date and time to a string.</summary>
		/// <param name="date">The date and time to convert.</param>
		/// <returns>A string representation of the value of the <see cref="T:System.DateTime" /> object.</returns>
		// Token: 0x060046F9 RID: 18169 RVA: 0x000041F3 File Offset: 0x000023F3
		protected static string FormatDate(DateTime date)
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class. </summary>
		// Token: 0x060046FA RID: 18170 RVA: 0x00002050 File Offset: 0x00000250
		public X509Certificate()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using a byte array and a password.</summary>
		/// <param name="rawData">A byte array containing data from an X.509 certificate.</param>
		/// <param name="password">The password required to access the X.509 certificate data.</param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="rawData" /> parameter is <see langword="null" />.-or-The length of the <paramref name="rawData" /> parameter is 0.</exception>
		// Token: 0x060046FB RID: 18171 RVA: 0x000F803C File Offset: 0x000F623C
		public X509Certificate(byte[] rawData, string password)
		{
			this.Import(rawData, password, X509KeyStorageFlags.DefaultKeySet);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using a byte array and a password.</summary>
		/// <param name="rawData">A byte array that contains data from an X.509 certificate.</param>
		/// <param name="password">The password required to access the X.509 certificate data.</param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="rawData" /> parameter is <see langword="null" />.-or-The length of the <paramref name="rawData" /> parameter is 0.</exception>
		// Token: 0x060046FC RID: 18172 RVA: 0x000F804D File Offset: 0x000F624D
		[MonoTODO("SecureString support is incomplete")]
		public X509Certificate(byte[] rawData, SecureString password)
		{
			this.Import(rawData, password, X509KeyStorageFlags.DefaultKeySet);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using a byte array, a password, and a key storage flag.</summary>
		/// <param name="rawData">A byte array containing data from an X.509 certificate. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <param name="keyStorageFlags">A bitwise combination of the enumeration values that control where and how to import the certificate. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="rawData" /> parameter is <see langword="null" />.-or-The length of the <paramref name="rawData" /> parameter is 0.</exception>
		// Token: 0x060046FD RID: 18173 RVA: 0x000F805E File Offset: 0x000F625E
		public X509Certificate(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, password, keyStorageFlags);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using a byte array, a password, and a key storage flag.</summary>
		/// <param name="rawData">A byte array that contains data from an X.509 certificate. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <param name="keyStorageFlags">A bitwise combination of the enumeration values that control where and how to import the certificate. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="rawData" /> parameter is <see langword="null" />.-or-The length of the <paramref name="rawData" /> parameter is 0.</exception>
		// Token: 0x060046FE RID: 18174 RVA: 0x000F806F File Offset: 0x000F626F
		[MonoTODO("SecureString support is incomplete")]
		public X509Certificate(byte[] rawData, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, password, keyStorageFlags);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using the name of a PKCS7 signed file. </summary>
		/// <param name="fileName">The name of a PKCS7 signed file.</param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="fileName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x060046FF RID: 18175 RVA: 0x000F8080 File Offset: 0x000F6280
		public X509Certificate(string fileName)
		{
			this.Import(fileName, null, X509KeyStorageFlags.DefaultKeySet);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using the name of a PKCS7 signed file and a password to access the certificate.</summary>
		/// <param name="fileName">The name of a PKCS7 signed file. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="fileName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06004700 RID: 18176 RVA: 0x000F8091 File Offset: 0x000F6291
		public X509Certificate(string fileName, string password)
		{
			this.Import(fileName, password, X509KeyStorageFlags.DefaultKeySet);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using a certificate file name and a password.</summary>
		/// <param name="fileName">The name of a certificate file. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="fileName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06004701 RID: 18177 RVA: 0x000F80A2 File Offset: 0x000F62A2
		[MonoTODO("SecureString support is incomplete")]
		public X509Certificate(string fileName, SecureString password)
		{
			this.Import(fileName, password, X509KeyStorageFlags.DefaultKeySet);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using the name of a PKCS7 signed file, a password to access the certificate, and a key storage flag. </summary>
		/// <param name="fileName">The name of a PKCS7 signed file. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <param name="keyStorageFlags">A bitwise combination of the enumeration values that control where and how to import the certificate. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="fileName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06004702 RID: 18178 RVA: 0x000F80B3 File Offset: 0x000F62B3
		public X509Certificate(string fileName, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(fileName, password, keyStorageFlags);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using a certificate file name, a password, and a key storage flag. </summary>
		/// <param name="fileName">The name of a certificate file. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <param name="keyStorageFlags">A bitwise combination of the enumeration values that control where and how to import the certificate. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="fileName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06004703 RID: 18179 RVA: 0x000F80C4 File Offset: 0x000F62C4
		[MonoTODO("SecureString support is incomplete")]
		public X509Certificate(string fileName, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(fileName, password, keyStorageFlags);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> class using a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object and a <see cref="T:System.Runtime.Serialization.StreamingContext" /> structure.</summary>
		/// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object that describes serialization information.</param>
		/// <param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext" /> structure that describes how serialization should be performed.</param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An error with the certificate occurs. For example:The certificate file does not exist.The certificate is invalid.The certificate's password is incorrect.</exception>
		// Token: 0x06004704 RID: 18180 RVA: 0x000F80D8 File Offset: 0x000F62D8
		public X509Certificate(SerializationInfo info, StreamingContext context)
		{
			byte[] rawData = (byte[])info.GetValue("RawData", typeof(byte[]));
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		/// <summary>Gets the name of the certificate authority that issued the X.509v3 certificate.</summary>
		/// <returns>The name of the certificate authority that issued the X.509v3 certificate.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The certificate handle is invalid.</exception>
		// Token: 0x17000BED RID: 3053
		// (get) Token: 0x06004705 RID: 18181 RVA: 0x000F810F File Offset: 0x000F630F
		public string Issuer
		{
			get
			{
				X509Helper.ThrowIfContextInvalid(this.impl);
				if (this.issuer_name == null)
				{
					this.issuer_name = this.impl.GetIssuerName(false);
				}
				return this.issuer_name;
			}
		}

		/// <summary>Gets the subject distinguished name from the certificate.</summary>
		/// <returns>The subject distinguished name from the certificate.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The certificate handle is invalid.</exception>
		// Token: 0x17000BEE RID: 3054
		// (get) Token: 0x06004706 RID: 18182 RVA: 0x000F813C File Offset: 0x000F633C
		public string Subject
		{
			get
			{
				X509Helper.ThrowIfContextInvalid(this.impl);
				if (this.subject_name == null)
				{
					this.subject_name = this.impl.GetSubjectName(false);
				}
				return this.subject_name;
			}
		}

		/// <summary>Gets a handle to a Microsoft Cryptographic API certificate context described by an unmanaged <see langword="PCCERT_CONTEXT" /> structure. </summary>
		/// <returns>An <see cref="T:System.IntPtr" /> structure that represents an unmanaged <see langword="PCCERT_CONTEXT" /> structure.</returns>
		// Token: 0x17000BEF RID: 3055
		// (get) Token: 0x06004707 RID: 18183 RVA: 0x000F8169 File Offset: 0x000F6369
		[ComVisible(false)]
		public IntPtr Handle
		{
			get
			{
				if (X509Helper.IsValid(this.impl))
				{
					return this.impl.Handle;
				}
				return IntPtr.Zero;
			}
		}

		/// <summary>Compares two <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> objects for equality.</summary>
		/// <param name="obj">An <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object to compare to the current object. </param>
		/// <returns>
		///     <see langword="true" /> if the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object is equal to the object specified by the <paramref name="other" /> parameter; otherwise, <see langword="false" />.</returns>
		// Token: 0x06004708 RID: 18184 RVA: 0x000F818C File Offset: 0x000F638C
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			X509Certificate x509Certificate = obj as X509Certificate;
			return x509Certificate != null && this.Equals(x509Certificate);
		}

		/// <summary>Exports the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object to a byte array in a format described by one of the <see cref="T:System.Security.Cryptography.X509Certificates.X509ContentType" /> values. </summary>
		/// <param name="contentType">One of the <see cref="T:System.Security.Cryptography.X509Certificates.X509ContentType" /> values that describes how to format the output data. </param>
		/// <returns>An array of bytes that represents the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">A value other than <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Cert" />, <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.SerializedCert" />, or <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Pkcs12" /> was passed to the <paramref name="contentType" /> parameter.-or-The certificate could not be exported.</exception>
		// Token: 0x06004709 RID: 18185 RVA: 0x000F81AC File Offset: 0x000F63AC
		[ComVisible(false)]
		[MonoTODO("X509ContentType.Pfx/Pkcs12 and SerializedCert are not supported")]
		public virtual byte[] Export(X509ContentType contentType)
		{
			return this.Export(contentType, null);
		}

		/// <summary>Exports the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object to a byte array in a format described by one of the <see cref="T:System.Security.Cryptography.X509Certificates.X509ContentType" /> values, and using the specified password.</summary>
		/// <param name="contentType">One of the <see cref="T:System.Security.Cryptography.X509Certificates.X509ContentType" /> values that describes how to format the output data.</param>
		/// <param name="password">The password required to access the X.509 certificate data.</param>
		/// <returns>An array of bytes that represents the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">A value other than <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Cert" />, <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.SerializedCert" />, or <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Pkcs12" /> was passed to the <paramref name="contentType" /> parameter.-or-The certificate could not be exported.</exception>
		// Token: 0x0600470A RID: 18186 RVA: 0x000F81B8 File Offset: 0x000F63B8
		[ComVisible(false)]
		[MonoTODO("X509ContentType.Pfx/Pkcs12 and SerializedCert are not supported")]
		public virtual byte[] Export(X509ContentType contentType, string password)
		{
			byte[] password2 = (password == null) ? null : Encoding.UTF8.GetBytes(password);
			return this.Export(contentType, password2);
		}

		/// <summary>Exports the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object to a byte array using the specified format and a password.</summary>
		/// <param name="contentType">One of the <see cref="T:System.Security.Cryptography.X509Certificates.X509ContentType" /> values that describes how to format the output data.</param>
		/// <param name="password">The password required to access the X.509 certificate data.</param>
		/// <returns>A byte array that represents the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">A value other than <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Cert" />, <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.SerializedCert" />, or <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Pkcs12" /> was passed to the <paramref name="contentType" /> parameter.-or-The certificate could not be exported.</exception>
		// Token: 0x0600470B RID: 18187 RVA: 0x000F81E0 File Offset: 0x000F63E0
		[MonoTODO("X509ContentType.Pfx/Pkcs12 and SerializedCert are not supported. SecureString support is incomplete.")]
		public virtual byte[] Export(X509ContentType contentType, SecureString password)
		{
			byte[] password2 = (password == null) ? null : password.GetBuffer();
			return this.Export(contentType, password2);
		}

		// Token: 0x0600470C RID: 18188 RVA: 0x000F8204 File Offset: 0x000F6404
		internal byte[] Export(X509ContentType contentType, byte[] password)
		{
			byte[] result;
			try
			{
				X509Helper.ThrowIfContextInvalid(this.impl);
				result = this.impl.Export(contentType, password);
			}
			finally
			{
				if (password != null)
				{
					Array.Clear(password, 0, password.Length);
				}
			}
			return result;
		}

		/// <summary>Populates the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object with data from a byte array.</summary>
		/// <param name="rawData">A byte array containing data from an X.509 certificate. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="rawData" /> parameter is <see langword="null" />.-or-The length of the <paramref name="rawData" /> parameter is 0.</exception>
		// Token: 0x0600470D RID: 18189 RVA: 0x000F824C File Offset: 0x000F644C
		[ComVisible(false)]
		public virtual void Import(byte[] rawData)
		{
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		/// <summary>Populates the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object using data from a byte array, a password, and flags for determining how the private key is imported.</summary>
		/// <param name="rawData">A byte array containing data from an X.509 certificate. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <param name="keyStorageFlags">A bitwise combination of the enumeration values that control where and how to import the certificate. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="rawData" /> parameter is <see langword="null" />.-or-The length of the <paramref name="rawData" /> parameter is 0.</exception>
		// Token: 0x0600470E RID: 18190 RVA: 0x000F8257 File Offset: 0x000F6457
		[MonoTODO("missing KeyStorageFlags support")]
		[ComVisible(false)]
		public virtual void Import(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Reset();
			this.impl = X509Helper.Import(rawData, password, keyStorageFlags);
		}

		/// <summary>Populates an <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object using data from a byte array, a password, and a key storage flag.</summary>
		/// <param name="rawData">A byte array that contains data from an X.509 certificate. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <param name="keyStorageFlags">A bitwise combination of the enumeration values that control where and how to import the certificate. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="rawData" /> parameter is <see langword="null" />.-or-The length of the <paramref name="rawData" /> parameter is 0.</exception>
		// Token: 0x0600470F RID: 18191 RVA: 0x000F826D File Offset: 0x000F646D
		[MonoTODO("SecureString support is incomplete")]
		public virtual void Import(byte[] rawData, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, null, keyStorageFlags);
		}

		/// <summary>Populates the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object with information from a certificate file.</summary>
		/// <param name="fileName">The name of a certificate file represented as a string. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="fileName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06004710 RID: 18192 RVA: 0x000F8278 File Offset: 0x000F6478
		[ComVisible(false)]
		public virtual void Import(string fileName)
		{
			byte[] rawData = File.ReadAllBytes(fileName);
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		/// <summary>Populates the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object with information from a certificate file, a password, and a <see cref="T:System.Security.Cryptography.X509Certificates.X509KeyStorageFlags" /> value.</summary>
		/// <param name="fileName">The name of a certificate file represented as a string. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <param name="keyStorageFlags">A bitwise combination of the enumeration values that control where and how to import the certificate. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="fileName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06004711 RID: 18193 RVA: 0x000F8298 File Offset: 0x000F6498
		[ComVisible(false)]
		[MonoTODO("missing KeyStorageFlags support")]
		public virtual void Import(string fileName, string password, X509KeyStorageFlags keyStorageFlags)
		{
			byte[] rawData = File.ReadAllBytes(fileName);
			this.Import(rawData, password, keyStorageFlags);
		}

		/// <summary>Populates an <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object with information from a certificate file, a password, and a key storage flag.</summary>
		/// <param name="fileName">The name of a certificate file. </param>
		/// <param name="password">The password required to access the X.509 certificate data. </param>
		/// <param name="keyStorageFlags">A bitwise combination of the enumeration values that control where and how to import the certificate. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="fileName" /> parameter is <see langword="null" />.</exception>
		// Token: 0x06004712 RID: 18194 RVA: 0x000F82B8 File Offset: 0x000F64B8
		[MonoTODO("SecureString support is incomplete, missing KeyStorageFlags support")]
		public virtual void Import(string fileName, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			byte[] rawData = File.ReadAllBytes(fileName);
			this.Import(rawData, null, keyStorageFlags);
		}

		/// <summary>Implements the <see cref="T:System.Runtime.Serialization.ISerializable" /> interface and is called back by the deserialization event when deserialization is complete.  </summary>
		/// <param name="sender">The source of the deserialization event.</param>
		// Token: 0x06004713 RID: 18195 RVA: 0x000020D3 File Offset: 0x000002D3
		void IDeserializationCallback.OnDeserialization(object sender)
		{
		}

		/// <summary>Gets serialization information with all the data needed to recreate an instance of the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object.</summary>
		/// <param name="info">The object to populate with serialization information.</param>
		/// <param name="context">The destination context of the serialization.</param>
		// Token: 0x06004714 RID: 18196 RVA: 0x000F82D5 File Offset: 0x000F64D5
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (!X509Helper.IsValid(this.impl))
			{
				throw new NullReferenceException();
			}
			info.AddValue("RawData", this.impl.GetRawCertData());
		}

		/// <summary>Releases all resources used by the current <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> object.</summary>
		// Token: 0x06004715 RID: 18197 RVA: 0x000F8300 File Offset: 0x000F6500
		public void Dispose()
		{
			this.Dispose(true);
		}

		/// <summary>Releases all of the unmanaged resources used by this <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources.</param>
		// Token: 0x06004716 RID: 18198 RVA: 0x000F8309 File Offset: 0x000F6509
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Reset();
			}
		}

		/// <summary>Resets the state of the <see cref="T:System.Security.Cryptography.X509Certificates.X509Certificate2" /> object.</summary>
		// Token: 0x06004717 RID: 18199 RVA: 0x000F8314 File Offset: 0x000F6514
		[ComVisible(false)]
		public virtual void Reset()
		{
			if (this.impl != null)
			{
				this.impl.Dispose();
				this.impl = null;
			}
			this.issuer_name = null;
			this.subject_name = null;
			this.hideDates = false;
		}

		// Token: 0x040024C4 RID: 9412
		private X509CertificateImpl impl;

		// Token: 0x040024C5 RID: 9413
		private bool hideDates;

		// Token: 0x040024C6 RID: 9414
		private string issuer_name;

		// Token: 0x040024C7 RID: 9415
		private string subject_name;
	}
}
