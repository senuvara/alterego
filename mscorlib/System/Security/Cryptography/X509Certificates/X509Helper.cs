﻿using System;
using System.Text;
using System.Threading;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020006C4 RID: 1732
	internal static class X509Helper
	{
		// Token: 0x06004744 RID: 18244 RVA: 0x000F86EC File Offset: 0x000F68EC
		internal static void InstallNativeHelper(INativeCertificateHelper helper)
		{
			if (X509Helper.nativeHelper == null)
			{
				Interlocked.CompareExchange<INativeCertificateHelper>(ref X509Helper.nativeHelper, helper, null);
			}
		}

		// Token: 0x06004745 RID: 18245 RVA: 0x000175EA File Offset: 0x000157EA
		public static X509CertificateImpl InitFromHandle(IntPtr handle)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004746 RID: 18246 RVA: 0x000F8702 File Offset: 0x000F6902
		private static X509CertificateImpl Import(byte[] rawData)
		{
			return X509Helper.ImportCore(rawData);
		}

		// Token: 0x06004747 RID: 18247 RVA: 0x000F870A File Offset: 0x000F690A
		public static X509CertificateImpl InitFromCertificate(X509Certificate cert)
		{
			if (X509Helper.nativeHelper != null)
			{
				return X509Helper.nativeHelper.Import(cert);
			}
			return X509Helper.InitFromCertificate(cert.Impl);
		}

		// Token: 0x06004748 RID: 18248 RVA: 0x000F872C File Offset: 0x000F692C
		public static X509CertificateImpl InitFromCertificate(X509CertificateImpl impl)
		{
			X509Helper.ThrowIfContextInvalid(impl);
			X509CertificateImpl x509CertificateImpl = impl.Clone();
			if (x509CertificateImpl != null)
			{
				return x509CertificateImpl;
			}
			byte[] rawCertData = impl.GetRawCertData();
			if (rawCertData == null)
			{
				return null;
			}
			return new X509CertificateImplMono(new X509Certificate(rawCertData));
		}

		// Token: 0x06004749 RID: 18249 RVA: 0x000F8762 File Offset: 0x000F6962
		public static bool IsValid(X509CertificateImpl impl)
		{
			return impl != null && impl.IsValid;
		}

		// Token: 0x0600474A RID: 18250 RVA: 0x000F876F File Offset: 0x000F696F
		internal static void ThrowIfContextInvalid(X509CertificateImpl impl)
		{
			if (!X509Helper.IsValid(impl))
			{
				throw X509Helper.GetInvalidContextException();
			}
		}

		// Token: 0x0600474B RID: 18251 RVA: 0x000F877F File Offset: 0x000F697F
		internal static Exception GetInvalidContextException()
		{
			return new CryptographicException(Locale.GetText("Certificate instance is empty."));
		}

		// Token: 0x0600474C RID: 18252 RVA: 0x000F8790 File Offset: 0x000F6990
		internal static X509Certificate ImportPkcs12(byte[] rawData, string password)
		{
			PKCS12 pkcs = (password == null) ? new PKCS12(rawData) : new PKCS12(rawData, password);
			if (pkcs.Certificates.Count == 0)
			{
				return null;
			}
			if (pkcs.Keys.Count == 0)
			{
				return pkcs.Certificates[0];
			}
			string a = (pkcs.Keys[0] as AsymmetricAlgorithm).ToXmlString(false);
			foreach (X509Certificate x509Certificate in pkcs.Certificates)
			{
				if (x509Certificate.RSA != null && a == x509Certificate.RSA.ToXmlString(false))
				{
					return x509Certificate;
				}
				if (x509Certificate.DSA != null && a == x509Certificate.DSA.ToXmlString(false))
				{
					return x509Certificate;
				}
			}
			return pkcs.Certificates[0];
		}

		// Token: 0x0600474D RID: 18253 RVA: 0x000F8888 File Offset: 0x000F6A88
		private static byte[] PEM(string type, byte[] data)
		{
			string @string = Encoding.ASCII.GetString(data);
			string text = string.Format("-----BEGIN {0}-----", type);
			string value = string.Format("-----END {0}-----", type);
			int num = @string.IndexOf(text) + text.Length;
			int num2 = @string.IndexOf(value, num);
			return Convert.FromBase64String(@string.Substring(num, num2 - num));
		}

		// Token: 0x0600474E RID: 18254 RVA: 0x000F88E0 File Offset: 0x000F6AE0
		private static byte[] ConvertData(byte[] data)
		{
			if (data == null || data.Length == 0)
			{
				return data;
			}
			if (data[0] != 48)
			{
				try
				{
					return X509Helper.PEM("CERTIFICATE", data);
				}
				catch
				{
				}
				return data;
			}
			return data;
		}

		// Token: 0x0600474F RID: 18255 RVA: 0x000F8924 File Offset: 0x000F6B24
		private static X509CertificateImpl ImportCore(byte[] rawData)
		{
			X509Certificate x;
			try
			{
				x = new X509Certificate(rawData);
			}
			catch (Exception inner)
			{
				try
				{
					x = X509Helper.ImportPkcs12(rawData, null);
				}
				catch
				{
					throw new CryptographicException(Locale.GetText("Unable to decode certificate."), inner);
				}
			}
			return new X509CertificateImplMono(x);
		}

		// Token: 0x06004750 RID: 18256 RVA: 0x000F897C File Offset: 0x000F6B7C
		public static X509CertificateImpl Import(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags)
		{
			if (password == null)
			{
				rawData = X509Helper.ConvertData(rawData);
				return X509Helper.Import(rawData);
			}
			X509Certificate x;
			try
			{
				x = X509Helper.ImportPkcs12(rawData, password);
			}
			catch
			{
				x = new X509Certificate(rawData);
			}
			return new X509CertificateImplMono(x);
		}

		// Token: 0x06004751 RID: 18257 RVA: 0x000F89C8 File Offset: 0x000F6BC8
		public static byte[] Export(X509CertificateImpl impl, X509ContentType contentType, byte[] password)
		{
			X509Helper.ThrowIfContextInvalid(impl);
			return impl.Export(contentType, password);
		}

		// Token: 0x06004752 RID: 18258 RVA: 0x000F89D8 File Offset: 0x000F6BD8
		public static bool Equals(X509CertificateImpl first, X509CertificateImpl second)
		{
			if (!X509Helper.IsValid(first) || !X509Helper.IsValid(second))
			{
				return false;
			}
			bool result;
			if (first.Equals(second, out result))
			{
				return result;
			}
			byte[] rawCertData = first.GetRawCertData();
			byte[] rawCertData2 = second.GetRawCertData();
			if (rawCertData == null)
			{
				return rawCertData2 == null;
			}
			if (rawCertData2 == null)
			{
				return false;
			}
			if (rawCertData.Length != rawCertData2.Length)
			{
				return false;
			}
			for (int i = 0; i < rawCertData.Length; i++)
			{
				if (rawCertData[i] != rawCertData2[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06004753 RID: 18259 RVA: 0x000F8A44 File Offset: 0x000F6C44
		public static string ToHexString(byte[] data)
		{
			if (data != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < data.Length; i++)
				{
					stringBuilder.Append(data[i].ToString("X2"));
				}
				return stringBuilder.ToString();
			}
			return null;
		}

		// Token: 0x040024CA RID: 9418
		private static INativeCertificateHelper nativeHelper;
	}
}
