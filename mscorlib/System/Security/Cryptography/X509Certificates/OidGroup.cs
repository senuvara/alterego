﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020006BE RID: 1726
	internal enum OidGroup
	{
		// Token: 0x040024B1 RID: 9393
		AllGroups,
		// Token: 0x040024B2 RID: 9394
		HashAlgorithm,
		// Token: 0x040024B3 RID: 9395
		EncryptionAlgorithm,
		// Token: 0x040024B4 RID: 9396
		PublicKeyAlgorithm,
		// Token: 0x040024B5 RID: 9397
		SignatureAlgorithm,
		// Token: 0x040024B6 RID: 9398
		Attribute,
		// Token: 0x040024B7 RID: 9399
		ExtensionOrAttribute,
		// Token: 0x040024B8 RID: 9400
		EnhancedKeyUsage,
		// Token: 0x040024B9 RID: 9401
		Policy,
		// Token: 0x040024BA RID: 9402
		Template,
		// Token: 0x040024BB RID: 9403
		KeyDerivationFunction,
		// Token: 0x040024BC RID: 9404
		DisableSearchDS = -2147483648
	}
}
