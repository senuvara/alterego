﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Specifies the format of an X.509 certificate. </summary>
	// Token: 0x020006BB RID: 1723
	public enum X509ContentType
	{
		/// <summary>An unknown X.509 certificate.  </summary>
		// Token: 0x04002478 RID: 9336
		Unknown,
		/// <summary>A single X.509 certificate.</summary>
		// Token: 0x04002479 RID: 9337
		Cert,
		/// <summary>A single serialized X.509 certificate. </summary>
		// Token: 0x0400247A RID: 9338
		SerializedCert,
		/// <summary>A PFX-formatted certificate. The <see langword="Pfx" /> value is identical to the <see langword="Pkcs12" /> value.</summary>
		// Token: 0x0400247B RID: 9339
		Pfx,
		/// <summary>A PKCS #12–formatted certificate. The <see langword="Pkcs12" /> value is identical to the <see langword="Pfx" /> value.</summary>
		// Token: 0x0400247C RID: 9340
		Pkcs12 = 3,
		/// <summary>A serialized store.</summary>
		// Token: 0x0400247D RID: 9341
		SerializedStore,
		/// <summary>A PKCS #7–formatted certificate.</summary>
		// Token: 0x0400247E RID: 9342
		Pkcs7,
		/// <summary>An Authenticode X.509 certificate. </summary>
		// Token: 0x0400247F RID: 9343
		Authenticode
	}
}
