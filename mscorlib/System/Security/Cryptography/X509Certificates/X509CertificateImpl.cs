﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020006C2 RID: 1730
	internal abstract class X509CertificateImpl : IDisposable
	{
		// Token: 0x17000BF0 RID: 3056
		// (get) Token: 0x06004718 RID: 18200
		public abstract bool IsValid { get; }

		// Token: 0x17000BF1 RID: 3057
		// (get) Token: 0x06004719 RID: 18201
		public abstract IntPtr Handle { get; }

		// Token: 0x0600471A RID: 18202
		public abstract IntPtr GetNativeAppleCertificate();

		// Token: 0x0600471B RID: 18203 RVA: 0x000F8345 File Offset: 0x000F6545
		protected void ThrowIfContextInvalid()
		{
			if (!this.IsValid)
			{
				throw X509Helper.GetInvalidContextException();
			}
		}

		// Token: 0x0600471C RID: 18204
		public abstract X509CertificateImpl Clone();

		// Token: 0x0600471D RID: 18205
		public abstract string GetIssuerName(bool legacyV1Mode);

		// Token: 0x0600471E RID: 18206
		public abstract string GetSubjectName(bool legacyV1Mode);

		// Token: 0x0600471F RID: 18207
		public abstract byte[] GetRawCertData();

		// Token: 0x06004720 RID: 18208
		public abstract DateTime GetValidFrom();

		// Token: 0x06004721 RID: 18209
		public abstract DateTime GetValidUntil();

		// Token: 0x06004722 RID: 18210 RVA: 0x000F8355 File Offset: 0x000F6555
		public byte[] GetCertHash()
		{
			this.ThrowIfContextInvalid();
			if (this.cachedCertificateHash == null)
			{
				this.cachedCertificateHash = this.GetCertHash(false);
			}
			return this.cachedCertificateHash;
		}

		// Token: 0x06004723 RID: 18211
		protected abstract byte[] GetCertHash(bool lazy);

		// Token: 0x06004724 RID: 18212 RVA: 0x000F8378 File Offset: 0x000F6578
		public override int GetHashCode()
		{
			if (!this.IsValid)
			{
				return 0;
			}
			if (this.cachedCertificateHash == null)
			{
				this.cachedCertificateHash = this.GetCertHash(true);
			}
			if (this.cachedCertificateHash != null && this.cachedCertificateHash.Length >= 4)
			{
				return (int)this.cachedCertificateHash[0] << 24 | (int)this.cachedCertificateHash[1] << 16 | (int)this.cachedCertificateHash[2] << 8 | (int)this.cachedCertificateHash[3];
			}
			return 0;
		}

		// Token: 0x06004725 RID: 18213
		public abstract bool Equals(X509CertificateImpl other, out bool result);

		// Token: 0x06004726 RID: 18214
		public abstract string GetKeyAlgorithm();

		// Token: 0x06004727 RID: 18215
		public abstract byte[] GetKeyAlgorithmParameters();

		// Token: 0x06004728 RID: 18216
		public abstract byte[] GetPublicKey();

		// Token: 0x06004729 RID: 18217
		public abstract byte[] GetSerialNumber();

		// Token: 0x0600472A RID: 18218
		public abstract byte[] Export(X509ContentType contentType, byte[] password);

		// Token: 0x0600472B RID: 18219
		public abstract string ToString(bool full);

		// Token: 0x0600472C RID: 18220 RVA: 0x000F83E4 File Offset: 0x000F65E4
		public override bool Equals(object obj)
		{
			X509CertificateImpl x509CertificateImpl = obj as X509CertificateImpl;
			if (x509CertificateImpl == null)
			{
				return false;
			}
			if (!this.IsValid || !x509CertificateImpl.IsValid)
			{
				return false;
			}
			bool result;
			if (this.Equals(x509CertificateImpl, out result))
			{
				return result;
			}
			byte[] rawCertData = this.GetRawCertData();
			byte[] rawCertData2 = x509CertificateImpl.GetRawCertData();
			if (rawCertData == null)
			{
				return rawCertData2 == null;
			}
			if (rawCertData2 == null)
			{
				return false;
			}
			if (rawCertData.Length != rawCertData2.Length)
			{
				return false;
			}
			for (int i = 0; i < rawCertData.Length; i++)
			{
				if (rawCertData[i] != rawCertData2[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600472D RID: 18221 RVA: 0x000F8460 File Offset: 0x000F6660
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600472E RID: 18222 RVA: 0x000F846F File Offset: 0x000F666F
		protected virtual void Dispose(bool disposing)
		{
			this.cachedCertificateHash = null;
		}

		// Token: 0x0600472F RID: 18223 RVA: 0x000F8478 File Offset: 0x000F6678
		~X509CertificateImpl()
		{
			this.Dispose(false);
		}

		// Token: 0x06004730 RID: 18224 RVA: 0x00002050 File Offset: 0x00000250
		protected X509CertificateImpl()
		{
		}

		// Token: 0x040024C8 RID: 9416
		private byte[] cachedCertificateHash;
	}
}
