﻿using System;
using System.Text;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020006C3 RID: 1731
	internal sealed class X509CertificateImplMono : X509CertificateImpl
	{
		// Token: 0x06004731 RID: 18225 RVA: 0x000F84A8 File Offset: 0x000F66A8
		public X509CertificateImplMono(X509Certificate x509)
		{
			this.x509 = x509;
		}

		// Token: 0x17000BF2 RID: 3058
		// (get) Token: 0x06004732 RID: 18226 RVA: 0x000F84B7 File Offset: 0x000F66B7
		public override bool IsValid
		{
			get
			{
				return this.x509 != null;
			}
		}

		// Token: 0x17000BF3 RID: 3059
		// (get) Token: 0x06004733 RID: 18227 RVA: 0x000F2DFC File Offset: 0x000F0FFC
		public override IntPtr Handle
		{
			get
			{
				return IntPtr.Zero;
			}
		}

		// Token: 0x06004734 RID: 18228 RVA: 0x000F2DFC File Offset: 0x000F0FFC
		public override IntPtr GetNativeAppleCertificate()
		{
			return IntPtr.Zero;
		}

		// Token: 0x06004735 RID: 18229 RVA: 0x000F84C2 File Offset: 0x000F66C2
		public override X509CertificateImpl Clone()
		{
			base.ThrowIfContextInvalid();
			return new X509CertificateImplMono(this.x509);
		}

		// Token: 0x06004736 RID: 18230 RVA: 0x000F84D5 File Offset: 0x000F66D5
		public override string GetIssuerName(bool legacyV1Mode)
		{
			base.ThrowIfContextInvalid();
			if (legacyV1Mode)
			{
				return this.x509.IssuerName;
			}
			return X501.ToString(this.x509.GetIssuerName(), true, ", ", true);
		}

		// Token: 0x06004737 RID: 18231 RVA: 0x000F8503 File Offset: 0x000F6703
		public override string GetSubjectName(bool legacyV1Mode)
		{
			base.ThrowIfContextInvalid();
			if (legacyV1Mode)
			{
				return this.x509.SubjectName;
			}
			return X501.ToString(this.x509.GetSubjectName(), true, ", ", true);
		}

		// Token: 0x06004738 RID: 18232 RVA: 0x000F8531 File Offset: 0x000F6731
		public override byte[] GetRawCertData()
		{
			base.ThrowIfContextInvalid();
			return this.x509.RawData;
		}

		// Token: 0x06004739 RID: 18233 RVA: 0x000F8544 File Offset: 0x000F6744
		protected override byte[] GetCertHash(bool lazy)
		{
			base.ThrowIfContextInvalid();
			return SHA1.Create().ComputeHash(this.x509.RawData);
		}

		// Token: 0x0600473A RID: 18234 RVA: 0x000F8561 File Offset: 0x000F6761
		public override DateTime GetValidFrom()
		{
			base.ThrowIfContextInvalid();
			return this.x509.ValidFrom;
		}

		// Token: 0x0600473B RID: 18235 RVA: 0x000F8574 File Offset: 0x000F6774
		public override DateTime GetValidUntil()
		{
			base.ThrowIfContextInvalid();
			return this.x509.ValidUntil;
		}

		// Token: 0x0600473C RID: 18236 RVA: 0x000F8587 File Offset: 0x000F6787
		public override bool Equals(X509CertificateImpl other, out bool result)
		{
			result = false;
			return false;
		}

		// Token: 0x0600473D RID: 18237 RVA: 0x000F858D File Offset: 0x000F678D
		public override string GetKeyAlgorithm()
		{
			base.ThrowIfContextInvalid();
			return this.x509.KeyAlgorithm;
		}

		// Token: 0x0600473E RID: 18238 RVA: 0x000F85A0 File Offset: 0x000F67A0
		public override byte[] GetKeyAlgorithmParameters()
		{
			base.ThrowIfContextInvalid();
			return this.x509.KeyAlgorithmParameters;
		}

		// Token: 0x0600473F RID: 18239 RVA: 0x000F85B3 File Offset: 0x000F67B3
		public override byte[] GetPublicKey()
		{
			base.ThrowIfContextInvalid();
			return this.x509.PublicKey;
		}

		// Token: 0x06004740 RID: 18240 RVA: 0x000F85C6 File Offset: 0x000F67C6
		public override byte[] GetSerialNumber()
		{
			base.ThrowIfContextInvalid();
			return this.x509.SerialNumber;
		}

		// Token: 0x06004741 RID: 18241 RVA: 0x000F85DC File Offset: 0x000F67DC
		public override byte[] Export(X509ContentType contentType, byte[] password)
		{
			base.ThrowIfContextInvalid();
			switch (contentType)
			{
			case X509ContentType.Cert:
				return this.GetRawCertData();
			case X509ContentType.SerializedCert:
				throw new NotSupportedException();
			case X509ContentType.Pfx:
				throw new NotSupportedException();
			default:
				throw new CryptographicException(Locale.GetText("This certificate format '{0}' cannot be exported.", new object[]
				{
					contentType
				}));
			}
		}

		// Token: 0x06004742 RID: 18242 RVA: 0x000F8638 File Offset: 0x000F6838
		public override string ToString(bool full)
		{
			base.ThrowIfContextInvalid();
			string newLine = Environment.NewLine;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("[Subject]{0}  {1}{0}{0}", newLine, this.GetSubjectName(false));
			stringBuilder.AppendFormat("[Issuer]{0}  {1}{0}{0}", newLine, this.GetIssuerName(false));
			stringBuilder.AppendFormat("[Not Before]{0}  {1}{0}{0}", newLine, this.GetValidFrom().ToLocalTime());
			stringBuilder.AppendFormat("[Not After]{0}  {1}{0}{0}", newLine, this.GetValidUntil().ToLocalTime());
			stringBuilder.AppendFormat("[Thumbprint]{0}  {1}{0}", newLine, X509Helper.ToHexString(base.GetCertHash()));
			stringBuilder.Append(newLine);
			return stringBuilder.ToString();
		}

		// Token: 0x06004743 RID: 18243 RVA: 0x000F86E3 File Offset: 0x000F68E3
		protected override void Dispose(bool disposing)
		{
			this.x509 = null;
		}

		// Token: 0x040024C9 RID: 9417
		private X509Certificate x509;
	}
}
