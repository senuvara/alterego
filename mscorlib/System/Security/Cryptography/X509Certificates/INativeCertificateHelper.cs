﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020006C0 RID: 1728
	internal interface INativeCertificateHelper
	{
		// Token: 0x060046D8 RID: 18136
		X509CertificateImpl Import(byte[] data, string password, X509KeyStorageFlags flags);

		// Token: 0x060046D9 RID: 18137
		X509CertificateImpl Import(X509Certificate cert);
	}
}
