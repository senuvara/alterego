﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020006BF RID: 1727
	internal enum OidKeyType
	{
		// Token: 0x040024BE RID: 9406
		Oid = 1,
		// Token: 0x040024BF RID: 9407
		Name,
		// Token: 0x040024C0 RID: 9408
		AlgorithmID,
		// Token: 0x040024C1 RID: 9409
		SignatureID,
		// Token: 0x040024C2 RID: 9410
		CngAlgorithmID,
		// Token: 0x040024C3 RID: 9411
		CngSignatureID
	}
}
