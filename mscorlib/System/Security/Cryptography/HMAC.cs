﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Represents the abstract class from which all implementations of Hash-based Message Authentication Code (HMAC) must derive.</summary>
	// Token: 0x02000677 RID: 1655
	[ComVisible(true)]
	public abstract class HMAC : KeyedHashAlgorithm
	{
		/// <summary>Gets or sets the block size to use in the hash value.</summary>
		/// <returns>The block size to use in the hash value.</returns>
		// Token: 0x17000B8A RID: 2954
		// (get) Token: 0x06004484 RID: 17540 RVA: 0x000E8FCD File Offset: 0x000E71CD
		// (set) Token: 0x06004485 RID: 17541 RVA: 0x000E8FD5 File Offset: 0x000E71D5
		protected int BlockSizeValue
		{
			get
			{
				return this.blockSizeValue;
			}
			set
			{
				this.blockSizeValue = value;
			}
		}

		// Token: 0x06004486 RID: 17542 RVA: 0x000E8FE0 File Offset: 0x000E71E0
		private void UpdateIOPadBuffers()
		{
			if (this.m_inner == null)
			{
				this.m_inner = new byte[this.BlockSizeValue];
			}
			if (this.m_outer == null)
			{
				this.m_outer = new byte[this.BlockSizeValue];
			}
			for (int i = 0; i < this.BlockSizeValue; i++)
			{
				this.m_inner[i] = 54;
				this.m_outer[i] = 92;
			}
			for (int i = 0; i < this.KeyValue.Length; i++)
			{
				byte[] inner = this.m_inner;
				int num = i;
				inner[num] ^= this.KeyValue[i];
				byte[] outer = this.m_outer;
				int num2 = i;
				outer[num2] ^= this.KeyValue[i];
			}
		}

		// Token: 0x06004487 RID: 17543 RVA: 0x000E908C File Offset: 0x000E728C
		internal void InitializeKey(byte[] key)
		{
			this.m_inner = null;
			this.m_outer = null;
			if (key.Length > this.BlockSizeValue)
			{
				this.KeyValue = this.m_hash1.ComputeHash(key);
			}
			else
			{
				this.KeyValue = (byte[])key.Clone();
			}
			this.UpdateIOPadBuffers();
		}

		/// <summary>Gets or sets the key to use in the hash algorithm.</summary>
		/// <returns>The key to use in the hash algorithm.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An attempt is made to change the <see cref="P:System.Security.Cryptography.HMAC.Key" /> property after hashing has begun. </exception>
		// Token: 0x17000B8B RID: 2955
		// (get) Token: 0x06004488 RID: 17544 RVA: 0x000E90DD File Offset: 0x000E72DD
		// (set) Token: 0x06004489 RID: 17545 RVA: 0x000E90EF File Offset: 0x000E72EF
		public override byte[] Key
		{
			get
			{
				return (byte[])this.KeyValue.Clone();
			}
			set
			{
				if (this.m_hashing)
				{
					throw new CryptographicException(Environment.GetResourceString("Hash key cannot be changed after the first write to the stream."));
				}
				this.InitializeKey(value);
			}
		}

		/// <summary>Gets or sets the name of the hash algorithm to use for hashing.</summary>
		/// <returns>The name of the hash algorithm.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The current hash algorithm cannot be changed.</exception>
		// Token: 0x17000B8C RID: 2956
		// (get) Token: 0x0600448A RID: 17546 RVA: 0x000E9110 File Offset: 0x000E7310
		// (set) Token: 0x0600448B RID: 17547 RVA: 0x000E9118 File Offset: 0x000E7318
		public string HashName
		{
			get
			{
				return this.m_hashName;
			}
			set
			{
				if (this.m_hashing)
				{
					throw new CryptographicException(Environment.GetResourceString("Hash name cannot be changed after the first write to the stream."));
				}
				this.m_hashName = value;
				this.m_hash1 = HashAlgorithm.Create(this.m_hashName);
				this.m_hash2 = HashAlgorithm.Create(this.m_hashName);
			}
		}

		/// <summary>Creates an instance of the default implementation of a Hash-based Message Authentication Code (HMAC).</summary>
		/// <returns>A new SHA-1 instance, unless the default settings have been changed by using the &lt;cryptoClass&gt; element.</returns>
		// Token: 0x0600448C RID: 17548 RVA: 0x000E9166 File Offset: 0x000E7366
		public new static HMAC Create()
		{
			return new HMACSHA1();
		}

		/// <summary>Creates an instance of the specified implementation of a Hash-based Message Authentication Code (HMAC).</summary>
		/// <param name="algorithmName">The HMAC implementation to use. The following table shows the valid values for the <paramref name="algorithmName" /> parameter and the algorithms they map to.Parameter valueImplements System.Security.Cryptography.HMAC
		///               <see cref="T:System.Security.Cryptography.HMACSHA1" />
		///             System.Security.Cryptography.KeyedHashAlgorithm
		///               <see cref="T:System.Security.Cryptography.HMACSHA1" />
		///             HMACMD5
		///               <see cref="T:System.Security.Cryptography.HMACMD5" />
		///             System.Security.Cryptography.HMACMD5
		///               <see cref="T:System.Security.Cryptography.HMACMD5" />
		///             HMACRIPEMD160
		///               <see cref="T:System.Security.Cryptography.HMACRIPEMD160" />
		///             System.Security.Cryptography.HMACRIPEMD160
		///               <see cref="T:System.Security.Cryptography.HMACRIPEMD160" />
		///             HMACSHA1
		///               <see cref="T:System.Security.Cryptography.HMACSHA1" />
		///             System.Security.Cryptography.HMACSHA1
		///               <see cref="T:System.Security.Cryptography.HMACSHA1" />
		///             HMACSHA256
		///               <see cref="T:System.Security.Cryptography.HMACSHA256" />
		///             System.Security.Cryptography.HMACSHA256
		///               <see cref="T:System.Security.Cryptography.HMACSHA256" />
		///             HMACSHA384
		///               <see cref="T:System.Security.Cryptography.HMACSHA384" />
		///             System.Security.Cryptography.HMACSHA384
		///               <see cref="T:System.Security.Cryptography.HMACSHA384" />
		///             HMACSHA512
		///               <see cref="T:System.Security.Cryptography.HMACSHA512" />
		///             System.Security.Cryptography.HMACSHA512
		///               <see cref="T:System.Security.Cryptography.HMACSHA512" />
		///             MACTripleDES
		///               <see cref="T:System.Security.Cryptography.MACTripleDES" />
		///             System.Security.Cryptography.MACTripleDES
		///               <see cref="T:System.Security.Cryptography.MACTripleDES" />
		///             </param>
		/// <returns>A new instance of the specified HMAC implementation.</returns>
		// Token: 0x0600448D RID: 17549 RVA: 0x000E916D File Offset: 0x000E736D
		public new static HMAC Create(string algorithmName)
		{
			return (HMAC)CryptoConfig.CreateFromName(algorithmName);
		}

		/// <summary>Initializes an instance of the default implementation of <see cref="T:System.Security.Cryptography.HMAC" />.</summary>
		// Token: 0x0600448E RID: 17550 RVA: 0x000E917A File Offset: 0x000E737A
		public override void Initialize()
		{
			this.m_hash1.Initialize();
			this.m_hash2.Initialize();
			this.m_hashing = false;
		}

		/// <summary>When overridden in a derived class, routes data written to the object into the default <see cref="T:System.Security.Cryptography.HMAC" /> hash algorithm for computing the hash value.</summary>
		/// <param name="rgb">The input data. </param>
		/// <param name="ib">The offset into the byte array from which to begin using data. </param>
		/// <param name="cb">The number of bytes in the array to use as data. </param>
		// Token: 0x0600448F RID: 17551 RVA: 0x000E919C File Offset: 0x000E739C
		protected override void HashCore(byte[] rgb, int ib, int cb)
		{
			if (!this.m_hashing)
			{
				this.m_hash1.TransformBlock(this.m_inner, 0, this.m_inner.Length, this.m_inner, 0);
				this.m_hashing = true;
			}
			this.m_hash1.TransformBlock(rgb, ib, cb, rgb, ib);
		}

		/// <summary>When overridden in a derived class, finalizes the hash computation after the last data is processed by the cryptographic stream object.</summary>
		/// <returns>The computed hash code in a byte array.</returns>
		// Token: 0x06004490 RID: 17552 RVA: 0x000E91EC File Offset: 0x000E73EC
		protected override byte[] HashFinal()
		{
			if (!this.m_hashing)
			{
				this.m_hash1.TransformBlock(this.m_inner, 0, this.m_inner.Length, this.m_inner, 0);
				this.m_hashing = true;
			}
			this.m_hash1.TransformFinalBlock(EmptyArray<byte>.Value, 0, 0);
			byte[] hashValue = this.m_hash1.HashValue;
			this.m_hash2.TransformBlock(this.m_outer, 0, this.m_outer.Length, this.m_outer, 0);
			this.m_hash2.TransformBlock(hashValue, 0, hashValue.Length, hashValue, 0);
			this.m_hashing = false;
			this.m_hash2.TransformFinalBlock(EmptyArray<byte>.Value, 0, 0);
			return this.m_hash2.HashValue;
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Security.Cryptography.HMAC" /> class when a key change is legitimate and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06004491 RID: 17553 RVA: 0x000E92A4 File Offset: 0x000E74A4
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.m_hash1 != null)
				{
					((IDisposable)this.m_hash1).Dispose();
				}
				if (this.m_hash2 != null)
				{
					((IDisposable)this.m_hash2).Dispose();
				}
				if (this.m_inner != null)
				{
					Array.Clear(this.m_inner, 0, this.m_inner.Length);
				}
				if (this.m_outer != null)
				{
					Array.Clear(this.m_outer, 0, this.m_outer.Length);
				}
			}
			base.Dispose(disposing);
		}

		// Token: 0x06004492 RID: 17554 RVA: 0x000E931C File Offset: 0x000E751C
		internal static HashAlgorithm GetHashAlgorithmWithFipsFallback(Func<HashAlgorithm> createStandardHashAlgorithmCallback, Func<HashAlgorithm> createFipsHashAlgorithmCallback)
		{
			if (CryptoConfig.AllowOnlyFipsAlgorithms)
			{
				try
				{
					return createFipsHashAlgorithmCallback();
				}
				catch (PlatformNotSupportedException ex)
				{
					throw new InvalidOperationException(ex.Message, ex);
				}
			}
			return createStandardHashAlgorithmCallback();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.HMAC" /> class. </summary>
		// Token: 0x06004493 RID: 17555 RVA: 0x000E9360 File Offset: 0x000E7560
		protected HMAC()
		{
		}

		// Token: 0x04002363 RID: 9059
		private int blockSizeValue = 64;

		// Token: 0x04002364 RID: 9060
		internal string m_hashName;

		// Token: 0x04002365 RID: 9061
		internal HashAlgorithm m_hash1;

		// Token: 0x04002366 RID: 9062
		internal HashAlgorithm m_hash2;

		// Token: 0x04002367 RID: 9063
		private byte[] m_inner;

		// Token: 0x04002368 RID: 9064
		private byte[] m_outer;

		// Token: 0x04002369 RID: 9065
		private bool m_hashing;
	}
}
