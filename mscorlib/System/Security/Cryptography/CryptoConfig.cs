﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	/// <summary>Accesses the cryptography configuration information.</summary>
	// Token: 0x020006AD RID: 1709
	[ComVisible(true)]
	public class CryptoConfig
	{
		/// <summary>Encodes the specified object identifier (OID).</summary>
		/// <param name="str">The OID to encode. </param>
		/// <returns>A byte array containing the encoded OID.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="str" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicUnexpectedOperationException">An error occurred while encoding the OID. </exception>
		// Token: 0x0600465D RID: 18013 RVA: 0x000F2E30 File Offset: 0x000F1030
		public static byte[] EncodeOID(string str)
		{
			if (str == null)
			{
				throw new ArgumentNullException("str");
			}
			char[] separator = new char[]
			{
				'.'
			};
			string[] array = str.Split(separator);
			if (array.Length < 2)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("OID must have at least two parts"));
			}
			byte[] array2 = new byte[str.Length];
			try
			{
				byte b = Convert.ToByte(array[0]);
				byte b2 = Convert.ToByte(array[1]);
				array2[2] = Convert.ToByte((int)(b * 40 + b2));
			}
			catch
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("Invalid OID"));
			}
			int num = 3;
			for (int i = 2; i < array.Length; i++)
			{
				long num2 = Convert.ToInt64(array[i]);
				if (num2 > 127L)
				{
					byte[] array3 = CryptoConfig.EncodeLongNumber(num2);
					Buffer.BlockCopy(array3, 0, array2, num, array3.Length);
					num += array3.Length;
				}
				else
				{
					array2[num++] = Convert.ToByte(num2);
				}
			}
			int num3 = 2;
			byte[] array4 = new byte[num];
			array4[0] = 6;
			if (num > 127)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("OID > 127 bytes"));
			}
			array4[1] = Convert.ToByte(num - 2);
			Buffer.BlockCopy(array2, num3, array4, num3, num - num3);
			return array4;
		}

		// Token: 0x0600465E RID: 18014 RVA: 0x000F2F60 File Offset: 0x000F1160
		private static byte[] EncodeLongNumber(long x)
		{
			if (x > 2147483647L || x < -2147483648L)
			{
				throw new OverflowException(Locale.GetText("Part of OID doesn't fit in Int32"));
			}
			long num = x;
			int num2 = 1;
			while (num > 127L)
			{
				num >>= 7;
				num2++;
			}
			byte[] array = new byte[num2];
			for (int i = 0; i < num2; i++)
			{
				num = x >> 7 * i;
				num &= 127L;
				if (i != 0)
				{
					num += 128L;
				}
				array[num2 - i - 1] = Convert.ToByte(num);
			}
			return array;
		}

		/// <summary>Indicates whether the runtime should enforce the policy to create only Federal Information Processing Standard (FIPS) certified algorithms.</summary>
		/// <returns>
		///     <see langword="true" /> to enforce the policy; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000BD5 RID: 3029
		// (get) Token: 0x0600465F RID: 18015 RVA: 0x00002526 File Offset: 0x00000726
		[MonoLimitation("nothing is FIPS certified so it never make sense to restrict to this (empty) subset")]
		public static bool AllowOnlyFipsAlgorithms
		{
			get
			{
				return false;
			}
		}

		/// <summary>Adds a set of names to algorithm mappings to be used for the current application domain.  </summary>
		/// <param name="algorithm">The algorithm to map to.</param>
		/// <param name="names">An array of names to map to the algorithm.</param>
		/// <exception cref="T:System.ArgumentNullException">The<paramref name=" algorithm" /> or <paramref name="names" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="algorithm" /> cannot be accessed from outside the assembly.-or-One of the entries in the <paramref name="names" /> parameter is empty or <see langword="null" />.</exception>
		// Token: 0x06004660 RID: 18016 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static void AddAlgorithm(Type algorithm, params string[] names)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Adds a set of names to object identifier (OID) mappings to be used for the current application domain.  </summary>
		/// <param name="oid">The object identifier (OID) to map to.</param>
		/// <param name="names">An array of names to map to the OID.</param>
		/// <exception cref="T:System.ArgumentNullException">The<paramref name=" oid" /> or <paramref name="names" /> parameter is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentException">One of the entries in the <paramref name="names" /> parameter is empty or <see langword="null" />.</exception>
		// Token: 0x06004661 RID: 18017 RVA: 0x0005A2B6 File Offset: 0x000584B6
		public static void AddOID(string oid, params string[] names)
		{
			throw new PlatformNotSupportedException();
		}

		/// <summary>Creates a new instance of the specified cryptographic object.</summary>
		/// <param name="name">The simple name of the cryptographic object of which to create an instance. </param>
		/// <returns>A new instance of the specified cryptographic object.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="name" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Reflection.TargetInvocationException">The algorithm described by the <paramref name="name" /> parameter was used with Federal Information Processing Standards (FIPS) mode enabled, but is not FIPS compatible.</exception>
		// Token: 0x06004662 RID: 18018 RVA: 0x000F2FDF File Offset: 0x000F11DF
		public static object CreateFromName(string name)
		{
			return CryptoConfig.CreateFromName(name, null);
		}

		/// <summary>Creates a new instance of the specified cryptographic object with the specified arguments.</summary>
		/// <param name="name">The simple name of the cryptographic object of which to create an instance. </param>
		/// <param name="args">The arguments used to create the specified cryptographic object. </param>
		/// <returns>A new instance of the specified cryptographic object.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="name" /> parameter is <see langword="null" />. </exception>
		/// <exception cref="T:System.Reflection.TargetInvocationException">The algorithm described by the <paramref name="name" /> parameter was used with Federal Information Processing Standards (FIPS) mode enabled, but is not FIPS compatible.</exception>
		// Token: 0x06004663 RID: 18019 RVA: 0x000F2FE8 File Offset: 0x000F11E8
		[PermissionSet(SecurityAction.LinkDemand, Unrestricted = true)]
		public static object CreateFromName(string name, params object[] args)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			string text = name.ToLowerInvariant();
			uint num = <PrivateImplementationDetails>.ComputeStringHash(text);
			if (num <= 2415328530U)
			{
				if (num <= 1279198866U)
				{
					if (num <= 589079309U)
					{
						if (num <= 401798778U)
						{
							if (num <= 294650258U)
							{
								if (num != 97835172U)
								{
									if (num != 289646596U)
									{
										if (num != 294650258U)
										{
											goto IL_E8A;
										}
										if (!(text == "3des"))
										{
											goto IL_E8A;
										}
										goto IL_E74;
									}
									else
									{
										if (!(text == "system.security.cryptography.sha1cryptoserviceprovider"))
										{
											goto IL_E8A;
										}
										goto IL_E56;
									}
								}
								else
								{
									if (!(text == "http://www.w3.org/2001/04/xmldsig-more#sha384"))
									{
										goto IL_E8A;
									}
									goto IL_E68;
								}
							}
							else if (num != 373238979U)
							{
								if (num != 381964475U)
								{
									if (num != 401798778U)
									{
										goto IL_E8A;
									}
									if (!(text == "system.security.cryptography.dsasignaturedescription"))
									{
										goto IL_E8A;
									}
								}
								else
								{
									if (!(text == "hmacsha1"))
									{
										goto IL_E8A;
									}
									goto IL_DF0;
								}
							}
							else
							{
								if (!(text == "system.security.cryptography.sha1cng"))
								{
									goto IL_E8A;
								}
								goto IL_E56;
							}
						}
						else if (num <= 550229268U)
						{
							if (num != 418711287U)
							{
								if (num != 524624695U)
								{
									if (num != 550229268U)
									{
										goto IL_E8A;
									}
									if (!(text == "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256"))
									{
										goto IL_E8A;
									}
									goto IL_DF6;
								}
								else
								{
									if (!(text == "system.security.cryptography.hashalgorithm"))
									{
										goto IL_E8A;
									}
									goto IL_E56;
								}
							}
							else
							{
								if (!(text == "system.security.cryptography.mactripledes"))
								{
									goto IL_E8A;
								}
								goto IL_E08;
							}
						}
						else if (num != 572088812U)
						{
							if (num != 585245684U)
							{
								if (num != 589079309U)
								{
									goto IL_E8A;
								}
								if (!(text == "system.security.cryptography.hmac"))
								{
									goto IL_E8A;
								}
								goto IL_DF0;
							}
							else
							{
								if (!(text == "system.security.cryptography.sha1managed"))
								{
									goto IL_E8A;
								}
								return new SHA1Managed();
							}
						}
						else
						{
							if (!(text == "http://www.w3.org/2001/04/xmldsig-more#hmac-sha384"))
							{
								goto IL_E8A;
							}
							goto IL_DFC;
						}
					}
					else if (num <= 900295094U)
					{
						if (num <= 734683829U)
						{
							if (num != 699966473U)
							{
								if (num != 708523592U)
								{
									if (num != 734683829U)
									{
										goto IL_E8A;
									}
									if (!(text == "hmacsha256"))
									{
										goto IL_E8A;
									}
									goto IL_DF6;
								}
								else
								{
									if (!(text == "aes"))
									{
										goto IL_E8A;
									}
									name = "System.Security.Cryptography.AesCryptoServiceProvider, System.Core";
									goto IL_E8A;
								}
							}
							else if (!(text == "http://www.w3.org/2000/09/xmldsig#dsa-sha1"))
							{
								goto IL_E8A;
							}
						}
						else if (num != 853553133U)
						{
							if (num != 877368883U)
							{
								if (num != 900295094U)
								{
									goto IL_E8A;
								}
								if (!(text == "system.security.cryptography.dsasignatureformatter"))
								{
									goto IL_E8A;
								}
								return new DSASignatureFormatter();
							}
							else
							{
								if (!(text == "http://www.w3.org/2000/09/xmldsig#rsa-sha1"))
								{
									goto IL_E8A;
								}
								goto IL_E3E;
							}
						}
						else
						{
							if (!(text == "system.security.cryptography.rsapkcs1sha384signaturedescription"))
							{
								goto IL_E8A;
							}
							goto IL_E4A;
						}
					}
					else if (num <= 1104969097U)
					{
						if (num != 965923590U)
						{
							if (num != 999454301U)
							{
								if (num != 1104969097U)
								{
									goto IL_E8A;
								}
								if (!(text == "rsa"))
								{
									goto IL_E8A;
								}
								goto IL_E2C;
							}
							else
							{
								if (!(text == "system.security.cryptography.rsapkcs1sha256signaturedescription"))
								{
									goto IL_E8A;
								}
								goto IL_E44;
							}
						}
						else
						{
							if (!(text == "http://www.w3.org/2000/09/xmldsig#sha1"))
							{
								goto IL_E8A;
							}
							goto IL_E56;
						}
					}
					else if (num != 1147401626U)
					{
						if (num != 1168228931U)
						{
							if (num != 1279198866U)
							{
								goto IL_E8A;
							}
							if (!(text == "system.security.cryptography.tripledes"))
							{
								goto IL_E8A;
							}
							goto IL_E74;
						}
						else
						{
							if (!(text == "system.security.cryptography.ripemd160managed"))
							{
								goto IL_E8A;
							}
							goto IL_E20;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.rsapkcs1signatureformatter"))
						{
							goto IL_E8A;
						}
						return new RSAPKCS1SignatureFormatter();
					}
					return new DSASignatureDescription();
				}
				if (num <= 1862521808U)
				{
					if (num <= 1664836558U)
					{
						if (num <= 1604759256U)
						{
							if (num != 1495151835U)
							{
								if (num != 1600607069U)
								{
									if (num != 1604759256U)
									{
										goto IL_E8A;
									}
									if (!(text == "sha-512"))
									{
										goto IL_E8A;
									}
									goto IL_E6E;
								}
								else
								{
									if (!(text == "system.security.cryptography.sha384cng"))
									{
										goto IL_E8A;
									}
									goto IL_E68;
								}
							}
							else
							{
								if (!(text == "system.security.cryptography.descryptoserviceprovider"))
								{
									goto IL_E8A;
								}
								goto IL_DDE;
							}
						}
						else if (num != 1610008198U)
						{
							if (num != 1629735498U)
							{
								if (num != 1664836558U)
								{
									goto IL_E8A;
								}
								if (!(text == "system.security.cryptography.sha256cryptoserviceprovider"))
								{
									goto IL_E8A;
								}
								goto IL_E62;
							}
							else
							{
								if (!(text == "system.security.cryptography.rc2cryptoserviceprovider"))
								{
									goto IL_E8A;
								}
								goto IL_E14;
							}
						}
						else
						{
							if (!(text == "system.security.cryptography.rijndaelmanaged"))
							{
								goto IL_E8A;
							}
							goto IL_E1A;
						}
					}
					else if (num <= 1720406050U)
					{
						if (num != 1686995390U)
						{
							if (num != 1688024611U)
							{
								if (num != 1720406050U)
								{
									goto IL_E8A;
								}
								if (!(text == "x509chain"))
								{
									goto IL_E8A;
								}
								name = "System.Security.Cryptography.X509Certificates.X509Chain, System";
								goto IL_E8A;
							}
							else
							{
								if (!(text == "http://www.w3.org/2001/04/xmlenc#sha256"))
								{
									goto IL_E8A;
								}
								goto IL_E62;
							}
						}
						else if (!(text == "system.security.cryptography.md5"))
						{
							goto IL_E8A;
						}
					}
					else if (num != 1778503857U)
					{
						if (num != 1820576144U)
						{
							if (num != 1862521808U)
							{
								goto IL_E8A;
							}
							if (!(text == "rc2"))
							{
								goto IL_E8A;
							}
							goto IL_E14;
						}
						else
						{
							if (!(text == "hmacsha512"))
							{
								goto IL_E8A;
							}
							goto IL_E02;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.randomnumbergenerator"))
						{
							goto IL_E8A;
						}
						goto IL_E26;
					}
				}
				else if (num <= 2246759783U)
				{
					if (num <= 2120664437U)
					{
						if (num != 2070555668U)
						{
							if (num != 2102584679U)
							{
								if (num != 2120664437U)
								{
									goto IL_E8A;
								}
								if (!(text == "sha-384"))
								{
									goto IL_E8A;
								}
								goto IL_E68;
							}
							else
							{
								if (!(text == "ripemd160"))
								{
									goto IL_E8A;
								}
								goto IL_E20;
							}
						}
						else
						{
							if (!(text == "sha1"))
							{
								goto IL_E8A;
							}
							goto IL_E56;
						}
					}
					else if (num != 2131651891U)
					{
						if (num != 2214607313U)
						{
							if (num != 2246759783U)
							{
								goto IL_E8A;
							}
							if (!(text == "system.security.cryptography.dsasignaturedeformatter"))
							{
								goto IL_E8A;
							}
							return new DSASignatureDeformatter();
						}
						else
						{
							if (!(text == "system.security.cryptography.sha512cryptoserviceprovider"))
							{
								goto IL_E8A;
							}
							goto IL_E6E;
						}
					}
					else
					{
						if (!(text == "tripledes"))
						{
							goto IL_E8A;
						}
						goto IL_E74;
					}
				}
				else if (num <= 2346491937U)
				{
					if (num != 2269936011U)
					{
						if (num != 2340547105U)
						{
							if (num != 2346491937U)
							{
								goto IL_E8A;
							}
							if (!(text == "system.security.cryptography.sha256"))
							{
								goto IL_E8A;
							}
							goto IL_E62;
						}
						else
						{
							if (!(text == "system.security.cryptography.keyedhashalgorithm"))
							{
								goto IL_E8A;
							}
							goto IL_DF0;
						}
					}
					else if (!(text == "system.security.cryptography.md5cryptoserviceprovider"))
					{
						goto IL_E8A;
					}
				}
				else if (num != 2393554675U)
				{
					if (num != 2394616414U)
					{
						if (num != 2415328530U)
						{
							goto IL_E8A;
						}
						if (!(text == "system.security.cryptography.hmacsha256"))
						{
							goto IL_E8A;
						}
						goto IL_DF6;
					}
					else
					{
						if (!(text == "system.security.cryptography.sha384cryptoserviceprovider"))
						{
							goto IL_E8A;
						}
						goto IL_E68;
					}
				}
				else if (!(text == "md5"))
				{
					goto IL_E8A;
				}
				return new MD5CryptoServiceProvider();
				IL_DF6:
				return new HMACSHA256();
			}
			if (num <= 3271835900U)
			{
				if (num <= 2855136637U)
				{
					if (num <= 2661179293U)
					{
						if (num <= 2478224771U)
						{
							if (num != 2442086578U)
							{
								if (num != 2451404838U)
								{
									if (num != 2478224771U)
									{
										goto IL_E8A;
									}
									if (!(text == "system.security.cryptography.hmacsha512"))
									{
										goto IL_E8A;
									}
									goto IL_E02;
								}
								else
								{
									if (!(text == "system.security.cryptography.ripemd160"))
									{
										goto IL_E8A;
									}
									goto IL_E20;
								}
							}
							else
							{
								if (!(text == "hmacripemd160"))
								{
									goto IL_E8A;
								}
								goto IL_DEA;
							}
						}
						else if (num != 2484875538U)
						{
							if (num != 2631153146U)
							{
								if (num != 2661179293U)
								{
									goto IL_E8A;
								}
								if (!(text == "sha-256"))
								{
									goto IL_E8A;
								}
								goto IL_E62;
							}
							else
							{
								if (!(text == "sha256"))
								{
									goto IL_E8A;
								}
								goto IL_E62;
							}
						}
						else
						{
							if (!(text == "system.security.cryptography.sha256managed"))
							{
								goto IL_E8A;
							}
							goto IL_E62;
						}
					}
					else if (num <= 2700614742U)
					{
						if (num != 2685283101U)
						{
							if (num != 2694049387U)
							{
								if (num != 2700614742U)
								{
									goto IL_E8A;
								}
								if (!(text == "sha384"))
								{
									goto IL_E8A;
								}
								goto IL_E68;
							}
							else
							{
								if (!(text == "sha512"))
								{
									goto IL_E8A;
								}
								goto IL_E6E;
							}
						}
						else
						{
							if (!(text == "system.security.cryptography.rc2"))
							{
								goto IL_E8A;
							}
							goto IL_E14;
						}
					}
					else if (num != 2803157229U)
					{
						if (num != 2824063256U)
						{
							if (num != 2855136637U)
							{
								goto IL_E8A;
							}
							if (!(text == "hmacsha384"))
							{
								goto IL_E8A;
							}
							goto IL_DFC;
						}
						else if (!(text == "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512"))
						{
							goto IL_E8A;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.sha256cng"))
						{
							goto IL_E8A;
						}
						goto IL_E62;
					}
				}
				else if (num <= 3091284687U)
				{
					if (num <= 2930817943U)
					{
						if (num != 2920802226U)
						{
							if (num != 2930374873U)
							{
								if (num != 2930817943U)
								{
									goto IL_E8A;
								}
								if (!(text == "system.security.cryptography.sha1"))
								{
									goto IL_E8A;
								}
								goto IL_E56;
							}
							else
							{
								if (!(text == "http://www.w3.org/2001/04/xmldsig-more#hmac-sha512"))
								{
									goto IL_E8A;
								}
								goto IL_E02;
							}
						}
						else
						{
							if (!(text == "rijndael"))
							{
								goto IL_E8A;
							}
							goto IL_E1A;
						}
					}
					else if (num != 3024233790U)
					{
						if (num != 3071220272U)
						{
							if (num != 3091284687U)
							{
								goto IL_E8A;
							}
							if (!(text == "system.security.cryptography.rsapkcs1sha1signaturedescription"))
							{
								goto IL_E8A;
							}
							goto IL_E3E;
						}
						else if (!(text == "system.security.cryptography.rsapkcs1sha512signaturedescription"))
						{
							goto IL_E8A;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.hmacsha384"))
						{
							goto IL_E8A;
						}
						goto IL_DFC;
					}
				}
				else if (num <= 3177008669U)
				{
					if (num != 3106619289U)
					{
						if (num != 3155186700U)
						{
							if (num != 3177008669U)
							{
								goto IL_E8A;
							}
							if (!(text == "triple des"))
							{
								goto IL_E8A;
							}
							goto IL_E74;
						}
						else
						{
							if (!(text == "system.security.cryptography.hmacsha1"))
							{
								goto IL_E8A;
							}
							goto IL_DF0;
						}
					}
					else
					{
						if (!(text == "http://www.w3.org/2001/04/xmldsig-more#hmac-ripemd160"))
						{
							goto IL_E8A;
						}
						goto IL_DEA;
					}
				}
				else if (num != 3193284448U)
				{
					if (num != 3223542963U)
					{
						if (num != 3271835900U)
						{
							goto IL_E8A;
						}
						if (!(text == "randomnumbergenerator"))
						{
							goto IL_E8A;
						}
						goto IL_E26;
					}
					else
					{
						if (!(text == "dsa"))
						{
							goto IL_E8A;
						}
						goto IL_DC6;
					}
				}
				else
				{
					if (!(text == "system.security.cryptography.rngcryptoserviceprovider"))
					{
						goto IL_E8A;
					}
					goto IL_E26;
				}
				return new RSAPKCS1SHA512SignatureDescription();
			}
			if (num <= 3849984186U)
			{
				if (num <= 3529085699U)
				{
					if (num <= 3416134926U)
					{
						if (num != 3295766687U)
						{
							if (num != 3339968437U)
							{
								if (num != 3416134926U)
								{
									goto IL_E8A;
								}
								if (!(text == "system.security.cryptography.des"))
								{
									goto IL_E8A;
								}
								goto IL_DDE;
							}
							else
							{
								if (!(text == "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384"))
								{
									goto IL_E8A;
								}
								goto IL_E4A;
							}
						}
						else
						{
							if (!(text == "system.security.cryptography.hmacripemd160"))
							{
								goto IL_E8A;
							}
							goto IL_DEA;
						}
					}
					else if (num != 3442835812U)
					{
						if (num != 3457114317U)
						{
							if (num != 3529085699U)
							{
								goto IL_E8A;
							}
							if (!(text == "des"))
							{
								goto IL_E8A;
							}
							goto IL_DDE;
						}
						else
						{
							if (!(text == "system.security.cryptography.sha512managed"))
							{
								goto IL_E8A;
							}
							goto IL_E6E;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.rsa"))
						{
							goto IL_E8A;
						}
						goto IL_E2C;
					}
				}
				else if (num <= 3711531707U)
				{
					if (num != 3569803917U)
					{
						if (num != 3708241362U)
						{
							if (num != 3711531707U)
							{
								goto IL_E8A;
							}
							if (!(text == "system.security.cryptography.hmacmd5"))
							{
								goto IL_E8A;
							}
						}
						else
						{
							if (!(text == "system.security.cryptography.sha512cng"))
							{
								goto IL_E8A;
							}
							goto IL_E6E;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.asymmetricalgorithm"))
						{
							goto IL_E8A;
						}
						goto IL_E2C;
					}
				}
				else if (num != 3772560434U)
				{
					if (num != 3841402386U)
					{
						if (num != 3849984186U)
						{
							goto IL_E8A;
						}
						if (!(text == "system.security.cryptography.dsa"))
						{
							goto IL_E8A;
						}
						goto IL_DC6;
					}
					else
					{
						if (!(text == "ripemd-160"))
						{
							goto IL_E8A;
						}
						goto IL_E20;
					}
				}
				else
				{
					if (!(text == "http://www.w3.org/2001/04/xmlenc#sha512"))
					{
						goto IL_E8A;
					}
					goto IL_E6E;
				}
			}
			else if (num <= 4092224578U)
			{
				if (num <= 3991591796U)
				{
					if (num != 3880483293U)
					{
						if (num != 3979214893U)
						{
							if (num != 3991591796U)
							{
								goto IL_E8A;
							}
							if (!(text == "system.security.cryptography.sha512"))
							{
								goto IL_E8A;
							}
							goto IL_E6E;
						}
						else
						{
							if (!(text == "sha"))
							{
								goto IL_E8A;
							}
							goto IL_E56;
						}
					}
					else
					{
						if (!(text == "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"))
						{
							goto IL_E8A;
						}
						goto IL_E44;
					}
				}
				else if (num != 4003337351U)
				{
					if (num != 4070407701U)
					{
						if (num != 4092224578U)
						{
							goto IL_E8A;
						}
						if (!(text == "hmacmd5"))
						{
							goto IL_E8A;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.rijndael"))
						{
							goto IL_E8A;
						}
						goto IL_E1A;
					}
				}
				else
				{
					if (!(text == "system.security.cryptography.dsacryptoserviceprovider"))
					{
						goto IL_E8A;
					}
					goto IL_DC6;
				}
			}
			else if (num <= 4120168715U)
			{
				if (num != 4106266752U)
				{
					if (num != 4109837759U)
					{
						if (num != 4120168715U)
						{
							goto IL_E8A;
						}
						if (!(text == "system.security.cryptography.rsapkcs1signaturedeformatter"))
						{
							goto IL_E8A;
						}
						return new RSAPKCS1SignatureDeformatter();
					}
					else
					{
						if (!(text == "system.security.cryptography.tripledescryptoserviceprovider"))
						{
							goto IL_E8A;
						}
						goto IL_E74;
					}
				}
				else
				{
					if (!(text == "mactripledes"))
					{
						goto IL_E8A;
					}
					goto IL_E08;
				}
			}
			else if (num != 4181060418U)
			{
				if (num != 4199782769U)
				{
					if (num != 4265317454U)
					{
						goto IL_E8A;
					}
					if (!(text == "system.security.cryptography.symmetricalgorithm"))
					{
						goto IL_E8A;
					}
					goto IL_E1A;
				}
				else
				{
					if (!(text == "system.security.cryptography.sha384"))
					{
						goto IL_E8A;
					}
					goto IL_E68;
				}
			}
			else
			{
				if (!(text == "system.security.cryptography.sha384managed"))
				{
					goto IL_E8A;
				}
				goto IL_E68;
			}
			return new HMACMD5();
			IL_DC6:
			return new DSACryptoServiceProvider();
			IL_DEA:
			return new HMACRIPEMD160();
			IL_DDE:
			return new DESCryptoServiceProvider();
			IL_DF0:
			return new HMACSHA1();
			IL_DFC:
			return new HMACSHA384();
			IL_E02:
			return new HMACSHA512();
			IL_E08:
			return new MACTripleDES();
			IL_E14:
			return new RC2CryptoServiceProvider();
			IL_E1A:
			return new RijndaelManaged();
			IL_E20:
			return new RIPEMD160Managed();
			IL_E26:
			return new RNGCryptoServiceProvider();
			IL_E2C:
			return new RSACryptoServiceProvider();
			IL_E3E:
			return new RSAPKCS1SHA1SignatureDescription();
			IL_E44:
			return new RSAPKCS1SHA256SignatureDescription();
			IL_E4A:
			return new RSAPKCS1SHA384SignatureDescription();
			IL_E56:
			return new SHA1CryptoServiceProvider();
			IL_E62:
			return new SHA256Managed();
			IL_E68:
			return new SHA384Managed();
			IL_E6E:
			return new SHA512Managed();
			IL_E74:
			return new TripleDESCryptoServiceProvider();
			IL_E8A:
			object result;
			try
			{
				result = Activator.CreateInstance(Type.GetType(name));
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06004664 RID: 18020 RVA: 0x000F3EA4 File Offset: 0x000F20A4
		internal static string MapNameToOID(string name, object arg)
		{
			return CryptoConfig.MapNameToOID(name);
		}

		/// <summary>Gets the object identifier (OID) of the algorithm corresponding to the specified simple name.</summary>
		/// <param name="name">The simple name of the algorithm for which to get the OID. </param>
		/// <returns>The OID of the specified algorithm.</returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="name" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06004665 RID: 18021 RVA: 0x000F3EAC File Offset: 0x000F20AC
		public static string MapNameToOID(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			string text = name.ToLowerInvariant();
			uint num = <PrivateImplementationDetails>.ComputeStringHash(text);
			if (num <= 2393554675U)
			{
				if (num <= 1686995390U)
				{
					if (num <= 585245684U)
					{
						if (num != 289646596U)
						{
							if (num != 373238979U)
							{
								if (num != 585245684U)
								{
									goto IL_4A8;
								}
								if (!(text == "system.security.cryptography.sha1managed"))
								{
									goto IL_4A8;
								}
								goto IL_46C;
							}
							else
							{
								if (!(text == "system.security.cryptography.sha1cng"))
								{
									goto IL_4A8;
								}
								goto IL_46C;
							}
						}
						else
						{
							if (!(text == "system.security.cryptography.sha1cryptoserviceprovider"))
							{
								goto IL_4A8;
							}
							goto IL_46C;
						}
					}
					else if (num <= 1600607069U)
					{
						if (num != 1168228931U)
						{
							if (num != 1600607069U)
							{
								goto IL_4A8;
							}
							if (!(text == "system.security.cryptography.sha384cng"))
							{
								goto IL_4A8;
							}
							goto IL_47E;
						}
						else
						{
							if (!(text == "system.security.cryptography.ripemd160managed"))
							{
								goto IL_4A8;
							}
							goto IL_48A;
						}
					}
					else if (num != 1664836558U)
					{
						if (num != 1686995390U)
						{
							goto IL_4A8;
						}
						if (!(text == "system.security.cryptography.md5"))
						{
							goto IL_4A8;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.sha256cryptoserviceprovider"))
						{
							goto IL_4A8;
						}
						goto IL_478;
					}
				}
				else if (num <= 2131651891U)
				{
					if (num <= 2070555668U)
					{
						if (num != 1862521808U)
						{
							if (num != 2070555668U)
							{
								goto IL_4A8;
							}
							if (!(text == "sha1"))
							{
								goto IL_4A8;
							}
							goto IL_46C;
						}
						else
						{
							if (!(text == "rc2"))
							{
								goto IL_4A8;
							}
							return "1.2.840.113549.3.2";
						}
					}
					else if (num != 2102584679U)
					{
						if (num != 2131651891U)
						{
							goto IL_4A8;
						}
						if (!(text == "tripledes"))
						{
							goto IL_4A8;
						}
						return "1.2.840.113549.3.7";
					}
					else
					{
						if (!(text == "ripemd160"))
						{
							goto IL_4A8;
						}
						goto IL_48A;
					}
				}
				else if (num <= 2269936011U)
				{
					if (num != 2214607313U)
					{
						if (num != 2269936011U)
						{
							goto IL_4A8;
						}
						if (!(text == "system.security.cryptography.md5cryptoserviceprovider"))
						{
							goto IL_4A8;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.sha512cryptoserviceprovider"))
						{
							goto IL_4A8;
						}
						goto IL_484;
					}
				}
				else if (num != 2346491937U)
				{
					if (num != 2393554675U)
					{
						goto IL_4A8;
					}
					if (!(text == "md5"))
					{
						goto IL_4A8;
					}
				}
				else
				{
					if (!(text == "system.security.cryptography.sha256"))
					{
						goto IL_4A8;
					}
					goto IL_478;
				}
				return "1.2.840.113549.2.5";
			}
			if (num <= 2700614742U)
			{
				if (num <= 2484875538U)
				{
					if (num != 2394616414U)
					{
						if (num != 2451404838U)
						{
							if (num != 2484875538U)
							{
								goto IL_4A8;
							}
							if (!(text == "system.security.cryptography.sha256managed"))
							{
								goto IL_4A8;
							}
							goto IL_478;
						}
						else
						{
							if (!(text == "system.security.cryptography.ripemd160"))
							{
								goto IL_4A8;
							}
							goto IL_48A;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.sha384cryptoserviceprovider"))
						{
							goto IL_4A8;
						}
						goto IL_47E;
					}
				}
				else if (num <= 2672027822U)
				{
					if (num != 2631153146U)
					{
						if (num != 2672027822U)
						{
							goto IL_4A8;
						}
						if (!(text == "tripledeskeywrap"))
						{
							goto IL_4A8;
						}
						return "1.2.840.113549.1.9.16.3.6";
					}
					else
					{
						if (!(text == "sha256"))
						{
							goto IL_4A8;
						}
						goto IL_478;
					}
				}
				else if (num != 2694049387U)
				{
					if (num != 2700614742U)
					{
						goto IL_4A8;
					}
					if (!(text == "sha384"))
					{
						goto IL_4A8;
					}
					goto IL_47E;
				}
				else
				{
					if (!(text == "sha512"))
					{
						goto IL_4A8;
					}
					goto IL_484;
				}
			}
			else if (num <= 3529085699U)
			{
				if (num <= 2930817943U)
				{
					if (num != 2803157229U)
					{
						if (num != 2930817943U)
						{
							goto IL_4A8;
						}
						if (!(text == "system.security.cryptography.sha1"))
						{
							goto IL_4A8;
						}
					}
					else
					{
						if (!(text == "system.security.cryptography.sha256cng"))
						{
							goto IL_4A8;
						}
						goto IL_478;
					}
				}
				else if (num != 3457114317U)
				{
					if (num != 3529085699U)
					{
						goto IL_4A8;
					}
					if (!(text == "des"))
					{
						goto IL_4A8;
					}
					return "1.3.14.3.2.7";
				}
				else
				{
					if (!(text == "system.security.cryptography.sha512managed"))
					{
						goto IL_4A8;
					}
					goto IL_484;
				}
			}
			else if (num <= 3991591796U)
			{
				if (num != 3708241362U)
				{
					if (num != 3991591796U)
					{
						goto IL_4A8;
					}
					if (!(text == "system.security.cryptography.sha512"))
					{
						goto IL_4A8;
					}
					goto IL_484;
				}
				else
				{
					if (!(text == "system.security.cryptography.sha512cng"))
					{
						goto IL_4A8;
					}
					goto IL_484;
				}
			}
			else if (num != 4181060418U)
			{
				if (num != 4199782769U)
				{
					goto IL_4A8;
				}
				if (!(text == "system.security.cryptography.sha384"))
				{
					goto IL_4A8;
				}
				goto IL_47E;
			}
			else
			{
				if (!(text == "system.security.cryptography.sha384managed"))
				{
					goto IL_4A8;
				}
				goto IL_47E;
			}
			IL_46C:
			return "1.3.14.3.2.26";
			IL_478:
			return "2.16.840.1.101.3.4.2.1";
			IL_47E:
			return "2.16.840.1.101.3.4.2.2";
			IL_484:
			return "2.16.840.1.101.3.4.2.3";
			IL_48A:
			return "1.3.36.3.2.1";
			IL_4A8:
			return null;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CryptoConfig" /> class. </summary>
		// Token: 0x06004666 RID: 18022 RVA: 0x00002050 File Offset: 0x00000250
		public CryptoConfig()
		{
		}
	}
}
