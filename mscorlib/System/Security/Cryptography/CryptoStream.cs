﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace System.Security.Cryptography
{
	/// <summary>Defines a stream that links data streams to cryptographic transformations.</summary>
	// Token: 0x0200066B RID: 1643
	[ComVisible(true)]
	public class CryptoStream : Stream, IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CryptoStream" /> class with a target data stream, the transformation to use, and the mode of the stream.</summary>
		/// <param name="stream">The stream on which to perform the cryptographic transformation. </param>
		/// <param name="transform">The cryptographic transformation that is to be performed on the stream. </param>
		/// <param name="mode">One of the <see cref="T:System.Security.Cryptography.CryptoStreamMode" /> values. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> is not readable.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> is not writable.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="stream" /> is invalid.</exception>
		// Token: 0x06004421 RID: 17441 RVA: 0x000E6908 File Offset: 0x000E4B08
		public CryptoStream(Stream stream, ICryptoTransform transform, CryptoStreamMode mode)
		{
			this._stream = stream;
			this._transformMode = mode;
			this._Transform = transform;
			CryptoStreamMode transformMode = this._transformMode;
			if (transformMode != CryptoStreamMode.Read)
			{
				if (transformMode != CryptoStreamMode.Write)
				{
					throw new ArgumentException(Environment.GetResourceString("Value was invalid."));
				}
				if (!this._stream.CanWrite)
				{
					throw new ArgumentException(Environment.GetResourceString("Stream was not writable."), "stream");
				}
				this._canWrite = true;
			}
			else
			{
				if (!this._stream.CanRead)
				{
					throw new ArgumentException(Environment.GetResourceString("Stream was not readable."), "stream");
				}
				this._canRead = true;
			}
			this.InitializeBuffer();
		}

		/// <summary>Gets a value indicating whether the current <see cref="T:System.Security.Cryptography.CryptoStream" /> is readable.</summary>
		/// <returns>
		///     <see langword="true" /> if the current stream is readable; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000B7C RID: 2940
		// (get) Token: 0x06004422 RID: 17442 RVA: 0x000E69AC File Offset: 0x000E4BAC
		public override bool CanRead
		{
			get
			{
				return this._canRead;
			}
		}

		/// <summary>Gets a value indicating whether you can seek within the current <see cref="T:System.Security.Cryptography.CryptoStream" />.</summary>
		/// <returns>Always <see langword="false" />.</returns>
		// Token: 0x17000B7D RID: 2941
		// (get) Token: 0x06004423 RID: 17443 RVA: 0x00002526 File Offset: 0x00000726
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value indicating whether the current <see cref="T:System.Security.Cryptography.CryptoStream" /> is writable.</summary>
		/// <returns>
		///     <see langword="true" /> if the current stream is writable; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000B7E RID: 2942
		// (get) Token: 0x06004424 RID: 17444 RVA: 0x000E69B4 File Offset: 0x000E4BB4
		public override bool CanWrite
		{
			get
			{
				return this._canWrite;
			}
		}

		/// <summary>Gets the length in bytes of the stream.</summary>
		/// <returns>This property is not supported.</returns>
		/// <exception cref="T:System.NotSupportedException">This property is not supported. </exception>
		// Token: 0x17000B7F RID: 2943
		// (get) Token: 0x06004425 RID: 17445 RVA: 0x00084E84 File Offset: 0x00083084
		public override long Length
		{
			get
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
			}
		}

		/// <summary>Gets or sets the position within the current stream.</summary>
		/// <returns>This property is not supported.</returns>
		/// <exception cref="T:System.NotSupportedException">This property is not supported. </exception>
		// Token: 0x17000B80 RID: 2944
		// (get) Token: 0x06004426 RID: 17446 RVA: 0x00084E84 File Offset: 0x00083084
		// (set) Token: 0x06004427 RID: 17447 RVA: 0x00084E84 File Offset: 0x00083084
		public override long Position
		{
			get
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
			}
			set
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
			}
		}

		/// <summary>Gets a value indicating whether the final buffer block has been written to the underlying stream. </summary>
		/// <returns>
		///     <see langword="true" /> if the final block has been flushed; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000B81 RID: 2945
		// (get) Token: 0x06004428 RID: 17448 RVA: 0x000E69BC File Offset: 0x000E4BBC
		public bool HasFlushedFinalBlock
		{
			get
			{
				return this._finalBlockTransformed;
			}
		}

		/// <summary>Updates the underlying data source or repository with the current state of the buffer, then clears the buffer.</summary>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key is corrupt which can cause invalid padding to the stream. </exception>
		/// <exception cref="T:System.NotSupportedException">The current stream is not writable.-or- The final block has already been transformed. </exception>
		// Token: 0x06004429 RID: 17449 RVA: 0x000E69C4 File Offset: 0x000E4BC4
		public void FlushFinalBlock()
		{
			if (this._finalBlockTransformed)
			{
				throw new NotSupportedException(Environment.GetResourceString("FlushFinalBlock() method was called twice on a CryptoStream. It can only be called once."));
			}
			byte[] array = this._Transform.TransformFinalBlock(this._InputBuffer, 0, this._InputBufferIndex);
			this._finalBlockTransformed = true;
			if (this._canWrite && this._OutputBufferIndex > 0)
			{
				this._stream.Write(this._OutputBuffer, 0, this._OutputBufferIndex);
				this._OutputBufferIndex = 0;
			}
			if (this._canWrite)
			{
				this._stream.Write(array, 0, array.Length);
			}
			CryptoStream cryptoStream = this._stream as CryptoStream;
			if (cryptoStream != null)
			{
				if (!cryptoStream.HasFlushedFinalBlock)
				{
					cryptoStream.FlushFinalBlock();
				}
			}
			else
			{
				this._stream.Flush();
			}
			if (this._InputBuffer != null)
			{
				Array.Clear(this._InputBuffer, 0, this._InputBuffer.Length);
			}
			if (this._OutputBuffer != null)
			{
				Array.Clear(this._OutputBuffer, 0, this._OutputBuffer.Length);
			}
		}

		/// <summary>Clears all buffers for the current stream and causes any buffered data to be written to the underlying device.</summary>
		// Token: 0x0600442A RID: 17450 RVA: 0x000020D3 File Offset: 0x000002D3
		public override void Flush()
		{
		}

		/// <summary>Clears all buffers for the current stream asynchronously, causes any buffered data to be written to the underlying device, and monitors cancellation requests.</summary>
		/// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is <see cref="P:System.Threading.CancellationToken.None" />.</param>
		/// <returns>A task that represents the asynchronous flush operation.</returns>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		// Token: 0x0600442B RID: 17451 RVA: 0x000E6AB3 File Offset: 0x000E4CB3
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			if (base.GetType() != typeof(CryptoStream))
			{
				return base.FlushAsync(cancellationToken);
			}
			if (!cancellationToken.IsCancellationRequested)
			{
				return Task.CompletedTask;
			}
			return Task.FromCancellation(cancellationToken);
		}

		/// <summary>Sets the position within the current stream.</summary>
		/// <param name="offset">A byte offset relative to the <paramref name="origin" /> parameter. </param>
		/// <param name="origin">A <see cref="T:System.IO.SeekOrigin" /> object indicating the reference point used to obtain the new position. </param>
		/// <returns>This method is not supported.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		// Token: 0x0600442C RID: 17452 RVA: 0x00084E84 File Offset: 0x00083084
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
		}

		/// <summary>Sets the length of the current stream.</summary>
		/// <param name="value">The desired length of the current stream in bytes. </param>
		/// <exception cref="T:System.NotSupportedException">This property exists only to support inheritance from <see cref="T:System.IO.Stream" />, and cannot be used.</exception>
		// Token: 0x0600442D RID: 17453 RVA: 0x00084E84 File Offset: 0x00083084
		public override void SetLength(long value)
		{
			throw new NotSupportedException(Environment.GetResourceString("Stream does not support seeking."));
		}

		/// <summary>Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.</summary>
		/// <param name="buffer">An array of bytes. A maximum of <paramref name="count" /> bytes are read from the current stream and stored in <paramref name="buffer" />. </param>
		/// <param name="offset">The byte offset in <paramref name="buffer" /> at which to begin storing the data read from the current stream. </param>
		/// <param name="count">The maximum number of bytes to be read from the current stream. </param>
		/// <returns>The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero if the end of the stream has been reached.</returns>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Security.Cryptography.CryptoStreamMode" /> associated with current <see cref="T:System.Security.Cryptography.CryptoStream" /> object does not match the underlying stream.  For example, this exception is thrown when using <see cref="F:System.Security.Cryptography.CryptoStreamMode.Read" /> with an underlying stream that is write only.  </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset" /> parameter is less than zero.-or- The <paramref name="count" /> parameter is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">Thesum of the <paramref name="count" /> and <paramref name="offset" /> parameters is longer than the length of the buffer. </exception>
		// Token: 0x0600442E RID: 17454 RVA: 0x000E6AEC File Offset: 0x000E4CEC
		public override int Read([In] [Out] byte[] buffer, int offset, int count)
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support reading."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			int i = count;
			int num = offset;
			if (this._OutputBufferIndex != 0)
			{
				if (this._OutputBufferIndex > count)
				{
					Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, offset, count);
					Buffer.InternalBlockCopy(this._OutputBuffer, count, this._OutputBuffer, 0, this._OutputBufferIndex - count);
					this._OutputBufferIndex -= count;
					return count;
				}
				Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, offset, this._OutputBufferIndex);
				i -= this._OutputBufferIndex;
				num += this._OutputBufferIndex;
				this._OutputBufferIndex = 0;
			}
			if (this._finalBlockTransformed)
			{
				return count - i;
			}
			if (i > this._OutputBlockSize && this._Transform.CanTransformMultipleBlocks)
			{
				int num2 = i / this._OutputBlockSize * this._InputBlockSize;
				byte[] array = new byte[num2];
				Buffer.InternalBlockCopy(this._InputBuffer, 0, array, 0, this._InputBufferIndex);
				int num3 = this._InputBufferIndex;
				num3 += this._stream.Read(array, this._InputBufferIndex, num2 - this._InputBufferIndex);
				this._InputBufferIndex = 0;
				if (num3 <= this._InputBlockSize)
				{
					this._InputBuffer = array;
					this._InputBufferIndex = num3;
				}
				else
				{
					int num4 = num3 / this._InputBlockSize * this._InputBlockSize;
					int num5 = num3 - num4;
					if (num5 != 0)
					{
						this._InputBufferIndex = num5;
						Buffer.InternalBlockCopy(array, num4, this._InputBuffer, 0, num5);
					}
					byte[] array2 = new byte[num4 / this._InputBlockSize * this._OutputBlockSize];
					int num6 = this._Transform.TransformBlock(array, 0, num4, array2, 0);
					Buffer.InternalBlockCopy(array2, 0, buffer, num, num6);
					Array.Clear(array, 0, array.Length);
					Array.Clear(array2, 0, array2.Length);
					i -= num6;
					num += num6;
				}
			}
			while (i > 0)
			{
				while (this._InputBufferIndex < this._InputBlockSize)
				{
					int num3 = this._stream.Read(this._InputBuffer, this._InputBufferIndex, this._InputBlockSize - this._InputBufferIndex);
					if (num3 != 0)
					{
						this._InputBufferIndex += num3;
					}
					else
					{
						byte[] array3 = this._Transform.TransformFinalBlock(this._InputBuffer, 0, this._InputBufferIndex);
						this._OutputBuffer = array3;
						this._OutputBufferIndex = array3.Length;
						this._finalBlockTransformed = true;
						if (i < this._OutputBufferIndex)
						{
							Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, num, i);
							this._OutputBufferIndex -= i;
							Buffer.InternalBlockCopy(this._OutputBuffer, i, this._OutputBuffer, 0, this._OutputBufferIndex);
							return count;
						}
						Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, num, this._OutputBufferIndex);
						i -= this._OutputBufferIndex;
						this._OutputBufferIndex = 0;
						return count - i;
					}
				}
				int num6 = this._Transform.TransformBlock(this._InputBuffer, 0, this._InputBlockSize, this._OutputBuffer, 0);
				this._InputBufferIndex = 0;
				if (i < num6)
				{
					Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, num, i);
					this._OutputBufferIndex = num6 - i;
					Buffer.InternalBlockCopy(this._OutputBuffer, i, this._OutputBuffer, 0, this._OutputBufferIndex);
					return count;
				}
				Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, num, num6);
				num += num6;
				i -= num6;
			}
			return count;
		}

		/// <summary>Reads a sequence of bytes from the current stream asynchronously, advances the position within the stream by the number of bytes read, and monitors cancellation requests.</summary>
		/// <param name="buffer">The buffer to write the data into.</param>
		/// <param name="offset">The byte offset in <paramref name="buffer" /> at which to begin writing data from the stream.</param>
		/// <param name="count">The maximum number of bytes to read.</param>
		/// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is <see cref="P:System.Threading.CancellationToken.None" />.</param>
		/// <returns>A task that represents the asynchronous read operation. The value of the task object's <paramref name="TResult" /> parameter contains the total number of bytes read into the buffer. The result can be less than the number of bytes requested if the number of bytes currently available is less than the requested number, or it can be 0 (zero) if the end of the stream has been reached. </returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is negative.</exception>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset" /> and <paramref name="count" /> is larger than the buffer length.</exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support reading.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is currently in use by a previous read operation. </exception>
		// Token: 0x0600442F RID: 17455 RVA: 0x000E6E7C File Offset: 0x000E507C
		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support reading."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (base.GetType() != typeof(CryptoStream))
			{
				return base.ReadAsync(buffer, offset, count, cancellationToken);
			}
			if (cancellationToken.IsCancellationRequested)
			{
				return Task.FromCancellation<int>(cancellationToken);
			}
			return this.ReadAsyncInternal(buffer, offset, count, cancellationToken);
		}

		// Token: 0x06004430 RID: 17456 RVA: 0x000E6F2C File Offset: 0x000E512C
		private async Task<int> ReadAsyncInternal(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			await default(CryptoStream.HopToThreadPoolAwaitable);
			SemaphoreSlim sem = base.EnsureAsyncActiveSemaphoreInitialized();
			await sem.WaitAsync().ConfigureAwait(false);
			int result;
			try
			{
				int bytesToDeliver = count;
				int currentOutputIndex = offset;
				if (this._OutputBufferIndex != 0)
				{
					if (this._OutputBufferIndex > count)
					{
						Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, offset, count);
						Buffer.InternalBlockCopy(this._OutputBuffer, count, this._OutputBuffer, 0, this._OutputBufferIndex - count);
						this._OutputBufferIndex -= count;
						return count;
					}
					Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, offset, this._OutputBufferIndex);
					bytesToDeliver -= this._OutputBufferIndex;
					currentOutputIndex += this._OutputBufferIndex;
					this._OutputBufferIndex = 0;
				}
				if (this._finalBlockTransformed)
				{
					result = count - bytesToDeliver;
				}
				else
				{
					if (bytesToDeliver > this._OutputBlockSize && this._Transform.CanTransformMultipleBlocks)
					{
						int num = bytesToDeliver / this._OutputBlockSize * this._InputBlockSize;
						byte[] tempInputBuffer = new byte[num];
						Buffer.InternalBlockCopy(this._InputBuffer, 0, tempInputBuffer, 0, this._InputBufferIndex);
						int inputBufferIndex = this._InputBufferIndex;
						int num2 = inputBufferIndex + await this._stream.ReadAsync(tempInputBuffer, this._InputBufferIndex, num - this._InputBufferIndex, cancellationToken).ConfigureAwait(false);
						this._InputBufferIndex = 0;
						if (num2 <= this._InputBlockSize)
						{
							this._InputBuffer = tempInputBuffer;
							this._InputBufferIndex = num2;
						}
						else
						{
							int num3 = num2 / this._InputBlockSize * this._InputBlockSize;
							int num4 = num2 - num3;
							if (num4 != 0)
							{
								this._InputBufferIndex = num4;
								Buffer.InternalBlockCopy(tempInputBuffer, num3, this._InputBuffer, 0, num4);
							}
							byte[] array = new byte[num3 / this._InputBlockSize * this._OutputBlockSize];
							int num5 = this._Transform.TransformBlock(tempInputBuffer, 0, num3, array, 0);
							Buffer.InternalBlockCopy(array, 0, buffer, currentOutputIndex, num5);
							Array.Clear(tempInputBuffer, 0, tempInputBuffer.Length);
							Array.Clear(array, 0, array.Length);
							bytesToDeliver -= num5;
							currentOutputIndex += num5;
							tempInputBuffer = null;
						}
					}
					while (bytesToDeliver > 0)
					{
						while (this._InputBufferIndex < this._InputBlockSize)
						{
							int num2 = await this._stream.ReadAsync(this._InputBuffer, this._InputBufferIndex, this._InputBlockSize - this._InputBufferIndex, cancellationToken).ConfigureAwait(false);
							if (num2 != 0)
							{
								this._InputBufferIndex += num2;
							}
							else
							{
								byte[] array2 = this._Transform.TransformFinalBlock(this._InputBuffer, 0, this._InputBufferIndex);
								this._OutputBuffer = array2;
								this._OutputBufferIndex = array2.Length;
								this._finalBlockTransformed = true;
								if (bytesToDeliver < this._OutputBufferIndex)
								{
									Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, currentOutputIndex, bytesToDeliver);
									this._OutputBufferIndex -= bytesToDeliver;
									Buffer.InternalBlockCopy(this._OutputBuffer, bytesToDeliver, this._OutputBuffer, 0, this._OutputBufferIndex);
									return count;
								}
								Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, currentOutputIndex, this._OutputBufferIndex);
								bytesToDeliver -= this._OutputBufferIndex;
								this._OutputBufferIndex = 0;
								return count - bytesToDeliver;
							}
						}
						int num5 = this._Transform.TransformBlock(this._InputBuffer, 0, this._InputBlockSize, this._OutputBuffer, 0);
						this._InputBufferIndex = 0;
						if (bytesToDeliver < num5)
						{
							Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, currentOutputIndex, bytesToDeliver);
							this._OutputBufferIndex = num5 - bytesToDeliver;
							Buffer.InternalBlockCopy(this._OutputBuffer, bytesToDeliver, this._OutputBuffer, 0, this._OutputBufferIndex);
							return count;
						}
						Buffer.InternalBlockCopy(this._OutputBuffer, 0, buffer, currentOutputIndex, num5);
						currentOutputIndex += num5;
						bytesToDeliver -= num5;
					}
					result = count;
				}
			}
			finally
			{
				sem.Release();
			}
			return result;
		}

		/// <summary>Writes a sequence of bytes to the current <see cref="T:System.Security.Cryptography.CryptoStream" /> and advances the current position within the stream by the number of bytes written.</summary>
		/// <param name="buffer">An array of bytes. This method copies <paramref name="count" /> bytes from <paramref name="buffer" /> to the current stream. </param>
		/// <param name="offset">The byte offset in <paramref name="buffer" /> at which to begin copying bytes to the current stream. </param>
		/// <param name="count">The number of bytes to be written to the current stream. </param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Security.Cryptography.CryptoStreamMode" /> associated with current <see cref="T:System.Security.Cryptography.CryptoStream" /> object does not match the underlying stream.  For example, this exception is thrown when using <see cref="F:System.Security.Cryptography.CryptoStreamMode.Write" />  with an underlying stream that is read only.  </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset" /> parameter is less than zero.-or- The <paramref name="count" /> parameter is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">The sum of the <paramref name="count" /> and <paramref name="offset" /> parameters is longer than the length of the buffer. </exception>
		// Token: 0x06004431 RID: 17457 RVA: 0x000E6F94 File Offset: 0x000E5194
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (!this.CanWrite)
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support writing."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			int i = count;
			int num = offset;
			if (this._InputBufferIndex > 0)
			{
				if (count < this._InputBlockSize - this._InputBufferIndex)
				{
					Buffer.InternalBlockCopy(buffer, offset, this._InputBuffer, this._InputBufferIndex, count);
					this._InputBufferIndex += count;
					return;
				}
				Buffer.InternalBlockCopy(buffer, offset, this._InputBuffer, this._InputBufferIndex, this._InputBlockSize - this._InputBufferIndex);
				num += this._InputBlockSize - this._InputBufferIndex;
				i -= this._InputBlockSize - this._InputBufferIndex;
				this._InputBufferIndex = this._InputBlockSize;
			}
			if (this._OutputBufferIndex > 0)
			{
				this._stream.Write(this._OutputBuffer, 0, this._OutputBufferIndex);
				this._OutputBufferIndex = 0;
			}
			if (this._InputBufferIndex == this._InputBlockSize)
			{
				int count2 = this._Transform.TransformBlock(this._InputBuffer, 0, this._InputBlockSize, this._OutputBuffer, 0);
				this._stream.Write(this._OutputBuffer, 0, count2);
				this._InputBufferIndex = 0;
			}
			while (i > 0)
			{
				if (i < this._InputBlockSize)
				{
					Buffer.InternalBlockCopy(buffer, num, this._InputBuffer, 0, i);
					this._InputBufferIndex += i;
					return;
				}
				if (this._Transform.CanTransformMultipleBlocks)
				{
					int num2 = i / this._InputBlockSize;
					int num3 = num2 * this._InputBlockSize;
					byte[] array = new byte[num2 * this._OutputBlockSize];
					int count2 = this._Transform.TransformBlock(buffer, num, num3, array, 0);
					this._stream.Write(array, 0, count2);
					num += num3;
					i -= num3;
				}
				else
				{
					int count2 = this._Transform.TransformBlock(buffer, num, this._InputBlockSize, this._OutputBuffer, 0);
					this._stream.Write(this._OutputBuffer, 0, count2);
					num += this._InputBlockSize;
					i -= this._InputBlockSize;
				}
			}
		}

		/// <summary>Writes a sequence of bytes to the current stream asynchronously, advances the current position within the stream by the number of bytes written, and monitors cancellation requests.</summary>
		/// <param name="buffer">The buffer to write data from.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> from which to begin writing bytes to the stream.</param>
		/// <param name="count">The maximum number of bytes to write.</param>
		/// <param name="cancellationToken">The token to monitor for cancellation requests. The default value is <see cref="P:System.Threading.CancellationToken.None" />.</param>
		/// <returns>A task that represents the asynchronous write operation.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="buffer" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is negative.</exception>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset" /> and <paramref name="count" /> is larger than the buffer length.</exception>
		/// <exception cref="T:System.NotSupportedException">The stream does not support writing.</exception>
		/// <exception cref="T:System.ObjectDisposedException">The stream has been disposed.</exception>
		/// <exception cref="T:System.InvalidOperationException">The stream is currently in use by a previous write operation. </exception>
		// Token: 0x06004432 RID: 17458 RVA: 0x000E71DC File Offset: 0x000E53DC
		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (!this.CanWrite)
			{
				throw new NotSupportedException(Environment.GetResourceString("Stream does not support writing."));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			if (base.GetType() != typeof(CryptoStream))
			{
				return base.WriteAsync(buffer, offset, count, cancellationToken);
			}
			if (cancellationToken.IsCancellationRequested)
			{
				return Task.FromCancellation(cancellationToken);
			}
			return this.WriteAsyncInternal(buffer, offset, count, cancellationToken);
		}

		// Token: 0x06004433 RID: 17459 RVA: 0x000E728C File Offset: 0x000E548C
		private async Task WriteAsyncInternal(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			await default(CryptoStream.HopToThreadPoolAwaitable);
			SemaphoreSlim sem = base.EnsureAsyncActiveSemaphoreInitialized();
			await sem.WaitAsync().ConfigureAwait(false);
			try
			{
				int bytesToWrite = count;
				int currentInputIndex = offset;
				if (this._InputBufferIndex > 0)
				{
					if (count < this._InputBlockSize - this._InputBufferIndex)
					{
						Buffer.InternalBlockCopy(buffer, offset, this._InputBuffer, this._InputBufferIndex, count);
						this._InputBufferIndex += count;
						return;
					}
					Buffer.InternalBlockCopy(buffer, offset, this._InputBuffer, this._InputBufferIndex, this._InputBlockSize - this._InputBufferIndex);
					currentInputIndex += this._InputBlockSize - this._InputBufferIndex;
					bytesToWrite -= this._InputBlockSize - this._InputBufferIndex;
					this._InputBufferIndex = this._InputBlockSize;
				}
				if (this._OutputBufferIndex > 0)
				{
					await this._stream.WriteAsync(this._OutputBuffer, 0, this._OutputBufferIndex, cancellationToken).ConfigureAwait(false);
					this._OutputBufferIndex = 0;
				}
				if (this._InputBufferIndex == this._InputBlockSize)
				{
					int count2 = this._Transform.TransformBlock(this._InputBuffer, 0, this._InputBlockSize, this._OutputBuffer, 0);
					await this._stream.WriteAsync(this._OutputBuffer, 0, count2, cancellationToken).ConfigureAwait(false);
					this._InputBufferIndex = 0;
				}
				while (bytesToWrite > 0)
				{
					if (bytesToWrite < this._InputBlockSize)
					{
						Buffer.InternalBlockCopy(buffer, currentInputIndex, this._InputBuffer, 0, bytesToWrite);
						this._InputBufferIndex += bytesToWrite;
						break;
					}
					if (this._Transform.CanTransformMultipleBlocks)
					{
						int num = bytesToWrite / this._InputBlockSize;
						int numWholeBlocksInBytes = num * this._InputBlockSize;
						byte[] array = new byte[num * this._OutputBlockSize];
						int count2 = this._Transform.TransformBlock(buffer, currentInputIndex, numWholeBlocksInBytes, array, 0);
						await this._stream.WriteAsync(array, 0, count2, cancellationToken).ConfigureAwait(false);
						currentInputIndex += numWholeBlocksInBytes;
						bytesToWrite -= numWholeBlocksInBytes;
					}
					else
					{
						int count2 = this._Transform.TransformBlock(buffer, currentInputIndex, this._InputBlockSize, this._OutputBuffer, 0);
						await this._stream.WriteAsync(this._OutputBuffer, 0, count2, cancellationToken).ConfigureAwait(false);
						currentInputIndex += this._InputBlockSize;
						bytesToWrite -= this._InputBlockSize;
					}
				}
			}
			finally
			{
				sem.Release();
			}
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Security.Cryptography.CryptoStream" />.</summary>
		// Token: 0x06004434 RID: 17460 RVA: 0x0008B7ED File Offset: 0x000899ED
		public void Clear()
		{
			this.Close();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Security.Cryptography.CryptoStream" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x06004435 RID: 17461 RVA: 0x000E72F4 File Offset: 0x000E54F4
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					if (!this._finalBlockTransformed)
					{
						this.FlushFinalBlock();
					}
					this._stream.Close();
				}
			}
			finally
			{
				try
				{
					this._finalBlockTransformed = true;
					if (this._InputBuffer != null)
					{
						Array.Clear(this._InputBuffer, 0, this._InputBuffer.Length);
					}
					if (this._OutputBuffer != null)
					{
						Array.Clear(this._OutputBuffer, 0, this._OutputBuffer.Length);
					}
					this._InputBuffer = null;
					this._OutputBuffer = null;
					this._canRead = false;
					this._canWrite = false;
				}
				finally
				{
					base.Dispose(disposing);
				}
			}
		}

		// Token: 0x06004436 RID: 17462 RVA: 0x000E73A4 File Offset: 0x000E55A4
		private void InitializeBuffer()
		{
			if (this._Transform != null)
			{
				this._InputBlockSize = this._Transform.InputBlockSize;
				this._InputBuffer = new byte[this._InputBlockSize];
				this._OutputBlockSize = this._Transform.OutputBlockSize;
				this._OutputBuffer = new byte[this._OutputBlockSize];
			}
		}

		// Token: 0x04002329 RID: 9001
		private Stream _stream;

		// Token: 0x0400232A RID: 9002
		private ICryptoTransform _Transform;

		// Token: 0x0400232B RID: 9003
		private byte[] _InputBuffer;

		// Token: 0x0400232C RID: 9004
		private int _InputBufferIndex;

		// Token: 0x0400232D RID: 9005
		private int _InputBlockSize;

		// Token: 0x0400232E RID: 9006
		private byte[] _OutputBuffer;

		// Token: 0x0400232F RID: 9007
		private int _OutputBufferIndex;

		// Token: 0x04002330 RID: 9008
		private int _OutputBlockSize;

		// Token: 0x04002331 RID: 9009
		private CryptoStreamMode _transformMode;

		// Token: 0x04002332 RID: 9010
		private bool _canRead;

		// Token: 0x04002333 RID: 9011
		private bool _canWrite;

		// Token: 0x04002334 RID: 9012
		private bool _finalBlockTransformed;

		// Token: 0x0200066C RID: 1644
		private struct HopToThreadPoolAwaitable : INotifyCompletion
		{
			// Token: 0x06004437 RID: 17463 RVA: 0x000E73FD File Offset: 0x000E55FD
			public CryptoStream.HopToThreadPoolAwaitable GetAwaiter()
			{
				return this;
			}

			// Token: 0x17000B82 RID: 2946
			// (get) Token: 0x06004438 RID: 17464 RVA: 0x00002526 File Offset: 0x00000726
			public bool IsCompleted
			{
				get
				{
					return false;
				}
			}

			// Token: 0x06004439 RID: 17465 RVA: 0x000E7405 File Offset: 0x000E5605
			public void OnCompleted(Action continuation)
			{
				Task.Run(continuation);
			}

			// Token: 0x0600443A RID: 17466 RVA: 0x000020D3 File Offset: 0x000002D3
			public void GetResult()
			{
			}
		}

		// Token: 0x0200066D RID: 1645
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <ReadAsyncInternal>d__34 : IAsyncStateMachine
		{
			// Token: 0x0600443B RID: 17467 RVA: 0x000E7410 File Offset: 0x000E5610
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				CryptoStream cryptoStream = this;
				int result;
				try
				{
					CryptoStream.HopToThreadPoolAwaitable hopToThreadPoolAwaitable;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						CryptoStream.HopToThreadPoolAwaitable hopToThreadPoolAwaitable2;
						hopToThreadPoolAwaitable = hopToThreadPoolAwaitable2;
						hopToThreadPoolAwaitable2 = default(CryptoStream.HopToThreadPoolAwaitable);
						num = (num2 = -1);
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						goto IL_F4;
					}
					case 2:
					case 3:
						IL_FB:
						try
						{
							ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter3;
							int num4;
							if (num != 2)
							{
								if (num == 3)
								{
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
									configuredTaskAwaiter3 = configuredTaskAwaiter4;
									configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
									num = (num2 = -1);
									goto IL_48A;
								}
								bytesToDeliver = count;
								currentOutputIndex = offset;
								if (cryptoStream._OutputBufferIndex != 0)
								{
									if (cryptoStream._OutputBufferIndex > count)
									{
										Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, 0, buffer, offset, count);
										Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, count, cryptoStream._OutputBuffer, 0, cryptoStream._OutputBufferIndex - count);
										cryptoStream._OutputBufferIndex -= count;
										result = count;
										goto IL_6A6;
									}
									Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, 0, buffer, offset, cryptoStream._OutputBufferIndex);
									bytesToDeliver -= cryptoStream._OutputBufferIndex;
									currentOutputIndex += cryptoStream._OutputBufferIndex;
									cryptoStream._OutputBufferIndex = 0;
								}
								if (cryptoStream._finalBlockTransformed)
								{
									result = count - bytesToDeliver;
									goto IL_6A6;
								}
								if (bytesToDeliver <= cryptoStream._OutputBlockSize || !cryptoStream._Transform.CanTransformMultipleBlocks)
								{
									goto IL_580;
								}
								int num3 = bytesToDeliver / cryptoStream._OutputBlockSize * cryptoStream._InputBlockSize;
								tempInputBuffer = new byte[num3];
								Buffer.InternalBlockCopy(cryptoStream._InputBuffer, 0, tempInputBuffer, 0, cryptoStream._InputBufferIndex);
								num4 = cryptoStream._InputBufferIndex;
								inputBufferIndex = num4;
								configuredTaskAwaiter3 = cryptoStream._stream.ReadAsync(tempInputBuffer, cryptoStream._InputBufferIndex, num3 - cryptoStream._InputBufferIndex, cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num = (num2 = 2);
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, CryptoStream.<ReadAsyncInternal>d__34>(ref configuredTaskAwaiter3, ref this);
									return;
								}
							}
							else
							{
								ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4;
								configuredTaskAwaiter3 = configuredTaskAwaiter4;
								configuredTaskAwaiter4 = default(ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter);
								num = (num2 = -1);
							}
							int result2 = configuredTaskAwaiter3.GetResult();
							num4 = inputBufferIndex + result2;
							cryptoStream._InputBufferIndex = 0;
							if (num4 <= cryptoStream._InputBlockSize)
							{
								cryptoStream._InputBuffer = tempInputBuffer;
								cryptoStream._InputBufferIndex = num4;
								goto IL_580;
							}
							int num5 = num4 / cryptoStream._InputBlockSize * cryptoStream._InputBlockSize;
							int num6 = num4 - num5;
							if (num6 != 0)
							{
								cryptoStream._InputBufferIndex = num6;
								Buffer.InternalBlockCopy(tempInputBuffer, num5, cryptoStream._InputBuffer, 0, num6);
							}
							byte[] array = new byte[num5 / cryptoStream._InputBlockSize * cryptoStream._OutputBlockSize];
							int num7 = cryptoStream._Transform.TransformBlock(tempInputBuffer, 0, num5, array, 0);
							Buffer.InternalBlockCopy(array, 0, buffer, currentOutputIndex, num7);
							Array.Clear(tempInputBuffer, 0, tempInputBuffer.Length);
							Array.Clear(array, 0, array.Length);
							bytesToDeliver -= num7;
							currentOutputIndex += num7;
							tempInputBuffer = null;
							goto IL_580;
							IL_48A:
							num4 = configuredTaskAwaiter3.GetResult();
							if (num4 != 0)
							{
								cryptoStream._InputBufferIndex += num4;
							}
							else
							{
								byte[] array2 = cryptoStream._Transform.TransformFinalBlock(cryptoStream._InputBuffer, 0, cryptoStream._InputBufferIndex);
								cryptoStream._OutputBuffer = array2;
								cryptoStream._OutputBufferIndex = array2.Length;
								cryptoStream._finalBlockTransformed = true;
								if (bytesToDeliver < cryptoStream._OutputBufferIndex)
								{
									Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, 0, buffer, currentOutputIndex, bytesToDeliver);
									cryptoStream._OutputBufferIndex -= bytesToDeliver;
									Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, bytesToDeliver, cryptoStream._OutputBuffer, 0, cryptoStream._OutputBufferIndex);
									result = count;
									goto IL_6A6;
								}
								Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, 0, buffer, currentOutputIndex, cryptoStream._OutputBufferIndex);
								bytesToDeliver -= cryptoStream._OutputBufferIndex;
								cryptoStream._OutputBufferIndex = 0;
								result = count - bytesToDeliver;
								goto IL_6A6;
							}
							IL_4A9:
							if (cryptoStream._InputBufferIndex >= cryptoStream._InputBlockSize)
							{
								num7 = cryptoStream._Transform.TransformBlock(cryptoStream._InputBuffer, 0, cryptoStream._InputBlockSize, cryptoStream._OutputBuffer, 0);
								cryptoStream._InputBufferIndex = 0;
								if (bytesToDeliver < num7)
								{
									Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, 0, buffer, currentOutputIndex, bytesToDeliver);
									cryptoStream._OutputBufferIndex = num7 - bytesToDeliver;
									Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, bytesToDeliver, cryptoStream._OutputBuffer, 0, cryptoStream._OutputBufferIndex);
									result = count;
									goto IL_6A6;
								}
								Buffer.InternalBlockCopy(cryptoStream._OutputBuffer, 0, buffer, currentOutputIndex, num7);
								currentOutputIndex += num7;
								bytesToDeliver -= num7;
							}
							else
							{
								configuredTaskAwaiter3 = cryptoStream._stream.ReadAsync(cryptoStream._InputBuffer, cryptoStream._InputBufferIndex, cryptoStream._InputBlockSize - cryptoStream._InputBufferIndex, cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter3.IsCompleted)
								{
									num = (num2 = 3);
									ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter configuredTaskAwaiter4 = configuredTaskAwaiter3;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter, CryptoStream.<ReadAsyncInternal>d__34>(ref configuredTaskAwaiter3, ref this);
									return;
								}
								goto IL_48A;
							}
							IL_580:
							if (bytesToDeliver <= 0)
							{
								result = count;
								goto IL_6A6;
							}
							goto IL_4A9;
						}
						finally
						{
							if (num < 0)
							{
								sem.Release();
							}
						}
						break;
					default:
						hopToThreadPoolAwaitable = default(CryptoStream.HopToThreadPoolAwaitable).GetAwaiter();
						if (!hopToThreadPoolAwaitable.IsCompleted)
						{
							num = (num2 = 0);
							CryptoStream.HopToThreadPoolAwaitable hopToThreadPoolAwaitable2 = hopToThreadPoolAwaitable;
							this.<>t__builder.AwaitOnCompleted<CryptoStream.HopToThreadPoolAwaitable, CryptoStream.<ReadAsyncInternal>d__34>(ref hopToThreadPoolAwaitable, ref this);
							return;
						}
						break;
					}
					hopToThreadPoolAwaitable.GetResult();
					sem = cryptoStream.EnsureAsyncActiveSemaphoreInitialized();
					configuredTaskAwaiter = sem.WaitAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num = (num2 = 1);
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, CryptoStream.<ReadAsyncInternal>d__34>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_F4:
					configuredTaskAwaiter.GetResult();
					goto IL_FB;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_6A6:
				num2 = -2;
				this.<>t__builder.SetResult(result);
			}

			// Token: 0x0600443C RID: 17468 RVA: 0x000E7B0C File Offset: 0x000E5D0C
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04002335 RID: 9013
			public int <>1__state;

			// Token: 0x04002336 RID: 9014
			public AsyncTaskMethodBuilder<int> <>t__builder;

			// Token: 0x04002337 RID: 9015
			public CryptoStream <>4__this;

			// Token: 0x04002338 RID: 9016
			public int count;

			// Token: 0x04002339 RID: 9017
			public int offset;

			// Token: 0x0400233A RID: 9018
			public byte[] buffer;

			// Token: 0x0400233B RID: 9019
			public CancellationToken cancellationToken;

			// Token: 0x0400233C RID: 9020
			private byte[] <tempInputBuffer>5__1;

			// Token: 0x0400233D RID: 9021
			private int <currentOutputIndex>5__2;

			// Token: 0x0400233E RID: 9022
			private int <bytesToDeliver>5__3;

			// Token: 0x0400233F RID: 9023
			private SemaphoreSlim <sem>5__4;

			// Token: 0x04002340 RID: 9024
			private CryptoStream.HopToThreadPoolAwaitable <>u__1;

			// Token: 0x04002341 RID: 9025
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;

			// Token: 0x04002342 RID: 9026
			private int <>7__wrap1;

			// Token: 0x04002343 RID: 9027
			private ConfiguredTaskAwaitable<int>.ConfiguredTaskAwaiter <>u__3;
		}

		// Token: 0x0200066E RID: 1646
		[CompilerGenerated]
		[StructLayout(LayoutKind.Auto)]
		private struct <WriteAsyncInternal>d__37 : IAsyncStateMachine
		{
			// Token: 0x0600443D RID: 17469 RVA: 0x000E7B1C File Offset: 0x000E5D1C
			void IAsyncStateMachine.MoveNext()
			{
				int num2;
				int num = num2;
				CryptoStream cryptoStream = this;
				try
				{
					CryptoStream.HopToThreadPoolAwaitable hopToThreadPoolAwaitable;
					ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter;
					switch (num)
					{
					case 0:
					{
						CryptoStream.HopToThreadPoolAwaitable hopToThreadPoolAwaitable2;
						hopToThreadPoolAwaitable = hopToThreadPoolAwaitable2;
						hopToThreadPoolAwaitable2 = default(CryptoStream.HopToThreadPoolAwaitable);
						num = (num2 = -1);
						break;
					}
					case 1:
					{
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
						configuredTaskAwaiter = configuredTaskAwaiter2;
						configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
						num = (num2 = -1);
						goto IL_FC;
					}
					case 2:
					case 3:
					case 4:
					case 5:
						IL_103:
						try
						{
							switch (num)
							{
							case 2:
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num = (num2 = -1);
								break;
							}
							case 3:
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num = (num2 = -1);
								goto IL_338;
							}
							case 4:
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num = (num2 = -1);
								goto IL_42D;
							}
							case 5:
							{
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2;
								configuredTaskAwaiter = configuredTaskAwaiter2;
								configuredTaskAwaiter2 = default(ConfiguredTaskAwaitable.ConfiguredTaskAwaiter);
								num = (num2 = -1);
								goto IL_4FA;
							}
							default:
								bytesToWrite = count;
								currentInputIndex = offset;
								if (cryptoStream._InputBufferIndex > 0)
								{
									if (count < cryptoStream._InputBlockSize - cryptoStream._InputBufferIndex)
									{
										Buffer.InternalBlockCopy(buffer, offset, cryptoStream._InputBuffer, cryptoStream._InputBufferIndex, count);
										cryptoStream._InputBufferIndex += count;
										goto IL_595;
									}
									Buffer.InternalBlockCopy(buffer, offset, cryptoStream._InputBuffer, cryptoStream._InputBufferIndex, cryptoStream._InputBlockSize - cryptoStream._InputBufferIndex);
									currentInputIndex += cryptoStream._InputBlockSize - cryptoStream._InputBufferIndex;
									bytesToWrite -= cryptoStream._InputBlockSize - cryptoStream._InputBufferIndex;
									cryptoStream._InputBufferIndex = cryptoStream._InputBlockSize;
								}
								if (cryptoStream._OutputBufferIndex <= 0)
								{
									goto IL_291;
								}
								configuredTaskAwaiter = cryptoStream._stream.WriteAsync(cryptoStream._OutputBuffer, 0, cryptoStream._OutputBufferIndex, cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 2);
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, CryptoStream.<WriteAsyncInternal>d__37>(ref configuredTaskAwaiter, ref this);
									return;
								}
								break;
							}
							configuredTaskAwaiter.GetResult();
							cryptoStream._OutputBufferIndex = 0;
							IL_291:
							if (cryptoStream._InputBufferIndex != cryptoStream._InputBlockSize)
							{
								goto IL_55D;
							}
							int num3 = cryptoStream._Transform.TransformBlock(cryptoStream._InputBuffer, 0, cryptoStream._InputBlockSize, cryptoStream._OutputBuffer, 0);
							configuredTaskAwaiter = cryptoStream._stream.WriteAsync(cryptoStream._OutputBuffer, 0, num3, cancellationToken).ConfigureAwait(false).GetAwaiter();
							if (!configuredTaskAwaiter.IsCompleted)
							{
								num = (num2 = 3);
								ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
								this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, CryptoStream.<WriteAsyncInternal>d__37>(ref configuredTaskAwaiter, ref this);
								return;
							}
							IL_338:
							configuredTaskAwaiter.GetResult();
							cryptoStream._InputBufferIndex = 0;
							goto IL_55D;
							IL_42D:
							configuredTaskAwaiter.GetResult();
							currentInputIndex += numWholeBlocksInBytes;
							bytesToWrite -= numWholeBlocksInBytes;
							goto IL_55D;
							IL_4FA:
							configuredTaskAwaiter.GetResult();
							currentInputIndex += cryptoStream._InputBlockSize;
							bytesToWrite -= cryptoStream._InputBlockSize;
							IL_55D:
							if (bytesToWrite <= 0)
							{
								goto IL_595;
							}
							if (bytesToWrite < cryptoStream._InputBlockSize)
							{
								Buffer.InternalBlockCopy(buffer, currentInputIndex, cryptoStream._InputBuffer, 0, bytesToWrite);
								cryptoStream._InputBufferIndex += bytesToWrite;
								goto IL_595;
							}
							if (cryptoStream._Transform.CanTransformMultipleBlocks)
							{
								int num4 = bytesToWrite / cryptoStream._InputBlockSize;
								numWholeBlocksInBytes = num4 * cryptoStream._InputBlockSize;
								byte[] outputBuffer = new byte[num4 * cryptoStream._OutputBlockSize];
								num3 = cryptoStream._Transform.TransformBlock(buffer, currentInputIndex, numWholeBlocksInBytes, outputBuffer, 0);
								configuredTaskAwaiter = cryptoStream._stream.WriteAsync(outputBuffer, 0, num3, cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 4);
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, CryptoStream.<WriteAsyncInternal>d__37>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_42D;
							}
							else
							{
								num3 = cryptoStream._Transform.TransformBlock(buffer, currentInputIndex, cryptoStream._InputBlockSize, cryptoStream._OutputBuffer, 0);
								configuredTaskAwaiter = cryptoStream._stream.WriteAsync(cryptoStream._OutputBuffer, 0, num3, cancellationToken).ConfigureAwait(false).GetAwaiter();
								if (!configuredTaskAwaiter.IsCompleted)
								{
									num = (num2 = 5);
									ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
									this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, CryptoStream.<WriteAsyncInternal>d__37>(ref configuredTaskAwaiter, ref this);
									return;
								}
								goto IL_4FA;
							}
						}
						finally
						{
							if (num < 0)
							{
								sem.Release();
							}
						}
						break;
					default:
						hopToThreadPoolAwaitable = default(CryptoStream.HopToThreadPoolAwaitable).GetAwaiter();
						if (!hopToThreadPoolAwaitable.IsCompleted)
						{
							num = (num2 = 0);
							CryptoStream.HopToThreadPoolAwaitable hopToThreadPoolAwaitable2 = hopToThreadPoolAwaitable;
							this.<>t__builder.AwaitOnCompleted<CryptoStream.HopToThreadPoolAwaitable, CryptoStream.<WriteAsyncInternal>d__37>(ref hopToThreadPoolAwaitable, ref this);
							return;
						}
						break;
					}
					hopToThreadPoolAwaitable.GetResult();
					sem = cryptoStream.EnsureAsyncActiveSemaphoreInitialized();
					configuredTaskAwaiter = sem.WaitAsync().ConfigureAwait(false).GetAwaiter();
					if (!configuredTaskAwaiter.IsCompleted)
					{
						num = (num2 = 1);
						ConfiguredTaskAwaitable.ConfiguredTaskAwaiter configuredTaskAwaiter2 = configuredTaskAwaiter;
						this.<>t__builder.AwaitUnsafeOnCompleted<ConfiguredTaskAwaitable.ConfiguredTaskAwaiter, CryptoStream.<WriteAsyncInternal>d__37>(ref configuredTaskAwaiter, ref this);
						return;
					}
					IL_FC:
					configuredTaskAwaiter.GetResult();
					goto IL_103;
				}
				catch (Exception exception)
				{
					num2 = -2;
					this.<>t__builder.SetException(exception);
					return;
				}
				IL_595:
				num2 = -2;
				this.<>t__builder.SetResult();
			}

			// Token: 0x0600443E RID: 17470 RVA: 0x000E8108 File Offset: 0x000E6308
			[DebuggerHidden]
			void IAsyncStateMachine.SetStateMachine(IAsyncStateMachine stateMachine)
			{
				this.<>t__builder.SetStateMachine(stateMachine);
			}

			// Token: 0x04002344 RID: 9028
			public int <>1__state;

			// Token: 0x04002345 RID: 9029
			public AsyncTaskMethodBuilder <>t__builder;

			// Token: 0x04002346 RID: 9030
			public CryptoStream <>4__this;

			// Token: 0x04002347 RID: 9031
			public int count;

			// Token: 0x04002348 RID: 9032
			public int offset;

			// Token: 0x04002349 RID: 9033
			public byte[] buffer;

			// Token: 0x0400234A RID: 9034
			public CancellationToken cancellationToken;

			// Token: 0x0400234B RID: 9035
			private int <bytesToWrite>5__1;

			// Token: 0x0400234C RID: 9036
			private int <currentInputIndex>5__2;

			// Token: 0x0400234D RID: 9037
			private int <numWholeBlocksInBytes>5__3;

			// Token: 0x0400234E RID: 9038
			private SemaphoreSlim <sem>5__4;

			// Token: 0x0400234F RID: 9039
			private CryptoStream.HopToThreadPoolAwaitable <>u__1;

			// Token: 0x04002350 RID: 9040
			private ConfiguredTaskAwaitable.ConfiguredTaskAwaiter <>u__2;
		}
	}
}
