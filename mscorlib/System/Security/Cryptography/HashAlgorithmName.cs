﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the name of a cryptographic hash algorithm. </summary>
	// Token: 0x02000655 RID: 1621
	public struct HashAlgorithmName : IEquatable<HashAlgorithmName>
	{
		/// <summary>Gets a hash algorithm name that represents "MD5".</summary>
		/// <returns>A hash algorithm name that represents "MD5". </returns>
		// Token: 0x17000B57 RID: 2903
		// (get) Token: 0x06004396 RID: 17302 RVA: 0x000E5B9F File Offset: 0x000E3D9F
		public static HashAlgorithmName MD5
		{
			get
			{
				return new HashAlgorithmName("MD5");
			}
		}

		/// <summary>Gets a hash algorithm name that represents "SHA1".</summary>
		/// <returns>A hash algorithm name that represents "SHA1". </returns>
		// Token: 0x17000B58 RID: 2904
		// (get) Token: 0x06004397 RID: 17303 RVA: 0x000E5BAB File Offset: 0x000E3DAB
		public static HashAlgorithmName SHA1
		{
			get
			{
				return new HashAlgorithmName("SHA1");
			}
		}

		/// <summary>Gets a hash algorithm name that represents "SHA256".</summary>
		/// <returns>A hash algorithm name that represents "SHA256". </returns>
		// Token: 0x17000B59 RID: 2905
		// (get) Token: 0x06004398 RID: 17304 RVA: 0x000E5BB7 File Offset: 0x000E3DB7
		public static HashAlgorithmName SHA256
		{
			get
			{
				return new HashAlgorithmName("SHA256");
			}
		}

		/// <summary>Gets a hash algorithm name that represents "SHA384".</summary>
		/// <returns>A hash algorithm name that represents "SHA384". </returns>
		// Token: 0x17000B5A RID: 2906
		// (get) Token: 0x06004399 RID: 17305 RVA: 0x000E5BC3 File Offset: 0x000E3DC3
		public static HashAlgorithmName SHA384
		{
			get
			{
				return new HashAlgorithmName("SHA384");
			}
		}

		/// <summary>Gets a hash algorithm name that represents "SHA512".</summary>
		/// <returns>A hash algorithm name that represents "SHA512". </returns>
		// Token: 0x17000B5B RID: 2907
		// (get) Token: 0x0600439A RID: 17306 RVA: 0x000E5BCF File Offset: 0x000E3DCF
		public static HashAlgorithmName SHA512
		{
			get
			{
				return new HashAlgorithmName("SHA512");
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.HashAlgorithmName" /> structure with a custom name. </summary>
		/// <param name="name">The custom hash algorithm name. </param>
		// Token: 0x0600439B RID: 17307 RVA: 0x000E5BDB File Offset: 0x000E3DDB
		public HashAlgorithmName(string name)
		{
			this._name = name;
		}

		/// <summary>Gets the underlying string representation of the algorithm name. </summary>
		/// <returns>The string representation of the algorithm name, or <see langword="null" /> or <see cref="F:System.String.Empty" /> if no hash algorithm is available. </returns>
		// Token: 0x17000B5C RID: 2908
		// (get) Token: 0x0600439C RID: 17308 RVA: 0x000E5BE4 File Offset: 0x000E3DE4
		public string Name
		{
			get
			{
				return this._name;
			}
		}

		/// <summary>Returns the string representation of the current <see cref="T:System.Security.Cryptography.HashAlgorithmName" /> instance. </summary>
		/// <returns>The string representation of the current <see cref="T:System.Security.Cryptography.HashAlgorithmName" /> instance. </returns>
		// Token: 0x0600439D RID: 17309 RVA: 0x000E5BEC File Offset: 0x000E3DEC
		public override string ToString()
		{
			return this._name ?? string.Empty;
		}

		/// <summary>Returns a value that indicates whether the current instance and a specified object are equal. </summary>
		/// <param name="obj">The object to compare with the current instance. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is a <see cref="T:System.Security.Cryptography.HashAlgorithmName" /> object and its <see cref="P:System.Security.Cryptography.HashAlgorithmName.Name" /> property is equal to that of the current instance. The comparison is ordinal and case-sensitive. </returns>
		// Token: 0x0600439E RID: 17310 RVA: 0x000E5BFD File Offset: 0x000E3DFD
		public override bool Equals(object obj)
		{
			return obj is HashAlgorithmName && this.Equals((HashAlgorithmName)obj);
		}

		/// <summary>Returns a value that indicates whether two <see cref="T:System.Security.Cryptography.HashAlgorithmName" /> instances are equal. </summary>
		/// <param name="other">The object to compare with the current instance. </param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="P:System.Security.Cryptography.HashAlgorithmName.Name" /> property of <paramref name="other" /> is equal to that of the current instance. The comparison is ordinal and case-sensitive. </returns>
		// Token: 0x0600439F RID: 17311 RVA: 0x000E5C15 File Offset: 0x000E3E15
		public bool Equals(HashAlgorithmName other)
		{
			return this._name == other._name;
		}

		/// <summary>Returns the hash code for the current instance. </summary>
		/// <returns>The hash code for the current instance, or 0 if no <paramref name="name" /> value was supplied to the <see cref="T:System.Security.Cryptography.HashAlgorithmName" /> constructor. </returns>
		// Token: 0x060043A0 RID: 17312 RVA: 0x000E5C28 File Offset: 0x000E3E28
		public override int GetHashCode()
		{
			if (this._name != null)
			{
				return this._name.GetHashCode();
			}
			return 0;
		}

		/// <summary>Determines whether two specified <see cref="T:System.Security.Cryptography.HashAlgorithmName" /> objects are equal. </summary>
		/// <param name="left">The first object to compare. </param>
		/// <param name="right">The second object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if both <paramref name="left" /> and <paramref name="right" /> have the same <see cref="P:System.Security.Cryptography.HashAlgorithmName.Name" /> value; otherwise, <see langword="false" />.  </returns>
		// Token: 0x060043A1 RID: 17313 RVA: 0x000E5C3F File Offset: 0x000E3E3F
		public static bool operator ==(HashAlgorithmName left, HashAlgorithmName right)
		{
			return left.Equals(right);
		}

		/// <summary>Determines whether two specified <see cref="T:System.Security.Cryptography.HashAlgorithmName" /> objects are not equal. </summary>
		/// <param name="left">The first object to compare. </param>
		/// <param name="right">The second object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if both <paramref name="left" /> and <paramref name="right" /> do not have the same <see cref="P:System.Security.Cryptography.HashAlgorithmName.Name" /> value; otherwise, <see langword="false" />.  </returns>
		// Token: 0x060043A2 RID: 17314 RVA: 0x000E5C49 File Offset: 0x000E3E49
		public static bool operator !=(HashAlgorithmName left, HashAlgorithmName right)
		{
			return !(left == right);
		}

		// Token: 0x040022E7 RID: 8935
		private readonly string _name;
	}
}
