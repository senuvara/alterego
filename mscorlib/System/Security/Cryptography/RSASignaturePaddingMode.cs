﻿using System;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the padding mode to use with RSA signature creation or verification operations. </summary>
	// Token: 0x02000659 RID: 1625
	public enum RSASignaturePaddingMode
	{
		/// <summary>PKCS #1 v1.5</summary>
		// Token: 0x040022F6 RID: 8950
		Pkcs1,
		/// <summary>Probabilistic Signature Scheme</summary>
		// Token: 0x040022F7 RID: 8951
		Pss
	}
}
