﻿using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace System.Security.Cryptography
{
	/// <summary>Implements a cryptographic Random Number Generator (RNG) using the implementation provided by the cryptographic service provider (CSP). This class cannot be inherited.</summary>
	// Token: 0x020006B5 RID: 1717
	public sealed class RNGCryptoServiceProvider : RandomNumberGenerator
	{
		// Token: 0x060046AE RID: 18094 RVA: 0x000F6C2F File Offset: 0x000F4E2F
		static RNGCryptoServiceProvider()
		{
			if (RNGCryptoServiceProvider.RngOpen())
			{
				RNGCryptoServiceProvider._lock = new object();
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.RNGCryptoServiceProvider" /> class.</summary>
		// Token: 0x060046AF RID: 18095 RVA: 0x000F6C42 File Offset: 0x000F4E42
		public RNGCryptoServiceProvider()
		{
			this._handle = RNGCryptoServiceProvider.RngInitialize(null);
			this.Check();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.RNGCryptoServiceProvider" /> class.</summary>
		/// <param name="rgb">A byte array. This value is ignored.</param>
		// Token: 0x060046B0 RID: 18096 RVA: 0x000F6C5C File Offset: 0x000F4E5C
		public RNGCryptoServiceProvider(byte[] rgb)
		{
			this._handle = RNGCryptoServiceProvider.RngInitialize(rgb);
			this.Check();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.RNGCryptoServiceProvider" /> class with the specified parameters.</summary>
		/// <param name="cspParams">The parameters to pass to the cryptographic service provider (CSP). </param>
		// Token: 0x060046B1 RID: 18097 RVA: 0x000F6C42 File Offset: 0x000F4E42
		public RNGCryptoServiceProvider(CspParameters cspParams)
		{
			this._handle = RNGCryptoServiceProvider.RngInitialize(null);
			this.Check();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.RNGCryptoServiceProvider" /> class.</summary>
		/// <param name="str">The string input. This parameter is ignored.</param>
		// Token: 0x060046B2 RID: 18098 RVA: 0x000F6C76 File Offset: 0x000F4E76
		public RNGCryptoServiceProvider(string str)
		{
			if (str == null)
			{
				this._handle = RNGCryptoServiceProvider.RngInitialize(null);
			}
			else
			{
				this._handle = RNGCryptoServiceProvider.RngInitialize(Encoding.UTF8.GetBytes(str));
			}
			this.Check();
		}

		// Token: 0x060046B3 RID: 18099 RVA: 0x000F6CAB File Offset: 0x000F4EAB
		private void Check()
		{
			if (this._handle == IntPtr.Zero)
			{
				throw new CryptographicException(Locale.GetText("Couldn't access random source."));
			}
		}

		// Token: 0x060046B4 RID: 18100
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool RngOpen();

		// Token: 0x060046B5 RID: 18101
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr RngInitialize(byte[] seed);

		// Token: 0x060046B6 RID: 18102
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr RngGetBytes(IntPtr handle, byte[] data);

		// Token: 0x060046B7 RID: 18103
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void RngClose(IntPtr handle);

		/// <summary>Fills an array of bytes with a cryptographically strong sequence of random values.</summary>
		/// <param name="data">The array to fill with a cryptographically strong sequence of random values. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The cryptographic service provider (CSP) cannot be acquired. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		// Token: 0x060046B8 RID: 18104 RVA: 0x000F6CD0 File Offset: 0x000F4ED0
		public override void GetBytes(byte[] data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (RNGCryptoServiceProvider._lock == null)
			{
				this._handle = RNGCryptoServiceProvider.RngGetBytes(this._handle, data);
			}
			else
			{
				object @lock = RNGCryptoServiceProvider._lock;
				lock (@lock)
				{
					this._handle = RNGCryptoServiceProvider.RngGetBytes(this._handle, data);
				}
			}
			this.Check();
		}

		/// <summary>Fills an array of bytes with a cryptographically strong sequence of random nonzero values.</summary>
		/// <param name="data">The array to fill with a cryptographically strong sequence of random nonzero values. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The cryptographic service provider (CSP) cannot be acquired. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="data" /> is <see langword="null" />.</exception>
		// Token: 0x060046B9 RID: 18105 RVA: 0x000F6D4C File Offset: 0x000F4F4C
		public override void GetNonZeroBytes(byte[] data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			byte[] array = new byte[data.Length * 2];
			int i = 0;
			while (i < data.Length)
			{
				this._handle = RNGCryptoServiceProvider.RngGetBytes(this._handle, array);
				this.Check();
				int num = 0;
				while (num < array.Length && i != data.Length)
				{
					if (array[num] != 0)
					{
						data[i++] = array[num];
					}
					num++;
				}
			}
		}

		// Token: 0x060046BA RID: 18106 RVA: 0x000F6DB8 File Offset: 0x000F4FB8
		~RNGCryptoServiceProvider()
		{
			if (this._handle != IntPtr.Zero)
			{
				RNGCryptoServiceProvider.RngClose(this._handle);
				this._handle = IntPtr.Zero;
			}
		}

		// Token: 0x060046BB RID: 18107 RVA: 0x000F6E08 File Offset: 0x000F5008
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		// Token: 0x04002464 RID: 9316
		private static object _lock;

		// Token: 0x04002465 RID: 9317
		private IntPtr _handle;
	}
}
