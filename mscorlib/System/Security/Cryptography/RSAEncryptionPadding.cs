﻿using System;
using Unity;

namespace System.Security.Cryptography
{
	/// <summary>Specifies the padding mode and parameters to use with RSA encryption or decryption operations. </summary>
	// Token: 0x02000656 RID: 1622
	public sealed class RSAEncryptionPadding : IEquatable<RSAEncryptionPadding>
	{
		/// <summary>Gets an object that represents the PKCS #1 encryption standard. </summary>
		/// <returns>An object that represents the PKCS #1 encryption standard. </returns>
		// Token: 0x17000B5D RID: 2909
		// (get) Token: 0x060043A3 RID: 17315 RVA: 0x000E5C55 File Offset: 0x000E3E55
		public static RSAEncryptionPadding Pkcs1
		{
			get
			{
				return RSAEncryptionPadding.s_pkcs1;
			}
		}

		/// <summary>Gets an object that represents the Optimal Asymmetric Encryption Padding (OAEP) encryption standard with a SHA1 hash algorithm.</summary>
		/// <returns>An object that represents the OAEP encryption standard with a SHA1 hash algorithm.</returns>
		// Token: 0x17000B5E RID: 2910
		// (get) Token: 0x060043A4 RID: 17316 RVA: 0x000E5C5C File Offset: 0x000E3E5C
		public static RSAEncryptionPadding OaepSHA1
		{
			get
			{
				return RSAEncryptionPadding.s_oaepSHA1;
			}
		}

		/// <summary>Gets an object that represents the Optimal Asymmetric Encryption Padding (OAEP) encryption standard with a SHA256 hash algorithm. </summary>
		/// <returns>An object that represents the OAEP encryption standard with a SHA256 hash algorithm. </returns>
		// Token: 0x17000B5F RID: 2911
		// (get) Token: 0x060043A5 RID: 17317 RVA: 0x000E5C63 File Offset: 0x000E3E63
		public static RSAEncryptionPadding OaepSHA256
		{
			get
			{
				return RSAEncryptionPadding.s_oaepSHA256;
			}
		}

		/// <summary>Gets an object that represents the Optimal Asymmetric Encryption Padding (OAEP) encryption standard with a SHA-384 hash algorithm.</summary>
		/// <returns>An object that represents the OAEP encryption standard with a SHA384 hash algorithm. </returns>
		// Token: 0x17000B60 RID: 2912
		// (get) Token: 0x060043A6 RID: 17318 RVA: 0x000E5C6A File Offset: 0x000E3E6A
		public static RSAEncryptionPadding OaepSHA384
		{
			get
			{
				return RSAEncryptionPadding.s_oaepSHA384;
			}
		}

		/// <summary>Gets an object that represents the Optimal Asymmetric Encryption Padding (OAEP) encryption standard with a SHA512 hash algorithm. </summary>
		/// <returns>An object that represents the OAEP encryption standard with a SHA512 hash algorithm. </returns>
		// Token: 0x17000B61 RID: 2913
		// (get) Token: 0x060043A7 RID: 17319 RVA: 0x000E5C71 File Offset: 0x000E3E71
		public static RSAEncryptionPadding OaepSHA512
		{
			get
			{
				return RSAEncryptionPadding.s_oaepSHA512;
			}
		}

		// Token: 0x060043A8 RID: 17320 RVA: 0x000E5C78 File Offset: 0x000E3E78
		private RSAEncryptionPadding(RSAEncryptionPaddingMode mode, HashAlgorithmName oaepHashAlgorithm)
		{
			this._mode = mode;
			this._oaepHashAlgorithm = oaepHashAlgorithm;
		}

		/// <summary>Creates a new <see cref="T:System.Security.Cryptography.RSAEncryptionPadding" /> instance whose <see cref="P:System.Security.Cryptography.RSAEncryptionPadding.Mode" /> is <see cref="F:System.Security.Cryptography.RSAEncryptionPaddingMode.Oaep" /> with the given hash algorithm. </summary>
		/// <param name="hashAlgorithm">The hash algorithm. </param>
		/// <returns>An object whose mode is <see cref="P:System.Security.Cryptography.RSAEncryptionPadding.Mode" /> is <see cref="F:System.Security.Cryptography.RSAEncryptionPaddingMode.Oaep" /> with the hash algorithm specified by <paramref name="hashAlgorithm" />. .</returns>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Security.Cryptography.HashAlgorithmName.Name" /> property of <paramref name="hashAlgorithm" /> is either <see langword="null" /> or <see cref="F:System.String.Empty" />. </exception>
		// Token: 0x060043A9 RID: 17321 RVA: 0x000E5C8E File Offset: 0x000E3E8E
		public static RSAEncryptionPadding CreateOaep(HashAlgorithmName hashAlgorithm)
		{
			if (string.IsNullOrEmpty(hashAlgorithm.Name))
			{
				throw new ArgumentException(Environment.GetResourceString("The hash algorithm name cannot be null or empty."), "hashAlgorithm");
			}
			return new RSAEncryptionPadding(RSAEncryptionPaddingMode.Oaep, hashAlgorithm);
		}

		/// <summary>Gets the padding mode represented by this <see cref="T:System.Security.Cryptography.RSAEncryptionPadding" /> instance. </summary>
		/// <returns>A padding mode. </returns>
		// Token: 0x17000B62 RID: 2914
		// (get) Token: 0x060043AA RID: 17322 RVA: 0x000E5CBA File Offset: 0x000E3EBA
		public RSAEncryptionPaddingMode Mode
		{
			get
			{
				return this._mode;
			}
		}

		/// <summary>Gets the hash algorithm used in conjunction with the <see cref="F:System.Security.Cryptography.RSAEncryptionPaddingMode.Oaep" /> padding mode. If the value of the <see cref="P:System.Security.Cryptography.RSAEncryptionPadding.Mode" /> property is not <see cref="F:System.Security.Cryptography.RSAEncryptionPaddingMode.Oaep" />, <see cref="P:System.Security.Cryptography.HashAlgorithmName.Name" /> is <see langword="null" />. </summary>
		/// <returns>The hash algorithm. </returns>
		// Token: 0x17000B63 RID: 2915
		// (get) Token: 0x060043AB RID: 17323 RVA: 0x000E5CC2 File Offset: 0x000E3EC2
		public HashAlgorithmName OaepHashAlgorithm
		{
			get
			{
				return this._oaepHashAlgorithm;
			}
		}

		/// <summary>Returns the hash code of this <see cref="T:System.Security.Cryptography.RSAEncryptionPadding" /> object. </summary>
		/// <returns>The hash code of this instance. </returns>
		// Token: 0x060043AC RID: 17324 RVA: 0x000E5CCA File Offset: 0x000E3ECA
		public override int GetHashCode()
		{
			return RSAEncryptionPadding.CombineHashCodes(this._mode.GetHashCode(), this._oaepHashAlgorithm.GetHashCode());
		}

		// Token: 0x060043AD RID: 17325 RVA: 0x00021FC3 File Offset: 0x000201C3
		private static int CombineHashCodes(int h1, int h2)
		{
			return (h1 << 5) + h1 ^ h2;
		}

		/// <summary>Determines whether the current instance is equal to the specified object. </summary>
		/// <param name="obj">The object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is equal to the current instance; otherwise, <see langword="false" />. </returns>
		// Token: 0x060043AE RID: 17326 RVA: 0x000E5CF3 File Offset: 0x000E3EF3
		public override bool Equals(object obj)
		{
			return this.Equals(obj as RSAEncryptionPadding);
		}

		/// <summary>Determines whether the current instance is equal to the specified <see cref="T:System.Security.Cryptography.RSAEncryptionPadding" /> object. </summary>
		/// <param name="other">The object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="other" /> is equal to the current instance; otherwise, <see langword="false" />. </returns>
		// Token: 0x060043AF RID: 17327 RVA: 0x000E5D01 File Offset: 0x000E3F01
		public bool Equals(RSAEncryptionPadding other)
		{
			return other != null && this._mode == other._mode && this._oaepHashAlgorithm == other._oaepHashAlgorithm;
		}

		/// <summary>Indicates whether two specified <see cref="T:System.Security.Cryptography.RSAEncryptionPadding" /> objects are equal. </summary>
		/// <param name="left">The first object to compare. </param>
		/// <param name="right">The second object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if <see langword="left" /> and <see langword="right" /> are equal; otherwise, <see langword="false" />. </returns>
		// Token: 0x060043B0 RID: 17328 RVA: 0x000E5D2D File Offset: 0x000E3F2D
		public static bool operator ==(RSAEncryptionPadding left, RSAEncryptionPadding right)
		{
			if (left == null)
			{
				return right == null;
			}
			return left.Equals(right);
		}

		/// <summary>Indicates whether two specified <see cref="T:System.Security.Cryptography.RSAEncryptionPadding" /> objects are unequal. </summary>
		/// <param name="left">The first object to compare. </param>
		/// <param name="right">The second object to compare. </param>
		/// <returns>
		///     <see langword="true" /> if <see langword="left" /> and <see langword="right" /> are not equal; otherwise, <see langword="false" />.</returns>
		// Token: 0x060043B1 RID: 17329 RVA: 0x000E5D3E File Offset: 0x000E3F3E
		public static bool operator !=(RSAEncryptionPadding left, RSAEncryptionPadding right)
		{
			return !(left == right);
		}

		/// <summary>Returns the string representation of the current <see cref="T:System.Security.Cryptography.RSAEncryptionPadding" /> instance. </summary>
		/// <returns>The string representation of the current object. </returns>
		// Token: 0x060043B2 RID: 17330 RVA: 0x000E5D4A File Offset: 0x000E3F4A
		public override string ToString()
		{
			return this._mode.ToString() + this._oaepHashAlgorithm.Name;
		}

		// Token: 0x060043B3 RID: 17331 RVA: 0x000E5D70 File Offset: 0x000E3F70
		// Note: this type is marked as 'beforefieldinit'.
		static RSAEncryptionPadding()
		{
		}

		// Token: 0x060043B4 RID: 17332 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal RSAEncryptionPadding()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040022E8 RID: 8936
		private static readonly RSAEncryptionPadding s_pkcs1 = new RSAEncryptionPadding(RSAEncryptionPaddingMode.Pkcs1, default(HashAlgorithmName));

		// Token: 0x040022E9 RID: 8937
		private static readonly RSAEncryptionPadding s_oaepSHA1 = RSAEncryptionPadding.CreateOaep(HashAlgorithmName.SHA1);

		// Token: 0x040022EA RID: 8938
		private static readonly RSAEncryptionPadding s_oaepSHA256 = RSAEncryptionPadding.CreateOaep(HashAlgorithmName.SHA256);

		// Token: 0x040022EB RID: 8939
		private static readonly RSAEncryptionPadding s_oaepSHA384 = RSAEncryptionPadding.CreateOaep(HashAlgorithmName.SHA384);

		// Token: 0x040022EC RID: 8940
		private static readonly RSAEncryptionPadding s_oaepSHA512 = RSAEncryptionPadding.CreateOaep(HashAlgorithmName.SHA512);

		// Token: 0x040022ED RID: 8941
		private RSAEncryptionPaddingMode _mode;

		// Token: 0x040022EE RID: 8942
		private HashAlgorithmName _oaepHashAlgorithm;
	}
}
