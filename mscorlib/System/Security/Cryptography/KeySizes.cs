﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Determines the set of valid key sizes for the symmetric cryptographic algorithms.</summary>
	// Token: 0x02000665 RID: 1637
	[ComVisible(true)]
	public sealed class KeySizes
	{
		/// <summary>Specifies the minimum key size in bits.</summary>
		/// <returns>The minimum key size in bits.</returns>
		// Token: 0x17000B75 RID: 2933
		// (get) Token: 0x060043FF RID: 17407 RVA: 0x000E65C0 File Offset: 0x000E47C0
		public int MinSize
		{
			get
			{
				return this.m_minSize;
			}
		}

		/// <summary>Specifies the maximum key size in bits.</summary>
		/// <returns>The maximum key size in bits.</returns>
		// Token: 0x17000B76 RID: 2934
		// (get) Token: 0x06004400 RID: 17408 RVA: 0x000E65C8 File Offset: 0x000E47C8
		public int MaxSize
		{
			get
			{
				return this.m_maxSize;
			}
		}

		/// <summary>Specifies the interval between valid key sizes in bits.</summary>
		/// <returns>The interval between valid key sizes in bits.</returns>
		// Token: 0x17000B77 RID: 2935
		// (get) Token: 0x06004401 RID: 17409 RVA: 0x000E65D0 File Offset: 0x000E47D0
		public int SkipSize
		{
			get
			{
				return this.m_skipSize;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.KeySizes" /> class with the specified key values.</summary>
		/// <param name="minSize">The minimum valid key size. </param>
		/// <param name="maxSize">The maximum valid key size. </param>
		/// <param name="skipSize">The interval between valid key sizes. </param>
		// Token: 0x06004402 RID: 17410 RVA: 0x000E65D8 File Offset: 0x000E47D8
		public KeySizes(int minSize, int maxSize, int skipSize)
		{
			this.m_minSize = minSize;
			this.m_maxSize = maxSize;
			this.m_skipSize = skipSize;
		}

		// Token: 0x06004403 RID: 17411 RVA: 0x000E65F8 File Offset: 0x000E47F8
		internal bool IsLegal(int keySize)
		{
			int num = keySize - this.MinSize;
			bool flag = num >= 0 && keySize <= this.MaxSize;
			if (this.SkipSize != 0)
			{
				return flag && num % this.SkipSize == 0;
			}
			return flag;
		}

		// Token: 0x06004404 RID: 17412 RVA: 0x000E663C File Offset: 0x000E483C
		internal static bool IsLegalKeySize(KeySizes[] legalKeys, int size)
		{
			for (int i = 0; i < legalKeys.Length; i++)
			{
				if (legalKeys[i].IsLegal(size))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0400230E RID: 8974
		private int m_minSize;

		// Token: 0x0400230F RID: 8975
		private int m_maxSize;

		// Token: 0x04002310 RID: 8976
		private int m_skipSize;
	}
}
