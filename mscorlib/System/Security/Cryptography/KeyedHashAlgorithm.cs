﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Represents the abstract class from which all implementations of keyed hash algorithms must derive. </summary>
	// Token: 0x0200067F RID: 1663
	[ComVisible(true)]
	public abstract class KeyedHashAlgorithm : HashAlgorithm
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.KeyedHashAlgorithm" /> class.</summary>
		// Token: 0x060044AD RID: 17581 RVA: 0x000E95F8 File Offset: 0x000E77F8
		protected KeyedHashAlgorithm()
		{
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Security.Cryptography.KeyedHashAlgorithm" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x060044AE RID: 17582 RVA: 0x000E9600 File Offset: 0x000E7800
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.KeyValue != null)
				{
					Array.Clear(this.KeyValue, 0, this.KeyValue.Length);
				}
				this.KeyValue = null;
			}
			base.Dispose(disposing);
		}

		/// <summary>Gets or sets the key to use in the hash algorithm.</summary>
		/// <returns>The key to use in the hash algorithm.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An attempt was made to change the <see cref="P:System.Security.Cryptography.KeyedHashAlgorithm.Key" /> property after hashing has begun. </exception>
		// Token: 0x17000B95 RID: 2965
		// (get) Token: 0x060044AF RID: 17583 RVA: 0x000E90DD File Offset: 0x000E72DD
		// (set) Token: 0x060044B0 RID: 17584 RVA: 0x000E962F File Offset: 0x000E782F
		public virtual byte[] Key
		{
			get
			{
				return (byte[])this.KeyValue.Clone();
			}
			set
			{
				if (this.State != 0)
				{
					throw new CryptographicException(Environment.GetResourceString("Hash key cannot be changed after the first write to the stream."));
				}
				this.KeyValue = (byte[])value.Clone();
			}
		}

		/// <summary>Creates an instance of the default implementation of a keyed hash algorithm.</summary>
		/// <returns>A new <see cref="T:System.Security.Cryptography.HMACSHA1" /> instance, unless the default settings have been changed.</returns>
		// Token: 0x060044B1 RID: 17585 RVA: 0x000E9166 File Offset: 0x000E7366
		public new static KeyedHashAlgorithm Create()
		{
			return new HMACSHA1();
		}

		/// <summary>Creates an instance of the specified implementation of a keyed hash algorithm.</summary>
		/// <param name="algName">The keyed hash algorithm implementation to use. The following table shows the valid values for the <paramref name="algName" /> parameter and the algorithms they map to.Parameter valueImplements System.Security.Cryptography.HMAC
		///               <see cref="T:System.Security.Cryptography.HMACSHA1" />
		///             System.Security.Cryptography.KeyedHashAlgorithm
		///               <see cref="T:System.Security.Cryptography.HMACSHA1" />
		///             HMACMD5
		///               <see cref="T:System.Security.Cryptography.HMACMD5" />
		///             System.Security.Cryptography.HMACMD5
		///               <see cref="T:System.Security.Cryptography.HMACMD5" />
		///             HMACRIPEMD160
		///               <see cref="T:System.Security.Cryptography.HMACRIPEMD160" />
		///             System.Security.Cryptography.HMACRIPEMD160
		///               <see cref="T:System.Security.Cryptography.HMACRIPEMD160" />
		///             HMACSHA1
		///               <see cref="T:System.Security.Cryptography.HMACSHA1" />
		///             System.Security.Cryptography.HMACSHA1
		///               <see cref="T:System.Security.Cryptography.HMACSHA1" />
		///             HMACSHA256
		///               <see cref="T:System.Security.Cryptography.HMACSHA256" />
		///             System.Security.Cryptography.HMACSHA256
		///               <see cref="T:System.Security.Cryptography.HMACSHA256" />
		///             HMACSHA384
		///               <see cref="T:System.Security.Cryptography.HMACSHA384" />
		///             System.Security.Cryptography.HMACSHA384
		///               <see cref="T:System.Security.Cryptography.HMACSHA384" />
		///             HMACSHA512
		///               <see cref="T:System.Security.Cryptography.HMACSHA512" />
		///             System.Security.Cryptography.HMACSHA512
		///               <see cref="T:System.Security.Cryptography.HMACSHA512" />
		///             MACTripleDES
		///               <see cref="T:System.Security.Cryptography.MACTripleDES" />
		///             System.Security.Cryptography.MACTripleDES
		///               <see cref="T:System.Security.Cryptography.MACTripleDES" />
		///             </param>
		/// <returns>A new instance of the specified keyed hash algorithm.</returns>
		// Token: 0x060044B2 RID: 17586 RVA: 0x000E965A File Offset: 0x000E785A
		public new static KeyedHashAlgorithm Create(string algName)
		{
			return (KeyedHashAlgorithm)CryptoConfig.CreateFromName(algName);
		}

		/// <summary>The key to use in the hash algorithm.</summary>
		// Token: 0x0400236C RID: 9068
		protected byte[] KeyValue;
	}
}
