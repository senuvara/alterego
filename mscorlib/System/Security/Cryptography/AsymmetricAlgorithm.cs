﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Represents the abstract base class from which all implementations of asymmetric algorithms must inherit.</summary>
	// Token: 0x0200065B RID: 1627
	[ComVisible(true)]
	public abstract class AsymmetricAlgorithm : IDisposable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> class.</summary>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The implementation of the derived class is not valid. </exception>
		// Token: 0x060043C5 RID: 17349 RVA: 0x00002050 File Offset: 0x00000250
		protected AsymmetricAlgorithm()
		{
		}

		/// <summary>Releases all resources used by the current instance of the <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> class.</summary>
		// Token: 0x060043C6 RID: 17350 RVA: 0x000E5F4B File Offset: 0x000E414B
		public void Dispose()
		{
			this.Clear();
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> class.</summary>
		// Token: 0x060043C7 RID: 17351 RVA: 0x000E5F53 File Offset: 0x000E4153
		public void Clear()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> class and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x060043C8 RID: 17352 RVA: 0x000020D3 File Offset: 0x000002D3
		protected virtual void Dispose(bool disposing)
		{
		}

		/// <summary>Gets or sets the size, in bits, of the key modulus used by the asymmetric algorithm.</summary>
		/// <returns>The size, in bits, of the key modulus used by the asymmetric algorithm.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key modulus size is invalid. </exception>
		// Token: 0x17000B67 RID: 2919
		// (get) Token: 0x060043C9 RID: 17353 RVA: 0x000E5F62 File Offset: 0x000E4162
		// (set) Token: 0x060043CA RID: 17354 RVA: 0x000E5F6C File Offset: 0x000E416C
		public virtual int KeySize
		{
			get
			{
				return this.KeySizeValue;
			}
			set
			{
				for (int i = 0; i < this.LegalKeySizesValue.Length; i++)
				{
					if (this.LegalKeySizesValue[i].SkipSize == 0)
					{
						if (this.LegalKeySizesValue[i].MinSize == value)
						{
							this.KeySizeValue = value;
							return;
						}
					}
					else
					{
						for (int j = this.LegalKeySizesValue[i].MinSize; j <= this.LegalKeySizesValue[i].MaxSize; j += this.LegalKeySizesValue[i].SkipSize)
						{
							if (j == value)
							{
								this.KeySizeValue = value;
								return;
							}
						}
					}
				}
				throw new CryptographicException(Environment.GetResourceString("Specified key is not a valid size for this algorithm."));
			}
		}

		/// <summary>Gets the key sizes that are supported by the asymmetric algorithm.</summary>
		/// <returns>An array that contains the key sizes supported by the asymmetric algorithm.</returns>
		// Token: 0x17000B68 RID: 2920
		// (get) Token: 0x060043CB RID: 17355 RVA: 0x000E5FFE File Offset: 0x000E41FE
		public virtual KeySizes[] LegalKeySizes
		{
			get
			{
				return (KeySizes[])this.LegalKeySizesValue.Clone();
			}
		}

		/// <summary>When implemented in a derived class, gets the name of the signature algorithm. Otherwise, always throws a <see cref="T:System.NotImplementedException" />.</summary>
		/// <returns>The name of the signature algorithm.</returns>
		// Token: 0x17000B69 RID: 2921
		// (get) Token: 0x060043CC RID: 17356 RVA: 0x000041F3 File Offset: 0x000023F3
		public virtual string SignatureAlgorithm
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>When overridden in a derived class, gets the name of the key exchange algorithm. Otherwise, throws an <see cref="T:System.NotImplementedException" />.</summary>
		/// <returns>The name of the key exchange algorithm.</returns>
		// Token: 0x17000B6A RID: 2922
		// (get) Token: 0x060043CD RID: 17357 RVA: 0x000041F3 File Offset: 0x000023F3
		public virtual string KeyExchangeAlgorithm
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Creates a default cryptographic object used to perform the asymmetric algorithm.</summary>
		/// <returns>A new <see cref="T:System.Security.Cryptography.RSACryptoServiceProvider" /> instance, unless the default settings have been changed with the &lt;cryptoClass&gt; element.</returns>
		// Token: 0x060043CE RID: 17358 RVA: 0x000E6010 File Offset: 0x000E4210
		public static AsymmetricAlgorithm Create()
		{
			return new RSACryptoServiceProvider();
		}

		/// <summary>Creates an instance of the specified implementation of an asymmetric algorithm.</summary>
		/// <param name="algName">The asymmetric algorithm implementation to use. The following table shows the valid values for the <paramref name="algName" /> parameter and the algorithms they map to.Parameter valueImplements System.Security.Cryptography.AsymmetricAlgorithm
		///               <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" />
		///             RSA
		///               <see cref="T:System.Security.Cryptography.RSA" />
		///             System.Security.Cryptography.RSA
		///               <see cref="T:System.Security.Cryptography.RSA" />
		///             DSA
		///               <see cref="T:System.Security.Cryptography.DSA" />
		///             System.Security.Cryptography.DSA
		///               <see cref="T:System.Security.Cryptography.DSA" />
		///             ECDsa
		///               <see cref="T:System.Security.Cryptography.ECDsa" />
		///             ECDsaCng
		///               <see cref="T:System.Security.Cryptography.ECDsaCng" />
		///             System.Security.Cryptography.ECDsaCng
		///               <see cref="T:System.Security.Cryptography.ECDsaCng" />
		///             ECDH
		///               <see cref="T:System.Security.Cryptography.ECDiffieHellman" />
		///             ECDiffieHellman
		///               <see cref="T:System.Security.Cryptography.ECDiffieHellman" />
		///             ECDiffieHellmanCng
		///               <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" />
		///             System.Security.Cryptography.ECDiffieHellmanCng
		///               <see cref="T:System.Security.Cryptography.ECDiffieHellmanCng" />
		///             </param>
		/// <returns>A new instance of the specified asymmetric algorithm implementation.</returns>
		// Token: 0x060043CF RID: 17359 RVA: 0x000E6017 File Offset: 0x000E4217
		public static AsymmetricAlgorithm Create(string algName)
		{
			return (AsymmetricAlgorithm)CryptoConfig.CreateFromName(algName);
		}

		/// <summary>When overridden in a derived class, reconstructs an <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> object from an XML string. Otherwise, throws a <see cref="T:System.NotImplementedException" />.</summary>
		/// <param name="xmlString">The XML string to use to reconstruct the <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> object. </param>
		// Token: 0x060043D0 RID: 17360 RVA: 0x000041F3 File Offset: 0x000023F3
		public virtual void FromXmlString(string xmlString)
		{
			throw new NotImplementedException();
		}

		/// <summary>When overridden in a derived class, creates and returns an XML string representation of the current <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> object. Otherwise, throws a <see cref="T:System.NotImplementedException" />.</summary>
		/// <param name="includePrivateParameters">
		///       <see langword="true" /> to include private parameters; otherwise, <see langword="false" />. </param>
		/// <returns>An XML string encoding of the current <see cref="T:System.Security.Cryptography.AsymmetricAlgorithm" /> object.</returns>
		// Token: 0x060043D1 RID: 17361 RVA: 0x000041F3 File Offset: 0x000023F3
		public virtual string ToXmlString(bool includePrivateParameters)
		{
			throw new NotImplementedException();
		}

		/// <summary>Represents the size, in bits, of the key modulus used by the asymmetric algorithm.</summary>
		// Token: 0x040022FA RID: 8954
		protected int KeySizeValue;

		/// <summary>Specifies the key sizes that are supported by the asymmetric algorithm.</summary>
		// Token: 0x040022FB RID: 8955
		protected KeySizes[] LegalKeySizesValue;
	}
}
