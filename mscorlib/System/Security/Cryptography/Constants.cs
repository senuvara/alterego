﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x020006AA RID: 1706
	internal static class Constants
	{
		// Token: 0x040023F4 RID: 9204
		internal const int S_OK = 0;

		// Token: 0x040023F5 RID: 9205
		internal const int NTE_FILENOTFOUND = -2147024894;

		// Token: 0x040023F6 RID: 9206
		internal const int NTE_NO_KEY = -2146893811;

		// Token: 0x040023F7 RID: 9207
		internal const int NTE_BAD_KEYSET = -2146893802;

		// Token: 0x040023F8 RID: 9208
		internal const int NTE_KEYSET_NOT_DEF = -2146893799;

		// Token: 0x040023F9 RID: 9209
		internal const int KP_IV = 1;

		// Token: 0x040023FA RID: 9210
		internal const int KP_MODE = 4;

		// Token: 0x040023FB RID: 9211
		internal const int KP_MODE_BITS = 5;

		// Token: 0x040023FC RID: 9212
		internal const int KP_EFFECTIVE_KEYLEN = 19;

		// Token: 0x040023FD RID: 9213
		internal const int ALG_CLASS_SIGNATURE = 8192;

		// Token: 0x040023FE RID: 9214
		internal const int ALG_CLASS_DATA_ENCRYPT = 24576;

		// Token: 0x040023FF RID: 9215
		internal const int ALG_CLASS_HASH = 32768;

		// Token: 0x04002400 RID: 9216
		internal const int ALG_CLASS_KEY_EXCHANGE = 40960;

		// Token: 0x04002401 RID: 9217
		internal const int ALG_TYPE_DSS = 512;

		// Token: 0x04002402 RID: 9218
		internal const int ALG_TYPE_RSA = 1024;

		// Token: 0x04002403 RID: 9219
		internal const int ALG_TYPE_BLOCK = 1536;

		// Token: 0x04002404 RID: 9220
		internal const int ALG_TYPE_STREAM = 2048;

		// Token: 0x04002405 RID: 9221
		internal const int ALG_TYPE_ANY = 0;

		// Token: 0x04002406 RID: 9222
		internal const int CALG_MD5 = 32771;

		// Token: 0x04002407 RID: 9223
		internal const int CALG_SHA1 = 32772;

		// Token: 0x04002408 RID: 9224
		internal const int CALG_SHA_256 = 32780;

		// Token: 0x04002409 RID: 9225
		internal const int CALG_SHA_384 = 32781;

		// Token: 0x0400240A RID: 9226
		internal const int CALG_SHA_512 = 32782;

		// Token: 0x0400240B RID: 9227
		internal const int CALG_RSA_KEYX = 41984;

		// Token: 0x0400240C RID: 9228
		internal const int CALG_RSA_SIGN = 9216;

		// Token: 0x0400240D RID: 9229
		internal const int CALG_DSS_SIGN = 8704;

		// Token: 0x0400240E RID: 9230
		internal const int CALG_DES = 26113;

		// Token: 0x0400240F RID: 9231
		internal const int CALG_RC2 = 26114;

		// Token: 0x04002410 RID: 9232
		internal const int CALG_3DES = 26115;

		// Token: 0x04002411 RID: 9233
		internal const int CALG_3DES_112 = 26121;

		// Token: 0x04002412 RID: 9234
		internal const int CALG_AES_128 = 26126;

		// Token: 0x04002413 RID: 9235
		internal const int CALG_AES_192 = 26127;

		// Token: 0x04002414 RID: 9236
		internal const int CALG_AES_256 = 26128;

		// Token: 0x04002415 RID: 9237
		internal const int CALG_RC4 = 26625;

		// Token: 0x04002416 RID: 9238
		internal const int PROV_RSA_FULL = 1;

		// Token: 0x04002417 RID: 9239
		internal const int PROV_DSS_DH = 13;

		// Token: 0x04002418 RID: 9240
		internal const int PROV_RSA_AES = 24;

		// Token: 0x04002419 RID: 9241
		internal const int AT_KEYEXCHANGE = 1;

		// Token: 0x0400241A RID: 9242
		internal const int AT_SIGNATURE = 2;

		// Token: 0x0400241B RID: 9243
		internal const int PUBLICKEYBLOB = 6;

		// Token: 0x0400241C RID: 9244
		internal const int PRIVATEKEYBLOB = 7;

		// Token: 0x0400241D RID: 9245
		internal const int CRYPT_OAEP = 64;

		// Token: 0x0400241E RID: 9246
		internal const uint CRYPT_VERIFYCONTEXT = 4026531840U;

		// Token: 0x0400241F RID: 9247
		internal const uint CRYPT_NEWKEYSET = 8U;

		// Token: 0x04002420 RID: 9248
		internal const uint CRYPT_DELETEKEYSET = 16U;

		// Token: 0x04002421 RID: 9249
		internal const uint CRYPT_MACHINE_KEYSET = 32U;

		// Token: 0x04002422 RID: 9250
		internal const uint CRYPT_SILENT = 64U;

		// Token: 0x04002423 RID: 9251
		internal const uint CRYPT_EXPORTABLE = 1U;

		// Token: 0x04002424 RID: 9252
		internal const uint CLR_KEYLEN = 1U;

		// Token: 0x04002425 RID: 9253
		internal const uint CLR_PUBLICKEYONLY = 2U;

		// Token: 0x04002426 RID: 9254
		internal const uint CLR_EXPORTABLE = 3U;

		// Token: 0x04002427 RID: 9255
		internal const uint CLR_REMOVABLE = 4U;

		// Token: 0x04002428 RID: 9256
		internal const uint CLR_HARDWARE = 5U;

		// Token: 0x04002429 RID: 9257
		internal const uint CLR_ACCESSIBLE = 6U;

		// Token: 0x0400242A RID: 9258
		internal const uint CLR_PROTECTED = 7U;

		// Token: 0x0400242B RID: 9259
		internal const uint CLR_UNIQUE_CONTAINER = 8U;

		// Token: 0x0400242C RID: 9260
		internal const uint CLR_ALGID = 9U;

		// Token: 0x0400242D RID: 9261
		internal const uint CLR_PP_CLIENT_HWND = 10U;

		// Token: 0x0400242E RID: 9262
		internal const uint CLR_PP_PIN = 11U;

		// Token: 0x0400242F RID: 9263
		internal const string OID_RSA_SMIMEalgCMS3DESwrap = "1.2.840.113549.1.9.16.3.6";

		// Token: 0x04002430 RID: 9264
		internal const string OID_RSA_MD5 = "1.2.840.113549.2.5";

		// Token: 0x04002431 RID: 9265
		internal const string OID_RSA_RC2CBC = "1.2.840.113549.3.2";

		// Token: 0x04002432 RID: 9266
		internal const string OID_RSA_DES_EDE3_CBC = "1.2.840.113549.3.7";

		// Token: 0x04002433 RID: 9267
		internal const string OID_OIWSEC_desCBC = "1.3.14.3.2.7";

		// Token: 0x04002434 RID: 9268
		internal const string OID_OIWSEC_SHA1 = "1.3.14.3.2.26";

		// Token: 0x04002435 RID: 9269
		internal const string OID_OIWSEC_SHA256 = "2.16.840.1.101.3.4.2.1";

		// Token: 0x04002436 RID: 9270
		internal const string OID_OIWSEC_SHA384 = "2.16.840.1.101.3.4.2.2";

		// Token: 0x04002437 RID: 9271
		internal const string OID_OIWSEC_SHA512 = "2.16.840.1.101.3.4.2.3";

		// Token: 0x04002438 RID: 9272
		internal const string OID_OIWSEC_RIPEMD160 = "1.3.36.3.2.1";
	}
}
