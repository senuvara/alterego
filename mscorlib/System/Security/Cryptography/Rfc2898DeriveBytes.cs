﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Security.Cryptography
{
	/// <summary>Implements password-based key derivation functionality, PBKDF2, by using a pseudo-random number generator based on <see cref="T:System.Security.Cryptography.HMACSHA1" />.</summary>
	// Token: 0x02000689 RID: 1673
	[ComVisible(true)]
	public class Rfc2898DeriveBytes : DeriveBytes
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.Rfc2898DeriveBytes" /> class using the password and salt size to derive the key.</summary>
		/// <param name="password">The password used to derive the key. </param>
		/// <param name="saltSize">The size of the random salt that you want the class to generate. </param>
		/// <exception cref="T:System.ArgumentException">The specified salt size is smaller than 8 bytes. </exception>
		/// <exception cref="T:System.ArgumentNullException">The password or salt is <see langword="null" />. </exception>
		// Token: 0x06004503 RID: 17667 RVA: 0x000EA453 File Offset: 0x000E8653
		public Rfc2898DeriveBytes(string password, int saltSize) : this(password, saltSize, 1000)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.Rfc2898DeriveBytes" /> class using a password, a salt size, and number of iterations to derive the key.</summary>
		/// <param name="password">The password used to derive the key. </param>
		/// <param name="saltSize">The size of the random salt that you want the class to generate. </param>
		/// <param name="iterations">The number of iterations for the operation. </param>
		/// <exception cref="T:System.ArgumentException">The specified salt size is smaller than 8 bytes or the iteration count is less than 1. </exception>
		/// <exception cref="T:System.ArgumentNullException">The password or salt is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="iterations " />is out of range. This parameter requires a non-negative number.</exception>
		// Token: 0x06004504 RID: 17668 RVA: 0x000EA464 File Offset: 0x000E8664
		[SecuritySafeCritical]
		public Rfc2898DeriveBytes(string password, int saltSize, int iterations)
		{
			if (saltSize < 0)
			{
				throw new ArgumentOutOfRangeException("saltSize", Environment.GetResourceString("Non-negative number required."));
			}
			byte[] array = new byte[saltSize];
			Utils.StaticRandomNumberGenerator.GetBytes(array);
			this.Salt = array;
			this.IterationCount = iterations;
			this.m_password = new UTF8Encoding(false).GetBytes(password);
			this.m_hmacsha1 = new HMACSHA1(this.m_password);
			this.Initialize();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.Rfc2898DeriveBytes" /> class using a password and salt to derive the key.</summary>
		/// <param name="password">The password used to derive the key. </param>
		/// <param name="salt">The key salt used to derive the key. </param>
		/// <exception cref="T:System.ArgumentException">The specified salt size is smaller than 8 bytes or the iteration count is less than 1. </exception>
		/// <exception cref="T:System.ArgumentNullException">The password or salt is <see langword="null" />. </exception>
		// Token: 0x06004505 RID: 17669 RVA: 0x000EA4D9 File Offset: 0x000E86D9
		public Rfc2898DeriveBytes(string password, byte[] salt) : this(password, salt, 1000)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.Rfc2898DeriveBytes" /> class using a password, a salt, and number of iterations to derive the key.</summary>
		/// <param name="password">The password used to derive the key. </param>
		/// <param name="salt">The key salt used to derive the key. </param>
		/// <param name="iterations">The number of iterations for the operation. </param>
		/// <exception cref="T:System.ArgumentException">The specified salt size is smaller than 8 bytes or the iteration count is less than 1. </exception>
		/// <exception cref="T:System.ArgumentNullException">The password or salt is <see langword="null" />. </exception>
		// Token: 0x06004506 RID: 17670 RVA: 0x000EA4E8 File Offset: 0x000E86E8
		public Rfc2898DeriveBytes(string password, byte[] salt, int iterations) : this(new UTF8Encoding(false).GetBytes(password), salt, iterations)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.Rfc2898DeriveBytes" /> class using a password, a salt, and number of iterations to derive the key.</summary>
		/// <param name="password">The password used to derive the key. </param>
		/// <param name="salt">The key salt used to derive the key.</param>
		/// <param name="iterations">The number of iterations for the operation. </param>
		/// <exception cref="T:System.ArgumentException">The specified salt size is smaller than 8 bytes or the iteration count is less than 1. </exception>
		/// <exception cref="T:System.ArgumentNullException">The password or salt is <see langword="null" />. </exception>
		// Token: 0x06004507 RID: 17671 RVA: 0x000EA4FE File Offset: 0x000E86FE
		[SecuritySafeCritical]
		public Rfc2898DeriveBytes(byte[] password, byte[] salt, int iterations)
		{
			this.Salt = salt;
			this.IterationCount = iterations;
			this.m_password = password;
			this.m_hmacsha1 = new HMACSHA1(password);
			this.Initialize();
		}

		/// <summary>Gets or sets the number of iterations for the operation.</summary>
		/// <returns>The number of iterations for the operation.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The number of iterations is less than 1. </exception>
		// Token: 0x17000BA5 RID: 2981
		// (get) Token: 0x06004508 RID: 17672 RVA: 0x000EA52D File Offset: 0x000E872D
		// (set) Token: 0x06004509 RID: 17673 RVA: 0x000EA535 File Offset: 0x000E8735
		public int IterationCount
		{
			get
			{
				return (int)this.m_iterations;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("Positive number required."));
				}
				this.m_iterations = (uint)value;
				this.Initialize();
			}
		}

		/// <summary>Gets or sets the key salt value for the operation.</summary>
		/// <returns>The key salt value for the operation.</returns>
		/// <exception cref="T:System.ArgumentException">The specified salt size is smaller than 8 bytes. </exception>
		/// <exception cref="T:System.ArgumentNullException">The salt is <see langword="null" />. </exception>
		// Token: 0x17000BA6 RID: 2982
		// (get) Token: 0x0600450A RID: 17674 RVA: 0x000EA55D File Offset: 0x000E875D
		// (set) Token: 0x0600450B RID: 17675 RVA: 0x000EA56F File Offset: 0x000E876F
		public byte[] Salt
		{
			get
			{
				return (byte[])this.m_salt.Clone();
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (value.Length < 8)
				{
					throw new ArgumentException(Environment.GetResourceString("Salt is not at least eight bytes."));
				}
				this.m_salt = (byte[])value.Clone();
				this.Initialize();
			}
		}

		/// <summary>Returns the pseudo-random key for this object.</summary>
		/// <param name="cb">The number of pseudo-random key bytes to generate. </param>
		/// <returns>A byte array filled with pseudo-random key bytes.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="cb " />is out of range. This parameter requires a non-negative number.</exception>
		// Token: 0x0600450C RID: 17676 RVA: 0x000EA5AC File Offset: 0x000E87AC
		public override byte[] GetBytes(int cb)
		{
			if (cb <= 0)
			{
				throw new ArgumentOutOfRangeException("cb", Environment.GetResourceString("Positive number required."));
			}
			byte[] array = new byte[cb];
			int i = 0;
			int num = this.m_endIndex - this.m_startIndex;
			if (num > 0)
			{
				if (cb < num)
				{
					Buffer.InternalBlockCopy(this.m_buffer, this.m_startIndex, array, 0, cb);
					this.m_startIndex += cb;
					return array;
				}
				Buffer.InternalBlockCopy(this.m_buffer, this.m_startIndex, array, 0, num);
				this.m_startIndex = (this.m_endIndex = 0);
				i += num;
			}
			while (i < cb)
			{
				byte[] src = this.Func();
				int num2 = cb - i;
				if (num2 <= 20)
				{
					Buffer.InternalBlockCopy(src, 0, array, i, num2);
					i += num2;
					Buffer.InternalBlockCopy(src, num2, this.m_buffer, this.m_startIndex, 20 - num2);
					this.m_endIndex += 20 - num2;
					return array;
				}
				Buffer.InternalBlockCopy(src, 0, array, i, 20);
				i += 20;
			}
			return array;
		}

		/// <summary>Resets the state of the operation.</summary>
		// Token: 0x0600450D RID: 17677 RVA: 0x000EA6B4 File Offset: 0x000E88B4
		public override void Reset()
		{
			this.Initialize();
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Security.Cryptography.Rfc2898DeriveBytes" /> class and optionally releases the managed resources.</summary>
		/// <param name="disposing">
		///       <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
		// Token: 0x0600450E RID: 17678 RVA: 0x000EA6BC File Offset: 0x000E88BC
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (disposing)
			{
				if (this.m_hmacsha1 != null)
				{
					((IDisposable)this.m_hmacsha1).Dispose();
				}
				if (this.m_buffer != null)
				{
					Array.Clear(this.m_buffer, 0, this.m_buffer.Length);
				}
				if (this.m_salt != null)
				{
					Array.Clear(this.m_salt, 0, this.m_salt.Length);
				}
			}
		}

		// Token: 0x0600450F RID: 17679 RVA: 0x000EA720 File Offset: 0x000E8920
		private void Initialize()
		{
			if (this.m_buffer != null)
			{
				Array.Clear(this.m_buffer, 0, this.m_buffer.Length);
			}
			this.m_buffer = new byte[20];
			this.m_block = 1U;
			this.m_startIndex = (this.m_endIndex = 0);
		}

		// Token: 0x06004510 RID: 17680 RVA: 0x000EA770 File Offset: 0x000E8970
		private byte[] Func()
		{
			byte[] array = Utils.Int(this.m_block);
			this.m_hmacsha1.TransformBlock(this.m_salt, 0, this.m_salt.Length, null, 0);
			this.m_hmacsha1.TransformBlock(array, 0, array.Length, null, 0);
			this.m_hmacsha1.TransformFinalBlock(EmptyArray<byte>.Value, 0, 0);
			byte[] hashValue = this.m_hmacsha1.HashValue;
			this.m_hmacsha1.Initialize();
			byte[] array2 = hashValue;
			int num = 2;
			while ((long)num <= (long)((ulong)this.m_iterations))
			{
				this.m_hmacsha1.TransformBlock(hashValue, 0, hashValue.Length, null, 0);
				this.m_hmacsha1.TransformFinalBlock(EmptyArray<byte>.Value, 0, 0);
				hashValue = this.m_hmacsha1.HashValue;
				for (int i = 0; i < 20; i++)
				{
					byte[] array3 = array2;
					int num2 = i;
					array3[num2] ^= hashValue[i];
				}
				this.m_hmacsha1.Initialize();
				num++;
			}
			this.m_block += 1U;
			return array2;
		}

		/// <summary>Derives a cryptographic key from the <see cref="T:System.Security.Cryptography.Rfc2898DeriveBytes" /> object.</summary>
		/// <param name="algname">The algorithm name for which to derive the key.</param>
		/// <param name="alghashname">The hash algorithm name to use to derive the key.</param>
		/// <param name="keySize">The size of the key, in bits, to derive.</param>
		/// <param name="rgbIV">The initialization vector (IV) to use to derive the key.</param>
		/// <returns>The derived key.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The <paramref name="keySize" /> parameter is incorrect.-or- The cryptographic service provider (CSP) cannot be acquired.-or- The <paramref name="algname" /> parameter is not a valid algorithm name.-or- The <paramref name="alghashname" /> parameter is not a valid hash algorithm name. </exception>
		// Token: 0x06004511 RID: 17681 RVA: 0x000E9E55 File Offset: 0x000E8055
		[SecuritySafeCritical]
		public byte[] CryptDeriveKey(string algname, string alghashname, int keySize, byte[] rgbIV)
		{
			if (keySize < 0)
			{
				throw new CryptographicException(Environment.GetResourceString("Specified key is not a valid size for this algorithm."));
			}
			throw new NotSupportedException("CspParameters are not supported by Mono");
		}

		// Token: 0x04002386 RID: 9094
		private byte[] m_buffer;

		// Token: 0x04002387 RID: 9095
		private byte[] m_salt;

		// Token: 0x04002388 RID: 9096
		private HMACSHA1 m_hmacsha1;

		// Token: 0x04002389 RID: 9097
		private byte[] m_password;

		// Token: 0x0400238A RID: 9098
		private uint m_iterations;

		// Token: 0x0400238B RID: 9099
		private uint m_block;

		// Token: 0x0400238C RID: 9100
		private int m_startIndex;

		// Token: 0x0400238D RID: 9101
		private int m_endIndex;

		// Token: 0x0400238E RID: 9102
		private const int BlockSize = 20;
	}
}
