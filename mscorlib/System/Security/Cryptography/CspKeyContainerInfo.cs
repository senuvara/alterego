﻿using System;
using System.Runtime.InteropServices;
using System.Security.AccessControl;

namespace System.Security.Cryptography
{
	/// <summary>Provides additional information about a cryptographic key pair. This class cannot be inherited.</summary>
	// Token: 0x020006AE RID: 1710
	[ComVisible(true)]
	public sealed class CspKeyContainerInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CspKeyContainerInfo" /> class using the specified parameters.</summary>
		/// <param name="parameters">A <see cref="T:System.Security.Cryptography.CspParameters" /> object that provides information about the key.</param>
		// Token: 0x06004667 RID: 18023 RVA: 0x000F4362 File Offset: 0x000F2562
		public CspKeyContainerInfo(CspParameters parameters)
		{
			this._params = parameters;
			this._random = true;
		}

		/// <summary>Gets a value indicating whether a key in a key container is accessible.</summary>
		/// <returns>
		///     <see langword="true" /> if the key is accessible; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotSupportedException">The key type is not supported.</exception>
		// Token: 0x17000BD6 RID: 3030
		// (get) Token: 0x06004668 RID: 18024 RVA: 0x00004E08 File Offset: 0x00003008
		public bool Accessible
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets a <see cref="T:System.Security.AccessControl.CryptoKeySecurity" /> object that represents access rights and audit rules for a container. </summary>
		/// <returns>A <see cref="T:System.Security.AccessControl.CryptoKeySecurity" /> object that represents access rights and audit rules for a container.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key type is not supported.</exception>
		/// <exception cref="T:System.NotSupportedException">The cryptographic service provider cannot be found.-or-The key container was not found.</exception>
		// Token: 0x17000BD7 RID: 3031
		// (get) Token: 0x06004669 RID: 18025 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public CryptoKeySecurity CryptoKeySecurity
		{
			get
			{
				return null;
			}
		}

		/// <summary>Gets a value indicating whether a key can be exported from a key container.</summary>
		/// <returns>
		///     <see langword="true" /> if the key can be exported; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotSupportedException">The key type is not supported.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The cryptographic service provider cannot be found.-or-The key container was not found.</exception>
		// Token: 0x17000BD8 RID: 3032
		// (get) Token: 0x0600466A RID: 18026 RVA: 0x00004E08 File Offset: 0x00003008
		public bool Exportable
		{
			get
			{
				return true;
			}
		}

		/// <summary>Gets a value indicating whether a key is a hardware key.</summary>
		/// <returns>
		///     <see langword="true" /> if the key is a hardware key; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The cryptographic service provider cannot be found.</exception>
		// Token: 0x17000BD9 RID: 3033
		// (get) Token: 0x0600466B RID: 18027 RVA: 0x00002526 File Offset: 0x00000726
		public bool HardwareDevice
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a key container name.</summary>
		/// <returns>The key container name.</returns>
		// Token: 0x17000BDA RID: 3034
		// (get) Token: 0x0600466C RID: 18028 RVA: 0x000F4378 File Offset: 0x000F2578
		public string KeyContainerName
		{
			get
			{
				return this._params.KeyContainerName;
			}
		}

		/// <summary>Gets a value that describes whether an asymmetric key was created as a signature key or an exchange key.</summary>
		/// <returns>One of the <see cref="T:System.Security.Cryptography.KeyNumber" /> values that describes whether an asymmetric key was created as a signature key or an exchange key.</returns>
		// Token: 0x17000BDB RID: 3035
		// (get) Token: 0x0600466D RID: 18029 RVA: 0x000F4385 File Offset: 0x000F2585
		public KeyNumber KeyNumber
		{
			get
			{
				return (KeyNumber)this._params.KeyNumber;
			}
		}

		/// <summary>Gets a value indicating whether a key is from a machine key set.</summary>
		/// <returns>
		///     <see langword="true" /> if the key is from the machine key set; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BDC RID: 3036
		// (get) Token: 0x0600466E RID: 18030 RVA: 0x00002526 File Offset: 0x00000726
		public bool MachineKeyStore
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value indicating whether a key pair is protected.</summary>
		/// <returns>
		///     <see langword="true" /> if the key pair is protected; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.NotSupportedException">The key type is not supported.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The cryptographic service provider cannot be found.-or-The key container was not found.</exception>
		// Token: 0x17000BDD RID: 3037
		// (get) Token: 0x0600466F RID: 18031 RVA: 0x00002526 File Offset: 0x00000726
		public bool Protected
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets the provider name of a key.</summary>
		/// <returns>The provider name.</returns>
		// Token: 0x17000BDE RID: 3038
		// (get) Token: 0x06004670 RID: 18032 RVA: 0x000F4392 File Offset: 0x000F2592
		public string ProviderName
		{
			get
			{
				return this._params.ProviderName;
			}
		}

		/// <summary>Gets the provider type of a key.</summary>
		/// <returns>The provider type. The default is 1.</returns>
		// Token: 0x17000BDF RID: 3039
		// (get) Token: 0x06004671 RID: 18033 RVA: 0x000F439F File Offset: 0x000F259F
		public int ProviderType
		{
			get
			{
				return this._params.ProviderType;
			}
		}

		/// <summary>Gets a value indicating whether a key container was randomly generated by a managed cryptography class.</summary>
		/// <returns>
		///     <see langword="true" /> if the key container was randomly generated; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000BE0 RID: 3040
		// (get) Token: 0x06004672 RID: 18034 RVA: 0x000F43AC File Offset: 0x000F25AC
		public bool RandomlyGenerated
		{
			get
			{
				return this._random;
			}
		}

		/// <summary>Gets a value indicating whether a key can be removed from a key container.</summary>
		/// <returns>
		///     <see langword="true" /> if the key is removable; otherwise, <see langword="false" />.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The cryptographic service provider (CSP) was not found.</exception>
		// Token: 0x17000BE1 RID: 3041
		// (get) Token: 0x06004673 RID: 18035 RVA: 0x00002526 File Offset: 0x00000726
		public bool Removable
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a unique key container name.</summary>
		/// <returns>The unique key container name.</returns>
		/// <exception cref="T:System.NotSupportedException">The key type is not supported.</exception>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The cryptographic service provider cannot be found.-or-The key container was not found.</exception>
		// Token: 0x17000BE2 RID: 3042
		// (get) Token: 0x06004674 RID: 18036 RVA: 0x000F43B4 File Offset: 0x000F25B4
		public string UniqueKeyContainerName
		{
			get
			{
				return this._params.ProviderName + "\\" + this._params.KeyContainerName;
			}
		}

		// Token: 0x0400243C RID: 9276
		private CspParameters _params;

		// Token: 0x0400243D RID: 9277
		internal bool _random;
	}
}
