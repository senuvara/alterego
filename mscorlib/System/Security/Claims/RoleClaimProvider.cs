﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Security.Claims
{
	// Token: 0x02000653 RID: 1619
	[ComVisible(false)]
	internal class RoleClaimProvider
	{
		// Token: 0x0600438C RID: 17292 RVA: 0x000E5A53 File Offset: 0x000E3C53
		public RoleClaimProvider(string issuer, string[] roles, ClaimsIdentity subject)
		{
			this.m_issuer = issuer;
			this.m_roles = roles;
			this.m_subject = subject;
		}

		// Token: 0x17000B54 RID: 2900
		// (get) Token: 0x0600438D RID: 17293 RVA: 0x000E5A70 File Offset: 0x000E3C70
		public IEnumerable<Claim> Claims
		{
			get
			{
				int num;
				for (int i = 0; i < this.m_roles.Length; i = num + 1)
				{
					if (this.m_roles[i] != null)
					{
						yield return new Claim(this.m_subject.RoleClaimType, this.m_roles[i], "http://www.w3.org/2001/XMLSchema#string", this.m_issuer, this.m_issuer, this.m_subject);
					}
					num = i;
				}
				yield break;
			}
		}

		// Token: 0x040022DF RID: 8927
		private string m_issuer;

		// Token: 0x040022E0 RID: 8928
		private string[] m_roles;

		// Token: 0x040022E1 RID: 8929
		private ClaimsIdentity m_subject;

		// Token: 0x02000654 RID: 1620
		[CompilerGenerated]
		private sealed class <get_Claims>d__5 : IEnumerable<Claim>, IEnumerable, IEnumerator<Claim>, IDisposable, IEnumerator
		{
			// Token: 0x0600438E RID: 17294 RVA: 0x000E5A80 File Offset: 0x000E3C80
			[DebuggerHidden]
			public <get_Claims>d__5(int <>1__state)
			{
				this.<>1__state = <>1__state;
				this.<>l__initialThreadId = Environment.CurrentManagedThreadId;
			}

			// Token: 0x0600438F RID: 17295 RVA: 0x000020D3 File Offset: 0x000002D3
			[DebuggerHidden]
			void IDisposable.Dispose()
			{
			}

			// Token: 0x06004390 RID: 17296 RVA: 0x000E5A9C File Offset: 0x000E3C9C
			bool IEnumerator.MoveNext()
			{
				int num = this.<>1__state;
				RoleClaimProvider roleClaimProvider = this;
				if (num == 0)
				{
					this.<>1__state = -1;
					i = 0;
					goto IL_90;
				}
				if (num != 1)
				{
					return false;
				}
				this.<>1__state = -1;
				IL_80:
				int num2 = i;
				i = num2 + 1;
				IL_90:
				if (i >= roleClaimProvider.m_roles.Length)
				{
					return false;
				}
				if (roleClaimProvider.m_roles[i] != null)
				{
					this.<>2__current = new Claim(roleClaimProvider.m_subject.RoleClaimType, roleClaimProvider.m_roles[i], "http://www.w3.org/2001/XMLSchema#string", roleClaimProvider.m_issuer, roleClaimProvider.m_issuer, roleClaimProvider.m_subject);
					this.<>1__state = 1;
					return true;
				}
				goto IL_80;
			}

			// Token: 0x17000B55 RID: 2901
			// (get) Token: 0x06004391 RID: 17297 RVA: 0x000E5B4A File Offset: 0x000E3D4A
			Claim IEnumerator<Claim>.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06004392 RID: 17298 RVA: 0x000175EA File Offset: 0x000157EA
			[DebuggerHidden]
			void IEnumerator.Reset()
			{
				throw new NotSupportedException();
			}

			// Token: 0x17000B56 RID: 2902
			// (get) Token: 0x06004393 RID: 17299 RVA: 0x000E5B4A File Offset: 0x000E3D4A
			object IEnumerator.Current
			{
				[DebuggerHidden]
				get
				{
					return this.<>2__current;
				}
			}

			// Token: 0x06004394 RID: 17300 RVA: 0x000E5B54 File Offset: 0x000E3D54
			[DebuggerHidden]
			IEnumerator<Claim> IEnumerable<Claim>.GetEnumerator()
			{
				RoleClaimProvider.<get_Claims>d__5 <get_Claims>d__;
				if (this.<>1__state == -2 && this.<>l__initialThreadId == Environment.CurrentManagedThreadId)
				{
					this.<>1__state = 0;
					<get_Claims>d__ = this;
				}
				else
				{
					<get_Claims>d__ = new RoleClaimProvider.<get_Claims>d__5(0);
					<get_Claims>d__.<>4__this = this;
				}
				return <get_Claims>d__;
			}

			// Token: 0x06004395 RID: 17301 RVA: 0x000E5B97 File Offset: 0x000E3D97
			[DebuggerHidden]
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.System.Collections.Generic.IEnumerable<System.Security.Claims.Claim>.GetEnumerator();
			}

			// Token: 0x040022E2 RID: 8930
			private int <>1__state;

			// Token: 0x040022E3 RID: 8931
			private Claim <>2__current;

			// Token: 0x040022E4 RID: 8932
			private int <>l__initialThreadId;

			// Token: 0x040022E5 RID: 8933
			public RoleClaimProvider <>4__this;

			// Token: 0x040022E6 RID: 8934
			private int <i>5__1;
		}
	}
}
