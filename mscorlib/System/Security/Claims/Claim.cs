﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;

namespace System.Security.Claims
{
	/// <summary>Represents a claim.</summary>
	// Token: 0x02000649 RID: 1609
	[Serializable]
	public class Claim
	{
		/// <summary />
		/// <param name="reader" />
		// Token: 0x06004304 RID: 17156 RVA: 0x000E31FC File Offset: 0x000E13FC
		public Claim(BinaryReader reader) : this(reader, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Claims.Claim" /> class with the specified reader, and subject.</summary>
		/// <param name="reader">The binary reader.</param>
		/// <param name="subject">The subject that this claim describes.</param>
		// Token: 0x06004305 RID: 17157 RVA: 0x000E3206 File Offset: 0x000E1406
		public Claim(BinaryReader reader, ClaimsIdentity subject)
		{
			this.m_propertyLock = new object();
			base..ctor();
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			this.Initialize(reader, subject);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Claims.Claim" /> class with the specified claim type, and value.</summary>
		/// <param name="type">The claim type.</param>
		/// <param name="value">The claim value.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06004306 RID: 17158 RVA: 0x000E322F File Offset: 0x000E142F
		public Claim(string type, string value) : this(type, value, "http://www.w3.org/2001/XMLSchema#string", "LOCAL AUTHORITY", "LOCAL AUTHORITY", null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Claims.Claim" /> class with the specified claim type, value, and value type.</summary>
		/// <param name="type">The claim type.</param>
		/// <param name="value">The claim value.</param>
		/// <param name="valueType">The claim value type. If this parameter is <see langword="null" />, then <see cref="F:System.Security.Claims.ClaimValueTypes.String" /> is used.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06004307 RID: 17159 RVA: 0x000E3249 File Offset: 0x000E1449
		public Claim(string type, string value, string valueType) : this(type, value, valueType, "LOCAL AUTHORITY", "LOCAL AUTHORITY", null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Claims.Claim" /> class with the specified claim type, value, value type, and issuer.</summary>
		/// <param name="type">The claim type.</param>
		/// <param name="value">The claim value.</param>
		/// <param name="valueType">The claim value type. If this parameter is <see langword="null" />, then <see cref="F:System.Security.Claims.ClaimValueTypes.String" /> is used.</param>
		/// <param name="issuer">The claim issuer. If this parameter is empty or <see langword="null" />, then <see cref="F:System.Security.Claims.ClaimsIdentity.DefaultIssuer" /> is used.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06004308 RID: 17160 RVA: 0x000E325F File Offset: 0x000E145F
		public Claim(string type, string value, string valueType, string issuer) : this(type, value, valueType, issuer, issuer, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Claims.Claim" /> class with the specified claim type, value, value type, issuer,  and original issuer.</summary>
		/// <param name="type">The claim type.</param>
		/// <param name="value">The claim value.</param>
		/// <param name="valueType">The claim value type. If this parameter is <see langword="null" />, then <see cref="F:System.Security.Claims.ClaimValueTypes.String" /> is used.</param>
		/// <param name="issuer">The claim issuer. If this parameter is empty or <see langword="null" />, then <see cref="F:System.Security.Claims.ClaimsIdentity.DefaultIssuer" /> is used.</param>
		/// <param name="originalIssuer">The original issuer of the claim. If this parameter is empty or <see langword="null" />, then the <see cref="P:System.Security.Claims.Claim.OriginalIssuer" /> property is set to the value of the <see cref="P:System.Security.Claims.Claim.Issuer" /> property.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x06004309 RID: 17161 RVA: 0x000E326F File Offset: 0x000E146F
		public Claim(string type, string value, string valueType, string issuer, string originalIssuer) : this(type, value, valueType, issuer, originalIssuer, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Claims.Claim" /> class with the specified claim type, value, value type, issuer, original issuer and subject.</summary>
		/// <param name="type">The claim type.</param>
		/// <param name="value">The claim value.</param>
		/// <param name="valueType">The claim value type. If this parameter is <see langword="null" />, then <see cref="F:System.Security.Claims.ClaimValueTypes.String" /> is used.</param>
		/// <param name="issuer">The claim issuer. If this parameter is empty or <see langword="null" />, then <see cref="F:System.Security.Claims.ClaimsIdentity.DefaultIssuer" /> is used.</param>
		/// <param name="originalIssuer">The original issuer of the claim. If this parameter is empty or <see langword="null" />, then the <see cref="P:System.Security.Claims.Claim.OriginalIssuer" /> property is set to the value of the <see cref="P:System.Security.Claims.Claim.Issuer" /> property.</param>
		/// <param name="subject">The subject that this claim describes.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="type" /> or <paramref name="value" /> is <see langword="null" />.</exception>
		// Token: 0x0600430A RID: 17162 RVA: 0x000E3280 File Offset: 0x000E1480
		public Claim(string type, string value, string valueType, string issuer, string originalIssuer, ClaimsIdentity subject) : this(type, value, valueType, issuer, originalIssuer, subject, null, null)
		{
		}

		// Token: 0x0600430B RID: 17163 RVA: 0x000E32A0 File Offset: 0x000E14A0
		internal Claim(string type, string value, string valueType, string issuer, string originalIssuer, ClaimsIdentity subject, string propertyKey, string propertyValue)
		{
			this.m_propertyLock = new object();
			base..ctor();
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.m_type = type;
			this.m_value = value;
			if (string.IsNullOrEmpty(valueType))
			{
				this.m_valueType = "http://www.w3.org/2001/XMLSchema#string";
			}
			else
			{
				this.m_valueType = valueType;
			}
			if (string.IsNullOrEmpty(issuer))
			{
				this.m_issuer = "LOCAL AUTHORITY";
			}
			else
			{
				this.m_issuer = issuer;
			}
			if (string.IsNullOrEmpty(originalIssuer))
			{
				this.m_originalIssuer = this.m_issuer;
			}
			else
			{
				this.m_originalIssuer = originalIssuer;
			}
			this.m_subject = subject;
			if (propertyKey != null)
			{
				this.Properties.Add(propertyKey, propertyValue);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Claims.Claim" /> class.</summary>
		/// <param name="other">The security claim.</param>
		// Token: 0x0600430C RID: 17164 RVA: 0x000E335C File Offset: 0x000E155C
		protected Claim(Claim other) : this(other, (other == null) ? null : other.m_subject)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Claims.Claim" /> class with the specified security claim, and subject.</summary>
		/// <param name="other">The security claim.</param>
		/// <param name="subject">The subject that this claim describes.</param>
		// Token: 0x0600430D RID: 17165 RVA: 0x000E3374 File Offset: 0x000E1574
		protected Claim(Claim other, ClaimsIdentity subject)
		{
			this.m_propertyLock = new object();
			base..ctor();
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			this.m_issuer = other.m_issuer;
			this.m_originalIssuer = other.m_originalIssuer;
			this.m_subject = subject;
			this.m_type = other.m_type;
			this.m_value = other.m_value;
			this.m_valueType = other.m_valueType;
			if (other.m_properties != null)
			{
				this.m_properties = new Dictionary<string, string>();
				foreach (string key in other.m_properties.Keys)
				{
					this.m_properties.Add(key, other.m_properties[key]);
				}
			}
			if (other.m_userSerializationData != null)
			{
				this.m_userSerializationData = (other.m_userSerializationData.Clone() as byte[]);
			}
		}

		/// <summary />
		/// <returns>Returns <see cref="T:System.Byte" />.</returns>
		// Token: 0x17000B36 RID: 2870
		// (get) Token: 0x0600430E RID: 17166 RVA: 0x000E3470 File Offset: 0x000E1670
		protected virtual byte[] CustomSerializationData
		{
			get
			{
				return this.m_userSerializationData;
			}
		}

		/// <summary>Gets the issuer of the claim.</summary>
		/// <returns>A name that refers to the issuer of the claim.</returns>
		// Token: 0x17000B37 RID: 2871
		// (get) Token: 0x0600430F RID: 17167 RVA: 0x000E3478 File Offset: 0x000E1678
		public string Issuer
		{
			get
			{
				return this.m_issuer;
			}
		}

		// Token: 0x06004310 RID: 17168 RVA: 0x000E3480 File Offset: 0x000E1680
		[OnDeserialized]
		private void OnDeserializedMethod(StreamingContext context)
		{
			this.m_propertyLock = new object();
		}

		/// <summary>Gets the original issuer of the claim. </summary>
		/// <returns>A name that refers to the original issuer of the claim.</returns>
		// Token: 0x17000B38 RID: 2872
		// (get) Token: 0x06004311 RID: 17169 RVA: 0x000E348D File Offset: 0x000E168D
		public string OriginalIssuer
		{
			get
			{
				return this.m_originalIssuer;
			}
		}

		/// <summary>Gets a dictionary that contains additional properties associated with this claim.</summary>
		/// <returns>A dictionary that contains additional properties associated with the claim. The properties are represented as name-value pairs.</returns>
		// Token: 0x17000B39 RID: 2873
		// (get) Token: 0x06004312 RID: 17170 RVA: 0x000E3498 File Offset: 0x000E1698
		public IDictionary<string, string> Properties
		{
			get
			{
				if (this.m_properties == null)
				{
					object propertyLock = this.m_propertyLock;
					lock (propertyLock)
					{
						if (this.m_properties == null)
						{
							this.m_properties = new Dictionary<string, string>();
						}
					}
				}
				return this.m_properties;
			}
		}

		/// <summary>Gets the subject of the claim.</summary>
		/// <returns>The subject of the claim.</returns>
		// Token: 0x17000B3A RID: 2874
		// (get) Token: 0x06004313 RID: 17171 RVA: 0x000E34F4 File Offset: 0x000E16F4
		// (set) Token: 0x06004314 RID: 17172 RVA: 0x000E34FC File Offset: 0x000E16FC
		public ClaimsIdentity Subject
		{
			get
			{
				return this.m_subject;
			}
			internal set
			{
				this.m_subject = value;
			}
		}

		/// <summary>Gets the claim type of the claim.</summary>
		/// <returns>The claim type.</returns>
		// Token: 0x17000B3B RID: 2875
		// (get) Token: 0x06004315 RID: 17173 RVA: 0x000E3505 File Offset: 0x000E1705
		public string Type
		{
			get
			{
				return this.m_type;
			}
		}

		/// <summary>Gets the value of the claim.</summary>
		/// <returns>The claim value.</returns>
		// Token: 0x17000B3C RID: 2876
		// (get) Token: 0x06004316 RID: 17174 RVA: 0x000E350D File Offset: 0x000E170D
		public string Value
		{
			get
			{
				return this.m_value;
			}
		}

		/// <summary>Gets the value type of the claim.</summary>
		/// <returns>The claim value type.</returns>
		// Token: 0x17000B3D RID: 2877
		// (get) Token: 0x06004317 RID: 17175 RVA: 0x000E3515 File Offset: 0x000E1715
		public string ValueType
		{
			get
			{
				return this.m_valueType;
			}
		}

		/// <summary>Returns a new <see cref="T:System.Security.Claims.Claim" /> object copied from this object. The new claim does not have a subject.</summary>
		/// <returns>The new claim object.</returns>
		// Token: 0x06004318 RID: 17176 RVA: 0x000E351D File Offset: 0x000E171D
		public virtual Claim Clone()
		{
			return this.Clone(null);
		}

		/// <summary>Returns a new <see cref="T:System.Security.Claims.Claim" /> object copied from this object. The subject of the new claim is set to the specified ClaimsIdentity.</summary>
		/// <param name="identity">The intended subject of the new claim.</param>
		/// <returns>The new claim object.</returns>
		// Token: 0x06004319 RID: 17177 RVA: 0x000E3526 File Offset: 0x000E1726
		public virtual Claim Clone(ClaimsIdentity identity)
		{
			return new Claim(this, identity);
		}

		// Token: 0x0600431A RID: 17178 RVA: 0x000E3530 File Offset: 0x000E1730
		private void Initialize(BinaryReader reader, ClaimsIdentity subject)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			this.m_subject = subject;
			Claim.SerializationMask serializationMask = (Claim.SerializationMask)reader.ReadInt32();
			int num = 1;
			int num2 = reader.ReadInt32();
			this.m_value = reader.ReadString();
			if ((serializationMask & Claim.SerializationMask.NameClaimType) == Claim.SerializationMask.NameClaimType)
			{
				this.m_type = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
			}
			else if ((serializationMask & Claim.SerializationMask.RoleClaimType) == Claim.SerializationMask.RoleClaimType)
			{
				this.m_type = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
			}
			else
			{
				this.m_type = reader.ReadString();
				num++;
			}
			if ((serializationMask & Claim.SerializationMask.StringType) == Claim.SerializationMask.StringType)
			{
				this.m_valueType = reader.ReadString();
				num++;
			}
			else
			{
				this.m_valueType = "http://www.w3.org/2001/XMLSchema#string";
			}
			if ((serializationMask & Claim.SerializationMask.Issuer) == Claim.SerializationMask.Issuer)
			{
				this.m_issuer = reader.ReadString();
				num++;
			}
			else
			{
				this.m_issuer = "LOCAL AUTHORITY";
			}
			if ((serializationMask & Claim.SerializationMask.OriginalIssuerEqualsIssuer) == Claim.SerializationMask.OriginalIssuerEqualsIssuer)
			{
				this.m_originalIssuer = this.m_issuer;
			}
			else if ((serializationMask & Claim.SerializationMask.OriginalIssuer) == Claim.SerializationMask.OriginalIssuer)
			{
				this.m_originalIssuer = reader.ReadString();
				num++;
			}
			else
			{
				this.m_originalIssuer = "LOCAL AUTHORITY";
			}
			if ((serializationMask & Claim.SerializationMask.HasProperties) == Claim.SerializationMask.HasProperties)
			{
				int num3 = reader.ReadInt32();
				for (int i = 0; i < num3; i++)
				{
					this.Properties.Add(reader.ReadString(), reader.ReadString());
				}
			}
			if ((serializationMask & Claim.SerializationMask.UserData) == Claim.SerializationMask.UserData)
			{
				int count = reader.ReadInt32();
				this.m_userSerializationData = reader.ReadBytes(count);
				num++;
			}
			for (int j = num; j < num2; j++)
			{
				reader.ReadString();
			}
		}

		/// <summary />
		/// <param name="writer" />
		// Token: 0x0600431B RID: 17179 RVA: 0x000E369A File Offset: 0x000E189A
		public virtual void WriteTo(BinaryWriter writer)
		{
			this.WriteTo(writer, null);
		}

		/// <summary>Writes this <see cref="T:System.Security.Claims.Claim" /> to the writer.</summary>
		/// <param name="writer">The writer to write this claim</param>
		/// <param name="userData">The user data to claim</param>
		// Token: 0x0600431C RID: 17180 RVA: 0x000E36A4 File Offset: 0x000E18A4
		protected virtual void WriteTo(BinaryWriter writer, byte[] userData)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			int num = 1;
			Claim.SerializationMask serializationMask = Claim.SerializationMask.None;
			if (string.Equals(this.m_type, "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"))
			{
				serializationMask |= Claim.SerializationMask.NameClaimType;
			}
			else if (string.Equals(this.m_type, "http://schemas.microsoft.com/ws/2008/06/identity/claims/role"))
			{
				serializationMask |= Claim.SerializationMask.RoleClaimType;
			}
			else
			{
				num++;
			}
			if (!string.Equals(this.m_valueType, "http://www.w3.org/2001/XMLSchema#string", StringComparison.Ordinal))
			{
				num++;
				serializationMask |= Claim.SerializationMask.StringType;
			}
			if (!string.Equals(this.m_issuer, "LOCAL AUTHORITY", StringComparison.Ordinal))
			{
				num++;
				serializationMask |= Claim.SerializationMask.Issuer;
			}
			if (string.Equals(this.m_originalIssuer, this.m_issuer, StringComparison.Ordinal))
			{
				serializationMask |= Claim.SerializationMask.OriginalIssuerEqualsIssuer;
			}
			else if (!string.Equals(this.m_originalIssuer, "LOCAL AUTHORITY", StringComparison.Ordinal))
			{
				num++;
				serializationMask |= Claim.SerializationMask.OriginalIssuer;
			}
			if (this.Properties.Count > 0)
			{
				num++;
				serializationMask |= Claim.SerializationMask.HasProperties;
			}
			if (userData != null && userData.Length != 0)
			{
				num++;
				serializationMask |= Claim.SerializationMask.UserData;
			}
			writer.Write((int)serializationMask);
			writer.Write(num);
			writer.Write(this.m_value);
			if ((serializationMask & Claim.SerializationMask.NameClaimType) != Claim.SerializationMask.NameClaimType && (serializationMask & Claim.SerializationMask.RoleClaimType) != Claim.SerializationMask.RoleClaimType)
			{
				writer.Write(this.m_type);
			}
			if ((serializationMask & Claim.SerializationMask.StringType) == Claim.SerializationMask.StringType)
			{
				writer.Write(this.m_valueType);
			}
			if ((serializationMask & Claim.SerializationMask.Issuer) == Claim.SerializationMask.Issuer)
			{
				writer.Write(this.m_issuer);
			}
			if ((serializationMask & Claim.SerializationMask.OriginalIssuer) == Claim.SerializationMask.OriginalIssuer)
			{
				writer.Write(this.m_originalIssuer);
			}
			if ((serializationMask & Claim.SerializationMask.HasProperties) == Claim.SerializationMask.HasProperties)
			{
				writer.Write(this.Properties.Count);
				foreach (string text in this.Properties.Keys)
				{
					writer.Write(text);
					writer.Write(this.Properties[text]);
				}
			}
			if ((serializationMask & Claim.SerializationMask.UserData) == Claim.SerializationMask.UserData)
			{
				writer.Write(userData.Length);
				writer.Write(userData);
			}
			writer.Flush();
		}

		/// <summary>Returns a string representation of this <see cref="T:System.Security.Claims.Claim" /> object.</summary>
		/// <returns>The string representation of this <see cref="T:System.Security.Claims.Claim" /> object.</returns>
		// Token: 0x0600431D RID: 17181 RVA: 0x000E388C File Offset: 0x000E1A8C
		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "{0}: {1}", this.m_type, this.m_value);
		}

		// Token: 0x04002236 RID: 8758
		private string m_issuer;

		// Token: 0x04002237 RID: 8759
		private string m_originalIssuer;

		// Token: 0x04002238 RID: 8760
		private string m_type;

		// Token: 0x04002239 RID: 8761
		private string m_value;

		// Token: 0x0400223A RID: 8762
		private string m_valueType;

		// Token: 0x0400223B RID: 8763
		[NonSerialized]
		private byte[] m_userSerializationData;

		// Token: 0x0400223C RID: 8764
		private Dictionary<string, string> m_properties;

		// Token: 0x0400223D RID: 8765
		[NonSerialized]
		private object m_propertyLock;

		// Token: 0x0400223E RID: 8766
		[NonSerialized]
		private ClaimsIdentity m_subject;

		// Token: 0x0200064A RID: 1610
		private enum SerializationMask
		{
			// Token: 0x04002240 RID: 8768
			None,
			// Token: 0x04002241 RID: 8769
			NameClaimType,
			// Token: 0x04002242 RID: 8770
			RoleClaimType,
			// Token: 0x04002243 RID: 8771
			StringType = 4,
			// Token: 0x04002244 RID: 8772
			Issuer = 8,
			// Token: 0x04002245 RID: 8773
			OriginalIssuerEqualsIssuer = 16,
			// Token: 0x04002246 RID: 8774
			OriginalIssuer = 32,
			// Token: 0x04002247 RID: 8775
			HasProperties = 64,
			// Token: 0x04002248 RID: 8776
			UserData = 128
		}
	}
}
