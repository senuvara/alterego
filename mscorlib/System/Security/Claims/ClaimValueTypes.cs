﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Claims
{
	/// <summary>Defines claim value types according to the type URIs defined by W3C and OASIS. This class cannot be inherited.</summary>
	// Token: 0x0200064C RID: 1612
	[ComVisible(false)]
	public static class ClaimValueTypes
	{
		// Token: 0x04002282 RID: 8834
		private const string XmlSchemaNamespace = "http://www.w3.org/2001/XMLSchema";

		/// <summary>A URI that represents the <see langword="base64Binary" /> XML data type.</summary>
		// Token: 0x04002283 RID: 8835
		public const string Base64Binary = "http://www.w3.org/2001/XMLSchema#base64Binary";

		/// <summary>A URI that that represents the <see langword="base64Octet" /> XML data type.</summary>
		// Token: 0x04002284 RID: 8836
		public const string Base64Octet = "http://www.w3.org/2001/XMLSchema#base64Octet";

		/// <summary>A URI that represents the <see langword="boolean" /> XML data type.</summary>
		// Token: 0x04002285 RID: 8837
		public const string Boolean = "http://www.w3.org/2001/XMLSchema#boolean";

		/// <summary>A URI that represents the <see langword="date" /> XML data type.</summary>
		// Token: 0x04002286 RID: 8838
		public const string Date = "http://www.w3.org/2001/XMLSchema#date";

		/// <summary>A URI that represents the <see langword="dateTime" /> XML data type.</summary>
		// Token: 0x04002287 RID: 8839
		public const string DateTime = "http://www.w3.org/2001/XMLSchema#dateTime";

		/// <summary>A URI that represents the <see langword="double" /> XML data type.</summary>
		// Token: 0x04002288 RID: 8840
		public const string Double = "http://www.w3.org/2001/XMLSchema#double";

		/// <summary>A URI that represents the <see langword="fqbn" /> XML data type.</summary>
		// Token: 0x04002289 RID: 8841
		public const string Fqbn = "http://www.w3.org/2001/XMLSchema#fqbn";

		/// <summary>A URI that represents the <see langword="hexBinary" /> XML data type.</summary>
		// Token: 0x0400228A RID: 8842
		public const string HexBinary = "http://www.w3.org/2001/XMLSchema#hexBinary";

		/// <summary>A URI that represents the <see langword="integer" /> XML data type.</summary>
		// Token: 0x0400228B RID: 8843
		public const string Integer = "http://www.w3.org/2001/XMLSchema#integer";

		/// <summary>A URI that represents the <see langword="integer32" /> XML data type.</summary>
		// Token: 0x0400228C RID: 8844
		public const string Integer32 = "http://www.w3.org/2001/XMLSchema#integer32";

		/// <summary>A URI that represents the <see langword="integer64" /> XML data type.</summary>
		// Token: 0x0400228D RID: 8845
		public const string Integer64 = "http://www.w3.org/2001/XMLSchema#integer64";

		/// <summary>A URI that represents the <see langword="sid" /> XML data type.</summary>
		// Token: 0x0400228E RID: 8846
		public const string Sid = "http://www.w3.org/2001/XMLSchema#sid";

		/// <summary>A URI that represents the <see langword="string" /> XML data type.</summary>
		// Token: 0x0400228F RID: 8847
		public const string String = "http://www.w3.org/2001/XMLSchema#string";

		/// <summary>A URI that represents the <see langword="time" /> XML data type.</summary>
		// Token: 0x04002290 RID: 8848
		public const string Time = "http://www.w3.org/2001/XMLSchema#time";

		/// <summary>A URI that represents the <see langword="uinteger32" /> XML data type.</summary>
		// Token: 0x04002291 RID: 8849
		public const string UInteger32 = "http://www.w3.org/2001/XMLSchema#uinteger32";

		/// <summary>A URI that represents the <see langword="uinteger64" /> XML data type.</summary>
		// Token: 0x04002292 RID: 8850
		public const string UInteger64 = "http://www.w3.org/2001/XMLSchema#uinteger64";

		// Token: 0x04002293 RID: 8851
		private const string SoapSchemaNamespace = "http://schemas.xmlsoap.org/";

		/// <summary>A URI that represents the <see langword="dns" /> SOAP data type.</summary>
		// Token: 0x04002294 RID: 8852
		public const string DnsName = "http://schemas.xmlsoap.org/claims/dns";

		/// <summary>A URI that represents the <see langword="emailaddress" /> SOAP data type.</summary>
		// Token: 0x04002295 RID: 8853
		public const string Email = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";

		/// <summary>A URI that represents the <see langword="rsa" /> SOAP data type.</summary>
		// Token: 0x04002296 RID: 8854
		public const string Rsa = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/rsa";

		/// <summary>A URI that represents the <see langword="UPN" /> SOAP data type.</summary>
		// Token: 0x04002297 RID: 8855
		public const string UpnName = "http://schemas.xmlsoap.org/claims/UPN";

		// Token: 0x04002298 RID: 8856
		private const string XmlSignatureConstantsNamespace = "http://www.w3.org/2000/09/xmldsig#";

		/// <summary>A URI that represents the <see langword="DSAKeyValue" /> XML Signature data type.</summary>
		// Token: 0x04002299 RID: 8857
		public const string DsaKeyValue = "http://www.w3.org/2000/09/xmldsig#DSAKeyValue";

		/// <summary>A URI that represents the <see langword="KeyInfo" /> XML Signature data type.</summary>
		// Token: 0x0400229A RID: 8858
		public const string KeyInfo = "http://www.w3.org/2000/09/xmldsig#KeyInfo";

		/// <summary>A URI that represents the <see langword="RSAKeyValue" /> XML Signature data type.</summary>
		// Token: 0x0400229B RID: 8859
		public const string RsaKeyValue = "http://www.w3.org/2000/09/xmldsig#RSAKeyValue";

		// Token: 0x0400229C RID: 8860
		private const string XQueryOperatorsNameSpace = "http://www.w3.org/TR/2002/WD-xquery-operators-20020816";

		/// <summary>A URI that represents the <see langword="daytimeDuration" /> XQuery data type.</summary>
		// Token: 0x0400229D RID: 8861
		public const string DaytimeDuration = "http://www.w3.org/TR/2002/WD-xquery-operators-20020816#dayTimeDuration";

		/// <summary>A URI that represents the <see langword="yearMonthDuration" /> XQuery data type.</summary>
		// Token: 0x0400229E RID: 8862
		public const string YearMonthDuration = "http://www.w3.org/TR/2002/WD-xquery-operators-20020816#yearMonthDuration";

		// Token: 0x0400229F RID: 8863
		private const string Xacml10Namespace = "urn:oasis:names:tc:xacml:1.0";

		/// <summary>A URI that represents the <see langword="rfc822Name" /> XACML 1.0 data type.</summary>
		// Token: 0x040022A0 RID: 8864
		public const string Rfc822Name = "urn:oasis:names:tc:xacml:1.0:data-type:rfc822Name";

		/// <summary>A URI that represents the <see langword="x500Name" /> XACML 1.0 data type.</summary>
		// Token: 0x040022A1 RID: 8865
		public const string X500Name = "urn:oasis:names:tc:xacml:1.0:data-type:x500Name";
	}
}
