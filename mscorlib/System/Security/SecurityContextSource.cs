﻿using System;

namespace System.Security
{
	/// <summary>Identifies the source for the security context.</summary>
	// Token: 0x0200054F RID: 1359
	public enum SecurityContextSource
	{
		/// <summary>The current application domain is the source for the security context.</summary>
		// Token: 0x04001E67 RID: 7783
		CurrentAppDomain,
		/// <summary>The current assembly is the source for the security context.</summary>
		// Token: 0x04001E68 RID: 7784
		CurrentAssembly
	}
}
