﻿using System;

namespace System.Security
{
	/// <summary>Specifies the scope of a <see cref="T:System.Security.SecurityCriticalAttribute" />.</summary>
	// Token: 0x02000548 RID: 1352
	[Obsolete("SecurityCriticalScope is only used for .NET 2.0 transparency compatibility.")]
	public enum SecurityCriticalScope
	{
		/// <summary>The attribute applies only to the immediate target.</summary>
		// Token: 0x04001E5D RID: 7773
		Explicit,
		/// <summary>The attribute applies to all code that follows it.</summary>
		// Token: 0x04001E5E RID: 7774
		Everything
	}
}
