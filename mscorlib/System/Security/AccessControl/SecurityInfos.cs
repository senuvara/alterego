﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the section of a security descriptor to be queried or set.</summary>
	// Token: 0x02000629 RID: 1577
	[Flags]
	public enum SecurityInfos
	{
		/// <summary>Specifies the owner identifier.</summary>
		// Token: 0x0400213F RID: 8511
		Owner = 1,
		/// <summary>Specifies the primary group identifier.</summary>
		// Token: 0x04002140 RID: 8512
		Group = 2,
		/// <summary>Specifies the discretionary access control list (DACL).</summary>
		// Token: 0x04002141 RID: 8513
		DiscretionaryAcl = 4,
		/// <summary>Specifies the system access control list (SACL).</summary>
		// Token: 0x04002142 RID: 8514
		SystemAcl = 8
	}
}
