﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	/// <summary>Represents a combination of a user's identity, an access mask, and an access control type (allow or deny). An <see cref="T:System.Security.AccessControl.ObjectAccessRule" /> object also contains information about the type of object to which the rule applies, the type of child object that can inherit the rule, how the rule is inherited by child objects, and how that inheritance is propagated.</summary>
	// Token: 0x02000618 RID: 1560
	public abstract class ObjectAccessRule : AccessRule
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.AccessControl.ObjectAccessRule" /> class with the specified values.</summary>
		/// <param name="identity">The identity to which the access rule applies.  It must be an object that can be cast as a <see cref="T:System.Security.Principal.SecurityIdentifier" />.</param>
		/// <param name="accessMask">The access mask of this rule. The access mask is a 32-bit collection of anonymous bits, the meaning of which is defined by the individual integrators.</param>
		/// <param name="isInherited">
		///       <see langword="true" /> if this rule is inherited from a parent container.</param>
		/// <param name="inheritanceFlags">Specifies the inheritance properties of the access rule.</param>
		/// <param name="propagationFlags">Specifies whether inherited access rules are automatically propagated. The propagation flags are ignored if <paramref name="inheritanceFlags" /> is set to <see cref="F:System.Security.AccessControl.InheritanceFlags.None" />.</param>
		/// <param name="objectType">The type of object to which the rule applies.</param>
		/// <param name="inheritedObjectType">The type of child object that can inherit the rule.</param>
		/// <param name="type">Specifies whether this rule allows or denies access.</param>
		/// <exception cref="T:System.ArgumentException">The value of the <paramref name="identity" /> parameter cannot be cast as a <see cref="T:System.Security.Principal.SecurityIdentifier" />, or the <paramref name="type" /> parameter contains an invalid value.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value of the <paramref name="accessMask" /> parameter is 0, or the <paramref name="inheritanceFlags" /> or <paramref name="propagationFlags" /> parameters contain unrecognized flag values.</exception>
		// Token: 0x06004152 RID: 16722 RVA: 0x000DD8BD File Offset: 0x000DBABD
		protected ObjectAccessRule(IdentityReference identity, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, Guid objectType, Guid inheritedObjectType, AccessControlType type) : base(identity, accessMask, isInherited, inheritanceFlags, propagationFlags, type)
		{
			this.object_type = objectType;
			this.inherited_object_type = inheritedObjectType;
		}

		/// <summary>Gets the type of child object that can inherit the <see cref="T:System.Security.AccessControl.ObjectAccessRule" /> object.</summary>
		/// <returns>The type of child object that can inherit the <see cref="T:System.Security.AccessControl.ObjectAccessRule" /> object.</returns>
		// Token: 0x17000AD4 RID: 2772
		// (get) Token: 0x06004153 RID: 16723 RVA: 0x000DD8DE File Offset: 0x000DBADE
		public Guid InheritedObjectType
		{
			get
			{
				return this.inherited_object_type;
			}
		}

		/// <summary>Gets flags that specify if the <see cref="P:System.Security.AccessControl.ObjectAccessRule.ObjectType" /> and <see cref="P:System.Security.AccessControl.ObjectAccessRule.InheritedObjectType" /> properties of the <see cref="T:System.Security.AccessControl.ObjectAccessRule" /> object contain valid values.</summary>
		/// <returns>
		///     <see cref="F:System.Security.AccessControl.ObjectAceFlags.ObjectAceTypePresent" /> specifies that the <see cref="P:System.Security.AccessControl.ObjectAccessRule.ObjectType" /> property contains a valid value. <see cref="F:System.Security.AccessControl.ObjectAceFlags.InheritedObjectAceTypePresent" /> specifies that the <see cref="P:System.Security.AccessControl.ObjectAccessRule.InheritedObjectType" /> property contains a valid value. These values can be combined with a logical OR.</returns>
		// Token: 0x17000AD5 RID: 2773
		// (get) Token: 0x06004154 RID: 16724 RVA: 0x000DD8E8 File Offset: 0x000DBAE8
		public ObjectAceFlags ObjectFlags
		{
			get
			{
				ObjectAceFlags objectAceFlags = ObjectAceFlags.None;
				if (this.object_type != Guid.Empty)
				{
					objectAceFlags |= ObjectAceFlags.ObjectAceTypePresent;
				}
				if (this.inherited_object_type != Guid.Empty)
				{
					objectAceFlags |= ObjectAceFlags.InheritedObjectAceTypePresent;
				}
				return objectAceFlags;
			}
		}

		/// <summary>Gets the type of object to which the <see cref="T:System.Security.AccessControl.ObjectAccessRule" /> applies.</summary>
		/// <returns>The type of object to which the <see cref="T:System.Security.AccessControl.ObjectAccessRule" /> applies.</returns>
		// Token: 0x17000AD6 RID: 2774
		// (get) Token: 0x06004155 RID: 16725 RVA: 0x000DD924 File Offset: 0x000DBB24
		public Guid ObjectType
		{
			get
			{
				return this.object_type;
			}
		}

		// Token: 0x04002102 RID: 8450
		private Guid object_type;

		// Token: 0x04002103 RID: 8451
		private Guid inherited_object_type;
	}
}
