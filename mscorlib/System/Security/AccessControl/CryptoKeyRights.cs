﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the cryptographic key operation for which an authorization rule controls access or auditing.</summary>
	// Token: 0x020005F8 RID: 1528
	[Flags]
	public enum CryptoKeyRights
	{
		/// <summary>Read the key data.</summary>
		// Token: 0x040020B3 RID: 8371
		ReadData = 1,
		/// <summary>Write key data.</summary>
		// Token: 0x040020B4 RID: 8372
		WriteData = 2,
		/// <summary>Read extended attributes of the key.</summary>
		// Token: 0x040020B5 RID: 8373
		ReadExtendedAttributes = 8,
		/// <summary>Write extended attributes of the key.</summary>
		// Token: 0x040020B6 RID: 8374
		WriteExtendedAttributes = 16,
		/// <summary>Read attributes of the key.</summary>
		// Token: 0x040020B7 RID: 8375
		ReadAttributes = 128,
		/// <summary>Write attributes of the key.</summary>
		// Token: 0x040020B8 RID: 8376
		WriteAttributes = 256,
		/// <summary>Delete the key.</summary>
		// Token: 0x040020B9 RID: 8377
		Delete = 65536,
		/// <summary>Read permissions for the key.</summary>
		// Token: 0x040020BA RID: 8378
		ReadPermissions = 131072,
		/// <summary>Change permissions for the key.</summary>
		// Token: 0x040020BB RID: 8379
		ChangePermissions = 262144,
		/// <summary>Take ownership of the key.</summary>
		// Token: 0x040020BC RID: 8380
		TakeOwnership = 524288,
		/// <summary>Use the key for synchronization.</summary>
		// Token: 0x040020BD RID: 8381
		Synchronize = 1048576,
		/// <summary>Full control of the key.</summary>
		// Token: 0x040020BE RID: 8382
		FullControl = 2032027,
		/// <summary>A combination of <see cref="F:System.Security.AccessControl.CryptoKeyRights.GenericRead" /> and <see cref="F:System.Security.AccessControl.CryptoKeyRights.GenericWrite" />.</summary>
		// Token: 0x040020BF RID: 8383
		GenericAll = 268435456,
		/// <summary>Not used.</summary>
		// Token: 0x040020C0 RID: 8384
		GenericExecute = 536870912,
		/// <summary>Write the key data, extended attributes of the key, attributes of the key, and permissions for the key.</summary>
		// Token: 0x040020C1 RID: 8385
		GenericWrite = 1073741824,
		/// <summary>Read the key data, extended attributes of the key, attributes of the key, and permissions for the key.</summary>
		// Token: 0x040020C2 RID: 8386
		GenericRead = -2147483648
	}
}
