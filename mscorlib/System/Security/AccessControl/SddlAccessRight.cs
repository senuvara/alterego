﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Security.AccessControl
{
	// Token: 0x02000628 RID: 1576
	internal class SddlAccessRight
	{
		// Token: 0x17000B07 RID: 2823
		// (get) Token: 0x0600420E RID: 16910 RVA: 0x000DF190 File Offset: 0x000DD390
		// (set) Token: 0x0600420F RID: 16911 RVA: 0x000DF198 File Offset: 0x000DD398
		public string Name
		{
			[CompilerGenerated]
			get
			{
				return this.<Name>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Name>k__BackingField = value;
			}
		}

		// Token: 0x17000B08 RID: 2824
		// (get) Token: 0x06004210 RID: 16912 RVA: 0x000DF1A1 File Offset: 0x000DD3A1
		// (set) Token: 0x06004211 RID: 16913 RVA: 0x000DF1A9 File Offset: 0x000DD3A9
		public int Value
		{
			[CompilerGenerated]
			get
			{
				return this.<Value>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<Value>k__BackingField = value;
			}
		}

		// Token: 0x17000B09 RID: 2825
		// (get) Token: 0x06004212 RID: 16914 RVA: 0x000DF1B2 File Offset: 0x000DD3B2
		// (set) Token: 0x06004213 RID: 16915 RVA: 0x000DF1BA File Offset: 0x000DD3BA
		public int ObjectType
		{
			[CompilerGenerated]
			get
			{
				return this.<ObjectType>k__BackingField;
			}
			[CompilerGenerated]
			set
			{
				this.<ObjectType>k__BackingField = value;
			}
		}

		// Token: 0x06004214 RID: 16916 RVA: 0x000DF1C4 File Offset: 0x000DD3C4
		public static SddlAccessRight LookupByName(string s)
		{
			foreach (SddlAccessRight sddlAccessRight in SddlAccessRight.rights)
			{
				if (sddlAccessRight.Name == s)
				{
					return sddlAccessRight;
				}
			}
			return null;
		}

		// Token: 0x06004215 RID: 16917 RVA: 0x000DF1FC File Offset: 0x000DD3FC
		public static SddlAccessRight[] Decompose(int mask)
		{
			foreach (SddlAccessRight sddlAccessRight in SddlAccessRight.rights)
			{
				if (mask == sddlAccessRight.Value)
				{
					return new SddlAccessRight[]
					{
						sddlAccessRight
					};
				}
			}
			int num = 0;
			List<SddlAccessRight> list = new List<SddlAccessRight>();
			int num2 = 0;
			foreach (SddlAccessRight sddlAccessRight2 in SddlAccessRight.rights)
			{
				if ((mask & sddlAccessRight2.Value) == sddlAccessRight2.Value && (num2 | sddlAccessRight2.Value) != num2)
				{
					if (num == 0)
					{
						num = sddlAccessRight2.ObjectType;
					}
					if (sddlAccessRight2.ObjectType != 0 && num != sddlAccessRight2.ObjectType)
					{
						return null;
					}
					list.Add(sddlAccessRight2);
					num2 |= sddlAccessRight2.Value;
				}
				if (num2 == mask)
				{
					return list.ToArray();
				}
			}
			return null;
		}

		// Token: 0x06004216 RID: 16918 RVA: 0x00002050 File Offset: 0x00000250
		public SddlAccessRight()
		{
		}

		// Token: 0x06004217 RID: 16919 RVA: 0x000DF2C4 File Offset: 0x000DD4C4
		// Note: this type is marked as 'beforefieldinit'.
		static SddlAccessRight()
		{
		}

		// Token: 0x0400213A RID: 8506
		[CompilerGenerated]
		private string <Name>k__BackingField;

		// Token: 0x0400213B RID: 8507
		[CompilerGenerated]
		private int <Value>k__BackingField;

		// Token: 0x0400213C RID: 8508
		[CompilerGenerated]
		private int <ObjectType>k__BackingField;

		// Token: 0x0400213D RID: 8509
		private static readonly SddlAccessRight[] rights = new SddlAccessRight[]
		{
			new SddlAccessRight
			{
				Name = "CC",
				Value = 1,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "DC",
				Value = 2,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "LC",
				Value = 4,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "SW",
				Value = 8,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "RP",
				Value = 16,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "WP",
				Value = 32,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "DT",
				Value = 64,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "LO",
				Value = 128,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "CR",
				Value = 256,
				ObjectType = 1
			},
			new SddlAccessRight
			{
				Name = "SD",
				Value = 65536
			},
			new SddlAccessRight
			{
				Name = "RC",
				Value = 131072
			},
			new SddlAccessRight
			{
				Name = "WD",
				Value = 262144
			},
			new SddlAccessRight
			{
				Name = "WO",
				Value = 524288
			},
			new SddlAccessRight
			{
				Name = "GA",
				Value = 268435456
			},
			new SddlAccessRight
			{
				Name = "GX",
				Value = 536870912
			},
			new SddlAccessRight
			{
				Name = "GW",
				Value = 1073741824
			},
			new SddlAccessRight
			{
				Name = "GR",
				Value = int.MinValue
			},
			new SddlAccessRight
			{
				Name = "FA",
				Value = 2032127,
				ObjectType = 2
			},
			new SddlAccessRight
			{
				Name = "FR",
				Value = 1179785,
				ObjectType = 2
			},
			new SddlAccessRight
			{
				Name = "FW",
				Value = 1179926,
				ObjectType = 2
			},
			new SddlAccessRight
			{
				Name = "FX",
				Value = 1179808,
				ObjectType = 2
			},
			new SddlAccessRight
			{
				Name = "KA",
				Value = 983103,
				ObjectType = 3
			},
			new SddlAccessRight
			{
				Name = "KR",
				Value = 131097,
				ObjectType = 3
			},
			new SddlAccessRight
			{
				Name = "KW",
				Value = 131078,
				ObjectType = 3
			},
			new SddlAccessRight
			{
				Name = "KX",
				Value = 131097,
				ObjectType = 3
			},
			new SddlAccessRight
			{
				Name = "NW",
				Value = 1
			},
			new SddlAccessRight
			{
				Name = "NR",
				Value = 2
			},
			new SddlAccessRight
			{
				Name = "NX",
				Value = 4
			}
		};
	}
}
