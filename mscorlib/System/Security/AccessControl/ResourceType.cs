﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the defined native object types.</summary>
	// Token: 0x02000627 RID: 1575
	public enum ResourceType
	{
		/// <summary>An unknown object type.</summary>
		// Token: 0x0400212D RID: 8493
		Unknown,
		/// <summary>A file or directory.</summary>
		// Token: 0x0400212E RID: 8494
		FileObject,
		/// <summary>A Windows service.</summary>
		// Token: 0x0400212F RID: 8495
		Service,
		/// <summary>A printer.</summary>
		// Token: 0x04002130 RID: 8496
		Printer,
		/// <summary>A registry key.</summary>
		// Token: 0x04002131 RID: 8497
		RegistryKey,
		/// <summary>A network share.</summary>
		// Token: 0x04002132 RID: 8498
		LMShare,
		/// <summary>A local kernel object.</summary>
		// Token: 0x04002133 RID: 8499
		KernelObject,
		/// <summary>A window station or desktop object on the local computer.</summary>
		// Token: 0x04002134 RID: 8500
		WindowObject,
		/// <summary>A directory service (DS) object or a property set or property of a directory service object.</summary>
		// Token: 0x04002135 RID: 8501
		DSObject,
		/// <summary>A directory service object and all of its property sets and properties.</summary>
		// Token: 0x04002136 RID: 8502
		DSObjectAll,
		/// <summary>An object defined by a provider.</summary>
		// Token: 0x04002137 RID: 8503
		ProviderDefined,
		/// <summary>A Windows Management Instrumentation (WMI) object.</summary>
		// Token: 0x04002138 RID: 8504
		WmiGuidObject,
		/// <summary>An object for a registry entry under WOW64.</summary>
		// Token: 0x04002139 RID: 8505
		RegistryWow6432Key
	}
}
