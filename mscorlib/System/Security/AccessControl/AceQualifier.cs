﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the function of an access control entry (ACE).</summary>
	// Token: 0x020005E2 RID: 1506
	public enum AceQualifier
	{
		/// <summary>Allow access.</summary>
		// Token: 0x0400205D RID: 8285
		AccessAllowed,
		/// <summary>Deny access.</summary>
		// Token: 0x0400205E RID: 8286
		AccessDenied,
		/// <summary>Cause a system audit.</summary>
		// Token: 0x0400205F RID: 8287
		SystemAudit,
		/// <summary>Cause a system alarm.</summary>
		// Token: 0x04002060 RID: 8288
		SystemAlarm
	}
}
