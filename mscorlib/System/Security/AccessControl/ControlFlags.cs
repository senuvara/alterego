﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>These flags affect the security descriptor behavior.</summary>
	// Token: 0x020005F5 RID: 1525
	[Flags]
	public enum ControlFlags
	{
		/// <summary>No control flags.</summary>
		// Token: 0x040020A1 RID: 8353
		None = 0,
		/// <summary>Specifies that the owner <see cref="T:System.Security.Principal.SecurityIdentifier" /> was obtained by a defaulting mechanism. Set by resource managers only; should not be set by callers.  </summary>
		// Token: 0x040020A2 RID: 8354
		OwnerDefaulted = 1,
		/// <summary>Specifies that the group <see cref="T:System.Security.Principal.SecurityIdentifier" /> was obtained by a defaulting mechanism. Set by resource managers only; should not be set by callers.</summary>
		// Token: 0x040020A3 RID: 8355
		GroupDefaulted = 2,
		/// <summary>Specifies that the DACL is not <see langword="null" />. Set by resource managers or users.  </summary>
		// Token: 0x040020A4 RID: 8356
		DiscretionaryAclPresent = 4,
		/// <summary>Specifies that the DACL was obtained by a defaulting mechanism. Set by resource managers only.</summary>
		// Token: 0x040020A5 RID: 8357
		DiscretionaryAclDefaulted = 8,
		/// <summary>Specifies that the SACL is not <see langword="null" />. Set by resource managers or users.</summary>
		// Token: 0x040020A6 RID: 8358
		SystemAclPresent = 16,
		/// <summary>Specifies that the SACL was obtained by a defaulting mechanism. Set by resource managers only.</summary>
		// Token: 0x040020A7 RID: 8359
		SystemAclDefaulted = 32,
		/// <summary>Ignored.</summary>
		// Token: 0x040020A8 RID: 8360
		DiscretionaryAclUntrusted = 64,
		/// <summary>Ignored.</summary>
		// Token: 0x040020A9 RID: 8361
		ServerSecurity = 128,
		/// <summary>Ignored.</summary>
		// Token: 0x040020AA RID: 8362
		DiscretionaryAclAutoInheritRequired = 256,
		/// <summary>Ignored.</summary>
		// Token: 0x040020AB RID: 8363
		SystemAclAutoInheritRequired = 512,
		/// <summary>Specifies that the Discretionary Access Control List (DACL) has been automatically inherited from the parent. Set by resource managers only.</summary>
		// Token: 0x040020AC RID: 8364
		DiscretionaryAclAutoInherited = 1024,
		/// <summary>Specifies that the System Access Control List (SACL) has been automatically inherited from the parent. Set by resource managers only.</summary>
		// Token: 0x040020AD RID: 8365
		SystemAclAutoInherited = 2048,
		/// <summary>Specifies that the resource manager prevents auto-inheritance. Set by resource managers or users.  </summary>
		// Token: 0x040020AE RID: 8366
		DiscretionaryAclProtected = 4096,
		/// <summary>Specifies that the resource manager prevents auto-inheritance. Set by resource managers or users.</summary>
		// Token: 0x040020AF RID: 8367
		SystemAclProtected = 8192,
		/// <summary>Specifies that the contents of the Reserved field are valid.</summary>
		// Token: 0x040020B0 RID: 8368
		RMControlValid = 16384,
		/// <summary>Specifies that the security descriptor binary representation is in the self-relative format.  This flag is always set.</summary>
		// Token: 0x040020B1 RID: 8369
		SelfRelative = 32768
	}
}
