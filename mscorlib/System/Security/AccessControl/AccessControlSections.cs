﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies which sections of a security descriptor to save or load.</summary>
	// Token: 0x020005DC RID: 1500
	[Flags]
	public enum AccessControlSections
	{
		/// <summary>No sections.</summary>
		// Token: 0x04002045 RID: 8261
		None = 0,
		/// <summary>The system access control list (SACL).</summary>
		// Token: 0x04002046 RID: 8262
		Audit = 1,
		/// <summary>The discretionary access control list (DACL).</summary>
		// Token: 0x04002047 RID: 8263
		Access = 2,
		/// <summary>The owner.</summary>
		// Token: 0x04002048 RID: 8264
		Owner = 4,
		/// <summary>The primary group.</summary>
		// Token: 0x04002049 RID: 8265
		Group = 8,
		/// <summary>The entire security descriptor.</summary>
		// Token: 0x0400204A RID: 8266
		All = 15
	}
}
