﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the presence of object types for Access Control Entries (ACEs).</summary>
	// Token: 0x0200061A RID: 1562
	[Flags]
	public enum ObjectAceFlags
	{
		/// <summary>No object types are present.</summary>
		// Token: 0x04002108 RID: 8456
		None = 0,
		/// <summary>The type of object that is associated with the ACE is present.</summary>
		// Token: 0x04002109 RID: 8457
		ObjectAceTypePresent = 1,
		/// <summary>The type of object that can inherit the ACE.</summary>
		// Token: 0x0400210A RID: 8458
		InheritedObjectAceTypePresent = 2
	}
}
