﻿using System;
using System.Globalization;
using System.Security.Principal;
using System.Text;
using Unity;

namespace System.Security.AccessControl
{
	/// <summary>Encapsulates all Access Control Entry (ACE) types currently defined by Microsoft Corporation. All <see cref="T:System.Security.AccessControl.KnownAce" /> objects contain a 32-bit access mask and a <see cref="T:System.Security.Principal.SecurityIdentifier" /> object.</summary>
	// Token: 0x0200060B RID: 1547
	public abstract class KnownAce : GenericAce
	{
		// Token: 0x060040FD RID: 16637 RVA: 0x000DD0EE File Offset: 0x000DB2EE
		internal KnownAce(AceType type, AceFlags flags) : base(type, flags)
		{
		}

		// Token: 0x060040FE RID: 16638 RVA: 0x000DD0F8 File Offset: 0x000DB2F8
		internal KnownAce(byte[] binaryForm, int offset) : base(binaryForm, offset)
		{
		}

		/// <summary>Gets or sets the access mask for this <see cref="T:System.Security.AccessControl.KnownAce" /> object.</summary>
		/// <returns>The access mask for this <see cref="T:System.Security.AccessControl.KnownAce" /> object.</returns>
		// Token: 0x17000ACC RID: 2764
		// (get) Token: 0x060040FF RID: 16639 RVA: 0x000DD102 File Offset: 0x000DB302
		// (set) Token: 0x06004100 RID: 16640 RVA: 0x000DD10A File Offset: 0x000DB30A
		public int AccessMask
		{
			get
			{
				return this.access_mask;
			}
			set
			{
				this.access_mask = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Security.Principal.SecurityIdentifier" /> object associated with this <see cref="T:System.Security.AccessControl.KnownAce" /> object.</summary>
		/// <returns>The <see cref="T:System.Security.Principal.SecurityIdentifier" /> object associated with this <see cref="T:System.Security.AccessControl.KnownAce" /> object.</returns>
		// Token: 0x17000ACD RID: 2765
		// (get) Token: 0x06004101 RID: 16641 RVA: 0x000DD113 File Offset: 0x000DB313
		// (set) Token: 0x06004102 RID: 16642 RVA: 0x000DD11B File Offset: 0x000DB31B
		public SecurityIdentifier SecurityIdentifier
		{
			get
			{
				return this.identifier;
			}
			set
			{
				this.identifier = value;
			}
		}

		// Token: 0x06004103 RID: 16643 RVA: 0x000DD124 File Offset: 0x000DB324
		internal static string GetSddlAccessRights(int accessMask)
		{
			string sddlAliasRights = KnownAce.GetSddlAliasRights(accessMask);
			if (!string.IsNullOrEmpty(sddlAliasRights))
			{
				return sddlAliasRights;
			}
			return string.Format(CultureInfo.InvariantCulture, "0x{0:x}", accessMask);
		}

		// Token: 0x06004104 RID: 16644 RVA: 0x000DD158 File Offset: 0x000DB358
		private static string GetSddlAliasRights(int accessMask)
		{
			SddlAccessRight[] array = SddlAccessRight.Decompose(accessMask);
			if (array == null)
			{
				return null;
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (SddlAccessRight sddlAccessRight in array)
			{
				stringBuilder.Append(sddlAccessRight.Name);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06004105 RID: 16645 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal KnownAce()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x040020EE RID: 8430
		private int access_mask;

		// Token: 0x040020EF RID: 8431
		private SecurityIdentifier identifier;
	}
}
