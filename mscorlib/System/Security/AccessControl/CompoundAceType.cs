﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the type of a <see cref="T:System.Security.AccessControl.CompoundAce" /> object.</summary>
	// Token: 0x020005F4 RID: 1524
	public enum CompoundAceType
	{
		/// <summary>The <see cref="T:System.Security.AccessControl.CompoundAce" /> object is used for impersonation.</summary>
		// Token: 0x0400209F RID: 8351
		Impersonation = 1
	}
}
