﻿using System;
using System.Collections;

namespace System.Security.AccessControl
{
	/// <summary>Represents a collection of <see cref="T:System.Security.AccessControl.AuthorizationRule" /> objects.</summary>
	// Token: 0x020005E8 RID: 1512
	public sealed class AuthorizationRuleCollection : ReadOnlyCollectionBase
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.AccessControl.AuthorizationRuleCollection" /> class.</summary>
		// Token: 0x06003FBB RID: 16315 RVA: 0x000DA22B File Offset: 0x000D842B
		public AuthorizationRuleCollection()
		{
		}

		// Token: 0x06003FBC RID: 16316 RVA: 0x000DA233 File Offset: 0x000D8433
		internal AuthorizationRuleCollection(AuthorizationRule[] rules)
		{
			base.InnerList.AddRange(rules);
		}

		/// <summary>Adds an <see cref="T:System.Web.Configuration.AuthorizationRule" /> object to the collection.</summary>
		/// <param name="rule">The <see cref="T:System.Web.Configuration.AuthorizationRule" /> object to add to the collection.</param>
		// Token: 0x06003FBD RID: 16317 RVA: 0x000DA247 File Offset: 0x000D8447
		public void AddRule(AuthorizationRule rule)
		{
			base.InnerList.Add(rule);
		}

		/// <summary>Gets the <see cref="T:System.Security.AccessControl.AuthorizationRule" /> object at the specified index of the collection.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.Security.AccessControl.AuthorizationRule" /> object to get.</param>
		/// <returns>The <see cref="T:System.Security.AccessControl.AuthorizationRule" /> object at the specified index.</returns>
		// Token: 0x17000A8B RID: 2699
		public AuthorizationRule this[int index]
		{
			get
			{
				return (AuthorizationRule)base.InnerList[index];
			}
		}

		/// <summary>Copies the contents of the collection to an array.</summary>
		/// <param name="rules">An array to which to copy the contents of the collection.</param>
		/// <param name="index">The zero-based index from which to begin copying.</param>
		// Token: 0x06003FBF RID: 16319 RVA: 0x000DA269 File Offset: 0x000D8469
		public void CopyTo(AuthorizationRule[] rules, int index)
		{
			base.InnerList.CopyTo(rules, index);
		}
	}
}
