﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the inheritance and auditing behavior of an access control entry (ACE).</summary>
	// Token: 0x020005E1 RID: 1505
	[Flags]
	public enum AceFlags : byte
	{
		/// <summary>No ACE flags are set.</summary>
		// Token: 0x04002052 RID: 8274
		None = 0,
		/// <summary>The access mask is propagated onto child leaf objects.</summary>
		// Token: 0x04002053 RID: 8275
		ObjectInherit = 1,
		/// <summary>The access mask is propagated to child container objects.</summary>
		// Token: 0x04002054 RID: 8276
		ContainerInherit = 2,
		/// <summary>The access checks do not apply to the object; they only apply to its children.</summary>
		// Token: 0x04002055 RID: 8277
		NoPropagateInherit = 4,
		/// <summary>The access mask is propagated only to child objects. This includes both container and leaf child objects.</summary>
		// Token: 0x04002056 RID: 8278
		InheritOnly = 8,
		/// <summary>A logical <see langword="OR" /> of <see cref="F:System.Security.AccessControl.AceFlags.ObjectInherit" />, <see cref="F:System.Security.AccessControl.AceFlags.ContainerInherit" />, <see cref="F:System.Security.AccessControl.AceFlags.NoPropagateInherit" />, and <see cref="F:System.Security.AccessControl.AceFlags.InheritOnly" />.</summary>
		// Token: 0x04002057 RID: 8279
		InheritanceFlags = 15,
		/// <summary>An ACE is inherited from a parent container rather than being explicitly set for an object.</summary>
		// Token: 0x04002058 RID: 8280
		Inherited = 16,
		/// <summary>Successful access attempts are audited.</summary>
		// Token: 0x04002059 RID: 8281
		SuccessfulAccess = 64,
		/// <summary>Failed access attempts are audited.</summary>
		// Token: 0x0400205A RID: 8282
		FailedAccess = 128,
		/// <summary>All access attempts are audited.</summary>
		// Token: 0x0400205B RID: 8283
		AuditFlags = 192
	}
}
