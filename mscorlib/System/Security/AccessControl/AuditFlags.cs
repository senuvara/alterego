﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the conditions for auditing attempts to access a securable object.</summary>
	// Token: 0x020005E4 RID: 1508
	[Flags]
	public enum AuditFlags
	{
		/// <summary>No access attempts are to be audited.</summary>
		// Token: 0x04002075 RID: 8309
		None = 0,
		/// <summary>Successful access attempts are to be audited.</summary>
		// Token: 0x04002076 RID: 8310
		Success = 1,
		/// <summary>Failed access attempts are to be audited.</summary>
		// Token: 0x04002077 RID: 8311
		Failure = 2
	}
}
