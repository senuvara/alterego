﻿using System;
using System.Collections;
using Unity;

namespace System.Security.AccessControl
{
	/// <summary>Provides the ability to iterate through the access control entries (ACEs) in an access control list (ACL). </summary>
	// Token: 0x020005E0 RID: 1504
	public sealed class AceEnumerator : IEnumerator
	{
		// Token: 0x06003FA6 RID: 16294 RVA: 0x000DA073 File Offset: 0x000D8273
		internal AceEnumerator(GenericAcl owner)
		{
			this.current = -1;
			base..ctor();
			this.owner = owner;
		}

		/// <summary>Gets the current element in the <see cref="T:System.Security.AccessControl.GenericAce" /> collection. This property gets the type-friendly version of the object. </summary>
		/// <returns>The current element in the <see cref="T:System.Security.AccessControl.GenericAce" /> collection.</returns>
		// Token: 0x17000A82 RID: 2690
		// (get) Token: 0x06003FA7 RID: 16295 RVA: 0x000DA089 File Offset: 0x000D8289
		public GenericAce Current
		{
			get
			{
				if (this.current >= 0)
				{
					return this.owner[this.current];
				}
				return null;
			}
		}

		/// <summary>Gets the current element in the collection.</summary>
		/// <returns>Returns the current element in the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created.</exception>
		// Token: 0x17000A83 RID: 2691
		// (get) Token: 0x06003FA8 RID: 16296 RVA: 0x000DA0A7 File Offset: 0x000D82A7
		object IEnumerator.Current
		{
			get
			{
				return this.Current;
			}
		}

		/// <summary>Advances the enumerator to the next element of the <see cref="T:System.Security.AccessControl.GenericAce" /> collection.</summary>
		/// <returns>
		///     <see langword="true" /> if the enumerator was successfully advanced to the next element; <see langword="false" /> if the enumerator has passed the end of the collection.</returns>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created.</exception>
		// Token: 0x06003FA9 RID: 16297 RVA: 0x000DA0AF File Offset: 0x000D82AF
		public bool MoveNext()
		{
			if (this.current + 1 == this.owner.Count)
			{
				return false;
			}
			this.current++;
			return true;
		}

		/// <summary>Sets the enumerator to its initial position, which is before the first element in the <see cref="T:System.Security.AccessControl.GenericAce" /> collection.</summary>
		/// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created.</exception>
		// Token: 0x06003FAA RID: 16298 RVA: 0x000DA0D7 File Offset: 0x000D82D7
		public void Reset()
		{
			this.current = -1;
		}

		// Token: 0x06003FAB RID: 16299 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal AceEnumerator()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400204F RID: 8271
		private GenericAcl owner;

		// Token: 0x04002050 RID: 8272
		private int current;
	}
}
