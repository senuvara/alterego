﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using Unity;

namespace System.Security.AccessControl
{
	/// <summary>Represents an access control list (ACL) and is the base class for the <see cref="T:System.Security.AccessControl.DiscretionaryAcl" /> and <see cref="T:System.Security.AccessControl.SystemAcl" /> classes.</summary>
	// Token: 0x020005EA RID: 1514
	public abstract class CommonAcl : GenericAcl
	{
		// Token: 0x06003FC8 RID: 16328 RVA: 0x000DA4C4 File Offset: 0x000D86C4
		internal CommonAcl(bool isContainer, bool isDS, RawAcl rawAcl)
		{
			if (rawAcl == null)
			{
				rawAcl = new RawAcl(isDS ? GenericAcl.AclRevisionDS : GenericAcl.AclRevision, 10);
			}
			else
			{
				byte[] binaryForm = new byte[rawAcl.BinaryLength];
				rawAcl.GetBinaryForm(binaryForm, 0);
				rawAcl = new RawAcl(binaryForm, 0);
			}
			this.Init(isContainer, isDS, rawAcl);
		}

		// Token: 0x06003FC9 RID: 16329 RVA: 0x000DA51A File Offset: 0x000D871A
		internal CommonAcl(bool isContainer, bool isDS, byte revision, int capacity)
		{
			this.Init(isContainer, isDS, new RawAcl(revision, capacity));
		}

		// Token: 0x06003FCA RID: 16330 RVA: 0x000DA532 File Offset: 0x000D8732
		internal CommonAcl(bool isContainer, bool isDS, int capacity) : this(isContainer, isDS, isDS ? GenericAcl.AclRevisionDS : GenericAcl.AclRevision, capacity)
		{
		}

		// Token: 0x06003FCB RID: 16331 RVA: 0x000DA54C File Offset: 0x000D874C
		private void Init(bool isContainer, bool isDS, RawAcl rawAcl)
		{
			this.is_container = isContainer;
			this.is_ds = isDS;
			this.raw_acl = rawAcl;
			this.CanonicalizeAndClearAefa();
		}

		/// <summary>Gets the length, in bytes, of the binary representation of the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object. This length should be used before marshaling the access control list (ACL) into a binary array by using the <see cref="M:System.Security.AccessControl.CommonAcl.GetBinaryForm(System.Byte[],System.Int32)" /> method.</summary>
		/// <returns>The length, in bytes, of the binary representation of the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object.</returns>
		// Token: 0x17000A8D RID: 2701
		// (get) Token: 0x06003FCC RID: 16332 RVA: 0x000DA569 File Offset: 0x000D8769
		public sealed override int BinaryLength
		{
			get
			{
				return this.raw_acl.BinaryLength;
			}
		}

		/// <summary>Gets the number of access control entries (ACEs) in the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object.</summary>
		/// <returns>The number of ACEs in the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object.</returns>
		// Token: 0x17000A8E RID: 2702
		// (get) Token: 0x06003FCD RID: 16333 RVA: 0x000DA576 File Offset: 0x000D8776
		public sealed override int Count
		{
			get
			{
				return this.raw_acl.Count;
			}
		}

		/// <summary>Gets a Boolean value that specifies whether the access control entries (ACEs) in the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object are in canonical order.</summary>
		/// <returns>
		///     <see langword="true" /> if the ACEs in the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object are in canonical order; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A8F RID: 2703
		// (get) Token: 0x06003FCE RID: 16334 RVA: 0x000DA583 File Offset: 0x000D8783
		public bool IsCanonical
		{
			get
			{
				return this.is_canonical;
			}
		}

		/// <summary>Sets whether the <see cref="T:System.Security.AccessControl.CommonAcl" /> object is a container. </summary>
		/// <returns>
		///     <see langword="true" /> if the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object is a container.</returns>
		// Token: 0x17000A90 RID: 2704
		// (get) Token: 0x06003FCF RID: 16335 RVA: 0x000DA58B File Offset: 0x000D878B
		public bool IsContainer
		{
			get
			{
				return this.is_container;
			}
		}

		/// <summary>Sets whether the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object is a directory object access control list (ACL).</summary>
		/// <returns>
		///     <see langword="true" /> if the current <see cref="T:System.Security.AccessControl.CommonAcl" /> object is a directory object ACL.</returns>
		// Token: 0x17000A91 RID: 2705
		// (get) Token: 0x06003FD0 RID: 16336 RVA: 0x000DA593 File Offset: 0x000D8793
		public bool IsDS
		{
			get
			{
				return this.is_ds;
			}
		}

		// Token: 0x17000A92 RID: 2706
		// (get) Token: 0x06003FD1 RID: 16337 RVA: 0x000DA59B File Offset: 0x000D879B
		// (set) Token: 0x06003FD2 RID: 16338 RVA: 0x000DA5A3 File Offset: 0x000D87A3
		internal bool IsAefa
		{
			get
			{
				return this.is_aefa;
			}
			set
			{
				this.is_aefa = value;
			}
		}

		/// <summary>Gets the revision level of the <see cref="T:System.Security.AccessControl.CommonAcl" />.</summary>
		/// <returns>A byte value that specifies the revision level of the <see cref="T:System.Security.AccessControl.CommonAcl" />.</returns>
		// Token: 0x17000A93 RID: 2707
		// (get) Token: 0x06003FD3 RID: 16339 RVA: 0x000DA5AC File Offset: 0x000D87AC
		public sealed override byte Revision
		{
			get
			{
				return this.raw_acl.Revision;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Security.AccessControl.CommonAce" /> at the specified index.</summary>
		/// <param name="index">The zero-based index of the <see cref="T:System.Security.AccessControl.CommonAce" /> to get or set.</param>
		/// <returns>The <see cref="T:System.Security.AccessControl.CommonAce" /> at the specified index.</returns>
		// Token: 0x17000A94 RID: 2708
		public sealed override GenericAce this[int index]
		{
			get
			{
				return CommonAcl.CopyAce(this.raw_acl[index]);
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>Marshals the contents of the <see cref="T:System.Security.AccessControl.CommonAcl" /> object into the specified byte array beginning at the specified offset.</summary>
		/// <param name="binaryForm">The byte array into which the contents of the <see cref="T:System.Security.AccessControl.CommonAcl" /> is marshaled.</param>
		/// <param name="offset">The offset at which to start marshaling.</param>
		// Token: 0x06003FD6 RID: 16342 RVA: 0x000DA5CC File Offset: 0x000D87CC
		public sealed override void GetBinaryForm(byte[] binaryForm, int offset)
		{
			this.raw_acl.GetBinaryForm(binaryForm, offset);
		}

		/// <summary>Removes all access control entries (ACEs) contained by this <see cref="T:System.Security.AccessControl.CommonAcl" /> object that are associated with the specified <see cref="T:System.Security.Principal.SecurityIdentifier" /> object.</summary>
		/// <param name="sid">The <see cref="T:System.Security.Principal.SecurityIdentifier" /> object to check for.</param>
		// Token: 0x06003FD7 RID: 16343 RVA: 0x000DA5DC File Offset: 0x000D87DC
		public void Purge(SecurityIdentifier sid)
		{
			this.RequireCanonicity();
			this.RemoveAces<KnownAce>((KnownAce ace) => ace.SecurityIdentifier == sid);
		}

		/// <summary>Removes all inherited access control entries (ACEs) from this <see cref="T:System.Security.AccessControl.CommonAcl" /> object.</summary>
		// Token: 0x06003FD8 RID: 16344 RVA: 0x000DA60E File Offset: 0x000D880E
		public void RemoveInheritedAces()
		{
			this.RequireCanonicity();
			this.RemoveAces<GenericAce>((GenericAce ace) => ace.IsInherited);
		}

		// Token: 0x06003FD9 RID: 16345 RVA: 0x000DA63B File Offset: 0x000D883B
		internal void RequireCanonicity()
		{
			if (!this.IsCanonical)
			{
				throw new InvalidOperationException("ACL is not canonical.");
			}
		}

		// Token: 0x06003FDA RID: 16346 RVA: 0x000DA650 File Offset: 0x000D8850
		internal void CanonicalizeAndClearAefa()
		{
			this.RemoveAces<GenericAce>(new CommonAcl.RemoveAcesCallback<GenericAce>(this.IsAceMeaningless));
			this.is_canonical = this.TestCanonicity();
			if (this.IsCanonical)
			{
				this.ApplyCanonicalSortToExplicitAces();
				this.MergeExplicitAces();
			}
			this.IsAefa = false;
		}

		// Token: 0x06003FDB RID: 16347 RVA: 0x000DA68C File Offset: 0x000D888C
		internal virtual bool IsAceMeaningless(GenericAce ace)
		{
			AceFlags aceFlags = ace.AceFlags;
			KnownAce knownAce = ace as KnownAce;
			if (knownAce != null)
			{
				if (knownAce.AccessMask == 0)
				{
					return true;
				}
				if ((aceFlags & AceFlags.InheritOnly) != AceFlags.None)
				{
					if (knownAce is ObjectAce)
					{
						return true;
					}
					if (!this.IsContainer)
					{
						return true;
					}
					if ((aceFlags & (AceFlags.ObjectInherit | AceFlags.ContainerInherit)) == AceFlags.None)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06003FDC RID: 16348 RVA: 0x000DA6DC File Offset: 0x000D88DC
		private bool TestCanonicity()
		{
			AceEnumerator enumerator = base.GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (!(enumerator.Current is QualifiedAce))
				{
					return false;
				}
			}
			bool flag = false;
			enumerator = base.GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (((QualifiedAce)enumerator.Current).IsInherited)
				{
					flag = true;
				}
				else if (flag)
				{
					return false;
				}
			}
			bool flag2 = false;
			foreach (GenericAce genericAce in this)
			{
				QualifiedAce qualifiedAce = (QualifiedAce)genericAce;
				if (qualifiedAce.IsInherited)
				{
					break;
				}
				if (qualifiedAce.AceQualifier == AceQualifier.AccessAllowed)
				{
					flag2 = true;
				}
				else if (AceQualifier.AccessDenied == qualifiedAce.AceQualifier && flag2)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06003FDD RID: 16349 RVA: 0x000DA77C File Offset: 0x000D897C
		internal int GetCanonicalExplicitDenyAceCount()
		{
			int num = 0;
			while (num < this.Count && !this.raw_acl[num].IsInherited)
			{
				QualifiedAce qualifiedAce = this.raw_acl[num] as QualifiedAce;
				if (qualifiedAce == null || qualifiedAce.AceQualifier != AceQualifier.AccessDenied)
				{
					break;
				}
				num++;
			}
			return num;
		}

		// Token: 0x06003FDE RID: 16350 RVA: 0x000DA7D4 File Offset: 0x000D89D4
		internal int GetCanonicalExplicitAceCount()
		{
			int num = 0;
			while (num < this.Count && !this.raw_acl[num].IsInherited)
			{
				num++;
			}
			return num;
		}

		// Token: 0x06003FDF RID: 16351 RVA: 0x000DA808 File Offset: 0x000D8A08
		private void MergeExplicitAces()
		{
			int num = this.GetCanonicalExplicitAceCount();
			int i = 0;
			while (i < num - 1)
			{
				GenericAce genericAce = this.MergeExplicitAcePair(this.raw_acl[i], this.raw_acl[i + 1]);
				if (null != genericAce)
				{
					this.raw_acl[i] = genericAce;
					this.raw_acl.RemoveAce(i + 1);
					num--;
				}
				else
				{
					i++;
				}
			}
		}

		// Token: 0x06003FE0 RID: 16352 RVA: 0x000DA878 File Offset: 0x000D8A78
		private GenericAce MergeExplicitAcePair(GenericAce ace1, GenericAce ace2)
		{
			QualifiedAce qualifiedAce = ace1 as QualifiedAce;
			QualifiedAce qualifiedAce2 = ace2 as QualifiedAce;
			if (!(null != qualifiedAce) || !(null != qualifiedAce2))
			{
				return null;
			}
			if (qualifiedAce.AceQualifier != qualifiedAce2.AceQualifier)
			{
				return null;
			}
			if (!(qualifiedAce.SecurityIdentifier == qualifiedAce2.SecurityIdentifier))
			{
				return null;
			}
			AceFlags aceFlags = qualifiedAce.AceFlags;
			AceFlags aceFlags2 = qualifiedAce2.AceFlags;
			int accessMask = qualifiedAce.AccessMask;
			int accessMask2 = qualifiedAce2.AccessMask;
			if (!this.IsContainer)
			{
				aceFlags &= ~(AceFlags.ObjectInherit | AceFlags.ContainerInherit | AceFlags.NoPropagateInherit | AceFlags.InheritOnly);
				aceFlags2 &= ~(AceFlags.ObjectInherit | AceFlags.ContainerInherit | AceFlags.NoPropagateInherit | AceFlags.InheritOnly);
			}
			AceFlags aceFlags3;
			int accessMask3;
			if (aceFlags != aceFlags2)
			{
				if (accessMask != accessMask2)
				{
					return null;
				}
				if ((aceFlags & ~(AceFlags.ObjectInherit | AceFlags.ContainerInherit)) == (aceFlags2 & ~(AceFlags.ObjectInherit | AceFlags.ContainerInherit)))
				{
					aceFlags3 = (aceFlags | aceFlags2);
					accessMask3 = accessMask;
				}
				else
				{
					if ((aceFlags & ~(AceFlags.SuccessfulAccess | AceFlags.FailedAccess)) != (aceFlags2 & ~(AceFlags.SuccessfulAccess | AceFlags.FailedAccess)))
					{
						return null;
					}
					aceFlags3 = (aceFlags | aceFlags2);
					accessMask3 = accessMask;
				}
			}
			else
			{
				aceFlags3 = aceFlags;
				accessMask3 = (accessMask | accessMask2);
			}
			CommonAce commonAce = ace1 as CommonAce;
			CommonAce right = ace2 as CommonAce;
			if (null != commonAce && null != right)
			{
				return new CommonAce(aceFlags3, commonAce.AceQualifier, accessMask3, commonAce.SecurityIdentifier, commonAce.IsCallback, commonAce.GetOpaque());
			}
			ObjectAce objectAce = ace1 as ObjectAce;
			ObjectAce objectAce2 = ace2 as ObjectAce;
			if (null != objectAce && null != objectAce2)
			{
				Guid a;
				Guid a2;
				CommonAcl.GetObjectAceTypeGuids(objectAce, out a, out a2);
				Guid b;
				Guid b2;
				CommonAcl.GetObjectAceTypeGuids(objectAce2, out b, out b2);
				if (a == b && a2 == b2)
				{
					return new ObjectAce(aceFlags3, objectAce.AceQualifier, accessMask3, objectAce.SecurityIdentifier, objectAce.ObjectAceFlags, objectAce.ObjectAceType, objectAce.InheritedObjectAceType, objectAce.IsCallback, objectAce.GetOpaque());
				}
			}
			return null;
		}

		// Token: 0x06003FE1 RID: 16353 RVA: 0x000DAA20 File Offset: 0x000D8C20
		private static void GetObjectAceTypeGuids(ObjectAce ace, out Guid type, out Guid inheritedType)
		{
			type = Guid.Empty;
			inheritedType = Guid.Empty;
			if ((ace.ObjectAceFlags & ObjectAceFlags.ObjectAceTypePresent) != ObjectAceFlags.None)
			{
				type = ace.ObjectAceType;
			}
			if ((ace.ObjectAceFlags & ObjectAceFlags.InheritedObjectAceTypePresent) != ObjectAceFlags.None)
			{
				inheritedType = ace.InheritedObjectAceType;
			}
		}

		// Token: 0x06003FE2 RID: 16354
		internal abstract void ApplyCanonicalSortToExplicitAces();

		// Token: 0x06003FE3 RID: 16355 RVA: 0x000DAA70 File Offset: 0x000D8C70
		internal void ApplyCanonicalSortToExplicitAces(int start, int count)
		{
			for (int i = start + 1; i < start + count; i++)
			{
				KnownAce knownAce = (KnownAce)this.raw_acl[i];
				SecurityIdentifier securityIdentifier = knownAce.SecurityIdentifier;
				int num = i;
				while (num > start && ((KnownAce)this.raw_acl[num - 1]).SecurityIdentifier.CompareTo(securityIdentifier) > 0)
				{
					this.raw_acl[num] = this.raw_acl[num - 1];
					num--;
				}
				this.raw_acl[num] = knownAce;
			}
		}

		// Token: 0x06003FE4 RID: 16356 RVA: 0x000DAAFA File Offset: 0x000D8CFA
		internal override string GetSddlForm(ControlFlags sdFlags, bool isDacl)
		{
			return this.raw_acl.GetSddlForm(sdFlags, isDacl);
		}

		// Token: 0x06003FE5 RID: 16357 RVA: 0x000DAB0C File Offset: 0x000D8D0C
		internal void RemoveAces<T>(CommonAcl.RemoveAcesCallback<T> callback) where T : GenericAce
		{
			int i = 0;
			while (i < this.raw_acl.Count)
			{
				if (this.raw_acl[i] is T && callback((T)((object)this.raw_acl[i])))
				{
					this.raw_acl.RemoveAce(i);
				}
				else
				{
					i++;
				}
			}
		}

		// Token: 0x06003FE6 RID: 16358 RVA: 0x000DAB6C File Offset: 0x000D8D6C
		internal void AddAce(AceQualifier aceQualifier, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags)
		{
			QualifiedAce newAce = this.AddAceGetQualifiedAce(aceQualifier, sid, accessMask, inheritanceFlags, propagationFlags, auditFlags);
			this.AddAce(newAce);
		}

		// Token: 0x06003FE7 RID: 16359 RVA: 0x000DAB90 File Offset: 0x000D8D90
		internal void AddAce(AceQualifier aceQualifier, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			QualifiedAce newAce = this.AddAceGetQualifiedAce(aceQualifier, sid, accessMask, inheritanceFlags, propagationFlags, auditFlags, objectFlags, objectType, inheritedObjectType);
			this.AddAce(newAce);
		}

		// Token: 0x06003FE8 RID: 16360 RVA: 0x000DABBC File Offset: 0x000D8DBC
		private QualifiedAce AddAceGetQualifiedAce(AceQualifier aceQualifier, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			if (!this.IsDS)
			{
				throw new InvalidOperationException("For this overload, IsDS must be true.");
			}
			if (objectFlags == ObjectAceFlags.None)
			{
				return this.AddAceGetQualifiedAce(aceQualifier, sid, accessMask, inheritanceFlags, propagationFlags, auditFlags);
			}
			return new ObjectAce(this.GetAceFlags(inheritanceFlags, propagationFlags, auditFlags), aceQualifier, accessMask, sid, objectFlags, objectType, inheritedObjectType, false, null);
		}

		// Token: 0x06003FE9 RID: 16361 RVA: 0x000DAC0C File Offset: 0x000D8E0C
		private QualifiedAce AddAceGetQualifiedAce(AceQualifier aceQualifier, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags)
		{
			return new CommonAce(this.GetAceFlags(inheritanceFlags, propagationFlags, auditFlags), aceQualifier, accessMask, sid, false, null);
		}

		// Token: 0x06003FEA RID: 16362 RVA: 0x000DAC24 File Offset: 0x000D8E24
		private void AddAce(QualifiedAce newAce)
		{
			this.RequireCanonicity();
			int aceInsertPosition = this.GetAceInsertPosition(newAce.AceQualifier);
			this.raw_acl.InsertAce(aceInsertPosition, CommonAcl.CopyAce(newAce));
			this.CanonicalizeAndClearAefa();
		}

		// Token: 0x06003FEB RID: 16363 RVA: 0x000DAC5C File Offset: 0x000D8E5C
		private static GenericAce CopyAce(GenericAce ace)
		{
			byte[] binaryForm = new byte[ace.BinaryLength];
			ace.GetBinaryForm(binaryForm, 0);
			return GenericAce.CreateFromBinaryForm(binaryForm, 0);
		}

		// Token: 0x06003FEC RID: 16364
		internal abstract int GetAceInsertPosition(AceQualifier aceQualifier);

		// Token: 0x06003FED RID: 16365 RVA: 0x000DAC84 File Offset: 0x000D8E84
		private AceFlags GetAceFlags(InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags)
		{
			if (inheritanceFlags != InheritanceFlags.None && !this.IsContainer)
			{
				throw new ArgumentException("Flags only work with containers.", "inheritanceFlags");
			}
			if (inheritanceFlags == InheritanceFlags.None && propagationFlags != PropagationFlags.None)
			{
				throw new ArgumentException("Propagation flags need inheritance flags.", "propagationFlags");
			}
			AceFlags aceFlags = AceFlags.None;
			if ((InheritanceFlags.ContainerInherit & inheritanceFlags) != InheritanceFlags.None)
			{
				aceFlags |= AceFlags.ContainerInherit;
			}
			if ((InheritanceFlags.ObjectInherit & inheritanceFlags) != InheritanceFlags.None)
			{
				aceFlags |= AceFlags.ObjectInherit;
			}
			if ((PropagationFlags.InheritOnly & propagationFlags) != PropagationFlags.None)
			{
				aceFlags |= AceFlags.InheritOnly;
			}
			if ((PropagationFlags.NoPropagateInherit & propagationFlags) != PropagationFlags.None)
			{
				aceFlags |= AceFlags.NoPropagateInherit;
			}
			if ((AuditFlags.Success & auditFlags) != AuditFlags.None)
			{
				aceFlags |= AceFlags.SuccessfulAccess;
			}
			if ((AuditFlags.Failure & auditFlags) != AuditFlags.None)
			{
				aceFlags |= AceFlags.FailedAccess;
			}
			return aceFlags;
		}

		// Token: 0x06003FEE RID: 16366 RVA: 0x000DAD00 File Offset: 0x000D8F00
		internal void RemoveAceSpecific(AceQualifier aceQualifier, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags)
		{
			this.RequireCanonicity();
			this.RemoveAces<CommonAce>((CommonAce ace) => ace.AccessMask == accessMask && ace.AceQualifier == aceQualifier && !(ace.SecurityIdentifier != sid) && ace.InheritanceFlags == inheritanceFlags && (inheritanceFlags == InheritanceFlags.None || ace.PropagationFlags == propagationFlags) && ace.AuditFlags == auditFlags);
			this.CanonicalizeAndClearAefa();
		}

		// Token: 0x06003FEF RID: 16367 RVA: 0x000DAD60 File Offset: 0x000D8F60
		internal void RemoveAceSpecific(AceQualifier aceQualifier, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			if (!this.IsDS)
			{
				throw new InvalidOperationException("For this overload, IsDS must be true.");
			}
			if (objectFlags == ObjectAceFlags.None)
			{
				this.RemoveAceSpecific(aceQualifier, sid, accessMask, inheritanceFlags, propagationFlags, auditFlags);
				return;
			}
			this.RequireCanonicity();
			this.RemoveAces<ObjectAce>((ObjectAce ace) => ace.AccessMask == accessMask && ace.AceQualifier == aceQualifier && !(ace.SecurityIdentifier != sid) && ace.InheritanceFlags == inheritanceFlags && (inheritanceFlags == InheritanceFlags.None || ace.PropagationFlags == propagationFlags) && ace.AuditFlags == auditFlags && ace.ObjectAceFlags == objectFlags && ((objectFlags & ObjectAceFlags.ObjectAceTypePresent) == ObjectAceFlags.None || !(ace.ObjectAceType != objectType)) && ((objectFlags & ObjectAceFlags.InheritedObjectAceTypePresent) == ObjectAceFlags.None || !(ace.InheritedObjectAceType != objectType)));
			this.CanonicalizeAndClearAefa();
		}

		// Token: 0x06003FF0 RID: 16368 RVA: 0x000DAE14 File Offset: 0x000D9014
		internal void SetAce(AceQualifier aceQualifier, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags)
		{
			QualifiedAce ace = this.AddAceGetQualifiedAce(aceQualifier, sid, accessMask, inheritanceFlags, propagationFlags, auditFlags);
			this.SetAce(ace);
		}

		// Token: 0x06003FF1 RID: 16369 RVA: 0x000DAE38 File Offset: 0x000D9038
		internal void SetAce(AceQualifier aceQualifier, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			QualifiedAce ace = this.AddAceGetQualifiedAce(aceQualifier, sid, accessMask, inheritanceFlags, propagationFlags, auditFlags, objectFlags, objectType, inheritedObjectType);
			this.SetAce(ace);
		}

		// Token: 0x06003FF2 RID: 16370 RVA: 0x000DAE64 File Offset: 0x000D9064
		private void SetAce(QualifiedAce newAce)
		{
			this.RequireCanonicity();
			this.RemoveAces<QualifiedAce>((QualifiedAce oldAce) => oldAce.AceQualifier == newAce.AceQualifier && oldAce.SecurityIdentifier == newAce.SecurityIdentifier);
			this.CanonicalizeAndClearAefa();
			this.AddAce(newAce);
		}

		// Token: 0x06003FF3 RID: 16371 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal CommonAcl()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x0400207E RID: 8318
		private const int default_capacity = 10;

		// Token: 0x0400207F RID: 8319
		private bool is_aefa;

		// Token: 0x04002080 RID: 8320
		private bool is_canonical;

		// Token: 0x04002081 RID: 8321
		private bool is_container;

		// Token: 0x04002082 RID: 8322
		private bool is_ds;

		// Token: 0x04002083 RID: 8323
		internal RawAcl raw_acl;

		// Token: 0x020005EB RID: 1515
		// (Invoke) Token: 0x06003FF5 RID: 16373
		internal delegate bool RemoveAcesCallback<T>(T ace);

		// Token: 0x020005EC RID: 1516
		[CompilerGenerated]
		private sealed class <>c__DisplayClass30_0
		{
			// Token: 0x06003FF8 RID: 16376 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass30_0()
			{
			}

			// Token: 0x06003FF9 RID: 16377 RVA: 0x000DAEA8 File Offset: 0x000D90A8
			internal bool <Purge>b__0(KnownAce ace)
			{
				return ace.SecurityIdentifier == this.sid;
			}

			// Token: 0x04002084 RID: 8324
			public SecurityIdentifier sid;
		}

		// Token: 0x020005ED RID: 1517
		[CompilerGenerated]
		[Serializable]
		private sealed class <>c
		{
			// Token: 0x06003FFA RID: 16378 RVA: 0x000DAEBB File Offset: 0x000D90BB
			// Note: this type is marked as 'beforefieldinit'.
			static <>c()
			{
			}

			// Token: 0x06003FFB RID: 16379 RVA: 0x00002050 File Offset: 0x00000250
			public <>c()
			{
			}

			// Token: 0x06003FFC RID: 16380 RVA: 0x000DAEC7 File Offset: 0x000D90C7
			internal bool <RemoveInheritedAces>b__31_0(GenericAce ace)
			{
				return ace.IsInherited;
			}

			// Token: 0x04002085 RID: 8325
			public static readonly CommonAcl.<>c <>9 = new CommonAcl.<>c();

			// Token: 0x04002086 RID: 8326
			public static CommonAcl.RemoveAcesCallback<GenericAce> <>9__31_0;
		}

		// Token: 0x020005EE RID: 1518
		[CompilerGenerated]
		private sealed class <>c__DisplayClass53_0
		{
			// Token: 0x06003FFD RID: 16381 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass53_0()
			{
			}

			// Token: 0x06003FFE RID: 16382 RVA: 0x000DAED0 File Offset: 0x000D90D0
			internal bool <RemoveAceSpecific>b__0(CommonAce ace)
			{
				return ace.AccessMask == this.accessMask && ace.AceQualifier == this.aceQualifier && !(ace.SecurityIdentifier != this.sid) && ace.InheritanceFlags == this.inheritanceFlags && (this.inheritanceFlags == InheritanceFlags.None || ace.PropagationFlags == this.propagationFlags) && ace.AuditFlags == this.auditFlags;
			}

			// Token: 0x04002087 RID: 8327
			public int accessMask;

			// Token: 0x04002088 RID: 8328
			public AceQualifier aceQualifier;

			// Token: 0x04002089 RID: 8329
			public SecurityIdentifier sid;

			// Token: 0x0400208A RID: 8330
			public InheritanceFlags inheritanceFlags;

			// Token: 0x0400208B RID: 8331
			public PropagationFlags propagationFlags;

			// Token: 0x0400208C RID: 8332
			public AuditFlags auditFlags;
		}

		// Token: 0x020005EF RID: 1519
		[CompilerGenerated]
		private sealed class <>c__DisplayClass54_0
		{
			// Token: 0x06003FFF RID: 16383 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass54_0()
			{
			}

			// Token: 0x06004000 RID: 16384 RVA: 0x000DAF4C File Offset: 0x000D914C
			internal bool <RemoveAceSpecific>b__0(ObjectAce ace)
			{
				return ace.AccessMask == this.accessMask && ace.AceQualifier == this.aceQualifier && !(ace.SecurityIdentifier != this.sid) && ace.InheritanceFlags == this.inheritanceFlags && (this.inheritanceFlags == InheritanceFlags.None || ace.PropagationFlags == this.propagationFlags) && ace.AuditFlags == this.auditFlags && ace.ObjectAceFlags == this.objectFlags && ((this.objectFlags & ObjectAceFlags.ObjectAceTypePresent) == ObjectAceFlags.None || !(ace.ObjectAceType != this.objectType)) && ((this.objectFlags & ObjectAceFlags.InheritedObjectAceTypePresent) == ObjectAceFlags.None || !(ace.InheritedObjectAceType != this.objectType));
			}

			// Token: 0x0400208D RID: 8333
			public int accessMask;

			// Token: 0x0400208E RID: 8334
			public AceQualifier aceQualifier;

			// Token: 0x0400208F RID: 8335
			public SecurityIdentifier sid;

			// Token: 0x04002090 RID: 8336
			public InheritanceFlags inheritanceFlags;

			// Token: 0x04002091 RID: 8337
			public PropagationFlags propagationFlags;

			// Token: 0x04002092 RID: 8338
			public AuditFlags auditFlags;

			// Token: 0x04002093 RID: 8339
			public ObjectAceFlags objectFlags;

			// Token: 0x04002094 RID: 8340
			public Guid objectType;
		}

		// Token: 0x020005F0 RID: 1520
		[CompilerGenerated]
		private sealed class <>c__DisplayClass57_0
		{
			// Token: 0x06004001 RID: 16385 RVA: 0x00002050 File Offset: 0x00000250
			public <>c__DisplayClass57_0()
			{
			}

			// Token: 0x06004002 RID: 16386 RVA: 0x000DB015 File Offset: 0x000D9215
			internal bool <SetAce>b__0(QualifiedAce oldAce)
			{
				return oldAce.AceQualifier == this.newAce.AceQualifier && oldAce.SecurityIdentifier == this.newAce.SecurityIdentifier;
			}

			// Token: 0x04002095 RID: 8341
			public QualifiedAce newAce;
		}
	}
}
