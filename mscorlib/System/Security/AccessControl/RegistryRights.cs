﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the access control rights that can be applied to registry objects.</summary>
	// Token: 0x02000625 RID: 1573
	[Flags]
	public enum RegistryRights
	{
		/// <summary>The right to query the name/value pairs in a registry key.</summary>
		// Token: 0x0400211E RID: 8478
		QueryValues = 1,
		/// <summary>The right to create, delete, or set name/value pairs in a registry key.</summary>
		// Token: 0x0400211F RID: 8479
		SetValue = 2,
		/// <summary>The right to create subkeys of a registry key.</summary>
		// Token: 0x04002120 RID: 8480
		CreateSubKey = 4,
		/// <summary>The right to list the subkeys of a registry key.</summary>
		// Token: 0x04002121 RID: 8481
		EnumerateSubKeys = 8,
		/// <summary>The right to request notification of changes on a registry key.</summary>
		// Token: 0x04002122 RID: 8482
		Notify = 16,
		/// <summary>Reserved for system use.</summary>
		// Token: 0x04002123 RID: 8483
		CreateLink = 32,
		/// <summary>The right to delete a registry key.</summary>
		// Token: 0x04002124 RID: 8484
		Delete = 65536,
		/// <summary>The right to open and copy the access rules and audit rules for a registry key.</summary>
		// Token: 0x04002125 RID: 8485
		ReadPermissions = 131072,
		/// <summary>The right to create, delete, and set the name/value pairs in a registry key, to create or delete subkeys, to request notification of changes, to enumerate its subkeys, and to read its access rules and audit rules.</summary>
		// Token: 0x04002126 RID: 8486
		WriteKey = 131078,
		/// <summary>The right to query the name/value pairs in a registry key, to request notification of changes, to enumerate its subkeys, and to read its access rules and audit rules.</summary>
		// Token: 0x04002127 RID: 8487
		ReadKey = 131097,
		/// <summary>Same as <see cref="F:System.Security.AccessControl.RegistryRights.ReadKey" />.</summary>
		// Token: 0x04002128 RID: 8488
		ExecuteKey = 131097,
		/// <summary>The right to change the access rules and audit rules associated with a registry key.</summary>
		// Token: 0x04002129 RID: 8489
		ChangePermissions = 262144,
		/// <summary>The right to change the owner of a registry key.</summary>
		// Token: 0x0400212A RID: 8490
		TakeOwnership = 524288,
		/// <summary>The right to exert full control over a registry key, and to modify its access rules and audit rules.</summary>
		// Token: 0x0400212B RID: 8491
		FullControl = 983103
	}
}
