﻿using System;

namespace System.Security.AccessControl
{
	/// <summary>Specifies the actions that are permitted for securable objects.</summary>
	// Token: 0x020005DA RID: 1498
	[Flags]
	public enum AccessControlActions
	{
		/// <summary>Specifies no access.</summary>
		// Token: 0x0400203A RID: 8250
		None = 0,
		/// <summary>Specifies read-only access.</summary>
		// Token: 0x0400203B RID: 8251
		View = 1,
		/// <summary>Specifies write-only access.</summary>
		// Token: 0x0400203C RID: 8252
		Change = 2
	}
}
