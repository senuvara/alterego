﻿using System;
using System.Globalization;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	/// <summary>Represents an access control entry (ACE).</summary>
	// Token: 0x020005E9 RID: 1513
	public sealed class CommonAce : QualifiedAce
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.AccessControl.CommonAce" /> class.</summary>
		/// <param name="flags">Flags that specify information about the inheritance, inheritance propagation, and auditing conditions for the new access control entry (ACE).</param>
		/// <param name="qualifier">The use of the new ACE.</param>
		/// <param name="accessMask">The access mask for the ACE.</param>
		/// <param name="sid">The <see cref="T:System.Security.Principal.SecurityIdentifier" /> associated with the new ACE.</param>
		/// <param name="isCallback">
		///       <see langword="true" /> to specify that the new ACE is a callback type ACE.</param>
		/// <param name="opaque">Opaque data associated with the new ACE. Opaque data is allowed only for callback ACE types. The length of this array must not be greater than the return value of the <see cref="M:System.Security.AccessControl.CommonAce.MaxOpaqueLength(System.Boolean)" /> method.</param>
		// Token: 0x06003FC0 RID: 16320 RVA: 0x000DA278 File Offset: 0x000D8478
		public CommonAce(AceFlags flags, AceQualifier qualifier, int accessMask, SecurityIdentifier sid, bool isCallback, byte[] opaque) : base(CommonAce.ConvertType(qualifier, isCallback), flags, opaque)
		{
			base.AccessMask = accessMask;
			base.SecurityIdentifier = sid;
		}

		// Token: 0x06003FC1 RID: 16321 RVA: 0x000DA29A File Offset: 0x000D849A
		internal CommonAce(AceType type, AceFlags flags, int accessMask, SecurityIdentifier sid, byte[] opaque) : base(type, flags, opaque)
		{
			base.AccessMask = accessMask;
			base.SecurityIdentifier = sid;
		}

		// Token: 0x06003FC2 RID: 16322 RVA: 0x000DA2B8 File Offset: 0x000D84B8
		internal CommonAce(byte[] binaryForm, int offset) : base(binaryForm, offset)
		{
			int num = (int)GenericAce.ReadUShort(binaryForm, offset + 2);
			if (offset > binaryForm.Length - num)
			{
				throw new ArgumentException("Invalid ACE - truncated", "binaryForm");
			}
			if (num < 8 + SecurityIdentifier.MinBinaryLength)
			{
				throw new ArgumentException("Invalid ACE", "binaryForm");
			}
			base.AccessMask = GenericAce.ReadInt(binaryForm, offset + 4);
			base.SecurityIdentifier = new SecurityIdentifier(binaryForm, offset + 8);
			int num2 = num - (8 + base.SecurityIdentifier.BinaryLength);
			if (num2 > 0)
			{
				byte[] array = new byte[num2];
				Array.Copy(binaryForm, offset + 8 + base.SecurityIdentifier.BinaryLength, array, 0, num2);
				base.SetOpaque(array);
			}
		}

		/// <summary>Gets the length, in bytes, of the binary representation of the current <see cref="T:System.Security.AccessControl.CommonAce" /> object. Use this length with the <see cref="M:System.Security.AccessControl.CommonAce.GetBinaryForm(System.Byte[],System.Int32)" /> method before marshaling the ACL into a binary array.</summary>
		/// <returns>The length, in bytes, of the binary representation of the current <see cref="T:System.Security.AccessControl.CommonAce" /> object.</returns>
		// Token: 0x17000A8C RID: 2700
		// (get) Token: 0x06003FC3 RID: 16323 RVA: 0x000DA361 File Offset: 0x000D8561
		public override int BinaryLength
		{
			get
			{
				return 8 + base.SecurityIdentifier.BinaryLength + base.OpaqueLength;
			}
		}

		/// <summary>Marshals the contents of the <see cref="T:System.Security.AccessControl.CommonAce" /> object into the specified byte array beginning at the specified offset.</summary>
		/// <param name="binaryForm">The byte array into which the contents of the <see cref="T:System.Security.AccessControl.CommonAce" /> object is marshaled.</param>
		/// <param name="offset">The offset at which to start marshaling.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> is negative or too high to allow the entire <see cref="T:System.Security.AccessControl.CommonAce" /> to be copied into the <paramref name="binaryForm" /> array.</exception>
		// Token: 0x06003FC4 RID: 16324 RVA: 0x000DA378 File Offset: 0x000D8578
		public override void GetBinaryForm(byte[] binaryForm, int offset)
		{
			ushort binaryLength = (ushort)this.BinaryLength;
			binaryForm[offset] = (byte)base.AceType;
			binaryForm[offset + 1] = (byte)base.AceFlags;
			GenericAce.WriteUShort(binaryLength, binaryForm, offset + 2);
			GenericAce.WriteInt(base.AccessMask, binaryForm, offset + 4);
			base.SecurityIdentifier.GetBinaryForm(binaryForm, offset + 8);
			byte[] opaque = base.GetOpaque();
			if (opaque != null)
			{
				Array.Copy(opaque, 0, binaryForm, offset + 8 + base.SecurityIdentifier.BinaryLength, opaque.Length);
			}
		}

		/// <summary>Gets the maximum allowed length of an opaque data BLOB for callback access control entries (ACEs).</summary>
		/// <param name="isCallback">
		///       <see langword="true" /> to specify that the <see cref="T:System.Security.AccessControl.CommonAce" /> object is a callback ACE type.</param>
		/// <returns>The allowed length of an opaque data BLOB.</returns>
		// Token: 0x06003FC5 RID: 16325 RVA: 0x000DA3EB File Offset: 0x000D85EB
		public static int MaxOpaqueLength(bool isCallback)
		{
			return 65459;
		}

		// Token: 0x06003FC6 RID: 16326 RVA: 0x000DA3F4 File Offset: 0x000D85F4
		internal override string GetSddlForm()
		{
			if (base.OpaqueLength != 0)
			{
				throw new NotImplementedException("Unable to convert conditional ACEs to SDDL");
			}
			return string.Format(CultureInfo.InvariantCulture, "({0};{1};{2};;;{3})", new object[]
			{
				GenericAce.GetSddlAceType(base.AceType),
				GenericAce.GetSddlAceFlags(base.AceFlags),
				KnownAce.GetSddlAccessRights(base.AccessMask),
				base.SecurityIdentifier.GetSddlForm()
			});
		}

		// Token: 0x06003FC7 RID: 16327 RVA: 0x000DA464 File Offset: 0x000D8664
		private static AceType ConvertType(AceQualifier qualifier, bool isCallback)
		{
			switch (qualifier)
			{
			case AceQualifier.AccessAllowed:
				if (isCallback)
				{
					return AceType.AccessAllowedCallback;
				}
				return AceType.AccessAllowed;
			case AceQualifier.AccessDenied:
				if (isCallback)
				{
					return AceType.AccessDeniedCallback;
				}
				return AceType.AccessDenied;
			case AceQualifier.SystemAudit:
				if (isCallback)
				{
					return AceType.SystemAuditCallback;
				}
				return AceType.SystemAudit;
			case AceQualifier.SystemAlarm:
				if (isCallback)
				{
					return AceType.SystemAlarmCallback;
				}
				return AceType.SystemAlarm;
			default:
				throw new ArgumentException("Unrecognized ACE qualifier: " + qualifier, "qualifier");
			}
		}
	}
}
