﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	/// <summary>Determines access to securable objects. The derived classes <see cref="T:System.Security.AccessControl.AccessRule" /> and <see cref="T:System.Security.AccessControl.AuditRule" /> offer specializations for access and audit functionality.</summary>
	// Token: 0x020005E7 RID: 1511
	public abstract class AuthorizationRule
	{
		// Token: 0x06003FB4 RID: 16308 RVA: 0x00002050 File Offset: 0x00000250
		internal AuthorizationRule()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.AccessControl.AccessRule" /> class by using the specified values.</summary>
		/// <param name="identity">The identity to which the access rule applies. This parameter must be an object that can be cast as a <see cref="T:System.Security.Principal.SecurityIdentifier" />.</param>
		/// <param name="accessMask">The access mask of this rule. The access mask is a 32-bit collection of anonymous bits, the meaning of which is defined by the individual integrators.</param>
		/// <param name="isInherited">
		///       <see langword="true" /> to inherit this rule from a parent container.</param>
		/// <param name="inheritanceFlags">The inheritance properties of the access rule.</param>
		/// <param name="propagationFlags">Whether inherited access rules are automatically propagated. The propagation flags are ignored if <paramref name="inheritanceFlags" /> is set to <see cref="F:System.Security.AccessControl.InheritanceFlags.None" />.</param>
		/// <exception cref="T:System.ArgumentException">The value of the <paramref name="identity" /> parameter cannot be cast as a <see cref="T:System.Security.Principal.SecurityIdentifier" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value of the <paramref name="accessMask" /> parameter is zero, or the <paramref name="inheritanceFlags" /> or <paramref name="propagationFlags" /> parameters contain unrecognized flag values.</exception>
		// Token: 0x06003FB5 RID: 16309 RVA: 0x000DA174 File Offset: 0x000D8374
		protected internal AuthorizationRule(IdentityReference identity, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			if (null == identity)
			{
				throw new ArgumentNullException("identity");
			}
			if (!(identity is SecurityIdentifier) && !(identity is NTAccount))
			{
				throw new ArgumentException("identity");
			}
			if (accessMask == 0)
			{
				throw new ArgumentException("accessMask");
			}
			if ((inheritanceFlags & ~(InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit)) != InheritanceFlags.None)
			{
				throw new ArgumentOutOfRangeException();
			}
			if ((propagationFlags & ~(PropagationFlags.NoPropagateInherit | PropagationFlags.InheritOnly)) != PropagationFlags.None)
			{
				throw new ArgumentOutOfRangeException();
			}
			this.identity = identity;
			this.accessMask = accessMask;
			this.isInherited = isInherited;
			this.inheritanceFlags = inheritanceFlags;
			this.propagationFlags = propagationFlags;
		}

		/// <summary>Gets the <see cref="T:System.Security.Principal.IdentityReference" /> to which this rule applies.</summary>
		/// <returns>The <see cref="T:System.Security.Principal.IdentityReference" /> to which this rule applies.</returns>
		// Token: 0x17000A86 RID: 2694
		// (get) Token: 0x06003FB6 RID: 16310 RVA: 0x000DA203 File Offset: 0x000D8403
		public IdentityReference IdentityReference
		{
			get
			{
				return this.identity;
			}
		}

		/// <summary>Gets the value of flags that determine how this rule is inherited by child objects.</summary>
		/// <returns>A bitwise combination of the enumeration values.</returns>
		// Token: 0x17000A87 RID: 2695
		// (get) Token: 0x06003FB7 RID: 16311 RVA: 0x000DA20B File Offset: 0x000D840B
		public InheritanceFlags InheritanceFlags
		{
			get
			{
				return this.inheritanceFlags;
			}
		}

		/// <summary>Gets a value indicating whether this rule is explicitly set or is inherited from a parent container object.</summary>
		/// <returns>
		///     <see langword="true" /> if this rule is not explicitly set but is instead inherited from a parent container.</returns>
		// Token: 0x17000A88 RID: 2696
		// (get) Token: 0x06003FB8 RID: 16312 RVA: 0x000DA213 File Offset: 0x000D8413
		public bool IsInherited
		{
			get
			{
				return this.isInherited;
			}
		}

		/// <summary>Gets the value of the propagation flags, which determine how inheritance of this rule is propagated to child objects. This property is significant only when the value of the <see cref="T:System.Security.AccessControl.InheritanceFlags" /> enumeration is not <see cref="F:System.Security.AccessControl.InheritanceFlags.None" />.</summary>
		/// <returns>A bitwise combination of the enumeration values.</returns>
		// Token: 0x17000A89 RID: 2697
		// (get) Token: 0x06003FB9 RID: 16313 RVA: 0x000DA21B File Offset: 0x000D841B
		public PropagationFlags PropagationFlags
		{
			get
			{
				return this.propagationFlags;
			}
		}

		/// <summary>Gets the access mask for this rule.</summary>
		/// <returns>The access mask for this rule.</returns>
		// Token: 0x17000A8A RID: 2698
		// (get) Token: 0x06003FBA RID: 16314 RVA: 0x000DA223 File Offset: 0x000D8423
		protected internal int AccessMask
		{
			get
			{
				return this.accessMask;
			}
		}

		// Token: 0x04002079 RID: 8313
		private IdentityReference identity;

		// Token: 0x0400207A RID: 8314
		private int accessMask;

		// Token: 0x0400207B RID: 8315
		private bool isInherited;

		// Token: 0x0400207C RID: 8316
		private InheritanceFlags inheritanceFlags;

		// Token: 0x0400207D RID: 8317
		private PropagationFlags propagationFlags;
	}
}
