﻿using System;

namespace System.Security
{
	/// <summary>Specifies that code or an assembly performs security-critical operations.</summary>
	// Token: 0x02000549 RID: 1353
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Field | AttributeTargets.Interface | AttributeTargets.Delegate, AllowMultiple = false, Inherited = false)]
	public sealed class SecurityCriticalAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityCriticalAttribute" /> class. </summary>
		// Token: 0x06003A5C RID: 14940 RVA: 0x000020BF File Offset: 0x000002BF
		public SecurityCriticalAttribute()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityCriticalAttribute" /> class with the specified scope. </summary>
		/// <param name="scope">One of the enumeration values that specifies the scope of the attribute. </param>
		// Token: 0x06003A5D RID: 14941 RVA: 0x000CB0AE File Offset: 0x000C92AE
		public SecurityCriticalAttribute(SecurityCriticalScope scope)
		{
			this._val = scope;
		}

		/// <summary>Gets the scope for the attribute.</summary>
		/// <returns>One of the enumeration values that specifies the scope of the attribute. The default is <see cref="F:System.Security.SecurityCriticalScope.Explicit" />, which indicates that the attribute applies only to the immediate target.</returns>
		// Token: 0x17000987 RID: 2439
		// (get) Token: 0x06003A5E RID: 14942 RVA: 0x000CB0BD File Offset: 0x000C92BD
		[Obsolete("SecurityCriticalScope is only used for .NET 2.0 transparency compatibility.")]
		public SecurityCriticalScope Scope
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x04001E5F RID: 7775
		private SecurityCriticalScope _val;
	}
}
