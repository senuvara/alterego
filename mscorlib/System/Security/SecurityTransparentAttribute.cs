﻿using System;

namespace System.Security
{
	/// <summary>Specifies that an assembly cannot cause an elevation of privilege. </summary>
	// Token: 0x0200054C RID: 1356
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	public sealed class SecurityTransparentAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityTransparentAttribute" /> class. </summary>
		// Token: 0x06003A61 RID: 14945 RVA: 0x000020BF File Offset: 0x000002BF
		public SecurityTransparentAttribute()
		{
		}
	}
}
