﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	/// <summary>Marks modules containing unverifiable code. This class cannot be inherited.</summary>
	// Token: 0x02000545 RID: 1349
	[AttributeUsage(AttributeTargets.Module, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	public sealed class UnverifiableCodeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.UnverifiableCodeAttribute" /> class. </summary>
		// Token: 0x06003A58 RID: 14936 RVA: 0x000020BF File Offset: 0x000002BF
		public UnverifiableCodeAttribute()
		{
		}
	}
}
