﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	/// <summary>Provides the application directory as evidence for policy evaluation. This class cannot be inherited.</summary>
	// Token: 0x0200056D RID: 1389
	[ComVisible(true)]
	[Serializable]
	public sealed class ApplicationDirectory : EvidenceBase, IBuiltInEvidence
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.ApplicationDirectory" /> class.</summary>
		/// <param name="name">The path of the application directory. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="name" /> parameter is <see langword="null" />. </exception>
		// Token: 0x06003B98 RID: 15256 RVA: 0x000CE43C File Offset: 0x000CC63C
		public ApplicationDirectory(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length < 1)
			{
				throw new FormatException(Locale.GetText("Empty"));
			}
			this.directory = name;
		}

		/// <summary>Gets the path of the application directory.</summary>
		/// <returns>The path of the application directory.</returns>
		// Token: 0x170009B3 RID: 2483
		// (get) Token: 0x06003B99 RID: 15257 RVA: 0x000CE472 File Offset: 0x000CC672
		public string Directory
		{
			get
			{
				return this.directory;
			}
		}

		/// <summary>Creates a new copy of the <see cref="T:System.Security.Policy.ApplicationDirectory" />.</summary>
		/// <returns>A new, identical copy of the <see cref="T:System.Security.Policy.ApplicationDirectory" />.</returns>
		// Token: 0x06003B9A RID: 15258 RVA: 0x000CE47A File Offset: 0x000CC67A
		public object Copy()
		{
			return new ApplicationDirectory(this.Directory);
		}

		/// <summary>Determines whether instances of the same type of an evidence object are equivalent.</summary>
		/// <param name="o">An object of same type as the current evidence object. </param>
		/// <returns>
		///     <see langword="true" /> if the two instances are equivalent; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003B9B RID: 15259 RVA: 0x000CE488 File Offset: 0x000CC688
		public override bool Equals(object o)
		{
			ApplicationDirectory applicationDirectory = o as ApplicationDirectory;
			if (applicationDirectory != null)
			{
				this.ThrowOnInvalid(applicationDirectory.directory);
				return this.directory == applicationDirectory.directory;
			}
			return false;
		}

		/// <summary>Gets the hash code of the current application directory.</summary>
		/// <returns>The hash code of the current application directory.</returns>
		// Token: 0x06003B9C RID: 15260 RVA: 0x000CE4BE File Offset: 0x000CC6BE
		public override int GetHashCode()
		{
			return this.Directory.GetHashCode();
		}

		/// <summary>Gets a string representation of the state of the <see cref="T:System.Security.Policy.ApplicationDirectory" /> evidence object.</summary>
		/// <returns>A representation of the state of the <see cref="T:System.Security.Policy.ApplicationDirectory" /> evidence object.</returns>
		// Token: 0x06003B9D RID: 15261 RVA: 0x000CE4CC File Offset: 0x000CC6CC
		public override string ToString()
		{
			this.ThrowOnInvalid(this.Directory);
			SecurityElement securityElement = new SecurityElement("System.Security.Policy.ApplicationDirectory");
			securityElement.AddAttribute("version", "1");
			securityElement.AddChild(new SecurityElement("Directory", this.directory));
			return securityElement.ToString();
		}

		// Token: 0x06003B9E RID: 15262 RVA: 0x000CE51A File Offset: 0x000CC71A
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return (verbose ? 3 : 1) + this.directory.Length;
		}

		// Token: 0x06003B9F RID: 15263 RVA: 0x00002526 File Offset: 0x00000726
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003BA0 RID: 15264 RVA: 0x00002526 File Offset: 0x00000726
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x06003BA1 RID: 15265 RVA: 0x000CE52F File Offset: 0x000CC72F
		private void ThrowOnInvalid(string appdir)
		{
			if (appdir.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException(string.Format(Locale.GetText("Invalid character(s) in directory {0}"), appdir), "other");
			}
		}

		// Token: 0x04001EBF RID: 7871
		private string directory;
	}
}
