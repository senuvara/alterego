﻿using System;

namespace System.Security.Policy
{
	// Token: 0x02000588 RID: 1416
	internal sealed class MembershipConditionHelper
	{
		// Token: 0x06003C8E RID: 15502 RVA: 0x000D0B3C File Offset: 0x000CED3C
		internal static int CheckSecurityElement(SecurityElement se, string parameterName, int minimumVersion, int maximumVersion)
		{
			if (se == null)
			{
				throw new ArgumentNullException(parameterName);
			}
			if (se.Tag != MembershipConditionHelper.XmlTag)
			{
				throw new ArgumentException(string.Format(Locale.GetText("Invalid tag {0}, expected {1}."), se.Tag, MembershipConditionHelper.XmlTag), parameterName);
			}
			int result = minimumVersion;
			string text = se.Attribute("version");
			if (text != null)
			{
				try
				{
					result = int.Parse(text);
				}
				catch (Exception innerException)
				{
					throw new ArgumentException(string.Format(Locale.GetText("Couldn't parse version from '{0}'."), text), parameterName, innerException);
				}
			}
			return result;
		}

		// Token: 0x06003C8F RID: 15503 RVA: 0x000D0BCC File Offset: 0x000CEDCC
		internal static SecurityElement Element(Type type, int version)
		{
			SecurityElement securityElement = new SecurityElement(MembershipConditionHelper.XmlTag);
			securityElement.AddAttribute("class", type.FullName + ", " + type.Assembly.ToString().Replace('"', '\''));
			securityElement.AddAttribute("version", version.ToString());
			return securityElement;
		}

		// Token: 0x06003C90 RID: 15504 RVA: 0x00002050 File Offset: 0x00000250
		public MembershipConditionHelper()
		{
		}

		// Token: 0x06003C91 RID: 15505 RVA: 0x000D0C24 File Offset: 0x000CEE24
		// Note: this type is marked as 'beforefieldinit'.
		static MembershipConditionHelper()
		{
		}

		// Token: 0x04001F0D RID: 7949
		private static readonly string XmlTag = "IMembershipCondition";
	}
}
