﻿using System;

namespace System.Security.Policy
{
	// Token: 0x02000584 RID: 1412
	internal interface IBuiltInEvidence
	{
		// Token: 0x06003C86 RID: 15494
		int GetRequiredSize(bool verbose);

		// Token: 0x06003C87 RID: 15495
		int InitFromBuffer(char[] buffer, int position);

		// Token: 0x06003C88 RID: 15496
		int OutputToBuffer(char[] buffer, int position, bool verbose);
	}
}
