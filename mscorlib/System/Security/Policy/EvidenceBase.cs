﻿using System;
using System.Security.Permissions;

namespace System.Security.Policy
{
	/// <summary>Provides a base class from which all objects to be used as evidence must derive. </summary>
	// Token: 0x0200057C RID: 1404
	[PermissionSet(SecurityAction.InheritanceDemand, Unrestricted = true)]
	[Serializable]
	public abstract class EvidenceBase
	{
		/// <summary>Creates a new object that is a complete copy of the current instance.</summary>
		/// <returns>A duplicate copy of this evidence object.</returns>
		// Token: 0x06003C3B RID: 15419 RVA: 0x000041F3 File Offset: 0x000023F3
		[SecurityPermission(SecurityAction.Assert, SerializationFormatter = true)]
		public virtual EvidenceBase Clone()
		{
			throw new NotImplementedException();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.EvidenceBase" /> class. </summary>
		/// <exception cref="T:System.InvalidOperationException">An object to be used as evidence is not serializable.</exception>
		// Token: 0x06003C3C RID: 15420 RVA: 0x00002050 File Offset: 0x00000250
		protected EvidenceBase()
		{
		}
	}
}
