﻿using System;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x02000589 RID: 1417
	internal class MonoTrustManager : IApplicationTrustManager, ISecurityEncodable
	{
		// Token: 0x06003C92 RID: 15506 RVA: 0x000D0C30 File Offset: 0x000CEE30
		[SecurityPermission(SecurityAction.Demand, ControlPolicy = true)]
		public ApplicationTrust DetermineApplicationTrust(ActivationContext activationContext, TrustManagerContext context)
		{
			if (activationContext == null)
			{
				throw new ArgumentNullException("activationContext");
			}
			return null;
		}

		// Token: 0x06003C93 RID: 15507 RVA: 0x000D0C41 File Offset: 0x000CEE41
		public void FromXml(SecurityElement e)
		{
			if (e == null)
			{
				throw new ArgumentNullException("e");
			}
			if (e.Tag != "IApplicationTrustManager")
			{
				throw new ArgumentException("e", Locale.GetText("Invalid XML tag."));
			}
		}

		// Token: 0x06003C94 RID: 15508 RVA: 0x000D0C78 File Offset: 0x000CEE78
		public SecurityElement ToXml()
		{
			SecurityElement securityElement = new SecurityElement("IApplicationTrustManager");
			securityElement.AddAttribute("class", typeof(MonoTrustManager).AssemblyQualifiedName);
			securityElement.AddAttribute("version", "1");
			return securityElement;
		}

		// Token: 0x06003C95 RID: 15509 RVA: 0x00002050 File Offset: 0x00000250
		public MonoTrustManager()
		{
		}

		// Token: 0x04001F0E RID: 7950
		private const string tag = "IApplicationTrustManager";
	}
}
