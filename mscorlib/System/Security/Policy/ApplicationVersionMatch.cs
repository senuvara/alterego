﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	/// <summary>Specifies how to match versions when locating application trusts in a collection.</summary>
	// Token: 0x02000574 RID: 1396
	[ComVisible(true)]
	public enum ApplicationVersionMatch
	{
		/// <summary>Match on the exact version.</summary>
		// Token: 0x04001ED0 RID: 7888
		MatchExactVersion,
		/// <summary>Match on all versions.</summary>
		// Token: 0x04001ED1 RID: 7889
		MatchAllVersions
	}
}
