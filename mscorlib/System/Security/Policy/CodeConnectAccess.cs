﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	/// <summary>Specifies the network resource access that is granted to code.</summary>
	// Token: 0x02000575 RID: 1397
	[ComVisible(true)]
	[Serializable]
	public class CodeConnectAccess
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.CodeConnectAccess" /> class. </summary>
		/// <param name="allowScheme">The URI scheme represented by the current instance.</param>
		/// <param name="allowPort">The port represented by the current instance.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="allowScheme" /> is <see langword="null" />.-or-
		///         <paramref name="allowScheme" /> is an empty string ("").-or-
		///         <paramref name="allowScheme" /> contains characters that are not permitted in schemes.-or-
		///         <paramref name="allowPort" /> is less than 0.-or-
		///         <paramref name="allowPort" /> is greater than 65,535.</exception>
		// Token: 0x06003BE3 RID: 15331 RVA: 0x000CEEFC File Offset: 0x000CD0FC
		[MonoTODO("(2.0) validations incomplete")]
		public CodeConnectAccess(string allowScheme, int allowPort)
		{
			if (allowScheme == null || allowScheme.Length == 0)
			{
				throw new ArgumentOutOfRangeException("allowScheme");
			}
			if (allowPort < 0 || allowPort > 65535)
			{
				throw new ArgumentOutOfRangeException("allowPort");
			}
			this._scheme = allowScheme;
			this._port = allowPort;
		}

		/// <summary>Gets the port represented by the current instance.</summary>
		/// <returns>A <see cref="T:System.Int32" /> value that identifies a computer port used in conjunction with the <see cref="P:System.Security.Policy.CodeConnectAccess.Scheme" /> property.</returns>
		// Token: 0x170009C7 RID: 2503
		// (get) Token: 0x06003BE4 RID: 15332 RVA: 0x000CEF4A File Offset: 0x000CD14A
		public int Port
		{
			get
			{
				return this._port;
			}
		}

		/// <summary>Gets the URI scheme represented by the current instance.</summary>
		/// <returns>A <see cref="T:System.String" /> that identifies a URI scheme, converted to lowercase.</returns>
		// Token: 0x170009C8 RID: 2504
		// (get) Token: 0x06003BE5 RID: 15333 RVA: 0x000CEF52 File Offset: 0x000CD152
		public string Scheme
		{
			get
			{
				return this._scheme;
			}
		}

		/// <summary>Returns a value indicating whether two <see cref="T:System.Security.Policy.CodeConnectAccess" /> objects represent the same scheme and port.</summary>
		/// <param name="o">The object to compare to the current <see cref="T:System.Security.Policy.CodeConnectAccess" /> object.</param>
		/// <returns>
		///     <see langword="true" /> if the two objects represent the same scheme and port; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003BE6 RID: 15334 RVA: 0x000CEF5C File Offset: 0x000CD15C
		public override bool Equals(object o)
		{
			CodeConnectAccess codeConnectAccess = o as CodeConnectAccess;
			return codeConnectAccess != null && this._scheme == codeConnectAccess._scheme && this._port == codeConnectAccess._port;
		}

		/// <summary>Serves as a hash function for a particular type.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Object" />.</returns>
		// Token: 0x06003BE7 RID: 15335 RVA: 0x000CEF98 File Offset: 0x000CD198
		public override int GetHashCode()
		{
			return this._scheme.GetHashCode() ^ this._port;
		}

		/// <summary>Returns a <see cref="T:System.Security.Policy.CodeConnectAccess" /> instance that represents access to the specified port using any scheme.</summary>
		/// <param name="allowPort">The port represented by the returned instance.</param>
		/// <returns>A <see cref="T:System.Security.Policy.CodeConnectAccess" /> instance for the specified port.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="allowPort" /> is less than 0.-or-
		///         <paramref name="allowPort" /> is greater than 65,535.</exception>
		// Token: 0x06003BE8 RID: 15336 RVA: 0x000CEFAC File Offset: 0x000CD1AC
		public static CodeConnectAccess CreateAnySchemeAccess(int allowPort)
		{
			return new CodeConnectAccess(CodeConnectAccess.AnyScheme, allowPort);
		}

		/// <summary>Returns a <see cref="T:System.Security.Policy.CodeConnectAccess" /> instance that represents access to the specified port using the code's scheme of origin.</summary>
		/// <param name="allowPort">The port represented by the returned instance.</param>
		/// <returns>A <see cref="T:System.Security.Policy.CodeConnectAccess" /> instance for the specified port.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="allowPort" /> is less than 0.-or-
		///         <paramref name="allowPort" /> is greater than 65,535.</exception>
		// Token: 0x06003BE9 RID: 15337 RVA: 0x000CEFB9 File Offset: 0x000CD1B9
		public static CodeConnectAccess CreateOriginSchemeAccess(int allowPort)
		{
			return new CodeConnectAccess(CodeConnectAccess.OriginScheme, allowPort);
		}

		// Token: 0x06003BEA RID: 15338 RVA: 0x000CEFC6 File Offset: 0x000CD1C6
		// Note: this type is marked as 'beforefieldinit'.
		static CodeConnectAccess()
		{
		}

		/// <summary>Contains the string value that represents the scheme wildcard.</summary>
		// Token: 0x04001ED2 RID: 7890
		public static readonly string AnyScheme = "*";

		/// <summary>Contains the value used to represent the default port.</summary>
		// Token: 0x04001ED3 RID: 7891
		public static readonly int DefaultPort = -3;

		/// <summary>Contains the value used to represent the port value in the URI where code originated.</summary>
		// Token: 0x04001ED4 RID: 7892
		public static readonly int OriginPort = -4;

		/// <summary>Contains the value used to represent the scheme in the URL where the code originated.</summary>
		// Token: 0x04001ED5 RID: 7893
		public static readonly string OriginScheme = "$origin";

		// Token: 0x04001ED6 RID: 7894
		private string _scheme;

		// Token: 0x04001ED7 RID: 7895
		private int _port;
	}
}
