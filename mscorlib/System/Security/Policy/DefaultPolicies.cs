﻿using System;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x02000577 RID: 1399
	internal static class DefaultPolicies
	{
		// Token: 0x06003C09 RID: 15369 RVA: 0x000CF5C4 File Offset: 0x000CD7C4
		public static PermissionSet GetSpecialPermissionSet(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			uint num = <PrivateImplementationDetails>.ComputeStringHash(name);
			if (num <= 2314740779U)
			{
				if (num != 734303062U)
				{
					if (num != 753551658U)
					{
						if (num == 2314740779U)
						{
							if (name == "LocalIntranet")
							{
								return DefaultPolicies.LocalIntranet;
							}
						}
					}
					else if (name == "Nothing")
					{
						return DefaultPolicies.Nothing;
					}
				}
				else if (name == "FullTrust")
				{
					return DefaultPolicies.FullTrust;
				}
			}
			else if (num <= 3132872517U)
			{
				if (num != 2939433820U)
				{
					if (num == 3132872517U)
					{
						if (name == "SkipVerification")
						{
							return DefaultPolicies.SkipVerification;
						}
					}
				}
				else if (name == "Internet")
				{
					return DefaultPolicies.Internet;
				}
			}
			else if (num != 3650199797U)
			{
				if (num == 4030759744U)
				{
					if (name == "Everything")
					{
						return DefaultPolicies.Everything;
					}
				}
			}
			else if (name == "Execution")
			{
				return DefaultPolicies.Execution;
			}
			return null;
		}

		// Token: 0x170009D1 RID: 2513
		// (get) Token: 0x06003C0A RID: 15370 RVA: 0x000CF6D7 File Offset: 0x000CD8D7
		public static PermissionSet FullTrust
		{
			get
			{
				if (DefaultPolicies._fullTrust == null)
				{
					DefaultPolicies._fullTrust = DefaultPolicies.BuildFullTrust();
				}
				return DefaultPolicies._fullTrust;
			}
		}

		// Token: 0x170009D2 RID: 2514
		// (get) Token: 0x06003C0B RID: 15371 RVA: 0x000CF6EF File Offset: 0x000CD8EF
		public static PermissionSet LocalIntranet
		{
			get
			{
				if (DefaultPolicies._localIntranet == null)
				{
					DefaultPolicies._localIntranet = DefaultPolicies.BuildLocalIntranet();
				}
				return DefaultPolicies._localIntranet;
			}
		}

		// Token: 0x170009D3 RID: 2515
		// (get) Token: 0x06003C0C RID: 15372 RVA: 0x000CF707 File Offset: 0x000CD907
		public static PermissionSet Internet
		{
			get
			{
				if (DefaultPolicies._internet == null)
				{
					DefaultPolicies._internet = DefaultPolicies.BuildInternet();
				}
				return DefaultPolicies._internet;
			}
		}

		// Token: 0x170009D4 RID: 2516
		// (get) Token: 0x06003C0D RID: 15373 RVA: 0x000CF71F File Offset: 0x000CD91F
		public static PermissionSet SkipVerification
		{
			get
			{
				if (DefaultPolicies._skipVerification == null)
				{
					DefaultPolicies._skipVerification = DefaultPolicies.BuildSkipVerification();
				}
				return DefaultPolicies._skipVerification;
			}
		}

		// Token: 0x170009D5 RID: 2517
		// (get) Token: 0x06003C0E RID: 15374 RVA: 0x000CF737 File Offset: 0x000CD937
		public static PermissionSet Execution
		{
			get
			{
				if (DefaultPolicies._execution == null)
				{
					DefaultPolicies._execution = DefaultPolicies.BuildExecution();
				}
				return DefaultPolicies._execution;
			}
		}

		// Token: 0x170009D6 RID: 2518
		// (get) Token: 0x06003C0F RID: 15375 RVA: 0x000CF74F File Offset: 0x000CD94F
		public static PermissionSet Nothing
		{
			get
			{
				if (DefaultPolicies._nothing == null)
				{
					DefaultPolicies._nothing = DefaultPolicies.BuildNothing();
				}
				return DefaultPolicies._nothing;
			}
		}

		// Token: 0x170009D7 RID: 2519
		// (get) Token: 0x06003C10 RID: 15376 RVA: 0x000CF767 File Offset: 0x000CD967
		public static PermissionSet Everything
		{
			get
			{
				if (DefaultPolicies._everything == null)
				{
					DefaultPolicies._everything = DefaultPolicies.BuildEverything();
				}
				return DefaultPolicies._everything;
			}
		}

		// Token: 0x06003C11 RID: 15377 RVA: 0x000CF780 File Offset: 0x000CD980
		public static StrongNameMembershipCondition FullTrustMembership(string name, DefaultPolicies.Key key)
		{
			StrongNamePublicKeyBlob blob = null;
			if (key != DefaultPolicies.Key.Ecma)
			{
				if (key == DefaultPolicies.Key.MsFinal)
				{
					if (DefaultPolicies._msFinal == null)
					{
						DefaultPolicies._msFinal = new StrongNamePublicKeyBlob(DefaultPolicies._msFinalKey);
					}
					blob = DefaultPolicies._msFinal;
				}
			}
			else
			{
				if (DefaultPolicies._ecma == null)
				{
					DefaultPolicies._ecma = new StrongNamePublicKeyBlob(DefaultPolicies._ecmaKey);
				}
				blob = DefaultPolicies._ecma;
			}
			if (DefaultPolicies._fxVersion == null)
			{
				DefaultPolicies._fxVersion = new Version("4.0.0.0");
			}
			return new StrongNameMembershipCondition(blob, name, DefaultPolicies._fxVersion);
		}

		// Token: 0x06003C12 RID: 15378 RVA: 0x000CF7FA File Offset: 0x000CD9FA
		private static NamedPermissionSet BuildFullTrust()
		{
			return new NamedPermissionSet("FullTrust", PermissionState.Unrestricted);
		}

		// Token: 0x06003C13 RID: 15379 RVA: 0x000CF808 File Offset: 0x000CDA08
		private static NamedPermissionSet BuildLocalIntranet()
		{
			NamedPermissionSet namedPermissionSet = new NamedPermissionSet("LocalIntranet", PermissionState.None);
			namedPermissionSet.AddPermission(new EnvironmentPermission(EnvironmentPermissionAccess.Read, "USERNAME;USER"));
			namedPermissionSet.AddPermission(new FileDialogPermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(new IsolatedStorageFilePermission(PermissionState.None)
			{
				UsageAllowed = IsolatedStorageContainment.AssemblyIsolationByUser,
				UserQuota = long.MaxValue
			});
			namedPermissionSet.AddPermission(new ReflectionPermission(ReflectionPermissionFlag.ReflectionEmit));
			SecurityPermissionFlag flag = SecurityPermissionFlag.Assertion | SecurityPermissionFlag.Execution;
			namedPermissionSet.AddPermission(new SecurityPermission(flag));
			namedPermissionSet.AddPermission(new UIPermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Net.DnsPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create(DefaultPolicies.PrintingPermission("SafePrinting")));
			return namedPermissionSet;
		}

		// Token: 0x06003C14 RID: 15380 RVA: 0x000CF8B8 File Offset: 0x000CDAB8
		private static NamedPermissionSet BuildInternet()
		{
			NamedPermissionSet namedPermissionSet = new NamedPermissionSet("Internet", PermissionState.None);
			namedPermissionSet.AddPermission(new FileDialogPermission(FileDialogPermissionAccess.Open));
			namedPermissionSet.AddPermission(new IsolatedStorageFilePermission(PermissionState.None)
			{
				UsageAllowed = IsolatedStorageContainment.DomainIsolationByUser,
				UserQuota = 512000L
			});
			namedPermissionSet.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));
			namedPermissionSet.AddPermission(new UIPermission(UIPermissionWindow.SafeTopLevelWindows, UIPermissionClipboard.OwnClipboard));
			namedPermissionSet.AddPermission(PermissionBuilder.Create(DefaultPolicies.PrintingPermission("SafePrinting")));
			return namedPermissionSet;
		}

		// Token: 0x06003C15 RID: 15381 RVA: 0x000CF931 File Offset: 0x000CDB31
		private static NamedPermissionSet BuildSkipVerification()
		{
			NamedPermissionSet namedPermissionSet = new NamedPermissionSet("SkipVerification", PermissionState.None);
			namedPermissionSet.AddPermission(new SecurityPermission(SecurityPermissionFlag.SkipVerification));
			return namedPermissionSet;
		}

		// Token: 0x06003C16 RID: 15382 RVA: 0x000CF94B File Offset: 0x000CDB4B
		private static NamedPermissionSet BuildExecution()
		{
			NamedPermissionSet namedPermissionSet = new NamedPermissionSet("Execution", PermissionState.None);
			namedPermissionSet.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));
			return namedPermissionSet;
		}

		// Token: 0x06003C17 RID: 15383 RVA: 0x000CF965 File Offset: 0x000CDB65
		private static NamedPermissionSet BuildNothing()
		{
			return new NamedPermissionSet("Nothing", PermissionState.None);
		}

		// Token: 0x06003C18 RID: 15384 RVA: 0x000CF974 File Offset: 0x000CDB74
		private static NamedPermissionSet BuildEverything()
		{
			NamedPermissionSet namedPermissionSet = new NamedPermissionSet("Everything", PermissionState.None);
			namedPermissionSet.AddPermission(new EnvironmentPermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(new FileDialogPermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(new FileIOPermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(new IsolatedStorageFilePermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(new ReflectionPermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(new RegistryPermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(new KeyContainerPermission(PermissionState.Unrestricted));
			SecurityPermissionFlag securityPermissionFlag = SecurityPermissionFlag.AllFlags;
			securityPermissionFlag &= ~SecurityPermissionFlag.SkipVerification;
			namedPermissionSet.AddPermission(new SecurityPermission(securityPermissionFlag));
			namedPermissionSet.AddPermission(new UIPermission(PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Net.DnsPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Drawing.Printing.PrintingPermission, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Diagnostics.EventLogPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Net.SocketPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Net.WebPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Diagnostics.PerformanceCounterPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.DirectoryServices.DirectoryServicesPermission, System.DirectoryServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Messaging.MessageQueuePermission, System.Messaging, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.ServiceProcess.ServiceControllerPermission, System.ServiceProcess, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Data.OleDb.OleDbPermission, System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", PermissionState.Unrestricted));
			namedPermissionSet.AddPermission(PermissionBuilder.Create("System.Data.SqlClient.SqlClientPermission, System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089", PermissionState.Unrestricted));
			return namedPermissionSet;
		}

		// Token: 0x06003C19 RID: 15385 RVA: 0x000CFAD2 File Offset: 0x000CDCD2
		private static SecurityElement PrintingPermission(string level)
		{
			SecurityElement securityElement = new SecurityElement("IPermission");
			securityElement.AddAttribute("class", "System.Drawing.Printing.PrintingPermission, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");
			securityElement.AddAttribute("version", "1");
			securityElement.AddAttribute("Level", level);
			return securityElement;
		}

		// Token: 0x06003C1A RID: 15386 RVA: 0x000CFB0A File Offset: 0x000CDD0A
		// Note: this type is marked as 'beforefieldinit'.
		static DefaultPolicies()
		{
			byte[] array = new byte[16];
			array[8] = 4;
			DefaultPolicies._ecmaKey = array;
			DefaultPolicies._msFinalKey = new byte[]
			{
				0,
				36,
				0,
				0,
				4,
				128,
				0,
				0,
				148,
				0,
				0,
				0,
				6,
				2,
				0,
				0,
				0,
				36,
				0,
				0,
				82,
				83,
				65,
				49,
				0,
				4,
				0,
				0,
				1,
				0,
				1,
				0,
				7,
				209,
				250,
				87,
				196,
				174,
				217,
				240,
				163,
				46,
				132,
				170,
				15,
				174,
				253,
				13,
				233,
				232,
				253,
				106,
				236,
				143,
				135,
				251,
				3,
				118,
				108,
				131,
				76,
				153,
				146,
				30,
				178,
				59,
				231,
				154,
				217,
				213,
				220,
				193,
				221,
				154,
				210,
				54,
				19,
				33,
				2,
				144,
				11,
				114,
				60,
				249,
				128,
				149,
				127,
				196,
				225,
				119,
				16,
				143,
				198,
				7,
				119,
				79,
				41,
				232,
				50,
				14,
				146,
				234,
				5,
				236,
				228,
				232,
				33,
				192,
				165,
				239,
				232,
				241,
				100,
				92,
				76,
				12,
				147,
				193,
				171,
				153,
				40,
				93,
				98,
				44,
				170,
				101,
				44,
				29,
				250,
				214,
				61,
				116,
				93,
				111,
				45,
				229,
				241,
				126,
				94,
				175,
				15,
				196,
				150,
				61,
				38,
				28,
				138,
				18,
				67,
				101,
				24,
				32,
				109,
				192,
				147,
				52,
				77,
				90,
				210,
				147
			};
		}

		// Token: 0x04001EDD RID: 7901
		private const string DnsPermissionClass = "System.Net.DnsPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

		// Token: 0x04001EDE RID: 7902
		private const string EventLogPermissionClass = "System.Diagnostics.EventLogPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

		// Token: 0x04001EDF RID: 7903
		private const string PrintingPermissionClass = "System.Drawing.Printing.PrintingPermission, System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

		// Token: 0x04001EE0 RID: 7904
		private const string SocketPermissionClass = "System.Net.SocketPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

		// Token: 0x04001EE1 RID: 7905
		private const string WebPermissionClass = "System.Net.WebPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

		// Token: 0x04001EE2 RID: 7906
		private const string PerformanceCounterPermissionClass = "System.Diagnostics.PerformanceCounterPermission, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

		// Token: 0x04001EE3 RID: 7907
		private const string DirectoryServicesPermissionClass = "System.DirectoryServices.DirectoryServicesPermission, System.DirectoryServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

		// Token: 0x04001EE4 RID: 7908
		private const string MessageQueuePermissionClass = "System.Messaging.MessageQueuePermission, System.Messaging, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

		// Token: 0x04001EE5 RID: 7909
		private const string ServiceControllerPermissionClass = "System.ServiceProcess.ServiceControllerPermission, System.ServiceProcess, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a";

		// Token: 0x04001EE6 RID: 7910
		private const string OleDbPermissionClass = "System.Data.OleDb.OleDbPermission, System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

		// Token: 0x04001EE7 RID: 7911
		private const string SqlClientPermissionClass = "System.Data.SqlClient.SqlClientPermission, System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089";

		// Token: 0x04001EE8 RID: 7912
		private static Version _fxVersion;

		// Token: 0x04001EE9 RID: 7913
		private static byte[] _ecmaKey;

		// Token: 0x04001EEA RID: 7914
		private static StrongNamePublicKeyBlob _ecma;

		// Token: 0x04001EEB RID: 7915
		private static byte[] _msFinalKey;

		// Token: 0x04001EEC RID: 7916
		private static StrongNamePublicKeyBlob _msFinal;

		// Token: 0x04001EED RID: 7917
		private static NamedPermissionSet _fullTrust;

		// Token: 0x04001EEE RID: 7918
		private static NamedPermissionSet _localIntranet;

		// Token: 0x04001EEF RID: 7919
		private static NamedPermissionSet _internet;

		// Token: 0x04001EF0 RID: 7920
		private static NamedPermissionSet _skipVerification;

		// Token: 0x04001EF1 RID: 7921
		private static NamedPermissionSet _execution;

		// Token: 0x04001EF2 RID: 7922
		private static NamedPermissionSet _nothing;

		// Token: 0x04001EF3 RID: 7923
		private static NamedPermissionSet _everything;

		// Token: 0x02000578 RID: 1400
		public static class ReservedNames
		{
			// Token: 0x06003C1B RID: 15387 RVA: 0x000CFB38 File Offset: 0x000CDD38
			public static bool IsReserved(string name)
			{
				uint num = <PrivateImplementationDetails>.ComputeStringHash(name);
				if (num <= 2314740779U)
				{
					if (num != 734303062U)
					{
						if (num != 753551658U)
						{
							if (num != 2314740779U)
							{
								return false;
							}
							if (!(name == "LocalIntranet"))
							{
								return false;
							}
						}
						else if (!(name == "Nothing"))
						{
							return false;
						}
					}
					else if (!(name == "FullTrust"))
					{
						return false;
					}
				}
				else if (num <= 3132872517U)
				{
					if (num != 2939433820U)
					{
						if (num != 3132872517U)
						{
							return false;
						}
						if (!(name == "SkipVerification"))
						{
							return false;
						}
					}
					else if (!(name == "Internet"))
					{
						return false;
					}
				}
				else if (num != 3650199797U)
				{
					if (num != 4030759744U)
					{
						return false;
					}
					if (!(name == "Everything"))
					{
						return false;
					}
				}
				else if (!(name == "Execution"))
				{
					return false;
				}
				return true;
			}

			// Token: 0x04001EF4 RID: 7924
			public const string FullTrust = "FullTrust";

			// Token: 0x04001EF5 RID: 7925
			public const string LocalIntranet = "LocalIntranet";

			// Token: 0x04001EF6 RID: 7926
			public const string Internet = "Internet";

			// Token: 0x04001EF7 RID: 7927
			public const string SkipVerification = "SkipVerification";

			// Token: 0x04001EF8 RID: 7928
			public const string Execution = "Execution";

			// Token: 0x04001EF9 RID: 7929
			public const string Nothing = "Nothing";

			// Token: 0x04001EFA RID: 7930
			public const string Everything = "Everything";
		}

		// Token: 0x02000579 RID: 1401
		public enum Key
		{
			// Token: 0x04001EFC RID: 7932
			Ecma,
			// Token: 0x04001EFD RID: 7933
			MsFinal
		}
	}
}
