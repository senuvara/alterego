﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	/// <summary>Confirms that a code assembly originates in the global assembly cache (GAC) as evidence for policy evaluation. This class cannot be inherited.</summary>
	// Token: 0x0200057F RID: 1407
	[ComVisible(true)]
	[Serializable]
	public sealed class GacInstalled : EvidenceBase, IIdentityPermissionFactory, IBuiltInEvidence
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.GacInstalled" /> class. </summary>
		// Token: 0x06003C50 RID: 15440 RVA: 0x000D04CF File Offset: 0x000CE6CF
		public GacInstalled()
		{
		}

		/// <summary>Creates an equivalent copy of the current object.</summary>
		/// <returns>An equivalent copy of <see cref="T:System.Security.Policy.GacInstalled" />.</returns>
		// Token: 0x06003C51 RID: 15441 RVA: 0x000D04D7 File Offset: 0x000CE6D7
		public object Copy()
		{
			return new GacInstalled();
		}

		/// <summary>Creates a new identity permission that corresponds to the current object.</summary>
		/// <param name="evidence">The <see cref="T:System.Security.Policy.Evidence" /> from which to construct the identity permission. </param>
		/// <returns>A new identity permission that corresponds to the current object.</returns>
		// Token: 0x06003C52 RID: 15442 RVA: 0x000D04DE File Offset: 0x000CE6DE
		public IPermission CreateIdentityPermission(Evidence evidence)
		{
			return new GacIdentityPermission();
		}

		/// <summary>Indicates whether the current object is equivalent to the specified object.</summary>
		/// <param name="o">The object to compare with the current object. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="o" /> is a <see cref="T:System.Security.Policy.GacInstalled" /> object; otherwise, <see langword="false" />.</returns>
		// Token: 0x06003C53 RID: 15443 RVA: 0x000D04E5 File Offset: 0x000CE6E5
		public override bool Equals(object o)
		{
			return o != null && o is GacInstalled;
		}

		/// <summary>Returns a hash code for the current object.</summary>
		/// <returns>A hash code for the current object.</returns>
		// Token: 0x06003C54 RID: 15444 RVA: 0x00002526 File Offset: 0x00000726
		public override int GetHashCode()
		{
			return 0;
		}

		/// <summary>Returns a string representation of the current  object.</summary>
		/// <returns>A string representation of the current object.</returns>
		// Token: 0x06003C55 RID: 15445 RVA: 0x000D04F5 File Offset: 0x000CE6F5
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement(base.GetType().FullName);
			securityElement.AddAttribute("version", "1");
			return securityElement.ToString();
		}

		// Token: 0x06003C56 RID: 15446 RVA: 0x00004E08 File Offset: 0x00003008
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return 1;
		}

		// Token: 0x06003C57 RID: 15447 RVA: 0x0006DB95 File Offset: 0x0006BD95
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return position;
		}

		// Token: 0x06003C58 RID: 15448 RVA: 0x000D051C File Offset: 0x000CE71C
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			buffer[position] = '\t';
			return position + 1;
		}
	}
}
