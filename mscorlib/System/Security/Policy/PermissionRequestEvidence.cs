﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	/// <summary>Defines evidence that represents permission requests. This class cannot be inherited.</summary>
	// Token: 0x0200058B RID: 1419
	[ComVisible(true)]
	[Serializable]
	public sealed class PermissionRequestEvidence : EvidenceBase, IBuiltInEvidence
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.PermissionRequestEvidence" /> class with the permission request of a code assembly.</summary>
		/// <param name="request">The minimum permissions the code requires to run. </param>
		/// <param name="optional">The permissions the code can use if they are granted, but that are not required. </param>
		/// <param name="denied">The permissions the code explicitly asks not to be granted. </param>
		// Token: 0x06003CA7 RID: 15527 RVA: 0x000D1192 File Offset: 0x000CF392
		public PermissionRequestEvidence(PermissionSet request, PermissionSet optional, PermissionSet denied)
		{
			if (request != null)
			{
				this.requested = new PermissionSet(request);
			}
			if (optional != null)
			{
				this.optional = new PermissionSet(optional);
			}
			if (denied != null)
			{
				this.denied = new PermissionSet(denied);
			}
		}

		/// <summary>Gets the permissions the code explicitly asks not to be granted.</summary>
		/// <returns>The permissions the code explicitly asks not to be granted.</returns>
		// Token: 0x170009EC RID: 2540
		// (get) Token: 0x06003CA8 RID: 15528 RVA: 0x000D11C7 File Offset: 0x000CF3C7
		public PermissionSet DeniedPermissions
		{
			get
			{
				return this.denied;
			}
		}

		/// <summary>Gets the permissions the code can use if they are granted, but are not required.</summary>
		/// <returns>The permissions the code can use if they are granted, but are not required.</returns>
		// Token: 0x170009ED RID: 2541
		// (get) Token: 0x06003CA9 RID: 15529 RVA: 0x000D11CF File Offset: 0x000CF3CF
		public PermissionSet OptionalPermissions
		{
			get
			{
				return this.optional;
			}
		}

		/// <summary>Gets the minimum permissions the code requires to run.</summary>
		/// <returns>The minimum permissions the code requires to run.</returns>
		// Token: 0x170009EE RID: 2542
		// (get) Token: 0x06003CAA RID: 15530 RVA: 0x000D11D7 File Offset: 0x000CF3D7
		public PermissionSet RequestedPermissions
		{
			get
			{
				return this.requested;
			}
		}

		/// <summary>Creates an equivalent copy of the current <see cref="T:System.Security.Policy.PermissionRequestEvidence" />.</summary>
		/// <returns>An equivalent copy of the current <see cref="T:System.Security.Policy.PermissionRequestEvidence" />.</returns>
		// Token: 0x06003CAB RID: 15531 RVA: 0x000D11DF File Offset: 0x000CF3DF
		public PermissionRequestEvidence Copy()
		{
			return new PermissionRequestEvidence(this.requested, this.optional, this.denied);
		}

		/// <summary>Gets a string representation of the state of the <see cref="T:System.Security.Policy.PermissionRequestEvidence" />.</summary>
		/// <returns>A representation of the state of the <see cref="T:System.Security.Policy.PermissionRequestEvidence" />.</returns>
		// Token: 0x06003CAC RID: 15532 RVA: 0x000D11F8 File Offset: 0x000CF3F8
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement("System.Security.Policy.PermissionRequestEvidence");
			securityElement.AddAttribute("version", "1");
			if (this.requested != null)
			{
				SecurityElement securityElement2 = new SecurityElement("Request");
				securityElement2.AddChild(this.requested.ToXml());
				securityElement.AddChild(securityElement2);
			}
			if (this.optional != null)
			{
				SecurityElement securityElement3 = new SecurityElement("Optional");
				securityElement3.AddChild(this.optional.ToXml());
				securityElement.AddChild(securityElement3);
			}
			if (this.denied != null)
			{
				SecurityElement securityElement4 = new SecurityElement("Denied");
				securityElement4.AddChild(this.denied.ToXml());
				securityElement.AddChild(securityElement4);
			}
			return securityElement.ToString();
		}

		// Token: 0x06003CAD RID: 15533 RVA: 0x000D12A8 File Offset: 0x000CF4A8
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			int num = verbose ? 3 : 1;
			if (this.requested != null)
			{
				int num2 = this.requested.ToXml().ToString().Length + (verbose ? 5 : 0);
				num += num2;
			}
			if (this.optional != null)
			{
				int num3 = this.optional.ToXml().ToString().Length + (verbose ? 5 : 0);
				num += num3;
			}
			if (this.denied != null)
			{
				int num4 = this.denied.ToXml().ToString().Length + (verbose ? 5 : 0);
				num += num4;
			}
			return num;
		}

		// Token: 0x06003CAE RID: 15534 RVA: 0x00002526 File Offset: 0x00000726
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003CAF RID: 15535 RVA: 0x00002526 File Offset: 0x00000726
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x04001F13 RID: 7955
		private PermissionSet requested;

		// Token: 0x04001F14 RID: 7956
		private PermissionSet optional;

		// Token: 0x04001F15 RID: 7957
		private PermissionSet denied;
	}
}
