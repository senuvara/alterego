﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Unity;

namespace System.Security.Policy
{
	/// <summary>Defines the set of information that constitutes input to security policy decisions. This class cannot be inherited.</summary>
	// Token: 0x0200057A RID: 1402
	[MonoTODO("Serialization format not compatible with .NET")]
	[ComVisible(true)]
	[Serializable]
	public sealed class Evidence : ICollection, IEnumerable
	{
		/// <summary>Initializes a new empty instance of the <see cref="T:System.Security.Policy.Evidence" /> class.</summary>
		// Token: 0x06003C1C RID: 15388 RVA: 0x00002050 File Offset: 0x00000250
		public Evidence()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.Evidence" /> class from a shallow copy of an existing one.</summary>
		/// <param name="evidence">The <see cref="T:System.Security.Policy.Evidence" /> instance from which to create the new instance. This instance is not deep-copied. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="evidence" /> parameter is not a valid instance of <see cref="T:System.Security.Policy.Evidence" />. </exception>
		// Token: 0x06003C1D RID: 15389 RVA: 0x000CFC0A File Offset: 0x000CDE0A
		public Evidence(Evidence evidence)
		{
			if (evidence != null)
			{
				this.Merge(evidence);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.Evidence" /> class from multiple sets of host and assembly evidence.</summary>
		/// <param name="hostEvidence">The host evidence from which to create the new instance. </param>
		/// <param name="assemblyEvidence">The assembly evidence from which to create the new instance. </param>
		// Token: 0x06003C1E RID: 15390 RVA: 0x000CFC1C File Offset: 0x000CDE1C
		[Obsolete]
		public Evidence(object[] hostEvidence, object[] assemblyEvidence)
		{
			if (hostEvidence != null)
			{
				this.HostEvidenceList.AddRange(hostEvidence);
			}
			if (assemblyEvidence != null)
			{
				this.AssemblyEvidenceList.AddRange(assemblyEvidence);
			}
		}

		/// <summary>Gets the number of evidence objects in the evidence set.</summary>
		/// <returns>The number of evidence objects in the evidence set.</returns>
		// Token: 0x170009D8 RID: 2520
		// (get) Token: 0x06003C1F RID: 15391 RVA: 0x000CFC44 File Offset: 0x000CDE44
		[Obsolete]
		public int Count
		{
			get
			{
				int num = 0;
				if (this.hostEvidenceList != null)
				{
					num += this.hostEvidenceList.Count;
				}
				if (this.assemblyEvidenceList != null)
				{
					num += this.assemblyEvidenceList.Count;
				}
				return num;
			}
		}

		/// <summary>Gets a value indicating whether the evidence set is read-only.</summary>
		/// <returns>Always <see langword="false" />, because read-only evidence sets are not supported.</returns>
		// Token: 0x170009D9 RID: 2521
		// (get) Token: 0x06003C20 RID: 15392 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets a value indicating whether the evidence set is thread-safe.</summary>
		/// <returns>Always <see langword="false" /> because thread-safe evidence sets are not supported.</returns>
		// Token: 0x170009DA RID: 2522
		// (get) Token: 0x06003C21 RID: 15393 RVA: 0x00002526 File Offset: 0x00000726
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets or sets a value indicating whether the evidence is locked.</summary>
		/// <returns>
		///     <see langword="true" /> if the evidence is locked; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x170009DB RID: 2523
		// (get) Token: 0x06003C22 RID: 15394 RVA: 0x000CFC80 File Offset: 0x000CDE80
		// (set) Token: 0x06003C23 RID: 15395 RVA: 0x000CFC88 File Offset: 0x000CDE88
		public bool Locked
		{
			get
			{
				return this._locked;
			}
			[SecurityPermission(SecurityAction.Demand, ControlEvidence = true)]
			set
			{
				this._locked = value;
			}
		}

		/// <summary>Gets the synchronization root.</summary>
		/// <returns>Always <see langword="this" /> (<see langword="Me" /> in Visual Basic), because synchronization of evidence sets is not supported.</returns>
		// Token: 0x170009DC RID: 2524
		// (get) Token: 0x06003C24 RID: 15396 RVA: 0x00002058 File Offset: 0x00000258
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x170009DD RID: 2525
		// (get) Token: 0x06003C25 RID: 15397 RVA: 0x000CFC91 File Offset: 0x000CDE91
		internal ArrayList HostEvidenceList
		{
			get
			{
				if (this.hostEvidenceList == null)
				{
					this.hostEvidenceList = ArrayList.Synchronized(new ArrayList());
				}
				return this.hostEvidenceList;
			}
		}

		// Token: 0x170009DE RID: 2526
		// (get) Token: 0x06003C26 RID: 15398 RVA: 0x000CFCB1 File Offset: 0x000CDEB1
		internal ArrayList AssemblyEvidenceList
		{
			get
			{
				if (this.assemblyEvidenceList == null)
				{
					this.assemblyEvidenceList = ArrayList.Synchronized(new ArrayList());
				}
				return this.assemblyEvidenceList;
			}
		}

		/// <summary>Adds the specified assembly evidence to the evidence set.</summary>
		/// <param name="id">Any evidence object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="id" /> is null.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="id" /> is not serializable.</exception>
		// Token: 0x06003C27 RID: 15399 RVA: 0x000CFCD1 File Offset: 0x000CDED1
		[Obsolete]
		public void AddAssembly(object id)
		{
			this.AssemblyEvidenceList.Add(id);
		}

		/// <summary>Adds the specified evidence supplied by the host to the evidence set.</summary>
		/// <param name="id">Any evidence object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="id" /> is null.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="id" /> is not serializable.</exception>
		// Token: 0x06003C28 RID: 15400 RVA: 0x000CFCE0 File Offset: 0x000CDEE0
		[Obsolete]
		public void AddHost(object id)
		{
			if (this._locked)
			{
				bool securityEnabled = SecurityManager.SecurityEnabled;
			}
			this.HostEvidenceList.Add(id);
		}

		/// <summary>Removes the host and assembly evidence from the evidence set.</summary>
		// Token: 0x06003C29 RID: 15401 RVA: 0x000CFCFD File Offset: 0x000CDEFD
		[ComVisible(false)]
		public void Clear()
		{
			if (this.hostEvidenceList != null)
			{
				this.hostEvidenceList.Clear();
			}
			if (this.assemblyEvidenceList != null)
			{
				this.assemblyEvidenceList.Clear();
			}
		}

		/// <summary>Returns a duplicate copy of this evidence object.</summary>
		/// <returns>A duplicate copy of this evidence object.</returns>
		// Token: 0x06003C2A RID: 15402 RVA: 0x000CFD25 File Offset: 0x000CDF25
		[ComVisible(false)]
		public Evidence Clone()
		{
			return new Evidence(this);
		}

		/// <summary>Copies evidence objects to an <see cref="T:System.Array" />.</summary>
		/// <param name="array">The target array to which to copy evidence objects. </param>
		/// <param name="index">The zero-based position in the array to which to begin copying evidence objects. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is null.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index " />is outside the range of the target array<paramref name="." /></exception>
		// Token: 0x06003C2B RID: 15403 RVA: 0x000CFD30 File Offset: 0x000CDF30
		[Obsolete]
		public void CopyTo(Array array, int index)
		{
			int num = 0;
			if (this.hostEvidenceList != null)
			{
				num = this.hostEvidenceList.Count;
				if (num > 0)
				{
					this.hostEvidenceList.CopyTo(array, index);
				}
			}
			if (this.assemblyEvidenceList != null && this.assemblyEvidenceList.Count > 0)
			{
				this.assemblyEvidenceList.CopyTo(array, index + num);
			}
		}

		/// <summary>Enumerates all evidence in the set, both that provided by the host and that provided by the assembly.</summary>
		/// <returns>An enumerator for evidence added by both the <see cref="M:System.Security.Policy.Evidence.AddHost(System.Object)" /> method and the <see cref="M:System.Security.Policy.Evidence.AddAssembly(System.Object)" /> method.</returns>
		// Token: 0x06003C2C RID: 15404 RVA: 0x000CFD8C File Offset: 0x000CDF8C
		[Obsolete]
		public IEnumerator GetEnumerator()
		{
			IEnumerator hostenum = null;
			if (this.hostEvidenceList != null)
			{
				hostenum = this.hostEvidenceList.GetEnumerator();
			}
			IEnumerator assemblyenum = null;
			if (this.assemblyEvidenceList != null)
			{
				assemblyenum = this.assemblyEvidenceList.GetEnumerator();
			}
			return new Evidence.EvidenceEnumerator(hostenum, assemblyenum);
		}

		/// <summary>Enumerates evidence provided by the assembly.</summary>
		/// <returns>An enumerator for evidence added by the <see cref="M:System.Security.Policy.Evidence.AddAssembly(System.Object)" /> method.</returns>
		// Token: 0x06003C2D RID: 15405 RVA: 0x000CFDCC File Offset: 0x000CDFCC
		public IEnumerator GetAssemblyEnumerator()
		{
			return this.AssemblyEvidenceList.GetEnumerator();
		}

		/// <summary>Enumerates evidence supplied by the host.</summary>
		/// <returns>An enumerator for evidence added by the <see cref="M:System.Security.Policy.Evidence.AddHost(System.Object)" /> method.</returns>
		// Token: 0x06003C2E RID: 15406 RVA: 0x000CFDD9 File Offset: 0x000CDFD9
		public IEnumerator GetHostEnumerator()
		{
			return this.HostEvidenceList.GetEnumerator();
		}

		/// <summary>Merges the specified evidence set into the current evidence set.</summary>
		/// <param name="evidence">The evidence set to be merged into the current evidence set. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="evidence" /> parameter is not a valid instance of <see cref="T:System.Security.Policy.Evidence" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">
		///         <see cref="P:System.Security.Policy.Evidence.Locked" /> is <see langword="true" />, the code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlEvidence" />, and the <paramref name="evidence" /> parameter has a host list that is not empty. </exception>
		// Token: 0x06003C2F RID: 15407 RVA: 0x000CFDE8 File Offset: 0x000CDFE8
		public void Merge(Evidence evidence)
		{
			if (evidence != null && evidence.Count > 0)
			{
				if (evidence.hostEvidenceList != null)
				{
					foreach (object id in evidence.hostEvidenceList)
					{
						this.AddHost(id);
					}
				}
				if (evidence.assemblyEvidenceList != null)
				{
					foreach (object id2 in evidence.assemblyEvidenceList)
					{
						this.AddAssembly(id2);
					}
				}
			}
		}

		/// <summary>Removes the evidence for a given type from the host and assembly enumerations.</summary>
		/// <param name="t">The type of the evidence to be removed. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="t" /> is null.</exception>
		// Token: 0x06003C30 RID: 15408 RVA: 0x000CFEA0 File Offset: 0x000CE0A0
		[ComVisible(false)]
		public void RemoveType(Type t)
		{
			for (int i = this.hostEvidenceList.Count; i >= 0; i--)
			{
				if (this.hostEvidenceList.GetType() == t)
				{
					this.hostEvidenceList.RemoveAt(i);
				}
			}
			for (int j = this.assemblyEvidenceList.Count; j >= 0; j--)
			{
				if (this.assemblyEvidenceList.GetType() == t)
				{
					this.assemblyEvidenceList.RemoveAt(j);
				}
			}
		}

		// Token: 0x06003C31 RID: 15409
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsAuthenticodePresent(Assembly a);

		// Token: 0x06003C32 RID: 15410 RVA: 0x000CFF17 File Offset: 0x000CE117
		internal static Evidence GetDefaultHostEvidence(Assembly a)
		{
			return new Evidence();
		}

		/// <summary>Adds an evidence object of the specified type to the assembly-supplied evidence list. </summary>
		/// <param name="evidence">The assembly evidence to add.</param>
		/// <typeparam name="T">The type of the object in <paramref name="evidence" />.</typeparam>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="evidence" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">Evidence of type <paramref name="T" /> is already in the list.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="evidence" /> is not serializable.</exception>
		// Token: 0x06003C33 RID: 15411 RVA: 0x00002ABD File Offset: 0x00000CBD
		[ComVisible(false)]
		public void AddAssemblyEvidence<T>(T evidence)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Adds host evidence of the specified type to the host evidence collection.</summary>
		/// <param name="evidence">The host evidence to add.</param>
		/// <typeparam name="T">The type of the object in <paramref name="evidence" />.</typeparam>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="evidence" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">Evidence of type <paramref name="T" /> is already in the list.</exception>
		// Token: 0x06003C34 RID: 15412 RVA: 0x00002ABD File Offset: 0x00000CBD
		[ComVisible(false)]
		public void AddHostEvidence<T>(T evidence)
		{
			ThrowStub.ThrowNotSupportedException();
		}

		/// <summary>Gets assembly evidence of the specified type from the collection.</summary>
		/// <typeparam name="T">The type of the evidence to get.</typeparam>
		/// <returns>Evidence of type <paramref name="T" /> in the assembly evidence collection.</returns>
		// Token: 0x06003C35 RID: 15413 RVA: 0x000CFF20 File Offset: 0x000CE120
		[ComVisible(false)]
		public T GetAssemblyEvidence<T>()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(T);
		}

		/// <summary>Gets host evidence of the specified type from the collection.</summary>
		/// <typeparam name="T">The type of the evidence to get.</typeparam>
		/// <returns>Evidence of type <paramref name="T" /> in the host evidence collection.</returns>
		// Token: 0x06003C36 RID: 15414 RVA: 0x000CFF3C File Offset: 0x000CE13C
		[ComVisible(false)]
		public T GetHostEvidence<T>()
		{
			ThrowStub.ThrowNotSupportedException();
			return default(T);
		}

		// Token: 0x04001EFE RID: 7934
		private bool _locked;

		// Token: 0x04001EFF RID: 7935
		private ArrayList hostEvidenceList;

		// Token: 0x04001F00 RID: 7936
		private ArrayList assemblyEvidenceList;

		// Token: 0x0200057B RID: 1403
		private class EvidenceEnumerator : IEnumerator
		{
			// Token: 0x06003C37 RID: 15415 RVA: 0x000CFF57 File Offset: 0x000CE157
			public EvidenceEnumerator(IEnumerator hostenum, IEnumerator assemblyenum)
			{
				this.hostEnum = hostenum;
				this.assemblyEnum = assemblyenum;
				this.currentEnum = this.hostEnum;
			}

			// Token: 0x06003C38 RID: 15416 RVA: 0x000CFF7C File Offset: 0x000CE17C
			public bool MoveNext()
			{
				if (this.currentEnum == null)
				{
					return false;
				}
				bool flag = this.currentEnum.MoveNext();
				if (!flag && this.hostEnum == this.currentEnum && this.assemblyEnum != null)
				{
					this.currentEnum = this.assemblyEnum;
					flag = this.assemblyEnum.MoveNext();
				}
				return flag;
			}

			// Token: 0x06003C39 RID: 15417 RVA: 0x000CFFD4 File Offset: 0x000CE1D4
			public void Reset()
			{
				if (this.hostEnum != null)
				{
					this.hostEnum.Reset();
					this.currentEnum = this.hostEnum;
				}
				else
				{
					this.currentEnum = this.assemblyEnum;
				}
				if (this.assemblyEnum != null)
				{
					this.assemblyEnum.Reset();
				}
			}

			// Token: 0x170009DF RID: 2527
			// (get) Token: 0x06003C3A RID: 15418 RVA: 0x000D0021 File Offset: 0x000CE221
			public object Current
			{
				get
				{
					return this.currentEnum.Current;
				}
			}

			// Token: 0x04001F01 RID: 7937
			private IEnumerator currentEnum;

			// Token: 0x04001F02 RID: 7938
			private IEnumerator hostEnum;

			// Token: 0x04001F03 RID: 7939
			private IEnumerator assemblyEnum;
		}
	}
}
