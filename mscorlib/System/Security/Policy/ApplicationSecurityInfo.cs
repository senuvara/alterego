﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	/// <summary>Holds the security evidence for an application. This class cannot be inherited.</summary>
	// Token: 0x0200056F RID: 1391
	[ComVisible(true)]
	public sealed class ApplicationSecurityInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.ApplicationSecurityInfo" /> class using the provided activation context. </summary>
		/// <param name="activationContext">An <see cref="T:System.ActivationContext" /> object that uniquely identifies the target application.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="activationContext" /> is <see langword="null" />. </exception>
		// Token: 0x06003BAC RID: 15276 RVA: 0x000CE67C File Offset: 0x000CC87C
		public ApplicationSecurityInfo(ActivationContext activationContext)
		{
			if (activationContext == null)
			{
				throw new ArgumentNullException("activationContext");
			}
		}

		/// <summary>Gets or sets the evidence for the application.</summary>
		/// <returns>An <see cref="T:System.Security.Policy.Evidence" /> object for the application.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Security.Policy.ApplicationSecurityInfo.ApplicationEvidence" /> is set to <see langword="null" />. </exception>
		// Token: 0x170009B4 RID: 2484
		// (get) Token: 0x06003BAD RID: 15277 RVA: 0x000CE692 File Offset: 0x000CC892
		// (set) Token: 0x06003BAE RID: 15278 RVA: 0x000CE69A File Offset: 0x000CC89A
		public Evidence ApplicationEvidence
		{
			get
			{
				return this._evidence;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("ApplicationEvidence");
				}
				this._evidence = value;
			}
		}

		/// <summary>Gets or sets the application identity information.</summary>
		/// <returns>An <see cref="T:System.ApplicationId" /> object.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Security.Policy.ApplicationSecurityInfo.ApplicationId" /> is set to <see langword="null" />.</exception>
		// Token: 0x170009B5 RID: 2485
		// (get) Token: 0x06003BAF RID: 15279 RVA: 0x000CE6B1 File Offset: 0x000CC8B1
		// (set) Token: 0x06003BB0 RID: 15280 RVA: 0x000CE6B9 File Offset: 0x000CC8B9
		public ApplicationId ApplicationId
		{
			get
			{
				return this._appid;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("ApplicationId");
				}
				this._appid = value;
			}
		}

		/// <summary>Gets or sets the default permission set.</summary>
		/// <returns>A <see cref="T:System.Security.PermissionSet" /> object representing the default permissions for the application. The default is a <see cref="T:System.Security.PermissionSet" /> with a permission state of <see cref="F:System.Security.Permissions.PermissionState.None" /></returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Security.Policy.ApplicationSecurityInfo.DefaultRequestSet" /> is set to <see langword="null" />. </exception>
		// Token: 0x170009B6 RID: 2486
		// (get) Token: 0x06003BB1 RID: 15281 RVA: 0x000CE6D0 File Offset: 0x000CC8D0
		// (set) Token: 0x06003BB2 RID: 15282 RVA: 0x000CE6E7 File Offset: 0x000CC8E7
		public PermissionSet DefaultRequestSet
		{
			get
			{
				if (this._defaultSet == null)
				{
					return new PermissionSet(PermissionState.None);
				}
				return this._defaultSet;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("DefaultRequestSet");
				}
				this._defaultSet = value;
			}
		}

		/// <summary>Gets or sets the top element in the application, which is described in the deployment identity.</summary>
		/// <returns>An <see cref="T:System.ApplicationId" /> object describing the top element of the application.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <see cref="P:System.Security.Policy.ApplicationSecurityInfo.DeploymentId" /> is set to <see langword="null" />. </exception>
		// Token: 0x170009B7 RID: 2487
		// (get) Token: 0x06003BB3 RID: 15283 RVA: 0x000CE6FE File Offset: 0x000CC8FE
		// (set) Token: 0x06003BB4 RID: 15284 RVA: 0x000CE706 File Offset: 0x000CC906
		public ApplicationId DeploymentId
		{
			get
			{
				return this._deployid;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("DeploymentId");
				}
				this._deployid = value;
			}
		}

		// Token: 0x04001EC1 RID: 7873
		private Evidence _evidence;

		// Token: 0x04001EC2 RID: 7874
		private ApplicationId _appid;

		// Token: 0x04001EC3 RID: 7875
		private PermissionSet _defaultSet;

		// Token: 0x04001EC4 RID: 7876
		private ApplicationId _deployid;
	}
}
