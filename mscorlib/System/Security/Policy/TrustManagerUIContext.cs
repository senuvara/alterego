﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	/// <summary>Specifies the type of user interface (UI) the trust manager should use for trust decisions. </summary>
	// Token: 0x02000597 RID: 1431
	[ComVisible(true)]
	public enum TrustManagerUIContext
	{
		/// <summary>An Install UI.</summary>
		// Token: 0x04001F39 RID: 7993
		Install,
		/// <summary>An Upgrade UI.</summary>
		// Token: 0x04001F3A RID: 7994
		Upgrade,
		/// <summary>A Run UI.</summary>
		// Token: 0x04001F3B RID: 7995
		Run
	}
}
