﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	/// <summary>Represents the context for the trust manager to consider when making the decision to run an application, and when setting up the security on a new <see cref="T:System.AppDomain" /> in which to run an application.</summary>
	// Token: 0x02000596 RID: 1430
	[ComVisible(true)]
	public class TrustManagerContext
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.TrustManagerContext" /> class. </summary>
		// Token: 0x06003D34 RID: 15668 RVA: 0x000D327A File Offset: 0x000D147A
		public TrustManagerContext() : this(TrustManagerUIContext.Run)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.TrustManagerContext" /> class using the specified <see cref="T:System.Security.Policy.TrustManagerUIContext" /> object. </summary>
		/// <param name="uiContext">One of the <see cref="T:System.Security.Policy.TrustManagerUIContext" /> values that specifies the type of trust manager user interface to use. </param>
		// Token: 0x06003D35 RID: 15669 RVA: 0x000D3283 File Offset: 0x000D1483
		public TrustManagerContext(TrustManagerUIContext uiContext)
		{
			this._ignorePersistedDecision = false;
			this._noPrompt = false;
			this._keepAlive = false;
			this._persist = false;
			this._ui = uiContext;
		}

		/// <summary>Gets or sets a value indicating whether the application security manager should ignore any persisted decisions and call the trust manager.</summary>
		/// <returns>
		///     <see langword="true" /> to call the trust manager; otherwise, <see langword="false" />. </returns>
		// Token: 0x17000A02 RID: 2562
		// (get) Token: 0x06003D36 RID: 15670 RVA: 0x000D32AE File Offset: 0x000D14AE
		// (set) Token: 0x06003D37 RID: 15671 RVA: 0x000D32B6 File Offset: 0x000D14B6
		public virtual bool IgnorePersistedDecision
		{
			get
			{
				return this._ignorePersistedDecision;
			}
			set
			{
				this._ignorePersistedDecision = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the trust manager should cache state for this application, to facilitate future requests to determine application trust.</summary>
		/// <returns>
		///     <see langword="true" /> to cache state data; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A03 RID: 2563
		// (get) Token: 0x06003D38 RID: 15672 RVA: 0x000D32BF File Offset: 0x000D14BF
		// (set) Token: 0x06003D39 RID: 15673 RVA: 0x000D32C7 File Offset: 0x000D14C7
		public virtual bool KeepAlive
		{
			get
			{
				return this._keepAlive;
			}
			set
			{
				this._keepAlive = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the trust manager should prompt the user for trust decisions.</summary>
		/// <returns>
		///     <see langword="true" /> to not prompt the user; <see langword="false" /> to prompt the user. The default is <see langword="false" />.</returns>
		// Token: 0x17000A04 RID: 2564
		// (get) Token: 0x06003D3A RID: 15674 RVA: 0x000D32D0 File Offset: 0x000D14D0
		// (set) Token: 0x06003D3B RID: 15675 RVA: 0x000D32D8 File Offset: 0x000D14D8
		public virtual bool NoPrompt
		{
			get
			{
				return this._noPrompt;
			}
			set
			{
				this._noPrompt = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the user's response to the consent dialog should be persisted. </summary>
		/// <returns>
		///     <see langword="true" /> to cache state data; otherwise, <see langword="false" />. The default is <see langword="true" />.</returns>
		// Token: 0x17000A05 RID: 2565
		// (get) Token: 0x06003D3C RID: 15676 RVA: 0x000D32E1 File Offset: 0x000D14E1
		// (set) Token: 0x06003D3D RID: 15677 RVA: 0x000D32E9 File Offset: 0x000D14E9
		public virtual bool Persist
		{
			get
			{
				return this._persist;
			}
			set
			{
				this._persist = value;
			}
		}

		/// <summary>Gets or sets the identity of the previous application identity.</summary>
		/// <returns>An <see cref="T:System.ApplicationIdentity" /> object representing the previous <see cref="T:System.ApplicationIdentity" />.</returns>
		// Token: 0x17000A06 RID: 2566
		// (get) Token: 0x06003D3E RID: 15678 RVA: 0x000D32F2 File Offset: 0x000D14F2
		// (set) Token: 0x06003D3F RID: 15679 RVA: 0x000D32FA File Offset: 0x000D14FA
		public virtual ApplicationIdentity PreviousApplicationIdentity
		{
			get
			{
				return this._previousId;
			}
			set
			{
				this._previousId = value;
			}
		}

		/// <summary>Gets or sets the type of user interface the trust manager should display.</summary>
		/// <returns>One of the <see cref="T:System.Security.Policy.TrustManagerUIContext" /> values. The default is <see cref="F:System.Security.Policy.TrustManagerUIContext.Run" />. </returns>
		// Token: 0x17000A07 RID: 2567
		// (get) Token: 0x06003D40 RID: 15680 RVA: 0x000D3303 File Offset: 0x000D1503
		// (set) Token: 0x06003D41 RID: 15681 RVA: 0x000D330B File Offset: 0x000D150B
		public virtual TrustManagerUIContext UIContext
		{
			get
			{
				return this._ui;
			}
			set
			{
				this._ui = value;
			}
		}

		// Token: 0x04001F32 RID: 7986
		private bool _ignorePersistedDecision;

		// Token: 0x04001F33 RID: 7987
		private bool _noPrompt;

		// Token: 0x04001F34 RID: 7988
		private bool _keepAlive;

		// Token: 0x04001F35 RID: 7989
		private bool _persist;

		// Token: 0x04001F36 RID: 7990
		private ApplicationIdentity _previousId;

		// Token: 0x04001F37 RID: 7991
		private TrustManagerUIContext _ui;
	}
}
