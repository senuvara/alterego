﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x020005AC RID: 1452
	internal interface IBuiltInPermission
	{
		// Token: 0x06003E13 RID: 15891
		int GetTokenIndex();
	}
}
