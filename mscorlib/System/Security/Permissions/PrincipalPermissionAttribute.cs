﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.PrincipalPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005BE RID: 1470
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class PrincipalPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.PrincipalPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003E95 RID: 16021 RVA: 0x000D6E0D File Offset: 0x000D500D
		public PrincipalPermissionAttribute(SecurityAction action) : base(action)
		{
			this.authenticated = true;
		}

		/// <summary>Gets or sets a value indicating whether the current principal has been authenticated by the underlying role-based security provider.</summary>
		/// <returns>
		///     <see langword="true" /> if the current principal has been authenticated; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A4A RID: 2634
		// (get) Token: 0x06003E96 RID: 16022 RVA: 0x000D6E1D File Offset: 0x000D501D
		// (set) Token: 0x06003E97 RID: 16023 RVA: 0x000D6E25 File Offset: 0x000D5025
		public bool Authenticated
		{
			get
			{
				return this.authenticated;
			}
			set
			{
				this.authenticated = value;
			}
		}

		/// <summary>Gets or sets the name of the identity associated with the current principal.</summary>
		/// <returns>A name to match against that provided by the underlying role-based security provider.</returns>
		// Token: 0x17000A4B RID: 2635
		// (get) Token: 0x06003E98 RID: 16024 RVA: 0x000D6E2E File Offset: 0x000D502E
		// (set) Token: 0x06003E99 RID: 16025 RVA: 0x000D6E36 File Offset: 0x000D5036
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets membership in a specified security role.</summary>
		/// <returns>The name of a role from the underlying role-based security provider.</returns>
		// Token: 0x17000A4C RID: 2636
		// (get) Token: 0x06003E9A RID: 16026 RVA: 0x000D6E3F File Offset: 0x000D503F
		// (set) Token: 0x06003E9B RID: 16027 RVA: 0x000D6E47 File Offset: 0x000D5047
		public string Role
		{
			get
			{
				return this.role;
			}
			set
			{
				this.role = value;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.PrincipalPermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.PrincipalPermission" /> that corresponds to this attribute.</returns>
		// Token: 0x06003E9C RID: 16028 RVA: 0x000D6E50 File Offset: 0x000D5050
		public override IPermission CreatePermission()
		{
			PrincipalPermission result;
			if (base.Unrestricted)
			{
				result = new PrincipalPermission(PermissionState.Unrestricted);
			}
			else
			{
				result = new PrincipalPermission(this.name, this.role, this.authenticated);
			}
			return result;
		}

		// Token: 0x04001FD1 RID: 8145
		private bool authenticated;

		// Token: 0x04001FD2 RID: 8146
		private string name;

		// Token: 0x04001FD3 RID: 8147
		private string role;
	}
}
