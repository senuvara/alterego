﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.RegistryPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005C6 RID: 1478
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class RegistryPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.RegistryPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="action" /> parameter is not a valid <see cref="T:System.Security.Permissions.SecurityAction" />. </exception>
		// Token: 0x06003EE0 RID: 16096 RVA: 0x000D4458 File Offset: 0x000D2658
		public RegistryPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets full access for the specified registry keys.</summary>
		/// <returns>A semicolon-separated list of registry key paths, for full access. </returns>
		/// <exception cref="T:System.NotSupportedException">The get accessor is called; it is only provided for C# compiler compatibility.</exception>
		// Token: 0x17000A57 RID: 2647
		// (get) Token: 0x06003EE1 RID: 16097 RVA: 0x000D4461 File Offset: 0x000D2661
		// (set) Token: 0x06003EE2 RID: 16098 RVA: 0x000D7F48 File Offset: 0x000D6148
		[Obsolete("use newer properties")]
		public string All
		{
			get
			{
				throw new NotSupportedException("All");
			}
			set
			{
				this.create = value;
				this.read = value;
				this.write = value;
			}
		}

		/// <summary>Gets or sets create-level access for the specified registry keys. </summary>
		/// <returns>A semicolon-separated list of registry key paths, for create-level access. </returns>
		// Token: 0x17000A58 RID: 2648
		// (get) Token: 0x06003EE3 RID: 16099 RVA: 0x000D7F5F File Offset: 0x000D615F
		// (set) Token: 0x06003EE4 RID: 16100 RVA: 0x000D7F67 File Offset: 0x000D6167
		public string Create
		{
			get
			{
				return this.create;
			}
			set
			{
				this.create = value;
			}
		}

		/// <summary>Gets or sets read access for the specified registry keys.</summary>
		/// <returns>A semicolon-separated list of registry key paths, for read access. </returns>
		// Token: 0x17000A59 RID: 2649
		// (get) Token: 0x06003EE5 RID: 16101 RVA: 0x000D7F70 File Offset: 0x000D6170
		// (set) Token: 0x06003EE6 RID: 16102 RVA: 0x000D7F78 File Offset: 0x000D6178
		public string Read
		{
			get
			{
				return this.read;
			}
			set
			{
				this.read = value;
			}
		}

		/// <summary>Gets or sets write access for the specified registry keys.</summary>
		/// <returns>A semicolon-separated list of registry key paths, for write access. </returns>
		// Token: 0x17000A5A RID: 2650
		// (get) Token: 0x06003EE7 RID: 16103 RVA: 0x000D7F81 File Offset: 0x000D6181
		// (set) Token: 0x06003EE8 RID: 16104 RVA: 0x000D7F89 File Offset: 0x000D6189
		public string Write
		{
			get
			{
				return this.write;
			}
			set
			{
				this.write = value;
			}
		}

		/// <summary>Gets or sets change access control for the specified registry keys.</summary>
		/// <returns>A semicolon-separated list of registry key paths, for change access control. .</returns>
		// Token: 0x17000A5B RID: 2651
		// (get) Token: 0x06003EE9 RID: 16105 RVA: 0x000D7F92 File Offset: 0x000D6192
		// (set) Token: 0x06003EEA RID: 16106 RVA: 0x000D7F9A File Offset: 0x000D619A
		public string ChangeAccessControl
		{
			get
			{
				return this.changeAccessControl;
			}
			set
			{
				this.changeAccessControl = value;
			}
		}

		/// <summary>Gets or sets view access control for the specified registry keys.</summary>
		/// <returns>A semicolon-separated list of registry key paths, for view access control.</returns>
		// Token: 0x17000A5C RID: 2652
		// (get) Token: 0x06003EEB RID: 16107 RVA: 0x000D7FA3 File Offset: 0x000D61A3
		// (set) Token: 0x06003EEC RID: 16108 RVA: 0x000D7FAB File Offset: 0x000D61AB
		public string ViewAccessControl
		{
			get
			{
				return this.viewAccessControl;
			}
			set
			{
				this.viewAccessControl = value;
			}
		}

		/// <summary>Gets or sets a specified set of registry keys that can be viewed and modified.</summary>
		/// <returns>A semicolon-separated list of registry key paths, for create, read, and write access. </returns>
		/// <exception cref="T:System.NotSupportedException">The get accessor is called; it is only provided for C# compiler compatibility. </exception>
		// Token: 0x17000A5D RID: 2653
		// (get) Token: 0x06003EED RID: 16109 RVA: 0x000175EA File Offset: 0x000157EA
		// (set) Token: 0x06003EEE RID: 16110 RVA: 0x000D7F48 File Offset: 0x000D6148
		public string ViewAndModify
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				this.create = value;
				this.read = value;
				this.write = value;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.RegistryPermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.RegistryPermission" /> that corresponds to this attribute.</returns>
		// Token: 0x06003EEF RID: 16111 RVA: 0x000D7FB4 File Offset: 0x000D61B4
		public override IPermission CreatePermission()
		{
			RegistryPermission registryPermission;
			if (base.Unrestricted)
			{
				registryPermission = new RegistryPermission(PermissionState.Unrestricted);
			}
			else
			{
				registryPermission = new RegistryPermission(PermissionState.None);
				if (this.create != null)
				{
					registryPermission.AddPathList(RegistryPermissionAccess.Create, this.create);
				}
				if (this.read != null)
				{
					registryPermission.AddPathList(RegistryPermissionAccess.Read, this.read);
				}
				if (this.write != null)
				{
					registryPermission.AddPathList(RegistryPermissionAccess.Write, this.write);
				}
			}
			return registryPermission;
		}

		// Token: 0x04001FF1 RID: 8177
		private string create;

		// Token: 0x04001FF2 RID: 8178
		private string read;

		// Token: 0x04001FF3 RID: 8179
		private string write;

		// Token: 0x04001FF4 RID: 8180
		private string changeAccessControl;

		// Token: 0x04001FF5 RID: 8181
		private string viewAccessControl;
	}
}
