﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.ReflectionPermission" /> to be applied to code using declarative security. </summary>
	// Token: 0x020005C2 RID: 1474
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class ReflectionPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.ReflectionPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003EBE RID: 16062 RVA: 0x000D4458 File Offset: 0x000D2658
		public ReflectionPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets the current allowed uses of reflection.</summary>
		/// <returns>One or more of the <see cref="T:System.Security.Permissions.ReflectionPermissionFlag" /> values combined using a bitwise OR.</returns>
		/// <exception cref="T:System.ArgumentException">An attempt is made to set this property to an invalid value. See <see cref="T:System.Security.Permissions.ReflectionPermissionFlag" /> for the valid values. </exception>
		// Token: 0x17000A52 RID: 2642
		// (get) Token: 0x06003EBF RID: 16063 RVA: 0x000D7498 File Offset: 0x000D5698
		// (set) Token: 0x06003EC0 RID: 16064 RVA: 0x000D74A0 File Offset: 0x000D56A0
		public ReflectionPermissionFlag Flags
		{
			get
			{
				return this.flags;
			}
			set
			{
				this.flags = value;
				this.memberAccess = ((this.flags & ReflectionPermissionFlag.MemberAccess) == ReflectionPermissionFlag.MemberAccess);
				this.reflectionEmit = ((this.flags & ReflectionPermissionFlag.ReflectionEmit) == ReflectionPermissionFlag.ReflectionEmit);
				this.typeInfo = ((this.flags & ReflectionPermissionFlag.TypeInformation) == ReflectionPermissionFlag.TypeInformation);
			}
		}

		/// <summary>Gets or sets a value that indicates whether invocation of operations on non-public members is allowed.</summary>
		/// <returns>
		///     <see langword="true" /> if invocation of operations on non-public members is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A53 RID: 2643
		// (get) Token: 0x06003EC1 RID: 16065 RVA: 0x000D74DC File Offset: 0x000D56DC
		// (set) Token: 0x06003EC2 RID: 16066 RVA: 0x000D74E4 File Offset: 0x000D56E4
		public bool MemberAccess
		{
			get
			{
				return this.memberAccess;
			}
			set
			{
				if (value)
				{
					this.flags |= ReflectionPermissionFlag.MemberAccess;
				}
				else
				{
					this.flags -= 2;
				}
				this.memberAccess = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether use of certain features in <see cref="N:System.Reflection.Emit" />, such as emitting debug symbols, is allowed.</summary>
		/// <returns>
		///     <see langword="true" /> if use of the affected features is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A54 RID: 2644
		// (get) Token: 0x06003EC3 RID: 16067 RVA: 0x000D750E File Offset: 0x000D570E
		// (set) Token: 0x06003EC4 RID: 16068 RVA: 0x000D7516 File Offset: 0x000D5716
		[Obsolete]
		public bool ReflectionEmit
		{
			get
			{
				return this.reflectionEmit;
			}
			set
			{
				if (value)
				{
					this.flags |= ReflectionPermissionFlag.ReflectionEmit;
				}
				else
				{
					this.flags -= 4;
				}
				this.reflectionEmit = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether restricted invocation of non-public members is allowed. Restricted invocation means that the grant set of the assembly that contains the non-public member that is being invoked must be equal to, or a subset of, the grant set of the invoking assembly. </summary>
		/// <returns>
		///     <see langword="true" /> if restricted invocation of non-public members is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A55 RID: 2645
		// (get) Token: 0x06003EC5 RID: 16069 RVA: 0x000D7540 File Offset: 0x000D5740
		// (set) Token: 0x06003EC6 RID: 16070 RVA: 0x000D754D File Offset: 0x000D574D
		public bool RestrictedMemberAccess
		{
			get
			{
				return (this.flags & ReflectionPermissionFlag.RestrictedMemberAccess) == ReflectionPermissionFlag.RestrictedMemberAccess;
			}
			set
			{
				if (value)
				{
					this.flags |= ReflectionPermissionFlag.RestrictedMemberAccess;
					return;
				}
				this.flags -= 8;
			}
		}

		/// <summary>Gets or sets a value that indicates whether reflection on members that are not visible is allowed.</summary>
		/// <returns>
		///     <see langword="true" /> if reflection on members that are not visible is allowed; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A56 RID: 2646
		// (get) Token: 0x06003EC7 RID: 16071 RVA: 0x000D756F File Offset: 0x000D576F
		// (set) Token: 0x06003EC8 RID: 16072 RVA: 0x000D7577 File Offset: 0x000D5777
		[Obsolete("not enforced in 2.0+")]
		public bool TypeInformation
		{
			get
			{
				return this.typeInfo;
			}
			set
			{
				if (value)
				{
					this.flags |= ReflectionPermissionFlag.TypeInformation;
				}
				else
				{
					this.flags--;
				}
				this.typeInfo = value;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.ReflectionPermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.ReflectionPermission" /> that corresponds to this attribute.</returns>
		// Token: 0x06003EC9 RID: 16073 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public override IPermission CreatePermission()
		{
			return null;
		}

		// Token: 0x04001FDB RID: 8155
		private ReflectionPermissionFlag flags;

		// Token: 0x04001FDC RID: 8156
		private bool memberAccess;

		// Token: 0x04001FDD RID: 8157
		private bool reflectionEmit;

		// Token: 0x04001FDE RID: 8158
		private bool typeInfo;
	}
}
