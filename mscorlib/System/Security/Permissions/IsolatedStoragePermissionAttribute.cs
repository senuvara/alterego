﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.IsolatedStoragePermission" /> to be applied to code using declarative security.</summary>
	// Token: 0x020005B3 RID: 1459
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public abstract class IsolatedStoragePermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.IsolatedStoragePermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003E28 RID: 15912 RVA: 0x000D4458 File Offset: 0x000D2658
		protected IsolatedStoragePermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets the level of isolated storage that should be declared.</summary>
		/// <returns>One of the <see cref="T:System.Security.Permissions.IsolatedStorageContainment" /> values.</returns>
		// Token: 0x17000A2C RID: 2604
		// (get) Token: 0x06003E29 RID: 15913 RVA: 0x000D5C74 File Offset: 0x000D3E74
		// (set) Token: 0x06003E2A RID: 15914 RVA: 0x000D5C7C File Offset: 0x000D3E7C
		public IsolatedStorageContainment UsageAllowed
		{
			get
			{
				return this.usage_allowed;
			}
			set
			{
				this.usage_allowed = value;
			}
		}

		/// <summary>Gets or sets the maximum user storage quota size.</summary>
		/// <returns>The maximum user storage quota size in bytes.</returns>
		// Token: 0x17000A2D RID: 2605
		// (get) Token: 0x06003E2B RID: 15915 RVA: 0x000D5C85 File Offset: 0x000D3E85
		// (set) Token: 0x06003E2C RID: 15916 RVA: 0x000D5C8D File Offset: 0x000D3E8D
		public long UserQuota
		{
			get
			{
				return this.user_quota;
			}
			set
			{
				this.user_quota = value;
			}
		}

		// Token: 0x04001FA5 RID: 8101
		private IsolatedStorageContainment usage_allowed;

		// Token: 0x04001FA6 RID: 8102
		private long user_quota;
	}
}
