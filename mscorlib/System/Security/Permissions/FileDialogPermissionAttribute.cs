﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.FileDialogPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005A3 RID: 1443
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class FileDialogPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.FileDialogPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003DA2 RID: 15778 RVA: 0x000D4458 File Offset: 0x000D2658
		public FileDialogPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets a value indicating whether permission to open files through the file dialog is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to open files through the file dialog is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A11 RID: 2577
		// (get) Token: 0x06003DA3 RID: 15779 RVA: 0x000D46C4 File Offset: 0x000D28C4
		// (set) Token: 0x06003DA4 RID: 15780 RVA: 0x000D46CC File Offset: 0x000D28CC
		public bool Open
		{
			get
			{
				return this.canOpen;
			}
			set
			{
				this.canOpen = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to save files through the file dialog is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to save files through the file dialog is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A12 RID: 2578
		// (get) Token: 0x06003DA5 RID: 15781 RVA: 0x000D46D5 File Offset: 0x000D28D5
		// (set) Token: 0x06003DA6 RID: 15782 RVA: 0x000D46DD File Offset: 0x000D28DD
		public bool Save
		{
			get
			{
				return this.canSave;
			}
			set
			{
				this.canSave = value;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.FileDialogPermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.FileDialogPermission" /> that corresponds to this attribute.</returns>
		// Token: 0x06003DA7 RID: 15783 RVA: 0x000D46E8 File Offset: 0x000D28E8
		public override IPermission CreatePermission()
		{
			FileDialogPermission result;
			if (base.Unrestricted)
			{
				result = new FileDialogPermission(PermissionState.Unrestricted);
			}
			else
			{
				FileDialogPermissionAccess fileDialogPermissionAccess = FileDialogPermissionAccess.None;
				if (this.canOpen)
				{
					fileDialogPermissionAccess |= FileDialogPermissionAccess.Open;
				}
				if (this.canSave)
				{
					fileDialogPermissionAccess |= FileDialogPermissionAccess.Save;
				}
				result = new FileDialogPermission(fileDialogPermissionAccess);
			}
			return result;
		}

		// Token: 0x04001F55 RID: 8021
		private bool canOpen;

		// Token: 0x04001F56 RID: 8022
		private bool canSave;
	}
}
