﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.SecurityPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005CA RID: 1482
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Obsolete("CAS support is not available with Silverlight applications.")]
	[ComVisible(true)]
	[Serializable]
	public sealed class SecurityPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.SecurityPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003F04 RID: 16132 RVA: 0x000D82BB File Offset: 0x000D64BB
		public SecurityPermissionAttribute(SecurityAction action) : base(action)
		{
			this.m_Flags = SecurityPermissionFlag.NoFlags;
		}

		/// <summary>Gets or sets a value indicating whether permission to assert that all this code's callers have the requisite permission for the operation is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to assert is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A61 RID: 2657
		// (get) Token: 0x06003F05 RID: 16133 RVA: 0x000D82CB File Offset: 0x000D64CB
		// (set) Token: 0x06003F06 RID: 16134 RVA: 0x000D82D8 File Offset: 0x000D64D8
		public bool Assertion
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.Assertion) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.Assertion;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.Assertion;
			}
		}

		/// <summary>Gets or sets a value that indicates whether code has permission to perform binding redirection in the application configuration file.</summary>
		/// <returns>
		///     <see langword="true" /> if code can perform binding redirects; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A62 RID: 2658
		// (get) Token: 0x06003F07 RID: 16135 RVA: 0x000D82FB File Offset: 0x000D64FB
		// (set) Token: 0x06003F08 RID: 16136 RVA: 0x000D830C File Offset: 0x000D650C
		public bool BindingRedirects
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.BindingRedirects) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.BindingRedirects;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.BindingRedirects;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to manipulate <see cref="T:System.AppDomain" /> is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to manipulate <see cref="T:System.AppDomain" /> is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A63 RID: 2659
		// (get) Token: 0x06003F09 RID: 16137 RVA: 0x000D8336 File Offset: 0x000D6536
		// (set) Token: 0x06003F0A RID: 16138 RVA: 0x000D8347 File Offset: 0x000D6547
		public bool ControlAppDomain
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlAppDomain) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlAppDomain;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.ControlAppDomain;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to alter or manipulate domain security policy is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to alter or manipulate security policy in an application domain is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A64 RID: 2660
		// (get) Token: 0x06003F0B RID: 16139 RVA: 0x000D8371 File Offset: 0x000D6571
		// (set) Token: 0x06003F0C RID: 16140 RVA: 0x000D8382 File Offset: 0x000D6582
		public bool ControlDomainPolicy
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlDomainPolicy) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlDomainPolicy;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.ControlDomainPolicy;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to alter or manipulate evidence is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if the ability to alter or manipulate evidence is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A65 RID: 2661
		// (get) Token: 0x06003F0D RID: 16141 RVA: 0x000D83AC File Offset: 0x000D65AC
		// (set) Token: 0x06003F0E RID: 16142 RVA: 0x000D83BA File Offset: 0x000D65BA
		public bool ControlEvidence
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlEvidence) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlEvidence;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.ControlEvidence;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to view and manipulate security policy is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to manipulate security policy is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A66 RID: 2662
		// (get) Token: 0x06003F0F RID: 16143 RVA: 0x000D83DE File Offset: 0x000D65DE
		// (set) Token: 0x06003F10 RID: 16144 RVA: 0x000D83EC File Offset: 0x000D65EC
		public bool ControlPolicy
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlPolicy) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlPolicy;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.ControlPolicy;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to manipulate the current principal is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to manipulate the current principal is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A67 RID: 2663
		// (get) Token: 0x06003F11 RID: 16145 RVA: 0x000D8410 File Offset: 0x000D6610
		// (set) Token: 0x06003F12 RID: 16146 RVA: 0x000D8421 File Offset: 0x000D6621
		public bool ControlPrincipal
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlPrincipal) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlPrincipal;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.ControlPrincipal;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to manipulate threads is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to manipulate threads is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A68 RID: 2664
		// (get) Token: 0x06003F13 RID: 16147 RVA: 0x000D844B File Offset: 0x000D664B
		// (set) Token: 0x06003F14 RID: 16148 RVA: 0x000D8459 File Offset: 0x000D6659
		public bool ControlThread
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlThread) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlThread;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.ControlThread;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to execute code is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to execute code is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A69 RID: 2665
		// (get) Token: 0x06003F15 RID: 16149 RVA: 0x000D847D File Offset: 0x000D667D
		// (set) Token: 0x06003F16 RID: 16150 RVA: 0x000D848A File Offset: 0x000D668A
		public bool Execution
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.Execution) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.Execution;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.Execution;
			}
		}

		/// <summary>Gets or sets a value indicating whether code can plug into the common language runtime infrastructure, such as adding Remoting Context Sinks, Envoy Sinks and Dynamic Sinks.</summary>
		/// <returns>
		///     <see langword="true" /> if code can plug into the common language runtime infrastructure; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A6A RID: 2666
		// (get) Token: 0x06003F17 RID: 16151 RVA: 0x000D84AD File Offset: 0x000D66AD
		// (set) Token: 0x06003F18 RID: 16152 RVA: 0x000D84BE File Offset: 0x000D66BE
		[ComVisible(true)]
		public bool Infrastructure
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.Infrastructure) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.Infrastructure;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.Infrastructure;
			}
		}

		/// <summary>Gets or sets a value indicating whether code can configure remoting types and channels.</summary>
		/// <returns>
		///     <see langword="true" /> if code can configure remoting types and channels; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A6B RID: 2667
		// (get) Token: 0x06003F19 RID: 16153 RVA: 0x000D84E8 File Offset: 0x000D66E8
		// (set) Token: 0x06003F1A RID: 16154 RVA: 0x000D84F9 File Offset: 0x000D66F9
		public bool RemotingConfiguration
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.RemotingConfiguration) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.RemotingConfiguration;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.RemotingConfiguration;
			}
		}

		/// <summary>Gets or sets a value indicating whether code can use a serialization formatter to serialize or deserialize an object.</summary>
		/// <returns>
		///     <see langword="true" /> if code can use a serialization formatter to serialize or deserialize an object; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A6C RID: 2668
		// (get) Token: 0x06003F1B RID: 16155 RVA: 0x000D8523 File Offset: 0x000D6723
		// (set) Token: 0x06003F1C RID: 16156 RVA: 0x000D8534 File Offset: 0x000D6734
		public bool SerializationFormatter
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.SerializationFormatter) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.SerializationFormatter;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.SerializationFormatter;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to bypass code verification is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to bypass code verification is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A6D RID: 2669
		// (get) Token: 0x06003F1D RID: 16157 RVA: 0x000D855E File Offset: 0x000D675E
		// (set) Token: 0x06003F1E RID: 16158 RVA: 0x000D856B File Offset: 0x000D676B
		public bool SkipVerification
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.SkipVerification) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.SkipVerification;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.SkipVerification;
			}
		}

		/// <summary>Gets or sets a value indicating whether permission to call unmanaged code is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if permission to call unmanaged code is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A6E RID: 2670
		// (get) Token: 0x06003F1F RID: 16159 RVA: 0x000D858E File Offset: 0x000D678E
		// (set) Token: 0x06003F20 RID: 16160 RVA: 0x000D859B File Offset: 0x000D679B
		public bool UnmanagedCode
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.UnmanagedCode) > SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.UnmanagedCode;
					return;
				}
				this.m_Flags &= ~SecurityPermissionFlag.UnmanagedCode;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.SecurityPermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.SecurityPermission" /> that corresponds to this attribute.</returns>
		// Token: 0x06003F21 RID: 16161 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public override IPermission CreatePermission()
		{
			return null;
		}

		/// <summary>Gets or sets all permission flags comprising the <see cref="T:System.Security.Permissions.SecurityPermission" /> permissions.</summary>
		/// <returns>One or more of the <see cref="T:System.Security.Permissions.SecurityPermissionFlag" /> values combined using a bitwise OR.</returns>
		/// <exception cref="T:System.ArgumentException">An attempt is made to set this property to an invalid value. See <see cref="T:System.Security.Permissions.SecurityPermissionFlag" /> for the valid values. </exception>
		// Token: 0x17000A6F RID: 2671
		// (get) Token: 0x06003F22 RID: 16162 RVA: 0x000D85BE File Offset: 0x000D67BE
		// (set) Token: 0x06003F23 RID: 16163 RVA: 0x000D85C6 File Offset: 0x000D67C6
		public SecurityPermissionFlag Flags
		{
			get
			{
				return this.m_Flags;
			}
			set
			{
				this.m_Flags = value;
			}
		}

		// Token: 0x04002004 RID: 8196
		private SecurityPermissionFlag m_Flags;
	}
}
