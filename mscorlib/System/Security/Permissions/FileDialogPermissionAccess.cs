﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Specifies the type of access to files allowed through the File dialog boxes.</summary>
	// Token: 0x020005A2 RID: 1442
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum FileDialogPermissionAccess
	{
		/// <summary>No access to files through the File dialog boxes.</summary>
		// Token: 0x04001F51 RID: 8017
		None = 0,
		/// <summary>Ability to open files through the File dialog boxes.</summary>
		// Token: 0x04001F52 RID: 8018
		Open = 1,
		/// <summary>Ability to save files through the File dialog boxes.</summary>
		// Token: 0x04001F53 RID: 8019
		Save = 2,
		/// <summary>Ability to open and save files through the File dialog boxes.</summary>
		// Token: 0x04001F54 RID: 8020
		OpenSave = 3
	}
}
