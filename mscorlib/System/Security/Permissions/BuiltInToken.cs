﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x020005AD RID: 1453
	internal enum BuiltInToken
	{
		// Token: 0x04001F81 RID: 8065
		Environment,
		// Token: 0x04001F82 RID: 8066
		FileDialog,
		// Token: 0x04001F83 RID: 8067
		FileIO,
		// Token: 0x04001F84 RID: 8068
		IsolatedStorageFile,
		// Token: 0x04001F85 RID: 8069
		Reflection,
		// Token: 0x04001F86 RID: 8070
		Registry,
		// Token: 0x04001F87 RID: 8071
		Security,
		// Token: 0x04001F88 RID: 8072
		UI,
		// Token: 0x04001F89 RID: 8073
		Principal,
		// Token: 0x04001F8A RID: 8074
		HostProtection,
		// Token: 0x04001F8B RID: 8075
		PublisherIdentity,
		// Token: 0x04001F8C RID: 8076
		SiteIdentity,
		// Token: 0x04001F8D RID: 8077
		StrongNameIdentity,
		// Token: 0x04001F8E RID: 8078
		UrlIdentity,
		// Token: 0x04001F8F RID: 8079
		ZoneIdentity,
		// Token: 0x04001F90 RID: 8080
		GacIdentity,
		// Token: 0x04001F91 RID: 8081
		KeyContainer
	}
}
