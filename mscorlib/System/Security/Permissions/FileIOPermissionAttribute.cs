﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.FileIOPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005A6 RID: 1446
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class FileIOPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.FileIOPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="action" /> parameter is not a valid <see cref="T:System.Security.Permissions.SecurityAction" />. </exception>
		// Token: 0x06003DCD RID: 15821 RVA: 0x000D4458 File Offset: 0x000D2658
		public FileIOPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets full access for the file or directory that is specified by the string value.</summary>
		/// <returns>The absolute path of the file or directory for full access.</returns>
		/// <exception cref="T:System.NotSupportedException">The get method is not supported for this property.</exception>
		// Token: 0x17000A15 RID: 2581
		// (get) Token: 0x06003DCE RID: 15822 RVA: 0x000D4461 File Offset: 0x000D2661
		// (set) Token: 0x06003DCF RID: 15823 RVA: 0x000D5258 File Offset: 0x000D3458
		[Obsolete("use newer properties")]
		public string All
		{
			get
			{
				throw new NotSupportedException("All");
			}
			set
			{
				this.append = value;
				this.path = value;
				this.read = value;
				this.write = value;
			}
		}

		/// <summary>Gets or sets append access for the file or directory that is specified by the string value.</summary>
		/// <returns>The absolute path of the file or directory for append access.</returns>
		// Token: 0x17000A16 RID: 2582
		// (get) Token: 0x06003DD0 RID: 15824 RVA: 0x000D5276 File Offset: 0x000D3476
		// (set) Token: 0x06003DD1 RID: 15825 RVA: 0x000D527E File Offset: 0x000D347E
		public string Append
		{
			get
			{
				return this.append;
			}
			set
			{
				this.append = value;
			}
		}

		/// <summary>Gets or sets the file or directory to which to grant path discovery.</summary>
		/// <returns>The absolute path of the file or directory.</returns>
		// Token: 0x17000A17 RID: 2583
		// (get) Token: 0x06003DD2 RID: 15826 RVA: 0x000D5287 File Offset: 0x000D3487
		// (set) Token: 0x06003DD3 RID: 15827 RVA: 0x000D528F File Offset: 0x000D348F
		public string PathDiscovery
		{
			get
			{
				return this.path;
			}
			set
			{
				this.path = value;
			}
		}

		/// <summary>Gets or sets read access for the file or directory specified by the string value.</summary>
		/// <returns>The absolute path of the file or directory for read access.</returns>
		// Token: 0x17000A18 RID: 2584
		// (get) Token: 0x06003DD4 RID: 15828 RVA: 0x000D5298 File Offset: 0x000D3498
		// (set) Token: 0x06003DD5 RID: 15829 RVA: 0x000D52A0 File Offset: 0x000D34A0
		public string Read
		{
			get
			{
				return this.read;
			}
			set
			{
				this.read = value;
			}
		}

		/// <summary>Gets or sets write access for the file or directory specified by the string value.</summary>
		/// <returns>The absolute path of the file or directory for write access.</returns>
		// Token: 0x17000A19 RID: 2585
		// (get) Token: 0x06003DD6 RID: 15830 RVA: 0x000D52A9 File Offset: 0x000D34A9
		// (set) Token: 0x06003DD7 RID: 15831 RVA: 0x000D52B1 File Offset: 0x000D34B1
		public string Write
		{
			get
			{
				return this.write;
			}
			set
			{
				this.write = value;
			}
		}

		/// <summary>Gets or sets the permitted access to all files.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Security.Permissions.FileIOPermissionAccess" /> values that represents the permissions for all files. The default is <see cref="F:System.Security.Permissions.FileIOPermissionAccess.NoAccess" />.</returns>
		// Token: 0x17000A1A RID: 2586
		// (get) Token: 0x06003DD8 RID: 15832 RVA: 0x000D52BA File Offset: 0x000D34BA
		// (set) Token: 0x06003DD9 RID: 15833 RVA: 0x000D52C2 File Offset: 0x000D34C2
		public FileIOPermissionAccess AllFiles
		{
			get
			{
				return this.allFiles;
			}
			set
			{
				this.allFiles = value;
			}
		}

		/// <summary>Gets or sets the permitted access to all local files.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Security.Permissions.FileIOPermissionAccess" /> values that represents the permissions for all local files. The default is <see cref="F:System.Security.Permissions.FileIOPermissionAccess.NoAccess" />.</returns>
		// Token: 0x17000A1B RID: 2587
		// (get) Token: 0x06003DDA RID: 15834 RVA: 0x000D52CB File Offset: 0x000D34CB
		// (set) Token: 0x06003DDB RID: 15835 RVA: 0x000D52D3 File Offset: 0x000D34D3
		public FileIOPermissionAccess AllLocalFiles
		{
			get
			{
				return this.allLocalFiles;
			}
			set
			{
				this.allLocalFiles = value;
			}
		}

		/// <summary>Gets or sets the file or directory in which access control information can be changed.</summary>
		/// <returns>The absolute path of the file or directory in which access control information can be changed.</returns>
		// Token: 0x17000A1C RID: 2588
		// (get) Token: 0x06003DDC RID: 15836 RVA: 0x000D52DC File Offset: 0x000D34DC
		// (set) Token: 0x06003DDD RID: 15837 RVA: 0x000D52E4 File Offset: 0x000D34E4
		public string ChangeAccessControl
		{
			get
			{
				return this.changeAccessControl;
			}
			set
			{
				this.changeAccessControl = value;
			}
		}

		/// <summary>Gets or sets the file or directory in which access control information can be viewed.</summary>
		/// <returns>The absolute path of the file or directory in which access control information can be viewed.</returns>
		// Token: 0x17000A1D RID: 2589
		// (get) Token: 0x06003DDE RID: 15838 RVA: 0x000D52ED File Offset: 0x000D34ED
		// (set) Token: 0x06003DDF RID: 15839 RVA: 0x000D52F5 File Offset: 0x000D34F5
		public string ViewAccessControl
		{
			get
			{
				return this.viewAccessControl;
			}
			set
			{
				this.viewAccessControl = value;
			}
		}

		/// <summary>Gets or sets the file or directory in which file data can be viewed and modified.</summary>
		/// <returns>The absolute path of the file or directory in which file data can be viewed and modified.</returns>
		/// <exception cref="T:System.NotSupportedException">The <see langword="get" /> accessor is called. The accessor is provided only for C# compiler compatibility.</exception>
		// Token: 0x17000A1E RID: 2590
		// (get) Token: 0x06003DE0 RID: 15840 RVA: 0x000175EA File Offset: 0x000157EA
		// (set) Token: 0x06003DE1 RID: 15841 RVA: 0x000D5258 File Offset: 0x000D3458
		public string ViewAndModify
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				this.append = value;
				this.path = value;
				this.read = value;
				this.write = value;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.FileIOPermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.FileIOPermission" /> that corresponds to this attribute.</returns>
		/// <exception cref="T:System.ArgumentException">The path information for a file or directory for which access is to be secured contains invalid characters or wildcard specifiers. </exception>
		// Token: 0x06003DE2 RID: 15842 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public override IPermission CreatePermission()
		{
			return null;
		}

		// Token: 0x04001F68 RID: 8040
		private string append;

		// Token: 0x04001F69 RID: 8041
		private string path;

		// Token: 0x04001F6A RID: 8042
		private string read;

		// Token: 0x04001F6B RID: 8043
		private string write;

		// Token: 0x04001F6C RID: 8044
		private FileIOPermissionAccess allFiles;

		// Token: 0x04001F6D RID: 8045
		private FileIOPermissionAccess allLocalFiles;

		// Token: 0x04001F6E RID: 8046
		private string changeAccessControl;

		// Token: 0x04001F6F RID: 8047
		private string viewAccessControl;
	}
}
