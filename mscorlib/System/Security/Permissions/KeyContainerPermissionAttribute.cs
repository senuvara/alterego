﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.KeyContainerPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005B8 RID: 1464
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class KeyContainerPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.KeyContainerPermissionAttribute" /> class with the specified security action.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003E61 RID: 15969 RVA: 0x000D62BA File Offset: 0x000D44BA
		public KeyContainerPermissionAttribute(SecurityAction action) : base(action)
		{
			this._spec = -1;
			this._type = -1;
		}

		/// <summary>Gets or sets the key container permissions.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Security.Permissions.KeyContainerPermissionFlags" /> values. The default is <see cref="F:System.Security.Permissions.KeyContainerPermissionFlags.NoFlags" />.</returns>
		// Token: 0x17000A3C RID: 2620
		// (get) Token: 0x06003E62 RID: 15970 RVA: 0x000D62D1 File Offset: 0x000D44D1
		// (set) Token: 0x06003E63 RID: 15971 RVA: 0x000D62D9 File Offset: 0x000D44D9
		public KeyContainerPermissionFlags Flags
		{
			get
			{
				return this._flags;
			}
			set
			{
				this._flags = value;
			}
		}

		/// <summary>Gets or sets the name of the key container.</summary>
		/// <returns>The name of the key container.</returns>
		// Token: 0x17000A3D RID: 2621
		// (get) Token: 0x06003E64 RID: 15972 RVA: 0x000D62E2 File Offset: 0x000D44E2
		// (set) Token: 0x06003E65 RID: 15973 RVA: 0x000D62EA File Offset: 0x000D44EA
		public string KeyContainerName
		{
			get
			{
				return this._containerName;
			}
			set
			{
				this._containerName = value;
			}
		}

		/// <summary>Gets or sets the key specification.</summary>
		/// <returns>One of the AT_ values defined in the Wincrypt.h header file. </returns>
		// Token: 0x17000A3E RID: 2622
		// (get) Token: 0x06003E66 RID: 15974 RVA: 0x000D62F3 File Offset: 0x000D44F3
		// (set) Token: 0x06003E67 RID: 15975 RVA: 0x000D62FB File Offset: 0x000D44FB
		public int KeySpec
		{
			get
			{
				return this._spec;
			}
			set
			{
				this._spec = value;
			}
		}

		/// <summary>Gets or sets the name of the key store.</summary>
		/// <returns>The name of the key store. The default is "*".</returns>
		// Token: 0x17000A3F RID: 2623
		// (get) Token: 0x06003E68 RID: 15976 RVA: 0x000D6304 File Offset: 0x000D4504
		// (set) Token: 0x06003E69 RID: 15977 RVA: 0x000D630C File Offset: 0x000D450C
		public string KeyStore
		{
			get
			{
				return this._store;
			}
			set
			{
				this._store = value;
			}
		}

		/// <summary>Gets or sets the provider name.</summary>
		/// <returns>The name of the provider.</returns>
		// Token: 0x17000A40 RID: 2624
		// (get) Token: 0x06003E6A RID: 15978 RVA: 0x000D6315 File Offset: 0x000D4515
		// (set) Token: 0x06003E6B RID: 15979 RVA: 0x000D631D File Offset: 0x000D451D
		public string ProviderName
		{
			get
			{
				return this._providerName;
			}
			set
			{
				this._providerName = value;
			}
		}

		/// <summary>Gets or sets the provider type.</summary>
		/// <returns>One of the PROV_ values defined in the Wincrypt.h header file. </returns>
		// Token: 0x17000A41 RID: 2625
		// (get) Token: 0x06003E6C RID: 15980 RVA: 0x000D6326 File Offset: 0x000D4526
		// (set) Token: 0x06003E6D RID: 15981 RVA: 0x000D632E File Offset: 0x000D452E
		public int ProviderType
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.KeyContainerPermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.KeyContainerPermission" /> that corresponds to the attribute.</returns>
		// Token: 0x06003E6E RID: 15982 RVA: 0x000D6338 File Offset: 0x000D4538
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new KeyContainerPermission(PermissionState.Unrestricted);
			}
			if (this.EmptyEntry())
			{
				return new KeyContainerPermission(this._flags);
			}
			KeyContainerPermissionAccessEntry[] accessList = new KeyContainerPermissionAccessEntry[]
			{
				new KeyContainerPermissionAccessEntry(this._store, this._providerName, this._type, this._containerName, this._spec, this._flags)
			};
			return new KeyContainerPermission(this._flags, accessList);
		}

		// Token: 0x06003E6F RID: 15983 RVA: 0x000D63A7 File Offset: 0x000D45A7
		private bool EmptyEntry()
		{
			return this._containerName == null && this._spec == 0 && this._store == null && this._providerName == null && this._type == 0;
		}

		// Token: 0x04001FB2 RID: 8114
		private KeyContainerPermissionFlags _flags;

		// Token: 0x04001FB3 RID: 8115
		private string _containerName;

		// Token: 0x04001FB4 RID: 8116
		private int _spec;

		// Token: 0x04001FB5 RID: 8117
		private string _store;

		// Token: 0x04001FB6 RID: 8118
		private string _providerName;

		// Token: 0x04001FB7 RID: 8119
		private int _type;
	}
}
