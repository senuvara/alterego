﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.EnvironmentPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005A0 RID: 1440
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class EnvironmentPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.EnvironmentPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="action" /> parameter is not a valid value of <see cref="T:System.Security.Permissions.SecurityAction" />. </exception>
		// Token: 0x06003D8D RID: 15757 RVA: 0x000D4458 File Offset: 0x000D2658
		public EnvironmentPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Sets full access for the environment variables specified by the string value.</summary>
		/// <returns>A list of environment variables for full access.</returns>
		/// <exception cref="T:System.NotSupportedException">The get method is not supported for this property.</exception>
		// Token: 0x17000A0D RID: 2573
		// (get) Token: 0x06003D8E RID: 15758 RVA: 0x000D4461 File Offset: 0x000D2661
		// (set) Token: 0x06003D8F RID: 15759 RVA: 0x000D446D File Offset: 0x000D266D
		public string All
		{
			get
			{
				throw new NotSupportedException("All");
			}
			set
			{
				this.read = value;
				this.write = value;
			}
		}

		/// <summary>Gets or sets read access for the environment variables specified by the string value.</summary>
		/// <returns>A list of environment variables for read access.</returns>
		// Token: 0x17000A0E RID: 2574
		// (get) Token: 0x06003D90 RID: 15760 RVA: 0x000D447D File Offset: 0x000D267D
		// (set) Token: 0x06003D91 RID: 15761 RVA: 0x000D4485 File Offset: 0x000D2685
		public string Read
		{
			get
			{
				return this.read;
			}
			set
			{
				this.read = value;
			}
		}

		/// <summary>Gets or sets write access for the environment variables specified by the string value.</summary>
		/// <returns>A list of environment variables for write access.</returns>
		// Token: 0x17000A0F RID: 2575
		// (get) Token: 0x06003D92 RID: 15762 RVA: 0x000D448E File Offset: 0x000D268E
		// (set) Token: 0x06003D93 RID: 15763 RVA: 0x000D4496 File Offset: 0x000D2696
		public string Write
		{
			get
			{
				return this.write;
			}
			set
			{
				this.write = value;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.EnvironmentPermission" />.</summary>
		/// <returns>An <see cref="T:System.Security.Permissions.EnvironmentPermission" /> that corresponds to this attribute.</returns>
		// Token: 0x06003D94 RID: 15764 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public override IPermission CreatePermission()
		{
			return null;
		}

		// Token: 0x04001F4C RID: 8012
		private string read;

		// Token: 0x04001F4D RID: 8013
		private string write;
	}
}
