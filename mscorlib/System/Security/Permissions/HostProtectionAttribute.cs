﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows the use of declarative security actions to determine host protection requirements. This class cannot be inherited.</summary>
	// Token: 0x020005A9 RID: 1449
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Delegate, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class HostProtectionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.HostProtectionAttribute" /> class with default values.</summary>
		// Token: 0x06003DEF RID: 15855 RVA: 0x000D5376 File Offset: 0x000D3576
		public HostProtectionAttribute() : base(SecurityAction.LinkDemand)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.HostProtectionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" /> value.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="action" /> is not <see cref="F:System.Security.Permissions.SecurityAction.LinkDemand" />. </exception>
		// Token: 0x06003DF0 RID: 15856 RVA: 0x000D537F File Offset: 0x000D357F
		public HostProtectionAttribute(SecurityAction action) : base(action)
		{
			if (action != SecurityAction.LinkDemand)
			{
				throw new ArgumentException(string.Format(Locale.GetText("Only {0} is accepted."), SecurityAction.LinkDemand), "action");
			}
		}

		/// <summary>Gets or sets a value indicating whether external process management is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if external process management is exposed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A1F RID: 2591
		// (get) Token: 0x06003DF1 RID: 15857 RVA: 0x000D53AC File Offset: 0x000D35AC
		// (set) Token: 0x06003DF2 RID: 15858 RVA: 0x000D53B9 File Offset: 0x000D35B9
		public bool ExternalProcessMgmt
		{
			get
			{
				return (this._resources & HostProtectionResource.ExternalProcessMgmt) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.ExternalProcessMgmt;
					return;
				}
				this._resources &= ~HostProtectionResource.ExternalProcessMgmt;
			}
		}

		/// <summary>Gets or sets a value indicating whether external threading is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if external threading is exposed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A20 RID: 2592
		// (get) Token: 0x06003DF3 RID: 15859 RVA: 0x000D53DC File Offset: 0x000D35DC
		// (set) Token: 0x06003DF4 RID: 15860 RVA: 0x000D53EA File Offset: 0x000D35EA
		public bool ExternalThreading
		{
			get
			{
				return (this._resources & HostProtectionResource.ExternalThreading) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.ExternalThreading;
					return;
				}
				this._resources &= ~HostProtectionResource.ExternalThreading;
			}
		}

		/// <summary>Gets or sets a value indicating whether resources might leak memory if the operation is terminated.</summary>
		/// <returns>
		///     <see langword="true" /> if resources might leak memory on termination; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A21 RID: 2593
		// (get) Token: 0x06003DF5 RID: 15861 RVA: 0x000D540E File Offset: 0x000D360E
		// (set) Token: 0x06003DF6 RID: 15862 RVA: 0x000D541F File Offset: 0x000D361F
		public bool MayLeakOnAbort
		{
			get
			{
				return (this._resources & HostProtectionResource.MayLeakOnAbort) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.MayLeakOnAbort;
					return;
				}
				this._resources &= ~HostProtectionResource.MayLeakOnAbort;
			}
		}

		/// <summary>Gets or sets a value indicating whether the security infrastructure is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if the security infrastructure is exposed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A22 RID: 2594
		// (get) Token: 0x06003DF7 RID: 15863 RVA: 0x000D5449 File Offset: 0x000D3649
		// (set) Token: 0x06003DF8 RID: 15864 RVA: 0x000D5457 File Offset: 0x000D3657
		[ComVisible(true)]
		public bool SecurityInfrastructure
		{
			get
			{
				return (this._resources & HostProtectionResource.SecurityInfrastructure) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.SecurityInfrastructure;
					return;
				}
				this._resources &= ~HostProtectionResource.SecurityInfrastructure;
			}
		}

		/// <summary>Gets or sets a value indicating whether self-affecting process management is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if self-affecting process management is exposed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A23 RID: 2595
		// (get) Token: 0x06003DF9 RID: 15865 RVA: 0x000D547B File Offset: 0x000D367B
		// (set) Token: 0x06003DFA RID: 15866 RVA: 0x000D5488 File Offset: 0x000D3688
		public bool SelfAffectingProcessMgmt
		{
			get
			{
				return (this._resources & HostProtectionResource.SelfAffectingProcessMgmt) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.SelfAffectingProcessMgmt;
					return;
				}
				this._resources &= ~HostProtectionResource.SelfAffectingProcessMgmt;
			}
		}

		/// <summary>Gets or sets a value indicating whether self-affecting threading is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if self-affecting threading is exposed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A24 RID: 2596
		// (get) Token: 0x06003DFB RID: 15867 RVA: 0x000D54AB File Offset: 0x000D36AB
		// (set) Token: 0x06003DFC RID: 15868 RVA: 0x000D54B9 File Offset: 0x000D36B9
		public bool SelfAffectingThreading
		{
			get
			{
				return (this._resources & HostProtectionResource.SelfAffectingThreading) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.SelfAffectingThreading;
					return;
				}
				this._resources &= ~HostProtectionResource.SelfAffectingThreading;
			}
		}

		/// <summary>Gets or sets a value indicating whether shared state is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if shared state is exposed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A25 RID: 2597
		// (get) Token: 0x06003DFD RID: 15869 RVA: 0x000D54DD File Offset: 0x000D36DD
		// (set) Token: 0x06003DFE RID: 15870 RVA: 0x000D54EA File Offset: 0x000D36EA
		public bool SharedState
		{
			get
			{
				return (this._resources & HostProtectionResource.SharedState) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.SharedState;
					return;
				}
				this._resources &= ~HostProtectionResource.SharedState;
			}
		}

		/// <summary>Gets or sets a value indicating whether synchronization is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if synchronization is exposed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A26 RID: 2598
		// (get) Token: 0x06003DFF RID: 15871 RVA: 0x000D550D File Offset: 0x000D370D
		// (set) Token: 0x06003E00 RID: 15872 RVA: 0x000D551A File Offset: 0x000D371A
		public bool Synchronization
		{
			get
			{
				return (this._resources & HostProtectionResource.Synchronization) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.Synchronization;
					return;
				}
				this._resources &= ~HostProtectionResource.Synchronization;
			}
		}

		/// <summary>Gets or sets a value indicating whether the user interface is exposed.</summary>
		/// <returns>
		///     <see langword="true" /> if the user interface is exposed; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000A27 RID: 2599
		// (get) Token: 0x06003E01 RID: 15873 RVA: 0x000D553D File Offset: 0x000D373D
		// (set) Token: 0x06003E02 RID: 15874 RVA: 0x000D554E File Offset: 0x000D374E
		public bool UI
		{
			get
			{
				return (this._resources & HostProtectionResource.UI) > HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.UI;
					return;
				}
				this._resources &= ~HostProtectionResource.UI;
			}
		}

		/// <summary>Gets or sets flags specifying categories of functionality that are potentially harmful to the host.</summary>
		/// <returns>A bitwise combination of the <see cref="T:System.Security.Permissions.HostProtectionResource" /> values. The default is <see cref="F:System.Security.Permissions.HostProtectionResource.None" />.</returns>
		// Token: 0x17000A28 RID: 2600
		// (get) Token: 0x06003E03 RID: 15875 RVA: 0x000D5578 File Offset: 0x000D3778
		// (set) Token: 0x06003E04 RID: 15876 RVA: 0x000D5580 File Offset: 0x000D3780
		public HostProtectionResource Resources
		{
			get
			{
				return this._resources;
			}
			set
			{
				this._resources = value;
			}
		}

		/// <summary>Creates and returns a new host protection permission.</summary>
		/// <returns>An <see cref="T:System.Security.IPermission" /> that corresponds to the current attribute.</returns>
		// Token: 0x06003E05 RID: 15877 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public override IPermission CreatePermission()
		{
			return null;
		}

		// Token: 0x04001F71 RID: 8049
		private HostProtectionResource _resources;
	}
}
