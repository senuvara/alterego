﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Specifies the base attribute class for declarative security from which <see cref="T:System.Security.Permissions.CodeAccessSecurityAttribute" /> is derived.</summary>
	// Token: 0x020005C8 RID: 1480
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Obsolete("CAS support is not available with Silverlight applications.")]
	[ComVisible(true)]
	[Serializable]
	public abstract class SecurityAttribute : Attribute
	{
		/// <summary>Initializes a new instance of <see cref="T:System.Security.Permissions.SecurityAttribute" /> with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003EF0 RID: 16112 RVA: 0x000D801B File Offset: 0x000D621B
		protected SecurityAttribute(SecurityAction action)
		{
			this.Action = action;
		}

		/// <summary>When overridden in a derived class, creates a permission object that can then be serialized into binary form and persistently stored along with the <see cref="T:System.Security.Permissions.SecurityAction" /> in an assembly's metadata.</summary>
		/// <returns>A serializable permission object.</returns>
		// Token: 0x06003EF1 RID: 16113
		public abstract IPermission CreatePermission();

		/// <summary>Gets or sets a value indicating whether full (unrestricted) permission to the resource protected by the attribute is declared.</summary>
		/// <returns>
		///     <see langword="true" /> if full permission to the protected resource is declared; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A5E RID: 2654
		// (get) Token: 0x06003EF2 RID: 16114 RVA: 0x000D802A File Offset: 0x000D622A
		// (set) Token: 0x06003EF3 RID: 16115 RVA: 0x000D8032 File Offset: 0x000D6232
		public bool Unrestricted
		{
			get
			{
				return this.m_Unrestricted;
			}
			set
			{
				this.m_Unrestricted = value;
			}
		}

		/// <summary>Gets or sets a security action.</summary>
		/// <returns>One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values.</returns>
		// Token: 0x17000A5F RID: 2655
		// (get) Token: 0x06003EF4 RID: 16116 RVA: 0x000D803B File Offset: 0x000D623B
		// (set) Token: 0x06003EF5 RID: 16117 RVA: 0x000D8043 File Offset: 0x000D6243
		public SecurityAction Action
		{
			get
			{
				return this.m_Action;
			}
			set
			{
				this.m_Action = value;
			}
		}

		// Token: 0x04002000 RID: 8192
		private SecurityAction m_Action;

		// Token: 0x04002001 RID: 8193
		private bool m_Unrestricted;
	}
}
