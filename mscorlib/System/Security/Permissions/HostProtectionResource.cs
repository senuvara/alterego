﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Specifies categories of functionality potentially harmful to the host if invoked by a method or class.</summary>
	// Token: 0x020005AB RID: 1451
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum HostProtectionResource
	{
		/// <summary>Exposes no host resources.</summary>
		// Token: 0x04001F75 RID: 8053
		None = 0,
		/// <summary>Exposes synchronization.</summary>
		// Token: 0x04001F76 RID: 8054
		Synchronization = 1,
		/// <summary>Exposes state that might be shared between threads.</summary>
		// Token: 0x04001F77 RID: 8055
		SharedState = 2,
		/// <summary>Might create or destroy other processes.</summary>
		// Token: 0x04001F78 RID: 8056
		ExternalProcessMgmt = 4,
		/// <summary>Might exit the current process, terminating the server.</summary>
		// Token: 0x04001F79 RID: 8057
		SelfAffectingProcessMgmt = 8,
		/// <summary>Creates or manipulates threads other than its own, which might be harmful to the host.</summary>
		// Token: 0x04001F7A RID: 8058
		ExternalThreading = 16,
		/// <summary>Manipulates threads in a way that only affects user code.</summary>
		// Token: 0x04001F7B RID: 8059
		SelfAffectingThreading = 32,
		/// <summary>Exposes the security infrastructure.</summary>
		// Token: 0x04001F7C RID: 8060
		SecurityInfrastructure = 64,
		/// <summary>Exposes the user interface.</summary>
		// Token: 0x04001F7D RID: 8061
		UI = 128,
		/// <summary>Might cause a resource leak on termination, if not protected by a safe handle or some other means of ensuring the release of resources.</summary>
		// Token: 0x04001F7E RID: 8062
		MayLeakOnAbort = 256,
		/// <summary>Exposes all host resources.</summary>
		// Token: 0x04001F7F RID: 8063
		All = 511
	}
}
