﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Specifies the type of key container access allowed.</summary>
	// Token: 0x020005B9 RID: 1465
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum KeyContainerPermissionFlags
	{
		/// <summary>No access to a key container.</summary>
		// Token: 0x04001FB9 RID: 8121
		NoFlags = 0,
		/// <summary>Create a key container.</summary>
		// Token: 0x04001FBA RID: 8122
		Create = 1,
		/// <summary>Open a key container and use the public key.</summary>
		// Token: 0x04001FBB RID: 8123
		Open = 2,
		/// <summary>Delete a key container.</summary>
		// Token: 0x04001FBC RID: 8124
		Delete = 4,
		/// <summary>Import a key into a key container.</summary>
		// Token: 0x04001FBD RID: 8125
		Import = 16,
		/// <summary>Export a key from a key container.</summary>
		// Token: 0x04001FBE RID: 8126
		Export = 32,
		/// <summary>Sign a file using a key.</summary>
		// Token: 0x04001FBF RID: 8127
		Sign = 256,
		/// <summary>Decrypt a key container.</summary>
		// Token: 0x04001FC0 RID: 8128
		Decrypt = 512,
		/// <summary>View the access control list (ACL) for a key container.</summary>
		// Token: 0x04001FC1 RID: 8129
		ViewAcl = 4096,
		/// <summary>Change the access control list (ACL) for a key container. </summary>
		// Token: 0x04001FC2 RID: 8130
		ChangeAcl = 8192,
		/// <summary>Create, decrypt, delete, and open a key container; export and import a key; sign files using a key; and view and change the access control list for a key container.</summary>
		// Token: 0x04001FC3 RID: 8131
		AllFlags = 13111
	}
}
