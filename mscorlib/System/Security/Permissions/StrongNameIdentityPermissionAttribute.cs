﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for <see cref="T:System.Security.Permissions.StrongNameIdentityPermission" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005D0 RID: 1488
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class StrongNameIdentityPermissionAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.StrongNameIdentityPermissionAttribute" /> class with the specified <see cref="T:System.Security.Permissions.SecurityAction" />.</summary>
		/// <param name="action">One of the <see cref="T:System.Security.Permissions.SecurityAction" /> values. </param>
		// Token: 0x06003F56 RID: 16214 RVA: 0x000D4458 File Offset: 0x000D2658
		public StrongNameIdentityPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets the name of the strong name identity.</summary>
		/// <returns>A name to compare against the name specified by the security provider.</returns>
		// Token: 0x17000A75 RID: 2677
		// (get) Token: 0x06003F57 RID: 16215 RVA: 0x000D94A8 File Offset: 0x000D76A8
		// (set) Token: 0x06003F58 RID: 16216 RVA: 0x000D94B0 File Offset: 0x000D76B0
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets the public key value of the strong name identity expressed as a hexadecimal string.</summary>
		/// <returns>The public key value of the strong name identity expressed as a hexadecimal string.</returns>
		// Token: 0x17000A76 RID: 2678
		// (get) Token: 0x06003F59 RID: 16217 RVA: 0x000D94B9 File Offset: 0x000D76B9
		// (set) Token: 0x06003F5A RID: 16218 RVA: 0x000D94C1 File Offset: 0x000D76C1
		public string PublicKey
		{
			get
			{
				return this.key;
			}
			set
			{
				this.key = value;
			}
		}

		/// <summary>Gets or sets the version of the strong name identity.</summary>
		/// <returns>The version number of the strong name identity.</returns>
		// Token: 0x17000A77 RID: 2679
		// (get) Token: 0x06003F5B RID: 16219 RVA: 0x000D94CA File Offset: 0x000D76CA
		// (set) Token: 0x06003F5C RID: 16220 RVA: 0x000D94D2 File Offset: 0x000D76D2
		public string Version
		{
			get
			{
				return this.version;
			}
			set
			{
				this.version = value;
			}
		}

		/// <summary>Creates and returns a new <see cref="T:System.Security.Permissions.StrongNameIdentityPermission" />.</summary>
		/// <returns>A <see cref="T:System.Security.Permissions.StrongNameIdentityPermission" /> that corresponds to this attribute.</returns>
		/// <exception cref="T:System.ArgumentException">The method failed because the key is <see langword="null" />.</exception>
		// Token: 0x06003F5D RID: 16221 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public override IPermission CreatePermission()
		{
			return null;
		}

		// Token: 0x04002021 RID: 8225
		private string name;

		// Token: 0x04002022 RID: 8226
		private string key;

		// Token: 0x04002023 RID: 8227
		private string version;
	}
}
