﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Allows security actions for a <see cref="T:System.Security.PermissionSet" /> to be applied to code using declarative security. This class cannot be inherited.</summary>
	// Token: 0x020005BA RID: 1466
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class PermissionSetAttribute : CodeAccessSecurityAttribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.PermissionSetAttribute" /> class with the specified security action.</summary>
		/// <param name="action">One of the enumeration values that specifies a security action. </param>
		// Token: 0x06003E70 RID: 15984 RVA: 0x000D4458 File Offset: 0x000D2658
		public PermissionSetAttribute(SecurityAction action) : base(action)
		{
		}

		/// <summary>Gets or sets a file containing the XML representation of a custom permission set to be declared.</summary>
		/// <returns>The physical path to the file containing the XML representation of the permission set.</returns>
		// Token: 0x17000A42 RID: 2626
		// (get) Token: 0x06003E71 RID: 15985 RVA: 0x000D63DC File Offset: 0x000D45DC
		// (set) Token: 0x06003E72 RID: 15986 RVA: 0x000D63E4 File Offset: 0x000D45E4
		public string File
		{
			get
			{
				return this.file;
			}
			set
			{
				this.file = value;
			}
		}

		/// <summary>Gets or sets the hexadecimal representation of the XML encoded permission set.</summary>
		/// <returns>The hexadecimal representation of the XML encoded permission set.</returns>
		// Token: 0x17000A43 RID: 2627
		// (get) Token: 0x06003E73 RID: 15987 RVA: 0x000D63ED File Offset: 0x000D45ED
		// (set) Token: 0x06003E74 RID: 15988 RVA: 0x000D63F5 File Offset: 0x000D45F5
		public string Hex
		{
			get
			{
				return this.hex;
			}
			set
			{
				this.hex = value;
			}
		}

		/// <summary>Gets or sets the name of the permission set.</summary>
		/// <returns>The name of an immutable <see cref="T:System.Security.NamedPermissionSet" /> (one of several permission sets that are contained in the default policy and cannot be altered).</returns>
		// Token: 0x17000A44 RID: 2628
		// (get) Token: 0x06003E75 RID: 15989 RVA: 0x000D63FE File Offset: 0x000D45FE
		// (set) Token: 0x06003E76 RID: 15990 RVA: 0x000D6406 File Offset: 0x000D4606
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		/// <summary>Gets or sets a value indicating whether the file specified by <see cref="P:System.Security.Permissions.PermissionSetAttribute.File" /> is Unicode or ASCII encoded.</summary>
		/// <returns>
		///     <see langword="true" /> if the file is Unicode encoded; otherwise, <see langword="false" />.</returns>
		// Token: 0x17000A45 RID: 2629
		// (get) Token: 0x06003E77 RID: 15991 RVA: 0x000D640F File Offset: 0x000D460F
		// (set) Token: 0x06003E78 RID: 15992 RVA: 0x000D6417 File Offset: 0x000D4617
		public bool UnicodeEncoded
		{
			get
			{
				return this.isUnicodeEncoded;
			}
			set
			{
				this.isUnicodeEncoded = value;
			}
		}

		/// <summary>Gets or sets the XML representation of a permission set.</summary>
		/// <returns>The XML representation of a permission set.</returns>
		// Token: 0x17000A46 RID: 2630
		// (get) Token: 0x06003E79 RID: 15993 RVA: 0x000D6420 File Offset: 0x000D4620
		// (set) Token: 0x06003E7A RID: 15994 RVA: 0x000D6428 File Offset: 0x000D4628
		public string XML
		{
			get
			{
				return this.xml;
			}
			set
			{
				this.xml = value;
			}
		}

		/// <summary>This method is not used.</summary>
		/// <returns>A null reference (<see langword="nothing" /> in Visual Basic) in all cases.</returns>
		// Token: 0x06003E7B RID: 15995 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public override IPermission CreatePermission()
		{
			return null;
		}

		// Token: 0x06003E7C RID: 15996 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		private PermissionSet CreateFromXml(string xml)
		{
			return null;
		}

		/// <summary>Creates and returns a new permission set based on this permission set attribute object.</summary>
		/// <returns>A new permission set.</returns>
		// Token: 0x06003E7D RID: 15997 RVA: 0x0000CEAE File Offset: 0x0000B0AE
		public PermissionSet CreatePermissionSet()
		{
			return null;
		}

		// Token: 0x04001FC4 RID: 8132
		private string file;

		// Token: 0x04001FC5 RID: 8133
		private string name;

		// Token: 0x04001FC6 RID: 8134
		private bool isUnicodeEncoded;

		// Token: 0x04001FC7 RID: 8135
		private string xml;

		// Token: 0x04001FC8 RID: 8136
		private string hex;
	}
}
