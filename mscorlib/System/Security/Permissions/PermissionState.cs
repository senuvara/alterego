﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Specifies whether a permission should have all or no access to resources at creation.</summary>
	// Token: 0x020005BB RID: 1467
	[ComVisible(true)]
	[Serializable]
	public enum PermissionState
	{
		/// <summary>Full access to the resource protected by the permission.</summary>
		// Token: 0x04001FCA RID: 8138
		Unrestricted = 1,
		/// <summary>No access to the resource protected by the permission.</summary>
		// Token: 0x04001FCB RID: 8139
		None = 0
	}
}
