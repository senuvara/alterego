﻿using System;

namespace System.Security
{
	// Token: 0x02000561 RID: 1377
	internal enum SecurityElementType
	{
		// Token: 0x04001E96 RID: 7830
		Regular,
		// Token: 0x04001E97 RID: 7831
		Format,
		// Token: 0x04001E98 RID: 7832
		Comment
	}
}
