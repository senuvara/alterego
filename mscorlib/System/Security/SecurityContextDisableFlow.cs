﻿using System;

namespace System.Security
{
	// Token: 0x02000550 RID: 1360
	internal enum SecurityContextDisableFlow
	{
		// Token: 0x04001E6A RID: 7786
		Nothing,
		// Token: 0x04001E6B RID: 7787
		WI,
		// Token: 0x04001E6C RID: 7788
		All = 16383
	}
}
