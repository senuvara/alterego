﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	/// <summary>Specifies the type of a managed code policy level.</summary>
	// Token: 0x0200055F RID: 1375
	[ComVisible(true)]
	[Serializable]
	public enum PolicyLevelType
	{
		/// <summary>Security policy for all managed code that is run by the user.</summary>
		// Token: 0x04001E8B RID: 7819
		User,
		/// <summary>Security policy for all managed code that is run on the computer.</summary>
		// Token: 0x04001E8C RID: 7820
		Machine,
		/// <summary>Security policy for all managed code in an enterprise.</summary>
		// Token: 0x04001E8D RID: 7821
		Enterprise,
		/// <summary>Security policy for all managed code in an application.</summary>
		// Token: 0x04001E8E RID: 7822
		AppDomain
	}
}
