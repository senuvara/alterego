﻿using System;

namespace System.Security
{
	/// <summary>Identifies which of the nonpublic <see cref="T:System.Security.SecurityCriticalAttribute" /> members are accessible by transparent code within the assembly. </summary>
	// Token: 0x0200054A RID: 1354
	[Obsolete("SecurityTreatAsSafe is only used for .NET 2.0 transparency compatibility.  Please use the SecuritySafeCriticalAttribute instead.")]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Field | AttributeTargets.Interface | AttributeTargets.Delegate, AllowMultiple = false, Inherited = false)]
	public sealed class SecurityTreatAsSafeAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityTreatAsSafeAttribute" /> class. </summary>
		// Token: 0x06003A5F RID: 14943 RVA: 0x000020BF File Offset: 0x000002BF
		public SecurityTreatAsSafeAttribute()
		{
		}
	}
}
