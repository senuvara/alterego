﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Security
{
	/// <summary>The exception that is thrown when there is a syntax error in XML parsing. This class cannot be inherited.</summary>
	// Token: 0x0200056B RID: 1387
	[ComVisible(true)]
	[Serializable]
	public sealed class XmlSyntaxException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.XmlSyntaxException" /> class with default properties.</summary>
		// Token: 0x06003B88 RID: 15240 RVA: 0x000CB908 File Offset: 0x000C9B08
		public XmlSyntaxException()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.XmlSyntaxException" /> class with the line number where the exception was detected.</summary>
		/// <param name="lineNumber">The line number of the XML stream where the XML syntax error was detected. </param>
		// Token: 0x06003B89 RID: 15241 RVA: 0x000CE384 File Offset: 0x000CC584
		public XmlSyntaxException(int lineNumber) : base(string.Format(Locale.GetText("Invalid syntax on line {0}."), lineNumber))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.XmlSyntaxException" /> class with a specified error message and the line number where the exception was detected.</summary>
		/// <param name="lineNumber">The line number of the XML stream where the XML syntax error was detected. </param>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x06003B8A RID: 15242 RVA: 0x000CE3A1 File Offset: 0x000CC5A1
		public XmlSyntaxException(int lineNumber, string message) : base(string.Format(Locale.GetText("Invalid syntax on line {0} - {1}."), lineNumber, message))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.XmlSyntaxException" /> class with a specified error message.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x06003B8B RID: 15243 RVA: 0x000BA200 File Offset: 0x000B8400
		public XmlSyntaxException(string message) : base(message)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.XmlSyntaxException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. If the <paramref name="inner" /> parameter is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06003B8C RID: 15244 RVA: 0x000BA209 File Offset: 0x000B8409
		public XmlSyntaxException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06003B8D RID: 15245 RVA: 0x000319C9 File Offset: 0x0002FBC9
		internal XmlSyntaxException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
