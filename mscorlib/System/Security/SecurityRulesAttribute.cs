﻿using System;

namespace System.Security
{
	/// <summary>Indicates the set of security rules the common language runtime should enforce for an assembly.  </summary>
	// Token: 0x0200054E RID: 1358
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
	public sealed class SecurityRulesAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.SecurityRulesAttribute" /> class using the specified rule set value. </summary>
		/// <param name="ruleSet">One of the enumeration values that specifies the transparency rules set. </param>
		// Token: 0x06003A62 RID: 14946 RVA: 0x000CB0C5 File Offset: 0x000C92C5
		public SecurityRulesAttribute(SecurityRuleSet ruleSet)
		{
			this.m_ruleSet = ruleSet;
		}

		/// <summary>Determines whether fully trusted transparent code should skip Microsoft intermediate language (MSIL) verification.</summary>
		/// <returns>
		///     <see langword="true" /> if MSIL verification should be skipped; otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17000988 RID: 2440
		// (get) Token: 0x06003A63 RID: 14947 RVA: 0x000CB0D4 File Offset: 0x000C92D4
		// (set) Token: 0x06003A64 RID: 14948 RVA: 0x000CB0DC File Offset: 0x000C92DC
		public bool SkipVerificationInFullTrust
		{
			get
			{
				return this.m_skipVerificationInFullTrust;
			}
			set
			{
				this.m_skipVerificationInFullTrust = value;
			}
		}

		/// <summary>Gets the rule set to be applied.</summary>
		/// <returns>One of the enumeration values that specifies the transparency rules to be applied.</returns>
		// Token: 0x17000989 RID: 2441
		// (get) Token: 0x06003A65 RID: 14949 RVA: 0x000CB0E5 File Offset: 0x000C92E5
		public SecurityRuleSet RuleSet
		{
			get
			{
				return this.m_ruleSet;
			}
		}

		// Token: 0x04001E64 RID: 7780
		private SecurityRuleSet m_ruleSet;

		// Token: 0x04001E65 RID: 7781
		private bool m_skipVerificationInFullTrust;
	}
}
