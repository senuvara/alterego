﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	/// <summary>Allows an assembly to be called by partially trusted code. Without this declaration, only fully trusted callers are able to use the assembly. This class cannot be inherited.</summary>
	// Token: 0x02000546 RID: 1350
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	public sealed class AllowPartiallyTrustedCallersAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Security.AllowPartiallyTrustedCallersAttribute" /> class. </summary>
		// Token: 0x06003A59 RID: 14937 RVA: 0x000020BF File Offset: 0x000002BF
		public AllowPartiallyTrustedCallersAttribute()
		{
		}

		/// <summary>Gets or sets the default partial trust visibility for code that is marked with the <see cref="T:System.Security.AllowPartiallyTrustedCallersAttribute" /> (APTCA) attribute.</summary>
		/// <returns>One of the enumeration values. The default is <see cref="F:System.Security.PartialTrustVisibilityLevel.VisibleToAllHosts" />. </returns>
		// Token: 0x17000986 RID: 2438
		// (get) Token: 0x06003A5A RID: 14938 RVA: 0x000CB09D File Offset: 0x000C929D
		// (set) Token: 0x06003A5B RID: 14939 RVA: 0x000CB0A5 File Offset: 0x000C92A5
		public PartialTrustVisibilityLevel PartialTrustVisibilityLevel
		{
			get
			{
				return this._visibilityLevel;
			}
			set
			{
				this._visibilityLevel = value;
			}
		}

		// Token: 0x04001E58 RID: 7768
		private PartialTrustVisibilityLevel _visibilityLevel;
	}
}
