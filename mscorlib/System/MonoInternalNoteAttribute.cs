﻿using System;

namespace System
{
	// Token: 0x020000E4 RID: 228
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoInternalNoteAttribute : MonoTODOAttribute
	{
		// Token: 0x0600090C RID: 2316 RVA: 0x0003053C File Offset: 0x0002E73C
		public MonoInternalNoteAttribute(string comment) : base(comment)
		{
		}
	}
}
