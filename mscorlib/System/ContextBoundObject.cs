﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Defines the base class for all context-bound classes.</summary>
	// Token: 0x02000130 RID: 304
	[ComVisible(true)]
	[Serializable]
	public abstract class ContextBoundObject : MarshalByRefObject
	{
		/// <summary>Instantiates an instance of the <see cref="T:System.ContextBoundObject" /> class.</summary>
		// Token: 0x06000B67 RID: 2919 RVA: 0x00035633 File Offset: 0x00033833
		protected ContextBoundObject()
		{
		}
	}
}
