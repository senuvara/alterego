﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace System
{
	/// <summary>Provides the base class for value types.</summary>
	// Token: 0x02000242 RID: 578
	[ComVisible(true)]
	[Serializable]
	public abstract class ValueType
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ValueType" /> class. </summary>
		// Token: 0x06001B36 RID: 6966 RVA: 0x00002050 File Offset: 0x00000250
		protected ValueType()
		{
		}

		// Token: 0x06001B37 RID: 6967
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool InternalEquals(object o1, object o2, out object[] fields);

		// Token: 0x06001B38 RID: 6968 RVA: 0x00066D58 File Offset: 0x00064F58
		internal static bool DefaultEquals(object o1, object o2)
		{
			if (o1 == null && o2 == null)
			{
				return true;
			}
			if (o1 == null || o2 == null)
			{
				return false;
			}
			RuntimeType left = (RuntimeType)o1.GetType();
			RuntimeType right = (RuntimeType)o2.GetType();
			if (left != right)
			{
				return false;
			}
			object[] array;
			bool result = ValueType.InternalEquals(o1, o2, out array);
			if (array == null)
			{
				return result;
			}
			for (int i = 0; i < array.Length; i += 2)
			{
				object obj = array[i];
				object obj2 = array[i + 1];
				if (obj == null)
				{
					if (obj2 != null)
					{
						return false;
					}
				}
				else if (!obj.Equals(obj2))
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>Indicates whether this instance and a specified object are equal.</summary>
		/// <param name="obj">The object to compare with the current instance. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, <see langword="false" />. </returns>
		// Token: 0x06001B39 RID: 6969 RVA: 0x0003D22B File Offset: 0x0003B42B
		public override bool Equals(object obj)
		{
			return ValueType.DefaultEquals(this, obj);
		}

		// Token: 0x06001B3A RID: 6970
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int InternalGetHashCode(object o, out object[] fields);

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer that is the hash code for this instance.</returns>
		// Token: 0x06001B3B RID: 6971 RVA: 0x00066DD8 File Offset: 0x00064FD8
		public override int GetHashCode()
		{
			object[] array;
			int num = ValueType.InternalGetHashCode(this, out array);
			if (array != null)
			{
				for (int i = 0; i < array.Length; i++)
				{
					if (array[i] != null)
					{
						num ^= array[i].GetHashCode();
					}
				}
			}
			return num;
		}

		// Token: 0x06001B3C RID: 6972 RVA: 0x00066E10 File Offset: 0x00065010
		internal static int GetHashCodeOfPtr(IntPtr ptr)
		{
			int num = (int)ptr;
			int num2 = ValueType.Internal.hash_code_of_ptr_seed;
			if (num2 == 0)
			{
				num2 = num;
				Interlocked.CompareExchange(ref ValueType.Internal.hash_code_of_ptr_seed, num2, 0);
				num2 = ValueType.Internal.hash_code_of_ptr_seed;
			}
			return num - num2;
		}

		/// <summary>Returns the fully qualified type name of this instance.</summary>
		/// <returns>The fully qualified type name.</returns>
		// Token: 0x06001B3D RID: 6973 RVA: 0x00066E45 File Offset: 0x00065045
		public override string ToString()
		{
			return base.GetType().FullName;
		}

		// Token: 0x02000243 RID: 579
		private static class Internal
		{
			// Token: 0x06001B3E RID: 6974 RVA: 0x000020D3 File Offset: 0x000002D3
			// Note: this type is marked as 'beforefieldinit'.
			static Internal()
			{
			}

			// Token: 0x04000F36 RID: 3894
			public static int hash_code_of_ptr_seed;
		}
	}
}
