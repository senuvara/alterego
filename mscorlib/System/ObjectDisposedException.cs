﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>The exception that is thrown when an operation is performed on a disposed object.</summary>
	// Token: 0x02000197 RID: 407
	[ComVisible(true)]
	[Serializable]
	public class ObjectDisposedException : InvalidOperationException
	{
		// Token: 0x0600119B RID: 4507 RVA: 0x00048FCA File Offset: 0x000471CA
		private ObjectDisposedException() : this(null, Environment.GetResourceString("Cannot access a disposed object."))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ObjectDisposedException" /> class with a string containing the name of the disposed object.</summary>
		/// <param name="objectName">A string containing the name of the disposed object. </param>
		// Token: 0x0600119C RID: 4508 RVA: 0x00048FDD File Offset: 0x000471DD
		public ObjectDisposedException(string objectName) : this(objectName, Environment.GetResourceString("Cannot access a disposed object."))
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ObjectDisposedException" /> class with the specified object name and message.</summary>
		/// <param name="objectName">The name of the disposed object. </param>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		// Token: 0x0600119D RID: 4509 RVA: 0x00048FF0 File Offset: 0x000471F0
		public ObjectDisposedException(string objectName, string message) : base(message)
		{
			base.SetErrorCode(-2146232798);
			this.objectName = objectName;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ObjectDisposedException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception. If <paramref name="innerException" /> is not <see langword="null" />, the current exception is raised in a <see langword="catch" /> block that handles the inner exception.</param>
		// Token: 0x0600119E RID: 4510 RVA: 0x0004900B File Offset: 0x0004720B
		public ObjectDisposedException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146232798);
		}

		/// <summary>Gets the message that describes the error.</summary>
		/// <returns>A string that describes the error.</returns>
		// Token: 0x17000205 RID: 517
		// (get) Token: 0x0600119F RID: 4511 RVA: 0x00049020 File Offset: 0x00047220
		public override string Message
		{
			get
			{
				string text = this.ObjectName;
				if (text == null || text.Length == 0)
				{
					return base.Message;
				}
				string resourceString = Environment.GetResourceString("Object name: '{0}'.", new object[]
				{
					text
				});
				return base.Message + Environment.NewLine + resourceString;
			}
		}

		/// <summary>Gets the name of the disposed object.</summary>
		/// <returns>A string containing the name of the disposed object.</returns>
		// Token: 0x17000206 RID: 518
		// (get) Token: 0x060011A0 RID: 4512 RVA: 0x0004906C File Offset: 0x0004726C
		public string ObjectName
		{
			get
			{
				if (this.objectName == null && !CompatibilitySwitches.IsAppEarlierThanWindowsPhone8)
				{
					return string.Empty;
				}
				return this.objectName;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ObjectDisposedException" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		// Token: 0x060011A1 RID: 4513 RVA: 0x00049089 File Offset: 0x00047289
		protected ObjectDisposedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.objectName = info.GetString("ObjectName");
		}

		/// <summary>Retrieves the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the parameter name and additional exception information.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		// Token: 0x060011A2 RID: 4514 RVA: 0x000490A4 File Offset: 0x000472A4
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("ObjectName", this.ObjectName, typeof(string));
		}

		// Token: 0x04000A12 RID: 2578
		private string objectName;
	}
}
