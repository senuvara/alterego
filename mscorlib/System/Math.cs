﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Security;

namespace System
{
	/// <summary>Provides constants and static methods for trigonometric, logarithmic, and other common mathematical functions.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x02000188 RID: 392
	public static class Math
	{
		/// <summary>Returns the angle whose cosine is the specified number.</summary>
		/// <param name="d">A number representing a cosine, where <paramref name="d" /> must be greater than or equal to -1, but less than or equal to 1. </param>
		/// <returns>An angle, θ, measured in radians, such that 0 ≤θ≤π-or- 
		///     <see cref="F:System.Double.NaN" /> if <paramref name="d" /> &lt; -1 or <paramref name="d" /> &gt; 1 or <paramref name="d" /> equals <see cref="F:System.Double.NaN" />.</returns>
		// Token: 0x060010E5 RID: 4325
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Acos(double d);

		/// <summary>Returns the angle whose sine is the specified number.</summary>
		/// <param name="d">A number representing a sine, where <paramref name="d" /> must be greater than or equal to -1, but less than or equal to 1. </param>
		/// <returns>An angle, θ, measured in radians, such that -π/2 ≤θ≤π/2 -or- 
		///     <see cref="F:System.Double.NaN" /> if <paramref name="d" /> &lt; -1 or <paramref name="d" /> &gt; 1 or <paramref name="d" /> equals <see cref="F:System.Double.NaN" />.</returns>
		// Token: 0x060010E6 RID: 4326
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Asin(double d);

		/// <summary>Returns the angle whose tangent is the specified number.</summary>
		/// <param name="d">A number representing a tangent. </param>
		/// <returns>An angle, θ, measured in radians, such that -π/2 ≤θ≤π/2.-or- 
		///     <see cref="F:System.Double.NaN" /> if <paramref name="d" /> equals <see cref="F:System.Double.NaN" />, -π/2 rounded to double precision (-1.5707963267949) if <paramref name="d" /> equals <see cref="F:System.Double.NegativeInfinity" />, or π/2 rounded to double precision (1.5707963267949) if <paramref name="d" /> equals <see cref="F:System.Double.PositiveInfinity" />.</returns>
		// Token: 0x060010E7 RID: 4327
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Atan(double d);

		/// <summary>Returns the angle whose tangent is the quotient of two specified numbers.</summary>
		/// <param name="y">The y coordinate of a point. </param>
		/// <param name="x">The x coordinate of a point. </param>
		/// <returns>An angle, θ, measured in radians, such that -π≤θ≤π, and tan(θ) = <paramref name="y" /> / <paramref name="x" />, where (<paramref name="x" />, <paramref name="y" />) is a point in the Cartesian plane. Observe the following: For (<paramref name="x" />, <paramref name="y" />) in quadrant 1, 0 &lt; θ &lt; π/2.For (<paramref name="x" />, <paramref name="y" />) in quadrant 2, π/2 &lt; θ≤π.For (<paramref name="x" />, <paramref name="y" />) in quadrant 3, -π &lt; θ &lt; -π/2.For (<paramref name="x" />, <paramref name="y" />) in quadrant 4, -π/2 &lt; θ &lt; 0.
		///     For points on the boundaries of the quadrants, the return value is the following:If y is 0 and x is not negative, θ = 0.If y is 0 and x is negative, θ = π.If y is positive and x is 0, θ = π/2.If y is negative and x is 0, θ = -π/2.
		///         If y is 0 and x is 0, θ = 0. If <paramref name="x" /> or <paramref name="y" /> is <see cref="F:System.Double.NaN" />, or if <paramref name="x" /> and <paramref name="y" /> are either <see cref="F:System.Double.PositiveInfinity" /> or <see cref="F:System.Double.NegativeInfinity" />, the method returns <see cref="F:System.Double.NaN" />.</returns>
		// Token: 0x060010E8 RID: 4328
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Atan2(double y, double x);

		/// <summary>Returns the smallest integral value that is greater than or equal to the specified decimal number.</summary>
		/// <param name="d">A decimal number. </param>
		/// <returns>The smallest integral value that is greater than or equal to <paramref name="d" />. Note that this method returns a <see cref="T:System.Decimal" /> instead of an integral type.</returns>
		// Token: 0x060010E9 RID: 4329 RVA: 0x000474BB File Offset: 0x000456BB
		public static decimal Ceiling(decimal d)
		{
			return decimal.Ceiling(d);
		}

		/// <summary>Returns the smallest integral value that is greater than or equal to the specified double-precision floating-point number.</summary>
		/// <param name="a">A double-precision floating-point number. </param>
		/// <returns>The smallest integral value that is greater than or equal to <paramref name="a" />. If <paramref name="a" /> is equal to <see cref="F:System.Double.NaN" />, <see cref="F:System.Double.NegativeInfinity" />, or <see cref="F:System.Double.PositiveInfinity" />, that value is returned. Note that this method returns a <see cref="T:System.Double" /> instead of an integral type.</returns>
		// Token: 0x060010EA RID: 4330
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Ceiling(double a);

		/// <summary>Returns the cosine of the specified angle.</summary>
		/// <param name="d">An angle, measured in radians. </param>
		/// <returns>The cosine of <paramref name="d" />. If <paramref name="d" /> is equal to <see cref="F:System.Double.NaN" />, <see cref="F:System.Double.NegativeInfinity" />, or <see cref="F:System.Double.PositiveInfinity" />, this method returns <see cref="F:System.Double.NaN" />.</returns>
		// Token: 0x060010EB RID: 4331
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Cos(double d);

		/// <summary>Returns the hyperbolic cosine of the specified angle.</summary>
		/// <param name="value">An angle, measured in radians. </param>
		/// <returns>The hyperbolic cosine of <paramref name="value" />. If <paramref name="value" /> is equal to <see cref="F:System.Double.NegativeInfinity" /> or <see cref="F:System.Double.PositiveInfinity" />, <see cref="F:System.Double.PositiveInfinity" /> is returned. If <paramref name="value" /> is equal to <see cref="F:System.Double.NaN" />, <see cref="F:System.Double.NaN" /> is returned.</returns>
		// Token: 0x060010EC RID: 4332
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Cosh(double value);

		/// <summary>Returns the largest integer less than or equal to the specified decimal number.</summary>
		/// <param name="d">A decimal number. </param>
		/// <returns>The largest integer less than or equal to <paramref name="d" />.  Note that the method returns an integral value of type <see cref="T:System.Math" />. </returns>
		// Token: 0x060010ED RID: 4333 RVA: 0x000474C3 File Offset: 0x000456C3
		public static decimal Floor(decimal d)
		{
			return decimal.Floor(d);
		}

		/// <summary>Returns the largest integer less than or equal to the specified double-precision floating-point number.</summary>
		/// <param name="d">A double-precision floating-point number. </param>
		/// <returns>The largest integer less than or equal to <paramref name="d" />. If <paramref name="d" /> is equal to <see cref="F:System.Double.NaN" />, <see cref="F:System.Double.NegativeInfinity" />, or <see cref="F:System.Double.PositiveInfinity" />, that value is returned.</returns>
		// Token: 0x060010EE RID: 4334
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Floor(double d);

		// Token: 0x060010EF RID: 4335 RVA: 0x000474CC File Offset: 0x000456CC
		[SecuritySafeCritical]
		private unsafe static double InternalRound(double value, int digits, MidpointRounding mode)
		{
			if (Math.Abs(value) < Math.doubleRoundLimit)
			{
				double num = Math.roundPower10Double[digits];
				value *= num;
				if (mode == MidpointRounding.AwayFromZero)
				{
					double value2 = Math.SplitFractionDouble(&value);
					if (Math.Abs(value2) >= 0.5)
					{
						value += (double)Math.Sign(value2);
					}
				}
				else
				{
					value = Math.Round(value);
				}
				value /= num;
			}
			return value;
		}

		// Token: 0x060010F0 RID: 4336 RVA: 0x0004752C File Offset: 0x0004572C
		[SecuritySafeCritical]
		private unsafe static double InternalTruncate(double d)
		{
			Math.SplitFractionDouble(&d);
			return d;
		}

		/// <summary>Returns the sine of the specified angle.</summary>
		/// <param name="a">An angle, measured in radians. </param>
		/// <returns>The sine of <paramref name="a" />. If <paramref name="a" /> is equal to <see cref="F:System.Double.NaN" />, <see cref="F:System.Double.NegativeInfinity" />, or <see cref="F:System.Double.PositiveInfinity" />, this method returns <see cref="F:System.Double.NaN" />.</returns>
		// Token: 0x060010F1 RID: 4337
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Sin(double a);

		/// <summary>Returns the tangent of the specified angle.</summary>
		/// <param name="a">An angle, measured in radians. </param>
		/// <returns>The tangent of <paramref name="a" />. If <paramref name="a" /> is equal to <see cref="F:System.Double.NaN" />, <see cref="F:System.Double.NegativeInfinity" />, or <see cref="F:System.Double.PositiveInfinity" />, this method returns <see cref="F:System.Double.NaN" />.</returns>
		// Token: 0x060010F2 RID: 4338
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Tan(double a);

		/// <summary>Returns the hyperbolic sine of the specified angle.</summary>
		/// <param name="value">An angle, measured in radians. </param>
		/// <returns>The hyperbolic sine of <paramref name="value" />. If <paramref name="value" /> is equal to <see cref="F:System.Double.NegativeInfinity" />, <see cref="F:System.Double.PositiveInfinity" />, or <see cref="F:System.Double.NaN" />, this method returns a <see cref="T:System.Double" /> equal to <paramref name="value" />.</returns>
		// Token: 0x060010F3 RID: 4339
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Sinh(double value);

		/// <summary>Returns the hyperbolic tangent of the specified angle.</summary>
		/// <param name="value">An angle, measured in radians. </param>
		/// <returns>The hyperbolic tangent of <paramref name="value" />. If <paramref name="value" /> is equal to <see cref="F:System.Double.NegativeInfinity" />, this method returns -1. If value is equal to <see cref="F:System.Double.PositiveInfinity" />, this method returns 1. If <paramref name="value" /> is equal to <see cref="F:System.Double.NaN" />, this method returns <see cref="F:System.Double.NaN" />.</returns>
		// Token: 0x060010F4 RID: 4340
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Tanh(double value);

		/// <summary>Rounds a double-precision floating-point value to the nearest integral value.</summary>
		/// <param name="a">A double-precision floating-point number to be rounded. </param>
		/// <returns>The integer nearest <paramref name="a" />. If the fractional component of <paramref name="a" /> is halfway between two integers, one of which is even and the other odd, then the even number is returned. Note that this method returns a <see cref="T:System.Double" /> instead of an integral type.</returns>
		// Token: 0x060010F5 RID: 4341
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Round(double a);

		/// <summary>Rounds a double-precision floating-point value to a specified number of fractional digits.</summary>
		/// <param name="value">A double-precision floating-point number to be rounded. </param>
		/// <param name="digits">The number of fractional digits in the return value. </param>
		/// <returns>The number nearest to <paramref name="value" /> that contains a number of fractional digits equal to <paramref name="digits" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="digits" /> is less than 0 or greater than 15. </exception>
		// Token: 0x060010F6 RID: 4342 RVA: 0x00047538 File Offset: 0x00045738
		public static double Round(double value, int digits)
		{
			if (digits < 0 || digits > 15)
			{
				throw new ArgumentOutOfRangeException("digits", Environment.GetResourceString("Rounding digits must be between 0 and 15, inclusive."));
			}
			return Math.InternalRound(value, digits, MidpointRounding.ToEven);
		}

		/// <summary>Rounds a double-precision floating-point value to the nearest integer. A parameter specifies how to round the value if it is midway between two numbers.</summary>
		/// <param name="value">A double-precision floating-point number to be rounded. </param>
		/// <param name="mode">Specification for how to round <paramref name="value" /> if it is midway between two other numbers.</param>
		/// <returns>The integer nearest <paramref name="value" />. If <paramref name="value" /> is halfway between two integers, one of which is even and the other odd, then <paramref name="mode" /> determines which of the two is returned.</returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mode" /> is not a valid value of <see cref="T:System.MidpointRounding" />.</exception>
		// Token: 0x060010F7 RID: 4343 RVA: 0x00047560 File Offset: 0x00045760
		public static double Round(double value, MidpointRounding mode)
		{
			return Math.Round(value, 0, mode);
		}

		/// <summary>Rounds a double-precision floating-point value to a specified number of fractional digits. A parameter specifies how to round the value if it is midway between two numbers.</summary>
		/// <param name="value">A double-precision floating-point number to be rounded. </param>
		/// <param name="digits">The number of fractional digits in the return value. </param>
		/// <param name="mode">Specification for how to round <paramref name="value" /> if it is midway between two other numbers.</param>
		/// <returns>The number nearest to <paramref name="value" /> that has a number of fractional digits equal to <paramref name="digits" />. If <paramref name="value" /> has fewer fractional digits than <paramref name="digits" />, <paramref name="value" /> is returned unchanged.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="digits" /> is less than 0 or greater than 15. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mode" /> is not a valid value of <see cref="T:System.MidpointRounding" />.</exception>
		// Token: 0x060010F8 RID: 4344 RVA: 0x0004756C File Offset: 0x0004576C
		public static double Round(double value, int digits, MidpointRounding mode)
		{
			if (digits < 0 || digits > 15)
			{
				throw new ArgumentOutOfRangeException("digits", Environment.GetResourceString("Rounding digits must be between 0 and 15, inclusive."));
			}
			if (mode < MidpointRounding.ToEven || mode > MidpointRounding.AwayFromZero)
			{
				throw new ArgumentException(Environment.GetResourceString("The value '{0}' is not valid for this usage of the type {1}.", new object[]
				{
					mode,
					"MidpointRounding"
				}), "mode");
			}
			return Math.InternalRound(value, digits, mode);
		}

		/// <summary>Rounds a decimal value to the nearest integral value.</summary>
		/// <param name="d">A decimal number to be rounded. </param>
		/// <returns>The integer nearest parameter <paramref name="d" />. If the fractional component of <paramref name="d" /> is halfway between two integers, one of which is even and the other odd, the even number is returned. Note that this method returns a <see cref="T:System.Decimal" /> instead of an integral type.</returns>
		/// <exception cref="T:System.OverflowException">The result is outside the range of a <see cref="T:System.Decimal" />.</exception>
		// Token: 0x060010F9 RID: 4345 RVA: 0x0003A82F File Offset: 0x00038A2F
		public static decimal Round(decimal d)
		{
			return decimal.Round(d, 0);
		}

		/// <summary>Rounds a decimal value to a specified number of fractional digits.</summary>
		/// <param name="d">A decimal number to be rounded. </param>
		/// <param name="decimals">The number of decimal places in the return value. </param>
		/// <returns>The number nearest to <paramref name="d" /> that contains a number of fractional digits equal to <paramref name="decimals" />. </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="decimals" /> is less than 0 or greater than 28. </exception>
		/// <exception cref="T:System.OverflowException">The result is outside the range of a <see cref="T:System.Decimal" />.</exception>
		// Token: 0x060010FA RID: 4346 RVA: 0x000475D3 File Offset: 0x000457D3
		public static decimal Round(decimal d, int decimals)
		{
			return decimal.Round(d, decimals);
		}

		/// <summary>Rounds a decimal value to the nearest integer. A parameter specifies how to round the value if it is midway between two numbers.</summary>
		/// <param name="d">A decimal number to be rounded. </param>
		/// <param name="mode">Specification for how to round <paramref name="d" /> if it is midway between two other numbers.</param>
		/// <returns>The integer nearest <paramref name="d" />. If <paramref name="d" /> is halfway between two numbers, one of which is even and the other odd, then <paramref name="mode" /> determines which of the two is returned. </returns>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mode" /> is not a valid value of <see cref="T:System.MidpointRounding" />.</exception>
		/// <exception cref="T:System.OverflowException">The result is outside the range of a <see cref="T:System.Decimal" />.</exception>
		// Token: 0x060010FB RID: 4347 RVA: 0x0003A843 File Offset: 0x00038A43
		public static decimal Round(decimal d, MidpointRounding mode)
		{
			return decimal.Round(d, 0, mode);
		}

		/// <summary>Rounds a decimal value to a specified number of fractional digits. A parameter specifies how to round the value if it is midway between two numbers.</summary>
		/// <param name="d">A decimal number to be rounded. </param>
		/// <param name="decimals">The number of decimal places in the return value. </param>
		/// <param name="mode">Specification for how to round <paramref name="d" /> if it is midway between two other numbers.</param>
		/// <returns>The number nearest to <paramref name="d" /> that contains a number of fractional digits equal to <paramref name="decimals" />. If <paramref name="d" /> has fewer fractional digits than <paramref name="decimals" />, <paramref name="d" /> is returned unchanged.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="decimals" /> is less than 0 or greater than 28. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="mode" /> is not a valid value of <see cref="T:System.MidpointRounding" />.</exception>
		/// <exception cref="T:System.OverflowException">The result is outside the range of a <see cref="T:System.Decimal" />.</exception>
		// Token: 0x060010FC RID: 4348 RVA: 0x000475DC File Offset: 0x000457DC
		public static decimal Round(decimal d, int decimals, MidpointRounding mode)
		{
			return decimal.Round(d, decimals, mode);
		}

		// Token: 0x060010FD RID: 4349
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern double SplitFractionDouble(double* value);

		/// <summary>Calculates the integral part of a specified decimal number. </summary>
		/// <param name="d">A number to truncate.</param>
		/// <returns>The integral part of <paramref name="d" />; that is, the number that remains after any fractional digits have been discarded.</returns>
		// Token: 0x060010FE RID: 4350 RVA: 0x000475E6 File Offset: 0x000457E6
		public static decimal Truncate(decimal d)
		{
			return decimal.Truncate(d);
		}

		/// <summary>Calculates the integral part of a specified double-precision floating-point number. </summary>
		/// <param name="d">A number to truncate.</param>
		/// <returns>The integral part of <paramref name="d" />; that is, the number that remains after any fractional digits have been discarded, or one of the values listed in the following table. 
		///             <paramref name="d" />
		///           Return value
		///             <see cref="F:System.Double.NaN" />
		///
		///             <see cref="F:System.Double.NaN" />
		///
		///             <see cref="F:System.Double.NegativeInfinity" />
		///
		///             <see cref="F:System.Double.NegativeInfinity" />
		///
		///             <see cref="F:System.Double.PositiveInfinity" />
		///
		///             <see cref="F:System.Double.PositiveInfinity" />
		///           </returns>
		// Token: 0x060010FF RID: 4351 RVA: 0x000475EE File Offset: 0x000457EE
		public static double Truncate(double d)
		{
			return Math.InternalTruncate(d);
		}

		/// <summary>Returns the square root of a specified number.</summary>
		/// <param name="d">The number whose square root is to be found. </param>
		/// <returns>One of the values in the following table. 
		///             <paramref name="d" /> parameter Return value Zero or positive The positive square root of <paramref name="d" />. Negative 
		///             <see cref="F:System.Double.NaN" />
		///           Equals <see cref="F:System.Double.NaN" />
		///             <see cref="F:System.Double.NaN" />
		///           Equals <see cref="F:System.Double.PositiveInfinity" />
		///             <see cref="F:System.Double.PositiveInfinity" />
		///           </returns>
		// Token: 0x06001100 RID: 4352
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Sqrt(double d);

		/// <summary>Returns the natural (base <see langword="e" />) logarithm of a specified number.</summary>
		/// <param name="d">The number whose logarithm is to be found. </param>
		/// <returns>One of the values in the following table. 
		///             <paramref name="d" /> parameterReturn value Positive The natural logarithm of <paramref name="d" />; that is, ln <paramref name="d" />, or log e<paramref name="d" />Zero 
		///             <see cref="F:System.Double.NegativeInfinity" />
		///           Negative 
		///             <see cref="F:System.Double.NaN" />
		///           Equal to <see cref="F:System.Double.NaN" />
		///             <see cref="F:System.Double.NaN" />
		///           Equal to <see cref="F:System.Double.PositiveInfinity" />
		///             <see cref="F:System.Double.PositiveInfinity" />
		///           </returns>
		// Token: 0x06001101 RID: 4353
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Log(double d);

		/// <summary>Returns the base 10 logarithm of a specified number.</summary>
		/// <param name="d">A number whose logarithm is to be found. </param>
		/// <returns>One of the values in the following table. 
		///             <paramref name="d" /> parameter Return value Positive The base 10 log of <paramref name="d" />; that is, log 10<paramref name="d" />. Zero 
		///             <see cref="F:System.Double.NegativeInfinity" />
		///           Negative 
		///             <see cref="F:System.Double.NaN" />
		///           Equal to <see cref="F:System.Double.NaN" />
		///             <see cref="F:System.Double.NaN" />
		///           Equal to <see cref="F:System.Double.PositiveInfinity" />
		///             <see cref="F:System.Double.PositiveInfinity" />
		///           </returns>
		// Token: 0x06001102 RID: 4354
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Log10(double d);

		/// <summary>Returns <see langword="e" /> raised to the specified power.</summary>
		/// <param name="d">A number specifying a power. </param>
		/// <returns>The number <see langword="e" /> raised to the power <paramref name="d" />. If <paramref name="d" /> equals <see cref="F:System.Double.NaN" /> or <see cref="F:System.Double.PositiveInfinity" />, that value is returned. If <paramref name="d" /> equals <see cref="F:System.Double.NegativeInfinity" />, 0 is returned.</returns>
		// Token: 0x06001103 RID: 4355
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Exp(double d);

		/// <summary>Returns a specified number raised to the specified power.</summary>
		/// <param name="x">A double-precision floating-point number to be raised to a power. </param>
		/// <param name="y">A double-precision floating-point number that specifies a power. </param>
		/// <returns>The number <paramref name="x" /> raised to the power <paramref name="y" />.</returns>
		// Token: 0x06001104 RID: 4356
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Pow(double x, double y);

		/// <summary>Returns the remainder resulting from the division of a specified number by another specified number.</summary>
		/// <param name="x">A dividend. </param>
		/// <param name="y">A divisor. </param>
		/// <returns>A number equal to <paramref name="x" /> - (<paramref name="y" /> Q), where Q is the quotient of <paramref name="x" /> / <paramref name="y" /> rounded to the nearest integer (if <paramref name="x" /> / <paramref name="y" /> falls halfway between two integers, the even integer is returned).If <paramref name="x" /> - (<paramref name="y" /> Q) is zero, the value +0 is returned if <paramref name="x" /> is positive, or -0 if <paramref name="x" /> is negative.If <paramref name="y" /> = 0, <see cref="F:System.Double.NaN" /> is returned.</returns>
		// Token: 0x06001105 RID: 4357 RVA: 0x000475F8 File Offset: 0x000457F8
		public static double IEEERemainder(double x, double y)
		{
			if (double.IsNaN(x))
			{
				return x;
			}
			if (double.IsNaN(y))
			{
				return y;
			}
			double num = x % y;
			if (double.IsNaN(num))
			{
				return double.NaN;
			}
			if (num == 0.0 && double.IsNegative(x))
			{
				return double.NegativeZero;
			}
			double num2 = num - Math.Abs(y) * (double)Math.Sign(x);
			if (Math.Abs(num2) == Math.Abs(num))
			{
				double num3 = x / y;
				if (Math.Abs(Math.Round(num3)) > Math.Abs(num3))
				{
					return num2;
				}
				return num;
			}
			else
			{
				if (Math.Abs(num2) < Math.Abs(num))
				{
					return num2;
				}
				return num;
			}
		}

		/// <summary>Returns the absolute value of an 8-bit signed integer.</summary>
		/// <param name="value">A number that is greater than <see cref="F:System.SByte.MinValue" />, but less than or equal to <see cref="F:System.SByte.MaxValue" />.</param>
		/// <returns>An 8-bit signed integer, x, such that 0 ≤ x ≤<see cref="F:System.SByte.MaxValue" />.</returns>
		/// <exception cref="T:System.OverflowException">
		///         <paramref name="value" /> equals <see cref="F:System.SByte.MinValue" />. </exception>
		// Token: 0x06001106 RID: 4358 RVA: 0x00047694 File Offset: 0x00045894
		[CLSCompliant(false)]
		public static sbyte Abs(sbyte value)
		{
			if (value >= 0)
			{
				return value;
			}
			return Math.AbsHelper(value);
		}

		// Token: 0x06001107 RID: 4359 RVA: 0x000476A2 File Offset: 0x000458A2
		private static sbyte AbsHelper(sbyte value)
		{
			if (value == -128)
			{
				throw new OverflowException(Environment.GetResourceString("Negating the minimum value of a twos complement number is invalid."));
			}
			return -value;
		}

		/// <summary>Returns the absolute value of a 16-bit signed integer.</summary>
		/// <param name="value">A number that is greater than <see cref="F:System.Int16.MinValue" />, but less than or equal to <see cref="F:System.Int16.MaxValue" />.</param>
		/// <returns>A 16-bit signed integer, x, such that 0 ≤ x ≤<see cref="F:System.Int16.MaxValue" />.</returns>
		/// <exception cref="T:System.OverflowException">
		///         <paramref name="value" /> equals <see cref="F:System.Int16.MinValue" />. </exception>
		// Token: 0x06001108 RID: 4360 RVA: 0x000476BC File Offset: 0x000458BC
		public static short Abs(short value)
		{
			if (value >= 0)
			{
				return value;
			}
			return Math.AbsHelper(value);
		}

		// Token: 0x06001109 RID: 4361 RVA: 0x000476CA File Offset: 0x000458CA
		private static short AbsHelper(short value)
		{
			if (value == -32768)
			{
				throw new OverflowException(Environment.GetResourceString("Negating the minimum value of a twos complement number is invalid."));
			}
			return -value;
		}

		/// <summary>Returns the absolute value of a 32-bit signed integer.</summary>
		/// <param name="value">A number that is greater than <see cref="F:System.Int32.MinValue" />, but less than or equal to <see cref="F:System.Int32.MaxValue" />.</param>
		/// <returns>A 32-bit signed integer, x, such that 0 ≤ x ≤<see cref="F:System.Int32.MaxValue" />.</returns>
		/// <exception cref="T:System.OverflowException">
		///         <paramref name="value" /> equals <see cref="F:System.Int32.MinValue" />. </exception>
		// Token: 0x0600110A RID: 4362 RVA: 0x000476E7 File Offset: 0x000458E7
		public static int Abs(int value)
		{
			if (value >= 0)
			{
				return value;
			}
			return Math.AbsHelper(value);
		}

		// Token: 0x0600110B RID: 4363 RVA: 0x000476F5 File Offset: 0x000458F5
		private static int AbsHelper(int value)
		{
			if (value == -2147483648)
			{
				throw new OverflowException(Environment.GetResourceString("Negating the minimum value of a twos complement number is invalid."));
			}
			return -value;
		}

		/// <summary>Returns the absolute value of a 64-bit signed integer.</summary>
		/// <param name="value">A number that is greater than <see cref="F:System.Int64.MinValue" />, but less than or equal to <see cref="F:System.Int64.MaxValue" />.</param>
		/// <returns>A 64-bit signed integer, x, such that 0 ≤ x ≤<see cref="F:System.Int64.MaxValue" />.</returns>
		/// <exception cref="T:System.OverflowException">
		///         <paramref name="value" /> equals <see cref="F:System.Int64.MinValue" />. </exception>
		// Token: 0x0600110C RID: 4364 RVA: 0x00047711 File Offset: 0x00045911
		public static long Abs(long value)
		{
			if (value >= 0L)
			{
				return value;
			}
			return Math.AbsHelper(value);
		}

		// Token: 0x0600110D RID: 4365 RVA: 0x00047720 File Offset: 0x00045920
		private static long AbsHelper(long value)
		{
			if (value == -9223372036854775808L)
			{
				throw new OverflowException(Environment.GetResourceString("Negating the minimum value of a twos complement number is invalid."));
			}
			return -value;
		}

		/// <summary>Returns the absolute value of a single-precision floating-point number.</summary>
		/// <param name="value">A number that is greater than or equal to <see cref="F:System.Single.MinValue" />, but less than or equal to <see cref="F:System.Single.MaxValue" />.</param>
		/// <returns>A single-precision floating-point number, x, such that 0 ≤ x ≤<see cref="F:System.Single.MaxValue" />.</returns>
		// Token: 0x0600110E RID: 4366
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float Abs(float value);

		/// <summary>Returns the absolute value of a double-precision floating-point number.</summary>
		/// <param name="value">A number that is greater than or equal to <see cref="F:System.Double.MinValue" />, but less than or equal to <see cref="F:System.Double.MaxValue" />.</param>
		/// <returns>A double-precision floating-point number, x, such that 0 ≤ x ≤<see cref="F:System.Double.MaxValue" />.</returns>
		// Token: 0x0600110F RID: 4367
		[SecuritySafeCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Abs(double value);

		/// <summary>Returns the absolute value of a <see cref="T:System.Decimal" /> number.</summary>
		/// <param name="value">A number that is greater than or equal to <see cref="F:System.Decimal.MinValue" />, but less than or equal to <see cref="F:System.Decimal.MaxValue" />. </param>
		/// <returns>A decimal number, x, such that 0 ≤ x ≤<see cref="F:System.Decimal.MaxValue" />.</returns>
		// Token: 0x06001110 RID: 4368 RVA: 0x00047740 File Offset: 0x00045940
		public static decimal Abs(decimal value)
		{
			return decimal.Abs(value);
		}

		/// <summary>Returns the larger of two 8-bit signed integers.</summary>
		/// <param name="val1">The first of two 8-bit signed integers to compare. </param>
		/// <param name="val2">The second of two 8-bit signed integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x06001111 RID: 4369 RVA: 0x00047748 File Offset: 0x00045948
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static sbyte Max(sbyte val1, sbyte val2)
		{
			if (val1 < val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the larger of two 8-bit unsigned integers.</summary>
		/// <param name="val1">The first of two 8-bit unsigned integers to compare. </param>
		/// <param name="val2">The second of two 8-bit unsigned integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x06001112 RID: 4370 RVA: 0x00047748 File Offset: 0x00045948
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static byte Max(byte val1, byte val2)
		{
			if (val1 < val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the larger of two 16-bit signed integers.</summary>
		/// <param name="val1">The first of two 16-bit signed integers to compare. </param>
		/// <param name="val2">The second of two 16-bit signed integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x06001113 RID: 4371 RVA: 0x00047748 File Offset: 0x00045948
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static short Max(short val1, short val2)
		{
			if (val1 < val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the larger of two 16-bit unsigned integers.</summary>
		/// <param name="val1">The first of two 16-bit unsigned integers to compare. </param>
		/// <param name="val2">The second of two 16-bit unsigned integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x06001114 RID: 4372 RVA: 0x00047748 File Offset: 0x00045948
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[CLSCompliant(false)]
		public static ushort Max(ushort val1, ushort val2)
		{
			if (val1 < val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the larger of two 32-bit signed integers.</summary>
		/// <param name="val1">The first of two 32-bit signed integers to compare. </param>
		/// <param name="val2">The second of two 32-bit signed integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x06001115 RID: 4373 RVA: 0x00047748 File Offset: 0x00045948
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static int Max(int val1, int val2)
		{
			if (val1 < val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the larger of two 32-bit unsigned integers.</summary>
		/// <param name="val1">The first of two 32-bit unsigned integers to compare. </param>
		/// <param name="val2">The second of two 32-bit unsigned integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x06001116 RID: 4374 RVA: 0x00047751 File Offset: 0x00045951
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static uint Max(uint val1, uint val2)
		{
			if (val1 < val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the larger of two 64-bit signed integers.</summary>
		/// <param name="val1">The first of two 64-bit signed integers to compare. </param>
		/// <param name="val2">The second of two 64-bit signed integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x06001117 RID: 4375 RVA: 0x00047748 File Offset: 0x00045948
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static long Max(long val1, long val2)
		{
			if (val1 < val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the larger of two 64-bit unsigned integers.</summary>
		/// <param name="val1">The first of two 64-bit unsigned integers to compare. </param>
		/// <param name="val2">The second of two 64-bit unsigned integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x06001118 RID: 4376 RVA: 0x00047751 File Offset: 0x00045951
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static ulong Max(ulong val1, ulong val2)
		{
			if (val1 < val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the larger of two single-precision floating-point numbers.</summary>
		/// <param name="val1">The first of two single-precision floating-point numbers to compare. </param>
		/// <param name="val2">The second of two single-precision floating-point numbers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger. If <paramref name="val1" />, or <paramref name="val2" />, or both <paramref name="val1" /> and <paramref name="val2" /> are equal to <see cref="F:System.Single.NaN" />, <see cref="F:System.Single.NaN" /> is returned.</returns>
		// Token: 0x06001119 RID: 4377 RVA: 0x0004775A File Offset: 0x0004595A
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static float Max(float val1, float val2)
		{
			if (val1 > val2)
			{
				return val1;
			}
			if (float.IsNaN(val1))
			{
				return val1;
			}
			return val2;
		}

		/// <summary>Returns the larger of two double-precision floating-point numbers.</summary>
		/// <param name="val1">The first of two double-precision floating-point numbers to compare. </param>
		/// <param name="val2">The second of two double-precision floating-point numbers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger. If <paramref name="val1" />, <paramref name="val2" />, or both <paramref name="val1" /> and <paramref name="val2" /> are equal to <see cref="F:System.Double.NaN" />, <see cref="F:System.Double.NaN" /> is returned.</returns>
		// Token: 0x0600111A RID: 4378 RVA: 0x0004776D File Offset: 0x0004596D
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static double Max(double val1, double val2)
		{
			if (val1 > val2)
			{
				return val1;
			}
			if (double.IsNaN(val1))
			{
				return val1;
			}
			return val2;
		}

		/// <summary>Returns the larger of two decimal numbers.</summary>
		/// <param name="val1">The first of two decimal numbers to compare. </param>
		/// <param name="val2">The second of two decimal numbers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is larger.</returns>
		// Token: 0x0600111B RID: 4379 RVA: 0x00047780 File Offset: 0x00045980
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static decimal Max(decimal val1, decimal val2)
		{
			return decimal.Max(val1, val2);
		}

		/// <summary>Returns the smaller of two 8-bit signed integers.</summary>
		/// <param name="val1">The first of two 8-bit signed integers to compare. </param>
		/// <param name="val2">The second of two 8-bit signed integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x0600111C RID: 4380 RVA: 0x00047789 File Offset: 0x00045989
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[CLSCompliant(false)]
		public static sbyte Min(sbyte val1, sbyte val2)
		{
			if (val1 > val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the smaller of two 8-bit unsigned integers.</summary>
		/// <param name="val1">The first of two 8-bit unsigned integers to compare. </param>
		/// <param name="val2">The second of two 8-bit unsigned integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x0600111D RID: 4381 RVA: 0x00047789 File Offset: 0x00045989
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static byte Min(byte val1, byte val2)
		{
			if (val1 > val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the smaller of two 16-bit signed integers.</summary>
		/// <param name="val1">The first of two 16-bit signed integers to compare. </param>
		/// <param name="val2">The second of two 16-bit signed integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x0600111E RID: 4382 RVA: 0x00047789 File Offset: 0x00045989
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static short Min(short val1, short val2)
		{
			if (val1 > val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the smaller of two 16-bit unsigned integers.</summary>
		/// <param name="val1">The first of two 16-bit unsigned integers to compare. </param>
		/// <param name="val2">The second of two 16-bit unsigned integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x0600111F RID: 4383 RVA: 0x00047789 File Offset: 0x00045989
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[CLSCompliant(false)]
		public static ushort Min(ushort val1, ushort val2)
		{
			if (val1 > val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the smaller of two 32-bit signed integers.</summary>
		/// <param name="val1">The first of two 32-bit signed integers to compare. </param>
		/// <param name="val2">The second of two 32-bit signed integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x06001120 RID: 4384 RVA: 0x00047789 File Offset: 0x00045989
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static int Min(int val1, int val2)
		{
			if (val1 > val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the smaller of two 32-bit unsigned integers.</summary>
		/// <param name="val1">The first of two 32-bit unsigned integers to compare. </param>
		/// <param name="val2">The second of two 32-bit unsigned integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x06001121 RID: 4385 RVA: 0x00047792 File Offset: 0x00045992
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[CLSCompliant(false)]
		public static uint Min(uint val1, uint val2)
		{
			if (val1 > val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the smaller of two 64-bit signed integers.</summary>
		/// <param name="val1">The first of two 64-bit signed integers to compare. </param>
		/// <param name="val2">The second of two 64-bit signed integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x06001122 RID: 4386 RVA: 0x00047789 File Offset: 0x00045989
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static long Min(long val1, long val2)
		{
			if (val1 > val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the smaller of two 64-bit unsigned integers.</summary>
		/// <param name="val1">The first of two 64-bit unsigned integers to compare. </param>
		/// <param name="val2">The second of two 64-bit unsigned integers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x06001123 RID: 4387 RVA: 0x00047792 File Offset: 0x00045992
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static ulong Min(ulong val1, ulong val2)
		{
			if (val1 > val2)
			{
				return val2;
			}
			return val1;
		}

		/// <summary>Returns the smaller of two single-precision floating-point numbers.</summary>
		/// <param name="val1">The first of two single-precision floating-point numbers to compare. </param>
		/// <param name="val2">The second of two single-precision floating-point numbers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller. If <paramref name="val1" />, <paramref name="val2" />, or both <paramref name="val1" /> and <paramref name="val2" /> are equal to <see cref="F:System.Single.NaN" />, <see cref="F:System.Single.NaN" /> is returned.</returns>
		// Token: 0x06001124 RID: 4388 RVA: 0x0004779B File Offset: 0x0004599B
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static float Min(float val1, float val2)
		{
			if (val1 < val2)
			{
				return val1;
			}
			if (float.IsNaN(val1))
			{
				return val1;
			}
			return val2;
		}

		/// <summary>Returns the smaller of two double-precision floating-point numbers.</summary>
		/// <param name="val1">The first of two double-precision floating-point numbers to compare. </param>
		/// <param name="val2">The second of two double-precision floating-point numbers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller. If <paramref name="val1" />, <paramref name="val2" />, or both <paramref name="val1" /> and <paramref name="val2" /> are equal to <see cref="F:System.Double.NaN" />, <see cref="F:System.Double.NaN" /> is returned.</returns>
		// Token: 0x06001125 RID: 4389 RVA: 0x000477AE File Offset: 0x000459AE
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static double Min(double val1, double val2)
		{
			if (val1 < val2)
			{
				return val1;
			}
			if (double.IsNaN(val1))
			{
				return val1;
			}
			return val2;
		}

		/// <summary>Returns the smaller of two decimal numbers.</summary>
		/// <param name="val1">The first of two decimal numbers to compare. </param>
		/// <param name="val2">The second of two decimal numbers to compare. </param>
		/// <returns>Parameter <paramref name="val1" /> or <paramref name="val2" />, whichever is smaller.</returns>
		// Token: 0x06001126 RID: 4390 RVA: 0x000477C1 File Offset: 0x000459C1
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static decimal Min(decimal val1, decimal val2)
		{
			return decimal.Min(val1, val2);
		}

		/// <summary>Returns the logarithm of a specified number in a specified base.</summary>
		/// <param name="a">The number whose logarithm is to be found. </param>
		/// <param name="newBase">The base of the logarithm. </param>
		/// <returns>One of the values in the following table. (+Infinity denotes <see cref="F:System.Double.PositiveInfinity" />, -Infinity denotes <see cref="F:System.Double.NegativeInfinity" />, and NaN denotes <see cref="F:System.Double.NaN" />.)
		///             <paramref name="a" />
		///
		///             <paramref name="newBase" />
		///           Return value
		///             <paramref name="a" />
		///             &gt; 0(0 &lt;<paramref name="newBase" />&lt; 1) -or-(<paramref name="newBase" />&gt; 1)lognewBase(a)
		///             <paramref name="a" />
		///             &lt; 0(any value)NaN(any value)
		///             <paramref name="newBase" />
		///             &lt; 0NaN
		///             <paramref name="a" /> != 1
		///             <paramref name="newBase" /> = 0NaN
		///             <paramref name="a" /> != 1
		///             <paramref name="newBase" /> = +InfinityNaN
		///             <paramref name="a" /> = NaN(any value)NaN(any value)
		///             <paramref name="newBase" /> = NaNNaN(any value)
		///             <paramref name="newBase" /> = 1NaN
		///             <paramref name="a" /> = 00 &lt;<paramref name="newBase" />&lt; 1 +Infinity
		///             <paramref name="a" /> = 0
		///             <paramref name="newBase" />
		///             &gt; 1-Infinity
		///             <paramref name="a" /> =  +Infinity0 &lt;<paramref name="newBase" />&lt; 1-Infinity
		///             <paramref name="a" /> =  +Infinity
		///             <paramref name="newBase" />
		///             &gt; 1+Infinity
		///             <paramref name="a" /> = 1
		///             <paramref name="newBase" /> = 00
		///             <paramref name="a" /> = 1
		///             <paramref name="newBase" /> = +Infinity0</returns>
		// Token: 0x06001127 RID: 4391 RVA: 0x000477CC File Offset: 0x000459CC
		public static double Log(double a, double newBase)
		{
			if (double.IsNaN(a))
			{
				return a;
			}
			if (double.IsNaN(newBase))
			{
				return newBase;
			}
			if (newBase == 1.0)
			{
				return double.NaN;
			}
			if (a != 1.0 && (newBase == 0.0 || double.IsPositiveInfinity(newBase)))
			{
				return double.NaN;
			}
			return Math.Log(a) / Math.Log(newBase);
		}

		/// <summary>Returns an integer that indicates the sign of an 8-bit signed integer.</summary>
		/// <param name="value">A signed number. </param>
		/// <returns>A number that indicates the sign of <paramref name="value" />, as shown in the following table.Return value Meaning -1 
		///             <paramref name="value" /> is less than zero. 0 
		///             <paramref name="value" /> is equal to zero. 1 
		///             <paramref name="value" /> is greater than zero. </returns>
		// Token: 0x06001128 RID: 4392 RVA: 0x0004783A File Offset: 0x00045A3A
		[CLSCompliant(false)]
		public static int Sign(sbyte value)
		{
			if (value < 0)
			{
				return -1;
			}
			if (value > 0)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>Returns an integer that indicates the sign of a 16-bit signed integer.</summary>
		/// <param name="value">A signed number. </param>
		/// <returns>A number that indicates the sign of <paramref name="value" />, as shown in the following table.Return value Meaning -1 
		///             <paramref name="value" /> is less than zero. 0 
		///             <paramref name="value" /> is equal to zero. 1 
		///             <paramref name="value" /> is greater than zero. </returns>
		// Token: 0x06001129 RID: 4393 RVA: 0x0004783A File Offset: 0x00045A3A
		public static int Sign(short value)
		{
			if (value < 0)
			{
				return -1;
			}
			if (value > 0)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>Returns an integer that indicates the sign of a 32-bit signed integer.</summary>
		/// <param name="value">A signed number. </param>
		/// <returns>A number that indicates the sign of <paramref name="value" />, as shown in the following table.Return value Meaning -1 
		///             <paramref name="value" /> is less than zero. 0 
		///             <paramref name="value" /> is equal to zero. 1 
		///             <paramref name="value" /> is greater than zero. </returns>
		// Token: 0x0600112A RID: 4394 RVA: 0x0004783A File Offset: 0x00045A3A
		public static int Sign(int value)
		{
			if (value < 0)
			{
				return -1;
			}
			if (value > 0)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>Returns an integer that indicates the sign of a 64-bit signed integer.</summary>
		/// <param name="value">A signed number. </param>
		/// <returns>A number that indicates the sign of <paramref name="value" />, as shown in the following table.Return value Meaning -1 
		///             <paramref name="value" /> is less than zero. 0 
		///             <paramref name="value" /> is equal to zero. 1 
		///             <paramref name="value" /> is greater than zero. </returns>
		// Token: 0x0600112B RID: 4395 RVA: 0x00047849 File Offset: 0x00045A49
		public static int Sign(long value)
		{
			if (value < 0L)
			{
				return -1;
			}
			if (value > 0L)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>Returns an integer that indicates the sign of a single-precision floating-point number.</summary>
		/// <param name="value">A signed number. </param>
		/// <returns>A number that indicates the sign of <paramref name="value" />, as shown in the following table.Return value Meaning -1 
		///             <paramref name="value" /> is less than zero. 0 
		///             <paramref name="value" /> is equal to zero. 1 
		///             <paramref name="value" /> is greater than zero. </returns>
		/// <exception cref="T:System.ArithmeticException">
		///         <paramref name="value" /> is equal to <see cref="F:System.Single.NaN" />. </exception>
		// Token: 0x0600112C RID: 4396 RVA: 0x0004785A File Offset: 0x00045A5A
		public static int Sign(float value)
		{
			if (value < 0f)
			{
				return -1;
			}
			if (value > 0f)
			{
				return 1;
			}
			if (value == 0f)
			{
				return 0;
			}
			throw new ArithmeticException(Environment.GetResourceString("Function does not accept floating point Not-a-Number values."));
		}

		/// <summary>Returns an integer that indicates the sign of a double-precision floating-point number.</summary>
		/// <param name="value">A signed number. </param>
		/// <returns>A number that indicates the sign of <paramref name="value" />, as shown in the following table.Return value Meaning -1 
		///             <paramref name="value" /> is less than zero. 0 
		///             <paramref name="value" /> is equal to zero. 1 
		///             <paramref name="value" /> is greater than zero. </returns>
		/// <exception cref="T:System.ArithmeticException">
		///         <paramref name="value" /> is equal to <see cref="F:System.Double.NaN" />. </exception>
		// Token: 0x0600112D RID: 4397 RVA: 0x00047889 File Offset: 0x00045A89
		public static int Sign(double value)
		{
			if (value < 0.0)
			{
				return -1;
			}
			if (value > 0.0)
			{
				return 1;
			}
			if (value == 0.0)
			{
				return 0;
			}
			throw new ArithmeticException(Environment.GetResourceString("Function does not accept floating point Not-a-Number values."));
		}

		/// <summary>Returns an integer that indicates the sign of a decimal number.</summary>
		/// <param name="value">A signed decimal number. </param>
		/// <returns>A number that indicates the sign of <paramref name="value" />, as shown in the following table.Return value Meaning -1 
		///             <paramref name="value" /> is less than zero. 0 
		///             <paramref name="value" /> is equal to zero. 1 
		///             <paramref name="value" /> is greater than zero. </returns>
		// Token: 0x0600112E RID: 4398 RVA: 0x000478C4 File Offset: 0x00045AC4
		public static int Sign(decimal value)
		{
			if (value < 0m)
			{
				return -1;
			}
			if (value > 0m)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>Produces the full product of two 32-bit numbers.</summary>
		/// <param name="a">The first number to multiply. </param>
		/// <param name="b">The second number to multiply. </param>
		/// <returns>The number containing the product of the specified numbers.</returns>
		// Token: 0x0600112F RID: 4399 RVA: 0x000478E5 File Offset: 0x00045AE5
		public static long BigMul(int a, int b)
		{
			return (long)a * (long)b;
		}

		/// <summary>Calculates the quotient of two 32-bit signed integers and also returns the remainder in an output parameter.</summary>
		/// <param name="a">The dividend. </param>
		/// <param name="b">The divisor. </param>
		/// <param name="result">The remainder. </param>
		/// <returns>The quotient of the specified numbers.</returns>
		/// <exception cref="T:System.DivideByZeroException">
		///         <paramref name="b" /> is zero.</exception>
		// Token: 0x06001130 RID: 4400 RVA: 0x000478EC File Offset: 0x00045AEC
		public static int DivRem(int a, int b, out int result)
		{
			result = a % b;
			return a / b;
		}

		/// <summary>Calculates the quotient of two 64-bit signed integers and also returns the remainder in an output parameter.</summary>
		/// <param name="a">The dividend. </param>
		/// <param name="b">The divisor. </param>
		/// <param name="result">The remainder. </param>
		/// <returns>The quotient of the specified numbers.</returns>
		/// <exception cref="T:System.DivideByZeroException">
		///         <paramref name="b" /> is zero.</exception>
		// Token: 0x06001131 RID: 4401 RVA: 0x000478F6 File Offset: 0x00045AF6
		public static long DivRem(long a, long b, out long result)
		{
			result = a % b;
			return a / b;
		}

		// Token: 0x06001132 RID: 4402 RVA: 0x00047900 File Offset: 0x00045B00
		// Note: this type is marked as 'beforefieldinit'.
		static Math()
		{
		}

		// Token: 0x040009FA RID: 2554
		private static double doubleRoundLimit = 10000000000000000.0;

		// Token: 0x040009FB RID: 2555
		private const int maxRoundingDigits = 15;

		// Token: 0x040009FC RID: 2556
		private static double[] roundPower10Double = new double[]
		{
			1.0,
			10.0,
			100.0,
			1000.0,
			10000.0,
			100000.0,
			1000000.0,
			10000000.0,
			100000000.0,
			1000000000.0,
			10000000000.0,
			100000000000.0,
			1000000000000.0,
			10000000000000.0,
			100000000000000.0,
			1000000000000000.0
		};

		/// <summary>Represents the ratio of the circumference of a circle to its diameter, specified by the constant, π.</summary>
		// Token: 0x040009FD RID: 2557
		public const double PI = 3.141592653589793;

		/// <summary>Represents the natural logarithmic base, specified by the constant, <see langword="e" />.</summary>
		// Token: 0x040009FE RID: 2558
		public const double E = 2.718281828459045;
	}
}
