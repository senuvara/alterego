﻿using System;

namespace System
{
	/// <summary>Specifies constants that define foreground and background colors for the console.</summary>
	// Token: 0x0200012B RID: 299
	[Serializable]
	public enum ConsoleColor
	{
		/// <summary>The color black.</summary>
		// Token: 0x040007A2 RID: 1954
		Black,
		/// <summary>The color dark blue.</summary>
		// Token: 0x040007A3 RID: 1955
		DarkBlue,
		/// <summary>The color dark green.</summary>
		// Token: 0x040007A4 RID: 1956
		DarkGreen,
		/// <summary>The color dark cyan (dark blue-green).</summary>
		// Token: 0x040007A5 RID: 1957
		DarkCyan,
		/// <summary>The color dark red.</summary>
		// Token: 0x040007A6 RID: 1958
		DarkRed,
		/// <summary>The color dark magenta (dark purplish-red).</summary>
		// Token: 0x040007A7 RID: 1959
		DarkMagenta,
		/// <summary>The color dark yellow (ochre).</summary>
		// Token: 0x040007A8 RID: 1960
		DarkYellow,
		/// <summary>The color gray.</summary>
		// Token: 0x040007A9 RID: 1961
		Gray,
		/// <summary>The color dark gray.</summary>
		// Token: 0x040007AA RID: 1962
		DarkGray,
		/// <summary>The color blue.</summary>
		// Token: 0x040007AB RID: 1963
		Blue,
		/// <summary>The color green.</summary>
		// Token: 0x040007AC RID: 1964
		Green,
		/// <summary>The color cyan (blue-green).</summary>
		// Token: 0x040007AD RID: 1965
		Cyan,
		/// <summary>The color red.</summary>
		// Token: 0x040007AE RID: 1966
		Red,
		/// <summary>The color magenta (purplish-red).</summary>
		// Token: 0x040007AF RID: 1967
		Magenta,
		/// <summary>The color yellow.</summary>
		// Token: 0x040007B0 RID: 1968
		Yellow,
		/// <summary>The color white.</summary>
		// Token: 0x040007B1 RID: 1969
		White
	}
}
