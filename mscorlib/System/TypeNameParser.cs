﻿using System;
using System.Reflection;
using System.Threading;

namespace System
{
	// Token: 0x020001EA RID: 490
	internal sealed class TypeNameParser
	{
		// Token: 0x0600176E RID: 5998 RVA: 0x0005BCBC File Offset: 0x00059EBC
		internal static Type GetType(string typeName, Func<AssemblyName, Assembly> assemblyResolver, Func<Assembly, string, bool, Type> typeResolver, bool throwOnError, bool ignoreCase, ref StackCrawlMark stackMark)
		{
			return TypeSpec.Parse(typeName).Resolve(assemblyResolver, typeResolver, throwOnError, ignoreCase);
		}

		// Token: 0x0600176F RID: 5999 RVA: 0x00002050 File Offset: 0x00000250
		public TypeNameParser()
		{
		}
	}
}
