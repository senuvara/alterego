﻿using System;

namespace System
{
	// Token: 0x02000161 RID: 353
	internal struct DTSubString
	{
		// Token: 0x170001EC RID: 492
		internal char this[int relativeIndex]
		{
			get
			{
				return this.s[this.index + relativeIndex];
			}
		}

		// Token: 0x0400095E RID: 2398
		internal string s;

		// Token: 0x0400095F RID: 2399
		internal int index;

		// Token: 0x04000960 RID: 2400
		internal int length;

		// Token: 0x04000961 RID: 2401
		internal DTSubStringType type;

		// Token: 0x04000962 RID: 2402
		internal int value;
	}
}
