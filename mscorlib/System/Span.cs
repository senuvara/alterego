﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x020000BC RID: 188
	[DebuggerTypeProxy(typeof(SpanDebugView<>))]
	[Obsolete("Types with embedded references are not supported in this version of your compiler.", true)]
	[DebuggerDisplay("{DebuggerDisplay,nq}")]
	public readonly ref struct Span<T>
	{
		// Token: 0x06000665 RID: 1637 RVA: 0x00022868 File Offset: 0x00020A68
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Span(T[] array)
		{
			if (array == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
			}
			if (default(T) == null && array.GetType() != typeof(T[]))
			{
				ThrowHelper.ThrowArrayTypeMismatchException_ArrayTypeMustBeExactMatch(typeof(T));
			}
			this._length = array.Length;
			this._pinnable = Unsafe.As<Pinnable<T>>(array);
			this._byteOffset = SpanHelpers.PerTypeValues<T>.ArrayAdjustment;
		}

		// Token: 0x06000666 RID: 1638 RVA: 0x000228D4 File Offset: 0x00020AD4
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Span(T[] array, int start, int length)
		{
			if (array == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.array);
			}
			if (default(T) == null && array.GetType() != typeof(T[]))
			{
				ThrowHelper.ThrowArrayTypeMismatchException_ArrayTypeMustBeExactMatch(typeof(T));
			}
			if (start > array.Length || length > array.Length - start)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			this._length = length;
			this._pinnable = Unsafe.As<Pinnable<T>>(array);
			this._byteOffset = SpanHelpers.PerTypeValues<T>.ArrayAdjustment.Add(start);
		}

		// Token: 0x06000667 RID: 1639 RVA: 0x00022959 File Offset: 0x00020B59
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public unsafe Span(void* pointer, int length)
		{
			if (SpanHelpers.IsReferenceOrContainsReferences<T>())
			{
				ThrowHelper.ThrowArgumentException_InvalidTypeWithPointersNotSupported(typeof(T));
			}
			if (length < 0)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			this._length = length;
			this._pinnable = null;
			this._byteOffset = new IntPtr(pointer);
		}

		// Token: 0x06000668 RID: 1640 RVA: 0x00022998 File Offset: 0x00020B98
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Span<T> DangerousCreate(object obj, ref T objectData, int length)
		{
			Pinnable<T> pinnable = Unsafe.As<Pinnable<T>>(obj);
			IntPtr byteOffset = Unsafe.ByteOffset<T>(ref pinnable.Data, ref objectData);
			return new Span<T>(pinnable, byteOffset, length);
		}

		// Token: 0x06000669 RID: 1641 RVA: 0x000229BF File Offset: 0x00020BBF
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		internal Span(Pinnable<T> pinnable, IntPtr byteOffset, int length)
		{
			this._length = length;
			this._pinnable = pinnable;
			this._byteOffset = byteOffset;
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x0600066A RID: 1642 RVA: 0x000229D6 File Offset: 0x00020BD6
		private string DebuggerDisplay
		{
			get
			{
				return string.Format("{{{0}[{1}]}}", typeof(T).Name, this._length);
			}
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x0600066B RID: 1643 RVA: 0x000229FC File Offset: 0x00020BFC
		public int Length
		{
			get
			{
				return this._length;
			}
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x0600066C RID: 1644 RVA: 0x00022A04 File Offset: 0x00020C04
		public bool IsEmpty
		{
			get
			{
				return this._length == 0;
			}
		}

		// Token: 0x17000125 RID: 293
		public T this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if (index >= this._length)
				{
					ThrowHelper.ThrowIndexOutOfRangeException();
				}
				if (this._pinnable == null)
				{
					return Unsafe.Add<T>(Unsafe.AsRef<T>(this._byteOffset.ToPointer()), index);
				}
				return Unsafe.Add<T>(Unsafe.AddByteOffset<T>(ref this._pinnable.Data, this._byteOffset), index);
			}
		}

		// Token: 0x0600066E RID: 1646 RVA: 0x00022A6C File Offset: 0x00020C6C
		public unsafe void Clear()
		{
			int length = this._length;
			if (length == 0)
			{
				return;
			}
			UIntPtr byteLength = (UIntPtr)((ulong)length * (ulong)((long)Unsafe.SizeOf<T>()));
			if ((Unsafe.SizeOf<T>() & sizeof(IntPtr) - 1) != 0)
			{
				if (this._pinnable == null)
				{
					byte* ptr = (byte*)this._byteOffset.ToPointer();
					SpanHelpers.ClearLessThanPointerSized(ptr, byteLength);
					return;
				}
				SpanHelpers.ClearLessThanPointerSized(Unsafe.As<T, byte>(Unsafe.AddByteOffset<T>(ref this._pinnable.Data, this._byteOffset)), byteLength);
				return;
			}
			else
			{
				if (SpanHelpers.IsReferenceOrContainsReferences<T>())
				{
					UIntPtr pointerSizeLength = (UIntPtr)((ulong)((long)(length * Unsafe.SizeOf<T>() / sizeof(IntPtr))));
					SpanHelpers.ClearPointerSizedWithReferences(Unsafe.As<T, IntPtr>(this.DangerousGetPinnableReference()), pointerSizeLength);
					return;
				}
				SpanHelpers.ClearPointerSizedWithoutReferences(Unsafe.As<T, byte>(this.DangerousGetPinnableReference()), byteLength);
				return;
			}
		}

		// Token: 0x0600066F RID: 1647 RVA: 0x00022B28 File Offset: 0x00020D28
		public unsafe void Fill(T value)
		{
			int length = this._length;
			if (length == 0)
			{
				return;
			}
			if (Unsafe.SizeOf<T>() != 1)
			{
				ref T source = ref this.DangerousGetPinnableReference();
				int i;
				for (i = 0; i < (length & -8); i += 8)
				{
					*Unsafe.Add<T>(ref source, i) = value;
					*Unsafe.Add<T>(ref source, i + 1) = value;
					*Unsafe.Add<T>(ref source, i + 2) = value;
					*Unsafe.Add<T>(ref source, i + 3) = value;
					*Unsafe.Add<T>(ref source, i + 4) = value;
					*Unsafe.Add<T>(ref source, i + 5) = value;
					*Unsafe.Add<T>(ref source, i + 6) = value;
					*Unsafe.Add<T>(ref source, i + 7) = value;
				}
				if (i < (length & -4))
				{
					*Unsafe.Add<T>(ref source, i) = value;
					*Unsafe.Add<T>(ref source, i + 1) = value;
					*Unsafe.Add<T>(ref source, i + 2) = value;
					*Unsafe.Add<T>(ref source, i + 3) = value;
					i += 4;
				}
				while (i < length)
				{
					*Unsafe.Add<T>(ref source, i) = value;
					i++;
				}
				return;
			}
			byte value2 = *Unsafe.As<T, byte>(ref value);
			if (this._pinnable == null)
			{
				Unsafe.InitBlockUnaligned(this._byteOffset.ToPointer(), value2, (uint)length);
				return;
			}
			Unsafe.InitBlockUnaligned(Unsafe.As<T, byte>(Unsafe.AddByteOffset<T>(ref this._pinnable.Data, this._byteOffset)), value2, (uint)length);
		}

		// Token: 0x06000670 RID: 1648 RVA: 0x00022C97 File Offset: 0x00020E97
		public void CopyTo(Span<T> destination)
		{
			if (!this.TryCopyTo(destination))
			{
				ThrowHelper.ThrowArgumentException_DestinationTooShort();
			}
		}

		// Token: 0x06000671 RID: 1649 RVA: 0x00022CA8 File Offset: 0x00020EA8
		public bool TryCopyTo(Span<T> destination)
		{
			int length = this._length;
			int length2 = destination._length;
			if (length == 0)
			{
				return true;
			}
			if (length > length2)
			{
				return false;
			}
			ref T src = ref this.DangerousGetPinnableReference();
			SpanHelpers.CopyTo<T>(destination.DangerousGetPinnableReference(), length2, ref src, length);
			return true;
		}

		// Token: 0x06000672 RID: 1650 RVA: 0x00022CE5 File Offset: 0x00020EE5
		public static bool operator ==(Span<T> left, Span<T> right)
		{
			return left._length == right._length && Unsafe.AreSame<T>(left.DangerousGetPinnableReference(), right.DangerousGetPinnableReference());
		}

		// Token: 0x06000673 RID: 1651 RVA: 0x00022D0A File Offset: 0x00020F0A
		public static bool operator !=(Span<T> left, Span<T> right)
		{
			return !(left == right);
		}

		// Token: 0x06000674 RID: 1652 RVA: 0x0002270B File Offset: 0x0002090B
		[Obsolete("Equals() on Span will always throw an exception. Use == instead.")]
		public override bool Equals(object obj)
		{
			throw new NotSupportedException("Equals() on Span and ReadOnlySpan is not supported. Use operator== instead.");
		}

		// Token: 0x06000675 RID: 1653 RVA: 0x00022717 File Offset: 0x00020917
		[Obsolete("GetHashCode() on Span will always throw an exception.")]
		public override int GetHashCode()
		{
			throw new NotSupportedException("GetHashCode() on Span and ReadOnlySpan is not supported.");
		}

		// Token: 0x06000676 RID: 1654 RVA: 0x00022D16 File Offset: 0x00020F16
		public static implicit operator Span<T>(T[] array)
		{
			return new Span<T>(array);
		}

		// Token: 0x06000677 RID: 1655 RVA: 0x00022D1E File Offset: 0x00020F1E
		public static implicit operator Span<T>(ArraySegment<T> arraySegment)
		{
			return new Span<T>(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
		}

		// Token: 0x06000678 RID: 1656 RVA: 0x00022D3A File Offset: 0x00020F3A
		public static implicit operator ReadOnlySpan<T>(Span<T> span)
		{
			return new ReadOnlySpan<T>(span._pinnable, span._byteOffset, span._length);
		}

		// Token: 0x06000679 RID: 1657 RVA: 0x00022D54 File Offset: 0x00020F54
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Span<T> Slice(int start)
		{
			if (start > this._length)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			IntPtr byteOffset = this._byteOffset.Add(start);
			int length = this._length - start;
			return new Span<T>(this._pinnable, byteOffset, length);
		}

		// Token: 0x0600067A RID: 1658 RVA: 0x00022D94 File Offset: 0x00020F94
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Span<T> Slice(int start, int length)
		{
			if (start > this._length || length > this._length - start)
			{
				ThrowHelper.ThrowArgumentOutOfRangeException(ExceptionArgument.start);
			}
			IntPtr byteOffset = this._byteOffset.Add(start);
			return new Span<T>(this._pinnable, byteOffset, length);
		}

		// Token: 0x0600067B RID: 1659 RVA: 0x00022DD8 File Offset: 0x00020FD8
		public T[] ToArray()
		{
			if (this._length == 0)
			{
				return SpanHelpers.PerTypeValues<T>.EmptyArray;
			}
			T[] array = new T[this._length];
			this.CopyTo(array);
			return array;
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x0600067C RID: 1660 RVA: 0x00022E0C File Offset: 0x0002100C
		public static Span<T> Empty
		{
			get
			{
				return default(Span<T>);
			}
		}

		// Token: 0x0600067D RID: 1661 RVA: 0x00022E24 File Offset: 0x00021024
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ref T DangerousGetPinnableReference()
		{
			if (this._pinnable == null)
			{
				return Unsafe.AsRef<T>(this._byteOffset.ToPointer());
			}
			return Unsafe.AddByteOffset<T>(ref this._pinnable.Data, this._byteOffset);
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x0600067E RID: 1662 RVA: 0x00022E63 File Offset: 0x00021063
		internal Pinnable<T> Pinnable
		{
			get
			{
				return this._pinnable;
			}
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x0600067F RID: 1663 RVA: 0x00022E6B File Offset: 0x0002106B
		internal IntPtr ByteOffset
		{
			get
			{
				return this._byteOffset;
			}
		}

		// Token: 0x0400066B RID: 1643
		private readonly Pinnable<T> _pinnable;

		// Token: 0x0400066C RID: 1644
		private readonly IntPtr _byteOffset;

		// Token: 0x0400066D RID: 1645
		private readonly int _length;
	}
}
