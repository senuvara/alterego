﻿using System;

namespace System
{
	// Token: 0x02000230 RID: 560
	internal class TimeType
	{
		// Token: 0x06001AD3 RID: 6867 RVA: 0x00065CB6 File Offset: 0x00063EB6
		public TimeType(int offset, bool is_dst, string abbrev)
		{
			this.Offset = offset;
			this.IsDst = is_dst;
			this.Name = abbrev;
		}

		// Token: 0x06001AD4 RID: 6868 RVA: 0x00065CD4 File Offset: 0x00063ED4
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"offset: ",
				this.Offset,
				"s, is_dst: ",
				this.IsDst.ToString(),
				", zone name: ",
				this.Name
			});
		}

		// Token: 0x04000F08 RID: 3848
		public readonly int Offset;

		// Token: 0x04000F09 RID: 3849
		public readonly bool IsDst;

		// Token: 0x04000F0A RID: 3850
		public string Name;
	}
}
