﻿using System;

namespace System
{
	// Token: 0x020000F4 RID: 244
	internal sealed class LocalDataStoreHolder
	{
		// Token: 0x06000956 RID: 2390 RVA: 0x00031369 File Offset: 0x0002F569
		public LocalDataStoreHolder(LocalDataStore store)
		{
			this.m_Store = store;
		}

		// Token: 0x06000957 RID: 2391 RVA: 0x00031378 File Offset: 0x0002F578
		protected override void Finalize()
		{
			try
			{
				LocalDataStore store = this.m_Store;
				if (store != null)
				{
					store.Dispose();
				}
			}
			finally
			{
				base.Finalize();
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x06000958 RID: 2392 RVA: 0x000313B0 File Offset: 0x0002F5B0
		public LocalDataStore Store
		{
			get
			{
				return this.m_Store;
			}
		}

		// Token: 0x04000743 RID: 1859
		private LocalDataStore m_Store;
	}
}
