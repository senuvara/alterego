﻿using System;

namespace System
{
	// Token: 0x020001E8 RID: 488
	internal struct RuntimeMethodHandleInternal
	{
		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06001769 RID: 5993 RVA: 0x0005BC88 File Offset: 0x00059E88
		internal static RuntimeMethodHandleInternal EmptyHandle
		{
			get
			{
				return default(RuntimeMethodHandleInternal);
			}
		}

		// Token: 0x0600176A RID: 5994 RVA: 0x0005BC9E File Offset: 0x00059E9E
		internal bool IsNullHandle()
		{
			return this.m_handle.IsNull();
		}

		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x0600176B RID: 5995 RVA: 0x0005BCAB File Offset: 0x00059EAB
		internal IntPtr Value
		{
			get
			{
				return this.m_handle;
			}
		}

		// Token: 0x0600176C RID: 5996 RVA: 0x0005BCB3 File Offset: 0x00059EB3
		internal RuntimeMethodHandleInternal(IntPtr value)
		{
			this.m_handle = value;
		}

		// Token: 0x04000C1A RID: 3098
		internal IntPtr m_handle;
	}
}
