﻿using System;

namespace System
{
	// Token: 0x02000200 RID: 512
	internal static class EmptyArray<T>
	{
		// Token: 0x060018CE RID: 6350 RVA: 0x0005DFDD File Offset: 0x0005C1DD
		// Note: this type is marked as 'beforefieldinit'.
		static EmptyArray()
		{
		}

		// Token: 0x04000C79 RID: 3193
		public static readonly T[] Value = new T[0];
	}
}
