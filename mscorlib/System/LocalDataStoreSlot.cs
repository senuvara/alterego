﻿using System;
using System.Runtime.InteropServices;
using Unity;

namespace System
{
	/// <summary>Encapsulates a memory slot to store local data. This class cannot be inherited.</summary>
	// Token: 0x020000F7 RID: 247
	[ComVisible(true)]
	public sealed class LocalDataStoreSlot
	{
		// Token: 0x06000963 RID: 2403 RVA: 0x000315D0 File Offset: 0x0002F7D0
		internal LocalDataStoreSlot(LocalDataStoreMgr mgr, int slot, long cookie)
		{
			this.m_mgr = mgr;
			this.m_slot = slot;
			this.m_cookie = cookie;
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000964 RID: 2404 RVA: 0x000315ED File Offset: 0x0002F7ED
		internal LocalDataStoreMgr Manager
		{
			get
			{
				return this.m_mgr;
			}
		}

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x06000965 RID: 2405 RVA: 0x000315F5 File Offset: 0x0002F7F5
		internal int Slot
		{
			get
			{
				return this.m_slot;
			}
		}

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000966 RID: 2406 RVA: 0x000315FD File Offset: 0x0002F7FD
		internal long Cookie
		{
			get
			{
				return this.m_cookie;
			}
		}

		/// <summary>Ensures that resources are freed and other cleanup operations are performed when the garbage collector reclaims the <see cref="T:System.LocalDataStoreSlot" /> object. </summary>
		// Token: 0x06000967 RID: 2407 RVA: 0x00031608 File Offset: 0x0002F808
		protected override void Finalize()
		{
			try
			{
				LocalDataStoreMgr mgr = this.m_mgr;
				if (mgr != null)
				{
					int slot = this.m_slot;
					this.m_slot = -1;
					mgr.FreeDataSlot(slot, this.m_cookie);
				}
			}
			finally
			{
				base.Finalize();
			}
		}

		// Token: 0x06000968 RID: 2408 RVA: 0x00002ABD File Offset: 0x00000CBD
		internal LocalDataStoreSlot()
		{
			ThrowStub.ThrowNotSupportedException();
		}

		// Token: 0x04000748 RID: 1864
		private LocalDataStoreMgr m_mgr;

		// Token: 0x04000749 RID: 1865
		private int m_slot;

		// Token: 0x0400074A RID: 1866
		private long m_cookie;
	}
}
