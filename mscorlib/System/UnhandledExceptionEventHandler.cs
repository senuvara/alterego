﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents the method that will handle the event raised by an exception that is not handled by the application domain.</summary>
	/// <param name="sender">The source of the unhandled exception event. </param>
	/// <param name="e">An <paramref name="UnhandledExceptionEventArgs" /> that contains the event data. </param>
	// Token: 0x020001D6 RID: 470
	// (Invoke) Token: 0x06001626 RID: 5670
	[ComVisible(true)]
	[Serializable]
	public delegate void UnhandledExceptionEventHandler(object sender, UnhandledExceptionEventArgs e);
}
