﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Specifies the action that a custom application domain manager takes when initializing a new domain.</summary>
	// Token: 0x020001FF RID: 511
	[Flags]
	[ComVisible(true)]
	public enum AppDomainManagerInitializationOptions
	{
		/// <summary>No initialization action.</summary>
		// Token: 0x04000C77 RID: 3191
		None = 0,
		/// <summary>Register the COM callable wrapper for the current <see cref="T:System.AppDomainManager" /> with the unmanaged host. </summary>
		// Token: 0x04000C78 RID: 3192
		RegisterWithHost = 1
	}
}
