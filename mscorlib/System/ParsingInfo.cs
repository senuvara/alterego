﻿using System;
using System.Globalization;

namespace System
{
	// Token: 0x02000167 RID: 359
	internal struct ParsingInfo
	{
		// Token: 0x06000FBD RID: 4029 RVA: 0x00044F7E File Offset: 0x0004317E
		internal void Init()
		{
			this.dayOfWeek = -1;
			this.timeMark = DateTimeParse.TM.NotSet;
		}

		// Token: 0x04000996 RID: 2454
		internal Calendar calendar;

		// Token: 0x04000997 RID: 2455
		internal int dayOfWeek;

		// Token: 0x04000998 RID: 2456
		internal DateTimeParse.TM timeMark;

		// Token: 0x04000999 RID: 2457
		internal bool fUseHour12;

		// Token: 0x0400099A RID: 2458
		internal bool fUseTwoDigitYear;

		// Token: 0x0400099B RID: 2459
		internal bool fAllowInnerWhite;

		// Token: 0x0400099C RID: 2460
		internal bool fAllowTrailingWhite;

		// Token: 0x0400099D RID: 2461
		internal bool fCustomNumberParser;

		// Token: 0x0400099E RID: 2462
		internal DateTimeParse.MatchNumberDelegate parseNumberDelegate;
	}
}
