﻿using System;
using System.Runtime.InteropServices;

namespace System.Deployment.Internal
{
	/// <summary>Provides access to data from an <see cref="T:System.ActivationContext" /> object.</summary>
	// Token: 0x02000252 RID: 594
	[ComVisible(false)]
	public static class InternalActivationContextHelper
	{
		/// <summary>Gets the contents of the application manifest from an <see cref="T:System.ActivationContext" /> object.</summary>
		/// <param name="appInfo">The object containing the manifest.</param>
		/// <returns>The application manifest that is contained by the <see cref="T:System.ActivationContext" /> object.</returns>
		// Token: 0x06001BA0 RID: 7072 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static object GetActivationContextData(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the manifest of the last deployment component in an <see cref="T:System.ActivationContext" /> object.</summary>
		/// <param name="appInfo">The object containing the manifest.</param>
		/// <returns>The manifest of the last deployment component in the <see cref="T:System.ActivationContext" /> object.</returns>
		// Token: 0x06001BA1 RID: 7073 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static object GetApplicationComponentManifest(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a byte array containing the raw content of the application manifest..</summary>
		/// <param name="appInfo">The object to get bytes from.</param>
		/// <returns>An array containing the application manifest as raw data.</returns>
		// Token: 0x06001BA2 RID: 7074 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO("2.0 SP1 member")]
		public static byte[] GetApplicationManifestBytes(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets the manifest of the first deployment component in an <see cref="T:System.ActivationContext" /> object.</summary>
		/// <param name="appInfo">The object containing the manifest.</param>
		/// <returns>The manifest of the first deployment component in the <see cref="T:System.ActivationContext" /> object.</returns>
		// Token: 0x06001BA3 RID: 7075 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static object GetDeploymentComponentManifest(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a byte array containing the raw content of the deployment manifest.</summary>
		/// <param name="appInfo">The object to get bytes from.</param>
		/// <returns>An array containing the deployment manifest as raw data.</returns>
		// Token: 0x06001BA4 RID: 7076 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO("2.0 SP1 member")]
		public static byte[] GetDeploymentManifestBytes(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a value indicating whether this is the first time this <see cref="T:System.ActivationContext" /> object has been run.</summary>
		/// <param name="appInfo">The object to examine.</param>
		/// <returns>
		///     <see langword="true" /> if the <see cref="T:System.ActivationContext" /> indicates it is running for the first time; otherwise, <see langword="false" />.</returns>
		// Token: 0x06001BA5 RID: 7077 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static bool IsFirstRun(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		/// <summary>Informs an <see cref="T:System.ActivationContext" /> to get ready to be run.</summary>
		/// <param name="appInfo">The object to inform.</param>
		// Token: 0x06001BA6 RID: 7078 RVA: 0x000041F3 File Offset: 0x000023F3
		[MonoTODO]
		public static void PrepareForExecution(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}
	}
}
