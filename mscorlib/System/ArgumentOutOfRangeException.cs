﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;

namespace System
{
	/// <summary>The exception that is thrown when the value of an argument is outside the allowable range of values as defined by the invoked method.</summary>
	// Token: 0x02000117 RID: 279
	[ComVisible(true)]
	[Serializable]
	public class ArgumentOutOfRangeException : ArgumentException, ISerializable
	{
		// Token: 0x1700019D RID: 413
		// (get) Token: 0x06000A12 RID: 2578 RVA: 0x00032433 File Offset: 0x00030633
		private static string RangeMessage
		{
			get
			{
				if (ArgumentOutOfRangeException._rangeMessage == null)
				{
					ArgumentOutOfRangeException._rangeMessage = Environment.GetResourceString("Specified argument was out of the range of valid values.");
				}
				return ArgumentOutOfRangeException._rangeMessage;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentOutOfRangeException" /> class.</summary>
		// Token: 0x06000A13 RID: 2579 RVA: 0x00032456 File Offset: 0x00030656
		public ArgumentOutOfRangeException() : base(ArgumentOutOfRangeException.RangeMessage)
		{
			base.SetErrorCode(-2146233086);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentOutOfRangeException" /> class with the name of the parameter that causes this exception.</summary>
		/// <param name="paramName">The name of the parameter that causes this exception. </param>
		// Token: 0x06000A14 RID: 2580 RVA: 0x0003246E File Offset: 0x0003066E
		public ArgumentOutOfRangeException(string paramName) : base(ArgumentOutOfRangeException.RangeMessage, paramName)
		{
			base.SetErrorCode(-2146233086);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentOutOfRangeException" /> class with the name of the parameter that causes this exception and a specified error message.</summary>
		/// <param name="paramName">The name of the parameter that caused the exception. </param>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06000A15 RID: 2581 RVA: 0x00032487 File Offset: 0x00030687
		public ArgumentOutOfRangeException(string paramName, string message) : base(message, paramName)
		{
			base.SetErrorCode(-2146233086);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentOutOfRangeException" /> class with a specified error message and the exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for this exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception, or a null reference (<see langword="Nothing" /> in Visual Basic) if no inner exception is specified. </param>
		// Token: 0x06000A16 RID: 2582 RVA: 0x0003249C File Offset: 0x0003069C
		public ArgumentOutOfRangeException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233086);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentOutOfRangeException" /> class with the parameter name, the value of the argument, and a specified error message.</summary>
		/// <param name="paramName">The name of the parameter that caused the exception. </param>
		/// <param name="actualValue">The value of the argument that causes this exception. </param>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06000A17 RID: 2583 RVA: 0x000324B1 File Offset: 0x000306B1
		public ArgumentOutOfRangeException(string paramName, object actualValue, string message) : base(message, paramName)
		{
			this.m_actualValue = actualValue;
			base.SetErrorCode(-2146233086);
		}

		/// <summary>Gets the error message and the string representation of the invalid argument value, or only the error message if the argument value is null.</summary>
		/// <returns>The text message for this exception. The value of this property takes one of two forms, as follows.Condition Value The <paramref name="actualValue" /> is <see langword="null" />. The <paramref name="message" /> string passed to the constructor. The <paramref name="actualValue" /> is not <see langword="null" />. The <paramref name="message" /> string appended with the string representation of the invalid argument value. </returns>
		// Token: 0x1700019E RID: 414
		// (get) Token: 0x06000A18 RID: 2584 RVA: 0x000324D0 File Offset: 0x000306D0
		public override string Message
		{
			get
			{
				string message = base.Message;
				if (this.m_actualValue == null)
				{
					return message;
				}
				string resourceString = Environment.GetResourceString("Actual value was {0}.", new object[]
				{
					this.m_actualValue.ToString()
				});
				if (message == null)
				{
					return resourceString;
				}
				return message + Environment.NewLine + resourceString;
			}
		}

		/// <summary>Gets the argument value that causes this exception.</summary>
		/// <returns>An <see langword="Object" /> that contains the value of the parameter that caused the current <see cref="T:System.Exception" />.</returns>
		// Token: 0x1700019F RID: 415
		// (get) Token: 0x06000A19 RID: 2585 RVA: 0x0003251E File Offset: 0x0003071E
		public virtual object ActualValue
		{
			get
			{
				return this.m_actualValue;
			}
		}

		/// <summary>Sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the invalid argument value and additional exception information.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">An object that describes the source or destination of the serialized data. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> object is <see langword="null" />. </exception>
		// Token: 0x06000A1A RID: 2586 RVA: 0x00032526 File Offset: 0x00030726
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
			info.AddValue("ActualValue", this.m_actualValue, typeof(object));
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArgumentOutOfRangeException" /> class with serialized data.</summary>
		/// <param name="info">The object that holds the serialized object data. </param>
		/// <param name="context">An object that describes the source or destination of the serialized data. </param>
		// Token: 0x06000A1B RID: 2587 RVA: 0x00032559 File Offset: 0x00030759
		protected ArgumentOutOfRangeException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.m_actualValue = info.GetValue("ActualValue", typeof(object));
		}

		// Token: 0x04000767 RID: 1895
		private static volatile string _rangeMessage;

		// Token: 0x04000768 RID: 1896
		private object m_actualValue;
	}
}
