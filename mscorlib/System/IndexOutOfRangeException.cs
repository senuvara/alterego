﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	/// <summary>The exception that is thrown when an attempt is made to access an element of an array or collection with an index that is outside its bounds. </summary>
	// Token: 0x0200017A RID: 378
	[ComVisible(true)]
	[Serializable]
	public sealed class IndexOutOfRangeException : SystemException
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.IndexOutOfRangeException" /> class.</summary>
		// Token: 0x06001062 RID: 4194 RVA: 0x00046C7F File Offset: 0x00044E7F
		public IndexOutOfRangeException() : base(Environment.GetResourceString("Index was outside the bounds of the array."))
		{
			base.SetErrorCode(-2146233080);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IndexOutOfRangeException" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		// Token: 0x06001063 RID: 4195 RVA: 0x00046C9C File Offset: 0x00044E9C
		public IndexOutOfRangeException(string message) : base(message)
		{
			base.SetErrorCode(-2146233080);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.IndexOutOfRangeException" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception. If the <paramref name="innerException" /> parameter is not a null reference (<see langword="Nothing" /> in Visual Basic), the current exception is raised in a <see langword="catch" /> block that handles the inner exception. </param>
		// Token: 0x06001064 RID: 4196 RVA: 0x00046CB0 File Offset: 0x00044EB0
		public IndexOutOfRangeException(string message, Exception innerException) : base(message, innerException)
		{
			base.SetErrorCode(-2146233080);
		}

		// Token: 0x06001065 RID: 4197 RVA: 0x000319C9 File Offset: 0x0002FBC9
		internal IndexOutOfRangeException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
