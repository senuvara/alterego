﻿using System;

namespace System
{
	// Token: 0x020000E5 RID: 229
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoLimitationAttribute : MonoTODOAttribute
	{
		// Token: 0x0600090D RID: 2317 RVA: 0x0003053C File Offset: 0x0002E73C
		public MonoLimitationAttribute(string comment) : base(comment)
		{
		}
	}
}
