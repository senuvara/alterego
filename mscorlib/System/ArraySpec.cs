﻿using System;
using System.Text;

namespace System
{
	// Token: 0x0200023C RID: 572
	internal class ArraySpec : ModifierSpec
	{
		// Token: 0x06001AF9 RID: 6905 RVA: 0x00065EA4 File Offset: 0x000640A4
		internal ArraySpec(int dimensions, bool bound)
		{
			this.dimensions = dimensions;
			this.bound = bound;
		}

		// Token: 0x06001AFA RID: 6906 RVA: 0x00065EBA File Offset: 0x000640BA
		public Type Resolve(Type type)
		{
			if (this.bound)
			{
				return type.MakeArrayType(1);
			}
			if (this.dimensions == 1)
			{
				return type.MakeArrayType();
			}
			return type.MakeArrayType(this.dimensions);
		}

		// Token: 0x06001AFB RID: 6907 RVA: 0x00065EE8 File Offset: 0x000640E8
		public StringBuilder Append(StringBuilder sb)
		{
			if (this.bound)
			{
				return sb.Append("[*]");
			}
			return sb.Append('[').Append(',', this.dimensions - 1).Append(']');
		}

		// Token: 0x06001AFC RID: 6908 RVA: 0x00065F1C File Offset: 0x0006411C
		public override string ToString()
		{
			return this.Append(new StringBuilder()).ToString();
		}

		// Token: 0x170003A4 RID: 932
		// (get) Token: 0x06001AFD RID: 6909 RVA: 0x00065F2E File Offset: 0x0006412E
		public int Rank
		{
			get
			{
				return this.dimensions;
			}
		}

		// Token: 0x170003A5 RID: 933
		// (get) Token: 0x06001AFE RID: 6910 RVA: 0x00065F36 File Offset: 0x00064136
		public bool IsBound
		{
			get
			{
				return this.bound;
			}
		}

		// Token: 0x04000F24 RID: 3876
		private int dimensions;

		// Token: 0x04000F25 RID: 3877
		private bool bound;
	}
}
