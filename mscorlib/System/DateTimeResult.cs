﻿using System;
using System.Globalization;

namespace System
{
	// Token: 0x02000166 RID: 358
	internal struct DateTimeResult
	{
		// Token: 0x06000FB9 RID: 4025 RVA: 0x00044F04 File Offset: 0x00043104
		internal void Init()
		{
			this.Year = -1;
			this.Month = -1;
			this.Day = -1;
			this.fraction = -1.0;
			this.era = -1;
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x00044F31 File Offset: 0x00043131
		internal void SetDate(int year, int month, int day)
		{
			this.Year = year;
			this.Month = month;
			this.Day = day;
		}

		// Token: 0x06000FBB RID: 4027 RVA: 0x00044F48 File Offset: 0x00043148
		internal void SetFailure(ParseFailureKind failure, string failureMessageID, object failureMessageFormatArgument)
		{
			this.failure = failure;
			this.failureMessageID = failureMessageID;
			this.failureMessageFormatArgument = failureMessageFormatArgument;
		}

		// Token: 0x06000FBC RID: 4028 RVA: 0x00044F5F File Offset: 0x0004315F
		internal void SetFailure(ParseFailureKind failure, string failureMessageID, object failureMessageFormatArgument, string failureArgumentName)
		{
			this.failure = failure;
			this.failureMessageID = failureMessageID;
			this.failureMessageFormatArgument = failureMessageFormatArgument;
			this.failureArgumentName = failureArgumentName;
		}

		// Token: 0x04000986 RID: 2438
		internal int Year;

		// Token: 0x04000987 RID: 2439
		internal int Month;

		// Token: 0x04000988 RID: 2440
		internal int Day;

		// Token: 0x04000989 RID: 2441
		internal int Hour;

		// Token: 0x0400098A RID: 2442
		internal int Minute;

		// Token: 0x0400098B RID: 2443
		internal int Second;

		// Token: 0x0400098C RID: 2444
		internal double fraction;

		// Token: 0x0400098D RID: 2445
		internal int era;

		// Token: 0x0400098E RID: 2446
		internal ParseFlags flags;

		// Token: 0x0400098F RID: 2447
		internal TimeSpan timeZoneOffset;

		// Token: 0x04000990 RID: 2448
		internal Calendar calendar;

		// Token: 0x04000991 RID: 2449
		internal DateTime parsedDate;

		// Token: 0x04000992 RID: 2450
		internal ParseFailureKind failure;

		// Token: 0x04000993 RID: 2451
		internal string failureMessageID;

		// Token: 0x04000994 RID: 2452
		internal object failureMessageFormatArgument;

		// Token: 0x04000995 RID: 2453
		internal string failureArgumentName;
	}
}
