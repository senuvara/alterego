﻿using System;

namespace System
{
	// Token: 0x02000213 RID: 531
	internal sealed class MonoListItem
	{
		// Token: 0x0600194A RID: 6474 RVA: 0x00002050 File Offset: 0x00000250
		public MonoListItem()
		{
		}

		// Token: 0x04000C9A RID: 3226
		private MonoListItem next;

		// Token: 0x04000C9B RID: 3227
		private object data;
	}
}
