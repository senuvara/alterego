﻿using System;

namespace System
{
	// Token: 0x020000F5 RID: 245
	internal sealed class LocalDataStoreElement
	{
		// Token: 0x06000959 RID: 2393 RVA: 0x000313B8 File Offset: 0x0002F5B8
		public LocalDataStoreElement(long cookie)
		{
			this.m_cookie = cookie;
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x0600095A RID: 2394 RVA: 0x000313C7 File Offset: 0x0002F5C7
		// (set) Token: 0x0600095B RID: 2395 RVA: 0x000313CF File Offset: 0x0002F5CF
		public object Value
		{
			get
			{
				return this.m_value;
			}
			set
			{
				this.m_value = value;
			}
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x0600095C RID: 2396 RVA: 0x000313D8 File Offset: 0x0002F5D8
		public long Cookie
		{
			get
			{
				return this.m_cookie;
			}
		}

		// Token: 0x04000744 RID: 1860
		private object m_value;

		// Token: 0x04000745 RID: 1861
		private long m_cookie;
	}
}
