﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Indicates that a method will allow a variable number of arguments in its invocation. This class cannot be inherited.</summary>
	// Token: 0x0200019C RID: 412
	[AttributeUsage(AttributeTargets.Parameter, Inherited = true, AllowMultiple = false)]
	[ComVisible(true)]
	public sealed class ParamArrayAttribute : Attribute
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ParamArrayAttribute" /> class with default properties.</summary>
		// Token: 0x060011B9 RID: 4537 RVA: 0x000020BF File Offset: 0x000002BF
		public ParamArrayAttribute()
		{
		}
	}
}
