﻿using System;

namespace System
{
	// Token: 0x02000249 RID: 585
	internal struct ConsoleCursorInfo
	{
		// Token: 0x04000F51 RID: 3921
		public int Size;

		// Token: 0x04000F52 RID: 3922
		public bool Visible;
	}
}
