﻿using System;

namespace System
{
	// Token: 0x020001E2 RID: 482
	internal static class CompatibilitySwitches
	{
		// Token: 0x06001716 RID: 5910 RVA: 0x000020D3 File Offset: 0x000002D3
		// Note: this type is marked as 'beforefieldinit'.
		static CompatibilitySwitches()
		{
		}

		// Token: 0x04000BD8 RID: 3032
		public static readonly bool IsAppEarlierThanSilverlight4;

		// Token: 0x04000BD9 RID: 3033
		public static readonly bool IsAppEarlierThanWindowsPhone8;
	}
}
