﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>References a method to be called when a corresponding asynchronous operation completes.</summary>
	/// <param name="ar">The result of the asynchronous operation. </param>
	// Token: 0x0200011C RID: 284
	// (Invoke) Token: 0x06000A43 RID: 2627
	[ComVisible(true)]
	[Serializable]
	public delegate void AsyncCallback(IAsyncResult ar);
}
