﻿using System;
using System.Runtime.CompilerServices;

namespace System.Buffers.Binary
{
	// Token: 0x0200098E RID: 2446
	public static class BinaryPrimitives
	{
		// Token: 0x06005758 RID: 22360 RVA: 0x00002058 File Offset: 0x00000258
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static sbyte ReverseEndianness(sbyte value)
		{
			return value;
		}

		// Token: 0x06005759 RID: 22361 RVA: 0x0011C3A6 File Offset: 0x0011A5A6
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static short ReverseEndianness(short value)
		{
			return (short)((int)(value & 255) << 8 | ((int)value & 65280) >> 8);
		}

		// Token: 0x0600575A RID: 22362 RVA: 0x0011C3BC File Offset: 0x0011A5BC
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int ReverseEndianness(int value)
		{
			return (int)BinaryPrimitives.ReverseEndianness((uint)value);
		}

		// Token: 0x0600575B RID: 22363 RVA: 0x0011C3C4 File Offset: 0x0011A5C4
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long ReverseEndianness(long value)
		{
			return (long)BinaryPrimitives.ReverseEndianness((ulong)value);
		}

		// Token: 0x0600575C RID: 22364 RVA: 0x00002058 File Offset: 0x00000258
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static byte ReverseEndianness(byte value)
		{
			return value;
		}

		// Token: 0x0600575D RID: 22365 RVA: 0x0011C3CC File Offset: 0x0011A5CC
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ushort ReverseEndianness(ushort value)
		{
			return (ushort)((int)(value & 255) << 8 | (int)((uint)(value & 65280) >> 8));
		}

		// Token: 0x0600575E RID: 22366 RVA: 0x0011C3E2 File Offset: 0x0011A5E2
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint ReverseEndianness(uint value)
		{
			value = (value << 16 | value >> 16);
			value = ((value & 16711935U) << 8 | (value & 4278255360U) >> 8);
			return value;
		}

		// Token: 0x0600575F RID: 22367 RVA: 0x0011C408 File Offset: 0x0011A608
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong ReverseEndianness(ulong value)
		{
			value = (value << 32 | value >> 32);
			value = ((value & 281470681808895UL) << 16 | (value & 18446462603027742720UL) >> 16);
			value = ((value & 71777214294589695UL) << 8 | (value & 18374966859414961920UL) >> 8);
			return value;
		}

		// Token: 0x06005760 RID: 22368 RVA: 0x0011C460 File Offset: 0x0011A660
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T ReadMachineEndian<T>(ReadOnlySpan<byte> buffer) where T : struct
		{
			if (RuntimeHelpers.IsReferenceOrContainsReferences<T>())
			{
				throw new ArgumentException(SR.Format("Cannot use type '{0}'. Only value types without pointers or references are supported.", typeof(T)));
			}
			if (Unsafe.SizeOf<T>() > buffer.Length)
			{
				throw new ArgumentOutOfRangeException();
			}
			return Unsafe.ReadUnaligned<T>(buffer.DangerousGetPinnableReference());
		}

		// Token: 0x06005761 RID: 22369 RVA: 0x0011C4B0 File Offset: 0x0011A6B0
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadMachineEndian<T>(ReadOnlySpan<byte> buffer, out T value) where T : struct
		{
			if (RuntimeHelpers.IsReferenceOrContainsReferences<T>())
			{
				throw new ArgumentException(SR.Format("Cannot use type '{0}'. Only value types without pointers or references are supported.", typeof(T)));
			}
			if ((long)Unsafe.SizeOf<T>() > (long)((ulong)buffer.Length))
			{
				value = default(T);
				return false;
			}
			value = Unsafe.ReadUnaligned<T>(buffer.DangerousGetPinnableReference());
			return true;
		}

		// Token: 0x06005762 RID: 22370 RVA: 0x0011C50C File Offset: 0x0011A70C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static short ReadInt16BigEndian(ReadOnlySpan<byte> buffer)
		{
			short num = BinaryPrimitives.ReadMachineEndian<short>(buffer);
			if (BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005763 RID: 22371 RVA: 0x0011C530 File Offset: 0x0011A730
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int ReadInt32BigEndian(ReadOnlySpan<byte> buffer)
		{
			int num = BinaryPrimitives.ReadMachineEndian<int>(buffer);
			if (BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005764 RID: 22372 RVA: 0x0011C554 File Offset: 0x0011A754
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long ReadInt64BigEndian(ReadOnlySpan<byte> buffer)
		{
			long num = BinaryPrimitives.ReadMachineEndian<long>(buffer);
			if (BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005765 RID: 22373 RVA: 0x0011C578 File Offset: 0x0011A778
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ushort ReadUInt16BigEndian(ReadOnlySpan<byte> buffer)
		{
			ushort num = BinaryPrimitives.ReadMachineEndian<ushort>(buffer);
			if (BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005766 RID: 22374 RVA: 0x0011C59C File Offset: 0x0011A79C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint ReadUInt32BigEndian(ReadOnlySpan<byte> buffer)
		{
			uint num = BinaryPrimitives.ReadMachineEndian<uint>(buffer);
			if (BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005767 RID: 22375 RVA: 0x0011C5C0 File Offset: 0x0011A7C0
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong ReadUInt64BigEndian(ReadOnlySpan<byte> buffer)
		{
			ulong num = BinaryPrimitives.ReadMachineEndian<ulong>(buffer);
			if (BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005768 RID: 22376 RVA: 0x0011C5E3 File Offset: 0x0011A7E3
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadInt16BigEndian(ReadOnlySpan<byte> buffer, out short value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<short>(buffer, out value);
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x06005769 RID: 22377 RVA: 0x0011C5FC File Offset: 0x0011A7FC
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadInt32BigEndian(ReadOnlySpan<byte> buffer, out int value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<int>(buffer, out value);
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x0600576A RID: 22378 RVA: 0x0011C615 File Offset: 0x0011A815
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadInt64BigEndian(ReadOnlySpan<byte> buffer, out long value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<long>(buffer, out value);
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x0600576B RID: 22379 RVA: 0x0011C62E File Offset: 0x0011A82E
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadUInt16BigEndian(ReadOnlySpan<byte> buffer, out ushort value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<ushort>(buffer, out value);
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x0600576C RID: 22380 RVA: 0x0011C647 File Offset: 0x0011A847
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadUInt32BigEndian(ReadOnlySpan<byte> buffer, out uint value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<uint>(buffer, out value);
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x0600576D RID: 22381 RVA: 0x0011C660 File Offset: 0x0011A860
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadUInt64BigEndian(ReadOnlySpan<byte> buffer, out ulong value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<ulong>(buffer, out value);
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x0600576E RID: 22382 RVA: 0x0011C67C File Offset: 0x0011A87C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static short ReadInt16LittleEndian(ReadOnlySpan<byte> buffer)
		{
			short num = BinaryPrimitives.ReadMachineEndian<short>(buffer);
			if (!BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x0600576F RID: 22383 RVA: 0x0011C6A0 File Offset: 0x0011A8A0
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int ReadInt32LittleEndian(ReadOnlySpan<byte> buffer)
		{
			int num = BinaryPrimitives.ReadMachineEndian<int>(buffer);
			if (!BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005770 RID: 22384 RVA: 0x0011C6C4 File Offset: 0x0011A8C4
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long ReadInt64LittleEndian(ReadOnlySpan<byte> buffer)
		{
			long num = BinaryPrimitives.ReadMachineEndian<long>(buffer);
			if (!BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005771 RID: 22385 RVA: 0x0011C6E8 File Offset: 0x0011A8E8
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ushort ReadUInt16LittleEndian(ReadOnlySpan<byte> buffer)
		{
			ushort num = BinaryPrimitives.ReadMachineEndian<ushort>(buffer);
			if (!BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005772 RID: 22386 RVA: 0x0011C70C File Offset: 0x0011A90C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint ReadUInt32LittleEndian(ReadOnlySpan<byte> buffer)
		{
			uint num = BinaryPrimitives.ReadMachineEndian<uint>(buffer);
			if (!BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005773 RID: 22387 RVA: 0x0011C730 File Offset: 0x0011A930
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong ReadUInt64LittleEndian(ReadOnlySpan<byte> buffer)
		{
			ulong num = BinaryPrimitives.ReadMachineEndian<ulong>(buffer);
			if (!BitConverter.IsLittleEndian)
			{
				num = BinaryPrimitives.ReverseEndianness(num);
			}
			return num;
		}

		// Token: 0x06005774 RID: 22388 RVA: 0x0011C753 File Offset: 0x0011A953
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadInt16LittleEndian(ReadOnlySpan<byte> buffer, out short value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<short>(buffer, out value);
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x06005775 RID: 22389 RVA: 0x0011C76C File Offset: 0x0011A96C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadInt32LittleEndian(ReadOnlySpan<byte> buffer, out int value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<int>(buffer, out value);
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x06005776 RID: 22390 RVA: 0x0011C785 File Offset: 0x0011A985
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadInt64LittleEndian(ReadOnlySpan<byte> buffer, out long value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<long>(buffer, out value);
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x06005777 RID: 22391 RVA: 0x0011C79E File Offset: 0x0011A99E
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadUInt16LittleEndian(ReadOnlySpan<byte> buffer, out ushort value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<ushort>(buffer, out value);
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x06005778 RID: 22392 RVA: 0x0011C7B7 File Offset: 0x0011A9B7
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadUInt32LittleEndian(ReadOnlySpan<byte> buffer, out uint value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<uint>(buffer, out value);
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x06005779 RID: 22393 RVA: 0x0011C7D0 File Offset: 0x0011A9D0
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadUInt64LittleEndian(ReadOnlySpan<byte> buffer, out ulong value)
		{
			bool result = BinaryPrimitives.TryReadMachineEndian<ulong>(buffer, out value);
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return result;
		}

		// Token: 0x0600577A RID: 22394 RVA: 0x0011C7EC File Offset: 0x0011A9EC
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteMachineEndian<T>(Span<byte> buffer, ref T value) where T : struct
		{
			if (RuntimeHelpers.IsReferenceOrContainsReferences<T>())
			{
				throw new ArgumentException(SR.Format("Cannot use type '{0}'. Only value types without pointers or references are supported.", typeof(T)));
			}
			if (Unsafe.SizeOf<T>() > buffer.Length)
			{
				throw new ArgumentOutOfRangeException();
			}
			Unsafe.WriteUnaligned<T>(buffer.DangerousGetPinnableReference(), value);
		}

		// Token: 0x0600577B RID: 22395 RVA: 0x0011C840 File Offset: 0x0011AA40
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteMachineEndian<T>(Span<byte> buffer, ref T value) where T : struct
		{
			if (RuntimeHelpers.IsReferenceOrContainsReferences<T>())
			{
				throw new ArgumentException(SR.Format("Cannot use type '{0}'. Only value types without pointers or references are supported.", typeof(T)));
			}
			if ((long)Unsafe.SizeOf<T>() > (long)((ulong)buffer.Length))
			{
				return false;
			}
			Unsafe.WriteUnaligned<T>(buffer.DangerousGetPinnableReference(), value);
			return true;
		}

		// Token: 0x0600577C RID: 22396 RVA: 0x0011C893 File Offset: 0x0011AA93
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteInt16BigEndian(Span<byte> buffer, short value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<short>(buffer, ref value);
		}

		// Token: 0x0600577D RID: 22397 RVA: 0x0011C8AC File Offset: 0x0011AAAC
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteInt32BigEndian(Span<byte> buffer, int value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<int>(buffer, ref value);
		}

		// Token: 0x0600577E RID: 22398 RVA: 0x0011C8C5 File Offset: 0x0011AAC5
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteInt64BigEndian(Span<byte> buffer, long value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<long>(buffer, ref value);
		}

		// Token: 0x0600577F RID: 22399 RVA: 0x0011C8DE File Offset: 0x0011AADE
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteUInt16BigEndian(Span<byte> buffer, ushort value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<ushort>(buffer, ref value);
		}

		// Token: 0x06005780 RID: 22400 RVA: 0x0011C8F7 File Offset: 0x0011AAF7
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteUInt32BigEndian(Span<byte> buffer, uint value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<uint>(buffer, ref value);
		}

		// Token: 0x06005781 RID: 22401 RVA: 0x0011C910 File Offset: 0x0011AB10
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteUInt64BigEndian(Span<byte> buffer, ulong value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<ulong>(buffer, ref value);
		}

		// Token: 0x06005782 RID: 22402 RVA: 0x0011C929 File Offset: 0x0011AB29
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteInt16BigEndian(Span<byte> buffer, short value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<short>(buffer, ref value);
		}

		// Token: 0x06005783 RID: 22403 RVA: 0x0011C942 File Offset: 0x0011AB42
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteInt32BigEndian(Span<byte> buffer, int value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<int>(buffer, ref value);
		}

		// Token: 0x06005784 RID: 22404 RVA: 0x0011C95B File Offset: 0x0011AB5B
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteInt64BigEndian(Span<byte> buffer, long value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<long>(buffer, ref value);
		}

		// Token: 0x06005785 RID: 22405 RVA: 0x0011C974 File Offset: 0x0011AB74
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteUInt16BigEndian(Span<byte> buffer, ushort value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<ushort>(buffer, ref value);
		}

		// Token: 0x06005786 RID: 22406 RVA: 0x0011C98D File Offset: 0x0011AB8D
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteUInt32BigEndian(Span<byte> buffer, uint value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<uint>(buffer, ref value);
		}

		// Token: 0x06005787 RID: 22407 RVA: 0x0011C9A6 File Offset: 0x0011ABA6
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteUInt64BigEndian(Span<byte> buffer, ulong value)
		{
			if (BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<ulong>(buffer, ref value);
		}

		// Token: 0x06005788 RID: 22408 RVA: 0x0011C9BF File Offset: 0x0011ABBF
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteInt16LittleEndian(Span<byte> buffer, short value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<short>(buffer, ref value);
		}

		// Token: 0x06005789 RID: 22409 RVA: 0x0011C9D8 File Offset: 0x0011ABD8
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteInt32LittleEndian(Span<byte> buffer, int value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<int>(buffer, ref value);
		}

		// Token: 0x0600578A RID: 22410 RVA: 0x0011C9F1 File Offset: 0x0011ABF1
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteInt64LittleEndian(Span<byte> buffer, long value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<long>(buffer, ref value);
		}

		// Token: 0x0600578B RID: 22411 RVA: 0x0011CA0A File Offset: 0x0011AC0A
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteUInt16LittleEndian(Span<byte> buffer, ushort value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<ushort>(buffer, ref value);
		}

		// Token: 0x0600578C RID: 22412 RVA: 0x0011CA23 File Offset: 0x0011AC23
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteUInt32LittleEndian(Span<byte> buffer, uint value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<uint>(buffer, ref value);
		}

		// Token: 0x0600578D RID: 22413 RVA: 0x0011CA3C File Offset: 0x0011AC3C
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WriteUInt64LittleEndian(Span<byte> buffer, ulong value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			BinaryPrimitives.WriteMachineEndian<ulong>(buffer, ref value);
		}

		// Token: 0x0600578E RID: 22414 RVA: 0x0011CA55 File Offset: 0x0011AC55
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteInt16LittleEndian(Span<byte> buffer, short value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<short>(buffer, ref value);
		}

		// Token: 0x0600578F RID: 22415 RVA: 0x0011CA6E File Offset: 0x0011AC6E
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteInt32LittleEndian(Span<byte> buffer, int value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<int>(buffer, ref value);
		}

		// Token: 0x06005790 RID: 22416 RVA: 0x0011CA87 File Offset: 0x0011AC87
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteInt64LittleEndian(Span<byte> buffer, long value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<long>(buffer, ref value);
		}

		// Token: 0x06005791 RID: 22417 RVA: 0x0011CAA0 File Offset: 0x0011ACA0
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteUInt16LittleEndian(Span<byte> buffer, ushort value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<ushort>(buffer, ref value);
		}

		// Token: 0x06005792 RID: 22418 RVA: 0x0011CAB9 File Offset: 0x0011ACB9
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteUInt32LittleEndian(Span<byte> buffer, uint value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<uint>(buffer, ref value);
		}

		// Token: 0x06005793 RID: 22419 RVA: 0x0011CAD2 File Offset: 0x0011ACD2
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryWriteUInt64LittleEndian(Span<byte> buffer, ulong value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				value = BinaryPrimitives.ReverseEndianness(value);
			}
			return BinaryPrimitives.TryWriteMachineEndian<ulong>(buffer, ref value);
		}
	}
}
