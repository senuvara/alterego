﻿using System;

namespace System.Buffers
{
	// Token: 0x0200098D RID: 2445
	public abstract class OwnedMemory<T> : IDisposable, IRetainable
	{
		// Token: 0x17000F52 RID: 3922
		// (get) Token: 0x0600574C RID: 22348
		public abstract int Length { get; }

		// Token: 0x17000F53 RID: 3923
		// (get) Token: 0x0600574D RID: 22349
		public abstract Span<T> Span { get; }

		// Token: 0x17000F54 RID: 3924
		// (get) Token: 0x0600574E RID: 22350 RVA: 0x0011C369 File Offset: 0x0011A569
		public Memory<T> Memory
		{
			get
			{
				if (this.IsDisposed)
				{
					ThrowHelper.ThrowObjectDisposedException_MemoryDisposed("OwnedMemory");
				}
				return new Memory<T>(this, 0, this.Length);
			}
		}

		// Token: 0x0600574F RID: 22351
		public abstract MemoryHandle Pin();

		// Token: 0x06005750 RID: 22352
		protected internal abstract bool TryGetArray(out ArraySegment<T> arraySegment);

		// Token: 0x06005751 RID: 22353 RVA: 0x0011C38A File Offset: 0x0011A58A
		public void Dispose()
		{
			if (this.IsRetained)
			{
				ThrowHelper.ThrowInvalidOperationException_OutstandingReferences();
			}
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06005752 RID: 22354
		protected abstract void Dispose(bool disposing);

		// Token: 0x17000F55 RID: 3925
		// (get) Token: 0x06005753 RID: 22355
		protected abstract bool IsRetained { get; }

		// Token: 0x17000F56 RID: 3926
		// (get) Token: 0x06005754 RID: 22356
		public abstract bool IsDisposed { get; }

		// Token: 0x06005755 RID: 22357
		public abstract void Retain();

		// Token: 0x06005756 RID: 22358
		public abstract bool Release();

		// Token: 0x06005757 RID: 22359 RVA: 0x00002050 File Offset: 0x00000250
		protected OwnedMemory()
		{
		}
	}
}
