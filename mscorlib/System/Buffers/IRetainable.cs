﻿using System;

namespace System.Buffers
{
	// Token: 0x0200098B RID: 2443
	public interface IRetainable
	{
		// Token: 0x06005746 RID: 22342
		void Retain();

		// Token: 0x06005747 RID: 22343
		bool Release();
	}
}
