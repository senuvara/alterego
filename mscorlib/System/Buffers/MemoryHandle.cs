﻿using System;
using System.Runtime.InteropServices;

namespace System.Buffers
{
	// Token: 0x0200098C RID: 2444
	public struct MemoryHandle : IDisposable
	{
		// Token: 0x06005748 RID: 22344 RVA: 0x0011C2EB File Offset: 0x0011A4EB
		public unsafe MemoryHandle(IRetainable retainable, void* pinnedPointer = null, GCHandle handle = default(GCHandle))
		{
			this._retainable = retainable;
			this._pointer = pinnedPointer;
			this._handle = handle;
		}

		// Token: 0x17000F51 RID: 3921
		// (get) Token: 0x06005749 RID: 22345 RVA: 0x0011C302 File Offset: 0x0011A502
		public unsafe void* PinnedPointer
		{
			get
			{
				return this._pointer;
			}
		}

		// Token: 0x0600574A RID: 22346 RVA: 0x0011C30A File Offset: 0x0011A50A
		internal unsafe void AddOffset(int offset)
		{
			if (this._pointer == null)
			{
				ThrowHelper.ThrowArgumentNullException(ExceptionArgument.pointer);
				return;
			}
			this._pointer = (void*)((byte*)this._pointer + offset);
		}

		// Token: 0x0600574B RID: 22347 RVA: 0x0011C32C File Offset: 0x0011A52C
		public void Dispose()
		{
			if (this._handle.IsAllocated)
			{
				this._handle.Free();
			}
			if (this._retainable != null)
			{
				this._retainable.Release();
				this._retainable = null;
			}
			this._pointer = null;
		}

		// Token: 0x04002D1A RID: 11546
		private IRetainable _retainable;

		// Token: 0x04002D1B RID: 11547
		private unsafe void* _pointer;

		// Token: 0x04002D1C RID: 11548
		private GCHandle _handle;
	}
}
