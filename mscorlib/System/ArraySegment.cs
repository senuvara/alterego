﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System
{
	/// <summary>Delimits a section of a one-dimensional array.</summary>
	/// <typeparam name="T">The type of the elements in the array segment.</typeparam>
	// Token: 0x02000119 RID: 281
	[Serializable]
	public struct ArraySegment<T> : IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable, IReadOnlyList<T>, IReadOnlyCollection<T>
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.ArraySegment`1" /> structure that delimits all the elements in the specified array.</summary>
		/// <param name="array">The array to wrap.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		// Token: 0x06000A20 RID: 2592 RVA: 0x000325C4 File Offset: 0x000307C4
		public ArraySegment(T[] array)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			this._array = array;
			this._offset = 0;
			this._count = array.Length;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.ArraySegment`1" /> structure that delimits the specified range of the elements in the specified array.</summary>
		/// <param name="array">The array containing the range of elements to delimit.</param>
		/// <param name="offset">The zero-based index of the first element in the range.</param>
		/// <param name="count">The number of elements in the range.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="offset" /> or <paramref name="count" /> is less than 0.</exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="offset" /> and <paramref name="count" /> do not specify a valid range in <paramref name="array" />.</exception>
		// Token: 0x06000A21 RID: 2593 RVA: 0x000325EC File Offset: 0x000307EC
		public ArraySegment(T[] array, int offset, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Environment.GetResourceString("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Environment.GetResourceString("Non-negative number required."));
			}
			if (array.Length - offset < count)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			this._array = array;
			this._offset = offset;
			this._count = count;
		}

		/// <summary>Gets the original array containing the range of elements that the array segment delimits.</summary>
		/// <returns>The original array that was passed to the constructor, and that contains the range delimited by the <see cref="T:System.ArraySegment`1" />.</returns>
		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x06000A22 RID: 2594 RVA: 0x00032666 File Offset: 0x00030866
		public T[] Array
		{
			get
			{
				return this._array;
			}
		}

		/// <summary>Gets the position of the first element in the range delimited by the array segment, relative to the start of the original array.</summary>
		/// <returns>The position of the first element in the range delimited by the <see cref="T:System.ArraySegment`1" />, relative to the start of the original array.</returns>
		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x06000A23 RID: 2595 RVA: 0x0003266E File Offset: 0x0003086E
		public int Offset
		{
			get
			{
				return this._offset;
			}
		}

		/// <summary>Gets the number of elements in the range delimited by the array segment.</summary>
		/// <returns>The number of elements in the range delimited by the <see cref="T:System.ArraySegment`1" />.</returns>
		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x06000A24 RID: 2596 RVA: 0x00032676 File Offset: 0x00030876
		public int Count
		{
			get
			{
				return this._count;
			}
		}

		/// <summary>Returns the hash code for the current instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		// Token: 0x06000A25 RID: 2597 RVA: 0x0003267E File Offset: 0x0003087E
		public override int GetHashCode()
		{
			if (this._array != null)
			{
				return this._array.GetHashCode() ^ this._offset ^ this._count;
			}
			return 0;
		}

		/// <summary>Determines whether the specified object is equal to the current instance.</summary>
		/// <param name="obj">The object to be compared with the current instance.</param>
		/// <returns>
		///     <see langword="true" /> if the specified object is a <see cref="T:System.ArraySegment`1" /> structure and is equal to the current instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000A26 RID: 2598 RVA: 0x000326A3 File Offset: 0x000308A3
		public override bool Equals(object obj)
		{
			return obj is ArraySegment<T> && this.Equals((ArraySegment<T>)obj);
		}

		/// <summary>Determines whether the specified <see cref="T:System.ArraySegment`1" /> structure is equal to the current instance.</summary>
		/// <param name="obj">The structure to compare with the current instance.</param>
		/// <returns>
		///     <see langword="true" /> if the specified <see cref="T:System.ArraySegment`1" /> structure is equal to the current instance; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000A27 RID: 2599 RVA: 0x000326BB File Offset: 0x000308BB
		public bool Equals(ArraySegment<T> obj)
		{
			return obj._array == this._array && obj._offset == this._offset && obj._count == this._count;
		}

		/// <summary>Indicates whether two <see cref="T:System.ArraySegment`1" /> structures are equal.</summary>
		/// <param name="a">The  structure on the left side of the equality operator.</param>
		/// <param name="b">The structure on the right side of the equality operator.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> is equal to <paramref name="b" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000A28 RID: 2600 RVA: 0x000326E9 File Offset: 0x000308E9
		public static bool operator ==(ArraySegment<T> a, ArraySegment<T> b)
		{
			return a.Equals(b);
		}

		/// <summary>Indicates whether two <see cref="T:System.ArraySegment`1" /> structures are unequal.</summary>
		/// <param name="a">The structure on the left side of the inequality operator.</param>
		/// <param name="b">The structure on the right side of the inequality operator.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="a" /> is not equal to <paramref name="b" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06000A29 RID: 2601 RVA: 0x000326F3 File Offset: 0x000308F3
		public static bool operator !=(ArraySegment<T> a, ArraySegment<T> b)
		{
			return !(a == b);
		}

		// Token: 0x170001A3 RID: 419
		T IList<!0>.this[int index]
		{
			get
			{
				if (this._array == null)
				{
					throw new InvalidOperationException(Environment.GetResourceString("The underlying array is null."));
				}
				if (index < 0 || index >= this._count)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				return this._array[this._offset + index];
			}
			set
			{
				if (this._array == null)
				{
					throw new InvalidOperationException(Environment.GetResourceString("The underlying array is null."));
				}
				if (index < 0 || index >= this._count)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				this._array[this._offset + index] = value;
			}
		}

		// Token: 0x06000A2C RID: 2604 RVA: 0x000327A4 File Offset: 0x000309A4
		int IList<!0>.IndexOf(T item)
		{
			if (this._array == null)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The underlying array is null."));
			}
			int num = System.Array.IndexOf<T>(this._array, item, this._offset, this._count);
			if (num < 0)
			{
				return -1;
			}
			return num - this._offset;
		}

		// Token: 0x06000A2D RID: 2605 RVA: 0x000175EA File Offset: 0x000157EA
		void IList<!0>.Insert(int index, T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000A2E RID: 2606 RVA: 0x000175EA File Offset: 0x000157EA
		void IList<!0>.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		// Token: 0x170001A4 RID: 420
		T IReadOnlyList<!0>.this[int index]
		{
			get
			{
				if (this._array == null)
				{
					throw new InvalidOperationException(Environment.GetResourceString("The underlying array is null."));
				}
				if (index < 0 || index >= this._count)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				return this._array[this._offset + index];
			}
		}

		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x06000A30 RID: 2608 RVA: 0x00004E08 File Offset: 0x00003008
		bool ICollection<!0>.IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000A31 RID: 2609 RVA: 0x000175EA File Offset: 0x000157EA
		void ICollection<!0>.Add(T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000A32 RID: 2610 RVA: 0x000175EA File Offset: 0x000157EA
		void ICollection<!0>.Clear()
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000A33 RID: 2611 RVA: 0x00032840 File Offset: 0x00030A40
		bool ICollection<!0>.Contains(T item)
		{
			if (this._array == null)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The underlying array is null."));
			}
			return System.Array.IndexOf<T>(this._array, item, this._offset, this._count) >= 0;
		}

		// Token: 0x06000A34 RID: 2612 RVA: 0x00032878 File Offset: 0x00030A78
		void ICollection<!0>.CopyTo(T[] array, int arrayIndex)
		{
			if (this._array == null)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The underlying array is null."));
			}
			System.Array.Copy(this._array, this._offset, array, arrayIndex, this._count);
		}

		// Token: 0x06000A35 RID: 2613 RVA: 0x000175EA File Offset: 0x000157EA
		bool ICollection<!0>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000A36 RID: 2614 RVA: 0x000328AB File Offset: 0x00030AAB
		IEnumerator<T> IEnumerable<!0>.GetEnumerator()
		{
			if (this._array == null)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The underlying array is null."));
			}
			return new ArraySegment<T>.ArraySegmentEnumerator(this);
		}

		/// <summary>Returns an enumerator that iterates through an array segment.</summary>
		/// <returns>An enumerator that can be used to iterate through the array segment.</returns>
		// Token: 0x06000A37 RID: 2615 RVA: 0x000328AB File Offset: 0x00030AAB
		IEnumerator IEnumerable.GetEnumerator()
		{
			if (this._array == null)
			{
				throw new InvalidOperationException(Environment.GetResourceString("The underlying array is null."));
			}
			return new ArraySegment<T>.ArraySegmentEnumerator(this);
		}

		// Token: 0x04000769 RID: 1897
		private T[] _array;

		// Token: 0x0400076A RID: 1898
		private int _offset;

		// Token: 0x0400076B RID: 1899
		private int _count;

		// Token: 0x0200011A RID: 282
		[Serializable]
		private sealed class ArraySegmentEnumerator : IEnumerator<T>, IDisposable, IEnumerator
		{
			// Token: 0x06000A38 RID: 2616 RVA: 0x000328D0 File Offset: 0x00030AD0
			internal ArraySegmentEnumerator(ArraySegment<T> arraySegment)
			{
				this._array = arraySegment._array;
				this._start = arraySegment._offset;
				this._end = this._start + arraySegment._count;
				this._current = this._start - 1;
			}

			// Token: 0x06000A39 RID: 2617 RVA: 0x0003291C File Offset: 0x00030B1C
			public bool MoveNext()
			{
				if (this._current < this._end)
				{
					this._current++;
					return this._current < this._end;
				}
				return false;
			}

			// Token: 0x170001A6 RID: 422
			// (get) Token: 0x06000A3A RID: 2618 RVA: 0x0003294C File Offset: 0x00030B4C
			public T Current
			{
				get
				{
					if (this._current < this._start)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has not started. Call MoveNext."));
					}
					if (this._current >= this._end)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration already finished."));
					}
					return this._array[this._current];
				}
			}

			// Token: 0x170001A7 RID: 423
			// (get) Token: 0x06000A3B RID: 2619 RVA: 0x000329A6 File Offset: 0x00030BA6
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x06000A3C RID: 2620 RVA: 0x000329B3 File Offset: 0x00030BB3
			void IEnumerator.Reset()
			{
				this._current = this._start - 1;
			}

			// Token: 0x06000A3D RID: 2621 RVA: 0x000020D3 File Offset: 0x000002D3
			public void Dispose()
			{
			}

			// Token: 0x0400076C RID: 1900
			private T[] _array;

			// Token: 0x0400076D RID: 1901
			private int _start;

			// Token: 0x0400076E RID: 1902
			private int _end;

			// Token: 0x0400076F RID: 1903
			private int _current;
		}
	}
}
