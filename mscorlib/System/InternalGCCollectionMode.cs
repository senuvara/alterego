﻿using System;

namespace System
{
	// Token: 0x02000153 RID: 339
	[Serializable]
	internal enum InternalGCCollectionMode
	{
		// Token: 0x040008E5 RID: 2277
		NonBlocking = 1,
		// Token: 0x040008E6 RID: 2278
		Blocking,
		// Token: 0x040008E7 RID: 2279
		Optimized = 4,
		// Token: 0x040008E8 RID: 2280
		Compacting = 8
	}
}
