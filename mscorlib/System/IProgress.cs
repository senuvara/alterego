﻿using System;

namespace System
{
	/// <summary>Defines a provider for progress updates.</summary>
	/// <typeparam name="T">The type of progress update value.</typeparam>
	// Token: 0x02000186 RID: 390
	public interface IProgress<in T>
	{
		/// <summary>Reports a progress update.</summary>
		/// <param name="value">The value of the updated progress.</param>
		// Token: 0x060010E3 RID: 4323
		void Report(T value);
	}
}
