﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents a string comparison operation that uses specific case and culture-based or ordinal comparison rules.</summary>
	// Token: 0x020001B2 RID: 434
	[ComVisible(true)]
	[Serializable]
	public abstract class StringComparer : IComparer, IEqualityComparer, IComparer<string>, IEqualityComparer<string>
	{
		/// <summary>Gets a <see cref="T:System.StringComparer" /> object that performs a case-sensitive string comparison using the word comparison rules of the invariant culture.</summary>
		/// <returns>A new <see cref="T:System.StringComparer" /> object.</returns>
		// Token: 0x1700022F RID: 559
		// (get) Token: 0x060013C0 RID: 5056 RVA: 0x00050E68 File Offset: 0x0004F068
		public static StringComparer InvariantCulture
		{
			get
			{
				return StringComparer._invariantCulture;
			}
		}

		/// <summary>Gets a <see cref="T:System.StringComparer" /> object that performs a case-insensitive string comparison using the word comparison rules of the invariant culture.</summary>
		/// <returns>A new <see cref="T:System.StringComparer" /> object.</returns>
		// Token: 0x17000230 RID: 560
		// (get) Token: 0x060013C1 RID: 5057 RVA: 0x00050E6F File Offset: 0x0004F06F
		public static StringComparer InvariantCultureIgnoreCase
		{
			get
			{
				return StringComparer._invariantCultureIgnoreCase;
			}
		}

		/// <summary>Gets a <see cref="T:System.StringComparer" /> object that performs a case-sensitive string comparison using the word comparison rules of the current culture.</summary>
		/// <returns>A new <see cref="T:System.StringComparer" /> object.</returns>
		// Token: 0x17000231 RID: 561
		// (get) Token: 0x060013C2 RID: 5058 RVA: 0x00050E76 File Offset: 0x0004F076
		public static StringComparer CurrentCulture
		{
			get
			{
				return new CultureAwareComparer(CultureInfo.CurrentCulture, false);
			}
		}

		/// <summary>Gets a <see cref="T:System.StringComparer" /> object that performs case-insensitive string comparisons using the word comparison rules of the current culture.</summary>
		/// <returns>A new <see cref="T:System.StringComparer" /> object.</returns>
		// Token: 0x17000232 RID: 562
		// (get) Token: 0x060013C3 RID: 5059 RVA: 0x00050E83 File Offset: 0x0004F083
		public static StringComparer CurrentCultureIgnoreCase
		{
			get
			{
				return new CultureAwareComparer(CultureInfo.CurrentCulture, true);
			}
		}

		/// <summary>Gets a <see cref="T:System.StringComparer" /> object that performs a case-sensitive ordinal string comparison.</summary>
		/// <returns>A <see cref="T:System.StringComparer" /> object.</returns>
		// Token: 0x17000233 RID: 563
		// (get) Token: 0x060013C4 RID: 5060 RVA: 0x00050E90 File Offset: 0x0004F090
		public static StringComparer Ordinal
		{
			get
			{
				return StringComparer._ordinal;
			}
		}

		/// <summary>Gets a <see cref="T:System.StringComparer" /> object that performs a case-insensitive ordinal string comparison.</summary>
		/// <returns>A <see cref="T:System.StringComparer" /> object.</returns>
		// Token: 0x17000234 RID: 564
		// (get) Token: 0x060013C5 RID: 5061 RVA: 0x00050E97 File Offset: 0x0004F097
		public static StringComparer OrdinalIgnoreCase
		{
			get
			{
				return StringComparer._ordinalIgnoreCase;
			}
		}

		/// <summary>Creates a <see cref="T:System.StringComparer" /> object that compares strings according to the rules of a specified culture.</summary>
		/// <param name="culture">A culture whose linguistic rules are used to perform a string comparison.</param>
		/// <param name="ignoreCase">
		///       <see langword="true" /> to specify that comparison operations be case-insensitive; <see langword="false" /> to specify that comparison operations be case-sensitive.</param>
		/// <returns>A new <see cref="T:System.StringComparer" /> object that performs string comparisons according to the comparison rules used by the <paramref name="culture" /> parameter and the case rule specified by the <paramref name="ignoreCase" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="culture" /> is <see langword="null" />.</exception>
		// Token: 0x060013C6 RID: 5062 RVA: 0x00050E9E File Offset: 0x0004F09E
		public static StringComparer Create(CultureInfo culture, bool ignoreCase)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			return new CultureAwareComparer(culture, ignoreCase);
		}

		/// <summary>When overridden in a derived class, compares two objects and returns an indication of their relative sort order.</summary>
		/// <param name="x">An object to compare to <paramref name="y" />.</param>
		/// <param name="y">An object to compare to <paramref name="x" />.</param>
		/// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.ValueMeaningLess than zero
		///             <paramref name="x" /> precedes  <paramref name="y" /> in the sort order. -or-
		///             <paramref name="x" /> is <see langword="null" /> and <paramref name="y" /> is not <see langword="null" />.Zero
		///             <paramref name="x" /> is equal to <paramref name="y" />.-or-
		///             <paramref name="x" /> and <paramref name="y" /> are both <see langword="null" />. Greater than zero
		///             <paramref name="x" /> follows <paramref name="y" /> in the sort order.-or-
		///             <paramref name="y" /> is <see langword="null" /> and <paramref name="x" /> is not <see langword="null" />. </returns>
		/// <exception cref="T:System.ArgumentException">Neither <paramref name="x" /> nor <paramref name="y" /> is a <see cref="T:System.String" /> object, and neither <paramref name="x" /> nor <paramref name="y" /> implements the <see cref="T:System.IComparable" /> interface.</exception>
		// Token: 0x060013C7 RID: 5063 RVA: 0x00050EB8 File Offset: 0x0004F0B8
		public int Compare(object x, object y)
		{
			if (x == y)
			{
				return 0;
			}
			if (x == null)
			{
				return -1;
			}
			if (y == null)
			{
				return 1;
			}
			string text = x as string;
			if (text != null)
			{
				string text2 = y as string;
				if (text2 != null)
				{
					return this.Compare(text, text2);
				}
			}
			IComparable comparable = x as IComparable;
			if (comparable != null)
			{
				return comparable.CompareTo(y);
			}
			throw new ArgumentException(Environment.GetResourceString("At least one object must implement IComparable."));
		}

		/// <summary>When overridden in a derived class, indicates whether two objects are equal.</summary>
		/// <param name="x">An object to compare to <paramref name="y" />.</param>
		/// <param name="y">An object to compare to <paramref name="x" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="x" /> and <paramref name="y" /> refer to the same object, or <paramref name="x" /> and <paramref name="y" /> are both the same type of object and those objects are equal, or both <paramref name="x" /> and <paramref name="y" /> are <see langword="null" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060013C8 RID: 5064 RVA: 0x00050F14 File Offset: 0x0004F114
		public bool Equals(object x, object y)
		{
			if (x == y)
			{
				return true;
			}
			if (x == null || y == null)
			{
				return false;
			}
			string text = x as string;
			if (text != null)
			{
				string text2 = y as string;
				if (text2 != null)
				{
					return this.Equals(text, text2);
				}
			}
			return x.Equals(y);
		}

		/// <summary>When overridden in a derived class, gets the hash code for the specified object.</summary>
		/// <param name="obj">An object.</param>
		/// <returns>A 32-bit signed hash code calculated from the value of the <paramref name="obj" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">Not enough memory is available to allocate the buffer that is required to compute the hash code.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="obj" /> is <see langword="null" />.</exception>
		// Token: 0x060013C9 RID: 5065 RVA: 0x00050F54 File Offset: 0x0004F154
		public int GetHashCode(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			string text = obj as string;
			if (text != null)
			{
				return this.GetHashCode(text);
			}
			return obj.GetHashCode();
		}

		/// <summary>When overridden in a derived class, compares two strings and returns an indication of their relative sort order.</summary>
		/// <param name="x">A string to compare to <paramref name="y" />.</param>
		/// <param name="y">A string to compare to <paramref name="x" />.</param>
		/// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.ValueMeaningLess than zero
		///             <paramref name="x" /> precedes <paramref name="y" /> in the sort order.-or-
		///             <paramref name="x" /> is <see langword="null" /> and <paramref name="y" /> is not <see langword="null" />.Zero
		///             <paramref name="x" /> is equal to <paramref name="y" />.-or-
		///             <paramref name="x" /> and <paramref name="y" /> are both <see langword="null" />. Greater than zero
		///             <paramref name="x" /> follows <paramref name="y" /> in the sort order.-or-
		///             <paramref name="y" /> is <see langword="null" /> and <paramref name="x" /> is not <see langword="null" />. </returns>
		// Token: 0x060013CA RID: 5066
		public abstract int Compare(string x, string y);

		/// <summary>When overridden in a derived class, indicates whether two strings are equal.</summary>
		/// <param name="x">A string to compare to <paramref name="y" />.</param>
		/// <param name="y">A string to compare to <paramref name="x" />.</param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="x" /> and <paramref name="y" /> refer to the same object, or <paramref name="x" /> and <paramref name="y" /> are equal, or <paramref name="x" /> and <paramref name="y" /> are <see langword="null" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x060013CB RID: 5067
		public abstract bool Equals(string x, string y);

		/// <summary>When overridden in a derived class, gets the hash code for the specified string.</summary>
		/// <param name="obj">A string.</param>
		/// <returns>A 32-bit signed hash code calculated from the value of the <paramref name="obj" /> parameter.</returns>
		/// <exception cref="T:System.ArgumentException">Not enough memory is available to allocate the buffer that is required to compute the hash code.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="obj" /> is <see langword="null" />. </exception>
		// Token: 0x060013CC RID: 5068
		public abstract int GetHashCode(string obj);

		/// <summary>Initializes a new instance of the <see cref="T:System.StringComparer" /> class. </summary>
		// Token: 0x060013CD RID: 5069 RVA: 0x00002050 File Offset: 0x00000250
		protected StringComparer()
		{
		}

		// Token: 0x060013CE RID: 5070 RVA: 0x00050F87 File Offset: 0x0004F187
		// Note: this type is marked as 'beforefieldinit'.
		static StringComparer()
		{
		}

		// Token: 0x04000AAD RID: 2733
		private static readonly StringComparer _invariantCulture = new CultureAwareComparer(CultureInfo.InvariantCulture, false);

		// Token: 0x04000AAE RID: 2734
		private static readonly StringComparer _invariantCultureIgnoreCase = new CultureAwareComparer(CultureInfo.InvariantCulture, true);

		// Token: 0x04000AAF RID: 2735
		private static readonly StringComparer _ordinal = new OrdinalComparer(false);

		// Token: 0x04000AB0 RID: 2736
		private static readonly StringComparer _ordinalIgnoreCase = new OrdinalComparer(true);
	}
}
