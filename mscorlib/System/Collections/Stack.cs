﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;

namespace System.Collections
{
	/// <summary>Represents a simple last-in-first-out (LIFO) non-generic collection of objects.</summary>
	// Token: 0x020009CC RID: 2508
	[DebuggerTypeProxy(typeof(Stack.StackDebugView))]
	[ComVisible(true)]
	[DebuggerDisplay("Count = {Count}")]
	[Serializable]
	public class Stack : ICollection, IEnumerable, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Stack" /> class that is empty and has the default initial capacity.</summary>
		// Token: 0x06005AC7 RID: 23239 RVA: 0x00125D42 File Offset: 0x00123F42
		public Stack()
		{
			this._array = new object[10];
			this._size = 0;
			this._version = 0;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Stack" /> class that is empty and has the specified initial capacity or the default initial capacity, whichever is greater.</summary>
		/// <param name="initialCapacity">The initial number of elements that the <see cref="T:System.Collections.Stack" /> can contain. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="initialCapacity" /> is less than zero. </exception>
		// Token: 0x06005AC8 RID: 23240 RVA: 0x00125D68 File Offset: 0x00123F68
		public Stack(int initialCapacity)
		{
			if (initialCapacity < 0)
			{
				throw new ArgumentOutOfRangeException("initialCapacity", Environment.GetResourceString("Non-negative number required."));
			}
			if (initialCapacity < 10)
			{
				initialCapacity = 10;
			}
			this._array = new object[initialCapacity];
			this._size = 0;
			this._version = 0;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Stack" /> class that contains elements copied from the specified collection and has the same initial capacity as the number of elements copied.</summary>
		/// <param name="col">The <see cref="T:System.Collections.ICollection" /> to copy elements from. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="col" /> is <see langword="null" />. </exception>
		// Token: 0x06005AC9 RID: 23241 RVA: 0x00125DB8 File Offset: 0x00123FB8
		public Stack(ICollection col) : this((col == null) ? 32 : col.Count)
		{
			if (col == null)
			{
				throw new ArgumentNullException("col");
			}
			foreach (object obj in col)
			{
				this.Push(obj);
			}
		}

		/// <summary>Gets the number of elements contained in the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.Stack" />.</returns>
		// Token: 0x17001032 RID: 4146
		// (get) Token: 0x06005ACA RID: 23242 RVA: 0x00125E03 File Offset: 0x00124003
		public virtual int Count
		{
			get
			{
				return this._size;
			}
		}

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.Stack" /> is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" />, if access to the <see cref="T:System.Collections.Stack" /> is synchronized (thread safe); otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17001033 RID: 4147
		// (get) Token: 0x06005ACB RID: 23243 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>An <see cref="T:System.Object" /> that can be used to synchronize access to the <see cref="T:System.Collections.Stack" />.</returns>
		// Token: 0x17001034 RID: 4148
		// (get) Token: 0x06005ACC RID: 23244 RVA: 0x00125E0B File Offset: 0x0012400B
		public virtual object SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					Interlocked.CompareExchange<object>(ref this._syncRoot, new object(), null);
				}
				return this._syncRoot;
			}
		}

		/// <summary>Removes all objects from the <see cref="T:System.Collections.Stack" />.</summary>
		// Token: 0x06005ACD RID: 23245 RVA: 0x00125E2D File Offset: 0x0012402D
		public virtual void Clear()
		{
			Array.Clear(this._array, 0, this._size);
			this._size = 0;
			this._version++;
		}

		/// <summary>Creates a shallow copy of the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>A shallow copy of the <see cref="T:System.Collections.Stack" />.</returns>
		// Token: 0x06005ACE RID: 23246 RVA: 0x00125E58 File Offset: 0x00124058
		public virtual object Clone()
		{
			Stack stack = new Stack(this._size);
			stack._size = this._size;
			Array.Copy(this._array, 0, stack._array, 0, this._size);
			stack._version = this._version;
			return stack;
		}

		/// <summary>Determines whether an element is in the <see cref="T:System.Collections.Stack" />.</summary>
		/// <param name="obj">The object to locate in the <see cref="T:System.Collections.Stack" />. The value can be <see langword="null" />. </param>
		/// <returns>
		///     <see langword="true" />, if <paramref name="obj" /> is found in the <see cref="T:System.Collections.Stack" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06005ACF RID: 23247 RVA: 0x00125EA4 File Offset: 0x001240A4
		public virtual bool Contains(object obj)
		{
			int size = this._size;
			while (size-- > 0)
			{
				if (obj == null)
				{
					if (this._array[size] == null)
					{
						return true;
					}
				}
				else if (this._array[size] != null && this._array[size].Equals(obj))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Copies the <see cref="T:System.Collections.Stack" /> to an existing one-dimensional <see cref="T:System.Array" />, starting at the specified array index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Stack" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional.-or- The number of elements in the source <see cref="T:System.Collections.Stack" /> is greater than the available space from <paramref name="index" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.Stack" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		// Token: 0x06005AD0 RID: 23248 RVA: 0x00125EF0 File Offset: 0x001240F0
		public virtual void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Non-negative number required."));
			}
			if (array.Length - index < this._size)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			int i = 0;
			if (array is object[])
			{
				object[] array2 = (object[])array;
				while (i < this._size)
				{
					array2[i + index] = this._array[this._size - i - 1];
					i++;
				}
				return;
			}
			while (i < this._size)
			{
				array.SetValue(this._array[this._size - i - 1], i + index);
				i++;
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Collections.Stack" />.</returns>
		// Token: 0x06005AD1 RID: 23249 RVA: 0x00125FBB File Offset: 0x001241BB
		public virtual IEnumerator GetEnumerator()
		{
			return new Stack.StackEnumerator(this);
		}

		/// <summary>Returns the object at the top of the <see cref="T:System.Collections.Stack" /> without removing it.</summary>
		/// <returns>The <see cref="T:System.Object" /> at the top of the <see cref="T:System.Collections.Stack" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Collections.Stack" /> is empty. </exception>
		// Token: 0x06005AD2 RID: 23250 RVA: 0x00125FC3 File Offset: 0x001241C3
		public virtual object Peek()
		{
			if (this._size == 0)
			{
				throw new InvalidOperationException(Environment.GetResourceString("Stack empty."));
			}
			return this._array[this._size - 1];
		}

		/// <summary>Removes and returns the object at the top of the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>The <see cref="T:System.Object" /> removed from the top of the <see cref="T:System.Collections.Stack" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Collections.Stack" /> is empty. </exception>
		// Token: 0x06005AD3 RID: 23251 RVA: 0x00125FEC File Offset: 0x001241EC
		public virtual object Pop()
		{
			if (this._size == 0)
			{
				throw new InvalidOperationException(Environment.GetResourceString("Stack empty."));
			}
			this._version++;
			object[] array = this._array;
			int num = this._size - 1;
			this._size = num;
			object result = array[num];
			this._array[this._size] = null;
			return result;
		}

		/// <summary>Inserts an object at the top of the <see cref="T:System.Collections.Stack" />.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to push onto the <see cref="T:System.Collections.Stack" />. The value can be <see langword="null" />. </param>
		// Token: 0x06005AD4 RID: 23252 RVA: 0x00126048 File Offset: 0x00124248
		public virtual void Push(object obj)
		{
			if (this._size == this._array.Length)
			{
				object[] array = new object[2 * this._array.Length];
				Array.Copy(this._array, 0, array, 0, this._size);
				this._array = array;
			}
			object[] array2 = this._array;
			int size = this._size;
			this._size = size + 1;
			array2[size] = obj;
			this._version++;
		}

		/// <summary>Returns a synchronized (thread safe) wrapper for the <see cref="T:System.Collections.Stack" />.</summary>
		/// <param name="stack">The <see cref="T:System.Collections.Stack" /> to synchronize. </param>
		/// <returns>A synchronized wrapper around the <see cref="T:System.Collections.Stack" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="stack" /> is <see langword="null" />. </exception>
		// Token: 0x06005AD5 RID: 23253 RVA: 0x001260B7 File Offset: 0x001242B7
		[HostProtection(SecurityAction.LinkDemand, Synchronization = true)]
		public static Stack Synchronized(Stack stack)
		{
			if (stack == null)
			{
				throw new ArgumentNullException("stack");
			}
			return new Stack.SyncStack(stack);
		}

		/// <summary>Copies the <see cref="T:System.Collections.Stack" /> to a new array.</summary>
		/// <returns>A new array containing copies of the elements of the <see cref="T:System.Collections.Stack" />.</returns>
		// Token: 0x06005AD6 RID: 23254 RVA: 0x001260D0 File Offset: 0x001242D0
		public virtual object[] ToArray()
		{
			object[] array = new object[this._size];
			for (int i = 0; i < this._size; i++)
			{
				array[i] = this._array[this._size - i - 1];
			}
			return array;
		}

		// Token: 0x04002DC7 RID: 11719
		private object[] _array;

		// Token: 0x04002DC8 RID: 11720
		private int _size;

		// Token: 0x04002DC9 RID: 11721
		private int _version;

		// Token: 0x04002DCA RID: 11722
		[NonSerialized]
		private object _syncRoot;

		// Token: 0x04002DCB RID: 11723
		private const int _defaultCapacity = 10;

		// Token: 0x020009CD RID: 2509
		[Serializable]
		private class SyncStack : Stack
		{
			// Token: 0x06005AD7 RID: 23255 RVA: 0x0012610F File Offset: 0x0012430F
			internal SyncStack(Stack stack)
			{
				this._s = stack;
				this._root = stack.SyncRoot;
			}

			// Token: 0x17001035 RID: 4149
			// (get) Token: 0x06005AD8 RID: 23256 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17001036 RID: 4150
			// (get) Token: 0x06005AD9 RID: 23257 RVA: 0x0012612A File Offset: 0x0012432A
			public override object SyncRoot
			{
				get
				{
					return this._root;
				}
			}

			// Token: 0x17001037 RID: 4151
			// (get) Token: 0x06005ADA RID: 23258 RVA: 0x00126134 File Offset: 0x00124334
			public override int Count
			{
				get
				{
					object root = this._root;
					int count;
					lock (root)
					{
						count = this._s.Count;
					}
					return count;
				}
			}

			// Token: 0x06005ADB RID: 23259 RVA: 0x0012617C File Offset: 0x0012437C
			public override bool Contains(object obj)
			{
				object root = this._root;
				bool result;
				lock (root)
				{
					result = this._s.Contains(obj);
				}
				return result;
			}

			// Token: 0x06005ADC RID: 23260 RVA: 0x001261C4 File Offset: 0x001243C4
			public override object Clone()
			{
				object root = this._root;
				object result;
				lock (root)
				{
					result = new Stack.SyncStack((Stack)this._s.Clone());
				}
				return result;
			}

			// Token: 0x06005ADD RID: 23261 RVA: 0x00126218 File Offset: 0x00124418
			public override void Clear()
			{
				object root = this._root;
				lock (root)
				{
					this._s.Clear();
				}
			}

			// Token: 0x06005ADE RID: 23262 RVA: 0x00126260 File Offset: 0x00124460
			public override void CopyTo(Array array, int arrayIndex)
			{
				object root = this._root;
				lock (root)
				{
					this._s.CopyTo(array, arrayIndex);
				}
			}

			// Token: 0x06005ADF RID: 23263 RVA: 0x001262A8 File Offset: 0x001244A8
			public override void Push(object value)
			{
				object root = this._root;
				lock (root)
				{
					this._s.Push(value);
				}
			}

			// Token: 0x06005AE0 RID: 23264 RVA: 0x001262F0 File Offset: 0x001244F0
			public override object Pop()
			{
				object root = this._root;
				object result;
				lock (root)
				{
					result = this._s.Pop();
				}
				return result;
			}

			// Token: 0x06005AE1 RID: 23265 RVA: 0x00126338 File Offset: 0x00124538
			public override IEnumerator GetEnumerator()
			{
				object root = this._root;
				IEnumerator enumerator;
				lock (root)
				{
					enumerator = this._s.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x06005AE2 RID: 23266 RVA: 0x00126380 File Offset: 0x00124580
			public override object Peek()
			{
				object root = this._root;
				object result;
				lock (root)
				{
					result = this._s.Peek();
				}
				return result;
			}

			// Token: 0x06005AE3 RID: 23267 RVA: 0x001263C8 File Offset: 0x001245C8
			public override object[] ToArray()
			{
				object root = this._root;
				object[] result;
				lock (root)
				{
					result = this._s.ToArray();
				}
				return result;
			}

			// Token: 0x04002DCC RID: 11724
			private Stack _s;

			// Token: 0x04002DCD RID: 11725
			private object _root;
		}

		// Token: 0x020009CE RID: 2510
		[Serializable]
		private class StackEnumerator : IEnumerator, ICloneable
		{
			// Token: 0x06005AE4 RID: 23268 RVA: 0x00126410 File Offset: 0x00124610
			internal StackEnumerator(Stack stack)
			{
				this._stack = stack;
				this._version = this._stack._version;
				this._index = -2;
				this.currentElement = null;
			}

			// Token: 0x06005AE5 RID: 23269 RVA: 0x0002BE93 File Offset: 0x0002A093
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x06005AE6 RID: 23270 RVA: 0x00126440 File Offset: 0x00124640
			public virtual bool MoveNext()
			{
				if (this._version != this._stack._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				if (this._index == -2)
				{
					this._index = this._stack._size - 1;
					bool flag = this._index >= 0;
					if (flag)
					{
						this.currentElement = this._stack._array[this._index];
					}
					return flag;
				}
				if (this._index == -1)
				{
					return false;
				}
				int num = this._index - 1;
				this._index = num;
				bool flag2 = num >= 0;
				if (flag2)
				{
					this.currentElement = this._stack._array[this._index];
					return flag2;
				}
				this.currentElement = null;
				return flag2;
			}

			// Token: 0x17001038 RID: 4152
			// (get) Token: 0x06005AE7 RID: 23271 RVA: 0x001264FA File Offset: 0x001246FA
			public virtual object Current
			{
				get
				{
					if (this._index == -2)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has not started. Call MoveNext."));
					}
					if (this._index == -1)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration already finished."));
					}
					return this.currentElement;
				}
			}

			// Token: 0x06005AE8 RID: 23272 RVA: 0x00126535 File Offset: 0x00124735
			public virtual void Reset()
			{
				if (this._version != this._stack._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				this._index = -2;
				this.currentElement = null;
			}

			// Token: 0x04002DCE RID: 11726
			private Stack _stack;

			// Token: 0x04002DCF RID: 11727
			private int _index;

			// Token: 0x04002DD0 RID: 11728
			private int _version;

			// Token: 0x04002DD1 RID: 11729
			private object currentElement;
		}

		// Token: 0x020009CF RID: 2511
		internal class StackDebugView
		{
			// Token: 0x06005AE9 RID: 23273 RVA: 0x00126569 File Offset: 0x00124769
			public StackDebugView(Stack stack)
			{
				if (stack == null)
				{
					throw new ArgumentNullException("stack");
				}
				this.stack = stack;
			}

			// Token: 0x17001039 RID: 4153
			// (get) Token: 0x06005AEA RID: 23274 RVA: 0x00126586 File Offset: 0x00124786
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public object[] Items
			{
				get
				{
					return this.stack.ToArray();
				}
			}

			// Token: 0x04002DD2 RID: 11730
			private Stack stack;
		}
	}
}
