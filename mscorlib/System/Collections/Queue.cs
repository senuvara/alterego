﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;

namespace System.Collections
{
	/// <summary>Represents a first-in, first-out collection of objects.</summary>
	// Token: 0x020009C1 RID: 2497
	[DebuggerDisplay("Count = {Count}")]
	[ComVisible(true)]
	[DebuggerTypeProxy(typeof(Queue.QueueDebugView))]
	[Serializable]
	public class Queue : ICollection, IEnumerable, ICloneable
	{
		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Queue" /> class that is empty, has the default initial capacity, and uses the default growth factor.</summary>
		// Token: 0x06005A25 RID: 23077 RVA: 0x00123E67 File Offset: 0x00122067
		public Queue() : this(32, 2f)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Queue" /> class that is empty, has the specified initial capacity, and uses the default growth factor.</summary>
		/// <param name="capacity">The initial number of elements that the <see cref="T:System.Collections.Queue" /> can contain. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than zero. </exception>
		// Token: 0x06005A26 RID: 23078 RVA: 0x00123E76 File Offset: 0x00122076
		public Queue(int capacity) : this(capacity, 2f)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Queue" /> class that is empty, has the specified initial capacity, and uses the specified growth factor.</summary>
		/// <param name="capacity">The initial number of elements that the <see cref="T:System.Collections.Queue" /> can contain. </param>
		/// <param name="growFactor">The factor by which the capacity of the <see cref="T:System.Collections.Queue" /> is expanded. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="capacity" /> is less than zero.-or- 
		///         <paramref name="growFactor" /> is less than 1.0 or greater than 10.0. </exception>
		// Token: 0x06005A27 RID: 23079 RVA: 0x00123E84 File Offset: 0x00122084
		public Queue(int capacity, float growFactor)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity", Environment.GetResourceString("Non-negative number required."));
			}
			if ((double)growFactor < 1.0 || (double)growFactor > 10.0)
			{
				throw new ArgumentOutOfRangeException("growFactor", Environment.GetResourceString("Queue grow factor must be between {0} and {1}.", new object[]
				{
					1,
					10
				}));
			}
			this._array = new object[capacity];
			this._head = 0;
			this._tail = 0;
			this._size = 0;
			this._growFactor = (int)(growFactor * 100f);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Queue" /> class that contains elements copied from the specified collection, has the same initial capacity as the number of elements copied, and uses the default growth factor.</summary>
		/// <param name="col">The <see cref="T:System.Collections.ICollection" /> to copy elements from. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="col" /> is <see langword="null" />. </exception>
		// Token: 0x06005A28 RID: 23080 RVA: 0x00123F28 File Offset: 0x00122128
		public Queue(ICollection col) : this((col == null) ? 32 : col.Count)
		{
			if (col == null)
			{
				throw new ArgumentNullException("col");
			}
			foreach (object obj in col)
			{
				this.Enqueue(obj);
			}
		}

		/// <summary>Gets the number of elements contained in the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.Queue" />.</returns>
		// Token: 0x17001005 RID: 4101
		// (get) Token: 0x06005A29 RID: 23081 RVA: 0x00123F73 File Offset: 0x00122173
		public virtual int Count
		{
			get
			{
				return this._size;
			}
		}

		/// <summary>Creates a shallow copy of the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>A shallow copy of the <see cref="T:System.Collections.Queue" />.</returns>
		// Token: 0x06005A2A RID: 23082 RVA: 0x00123F7C File Offset: 0x0012217C
		public virtual object Clone()
		{
			Queue queue = new Queue(this._size);
			queue._size = this._size;
			int num = this._size;
			int num2 = (this._array.Length - this._head < num) ? (this._array.Length - this._head) : num;
			Array.Copy(this._array, this._head, queue._array, 0, num2);
			num -= num2;
			if (num > 0)
			{
				Array.Copy(this._array, 0, queue._array, this._array.Length - this._head, num);
			}
			queue._version = this._version;
			return queue;
		}

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.Queue" /> is synchronized (thread safe).</summary>
		/// <returns>
		///     <see langword="true" /> if access to the <see cref="T:System.Collections.Queue" /> is synchronized (thread safe); otherwise, <see langword="false" />. The default is <see langword="false" />.</returns>
		// Token: 0x17001006 RID: 4102
		// (get) Token: 0x06005A2B RID: 23083 RVA: 0x00002526 File Offset: 0x00000726
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.Queue" />.</returns>
		// Token: 0x17001007 RID: 4103
		// (get) Token: 0x06005A2C RID: 23084 RVA: 0x0012401D File Offset: 0x0012221D
		public virtual object SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					Interlocked.CompareExchange(ref this._syncRoot, new object(), null);
				}
				return this._syncRoot;
			}
		}

		/// <summary>Removes all objects from the <see cref="T:System.Collections.Queue" />.</summary>
		// Token: 0x06005A2D RID: 23085 RVA: 0x00124040 File Offset: 0x00122240
		public virtual void Clear()
		{
			if (this._head < this._tail)
			{
				Array.Clear(this._array, this._head, this._size);
			}
			else
			{
				Array.Clear(this._array, this._head, this._array.Length - this._head);
				Array.Clear(this._array, 0, this._tail);
			}
			this._head = 0;
			this._tail = 0;
			this._size = 0;
			this._version++;
		}

		/// <summary>Copies the <see cref="T:System.Collections.Queue" /> elements to an existing one-dimensional <see cref="T:System.Array" />, starting at the specified array index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Queue" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="array" /> is <see langword="null" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///         <paramref name="index" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///         <paramref name="array" /> is multidimensional.-or- The number of elements in the source <see cref="T:System.Collections.Queue" /> is greater than the available space from <paramref name="index" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.ArrayTypeMismatchException">The type of the source <see cref="T:System.Collections.Queue" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		// Token: 0x06005A2E RID: 23086 RVA: 0x001240CC File Offset: 0x001222CC
		public virtual void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException(Environment.GetResourceString("Only single dimensional arrays are supported for the requested action."));
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Environment.GetResourceString("Index was out of range. Must be non-negative and less than the size of the collection."));
			}
			if (array.Length - index < this._size)
			{
				throw new ArgumentException(Environment.GetResourceString("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
			int num = this._size;
			if (num == 0)
			{
				return;
			}
			int num2 = (this._array.Length - this._head < num) ? (this._array.Length - this._head) : num;
			Array.Copy(this._array, this._head, array, index, num2);
			num -= num2;
			if (num > 0)
			{
				Array.Copy(this._array, 0, array, index + this._array.Length - this._head, num);
			}
		}

		/// <summary>Adds an object to the end of the <see cref="T:System.Collections.Queue" />.</summary>
		/// <param name="obj">The object to add to the <see cref="T:System.Collections.Queue" />. The value can be <see langword="null" />. </param>
		// Token: 0x06005A2F RID: 23087 RVA: 0x001241A4 File Offset: 0x001223A4
		public virtual void Enqueue(object obj)
		{
			if (this._size == this._array.Length)
			{
				int num = (int)((long)this._array.Length * (long)this._growFactor / 100L);
				if (num < this._array.Length + 4)
				{
					num = this._array.Length + 4;
				}
				this.SetCapacity(num);
			}
			this._array[this._tail] = obj;
			this._tail = (this._tail + 1) % this._array.Length;
			this._size++;
			this._version++;
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Collections.Queue" />.</returns>
		// Token: 0x06005A30 RID: 23088 RVA: 0x00124238 File Offset: 0x00122438
		public virtual IEnumerator GetEnumerator()
		{
			return new Queue.QueueEnumerator(this);
		}

		/// <summary>Removes and returns the object at the beginning of the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>The object that is removed from the beginning of the <see cref="T:System.Collections.Queue" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Collections.Queue" /> is empty. </exception>
		// Token: 0x06005A31 RID: 23089 RVA: 0x00124240 File Offset: 0x00122440
		public virtual object Dequeue()
		{
			if (this.Count == 0)
			{
				throw new InvalidOperationException(Environment.GetResourceString("Queue empty."));
			}
			object result = this._array[this._head];
			this._array[this._head] = null;
			this._head = (this._head + 1) % this._array.Length;
			this._size--;
			this._version++;
			return result;
		}

		/// <summary>Returns the object at the beginning of the <see cref="T:System.Collections.Queue" /> without removing it.</summary>
		/// <returns>The object at the beginning of the <see cref="T:System.Collections.Queue" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Collections.Queue" /> is empty. </exception>
		// Token: 0x06005A32 RID: 23090 RVA: 0x001242B3 File Offset: 0x001224B3
		public virtual object Peek()
		{
			if (this.Count == 0)
			{
				throw new InvalidOperationException(Environment.GetResourceString("Queue empty."));
			}
			return this._array[this._head];
		}

		/// <summary>Returns a new <see cref="T:System.Collections.Queue" /> that wraps the original queue, and is thread safe.</summary>
		/// <param name="queue">The <see cref="T:System.Collections.Queue" /> to synchronize. </param>
		/// <returns>A <see cref="T:System.Collections.Queue" /> wrapper that is synchronized (thread safe).</returns>
		/// <exception cref="T:System.ArgumentNullException">
		///         <paramref name="queue" /> is <see langword="null" />. </exception>
		// Token: 0x06005A33 RID: 23091 RVA: 0x001242DA File Offset: 0x001224DA
		[HostProtection(SecurityAction.LinkDemand, Synchronization = true)]
		public static Queue Synchronized(Queue queue)
		{
			if (queue == null)
			{
				throw new ArgumentNullException("queue");
			}
			return new Queue.SynchronizedQueue(queue);
		}

		/// <summary>Determines whether an element is in the <see cref="T:System.Collections.Queue" />.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.Queue" />. The value can be <see langword="null" />. </param>
		/// <returns>
		///     <see langword="true" /> if <paramref name="obj" /> is found in the <see cref="T:System.Collections.Queue" />; otherwise, <see langword="false" />.</returns>
		// Token: 0x06005A34 RID: 23092 RVA: 0x001242F0 File Offset: 0x001224F0
		public virtual bool Contains(object obj)
		{
			int num = this._head;
			int size = this._size;
			while (size-- > 0)
			{
				if (obj == null)
				{
					if (this._array[num] == null)
					{
						return true;
					}
				}
				else if (this._array[num] != null && this._array[num].Equals(obj))
				{
					return true;
				}
				num = (num + 1) % this._array.Length;
			}
			return false;
		}

		// Token: 0x06005A35 RID: 23093 RVA: 0x0012434E File Offset: 0x0012254E
		internal object GetElement(int i)
		{
			return this._array[(this._head + i) % this._array.Length];
		}

		/// <summary>Copies the <see cref="T:System.Collections.Queue" /> elements to a new array.</summary>
		/// <returns>A new array containing elements copied from the <see cref="T:System.Collections.Queue" />.</returns>
		// Token: 0x06005A36 RID: 23094 RVA: 0x00124368 File Offset: 0x00122568
		public virtual object[] ToArray()
		{
			object[] array = new object[this._size];
			if (this._size == 0)
			{
				return array;
			}
			if (this._head < this._tail)
			{
				Array.Copy(this._array, this._head, array, 0, this._size);
			}
			else
			{
				Array.Copy(this._array, this._head, array, 0, this._array.Length - this._head);
				Array.Copy(this._array, 0, array, this._array.Length - this._head, this._tail);
			}
			return array;
		}

		// Token: 0x06005A37 RID: 23095 RVA: 0x001243FC File Offset: 0x001225FC
		private void SetCapacity(int capacity)
		{
			object[] array = new object[capacity];
			if (this._size > 0)
			{
				if (this._head < this._tail)
				{
					Array.Copy(this._array, this._head, array, 0, this._size);
				}
				else
				{
					Array.Copy(this._array, this._head, array, 0, this._array.Length - this._head);
					Array.Copy(this._array, 0, array, this._array.Length - this._head, this._tail);
				}
			}
			this._array = array;
			this._head = 0;
			this._tail = ((this._size == capacity) ? 0 : this._size);
			this._version++;
		}

		/// <summary>Sets the capacity to the actual number of elements in the <see cref="T:System.Collections.Queue" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Queue" /> is read-only.</exception>
		// Token: 0x06005A38 RID: 23096 RVA: 0x001244BA File Offset: 0x001226BA
		public virtual void TrimToSize()
		{
			this.SetCapacity(this._size);
		}

		// Token: 0x04002D9B RID: 11675
		private object[] _array;

		// Token: 0x04002D9C RID: 11676
		private int _head;

		// Token: 0x04002D9D RID: 11677
		private int _tail;

		// Token: 0x04002D9E RID: 11678
		private int _size;

		// Token: 0x04002D9F RID: 11679
		private int _growFactor;

		// Token: 0x04002DA0 RID: 11680
		private int _version;

		// Token: 0x04002DA1 RID: 11681
		[NonSerialized]
		private object _syncRoot;

		// Token: 0x04002DA2 RID: 11682
		private const int _MinimumGrow = 4;

		// Token: 0x04002DA3 RID: 11683
		private const int _ShrinkThreshold = 32;

		// Token: 0x020009C2 RID: 2498
		[Serializable]
		private class SynchronizedQueue : Queue
		{
			// Token: 0x06005A39 RID: 23097 RVA: 0x001244C8 File Offset: 0x001226C8
			internal SynchronizedQueue(Queue q)
			{
				this._q = q;
				this.root = this._q.SyncRoot;
			}

			// Token: 0x17001008 RID: 4104
			// (get) Token: 0x06005A3A RID: 23098 RVA: 0x00004E08 File Offset: 0x00003008
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17001009 RID: 4105
			// (get) Token: 0x06005A3B RID: 23099 RVA: 0x001244E8 File Offset: 0x001226E8
			public override object SyncRoot
			{
				get
				{
					return this.root;
				}
			}

			// Token: 0x1700100A RID: 4106
			// (get) Token: 0x06005A3C RID: 23100 RVA: 0x001244F0 File Offset: 0x001226F0
			public override int Count
			{
				get
				{
					object obj = this.root;
					int count;
					lock (obj)
					{
						count = this._q.Count;
					}
					return count;
				}
			}

			// Token: 0x06005A3D RID: 23101 RVA: 0x00124538 File Offset: 0x00122738
			public override void Clear()
			{
				object obj = this.root;
				lock (obj)
				{
					this._q.Clear();
				}
			}

			// Token: 0x06005A3E RID: 23102 RVA: 0x00124580 File Offset: 0x00122780
			public override object Clone()
			{
				object obj = this.root;
				object result;
				lock (obj)
				{
					result = new Queue.SynchronizedQueue((Queue)this._q.Clone());
				}
				return result;
			}

			// Token: 0x06005A3F RID: 23103 RVA: 0x001245D4 File Offset: 0x001227D4
			public override bool Contains(object obj)
			{
				object obj2 = this.root;
				bool result;
				lock (obj2)
				{
					result = this._q.Contains(obj);
				}
				return result;
			}

			// Token: 0x06005A40 RID: 23104 RVA: 0x0012461C File Offset: 0x0012281C
			public override void CopyTo(Array array, int arrayIndex)
			{
				object obj = this.root;
				lock (obj)
				{
					this._q.CopyTo(array, arrayIndex);
				}
			}

			// Token: 0x06005A41 RID: 23105 RVA: 0x00124664 File Offset: 0x00122864
			public override void Enqueue(object value)
			{
				object obj = this.root;
				lock (obj)
				{
					this._q.Enqueue(value);
				}
			}

			// Token: 0x06005A42 RID: 23106 RVA: 0x001246AC File Offset: 0x001228AC
			public override object Dequeue()
			{
				object obj = this.root;
				object result;
				lock (obj)
				{
					result = this._q.Dequeue();
				}
				return result;
			}

			// Token: 0x06005A43 RID: 23107 RVA: 0x001246F4 File Offset: 0x001228F4
			public override IEnumerator GetEnumerator()
			{
				object obj = this.root;
				IEnumerator enumerator;
				lock (obj)
				{
					enumerator = this._q.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x06005A44 RID: 23108 RVA: 0x0012473C File Offset: 0x0012293C
			public override object Peek()
			{
				object obj = this.root;
				object result;
				lock (obj)
				{
					result = this._q.Peek();
				}
				return result;
			}

			// Token: 0x06005A45 RID: 23109 RVA: 0x00124784 File Offset: 0x00122984
			public override object[] ToArray()
			{
				object obj = this.root;
				object[] result;
				lock (obj)
				{
					result = this._q.ToArray();
				}
				return result;
			}

			// Token: 0x06005A46 RID: 23110 RVA: 0x001247CC File Offset: 0x001229CC
			public override void TrimToSize()
			{
				object obj = this.root;
				lock (obj)
				{
					this._q.TrimToSize();
				}
			}

			// Token: 0x04002DA4 RID: 11684
			private Queue _q;

			// Token: 0x04002DA5 RID: 11685
			private object root;
		}

		// Token: 0x020009C3 RID: 2499
		[Serializable]
		private class QueueEnumerator : IEnumerator, ICloneable
		{
			// Token: 0x06005A47 RID: 23111 RVA: 0x00124814 File Offset: 0x00122A14
			internal QueueEnumerator(Queue q)
			{
				this._q = q;
				this._version = this._q._version;
				this._index = 0;
				this.currentElement = this._q._array;
				if (this._q._size == 0)
				{
					this._index = -1;
				}
			}

			// Token: 0x06005A48 RID: 23112 RVA: 0x0002BE93 File Offset: 0x0002A093
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x06005A49 RID: 23113 RVA: 0x0012486C File Offset: 0x00122A6C
			public virtual bool MoveNext()
			{
				if (this._version != this._q._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				if (this._index < 0)
				{
					this.currentElement = this._q._array;
					return false;
				}
				this.currentElement = this._q.GetElement(this._index);
				this._index++;
				if (this._index == this._q._size)
				{
					this._index = -1;
				}
				return true;
			}

			// Token: 0x1700100B RID: 4107
			// (get) Token: 0x06005A4A RID: 23114 RVA: 0x001248F8 File Offset: 0x00122AF8
			public virtual object Current
			{
				get
				{
					if (this.currentElement != this._q._array)
					{
						return this.currentElement;
					}
					if (this._index == 0)
					{
						throw new InvalidOperationException(Environment.GetResourceString("Enumeration has not started. Call MoveNext."));
					}
					throw new InvalidOperationException(Environment.GetResourceString("Enumeration already finished."));
				}
			}

			// Token: 0x06005A4B RID: 23115 RVA: 0x00124948 File Offset: 0x00122B48
			public virtual void Reset()
			{
				if (this._version != this._q._version)
				{
					throw new InvalidOperationException(Environment.GetResourceString("Collection was modified; enumeration operation may not execute."));
				}
				if (this._q._size == 0)
				{
					this._index = -1;
				}
				else
				{
					this._index = 0;
				}
				this.currentElement = this._q._array;
			}

			// Token: 0x04002DA6 RID: 11686
			private Queue _q;

			// Token: 0x04002DA7 RID: 11687
			private int _index;

			// Token: 0x04002DA8 RID: 11688
			private int _version;

			// Token: 0x04002DA9 RID: 11689
			private object currentElement;
		}

		// Token: 0x020009C4 RID: 2500
		internal class QueueDebugView
		{
			// Token: 0x06005A4C RID: 23116 RVA: 0x001249A6 File Offset: 0x00122BA6
			public QueueDebugView(Queue queue)
			{
				if (queue == null)
				{
					throw new ArgumentNullException("queue");
				}
				this.queue = queue;
			}

			// Token: 0x1700100C RID: 4108
			// (get) Token: 0x06005A4D RID: 23117 RVA: 0x001249C3 File Offset: 0x00122BC3
			[DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
			public object[] Items
			{
				get
				{
					return this.queue.ToArray();
				}
			}

			// Token: 0x04002DAA RID: 11690
			private Queue queue;
		}
	}
}
