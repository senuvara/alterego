﻿using System;

namespace System.Collections
{
	// Token: 0x020009D1 RID: 2513
	[Serializable]
	internal class StructuralEqualityComparer : IEqualityComparer
	{
		// Token: 0x06005AED RID: 23277 RVA: 0x001265E4 File Offset: 0x001247E4
		public bool Equals(object x, object y)
		{
			if (x == null)
			{
				return y == null;
			}
			IStructuralEquatable structuralEquatable = x as IStructuralEquatable;
			if (structuralEquatable != null)
			{
				return structuralEquatable.Equals(y, this);
			}
			return y != null && x.Equals(y);
		}

		// Token: 0x06005AEE RID: 23278 RVA: 0x0012661C File Offset: 0x0012481C
		public int GetHashCode(object obj)
		{
			if (obj == null)
			{
				return 0;
			}
			IStructuralEquatable structuralEquatable = obj as IStructuralEquatable;
			if (structuralEquatable != null)
			{
				return structuralEquatable.GetHashCode(this);
			}
			return obj.GetHashCode();
		}

		// Token: 0x06005AEF RID: 23279 RVA: 0x00002050 File Offset: 0x00000250
		public StructuralEqualityComparer()
		{
		}
	}
}
