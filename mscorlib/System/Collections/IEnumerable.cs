﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	/// <summary>Exposes an enumerator, which supports a simple iteration over a non-generic collection.To browse the .NET Framework source code for this type, see the Reference Source.</summary>
	// Token: 0x020009B4 RID: 2484
	[ComVisible(true)]
	[Guid("496B0ABE-CDEE-11d3-88E8-00902754C43A")]
	public interface IEnumerable
	{
		/// <summary>Returns an enumerator that iterates through a collection.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
		// Token: 0x060059EA RID: 23018
		[DispId(-4)]
		IEnumerator GetEnumerator();
	}
}
