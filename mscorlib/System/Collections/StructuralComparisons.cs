﻿using System;

namespace System.Collections
{
	/// <summary>Provides objects for performing a structural comparison of two collection objects.</summary>
	// Token: 0x020009D0 RID: 2512
	public static class StructuralComparisons
	{
		/// <summary>Gets a predefined object that performs a structural comparison of two objects.</summary>
		/// <returns>A predefined object that is used to perform a structural comparison of two collection objects.</returns>
		// Token: 0x1700103A RID: 4154
		// (get) Token: 0x06005AEB RID: 23275 RVA: 0x00126594 File Offset: 0x00124794
		public static IComparer StructuralComparer
		{
			get
			{
				IComparer comparer = StructuralComparisons.s_StructuralComparer;
				if (comparer == null)
				{
					comparer = new StructuralComparer();
					StructuralComparisons.s_StructuralComparer = comparer;
				}
				return comparer;
			}
		}

		/// <summary>Gets a predefined object that compares two objects for structural equality.</summary>
		/// <returns>A predefined object that is used to compare two collection objects for structural equality.</returns>
		// Token: 0x1700103B RID: 4155
		// (get) Token: 0x06005AEC RID: 23276 RVA: 0x001265BC File Offset: 0x001247BC
		public static IEqualityComparer StructuralEqualityComparer
		{
			get
			{
				IEqualityComparer equalityComparer = StructuralComparisons.s_StructuralEqualityComparer;
				if (equalityComparer == null)
				{
					equalityComparer = new StructuralEqualityComparer();
					StructuralComparisons.s_StructuralEqualityComparer = equalityComparer;
				}
				return equalityComparer;
			}
		}

		// Token: 0x04002DD3 RID: 11731
		private static volatile IComparer s_StructuralComparer;

		// Token: 0x04002DD4 RID: 11732
		private static volatile IEqualityComparer s_StructuralEqualityComparer;
	}
}
